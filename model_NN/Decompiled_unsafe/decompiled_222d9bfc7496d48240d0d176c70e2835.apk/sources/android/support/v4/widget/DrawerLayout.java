package android.support.v4.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.SystemClock;
import android.support.annotation.ColorInt;
import android.support.annotation.DrawableRes;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.view.AccessibilityDelegateCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.KeyEventCompat;
import android.support.v4.view.MotionEventCompat;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.accessibility.AccessibilityNodeInfoCompat;
import android.support.v4.widget.ViewDragHelper;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.accessibility.AccessibilityEvent;
import java.util.ArrayList;
import java.util.List;

public class DrawerLayout extends ViewGroup implements DrawerLayoutImpl {
    private static final boolean ALLOW_EDGE_LOCK = false;
    /* access modifiers changed from: private */
    public static final boolean CAN_HIDE_DESCENDANTS = (Build.VERSION.SDK_INT >= 19);
    private static final boolean CHILDREN_DISALLOW_INTERCEPT = true;
    private static final int DEFAULT_SCRIM_COLOR = -1728053248;
    private static final int DRAWER_ELEVATION = 10;
    static final DrawerLayoutCompatImpl IMPL;
    /* access modifiers changed from: private */
    public static final int[] LAYOUT_ATTRS = {16842931};
    public static final int LOCK_MODE_LOCKED_CLOSED = 1;
    public static final int LOCK_MODE_LOCKED_OPEN = 2;
    public static final int LOCK_MODE_UNLOCKED = 0;
    private static final int MIN_DRAWER_MARGIN = 64;
    private static final int MIN_FLING_VELOCITY = 400;
    private static final int PEEK_DELAY = 160;
    private static final boolean SET_DRAWER_SHADOW_FROM_ELEVATION;
    public static final int STATE_DRAGGING = 1;
    public static final int STATE_IDLE = 0;
    public static final int STATE_SETTLING = 2;
    private static final String TAG = "DrawerLayout";
    private static final float TOUCH_SLOP_SENSITIVITY = 1.0f;
    private final ChildAccessibilityDelegate mChildAccessibilityDelegate;
    private boolean mChildrenCanceledTouch;
    private boolean mDisallowInterceptRequested;
    private boolean mDrawStatusBarBackground;
    private float mDrawerElevation;
    private int mDrawerState;
    private boolean mFirstLayout;
    private boolean mInLayout;
    private float mInitialMotionX;
    private float mInitialMotionY;
    private Object mLastInsets;
    private final ViewDragCallback mLeftCallback;
    private final ViewDragHelper mLeftDragger;
    private DrawerListener mListener;
    private int mLockModeLeft;
    private int mLockModeRight;
    private int mMinDrawerMargin;
    private final ArrayList<View> mNonDrawerViews;
    private final ViewDragCallback mRightCallback;
    private final ViewDragHelper mRightDragger;
    private int mScrimColor;
    private float mScrimOpacity;
    private Paint mScrimPaint;
    private Drawable mShadowEnd;
    private Drawable mShadowLeft;
    private Drawable mShadowLeftResolved;
    private Drawable mShadowRight;
    private Drawable mShadowRightResolved;
    private Drawable mShadowStart;
    private Drawable mStatusBarBackground;
    private CharSequence mTitleLeft;
    private CharSequence mTitleRight;

    interface DrawerLayoutCompatImpl {
        void applyMarginInsets(ViewGroup.MarginLayoutParams marginLayoutParams, Object obj, int i);

        void configureApplyInsets(View view);

        void dispatchChildInsets(View view, Object obj, int i);

        Drawable getDefaultStatusBarBackground(Context context);

        int getTopInset(Object obj);
    }

    public interface DrawerListener {
        void onDrawerClosed(View view);

        void onDrawerOpened(View view);

        void onDrawerSlide(View view, float f);

        void onDrawerStateChanged(int i);
    }

    static {
        boolean z;
        DrawerLayoutCompatImpl drawerLayoutCompatImpl;
        DrawerLayoutCompatImpl drawerLayoutCompatImpl2;
        if (Build.VERSION.SDK_INT >= 21) {
            z = true;
        } else {
            z = false;
        }
        SET_DRAWER_SHADOW_FROM_ELEVATION = z;
        if (Build.VERSION.SDK_INT >= 21) {
            new DrawerLayoutCompatImplApi21();
            IMPL = drawerLayoutCompatImpl2;
            return;
        }
        new DrawerLayoutCompatImplBase();
        IMPL = drawerLayoutCompatImpl;
    }

    public static abstract class SimpleDrawerListener implements DrawerListener {
        public void onDrawerSlide(View view, float f) {
        }

        public void onDrawerOpened(View view) {
        }

        public void onDrawerClosed(View view) {
        }

        public void onDrawerStateChanged(int i) {
        }
    }

    static class DrawerLayoutCompatImplBase implements DrawerLayoutCompatImpl {
        DrawerLayoutCompatImplBase() {
        }

        public void configureApplyInsets(View view) {
        }

        public void dispatchChildInsets(View view, Object obj, int i) {
        }

        public void applyMarginInsets(ViewGroup.MarginLayoutParams marginLayoutParams, Object obj, int i) {
        }

        public int getTopInset(Object obj) {
            return 0;
        }

        public Drawable getDefaultStatusBarBackground(Context context) {
            return null;
        }
    }

    static class DrawerLayoutCompatImplApi21 implements DrawerLayoutCompatImpl {
        DrawerLayoutCompatImplApi21() {
        }

        public void configureApplyInsets(View view) {
            DrawerLayoutCompatApi21.configureApplyInsets(view);
        }

        public void dispatchChildInsets(View view, Object obj, int i) {
            DrawerLayoutCompatApi21.dispatchChildInsets(view, obj, i);
        }

        public void applyMarginInsets(ViewGroup.MarginLayoutParams marginLayoutParams, Object obj, int i) {
            DrawerLayoutCompatApi21.applyMarginInsets(marginLayoutParams, obj, i);
        }

        public int getTopInset(Object obj) {
            return DrawerLayoutCompatApi21.getTopInset(obj);
        }

        public Drawable getDefaultStatusBarBackground(Context context) {
            return DrawerLayoutCompatApi21.getDefaultStatusBarBackground(context);
        }
    }

    public DrawerLayout(Context context) {
        this(context, null);
    }

    public DrawerLayout(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public DrawerLayout(android.content.Context r13, android.util.AttributeSet r14, int r15) {
        /*
            r12 = this;
            r0 = r12
            r1 = r13
            r2 = r14
            r3 = r15
            r6 = r0
            r7 = r1
            r8 = r2
            r9 = r3
            r6.<init>(r7, r8, r9)
            r6 = r0
            android.support.v4.widget.DrawerLayout$ChildAccessibilityDelegate r7 = new android.support.v4.widget.DrawerLayout$ChildAccessibilityDelegate
            r11 = r7
            r7 = r11
            r8 = r11
            r9 = r0
            r8.<init>()
            r6.mChildAccessibilityDelegate = r7
            r6 = r0
            r7 = -1728053248(0xffffffff99000000, float:-6.617445E-24)
            r6.mScrimColor = r7
            r6 = r0
            android.graphics.Paint r7 = new android.graphics.Paint
            r11 = r7
            r7 = r11
            r8 = r11
            r8.<init>()
            r6.mScrimPaint = r7
            r6 = r0
            r7 = 1
            r6.mFirstLayout = r7
            r6 = r0
            r7 = 0
            r6.mShadowStart = r7
            r6 = r0
            r7 = 0
            r6.mShadowEnd = r7
            r6 = r0
            r7 = 0
            r6.mShadowLeft = r7
            r6 = r0
            r7 = 0
            r6.mShadowRight = r7
            r6 = r0
            r7 = 262144(0x40000, float:3.67342E-40)
            r6.setDescendantFocusability(r7)
            r6 = r0
            android.content.res.Resources r6 = r6.getResources()
            android.util.DisplayMetrics r6 = r6.getDisplayMetrics()
            float r6 = r6.density
            r4 = r6
            r6 = r0
            r7 = 1115684864(0x42800000, float:64.0)
            r8 = r4
            float r7 = r7 * r8
            r8 = 1056964608(0x3f000000, float:0.5)
            float r7 = r7 + r8
            int r7 = (int) r7
            r6.mMinDrawerMargin = r7
            r6 = 1137180672(0x43c80000, float:400.0)
            r7 = r4
            float r6 = r6 * r7
            r5 = r6
            r6 = r0
            android.support.v4.widget.DrawerLayout$ViewDragCallback r7 = new android.support.v4.widget.DrawerLayout$ViewDragCallback
            r11 = r7
            r7 = r11
            r8 = r11
            r9 = r0
            r10 = 3
            r8.<init>(r10)
            r6.mLeftCallback = r7
            r6 = r0
            android.support.v4.widget.DrawerLayout$ViewDragCallback r7 = new android.support.v4.widget.DrawerLayout$ViewDragCallback
            r11 = r7
            r7 = r11
            r8 = r11
            r9 = r0
            r10 = 5
            r8.<init>(r10)
            r6.mRightCallback = r7
            r6 = r0
            r7 = r0
            r8 = 1065353216(0x3f800000, float:1.0)
            r9 = r0
            android.support.v4.widget.DrawerLayout$ViewDragCallback r9 = r9.mLeftCallback
            android.support.v4.widget.ViewDragHelper r7 = android.support.v4.widget.ViewDragHelper.create(r7, r8, r9)
            r6.mLeftDragger = r7
            r6 = r0
            android.support.v4.widget.ViewDragHelper r6 = r6.mLeftDragger
            r7 = 1
            r6.setEdgeTrackingEnabled(r7)
            r6 = r0
            android.support.v4.widget.ViewDragHelper r6 = r6.mLeftDragger
            r7 = r5
            r6.setMinVelocity(r7)
            r6 = r0
            android.support.v4.widget.DrawerLayout$ViewDragCallback r6 = r6.mLeftCallback
            r7 = r0
            android.support.v4.widget.ViewDragHelper r7 = r7.mLeftDragger
            r6.setDragger(r7)
            r6 = r0
            r7 = r0
            r8 = 1065353216(0x3f800000, float:1.0)
            r9 = r0
            android.support.v4.widget.DrawerLayout$ViewDragCallback r9 = r9.mRightCallback
            android.support.v4.widget.ViewDragHelper r7 = android.support.v4.widget.ViewDragHelper.create(r7, r8, r9)
            r6.mRightDragger = r7
            r6 = r0
            android.support.v4.widget.ViewDragHelper r6 = r6.mRightDragger
            r7 = 2
            r6.setEdgeTrackingEnabled(r7)
            r6 = r0
            android.support.v4.widget.ViewDragHelper r6 = r6.mRightDragger
            r7 = r5
            r6.setMinVelocity(r7)
            r6 = r0
            android.support.v4.widget.DrawerLayout$ViewDragCallback r6 = r6.mRightCallback
            r7 = r0
            android.support.v4.widget.ViewDragHelper r7 = r7.mRightDragger
            r6.setDragger(r7)
            r6 = r0
            r7 = 1
            r6.setFocusableInTouchMode(r7)
            r6 = r0
            r7 = 1
            android.support.v4.view.ViewCompat.setImportantForAccessibility(r6, r7)
            r6 = r0
            android.support.v4.widget.DrawerLayout$AccessibilityDelegate r7 = new android.support.v4.widget.DrawerLayout$AccessibilityDelegate
            r11 = r7
            r7 = r11
            r8 = r11
            r9 = r0
            r8.<init>()
            android.support.v4.view.ViewCompat.setAccessibilityDelegate(r6, r7)
            r6 = r0
            r7 = 0
            android.support.v4.view.ViewGroupCompat.setMotionEventSplittingEnabled(r6, r7)
            r6 = r0
            boolean r6 = android.support.v4.view.ViewCompat.getFitsSystemWindows(r6)
            if (r6 == 0) goto L_0x00f2
            android.support.v4.widget.DrawerLayout$DrawerLayoutCompatImpl r6 = android.support.v4.widget.DrawerLayout.IMPL
            r7 = r0
            r6.configureApplyInsets(r7)
            r6 = r0
            android.support.v4.widget.DrawerLayout$DrawerLayoutCompatImpl r7 = android.support.v4.widget.DrawerLayout.IMPL
            r8 = r1
            android.graphics.drawable.Drawable r7 = r7.getDefaultStatusBarBackground(r8)
            r6.mStatusBarBackground = r7
        L_0x00f2:
            r6 = r0
            r7 = 1092616192(0x41200000, float:10.0)
            r8 = r4
            float r7 = r7 * r8
            r6.mDrawerElevation = r7
            r6 = r0
            java.util.ArrayList r7 = new java.util.ArrayList
            r11 = r7
            r7 = r11
            r8 = r11
            r8.<init>()
            r6.mNonDrawerViews = r7
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v4.widget.DrawerLayout.<init>(android.content.Context, android.util.AttributeSet, int):void");
    }

    public void setDrawerElevation(float f) {
        this.mDrawerElevation = f;
        for (int i = 0; i < getChildCount(); i++) {
            View childAt = getChildAt(i);
            if (isDrawerView(childAt)) {
                ViewCompat.setElevation(childAt, this.mDrawerElevation);
            }
        }
    }

    public float getDrawerElevation() {
        if (SET_DRAWER_SHADOW_FROM_ELEVATION) {
            return this.mDrawerElevation;
        }
        return 0.0f;
    }

    public void setChildInsets(Object obj, boolean z) {
        boolean z2 = z;
        this.mLastInsets = obj;
        this.mDrawStatusBarBackground = z2;
        setWillNotDraw(!z2 && getBackground() == null);
        requestLayout();
    }

    public void setDrawerShadow(Drawable drawable, int i) {
        Drawable drawable2 = drawable;
        int i2 = i;
        if (!SET_DRAWER_SHADOW_FROM_ELEVATION) {
            if ((i2 & 8388611) == 8388611) {
                this.mShadowStart = drawable2;
            } else if ((i2 & GravityCompat.END) == 8388613) {
                this.mShadowEnd = drawable2;
            } else if ((i2 & 3) == 3) {
                this.mShadowLeft = drawable2;
            } else if ((i2 & 5) == 5) {
                this.mShadowRight = drawable2;
            } else {
                return;
            }
            resolveShadowDrawables();
            invalidate();
        }
    }

    public void setDrawerShadow(@DrawableRes int i, int i2) {
        setDrawerShadow(getResources().getDrawable(i), i2);
    }

    public void setScrimColor(@ColorInt int i) {
        this.mScrimColor = i;
        invalidate();
    }

    public void setDrawerListener(DrawerListener drawerListener) {
        this.mListener = drawerListener;
    }

    public void setDrawerLockMode(int i) {
        int i2 = i;
        setDrawerLockMode(i2, 3);
        setDrawerLockMode(i2, 5);
    }

    public void setDrawerLockMode(int i, int i2) {
        int i3 = i;
        int absoluteGravity = GravityCompat.getAbsoluteGravity(i2, ViewCompat.getLayoutDirection(this));
        if (absoluteGravity == 3) {
            this.mLockModeLeft = i3;
        } else if (absoluteGravity == 5) {
            this.mLockModeRight = i3;
        }
        if (i3 != 0) {
            (absoluteGravity == 3 ? this.mLeftDragger : this.mRightDragger).cancel();
        }
        switch (i3) {
            case 1:
                View findDrawerWithGravity = findDrawerWithGravity(absoluteGravity);
                if (findDrawerWithGravity != null) {
                    closeDrawer(findDrawerWithGravity);
                    return;
                }
                return;
            case 2:
                View findDrawerWithGravity2 = findDrawerWithGravity(absoluteGravity);
                if (findDrawerWithGravity2 != null) {
                    openDrawer(findDrawerWithGravity2);
                    return;
                }
                return;
            default:
                return;
        }
    }

    public void setDrawerLockMode(int i, View view) {
        Throwable th;
        StringBuilder sb;
        int i2 = i;
        View view2 = view;
        if (!isDrawerView(view2)) {
            Throwable th2 = th;
            new StringBuilder();
            new IllegalArgumentException(sb.append("View ").append(view2).append(" is not a ").append("drawer with appropriate layout_gravity").toString());
            throw th2;
        }
        setDrawerLockMode(i2, ((LayoutParams) view2.getLayoutParams()).gravity);
    }

    public int getDrawerLockMode(int i) {
        int absoluteGravity = GravityCompat.getAbsoluteGravity(i, ViewCompat.getLayoutDirection(this));
        if (absoluteGravity == 3) {
            return this.mLockModeLeft;
        }
        if (absoluteGravity == 5) {
            return this.mLockModeRight;
        }
        return 0;
    }

    public int getDrawerLockMode(View view) {
        int drawerViewAbsoluteGravity = getDrawerViewAbsoluteGravity(view);
        if (drawerViewAbsoluteGravity == 3) {
            return this.mLockModeLeft;
        }
        if (drawerViewAbsoluteGravity == 5) {
            return this.mLockModeRight;
        }
        return 0;
    }

    public void setDrawerTitle(int i, CharSequence charSequence) {
        CharSequence charSequence2 = charSequence;
        int absoluteGravity = GravityCompat.getAbsoluteGravity(i, ViewCompat.getLayoutDirection(this));
        if (absoluteGravity == 3) {
            this.mTitleLeft = charSequence2;
        } else if (absoluteGravity == 5) {
            this.mTitleRight = charSequence2;
        }
    }

    @Nullable
    public CharSequence getDrawerTitle(int i) {
        int absoluteGravity = GravityCompat.getAbsoluteGravity(i, ViewCompat.getLayoutDirection(this));
        if (absoluteGravity == 3) {
            return this.mTitleLeft;
        }
        if (absoluteGravity == 5) {
            return this.mTitleRight;
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public void updateDrawerState(int i, int i2, View view) {
        int i3;
        int i4 = i2;
        View view2 = view;
        int viewDragState = this.mLeftDragger.getViewDragState();
        int viewDragState2 = this.mRightDragger.getViewDragState();
        if (viewDragState == 1 || viewDragState2 == 1) {
            i3 = 1;
        } else if (viewDragState == 2 || viewDragState2 == 2) {
            i3 = 2;
        } else {
            i3 = 0;
        }
        if (view2 != null && i4 == 0) {
            LayoutParams layoutParams = (LayoutParams) view2.getLayoutParams();
            if (layoutParams.onScreen == 0.0f) {
                dispatchOnDrawerClosed(view2);
            } else if (layoutParams.onScreen == TOUCH_SLOP_SENSITIVITY) {
                dispatchOnDrawerOpened(view2);
            }
        }
        if (i3 != this.mDrawerState) {
            this.mDrawerState = i3;
            if (this.mListener != null) {
                this.mListener.onDrawerStateChanged(i3);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void dispatchOnDrawerClosed(View view) {
        View rootView;
        View view2 = view;
        LayoutParams layoutParams = (LayoutParams) view2.getLayoutParams();
        if (layoutParams.knownOpen) {
            layoutParams.knownOpen = false;
            if (this.mListener != null) {
                this.mListener.onDrawerClosed(view2);
            }
            updateChildrenImportantForAccessibility(view2, false);
            if (hasWindowFocus() && (rootView = getRootView()) != null) {
                rootView.sendAccessibilityEvent(32);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void dispatchOnDrawerOpened(View view) {
        View view2 = view;
        LayoutParams layoutParams = (LayoutParams) view2.getLayoutParams();
        if (!layoutParams.knownOpen) {
            layoutParams.knownOpen = true;
            if (this.mListener != null) {
                this.mListener.onDrawerOpened(view2);
            }
            updateChildrenImportantForAccessibility(view2, true);
            if (hasWindowFocus()) {
                sendAccessibilityEvent(32);
            }
            boolean requestFocus = view2.requestFocus();
        }
    }

    private void updateChildrenImportantForAccessibility(View view, boolean z) {
        View view2 = view;
        boolean z2 = z;
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = getChildAt(i);
            if ((z2 || isDrawerView(childAt)) && (!z2 || childAt != view2)) {
                ViewCompat.setImportantForAccessibility(childAt, 4);
            } else {
                ViewCompat.setImportantForAccessibility(childAt, 1);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void dispatchOnDrawerSlide(View view, float f) {
        View view2 = view;
        float f2 = f;
        if (this.mListener != null) {
            this.mListener.onDrawerSlide(view2, f2);
        }
    }

    /* access modifiers changed from: package-private */
    public void setDrawerViewOffset(View view, float f) {
        View view2 = view;
        float f2 = f;
        LayoutParams layoutParams = (LayoutParams) view2.getLayoutParams();
        if (f2 != layoutParams.onScreen) {
            layoutParams.onScreen = f2;
            dispatchOnDrawerSlide(view2, f2);
        }
    }

    /* access modifiers changed from: package-private */
    public float getDrawerViewOffset(View view) {
        return ((LayoutParams) view.getLayoutParams()).onScreen;
    }

    /* access modifiers changed from: package-private */
    public int getDrawerViewAbsoluteGravity(View view) {
        return GravityCompat.getAbsoluteGravity(((LayoutParams) view.getLayoutParams()).gravity, ViewCompat.getLayoutDirection(this));
    }

    /* access modifiers changed from: package-private */
    public boolean checkDrawerViewAbsoluteGravity(View view, int i) {
        int i2 = i;
        return (getDrawerViewAbsoluteGravity(view) & i2) == i2;
    }

    /* access modifiers changed from: package-private */
    public View findOpenDrawer() {
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = getChildAt(i);
            if (((LayoutParams) childAt.getLayoutParams()).knownOpen) {
                return childAt;
            }
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public void moveDrawerToOffset(View view, float f) {
        View view2 = view;
        float f2 = f;
        float drawerViewOffset = getDrawerViewOffset(view2);
        int width = view2.getWidth();
        int i = ((int) (((float) width) * f2)) - ((int) (((float) width) * drawerViewOffset));
        view2.offsetLeftAndRight(checkDrawerViewAbsoluteGravity(view2, 3) ? i : -i);
        setDrawerViewOffset(view2, f2);
    }

    /* access modifiers changed from: package-private */
    public View findDrawerWithGravity(int i) {
        int absoluteGravity = GravityCompat.getAbsoluteGravity(i, ViewCompat.getLayoutDirection(this)) & 7;
        int childCount = getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            View childAt = getChildAt(i2);
            if ((getDrawerViewAbsoluteGravity(childAt) & 7) == absoluteGravity) {
                return childAt;
            }
        }
        return null;
    }

    static String gravityToString(int i) {
        int i2 = i;
        if ((i2 & 3) == 3) {
            return "LEFT";
        }
        if ((i2 & 5) == 5) {
            return "RIGHT";
        }
        return Integer.toHexString(i2);
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.mFirstLayout = true;
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.mFirstLayout = true;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        Throwable th;
        StringBuilder sb;
        Throwable th2;
        StringBuilder sb2;
        Throwable th3;
        int i3 = i;
        int i4 = i2;
        int mode = View.MeasureSpec.getMode(i3);
        int mode2 = View.MeasureSpec.getMode(i4);
        int size = View.MeasureSpec.getSize(i3);
        int size2 = View.MeasureSpec.getSize(i4);
        if (!(mode == 1073741824 && mode2 == 1073741824)) {
            if (isInEditMode()) {
                if (mode != Integer.MIN_VALUE) {
                    if (mode == 0) {
                        size = 300;
                    }
                }
                if (mode2 != Integer.MIN_VALUE) {
                    if (mode2 == 0) {
                        size2 = 300;
                    }
                }
            } else {
                Throwable th4 = th3;
                new IllegalArgumentException("DrawerLayout must be measured with MeasureSpec.EXACTLY.");
                throw th4;
            }
        }
        setMeasuredDimension(size, size2);
        boolean z = this.mLastInsets != null && ViewCompat.getFitsSystemWindows(this);
        int layoutDirection = ViewCompat.getLayoutDirection(this);
        int childCount = getChildCount();
        for (int i5 = 0; i5 < childCount; i5++) {
            View childAt = getChildAt(i5);
            if (childAt.getVisibility() != 8) {
                LayoutParams layoutParams = (LayoutParams) childAt.getLayoutParams();
                if (z) {
                    int absoluteGravity = GravityCompat.getAbsoluteGravity(layoutParams.gravity, layoutDirection);
                    if (ViewCompat.getFitsSystemWindows(childAt)) {
                        IMPL.dispatchChildInsets(childAt, this.mLastInsets, absoluteGravity);
                    } else {
                        IMPL.applyMarginInsets(layoutParams, this.mLastInsets, absoluteGravity);
                    }
                }
                if (isContentView(childAt)) {
                    childAt.measure(View.MeasureSpec.makeMeasureSpec((size - layoutParams.leftMargin) - layoutParams.rightMargin, 1073741824), View.MeasureSpec.makeMeasureSpec((size2 - layoutParams.topMargin) - layoutParams.bottomMargin, 1073741824));
                } else if (isDrawerView(childAt)) {
                    if (SET_DRAWER_SHADOW_FROM_ELEVATION && ViewCompat.getElevation(childAt) != this.mDrawerElevation) {
                        ViewCompat.setElevation(childAt, this.mDrawerElevation);
                    }
                    int drawerViewAbsoluteGravity = getDrawerViewAbsoluteGravity(childAt) & 7;
                    if ((0 & drawerViewAbsoluteGravity) != 0) {
                        Throwable th5 = th2;
                        new StringBuilder();
                        new IllegalStateException(sb2.append("Child drawer has absolute gravity ").append(gravityToString(drawerViewAbsoluteGravity)).append(" but this ").append(TAG).append(" already has a ").append("drawer view along that edge").toString());
                        throw th5;
                    }
                    childAt.measure(getChildMeasureSpec(i3, this.mMinDrawerMargin + layoutParams.leftMargin + layoutParams.rightMargin, layoutParams.width), getChildMeasureSpec(i4, layoutParams.topMargin + layoutParams.bottomMargin, layoutParams.height));
                } else {
                    Throwable th6 = th;
                    new StringBuilder();
                    new IllegalStateException(sb.append("Child ").append(childAt).append(" at index ").append(i5).append(" does not have a valid layout_gravity - must be Gravity.LEFT, ").append("Gravity.RIGHT or Gravity.NO_GRAVITY").toString());
                    throw th6;
                }
            }
        }
    }

    private void resolveShadowDrawables() {
        if (!SET_DRAWER_SHADOW_FROM_ELEVATION) {
            this.mShadowLeftResolved = resolveLeftShadow();
            this.mShadowRightResolved = resolveRightShadow();
        }
    }

    private Drawable resolveLeftShadow() {
        int layoutDirection = ViewCompat.getLayoutDirection(this);
        if (layoutDirection == 0) {
            if (this.mShadowStart != null) {
                boolean mirror = mirror(this.mShadowStart, layoutDirection);
                return this.mShadowStart;
            }
        } else if (this.mShadowEnd != null) {
            boolean mirror2 = mirror(this.mShadowEnd, layoutDirection);
            return this.mShadowEnd;
        }
        return this.mShadowLeft;
    }

    private Drawable resolveRightShadow() {
        int layoutDirection = ViewCompat.getLayoutDirection(this);
        if (layoutDirection == 0) {
            if (this.mShadowEnd != null) {
                boolean mirror = mirror(this.mShadowEnd, layoutDirection);
                return this.mShadowEnd;
            }
        } else if (this.mShadowStart != null) {
            boolean mirror2 = mirror(this.mShadowStart, layoutDirection);
            return this.mShadowStart;
        }
        return this.mShadowRight;
    }

    private boolean mirror(Drawable drawable, int i) {
        Drawable drawable2 = drawable;
        int i2 = i;
        if (drawable2 == null || !DrawableCompat.isAutoMirrored(drawable2)) {
            return false;
        }
        DrawableCompat.setLayoutDirection(drawable2, i2);
        return true;
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        int i5;
        float f;
        int i6 = i2;
        int i7 = i4;
        this.mInLayout = true;
        int i8 = i3 - i;
        int childCount = getChildCount();
        for (int i9 = 0; i9 < childCount; i9++) {
            View childAt = getChildAt(i9);
            if (childAt.getVisibility() != 8) {
                LayoutParams layoutParams = (LayoutParams) childAt.getLayoutParams();
                if (isContentView(childAt)) {
                    childAt.layout(layoutParams.leftMargin, layoutParams.topMargin, layoutParams.leftMargin + childAt.getMeasuredWidth(), layoutParams.topMargin + childAt.getMeasuredHeight());
                } else {
                    int measuredWidth = childAt.getMeasuredWidth();
                    int measuredHeight = childAt.getMeasuredHeight();
                    if (checkDrawerViewAbsoluteGravity(childAt, 3)) {
                        i5 = (-measuredWidth) + ((int) (((float) measuredWidth) * layoutParams.onScreen));
                        f = ((float) (measuredWidth + i5)) / ((float) measuredWidth);
                    } else {
                        i5 = i8 - ((int) (((float) measuredWidth) * layoutParams.onScreen));
                        f = ((float) (i8 - i5)) / ((float) measuredWidth);
                    }
                    boolean z2 = f != layoutParams.onScreen;
                    switch (layoutParams.gravity & 112) {
                        case 16:
                            int i10 = i7 - i6;
                            int i11 = (i10 - measuredHeight) / 2;
                            if (i11 < layoutParams.topMargin) {
                                i11 = layoutParams.topMargin;
                            } else {
                                if (i11 + measuredHeight > i10 - layoutParams.bottomMargin) {
                                    i11 = (i10 - layoutParams.bottomMargin) - measuredHeight;
                                }
                            }
                            childAt.layout(i5, i11, i5 + measuredWidth, i11 + measuredHeight);
                            break;
                        case 80:
                            int i12 = i7 - i6;
                            childAt.layout(i5, (i12 - layoutParams.bottomMargin) - childAt.getMeasuredHeight(), i5 + measuredWidth, i12 - layoutParams.bottomMargin);
                            break;
                        default:
                            childAt.layout(i5, layoutParams.topMargin, i5 + measuredWidth, layoutParams.topMargin + measuredHeight);
                            break;
                    }
                    if (z2) {
                        setDrawerViewOffset(childAt, f);
                    }
                    int i13 = layoutParams.onScreen > 0.0f ? 0 : 4;
                    if (childAt.getVisibility() != i13) {
                        childAt.setVisibility(i13);
                    }
                }
            }
        }
        this.mInLayout = false;
        this.mFirstLayout = false;
    }

    public void requestLayout() {
        if (!this.mInLayout) {
            super.requestLayout();
        }
    }

    public void computeScroll() {
        int childCount = getChildCount();
        float f = 0.0f;
        for (int i = 0; i < childCount; i++) {
            f = Math.max(f, ((LayoutParams) getChildAt(i).getLayoutParams()).onScreen);
        }
        this.mScrimOpacity = f;
        if (this.mLeftDragger.continueSettling(true) || this.mRightDragger.continueSettling(true)) {
            ViewCompat.postInvalidateOnAnimation(this);
        }
    }

    private static boolean hasOpaqueBackground(View view) {
        Drawable background = view.getBackground();
        if (background == null) {
            return false;
        }
        return background.getOpacity() == -1;
    }

    public void setStatusBarBackground(Drawable drawable) {
        this.mStatusBarBackground = drawable;
        invalidate();
    }

    public Drawable getStatusBarBackgroundDrawable() {
        return this.mStatusBarBackground;
    }

    public void setStatusBarBackground(int i) {
        int i2 = i;
        this.mStatusBarBackground = i2 != 0 ? ContextCompat.getDrawable(getContext(), i2) : null;
        invalidate();
    }

    public void setStatusBarBackgroundColor(@ColorInt int i) {
        Drawable drawable;
        new ColorDrawable(i);
        this.mStatusBarBackground = drawable;
        invalidate();
    }

    public void onRtlPropertiesChanged(int i) {
        resolveShadowDrawables();
    }

    public void onDraw(Canvas canvas) {
        int topInset;
        Canvas canvas2 = canvas;
        super.onDraw(canvas2);
        if (this.mDrawStatusBarBackground && this.mStatusBarBackground != null && (topInset = IMPL.getTopInset(this.mLastInsets)) > 0) {
            this.mStatusBarBackground.setBounds(0, 0, getWidth(), topInset);
            this.mStatusBarBackground.draw(canvas2);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(long, long):long}
      ClspMth{java.lang.Math.max(float, float):float} */
    /* access modifiers changed from: protected */
    public boolean drawChild(Canvas canvas, View view, long j) {
        Canvas canvas2 = canvas;
        View view2 = view;
        long j2 = j;
        int height = getHeight();
        boolean isContentView = isContentView(view2);
        int i = 0;
        int width = getWidth();
        int save = canvas2.save();
        if (isContentView) {
            int childCount = getChildCount();
            for (int i2 = 0; i2 < childCount; i2++) {
                View childAt = getChildAt(i2);
                if (childAt != view2 && childAt.getVisibility() == 0 && hasOpaqueBackground(childAt) && isDrawerView(childAt) && childAt.getHeight() >= height) {
                    if (checkDrawerViewAbsoluteGravity(childAt, 3)) {
                        int right = childAt.getRight();
                        if (right > i) {
                            i = right;
                        }
                    } else {
                        int left = childAt.getLeft();
                        if (left < width) {
                            width = left;
                        }
                    }
                }
            }
            boolean clipRect = canvas2.clipRect(i, 0, width, getHeight());
        }
        boolean drawChild = super.drawChild(canvas2, view2, j2);
        canvas2.restoreToCount(save);
        if (this.mScrimOpacity > 0.0f && isContentView) {
            this.mScrimPaint.setColor((((int) (((float) ((this.mScrimColor & ViewCompat.MEASURED_STATE_MASK) >>> 24)) * this.mScrimOpacity)) << 24) | (this.mScrimColor & ViewCompat.MEASURED_SIZE_MASK));
            canvas2.drawRect((float) i, 0.0f, (float) width, (float) getHeight(), this.mScrimPaint);
        } else if (this.mShadowLeftResolved != null && checkDrawerViewAbsoluteGravity(view2, 3)) {
            int intrinsicWidth = this.mShadowLeftResolved.getIntrinsicWidth();
            int right2 = view2.getRight();
            float max = Math.max(0.0f, Math.min(((float) right2) / ((float) this.mLeftDragger.getEdgeSize()), (float) TOUCH_SLOP_SENSITIVITY));
            this.mShadowLeftResolved.setBounds(right2, view2.getTop(), right2 + intrinsicWidth, view2.getBottom());
            this.mShadowLeftResolved.setAlpha((int) (255.0f * max));
            this.mShadowLeftResolved.draw(canvas2);
        } else if (this.mShadowRightResolved != null && checkDrawerViewAbsoluteGravity(view2, 5)) {
            int intrinsicWidth2 = this.mShadowRightResolved.getIntrinsicWidth();
            int left2 = view2.getLeft();
            float max2 = Math.max(0.0f, Math.min(((float) (getWidth() - left2)) / ((float) this.mRightDragger.getEdgeSize()), (float) TOUCH_SLOP_SENSITIVITY));
            this.mShadowRightResolved.setBounds(left2 - intrinsicWidth2, view2.getTop(), left2, view2.getBottom());
            this.mShadowRightResolved.setAlpha((int) (255.0f * max2));
            this.mShadowRightResolved.draw(canvas2);
        }
        return drawChild;
    }

    /* access modifiers changed from: package-private */
    public boolean isContentView(View view) {
        return ((LayoutParams) view.getLayoutParams()).gravity == 0;
    }

    /* access modifiers changed from: package-private */
    public boolean isDrawerView(View view) {
        View view2 = view;
        return (GravityCompat.getAbsoluteGravity(((LayoutParams) view2.getLayoutParams()).gravity, ViewCompat.getLayoutDirection(view2)) & 7) != 0;
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        View findTopChildUnder;
        boolean z;
        MotionEvent motionEvent2 = motionEvent;
        int actionMasked = MotionEventCompat.getActionMasked(motionEvent2);
        boolean shouldInterceptTouchEvent = this.mLeftDragger.shouldInterceptTouchEvent(motionEvent2) | this.mRightDragger.shouldInterceptTouchEvent(motionEvent2);
        boolean z2 = false;
        switch (actionMasked) {
            case 0:
                float x = motionEvent2.getX();
                float y = motionEvent2.getY();
                this.mInitialMotionX = x;
                this.mInitialMotionY = y;
                if (this.mScrimOpacity > 0.0f && (findTopChildUnder = this.mLeftDragger.findTopChildUnder((int) x, (int) y)) != null && isContentView(findTopChildUnder)) {
                    z2 = true;
                }
                this.mDisallowInterceptRequested = false;
                this.mChildrenCanceledTouch = false;
                break;
            case 1:
            case 3:
                closeDrawers(true);
                this.mDisallowInterceptRequested = false;
                this.mChildrenCanceledTouch = false;
                break;
            case 2:
                if (this.mLeftDragger.checkTouchSlop(3)) {
                    this.mLeftCallback.removeCallbacks();
                    this.mRightCallback.removeCallbacks();
                    break;
                }
                break;
        }
        if (shouldInterceptTouchEvent || z2 || hasPeekingDrawer() || this.mChildrenCanceledTouch) {
            z = true;
        } else {
            z = false;
        }
        return z;
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        View findOpenDrawer;
        MotionEvent motionEvent2 = motionEvent;
        this.mLeftDragger.processTouchEvent(motionEvent2);
        this.mRightDragger.processTouchEvent(motionEvent2);
        switch (motionEvent2.getAction() & 255) {
            case 0:
                this.mInitialMotionX = motionEvent2.getX();
                this.mInitialMotionY = motionEvent2.getY();
                this.mDisallowInterceptRequested = false;
                this.mChildrenCanceledTouch = false;
                break;
            case 1:
                float x = motionEvent2.getX();
                float y = motionEvent2.getY();
                boolean z = true;
                View findTopChildUnder = this.mLeftDragger.findTopChildUnder((int) x, (int) y);
                if (findTopChildUnder != null && isContentView(findTopChildUnder)) {
                    float f = x - this.mInitialMotionX;
                    float f2 = y - this.mInitialMotionY;
                    int touchSlop = this.mLeftDragger.getTouchSlop();
                    if ((f * f) + (f2 * f2) < ((float) (touchSlop * touchSlop)) && (findOpenDrawer = findOpenDrawer()) != null) {
                        z = getDrawerLockMode(findOpenDrawer) == 2;
                    }
                }
                closeDrawers(z);
                this.mDisallowInterceptRequested = false;
                break;
            case 3:
                closeDrawers(true);
                this.mDisallowInterceptRequested = false;
                this.mChildrenCanceledTouch = false;
                break;
        }
        return true;
    }

    public void requestDisallowInterceptTouchEvent(boolean z) {
        boolean z2 = z;
        super.requestDisallowInterceptTouchEvent(z2);
        this.mDisallowInterceptRequested = z2;
        if (z2) {
            closeDrawers(true);
        }
    }

    public void closeDrawers() {
        closeDrawers(false);
    }

    /* access modifiers changed from: package-private */
    public void closeDrawers(boolean z) {
        boolean z2 = z;
        boolean z3 = false;
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = getChildAt(i);
            LayoutParams layoutParams = (LayoutParams) childAt.getLayoutParams();
            if (isDrawerView(childAt) && (!z2 || layoutParams.isPeeking)) {
                int width = childAt.getWidth();
                if (checkDrawerViewAbsoluteGravity(childAt, 3)) {
                    z3 |= this.mLeftDragger.smoothSlideViewTo(childAt, -width, childAt.getTop());
                } else {
                    z3 |= this.mRightDragger.smoothSlideViewTo(childAt, getWidth(), childAt.getTop());
                }
                layoutParams.isPeeking = false;
            }
        }
        this.mLeftCallback.removeCallbacks();
        this.mRightCallback.removeCallbacks();
        if (z3) {
            invalidate();
        }
    }

    public void openDrawer(View view) {
        Throwable th;
        StringBuilder sb;
        View view2 = view;
        if (!isDrawerView(view2)) {
            Throwable th2 = th;
            new StringBuilder();
            new IllegalArgumentException(sb.append("View ").append(view2).append(" is not a sliding drawer").toString());
            throw th2;
        }
        if (this.mFirstLayout) {
            LayoutParams layoutParams = (LayoutParams) view2.getLayoutParams();
            layoutParams.onScreen = TOUCH_SLOP_SENSITIVITY;
            layoutParams.knownOpen = true;
            updateChildrenImportantForAccessibility(view2, true);
        } else if (checkDrawerViewAbsoluteGravity(view2, 3)) {
            boolean smoothSlideViewTo = this.mLeftDragger.smoothSlideViewTo(view2, 0, view2.getTop());
        } else {
            boolean smoothSlideViewTo2 = this.mRightDragger.smoothSlideViewTo(view2, getWidth() - view2.getWidth(), view2.getTop());
        }
        invalidate();
    }

    public void openDrawer(int i) {
        Throwable th;
        StringBuilder sb;
        int i2 = i;
        View findDrawerWithGravity = findDrawerWithGravity(i2);
        if (findDrawerWithGravity == null) {
            Throwable th2 = th;
            new StringBuilder();
            new IllegalArgumentException(sb.append("No drawer view found with gravity ").append(gravityToString(i2)).toString());
            throw th2;
        }
        openDrawer(findDrawerWithGravity);
    }

    public void closeDrawer(View view) {
        Throwable th;
        StringBuilder sb;
        View view2 = view;
        if (!isDrawerView(view2)) {
            Throwable th2 = th;
            new StringBuilder();
            new IllegalArgumentException(sb.append("View ").append(view2).append(" is not a sliding drawer").toString());
            throw th2;
        }
        if (this.mFirstLayout) {
            LayoutParams layoutParams = (LayoutParams) view2.getLayoutParams();
            layoutParams.onScreen = 0.0f;
            layoutParams.knownOpen = false;
        } else if (checkDrawerViewAbsoluteGravity(view2, 3)) {
            boolean smoothSlideViewTo = this.mLeftDragger.smoothSlideViewTo(view2, -view2.getWidth(), view2.getTop());
        } else {
            boolean smoothSlideViewTo2 = this.mRightDragger.smoothSlideViewTo(view2, getWidth(), view2.getTop());
        }
        invalidate();
    }

    public void closeDrawer(int i) {
        Throwable th;
        StringBuilder sb;
        int i2 = i;
        View findDrawerWithGravity = findDrawerWithGravity(i2);
        if (findDrawerWithGravity == null) {
            Throwable th2 = th;
            new StringBuilder();
            new IllegalArgumentException(sb.append("No drawer view found with gravity ").append(gravityToString(i2)).toString());
            throw th2;
        }
        closeDrawer(findDrawerWithGravity);
    }

    public boolean isDrawerOpen(View view) {
        Throwable th;
        StringBuilder sb;
        View view2 = view;
        if (isDrawerView(view2)) {
            return ((LayoutParams) view2.getLayoutParams()).knownOpen;
        }
        Throwable th2 = th;
        new StringBuilder();
        new IllegalArgumentException(sb.append("View ").append(view2).append(" is not a drawer").toString());
        throw th2;
    }

    public boolean isDrawerOpen(int i) {
        View findDrawerWithGravity = findDrawerWithGravity(i);
        if (findDrawerWithGravity != null) {
            return isDrawerOpen(findDrawerWithGravity);
        }
        return false;
    }

    public boolean isDrawerVisible(View view) {
        Throwable th;
        StringBuilder sb;
        View view2 = view;
        if (!isDrawerView(view2)) {
            Throwable th2 = th;
            new StringBuilder();
            new IllegalArgumentException(sb.append("View ").append(view2).append(" is not a drawer").toString());
            throw th2;
        }
        return ((LayoutParams) view2.getLayoutParams()).onScreen > 0.0f;
    }

    public boolean isDrawerVisible(int i) {
        View findDrawerWithGravity = findDrawerWithGravity(i);
        if (findDrawerWithGravity != null) {
            return isDrawerVisible(findDrawerWithGravity);
        }
        return false;
    }

    private boolean hasPeekingDrawer() {
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            if (((LayoutParams) getChildAt(i).getLayoutParams()).isPeeking) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public ViewGroup.LayoutParams generateDefaultLayoutParams() {
        ViewGroup.LayoutParams layoutParams;
        new LayoutParams(-1, -1);
        return layoutParams;
    }

    /* access modifiers changed from: protected */
    public ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams layoutParams) {
        ViewGroup.LayoutParams layoutParams2;
        ViewGroup.LayoutParams layoutParams3;
        ViewGroup.LayoutParams layoutParams4;
        ViewGroup.LayoutParams layoutParams5;
        ViewGroup.LayoutParams layoutParams6 = layoutParams;
        if (layoutParams6 instanceof LayoutParams) {
            layoutParams3 = layoutParams5;
            new LayoutParams((LayoutParams) layoutParams6);
        } else if (layoutParams6 instanceof ViewGroup.MarginLayoutParams) {
            layoutParams3 = layoutParams4;
            new LayoutParams((ViewGroup.MarginLayoutParams) layoutParams6);
        } else {
            layoutParams3 = layoutParams2;
            new LayoutParams(layoutParams6);
        }
        return layoutParams3;
    }

    /* access modifiers changed from: protected */
    public boolean checkLayoutParams(ViewGroup.LayoutParams layoutParams) {
        ViewGroup.LayoutParams layoutParams2 = layoutParams;
        return (layoutParams2 instanceof LayoutParams) && super.checkLayoutParams(layoutParams2);
    }

    public ViewGroup.LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        ViewGroup.LayoutParams layoutParams;
        new LayoutParams(getContext(), attributeSet);
        return layoutParams;
    }

    public void addFocusables(ArrayList<View> arrayList, int i, int i2) {
        ArrayList<View> arrayList2 = arrayList;
        int i3 = i;
        int i4 = i2;
        if (getDescendantFocusability() != 393216) {
            int childCount = getChildCount();
            boolean z = false;
            for (int i5 = 0; i5 < childCount; i5++) {
                View childAt = getChildAt(i5);
                if (!isDrawerView(childAt)) {
                    boolean add = this.mNonDrawerViews.add(childAt);
                } else if (isDrawerOpen(childAt)) {
                    z = true;
                    childAt.addFocusables(arrayList2, i3, i4);
                }
            }
            if (!z) {
                int size = this.mNonDrawerViews.size();
                for (int i6 = 0; i6 < size; i6++) {
                    View view = this.mNonDrawerViews.get(i6);
                    if (view.getVisibility() == 0) {
                        view.addFocusables(arrayList2, i3, i4);
                    }
                }
            }
            this.mNonDrawerViews.clear();
        }
    }

    private boolean hasVisibleDrawer() {
        return findVisibleDrawer() != null;
    }

    /* access modifiers changed from: private */
    public View findVisibleDrawer() {
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = getChildAt(i);
            if (isDrawerView(childAt) && isDrawerVisible(childAt)) {
                return childAt;
            }
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public void cancelChildViewTouch() {
        if (!this.mChildrenCanceledTouch) {
            long uptimeMillis = SystemClock.uptimeMillis();
            MotionEvent obtain = MotionEvent.obtain(uptimeMillis, uptimeMillis, 3, 0.0f, 0.0f, 0);
            int childCount = getChildCount();
            for (int i = 0; i < childCount; i++) {
                boolean dispatchTouchEvent = getChildAt(i).dispatchTouchEvent(obtain);
            }
            obtain.recycle();
            this.mChildrenCanceledTouch = true;
        }
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        int i2 = i;
        KeyEvent keyEvent2 = keyEvent;
        if (i2 != 4 || !hasVisibleDrawer()) {
            return super.onKeyDown(i2, keyEvent2);
        }
        KeyEventCompat.startTracking(keyEvent2);
        return true;
    }

    public boolean onKeyUp(int i, KeyEvent keyEvent) {
        int i2 = i;
        KeyEvent keyEvent2 = keyEvent;
        if (i2 != 4) {
            return super.onKeyUp(i2, keyEvent2);
        }
        View findVisibleDrawer = findVisibleDrawer();
        if (findVisibleDrawer != null && getDrawerLockMode(findVisibleDrawer) == 0) {
            closeDrawers();
        }
        return findVisibleDrawer != null;
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Parcelable parcelable) {
        View findDrawerWithGravity;
        SavedState savedState = (SavedState) parcelable;
        super.onRestoreInstanceState(savedState.getSuperState());
        if (!(savedState.openDrawerGravity == 0 || (findDrawerWithGravity = findDrawerWithGravity(savedState.openDrawerGravity)) == null)) {
            openDrawer(findDrawerWithGravity);
        }
        setDrawerLockMode(savedState.lockModeLeft, 3);
        setDrawerLockMode(savedState.lockModeRight, 5);
    }

    /* access modifiers changed from: protected */
    public Parcelable onSaveInstanceState() {
        SavedState savedState;
        new SavedState(super.onSaveInstanceState());
        SavedState savedState2 = savedState;
        View findOpenDrawer = findOpenDrawer();
        if (findOpenDrawer != null) {
            savedState2.openDrawerGravity = ((LayoutParams) findOpenDrawer.getLayoutParams()).gravity;
        }
        savedState2.lockModeLeft = this.mLockModeLeft;
        savedState2.lockModeRight = this.mLockModeRight;
        return savedState2;
    }

    public void addView(View view, int i, ViewGroup.LayoutParams layoutParams) {
        View view2 = view;
        super.addView(view2, i, layoutParams);
        if (findOpenDrawer() != null || isDrawerView(view2)) {
            ViewCompat.setImportantForAccessibility(view2, 4);
        } else {
            ViewCompat.setImportantForAccessibility(view2, 1);
        }
        if (!CAN_HIDE_DESCENDANTS) {
            ViewCompat.setAccessibilityDelegate(view2, this.mChildAccessibilityDelegate);
        }
    }

    /* access modifiers changed from: private */
    public static boolean includeChildForAccessibility(View view) {
        View view2 = view;
        return (ViewCompat.getImportantForAccessibility(view2) == 4 || ViewCompat.getImportantForAccessibility(view2) == 2) ? false : true;
    }

    protected static class SavedState extends View.BaseSavedState {
        public static final Parcelable.Creator<SavedState> CREATOR;
        int lockModeLeft = 0;
        int lockModeRight = 0;
        int openDrawerGravity = 0;

        /* JADX WARNING: Illegal instructions before constructor call */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public SavedState(android.os.Parcel r5) {
            /*
                r4 = this;
                r0 = r4
                r1 = r5
                r2 = r0
                r3 = r1
                r2.<init>(r3)
                r2 = r0
                r3 = 0
                r2.openDrawerGravity = r3
                r2 = r0
                r3 = 0
                r2.lockModeLeft = r3
                r2 = r0
                r3 = 0
                r2.lockModeRight = r3
                r2 = r0
                r3 = r1
                int r3 = r3.readInt()
                r2.openDrawerGravity = r3
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: android.support.v4.widget.DrawerLayout.SavedState.<init>(android.os.Parcel):void");
        }

        public SavedState(Parcelable parcelable) {
            super(parcelable);
        }

        public void writeToParcel(Parcel parcel, int i) {
            Parcel parcel2 = parcel;
            super.writeToParcel(parcel2, i);
            parcel2.writeInt(this.openDrawerGravity);
        }

        static {
            Parcelable.Creator<SavedState> creator;
            new Parcelable.Creator<SavedState>() {
                public SavedState createFromParcel(Parcel parcel) {
                    SavedState savedState;
                    new SavedState(parcel);
                    return savedState;
                }

                public SavedState[] newArray(int i) {
                    return new SavedState[i];
                }
            };
            CREATOR = creator;
        }
    }

    private class ViewDragCallback extends ViewDragHelper.Callback {
        private final int mAbsGravity;
        private ViewDragHelper mDragger;
        private final Runnable mPeekRunnable;

        public ViewDragCallback(int i) {
            Runnable runnable;
            new Runnable() {
                public void run() {
                    ViewDragCallback.this.peekDrawer();
                }
            };
            this.mPeekRunnable = runnable;
            this.mAbsGravity = i;
        }

        public void setDragger(ViewDragHelper viewDragHelper) {
            this.mDragger = viewDragHelper;
        }

        public void removeCallbacks() {
            boolean removeCallbacks = DrawerLayout.this.removeCallbacks(this.mPeekRunnable);
        }

        public boolean tryCaptureView(View view, int i) {
            View view2 = view;
            return DrawerLayout.this.isDrawerView(view2) && DrawerLayout.this.checkDrawerViewAbsoluteGravity(view2, this.mAbsGravity) && DrawerLayout.this.getDrawerLockMode(view2) == 0;
        }

        public void onViewDragStateChanged(int i) {
            DrawerLayout.this.updateDrawerState(this.mAbsGravity, i, this.mDragger.getCapturedView());
        }

        public void onViewPositionChanged(View view, int i, int i2, int i3, int i4) {
            float width;
            View view2 = view;
            int i5 = i;
            int width2 = view2.getWidth();
            if (DrawerLayout.this.checkDrawerViewAbsoluteGravity(view2, 3)) {
                width = ((float) (width2 + i5)) / ((float) width2);
            } else {
                width = ((float) (DrawerLayout.this.getWidth() - i5)) / ((float) width2);
            }
            DrawerLayout.this.setDrawerViewOffset(view2, width);
            view2.setVisibility(width == 0.0f ? 4 : 0);
            DrawerLayout.this.invalidate();
        }

        public void onViewCaptured(View view, int i) {
            ((LayoutParams) view.getLayoutParams()).isPeeking = false;
            closeOtherDrawer();
        }

        private void closeOtherDrawer() {
            View findDrawerWithGravity = DrawerLayout.this.findDrawerWithGravity(this.mAbsGravity == 3 ? 5 : 3);
            if (findDrawerWithGravity != null) {
                DrawerLayout.this.closeDrawer(findDrawerWithGravity);
            }
        }

        public void onViewReleased(View view, float f, float f2) {
            int i;
            View view2 = view;
            float f3 = f;
            float drawerViewOffset = DrawerLayout.this.getDrawerViewOffset(view2);
            int width = view2.getWidth();
            if (DrawerLayout.this.checkDrawerViewAbsoluteGravity(view2, 3)) {
                i = (f3 > 0.0f || (f3 == 0.0f && drawerViewOffset > 0.5f)) ? 0 : -width;
            } else {
                int width2 = DrawerLayout.this.getWidth();
                i = (f3 < 0.0f || (f3 == 0.0f && drawerViewOffset > 0.5f)) ? width2 - width : width2;
            }
            boolean z = this.mDragger.settleCapturedViewAt(i, view2.getTop());
            DrawerLayout.this.invalidate();
        }

        public void onEdgeTouched(int i, int i2) {
            boolean postDelayed = DrawerLayout.this.postDelayed(this.mPeekRunnable, 160);
        }

        /* access modifiers changed from: private */
        public void peekDrawer() {
            View findDrawerWithGravity;
            int width;
            int edgeSize = this.mDragger.getEdgeSize();
            boolean z = this.mAbsGravity == 3;
            if (z) {
                findDrawerWithGravity = DrawerLayout.this.findDrawerWithGravity(3);
                width = (findDrawerWithGravity != null ? -findDrawerWithGravity.getWidth() : 0) + edgeSize;
            } else {
                findDrawerWithGravity = DrawerLayout.this.findDrawerWithGravity(5);
                width = DrawerLayout.this.getWidth() - edgeSize;
            }
            if (findDrawerWithGravity == null) {
                return;
            }
            if (((z && findDrawerWithGravity.getLeft() < width) || (!z && findDrawerWithGravity.getLeft() > width)) && DrawerLayout.this.getDrawerLockMode(findDrawerWithGravity) == 0) {
                LayoutParams layoutParams = (LayoutParams) findDrawerWithGravity.getLayoutParams();
                boolean smoothSlideViewTo = this.mDragger.smoothSlideViewTo(findDrawerWithGravity, width, findDrawerWithGravity.getTop());
                layoutParams.isPeeking = true;
                DrawerLayout.this.invalidate();
                closeOtherDrawer();
                DrawerLayout.this.cancelChildViewTouch();
            }
        }

        public boolean onEdgeLock(int i) {
            return false;
        }

        public void onEdgeDragStarted(int i, int i2) {
            View findDrawerWithGravity;
            int i3 = i2;
            if ((i & 1) == 1) {
                findDrawerWithGravity = DrawerLayout.this.findDrawerWithGravity(3);
            } else {
                findDrawerWithGravity = DrawerLayout.this.findDrawerWithGravity(5);
            }
            if (findDrawerWithGravity != null && DrawerLayout.this.getDrawerLockMode(findDrawerWithGravity) == 0) {
                this.mDragger.captureChildView(findDrawerWithGravity, i3);
            }
        }

        public int getViewHorizontalDragRange(View view) {
            View view2 = view;
            return DrawerLayout.this.isDrawerView(view2) ? view2.getWidth() : 0;
        }

        public int clampViewPositionHorizontal(View view, int i, int i2) {
            View view2 = view;
            int i3 = i;
            if (DrawerLayout.this.checkDrawerViewAbsoluteGravity(view2, 3)) {
                return Math.max(-view2.getWidth(), Math.min(i3, 0));
            }
            int width = DrawerLayout.this.getWidth();
            return Math.max(width - view2.getWidth(), Math.min(i3, width));
        }

        public int clampViewPositionVertical(View view, int i, int i2) {
            return view.getTop();
        }
    }

    public static class LayoutParams extends ViewGroup.MarginLayoutParams {
        public int gravity;
        boolean isPeeking;
        boolean knownOpen;
        float onScreen;

        /* JADX WARNING: Illegal instructions before constructor call */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public LayoutParams(android.content.Context r9, android.util.AttributeSet r10) {
            /*
                r8 = this;
                r0 = r8
                r1 = r9
                r2 = r10
                r4 = r0
                r5 = r1
                r6 = r2
                r4.<init>(r5, r6)
                r4 = r0
                r5 = 0
                r4.gravity = r5
                r4 = r1
                r5 = r2
                int[] r6 = android.support.v4.widget.DrawerLayout.LAYOUT_ATTRS
                android.content.res.TypedArray r4 = r4.obtainStyledAttributes(r5, r6)
                r3 = r4
                r4 = r0
                r5 = r3
                r6 = 0
                r7 = 0
                int r5 = r5.getInt(r6, r7)
                r4.gravity = r5
                r4 = r3
                r4.recycle()
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: android.support.v4.widget.DrawerLayout.LayoutParams.<init>(android.content.Context, android.util.AttributeSet):void");
        }

        public LayoutParams(int i, int i2) {
            super(i, i2);
            this.gravity = 0;
        }

        public LayoutParams(int i, int i2, int i3) {
            this(i, i2);
            this.gravity = i3;
        }

        /* JADX WARNING: Illegal instructions before constructor call */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public LayoutParams(android.support.v4.widget.DrawerLayout.LayoutParams r5) {
            /*
                r4 = this;
                r0 = r4
                r1 = r5
                r2 = r0
                r3 = r1
                r2.<init>(r3)
                r2 = r0
                r3 = 0
                r2.gravity = r3
                r2 = r0
                r3 = r1
                int r3 = r3.gravity
                r2.gravity = r3
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: android.support.v4.widget.DrawerLayout.LayoutParams.<init>(android.support.v4.widget.DrawerLayout$LayoutParams):void");
        }

        public LayoutParams(ViewGroup.LayoutParams layoutParams) {
            super(layoutParams);
            this.gravity = 0;
        }

        public LayoutParams(ViewGroup.MarginLayoutParams marginLayoutParams) {
            super(marginLayoutParams);
            this.gravity = 0;
        }
    }

    class AccessibilityDelegate extends AccessibilityDelegateCompat {
        private final Rect mTmpRect;

        AccessibilityDelegate() {
            Rect rect;
            new Rect();
            this.mTmpRect = rect;
        }

        public void onInitializeAccessibilityNodeInfo(View view, AccessibilityNodeInfoCompat accessibilityNodeInfoCompat) {
            View view2 = view;
            AccessibilityNodeInfoCompat accessibilityNodeInfoCompat2 = accessibilityNodeInfoCompat;
            if (DrawerLayout.CAN_HIDE_DESCENDANTS) {
                super.onInitializeAccessibilityNodeInfo(view2, accessibilityNodeInfoCompat2);
            } else {
                AccessibilityNodeInfoCompat obtain = AccessibilityNodeInfoCompat.obtain(accessibilityNodeInfoCompat2);
                super.onInitializeAccessibilityNodeInfo(view2, obtain);
                accessibilityNodeInfoCompat2.setSource(view2);
                ViewParent parentForAccessibility = ViewCompat.getParentForAccessibility(view2);
                if (parentForAccessibility instanceof View) {
                    accessibilityNodeInfoCompat2.setParent((View) parentForAccessibility);
                }
                copyNodeInfoNoChildren(accessibilityNodeInfoCompat2, obtain);
                obtain.recycle();
                addChildrenForAccessibility(accessibilityNodeInfoCompat2, (ViewGroup) view2);
            }
            accessibilityNodeInfoCompat2.setClassName(DrawerLayout.class.getName());
            accessibilityNodeInfoCompat2.setFocusable(false);
            accessibilityNodeInfoCompat2.setFocused(false);
            boolean removeAction = accessibilityNodeInfoCompat2.removeAction(AccessibilityNodeInfoCompat.AccessibilityActionCompat.ACTION_FOCUS);
            boolean removeAction2 = accessibilityNodeInfoCompat2.removeAction(AccessibilityNodeInfoCompat.AccessibilityActionCompat.ACTION_CLEAR_FOCUS);
        }

        public void onInitializeAccessibilityEvent(View view, AccessibilityEvent accessibilityEvent) {
            AccessibilityEvent accessibilityEvent2 = accessibilityEvent;
            super.onInitializeAccessibilityEvent(view, accessibilityEvent2);
            accessibilityEvent2.setClassName(DrawerLayout.class.getName());
        }

        public boolean dispatchPopulateAccessibilityEvent(View view, AccessibilityEvent accessibilityEvent) {
            CharSequence drawerTitle;
            View view2 = view;
            AccessibilityEvent accessibilityEvent2 = accessibilityEvent;
            if (accessibilityEvent2.getEventType() != 32) {
                return super.dispatchPopulateAccessibilityEvent(view2, accessibilityEvent2);
            }
            List<CharSequence> text = accessibilityEvent2.getText();
            View access$300 = DrawerLayout.this.findVisibleDrawer();
            if (!(access$300 == null || (drawerTitle = DrawerLayout.this.getDrawerTitle(DrawerLayout.this.getDrawerViewAbsoluteGravity(access$300))) == null)) {
                boolean add = text.add(drawerTitle);
            }
            return true;
        }

        public boolean onRequestSendAccessibilityEvent(ViewGroup viewGroup, View view, AccessibilityEvent accessibilityEvent) {
            ViewGroup viewGroup2 = viewGroup;
            View view2 = view;
            AccessibilityEvent accessibilityEvent2 = accessibilityEvent;
            if (DrawerLayout.CAN_HIDE_DESCENDANTS || DrawerLayout.includeChildForAccessibility(view2)) {
                return super.onRequestSendAccessibilityEvent(viewGroup2, view2, accessibilityEvent2);
            }
            return false;
        }

        private void addChildrenForAccessibility(AccessibilityNodeInfoCompat accessibilityNodeInfoCompat, ViewGroup viewGroup) {
            AccessibilityNodeInfoCompat accessibilityNodeInfoCompat2 = accessibilityNodeInfoCompat;
            ViewGroup viewGroup2 = viewGroup;
            int childCount = viewGroup2.getChildCount();
            for (int i = 0; i < childCount; i++) {
                View childAt = viewGroup2.getChildAt(i);
                if (DrawerLayout.includeChildForAccessibility(childAt)) {
                    accessibilityNodeInfoCompat2.addChild(childAt);
                }
            }
        }

        private void copyNodeInfoNoChildren(AccessibilityNodeInfoCompat accessibilityNodeInfoCompat, AccessibilityNodeInfoCompat accessibilityNodeInfoCompat2) {
            AccessibilityNodeInfoCompat accessibilityNodeInfoCompat3 = accessibilityNodeInfoCompat;
            AccessibilityNodeInfoCompat accessibilityNodeInfoCompat4 = accessibilityNodeInfoCompat2;
            Rect rect = this.mTmpRect;
            accessibilityNodeInfoCompat4.getBoundsInParent(rect);
            accessibilityNodeInfoCompat3.setBoundsInParent(rect);
            accessibilityNodeInfoCompat4.getBoundsInScreen(rect);
            accessibilityNodeInfoCompat3.setBoundsInScreen(rect);
            accessibilityNodeInfoCompat3.setVisibleToUser(accessibilityNodeInfoCompat4.isVisibleToUser());
            accessibilityNodeInfoCompat3.setPackageName(accessibilityNodeInfoCompat4.getPackageName());
            accessibilityNodeInfoCompat3.setClassName(accessibilityNodeInfoCompat4.getClassName());
            accessibilityNodeInfoCompat3.setContentDescription(accessibilityNodeInfoCompat4.getContentDescription());
            accessibilityNodeInfoCompat3.setEnabled(accessibilityNodeInfoCompat4.isEnabled());
            accessibilityNodeInfoCompat3.setClickable(accessibilityNodeInfoCompat4.isClickable());
            accessibilityNodeInfoCompat3.setFocusable(accessibilityNodeInfoCompat4.isFocusable());
            accessibilityNodeInfoCompat3.setFocused(accessibilityNodeInfoCompat4.isFocused());
            accessibilityNodeInfoCompat3.setAccessibilityFocused(accessibilityNodeInfoCompat4.isAccessibilityFocused());
            accessibilityNodeInfoCompat3.setSelected(accessibilityNodeInfoCompat4.isSelected());
            accessibilityNodeInfoCompat3.setLongClickable(accessibilityNodeInfoCompat4.isLongClickable());
            accessibilityNodeInfoCompat3.addAction(accessibilityNodeInfoCompat4.getActions());
        }
    }

    final class ChildAccessibilityDelegate extends AccessibilityDelegateCompat {
        ChildAccessibilityDelegate() {
        }

        public void onInitializeAccessibilityNodeInfo(View view, AccessibilityNodeInfoCompat accessibilityNodeInfoCompat) {
            View view2 = view;
            AccessibilityNodeInfoCompat accessibilityNodeInfoCompat2 = accessibilityNodeInfoCompat;
            super.onInitializeAccessibilityNodeInfo(view2, accessibilityNodeInfoCompat2);
            if (!DrawerLayout.includeChildForAccessibility(view2)) {
                accessibilityNodeInfoCompat2.setParent(null);
            }
        }
    }
}
