package android.support.v4.view.accessibility;

import android.graphics.Rect;
import android.os.Build;

public class AccessibilityWindowInfoCompat {
    private static final AccessibilityWindowInfoImpl IMPL;
    public static final int TYPE_ACCESSIBILITY_OVERLAY = 4;
    public static final int TYPE_APPLICATION = 1;
    public static final int TYPE_INPUT_METHOD = 2;
    public static final int TYPE_SYSTEM = 3;
    private static final int UNDEFINED = -1;
    private Object mInfo;

    private interface AccessibilityWindowInfoImpl {
        void getBoundsInScreen(Object obj, Rect rect);

        Object getChild(Object obj, int i);

        int getChildCount(Object obj);

        int getId(Object obj);

        int getLayer(Object obj);

        Object getParent(Object obj);

        Object getRoot(Object obj);

        int getType(Object obj);

        boolean isAccessibilityFocused(Object obj);

        boolean isActive(Object obj);

        boolean isFocused(Object obj);

        Object obtain();

        Object obtain(Object obj);

        void recycle(Object obj);
    }

    private static class AccessibilityWindowInfoStubImpl implements AccessibilityWindowInfoImpl {
        private AccessibilityWindowInfoStubImpl() {
        }

        public Object obtain() {
            return null;
        }

        public Object obtain(Object obj) {
            return null;
        }

        public int getType(Object obj) {
            return -1;
        }

        public int getLayer(Object obj) {
            return -1;
        }

        public Object getRoot(Object obj) {
            return null;
        }

        public Object getParent(Object obj) {
            return null;
        }

        public int getId(Object obj) {
            return -1;
        }

        public void getBoundsInScreen(Object obj, Rect rect) {
        }

        public boolean isActive(Object obj) {
            return true;
        }

        public boolean isFocused(Object obj) {
            return true;
        }

        public boolean isAccessibilityFocused(Object obj) {
            return true;
        }

        public int getChildCount(Object obj) {
            return 0;
        }

        public Object getChild(Object obj, int i) {
            return null;
        }

        public void recycle(Object obj) {
        }
    }

    private static class AccessibilityWindowInfoApi21Impl extends AccessibilityWindowInfoStubImpl {
        private AccessibilityWindowInfoApi21Impl() {
            super();
        }

        public Object obtain() {
            return AccessibilityWindowInfoCompatApi21.obtain();
        }

        public Object obtain(Object obj) {
            return AccessibilityWindowInfoCompatApi21.obtain(obj);
        }

        public int getType(Object obj) {
            return AccessibilityWindowInfoCompatApi21.getType(obj);
        }

        public int getLayer(Object obj) {
            return AccessibilityWindowInfoCompatApi21.getLayer(obj);
        }

        public Object getRoot(Object obj) {
            return AccessibilityWindowInfoCompatApi21.getRoot(obj);
        }

        public Object getParent(Object obj) {
            return AccessibilityWindowInfoCompatApi21.getParent(obj);
        }

        public int getId(Object obj) {
            return AccessibilityWindowInfoCompatApi21.getId(obj);
        }

        public void getBoundsInScreen(Object obj, Rect rect) {
            AccessibilityWindowInfoCompatApi21.getBoundsInScreen(obj, rect);
        }

        public boolean isActive(Object obj) {
            return AccessibilityWindowInfoCompatApi21.isActive(obj);
        }

        public boolean isFocused(Object obj) {
            return AccessibilityWindowInfoCompatApi21.isFocused(obj);
        }

        public boolean isAccessibilityFocused(Object obj) {
            return AccessibilityWindowInfoCompatApi21.isAccessibilityFocused(obj);
        }

        public int getChildCount(Object obj) {
            return AccessibilityWindowInfoCompatApi21.getChildCount(obj);
        }

        public Object getChild(Object obj, int i) {
            return AccessibilityWindowInfoCompatApi21.getChild(obj, i);
        }

        public void recycle(Object obj) {
            AccessibilityWindowInfoCompatApi21.recycle(obj);
        }
    }

    static {
        AccessibilityWindowInfoImpl accessibilityWindowInfoImpl;
        AccessibilityWindowInfoImpl accessibilityWindowInfoImpl2;
        if (Build.VERSION.SDK_INT >= 21) {
            new AccessibilityWindowInfoApi21Impl();
            IMPL = accessibilityWindowInfoImpl2;
            return;
        }
        new AccessibilityWindowInfoStubImpl();
        IMPL = accessibilityWindowInfoImpl;
    }

    static AccessibilityWindowInfoCompat wrapNonNullInstance(Object obj) {
        AccessibilityWindowInfoCompat accessibilityWindowInfoCompat;
        Object obj2 = obj;
        if (obj2 == null) {
            return null;
        }
        new AccessibilityWindowInfoCompat(obj2);
        return accessibilityWindowInfoCompat;
    }

    private AccessibilityWindowInfoCompat(Object obj) {
        this.mInfo = obj;
    }

    public int getType() {
        return IMPL.getType(this.mInfo);
    }

    public int getLayer() {
        return IMPL.getLayer(this.mInfo);
    }

    public AccessibilityNodeInfoCompat getRoot() {
        return AccessibilityNodeInfoCompat.wrapNonNullInstance(IMPL.getRoot(this.mInfo));
    }

    public AccessibilityWindowInfoCompat getParent() {
        return wrapNonNullInstance(IMPL.getParent(this.mInfo));
    }

    public int getId() {
        return IMPL.getId(this.mInfo);
    }

    public void getBoundsInScreen(Rect rect) {
        IMPL.getBoundsInScreen(this.mInfo, rect);
    }

    public boolean isActive() {
        return IMPL.isActive(this.mInfo);
    }

    public boolean isFocused() {
        return IMPL.isFocused(this.mInfo);
    }

    public boolean isAccessibilityFocused() {
        return IMPL.isAccessibilityFocused(this.mInfo);
    }

    public int getChildCount() {
        return IMPL.getChildCount(this.mInfo);
    }

    public AccessibilityWindowInfoCompat getChild(int i) {
        return wrapNonNullInstance(IMPL.getChild(this.mInfo, i));
    }

    public static AccessibilityWindowInfoCompat obtain() {
        return wrapNonNullInstance(IMPL.obtain());
    }

    public static AccessibilityWindowInfoCompat obtain(AccessibilityWindowInfoCompat accessibilityWindowInfoCompat) {
        return wrapNonNullInstance(IMPL.obtain(accessibilityWindowInfoCompat.mInfo));
    }

    public void recycle() {
        IMPL.recycle(this.mInfo);
    }

    public int hashCode() {
        return this.mInfo == null ? 0 : this.mInfo.hashCode();
    }

    /* JADX WARN: Type inference failed for: r6v0, types: [java.lang.Object] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean equals(java.lang.Object r6) {
        /*
            r5 = this;
            r0 = r5
            r1 = r6
            r3 = r0
            r4 = r1
            if (r3 != r4) goto L_0x0009
            r3 = 1
            r0 = r3
        L_0x0008:
            return r0
        L_0x0009:
            r3 = r1
            if (r3 != 0) goto L_0x000f
            r3 = 0
            r0 = r3
            goto L_0x0008
        L_0x000f:
            r3 = r0
            java.lang.Class r3 = r3.getClass()
            r4 = r1
            java.lang.Class r4 = r4.getClass()
            if (r3 == r4) goto L_0x001e
            r3 = 0
            r0 = r3
            goto L_0x0008
        L_0x001e:
            r3 = r1
            android.support.v4.view.accessibility.AccessibilityWindowInfoCompat r3 = (android.support.v4.view.accessibility.AccessibilityWindowInfoCompat) r3
            r2 = r3
            r3 = r0
            java.lang.Object r3 = r3.mInfo
            if (r3 != 0) goto L_0x002f
            r3 = r2
            java.lang.Object r3 = r3.mInfo
            if (r3 == 0) goto L_0x003e
            r3 = 0
            r0 = r3
            goto L_0x0008
        L_0x002f:
            r3 = r0
            java.lang.Object r3 = r3.mInfo
            r4 = r2
            java.lang.Object r4 = r4.mInfo
            boolean r3 = r3.equals(r4)
            if (r3 != 0) goto L_0x003e
            r3 = 0
            r0 = r3
            goto L_0x0008
        L_0x003e:
            r3 = 1
            r0 = r3
            goto L_0x0008
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v4.view.accessibility.AccessibilityWindowInfoCompat.equals(java.lang.Object):boolean");
    }

    public String toString() {
        StringBuilder sb;
        Rect rect;
        new StringBuilder();
        StringBuilder sb2 = sb;
        new Rect();
        Rect rect2 = rect;
        getBoundsInScreen(rect2);
        StringBuilder append = sb2.append("AccessibilityWindowInfo[");
        StringBuilder append2 = sb2.append("id=").append(getId());
        StringBuilder append3 = sb2.append(", type=").append(typeToString(getType()));
        StringBuilder append4 = sb2.append(", layer=").append(getLayer());
        StringBuilder append5 = sb2.append(", bounds=").append(rect2);
        StringBuilder append6 = sb2.append(", focused=").append(isFocused());
        StringBuilder append7 = sb2.append(", active=").append(isActive());
        StringBuilder append8 = sb2.append(", hasParent=").append(getParent() != null);
        StringBuilder append9 = sb2.append(", hasChildren=").append(getChildCount() > 0);
        StringBuilder append10 = sb2.append(']');
        return sb2.toString();
    }

    private static String typeToString(int i) {
        switch (i) {
            case 1:
                return "TYPE_APPLICATION";
            case 2:
                return "TYPE_INPUT_METHOD";
            case 3:
                return "TYPE_SYSTEM";
            case 4:
                return "TYPE_ACCESSIBILITY_OVERLAY";
            default:
                return "<UNKNOWN>";
        }
    }
}
