package android.support.v4.view;

import android.content.Context;
import android.database.DataSetObserver;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.ColorInt;
import android.support.annotation.FloatRange;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.TextView;
import java.lang.ref.WeakReference;

public class PagerTitleStrip extends ViewGroup implements ViewPager.Decor {
    private static final int[] ATTRS = {16842804, 16842901, 16842904, 16842927};
    private static final PagerTitleStripImpl IMPL;
    private static final float SIDE_ALPHA = 0.6f;
    private static final String TAG = "PagerTitleStrip";
    private static final int[] TEXT_ATTRS = {16843660};
    private static final int TEXT_SPACING = 16;
    TextView mCurrText;
    private int mGravity;
    private int mLastKnownCurrentPage;
    /* access modifiers changed from: private */
    public float mLastKnownPositionOffset;
    TextView mNextText;
    private int mNonPrimaryAlpha;
    private final PageListener mPageListener;
    ViewPager mPager;
    TextView mPrevText;
    private int mScaledTextSpacing;
    int mTextColor;
    private boolean mUpdatingPositions;
    private boolean mUpdatingText;
    private WeakReference<PagerAdapter> mWatchingAdapter;

    interface PagerTitleStripImpl {
        void setSingleLineAllCaps(TextView textView);
    }

    static {
        PagerTitleStripImpl pagerTitleStripImpl;
        PagerTitleStripImpl pagerTitleStripImpl2;
        if (Build.VERSION.SDK_INT >= 14) {
            new PagerTitleStripImplIcs();
            IMPL = pagerTitleStripImpl2;
            return;
        }
        new PagerTitleStripImplBase();
        IMPL = pagerTitleStripImpl;
    }

    static class PagerTitleStripImplBase implements PagerTitleStripImpl {
        PagerTitleStripImplBase() {
        }

        public void setSingleLineAllCaps(TextView textView) {
            textView.setSingleLine();
        }
    }

    static class PagerTitleStripImplIcs implements PagerTitleStripImpl {
        PagerTitleStripImplIcs() {
        }

        public void setSingleLineAllCaps(TextView textView) {
            PagerTitleStripIcs.setSingleLineAllCaps(textView);
        }
    }

    private static void setSingleLineAllCaps(TextView textView) {
        IMPL.setSingleLineAllCaps(textView);
    }

    public PagerTitleStrip(Context context) {
        this(context, null);
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public PagerTitleStrip(android.content.Context r16, android.util.AttributeSet r17) {
        /*
            r15 = this;
            r0 = r15
            r1 = r16
            r2 = r17
            r8 = r0
            r9 = r1
            r10 = r2
            r8.<init>(r9, r10)
            r8 = r0
            r9 = -1
            r8.mLastKnownCurrentPage = r9
            r8 = r0
            r9 = -1082130432(0xffffffffbf800000, float:-1.0)
            r8.mLastKnownPositionOffset = r9
            r8 = r0
            android.support.v4.view.PagerTitleStrip$PageListener r9 = new android.support.v4.view.PagerTitleStrip$PageListener
            r13 = r9
            r9 = r13
            r10 = r13
            r11 = r0
            r12 = 0
            r10.<init>()
            r8.mPageListener = r9
            r8 = r0
            r9 = r0
            android.widget.TextView r10 = new android.widget.TextView
            r13 = r10
            r10 = r13
            r11 = r13
            r12 = r1
            r11.<init>(r12)
            r13 = r9
            r14 = r10
            r9 = r14
            r10 = r13
            r11 = r14
            r10.mPrevText = r11
            r8.addView(r9)
            r8 = r0
            r9 = r0
            android.widget.TextView r10 = new android.widget.TextView
            r13 = r10
            r10 = r13
            r11 = r13
            r12 = r1
            r11.<init>(r12)
            r13 = r9
            r14 = r10
            r9 = r14
            r10 = r13
            r11 = r14
            r10.mCurrText = r11
            r8.addView(r9)
            r8 = r0
            r9 = r0
            android.widget.TextView r10 = new android.widget.TextView
            r13 = r10
            r10 = r13
            r11 = r13
            r12 = r1
            r11.<init>(r12)
            r13 = r9
            r14 = r10
            r9 = r14
            r10 = r13
            r11 = r14
            r10.mNextText = r11
            r8.addView(r9)
            r8 = r1
            r9 = r2
            int[] r10 = android.support.v4.view.PagerTitleStrip.ATTRS
            android.content.res.TypedArray r8 = r8.obtainStyledAttributes(r9, r10)
            r3 = r8
            r8 = r3
            r9 = 0
            r10 = 0
            int r8 = r8.getResourceId(r9, r10)
            r4 = r8
            r8 = r4
            if (r8 == 0) goto L_0x008c
            r8 = r0
            android.widget.TextView r8 = r8.mPrevText
            r9 = r1
            r10 = r4
            r8.setTextAppearance(r9, r10)
            r8 = r0
            android.widget.TextView r8 = r8.mCurrText
            r9 = r1
            r10 = r4
            r8.setTextAppearance(r9, r10)
            r8 = r0
            android.widget.TextView r8 = r8.mNextText
            r9 = r1
            r10 = r4
            r8.setTextAppearance(r9, r10)
        L_0x008c:
            r8 = r3
            r9 = 1
            r10 = 0
            int r8 = r8.getDimensionPixelSize(r9, r10)
            r5 = r8
            r8 = r5
            if (r8 == 0) goto L_0x009e
            r8 = r0
            r9 = 0
            r10 = r5
            float r10 = (float) r10
            r8.setTextSize(r9, r10)
        L_0x009e:
            r8 = r3
            r9 = 2
            boolean r8 = r8.hasValue(r9)
            if (r8 == 0) goto L_0x00c3
            r8 = r3
            r9 = 2
            r10 = 0
            int r8 = r8.getColor(r9, r10)
            r6 = r8
            r8 = r0
            android.widget.TextView r8 = r8.mPrevText
            r9 = r6
            r8.setTextColor(r9)
            r8 = r0
            android.widget.TextView r8 = r8.mCurrText
            r9 = r6
            r8.setTextColor(r9)
            r8 = r0
            android.widget.TextView r8 = r8.mNextText
            r9 = r6
            r8.setTextColor(r9)
        L_0x00c3:
            r8 = r0
            r9 = r3
            r10 = 3
            r11 = 80
            int r9 = r9.getInteger(r10, r11)
            r8.mGravity = r9
            r8 = r3
            r8.recycle()
            r8 = r0
            r9 = r0
            android.widget.TextView r9 = r9.mCurrText
            android.content.res.ColorStateList r9 = r9.getTextColors()
            int r9 = r9.getDefaultColor()
            r8.mTextColor = r9
            r8 = r0
            r9 = 1058642330(0x3f19999a, float:0.6)
            r8.setNonPrimaryAlpha(r9)
            r8 = r0
            android.widget.TextView r8 = r8.mPrevText
            android.text.TextUtils$TruncateAt r9 = android.text.TextUtils.TruncateAt.END
            r8.setEllipsize(r9)
            r8 = r0
            android.widget.TextView r8 = r8.mCurrText
            android.text.TextUtils$TruncateAt r9 = android.text.TextUtils.TruncateAt.END
            r8.setEllipsize(r9)
            r8 = r0
            android.widget.TextView r8 = r8.mNextText
            android.text.TextUtils$TruncateAt r9 = android.text.TextUtils.TruncateAt.END
            r8.setEllipsize(r9)
            r8 = 0
            r6 = r8
            r8 = r4
            if (r8 == 0) goto L_0x0119
            r8 = r1
            r9 = r4
            int[] r10 = android.support.v4.view.PagerTitleStrip.TEXT_ATTRS
            android.content.res.TypedArray r8 = r8.obtainStyledAttributes(r9, r10)
            r7 = r8
            r8 = r7
            r9 = 0
            r10 = 0
            boolean r8 = r8.getBoolean(r9, r10)
            r6 = r8
            r8 = r7
            r8.recycle()
        L_0x0119:
            r8 = r6
            if (r8 == 0) goto L_0x0143
            r8 = r0
            android.widget.TextView r8 = r8.mPrevText
            setSingleLineAllCaps(r8)
            r8 = r0
            android.widget.TextView r8 = r8.mCurrText
            setSingleLineAllCaps(r8)
            r8 = r0
            android.widget.TextView r8 = r8.mNextText
            setSingleLineAllCaps(r8)
        L_0x012e:
            r8 = r1
            android.content.res.Resources r8 = r8.getResources()
            android.util.DisplayMetrics r8 = r8.getDisplayMetrics()
            float r8 = r8.density
            r7 = r8
            r8 = r0
            r9 = 1098907648(0x41800000, float:16.0)
            r10 = r7
            float r9 = r9 * r10
            int r9 = (int) r9
            r8.mScaledTextSpacing = r9
            return
        L_0x0143:
            r8 = r0
            android.widget.TextView r8 = r8.mPrevText
            r8.setSingleLine()
            r8 = r0
            android.widget.TextView r8 = r8.mCurrText
            r8.setSingleLine()
            r8 = r0
            android.widget.TextView r8 = r8.mNextText
            r8.setSingleLine()
            goto L_0x012e
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v4.view.PagerTitleStrip.<init>(android.content.Context, android.util.AttributeSet):void");
    }

    public void setTextSpacing(int i) {
        this.mScaledTextSpacing = i;
        requestLayout();
    }

    public int getTextSpacing() {
        return this.mScaledTextSpacing;
    }

    public void setNonPrimaryAlpha(@FloatRange(from = 0.0d, to = 1.0d) float f) {
        this.mNonPrimaryAlpha = ((int) (f * 255.0f)) & 255;
        int i = (this.mNonPrimaryAlpha << 24) | (this.mTextColor & ViewCompat.MEASURED_SIZE_MASK);
        this.mPrevText.setTextColor(i);
        this.mNextText.setTextColor(i);
    }

    public void setTextColor(@ColorInt int i) {
        int i2 = i;
        this.mTextColor = i2;
        this.mCurrText.setTextColor(i2);
        int i3 = (this.mNonPrimaryAlpha << 24) | (this.mTextColor & ViewCompat.MEASURED_SIZE_MASK);
        this.mPrevText.setTextColor(i3);
        this.mNextText.setTextColor(i3);
    }

    public void setTextSize(int i, float f) {
        int i2 = i;
        float f2 = f;
        this.mPrevText.setTextSize(i2, f2);
        this.mCurrText.setTextSize(i2, f2);
        this.mNextText.setTextSize(i2, f2);
    }

    public void setGravity(int i) {
        this.mGravity = i;
        requestLayout();
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        Throwable th;
        super.onAttachedToWindow();
        ViewParent parent = getParent();
        if (!(parent instanceof ViewPager)) {
            Throwable th2 = th;
            new IllegalStateException("PagerTitleStrip must be a direct child of a ViewPager.");
            throw th2;
        }
        ViewPager viewPager = (ViewPager) parent;
        PagerAdapter adapter = viewPager.getAdapter();
        ViewPager.OnPageChangeListener internalPageChangeListener = viewPager.setInternalPageChangeListener(this.mPageListener);
        viewPager.setOnAdapterChangeListener(this.mPageListener);
        this.mPager = viewPager;
        updateAdapter(this.mWatchingAdapter != null ? this.mWatchingAdapter.get() : null, adapter);
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (this.mPager != null) {
            updateAdapter(this.mPager.getAdapter(), null);
            ViewPager.OnPageChangeListener internalPageChangeListener = this.mPager.setInternalPageChangeListener(null);
            this.mPager.setOnAdapterChangeListener(null);
            this.mPager = null;
        }
    }

    /* access modifiers changed from: package-private */
    public void updateText(int i, PagerAdapter pagerAdapter) {
        int i2 = i;
        PagerAdapter pagerAdapter2 = pagerAdapter;
        int count = pagerAdapter2 != null ? pagerAdapter2.getCount() : 0;
        this.mUpdatingText = true;
        CharSequence charSequence = null;
        if (i2 >= 1 && pagerAdapter2 != null) {
            charSequence = pagerAdapter2.getPageTitle(i2 - 1);
        }
        this.mPrevText.setText(charSequence);
        this.mCurrText.setText((pagerAdapter2 == null || i2 >= count) ? null : pagerAdapter2.getPageTitle(i2));
        CharSequence charSequence2 = null;
        if (i2 + 1 < count && pagerAdapter2 != null) {
            charSequence2 = pagerAdapter2.getPageTitle(i2 + 1);
        }
        this.mNextText.setText(charSequence2);
        int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(Math.max(0, (int) (((float) ((getWidth() - getPaddingLeft()) - getPaddingRight())) * 0.8f)), Integer.MIN_VALUE);
        int makeMeasureSpec2 = View.MeasureSpec.makeMeasureSpec(Math.max(0, (getHeight() - getPaddingTop()) - getPaddingBottom()), Integer.MIN_VALUE);
        this.mPrevText.measure(makeMeasureSpec, makeMeasureSpec2);
        this.mCurrText.measure(makeMeasureSpec, makeMeasureSpec2);
        this.mNextText.measure(makeMeasureSpec, makeMeasureSpec2);
        this.mLastKnownCurrentPage = i2;
        if (!this.mUpdatingPositions) {
            updateTextPositions(i2, this.mLastKnownPositionOffset, false);
        }
        this.mUpdatingText = false;
    }

    public void requestLayout() {
        if (!this.mUpdatingText) {
            super.requestLayout();
        }
    }

    /* access modifiers changed from: package-private */
    public void updateAdapter(PagerAdapter pagerAdapter, PagerAdapter pagerAdapter2) {
        WeakReference<PagerAdapter> weakReference;
        PagerAdapter pagerAdapter3 = pagerAdapter;
        PagerAdapter pagerAdapter4 = pagerAdapter2;
        if (pagerAdapter3 != null) {
            pagerAdapter3.unregisterDataSetObserver(this.mPageListener);
            this.mWatchingAdapter = null;
        }
        if (pagerAdapter4 != null) {
            pagerAdapter4.registerDataSetObserver(this.mPageListener);
            new WeakReference<>(pagerAdapter4);
            this.mWatchingAdapter = weakReference;
        }
        if (this.mPager != null) {
            this.mLastKnownCurrentPage = -1;
            this.mLastKnownPositionOffset = -1.0f;
            updateText(this.mPager.getCurrentItem(), pagerAdapter4);
            requestLayout();
        }
    }

    /* access modifiers changed from: package-private */
    public void updateTextPositions(int i, float f, boolean z) {
        int i2;
        int i3;
        int i4;
        int i5 = i;
        float f2 = f;
        boolean z2 = z;
        if (i5 != this.mLastKnownCurrentPage) {
            updateText(i5, this.mPager.getAdapter());
        } else if (!z2 && f2 == this.mLastKnownPositionOffset) {
            return;
        }
        this.mUpdatingPositions = true;
        int measuredWidth = this.mPrevText.getMeasuredWidth();
        int measuredWidth2 = this.mCurrText.getMeasuredWidth();
        int measuredWidth3 = this.mNextText.getMeasuredWidth();
        int i6 = measuredWidth2 / 2;
        int width = getWidth();
        int height = getHeight();
        int paddingLeft = getPaddingLeft();
        int paddingRight = getPaddingRight();
        int paddingTop = getPaddingTop();
        int paddingBottom = getPaddingBottom();
        int i7 = paddingRight + i6;
        int i8 = (width - (paddingLeft + i6)) - i7;
        float f3 = f2 + 0.5f;
        if (f3 > 1.0f) {
            f3 -= 1.0f;
        }
        int i9 = ((width - i7) - ((int) (((float) i8) * f3))) - (measuredWidth2 / 2);
        int i10 = i9 + measuredWidth2;
        int baseline = this.mPrevText.getBaseline();
        int baseline2 = this.mCurrText.getBaseline();
        int baseline3 = this.mNextText.getBaseline();
        int max = Math.max(Math.max(baseline, baseline2), baseline3);
        int i11 = max - baseline;
        int i12 = max - baseline2;
        int i13 = max - baseline3;
        int measuredHeight = i11 + this.mPrevText.getMeasuredHeight();
        int measuredHeight2 = i12 + this.mCurrText.getMeasuredHeight();
        int max2 = Math.max(Math.max(measuredHeight, measuredHeight2), i13 + this.mNextText.getMeasuredHeight());
        switch (this.mGravity & 112) {
            case 16:
                int i14 = (((height - paddingTop) - paddingBottom) - max2) / 2;
                i2 = i14 + i11;
                i3 = i14 + i12;
                i4 = i14 + i13;
                break;
            case 80:
                int i15 = (height - paddingBottom) - max2;
                i2 = i15 + i11;
                i3 = i15 + i12;
                i4 = i15 + i13;
                break;
            default:
                i2 = paddingTop + i11;
                i3 = paddingTop + i12;
                i4 = paddingTop + i13;
                break;
        }
        this.mCurrText.layout(i9, i3, i10, i3 + this.mCurrText.getMeasuredHeight());
        int min = Math.min(paddingLeft, (i9 - this.mScaledTextSpacing) - measuredWidth);
        this.mPrevText.layout(min, i2, min + measuredWidth, i2 + this.mPrevText.getMeasuredHeight());
        int max3 = Math.max((width - paddingRight) - measuredWidth3, i10 + this.mScaledTextSpacing);
        this.mNextText.layout(max3, i4, max3 + measuredWidth3, i4 + this.mNextText.getMeasuredHeight());
        this.mLastKnownPositionOffset = f2;
        this.mUpdatingPositions = false;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        int max;
        Throwable th;
        int i3 = i;
        int i4 = i2;
        if (View.MeasureSpec.getMode(i3) != 1073741824) {
            Throwable th2 = th;
            new IllegalStateException("Must measure with an exact width");
            throw th2;
        }
        int paddingTop = getPaddingTop() + getPaddingBottom();
        int childMeasureSpec = getChildMeasureSpec(i4, paddingTop, -2);
        int size = View.MeasureSpec.getSize(i3);
        int childMeasureSpec2 = getChildMeasureSpec(i3, (int) (((float) size) * 0.2f), -2);
        this.mPrevText.measure(childMeasureSpec2, childMeasureSpec);
        this.mCurrText.measure(childMeasureSpec2, childMeasureSpec);
        this.mNextText.measure(childMeasureSpec2, childMeasureSpec);
        if (View.MeasureSpec.getMode(i4) == 1073741824) {
            max = View.MeasureSpec.getSize(i4);
        } else {
            max = Math.max(getMinHeight(), this.mCurrText.getMeasuredHeight() + paddingTop);
        }
        setMeasuredDimension(size, ViewCompat.resolveSizeAndState(max, i4, ViewCompat.getMeasuredState(this.mCurrText) << 16));
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        if (this.mPager != null) {
            updateTextPositions(this.mLastKnownCurrentPage, this.mLastKnownPositionOffset >= 0.0f ? this.mLastKnownPositionOffset : 0.0f, true);
        }
    }

    /* access modifiers changed from: package-private */
    public int getMinHeight() {
        int i = 0;
        Drawable background = getBackground();
        if (background != null) {
            i = background.getIntrinsicHeight();
        }
        return i;
    }

    private class PageListener extends DataSetObserver implements ViewPager.OnPageChangeListener, ViewPager.OnAdapterChangeListener {
        private int mScrollState;

        private PageListener() {
        }

        public void onPageScrolled(int i, float f, int i2) {
            int i3 = i;
            float f2 = f;
            if (f2 > 0.5f) {
                i3++;
            }
            PagerTitleStrip.this.updateTextPositions(i3, f2, false);
        }

        public void onPageSelected(int i) {
            if (this.mScrollState == 0) {
                PagerTitleStrip.this.updateText(PagerTitleStrip.this.mPager.getCurrentItem(), PagerTitleStrip.this.mPager.getAdapter());
                PagerTitleStrip.this.updateTextPositions(PagerTitleStrip.this.mPager.getCurrentItem(), PagerTitleStrip.this.mLastKnownPositionOffset >= 0.0f ? PagerTitleStrip.this.mLastKnownPositionOffset : 0.0f, true);
            }
        }

        public void onPageScrollStateChanged(int i) {
            this.mScrollState = i;
        }

        public void onAdapterChanged(PagerAdapter pagerAdapter, PagerAdapter pagerAdapter2) {
            PagerTitleStrip.this.updateAdapter(pagerAdapter, pagerAdapter2);
        }

        public void onChanged() {
            PagerTitleStrip.this.updateText(PagerTitleStrip.this.mPager.getCurrentItem(), PagerTitleStrip.this.mPager.getAdapter());
            PagerTitleStrip.this.updateTextPositions(PagerTitleStrip.this.mPager.getCurrentItem(), PagerTitleStrip.this.mLastKnownPositionOffset >= 0.0f ? PagerTitleStrip.this.mLastKnownPositionOffset : 0.0f, true);
        }
    }
}
