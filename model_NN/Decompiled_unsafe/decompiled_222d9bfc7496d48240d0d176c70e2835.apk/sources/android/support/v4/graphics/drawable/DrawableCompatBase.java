package android.support.v4.graphics.drawable;

import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;

class DrawableCompatBase {
    DrawableCompatBase() {
    }

    public static void setTint(Drawable drawable, int i) {
        Drawable drawable2 = drawable;
        int i2 = i;
        if (drawable2 instanceof DrawableWrapper) {
            ((DrawableWrapper) drawable2).setTint(i2);
        }
    }

    public static void setTintList(Drawable drawable, ColorStateList colorStateList) {
        Drawable drawable2 = drawable;
        ColorStateList colorStateList2 = colorStateList;
        if (drawable2 instanceof DrawableWrapper) {
            ((DrawableWrapper) drawable2).setTintList(colorStateList2);
        }
    }

    public static void setTintMode(Drawable drawable, PorterDuff.Mode mode) {
        Drawable drawable2 = drawable;
        PorterDuff.Mode mode2 = mode;
        if (drawable2 instanceof DrawableWrapper) {
            ((DrawableWrapper) drawable2).setTintMode(mode2);
        }
    }

    public static Drawable wrapForTinting(Drawable drawable) {
        Drawable drawable2;
        Drawable drawable3 = drawable;
        if (drawable3 instanceof DrawableWrapperDonut) {
            return drawable3;
        }
        new DrawableWrapperDonut(drawable3);
        return drawable2;
    }
}
