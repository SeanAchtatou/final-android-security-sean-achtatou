package android.support.v4.graphics.drawable;

import android.graphics.drawable.Drawable;

class DrawableCompatKitKat {
    DrawableCompatKitKat() {
    }

    public static void setAutoMirrored(Drawable drawable, boolean z) {
        drawable.setAutoMirrored(z);
    }

    public static boolean isAutoMirrored(Drawable drawable) {
        return drawable.isAutoMirrored();
    }

    public static Drawable wrapForTinting(Drawable drawable) {
        Drawable drawable2;
        Drawable drawable3 = drawable;
        if (drawable3 instanceof DrawableWrapperKitKat) {
            return drawable3;
        }
        new DrawableWrapperKitKat(drawable3);
        return drawable2;
    }
}
