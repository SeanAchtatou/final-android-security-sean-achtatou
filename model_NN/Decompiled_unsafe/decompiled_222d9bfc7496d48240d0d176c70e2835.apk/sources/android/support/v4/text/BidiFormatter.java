package android.support.v4.text;

import java.util.Locale;

public final class BidiFormatter {
    private static final int DEFAULT_FLAGS = 2;
    /* access modifiers changed from: private */
    public static final BidiFormatter DEFAULT_LTR_INSTANCE;
    /* access modifiers changed from: private */
    public static final BidiFormatter DEFAULT_RTL_INSTANCE;
    /* access modifiers changed from: private */
    public static TextDirectionHeuristicCompat DEFAULT_TEXT_DIRECTION_HEURISTIC = TextDirectionHeuristicsCompat.FIRSTSTRONG_LTR;
    private static final int DIR_LTR = -1;
    private static final int DIR_RTL = 1;
    private static final int DIR_UNKNOWN = 0;
    private static final String EMPTY_STRING = "";
    private static final int FLAG_STEREO_RESET = 2;
    private static final char LRE = '‪';
    private static final char LRM = '‎';
    private static final String LRM_STRING = Character.toString(LRM);
    private static final char PDF = '‬';
    private static final char RLE = '‫';
    private static final char RLM = '‏';
    private static final String RLM_STRING = Character.toString(RLM);
    private final TextDirectionHeuristicCompat mDefaultTextDirectionHeuristicCompat;
    private final int mFlags;
    private final boolean mIsRtlContext;

    static {
        BidiFormatter bidiFormatter;
        BidiFormatter bidiFormatter2;
        new BidiFormatter(false, 2, DEFAULT_TEXT_DIRECTION_HEURISTIC);
        DEFAULT_LTR_INSTANCE = bidiFormatter;
        new BidiFormatter(true, 2, DEFAULT_TEXT_DIRECTION_HEURISTIC);
        DEFAULT_RTL_INSTANCE = bidiFormatter2;
    }

    public static final class Builder {
        private int mFlags;
        private boolean mIsRtlContext;
        private TextDirectionHeuristicCompat mTextDirectionHeuristicCompat;

        public Builder() {
            initialize(BidiFormatter.isRtlLocale(Locale.getDefault()));
        }

        public Builder(boolean z) {
            initialize(z);
        }

        public Builder(Locale locale) {
            initialize(BidiFormatter.isRtlLocale(locale));
        }

        private void initialize(boolean z) {
            this.mIsRtlContext = z;
            this.mTextDirectionHeuristicCompat = BidiFormatter.DEFAULT_TEXT_DIRECTION_HEURISTIC;
            this.mFlags = 2;
        }

        public Builder stereoReset(boolean z) {
            if (z) {
                this.mFlags = this.mFlags | 2;
            } else {
                this.mFlags = this.mFlags & -3;
            }
            return this;
        }

        public Builder setTextDirectionHeuristic(TextDirectionHeuristicCompat textDirectionHeuristicCompat) {
            this.mTextDirectionHeuristicCompat = textDirectionHeuristicCompat;
            return this;
        }

        private static BidiFormatter getDefaultInstanceFromContext(boolean z) {
            return z ? BidiFormatter.DEFAULT_RTL_INSTANCE : BidiFormatter.DEFAULT_LTR_INSTANCE;
        }

        public BidiFormatter build() {
            BidiFormatter bidiFormatter;
            if (this.mFlags == 2 && this.mTextDirectionHeuristicCompat == BidiFormatter.DEFAULT_TEXT_DIRECTION_HEURISTIC) {
                return getDefaultInstanceFromContext(this.mIsRtlContext);
            }
            new BidiFormatter(this.mIsRtlContext, this.mFlags, this.mTextDirectionHeuristicCompat);
            return bidiFormatter;
        }
    }

    public static BidiFormatter getInstance() {
        Builder builder;
        new Builder();
        return builder.build();
    }

    public static BidiFormatter getInstance(boolean z) {
        Builder builder;
        new Builder(z);
        return builder.build();
    }

    public static BidiFormatter getInstance(Locale locale) {
        Builder builder;
        new Builder(locale);
        return builder.build();
    }

    private BidiFormatter(boolean z, int i, TextDirectionHeuristicCompat textDirectionHeuristicCompat) {
        this.mIsRtlContext = z;
        this.mFlags = i;
        this.mDefaultTextDirectionHeuristicCompat = textDirectionHeuristicCompat;
    }

    public boolean isRtlContext() {
        return this.mIsRtlContext;
    }

    public boolean getStereoReset() {
        return (this.mFlags & 2) != 0;
    }

    private String markAfter(String str, TextDirectionHeuristicCompat textDirectionHeuristicCompat) {
        String str2 = str;
        boolean isRtl = textDirectionHeuristicCompat.isRtl(str2, 0, str2.length());
        if (!this.mIsRtlContext && (isRtl || getExitDir(str2) == 1)) {
            return LRM_STRING;
        }
        if (!this.mIsRtlContext || (isRtl && getExitDir(str2) != -1)) {
            return "";
        }
        return RLM_STRING;
    }

    private String markBefore(String str, TextDirectionHeuristicCompat textDirectionHeuristicCompat) {
        String str2 = str;
        boolean isRtl = textDirectionHeuristicCompat.isRtl(str2, 0, str2.length());
        if (!this.mIsRtlContext && (isRtl || getEntryDir(str2) == 1)) {
            return LRM_STRING;
        }
        if (!this.mIsRtlContext || (isRtl && getEntryDir(str2) != -1)) {
            return "";
        }
        return RLM_STRING;
    }

    public boolean isRtl(String str) {
        String str2 = str;
        return this.mDefaultTextDirectionHeuristicCompat.isRtl(str2, 0, str2.length());
    }

    public String unicodeWrap(String str, TextDirectionHeuristicCompat textDirectionHeuristicCompat, boolean z) {
        StringBuilder sb;
        String str2 = str;
        TextDirectionHeuristicCompat textDirectionHeuristicCompat2 = textDirectionHeuristicCompat;
        boolean z2 = z;
        if (str2 == null) {
            return null;
        }
        boolean isRtl = textDirectionHeuristicCompat2.isRtl(str2, 0, str2.length());
        new StringBuilder();
        StringBuilder sb2 = sb;
        if (getStereoReset() && z2) {
            StringBuilder append = sb2.append(markBefore(str2, isRtl ? TextDirectionHeuristicsCompat.RTL : TextDirectionHeuristicsCompat.LTR));
        }
        if (isRtl != this.mIsRtlContext) {
            StringBuilder append2 = sb2.append(isRtl ? RLE : LRE);
            StringBuilder append3 = sb2.append(str2);
            StringBuilder append4 = sb2.append((char) PDF);
        } else {
            StringBuilder append5 = sb2.append(str2);
        }
        if (z2) {
            StringBuilder append6 = sb2.append(markAfter(str2, isRtl ? TextDirectionHeuristicsCompat.RTL : TextDirectionHeuristicsCompat.LTR));
        }
        return sb2.toString();
    }

    public String unicodeWrap(String str, TextDirectionHeuristicCompat textDirectionHeuristicCompat) {
        return unicodeWrap(str, textDirectionHeuristicCompat, true);
    }

    public String unicodeWrap(String str, boolean z) {
        return unicodeWrap(str, this.mDefaultTextDirectionHeuristicCompat, z);
    }

    public String unicodeWrap(String str) {
        return unicodeWrap(str, this.mDefaultTextDirectionHeuristicCompat, true);
    }

    /* access modifiers changed from: private */
    public static boolean isRtlLocale(Locale locale) {
        return TextUtilsCompat.getLayoutDirectionFromLocale(locale) == 1;
    }

    private static int getExitDir(String str) {
        DirectionalityEstimator directionalityEstimator;
        new DirectionalityEstimator(str, false);
        return directionalityEstimator.getExitDir();
    }

    private static int getEntryDir(String str) {
        DirectionalityEstimator directionalityEstimator;
        new DirectionalityEstimator(str, false);
        return directionalityEstimator.getEntryDir();
    }

    private static class DirectionalityEstimator {
        private static final byte[] DIR_TYPE_CACHE = new byte[DIR_TYPE_CACHE_SIZE];
        private static final int DIR_TYPE_CACHE_SIZE = 1792;
        private int charIndex;
        private final boolean isHtml;
        private char lastChar;
        private final int length;
        private final String text;

        static {
            for (int i = 0; i < DIR_TYPE_CACHE_SIZE; i++) {
                DIR_TYPE_CACHE[i] = Character.getDirectionality(i);
            }
        }

        DirectionalityEstimator(String str, boolean z) {
            String str2 = str;
            this.text = str2;
            this.isHtml = z;
            this.length = str2.length();
        }

        /* access modifiers changed from: package-private */
        public int getEntryDir() {
            this.charIndex = 0;
            int i = 0;
            int i2 = 0;
            int i3 = 0;
            while (this.charIndex < this.length && i3 == 0) {
                switch (dirTypeForward()) {
                    case 0:
                        if (i != 0) {
                            i3 = i;
                            break;
                        } else {
                            return -1;
                        }
                    case 1:
                    case 2:
                        if (i != 0) {
                            i3 = i;
                            break;
                        } else {
                            return 1;
                        }
                    case 3:
                    case 4:
                    case 5:
                    case 6:
                    case 7:
                    case 8:
                    case 10:
                    case 11:
                    case 12:
                    case 13:
                    default:
                        i3 = i;
                        break;
                    case 9:
                        break;
                    case 14:
                    case 15:
                        i++;
                        i2 = -1;
                        break;
                    case 16:
                    case 17:
                        i++;
                        i2 = 1;
                        break;
                    case 18:
                        i--;
                        i2 = 0;
                        break;
                }
            }
            if (i3 == 0) {
                return 0;
            }
            if (i2 != 0) {
                return i2;
            }
            while (this.charIndex > 0) {
                switch (dirTypeBackward()) {
                    case 14:
                    case 15:
                        if (i3 != i) {
                            i--;
                            break;
                        } else {
                            return -1;
                        }
                    case 16:
                    case 17:
                        if (i3 != i) {
                            i--;
                            break;
                        } else {
                            return 1;
                        }
                    case 18:
                        i++;
                        break;
                }
            }
            return 0;
        }

        /* access modifiers changed from: package-private */
        public int getExitDir() {
            this.charIndex = this.length;
            int i = 0;
            int i2 = 0;
            while (this.charIndex > 0) {
                switch (dirTypeBackward()) {
                    case 0:
                        if (i != 0) {
                            if (i2 != 0) {
                                break;
                            } else {
                                i2 = i;
                                break;
                            }
                        } else {
                            return -1;
                        }
                    case 1:
                    case 2:
                        if (i != 0) {
                            if (i2 != 0) {
                                break;
                            } else {
                                i2 = i;
                                break;
                            }
                        } else {
                            return 1;
                        }
                    case 3:
                    case 4:
                    case 5:
                    case 6:
                    case 7:
                    case 8:
                    case 10:
                    case 11:
                    case 12:
                    case 13:
                    default:
                        if (i2 != 0) {
                            break;
                        } else {
                            i2 = i;
                            break;
                        }
                    case 9:
                        break;
                    case 14:
                    case 15:
                        if (i2 != i) {
                            i--;
                            break;
                        } else {
                            return -1;
                        }
                    case 16:
                    case 17:
                        if (i2 != i) {
                            i--;
                            break;
                        } else {
                            return 1;
                        }
                    case 18:
                        i++;
                        break;
                }
            }
            return 0;
        }

        private static byte getCachedDirectionality(char c) {
            char c2 = c;
            return c2 < DIR_TYPE_CACHE_SIZE ? DIR_TYPE_CACHE[c2] : Character.getDirectionality(c2);
        }

        /* access modifiers changed from: package-private */
        public byte dirTypeForward() {
            this.lastChar = this.text.charAt(this.charIndex);
            if (Character.isHighSurrogate(this.lastChar)) {
                int codePointAt = Character.codePointAt(this.text, this.charIndex);
                this.charIndex = this.charIndex + Character.charCount(codePointAt);
                return Character.getDirectionality(codePointAt);
            }
            this.charIndex = this.charIndex + 1;
            byte cachedDirectionality = getCachedDirectionality(this.lastChar);
            if (this.isHtml) {
                if (this.lastChar == '<') {
                    cachedDirectionality = skipTagForward();
                } else if (this.lastChar == '&') {
                    cachedDirectionality = skipEntityForward();
                }
            }
            return cachedDirectionality;
        }

        /* access modifiers changed from: package-private */
        public byte dirTypeBackward() {
            this.lastChar = this.text.charAt(this.charIndex - 1);
            if (Character.isLowSurrogate(this.lastChar)) {
                int codePointBefore = Character.codePointBefore(this.text, this.charIndex);
                this.charIndex = this.charIndex - Character.charCount(codePointBefore);
                return Character.getDirectionality(codePointBefore);
            }
            this.charIndex = this.charIndex - 1;
            byte cachedDirectionality = getCachedDirectionality(this.lastChar);
            if (this.isHtml) {
                if (this.lastChar == '>') {
                    cachedDirectionality = skipTagBackward();
                } else if (this.lastChar == ';') {
                    cachedDirectionality = skipEntityBackward();
                }
            }
            return cachedDirectionality;
        }

        private byte skipTagForward() {
            int i = this.charIndex;
            while (this.charIndex < this.length) {
                String str = this.text;
                int i2 = this.charIndex;
                this.charIndex = i2 + 1;
                this.lastChar = str.charAt(i2);
                if (this.lastChar == '>') {
                    return 12;
                }
                if (this.lastChar == '\"' || this.lastChar == '\'') {
                    char c = this.lastChar;
                    while (this.charIndex < this.length) {
                        String str2 = this.text;
                        int i3 = this.charIndex;
                        this.charIndex = i3 + 1;
                        char charAt = str2.charAt(i3);
                        char c2 = charAt;
                        this.lastChar = charAt;
                        if (c2 == c) {
                            break;
                        }
                    }
                }
            }
            this.charIndex = i;
            this.lastChar = '<';
            return 13;
        }

        private byte skipTagBackward() {
            int i = this.charIndex;
            while (this.charIndex > 0) {
                String str = this.text;
                int i2 = this.charIndex - 1;
                this.charIndex = i2;
                this.lastChar = str.charAt(i2);
                if (this.lastChar == '<') {
                    return 12;
                }
                if (this.lastChar == '>') {
                    break;
                } else if (this.lastChar == '\"' || this.lastChar == '\'') {
                    char c = this.lastChar;
                    while (this.charIndex > 0) {
                        String str2 = this.text;
                        int i3 = this.charIndex - 1;
                        this.charIndex = i3;
                        char charAt = str2.charAt(i3);
                        char c2 = charAt;
                        this.lastChar = charAt;
                        if (c2 == c) {
                            break;
                        }
                    }
                }
            }
            this.charIndex = i;
            this.lastChar = '>';
            return 13;
        }

        private byte skipEntityForward() {
            while (this.charIndex < this.length) {
                String str = this.text;
                int i = this.charIndex;
                this.charIndex = i + 1;
                char charAt = str.charAt(i);
                char c = charAt;
                this.lastChar = charAt;
                if (c == ';') {
                    break;
                }
            }
            return 12;
        }

        private byte skipEntityBackward() {
            int i = this.charIndex;
            while (this.charIndex > 0) {
                String str = this.text;
                int i2 = this.charIndex - 1;
                this.charIndex = i2;
                this.lastChar = str.charAt(i2);
                if (this.lastChar != '&') {
                    if (this.lastChar == ';') {
                        break;
                    }
                } else {
                    return 12;
                }
            }
            this.charIndex = i;
            this.lastChar = ';';
            return 13;
        }
    }
}
