package android.support.v4.app;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;

public abstract class FragmentStatePagerAdapter extends PagerAdapter {
    private static final boolean DEBUG = false;
    private static final String TAG = "FragmentStatePagerAdapter";
    private FragmentTransaction mCurTransaction = null;
    private Fragment mCurrentPrimaryItem;
    private final FragmentManager mFragmentManager;
    private ArrayList<Fragment> mFragments;
    private ArrayList<Fragment.SavedState> mSavedState;

    public abstract Fragment getItem(int i);

    public FragmentStatePagerAdapter(FragmentManager fragmentManager) {
        ArrayList<Fragment.SavedState> arrayList;
        ArrayList<Fragment> arrayList2;
        new ArrayList<>();
        this.mSavedState = arrayList;
        new ArrayList<>();
        this.mFragments = arrayList2;
        this.mCurrentPrimaryItem = null;
        this.mFragmentManager = fragmentManager;
    }

    public void startUpdate(ViewGroup viewGroup) {
    }

    public Object instantiateItem(ViewGroup viewGroup, int i) {
        Fragment.SavedState savedState;
        Fragment fragment;
        ViewGroup viewGroup2 = viewGroup;
        int i2 = i;
        if (this.mFragments.size() > i2 && (fragment = this.mFragments.get(i2)) != null) {
            return fragment;
        }
        if (this.mCurTransaction == null) {
            this.mCurTransaction = this.mFragmentManager.beginTransaction();
        }
        Fragment item = getItem(i2);
        if (this.mSavedState.size() > i2 && (savedState = this.mSavedState.get(i2)) != null) {
            item.setInitialSavedState(savedState);
        }
        while (this.mFragments.size() <= i2) {
            boolean add = this.mFragments.add(null);
        }
        item.setMenuVisibility(false);
        item.setUserVisibleHint(false);
        Fragment fragment2 = this.mFragments.set(i2, item);
        FragmentTransaction add2 = this.mCurTransaction.add(viewGroup2.getId(), item);
        return item;
    }

    public void destroyItem(ViewGroup viewGroup, int i, Object obj) {
        int i2 = i;
        Fragment fragment = (Fragment) obj;
        if (this.mCurTransaction == null) {
            this.mCurTransaction = this.mFragmentManager.beginTransaction();
        }
        while (this.mSavedState.size() <= i2) {
            boolean add = this.mSavedState.add(null);
        }
        Fragment.SavedState savedState = this.mSavedState.set(i2, this.mFragmentManager.saveFragmentInstanceState(fragment));
        Fragment fragment2 = this.mFragments.set(i2, null);
        FragmentTransaction remove = this.mCurTransaction.remove(fragment);
    }

    public void setPrimaryItem(ViewGroup viewGroup, int i, Object obj) {
        Fragment fragment = (Fragment) obj;
        if (fragment != this.mCurrentPrimaryItem) {
            if (this.mCurrentPrimaryItem != null) {
                this.mCurrentPrimaryItem.setMenuVisibility(false);
                this.mCurrentPrimaryItem.setUserVisibleHint(false);
            }
            if (fragment != null) {
                fragment.setMenuVisibility(true);
                fragment.setUserVisibleHint(true);
            }
            this.mCurrentPrimaryItem = fragment;
        }
    }

    public void finishUpdate(ViewGroup viewGroup) {
        if (this.mCurTransaction != null) {
            int commitAllowingStateLoss = this.mCurTransaction.commitAllowingStateLoss();
            this.mCurTransaction = null;
            boolean executePendingTransactions = this.mFragmentManager.executePendingTransactions();
        }
    }

    public boolean isViewFromObject(View view, Object obj) {
        return ((Fragment) obj).getView() == view;
    }

    public Parcelable saveState() {
        StringBuilder sb;
        Bundle bundle;
        Bundle bundle2;
        Bundle bundle3 = null;
        if (this.mSavedState.size() > 0) {
            new Bundle();
            bundle3 = bundle2;
            Fragment.SavedState[] savedStateArr = new Fragment.SavedState[this.mSavedState.size()];
            Object[] array = this.mSavedState.toArray(savedStateArr);
            bundle3.putParcelableArray("states", savedStateArr);
        }
        for (int i = 0; i < this.mFragments.size(); i++) {
            Fragment fragment = this.mFragments.get(i);
            if (fragment != null && fragment.isAdded()) {
                if (bundle3 == null) {
                    new Bundle();
                    bundle3 = bundle;
                }
                new StringBuilder();
                this.mFragmentManager.putFragment(bundle3, sb.append("f").append(i).toString(), fragment);
            }
        }
        return bundle3;
    }

    public void restoreState(Parcelable parcelable, ClassLoader classLoader) {
        StringBuilder sb;
        Parcelable parcelable2 = parcelable;
        ClassLoader classLoader2 = classLoader;
        if (parcelable2 != null) {
            Bundle bundle = (Bundle) parcelable2;
            bundle.setClassLoader(classLoader2);
            Parcelable[] parcelableArray = bundle.getParcelableArray("states");
            this.mSavedState.clear();
            this.mFragments.clear();
            if (parcelableArray != null) {
                for (int i = 0; i < parcelableArray.length; i++) {
                    boolean add = this.mSavedState.add((Fragment.SavedState) parcelableArray[i]);
                }
            }
            for (String str : bundle.keySet()) {
                if (str.startsWith("f")) {
                    int parseInt = Integer.parseInt(str.substring(1));
                    Fragment fragment = this.mFragmentManager.getFragment(bundle, str);
                    if (fragment != null) {
                        while (this.mFragments.size() <= parseInt) {
                            boolean add2 = this.mFragments.add(null);
                        }
                        fragment.setMenuVisibility(false);
                        Fragment fragment2 = this.mFragments.set(parseInt, fragment);
                    } else {
                        new StringBuilder();
                        int w = Log.w(TAG, sb.append("Bad fragment at key ").append(str).toString());
                    }
                }
            }
        }
    }
}
