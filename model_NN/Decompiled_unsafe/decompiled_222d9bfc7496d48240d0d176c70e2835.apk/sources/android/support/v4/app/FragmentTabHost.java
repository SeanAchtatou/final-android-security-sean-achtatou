package android.support.v4.app;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TabHost;
import android.widget.TabWidget;
import java.util.ArrayList;

public class FragmentTabHost extends TabHost implements TabHost.OnTabChangeListener {
    private boolean mAttached;
    private int mContainerId;
    private Context mContext;
    private FragmentManager mFragmentManager;
    private TabInfo mLastTab;
    private TabHost.OnTabChangeListener mOnTabChangeListener;
    private FrameLayout mRealTabContent;
    private final ArrayList<TabInfo> mTabs;

    static final class TabInfo {
        /* access modifiers changed from: private */
        public final Bundle args;
        /* access modifiers changed from: private */
        public final Class<?> clss;
        /* access modifiers changed from: private */
        public Fragment fragment;
        /* access modifiers changed from: private */
        public final String tag;

        static /* synthetic */ Fragment access$102(TabInfo tabInfo, Fragment fragment2) {
            Fragment fragment3 = fragment2;
            Fragment fragment4 = fragment3;
            tabInfo.fragment = fragment4;
            return fragment3;
        }

        TabInfo(String str, Class<?> cls, Bundle bundle) {
            this.tag = str;
            this.clss = cls;
            this.args = bundle;
        }
    }

    static class DummyTabFactory implements TabHost.TabContentFactory {
        private final Context mContext;

        public DummyTabFactory(Context context) {
            this.mContext = context;
        }

        public View createTabContent(String str) {
            View view;
            new View(this.mContext);
            View view2 = view;
            view2.setMinimumWidth(0);
            view2.setMinimumHeight(0);
            return view2;
        }
    }

    static class SavedState extends View.BaseSavedState {
        public static final Parcelable.Creator<SavedState> CREATOR;
        String curTab;

        SavedState(Parcelable parcelable) {
            super(parcelable);
        }

        /* JADX WARNING: Illegal instructions before constructor call */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        private SavedState(android.os.Parcel r5) {
            /*
                r4 = this;
                r0 = r4
                r1 = r5
                r2 = r0
                r3 = r1
                r2.<init>(r3)
                r2 = r0
                r3 = r1
                java.lang.String r3 = r3.readString()
                r2.curTab = r3
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: android.support.v4.app.FragmentTabHost.SavedState.<init>(android.os.Parcel):void");
        }

        public void writeToParcel(Parcel parcel, int i) {
            Parcel parcel2 = parcel;
            super.writeToParcel(parcel2, i);
            parcel2.writeString(this.curTab);
        }

        public String toString() {
            StringBuilder sb;
            new StringBuilder();
            return sb.append("FragmentTabHost.SavedState{").append(Integer.toHexString(System.identityHashCode(this))).append(" curTab=").append(this.curTab).append("}").toString();
        }

        static {
            Parcelable.Creator<SavedState> creator;
            new Parcelable.Creator<SavedState>() {
                public SavedState createFromParcel(Parcel parcel) {
                    SavedState savedState;
                    new SavedState(parcel);
                    return savedState;
                }

                public SavedState[] newArray(int i) {
                    return new SavedState[i];
                }
            };
            CREATOR = creator;
        }
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public FragmentTabHost(android.content.Context r7) {
        /*
            r6 = this;
            r0 = r6
            r1 = r7
            r2 = r0
            r3 = r1
            r4 = 0
            r2.<init>(r3, r4)
            r2 = r0
            java.util.ArrayList r3 = new java.util.ArrayList
            r5 = r3
            r3 = r5
            r4 = r5
            r4.<init>()
            r2.mTabs = r3
            r2 = r0
            r3 = r1
            r4 = 0
            r2.initFragmentTabHost(r3, r4)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v4.app.FragmentTabHost.<init>(android.content.Context):void");
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public FragmentTabHost(android.content.Context r8, android.util.AttributeSet r9) {
        /*
            r7 = this;
            r0 = r7
            r1 = r8
            r2 = r9
            r3 = r0
            r4 = r1
            r5 = r2
            r3.<init>(r4, r5)
            r3 = r0
            java.util.ArrayList r4 = new java.util.ArrayList
            r6 = r4
            r4 = r6
            r5 = r6
            r5.<init>()
            r3.mTabs = r4
            r3 = r0
            r4 = r1
            r5 = r2
            r3.initFragmentTabHost(r4, r5)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v4.app.FragmentTabHost.<init>(android.content.Context, android.util.AttributeSet):void");
    }

    private void initFragmentTabHost(Context context, AttributeSet attributeSet) {
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, new int[]{16842995}, 0, 0);
        this.mContainerId = obtainStyledAttributes.getResourceId(0, 0);
        obtainStyledAttributes.recycle();
        super.setOnTabChangedListener(this);
    }

    private void ensureHierarchy(Context context) {
        LinearLayout linearLayout;
        ViewGroup.LayoutParams layoutParams;
        TabWidget tabWidget;
        ViewGroup.LayoutParams layoutParams2;
        FrameLayout frameLayout;
        ViewGroup.LayoutParams layoutParams3;
        FrameLayout frameLayout2;
        ViewGroup.LayoutParams layoutParams4;
        Context context2 = context;
        if (findViewById(16908307) == null) {
            new LinearLayout(context2);
            LinearLayout linearLayout2 = linearLayout;
            linearLayout2.setOrientation(1);
            new FrameLayout.LayoutParams(-1, -1);
            addView(linearLayout2, layoutParams);
            new TabWidget(context2);
            TabWidget tabWidget2 = tabWidget;
            tabWidget2.setId(16908307);
            tabWidget2.setOrientation(0);
            new LinearLayout.LayoutParams(-1, -2, 0.0f);
            linearLayout2.addView(tabWidget2, layoutParams2);
            new FrameLayout(context2);
            FrameLayout frameLayout3 = frameLayout;
            frameLayout3.setId(16908305);
            new LinearLayout.LayoutParams(0, 0, 0.0f);
            linearLayout2.addView(frameLayout3, layoutParams3);
            new FrameLayout(context2);
            FrameLayout frameLayout4 = frameLayout2;
            this.mRealTabContent = frameLayout4;
            this.mRealTabContent.setId(this.mContainerId);
            new LinearLayout.LayoutParams(-1, 0, 1.0f);
            linearLayout2.addView(frameLayout4, layoutParams4);
        }
    }

    @Deprecated
    public void setup() {
        Throwable th;
        Throwable th2 = th;
        new IllegalStateException("Must call setup() that takes a Context and FragmentManager");
        throw th2;
    }

    public void setup(Context context, FragmentManager fragmentManager) {
        Context context2 = context;
        ensureHierarchy(context2);
        super.setup();
        this.mContext = context2;
        this.mFragmentManager = fragmentManager;
        ensureContent();
    }

    public void setup(Context context, FragmentManager fragmentManager, int i) {
        Context context2 = context;
        int i2 = i;
        ensureHierarchy(context2);
        super.setup();
        this.mContext = context2;
        this.mFragmentManager = fragmentManager;
        this.mContainerId = i2;
        ensureContent();
        this.mRealTabContent.setId(i2);
        if (getId() == -1) {
            setId(16908306);
        }
    }

    private void ensureContent() {
        Throwable th;
        StringBuilder sb;
        if (this.mRealTabContent == null) {
            this.mRealTabContent = (FrameLayout) findViewById(this.mContainerId);
            if (this.mRealTabContent == null) {
                Throwable th2 = th;
                new StringBuilder();
                new IllegalStateException(sb.append("No tab content FrameLayout found for id ").append(this.mContainerId).toString());
                throw th2;
            }
        }
    }

    public void setOnTabChangedListener(TabHost.OnTabChangeListener onTabChangeListener) {
        this.mOnTabChangeListener = onTabChangeListener;
    }

    public void addTab(TabHost.TabSpec tabSpec, Class<?> cls, Bundle bundle) {
        TabHost.TabContentFactory tabContentFactory;
        TabInfo tabInfo;
        TabHost.TabSpec tabSpec2 = tabSpec;
        new DummyTabFactory(this.mContext);
        TabHost.TabSpec content = tabSpec2.setContent(tabContentFactory);
        String tag = tabSpec2.getTag();
        new TabInfo(tag, cls, bundle);
        TabInfo tabInfo2 = tabInfo;
        if (this.mAttached) {
            Fragment access$102 = TabInfo.access$102(tabInfo2, this.mFragmentManager.findFragmentByTag(tag));
            if (tabInfo2.fragment != null && !tabInfo2.fragment.isDetached()) {
                FragmentTransaction beginTransaction = this.mFragmentManager.beginTransaction();
                FragmentTransaction detach = beginTransaction.detach(tabInfo2.fragment);
                int commit = beginTransaction.commit();
            }
        }
        boolean add = this.mTabs.add(tabInfo2);
        addTab(tabSpec2);
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        String currentTabTag = getCurrentTabTag();
        FragmentTransaction fragmentTransaction = null;
        for (int i = 0; i < this.mTabs.size(); i++) {
            TabInfo tabInfo = this.mTabs.get(i);
            Fragment access$102 = TabInfo.access$102(tabInfo, this.mFragmentManager.findFragmentByTag(tabInfo.tag));
            if (tabInfo.fragment != null && !tabInfo.fragment.isDetached()) {
                if (tabInfo.tag.equals(currentTabTag)) {
                    this.mLastTab = tabInfo;
                } else {
                    if (fragmentTransaction == null) {
                        fragmentTransaction = this.mFragmentManager.beginTransaction();
                    }
                    FragmentTransaction detach = fragmentTransaction.detach(tabInfo.fragment);
                }
            }
        }
        this.mAttached = true;
        FragmentTransaction doTabChanged = doTabChanged(currentTabTag, fragmentTransaction);
        if (doTabChanged != null) {
            int commit = doTabChanged.commit();
            boolean executePendingTransactions = this.mFragmentManager.executePendingTransactions();
        }
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.mAttached = false;
    }

    /* access modifiers changed from: protected */
    public Parcelable onSaveInstanceState() {
        SavedState savedState;
        new SavedState(super.onSaveInstanceState());
        SavedState savedState2 = savedState;
        savedState2.curTab = getCurrentTabTag();
        return savedState2;
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Parcelable parcelable) {
        SavedState savedState = (SavedState) parcelable;
        super.onRestoreInstanceState(savedState.getSuperState());
        setCurrentTabByTag(savedState.curTab);
    }

    public void onTabChanged(String str) {
        FragmentTransaction doTabChanged;
        String str2 = str;
        if (this.mAttached && (doTabChanged = doTabChanged(str2, null)) != null) {
            int commit = doTabChanged.commit();
        }
        if (this.mOnTabChangeListener != null) {
            this.mOnTabChangeListener.onTabChanged(str2);
        }
    }

    private FragmentTransaction doTabChanged(String str, FragmentTransaction fragmentTransaction) {
        Throwable th;
        StringBuilder sb;
        String str2 = str;
        FragmentTransaction fragmentTransaction2 = fragmentTransaction;
        TabInfo tabInfo = null;
        for (int i = 0; i < this.mTabs.size(); i++) {
            TabInfo tabInfo2 = this.mTabs.get(i);
            if (tabInfo2.tag.equals(str2)) {
                tabInfo = tabInfo2;
            }
        }
        if (tabInfo == null) {
            Throwable th2 = th;
            new StringBuilder();
            new IllegalStateException(sb.append("No tab known for tag ").append(str2).toString());
            throw th2;
        }
        if (this.mLastTab != tabInfo) {
            if (fragmentTransaction2 == null) {
                fragmentTransaction2 = this.mFragmentManager.beginTransaction();
            }
            if (!(this.mLastTab == null || this.mLastTab.fragment == null)) {
                FragmentTransaction detach = fragmentTransaction2.detach(this.mLastTab.fragment);
            }
            if (tabInfo != null) {
                if (tabInfo.fragment == null) {
                    Fragment access$102 = TabInfo.access$102(tabInfo, Fragment.instantiate(this.mContext, tabInfo.clss.getName(), tabInfo.args));
                    FragmentTransaction add = fragmentTransaction2.add(this.mContainerId, tabInfo.fragment, tabInfo.tag);
                } else {
                    FragmentTransaction attach = fragmentTransaction2.attach(tabInfo.fragment);
                }
            }
            this.mLastTab = tabInfo;
        }
        return fragmentTransaction2;
    }
}
