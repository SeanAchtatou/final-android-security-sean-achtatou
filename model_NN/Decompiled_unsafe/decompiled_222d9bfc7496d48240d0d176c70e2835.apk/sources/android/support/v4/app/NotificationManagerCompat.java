package android.support.v4.app;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.DeadObjectException;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Message;
import android.os.RemoteException;
import android.provider.Settings;
import android.support.v4.app.INotificationSideChannel;
import android.util.Log;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class NotificationManagerCompat {
    public static final String ACTION_BIND_SIDE_CHANNEL = "android.support.BIND_NOTIFICATION_SIDE_CHANNEL";
    public static final String EXTRA_USE_SIDE_CHANNEL = "android.support.useSideChannel";
    private static final Impl IMPL;
    static final int MAX_SIDE_CHANNEL_SDK_VERSION = 19;
    private static final String SETTING_ENABLED_NOTIFICATION_LISTENERS = "enabled_notification_listeners";
    /* access modifiers changed from: private */
    public static final int SIDE_CHANNEL_BIND_FLAGS = IMPL.getSideChannelBindFlags();
    private static final int SIDE_CHANNEL_RETRY_BASE_INTERVAL_MS = 1000;
    private static final int SIDE_CHANNEL_RETRY_MAX_COUNT = 6;
    private static final String TAG = "NotifManCompat";
    private static Set<String> sEnabledNotificationListenerPackages;
    private static String sEnabledNotificationListeners;
    private static final Object sEnabledNotificationListenersLock;
    private static final Object sLock;
    private static SideChannelManager sSideChannelManager;
    private final Context mContext;
    private final NotificationManager mNotificationManager = ((NotificationManager) this.mContext.getSystemService("notification"));

    interface Impl {
        void cancelNotification(NotificationManager notificationManager, String str, int i);

        int getSideChannelBindFlags();

        void postNotification(NotificationManager notificationManager, String str, int i, Notification notification);
    }

    private interface Task {
        void send(INotificationSideChannel iNotificationSideChannel) throws RemoteException;
    }

    static {
        Object obj;
        Set<String> set;
        Object obj2;
        Impl impl;
        Impl impl2;
        Impl impl3;
        new Object();
        sEnabledNotificationListenersLock = obj;
        new HashSet();
        sEnabledNotificationListenerPackages = set;
        new Object();
        sLock = obj2;
        if (Build.VERSION.SDK_INT >= 14) {
            new ImplIceCreamSandwich();
            IMPL = impl3;
        } else if (Build.VERSION.SDK_INT >= 5) {
            new ImplEclair();
            IMPL = impl2;
        } else {
            new ImplBase();
            IMPL = impl;
        }
    }

    public static NotificationManagerCompat from(Context context) {
        NotificationManagerCompat notificationManagerCompat;
        new NotificationManagerCompat(context);
        return notificationManagerCompat;
    }

    private NotificationManagerCompat(Context context) {
        this.mContext = context;
    }

    static class ImplBase implements Impl {
        ImplBase() {
        }

        public void cancelNotification(NotificationManager notificationManager, String str, int i) {
            notificationManager.cancel(i);
        }

        public void postNotification(NotificationManager notificationManager, String str, int i, Notification notification) {
            notificationManager.notify(i, notification);
        }

        public int getSideChannelBindFlags() {
            return 1;
        }
    }

    static class ImplEclair extends ImplBase {
        ImplEclair() {
        }

        public void cancelNotification(NotificationManager notificationManager, String str, int i) {
            NotificationManagerCompatEclair.cancelNotification(notificationManager, str, i);
        }

        public void postNotification(NotificationManager notificationManager, String str, int i, Notification notification) {
            NotificationManagerCompatEclair.postNotification(notificationManager, str, i, notification);
        }
    }

    static class ImplIceCreamSandwich extends ImplEclair {
        ImplIceCreamSandwich() {
        }

        public int getSideChannelBindFlags() {
            return 33;
        }
    }

    public void cancel(int i) {
        cancel(null, i);
    }

    public void cancel(String str, int i) {
        Task task;
        String str2 = str;
        int i2 = i;
        IMPL.cancelNotification(this.mNotificationManager, str2, i2);
        if (Build.VERSION.SDK_INT <= 19) {
            new CancelTask(this.mContext.getPackageName(), i2, str2);
            pushSideChannelQueue(task);
        }
    }

    public void cancelAll() {
        Task task;
        this.mNotificationManager.cancelAll();
        if (Build.VERSION.SDK_INT <= 19) {
            new CancelTask(this.mContext.getPackageName());
            pushSideChannelQueue(task);
        }
    }

    public void notify(int i, Notification notification) {
        notify(null, i, notification);
    }

    public void notify(String str, int i, Notification notification) {
        Task task;
        String str2 = str;
        int i2 = i;
        Notification notification2 = notification;
        if (useSideChannelForNotification(notification2)) {
            new NotifyTask(this.mContext.getPackageName(), i2, str2, notification2);
            pushSideChannelQueue(task);
            IMPL.cancelNotification(this.mNotificationManager, str2, i2);
            return;
        }
        IMPL.postNotification(this.mNotificationManager, str2, i2, notification2);
    }

    public static Set<String> getEnabledListenerPackages(Context context) {
        Set<String> set;
        String string = Settings.Secure.getString(context.getContentResolver(), SETTING_ENABLED_NOTIFICATION_LISTENERS);
        if (string != null && !string.equals(sEnabledNotificationListeners)) {
            String[] split = string.split(":");
            new HashSet(split.length);
            Set<String> set2 = set;
            String[] strArr = split;
            int length = strArr.length;
            for (int i = 0; i < length; i++) {
                ComponentName unflattenFromString = ComponentName.unflattenFromString(strArr[i]);
                if (unflattenFromString != null) {
                    boolean add = set2.add(unflattenFromString.getPackageName());
                }
            }
            Object obj = sEnabledNotificationListenersLock;
            Object obj2 = obj;
            synchronized (obj) {
                try {
                    sEnabledNotificationListenerPackages = set2;
                    sEnabledNotificationListeners = string;
                } catch (Throwable th) {
                    while (true) {
                        throw th;
                    }
                }
            }
        }
        return sEnabledNotificationListenerPackages;
    }

    private static boolean useSideChannelForNotification(Notification notification) {
        Bundle extras = NotificationCompat.getExtras(notification);
        return extras != null && extras.getBoolean(EXTRA_USE_SIDE_CHANNEL);
    }

    private void pushSideChannelQueue(Task task) {
        SideChannelManager sideChannelManager;
        Task task2 = task;
        Object obj = sLock;
        Object obj2 = obj;
        synchronized (obj) {
            try {
                if (sSideChannelManager == null) {
                    new SideChannelManager(this.mContext.getApplicationContext());
                    sSideChannelManager = sideChannelManager;
                }
                sSideChannelManager.queueTask(task2);
            } catch (Throwable th) {
                while (true) {
                    throw th;
                }
            }
        }
    }

    private static class SideChannelManager implements Handler.Callback, ServiceConnection {
        private static final String KEY_BINDER = "binder";
        private static final int MSG_QUEUE_TASK = 0;
        private static final int MSG_RETRY_LISTENER_QUEUE = 3;
        private static final int MSG_SERVICE_CONNECTED = 1;
        private static final int MSG_SERVICE_DISCONNECTED = 2;
        private Set<String> mCachedEnabledPackages;
        private final Context mContext;
        private final Handler mHandler;
        private final HandlerThread mHandlerThread;
        private final Map<ComponentName, ListenerRecord> mRecordMap;

        public SideChannelManager(Context context) {
            Map<ComponentName, ListenerRecord> map;
            Set<String> set;
            HandlerThread handlerThread;
            Handler handler;
            new HashMap();
            this.mRecordMap = map;
            new HashSet();
            this.mCachedEnabledPackages = set;
            this.mContext = context;
            new HandlerThread("NotificationManagerCompat");
            this.mHandlerThread = handlerThread;
            this.mHandlerThread.start();
            new Handler(this.mHandlerThread.getLooper(), this);
            this.mHandler = handler;
        }

        public void queueTask(Task task) {
            this.mHandler.obtainMessage(0, task).sendToTarget();
        }

        public boolean handleMessage(Message message) {
            Message message2 = message;
            switch (message2.what) {
                case 0:
                    handleQueueTask((Task) message2.obj);
                    return true;
                case 1:
                    ServiceConnectedEvent serviceConnectedEvent = (ServiceConnectedEvent) message2.obj;
                    handleServiceConnected(serviceConnectedEvent.componentName, serviceConnectedEvent.iBinder);
                    return true;
                case 2:
                    handleServiceDisconnected((ComponentName) message2.obj);
                    return true;
                case 3:
                    handleRetryListenerQueue((ComponentName) message2.obj);
                    return true;
                default:
                    return false;
            }
        }

        private void handleQueueTask(Task task) {
            Task task2 = task;
            updateListenerMap();
            for (ListenerRecord next : this.mRecordMap.values()) {
                boolean add = next.taskQueue.add(task2);
                processListenerQueue(next);
            }
        }

        private void handleServiceConnected(ComponentName componentName, IBinder iBinder) {
            IBinder iBinder2 = iBinder;
            ListenerRecord listenerRecord = this.mRecordMap.get(componentName);
            if (listenerRecord != null) {
                listenerRecord.service = INotificationSideChannel.Stub.asInterface(iBinder2);
                listenerRecord.retryCount = 0;
                processListenerQueue(listenerRecord);
            }
        }

        private void handleServiceDisconnected(ComponentName componentName) {
            ListenerRecord listenerRecord = this.mRecordMap.get(componentName);
            if (listenerRecord != null) {
                ensureServiceUnbound(listenerRecord);
            }
        }

        private void handleRetryListenerQueue(ComponentName componentName) {
            ListenerRecord listenerRecord = this.mRecordMap.get(componentName);
            if (listenerRecord != null) {
                processListenerQueue(listenerRecord);
            }
        }

        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            Object obj;
            StringBuilder sb;
            ComponentName componentName2 = componentName;
            IBinder iBinder2 = iBinder;
            if (Log.isLoggable(NotificationManagerCompat.TAG, 3)) {
                new StringBuilder();
                int d = Log.d(NotificationManagerCompat.TAG, sb.append("Connected to service ").append(componentName2).toString());
            }
            new ServiceConnectedEvent(componentName2, iBinder2);
            this.mHandler.obtainMessage(1, obj).sendToTarget();
        }

        public void onServiceDisconnected(ComponentName componentName) {
            StringBuilder sb;
            ComponentName componentName2 = componentName;
            if (Log.isLoggable(NotificationManagerCompat.TAG, 3)) {
                new StringBuilder();
                int d = Log.d(NotificationManagerCompat.TAG, sb.append("Disconnected from service ").append(componentName2).toString());
            }
            this.mHandler.obtainMessage(2, componentName2).sendToTarget();
        }

        private void updateListenerMap() {
            Intent intent;
            Set set;
            StringBuilder sb;
            Object obj;
            StringBuilder sb2;
            Object obj2;
            StringBuilder sb3;
            Set<String> enabledListenerPackages = NotificationManagerCompat.getEnabledListenerPackages(this.mContext);
            if (!enabledListenerPackages.equals(this.mCachedEnabledPackages)) {
                this.mCachedEnabledPackages = enabledListenerPackages;
                PackageManager packageManager = this.mContext.getPackageManager();
                new Intent();
                List<ResolveInfo> queryIntentServices = packageManager.queryIntentServices(intent.setAction(NotificationManagerCompat.ACTION_BIND_SIDE_CHANNEL), 4);
                new HashSet();
                Set<ComponentName> set2 = set;
                for (ResolveInfo next : queryIntentServices) {
                    if (enabledListenerPackages.contains(next.serviceInfo.packageName)) {
                        new ComponentName(next.serviceInfo.packageName, next.serviceInfo.name);
                        Object obj3 = obj2;
                        if (next.serviceInfo.permission != null) {
                            new StringBuilder();
                            int w = Log.w(NotificationManagerCompat.TAG, sb3.append("Permission present on component ").append(obj3).append(", not adding listener record.").toString());
                        } else {
                            boolean add = set2.add(obj3);
                        }
                    }
                }
                for (ComponentName componentName : set2) {
                    if (!this.mRecordMap.containsKey(componentName)) {
                        if (Log.isLoggable(NotificationManagerCompat.TAG, 3)) {
                            new StringBuilder();
                            int d = Log.d(NotificationManagerCompat.TAG, sb2.append("Adding listener record for ").append(componentName).toString());
                        }
                        new ListenerRecord(componentName);
                        ListenerRecord put = this.mRecordMap.put(componentName, obj);
                    }
                }
                Iterator<Map.Entry<ComponentName, ListenerRecord>> it = this.mRecordMap.entrySet().iterator();
                while (it.hasNext()) {
                    Map.Entry next2 = it.next();
                    if (!set2.contains(next2.getKey())) {
                        if (Log.isLoggable(NotificationManagerCompat.TAG, 3)) {
                            new StringBuilder();
                            int d2 = Log.d(NotificationManagerCompat.TAG, sb.append("Removing listener record for ").append(next2.getKey()).toString());
                        }
                        ensureServiceUnbound((ListenerRecord) next2.getValue());
                        it.remove();
                    }
                }
            }
        }

        private boolean ensureServiceBound(ListenerRecord listenerRecord) {
            Intent intent;
            StringBuilder sb;
            ListenerRecord listenerRecord2 = listenerRecord;
            if (listenerRecord2.bound) {
                return true;
            }
            new Intent(NotificationManagerCompat.ACTION_BIND_SIDE_CHANNEL);
            listenerRecord2.bound = this.mContext.bindService(intent.setComponent(listenerRecord2.componentName), this, NotificationManagerCompat.SIDE_CHANNEL_BIND_FLAGS);
            if (listenerRecord2.bound) {
                listenerRecord2.retryCount = 0;
            } else {
                new StringBuilder();
                int w = Log.w(NotificationManagerCompat.TAG, sb.append("Unable to bind to listener ").append(listenerRecord2.componentName).toString());
                this.mContext.unbindService(this);
            }
            return listenerRecord2.bound;
        }

        private void ensureServiceUnbound(ListenerRecord listenerRecord) {
            ListenerRecord listenerRecord2 = listenerRecord;
            if (listenerRecord2.bound) {
                this.mContext.unbindService(this);
                listenerRecord2.bound = false;
            }
            listenerRecord2.service = null;
        }

        private void scheduleListenerRetry(ListenerRecord listenerRecord) {
            StringBuilder sb;
            StringBuilder sb2;
            ListenerRecord listenerRecord2 = listenerRecord;
            if (!this.mHandler.hasMessages(3, listenerRecord2.componentName)) {
                listenerRecord2.retryCount++;
                if (listenerRecord2.retryCount > 6) {
                    new StringBuilder();
                    int w = Log.w(NotificationManagerCompat.TAG, sb2.append("Giving up on delivering ").append(listenerRecord2.taskQueue.size()).append(" tasks to ").append(listenerRecord2.componentName).append(" after ").append(listenerRecord2.retryCount).append(" retries").toString());
                    listenerRecord2.taskQueue.clear();
                    return;
                }
                int i = NotificationManagerCompat.SIDE_CHANNEL_RETRY_BASE_INTERVAL_MS * (1 << (listenerRecord2.retryCount - 1));
                if (Log.isLoggable(NotificationManagerCompat.TAG, 3)) {
                    new StringBuilder();
                    int d = Log.d(NotificationManagerCompat.TAG, sb.append("Scheduling retry for ").append(i).append(" ms").toString());
                }
                boolean sendMessageDelayed = this.mHandler.sendMessageDelayed(this.mHandler.obtainMessage(3, listenerRecord2.componentName), (long) i);
            }
        }

        private void processListenerQueue(ListenerRecord listenerRecord) {
            StringBuilder sb;
            StringBuilder sb2;
            StringBuilder sb3;
            StringBuilder sb4;
            ListenerRecord listenerRecord2 = listenerRecord;
            if (Log.isLoggable(NotificationManagerCompat.TAG, 3)) {
                new StringBuilder();
                int d = Log.d(NotificationManagerCompat.TAG, sb4.append("Processing component ").append(listenerRecord2.componentName).append(", ").append(listenerRecord2.taskQueue.size()).append(" queued tasks").toString());
            }
            if (!listenerRecord2.taskQueue.isEmpty()) {
                if (!ensureServiceBound(listenerRecord2) || listenerRecord2.service == null) {
                    scheduleListenerRetry(listenerRecord2);
                    return;
                }
                while (true) {
                    Task peek = listenerRecord2.taskQueue.peek();
                    if (peek == null) {
                        break;
                    }
                    try {
                        if (Log.isLoggable(NotificationManagerCompat.TAG, 3)) {
                            new StringBuilder();
                            int d2 = Log.d(NotificationManagerCompat.TAG, sb3.append("Sending task ").append(peek).toString());
                        }
                        peek.send(listenerRecord2.service);
                        Task remove = listenerRecord2.taskQueue.remove();
                    } catch (DeadObjectException e) {
                        if (Log.isLoggable(NotificationManagerCompat.TAG, 3)) {
                            new StringBuilder();
                            int d3 = Log.d(NotificationManagerCompat.TAG, sb2.append("Remote service has died: ").append(listenerRecord2.componentName).toString());
                        }
                    } catch (RemoteException e2) {
                        new StringBuilder();
                        int w = Log.w(NotificationManagerCompat.TAG, sb.append("RemoteException communicating with ").append(listenerRecord2.componentName).toString(), e2);
                    }
                }
                if (!listenerRecord2.taskQueue.isEmpty()) {
                    scheduleListenerRetry(listenerRecord2);
                }
            }
        }

        private static class ListenerRecord {
            public boolean bound = false;
            public final ComponentName componentName;
            public int retryCount;
            public INotificationSideChannel service;
            public LinkedList<Task> taskQueue;

            public ListenerRecord(ComponentName componentName2) {
                LinkedList<Task> linkedList;
                new LinkedList<>();
                this.taskQueue = linkedList;
                this.retryCount = 0;
                this.componentName = componentName2;
            }
        }
    }

    private static class ServiceConnectedEvent {
        final ComponentName componentName;
        final IBinder iBinder;

        public ServiceConnectedEvent(ComponentName componentName2, IBinder iBinder2) {
            this.componentName = componentName2;
            this.iBinder = iBinder2;
        }
    }

    private static class NotifyTask implements Task {
        final int id;
        final Notification notif;
        final String packageName;
        final String tag;

        public NotifyTask(String str, int i, String str2, Notification notification) {
            this.packageName = str;
            this.id = i;
            this.tag = str2;
            this.notif = notification;
        }

        public void send(INotificationSideChannel iNotificationSideChannel) throws RemoteException {
            iNotificationSideChannel.notify(this.packageName, this.id, this.tag, this.notif);
        }

        public String toString() {
            StringBuilder sb;
            new StringBuilder("NotifyTask[");
            StringBuilder sb2 = sb;
            StringBuilder append = sb2.append("packageName:").append(this.packageName);
            StringBuilder append2 = sb2.append(", id:").append(this.id);
            StringBuilder append3 = sb2.append(", tag:").append(this.tag);
            StringBuilder append4 = sb2.append("]");
            return sb2.toString();
        }
    }

    private static class CancelTask implements Task {
        final boolean all;
        final int id;
        final String packageName;
        final String tag;

        public CancelTask(String str) {
            this.packageName = str;
            this.id = 0;
            this.tag = null;
            this.all = true;
        }

        public CancelTask(String str, int i, String str2) {
            this.packageName = str;
            this.id = i;
            this.tag = str2;
            this.all = false;
        }

        public void send(INotificationSideChannel iNotificationSideChannel) throws RemoteException {
            INotificationSideChannel iNotificationSideChannel2 = iNotificationSideChannel;
            if (this.all) {
                iNotificationSideChannel2.cancelAll(this.packageName);
            } else {
                iNotificationSideChannel2.cancel(this.packageName, this.id, this.tag);
            }
        }

        public String toString() {
            StringBuilder sb;
            new StringBuilder("CancelTask[");
            StringBuilder sb2 = sb;
            StringBuilder append = sb2.append("packageName:").append(this.packageName);
            StringBuilder append2 = sb2.append(", id:").append(this.id);
            StringBuilder append3 = sb2.append(", tag:").append(this.tag);
            StringBuilder append4 = sb2.append(", all:").append(this.all);
            StringBuilder append5 = sb2.append("]");
            return sb2.toString();
        }
    }
}
