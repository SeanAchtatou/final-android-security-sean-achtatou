package android.support.v4.provider;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.DocumentsContract;
import android.text.TextUtils;
import android.util.Log;

class DocumentsContractApi19 {
    private static final String TAG = "DocumentFile";

    DocumentsContractApi19() {
    }

    public static boolean isDocumentUri(Context context, Uri uri) {
        return DocumentsContract.isDocumentUri(context, uri);
    }

    public static String getName(Context context, Uri uri) {
        return queryForString(context, uri, "_display_name", null);
    }

    private static String getRawType(Context context, Uri uri) {
        return queryForString(context, uri, "mime_type", null);
    }

    public static String getType(Context context, Uri uri) {
        String rawType = getRawType(context, uri);
        if ("vnd.android.document/directory".equals(rawType)) {
            return null;
        }
        return rawType;
    }

    public static boolean isDirectory(Context context, Uri uri) {
        return "vnd.android.document/directory".equals(getRawType(context, uri));
    }

    public static boolean isFile(Context context, Uri uri) {
        String rawType = getRawType(context, uri);
        if ("vnd.android.document/directory".equals(rawType) || TextUtils.isEmpty(rawType)) {
            return false;
        }
        return true;
    }

    public static long lastModified(Context context, Uri uri) {
        return queryForLong(context, uri, "last_modified", 0);
    }

    public static long length(Context context, Uri uri) {
        return queryForLong(context, uri, "_size", 0);
    }

    public static boolean canRead(Context context, Uri uri) {
        Context context2 = context;
        Uri uri2 = uri;
        if (context2.checkCallingOrSelfUriPermission(uri2, 1) != 0) {
            return false;
        }
        if (TextUtils.isEmpty(getRawType(context2, uri2))) {
            return false;
        }
        return true;
    }

    public static boolean canWrite(Context context, Uri uri) {
        Context context2 = context;
        Uri uri2 = uri;
        if (context2.checkCallingOrSelfUriPermission(uri2, 2) != 0) {
            return false;
        }
        String rawType = getRawType(context2, uri2);
        int queryForInt = queryForInt(context2, uri2, "flags", 0);
        if (TextUtils.isEmpty(rawType)) {
            return false;
        }
        if ((queryForInt & 4) != 0) {
            return true;
        }
        if ("vnd.android.document/directory".equals(rawType) && (queryForInt & 8) != 0) {
            return true;
        }
        if (TextUtils.isEmpty(rawType) || (queryForInt & 2) == 0) {
            return false;
        }
        return true;
    }

    public static boolean delete(Context context, Uri uri) {
        return DocumentsContract.deleteDocument(context.getContentResolver(), uri);
    }

    public static boolean exists(Context context, Uri uri) {
        StringBuilder sb;
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(uri, new String[]{"document_id"}, null, null, null);
            boolean z = cursor.getCount() > 0;
            closeQuietly(cursor);
            return z;
        } catch (Exception e) {
            Exception exc = e;
            new StringBuilder();
            int w = Log.w(TAG, sb.append("Failed query: ").append(exc).toString());
            closeQuietly(cursor);
            return false;
        } catch (Throwable th) {
            Throwable th2 = th;
            closeQuietly(cursor);
            throw th2;
        }
    }

    private static String queryForString(Context context, Uri uri, String str, String str2) {
        StringBuilder sb;
        String str3 = str;
        String str4 = str2;
        try {
            Cursor query = context.getContentResolver().query(uri, new String[]{str3}, null, null, null);
            if (!query.moveToFirst() || query.isNull(0)) {
                String str5 = str4;
                closeQuietly(query);
                return str5;
            }
            String string = query.getString(0);
            closeQuietly(query);
            return string;
        } catch (Exception e) {
            Exception exc = e;
            new StringBuilder();
            int w = Log.w(TAG, sb.append("Failed query: ").append(exc).toString());
            closeQuietly(null);
            return str4;
        } catch (Throwable th) {
            closeQuietly(null);
            throw th;
        }
    }

    private static int queryForInt(Context context, Uri uri, String str, int i) {
        return (int) queryForLong(context, uri, str, (long) i);
    }

    private static long queryForLong(Context context, Uri uri, String str, long j) {
        StringBuilder sb;
        String str2 = str;
        long j2 = j;
        try {
            Cursor query = context.getContentResolver().query(uri, new String[]{str2}, null, null, null);
            if (!query.moveToFirst() || query.isNull(0)) {
                long j3 = j2;
                closeQuietly(query);
                return j3;
            }
            long j4 = query.getLong(0);
            closeQuietly(query);
            return j4;
        } catch (Exception e) {
            Exception exc = e;
            new StringBuilder();
            int w = Log.w(TAG, sb.append("Failed query: ").append(exc).toString());
            closeQuietly(null);
            return j2;
        } catch (Throwable th) {
            closeQuietly(null);
            throw th;
        }
    }

    private static void closeQuietly(AutoCloseable autoCloseable) {
        AutoCloseable autoCloseable2 = autoCloseable;
        if (autoCloseable2 != null) {
            try {
                autoCloseable2.close();
            } catch (RuntimeException e) {
                throw e;
            } catch (Exception e2) {
            }
        }
    }
}
