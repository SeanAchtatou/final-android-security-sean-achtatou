package android.support.v4.os;

import android.os.Parcelable;

/* compiled from: ParcelableCompatHoneycombMR2 */
class ParcelableCompatCreatorHoneycombMR2Stub {
    ParcelableCompatCreatorHoneycombMR2Stub() {
    }

    static <T> Parcelable.Creator<T> instantiate(ParcelableCompatCreatorCallbacks<T> parcelableCompatCreatorCallbacks) {
        Parcelable.Creator<T> creator;
        new ParcelableCompatCreatorHoneycombMR2(parcelableCompatCreatorCallbacks);
        return creator;
    }
}
