package android.support.v4.media;

import android.media.VolumeProvider;

class VolumeProviderCompatApi21 {

    public interface Delegate {
        void onAdjustVolume(int i);

        void onSetVolumeTo(int i);
    }

    VolumeProviderCompatApi21() {
    }

    public static Object createVolumeProvider(int i, int i2, int i3, Delegate delegate) {
        Object obj;
        final Delegate delegate2 = delegate;
        new VolumeProvider(i, i2, i3) {
            public void onSetVolumeTo(int i) {
                delegate2.onSetVolumeTo(i);
            }

            public void onAdjustVolume(int i) {
                delegate2.onAdjustVolume(i);
            }
        };
        return obj;
    }

    public static void setCurrentVolume(Object obj, int i) {
        ((VolumeProvider) obj).setCurrentVolume(i);
    }
}
