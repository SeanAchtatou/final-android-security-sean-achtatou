package android.support.v4.media.session;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.view.KeyEvent;
import java.util.List;

public class MediaButtonReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        Intent intent2;
        ComponentName componentName;
        Throwable th;
        StringBuilder sb;
        Context context2 = context;
        Intent intent3 = intent;
        new Intent("android.intent.action.MEDIA_BUTTON");
        Intent intent4 = intent2;
        Intent intent5 = intent4.setPackage(context2.getPackageName());
        List<ResolveInfo> queryIntentServices = context2.getPackageManager().queryIntentServices(intent4, 0);
        if (queryIntentServices.size() != 1) {
            Throwable th2 = th;
            new StringBuilder();
            new IllegalStateException(sb.append("Expected 1 Service that handles android.intent.action.MEDIA_BUTTON, found ").append(queryIntentServices.size()).toString());
            throw th2;
        }
        ResolveInfo resolveInfo = queryIntentServices.get(0);
        new ComponentName(resolveInfo.serviceInfo.packageName, resolveInfo.serviceInfo.name);
        Intent component = intent3.setComponent(componentName);
        ComponentName startService = context2.startService(intent3);
    }

    public static KeyEvent handleIntent(MediaSessionCompat mediaSessionCompat, Intent intent) {
        MediaSessionCompat mediaSessionCompat2 = mediaSessionCompat;
        Intent intent2 = intent;
        if (mediaSessionCompat2 == null || intent2 == null || !"android.intent.action.MEDIA_BUTTON".equals(intent2.getAction()) || !intent2.hasExtra("android.intent.extra.KEY_EVENT")) {
            return null;
        }
        KeyEvent keyEvent = (KeyEvent) intent2.getParcelableExtra("android.intent.extra.KEY_EVENT");
        boolean dispatchMediaButtonEvent = mediaSessionCompat2.getController().dispatchMediaButtonEvent(keyEvent);
        return keyEvent;
    }
}
