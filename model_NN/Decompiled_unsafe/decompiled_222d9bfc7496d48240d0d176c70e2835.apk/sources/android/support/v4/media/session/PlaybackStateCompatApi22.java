package android.support.v4.media.session;

import android.media.session.PlaybackState;
import android.os.Bundle;
import java.util.Iterator;
import java.util.List;

class PlaybackStateCompatApi22 {
    PlaybackStateCompatApi22() {
    }

    public static Bundle getExtras(Object obj) {
        return ((PlaybackState) obj).getExtras();
    }

    public static Object newInstance(int i, long j, long j2, float f, long j3, CharSequence charSequence, long j4, List<Object> list, long j5, Bundle bundle) {
        PlaybackState.Builder builder;
        long j6 = j5;
        Bundle bundle2 = bundle;
        new PlaybackState.Builder();
        PlaybackState.Builder builder2 = builder;
        PlaybackState.Builder state = builder2.setState(i, j, f, j4);
        PlaybackState.Builder bufferedPosition = builder2.setBufferedPosition(j2);
        PlaybackState.Builder actions = builder2.setActions(j3);
        PlaybackState.Builder errorMessage = builder2.setErrorMessage(charSequence);
        Iterator<Object> it = list.iterator();
        while (it.hasNext()) {
            PlaybackState.Builder addCustomAction = builder2.addCustomAction((PlaybackState.CustomAction) it.next());
        }
        PlaybackState.Builder activeQueueItemId = builder2.setActiveQueueItemId(j6);
        PlaybackState.Builder extras = builder2.setExtras(bundle2);
        return builder2.build();
    }
}
