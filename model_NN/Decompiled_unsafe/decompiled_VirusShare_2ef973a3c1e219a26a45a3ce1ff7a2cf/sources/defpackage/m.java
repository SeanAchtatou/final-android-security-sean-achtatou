package defpackage;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;

/* renamed from: m  reason: default package */
public class m {
    private static int a = 50000;
    private static int b = 50000;
    private Proxy c;
    private Proxy d;

    private HttpURLConnection a(URL url) {
        try {
            if (b.r != null && (b.r.indexOf("cmwap") != -1 || b.r.indexOf("uniwap") != -1 || b.r.indexOf("3gwap") != -1)) {
                if (this.c == null) {
                    this.c = new Proxy(Proxy.Type.HTTP, new InetSocketAddress("10.0.0.172", 80));
                }
                return (HttpURLConnection) url.openConnection(this.c);
            } else if (b.r == null || b.r.indexOf("ctwap") == -1) {
                return (HttpURLConnection) url.openConnection();
            } else {
                if (this.d == null) {
                    this.d = new Proxy(Proxy.Type.HTTP, new InetSocketAddress("10.0.0.200", 80));
                }
                return (HttpURLConnection) url.openConnection(this.d);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public void a(String str, String str2, n nVar) {
        HttpURLConnection httpURLConnection;
        InputStream inputStream;
        try {
            HttpURLConnection a2 = a(new URL(str));
            try {
                a2.setRequestMethod("POST");
                a2.setConnectTimeout(a);
                a2.setDoOutput(true);
                a2.setReadTimeout(b);
                OutputStream outputStream = a2.getOutputStream();
                PrintWriter printWriter = new PrintWriter(outputStream);
                printWriter.write(str2);
                printWriter.flush();
                InputStream inputStream2 = a2.getInputStream();
                try {
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    byte[] bArr = new byte[1024];
                    while (true) {
                        int read = inputStream2.read(bArr);
                        if (read != -1) {
                            byteArrayOutputStream.write(bArr, 0, read);
                        } else {
                            byteArrayOutputStream.close();
                            inputStream2.close();
                            nVar.a(new ByteArrayInputStream(byteArrayOutputStream.toByteArray()));
                            a2.disconnect();
                            outputStream.close();
                            printWriter.close();
                            return;
                        }
                    }
                } catch (Exception e) {
                    inputStream = inputStream2;
                    HttpURLConnection httpURLConnection2 = a2;
                    e = e;
                    httpURLConnection = httpURLConnection2;
                }
            } catch (Exception e2) {
                inputStream = null;
                HttpURLConnection httpURLConnection3 = a2;
                e = e2;
                httpURLConnection = httpURLConnection3;
            }
        } catch (Exception e3) {
            e = e3;
            httpURLConnection = null;
            inputStream = null;
            e.printStackTrace();
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (Exception e4) {
                    e4.printStackTrace();
                }
            }
            if (httpURLConnection != null) {
                try {
                    httpURLConnection.disconnect();
                } catch (Exception e5) {
                    e5.printStackTrace();
                }
            }
            nVar.a(0);
        }
    }

    public void a(String str, n nVar) {
        HttpURLConnection httpURLConnection;
        InputStream inputStream = null;
        try {
            HttpURLConnection a2 = a(new URL(str));
            try {
                a2.setRequestMethod("GET");
                a2.setConnectTimeout(a);
                a2.setDoOutput(true);
                a2.setReadTimeout(b);
                InputStream inputStream2 = a2.getInputStream();
                try {
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    byte[] bArr = new byte[1024];
                    while (true) {
                        int read = inputStream2.read(bArr);
                        if (read != -1) {
                            byteArrayOutputStream.write(bArr, 0, read);
                        } else {
                            byteArrayOutputStream.close();
                            inputStream2.close();
                            nVar.a(new ByteArrayInputStream(byteArrayOutputStream.toByteArray()));
                            a2.disconnect();
                            return;
                        }
                    }
                } catch (Exception e) {
                    Exception exc = e;
                    inputStream = inputStream2;
                    httpURLConnection = a2;
                    e = exc;
                }
            } catch (Exception e2) {
                Exception exc2 = e2;
                httpURLConnection = a2;
                e = exc2;
            }
        } catch (Exception e3) {
            e = e3;
            httpURLConnection = null;
            e.printStackTrace();
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (Exception e4) {
                    e4.printStackTrace();
                }
            }
            httpURLConnection.disconnect();
            nVar.a(0);
        }
    }
}
