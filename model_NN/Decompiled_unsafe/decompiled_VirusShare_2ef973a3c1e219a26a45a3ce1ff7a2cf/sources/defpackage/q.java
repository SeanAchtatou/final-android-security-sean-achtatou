package defpackage;

import java.io.InputStream;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.SAXException;

/* renamed from: q  reason: default package */
public class q extends p {
    b c;
    StringBuffer d = new StringBuffer();
    m e = new m();
    SAXParserFactory f = SAXParserFactory.newInstance();
    SAXParser g;
    s h;

    public q() {
        try {
            this.g = this.f.newSAXParser();
        } catch (ParserConfigurationException e2) {
            e2.printStackTrace();
        } catch (SAXException e3) {
            e3.printStackTrace();
        }
        this.h = new s(this);
    }

    public void a(int i) {
        this.c.c(30000);
    }

    public void a(b bVar) {
        this.c = bVar;
        this.d.setLength(0);
        this.d.append("ret=2");
        this.d.append("&rqt=1");
        this.d.append("&aid=" + bVar.b.d);
        this.d.append("&u=" + bVar.g);
        this.d.append("&tid=" + bVar.d);
        this.d.append("&shid=" + bVar.b.c);
        this.d.append("&pid=" + bVar.c);
        this.d.append("&m=" + bVar.h);
        this.d.append("&o=" + bVar.i);
        this.d.append("&ty=" + bVar.f);
        this.d.append("&ip=" + bVar.j);
        this.d.append("&tt=" + bVar.l);
        this.d.append("&v=" + bVar.q);
        this.d.append("&st=" + bVar.n);
        this.d.append("&ac=" + b.r);
        this.d.append("&ca=" + bVar.s);
        if (o.a().d.toString().equals("none")) {
            this.e.a(a, this.d.toString(), this);
        } else {
            this.e.a(o.a().d.toString(), this.d.toString(), this);
        }
    }

    public void a(InputStream inputStream) {
        try {
            this.g.parse(inputStream, this.h);
        } catch (Exception e2) {
            e2.printStackTrace();
            this.c.c(30000);
        }
    }
}
