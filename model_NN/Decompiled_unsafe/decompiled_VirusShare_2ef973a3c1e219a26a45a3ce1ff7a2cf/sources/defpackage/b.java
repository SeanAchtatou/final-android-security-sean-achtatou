package defpackage;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Message;
import android.provider.Browser;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.izp.views.IZPDelegate;
import com.izp.views.IZPView;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Timer;

/* renamed from: b  reason: default package */
public class b implements View.OnTouchListener, Animation.AnimationListener {
    public static b P = null;
    public static String r = "";
    public TextView A;
    public ImageView B;
    public TextView C;
    public ad D;
    public ProgressBar E;
    public boolean F;
    public boolean G;
    public Timer H;
    public Timer I;
    public String J = "-1";
    public IZPView K;
    public IZPDelegate L;
    e M;
    public int N;
    public boolean O = false;
    public boolean Q = true;
    public ArrayList R = new ArrayList();
    public boolean S = false;
    public double T = 0.0d;
    public boolean U = false;
    public boolean V = false;
    public boolean W = false;
    int[] X = new int[2];
    DisplayMetrics Y = new DisplayMetrics();
    Bitmap Z = null;
    public a a;
    final int aa = 0;
    final int ab = 1;
    final int ac = 2;
    /* access modifiers changed from: private */
    public b ad = this;
    public k b;
    public String c;
    public String d;
    public String e = "2";
    public String f = "1";
    public String g;
    public String h;
    public String i;
    public String j;
    public String k;
    public String l;
    public StringBuffer m = new StringBuffer();
    public String n = "2";
    public int o;
    public int p;
    public String q = "1.0.2";
    public String s = "";
    public u t;
    public x u;
    public t v;
    public w w;
    public v x;
    public q y;
    public ae z;

    public b() {
        this.m.append("false");
        this.u = new x();
        this.t = new u();
        this.v = new t();
        this.w = new w();
        this.x = new v();
        this.y = new q();
        this.a = new a();
        this.b = new k();
        this.M = new e(this);
        this.I = new Timer();
        this.G = false;
        this.F = false;
        this.R.add("REQUEST_CONFIGURE");
        this.H = new Timer();
        this.H.scheduleAtFixedRate(new d(this), 10, 800);
    }

    public static b a() {
        if (P == null) {
            P = new b();
        }
        return P;
    }

    public void a(int i2) {
        this.I.schedule(new f(this), (long) i2);
    }

    public void a(int i2, String str) {
        if (this.L != null) {
            this.L.errorReport(this.K, i2, str);
        }
        a(10000);
    }

    public void a(IZPDelegate iZPDelegate) {
        this.L = iZPDelegate;
    }

    public void a(IZPView iZPView) {
        if (this.K != null) {
            this.K.removeAllViews();
        }
        this.K = iZPView;
        this.E = new ProgressBar(this.K.getContext(), null, 16842873);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams.addRule(13);
        this.E.setLayoutParams(layoutParams);
        this.z = new ae(iZPView.getContext());
        this.z.setBackgroundColor(0);
        this.z.setOnTouchListener(this);
        this.z.addView(this.E);
        this.B = new ImageView(iZPView.getContext());
        this.B.setVisibility(4);
        this.z.addView(this.B);
        this.A = new TextView(iZPView.getContext());
        this.A.setVisibility(4);
        this.A.setBackgroundColor(17170445);
        this.z.addView(this.A);
        this.C = new TextView(iZPView.getContext());
        this.C.setVisibility(4);
        this.C.setBackgroundColor(17170445);
        this.z.addView(this.C);
        this.D = new ad(iZPView.getContext());
        this.D.b = this;
        this.D.setVisibility(4);
        this.z.addView(this.D);
        this.K.addView(this.z);
        SharedPreferences sharedPreferences = this.K.getContext().getSharedPreferences("izp_pre", 0);
        o.a().d.setLength(0);
        o.a().d.append(sharedPreferences.getString("izp_pre", "none"));
        f();
        this.J = "-1";
        if (this.b.s >= 1) {
            this.R.add("FLUSH_AD");
        }
    }

    public void a(String str) {
        this.c = str;
    }

    public void a(boolean z2) {
        if (z2) {
            this.h = "true";
        } else {
            this.h = "false";
        }
    }

    public void b(int i2) {
        this.I.schedule(new h(this), (long) i2);
    }

    public void b(String str) {
        this.k = str;
    }

    public boolean b() {
        int i2;
        int i3;
        if (this.K != null) {
            Context context = this.K.getContext();
            if (context instanceof Activity) {
                Display defaultDisplay = ((Activity) context).getWindowManager().getDefaultDisplay();
                i3 = defaultDisplay.getWidth();
                i2 = defaultDisplay.getHeight();
            } else {
                i2 = 0;
                i3 = 0;
            }
            this.K.getLocationOnScreen(this.X);
            int measuredWidth = this.K.getMeasuredWidth();
            int measuredHeight = this.K.getMeasuredHeight();
            if (this.X[0] < 0 || this.X[1] < 0 || measuredWidth + this.X[0] > i3 || this.X[1] + measuredHeight > i2) {
                return false;
            }
        }
        return true;
    }

    public void c() {
        if (this.K != null) {
            this.K.bringToFront();
        }
        if (this.R.size() != 0 && !this.S && !this.Q) {
            String str = (String) this.R.get(0);
            if (str.equals("REQUEST_CONFIGURE")) {
                this.y.a(this.ad);
            } else if (str.equals("REQUEST_XML")) {
                if (this.L != null && !this.L.shouldRequestFreshAd(this.K)) {
                    return;
                }
                if (((double) System.currentTimeMillis()) - this.T >= 3600000.0d) {
                    this.R.add("REQUEST_CONFIGURE");
                } else {
                    this.u.a(this.ad);
                }
            } else if (str.equals("REQUEST_IMG")) {
                if (this.a.e.toString().compareTo("1") == 0 || this.a.e.toString().compareTo("5") == 0) {
                    this.t.a(this, this.a.f.toString());
                } else if (this.a.e.toString().compareTo("2") == 0) {
                    this.t.a(this, this.a.p.toString());
                } else {
                    this.R.add("RECEIVE_AD_NOTIFICATION");
                }
            } else if (str.equals("RECEIVE_AD_NOTIFICATION")) {
                this.a.s++;
                if (this.L != null) {
                    this.L.didReceiveFreshAd(this.K, this.a.s);
                }
                this.R.add("SWITCH_AD");
            } else if (str.equals("SWITCH_AD")) {
                if (this.K != null && this.U && this.V && this.W && b()) {
                    if (this.L == null || this.L.shouldShowFreshAd(this.K)) {
                        p();
                    } else {
                        return;
                    }
                } else {
                    return;
                }
            } else if (str.equals("FLUSH_AD")) {
                q();
            }
            this.R.remove(0);
        }
    }

    public void c(int i2) {
        this.I.schedule(new g(this), (long) i2);
    }

    public void d() {
        this.Q = false;
    }

    public void e() {
        this.Q = true;
    }

    public void f() {
        String o2;
        TelephonyManager telephonyManager = (TelephonyManager) this.K.getContext().getSystemService("phone");
        this.s = telephonyManager.getNetworkOperatorName();
        this.d = aa.a(telephonyManager.getDeviceId().getBytes(), 0).trim();
        this.g = aa.a(Build.MANUFACTURER.getBytes(), 0).trim();
        this.l = aa.a(Build.MODEL.getBytes(), 0).trim();
        this.i = aa.a(("Android" + Build.VERSION.RELEASE).getBytes(), 0).trim();
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) this.K.getContext().getSystemService("connectivity")).getActiveNetworkInfo();
        if (activeNetworkInfo != null && activeNetworkInfo.isAvailable()) {
            if (activeNetworkInfo.getType() == 0) {
                String extraInfo = activeNetworkInfo.getExtraInfo();
                if (extraInfo != null) {
                    r = extraInfo.toLowerCase();
                } else {
                    r = "unknown";
                }
            } else {
                r = "wifi";
            }
        }
        WifiManager wifiManager = (WifiManager) this.K.getContext().getSystemService("wifi");
        if (wifiManager.isWifiEnabled()) {
            int ipAddress = wifiManager.getConnectionInfo().getIpAddress();
            o2 = String.format("%d.%d.%d.%d", Integer.valueOf(ipAddress & 255), Integer.valueOf((ipAddress >> 8) & 255), Integer.valueOf((ipAddress >> 16) & 255), Integer.valueOf((ipAddress >> 24) & 255));
        } else {
            o2 = o();
        }
        this.j = o2;
    }

    public void g() {
        this.o = this.K.getWidth();
        this.p = this.K.getHeight();
    }

    public void h() {
        this.b.a = this.a.a.toString();
        this.b.b = this.a.b.toString();
        this.b.c = this.a.c.toString();
        this.b.d = this.a.d.toString();
        this.b.e = this.a.e.toString();
        this.b.f = this.a.f.toString();
        this.b.g = this.a.g.toString();
        this.b.h = this.a.h.toString();
        this.b.i = this.a.i.toString();
        this.b.j = this.a.j.toString();
        this.b.k = this.a.k.toString();
        this.b.l = this.a.l.toString();
        this.b.m = this.a.m.toString();
        this.b.n = this.a.n.toString();
        this.b.o = this.a.o.toString();
        this.b.p = this.a.p.toString();
        this.b.q = this.a.q.toString();
        this.Z = this.b.r;
        this.b.r = this.a.r;
        this.b.s++;
    }

    public synchronized void i() {
        this.F = false;
        h();
        if (this.b.s == 1) {
            l();
            this.F = true;
        } else {
            n();
        }
        a(Integer.valueOf(o.a().b.toString()).intValue() * 1000);
        b(Integer.valueOf(o.a().c.toString()).intValue() * 1000);
        if (this.m.toString().equals("true") && !this.O) {
            this.I.schedule(new j(this), 1000);
            this.O = true;
        }
    }

    public void j() {
        if (this.J.compareTo(this.b.e) != 0) {
            if (this.k.toString().compareTo("1") == 0) {
                if (this.b.e.compareTo("1") == 0) {
                    RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(this.K.getWidth(), this.K.getHeight());
                    layoutParams.addRule(14);
                    this.z.setLayoutParams(layoutParams);
                    this.z.a = false;
                    RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(this.z.getWidth(), this.z.getHeight());
                    layoutParams2.addRule(13);
                    this.B.setLayoutParams(layoutParams2);
                    this.B.setVisibility(0);
                    this.A.setVisibility(4);
                    this.C.setVisibility(4);
                    this.D.setVisibility(4);
                } else if (this.b.e.compareTo("2") == 0) {
                    RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(this.K.getWidth(), this.K.getHeight());
                    layoutParams3.addRule(14);
                    this.z.setLayoutParams(layoutParams3);
                    this.z.a = true;
                    RelativeLayout.LayoutParams layoutParams4 = new RelativeLayout.LayoutParams(this.b.r.getWidth(), this.b.r.getHeight());
                    layoutParams4.addRule(15);
                    layoutParams4.setMargins(5, (this.K.getHeight() - this.b.r.getHeight()) / 2, 0, 0);
                    this.B.setLayoutParams(layoutParams4);
                    this.B.setVisibility(0);
                    this.B.setId(18);
                    this.B.setMinimumHeight(this.b.r.getHeight());
                    RelativeLayout.LayoutParams layoutParams5 = new RelativeLayout.LayoutParams((this.z.getWidth() - 38) - 5, this.z.getHeight());
                    layoutParams5.addRule(1, 18);
                    this.A.setLayoutParams(layoutParams5);
                    this.A.setVisibility(0);
                    RelativeLayout.LayoutParams layoutParams6 = new RelativeLayout.LayoutParams(65, 12);
                    layoutParams6.addRule(11);
                    layoutParams6.addRule(12, -1);
                    this.C.setLayoutParams(layoutParams6);
                    this.C.setVisibility(0);
                    this.D.setVisibility(4);
                } else if (this.b.e.compareTo("3") == 0) {
                    RelativeLayout.LayoutParams layoutParams7 = new RelativeLayout.LayoutParams(this.K.getWidth(), this.K.getHeight());
                    layoutParams7.addRule(14);
                    this.z.setLayoutParams(layoutParams7);
                    this.z.a = true;
                    RelativeLayout.LayoutParams layoutParams8 = new RelativeLayout.LayoutParams(this.z.getWidth(), this.z.getHeight());
                    layoutParams8.addRule(13);
                    this.A.setLayoutParams(layoutParams8);
                    this.A.setVisibility(0);
                    RelativeLayout.LayoutParams layoutParams9 = new RelativeLayout.LayoutParams(65, 12);
                    layoutParams9.addRule(11);
                    layoutParams9.addRule(12, -1);
                    this.C.setLayoutParams(layoutParams9);
                    this.C.setVisibility(0);
                    this.B.setVisibility(4);
                    this.D.setVisibility(4);
                }
            } else if (this.k.toString().compareTo("2") == 0 && this.b.e.compareTo("5") == 0) {
                RelativeLayout.LayoutParams layoutParams10 = new RelativeLayout.LayoutParams(this.K.getWidth(), this.K.getHeight());
                layoutParams10.addRule(13);
                this.z.setLayoutParams(layoutParams10);
                this.z.a = false;
                RelativeLayout.LayoutParams layoutParams11 = new RelativeLayout.LayoutParams(this.K.getWidth(), this.K.getHeight());
                layoutParams11.addRule(13);
                this.B.setLayoutParams(layoutParams11);
                this.B.setScaleType(ImageView.ScaleType.MATRIX);
                this.B.setVisibility(0);
                if (this.K.getWidth() < this.K.getHeight()) {
                    Matrix matrix = new Matrix();
                    matrix.preScale((((float) this.K.getWidth()) * 1.0f) / (((float) this.a.r.getHeight()) * 1.0f), (((float) this.K.getHeight()) * 1.0f) / (((float) this.a.r.getWidth()) * 1.0f));
                    matrix.preRotate(90.0f);
                    matrix.postTranslate((float) this.K.getWidth(), 0.0f);
                    this.B.setImageMatrix(matrix);
                } else {
                    Matrix matrix2 = new Matrix();
                    matrix2.preScale((float) (this.K.getWidth() / this.a.r.getWidth()), (float) (this.K.getHeight() / this.a.r.getHeight()));
                    this.B.setImageMatrix(matrix2);
                }
                RelativeLayout.LayoutParams layoutParams12 = new RelativeLayout.LayoutParams(60, 60);
                layoutParams12.addRule(11);
                layoutParams12.addRule(12, -1);
                layoutParams12.setMargins(0, 0, 20, 20);
                this.D.setLayoutParams(layoutParams12);
                this.D.setVisibility(0);
                this.A.setVisibility(4);
                this.C.setVisibility(4);
            }
            this.J = this.b.e;
        }
    }

    public void k() {
        if (this.b.e.compareTo("1") == 0) {
            this.B.setImageBitmap(this.b.r);
        } else if (this.b.e.compareTo("2") == 0) {
            this.B.setImageBitmap(this.b.r);
            m();
        } else if (this.b.e.compareTo("3") == 0) {
            m();
        } else if (this.b.e.compareTo("5") == 0) {
            this.B.setImageBitmap(this.b.r);
        }
        if (this.Z != null && !this.Z.isRecycled()) {
            this.Z.recycle();
            this.Z = null;
        }
    }

    public void l() {
        if (this.E.getVisibility() == 0) {
            this.E.setVisibility(4);
        }
        j();
        k();
    }

    public void m() {
        if (this.b.q.compareTo("1") == 0) {
            this.A.setGravity(3);
        } else if (this.b.q.compareTo("2") == 0) {
            this.A.setGravity(17);
        } else if (this.b.q.compareTo("3") == 0) {
            this.A.setGravity(5);
        }
        this.A.setTextColor(Long.decode(this.b.j).intValue());
        this.C.setTextColor(Long.decode(this.b.j).intValue());
        this.C.setText("Powered by IZP");
        this.C.setTextSize(6.0f);
        if (this.b.l.compareTo("1") == 0) {
            this.A.setTypeface(null, 1);
        } else if (this.b.l.compareTo("2") == 0) {
            this.A.setTypeface(null, 2);
        } else {
            this.A.setTypeface(null, 0);
        }
        this.A.setTextSize((float) Long.decode(this.b.m).intValue());
        this.A.setText(this.b.q);
    }

    public void n() {
        l lVar = new l(0, -((this.K.getWidth() / 2) + (this.z.getWidth() / 2)), 0, 0);
        lVar.a = 1;
        lVar.setDuration(1000);
        lVar.setInterpolator(new LinearInterpolator());
        lVar.setAnimationListener(this);
        this.z.startAnimation(lVar);
    }

    public String o() {
        try {
            Enumeration<NetworkInterface> networkInterfaces = NetworkInterface.getNetworkInterfaces();
            while (networkInterfaces.hasMoreElements()) {
                Enumeration<InetAddress> inetAddresses = networkInterfaces.nextElement().getInetAddresses();
                while (true) {
                    if (inetAddresses.hasMoreElements()) {
                        InetAddress nextElement = inetAddresses.nextElement();
                        if (!nextElement.isLoopbackAddress()) {
                            return nextElement.getHostAddress().toString();
                        }
                    }
                }
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        return null;
    }

    public void onAnimationEnd(Animation animation) {
        if (((l) animation).a == 1) {
            l();
            l lVar = new l(this.K.getWidth(), 0, 0, 0);
            lVar.a = 2;
            animation.setInterpolator(new LinearInterpolator());
            lVar.setDuration(1000);
            lVar.setAnimationListener(this);
            this.z.startAnimation(lVar);
        } else if (((l) animation).a == 2) {
            this.F = true;
        }
    }

    public void onAnimationRepeat(Animation animation) {
    }

    public void onAnimationStart(Animation animation) {
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        if (motionEvent.getAction() == 0 && this.F) {
            if (!this.S) {
                this.S = true;
                r();
            }
            s();
        }
        return true;
    }

    public void p() {
        Message message = new Message();
        message.what = 0;
        this.M.sendMessage(message);
    }

    public void q() {
        Message message = new Message();
        message.what = 2;
        this.M.sendMessage(message);
    }

    public void r() {
        this.I.schedule(new i(this), 10);
    }

    public synchronized void s() {
        if (this.L != null) {
            this.L.willLeaveApplication(this.K);
        }
        String trim = this.b.g.trim();
        if (this.b.h.compareTo("1") == 0) {
            this.K.getContext().startActivity(new Intent("android.intent.action.VIEW", Uri.parse(trim)));
        } else if (this.b.h.compareTo("3") == 0) {
            try {
                this.K.getContext().startActivity(new Intent("android.intent.action.CALL", Uri.parse("tel:" + trim)));
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        } else if (this.b.h.compareTo("4") == 0) {
            try {
                int indexOf = trim.indexOf("@");
                String substring = trim.substring(0, indexOf);
                Intent intent = new Intent("android.intent.action.SENDTO", Uri.parse("smsto:" + trim.substring(indexOf + 1, trim.length() - 1)));
                intent.putExtra("sms_body", substring);
                this.K.getContext().startActivity(intent);
            } catch (Exception e3) {
                e3.printStackTrace();
            }
        }
        return;
    }

    public String t() {
        StringBuffer stringBuffer = new StringBuffer();
        PackageManager packageManager = this.K.getContext().getPackageManager();
        for (ApplicationInfo next : packageManager.getInstalledApplications(128)) {
            if ((next.flags & 1) == 0) {
                stringBuffer.append(packageManager.getApplicationLabel(next));
                stringBuffer.append(",");
            }
        }
        try {
            return aa.a(stringBuffer.toString().getBytes(), 2);
        } catch (Exception e2) {
            e2.printStackTrace();
            return null;
        }
    }

    public String u() {
        StringBuffer stringBuffer = new StringBuffer();
        Cursor allVisitedUrls = Browser.getAllVisitedUrls(this.K.getContext().getContentResolver());
        if (allVisitedUrls.moveToFirst() && allVisitedUrls.getCount() > 0) {
            while (!allVisitedUrls.isAfterLast()) {
                String string = allVisitedUrls.getString(allVisitedUrls.getColumnIndex("url"));
                if (string.indexOf("http://") != -1) {
                    string = string.substring(7);
                }
                int i2 = 0;
                int i3 = 0;
                while (i3 < 4) {
                    i2 = string.indexOf("/", i2 + 1);
                    if (i2 == -1) {
                        break;
                    }
                    i3++;
                }
                if (i3 == 4 && i2 != -1) {
                    string = string.substring(0, i2);
                }
                int indexOf = string.indexOf("?");
                if (indexOf != -1) {
                    string = string.substring(0, indexOf);
                }
                if (string.length() > 60) {
                    string = string.substring(0, 60);
                }
                stringBuffer.append(string + ",");
                allVisitedUrls.moveToNext();
            }
        }
        if (stringBuffer.toString().lastIndexOf(",") == stringBuffer.length() - 1) {
            stringBuffer.delete(stringBuffer.length() - 1, stringBuffer.length());
        }
        try {
            return aa.a(stringBuffer.toString().getBytes(), 2);
        } catch (Exception e2) {
            e2.printStackTrace();
            return null;
        }
    }
}
