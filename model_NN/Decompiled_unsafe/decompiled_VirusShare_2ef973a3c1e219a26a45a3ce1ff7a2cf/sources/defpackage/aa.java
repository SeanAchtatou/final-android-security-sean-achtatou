package defpackage;

import java.io.UnsupportedEncodingException;

/* renamed from: aa  reason: default package */
public class aa {
    static final /* synthetic */ boolean a = (!aa.class.desiredAssertionStatus());

    private aa() {
    }

    public static String a(byte[] bArr, int i) {
        try {
            return new String(b(bArr, i), "US-ASCII");
        } catch (UnsupportedEncodingException e) {
            throw new AssertionError(e);
        }
    }

    public static byte[] a(byte[] bArr, int i, int i2, int i3) {
        ac acVar = new ac(i3, null);
        int i4 = (i2 / 3) * 4;
        if (!acVar.d) {
            switch (i2 % 3) {
                case 1:
                    i4 += 2;
                    break;
                case 2:
                    i4 += 3;
                    break;
            }
        } else if (i2 % 3 > 0) {
            i4 += 4;
        }
        if (acVar.e && i2 > 0) {
            i4 += (((i2 - 1) / 57) + 1) * (acVar.f ? 2 : 1);
        }
        acVar.a = new byte[i4];
        acVar.a(bArr, i, i2, true);
        if (a || acVar.b == i4) {
            return acVar.a;
        }
        throw new AssertionError();
    }

    public static byte[] b(byte[] bArr, int i) {
        return a(bArr, 0, bArr.length, i);
    }
}
