package defpackage;

import org.xml.sax.Attributes;
import org.xml.sax.SAXParseException;
import org.xml.sax.helpers.DefaultHandler;

/* renamed from: z  reason: default package */
final class z extends DefaultHandler {
    String a;
    StringBuffer b;
    final /* synthetic */ x c;

    private z(x xVar) {
        this.c = xVar;
        this.a = "";
        this.b = new StringBuffer();
    }

    public void characters(char[] cArr, int i, int i2) {
        if (this.a.compareTo("ad_freq") == 0) {
            this.c.c.a.a.append(cArr, i, i2);
        } else if (this.a.compareTo("ad_vtime") == 0) {
            this.c.c.a.b.append(cArr, i, i2);
        } else if (this.a.compareTo("shid") == 0) {
            this.c.c.a.c.append(cArr, i, i2);
        } else if (this.a.compareTo("aid") == 0) {
            this.c.c.a.d.append(cArr, i, i2);
        } else if (this.a.compareTo("ay") == 0) {
            this.c.c.a.e.append(cArr, i, i2);
        } else if (this.a.compareTo("ad_url") == 0) {
            this.c.c.a.f.append(cArr, i, i2);
        } else if (this.a.compareTo("ad_link") == 0) {
            this.c.c.a.g.append(cArr, i, i2);
        } else if (this.a.compareTo("cr") == 0) {
            this.c.c.a.h.append(cArr, i, i2);
        } else if (this.a.compareTo("ad_height") == 0) {
            this.c.c.a.i.append(cArr, i, i2);
        } else if (this.a.compareTo("font_color") == 0) {
            this.c.c.a.j.append(cArr, i, i2);
        } else if (this.a.compareTo("bg_color") == 0) {
            this.c.c.a.k.append(cArr, i, i2);
        } else if (this.a.compareTo("font_type") == 0) {
            this.c.c.a.l.append(cArr, i, i2);
        } else if (this.a.compareTo("font_size") == 0) {
            this.c.c.a.m.append(cArr, i, i2);
        } else if (this.a.compareTo("text_align") == 0) {
            this.c.c.a.n.append(cArr, i, i2);
        } else if (this.a.compareTo("margin_span") == 0) {
            this.c.c.a.o.append(cArr, i, i2);
        } else if (this.a.compareTo("ad_icon") == 0) {
            this.c.c.a.p.append(cArr, i, i2);
        } else if (this.a.compareTo("ad_text") == 0) {
            this.c.c.a.q.append(cArr, i, i2);
        } else if (this.a.compareTo("dsi") == 0) {
            this.c.c.m.append(cArr, i, i2);
        } else if (this.a.compareTo("err_code") == 0) {
            this.c.b.append(cArr, i, i2);
        }
        this.b.append(cArr, i, i2);
    }

    public void endDocument() {
    }

    public void endElement(String str, String str2, String str3) {
        this.a = "";
        if (str2.compareTo("ad") != 0) {
            return;
        }
        if (this.c.b.toString().compareTo("0") == 0) {
            this.c.c.R.add("REQUEST_IMG");
        } else {
            this.c.c.a(Integer.valueOf(this.c.b.toString()).intValue(), "system error");
        }
    }

    public void error(SAXParseException sAXParseException) {
        this.c.c.a(this.c.c.N + 2, "parse ad error!");
    }

    public void startDocument() {
        this.a = "";
    }

    public void startElement(String str, String str2, String str3, Attributes attributes) {
        if (str2.compareTo("ad_freq") == 0) {
            this.a = "ad_freq";
            this.c.c.a.a.setLength(0);
        } else if (str2.compareTo("ad_vtime") == 0) {
            this.a = "ad_vtime";
            this.c.c.a.b.setLength(0);
        } else if (str2.compareTo("shid") == 0) {
            this.a = "shid";
            this.c.c.a.c.setLength(0);
        } else if (str2.compareTo("aid") == 0) {
            this.a = "aid";
            this.c.c.a.d.setLength(0);
        } else if (str2.compareTo("ay") == 0) {
            this.a = "ay";
            this.c.c.a.e.setLength(0);
        } else if (str2.compareTo("ad_url") == 0) {
            this.a = "ad_url";
            this.c.c.a.f.setLength(0);
        } else if (str2.compareTo("ad_link") == 0) {
            this.a = "ad_link";
            this.c.c.a.g.setLength(0);
        } else if (str2.compareTo("cr") == 0) {
            this.a = "cr";
            this.c.c.a.h.setLength(0);
        } else if (str2.compareTo("ad_height") == 0) {
            this.a = "ad_height";
            this.c.c.a.i.setLength(0);
        } else if (str2.compareTo("font_color") == 0) {
            this.a = "font_color";
            this.c.c.a.j.setLength(0);
        } else if (str2.compareTo("bg_color") == 0) {
            this.a = "bg_color";
            this.c.c.a.k.setLength(0);
        } else if (str2.compareTo("font_type") == 0) {
            this.a = "font_type";
            this.c.c.a.l.setLength(0);
        } else if (str2.compareTo("font_size") == 0) {
            this.a = "font_size";
            this.c.c.a.m.setLength(0);
        } else if (str2.compareTo("text_align") == 0) {
            this.a = "text_align";
            this.c.c.a.n.setLength(0);
        } else if (str2.compareTo("margin_span") == 0) {
            this.a = "margin_span";
            this.c.c.a.o.setLength(0);
        } else if (str2.compareTo("ad_icon") == 0) {
            this.a = "ad_icon";
            this.c.c.a.p.setLength(0);
        } else if (str2.compareTo("ad_text") == 0) {
            this.a = "ad_text";
            this.c.c.a.q.setLength(0);
        } else if (str2.compareTo("dsi") == 0) {
            this.a = "dsi";
            this.c.c.m.setLength(0);
        } else if (str2.compareTo("err_code") == 0) {
            this.a = "err_code";
            this.c.b.setLength(0);
        }
        this.b.setLength(0);
    }
}
