package com.neworld.demo;

import android.app.Activity;
import android.net.ConnectivityManager;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import com.adchina.android.ads.AdEngine;
import com.adchina.android.ads.AdListener;
import com.adchina.android.ads.views.AdView;
import com.adchina.android.ads.views.ShrinkFSAdView;
import com.izp.views.IZPDelegate;
import com.izp.views.IZPView;
import java.util.Timer;
import java.util.TimerTask;

public class MyAd {
    public static final int LEFT_BOTTOM = 1;
    public static final int LEFT_TOP = 0;
    public static final int RIGHT_BOTTOM = 3;
    public static final int RIGHT_TOP = 2;
    private Activity mActivity;
    private int mAdH = 48;
    private View[] mAdViews;
    private int mAdW = 320;
    private AdView mAdchina = null;
    /* access modifiers changed from: private */
    public int mClickCount = 0;
    /* access modifiers changed from: private */
    public View mCurrentAd;
    private int mCurrentShow = 0;
    private boolean mDebug = false;
    private IZPView mIZPAd = null;
    private int mInterval = 30;
    /* access modifiers changed from: private */
    public boolean mIsInit = false;
    private boolean mLandscape = false;
    /* access modifiers changed from: private */
    public long mLastTime = 0;
    /* access modifiers changed from: private */
    public RelativeLayout mLayout;
    /* access modifiers changed from: private */
    public int[] mLeftCount;
    View.OnClickListener mOnClickListener;
    private RelativeLayout.LayoutParams mParams;
    private int mPosition = 0;
    private int mScreenH;
    private int mScreenW;
    private Timer mTimer = null;
    private TimerTask mTimerTask;

    private static class ORDER {
        public static int ADCHN = 1;
        public static final int COUNT = 2;
        private static final String DEFAULT = "ADCHN:0:75740";
        public static int IZP = 0;

        private ORDER() {
        }
    }

    public MyAd(Activity activiy, int position, boolean isLandscape, RelativeLayout layout) {
        this.mActivity = activiy;
        this.mLeftCount = new int[2];
        this.mAdViews = new View[2];
        for (int i = 0; i < 2; i++) {
            this.mLeftCount[i] = 0;
            this.mAdViews[i] = null;
        }
        if (position < 0 || position > 3) {
            this.mPosition = 3;
        } else {
            this.mPosition = position;
        }
        this.mScreenW = this.mActivity.getWindowManager().getDefaultDisplay().getWidth();
        this.mScreenH = this.mActivity.getWindowManager().getDefaultDisplay().getHeight();
        if (layout != null) {
            this.mLayout = layout;
        } else {
            this.mLayout = new RelativeLayout(this.mActivity);
            this.mLayout.setEnabled(false);
            this.mLayout.setBackgroundColor(0);
            this.mActivity.addContentView(this.mLayout, new LinearLayout.LayoutParams(-1, -1));
        }
        this.mLandscape = isLandscape;
        if (this.mLandscape) {
            this.mAdH = this.mScreenH < this.mScreenW ? this.mScreenH : this.mScreenW;
            if (this.mAdH >= 480) {
                this.mAdH = 72;
                this.mAdW = 480;
            }
            this.mParams = new RelativeLayout.LayoutParams(this.mAdW, this.mAdH);
        } else {
            this.mAdH = this.mScreenH > this.mScreenW ? this.mScreenH : this.mScreenW;
            if (this.mAdH >= 800) {
                this.mAdH = 72;
            } else {
                this.mAdH = 48;
            }
            this.mParams = new RelativeLayout.LayoutParams(-1, this.mAdH);
        }
        this.mParams.setMargins(0, 0, 0, 0);
        switch (this.mPosition) {
            case 0:
                this.mParams.addRule(10);
                this.mParams.addRule(9);
                break;
            case 1:
                this.mParams.addRule(12);
                this.mParams.addRule(9);
                this.mLayout.setPadding(0, this.mScreenH - this.mAdH, 0, 0);
                break;
            case 2:
                this.mParams.addRule(10);
                this.mParams.addRule(11);
                break;
            default:
                this.mParams.addRule(12);
                this.mParams.addRule(11);
                this.mLayout.setPadding(this.mScreenW - this.mAdW, this.mScreenH - this.mAdH, 0, 0);
                break;
        }
        this.mOnClickListener = new View.OnClickListener() {
            public void onClick(View v) {
                MyAd myAd = MyAd.this;
                myAd.mClickCount = myAd.mClickCount + 1;
            }
        };
    }

    public void setDebug(boolean debug) {
        this.mDebug = debug;
    }

    public void setRefreshInterval(int interval) {
        this.mInterval = interval;
    }

    public void startAd() {
        if (this.mTimer == null) {
            this.mTimer = new Timer();
            this.mTimerTask = new TimerTask() {
                public void run() {
                    if (MyAd.this.mIsInit) {
                        if (MyAd.this.mLastTime == 0) {
                            MyAd.this.mLastTime = System.currentTimeMillis();
                        }
                        MyAd.this.showAdView();
                        return;
                    }
                    MyAd.this.initAdViews();
                }
            };
            this.mTimer.schedule(this.mTimerTask, 1000, (long) (this.mInterval >= 70 ? this.mInterval * 500 : 38000));
        }
    }

    /* access modifiers changed from: private */
    public void initAdViews() {
        if (!this.mIsInit && isConnected()) {
            String[] adParams = "ADCHN:0:76165".split(";");
            Log.d("AD", "line = " + "ADCHN:0:76165");
            for (String split : adParams) {
                initSingleAd(split.split(":"));
            }
            this.mIsInit = true;
        }
    }

    private void initSingleAd(final String[] params) {
        final String name = params[0];
        final int order = Integer.parseInt(params[1]);
        this.mActivity.runOnUiThread(new Runnable() {
            public void run() {
                if (name.equalsIgnoreCase("IZP")) {
                    Log.d("AD", "IZP: " + params[2]);
                    ORDER.IZP = order;
                    MyAd.this.initIZPAd(params[2]);
                } else if (name.equalsIgnoreCase("ADCHN")) {
                    Log.d("AD", "ADCHN: " + params[2]);
                    ORDER.ADCHN = order;
                    MyAd.this.initAdchina(params[2]);
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public void initAdchina(String appID) {
        if (appID != null) {
            this.mAdchina = new AdView(this.mActivity, appID, true, true);
            AdEngine.setAdListener(new AdListener() {
                public boolean OnRecvSms(AdView arg0, String arg1) {
                    return true;
                }

                public void onDisplayFullScreenAd() {
                }

                public void onFailedToPlayVideoAd() {
                }

                public void onFailedToReceiveAd(AdView arg0) {
                }

                public void onFailedToReceiveFullScreenAd() {
                }

                public void onFailedToReceiveShrinkFSAd(ShrinkFSAdView arg0) {
                }

                public void onFailedToReceiveVideoAd() {
                }

                public void onFailedToRefreshAd(AdView arg0) {
                }

                public void onPlayVideoAd() {
                }

                public void onReceiveFullScreenAd() {
                }

                public void onReceiveShrinkFSAd(ShrinkFSAdView arg0) {
                }

                public void onReceiveAd(AdView arg0) {
                    MyAd.this.mLeftCount[ORDER.ADCHN] = 2;
                }

                public void onRefreshAd(AdView arg0) {
                    MyAd.this.mLeftCount[ORDER.ADCHN] = 2;
                }

                public void onReceiveVideoAd() {
                }
            });
            this.mAdchina.setOnClickListener(this.mOnClickListener);
            this.mLayout.addView(this.mAdchina, this.mParams);
            this.mAdViews[ORDER.ADCHN] = this.mAdchina;
        }
    }

    /* access modifiers changed from: private */
    public void initIZPAd(String appID) {
        if (appID != null) {
            this.mIZPAd = new IZPView(this.mActivity);
            this.mIZPAd.productID = appID;
            this.mIZPAd.adType = "1";
            this.mIZPAd.isDev = this.mDebug;
            this.mIZPAd.delegate = new IZPDelegate() {
                public void didReceiveFreshAd(IZPView arg0, int arg1) {
                    MyAd.this.mLeftCount[ORDER.IZP] = 2;
                }

                public boolean shouldRequestFreshAd(IZPView arg0) {
                    return true;
                }

                public boolean shouldShowFreshAd(IZPView arg0) {
                    return true;
                }

                public void didStopFullScreenAd(IZPView arg0) {
                }

                public void errorReport(IZPView arg0, int arg1, String arg2) {
                }

                public void willLeaveApplication(IZPView arg0) {
                }

                public void didShowFreshAd(IZPView arg0) {
                }
            };
            this.mIZPAd.setOnClickListener(this.mOnClickListener);
            this.mLayout.addView(this.mIZPAd, this.mParams);
            this.mAdViews[ORDER.IZP] = this.mIZPAd;
            this.mIZPAd.startAdExchange();
        }
    }

    public void showAdView() {
        if (this.mLastTime + ((long) (this.mInterval * 500)) >= SystemClock.elapsedRealtime()) {
            int i = 1;
            while (i < 6) {
                int j = (this.mCurrentShow + i) % 2;
                if (this.mAdViews[j] == null || this.mLeftCount[j] <= 0) {
                    i++;
                } else {
                    showAdView(j);
                    return;
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void hideAds() {
        for (int i = 0; i < 2; i++) {
            if (this.mAdViews[i] != null) {
                this.mAdViews[i].setVisibility(8);
            }
        }
    }

    public void showAdView(int index) {
        int[] iArr = this.mLeftCount;
        iArr[index] = iArr[index] - 1;
        this.mLastTime = System.currentTimeMillis();
        this.mCurrentShow = index;
        this.mCurrentAd = this.mAdViews[index];
        this.mActivity.runOnUiThread(new Runnable() {
            public void run() {
                MyAd.this.hideAds();
                if (MyAd.this.isConnected()) {
                    MyAd.this.mLayout.bringToFront();
                    if (MyAd.this.mCurrentAd != null) {
                        MyAd.this.mCurrentAd.setVisibility(0);
                        MyAd.this.mCurrentAd.bringToFront();
                    }
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public boolean isConnected() {
        ConnectivityManager cmd = (ConnectivityManager) this.mActivity.getSystemService("connectivity");
        if (cmd.getNetworkInfo(1).isConnected()) {
            return true;
        }
        if (cmd.getNetworkInfo(0).isConnected()) {
            return true;
        }
        return false;
    }
}
