package com.imadpush.ad.poster;

import android.os.Handler;
import android.os.Message;
import android.widget.Toast;

class h extends Handler {
    final /* synthetic */ PosterInfoActivity a;

    h(PosterInfoActivity posterInfoActivity) {
        this.a = posterInfoActivity;
    }

    public void handleMessage(Message message) {
        super.handleMessage(message);
        switch (message.what) {
            case 0:
                Toast.makeText(this.a, "网络链接出现问题,请重试.", 1).show();
                break;
            case 1:
                Toast.makeText(this.a, "下载完成", 1).show();
                break;
            case 2:
                Toast.makeText(this.a, "内存卡空间不够或内存卡不存在,请插入内存卡后重试.", 1).show();
                break;
            case 3:
                Toast.makeText(this.a, "读写文件失败,请确保内存卡可以用.", 1).show();
                break;
        }
        this.a.d.b.setEnabled(true);
    }
}
