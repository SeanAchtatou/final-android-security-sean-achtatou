package com.imadpush.ad.poster;

import android.app.PendingIntent;
import android.content.Intent;
import android.os.AsyncTask;
import com.imadpush.ad.a.a;
import com.imadpush.ad.util.b;
import com.imadpush.ad.util.c;
import com.imadpush.ad.util.n;
import com.imadpush.ad.util.p;
import java.io.Serializable;
import java.util.List;
import org.apache.http.message.BasicNameValuePair;

public class d extends AsyncTask {
    /* access modifiers changed from: package-private */
    public final /* synthetic */ AlarmService a;

    public d(AlarmService alarmService) {
        this.a = alarmService;
    }

    private void a(a aVar, Intent intent, int i) {
        String a2 = aVar.a();
        String b = aVar.b();
        AlarmService.b.tickerText = a2;
        if (b.equals("null")) {
            b = "";
        }
        AlarmService.b.setLatestEventInfo(this.a.c, a2, b, PendingIntent.getActivity(this.a.c, i, intent, 134217728));
        AlarmService.a.notify(i, AlarmService.b);
    }

    private void a(List list) {
        for (int i = 0; i < list.size(); i++) {
            this.a.e = ((a) list.get(i)).k();
            Intent intent = new Intent(this.a.c, PosterInfoActivity.class);
            intent.putExtra("notifyId", 1);
            intent.putExtra("userId", this.a.d);
            intent.putExtra("dId", this.a.f);
            intent.putExtra("push", (Serializable) list.get(i));
            a((a) list.get(i), intent, 1);
            new a(this).execute(null, 0, null);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public String doInBackground(Object... objArr) {
        if (this.a.f.longValue() == 0) {
            this.a.f = b.b(this.a.c);
        }
        List a2 = n.a(this.a.c);
        a2.add(new BasicNameValuePair("rq_poster", "1"));
        a2.add(new BasicNameValuePair("user_detal_info", "1"));
        a2.add(new BasicNameValuePair("dId", String.valueOf(this.a.f)));
        a2.add(new BasicNameValuePair("version", "V1.1"));
        a2.add(new BasicNameValuePair("location", new c(this.a.c).a));
        if (this.a.d.longValue() == 0) {
            p.a("用户Id不存在");
            AppPosterManager.f = com.imadpush.ad.poster.a.a.a(n.a("user/register/register", a2));
            AppPosterManager.d.edit().putLong("userId", AppPosterManager.f.a());
            this.a.d = Long.valueOf(AppPosterManager.f.a());
        }
        String a3 = n.a("AppPoster/mgPush/getPush", a2);
        p.a("获取用户Id:" + this.a.d);
        p.a("获取推送广告信息:" + a3);
        if (a3 == null || a3.equals("")) {
            return null;
        }
        a(com.imadpush.ad.poster.a.a.b(a3));
        return null;
    }
}
