package com.imadpush.ad.poster.a;

import com.imadpush.ad.a.b;
import com.imadpush.ad.util.p;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class a {
    public static b a(String str) {
        JSONObject a = b.a(str);
        if (a == null || a.optInt("STATUS") != 1) {
            return null;
        }
        b bVar = new b();
        bVar.a(a.optDouble("JM_MONEY"));
        bVar.c(a.optString("PET_NAME"));
        JSONObject optJSONObject = a.optJSONObject("USER_INFO");
        if (optJSONObject != null) {
            bVar.a(optJSONObject.optString("USER_NAME"));
            bVar.a(optJSONObject.optLong("USER_ID"));
            bVar.b(optJSONObject.optString("USER_UNI"));
        }
        return bVar;
    }

    public static List b(String str) {
        JSONObject jSONObject;
        JSONArray b = b.b(str);
        if (b == null) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        for (int i = 0; i < b.length(); i++) {
            try {
                jSONObject = b.getJSONObject(i);
            } catch (JSONException e) {
                e.printStackTrace();
                p.c("JSONObject获取数据失败->ConvertToObject->toAppInfoList");
                jSONObject = null;
            }
            if (jSONObject != null) {
                com.imadpush.ad.a.a aVar = new com.imadpush.ad.a.a();
                aVar.a(Long.valueOf(jSONObject.optLong("id")));
                aVar.a(jSONObject.optString("name"));
                aVar.b(jSONObject.optString("description"));
                aVar.i(jSONObject.optString("content"));
                aVar.d(jSONObject.optString("img_url"));
                aVar.e(jSONObject.optString("imgs_url1"));
                aVar.f(jSONObject.optString("imgs_url2"));
                aVar.g(jSONObject.optString("imgs_url3"));
                aVar.h(jSONObject.optString("file_url"));
                aVar.c(jSONObject.optString("net_url"));
                aVar.j(jSONObject.optString("play_url"));
                aVar.d(jSONObject.optInt("type"));
                aVar.c(jSONObject.optInt("point"));
                aVar.b(jSONObject.optInt("file_size"));
                aVar.a(jSONObject.optInt("condition"));
                arrayList.add(aVar);
            }
        }
        return arrayList;
    }
}
