package com.imadpush.ad.poster;

import android.os.AsyncTask;
import com.imadpush.ad.util.n;
import com.imadpush.ad.util.p;
import java.util.ArrayList;
import org.apache.http.message.BasicNameValuePair;

public class e extends AsyncTask {
    final /* synthetic */ PosterInfoActivity a;

    public e(PosterInfoActivity posterInfoActivity) {
        this.a = posterInfoActivity;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public String doInBackground(Object... objArr) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("psid", String.valueOf(this.a.r)));
        arrayList.add(new BasicNameValuePair("uid", String.valueOf(this.a.s)));
        arrayList.add(new BasicNameValuePair("dId", String.valueOf(this.a.t)));
        String a2 = n.a("AppPoster/mgPlookLog/look", arrayList);
        p.a("成功后返回的信息:" + a2);
        return a2;
    }
}
