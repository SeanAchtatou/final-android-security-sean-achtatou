package com.imadpush.ad.poster;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class ReceiverAlarm extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        context.startService(new Intent(context, AlarmService.class));
    }
}
