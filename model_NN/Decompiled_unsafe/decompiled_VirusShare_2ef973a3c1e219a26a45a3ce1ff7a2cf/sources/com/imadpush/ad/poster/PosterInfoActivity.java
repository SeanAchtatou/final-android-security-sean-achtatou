package com.imadpush.ad.poster;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.widget.ImageView;
import android.widget.ProgressBar;
import com.imadpush.ad.a.a;
import com.imadpush.ad.util.MyLayout;
import com.imadpush.ad.util.f;
import com.imadpush.ad.util.m;
import com.imadpush.ad.util.o;
import com.imadpush.ad.util.p;
import java.io.File;
import java.io.IOException;

public class PosterInfoActivity extends Activity {
    public int a;
    public Context b = this;
    public PackageReceiver c;
    public MyLayout d;
    private Intent e;
    /* access modifiers changed from: private */
    public a f;
    private String g;
    private String h;
    private String i;
    private String j;
    private String k;
    private String l;
    /* access modifiers changed from: private */
    public String m;
    /* access modifiers changed from: private */
    public String n;
    /* access modifiers changed from: private */
    public Handler o;
    /* access modifiers changed from: private */
    public NotificationManager p;
    private Notification q;
    /* access modifiers changed from: private */
    public Long r;
    /* access modifiers changed from: private */
    public Long s;
    /* access modifiers changed from: private */
    public Long t;
    /* access modifiers changed from: private */
    public File u;
    /* access modifiers changed from: private */
    public boolean v = false;
    /* access modifiers changed from: private */
    public ProgressBar w;
    private String[] x;
    private String[] y;

    public class PackageReceiver extends BroadcastReceiver {
        public PackageReceiver() {
        }

        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals("android.intent.action.PACKAGE_ADDED")) {
                Intent launchIntentForPackage = context.getPackageManager().getLaunchIntentForPackage(intent.getDataString().substring(8));
                if (launchIntentForPackage != null) {
                    PosterInfoActivity.this.startActivity(launchIntentForPackage);
                }
                PosterInfoActivity.this.p.cancel(1);
                new b(PosterInfoActivity.this).execute(null, 0, null);
                if (PosterInfoActivity.this.v) {
                    PosterInfoActivity.this.finish();
                }
            }
        }
    }

    public void a() {
        if (o.a(this.b)) {
            new Thread(new f(this, this.o, this.f, this.w, this.s, this.t, this.n, this.d.b)).start();
        } else {
            showDialog(1);
        }
    }

    public void a(ImageView imageView, String str) {
        if (!com.imadpush.ad.util.a.c(str)) {
            imageView.setTag(str);
            try {
                new m().execute(imageView);
                imageView.setDrawingCacheEnabled(true);
            } catch (IllegalStateException e2) {
                e2.printStackTrace();
                p.c("异步任务加载失败->BookListAdapter");
            }
        } else {
            imageView.setImageBitmap(com.imadpush.ad.util.a.a(str));
        }
    }

    public boolean a(String str) {
        String[] split = str.split("/");
        return !split[split.length - 1].equals("images");
    }

    public Bitmap b(String str) {
        try {
            return BitmapFactory.decodeStream(getAssets().open(str));
        } catch (IOException e2) {
            e2.printStackTrace();
            return null;
        }
    }

    public void b() {
        if (this.a == 0) {
            p.a("检测到使用wifi下载");
            if (o.a(this.b)) {
                new Thread(new f(this, this.o, this.f, this.w, this.s, this.t, this.n, this.d.b)).start();
                return;
            }
            return;
        }
        p.a("使用直接下载");
        new Thread(new f(this, this.o, this.f, this.w, this.s, this.t, this.n, this.d.b)).start();
    }

    public void c() {
        this.p = (NotificationManager) getSystemService("notification");
        this.q = new Notification();
        this.q.icon = 17301545;
        this.q.when = System.currentTimeMillis();
        this.q.setLatestEventInfo(this, this.f.a(), this.i, PendingIntent.getActivity(this, 0, this.e, 134217728));
        this.p.notify(1, this.q);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.d = new MyLayout(this, null);
        this.w = new ProgressBar(this);
        this.p = AlarmService.a;
        this.q = AlarmService.b;
        this.e = getIntent();
        this.s = Long.valueOf(this.e.getExtras().getLong("userId"));
        this.t = Long.valueOf(this.e.getExtras().getLong("dId"));
        this.f = (a) this.e.getExtras().getSerializable("push");
        this.g = this.f.e();
        this.h = this.f.l();
        this.i = this.f.b();
        this.j = this.f.f();
        this.k = this.f.g();
        this.l = this.f.h();
        this.a = this.f.c();
        this.m = this.f.d();
        this.r = this.f.k();
        this.x = this.f.i().split("/");
        this.y = this.f.m().split("/");
        if (this.f.j() == 1) {
            this.n = this.x[this.x.length - 1];
        } else if (this.f.j() == 2) {
            this.n = this.y[this.y.length - 1];
        }
        this.c = new PackageReceiver();
        IntentFilter intentFilter = new IntentFilter("android.intent.action.PACKAGE_ADDED");
        intentFilter.addDataScheme("package");
        registerReceiver(this.c, intentFilter);
        this.u = com.imadpush.ad.util.a.f(this.n);
        if (this.u == null || this.f.j() != 1) {
            setContentView(this.d);
            this.d.c.setImageBitmap(b("default_icon.png"));
            if (a(this.g)) {
                a(this.d.c, this.g);
            }
            if (a(this.j)) {
                a(this.d.d, this.j);
                this.d.d.setVisibility(0);
            }
            if (a(this.k)) {
                a(this.d.e, this.k);
                this.d.e.setVisibility(0);
            }
            if (a(this.l)) {
                a(this.d.f, this.l);
                this.d.f.setVisibility(0);
            }
            if (this.i.equals("null") || this.i.length() == 0) {
                this.d.g.setText("");
            } else {
                this.d.g.setText(this.i);
            }
            if (this.i.equals("null") || this.h.length() == 0) {
                this.d.h.setText("");
            } else {
                this.d.h.setText(this.h);
            }
            this.o = new h(this);
            if (this.f.j() == 0) {
                this.d.b.setText("打开");
            } else if (this.f.j() == 1) {
                b();
            } else if (this.f.j() == 2) {
                this.d.b.setText("播放");
                if (this.u == null) {
                    b();
                }
            }
            this.d.a.setOnClickListener(new g(this));
            this.d.b.setOnClickListener(new f(this));
            new e(this).execute(null, 0, null);
            return;
        }
        this.v = true;
        new e(this).execute(null, 0, null);
        com.imadpush.ad.util.a.a(this, this.u);
        this.p.cancel(1);
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int i2) {
        return new AlertDialog.Builder(this).setIcon(17301659).setTitle("网络提示").setMessage("你使用的不是WIFI,是否继续下载！").setNegativeButton("取消", new i(this)).setPositiveButton("确定", new j(this)).create();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        unregisterReceiver(this.c);
        super.onDestroy();
    }
}
