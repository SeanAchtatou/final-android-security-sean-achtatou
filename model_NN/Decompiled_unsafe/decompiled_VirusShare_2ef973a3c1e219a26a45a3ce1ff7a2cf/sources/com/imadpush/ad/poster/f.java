package com.imadpush.ad.poster;

import android.content.Intent;
import android.net.Uri;
import android.view.View;
import com.imadpush.ad.util.a;
import com.imadpush.ad.util.e;
import com.imadpush.ad.util.p;

class f implements View.OnClickListener {
    final /* synthetic */ PosterInfoActivity a;

    f(PosterInfoActivity posterInfoActivity) {
        this.a = posterInfoActivity;
    }

    public void onClick(View view) {
        if (this.a.f.j() == 0) {
            p.c(this.a.m);
            Intent intent = new Intent();
            intent.setAction("android.intent.action.VIEW");
            intent.setData(Uri.parse(this.a.m));
            this.a.startActivity(intent);
        } else if (this.a.f.j() == 1) {
            this.a.u = a.f(this.a.n);
            if (this.a.u != null) {
                a.a(this.a, this.a.u);
            } else {
                this.a.a();
            }
        } else if (this.a.f.j() == 2) {
            this.a.u = a.f(this.a.n);
            if (this.a.u != null) {
                Uri parse = Uri.parse(String.valueOf(e.b) + "/" + this.a.n);
                Intent intent2 = new Intent();
                intent2.setAction("android.intent.action.VIEW");
                intent2.setDataAndType(parse, "video/*");
                this.a.startActivity(intent2);
                return;
            }
            p.c("本地没有该视频开始下载");
            this.a.a();
        }
    }
}
