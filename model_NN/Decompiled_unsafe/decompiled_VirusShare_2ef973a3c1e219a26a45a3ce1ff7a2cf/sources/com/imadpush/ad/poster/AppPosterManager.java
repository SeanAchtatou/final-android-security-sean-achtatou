package com.imadpush.ad.poster;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.SharedPreferences;
import com.imadpush.ad.a.b;
import com.imadpush.ad.util.p;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class AppPosterManager {
    public static Activity a;
    public static int b = 0;
    public static AlarmManager c;
    public static SharedPreferences d;
    public static PendingIntent e;
    public static b f;
    public Long g = 123456L;

    public AppPosterManager(Activity activity) {
        b(activity, false);
    }

    private static void b() {
        e = PendingIntent.getBroadcast(a, 0, new Intent(a, ReceiverAlarm.class), 0);
        Calendar instance = Calendar.getInstance();
        instance.setTimeInMillis(System.currentTimeMillis());
        p.a(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(instance.getTimeInMillis())));
        c = (AlarmManager) a.getSystemService("alarm");
        c.setRepeating(0, instance.getTimeInMillis() + 30000, 900000, e);
    }

    private void b(Activity activity, boolean z) {
        a(activity, z);
        a();
    }

    public void a() {
        b();
    }

    public void a(Activity activity, boolean z) {
        p.a = z;
        a = activity;
        this.g = com.imadpush.ad.util.b.b(activity);
        d = activity.getSharedPreferences("jmuser", 0);
        d.edit().putLong("dId", this.g.longValue()).commit();
        new c(this).execute(null, 0, null);
    }
}
