package com.imadpush.ad.poster;

import android.os.AsyncTask;
import com.imadpush.ad.poster.a.a;
import com.imadpush.ad.util.n;
import com.imadpush.ad.util.o;
import com.imadpush.ad.util.p;
import java.util.List;
import org.apache.http.message.BasicNameValuePair;

public class c extends AsyncTask {
    final /* synthetic */ AppPosterManager a;

    public c(AppPosterManager appPosterManager) {
        this.a = appPosterManager;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public String doInBackground(Object... objArr) {
        List a2 = n.a(AppPosterManager.a);
        a2.add(new BasicNameValuePair("IMEI", o.b(AppPosterManager.a)));
        a2.add(new BasicNameValuePair("login_way", "1"));
        a2.add(new BasicNameValuePair("user_detal_info", "1"));
        String a3 = n.a("user/register/register", a2);
        p.a("获取快速登录的用户信息:" + a3);
        if (a3 == null || a3.equals("")) {
            return null;
        }
        AppPosterManager.f = a.a(a3);
        AppPosterManager.d.edit().putLong("userId", AppPosterManager.f.a()).commit();
        return null;
    }
}
