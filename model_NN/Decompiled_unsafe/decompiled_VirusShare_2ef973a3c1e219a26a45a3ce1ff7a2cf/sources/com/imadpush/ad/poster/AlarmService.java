package com.imadpush.ad.poster;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

public class AlarmService extends Service {
    public static NotificationManager a;
    public static Notification b;
    /* access modifiers changed from: private */
    public Activity c;
    /* access modifiers changed from: private */
    public Long d;
    /* access modifiers changed from: private */
    public Long e;
    /* access modifiers changed from: private */
    public Long f;

    public void a() {
        a = (NotificationManager) getSystemService("notification");
        b = new Notification();
        b.icon = 17301545;
        b.defaults |= 1;
        b.defaults |= 2;
        b.flags |= 2;
        b.flags |= 32;
        b.when = System.currentTimeMillis();
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onCreate() {
        super.onCreate();
    }

    public void onStart(Intent intent, int i) {
        super.onStart(intent, i);
        this.c = AppPosterManager.a;
        this.d = Long.valueOf(AppPosterManager.d.getLong("userId", 0));
        this.f = Long.valueOf(AppPosterManager.d.getLong("dId", 0));
        a();
        new d(this).execute(null, 0, null);
    }
}
