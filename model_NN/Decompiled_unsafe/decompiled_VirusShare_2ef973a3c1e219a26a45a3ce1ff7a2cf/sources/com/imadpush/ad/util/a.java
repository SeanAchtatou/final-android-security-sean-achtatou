package com.imadpush.ad.util;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.ref.SoftReference;
import java.util.HashMap;
import java.util.Map;

public class a {
    public static Map a = new HashMap();

    public static Bitmap a(String str) {
        p.a(str);
        String[] split = str.split("/");
        if (split[split.length - 1] == null) {
            return null;
        }
        Bitmap b = b(split[split.length - 1]);
        if (!a.containsKey(split[split.length - 1]) || b == null) {
            File file = new File(String.valueOf(e.b) + "/image/" + split[split.length - 1]);
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inTempStorage = new byte[16384];
            options.inSampleSize = 1;
            try {
                b = BitmapFactory.decodeStream(new FileInputStream(file), null, options);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            a(split[split.length - 1], b);
            p.a("缓存不存在 新建");
        }
        return b;
    }

    public static void a(Activity activity, File file) {
        Intent intent = new Intent();
        intent.addFlags(268435456);
        intent.setAction("android.intent.action.VIEW");
        intent.setDataAndType(Uri.fromFile(file), "application/vnd.android.package-archive");
        activity.startActivity(intent);
    }

    public static void a(String str, Bitmap bitmap) {
        a.put(str, new SoftReference(bitmap));
    }

    public static boolean a(Bitmap bitmap, String str) {
        if (o.a()) {
            String[] split = str.split("/");
            if (split[split.length - 1] != null) {
                File file = new File(String.valueOf(e.b) + "/image/" + split[split.length - 1]);
                if (!file.getParentFile().exists()) {
                    file.getParentFile().mkdirs();
                }
                if (!file.getParentFile().exists() || file.exists()) {
                    return true;
                }
                try {
                    file.createNewFile();
                    BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(new FileOutputStream(file));
                    bitmap.compress(Bitmap.CompressFormat.PNG, 100, bufferedOutputStream);
                    bufferedOutputStream.flush();
                    bufferedOutputStream.close();
                    return true;
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                    return false;
                } catch (IOException e2) {
                    e2.printStackTrace();
                    return false;
                }
            }
        }
        return false;
    }

    public static Bitmap b(String str) {
        SoftReference softReference = (SoftReference) a.get(str);
        if (softReference == null || softReference.get() == null) {
            return null;
        }
        return (Bitmap) softReference.get();
    }

    public static boolean c(String str) {
        if (o.a() && !str.equals("")) {
            String[] split = str.split("/");
            if (split[split.length - 1] != null && new File(String.valueOf(e.b) + "/image/" + split[split.length - 1]).exists()) {
                return true;
            }
        }
        return false;
    }

    public static File d(String str) {
        if (!o.a()) {
            return null;
        }
        File file = new File(str);
        if (!file.exists()) {
            file.delete();
        }
        try {
            file.createNewFile();
            return file;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static File e(String str) {
        String str2 = String.valueOf(e.b) + "/" + str;
        p.a("-----" + str2);
        File d = d(str2);
        if (d != null) {
            return d;
        }
        try {
            return File.createTempFile("temp_" + str, "apk");
        } catch (IOException e) {
            e.printStackTrace();
            return d;
        }
    }

    public static File f(String str) {
        File file = new File(String.valueOf(e.b) + "/" + str);
        if (file.exists()) {
            return file;
        }
        return null;
    }
}
