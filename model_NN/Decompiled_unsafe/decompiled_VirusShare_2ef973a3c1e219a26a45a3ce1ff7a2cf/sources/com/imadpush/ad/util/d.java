package com.imadpush.ad.util;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.MessageFormat;

public class d {
    public static String a(String str) {
        String format = MessageFormat.format("http://{0}/{1}", "ad.imadpush.com:7500/AppManager/index.php", str);
        try {
            HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(format).openConnection();
            httpURLConnection.setConnectTimeout(5000);
            httpURLConnection.connect();
            p.b(format);
            return format;
        } catch (MalformedURLException e) {
            p.b("创建网络连接失败");
            e.printStackTrace();
            p.b(format);
            return null;
        } catch (IOException e2) {
            p.b("网络连接IO异常");
            e2.printStackTrace();
            p.b(format);
            return null;
        } catch (Throwable th) {
            p.b(format);
            throw th;
        }
    }
}
