package com.imadpush.ad.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.os.Environment;
import android.provider.Settings;
import android.telephony.TelephonyManager;

public class o {
    private static String a = null;
    private static String b = null;
    private static String c = null;

    public static boolean a() {
        return Environment.getExternalStorageState().equals("mounted");
    }

    public static boolean a(Context context) {
        return ((ConnectivityManager) context.getSystemService("connectivity")).getNetworkInfo(1).isAvailable();
    }

    public static String b(Context context) {
        if (b == null) {
            b = ((TelephonyManager) context.getSystemService("phone")).getDeviceId();
        }
        return b == null ? Settings.System.getString(context.getContentResolver(), "android_id") : b;
    }
}
