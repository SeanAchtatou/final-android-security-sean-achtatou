package com.imadpush.ad.util;

import android.app.Activity;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;

public class b {
    public static String a(Activity activity) {
        return activity.getPackageName();
    }

    public static String a(Activity activity, String str) {
        try {
            PackageInfo packageInfo = activity.getPackageManager().getPackageInfo(str, 16384);
            p.c("getVersionName:" + packageInfo.versionName);
            return packageInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            p.b("获取包" + str + "的VersionName失败");
            e.printStackTrace();
            return "";
        }
    }

    public static int b(Activity activity, String str) {
        try {
            PackageInfo packageInfo = activity.getPackageManager().getPackageInfo(str, 16384);
            p.c("getVersionCode:" + packageInfo.versionCode);
            return packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            p.b("获取包" + str + "的VersionCode失败");
            e.printStackTrace();
            return 0;
        }
    }

    public static Long b(Activity activity) {
        long j = 123456L;
        try {
            Object obj = activity.getPackageManager().getApplicationInfo(activity.getPackageName(), 128).metaData.get("com.imadpush.ad");
            if (obj != null) {
                j = Long.valueOf(Long.parseLong(obj.toString()));
            }
        } catch (Exception e) {
            e.printStackTrace();
            j = 123456L;
        }
        p.c("getChannel:" + j);
        return j;
    }
}
