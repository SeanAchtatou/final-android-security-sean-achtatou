package com.imadpush.ad.util;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.widget.ImageView;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class m extends AsyncTask {
    private ImageView a;
    private Bitmap b;

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Bitmap doInBackground(ImageView... imageViewArr) {
        int i = 0;
        this.a = imageViewArr[0];
        if (this.a.getTag() != null) {
            p.a(this.a.getTag().toString());
            try {
                HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(this.a.getTag().toString()).openConnection();
                httpURLConnection.setDoInput(true);
                httpURLConnection.setConnectTimeout(5000);
                httpURLConnection.connect();
                InputStream inputStream = httpURLConnection.getInputStream();
                int contentLength = httpURLConnection.getContentLength();
                if (contentLength != -1) {
                    byte[] bArr = new byte[512];
                    byte[] bArr2 = new byte[contentLength];
                    while (true) {
                        int read = inputStream.read(bArr);
                        if (read <= 0) {
                            break;
                        }
                        System.arraycopy(bArr, 0, bArr2, i, read);
                        i += read;
                    }
                    this.b = BitmapFactory.decodeByteArray(bArr2, 0, bArr2.length);
                    a.a(this.b, this.a.getTag().toString());
                }
                inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
                p.c("获取网络图片异常->ImageAsyncTask");
                return null;
            }
        }
        return this.b;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void onPostExecute(Bitmap bitmap) {
        if (this.b != null) {
            this.a.setImageBitmap(this.b);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void onProgressUpdate(Void... voidArr) {
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
    }
}
