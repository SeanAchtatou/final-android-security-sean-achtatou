package com.imadpush.ad.util;

import android.app.Activity;
import android.telephony.TelephonyManager;
import android.telephony.gsm.GsmCellLocation;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

public class c {
    public String a;
    private Activity b;

    public c(Activity activity) {
        this.b = activity;
        try {
            this.a = a(a(a()));
            p.a("获得物理位置：" + this.a);
        } catch (Exception e) {
            p.c("Error" + e.getMessage());
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{org.json.JSONObject.put(java.lang.String, double):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, long):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, int):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, java.lang.Object):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException} */
    private g a(l lVar) {
        g gVar = new g(this);
        DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
        HttpPost httpPost = new HttpPost("http://www.google.com/loc/json");
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("version", "1.1.0");
            jSONObject.put("host", "maps.google.com");
            jSONObject.put("address_language", "zh_CN");
            jSONObject.put("request_address", true);
            jSONObject.put("radio_type", "gsm");
            jSONObject.put("carrier", "HTC");
            JSONObject jSONObject2 = new JSONObject();
            jSONObject2.put("mobile_country_code", lVar.a);
            jSONObject2.put("mobile_network_code", lVar.b);
            jSONObject2.put("cell_id", lVar.d);
            jSONObject2.put("location_area_code", lVar.c);
            JSONArray jSONArray = new JSONArray();
            jSONArray.put(jSONObject2);
            jSONObject.put("cell_towers", jSONArray);
            httpPost.setEntity(new StringEntity(jSONObject.toString()));
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(defaultHttpClient.execute(httpPost).getEntity().getContent()));
            StringBuffer stringBuffer = new StringBuffer();
            while (true) {
                String readLine = bufferedReader.readLine();
                if (readLine == null) {
                    JSONObject jSONObject3 = new JSONObject(new JSONObject(stringBuffer.toString()).getString("location"));
                    gVar.a = jSONObject3.getString("latitude");
                    gVar.b = jSONObject3.getString("longitude");
                    p.a("latitude: " + gVar.a + "---longitude: " + gVar.b);
                    httpPost.abort();
                    return gVar;
                }
                stringBuffer.append(readLine);
            }
        } catch (Exception e) {
            p.c(e.getMessage());
            throw new Exception("获取经纬度出现错误:" + e.getMessage());
        } catch (Throwable th) {
            httpPost.abort();
            throw th;
        }
    }

    private l a() {
        l lVar = new l(this);
        TelephonyManager telephonyManager = (TelephonyManager) this.b.getSystemService("phone");
        GsmCellLocation gsmCellLocation = (GsmCellLocation) telephonyManager.getCellLocation();
        if (gsmCellLocation == null) {
            throw new Exception("获取基站信息失败");
        }
        String networkOperator = telephonyManager.getNetworkOperator();
        int parseInt = Integer.parseInt(networkOperator.substring(0, 3));
        int parseInt2 = Integer.parseInt(networkOperator.substring(3));
        int cid = gsmCellLocation.getCid();
        int lac = gsmCellLocation.getLac();
        lVar.a = parseInt;
        lVar.b = parseInt2;
        lVar.c = lac;
        lVar.d = cid;
        return lVar;
    }

    private String a(g gVar) {
        String format = String.format("http://maps.google.cn/maps/geo?key=abcdefg&q=%s,%s", gVar.a, gVar.b);
        p.a("URL:" + format);
        DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
        HttpGet httpGet = new HttpGet(format);
        try {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(defaultHttpClient.execute(httpGet).getEntity().getContent()));
            StringBuffer stringBuffer = new StringBuffer();
            while (true) {
                String readLine = bufferedReader.readLine();
                if (readLine == null) {
                    break;
                }
                stringBuffer.append(readLine);
            }
            String stringBuffer2 = stringBuffer.toString();
            if (stringBuffer2 != null && stringBuffer2.length() > 0) {
                JSONArray jSONArray = new JSONArray(new JSONObject(stringBuffer2).get("Placemark").toString());
                String str = "";
                for (int i = 0; i < jSONArray.length(); i++) {
                    str = jSONArray.getJSONObject(i).getString("address");
                }
                stringBuffer2 = str;
            }
            httpGet.abort();
            return stringBuffer2;
        } catch (Exception e) {
            throw new Exception("获取物理位置出现错误:" + e.getMessage());
        } catch (Throwable th) {
            httpGet.abort();
            throw th;
        }
    }
}
