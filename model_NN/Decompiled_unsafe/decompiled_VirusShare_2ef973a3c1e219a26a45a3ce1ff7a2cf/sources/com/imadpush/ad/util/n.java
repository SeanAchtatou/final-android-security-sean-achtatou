package com.imadpush.ad.util;

import android.app.Activity;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

public class n {
    private static List a;

    public static String a(String str, List list) {
        String a2 = d.a(str);
        if (a2 != null) {
            p.a(a2);
        }
        if (a2 != null) {
            HttpPost httpPost = new HttpPost(a2);
            try {
                httpPost.setEntity(new UrlEncodedFormEntity(list, "UTF-8"));
                HttpResponse execute = new DefaultHttpClient().execute(httpPost);
                if (execute.getStatusLine().getStatusCode() == 200) {
                    return EntityUtils.toString(execute.getEntity(), "UTF-8");
                }
                p.c("connect server fail statuscode=" + execute.getStatusLine().getStatusCode());
            } catch (UnsupportedEncodingException e) {
                p.c("HttpUtil 不支持的编码异常");
                e.printStackTrace();
            } catch (ClientProtocolException e2) {
                p.c("HttpUtil 客户端协议异常");
                e2.printStackTrace();
            } catch (IOException e3) {
                p.c("HttpUtil 输入输出异常");
                e3.printStackTrace();
            }
        }
        return "";
    }

    public static List a(Activity activity) {
        if (a == null) {
            a = new ArrayList();
            a.add(new BasicNameValuePair("imei", o.b(activity)));
            a.add(new BasicNameValuePair("packagename", b.a(activity)));
            a.add(new BasicNameValuePair("versionname", b.a(activity, b.a(activity))));
            a.add(new BasicNameValuePair("versioncode", String.valueOf(b.b(activity, b.a(activity)))));
        }
        return a;
    }
}
