package com.imadpush.ad.util;

import android.app.Activity;
import android.os.Handler;
import android.os.Message;
import android.widget.Button;
import android.widget.ProgressBar;
import com.imadpush.ad.a.a;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class f implements Runnable {
    private Activity a;
    private Handler b;
    private a c;
    private boolean d = false;
    private byte[] e = new byte[0];
    /* access modifiers changed from: private */
    public ProgressBar f;
    private String g;
    private Message h;
    private Long i;
    private Long j;
    private String k;

    public f(Activity activity, Handler handler, a aVar, ProgressBar progressBar, Long l, Long l2, String str, Button button) {
        this.a = activity;
        this.b = handler;
        this.c = aVar;
        this.f = progressBar;
        this.i = l;
        this.j = l2;
        String str2 = "&userid=" + l;
        String str3 = "&dId=" + l2;
        this.k = str;
        button.setEnabled(false);
        if (aVar.j() == 1) {
            this.g = "http://ad.imadpush.com:7500/AppManager/index.php/AppPoster/mgPdownLog/downMore?appid=" + aVar.k() + str2 + "&imei=" + o.b(activity) + str3 + "&type=1";
        } else if (aVar.j() == 2) {
            this.g = "http://ad.imadpush.com:7500/AppManager/index.php/AppPoster/mgPdownLog/downMore?appid=" + aVar.k() + str2 + "&imei=" + o.b(activity) + str3 + "&type=2";
        }
        this.h = new Message();
        this.h.obj = progressBar;
    }

    public void run() {
        FileOutputStream fileOutputStream;
        HttpURLConnection httpURLConnection;
        int i2 = 0;
        this.b.post(new j(this));
        p.a("下载文件地址:" + this.g);
        byte[] bArr = new byte[1024];
        int i3 = 0;
        HttpURLConnection httpURLConnection2 = null;
        while (httpURLConnection2 == null && i3 < 10) {
            try {
                httpURLConnection = (HttpURLConnection) new URL(this.g).openConnection();
            } catch (MalformedURLException e2) {
                p.c("AppDownLoad MalformedURLException");
                e2.printStackTrace();
                httpURLConnection = httpURLConnection2;
            } catch (IOException e3) {
                p.c("AppDownLoad IOException");
                e3.printStackTrace();
                httpURLConnection = httpURLConnection2;
            }
            i3++;
            httpURLConnection2 = httpURLConnection;
        }
        if (httpURLConnection2 != null) {
            p.a("mConnection 连接成功");
            try {
                InputStream inputStream = httpURLConnection2.getInputStream();
                Integer valueOf = Integer.valueOf(Integer.parseInt(httpURLConnection2.getHeaderField("Content-Length")));
                if (valueOf != null) {
                    p.a("mTotalSize(获取的文件长度):" + valueOf);
                    this.b.post(new h(this, valueOf));
                    if (inputStream != null) {
                        InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                        File e4 = a.e(this.k);
                        if (e4 == null || inputStreamReader == null) {
                            p.c("处理获取文件以及新建文件失败");
                            if (httpURLConnection2 != null) {
                                httpURLConnection2.disconnect();
                            }
                            this.h.what = 3;
                            this.b.sendMessage(this.h);
                            return;
                        }
                        try {
                            fileOutputStream = new FileOutputStream(e4);
                        } catch (FileNotFoundException e5) {
                            p.c("下载文件未发现");
                            e5.printStackTrace();
                            fileOutputStream = null;
                        }
                        if (fileOutputStream != null) {
                            try {
                                synchronized (this.e) {
                                    while (true) {
                                        int read = inputStream.read(bArr);
                                        if (read != -1 && !this.d) {
                                            fileOutputStream.write(bArr, 0, read);
                                            i2 += read;
                                            this.b.post(new i(this, i2));
                                        }
                                    }
                                }
                                if (fileOutputStream != null) {
                                    fileOutputStream.flush();
                                    fileOutputStream.close();
                                }
                                if (inputStream != null) {
                                    inputStream.close();
                                }
                                this.b.postDelayed(new k(this), 500);
                                synchronized (this.e) {
                                    if (!this.d) {
                                        this.h.what = 1;
                                        this.b.sendMessage(this.h);
                                        if (this.c.j() == 1) {
                                            a.a(this.a, e4);
                                        }
                                    }
                                }
                            } catch (IOException e6) {
                                p.c("读写本地文件出现IO异常 IOException");
                                e6.printStackTrace();
                                if (inputStream != null) {
                                    try {
                                        fileOutputStream.flush();
                                        fileOutputStream.close();
                                        inputStream.close();
                                    } catch (IOException e7) {
                                        e7.printStackTrace();
                                    }
                                    if (httpURLConnection2 != null) {
                                        httpURLConnection2.disconnect();
                                    }
                                }
                                this.h.what = 3;
                                this.b.sendMessage(this.h);
                            }
                        } else {
                            p.c("处理获取本地流的文件失败");
                            if (httpURLConnection2 != null) {
                                httpURLConnection2.disconnect();
                            }
                            this.h.what = 3;
                            this.b.sendMessage(this.h);
                        }
                    } else {
                        p.c("获取连接的数据流错误");
                        if (httpURLConnection2 != null) {
                            httpURLConnection2.disconnect();
                        }
                        this.h.what = 0;
                        this.b.sendMessage(this.h);
                    }
                } else {
                    p.c("mTotalSize null 获取的文件长度为null");
                    this.h.what = 0;
                    this.b.sendMessage(this.h);
                }
            } catch (IOException e8) {
                p.c("mInputStream is null 获取输入流为null");
                e8.printStackTrace();
                this.h.what = 0;
                this.b.sendMessage(this.h);
            }
        } else {
            p.c("mConnection连接为null");
            this.h.what = 0;
            this.b.sendMessage(this.h);
        }
    }
}
