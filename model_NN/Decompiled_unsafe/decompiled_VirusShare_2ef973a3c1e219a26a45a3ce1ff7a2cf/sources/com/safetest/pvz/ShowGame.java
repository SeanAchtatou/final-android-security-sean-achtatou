package com.safetest.pvz;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import com.neworld.demo.MyAd;
import java.lang.reflect.Array;

public class ShowGame extends Activity {
    int bullet_h = 15;
    int[][] bullet_kind;
    int bullet_num_max;
    int[][] bullet_position_x;
    int[] bullet_position_y;
    int bullet_w = 15;
    int[] bullet_x_add;
    int cur_stage = 0;
    int delay_time;
    DrawGame dg;
    boolean is_drawing = true;
    private MyAd mAd;
    int map_cell_h;
    int map_cell_w;
    int mouse_x;
    int mouse_y;
    int[][] plant_fire_delay_cur;
    int[] plant_fire_delay_set;
    int[][] plant_frame;
    int[][] plant_health;
    int[] plant_health_set;
    int[][] plant_kind;
    Rect r_flagmeter_bk = new Rect();
    Rect r_map = new Rect();
    Rect[][] r_map_cell;
    Rect r_map_seedbank = new Rect();
    Rect[] r_seedpacket = new Rect[6];
    Rect r_seedpacket_bk = new Rect();
    Rect r_shovel = new Rect();
    Rect[] r_sun_drop;
    Rect[] r_sun_move;
    Rect[] r_sun_static;
    int seed_selected;
    int[] seedpacket_cool_cur;
    int[] seedpacket_cool_set;
    int[] seedpacket_cost;
    int[] seedpacket_kind;
    int sun_destination_x;
    int sun_destination_y;
    int sun_drop_num_max;
    int sun_h = 50;
    int sun_num;
    int sun_num_x;
    int sun_num_y;
    int sun_select_move_speed;
    int[] sun_static_life;
    int sun_static_life_set;
    int sun_static_num_max;
    int sun_w = 50;
    int sun_y_add = 5;
    int win_h;
    int win_w;
    int[][] zomb_frame;
    int zomb_h = 57;
    int[][] zomb_health;
    int[][] zomb_kind;
    int zomb_num_max;
    int[] zomb_num_row;
    float[][] zomb_position_x;
    int[] zomb_position_y;
    int[][] zomb_status;
    int[][] zomb_status_time;
    int zomb_w = 40;
    float[] zomb_x_add = {0.7f, 0.7f, 0.7f};

    public ShowGame() {
        int[] iArr = new int[6];
        iArr[1] = 1;
        iArr[2] = 2;
        iArr[3] = 3;
        iArr[4] = 4;
        iArr[5] = 5;
        this.seedpacket_kind = iArr;
        this.seedpacket_cost = new int[]{50, 100, 200, 175, 250, 125};
        this.seedpacket_cool_set = new int[]{90, 90, 90, 90, 90, 600};
        this.seedpacket_cool_cur = new int[6];
        this.seed_selected = -1;
        this.r_map_cell = (Rect[][]) Array.newInstance(Rect.class, 5, 9);
        this.sun_drop_num_max = 5;
        this.sun_static_num_max = 20;
        this.r_sun_drop = new Rect[this.sun_drop_num_max];
        this.r_sun_move = new Rect[this.sun_static_num_max];
        this.r_sun_static = new Rect[this.sun_static_num_max];
        this.sun_static_life = new int[this.sun_static_num_max];
        this.sun_static_life_set = 50;
        this.mouse_x = -1;
        this.mouse_y = -1;
        this.delay_time = 5;
        this.bullet_num_max = 80;
        this.zomb_num_max = 200;
        this.sun_num = 50;
        this.plant_kind = (int[][]) Array.newInstance(Integer.TYPE, 5, 9);
        this.plant_frame = (int[][]) Array.newInstance(Integer.TYPE, 5, 9);
        this.plant_health_set = new int[]{60, 60, 60, 60, 60, 1200};
        this.plant_health = (int[][]) Array.newInstance(Integer.TYPE, 5, 9);
        this.bullet_kind = (int[][]) Array.newInstance(Integer.TYPE, 5, this.bullet_num_max);
        this.bullet_position_x = (int[][]) Array.newInstance(Integer.TYPE, 5, this.bullet_num_max);
        this.bullet_position_y = new int[5];
        this.bullet_x_add = new int[]{10, 10};
        this.zomb_kind = (int[][]) Array.newInstance(Integer.TYPE, 5, this.zomb_num_max);
        this.zomb_status = (int[][]) Array.newInstance(Integer.TYPE, 5, this.zomb_num_max);
        this.zomb_status_time = (int[][]) Array.newInstance(Integer.TYPE, 5, this.zomb_num_max);
        this.zomb_num_row = new int[5];
        this.zomb_position_x = (float[][]) Array.newInstance(Float.TYPE, 5, this.zomb_num_max);
        this.zomb_position_y = new int[5];
        this.zomb_frame = (int[][]) Array.newInstance(Integer.TYPE, 5, this.zomb_num_max);
        this.zomb_health = (int[][]) Array.newInstance(Integer.TYPE, 5, this.zomb_num_max);
        this.plant_fire_delay_set = new int[]{16, 1, 1, 1, 1, 1, 1, 1};
        this.plant_fire_delay_cur = new int[][]{new int[]{1, 1, 1, 1, 1, 1, 1, 1, 1}, new int[]{1, 1, 1, 1, 1, 1, 1, 1, 1}, new int[]{1, 1, 1, 1, 1, 1, 1, 1, 1}, new int[]{1, 1, 1, 1, 1, 1, 1, 1, 1}, new int[]{1, 1, 1, 1, 1, 1, 1, 1, 1}};
        this.sun_select_move_speed = 40;
        this.sun_destination_x = 139;
        this.sun_destination_y = 5;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        this.dg = new DrawGame(this);
        setContentView(this.dg);
        this.mAd = new MyAd(this, 1, true, null);
        this.mAd.startAd();
        getWindow().setFlags(1024, 1024);
        this.win_w = getWindowManager().getDefaultDisplay().getWidth();
        this.win_h = getWindowManager().getDefaultDisplay().getHeight();
        this.r_map = new Rect((this.win_w * 67) / 480, (this.win_h * 43) / 320, (this.win_w * 459) / 480, (this.win_h * 307) / 320);
        this.map_cell_w = this.r_map.width() / 9;
        this.map_cell_h = this.r_map.height() / 5;
        this.r_map_seedbank = new Rect((this.win_w * 128) / 480, 0, (this.win_w * 365) / 480, (this.win_h * 48) / 320);
        this.r_flagmeter_bk = new Rect(this.r_map_seedbank.right, (this.win_h * 300) / 320, (this.win_w * 450) / 480, (this.win_h * 314) / 320);
        this.r_seedpacket_bk = new Rect((this.win_w * 168) / 480, (this.win_h * 5) / 320, (this.win_w * 360) / 480, (this.win_h * 42) / 320);
        this.r_shovel.left = this.r_map_seedbank.right;
        this.r_shovel.right = (this.win_w * 403) / 480;
        this.r_shovel.top = 0;
        this.r_shovel.bottom = this.r_shovel.width();
        int packet_w = (this.win_w * 26) / 480;
        int margen = (this.r_seedpacket_bk.width() - (packet_w * 6)) / 12;
        for (int i = 0; i < 6; i++) {
            this.r_seedpacket[i] = new Rect();
            this.r_seedpacket[i].left = this.r_seedpacket_bk.left + (packet_w * i) + (((i * 2) + 1) * margen);
            this.r_seedpacket[i].right = this.r_seedpacket[i].left + packet_w;
            this.r_seedpacket[i].top = this.r_seedpacket_bk.top;
            this.r_seedpacket[i].bottom = this.r_seedpacket_bk.bottom;
        }
        this.sun_num_x = (this.win_w * 147) / 480;
        this.sun_num_y = (this.win_h * 45) / 320;
        for (int i2 = 0; i2 < 5; i2++) {
            for (int j = 0; j < 9; j++) {
                this.plant_kind[i2][j] = -1;
                this.plant_frame[i2][j] = 0;
                this.r_map_cell[i2][j] = new Rect(this.r_map.left + (this.map_cell_w * j), this.r_map.top + (this.map_cell_h * i2), this.r_map.left + (this.map_cell_w * (j + 1)), this.r_map.top + (this.map_cell_h * (i2 + 1)));
            }
        }
        for (int i3 = 0; i3 < 5; i3++) {
            for (int j2 = 0; j2 < this.bullet_kind[0].length; j2++) {
                this.bullet_kind[i3][j2] = -1;
            }
        }
        for (int i4 = 0; i4 < 5; i4++) {
            this.bullet_position_y[i4] = this.r_map_cell[i4][0].top + (this.r_map_cell[i4][0].width() / 2);
        }
        for (int i5 = 0; i5 < 5; i5++) {
            for (int j3 = 0; j3 < this.zomb_kind[0].length; j3++) {
                this.zomb_kind[i5][j3] = -1;
                this.zomb_status[i5][j3] = 1;
                this.zomb_status_time[i5][j3] = 0;
                this.zomb_frame[i5][j3] = 0;
                this.zomb_health[i5][j3] = -1;
            }
        }
        for (int i6 = 0; i6 < 5; i6++) {
            this.zomb_position_y[i6] = this.r_map_cell[i6][0].top + (this.r_map_cell[i6][0].width() / 2);
        }
        for (int i7 = 0; i7 < this.r_sun_drop.length; i7++) {
            this.r_sun_drop[i7] = new Rect(-1, -1, -1, -1);
        }
        for (int i8 = 0; i8 < this.r_sun_static.length; i8++) {
            this.r_sun_static[i8] = new Rect(-1, -1, -1, -1);
            this.sun_static_life[i8] = 0;
        }
        for (int i9 = 0; i9 < this.r_sun_move.length; i9++) {
            this.r_sun_move[i9] = new Rect(-1, -1, -1, -1);
        }
        this.sun_select_move_speed = (this.win_w * 40) / 480;
        this.sun_destination_x = (this.win_w * 139) / 480;
        this.sun_destination_y = (this.win_h * 5) / 320;
        this.bullet_x_add[0] = (this.bullet_x_add[0] * this.win_w) / 480;
        this.bullet_x_add[1] = (this.bullet_x_add[1] * this.win_w) / 480;
        this.bullet_w = (this.bullet_w * this.win_w) / 480;
        this.bullet_h = (this.bullet_h * this.win_h) / 320;
        this.zomb_w = (this.zomb_w * this.win_w) / 480;
        this.zomb_h = (this.zomb_h * this.win_h) / 320;
        this.sun_w = (this.sun_w * this.win_w) / 480;
        this.sun_h = (this.sun_h * this.win_h) / 320;
        for (int i10 = 0; i10 < this.zomb_x_add.length; i10++) {
            this.zomb_x_add[i10] = (this.zomb_x_add[i10] * ((float) this.win_w)) / 480.0f;
        }
        this.sun_y_add = (this.sun_y_add * this.win_h) / 320;
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        this.is_drawing = false;
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        this.is_drawing = true;
        super.onResume();
    }

    public boolean onTouchEvent(MotionEvent event) {
        if (!this.is_drawing) {
            return false;
        }
        int x = (int) event.getX();
        int y = (int) event.getY();
        if (event.getAction() == 0) {
            if (this.seed_selected != -1) {
                return false;
            }
            for (int i = 0; i < this.r_sun_drop.length; i++) {
                if (this.r_sun_drop[i].contains(x, y)) {
                    int j = 0;
                    while (this.r_sun_move[j].left != -1) {
                        j++;
                        if (j >= this.r_sun_move.length) {
                            return false;
                        }
                    }
                    this.r_sun_move[j] = new Rect(this.r_sun_drop[i]);
                    this.r_sun_drop[i].left = -1;
                    this.r_sun_drop[i].right = -1;
                    return true;
                }
            }
            int i2 = 0;
            while (i2 < this.r_sun_static.length) {
                if (this.sun_static_life[i2] <= 0 || !this.r_sun_static[i2].contains(x, y)) {
                    i2++;
                } else {
                    int j2 = 0;
                    while (this.r_sun_move[j2].left != -1) {
                        j2++;
                        if (j2 >= this.r_sun_move.length) {
                            return false;
                        }
                    }
                    this.r_sun_move[j2] = new Rect(this.r_sun_static[i2]);
                    this.sun_static_life[i2] = 0;
                    return true;
                }
            }
            int i3 = 0;
            while (i3 < this.r_seedpacket.length) {
                if (!this.r_seedpacket[i3].contains(x, y) || this.sun_num < this.seedpacket_cost[i3] || this.seedpacket_cool_cur[i3] != 0) {
                    i3++;
                } else {
                    this.seed_selected = i3;
                    this.mouse_x = x;
                    this.mouse_y = y;
                    return true;
                }
            }
            if (this.r_shovel.contains(x, y)) {
                this.seed_selected = 10;
                this.mouse_x = x;
                this.mouse_y = y;
                return true;
            }
        } else if (event.getAction() == 1) {
            if (this.seed_selected != -1) {
                if (this.r_map.contains(x, y)) {
                    int column = (x - this.r_map.left) / this.map_cell_w;
                    int row = (y - this.r_map.top) / this.map_cell_h;
                    if (column < 0) {
                        column = 0;
                    }
                    if (row < 0) {
                        row = 0;
                    }
                    if (column > 8) {
                        column = 8;
                    }
                    if (row > 4) {
                        row = 4;
                    }
                    if (this.seed_selected == 10) {
                        this.plant_kind[row][column] = -1;
                    } else if (this.seed_selected == 4) {
                        if (this.plant_kind[row][column] != 2) {
                            this.seed_selected = -1;
                            return false;
                        }
                        this.plant_kind[row][column] = this.seedpacket_kind[this.seed_selected];
                        this.plant_fire_delay_cur[row][column] = this.plant_fire_delay_set[this.seed_selected] / 2;
                        this.plant_health[row][column] = this.plant_health_set[this.seedpacket_kind[this.seed_selected]];
                        this.sun_num -= this.seedpacket_cost[this.seed_selected];
                        this.seedpacket_cool_cur[this.seed_selected] = this.seedpacket_cool_set[this.seed_selected];
                    } else if (this.plant_kind[row][column] == -1) {
                        this.plant_kind[row][column] = this.seedpacket_kind[this.seed_selected];
                        this.plant_fire_delay_cur[row][column] = this.plant_fire_delay_set[this.seed_selected] / 2;
                        this.plant_health[row][column] = this.plant_health_set[this.seedpacket_kind[this.seed_selected]];
                        this.sun_num -= this.seedpacket_cost[this.seed_selected];
                        this.seedpacket_cool_cur[this.seed_selected] = this.seedpacket_cool_set[this.seed_selected];
                    }
                }
                this.seed_selected = -1;
            }
        } else if (event.getAction() == 2) {
            this.mouse_x = x;
            this.mouse_y = y;
        }
        return super.onTouchEvent(event);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, 0, 0, "暂停|继续");
        menu.add(0, 1, 0, "关于");
        menu.add(0, 2, 0, "退出");
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 0:
                this.is_drawing = !this.is_drawing;
                break;
            case 1:
                new AlertDialog.Builder(this).setTitle("关于").setMessage("植物大战僵尸1.0\n\n编程:lzn1007\n\nMAIL:liangzhenning@gmail.com").setPositiveButton("确认", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                }).show();
                break;
            case 2:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private class DrawGame extends SurfaceView implements SurfaceHolder.Callback, Runnable {
        Bitmap bmp_bk1 = ((BitmapDrawable) getResources().getDrawable(R.drawable.bk1)).getBitmap();
        Bitmap[] bmp_bullet = {((BitmapDrawable) getResources().getDrawable(R.drawable.bullet_01)).getBitmap(), ((BitmapDrawable) getResources().getDrawable(R.drawable.bullet_02)).getBitmap()};
        Bitmap bmp_flagmeter_bk = ((BitmapDrawable) getResources().getDrawable(R.drawable.flagmeter)).getBitmap();
        Bitmap[][] bmp_plant = {new Bitmap[]{((BitmapDrawable) getResources().getDrawable(R.drawable.p_00_01)).getBitmap(), ((BitmapDrawable) getResources().getDrawable(R.drawable.p_00_02)).getBitmap(), ((BitmapDrawable) getResources().getDrawable(R.drawable.p_00_03)).getBitmap(), ((BitmapDrawable) getResources().getDrawable(R.drawable.p_00_04)).getBitmap(), ((BitmapDrawable) getResources().getDrawable(R.drawable.p_00_05)).getBitmap(), ((BitmapDrawable) getResources().getDrawable(R.drawable.p_00_06)).getBitmap(), ((BitmapDrawable) getResources().getDrawable(R.drawable.p_00_07)).getBitmap(), ((BitmapDrawable) getResources().getDrawable(R.drawable.p_00_08)).getBitmap()}, new Bitmap[]{((BitmapDrawable) getResources().getDrawable(R.drawable.p_01_01)).getBitmap(), ((BitmapDrawable) getResources().getDrawable(R.drawable.p_01_02)).getBitmap(), ((BitmapDrawable) getResources().getDrawable(R.drawable.p_01_03)).getBitmap(), ((BitmapDrawable) getResources().getDrawable(R.drawable.p_01_04)).getBitmap(), ((BitmapDrawable) getResources().getDrawable(R.drawable.p_01_05)).getBitmap(), ((BitmapDrawable) getResources().getDrawable(R.drawable.p_01_06)).getBitmap(), ((BitmapDrawable) getResources().getDrawable(R.drawable.p_01_07)).getBitmap(), ((BitmapDrawable) getResources().getDrawable(R.drawable.p_01_08)).getBitmap()}, new Bitmap[]{((BitmapDrawable) getResources().getDrawable(R.drawable.p_02_01)).getBitmap(), ((BitmapDrawable) getResources().getDrawable(R.drawable.p_02_02)).getBitmap(), ((BitmapDrawable) getResources().getDrawable(R.drawable.p_02_03)).getBitmap(), ((BitmapDrawable) getResources().getDrawable(R.drawable.p_02_04)).getBitmap(), ((BitmapDrawable) getResources().getDrawable(R.drawable.p_02_05)).getBitmap(), ((BitmapDrawable) getResources().getDrawable(R.drawable.p_02_06)).getBitmap(), ((BitmapDrawable) getResources().getDrawable(R.drawable.p_02_07)).getBitmap(), ((BitmapDrawable) getResources().getDrawable(R.drawable.p_02_08)).getBitmap()}, new Bitmap[]{((BitmapDrawable) getResources().getDrawable(R.drawable.p_03_01)).getBitmap(), ((BitmapDrawable) getResources().getDrawable(R.drawable.p_03_02)).getBitmap(), ((BitmapDrawable) getResources().getDrawable(R.drawable.p_03_03)).getBitmap(), ((BitmapDrawable) getResources().getDrawable(R.drawable.p_03_04)).getBitmap(), ((BitmapDrawable) getResources().getDrawable(R.drawable.p_03_05)).getBitmap(), ((BitmapDrawable) getResources().getDrawable(R.drawable.p_03_06)).getBitmap(), ((BitmapDrawable) getResources().getDrawable(R.drawable.p_03_07)).getBitmap(), ((BitmapDrawable) getResources().getDrawable(R.drawable.p_03_08)).getBitmap()}, new Bitmap[]{((BitmapDrawable) getResources().getDrawable(R.drawable.p_04_01)).getBitmap(), ((BitmapDrawable) getResources().getDrawable(R.drawable.p_04_02)).getBitmap(), ((BitmapDrawable) getResources().getDrawable(R.drawable.p_04_03)).getBitmap(), ((BitmapDrawable) getResources().getDrawable(R.drawable.p_04_04)).getBitmap(), ((BitmapDrawable) getResources().getDrawable(R.drawable.p_04_05)).getBitmap(), ((BitmapDrawable) getResources().getDrawable(R.drawable.p_04_06)).getBitmap(), ((BitmapDrawable) getResources().getDrawable(R.drawable.p_04_07)).getBitmap(), ((BitmapDrawable) getResources().getDrawable(R.drawable.p_04_08)).getBitmap()}, new Bitmap[]{((BitmapDrawable) getResources().getDrawable(R.drawable.p_05_01)).getBitmap(), ((BitmapDrawable) getResources().getDrawable(R.drawable.p_05_02)).getBitmap(), ((BitmapDrawable) getResources().getDrawable(R.drawable.p_05_03)).getBitmap(), ((BitmapDrawable) getResources().getDrawable(R.drawable.p_05_04)).getBitmap(), ((BitmapDrawable) getResources().getDrawable(R.drawable.p_05_05)).getBitmap(), ((BitmapDrawable) getResources().getDrawable(R.drawable.p_05_06)).getBitmap(), ((BitmapDrawable) getResources().getDrawable(R.drawable.p_05_07)).getBitmap(), ((BitmapDrawable) getResources().getDrawable(R.drawable.p_05_08)).getBitmap()}, new Bitmap[]{((BitmapDrawable) getResources().getDrawable(R.drawable.p_06_01)).getBitmap(), ((BitmapDrawable) getResources().getDrawable(R.drawable.p_06_02)).getBitmap(), ((BitmapDrawable) getResources().getDrawable(R.drawable.p_06_03)).getBitmap(), ((BitmapDrawable) getResources().getDrawable(R.drawable.p_06_04)).getBitmap(), ((BitmapDrawable) getResources().getDrawable(R.drawable.p_06_05)).getBitmap(), ((BitmapDrawable) getResources().getDrawable(R.drawable.p_06_06)).getBitmap(), ((BitmapDrawable) getResources().getDrawable(R.drawable.p_06_07)).getBitmap(), ((BitmapDrawable) getResources().getDrawable(R.drawable.p_06_08)).getBitmap()}, new Bitmap[]{((BitmapDrawable) getResources().getDrawable(R.drawable.p_07_01)).getBitmap(), ((BitmapDrawable) getResources().getDrawable(R.drawable.p_07_02)).getBitmap(), ((BitmapDrawable) getResources().getDrawable(R.drawable.p_07_03)).getBitmap(), ((BitmapDrawable) getResources().getDrawable(R.drawable.p_07_04)).getBitmap(), ((BitmapDrawable) getResources().getDrawable(R.drawable.p_07_05)).getBitmap(), ((BitmapDrawable) getResources().getDrawable(R.drawable.p_07_06)).getBitmap(), ((BitmapDrawable) getResources().getDrawable(R.drawable.p_07_07)).getBitmap(), ((BitmapDrawable) getResources().getDrawable(R.drawable.p_07_08)).getBitmap()}};
        Bitmap bmp_seedbank = ((BitmapDrawable) getResources().getDrawable(R.drawable.seedbank)).getBitmap();
        Bitmap[] bmp_seedpacket = {((BitmapDrawable) getResources().getDrawable(R.drawable.seed_00)).getBitmap(), ((BitmapDrawable) getResources().getDrawable(R.drawable.seed_01)).getBitmap(), ((BitmapDrawable) getResources().getDrawable(R.drawable.seed_02)).getBitmap(), ((BitmapDrawable) getResources().getDrawable(R.drawable.seed_03)).getBitmap(), ((BitmapDrawable) getResources().getDrawable(R.drawable.seed_04)).getBitmap(), ((BitmapDrawable) getResources().getDrawable(R.drawable.seed_05)).getBitmap()};
        Bitmap bmp_shovel = ((BitmapDrawable) getResources().getDrawable(R.drawable.shovel)).getBitmap();
        Bitmap bmp_shovel_bk = ((BitmapDrawable) getResources().getDrawable(R.drawable.shovel_bk)).getBitmap();
        Bitmap bmp_sun = ((BitmapDrawable) getResources().getDrawable(R.drawable.sun)).getBitmap();
        Bitmap[][] bmp_zomb = {new Bitmap[]{((BitmapDrawable) getResources().getDrawable(R.drawable.z_00_01)).getBitmap(), ((BitmapDrawable) getResources().getDrawable(R.drawable.z_00_02)).getBitmap(), ((BitmapDrawable) getResources().getDrawable(R.drawable.z_00_03)).getBitmap(), ((BitmapDrawable) getResources().getDrawable(R.drawable.z_00_04)).getBitmap(), ((BitmapDrawable) getResources().getDrawable(R.drawable.z_00_05)).getBitmap(), ((BitmapDrawable) getResources().getDrawable(R.drawable.z_00_06)).getBitmap(), ((BitmapDrawable) getResources().getDrawable(R.drawable.z_00_07)).getBitmap(), ((BitmapDrawable) getResources().getDrawable(R.drawable.z_00_08)).getBitmap()}, new Bitmap[]{((BitmapDrawable) getResources().getDrawable(R.drawable.z_01_01)).getBitmap(), ((BitmapDrawable) getResources().getDrawable(R.drawable.z_01_02)).getBitmap(), ((BitmapDrawable) getResources().getDrawable(R.drawable.z_01_03)).getBitmap(), ((BitmapDrawable) getResources().getDrawable(R.drawable.z_01_04)).getBitmap(), ((BitmapDrawable) getResources().getDrawable(R.drawable.z_01_05)).getBitmap(), ((BitmapDrawable) getResources().getDrawable(R.drawable.z_01_06)).getBitmap(), ((BitmapDrawable) getResources().getDrawable(R.drawable.z_01_07)).getBitmap(), ((BitmapDrawable) getResources().getDrawable(R.drawable.z_01_08)).getBitmap()}, new Bitmap[]{((BitmapDrawable) getResources().getDrawable(R.drawable.z_02_01)).getBitmap(), ((BitmapDrawable) getResources().getDrawable(R.drawable.z_02_02)).getBitmap(), ((BitmapDrawable) getResources().getDrawable(R.drawable.z_02_03)).getBitmap(), ((BitmapDrawable) getResources().getDrawable(R.drawable.z_02_04)).getBitmap(), ((BitmapDrawable) getResources().getDrawable(R.drawable.z_02_05)).getBitmap(), ((BitmapDrawable) getResources().getDrawable(R.drawable.z_02_06)).getBitmap(), ((BitmapDrawable) getResources().getDrawable(R.drawable.z_02_07)).getBitmap(), ((BitmapDrawable) getResources().getDrawable(R.drawable.z_02_08)).getBitmap()}};
        Bitmap bmp_zombhead = ((BitmapDrawable) getResources().getDrawable(R.drawable.zombhead)).getBitmap();
        int cur_time;
        Canvas cv;
        int[][] plant_bmp_delay_cur = {new int[]{1, 1, 1, 1, 1, 1, 1, 1, 1}, new int[]{1, 1, 1, 1, 1, 1, 1, 1, 1}, new int[]{1, 1, 1, 1, 1, 1, 1, 1, 1}, new int[]{1, 1, 1, 1, 1, 1, 1, 1, 1}, new int[]{1, 1, 1, 1, 1, 1, 1, 1, 1}};
        int[] plant_bmp_delay_set = {2, 2, 2, 2, 2, 2, 2, 2};
        int plant_margen = 5;
        Paint pt;
        SurfaceHolder sh;
        long time_before;
        long time_now;
        int time_sun_creat_interval;
        int zomb1_start_time;
        double zomb1_time_percent;
        int zomb2_start_time;
        double zomb2_time_percent;
        int zomb_biggroup_time;
        int[][] zomb_bmp_delay_cur;
        int[] zomb_bmp_delay_set = {5, 5, 5};
        int zomb_creat_num_cur;
        float zomb_creat_num_set;
        int[] zomb_health_set;
        int zomb_start_time;

        public DrawGame(Context context) {
            super(context);
            this.zomb_bmp_delay_cur = (int[][]) Array.newInstance(Integer.TYPE, 5, ShowGame.this.zomb_num_max);
            this.zomb_health_set = new int[]{10, 30, 50};
            this.zomb_creat_num_set = 5.0f;
            this.zomb_creat_num_cur = 0;
            this.time_before = 0;
            this.time_now = 0;
            this.cur_time = 0;
            this.zomb_start_time = 180;
            this.zomb1_start_time = 1080;
            this.zomb1_time_percent = 0.6d;
            this.zomb2_start_time = 2000;
            this.zomb2_time_percent = 0.8d;
            this.zomb_biggroup_time = 900;
            this.time_sun_creat_interval = 60;
            for (int i = 0; i < 5; i++) {
                for (int j = 0; j < ShowGame.this.zomb_num_max; j++) {
                    this.zomb_bmp_delay_cur[i][j] = 1;
                }
            }
            this.sh = getHolder();
            this.sh.addCallback(this);
            setFocusable(true);
        }

        public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        }

        /* JADX WARNING: Code restructure failed: missing block: B:79:0x0412, code lost:
            android.util.Log.i("tst", "draw plant error");
         */
        /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void run() {
            /*
                r26 = this;
            L_0x0000:
                r0 = r26
                com.safetest.pvz.ShowGame r0 = com.safetest.pvz.ShowGame.this     // Catch:{ InterruptedException -> 0x05fc }
                r19 = r0
                r0 = r19
                int r0 = r0.delay_time     // Catch:{ InterruptedException -> 0x05fc }
                r19 = r0
                r0 = r19
                long r0 = (long) r0     // Catch:{ InterruptedException -> 0x05fc }
                r19 = r0
                java.lang.Thread.sleep(r19)     // Catch:{ InterruptedException -> 0x05fc }
            L_0x0014:
                r0 = r26
                com.safetest.pvz.ShowGame r0 = com.safetest.pvz.ShowGame.this
                r19 = r0
                r0 = r19
                boolean r0 = r0.is_drawing
                r19 = r0
                if (r19 == 0) goto L_0x0000
                r0 = r26
                android.view.SurfaceHolder r0 = r0.sh
                r19 = r0
                monitor-enter(r19)
                r0 = r26
                android.view.SurfaceHolder r0 = r0.sh     // Catch:{ all -> 0x0404 }
                r20 = r0
                android.graphics.Canvas r20 = r20.lockCanvas()     // Catch:{ all -> 0x0404 }
                r0 = r20
                r1 = r26
                r1.cv = r0     // Catch:{ all -> 0x0404 }
                android.graphics.Paint r20 = new android.graphics.Paint     // Catch:{ all -> 0x0404 }
                r20.<init>()     // Catch:{ all -> 0x0404 }
                r0 = r20
                r1 = r26
                r1.pt = r0     // Catch:{ all -> 0x0404 }
                r26.f_draw_back()     // Catch:{ Exception -> 0x0407 }
            L_0x0047:
                r26.f_draw_plant()     // Catch:{ Exception -> 0x0411 }
            L_0x004a:
                r26.f_draw_bullet()     // Catch:{ Exception -> 0x041b }
            L_0x004d:
                r26.f_draw_zomb()     // Catch:{ Exception -> 0x0425 }
            L_0x0050:
                r0 = r26
                int r0 = r0.cur_time     // Catch:{ all -> 0x0404 }
                r20 = r0
                r0 = r26
                int r0 = r0.zomb_start_time     // Catch:{ all -> 0x0404 }
                r21 = r0
                r0 = r20
                r1 = r21
                if (r0 <= r1) goto L_0x0065
                r26.f_draw_flag()     // Catch:{ all -> 0x0404 }
            L_0x0065:
                r26.f_draw_sun()     // Catch:{ all -> 0x0404 }
                r0 = r26
                int r0 = r0.cur_time     // Catch:{ all -> 0x0404 }
                r20 = r0
                int r20 = r20 + 1
                r0 = r20
                r1 = r26
                r1.cur_time = r0     // Catch:{ all -> 0x0404 }
                r0 = r26
                int r0 = r0.cur_time     // Catch:{ all -> 0x0404 }
                r20 = r0
                r0 = r26
                int r0 = r0.zomb_start_time     // Catch:{ all -> 0x0404 }
                r21 = r0
                r0 = r20
                r1 = r21
                if (r0 <= r1) goto L_0x0163
                r0 = r26
                int r0 = r0.zomb_creat_num_cur     // Catch:{ all -> 0x0404 }
                r20 = r0
                r0 = r26
                int r0 = r0.zomb_biggroup_time     // Catch:{ all -> 0x0404 }
                r21 = r0
                int r20 = r20 * r21
                r0 = r20
                float r0 = (float) r0     // Catch:{ all -> 0x0404 }
                r20 = r0
                r0 = r26
                float r0 = r0.zomb_creat_num_set     // Catch:{ all -> 0x0404 }
                r21 = r0
                r0 = r26
                int r0 = r0.cur_time     // Catch:{ all -> 0x0404 }
                r22 = r0
                r0 = r26
                int r0 = r0.zomb_start_time     // Catch:{ all -> 0x0404 }
                r23 = r0
                int r22 = r22 - r23
                r0 = r26
                int r0 = r0.zomb_biggroup_time     // Catch:{ all -> 0x0404 }
                r23 = r0
                int r22 = r22 % r23
                r0 = r22
                float r0 = (float) r0     // Catch:{ all -> 0x0404 }
                r22 = r0
                float r21 = r21 * r22
                int r20 = (r20 > r21 ? 1 : (r20 == r21 ? 0 : -1))
                if (r20 >= 0) goto L_0x0129
                double r20 = java.lang.Math.random()     // Catch:{ all -> 0x0404 }
                r22 = 4617315517961601024(0x4014000000000000, double:5.0)
                double r20 = r20 * r22
                r0 = r20
                int r0 = (int) r0     // Catch:{ all -> 0x0404 }
                r15 = r0
                r10 = 0
                r0 = r26
                int r0 = r0.cur_time     // Catch:{ all -> 0x0404 }
                r20 = r0
                r0 = r26
                int r0 = r0.zomb2_start_time     // Catch:{ all -> 0x0404 }
                r21 = r0
                r0 = r20
                r1 = r21
                if (r0 <= r1) goto L_0x0464
                r0 = r26
                int r0 = r0.cur_time     // Catch:{ all -> 0x0404 }
                r20 = r0
                r0 = r26
                int r0 = r0.zomb_start_time     // Catch:{ all -> 0x0404 }
                r21 = r0
                int r20 = r20 - r21
                r0 = r26
                int r0 = r0.zomb_biggroup_time     // Catch:{ all -> 0x0404 }
                r21 = r0
                int r20 = r20 % r21
                r0 = r20
                double r0 = (double) r0     // Catch:{ all -> 0x0404 }
                r20 = r0
                r0 = r26
                int r0 = r0.zomb_biggroup_time     // Catch:{ all -> 0x0404 }
                r22 = r0
                r0 = r22
                double r0 = (double) r0     // Catch:{ all -> 0x0404 }
                r22 = r0
                r0 = r26
                double r0 = r0.zomb2_time_percent     // Catch:{ all -> 0x0404 }
                r24 = r0
                double r22 = r22 * r24
                int r20 = (r20 > r22 ? 1 : (r20 == r22 ? 0 : -1))
                if (r20 <= 0) goto L_0x042f
                r10 = 2
            L_0x0114:
                r0 = r26
                r1 = r15
                r2 = r10
                r0.f_add_zomb(r1, r2)     // Catch:{ all -> 0x0404 }
                r0 = r26
                int r0 = r0.zomb_creat_num_cur     // Catch:{ all -> 0x0404 }
                r20 = r0
                int r20 = r20 + 1
                r0 = r20
                r1 = r26
                r1.zomb_creat_num_cur = r0     // Catch:{ all -> 0x0404 }
            L_0x0129:
                r0 = r26
                int r0 = r0.cur_time     // Catch:{ all -> 0x0404 }
                r20 = r0
                r0 = r26
                int r0 = r0.zomb_start_time     // Catch:{ all -> 0x0404 }
                r21 = r0
                int r20 = r20 - r21
                r0 = r26
                int r0 = r0.zomb_biggroup_time     // Catch:{ all -> 0x0404 }
                r21 = r0
                int r20 = r20 % r21
                if (r20 != 0) goto L_0x0163
                r0 = r26
                float r0 = r0.zomb_creat_num_set     // Catch:{ all -> 0x0404 }
                r20 = r0
                r0 = r20
                double r0 = (double) r0     // Catch:{ all -> 0x0404 }
                r20 = r0
                r22 = 4609434218613702656(0x3ff8000000000000, double:1.5)
                double r20 = r20 * r22
                r0 = r20
                float r0 = (float) r0     // Catch:{ all -> 0x0404 }
                r20 = r0
                r0 = r20
                r1 = r26
                r1.zomb_creat_num_set = r0     // Catch:{ all -> 0x0404 }
                r20 = 0
                r0 = r20
                r1 = r26
                r1.zomb_creat_num_cur = r0     // Catch:{ all -> 0x0404 }
            L_0x0163:
                r0 = r26
                int r0 = r0.cur_time     // Catch:{ all -> 0x0404 }
                r20 = r0
                r0 = r26
                int r0 = r0.time_sun_creat_interval     // Catch:{ all -> 0x0404 }
                r21 = r0
                int r20 = r20 % r21
                if (r20 != 0) goto L_0x0176
                r26.f_add_sun_drop()     // Catch:{ all -> 0x0404 }
            L_0x0176:
                long r20 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x0404 }
                r0 = r20
                r2 = r26
                r2.time_now = r0     // Catch:{ all -> 0x0404 }
                r0 = r26
                long r0 = r0.time_now     // Catch:{ all -> 0x0404 }
                r20 = r0
                r0 = r26
                long r0 = r0.time_before     // Catch:{ all -> 0x0404 }
                r22 = r0
                long r17 = r20 - r22
                r0 = r26
                long r0 = r0.time_now     // Catch:{ all -> 0x0404 }
                r20 = r0
                r0 = r20
                r2 = r26
                r2.time_before = r0     // Catch:{ all -> 0x0404 }
                r20 = 1000(0x3e8, double:4.94E-321)
                long r7 = r20 / r17
                r0 = r26
                android.graphics.Paint r0 = r0.pt     // Catch:{ all -> 0x0404 }
                r20 = r0
                r21 = 255(0xff, float:3.57E-43)
                r22 = 0
                r23 = 0
                r24 = 0
                r20.setARGB(r21, r22, r23, r24)     // Catch:{ all -> 0x0404 }
                r0 = r26
                android.graphics.Paint r0 = r0.pt     // Catch:{ all -> 0x0404 }
                r20 = r0
                r21 = 1106247680(0x41f00000, float:30.0)
                r20.setTextSize(r21)     // Catch:{ all -> 0x0404 }
                r20 = 10
                int r20 = (r7 > r20 ? 1 : (r7 == r20 ? 0 : -1))
                if (r20 <= 0) goto L_0x04ab
                r0 = r26
                com.safetest.pvz.ShowGame r0 = com.safetest.pvz.ShowGame.this     // Catch:{ all -> 0x0404 }
                r20 = r0
                r0 = r20
                int r0 = r0.delay_time     // Catch:{ all -> 0x0404 }
                r21 = r0
                int r21 = r21 + 1
                r0 = r21
                r1 = r20
                r1.delay_time = r0     // Catch:{ all -> 0x0404 }
            L_0x01d4:
                r0 = r26
                com.safetest.pvz.ShowGame r0 = com.safetest.pvz.ShowGame.this     // Catch:{ all -> 0x0404 }
                r20 = r0
                r0 = r20
                int r0 = r0.seed_selected     // Catch:{ all -> 0x0404 }
                r16 = r0
                r0 = r26
                com.safetest.pvz.ShowGame r0 = com.safetest.pvz.ShowGame.this     // Catch:{ all -> 0x0404 }
                r20 = r0
                r0 = r20
                int r0 = r0.mouse_x     // Catch:{ all -> 0x0404 }
                r11 = r0
                r0 = r26
                com.safetest.pvz.ShowGame r0 = com.safetest.pvz.ShowGame.this     // Catch:{ all -> 0x0404 }
                r20 = r0
                r0 = r20
                int r0 = r0.mouse_y     // Catch:{ all -> 0x0404 }
                r12 = r0
                r20 = -1
                r0 = r16
                r1 = r20
                if (r0 == r1) goto L_0x03db
                r0 = r26
                com.safetest.pvz.ShowGame r0 = com.safetest.pvz.ShowGame.this     // Catch:{ all -> 0x0404 }
                r20 = r0
                r0 = r20
                android.graphics.Rect r0 = r0.r_map     // Catch:{ all -> 0x0404 }
                r20 = r0
                r0 = r20
                r1 = r11
                r2 = r12
                boolean r20 = r0.contains(r1, r2)     // Catch:{ all -> 0x0404 }
                if (r20 == 0) goto L_0x0334
                r0 = r26
                com.safetest.pvz.ShowGame r0 = com.safetest.pvz.ShowGame.this     // Catch:{ all -> 0x0404 }
                r20 = r0
                r0 = r20
                android.graphics.Rect r0 = r0.r_map     // Catch:{ all -> 0x0404 }
                r20 = r0
                r0 = r20
                int r0 = r0.left     // Catch:{ all -> 0x0404 }
                r20 = r0
                int r20 = r11 - r20
                r0 = r26
                com.safetest.pvz.ShowGame r0 = com.safetest.pvz.ShowGame.this     // Catch:{ all -> 0x0404 }
                r21 = r0
                r0 = r21
                int r0 = r0.map_cell_w     // Catch:{ all -> 0x0404 }
                r21 = r0
                int r5 = r20 / r21
                r0 = r26
                com.safetest.pvz.ShowGame r0 = com.safetest.pvz.ShowGame.this     // Catch:{ all -> 0x0404 }
                r20 = r0
                r0 = r20
                android.graphics.Rect r0 = r0.r_map     // Catch:{ all -> 0x0404 }
                r20 = r0
                r0 = r20
                int r0 = r0.top     // Catch:{ all -> 0x0404 }
                r20 = r0
                int r20 = r12 - r20
                r0 = r26
                com.safetest.pvz.ShowGame r0 = com.safetest.pvz.ShowGame.this     // Catch:{ all -> 0x0404 }
                r21 = r0
                r0 = r21
                int r0 = r0.map_cell_h     // Catch:{ all -> 0x0404 }
                r21 = r0
                int r15 = r20 / r21
                if (r5 >= 0) goto L_0x025b
                r5 = 0
            L_0x025b:
                if (r15 >= 0) goto L_0x025e
                r15 = 0
            L_0x025e:
                r20 = 8
                r0 = r5
                r1 = r20
                if (r0 <= r1) goto L_0x0267
                r5 = 8
            L_0x0267:
                r20 = 4
                r0 = r15
                r1 = r20
                if (r0 < r1) goto L_0x026f
                r15 = 4
            L_0x026f:
                r20 = 4
                r0 = r16
                r1 = r20
                if (r0 != r1) goto L_0x04d7
                r0 = r26
                com.safetest.pvz.ShowGame r0 = com.safetest.pvz.ShowGame.this     // Catch:{ all -> 0x0404 }
                r20 = r0
                r0 = r20
                int[][] r0 = r0.plant_kind     // Catch:{ all -> 0x0404 }
                r20 = r0
                r20 = r20[r15]     // Catch:{ all -> 0x0404 }
                r20 = r20[r5]     // Catch:{ all -> 0x0404 }
                r21 = 2
                r0 = r20
                r1 = r21
                if (r0 != r1) goto L_0x0334
                r0 = r26
                android.graphics.Paint r0 = r0.pt     // Catch:{ all -> 0x0404 }
                r20 = r0
                r21 = 100
                r22 = 0
                r23 = 0
                r24 = 0
                r20.setARGB(r21, r22, r23, r24)     // Catch:{ all -> 0x0404 }
                android.graphics.Rect r14 = new android.graphics.Rect     // Catch:{ all -> 0x0404 }
                r0 = r26
                com.safetest.pvz.ShowGame r0 = com.safetest.pvz.ShowGame.this     // Catch:{ all -> 0x0404 }
                r20 = r0
                r0 = r20
                android.graphics.Rect[][] r0 = r0.r_map_cell     // Catch:{ all -> 0x0404 }
                r20 = r0
                r20 = r20[r15]     // Catch:{ all -> 0x0404 }
                r20 = r20[r5]     // Catch:{ all -> 0x0404 }
                r0 = r14
                r1 = r20
                r0.<init>(r1)     // Catch:{ all -> 0x0404 }
                r0 = r14
                int r0 = r0.left     // Catch:{ all -> 0x0404 }
                r20 = r0
                r0 = r26
                int r0 = r0.plant_margen     // Catch:{ all -> 0x0404 }
                r21 = r0
                int r20 = r20 + r21
                r0 = r20
                r1 = r14
                r1.left = r0     // Catch:{ all -> 0x0404 }
                r0 = r14
                int r0 = r0.right     // Catch:{ all -> 0x0404 }
                r20 = r0
                r0 = r26
                int r0 = r0.plant_margen     // Catch:{ all -> 0x0404 }
                r21 = r0
                int r20 = r20 - r21
                r0 = r20
                r1 = r14
                r1.right = r0     // Catch:{ all -> 0x0404 }
                r0 = r14
                int r0 = r0.top     // Catch:{ all -> 0x0404 }
                r20 = r0
                r0 = r26
                int r0 = r0.plant_margen     // Catch:{ all -> 0x0404 }
                r21 = r0
                int r20 = r20 + r21
                r0 = r20
                r1 = r14
                r1.top = r0     // Catch:{ all -> 0x0404 }
                r0 = r14
                int r0 = r0.bottom     // Catch:{ all -> 0x0404 }
                r20 = r0
                r0 = r26
                int r0 = r0.plant_margen     // Catch:{ all -> 0x0404 }
                r21 = r0
                int r20 = r20 - r21
                r0 = r20
                r1 = r14
                r1.bottom = r0     // Catch:{ all -> 0x0404 }
                r0 = r26
                android.graphics.Canvas r0 = r0.cv     // Catch:{ all -> 0x0404 }
                r20 = r0
                r0 = r26
                android.graphics.Bitmap[][] r0 = r0.bmp_plant     // Catch:{ all -> 0x0404 }
                r21 = r0
                r0 = r26
                com.safetest.pvz.ShowGame r0 = com.safetest.pvz.ShowGame.this     // Catch:{ all -> 0x0404 }
                r22 = r0
                r0 = r22
                int[] r0 = r0.seedpacket_kind     // Catch:{ all -> 0x0404 }
                r22 = r0
                r22 = r22[r16]     // Catch:{ all -> 0x0404 }
                r21 = r21[r22]     // Catch:{ all -> 0x0404 }
                r22 = 0
                r21 = r21[r22]     // Catch:{ all -> 0x0404 }
                r22 = 0
                r0 = r26
                android.graphics.Paint r0 = r0.pt     // Catch:{ all -> 0x0404 }
                r23 = r0
                r0 = r20
                r1 = r21
                r2 = r22
                r3 = r14
                r4 = r23
                r0.drawBitmap(r1, r2, r3, r4)     // Catch:{ all -> 0x0404 }
            L_0x0334:
                r0 = r26
                android.graphics.Paint r0 = r0.pt     // Catch:{ all -> 0x0404 }
                r20 = r0
                r21 = 255(0xff, float:3.57E-43)
                r22 = 0
                r23 = 0
                r24 = 0
                r20.setARGB(r21, r22, r23, r24)     // Catch:{ all -> 0x0404 }
                android.graphics.Rect r13 = new android.graphics.Rect     // Catch:{ all -> 0x0404 }
                r0 = r26
                com.safetest.pvz.ShowGame r0 = com.safetest.pvz.ShowGame.this     // Catch:{ all -> 0x0404 }
                r20 = r0
                r0 = r20
                int r0 = r0.map_cell_w     // Catch:{ all -> 0x0404 }
                r20 = r0
                int r20 = r20 / 2
                int r20 = r11 - r20
                r0 = r26
                int r0 = r0.plant_margen     // Catch:{ all -> 0x0404 }
                r21 = r0
                int r20 = r20 + r21
                r0 = r26
                com.safetest.pvz.ShowGame r0 = com.safetest.pvz.ShowGame.this     // Catch:{ all -> 0x0404 }
                r21 = r0
                r0 = r21
                int r0 = r0.map_cell_h     // Catch:{ all -> 0x0404 }
                r21 = r0
                int r21 = r21 / 2
                int r21 = r12 - r21
                r0 = r26
                int r0 = r0.plant_margen     // Catch:{ all -> 0x0404 }
                r22 = r0
                int r21 = r21 + r22
                r0 = r26
                com.safetest.pvz.ShowGame r0 = com.safetest.pvz.ShowGame.this     // Catch:{ all -> 0x0404 }
                r22 = r0
                r0 = r22
                int r0 = r0.map_cell_w     // Catch:{ all -> 0x0404 }
                r22 = r0
                int r22 = r22 / 2
                int r22 = r22 + r11
                r0 = r26
                int r0 = r0.plant_margen     // Catch:{ all -> 0x0404 }
                r23 = r0
                int r22 = r22 - r23
                r0 = r26
                com.safetest.pvz.ShowGame r0 = com.safetest.pvz.ShowGame.this     // Catch:{ all -> 0x0404 }
                r23 = r0
                r0 = r23
                int r0 = r0.map_cell_h     // Catch:{ all -> 0x0404 }
                r23 = r0
                int r23 = r23 / 2
                int r23 = r23 + r12
                r0 = r26
                int r0 = r0.plant_margen     // Catch:{ all -> 0x0404 }
                r24 = r0
                int r23 = r23 - r24
                r0 = r13
                r1 = r20
                r2 = r21
                r3 = r22
                r4 = r23
                r0.<init>(r1, r2, r3, r4)     // Catch:{ all -> 0x0404 }
                r20 = 10
                r0 = r16
                r1 = r20
                if (r0 != r1) goto L_0x059e
                r0 = r26
                android.graphics.Canvas r0 = r0.cv     // Catch:{ all -> 0x0404 }
                r20 = r0
                r0 = r26
                android.graphics.Bitmap r0 = r0.bmp_shovel     // Catch:{ all -> 0x0404 }
                r21 = r0
                r22 = 0
                r0 = r26
                android.graphics.Paint r0 = r0.pt     // Catch:{ all -> 0x0404 }
                r23 = r0
                r0 = r20
                r1 = r21
                r2 = r22
                r3 = r13
                r4 = r23
                r0.drawBitmap(r1, r2, r3, r4)     // Catch:{ all -> 0x0404 }
            L_0x03db:
                r9 = 0
            L_0x03dc:
                r0 = r26
                com.safetest.pvz.ShowGame r0 = com.safetest.pvz.ShowGame.this     // Catch:{ all -> 0x0404 }
                r20 = r0
                r0 = r20
                int[] r0 = r0.seedpacket_cool_cur     // Catch:{ all -> 0x0404 }
                r20 = r0
                r0 = r20
                int r0 = r0.length     // Catch:{ all -> 0x0404 }
                r20 = r0
                r0 = r9
                r1 = r20
                if (r0 < r1) goto L_0x05d4
                r0 = r26
                android.view.SurfaceHolder r0 = r0.sh     // Catch:{ all -> 0x0404 }
                r20 = r0
                r0 = r26
                android.graphics.Canvas r0 = r0.cv     // Catch:{ all -> 0x0404 }
                r21 = r0
                r20.unlockCanvasAndPost(r21)     // Catch:{ all -> 0x0404 }
                monitor-exit(r19)     // Catch:{ all -> 0x0404 }
                goto L_0x0000
            L_0x0404:
                r20 = move-exception
                monitor-exit(r19)     // Catch:{ all -> 0x0404 }
                throw r20
            L_0x0407:
                r6 = move-exception
                java.lang.String r20 = "tst"
                java.lang.String r21 = "draw back error"
                android.util.Log.i(r20, r21)     // Catch:{ all -> 0x0404 }
                goto L_0x0047
            L_0x0411:
                r6 = move-exception
                java.lang.String r20 = "tst"
                java.lang.String r21 = "draw plant error"
                android.util.Log.i(r20, r21)     // Catch:{ all -> 0x0404 }
                goto L_0x004a
            L_0x041b:
                r6 = move-exception
                java.lang.String r20 = "tst"
                java.lang.String r21 = "draw bullet error"
                android.util.Log.i(r20, r21)     // Catch:{ all -> 0x0404 }
                goto L_0x004d
            L_0x0425:
                r6 = move-exception
                java.lang.String r20 = "tst"
                java.lang.String r21 = "draw zomb error"
                android.util.Log.i(r20, r21)     // Catch:{ all -> 0x0404 }
                goto L_0x0050
            L_0x042f:
                r0 = r26
                int r0 = r0.cur_time     // Catch:{ all -> 0x0404 }
                r20 = r0
                r0 = r26
                int r0 = r0.zomb_start_time     // Catch:{ all -> 0x0404 }
                r21 = r0
                int r20 = r20 - r21
                r0 = r26
                int r0 = r0.zomb_biggroup_time     // Catch:{ all -> 0x0404 }
                r21 = r0
                int r20 = r20 % r21
                r0 = r20
                double r0 = (double) r0     // Catch:{ all -> 0x0404 }
                r20 = r0
                r0 = r26
                int r0 = r0.zomb_biggroup_time     // Catch:{ all -> 0x0404 }
                r22 = r0
                r0 = r22
                double r0 = (double) r0     // Catch:{ all -> 0x0404 }
                r22 = r0
                r0 = r26
                double r0 = r0.zomb1_time_percent     // Catch:{ all -> 0x0404 }
                r24 = r0
                double r22 = r22 * r24
                int r20 = (r20 > r22 ? 1 : (r20 == r22 ? 0 : -1))
                if (r20 <= 0) goto L_0x0114
                r10 = 1
                goto L_0x0114
            L_0x0464:
                r0 = r26
                int r0 = r0.cur_time     // Catch:{ all -> 0x0404 }
                r20 = r0
                r0 = r26
                int r0 = r0.zomb1_start_time     // Catch:{ all -> 0x0404 }
                r21 = r0
                r0 = r20
                r1 = r21
                if (r0 <= r1) goto L_0x0114
                r0 = r26
                int r0 = r0.cur_time     // Catch:{ all -> 0x0404 }
                r20 = r0
                r0 = r26
                int r0 = r0.zomb_start_time     // Catch:{ all -> 0x0404 }
                r21 = r0
                int r20 = r20 - r21
                r0 = r26
                int r0 = r0.zomb_biggroup_time     // Catch:{ all -> 0x0404 }
                r21 = r0
                int r20 = r20 % r21
                r0 = r20
                double r0 = (double) r0     // Catch:{ all -> 0x0404 }
                r20 = r0
                r0 = r26
                int r0 = r0.zomb_biggroup_time     // Catch:{ all -> 0x0404 }
                r22 = r0
                r0 = r22
                double r0 = (double) r0     // Catch:{ all -> 0x0404 }
                r22 = r0
                r0 = r26
                double r0 = r0.zomb1_time_percent     // Catch:{ all -> 0x0404 }
                r24 = r0
                double r22 = r22 * r24
                int r20 = (r20 > r22 ? 1 : (r20 == r22 ? 0 : -1))
                if (r20 <= 0) goto L_0x0114
                r10 = 1
                goto L_0x0114
            L_0x04ab:
                r20 = 10
                int r20 = (r7 > r20 ? 1 : (r7 == r20 ? 0 : -1))
                if (r20 >= 0) goto L_0x01d4
                r0 = r26
                com.safetest.pvz.ShowGame r0 = com.safetest.pvz.ShowGame.this     // Catch:{ all -> 0x0404 }
                r20 = r0
                r0 = r20
                int r0 = r0.delay_time     // Catch:{ all -> 0x0404 }
                r20 = r0
                if (r20 <= 0) goto L_0x01d4
                r0 = r26
                com.safetest.pvz.ShowGame r0 = com.safetest.pvz.ShowGame.this     // Catch:{ all -> 0x0404 }
                r20 = r0
                r0 = r20
                int r0 = r0.delay_time     // Catch:{ all -> 0x0404 }
                r21 = r0
                r22 = 1
                int r21 = r21 - r22
                r0 = r21
                r1 = r20
                r1.delay_time = r0     // Catch:{ all -> 0x0404 }
                goto L_0x01d4
            L_0x04d7:
                r0 = r26
                com.safetest.pvz.ShowGame r0 = com.safetest.pvz.ShowGame.this     // Catch:{ all -> 0x0404 }
                r20 = r0
                r0 = r20
                int[][] r0 = r0.plant_kind     // Catch:{ all -> 0x0404 }
                r20 = r0
                r20 = r20[r15]     // Catch:{ all -> 0x0404 }
                r20 = r20[r5]     // Catch:{ all -> 0x0404 }
                r21 = -1
                r0 = r20
                r1 = r21
                if (r0 != r1) goto L_0x0334
                r20 = 10
                r0 = r16
                r1 = r20
                if (r0 == r1) goto L_0x0334
                r0 = r26
                android.graphics.Paint r0 = r0.pt     // Catch:{ all -> 0x0404 }
                r20 = r0
                r21 = 100
                r22 = 0
                r23 = 0
                r24 = 0
                r20.setARGB(r21, r22, r23, r24)     // Catch:{ all -> 0x0404 }
                android.graphics.Rect r14 = new android.graphics.Rect     // Catch:{ all -> 0x0404 }
                r0 = r26
                com.safetest.pvz.ShowGame r0 = com.safetest.pvz.ShowGame.this     // Catch:{ all -> 0x0404 }
                r20 = r0
                r0 = r20
                android.graphics.Rect[][] r0 = r0.r_map_cell     // Catch:{ all -> 0x0404 }
                r20 = r0
                r20 = r20[r15]     // Catch:{ all -> 0x0404 }
                r20 = r20[r5]     // Catch:{ all -> 0x0404 }
                r0 = r14
                r1 = r20
                r0.<init>(r1)     // Catch:{ all -> 0x0404 }
                r0 = r14
                int r0 = r0.left     // Catch:{ all -> 0x0404 }
                r20 = r0
                r0 = r26
                int r0 = r0.plant_margen     // Catch:{ all -> 0x0404 }
                r21 = r0
                int r20 = r20 + r21
                r0 = r20
                r1 = r14
                r1.left = r0     // Catch:{ all -> 0x0404 }
                r0 = r14
                int r0 = r0.right     // Catch:{ all -> 0x0404 }
                r20 = r0
                r0 = r26
                int r0 = r0.plant_margen     // Catch:{ all -> 0x0404 }
                r21 = r0
                int r20 = r20 - r21
                r0 = r20
                r1 = r14
                r1.right = r0     // Catch:{ all -> 0x0404 }
                r0 = r14
                int r0 = r0.top     // Catch:{ all -> 0x0404 }
                r20 = r0
                r0 = r26
                int r0 = r0.plant_margen     // Catch:{ all -> 0x0404 }
                r21 = r0
                int r20 = r20 + r21
                r0 = r20
                r1 = r14
                r1.top = r0     // Catch:{ all -> 0x0404 }
                r0 = r14
                int r0 = r0.bottom     // Catch:{ all -> 0x0404 }
                r20 = r0
                r0 = r26
                int r0 = r0.plant_margen     // Catch:{ all -> 0x0404 }
                r21 = r0
                int r20 = r20 - r21
                r0 = r20
                r1 = r14
                r1.bottom = r0     // Catch:{ all -> 0x0404 }
                r0 = r26
                android.graphics.Canvas r0 = r0.cv     // Catch:{ all -> 0x0404 }
                r20 = r0
                r0 = r26
                android.graphics.Bitmap[][] r0 = r0.bmp_plant     // Catch:{ all -> 0x0404 }
                r21 = r0
                r0 = r26
                com.safetest.pvz.ShowGame r0 = com.safetest.pvz.ShowGame.this     // Catch:{ all -> 0x0404 }
                r22 = r0
                r0 = r22
                int[] r0 = r0.seedpacket_kind     // Catch:{ all -> 0x0404 }
                r22 = r0
                r22 = r22[r16]     // Catch:{ all -> 0x0404 }
                r21 = r21[r22]     // Catch:{ all -> 0x0404 }
                r22 = 0
                r21 = r21[r22]     // Catch:{ all -> 0x0404 }
                r22 = 0
                r0 = r26
                android.graphics.Paint r0 = r0.pt     // Catch:{ all -> 0x0404 }
                r23 = r0
                r0 = r20
                r1 = r21
                r2 = r22
                r3 = r14
                r4 = r23
                r0.drawBitmap(r1, r2, r3, r4)     // Catch:{ all -> 0x0404 }
                goto L_0x0334
            L_0x059e:
                r0 = r26
                android.graphics.Canvas r0 = r0.cv     // Catch:{ all -> 0x0404 }
                r20 = r0
                r0 = r26
                android.graphics.Bitmap[][] r0 = r0.bmp_plant     // Catch:{ all -> 0x0404 }
                r21 = r0
                r0 = r26
                com.safetest.pvz.ShowGame r0 = com.safetest.pvz.ShowGame.this     // Catch:{ all -> 0x0404 }
                r22 = r0
                r0 = r22
                int[] r0 = r0.seedpacket_kind     // Catch:{ all -> 0x0404 }
                r22 = r0
                r22 = r22[r16]     // Catch:{ all -> 0x0404 }
                r21 = r21[r22]     // Catch:{ all -> 0x0404 }
                r22 = 0
                r21 = r21[r22]     // Catch:{ all -> 0x0404 }
                r22 = 0
                r0 = r26
                android.graphics.Paint r0 = r0.pt     // Catch:{ all -> 0x0404 }
                r23 = r0
                r0 = r20
                r1 = r21
                r2 = r22
                r3 = r13
                r4 = r23
                r0.drawBitmap(r1, r2, r3, r4)     // Catch:{ all -> 0x0404 }
                goto L_0x03db
            L_0x05d4:
                r0 = r26
                com.safetest.pvz.ShowGame r0 = com.safetest.pvz.ShowGame.this     // Catch:{ all -> 0x0404 }
                r20 = r0
                r0 = r20
                int[] r0 = r0.seedpacket_cool_cur     // Catch:{ all -> 0x0404 }
                r20 = r0
                r20 = r20[r9]     // Catch:{ all -> 0x0404 }
                if (r20 <= 0) goto L_0x05f8
                r0 = r26
                com.safetest.pvz.ShowGame r0 = com.safetest.pvz.ShowGame.this     // Catch:{ all -> 0x0404 }
                r20 = r0
                r0 = r20
                int[] r0 = r0.seedpacket_cool_cur     // Catch:{ all -> 0x0404 }
                r20 = r0
                r21 = r20[r9]     // Catch:{ all -> 0x0404 }
                r22 = 1
                int r21 = r21 - r22
                r20[r9] = r21     // Catch:{ all -> 0x0404 }
            L_0x05f8:
                int r9 = r9 + 1
                goto L_0x03dc
            L_0x05fc:
                r19 = move-exception
                goto L_0x0014
            */
            throw new UnsupportedOperationException("Method not decompiled: com.safetest.pvz.ShowGame.DrawGame.run():void");
        }

        /* access modifiers changed from: package-private */
        public void f_draw_back() {
            this.pt.setARGB(255, 0, 0, 0);
            this.cv.drawBitmap(this.bmp_bk1, (Rect) null, new Rect(0, 0, ShowGame.this.win_w, ShowGame.this.win_h), this.pt);
            this.cv.drawBitmap(this.bmp_seedbank, (Rect) null, ShowGame.this.r_map_seedbank, this.pt);
            this.cv.drawBitmap(this.bmp_shovel_bk, (Rect) null, ShowGame.this.r_shovel, this.pt);
            this.pt.setTextSize((float) ((ShowGame.this.win_h * 10) / 320));
            float[] widths = new float[5];
            long num = (long) this.pt.getTextWidths(new StringBuilder().append(ShowGame.this.sun_num).toString(), widths);
            float w_sum = 0.0f;
            for (int i = 0; ((long) i) < num; i++) {
                w_sum += widths[i];
            }
            this.cv.drawText(new StringBuilder().append(ShowGame.this.sun_num).toString(), ((float) ShowGame.this.sun_num_x) - (w_sum / 2.0f), (float) ShowGame.this.sun_num_y, this.pt);
            for (int i2 = 0; i2 < ShowGame.this.r_seedpacket.length; i2++) {
                if (ShowGame.this.sun_num < ShowGame.this.seedpacket_cost[i2] || ShowGame.this.seedpacket_cool_cur[i2] > 0) {
                    this.pt.setARGB(100, 0, 0, 0);
                } else {
                    this.pt.setARGB(255, 0, 0, 0);
                }
                this.cv.drawBitmap(this.bmp_seedpacket[i2], (Rect) null, ShowGame.this.r_seedpacket[i2], this.pt);
                this.pt.setARGB(100, 0, 0, 0);
                Rect r_cool = new Rect();
                r_cool.left = ShowGame.this.r_seedpacket[i2].left;
                r_cool.right = ShowGame.this.r_seedpacket[i2].right;
                r_cool.top = ShowGame.this.r_seedpacket[i2].top;
                r_cool.bottom = r_cool.top + ((ShowGame.this.r_seedpacket[i2].height() * ShowGame.this.seedpacket_cool_cur[i2]) / ShowGame.this.seedpacket_cool_set[i2]);
                this.cv.drawRect(r_cool, this.pt);
                this.pt.setARGB(255, 0, 0, 0);
            }
        }

        /* access modifiers changed from: package-private */
        public void f_draw_flag() {
            this.pt.setARGB(255, 255, 255, 126);
            this.cv.drawBitmap(this.bmp_flagmeter_bk, (Rect) null, ShowGame.this.r_flagmeter_bk, this.pt);
            Rect r_flag_pos = new Rect();
            r_flag_pos.right = ShowGame.this.r_flagmeter_bk.right - 4;
            r_flag_pos.left = (int) (((float) r_flag_pos.right) - (((float) (ShowGame.this.r_flagmeter_bk.width() - (4 * 2))) * ((((float) (this.cur_time - this.zomb_start_time)) % ((float) this.zomb_biggroup_time)) / ((float) this.zomb_biggroup_time))));
            r_flag_pos.top = ShowGame.this.r_flagmeter_bk.top + 4;
            r_flag_pos.bottom = ShowGame.this.r_flagmeter_bk.bottom - 4;
            this.cv.drawRect(r_flag_pos, this.pt);
            Rect r_zombhead = new Rect();
            r_zombhead.top = ShowGame.this.r_flagmeter_bk.top - 4;
            r_zombhead.bottom = ShowGame.this.r_flagmeter_bk.bottom;
            r_zombhead.right = r_flag_pos.left + 4;
            r_zombhead.left = r_zombhead.right - r_zombhead.height();
            this.cv.drawBitmap(this.bmp_zombhead, (Rect) null, r_zombhead, this.pt);
            this.pt.setARGB(255, 0, 0, 0);
            String txt = "第" + (((this.cur_time - this.zomb_start_time) / this.zomb_biggroup_time) + 1) + "轮";
            this.pt.setTextSize((float) ShowGame.this.r_flagmeter_bk.height());
            float[] widths = new float[10];
            long num = (long) this.pt.getTextWidths(txt, widths);
            float w_sum = 0.0f;
            for (int i = 0; ((long) i) < num; i++) {
                w_sum += widths[i];
            }
            this.cv.drawText(txt, ((float) (ShowGame.this.r_flagmeter_bk.left - 10)) - w_sum, (float) ShowGame.this.r_flagmeter_bk.bottom, this.pt);
        }

        /* access modifiers changed from: package-private */
        public void f_draw_plant() {
            this.pt.setARGB(255, 0, 0, 0);
            for (int i = 0; i < 5; i++) {
                for (int j = 0; j < 9; j++) {
                    if (ShowGame.this.plant_kind[i][j] != -1) {
                        if (ShowGame.this.plant_kind[i][j] == 6 && ShowGame.this.plant_health[i][j] < ShowGame.this.plant_health_set[5] / 3) {
                            ShowGame.this.plant_kind[i][j] = 7;
                            this.plant_bmp_delay_cur[i][j] = 1;
                            ShowGame.this.plant_frame[i][j] = 0;
                        }
                        if (ShowGame.this.plant_kind[i][j] == 5 && ShowGame.this.plant_health[i][j] < (ShowGame.this.plant_health_set[5] * 2) / 3) {
                            ShowGame.this.plant_kind[i][j] = 6;
                            this.plant_bmp_delay_cur[i][j] = 1;
                            ShowGame.this.plant_frame[i][j] = 0;
                        }
                        int kind = ShowGame.this.plant_kind[i][j];
                        int frame = ShowGame.this.plant_frame[i][j];
                        this.cv.drawBitmap(this.bmp_plant[kind][frame], (Rect) null, new Rect(ShowGame.this.r_map_cell[i][j].left + this.plant_margen, ShowGame.this.r_map_cell[i][j].top + this.plant_margen, ShowGame.this.r_map_cell[i][j].right - this.plant_margen, ShowGame.this.r_map_cell[i][j].bottom - this.plant_margen), this.pt);
                        if (this.plant_bmp_delay_cur[i][j] >= this.plant_bmp_delay_set[kind]) {
                            int[] iArr = ShowGame.this.plant_frame[i];
                            iArr[j] = iArr[j] + 1;
                            this.plant_bmp_delay_cur[i][j] = 1;
                        } else {
                            int[] iArr2 = this.plant_bmp_delay_cur[i];
                            iArr2[j] = iArr2[j] + 1;
                        }
                        if (ShowGame.this.plant_frame[i][j] >= this.bmp_plant[kind].length) {
                            ShowGame.this.plant_frame[i][j] = 0;
                            int[] iArr3 = ShowGame.this.plant_fire_delay_cur[i];
                            iArr3[j] = iArr3[j] + 1;
                            if (ShowGame.this.plant_fire_delay_cur[i][j] >= ShowGame.this.plant_fire_delay_set[kind]) {
                                switch (kind) {
                                    case 0:
                                        f_add_sun_static(ShowGame.this.r_map_cell[i][j].right, ShowGame.this.r_map_cell[i][j].bottom);
                                        ShowGame.this.plant_fire_delay_cur[i][j] = 1;
                                        break;
                                    case 1:
                                        if (ShowGame.this.zomb_num_row[i] <= 0) {
                                            break;
                                        } else {
                                            f_add_bullet(i, ShowGame.this.r_map_cell[i][j].right - this.plant_margen, 0);
                                            ShowGame.this.plant_fire_delay_cur[i][j] = 1;
                                            break;
                                        }
                                    case 2:
                                        if (ShowGame.this.zomb_num_row[i] <= 0) {
                                            break;
                                        } else {
                                            f_add_bullet(i, ShowGame.this.r_map_cell[i][j].right, 0);
                                            f_add_bullet(i, ShowGame.this.r_map_cell[i][j].right - this.plant_margen, 0);
                                            ShowGame.this.plant_fire_delay_cur[i][j] = 1;
                                            break;
                                        }
                                    case 3:
                                        if (ShowGame.this.zomb_num_row[i] <= 0) {
                                            break;
                                        } else {
                                            f_add_bullet(i, ShowGame.this.r_map_cell[i][j].right - this.plant_margen, 1);
                                            ShowGame.this.plant_fire_delay_cur[i][j] = 1;
                                            break;
                                        }
                                    case 4:
                                        if (ShowGame.this.zomb_num_row[i] <= 0) {
                                            break;
                                        } else {
                                            f_add_bullet(i, ShowGame.this.r_map_cell[i][j].right, 0);
                                            f_add_bullet(i, ShowGame.this.r_map_cell[i][j].right - this.plant_margen, 0);
                                            f_add_bullet(i, ShowGame.this.r_map_cell[i][j].right - (this.plant_margen * 2), 0);
                                            f_add_bullet(i, ShowGame.this.r_map_cell[i][j].right - (this.plant_margen * 3), 0);
                                            ShowGame.this.plant_fire_delay_cur[i][j] = 1;
                                            break;
                                        }
                                }
                            }
                        }
                    }
                }
            }
        }

        /* access modifiers changed from: package-private */
        public void f_add_bullet(int row, int x, int kind) {
            int j = 0;
            while (ShowGame.this.bullet_kind[row][j] != -1) {
                j++;
                if (j >= ShowGame.this.bullet_kind[row].length) {
                    return;
                }
            }
            ShowGame.this.bullet_kind[row][j] = kind;
            ShowGame.this.bullet_position_x[row][j] = x;
        }

        /* access modifiers changed from: package-private */
        public void f_draw_bullet() {
            for (int i = 0; i < 5; i++) {
                for (int j = 0; j < ShowGame.this.bullet_kind[0].length; j++) {
                    if (ShowGame.this.bullet_kind[i][j] != -1) {
                        int kind = ShowGame.this.bullet_kind[i][j];
                        this.cv.drawBitmap(this.bmp_bullet[kind], (Rect) null, new Rect(ShowGame.this.bullet_position_x[i][j] - (ShowGame.this.bullet_w / 2), ShowGame.this.bullet_position_y[i] - (ShowGame.this.bullet_h / 2), ShowGame.this.bullet_position_x[i][j] + (ShowGame.this.bullet_w / 2), ShowGame.this.bullet_position_y[i] + (ShowGame.this.bullet_h / 2)), this.pt);
                        int[] iArr = ShowGame.this.bullet_position_x[i];
                        iArr[j] = iArr[j] + ShowGame.this.bullet_x_add[kind];
                        int z = 0;
                        while (true) {
                            if (z < ShowGame.this.zomb_kind[i].length) {
                                if (ShowGame.this.zomb_kind[i][z] == -1 || ((float) ShowGame.this.bullet_position_x[i][j]) <= ShowGame.this.zomb_position_x[i][z] - ((float) (ShowGame.this.zomb_w / 2)) || ((float) ShowGame.this.bullet_position_x[i][j]) >= ShowGame.this.zomb_position_x[i][z] + ((float) (ShowGame.this.zomb_w / 2))) {
                                    z++;
                                } else {
                                    switch (kind) {
                                        case 0:
                                            int[] iArr2 = ShowGame.this.zomb_health[i];
                                            iArr2[z] = iArr2[z] - 1;
                                            break;
                                        case 1:
                                            int[] iArr3 = ShowGame.this.zomb_health[i];
                                            iArr3[z] = iArr3[z] - 1;
                                            ShowGame.this.zomb_status[i][z] = 2;
                                            ShowGame.this.zomb_status_time[i][z] = 100;
                                            break;
                                    }
                                    ShowGame.this.bullet_kind[i][j] = -1;
                                    if (ShowGame.this.zomb_kind[i][z] >= 1 && ShowGame.this.zomb_health[i][z] < 10) {
                                        ShowGame.this.zomb_kind[i][z] = 0;
                                    }
                                    if (ShowGame.this.zomb_health[i][z] < 0) {
                                        ShowGame.this.zomb_kind[i][z] = -1;
                                        int[] iArr4 = ShowGame.this.zomb_num_row;
                                        iArr4[i] = iArr4[i] - 1;
                                    }
                                }
                            }
                        }
                        if (ShowGame.this.bullet_position_x[i][j] > ShowGame.this.win_w) {
                            ShowGame.this.bullet_kind[i][j] = -1;
                        }
                    }
                }
            }
        }

        /* access modifiers changed from: package-private */
        public void f_add_zomb(int row, int kind) {
            int j = 0;
            while (ShowGame.this.zomb_kind[row][j] != -1) {
                j++;
                if (j >= ShowGame.this.zomb_kind[row].length) {
                    return;
                }
            }
            ShowGame.this.zomb_kind[row][j] = kind;
            int[] iArr = ShowGame.this.zomb_num_row;
            iArr[row] = iArr[row] + 1;
            ShowGame.this.zomb_position_x[row][j] = (float) ShowGame.this.win_w;
            ShowGame.this.zomb_health[row][j] = this.zomb_health_set[kind];
        }

        /* access modifiers changed from: package-private */
        public void f_draw_zomb() {
            for (int i = 0; i < 5; i++) {
                for (int j = 0; j < ShowGame.this.zomb_kind[0].length; j++) {
                    if (ShowGame.this.zomb_kind[i][j] != -1) {
                        int kind = ShowGame.this.zomb_kind[i][j];
                        int frame = ShowGame.this.zomb_frame[i][j];
                        int status = ShowGame.this.zomb_status[i][j];
                        Rect r_zomb = new Rect(((int) ShowGame.this.zomb_position_x[i][j]) - (ShowGame.this.zomb_w / 2), ShowGame.this.zomb_position_y[i] - (ShowGame.this.zomb_h / 2), ((int) ShowGame.this.zomb_position_x[i][j]) + (ShowGame.this.zomb_w / 2), ShowGame.this.zomb_position_y[i] + (ShowGame.this.zomb_h / 2));
                        if (status == 1) {
                            this.pt.setARGB(255, 0, 0, 0);
                        } else if (status == 2) {
                            this.pt.setARGB(200, 0, 0, 0);
                            if (ShowGame.this.zomb_status_time[i][j] > 0) {
                                int[] iArr = ShowGame.this.zomb_status_time[i];
                                iArr[j] = iArr[j] - 1;
                            } else {
                                ShowGame.this.zomb_status[i][j] = 1;
                            }
                        }
                        this.cv.drawBitmap(this.bmp_zomb[kind][frame], (Rect) null, r_zomb, this.pt);
                        this.pt.setARGB(255, 0, 0, 0);
                        if (this.zomb_bmp_delay_cur[i][j] >= this.zomb_bmp_delay_set[kind]) {
                            int[] iArr2 = ShowGame.this.zomb_frame[i];
                            iArr2[j] = iArr2[j] + 1;
                            this.zomb_bmp_delay_cur[i][j] = 1;
                        } else {
                            int[] iArr3 = this.zomb_bmp_delay_cur[i];
                            iArr3[j] = iArr3[j] + 1;
                        }
                        if (ShowGame.this.zomb_frame[i][j] >= this.bmp_zomb[kind].length) {
                            ShowGame.this.zomb_frame[i][j] = 1;
                        }
                        boolean can_move = true;
                        int p = 0;
                        while (true) {
                            if (p < 9) {
                                if (ShowGame.this.plant_kind[i][p] == -1 || Math.abs(((float) ShowGame.this.r_map_cell[i][p].right) - ShowGame.this.zomb_position_x[i][j]) >= 5.0f) {
                                    p++;
                                } else {
                                    can_move = false;
                                    int[] iArr4 = ShowGame.this.plant_health[i];
                                    iArr4[p] = iArr4[p] - 1;
                                    if (ShowGame.this.plant_health[i][p] < 0) {
                                        ShowGame.this.plant_kind[i][p] = -1;
                                    }
                                }
                            }
                        }
                        if (can_move) {
                            switch (status) {
                                case 1:
                                    float[] fArr = ShowGame.this.zomb_position_x[i];
                                    fArr[j] = fArr[j] - ShowGame.this.zomb_x_add[kind];
                                    break;
                                case 2:
                                    float[] fArr2 = ShowGame.this.zomb_position_x[i];
                                    fArr2[j] = fArr2[j] - (ShowGame.this.zomb_x_add[kind] / 2.0f);
                                    break;
                            }
                        }
                        if (ShowGame.this.zomb_position_x[i][j] < 0.0f) {
                            Intent i_end = new Intent();
                            i_end.setClass(ShowGame.this, End.class);
                            ShowGame.this.startActivity(i_end);
                            ShowGame.this.finish();
                        }
                    }
                }
            }
        }

        /* access modifiers changed from: package-private */
        public void f_draw_sun() {
            for (int i = 0; i < ShowGame.this.r_sun_drop.length; i++) {
                if (ShowGame.this.r_sun_drop[i].left != -1) {
                    this.cv.drawBitmap(this.bmp_sun, (Rect) null, ShowGame.this.r_sun_drop[i], this.pt);
                    ShowGame.this.r_sun_drop[i].top += ShowGame.this.sun_y_add;
                    ShowGame.this.r_sun_drop[i].bottom += ShowGame.this.sun_y_add;
                    if (ShowGame.this.r_sun_drop[i].bottom > ShowGame.this.win_h) {
                        ShowGame.this.r_sun_drop[i].left = -1;
                        ShowGame.this.r_sun_drop[i].right = -1;
                    }
                }
            }
            for (int i2 = 0; i2 < ShowGame.this.sun_static_life.length; i2++) {
                if (ShowGame.this.sun_static_life[i2] > 0) {
                    this.cv.drawBitmap(this.bmp_sun, (Rect) null, ShowGame.this.r_sun_static[i2], this.pt);
                    int[] iArr = ShowGame.this.sun_static_life;
                    iArr[i2] = iArr[i2] - 1;
                }
            }
            for (int i3 = 0; i3 < ShowGame.this.r_sun_move.length; i3++) {
                if (ShowGame.this.r_sun_move[i3].left != -1) {
                    this.cv.drawBitmap(this.bmp_sun, (Rect) null, ShowGame.this.r_sun_move[i3], this.pt);
                    double length = Math.sqrt((double) (((ShowGame.this.r_sun_move[i3].left - ShowGame.this.sun_destination_x) * (ShowGame.this.r_sun_move[i3].left - ShowGame.this.sun_destination_x)) + ((ShowGame.this.r_sun_move[i3].top - ShowGame.this.sun_destination_y) * (ShowGame.this.r_sun_move[i3].top - ShowGame.this.sun_destination_y))));
                    if (length > ((double) ShowGame.this.sun_select_move_speed)) {
                        Rect rect = ShowGame.this.r_sun_move[i3];
                        rect.left = (int) (((double) rect.left) - (((double) ((ShowGame.this.r_sun_move[i3].left - ShowGame.this.sun_destination_x) * ShowGame.this.sun_select_move_speed)) / length));
                        ShowGame.this.r_sun_move[i3].right = ShowGame.this.r_sun_move[i3].left + ShowGame.this.sun_w;
                        Rect rect2 = ShowGame.this.r_sun_move[i3];
                        rect2.top = (int) (((double) rect2.top) - (((double) ((ShowGame.this.r_sun_move[i3].top - ShowGame.this.sun_destination_y) * ShowGame.this.sun_select_move_speed)) / length));
                        ShowGame.this.r_sun_move[i3].bottom = ShowGame.this.r_sun_move[i3].top + ShowGame.this.sun_h;
                    } else {
                        ShowGame.this.sun_num += 25;
                        ShowGame.this.r_sun_move[i3].left = -1;
                    }
                }
            }
        }

        /* access modifiers changed from: package-private */
        public void f_add_sun_drop() {
            int i = 0;
            while (ShowGame.this.r_sun_drop[i].left != -1) {
                i++;
                if (i >= ShowGame.this.r_sun_drop.length) {
                    return;
                }
            }
            ShowGame.this.r_sun_drop[i].left = (int) (((double) ShowGame.this.r_map.left) + (((double) (ShowGame.this.r_map.width() - ShowGame.this.sun_w)) * Math.random()));
            ShowGame.this.r_sun_drop[i].right = ShowGame.this.r_sun_drop[i].left + ShowGame.this.sun_w;
            ShowGame.this.r_sun_drop[i].top = -ShowGame.this.sun_h;
            ShowGame.this.r_sun_drop[i].bottom = 0;
        }

        /* access modifiers changed from: package-private */
        public void f_add_sun_static(int right, int bottom) {
            int i = 0;
            while (ShowGame.this.sun_static_life[i] != 0) {
                i++;
                if (i >= ShowGame.this.sun_static_life.length) {
                    return;
                }
            }
            ShowGame.this.sun_static_life[i] = ShowGame.this.sun_static_life_set;
            ShowGame.this.r_sun_static[i].right = right;
            ShowGame.this.r_sun_static[i].left = ShowGame.this.r_sun_static[i].right - ShowGame.this.sun_w;
            ShowGame.this.r_sun_static[i].bottom = bottom;
            ShowGame.this.r_sun_static[i].top = ShowGame.this.r_sun_static[i].bottom - ShowGame.this.sun_h;
        }

        public void surfaceCreated(SurfaceHolder holder) {
            new Thread(this).start();
        }

        public void surfaceDestroyed(SurfaceHolder holder) {
        }
    }
}
