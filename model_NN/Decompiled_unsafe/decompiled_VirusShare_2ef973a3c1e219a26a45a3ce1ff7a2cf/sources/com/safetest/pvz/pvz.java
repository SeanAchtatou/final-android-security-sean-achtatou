package com.safetest.pvz;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import com.imadpush.ad.poster.AppPosterManager;

public class pvz extends Activity {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.main);
        new AppPosterManager(this);
        getWindow().setFlags(1024, 1024);
        ((ImageView) findViewById(R.id.ImageView01)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent i = new Intent();
                i.setClass(pvz.this, ShowGame.class);
                pvz.this.startActivity(i);
                pvz.this.finish();
            }
        });
    }
}
