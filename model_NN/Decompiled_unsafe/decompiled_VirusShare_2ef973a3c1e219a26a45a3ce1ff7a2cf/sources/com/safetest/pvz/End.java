package com.safetest.pvz;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class End extends Activity {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.end);
        getWindow().setFlags(1024, 1024);
        ((Button) findViewById(R.id.end_bn1)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent i = new Intent();
                i.setClass(End.this, ShowGame.class);
                End.this.startActivity(i);
                End.this.finish();
            }
        });
        ((Button) findViewById(R.id.end_bn2)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                End.this.finish();
            }
        });
    }
}
