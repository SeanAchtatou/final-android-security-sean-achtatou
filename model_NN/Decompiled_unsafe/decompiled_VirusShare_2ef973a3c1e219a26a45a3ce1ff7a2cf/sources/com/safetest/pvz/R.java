package com.safetest.pvz;

public final class R {

    public static final class attr {
        public static final int backgroundColor = 2130771969;
        public static final int backgroundTransparent = 2130771975;
        public static final int changeAdAnimation = 2130771974;
        public static final int isGoneWithoutAd = 2130771973;
        public static final int keywords = 2130771971;
        public static final int refreshInterval = 2130771972;
        public static final int testing = 2130771968;
        public static final int textColor = 2130771970;
    }

    public static final class drawable {
        public static final int bk1 = 2130837504;
        public static final int bullet_01 = 2130837505;
        public static final int bullet_02 = 2130837506;
        public static final int button1 = 2130837507;
        public static final int button2 = 2130837508;
        public static final int cover = 2130837509;
        public static final int end = 2130837510;
        public static final int flagmeter = 2130837511;
        public static final int icon = 2130837512;
        public static final int p_00_01 = 2130837513;
        public static final int p_00_02 = 2130837514;
        public static final int p_00_03 = 2130837515;
        public static final int p_00_04 = 2130837516;
        public static final int p_00_05 = 2130837517;
        public static final int p_00_06 = 2130837518;
        public static final int p_00_07 = 2130837519;
        public static final int p_00_08 = 2130837520;
        public static final int p_01_01 = 2130837521;
        public static final int p_01_02 = 2130837522;
        public static final int p_01_03 = 2130837523;
        public static final int p_01_04 = 2130837524;
        public static final int p_01_05 = 2130837525;
        public static final int p_01_06 = 2130837526;
        public static final int p_01_07 = 2130837527;
        public static final int p_01_08 = 2130837528;
        public static final int p_02_01 = 2130837529;
        public static final int p_02_02 = 2130837530;
        public static final int p_02_03 = 2130837531;
        public static final int p_02_04 = 2130837532;
        public static final int p_02_05 = 2130837533;
        public static final int p_02_06 = 2130837534;
        public static final int p_02_07 = 2130837535;
        public static final int p_02_08 = 2130837536;
        public static final int p_03_01 = 2130837537;
        public static final int p_03_02 = 2130837538;
        public static final int p_03_03 = 2130837539;
        public static final int p_03_04 = 2130837540;
        public static final int p_03_05 = 2130837541;
        public static final int p_03_06 = 2130837542;
        public static final int p_03_07 = 2130837543;
        public static final int p_03_08 = 2130837544;
        public static final int p_04_01 = 2130837545;
        public static final int p_04_02 = 2130837546;
        public static final int p_04_03 = 2130837547;
        public static final int p_04_04 = 2130837548;
        public static final int p_04_05 = 2130837549;
        public static final int p_04_06 = 2130837550;
        public static final int p_04_07 = 2130837551;
        public static final int p_04_08 = 2130837552;
        public static final int p_05_01 = 2130837553;
        public static final int p_05_02 = 2130837554;
        public static final int p_05_03 = 2130837555;
        public static final int p_05_04 = 2130837556;
        public static final int p_05_05 = 2130837557;
        public static final int p_05_06 = 2130837558;
        public static final int p_05_07 = 2130837559;
        public static final int p_05_08 = 2130837560;
        public static final int p_06_01 = 2130837561;
        public static final int p_06_02 = 2130837562;
        public static final int p_06_03 = 2130837563;
        public static final int p_06_04 = 2130837564;
        public static final int p_06_05 = 2130837565;
        public static final int p_06_06 = 2130837566;
        public static final int p_06_07 = 2130837567;
        public static final int p_06_08 = 2130837568;
        public static final int p_07_01 = 2130837569;
        public static final int p_07_02 = 2130837570;
        public static final int p_07_03 = 2130837571;
        public static final int p_07_04 = 2130837572;
        public static final int p_07_05 = 2130837573;
        public static final int p_07_06 = 2130837574;
        public static final int p_07_07 = 2130837575;
        public static final int p_07_08 = 2130837576;
        public static final int seed_00 = 2130837577;
        public static final int seed_01 = 2130837578;
        public static final int seed_02 = 2130837579;
        public static final int seed_03 = 2130837580;
        public static final int seed_04 = 2130837581;
        public static final int seed_05 = 2130837582;
        public static final int seedbank = 2130837583;
        public static final int shovel = 2130837584;
        public static final int shovel_bk = 2130837585;
        public static final int sun = 2130837586;
        public static final int z_00_01 = 2130837587;
        public static final int z_00_02 = 2130837588;
        public static final int z_00_03 = 2130837589;
        public static final int z_00_04 = 2130837590;
        public static final int z_00_05 = 2130837591;
        public static final int z_00_06 = 2130837592;
        public static final int z_00_07 = 2130837593;
        public static final int z_00_08 = 2130837594;
        public static final int z_01_01 = 2130837595;
        public static final int z_01_02 = 2130837596;
        public static final int z_01_03 = 2130837597;
        public static final int z_01_04 = 2130837598;
        public static final int z_01_05 = 2130837599;
        public static final int z_01_06 = 2130837600;
        public static final int z_01_07 = 2130837601;
        public static final int z_01_08 = 2130837602;
        public static final int z_02_01 = 2130837603;
        public static final int z_02_02 = 2130837604;
        public static final int z_02_03 = 2130837605;
        public static final int z_02_04 = 2130837606;
        public static final int z_02_05 = 2130837607;
        public static final int z_02_06 = 2130837608;
        public static final int z_02_07 = 2130837609;
        public static final int z_02_08 = 2130837610;
        public static final int zombhead = 2130837611;
    }

    public static final class id {
        public static final int ImageView01 = 2131099652;
        public static final int RelativeLayout01 = 2131099648;
        public static final int ad = 2131099650;
        public static final int end_bn1 = 2131099649;
        public static final int end_bn2 = 2131099651;
    }

    public static final class layout {
        public static final int end = 2130903040;
        public static final int main = 2130903041;
    }

    public static final class string {
        public static final int app_name = 2131034112;
    }

    public static final class styleable {
        public static final int[] net_youmi_android_AdView = {R.attr.testing, R.attr.backgroundColor, R.attr.textColor, R.attr.keywords, R.attr.refreshInterval, R.attr.isGoneWithoutAd, R.attr.changeAdAnimation, R.attr.backgroundTransparent};
        public static final int net_youmi_android_AdView_backgroundColor = 1;
        public static final int net_youmi_android_AdView_backgroundTransparent = 7;
        public static final int net_youmi_android_AdView_changeAdAnimation = 6;
        public static final int net_youmi_android_AdView_isGoneWithoutAd = 5;
        public static final int net_youmi_android_AdView_keywords = 3;
        public static final int net_youmi_android_AdView_refreshInterval = 4;
        public static final int net_youmi_android_AdView_testing = 0;
        public static final int net_youmi_android_AdView_textColor = 2;
    }

    public static final class xml {
        public static final int btn = 2130968576;
    }
}
