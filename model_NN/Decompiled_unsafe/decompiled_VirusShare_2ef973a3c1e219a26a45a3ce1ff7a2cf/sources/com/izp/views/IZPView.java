package com.izp.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;

public class IZPView extends RelativeLayout {
    public String adType;
    private b adm = b.a();
    public IZPDelegate delegate;
    public boolean isDev = true;
    public String productID;
    private boolean started;

    public IZPView(Context context) {
        super(context);
        b.a().a(this);
        setBackgroundColor(0);
    }

    public IZPView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        b.a().a(this);
        setBackgroundColor(0);
    }

    public IZPView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        b.a().a(this);
        setBackgroundColor(0);
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        b.a().U = true;
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        b.a().U = false;
    }

    /* access modifiers changed from: protected */
    public void onVisibilityChanged(View view, int i) {
        if (i == 0) {
            b.a().W = true;
        } else {
            b.a().W = false;
        }
    }

    public void onWindowFocusChanged(boolean z) {
        b.a().V = z;
    }

    public void startAdExchange() {
        if (this.started) {
            return;
        }
        if (this.productID == null) {
            if (this.delegate != null) {
                this.delegate.errorReport(this, 100, "product id is null !");
            }
        } else if (this.adType != null) {
            this.adm.a(this.productID);
            this.adm.b(this.adType);
            this.adm.a(this.isDev);
            this.adm.a(this.delegate);
            this.adm.d();
            this.started = true;
        } else if (this.delegate != null) {
            this.delegate.errorReport(this, 101, "adType is null !");
        }
    }

    public void stopAdExchange() {
        if (this.started) {
            this.adm.e();
            this.started = false;
        }
    }
}
