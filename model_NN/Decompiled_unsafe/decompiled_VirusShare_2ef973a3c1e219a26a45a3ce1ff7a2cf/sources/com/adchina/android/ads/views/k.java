package com.adchina.android.ads.views;

import java.util.TimerTask;

final class k extends TimerTask {
    final /* synthetic */ FullScreenAdActivity a;

    k(FullScreenAdActivity fullScreenAdActivity) {
        this.a = fullScreenAdActivity;
    }

    public final void run() {
        this.a.sendMessage(Integer.valueOf(this.a.mNumber));
        FullScreenAdActivity fullScreenAdActivity = this.a;
        fullScreenAdActivity.mNumber = fullScreenAdActivity.mNumber - 1;
    }
}
