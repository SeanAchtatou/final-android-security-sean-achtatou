package com.adchina.android.ads;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.Environment;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Window;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.UUID;

public class Utils {
    private static String a;

    public static int GetRandomNumber() {
        return (int) (Math.random() * 10000.0d);
    }

    public static void closeStream(InputStream inputStream) {
        if (inputStream != null) {
            try {
                inputStream.close();
            } catch (IOException e) {
            }
        }
    }

    public static void closeStream(OutputStream outputStream) {
        if (outputStream != null) {
            try {
                outputStream.flush();
                outputStream.close();
            } catch (IOException e) {
            }
        }
    }

    public static String concatString(Object... objArr) {
        StringBuffer stringBuffer = new StringBuffer();
        for (Object append : objArr) {
            stringBuffer.append(append);
        }
        return stringBuffer.toString();
    }

    public static Bitmap convertByteArrayTobitmap(byte[] bArr) {
        try {
            return BitmapFactory.decodeByteArray(bArr, 0, bArr.length);
        } catch (Exception e) {
            return null;
        }
    }

    public static Bitmap convertStreamToBitmap(InputStream inputStream) {
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            while (true) {
                int read = inputStream.read();
                if (read == -1) {
                    byte[] byteArray = byteArrayOutputStream.toByteArray();
                    closeStream(inputStream);
                    return convertByteArrayTobitmap(byteArray);
                }
                byteArrayOutputStream.write(read);
            }
        } catch (Exception e) {
            return null;
        }
    }

    protected static String convertStreamToString(InputStream inputStream) {
        new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder sb = new StringBuilder();
        try {
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
            char[] cArr = new char[1000];
            while (true) {
                int read = inputStreamReader.read(cArr);
                if (read < 0) {
                    break;
                }
                sb.append(cArr, 0, read);
            }
        } catch (IOException e) {
            Log.e("AdChinaError", concatString("convertStreamToString:", e.toString()));
        }
        return sb.toString();
    }

    public static int dip2px(Context context, float f) {
        return (int) ((context.getResources().getDisplayMetrics().density * f) + 0.5f);
    }

    public static String getActiveNetworkType(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        if (activeNetworkInfo == null || !connectivityManager.getBackgroundDataSetting()) {
            return "";
        }
        StringBuffer stringBuffer = new StringBuffer();
        if (activeNetworkInfo.isConnected()) {
            stringBuffer.append(activeNetworkInfo.getTypeName());
            stringBuffer.append(",");
            stringBuffer.append(activeNetworkInfo.getSubtypeName());
        }
        return stringBuffer.toString();
    }

    public static String getIMEI(Context context) {
        WifiManager wifiManager;
        if (a == null) {
            String deviceId = ((TelephonyManager) context.getSystemService("phone")).getDeviceId();
            a = deviceId;
            if (!(deviceId != null || (wifiManager = (WifiManager) context.getSystemService("wifi")) == null || wifiManager.getConnectionInfo() == null || wifiManager.getConnectionInfo().getMacAddress() == null)) {
                a = new UUID((long) wifiManager.getConnectionInfo().getMacAddress().hashCode(), (long) "adchina".hashCode()).toString();
            }
            if (a == null) {
                a = Settings.Secure.getString(context.getContentResolver(), "android_id");
            }
        }
        return a == null ? "" : a;
    }

    public static String getNetworkTypes(Context context) {
        NetworkInfo[] allNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getAllNetworkInfo();
        StringBuffer stringBuffer = new StringBuffer();
        if (allNetworkInfo != null) {
            for (NetworkInfo networkInfo : allNetworkInfo) {
                if (networkInfo.isConnected()) {
                    stringBuffer.append(",");
                    stringBuffer.append(networkInfo.getTypeName());
                    stringBuffer.append(",");
                    stringBuffer.append(networkInfo.getSubtypeName());
                }
            }
        }
        return stringBuffer.length() > 0 ? stringBuffer.substring(1) : "";
    }

    public static String getNowTime(String str) {
        return new SimpleDateFormat(str).format((Object) new Date());
    }

    public static String getSDPath() {
        File file = null;
        if (Environment.getExternalStorageState().equals("mounted")) {
            file = Environment.getExternalStorageDirectory();
        }
        return file.toString();
    }

    public static boolean isCachedFileTimeout(String str) {
        try {
            return isCachedFileTimeout(str, 7);
        } catch (ParseException e) {
            Log.e("AdChinaError", concatString("isCachedFileTimeout:", e.toString()));
            return true;
        }
    }

    public static boolean isCachedFileTimeout(String str, int i) {
        if (str.length() < 14) {
            return true;
        }
        Date parse = new SimpleDateFormat("yyyyMMddHHmmss").parse(str.substring(0, 14));
        Calendar instance = Calendar.getInstance();
        instance.setTime(parse);
        instance.add(5, i);
        return instance.getTime().before(new Date());
    }

    public static boolean isWifiOrNotRoaming3G(Context context) {
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        if (activeNetworkInfo == null || !connectivityManager.getBackgroundDataSetting()) {
            return false;
        }
        int type = activeNetworkInfo.getType();
        activeNetworkInfo.getSubtype();
        if (type == 1) {
            return activeNetworkInfo.isConnected();
        }
        String upperCase = concatString(activeNetworkInfo.getTypeName(), ",", activeNetworkInfo.getSubtypeName()).toUpperCase();
        if (type != 0 || ((!upperCase.contains("UMTS") && !upperCase.contains("EVDO") && !upperCase.contains("WCDMA") && !upperCase.contains("HSDPA") && !upperCase.contains("HSUPA")) || telephonyManager.isNetworkRoaming())) {
            return false;
        }
        return activeNetworkInfo.isConnected();
    }

    public static Bitmap loadAssetsBitmap(Context context, String str) {
        try {
            return convertStreamToBitmap(loadAssetsInputStream(context, str));
        } catch (IOException e) {
            return null;
        }
    }

    public static InputStream loadAssetsInputStream(Context context, String str) {
        try {
            return context.getAssets().open(str);
        } catch (IOException e) {
            return null;
        }
    }

    public static int px2dip(Context context, float f) {
        return (int) ((f / context.getResources().getDisplayMetrics().density) + 0.5f);
    }

    public static void setWindowBackgroundColor(Window window, int i, int i2) {
        window.setBackgroundDrawable(new ColorDrawable((i2 << 24) | i));
    }

    public static void splitTrackUrl(String str, LinkedList linkedList) {
        linkedList.clear();
        int i = 0;
        while (true) {
            int indexOf = str.indexOf("|||", i);
            if (-1 == indexOf) {
                break;
            }
            String substring = str.substring(i, indexOf);
            if (substring.length() > 0) {
                linkedList.add(substring);
            }
            i = indexOf + 3;
        }
        String substring2 = str.substring(i);
        if (substring2.length() > 0) {
            linkedList.add(substring2);
        }
    }
}
