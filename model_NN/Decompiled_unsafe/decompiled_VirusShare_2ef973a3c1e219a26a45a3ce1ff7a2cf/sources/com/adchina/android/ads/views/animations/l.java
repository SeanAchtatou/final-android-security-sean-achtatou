package com.adchina.android.ads.views.animations;

import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;

final class l implements Runnable {
    final /* synthetic */ h a;

    l(h hVar) {
        this.a = hVar;
    }

    public final void run() {
        AlphaAnimation alphaAnimation = new AlphaAnimation(0.2f, 1.0f);
        alphaAnimation.setDuration(300);
        alphaAnimation.setFillAfter(true);
        alphaAnimation.setInterpolator(new AccelerateInterpolator());
        this.a.a.startAnimation(alphaAnimation);
    }
}
