package com.adchina.android.ads.views;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.ViewGroup;
import android.webkit.DownloadListener;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.RelativeLayout;
import com.adchina.android.ads.AdManager;
import com.adchina.android.ads.Utils;
import com.adchina.android.ads.r;
import com.adchina.android.ads.s;

public class AdBrowserView extends Activity implements DownloadListener, r {
    private Button mBtnClose;
    private RelativeLayout mLayout;
    private int mLoadCount = 0;
    private WebView mWebview;

    private void clearWebView() {
        if (this.mWebview != null) {
            this.mWebview.clearCache(true);
            this.mWebview.clearHistory();
            this.mLayout.removeView(this.mWebview);
            this.mWebview.destroy();
            this.mWebview = null;
            System.gc();
        }
    }

    /* access modifiers changed from: private */
    public void closeAdBrowserView() {
        clearWebView();
        finish();
    }

    private void setUpControls() {
        Display defaultDisplay = getWindowManager().getDefaultDisplay();
        defaultDisplay.getWidth();
        defaultDisplay.getHeight();
        if (this.mLayout == null) {
            this.mLayout = new RelativeLayout(getApplicationContext());
            this.mLayout.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
            setContentView(this.mLayout);
        }
        if (this.mWebview == null) {
            this.mWebview = new WebView(getApplicationContext());
            this.mLayout.addView(this.mWebview, new ViewGroup.LayoutParams(-1, -1));
            this.mWebview.getSettings().setJavaScriptEnabled(true);
            this.mWebview.setScrollBarStyle(0);
            this.mWebview.setWebViewClient(new b(this));
            this.mWebview.setDownloadListener(this);
            this.mWebview.loadUrl(getIntent().getExtras().getString("browserurl"));
        }
        if (this.mBtnClose == null) {
            this.mBtnClose = new Button(getApplicationContext());
            this.mLayout.addView(this.mBtnClose);
            if (AdManager.getCloseImg() == 0) {
                this.mBtnClose.setBackgroundDrawable(new BitmapDrawable(Utils.loadAssetsBitmap(getApplicationContext(), AdManager.getDefaultCloseImgPath())));
            } else {
                this.mBtnClose.setBackgroundResource(AdManager.getCloseImg());
            }
            this.mBtnClose.setOnClickListener(new a(this));
        }
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(Utils.dip2px(this, 32.0f), Utils.dip2px(this, 32.0f));
        layoutParams.addRule(12);
        layoutParams.addRule(11);
        this.mBtnClose.setLayoutParams(layoutParams);
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        setUpControls();
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        super.onCreate(bundle);
        setUpControls();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        this.mWebview = null;
    }

    public void onDownloadStart(String str, String str2, String str3, String str4, long j) {
        try {
            Uri parse = Uri.parse(str);
            Intent intent = new Intent("android.intent.action.VIEW", parse);
            if (str4.equalsIgnoreCase("video/mp4")) {
                intent.setDataAndType(parse, str4);
            }
            startActivity(intent);
            if (getIntent().getExtras().getString("browserurl").equals(str)) {
                closeAdBrowserView();
            }
        } catch (Exception e) {
            Log.e("AdChinaError", e.toString());
        }
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i == 4) {
            if (this.mWebview == null || !this.mWebview.canGoBack()) {
                clearWebView();
            } else {
                this.mWebview.goBack();
                return true;
            }
        }
        return super.onKeyDown(i, keyEvent);
    }

    public void onMoving(s sVar, s sVar2, float f, int i) {
        if (this.mWebview != null) {
            this.mWebview.loadUrl(String.format("javascript:ad.sensors.fireMovingSensor({x:%s,y:%s,z:%s},{x:%s,y:%s,z:%s},{speed:%s,orientation:%s});", Float.valueOf(sVar.a()), Float.valueOf(sVar.b()), Float.valueOf(sVar.c()), Float.valueOf(sVar2.a()), Float.valueOf(sVar2.b()), Float.valueOf(sVar2.c()), Float.valueOf(f), Integer.valueOf(i)));
        }
    }
}
