package com.adchina.android.ads;

public enum f {
    ESendClkTrack,
    ESendThdClkTrack,
    ESendBtnClkTrack,
    ESendBtnThdClkTrack,
    ESendShrinkFSClkTrack,
    ESendShrinkFSThdClkTrack,
    ESendVideoClkTrack,
    ESendVideoThdClkTrack,
    ESendVideoCloseClkTrack,
    ESendVideoCloseThdClkTrack,
    ESendFullScreenClkTrack,
    ESendFullScreenClkThdTrack,
    ESendFullScreenCloseTrack,
    EIdle
}
