package com.adchina.android.ads.views.animations;

import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;

final class j implements Runnable {
    /* access modifiers changed from: package-private */
    public final /* synthetic */ h a;

    j(h hVar) {
        this.a = hVar;
    }

    public final void run() {
        AlphaAnimation alphaAnimation = new AlphaAnimation(0.8f, 0.2f);
        alphaAnimation.setDuration(500);
        alphaAnimation.setFillAfter(true);
        alphaAnimation.setInterpolator(new AccelerateInterpolator());
        alphaAnimation.setAnimationListener(new k(this));
        this.a.a.startAnimation(alphaAnimation);
    }
}
