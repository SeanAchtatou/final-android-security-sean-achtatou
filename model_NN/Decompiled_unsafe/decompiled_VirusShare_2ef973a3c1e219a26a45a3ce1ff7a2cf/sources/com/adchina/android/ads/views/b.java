package com.adchina.android.ads.views;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.webkit.WebView;
import android.webkit.WebViewClient;

final class b extends WebViewClient {
    final /* synthetic */ AdBrowserView a;

    b(AdBrowserView adBrowserView) {
        this.a = adBrowserView;
    }

    public final void onPageFinished(WebView webView, String str) {
        super.onPageFinished(webView, str);
    }

    public final void onPageStarted(WebView webView, String str, Bitmap bitmap) {
        super.onPageStarted(webView, str, bitmap);
    }

    public final boolean shouldOverrideUrlLoading(WebView webView, String str) {
        try {
            if (str.toLowerCase().startsWith("https://") || str.toLowerCase().startsWith("http://")) {
                webView.loadUrl(str);
                return true;
            }
            this.a.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(str)));
            return true;
        } catch (Exception e) {
            return true;
        }
    }
}
