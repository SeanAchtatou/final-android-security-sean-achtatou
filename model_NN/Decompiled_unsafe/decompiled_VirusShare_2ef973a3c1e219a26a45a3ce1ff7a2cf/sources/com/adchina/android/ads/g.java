package com.adchina.android.ads;

public enum g {
    EReceiveAd,
    EGetShrinkFSImgMaterial,
    EGetImgMaterial,
    ESendImpTrack,
    ESendThdImpTrack,
    ERefreshAd,
    ESendShrinkFSImpTrack,
    ESendShrinkFSThdImpTrack,
    ESendVideoStartedImpTrack,
    ESendVideoStartedThdImpTrack,
    ESendVideoEndedImpTrack,
    ESendVideoEndedThdImpTrack,
    ESendFullScreenStartedTrack,
    ESendFullScreenImpTrack,
    ESendFullScreenThdImpTrack,
    ESendFullScreenEndedTrack,
    EIdle
}
