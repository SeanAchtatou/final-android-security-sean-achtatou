package com.adchina.android.ads;

import java.io.File;
import java.io.FileOutputStream;
import java.sql.Date;
import java.text.SimpleDateFormat;

public final class h {
    private File a;
    private File b;
    private FileOutputStream c;
    private FileOutputStream d;
    private boolean e;
    private boolean f;
    private boolean g;
    private boolean h;

    public h() {
        this.a = null;
        this.b = null;
        this.c = null;
        this.d = null;
        this.e = false;
        this.f = false;
        this.g = false;
        this.h = false;
        this.g = AdManager.getLogMode();
        this.h = this.g && AdManager.getDebugMode();
    }

    public final void a() {
        if (this.g) {
            try {
                this.c.close();
            } catch (Exception e2) {
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException}
     arg types: [java.io.File, int]
     candidates:
      ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException}
      ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException} */
    public final void a(String str) {
        if (this.g && !this.e) {
            this.e = true;
            this.a = new File(Utils.concatString(Utils.getSDPath(), "/", str));
            if (this.a.exists()) {
                this.a.delete();
            }
            try {
                this.a.createNewFile();
                this.c = new FileOutputStream(this.a, true);
            } catch (Exception e2) {
            }
        }
    }

    public final void b() {
        if (this.h) {
            try {
                this.d.close();
            } catch (Exception e2) {
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException}
     arg types: [java.io.File, int]
     candidates:
      ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException}
      ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException} */
    public final void b(String str) {
        if (this.h && !this.f) {
            this.f = true;
            this.b = new File(Utils.concatString(Utils.getSDPath(), "/", str));
            if (this.b.exists()) {
                this.b.delete();
            }
            try {
                this.b.createNewFile();
                this.d = new FileOutputStream(this.b, true);
            } catch (Exception e2) {
            }
        }
    }

    public final void c(String str) {
        if (this.g) {
            try {
                this.c.write(Utils.concatString(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss --- ").format(new Date(System.currentTimeMillis())), str, "\r\n").getBytes());
                this.c.flush();
            } catch (Exception e2) {
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void d(String str) {
        if (this.h) {
            try {
                this.d.write(Utils.concatString(str, "\r\n").getBytes());
                this.d.flush();
            } catch (Exception e2) {
            }
        }
    }
}
