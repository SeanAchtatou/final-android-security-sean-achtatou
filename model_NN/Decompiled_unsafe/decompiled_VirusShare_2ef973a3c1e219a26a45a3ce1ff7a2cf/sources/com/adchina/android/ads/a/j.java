package com.adchina.android.ads.a;

import com.adchina.android.ads.Utils;
import com.adchina.android.ads.c;
import com.adchina.android.ads.o;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;

final class j implements c {
    final /* synthetic */ e a;
    private final /* synthetic */ String b;

    j(e eVar, String str) {
        this.a = eVar;
        this.b = str;
    }

    public final Object a() {
        ByteArrayOutputStream byteArrayOutputStream;
        FileInputStream fileInputStream;
        FileInputStream fileInputStream2;
        ByteArrayOutputStream byteArrayOutputStream2;
        try {
            FileInputStream fileInputStream3 = new FileInputStream(this.b);
            try {
                byteArrayOutputStream2 = new ByteArrayOutputStream();
                while (true) {
                    try {
                        int read = fileInputStream3.read();
                        if (read == -1) {
                            break;
                        }
                        byteArrayOutputStream2.write(read);
                    } catch (Exception e) {
                        ByteArrayOutputStream byteArrayOutputStream3 = byteArrayOutputStream2;
                        fileInputStream = fileInputStream3;
                        byteArrayOutputStream = byteArrayOutputStream3;
                    } catch (Throwable th) {
                        Throwable th2 = th;
                        fileInputStream2 = fileInputStream3;
                        th = th2;
                        Utils.closeStream(fileInputStream2);
                        Utils.closeStream(byteArrayOutputStream2);
                        throw th;
                    }
                }
                byte[] byteArray = byteArrayOutputStream2.toByteArray();
                o a2 = (byteArray == null || byteArray.length <= 0) ? null : o.a(byteArray);
                Utils.closeStream(fileInputStream3);
                Utils.closeStream(byteArrayOutputStream2);
                return a2;
            } catch (Exception e2) {
                fileInputStream = fileInputStream3;
                byteArrayOutputStream = null;
                try {
                    this.a.a(18, "Gif AdMaterial is null");
                    Utils.closeStream(fileInputStream);
                    Utils.closeStream(byteArrayOutputStream);
                    return null;
                } catch (Throwable th3) {
                    Throwable th4 = th3;
                    fileInputStream2 = fileInputStream;
                    byteArrayOutputStream2 = byteArrayOutputStream;
                    th = th4;
                    Utils.closeStream(fileInputStream2);
                    Utils.closeStream(byteArrayOutputStream2);
                    throw th;
                }
            } catch (Throwable th5) {
                fileInputStream2 = fileInputStream3;
                th = th5;
                byteArrayOutputStream2 = null;
                Utils.closeStream(fileInputStream2);
                Utils.closeStream(byteArrayOutputStream2);
                throw th;
            }
        } catch (Exception e3) {
            byteArrayOutputStream = null;
            fileInputStream = null;
            this.a.a(18, "Gif AdMaterial is null");
            Utils.closeStream(fileInputStream);
            Utils.closeStream(byteArrayOutputStream);
            return null;
        } catch (Throwable th6) {
            th = th6;
            byteArrayOutputStream2 = null;
            fileInputStream2 = null;
            Utils.closeStream(fileInputStream2);
            Utils.closeStream(byteArrayOutputStream2);
            throw th;
        }
    }
}
