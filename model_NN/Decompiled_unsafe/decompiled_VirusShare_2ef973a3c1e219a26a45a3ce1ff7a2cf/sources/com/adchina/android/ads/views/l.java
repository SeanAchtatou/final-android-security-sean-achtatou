package com.adchina.android.ads.views;

import android.os.Handler;
import android.os.Message;

final class l implements Handler.Callback {
    final /* synthetic */ FullScreenAdActivity a;

    l(FullScreenAdActivity fullScreenAdActivity) {
        this.a = fullScreenAdActivity;
    }

    public final boolean handleMessage(Message message) {
        if (this.a.mNumber <= 0) {
            this.a.endFullScreen();
            return false;
        } else if (this.a.mTxtTimer == null) {
            return false;
        } else {
            this.a.mTxtTimer.setText("广告剩余");
            this.a.mTxtTimer.append(new StringBuilder(String.valueOf(this.a.mNumber)).toString());
            this.a.mTxtTimer.append("秒");
            return false;
        }
    }
}
