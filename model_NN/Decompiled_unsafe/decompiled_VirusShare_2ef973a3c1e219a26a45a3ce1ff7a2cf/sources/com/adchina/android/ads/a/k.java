package com.adchina.android.ads.a;

import android.widget.ImageView;
import com.adchina.android.ads.d;
import com.adchina.android.ads.o;
import com.adchina.android.ads.views.GifImageView;
import com.adchina.android.ads.views.m;

final class k implements d {
    final /* synthetic */ e a;
    private final /* synthetic */ GifImageView b;
    private final /* synthetic */ m c;

    k(e eVar, GifImageView gifImageView, m mVar) {
        this.a = eVar;
        this.b = gifImageView;
        this.c = mVar;
    }

    public final void a(Object obj) {
        if (obj != null) {
            this.b.a();
            this.b.setScaleType(ImageView.ScaleType.FIT_CENTER);
            this.b.a((o) obj);
            this.c.a();
            this.a.a(17, "Displayd FullScreen Ad");
            return;
        }
        this.a.a(18, "Gif AdMaterial is null");
    }
}
