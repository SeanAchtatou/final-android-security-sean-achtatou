package com.adchina.android.ads.views.animations;

import android.view.animation.AccelerateInterpolator;
import android.view.animation.ScaleAnimation;

final class e implements Runnable {
    /* access modifiers changed from: package-private */
    public final /* synthetic */ c a;

    e(c cVar) {
        this.a = cVar;
    }

    public final void run() {
        ScaleAnimation scaleAnimation = new ScaleAnimation(1.4f, 0.8f, 1.4f, 0.8f, 1, 0.5f, 1, 0.5f);
        scaleAnimation.setDuration(500);
        scaleAnimation.setFillAfter(true);
        scaleAnimation.setInterpolator(new AccelerateInterpolator());
        scaleAnimation.setAnimationListener(new f(this));
        this.a.a.startAnimation(scaleAnimation);
    }
}
