package com.adchina.android.ads.views;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import com.adchina.android.ads.a.a;
import com.adchina.android.ads.o;
import com.adchina.android.ads.views.animations.AnimationManager;
import java.util.Timer;

public final class ContentView extends LinearLayout {
    private StringBuffer a = new StringBuffer();
    private Matrix b = new Matrix();
    private g c = g.ENONE;
    private int d = -1;
    private int e = -16777216;
    private Bitmap f = null;
    private Paint g = null;
    private Typeface h = null;
    private o i = null;
    private Timer j = null;
    private GifImageView k = null;
    /* access modifiers changed from: private */
    public AdView l = null;
    /* access modifiers changed from: private */
    public a m = null;

    public ContentView(Context context) {
        super(context);
        a(context);
        setFocusable(true);
        setFocusableInTouchMode(true);
        setClickable(true);
    }

    public ContentView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a(context);
    }

    private void a(Context context) {
        setLayoutParams(new RelativeLayout.LayoutParams(-1, -2));
        this.h = Typeface.create("宋体", 1);
        this.k = new GifImageView(context);
        this.k.setScaleType(ImageView.ScaleType.FIT_CENTER);
        addView(this.k, new LinearLayout.LayoutParams(-1, -1, 0.0f));
        this.k.setOnTouchListener(new f(this));
    }

    private void c(Bitmap bitmap) {
        if (bitmap != null) {
            AnimationManager.a(this);
        }
    }

    public final void a(int i2) {
        this.d = i2;
    }

    /* access modifiers changed from: protected */
    public final void a(Bitmap bitmap) {
        this.f = bitmap;
        this.c = g.EIMG;
        this.k.setImageBitmap(this.f);
    }

    public final void a(Typeface typeface) {
        this.h = typeface;
    }

    public final void a(a aVar) {
        this.m = aVar;
    }

    /* access modifiers changed from: protected */
    public final void a(o oVar) {
        this.k.a(oVar);
        this.c = g.EGIF;
        c(this.k.b());
    }

    public final void a(AdView adView) {
        this.l = adView;
    }

    /* access modifiers changed from: protected */
    public final void a(String str) {
        this.a.setLength(0);
        this.a.append(str);
        this.c = g.ETXT;
        if (this.g == null) {
            this.g = new Paint();
        }
        Rect rect = new Rect();
        getHitRect(rect);
        Bitmap createBitmap = Bitmap.createBitmap(rect.width(), rect.height(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas();
        canvas.setBitmap(createBitmap);
        this.g.setColor(this.e);
        this.g.setStyle(Paint.Style.FILL);
        canvas.drawRect(rect, this.g);
        this.g.setColor(this.d);
        this.g.setTypeface(this.h);
        canvas.drawText(this.a.toString(), (((float) rect.width()) - this.g.measureText(str)) / 2.0f, (float) (rect.height() / 2), this.g);
        b(createBitmap);
    }

    /* access modifiers changed from: protected */
    public final void b(Bitmap bitmap) {
        this.f = bitmap;
        this.c = g.EIMG;
        this.k.a(this.f);
        c(this.f);
    }

    public final void setBackgroundColor(int i2) {
        this.e = i2;
        this.k.setBackgroundColor(i2);
    }
}
