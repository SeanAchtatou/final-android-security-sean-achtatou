package com.adchina.android.ads.views.animations;

import android.view.animation.AccelerateInterpolator;
import android.view.animation.ScaleAnimation;
import com.adchina.android.ads.views.ContentView;

public final class c {
    /* access modifiers changed from: private */
    public ContentView a;

    public c(ContentView contentView) {
        this.a = contentView;
    }

    public final void a() {
        ScaleAnimation scaleAnimation = new ScaleAnimation(0.0f, 1.4f, 0.0f, 1.4f, 1, 0.5f, 1, 0.5f);
        scaleAnimation.setDuration(700);
        scaleAnimation.setFillAfter(true);
        scaleAnimation.setInterpolator(new AccelerateInterpolator());
        scaleAnimation.setAnimationListener(new d(this));
        this.a.startAnimation(scaleAnimation);
    }
}
