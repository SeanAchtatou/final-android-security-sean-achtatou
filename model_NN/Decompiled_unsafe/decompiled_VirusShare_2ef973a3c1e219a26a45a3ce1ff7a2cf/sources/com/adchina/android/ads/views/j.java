package com.adchina.android.ads.views;

import android.view.View;

final class j implements View.OnClickListener {
    final /* synthetic */ FullScreenAdActivity a;

    j(FullScreenAdActivity fullScreenAdActivity) {
        this.a = fullScreenAdActivity;
    }

    public final void onClick(View view) {
        this.a.closeFullScreen();
    }
}
