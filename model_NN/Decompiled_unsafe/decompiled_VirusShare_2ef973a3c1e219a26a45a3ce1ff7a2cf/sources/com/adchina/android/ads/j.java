package com.adchina.android.ads;

import android.os.Handler;
import android.os.Message;

public final class j extends Thread {
    c a;
    Handler b;

    public j(c cVar, Handler handler) {
        this.a = cVar;
        this.b = handler;
    }

    public final void run() {
        Object obj = null;
        super.run();
        try {
            obj = this.a.a();
        } catch (Exception e) {
        } finally {
            Message message = new Message();
            message.what = 1;
            message.obj = obj;
            this.b.sendMessage(message);
        }
    }
}
