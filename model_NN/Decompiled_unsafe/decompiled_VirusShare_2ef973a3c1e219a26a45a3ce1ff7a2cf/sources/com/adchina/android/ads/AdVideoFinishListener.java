package com.adchina.android.ads;

import android.content.Context;

public interface AdVideoFinishListener {
    void finished(Context context, Object obj);
}
