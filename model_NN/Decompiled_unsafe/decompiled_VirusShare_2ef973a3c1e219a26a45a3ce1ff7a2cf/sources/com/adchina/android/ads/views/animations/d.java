package com.adchina.android.ads.views.animations;

import android.view.animation.Animation;

final class d implements Animation.AnimationListener {
    final /* synthetic */ c a;

    d(c cVar) {
        this.a = cVar;
    }

    public final void onAnimationEnd(Animation animation) {
        this.a.a.post(new e(this.a));
    }

    public final void onAnimationRepeat(Animation animation) {
    }

    public final void onAnimationStart(Animation animation) {
    }
}
