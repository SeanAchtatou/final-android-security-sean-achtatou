package com.adchina.android.ads.views.animations;

import android.graphics.Camera;
import android.graphics.Matrix;
import android.view.animation.Animation;
import android.view.animation.Transformation;

public final class m extends Animation {
    private static /* synthetic */ int[] i;
    private n a;
    private int[] b;
    private Camera c;
    private int d = 0;
    private float e = 0.0f;
    private int f = 0;
    private float g = 0.0f;
    private int[] h;

    public m(n nVar, int[] iArr) {
        this.a = nVar;
        this.b = iArr;
        if (iArr == null && iArr.length == 0) {
            throw new Exception("aPoints不能为空");
        }
        this.f = 0;
        this.g = 0.0f;
        this.h = new int[iArr.length];
        for (int i2 = 0; i2 < iArr.length - 1; i2++) {
            this.f += Math.abs(iArr[i2] - iArr[i2 + 1]);
            this.h[i2 + 1] = this.f;
        }
        this.e = (float) iArr[0];
    }

    private static /* synthetic */ int[] a() {
        int[] iArr = i;
        if (iArr == null) {
            iArr = new int[n.values().length];
            try {
                iArr[n.EHorizontal.ordinal()] = 1;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[n.EVertical.ordinal()] = 2;
            } catch (NoSuchFieldError e3) {
            }
            i = iArr;
        }
        return iArr;
    }

    /* access modifiers changed from: protected */
    public final void applyTransformation(float f2, Transformation transformation) {
        super.applyTransformation(f2, transformation);
        Camera camera = this.c;
        this.g = ((float) this.f) * f2;
        if (this.d <= this.b.length - 1) {
            if (this.g >= ((float) this.h[this.d])) {
                this.d++;
                if (this.d > this.b.length - 1) {
                    return;
                }
            }
            int i2 = this.b[this.d - 1] > this.b[this.d] ? -1 : 1;
            Matrix matrix = transformation.getMatrix();
            camera.save();
            this.e = (((float) i2) * ((((float) this.f) * f2) - ((float) this.h[this.d - 1]))) + ((float) this.b[this.d - 1]);
            switch (a()[this.a.ordinal()]) {
                case 1:
                    camera.translate(this.e, 0.0f, 0.0f);
                    break;
                case 2:
                    camera.translate(0.0f, this.e * -1.0f, 0.0f);
                    break;
            }
            camera.getMatrix(matrix);
            camera.restore();
        }
    }

    public final void initialize(int i2, int i3, int i4, int i5) {
        super.initialize(i2, i3, i4, i5);
        this.c = new Camera();
    }
}
