package com.adchina.android.ads.views;

import android.view.MotionEvent;
import android.view.View;

final class f implements View.OnTouchListener {
    final /* synthetic */ ContentView a;

    f(ContentView contentView) {
        this.a = contentView;
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        if (this.a.m == null || motionEvent.getAction() != 0) {
            return false;
        }
        this.a.m.a(this.a.l, motionEvent);
        return true;
    }
}
