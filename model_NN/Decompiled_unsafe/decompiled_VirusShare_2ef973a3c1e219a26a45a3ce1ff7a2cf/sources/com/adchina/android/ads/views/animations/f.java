package com.adchina.android.ads.views.animations;

import android.view.animation.Animation;

final class f implements Animation.AnimationListener {
    final /* synthetic */ e a;

    f(e eVar) {
        this.a = eVar;
    }

    public final void onAnimationEnd(Animation animation) {
        this.a.a.a.post(new g(this.a.a));
    }

    public final void onAnimationRepeat(Animation animation) {
    }

    public final void onAnimationStart(Animation animation) {
    }
}
