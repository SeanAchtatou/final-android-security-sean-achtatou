package com.adchina.android.ads.views.animations;

import android.view.animation.AccelerateInterpolator;
import com.adchina.android.ads.AdManager;
import com.adchina.android.ads.views.ContentView;
import java.util.ArrayList;

public class AnimationManager {
    public static final int ANY_ANIMATION = 0;
    public static final int ENLARGEMENT_ANIMATION = 16;
    public static final int FADEIN_ANIMATION = 8;
    public static final int FALLDOWN_ANIMATION = 4;
    public static final int ROTATE_3D_ANIMATION = 1;
    public static final int RUNIN_ANIMATION = 2;
    private static ArrayList mAnimations;

    public static void a(ContentView contentView) {
        if (mAnimations == null) {
            mAnimations = new ArrayList();
            int animations = AdManager.getAnimations();
            if (animations <= 0) {
                animations = 31;
            }
            if ((animations & 1) > 0) {
                mAnimations.add(1);
            }
            if ((animations & 2) > 0) {
                mAnimations.add(2);
            }
            if ((animations & 4) > 0) {
                mAnimations.add(4);
            }
            if ((animations & 8) > 0) {
                mAnimations.add(8);
            }
            if ((animations & 16) > 0) {
                mAnimations.add(16);
            }
        }
        if (mAnimations.size() != 0) {
            switch (((Integer) mAnimations.get(((int) (Math.random() * 10000.0d)) % mAnimations.size())).intValue()) {
                case 1:
                    float width = contentView.getWidth() != 0 ? ((float) contentView.getWidth()) / 2.0f : ((float) contentView.getContext().getResources().getDisplayMetrics().widthPixels) / 2.0f;
                    float height = ((float) contentView.getHeight()) / 2.0f;
                    o oVar = new o(0.0f, 90.0f, width, height, true);
                    oVar.setDuration(750);
                    oVar.setFillAfter(true);
                    oVar.setInterpolator(new AccelerateInterpolator());
                    oVar.setAnimationListener(new a(contentView, width, height));
                    contentView.startAnimation(oVar);
                    return;
                case 2:
                    try {
                        n nVar = n.EHorizontal;
                        int[] iArr = new int[6];
                        iArr[0] = contentView.getWidth() != 0 ? contentView.getWidth() : contentView.getContext().getResources().getDisplayMetrics().widthPixels;
                        iArr[1] = -60;
                        iArr[2] = 50;
                        iArr[3] = -40;
                        iArr[4] = 30;
                        m mVar = new m(nVar, iArr);
                        mVar.setDuration(1500);
                        mVar.setFillAfter(true);
                        mVar.setInterpolator(new AccelerateInterpolator());
                        contentView.startAnimation(mVar);
                        return;
                    } catch (Exception e) {
                        e.printStackTrace();
                        return;
                    }
                case 4:
                    try {
                        n nVar2 = n.EVertical;
                        int[] iArr2 = new int[6];
                        iArr2[0] = -(contentView.getHeight() != 0 ? contentView.getHeight() : (int) ((((float) contentView.getContext().getResources().getDisplayMetrics().widthPixels) * 3.0f) / 20.0f));
                        iArr2[1] = 40;
                        iArr2[2] = -30;
                        iArr2[3] = 20;
                        iArr2[4] = -10;
                        m mVar2 = new m(nVar2, iArr2);
                        mVar2.setDuration(1500);
                        mVar2.setFillAfter(true);
                        mVar2.setInterpolator(new AccelerateInterpolator());
                        contentView.startAnimation(mVar2);
                        return;
                    } catch (Exception e2) {
                        e2.printStackTrace();
                        return;
                    }
                case FADEIN_ANIMATION /*8*/:
                    new h(contentView).a();
                    return;
                case ENLARGEMENT_ANIMATION /*16*/:
                    new c(contentView).a();
                    return;
                default:
                    return;
            }
        }
    }
}
