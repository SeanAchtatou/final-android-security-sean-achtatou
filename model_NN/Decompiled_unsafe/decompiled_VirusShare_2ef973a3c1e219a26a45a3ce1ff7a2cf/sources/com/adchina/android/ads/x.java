package com.adchina.android.ads;

import java.io.InputStream;

final class x implements c {
    final /* synthetic */ w a;
    private final /* synthetic */ t b;
    private final /* synthetic */ n c;

    x(w wVar, t tVar, n nVar) {
        this.a = wVar;
        this.b = tVar;
        this.c = nVar;
    }

    public final Object a() {
        InputStream inputStream;
        InputStream inputStream2;
        long currentTimeMillis = System.currentTimeMillis();
        try {
            t tVar = this.b;
            InputStream b2 = t.b(this.c.b());
            try {
                Utils.convertStreamToString(b2);
                t tVar2 = this.b;
                t.a(b2);
                return Long.valueOf(System.currentTimeMillis() - currentTimeMillis);
            } catch (Exception e) {
                inputStream2 = b2;
                t tVar3 = this.b;
                t.a(inputStream2);
                return null;
            } catch (Throwable th) {
                th = th;
                inputStream = b2;
                t tVar4 = this.b;
                t.a(inputStream);
                throw th;
            }
        } catch (Exception e2) {
            inputStream2 = null;
        } catch (Throwable th2) {
            th = th2;
            inputStream = null;
            t tVar42 = this.b;
            t.a(inputStream);
            throw th;
        }
    }
}
