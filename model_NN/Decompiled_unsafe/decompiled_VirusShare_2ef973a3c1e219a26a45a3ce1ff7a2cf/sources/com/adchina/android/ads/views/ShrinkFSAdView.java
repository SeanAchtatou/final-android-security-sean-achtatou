package com.adchina.android.ads.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.adchina.android.ads.a.o;

public class ShrinkFSAdView extends ImageView implements View.OnClickListener {
    private o a;

    public ShrinkFSAdView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a();
    }

    public ShrinkFSAdView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        a();
    }

    private void a() {
        setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        setScaleType(ImageView.ScaleType.FIT_XY);
        setOnClickListener(this);
        setFocusable(true);
    }

    public final void a(o oVar) {
        this.a = oVar;
    }

    public void onClick(View view) {
        if (this.a != null) {
            this.a.a();
        }
    }
}
