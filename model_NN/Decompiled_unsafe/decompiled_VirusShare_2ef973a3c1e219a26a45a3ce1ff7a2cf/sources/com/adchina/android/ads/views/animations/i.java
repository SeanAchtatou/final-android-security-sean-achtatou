package com.adchina.android.ads.views.animations;

import android.view.animation.Animation;

final class i implements Animation.AnimationListener {
    final /* synthetic */ h a;

    i(h hVar) {
        this.a = hVar;
    }

    public final void onAnimationEnd(Animation animation) {
        this.a.a.post(new j(this.a));
    }

    public final void onAnimationRepeat(Animation animation) {
    }

    public final void onAnimationStart(Animation animation) {
    }
}
