package com.adchina.android.ads.a;

final class t extends Thread {
    final /* synthetic */ q a;
    private String b;

    public t(q qVar, String str) {
        this.a = qVar;
        this.b = str;
    }

    /* JADX WARNING: Removed duplicated region for block: B:35:0x00b3 A[Catch:{ all -> 0x00f6 }] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:36:0x00b6=Splitter:B:36:0x00b6, B:42:0x00d9=Splitter:B:42:0x00d9} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void run() {
        /*
            r8 = this;
            r6 = 0
            java.lang.String r0 = r8.b
            if (r0 == 0) goto L_0x000d
            java.lang.String r0 = r8.b
            int r0 = r0.length()
            if (r0 != 0) goto L_0x000e
        L_0x000d:
            return
        L_0x000e:
            com.adchina.android.ads.a.q r0 = r8.a     // Catch:{ Exception -> 0x00f2, all -> 0x00ed }
            com.adchina.android.ads.h r0 = r0.Z     // Catch:{ Exception -> 0x00f2, all -> 0x00ed }
            r1 = 2
            java.lang.Object[] r1 = new java.lang.Object[r1]     // Catch:{ Exception -> 0x00f2, all -> 0x00ed }
            r2 = 0
            java.lang.String r3 = "++ start to download video file"
            r1[r2] = r3     // Catch:{ Exception -> 0x00f2, all -> 0x00ed }
            r2 = 1
            java.lang.String r3 = r8.b     // Catch:{ Exception -> 0x00f2, all -> 0x00ed }
            r1[r2] = r3     // Catch:{ Exception -> 0x00f2, all -> 0x00ed }
            java.lang.String r1 = com.adchina.android.ads.Utils.concatString(r1)     // Catch:{ Exception -> 0x00f2, all -> 0x00ed }
            r0.c(r1)     // Catch:{ Exception -> 0x00f2, all -> 0x00ed }
            com.adchina.android.ads.a.q r0 = r8.a     // Catch:{ Exception -> 0x00f2, all -> 0x00ed }
            com.adchina.android.ads.t r0 = r0.W     // Catch:{ Exception -> 0x00f2, all -> 0x00ed }
            java.lang.String r0 = r8.b     // Catch:{ Exception -> 0x00f2, all -> 0x00ed }
            java.io.InputStream r0 = com.adchina.android.ads.t.b(r0)     // Catch:{ Exception -> 0x00f2, all -> 0x00ed }
            if (r0 != 0) goto L_0x0065
            java.lang.Exception r1 = new java.lang.Exception     // Catch:{ Exception -> 0x003a, all -> 0x00dd }
            java.lang.String r2 = "video address is not exists"
            r1.<init>(r2)     // Catch:{ Exception -> 0x003a, all -> 0x00dd }
            throw r1     // Catch:{ Exception -> 0x003a, all -> 0x00dd }
        L_0x003a:
            r1 = move-exception
            r7 = r1
            r1 = r0
            r0 = r7
        L_0x003e:
            r2 = 2
            java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch:{ all -> 0x00f0 }
            r3 = 0
            java.lang.String r4 = "Failed to download video file, err = "
            r2[r3] = r4     // Catch:{ all -> 0x00f0 }
            r3 = 1
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x00f0 }
            r2[r3] = r0     // Catch:{ all -> 0x00f0 }
            java.lang.String r0 = com.adchina.android.ads.Utils.concatString(r2)     // Catch:{ all -> 0x00f0 }
            com.adchina.android.ads.a.q r2 = r8.a     // Catch:{ all -> 0x00f0 }
            com.adchina.android.ads.h r2 = r2.Z     // Catch:{ all -> 0x00f0 }
            r2.c(r0)     // Catch:{ all -> 0x00f0 }
            java.lang.String r2 = "AdChinaError"
            android.util.Log.e(r2, r0)     // Catch:{ all -> 0x00f0 }
            com.adchina.android.ads.a.q r0 = r8.a
            com.adchina.android.ads.t r0 = r0.W
            com.adchina.android.ads.t.a(r1)
            goto L_0x000d
        L_0x0065:
            java.io.File r1 = new java.io.File     // Catch:{ Exception -> 0x003a, all -> 0x00dd }
            java.lang.String r2 = com.adchina.android.ads.k.a     // Catch:{ Exception -> 0x003a, all -> 0x00dd }
            r1.<init>(r2)     // Catch:{ Exception -> 0x003a, all -> 0x00dd }
            boolean r2 = r1.exists()     // Catch:{ Exception -> 0x003a, all -> 0x00dd }
            if (r2 != 0) goto L_0x0075
            r1.mkdirs()     // Catch:{ Exception -> 0x003a, all -> 0x00dd }
        L_0x0075:
            java.io.File r2 = new java.io.File     // Catch:{ Exception -> 0x003a, all -> 0x00dd }
            r3 = 2
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ Exception -> 0x003a, all -> 0x00dd }
            r4 = 0
            java.lang.String r5 = "yyyyMMddHHmmss"
            java.lang.String r5 = com.adchina.android.ads.Utils.getNowTime(r5)     // Catch:{ Exception -> 0x003a, all -> 0x00dd }
            r3[r4] = r5     // Catch:{ Exception -> 0x003a, all -> 0x00dd }
            r4 = 1
            java.lang.String r5 = "video.tmp"
            r3[r4] = r5     // Catch:{ Exception -> 0x003a, all -> 0x00dd }
            java.lang.String r3 = com.adchina.android.ads.Utils.concatString(r3)     // Catch:{ Exception -> 0x003a, all -> 0x00dd }
            r2.<init>(r1, r3)     // Catch:{ Exception -> 0x003a, all -> 0x00dd }
            boolean r1 = r2.exists()     // Catch:{ Exception -> 0x003a, all -> 0x00dd }
            if (r1 == 0) goto L_0x0098
            r2.delete()     // Catch:{ Exception -> 0x003a, all -> 0x00dd }
        L_0x0098:
            java.io.FileOutputStream r1 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x00fb, all -> 0x00d7 }
            r1.<init>(r2)     // Catch:{ Exception -> 0x00fb, all -> 0x00d7 }
            r3 = 16384(0x4000, float:2.2959E-41)
            byte[] r3 = new byte[r3]     // Catch:{ Exception -> 0x00ac }
        L_0x00a1:
            int r4 = r0.read(r3)     // Catch:{ Exception -> 0x00ac }
            if (r4 <= 0) goto L_0x00e9
            r5 = 0
            r1.write(r3, r5, r4)     // Catch:{ Exception -> 0x00ac }
            goto L_0x00a1
        L_0x00ac:
            r3 = move-exception
        L_0x00ad:
            boolean r3 = r2.exists()     // Catch:{ all -> 0x00f6 }
            if (r3 == 0) goto L_0x00b6
            r2.delete()     // Catch:{ all -> 0x00f6 }
        L_0x00b6:
            com.adchina.android.ads.Utils.closeStream(r1)     // Catch:{ Exception -> 0x003a, all -> 0x00dd }
        L_0x00b9:
            com.adchina.android.ads.a.q r1 = r8.a     // Catch:{ Exception -> 0x003a, all -> 0x00dd }
            r3 = 6
            r4 = 2
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ Exception -> 0x003a, all -> 0x00dd }
            r5 = 0
            java.lang.String r6 = r8.b     // Catch:{ Exception -> 0x003a, all -> 0x00dd }
            r4[r5] = r6     // Catch:{ Exception -> 0x003a, all -> 0x00dd }
            r5 = 1
            java.lang.String r2 = r2.getAbsolutePath()     // Catch:{ Exception -> 0x003a, all -> 0x00dd }
            r4[r5] = r2     // Catch:{ Exception -> 0x003a, all -> 0x00dd }
            r1.a(r3, r4)     // Catch:{ Exception -> 0x003a, all -> 0x00dd }
            com.adchina.android.ads.a.q r1 = r8.a
            com.adchina.android.ads.t r1 = r1.W
            com.adchina.android.ads.t.a(r0)
            goto L_0x000d
        L_0x00d7:
            r1 = move-exception
            r2 = r6
        L_0x00d9:
            com.adchina.android.ads.Utils.closeStream(r2)     // Catch:{ Exception -> 0x003a, all -> 0x00dd }
            throw r1     // Catch:{ Exception -> 0x003a, all -> 0x00dd }
        L_0x00dd:
            r1 = move-exception
            r7 = r1
            r1 = r0
            r0 = r7
        L_0x00e1:
            com.adchina.android.ads.a.q r2 = r8.a
            com.adchina.android.ads.t r2 = r2.W
            com.adchina.android.ads.t.a(r1)
            throw r0
        L_0x00e9:
            com.adchina.android.ads.Utils.closeStream(r1)     // Catch:{ Exception -> 0x003a, all -> 0x00dd }
            goto L_0x00b9
        L_0x00ed:
            r0 = move-exception
            r1 = r6
            goto L_0x00e1
        L_0x00f0:
            r0 = move-exception
            goto L_0x00e1
        L_0x00f2:
            r0 = move-exception
            r1 = r6
            goto L_0x003e
        L_0x00f6:
            r2 = move-exception
            r7 = r2
            r2 = r1
            r1 = r7
            goto L_0x00d9
        L_0x00fb:
            r1 = move-exception
            r1 = r6
            goto L_0x00ad
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adchina.android.ads.a.t.run():void");
    }
}
