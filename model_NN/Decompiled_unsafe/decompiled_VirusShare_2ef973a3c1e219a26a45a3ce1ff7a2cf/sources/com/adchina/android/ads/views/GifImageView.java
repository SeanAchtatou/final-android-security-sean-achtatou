package com.adchina.android.ads.views;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.widget.ImageView;
import com.adchina.android.ads.Utils;
import com.adchina.android.ads.e;
import com.adchina.android.ads.o;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Timer;

public class GifImageView extends ImageView implements Handler.Callback {
    private o a;
    private Timer b;
    /* access modifiers changed from: private */
    public Handler c;
    private float d = 12.0f;
    private boolean e = false;
    private Bitmap f;

    public GifImageView(Context context) {
        super(context);
        c();
    }

    public GifImageView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        c();
    }

    public GifImageView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        c();
    }

    private void b(InputStream inputStream) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        while (true) {
            try {
                int read = inputStream.read();
                if (read == -1) {
                    this.a = o.a(byteArrayOutputStream.toByteArray());
                    return;
                }
                byteArrayOutputStream.write(read);
            } catch (IOException e2) {
                e2.printStackTrace();
                return;
            } finally {
                Utils.closeStream(byteArrayOutputStream);
                Utils.closeStream(inputStream);
            }
        }
    }

    private void c() {
        this.c = new Handler(this);
    }

    private void d() {
        int i = this.d != 0.0f ? (int) (1000.0d / ((double) this.d)) : 100;
        this.b = new Timer();
        this.b.schedule(new n(this), 0, (long) i);
    }

    private void e() {
        if (this.a != null && this.a.e() != null) {
            int a2 = this.a.e().a();
            if (a2 <= 100) {
                a2 = 100;
            }
            float f2 = 1000.0f / ((float) a2);
            if (f2 < this.d) {
                this.d = f2;
            }
        }
    }

    private void f() {
        try {
            Rect rect = new Rect();
            getHitRect(rect);
            int width = rect.width();
            int height = rect.height();
            if (this.a != null) {
                int b2 = this.a.b();
                width = b2;
                height = this.a.c();
            }
            if (width > 0 && height > 0) {
                if (this.f != null) {
                    this.f.recycle();
                }
                this.f = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
                setImageBitmap(this.f);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public final void a() {
        if (this.b != null) {
            this.c.removeMessages(1);
            this.b.cancel();
            this.d = 12.0f;
        }
        if (this.a != null) {
            this.a.a();
            this.a = null;
        }
    }

    public final void a(int i) {
        a();
        f();
        b(getResources().openRawResource(i));
        e();
        f();
        d();
    }

    public final void a(Bitmap bitmap) {
        a();
        if (bitmap != null) {
            if (this.f != null && !this.f.isRecycled()) {
                this.f.recycle();
            }
            this.f = bitmap;
            setImageBitmap(bitmap);
        }
    }

    public final void a(o oVar) {
        a();
        f();
        this.a = oVar;
        e();
        f();
        d();
    }

    public final void a(InputStream inputStream) {
        a();
        f();
        b(inputStream);
        e();
        f();
        d();
    }

    public final Bitmap b() {
        return this.f;
    }

    public boolean handleMessage(Message message) {
        int width;
        int round;
        if (this.a != null) {
            try {
                e e2 = this.a.e();
                this.a.f();
                if (!(e2 == null || e2.b() == null || e2.b().isRecycled())) {
                    if (this.f == null) {
                        f();
                    }
                    if (this.f != null) {
                        if (this.f != null && this.a != null && this.f.getWidth() > 0 && this.f.getHeight() > 0 && this.a.b() > 0 && this.a.c() > 0) {
                            Canvas canvas = new Canvas();
                            if (!this.f.isMutable()) {
                                Bitmap bitmap = this.f;
                                this.f = this.f.copy(Bitmap.Config.ARGB_8888, true);
                                bitmap.recycle();
                            }
                            canvas.setBitmap(this.f);
                            float width2 = (((float) this.f.getWidth()) * 1.0f) / ((float) this.f.getHeight());
                            float b2 = (((float) this.a.b()) * 1.0f) / ((float) this.a.c());
                            if (width2 > b2) {
                                int height = this.f.getHeight();
                                int round2 = Math.round(b2 * ((float) height));
                                round = height;
                                width = round2;
                            } else {
                                width = this.f.getWidth();
                                round = Math.round(((float) width) / b2);
                            }
                            int c2 = ((e2.c() * width) / this.a.b()) + ((this.f.getWidth() - width) / 2);
                            int d2 = ((e2.d() * round) / this.a.c()) + ((this.f.getHeight() - round) / 2);
                            canvas.drawBitmap(e2.b(), new Rect(0, 0, e2.b().getWidth(), e2.b().getHeight()), new Rect(c2, d2, ((width * e2.b().getWidth()) / this.a.b()) + c2, ((round * e2.b().getHeight()) / this.a.c()) + d2), (Paint) null);
                            canvas.save();
                        }
                        setImageBitmap(this.f);
                    }
                    if (!this.e) {
                        int a2 = e2.a();
                        if (a2 <= 100) {
                            a2 = 100;
                        }
                        float f2 = 1000.0f / ((float) a2);
                        if (f2 < this.d) {
                            this.d = f2;
                            this.b.cancel();
                            d();
                        }
                    }
                }
            } catch (Exception e3) {
                e3.printStackTrace();
            }
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        a();
        if (this.f != null && !this.f.isRecycled()) {
            this.f.recycle();
        }
        setImageBitmap(null);
    }
}
