package com.adchina.android.ads;

public final class n {
    final /* synthetic */ m a;
    private boolean b = false;
    private String c = "";
    private String d = "";

    public n(m mVar) {
        this.a = mVar;
    }

    public final void a(String str) {
        this.c = str;
    }

    public final void a(boolean z) {
        this.b = z;
    }

    public final boolean a() {
        return this.b;
    }

    public final String b() {
        return this.c;
    }

    public final void b(String str) {
        this.d = str;
    }

    public final String c() {
        return this.d;
    }
}
