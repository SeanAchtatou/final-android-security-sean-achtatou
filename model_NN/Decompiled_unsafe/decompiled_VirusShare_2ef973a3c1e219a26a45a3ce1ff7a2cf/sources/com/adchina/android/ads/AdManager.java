package com.adchina.android.ads;

import android.util.DisplayMetrics;
import android.view.Display;

public class AdManager {
    private static boolean a = false;
    private static boolean b = false;
    private static StringBuffer c = new StringBuffer();
    private static StringBuffer d = new StringBuffer();
    private static StringBuffer e = new StringBuffer();
    private static StringBuffer f = new StringBuffer();
    private static StringBuffer g = new StringBuffer();
    private static StringBuffer h = new StringBuffer();
    private static StringBuffer i = new StringBuffer("");
    private static StringBuffer j = new StringBuffer("");
    private static i k = i.EFemale;
    private static StringBuffer l = new StringBuffer();
    private static StringBuffer m = new StringBuffer();
    private static StringBuffer n = new StringBuffer();
    private static boolean o = true;
    private static int p = 0;
    private static int q = 0;
    private static int r = 0;
    private static int s = -16777216;
    private static int t = 16777215;
    private static int u = 0;
    private static int v = 0;
    private static int w;
    private static int x;

    public static int getAdWindowBackgroundColor() {
        return q;
    }

    public static int getAdWindowBackgroundOpacity() {
        return r;
    }

    public static String getAdspaceId() {
        return c.toString();
    }

    public static int getAnimations() {
        return p;
    }

    public static StringBuffer getAppName() {
        return m;
    }

    public static String getBirthday() {
        return i.toString();
    }

    public static int getCloseImg() {
        return w;
    }

    public static StringBuffer getContentTargeting() {
        return n;
    }

    public static boolean getDebugMode() {
        return a;
    }

    public static String getDefaultCloseImgPath() {
        return "adchina_close.png";
    }

    public static String getDefaultLoadingGifPath() {
        return "loading.gif";
    }

    public static String getFullScreenAdspaceId() {
        return f.toString();
    }

    public static int getFullScreenTimerBgColor() {
        return t;
    }

    public static int getFullScreenTimerTextColor() {
        return s;
    }

    public static i getGender() {
        return k;
    }

    public static int getLoadingImg() {
        return x;
    }

    public static boolean getLogMode() {
        return b;
    }

    public static StringBuffer getPhoneUA() {
        return l;
    }

    public static String getPostalCode() {
        return j.toString();
    }

    public static String getResolution() {
        return h.toString();
    }

    public static String getShrinkFSAdspaceId() {
        return d.toString();
    }

    public static String getTelephoneNumber() {
        return g.toString();
    }

    public static String getVideoAdspaceId() {
        return e.toString();
    }

    public static int getVideoHeight() {
        return v;
    }

    public static int getVideoWidth() {
        return u;
    }

    public static boolean isShowFullScreenTimer() {
        return o;
    }

    public static void setAdWindowBackgroundColor(int i2) {
        q = i2;
    }

    public static void setAdWindowBackgroundOpacity(int i2) {
        r = i2;
    }

    public static void setAdspaceId(String str) {
        c.setLength(0);
        c.append(str);
    }

    public static void setAnimations(int i2) {
        p = i2;
    }

    public static void setAppName(String str) {
        m.setLength(0);
        m.append(str);
    }

    public static void setBirthday(String str) {
        i.setLength(0);
        i.append(str);
    }

    public static void setCloseImg(int i2) {
        w = i2;
    }

    public static void setContentTargeting(String str) {
        n.setLength(0);
        n.append(str);
    }

    public static void setDebugMode(boolean z) {
        a = z;
    }

    public static void setFullScreenAdspaceId(String str) {
        f.setLength(0);
        f.append(str);
    }

    public static void setFullScreenTimerBgColor(int i2) {
        t = i2;
    }

    public static void setFullScreenTimerTextColor(int i2) {
        s = i2;
    }

    public static void setGender(i iVar) {
        k = iVar;
    }

    public static void setLoadingImg(int i2) {
        x = i2;
    }

    public static void setLogMode(boolean z) {
        b = z;
    }

    protected static void setPhoneUA(String str) {
        if (l.length() == 0) {
            l.append(str);
        }
    }

    public static void setPostalCode(String str) {
        j.setLength(0);
        j.append(str);
    }

    public static void setResolution(DisplayMetrics displayMetrics) {
        setResolution(Utils.concatString(Integer.valueOf(displayMetrics.widthPixels), "x", Integer.valueOf(displayMetrics.heightPixels)));
    }

    public static void setResolution(Display display) {
        setResolution(Utils.concatString(Integer.valueOf(display.getWidth()), "x", Integer.valueOf(display.getHeight())));
    }

    public static void setResolution(String str) {
        h.setLength(0);
        h.append(str);
    }

    public static void setShowFullScreenTimer(boolean z) {
        o = z;
    }

    public static void setShrinkFSAdspaceId(String str) {
        d.setLength(0);
        d.append(str);
    }

    public static void setTelephoneNumber(String str) {
        g.setLength(0);
        g.append(str);
    }

    public static void setVideoAdspaceId(String str) {
        e.setLength(0);
        e.append(str);
    }

    public static void setVideoHeight(int i2) {
        v = i2;
    }

    public static void setVideoWidth(int i2) {
        u = i2;
    }
}
