package com.adchina.android.ads.a;

import android.graphics.Bitmap;
import com.adchina.android.ads.Utils;
import com.adchina.android.ads.c;
import java.io.FileInputStream;

final class f implements c {
    final /* synthetic */ e a;
    private final /* synthetic */ String b;

    f(e eVar, String str) {
        this.a = eVar;
        this.b = str;
    }

    public final Object a() {
        FileInputStream fileInputStream;
        FileInputStream fileInputStream2;
        try {
            fileInputStream2 = new FileInputStream(this.b);
            try {
                Bitmap convertStreamToBitmap = Utils.convertStreamToBitmap(fileInputStream2);
                Utils.closeStream(fileInputStream2);
                return convertStreamToBitmap;
            } catch (Exception e) {
                try {
                    this.a.a(18, "Gif AdMaterial is null");
                    Utils.closeStream(fileInputStream2);
                    return null;
                } catch (Throwable th) {
                    Throwable th2 = th;
                    fileInputStream = fileInputStream2;
                    th = th2;
                    Utils.closeStream(fileInputStream);
                    throw th;
                }
            }
        } catch (Exception e2) {
            fileInputStream2 = null;
            this.a.a(18, "Gif AdMaterial is null");
            Utils.closeStream(fileInputStream2);
            return null;
        } catch (Throwable th3) {
            th = th3;
            fileInputStream = null;
            Utils.closeStream(fileInputStream);
            throw th;
        }
    }
}
