package com.adchina.android.ads.a;

import android.content.Context;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.telephony.TelephonyManager;
import android.util.Log;
import com.adchina.android.ads.AdListener;
import com.adchina.android.ads.AdManager;
import com.adchina.android.ads.Utils;
import com.adchina.android.ads.aa;
import com.adchina.android.ads.f;
import com.adchina.android.ads.g;
import com.adchina.android.ads.h;
import com.adchina.android.ads.i;
import com.adchina.android.ads.k;
import com.adchina.android.ads.l;
import com.adchina.android.ads.t;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URLEncoder;
import java.util.LinkedList;

public abstract class b implements Handler.Callback {
    protected static AdListener X = null;
    protected StringBuffer A = new StringBuffer();
    protected StringBuffer B = new StringBuffer();
    protected StringBuffer C = new StringBuffer();
    protected StringBuffer D = new StringBuffer();
    protected StringBuffer E = new StringBuffer();
    protected StringBuffer F = new StringBuffer();
    protected StringBuffer G = new StringBuffer();
    protected boolean H = false;
    protected boolean I = false;
    protected Context J;
    protected StringBuffer K = new StringBuffer();
    protected StringBuffer L = new StringBuffer();
    protected StringBuffer M = new StringBuffer();
    protected StringBuffer N = new StringBuffer();
    protected boolean O = false;
    protected boolean P = false;
    protected LinkedList Q = new LinkedList();
    protected LinkedList R = new LinkedList();
    protected LinkedList S = new LinkedList();
    protected d T;
    protected c U;
    protected f V = f.EIdle;
    protected t W = null;
    protected Handler Y;
    protected h Z;
    protected g a = g.EReceiveAd;
    protected l b = l.EAdNONE;
    protected StringBuffer c = new StringBuffer();
    protected StringBuffer d = new StringBuffer();
    protected StringBuffer e = new StringBuffer();
    protected StringBuffer f = new StringBuffer();
    protected StringBuffer g = new StringBuffer();
    protected StringBuffer h = new StringBuffer();
    protected StringBuffer i = new StringBuffer();
    protected StringBuffer j = new StringBuffer();
    protected StringBuffer k = new StringBuffer();
    protected StringBuffer l = new StringBuffer();
    protected StringBuffer m = new StringBuffer();
    protected StringBuffer n = new StringBuffer();
    protected StringBuffer o = new StringBuffer();
    protected StringBuffer p = new StringBuffer();
    protected StringBuffer q = new StringBuffer();
    protected StringBuffer r = new StringBuffer();
    protected StringBuffer s = new StringBuffer();
    protected StringBuffer t = new StringBuffer();
    protected StringBuffer u = new StringBuffer();
    protected StringBuffer v = new StringBuffer();
    protected StringBuffer w = new StringBuffer();
    protected StringBuffer x = new StringBuffer();
    protected StringBuffer y = new StringBuffer();
    protected StringBuffer z = new StringBuffer();

    public b(Context context) {
        this.J = context;
        this.W = new t(context);
        this.Y = new Handler(this);
    }

    public static void a(AdListener adListener) {
        X = adListener;
    }

    /* access modifiers changed from: protected */
    public final void a(int i2, Object obj) {
        Message message = new Message();
        message.what = i2;
        message.obj = obj;
        this.Y.sendMessage(message);
    }

    public void a(c cVar) {
    }

    public void a(d dVar) {
    }

    /* access modifiers changed from: protected */
    public final void a(f fVar) {
        this.V = fVar;
    }

    /* access modifiers changed from: protected */
    public final void a(g gVar) {
        this.a = gVar;
    }

    /* access modifiers changed from: protected */
    public final void a(InputStream inputStream) {
        this.Z.c("++ readAdserverInfo");
        aa aaVar = new aa();
        aaVar.a(this.Z);
        aaVar.a(inputStream);
        g();
        aaVar.a(this.c, this.d, this.e, this.f, this.g, this.h, this.i, this.j, this.k, this.l, this.m, this.n, this.o, this.p, this.q, this.r, this.s, this.t, this.u, this.v, this.w, this.x, this.y, this.z, this.A, this.B, this.C, this.D, this.E, this.F, this.G);
        if (this.d.toString().compareToIgnoreCase("txt") == 0) {
            this.b = l.EAdTXT;
        } else if (this.d.toString().compareToIgnoreCase("jpg") == 0) {
            this.b = l.EAdJPG;
        } else if (this.d.toString().compareToIgnoreCase("png") == 0) {
            this.b = l.EAdPNG;
        } else if (this.d.toString().compareToIgnoreCase("gif") == 0) {
            this.b = l.EAdGIF;
        } else {
            this.b = l.EAdNONE;
        }
        b(this.i.toString());
        this.Z.c(Utils.concatString(this.d, " : "));
        this.Z.c(Utils.concatString("ImpUrl:", this.g.toString()));
        this.Z.c(Utils.concatString("ClkUrl:", this.e.toString()));
        this.Z.c(Utils.concatString("FC:", this.i.toString()));
        this.Z.c(Utils.concatString("FullScreenImgAddr:", this.z.toString()));
        this.Z.c(Utils.concatString("VideoAddr:", this.D.toString()));
        if (!(l.EAdTXT == this.b || l.EAdNONE == this.b)) {
            this.Z.c(Utils.concatString("ImgAddr:", this.n.toString()));
        }
        this.Z.c("-- readAdserverInfo");
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x004d A[SYNTHETIC, Splitter:B:16:0x004d] */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0052 A[Catch:{ IOException -> 0x0056 }] */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x005d A[SYNTHETIC, Splitter:B:24:0x005d] */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0062 A[Catch:{ IOException -> 0x0068 }] */
    /* JADX WARNING: Removed duplicated region for block: B:42:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(java.lang.String r9, java.lang.String r10) {
        /*
            r8 = this;
            r5 = 0
            r1 = 2
            r4 = 1
            r3 = 0
            com.adchina.android.ads.h r0 = r8.Z
            java.lang.Object[] r1 = new java.lang.Object[r1]
            java.lang.String r2 = "saveFcParams : "
            r1[r3] = r2
            r1[r4] = r9
            java.lang.String r1 = com.adchina.android.ads.Utils.concatString(r1)
            r0.c(r1)
            android.content.Context r0 = r8.J     // Catch:{ Exception -> 0x0030, all -> 0x0058 }
            r1 = 0
            java.io.FileOutputStream r0 = r0.openFileOutput(r10, r1)     // Catch:{ Exception -> 0x0030, all -> 0x0058 }
            java.io.OutputStreamWriter r1 = new java.io.OutputStreamWriter     // Catch:{ Exception -> 0x0076, all -> 0x006a }
            r1.<init>(r0)     // Catch:{ Exception -> 0x0076, all -> 0x006a }
            r1.write(r9)     // Catch:{ Exception -> 0x007b, all -> 0x006f }
            r1.flush()     // Catch:{ Exception -> 0x007b, all -> 0x006f }
            r1.close()     // Catch:{ IOException -> 0x0066 }
            if (r0 == 0) goto L_0x002f
            r0.close()     // Catch:{ IOException -> 0x0066 }
        L_0x002f:
            return
        L_0x0030:
            r0 = move-exception
            r1 = r5
            r2 = r5
        L_0x0033:
            com.adchina.android.ads.h r3 = r8.Z     // Catch:{ all -> 0x0074 }
            r4 = 2
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ all -> 0x0074 }
            r5 = 0
            java.lang.String r6 = "Exceptions in saveFcParams, err = "
            r4[r5] = r6     // Catch:{ all -> 0x0074 }
            r5 = 1
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0074 }
            r4[r5] = r0     // Catch:{ all -> 0x0074 }
            java.lang.String r0 = com.adchina.android.ads.Utils.concatString(r4)     // Catch:{ all -> 0x0074 }
            r3.c(r0)     // Catch:{ all -> 0x0074 }
            if (r1 == 0) goto L_0x0050
            r1.close()     // Catch:{ IOException -> 0x0056 }
        L_0x0050:
            if (r2 == 0) goto L_0x002f
            r2.close()     // Catch:{ IOException -> 0x0056 }
            goto L_0x002f
        L_0x0056:
            r0 = move-exception
            goto L_0x002f
        L_0x0058:
            r0 = move-exception
            r1 = r5
            r2 = r5
        L_0x005b:
            if (r1 == 0) goto L_0x0060
            r1.close()     // Catch:{ IOException -> 0x0068 }
        L_0x0060:
            if (r2 == 0) goto L_0x0065
            r2.close()     // Catch:{ IOException -> 0x0068 }
        L_0x0065:
            throw r0
        L_0x0066:
            r0 = move-exception
            goto L_0x002f
        L_0x0068:
            r1 = move-exception
            goto L_0x0065
        L_0x006a:
            r1 = move-exception
            r2 = r0
            r0 = r1
            r1 = r5
            goto L_0x005b
        L_0x006f:
            r2 = move-exception
            r7 = r2
            r2 = r0
            r0 = r7
            goto L_0x005b
        L_0x0074:
            r0 = move-exception
            goto L_0x005b
        L_0x0076:
            r1 = move-exception
            r2 = r0
            r0 = r1
            r1 = r5
            goto L_0x0033
        L_0x007b:
            r2 = move-exception
            r7 = r2
            r2 = r0
            r0 = r7
            goto L_0x0033
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adchina.android.ads.a.b.a(java.lang.String, java.lang.String):void");
    }

    /* access modifiers changed from: protected */
    public void b(String str) {
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x004d A[SYNTHETIC, Splitter:B:17:0x004d] */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0052 A[Catch:{ IOException -> 0x0057 }] */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0074 A[SYNTHETIC, Splitter:B:27:0x0074] */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0079 A[Catch:{ IOException -> 0x007d }] */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x00c2  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.String c(java.lang.String r13) {
        /*
            r12 = this;
            r10 = 0
            r9 = 2
            r8 = 1
            r7 = 0
            android.content.Context r0 = r12.J     // Catch:{ Exception -> 0x0030, all -> 0x006f }
            java.io.File r0 = r0.getFileStreamPath(r13)     // Catch:{ Exception -> 0x0030, all -> 0x006f }
            long r0 = r0.length()     // Catch:{ Exception -> 0x0030, all -> 0x006f }
            int r0 = (int) r0     // Catch:{ Exception -> 0x0030, all -> 0x006f }
            char[] r0 = new char[r0]     // Catch:{ Exception -> 0x0030, all -> 0x006f }
            android.content.Context r1 = r12.J     // Catch:{ Exception -> 0x0030, all -> 0x006f }
            java.lang.String r2 = "adchinaFC.fc"
            java.io.FileInputStream r1 = r1.openFileInput(r2)     // Catch:{ Exception -> 0x0030, all -> 0x006f }
            java.io.InputStreamReader r2 = new java.io.InputStreamReader     // Catch:{ Exception -> 0x00b7, all -> 0x00ac }
            r2.<init>(r1)     // Catch:{ Exception -> 0x00b7, all -> 0x00ac }
            r2.read(r0)     // Catch:{ Exception -> 0x00bc, all -> 0x00b0 }
            java.lang.String r3 = new java.lang.String     // Catch:{ Exception -> 0x00bc, all -> 0x00b0 }
            r3.<init>(r0)     // Catch:{ Exception -> 0x00bc, all -> 0x00b0 }
            r2.close()     // Catch:{ IOException -> 0x0094 }
            if (r1 == 0) goto L_0x00aa
            r1.close()     // Catch:{ IOException -> 0x0094 }
            r0 = r3
        L_0x002f:
            return r0
        L_0x0030:
            r0 = move-exception
            r1 = r10
            r2 = r10
        L_0x0033:
            com.adchina.android.ads.h r3 = r12.Z     // Catch:{ all -> 0x00b5 }
            r4 = 2
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ all -> 0x00b5 }
            r5 = 0
            java.lang.String r6 = "Exceptions in getFcParams 1, err = "
            r4[r5] = r6     // Catch:{ all -> 0x00b5 }
            r5 = 1
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x00b5 }
            r4[r5] = r0     // Catch:{ all -> 0x00b5 }
            java.lang.String r0 = com.adchina.android.ads.Utils.concatString(r4)     // Catch:{ all -> 0x00b5 }
            r3.c(r0)     // Catch:{ all -> 0x00b5 }
            if (r1 == 0) goto L_0x0050
            r1.close()     // Catch:{ IOException -> 0x0057 }
        L_0x0050:
            if (r2 == 0) goto L_0x00c2
            r2.close()     // Catch:{ IOException -> 0x0057 }
            r0 = r10
            goto L_0x002f
        L_0x0057:
            r0 = move-exception
            com.adchina.android.ads.h r1 = r12.Z
            java.lang.Object[] r2 = new java.lang.Object[r9]
            java.lang.String r3 = "Exceptions in getFcParams 2, err = "
            r2[r7] = r3
            java.lang.String r0 = r0.toString()
            r2[r8] = r0
            java.lang.String r0 = com.adchina.android.ads.Utils.concatString(r2)
            r1.c(r0)
            r0 = r10
            goto L_0x002f
        L_0x006f:
            r0 = move-exception
            r1 = r10
            r2 = r10
        L_0x0072:
            if (r1 == 0) goto L_0x0077
            r1.close()     // Catch:{ IOException -> 0x007d }
        L_0x0077:
            if (r2 == 0) goto L_0x007c
            r2.close()     // Catch:{ IOException -> 0x007d }
        L_0x007c:
            throw r0
        L_0x007d:
            r1 = move-exception
            com.adchina.android.ads.h r2 = r12.Z
            java.lang.Object[] r3 = new java.lang.Object[r9]
            java.lang.String r4 = "Exceptions in getFcParams 2, err = "
            r3[r7] = r4
            java.lang.String r1 = r1.toString()
            r3[r8] = r1
            java.lang.String r1 = com.adchina.android.ads.Utils.concatString(r3)
            r2.c(r1)
            goto L_0x007c
        L_0x0094:
            r0 = move-exception
            com.adchina.android.ads.h r1 = r12.Z
            java.lang.Object[] r2 = new java.lang.Object[r9]
            java.lang.String r4 = "Exceptions in getFcParams 2, err = "
            r2[r7] = r4
            java.lang.String r0 = r0.toString()
            r2[r8] = r0
            java.lang.String r0 = com.adchina.android.ads.Utils.concatString(r2)
            r1.c(r0)
        L_0x00aa:
            r0 = r3
            goto L_0x002f
        L_0x00ac:
            r0 = move-exception
            r2 = r1
            r1 = r10
            goto L_0x0072
        L_0x00b0:
            r0 = move-exception
            r11 = r2
            r2 = r1
            r1 = r11
            goto L_0x0072
        L_0x00b5:
            r0 = move-exception
            goto L_0x0072
        L_0x00b7:
            r0 = move-exception
            r2 = r1
            r1 = r10
            goto L_0x0033
        L_0x00bc:
            r0 = move-exception
            r11 = r2
            r2 = r1
            r1 = r11
            goto L_0x0033
        L_0x00c2:
            r0 = r10
            goto L_0x002f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adchina.android.ads.a.b.c(java.lang.String):java.lang.String");
    }

    /* access modifiers changed from: protected */
    public void c() {
        this.N.setLength(0);
        this.N.append(AdManager.getAdspaceId());
    }

    /* access modifiers changed from: protected */
    public String d() {
        return "";
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        if (this.Z != null) {
            this.Z.a();
            this.Z.b();
        }
    }

    /* access modifiers changed from: protected */
    public void g() {
        this.c.setLength(0);
        this.d.setLength(0);
        this.n.setLength(0);
        this.e.setLength(0);
        this.f.setLength(0);
        this.g.setLength(0);
        this.h.setLength(0);
        this.i.setLength(0);
        this.j.setLength(0);
        this.o.setLength(0);
        this.m.setLength(0);
        this.Q.clear();
        this.R.clear();
        this.S.clear();
        this.p.setLength(0);
        this.q.setLength(0);
        this.r.setLength(0);
        this.s.setLength(0);
        this.t.setLength(0);
        this.u.setLength(0);
        this.v.setLength(0);
        this.w.setLength(0);
        this.x.setLength(0);
        this.y.setLength(0);
        this.z.setLength(0);
        this.A.setLength(0);
        this.B.setLength(0);
        this.C.setLength(0);
        this.D.setLength(0);
        this.E.setLength(0);
        this.F.setLength(0);
        this.G.setLength(0);
    }

    /* access modifiers changed from: protected */
    public final String h() {
        try {
            String concatString = Utils.concatString("Android ", Build.VERSION.RELEASE);
            Object[] objArr = new Object[2];
            objArr[0] = "&ma=";
            Object[] objArr2 = new Object[8];
            objArr2[0] = URLEncoder.encode("2.3.6", "utf-8");
            objArr2[1] = URLEncoder.encode(concatString, "utf-8");
            objArr2[2] = URLEncoder.encode(this.M.toString(), "utf-8");
            objArr2[3] = URLEncoder.encode(this.L.toString(), "utf-8");
            objArr2[4] = URLEncoder.encode(AdManager.getResolution(), "utf-8");
            objArr2[5] = AdManager.getGender() == i.EFemale ? "2" : "1";
            objArr2[6] = URLEncoder.encode(AdManager.getPostalCode(), "utf-8");
            objArr2[7] = URLEncoder.encode(AdManager.getBirthday(), "utf-8");
            objArr[1] = URLEncoder.encode(String.format("%s,%s,%s,%s,%s,%s,%s,%s", objArr2), "utf-8");
            return Utils.concatString(objArr);
        } catch (Exception e2) {
            String concatString2 = Utils.concatString("Failed to getMaParams, err = ", e2.toString());
            this.Z.c(concatString2);
            Log.e("AdChinaError", concatString2);
            return "";
        }
    }

    /* access modifiers changed from: protected */
    public final String i() {
        String d2 = d();
        if (!(d2 == null || -1 == d2.indexOf(" "))) {
            d2 = "";
        }
        this.Z.c(Utils.concatString("FC:", d2));
        c();
        return Utils.concatString(String.format("http://amob.acs86.com/a.htm?pv=1&sp=%s,0,0,0,0,1,1,10&ec=utf-8&cb=%d&fc=%s&ts=1262152690.23&stt=4&mpid=100&muid=%s&g=4&an=%s&ct=%s&ua=%s&bn=%s&pad=0", this.N, Integer.valueOf(Utils.GetRandomNumber()), d2, URLEncoder.encode(this.K.toString(), "utf-8"), URLEncoder.encode(AdManager.getAppName().toString(), "utf-8"), URLEncoder.encode(AdManager.getContentTargeting().toString(), "utf-8"), URLEncoder.encode(AdManager.getPhoneUA().toString(), "utf-8"), URLEncoder.encode(Build.BRAND, "utf-8")), h());
    }

    /* access modifiers changed from: protected */
    public final String j() {
        String d2 = d();
        if (!(d2 == null || -1 == d2.indexOf(" "))) {
            d2 = "";
        }
        this.Z.c(Utils.concatString("FC:", d2));
        c();
        return Utils.concatString(String.format(k.f, this.N, Integer.valueOf(Utils.GetRandomNumber()), d2, this.K.toString(), URLEncoder.encode(AdManager.getAppName().toString(), "utf-8"), URLEncoder.encode(AdManager.getContentTargeting().toString(), "utf-8"), URLEncoder.encode(AdManager.getPhoneUA().toString(), "utf-8"), URLEncoder.encode(Build.BRAND, "utf-8")), h());
    }

    public final void k() {
        if (this.H) {
            this.a = g.EReceiveAd;
            return;
        }
        this.I = false;
        this.O = false;
        this.P = false;
        File file = new File(k.e);
        if (file.exists()) {
            AdManager.setDebugMode(true);
            AdManager.setLogMode(true);
            try {
                InputStreamReader inputStreamReader = new InputStreamReader(new FileInputStream(file));
                char[] cArr = new char[((int) file.length())];
                inputStreamReader.read(cArr);
                String[] split = new String(cArr).replace("\r", "").split("\n");
                for (String str : split) {
                    int indexOf = str.indexOf(58);
                    if (indexOf >= 0 && indexOf < str.length()) {
                        String substring = str.substring(0, indexOf);
                        String substring2 = str.substring(indexOf + 1);
                        if (substring.equals("testurl")) {
                            k.f = Utils.concatString(substring2.trim(), "/a.htm?pv=1&sp=%s,0,0,0,0,1,1,10&ec=utf-8&cb=%d&fc=%s&ts=1262152690.23&stt=4&mpid=100&muid=%s&g=4&an=%s&ct=%s&ua=%s&bn=%s&pad=0");
                        } else if (substring.equals("bannerAdSpaceId")) {
                            AdManager.setAdspaceId(substring2.trim());
                        } else if (substring.equals("shrinkFSAdSpaceId")) {
                            AdManager.setShrinkFSAdspaceId(substring2.trim());
                        } else if (substring.equals("videoPreAdSpaceId")) {
                            AdManager.setVideoAdspaceId(substring2.trim());
                        } else if (substring.equals("fullScreenAdSpaceId")) {
                            AdManager.setFullScreenAdspaceId(substring2.trim());
                        }
                    }
                }
            } catch (FileNotFoundException e2) {
            } catch (IOException e3) {
            }
        }
        String imei = Utils.getIMEI(this.J);
        String line1Number = ((TelephonyManager) this.J.getSystemService("phone")).getLine1Number();
        if (imei != null) {
            this.K.setLength(0);
            this.K.append(imei);
        }
        if (line1Number != null) {
            this.M.setLength(0);
            this.M.append(line1Number);
        }
        c();
        this.L.setLength(0);
        this.L.append(Build.MODEL.replaceAll(" ", "%20"));
        this.Z = new h();
        this.Z.a(Utils.concatString("AdChina_", this.N, ".txt"));
        this.Z.b(Utils.concatString("AdChinaOutput_", this.N, ".txt"));
        this.W.a(this.Z);
        this.a = g.EReceiveAd;
        this.T = new d(this);
        this.T.start();
        this.U = new c(this);
        this.U.start();
        this.H = true;
    }

    public void l() {
        this.I = true;
        this.H = false;
        this.O = false;
        this.P = false;
        try {
            if (this.T != null && this.T.isAlive()) {
                this.T.a = true;
                this.T = null;
            }
            if (this.U != null && this.U.isAlive()) {
                this.U.a = true;
                this.U = null;
            }
            if (this.Z != null) {
                this.Z.a();
                this.Z.b();
            }
        } catch (Exception e2) {
        }
    }
}
