package com.adchina.android.ads.views.animations;

import android.view.animation.DecelerateInterpolator;
import com.adchina.android.ads.views.ContentView;

final class b implements Runnable {
    final /* synthetic */ a a;
    private final /* synthetic */ float b;
    private final /* synthetic */ float c;
    private final /* synthetic */ ContentView d;

    b(a aVar, float f, float f2, ContentView contentView) {
        this.a = aVar;
        this.b = f;
        this.c = f2;
        this.d = contentView;
    }

    public final void run() {
        o oVar = new o(-90.0f, 0.0f, this.b, this.c, false);
        oVar.setDuration(750);
        oVar.setFillAfter(true);
        oVar.setInterpolator(new DecelerateInterpolator());
        this.d.startAnimation(oVar);
    }
}
