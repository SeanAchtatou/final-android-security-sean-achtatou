package com.adchina.android.ads.a;

import android.content.Context;
import android.os.Message;
import android.util.Log;
import com.adchina.android.ads.AdEngine;
import com.adchina.android.ads.AdFullScreenFinishListener;
import com.adchina.android.ads.AdManager;
import com.adchina.android.ads.Utils;
import com.adchina.android.ads.a;
import com.adchina.android.ads.f;
import com.adchina.android.ads.g;
import com.adchina.android.ads.k;
import com.adchina.android.ads.l;
import com.adchina.android.ads.t;
import com.adchina.android.ads.views.GifImageView;
import com.adchina.android.ads.views.m;
import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;

public final class e extends b {
    private static e ab;
    private static /* synthetic */ int[] ai;
    private static /* synthetic */ int[] aj;
    private HashMap aa = new HashMap();
    private int ac = 1;
    private n ad;
    /* access modifiers changed from: private */
    public AdFullScreenFinishListener ae;
    private HashMap af = new HashMap();
    private boolean ag = false;
    private boolean ah = false;

    private e(Context context) {
        super(context);
    }

    public static e a() {
        return ab;
    }

    public static e a(Context context) {
        if (ab == null) {
            ab = new e(context);
        }
        return ab;
    }

    /* JADX WARNING: Removed duplicated region for block: B:39:0x00d7 A[Catch:{ all -> 0x00db }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void n() {
        /*
            r10 = this;
            r9 = 2
            r8 = 1
            r7 = 0
            com.adchina.android.ads.h r0 = r10.Z
            java.lang.String r1 = "++ onGetImgMaterial"
            r0.c(r1)
            com.adchina.android.ads.g r0 = com.adchina.android.ads.g.EIdle
            r10.a(r0)
            java.lang.StringBuffer r0 = r10.n     // Catch:{ Exception -> 0x0110 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x0110 }
            com.adchina.android.ads.l r1 = com.adchina.android.ads.l.EAdNONE     // Catch:{ Exception -> 0x0110 }
            com.adchina.android.ads.l r2 = r10.b     // Catch:{ Exception -> 0x0110 }
            if (r1 == r2) goto L_0x007c
            java.util.HashMap r1 = r10.aa     // Catch:{ Exception -> 0x0110 }
            boolean r1 = r1.containsKey(r0)     // Catch:{ Exception -> 0x0110 }
            if (r1 != 0) goto L_0x007c
            com.adchina.android.ads.h r1 = r10.Z     // Catch:{ Exception -> 0x0057 }
            r2 = 2
            java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch:{ Exception -> 0x0057 }
            r3 = 0
            java.lang.String r4 = "++ start to download FullScreen Img file"
            r2[r3] = r4     // Catch:{ Exception -> 0x0057 }
            r3 = 1
            r2[r3] = r0     // Catch:{ Exception -> 0x0057 }
            java.lang.String r2 = com.adchina.android.ads.Utils.concatString(r2)     // Catch:{ Exception -> 0x0057 }
            r1.c(r2)     // Catch:{ Exception -> 0x0057 }
            java.io.File r1 = new java.io.File     // Catch:{ Exception -> 0x0057 }
            java.lang.String r2 = com.adchina.android.ads.k.b     // Catch:{ Exception -> 0x0057 }
            r1.<init>(r2)     // Catch:{ Exception -> 0x0057 }
            boolean r2 = r1.exists()     // Catch:{ Exception -> 0x0057 }
            if (r2 != 0) goto L_0x0047
            r1.mkdirs()     // Catch:{ Exception -> 0x0057 }
        L_0x0047:
            com.adchina.android.ads.t r2 = r10.W     // Catch:{ Exception -> 0x0057 }
            java.io.InputStream r2 = com.adchina.android.ads.t.b(r0)     // Catch:{ Exception -> 0x0057 }
            if (r2 != 0) goto L_0x0097
            java.lang.Exception r0 = new java.lang.Exception     // Catch:{ Exception -> 0x0057 }
            java.lang.String r1 = "image address is not exists"
            r0.<init>(r1)     // Catch:{ Exception -> 0x0057 }
            throw r0     // Catch:{ Exception -> 0x0057 }
        L_0x0057:
            r0 = move-exception
            r1 = 2
            java.lang.Object[] r1 = new java.lang.Object[r1]     // Catch:{ Exception -> 0x0110 }
            r2 = 0
            java.lang.String r3 = "Failed to download FullScreen Img file, err = "
            r1[r2] = r3     // Catch:{ Exception -> 0x0110 }
            r2 = 1
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x0110 }
            r1[r2] = r0     // Catch:{ Exception -> 0x0110 }
            java.lang.String r0 = com.adchina.android.ads.Utils.concatString(r1)     // Catch:{ Exception -> 0x0110 }
            com.adchina.android.ads.h r1 = r10.Z     // Catch:{ Exception -> 0x0110 }
            r1.c(r0)     // Catch:{ Exception -> 0x0110 }
            java.lang.String r1 = "AdChinaError"
            android.util.Log.e(r1, r0)     // Catch:{ Exception -> 0x0110 }
        L_0x0075:
            com.adchina.android.ads.h r0 = r10.Z     // Catch:{ Exception -> 0x0110 }
            java.lang.String r1 = "++ load remote fullscreen img"
            r0.c(r1)     // Catch:{ Exception -> 0x0110 }
        L_0x007c:
            r0 = 19
            java.lang.String r1 = "Received FullScreen Ad"
            r10.a(r0, r1)     // Catch:{ Exception -> 0x0110 }
            com.adchina.android.ads.h r0 = r10.Z
            java.lang.Object[] r1 = new java.lang.Object[r9]
            java.lang.String r2 = "-- onGetImgMaterial, AdModel = "
            r1[r7] = r2
            com.adchina.android.ads.l r2 = r10.b
            r1[r8] = r2
            java.lang.String r1 = com.adchina.android.ads.Utils.concatString(r1)
            r0.c(r1)
        L_0x0096:
            return
        L_0x0097:
            java.io.File r3 = new java.io.File     // Catch:{ Exception -> 0x0057 }
            r4 = 2
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ Exception -> 0x0057 }
            r5 = 0
            java.lang.String r6 = "yyyyMMddHHmmss"
            java.lang.String r6 = com.adchina.android.ads.Utils.getNowTime(r6)     // Catch:{ Exception -> 0x0057 }
            r4[r5] = r6     // Catch:{ Exception -> 0x0057 }
            r5 = 1
            java.lang.String r6 = "fsImg.tmp"
            r4[r5] = r6     // Catch:{ Exception -> 0x0057 }
            java.lang.String r4 = com.adchina.android.ads.Utils.concatString(r4)     // Catch:{ Exception -> 0x0057 }
            r3.<init>(r1, r4)     // Catch:{ Exception -> 0x0057 }
            boolean r1 = r3.exists()     // Catch:{ Exception -> 0x0057 }
            if (r1 == 0) goto L_0x00ba
            r3.delete()     // Catch:{ Exception -> 0x0057 }
        L_0x00ba:
            r1 = 0
            java.io.FileOutputStream r4 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x014b }
            r4.<init>(r3)     // Catch:{ Exception -> 0x014b }
            r1 = 16384(0x4000, float:2.2959E-41)
            byte[] r1 = new byte[r1]     // Catch:{ Exception -> 0x00cf, all -> 0x0148 }
        L_0x00c4:
            int r5 = r2.read(r1)     // Catch:{ Exception -> 0x00cf, all -> 0x0148 }
            if (r5 <= 0) goto L_0x00fa
            r6 = 0
            r4.write(r1, r6, r5)     // Catch:{ Exception -> 0x00cf, all -> 0x0148 }
            goto L_0x00c4
        L_0x00cf:
            r0 = move-exception
            r1 = r4
        L_0x00d1:
            boolean r4 = r3.exists()     // Catch:{ all -> 0x00db }
            if (r4 == 0) goto L_0x00da
            r3.delete()     // Catch:{ all -> 0x00db }
        L_0x00da:
            throw r0     // Catch:{ all -> 0x00db }
        L_0x00db:
            r0 = move-exception
        L_0x00dc:
            com.adchina.android.ads.t r3 = r10.W     // Catch:{ Exception -> 0x0057 }
            com.adchina.android.ads.t.a(r2)     // Catch:{ Exception -> 0x0057 }
            com.adchina.android.ads.Utils.closeStream(r1)     // Catch:{ Exception -> 0x0057 }
            throw r0     // Catch:{ Exception -> 0x0057 }
        L_0x00e5:
            r0 = move-exception
            com.adchina.android.ads.h r1 = r10.Z
            java.lang.Object[] r2 = new java.lang.Object[r9]
            java.lang.String r3 = "-- onGetImgMaterial, AdModel = "
            r2[r7] = r3
            com.adchina.android.ads.l r3 = r10.b
            r2[r8] = r3
            java.lang.String r2 = com.adchina.android.ads.Utils.concatString(r2)
            r1.c(r2)
            throw r0
        L_0x00fa:
            com.adchina.android.ads.t r1 = r10.W     // Catch:{ Exception -> 0x0057 }
            com.adchina.android.ads.t.a(r2)     // Catch:{ Exception -> 0x0057 }
            com.adchina.android.ads.Utils.closeStream(r4)     // Catch:{ Exception -> 0x0057 }
            java.util.HashMap r1 = r10.aa     // Catch:{ Exception -> 0x0057 }
            java.lang.String r2 = r3.getAbsolutePath()     // Catch:{ Exception -> 0x0057 }
            r1.put(r0, r2)     // Catch:{ Exception -> 0x0057 }
            r10.p()     // Catch:{ Exception -> 0x0057 }
            goto L_0x0075
        L_0x0110:
            r0 = move-exception
            r1 = 2
            java.lang.Object[] r1 = new java.lang.Object[r1]     // Catch:{ all -> 0x00e5 }
            r2 = 0
            java.lang.String r3 = "Failed to get fullscreen ad material, err = "
            r1[r2] = r3     // Catch:{ all -> 0x00e5 }
            r2 = 1
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x00e5 }
            r1[r2] = r0     // Catch:{ all -> 0x00e5 }
            java.lang.String r0 = com.adchina.android.ads.Utils.concatString(r1)     // Catch:{ all -> 0x00e5 }
            r1 = 18
            r10.a(r1, r0)     // Catch:{ all -> 0x00e5 }
            com.adchina.android.ads.h r1 = r10.Z     // Catch:{ all -> 0x00e5 }
            r1.c(r0)     // Catch:{ all -> 0x00e5 }
            java.lang.String r1 = "AdChinaError"
            android.util.Log.e(r1, r0)     // Catch:{ all -> 0x00e5 }
            com.adchina.android.ads.h r0 = r10.Z
            java.lang.Object[] r1 = new java.lang.Object[r9]
            java.lang.String r2 = "-- onGetImgMaterial, AdModel = "
            r1[r7] = r2
            com.adchina.android.ads.l r2 = r10.b
            r1[r8] = r2
            java.lang.String r1 = com.adchina.android.ads.Utils.concatString(r1)
            r0.c(r1)
            goto L_0x0096
        L_0x0148:
            r0 = move-exception
            r1 = r4
            goto L_0x00dc
        L_0x014b:
            r0 = move-exception
            goto L_0x00d1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adchina.android.ads.a.e.n():void");
    }

    private void o() {
        File file = new File(k.b);
        if (file.exists()) {
            for (File file2 : file.listFiles()) {
                if (!this.aa.containsValue(file2.getAbsolutePath())) {
                    file2.delete();
                }
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:29:0x007f A[SYNTHETIC, Splitter:B:29:0x007f] */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0084 A[Catch:{ IOException -> 0x008a }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void p() {
        /*
            r8 = this;
            r3 = 0
            com.adchina.android.ads.h r0 = r8.Z
            java.lang.String r1 = "saveLocalFullScreenImgList"
            r0.c(r1)
            android.content.Context r0 = r8.J     // Catch:{ Exception -> 0x0097, all -> 0x007a }
            java.lang.String r1 = "adchinaFSImgs.fc"
            r2 = 0
            java.io.FileOutputStream r1 = r0.openFileOutput(r1, r2)     // Catch:{ Exception -> 0x0097, all -> 0x007a }
            java.io.OutputStreamWriter r2 = new java.io.OutputStreamWriter     // Catch:{ Exception -> 0x009b, all -> 0x008c }
            r2.<init>(r1)     // Catch:{ Exception -> 0x009b, all -> 0x008c }
            java.util.HashMap r0 = r8.aa     // Catch:{ Exception -> 0x0051, all -> 0x0090 }
            java.util.Set r0 = r0.keySet()     // Catch:{ Exception -> 0x0051, all -> 0x0090 }
            java.util.Iterator r3 = r0.iterator()     // Catch:{ Exception -> 0x0051, all -> 0x0090 }
        L_0x0020:
            boolean r0 = r3.hasNext()     // Catch:{ Exception -> 0x0051, all -> 0x0090 }
            if (r0 != 0) goto L_0x0032
            r2.flush()     // Catch:{ Exception -> 0x0051, all -> 0x0090 }
            r2.close()     // Catch:{ IOException -> 0x0088 }
            if (r1 == 0) goto L_0x0031
            r1.close()     // Catch:{ IOException -> 0x0088 }
        L_0x0031:
            return
        L_0x0032:
            java.lang.Object r0 = r3.next()     // Catch:{ Exception -> 0x0051, all -> 0x0090 }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ Exception -> 0x0051, all -> 0x0090 }
            r2.write(r0)     // Catch:{ Exception -> 0x0051, all -> 0x0090 }
            java.lang.String r4 = "|||"
            r2.write(r4)     // Catch:{ Exception -> 0x0051, all -> 0x0090 }
            java.util.HashMap r4 = r8.aa     // Catch:{ Exception -> 0x0051, all -> 0x0090 }
            java.lang.Object r0 = r4.get(r0)     // Catch:{ Exception -> 0x0051, all -> 0x0090 }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ Exception -> 0x0051, all -> 0x0090 }
            r2.write(r0)     // Catch:{ Exception -> 0x0051, all -> 0x0090 }
            java.lang.String r0 = "\n"
            r2.write(r0)     // Catch:{ Exception -> 0x0051, all -> 0x0090 }
            goto L_0x0020
        L_0x0051:
            r0 = move-exception
            r7 = r2
            r2 = r1
            r1 = r7
        L_0x0055:
            com.adchina.android.ads.h r3 = r8.Z     // Catch:{ all -> 0x0095 }
            r4 = 2
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ all -> 0x0095 }
            r5 = 0
            java.lang.String r6 = "Exceptions in saveLocalFullScreenImgList, err = "
            r4[r5] = r6     // Catch:{ all -> 0x0095 }
            r5 = 1
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0095 }
            r4[r5] = r0     // Catch:{ all -> 0x0095 }
            java.lang.String r0 = com.adchina.android.ads.Utils.concatString(r4)     // Catch:{ all -> 0x0095 }
            r3.c(r0)     // Catch:{ all -> 0x0095 }
            if (r1 == 0) goto L_0x0072
            r1.close()     // Catch:{ IOException -> 0x0078 }
        L_0x0072:
            if (r2 == 0) goto L_0x0031
            r2.close()     // Catch:{ IOException -> 0x0078 }
            goto L_0x0031
        L_0x0078:
            r0 = move-exception
            goto L_0x0031
        L_0x007a:
            r0 = move-exception
            r1 = r3
            r2 = r3
        L_0x007d:
            if (r1 == 0) goto L_0x0082
            r1.close()     // Catch:{ IOException -> 0x008a }
        L_0x0082:
            if (r2 == 0) goto L_0x0087
            r2.close()     // Catch:{ IOException -> 0x008a }
        L_0x0087:
            throw r0
        L_0x0088:
            r0 = move-exception
            goto L_0x0031
        L_0x008a:
            r1 = move-exception
            goto L_0x0087
        L_0x008c:
            r0 = move-exception
            r2 = r1
            r1 = r3
            goto L_0x007d
        L_0x0090:
            r0 = move-exception
            r7 = r2
            r2 = r1
            r1 = r7
            goto L_0x007d
        L_0x0095:
            r0 = move-exception
            goto L_0x007d
        L_0x0097:
            r0 = move-exception
            r1 = r3
            r2 = r3
            goto L_0x0055
        L_0x009b:
            r0 = move-exception
            r2 = r1
            r1 = r3
            goto L_0x0055
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adchina.android.ads.a.e.p():void");
    }

    private static /* synthetic */ int[] q() {
        int[] iArr = ai;
        if (iArr == null) {
            iArr = new int[g.values().length];
            try {
                iArr[g.EGetImgMaterial.ordinal()] = 3;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[g.EGetShrinkFSImgMaterial.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[g.EIdle.ordinal()] = 17;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[g.EReceiveAd.ordinal()] = 1;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[g.ERefreshAd.ordinal()] = 6;
            } catch (NoSuchFieldError e5) {
            }
            try {
                iArr[g.ESendFullScreenEndedTrack.ordinal()] = 16;
            } catch (NoSuchFieldError e6) {
            }
            try {
                iArr[g.ESendFullScreenImpTrack.ordinal()] = 14;
            } catch (NoSuchFieldError e7) {
            }
            try {
                iArr[g.ESendFullScreenStartedTrack.ordinal()] = 13;
            } catch (NoSuchFieldError e8) {
            }
            try {
                iArr[g.ESendFullScreenThdImpTrack.ordinal()] = 15;
            } catch (NoSuchFieldError e9) {
            }
            try {
                iArr[g.ESendImpTrack.ordinal()] = 4;
            } catch (NoSuchFieldError e10) {
            }
            try {
                iArr[g.ESendShrinkFSImpTrack.ordinal()] = 7;
            } catch (NoSuchFieldError e11) {
            }
            try {
                iArr[g.ESendShrinkFSThdImpTrack.ordinal()] = 8;
            } catch (NoSuchFieldError e12) {
            }
            try {
                iArr[g.ESendThdImpTrack.ordinal()] = 5;
            } catch (NoSuchFieldError e13) {
            }
            try {
                iArr[g.ESendVideoEndedImpTrack.ordinal()] = 11;
            } catch (NoSuchFieldError e14) {
            }
            try {
                iArr[g.ESendVideoEndedThdImpTrack.ordinal()] = 12;
            } catch (NoSuchFieldError e15) {
            }
            try {
                iArr[g.ESendVideoStartedImpTrack.ordinal()] = 9;
            } catch (NoSuchFieldError e16) {
            }
            try {
                iArr[g.ESendVideoStartedThdImpTrack.ordinal()] = 10;
            } catch (NoSuchFieldError e17) {
            }
            ai = iArr;
        }
        return iArr;
    }

    private static /* synthetic */ int[] r() {
        int[] iArr = aj;
        if (iArr == null) {
            iArr = new int[f.values().length];
            try {
                iArr[f.EIdle.ordinal()] = 14;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[f.ESendBtnClkTrack.ordinal()] = 3;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[f.ESendBtnThdClkTrack.ordinal()] = 4;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[f.ESendClkTrack.ordinal()] = 1;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[f.ESendFullScreenClkThdTrack.ordinal()] = 12;
            } catch (NoSuchFieldError e5) {
            }
            try {
                iArr[f.ESendFullScreenClkTrack.ordinal()] = 11;
            } catch (NoSuchFieldError e6) {
            }
            try {
                iArr[f.ESendFullScreenCloseTrack.ordinal()] = 13;
            } catch (NoSuchFieldError e7) {
            }
            try {
                iArr[f.ESendShrinkFSClkTrack.ordinal()] = 5;
            } catch (NoSuchFieldError e8) {
            }
            try {
                iArr[f.ESendShrinkFSThdClkTrack.ordinal()] = 6;
            } catch (NoSuchFieldError e9) {
            }
            try {
                iArr[f.ESendThdClkTrack.ordinal()] = 2;
            } catch (NoSuchFieldError e10) {
            }
            try {
                iArr[f.ESendVideoClkTrack.ordinal()] = 7;
            } catch (NoSuchFieldError e11) {
            }
            try {
                iArr[f.ESendVideoCloseClkTrack.ordinal()] = 9;
            } catch (NoSuchFieldError e12) {
            }
            try {
                iArr[f.ESendVideoCloseThdClkTrack.ordinal()] = 10;
            } catch (NoSuchFieldError e13) {
            }
            try {
                iArr[f.ESendVideoThdClkTrack.ordinal()] = 8;
            } catch (NoSuchFieldError e14) {
            }
            aj = iArr;
        }
        return iArr;
    }

    public final void a(Context context, String str) {
        Object obj = null;
        if (this.af.containsKey(str)) {
            obj = this.af.get(str);
            this.af.remove(str);
        }
        if (this.ae != null) {
            a.a(new l(this), new m(this, context, obj));
        }
    }

    public final void a(AdFullScreenFinishListener adFullScreenFinishListener) {
        this.ae = adFullScreenFinishListener;
    }

    public final void a(c cVar) {
        String str;
        String str2;
        String str3;
        while (!this.I && !cVar.a) {
            switch (r()[this.V.ordinal()]) {
                case 11:
                    this.Z.c("++ onSendFullScreenClkTrack");
                    a(f.EIdle);
                    try {
                        String concatString = Utils.concatString(this.f.toString(), "1", ",0,0,0", h());
                        t tVar = this.W;
                        t.a(concatString);
                        this.Z.c(Utils.concatString("send ClkTrack to ", concatString));
                        break;
                    } catch (Exception e) {
                        String concatString2 = Utils.concatString("Exceptions in onSendFullScreenClkTrack, err = ", e.toString());
                        this.Z.c(concatString2);
                        Log.e("AdChinaError", concatString2);
                        break;
                    } finally {
                        a(f.ESendFullScreenClkThdTrack);
                        str3 = "-- onSendFullScreenClkTrack";
                        this.Z.c(str3);
                    }
                case 12:
                    a(f.EIdle);
                    int size = this.Q.size();
                    this.Z.c(Utils.concatString("++ onSendFullScreenThdClkTrack, Size of list is ", Integer.valueOf(size)));
                    if (size > 0) {
                        try {
                            String str4 = (String) this.Q.get(size - 1);
                            this.Q.removeLast();
                            t tVar2 = this.W;
                            t.a(str4);
                            this.Z.c(Utils.concatString("send FullScreenThdClkTrack to ", str4));
                            break;
                        } catch (Exception e2) {
                            String concatString3 = Utils.concatString("Failed to onSendFullScreenThdClkTrack, err = ", e2.toString());
                            this.Z.c(concatString3);
                            Log.e("AdChinaError", concatString3);
                            break;
                        } finally {
                            a(f.ESendFullScreenClkThdTrack);
                            str2 = "-- onSendFullScreenThdClkTrack";
                            this.Z.c(str2);
                        }
                    }
                    break;
                case 13:
                    this.Z.c("++ onSendFullScreenCloseTrack");
                    a(f.EIdle);
                    try {
                        String concatString4 = Utils.concatString(this.f.toString(), "211", ",0,0,0", h());
                        t tVar3 = this.W;
                        t.a(concatString4);
                        this.Z.c(Utils.concatString("send BtnClkTrack to ", concatString4));
                    } catch (Exception e3) {
                        String concatString5 = Utils.concatString("Exceptions in onSendFullScreenCloseTrack, err = ", e3.toString());
                        this.Z.c(concatString5);
                        Log.e("AdChinaError", concatString5);
                    } finally {
                        a(f.ESendBtnThdClkTrack);
                        str = "-- onSendFullScreenCloseTrack";
                        this.Z.c(str);
                    }
                    if (this.ad != null) {
                        this.ad.c();
                    }
                    a(3, "RefreshAd");
                    break;
            }
            try {
                Thread.sleep(50);
            } catch (InterruptedException e4) {
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:65:0x0187 A[SYNTHETIC, Splitter:B:65:0x0187] */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x018c A[Catch:{ IOException -> 0x0194 }] */
    /* JADX WARNING: Removed duplicated region for block: B:82:0x01e9 A[SYNTHETIC, Splitter:B:82:0x01e9] */
    /* JADX WARNING: Removed duplicated region for block: B:85:0x01ee A[Catch:{ IOException -> 0x0206 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(com.adchina.android.ads.a.d r12) {
        /*
            r11 = this;
        L_0x0000:
            boolean r0 = r11.I
            if (r0 != 0) goto L_0x0008
            boolean r0 = r12.a
            if (r0 == 0) goto L_0x0009
        L_0x0008:
            return
        L_0x0009:
            int[] r0 = q()
            com.adchina.android.ads.g r1 = r11.a
            int r1 = r1.ordinal()
            r0 = r0[r1]
            switch(r0) {
                case 1: goto L_0x0020;
                case 2: goto L_0x0018;
                case 3: goto L_0x0243;
                case 4: goto L_0x0018;
                case 5: goto L_0x0018;
                case 6: goto L_0x046d;
                case 7: goto L_0x0018;
                case 8: goto L_0x0018;
                case 9: goto L_0x0018;
                case 10: goto L_0x0018;
                case 11: goto L_0x0018;
                case 12: goto L_0x0018;
                case 13: goto L_0x0248;
                case 14: goto L_0x02d1;
                case 15: goto L_0x035a;
                case 16: goto L_0x03e0;
                default: goto L_0x0018;
            }
        L_0x0018:
            r0 = 50
            java.lang.Thread.sleep(r0)     // Catch:{ InterruptedException -> 0x001e }
            goto L_0x0000
        L_0x001e:
            r0 = move-exception
            goto L_0x0000
        L_0x0020:
            com.adchina.android.ads.a.n r0 = r11.ad
            if (r0 == 0) goto L_0x0038
            com.adchina.android.ads.a.n r0 = r11.ad
            boolean r0 = r0.b()
            if (r0 != 0) goto L_0x0038
            com.adchina.android.ads.a.n r0 = r11.ad
            java.lang.StringBuffer r0 = r0.a()
            int r0 = r0.length()
            if (r0 > 0) goto L_0x0018
        L_0x0038:
            com.adchina.android.ads.h r0 = r11.Z
            java.lang.String r1 = "++ onReceiveFullScreenAd"
            r0.c(r1)
            com.adchina.android.ads.g r0 = com.adchina.android.ads.g.EIdle
            r11.a(r0)
            r0 = 0
            boolean r1 = com.adchina.android.ads.AdManager.getDebugMode()     // Catch:{ Exception -> 0x04a8, all -> 0x049f }
            if (r1 == 0) goto L_0x0112
            java.lang.String r1 = r11.j()     // Catch:{ Exception -> 0x04a8, all -> 0x049f }
            if (r1 == 0) goto L_0x0071
            com.adchina.android.ads.h r2 = r11.Z     // Catch:{ Exception -> 0x04a8, all -> 0x049f }
            r3 = 2
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ Exception -> 0x04a8, all -> 0x049f }
            r4 = 0
            java.lang.String r5 = "AdserverUrl:"
            r3[r4] = r5     // Catch:{ Exception -> 0x04a8, all -> 0x049f }
            r4 = 1
            r3[r4] = r1     // Catch:{ Exception -> 0x04a8, all -> 0x049f }
            java.lang.String r3 = com.adchina.android.ads.Utils.concatString(r3)     // Catch:{ Exception -> 0x04a8, all -> 0x049f }
            r2.c(r3)     // Catch:{ Exception -> 0x04a8, all -> 0x049f }
            com.adchina.android.ads.t r2 = r11.W     // Catch:{ Exception -> 0x04a8, all -> 0x049f }
            java.lang.StringBuffer r3 = r11.N     // Catch:{ Exception -> 0x04a8, all -> 0x049f }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x04a8, all -> 0x049f }
            java.io.InputStream r0 = r2.a(r1, r3)     // Catch:{ Exception -> 0x04a8, all -> 0x049f }
        L_0x0071:
            r11.a(r0)     // Catch:{ Exception -> 0x01af, all -> 0x01f5 }
            java.lang.StringBuffer r1 = r11.n     // Catch:{ Exception -> 0x01af, all -> 0x01f5 }
            int r1 = r1.length()     // Catch:{ Exception -> 0x01af, all -> 0x01f5 }
            if (r1 <= 0) goto L_0x008a
            com.adchina.android.ads.a.n r1 = new com.adchina.android.ads.a.n     // Catch:{ Exception -> 0x01af, all -> 0x01f5 }
            r1.<init>(r11)     // Catch:{ Exception -> 0x01af, all -> 0x01f5 }
            r11.ad = r1     // Catch:{ Exception -> 0x01af, all -> 0x01f5 }
            com.adchina.android.ads.a.n r1 = r11.ad     // Catch:{ Exception -> 0x01af, all -> 0x01f5 }
            java.lang.StringBuffer r2 = r11.n     // Catch:{ Exception -> 0x01af, all -> 0x01f5 }
            r1.a(r2)     // Catch:{ Exception -> 0x01af, all -> 0x01f5 }
        L_0x008a:
            com.adchina.android.ads.l r1 = com.adchina.android.ads.l.EAdJPG     // Catch:{ Exception -> 0x01af, all -> 0x01f5 }
            com.adchina.android.ads.l r2 = r11.b     // Catch:{ Exception -> 0x01af, all -> 0x01f5 }
            if (r1 == r2) goto L_0x009c
            com.adchina.android.ads.l r1 = com.adchina.android.ads.l.EAdPNG     // Catch:{ Exception -> 0x01af, all -> 0x01f5 }
            com.adchina.android.ads.l r2 = r11.b     // Catch:{ Exception -> 0x01af, all -> 0x01f5 }
            if (r1 == r2) goto L_0x009c
            com.adchina.android.ads.l r1 = com.adchina.android.ads.l.EAdGIF     // Catch:{ Exception -> 0x01af, all -> 0x01f5 }
            com.adchina.android.ads.l r2 = r11.b     // Catch:{ Exception -> 0x01af, all -> 0x01f5 }
            if (r1 != r2) goto L_0x023b
        L_0x009c:
            com.adchina.android.ads.h r1 = r11.Z     // Catch:{ Exception -> 0x01af, all -> 0x01f5 }
            java.lang.String r2 = "loadLocalFullScreenImgList"
            r1.c(r2)     // Catch:{ Exception -> 0x01af, all -> 0x01f5 }
            r1 = 0
            r2 = 0
            android.content.Context r3 = r11.J     // Catch:{ Exception -> 0x0169, all -> 0x01e3 }
            java.lang.String r4 = "adchinaFSImgs.fc"
            java.io.File r3 = r3.getFileStreamPath(r4)     // Catch:{ Exception -> 0x0169, all -> 0x01e3 }
            long r3 = r3.length()     // Catch:{ Exception -> 0x0169, all -> 0x01e3 }
            int r3 = (int) r3     // Catch:{ Exception -> 0x0169, all -> 0x01e3 }
            char[] r3 = new char[r3]     // Catch:{ Exception -> 0x0169, all -> 0x01e3 }
            android.content.Context r4 = r11.J     // Catch:{ Exception -> 0x0169, all -> 0x01e3 }
            java.lang.String r5 = "adchinaFSImgs.fc"
            java.io.FileInputStream r1 = r4.openFileInput(r5)     // Catch:{ Exception -> 0x0169, all -> 0x01e3 }
            java.io.InputStreamReader r4 = new java.io.InputStreamReader     // Catch:{ Exception -> 0x04bd, all -> 0x04ae }
            r4.<init>(r1)     // Catch:{ Exception -> 0x04bd, all -> 0x04ae }
            r4.read(r3)     // Catch:{ Exception -> 0x04c3, all -> 0x04b4 }
            java.lang.String r2 = new java.lang.String     // Catch:{ Exception -> 0x04c3, all -> 0x04b4 }
            r2.<init>(r3)     // Catch:{ Exception -> 0x04c3, all -> 0x04b4 }
            java.util.HashMap r3 = r11.aa     // Catch:{ Exception -> 0x04c3, all -> 0x04b4 }
            r3.clear()     // Catch:{ Exception -> 0x04c3, all -> 0x04b4 }
            java.lang.String r3 = "\n"
            java.lang.String[] r2 = r2.split(r3)     // Catch:{ Exception -> 0x04c3, all -> 0x04b4 }
            int r3 = r2.length     // Catch:{ Exception -> 0x04c3, all -> 0x04b4 }
            r5 = 0
        L_0x00d6:
            if (r5 < r3) goto L_0x0138
            r4.close()     // Catch:{ IOException -> 0x0220 }
            if (r1 == 0) goto L_0x00e0
            r1.close()     // Catch:{ IOException -> 0x0220 }
        L_0x00e0:
            r11.o()     // Catch:{ IOException -> 0x0220 }
        L_0x00e3:
            com.adchina.android.ads.g r1 = com.adchina.android.ads.g.EGetImgMaterial     // Catch:{ Exception -> 0x01af, all -> 0x01f5 }
            r11.a(r1)     // Catch:{ Exception -> 0x01af, all -> 0x01f5 }
            java.lang.StringBuffer r1 = r11.h     // Catch:{ Exception -> 0x01af, all -> 0x01f5 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x01af, all -> 0x01f5 }
            java.util.LinkedList r2 = r11.Q     // Catch:{ Exception -> 0x01af, all -> 0x01f5 }
            com.adchina.android.ads.Utils.splitTrackUrl(r1, r2)     // Catch:{ Exception -> 0x01af, all -> 0x01f5 }
            java.lang.StringBuffer r1 = r11.g     // Catch:{ Exception -> 0x01af, all -> 0x01f5 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x01af, all -> 0x01f5 }
            java.util.LinkedList r2 = r11.R     // Catch:{ Exception -> 0x01af, all -> 0x01f5 }
            com.adchina.android.ads.Utils.splitTrackUrl(r1, r2)     // Catch:{ Exception -> 0x01af, all -> 0x01f5 }
            r1 = 1
            r11.ac = r1     // Catch:{ Exception -> 0x01af, all -> 0x01f5 }
            r1 = 0
            r11.ag = r1     // Catch:{ Exception -> 0x01af, all -> 0x01f5 }
            com.adchina.android.ads.t r1 = r11.W
            com.adchina.android.ads.t.a(r0)
            com.adchina.android.ads.h r0 = r11.Z
            java.lang.String r1 = "-- onReceiveFullScreenAd"
            r0.c(r1)
            goto L_0x0018
        L_0x0112:
            java.lang.String r1 = r11.i()     // Catch:{ Exception -> 0x04a8, all -> 0x049f }
            com.adchina.android.ads.h r2 = r11.Z     // Catch:{ Exception -> 0x04a8, all -> 0x049f }
            r3 = 2
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ Exception -> 0x04a8, all -> 0x049f }
            r4 = 0
            java.lang.String r5 = "AdserverUrl:"
            r3[r4] = r5     // Catch:{ Exception -> 0x04a8, all -> 0x049f }
            r4 = 1
            r3[r4] = r1     // Catch:{ Exception -> 0x04a8, all -> 0x049f }
            java.lang.String r3 = com.adchina.android.ads.Utils.concatString(r3)     // Catch:{ Exception -> 0x04a8, all -> 0x049f }
            r2.c(r3)     // Catch:{ Exception -> 0x04a8, all -> 0x049f }
            com.adchina.android.ads.t r2 = r11.W     // Catch:{ Exception -> 0x04a8, all -> 0x049f }
            java.lang.StringBuffer r3 = r11.N     // Catch:{ Exception -> 0x04a8, all -> 0x049f }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x04a8, all -> 0x049f }
            java.io.InputStream r0 = r2.a(r1, r3)     // Catch:{ Exception -> 0x04a8, all -> 0x049f }
            goto L_0x0071
        L_0x0138:
            r6 = r2[r5]     // Catch:{ Exception -> 0x04c3, all -> 0x04b4 }
            java.lang.String r7 = "|||"
            int r7 = r6.indexOf(r7)     // Catch:{ Exception -> 0x04c3, all -> 0x04b4 }
            r8 = 0
            java.lang.String r8 = r6.substring(r8, r7)     // Catch:{ Exception -> 0x04c3, all -> 0x04b4 }
            int r7 = r7 + 3
            java.lang.String r6 = r6.substring(r7)     // Catch:{ Exception -> 0x04c3, all -> 0x04b4 }
            java.io.File r7 = new java.io.File     // Catch:{ Exception -> 0x04c3, all -> 0x04b4 }
            r7.<init>(r6)     // Catch:{ Exception -> 0x04c3, all -> 0x04b4 }
            boolean r9 = r7.exists()     // Catch:{ Exception -> 0x04c3, all -> 0x04b4 }
            if (r9 == 0) goto L_0x0165
            java.lang.String r7 = r7.getName()     // Catch:{ Exception -> 0x04c3, all -> 0x04b4 }
            boolean r7 = com.adchina.android.ads.Utils.isCachedFileTimeout(r7)     // Catch:{ Exception -> 0x04c3, all -> 0x04b4 }
            if (r7 != 0) goto L_0x0165
            java.util.HashMap r7 = r11.aa     // Catch:{ Exception -> 0x04c3, all -> 0x04b4 }
            r7.put(r8, r6)     // Catch:{ Exception -> 0x04c3, all -> 0x04b4 }
        L_0x0165:
            int r5 = r5 + 1
            goto L_0x00d6
        L_0x0169:
            r3 = move-exception
            r10 = r3
            r3 = r1
            r1 = r10
        L_0x016d:
            com.adchina.android.ads.h r4 = r11.Z     // Catch:{ all -> 0x04ba }
            r5 = 2
            java.lang.Object[] r5 = new java.lang.Object[r5]     // Catch:{ all -> 0x04ba }
            r6 = 0
            java.lang.String r7 = "Exceptions in loadLocalFullScreenImgList , err = "
            r5[r6] = r7     // Catch:{ all -> 0x04ba }
            r6 = 1
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x04ba }
            r5[r6] = r1     // Catch:{ all -> 0x04ba }
            java.lang.String r1 = com.adchina.android.ads.Utils.concatString(r5)     // Catch:{ all -> 0x04ba }
            r4.c(r1)     // Catch:{ all -> 0x04ba }
            if (r2 == 0) goto L_0x018a
            r2.close()     // Catch:{ IOException -> 0x0194 }
        L_0x018a:
            if (r3 == 0) goto L_0x018f
            r3.close()     // Catch:{ IOException -> 0x0194 }
        L_0x018f:
            r11.o()     // Catch:{ IOException -> 0x0194 }
            goto L_0x00e3
        L_0x0194:
            r1 = move-exception
            com.adchina.android.ads.h r2 = r11.Z     // Catch:{ Exception -> 0x01af, all -> 0x01f5 }
            r3 = 2
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ Exception -> 0x01af, all -> 0x01f5 }
            r4 = 0
            java.lang.String r5 = "Exceptions in loadLocalBannerImgList , err = "
            r3[r4] = r5     // Catch:{ Exception -> 0x01af, all -> 0x01f5 }
            r4 = 1
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x01af, all -> 0x01f5 }
            r3[r4] = r1     // Catch:{ Exception -> 0x01af, all -> 0x01f5 }
            java.lang.String r1 = com.adchina.android.ads.Utils.concatString(r3)     // Catch:{ Exception -> 0x01af, all -> 0x01f5 }
            r2.c(r1)     // Catch:{ Exception -> 0x01af, all -> 0x01f5 }
            goto L_0x00e3
        L_0x01af:
            r1 = move-exception
            r10 = r1
            r1 = r0
            r0 = r10
        L_0x01b3:
            r2 = 2
            java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch:{ all -> 0x04a5 }
            r3 = 0
            java.lang.String r4 = "Exceptions in onReceiveFullScreenAd, err = "
            r2[r3] = r4     // Catch:{ all -> 0x04a5 }
            r3 = 1
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x04a5 }
            r2[r3] = r0     // Catch:{ all -> 0x04a5 }
            java.lang.String r0 = com.adchina.android.ads.Utils.concatString(r2)     // Catch:{ all -> 0x04a5 }
            r2 = 18
            r11.a(r2, r0)     // Catch:{ all -> 0x04a5 }
            com.adchina.android.ads.h r2 = r11.Z     // Catch:{ all -> 0x04a5 }
            r2.c(r0)     // Catch:{ all -> 0x04a5 }
            java.lang.String r2 = "AdChinaError"
            android.util.Log.e(r2, r0)     // Catch:{ all -> 0x04a5 }
            com.adchina.android.ads.t r0 = r11.W
            com.adchina.android.ads.t.a(r1)
            com.adchina.android.ads.h r0 = r11.Z
            java.lang.String r1 = "-- onReceiveFullScreenAd"
            r0.c(r1)
            goto L_0x0018
        L_0x01e3:
            r3 = move-exception
            r10 = r3
            r3 = r1
            r1 = r10
        L_0x01e7:
            if (r2 == 0) goto L_0x01ec
            r2.close()     // Catch:{ IOException -> 0x0206 }
        L_0x01ec:
            if (r3 == 0) goto L_0x01f1
            r3.close()     // Catch:{ IOException -> 0x0206 }
        L_0x01f1:
            r11.o()     // Catch:{ IOException -> 0x0206 }
        L_0x01f4:
            throw r1     // Catch:{ Exception -> 0x01af, all -> 0x01f5 }
        L_0x01f5:
            r1 = move-exception
            r10 = r1
            r1 = r0
            r0 = r10
        L_0x01f9:
            com.adchina.android.ads.t r2 = r11.W
            com.adchina.android.ads.t.a(r1)
            com.adchina.android.ads.h r1 = r11.Z
            java.lang.String r2 = "-- onReceiveFullScreenAd"
            r1.c(r2)
            throw r0
        L_0x0206:
            r2 = move-exception
            com.adchina.android.ads.h r3 = r11.Z     // Catch:{ Exception -> 0x01af, all -> 0x01f5 }
            r4 = 2
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ Exception -> 0x01af, all -> 0x01f5 }
            r5 = 0
            java.lang.String r6 = "Exceptions in loadLocalBannerImgList , err = "
            r4[r5] = r6     // Catch:{ Exception -> 0x01af, all -> 0x01f5 }
            r5 = 1
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x01af, all -> 0x01f5 }
            r4[r5] = r2     // Catch:{ Exception -> 0x01af, all -> 0x01f5 }
            java.lang.String r2 = com.adchina.android.ads.Utils.concatString(r4)     // Catch:{ Exception -> 0x01af, all -> 0x01f5 }
            r3.c(r2)     // Catch:{ Exception -> 0x01af, all -> 0x01f5 }
            goto L_0x01f4
        L_0x0220:
            r1 = move-exception
            com.adchina.android.ads.h r2 = r11.Z     // Catch:{ Exception -> 0x01af, all -> 0x01f5 }
            r3 = 2
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ Exception -> 0x01af, all -> 0x01f5 }
            r4 = 0
            java.lang.String r5 = "Exceptions in loadLocalBannerImgList , err = "
            r3[r4] = r5     // Catch:{ Exception -> 0x01af, all -> 0x01f5 }
            r4 = 1
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x01af, all -> 0x01f5 }
            r3[r4] = r1     // Catch:{ Exception -> 0x01af, all -> 0x01f5 }
            java.lang.String r1 = com.adchina.android.ads.Utils.concatString(r3)     // Catch:{ Exception -> 0x01af, all -> 0x01f5 }
            r2.c(r1)     // Catch:{ Exception -> 0x01af, all -> 0x01f5 }
            goto L_0x00e3
        L_0x023b:
            org.xmlpull.v1.XmlPullParserException r1 = new org.xmlpull.v1.XmlPullParserException     // Catch:{ Exception -> 0x01af, all -> 0x01f5 }
            java.lang.String r2 = "Invalidate mAdModel，可能因为不在广告定向区域，所以FullScreen广告位上现在没有可供展示的广告"
            r1.<init>(r2)     // Catch:{ Exception -> 0x01af, all -> 0x01f5 }
            throw r1     // Catch:{ Exception -> 0x01af, all -> 0x01f5 }
        L_0x0243:
            r11.n()
            goto L_0x0018
        L_0x0248:
            com.adchina.android.ads.h r0 = r11.Z
            java.lang.String r1 = "++ onSendFullScreenStartedTrack"
            r0.c(r1)
            r0 = 4
            java.lang.Object[] r0 = new java.lang.Object[r0]     // Catch:{ Exception -> 0x0297 }
            r1 = 0
            java.lang.StringBuffer r2 = r11.f     // Catch:{ Exception -> 0x0297 }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x0297 }
            r0[r1] = r2     // Catch:{ Exception -> 0x0297 }
            r1 = 1
            java.lang.String r2 = "210"
            r0[r1] = r2     // Catch:{ Exception -> 0x0297 }
            r1 = 2
            java.lang.String r2 = ",0,0,0"
            r0[r1] = r2     // Catch:{ Exception -> 0x0297 }
            r1 = 3
            java.lang.String r2 = r11.h()     // Catch:{ Exception -> 0x0297 }
            r0[r1] = r2     // Catch:{ Exception -> 0x0297 }
            java.lang.String r0 = com.adchina.android.ads.Utils.concatString(r0)     // Catch:{ Exception -> 0x0297 }
            com.adchina.android.ads.t r1 = r11.W     // Catch:{ Exception -> 0x0297 }
            com.adchina.android.ads.t.a(r0)     // Catch:{ Exception -> 0x0297 }
            com.adchina.android.ads.h r1 = r11.Z     // Catch:{ Exception -> 0x0297 }
            r2 = 2
            java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch:{ Exception -> 0x0297 }
            r3 = 0
            java.lang.String r4 = "send FullScreenStartedTrack to "
            r2[r3] = r4     // Catch:{ Exception -> 0x0297 }
            r3 = 1
            r2[r3] = r0     // Catch:{ Exception -> 0x0297 }
            java.lang.String r0 = com.adchina.android.ads.Utils.concatString(r2)     // Catch:{ Exception -> 0x0297 }
            r1.c(r0)     // Catch:{ Exception -> 0x0297 }
            com.adchina.android.ads.g r0 = com.adchina.android.ads.g.ESendFullScreenImpTrack
            r11.a(r0)
            com.adchina.android.ads.h r0 = r11.Z
            java.lang.String r1 = "-- onSendFullScreenStartedTrack"
            r0.c(r1)
            goto L_0x0018
        L_0x0297:
            r0 = move-exception
            r1 = 2
            java.lang.Object[] r1 = new java.lang.Object[r1]     // Catch:{ all -> 0x02c3 }
            r2 = 0
            java.lang.String r3 = "Exceptions in onSendFullScreenStartedTrack, err = "
            r1[r2] = r3     // Catch:{ all -> 0x02c3 }
            r2 = 1
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x02c3 }
            r1[r2] = r0     // Catch:{ all -> 0x02c3 }
            java.lang.String r0 = com.adchina.android.ads.Utils.concatString(r1)     // Catch:{ all -> 0x02c3 }
            com.adchina.android.ads.h r1 = r11.Z     // Catch:{ all -> 0x02c3 }
            r1.c(r0)     // Catch:{ all -> 0x02c3 }
            java.lang.String r1 = "AdChinaError"
            android.util.Log.e(r1, r0)     // Catch:{ all -> 0x02c3 }
            com.adchina.android.ads.g r0 = com.adchina.android.ads.g.ESendFullScreenImpTrack
            r11.a(r0)
            com.adchina.android.ads.h r0 = r11.Z
            java.lang.String r1 = "-- onSendFullScreenStartedTrack"
            r0.c(r1)
            goto L_0x0018
        L_0x02c3:
            r0 = move-exception
            com.adchina.android.ads.g r1 = com.adchina.android.ads.g.ESendFullScreenImpTrack
            r11.a(r1)
            com.adchina.android.ads.h r1 = r11.Z
            java.lang.String r2 = "-- onSendFullScreenStartedTrack"
            r1.c(r2)
            throw r0
        L_0x02d1:
            com.adchina.android.ads.h r0 = r11.Z
            java.lang.String r1 = "++ onSendFullScreenImpTrack"
            r0.c(r1)
            r0 = 4
            java.lang.Object[] r0 = new java.lang.Object[r0]     // Catch:{ Exception -> 0x0320 }
            r1 = 0
            java.lang.StringBuffer r2 = r11.f     // Catch:{ Exception -> 0x0320 }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x0320 }
            r0[r1] = r2     // Catch:{ Exception -> 0x0320 }
            r1 = 1
            java.lang.String r2 = "2"
            r0[r1] = r2     // Catch:{ Exception -> 0x0320 }
            r1 = 2
            java.lang.String r2 = ",0,0,0"
            r0[r1] = r2     // Catch:{ Exception -> 0x0320 }
            r1 = 3
            java.lang.String r2 = r11.h()     // Catch:{ Exception -> 0x0320 }
            r0[r1] = r2     // Catch:{ Exception -> 0x0320 }
            java.lang.String r0 = com.adchina.android.ads.Utils.concatString(r0)     // Catch:{ Exception -> 0x0320 }
            com.adchina.android.ads.t r1 = r11.W     // Catch:{ Exception -> 0x0320 }
            com.adchina.android.ads.t.a(r0)     // Catch:{ Exception -> 0x0320 }
            com.adchina.android.ads.h r1 = r11.Z     // Catch:{ Exception -> 0x0320 }
            r2 = 2
            java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch:{ Exception -> 0x0320 }
            r3 = 0
            java.lang.String r4 = "send FullScreenImpTrack to "
            r2[r3] = r4     // Catch:{ Exception -> 0x0320 }
            r3 = 1
            r2[r3] = r0     // Catch:{ Exception -> 0x0320 }
            java.lang.String r0 = com.adchina.android.ads.Utils.concatString(r2)     // Catch:{ Exception -> 0x0320 }
            r1.c(r0)     // Catch:{ Exception -> 0x0320 }
            com.adchina.android.ads.g r0 = com.adchina.android.ads.g.ESendFullScreenThdImpTrack
            r11.a(r0)
            com.adchina.android.ads.h r0 = r11.Z
            java.lang.String r1 = "-- onSendFullScreenImpTrack"
            r0.c(r1)
            goto L_0x0018
        L_0x0320:
            r0 = move-exception
            r1 = 2
            java.lang.Object[] r1 = new java.lang.Object[r1]     // Catch:{ all -> 0x034c }
            r2 = 0
            java.lang.String r3 = "Exceptions in onSendFullScreenImpTrack, err = "
            r1[r2] = r3     // Catch:{ all -> 0x034c }
            r2 = 1
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x034c }
            r1[r2] = r0     // Catch:{ all -> 0x034c }
            java.lang.String r0 = com.adchina.android.ads.Utils.concatString(r1)     // Catch:{ all -> 0x034c }
            com.adchina.android.ads.h r1 = r11.Z     // Catch:{ all -> 0x034c }
            r1.c(r0)     // Catch:{ all -> 0x034c }
            java.lang.String r1 = "AdChinaError"
            android.util.Log.e(r1, r0)     // Catch:{ all -> 0x034c }
            com.adchina.android.ads.g r0 = com.adchina.android.ads.g.ESendFullScreenThdImpTrack
            r11.a(r0)
            com.adchina.android.ads.h r0 = r11.Z
            java.lang.String r1 = "-- onSendFullScreenImpTrack"
            r0.c(r1)
            goto L_0x0018
        L_0x034c:
            r0 = move-exception
            com.adchina.android.ads.g r1 = com.adchina.android.ads.g.ESendFullScreenThdImpTrack
            r11.a(r1)
            com.adchina.android.ads.h r1 = r11.Z
            java.lang.String r2 = "-- onSendFullScreenImpTrack"
            r1.c(r2)
            throw r0
        L_0x035a:
            com.adchina.android.ads.g r0 = com.adchina.android.ads.g.EIdle
            r11.a(r0)
            java.util.LinkedList r0 = r11.R
            int r0 = r0.size()
            com.adchina.android.ads.h r1 = r11.Z
            r2 = 2
            java.lang.Object[] r2 = new java.lang.Object[r2]
            r3 = 0
            java.lang.String r4 = "++ onSendFullScreenThdImpTrack, Size of list is "
            r2[r3] = r4
            r3 = 1
            java.lang.Integer r4 = java.lang.Integer.valueOf(r0)
            r2[r3] = r4
            java.lang.String r2 = com.adchina.android.ads.Utils.concatString(r2)
            r1.c(r2)
            if (r0 <= 0) goto L_0x03ac
            java.util.LinkedList r1 = r11.R     // Catch:{ Exception -> 0x03b5 }
            r2 = 1
            int r0 = r0 - r2
            java.lang.Object r0 = r1.get(r0)     // Catch:{ Exception -> 0x03b5 }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ Exception -> 0x03b5 }
            java.util.LinkedList r1 = r11.R     // Catch:{ Exception -> 0x03b5 }
            r1.removeLast()     // Catch:{ Exception -> 0x03b5 }
            com.adchina.android.ads.t r1 = r11.W     // Catch:{ Exception -> 0x03b5 }
            com.adchina.android.ads.t.a(r0)     // Catch:{ Exception -> 0x03b5 }
            com.adchina.android.ads.h r1 = r11.Z     // Catch:{ Exception -> 0x03b5 }
            r2 = 2
            java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch:{ Exception -> 0x03b5 }
            r3 = 0
            java.lang.String r4 = "send FullScreenThdImpTrack to "
            r2[r3] = r4     // Catch:{ Exception -> 0x03b5 }
            r3 = 1
            r2[r3] = r0     // Catch:{ Exception -> 0x03b5 }
            java.lang.String r0 = com.adchina.android.ads.Utils.concatString(r2)     // Catch:{ Exception -> 0x03b5 }
            r1.c(r0)     // Catch:{ Exception -> 0x03b5 }
            com.adchina.android.ads.g r0 = com.adchina.android.ads.g.ESendFullScreenThdImpTrack
            r11.a(r0)
        L_0x03ac:
            com.adchina.android.ads.h r0 = r11.Z
            java.lang.String r1 = "-- onSendFullScreenThdImpTrack"
            r0.c(r1)
            goto L_0x0018
        L_0x03b5:
            r0 = move-exception
            r1 = 2
            java.lang.Object[] r1 = new java.lang.Object[r1]     // Catch:{ all -> 0x03d9 }
            r2 = 0
            java.lang.String r3 = "Failed to onSendFullScreenThdImpTrack, err = "
            r1[r2] = r3     // Catch:{ all -> 0x03d9 }
            r2 = 1
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x03d9 }
            r1[r2] = r0     // Catch:{ all -> 0x03d9 }
            java.lang.String r0 = com.adchina.android.ads.Utils.concatString(r1)     // Catch:{ all -> 0x03d9 }
            com.adchina.android.ads.h r1 = r11.Z     // Catch:{ all -> 0x03d9 }
            r1.c(r0)     // Catch:{ all -> 0x03d9 }
            java.lang.String r1 = "AdChinaError"
            android.util.Log.e(r1, r0)     // Catch:{ all -> 0x03d9 }
            com.adchina.android.ads.g r0 = com.adchina.android.ads.g.ESendFullScreenThdImpTrack
            r11.a(r0)
            goto L_0x03ac
        L_0x03d9:
            r0 = move-exception
            com.adchina.android.ads.g r1 = com.adchina.android.ads.g.ESendFullScreenThdImpTrack
            r11.a(r1)
            throw r0
        L_0x03e0:
            com.adchina.android.ads.h r0 = r11.Z
            java.lang.String r1 = "++ onSendFullScreenEndedTrack"
            r0.c(r1)
            com.adchina.android.ads.g r0 = com.adchina.android.ads.g.EIdle
            r11.a(r0)
            r0 = 4
            java.lang.Object[] r0 = new java.lang.Object[r0]     // Catch:{ Exception -> 0x043e }
            r1 = 0
            java.lang.StringBuffer r2 = r11.f     // Catch:{ Exception -> 0x043e }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x043e }
            r0[r1] = r2     // Catch:{ Exception -> 0x043e }
            r1 = 1
            java.lang.String r2 = "212"
            r0[r1] = r2     // Catch:{ Exception -> 0x043e }
            r1 = 2
            java.lang.String r2 = ",0,0,0"
            r0[r1] = r2     // Catch:{ Exception -> 0x043e }
            r1 = 3
            java.lang.String r2 = r11.h()     // Catch:{ Exception -> 0x043e }
            r0[r1] = r2     // Catch:{ Exception -> 0x043e }
            java.lang.String r0 = com.adchina.android.ads.Utils.concatString(r0)     // Catch:{ Exception -> 0x043e }
            com.adchina.android.ads.t r1 = r11.W     // Catch:{ Exception -> 0x043e }
            com.adchina.android.ads.t.a(r0)     // Catch:{ Exception -> 0x043e }
            com.adchina.android.ads.h r1 = r11.Z     // Catch:{ Exception -> 0x043e }
            r2 = 2
            java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch:{ Exception -> 0x043e }
            r3 = 0
            java.lang.String r4 = "send FullScreenEndedTrack to "
            r2[r3] = r4     // Catch:{ Exception -> 0x043e }
            r3 = 1
            r2[r3] = r0     // Catch:{ Exception -> 0x043e }
            java.lang.String r0 = com.adchina.android.ads.Utils.concatString(r2)     // Catch:{ Exception -> 0x043e }
            r1.c(r0)     // Catch:{ Exception -> 0x043e }
            com.adchina.android.ads.h r0 = r11.Z
            java.lang.String r1 = "-- onSendFullScreenEndedTrack"
            r0.c(r1)
        L_0x042d:
            com.adchina.android.ads.a.n r0 = r11.ad
            if (r0 == 0) goto L_0x0436
            com.adchina.android.ads.a.n r0 = r11.ad
            r0.c()
        L_0x0436:
            r0 = 3
            java.lang.String r1 = "RefreshAd"
            r11.a(r0, r1)
            goto L_0x0018
        L_0x043e:
            r0 = move-exception
            r1 = 2
            java.lang.Object[] r1 = new java.lang.Object[r1]     // Catch:{ all -> 0x0464 }
            r2 = 0
            java.lang.String r3 = "Exceptions in onSendFullScreenEndedTrack, err = "
            r1[r2] = r3     // Catch:{ all -> 0x0464 }
            r2 = 1
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0464 }
            r1[r2] = r0     // Catch:{ all -> 0x0464 }
            java.lang.String r0 = com.adchina.android.ads.Utils.concatString(r1)     // Catch:{ all -> 0x0464 }
            com.adchina.android.ads.h r1 = r11.Z     // Catch:{ all -> 0x0464 }
            r1.c(r0)     // Catch:{ all -> 0x0464 }
            java.lang.String r1 = "AdChinaError"
            android.util.Log.e(r1, r0)     // Catch:{ all -> 0x0464 }
            com.adchina.android.ads.h r0 = r11.Z
            java.lang.String r1 = "-- onSendFullScreenEndedTrack"
            r0.c(r1)
            goto L_0x042d
        L_0x0464:
            r0 = move-exception
            com.adchina.android.ads.h r1 = r11.Z
            java.lang.String r2 = "-- onSendFullScreenEndedTrack"
            r1.c(r2)
            throw r0
        L_0x046d:
            boolean r0 = r11.ah
            if (r0 != 0) goto L_0x0474
            r11.l()
        L_0x0474:
            com.adchina.android.ads.h r0 = r11.Z
            java.lang.String r1 = "++ onRefreshAd"
            r0.c(r1)
            int r0 = r11.ac     // Catch:{ InterruptedException -> 0x0491, all -> 0x0498 }
            int r0 = r0 * 1000
            long r0 = (long) r0     // Catch:{ InterruptedException -> 0x0491, all -> 0x0498 }
            java.lang.Thread.sleep(r0)     // Catch:{ InterruptedException -> 0x0491, all -> 0x0498 }
            com.adchina.android.ads.g r0 = com.adchina.android.ads.g.EReceiveAd
            r11.a(r0)
        L_0x0488:
            com.adchina.android.ads.h r0 = r11.Z
            java.lang.String r1 = "-- onRefreshAd"
            r0.c(r1)
            goto L_0x0018
        L_0x0491:
            r0 = move-exception
            com.adchina.android.ads.g r0 = com.adchina.android.ads.g.EReceiveAd
            r11.a(r0)
            goto L_0x0488
        L_0x0498:
            r0 = move-exception
            com.adchina.android.ads.g r1 = com.adchina.android.ads.g.EReceiveAd
            r11.a(r1)
            throw r0
        L_0x049f:
            r1 = move-exception
            r10 = r1
            r1 = r0
            r0 = r10
            goto L_0x01f9
        L_0x04a5:
            r0 = move-exception
            goto L_0x01f9
        L_0x04a8:
            r1 = move-exception
            r10 = r1
            r1 = r0
            r0 = r10
            goto L_0x01b3
        L_0x04ae:
            r3 = move-exception
            r10 = r3
            r3 = r1
            r1 = r10
            goto L_0x01e7
        L_0x04b4:
            r2 = move-exception
            r3 = r1
            r1 = r2
            r2 = r4
            goto L_0x01e7
        L_0x04ba:
            r1 = move-exception
            goto L_0x01e7
        L_0x04bd:
            r3 = move-exception
            r10 = r3
            r3 = r1
            r1 = r10
            goto L_0x016d
        L_0x04c3:
            r2 = move-exception
            r3 = r1
            r1 = r2
            r2 = r4
            goto L_0x016d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adchina.android.ads.a.e.a(com.adchina.android.ads.a.d):void");
    }

    public final void a(GifImageView gifImageView, m mVar) {
        this.Z.c("++ showFullScreenAdImg");
        String str = (String) this.aa.get(this.ad.a().toString());
        try {
            if (l.EAdJPG == this.b) {
                a.a(new f(this, str), new g(this, gifImageView, mVar));
            } else if (l.EAdPNG == this.b) {
                a.a(new h(this, str), new i(this, gifImageView, mVar));
            } else if (l.EAdGIF == this.b) {
                a.a(new j(this, str), new k(this, gifImageView, mVar));
            } else {
                a(18, "Invalidate mAdModel");
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            Utils.closeStream((InputStream) null);
            Utils.closeStream((OutputStream) null);
        }
        this.Z.c("-- showFullScreenAdImg");
    }

    /* JADX WARNING: Removed duplicated region for block: B:23:0x0091  */
    /* JADX WARNING: Removed duplicated region for block: B:34:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(java.lang.Object r8) {
        /*
            r7 = this;
            r6 = 2
            r5 = 1
            r4 = 0
            com.adchina.android.ads.h r0 = r7.Z
            if (r0 == 0) goto L_0x001c
            com.adchina.android.ads.h r0 = r7.Z
            java.lang.Object[] r1 = new java.lang.Object[r6]
            java.lang.String r2 = "++ showFullScreenAd arg ="
            r1[r4] = r2
            if (r8 != 0) goto L_0x00a7
            java.lang.String r2 = ""
        L_0x0013:
            r1[r5] = r2
            java.lang.String r1 = com.adchina.android.ads.Utils.concatString(r1)
            r0.c(r1)
        L_0x001c:
            java.util.UUID r0 = java.util.UUID.randomUUID()
            java.lang.String r1 = r0.toString()
            if (r8 == 0) goto L_0x002b
            java.util.HashMap r0 = r7.af
            r0.put(r1, r8)
        L_0x002b:
            com.adchina.android.ads.a.n r0 = r7.ad     // Catch:{ Exception -> 0x00b0 }
            if (r0 == 0) goto L_0x00aa
            com.adchina.android.ads.a.n r0 = r7.ad     // Catch:{ Exception -> 0x00b0 }
            boolean r0 = r0.b()     // Catch:{ Exception -> 0x00b0 }
            if (r0 != 0) goto L_0x00aa
            com.adchina.android.ads.a.n r0 = r7.ad     // Catch:{ Exception -> 0x00b0 }
            java.lang.StringBuffer r0 = r0.a()     // Catch:{ Exception -> 0x00b0 }
            int r0 = r0.length()     // Catch:{ Exception -> 0x00b0 }
            if (r0 <= 0) goto L_0x00aa
            java.util.HashMap r0 = r7.aa     // Catch:{ Exception -> 0x00b0 }
            com.adchina.android.ads.a.n r2 = r7.ad     // Catch:{ Exception -> 0x00b0 }
            java.lang.StringBuffer r2 = r2.a()     // Catch:{ Exception -> 0x00b0 }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x00b0 }
            boolean r0 = r0.containsKey(r2)     // Catch:{ Exception -> 0x00b0 }
            if (r0 == 0) goto L_0x00aa
            java.io.File r2 = new java.io.File     // Catch:{ Exception -> 0x00b0 }
            java.util.HashMap r0 = r7.aa     // Catch:{ Exception -> 0x00b0 }
            com.adchina.android.ads.a.n r3 = r7.ad     // Catch:{ Exception -> 0x00b0 }
            java.lang.StringBuffer r3 = r3.a()     // Catch:{ Exception -> 0x00b0 }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x00b0 }
            java.lang.Object r0 = r0.get(r3)     // Catch:{ Exception -> 0x00b0 }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ Exception -> 0x00b0 }
            r2.<init>(r0)     // Catch:{ Exception -> 0x00b0 }
            boolean r0 = r2.exists()     // Catch:{ Exception -> 0x00b0 }
            if (r0 == 0) goto L_0x00aa
            android.content.Intent r0 = new android.content.Intent     // Catch:{ Exception -> 0x00b0 }
            r0.<init>()     // Catch:{ Exception -> 0x00b0 }
            r2 = 268435456(0x10000000, float:2.5243549E-29)
            r0.addFlags(r2)     // Catch:{ Exception -> 0x00b0 }
            java.lang.String r2 = "ArgName"
            r0.putExtra(r2, r1)     // Catch:{ Exception -> 0x00b0 }
            android.content.Context r1 = r7.J     // Catch:{ Exception -> 0x00b0 }
            java.lang.Class<com.adchina.android.ads.views.FullScreenAdActivity> r2 = com.adchina.android.ads.views.FullScreenAdActivity.class
            r0.setClass(r1, r2)     // Catch:{ Exception -> 0x00b0 }
            android.content.Context r1 = r7.J     // Catch:{ Exception -> 0x00b0 }
            r1.startActivity(r0)     // Catch:{ Exception -> 0x00b0 }
        L_0x008d:
            com.adchina.android.ads.h r0 = r7.Z
            if (r0 == 0) goto L_0x00a6
            com.adchina.android.ads.h r0 = r7.Z
            java.lang.Object[] r1 = new java.lang.Object[r6]
            java.lang.String r2 = "-- showFullScreenAd arg ="
            r1[r4] = r2
            if (r8 != 0) goto L_0x00d1
            java.lang.String r2 = ""
        L_0x009d:
            r1[r5] = r2
            java.lang.String r1 = com.adchina.android.ads.Utils.concatString(r1)
            r0.c(r1)
        L_0x00a6:
            return
        L_0x00a7:
            r2 = r8
            goto L_0x0013
        L_0x00aa:
            android.content.Context r0 = r7.J     // Catch:{ Exception -> 0x00b0 }
            r7.a(r0, r1)     // Catch:{ Exception -> 0x00b0 }
            goto L_0x008d
        L_0x00b0:
            r0 = move-exception
            java.lang.Object[] r1 = new java.lang.Object[r6]
            java.lang.String r2 = "Exceptions in onReceiveFullScreenAd, err = "
            r1[r4] = r2
            java.lang.String r0 = r0.toString()
            r1[r5] = r0
            java.lang.String r0 = com.adchina.android.ads.Utils.concatString(r1)
            r1 = 18
            r7.a(r1, r0)
            com.adchina.android.ads.h r1 = r7.Z
            r1.c(r0)
            java.lang.String r1 = "AdChinaError"
            android.util.Log.e(r1, r0)
            goto L_0x008d
        L_0x00d1:
            r2 = r8
            goto L_0x009d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adchina.android.ads.a.e.a(java.lang.Object):void");
    }

    public final void a(boolean z) {
        this.ah = z;
        k();
    }

    public final void b() {
        this.Z.c("++ onFullScreenAdEnded");
        a(g.ESendFullScreenEndedTrack);
        this.Z.c("-- onFullScreenAdEnded");
    }

    /* access modifiers changed from: protected */
    public final void b(String str) {
        a(str, "adchinaFSFC.fc");
    }

    /* access modifiers changed from: protected */
    public final void c() {
        this.N.setLength(0);
        this.N.append(AdManager.getFullScreenAdspaceId());
    }

    /* access modifiers changed from: protected */
    public final String d() {
        return c("adchinaFSFC.fc");
    }

    public final void e() {
        this.Z.c("++ onClickFullScreenAd");
        this.Z.c("++ open " + this.e + " with browser");
        AdEngine.getAdEngine().openBrowser(this.e.toString());
        this.Z.c("-- open browser");
        a(f.ESendFullScreenClkThdTrack);
        this.Z.c("-- onClickFullScreenAd");
    }

    public final void f() {
        this.Z.c("++ onClickCloseButton");
        a(f.ESendFullScreenCloseTrack);
        this.Z.c("-- onClickCloseButton");
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public final boolean handleMessage(Message message) {
        switch (message.what) {
            case 3:
                a(g.ERefreshAd);
                break;
            case 17:
                if (X != null) {
                    try {
                        X.onDisplayFullScreenAd();
                    } catch (Exception e) {
                    }
                }
                if (!this.ag) {
                    a(g.ESendFullScreenStartedTrack);
                }
                this.ag = true;
                break;
            case 18:
                if (X != null) {
                    try {
                        X.onFailedToReceiveFullScreenAd();
                    } catch (Exception e2) {
                    }
                }
                this.O = false;
                this.ac = 30;
                a(g.ERefreshAd);
                break;
            case 19:
                if (X != null) {
                    try {
                        X.onReceiveFullScreenAd();
                        break;
                    } catch (Exception e3) {
                        break;
                    }
                }
                break;
        }
        if (message != null) {
            this.Y.removeMessages(message.what);
        }
        return true;
    }

    public final int m() {
        try {
            return Integer.valueOf(this.j.toString()).intValue();
        } catch (Exception e) {
            return 0;
        }
    }
}
