package com.adchina.android.ads;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.webkit.WebView;
import com.adchina.android.ads.a.a;
import com.adchina.android.ads.a.b;
import com.adchina.android.ads.a.e;
import com.adchina.android.ads.a.o;
import com.adchina.android.ads.a.q;
import com.adchina.android.ads.views.AdView;
import com.adchina.android.ads.views.ShrinkFSAdView;

public class AdEngine {
    private static AdEngine j;
    private boolean a = false;
    private boolean b = false;
    private boolean c = false;
    private Context d;
    private a e;
    private o f;
    private q g;
    private e h;
    private boolean i = false;

    private AdEngine(Context context) {
        this.d = context;
        AdManager.setResolution(context.getResources().getDisplayMetrics());
        AdManager.setPhoneUA(new WebView(context).getSettings().getUserAgentString());
        j = this;
    }

    public static AdEngine getAdEngine() {
        return j;
    }

    public static AdEngine initAdEngine(Context context) {
        if (j == null) {
            j = new AdEngine(context);
            a.a(new v(context), new w(context));
        }
        return j;
    }

    public static void setAdListener(AdListener adListener) {
        b.a(adListener);
    }

    public void addBannerAdView(AdView adView) {
        getAdViewController().a(adView);
    }

    public void addBannerAdView(AdView adView, boolean z) {
        getAdViewController().a(adView);
        if (z) {
            startBannerAd();
        }
    }

    public a getAdViewController() {
        if (this.e != null) {
            return this.e;
        }
        synchronized (j) {
            if (this.e == null) {
                this.e = new a(this.d);
            }
        }
        return this.e;
    }

    public e getFullScreenAdController() {
        if (this.h != null) {
            return this.h;
        }
        synchronized (j) {
            if (this.h == null) {
                this.h = e.a(this.d);
            }
        }
        return this.h;
    }

    public o getShrinkFSAdController() {
        if (this.f != null) {
            return this.f;
        }
        synchronized (j) {
            if (this.f == null) {
                this.f = new o(this.d);
            }
        }
        return this.f;
    }

    public q getVideoAdController() {
        if (this.g != null) {
            return this.g;
        }
        synchronized (j) {
            if (this.g == null) {
                this.g = q.a(this.d);
            }
        }
        return this.g;
    }

    public void makeCall(String str) {
        try {
            Intent intent = new Intent("android.intent.action.DIAL", Uri.parse("tel:" + str));
            intent.addFlags(268435456);
            this.d.startActivity(intent);
        } catch (Exception e2) {
            Log.e("AdChinaError", Utils.concatString("Failed to makeCall, err = ", e2));
        }
    }

    /* access modifiers changed from: protected */
    public void onClickShrinkFSAd() {
        getShrinkFSAdController().a();
    }

    public void openBrowser(String str) {
        if (str.length() > 0 && this.d != null) {
            Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(str));
            intent.addFlags(268435456);
            this.d.startActivity(intent);
        }
    }

    public void playVideo(Object obj) {
        getVideoAdController().a(obj);
    }

    public void removeBannerAdView(AdView adView) {
        getAdViewController().b(adView);
    }

    public void sendSms(String str, String str2) {
        try {
            Intent intent = new Intent("android.intent.action.SENDTO", Uri.parse("smsto:" + str));
            intent.addFlags(268435456);
            intent.putExtra("sms_body", str2);
            this.d.startActivity(intent);
        } catch (Exception e2) {
            Log.e("AdChinaError", Utils.concatString("Failed to sendSms, err = ", e2));
        }
    }

    public void setDefaultUrl(String str) {
        getAdViewController().a(str);
    }

    public void setFullScreenFinishEvent(AdFullScreenFinishListener adFullScreenFinishListener) {
        getFullScreenAdController().a(adFullScreenFinishListener);
    }

    public void setShrinkFSAdView(ShrinkFSAdView shrinkFSAdView) {
        getShrinkFSAdController().a(shrinkFSAdView);
    }

    public void setVideoFinishEvent(AdVideoFinishListener adVideoFinishListener) {
        getVideoAdController().a(adVideoFinishListener);
    }

    public void showFullScreenAd(Object obj) {
        getFullScreenAdController().a(obj);
    }

    public void startBannerAd() {
        getAdViewController().k();
    }

    public void startFullScreenAd() {
        startFullScreenAd(false);
    }

    public void startFullScreenAd(boolean z) {
        getFullScreenAdController().a(z);
    }

    public void startShrinkFSAd() {
        getShrinkFSAdController().a(getAdViewController());
        getShrinkFSAdController().k();
    }

    public void startVideoAd() {
        startVideoAd(false);
    }

    public void startVideoAd(boolean z) {
        getVideoAdController().a(z);
    }

    public void stopBannerAd() {
        getAdViewController().l();
        this.e = null;
    }

    public void stopFullScreenAd() {
        getFullScreenAdController().l();
        this.h = null;
    }

    public void stopShrinkFSAd() {
        getShrinkFSAdController().l();
        this.f = null;
    }

    public void stopVideoAd() {
        getVideoAdController().l();
        this.g = null;
    }
}
