package com.adchina.android.ads.a;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Message;
import android.util.Log;
import com.adchina.android.ads.AdEngine;
import com.adchina.android.ads.AdManager;
import com.adchina.android.ads.Utils;
import com.adchina.android.ads.f;
import com.adchina.android.ads.g;
import com.adchina.android.ads.k;
import com.adchina.android.ads.t;
import com.adchina.android.ads.views.ShrinkFSAdView;
import com.safetest.pvz.R;
import java.io.File;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Timer;

public final class o extends b {
    private static /* synthetic */ int[] ah;
    private static /* synthetic */ int[] ai;
    private LinkedList aa = new LinkedList();
    private LinkedList ab = new LinkedList();
    private HashMap ac = new HashMap();
    private ShrinkFSAdView ad;
    /* access modifiers changed from: private */
    public a ae;
    /* access modifiers changed from: private */
    public String af;
    private boolean ag = false;

    public o(Context context) {
        super(context);
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x0059 A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00a7 A[Catch:{ all -> 0x0129 }] */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x00ac A[SYNTHETIC, Splitter:B:40:0x00ac] */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x00d1 A[SYNTHETIC, Splitter:B:50:0x00d1] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.io.InputStream a(java.lang.String r11) {
        /*
            r10 = this;
            r8 = 0
            r7 = 1
            r6 = 0
            com.adchina.android.ads.h r0 = r10.Z     // Catch:{ Exception -> 0x010c, all -> 0x00eb }
            r1 = 2
            java.lang.Object[] r1 = new java.lang.Object[r1]     // Catch:{ Exception -> 0x010c, all -> 0x00eb }
            r2 = 0
            java.lang.String r3 = "++ start to download Img file"
            r1[r2] = r3     // Catch:{ Exception -> 0x010c, all -> 0x00eb }
            r2 = 1
            r1[r2] = r11     // Catch:{ Exception -> 0x010c, all -> 0x00eb }
            java.lang.String r1 = com.adchina.android.ads.Utils.concatString(r1)     // Catch:{ Exception -> 0x010c, all -> 0x00eb }
            r0.c(r1)     // Catch:{ Exception -> 0x010c, all -> 0x00eb }
            java.io.File r0 = new java.io.File     // Catch:{ Exception -> 0x010c, all -> 0x00eb }
            java.lang.String r1 = com.adchina.android.ads.k.c     // Catch:{ Exception -> 0x010c, all -> 0x00eb }
            r0.<init>(r1)     // Catch:{ Exception -> 0x010c, all -> 0x00eb }
            boolean r1 = r0.exists()     // Catch:{ Exception -> 0x010c, all -> 0x00eb }
            if (r1 != 0) goto L_0x0027
            r0.mkdirs()     // Catch:{ Exception -> 0x010c, all -> 0x00eb }
        L_0x0027:
            com.adchina.android.ads.t r1 = r10.W     // Catch:{ Exception -> 0x010c, all -> 0x00eb }
            java.io.InputStream r1 = com.adchina.android.ads.t.b(r11)     // Catch:{ Exception -> 0x010c, all -> 0x00eb }
            if (r1 != 0) goto L_0x0062
            java.lang.Exception r0 = new java.lang.Exception     // Catch:{ Exception -> 0x0037, all -> 0x00f8 }
            java.lang.String r2 = "image address is not exists"
            r0.<init>(r2)     // Catch:{ Exception -> 0x0037, all -> 0x00f8 }
            throw r0     // Catch:{ Exception -> 0x0037, all -> 0x00f8 }
        L_0x0037:
            r0 = move-exception
            r2 = r1
            r1 = r6
        L_0x003a:
            r3 = 2
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ all -> 0x010a }
            r4 = 0
            java.lang.String r5 = "Failed to download Img file, err = "
            r3[r4] = r5     // Catch:{ all -> 0x010a }
            r4 = 1
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x010a }
            r3[r4] = r0     // Catch:{ all -> 0x010a }
            java.lang.String r0 = com.adchina.android.ads.Utils.concatString(r3)     // Catch:{ all -> 0x010a }
            com.adchina.android.ads.h r3 = r10.Z     // Catch:{ all -> 0x010a }
            r3.c(r0)     // Catch:{ all -> 0x010a }
            java.lang.String r3 = "AdChinaError"
            android.util.Log.e(r3, r0)     // Catch:{ all -> 0x010a }
            if (r1 != 0) goto L_0x005b
            if (r2 != 0) goto L_0x013a
        L_0x005b:
            com.adchina.android.ads.t r0 = r10.W
            java.io.InputStream r0 = com.adchina.android.ads.t.b(r11)
        L_0x0061:
            return r0
        L_0x0062:
            java.io.File r2 = new java.io.File     // Catch:{ Exception -> 0x0037, all -> 0x00f8 }
            r3 = 2
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ Exception -> 0x0037, all -> 0x00f8 }
            r4 = 0
            java.lang.String r5 = "yyyyMMddHHmmss"
            java.lang.String r5 = com.adchina.android.ads.Utils.getNowTime(r5)     // Catch:{ Exception -> 0x0037, all -> 0x00f8 }
            r3[r4] = r5     // Catch:{ Exception -> 0x0037, all -> 0x00f8 }
            r4 = 1
            java.lang.String r5 = "fsImg.tmp"
            r3[r4] = r5     // Catch:{ Exception -> 0x0037, all -> 0x00f8 }
            java.lang.String r3 = com.adchina.android.ads.Utils.concatString(r3)     // Catch:{ Exception -> 0x0037, all -> 0x00f8 }
            r2.<init>(r0, r3)     // Catch:{ Exception -> 0x0037, all -> 0x00f8 }
            boolean r0 = r2.exists()     // Catch:{ Exception -> 0x0037, all -> 0x00f8 }
            if (r0 == 0) goto L_0x0085
            r2.delete()     // Catch:{ Exception -> 0x0037, all -> 0x00f8 }
        L_0x0085:
            java.io.FileOutputStream r0 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x009b, all -> 0x00cc }
            r0.<init>(r2)     // Catch:{ Exception -> 0x009b, all -> 0x00cc }
            r3 = 16384(0x4000, float:2.2959E-41)
            byte[] r3 = new byte[r3]     // Catch:{ Exception -> 0x012d, all -> 0x011d }
            r4 = r6
        L_0x008f:
            int r5 = r1.read(r3)     // Catch:{ Exception -> 0x0134, all -> 0x0123 }
            if (r5 <= 0) goto L_0x00df
            r6 = 0
            r0.write(r3, r6, r5)     // Catch:{ Exception -> 0x0134, all -> 0x0123 }
            r4 = r7
            goto L_0x008f
        L_0x009b:
            r0 = move-exception
            r3 = r8
            r4 = r6
        L_0x009e:
            r0.printStackTrace()     // Catch:{ all -> 0x0129 }
            boolean r0 = r2.exists()     // Catch:{ all -> 0x0129 }
            if (r0 == 0) goto L_0x00aa
            r2.delete()     // Catch:{ all -> 0x0129 }
        L_0x00aa:
            if (r4 == 0) goto L_0x00b1
            com.adchina.android.ads.t r0 = r10.W     // Catch:{ Exception -> 0x0111, all -> 0x00fc }
            com.adchina.android.ads.t.a(r1)     // Catch:{ Exception -> 0x0111, all -> 0x00fc }
        L_0x00b1:
            com.adchina.android.ads.Utils.closeStream(r3)     // Catch:{ Exception -> 0x0111, all -> 0x00fc }
            r0 = r4
        L_0x00b5:
            java.util.HashMap r3 = r10.ac     // Catch:{ Exception -> 0x0116, all -> 0x0104 }
            java.lang.String r4 = r2.getAbsolutePath()     // Catch:{ Exception -> 0x0116, all -> 0x0104 }
            r3.put(r11, r4)     // Catch:{ Exception -> 0x0116, all -> 0x0104 }
            r10.e()     // Catch:{ Exception -> 0x0116, all -> 0x0104 }
            java.io.FileInputStream r3 = new java.io.FileInputStream     // Catch:{ Exception -> 0x0116, all -> 0x0104 }
            java.lang.String r2 = r2.getAbsolutePath()     // Catch:{ Exception -> 0x0116, all -> 0x0104 }
            r3.<init>(r2)     // Catch:{ Exception -> 0x0116, all -> 0x0104 }
            r0 = r3
            goto L_0x0061
        L_0x00cc:
            r0 = move-exception
            r2 = r8
            r3 = r6
        L_0x00cf:
            if (r3 == 0) goto L_0x00d6
            com.adchina.android.ads.t r4 = r10.W     // Catch:{ Exception -> 0x00da, all -> 0x0100 }
            com.adchina.android.ads.t.a(r1)     // Catch:{ Exception -> 0x00da, all -> 0x0100 }
        L_0x00d6:
            com.adchina.android.ads.Utils.closeStream(r2)     // Catch:{ Exception -> 0x00da, all -> 0x0100 }
            throw r0     // Catch:{ Exception -> 0x00da, all -> 0x0100 }
        L_0x00da:
            r0 = move-exception
            r2 = r1
            r1 = r3
            goto L_0x003a
        L_0x00df:
            if (r4 == 0) goto L_0x00e6
            com.adchina.android.ads.t r3 = r10.W     // Catch:{ Exception -> 0x0111, all -> 0x00fc }
            com.adchina.android.ads.t.a(r1)     // Catch:{ Exception -> 0x0111, all -> 0x00fc }
        L_0x00e6:
            com.adchina.android.ads.Utils.closeStream(r0)     // Catch:{ Exception -> 0x0111, all -> 0x00fc }
            r0 = r4
            goto L_0x00b5
        L_0x00eb:
            r0 = move-exception
            r1 = r6
            r2 = r8
        L_0x00ee:
            if (r1 != 0) goto L_0x00f2
            if (r2 != 0) goto L_0x00f7
        L_0x00f2:
            com.adchina.android.ads.t r1 = r10.W
            com.adchina.android.ads.t.b(r11)
        L_0x00f7:
            throw r0
        L_0x00f8:
            r0 = move-exception
            r2 = r1
            r1 = r6
            goto L_0x00ee
        L_0x00fc:
            r0 = move-exception
            r2 = r1
            r1 = r4
            goto L_0x00ee
        L_0x0100:
            r0 = move-exception
            r2 = r1
            r1 = r3
            goto L_0x00ee
        L_0x0104:
            r2 = move-exception
            r9 = r2
            r2 = r1
            r1 = r0
            r0 = r9
            goto L_0x00ee
        L_0x010a:
            r0 = move-exception
            goto L_0x00ee
        L_0x010c:
            r0 = move-exception
            r1 = r6
            r2 = r8
            goto L_0x003a
        L_0x0111:
            r0 = move-exception
            r2 = r1
            r1 = r4
            goto L_0x003a
        L_0x0116:
            r2 = move-exception
            r9 = r2
            r2 = r1
            r1 = r0
            r0 = r9
            goto L_0x003a
        L_0x011d:
            r2 = move-exception
            r3 = r6
            r9 = r0
            r0 = r2
            r2 = r9
            goto L_0x00cf
        L_0x0123:
            r2 = move-exception
            r3 = r4
            r9 = r0
            r0 = r2
            r2 = r9
            goto L_0x00cf
        L_0x0129:
            r0 = move-exception
            r2 = r3
            r3 = r4
            goto L_0x00cf
        L_0x012d:
            r3 = move-exception
            r4 = r6
            r9 = r0
            r0 = r3
            r3 = r9
            goto L_0x009e
        L_0x0134:
            r3 = move-exception
            r9 = r3
            r3 = r0
            r0 = r9
            goto L_0x009e
        L_0x013a:
            r0 = r2
            goto L_0x0061
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adchina.android.ads.a.o.a(java.lang.String):java.io.InputStream");
    }

    private void b() {
        File file = new File(k.c);
        if (file.exists()) {
            for (File file2 : file.listFiles()) {
                if (!this.ac.containsValue(file2.getAbsolutePath())) {
                    file2.delete();
                }
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:29:0x007f A[SYNTHETIC, Splitter:B:29:0x007f] */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0084 A[Catch:{ IOException -> 0x008a }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void e() {
        /*
            r8 = this;
            r3 = 0
            com.adchina.android.ads.h r0 = r8.Z
            java.lang.String r1 = "saveLocalFSImgList"
            r0.c(r1)
            android.content.Context r0 = r8.J     // Catch:{ Exception -> 0x0097, all -> 0x007a }
            java.lang.String r1 = "adchinaShrinkFSImgs.fc"
            r2 = 0
            java.io.FileOutputStream r1 = r0.openFileOutput(r1, r2)     // Catch:{ Exception -> 0x0097, all -> 0x007a }
            java.io.OutputStreamWriter r2 = new java.io.OutputStreamWriter     // Catch:{ Exception -> 0x009b, all -> 0x008c }
            r2.<init>(r1)     // Catch:{ Exception -> 0x009b, all -> 0x008c }
            java.util.HashMap r0 = r8.ac     // Catch:{ Exception -> 0x0051, all -> 0x0090 }
            java.util.Set r0 = r0.keySet()     // Catch:{ Exception -> 0x0051, all -> 0x0090 }
            java.util.Iterator r3 = r0.iterator()     // Catch:{ Exception -> 0x0051, all -> 0x0090 }
        L_0x0020:
            boolean r0 = r3.hasNext()     // Catch:{ Exception -> 0x0051, all -> 0x0090 }
            if (r0 != 0) goto L_0x0032
            r2.flush()     // Catch:{ Exception -> 0x0051, all -> 0x0090 }
            r2.close()     // Catch:{ IOException -> 0x0088 }
            if (r1 == 0) goto L_0x0031
            r1.close()     // Catch:{ IOException -> 0x0088 }
        L_0x0031:
            return
        L_0x0032:
            java.lang.Object r0 = r3.next()     // Catch:{ Exception -> 0x0051, all -> 0x0090 }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ Exception -> 0x0051, all -> 0x0090 }
            r2.write(r0)     // Catch:{ Exception -> 0x0051, all -> 0x0090 }
            java.lang.String r4 = "|||"
            r2.write(r4)     // Catch:{ Exception -> 0x0051, all -> 0x0090 }
            java.util.HashMap r4 = r8.ac     // Catch:{ Exception -> 0x0051, all -> 0x0090 }
            java.lang.Object r0 = r4.get(r0)     // Catch:{ Exception -> 0x0051, all -> 0x0090 }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ Exception -> 0x0051, all -> 0x0090 }
            r2.write(r0)     // Catch:{ Exception -> 0x0051, all -> 0x0090 }
            java.lang.String r0 = "\n"
            r2.write(r0)     // Catch:{ Exception -> 0x0051, all -> 0x0090 }
            goto L_0x0020
        L_0x0051:
            r0 = move-exception
            r7 = r2
            r2 = r1
            r1 = r7
        L_0x0055:
            com.adchina.android.ads.h r3 = r8.Z     // Catch:{ all -> 0x0095 }
            r4 = 2
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ all -> 0x0095 }
            r5 = 0
            java.lang.String r6 = "Exceptions in saveLocalFSImgList, err = "
            r4[r5] = r6     // Catch:{ all -> 0x0095 }
            r5 = 1
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0095 }
            r4[r5] = r0     // Catch:{ all -> 0x0095 }
            java.lang.String r0 = com.adchina.android.ads.Utils.concatString(r4)     // Catch:{ all -> 0x0095 }
            r3.c(r0)     // Catch:{ all -> 0x0095 }
            if (r1 == 0) goto L_0x0072
            r1.close()     // Catch:{ IOException -> 0x0078 }
        L_0x0072:
            if (r2 == 0) goto L_0x0031
            r2.close()     // Catch:{ IOException -> 0x0078 }
            goto L_0x0031
        L_0x0078:
            r0 = move-exception
            goto L_0x0031
        L_0x007a:
            r0 = move-exception
            r1 = r3
            r2 = r3
        L_0x007d:
            if (r1 == 0) goto L_0x0082
            r1.close()     // Catch:{ IOException -> 0x008a }
        L_0x0082:
            if (r2 == 0) goto L_0x0087
            r2.close()     // Catch:{ IOException -> 0x008a }
        L_0x0087:
            throw r0
        L_0x0088:
            r0 = move-exception
            goto L_0x0031
        L_0x008a:
            r1 = move-exception
            goto L_0x0087
        L_0x008c:
            r0 = move-exception
            r2 = r1
            r1 = r3
            goto L_0x007d
        L_0x0090:
            r0 = move-exception
            r7 = r2
            r2 = r1
            r1 = r7
            goto L_0x007d
        L_0x0095:
            r0 = move-exception
            goto L_0x007d
        L_0x0097:
            r0 = move-exception
            r1 = r3
            r2 = r3
            goto L_0x0055
        L_0x009b:
            r0 = move-exception
            r2 = r1
            r1 = r3
            goto L_0x0055
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adchina.android.ads.a.o.e():void");
    }

    private static /* synthetic */ int[] f() {
        int[] iArr = ah;
        if (iArr == null) {
            iArr = new int[g.values().length];
            try {
                iArr[g.EGetImgMaterial.ordinal()] = 3;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[g.EGetShrinkFSImgMaterial.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[g.EIdle.ordinal()] = 17;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[g.EReceiveAd.ordinal()] = 1;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[g.ERefreshAd.ordinal()] = 6;
            } catch (NoSuchFieldError e5) {
            }
            try {
                iArr[g.ESendFullScreenEndedTrack.ordinal()] = 16;
            } catch (NoSuchFieldError e6) {
            }
            try {
                iArr[g.ESendFullScreenImpTrack.ordinal()] = 14;
            } catch (NoSuchFieldError e7) {
            }
            try {
                iArr[g.ESendFullScreenStartedTrack.ordinal()] = 13;
            } catch (NoSuchFieldError e8) {
            }
            try {
                iArr[g.ESendFullScreenThdImpTrack.ordinal()] = 15;
            } catch (NoSuchFieldError e9) {
            }
            try {
                iArr[g.ESendImpTrack.ordinal()] = 4;
            } catch (NoSuchFieldError e10) {
            }
            try {
                iArr[g.ESendShrinkFSImpTrack.ordinal()] = 7;
            } catch (NoSuchFieldError e11) {
            }
            try {
                iArr[g.ESendShrinkFSThdImpTrack.ordinal()] = 8;
            } catch (NoSuchFieldError e12) {
            }
            try {
                iArr[g.ESendThdImpTrack.ordinal()] = 5;
            } catch (NoSuchFieldError e13) {
            }
            try {
                iArr[g.ESendVideoEndedImpTrack.ordinal()] = 11;
            } catch (NoSuchFieldError e14) {
            }
            try {
                iArr[g.ESendVideoEndedThdImpTrack.ordinal()] = 12;
            } catch (NoSuchFieldError e15) {
            }
            try {
                iArr[g.ESendVideoStartedImpTrack.ordinal()] = 9;
            } catch (NoSuchFieldError e16) {
            }
            try {
                iArr[g.ESendVideoStartedThdImpTrack.ordinal()] = 10;
            } catch (NoSuchFieldError e17) {
            }
            ah = iArr;
        }
        return iArr;
    }

    private static /* synthetic */ int[] m() {
        int[] iArr = ai;
        if (iArr == null) {
            iArr = new int[f.values().length];
            try {
                iArr[f.EIdle.ordinal()] = 14;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[f.ESendBtnClkTrack.ordinal()] = 3;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[f.ESendBtnThdClkTrack.ordinal()] = 4;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[f.ESendClkTrack.ordinal()] = 1;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[f.ESendFullScreenClkThdTrack.ordinal()] = 12;
            } catch (NoSuchFieldError e5) {
            }
            try {
                iArr[f.ESendFullScreenClkTrack.ordinal()] = 11;
            } catch (NoSuchFieldError e6) {
            }
            try {
                iArr[f.ESendFullScreenCloseTrack.ordinal()] = 13;
            } catch (NoSuchFieldError e7) {
            }
            try {
                iArr[f.ESendShrinkFSClkTrack.ordinal()] = 5;
            } catch (NoSuchFieldError e8) {
            }
            try {
                iArr[f.ESendShrinkFSThdClkTrack.ordinal()] = 6;
            } catch (NoSuchFieldError e9) {
            }
            try {
                iArr[f.ESendThdClkTrack.ordinal()] = 2;
            } catch (NoSuchFieldError e10) {
            }
            try {
                iArr[f.ESendVideoClkTrack.ordinal()] = 7;
            } catch (NoSuchFieldError e11) {
            }
            try {
                iArr[f.ESendVideoCloseClkTrack.ordinal()] = 9;
            } catch (NoSuchFieldError e12) {
            }
            try {
                iArr[f.ESendVideoCloseThdClkTrack.ordinal()] = 10;
            } catch (NoSuchFieldError e13) {
            }
            try {
                iArr[f.ESendVideoThdClkTrack.ordinal()] = 8;
            } catch (NoSuchFieldError e14) {
            }
            ai = iArr;
        }
        return iArr;
    }

    public final void a() {
        this.Z.c("++ onClickShrinkFSAd");
        this.Z.c(Utils.concatString("++ open ", this.A, " with browser"));
        AdEngine.getAdEngine().openBrowser(this.A.toString());
        this.Z.c("-- open browser");
        a(f.ESendShrinkFSClkTrack);
        this.Z.c("-- onClickShrinkFSAd");
    }

    public final void a(a aVar) {
        aVar.l();
        this.ae = aVar;
        aVar.a(false);
        aVar.a(g.EIdle);
        this.af = AdManager.getAdspaceId();
        AdManager.setAdspaceId(AdManager.getShrinkFSAdspaceId());
        aVar.k();
    }

    public final void a(c cVar) {
        String str;
        String str2;
        while (!this.I && !cVar.a) {
            switch (m()[this.V.ordinal()]) {
                case R.styleable.net_youmi_android_AdView_isGoneWithoutAd /*5*/:
                    this.Z.c("++ onSendShrinkFSClkTrack");
                    a(f.EIdle);
                    try {
                        String concatString = Utils.concatString(this.f.toString(), "204", ",0,0,0", h());
                        t tVar = this.W;
                        t.a(concatString);
                        this.Z.c(Utils.concatString("send ShrinkFSClkTrack to ", concatString));
                        break;
                    } catch (Exception e) {
                        String concatString2 = Utils.concatString("Exceptions in onSendShrinkFSClkTrack, err = ", e.toString());
                        this.Z.c(concatString2);
                        Log.e("AdChinaError", concatString2);
                        break;
                    } finally {
                        Utils.splitTrackUrl(this.C.toString(), this.ab);
                        a(f.ESendShrinkFSThdClkTrack);
                        str2 = "-- onSendShrinkFSClkTrack";
                        this.Z.c(str2);
                    }
                case R.styleable.net_youmi_android_AdView_changeAdAnimation /*6*/:
                    a(f.EIdle);
                    int size = this.ab.size();
                    this.Z.c(Utils.concatString("++ onSendShrinkFSThdClkTrack, Size of list is ", Integer.valueOf(size)));
                    if (size > 0) {
                        try {
                            String str3 = (String) this.ab.get(size - 1);
                            this.ab.removeLast();
                            t tVar2 = this.W;
                            t.a(str3);
                            this.Z.c(Utils.concatString("send ShrinkFSThdClkTrack to ", str3));
                            break;
                        } catch (Exception e2) {
                            String concatString3 = Utils.concatString("Failed to onSendShrinkFSThdClkTrack, err = ", e2.toString());
                            this.Z.c(concatString3);
                            Log.e("AdChinaError", concatString3);
                            break;
                        } finally {
                            a(f.ESendShrinkFSThdClkTrack);
                            str = "-- onSendShrinkFSThdClkTrack";
                            this.Z.c(str);
                        }
                    }
                    break;
            }
            try {
                Thread.sleep(50);
            } catch (InterruptedException e3) {
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:54:0x0244 A[SYNTHETIC, Splitter:B:54:0x0244] */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x0249 A[Catch:{ IOException -> 0x0251 }] */
    /* JADX WARNING: Removed duplicated region for block: B:72:0x02b6 A[SYNTHETIC, Splitter:B:72:0x02b6] */
    /* JADX WARNING: Removed duplicated region for block: B:75:0x02bb A[Catch:{ IOException -> 0x02d8 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(com.adchina.android.ads.a.d r37) {
        /*
            r36 = this;
        L_0x0000:
            r0 = r36
            boolean r0 = r0.I
            r3 = r0
            if (r3 != 0) goto L_0x000e
            r0 = r37
            boolean r0 = r0.a
            r3 = r0
            if (r3 == 0) goto L_0x000f
        L_0x000e:
            return
        L_0x000f:
            int[] r3 = f()
            r0 = r36
            com.adchina.android.ads.g r0 = r0.a
            r4 = r0
            int r4 = r4.ordinal()
            r3 = r3[r4]
            switch(r3) {
                case 1: goto L_0x0029;
                case 2: goto L_0x0313;
                case 3: goto L_0x0021;
                case 4: goto L_0x0021;
                case 5: goto L_0x0021;
                case 6: goto L_0x0549;
                case 7: goto L_0x03ea;
                case 8: goto L_0x0494;
                default: goto L_0x0021;
            }
        L_0x0021:
            r3 = 50
            java.lang.Thread.sleep(r3)     // Catch:{ InterruptedException -> 0x0027 }
            goto L_0x0000
        L_0x0027:
            r3 = move-exception
            goto L_0x0000
        L_0x0029:
            r0 = r36
            com.adchina.android.ads.h r0 = r0.Z
            r3 = r0
            java.lang.String r4 = "++ onReceiveShrinkFSAd"
            r3.c(r4)
            com.adchina.android.ads.g r3 = com.adchina.android.ads.g.EIdle
            r0 = r36
            r1 = r3
            r0.a(r1)
            r3 = 0
            boolean r4 = com.adchina.android.ads.AdManager.getDebugMode()     // Catch:{ Exception -> 0x06fb, all -> 0x06f0 }
            if (r4 == 0) goto L_0x01bc
            java.lang.String r4 = r36.j()     // Catch:{ Exception -> 0x06fb, all -> 0x06f0 }
            if (r4 == 0) goto L_0x0722
            r0 = r36
            com.adchina.android.ads.h r0 = r0.Z     // Catch:{ Exception -> 0x06fb, all -> 0x06f0 }
            r5 = r0
            r6 = 2
            java.lang.Object[] r6 = new java.lang.Object[r6]     // Catch:{ Exception -> 0x06fb, all -> 0x06f0 }
            r7 = 0
            java.lang.String r8 = "AdserverUrl:"
            r6[r7] = r8     // Catch:{ Exception -> 0x06fb, all -> 0x06f0 }
            r7 = 1
            r6[r7] = r4     // Catch:{ Exception -> 0x06fb, all -> 0x06f0 }
            java.lang.String r6 = com.adchina.android.ads.Utils.concatString(r6)     // Catch:{ Exception -> 0x06fb, all -> 0x06f0 }
            r5.c(r6)     // Catch:{ Exception -> 0x06fb, all -> 0x06f0 }
            r0 = r36
            com.adchina.android.ads.t r0 = r0.W     // Catch:{ Exception -> 0x06fb, all -> 0x06f0 }
            r5 = r0
            r0 = r36
            java.lang.StringBuffer r0 = r0.N     // Catch:{ Exception -> 0x06fb, all -> 0x06f0 }
            r6 = r0
            java.lang.String r6 = r6.toString()     // Catch:{ Exception -> 0x06fb, all -> 0x06f0 }
            java.io.InputStream r3 = r5.a(r4, r6)     // Catch:{ Exception -> 0x06fb, all -> 0x06f0 }
            r34 = r3
        L_0x0073:
            r0 = r36
            r1 = r34
            r0.a(r1)     // Catch:{ Exception -> 0x026f, all -> 0x02c2 }
            r0 = r36
            com.adchina.android.ads.a.a r0 = r0.ae     // Catch:{ Exception -> 0x026f, all -> 0x02c2 }
            r3 = r0
            r0 = r36
            java.lang.StringBuffer r0 = r0.c     // Catch:{ Exception -> 0x026f, all -> 0x02c2 }
            r4 = r0
            r0 = r36
            java.lang.StringBuffer r0 = r0.d     // Catch:{ Exception -> 0x026f, all -> 0x02c2 }
            r5 = r0
            r0 = r36
            java.lang.StringBuffer r0 = r0.e     // Catch:{ Exception -> 0x026f, all -> 0x02c2 }
            r6 = r0
            r0 = r36
            java.lang.StringBuffer r0 = r0.f     // Catch:{ Exception -> 0x026f, all -> 0x02c2 }
            r7 = r0
            r0 = r36
            java.lang.StringBuffer r0 = r0.g     // Catch:{ Exception -> 0x026f, all -> 0x02c2 }
            r8 = r0
            r0 = r36
            java.lang.StringBuffer r0 = r0.h     // Catch:{ Exception -> 0x026f, all -> 0x02c2 }
            r9 = r0
            r0 = r36
            java.lang.StringBuffer r0 = r0.i     // Catch:{ Exception -> 0x026f, all -> 0x02c2 }
            r10 = r0
            r0 = r36
            java.lang.StringBuffer r0 = r0.j     // Catch:{ Exception -> 0x026f, all -> 0x02c2 }
            r11 = r0
            r0 = r36
            java.lang.StringBuffer r0 = r0.k     // Catch:{ Exception -> 0x026f, all -> 0x02c2 }
            r12 = r0
            r0 = r36
            java.lang.StringBuffer r0 = r0.l     // Catch:{ Exception -> 0x026f, all -> 0x02c2 }
            r13 = r0
            r0 = r36
            java.lang.StringBuffer r0 = r0.m     // Catch:{ Exception -> 0x026f, all -> 0x02c2 }
            r14 = r0
            r0 = r36
            java.lang.StringBuffer r0 = r0.n     // Catch:{ Exception -> 0x026f, all -> 0x02c2 }
            r15 = r0
            r0 = r36
            java.lang.StringBuffer r0 = r0.o     // Catch:{ Exception -> 0x026f, all -> 0x02c2 }
            r16 = r0
            r0 = r36
            java.lang.StringBuffer r0 = r0.p     // Catch:{ Exception -> 0x026f, all -> 0x02c2 }
            r17 = r0
            r0 = r36
            java.lang.StringBuffer r0 = r0.q     // Catch:{ Exception -> 0x026f, all -> 0x02c2 }
            r18 = r0
            r0 = r36
            java.lang.StringBuffer r0 = r0.r     // Catch:{ Exception -> 0x026f, all -> 0x02c2 }
            r19 = r0
            r0 = r36
            java.lang.StringBuffer r0 = r0.s     // Catch:{ Exception -> 0x026f, all -> 0x02c2 }
            r20 = r0
            r0 = r36
            java.lang.StringBuffer r0 = r0.t     // Catch:{ Exception -> 0x026f, all -> 0x02c2 }
            r21 = r0
            r0 = r36
            java.lang.StringBuffer r0 = r0.u     // Catch:{ Exception -> 0x026f, all -> 0x02c2 }
            r22 = r0
            r0 = r36
            java.lang.StringBuffer r0 = r0.v     // Catch:{ Exception -> 0x026f, all -> 0x02c2 }
            r23 = r0
            r0 = r36
            java.lang.StringBuffer r0 = r0.w     // Catch:{ Exception -> 0x026f, all -> 0x02c2 }
            r24 = r0
            r0 = r36
            java.lang.StringBuffer r0 = r0.x     // Catch:{ Exception -> 0x026f, all -> 0x02c2 }
            r25 = r0
            r0 = r36
            java.lang.StringBuffer r0 = r0.y     // Catch:{ Exception -> 0x026f, all -> 0x02c2 }
            r26 = r0
            r0 = r36
            java.lang.StringBuffer r0 = r0.z     // Catch:{ Exception -> 0x026f, all -> 0x02c2 }
            r27 = r0
            r0 = r36
            java.lang.StringBuffer r0 = r0.A     // Catch:{ Exception -> 0x026f, all -> 0x02c2 }
            r28 = r0
            r0 = r36
            java.lang.StringBuffer r0 = r0.B     // Catch:{ Exception -> 0x026f, all -> 0x02c2 }
            r29 = r0
            r0 = r36
            java.lang.StringBuffer r0 = r0.C     // Catch:{ Exception -> 0x026f, all -> 0x02c2 }
            r30 = r0
            r0 = r36
            java.lang.StringBuffer r0 = r0.D     // Catch:{ Exception -> 0x026f, all -> 0x02c2 }
            r31 = r0
            r0 = r36
            java.lang.StringBuffer r0 = r0.E     // Catch:{ Exception -> 0x026f, all -> 0x02c2 }
            r32 = r0
            r0 = r36
            java.lang.StringBuffer r0 = r0.F     // Catch:{ Exception -> 0x026f, all -> 0x02c2 }
            r33 = r0
            r3.a(r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15, r16, r17, r18, r19, r20, r21, r22, r23, r24, r25, r26, r27, r28, r29, r30, r31, r32, r33)     // Catch:{ Exception -> 0x026f, all -> 0x02c2 }
            r0 = r36
            com.adchina.android.ads.a.a r0 = r0.ae     // Catch:{ Exception -> 0x026f, all -> 0x02c2 }
            r3 = r0
            r3.e()     // Catch:{ Exception -> 0x026f, all -> 0x02c2 }
            r0 = r36
            com.adchina.android.ads.a.a r0 = r0.ae     // Catch:{ Exception -> 0x026f, all -> 0x02c2 }
            r3 = r0
            com.adchina.android.ads.g r4 = com.adchina.android.ads.g.EGetImgMaterial     // Catch:{ Exception -> 0x026f, all -> 0x02c2 }
            r3.a(r4)     // Catch:{ Exception -> 0x026f, all -> 0x02c2 }
            r0 = r36
            com.adchina.android.ads.h r0 = r0.Z     // Catch:{ Exception -> 0x026f, all -> 0x02c2 }
            r3 = r0
            java.lang.String r4 = "loadLocalFSImgList"
            r3.c(r4)     // Catch:{ Exception -> 0x026f, all -> 0x02c2 }
            r3 = 0
            r4 = 0
            r0 = r36
            android.content.Context r0 = r0.J     // Catch:{ Exception -> 0x0221, all -> 0x02ae }
            r5 = r0
            java.lang.String r6 = "adchinaShrinkFSImgs.fc"
            java.io.File r5 = r5.getFileStreamPath(r6)     // Catch:{ Exception -> 0x0221, all -> 0x02ae }
            long r5 = r5.length()     // Catch:{ Exception -> 0x0221, all -> 0x02ae }
            int r5 = (int) r5     // Catch:{ Exception -> 0x0221, all -> 0x02ae }
            char[] r5 = new char[r5]     // Catch:{ Exception -> 0x0221, all -> 0x02ae }
            r0 = r36
            android.content.Context r0 = r0.J     // Catch:{ Exception -> 0x0221, all -> 0x02ae }
            r6 = r0
            java.lang.String r7 = "adchinaShrinkFSImgs.fc"
            java.io.FileInputStream r3 = r6.openFileInput(r7)     // Catch:{ Exception -> 0x0221, all -> 0x02ae }
            java.io.InputStreamReader r6 = new java.io.InputStreamReader     // Catch:{ Exception -> 0x0714, all -> 0x0703 }
            r6.<init>(r3)     // Catch:{ Exception -> 0x0714, all -> 0x0703 }
            r6.read(r5)     // Catch:{ Exception -> 0x071c, all -> 0x070b }
            java.lang.String r4 = new java.lang.String     // Catch:{ Exception -> 0x071c, all -> 0x070b }
            r4.<init>(r5)     // Catch:{ Exception -> 0x071c, all -> 0x070b }
            r0 = r36
            java.util.HashMap r0 = r0.ac     // Catch:{ Exception -> 0x071c, all -> 0x070b }
            r5 = r0
            r5.clear()     // Catch:{ Exception -> 0x071c, all -> 0x070b }
            java.lang.String r5 = "\n"
            java.lang.String[] r4 = r4.split(r5)     // Catch:{ Exception -> 0x071c, all -> 0x070b }
            int r5 = r4.length     // Catch:{ Exception -> 0x071c, all -> 0x070b }
            r7 = 0
        L_0x0182:
            if (r7 < r5) goto L_0x01ed
            r6.close()     // Catch:{ IOException -> 0x02f5 }
            if (r3 == 0) goto L_0x018c
            r3.close()     // Catch:{ IOException -> 0x02f5 }
        L_0x018c:
            r36.b()     // Catch:{ IOException -> 0x02f5 }
        L_0x018f:
            com.adchina.android.ads.g r3 = com.adchina.android.ads.g.EGetShrinkFSImgMaterial     // Catch:{ Exception -> 0x026f, all -> 0x02c2 }
            r0 = r36
            r1 = r3
            r0.a(r1)     // Catch:{ Exception -> 0x026f, all -> 0x02c2 }
            r0 = r36
            java.lang.StringBuffer r0 = r0.B     // Catch:{ Exception -> 0x026f, all -> 0x02c2 }
            r3 = r0
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x026f, all -> 0x02c2 }
            r0 = r36
            java.util.LinkedList r0 = r0.aa     // Catch:{ Exception -> 0x026f, all -> 0x02c2 }
            r4 = r0
            com.adchina.android.ads.Utils.splitTrackUrl(r3, r4)     // Catch:{ Exception -> 0x026f, all -> 0x02c2 }
            r0 = r36
            com.adchina.android.ads.t r0 = r0.W
            r3 = r0
            com.adchina.android.ads.t.a(r34)
            r0 = r36
            com.adchina.android.ads.h r0 = r0.Z
            r3 = r0
            java.lang.String r4 = "-- onReceiveShrinkFSAd"
            r3.c(r4)
            goto L_0x0021
        L_0x01bc:
            java.lang.String r4 = r36.i()     // Catch:{ Exception -> 0x06fb, all -> 0x06f0 }
            r0 = r36
            com.adchina.android.ads.h r0 = r0.Z     // Catch:{ Exception -> 0x06fb, all -> 0x06f0 }
            r5 = r0
            r6 = 2
            java.lang.Object[] r6 = new java.lang.Object[r6]     // Catch:{ Exception -> 0x06fb, all -> 0x06f0 }
            r7 = 0
            java.lang.String r8 = "AdserverUrl:"
            r6[r7] = r8     // Catch:{ Exception -> 0x06fb, all -> 0x06f0 }
            r7 = 1
            r6[r7] = r4     // Catch:{ Exception -> 0x06fb, all -> 0x06f0 }
            java.lang.String r6 = com.adchina.android.ads.Utils.concatString(r6)     // Catch:{ Exception -> 0x06fb, all -> 0x06f0 }
            r5.c(r6)     // Catch:{ Exception -> 0x06fb, all -> 0x06f0 }
            r0 = r36
            com.adchina.android.ads.t r0 = r0.W     // Catch:{ Exception -> 0x06fb, all -> 0x06f0 }
            r5 = r0
            r0 = r36
            java.lang.StringBuffer r0 = r0.N     // Catch:{ Exception -> 0x06fb, all -> 0x06f0 }
            r6 = r0
            java.lang.String r6 = r6.toString()     // Catch:{ Exception -> 0x06fb, all -> 0x06f0 }
            java.io.InputStream r3 = r5.a(r4, r6)     // Catch:{ Exception -> 0x06fb, all -> 0x06f0 }
            r34 = r3
            goto L_0x0073
        L_0x01ed:
            r8 = r4[r7]     // Catch:{ Exception -> 0x071c, all -> 0x070b }
            java.lang.String r9 = "|||"
            int r9 = r8.indexOf(r9)     // Catch:{ Exception -> 0x071c, all -> 0x070b }
            r10 = 0
            java.lang.String r10 = r8.substring(r10, r9)     // Catch:{ Exception -> 0x071c, all -> 0x070b }
            int r9 = r9 + 3
            java.lang.String r8 = r8.substring(r9)     // Catch:{ Exception -> 0x071c, all -> 0x070b }
            java.io.File r9 = new java.io.File     // Catch:{ Exception -> 0x071c, all -> 0x070b }
            r9.<init>(r8)     // Catch:{ Exception -> 0x071c, all -> 0x070b }
            boolean r11 = r9.exists()     // Catch:{ Exception -> 0x071c, all -> 0x070b }
            if (r11 == 0) goto L_0x021d
            java.lang.String r9 = r9.getName()     // Catch:{ Exception -> 0x071c, all -> 0x070b }
            boolean r9 = com.adchina.android.ads.Utils.isCachedFileTimeout(r9)     // Catch:{ Exception -> 0x071c, all -> 0x070b }
            if (r9 != 0) goto L_0x021d
            r0 = r36
            java.util.HashMap r0 = r0.ac     // Catch:{ Exception -> 0x071c, all -> 0x070b }
            r9 = r0
            r9.put(r10, r8)     // Catch:{ Exception -> 0x071c, all -> 0x070b }
        L_0x021d:
            int r7 = r7 + 1
            goto L_0x0182
        L_0x0221:
            r5 = move-exception
            r35 = r5
            r5 = r3
            r3 = r35
        L_0x0227:
            r0 = r36
            com.adchina.android.ads.h r0 = r0.Z     // Catch:{ all -> 0x0711 }
            r6 = r0
            r7 = 2
            java.lang.Object[] r7 = new java.lang.Object[r7]     // Catch:{ all -> 0x0711 }
            r8 = 0
            java.lang.String r9 = "Exceptions in loadLocalFSImgList , err = "
            r7[r8] = r9     // Catch:{ all -> 0x0711 }
            r8 = 1
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x0711 }
            r7[r8] = r3     // Catch:{ all -> 0x0711 }
            java.lang.String r3 = com.adchina.android.ads.Utils.concatString(r7)     // Catch:{ all -> 0x0711 }
            r6.c(r3)     // Catch:{ all -> 0x0711 }
            if (r4 == 0) goto L_0x0247
            r4.close()     // Catch:{ IOException -> 0x0251 }
        L_0x0247:
            if (r5 == 0) goto L_0x024c
            r5.close()     // Catch:{ IOException -> 0x0251 }
        L_0x024c:
            r36.b()     // Catch:{ IOException -> 0x0251 }
            goto L_0x018f
        L_0x0251:
            r3 = move-exception
            r0 = r36
            com.adchina.android.ads.h r0 = r0.Z     // Catch:{ Exception -> 0x026f, all -> 0x02c2 }
            r4 = r0
            r5 = 2
            java.lang.Object[] r5 = new java.lang.Object[r5]     // Catch:{ Exception -> 0x026f, all -> 0x02c2 }
            r6 = 0
            java.lang.String r7 = "Exceptions in loadLocalFSImgList , err = "
            r5[r6] = r7     // Catch:{ Exception -> 0x026f, all -> 0x02c2 }
            r6 = 1
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x026f, all -> 0x02c2 }
            r5[r6] = r3     // Catch:{ Exception -> 0x026f, all -> 0x02c2 }
            java.lang.String r3 = com.adchina.android.ads.Utils.concatString(r5)     // Catch:{ Exception -> 0x026f, all -> 0x02c2 }
            r4.c(r3)     // Catch:{ Exception -> 0x026f, all -> 0x02c2 }
            goto L_0x018f
        L_0x026f:
            r3 = move-exception
            r4 = r34
        L_0x0272:
            r5 = 2
            java.lang.Object[] r5 = new java.lang.Object[r5]     // Catch:{ all -> 0x06f8 }
            r6 = 0
            java.lang.String r7 = "Exceptions in onReceiveShrinkFSAd, err = "
            r5[r6] = r7     // Catch:{ all -> 0x06f8 }
            r6 = 1
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x06f8 }
            r5[r6] = r3     // Catch:{ all -> 0x06f8 }
            java.lang.String r3 = com.adchina.android.ads.Utils.concatString(r5)     // Catch:{ all -> 0x06f8 }
            r5 = 5
            r0 = r36
            r1 = r5
            r2 = r3
            r0.a(r1, r2)     // Catch:{ all -> 0x06f8 }
            r0 = r36
            com.adchina.android.ads.h r0 = r0.Z     // Catch:{ all -> 0x06f8 }
            r5 = r0
            r5.c(r3)     // Catch:{ all -> 0x06f8 }
            java.lang.String r5 = "AdChinaError"
            android.util.Log.e(r5, r3)     // Catch:{ all -> 0x06f8 }
            r0 = r36
            com.adchina.android.ads.t r0 = r0.W
            r3 = r0
            com.adchina.android.ads.t.a(r4)
            r0 = r36
            com.adchina.android.ads.h r0 = r0.Z
            r3 = r0
            java.lang.String r4 = "-- onReceiveShrinkFSAd"
            r3.c(r4)
            goto L_0x0021
        L_0x02ae:
            r5 = move-exception
            r35 = r5
            r5 = r3
            r3 = r35
        L_0x02b4:
            if (r4 == 0) goto L_0x02b9
            r4.close()     // Catch:{ IOException -> 0x02d8 }
        L_0x02b9:
            if (r5 == 0) goto L_0x02be
            r5.close()     // Catch:{ IOException -> 0x02d8 }
        L_0x02be:
            r36.b()     // Catch:{ IOException -> 0x02d8 }
        L_0x02c1:
            throw r3     // Catch:{ Exception -> 0x026f, all -> 0x02c2 }
        L_0x02c2:
            r3 = move-exception
            r4 = r34
        L_0x02c5:
            r0 = r36
            com.adchina.android.ads.t r0 = r0.W
            r5 = r0
            com.adchina.android.ads.t.a(r4)
            r0 = r36
            com.adchina.android.ads.h r0 = r0.Z
            r4 = r0
            java.lang.String r5 = "-- onReceiveShrinkFSAd"
            r4.c(r5)
            throw r3
        L_0x02d8:
            r4 = move-exception
            r0 = r36
            com.adchina.android.ads.h r0 = r0.Z     // Catch:{ Exception -> 0x026f, all -> 0x02c2 }
            r5 = r0
            r6 = 2
            java.lang.Object[] r6 = new java.lang.Object[r6]     // Catch:{ Exception -> 0x026f, all -> 0x02c2 }
            r7 = 0
            java.lang.String r8 = "Exceptions in loadLocalFSImgList , err = "
            r6[r7] = r8     // Catch:{ Exception -> 0x026f, all -> 0x02c2 }
            r7 = 1
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x026f, all -> 0x02c2 }
            r6[r7] = r4     // Catch:{ Exception -> 0x026f, all -> 0x02c2 }
            java.lang.String r4 = com.adchina.android.ads.Utils.concatString(r6)     // Catch:{ Exception -> 0x026f, all -> 0x02c2 }
            r5.c(r4)     // Catch:{ Exception -> 0x026f, all -> 0x02c2 }
            goto L_0x02c1
        L_0x02f5:
            r3 = move-exception
            r0 = r36
            com.adchina.android.ads.h r0 = r0.Z     // Catch:{ Exception -> 0x026f, all -> 0x02c2 }
            r4 = r0
            r5 = 2
            java.lang.Object[] r5 = new java.lang.Object[r5]     // Catch:{ Exception -> 0x026f, all -> 0x02c2 }
            r6 = 0
            java.lang.String r7 = "Exceptions in loadLocalFSImgList , err = "
            r5[r6] = r7     // Catch:{ Exception -> 0x026f, all -> 0x02c2 }
            r6 = 1
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x026f, all -> 0x02c2 }
            r5[r6] = r3     // Catch:{ Exception -> 0x026f, all -> 0x02c2 }
            java.lang.String r3 = com.adchina.android.ads.Utils.concatString(r5)     // Catch:{ Exception -> 0x026f, all -> 0x02c2 }
            r4.c(r3)     // Catch:{ Exception -> 0x026f, all -> 0x02c2 }
            goto L_0x018f
        L_0x0313:
            r0 = r36
            com.adchina.android.ads.h r0 = r0.Z
            r3 = r0
            java.lang.String r4 = "++ onGetShrinkFSImgMaterial"
            r3.c(r4)
            com.adchina.android.ads.g r3 = com.adchina.android.ads.g.EIdle
            r0 = r36
            r1 = r3
            r0.a(r1)
            r4 = 0
            r0 = r36
            java.lang.StringBuffer r0 = r0.z     // Catch:{ Exception -> 0x06e1 }
            r3 = r0
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x06e1 }
            r0 = r36
            java.util.HashMap r0 = r0.ac     // Catch:{ Exception -> 0x06e1 }
            r5 = r0
            boolean r5 = r5.containsKey(r3)     // Catch:{ Exception -> 0x06e1 }
            if (r5 != 0) goto L_0x036d
            r0 = r36
            r1 = r3
            java.io.InputStream r3 = r0.a(r1)     // Catch:{ Exception -> 0x06e1 }
            r0 = r36
            com.adchina.android.ads.h r0 = r0.Z     // Catch:{ Exception -> 0x06e4, all -> 0x06cd }
            r4 = r0
            java.lang.String r5 = "++ load remote fullScreen img"
            r4.c(r5)     // Catch:{ Exception -> 0x06e4, all -> 0x06cd }
        L_0x034b:
            android.graphics.Bitmap r4 = com.adchina.android.ads.Utils.convertStreamToBitmap(r3)     // Catch:{ Exception -> 0x0394, all -> 0x06d9 }
            if (r4 == 0) goto L_0x0389
            r5 = 4
            r0 = r36
            r1 = r5
            r2 = r4
            r0.a(r1, r2)     // Catch:{ Exception -> 0x0394, all -> 0x06d9 }
        L_0x0359:
            r0 = r36
            com.adchina.android.ads.t r0 = r0.W
            r4 = r0
            com.adchina.android.ads.t.a(r3)
            r0 = r36
            com.adchina.android.ads.h r0 = r0.Z
            r3 = r0
            java.lang.String r4 = "-- onGetShrinkFSImgMaterial"
            r3.c(r4)
            goto L_0x0021
        L_0x036d:
            java.io.FileInputStream r5 = new java.io.FileInputStream     // Catch:{ Exception -> 0x06e1 }
            r0 = r36
            java.util.HashMap r0 = r0.ac     // Catch:{ Exception -> 0x06e1 }
            r6 = r0
            java.lang.Object r3 = r6.get(r3)     // Catch:{ Exception -> 0x06e1 }
            java.lang.String r3 = (java.lang.String) r3     // Catch:{ Exception -> 0x06e1 }
            r5.<init>(r3)     // Catch:{ Exception -> 0x06e1 }
            r0 = r36
            com.adchina.android.ads.h r0 = r0.Z     // Catch:{ Exception -> 0x06ec, all -> 0x06d5 }
            r3 = r0
            java.lang.String r4 = "++ load local fullScreen img"
            r3.c(r4)     // Catch:{ Exception -> 0x06ec, all -> 0x06d5 }
            r3 = r5
            goto L_0x034b
        L_0x0389:
            r4 = 5
            java.lang.String r5 = "Full Screen AdMaterial is null"
            r0 = r36
            r1 = r4
            r2 = r5
            r0.a(r1, r2)     // Catch:{ Exception -> 0x0394, all -> 0x06d9 }
            goto L_0x0359
        L_0x0394:
            r4 = move-exception
            r35 = r4
            r4 = r3
            r3 = r35
        L_0x039a:
            r5 = 2
            java.lang.Object[] r5 = new java.lang.Object[r5]     // Catch:{ all -> 0x03d6 }
            r6 = 0
            java.lang.String r7 = "Failed to get full-screen ad material, err = "
            r5[r6] = r7     // Catch:{ all -> 0x03d6 }
            r6 = 1
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x03d6 }
            r5[r6] = r3     // Catch:{ all -> 0x03d6 }
            java.lang.String r3 = com.adchina.android.ads.Utils.concatString(r5)     // Catch:{ all -> 0x03d6 }
            r5 = 5
            r0 = r36
            r1 = r5
            r2 = r3
            r0.a(r1, r2)     // Catch:{ all -> 0x03d6 }
            r0 = r36
            com.adchina.android.ads.h r0 = r0.Z     // Catch:{ all -> 0x03d6 }
            r5 = r0
            r5.c(r3)     // Catch:{ all -> 0x03d6 }
            java.lang.String r5 = "AdChinaError"
            android.util.Log.e(r5, r3)     // Catch:{ all -> 0x03d6 }
            r0 = r36
            com.adchina.android.ads.t r0 = r0.W
            r3 = r0
            com.adchina.android.ads.t.a(r4)
            r0 = r36
            com.adchina.android.ads.h r0 = r0.Z
            r3 = r0
            java.lang.String r4 = "-- onGetShrinkFSImgMaterial"
            r3.c(r4)
            goto L_0x0021
        L_0x03d6:
            r3 = move-exception
        L_0x03d7:
            r0 = r36
            com.adchina.android.ads.t r0 = r0.W
            r5 = r0
            com.adchina.android.ads.t.a(r4)
            r0 = r36
            com.adchina.android.ads.h r0 = r0.Z
            r4 = r0
            java.lang.String r5 = "-- onGetShrinkFSImgMaterial"
            r4.c(r5)
            throw r3
        L_0x03ea:
            r0 = r36
            com.adchina.android.ads.h r0 = r0.Z
            r3 = r0
            java.lang.String r4 = "++ onSendShrinkFSImpTrack"
            r3.c(r4)
            r3 = 4
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ Exception -> 0x044b }
            r4 = 0
            r0 = r36
            java.lang.StringBuffer r0 = r0.f     // Catch:{ Exception -> 0x044b }
            r5 = r0
            java.lang.String r5 = r5.toString()     // Catch:{ Exception -> 0x044b }
            r3[r4] = r5     // Catch:{ Exception -> 0x044b }
            r4 = 1
            java.lang.String r5 = "203"
            r3[r4] = r5     // Catch:{ Exception -> 0x044b }
            r4 = 2
            java.lang.String r5 = ",0,0,0"
            r3[r4] = r5     // Catch:{ Exception -> 0x044b }
            r4 = 3
            java.lang.String r5 = r36.h()     // Catch:{ Exception -> 0x044b }
            r3[r4] = r5     // Catch:{ Exception -> 0x044b }
            java.lang.String r3 = com.adchina.android.ads.Utils.concatString(r3)     // Catch:{ Exception -> 0x044b }
            r0 = r36
            com.adchina.android.ads.t r0 = r0.W     // Catch:{ Exception -> 0x044b }
            r4 = r0
            com.adchina.android.ads.t.a(r3)     // Catch:{ Exception -> 0x044b }
            r0 = r36
            com.adchina.android.ads.h r0 = r0.Z     // Catch:{ Exception -> 0x044b }
            r4 = r0
            r5 = 2
            java.lang.Object[] r5 = new java.lang.Object[r5]     // Catch:{ Exception -> 0x044b }
            r6 = 0
            java.lang.String r7 = "send ShrinkFSImpTrack to "
            r5[r6] = r7     // Catch:{ Exception -> 0x044b }
            r6 = 1
            r5[r6] = r3     // Catch:{ Exception -> 0x044b }
            java.lang.String r3 = com.adchina.android.ads.Utils.concatString(r5)     // Catch:{ Exception -> 0x044b }
            r4.c(r3)     // Catch:{ Exception -> 0x044b }
            com.adchina.android.ads.g r3 = com.adchina.android.ads.g.ESendShrinkFSThdImpTrack
            r0 = r36
            r1 = r3
            r0.a(r1)
            r0 = r36
            com.adchina.android.ads.h r0 = r0.Z
            r3 = r0
            java.lang.String r4 = "-- onSendShrinkFSImpTrack"
            r3.c(r4)
            goto L_0x0021
        L_0x044b:
            r3 = move-exception
            r4 = 2
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ all -> 0x0480 }
            r5 = 0
            java.lang.String r6 = "Exceptions in onSendShrinkFSImpTrack, err = "
            r4[r5] = r6     // Catch:{ all -> 0x0480 }
            r5 = 1
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x0480 }
            r4[r5] = r3     // Catch:{ all -> 0x0480 }
            java.lang.String r3 = com.adchina.android.ads.Utils.concatString(r4)     // Catch:{ all -> 0x0480 }
            r0 = r36
            com.adchina.android.ads.h r0 = r0.Z     // Catch:{ all -> 0x0480 }
            r4 = r0
            r4.c(r3)     // Catch:{ all -> 0x0480 }
            java.lang.String r4 = "AdChinaError"
            android.util.Log.e(r4, r3)     // Catch:{ all -> 0x0480 }
            com.adchina.android.ads.g r3 = com.adchina.android.ads.g.ESendShrinkFSThdImpTrack
            r0 = r36
            r1 = r3
            r0.a(r1)
            r0 = r36
            com.adchina.android.ads.h r0 = r0.Z
            r3 = r0
            java.lang.String r4 = "-- onSendShrinkFSImpTrack"
            r3.c(r4)
            goto L_0x0021
        L_0x0480:
            r3 = move-exception
            com.adchina.android.ads.g r4 = com.adchina.android.ads.g.ESendShrinkFSThdImpTrack
            r0 = r36
            r1 = r4
            r0.a(r1)
            r0 = r36
            com.adchina.android.ads.h r0 = r0.Z
            r4 = r0
            java.lang.String r5 = "-- onSendShrinkFSImpTrack"
            r4.c(r5)
            throw r3
        L_0x0494:
            com.adchina.android.ads.g r3 = com.adchina.android.ads.g.EIdle
            r0 = r36
            r1 = r3
            r0.a(r1)
            r0 = r36
            java.util.LinkedList r0 = r0.aa
            r3 = r0
            int r3 = r3.size()
            r0 = r36
            com.adchina.android.ads.h r0 = r0.Z
            r4 = r0
            r5 = 2
            java.lang.Object[] r5 = new java.lang.Object[r5]
            r6 = 0
            java.lang.String r7 = "++ onSendShrinkFSThdImpTrack, Size of list is "
            r5[r6] = r7
            r6 = 1
            java.lang.Integer r7 = java.lang.Integer.valueOf(r3)
            r5[r6] = r7
            java.lang.String r5 = com.adchina.android.ads.Utils.concatString(r5)
            r4.c(r5)
            if (r3 <= 0) goto L_0x053e
            r0 = r36
            java.util.LinkedList r0 = r0.aa     // Catch:{ Exception -> 0x050a }
            r4 = r0
            r5 = 1
            int r3 = r3 - r5
            java.lang.Object r3 = r4.get(r3)     // Catch:{ Exception -> 0x050a }
            java.lang.String r3 = (java.lang.String) r3     // Catch:{ Exception -> 0x050a }
            r0 = r36
            java.util.LinkedList r0 = r0.aa     // Catch:{ Exception -> 0x050a }
            r4 = r0
            r4.removeLast()     // Catch:{ Exception -> 0x050a }
            r0 = r36
            com.adchina.android.ads.t r0 = r0.W     // Catch:{ Exception -> 0x050a }
            r4 = r0
            com.adchina.android.ads.t.a(r3)     // Catch:{ Exception -> 0x050a }
            r0 = r36
            com.adchina.android.ads.h r0 = r0.Z     // Catch:{ Exception -> 0x050a }
            r4 = r0
            r5 = 2
            java.lang.Object[] r5 = new java.lang.Object[r5]     // Catch:{ Exception -> 0x050a }
            r6 = 0
            java.lang.String r7 = "send ShrinkFSThdImpTrack to "
            r5[r6] = r7     // Catch:{ Exception -> 0x050a }
            r6 = 1
            r5[r6] = r3     // Catch:{ Exception -> 0x050a }
            java.lang.String r3 = com.adchina.android.ads.Utils.concatString(r5)     // Catch:{ Exception -> 0x050a }
            r4.c(r3)     // Catch:{ Exception -> 0x050a }
            com.adchina.android.ads.g r3 = com.adchina.android.ads.g.ESendShrinkFSThdImpTrack
            r0 = r36
            r1 = r3
            r0.a(r1)
        L_0x04fe:
            r0 = r36
            com.adchina.android.ads.h r0 = r0.Z
            r3 = r0
            java.lang.String r4 = "-- onSendShrinkFSThdImpTrack"
            r3.c(r4)
            goto L_0x0021
        L_0x050a:
            r3 = move-exception
            r4 = 2
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ all -> 0x0534 }
            r5 = 0
            java.lang.String r6 = "Failed to onSendShrinkFSThdImpTrack, err = "
            r4[r5] = r6     // Catch:{ all -> 0x0534 }
            r5 = 1
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x0534 }
            r4[r5] = r3     // Catch:{ all -> 0x0534 }
            java.lang.String r3 = com.adchina.android.ads.Utils.concatString(r4)     // Catch:{ all -> 0x0534 }
            r0 = r36
            com.adchina.android.ads.h r0 = r0.Z     // Catch:{ all -> 0x0534 }
            r4 = r0
            r4.c(r3)     // Catch:{ all -> 0x0534 }
            java.lang.String r4 = "AdChinaError"
            android.util.Log.e(r4, r3)     // Catch:{ all -> 0x0534 }
            com.adchina.android.ads.g r3 = com.adchina.android.ads.g.ESendShrinkFSThdImpTrack
            r0 = r36
            r1 = r3
            r0.a(r1)
            goto L_0x04fe
        L_0x0534:
            r3 = move-exception
            com.adchina.android.ads.g r4 = com.adchina.android.ads.g.ESendShrinkFSThdImpTrack
            r0 = r36
            r1 = r4
            r0.a(r1)
            throw r3
        L_0x053e:
            r3 = 3
            java.lang.String r4 = "RefreshAd"
            r0 = r36
            r1 = r3
            r2 = r4
            r0.a(r1, r2)
            goto L_0x04fe
        L_0x0549:
            r0 = r36
            com.adchina.android.ads.h r0 = r0.Z
            r3 = r0
            java.lang.String r4 = "++ onRefreshAd"
            r3.c(r4)
            r3 = 30
            r0 = r36
            java.lang.StringBuffer r0 = r0.j     // Catch:{ Exception -> 0x0591, all -> 0x060d }
            r4 = r0
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x0591, all -> 0x060d }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ Exception -> 0x0591, all -> 0x060d }
            int r3 = r4.intValue()     // Catch:{ Exception -> 0x0591, all -> 0x060d }
            int r4 = r3 * 1000
            long r4 = (long) r4
            java.lang.Thread.sleep(r4)     // Catch:{ Exception -> 0x0682, all -> 0x06a8 }
            com.adchina.android.ads.g r4 = com.adchina.android.ads.g.EReceiveAd
            r0 = r36
            r1 = r4
            r0.a(r1)
            r0 = r36
            com.adchina.android.ads.h r0 = r0.Z
            r4 = r0
            r5 = 2
            java.lang.Object[] r5 = new java.lang.Object[r5]
            r6 = 0
            java.lang.String r7 = "-- onRefreshAd, delay = "
            r5[r6] = r7
            r6 = 1
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)
            r5[r6] = r3
            java.lang.String r3 = com.adchina.android.ads.Utils.concatString(r5)
            r4.c(r3)
            goto L_0x0021
        L_0x0591:
            r3 = move-exception
            r3 = 30000(0x7530, double:1.4822E-319)
            java.lang.Thread.sleep(r3)     // Catch:{ Exception -> 0x05be, all -> 0x05e6 }
            com.adchina.android.ads.g r3 = com.adchina.android.ads.g.EReceiveAd
            r0 = r36
            r1 = r3
            r0.a(r1)
            r0 = r36
            com.adchina.android.ads.h r0 = r0.Z
            r3 = r0
            r4 = 2
            java.lang.Object[] r4 = new java.lang.Object[r4]
            r5 = 0
            java.lang.String r6 = "-- onRefreshAd, delay = "
            r4[r5] = r6
            r5 = 1
            r6 = 30
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)
            r4[r5] = r6
            java.lang.String r4 = com.adchina.android.ads.Utils.concatString(r4)
            r3.c(r4)
            goto L_0x0021
        L_0x05be:
            r3 = move-exception
            com.adchina.android.ads.g r3 = com.adchina.android.ads.g.EReceiveAd
            r0 = r36
            r1 = r3
            r0.a(r1)
            r0 = r36
            com.adchina.android.ads.h r0 = r0.Z
            r3 = r0
            r4 = 2
            java.lang.Object[] r4 = new java.lang.Object[r4]
            r5 = 0
            java.lang.String r6 = "-- onRefreshAd, delay = "
            r4[r5] = r6
            r5 = 1
            r6 = 30
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)
            r4[r5] = r6
            java.lang.String r4 = com.adchina.android.ads.Utils.concatString(r4)
            r3.c(r4)
            goto L_0x0021
        L_0x05e6:
            r3 = move-exception
            com.adchina.android.ads.g r4 = com.adchina.android.ads.g.EReceiveAd
            r0 = r36
            r1 = r4
            r0.a(r1)
            r0 = r36
            com.adchina.android.ads.h r0 = r0.Z
            r4 = r0
            r5 = 2
            java.lang.Object[] r5 = new java.lang.Object[r5]
            r6 = 0
            java.lang.String r7 = "-- onRefreshAd, delay = "
            r5[r6] = r7
            r6 = 1
            r7 = 30
            java.lang.Integer r7 = java.lang.Integer.valueOf(r7)
            r5[r6] = r7
            java.lang.String r5 = com.adchina.android.ads.Utils.concatString(r5)
            r4.c(r5)
            throw r3
        L_0x060d:
            r4 = move-exception
            int r5 = r3 * 1000
            long r5 = (long) r5
            java.lang.Thread.sleep(r5)     // Catch:{ Exception -> 0x0638, all -> 0x065d }
            com.adchina.android.ads.g r5 = com.adchina.android.ads.g.EReceiveAd
            r0 = r36
            r1 = r5
            r0.a(r1)
            r0 = r36
            com.adchina.android.ads.h r0 = r0.Z
            r5 = r0
            r6 = 2
            java.lang.Object[] r6 = new java.lang.Object[r6]
            r7 = 0
            java.lang.String r8 = "-- onRefreshAd, delay = "
            r6[r7] = r8
            r7 = 1
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)
            r6[r7] = r3
            java.lang.String r3 = com.adchina.android.ads.Utils.concatString(r6)
            r5.c(r3)
        L_0x0637:
            throw r4
        L_0x0638:
            r5 = move-exception
            com.adchina.android.ads.g r5 = com.adchina.android.ads.g.EReceiveAd
            r0 = r36
            r1 = r5
            r0.a(r1)
            r0 = r36
            com.adchina.android.ads.h r0 = r0.Z
            r5 = r0
            r6 = 2
            java.lang.Object[] r6 = new java.lang.Object[r6]
            r7 = 0
            java.lang.String r8 = "-- onRefreshAd, delay = "
            r6[r7] = r8
            r7 = 1
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)
            r6[r7] = r3
            java.lang.String r3 = com.adchina.android.ads.Utils.concatString(r6)
            r5.c(r3)
            goto L_0x0637
        L_0x065d:
            r4 = move-exception
            com.adchina.android.ads.g r5 = com.adchina.android.ads.g.EReceiveAd
            r0 = r36
            r1 = r5
            r0.a(r1)
            r0 = r36
            com.adchina.android.ads.h r0 = r0.Z
            r5 = r0
            r6 = 2
            java.lang.Object[] r6 = new java.lang.Object[r6]
            r7 = 0
            java.lang.String r8 = "-- onRefreshAd, delay = "
            r6[r7] = r8
            r7 = 1
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)
            r6[r7] = r3
            java.lang.String r3 = com.adchina.android.ads.Utils.concatString(r6)
            r5.c(r3)
            throw r4
        L_0x0682:
            r4 = move-exception
            com.adchina.android.ads.g r4 = com.adchina.android.ads.g.EReceiveAd
            r0 = r36
            r1 = r4
            r0.a(r1)
            r0 = r36
            com.adchina.android.ads.h r0 = r0.Z
            r4 = r0
            r5 = 2
            java.lang.Object[] r5 = new java.lang.Object[r5]
            r6 = 0
            java.lang.String r7 = "-- onRefreshAd, delay = "
            r5[r6] = r7
            r6 = 1
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)
            r5[r6] = r3
            java.lang.String r3 = com.adchina.android.ads.Utils.concatString(r5)
            r4.c(r3)
            goto L_0x0021
        L_0x06a8:
            r4 = move-exception
            com.adchina.android.ads.g r5 = com.adchina.android.ads.g.EReceiveAd
            r0 = r36
            r1 = r5
            r0.a(r1)
            r0 = r36
            com.adchina.android.ads.h r0 = r0.Z
            r5 = r0
            r6 = 2
            java.lang.Object[] r6 = new java.lang.Object[r6]
            r7 = 0
            java.lang.String r8 = "-- onRefreshAd, delay = "
            r6[r7] = r8
            r7 = 1
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)
            r6[r7] = r3
            java.lang.String r3 = com.adchina.android.ads.Utils.concatString(r6)
            r5.c(r3)
            throw r4
        L_0x06cd:
            r4 = move-exception
            r35 = r4
            r4 = r3
            r3 = r35
            goto L_0x03d7
        L_0x06d5:
            r3 = move-exception
            r4 = r5
            goto L_0x03d7
        L_0x06d9:
            r4 = move-exception
            r35 = r4
            r4 = r3
            r3 = r35
            goto L_0x03d7
        L_0x06e1:
            r3 = move-exception
            goto L_0x039a
        L_0x06e4:
            r4 = move-exception
            r35 = r4
            r4 = r3
            r3 = r35
            goto L_0x039a
        L_0x06ec:
            r3 = move-exception
            r4 = r5
            goto L_0x039a
        L_0x06f0:
            r4 = move-exception
            r35 = r4
            r4 = r3
            r3 = r35
            goto L_0x02c5
        L_0x06f8:
            r3 = move-exception
            goto L_0x02c5
        L_0x06fb:
            r4 = move-exception
            r35 = r4
            r4 = r3
            r3 = r35
            goto L_0x0272
        L_0x0703:
            r5 = move-exception
            r35 = r5
            r5 = r3
            r3 = r35
            goto L_0x02b4
        L_0x070b:
            r4 = move-exception
            r5 = r3
            r3 = r4
            r4 = r6
            goto L_0x02b4
        L_0x0711:
            r3 = move-exception
            goto L_0x02b4
        L_0x0714:
            r5 = move-exception
            r35 = r5
            r5 = r3
            r3 = r35
            goto L_0x0227
        L_0x071c:
            r4 = move-exception
            r5 = r3
            r3 = r4
            r4 = r6
            goto L_0x0227
        L_0x0722:
            r34 = r3
            goto L_0x0073
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adchina.android.ads.a.o.a(com.adchina.android.ads.a.d):void");
    }

    public final void a(ShrinkFSAdView shrinkFSAdView) {
        this.ad = shrinkFSAdView;
        this.ad.a(this);
    }

    /* access modifiers changed from: protected */
    public final void b(String str) {
        a(str, "adchinaShFSFC.fc");
    }

    /* access modifiers changed from: protected */
    public final void c() {
        this.N.setLength(0);
        this.N.append(AdManager.getShrinkFSAdspaceId());
    }

    /* access modifiers changed from: protected */
    public final String d() {
        return c("adchinaShFSFC.fc");
    }

    /* access modifiers changed from: protected */
    public final void g() {
        super.g();
        this.aa.clear();
        this.ab.clear();
    }

    public final boolean handleMessage(Message message) {
        switch (message.what) {
            case 3:
                a(g.ERefreshAd);
                break;
            case 4:
                if (X != null) {
                    try {
                        X.onReceiveShrinkFSAd(this.ad);
                    } catch (Exception e) {
                    }
                }
                this.Z.c("++ onDisplayShrinkFSAd");
                this.ad.setImageBitmap((Bitmap) message.obj);
                a(g.ESendShrinkFSImpTrack);
                this.Z.c("-- onDisplayShrinkFSAd");
                break;
            case R.styleable.net_youmi_android_AdView_isGoneWithoutAd /*5*/:
                if (X != null) {
                    try {
                        X.onFailedToReceiveShrinkFSAd(this.ad);
                    } catch (Exception e2) {
                    }
                }
                a(g.ERefreshAd);
                break;
        }
        if (message == null) {
            return true;
        }
        this.Y.removeMessages(message.what);
        return true;
    }

    public final void l() {
        this.H = false;
        this.I = true;
        if (this.ae != null) {
            new Timer().schedule(new p(this), 30000);
        }
    }
}
