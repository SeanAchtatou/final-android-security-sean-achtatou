package com.adchina.android.ads;

import com.adchina.android.ads.views.AdView;
import com.adchina.android.ads.views.ShrinkFSAdView;

public interface AdListener {
    boolean OnRecvSms(AdView adView, String str);

    void onDisplayFullScreenAd();

    void onFailedToPlayVideoAd();

    void onFailedToReceiveAd(AdView adView);

    void onFailedToReceiveFullScreenAd();

    void onFailedToReceiveShrinkFSAd(ShrinkFSAdView shrinkFSAdView);

    void onFailedToReceiveVideoAd();

    void onFailedToRefreshAd(AdView adView);

    void onPlayVideoAd();

    void onReceiveAd(AdView adView);

    void onReceiveFullScreenAd();

    void onReceiveShrinkFSAd(ShrinkFSAdView shrinkFSAdView);

    void onReceiveVideoAd();

    void onRefreshAd(AdView adView);
}
