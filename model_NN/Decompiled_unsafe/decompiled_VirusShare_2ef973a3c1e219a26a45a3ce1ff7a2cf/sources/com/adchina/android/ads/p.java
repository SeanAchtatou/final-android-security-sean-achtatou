package com.adchina.android.ads;

final class p {
    private static final int[] D = {8, 8, 4, 2};
    private static final int[] E;
    private int A;
    private long B;
    private int C = 0;
    private int F = 100;
    int a = 0;
    int b;
    int c;
    byte[] d;
    int e;
    int f;
    int g;
    /* access modifiers changed from: private */
    public int h = 0;
    /* access modifiers changed from: private */
    public int i = 0;
    private float j = 1.0f;
    private int k = -1;
    private int[] l = new int[280];
    private int m = -1;
    private int n = 0;
    private boolean o = false;
    private int[] p = null;
    private int q = 0;
    private int r = 0;
    private int s;
    private int t;
    private int u = 0;
    private boolean v = false;
    private boolean w = false;
    private int[] x = null;
    private int y = 0;
    private boolean z;

    static {
        int[] iArr = new int[4];
        iArr[1] = 4;
        iArr[2] = 2;
        iArr[3] = 1;
        E = iArr;
    }

    public p(byte[] bArr) {
        this.d = bArr;
        this.b = this.d.length;
        this.c = 0;
        this.h = a(this.d[6], this.d[7]);
        this.i = a(this.d[8], this.d[9]);
    }

    private static final int a(int i2, int i3) {
        return (i3 << 8) | b((byte) i2);
    }

    private boolean a(int i2) {
        if (this.c + i2 >= this.b) {
            return false;
        }
        for (int i3 = 0; i3 < i2; i3++) {
            this.l[i3] = b(this.d[this.c + i3]);
        }
        this.c += i2;
        return true;
    }

    private boolean a(int i2, boolean z2) {
        int[] iArr = new int[i2];
        for (int i3 = 0; i3 < i2; i3++) {
            if (!a(3)) {
                return false;
            }
            iArr[i3] = (this.l[0] << 16) | (this.l[1] << 8) | this.l[2] | -16777216;
        }
        if (z2) {
            this.p = iArr;
        } else {
            this.x = iArr;
        }
        return true;
    }

    private static final int b(int i2) {
        return i2 < 0 ? i2 + 256 : i2;
    }

    private static final boolean b(int i2, int i3) {
        return (i2 & i3) == i3;
    }

    private int e() {
        if (!a(1)) {
            return -1;
        }
        int i2 = this.l[0];
        if (i2 == 0 || a(i2)) {
            return i2;
        }
        return -1;
    }

    public final boolean a() {
        return this.b - this.c >= 16;
    }

    public final void b() {
        this.a++;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:98:0x033d, code lost:
        r23 = ((int) r0.B) & ((1 << r22) - 1);
        r2.B = r0.B >> r22;
        r1.C = r0.C - r22;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.adchina.android.ads.e c() {
        /*
            r30 = this;
            r0 = r30
            int r0 = r0.a
            r3 = r0
            r4 = 100
            r0 = r4
            r1 = r30
            r1.F = r0
            r0 = r30
            int r0 = r0.k
            r4 = r0
            if (r3 > r4) goto L_0x0015
            r3 = 0
        L_0x0014:
            return r3
        L_0x0015:
            r0 = r30
            int r0 = r0.k
            r4 = r0
            if (r4 >= 0) goto L_0x00cd
            r4 = 6
            r0 = r30
            r1 = r4
            boolean r4 = r0.a(r1)
            if (r4 == 0) goto L_0x007f
            r0 = r30
            int[] r0 = r0.l
            r4 = r0
            r5 = 0
            r4 = r4[r5]
            r5 = 71
            if (r4 != r5) goto L_0x007f
            r0 = r30
            int[] r0 = r0.l
            r4 = r0
            r5 = 1
            r4 = r4[r5]
            r5 = 73
            if (r4 != r5) goto L_0x007f
            r0 = r30
            int[] r0 = r0.l
            r4 = r0
            r5 = 2
            r4 = r4[r5]
            r5 = 70
            if (r4 != r5) goto L_0x007f
            r0 = r30
            int[] r0 = r0.l
            r4 = r0
            r5 = 3
            r4 = r4[r5]
            r5 = 56
            if (r4 != r5) goto L_0x007f
            r0 = r30
            int[] r0 = r0.l
            r4 = r0
            r5 = 4
            r4 = r4[r5]
            r5 = 55
            if (r4 == r5) goto L_0x006e
            r0 = r30
            int[] r0 = r0.l
            r4 = r0
            r5 = 4
            r4 = r4[r5]
            r5 = 57
            if (r4 != r5) goto L_0x007f
        L_0x006e:
            r0 = r30
            int[] r0 = r0.l
            r4 = r0
            r5 = 5
            r4 = r4[r5]
            r5 = 97
            if (r4 != r5) goto L_0x007f
            r4 = 1
        L_0x007b:
            if (r4 != 0) goto L_0x0081
            r3 = 0
            goto L_0x0014
        L_0x007f:
            r4 = 0
            goto L_0x007b
        L_0x0081:
            r4 = 7
            r0 = r30
            r1 = r4
            boolean r4 = r0.a(r1)
            if (r4 != 0) goto L_0x0090
            r4 = 0
        L_0x008c:
            if (r4 != 0) goto L_0x00cd
            r3 = 0
            goto L_0x0014
        L_0x0090:
            r0 = r30
            int[] r0 = r0.l
            r4 = r0
            r5 = 4
            r4 = r4[r5]
            r5 = 2
            r6 = r4 & 7
            int r5 = r5 << r6
            r0 = r5
            r1 = r30
            r1.n = r0
            r5 = 128(0x80, float:1.794E-43)
            boolean r4 = b(r4, r5)
            r0 = r4
            r1 = r30
            r1.o = r0
            r4 = 0
            r0 = r4
            r1 = r30
            r1.p = r0
            r0 = r30
            boolean r0 = r0.o
            r4 = r0
            if (r4 == 0) goto L_0x00cb
            r0 = r30
            int r0 = r0.n
            r4 = r0
            r5 = 1
            r0 = r30
            r1 = r4
            r2 = r5
            boolean r4 = r0.a(r1, r2)
            if (r4 != 0) goto L_0x00cb
            r4 = 0
            goto L_0x008c
        L_0x00cb:
            r4 = 1
            goto L_0x008c
        L_0x00cd:
            r4 = 1
            r0 = r30
            r1 = r4
            boolean r4 = r0.a(r1)
            if (r4 != 0) goto L_0x00da
            r3 = 0
            goto L_0x0014
        L_0x00da:
            r0 = r30
            int[] r0 = r0.l
            r4 = r0
            r5 = 0
            r4 = r4[r5]
            r5 = 59
            if (r4 != r5) goto L_0x00e9
            r3 = 0
            goto L_0x0014
        L_0x00e9:
            r5 = 33
            if (r4 != r5) goto L_0x0168
            r4 = 1
            r0 = r30
            r1 = r4
            boolean r4 = r0.a(r1)
            if (r4 != 0) goto L_0x00fd
            r4 = 0
        L_0x00f8:
            if (r4 != 0) goto L_0x00cd
            r3 = 0
            goto L_0x0014
        L_0x00fd:
            r0 = r30
            int[] r0 = r0.l
            r4 = r0
            r5 = 0
            r4 = r4[r5]
            switch(r4) {
                case 249: goto L_0x0110;
                default: goto L_0x0108;
            }
        L_0x0108:
            int r4 = r30.e()
            if (r4 > 0) goto L_0x0108
            r4 = 1
            goto L_0x00f8
        L_0x0110:
            r0 = r30
            byte[] r0 = r0.d
            r4 = r0
            r0 = r30
            int r0 = r0.c
            r5 = r0
            int r5 = r5 + 2
            byte r4 = r4[r5]
            int r4 = b(r4)
            r0 = r30
            byte[] r0 = r0.d
            r5 = r0
            r0 = r30
            int r0 = r0.c
            r6 = r0
            int r6 = r6 + 3
            byte r5 = r5[r6]
            int r5 = b(r5)
            int r4 = a(r4, r5)
            int r4 = r4 * 10
            r0 = r4
            r1 = r30
            r1.F = r0
            int r4 = r30.e()
            if (r4 >= 0) goto L_0x0147
            r4 = 1
            goto L_0x00f8
        L_0x0147:
            r0 = r30
            int[] r0 = r0.l
            r4 = r0
            r5 = 0
            r4 = r4[r5]
            r4 = r4 & 1
            if (r4 == 0) goto L_0x0161
            r0 = r30
            int[] r0 = r0.l
            r4 = r0
            r5 = 3
            r4 = r4[r5]
            r0 = r4
            r1 = r30
            r1.m = r0
            goto L_0x0108
        L_0x0161:
            r4 = -1
            r0 = r4
            r1 = r30
            r1.m = r0
            goto L_0x0108
        L_0x0168:
            r5 = 44
            if (r4 != r5) goto L_0x00cd
            r4 = 9
            r0 = r30
            r1 = r4
            boolean r4 = r0.a(r1)
            if (r4 != 0) goto L_0x017d
            r4 = 0
        L_0x0178:
            if (r4 != 0) goto L_0x022c
            r3 = 0
            goto L_0x0014
        L_0x017d:
            r0 = r30
            int[] r0 = r0.l
            r4 = r0
            r5 = 0
            r4 = r4[r5]
            r0 = r30
            int[] r0 = r0.l
            r5 = r0
            r6 = 1
            r5 = r5[r6]
            int r4 = a(r4, r5)
            r0 = r4
            r1 = r30
            r1.s = r0
            r0 = r30
            int[] r0 = r0.l
            r4 = r0
            r5 = 2
            r4 = r4[r5]
            r0 = r30
            int[] r0 = r0.l
            r5 = r0
            r6 = 3
            r5 = r5[r6]
            int r4 = a(r4, r5)
            r0 = r4
            r1 = r30
            r1.t = r0
            r0 = r30
            int[] r0 = r0.l
            r4 = r0
            r5 = 4
            r4 = r4[r5]
            r0 = r30
            int[] r0 = r0.l
            r5 = r0
            r6 = 5
            r5 = r5[r6]
            int r4 = a(r4, r5)
            r0 = r4
            r1 = r30
            r1.q = r0
            r0 = r30
            int[] r0 = r0.l
            r4 = r0
            r5 = 6
            r4 = r4[r5]
            r0 = r30
            int[] r0 = r0.l
            r5 = r0
            r6 = 7
            r5 = r5[r6]
            int r4 = a(r4, r5)
            r0 = r4
            r1 = r30
            r1.r = r0
            r0 = r30
            int[] r0 = r0.l
            r4 = r0
            r5 = 8
            r4 = r4[r5]
            r5 = 128(0x80, float:1.794E-43)
            boolean r5 = b(r4, r5)
            r0 = r5
            r1 = r30
            r1.v = r0
            r5 = 2
            r6 = r4 & 7
            int r5 = r5 << r6
            r0 = r5
            r1 = r30
            r1.u = r0
            r5 = 64
            boolean r4 = b(r4, r5)
            r0 = r4
            r1 = r30
            r1.w = r0
            r4 = 0
            r0 = r4
            r1 = r30
            r1.x = r0
            r0 = r30
            boolean r0 = r0.v
            r4 = r0
            if (r4 == 0) goto L_0x0229
            r0 = r30
            int r0 = r0.u
            r4 = r0
            r5 = 0
            r0 = r30
            r1 = r4
            r2 = r5
            boolean r4 = r0.a(r1, r2)
            if (r4 != 0) goto L_0x0229
            r4 = 0
            goto L_0x0178
        L_0x0229:
            r4 = 1
            goto L_0x0178
        L_0x022c:
            r0 = r30
            int r0 = r0.q
            r4 = r0
            r0 = r30
            int r0 = r0.r
            r5 = r0
            r6 = 0
            r7 = 4096(0x1000, float:5.74E-42)
            int[] r7 = new int[r7]
            r8 = 4096(0x1000, float:5.74E-42)
            int[] r8 = new int[r8]
            r9 = 8192(0x2000, float:1.14794E-41)
            int[] r9 = new int[r9]
            r10 = 1
            r0 = r30
            r1 = r10
            boolean r10 = r0.a(r1)
            if (r10 != 0) goto L_0x0264
            r4 = 0
        L_0x024e:
            r0 = r30
            int r0 = r0.k
            r5 = r0
            int r5 = r5 + 1
            r0 = r5
            r1 = r30
            r1.k = r0
            r0 = r30
            int r0 = r0.k
            r5 = r0
            if (r5 < r3) goto L_0x00cd
            r3 = r4
            goto L_0x0014
        L_0x0264:
            r0 = r30
            int[] r0 = r0.l
            r10 = r0
            r11 = 0
            r10 = r10[r11]
            r0 = r30
            int r0 = r0.q
            r11 = r0
            r0 = r30
            int r0 = r0.r
            r12 = r0
            int r11 = r11 * r12
            int[] r11 = new int[r11]
            r0 = r30
            int[] r0 = r0.p
            r12 = r0
            r0 = r30
            boolean r0 = r0.v
            r13 = r0
            if (r13 == 0) goto L_0x028a
            r0 = r30
            int[] r0 = r0.x
            r12 = r0
        L_0x028a:
            r0 = r30
            int r0 = r0.m
            r13 = r0
            if (r13 < 0) goto L_0x029b
            r0 = r30
            int r0 = r0.m
            r13 = r0
            r14 = 16777215(0xffffff, float:2.3509886E-38)
            r12[r13] = r14
        L_0x029b:
            r13 = 1
            int r13 = r13 << r10
            int r14 = r13 + 1
            int r15 = r10 + 1
            int r16 = r13 + 2
            r17 = -1
            r18 = -1
            r19 = 0
        L_0x02a9:
            r0 = r19
            r1 = r13
            if (r0 < r1) goto L_0x0318
            r19 = 0
            r20 = 0
            r0 = r20
            r1 = r30
            r1.C = r0
            r20 = 0
            r0 = r20
            r1 = r30
            r1.y = r0
            r20 = 0
            r0 = r20
            r2 = r30
            r2.B = r0
            r20 = 0
            r0 = r20
            r1 = r30
            r1.z = r0
            r20 = -1
            r0 = r20
            r1 = r30
            r1.A = r0
            r20 = 0
            r21 = 0
            r27 = r21
            r21 = r20
            r20 = r6
            r6 = r27
            r28 = r16
            r16 = r18
            r18 = r28
            r29 = r19
            r19 = r15
            r15 = r29
        L_0x02f0:
            if (r6 < r5) goto L_0x031d
            com.adchina.android.ads.e r4 = new com.adchina.android.ads.e
            r0 = r30
            int r0 = r0.q
            r5 = r0
            r0 = r30
            int r0 = r0.r
            r6 = r0
            android.graphics.Bitmap$Config r7 = android.graphics.Bitmap.Config.ARGB_8888
            android.graphics.Bitmap r5 = android.graphics.Bitmap.createBitmap(r11, r5, r6, r7)
            r0 = r30
            int r0 = r0.s
            r6 = r0
            r0 = r30
            int r0 = r0.t
            r7 = r0
            r0 = r30
            int r0 = r0.F
            r8 = r0
            r4.<init>(r5, r6, r7, r8)
            goto L_0x024e
        L_0x0318:
            r8[r19] = r19
            int r19 = r19 + 1
            goto L_0x02a9
        L_0x031d:
            r22 = 0
            r27 = r22
            r22 = r19
            r19 = r18
            r18 = r17
            r17 = r16
            r16 = r15
            r15 = r27
        L_0x032d:
            if (r15 >= r4) goto L_0x05ee
            if (r16 != 0) goto L_0x04f4
        L_0x0331:
            r0 = r30
            int r0 = r0.C
            r23 = r0
            r0 = r23
            r1 = r22
            if (r0 < r1) goto L_0x0396
        L_0x033d:
            r0 = r30
            long r0 = r0.B
            r23 = r0
            r0 = r23
            int r0 = (int) r0
            r23 = r0
            r24 = 1
            int r24 = r24 << r22
            r25 = 1
            int r24 = r24 - r25
            r23 = r23 & r24
            r0 = r30
            long r0 = r0.B
            r24 = r0
            long r24 = r24 >> r22
            r0 = r24
            r2 = r30
            r2.B = r0
            r0 = r30
            int r0 = r0.C
            r24 = r0
            int r24 = r24 - r22
            r0 = r24
            r1 = r30
            r1.C = r0
        L_0x036e:
            if (r23 >= 0) goto L_0x0424
            com.adchina.android.ads.e r4 = new com.adchina.android.ads.e
            r0 = r30
            int r0 = r0.q
            r5 = r0
            r0 = r30
            int r0 = r0.r
            r6 = r0
            android.graphics.Bitmap$Config r7 = android.graphics.Bitmap.Config.ARGB_8888
            android.graphics.Bitmap r5 = android.graphics.Bitmap.createBitmap(r11, r5, r6, r7)
            r0 = r30
            int r0 = r0.s
            r6 = r0
            r0 = r30
            int r0 = r0.t
            r7 = r0
            r0 = r30
            int r0 = r0.F
            r8 = r0
            r4.<init>(r5, r6, r7, r8)
            goto L_0x024e
        L_0x0396:
            r0 = r30
            boolean r0 = r0.z
            r23 = r0
            if (r23 == 0) goto L_0x03a1
            r23 = -1
            goto L_0x036e
        L_0x03a1:
            r0 = r30
            int r0 = r0.y
            r23 = r0
            if (r23 != 0) goto L_0x03cd
            int r23 = r30.e()
            r0 = r23
            r1 = r30
            r1.y = r0
            r23 = 0
            r0 = r23
            r1 = r30
            r1.A = r0
            r0 = r30
            int r0 = r0.y
            r23 = r0
            if (r23 > 0) goto L_0x03cd
            r23 = 1
            r0 = r23
            r1 = r30
            r1.z = r0
            goto L_0x033d
        L_0x03cd:
            r0 = r30
            long r0 = r0.B
            r23 = r0
            r0 = r30
            int[] r0 = r0.l
            r25 = r0
            r0 = r30
            int r0 = r0.A
            r26 = r0
            r25 = r25[r26]
            r0 = r30
            int r0 = r0.C
            r26 = r0
            int r25 = r25 << r26
            r0 = r25
            long r0 = (long) r0
            r25 = r0
            long r23 = r23 + r25
            r0 = r23
            r2 = r30
            r2.B = r0
            r0 = r30
            int r0 = r0.A
            r23 = r0
            int r23 = r23 + 1
            r0 = r23
            r1 = r30
            r1.A = r0
            r0 = r30
            int r0 = r0.C
            r23 = r0
            int r23 = r23 + 8
            r0 = r23
            r1 = r30
            r1.C = r0
            r0 = r30
            int r0 = r0.y
            r23 = r0
            r24 = 1
            int r23 = r23 - r24
            r0 = r23
            r1 = r30
            r1.y = r0
            goto L_0x0331
        L_0x0424:
            r0 = r23
            r1 = r19
            if (r0 > r1) goto L_0x042f
            r0 = r23
            r1 = r14
            if (r0 != r1) goto L_0x0455
        L_0x042f:
            com.adchina.android.ads.e r4 = new com.adchina.android.ads.e
            r0 = r30
            int r0 = r0.q
            r5 = r0
            r0 = r30
            int r0 = r0.r
            r6 = r0
            android.graphics.Bitmap$Config r7 = android.graphics.Bitmap.Config.ARGB_8888
            android.graphics.Bitmap r5 = android.graphics.Bitmap.createBitmap(r11, r5, r6, r7)
            r0 = r30
            int r0 = r0.s
            r6 = r0
            r0 = r30
            int r0 = r0.t
            r7 = r0
            r0 = r30
            int r0 = r0.F
            r8 = r0
            r4.<init>(r5, r6, r7, r8)
            goto L_0x024e
        L_0x0455:
            r0 = r23
            r1 = r13
            if (r0 != r1) goto L_0x0468
            int r18 = r10 + 1
            int r19 = r13 + 2
            r22 = -1
            r27 = r22
            r22 = r18
            r18 = r27
            goto L_0x032d
        L_0x0468:
            r24 = -1
            r0 = r18
            r1 = r24
            if (r0 != r1) goto L_0x047e
            int r17 = r16 + 1
            r18 = r8[r23]
            r9[r16] = r18
            r16 = r17
            r18 = r23
            r17 = r23
            goto L_0x032d
        L_0x047e:
            r0 = r23
            r1 = r19
            if (r0 != r1) goto L_0x0658
            int r24 = r16 + 1
            r9[r16] = r17
            r16 = r18
            r17 = r24
        L_0x048c:
            r0 = r16
            r1 = r13
            if (r0 > r1) goto L_0x04c1
            r16 = r8[r16]
            r24 = 4096(0x1000, float:5.74E-42)
            r0 = r19
            r1 = r24
            if (r0 < r1) goto L_0x04cc
            com.adchina.android.ads.e r4 = new com.adchina.android.ads.e
            r0 = r30
            int r0 = r0.q
            r5 = r0
            r0 = r30
            int r0 = r0.r
            r6 = r0
            android.graphics.Bitmap$Config r7 = android.graphics.Bitmap.Config.ARGB_8888
            android.graphics.Bitmap r5 = android.graphics.Bitmap.createBitmap(r11, r5, r6, r7)
            r0 = r30
            int r0 = r0.s
            r6 = r0
            r0 = r30
            int r0 = r0.t
            r7 = r0
            r0 = r30
            int r0 = r0.F
            r8 = r0
            r4.<init>(r5, r6, r7, r8)
            goto L_0x024e
        L_0x04c1:
            int r24 = r17 + 1
            r25 = r8[r16]
            r9[r17] = r25
            r16 = r7[r16]
            r17 = r24
            goto L_0x048c
        L_0x04cc:
            int r24 = r17 + 1
            r9[r17] = r16
            r7[r19] = r18
            r8[r19] = r16
            int r17 = r19 + 1
            r18 = 1
            int r18 = r18 << r22
            r0 = r17
            r1 = r18
            if (r0 < r1) goto L_0x0654
            r18 = 4096(0x1000, float:5.74E-42)
            r0 = r17
            r1 = r18
            if (r0 >= r1) goto L_0x0654
            int r18 = r22 + 1
        L_0x04ea:
            r19 = r17
            r22 = r18
            r18 = r23
            r17 = r16
            r16 = r24
        L_0x04f4:
            int r16 = r16 + -1
            r23 = r9[r16]
            if (r23 >= 0) goto L_0x0520
            com.adchina.android.ads.e r4 = new com.adchina.android.ads.e
            r0 = r30
            int r0 = r0.q
            r5 = r0
            r0 = r30
            int r0 = r0.r
            r6 = r0
            android.graphics.Bitmap$Config r7 = android.graphics.Bitmap.Config.ARGB_8888
            android.graphics.Bitmap r5 = android.graphics.Bitmap.createBitmap(r11, r5, r6, r7)
            r0 = r30
            int r0 = r0.s
            r6 = r0
            r0 = r30
            int r0 = r0.t
            r7 = r0
            r0 = r30
            int r0 = r0.F
            r8 = r0
            r4.<init>(r5, r6, r7, r8)
            goto L_0x024e
        L_0x0520:
            if (r15 != 0) goto L_0x053e
            r24 = 0
            r0 = r24
            r1 = r30
            r1.f = r0
            r23 = r12[r23]
            r0 = r23
            r1 = r30
            r1.e = r0
            r23 = 0
            r0 = r23
            r1 = r30
            r1.g = r0
        L_0x053a:
            int r15 = r15 + 1
            goto L_0x032d
        L_0x053e:
            r0 = r30
            int r0 = r0.e
            r24 = r0
            r25 = r12[r23]
            r0 = r24
            r1 = r25
            if (r0 == r1) goto L_0x05a8
            r0 = r30
            int r0 = r0.g
            r24 = r0
        L_0x0552:
            r0 = r30
            int r0 = r0.g
            r25 = r0
            r0 = r30
            int r0 = r0.f
            r26 = r0
            int r25 = r25 + r26
            r0 = r24
            r1 = r25
            if (r0 <= r1) goto L_0x0593
            r24 = 0
            r0 = r24
            r1 = r30
            r1.f = r0
            r24 = r12[r23]
            r0 = r24
            r1 = r30
            r1.e = r0
            r0 = r15
            r1 = r30
            r1.g = r0
            r24 = 1
            int r24 = r4 - r24
            r0 = r15
            r1 = r24
            if (r0 != r1) goto L_0x053a
            r0 = r30
            int r0 = r0.q
            r24 = r0
            int r24 = r24 * r21
            int r24 = r24 + r15
            r23 = r12[r23]
            r11[r24] = r23
            goto L_0x053a
        L_0x0593:
            r0 = r30
            int r0 = r0.q
            r25 = r0
            int r25 = r25 * r21
            int r25 = r25 + r24
            r0 = r30
            int r0 = r0.e
            r26 = r0
            r11[r25] = r26
            int r24 = r24 + 1
            goto L_0x0552
        L_0x05a8:
            r0 = r30
            int r0 = r0.f
            r23 = r0
            int r23 = r23 + 1
            r0 = r23
            r1 = r30
            r1.f = r0
            r23 = 1
            int r23 = r4 - r23
            r0 = r15
            r1 = r23
            if (r0 != r1) goto L_0x053a
            r0 = r30
            int r0 = r0.g
            r23 = r0
        L_0x05c5:
            r0 = r30
            int r0 = r0.g
            r24 = r0
            r0 = r30
            int r0 = r0.f
            r25 = r0
            int r24 = r24 + r25
            r0 = r23
            r1 = r24
            if (r0 > r1) goto L_0x053a
            r0 = r30
            int r0 = r0.q
            r24 = r0
            int r24 = r24 * r21
            int r24 = r24 + r23
            r0 = r30
            int r0 = r0.e
            r25 = r0
            r11[r24] = r25
            int r23 = r23 + 1
            goto L_0x05c5
        L_0x05ee:
            r0 = r30
            boolean r0 = r0.w
            r15 = r0
            if (r15 == 0) goto L_0x063a
            int[] r15 = com.adchina.android.ads.p.D
            r15 = r15[r20]
            int r15 = r15 + r21
            r27 = r20
            r20 = r15
            r15 = r27
        L_0x0601:
            r0 = r20
            r1 = r5
            if (r0 < r1) goto L_0x0642
            int r15 = r15 + 1
            r20 = 3
            r0 = r15
            r1 = r20
            if (r0 <= r1) goto L_0x0635
            com.adchina.android.ads.e r4 = new com.adchina.android.ads.e
            r0 = r30
            int r0 = r0.q
            r5 = r0
            r0 = r30
            int r0 = r0.r
            r6 = r0
            android.graphics.Bitmap$Config r7 = android.graphics.Bitmap.Config.ARGB_8888
            android.graphics.Bitmap r5 = android.graphics.Bitmap.createBitmap(r11, r5, r6, r7)
            r0 = r30
            int r0 = r0.s
            r6 = r0
            r0 = r30
            int r0 = r0.t
            r7 = r0
            r0 = r30
            int r0 = r0.F
            r8 = r0
            r4.<init>(r5, r6, r7, r8)
            goto L_0x024e
        L_0x0635:
            int[] r20 = com.adchina.android.ads.p.E
            r20 = r20[r15]
            goto L_0x0601
        L_0x063a:
            int r15 = r21 + 1
            r27 = r20
            r20 = r15
            r15 = r27
        L_0x0642:
            int r6 = r6 + 1
            r21 = r20
            r20 = r15
            r15 = r16
            r16 = r17
            r17 = r18
            r18 = r19
            r19 = r22
            goto L_0x02f0
        L_0x0654:
            r18 = r22
            goto L_0x04ea
        L_0x0658:
            r17 = r16
            r16 = r23
            goto L_0x048c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adchina.android.ads.p.c():com.adchina.android.ads.e");
    }

    public final void d() {
        this.d = null;
        this.l = null;
        this.p = null;
        this.x = null;
    }
}
