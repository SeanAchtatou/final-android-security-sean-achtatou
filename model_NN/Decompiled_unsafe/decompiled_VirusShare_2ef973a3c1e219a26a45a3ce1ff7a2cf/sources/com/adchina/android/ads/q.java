package com.adchina.android.ads;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.util.FloatMath;

public final class q implements SensorEventListener {
    public static boolean a = false;
    public static final Object b = new Object();
    private static boolean c = false;
    private float d;
    private float e;
    private float f;
    private float g;
    private float h;
    private float i;
    private long j;
    private r k;

    public final void onAccuracyChanged(Sensor sensor, int i2) {
    }

    public final void onSensorChanged(SensorEvent sensorEvent) {
        if (!a) {
            synchronized (this) {
                long currentTimeMillis = System.currentTimeMillis();
                if (currentTimeMillis - this.j > 100) {
                    long j2 = currentTimeMillis - this.j;
                    this.j = currentTimeMillis;
                    this.d = sensorEvent.values[0];
                    this.e = sensorEvent.values[1];
                    this.f = sensorEvent.values[2];
                    float f2 = this.d - this.g;
                    float f3 = this.e - this.h;
                    float f4 = this.f - this.i;
                    float abs = Math.abs(this.d);
                    float abs2 = Math.abs(this.e);
                    float abs3 = Math.abs(this.f);
                    int i2 = (abs <= abs2 || abs <= abs3) ? (abs2 <= abs || abs2 <= abs3) ? (abs3 <= abs || abs3 <= abs2) ? -1 : this.f > 0.0f ? 0 : 1 : this.e > 0.0f ? 8 : 16 : this.d > 0.0f ? 4 : 2;
                    float sqrt = (FloatMath.sqrt(((f2 * f2) + (f3 * f3)) + (f4 * f4)) / ((float) j2)) * 10000.0f;
                    if (sqrt > 800.0f) {
                        s sVar = new s(this, this.g, this.h, this.i);
                        s sVar2 = new s(this, this.d, this.e, this.f);
                        synchronized (b) {
                            if (!a) {
                                a = true;
                                if (this.k != null) {
                                    this.k.onMoving(sVar, sVar2, sqrt, i2);
                                }
                                a = false;
                            }
                        }
                        this.j = currentTimeMillis;
                    }
                    this.g = this.d;
                    this.h = this.e;
                    this.i = this.f;
                }
            }
        }
    }
}
