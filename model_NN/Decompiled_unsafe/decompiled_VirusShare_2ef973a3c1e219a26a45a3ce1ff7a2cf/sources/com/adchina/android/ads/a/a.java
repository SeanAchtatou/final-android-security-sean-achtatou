package com.adchina.android.ads.a;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.os.Message;
import android.util.Log;
import android.view.MotionEvent;
import com.adchina.android.ads.AdEngine;
import com.adchina.android.ads.AdManager;
import com.adchina.android.ads.Utils;
import com.adchina.android.ads.f;
import com.adchina.android.ads.g;
import com.adchina.android.ads.k;
import com.adchina.android.ads.l;
import com.adchina.android.ads.o;
import com.adchina.android.ads.t;
import com.adchina.android.ads.views.AdView;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;

public final class a extends b {
    private static /* synthetic */ int[] ah;
    private static /* synthetic */ int[] ai;
    private long aa = 0;
    private LinkedList ab = new LinkedList();
    private HashMap ac = new HashMap();
    private int ad = 0;
    private int ae = 0;
    private boolean af = true;
    private boolean ag = false;

    public a(Context context) {
        super(context);
    }

    private void a(int i, int i2) {
        this.ad = i;
        this.ae = i2;
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x0059 A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00a4 A[Catch:{ all -> 0x00a8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x00ad A[SYNTHETIC, Splitter:B:43:0x00ad] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.io.InputStream d(java.lang.String r11) {
        /*
            r10 = this;
            r8 = 0
            r7 = 1
            r6 = 0
            com.adchina.android.ads.h r0 = r10.Z     // Catch:{ Exception -> 0x00fa, all -> 0x00df }
            r1 = 2
            java.lang.Object[] r1 = new java.lang.Object[r1]     // Catch:{ Exception -> 0x00fa, all -> 0x00df }
            r2 = 0
            java.lang.String r3 = "++ start to download Banner Img file"
            r1[r2] = r3     // Catch:{ Exception -> 0x00fa, all -> 0x00df }
            r2 = 1
            r1[r2] = r11     // Catch:{ Exception -> 0x00fa, all -> 0x00df }
            java.lang.String r1 = com.adchina.android.ads.Utils.concatString(r1)     // Catch:{ Exception -> 0x00fa, all -> 0x00df }
            r0.c(r1)     // Catch:{ Exception -> 0x00fa, all -> 0x00df }
            java.io.File r0 = new java.io.File     // Catch:{ Exception -> 0x00fa, all -> 0x00df }
            java.lang.String r1 = com.adchina.android.ads.k.d     // Catch:{ Exception -> 0x00fa, all -> 0x00df }
            r0.<init>(r1)     // Catch:{ Exception -> 0x00fa, all -> 0x00df }
            boolean r1 = r0.exists()     // Catch:{ Exception -> 0x00fa, all -> 0x00df }
            if (r1 != 0) goto L_0x0027
            r0.mkdirs()     // Catch:{ Exception -> 0x00fa, all -> 0x00df }
        L_0x0027:
            com.adchina.android.ads.t r1 = r10.W     // Catch:{ Exception -> 0x00fa, all -> 0x00df }
            java.io.InputStream r1 = com.adchina.android.ads.t.b(r11)     // Catch:{ Exception -> 0x00fa, all -> 0x00df }
            if (r1 != 0) goto L_0x0062
            java.lang.Exception r0 = new java.lang.Exception     // Catch:{ Exception -> 0x0037, all -> 0x00ec }
            java.lang.String r2 = "image address is not exists"
            r0.<init>(r2)     // Catch:{ Exception -> 0x0037, all -> 0x00ec }
            throw r0     // Catch:{ Exception -> 0x0037, all -> 0x00ec }
        L_0x0037:
            r0 = move-exception
            r2 = r1
            r1 = r6
        L_0x003a:
            r3 = 2
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ all -> 0x00f8 }
            r4 = 0
            java.lang.String r5 = "Failed to download Banner Img file, err = "
            r3[r4] = r5     // Catch:{ all -> 0x00f8 }
            r4 = 1
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x00f8 }
            r3[r4] = r0     // Catch:{ all -> 0x00f8 }
            java.lang.String r0 = com.adchina.android.ads.Utils.concatString(r3)     // Catch:{ all -> 0x00f8 }
            com.adchina.android.ads.h r3 = r10.Z     // Catch:{ all -> 0x00f8 }
            r3.c(r0)     // Catch:{ all -> 0x00f8 }
            java.lang.String r3 = "AdChinaError"
            android.util.Log.e(r3, r0)     // Catch:{ all -> 0x00f8 }
            if (r1 != 0) goto L_0x005b
            if (r2 != 0) goto L_0x011a
        L_0x005b:
            com.adchina.android.ads.t r0 = r10.W
            java.io.InputStream r0 = com.adchina.android.ads.t.b(r11)
        L_0x0061:
            return r0
        L_0x0062:
            java.io.File r2 = new java.io.File     // Catch:{ Exception -> 0x0037, all -> 0x00ec }
            r3 = 2
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ Exception -> 0x0037, all -> 0x00ec }
            r4 = 0
            java.lang.String r5 = "yyyyMMddHHmmss"
            java.lang.String r5 = com.adchina.android.ads.Utils.getNowTime(r5)     // Catch:{ Exception -> 0x0037, all -> 0x00ec }
            r3[r4] = r5     // Catch:{ Exception -> 0x0037, all -> 0x00ec }
            r4 = 1
            java.lang.String r5 = "bnImg.tmp"
            r3[r4] = r5     // Catch:{ Exception -> 0x0037, all -> 0x00ec }
            java.lang.String r3 = com.adchina.android.ads.Utils.concatString(r3)     // Catch:{ Exception -> 0x0037, all -> 0x00ec }
            r2.<init>(r0, r3)     // Catch:{ Exception -> 0x0037, all -> 0x00ec }
            boolean r0 = r2.exists()     // Catch:{ Exception -> 0x0037, all -> 0x00ec }
            if (r0 == 0) goto L_0x0085
            r2.delete()     // Catch:{ Exception -> 0x0037, all -> 0x00ec }
        L_0x0085:
            java.io.FileOutputStream r0 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x009b, all -> 0x00ff }
            r0.<init>(r2)     // Catch:{ Exception -> 0x009b, all -> 0x00ff }
            r3 = 16384(0x4000, float:2.2959E-41)
            byte[] r3 = new byte[r3]     // Catch:{ Exception -> 0x010f, all -> 0x0103 }
            r4 = r6
        L_0x008f:
            int r5 = r1.read(r3)     // Catch:{ Exception -> 0x0115, all -> 0x0109 }
            if (r5 <= 0) goto L_0x00ba
            r6 = 0
            r0.write(r3, r6, r5)     // Catch:{ Exception -> 0x0115, all -> 0x0109 }
            r4 = r7
            goto L_0x008f
        L_0x009b:
            r0 = move-exception
            r3 = r8
            r4 = r6
        L_0x009e:
            boolean r5 = r2.exists()     // Catch:{ all -> 0x00a8 }
            if (r5 == 0) goto L_0x00a7
            r2.delete()     // Catch:{ all -> 0x00a8 }
        L_0x00a7:
            throw r0     // Catch:{ all -> 0x00a8 }
        L_0x00a8:
            r0 = move-exception
            r2 = r3
            r3 = r4
        L_0x00ab:
            if (r3 == 0) goto L_0x00b2
            com.adchina.android.ads.t r4 = r10.W     // Catch:{ Exception -> 0x00b6, all -> 0x00f0 }
            com.adchina.android.ads.t.a(r1)     // Catch:{ Exception -> 0x00b6, all -> 0x00f0 }
        L_0x00b2:
            com.adchina.android.ads.Utils.closeStream(r2)     // Catch:{ Exception -> 0x00b6, all -> 0x00f0 }
            throw r0     // Catch:{ Exception -> 0x00b6, all -> 0x00f0 }
        L_0x00b6:
            r0 = move-exception
            r2 = r1
            r1 = r3
            goto L_0x003a
        L_0x00ba:
            if (r4 == 0) goto L_0x00c1
            com.adchina.android.ads.t r3 = r10.W     // Catch:{ Exception -> 0x00da, all -> 0x00f4 }
            com.adchina.android.ads.t.a(r1)     // Catch:{ Exception -> 0x00da, all -> 0x00f4 }
        L_0x00c1:
            com.adchina.android.ads.Utils.closeStream(r0)     // Catch:{ Exception -> 0x00da, all -> 0x00f4 }
            java.util.HashMap r0 = r10.ac     // Catch:{ Exception -> 0x00da, all -> 0x00f4 }
            java.lang.String r3 = r2.getAbsolutePath()     // Catch:{ Exception -> 0x00da, all -> 0x00f4 }
            r0.put(r11, r3)     // Catch:{ Exception -> 0x00da, all -> 0x00f4 }
            r10.o()     // Catch:{ Exception -> 0x00da, all -> 0x00f4 }
            java.io.FileInputStream r0 = new java.io.FileInputStream     // Catch:{ Exception -> 0x00da, all -> 0x00f4 }
            java.lang.String r2 = r2.getAbsolutePath()     // Catch:{ Exception -> 0x00da, all -> 0x00f4 }
            r0.<init>(r2)     // Catch:{ Exception -> 0x00da, all -> 0x00f4 }
            goto L_0x0061
        L_0x00da:
            r0 = move-exception
            r2 = r1
            r1 = r4
            goto L_0x003a
        L_0x00df:
            r0 = move-exception
            r1 = r6
            r2 = r8
        L_0x00e2:
            if (r1 != 0) goto L_0x00e6
            if (r2 != 0) goto L_0x00eb
        L_0x00e6:
            com.adchina.android.ads.t r1 = r10.W
            com.adchina.android.ads.t.b(r11)
        L_0x00eb:
            throw r0
        L_0x00ec:
            r0 = move-exception
            r2 = r1
            r1 = r6
            goto L_0x00e2
        L_0x00f0:
            r0 = move-exception
            r2 = r1
            r1 = r3
            goto L_0x00e2
        L_0x00f4:
            r0 = move-exception
            r2 = r1
            r1 = r4
            goto L_0x00e2
        L_0x00f8:
            r0 = move-exception
            goto L_0x00e2
        L_0x00fa:
            r0 = move-exception
            r1 = r6
            r2 = r8
            goto L_0x003a
        L_0x00ff:
            r0 = move-exception
            r2 = r8
            r3 = r6
            goto L_0x00ab
        L_0x0103:
            r2 = move-exception
            r3 = r6
            r9 = r0
            r0 = r2
            r2 = r9
            goto L_0x00ab
        L_0x0109:
            r2 = move-exception
            r3 = r4
            r9 = r0
            r0 = r2
            r2 = r9
            goto L_0x00ab
        L_0x010f:
            r3 = move-exception
            r4 = r6
            r9 = r0
            r0 = r3
            r3 = r9
            goto L_0x009e
        L_0x0115:
            r3 = move-exception
            r9 = r3
            r3 = r0
            r0 = r9
            goto L_0x009e
        L_0x011a:
            r0 = r2
            goto L_0x0061
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adchina.android.ads.a.a.d(java.lang.String):java.io.InputStream");
    }

    private void m() {
        InputStream inputStream;
        this.Z.c("++ onGetImgMaterial");
        a(g.EIdle);
        FileInputStream fileInputStream = null;
        try {
            String stringBuffer = this.n.toString();
            if (l.EAdNONE == this.b || l.EAdTXT == this.b) {
                inputStream = null;
            } else if (!this.ac.containsKey(stringBuffer)) {
                inputStream = d(stringBuffer);
                try {
                    this.Z.c("++ load remote banner img");
                } catch (Exception e) {
                    Exception exc = e;
                    fileInputStream = inputStream;
                    e = exc;
                    try {
                        String concatString = Utils.concatString("Failed to get ad material, err = ", e.toString());
                        a(2, concatString);
                        this.Z.c(concatString);
                        Log.e("AdChinaError", concatString);
                        t tVar = this.W;
                        t.a(fileInputStream);
                        this.Z.c(Utils.concatString("-- onGetImgMaterial, AdModel = ", this.b));
                    } catch (Throwable th) {
                        th = th;
                        t tVar2 = this.W;
                        t.a(fileInputStream);
                        this.Z.c(Utils.concatString("-- onGetImgMaterial, AdModel = ", this.b));
                        throw th;
                    }
                } catch (Throwable th2) {
                    Throwable th3 = th2;
                    fileInputStream = inputStream;
                    th = th3;
                    t tVar22 = this.W;
                    t.a(fileInputStream);
                    this.Z.c(Utils.concatString("-- onGetImgMaterial, AdModel = ", this.b));
                    throw th;
                }
            } else {
                FileInputStream fileInputStream2 = new FileInputStream((String) this.ac.get(stringBuffer));
                try {
                    this.Z.c("++ load local banner img");
                    inputStream = fileInputStream2;
                } catch (Exception e2) {
                    e = e2;
                    fileInputStream = fileInputStream2;
                    String concatString2 = Utils.concatString("Failed to get ad material, err = ", e.toString());
                    a(2, concatString2);
                    this.Z.c(concatString2);
                    Log.e("AdChinaError", concatString2);
                    t tVar3 = this.W;
                    t.a(fileInputStream);
                    this.Z.c(Utils.concatString("-- onGetImgMaterial, AdModel = ", this.b));
                } catch (Throwable th4) {
                    th = th4;
                    fileInputStream = fileInputStream2;
                    t tVar222 = this.W;
                    t.a(fileInputStream);
                    this.Z.c(Utils.concatString("-- onGetImgMaterial, AdModel = ", this.b));
                    throw th;
                }
            }
            if (inputStream == null) {
                try {
                    throw new Exception("image is not exists");
                } catch (Exception e3) {
                    Exception exc2 = e3;
                    fileInputStream = inputStream;
                    e = exc2;
                } catch (Throwable th5) {
                    Throwable th6 = th5;
                    fileInputStream = inputStream;
                    th = th6;
                    t tVar2222 = this.W;
                    t.a(fileInputStream);
                    this.Z.c(Utils.concatString("-- onGetImgMaterial, AdModel = ", this.b));
                    throw th;
                }
            } else {
                if (l.EAdJPG == this.b) {
                    Bitmap convertStreamToBitmap = Utils.convertStreamToBitmap(inputStream);
                    if (convertStreamToBitmap != null) {
                        a(convertStreamToBitmap.getWidth(), convertStreamToBitmap.getHeight());
                        a(1, convertStreamToBitmap);
                    } else {
                        a(2, "JPG AdMaterial is null");
                    }
                } else if (l.EAdPNG == this.b) {
                    Bitmap convertStreamToBitmap2 = Utils.convertStreamToBitmap(inputStream);
                    if (convertStreamToBitmap2 != null) {
                        a(convertStreamToBitmap2.getWidth(), convertStreamToBitmap2.getHeight());
                        a(1, convertStreamToBitmap2);
                    } else {
                        a(2, "PNG AdMaterial is null");
                    }
                } else if (l.EAdGIF == this.b) {
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    while (true) {
                        int read = inputStream.read();
                        if (read == -1) {
                            break;
                        }
                        byteArrayOutputStream.write(read);
                    }
                    byte[] byteArray = byteArrayOutputStream.toByteArray();
                    if (byteArray != null) {
                        o a = o.a(byteArray);
                        a(a.b(), a.c());
                        a(1, a);
                    }
                    byteArrayOutputStream.close();
                }
                t tVar4 = this.W;
                t.a(inputStream);
                this.Z.c(Utils.concatString("-- onGetImgMaterial, AdModel = ", this.b));
            }
        } catch (Exception e4) {
            e = e4;
            String concatString22 = Utils.concatString("Failed to get ad material, err = ", e.toString());
            a(2, concatString22);
            this.Z.c(concatString22);
            Log.e("AdChinaError", concatString22);
            t tVar32 = this.W;
            t.a(fileInputStream);
            this.Z.c(Utils.concatString("-- onGetImgMaterial, AdModel = ", this.b));
        }
    }

    private void n() {
        File file = new File(k.d);
        if (file.exists()) {
            for (File file2 : file.listFiles()) {
                if (!this.ac.containsValue(file2.getAbsolutePath())) {
                    file2.delete();
                }
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:29:0x007f A[SYNTHETIC, Splitter:B:29:0x007f] */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0084 A[Catch:{ IOException -> 0x008a }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void o() {
        /*
            r8 = this;
            r3 = 0
            com.adchina.android.ads.h r0 = r8.Z
            java.lang.String r1 = "saveLocalBannerImgList"
            r0.c(r1)
            android.content.Context r0 = r8.J     // Catch:{ Exception -> 0x0097, all -> 0x007a }
            java.lang.String r1 = "adchinaBNImgs.fc"
            r2 = 0
            java.io.FileOutputStream r1 = r0.openFileOutput(r1, r2)     // Catch:{ Exception -> 0x0097, all -> 0x007a }
            java.io.OutputStreamWriter r2 = new java.io.OutputStreamWriter     // Catch:{ Exception -> 0x009b, all -> 0x008c }
            r2.<init>(r1)     // Catch:{ Exception -> 0x009b, all -> 0x008c }
            java.util.HashMap r0 = r8.ac     // Catch:{ Exception -> 0x0051, all -> 0x0090 }
            java.util.Set r0 = r0.keySet()     // Catch:{ Exception -> 0x0051, all -> 0x0090 }
            java.util.Iterator r3 = r0.iterator()     // Catch:{ Exception -> 0x0051, all -> 0x0090 }
        L_0x0020:
            boolean r0 = r3.hasNext()     // Catch:{ Exception -> 0x0051, all -> 0x0090 }
            if (r0 != 0) goto L_0x0032
            r2.flush()     // Catch:{ Exception -> 0x0051, all -> 0x0090 }
            r2.close()     // Catch:{ IOException -> 0x0088 }
            if (r1 == 0) goto L_0x0031
            r1.close()     // Catch:{ IOException -> 0x0088 }
        L_0x0031:
            return
        L_0x0032:
            java.lang.Object r0 = r3.next()     // Catch:{ Exception -> 0x0051, all -> 0x0090 }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ Exception -> 0x0051, all -> 0x0090 }
            r2.write(r0)     // Catch:{ Exception -> 0x0051, all -> 0x0090 }
            java.lang.String r4 = "|||"
            r2.write(r4)     // Catch:{ Exception -> 0x0051, all -> 0x0090 }
            java.util.HashMap r4 = r8.ac     // Catch:{ Exception -> 0x0051, all -> 0x0090 }
            java.lang.Object r0 = r4.get(r0)     // Catch:{ Exception -> 0x0051, all -> 0x0090 }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ Exception -> 0x0051, all -> 0x0090 }
            r2.write(r0)     // Catch:{ Exception -> 0x0051, all -> 0x0090 }
            java.lang.String r0 = "\n"
            r2.write(r0)     // Catch:{ Exception -> 0x0051, all -> 0x0090 }
            goto L_0x0020
        L_0x0051:
            r0 = move-exception
            r7 = r2
            r2 = r1
            r1 = r7
        L_0x0055:
            com.adchina.android.ads.h r3 = r8.Z     // Catch:{ all -> 0x0095 }
            r4 = 2
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ all -> 0x0095 }
            r5 = 0
            java.lang.String r6 = "Exceptions in saveLocalBannerImgList, err = "
            r4[r5] = r6     // Catch:{ all -> 0x0095 }
            r5 = 1
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0095 }
            r4[r5] = r0     // Catch:{ all -> 0x0095 }
            java.lang.String r0 = com.adchina.android.ads.Utils.concatString(r4)     // Catch:{ all -> 0x0095 }
            r3.c(r0)     // Catch:{ all -> 0x0095 }
            if (r1 == 0) goto L_0x0072
            r1.close()     // Catch:{ IOException -> 0x0078 }
        L_0x0072:
            if (r2 == 0) goto L_0x0031
            r2.close()     // Catch:{ IOException -> 0x0078 }
            goto L_0x0031
        L_0x0078:
            r0 = move-exception
            goto L_0x0031
        L_0x007a:
            r0 = move-exception
            r1 = r3
            r2 = r3
        L_0x007d:
            if (r1 == 0) goto L_0x0082
            r1.close()     // Catch:{ IOException -> 0x008a }
        L_0x0082:
            if (r2 == 0) goto L_0x0087
            r2.close()     // Catch:{ IOException -> 0x008a }
        L_0x0087:
            throw r0
        L_0x0088:
            r0 = move-exception
            goto L_0x0031
        L_0x008a:
            r1 = move-exception
            goto L_0x0087
        L_0x008c:
            r0 = move-exception
            r2 = r1
            r1 = r3
            goto L_0x007d
        L_0x0090:
            r0 = move-exception
            r7 = r2
            r2 = r1
            r1 = r7
            goto L_0x007d
        L_0x0095:
            r0 = move-exception
            goto L_0x007d
        L_0x0097:
            r0 = move-exception
            r1 = r3
            r2 = r3
            goto L_0x0055
        L_0x009b:
            r0 = move-exception
            r2 = r1
            r1 = r3
            goto L_0x0055
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adchina.android.ads.a.a.o():void");
    }

    private static /* synthetic */ int[] p() {
        int[] iArr = ah;
        if (iArr == null) {
            iArr = new int[g.values().length];
            try {
                iArr[g.EGetImgMaterial.ordinal()] = 3;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[g.EGetShrinkFSImgMaterial.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[g.EIdle.ordinal()] = 17;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[g.EReceiveAd.ordinal()] = 1;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[g.ERefreshAd.ordinal()] = 6;
            } catch (NoSuchFieldError e5) {
            }
            try {
                iArr[g.ESendFullScreenEndedTrack.ordinal()] = 16;
            } catch (NoSuchFieldError e6) {
            }
            try {
                iArr[g.ESendFullScreenImpTrack.ordinal()] = 14;
            } catch (NoSuchFieldError e7) {
            }
            try {
                iArr[g.ESendFullScreenStartedTrack.ordinal()] = 13;
            } catch (NoSuchFieldError e8) {
            }
            try {
                iArr[g.ESendFullScreenThdImpTrack.ordinal()] = 15;
            } catch (NoSuchFieldError e9) {
            }
            try {
                iArr[g.ESendImpTrack.ordinal()] = 4;
            } catch (NoSuchFieldError e10) {
            }
            try {
                iArr[g.ESendShrinkFSImpTrack.ordinal()] = 7;
            } catch (NoSuchFieldError e11) {
            }
            try {
                iArr[g.ESendShrinkFSThdImpTrack.ordinal()] = 8;
            } catch (NoSuchFieldError e12) {
            }
            try {
                iArr[g.ESendThdImpTrack.ordinal()] = 5;
            } catch (NoSuchFieldError e13) {
            }
            try {
                iArr[g.ESendVideoEndedImpTrack.ordinal()] = 11;
            } catch (NoSuchFieldError e14) {
            }
            try {
                iArr[g.ESendVideoEndedThdImpTrack.ordinal()] = 12;
            } catch (NoSuchFieldError e15) {
            }
            try {
                iArr[g.ESendVideoStartedImpTrack.ordinal()] = 9;
            } catch (NoSuchFieldError e16) {
            }
            try {
                iArr[g.ESendVideoStartedThdImpTrack.ordinal()] = 10;
            } catch (NoSuchFieldError e17) {
            }
            ah = iArr;
        }
        return iArr;
    }

    private static /* synthetic */ int[] q() {
        int[] iArr = ai;
        if (iArr == null) {
            iArr = new int[f.values().length];
            try {
                iArr[f.EIdle.ordinal()] = 14;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[f.ESendBtnClkTrack.ordinal()] = 3;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[f.ESendBtnThdClkTrack.ordinal()] = 4;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[f.ESendClkTrack.ordinal()] = 1;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[f.ESendFullScreenClkThdTrack.ordinal()] = 12;
            } catch (NoSuchFieldError e5) {
            }
            try {
                iArr[f.ESendFullScreenClkTrack.ordinal()] = 11;
            } catch (NoSuchFieldError e6) {
            }
            try {
                iArr[f.ESendFullScreenCloseTrack.ordinal()] = 13;
            } catch (NoSuchFieldError e7) {
            }
            try {
                iArr[f.ESendShrinkFSClkTrack.ordinal()] = 5;
            } catch (NoSuchFieldError e8) {
            }
            try {
                iArr[f.ESendShrinkFSThdClkTrack.ordinal()] = 6;
            } catch (NoSuchFieldError e9) {
            }
            try {
                iArr[f.ESendThdClkTrack.ordinal()] = 2;
            } catch (NoSuchFieldError e10) {
            }
            try {
                iArr[f.ESendVideoClkTrack.ordinal()] = 7;
            } catch (NoSuchFieldError e11) {
            }
            try {
                iArr[f.ESendVideoCloseClkTrack.ordinal()] = 9;
            } catch (NoSuchFieldError e12) {
            }
            try {
                iArr[f.ESendVideoCloseThdClkTrack.ordinal()] = 10;
            } catch (NoSuchFieldError e13) {
            }
            try {
                iArr[f.ESendVideoThdClkTrack.ordinal()] = 8;
            } catch (NoSuchFieldError e14) {
            }
            ai = iArr;
        }
        return iArr;
    }

    public final void a() {
        int i;
        int i2 = 0;
        int i3 = 0;
        while (true) {
            i = i2;
            if (i3 >= this.ab.size()) {
                break;
            }
            i2 = ((AdView) this.ab.get(i3)).getVisibility() == 0 ? i + 1 : i;
            i3++;
        }
        if (i <= 0) {
            this.ag = true;
        }
    }

    public final void a(c cVar) {
        String str;
        String str2;
        String str3;
        String str4;
        while (!this.I && !cVar.a) {
            switch (q()[this.V.ordinal()]) {
                case 1:
                    this.Z.c("++ onSendClkTrack");
                    a(f.EIdle);
                    try {
                        String concatString = Utils.concatString(this.f, "1", ",0,0,0", h());
                        t tVar = this.W;
                        t.a(concatString);
                        this.Z.c(Utils.concatString("send ClkTrack to ", concatString));
                        break;
                    } catch (Exception e) {
                        String concatString2 = Utils.concatString("Exceptions in onSendClkTrack, err = ", e.toString());
                        this.Z.c(concatString2);
                        Log.e("AdChinaError", concatString2);
                        break;
                    } finally {
                        a(f.ESendThdClkTrack);
                        str4 = "-- onSendClkTrack";
                        this.Z.c(str4);
                    }
                case 2:
                    a(f.EIdle);
                    int size = this.Q.size();
                    this.Z.c(Utils.concatString("++ onSendThdClkTrack, Size of list is ", Integer.valueOf(size)));
                    if (size > 0) {
                        try {
                            String str5 = (String) this.Q.get(size - 1);
                            this.Q.removeLast();
                            t tVar2 = this.W;
                            t.a(str5);
                            this.Z.c(Utils.concatString("send ThdClkTrack to ", str5));
                            break;
                        } catch (Exception e2) {
                            String concatString3 = Utils.concatString("Failed to onSendThdClkTrack, err = ", e2.toString());
                            this.Z.c(concatString3);
                            Log.e("AdChinaError", concatString3);
                            break;
                        } finally {
                            a(f.ESendThdClkTrack);
                            str3 = "-- onSendThdClkTrack";
                            this.Z.c(str3);
                        }
                    }
                    break;
                case 3:
                    this.Z.c("++ onSendBtnClkTrack");
                    a(f.EIdle);
                    try {
                        String concatString4 = Utils.concatString(this.f, "200", ",0,0,0", h());
                        t tVar3 = this.W;
                        t.a(concatString4);
                        this.Z.c(Utils.concatString("send BtnClkTrack to ", concatString4));
                        break;
                    } catch (Exception e3) {
                        String concatString5 = Utils.concatString("Exceptions in onSendBtnClkTrack, err = ", e3.toString());
                        this.Z.c(concatString5);
                        Log.e("AdChinaError", concatString5);
                        break;
                    } finally {
                        Utils.splitTrackUrl(this.u.toString(), this.S);
                        a(f.ESendBtnThdClkTrack);
                        str2 = "-- onSendBtnClkTrack";
                        this.Z.c(str2);
                    }
                case 4:
                    a(f.EIdle);
                    int size2 = this.S.size();
                    this.Z.c(Utils.concatString("++ onSendBtnThdClkTrack, Size of list is ", Integer.valueOf(size2)));
                    if (size2 > 0) {
                        try {
                            String str6 = (String) this.S.get(size2 - 1);
                            this.S.removeLast();
                            t tVar4 = this.W;
                            t.a(str6);
                            this.Z.c(Utils.concatString("send btnthdclktrack to ", str6));
                            break;
                        } catch (Exception e4) {
                            String concatString6 = Utils.concatString("Failed to onSendBtnThdClkTrack, err = ", e4.toString());
                            this.Z.c(concatString6);
                            Log.e("AdChinaError", concatString6);
                            break;
                        } finally {
                            a(f.ESendBtnThdClkTrack);
                            str = "-- onSendBtnThdClkTrack";
                            this.Z.c(str);
                        }
                    }
                    break;
            }
            try {
                Thread.sleep(50);
            } catch (InterruptedException e5) {
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:71:0x018a A[SYNTHETIC, Splitter:B:71:0x018a] */
    /* JADX WARNING: Removed duplicated region for block: B:74:0x018f A[Catch:{ IOException -> 0x0197 }] */
    /* JADX WARNING: Removed duplicated region for block: B:86:0x01c9 A[SYNTHETIC, Splitter:B:86:0x01c9] */
    /* JADX WARNING: Removed duplicated region for block: B:89:0x01ce A[Catch:{ IOException -> 0x01d5 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(com.adchina.android.ads.a.d r12) {
        /*
            r11 = this;
        L_0x0000:
            boolean r0 = r11.I
            if (r0 != 0) goto L_0x0008
            boolean r0 = r12.a
            if (r0 == 0) goto L_0x0009
        L_0x0008:
            return
        L_0x0009:
            int[] r0 = p()
            com.adchina.android.ads.g r1 = r11.a
            int r1 = r1.ordinal()
            r0 = r0[r1]
            switch(r0) {
                case 1: goto L_0x0020;
                case 2: goto L_0x0018;
                case 3: goto L_0x0212;
                case 4: goto L_0x0217;
                case 5: goto L_0x02a0;
                case 6: goto L_0x032d;
                default: goto L_0x0018;
            }
        L_0x0018:
            r0 = 50
            java.lang.Thread.sleep(r0)     // Catch:{ InterruptedException -> 0x001e }
            goto L_0x0000
        L_0x001e:
            r0 = move-exception
            goto L_0x0000
        L_0x0020:
            boolean r0 = r11.af
            if (r0 == 0) goto L_0x0018
            boolean r0 = r11.ag
            if (r0 != 0) goto L_0x0018
            com.adchina.android.ads.h r0 = r11.Z
            java.lang.String r1 = "++ onReceiveAd"
            r0.c(r1)
            com.adchina.android.ads.g r0 = com.adchina.android.ads.g.EIdle
            r11.a(r0)
            r0 = 0
            boolean r1 = com.adchina.android.ads.AdManager.getDebugMode()     // Catch:{ Exception -> 0x047e, all -> 0x0475 }
            if (r1 == 0) goto L_0x0084
            java.lang.String r1 = r11.j()     // Catch:{ Exception -> 0x047e, all -> 0x0475 }
            if (r1 == 0) goto L_0x0061
            com.adchina.android.ads.h r2 = r11.Z     // Catch:{ Exception -> 0x047e, all -> 0x0475 }
            r3 = 2
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ Exception -> 0x047e, all -> 0x0475 }
            r4 = 0
            java.lang.String r5 = "AdserverUrl:"
            r3[r4] = r5     // Catch:{ Exception -> 0x047e, all -> 0x0475 }
            r4 = 1
            r3[r4] = r1     // Catch:{ Exception -> 0x047e, all -> 0x0475 }
            java.lang.String r3 = com.adchina.android.ads.Utils.concatString(r3)     // Catch:{ Exception -> 0x047e, all -> 0x0475 }
            r2.c(r3)     // Catch:{ Exception -> 0x047e, all -> 0x0475 }
            com.adchina.android.ads.t r2 = r11.W     // Catch:{ Exception -> 0x047e, all -> 0x0475 }
            java.lang.StringBuffer r3 = r11.N     // Catch:{ Exception -> 0x047e, all -> 0x0475 }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x047e, all -> 0x0475 }
            java.io.InputStream r0 = r2.a(r1, r3)     // Catch:{ Exception -> 0x047e, all -> 0x0475 }
        L_0x0061:
            r11.a(r0)     // Catch:{ Exception -> 0x0109, all -> 0x01b2 }
            com.adchina.android.ads.l r1 = com.adchina.android.ads.l.EAdTXT     // Catch:{ Exception -> 0x0109, all -> 0x01b2 }
            com.adchina.android.ads.l r2 = r11.b     // Catch:{ Exception -> 0x0109, all -> 0x01b2 }
            if (r1 != r2) goto L_0x00a9
            r1 = 1
            java.lang.StringBuffer r2 = r11.o     // Catch:{ Exception -> 0x0109, all -> 0x01b2 }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x0109, all -> 0x01b2 }
            r11.a(r1, r2)     // Catch:{ Exception -> 0x0109, all -> 0x01b2 }
        L_0x0074:
            r11.e()     // Catch:{ Exception -> 0x0109, all -> 0x01b2 }
            com.adchina.android.ads.t r1 = r11.W
            com.adchina.android.ads.t.a(r0)
            com.adchina.android.ads.h r0 = r11.Z
            java.lang.String r1 = "-- onReceiveAd"
            r0.c(r1)
            goto L_0x0018
        L_0x0084:
            java.lang.String r1 = r11.i()     // Catch:{ Exception -> 0x047e, all -> 0x0475 }
            com.adchina.android.ads.h r2 = r11.Z     // Catch:{ Exception -> 0x047e, all -> 0x0475 }
            r3 = 2
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ Exception -> 0x047e, all -> 0x0475 }
            r4 = 0
            java.lang.String r5 = "AdserverUrl:"
            r3[r4] = r5     // Catch:{ Exception -> 0x047e, all -> 0x0475 }
            r4 = 1
            r3[r4] = r1     // Catch:{ Exception -> 0x047e, all -> 0x0475 }
            java.lang.String r3 = com.adchina.android.ads.Utils.concatString(r3)     // Catch:{ Exception -> 0x047e, all -> 0x0475 }
            r2.c(r3)     // Catch:{ Exception -> 0x047e, all -> 0x0475 }
            com.adchina.android.ads.t r2 = r11.W     // Catch:{ Exception -> 0x047e, all -> 0x0475 }
            java.lang.StringBuffer r3 = r11.N     // Catch:{ Exception -> 0x047e, all -> 0x0475 }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x047e, all -> 0x0475 }
            java.io.InputStream r0 = r2.a(r1, r3)     // Catch:{ Exception -> 0x047e, all -> 0x0475 }
            goto L_0x0061
        L_0x00a9:
            com.adchina.android.ads.l r1 = com.adchina.android.ads.l.EAdJPG     // Catch:{ Exception -> 0x0109, all -> 0x01b2 }
            com.adchina.android.ads.l r2 = r11.b     // Catch:{ Exception -> 0x0109, all -> 0x01b2 }
            if (r1 == r2) goto L_0x00bb
            com.adchina.android.ads.l r1 = com.adchina.android.ads.l.EAdPNG     // Catch:{ Exception -> 0x0109, all -> 0x01b2 }
            com.adchina.android.ads.l r2 = r11.b     // Catch:{ Exception -> 0x0109, all -> 0x01b2 }
            if (r1 == r2) goto L_0x00bb
            com.adchina.android.ads.l r1 = com.adchina.android.ads.l.EAdGIF     // Catch:{ Exception -> 0x0109, all -> 0x01b2 }
            com.adchina.android.ads.l r2 = r11.b     // Catch:{ Exception -> 0x0109, all -> 0x01b2 }
            if (r1 != r2) goto L_0x020a
        L_0x00bb:
            com.adchina.android.ads.h r1 = r11.Z     // Catch:{ Exception -> 0x0109, all -> 0x01b2 }
            java.lang.String r2 = "loadLocalBannerImgList"
            r1.c(r2)     // Catch:{ Exception -> 0x0109, all -> 0x01b2 }
            r1 = 0
            r2 = 0
            android.content.Context r3 = r11.J     // Catch:{ Exception -> 0x016c, all -> 0x01c3 }
            java.lang.String r4 = "adchinaBNImgs.fc"
            java.io.File r3 = r3.getFileStreamPath(r4)     // Catch:{ Exception -> 0x016c, all -> 0x01c3 }
            long r3 = r3.length()     // Catch:{ Exception -> 0x016c, all -> 0x01c3 }
            int r3 = (int) r3     // Catch:{ Exception -> 0x016c, all -> 0x01c3 }
            char[] r3 = new char[r3]     // Catch:{ Exception -> 0x016c, all -> 0x01c3 }
            android.content.Context r4 = r11.J     // Catch:{ Exception -> 0x016c, all -> 0x01c3 }
            java.lang.String r5 = "adchinaBNImgs.fc"
            java.io.FileInputStream r1 = r4.openFileInput(r5)     // Catch:{ Exception -> 0x016c, all -> 0x01c3 }
            java.io.InputStreamReader r4 = new java.io.InputStreamReader     // Catch:{ Exception -> 0x0493, all -> 0x0484 }
            r4.<init>(r1)     // Catch:{ Exception -> 0x0493, all -> 0x0484 }
            r4.read(r3)     // Catch:{ Exception -> 0x0499, all -> 0x048a }
            java.lang.String r2 = new java.lang.String     // Catch:{ Exception -> 0x0499, all -> 0x048a }
            r2.<init>(r3)     // Catch:{ Exception -> 0x0499, all -> 0x048a }
            java.util.HashMap r3 = r11.ac     // Catch:{ Exception -> 0x0499, all -> 0x048a }
            r3.clear()     // Catch:{ Exception -> 0x0499, all -> 0x048a }
            java.lang.String r3 = "\n"
            java.lang.String[] r2 = r2.split(r3)     // Catch:{ Exception -> 0x0499, all -> 0x048a }
            int r3 = r2.length     // Catch:{ Exception -> 0x0499, all -> 0x048a }
            r5 = 0
        L_0x00f5:
            if (r5 < r3) goto L_0x013c
            r4.close()     // Catch:{ IOException -> 0x01ef }
            if (r1 == 0) goto L_0x00ff
            r1.close()     // Catch:{ IOException -> 0x01ef }
        L_0x00ff:
            r11.n()     // Catch:{ IOException -> 0x01ef }
        L_0x0102:
            com.adchina.android.ads.g r1 = com.adchina.android.ads.g.EGetImgMaterial     // Catch:{ Exception -> 0x0109, all -> 0x01b2 }
            r11.a(r1)     // Catch:{ Exception -> 0x0109, all -> 0x01b2 }
            goto L_0x0074
        L_0x0109:
            r1 = move-exception
            r10 = r1
            r1 = r0
            r0 = r10
        L_0x010d:
            r2 = 2
            java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch:{ all -> 0x047b }
            r3 = 0
            java.lang.String r4 = "Exceptions in onReceiveAd, err = "
            r2[r3] = r4     // Catch:{ all -> 0x047b }
            r3 = 1
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x047b }
            r2[r3] = r0     // Catch:{ all -> 0x047b }
            java.lang.String r0 = com.adchina.android.ads.Utils.concatString(r2)     // Catch:{ all -> 0x047b }
            r2 = 2
            r11.a(r2, r0)     // Catch:{ all -> 0x047b }
            com.adchina.android.ads.h r2 = r11.Z     // Catch:{ all -> 0x047b }
            r2.c(r0)     // Catch:{ all -> 0x047b }
            java.lang.String r2 = "AdChinaError"
            android.util.Log.e(r2, r0)     // Catch:{ all -> 0x047b }
            com.adchina.android.ads.t r0 = r11.W
            com.adchina.android.ads.t.a(r1)
            com.adchina.android.ads.h r0 = r11.Z
            java.lang.String r1 = "-- onReceiveAd"
            r0.c(r1)
            goto L_0x0018
        L_0x013c:
            r6 = r2[r5]     // Catch:{ Exception -> 0x0499, all -> 0x048a }
            java.lang.String r7 = "|||"
            int r7 = r6.indexOf(r7)     // Catch:{ Exception -> 0x0499, all -> 0x048a }
            r8 = 0
            java.lang.String r8 = r6.substring(r8, r7)     // Catch:{ Exception -> 0x0499, all -> 0x048a }
            int r7 = r7 + 3
            java.lang.String r6 = r6.substring(r7)     // Catch:{ Exception -> 0x0499, all -> 0x048a }
            java.io.File r7 = new java.io.File     // Catch:{ Exception -> 0x0499, all -> 0x048a }
            r7.<init>(r6)     // Catch:{ Exception -> 0x0499, all -> 0x048a }
            boolean r9 = r7.exists()     // Catch:{ Exception -> 0x0499, all -> 0x048a }
            if (r9 == 0) goto L_0x0169
            java.lang.String r7 = r7.getName()     // Catch:{ Exception -> 0x0499, all -> 0x048a }
            boolean r7 = com.adchina.android.ads.Utils.isCachedFileTimeout(r7)     // Catch:{ Exception -> 0x0499, all -> 0x048a }
            if (r7 != 0) goto L_0x0169
            java.util.HashMap r7 = r11.ac     // Catch:{ Exception -> 0x0499, all -> 0x048a }
            r7.put(r8, r6)     // Catch:{ Exception -> 0x0499, all -> 0x048a }
        L_0x0169:
            int r5 = r5 + 1
            goto L_0x00f5
        L_0x016c:
            r3 = move-exception
            r10 = r3
            r3 = r1
            r1 = r10
        L_0x0170:
            com.adchina.android.ads.h r4 = r11.Z     // Catch:{ all -> 0x0490 }
            r5 = 2
            java.lang.Object[] r5 = new java.lang.Object[r5]     // Catch:{ all -> 0x0490 }
            r6 = 0
            java.lang.String r7 = "Exceptions in loadLocalBannerImgList , err = "
            r5[r6] = r7     // Catch:{ all -> 0x0490 }
            r6 = 1
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x0490 }
            r5[r6] = r1     // Catch:{ all -> 0x0490 }
            java.lang.String r1 = com.adchina.android.ads.Utils.concatString(r5)     // Catch:{ all -> 0x0490 }
            r4.c(r1)     // Catch:{ all -> 0x0490 }
            if (r2 == 0) goto L_0x018d
            r2.close()     // Catch:{ IOException -> 0x0197 }
        L_0x018d:
            if (r3 == 0) goto L_0x0192
            r3.close()     // Catch:{ IOException -> 0x0197 }
        L_0x0192:
            r11.n()     // Catch:{ IOException -> 0x0197 }
            goto L_0x0102
        L_0x0197:
            r1 = move-exception
            com.adchina.android.ads.h r2 = r11.Z     // Catch:{ Exception -> 0x0109, all -> 0x01b2 }
            r3 = 2
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ Exception -> 0x0109, all -> 0x01b2 }
            r4 = 0
            java.lang.String r5 = "Exceptions in loadLocalBannerImgList , err = "
            r3[r4] = r5     // Catch:{ Exception -> 0x0109, all -> 0x01b2 }
            r4 = 1
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x0109, all -> 0x01b2 }
            r3[r4] = r1     // Catch:{ Exception -> 0x0109, all -> 0x01b2 }
            java.lang.String r1 = com.adchina.android.ads.Utils.concatString(r3)     // Catch:{ Exception -> 0x0109, all -> 0x01b2 }
            r2.c(r1)     // Catch:{ Exception -> 0x0109, all -> 0x01b2 }
            goto L_0x0102
        L_0x01b2:
            r1 = move-exception
            r10 = r1
            r1 = r0
            r0 = r10
        L_0x01b6:
            com.adchina.android.ads.t r2 = r11.W
            com.adchina.android.ads.t.a(r1)
            com.adchina.android.ads.h r1 = r11.Z
            java.lang.String r2 = "-- onReceiveAd"
            r1.c(r2)
            throw r0
        L_0x01c3:
            r3 = move-exception
            r10 = r3
            r3 = r1
            r1 = r10
        L_0x01c7:
            if (r2 == 0) goto L_0x01cc
            r2.close()     // Catch:{ IOException -> 0x01d5 }
        L_0x01cc:
            if (r3 == 0) goto L_0x01d1
            r3.close()     // Catch:{ IOException -> 0x01d5 }
        L_0x01d1:
            r11.n()     // Catch:{ IOException -> 0x01d5 }
        L_0x01d4:
            throw r1     // Catch:{ Exception -> 0x0109, all -> 0x01b2 }
        L_0x01d5:
            r2 = move-exception
            com.adchina.android.ads.h r3 = r11.Z     // Catch:{ Exception -> 0x0109, all -> 0x01b2 }
            r4 = 2
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ Exception -> 0x0109, all -> 0x01b2 }
            r5 = 0
            java.lang.String r6 = "Exceptions in loadLocalBannerImgList , err = "
            r4[r5] = r6     // Catch:{ Exception -> 0x0109, all -> 0x01b2 }
            r5 = 1
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x0109, all -> 0x01b2 }
            r4[r5] = r2     // Catch:{ Exception -> 0x0109, all -> 0x01b2 }
            java.lang.String r2 = com.adchina.android.ads.Utils.concatString(r4)     // Catch:{ Exception -> 0x0109, all -> 0x01b2 }
            r3.c(r2)     // Catch:{ Exception -> 0x0109, all -> 0x01b2 }
            goto L_0x01d4
        L_0x01ef:
            r1 = move-exception
            com.adchina.android.ads.h r2 = r11.Z     // Catch:{ Exception -> 0x0109, all -> 0x01b2 }
            r3 = 2
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ Exception -> 0x0109, all -> 0x01b2 }
            r4 = 0
            java.lang.String r5 = "Exceptions in loadLocalBannerImgList , err = "
            r3[r4] = r5     // Catch:{ Exception -> 0x0109, all -> 0x01b2 }
            r4 = 1
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x0109, all -> 0x01b2 }
            r3[r4] = r1     // Catch:{ Exception -> 0x0109, all -> 0x01b2 }
            java.lang.String r1 = com.adchina.android.ads.Utils.concatString(r3)     // Catch:{ Exception -> 0x0109, all -> 0x01b2 }
            r2.c(r1)     // Catch:{ Exception -> 0x0109, all -> 0x01b2 }
            goto L_0x0102
        L_0x020a:
            org.xmlpull.v1.XmlPullParserException r1 = new org.xmlpull.v1.XmlPullParserException     // Catch:{ Exception -> 0x0109, all -> 0x01b2 }
            java.lang.String r2 = "Invalidate mAdModel，可能因为不在广告定向区域，所以Banner广告位上现在没有可供展示的广告"
            r1.<init>(r2)     // Catch:{ Exception -> 0x0109, all -> 0x01b2 }
            throw r1     // Catch:{ Exception -> 0x0109, all -> 0x01b2 }
        L_0x0212:
            r11.m()
            goto L_0x0018
        L_0x0217:
            com.adchina.android.ads.h r0 = r11.Z
            java.lang.String r1 = "++ onSendImpTrack"
            r0.c(r1)
            r0 = 4
            java.lang.Object[] r0 = new java.lang.Object[r0]     // Catch:{ Exception -> 0x0266 }
            r1 = 0
            java.lang.StringBuffer r2 = r11.f     // Catch:{ Exception -> 0x0266 }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x0266 }
            r0[r1] = r2     // Catch:{ Exception -> 0x0266 }
            r1 = 1
            java.lang.String r2 = "2"
            r0[r1] = r2     // Catch:{ Exception -> 0x0266 }
            r1 = 2
            java.lang.String r2 = ",0,0,0"
            r0[r1] = r2     // Catch:{ Exception -> 0x0266 }
            r1 = 3
            java.lang.String r2 = r11.h()     // Catch:{ Exception -> 0x0266 }
            r0[r1] = r2     // Catch:{ Exception -> 0x0266 }
            java.lang.String r0 = com.adchina.android.ads.Utils.concatString(r0)     // Catch:{ Exception -> 0x0266 }
            com.adchina.android.ads.t r1 = r11.W     // Catch:{ Exception -> 0x0266 }
            com.adchina.android.ads.t.a(r0)     // Catch:{ Exception -> 0x0266 }
            com.adchina.android.ads.h r1 = r11.Z     // Catch:{ Exception -> 0x0266 }
            r2 = 2
            java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch:{ Exception -> 0x0266 }
            r3 = 0
            java.lang.String r4 = "send ImpTrack to "
            r2[r3] = r4     // Catch:{ Exception -> 0x0266 }
            r3 = 1
            r2[r3] = r0     // Catch:{ Exception -> 0x0266 }
            java.lang.String r0 = com.adchina.android.ads.Utils.concatString(r2)     // Catch:{ Exception -> 0x0266 }
            r1.c(r0)     // Catch:{ Exception -> 0x0266 }
            com.adchina.android.ads.g r0 = com.adchina.android.ads.g.ESendThdImpTrack
            r11.a(r0)
            com.adchina.android.ads.h r0 = r11.Z
            java.lang.String r1 = "-- onSendImpTrack"
            r0.c(r1)
            goto L_0x0018
        L_0x0266:
            r0 = move-exception
            r1 = 2
            java.lang.Object[] r1 = new java.lang.Object[r1]     // Catch:{ all -> 0x0292 }
            r2 = 0
            java.lang.String r3 = "Exceptions in onSendImpTrack, err = "
            r1[r2] = r3     // Catch:{ all -> 0x0292 }
            r2 = 1
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0292 }
            r1[r2] = r0     // Catch:{ all -> 0x0292 }
            java.lang.String r0 = com.adchina.android.ads.Utils.concatString(r1)     // Catch:{ all -> 0x0292 }
            com.adchina.android.ads.h r1 = r11.Z     // Catch:{ all -> 0x0292 }
            r1.c(r0)     // Catch:{ all -> 0x0292 }
            java.lang.String r1 = "AdChinaError"
            android.util.Log.e(r1, r0)     // Catch:{ all -> 0x0292 }
            com.adchina.android.ads.g r0 = com.adchina.android.ads.g.ESendThdImpTrack
            r11.a(r0)
            com.adchina.android.ads.h r0 = r11.Z
            java.lang.String r1 = "-- onSendImpTrack"
            r0.c(r1)
            goto L_0x0018
        L_0x0292:
            r0 = move-exception
            com.adchina.android.ads.g r1 = com.adchina.android.ads.g.ESendThdImpTrack
            r11.a(r1)
            com.adchina.android.ads.h r1 = r11.Z
            java.lang.String r2 = "-- onSendImpTrack"
            r1.c(r2)
            throw r0
        L_0x02a0:
            com.adchina.android.ads.g r0 = com.adchina.android.ads.g.EIdle
            r11.a(r0)
            java.util.LinkedList r0 = r11.R
            int r0 = r0.size()
            com.adchina.android.ads.h r1 = r11.Z
            r2 = 2
            java.lang.Object[] r2 = new java.lang.Object[r2]
            r3 = 0
            java.lang.String r4 = "++ onSendThdImpTrack, Size of list is "
            r2[r3] = r4
            r3 = 1
            java.lang.Integer r4 = java.lang.Integer.valueOf(r0)
            r2[r3] = r4
            java.lang.String r2 = com.adchina.android.ads.Utils.concatString(r2)
            r1.c(r2)
            if (r0 <= 0) goto L_0x0326
            java.util.LinkedList r1 = r11.R     // Catch:{ Exception -> 0x02fb }
            r2 = 1
            int r0 = r0 - r2
            java.lang.Object r0 = r1.get(r0)     // Catch:{ Exception -> 0x02fb }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ Exception -> 0x02fb }
            java.util.LinkedList r1 = r11.R     // Catch:{ Exception -> 0x02fb }
            r1.removeLast()     // Catch:{ Exception -> 0x02fb }
            com.adchina.android.ads.t r1 = r11.W     // Catch:{ Exception -> 0x02fb }
            com.adchina.android.ads.t.a(r0)     // Catch:{ Exception -> 0x02fb }
            com.adchina.android.ads.h r1 = r11.Z     // Catch:{ Exception -> 0x02fb }
            r2 = 2
            java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch:{ Exception -> 0x02fb }
            r3 = 0
            java.lang.String r4 = "send ThdImpTrack to "
            r2[r3] = r4     // Catch:{ Exception -> 0x02fb }
            r3 = 1
            r2[r3] = r0     // Catch:{ Exception -> 0x02fb }
            java.lang.String r0 = com.adchina.android.ads.Utils.concatString(r2)     // Catch:{ Exception -> 0x02fb }
            r1.c(r0)     // Catch:{ Exception -> 0x02fb }
            com.adchina.android.ads.g r0 = com.adchina.android.ads.g.ESendThdImpTrack
            r11.a(r0)
        L_0x02f2:
            com.adchina.android.ads.h r0 = r11.Z
            java.lang.String r1 = "-- onSendThdImpTrack"
            r0.c(r1)
            goto L_0x0018
        L_0x02fb:
            r0 = move-exception
            r1 = 2
            java.lang.Object[] r1 = new java.lang.Object[r1]     // Catch:{ all -> 0x031f }
            r2 = 0
            java.lang.String r3 = "Failed to onSendThdImpTrack, err = "
            r1[r2] = r3     // Catch:{ all -> 0x031f }
            r2 = 1
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x031f }
            r1[r2] = r0     // Catch:{ all -> 0x031f }
            java.lang.String r0 = com.adchina.android.ads.Utils.concatString(r1)     // Catch:{ all -> 0x031f }
            com.adchina.android.ads.h r1 = r11.Z     // Catch:{ all -> 0x031f }
            r1.c(r0)     // Catch:{ all -> 0x031f }
            java.lang.String r1 = "AdChinaError"
            android.util.Log.e(r1, r0)     // Catch:{ all -> 0x031f }
            com.adchina.android.ads.g r0 = com.adchina.android.ads.g.ESendThdImpTrack
            r11.a(r0)
            goto L_0x02f2
        L_0x031f:
            r0 = move-exception
            com.adchina.android.ads.g r1 = com.adchina.android.ads.g.ESendThdImpTrack
            r11.a(r1)
            throw r0
        L_0x0326:
            r0 = 3
            java.lang.String r1 = "RefreshAd"
            r11.a(r0, r1)
            goto L_0x02f2
        L_0x032d:
            com.adchina.android.ads.h r0 = r11.Z
            java.lang.String r1 = "++ onRefreshAd"
            r0.c(r1)
            r0 = 30
            java.lang.StringBuffer r1 = r11.j     // Catch:{ Exception -> 0x0369, all -> 0x03d3 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x0369, all -> 0x03d3 }
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)     // Catch:{ Exception -> 0x0369, all -> 0x03d3 }
            int r0 = r1.intValue()     // Catch:{ Exception -> 0x0369, all -> 0x03d3 }
            int r1 = r0 * 1000
            long r1 = (long) r1
            java.lang.Thread.sleep(r1)     // Catch:{ Exception -> 0x0436, all -> 0x0456 }
            com.adchina.android.ads.g r1 = com.adchina.android.ads.g.EReceiveAd
            r11.a(r1)
            com.adchina.android.ads.h r1 = r11.Z
            r2 = 2
            java.lang.Object[] r2 = new java.lang.Object[r2]
            r3 = 0
            java.lang.String r4 = "-- onRefreshAd, delay = "
            r2[r3] = r4
            r3 = 1
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            r2[r3] = r0
            java.lang.String r0 = com.adchina.android.ads.Utils.concatString(r2)
            r1.c(r0)
            goto L_0x0018
        L_0x0369:
            r0 = move-exception
            r0 = 30000(0x7530, double:1.4822E-319)
            java.lang.Thread.sleep(r0)     // Catch:{ Exception -> 0x0390, all -> 0x03b2 }
            com.adchina.android.ads.g r0 = com.adchina.android.ads.g.EReceiveAd
            r11.a(r0)
            com.adchina.android.ads.h r0 = r11.Z
            r1 = 2
            java.lang.Object[] r1 = new java.lang.Object[r1]
            r2 = 0
            java.lang.String r3 = "-- onRefreshAd, delay = "
            r1[r2] = r3
            r2 = 1
            r3 = 30
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)
            r1[r2] = r3
            java.lang.String r1 = com.adchina.android.ads.Utils.concatString(r1)
            r0.c(r1)
            goto L_0x0018
        L_0x0390:
            r0 = move-exception
            com.adchina.android.ads.g r0 = com.adchina.android.ads.g.EReceiveAd
            r11.a(r0)
            com.adchina.android.ads.h r0 = r11.Z
            r1 = 2
            java.lang.Object[] r1 = new java.lang.Object[r1]
            r2 = 0
            java.lang.String r3 = "-- onRefreshAd, delay = "
            r1[r2] = r3
            r2 = 1
            r3 = 30
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)
            r1[r2] = r3
            java.lang.String r1 = com.adchina.android.ads.Utils.concatString(r1)
            r0.c(r1)
            goto L_0x0018
        L_0x03b2:
            r0 = move-exception
            com.adchina.android.ads.g r1 = com.adchina.android.ads.g.EReceiveAd
            r11.a(r1)
            com.adchina.android.ads.h r1 = r11.Z
            r2 = 2
            java.lang.Object[] r2 = new java.lang.Object[r2]
            r3 = 0
            java.lang.String r4 = "-- onRefreshAd, delay = "
            r2[r3] = r4
            r3 = 1
            r4 = 30
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)
            r2[r3] = r4
            java.lang.String r2 = com.adchina.android.ads.Utils.concatString(r2)
            r1.c(r2)
            throw r0
        L_0x03d3:
            r1 = move-exception
            int r2 = r0 * 1000
            long r2 = (long) r2
            java.lang.Thread.sleep(r2)     // Catch:{ Exception -> 0x03f8, all -> 0x0417 }
            com.adchina.android.ads.g r2 = com.adchina.android.ads.g.EReceiveAd
            r11.a(r2)
            com.adchina.android.ads.h r2 = r11.Z
            r3 = 2
            java.lang.Object[] r3 = new java.lang.Object[r3]
            r4 = 0
            java.lang.String r5 = "-- onRefreshAd, delay = "
            r3[r4] = r5
            r4 = 1
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            r3[r4] = r0
            java.lang.String r0 = com.adchina.android.ads.Utils.concatString(r3)
            r2.c(r0)
        L_0x03f7:
            throw r1
        L_0x03f8:
            r2 = move-exception
            com.adchina.android.ads.g r2 = com.adchina.android.ads.g.EReceiveAd
            r11.a(r2)
            com.adchina.android.ads.h r2 = r11.Z
            r3 = 2
            java.lang.Object[] r3 = new java.lang.Object[r3]
            r4 = 0
            java.lang.String r5 = "-- onRefreshAd, delay = "
            r3[r4] = r5
            r4 = 1
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            r3[r4] = r0
            java.lang.String r0 = com.adchina.android.ads.Utils.concatString(r3)
            r2.c(r0)
            goto L_0x03f7
        L_0x0417:
            r1 = move-exception
            com.adchina.android.ads.g r2 = com.adchina.android.ads.g.EReceiveAd
            r11.a(r2)
            com.adchina.android.ads.h r2 = r11.Z
            r3 = 2
            java.lang.Object[] r3 = new java.lang.Object[r3]
            r4 = 0
            java.lang.String r5 = "-- onRefreshAd, delay = "
            r3[r4] = r5
            r4 = 1
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            r3[r4] = r0
            java.lang.String r0 = com.adchina.android.ads.Utils.concatString(r3)
            r2.c(r0)
            throw r1
        L_0x0436:
            r1 = move-exception
            com.adchina.android.ads.g r1 = com.adchina.android.ads.g.EReceiveAd
            r11.a(r1)
            com.adchina.android.ads.h r1 = r11.Z
            r2 = 2
            java.lang.Object[] r2 = new java.lang.Object[r2]
            r3 = 0
            java.lang.String r4 = "-- onRefreshAd, delay = "
            r2[r3] = r4
            r3 = 1
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            r2[r3] = r0
            java.lang.String r0 = com.adchina.android.ads.Utils.concatString(r2)
            r1.c(r0)
            goto L_0x0018
        L_0x0456:
            r1 = move-exception
            com.adchina.android.ads.g r2 = com.adchina.android.ads.g.EReceiveAd
            r11.a(r2)
            com.adchina.android.ads.h r2 = r11.Z
            r3 = 2
            java.lang.Object[] r3 = new java.lang.Object[r3]
            r4 = 0
            java.lang.String r5 = "-- onRefreshAd, delay = "
            r3[r4] = r5
            r4 = 1
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            r3[r4] = r0
            java.lang.String r0 = com.adchina.android.ads.Utils.concatString(r3)
            r2.c(r0)
            throw r1
        L_0x0475:
            r1 = move-exception
            r10 = r1
            r1 = r0
            r0 = r10
            goto L_0x01b6
        L_0x047b:
            r0 = move-exception
            goto L_0x01b6
        L_0x047e:
            r1 = move-exception
            r10 = r1
            r1 = r0
            r0 = r10
            goto L_0x010d
        L_0x0484:
            r3 = move-exception
            r10 = r3
            r3 = r1
            r1 = r10
            goto L_0x01c7
        L_0x048a:
            r2 = move-exception
            r3 = r1
            r1 = r2
            r2 = r4
            goto L_0x01c7
        L_0x0490:
            r1 = move-exception
            goto L_0x01c7
        L_0x0493:
            r3 = move-exception
            r10 = r3
            r3 = r1
            r1 = r10
            goto L_0x0170
        L_0x0499:
            r2 = move-exception
            r3 = r1
            r1 = r2
            r2 = r4
            goto L_0x0170
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adchina.android.ads.a.a.a(com.adchina.android.ads.a.d):void");
    }

    public final void a(AdView adView) {
        if (adView != null) {
            if (!this.ab.contains(adView)) {
                this.ab.add(adView);
            }
            adView.setController(this);
        }
    }

    public final void a(AdView adView, MotionEvent motionEvent) {
        Rect rect;
        int i;
        int i2;
        int i3;
        int round;
        float x = motionEvent.getX();
        float y = motionEvent.getY();
        try {
            if (this.p.length() > 0) {
                float floatValue = Float.valueOf(this.q.toString()).floatValue() / 100.0f;
                float floatValue2 = Float.valueOf(this.r.toString()).floatValue() / 100.0f;
                float floatValue3 = Float.valueOf(this.s.toString()).floatValue() / 100.0f;
                float floatValue4 = Float.valueOf(this.t.toString()).floatValue() / 100.0f;
                if (adView.getWidth() == 0 || adView.getHeight() == 0 || this.ad == 0 || this.ae == 0) {
                    rect = new Rect(0, 0, 0, 0);
                } else {
                    if ((((double) adView.getWidth()) * 1.0d) / ((double) adView.getHeight()) > (((double) this.ad) * 1.0d) / ((double) this.ae)) {
                        i2 = adView.getHeight();
                        i = (int) Math.round(((((double) i2) * 1.0d) / ((double) this.ae)) * ((double) this.ad));
                        round = 0;
                        i3 = (int) Math.round(((double) (adView.getWidth() - i)) / 2.0d);
                    } else {
                        int width = adView.getWidth();
                        int round2 = (int) Math.round(((((double) width) * 1.0d) / ((double) this.ad)) * ((double) this.ae));
                        int i4 = round2;
                        i = width;
                        i2 = i4;
                        i3 = 0;
                        round = (int) Math.round(((double) (adView.getHeight() - round2)) / 2.0d);
                    }
                    int round3 = Math.round(floatValue * ((float) i)) + i3;
                    int round4 = Math.round(floatValue2 * ((float) i2)) + round;
                    rect = new Rect(round3, round4, Math.round(floatValue3 * ((float) i)) + round3, Math.round(floatValue4 * ((float) i2)) + round4);
                }
                if (rect.contains((int) x, (int) y)) {
                    a(f.ESendBtnClkTrack);
                    if (this.p.toString().equalsIgnoreCase("down")) {
                        this.Z.c(Utils.concatString("++ open ", this.v, " with browser"));
                        AdEngine.getAdEngine().openBrowser(this.v.toString());
                        this.Z.c("-- open browser");
                    } else if (this.p.toString().equalsIgnoreCase("sms")) {
                        try {
                            if (X.OnRecvSms(adView, Utils.concatString("SMS:", this.x, " To:", this.w))) {
                                this.Z.c(Utils.concatString("++ send sms to number ", this.w));
                            }
                        } catch (Exception e) {
                        }
                        AdEngine.getAdEngine().sendSms(this.w.toString(), this.x.toString());
                        this.Z.c("-- send sms");
                    } else if (this.p.toString().equalsIgnoreCase("call")) {
                        this.Z.c(Utils.concatString("++ make call to number ", this.y));
                        AdEngine.getAdEngine().makeCall(this.y.toString());
                        this.Z.c("-- make call");
                    }
                } else {
                    f();
                }
            } else {
                f();
            }
        } catch (Exception e2) {
            this.Z.c(Utils.concatString("Failed to AdView.onTouchEvent, err = ", e2.toString()));
        }
    }

    public final void a(String str) {
        this.e.setLength(0);
        this.e.append(str);
        this.m.setLength(0);
        this.m.append("2");
    }

    /* access modifiers changed from: package-private */
    public final void a(StringBuffer stringBuffer, StringBuffer stringBuffer2, StringBuffer stringBuffer3, StringBuffer stringBuffer4, StringBuffer stringBuffer5, StringBuffer stringBuffer6, StringBuffer stringBuffer7, StringBuffer stringBuffer8, StringBuffer stringBuffer9, StringBuffer stringBuffer10, StringBuffer stringBuffer11, StringBuffer stringBuffer12, StringBuffer stringBuffer13, StringBuffer stringBuffer14, StringBuffer stringBuffer15, StringBuffer stringBuffer16, StringBuffer stringBuffer17, StringBuffer stringBuffer18, StringBuffer stringBuffer19, StringBuffer stringBuffer20, StringBuffer stringBuffer21, StringBuffer stringBuffer22, StringBuffer stringBuffer23, StringBuffer stringBuffer24, StringBuffer stringBuffer25, StringBuffer stringBuffer26, StringBuffer stringBuffer27, StringBuffer stringBuffer28, StringBuffer stringBuffer29, StringBuffer stringBuffer30) {
        g();
        this.c.append(stringBuffer);
        this.d.append(stringBuffer2);
        this.e.append(stringBuffer3);
        this.f.append(stringBuffer4);
        this.g.append(stringBuffer5);
        this.h.append(stringBuffer6);
        this.i.append(stringBuffer7);
        this.j.append(stringBuffer8);
        this.k.append(stringBuffer9);
        this.l.append(stringBuffer10);
        this.m.append(stringBuffer11);
        this.n.append(stringBuffer12);
        this.o.append(stringBuffer13);
        this.p.append(stringBuffer14);
        this.q.append(stringBuffer15);
        this.r.append(stringBuffer16);
        this.s.append(stringBuffer17);
        this.t.append(stringBuffer18);
        this.u.append(stringBuffer19);
        this.v.append(stringBuffer20);
        this.w.append(stringBuffer21);
        this.x.append(stringBuffer22);
        this.y.append(stringBuffer23);
        this.z.append(stringBuffer24);
        this.A.append(stringBuffer25);
        this.B.append(stringBuffer26);
        this.C.append(stringBuffer27);
        this.D.append(stringBuffer28);
        this.E.append(stringBuffer29);
        this.F.append(stringBuffer30);
        if (this.d.toString().compareToIgnoreCase("txt") == 0) {
            this.b = l.EAdTXT;
        } else if (this.d.toString().compareToIgnoreCase("jpg") == 0) {
            this.b = l.EAdJPG;
        } else if (this.d.toString().compareToIgnoreCase("png") == 0) {
            this.b = l.EAdPNG;
        } else if (this.d.toString().compareToIgnoreCase("gif") == 0) {
            this.b = l.EAdGIF;
        } else {
            this.b = l.EAdNONE;
        }
        a(this.i.toString(), "adchinaFC.fc");
        this.Z.c(((Object) this.d) + " : ");
        this.Z.c(Utils.concatString("ImpUrl:", this.g));
        this.Z.c(Utils.concatString("ClkUrl:", this.e));
        this.Z.c(Utils.concatString("FC:", this.i));
        this.Z.c(Utils.concatString("BannerImgAddr:", this.z));
        if (!(l.EAdTXT == this.b || l.EAdNONE == this.b)) {
            this.Z.c(Utils.concatString("ImgAddr:", this.n));
        }
        this.Z.c("-- readAdserverInfo");
    }

    /* access modifiers changed from: package-private */
    public final void a(boolean z) {
        this.af = z;
    }

    public final void b() {
        this.ag = false;
    }

    public final void b(AdView adView) {
        if (adView != null) {
            if (this.ab.contains(adView)) {
                this.ab.remove(adView);
            }
            if (this.ab.size() == 0) {
                l();
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void b(String str) {
        a(str, "adchinaFC.fc");
    }

    /* access modifiers changed from: protected */
    public final void c() {
        this.N.setLength(0);
        this.N.append(AdManager.getAdspaceId());
    }

    /* access modifiers changed from: protected */
    public final String d() {
        return c("adchinaFC.fc");
    }

    /* access modifiers changed from: package-private */
    public final void e() {
        Utils.splitTrackUrl(this.h.toString(), this.Q);
        Utils.splitTrackUrl(this.g.toString(), this.R);
        Utils.splitTrackUrl(this.u.toString(), this.S);
    }

    public final void f() {
        this.Z.c("++ onClick");
        if (this.m.length() > 0) {
            this.Z.c(Utils.concatString("++ open ", this.e, " with browser"));
            AdEngine.getAdEngine().openBrowser(this.e.toString());
            this.Z.c("-- open browser");
            Utils.splitTrackUrl(this.h.toString(), this.Q);
            a(f.ESendThdClkTrack);
            this.Z.c("-- onClick");
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public final boolean handleMessage(Message message) {
        if (message != null) {
            try {
                switch (message.what) {
                    case 1:
                        if (X != null) {
                            Iterator it = this.ab.iterator();
                            while (it.hasNext()) {
                                AdView adView = (AdView) it.next();
                                if (this.P) {
                                    try {
                                        X.onRefreshAd(adView);
                                    } catch (Exception e) {
                                    }
                                } else {
                                    try {
                                        X.onReceiveAd(adView);
                                    } catch (Exception e2) {
                                    }
                                }
                            }
                        }
                        try {
                            this.Z.c(Utils.concatString("++ onDisplayAd, mAdModel = ", this.b));
                            this.P = true;
                            if (this.ab != null && this.ab.size() != 0) {
                                Iterator it2 = this.ab.iterator();
                                while (it2.hasNext()) {
                                    AdView adView2 = (AdView) it2.next();
                                    try {
                                        if (l.EAdTXT == this.b) {
                                            adView2.setContent(this.o.toString());
                                        } else if (l.EAdJPG == this.b || l.EAdPNG == this.b) {
                                            Bitmap bitmap = (Bitmap) message.obj;
                                            adView2.setContent(bitmap.copy(bitmap.getConfig(), true));
                                        } else if (l.EAdGIF == this.b) {
                                            adView2.setContent(((o) message.obj).d());
                                        }
                                    } catch (Exception e3) {
                                        Log.e("AdChinaError", Utils.concatString("Failed to DisplayAd, err = ", e3.toString()));
                                    }
                                }
                                this.O = true;
                                a(g.ESendImpTrack);
                                this.Z.c("-- onDisplayAd");
                                break;
                            } else {
                                this.Z.c(Utils.concatString("Failed to DisplayAd, err = ", "no AdView is visible"));
                                this.O = false;
                                this.j.setLength(0);
                                this.j.append("30");
                                a(g.ERefreshAd);
                                break;
                            }
                        } catch (Exception e4) {
                            break;
                        }
                        break;
                    case 2:
                        if (X != null) {
                            Iterator it3 = this.ab.iterator();
                            while (it3.hasNext()) {
                                AdView adView3 = (AdView) it3.next();
                                if (this.P) {
                                    try {
                                        X.onFailedToRefreshAd(adView3);
                                    } catch (Exception e5) {
                                    }
                                } else {
                                    try {
                                        X.onFailedToReceiveAd(adView3);
                                    } catch (Exception e6) {
                                    }
                                }
                            }
                        }
                        this.O = false;
                        this.j.setLength(0);
                        this.j.append("30");
                        a(g.ERefreshAd);
                        break;
                    case 3:
                        a(g.ERefreshAd);
                        break;
                }
            } catch (Exception e7) {
                Log.e("AdChinaError", "handleMessage" + e7.getMessage());
            }
            return true;
        }
        if (message != null) {
            this.Y.removeMessages(message.what);
        }
        return true;
    }
}
