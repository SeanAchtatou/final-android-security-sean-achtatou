package com.adchina.android.ads.a;

import android.content.Context;
import android.util.Log;
import com.adchina.android.ads.AdEngine;
import com.adchina.android.ads.AdManager;
import com.adchina.android.ads.AdVideoFinishListener;
import com.adchina.android.ads.Utils;
import com.adchina.android.ads.a;
import com.adchina.android.ads.f;
import com.adchina.android.ads.g;
import com.adchina.android.ads.k;
import com.adchina.android.ads.t;
import com.adchina.android.ads.views.animations.AnimationManager;
import com.safetest.pvz.R;
import java.io.File;
import java.util.HashMap;
import java.util.LinkedList;

public final class q extends b {
    private static q aj;
    private static /* synthetic */ int[] ak;
    private static /* synthetic */ int[] al;
    private int aa = 1;
    private LinkedList ab = new LinkedList();
    private LinkedList ac = new LinkedList();
    private LinkedList ad = new LinkedList();
    private HashMap ae = new HashMap();
    private HashMap af = new HashMap();
    private u ag;
    /* access modifiers changed from: private */
    public AdVideoFinishListener ah;
    private boolean ai = false;

    private q(Context context) {
        super(context);
    }

    public static q a() {
        return aj;
    }

    public static q a(Context context) {
        if (aj == null) {
            aj = new q(context);
        }
        return aj;
    }

    private void o() {
        File file = new File(k.a);
        if (file.exists()) {
            for (File file2 : file.listFiles()) {
                if (!this.ae.containsValue(file2.getAbsolutePath())) {
                    file2.delete();
                }
            }
        }
    }

    private static /* synthetic */ int[] p() {
        int[] iArr = ak;
        if (iArr == null) {
            iArr = new int[g.values().length];
            try {
                iArr[g.EGetImgMaterial.ordinal()] = 3;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[g.EGetShrinkFSImgMaterial.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[g.EIdle.ordinal()] = 17;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[g.EReceiveAd.ordinal()] = 1;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[g.ERefreshAd.ordinal()] = 6;
            } catch (NoSuchFieldError e5) {
            }
            try {
                iArr[g.ESendFullScreenEndedTrack.ordinal()] = 16;
            } catch (NoSuchFieldError e6) {
            }
            try {
                iArr[g.ESendFullScreenImpTrack.ordinal()] = 14;
            } catch (NoSuchFieldError e7) {
            }
            try {
                iArr[g.ESendFullScreenStartedTrack.ordinal()] = 13;
            } catch (NoSuchFieldError e8) {
            }
            try {
                iArr[g.ESendFullScreenThdImpTrack.ordinal()] = 15;
            } catch (NoSuchFieldError e9) {
            }
            try {
                iArr[g.ESendImpTrack.ordinal()] = 4;
            } catch (NoSuchFieldError e10) {
            }
            try {
                iArr[g.ESendShrinkFSImpTrack.ordinal()] = 7;
            } catch (NoSuchFieldError e11) {
            }
            try {
                iArr[g.ESendShrinkFSThdImpTrack.ordinal()] = 8;
            } catch (NoSuchFieldError e12) {
            }
            try {
                iArr[g.ESendThdImpTrack.ordinal()] = 5;
            } catch (NoSuchFieldError e13) {
            }
            try {
                iArr[g.ESendVideoEndedImpTrack.ordinal()] = 11;
            } catch (NoSuchFieldError e14) {
            }
            try {
                iArr[g.ESendVideoEndedThdImpTrack.ordinal()] = 12;
            } catch (NoSuchFieldError e15) {
            }
            try {
                iArr[g.ESendVideoStartedImpTrack.ordinal()] = 9;
            } catch (NoSuchFieldError e16) {
            }
            try {
                iArr[g.ESendVideoStartedThdImpTrack.ordinal()] = 10;
            } catch (NoSuchFieldError e17) {
            }
            ak = iArr;
        }
        return iArr;
    }

    private static /* synthetic */ int[] q() {
        int[] iArr = al;
        if (iArr == null) {
            iArr = new int[f.values().length];
            try {
                iArr[f.EIdle.ordinal()] = 14;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[f.ESendBtnClkTrack.ordinal()] = 3;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[f.ESendBtnThdClkTrack.ordinal()] = 4;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[f.ESendClkTrack.ordinal()] = 1;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[f.ESendFullScreenClkThdTrack.ordinal()] = 12;
            } catch (NoSuchFieldError e5) {
            }
            try {
                iArr[f.ESendFullScreenClkTrack.ordinal()] = 11;
            } catch (NoSuchFieldError e6) {
            }
            try {
                iArr[f.ESendFullScreenCloseTrack.ordinal()] = 13;
            } catch (NoSuchFieldError e7) {
            }
            try {
                iArr[f.ESendShrinkFSClkTrack.ordinal()] = 5;
            } catch (NoSuchFieldError e8) {
            }
            try {
                iArr[f.ESendShrinkFSThdClkTrack.ordinal()] = 6;
            } catch (NoSuchFieldError e9) {
            }
            try {
                iArr[f.ESendThdClkTrack.ordinal()] = 2;
            } catch (NoSuchFieldError e10) {
            }
            try {
                iArr[f.ESendVideoClkTrack.ordinal()] = 7;
            } catch (NoSuchFieldError e11) {
            }
            try {
                iArr[f.ESendVideoCloseClkTrack.ordinal()] = 9;
            } catch (NoSuchFieldError e12) {
            }
            try {
                iArr[f.ESendVideoCloseThdClkTrack.ordinal()] = 10;
            } catch (NoSuchFieldError e13) {
            }
            try {
                iArr[f.ESendVideoThdClkTrack.ordinal()] = 8;
            } catch (NoSuchFieldError e14) {
            }
            al = iArr;
        }
        return iArr;
    }

    public final void a(Context context, String str) {
        Object obj = null;
        if (this.af.containsKey(str)) {
            obj = this.af.get(str);
            this.af.remove(str);
        }
        if (this.ah != null) {
            a.a(new r(this), new s(this, context, obj));
        }
    }

    public final void a(AdVideoFinishListener adVideoFinishListener) {
        this.ah = adVideoFinishListener;
    }

    public final void a(c cVar) {
        String str;
        String str2;
        String str3;
        String str4;
        while (!this.I && !cVar.a) {
            switch (q()[this.V.ordinal()]) {
                case R.styleable.net_youmi_android_AdView_backgroundTransparent /*7*/:
                    this.Z.c("++ onSendVideoClkTrack");
                    a(f.EIdle);
                    try {
                        String concatString = Utils.concatString(this.f.toString(), "1", ",0,0,0", h());
                        t tVar = this.W;
                        t.a(concatString);
                        this.Z.c(Utils.concatString("send VideoClkTrack to ", concatString));
                        break;
                    } catch (Exception e) {
                        String concatString2 = Utils.concatString("Exceptions in onSendVideoClkTrack, err = ", e.toString());
                        this.Z.c(concatString2);
                        Log.e("AdChinaError", concatString2);
                        break;
                    } finally {
                        a(f.ESendVideoThdClkTrack);
                        str4 = "-- onSendVideoClkTrack";
                        this.Z.c(str4);
                    }
                case AnimationManager.FADEIN_ANIMATION /*8*/:
                    a(f.EIdle);
                    int size = this.Q.size();
                    this.Z.c(Utils.concatString("++ onSendVideoThdClkTrack, Size of list is ", Integer.valueOf(size)));
                    if (size > 0) {
                        try {
                            String str5 = (String) this.Q.get(size - 1);
                            this.Q.removeLast();
                            t tVar2 = this.W;
                            t.a(str5);
                            this.Z.c(Utils.concatString("send VideoThdClkTrack to ", str5));
                            break;
                        } catch (Exception e2) {
                            String concatString3 = Utils.concatString("Failed to onSendVideoThdClkTrack, err = ", e2.toString());
                            this.Z.c(concatString3);
                            Log.e("AdChinaError", concatString3);
                            break;
                        } finally {
                            a(f.ESendVideoThdClkTrack);
                            str3 = "-- onSendVideoThdClkTrack";
                            this.Z.c(str3);
                        }
                    }
                    break;
                case 9:
                    this.Z.c("++ onSendVideoCloseClkTrack");
                    a(f.EIdle);
                    try {
                        String concatString4 = Utils.concatString(this.f.toString(), "209", ",0,0,0", h());
                        t tVar3 = this.W;
                        t.a(concatString4);
                        this.Z.c(Utils.concatString("send VideoCloseClkTrack to ", concatString4));
                        break;
                    } catch (Exception e3) {
                        String concatString5 = Utils.concatString("Exceptions in onSendVideoCloseClkTrack, err = ", e3.toString());
                        this.Z.c(concatString5);
                        Log.e("AdChinaError", concatString5);
                        break;
                    } finally {
                        a(f.ESendVideoCloseThdClkTrack);
                        str2 = "-- onSendVideoCloseClkTrack";
                        this.Z.c(str2);
                    }
                case 10:
                    a(f.EIdle);
                    int size2 = this.ad.size();
                    this.Z.c(Utils.concatString("++ onSendVideoCloseThdClkTrack, Size of list is ", Integer.valueOf(size2)));
                    if (size2 > 0) {
                        try {
                            String str6 = (String) this.ad.get(size2 - 1);
                            this.ad.removeLast();
                            t tVar4 = this.W;
                            t.a(str6);
                            this.Z.c(Utils.concatString("send VideoCloseThdClkTrack to ", str6));
                        } catch (Exception e4) {
                            String concatString6 = Utils.concatString("Failed to SendVideoCloseThdClkTrack, err = ", e4.toString());
                            this.Z.c(concatString6);
                            Log.e("AdChinaError", concatString6);
                        } finally {
                            a(f.ESendVideoCloseThdClkTrack);
                            str = "-- onSendVideoCloseThdClkTrack";
                            this.Z.c(str);
                        }
                    } else {
                        a(3, "RefreshAd");
                    }
                    if (size2 == 0) {
                        this.ag.c();
                        break;
                    }
                    break;
            }
            try {
                Thread.sleep(50);
            } catch (InterruptedException e5) {
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:63:0x01b2 A[SYNTHETIC, Splitter:B:63:0x01b2] */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x01b7 A[Catch:{ IOException -> 0x01bf }] */
    /* JADX WARNING: Removed duplicated region for block: B:80:0x0214 A[SYNTHETIC, Splitter:B:80:0x0214] */
    /* JADX WARNING: Removed duplicated region for block: B:83:0x0219 A[Catch:{ IOException -> 0x0231 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(com.adchina.android.ads.a.d r12) {
        /*
            r11 = this;
        L_0x0000:
            boolean r0 = r11.I
            if (r0 != 0) goto L_0x0008
            boolean r0 = r12.a
            if (r0 == 0) goto L_0x0009
        L_0x0008:
            return
        L_0x0009:
            int[] r0 = p()
            com.adchina.android.ads.g r1 = r11.a
            int r1 = r1.ordinal()
            r0 = r0[r1]
            switch(r0) {
                case 1: goto L_0x0020;
                case 2: goto L_0x0018;
                case 3: goto L_0x0018;
                case 4: goto L_0x0018;
                case 5: goto L_0x0018;
                case 6: goto L_0x04bd;
                case 7: goto L_0x0018;
                case 8: goto L_0x0018;
                case 9: goto L_0x0274;
                case 10: goto L_0x030b;
                case 11: goto L_0x0391;
                case 12: goto L_0x0428;
                default: goto L_0x0018;
            }
        L_0x0018:
            r0 = 50
            java.lang.Thread.sleep(r0)     // Catch:{ InterruptedException -> 0x001e }
            goto L_0x0000
        L_0x001e:
            r0 = move-exception
            goto L_0x0000
        L_0x0020:
            com.adchina.android.ads.a.u r0 = r11.ag
            if (r0 == 0) goto L_0x0038
            com.adchina.android.ads.a.u r0 = r11.ag
            boolean r0 = r0.b()
            if (r0 != 0) goto L_0x0038
            com.adchina.android.ads.a.u r0 = r11.ag
            java.lang.StringBuffer r0 = r0.a()
            int r0 = r0.length()
            if (r0 > 0) goto L_0x0018
        L_0x0038:
            com.adchina.android.ads.h r0 = r11.Z
            java.lang.String r1 = "++ onReceiveAd"
            r0.c(r1)
            com.adchina.android.ads.g r0 = com.adchina.android.ads.g.EIdle
            r11.a(r0)
            r0 = 0
            boolean r1 = com.adchina.android.ads.AdManager.getDebugMode()     // Catch:{ Exception -> 0x04f8, all -> 0x04ef }
            if (r1 == 0) goto L_0x013d
            java.lang.String r1 = r11.j()     // Catch:{ Exception -> 0x04f8, all -> 0x04ef }
            if (r1 == 0) goto L_0x0071
            com.adchina.android.ads.h r2 = r11.Z     // Catch:{ Exception -> 0x04f8, all -> 0x04ef }
            r3 = 2
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ Exception -> 0x04f8, all -> 0x04ef }
            r4 = 0
            java.lang.String r5 = "AdserverUrl:"
            r3[r4] = r5     // Catch:{ Exception -> 0x04f8, all -> 0x04ef }
            r4 = 1
            r3[r4] = r1     // Catch:{ Exception -> 0x04f8, all -> 0x04ef }
            java.lang.String r3 = com.adchina.android.ads.Utils.concatString(r3)     // Catch:{ Exception -> 0x04f8, all -> 0x04ef }
            r2.c(r3)     // Catch:{ Exception -> 0x04f8, all -> 0x04ef }
            com.adchina.android.ads.t r2 = r11.W     // Catch:{ Exception -> 0x04f8, all -> 0x04ef }
            java.lang.StringBuffer r3 = r11.N     // Catch:{ Exception -> 0x04f8, all -> 0x04ef }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x04f8, all -> 0x04ef }
            java.io.InputStream r0 = r2.a(r1, r3)     // Catch:{ Exception -> 0x04f8, all -> 0x04ef }
        L_0x0071:
            r11.a(r0)     // Catch:{ Exception -> 0x01da, all -> 0x0220 }
            java.lang.StringBuffer r1 = r11.D     // Catch:{ Exception -> 0x01da, all -> 0x0220 }
            int r1 = r1.length()     // Catch:{ Exception -> 0x01da, all -> 0x0220 }
            if (r1 <= 0) goto L_0x0266
            com.adchina.android.ads.a.u r1 = new com.adchina.android.ads.a.u     // Catch:{ Exception -> 0x01da, all -> 0x0220 }
            r1.<init>(r11)     // Catch:{ Exception -> 0x01da, all -> 0x0220 }
            r11.ag = r1     // Catch:{ Exception -> 0x01da, all -> 0x0220 }
            com.adchina.android.ads.a.u r1 = r11.ag     // Catch:{ Exception -> 0x01da, all -> 0x0220 }
            java.lang.StringBuffer r2 = r11.D     // Catch:{ Exception -> 0x01da, all -> 0x0220 }
            r1.a(r2)     // Catch:{ Exception -> 0x01da, all -> 0x0220 }
            com.adchina.android.ads.h r1 = r11.Z     // Catch:{ Exception -> 0x01da, all -> 0x0220 }
            java.lang.String r2 = "loadLocalVideoList"
            r1.c(r2)     // Catch:{ Exception -> 0x01da, all -> 0x0220 }
            r1 = 0
            r2 = 0
            android.content.Context r3 = r11.J     // Catch:{ Exception -> 0x0194, all -> 0x020e }
            java.lang.String r4 = "adchinaVideos.fc"
            java.io.File r3 = r3.getFileStreamPath(r4)     // Catch:{ Exception -> 0x0194, all -> 0x020e }
            long r3 = r3.length()     // Catch:{ Exception -> 0x0194, all -> 0x020e }
            int r3 = (int) r3     // Catch:{ Exception -> 0x0194, all -> 0x020e }
            char[] r3 = new char[r3]     // Catch:{ Exception -> 0x0194, all -> 0x020e }
            android.content.Context r4 = r11.J     // Catch:{ Exception -> 0x0194, all -> 0x020e }
            java.lang.String r5 = "adchinaVideos.fc"
            java.io.FileInputStream r1 = r4.openFileInput(r5)     // Catch:{ Exception -> 0x0194, all -> 0x020e }
            java.io.InputStreamReader r4 = new java.io.InputStreamReader     // Catch:{ Exception -> 0x050d, all -> 0x04fe }
            r4.<init>(r1)     // Catch:{ Exception -> 0x050d, all -> 0x04fe }
            r4.read(r3)     // Catch:{ Exception -> 0x0513, all -> 0x0504 }
            java.lang.String r2 = new java.lang.String     // Catch:{ Exception -> 0x0513, all -> 0x0504 }
            r2.<init>(r3)     // Catch:{ Exception -> 0x0513, all -> 0x0504 }
            java.util.HashMap r3 = r11.ae     // Catch:{ Exception -> 0x0513, all -> 0x0504 }
            r3.clear()     // Catch:{ Exception -> 0x0513, all -> 0x0504 }
            java.lang.String r3 = "\n"
            java.lang.String[] r2 = r2.split(r3)     // Catch:{ Exception -> 0x0513, all -> 0x0504 }
            int r3 = r2.length     // Catch:{ Exception -> 0x0513, all -> 0x0504 }
            r5 = 0
        L_0x00c4:
            if (r5 < r3) goto L_0x0163
            r4.close()     // Catch:{ IOException -> 0x024b }
            if (r1 == 0) goto L_0x00ce
            r1.close()     // Catch:{ IOException -> 0x024b }
        L_0x00ce:
            r11.o()     // Catch:{ IOException -> 0x024b }
        L_0x00d1:
            java.util.HashMap r1 = r11.ae     // Catch:{ Exception -> 0x01da, all -> 0x0220 }
            java.lang.StringBuffer r2 = r11.D     // Catch:{ Exception -> 0x01da, all -> 0x0220 }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x01da, all -> 0x0220 }
            boolean r1 = r1.containsKey(r2)     // Catch:{ Exception -> 0x01da, all -> 0x0220 }
            if (r1 != 0) goto L_0x00f9
            android.content.Context r1 = r11.J     // Catch:{ Exception -> 0x01da, all -> 0x0220 }
            boolean r1 = com.adchina.android.ads.Utils.isWifiOrNotRoaming3G(r1)     // Catch:{ Exception -> 0x01da, all -> 0x0220 }
            if (r1 == 0) goto L_0x00f9
            com.adchina.android.ads.a.t r1 = new com.adchina.android.ads.a.t     // Catch:{ Exception -> 0x01da, all -> 0x0220 }
            com.adchina.android.ads.a.u r2 = r11.ag     // Catch:{ Exception -> 0x01da, all -> 0x0220 }
            java.lang.StringBuffer r2 = r2.a()     // Catch:{ Exception -> 0x01da, all -> 0x0220 }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x01da, all -> 0x0220 }
            r1.<init>(r11, r2)     // Catch:{ Exception -> 0x01da, all -> 0x0220 }
            r1.start()     // Catch:{ Exception -> 0x01da, all -> 0x0220 }
        L_0x00f9:
            r1 = 20
            java.lang.String r2 = "ReceivedVideo"
            r11.a(r1, r2)     // Catch:{ Exception -> 0x01da, all -> 0x0220 }
            java.lang.StringBuffer r1 = r11.h     // Catch:{ Exception -> 0x01da, all -> 0x0220 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x01da, all -> 0x0220 }
            java.util.LinkedList r2 = r11.Q     // Catch:{ Exception -> 0x01da, all -> 0x0220 }
            com.adchina.android.ads.Utils.splitTrackUrl(r1, r2)     // Catch:{ Exception -> 0x01da, all -> 0x0220 }
            java.lang.StringBuffer r1 = r11.E     // Catch:{ Exception -> 0x01da, all -> 0x0220 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x01da, all -> 0x0220 }
            java.util.LinkedList r2 = r11.ab     // Catch:{ Exception -> 0x01da, all -> 0x0220 }
            com.adchina.android.ads.Utils.splitTrackUrl(r1, r2)     // Catch:{ Exception -> 0x01da, all -> 0x0220 }
            java.lang.StringBuffer r1 = r11.F     // Catch:{ Exception -> 0x01da, all -> 0x0220 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x01da, all -> 0x0220 }
            java.util.LinkedList r2 = r11.ac     // Catch:{ Exception -> 0x01da, all -> 0x0220 }
            com.adchina.android.ads.Utils.splitTrackUrl(r1, r2)     // Catch:{ Exception -> 0x01da, all -> 0x0220 }
            java.lang.StringBuffer r1 = r11.G     // Catch:{ Exception -> 0x01da, all -> 0x0220 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x01da, all -> 0x0220 }
            java.util.LinkedList r2 = r11.ad     // Catch:{ Exception -> 0x01da, all -> 0x0220 }
            com.adchina.android.ads.Utils.splitTrackUrl(r1, r2)     // Catch:{ Exception -> 0x01da, all -> 0x0220 }
            r1 = 1
            r11.aa = r1     // Catch:{ Exception -> 0x01da, all -> 0x0220 }
        L_0x012f:
            com.adchina.android.ads.t r1 = r11.W
            com.adchina.android.ads.t.a(r0)
            com.adchina.android.ads.h r0 = r11.Z
            java.lang.String r1 = "-- onReceiveAd"
            r0.c(r1)
            goto L_0x0018
        L_0x013d:
            java.lang.String r1 = r11.i()     // Catch:{ Exception -> 0x04f8, all -> 0x04ef }
            com.adchina.android.ads.h r2 = r11.Z     // Catch:{ Exception -> 0x04f8, all -> 0x04ef }
            r3 = 2
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ Exception -> 0x04f8, all -> 0x04ef }
            r4 = 0
            java.lang.String r5 = "AdserverUrl:"
            r3[r4] = r5     // Catch:{ Exception -> 0x04f8, all -> 0x04ef }
            r4 = 1
            r3[r4] = r1     // Catch:{ Exception -> 0x04f8, all -> 0x04ef }
            java.lang.String r3 = com.adchina.android.ads.Utils.concatString(r3)     // Catch:{ Exception -> 0x04f8, all -> 0x04ef }
            r2.c(r3)     // Catch:{ Exception -> 0x04f8, all -> 0x04ef }
            com.adchina.android.ads.t r2 = r11.W     // Catch:{ Exception -> 0x04f8, all -> 0x04ef }
            java.lang.StringBuffer r3 = r11.N     // Catch:{ Exception -> 0x04f8, all -> 0x04ef }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x04f8, all -> 0x04ef }
            java.io.InputStream r0 = r2.a(r1, r3)     // Catch:{ Exception -> 0x04f8, all -> 0x04ef }
            goto L_0x0071
        L_0x0163:
            r6 = r2[r5]     // Catch:{ Exception -> 0x0513, all -> 0x0504 }
            java.lang.String r7 = "|||"
            int r7 = r6.indexOf(r7)     // Catch:{ Exception -> 0x0513, all -> 0x0504 }
            r8 = 0
            java.lang.String r8 = r6.substring(r8, r7)     // Catch:{ Exception -> 0x0513, all -> 0x0504 }
            int r7 = r7 + 3
            java.lang.String r6 = r6.substring(r7)     // Catch:{ Exception -> 0x0513, all -> 0x0504 }
            java.io.File r7 = new java.io.File     // Catch:{ Exception -> 0x0513, all -> 0x0504 }
            r7.<init>(r6)     // Catch:{ Exception -> 0x0513, all -> 0x0504 }
            boolean r9 = r7.exists()     // Catch:{ Exception -> 0x0513, all -> 0x0504 }
            if (r9 == 0) goto L_0x0190
            java.lang.String r7 = r7.getName()     // Catch:{ Exception -> 0x0513, all -> 0x0504 }
            boolean r7 = com.adchina.android.ads.Utils.isCachedFileTimeout(r7)     // Catch:{ Exception -> 0x0513, all -> 0x0504 }
            if (r7 != 0) goto L_0x0190
            java.util.HashMap r7 = r11.ae     // Catch:{ Exception -> 0x0513, all -> 0x0504 }
            r7.put(r8, r6)     // Catch:{ Exception -> 0x0513, all -> 0x0504 }
        L_0x0190:
            int r5 = r5 + 1
            goto L_0x00c4
        L_0x0194:
            r3 = move-exception
            r10 = r3
            r3 = r1
            r1 = r10
        L_0x0198:
            com.adchina.android.ads.h r4 = r11.Z     // Catch:{ all -> 0x050a }
            r5 = 2
            java.lang.Object[] r5 = new java.lang.Object[r5]     // Catch:{ all -> 0x050a }
            r6 = 0
            java.lang.String r7 = "Exceptions in loadLocalVideoList 1, err = "
            r5[r6] = r7     // Catch:{ all -> 0x050a }
            r6 = 1
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x050a }
            r5[r6] = r1     // Catch:{ all -> 0x050a }
            java.lang.String r1 = com.adchina.android.ads.Utils.concatString(r5)     // Catch:{ all -> 0x050a }
            r4.c(r1)     // Catch:{ all -> 0x050a }
            if (r2 == 0) goto L_0x01b5
            r2.close()     // Catch:{ IOException -> 0x01bf }
        L_0x01b5:
            if (r3 == 0) goto L_0x01ba
            r3.close()     // Catch:{ IOException -> 0x01bf }
        L_0x01ba:
            r11.o()     // Catch:{ IOException -> 0x01bf }
            goto L_0x00d1
        L_0x01bf:
            r1 = move-exception
            com.adchina.android.ads.h r2 = r11.Z     // Catch:{ Exception -> 0x01da, all -> 0x0220 }
            r3 = 2
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ Exception -> 0x01da, all -> 0x0220 }
            r4 = 0
            java.lang.String r5 = "Exceptions in loadLocalVideoList 2, err = "
            r3[r4] = r5     // Catch:{ Exception -> 0x01da, all -> 0x0220 }
            r4 = 1
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x01da, all -> 0x0220 }
            r3[r4] = r1     // Catch:{ Exception -> 0x01da, all -> 0x0220 }
            java.lang.String r1 = com.adchina.android.ads.Utils.concatString(r3)     // Catch:{ Exception -> 0x01da, all -> 0x0220 }
            r2.c(r1)     // Catch:{ Exception -> 0x01da, all -> 0x0220 }
            goto L_0x00d1
        L_0x01da:
            r1 = move-exception
            r10 = r1
            r1 = r0
            r0 = r10
        L_0x01de:
            r2 = 2
            java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch:{ all -> 0x04f5 }
            r3 = 0
            java.lang.String r4 = "Exceptions in onReceiveAd, err = "
            r2[r3] = r4     // Catch:{ all -> 0x04f5 }
            r3 = 1
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x04f5 }
            r2[r3] = r0     // Catch:{ all -> 0x04f5 }
            java.lang.String r0 = com.adchina.android.ads.Utils.concatString(r2)     // Catch:{ all -> 0x04f5 }
            r2 = 16
            r11.a(r2, r0)     // Catch:{ all -> 0x04f5 }
            com.adchina.android.ads.h r2 = r11.Z     // Catch:{ all -> 0x04f5 }
            r2.c(r0)     // Catch:{ all -> 0x04f5 }
            java.lang.String r2 = "AdChinaError"
            android.util.Log.e(r2, r0)     // Catch:{ all -> 0x04f5 }
            com.adchina.android.ads.t r0 = r11.W
            com.adchina.android.ads.t.a(r1)
            com.adchina.android.ads.h r0 = r11.Z
            java.lang.String r1 = "-- onReceiveAd"
            r0.c(r1)
            goto L_0x0018
        L_0x020e:
            r3 = move-exception
            r10 = r3
            r3 = r1
            r1 = r10
        L_0x0212:
            if (r2 == 0) goto L_0x0217
            r2.close()     // Catch:{ IOException -> 0x0231 }
        L_0x0217:
            if (r3 == 0) goto L_0x021c
            r3.close()     // Catch:{ IOException -> 0x0231 }
        L_0x021c:
            r11.o()     // Catch:{ IOException -> 0x0231 }
        L_0x021f:
            throw r1     // Catch:{ Exception -> 0x01da, all -> 0x0220 }
        L_0x0220:
            r1 = move-exception
            r10 = r1
            r1 = r0
            r0 = r10
        L_0x0224:
            com.adchina.android.ads.t r2 = r11.W
            com.adchina.android.ads.t.a(r1)
            com.adchina.android.ads.h r1 = r11.Z
            java.lang.String r2 = "-- onReceiveAd"
            r1.c(r2)
            throw r0
        L_0x0231:
            r2 = move-exception
            com.adchina.android.ads.h r3 = r11.Z     // Catch:{ Exception -> 0x01da, all -> 0x0220 }
            r4 = 2
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ Exception -> 0x01da, all -> 0x0220 }
            r5 = 0
            java.lang.String r6 = "Exceptions in loadLocalVideoList 2, err = "
            r4[r5] = r6     // Catch:{ Exception -> 0x01da, all -> 0x0220 }
            r5 = 1
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x01da, all -> 0x0220 }
            r4[r5] = r2     // Catch:{ Exception -> 0x01da, all -> 0x0220 }
            java.lang.String r2 = com.adchina.android.ads.Utils.concatString(r4)     // Catch:{ Exception -> 0x01da, all -> 0x0220 }
            r3.c(r2)     // Catch:{ Exception -> 0x01da, all -> 0x0220 }
            goto L_0x021f
        L_0x024b:
            r1 = move-exception
            com.adchina.android.ads.h r2 = r11.Z     // Catch:{ Exception -> 0x01da, all -> 0x0220 }
            r3 = 2
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ Exception -> 0x01da, all -> 0x0220 }
            r4 = 0
            java.lang.String r5 = "Exceptions in loadLocalVideoList 2, err = "
            r3[r4] = r5     // Catch:{ Exception -> 0x01da, all -> 0x0220 }
            r4 = 1
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x01da, all -> 0x0220 }
            r3[r4] = r1     // Catch:{ Exception -> 0x01da, all -> 0x0220 }
            java.lang.String r1 = com.adchina.android.ads.Utils.concatString(r3)     // Catch:{ Exception -> 0x01da, all -> 0x0220 }
            r2.c(r1)     // Catch:{ Exception -> 0x01da, all -> 0x0220 }
            goto L_0x00d1
        L_0x0266:
            r1 = 16
            java.lang.String r2 = ""
            r11.a(r1, r2)     // Catch:{ Exception -> 0x01da, all -> 0x0220 }
            com.adchina.android.ads.g r1 = com.adchina.android.ads.g.ERefreshAd     // Catch:{ Exception -> 0x01da, all -> 0x0220 }
            r11.a(r1)     // Catch:{ Exception -> 0x01da, all -> 0x0220 }
            goto L_0x012f
        L_0x0274:
            com.adchina.android.ads.h r0 = r11.Z
            java.lang.String r1 = "++ onSendVideoStartedImpTrack"
            r0.c(r1)
            r0 = 6
            java.lang.Object[] r0 = new java.lang.Object[r0]     // Catch:{ Exception -> 0x02d1 }
            r1 = 0
            java.lang.StringBuffer r2 = r11.f     // Catch:{ Exception -> 0x02d1 }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x02d1 }
            r0[r1] = r2     // Catch:{ Exception -> 0x02d1 }
            r1 = 1
            java.lang.String r2 = "207"
            r0[r1] = r2     // Catch:{ Exception -> 0x02d1 }
            r1 = 2
            java.lang.String r2 = ",0,0,0"
            r0[r1] = r2     // Catch:{ Exception -> 0x02d1 }
            r1 = 3
            java.lang.String r2 = r11.h()     // Catch:{ Exception -> 0x02d1 }
            r0[r1] = r2     // Catch:{ Exception -> 0x02d1 }
            r1 = 4
            java.lang.String r2 = "&tm="
            r0[r1] = r2     // Catch:{ Exception -> 0x02d1 }
            r1 = 5
            java.lang.String r2 = "yyyyMMddHHmmss"
            java.lang.String r2 = com.adchina.android.ads.Utils.getNowTime(r2)     // Catch:{ Exception -> 0x02d1 }
            r0[r1] = r2     // Catch:{ Exception -> 0x02d1 }
            java.lang.String r0 = com.adchina.android.ads.Utils.concatString(r0)     // Catch:{ Exception -> 0x02d1 }
            com.adchina.android.ads.t r1 = r11.W     // Catch:{ Exception -> 0x02d1 }
            com.adchina.android.ads.t.a(r0)     // Catch:{ Exception -> 0x02d1 }
            com.adchina.android.ads.h r1 = r11.Z     // Catch:{ Exception -> 0x02d1 }
            r2 = 2
            java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch:{ Exception -> 0x02d1 }
            r3 = 0
            java.lang.String r4 = "send VideoStartedImpTrack to "
            r2[r3] = r4     // Catch:{ Exception -> 0x02d1 }
            r3 = 1
            r2[r3] = r0     // Catch:{ Exception -> 0x02d1 }
            java.lang.String r0 = com.adchina.android.ads.Utils.concatString(r2)     // Catch:{ Exception -> 0x02d1 }
            r1.c(r0)     // Catch:{ Exception -> 0x02d1 }
            com.adchina.android.ads.g r0 = com.adchina.android.ads.g.ESendVideoStartedThdImpTrack
            r11.a(r0)
            com.adchina.android.ads.h r0 = r11.Z
            java.lang.String r1 = "-- onSendVideoStartedImpTrack"
            r0.c(r1)
            goto L_0x0018
        L_0x02d1:
            r0 = move-exception
            r1 = 2
            java.lang.Object[] r1 = new java.lang.Object[r1]     // Catch:{ all -> 0x02fd }
            r2 = 0
            java.lang.String r3 = "Exceptions in onSendVideoStartedImpTrack, err = "
            r1[r2] = r3     // Catch:{ all -> 0x02fd }
            r2 = 1
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x02fd }
            r1[r2] = r0     // Catch:{ all -> 0x02fd }
            java.lang.String r0 = com.adchina.android.ads.Utils.concatString(r1)     // Catch:{ all -> 0x02fd }
            com.adchina.android.ads.h r1 = r11.Z     // Catch:{ all -> 0x02fd }
            r1.c(r0)     // Catch:{ all -> 0x02fd }
            java.lang.String r1 = "AdChinaError"
            android.util.Log.e(r1, r0)     // Catch:{ all -> 0x02fd }
            com.adchina.android.ads.g r0 = com.adchina.android.ads.g.ESendVideoStartedThdImpTrack
            r11.a(r0)
            com.adchina.android.ads.h r0 = r11.Z
            java.lang.String r1 = "-- onSendVideoStartedImpTrack"
            r0.c(r1)
            goto L_0x0018
        L_0x02fd:
            r0 = move-exception
            com.adchina.android.ads.g r1 = com.adchina.android.ads.g.ESendVideoStartedThdImpTrack
            r11.a(r1)
            com.adchina.android.ads.h r1 = r11.Z
            java.lang.String r2 = "-- onSendVideoStartedImpTrack"
            r1.c(r2)
            throw r0
        L_0x030b:
            com.adchina.android.ads.g r0 = com.adchina.android.ads.g.EIdle
            r11.a(r0)
            java.util.LinkedList r0 = r11.ab
            int r0 = r0.size()
            com.adchina.android.ads.h r1 = r11.Z
            r2 = 2
            java.lang.Object[] r2 = new java.lang.Object[r2]
            r3 = 0
            java.lang.String r4 = "++ onSendVideoStartedThdImpTrack, Size of list is "
            r2[r3] = r4
            r3 = 1
            java.lang.Integer r4 = java.lang.Integer.valueOf(r0)
            r2[r3] = r4
            java.lang.String r2 = com.adchina.android.ads.Utils.concatString(r2)
            r1.c(r2)
            if (r0 <= 0) goto L_0x035d
            java.util.LinkedList r1 = r11.ab     // Catch:{ Exception -> 0x0366 }
            r2 = 1
            int r0 = r0 - r2
            java.lang.Object r0 = r1.get(r0)     // Catch:{ Exception -> 0x0366 }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ Exception -> 0x0366 }
            java.util.LinkedList r1 = r11.ab     // Catch:{ Exception -> 0x0366 }
            r1.removeLast()     // Catch:{ Exception -> 0x0366 }
            com.adchina.android.ads.t r1 = r11.W     // Catch:{ Exception -> 0x0366 }
            com.adchina.android.ads.t.a(r0)     // Catch:{ Exception -> 0x0366 }
            com.adchina.android.ads.h r1 = r11.Z     // Catch:{ Exception -> 0x0366 }
            r2 = 2
            java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch:{ Exception -> 0x0366 }
            r3 = 0
            java.lang.String r4 = "send VideoStartedThdImpTrack to "
            r2[r3] = r4     // Catch:{ Exception -> 0x0366 }
            r3 = 1
            r2[r3] = r0     // Catch:{ Exception -> 0x0366 }
            java.lang.String r0 = com.adchina.android.ads.Utils.concatString(r2)     // Catch:{ Exception -> 0x0366 }
            r1.c(r0)     // Catch:{ Exception -> 0x0366 }
            com.adchina.android.ads.g r0 = com.adchina.android.ads.g.ESendVideoStartedThdImpTrack
            r11.a(r0)
        L_0x035d:
            com.adchina.android.ads.h r0 = r11.Z
            java.lang.String r1 = "-- onSendVideoStartedThdImpTrack"
            r0.c(r1)
            goto L_0x0018
        L_0x0366:
            r0 = move-exception
            r1 = 2
            java.lang.Object[] r1 = new java.lang.Object[r1]     // Catch:{ all -> 0x038a }
            r2 = 0
            java.lang.String r3 = "Failed to onSendVideoStartedThdImpTrack, err = "
            r1[r2] = r3     // Catch:{ all -> 0x038a }
            r2 = 1
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x038a }
            r1[r2] = r0     // Catch:{ all -> 0x038a }
            java.lang.String r0 = com.adchina.android.ads.Utils.concatString(r1)     // Catch:{ all -> 0x038a }
            com.adchina.android.ads.h r1 = r11.Z     // Catch:{ all -> 0x038a }
            r1.c(r0)     // Catch:{ all -> 0x038a }
            java.lang.String r1 = "AdChinaError"
            android.util.Log.e(r1, r0)     // Catch:{ all -> 0x038a }
            com.adchina.android.ads.g r0 = com.adchina.android.ads.g.ESendVideoStartedThdImpTrack
            r11.a(r0)
            goto L_0x035d
        L_0x038a:
            r0 = move-exception
            com.adchina.android.ads.g r1 = com.adchina.android.ads.g.ESendVideoStartedThdImpTrack
            r11.a(r1)
            throw r0
        L_0x0391:
            com.adchina.android.ads.h r0 = r11.Z
            java.lang.String r1 = "++ onSendVideoEndedImpTrack"
            r0.c(r1)
            r0 = 6
            java.lang.Object[] r0 = new java.lang.Object[r0]     // Catch:{ Exception -> 0x03ee }
            r1 = 0
            java.lang.StringBuffer r2 = r11.f     // Catch:{ Exception -> 0x03ee }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x03ee }
            r0[r1] = r2     // Catch:{ Exception -> 0x03ee }
            r1 = 1
            java.lang.String r2 = "208"
            r0[r1] = r2     // Catch:{ Exception -> 0x03ee }
            r1 = 2
            java.lang.String r2 = ",0,0,0"
            r0[r1] = r2     // Catch:{ Exception -> 0x03ee }
            r1 = 3
            java.lang.String r2 = r11.h()     // Catch:{ Exception -> 0x03ee }
            r0[r1] = r2     // Catch:{ Exception -> 0x03ee }
            r1 = 4
            java.lang.String r2 = "&tm="
            r0[r1] = r2     // Catch:{ Exception -> 0x03ee }
            r1 = 5
            java.lang.String r2 = "yyyyMMddHHmmss"
            java.lang.String r2 = com.adchina.android.ads.Utils.getNowTime(r2)     // Catch:{ Exception -> 0x03ee }
            r0[r1] = r2     // Catch:{ Exception -> 0x03ee }
            java.lang.String r0 = com.adchina.android.ads.Utils.concatString(r0)     // Catch:{ Exception -> 0x03ee }
            com.adchina.android.ads.t r1 = r11.W     // Catch:{ Exception -> 0x03ee }
            com.adchina.android.ads.t.a(r0)     // Catch:{ Exception -> 0x03ee }
            com.adchina.android.ads.h r1 = r11.Z     // Catch:{ Exception -> 0x03ee }
            r2 = 2
            java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch:{ Exception -> 0x03ee }
            r3 = 0
            java.lang.String r4 = "send VideoEndedImpTrack to "
            r2[r3] = r4     // Catch:{ Exception -> 0x03ee }
            r3 = 1
            r2[r3] = r0     // Catch:{ Exception -> 0x03ee }
            java.lang.String r0 = com.adchina.android.ads.Utils.concatString(r2)     // Catch:{ Exception -> 0x03ee }
            r1.c(r0)     // Catch:{ Exception -> 0x03ee }
            com.adchina.android.ads.g r0 = com.adchina.android.ads.g.ESendVideoEndedThdImpTrack
            r11.a(r0)
            com.adchina.android.ads.h r0 = r11.Z
            java.lang.String r1 = "-- onSendVideoEndedImpTrack"
            r0.c(r1)
            goto L_0x0018
        L_0x03ee:
            r0 = move-exception
            r1 = 2
            java.lang.Object[] r1 = new java.lang.Object[r1]     // Catch:{ all -> 0x041a }
            r2 = 0
            java.lang.String r3 = "Exceptions in onSendVideoEndedImpTrack, err = "
            r1[r2] = r3     // Catch:{ all -> 0x041a }
            r2 = 1
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x041a }
            r1[r2] = r0     // Catch:{ all -> 0x041a }
            java.lang.String r0 = com.adchina.android.ads.Utils.concatString(r1)     // Catch:{ all -> 0x041a }
            com.adchina.android.ads.h r1 = r11.Z     // Catch:{ all -> 0x041a }
            r1.c(r0)     // Catch:{ all -> 0x041a }
            java.lang.String r1 = "AdChinaError"
            android.util.Log.e(r1, r0)     // Catch:{ all -> 0x041a }
            com.adchina.android.ads.g r0 = com.adchina.android.ads.g.ESendVideoEndedThdImpTrack
            r11.a(r0)
            com.adchina.android.ads.h r0 = r11.Z
            java.lang.String r1 = "-- onSendVideoEndedImpTrack"
            r0.c(r1)
            goto L_0x0018
        L_0x041a:
            r0 = move-exception
            com.adchina.android.ads.g r1 = com.adchina.android.ads.g.ESendVideoEndedThdImpTrack
            r11.a(r1)
            com.adchina.android.ads.h r1 = r11.Z
            java.lang.String r2 = "-- onSendVideoEndedImpTrack"
            r1.c(r2)
            throw r0
        L_0x0428:
            com.adchina.android.ads.g r0 = com.adchina.android.ads.g.EIdle
            r11.a(r0)
            java.util.LinkedList r0 = r11.ac
            int r1 = r0.size()
            com.adchina.android.ads.h r0 = r11.Z
            r2 = 2
            java.lang.Object[] r2 = new java.lang.Object[r2]
            r3 = 0
            java.lang.String r4 = "++ onSendVideoEndedThdImpTrack, Size of list is "
            r2[r3] = r4
            r3 = 1
            java.lang.Integer r4 = java.lang.Integer.valueOf(r1)
            r2[r3] = r4
            java.lang.String r2 = com.adchina.android.ads.Utils.concatString(r2)
            r0.c(r2)
            if (r1 <= 0) goto L_0x04b6
            java.util.LinkedList r0 = r11.ac     // Catch:{ Exception -> 0x048b }
            r2 = 1
            int r2 = r1 - r2
            java.lang.Object r0 = r0.get(r2)     // Catch:{ Exception -> 0x048b }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ Exception -> 0x048b }
            java.util.LinkedList r2 = r11.ac     // Catch:{ Exception -> 0x048b }
            r2.removeLast()     // Catch:{ Exception -> 0x048b }
            com.adchina.android.ads.t r2 = r11.W     // Catch:{ Exception -> 0x048b }
            com.adchina.android.ads.t.a(r0)     // Catch:{ Exception -> 0x048b }
            com.adchina.android.ads.h r2 = r11.Z     // Catch:{ Exception -> 0x048b }
            r3 = 2
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ Exception -> 0x048b }
            r4 = 0
            java.lang.String r5 = "send VideoEndedThdImpTrack to "
            r3[r4] = r5     // Catch:{ Exception -> 0x048b }
            r4 = 1
            r3[r4] = r0     // Catch:{ Exception -> 0x048b }
            java.lang.String r0 = com.adchina.android.ads.Utils.concatString(r3)     // Catch:{ Exception -> 0x048b }
            r2.c(r0)     // Catch:{ Exception -> 0x048b }
            com.adchina.android.ads.g r0 = com.adchina.android.ads.g.ESendVideoEndedThdImpTrack
            r11.a(r0)
        L_0x047b:
            if (r1 != 0) goto L_0x0482
            com.adchina.android.ads.a.u r0 = r11.ag
            r0.c()
        L_0x0482:
            com.adchina.android.ads.h r0 = r11.Z
            java.lang.String r1 = "-- onSendVideoEndedThdImpTrack"
            r0.c(r1)
            goto L_0x0018
        L_0x048b:
            r0 = move-exception
            r2 = 2
            java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch:{ all -> 0x04af }
            r3 = 0
            java.lang.String r4 = "Failed to onSendVideoEndedThdImpTrack, err = "
            r2[r3] = r4     // Catch:{ all -> 0x04af }
            r3 = 1
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x04af }
            r2[r3] = r0     // Catch:{ all -> 0x04af }
            java.lang.String r0 = com.adchina.android.ads.Utils.concatString(r2)     // Catch:{ all -> 0x04af }
            com.adchina.android.ads.h r2 = r11.Z     // Catch:{ all -> 0x04af }
            r2.c(r0)     // Catch:{ all -> 0x04af }
            java.lang.String r2 = "AdChinaError"
            android.util.Log.e(r2, r0)     // Catch:{ all -> 0x04af }
            com.adchina.android.ads.g r0 = com.adchina.android.ads.g.ESendVideoEndedThdImpTrack
            r11.a(r0)
            goto L_0x047b
        L_0x04af:
            r0 = move-exception
            com.adchina.android.ads.g r1 = com.adchina.android.ads.g.ESendVideoEndedThdImpTrack
            r11.a(r1)
            throw r0
        L_0x04b6:
            r0 = 3
            java.lang.String r2 = "RefreshAd"
            r11.a(r0, r2)
            goto L_0x047b
        L_0x04bd:
            boolean r0 = r11.ai
            if (r0 != 0) goto L_0x04c4
            r11.l()
        L_0x04c4:
            com.adchina.android.ads.h r0 = r11.Z
            java.lang.String r1 = "++ onRefreshAd"
            r0.c(r1)
            int r0 = r11.aa     // Catch:{ InterruptedException -> 0x04e1, all -> 0x04e8 }
            int r0 = r0 * 1000
            long r0 = (long) r0     // Catch:{ InterruptedException -> 0x04e1, all -> 0x04e8 }
            java.lang.Thread.sleep(r0)     // Catch:{ InterruptedException -> 0x04e1, all -> 0x04e8 }
            com.adchina.android.ads.g r0 = com.adchina.android.ads.g.EReceiveAd
            r11.a(r0)
        L_0x04d8:
            com.adchina.android.ads.h r0 = r11.Z
            java.lang.String r1 = "-- onRefreshAd"
            r0.c(r1)
            goto L_0x0018
        L_0x04e1:
            r0 = move-exception
            com.adchina.android.ads.g r0 = com.adchina.android.ads.g.EReceiveAd
            r11.a(r0)
            goto L_0x04d8
        L_0x04e8:
            r0 = move-exception
            com.adchina.android.ads.g r1 = com.adchina.android.ads.g.EReceiveAd
            r11.a(r1)
            throw r0
        L_0x04ef:
            r1 = move-exception
            r10 = r1
            r1 = r0
            r0 = r10
            goto L_0x0224
        L_0x04f5:
            r0 = move-exception
            goto L_0x0224
        L_0x04f8:
            r1 = move-exception
            r10 = r1
            r1 = r0
            r0 = r10
            goto L_0x01de
        L_0x04fe:
            r3 = move-exception
            r10 = r3
            r3 = r1
            r1 = r10
            goto L_0x0212
        L_0x0504:
            r2 = move-exception
            r3 = r1
            r1 = r2
            r2 = r4
            goto L_0x0212
        L_0x050a:
            r1 = move-exception
            goto L_0x0212
        L_0x050d:
            r3 = move-exception
            r10 = r3
            r3 = r1
            r1 = r10
            goto L_0x0198
        L_0x0513:
            r2 = move-exception
            r3 = r1
            r1 = r2
            r2 = r4
            goto L_0x0198
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adchina.android.ads.a.q.a(com.adchina.android.ads.a.d):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x003c, code lost:
        if (new java.io.File(r0).exists() != false) goto L_0x003e;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(java.lang.Object r5) {
        /*
            r4 = this;
            r3 = 1
            com.adchina.android.ads.a.u r0 = r4.ag
            if (r0 == 0) goto L_0x0066
            com.adchina.android.ads.a.u r0 = r4.ag
            java.lang.StringBuffer r0 = r0.a()
            int r0 = r0.length()
            if (r0 <= 0) goto L_0x0066
            com.adchina.android.ads.a.u r0 = r4.ag
            boolean r0 = r0.b()
            if (r0 != 0) goto L_0x0066
            com.adchina.android.ads.a.u r0 = r4.ag
            java.lang.StringBuffer r0 = r0.a()
            java.lang.String r0 = r0.toString()
            java.util.HashMap r1 = r4.ae
            boolean r1 = r1.containsKey(r0)
            if (r1 == 0) goto L_0x005b
            java.util.HashMap r1 = r4.ae
            java.lang.Object r0 = r1.get(r0)
            java.lang.String r0 = (java.lang.String) r0
            java.io.File r1 = new java.io.File
            r1.<init>(r0)
            boolean r1 = r1.exists()
            if (r1 == 0) goto L_0x005b
        L_0x003e:
            java.util.UUID r1 = java.util.UUID.randomUUID()
            java.lang.String r1 = r1.toString()
            if (r5 == 0) goto L_0x004d
            java.util.HashMap r2 = r4.af
            r2.put(r1, r5)
        L_0x004d:
            if (r0 == 0) goto L_0x0055
            int r2 = r0.length()
            if (r2 != 0) goto L_0x0069
        L_0x0055:
            android.content.Context r0 = r4.J
            r4.a(r0, r1)
        L_0x005a:
            return
        L_0x005b:
            com.adchina.android.ads.a.u r0 = r4.ag
            java.lang.StringBuffer r0 = r0.a()
            java.lang.String r0 = r0.toString()
            goto L_0x003e
        L_0x0066:
            java.lang.String r0 = ""
            goto L_0x003e
        L_0x0069:
            r4.P = r3
            r4.O = r3
            android.content.Intent r2 = new android.content.Intent
            r2.<init>()
            r3 = 268435456(0x10000000, float:2.5243549E-29)
            r2.addFlags(r3)
            java.lang.String r3 = "ADUrl"
            r2.putExtra(r3, r0)
            java.lang.String r0 = "ArgName"
            r2.putExtra(r0, r1)
            android.content.Context r0 = r4.J
            java.lang.Class<com.adchina.android.ads.views.AdVideoPlayerActivity> r1 = com.adchina.android.ads.views.AdVideoPlayerActivity.class
            r2.setClass(r0, r1)
            android.content.Context r0 = r4.J
            r0.startActivity(r2)
            goto L_0x005a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adchina.android.ads.a.q.a(java.lang.Object):void");
    }

    public final void a(boolean z) {
        this.ai = z;
        k();
    }

    public final void b() {
        this.Z.c(Utils.concatString("++ onDisplayAd, mAdModel = ", this.b));
        a(7, "PlayVideo");
        a(g.ESendVideoStartedImpTrack);
        this.Z.c("-- onDisplayAd");
    }

    /* access modifiers changed from: protected */
    public final void b(String str) {
        a(str, "adchinaVideoFC.fc");
    }

    /* access modifiers changed from: protected */
    public final void c() {
        this.N.setLength(0);
        this.N.append(AdManager.getVideoAdspaceId());
    }

    /* access modifiers changed from: protected */
    public final String d() {
        return c("adchinaVideoFC.fc");
    }

    public final void e() {
        a(8, "FailedToPlayVideo");
    }

    public final void f() {
        a(9, "VideoPlayEnded");
    }

    /* access modifiers changed from: protected */
    public final void g() {
        super.g();
        this.ab.clear();
        this.ac.clear();
    }

    /* JADX WARNING: Removed duplicated region for block: B:3:0x000a  */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x00fa A[SYNTHETIC, Splitter:B:59:0x00fa] */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x00ff A[Catch:{ IOException -> 0x0103 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean handleMessage(android.os.Message r10) {
        /*
            r9 = this;
            r4 = 0
            r7 = 1
            r3 = 0
            int r0 = r10.what
            switch(r0) {
                case 3: goto L_0x0059;
                case 6: goto L_0x005f;
                case 7: goto L_0x0012;
                case 8: goto L_0x003d;
                case 9: goto L_0x0053;
                case 16: goto L_0x002a;
                case 20: goto L_0x001e;
                default: goto L_0x0008;
            }
        L_0x0008:
            if (r10 == 0) goto L_0x0011
            android.os.Handler r0 = r9.Y
            int r1 = r10.what
            r0.removeMessages(r1)
        L_0x0011:
            return r7
        L_0x0012:
            com.adchina.android.ads.AdListener r0 = com.adchina.android.ads.a.q.X
            if (r0 == 0) goto L_0x0008
            com.adchina.android.ads.AdListener r0 = com.adchina.android.ads.a.q.X     // Catch:{ Exception -> 0x001c }
            r0.onPlayVideoAd()     // Catch:{ Exception -> 0x001c }
            goto L_0x0008
        L_0x001c:
            r0 = move-exception
            goto L_0x0008
        L_0x001e:
            com.adchina.android.ads.AdListener r0 = com.adchina.android.ads.a.q.X
            if (r0 == 0) goto L_0x0008
            com.adchina.android.ads.AdListener r0 = com.adchina.android.ads.a.q.X     // Catch:{ Exception -> 0x0028 }
            r0.onReceiveVideoAd()     // Catch:{ Exception -> 0x0028 }
            goto L_0x0008
        L_0x0028:
            r0 = move-exception
            goto L_0x0008
        L_0x002a:
            com.adchina.android.ads.AdListener r0 = com.adchina.android.ads.a.q.X
            if (r0 == 0) goto L_0x0033
            com.adchina.android.ads.AdListener r0 = com.adchina.android.ads.a.q.X     // Catch:{ Exception -> 0x011b }
            r0.onFailedToReceiveVideoAd()     // Catch:{ Exception -> 0x011b }
        L_0x0033:
            r0 = 30
            r9.aa = r0
            com.adchina.android.ads.g r0 = com.adchina.android.ads.g.ERefreshAd
            r9.a(r0)
            goto L_0x0008
        L_0x003d:
            com.adchina.android.ads.AdListener r0 = com.adchina.android.ads.a.q.X
            if (r0 == 0) goto L_0x0046
            com.adchina.android.ads.AdListener r0 = com.adchina.android.ads.a.q.X     // Catch:{ Exception -> 0x0118 }
            r0.onFailedToPlayVideoAd()     // Catch:{ Exception -> 0x0118 }
        L_0x0046:
            r9.O = r3
            com.adchina.android.ads.a.u r0 = r9.ag
            r0.c()
            com.adchina.android.ads.g r0 = com.adchina.android.ads.g.ERefreshAd
            r9.a(r0)
            goto L_0x0008
        L_0x0053:
            com.adchina.android.ads.g r0 = com.adchina.android.ads.g.ESendVideoEndedImpTrack
            r9.a(r0)
            goto L_0x0008
        L_0x0059:
            com.adchina.android.ads.g r0 = com.adchina.android.ads.g.ERefreshAd
            r9.a(r0)
            goto L_0x0008
        L_0x005f:
            java.lang.Object r0 = r10.obj
            java.lang.String[] r0 = (java.lang.String[]) r0
            java.util.HashMap r1 = r9.ae
            r2 = r0[r3]
            boolean r1 = r1.containsKey(r2)
            if (r1 != 0) goto L_0x0008
            java.util.HashMap r1 = r9.ae
            r2 = r0[r3]
            r0 = r0[r7]
            r1.put(r2, r0)
            com.adchina.android.ads.h r0 = r9.Z
            java.lang.String r1 = "saveLocalVideoList"
            r0.c(r1)
            android.content.Context r0 = r9.J     // Catch:{ Exception -> 0x0110, all -> 0x00f5 }
            java.lang.String r1 = "adchinaVideos.fc"
            r2 = 0
            java.io.FileOutputStream r1 = r0.openFileOutput(r1, r2)     // Catch:{ Exception -> 0x0110, all -> 0x00f5 }
            java.io.OutputStreamWriter r2 = new java.io.OutputStreamWriter     // Catch:{ Exception -> 0x0114, all -> 0x0105 }
            r2.<init>(r1)     // Catch:{ Exception -> 0x0114, all -> 0x0105 }
            java.util.HashMap r0 = r9.ae     // Catch:{ Exception -> 0x00ca, all -> 0x0109 }
            java.util.Set r0 = r0.keySet()     // Catch:{ Exception -> 0x00ca, all -> 0x0109 }
            java.util.Iterator r3 = r0.iterator()     // Catch:{ Exception -> 0x00ca, all -> 0x0109 }
        L_0x0095:
            boolean r0 = r3.hasNext()     // Catch:{ Exception -> 0x00ca, all -> 0x0109 }
            if (r0 != 0) goto L_0x00ab
            r2.flush()     // Catch:{ Exception -> 0x00ca, all -> 0x0109 }
            r2.close()     // Catch:{ IOException -> 0x00a8 }
            if (r1 == 0) goto L_0x0008
            r1.close()     // Catch:{ IOException -> 0x00a8 }
            goto L_0x0008
        L_0x00a8:
            r0 = move-exception
            goto L_0x0008
        L_0x00ab:
            java.lang.Object r0 = r3.next()     // Catch:{ Exception -> 0x00ca, all -> 0x0109 }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ Exception -> 0x00ca, all -> 0x0109 }
            r2.write(r0)     // Catch:{ Exception -> 0x00ca, all -> 0x0109 }
            java.lang.String r4 = "|||"
            r2.write(r4)     // Catch:{ Exception -> 0x00ca, all -> 0x0109 }
            java.util.HashMap r4 = r9.ae     // Catch:{ Exception -> 0x00ca, all -> 0x0109 }
            java.lang.Object r0 = r4.get(r0)     // Catch:{ Exception -> 0x00ca, all -> 0x0109 }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ Exception -> 0x00ca, all -> 0x0109 }
            r2.write(r0)     // Catch:{ Exception -> 0x00ca, all -> 0x0109 }
            java.lang.String r0 = "\n"
            r2.write(r0)     // Catch:{ Exception -> 0x00ca, all -> 0x0109 }
            goto L_0x0095
        L_0x00ca:
            r0 = move-exception
            r8 = r2
            r2 = r1
            r1 = r8
        L_0x00ce:
            com.adchina.android.ads.h r3 = r9.Z     // Catch:{ all -> 0x010e }
            r4 = 2
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ all -> 0x010e }
            r5 = 0
            java.lang.String r6 = "Exceptions in saveLocalVideoList, err = "
            r4[r5] = r6     // Catch:{ all -> 0x010e }
            r5 = 1
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x010e }
            r4[r5] = r0     // Catch:{ all -> 0x010e }
            java.lang.String r0 = com.adchina.android.ads.Utils.concatString(r4)     // Catch:{ all -> 0x010e }
            r3.c(r0)     // Catch:{ all -> 0x010e }
            if (r1 == 0) goto L_0x00eb
            r1.close()     // Catch:{ IOException -> 0x00f2 }
        L_0x00eb:
            if (r2 == 0) goto L_0x0008
            r2.close()     // Catch:{ IOException -> 0x00f2 }
            goto L_0x0008
        L_0x00f2:
            r0 = move-exception
            goto L_0x0008
        L_0x00f5:
            r0 = move-exception
            r1 = r4
            r2 = r4
        L_0x00f8:
            if (r1 == 0) goto L_0x00fd
            r1.close()     // Catch:{ IOException -> 0x0103 }
        L_0x00fd:
            if (r2 == 0) goto L_0x0102
            r2.close()     // Catch:{ IOException -> 0x0103 }
        L_0x0102:
            throw r0
        L_0x0103:
            r1 = move-exception
            goto L_0x0102
        L_0x0105:
            r0 = move-exception
            r2 = r1
            r1 = r4
            goto L_0x00f8
        L_0x0109:
            r0 = move-exception
            r8 = r2
            r2 = r1
            r1 = r8
            goto L_0x00f8
        L_0x010e:
            r0 = move-exception
            goto L_0x00f8
        L_0x0110:
            r0 = move-exception
            r1 = r4
            r2 = r4
            goto L_0x00ce
        L_0x0114:
            r0 = move-exception
            r2 = r1
            r1 = r4
            goto L_0x00ce
        L_0x0118:
            r0 = move-exception
            goto L_0x0046
        L_0x011b:
            r0 = move-exception
            goto L_0x0033
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adchina.android.ads.a.q.handleMessage(android.os.Message):boolean");
    }

    public final void m() {
        this.Z.c("++ onClosedVideo");
        a(f.ESendVideoCloseClkTrack);
        this.Z.c("-- onClosedVideo");
    }

    public final void n() {
        this.Z.c("++ onVideoClick");
        this.Z.c(Utils.concatString("++ open ", this.e, " with browser"));
        AdEngine.getAdEngine().openBrowser(this.e.toString());
        this.Z.c("-- open browser");
        a(f.ESendVideoClkTrack);
        this.Z.c("-- onVideoClick");
    }
}
