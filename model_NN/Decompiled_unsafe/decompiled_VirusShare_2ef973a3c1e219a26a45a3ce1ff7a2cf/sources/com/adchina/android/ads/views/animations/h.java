package com.adchina.android.ads.views.animations;

import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import com.adchina.android.ads.views.ContentView;

public final class h {
    /* access modifiers changed from: private */
    public ContentView a;

    public h(ContentView contentView) {
        this.a = contentView;
    }

    public final void a() {
        AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 0.8f);
        alphaAnimation.setDuration(700);
        alphaAnimation.setFillAfter(true);
        alphaAnimation.setInterpolator(new AccelerateInterpolator());
        alphaAnimation.setAnimationListener(new i(this));
        this.a.startAnimation(alphaAnimation);
    }
}
