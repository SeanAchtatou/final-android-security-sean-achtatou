package com.adchina.android.ads;

import android.content.Context;
import android.util.Log;
import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedList;
import java.util.List;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.CookieStore;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.cookie.Cookie;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.cookie.BasicClientCookie;

public final class t {
    private List a = new LinkedList();
    private List b = new LinkedList();
    private Context c = null;
    private boolean d = false;
    private h e;

    public t(Context context) {
        this.c = context;
    }

    public static void a(InputStream inputStream) {
        if (inputStream != null) {
            try {
                inputStream.close();
            } catch (IOException e2) {
                Log.e("AdChinaError", Utils.concatString("closeStream:", e2.toString()));
            }
        }
    }

    private void a(DefaultHttpClient defaultHttpClient) {
        if (!this.a.isEmpty()) {
            CookieStore cookieStore = defaultHttpClient.getCookieStore();
            cookieStore.clear();
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 >= this.a.size()) {
                    defaultHttpClient.setCookieStore(cookieStore);
                    return;
                }
                BasicClientCookie basicClientCookie = (BasicClientCookie) this.a.get(i2);
                BasicClientCookie basicClientCookie2 = new BasicClientCookie(basicClientCookie.getName(), basicClientCookie.getValue());
                basicClientCookie2.setDomain(basicClientCookie.getDomain());
                cookieStore.addCookie(basicClientCookie2);
                i = i2 + 1;
            }
        }
    }

    private void a(DefaultHttpClient defaultHttpClient, String str) {
        boolean z;
        if (this.e != null) {
            this.e.d("Cookies:");
        }
        List cookies = defaultHttpClient.getCookieStore().getCookies();
        if (!cookies.isEmpty()) {
            this.b.clear();
            for (int i = 0; i < cookies.size(); i++) {
                Cookie cookie = (Cookie) cookies.get(i);
                BasicClientCookie basicClientCookie = new BasicClientCookie(cookie.getName(), cookie.getValue());
                basicClientCookie.setDomain(cookie.getDomain());
                basicClientCookie.setExpiryDate(cookie.getExpiryDate());
                if (!(basicClientCookie.getValue() == null || this.e == null)) {
                    this.e.d(String.valueOf(basicClientCookie.getName()) + ":" + basicClientCookie.getValue());
                }
                this.b.add(basicClientCookie);
            }
            for (int i2 = 0; i2 < this.a.size(); i2++) {
                int i3 = 0;
                while (true) {
                    if (i3 >= cookies.size()) {
                        z = false;
                        break;
                    } else if (((BasicClientCookie) this.a.get(i2)).getName().compareToIgnoreCase(((Cookie) cookies.get(i3)).getName()) == 0) {
                        z = true;
                        break;
                    } else {
                        i3++;
                    }
                }
                if (!z && ((BasicClientCookie) this.a.get(i2)).getValue() != null) {
                    this.b.add((BasicClientCookie) this.a.get(i2));
                }
            }
            d(str);
        }
    }

    public static boolean a(String str) {
        if (str.length() <= 0) {
            return false;
        }
        DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
        HttpGet httpGet = new HttpGet(str);
        httpGet.setHeader("Connection", "close");
        return defaultHttpClient.execute(httpGet).getStatusLine().getStatusCode() == 200;
    }

    public static InputStream b(String str) {
        HttpEntity entity;
        DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
        HttpGet httpGet = new HttpGet(str);
        httpGet.setHeader("Connection", "close");
        HttpResponse execute = defaultHttpClient.execute(httpGet);
        if (execute.getStatusLine().getStatusCode() != 200 || (entity = execute.getEntity()) == null) {
            return null;
        }
        return entity.getContent();
    }

    private static String c(String str) {
        return Utils.concatString("adchinaCookie.ce", str);
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x003b  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x0134 A[SYNTHETIC, Splitter:B:40:0x0134] */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x0139 A[Catch:{ IOException -> 0x0140 }] */
    /* JADX WARNING: Removed duplicated region for block: B:60:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void d(java.lang.String r10) {
        /*
            r9 = this;
            r4 = 0
            r3 = 0
            com.adchina.android.ads.h r0 = r9.e
            if (r0 == 0) goto L_0x000d
            com.adchina.android.ads.h r0 = r9.e
            java.lang.String r1 = "++ writeCookieToFile"
            r0.c(r1)
        L_0x000d:
            java.util.List r0 = r9.a
            r0.clear()
            java.lang.String r0 = c(r10)     // Catch:{ Exception -> 0x0101, all -> 0x012f }
            android.content.Context r1 = r9.c     // Catch:{ Exception -> 0x0101, all -> 0x012f }
            r1.deleteFile(r0)     // Catch:{ Exception -> 0x0101, all -> 0x012f }
            android.content.Context r1 = r9.c     // Catch:{ Exception -> 0x0101, all -> 0x012f }
            r2 = 0
            java.io.FileOutputStream r1 = r1.openFileOutput(r0, r2)     // Catch:{ Exception -> 0x0101, all -> 0x012f }
            java.io.OutputStreamWriter r2 = new java.io.OutputStreamWriter     // Catch:{ Exception -> 0x014d, all -> 0x0142 }
            r2.<init>(r1)     // Catch:{ Exception -> 0x014d, all -> 0x0142 }
        L_0x0027:
            java.util.List r0 = r9.b     // Catch:{ Exception -> 0x0151, all -> 0x0146 }
            int r0 = r0.size()     // Catch:{ Exception -> 0x0151, all -> 0x0146 }
            if (r3 < r0) goto L_0x0043
            r2.close()     // Catch:{ IOException -> 0x013d }
            if (r1 == 0) goto L_0x0037
            r1.close()     // Catch:{ IOException -> 0x013d }
        L_0x0037:
            com.adchina.android.ads.h r0 = r9.e
            if (r0 == 0) goto L_0x0042
            com.adchina.android.ads.h r0 = r9.e
            java.lang.String r1 = "-- writeCookieToFile"
            r0.c(r1)
        L_0x0042:
            return
        L_0x0043:
            java.util.List r0 = r9.b     // Catch:{ Exception -> 0x0151, all -> 0x0146 }
            java.lang.Object r0 = r0.get(r3)     // Catch:{ Exception -> 0x0151, all -> 0x0146 }
            org.apache.http.impl.cookie.BasicClientCookie r0 = (org.apache.http.impl.cookie.BasicClientCookie) r0     // Catch:{ Exception -> 0x0151, all -> 0x0146 }
            java.lang.String r4 = r0.getValue()     // Catch:{ Exception -> 0x0151, all -> 0x0146 }
            if (r4 == 0) goto L_0x00fc
            java.util.List r4 = r9.a     // Catch:{ Exception -> 0x0151, all -> 0x0146 }
            r4.add(r0)     // Catch:{ Exception -> 0x0151, all -> 0x0146 }
            r4 = 3
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ Exception -> 0x0151, all -> 0x0146 }
            r5 = 0
            java.lang.String r6 = "name:"
            r4[r5] = r6     // Catch:{ Exception -> 0x0151, all -> 0x0146 }
            r5 = 1
            java.lang.String r6 = r0.getName()     // Catch:{ Exception -> 0x0151, all -> 0x0146 }
            r4[r5] = r6     // Catch:{ Exception -> 0x0151, all -> 0x0146 }
            r5 = 2
            java.lang.String r6 = "\r\n"
            r4[r5] = r6     // Catch:{ Exception -> 0x0151, all -> 0x0146 }
            java.lang.String r4 = com.adchina.android.ads.Utils.concatString(r4)     // Catch:{ Exception -> 0x0151, all -> 0x0146 }
            r2.write(r4)     // Catch:{ Exception -> 0x0151, all -> 0x0146 }
            r2.flush()     // Catch:{ Exception -> 0x0151, all -> 0x0146 }
            r4 = 3
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ Exception -> 0x0151, all -> 0x0146 }
            r5 = 0
            java.lang.String r6 = "value:"
            r4[r5] = r6     // Catch:{ Exception -> 0x0151, all -> 0x0146 }
            r5 = 1
            java.lang.String r6 = r0.getValue()     // Catch:{ Exception -> 0x0151, all -> 0x0146 }
            r4[r5] = r6     // Catch:{ Exception -> 0x0151, all -> 0x0146 }
            r5 = 2
            java.lang.String r6 = "\r\n"
            r4[r5] = r6     // Catch:{ Exception -> 0x0151, all -> 0x0146 }
            java.lang.String r4 = com.adchina.android.ads.Utils.concatString(r4)     // Catch:{ Exception -> 0x0151, all -> 0x0146 }
            r2.write(r4)     // Catch:{ Exception -> 0x0151, all -> 0x0146 }
            r2.flush()     // Catch:{ Exception -> 0x0151, all -> 0x0146 }
            r4 = 3
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ Exception -> 0x0151, all -> 0x0146 }
            r5 = 0
            java.lang.String r6 = "domain:"
            r4[r5] = r6     // Catch:{ Exception -> 0x0151, all -> 0x0146 }
            r5 = 1
            java.lang.String r6 = r0.getDomain()     // Catch:{ Exception -> 0x0151, all -> 0x0146 }
            r4[r5] = r6     // Catch:{ Exception -> 0x0151, all -> 0x0146 }
            r5 = 2
            java.lang.String r6 = "\r\n"
            r4[r5] = r6     // Catch:{ Exception -> 0x0151, all -> 0x0146 }
            java.lang.String r4 = com.adchina.android.ads.Utils.concatString(r4)     // Catch:{ Exception -> 0x0151, all -> 0x0146 }
            r2.write(r4)     // Catch:{ Exception -> 0x0151, all -> 0x0146 }
            r2.flush()     // Catch:{ Exception -> 0x0151, all -> 0x0146 }
            com.adchina.android.ads.h r4 = r9.e     // Catch:{ Exception -> 0x0151, all -> 0x0146 }
            if (r4 == 0) goto L_0x00fc
            com.adchina.android.ads.h r4 = r9.e     // Catch:{ Exception -> 0x0151, all -> 0x0146 }
            r5 = 2
            java.lang.Object[] r5 = new java.lang.Object[r5]     // Catch:{ Exception -> 0x0151, all -> 0x0146 }
            r6 = 0
            java.lang.String r7 = "name:"
            r5[r6] = r7     // Catch:{ Exception -> 0x0151, all -> 0x0146 }
            r6 = 1
            java.lang.String r7 = r0.getName()     // Catch:{ Exception -> 0x0151, all -> 0x0146 }
            r5[r6] = r7     // Catch:{ Exception -> 0x0151, all -> 0x0146 }
            java.lang.String r5 = com.adchina.android.ads.Utils.concatString(r5)     // Catch:{ Exception -> 0x0151, all -> 0x0146 }
            r4.c(r5)     // Catch:{ Exception -> 0x0151, all -> 0x0146 }
            com.adchina.android.ads.h r4 = r9.e     // Catch:{ Exception -> 0x0151, all -> 0x0146 }
            r5 = 2
            java.lang.Object[] r5 = new java.lang.Object[r5]     // Catch:{ Exception -> 0x0151, all -> 0x0146 }
            r6 = 0
            java.lang.String r7 = "value:"
            r5[r6] = r7     // Catch:{ Exception -> 0x0151, all -> 0x0146 }
            r6 = 1
            java.lang.String r7 = r0.getValue()     // Catch:{ Exception -> 0x0151, all -> 0x0146 }
            r5[r6] = r7     // Catch:{ Exception -> 0x0151, all -> 0x0146 }
            java.lang.String r5 = com.adchina.android.ads.Utils.concatString(r5)     // Catch:{ Exception -> 0x0151, all -> 0x0146 }
            r4.c(r5)     // Catch:{ Exception -> 0x0151, all -> 0x0146 }
            com.adchina.android.ads.h r4 = r9.e     // Catch:{ Exception -> 0x0151, all -> 0x0146 }
            r5 = 2
            java.lang.Object[] r5 = new java.lang.Object[r5]     // Catch:{ Exception -> 0x0151, all -> 0x0146 }
            r6 = 0
            java.lang.String r7 = "domain:"
            r5[r6] = r7     // Catch:{ Exception -> 0x0151, all -> 0x0146 }
            r6 = 1
            java.lang.String r0 = r0.getDomain()     // Catch:{ Exception -> 0x0151, all -> 0x0146 }
            r5[r6] = r0     // Catch:{ Exception -> 0x0151, all -> 0x0146 }
            java.lang.String r0 = com.adchina.android.ads.Utils.concatString(r5)     // Catch:{ Exception -> 0x0151, all -> 0x0146 }
            r4.c(r0)     // Catch:{ Exception -> 0x0151, all -> 0x0146 }
        L_0x00fc:
            int r0 = r3 + 1
            r3 = r0
            goto L_0x0027
        L_0x0101:
            r0 = move-exception
            r1 = r4
            r2 = r4
        L_0x0104:
            com.adchina.android.ads.h r3 = r9.e     // Catch:{ all -> 0x014b }
            if (r3 == 0) goto L_0x0120
            com.adchina.android.ads.h r3 = r9.e     // Catch:{ all -> 0x014b }
            r4 = 2
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ all -> 0x014b }
            r5 = 0
            java.lang.String r6 = "Exceptions in writeCookieToFile, err = "
            r4[r5] = r6     // Catch:{ all -> 0x014b }
            r5 = 1
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x014b }
            r4[r5] = r0     // Catch:{ all -> 0x014b }
            java.lang.String r0 = com.adchina.android.ads.Utils.concatString(r4)     // Catch:{ all -> 0x014b }
            r3.c(r0)     // Catch:{ all -> 0x014b }
        L_0x0120:
            if (r1 == 0) goto L_0x0125
            r1.close()     // Catch:{ IOException -> 0x012c }
        L_0x0125:
            if (r2 == 0) goto L_0x0037
            r2.close()     // Catch:{ IOException -> 0x012c }
            goto L_0x0037
        L_0x012c:
            r0 = move-exception
            goto L_0x0037
        L_0x012f:
            r0 = move-exception
            r1 = r4
            r2 = r4
        L_0x0132:
            if (r1 == 0) goto L_0x0137
            r1.close()     // Catch:{ IOException -> 0x0140 }
        L_0x0137:
            if (r2 == 0) goto L_0x013c
            r2.close()     // Catch:{ IOException -> 0x0140 }
        L_0x013c:
            throw r0
        L_0x013d:
            r0 = move-exception
            goto L_0x0037
        L_0x0140:
            r1 = move-exception
            goto L_0x013c
        L_0x0142:
            r0 = move-exception
            r2 = r1
            r1 = r4
            goto L_0x0132
        L_0x0146:
            r0 = move-exception
            r8 = r2
            r2 = r1
            r1 = r8
            goto L_0x0132
        L_0x014b:
            r0 = move-exception
            goto L_0x0132
        L_0x014d:
            r0 = move-exception
            r2 = r1
            r1 = r4
            goto L_0x0104
        L_0x0151:
            r0 = move-exception
            r8 = r2
            r2 = r1
            r1 = r8
            goto L_0x0104
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adchina.android.ads.t.d(java.lang.String):void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:25:0x00de A[Catch:{ all -> 0x0189 }] */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x00f8 A[SYNTHETIC, Splitter:B:27:0x00f8] */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x00fd A[Catch:{ IOException -> 0x010c }] */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0104  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x0130 A[SYNTHETIC, Splitter:B:41:0x0130] */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x0135 A[Catch:{ IOException -> 0x0139 }] */
    /* JADX WARNING: Removed duplicated region for block: B:70:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void e(java.lang.String r13) {
        /*
            r12 = this;
            com.adchina.android.ads.h r0 = r12.e
            if (r0 == 0) goto L_0x000b
            com.adchina.android.ads.h r0 = r12.e
            java.lang.String r1 = "++ readCookieFromFile"
            r0.c(r1)
        L_0x000b:
            java.util.List r0 = r12.a
            r0.clear()
            r0 = 0
            r1 = 0
            java.lang.String r2 = c(r13)     // Catch:{ Exception -> 0x00d6, all -> 0x012a }
            android.content.Context r3 = r12.c     // Catch:{ Exception -> 0x00d6, all -> 0x012a }
            java.io.File r3 = r3.getFileStreamPath(r2)     // Catch:{ Exception -> 0x00d6, all -> 0x012a }
            long r3 = r3.length()     // Catch:{ Exception -> 0x00d6, all -> 0x012a }
            int r3 = (int) r3     // Catch:{ Exception -> 0x00d6, all -> 0x012a }
            char[] r3 = new char[r3]     // Catch:{ Exception -> 0x00d6, all -> 0x012a }
            android.content.Context r4 = r12.c     // Catch:{ Exception -> 0x00d6, all -> 0x012a }
            java.io.FileInputStream r0 = r4.openFileInput(r2)     // Catch:{ Exception -> 0x00d6, all -> 0x012a }
            java.io.InputStreamReader r2 = new java.io.InputStreamReader     // Catch:{ Exception -> 0x018b, all -> 0x017e }
            r2.<init>(r0)     // Catch:{ Exception -> 0x018b, all -> 0x017e }
            r2.read(r3)     // Catch:{ Exception -> 0x0191, all -> 0x0183 }
            java.lang.String r1 = new java.lang.String     // Catch:{ Exception -> 0x0191, all -> 0x0183 }
            r1.<init>(r3)     // Catch:{ Exception -> 0x0191, all -> 0x0183 }
        L_0x0036:
            java.lang.String r3 = "name:"
            int r3 = r1.indexOf(r3)     // Catch:{ Exception -> 0x0191, all -> 0x0183 }
            r4 = -1
            if (r4 == r3) goto L_0x0157
            int r3 = r3 + 5
            java.lang.String r4 = "\r\n"
            int r4 = r1.indexOf(r4)     // Catch:{ Exception -> 0x0191, all -> 0x0183 }
            java.lang.String r3 = r1.substring(r3, r4)     // Catch:{ Exception -> 0x0191, all -> 0x0183 }
            int r4 = r4 + 2
            java.lang.String r1 = r1.substring(r4)     // Catch:{ Exception -> 0x0191, all -> 0x0183 }
            java.lang.String r4 = "value:"
            int r4 = r1.indexOf(r4)     // Catch:{ Exception -> 0x0191, all -> 0x0183 }
            r5 = -1
            if (r5 == r4) goto L_0x0157
            int r4 = r4 + 6
            java.lang.String r5 = "\r\n"
            int r5 = r1.indexOf(r5)     // Catch:{ Exception -> 0x0191, all -> 0x0183 }
            java.lang.String r4 = r1.substring(r4, r5)     // Catch:{ Exception -> 0x0191, all -> 0x0183 }
            int r5 = r5 + 2
            java.lang.String r1 = r1.substring(r5)     // Catch:{ Exception -> 0x0191, all -> 0x0183 }
            java.lang.String r5 = "domain:"
            int r5 = r1.indexOf(r5)     // Catch:{ Exception -> 0x0191, all -> 0x0183 }
            r6 = -1
            if (r6 == r5) goto L_0x0157
            int r5 = r5 + 7
            java.lang.String r6 = "\r\n"
            int r6 = r1.indexOf(r6)     // Catch:{ Exception -> 0x0191, all -> 0x0183 }
            java.lang.String r5 = r1.substring(r5, r6)     // Catch:{ Exception -> 0x0191, all -> 0x0183 }
            org.apache.http.impl.cookie.BasicClientCookie r7 = new org.apache.http.impl.cookie.BasicClientCookie     // Catch:{ Exception -> 0x0191, all -> 0x0183 }
            r7.<init>(r3, r4)     // Catch:{ Exception -> 0x0191, all -> 0x0183 }
            r7.setDomain(r5)     // Catch:{ Exception -> 0x0191, all -> 0x0183 }
            java.util.List r8 = r12.a     // Catch:{ Exception -> 0x0191, all -> 0x0183 }
            r8.add(r7)     // Catch:{ Exception -> 0x0191, all -> 0x0183 }
            com.adchina.android.ads.h r7 = r12.e     // Catch:{ Exception -> 0x0191, all -> 0x0183 }
            if (r7 == 0) goto L_0x00ce
            com.adchina.android.ads.h r7 = r12.e     // Catch:{ Exception -> 0x0191, all -> 0x0183 }
            r8 = 2
            java.lang.Object[] r8 = new java.lang.Object[r8]     // Catch:{ Exception -> 0x0191, all -> 0x0183 }
            r9 = 0
            java.lang.String r10 = "name:"
            r8[r9] = r10     // Catch:{ Exception -> 0x0191, all -> 0x0183 }
            r9 = 1
            r8[r9] = r3     // Catch:{ Exception -> 0x0191, all -> 0x0183 }
            java.lang.String r3 = com.adchina.android.ads.Utils.concatString(r8)     // Catch:{ Exception -> 0x0191, all -> 0x0183 }
            r7.c(r3)     // Catch:{ Exception -> 0x0191, all -> 0x0183 }
            com.adchina.android.ads.h r3 = r12.e     // Catch:{ Exception -> 0x0191, all -> 0x0183 }
            r7 = 2
            java.lang.Object[] r7 = new java.lang.Object[r7]     // Catch:{ Exception -> 0x0191, all -> 0x0183 }
            r8 = 0
            java.lang.String r9 = "value:"
            r7[r8] = r9     // Catch:{ Exception -> 0x0191, all -> 0x0183 }
            r8 = 1
            r7[r8] = r4     // Catch:{ Exception -> 0x0191, all -> 0x0183 }
            java.lang.String r4 = com.adchina.android.ads.Utils.concatString(r7)     // Catch:{ Exception -> 0x0191, all -> 0x0183 }
            r3.c(r4)     // Catch:{ Exception -> 0x0191, all -> 0x0183 }
            com.adchina.android.ads.h r3 = r12.e     // Catch:{ Exception -> 0x0191, all -> 0x0183 }
            r4 = 2
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ Exception -> 0x0191, all -> 0x0183 }
            r7 = 0
            java.lang.String r8 = "domain:"
            r4[r7] = r8     // Catch:{ Exception -> 0x0191, all -> 0x0183 }
            r7 = 1
            r4[r7] = r5     // Catch:{ Exception -> 0x0191, all -> 0x0183 }
            java.lang.String r4 = com.adchina.android.ads.Utils.concatString(r4)     // Catch:{ Exception -> 0x0191, all -> 0x0183 }
            r3.c(r4)     // Catch:{ Exception -> 0x0191, all -> 0x0183 }
        L_0x00ce:
            int r3 = r6 + 2
            java.lang.String r1 = r1.substring(r3)     // Catch:{ Exception -> 0x0191, all -> 0x0183 }
            goto L_0x0036
        L_0x00d6:
            r2 = move-exception
            r11 = r2
            r2 = r0
            r0 = r11
        L_0x00da:
            com.adchina.android.ads.h r3 = r12.e     // Catch:{ all -> 0x0189 }
            if (r3 == 0) goto L_0x00f6
            com.adchina.android.ads.h r3 = r12.e     // Catch:{ all -> 0x0189 }
            r4 = 2
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ all -> 0x0189 }
            r5 = 0
            java.lang.String r6 = "Exceptions in readCookieFromFile , err = "
            r4[r5] = r6     // Catch:{ all -> 0x0189 }
            r5 = 1
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0189 }
            r4[r5] = r0     // Catch:{ all -> 0x0189 }
            java.lang.String r0 = com.adchina.android.ads.Utils.concatString(r4)     // Catch:{ all -> 0x0189 }
            r3.c(r0)     // Catch:{ all -> 0x0189 }
        L_0x00f6:
            if (r1 == 0) goto L_0x00fb
            r1.close()     // Catch:{ IOException -> 0x010c }
        L_0x00fb:
            if (r2 == 0) goto L_0x0100
            r2.close()     // Catch:{ IOException -> 0x010c }
        L_0x0100:
            com.adchina.android.ads.h r0 = r12.e
            if (r0 == 0) goto L_0x010b
            com.adchina.android.ads.h r0 = r12.e
            java.lang.String r1 = "-- readCookieFromFile"
            r0.c(r1)
        L_0x010b:
            return
        L_0x010c:
            r0 = move-exception
            com.adchina.android.ads.h r1 = r12.e
            if (r1 == 0) goto L_0x0100
            com.adchina.android.ads.h r1 = r12.e
            r2 = 2
            java.lang.Object[] r2 = new java.lang.Object[r2]
            r3 = 0
            java.lang.String r4 = "Exceptions in readCookieFromFile release resource, err = "
            r2[r3] = r4
            r3 = 1
            java.lang.String r0 = r0.toString()
            r2[r3] = r0
            java.lang.String r0 = com.adchina.android.ads.Utils.concatString(r2)
            r1.c(r0)
            goto L_0x0100
        L_0x012a:
            r2 = move-exception
            r11 = r2
            r2 = r0
            r0 = r11
        L_0x012e:
            if (r1 == 0) goto L_0x0133
            r1.close()     // Catch:{ IOException -> 0x0139 }
        L_0x0133:
            if (r2 == 0) goto L_0x0138
            r2.close()     // Catch:{ IOException -> 0x0139 }
        L_0x0138:
            throw r0
        L_0x0139:
            r1 = move-exception
            com.adchina.android.ads.h r2 = r12.e
            if (r2 == 0) goto L_0x0138
            com.adchina.android.ads.h r2 = r12.e
            r3 = 2
            java.lang.Object[] r3 = new java.lang.Object[r3]
            r4 = 0
            java.lang.String r5 = "Exceptions in readCookieFromFile release resource, err = "
            r3[r4] = r5
            r4 = 1
            java.lang.String r1 = r1.toString()
            r3[r4] = r1
            java.lang.String r1 = com.adchina.android.ads.Utils.concatString(r3)
            r2.c(r1)
            goto L_0x0138
        L_0x0157:
            r2.close()     // Catch:{ IOException -> 0x0160 }
            if (r0 == 0) goto L_0x0100
            r0.close()     // Catch:{ IOException -> 0x0160 }
            goto L_0x0100
        L_0x0160:
            r0 = move-exception
            com.adchina.android.ads.h r1 = r12.e
            if (r1 == 0) goto L_0x0100
            com.adchina.android.ads.h r1 = r12.e
            r2 = 2
            java.lang.Object[] r2 = new java.lang.Object[r2]
            r3 = 0
            java.lang.String r4 = "Exceptions in readCookieFromFile release resource, err = "
            r2[r3] = r4
            r3 = 1
            java.lang.String r0 = r0.toString()
            r2[r3] = r0
            java.lang.String r0 = com.adchina.android.ads.Utils.concatString(r2)
            r1.c(r0)
            goto L_0x0100
        L_0x017e:
            r2 = move-exception
            r11 = r2
            r2 = r0
            r0 = r11
            goto L_0x012e
        L_0x0183:
            r1 = move-exception
            r11 = r1
            r1 = r2
            r2 = r0
            r0 = r11
            goto L_0x012e
        L_0x0189:
            r0 = move-exception
            goto L_0x012e
        L_0x018b:
            r2 = move-exception
            r11 = r2
            r2 = r0
            r0 = r11
            goto L_0x00da
        L_0x0191:
            r1 = move-exception
            r11 = r1
            r1 = r2
            r2 = r0
            r0 = r11
            goto L_0x00da
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adchina.android.ads.t.e(java.lang.String):void");
    }

    public final InputStream a(String str, String str2) {
        HttpEntity entity;
        DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
        HttpGet httpGet = new HttpGet(str);
        httpGet.setHeader("Connection", "close");
        if (!this.d) {
            e(str2);
            this.d = true;
        }
        a(defaultHttpClient);
        HttpResponse execute = defaultHttpClient.execute(httpGet);
        if (execute.getStatusLine().getStatusCode() != 200 || (entity = execute.getEntity()) == null) {
            return null;
        }
        InputStream content = entity.getContent();
        a(defaultHttpClient, str2);
        return content;
    }

    public final void a(h hVar) {
        this.e = hVar;
    }
}
