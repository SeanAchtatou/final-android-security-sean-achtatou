package com.adchina.android.ads;

import java.util.Vector;

public final class o {
    private Vector a = new Vector(1);
    private int b = 0;
    private int c;
    private int d;

    private o() {
    }

    public static o a(byte[] bArr) {
        try {
            o oVar = new o();
            p pVar = new p(bArr);
            oVar.c = pVar.h;
            oVar.d = pVar.i;
            while (pVar.a()) {
                try {
                    e c2 = pVar.c();
                    if (c2 != null && c2.b().getWidth() > 0 && c2.b().getHeight() > 0) {
                        oVar.a.addElement(c2);
                    }
                    pVar.b();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            pVar.d();
            return oVar;
        } catch (Exception e2) {
            e2.printStackTrace();
            return null;
        }
    }

    public final void a() {
        try {
            if (this.a != null && this.a.size() > 0) {
                int i = 0;
                while (true) {
                    int i2 = i;
                    if (i2 >= this.a.size()) {
                        break;
                    }
                    ((e) this.a.get(i2)).b().recycle();
                    i = i2 + 1;
                }
            }
            this.a.clear();
        } catch (Exception e) {
        }
    }

    public final int b() {
        return this.c;
    }

    public final int c() {
        return this.d;
    }

    public final o d() {
        o oVar = new o();
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= this.a.size()) {
                oVar.c = this.c;
                oVar.d = this.d;
                oVar.b = this.b;
                return oVar;
            }
            oVar.a.add(((e) this.a.get(i2)).clone());
            i = i2 + 1;
        }
    }

    public final e e() {
        if (this.a.size() == 0) {
            return null;
        }
        return (e) this.a.elementAt(this.b);
    }

    public final void f() {
        if (this.b + 1 < this.a.size()) {
            this.b++;
        } else {
            this.b = 0;
        }
    }
}
