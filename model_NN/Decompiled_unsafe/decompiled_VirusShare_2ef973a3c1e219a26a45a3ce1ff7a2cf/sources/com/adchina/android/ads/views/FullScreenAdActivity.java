package com.adchina.android.ads.views;

import android.app.Activity;
import android.content.res.Configuration;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.Display;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.adchina.android.ads.AdListener;
import com.adchina.android.ads.AdManager;
import com.adchina.android.ads.Utils;
import com.adchina.android.ads.a.e;
import java.util.Timer;

public class FullScreenAdActivity extends Activity implements View.OnClickListener, View.OnTouchListener {
    private Button mBtnClose;
    private boolean mClosed;
    private boolean mFinished;
    private GifImageView mGifView;
    private RelativeLayout mLayout;
    private AdListener mListener;
    /* access modifiers changed from: private */
    public int mNumber = 15;
    private int mScreenHeight;
    private int mScreenWidth;
    /* access modifiers changed from: private */
    public Timer mTimer;
    private Handler mTimerHandler;
    /* access modifiers changed from: private */
    public TextView mTxtTimer;

    /* access modifiers changed from: private */
    public void closeFullScreen() {
        this.mFinished = false;
        this.mClosed = true;
        if (this.mTimerHandler != null) {
            this.mTimerHandler.removeMessages(1);
            this.mTimerHandler = null;
        }
        if (this.mTimer != null) {
            this.mTimer.cancel();
        }
        this.mTimer = null;
        if (this.mGifView != null) {
            this.mGifView.a();
        }
        this.mGifView = null;
        e.a().f();
        e.a().a(getApplicationContext(), getIntent().getExtras().getString("ArgName"));
        finish();
    }

    /* access modifiers changed from: private */
    public void endFullScreen() {
        if (this.mTimerHandler != null) {
            this.mTimerHandler.removeMessages(1);
            this.mTimerHandler = null;
        }
        if (this.mTimer != null) {
            this.mTimer.cancel();
        }
        this.mTimer = null;
        this.mFinished = true;
        if (this.mGifView != null) {
            this.mGifView.a();
        }
        this.mGifView = null;
        e.a().b();
        e.a().a(getApplicationContext(), getIntent().getExtras().getString("ArgName"));
        finish();
    }

    private void setUpControls() {
        Display defaultDisplay = getWindowManager().getDefaultDisplay();
        this.mScreenWidth = defaultDisplay.getWidth();
        this.mScreenHeight = defaultDisplay.getHeight();
        int min = Math.min(this.mScreenHeight, this.mScreenWidth);
        if (this.mLayout == null) {
            LinearLayout linearLayout = new LinearLayout(getApplicationContext());
            linearLayout.setLayoutParams(new ViewGroup.LayoutParams(new ViewGroup.LayoutParams(-1, -1)));
            setContentView(linearLayout);
            this.mLayout = new RelativeLayout(getApplicationContext());
            this.mLayout.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
            linearLayout.addView(this.mLayout);
        }
        if (this.mTimerHandler == null) {
            this.mTimerHandler = new Handler(new l(this));
        }
        if (this.mTimer == null) {
            this.mTimer = new Timer();
            this.mNumber = e.a().m();
            if (this.mNumber <= 0) {
                this.mNumber = 15;
            }
        }
        if (this.mGifView == null) {
            this.mGifView = new GifImageView(getApplicationContext());
            this.mGifView.setOnClickListener(this);
            this.mGifView.setOnTouchListener(this);
            this.mLayout.addView(this.mGifView);
            this.mGifView.setFocusableInTouchMode(true);
            this.mGifView.setScaleType(ImageView.ScaleType.CENTER);
            this.mGifView.setFocusable(true);
            this.mGifView.setId(Math.round(((float) Math.random()) * 10000.0f));
            if (AdManager.getLoadingImg() == 0) {
                this.mGifView.a(Utils.loadAssetsInputStream(getApplicationContext(), AdManager.getDefaultLoadingGifPath()));
            } else {
                this.mGifView.a(AdManager.getLoadingImg());
            }
            e.a().a(this.mGifView, new h(this));
        }
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(min, min);
        layoutParams.addRule(13);
        this.mGifView.setLayoutParams(layoutParams);
        if (this.mTxtTimer == null) {
            this.mTxtTimer = new TextView(getApplicationContext());
            this.mLayout.addView(this.mTxtTimer);
            this.mTxtTimer.setGravity(16);
            this.mTxtTimer.bringToFront();
            this.mTxtTimer.setSingleLine(true);
        }
        this.mTxtTimer.setBackgroundColor(AdManager.getFullScreenTimerBgColor());
        this.mTxtTimer.setTextColor(AdManager.getFullScreenTimerTextColor());
        this.mTxtTimer.setText("广告剩余");
        this.mTxtTimer.append(new StringBuilder(String.valueOf(this.mNumber)).toString());
        this.mTxtTimer.append("秒");
        Utils.dip2px(this, 98.0f);
        Utils.dip2px(this, 32.0f);
        RelativeLayout.LayoutParams layoutParams2 = (RelativeLayout.LayoutParams) this.mTxtTimer.getLayoutParams();
        layoutParams2.addRule(11);
        layoutParams2.addRule(10);
        this.mTxtTimer.setLayoutParams(layoutParams2);
        this.mTxtTimer.setVisibility(8);
        if (AdManager.isShowFullScreenTimer()) {
            this.mTxtTimer.setVisibility(0);
        }
        if (this.mBtnClose == null) {
            this.mBtnClose = new Button(getApplicationContext());
            if (AdManager.getCloseImg() == 0) {
                this.mBtnClose.setBackgroundDrawable(new BitmapDrawable(Utils.loadAssetsBitmap(getApplicationContext(), AdManager.getDefaultCloseImgPath())));
            } else {
                this.mBtnClose.setBackgroundResource(AdManager.getCloseImg());
            }
            this.mLayout.addView(this.mBtnClose);
            this.mBtnClose.bringToFront();
            this.mBtnClose.setOnClickListener(new j(this));
        }
        RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(Utils.dip2px(this, 32.0f), Utils.dip2px(this, 32.0f));
        layoutParams3.addRule(12);
        layoutParams3.addRule(11);
        this.mBtnClose.setLayoutParams(layoutParams3);
        this.mLayout.invalidate();
    }

    public void onClick(View view) {
        e.a().e();
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        setUpControls();
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        Utils.setWindowBackgroundColor(getWindow(), AdManager.getAdWindowBackgroundColor(), AdManager.getAdWindowBackgroundOpacity());
        super.onCreate(bundle);
        if (bundle != null && bundle.containsKey("Time")) {
            if (this.mTimer != null) {
                this.mTimer.cancel();
            }
            this.mNumber = bundle.getInt("Time");
        }
        setUpControls();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        if (this.mTimer != null) {
            this.mTimer.cancel();
        }
        this.mTimer = null;
        boolean z = this.mFinished;
        super.onDestroy();
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i != 4) {
            return super.onKeyDown(i, keyEvent);
        }
        closeFullScreen();
        return true;
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Bundle bundle) {
        super.onRestoreInstanceState(bundle);
        if (bundle.containsKey("Time")) {
            this.mNumber = bundle.getInt("Time");
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (this.mTimer == null) {
            this.mTimer = new Timer();
            this.mTimer.schedule(new k(this), 1000, 1000);
        }
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putInt("Time", this.mNumber);
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        switch (motionEvent.getAction()) {
            case 0:
                e.a().e();
                return true;
            default:
                return true;
        }
    }

    /* access modifiers changed from: protected */
    public void sendMessage(Object obj) {
        Message message = new Message();
        message.what = 1;
        message.obj = obj;
        this.mTimerHandler.sendMessage(message);
    }
}
