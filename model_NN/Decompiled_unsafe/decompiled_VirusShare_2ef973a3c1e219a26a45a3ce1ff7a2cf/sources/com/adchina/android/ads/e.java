package com.adchina.android.ads;

import android.graphics.Bitmap;

public final class e {
    private Bitmap a;
    private int b;
    private int c;
    private int d = 100;

    public e(Bitmap bitmap, int i, int i2, int i3) {
        this.a = bitmap;
        this.b = i;
        this.c = i2;
        this.d = i3;
    }

    public final int a() {
        return this.d;
    }

    public final Bitmap b() {
        return this.a;
    }

    public final int c() {
        return this.b;
    }

    public final int d() {
        return this.c;
    }

    /* renamed from: e */
    public final e clone() {
        return new e(this.a.copy(this.a.getConfig(), true), this.b, this.c, this.d);
    }
}
