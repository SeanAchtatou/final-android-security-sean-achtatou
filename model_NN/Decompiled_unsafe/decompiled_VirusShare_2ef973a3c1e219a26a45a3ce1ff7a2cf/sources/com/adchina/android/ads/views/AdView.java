package com.adchina.android.ads.views;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;
import com.adchina.android.ads.AdEngine;
import com.adchina.android.ads.AdManager;
import com.adchina.android.ads.a.a;
import com.adchina.android.ads.o;

public class AdView extends LinearLayout implements View.OnClickListener {
    private a mAdViewController;
    private ContentView mContentView = null;
    private int mOriginVisibility = 8;

    public AdView(Context context) {
        super(context);
        initView(context);
    }

    public AdView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        initView(context);
        loadAdAttributes(context, attributeSet);
    }

    public AdView(Context context, String str, boolean z, boolean z2) {
        super(context);
        initView(context);
        startBannerAd(context, str, z, z2);
    }

    private void initView(Context context) {
        this.mContentView = new ContentView(context);
        this.mContentView.a(this);
        this.mContentView.setOnClickListener(this);
        addView(this.mContentView, new LinearLayout.LayoutParams(-1, -1));
    }

    private void loadAdAttributes(Context context, AttributeSet attributeSet) {
        if (attributeSet != null) {
            startBannerAd(context, attributeSet.getAttributeValue(null, "adspace_id"), attributeSet.getAttributeBooleanValue(null, "autoStart", false), attributeSet.getAttributeBooleanValue(null, "log", false));
        }
    }

    private void startBannerAd(Context context, String str, boolean z, boolean z2) {
        if (str != null && str.length() > 0) {
            AdManager.setAdspaceId(str);
            AdManager.setLogMode(z2);
            AdEngine.initAdEngine(getContext());
            this.mAdViewController = new a(context);
            this.mAdViewController.a(this);
            if (z) {
                this.mAdViewController.k();
            }
        }
    }

    public void onClick(View view) {
        if (this.mAdViewController != null) {
            this.mAdViewController.f();
        }
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (this.mAdViewController != null) {
            this.mAdViewController.b(this);
        }
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        switch (motionEvent.getAction()) {
            case 0:
                a aVar = this.mAdViewController;
                return true;
            default:
                return true;
        }
    }

    /* access modifiers changed from: protected */
    public void onWindowVisibilityChanged(int i) {
        if (this.mAdViewController != null) {
            if (i == 0) {
                if (this.mOriginVisibility == 0) {
                    setVisibility(0);
                    this.mAdViewController.b();
                }
            } else if (i == 8) {
                if (getVisibility() != 0) {
                    this.mOriginVisibility = getVisibility();
                } else {
                    this.mOriginVisibility = 0;
                    setVisibility(4);
                    this.mAdViewController.a();
                }
            }
            super.onWindowVisibilityChanged(i);
        }
    }

    public void setBackgroundColor(int i) {
        if (this.mContentView != null) {
            this.mContentView.setBackgroundColor(i);
        }
    }

    public void setContent(Bitmap bitmap) {
        if (this.mContentView != null) {
            this.mContentView.b(bitmap);
        }
    }

    public void setContent(o oVar) {
        if (this.mContentView != null) {
            this.mContentView.a(oVar);
        }
    }

    public void setContent(String str) {
        if (this.mContentView != null) {
            this.mContentView.a(str);
        }
    }

    public void setContent(byte[] bArr) {
        if (this.mContentView != null) {
            this.mContentView.a(o.a(bArr));
        }
    }

    public void setController(a aVar) {
        this.mAdViewController = aVar;
        this.mContentView.a(aVar);
    }

    public void setDefaultImage(int i) {
        setDefaultImage(new BitmapDrawable(getContext().getResources().openRawResource(i)).getBitmap());
    }

    public void setDefaultImage(Bitmap bitmap) {
        if (this.mContentView != null) {
            this.mContentView.a(bitmap);
        }
    }

    public void setFont(Typeface typeface) {
        if (this.mContentView != null) {
            this.mContentView.a(typeface);
        }
    }

    public void setPenColor(int i) {
        if (this.mContentView != null) {
            this.mContentView.a(i);
        }
    }

    public void start() {
        if (this.mAdViewController != null) {
            this.mAdViewController.a(this);
            this.mAdViewController.k();
        }
    }

    public void stop() {
        if (this.mAdViewController != null) {
            this.mAdViewController.l();
        }
    }
}
