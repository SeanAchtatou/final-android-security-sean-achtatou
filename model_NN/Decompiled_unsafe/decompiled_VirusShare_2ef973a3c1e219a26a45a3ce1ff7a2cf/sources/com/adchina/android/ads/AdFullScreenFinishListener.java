package com.adchina.android.ads;

import android.content.Context;

public interface AdFullScreenFinishListener {
    void finished(Context context, Object obj);
}
