package com.adchina.android.ads.a;

import com.adchina.android.ads.AdManager;
import com.adchina.android.ads.g;
import java.util.TimerTask;

final class p extends TimerTask {
    final /* synthetic */ o a;

    p(o oVar) {
        this.a = oVar;
    }

    public final void run() {
        if (this.a.af == null || this.a.af.length() <= 0) {
            this.a.ae.l();
            return;
        }
        AdManager.setAdspaceId(this.a.af);
        this.a.ae.a(g.ERefreshAd);
        this.a.ae.a(true);
    }
}
