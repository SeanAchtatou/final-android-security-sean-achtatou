package com.adchina.android.ads.views.animations;

import android.view.animation.Animation;

final class k implements Animation.AnimationListener {
    final /* synthetic */ j a;

    k(j jVar) {
        this.a = jVar;
    }

    public final void onAnimationEnd(Animation animation) {
        this.a.a.a.post(new l(this.a.a));
    }

    public final void onAnimationRepeat(Animation animation) {
    }

    public final void onAnimationStart(Animation animation) {
    }
}
