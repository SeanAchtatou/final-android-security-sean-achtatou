package com.adchina.android.ads.views;

import android.app.Activity;
import android.content.res.Configuration;
import android.graphics.drawable.BitmapDrawable;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import com.adchina.android.ads.AdManager;
import com.adchina.android.ads.Utils;
import com.adchina.android.ads.a.q;
import java.io.IOException;

public class AdVideoPlayerActivity extends Activity implements MediaPlayer.OnBufferingUpdateListener, MediaPlayer.OnCompletionListener, MediaPlayer.OnErrorListener, MediaPlayer.OnPreparedListener, MediaPlayer.OnVideoSizeChangedListener, SurfaceHolder.Callback {
    private Button mBtnClose;
    private boolean mClosed = false;
    private boolean mFinished = false;
    private boolean mHasFiredVideoFinishEvent = false;
    private RelativeLayout mLayout;
    private Button mPlayerControl;
    private int mScreenHeight = 0;
    private int mScreenWidth = 0;
    private int mSeek = 0;
    private SurfaceView mSfv;
    private SurfaceHolder mSh;
    private GifImageView mWaitingImg;
    private MediaPlayer mp;

    private void FireVideoFinishEvent() {
        if (!this.mHasFiredVideoFinishEvent) {
            this.mHasFiredVideoFinishEvent = true;
            q.a().a(this, getIntent().getExtras().getString("ArgName"));
        }
    }

    private void playUrl(String str) {
        if (this.mp == null) {
            this.mp = new MediaPlayer();
        }
        this.mWaitingImg.setVisibility(0);
        this.mp.reset();
        this.mp.setDisplay(this.mSh);
        this.mp.setScreenOnWhilePlaying(true);
        this.mp.setDataSource(str);
        this.mp.setOnErrorListener(this);
        this.mp.setOnPreparedListener(this);
        this.mp.setOnVideoSizeChangedListener(this);
        this.mp.setAudioStreamType(3);
        this.mp.setOnCompletionListener(this);
        this.mp.prepareAsync();
    }

    /* access modifiers changed from: package-private */
    public void closeVideo() {
        if (this.mp != null) {
            this.mp.stop();
        }
        this.mClosed = true;
        this.mFinished = false;
        q.a().m();
        FireVideoFinishEvent();
        finish();
    }

    public void onBufferingUpdate(MediaPlayer mediaPlayer, int i) {
    }

    public void onCompletion(MediaPlayer mediaPlayer) {
        this.mFinished = true;
        q.a().f();
        FireVideoFinishEvent();
        finish();
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        setUpControls();
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        Utils.setWindowBackgroundColor(getWindow(), AdManager.getAdWindowBackgroundColor(), AdManager.getAdWindowBackgroundOpacity());
        super.onCreate(bundle);
        setUpControls();
        if (bundle != null && bundle.containsKey("Seek")) {
            this.mSeek = bundle.getInt("Seek");
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        boolean z = this.mFinished;
        this.mWaitingImg.a();
        if (this.mp != null) {
            this.mp.release();
            this.mp = null;
        }
        super.onDestroy();
    }

    public boolean onError(MediaPlayer mediaPlayer, int i, int i2) {
        q.a().e();
        this.mFinished = false;
        FireVideoFinishEvent();
        finish();
        return false;
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i != 4) {
            return super.onKeyDown(i, keyEvent);
        }
        closeVideo();
        return true;
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        if (this.mp != null && this.mp.isPlaying()) {
            this.mp.pause();
            this.mSeek = this.mp.getCurrentPosition();
        }
        super.onPause();
    }

    public void onPrepared(MediaPlayer mediaPlayer) {
        this.mWaitingImg.setVisibility(8);
        this.mSh.setFixedSize(this.mSfv.getWidth(), this.mSfv.getHeight());
        mediaPlayer.seekTo(this.mSeek);
        mediaPlayer.start();
        if (this.mSeek == 0) {
            q.a().b();
        }
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Bundle bundle) {
        super.onRestoreInstanceState(bundle);
        if (bundle.containsKey("Seek")) {
            this.mSeek = bundle.getInt("Seek");
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        if (this.mp != null) {
            this.mSeek = this.mp.getCurrentPosition();
            bundle.putInt("Seek", this.mSeek);
        }
    }

    public void onVideoSizeChanged(MediaPlayer mediaPlayer, int i, int i2) {
    }

    /* access modifiers changed from: package-private */
    public void setSurfaceViewLayout(int i, int i2) {
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(i, i2);
        layoutParams.addRule(13);
        this.mSfv.setLayoutParams(layoutParams);
        this.mLayout.postInvalidate();
    }

    /* access modifiers changed from: package-private */
    public void setUpControls() {
        int i;
        if (this.mLayout == null) {
            this.mLayout = new RelativeLayout(getApplicationContext());
            this.mLayout.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
            setContentView(this.mLayout);
        }
        Display defaultDisplay = getWindowManager().getDefaultDisplay();
        this.mScreenWidth = defaultDisplay.getWidth();
        this.mScreenHeight = defaultDisplay.getHeight();
        int min = Math.min(this.mScreenHeight, this.mScreenWidth);
        if (this.mSfv == null) {
            this.mSfv = new SurfaceView(getApplicationContext());
            this.mSfv.setId(Math.round(((float) Math.random()) * 10000.0f));
            this.mLayout.addView(this.mSfv);
            this.mSfv.setFocusable(true);
            this.mSfv.setFocusableInTouchMode(true);
        }
        int dip2px = Utils.dip2px(this, 64.0f);
        int dip2px2 = Utils.dip2px(this, 64.0f);
        if (this.mWaitingImg == null) {
            this.mWaitingImg = new GifImageView(getApplicationContext());
            this.mLayout.addView(this.mWaitingImg);
            this.mWaitingImg.setScaleType(ImageView.ScaleType.FIT_CENTER);
            this.mWaitingImg.bringToFront();
        }
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(dip2px, dip2px2);
        layoutParams.addRule(13);
        this.mWaitingImg.setLayoutParams(layoutParams);
        if (AdManager.getLoadingImg() == 0) {
            this.mWaitingImg.a(Utils.loadAssetsInputStream(getApplicationContext(), AdManager.getDefaultLoadingGifPath()));
        } else {
            this.mWaitingImg.a(AdManager.getLoadingImg());
        }
        if (this.mBtnClose == null) {
            this.mBtnClose = new Button(getApplicationContext());
            this.mLayout.addView(this.mBtnClose);
            this.mBtnClose.bringToFront();
            if (AdManager.getCloseImg() == 0) {
                this.mBtnClose.setBackgroundDrawable(new BitmapDrawable(Utils.loadAssetsBitmap(getApplicationContext(), AdManager.getDefaultCloseImgPath())));
            } else {
                this.mBtnClose.setBackgroundResource(AdManager.getCloseImg());
            }
            this.mBtnClose.setOnClickListener(new c(this));
        }
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(Utils.dip2px(this, 32.0f), Utils.dip2px(this, 32.0f));
        layoutParams2.addRule(12);
        layoutParams2.addRule(11);
        this.mBtnClose.setLayoutParams(layoutParams2);
        if (this.mSh == null) {
            this.mSh = this.mSfv.getHolder();
            this.mSh.addCallback(this);
            this.mSh.setType(3);
        }
        if (this.mSfv != null) {
            if (AdManager.getVideoHeight() > 0 && AdManager.getVideoWidth() > 0) {
                int videoWidth = AdManager.getVideoWidth();
                int videoWidth2 = AdManager.getVideoWidth();
                int dip2px3 = Utils.dip2px(this, (float) videoWidth);
                int dip2px4 = Utils.dip2px(this, (float) videoWidth2);
                i = dip2px3;
                min = dip2px4;
            } else if (getResources().getConfiguration().orientation == 2) {
                i = (min * 4) / 3;
            } else {
                i = min;
                min = (min * 3) / 4;
            }
            setSurfaceViewLayout(i, min);
            this.mSfv.setOnClickListener(new d(this));
            this.mSfv.setOnTouchListener(new e(this));
        }
    }

    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i2, int i3) {
    }

    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        try {
            playUrl(getIntent().getExtras().getString("ADUrl"));
        } catch (IllegalArgumentException e) {
            Log.v("AdChinaError", e.getMessage());
        } catch (IllegalStateException e2) {
            Log.v("AdChinaError", e2.getMessage());
        } catch (IOException e3) {
            Log.v("AdChinaError", e3.getMessage());
        }
    }

    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
    }
}
