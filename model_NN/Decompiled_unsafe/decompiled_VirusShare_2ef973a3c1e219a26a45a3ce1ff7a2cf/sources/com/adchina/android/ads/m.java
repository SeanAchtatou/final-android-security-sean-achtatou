package com.adchina.android.ads;

public final class m {
    private static String a = "http://amob.acs86.com/mst.htm?version=";
    private static String b = "http://192.168.21.132:7008/mst.htm?version=";

    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0099, code lost:
        r1 = r0;
        r0 = r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x00af, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x00b0, code lost:
        r8 = r1;
        r1 = r0;
        r0 = r8;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00af A[ExcHandler: all (r1v10 'th' java.lang.Throwable A[CUSTOM_DECLARE]), Splitter:B:6:0x0023] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.adchina.android.ads.n a(android.content.Context r10) {
        /*
            r9 = this;
            r5 = 0
            r4 = 0
            java.lang.String r0 = com.adchina.android.ads.m.a
            boolean r1 = com.adchina.android.ads.AdManager.getDebugMode()
            if (r1 == 0) goto L_0x000c
            java.lang.String r0 = com.adchina.android.ads.m.b
        L_0x000c:
            r1 = 2
            java.lang.Object[] r1 = new java.lang.Object[r1]
            r1[r4] = r0
            r0 = 1
            java.lang.String r2 = "2.3.6"
            r1[r0] = r2
            java.lang.String r0 = com.adchina.android.ads.Utils.concatString(r1)
            com.adchina.android.ads.t r1 = new com.adchina.android.ads.t
            r1.<init>(r10)
            java.io.InputStream r0 = com.adchina.android.ads.t.b(r0)     // Catch:{ Exception -> 0x00ba, all -> 0x00b7 }
            java.lang.String r1 = com.adchina.android.ads.Utils.convertStreamToString(r0)     // Catch:{ Exception -> 0x00be, all -> 0x00af }
            java.lang.String r2 = "\r"
            java.lang.String r3 = ""
            java.lang.String r1 = r1.replace(r2, r3)     // Catch:{ Exception -> 0x00be, all -> 0x00af }
            java.lang.String r2 = "\n"
            java.lang.String[] r1 = r1.split(r2)     // Catch:{ Exception -> 0x00be, all -> 0x00af }
            r2 = r4
            r3 = r5
        L_0x0037:
            int r4 = r1.length     // Catch:{ Exception -> 0x0098, all -> 0x00af }
            if (r2 < r4) goto L_0x003f
            com.adchina.android.ads.t.a(r0)
            r0 = r3
        L_0x003e:
            return r0
        L_0x003f:
            r4 = r1[r2]     // Catch:{ Exception -> 0x0098, all -> 0x00af }
            r5 = 58
            int r5 = r4.indexOf(r5)     // Catch:{ Exception -> 0x0098, all -> 0x00af }
            if (r5 < 0) goto L_0x0085
            int r6 = r4.length()     // Catch:{ Exception -> 0x0098, all -> 0x00af }
            if (r5 >= r6) goto L_0x0085
            r6 = 0
            java.lang.String r6 = r4.substring(r6, r5)     // Catch:{ Exception -> 0x0098, all -> 0x00af }
            java.lang.String r6 = r6.trim()     // Catch:{ Exception -> 0x0098, all -> 0x00af }
            int r5 = r5 + 1
            java.lang.String r4 = r4.substring(r5)     // Catch:{ Exception -> 0x0098, all -> 0x00af }
            java.lang.String r4 = r4.trim()     // Catch:{ Exception -> 0x0098, all -> 0x00af }
            java.lang.String r5 = "utf-8"
            java.lang.String r4 = java.net.URLDecoder.decode(r4, r5)     // Catch:{ Exception -> 0x0098, all -> 0x00af }
            if (r3 != 0) goto L_0x0070
            com.adchina.android.ads.n r5 = new com.adchina.android.ads.n     // Catch:{ Exception -> 0x0098, all -> 0x00af }
            r5.<init>(r9)     // Catch:{ Exception -> 0x0098, all -> 0x00af }
            r3 = r5
        L_0x0070:
            java.lang.String r5 = r6.toLowerCase()     // Catch:{ Exception -> 0x0098, all -> 0x00af }
            java.lang.String r7 = "isdotest"
            boolean r5 = r5.equals(r7)     // Catch:{ Exception -> 0x0098, all -> 0x00af }
            if (r5 == 0) goto L_0x0088
            java.lang.String r5 = "1"
            boolean r4 = r4.equals(r5)     // Catch:{ Exception -> 0x0098, all -> 0x00af }
            r3.a(r4)     // Catch:{ Exception -> 0x0098, all -> 0x00af }
        L_0x0085:
            int r2 = r2 + 1
            goto L_0x0037
        L_0x0088:
            java.lang.String r5 = r6.toLowerCase()     // Catch:{ Exception -> 0x0098, all -> 0x00af }
            java.lang.String r7 = "requesturl"
            boolean r5 = r5.equals(r7)     // Catch:{ Exception -> 0x0098, all -> 0x00af }
            if (r5 == 0) goto L_0x009f
            r3.a(r4)     // Catch:{ Exception -> 0x0098, all -> 0x00af }
            goto L_0x0085
        L_0x0098:
            r1 = move-exception
            r1 = r0
            r0 = r3
        L_0x009b:
            com.adchina.android.ads.t.a(r1)
            goto L_0x003e
        L_0x009f:
            java.lang.String r5 = r6.toLowerCase()     // Catch:{ Exception -> 0x0098, all -> 0x00af }
            java.lang.String r6 = "reporturl"
            boolean r5 = r5.equals(r6)     // Catch:{ Exception -> 0x0098, all -> 0x00af }
            if (r5 == 0) goto L_0x0085
            r3.b(r4)     // Catch:{ Exception -> 0x0098, all -> 0x00af }
            goto L_0x0085
        L_0x00af:
            r1 = move-exception
            r8 = r1
            r1 = r0
            r0 = r8
        L_0x00b3:
            com.adchina.android.ads.t.a(r1)
            throw r0
        L_0x00b7:
            r0 = move-exception
            r1 = r5
            goto L_0x00b3
        L_0x00ba:
            r0 = move-exception
            r0 = r5
            r1 = r5
            goto L_0x009b
        L_0x00be:
            r1 = move-exception
            r1 = r0
            r0 = r5
            goto L_0x009b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adchina.android.ads.m.a(android.content.Context):com.adchina.android.ads.n");
    }
}
