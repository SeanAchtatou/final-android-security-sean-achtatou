package com.adchina.android.ads;

import android.content.Context;
import java.net.URLEncoder;

final class z implements c {
    final /* synthetic */ y a;
    private final /* synthetic */ Context b;
    private final /* synthetic */ long c;
    private final /* synthetic */ n d;

    z(y yVar, Context context, long j, n nVar) {
        this.a = yVar;
        this.b = context;
        this.c = j;
        this.d = nVar;
    }

    public final Object a() {
        Context context = this.b;
        long j = this.c;
        n nVar = this.d;
        try {
            String concatString = Utils.concatString(nVar.c(), "?args=", URLEncoder.encode(String.format("%s|||%s|||android", Long.valueOf(j), Utils.getActiveNetworkType(context)), "utf-8"));
            new t(context);
            t.a(concatString);
            return null;
        } catch (Exception e) {
            return null;
        }
    }
}
