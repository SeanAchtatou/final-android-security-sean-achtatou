package com.agilebinary.mobilemonitor.device.a.b.a.a.d;

import com.agilebinary.mobilemonitor.device.a.b.a.a.c.a.c;
import com.agilebinary.mobilemonitor.device.a.b.a.a.c.a.d;
import com.agilebinary.mobilemonitor.device.a.b.a.a.c.a.h;

public final class a extends d {
    public a(h hVar) {
        super(hVar);
    }

    public final int a(byte[] bArr, int i, int i2) {
        return this.a.a(bArr, i, i2);
    }

    public final void a(byte[] bArr) {
        int i = 0;
        do {
            int a = this.a.a(bArr, i, bArr.length - i);
            if (a != -1) {
                i += a;
            } else {
                return;
            }
        } while (i != bArr.length);
    }

    public final boolean b() {
        int a = this.a.a();
        if (a >= 0) {
            return a != 0;
        }
        throw new c();
    }

    public final byte c() {
        int a = this.a.a();
        if (a >= 0) {
            return (byte) a;
        }
        throw new c();
    }

    public final short d() {
        h hVar = this.a;
        int a = hVar.a();
        int a2 = hVar.a();
        if ((a | a2) < 0) {
            throw new c();
        }
        return (short) ((a2 << 0) + (a << 8));
    }

    public final int e() {
        h hVar = this.a;
        int a = hVar.a();
        int a2 = hVar.a();
        int a3 = hVar.a();
        int a4 = hVar.a();
        if ((a | a2 | a3 | a4) < 0) {
            throw new c();
        }
        return (a4 << 0) + (a << 24) + (a2 << 16) + (a3 << 8);
    }

    public final long f() {
        return (((long) e()) << 32) + (((long) e()) & 4294967295L);
    }

    public final double g() {
        int i = 0;
        char[] cArr = new char[this.a.a()];
        int length = (cArr.length / 2) + (cArr.length & 1);
        for (int i2 = 0; i2 < length; i2++) {
            int a = this.a.a();
            cArr[i] = (char) ((a >> 4) + 45);
            if (cArr[i] == '/') {
                cArr[i] = 'E';
            }
            i++;
            if (i < cArr.length) {
                cArr[i] = (char) ((a & 15) + 45);
                if (cArr[i] == '/') {
                    cArr[i] = 'E';
                }
                i++;
            }
        }
        return Double.valueOf(new String(cArr)).doubleValue();
    }

    public final String[] h() {
        String[] strArr = new String[e()];
        for (int i = 0; i < strArr.length; i++) {
            strArr[i] = i();
        }
        return strArr;
    }

    public final String i() {
        if (c() == 1) {
            return null;
        }
        h hVar = this.a;
        int a = hVar.a();
        int a2 = hVar.a();
        if ((a | a2) < 0) {
            throw new c();
        }
        int i = (a2 << 0) + (a << 8);
        char[] cArr = new char[i];
        byte[] bArr = new byte[i];
        a(bArr, 0, i);
        int i2 = 0;
        int i3 = 0;
        while (i3 < i) {
            byte b = bArr[i3] & 255;
            switch (b >> 4) {
                case 0:
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                case 6:
                case 7:
                    i3++;
                    cArr[i2] = (char) b;
                    i2++;
                    break;
                case 8:
                case 9:
                case 10:
                case 11:
                default:
                    throw new IllegalArgumentException();
                case 12:
                case 13:
                    i3 += 2;
                    if (i3 <= i) {
                        byte b2 = bArr[i3 - 1];
                        if ((b2 & 192) == 128) {
                            cArr[i2] = (char) (((b & 31) << 6) | (b2 & 63));
                            i2++;
                            break;
                        } else {
                            throw new IllegalArgumentException();
                        }
                    } else {
                        throw new IllegalArgumentException();
                    }
                case 14:
                    i3 += 3;
                    if (i3 <= i) {
                        byte b3 = bArr[i3 - 2];
                        byte b4 = bArr[i3 - 1];
                        if ((b3 & 192) == 128 && (b4 & 192) == 128) {
                            cArr[i2] = (char) (((b & 15) << 12) | ((b3 & 63) << 6) | ((b4 & 63) << 0));
                            i2++;
                            break;
                        } else {
                            throw new IllegalArgumentException();
                        }
                    } else {
                        throw new IllegalArgumentException();
                    }
                    break;
            }
        }
        return new String(cArr, 0, i2);
    }
}
