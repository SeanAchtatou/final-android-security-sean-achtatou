package com.agilebinary.mobilemonitor.device.a.b.a.a.a;

import com.agilebinary.mobilemonitor.device.a.b.a.a.c;
import com.agilebinary.mobilemonitor.device.a.b.a.a.d;
import com.agilebinary.mobilemonitor.device.a.b.a.a.d.a;

public final class p extends q {
    private long a;
    private String b;

    public p(a aVar) {
        super(aVar);
        this.a = aVar.f();
        this.b = aVar.i();
    }

    public p(String str, String str2, d dVar, long j, String str3) {
        super(str, str2, dVar);
        this.a = j;
        this.b = str3;
    }

    public final String a(c cVar) {
        return super.a(cVar) + "\nTime: " + cVar.a(this.a) + "\nKeyword: " + this.b;
    }

    public final void a(com.agilebinary.mobilemonitor.device.a.b.a.a.a aVar) {
        super.a(aVar);
        aVar.a(this.a);
        aVar.a(this.b);
    }

    public final byte b() {
        return 13;
    }
}
