package com.agilebinary.mobilemonitor.device.a.d;

import com.agilebinary.a.a.a.ab;
import com.agilebinary.a.a.a.ac;
import com.agilebinary.a.a.a.ad;
import com.agilebinary.a.a.a.f;
import com.agilebinary.a.a.a.f.i;
import com.agilebinary.a.a.a.j;

public class b implements ab, ac {
    private final ab[] a;
    private final ad[] b;

    public b() {
    }

    public b(ab[] abVarArr, ad[] adVarArr) {
        if (abVarArr != null) {
            int length = abVarArr.length;
            this.a = new ab[length];
            for (int i = 0; i < length; i++) {
                this.a[i] = abVarArr[i];
            }
        } else {
            this.a = new ab[0];
        }
        if (adVarArr != null) {
            int length2 = adVarArr.length;
            this.b = new ad[length2];
            for (int i2 = 0; i2 < length2; i2++) {
                this.b[i2] = adVarArr[i2];
            }
            return;
        }
        this.b = new ad[0];
    }

    public void a(f fVar, i iVar) {
        for (ab a2 : this.a) {
            a2.a(fVar, iVar);
        }
    }

    public void a(j jVar, i iVar) {
        for (ad a2 : this.b) {
            a2.a(jVar, iVar);
        }
    }
}
