package com.agilebinary.mobilemonitor.device.android.device.a;

import android.location.Location;
import com.agilebinary.mobilemonitor.device.a.b.a.a.d;

public final class j implements d {
    private Location a;

    public j(Location location) {
        this.a = location;
    }

    public final boolean a() {
        return this.a.hasAltitude();
    }

    public final double b() {
        return this.a.getAltitude();
    }

    public final boolean c() {
        return this.a.hasBearing();
    }

    public final double d() {
        return (double) this.a.getBearing();
    }

    public final long e() {
        return this.a.getTime();
    }

    public final boolean f() {
        return this.a.hasAccuracy();
    }

    public final double g() {
        return (double) this.a.getAccuracy();
    }

    public final double h() {
        return (double) this.a.getAccuracy();
    }

    public final double i() {
        return this.a.getLatitude();
    }

    public final double j() {
        return this.a.getLongitude();
    }

    public final boolean k() {
        return this.a.hasSpeed();
    }

    public final double l() {
        return (double) this.a.getSpeed();
    }

    public final String toString() {
        return this.a.getLatitude() + " / " + this.a.getLongitude() + " hasSpeed: " + this.a.hasSpeed() + " speed: " + l();
    }
}
