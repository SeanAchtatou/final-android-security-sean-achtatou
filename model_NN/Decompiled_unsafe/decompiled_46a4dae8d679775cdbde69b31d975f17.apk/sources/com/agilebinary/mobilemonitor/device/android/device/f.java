package com.agilebinary.mobilemonitor.device.android.device;

import android.net.wifi.WifiManager;
import android.os.PowerManager;

public final class f {
    private PowerManager.WakeLock a;
    private WifiManager.WifiLock b;
    private int c;
    private String d;

    public f(i iVar, String str, PowerManager.WakeLock wakeLock, WifiManager.WifiLock wifiLock) {
        this.a = wakeLock;
        this.b = wifiLock;
        this.d = str;
    }

    public final boolean a() {
        "" + this.c;
        this.c++;
        this.a.acquire();
        this.b.acquire();
        "" + this.c;
        return this.c == 1;
    }

    public final boolean a(boolean z) {
        "WakeLockAndWifiLock.release " + this.d + ", refCount=";
        "" + this.c;
        "" + z;
        if (z) {
            if (this.c > 0) {
                "release WifiLock " + this.d;
                this.b.release();
                "release WakeLock " + this.d;
                this.a.release();
            }
            this.c = 0;
        } else if (this.c > 0) {
            this.c--;
            if (this.c == 0) {
                "release WifiLock " + this.d;
                this.b.release();
                "release WakeLock " + this.d;
                this.a.release();
            }
        }
        "" + this.c;
        return this.c == 0;
    }
}
