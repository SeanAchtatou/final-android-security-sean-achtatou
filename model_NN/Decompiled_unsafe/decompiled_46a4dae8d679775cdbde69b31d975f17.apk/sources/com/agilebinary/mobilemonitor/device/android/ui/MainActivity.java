package com.agilebinary.mobilemonitor.device.android.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.text.InputFilter;
import android.text.method.LinkMovementMethod;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.agilebinary.mobilemonitor.device.a.a.c;
import com.agilebinary.mobilemonitor.device.a.f.b;
import com.agilebinary.mobilemonitor.device.a.g.f;
import com.agilebinary.mobilemonitor.device.a.h.a;
import com.agilebinary.mobilemonitor.device.android.device.d;
import com.agilebinary.mobilemonitor.device.android.device.i;
import com.agilebinary.mobilemonitor.device.android.services.BackgroundService;
import com.agilebinary.phonebeagle.R;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MainActivity extends Activity implements a {
    public static MainActivity c;
    private static final String d = f.a();
    private ListView A;
    /* access modifiers changed from: private */
    public o B;
    /* access modifiers changed from: private */
    public i C;
    /* access modifiers changed from: private */
    public com.agilebinary.mobilemonitor.device.android.a.b.a D;
    private aa E;
    protected n a;
    protected d b;
    private Button e;
    private Button f;
    private TextView g;
    private TextView h;
    private TextView i;
    /* access modifiers changed from: private */
    public EditText j;
    private TextView k;
    private TextView l;
    private TextView m;
    private TextView n;
    private TextView o;
    private Button p;
    /* access modifiers changed from: private */
    public Button q;
    private TextView r;
    private TextView s;
    /* access modifiers changed from: private */
    public ProgressDialog t;
    /* access modifiers changed from: private */
    public ProgressDialog u;
    /* access modifiers changed from: private */
    public AlertDialog v;
    private BroadcastReceiver w;
    private BroadcastReceiver x;
    private BroadcastReceiver y;
    private BroadcastReceiver z;

    public MainActivity() {
        new Handler();
    }

    static /* synthetic */ void a(MainActivity mainActivity, long j2, int i2) {
        String a2;
        if (j2 == 0) {
            a2 = b.a("COMMONS_UNKNOWN");
        } else {
            a2 = b.a("EVENT_UPLOAD_STATUS", new String[]{SimpleDateFormat.getDateTimeInstance().format(new Date(j2)), b.a("CONNECTION_TYPE_" + i2)});
        }
        mainActivity.n.setText(a2);
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.v.hide();
        this.t.show();
        this.E = new aa(this);
        this.E.b(this.j.getText().toString());
    }

    public final void a(String str) {
        String string;
        boolean z2;
        this.j.setText("");
        if (c.a().Z()) {
            this.j.setText("");
            if (com.agilebinary.mobilemonitor.device.android.device.admin.a.c(this)) {
                try {
                    z2 = com.agilebinary.mobilemonitor.device.android.device.admin.a.a(this);
                } catch (Exception e2) {
                    z2 = false;
                }
                "adjustLabels, adminActive=" + z2;
                string = z2 ? getString(R.string.activation_status_suffix__device_admin_active) : getString(R.string.activation_status_suffix__device_admin_inactive);
            } else {
                string = getString(R.string.activation_status_suffix__device_admin_not_supported);
            }
            this.l.setText(b.a("COMMONS_ACTIVATED") + " " + string);
            this.h.setVisibility(8);
            this.i.setVisibility(8);
        } else {
            this.s.setText(b.a("COMMONS_UNKNOWN"));
            this.n.setText(b.a("COMMONS_UNKNOWN"));
            this.l.setText(b.a("COMMONS_NOT_ACTIVATED"));
            this.h.setVisibility(0);
            this.i.setVisibility(0);
        }
        if (str != null) {
            this.l.setText(str);
        }
    }

    public final void a(String str, String str2) {
        runOnUiThread(new x(this, b.a("CONNECTIVITYMANAGERLISTENER." + str, str2 == null ? "" : str2)));
    }

    /* access modifiers changed from: protected */
    public final void b() {
        if (this.a != null) {
            this.a.a(true);
        }
        ak.b();
        com.agilebinary.mobilemonitor.device.android.a.b.b();
    }

    /* access modifiers changed from: protected */
    public final void c() {
        if (this.E != null) {
            this.E.a(true);
        }
        if (this.b != null) {
            this.b.a(true);
        }
        ak.b();
        com.agilebinary.mobilemonitor.device.android.a.b.b();
    }

    /* access modifiers changed from: protected */
    public final void d() {
        boolean z2 = this.j.getText().toString().trim().length() == 16;
        this.e.setEnabled(z2);
        this.f.setEnabled(z2);
        this.q.setEnabled(c.a().Z());
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        c = this;
        d.a(this);
        this.C = new i(this, com.agilebinary.mobilemonitor.device.a.a.b.a(), c.a(), f.a());
        this.C.a();
        this.C.b();
        this.D = new com.agilebinary.mobilemonitor.device.android.a.b.a(null, this.C, com.agilebinary.mobilemonitor.device.a.a.b.a(), c.a());
        this.D.a();
        this.D.b();
        setContentView((int) R.layout.main);
        this.e = (Button) findViewById(R.id.Button_Activate);
        this.f = (Button) findViewById(R.id.Button_Deactivate);
        this.g = (TextView) findViewById(R.id.TextView_productId);
        this.h = (TextView) findViewById(R.id.TextView_buyKey_line1);
        this.i = (TextView) findViewById(R.id.TextView_buyKey_line2);
        this.l = (TextView) findViewById(R.id.TextView_statusActivationText);
        this.k = (TextView) findViewById(R.id.TextView_statusActivationLabel);
        this.n = (TextView) findViewById(R.id.TextView_statusUploadText);
        this.m = (TextView) findViewById(R.id.TextView_statusUploadLabel);
        this.o = (TextView) findViewById(R.id.TextView_keyLabel);
        this.j = (EditText) findViewById(R.id.EditText_Key);
        this.p = (Button) findViewById(R.id.Button_TestConnection);
        this.q = (Button) findViewById(R.id.Button_uploadNextBatchOfEventsNow);
        this.r = (TextView) findViewById(R.id.TextView_numeventsLabel);
        this.s = (TextView) findViewById(R.id.TextView_numeventsText);
        this.A = (ListView) findViewById(R.id.ListView_StatusLog);
        this.j.setFilters(new InputFilter[]{new InputFilter.LengthFilter(16)});
        this.j.addTextChangedListener(new f(this));
        this.g.setText(b.a("LABEL_HEADER", new String[]{getString(R.string.application_title), com.agilebinary.mobilemonitor.device.a.a.b.a().d()}));
        this.h.setText(b.a("LABEL_BUY_KEY_AT"));
        this.i.setMovementMethod(LinkMovementMethod.getInstance());
        this.i.setText(Html.fromHtml(b.a("LABEL_BUY_KEY_URL")));
        this.e.setText(b.a("BUTTON_ACTIVATE"));
        this.f.setText(b.a("BUTTON_DEACTIVATE"));
        this.p.setText(b.a("BUTTON_TEST_CONNECTION"));
        this.q.setText(b.a("BUTTON_SYNCHRONIZE"));
        this.o.setText(b.a("LABEL_KEY"));
        this.k.setText(b.a("LABEL_STATUS_ACTIVATION"));
        this.m.setText(b.a("LABEL_STATUS_UPLOAD"));
        this.r.setText(b.a("LABEL_NUMBER_OF_EVENTS_IN_DATABASE"));
        this.s.setText(b.a("COMMONS_UNKNOWN"));
        this.n.setText(b.a("COMMONS_UNKNOWN"));
        this.B = new o(this);
        this.A.setAdapter((ListAdapter) this.B);
        this.e.setOnClickListener(new g(this));
        this.f.setOnClickListener(new i(this));
        this.p.setOnClickListener(new h(this));
        this.q.setOnClickListener(new j(this));
        this.t = new ProgressDialog(this);
        this.t.setMessage(b.a("COMMONS_PLEASE_WAIT"));
        this.t.setIndeterminate(true);
        this.t.setButton(-2, b.a("COMMONS_CANCEL"), new l(this));
        this.t.setCancelable(true);
        this.t.setOnCancelListener(new k(this));
        this.u = new ProgressDialog(this);
        this.u.setMessage(b.a("COMMONS_PLEASE_WAIT"));
        this.u.setIndeterminate(true);
        this.u.setButton(-2, b.a("COMMONS_CANCEL"), new m(this));
        this.u.setCancelable(true);
        this.u.setOnCancelListener(new t(this));
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(b.a("EULA_TXT")).setCancelable(true).setPositiveButton(b.a("EULA_ACCEPT"), new v(this)).setNegativeButton(b.a("EULA_DECLINE"), new u(this));
        this.v = builder.create();
        this.v.setOnCancelListener(new w(this));
        this.w = new ac(this);
        this.x = new c(this);
        this.y = new ab(this);
        this.z = new a(this);
        d();
        a((String) null);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.base, menu);
        MenuItem findItem = menu.findItem(R.id.menu_base_help);
        findItem.setTitle(b.a("COMMONS_HELP"));
        findItem.setVisible(com.agilebinary.mobilemonitor.device.a.a.b.a().s().trim().length() > 0);
        return true;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        if (this.u.isShowing()) {
            this.u.cancel();
        }
        if (this.t.isShowing()) {
            this.t.cancel();
        }
        if (this.v.isShowing()) {
            this.v.cancel();
        }
        this.C.c();
        this.C.d();
        this.D.c();
        this.D.d();
        super.onDestroy();
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.menu_base_help /*2131230742*/:
                try {
                    startActivity(new Intent("android.intent.action.VIEW", Uri.parse(com.agilebinary.mobilemonitor.device.a.a.b.a().s())));
                } catch (ActivityNotFoundException e2) {
                }
                return true;
            default:
                return super.onOptionsItemSelected(menuItem);
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.D.g().b(this);
        unregisterReceiver(this.x);
        unregisterReceiver(this.w);
        unregisterReceiver(this.y);
        unregisterReceiver(this.z);
        this.B.clear();
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Bundle bundle) {
        try {
            super.onRestoreInstanceState(bundle);
            if (bundle.getBoolean("EXTRA_EULA_SHOWING", false)) {
                this.v.show();
            }
        } catch (Throwable th) {
            com.agilebinary.mobilemonitor.device.a.d.a.d(th);
        }
        d();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        this.D.g().a(this);
        registerReceiver(this.w, new IntentFilter(BackgroundService.a));
        registerReceiver(this.x, new IntentFilter(BackgroundService.b));
        registerReceiver(this.y, new IntentFilter(BackgroundService.c));
        registerReceiver(this.z, new IntentFilter(BackgroundService.d));
        BackgroundService.a(this, "EXTRA_FORCE_BROADCASTS", (Uri) null);
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putBoolean("EXTRA_EULA_SHOWING", this.v.isShowing());
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        a((String) null);
    }
}
