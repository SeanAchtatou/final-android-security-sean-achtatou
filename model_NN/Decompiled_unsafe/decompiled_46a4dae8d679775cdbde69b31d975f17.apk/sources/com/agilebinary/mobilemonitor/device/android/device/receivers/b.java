package com.agilebinary.mobilemonitor.device.android.device.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.agilebinary.mobilemonitor.device.android.device.i;

public final class b extends BroadcastReceiver {
    private final i a;

    public b(i iVar) {
        this.a = iVar;
    }

    public final void onReceive(Context context, Intent intent) {
        this.a.F();
    }
}
