package com.agilebinary.mobilemonitor.device.a.b.a.a.c.a;

public abstract class h {
    public abstract int a();

    public int a(byte[] bArr, int i, int i2) {
        if (bArr == null) {
            throw new NullPointerException();
        } else if (i < 0 || i > bArr.length || i2 < 0 || i + i2 > bArr.length || i + i2 < 0) {
            throw new IndexOutOfBoundsException();
        } else if (i2 == 0) {
            return 0;
        } else {
            int a = a();
            if (a == -1) {
                return -1;
            }
            bArr[i] = (byte) a;
            int i3 = 1;
            while (i3 < i2) {
                try {
                    int a2 = a();
                    if (a2 == -1) {
                        return i3;
                    }
                    if (bArr != null) {
                        bArr[i + i3] = (byte) a2;
                    }
                    i3++;
                } catch (e e) {
                    return i3;
                }
            }
            return i3;
        }
    }
}
