package com.agilebinary.mobilemonitor.device.android.services;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.IBinder;
import android.os.PowerManager;
import com.agilebinary.mobilemonitor.device.a.a.c;
import com.agilebinary.mobilemonitor.device.a.g.f;
import com.agilebinary.mobilemonitor.device.android.device.d;
import com.agilebinary.mobilemonitor.device.android.device.i;

public class BackgroundService extends Service {
    public static final String a = (f + "ACTION_SERVICE_STATUS_CALLBACK");
    public static final String b = (f + "ACTION_NUMBER_OF_EVENTS_CHANGED");
    public static final String c = (f + "ACTION_EVENT_UPLOADED");
    public static final String d = (f + "ACTION_DEACTIVATED");
    private static String e = f.a();
    private static String f = BackgroundService.class.getPackage().getName();
    private static PowerManager.WakeLock g;
    /* access modifiers changed from: private */
    public d h;
    private boolean i;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public static void a(Context context, String str, Uri uri) {
        try {
            Intent intent = new Intent().setClass(context, BackgroundService.class);
            if (str != null) {
                intent.putExtra(str, true);
            }
            if (uri != null) {
                intent.putExtra("EXTRA_URI", uri.toString());
            }
            PowerManager powerManager = (PowerManager) context.getSystemService("power");
            if (g == null) {
                g = powerManager.newWakeLock(1, "START_SERVICE");
            }
            g.setReferenceCounted(true);
            g.acquire();
            context.startService(intent);
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    private void a(boolean z) {
        Intent action = new Intent().setAction(a);
        action.putExtra("EXTRA_SERVICE_STATUS", z);
        sendBroadcast(action);
    }

    private void a(boolean z, boolean z2, String str) {
        this.i = false;
        if (!c.a().Z()) {
            stopSelf();
            if (z) {
                a(false);
                return;
            }
            return;
        }
        if (this.h == null) {
            this.h = new d(getApplicationContext(), this);
            this.h.a();
            this.h.b();
        }
        if (z2) {
            this.h.k();
        }
        if (z) {
            a(true);
            b();
        }
        if (str != null) {
            Uri parse = Uri.parse(str);
            if (parse.getScheme().equals("alarm")) {
                ((i) this.h.i()).e(parse.getSchemeSpecificPart());
            }
        }
    }

    private void b() {
        if (this.h != null) {
            a(this.h.h().l());
        }
    }

    public final void a() {
        sendBroadcast(new Intent().setAction(d));
    }

    public final void a(long j) {
        Intent action = new Intent().setAction(b);
        action.putExtra("EXTRA_NUMBER_OF_EVENTS", j);
        sendBroadcast(action);
    }

    public final void a(long j, int i2) {
        Intent action = new Intent().setAction(c);
        action.putExtra("EXTRA_LAST_EVENT_UPLOAD_TIME", j);
        action.putExtra("EXTRA_LAST_EVENT_UPLOAD_CONTYPE", i2);
        sendBroadcast(action);
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.mobilemonitor.device.android.services.BackgroundService.a(boolean, boolean, java.lang.String):void
     arg types: [int, int, ?[OBJECT, ARRAY]]
     candidates:
      com.agilebinary.mobilemonitor.device.android.services.BackgroundService.a(android.content.Context, java.lang.String, android.net.Uri):void
      com.agilebinary.mobilemonitor.device.android.services.BackgroundService.a(boolean, boolean, java.lang.String):void */
    public void onCreate() {
        super.onCreate();
        d.a(this);
        a(false, false, (String) null);
    }

    public void onDestroy() {
        if (this.i) {
            if (this.h != null) {
                this.h.c();
                this.h.d();
                this.h = null;
            }
        } else if (c.a().Z()) {
            ((AlarmManager) getSystemService("alarm")).set(0, System.currentTimeMillis() + 1000, PendingIntent.getBroadcast(this, 0, new Intent("com.agilebinary.mobilemonitor.WATCHDOG_ACTION"), 0));
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.mobilemonitor.device.a.c.f.a(com.agilebinary.mobilemonitor.device.a.g.a.d, boolean):void
     arg types: [com.agilebinary.mobilemonitor.device.android.services.a, int]
     candidates:
      com.agilebinary.mobilemonitor.device.a.c.f.a(java.lang.String, com.agilebinary.mobilemonitor.device.a.g.a.d):void
      com.agilebinary.mobilemonitor.device.a.c.f.a(java.lang.String, java.lang.String):void
      com.agilebinary.mobilemonitor.device.a.c.f.a(java.lang.String, boolean):void
      com.agilebinary.mobilemonitor.device.a.c.f.a(com.agilebinary.mobilemonitor.device.a.b.a.a.a.u[], java.lang.String):void
      com.agilebinary.mobilemonitor.device.a.c.f.a(java.lang.Object[], com.agilebinary.mobilemonitor.device.a.g.e):void
      com.agilebinary.mobilemonitor.device.a.c.f.a(com.agilebinary.mobilemonitor.device.a.g.a.d, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.mobilemonitor.device.android.services.BackgroundService.a(boolean, boolean, java.lang.String):void
     arg types: [int, int, ?[OBJECT, ARRAY]]
     candidates:
      com.agilebinary.mobilemonitor.device.android.services.BackgroundService.a(android.content.Context, java.lang.String, android.net.Uri):void
      com.agilebinary.mobilemonitor.device.android.services.BackgroundService.a(boolean, boolean, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.mobilemonitor.device.android.services.BackgroundService.a(boolean, boolean, java.lang.String):void
     arg types: [int, int, java.lang.String]
     candidates:
      com.agilebinary.mobilemonitor.device.android.services.BackgroundService.a(android.content.Context, java.lang.String, android.net.Uri):void
      com.agilebinary.mobilemonitor.device.android.services.BackgroundService.a(boolean, boolean, java.lang.String):void */
    public void onStart(Intent intent, int i2) {
        if (intent.getBooleanExtra("EXTRA_SYNC_NOW", false)) {
            if (c.a().Z()) {
                this.h.i().a((com.agilebinary.mobilemonitor.device.a.g.a.d) new a(this, "UploadNextBatchOfEventsNow"), true);
            }
        } else if (intent.getBooleanExtra("EXTRA_FETCH_COMMANDS_NOW", false)) {
            this.h.f().g();
        } else if (intent.getBooleanExtra("EXTRA_FORCE_BROADCASTS", false)) {
            if (c.a().Z()) {
                b();
                if (this.h != null) {
                    a(this.h.g().h(), this.h.g().i());
                }
            }
        } else if (intent.getBooleanExtra("EXTRA_DEACTIVATE_FROM_GUI", false)) {
            if (this.h != null) {
                this.h.e();
            }
            this.i = true;
            onDestroy();
            a(false);
        } else if (intent.getBooleanExtra("EXTRA_START_FROM_GUI", false)) {
            a(true, false, (String) null);
        } else if (intent.getBooleanExtra("EXTRA_START_FROM_BOOTCOMPLETERECEIVER", false)) {
            a(false, true, (String) null);
        } else if (intent.getBooleanExtra("EXTRA_START_WATCHDOG", false)) {
            a(false, false, (String) null);
        } else if (intent.getBooleanExtra("EXTRA_START_SCHEDULED_ACTION", false)) {
            intent.getStringExtra("EXTRA_URI");
            a(false, false, intent.getStringExtra("EXTRA_URI"));
        }
        if (g != null) {
            g.release();
        }
    }
}
