package com.agilebinary.mobilemonitor.device.android.ui;

import com.agilebinary.mobilemonitor.device.a.g.f;
import java.util.concurrent.FutureTask;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public abstract class ak {
    public static final String a = f.a();
    private static final ThreadFactory b = new r();
    private static ThreadPoolExecutor c = e();
    /* access modifiers changed from: private */
    public static final aj d = new aj();
    private final ae e = new s(this);
    private final FutureTask f = new p(this, this.e);
    private volatile af g = af.PENDING;

    static /* synthetic */ void a(ak akVar, Object obj) {
        akVar.a(obj);
        akVar.g = af.FINISHED;
    }

    public static void b() {
        try {
            c.shutdownNow();
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        c = e();
    }

    private static ThreadPoolExecutor e() {
        return new ThreadPoolExecutor(0, 3, 1, TimeUnit.SECONDS, new LinkedBlockingQueue(3), b);
    }

    /* access modifiers changed from: protected */
    public abstract Object a(Object... objArr);

    /* access modifiers changed from: protected */
    public void a() {
    }

    /* access modifiers changed from: protected */
    public void a(Object obj) {
    }

    public final boolean a(boolean z) {
        return this.f.cancel(true);
    }

    public final ak b(Object... objArr) {
        if (this.g != af.PENDING) {
            switch (q.a[this.g.ordinal()]) {
                case 1:
                    throw new IllegalStateException("Cannot execute task: the task is already running.");
                case 2:
                    throw new IllegalStateException("Cannot execute task: the task has already been executed (a task can be executed only once)");
            }
        }
        this.g = af.RUNNING;
        this.e.a = objArr;
        c.execute(this.f);
        return this;
    }

    public final boolean c() {
        return this.f.isCancelled();
    }
}
