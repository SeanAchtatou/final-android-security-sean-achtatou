package com.agilebinary.mobilemonitor.device.android.device;

import android.content.IntentFilter;
import android.net.wifi.ScanResult;
import com.agilebinary.b.a.a.l;
import com.agilebinary.b.a.a.o;
import com.agilebinary.mobilemonitor.device.a.a.c;
import com.agilebinary.mobilemonitor.device.a.c.a.g;
import java.util.List;

public final class b extends g {
    private g b;
    private /* synthetic */ i c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public b(i iVar, c cVar, i iVar2) {
        super(cVar, iVar2);
        this.c = iVar;
    }

    public final void b() {
        this.b = new g(this);
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.net.wifi.SCAN_RESULTS");
        this.c.d.registerReceiver(this.b, intentFilter);
        super.b();
    }

    public final void c() {
        super.c();
        this.c.d.unregisterReceiver(this.b);
    }

    public final o f() {
        if (this.c.e.startScan()) {
            synchronized (this.b) {
                try {
                    this.b.wait(10000);
                } catch (InterruptedException e) {
                }
            }
        }
        List<ScanResult> scanResults = this.c.e.getScanResults();
        l lVar = new l();
        if (scanResults != null) {
            for (ScanResult next : scanResults) {
                lVar.b(new com.agilebinary.mobilemonitor.device.a.c.a.c(next.BSSID, next.level));
            }
        }
        return lVar;
    }
}
