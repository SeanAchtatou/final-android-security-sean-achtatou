package com.agilebinary.mobilemonitor.device.a.b.a.a.b.a.a;

final class i {
    i() {
    }

    static long a(long j, byte[] bArr, int i, int i2) {
        int i3;
        if (bArr == null) {
            return 1;
        }
        long j2 = 65535 & j;
        long j3 = (j >> 16) & 65535;
        long j4 = j2;
        int i4 = i2;
        int i5 = i;
        long j5 = j4;
        while (i4 > 0) {
            int i6 = i4 < 5552 ? i4 : 5552;
            i4 -= i6;
            while (i6 >= 16) {
                int i7 = i5 + 1;
                long j6 = j5 + ((long) (bArr[i5] & 255));
                long j7 = j3 + j6;
                int i8 = i7 + 1;
                long j8 = j6 + ((long) (bArr[i7] & 255));
                long j9 = j7 + j8;
                int i9 = i8 + 1;
                long j10 = j8 + ((long) (bArr[i8] & 255));
                long j11 = j9 + j10;
                int i10 = i9 + 1;
                long j12 = j10 + ((long) (bArr[i9] & 255));
                long j13 = j11 + j12;
                int i11 = i10 + 1;
                long j14 = j12 + ((long) (bArr[i10] & 255));
                long j15 = j13 + j14;
                int i12 = i11 + 1;
                long j16 = j14 + ((long) (bArr[i11] & 255));
                long j17 = j15 + j16;
                int i13 = i12 + 1;
                long j18 = j16 + ((long) (bArr[i12] & 255));
                long j19 = j17 + j18;
                int i14 = i13 + 1;
                long j20 = j18 + ((long) (bArr[i13] & 255));
                long j21 = j19 + j20;
                int i15 = i14 + 1;
                long j22 = j20 + ((long) (bArr[i14] & 255));
                long j23 = j21 + j22;
                int i16 = i15 + 1;
                long j24 = j22 + ((long) (bArr[i15] & 255));
                long j25 = j23 + j24;
                int i17 = i16 + 1;
                long j26 = j24 + ((long) (bArr[i16] & 255));
                long j27 = j25 + j26;
                int i18 = i17 + 1;
                long j28 = j26 + ((long) (bArr[i17] & 255));
                long j29 = j27 + j28;
                int i19 = i18 + 1;
                long j30 = j28 + ((long) (bArr[i18] & 255));
                long j31 = j29 + j30;
                int i20 = i19 + 1;
                long j32 = j30 + ((long) (bArr[i19] & 255));
                long j33 = j31 + j32;
                int i21 = i20 + 1;
                long j34 = j32 + ((long) (bArr[i20] & 255));
                long j35 = j33 + j34;
                i5 = i21 + 1;
                j5 = j34 + ((long) (bArr[i21] & 255));
                j3 = j35 + j5;
                i6 -= 16;
            }
            if (i6 != 0) {
                while (true) {
                    i3 = i5 + 1;
                    j5 += (long) (bArr[i5] & 255);
                    j3 += j5;
                    int i22 = i6 - 1;
                    if (i22 == 0) {
                        break;
                    }
                    i6 = i22;
                    i5 = i3;
                }
                i5 = i3;
            }
            j5 %= 65521;
            j3 %= 65521;
        }
        return (j3 << 16) | j5;
    }
}
