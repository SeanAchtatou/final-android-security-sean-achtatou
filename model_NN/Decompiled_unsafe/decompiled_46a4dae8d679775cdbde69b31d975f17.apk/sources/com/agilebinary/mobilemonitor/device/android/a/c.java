package com.agilebinary.mobilemonitor.device.android.a;

import android.util.Log;
import com.agilebinary.a.a.a.d.c.b;
import com.agilebinary.a.a.a.j;
import com.agilebinary.mobilemonitor.device.a.d.a;
import com.agilebinary.mobilemonitor.device.a.g.f;
import java.io.IOException;
import java.util.TimerTask;

public final class c extends TimerTask {
    private static String a = f.a();
    private b b;
    private long c;

    private c(long j) {
        this.c = j;
    }

    public static j a(com.agilebinary.mobilemonitor.device.a.e.b bVar, b bVar2, b bVar3, long j) {
        return new c(j).a(bVar2, bVar3, bVar);
    }

    private j a(b bVar, b bVar2, com.agilebinary.mobilemonitor.device.a.e.b bVar3) {
        this.b = bVar2;
        com.agilebinary.mobilemonitor.device.a.e.b.j().schedule(this, this.c);
        try {
            j a2 = bVar.a(bVar2);
            this.b = null;
            cancel();
            return a2;
        } catch (IOException e) {
            a.d(e);
            bVar2.d();
            cancel();
            bVar.d();
            return null;
        } catch (Exception e2) {
            a.d(e2);
            bVar2.d();
            cancel();
            bVar.d();
            return null;
        }
    }

    public final void run() {
        if (this.b != null) {
            this.b.d();
            Log.d(a, "RequestTimeoutTimerTask aborted a request due to timeout!");
        }
    }
}
