package com.agilebinary.mobilemonitor.device.android.device;

import android.telephony.NeighboringCellInfo;
import com.agilebinary.b.a.a.l;
import com.agilebinary.b.a.a.o;
import com.agilebinary.mobilemonitor.device.a.c.a.b;
import com.agilebinary.mobilemonitor.device.a.c.f;

public final class c extends b {
    private /* synthetic */ i a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public c(i iVar, com.agilebinary.mobilemonitor.device.a.a.c cVar, f fVar) {
        super(cVar, fVar);
        this.a = iVar;
    }

    public final o f() {
        l lVar = new l();
        int networkType = this.a.l.getNetworkType();
        if ((networkType == 7 || networkType == 5 || networkType == 6 || networkType == 7) ? false : true) {
            for (NeighboringCellInfo neighboringCellInfo : this.a.l.getNeighboringCellInfo()) {
                int cid = neighboringCellInfo.getCid();
                if (cid != -1) {
                    int rssi = neighboringCellInfo.getRssi();
                    if (networkType == 3 || networkType == 8 || networkType == 10 || networkType == 9) {
                        rssi /= 2;
                        if (rssi < 0) {
                            rssi = 0;
                        } else if (rssi > 31) {
                            rssi = 31;
                        }
                    }
                    lVar.b(new com.agilebinary.mobilemonitor.device.a.c.a.l(String.valueOf(cid), rssi));
                }
            }
        }
        return lVar;
    }
}
