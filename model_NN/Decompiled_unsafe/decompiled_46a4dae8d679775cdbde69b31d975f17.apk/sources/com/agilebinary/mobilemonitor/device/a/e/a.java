package com.agilebinary.mobilemonitor.device.a.e;

import com.agilebinary.mobilemonitor.device.a.c.d;
import com.agilebinary.mobilemonitor.device.a.g.f;

public abstract class a {
    private static String a = f.a();

    public abstract String a(String str);

    public final boolean a(d dVar) {
        return b() == 200 && (b(dVar) == 0 || a("SI-S-T") != null);
    }

    public abstract byte[] a();

    public abstract int b();

    public final int b(d dVar) {
        String a2 = a("SI-S");
        if (a2 == null) {
            return -2;
        }
        try {
            int parseInt = Integer.parseInt(a2);
            if (parseInt != 8 && parseInt != 9 && parseInt != 5 && parseInt != 7) {
                return parseInt;
            }
            if (dVar != null) {
                dVar.e();
            }
            throw new d(parseInt);
        } catch (NumberFormatException e) {
            return -2;
        }
    }

    public final String c() {
        return a("SI-S-T");
    }
}
