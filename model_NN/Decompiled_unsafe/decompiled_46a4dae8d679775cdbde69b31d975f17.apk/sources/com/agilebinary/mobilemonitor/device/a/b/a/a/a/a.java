package com.agilebinary.mobilemonitor.device.a.b.a.a.a;

import com.agilebinary.mobilemonitor.device.a.b.a.a.c;

public final class a extends b {
    private int a = -1;

    public a(com.agilebinary.mobilemonitor.device.a.b.a.a.d.a aVar) {
        super(aVar);
        this.a = aVar.e();
    }

    public final int a() {
        return this.a;
    }

    public final String a(c cVar) {
        return super.a(cVar) + "\nCellId: " + this.a;
    }

    public final void a(com.agilebinary.mobilemonitor.device.a.b.a.a.a aVar) {
        super.a(aVar);
        aVar.a(this.a);
    }

    public final byte b() {
        return 12;
    }
}
