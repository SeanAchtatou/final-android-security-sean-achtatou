package com.agilebinary.mobilemonitor.device.a.b.a.a.b.a.a;

final class a {
    private static final int[] a = {0, 1, 3, 7, 15, 31, 63, 127, 255, 511, 1023, 2047, 4095, 8191, 16383, 32767, 65535};
    private int b;
    private int c;
    private int[] d;
    private int e = 0;
    private int f;
    private int g;
    private int h;
    private int i;
    private byte j;
    private byte k;
    private int[] l;
    private int m;
    private int[] n;
    private int o;

    a() {
    }

    /* JADX WARN: Failed to insert an additional move for type inference into block B:258:0x010d */
    /* JADX WARN: Type inference failed for: r0v354, types: [int] */
    /* JADX WARN: Type inference failed for: r15v4 */
    /* access modifiers changed from: package-private */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final int a(com.agilebinary.mobilemonitor.device.a.b.a.a.b.a.a.b r27, com.agilebinary.mobilemonitor.device.a.b.a.a.b.a.a.g r28, int r29) {
        /*
            r26 = this;
            r0 = r28
            int r0 = r0.b
            r5 = r0
            r0 = r28
            int r0 = r0.c
            r6 = r0
            r0 = r27
            int r0 = r0.b
            r7 = r0
            r0 = r27
            int r0 = r0.a
            r8 = r0
            r0 = r27
            int r0 = r0.f
            r9 = r0
            r0 = r27
            int r0 = r0.e
            r10 = r0
            if (r9 >= r10) goto L_0x0072
            r0 = r27
            int r0 = r0.e
            r10 = r0
            int r10 = r10 - r9
            r11 = 1
            int r10 = r10 - r11
        L_0x0028:
            r11 = r29
            r24 = r9
            r9 = r8
            r8 = r5
            r5 = r10
            r10 = r7
            r7 = r6
            r6 = r24
        L_0x0033:
            r0 = r26
            int r0 = r0.b
            r12 = r0
            switch(r12) {
                case 0: goto L_0x0079;
                case 1: goto L_0x052f;
                case 2: goto L_0x0653;
                case 3: goto L_0x06de;
                case 4: goto L_0x07de;
                case 5: goto L_0x084b;
                case 6: goto L_0x095d;
                case 7: goto L_0x0a39;
                case 8: goto L_0x0b58;
                case 9: goto L_0x0adf;
                default: goto L_0x003b;
            }
        L_0x003b:
            r0 = r10
            r1 = r27
            r1.b = r0
            r0 = r9
            r1 = r27
            r1.a = r0
            r0 = r7
            r1 = r28
            r1.c = r0
            r0 = r28
            long r0 = r0.d
            r9 = r0
            r0 = r28
            int r0 = r0.b
            r5 = r0
            int r5 = r8 - r5
            long r11 = (long) r5
            long r9 = r9 + r11
            r0 = r9
            r2 = r28
            r2.d = r0
            r0 = r8
            r1 = r28
            r1.b = r0
            r0 = r6
            r1 = r27
            r1.f = r0
            r5 = -2
            r0 = r27
            r1 = r28
            r2 = r5
            int r5 = r0.b(r1, r2)
        L_0x0071:
            return r5
        L_0x0072:
            r0 = r27
            int r0 = r0.d
            r10 = r0
            int r10 = r10 - r9
            goto L_0x0028
        L_0x0079:
            r12 = 258(0x102, float:3.62E-43)
            if (r5 < r12) goto L_0x050b
            r12 = 10
            if (r7 < r12) goto L_0x050b
            r0 = r10
            r1 = r27
            r1.b = r0
            r0 = r9
            r1 = r27
            r1.a = r0
            r0 = r7
            r1 = r28
            r1.c = r0
            r0 = r28
            long r0 = r0.d
            r9 = r0
            r0 = r28
            int r0 = r0.b
            r5 = r0
            int r5 = r8 - r5
            long r11 = (long) r5
            long r9 = r9 + r11
            r0 = r9
            r2 = r28
            r2.d = r0
            r0 = r8
            r1 = r28
            r1.b = r0
            r0 = r6
            r1 = r27
            r1.f = r0
            r0 = r26
            byte r0 = r0.j
            r5 = r0
            r0 = r26
            byte r0 = r0.k
            r6 = r0
            r0 = r26
            int[] r0 = r0.l
            r7 = r0
            r0 = r26
            int r0 = r0.m
            r8 = r0
            r0 = r26
            int[] r0 = r0.n
            r9 = r0
            r0 = r26
            int r0 = r0.o
            r10 = r0
            r0 = r28
            int r0 = r0.b
            r11 = r0
            r0 = r28
            int r0 = r0.c
            r12 = r0
            r0 = r27
            int r0 = r0.b
            r13 = r0
            r0 = r27
            int r0 = r0.a
            r14 = r0
            r0 = r27
            int r0 = r0.f
            r15 = r0
            r0 = r27
            int r0 = r0.e
            r16 = r0
            r0 = r15
            r1 = r16
            if (r0 >= r1) goto L_0x012c
            r0 = r27
            int r0 = r0.e
            r16 = r0
            int r16 = r16 - r15
            r17 = 1
            int r16 = r16 - r17
        L_0x00fb:
            int[] r17 = com.agilebinary.mobilemonitor.device.a.b.a.a.b.a.a.a.a
            r5 = r17[r5]
            int[] r17 = com.agilebinary.mobilemonitor.device.a.b.a.a.b.a.a.a.a
            r6 = r17[r6]
            r24 = r16
            r16 = r13
            r13 = r12
            r12 = r15
            r15 = r14
            r14 = r11
            r11 = r24
        L_0x010d:
            r17 = 20
            r0 = r15
            r1 = r17
            if (r0 >= r1) goto L_0x0135
            int r13 = r13 + -1
            r0 = r28
            byte[] r0 = r0.a
            r17 = r0
            int r18 = r14 + 1
            byte r14 = r17[r14]
            r14 = r14 & 255(0xff, float:3.57E-43)
            int r14 = r14 << r15
            r14 = r14 | r16
            int r15 = r15 + 8
            r16 = r14
            r14 = r18
            goto L_0x010d
        L_0x012c:
            r0 = r27
            int r0 = r0.d
            r16 = r0
            int r16 = r16 - r15
            goto L_0x00fb
        L_0x0135:
            r17 = r16 & r5
            int r18 = r8 + r17
            int r18 = r18 * 3
            r19 = r7[r18]
            if (r19 != 0) goto L_0x0b4a
            int r17 = r18 + 1
            r17 = r7[r17]
            int r16 = r16 >> r17
            int r17 = r18 + 1
            r17 = r7[r17]
            int r15 = r15 - r17
            r0 = r27
            byte[] r0 = r0.c
            r17 = r0
            int r19 = r12 + 1
            int r18 = r18 + 2
            r18 = r7[r18]
            r0 = r18
            byte r0 = (byte) r0
            r18 = r0
            r17[r12] = r18
            int r11 = r11 + -1
            r12 = r19
        L_0x0162:
            r17 = 258(0x102, float:3.62E-43)
            r0 = r11
            r1 = r17
            if (r0 < r1) goto L_0x0170
            r17 = 10
            r0 = r13
            r1 = r17
            if (r0 >= r1) goto L_0x010d
        L_0x0170:
            r0 = r28
            int r0 = r0.c
            r5 = r0
            int r5 = r5 - r13
            int r6 = r15 >> 3
            if (r6 >= r5) goto L_0x017c
            int r5 = r15 >> 3
        L_0x017c:
            int r6 = r13 + r5
            int r7 = r14 - r5
            int r5 = r5 << 3
            int r5 = r15 - r5
            r0 = r16
            r1 = r27
            r1.b = r0
            r0 = r5
            r1 = r27
            r1.a = r0
            r0 = r6
            r1 = r28
            r1.c = r0
            r0 = r28
            long r0 = r0.d
            r5 = r0
            r0 = r28
            int r0 = r0.b
            r8 = r0
            int r8 = r7 - r8
            long r8 = (long) r8
            long r5 = r5 + r8
            r0 = r5
            r2 = r28
            r2.d = r0
            r0 = r7
            r1 = r28
            r1.b = r0
            r0 = r12
            r1 = r27
            r1.f = r0
            r5 = 0
        L_0x01b2:
            r0 = r28
            int r0 = r0.b
            r6 = r0
            r0 = r28
            int r0 = r0.c
            r7 = r0
            r0 = r27
            int r0 = r0.b
            r8 = r0
            r0 = r27
            int r0 = r0.a
            r9 = r0
            r0 = r27
            int r0 = r0.f
            r10 = r0
            r0 = r27
            int r0 = r0.e
            r11 = r0
            if (r10 >= r11) goto L_0x04f4
            r0 = r27
            int r0 = r0.e
            r11 = r0
            int r11 = r11 - r10
            r12 = 1
            int r11 = r11 - r12
        L_0x01da:
            if (r5 == 0) goto L_0x0500
            r12 = 1
            if (r5 != r12) goto L_0x04fc
            r12 = 7
        L_0x01e0:
            r0 = r12
            r1 = r26
            r1.b = r0
            r24 = r11
            r11 = r5
            r5 = r24
            r25 = r6
            r6 = r10
            r10 = r8
            r8 = r25
            goto L_0x0033
        L_0x01f2:
            r24 = r18
            r18 = r19
            r19 = r15
            r15 = r24
        L_0x01fa:
            int r20 = r15 + 1
            r20 = r7[r20]
            int r17 = r17 >> r20
            int r20 = r15 + 1
            r20 = r7[r20]
            int r16 = r16 - r20
            r20 = r18 & 16
            if (r20 == 0) goto L_0x041c
            r18 = r18 & 15
            int r15 = r15 + 2
            r15 = r7[r15]
            int[] r19 = com.agilebinary.mobilemonitor.device.a.b.a.a.b.a.a.a.a
            r19 = r19[r18]
            r19 = r19 & r17
            int r15 = r15 + r19
            int r17 = r17 >> r18
            int r16 = r16 - r18
        L_0x021c:
            r18 = 15
            r0 = r16
            r1 = r18
            if (r0 >= r1) goto L_0x023d
            int r13 = r13 + -1
            r0 = r28
            byte[] r0 = r0.a
            r18 = r0
            int r19 = r14 + 1
            byte r14 = r18[r14]
            r14 = r14 & 255(0xff, float:3.57E-43)
            int r14 = r14 << r16
            r14 = r14 | r17
            int r16 = r16 + 8
            r17 = r14
            r14 = r19
            goto L_0x021c
        L_0x023d:
            r18 = r17 & r6
            int r19 = r10 + r18
            int r19 = r19 * 3
            r20 = r9[r19]
            r24 = r19
            r19 = r20
            r20 = r18
            r18 = r17
            r17 = r16
            r16 = r24
        L_0x0251:
            int r21 = r16 + 1
            r21 = r9[r21]
            int r18 = r18 >> r21
            int r21 = r16 + 1
            r21 = r9[r21]
            int r17 = r17 - r21
            r21 = r19 & 16
            if (r21 == 0) goto L_0x03af
            r19 = r19 & 15
        L_0x0263:
            r0 = r17
            r1 = r19
            if (r0 >= r1) goto L_0x0282
            int r13 = r13 + -1
            r0 = r28
            byte[] r0 = r0.a
            r20 = r0
            int r21 = r14 + 1
            byte r14 = r20[r14]
            r14 = r14 & 255(0xff, float:3.57E-43)
            int r14 = r14 << r17
            r14 = r14 | r18
            int r17 = r17 + 8
            r18 = r14
            r14 = r21
            goto L_0x0263
        L_0x0282:
            int r16 = r16 + 2
            r16 = r9[r16]
            int[] r20 = com.agilebinary.mobilemonitor.device.a.b.a.a.b.a.a.a.a
            r20 = r20[r19]
            r20 = r20 & r18
            int r16 = r16 + r20
            int r18 = r18 >> r19
            int r17 = r17 - r19
            int r11 = r11 - r15
            r0 = r12
            r1 = r16
            if (r0 < r1) goto L_0x0326
            int r16 = r12 - r16
            int r19 = r12 - r16
            if (r19 <= 0) goto L_0x0300
            r19 = 2
            int r20 = r12 - r16
            r0 = r19
            r1 = r20
            if (r0 <= r1) goto L_0x0300
            r0 = r27
            byte[] r0 = r0.c
            r19 = r0
            int r20 = r12 + 1
            r0 = r27
            byte[] r0 = r0.c
            r21 = r0
            int r22 = r16 + 1
            byte r16 = r21[r16]
            r19[r12] = r16
            r0 = r27
            byte[] r0 = r0.c
            r12 = r0
            int r16 = r20 + 1
            r0 = r27
            byte[] r0 = r0.c
            r19 = r0
            int r21 = r22 + 1
            byte r19 = r19[r22]
            r12[r20] = r19
            int r12 = r15 + -2
            r15 = r16
            r16 = r21
        L_0x02d5:
            int r19 = r15 - r16
            if (r19 <= 0) goto L_0x0391
            int r19 = r15 - r16
            r0 = r12
            r1 = r19
            if (r0 <= r1) goto L_0x0391
        L_0x02e0:
            r0 = r27
            byte[] r0 = r0.c
            r19 = r0
            int r20 = r15 + 1
            r0 = r27
            byte[] r0 = r0.c
            r21 = r0
            int r22 = r16 + 1
            byte r16 = r21[r16]
            r19[r15] = r16
            int r12 = r12 + -1
            if (r12 != 0) goto L_0x0b35
            r12 = r20
            r15 = r17
            r16 = r18
            goto L_0x0162
        L_0x0300:
            r0 = r27
            byte[] r0 = r0.c
            r19 = r0
            r0 = r27
            byte[] r0 = r0.c
            r20 = r0
            r21 = 2
            r0 = r19
            r1 = r16
            r2 = r20
            r3 = r12
            r4 = r21
            java.lang.System.arraycopy(r0, r1, r2, r3, r4)
            int r12 = r12 + 2
            int r16 = r16 + 2
            int r15 = r15 + -2
            r24 = r15
            r15 = r12
            r12 = r24
            goto L_0x02d5
        L_0x0326:
            int r16 = r12 - r16
        L_0x0328:
            r0 = r27
            int r0 = r0.d
            r19 = r0
            int r16 = r16 + r19
            if (r16 < 0) goto L_0x0328
            r0 = r27
            int r0 = r0.d
            r19 = r0
            int r19 = r19 - r16
            r0 = r15
            r1 = r19
            if (r0 <= r1) goto L_0x0b43
            int r15 = r15 - r19
            int r20 = r12 - r16
            if (r20 <= 0) goto L_0x0376
            int r20 = r12 - r16
            r0 = r19
            r1 = r20
            if (r0 <= r1) goto L_0x0376
            r24 = r19
            r19 = r16
            r16 = r24
        L_0x0353:
            r0 = r27
            byte[] r0 = r0.c
            r20 = r0
            int r21 = r12 + 1
            r0 = r27
            byte[] r0 = r0.c
            r22 = r0
            int r23 = r19 + 1
            byte r19 = r22[r19]
            r20[r12] = r19
            int r12 = r16 + -1
            if (r12 != 0) goto L_0x0b3b
            r12 = r21
        L_0x036d:
            r16 = 0
            r24 = r15
            r15 = r12
            r12 = r24
            goto L_0x02d5
        L_0x0376:
            r0 = r27
            byte[] r0 = r0.c
            r20 = r0
            r0 = r27
            byte[] r0 = r0.c
            r21 = r0
            r0 = r20
            r1 = r16
            r2 = r21
            r3 = r12
            r4 = r19
            java.lang.System.arraycopy(r0, r1, r2, r3, r4)
            int r12 = r12 + r19
            goto L_0x036d
        L_0x0391:
            r0 = r27
            byte[] r0 = r0.c
            r19 = r0
            r0 = r27
            byte[] r0 = r0.c
            r20 = r0
            r0 = r19
            r1 = r16
            r2 = r20
            r3 = r15
            r4 = r12
            java.lang.System.arraycopy(r0, r1, r2, r3, r4)
            int r12 = r12 + r15
            r15 = r17
            r16 = r18
            goto L_0x0162
        L_0x03af:
            r21 = r19 & 64
            if (r21 != 0) goto L_0x03d1
            int r16 = r16 + 2
            r16 = r9[r16]
            int r16 = r16 + r20
            int[] r20 = com.agilebinary.mobilemonitor.device.a.b.a.a.b.a.a.a.a
            r19 = r20[r19]
            r19 = r19 & r18
            int r16 = r16 + r19
            int r19 = r10 + r16
            int r19 = r19 * 3
            r20 = r9[r19]
            r24 = r19
            r19 = r20
            r20 = r16
            r16 = r24
            goto L_0x0251
        L_0x03d1:
            java.lang.String r5 = "invalid distance code"
            r0 = r5
            r1 = r28
            r1.i = r0
            r0 = r28
            int r0 = r0.c
            r5 = r0
            int r5 = r5 - r13
            int r6 = r17 >> 3
            if (r6 >= r5) goto L_0x03e4
            int r5 = r17 >> 3
        L_0x03e4:
            int r6 = r13 + r5
            int r7 = r14 - r5
            int r5 = r5 << 3
            int r5 = r17 - r5
            r0 = r18
            r1 = r27
            r1.b = r0
            r0 = r5
            r1 = r27
            r1.a = r0
            r0 = r6
            r1 = r28
            r1.c = r0
            r0 = r28
            long r0 = r0.d
            r5 = r0
            r0 = r28
            int r0 = r0.b
            r8 = r0
            int r8 = r7 - r8
            long r8 = (long) r8
            long r5 = r5 + r8
            r0 = r5
            r2 = r28
            r2.d = r0
            r0 = r7
            r1 = r28
            r1.b = r0
            r0 = r12
            r1 = r27
            r1.f = r0
            r5 = -3
            goto L_0x01b2
        L_0x041c:
            r20 = r18 & 64
            if (r20 != 0) goto L_0x0461
            int r15 = r15 + 2
            r15 = r7[r15]
            int r15 = r15 + r19
            int[] r19 = com.agilebinary.mobilemonitor.device.a.b.a.a.b.a.a.a.a
            r18 = r19[r18]
            r18 = r18 & r17
            int r15 = r15 + r18
            int r18 = r8 + r15
            int r18 = r18 * 3
            r19 = r7[r18]
            if (r19 != 0) goto L_0x01f2
            int r15 = r18 + 1
            r15 = r7[r15]
            int r15 = r17 >> r15
            int r17 = r18 + 1
            r17 = r7[r17]
            int r16 = r16 - r17
            r0 = r27
            byte[] r0 = r0.c
            r17 = r0
            int r19 = r12 + 1
            int r18 = r18 + 2
            r18 = r7[r18]
            r0 = r18
            byte r0 = (byte) r0
            r18 = r0
            r17[r12] = r18
            int r11 = r11 + -1
            r12 = r19
            r24 = r16
            r16 = r15
            r15 = r24
            goto L_0x0162
        L_0x0461:
            r5 = r18 & 32
            if (r5 == 0) goto L_0x04a9
            r0 = r28
            int r0 = r0.c
            r5 = r0
            int r5 = r5 - r13
            int r6 = r16 >> 3
            if (r6 >= r5) goto L_0x0471
            int r5 = r16 >> 3
        L_0x0471:
            int r6 = r13 + r5
            int r7 = r14 - r5
            int r5 = r5 << 3
            int r5 = r16 - r5
            r0 = r17
            r1 = r27
            r1.b = r0
            r0 = r5
            r1 = r27
            r1.a = r0
            r0 = r6
            r1 = r28
            r1.c = r0
            r0 = r28
            long r0 = r0.d
            r5 = r0
            r0 = r28
            int r0 = r0.b
            r8 = r0
            int r8 = r7 - r8
            long r8 = (long) r8
            long r5 = r5 + r8
            r0 = r5
            r2 = r28
            r2.d = r0
            r0 = r7
            r1 = r28
            r1.b = r0
            r0 = r12
            r1 = r27
            r1.f = r0
            r5 = 1
            goto L_0x01b2
        L_0x04a9:
            java.lang.String r5 = "invalid literal/length code"
            r0 = r5
            r1 = r28
            r1.i = r0
            r0 = r28
            int r0 = r0.c
            r5 = r0
            int r5 = r5 - r13
            int r6 = r16 >> 3
            if (r6 >= r5) goto L_0x04bc
            int r5 = r16 >> 3
        L_0x04bc:
            int r6 = r13 + r5
            int r7 = r14 - r5
            int r5 = r5 << 3
            int r5 = r16 - r5
            r0 = r17
            r1 = r27
            r1.b = r0
            r0 = r5
            r1 = r27
            r1.a = r0
            r0 = r6
            r1 = r28
            r1.c = r0
            r0 = r28
            long r0 = r0.d
            r5 = r0
            r0 = r28
            int r0 = r0.b
            r8 = r0
            int r8 = r7 - r8
            long r8 = (long) r8
            long r5 = r5 + r8
            r0 = r5
            r2 = r28
            r2.d = r0
            r0 = r7
            r1 = r28
            r1.b = r0
            r0 = r12
            r1 = r27
            r1.f = r0
            r5 = -3
            goto L_0x01b2
        L_0x04f4:
            r0 = r27
            int r0 = r0.d
            r11 = r0
            int r11 = r11 - r10
            goto L_0x01da
        L_0x04fc:
            r12 = 9
            goto L_0x01e0
        L_0x0500:
            r24 = r11
            r11 = r5
            r5 = r24
            r25 = r6
            r6 = r10
            r10 = r8
            r8 = r25
        L_0x050b:
            r0 = r26
            byte r0 = r0.j
            r12 = r0
            r0 = r12
            r1 = r26
            r1.f = r0
            r0 = r26
            int[] r0 = r0.l
            r12 = r0
            r0 = r12
            r1 = r26
            r1.d = r0
            r0 = r26
            int r0 = r0.m
            r12 = r0
            r0 = r12
            r1 = r26
            r1.e = r0
            r12 = 1
            r0 = r12
            r1 = r26
            r1.b = r0
        L_0x052f:
            r0 = r26
            int r0 = r0.f
            r12 = r0
        L_0x0534:
            if (r9 >= r12) goto L_0x0584
            if (r7 == 0) goto L_0x054d
            r11 = 0
            int r7 = r7 + -1
            r0 = r28
            byte[] r0 = r0.a
            r13 = r0
            int r14 = r8 + 1
            byte r8 = r13[r8]
            r8 = r8 & 255(0xff, float:3.57E-43)
            int r8 = r8 << r9
            r8 = r8 | r10
            int r9 = r9 + 8
            r10 = r8
            r8 = r14
            goto L_0x0534
        L_0x054d:
            r0 = r10
            r1 = r27
            r1.b = r0
            r0 = r9
            r1 = r27
            r1.a = r0
            r0 = r7
            r1 = r28
            r1.c = r0
            r0 = r28
            long r0 = r0.d
            r9 = r0
            r0 = r28
            int r0 = r0.b
            r5 = r0
            int r5 = r8 - r5
            long r12 = (long) r5
            long r9 = r9 + r12
            r0 = r9
            r2 = r28
            r2.d = r0
            r0 = r8
            r1 = r28
            r1.b = r0
            r0 = r6
            r1 = r27
            r1.f = r0
            r0 = r27
            r1 = r28
            r2 = r11
            int r5 = r0.b(r1, r2)
            goto L_0x0071
        L_0x0584:
            r0 = r26
            int r0 = r0.e
            r13 = r0
            int[] r14 = com.agilebinary.mobilemonitor.device.a.b.a.a.b.a.a.a.a
            r12 = r14[r12]
            r12 = r12 & r10
            int r12 = r12 + r13
            int r12 = r12 * 3
            r0 = r26
            int[] r0 = r0.d
            r13 = r0
            int r14 = r12 + 1
            r13 = r13[r14]
            int r10 = r10 >>> r13
            r0 = r26
            int[] r0 = r0.d
            r13 = r0
            int r14 = r12 + 1
            r13 = r13[r14]
            int r9 = r9 - r13
            r0 = r26
            int[] r0 = r0.d
            r13 = r0
            r13 = r13[r12]
            if (r13 != 0) goto L_0x05c4
            r0 = r26
            int[] r0 = r0.d
            r13 = r0
            int r12 = r12 + 2
            r12 = r13[r12]
            r0 = r12
            r1 = r26
            r1.g = r0
            r12 = 6
            r0 = r12
            r1 = r26
            r1.b = r0
            goto L_0x0033
        L_0x05c4:
            r14 = r13 & 16
            if (r14 == 0) goto L_0x05e5
            r13 = r13 & 15
            r0 = r13
            r1 = r26
            r1.h = r0
            r0 = r26
            int[] r0 = r0.d
            r13 = r0
            int r12 = r12 + 2
            r12 = r13[r12]
            r0 = r12
            r1 = r26
            r1.c = r0
            r12 = 2
            r0 = r12
            r1 = r26
            r1.b = r0
            goto L_0x0033
        L_0x05e5:
            r14 = r13 & 64
            if (r14 != 0) goto L_0x0601
            r0 = r13
            r1 = r26
            r1.f = r0
            int r13 = r12 / 3
            r0 = r26
            int[] r0 = r0.d
            r14 = r0
            int r12 = r12 + 2
            r12 = r14[r12]
            int r12 = r12 + r13
            r0 = r12
            r1 = r26
            r1.e = r0
            goto L_0x0033
        L_0x0601:
            r12 = r13 & 32
            if (r12 == 0) goto L_0x060d
            r12 = 7
            r0 = r12
            r1 = r26
            r1.b = r0
            goto L_0x0033
        L_0x060d:
            r5 = 9
            r0 = r5
            r1 = r26
            r1.b = r0
            java.lang.String r5 = "invalid literal/length code"
            r0 = r5
            r1 = r28
            r1.i = r0
            r0 = r10
            r1 = r27
            r1.b = r0
            r0 = r9
            r1 = r27
            r1.a = r0
            r0 = r7
            r1 = r28
            r1.c = r0
            r0 = r28
            long r0 = r0.d
            r9 = r0
            r0 = r28
            int r0 = r0.b
            r5 = r0
            int r5 = r8 - r5
            long r11 = (long) r5
            long r9 = r9 + r11
            r0 = r9
            r2 = r28
            r2.d = r0
            r0 = r8
            r1 = r28
            r1.b = r0
            r0 = r6
            r1 = r27
            r1.f = r0
            r5 = -3
            r0 = r27
            r1 = r28
            r2 = r5
            int r5 = r0.b(r1, r2)
            goto L_0x0071
        L_0x0653:
            r0 = r26
            int r0 = r0.h
            r12 = r0
        L_0x0658:
            if (r9 >= r12) goto L_0x06a8
            if (r7 == 0) goto L_0x0671
            r11 = 0
            int r7 = r7 + -1
            r0 = r28
            byte[] r0 = r0.a
            r13 = r0
            int r14 = r8 + 1
            byte r8 = r13[r8]
            r8 = r8 & 255(0xff, float:3.57E-43)
            int r8 = r8 << r9
            r8 = r8 | r10
            int r9 = r9 + 8
            r10 = r8
            r8 = r14
            goto L_0x0658
        L_0x0671:
            r0 = r10
            r1 = r27
            r1.b = r0
            r0 = r9
            r1 = r27
            r1.a = r0
            r0 = r7
            r1 = r28
            r1.c = r0
            r0 = r28
            long r0 = r0.d
            r9 = r0
            r0 = r28
            int r0 = r0.b
            r5 = r0
            int r5 = r8 - r5
            long r12 = (long) r5
            long r9 = r9 + r12
            r0 = r9
            r2 = r28
            r2.d = r0
            r0 = r8
            r1 = r28
            r1.b = r0
            r0 = r6
            r1 = r27
            r1.f = r0
            r0 = r27
            r1 = r28
            r2 = r11
            int r5 = r0.b(r1, r2)
            goto L_0x0071
        L_0x06a8:
            r0 = r26
            int r0 = r0.c
            r13 = r0
            int[] r14 = com.agilebinary.mobilemonitor.device.a.b.a.a.b.a.a.a.a
            r14 = r14[r12]
            r14 = r14 & r10
            int r13 = r13 + r14
            r0 = r13
            r1 = r26
            r1.c = r0
            int r10 = r10 >> r12
            int r9 = r9 - r12
            r0 = r26
            byte r0 = r0.k
            r12 = r0
            r0 = r12
            r1 = r26
            r1.f = r0
            r0 = r26
            int[] r0 = r0.n
            r12 = r0
            r0 = r12
            r1 = r26
            r1.d = r0
            r0 = r26
            int r0 = r0.o
            r12 = r0
            r0 = r12
            r1 = r26
            r1.e = r0
            r12 = 3
            r0 = r12
            r1 = r26
            r1.b = r0
        L_0x06de:
            r0 = r26
            int r0 = r0.f
            r12 = r0
        L_0x06e3:
            if (r9 >= r12) goto L_0x0733
            if (r7 == 0) goto L_0x06fc
            r11 = 0
            int r7 = r7 + -1
            r0 = r28
            byte[] r0 = r0.a
            r13 = r0
            int r14 = r8 + 1
            byte r8 = r13[r8]
            r8 = r8 & 255(0xff, float:3.57E-43)
            int r8 = r8 << r9
            r8 = r8 | r10
            int r9 = r9 + 8
            r10 = r8
            r8 = r14
            goto L_0x06e3
        L_0x06fc:
            r0 = r10
            r1 = r27
            r1.b = r0
            r0 = r9
            r1 = r27
            r1.a = r0
            r0 = r7
            r1 = r28
            r1.c = r0
            r0 = r28
            long r0 = r0.d
            r9 = r0
            r0 = r28
            int r0 = r0.b
            r5 = r0
            int r5 = r8 - r5
            long r12 = (long) r5
            long r9 = r9 + r12
            r0 = r9
            r2 = r28
            r2.d = r0
            r0 = r8
            r1 = r28
            r1.b = r0
            r0 = r6
            r1 = r27
            r1.f = r0
            r0 = r27
            r1 = r28
            r2 = r11
            int r5 = r0.b(r1, r2)
            goto L_0x0071
        L_0x0733:
            r0 = r26
            int r0 = r0.e
            r13 = r0
            int[] r14 = com.agilebinary.mobilemonitor.device.a.b.a.a.b.a.a.a.a
            r12 = r14[r12]
            r12 = r12 & r10
            int r12 = r12 + r13
            int r12 = r12 * 3
            r0 = r26
            int[] r0 = r0.d
            r13 = r0
            int r14 = r12 + 1
            r13 = r13[r14]
            int r10 = r10 >> r13
            r0 = r26
            int[] r0 = r0.d
            r13 = r0
            int r14 = r12 + 1
            r13 = r13[r14]
            int r9 = r9 - r13
            r0 = r26
            int[] r0 = r0.d
            r13 = r0
            r13 = r13[r12]
            r14 = r13 & 16
            if (r14 == 0) goto L_0x077c
            r13 = r13 & 15
            r0 = r13
            r1 = r26
            r1.h = r0
            r0 = r26
            int[] r0 = r0.d
            r13 = r0
            int r12 = r12 + 2
            r12 = r13[r12]
            r0 = r12
            r1 = r26
            r1.i = r0
            r12 = 4
            r0 = r12
            r1 = r26
            r1.b = r0
            goto L_0x0033
        L_0x077c:
            r14 = r13 & 64
            if (r14 != 0) goto L_0x0798
            r0 = r13
            r1 = r26
            r1.f = r0
            int r13 = r12 / 3
            r0 = r26
            int[] r0 = r0.d
            r14 = r0
            int r12 = r12 + 2
            r12 = r14[r12]
            int r12 = r12 + r13
            r0 = r12
            r1 = r26
            r1.e = r0
            goto L_0x0033
        L_0x0798:
            r5 = 9
            r0 = r5
            r1 = r26
            r1.b = r0
            java.lang.String r5 = "invalid distance code"
            r0 = r5
            r1 = r28
            r1.i = r0
            r0 = r10
            r1 = r27
            r1.b = r0
            r0 = r9
            r1 = r27
            r1.a = r0
            r0 = r7
            r1 = r28
            r1.c = r0
            r0 = r28
            long r0 = r0.d
            r9 = r0
            r0 = r28
            int r0 = r0.b
            r5 = r0
            int r5 = r8 - r5
            long r11 = (long) r5
            long r9 = r9 + r11
            r0 = r9
            r2 = r28
            r2.d = r0
            r0 = r8
            r1 = r28
            r1.b = r0
            r0 = r6
            r1 = r27
            r1.f = r0
            r5 = -3
            r0 = r27
            r1 = r28
            r2 = r5
            int r5 = r0.b(r1, r2)
            goto L_0x0071
        L_0x07de:
            r0 = r26
            int r0 = r0.h
            r12 = r0
        L_0x07e3:
            if (r9 >= r12) goto L_0x0833
            if (r7 == 0) goto L_0x07fc
            r11 = 0
            int r7 = r7 + -1
            r0 = r28
            byte[] r0 = r0.a
            r13 = r0
            int r14 = r8 + 1
            byte r8 = r13[r8]
            r8 = r8 & 255(0xff, float:3.57E-43)
            int r8 = r8 << r9
            r8 = r8 | r10
            int r9 = r9 + 8
            r10 = r8
            r8 = r14
            goto L_0x07e3
        L_0x07fc:
            r0 = r10
            r1 = r27
            r1.b = r0
            r0 = r9
            r1 = r27
            r1.a = r0
            r0 = r7
            r1 = r28
            r1.c = r0
            r0 = r28
            long r0 = r0.d
            r9 = r0
            r0 = r28
            int r0 = r0.b
            r5 = r0
            int r5 = r8 - r5
            long r12 = (long) r5
            long r9 = r9 + r12
            r0 = r9
            r2 = r28
            r2.d = r0
            r0 = r8
            r1 = r28
            r1.b = r0
            r0 = r6
            r1 = r27
            r1.f = r0
            r0 = r27
            r1 = r28
            r2 = r11
            int r5 = r0.b(r1, r2)
            goto L_0x0071
        L_0x0833:
            r0 = r26
            int r0 = r0.i
            r13 = r0
            int[] r14 = com.agilebinary.mobilemonitor.device.a.b.a.a.b.a.a.a.a
            r14 = r14[r12]
            r14 = r14 & r10
            int r13 = r13 + r14
            r0 = r13
            r1 = r26
            r1.i = r0
            int r10 = r10 >> r12
            int r9 = r9 - r12
            r12 = 5
            r0 = r12
            r1 = r26
            r1.b = r0
        L_0x084b:
            r0 = r26
            int r0 = r0.i
            r12 = r0
            int r12 = r6 - r12
        L_0x0852:
            if (r12 >= 0) goto L_0x0b2e
            r0 = r27
            int r0 = r0.d
            r13 = r0
            int r12 = r12 + r13
            goto L_0x0852
        L_0x085b:
            r24 = r6
            r6 = r12
            r12 = r5
            r5 = r24
        L_0x0861:
            r0 = r27
            byte[] r0 = r0.c
            r13 = r0
            int r14 = r6 + 1
            r0 = r27
            byte[] r0 = r0.c
            r15 = r0
            int r16 = r11 + 1
            byte r11 = r15[r11]
            r13[r6] = r11
            int r5 = r5 + -1
            r0 = r27
            int r0 = r0.d
            r6 = r0
            r0 = r16
            r1 = r6
            if (r0 != r1) goto L_0x0b23
            r6 = 0
        L_0x0880:
            r0 = r26
            int r0 = r0.c
            r11 = r0
            r13 = 1
            int r11 = r11 - r13
            r0 = r11
            r1 = r26
            r1.c = r0
            r11 = r6
            r6 = r14
        L_0x088e:
            r0 = r26
            int r0 = r0.c
            r13 = r0
            if (r13 == 0) goto L_0x0954
            if (r5 != 0) goto L_0x0861
            r0 = r27
            int r0 = r0.d
            r13 = r0
            if (r6 != r13) goto L_0x08bb
            r0 = r27
            int r0 = r0.e
            r13 = r0
            if (r13 == 0) goto L_0x08bb
            r5 = 0
            r0 = r27
            int r0 = r0.e
            r6 = r0
            if (r6 <= 0) goto L_0x093c
            r0 = r27
            int r0 = r0.e
            r6 = r0
            r13 = 0
            int r6 = r6 - r13
            r13 = 1
            int r6 = r6 - r13
        L_0x08b6:
            r24 = r6
            r6 = r5
            r5 = r24
        L_0x08bb:
            if (r5 != 0) goto L_0x0861
            r0 = r6
            r1 = r27
            r1.f = r0
            r0 = r27
            r1 = r28
            r2 = r12
            int r5 = r0.b(r1, r2)
            r0 = r27
            int r0 = r0.f
            r6 = r0
            r0 = r27
            int r0 = r0.e
            r12 = r0
            if (r6 >= r12) goto L_0x0945
            r0 = r27
            int r0 = r0.e
            r12 = r0
            int r12 = r12 - r6
            r13 = 1
            int r12 = r12 - r13
        L_0x08df:
            r0 = r27
            int r0 = r0.d
            r13 = r0
            if (r6 != r13) goto L_0x0b27
            r0 = r27
            int r0 = r0.e
            r13 = r0
            if (r13 == 0) goto L_0x0b27
            r6 = 0
            r0 = r27
            int r0 = r0.e
            r12 = r0
            if (r12 <= 0) goto L_0x094c
            r0 = r27
            int r0 = r0.e
            r12 = r0
            r13 = 0
            int r12 = r12 - r13
            r13 = 1
            int r12 = r12 - r13
        L_0x08fe:
            r24 = r12
            r12 = r6
            r6 = r24
        L_0x0903:
            if (r6 != 0) goto L_0x085b
            r0 = r10
            r1 = r27
            r1.b = r0
            r0 = r9
            r1 = r27
            r1.a = r0
            r0 = r7
            r1 = r28
            r1.c = r0
            r0 = r28
            long r0 = r0.d
            r6 = r0
            r0 = r28
            int r0 = r0.b
            r9 = r0
            int r9 = r8 - r9
            long r9 = (long) r9
            long r6 = r6 + r9
            r0 = r6
            r2 = r28
            r2.d = r0
            r0 = r8
            r1 = r28
            r1.b = r0
            r0 = r12
            r1 = r27
            r1.f = r0
            r0 = r27
            r1 = r28
            r2 = r5
            int r5 = r0.b(r1, r2)
            goto L_0x0071
        L_0x093c:
            r0 = r27
            int r0 = r0.d
            r6 = r0
            r13 = 0
            int r6 = r6 - r13
            goto L_0x08b6
        L_0x0945:
            r0 = r27
            int r0 = r0.d
            r12 = r0
            int r12 = r12 - r6
            goto L_0x08df
        L_0x094c:
            r0 = r27
            int r0 = r0.d
            r12 = r0
            r13 = 0
            int r12 = r12 - r13
            goto L_0x08fe
        L_0x0954:
            r11 = 0
            r0 = r11
            r1 = r26
            r1.b = r0
            r11 = r12
            goto L_0x0033
        L_0x095d:
            if (r5 != 0) goto L_0x0a1e
            r0 = r27
            int r0 = r0.d
            r12 = r0
            if (r6 != r12) goto L_0x0983
            r0 = r27
            int r0 = r0.e
            r12 = r0
            if (r12 == 0) goto L_0x0983
            r5 = 0
            r0 = r27
            int r0 = r0.e
            r6 = r0
            if (r6 <= 0) goto L_0x0a04
            r0 = r27
            int r0 = r0.e
            r6 = r0
            r12 = 0
            int r6 = r6 - r12
            r12 = 1
            int r6 = r6 - r12
        L_0x097e:
            r24 = r6
            r6 = r5
            r5 = r24
        L_0x0983:
            if (r5 != 0) goto L_0x0a1e
            r0 = r6
            r1 = r27
            r1.f = r0
            r0 = r27
            r1 = r28
            r2 = r11
            int r5 = r0.b(r1, r2)
            r0 = r27
            int r0 = r0.f
            r6 = r0
            r0 = r27
            int r0 = r0.e
            r11 = r0
            if (r6 >= r11) goto L_0x0a0d
            r0 = r27
            int r0 = r0.e
            r11 = r0
            int r11 = r11 - r6
            r12 = 1
            int r11 = r11 - r12
        L_0x09a7:
            r0 = r27
            int r0 = r0.d
            r12 = r0
            if (r6 != r12) goto L_0x0b1c
            r0 = r27
            int r0 = r0.e
            r12 = r0
            if (r12 == 0) goto L_0x0b1c
            r6 = 0
            r0 = r27
            int r0 = r0.e
            r11 = r0
            if (r11 <= 0) goto L_0x0a14
            r0 = r27
            int r0 = r0.e
            r11 = r0
            r12 = 0
            int r11 = r11 - r12
            r12 = 1
            int r11 = r11 - r12
        L_0x09c6:
            r24 = r11
            r11 = r6
            r6 = r24
        L_0x09cb:
            if (r6 != 0) goto L_0x0a1c
            r0 = r10
            r1 = r27
            r1.b = r0
            r0 = r9
            r1 = r27
            r1.a = r0
            r0 = r7
            r1 = r28
            r1.c = r0
            r0 = r28
            long r0 = r0.d
            r6 = r0
            r0 = r28
            int r0 = r0.b
            r9 = r0
            int r9 = r8 - r9
            long r9 = (long) r9
            long r6 = r6 + r9
            r0 = r6
            r2 = r28
            r2.d = r0
            r0 = r8
            r1 = r28
            r1.b = r0
            r0 = r11
            r1 = r27
            r1.f = r0
            r0 = r27
            r1 = r28
            r2 = r5
            int r5 = r0.b(r1, r2)
            goto L_0x0071
        L_0x0a04:
            r0 = r27
            int r0 = r0.d
            r6 = r0
            r12 = 0
            int r6 = r6 - r12
            goto L_0x097e
        L_0x0a0d:
            r0 = r27
            int r0 = r0.d
            r11 = r0
            int r11 = r11 - r6
            goto L_0x09a7
        L_0x0a14:
            r0 = r27
            int r0 = r0.d
            r11 = r0
            r12 = 0
            int r11 = r11 - r12
            goto L_0x09c6
        L_0x0a1c:
            r5 = r6
            r6 = r11
        L_0x0a1e:
            r11 = 0
            r0 = r27
            byte[] r0 = r0.c
            r12 = r0
            int r13 = r6 + 1
            r0 = r26
            int r0 = r0.g
            r14 = r0
            byte r14 = (byte) r14
            r12[r6] = r14
            int r5 = r5 + -1
            r6 = 0
            r0 = r6
            r1 = r26
            r1.b = r0
            r6 = r13
            goto L_0x0033
        L_0x0a39:
            r5 = 7
            if (r9 <= r5) goto L_0x0b17
            int r5 = r9 + -8
            int r7 = r7 + 1
            int r8 = r8 + -1
            r24 = r7
            r7 = r8
            r8 = r5
            r5 = r24
        L_0x0a48:
            r0 = r6
            r1 = r27
            r1.f = r0
            r0 = r27
            r1 = r28
            r2 = r11
            int r6 = r0.b(r1, r2)
            r0 = r27
            int r0 = r0.f
            r9 = r0
            r0 = r27
            int r0 = r0.e
            r11 = r0
            r0 = r27
            int r0 = r0.f
            r12 = r0
            if (r11 == r12) goto L_0x0a9e
            r0 = r10
            r1 = r27
            r1.b = r0
            r0 = r8
            r1 = r27
            r1.a = r0
            r0 = r5
            r1 = r28
            r1.c = r0
            r0 = r28
            long r0 = r0.d
            r10 = r0
            r0 = r28
            int r0 = r0.b
            r5 = r0
            int r5 = r7 - r5
            long r12 = (long) r5
            long r10 = r10 + r12
            r0 = r10
            r2 = r28
            r2.d = r0
            r0 = r7
            r1 = r28
            r1.b = r0
            r0 = r9
            r1 = r27
            r1.f = r0
            r0 = r27
            r1 = r28
            r2 = r6
            int r5 = r0.b(r1, r2)
            goto L_0x0071
        L_0x0a9e:
            r6 = 8
            r0 = r6
            r1 = r26
            r1.b = r0
            r6 = r5
            r5 = r9
        L_0x0aa7:
            r0 = r10
            r1 = r27
            r1.b = r0
            r0 = r8
            r1 = r27
            r1.a = r0
            r0 = r6
            r1 = r28
            r1.c = r0
            r0 = r28
            long r0 = r0.d
            r8 = r0
            r0 = r28
            int r0 = r0.b
            r6 = r0
            int r6 = r7 - r6
            long r10 = (long) r6
            long r8 = r8 + r10
            r0 = r8
            r2 = r28
            r2.d = r0
            r0 = r7
            r1 = r28
            r1.b = r0
            r0 = r5
            r1 = r27
            r1.f = r0
            r5 = 1
            r0 = r27
            r1 = r28
            r2 = r5
            int r5 = r0.b(r1, r2)
            goto L_0x0071
        L_0x0adf:
            r0 = r10
            r1 = r27
            r1.b = r0
            r0 = r9
            r1 = r27
            r1.a = r0
            r0 = r7
            r1 = r28
            r1.c = r0
            r0 = r28
            long r0 = r0.d
            r9 = r0
            r0 = r28
            int r0 = r0.b
            r5 = r0
            int r5 = r8 - r5
            long r11 = (long) r5
            long r9 = r9 + r11
            r0 = r9
            r2 = r28
            r2.d = r0
            r0 = r8
            r1 = r28
            r1.b = r0
            r0 = r6
            r1 = r27
            r1.f = r0
            r5 = -3
            r0 = r27
            r1 = r28
            r2 = r5
            int r5 = r0.b(r1, r2)
            goto L_0x0071
        L_0x0b17:
            r5 = r7
            r7 = r8
            r8 = r9
            goto L_0x0a48
        L_0x0b1c:
            r24 = r11
            r11 = r6
            r6 = r24
            goto L_0x09cb
        L_0x0b23:
            r6 = r16
            goto L_0x0880
        L_0x0b27:
            r24 = r12
            r12 = r6
            r6 = r24
            goto L_0x0903
        L_0x0b2e:
            r24 = r12
            r12 = r11
            r11 = r24
            goto L_0x088e
        L_0x0b35:
            r15 = r20
            r16 = r22
            goto L_0x02e0
        L_0x0b3b:
            r16 = r12
            r19 = r23
            r12 = r21
            goto L_0x0353
        L_0x0b43:
            r24 = r15
            r15 = r12
            r12 = r24
            goto L_0x02d5
        L_0x0b4a:
            r24 = r18
            r18 = r19
            r19 = r17
            r17 = r16
            r16 = r15
            r15 = r24
            goto L_0x01fa
        L_0x0b58:
            r5 = r6
            r6 = r7
            r7 = r8
            r8 = r9
            goto L_0x0aa7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.mobilemonitor.device.a.b.a.a.b.a.a.a.a(com.agilebinary.mobilemonitor.device.a.b.a.a.b.a.a.b, com.agilebinary.mobilemonitor.device.a.b.a.a.b.a.a.g, int):int");
    }

    /* access modifiers changed from: package-private */
    public final void a(int i2, int i3, int[] iArr, int i4, int[] iArr2, int i5) {
        this.b = 0;
        this.j = (byte) i2;
        this.k = (byte) i3;
        this.l = iArr;
        this.m = i4;
        this.n = iArr2;
        this.o = i5;
        this.d = null;
    }
}
