package com.agilebinary.mobilemonitor.device.android.ui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

final class a extends BroadcastReceiver {
    private /* synthetic */ MainActivity a;

    a(MainActivity mainActivity) {
        this.a = mainActivity;
    }

    public final void onReceive(Context context, Intent intent) {
        if (this.a.t != null && this.a.t.isShowing()) {
            this.a.t.cancel();
        }
        this.a.a((String) null);
    }
}
