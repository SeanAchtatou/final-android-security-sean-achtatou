package com.agilebinary.mobilemonitor.device.a.b.a.a;

import com.agilebinary.a.a.a.aa;
import com.agilebinary.a.a.a.e.b;
import com.agilebinary.mobilemonitor.device.a.b.a.a.b.a.a.e;
import com.agilebinary.mobilemonitor.device.a.b.a.a.d.c;

public class a {
    private c a;
    private com.agilebinary.mobilemonitor.device.a.b.a.a.c.a.a b;
    private com.agilebinary.mobilemonitor.device.a.b.a.a.c.a.a c;
    private boolean d;
    private int e;

    private a() {
    }

    public a(boolean z, int i) {
        this.d = z;
        this.e = i;
        this.b = new com.agilebinary.mobilemonitor.device.a.b.a.a.c.a.a(8192);
        this.c = new com.agilebinary.mobilemonitor.device.a.b.a.a.c.a.a(8192);
        this.a = new c(this.c);
    }

    public static String a(b bVar) {
        if (bVar == null) {
            throw new IllegalArgumentException("HTTP parameters may not be null");
        }
        String str = (String) bVar.a("http.protocol.element-charset");
        return str == null ? "US-ASCII" : str;
    }

    public static com.agilebinary.a.a.a.a b(b bVar) {
        if (bVar == null) {
            throw new IllegalArgumentException("HTTP parameters may not be null");
        }
        Object a2 = bVar.a("http.protocol.version");
        return a2 == null ? aa.d : (com.agilebinary.a.a.a.a) a2;
    }

    public final void a(byte b2) {
        this.a.b(b2);
    }

    public final void a(double d2) {
        this.a.a(d2);
    }

    public final void a(int i) {
        this.a.c(i);
    }

    public final void a(long j) {
        this.a.a(j);
    }

    public final void a(String str) {
        this.a.a(str);
    }

    public final void a(short s, byte b2) {
        this.b.a(0);
        this.b.a(0);
        this.b.a(b2);
    }

    public final void a(boolean z) {
        this.a.a(z);
    }

    public final void a(byte[] bArr) {
        this.a.a(bArr);
    }

    public final void a(String[] strArr) {
        c cVar = this.a;
        if (strArr == null) {
            cVar.c(0);
            return;
        }
        cVar.c(strArr.length);
        for (String a2 : strArr) {
            cVar.a(a2);
        }
    }

    public final byte[] a() {
        com.agilebinary.mobilemonitor.device.a.b.a.a.c.a.b bVar = this.b;
        if (this.d) {
            throw new IllegalArgumentException("PENDING: encryption not yet implemented");
        }
        if (this.e > 0) {
            bVar = new e(bVar, this.e);
        }
        bVar.a(this.c.c());
        bVar.a();
        byte[] c2 = this.b.c();
        this.b.b();
        this.c.b();
        return c2;
    }
}
