package com.agilebinary.mobilemonitor.device.a.c.a;

import com.agilebinary.b.a.a.e;
import com.agilebinary.b.a.a.l;
import com.agilebinary.b.a.a.o;
import com.agilebinary.mobilemonitor.device.a.a.c;
import com.agilebinary.mobilemonitor.device.a.g.f;

public abstract class b implements d {
    private static final String a = f.a();
    private c b;
    private o c;
    private e d = new e(this);

    public b(c cVar, com.agilebinary.mobilemonitor.device.a.c.f fVar) {
        this.b = cVar;
        this.c = new l();
    }

    private synchronized void g() {
        o f = f();
        e.a(f, this.d);
        this.c = f;
    }

    public final void a() {
    }

    public final void b() {
        g();
    }

    public final void c() {
    }

    public final void d() {
    }

    public final synchronized f e() {
        f fVar;
        o f = f();
        e.a(f, this.d);
        if (this.c.a() == 0 || f.a() == 0) {
            fVar = new f(0, a + ": no current and reference cells");
        } else {
            l lVar = (l) f.a(0);
            int c2 = this.c.c(lVar);
            if (c2 == -1) {
                fVar = new f(1, a + ": strongest cell not found in ref list ");
            } else {
                int abs = Math.abs(((l) this.c.a(c2)).a - lVar.a);
                fVar = abs >= this.b.t() ? new f(1, a + ": strongest cell asu change= " + abs) : new f(2, a + ": strongest cell asu change= " + abs);
            }
        }
        if (fVar.a != 2) {
            g();
        }
        "" + fVar;
        return fVar;
    }

    public abstract o f();
}
