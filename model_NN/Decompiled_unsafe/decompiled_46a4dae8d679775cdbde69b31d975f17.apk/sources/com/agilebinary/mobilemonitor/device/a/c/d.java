package com.agilebinary.mobilemonitor.device.a.c;

import com.agilebinary.mobilemonitor.device.a.a.b;
import com.agilebinary.mobilemonitor.device.a.a.c;
import com.agilebinary.mobilemonitor.device.a.b.l;
import com.agilebinary.mobilemonitor.device.a.b.m;
import com.agilebinary.mobilemonitor.device.a.e.a.h;
import com.agilebinary.mobilemonitor.device.a.e.a.i;
import com.agilebinary.mobilemonitor.device.a.e.b.e;
import com.agilebinary.mobilemonitor.device.a.g.f;
import com.agilebinary.mobilemonitor.device.a.j.a.a;

public abstract class d implements com.agilebinary.mobilemonitor.device.a.g.d {
    private static final String c = f.a();
    protected c a;
    protected boolean b;
    private com.agilebinary.mobilemonitor.device.a.e.b.c d;
    private com.agilebinary.mobilemonitor.device.a.b.c e;
    private m f;
    private a g;
    private g h;
    private com.agilebinary.mobilemonitor.device.a.e.b.d i;
    private i j;
    private b k;
    private boolean l;

    /* access modifiers changed from: protected */
    public abstract g a(b bVar, c cVar);

    /* access modifiers changed from: protected */
    public abstract e a(f fVar, com.agilebinary.mobilemonitor.device.a.j.a.b bVar, b bVar2, c cVar);

    /* access modifiers changed from: protected */
    public abstract com.agilebinary.mobilemonitor.device.a.j.a.b a(f fVar, c cVar, b bVar);

    public void a() {
        if (!this.l) {
            this.a = c.a();
            if (this.a == null) {
                throw new com.agilebinary.mobilemonitor.device.a.g.c();
            }
            this.k = b.a();
            if (this.k == null) {
                throw new com.agilebinary.mobilemonitor.device.a.g.c();
            }
            this.h = a(this.k, this.a);
            this.g = (a) a(this.h, this.a, this.k);
            this.i = a(this.h, this.g, this.k, this.a);
            this.j = b(this.h, this.g, this.k, this.a);
            ((e) this.i).a(this.j);
            i iVar = this.j;
            a aVar = this.g;
            com.agilebinary.mobilemonitor.device.a.e.b.d dVar = this.i;
            this.f = new m(this, iVar, aVar, this.h, this.k, this.a);
            this.a.a((com.agilebinary.mobilemonitor.device.a.b.a) this.f);
            this.a.a((l) this.f);
            this.d = new com.agilebinary.mobilemonitor.device.a.e.b.c(this.f, this.i, this.h, this.a, this.g);
            this.e = new com.agilebinary.mobilemonitor.device.a.b.c(this.f, this.d, this.a, this.k);
            this.h.a(this.e);
            this.h.a();
            this.i.a();
            this.j.a();
            this.g.a();
            this.d.a();
            this.f.a();
            this.l = true;
        }
    }

    /* access modifiers changed from: protected */
    public abstract h b(f fVar, com.agilebinary.mobilemonitor.device.a.j.a.b bVar, b bVar2, c cVar);

    public void b() {
        if (!this.b) {
            this.g.b();
            this.h.b();
            this.i.b();
            this.j.b();
            this.d.b();
            this.f.b();
            this.b = true;
        }
    }

    public void c() {
        if (this.b) {
            this.f.c();
            this.d.c();
            this.i.c();
            this.j.c();
            this.h.c();
            this.g.c();
            this.i.c();
            this.j.c();
            this.b = false;
        }
    }

    public final void d() {
        if (this.l) {
            this.f.d();
            this.d.d();
            this.i.d();
            this.j.d();
            this.h.d();
            com.agilebinary.mobilemonitor.device.a.i.a.b();
            this.g.d();
            this.i.d();
            this.j.d();
            this.l = false;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.mobilemonitor.device.a.a.c.b(java.lang.String, boolean):void
     arg types: [?[OBJECT, ARRAY], int]
     candidates:
      com.agilebinary.mobilemonitor.device.a.a.e.b(java.lang.String, java.lang.String):void
      com.agilebinary.mobilemonitor.device.a.a.c.b(java.lang.String, boolean):void */
    public void e() {
        com.agilebinary.mobilemonitor.device.a.i.a.a().cancel();
        this.a.b((String) null, true);
        this.a.aa();
        c();
        d();
        try {
            this.g.a();
            this.g.b();
            this.g.k();
            this.g.c();
            this.g.d();
        } catch (com.agilebinary.mobilemonitor.device.a.j.a e2) {
            e2.printStackTrace();
        }
    }

    public final com.agilebinary.mobilemonitor.device.a.e.b.c f() {
        return this.d;
    }

    public final m g() {
        return this.f;
    }

    public final com.agilebinary.mobilemonitor.device.a.j.a.b h() {
        return this.g;
    }

    public final g i() {
        return this.h;
    }

    public final i j() {
        return this.j;
    }

    public final void k() {
        int v = this.a.v();
        if (v > 0) {
            this.h.a("LocXB", new a(this, "ForceGetLocationAndUploadAfterBoot"), null, (long) (v * 1000), true);
        }
        if (this.a.j()) {
            try {
                this.e.a(System.currentTimeMillis(), 2);
            } catch (Exception e2) {
                com.agilebinary.mobilemonitor.device.a.d.a.e(e2);
            }
        }
    }
}
