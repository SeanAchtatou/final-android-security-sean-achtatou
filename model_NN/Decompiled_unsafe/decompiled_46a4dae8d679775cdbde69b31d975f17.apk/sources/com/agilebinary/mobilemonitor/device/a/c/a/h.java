package com.agilebinary.mobilemonitor.device.a.c.a;

import com.agilebinary.mobilemonitor.device.a.a.c;
import com.agilebinary.mobilemonitor.device.a.g.f;

public final class h implements d {
    private static final String a = f.a();
    private String b = "-1";
    private com.agilebinary.mobilemonitor.device.a.c.f c;

    public h(c cVar, com.agilebinary.mobilemonitor.device.a.c.f fVar) {
        this.c = fVar;
    }

    private String f() {
        return this.c.A();
    }

    private void g() {
        this.b = f();
    }

    public final void a() {
    }

    public final void b() {
        g();
    }

    public final void c() {
    }

    public final void d() {
    }

    public final synchronized f e() {
        f fVar;
        String f = f();
        fVar = this.b.equals("-1") ? new f(0, a + ": current cell id unknown") : !this.b.equals(f) ? new f(1, a + ": current cell id = " + f + ", ref cell id = " + this.b) : new f(0, a + ": current cell id unchanged");
        if (fVar.a != 2) {
            g();
        }
        "" + fVar;
        return fVar;
    }
}
