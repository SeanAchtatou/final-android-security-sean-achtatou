package com.agilebinary.mobilemonitor.device.a.j.a;

import com.agilebinary.mobilemonitor.device.a.a.c;
import com.agilebinary.mobilemonitor.device.a.b.b;
import com.agilebinary.mobilemonitor.device.a.g.f;
import java.util.Enumeration;
import java.util.Vector;

public abstract class a implements b {
    private static final String a = f.a();
    public static final b[] b = new b[0];
    protected com.agilebinary.mobilemonitor.device.a.a.b c;
    protected c d;
    private com.agilebinary.mobilemonitor.device.a.c.f e;
    private d f = new d(this);
    private Vector g = new Vector(1);
    private com.agilebinary.a.a.b.a.a h;

    protected a(com.agilebinary.mobilemonitor.device.a.c.f fVar, c cVar, com.agilebinary.mobilemonitor.device.a.a.b bVar, com.agilebinary.a.a.b.a.a aVar) {
        this.e = fVar;
        this.c = bVar;
        this.d = cVar;
        this.h = aVar;
    }

    private void a(Exception exc) {
        c();
        d();
        this.h.a();
        a();
        b();
        throw new com.agilebinary.mobilemonitor.device.a.j.a(exc);
    }

    private void m() {
        if (this.g.size() > 0) {
            long l = l();
            Enumeration elements = this.g.elements();
            while (elements.hasMoreElements()) {
                ((c) elements.nextElement()).a(l);
            }
        }
    }

    public final synchronized void a() {
        try {
            e();
        } catch (com.agilebinary.mobilemonitor.device.a.j.a e2) {
            com.agilebinary.mobilemonitor.device.a.d.a.e(e2);
        }
        return;
    }

    /* access modifiers changed from: protected */
    public abstract void a(long j);

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x001c, code lost:
        if (r0.a(r3) != false) goto L_0x001e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x001f, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0020, code lost:
        com.agilebinary.mobilemonitor.device.a.d.a.e(r0);
        a(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:6:?, code lost:
        m();
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized void a(com.agilebinary.mobilemonitor.device.a.b.b r3, com.agilebinary.mobilemonitor.device.a.e.a.i r4, com.agilebinary.mobilemonitor.device.a.b.m r5) {
        /*
            r2 = this;
            monitor-enter(r2)
            com.agilebinary.mobilemonitor.device.a.a.b r0 = r2.c     // Catch:{ all -> 0x0027 }
            int r1 = r3.i()     // Catch:{ all -> 0x0027 }
            int r0 = r0.k(r1)     // Catch:{ all -> 0x0027 }
            com.agilebinary.mobilemonitor.device.a.j.a.a.a r0 = com.agilebinary.mobilemonitor.device.a.j.a.a.e.a(r0, r2, r4, r5)     // Catch:{ all -> 0x0027 }
        L_0x000f:
            r2.b(r3)     // Catch:{ b -> 0x0017, Exception -> 0x001f }
        L_0x0012:
            r2.m()     // Catch:{ all -> 0x0027 }
            monitor-exit(r2)
            return
        L_0x0017:
            r1 = move-exception
            boolean r1 = r0.a(r3)     // Catch:{ all -> 0x0027 }
            if (r1 == 0) goto L_0x0012
            goto L_0x000f
        L_0x001f:
            r0 = move-exception
            com.agilebinary.mobilemonitor.device.a.d.a.e(r0)     // Catch:{ all -> 0x0027 }
            r2.a(r0)     // Catch:{ all -> 0x0027 }
            goto L_0x0012
        L_0x0027:
            r0 = move-exception
            monitor-exit(r2)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.mobilemonitor.device.a.j.a.a.a(com.agilebinary.mobilemonitor.device.a.b.b, com.agilebinary.mobilemonitor.device.a.e.a.i, com.agilebinary.mobilemonitor.device.a.b.m):void");
    }

    public final void a(c cVar) {
        this.g.addElement(cVar);
    }

    public final synchronized boolean a(long j, int i) {
        boolean z;
        try {
            if (c((long) ((int) j)) >= this.c.i(i)) {
                a((long) ((int) j));
                m();
                z = true;
            } else {
                b(j);
                z = false;
            }
        } catch (Exception e2) {
            com.agilebinary.mobilemonitor.device.a.d.a.e(e2);
            a(e2);
            z = false;
        }
        return z;
    }

    public abstract long b(b bVar);

    public final synchronized void b() {
        try {
            f();
        } catch (com.agilebinary.mobilemonitor.device.a.j.a e2) {
            com.agilebinary.mobilemonitor.device.a.d.a.e(e2);
        }
        return;
    }

    /* access modifiers changed from: protected */
    public abstract void b(long j);

    public final void b(c cVar) {
        this.g.removeElement(cVar);
    }

    /* access modifiers changed from: protected */
    public abstract int c(long j);

    public final synchronized void c() {
        try {
            g();
        } catch (com.agilebinary.mobilemonitor.device.a.j.a e2) {
            com.agilebinary.mobilemonitor.device.a.d.a.e(e2);
        }
        return;
    }

    public final synchronized void d() {
    }

    public final synchronized void d(long j) {
        try {
            a(j);
            m();
        } catch (Exception e2) {
            com.agilebinary.mobilemonitor.device.a.d.a.e(e2);
            a(e2);
        }
        return;
    }

    public abstract void e();

    public abstract void f();

    public abstract void g();

    /* access modifiers changed from: protected */
    public abstract void j();

    public final synchronized b[] n() {
        b[] bVarArr;
        try {
            j();
            bVarArr = h();
            this.e.a(bVarArr, this.f);
            m();
        } catch (Exception e2) {
            com.agilebinary.mobilemonitor.device.a.d.a.e(e2);
            a(e2);
            bVarArr = null;
        }
        return bVarArr;
    }

    public final synchronized b[] o() {
        b[] bVarArr;
        try {
            bVarArr = h();
            this.e.a(bVarArr, this.f);
        } catch (Exception e2) {
            com.agilebinary.mobilemonitor.device.a.d.a.e(e2);
            a(e2);
            bVarArr = null;
        }
        return bVarArr;
    }
}
