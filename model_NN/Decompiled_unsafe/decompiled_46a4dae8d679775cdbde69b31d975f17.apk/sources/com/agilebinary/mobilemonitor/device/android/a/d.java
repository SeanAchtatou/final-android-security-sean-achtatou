package com.agilebinary.mobilemonitor.device.android.a;

import com.agilebinary.mobilemonitor.device.a.a.b;
import com.agilebinary.mobilemonitor.device.a.d.a;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PublicKey;
import java.security.SignatureException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.security.spec.X509EncodedKeySpec;
import java.util.HashSet;
import javax.net.ssl.X509TrustManager;

public final class d implements X509TrustManager {
    private PublicKey a;
    private boolean b;
    private HashSet c = new HashSet();

    d(e eVar, b bVar) {
        String p = bVar.p();
        if (p.trim().length() > 0) {
            try {
                this.a = KeyFactory.getInstance("RSA").generatePublic(new X509EncodedKeySpec(com.agilebinary.mobilemonitor.device.android.b.b.a(p.toCharArray())));
            } catch (Exception e) {
                this.b = true;
            }
        }
    }

    public final void checkClientTrusted(X509Certificate[] x509CertificateArr, String str) {
    }

    public final void checkServerTrusted(X509Certificate[] x509CertificateArr, String str) {
        if (x509CertificateArr.length == 0) {
            throw new CertificateException("no certificate in chain");
        } else if (this.b) {
            throw new CertificateException("certificate invalid because couldn't decode configured public key");
        } else if (this.a != null) {
            X509Certificate x509Certificate = x509CertificateArr[0];
            if (!this.c.contains(x509Certificate)) {
                try {
                    x509Certificate.verify(this.a);
                    this.c.add(x509Certificate);
                } catch (InvalidKeyException e) {
                    a.e(e);
                    throw new CertificateException(e);
                } catch (NoSuchAlgorithmException e2) {
                    a.e(e2);
                    throw new CertificateException(e2);
                } catch (NoSuchProviderException e3) {
                    a.e(e3);
                    throw new CertificateException(e3);
                } catch (SignatureException e4) {
                    a.e(e4);
                    throw new CertificateException(e4);
                }
            }
        }
    }

    public final X509Certificate[] getAcceptedIssuers() {
        return null;
    }
}
