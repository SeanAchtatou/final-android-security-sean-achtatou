package com.agilebinary.mobilemonitor.device.a.c.a;

public final class c {
    int a;
    private String b;

    public c(String str, int i) {
        this.b = str;
        this.a = i;
    }

    public final boolean equals(Object obj) {
        if (!(obj instanceof c)) {
            return false;
        }
        return this.b.equals(((c) obj).b);
    }

    public final int hashCode() {
        return this.b.hashCode();
    }
}
