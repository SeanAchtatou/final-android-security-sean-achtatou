package com.agilebinary.mobilemonitor.device.android.device;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import com.agilebinary.mobilemonitor.device.a.a.b;
import com.agilebinary.mobilemonitor.device.a.a.f;
import com.agilebinary.mobilemonitor.device.a.a.h;
import com.agilebinary.mobilemonitor.device.a.b.n;
import com.agilebinary.mobilemonitor.device.a.c.g;
import com.agilebinary.mobilemonitor.device.a.e.a.e;
import com.agilebinary.mobilemonitor.device.a.j.a.c;
import com.agilebinary.mobilemonitor.device.android.d.a;
import com.agilebinary.mobilemonitor.device.android.services.BackgroundService;
import com.agilebinary.phonebeagle.R;
import java.util.Locale;

public final class d extends com.agilebinary.mobilemonitor.device.a.c.d implements n, e, c {
    private static h e;
    private Context c;
    private BackgroundService d;

    public d(Context context, BackgroundService backgroundService) {
        this.c = context;
        this.d = backgroundService;
        a(context);
    }

    public static void a(Context context) {
        new a();
        if (e == null) {
            e = new com.agilebinary.mobilemonitor.device.android.e.a();
        }
        b.a(e);
        com.agilebinary.mobilemonitor.device.a.f.b.a(e, Locale.getDefault().getLanguage(), Locale.getDefault().getCountry(), new com.agilebinary.mobilemonitor.device.a.g.a());
        com.agilebinary.mobilemonitor.device.a.a.c.a(e, new com.agilebinary.mobilemonitor.device.android.c.b.a(context), new f(), b.a(), new com.agilebinary.a.a.b.a.a(context));
    }

    private void b(boolean z) {
        int i;
        NotificationManager notificationManager = (NotificationManager) this.c.getSystemService("notification");
        if (!this.b || !z) {
            notificationManager.cancelAll();
            return;
        }
        String a = com.agilebinary.mobilemonitor.device.a.f.b.a("STATUSBAR_ACTIVATED_TITLE");
        String a2 = com.agilebinary.mobilemonitor.device.a.f.b.a("STATUSBAR_ACTIVATED_TEXT");
        try {
            i = ((Integer) com.agilebinary.c.a.class.getField(b.a().r()).get(null)).intValue();
        } catch (Exception e2) {
            e2.printStackTrace();
            i = R.drawable.paw_25;
        }
        Notification notification = new Notification(i, a2, System.currentTimeMillis());
        notification.flags = 34;
        notification.setLatestEventInfo(this.c, a, a2, PendingIntent.getBroadcast(this.c, 0, new Intent(), 0));
        notificationManager.notify(0, notification);
    }

    /* access modifiers changed from: protected */
    public final g a(b bVar, com.agilebinary.mobilemonitor.device.a.a.c cVar) {
        return new a(this.c, bVar, cVar, com.agilebinary.mobilemonitor.device.a.g.f.a());
    }

    /* access modifiers changed from: protected */
    public final com.agilebinary.mobilemonitor.device.a.e.b.e a(com.agilebinary.mobilemonitor.device.a.c.f fVar, com.agilebinary.mobilemonitor.device.a.j.a.b bVar, b bVar2, com.agilebinary.mobilemonitor.device.a.a.c cVar) {
        return new com.agilebinary.mobilemonitor.device.android.a.c.a(this, fVar, bVar, bVar2, cVar);
    }

    /* access modifiers changed from: protected */
    public final com.agilebinary.mobilemonitor.device.a.j.a.b a(com.agilebinary.mobilemonitor.device.a.c.f fVar, com.agilebinary.mobilemonitor.device.a.a.c cVar, b bVar) {
        return new com.agilebinary.mobilemonitor.device.android.c.a.a(this.c, fVar, cVar, bVar);
    }

    public final void a() {
        a(this.c);
        super.a();
    }

    public final void a(long j) {
        if (this.d != null) {
            this.d.a(j);
        }
    }

    public final void a(long j, int i) {
        if (this.d != null) {
            this.d.a(j, i);
        }
    }

    public final void a(boolean z) {
        b(z);
    }

    /* access modifiers changed from: protected */
    public final com.agilebinary.mobilemonitor.device.a.e.a.h b(com.agilebinary.mobilemonitor.device.a.c.f fVar, com.agilebinary.mobilemonitor.device.a.j.a.b bVar, b bVar2, com.agilebinary.mobilemonitor.device.a.a.c cVar) {
        return new com.agilebinary.mobilemonitor.device.android.a.a.a(this, fVar, bVar, bVar2, cVar);
    }

    public final void b() {
        super.b();
        h().a(this);
        j().a(this);
        b(this.a.r());
        this.a.a(this);
    }

    public final void c() {
        this.a.a((n) null);
        h().b(this);
        j().b(this);
        super.c();
        b(this.a.r());
    }

    public final void e() {
        super.e();
        if (this.d != null) {
            this.d.a();
            this.d.stopSelf();
        }
    }
}
