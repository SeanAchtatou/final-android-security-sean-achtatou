package com.agilebinary.mobilemonitor.device.android.ui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.agilebinary.mobilemonitor.device.a.f.b;

final class ac extends BroadcastReceiver {
    private /* synthetic */ MainActivity a;

    ac(MainActivity mainActivity) {
        this.a = mainActivity;
    }

    public final void onReceive(Context context, Intent intent) {
        if (this.a.t != null && this.a.t.isShowing()) {
            this.a.t.cancel();
        }
        if (!intent.hasExtra("EXTRA_SERVICE_STATUS")) {
            return;
        }
        if (intent.getBooleanExtra("EXTRA_SERVICE_STATUS", false)) {
            this.a.B.add(new ag(b.a("COMMON_SERVICE_STARTED")));
        } else {
            this.a.B.add(new ag(b.a("COMMON_SERVICE_STOPPED")));
        }
    }
}
