package com.agilebinary.mobilemonitor.device.android.ui;

import com.agilebinary.mobilemonitor.device.a.a.c;
import com.agilebinary.mobilemonitor.device.a.f.b;

final class n extends ak {
    private /* synthetic */ MainActivity b;

    n(MainActivity mainActivity) {
        this.b = mainActivity;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.mobilemonitor.device.a.e.b.a(boolean, boolean, boolean):void
     arg types: [int, int, int]
     candidates:
      com.agilebinary.mobilemonitor.device.a.e.b.a(java.lang.String, long, byte[]):com.agilebinary.mobilemonitor.device.a.e.a
      com.agilebinary.mobilemonitor.device.a.e.b.a(boolean, boolean, boolean):void */
    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ Object a(Object[] objArr) {
        this.b.D.a(true, true, true);
        if (c()) {
            return null;
        }
        int q = this.b.D.q();
        b bVar = new b(this.b);
        bVar.a = q != 0;
        bVar.b = b.a("CONNECTION_TYPE_" + q);
        return bVar;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        super.a();
        try {
            this.b.u.hide();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void a(Object obj) {
        b bVar = (b) obj;
        super.a(bVar);
        try {
            this.b.u.hide();
            if (bVar == null || !bVar.a || c()) {
                this.b.B.add(new ag(b.a("CONNECTION_TEST_FAILED")));
                return;
            }
            this.b.B.add(new ag(b.a("CONNECTION_TEST_SUCCESS", bVar.b)));
            if (c.a().Z()) {
                new z(this.b).b(new Void[0]);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
