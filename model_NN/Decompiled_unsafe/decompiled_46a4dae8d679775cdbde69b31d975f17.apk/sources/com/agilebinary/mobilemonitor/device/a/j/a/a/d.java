package com.agilebinary.mobilemonitor.device.a.j.a.a;

import com.agilebinary.mobilemonitor.device.a.b.m;
import com.agilebinary.mobilemonitor.device.a.e.a.i;
import com.agilebinary.mobilemonitor.device.a.j.a.b;

public final class d extends f {
    private int c;
    private m d;

    public d(int i, b bVar, i iVar, m mVar) {
        super(bVar, iVar);
        this.c = i;
        this.d = mVar;
    }

    public final boolean a(com.agilebinary.mobilemonitor.device.a.b.b bVar) {
        if (bVar.p() || (this.c != 3 && this.c != this.b.q())) {
            return super.a(bVar);
        }
        this.d.a(bVar, this.c);
        return false;
    }
}
