package com.agilebinary.mobilemonitor.device.android.ui;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import com.agilebinary.mobilemonitor.device.a.a.c;
import com.agilebinary.phonebeagle.R;

public final class ah extends Dialog implements View.OnClickListener {
    private EditText a = ((EditText) findViewById(R.id.smsrec_number));
    private Button b = ((Button) findViewById(R.id.smsrec_ok));

    public ah(Context context) {
        super(context);
        setContentView((int) R.layout.enter_alert_sms_recipient_dialog);
        getWindow().setLayout(-1, -2);
        setTitle(c.a().l());
        setCancelable(true);
        this.b.setOnClickListener(this);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.mobilemonitor.device.a.a.c.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.agilebinary.mobilemonitor.device.a.a.c.a(int, boolean):void
      com.agilebinary.mobilemonitor.device.a.a.e.a(java.lang.String, int):int
      com.agilebinary.mobilemonitor.device.a.a.e.a(java.lang.String, java.lang.String):java.lang.String
      com.agilebinary.mobilemonitor.device.a.a.e.a(com.agilebinary.mobilemonitor.device.a.a.g, boolean):void
      com.agilebinary.mobilemonitor.device.a.a.e.a(java.lang.String, long):void
      com.agilebinary.mobilemonitor.device.a.a.c.a(java.lang.String, boolean):void */
    public final void onClick(View view) {
        c.a().a(this.a.getEditableText().toString(), true);
        hide();
    }
}
