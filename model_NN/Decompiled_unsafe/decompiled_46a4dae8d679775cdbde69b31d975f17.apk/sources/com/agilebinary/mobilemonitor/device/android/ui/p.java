package com.agilebinary.mobilemonitor.device.android.ui;

import com.agilebinary.mobilemonitor.device.a.d.a;
import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

final class p extends FutureTask {
    private /* synthetic */ ak a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    p(ak akVar, Callable callable) {
        super(callable);
        this.a = akVar;
    }

    /* access modifiers changed from: protected */
    public final void done() {
        Object obj;
        try {
            obj = get();
        } catch (InterruptedException e) {
            a.d(e);
            obj = null;
        } catch (ExecutionException e2) {
            throw new RuntimeException("An error occured while executing doInBackground()", e2.getCause());
        } catch (CancellationException e3) {
            ak.d.obtainMessage(3, new ai(this.a, null)).sendToTarget();
            return;
        } catch (Throwable th) {
            throw new RuntimeException("An error occured while executing doInBackground()", th);
        }
        ak.d.obtainMessage(1, new ai(this.a, obj)).sendToTarget();
    }
}
