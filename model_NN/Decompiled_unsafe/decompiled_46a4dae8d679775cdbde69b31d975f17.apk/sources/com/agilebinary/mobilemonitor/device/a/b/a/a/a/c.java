package com.agilebinary.mobilemonitor.device.a.b.a.a.a;

import com.agilebinary.mobilemonitor.device.a.b.a.a.d;
import com.agilebinary.mobilemonitor.device.a.b.a.a.d.a;

public final class c extends q {
    private long a;
    private long b;
    private long c;
    private byte d;
    private String e;
    private String f;

    public c(a aVar) {
        super(aVar);
        this.a = aVar.f();
        this.b = aVar.f();
        this.c = aVar.f();
        this.d = aVar.c();
        this.e = aVar.i();
        this.f = aVar.i();
    }

    public c(String str, String str2, d dVar, long j, long j2, long j3, byte b2, String str3, String str4) {
        super(str, str2, dVar);
        this.a = j;
        this.b = j2;
        this.c = j3;
        this.d = b2;
        this.e = str3;
        this.f = str4;
    }

    public final String a(com.agilebinary.mobilemonitor.device.a.b.a.a.c cVar) {
        return super.a(cVar) + "\nTimeInitiated: " + cVar.a(this.a) + "\nTimeConnected: " + cVar.a(this.b) + "\nTimeTerminated: " + cVar.a(this.c) + "\nDirection: " + ((int) this.d) + "\nPhoneNumber: " + this.e + "\nRemotParty: " + this.f;
    }

    public final void a(com.agilebinary.mobilemonitor.device.a.b.a.a.a aVar) {
        super.a(aVar);
        aVar.a(this.a);
        aVar.a(this.b);
        aVar.a(this.c);
        aVar.a(this.d);
        aVar.a(this.e);
        aVar.a(this.f);
    }

    public final byte b() {
        return 1;
    }
}
