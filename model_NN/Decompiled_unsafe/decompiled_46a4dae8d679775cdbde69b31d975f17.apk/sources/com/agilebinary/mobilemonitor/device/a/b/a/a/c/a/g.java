package com.agilebinary.mobilemonitor.device.a.b.a.a.c.a;

public class g extends b {
    protected b a;

    public g(b bVar) {
        this.a = bVar;
    }

    public final void a() {
        this.a.a();
    }

    public final void a(int i) {
        this.a.a(i);
    }

    public void a(byte[] bArr) {
        a(bArr, 0, bArr.length);
    }

    public void a(byte[] bArr, int i, int i2) {
        if ((i | i2 | (bArr.length - (i2 + i)) | (i + i2)) < 0) {
            throw new IndexOutOfBoundsException();
        }
        for (int i3 = 0; i3 < i2; i3++) {
            a(bArr[i + i3]);
        }
    }
}
