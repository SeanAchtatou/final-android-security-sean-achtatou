package com.agilebinary.mobilemonitor.device.a.c.a;

import com.agilebinary.b.a.a.a;
import com.agilebinary.b.a.a.o;
import com.agilebinary.mobilemonitor.device.a.g.f;

public final class j implements d {
    private static final String a = f.a();
    private o b;

    public j(o oVar) {
        this.b = oVar;
        if (this.b.a() == 0) {
            this.b.b(new a());
        }
    }

    public final void a() {
        a b2 = this.b.b();
        while (b2.a()) {
            ((d) b2.b()).a();
        }
    }

    public final void b() {
        a b2 = this.b.b();
        while (b2.a()) {
            ((d) b2.b()).b();
        }
    }

    public final void c() {
        a b2 = this.b.b();
        while (b2.a()) {
            ((d) b2.b()).c();
        }
    }

    public final void d() {
        a b2 = this.b.b();
        while (b2.a()) {
            ((d) b2.b()).d();
        }
        this.b.d();
    }

    public final f e() {
        StringBuffer stringBuffer = new StringBuffer();
        a b2 = this.b.b();
        boolean z = true;
        boolean z2 = true;
        while (b2.a()) {
            f e = ((d) b2.b()).e();
            stringBuffer.append(e.b).append(", ");
            if (e.a != 0) {
                if (e.a == 1) {
                    z = false;
                    z2 = false;
                } else {
                    z = false;
                }
            }
        }
        return z ? new f(0, stringBuffer.toString()) : z2 ? new f(2, stringBuffer.toString()) : new f(1, stringBuffer.toString());
    }
}
