package com.agilebinary.mobilemonitor.device.a.e.b;

import com.agilebinary.mobilemonitor.device.a.a.a;
import com.agilebinary.mobilemonitor.device.a.b.m;
import com.agilebinary.mobilemonitor.device.a.c.g;
import com.agilebinary.mobilemonitor.device.a.g.d;
import com.agilebinary.mobilemonitor.device.a.g.f;
import com.agilebinary.mobilemonitor.device.a.j.a.b;

public final class c implements a, d {
    private static String a = f.a();
    private b b;
    private d c;
    private com.agilebinary.mobilemonitor.device.a.g.a.d d = new f(this);
    private com.agilebinary.mobilemonitor.device.a.a.c e;
    private com.agilebinary.mobilemonitor.device.a.c.f f;

    public c(m mVar, d dVar, g gVar, com.agilebinary.mobilemonitor.device.a.a.c cVar, b bVar) {
        this.f = gVar;
        this.b = new b(this, mVar, cVar, dVar, gVar, bVar);
        this.c = dVar;
        this.e = cVar;
    }

    private int h() {
        return this.e.p() * 60000;
    }

    public final void a() {
    }

    public final void a(String str, String str2) {
        this.b.a(str, str2);
    }

    public final boolean a(String str) {
        return this.b.a(str);
    }

    public final void b() {
        long h = (long) h();
        this.f.a("CmdP", this.d, null, true, h, h, true);
    }

    public final void c() {
        this.f.a("CmdP", this.d);
    }

    public final void d() {
    }

    public final synchronized void e() {
        this.f.a("CmdP", new f(this), null, 0, false);
    }

    public final void f() {
        com.agilebinary.mobilemonitor.device.a.a.g k_;
        this.c.a(true, false, false);
        if (this.c.k() && (k_ = this.c.k_()) != null) {
            this.b.a(k_);
        }
    }

    public final void g() {
        this.d = new f(this);
        this.f.a("CmdP", this.d, null, 0, false);
    }

    public final synchronized void g_() {
        this.f.a("CmdP", this.d);
        long h = (long) h();
        this.d = new f(this);
        this.f.a("CmdP", this.d, null, true, h, h, true);
    }
}
