package com.agilebinary.mobilemonitor.device.android.device;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import com.agilebinary.mobilemonitor.device.a.a.b;
import com.agilebinary.mobilemonitor.device.a.b.a.a.d;
import com.agilebinary.mobilemonitor.device.a.c.g;
import com.agilebinary.mobilemonitor.device.a.g.f;
import com.agilebinary.mobilemonitor.device.android.device.a.i;
import com.agilebinary.mobilemonitor.device.android.device.a.k;
import com.agilebinary.mobilemonitor.device.android.device.a.l;
import com.agilebinary.mobilemonitor.device.android.device.a.o;
import com.agilebinary.mobilemonitor.device.android.device.receivers.c;

public final class a extends i implements g {
    private static String g = f.a();
    private l h;
    private c i;
    private com.agilebinary.mobilemonitor.device.android.device.receivers.a j;
    private com.agilebinary.mobilemonitor.device.android.device.a.c k;
    private i l;
    private k m;
    private o n;
    private PendingIntent o;

    public a(Context context, b bVar, com.agilebinary.mobilemonitor.device.a.a.c cVar, String str) {
        super(context, bVar, cVar, str);
    }

    public final void a() {
        this.n = new o(this.d, this, this.a);
        this.n.a();
        this.i = new c(this.d, this.b);
        this.j = new com.agilebinary.mobilemonitor.device.android.device.receivers.a(this.d, this.b);
        this.h = new l(this.d, this.b, this, this.a);
        super.a();
    }

    public final void a(boolean z) {
        if (z) {
            this.l = new i(this.d, this.b, this, this.a);
            this.l.a();
            return;
        }
        this.l.b();
        this.l = null;
    }

    public final void a(boolean z, String str, boolean z2, boolean z3) {
        this.n.a(z, str, z2, z3);
    }

    public final void b() {
        super.b();
        this.a.a(this.n);
        this.h.c();
        this.o = PendingIntent.getBroadcast(this.d, 0, new Intent("com.agilebinary.mobilemonitor.WATCHDOG_ACTION"), 0);
        this.f.setInexactRepeating(0, System.currentTimeMillis() + 1800000, 1800000, this.o);
    }

    public final void b(boolean z) {
        if (z) {
            this.n.b(this.a.w());
            this.n.b();
            return;
        }
        this.n.c();
    }

    public final void c() {
        this.f.cancel(this.o);
        this.a.a((com.agilebinary.mobilemonitor.device.a.b.o) null);
        this.h.f();
        super.c();
    }

    public final void c(boolean z) {
        this.h.a(z);
    }

    public final void d() {
        super.d();
        this.n.d();
    }

    public final void d(boolean z) {
        if (z) {
            this.k = new com.agilebinary.mobilemonitor.device.android.device.a.c(this.d, this.b, this, this.a);
            this.k.b();
            return;
        }
        if (this.k != null) {
            this.k.c();
        }
        this.k = null;
    }

    public final d e() {
        return this.n.e();
    }

    public final void e(boolean z) {
    }

    public final void f(boolean z) {
    }

    public final void g(boolean z) {
        if (z) {
            this.m = new k(this.d, this.b, this, this.a);
            this.m.a();
            return;
        }
        if (this.m != null) {
            this.m.b();
        }
        this.m = null;
    }

    public final void h(boolean z) {
        if (z) {
            this.i = new c(this.d, this.b);
            this.i.a();
            return;
        }
        if (this.i != null) {
            this.i.b();
        }
        this.i = null;
    }

    public final void i(boolean z) {
        if (z) {
            this.j = new com.agilebinary.mobilemonitor.device.android.device.receivers.a(this.d, this.b);
            this.j.a();
            return;
        }
        if (this.j != null) {
            this.j.b();
        }
        this.j = null;
    }
}
