package com.agilebinary.mobilemonitor.device.a.c.a;

import com.agilebinary.b.a.a.e;
import com.agilebinary.b.a.a.l;
import com.agilebinary.b.a.a.o;
import com.agilebinary.mobilemonitor.device.a.a.c;
import com.agilebinary.mobilemonitor.device.a.g.f;

public abstract class g extends k {
    private static final String b = f.a();
    private o c = new l();
    private i d = new i(this);

    public g(c cVar, com.agilebinary.mobilemonitor.device.a.c.f fVar) {
        super(cVar, fVar);
    }

    public void b() {
        super.b();
        g();
    }

    public final synchronized f e() {
        f e;
        e = super.e();
        if (e.a != 1) {
            o f = f();
            e.a(f, this.d);
            if (this.c.a() == 0 && f.a() == 0) {
                e = new f(0, b + ": no current and reference APs");
            } else {
                c cVar = (c) f.a(0);
                int c2 = this.c.c(cVar);
                if (c2 == -1) {
                    e = new f(1, b + ": strongest AP not found in ref list ");
                } else {
                    int abs = Math.abs(((c) this.c.a(c2)).a - cVar.a);
                    e = abs >= this.a.u() ? new f(1, b + ": strongest AP rssi change= " + abs) : new f(2, b + ": strongest AP rssi change= " + abs);
                }
            }
        }
        if (e.a != 2) {
            g();
        }
        "" + e;
        return e;
    }

    public abstract o f();

    public final synchronized void g() {
        super.g();
        o f = f();
        e.a(f, this.d);
        this.c = f;
    }
}
