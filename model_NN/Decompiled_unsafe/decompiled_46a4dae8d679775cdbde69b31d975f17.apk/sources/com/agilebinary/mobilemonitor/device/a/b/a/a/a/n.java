package com.agilebinary.mobilemonitor.device.a.b.a.a.a;

import com.agilebinary.mobilemonitor.device.a.b.a.a.c;
import com.agilebinary.mobilemonitor.device.a.b.a.a.d;
import com.agilebinary.mobilemonitor.device.a.b.a.a.d.a;

public final class n extends q {
    private long a;
    private int b;

    public n(a aVar) {
        super(aVar);
        this.a = aVar.f();
        this.b = aVar.e();
    }

    public n(String str, String str2, d dVar, long j, int i) {
        super(str, str2, dVar);
        this.a = j;
        this.b = i;
    }

    public final String a(c cVar) {
        return super.a(cVar) + "\nTime: " + cVar.a(this.a) + "\nKind: " + this.b;
    }

    public final void a(com.agilebinary.mobilemonitor.device.a.b.a.a.a aVar) {
        super.a(aVar);
        aVar.a(this.a);
        aVar.a(this.b);
    }

    public final byte b() {
        return 10;
    }
}
