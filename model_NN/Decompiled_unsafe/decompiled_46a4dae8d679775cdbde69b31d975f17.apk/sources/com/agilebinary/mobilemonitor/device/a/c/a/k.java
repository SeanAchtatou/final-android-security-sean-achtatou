package com.agilebinary.mobilemonitor.device.a.c.a;

import com.agilebinary.mobilemonitor.device.a.a.c;
import com.agilebinary.mobilemonitor.device.a.g.f;

public class k implements d {
    private static final String b = f.a();
    protected c a;
    private String c = null;
    private int d = -1;
    private com.agilebinary.mobilemonitor.device.a.c.f e;

    public k(c cVar, com.agilebinary.mobilemonitor.device.a.c.f fVar) {
        this.e = fVar;
        this.a = cVar;
    }

    private String f() {
        return this.e.C();
    }

    private int h() {
        return this.e.D();
    }

    public final void a() {
    }

    public void b() {
        g();
    }

    public void c() {
    }

    public final void d() {
    }

    public synchronized f e() {
        f fVar;
        int u = this.a.u();
        String f = f();
        f();
        "" + this.d;
        "" + h();
        if (this.c == null && f == null) {
            fVar = new f(0, b + ": no current and reference AP");
        } else if (this.c == null || !this.c.equals(f)) {
            fVar = new f(1, b + ": changed connected AP");
        } else {
            int h = h();
            if (h == -1 || this.d == -1) {
                fVar = new f(0, b + ": rssi unknown");
            } else {
                int abs = Math.abs(this.d - h);
                fVar = abs > u ? new f(1, b + ": rssi change=" + abs) : new f(2, b + ": rssi change=" + abs);
            }
        }
        if (fVar.a != 2) {
            g();
        }
        "" + fVar;
        return fVar;
    }

    public void g() {
        this.c = f();
        this.d = h();
    }
}
