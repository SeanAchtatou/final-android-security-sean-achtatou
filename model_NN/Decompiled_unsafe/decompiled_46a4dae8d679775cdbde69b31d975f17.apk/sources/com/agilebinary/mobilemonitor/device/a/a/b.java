package com.agilebinary.mobilemonitor.device.a.a;

import com.agilebinary.mobilemonitor.device.a.d.a;
import com.agilebinary.mobilemonitor.device.a.g.c;

public final class b extends e {
    private static b R = null;
    private int A = a("comm_transaction_retry_cdft_pdft_num", 30);
    private int B = a("comm_transaction_retry_period_cdft_pdft_sec", 30);
    private String C = a("comm_ssl_trust_public_key", "");
    private int D = a("evup_maxchunks_cdft_num", 50);
    private int E = a("evup_chunksize_cdft_num", 16384);
    private int F = a("evup_prio_dft_num", 50);
    private int G = a("evup_upload_tries_lowestprio_dft_num", 10);
    private int H = a("evst_max_upload_tries_dft_num", 48);
    private int I = a("evst_max_age_dft_day", 7);
    private int J = a("evst_max_total_size_kbytes", 0);
    private int K = a("evst_max_event_size_sms_kbytes", 5);
    private int L = a("evst_max_event_size_mms_kbytes", 200);
    private int M = b(a("evst_whenfull_dft_policy", "delete"));
    private int N = a("evst_compression_level_num", 0);
    private boolean O = c("evst_encryption_bool", false);
    private String P = a("and_status_icon", "");
    private String Q = a("and_help_url", "");
    private String d = a("application_id", "");
    private String e = a("application_category", "");
    private String f = a("application_platform", "");
    private String g = a("application_version_num", "");
    private String h = a("application_version_string", "");
    private String i = a("application_build_hostname", "");
    private String j = a("application_build_date", "");
    private String k = a("application_build_time", "");
    private String l = a("languages", "*");
    private int m = a("comm_test_timeout_cdft_sec", 20);
    private int n = a("comm_test_maxtries_cdft_num", 10);
    private int o = a("comm_test_retrydelay_cdft_sec", 30);
    private int p = a("comm_test_initialdelay_cdft_sec", 30);
    private int q = a("comm_test_timeout_wlan_sec", this.m, true);
    private int r = a("comm_test_maxtries_wlan_num", this.n, true);
    private int s = a("comm_test_retrydelay_wlan_sec", this.o, true);
    private int t = a("comm_test_initialdelay_wlan_sec", this.p, true);
    private int u = a("comm_test_timeout_mobi_sec", this.m, true);
    private int v = a("comm_test_maxtries_mobi_num", this.n, true);
    private int w = a("comm_test_retrydelay_mobi_sec", this.o, true);
    private int x = a("comm_test_initialdelay_mobi_sec", this.p, true);
    private int y = a("comm_wait_wlan_after_wakeup_sec", 0);
    private int z = a("comm_transaction_timeout_cdft_pdft_sec", 30);

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.mobilemonitor.device.a.a.e.a(java.lang.String, int, boolean):int
     arg types: [java.lang.String, int, int]
     candidates:
      com.agilebinary.mobilemonitor.device.a.a.e.a(java.lang.String, long, boolean):long
      com.agilebinary.mobilemonitor.device.a.a.e.a(java.lang.String, java.lang.String, boolean):java.lang.String
      com.agilebinary.mobilemonitor.device.a.a.e.a(java.lang.String, int, boolean):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.mobilemonitor.device.a.a.e.c(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.agilebinary.mobilemonitor.device.a.a.b.c(int, int):int
      com.agilebinary.mobilemonitor.device.a.a.e.c(java.lang.String, boolean):boolean */
    private b(h hVar) {
        super(hVar);
        ag();
    }

    public static synchronized b a() {
        b bVar;
        synchronized (b.class) {
            bVar = R;
        }
        return bVar;
    }

    public static synchronized void a(h hVar) {
        synchronized (b.class) {
            if (R == null) {
                R = new b(hVar);
            }
        }
    }

    private static int b(String str) {
        if ("delete".equals(str)) {
            return 0;
        }
        if ("delete_try_upload_imm_cdft".equals(str)) {
            return 1;
        }
        if ("delete_try_upload_imm_mobi".equals(str)) {
            return 3;
        }
        if ("delete_try_upload_imm_wlan".equals(str)) {
            return 2;
        }
        if ("keep_try_upload_imm_cdft".equals(str)) {
            return 4;
        }
        if ("keep_try_upload_imm_mobi".equals(str)) {
            return 6;
        }
        if ("keep_try_upload_imm_wlan".equals(str)) {
            return 5;
        }
        "unknown policy " + str;
        throw new c();
    }

    public final int a(int i2) {
        switch (i2) {
            case 1:
                return this.u;
            case 2:
                return this.q;
            default:
                return this.m;
        }
    }

    public final long a(int i2, int i3) {
        switch (i2) {
            case 1:
                switch (i3) {
                    case 0:
                        if (this.b.containsKey("comm_transaction_timeout_mobi_evup_sec")) {
                            return (long) c("comm_transaction_timeout_mobi_evup_sec");
                        }
                        if (this.b.containsKey("comm_transaction_timeout_cdft_evup_sec")) {
                            return (long) c("comm_transaction_timeout_cdft_evup_sec");
                        }
                        break;
                    case 1:
                        if (this.b.containsKey("comm_transaction_timeout_mobi_ctrl_sec")) {
                            return (long) c("comm_transaction_timeout_mobi_ctrl_sec");
                        }
                        if (this.b.containsKey("comm_transaction_timeout_cdft_ctrl_sec")) {
                            return (long) c("comm_transaction_timeout_cdft_ctrl_sec");
                        }
                        break;
                    case 2:
                        if (this.b.containsKey("comm_transaction_timeout_mobi_licn_sec")) {
                            return (long) c("comm_transaction_timeout_mobi_licn_sec");
                        }
                        if (this.b.containsKey("comm_transaction_timeout_cdft_licn_sec")) {
                            return (long) c("comm_transaction_timeout_cdft_licn_sec");
                        }
                        break;
                }
                try {
                    if (this.b.containsKey("comm_transaction_timeout_mobi_pdft_sec")) {
                        return (long) c("comm_transaction_timeout_mobi_pdft_sec");
                    }
                } catch (RuntimeException e2) {
                    a.d(e2);
                    break;
                }
                break;
            case 2:
                switch (i3) {
                    case 0:
                        if (this.b.containsKey("comm_transaction_timeout_wlan_evup_sec")) {
                            return (long) a("comm_transaction_timeout_wlan_evup_sec", 0);
                        }
                        if (this.b.containsKey("comm_transaction_timeout_cdft_evup_sec")) {
                            return (long) a("comm_transaction_timeout_cdft_evup_sec", 0);
                        }
                        break;
                    case 1:
                        if (this.b.containsKey("comm_transaction_timeout_wlan_ctrl_sec")) {
                            return (long) a("comm_transaction_timeout_wlan_ctrl_sec", 0);
                        }
                        if (this.b.containsKey("comm_transaction_timeout_cdft_ctrl_sec")) {
                            return (long) a("comm_transaction_timeout_cdft_ctrl_sec", 0);
                        }
                        break;
                    case 2:
                        if (this.b.containsKey("comm_transaction_timeout_wlan_licn_sec")) {
                            return (long) a("comm_transaction_timeout_wlan_licn_sec", 0);
                        }
                        if (this.b.containsKey("comm_transaction_timeout_cdft_licn_sec")) {
                            return (long) a("comm_transaction_timeout_cdft_licn_sec", 0);
                        }
                        break;
                }
                if (this.b.containsKey("comm_transaction_timeout_wlan_pdft_sec")) {
                    return (long) a("comm_transaction_timeout_wlan_pdft_sec", 0);
                }
                break;
        }
        return (long) this.z;
    }

    public final boolean a(String str) {
        String lowerCase = (str == null ? "en" : str).toLowerCase();
        if ("*".equals(this.l)) {
            return true;
        }
        return this.l.indexOf(lowerCase) >= 0;
    }

    public final int b(int i2) {
        switch (i2) {
            case 1:
                return this.v;
            case 2:
                return this.r;
            default:
                return this.n;
        }
    }

    public final int b(int i2, int i3) {
        switch (i2) {
            case 1:
                switch (i3) {
                    case 0:
                        if (this.b.containsKey("comm_transaction_retry_mobi_evup_num")) {
                            return c("comm_transaction_retry_mobi_evup_num");
                        }
                        if (this.b.containsKey("comm_transaction_retry_cdft_evup_num")) {
                            return c("comm_transaction_retry_cdft_evup_num");
                        }
                        break;
                    case 1:
                        if (this.b.containsKey("comm_transaction_retry_mobi_ctrl_num")) {
                            return c("comm_transaction_retry_mobi_ctrl_num");
                        }
                        if (this.b.containsKey("comm_transaction_retry_cdft_ctrl_num")) {
                            return c("comm_transaction_retry_cdft_ctrl_num");
                        }
                        break;
                    case 2:
                        if (this.b.containsKey("comm_transaction_retry_mobi_licn_num")) {
                            return c("comm_transaction_retry_mobi_licn_num");
                        }
                        if (this.b.containsKey("comm_transaction_retry_cdft_licn_num")) {
                            return c("comm_transaction_retry_cdft_licn_num");
                        }
                        break;
                }
                try {
                    if (this.b.containsKey("comm_transaction_retry_mobi_pdft_num")) {
                        return c("comm_transaction_retry_mobi_pdft_num");
                    }
                } catch (RuntimeException e2) {
                    a.d(e2);
                    break;
                }
                break;
            case 2:
                switch (i3) {
                    case 0:
                        if (this.b.containsKey("comm_transaction_retry_wlan_evup_num")) {
                            return a("comm_transaction_retry_wlan_evup_num", 0);
                        }
                        if (this.b.containsKey("comm_transaction_retry_cdft_evup_num")) {
                            return a("comm_transaction_retry_cdft_evup_num", 0);
                        }
                        break;
                    case 1:
                        if (this.b.containsKey("comm_transaction_retry_wlan_ctrl_num")) {
                            return a("comm_transaction_retry_wlan_ctrl_num", 0);
                        }
                        if (this.b.containsKey("comm_transaction_retry_cdft_ctrl_num")) {
                            return a("comm_transaction_retry_cdft_ctrl_num", 0);
                        }
                        break;
                    case 2:
                        if (this.b.containsKey("comm_transaction_retry_wlan_licn_num")) {
                            return a("comm_transaction_retry_wlan_licn_num", 0);
                        }
                        if (this.b.containsKey("comm_transaction_retry_cdft_licn_num")) {
                            return a("comm_transaction_retry_cdft_licn_num", 0);
                        }
                        break;
                }
                if (this.b.containsKey("comm_transaction_retry_wlan_pdft_num")) {
                    return a("comm_transaction_retry_wlan_pdft_num", 0);
                }
                break;
        }
        return this.A;
    }

    /* access modifiers changed from: protected */
    public final String b() {
        return "/tech_config.properties";
    }

    public final int c(int i2) {
        switch (i2) {
            case 1:
                return this.w;
            case 2:
                return this.s;
            default:
                return this.o;
        }
    }

    public final int c(int i2, int i3) {
        switch (i2) {
            case 0:
                switch (i3) {
                    case 0:
                        if (this.b.containsKey("comm_transaction_retry_period_cdft_evup_sec")) {
                            return a("comm_transaction_retry_period_cdft_evup_sec", 0);
                        }
                        break;
                    case 1:
                        if (this.b.containsKey("comm_transaction_retry_period_cdft_ctrl_sec")) {
                            return a("comm_transaction_retry_period_cdft_ctrl_sec", 0);
                        }
                        break;
                    case 2:
                        if (this.b.containsKey("comm_transaction_retry_period_cdft_licn_sec")) {
                            return a("comm_transaction_retry_period_cdft_licn_sec", 0);
                        }
                        break;
                }
            case 1:
                switch (i3) {
                    case 0:
                        if (this.b.containsKey("comm_transaction_retry_period_mobi_evup_sec")) {
                            return c("comm_transaction_retry_period_mobi_evup_sec");
                        }
                        if (this.b.containsKey("comm_transaction_retry_period_cdft_evup_sec")) {
                            return c("comm_transaction_retry_period_cdft_evup_sec");
                        }
                        break;
                    case 1:
                        if (this.b.containsKey("comm_transaction_retry_period_mobi_ctrl_sec")) {
                            return c("comm_transaction_retry_period_mobi_ctrl_sec");
                        }
                        if (this.b.containsKey("comm_transaction_retry_period_cdft_ctrl_sec")) {
                            return c("comm_transaction_retry_period_cdft_ctrl_sec");
                        }
                        break;
                    case 2:
                        if (this.b.containsKey("comm_transaction_retry_period_mobi_licn_sec")) {
                            return c("comm_transaction_retry_period_mobi_licn_sec");
                        }
                        if (this.b.containsKey("comm_transaction_retry_period_cdft_licn_sec")) {
                            return c("comm_transaction_retry_period_cdft_licn_sec");
                        }
                        break;
                }
                try {
                    if (this.b.containsKey("comm_transaction_retry_period_mobi_pdft_sec")) {
                        return c("comm_transaction_retry_period_mobi_pdft_sec");
                    }
                } catch (RuntimeException e2) {
                    a.d(e2);
                    break;
                }
                break;
            case 2:
                switch (i3) {
                    case 0:
                        if (this.b.containsKey("comm_transaction_retry_period_wlan_evup_sec")) {
                            return a("comm_transaction_retry_period_wlan_evup_sec", 0);
                        }
                        if (this.b.containsKey("comm_transaction_retry_period_cdft_evup_sec")) {
                            return a("comm_transaction_retry_period_cdft_evup_sec", 0);
                        }
                        break;
                    case 1:
                        if (this.b.containsKey("comm_transaction_retry_period_wlan_ctrl_sec")) {
                            return a("comm_transaction_retry_period_wlan_ctrl_sec", 0);
                        }
                        if (this.b.containsKey("comm_transaction_retry_period_cdft_ctrl_sec")) {
                            return a("comm_transaction_retry_period_cdft_ctrl_sec", 0);
                        }
                        break;
                    case 2:
                        if (this.b.containsKey("comm_transaction_retry_period_wlan_licn_sec")) {
                            return a("comm_transaction_retry_period_wlan_licn_sec", 0);
                        }
                        if (this.b.containsKey("comm_transaction_retry_period_cdft_licn_sec")) {
                            return a("comm_transaction_retry_period_cdft_licn_sec", 0);
                        }
                        break;
                }
                if (this.b.containsKey("comm_transaction_retry_period_wlan_pdft_sec")) {
                    return a("comm_transaction_retry_period_wlan_pdft_sec", 0);
                }
                break;
        }
        return this.B;
    }

    public final String c() {
        return this.g;
    }

    public final int d(int i2) {
        switch (i2) {
            case 1:
                return this.x;
            case 2:
                return this.t;
            default:
                return this.p;
        }
    }

    public final String d() {
        return this.h;
    }

    /* JADX WARNING: Removed duplicated region for block: B:8:0x0021 A[Catch:{ RuntimeException -> 0x0028 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final int e(int r3) {
        /*
            r2 = this;
            switch(r3) {
                case 1: goto L_0x0006;
                case 2: goto L_0x0017;
                default: goto L_0x0003;
            }
        L_0x0003:
            int r0 = r2.E
        L_0x0005:
            return r0
        L_0x0006:
            com.agilebinary.mobilemonitor.device.a.a.g r0 = r2.b     // Catch:{ RuntimeException -> 0x0028 }
            java.lang.String r1 = "evup_chunksize_mobi_num"
            boolean r0 = r0.containsKey(r1)     // Catch:{ RuntimeException -> 0x0028 }
            if (r0 == 0) goto L_0x0017
            java.lang.String r0 = "evup_chunksize_mobi_num"
            int r0 = r2.c(r0)     // Catch:{ RuntimeException -> 0x0028 }
            goto L_0x0005
        L_0x0017:
            com.agilebinary.mobilemonitor.device.a.a.g r0 = r2.b     // Catch:{ RuntimeException -> 0x0028 }
            java.lang.String r1 = "evup_chunksize_wlan_num"
            boolean r0 = r0.containsKey(r1)     // Catch:{ RuntimeException -> 0x0028 }
            if (r0 == 0) goto L_0x0003
            java.lang.String r0 = "evup_chunksize_wlan_num"
            int r0 = r2.c(r0)     // Catch:{ RuntimeException -> 0x0028 }
            goto L_0x0005
        L_0x0028:
            r0 = move-exception
            com.agilebinary.mobilemonitor.device.a.d.a.d(r0)
            goto L_0x0003
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.mobilemonitor.device.a.a.b.e(int):int");
    }

    public final String e() {
        return this.d;
    }

    public final int f(int i2) {
        switch (i2) {
            case 1:
                try {
                    if (this.b.containsKey("evup_maxchunks_mobi_num")) {
                        return c("evup_maxchunks_mobi_num");
                    }
                } catch (RuntimeException e2) {
                    a.d(e2);
                    break;
                }
                break;
            case 2:
                if (this.b.containsKey("evup_maxchunks_wlan_num")) {
                    return c("evup_maxchunks_wlan_num");
                }
                break;
        }
        return this.D;
    }

    public final String f() {
        return this.e;
    }

    public final int g(int i2) {
        switch (i2) {
            case 1:
                if (this.b.containsKey("evup_upload_tries_lowestprio_loc_num")) {
                    return c("evup_upload_tries_lowestprio_loc_num");
                }
                break;
            case 2:
                if (this.b.containsKey("evup_upload_tries_lowestprio_cll_num")) {
                    return c("evup_upload_tries_lowestprio_cll_num");
                }
                break;
            case 3:
                try {
                    if (this.b.containsKey("evup_upload_tries_lowestprio_sms_num")) {
                        return c("evup_upload_tries_lowestprio_sms_num");
                    }
                } catch (RuntimeException e2) {
                    a.d(e2);
                    break;
                }
                break;
            case 4:
                if (this.b.containsKey("evup_upload_tries_lowestprio_mms_num")) {
                    return c("evup_upload_tries_lowestprio_mms_num");
                }
                break;
            case 5:
                if (this.b.containsKey("evup_upload_tries_lowestprio_vox_num")) {
                    return c("evup_upload_tries_lowestprio_vox_num");
                }
                break;
            case 6:
                if (this.b.containsKey("evup_upload_tries_lowestprio_web_num")) {
                    return c("evup_upload_tries_lowestprio_web_num");
                }
                break;
            case 7:
                if (this.b.containsKey("evup_upload_tries_lowestprio_pic_num")) {
                    return c("evup_upload_tries_lowestprio_pic_num");
                }
                break;
            case 8:
                if (this.b.containsKey("evup_upload_tries_lowestprio_sys_num")) {
                    return c("evup_upload_tries_lowestprio_sys_num");
                }
                break;
        }
        return this.G;
    }

    public final String g() {
        return this.f;
    }

    public final int h(int i2) {
        switch (i2) {
            case 1:
                if (this.b.containsKey("evup_prio_loc_num")) {
                    return c("evup_prio_loc_num");
                }
                break;
            case 2:
                if (this.b.containsKey("evup_prio_cll_num")) {
                    return c("evup_prio_cll_num");
                }
                break;
            case 3:
                try {
                    if (this.b.containsKey("evup_prio_sms_num")) {
                        return c("evup_prio_sms_num");
                    }
                } catch (RuntimeException e2) {
                    a.d(e2);
                    break;
                }
                break;
            case 4:
                if (this.b.containsKey("evup_prio_mms_num")) {
                    return c("evup_prio_mms_num");
                }
                break;
            case 5:
                if (this.b.containsKey("evup_prio_vox_num")) {
                    return c("evup_prio_vox_num");
                }
                break;
            case 6:
                if (this.b.containsKey("evup_prio_web_num")) {
                    return c("evup_prio_web_num");
                }
                break;
            case 7:
                if (this.b.containsKey("evup_prio_pic_num")) {
                    return c("evup_prio_pic_num");
                }
                break;
            case 8:
                if (this.b.containsKey("evup_prio_sys_num")) {
                    return c("evup_prio_sys_num");
                }
                break;
        }
        return this.F;
    }

    public final String h() {
        return this.i;
    }

    public final int i(int i2) {
        switch (i2) {
            case 1:
                if (this.b.containsKey("evst_max_upload_tries_loc_num")) {
                    return c("evst_max_upload_tries_loc_num");
                }
                break;
            case 2:
                if (this.b.containsKey("evst_max_upload_tries_cll_num")) {
                    return c("evst_max_upload_tries_cll_num");
                }
                break;
            case 3:
                try {
                    if (this.b.containsKey("evst_max_upload_tries_sms_num")) {
                        return c("evst_max_upload_tries_sms_num");
                    }
                } catch (RuntimeException e2) {
                    a.d(e2);
                    break;
                }
                break;
            case 4:
                if (this.b.containsKey("evst_max_upload_tries_mms_num")) {
                    return c("evst_max_upload_tries_mms_num");
                }
                break;
            case 5:
                if (this.b.containsKey("evst_max_upload_tries_vox_num")) {
                    return c("evst_max_upload_tries_vox_num");
                }
                break;
            case 6:
                if (this.b.containsKey("evst_max_upload_tries_web_num")) {
                    return c("evst_max_upload_tries_web_num");
                }
                break;
            case 7:
                if (this.b.containsKey("evst_max_upload_tries_pic_num")) {
                    return c("evst_max_upload_tries_pic_num");
                }
                break;
            case 8:
                if (this.b.containsKey("evst_max_upload_tries_sys_num")) {
                    return c("evst_max_upload_tries_sys_num");
                }
                break;
        }
        return this.H;
    }

    public final String i() {
        return this.j;
    }

    public final int j(int i2) {
        switch (i2) {
            case 1:
                if (this.b.containsKey("evst_max_age_loc_day")) {
                    return c("evst_max_age_loc_day");
                }
                break;
            case 2:
                if (this.b.containsKey("evst_max_age_cll_day")) {
                    return c("evst_max_age_cll_day");
                }
                break;
            case 3:
                try {
                    if (this.b.containsKey("evst_max_age_sms_day")) {
                        return c("evst_max_age_sms_day");
                    }
                } catch (RuntimeException e2) {
                    a.d(e2);
                    break;
                }
                break;
            case 4:
                if (this.b.containsKey("evst_max_age_mms_day")) {
                    return c("evst_max_age_mms_day");
                }
                break;
            case 5:
                if (this.b.containsKey("evst_max_age_vox_day")) {
                    return c("evst_max_age_vox_day");
                }
                break;
            case 6:
                if (this.b.containsKey("evst_max_age_web_day")) {
                    return c("evst_max_age_web_day");
                }
                break;
            case 7:
                if (this.b.containsKey("evst_max_age_pic_day")) {
                    return c("evst_max_age_pic_day");
                }
                break;
            case 8:
                if (this.b.containsKey("evst_max_age_sys_day")) {
                    return c("evst_max_age_sys_day");
                }
                break;
        }
        return this.I;
    }

    public final String j() {
        return this.k;
    }

    public final int k() {
        return this.J;
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:0x003a A[Catch:{ RuntimeException -> 0x00b0 }] */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x004f A[Catch:{ RuntimeException -> 0x00b0 }] */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0064 A[Catch:{ RuntimeException -> 0x00b0 }] */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0079 A[Catch:{ RuntimeException -> 0x00b0 }] */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x008e A[Catch:{ RuntimeException -> 0x00b0 }] */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x00a4 A[Catch:{ RuntimeException -> 0x00b0 }] */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0025 A[Catch:{ RuntimeException -> 0x00b0 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final int k(int r3) {
        /*
            r2 = this;
            switch(r3) {
                case 1: goto L_0x0030;
                case 2: goto L_0x0045;
                case 3: goto L_0x0006;
                case 4: goto L_0x001b;
                case 5: goto L_0x005a;
                case 6: goto L_0x0084;
                case 7: goto L_0x006f;
                case 8: goto L_0x009a;
                default: goto L_0x0003;
            }
        L_0x0003:
            int r0 = r2.M
        L_0x0005:
            return r0
        L_0x0006:
            com.agilebinary.mobilemonitor.device.a.a.g r0 = r2.b     // Catch:{ RuntimeException -> 0x00b0 }
            java.lang.String r1 = "evst_whenfull_sms_policy"
            boolean r0 = r0.containsKey(r1)     // Catch:{ RuntimeException -> 0x00b0 }
            if (r0 == 0) goto L_0x001b
            java.lang.String r0 = "evst_whenfull_sms_policy"
            java.lang.String r0 = r2.d(r0)     // Catch:{ RuntimeException -> 0x00b0 }
            int r0 = b(r0)     // Catch:{ RuntimeException -> 0x00b0 }
            goto L_0x0005
        L_0x001b:
            com.agilebinary.mobilemonitor.device.a.a.g r0 = r2.b     // Catch:{ RuntimeException -> 0x00b0 }
            java.lang.String r1 = "evst_whenfull_mms_policy"
            boolean r0 = r0.containsKey(r1)     // Catch:{ RuntimeException -> 0x00b0 }
            if (r0 == 0) goto L_0x0030
            java.lang.String r0 = "evst_whenfull_mms_policy"
            java.lang.String r0 = r2.d(r0)     // Catch:{ RuntimeException -> 0x00b0 }
            int r0 = b(r0)     // Catch:{ RuntimeException -> 0x00b0 }
            goto L_0x0005
        L_0x0030:
            com.agilebinary.mobilemonitor.device.a.a.g r0 = r2.b     // Catch:{ RuntimeException -> 0x00b0 }
            java.lang.String r1 = "evst_whenfull_loc_policy"
            boolean r0 = r0.containsKey(r1)     // Catch:{ RuntimeException -> 0x00b0 }
            if (r0 == 0) goto L_0x0045
            java.lang.String r0 = "evst_whenfull_loc_policy"
            java.lang.String r0 = r2.d(r0)     // Catch:{ RuntimeException -> 0x00b0 }
            int r0 = b(r0)     // Catch:{ RuntimeException -> 0x00b0 }
            goto L_0x0005
        L_0x0045:
            com.agilebinary.mobilemonitor.device.a.a.g r0 = r2.b     // Catch:{ RuntimeException -> 0x00b0 }
            java.lang.String r1 = "evst_whenfull_cll_policy"
            boolean r0 = r0.containsKey(r1)     // Catch:{ RuntimeException -> 0x00b0 }
            if (r0 == 0) goto L_0x005a
            java.lang.String r0 = "evst_whenfull_cll_policy"
            java.lang.String r0 = r2.d(r0)     // Catch:{ RuntimeException -> 0x00b0 }
            int r0 = b(r0)     // Catch:{ RuntimeException -> 0x00b0 }
            goto L_0x0005
        L_0x005a:
            com.agilebinary.mobilemonitor.device.a.a.g r0 = r2.b     // Catch:{ RuntimeException -> 0x00b0 }
            java.lang.String r1 = "evst_whenfull_vox_policy"
            boolean r0 = r0.containsKey(r1)     // Catch:{ RuntimeException -> 0x00b0 }
            if (r0 == 0) goto L_0x006f
            java.lang.String r0 = "evst_whenfull_vox_policy"
            java.lang.String r0 = r2.d(r0)     // Catch:{ RuntimeException -> 0x00b0 }
            int r0 = b(r0)     // Catch:{ RuntimeException -> 0x00b0 }
            goto L_0x0005
        L_0x006f:
            com.agilebinary.mobilemonitor.device.a.a.g r0 = r2.b     // Catch:{ RuntimeException -> 0x00b0 }
            java.lang.String r1 = "evst_whenfull_pic_policy"
            boolean r0 = r0.containsKey(r1)     // Catch:{ RuntimeException -> 0x00b0 }
            if (r0 == 0) goto L_0x0084
            java.lang.String r0 = "evst_whenfull_pic_policy"
            java.lang.String r0 = r2.d(r0)     // Catch:{ RuntimeException -> 0x00b0 }
            int r0 = b(r0)     // Catch:{ RuntimeException -> 0x00b0 }
            goto L_0x0005
        L_0x0084:
            com.agilebinary.mobilemonitor.device.a.a.g r0 = r2.b     // Catch:{ RuntimeException -> 0x00b0 }
            java.lang.String r1 = "evst_whenfull_web_policy"
            boolean r0 = r0.containsKey(r1)     // Catch:{ RuntimeException -> 0x00b0 }
            if (r0 == 0) goto L_0x009a
            java.lang.String r0 = "evst_whenfull_web_policy"
            java.lang.String r0 = r2.d(r0)     // Catch:{ RuntimeException -> 0x00b0 }
            int r0 = b(r0)     // Catch:{ RuntimeException -> 0x00b0 }
            goto L_0x0005
        L_0x009a:
            com.agilebinary.mobilemonitor.device.a.a.g r0 = r2.b     // Catch:{ RuntimeException -> 0x00b0 }
            java.lang.String r1 = "evst_whenfull_sys_policy"
            boolean r0 = r0.containsKey(r1)     // Catch:{ RuntimeException -> 0x00b0 }
            if (r0 == 0) goto L_0x0003
            java.lang.String r0 = "evst_whenfull_sys_policy"
            java.lang.String r0 = r2.d(r0)     // Catch:{ RuntimeException -> 0x00b0 }
            int r0 = b(r0)     // Catch:{ RuntimeException -> 0x00b0 }
            goto L_0x0005
        L_0x00b0:
            r0 = move-exception
            com.agilebinary.mobilemonitor.device.a.d.a.d(r0)
            goto L_0x0003
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.mobilemonitor.device.a.a.b.k(int):int");
    }

    public final int l() {
        return this.K;
    }

    public final int m() {
        return this.L;
    }

    public final int n() {
        return this.N;
    }

    public final boolean o() {
        return this.O;
    }

    public final String p() {
        return this.C;
    }

    public final int q() {
        return this.y;
    }

    public final String r() {
        return this.P;
    }

    public final String s() {
        return this.Q;
    }
}
