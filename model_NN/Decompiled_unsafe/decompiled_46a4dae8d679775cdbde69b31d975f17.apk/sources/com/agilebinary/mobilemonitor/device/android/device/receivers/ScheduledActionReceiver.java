package com.agilebinary.mobilemonitor.device.android.device.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.agilebinary.mobilemonitor.device.a.g.f;
import com.agilebinary.mobilemonitor.device.android.services.BackgroundService;

public class ScheduledActionReceiver extends BroadcastReceiver {
    private static final String a = f.a();

    public void onReceive(Context context, Intent intent) {
        BackgroundService.a(context, "EXTRA_START_SCHEDULED_ACTION", intent.getData());
    }
}
