package com.agilebinary.mobilemonitor.device.android.c.b;

import android.content.Context;
import android.util.Log;
import com.agilebinary.mobilemonitor.device.a.g.f;
import com.agilebinary.mobilemonitor.device.a.j.b.b;
import com.agilebinary.mobilemonitor.device.a.j.b.c;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public final class a implements b {
    private static final String a = f.a();
    private Context b;

    public a(Context context) {
        this.b = context;
    }

    public final void a(byte[] bArr) {
        try {
            FileOutputStream openFileOutput = this.b.openFileOutput("rtconfig", 0);
            openFileOutput.write(bArr);
            openFileOutput.close();
        } catch (Exception e) {
            Log.e(a, " exception creating/writing config data to rtconfig", e);
        }
    }

    public final byte[] a() {
        try {
            FileInputStream openFileInput = this.b.openFileInput("rtconfig");
            byte[] bArr = new byte[openFileInput.available()];
            openFileInput.read(bArr);
            return bArr;
        } catch (FileNotFoundException e) {
            throw new c(e);
        } catch (IOException e2) {
            throw new c(e2);
        }
    }

    public final void b() {
        try {
            new File("rtconfig").delete();
        } catch (Exception e) {
        }
    }
}
