package com.agilebinary.mobilemonitor.device.a.g.a;

import com.agilebinary.mobilemonitor.device.a.g.f;

public final class a extends e {
    private static final String a = f.a();
    /* access modifiers changed from: private */
    public d b;
    private b c;
    private boolean d;
    private boolean e;
    /* access modifiers changed from: private */
    public com.agilebinary.mobilemonitor.device.a.c.f f;

    public a(d dVar, b bVar, boolean z, boolean z2, com.agilebinary.mobilemonitor.device.a.c.f fVar) {
        super(dVar.e());
        this.b = dVar;
        this.c = bVar;
        this.d = z;
        this.e = z2;
        this.f = fVar;
    }

    public final void a() {
        e();
        if (this.e) {
            this.c.a(new f(this, this.b), this.d);
        } else {
            this.c.a(this.b, this.d);
        }
    }

    public final Object d() {
        return this.b.d();
    }

    public final String e() {
        return super.e() + " (wrapped)";
    }
}
