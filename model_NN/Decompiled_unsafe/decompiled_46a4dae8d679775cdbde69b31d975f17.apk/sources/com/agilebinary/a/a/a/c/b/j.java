package com.agilebinary.a.a.a.c.b;

import com.agilebinary.a.a.a.j.a;
import com.agilebinary.a.a.a.j.b;
import com.agilebinary.a.a.a.j.f;

public final class j implements a, b {
    private final a a;
    private final b b;
    private final p c;
    private final String d;

    public j(a aVar, p pVar, String str) {
        this.a = aVar;
        this.b = aVar instanceof b ? (b) aVar : null;
        this.c = pVar;
        this.d = str != null ? str : "ASCII";
    }

    public final int a(com.agilebinary.a.a.a.i.b bVar) {
        int a2 = this.a.a(bVar);
        if (this.c.a() && a2 >= 0) {
            this.c.b((new String(bVar.b(), bVar.c() - a2, a2) + "\r\n").getBytes(this.d));
        }
        return a2;
    }

    public final int a(byte[] bArr, int i, int i2) {
        int a2 = this.a.a(bArr, i, i2);
        if (this.c.a() && a2 > 0) {
            this.c.b(bArr, i, a2);
        }
        return a2;
    }

    public final boolean a(int i) {
        return this.a.a(i);
    }

    public final int d() {
        int d2 = this.a.d();
        if (this.c.a() && d2 != -1) {
            this.c.b(new byte[]{(byte) d2});
        }
        return d2;
    }

    public final f e() {
        return this.a.e();
    }

    public final boolean f() {
        if (this.b != null) {
            return this.b.f();
        }
        return false;
    }
}
