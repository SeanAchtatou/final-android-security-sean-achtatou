package com.agilebinary.a.a.a.c.b;

import com.agilebinary.a.a.a.q;
import com.agilebinary.a.a.b.a.a;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.logging.Log;

public final class e {
    private final Log a = a.a(getClass());
    private final Map b = new HashMap();

    public final void a() {
        this.b.clear();
    }

    public final void a(long j) {
        long currentTimeMillis = System.currentTimeMillis() - j;
        if (this.a.isDebugEnabled()) {
            this.a.debug("Checking for connections, idle timeout: " + currentTimeMillis);
        }
        for (Map.Entry entry : this.b.entrySet()) {
            q qVar = (q) entry.getKey();
            n nVar = (n) entry.getValue();
            if (0 <= currentTimeMillis) {
                if (this.a.isDebugEnabled()) {
                    this.a.debug("Closing idle connection, connection time: " + 0L);
                }
                try {
                    qVar.k();
                } catch (IOException e) {
                    this.a.debug("I/O error closing connection", e);
                }
            }
        }
    }
}
