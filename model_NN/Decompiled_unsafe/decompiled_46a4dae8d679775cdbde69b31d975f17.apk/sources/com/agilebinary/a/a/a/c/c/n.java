package com.agilebinary.a.a.a.c.c;

import com.agilebinary.a.a.a.j.a;
import com.agilebinary.a.a.a.j.g;
import java.io.InputStream;

public final class n extends InputStream {
    private final a a;
    private boolean b = false;

    public n(a aVar) {
        if (aVar == null) {
            throw new IllegalArgumentException("Session input buffer may not be null");
        }
        this.a = aVar;
    }

    public final int available() {
        if (this.a instanceof g) {
            return ((g) this.a).d_();
        }
        return 0;
    }

    public final void close() {
        this.b = true;
    }

    public final int read() {
        if (this.b) {
            return -1;
        }
        return this.a.d();
    }

    public final int read(byte[] bArr, int i, int i2) {
        if (this.b) {
            return -1;
        }
        return this.a.a(bArr, i, i2);
    }
}
