package com.agilebinary.a.a.a.f;

import com.agilebinary.a.a.a.ab;

public final class g implements ab {
    /* JADX WARNING: Removed duplicated region for block: B:20:0x006f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(com.agilebinary.a.a.a.f r6, com.agilebinary.a.a.a.f.i r7) {
        /*
            r5 = this;
            if (r6 != 0) goto L_0x000a
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.String r2 = "HTTP request may not be null"
            r1.<init>(r2)
            throw r1
        L_0x000a:
            if (r7 != 0) goto L_0x0014
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.String r2 = "HTTP context may not be null"
            r1.<init>(r2)
            throw r1
        L_0x0014:
            com.agilebinary.a.a.a.d r1 = r6.a()
            com.agilebinary.a.a.a.a r3 = r1.b()
            com.agilebinary.a.a.a.d r1 = r6.a()
            java.lang.String r1 = r1.a()
            java.lang.String r2 = "CONNECT"
            boolean r1 = r1.equalsIgnoreCase(r2)
            if (r1 == 0) goto L_0x0035
            com.agilebinary.a.a.a.aa r1 = com.agilebinary.a.a.a.aa.c
            boolean r1 = r3.a(r1)
            if (r1 == 0) goto L_0x0035
        L_0x0034:
            return
        L_0x0035:
            java.lang.String r1 = "Host"
            boolean r1 = r6.a(r1)
            if (r1 != 0) goto L_0x0034
            java.lang.String r1 = "http.target_host"
            java.lang.Object r5 = r7.a(r1)
            com.agilebinary.a.a.a.b r5 = (com.agilebinary.a.a.a.b) r5
            if (r5 != 0) goto L_0x007f
            java.lang.String r1 = "http.connection"
            java.lang.Object r1 = r7.a(r1)
            com.agilebinary.a.a.a.q r1 = (com.agilebinary.a.a.a.q) r1
            boolean r2 = r1 instanceof com.agilebinary.a.a.a.ae
            if (r2 == 0) goto L_0x008a
            r0 = r1
            com.agilebinary.a.a.a.ae r0 = (com.agilebinary.a.a.a.ae) r0
            r2 = r0
            java.net.InetAddress r2 = r2.p()
            com.agilebinary.a.a.a.ae r1 = (com.agilebinary.a.a.a.ae) r1
            int r1 = r1.q()
            if (r2 == 0) goto L_0x008a
            com.agilebinary.a.a.a.b r4 = new com.agilebinary.a.a.a.b
            java.lang.String r2 = r2.getHostName()
            r4.<init>(r2, r1)
            r1 = r4
        L_0x006d:
            if (r1 != 0) goto L_0x0080
            com.agilebinary.a.a.a.aa r1 = com.agilebinary.a.a.a.aa.c
            boolean r1 = r3.a(r1)
            if (r1 != 0) goto L_0x0034
            com.agilebinary.a.a.a.l r1 = new com.agilebinary.a.a.a.l
            java.lang.String r2 = "Target host missing"
            r1.<init>(r2)
            throw r1
        L_0x007f:
            r1 = r5
        L_0x0080:
            java.lang.String r2 = "Host"
            java.lang.String r1 = r1.d()
            r6.a(r2, r1)
            goto L_0x0034
        L_0x008a:
            r1 = r5
            goto L_0x006d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.a.a.a.f.g.a(com.agilebinary.a.a.a.f, com.agilebinary.a.a.a.f.i):void");
    }
}
