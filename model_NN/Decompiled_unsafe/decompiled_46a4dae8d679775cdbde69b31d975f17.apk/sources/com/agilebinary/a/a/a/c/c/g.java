package com.agilebinary.a.a.a.c.c;

import com.agilebinary.a.a.a.j.e;
import java.io.IOException;
import java.io.OutputStream;

public final class g extends OutputStream {
    private final e a;
    private byte[] b;
    private int c;
    private boolean d;
    private boolean e;

    public g(e eVar) {
        this(eVar, 2048);
    }

    private g(e eVar, int i) {
        this.c = 0;
        this.d = false;
        this.e = false;
        this.b = new byte[2048];
        this.a = eVar;
    }

    private void a() {
        if (this.c > 0) {
            this.a.a(Integer.toHexString(this.c));
            this.a.a(this.b, 0, this.c);
            this.a.a("");
            this.c = 0;
        }
    }

    public final void close() {
        if (!this.e) {
            this.e = true;
            if (!this.d) {
                a();
                this.a.a("0");
                this.a.a("");
                this.d = true;
            }
            this.a.a();
        }
    }

    public final void flush() {
        a();
        this.a.a();
    }

    public final void write(int i) {
        if (this.e) {
            throw new IOException("Attempted write to closed stream.");
        }
        this.b[this.c] = (byte) i;
        this.c++;
        if (this.c == this.b.length) {
            a();
        }
    }

    public final void write(byte[] bArr) {
        write(bArr, 0, bArr.length);
    }

    public final void write(byte[] bArr, int i, int i2) {
        if (this.e) {
            throw new IOException("Attempted write to closed stream.");
        } else if (i2 >= this.b.length - this.c) {
            this.a.a(Integer.toHexString(this.c + i2));
            this.a.a(this.b, 0, this.c);
            this.a.a(bArr, i, i2);
            this.a.a("");
            this.c = 0;
        } else {
            System.arraycopy(bArr, i, this.b, this.c, i2);
            this.c += i2;
        }
    }
}
