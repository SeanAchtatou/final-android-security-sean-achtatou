package com.agilebinary.a.a.a.c.b.a;

import com.agilebinary.a.a.a.h.a.b;
import com.agilebinary.a.a.b.a.a;
import java.util.LinkedList;
import java.util.Queue;
import org.apache.commons.logging.Log;

public class c {
    private final Log a;
    private com.agilebinary.a.a.a.h.c.c b;
    private int c;
    private b d;
    private LinkedList e;
    private Queue f;
    private int g;

    private c() {
    }

    public c(com.agilebinary.a.a.a.h.c.c cVar, b bVar) {
        this.a = a.a(getClass());
        this.b = cVar;
        this.d = bVar;
        this.c = bVar.a(cVar);
        this.e = new LinkedList();
        this.f = new LinkedList();
        this.g = 0;
    }

    public static boolean a(char c2) {
        return c2 == ' ' || c2 == 9 || c2 == 13 || c2 == 10;
    }

    /* JADX WARNING: Removed duplicated region for block: B:5:0x001a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.agilebinary.a.a.a.c.b.a.g a(java.lang.Object r5) {
        /*
            r4 = this;
            java.util.LinkedList r0 = r4.e
            boolean r0 = r0.isEmpty()
            if (r0 != 0) goto L_0x0034
            java.util.LinkedList r0 = r4.e
            java.util.LinkedList r1 = r4.e
            int r1 = r1.size()
            java.util.ListIterator r1 = r0.listIterator(r1)
        L_0x0014:
            boolean r0 = r1.hasPrevious()
            if (r0 == 0) goto L_0x0034
            java.lang.Object r0 = r1.previous()
            com.agilebinary.a.a.a.c.b.a.g r0 = (com.agilebinary.a.a.a.c.b.a.g) r0
            java.lang.Object r2 = r0.a()
            if (r2 == 0) goto L_0x0030
            java.lang.Object r2 = r0.a()
            boolean r2 = com.agilebinary.a.a.a.c.e.c.a(r5, r2)
            if (r2 == 0) goto L_0x0014
        L_0x0030:
            r1.remove()
        L_0x0033:
            return r0
        L_0x0034:
            int r0 = r4.d()
            if (r0 != 0) goto L_0x005e
            java.util.LinkedList r0 = r4.e
            boolean r0 = r0.isEmpty()
            if (r0 != 0) goto L_0x005e
            java.util.LinkedList r0 = r4.e
            java.lang.Object r0 = r0.remove()
            com.agilebinary.a.a.a.c.b.a.g r0 = (com.agilebinary.a.a.a.c.b.a.g) r0
            r0.b()
            com.agilebinary.a.a.a.h.g r1 = r0.c()
            r1.k()     // Catch:{ IOException -> 0x0055 }
            goto L_0x0033
        L_0x0055:
            r1 = move-exception
            org.apache.commons.logging.Log r2 = r4.a
            java.lang.String r3 = "I/O error closing connection"
            r2.debug(r3, r1)
            goto L_0x0033
        L_0x005e:
            r0 = 0
            goto L_0x0033
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.a.a.a.c.b.a.c.a(java.lang.Object):com.agilebinary.a.a.a.c.b.a.g");
    }

    public final com.agilebinary.a.a.a.h.c.c a() {
        return this.b;
    }

    public final void a(g gVar) {
        if (this.g <= 0) {
            throw new IllegalStateException("No entry created for this pool. " + this.b);
        } else if (this.g <= this.e.size()) {
            throw new IllegalStateException("No entry allocated from this pool. " + this.b);
        } else {
            this.e.add(gVar);
        }
    }

    public final void a(com.agilebinary.a.a.a.d.a.b bVar) {
        if (bVar == null) {
            throw new IllegalArgumentException("Waiting thread must not be null.");
        }
        this.f.add(bVar);
    }

    public final int b() {
        return this.c;
    }

    public final void b(g gVar) {
        if (!this.b.equals(gVar.d())) {
            throw new IllegalArgumentException("Entry not planned for this pool.\npool: " + this.b + "\nplan: " + gVar.d());
        }
        this.g++;
    }

    public final void b(com.agilebinary.a.a.a.d.a.b bVar) {
        if (bVar != null) {
            this.f.remove(bVar);
        }
    }

    public final boolean c() {
        return this.g <= 0 && this.f.isEmpty();
    }

    public final boolean c(g gVar) {
        boolean remove = this.e.remove(gVar);
        if (remove) {
            this.g--;
        }
        return remove;
    }

    public final int d() {
        return this.d.a(this.b) - this.g;
    }

    public final void e() {
        if (this.g <= 0) {
            throw new IllegalStateException("There is no entry that could be dropped.");
        }
        this.g--;
    }

    public final boolean f() {
        return !this.f.isEmpty();
    }

    public final com.agilebinary.a.a.a.d.a.b g() {
        return (com.agilebinary.a.a.a.d.a.b) this.f.peek();
    }
}
