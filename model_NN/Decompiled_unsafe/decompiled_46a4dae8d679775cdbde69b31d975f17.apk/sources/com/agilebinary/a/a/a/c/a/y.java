package com.agilebinary.a.a.a.c.a;

import com.agilebinary.a.a.a.k.b;
import com.agilebinary.a.a.a.k.c;
import com.agilebinary.a.a.a.k.e;
import com.agilebinary.a.a.a.k.f;
import com.agilebinary.a.a.a.k.i;
import com.agilebinary.a.a.a.k.k;
import com.agilebinary.a.a.a.k.m;
import java.util.Locale;

public final class y implements k {
    private static boolean a(String str, String str2) {
        return str.equals(str2) || (str2.startsWith(".") && str.endsWith(str2));
    }

    public final void a(b bVar, String str) {
        if (bVar == null) {
            throw new IllegalArgumentException("Cookie may not be null");
        } else if (str == null) {
            throw new e("Missing value for domain attribute");
        } else if (str.trim().length() == 0) {
            throw new e("Blank value for domain attribute");
        } else {
            String lowerCase = str.toLowerCase(Locale.ENGLISH);
            if (!lowerCase.startsWith(".")) {
                lowerCase = '.' + lowerCase;
            }
            bVar.a(lowerCase);
        }
    }

    public final void a(c cVar, f fVar) {
        if (cVar == null) {
            throw new IllegalArgumentException("Cookie may not be null");
        } else if (fVar == null) {
            throw new IllegalArgumentException("Cookie origin may not be null");
        } else {
            String lowerCase = fVar.a().toLowerCase(Locale.ENGLISH);
            if (cVar.c() == null) {
                throw new i("Invalid cookie state: domain not specified");
            }
            String lowerCase2 = cVar.c().toLowerCase(Locale.ENGLISH);
            if (!(cVar instanceof m) || !((m) cVar).d("domain")) {
                if (!cVar.c().equals(lowerCase)) {
                    throw new i("Illegal domain attribute: \"" + cVar.c() + "\"." + "Domain of origin: \"" + lowerCase + "\"");
                }
            } else if (!lowerCase2.startsWith(".")) {
                throw new i("Domain attribute \"" + cVar.c() + "\" violates RFC 2109: domain must start with a dot");
            } else {
                int indexOf = lowerCase2.indexOf(46, 1);
                if ((indexOf < 0 || indexOf == lowerCase2.length() - 1) && !lowerCase2.equals(".local")) {
                    throw new i("Domain attribute \"" + cVar.c() + "\" violates RFC 2965: the value contains no embedded dots " + "and the value is not .local");
                } else if (!a(lowerCase, lowerCase2)) {
                    throw new i("Domain attribute \"" + cVar.c() + "\" violates RFC 2965: effective host name does not " + "domain-match domain attribute.");
                } else if (lowerCase.substring(0, lowerCase.length() - lowerCase2.length()).indexOf(46) != -1) {
                    throw new i("Domain attribute \"" + cVar.c() + "\" violates RFC 2965: " + "effective host minus domain may not contain any dots");
                }
            }
        }
    }

    public final boolean b(c cVar, f fVar) {
        if (cVar == null) {
            throw new IllegalArgumentException("Cookie may not be null");
        } else if (fVar == null) {
            throw new IllegalArgumentException("Cookie origin may not be null");
        } else {
            String lowerCase = fVar.a().toLowerCase(Locale.ENGLISH);
            String c = cVar.c();
            if (!a(lowerCase, c)) {
                return false;
            }
            return lowerCase.substring(0, lowerCase.length() - c.length()).indexOf(46) == -1;
        }
    }
}
