package com.agilebinary.a.a.a.c.b;

import com.agilebinary.a.a.a.f.i;
import com.agilebinary.a.a.a.h.b.f;
import com.agilebinary.a.a.a.h.b.h;
import com.agilebinary.a.a.a.h.g;
import com.agilebinary.a.a.a.h.l;
import com.agilebinary.a.a.b.a.a;
import java.net.ConnectException;
import java.net.Socket;
import org.apache.commons.logging.Log;

public class b implements l {
    private final Log a;
    private h b;

    public b() {
    }

    public b(h hVar) {
        this.a = a.a(getClass());
        if (hVar == null) {
            throw new IllegalArgumentException("Scheme registry amy not be null");
        }
        this.b = hVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.a.a.a.e.b.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.agilebinary.a.a.a.e.b.a(java.lang.String, int):int
      com.agilebinary.a.a.a.e.b.a(java.lang.String, java.lang.Object):com.agilebinary.a.a.a.e.b
      com.agilebinary.a.a.a.e.b.a(java.lang.String, boolean):boolean */
    private static void a(Socket socket, com.agilebinary.a.a.a.e.b bVar) {
        if (bVar == null) {
            throw new IllegalArgumentException("HTTP parameters may not be null");
        }
        socket.setTcpNoDelay(bVar.a("http.tcp.nodelay", true));
        socket.setSoTimeout(com.agilebinary.a.a.a.c.e.a.a(bVar));
        if (bVar == null) {
            throw new IllegalArgumentException("HTTP parameters may not be null");
        }
        int a2 = bVar.a("http.socket.linger", -1);
        if (a2 >= 0) {
            socket.setSoLinger(a2 > 0, a2);
        }
    }

    public final g a() {
        return new h();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.a.a.a.h.b.f.a(java.net.Socket, java.lang.String, int, boolean):java.net.Socket
     arg types: [java.net.Socket, java.lang.String, int, int]
     candidates:
      com.agilebinary.a.a.a.h.b.i.a(java.net.Socket, java.net.InetSocketAddress, java.net.InetSocketAddress, com.agilebinary.a.a.a.e.b):java.net.Socket
      com.agilebinary.a.a.a.h.b.f.a(java.net.Socket, java.lang.String, int, boolean):java.net.Socket */
    public final void a(g gVar, com.agilebinary.a.a.a.b bVar, i iVar, com.agilebinary.a.a.a.e.b bVar2) {
        if (gVar == null) {
            throw new IllegalArgumentException("Connection may not be null");
        } else if (bVar == null) {
            throw new IllegalArgumentException("Target host may not be null");
        } else if (bVar2 == null) {
            throw new IllegalArgumentException("Parameters may not be null");
        } else if (!gVar.l()) {
            throw new IllegalStateException("Connection must be open");
        } else {
            com.agilebinary.a.a.a.h.b.g a2 = this.b.a(bVar.c());
            if (!(a2.b() instanceof f)) {
                throw new IllegalArgumentException("Target scheme (" + a2.c() + ") must have layered socket factory.");
            }
            f fVar = (f) a2.b();
            try {
                Socket a3 = fVar.a(gVar.i_(), bVar.a(), bVar.b(), true);
                a(a3, bVar2);
                gVar.a(a3, bVar, fVar.a(a3), bVar2);
            } catch (ConnectException e) {
                throw new com.agilebinary.a.a.a.h.f(bVar, e);
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:41:0x00e2  */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x0109 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(com.agilebinary.a.a.a.h.g r17, com.agilebinary.a.a.a.b r18, java.net.InetAddress r19, com.agilebinary.a.a.a.f.i r20, com.agilebinary.a.a.a.e.b r21) {
        /*
            r16 = this;
            if (r17 != 0) goto L_0x000a
            java.lang.IllegalArgumentException r5 = new java.lang.IllegalArgumentException
            java.lang.String r6 = "Connection may not be null"
            r5.<init>(r6)
            throw r5
        L_0x000a:
            if (r18 != 0) goto L_0x0014
            java.lang.IllegalArgumentException r5 = new java.lang.IllegalArgumentException
            java.lang.String r6 = "Target host may not be null"
            r5.<init>(r6)
            throw r5
        L_0x0014:
            if (r21 != 0) goto L_0x001e
            java.lang.IllegalArgumentException r5 = new java.lang.IllegalArgumentException
            java.lang.String r6 = "Parameters may not be null"
            r5.<init>(r6)
            throw r5
        L_0x001e:
            boolean r5 = r17.l()
            if (r5 == 0) goto L_0x002c
            java.lang.IllegalStateException r5 = new java.lang.IllegalStateException
            java.lang.String r6 = "Connection must not be open"
            r5.<init>(r6)
            throw r5
        L_0x002c:
            r0 = r16
            com.agilebinary.a.a.a.h.b.h r0 = r0.b
            r5 = r0
            java.lang.String r6 = r18.c()
            com.agilebinary.a.a.a.h.b.g r5 = r5.a(r6)
            com.agilebinary.a.a.a.h.b.i r6 = r5.b()
            java.lang.String r7 = r18.a()
            java.net.InetAddress[] r7 = java.net.InetAddress.getAllByName(r7)
            int r8 = r18.b()
            int r5 = r5.a(r8)
            r8 = 0
        L_0x004e:
            int r9 = r7.length
            if (r8 >= r9) goto L_0x00c3
            r9 = r7[r8]
            int r10 = r7.length
            r11 = 1
            int r10 = r10 - r11
            if (r8 != r10) goto L_0x00c4
            r10 = 1
        L_0x0059:
            java.net.Socket r11 = r6.e_()
            r0 = r17
            r1 = r11
            r2 = r18
            r0.a(r1, r2)
            java.net.InetSocketAddress r12 = new java.net.InetSocketAddress
            r12.<init>(r9, r5)
            r9 = 0
            if (r19 == 0) goto L_0x0077
            java.net.InetSocketAddress r9 = new java.net.InetSocketAddress
            r13 = 0
            r0 = r9
            r1 = r19
            r2 = r13
            r0.<init>(r1, r2)
        L_0x0077:
            r0 = r16
            org.apache.commons.logging.Log r0 = r0.a
            r13 = r0
            boolean r13 = r13.isDebugEnabled()
            if (r13 == 0) goto L_0x009d
            r0 = r16
            org.apache.commons.logging.Log r0 = r0.a
            r13 = r0
            java.lang.StringBuilder r14 = new java.lang.StringBuilder
            r14.<init>()
            java.lang.String r15 = "Connecting to "
            java.lang.StringBuilder r14 = r14.append(r15)
            java.lang.StringBuilder r14 = r14.append(r12)
            java.lang.String r14 = r14.toString()
            r13.debug(r14)
        L_0x009d:
            r0 = r6
            r1 = r11
            r2 = r12
            r3 = r9
            r4 = r21
            java.net.Socket r9 = r0.a(r1, r2, r3, r4)     // Catch:{ ConnectException -> 0x00c6, h -> 0x00d3 }
            if (r11 == r9) goto L_0x010d
            r0 = r17
            r1 = r9
            r2 = r18
            r0.a(r1, r2)     // Catch:{ ConnectException -> 0x00c6, h -> 0x00d3 }
        L_0x00b1:
            r0 = r9
            r1 = r21
            a(r0, r1)     // Catch:{ ConnectException -> 0x00c6, h -> 0x00d3 }
            boolean r9 = r6.a(r9)     // Catch:{ ConnectException -> 0x00c6, h -> 0x00d3 }
            r0 = r17
            r1 = r9
            r2 = r21
            r0.a(r1, r2)     // Catch:{ ConnectException -> 0x00c6, h -> 0x00d3 }
        L_0x00c3:
            return
        L_0x00c4:
            r10 = 0
            goto L_0x0059
        L_0x00c6:
            r9 = move-exception
            if (r10 == 0) goto L_0x00d7
            com.agilebinary.a.a.a.h.f r5 = new com.agilebinary.a.a.a.h.f
            r0 = r5
            r1 = r18
            r2 = r9
            r0.<init>(r1, r2)
            throw r5
        L_0x00d3:
            r9 = move-exception
            if (r10 == 0) goto L_0x00d7
            throw r9
        L_0x00d7:
            r0 = r16
            org.apache.commons.logging.Log r0 = r0.a
            r9 = r0
            boolean r9 = r9.isDebugEnabled()
            if (r9 == 0) goto L_0x0109
            r0 = r16
            org.apache.commons.logging.Log r0 = r0.a
            r9 = r0
            java.lang.StringBuilder r10 = new java.lang.StringBuilder
            r10.<init>()
            java.lang.String r11 = "Connect to "
            java.lang.StringBuilder r10 = r10.append(r11)
            java.lang.StringBuilder r10 = r10.append(r12)
            java.lang.String r11 = " timed out. "
            java.lang.StringBuilder r10 = r10.append(r11)
            java.lang.String r11 = "Connection will be retried using another IP address"
            java.lang.StringBuilder r10 = r10.append(r11)
            java.lang.String r10 = r10.toString()
            r9.debug(r10)
        L_0x0109:
            int r8 = r8 + 1
            goto L_0x004e
        L_0x010d:
            r9 = r11
            goto L_0x00b1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.a.a.a.c.b.b.a(com.agilebinary.a.a.a.h.g, com.agilebinary.a.a.a.b, java.net.InetAddress, com.agilebinary.a.a.a.f.i, com.agilebinary.a.a.a.e.b):void");
    }
}
