package com.agilebinary.a.a.a.c.c;

import com.agilebinary.a.a.a.i.d;
import com.agilebinary.a.a.a.j.a;
import com.agilebinary.a.a.a.j.f;
import com.agilebinary.a.a.a.j.g;
import java.io.IOException;
import java.io.InputStream;

public abstract class b implements a, g {
    private InputStream a;
    private byte[] b;
    private int c;
    private int d;
    private d e = null;
    private String f = "US-ASCII";
    private boolean g = true;
    private int h = -1;
    private int i = 512;
    private f j;

    private int f() {
        for (int i2 = this.c; i2 < this.d; i2++) {
            if (this.b[i2] == 10) {
                return i2;
            }
        }
        return -1;
    }

    public final int a(com.agilebinary.a.a.a.i.b bVar) {
        if (bVar == null) {
            throw new IllegalArgumentException("Char array buffer may not be null");
        }
        boolean z = true;
        int i2 = 0;
        while (z) {
            int f2 = f();
            if (f2 == -1) {
                if (c()) {
                    this.e.a(this.b, this.c, this.d - this.c);
                    this.c = this.d;
                }
                i2 = b();
                if (i2 == -1) {
                    z = false;
                }
            } else if (this.e.f()) {
                int i3 = this.c;
                this.c = f2 + 1;
                int i4 = ((f2 <= 0 || this.b[f2 - 1] != 13) ? f2 : f2 - 1) - i3;
                if (this.g) {
                    bVar.a(this.b, i3, i4);
                    return i4;
                }
                String str = new String(this.b, i3, i4, this.f);
                bVar.a(str);
                return str.length();
            } else {
                this.e.a(this.b, this.c, (f2 + 1) - this.c);
                this.c = f2 + 1;
                z = false;
            }
            if (this.h > 0 && this.e.d() >= this.h) {
                throw new IOException("Maximum line length limit exceeded");
            }
        }
        if (i2 == -1 && this.e.f()) {
            return -1;
        }
        int d2 = this.e.d();
        if (d2 > 0) {
            if (this.e.b(d2 - 1) == 10) {
                d2--;
                this.e.c(d2);
            }
            if (d2 > 0 && this.e.b(d2 - 1) == 13) {
                this.e.c(d2 - 1);
            }
        }
        int d3 = this.e.d();
        if (this.g) {
            d dVar = this.e;
            if (dVar != null) {
                bVar.a(dVar.e(), 0, d3);
            }
        } else {
            String str2 = new String(this.e.e(), 0, d3, this.f);
            d3 = str2.length();
            bVar.a(str2);
        }
        this.e.a();
        return d3;
    }

    public final int a(byte[] bArr, int i2, int i3) {
        if (bArr == null) {
            return 0;
        }
        if (c()) {
            int min = Math.min(i3, this.d - this.c);
            System.arraycopy(this.b, this.c, bArr, i2, min);
            this.c += min;
            return min;
        } else if (i3 > this.i) {
            return this.a.read(bArr, i2, i3);
        } else {
            while (!c()) {
                if (b() == -1) {
                    return -1;
                }
            }
            int min2 = Math.min(i3, this.d - this.c);
            System.arraycopy(this.b, this.c, bArr, i2, min2);
            this.c += min2;
            return min2;
        }
    }

    /* access modifiers changed from: protected */
    public final void a(InputStream inputStream, int i2, com.agilebinary.a.a.a.e.b bVar) {
        if (inputStream == null) {
            throw new IllegalArgumentException("Input stream may not be null");
        } else if (i2 <= 0) {
            throw new IllegalArgumentException("Buffer size may not be negative or zero");
        } else if (bVar == null) {
            throw new IllegalArgumentException("HTTP parameters may not be null");
        } else {
            this.a = inputStream;
            this.b = new byte[i2];
            this.c = 0;
            this.d = 0;
            this.e = new d(i2);
            this.f = com.agilebinary.mobilemonitor.device.a.b.a.a.a.a(bVar);
            this.g = this.f.equalsIgnoreCase("US-ASCII") || this.f.equalsIgnoreCase("ASCII");
            this.h = bVar.a("http.connection.max-line-length", -1);
            this.i = bVar.a("http.connection.min-chunk-limit", 512);
            this.j = new f();
        }
    }

    /* access modifiers changed from: protected */
    public int b() {
        if (this.c > 0) {
            int i2 = this.d - this.c;
            if (i2 > 0) {
                System.arraycopy(this.b, this.c, this.b, 0, i2);
            }
            this.c = 0;
            this.d = i2;
        }
        int i3 = this.d;
        int read = this.a.read(this.b, i3, this.b.length - i3);
        if (read == -1) {
            return -1;
        }
        this.d = i3 + read;
        this.j.a((long) read);
        return read;
    }

    /* access modifiers changed from: protected */
    public final boolean c() {
        return this.c < this.d;
    }

    public final int d() {
        while (!c()) {
            if (b() == -1) {
                return -1;
            }
        }
        byte[] bArr = this.b;
        int i2 = this.c;
        this.c = i2 + 1;
        return bArr[i2] & 255;
    }

    public final int d_() {
        return this.d - this.c;
    }

    public final f e() {
        return this.j;
    }
}
