package com.agilebinary.a.a.a.k;

import java.io.Serializable;
import java.util.Comparator;

public final class l implements Serializable, Comparator {
    private static String a(c cVar) {
        String d = cVar.d();
        if (d == null) {
            d = "/";
        }
        return !d.endsWith("/") ? d + '/' : d;
    }

    public final /* bridge */ /* synthetic */ int compare(Object obj, Object obj2) {
        String a = a((c) obj);
        String a2 = a((c) obj2);
        if (!a.equals(a2)) {
            if (a.startsWith(a2)) {
                return -1;
            }
            if (a2.startsWith(a)) {
                return 1;
            }
        }
        return 0;
    }
}
