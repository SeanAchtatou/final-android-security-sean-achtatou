package com.agilebinary.a.a.a.c.b.a;

import com.agilebinary.a.a.a.c.b.a;
import com.agilebinary.a.a.a.h.c.c;
import com.agilebinary.a.a.a.h.l;
import java.util.concurrent.TimeUnit;

public final class g extends a {
    private final long d;
    private long e;
    private long f;
    private long g;

    public g(l lVar, c cVar, long j, TimeUnit timeUnit) {
        super(lVar, cVar);
        if (cVar == null) {
            throw new IllegalArgumentException("HTTP route may not be null");
        }
        this.d = System.currentTimeMillis();
        if (j > 0) {
            this.f = this.d + timeUnit.toMillis(j);
        } else {
            this.f = Long.MAX_VALUE;
        }
        this.g = this.f;
    }

    public final void a(long j, TimeUnit timeUnit) {
        this.e = System.currentTimeMillis();
        this.g = Math.min(this.f, j > 0 ? this.e + timeUnit.toMillis(j) : Long.MAX_VALUE);
    }

    public final boolean a(long j) {
        return j >= this.g;
    }

    /* access modifiers changed from: protected */
    public final void b() {
        super.b();
    }

    /* access modifiers changed from: protected */
    public final com.agilebinary.a.a.a.h.g c() {
        return this.a;
    }

    /* access modifiers changed from: protected */
    public final c d() {
        return this.b;
    }

    public final long e() {
        return this.e;
    }
}
