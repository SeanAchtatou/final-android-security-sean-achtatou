package com.agilebinary.a.a.a.c.b.a;

import com.agilebinary.a.a.a.h.a.c;
import com.agilebinary.a.a.a.h.b.h;
import com.agilebinary.a.a.a.h.j;
import com.agilebinary.a.a.a.h.l;
import com.agilebinary.a.a.b.a.a;
import java.util.concurrent.TimeUnit;
import org.apache.commons.logging.Log;

public final class b implements j {
    /* access modifiers changed from: private */
    public final Log a;
    private h b;
    private a c;
    private l d;
    private c e;

    public b(h hVar, long j, TimeUnit timeUnit) {
        if (hVar == null) {
            throw new IllegalArgumentException("Scheme registry may not be null");
        }
        this.a = a.a(getClass());
        this.b = hVar;
        this.e = new c();
        this.d = new com.agilebinary.a.a.a.c.b.b(hVar);
        this.c = new a(this.d, this.e, 20, 5, timeUnit);
    }

    public final h a() {
        return this.b;
    }

    public final com.agilebinary.a.a.a.h.b a(com.agilebinary.a.a.a.h.c.c cVar, Object obj) {
        return new h(this, new e(this.c, new f(), cVar, obj), cVar);
    }

    public final void a(int i) {
        this.c.a(20);
    }

    public final void a(long j, TimeUnit timeUnit) {
        if (this.a.isDebugEnabled()) {
            this.a.debug("Closing connections idle longer than " + j + " " + timeUnit);
        }
        this.c.a(j, timeUnit);
    }

    /* JADX WARNING: Unknown top exception splitter block from list: {B:39:0x0078=Splitter:B:39:0x0078, B:21:0x003c=Splitter:B:21:0x003c} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(com.agilebinary.a.a.a.h.a r8, long r9, java.util.concurrent.TimeUnit r11) {
        /*
            r7 = this;
            boolean r0 = r8 instanceof com.agilebinary.a.a.a.c.b.a.d
            if (r0 != 0) goto L_0x000c
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "Connection class mismatch, connection not obtained from this manager."
            r0.<init>(r1)
            throw r0
        L_0x000c:
            com.agilebinary.a.a.a.c.b.a.d r8 = (com.agilebinary.a.a.a.c.b.a.d) r8
            com.agilebinary.a.a.a.c.b.a r0 = r8.i()
            if (r0 == 0) goto L_0x0022
            com.agilebinary.a.a.a.h.j r0 = r8.h()
            if (r0 == r7) goto L_0x0022
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "Connection not obtained from this manager."
            r0.<init>(r1)
            throw r0
        L_0x0022:
            monitor-enter(r8)
            com.agilebinary.a.a.a.c.b.a r1 = r8.i()     // Catch:{ all -> 0x005d }
            com.agilebinary.a.a.a.c.b.a.g r1 = (com.agilebinary.a.a.a.c.b.a.g) r1     // Catch:{ all -> 0x005d }
            if (r1 != 0) goto L_0x002d
            monitor-exit(r8)     // Catch:{ all -> 0x005d }
        L_0x002c:
            return
        L_0x002d:
            boolean r0 = r8.l()     // Catch:{ IOException -> 0x0068 }
            if (r0 == 0) goto L_0x003c
            boolean r0 = r8.r()     // Catch:{ IOException -> 0x0068 }
            if (r0 != 0) goto L_0x003c
            r8.m()     // Catch:{ IOException -> 0x0068 }
        L_0x003c:
            boolean r2 = r8.r()     // Catch:{ all -> 0x005d }
            org.apache.commons.logging.Log r0 = r7.a     // Catch:{ all -> 0x005d }
            boolean r0 = r0.isDebugEnabled()     // Catch:{ all -> 0x005d }
            if (r0 == 0) goto L_0x0051
            if (r2 == 0) goto L_0x0060
            org.apache.commons.logging.Log r0 = r7.a     // Catch:{ all -> 0x005d }
            java.lang.String r3 = "Released connection is reusable."
            r0.debug(r3)     // Catch:{ all -> 0x005d }
        L_0x0051:
            r8.j()     // Catch:{ all -> 0x005d }
            com.agilebinary.a.a.a.c.b.a.a r0 = r7.c     // Catch:{ all -> 0x005d }
            r3 = r9
            r5 = r11
            r0.a(r1, r2, r3, r5)     // Catch:{ all -> 0x005d }
        L_0x005b:
            monitor-exit(r8)     // Catch:{ all -> 0x005d }
            goto L_0x002c
        L_0x005d:
            r0 = move-exception
            monitor-exit(r8)     // Catch:{ all -> 0x005d }
            throw r0
        L_0x0060:
            org.apache.commons.logging.Log r0 = r7.a     // Catch:{ all -> 0x005d }
            java.lang.String r3 = "Released connection is not reusable."
            r0.debug(r3)     // Catch:{ all -> 0x005d }
            goto L_0x0051
        L_0x0068:
            r0 = move-exception
            org.apache.commons.logging.Log r2 = r7.a     // Catch:{ all -> 0x00a0 }
            boolean r2 = r2.isDebugEnabled()     // Catch:{ all -> 0x00a0 }
            if (r2 == 0) goto L_0x0078
            org.apache.commons.logging.Log r2 = r7.a     // Catch:{ all -> 0x00a0 }
            java.lang.String r3 = "Exception shutting down released connection."
            r2.debug(r3, r0)     // Catch:{ all -> 0x00a0 }
        L_0x0078:
            boolean r2 = r8.r()     // Catch:{ all -> 0x005d }
            org.apache.commons.logging.Log r0 = r7.a     // Catch:{ all -> 0x005d }
            boolean r0 = r0.isDebugEnabled()     // Catch:{ all -> 0x005d }
            if (r0 == 0) goto L_0x008d
            if (r2 == 0) goto L_0x0098
            org.apache.commons.logging.Log r0 = r7.a     // Catch:{ all -> 0x005d }
            java.lang.String r3 = "Released connection is reusable."
            r0.debug(r3)     // Catch:{ all -> 0x005d }
        L_0x008d:
            r8.j()     // Catch:{ all -> 0x005d }
            com.agilebinary.a.a.a.c.b.a.a r0 = r7.c     // Catch:{ all -> 0x005d }
            r3 = r9
            r5 = r11
            r0.a(r1, r2, r3, r5)     // Catch:{ all -> 0x005d }
            goto L_0x005b
        L_0x0098:
            org.apache.commons.logging.Log r0 = r7.a     // Catch:{ all -> 0x005d }
            java.lang.String r3 = "Released connection is not reusable."
            r0.debug(r3)     // Catch:{ all -> 0x005d }
            goto L_0x008d
        L_0x00a0:
            r0 = move-exception
            r6 = r0
            boolean r2 = r8.r()     // Catch:{ all -> 0x005d }
            org.apache.commons.logging.Log r0 = r7.a     // Catch:{ all -> 0x005d }
            boolean r0 = r0.isDebugEnabled()     // Catch:{ all -> 0x005d }
            if (r0 == 0) goto L_0x00b7
            if (r2 == 0) goto L_0x00c2
            org.apache.commons.logging.Log r0 = r7.a     // Catch:{ all -> 0x005d }
            java.lang.String r3 = "Released connection is reusable."
            r0.debug(r3)     // Catch:{ all -> 0x005d }
        L_0x00b7:
            r8.j()     // Catch:{ all -> 0x005d }
            com.agilebinary.a.a.a.c.b.a.a r0 = r7.c     // Catch:{ all -> 0x005d }
            r3 = r9
            r5 = r11
            r0.a(r1, r2, r3, r5)     // Catch:{ all -> 0x005d }
            throw r6     // Catch:{ all -> 0x005d }
        L_0x00c2:
            org.apache.commons.logging.Log r0 = r7.a     // Catch:{ all -> 0x005d }
            java.lang.String r3 = "Released connection is not reusable."
            r0.debug(r3)     // Catch:{ all -> 0x005d }
            goto L_0x00b7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.a.a.a.c.b.a.b.a(com.agilebinary.a.a.a.h.a, long, java.util.concurrent.TimeUnit):void");
    }

    public final void b(int i) {
        this.e.a(5);
    }

    /* access modifiers changed from: protected */
    public final void finalize() {
        try {
            this.a.debug("Shutting down");
            this.c.a();
        } finally {
            super.finalize();
        }
    }
}
