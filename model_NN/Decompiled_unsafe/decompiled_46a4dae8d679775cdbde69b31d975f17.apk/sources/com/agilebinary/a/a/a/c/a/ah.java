package com.agilebinary.a.a.a.c.a;

import com.agilebinary.a.a.a.k.c;
import com.agilebinary.a.a.a.k.e;
import com.agilebinary.a.a.a.k.f;
import com.agilebinary.a.a.a.k.k;
import com.agilebinary.a.a.a.m;
import com.agilebinary.a.a.a.o;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public abstract class ah extends u {
    protected static String a(f fVar) {
        String b = fVar.b();
        int lastIndexOf = b.lastIndexOf(47);
        if (lastIndexOf < 0) {
            return b;
        }
        if (lastIndexOf == 0) {
            lastIndexOf = 1;
        }
        return b.substring(0, lastIndexOf);
    }

    /* access modifiers changed from: protected */
    public List a(m[] mVarArr, f fVar) {
        ArrayList arrayList = new ArrayList(mVarArr.length);
        for (m mVar : mVarArr) {
            String a = mVar.a();
            String b = mVar.b();
            if (a == null || a.length() == 0) {
                throw new e("Cookie name may not be empty");
            }
            d dVar = new d(a, b);
            dVar.b(a(fVar));
            dVar.a(fVar.a());
            o[] c = mVar.c();
            for (int length = c.length - 1; length >= 0; length--) {
                o oVar = c[length];
                String lowerCase = oVar.a().toLowerCase(Locale.ENGLISH);
                dVar.a(lowerCase, oVar.b());
                k a2 = a(lowerCase);
                if (a2 != null) {
                    a2.a(dVar, oVar.b());
                }
            }
            arrayList.add(dVar);
        }
        return arrayList;
    }

    public void a(c cVar, f fVar) {
        if (cVar == null) {
            throw new IllegalArgumentException("Cookie may not be null");
        } else if (fVar == null) {
            throw new IllegalArgumentException("Cookie origin may not be null");
        } else {
            for (k a : c()) {
                a.a(cVar, fVar);
            }
        }
    }

    public boolean b(c cVar, f fVar) {
        if (cVar == null) {
            throw new IllegalArgumentException("Cookie may not be null");
        } else if (fVar == null) {
            throw new IllegalArgumentException("Cookie origin may not be null");
        } else {
            for (k b : c()) {
                if (!b.b(cVar, fVar)) {
                    return false;
                }
            }
            return true;
        }
    }
}
