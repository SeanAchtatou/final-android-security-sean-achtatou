package com.agilebinary.a.a.a.c.c;

import com.agilebinary.a.a.a.b.q;
import com.agilebinary.a.a.a.i.b;
import com.agilebinary.a.a.a.j.c;
import com.agilebinary.a.a.a.j.e;
import com.agilebinary.a.a.a.t;
import com.agilebinary.a.a.a.w;
import com.agilebinary.a.a.a.x;

public abstract class a implements c {
    protected final e a;
    protected final b b;
    protected final com.agilebinary.a.a.a.b.c c;

    public a(e eVar, com.agilebinary.a.a.a.b.c cVar) {
        if (eVar == null) {
            throw new IllegalArgumentException("Session input buffer may not be null");
        }
        this.a = eVar;
        this.b = new b(128);
        this.c = cVar != null ? cVar : q.a;
    }

    /* access modifiers changed from: protected */
    public abstract void a(x xVar);

    public final void b(x xVar) {
        if (xVar == null) {
            throw new IllegalArgumentException("HTTP message may not be null");
        }
        a(xVar);
        w f = xVar.f();
        while (f.hasNext()) {
            this.a.a(this.c.a(this.b, (t) f.next()));
        }
        this.b.a();
        this.a.a(this.b);
    }
}
