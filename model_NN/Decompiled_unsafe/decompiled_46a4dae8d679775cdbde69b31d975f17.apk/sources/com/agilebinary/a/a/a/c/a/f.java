package com.agilebinary.a.a.a.c.a;

import com.agilebinary.a.a.a.k.b;
import com.agilebinary.a.a.a.k.c;
import com.agilebinary.a.a.a.k.e;
import com.agilebinary.a.a.a.k.i;

public final class f extends x {
    public final void a(b bVar, String str) {
        if (bVar == null) {
            throw new IllegalArgumentException("Cookie may not be null");
        } else if (str == null) {
            throw new e("Missing value for version attribute");
        } else if (str.trim().length() == 0) {
            throw new e("Blank value for version attribute");
        } else {
            try {
                bVar.a(Integer.parseInt(str));
            } catch (NumberFormatException e) {
                throw new e("Invalid version: " + e.getMessage());
            }
        }
    }

    public final void a(c cVar, com.agilebinary.a.a.a.k.f fVar) {
        if (cVar == null) {
            throw new IllegalArgumentException("Cookie may not be null");
        } else if (cVar.g() < 0) {
            throw new i("Cookie version may not be negative");
        }
    }
}
