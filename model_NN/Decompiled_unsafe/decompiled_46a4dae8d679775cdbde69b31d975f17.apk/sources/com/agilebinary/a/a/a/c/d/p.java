package com.agilebinary.a.a.a.c.d;

import com.agilebinary.a.a.a.a.e;
import com.agilebinary.a.a.a.ab;
import com.agilebinary.a.a.a.b.n;
import com.agilebinary.a.a.a.c.b.g;
import com.agilebinary.a.a.a.d.a;
import com.agilebinary.a.a.a.d.c;
import com.agilebinary.a.a.a.d.f;
import com.agilebinary.a.a.a.d.k;
import com.agilebinary.a.a.a.f.b;
import com.agilebinary.a.a.a.f.i;
import com.agilebinary.a.a.a.h.c.h;
import com.agilebinary.a.a.a.h.j;
import com.agilebinary.a.a.a.h.m;
import com.agilebinary.a.a.a.l;
import com.agilebinary.a.a.a.s;
import com.agilebinary.a.a.a.t;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import org.apache.commons.logging.Log;

public final class p implements a {
    private final Log a;
    private j b;
    private h c;
    private s d;
    private m e;
    private b f;
    private ab g;
    private com.agilebinary.a.a.a.d.j h;
    private c i;
    private com.agilebinary.a.a.a.d.b j;
    private com.agilebinary.a.a.a.d.b k;
    private f l;
    private com.agilebinary.a.a.a.e.b m;
    private com.agilebinary.a.a.a.h.a n;
    private e o;
    private e p;
    private int q;
    private int r;
    private int s;
    private com.agilebinary.a.a.a.b t;

    public p(Log log, b bVar, j jVar, s sVar, m mVar, h hVar, ab abVar, com.agilebinary.a.a.a.d.j jVar2, c cVar, com.agilebinary.a.a.a.d.b bVar2, com.agilebinary.a.a.a.d.b bVar3, f fVar, com.agilebinary.a.a.a.e.b bVar4) {
        if (log == null) {
            throw new IllegalArgumentException("Log may not be null.");
        } else if (bVar == null) {
            throw new IllegalArgumentException("Request executor may not be null.");
        } else if (jVar == null) {
            throw new IllegalArgumentException("Client connection manager may not be null.");
        } else if (sVar == null) {
            throw new IllegalArgumentException("Connection reuse strategy may not be null.");
        } else if (mVar == null) {
            throw new IllegalArgumentException("Connection keep alive strategy may not be null.");
        } else if (hVar == null) {
            throw new IllegalArgumentException("Route planner may not be null.");
        } else if (abVar == null) {
            throw new IllegalArgumentException("HTTP protocol processor may not be null.");
        } else if (jVar2 == null) {
            throw new IllegalArgumentException("HTTP request retry handler may not be null.");
        } else if (cVar == null) {
            throw new IllegalArgumentException("Redirect strategy may not be null.");
        } else if (bVar2 == null) {
            throw new IllegalArgumentException("Target authentication handler may not be null.");
        } else if (bVar3 == null) {
            throw new IllegalArgumentException("Proxy authentication handler may not be null.");
        } else if (fVar == null) {
            throw new IllegalArgumentException("User token handler may not be null.");
        } else if (bVar4 == null) {
            throw new IllegalArgumentException("HTTP parameters may not be null");
        } else {
            this.a = log;
            this.f = bVar;
            this.b = jVar;
            this.d = sVar;
            this.e = mVar;
            this.c = hVar;
            this.g = abVar;
            this.h = jVar2;
            this.i = cVar;
            this.j = bVar2;
            this.k = bVar3;
            this.l = fVar;
            this.m = bVar4;
            this.n = null;
            this.q = 0;
            this.r = 0;
            this.s = this.m.a("http.protocol.max-redirects", 100);
            this.o = new e();
            this.p = new e();
        }
    }

    private static i a(com.agilebinary.a.a.a.f fVar) {
        return fVar instanceof com.agilebinary.a.a.a.p ? new h((com.agilebinary.a.a.a.p) fVar) : new i(fVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.a.a.a.e.b.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.agilebinary.a.a.a.e.b.a(java.lang.String, int):int
      com.agilebinary.a.a.a.e.b.a(java.lang.String, java.lang.Object):com.agilebinary.a.a.a.e.b
      com.agilebinary.a.a.a.e.b.a(java.lang.String, boolean):boolean */
    private j a(j jVar, com.agilebinary.a.a.a.j jVar2, i iVar) {
        com.agilebinary.a.a.a.h.c.c b2 = jVar.b();
        i a2 = jVar.a();
        com.agilebinary.a.a.a.e.b g2 = a2.g();
        if (g2 == null) {
            throw new IllegalArgumentException("HTTP parameters may not be null");
        } else if (!g2.a("http.protocol.handle-redirects", true) || !this.i.a(a2, jVar2)) {
            com.agilebinary.a.a.a.d.e eVar = (com.agilebinary.a.a.a.d.e) iVar.a("http.auth.credentials-provider");
            if (eVar != null && com.agilebinary.a.a.a.c.b.a.e.a(g2)) {
                if (this.j.a(jVar2)) {
                    com.agilebinary.a.a.a.b bVar = (com.agilebinary.a.a.a.b) iVar.a("http.target_host");
                    com.agilebinary.a.a.a.b a3 = bVar == null ? b2.a() : bVar;
                    this.a.debug("Target requested authentication");
                    try {
                        a(this.j.b(jVar2), this.o, this.j, jVar2, iVar);
                    } catch (com.agilebinary.a.a.a.a.b e2) {
                        if (this.a.isWarnEnabled()) {
                            this.a.warn("Authentication error: " + e2.getMessage());
                            return null;
                        }
                    }
                    a(this.o, a3, eVar);
                    if (this.o.d() != null) {
                        return jVar;
                    }
                    return null;
                }
                this.o.a((com.agilebinary.a.a.a.a.c) null);
                if (this.k.a(jVar2)) {
                    com.agilebinary.a.a.a.b g3 = b2.g();
                    this.a.debug("Proxy requested authentication");
                    try {
                        a(this.k.b(jVar2), this.p, this.k, jVar2, iVar);
                    } catch (com.agilebinary.a.a.a.a.b e3) {
                        if (this.a.isWarnEnabled()) {
                            this.a.warn("Authentication error: " + e3.getMessage());
                            return null;
                        }
                    }
                    a(this.p, g3, eVar);
                    if (this.p.d() != null) {
                        return jVar;
                    }
                    return null;
                }
                this.p.a((com.agilebinary.a.a.a.a.c) null);
            }
            return null;
        } else if (this.r >= this.s) {
            throw new k("Maximum redirects (" + this.s + ") exceeded");
        } else {
            this.r++;
            this.t = null;
            com.agilebinary.a.a.a.d.c.b a4 = this.i.a(a2, jVar2, iVar);
            a4.a(a2.k().e());
            URI c_ = a4.c_();
            if (c_.getHost() == null) {
                throw new l("Redirect URI does not specify a valid host name: " + c_);
            }
            com.agilebinary.a.a.a.b bVar2 = new com.agilebinary.a.a.a.b(c_.getHost(), c_.getPort(), c_.getScheme());
            this.o.a((com.agilebinary.a.a.a.a.c) null);
            this.p.a((com.agilebinary.a.a.a.a.c) null);
            if (!b2.a().equals(bVar2)) {
                this.o.a();
                com.agilebinary.a.a.a.a.f c2 = this.p.c();
                if (c2 != null && c2.d()) {
                    this.p.a();
                }
            }
            i a5 = a(a4);
            a5.a(g2);
            com.agilebinary.a.a.a.h.c.c b3 = b(bVar2, a5, iVar);
            j jVar3 = new j(a5, b3);
            if (this.a.isDebugEnabled()) {
                this.a.debug("Redirecting to '" + c_ + "' via " + b3);
            }
            return jVar3;
        }
    }

    private void a() {
        try {
            this.n.b_();
        } catch (IOException e2) {
            this.a.debug("IOException releasing connection", e2);
        }
        this.n = null;
    }

    private void a(e eVar, com.agilebinary.a.a.a.b bVar, com.agilebinary.a.a.a.d.e eVar2) {
        if (eVar.b()) {
            String a2 = bVar.a();
            int b2 = bVar.b();
            if (b2 < 0) {
                b2 = this.b.a().a(bVar).a();
            }
            com.agilebinary.a.a.a.a.f c2 = eVar.c();
            com.agilebinary.a.a.a.a.c cVar = new com.agilebinary.a.a.a.a.c(a2, b2, c2.c(), c2.b());
            if (this.a.isDebugEnabled()) {
                this.a.debug("Authentication scope: " + cVar);
            }
            com.agilebinary.a.a.a.h.b.b d2 = eVar.d();
            if (d2 == null) {
                d2 = eVar2.a(cVar);
                if (this.a.isDebugEnabled()) {
                    if (d2 != null) {
                        this.a.debug("Found credentials");
                    } else {
                        this.a.debug("Credentials not found");
                    }
                }
            } else if (c2.e()) {
                this.a.debug("Authentication failed");
                d2 = null;
            }
            eVar.a(cVar);
            eVar.a(d2);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.a.a.a.h.a.a(boolean, com.agilebinary.a.a.a.e.b):void
     arg types: [int, com.agilebinary.a.a.a.e.b]
     candidates:
      com.agilebinary.a.a.a.h.a.a(long, java.util.concurrent.TimeUnit):void
      com.agilebinary.a.a.a.h.a.a(com.agilebinary.a.a.a.f.i, com.agilebinary.a.a.a.e.b):void
      com.agilebinary.a.a.a.h.a.a(boolean, com.agilebinary.a.a.a.e.b):void */
    private void a(j jVar, i iVar) {
        int a2;
        com.agilebinary.a.a.a.h.c.c b2 = jVar.b();
        int i2 = 0;
        boolean z = true;
        while (z) {
            i2++;
            try {
                if (!this.n.l()) {
                    this.n.a(b2, iVar, this.m);
                } else {
                    this.n.b(com.agilebinary.a.a.a.c.e.a.a(this.m));
                }
                com.agilebinary.a.a.a.h.c.a aVar = new com.agilebinary.a.a.a.h.c.a();
                do {
                    com.agilebinary.a.a.a.h.c.c a_ = this.n.a_();
                    a2 = aVar.a(b2, a_);
                    switch (a2) {
                        case -1:
                            throw new IllegalStateException("Unable to establish route.\nplanned = " + b2 + "\ncurrent = " + a_);
                        case 0:
                            break;
                        case 1:
                        case 2:
                            this.n.a(b2, iVar, this.m);
                            continue;
                        case 3:
                            a(b2, iVar);
                            this.a.debug("Tunnel to target created.");
                            this.n.a(false, this.m);
                            continue;
                        case 4:
                            int c2 = a_.c() - 1;
                            throw new UnsupportedOperationException("Proxy chains are not supported.");
                        case 5:
                            this.n.a(iVar, this.m);
                            continue;
                        default:
                            throw new IllegalStateException("Unknown step indicator " + a2 + " from RouteDirector.");
                    }
                } while (a2 > 0);
                z = false;
            } catch (IOException e2) {
                try {
                    this.n.k();
                } catch (IOException e3) {
                }
                if (this.h.a(e2, i2, iVar)) {
                    if (this.a.isInfoEnabled()) {
                        this.a.info("I/O exception (" + e2.getClass().getName() + ") caught when connecting to the target host: " + e2.getMessage());
                    }
                    if (this.a.isDebugEnabled()) {
                        this.a.debug(e2.getMessage(), e2);
                    }
                    this.a.info("Retrying connect");
                } else {
                    throw e2;
                }
            }
        }
    }

    private void a(Map map, e eVar, com.agilebinary.a.a.a.d.b bVar, com.agilebinary.a.a.a.j jVar, i iVar) {
        com.agilebinary.a.a.a.a.f c2 = eVar.c();
        if (c2 == null) {
            c2 = bVar.a(map, jVar, iVar);
            eVar.a(c2);
        }
        String b2 = c2.b();
        if (((t) map.get(b2.toLowerCase(Locale.ENGLISH))) == null) {
            throw new com.agilebinary.a.a.a.a.b(b2 + " authorization challenge expected, but not found");
        }
        this.a.debug("Authorization challenge processed");
    }

    private boolean a(com.agilebinary.a.a.a.h.c.c cVar, i iVar) {
        com.agilebinary.a.a.a.j jVar;
        boolean z;
        com.agilebinary.a.a.a.b g2 = cVar.g();
        com.agilebinary.a.a.a.b a2 = cVar.a();
        boolean z2 = false;
        com.agilebinary.a.a.a.j jVar2 = null;
        while (true) {
            if (z2) {
                jVar = jVar2;
                break;
            }
            if (!this.n.l()) {
                this.n.a(cVar, iVar, this.m);
            }
            com.agilebinary.a.a.a.b a3 = cVar.a();
            String a4 = a3.a();
            int b2 = a3.b();
            int a5 = b2 < 0 ? this.b.a().a(a3.c()).a() : b2;
            StringBuilder sb = new StringBuilder(a4.length() + 6);
            sb.append(a4);
            sb.append(':');
            sb.append(Integer.toString(a5));
            n nVar = new n("CONNECT", sb.toString(), com.agilebinary.mobilemonitor.device.a.b.a.a.a.b(this.m));
            nVar.a(this.m);
            iVar.a("http.target_host", a2);
            iVar.a("http.proxy_host", g2);
            iVar.a("http.connection", this.n);
            iVar.a("http.auth.target-scope", this.o);
            iVar.a("http.auth.proxy-scope", this.p);
            iVar.a("http.request", nVar);
            b.a(nVar, this.g, iVar);
            com.agilebinary.a.a.a.j a6 = this.f.a(nVar, this.n, iVar);
            a6.a(this.m);
            b.a(a6, this.g, iVar);
            if (a6.a().b() < 200) {
                throw new com.agilebinary.a.a.a.k("Unexpected response to CONNECT request: " + a6.a());
            }
            com.agilebinary.a.a.a.d.e eVar = (com.agilebinary.a.a.a.d.e) iVar.a("http.auth.credentials-provider");
            if (eVar != null && com.agilebinary.a.a.a.c.b.a.e.a(this.m)) {
                if (this.k.a(a6)) {
                    this.a.debug("Proxy requested authentication");
                    try {
                        a(this.k.b(a6), this.p, this.k, a6, iVar);
                    } catch (com.agilebinary.a.a.a.a.b e2) {
                        if (this.a.isWarnEnabled()) {
                            this.a.warn("Authentication error: " + e2.getMessage());
                            jVar = a6;
                            break;
                        }
                    }
                    a(this.p, g2, eVar);
                    if (this.p.d() == null) {
                        z2 = true;
                    } else if (this.d.a(a6, iVar)) {
                        this.a.debug("Connection kept alive");
                        g.a(a6.b());
                        z = false;
                        jVar2 = a6;
                    } else {
                        this.n.k();
                        z2 = false;
                    }
                    jVar2 = a6;
                } else {
                    this.p.a((com.agilebinary.a.a.a.a.c) null);
                }
            }
            z = true;
            jVar2 = a6;
        }
        if (jVar.a().b() > 299) {
            com.agilebinary.a.a.a.c b3 = jVar.b();
            if (b3 != null) {
                jVar.a(new com.agilebinary.a.a.a.g.e(b3));
            }
            this.n.k();
            throw new d("CONNECT refused by proxy: " + jVar.a(), jVar);
        }
        this.n.g();
        return false;
    }

    private com.agilebinary.a.a.a.h.c.c b(com.agilebinary.a.a.a.b bVar, com.agilebinary.a.a.a.f fVar, i iVar) {
        com.agilebinary.a.a.a.b bVar2 = bVar == null ? (com.agilebinary.a.a.a.b) fVar.g().a("http.default-host") : bVar;
        if (bVar2 != null) {
            return this.c.a(bVar2, fVar);
        }
        throw new IllegalStateException("Target host must not be null, or set in parameters.");
    }

    private com.agilebinary.a.a.a.j b(j jVar, i iVar) {
        i a2 = jVar.a();
        com.agilebinary.a.a.a.h.c.c b2 = jVar.b();
        com.agilebinary.a.a.a.j jVar2 = null;
        IOException e2 = null;
        boolean z = true;
        while (z) {
            this.q++;
            a2.m();
            if (!a2.i()) {
                this.a.debug("Cannot retry non-repeatable request");
                if (e2 != null) {
                    throw new com.agilebinary.a.a.a.d.i("Cannot retry request with a non-repeatable request entity.  The cause lists the reason the original request failed.", e2);
                }
                throw new com.agilebinary.a.a.a.d.i("Cannot retry request with a non-repeatable request entity.");
            }
            try {
                if (this.a.isDebugEnabled()) {
                    this.a.debug("Attempt " + this.q + " to execute request");
                }
                jVar2 = this.f.a(a2, this.n, iVar);
                z = false;
            } catch (IOException e3) {
                e2 = e3;
                this.a.debug("Closing the connection.");
                try {
                    this.n.k();
                } catch (IOException e4) {
                }
                if (this.h.a(e2, a2.l(), iVar)) {
                    if (this.a.isInfoEnabled()) {
                        this.a.info("I/O exception (" + e2.getClass().getName() + ") caught when processing request: " + e2.getMessage());
                    }
                    if (this.a.isDebugEnabled()) {
                        this.a.debug(e2.getMessage(), e2);
                    }
                    this.a.info("Retrying request");
                    if (!b2.d()) {
                        this.a.debug("Reopening the direct connection.");
                        this.n.a(b2, iVar, this.m);
                    } else {
                        this.a.debug("Proxied connection. Need to start over.");
                        z = false;
                    }
                } else {
                    throw e2;
                }
            }
        }
        return jVar2;
    }

    private void b() {
        com.agilebinary.a.a.a.h.a aVar = this.n;
        if (aVar != null) {
            this.n = null;
            try {
                aVar.b();
            } catch (IOException e2) {
                if (this.a.isDebugEnabled()) {
                    this.a.debug(e2.getMessage(), e2);
                }
            }
            try {
                aVar.b_();
            } catch (IOException e3) {
                this.a.debug("Error releasing connection", e3);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.a.a.a.e.b.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.agilebinary.a.a.a.e.b.a(java.lang.String, int):int
      com.agilebinary.a.a.a.e.b.a(java.lang.String, java.lang.Object):com.agilebinary.a.a.a.e.b
      com.agilebinary.a.a.a.e.b.a(java.lang.String, boolean):boolean */
    public final com.agilebinary.a.a.a.j a(com.agilebinary.a.a.a.b bVar, com.agilebinary.a.a.a.f fVar, i iVar) {
        i a2;
        i a3 = a(fVar);
        a3.a(this.m);
        com.agilebinary.a.a.a.h.c.c b2 = b(bVar, a3, iVar);
        this.t = (com.agilebinary.a.a.a.b) fVar.g().a("http.virtual-host");
        j jVar = new j(a3, b2);
        long c2 = (long) com.agilebinary.a.a.a.c.e.a.c(this.m);
        boolean z = false;
        com.agilebinary.a.a.a.j jVar2 = null;
        j jVar3 = jVar;
        boolean z2 = false;
        while (!z) {
            try {
                a2 = jVar3.a();
                com.agilebinary.a.a.a.h.c.c b3 = jVar3.b();
                Object a4 = iVar.a("http.user-token");
                if (this.n == null) {
                    com.agilebinary.a.a.a.h.b a5 = this.b.a(b3, a4);
                    if (fVar instanceof com.agilebinary.a.a.a.d.c.g) {
                        ((com.agilebinary.a.a.a.d.c.g) fVar).a(a5);
                    }
                    this.n = a5.a(c2, TimeUnit.MILLISECONDS);
                    com.agilebinary.a.a.a.e.b bVar2 = this.m;
                    if (bVar2 == null) {
                        throw new IllegalArgumentException("HTTP parameters may not be null");
                    } else if (bVar2.a("http.connection.stalecheck", true) && this.n.l()) {
                        this.a.debug("Stale connection check");
                        if (this.n.e()) {
                            this.a.debug("Stale connection detected");
                            this.n.k();
                        }
                    }
                }
                if (fVar instanceof com.agilebinary.a.a.a.d.c.g) {
                    ((com.agilebinary.a.a.a.d.c.g) fVar).a(this.n);
                }
                try {
                    a(jVar3, iVar);
                    a2.j();
                    URI c_ = a2.c_();
                    if (b3.g() == null || b3.d()) {
                        if (c_.isAbsolute()) {
                            a2.a(com.agilebinary.a.a.a.d.a.b.a(c_, (com.agilebinary.a.a.a.b) null));
                        }
                    } else if (!c_.isAbsolute()) {
                        a2.a(com.agilebinary.a.a.a.d.a.b.a(c_, b3.a()));
                    }
                    com.agilebinary.a.a.a.b bVar3 = this.t;
                    if (bVar3 == null) {
                        bVar3 = b3.a();
                    }
                    com.agilebinary.a.a.a.b g2 = b3.g();
                    iVar.a("http.target_host", bVar3);
                    iVar.a("http.proxy_host", g2);
                    iVar.a("http.connection", this.n);
                    iVar.a("http.auth.target-scope", this.o);
                    iVar.a("http.auth.proxy-scope", this.p);
                    b.a(a2, this.g, iVar);
                    jVar2 = b(jVar3, iVar);
                    if (jVar2 != null) {
                        jVar2.a(this.m);
                        b.a(jVar2, this.g, iVar);
                        z2 = this.d.a(jVar2, iVar);
                        if (z2) {
                            long a6 = this.e.a(jVar2);
                            if (this.a.isDebugEnabled()) {
                                this.a.debug("Connection can be kept alive for " + (a6 >= 0 ? a6 + " " + TimeUnit.MILLISECONDS : "ever"));
                            }
                            this.n.a(a6, TimeUnit.MILLISECONDS);
                        }
                        j a7 = a(jVar3, jVar2, iVar);
                        if (a7 == null) {
                            z = true;
                        } else {
                            if (z2) {
                                g.a(jVar2.b());
                                this.n.g();
                            } else {
                                this.n.k();
                            }
                            if (!a7.b().equals(jVar3.b())) {
                                a();
                            }
                            jVar3 = a7;
                        }
                        if (this.n != null && a4 == null) {
                            Object a8 = this.l.a(iVar);
                            iVar.a("http.user-token", a8);
                            if (a8 != null) {
                                this.n.a(a8);
                            }
                        }
                    }
                } catch (d e2) {
                    if (this.a.isDebugEnabled()) {
                        this.a.debug(e2.getMessage());
                    }
                    jVar2 = e2.a();
                }
            } catch (URISyntaxException e3) {
                throw new l("Invalid URI: " + a2.a().c(), e3);
            } catch (InterruptedException e4) {
                InterruptedIOException interruptedIOException = new InterruptedIOException();
                interruptedIOException.initCause(e4);
                throw interruptedIOException;
            } catch (com.agilebinary.a.a.a.c.b.m e5) {
                InterruptedIOException interruptedIOException2 = new InterruptedIOException("Connection has been shut down");
                interruptedIOException2.initCause(e5);
                throw interruptedIOException2;
            } catch (com.agilebinary.a.a.a.k e6) {
                b();
                throw e6;
            } catch (IOException e7) {
                b();
                throw e7;
            } catch (RuntimeException e8) {
                b();
                throw e8;
            }
        }
        if (jVar2 == null || jVar2.b() == null || !jVar2.b().g()) {
            if (z2) {
                this.n.g();
            }
            a();
        } else {
            jVar2.a(new com.agilebinary.a.a.a.h.i(jVar2.b(), this.n, z2));
        }
        return jVar2;
    }
}
