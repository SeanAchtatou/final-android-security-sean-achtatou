package com.agilebinary.a.a.a.c.c;

import com.agilebinary.a.a.a.j.a;
import com.agilebinary.a.a.a.j.g;
import java.io.IOException;
import java.io.InputStream;

public final class l extends InputStream {
    private long a;
    private long b = 0;
    private boolean c = false;
    private a d = null;

    public l(a aVar, long j) {
        if (aVar == null) {
            throw new IllegalArgumentException("Input stream may not be null");
        } else if (j < 0) {
            throw new IllegalArgumentException("Content length may not be negative");
        } else {
            this.d = aVar;
            this.a = j;
        }
    }

    public final int available() {
        if (this.d instanceof g) {
            return Math.min(((g) this.d).d_(), (int) (this.a - this.b));
        }
        return 0;
    }

    public final void close() {
        if (!this.c) {
            try {
                do {
                } while (read(new byte[2048]) >= 0);
            } finally {
                this.c = true;
            }
        }
    }

    public final int read() {
        if (this.c) {
            throw new IOException("Attempted read from closed stream.");
        } else if (this.b >= this.a) {
            return -1;
        } else {
            this.b++;
            return this.d.d();
        }
    }

    public final int read(byte[] bArr) {
        return read(bArr, 0, bArr.length);
    }

    public final int read(byte[] bArr, int i, int i2) {
        if (this.c) {
            throw new IOException("Attempted read from closed stream.");
        } else if (this.b >= this.a) {
            return -1;
        } else {
            int a2 = this.d.a(bArr, i, this.b + ((long) i2) > this.a ? (int) (this.a - this.b) : i2);
            this.b += (long) a2;
            return a2;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(long, long):long}
     arg types: [int, long]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(float, float):float}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(long, long):long} */
    public final long skip(long j) {
        int read;
        if (j <= 0) {
            return 0;
        }
        byte[] bArr = new byte[2048];
        long min = Math.min(j, this.a - this.b);
        long j2 = 0;
        while (min > 0 && (read = read(bArr, 0, (int) Math.min(2048L, min))) != -1) {
            j2 += (long) read;
            min -= (long) read;
        }
        return j2;
    }
}
