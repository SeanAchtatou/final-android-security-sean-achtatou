package com.agilebinary.a.a.a.e;

public abstract class b {
    protected b() {
    }

    public int a(String str, int i) {
        Object a = a(str);
        return a == null ? i : ((Integer) a).intValue();
    }

    public abstract b a(String str, Object obj);

    public abstract Object a(String str);

    public boolean a(String str, boolean z) {
        Object a = a(str);
        return a == null ? z : ((Boolean) a).booleanValue();
    }

    public b b(String str, int i) {
        a(str, new Integer(i));
        return this;
    }

    public b b(String str, boolean z) {
        a(str, z ? Boolean.TRUE : Boolean.FALSE);
        return this;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.a.a.a.e.b.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.agilebinary.a.a.a.e.b.a(java.lang.String, int):int
      com.agilebinary.a.a.a.e.b.a(java.lang.String, java.lang.Object):com.agilebinary.a.a.a.e.b
      com.agilebinary.a.a.a.e.b.a(java.lang.String, boolean):boolean */
    public boolean b(String str) {
        return a(str, false);
    }

    public boolean c(String str) {
        return !a(str, false);
    }
}
