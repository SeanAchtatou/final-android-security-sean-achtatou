package com.agilebinary.a.a.a.c.a;

import com.agilebinary.a.a.a.b.e;
import com.agilebinary.a.a.a.i.b;
import com.agilebinary.a.a.a.k.c;
import com.agilebinary.a.a.a.t;
import java.util.ArrayList;
import java.util.List;

public final class w extends ah {
    private static final String[] a = {"EEE, dd MMM yyyy HH:mm:ss zzz", "EEEE, dd-MMM-yy HH:mm:ss zzz", "EEE MMM d HH:mm:ss yyyy", "EEE, dd-MMM-yyyy HH:mm:ss z", "EEE, dd-MMM-yyyy HH-mm-ss z", "EEE, dd MMM yy HH:mm:ss z", "EEE dd-MMM-yyyy HH:mm:ss z", "EEE dd MMM yyyy HH:mm:ss z", "EEE dd-MMM-yyyy HH-mm-ss z", "EEE dd-MMM-yy HH:mm:ss z", "EEE dd MMM yy HH:mm:ss z", "EEE,dd-MMM-yy HH:mm:ss z", "EEE,dd-MMM-yyyy HH:mm:ss z", "EEE, dd-MM-yyyy HH:mm:ss z"};
    private final String[] b;

    static {
        String[] strArr = {"EEE, dd MMM yyyy HH:mm:ss zzz", "EEEE, dd-MMM-yy HH:mm:ss zzz", "EEE MMM d HH:mm:ss yyyy", "EEE, dd-MMM-yyyy HH:mm:ss z", "EEE, dd-MMM-yyyy HH-mm-ss z", "EEE, dd MMM yy HH:mm:ss z", "EEE dd-MMM-yyyy HH:mm:ss z", "EEE dd MMM yyyy HH:mm:ss z", "EEE dd-MMM-yyyy HH-mm-ss z", "EEE dd-MMM-yy HH:mm:ss z", "EEE dd MMM yy HH:mm:ss z", "EEE,dd-MMM-yy HH:mm:ss z", "EEE,dd-MMM-yyyy HH:mm:ss z", "EEE, dd-MM-yyyy HH:mm:ss z"};
    }

    public w() {
        this(null);
    }

    public w(String[] strArr) {
        if (strArr != null) {
            this.b = (String[]) strArr.clone();
        } else {
            this.b = a;
        }
        a("path", new m());
        a("domain", new r());
        a("max-age", new ac());
        a("secure", new h());
        a("comment", new o());
        a("expires", new j(this.b));
    }

    public final int a() {
        return 0;
    }

    /* JADX WARNING: Removed duplicated region for block: B:20:0x0077  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x00cc  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.List a(com.agilebinary.a.a.a.t r10, com.agilebinary.a.a.a.k.f r11) {
        /*
            r9 = this;
            r7 = 1
            r4 = -1
            r6 = 0
            if (r10 != 0) goto L_0x000d
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.String r2 = "Header may not be null"
            r1.<init>(r2)
            throw r1
        L_0x000d:
            if (r11 != 0) goto L_0x0017
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.String r2 = "Cookie origin may not be null"
            r1.<init>(r2)
            throw r1
        L_0x0017:
            java.lang.String r1 = r10.a()
            java.lang.String r2 = r10.b()
            java.lang.String r3 = "Set-Cookie"
            boolean r1 = r1.equalsIgnoreCase(r3)
            if (r1 != 0) goto L_0x004a
            com.agilebinary.a.a.a.k.e r1 = new com.agilebinary.a.a.a.k.e
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Unrecognized cookie header '"
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r3 = r10.toString()
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r3 = "'"
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            r1.<init>(r2)
            throw r1
        L_0x004a:
            java.util.Locale r1 = java.util.Locale.ENGLISH
            java.lang.String r1 = r2.toLowerCase(r1)
            java.lang.String r3 = "expires="
            int r1 = r1.indexOf(r3)
            if (r1 == r4) goto L_0x00a3
            java.lang.String r3 = "expires="
            int r3 = r3.length()
            int r1 = r1 + r3
            r3 = 59
            int r3 = r2.indexOf(r3, r1)
            if (r3 != r4) goto L_0x006b
            int r3 = r2.length()
        L_0x006b:
            java.lang.String r1 = r2.substring(r1, r3)     // Catch:{ k -> 0x00a2 }
            java.lang.String[] r2 = r9.b     // Catch:{ k -> 0x00a2 }
            com.agilebinary.a.a.a.c.a.l.a(r1, r2)     // Catch:{ k -> 0x00a2 }
            r1 = r7
        L_0x0075:
            if (r1 == 0) goto L_0x00cc
            com.agilebinary.a.a.a.c.a.aa r2 = com.agilebinary.a.a.a.c.a.aa.a
            boolean r1 = r10 instanceof com.agilebinary.a.a.a.r
            if (r1 == 0) goto L_0x00a5
            r0 = r10
            com.agilebinary.a.a.a.r r0 = (com.agilebinary.a.a.a.r) r0
            r1 = r0
            com.agilebinary.a.a.a.i.b r1 = r1.e()
            com.agilebinary.a.a.a.b.i r3 = new com.agilebinary.a.a.a.b.i
            com.agilebinary.a.a.a.r r10 = (com.agilebinary.a.a.a.r) r10
            int r4 = r10.d()
            int r5 = r1.c()
            r3.<init>(r4, r5)
        L_0x0094:
            com.agilebinary.a.a.a.m[] r4 = new com.agilebinary.a.a.a.m[r7]
            com.agilebinary.a.a.a.m r1 = r2.a(r1, r3)
            r4[r6] = r1
            r1 = r4
        L_0x009d:
            java.util.List r1 = r9.a(r1, r11)
            return r1
        L_0x00a2:
            r1 = move-exception
        L_0x00a3:
            r1 = r6
            goto L_0x0075
        L_0x00a5:
            java.lang.String r1 = r10.b()
            if (r1 != 0) goto L_0x00b3
            com.agilebinary.a.a.a.k.e r1 = new com.agilebinary.a.a.a.k.e
            java.lang.String r2 = "Header value is null"
            r1.<init>(r2)
            throw r1
        L_0x00b3:
            com.agilebinary.a.a.a.i.b r3 = new com.agilebinary.a.a.a.i.b
            int r4 = r1.length()
            r3.<init>(r4)
            r3.a(r1)
            com.agilebinary.a.a.a.b.i r1 = new com.agilebinary.a.a.a.b.i
            int r4 = r3.c()
            r1.<init>(r6, r4)
            r8 = r3
            r3 = r1
            r1 = r8
            goto L_0x0094
        L_0x00cc:
            com.agilebinary.a.a.a.m[] r1 = r10.c()
            goto L_0x009d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.a.a.a.c.a.w.a(com.agilebinary.a.a.a.t, com.agilebinary.a.a.a.k.f):java.util.List");
    }

    public final List a(List list) {
        if (list == null) {
            throw new IllegalArgumentException("List of cookies may not be null");
        } else if (list.isEmpty()) {
            throw new IllegalArgumentException("List of cookies may not be empty");
        } else {
            b bVar = new b(list.size() * 20);
            bVar.a("Cookie");
            bVar.a(": ");
            for (int i = 0; i < list.size(); i++) {
                c cVar = (c) list.get(i);
                if (i > 0) {
                    bVar.a("; ");
                }
                bVar.a(cVar.a());
                bVar.a("=");
                String b2 = cVar.b();
                if (b2 != null) {
                    bVar.a(b2);
                }
            }
            ArrayList arrayList = new ArrayList(1);
            arrayList.add(new e(bVar));
            return arrayList;
        }
    }

    public final t b() {
        return null;
    }

    public final String toString() {
        return "compatibility";
    }
}
