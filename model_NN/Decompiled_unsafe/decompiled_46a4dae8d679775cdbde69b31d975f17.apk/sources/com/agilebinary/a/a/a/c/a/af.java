package com.agilebinary.a.a.a.c.a;

import com.agilebinary.a.a.a.k.h;
import java.io.Serializable;
import java.util.Date;

public final class af extends d implements h, Serializable {
    private int[] a;
    private boolean b;

    public af(String str, String str2) {
        super(str, str2);
    }

    public final void a(int[] iArr) {
        this.a = iArr;
    }

    public final void b(boolean z) {
        this.b = true;
    }

    public final boolean b(Date date) {
        return this.b || super.b(date);
    }

    public final Object clone() {
        af afVar = (af) super.clone();
        afVar.a = (int[]) this.a.clone();
        return afVar;
    }

    public final int[] f() {
        return this.a;
    }
}
