package com.agilebinary.a.a.a.h;

import com.agilebinary.a.a.a.j;
import com.agilebinary.a.a.a.v;

public class m {
    public long a(j jVar) {
        if (jVar == null) {
            throw new IllegalArgumentException("HTTP response may not be null");
        }
        v vVar = new v(jVar.d("Keep-Alive"));
        while (vVar.hasNext()) {
            com.agilebinary.a.a.a.m a = vVar.a();
            String a2 = a.a();
            String b = a.b();
            if (b != null && a2.equalsIgnoreCase("timeout")) {
                try {
                    return Long.parseLong(b) * 1000;
                } catch (NumberFormatException e) {
                }
            }
        }
        return -1;
    }
}
