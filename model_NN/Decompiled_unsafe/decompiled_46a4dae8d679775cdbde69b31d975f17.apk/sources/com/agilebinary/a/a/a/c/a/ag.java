package com.agilebinary.a.a.a.c.a;

import com.agilebinary.a.a.a.b.e;
import com.agilebinary.a.a.a.i.b;
import com.agilebinary.a.a.a.k.c;
import com.agilebinary.a.a.a.k.f;
import com.agilebinary.a.a.a.k.i;
import com.agilebinary.a.a.a.k.l;
import com.agilebinary.a.a.a.k.m;
import com.agilebinary.a.a.a.t;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class ag extends ah {
    private static final l a = new l();
    private static final String[] b = {"EEE, dd MMM yyyy HH:mm:ss zzz", "EEEE, dd-MMM-yy HH:mm:ss zzz", "EEE MMM d HH:mm:ss yyyy"};
    private final String[] c;
    private final boolean d;

    public ag() {
        this(null, false);
    }

    public ag(String[] strArr, boolean z) {
        if (strArr != null) {
            this.c = (String[]) strArr.clone();
        } else {
            this.c = b;
        }
        this.d = z;
        a("version", new f());
        a("path", new m());
        a("domain", new c());
        a("max-age", new ac());
        a("secure", new h());
        a("comment", new o());
        a("expires", new j(this.c));
    }

    private static void a(b bVar, String str, String str2, int i) {
        bVar.a(str);
        bVar.a("=");
        if (str2 == null) {
            return;
        }
        if (i > 0) {
            bVar.a('\"');
            bVar.a(str2);
            bVar.a('\"');
            return;
        }
        bVar.a(str2);
    }

    private List b(List list) {
        int i;
        int i2 = Integer.MAX_VALUE;
        Iterator it = list.iterator();
        while (true) {
            i = i2;
            if (!it.hasNext()) {
                break;
            }
            c cVar = (c) it.next();
            i2 = cVar.g() < i ? cVar.g() : i;
        }
        b bVar = new b(list.size() * 40);
        bVar.a("Cookie");
        bVar.a(": ");
        bVar.a("$Version=");
        bVar.a(Integer.toString(i));
        Iterator it2 = list.iterator();
        while (it2.hasNext()) {
            bVar.a("; ");
            a(bVar, (c) it2.next(), i);
        }
        ArrayList arrayList = new ArrayList(1);
        arrayList.add(new e(bVar));
        return arrayList;
    }

    private List c(List list) {
        ArrayList arrayList = new ArrayList(list.size());
        Iterator it = list.iterator();
        while (it.hasNext()) {
            c cVar = (c) it.next();
            int g = cVar.g();
            b bVar = new b(40);
            bVar.a("Cookie: ");
            bVar.a("$Version=");
            bVar.a(Integer.toString(g));
            bVar.a("; ");
            a(bVar, cVar, g);
            arrayList.add(new e(bVar));
        }
        return arrayList;
    }

    public int a() {
        return 1;
    }

    public List a(t tVar, f fVar) {
        if (tVar == null) {
            throw new IllegalArgumentException("Header may not be null");
        } else if (fVar == null) {
            throw new IllegalArgumentException("Cookie origin may not be null");
        } else if (tVar.a().equalsIgnoreCase("Set-Cookie")) {
            return a(tVar.c(), fVar);
        } else {
            throw new com.agilebinary.a.a.a.k.e("Unrecognized cookie header '" + tVar.toString() + "'");
        }
    }

    public final List a(List list) {
        List list2;
        if (list == null) {
            throw new IllegalArgumentException("List of cookies may not be null");
        } else if (list.isEmpty()) {
            throw new IllegalArgumentException("List of cookies may not be empty");
        } else {
            if (list.size() > 1) {
                list2 = new ArrayList(list);
                Collections.sort(list2, a);
            } else {
                list2 = list;
            }
            return this.d ? b(list2) : c(list2);
        }
    }

    /* access modifiers changed from: protected */
    public void a(b bVar, c cVar, int i) {
        a(bVar, cVar.a(), cVar.b(), i);
        if (cVar.d() != null && (cVar instanceof m) && ((m) cVar).d("path")) {
            bVar.a("; ");
            a(bVar, "$Path", cVar.d(), i);
        }
        if (cVar.c() != null && (cVar instanceof m) && ((m) cVar).d("domain")) {
            bVar.a("; ");
            a(bVar, "$Domain", cVar.c(), i);
        }
    }

    public void a(c cVar, f fVar) {
        if (cVar == null) {
            throw new IllegalArgumentException("Cookie may not be null");
        }
        String a2 = cVar.a();
        if (a2.indexOf(32) != -1) {
            throw new i("Cookie name may not contain blanks");
        } else if (a2.startsWith("$")) {
            throw new i("Cookie name may not start with $");
        } else {
            super.a(cVar, fVar);
        }
    }

    public t b() {
        return null;
    }

    public String toString() {
        return "rfc2109";
    }
}
