package com.agilebinary.a.a.a.c.d;

import com.agilebinary.a.a.a.a.a;
import com.agilebinary.a.a.a.aa;
import com.agilebinary.a.a.a.ab;
import com.agilebinary.a.a.a.c.a.e;
import com.agilebinary.a.a.a.c.a.s;
import com.agilebinary.a.a.a.c.a.t;
import com.agilebinary.a.a.a.c.a.v;
import com.agilebinary.a.a.a.e.b;
import com.agilebinary.a.a.a.e.c;
import com.agilebinary.a.a.a.f.d;
import com.agilebinary.a.a.a.f.g;
import com.agilebinary.a.a.a.f.h;
import com.agilebinary.a.a.a.f.i;
import com.agilebinary.a.a.a.h.j;
import com.agilebinary.a.a.a.h.m;

public class f extends c {
    public f() {
        super(null, null);
    }

    public f(j jVar) {
        super(jVar, null);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.a.a.a.e.b.b(java.lang.String, boolean):com.agilebinary.a.a.a.e.b
     arg types: [java.lang.String, int]
     candidates:
      com.agilebinary.a.a.a.e.b.b(java.lang.String, int):com.agilebinary.a.a.a.e.b
      com.agilebinary.a.a.a.e.b.b(java.lang.String, boolean):com.agilebinary.a.a.a.e.b */
    /* access modifiers changed from: protected */
    public final b e() {
        c cVar = new c();
        aa aaVar = aa.d;
        if (cVar == null) {
            throw new IllegalArgumentException("HTTP parameters may not be null");
        }
        cVar.a("http.protocol.version", aaVar);
        if (cVar == null) {
            throw new IllegalArgumentException("HTTP parameters may not be null");
        }
        cVar.a("http.protocol.content-charset", "ISO-8859-1");
        if (cVar == null) {
            throw new IllegalArgumentException("HTTP parameters may not be null");
        }
        cVar.b("http.tcp.nodelay", true);
        if (cVar == null) {
            throw new IllegalArgumentException("HTTP parameters may not be null");
        }
        cVar.b("http.socket.buffer-size", 8192);
        com.agilebinary.a.a.a.i.c a = com.agilebinary.a.a.a.i.c.a("org.apache.http.client", getClass().getClassLoader());
        String str = "Apache-HttpClient/" + (a != null ? a.a() : "UNAVAILABLE") + " (java 1.5)";
        if (cVar == null) {
            throw new IllegalArgumentException("HTTP parameters may not be null");
        }
        cVar.a("http.useragent", str);
        return cVar;
    }

    /* access modifiers changed from: protected */
    public final i f() {
        d dVar = new d();
        dVar.a("http.scheme-registry", v().a());
        dVar.a("http.authscheme-registry", w());
        dVar.a("http.cookiespec-registry", x());
        dVar.a("http.cookie-store", y());
        dVar.a("http.auth.credentials-provider", z());
        return dVar;
    }

    /* access modifiers changed from: protected */
    public final com.agilebinary.a.a.a.f.b g() {
        return new com.agilebinary.a.a.a.f.b();
    }

    /* JADX WARN: Type inference failed for: r1v14, types: [java.lang.Object] */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.agilebinary.a.a.a.h.j h() {
        /*
            r6 = this;
            com.agilebinary.a.a.a.h.b.h r2 = new com.agilebinary.a.a.a.h.b.h
            r2.<init>()
            com.agilebinary.a.a.a.h.b.g r1 = new com.agilebinary.a.a.a.h.b.g
            java.lang.String r3 = "http"
            r4 = 80
            com.agilebinary.a.a.a.h.b.e r5 = com.agilebinary.a.a.a.h.b.e.b()
            r1.<init>(r3, r4, r5)
            r2.a(r1)
            com.agilebinary.a.a.a.h.b.g r1 = new com.agilebinary.a.a.a.h.b.g
            java.lang.String r3 = "https"
            r4 = 443(0x1bb, float:6.21E-43)
            com.agilebinary.a.a.a.h.d.f r5 = com.agilebinary.a.a.a.h.d.f.b()
            r1.<init>(r3, r4, r5)
            r2.a(r1)
            com.agilebinary.a.a.a.e.b r1 = r6.u()
            r3 = 0
            java.lang.String r4 = "http.connection-manager.factory-class-name"
            java.lang.Object r6 = r1.a(r4)
            java.lang.String r6 = (java.lang.String) r6
            if (r6 == 0) goto L_0x007e
            java.lang.Class r1 = java.lang.Class.forName(r6)     // Catch:{ ClassNotFoundException -> 0x0048, IllegalAccessException -> 0x0062, InstantiationException -> 0x006d }
            java.lang.Object r1 = r1.newInstance()     // Catch:{ ClassNotFoundException -> 0x0048, IllegalAccessException -> 0x0062, InstantiationException -> 0x006d }
            r0 = r1
            com.agilebinary.a.a.a.h.b.b r0 = (com.agilebinary.a.a.a.h.b.b) r0     // Catch:{ ClassNotFoundException -> 0x0048, IllegalAccessException -> 0x0062, InstantiationException -> 0x006d }
            r6 = r0
            r1 = r6
        L_0x0041:
            if (r1 == 0) goto L_0x0078
            com.agilebinary.a.a.a.h.j r1 = r1.b()
        L_0x0047:
            return r1
        L_0x0048:
            r1 = move-exception
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Invalid class name: "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r2 = r2.append(r6)
            java.lang.String r2 = r2.toString()
            r1.<init>(r2)
            throw r1
        L_0x0062:
            r1 = move-exception
            java.lang.IllegalAccessError r2 = new java.lang.IllegalAccessError
            java.lang.String r1 = r1.getMessage()
            r2.<init>(r1)
            throw r2
        L_0x006d:
            r1 = move-exception
            java.lang.InstantiationError r2 = new java.lang.InstantiationError
            java.lang.String r1 = r1.getMessage()
            r2.<init>(r1)
            throw r2
        L_0x0078:
            com.agilebinary.a.a.a.c.b.i r1 = new com.agilebinary.a.a.a.c.b.i
            r1.<init>(r2)
            goto L_0x0047
        L_0x007e:
            r1 = r3
            goto L_0x0041
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.a.a.a.c.d.f.h():com.agilebinary.a.a.a.h.j");
    }

    /* access modifiers changed from: protected */
    public final a i() {
        return new a();
    }

    /* access modifiers changed from: protected */
    public final com.agilebinary.a.a.a.k.d j() {
        com.agilebinary.a.a.a.k.d dVar = new com.agilebinary.a.a.a.k.d();
        dVar.a("best-match", new t());
        dVar.a("compatibility", new v());
        dVar.a("netscape", new s());
        dVar.a("rfc2109", new e());
        dVar.a("rfc2965", new com.agilebinary.a.a.a.c.a.i());
        return dVar;
    }

    /* access modifiers changed from: protected */
    public final com.agilebinary.a.a.a.s k() {
        return new com.agilebinary.a.a.a.c.c();
    }

    /* access modifiers changed from: protected */
    public final m l() {
        return new m();
    }

    /* access modifiers changed from: protected */
    public final h m() {
        h hVar = new h();
        hVar.a((ab) new com.agilebinary.a.a.a.d.b.h());
        hVar.a((ab) new com.agilebinary.a.a.a.f.c());
        hVar.a((ab) new g());
        hVar.a((ab) new com.agilebinary.a.a.a.d.b.e());
        hVar.a((ab) new com.agilebinary.a.a.a.f.f());
        hVar.a((ab) new com.agilebinary.a.a.a.f.a());
        hVar.a((ab) new com.agilebinary.a.a.a.d.b.d());
        hVar.a(new com.agilebinary.a.a.a.d.b.c());
        hVar.a((ab) new com.agilebinary.a.a.a.d.b.g());
        hVar.a(new com.agilebinary.a.a.a.d.b.b());
        hVar.a((ab) new com.agilebinary.a.a.a.d.b.a());
        hVar.a((ab) new com.agilebinary.a.a.a.d.b.f());
        return hVar;
    }

    /* access modifiers changed from: protected */
    public final com.agilebinary.a.a.a.d.j n() {
        return new com.agilebinary.a.a.a.d.j();
    }

    /* access modifiers changed from: protected */
    public final com.agilebinary.a.a.a.d.b o() {
        return new b();
    }

    /* access modifiers changed from: protected */
    public final com.agilebinary.a.a.a.d.b p() {
        return new a();
    }

    /* access modifiers changed from: protected */
    public final com.agilebinary.a.a.a.d.d q() {
        return new l();
    }

    /* access modifiers changed from: protected */
    public final com.agilebinary.a.a.a.d.e r() {
        return new k();
    }

    /* access modifiers changed from: protected */
    public final com.agilebinary.a.a.a.h.c.h s() {
        return new com.agilebinary.a.a.a.c.b.g(v().a());
    }

    /* access modifiers changed from: protected */
    public final com.agilebinary.a.a.a.d.f t() {
        return new com.agilebinary.a.a.a.d.f();
    }
}
