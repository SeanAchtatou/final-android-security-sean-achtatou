package com.agilebinary.a.a.a.c.a;

import com.agilebinary.a.a.a.k.b;
import com.agilebinary.a.a.a.k.m;
import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class d implements b, m, Serializable, Cloneable {
    private final String a;
    private Map b;
    private String c;
    private String d;
    private Date e;
    private String f;
    private boolean g;
    private int h;

    public d(String str, String str2) {
        if (str == null) {
            throw new IllegalArgumentException("Name may not be null");
        }
        this.a = str;
        this.b = new HashMap();
        this.c = str2;
    }

    public final String a() {
        return this.a;
    }

    public final void a(int i) {
        this.h = i;
    }

    public final void a(String str) {
        if (str != null) {
            this.d = str.toLowerCase(Locale.ENGLISH);
        } else {
            this.d = null;
        }
    }

    public final void a(String str, String str2) {
        this.b.put(str, str2);
    }

    public final void a(Date date) {
        this.e = date;
    }

    public final void a(boolean z) {
        this.g = true;
    }

    public final String b() {
        return this.c;
    }

    public final void b(String str) {
        this.f = str;
    }

    public boolean b(Date date) {
        if (date != null) {
            return this.e != null && this.e.getTime() <= date.getTime();
        }
        throw new IllegalArgumentException("Date may not be null");
    }

    public final String c() {
        return this.d;
    }

    public final String c(String str) {
        return (String) this.b.get(str);
    }

    public Object clone() {
        d dVar = (d) super.clone();
        dVar.b = new HashMap(this.b);
        return dVar;
    }

    public final String d() {
        return this.f;
    }

    public final boolean d(String str) {
        return this.b.get(str) != null;
    }

    public final boolean e() {
        return this.g;
    }

    public int[] f() {
        return null;
    }

    public final int g() {
        return this.h;
    }

    public String toString() {
        return "[version: " + Integer.toString(this.h) + "]" + "[name: " + this.a + "]" + "[value: " + this.c + "]" + "[domain: " + this.d + "]" + "[path: " + this.f + "]" + "[expiry: " + this.e + "]";
    }
}
