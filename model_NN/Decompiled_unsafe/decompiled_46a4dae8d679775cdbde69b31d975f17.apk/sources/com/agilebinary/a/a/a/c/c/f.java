package com.agilebinary.a.a.a.c.c;

import com.agilebinary.a.a.a.b.r;
import com.agilebinary.a.a.a.b.s;
import com.agilebinary.a.a.a.e.b;
import com.agilebinary.a.a.a.j.a;
import com.agilebinary.a.a.a.j.d;
import com.agilebinary.a.a.a.l;
import com.agilebinary.a.a.a.t;
import com.agilebinary.a.a.a.u;
import com.agilebinary.a.a.a.x;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public abstract class f implements d {
    protected final s a;
    private final a b;
    private final int c;
    private final int d;
    private final List e;
    private int f;
    private x g;

    public f(a aVar, s sVar, b bVar) {
        if (aVar == null) {
            throw new IllegalArgumentException("Session input buffer may not be null");
        } else if (bVar == null) {
            throw new IllegalArgumentException("HTTP parameters may not be null");
        } else {
            this.b = aVar;
            this.c = bVar.a("http.connection.max-header-count", -1);
            this.d = bVar.a("http.connection.max-line-length", -1);
            this.a = sVar != null ? sVar : r.a;
            this.e = new ArrayList();
            this.f = 0;
        }
    }

    public static t[] a(a aVar, int i, int i2, s sVar, List list) {
        if (aVar == null) {
            throw new IllegalArgumentException("Session input buffer may not be null");
        } else if (sVar == null) {
            throw new IllegalArgumentException("Line parser may not be null");
        } else if (list == null) {
            throw new IllegalArgumentException("Header line list may not be null");
        } else {
            com.agilebinary.a.a.a.i.b bVar = null;
            com.agilebinary.a.a.a.i.b bVar2 = null;
            while (true) {
                if (bVar2 == null) {
                    bVar2 = new com.agilebinary.a.a.a.i.b(64);
                } else {
                    bVar2.a();
                }
                if (aVar.a(bVar2) == -1 || bVar2.c() <= 0) {
                    t[] tVarArr = new t[list.size()];
                    int i3 = 0;
                } else {
                    if ((bVar2.a(0) == ' ' || bVar2.a(0) == 9) && bVar != null) {
                        int i4 = 0;
                        while (i4 < bVar2.c() && ((r3 = bVar2.a(i4)) == ' ' || r3 == 9)) {
                            i4++;
                        }
                        if (i2 <= 0 || ((bVar.c() + 1) + bVar2.c()) - i4 <= i2) {
                            bVar.a(' ');
                            bVar.a(bVar2, i4, bVar2.c() - i4);
                        } else {
                            throw new IOException("Maximum line length limit exceeded");
                        }
                    } else {
                        list.add(bVar2);
                        bVar = bVar2;
                        bVar2 = null;
                    }
                    if (i > 0 && list.size() >= i) {
                        throw new IOException("Maximum header count exceeded");
                    }
                }
            }
            t[] tVarArr2 = new t[list.size()];
            int i32 = 0;
            while (i32 < list.size()) {
                try {
                    tVarArr2[i32] = sVar.a((com.agilebinary.a.a.a.i.b) list.get(i32));
                    i32++;
                } catch (u e2) {
                    throw new l(e2.getMessage());
                }
            }
            return tVarArr2;
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public final x a() {
        switch (this.f) {
            case 0:
                try {
                    this.g = a(this.b);
                    this.f = 1;
                    break;
                } catch (u e2) {
                    throw new l(e2.getMessage(), e2);
                }
            case 1:
                break;
            default:
                throw new IllegalStateException("Inconsistent parser state");
        }
        this.g.a(a(this.b, this.c, this.d, this.a, this.e));
        x xVar = this.g;
        this.g = null;
        this.e.clear();
        this.f = 0;
        return xVar;
    }

    /* access modifiers changed from: protected */
    public abstract x a(a aVar);
}
