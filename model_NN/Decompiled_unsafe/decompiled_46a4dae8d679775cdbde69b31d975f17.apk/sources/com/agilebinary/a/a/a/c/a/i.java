package com.agilebinary.a.a.a.c.a;

import com.agilebinary.a.a.a.e.b;
import com.agilebinary.a.a.a.k.g;
import com.agilebinary.a.a.a.k.j;
import java.util.Collection;

public final class i implements g {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.a.a.a.e.b.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.agilebinary.a.a.a.e.b.a(java.lang.String, int):int
      com.agilebinary.a.a.a.e.b.a(java.lang.String, java.lang.Object):com.agilebinary.a.a.a.e.b
      com.agilebinary.a.a.a.e.b.a(java.lang.String, boolean):boolean */
    public final j a(b bVar) {
        if (bVar == null) {
            return new ae();
        }
        String[] strArr = null;
        Collection collection = (Collection) bVar.a("http.protocol.cookie-datepatterns");
        if (collection != null) {
            strArr = (String[]) collection.toArray(new String[collection.size()]);
        }
        return new ae(strArr, bVar.a("http.protocol.single-cookie-header", false));
    }
}
