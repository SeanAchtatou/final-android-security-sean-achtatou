package com.agilebinary.a.a.a.c.c;

import com.agilebinary.a.a.a.j.b;
import java.io.InterruptedIOException;
import java.net.Socket;

public final class c extends b implements b {
    private static final Class a = g();
    private final Socket b;
    private boolean c;

    public c(Socket socket, int i, com.agilebinary.a.a.a.e.b bVar) {
        if (socket == null) {
            throw new IllegalArgumentException("Socket may not be null");
        }
        this.b = socket;
        this.c = false;
        int receiveBufferSize = i < 0 ? socket.getReceiveBufferSize() : i;
        a(socket.getInputStream(), receiveBufferSize < 1024 ? 1024 : receiveBufferSize, bVar);
    }

    private static Class g() {
        try {
            return Class.forName("java.net.SocketTimeoutException");
        } catch (ClassNotFoundException e) {
            return null;
        }
    }

    public final boolean a(int i) {
        boolean c2 = c();
        if (!c2) {
            int soTimeout = this.b.getSoTimeout();
            try {
                this.b.setSoTimeout(i);
                b();
                c2 = c();
            } catch (InterruptedIOException e) {
                if (!(a != null ? a.isInstance(e) : true)) {
                    throw e;
                }
            } finally {
                this.b.setSoTimeout(soTimeout);
            }
        }
        return c2;
    }

    /* access modifiers changed from: protected */
    public final int b() {
        int b2 = super.b();
        this.c = b2 == -1;
        return b2;
    }

    public final boolean f() {
        return this.c;
    }
}
