package com.agilebinary.a.a.a.h.d;

import com.agilebinary.a.a.a.c.e.a;
import com.agilebinary.a.a.a.h.b.b;
import com.agilebinary.a.a.a.h.b.j;
import com.agilebinary.a.a.a.h.h;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketTimeoutException;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;

public final class f implements com.agilebinary.a.a.a.h.b.f, j {
    private static final f a = new f();
    private final SSLSocketFactory b = HttpsURLConnection.getDefaultSSLSocketFactory();
    private final b c = null;
    private volatile b d = null;

    static {
        new e();
        new c();
        new a();
    }

    private f() {
    }

    public static f b() {
        return a;
    }

    public final Socket a() {
        return new Socket();
    }

    public final Socket a(Socket socket, String str, int i, InetAddress inetAddress, int i2, com.agilebinary.a.a.a.e.b bVar) {
        InetSocketAddress inetSocketAddress = null;
        if (inetAddress != null || i2 > 0) {
            inetSocketAddress = new InetSocketAddress(inetAddress, i2 < 0 ? 0 : i2);
        }
        return a(socket, new InetSocketAddress(InetAddress.getByName(str), i), inetSocketAddress, bVar);
    }

    public final Socket a(Socket socket, String str, int i, boolean z) {
        return (SSLSocket) this.b.createSocket(socket, str, i, z);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{javax.net.ssl.SSLSocketFactory.createSocket(java.net.Socket, java.lang.String, int, boolean):java.net.Socket throws java.io.IOException}
     arg types: [java.net.Socket, java.lang.String, int, int]
     candidates:
      ClspMth{javax.net.SocketFactory.createSocket(java.lang.String, int, java.net.InetAddress, int):java.net.Socket throws java.io.IOException, java.net.UnknownHostException}
      ClspMth{javax.net.SocketFactory.createSocket(java.net.InetAddress, int, java.net.InetAddress, int):java.net.Socket throws java.io.IOException}
      ClspMth{javax.net.ssl.SSLSocketFactory.createSocket(java.net.Socket, java.lang.String, int, boolean):java.net.Socket throws java.io.IOException} */
    public final Socket a(Socket socket, InetSocketAddress inetSocketAddress, InetSocketAddress inetSocketAddress2, com.agilebinary.a.a.a.e.b bVar) {
        if (inetSocketAddress == null) {
            throw new IllegalArgumentException("Remote address may not be null");
        } else if (bVar == null) {
            throw new IllegalArgumentException("HTTP parameters may not be null");
        } else {
            Socket socket2 = socket != null ? socket : new Socket();
            if (inetSocketAddress2 != null) {
                socket2.setReuseAddress(a.b(bVar));
                socket2.bind(inetSocketAddress2);
            }
            int c2 = a.c(bVar);
            int a2 = a.a(bVar);
            try {
                socket2.connect(inetSocketAddress, c2);
                socket2.setSoTimeout(a2);
                return socket2 instanceof SSLSocket ? (SSLSocket) socket2 : (SSLSocket) this.b.createSocket(socket2, inetSocketAddress.getHostName(), inetSocketAddress.getPort(), true);
            } catch (SocketTimeoutException e) {
                throw new h("Connect to " + inetSocketAddress.getHostName() + "/" + inetSocketAddress.getAddress() + " timed out");
            }
        }
    }

    public final boolean a(Socket socket) {
        if (socket == null) {
            throw new IllegalArgumentException("Socket may not be null");
        } else if (!(socket instanceof SSLSocket)) {
            throw new IllegalArgumentException("Socket not created by this factory");
        } else if (!socket.isClosed()) {
            return true;
        } else {
            throw new IllegalArgumentException("Socket is closed");
        }
    }

    public final Socket a_(Socket socket, String str, int i, boolean z) {
        return a(socket, str, i, z);
    }

    public final Socket e_() {
        return new Socket();
    }
}
