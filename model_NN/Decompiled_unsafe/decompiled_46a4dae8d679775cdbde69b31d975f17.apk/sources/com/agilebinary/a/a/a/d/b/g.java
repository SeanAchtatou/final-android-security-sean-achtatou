package com.agilebinary.a.a.a.d.b;

import com.agilebinary.a.a.a.a.c;
import com.agilebinary.a.a.a.a.e;
import com.agilebinary.a.a.a.a.f;
import com.agilebinary.a.a.a.ab;
import com.agilebinary.a.a.a.b;
import com.agilebinary.a.a.a.f.i;
import com.agilebinary.a.a.b.a.a;
import org.apache.commons.logging.Log;

public final class g implements ab {
    private final Log a = a.a(getClass());

    private void a(b bVar, f fVar, e eVar, com.agilebinary.a.a.a.d.e eVar2) {
        String b = fVar.b();
        if (this.a.isDebugEnabled()) {
            this.a.debug("Re-using cached '" + b + "' auth scheme for " + bVar);
        }
        com.agilebinary.a.a.a.h.b.b a2 = eVar2.a(new c(bVar.a(), bVar.b(), b));
        if (a2 != null) {
            eVar.a(fVar);
            eVar.a(a2);
            return;
        }
        this.a.debug("No credentials for preemptive authentication");
    }

    public final void a(com.agilebinary.a.a.a.f fVar, i iVar) {
        f a2;
        f a3;
        if (fVar == null) {
            throw new IllegalArgumentException("HTTP request may not be null");
        } else if (iVar == null) {
            throw new IllegalArgumentException("HTTP context may not be null");
        } else {
            com.agilebinary.a.a.a.d.g gVar = (com.agilebinary.a.a.a.d.g) iVar.a("http.auth.auth-cache");
            if (gVar == null) {
                iVar.a("http.auth.auth-cache", new com.agilebinary.a.a.a.c.d.g());
                return;
            }
            com.agilebinary.a.a.a.d.e eVar = (com.agilebinary.a.a.a.d.e) iVar.a("http.auth.credentials-provider");
            b bVar = (b) iVar.a("http.target_host");
            if (!(bVar == null || (a3 = gVar.a(bVar)) == null)) {
                a(bVar, a3, (e) iVar.a("http.auth.target-scope"), eVar);
            }
            b bVar2 = (b) iVar.a("http.proxy_host");
            if (bVar2 != null && (a2 = gVar.a(bVar2)) != null) {
                a(bVar2, a2, (e) iVar.a("http.auth.proxy-scope"), eVar);
            }
        }
    }
}
