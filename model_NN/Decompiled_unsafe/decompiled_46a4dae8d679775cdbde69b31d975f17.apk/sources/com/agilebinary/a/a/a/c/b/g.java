package com.agilebinary.a.a.a.c.b;

import com.agilebinary.a.a.a.b;
import com.agilebinary.a.a.a.c;
import com.agilebinary.a.a.a.f;
import com.agilebinary.a.a.a.h.a.a;
import com.agilebinary.a.a.a.h.c.h;
import com.agilebinary.a.a.a.i.d;
import java.io.InputStream;
import java.net.InetAddress;

public class g implements h {
    private com.agilebinary.a.a.a.h.b.h a;

    private g() {
    }

    public g(com.agilebinary.a.a.a.h.b.h hVar) {
        if (hVar == null) {
            throw new IllegalArgumentException("SchemeRegistry must not be null.");
        }
        this.a = hVar;
    }

    public static void a(c cVar) {
        InputStream f;
        if (cVar != null && cVar.g() && (f = cVar.f()) != null) {
            f.close();
        }
    }

    /* JADX INFO: finally extract failed */
    public static byte[] b(c cVar) {
        if (cVar == null) {
            throw new IllegalArgumentException("HTTP entity may not be null");
        }
        InputStream f = cVar.f();
        if (f == null) {
            return null;
        }
        if (cVar.c() > 2147483647L) {
            throw new IllegalArgumentException("HTTP entity too large to be buffered in memory");
        }
        int c = (int) cVar.c();
        if (c < 0) {
            c = 4096;
        }
        d dVar = new d(c);
        try {
            byte[] bArr = new byte[4096];
            while (true) {
                int read = f.read(bArr);
                if (read != -1) {
                    dVar.a(bArr, 0, read);
                } else {
                    f.close();
                    return dVar.b();
                }
            }
        } catch (Throwable th) {
            f.close();
            throw th;
        }
    }

    public final com.agilebinary.a.a.a.h.c.c a(b bVar, f fVar) {
        if (fVar == null) {
            throw new IllegalStateException("Request must not be null.");
        }
        com.agilebinary.a.a.a.h.c.c b = a.b(fVar.g());
        if (b != null) {
            return b;
        }
        if (bVar == null) {
            throw new IllegalStateException("Target host must not be null.");
        }
        InetAddress c = a.c(fVar.g());
        b a2 = a.a(fVar.g());
        boolean d = this.a.a(bVar.c()).d();
        return a2 == null ? new com.agilebinary.a.a.a.h.c.c(bVar, c, d) : new com.agilebinary.a.a.a.h.c.c(bVar, c, a2, d);
    }
}
