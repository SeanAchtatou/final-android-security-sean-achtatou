package com.agilebinary.a.a.a.c.b;

import com.agilebinary.a.a.a.c.e;
import com.agilebinary.a.a.a.e.b;
import com.agilebinary.a.a.a.f;
import com.agilebinary.a.a.a.f.i;
import com.agilebinary.a.a.a.h.g;
import com.agilebinary.a.a.a.j;
import com.agilebinary.a.a.a.j.d;
import com.agilebinary.a.a.a.t;
import com.agilebinary.a.a.b.a.a;
import java.io.IOException;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.logging.Log;

public final class h extends e implements i, g {
    private final Log a = a.a(getClass());
    private final Log b = a.a("org.apache.http.headers");
    private final Log c = a.a("org.apache.http.wire");
    private volatile Socket d;
    private boolean e;
    private volatile boolean f;
    private final Map g = new HashMap();

    /* access modifiers changed from: protected */
    public final com.agilebinary.a.a.a.j.a a(Socket socket, int i, b bVar) {
        com.agilebinary.a.a.a.j.a a2 = super.a(socket, i == -1 ? 8192 : i, bVar);
        return this.c.isDebugEnabled() ? new j(a2, new p(this.c), com.agilebinary.mobilemonitor.device.a.b.a.a.a.a(bVar)) : a2;
    }

    /* access modifiers changed from: protected */
    public final d a(com.agilebinary.a.a.a.j.a aVar, com.agilebinary.a.a.a.i iVar, b bVar) {
        return new o(aVar, null, iVar, bVar);
    }

    public final Object a(String str) {
        return this.g.get(str);
    }

    public final void a(f fVar) {
        if (this.a.isDebugEnabled()) {
            this.a.debug("Sending request: " + fVar.a());
        }
        super.a(fVar);
        if (this.b.isDebugEnabled()) {
            this.b.debug(">> " + fVar.a().toString());
            t[] e2 = fVar.e();
            int length = e2.length;
            for (int i = 0; i < length; i++) {
                this.b.debug(">> " + e2[i].toString());
            }
        }
    }

    public final void a(String str, Object obj) {
        this.g.put(str, obj);
    }

    public final void a(Socket socket, com.agilebinary.a.a.a.b bVar) {
        f();
        this.d = socket;
        if (this.f) {
            socket.close();
            throw new IOException("Connection already shutdown");
        }
    }

    public final void a(Socket socket, com.agilebinary.a.a.a.b bVar, boolean z, b bVar2) {
        a();
        if (bVar == null) {
            throw new IllegalArgumentException("Target host must not be null.");
        } else if (bVar2 == null) {
            throw new IllegalArgumentException("Parameters must not be null.");
        } else {
            if (socket != null) {
                this.d = socket;
                a(socket, bVar2);
            }
            this.e = z;
        }
    }

    public final void a(boolean z, b bVar) {
        f();
        if (bVar == null) {
            throw new IllegalArgumentException("Parameters must not be null.");
        }
        this.e = z;
        a(this.d, bVar);
    }

    /* access modifiers changed from: protected */
    public final com.agilebinary.a.a.a.j.e b(Socket socket, int i, b bVar) {
        com.agilebinary.a.a.a.j.e b2 = super.b(socket, i == -1 ? 8192 : i, bVar);
        return this.c.isDebugEnabled() ? new com.agilebinary.a.a.a.d.a.a(b2, new p(this.c), com.agilebinary.mobilemonitor.device.a.b.a.a.a.a(bVar)) : b2;
    }

    public final j d() {
        j d2 = super.d();
        if (this.a.isDebugEnabled()) {
            this.a.debug("Receiving response: " + d2.a());
        }
        if (this.b.isDebugEnabled()) {
            this.b.debug("<< " + d2.a().toString());
            t[] e2 = d2.e();
            int length = e2.length;
            for (int i = 0; i < length; i++) {
                this.b.debug("<< " + e2[i].toString());
            }
        }
        return d2;
    }

    public final boolean h_() {
        return this.e;
    }

    public final Socket i_() {
        return this.d;
    }

    public final void k() {
        try {
            super.k();
            this.a.debug("Connection closed");
        } catch (IOException e2) {
            this.a.debug("I/O error closing connection", e2);
        }
    }

    public final void m() {
        this.f = true;
        try {
            super.m();
            this.a.debug("Connection shut down");
            Socket socket = this.d;
            if (socket != null) {
                socket.close();
            }
        } catch (IOException e2) {
            this.a.debug("I/O error shutting down connection", e2);
        }
    }
}
