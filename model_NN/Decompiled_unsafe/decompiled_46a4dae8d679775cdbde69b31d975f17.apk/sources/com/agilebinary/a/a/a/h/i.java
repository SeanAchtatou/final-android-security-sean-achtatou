package com.agilebinary.a.a.a.h;

import com.agilebinary.a.a.a.c;
import com.agilebinary.a.a.a.c.b.g;
import com.agilebinary.a.a.a.g.b;
import java.io.InputStream;
import java.io.OutputStream;

public final class i extends b implements d, e {
    private a b;
    private boolean c;

    public i(c cVar, a aVar, boolean z) {
        super(cVar);
        if (aVar == null) {
            throw new IllegalArgumentException("Connection may not be null.");
        }
        this.b = aVar;
        this.c = z;
    }

    private void h() {
        if (this.b != null) {
            try {
                if (this.c) {
                    g.a(this.a);
                    this.b.g();
                }
            } finally {
                i();
            }
        }
    }

    private void i() {
        if (this.b != null) {
            try {
                this.b.b_();
            } finally {
                this.b = null;
            }
        }
    }

    public final void a(OutputStream outputStream) {
        super.a(outputStream);
        h();
    }

    public final boolean a() {
        return false;
    }

    /* JADX INFO: finally extract failed */
    public final boolean a(InputStream inputStream) {
        try {
            if (this.c && this.b != null) {
                inputStream.close();
                this.b.g();
            }
            i();
            return false;
        } catch (Throwable th) {
            i();
            throw th;
        }
    }

    public final void b() {
        if (this.b != null) {
            try {
                this.b.b();
            } finally {
                this.b = null;
            }
        }
    }

    /* JADX INFO: finally extract failed */
    public final boolean b(InputStream inputStream) {
        try {
            if (this.c && this.b != null) {
                inputStream.close();
                this.b.g();
            }
            i();
            return false;
        } catch (Throwable th) {
            i();
            throw th;
        }
    }

    public final void b_() {
        h();
    }

    public final InputStream f() {
        return new c(this.a.f(), this);
    }

    public final boolean m_() {
        if (this.b == null) {
            return false;
        }
        this.b.b();
        return false;
    }
}
