package com.agilebinary.a.a.a.c.c;

import com.agilebinary.a.a.a.i.b;
import com.agilebinary.a.a.a.j.e;
import com.agilebinary.a.a.a.j.f;
import com.agilebinary.a.a.a.j.g;
import com.agilebinary.mobilemonitor.device.a.b.a.a.a;
import java.io.OutputStream;

public abstract class d implements e, g {
    private static final byte[] a = {13, 10};
    private OutputStream b;
    private com.agilebinary.a.a.a.i.d c;
    private String d = "US-ASCII";
    private boolean e = true;
    private int f = 512;
    private f g;

    private void a(byte[] bArr) {
        if (bArr != null) {
            a(bArr, 0, bArr.length);
        }
    }

    private void c() {
        int d2 = this.c.d();
        if (d2 > 0) {
            this.b.write(this.c.e(), 0, d2);
            this.c.a();
            this.g.a((long) d2);
        }
    }

    public final void a() {
        c();
        this.b.flush();
    }

    public final void a(int i) {
        if (this.c.g()) {
            c();
        }
        this.c.a(i);
    }

    public final void a(b bVar) {
        if (bVar != null) {
            if (this.e) {
                int i = 0;
                int c2 = bVar.c();
                while (c2 > 0) {
                    int min = Math.min(this.c.c() - this.c.d(), c2);
                    if (min > 0) {
                        com.agilebinary.a.a.a.i.d dVar = this.c;
                        if (bVar != null) {
                            dVar.a(bVar.b(), i, min);
                        }
                    }
                    if (this.c.g()) {
                        c();
                    }
                    i += min;
                    c2 -= min;
                }
            } else {
                a(bVar.toString().getBytes(this.d));
            }
            a(a);
        }
    }

    /* access modifiers changed from: protected */
    public final void a(OutputStream outputStream, int i, com.agilebinary.a.a.a.e.b bVar) {
        if (outputStream == null) {
            throw new IllegalArgumentException("Input stream may not be null");
        } else if (i <= 0) {
            throw new IllegalArgumentException("Buffer size may not be negative or zero");
        } else if (bVar == null) {
            throw new IllegalArgumentException("HTTP parameters may not be null");
        } else {
            this.b = outputStream;
            this.c = new com.agilebinary.a.a.a.i.d(i);
            this.d = a.a(bVar);
            this.e = this.d.equalsIgnoreCase("US-ASCII") || this.d.equalsIgnoreCase("ASCII");
            this.f = bVar.a("http.connection.min-chunk-limit", 512);
            this.g = new f();
        }
    }

    public final void a(String str) {
        if (str != null) {
            if (str.length() > 0) {
                a(str.getBytes(this.d));
            }
            a(a);
        }
    }

    public final void a(byte[] bArr, int i, int i2) {
        if (bArr != null) {
            if (i2 > this.f || i2 > this.c.c()) {
                c();
                this.b.write(bArr, i, i2);
                this.g.a((long) i2);
                return;
            }
            if (i2 > this.c.c() - this.c.d()) {
                c();
            }
            this.c.a(bArr, i, i2);
        }
    }

    public final f b() {
        return this.g;
    }

    public final int d_() {
        return this.c.d();
    }
}
