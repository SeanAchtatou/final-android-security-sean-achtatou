package com.agilebinary.a.a.a.a;

import java.util.Locale;

public final class c {
    private static String a = null;
    private static String b = null;
    private static String c = null;
    private final String d;
    private final String e;
    private final String f;
    private final int g;

    static {
        new c(null, -1, null, null);
    }

    public c(String str, int i, String str2) {
        this(str, i, str2, null);
    }

    public c(String str, int i, String str2, String str3) {
        this.f = str == null ? null : str.toLowerCase(Locale.ENGLISH);
        this.g = i < 0 ? -1 : i;
        this.e = str2 == null ? null : str2;
        this.d = str3 == null ? null : str3.toUpperCase(Locale.ENGLISH);
    }

    public final int a(c cVar) {
        int i = 0;
        if (com.agilebinary.a.a.a.c.e.c.a(this.d, cVar.d)) {
            i = 0 + 1;
        } else if (!(this.d == null || cVar.d == null)) {
            return -1;
        }
        if (com.agilebinary.a.a.a.c.e.c.a(this.e, cVar.e)) {
            i += 2;
        } else if (!(this.e == null || cVar.e == null)) {
            return -1;
        }
        if (this.g == cVar.g) {
            i += 4;
        } else if (!(this.g == -1 || cVar.g == -1)) {
            return -1;
        }
        if (com.agilebinary.a.a.a.c.e.c.a(this.f, cVar.f)) {
            return i + 8;
        }
        if (this.f == null || cVar.f == null) {
            return i;
        }
        return -1;
    }

    public final boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof c)) {
            return super.equals(obj);
        }
        c cVar = (c) obj;
        return com.agilebinary.a.a.a.c.e.c.a(this.f, cVar.f) && this.g == cVar.g && com.agilebinary.a.a.a.c.e.c.a(this.e, cVar.e) && com.agilebinary.a.a.a.c.e.c.a(this.d, cVar.d);
    }

    public final int hashCode() {
        return com.agilebinary.a.a.a.c.e.c.a(com.agilebinary.a.a.a.c.e.c.a((com.agilebinary.a.a.a.c.e.c.a(17, this.f) * 37) + this.g, this.e), this.d);
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder();
        if (this.d != null) {
            sb.append(this.d.toUpperCase(Locale.ENGLISH));
            sb.append(' ');
        }
        if (this.e != null) {
            sb.append('\'');
            sb.append(this.e);
            sb.append('\'');
        } else {
            sb.append("<any realm>");
        }
        if (this.f != null) {
            sb.append('@');
            sb.append(this.f);
            if (this.g >= 0) {
                sb.append(':');
                sb.append(this.g);
            }
        }
        return sb.toString();
    }
}
