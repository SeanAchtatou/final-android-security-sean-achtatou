package com.agilebinary.a.a.a.g;

import com.agilebinary.a.a.a.c;
import com.agilebinary.a.a.a.t;

public abstract class f implements c {
    private t a;
    private t b;
    private boolean c;

    protected f() {
    }

    public final void a(t tVar) {
        this.a = tVar;
    }

    public final void a(boolean z) {
        this.c = z;
    }

    public final void b(t tVar) {
        this.b = tVar;
    }

    public final t d() {
        return this.a;
    }

    public final t e() {
        return this.b;
    }

    public final boolean n_() {
        return this.c;
    }
}
