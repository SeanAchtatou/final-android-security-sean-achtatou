package com.agilebinary.a.a.a.f;

import java.util.HashMap;
import java.util.Map;

public final class d implements i {
    private final i a;
    private Map b;

    public d() {
        this(null);
    }

    private d(i iVar) {
        this.b = null;
        this.a = null;
    }

    public final Object a(String str) {
        if (str == null) {
            throw new IllegalArgumentException("Id may not be null");
        }
        Object obj = null;
        if (this.b != null) {
            obj = this.b.get(str);
        }
        return (obj != null || this.a == null) ? obj : this.a.a(str);
    }

    public final void a(String str, Object obj) {
        if (str == null) {
            throw new IllegalArgumentException("Id may not be null");
        }
        if (this.b == null) {
            this.b = new HashMap();
        }
        this.b.put(str, obj);
    }
}
