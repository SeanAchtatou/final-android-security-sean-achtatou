package com.agilebinary.a.a.a.c.a;

import com.agilebinary.a.a.a.b.i;
import com.agilebinary.a.a.a.i.b;
import com.agilebinary.a.a.a.k.c;
import com.agilebinary.a.a.a.k.e;
import com.agilebinary.a.a.a.k.f;
import com.agilebinary.a.a.a.m;
import com.agilebinary.a.a.a.r;
import com.agilebinary.a.a.a.t;
import java.util.ArrayList;
import java.util.List;

public final class q extends ah {
    private final String[] a;

    public q() {
        this(null);
    }

    public q(String[] strArr) {
        if (strArr != null) {
            this.a = (String[]) strArr.clone();
        } else {
            this.a = new String[]{"EEE, dd-MMM-yy HH:mm:ss z"};
        }
        a("path", new m());
        a("domain", new ab());
        a("max-age", new ac());
        a("secure", new h());
        a("comment", new o());
        a("expires", new j(this.a));
    }

    public final int a() {
        return 0;
    }

    public final List a(t tVar, f fVar) {
        i iVar;
        b bVar;
        if (tVar == null) {
            throw new IllegalArgumentException("Header may not be null");
        } else if (fVar == null) {
            throw new IllegalArgumentException("Cookie origin may not be null");
        } else if (!tVar.a().equalsIgnoreCase("Set-Cookie")) {
            throw new e("Unrecognized cookie header '" + tVar.toString() + "'");
        } else {
            aa aaVar = aa.a;
            if (tVar instanceof r) {
                bVar = ((r) tVar).e();
                iVar = new i(((r) tVar).d(), bVar.c());
            } else {
                String b = tVar.b();
                if (b == null) {
                    throw new e("Header value is null");
                }
                b bVar2 = new b(b.length());
                bVar2.a(b);
                b bVar3 = bVar2;
                iVar = new i(0, bVar2.c());
                bVar = bVar3;
            }
            return a(new m[]{aaVar.a(bVar, iVar)}, fVar);
        }
    }

    public final List a(List list) {
        if (list == null) {
            throw new IllegalArgumentException("List of cookies may not be null");
        } else if (list.isEmpty()) {
            throw new IllegalArgumentException("List of cookies may not be empty");
        } else {
            b bVar = new b(list.size() * 20);
            bVar.a("Cookie");
            bVar.a(": ");
            for (int i = 0; i < list.size(); i++) {
                c cVar = (c) list.get(i);
                if (i > 0) {
                    bVar.a("; ");
                }
                bVar.a(cVar.a());
                String b = cVar.b();
                if (b != null) {
                    bVar.a("=");
                    bVar.a(b);
                }
            }
            ArrayList arrayList = new ArrayList(1);
            arrayList.add(new com.agilebinary.a.a.a.b.e(bVar));
            return arrayList;
        }
    }

    public final t b() {
        return null;
    }

    public final String toString() {
        return "netscape";
    }
}
