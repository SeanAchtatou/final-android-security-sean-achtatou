package com.agilebinary.a.a.a.c.a;

import com.agilebinary.a.a.a.i.b;
import com.agilebinary.a.a.a.k.c;
import com.agilebinary.a.a.a.k.e;
import com.agilebinary.a.a.a.k.f;
import com.agilebinary.a.a.a.k.k;
import com.agilebinary.a.a.a.m;
import com.agilebinary.a.a.a.o;
import com.agilebinary.a.a.a.t;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public final class ae extends ag {
    public ae() {
        this(null, false);
    }

    public ae(String[] strArr, boolean z) {
        super(strArr, z);
        a("domain", new y());
        a("port", new g());
        a("commenturl", new z());
        a("discard", new b());
        a("version", new n());
    }

    private static f b(f fVar) {
        String a = fVar.a();
        boolean z = true;
        int i = 0;
        while (true) {
            if (i >= a.length()) {
                break;
            }
            char charAt = a.charAt(i);
            if (charAt == '.' || charAt == ':') {
                z = false;
            } else {
                i++;
            }
        }
        return z ? new f(a + ".local", fVar.c(), fVar.b(), fVar.d()) : fVar;
    }

    private List b(m[] mVarArr, f fVar) {
        ArrayList arrayList = new ArrayList(mVarArr.length);
        for (m mVar : mVarArr) {
            String a = mVar.a();
            String b = mVar.b();
            if (a == null || a.length() == 0) {
                throw new e("Cookie name may not be empty");
            }
            af afVar = new af(a, b);
            afVar.b(a(fVar));
            afVar.a(fVar.a());
            afVar.a(new int[]{fVar.c()});
            o[] c = mVar.c();
            HashMap hashMap = new HashMap(c.length);
            for (int length = c.length - 1; length >= 0; length--) {
                o oVar = c[length];
                hashMap.put(oVar.a().toLowerCase(Locale.ENGLISH), oVar);
            }
            for (Map.Entry value : hashMap.entrySet()) {
                o oVar2 = (o) value.getValue();
                String lowerCase = oVar2.a().toLowerCase(Locale.ENGLISH);
                afVar.a(lowerCase, oVar2.b());
                k a2 = a(lowerCase);
                if (a2 != null) {
                    a2.a(afVar, oVar2.b());
                }
            }
            arrayList.add(afVar);
        }
        return arrayList;
    }

    public final int a() {
        return 1;
    }

    public final List a(t tVar, f fVar) {
        if (tVar == null) {
            throw new IllegalArgumentException("Header may not be null");
        } else if (fVar == null) {
            throw new IllegalArgumentException("Cookie origin may not be null");
        } else if (!tVar.a().equalsIgnoreCase("Set-Cookie2")) {
            throw new e("Unrecognized cookie header '" + tVar.toString() + "'");
        } else {
            return b(tVar.c(), b(fVar));
        }
    }

    /* access modifiers changed from: protected */
    public final List a(m[] mVarArr, f fVar) {
        return b(mVarArr, b(fVar));
    }

    /* access modifiers changed from: protected */
    public final void a(b bVar, c cVar, int i) {
        String c;
        int[] f;
        super.a(bVar, cVar, i);
        if ((cVar instanceof com.agilebinary.a.a.a.k.m) && (c = ((com.agilebinary.a.a.a.k.m) cVar).c("port")) != null) {
            bVar.a("; $Port");
            bVar.a("=\"");
            if (c.trim().length() > 0 && (f = cVar.f()) != null) {
                int length = f.length;
                for (int i2 = 0; i2 < length; i2++) {
                    if (i2 > 0) {
                        bVar.a(",");
                    }
                    bVar.a(Integer.toString(f[i2]));
                }
            }
            bVar.a("\"");
        }
    }

    public final void a(c cVar, f fVar) {
        if (cVar == null) {
            throw new IllegalArgumentException("Cookie may not be null");
        } else if (fVar == null) {
            throw new IllegalArgumentException("Cookie origin may not be null");
        } else {
            super.a(cVar, b(fVar));
        }
    }

    public final t b() {
        b bVar = new b(40);
        bVar.a("Cookie2");
        bVar.a(": ");
        bVar.a("$Version=");
        bVar.a(Integer.toString(1));
        return new com.agilebinary.a.a.a.b.e(bVar);
    }

    public final boolean b(c cVar, f fVar) {
        if (cVar == null) {
            throw new IllegalArgumentException("Cookie may not be null");
        } else if (fVar != null) {
            return super.b(cVar, b(fVar));
        } else {
            throw new IllegalArgumentException("Cookie origin may not be null");
        }
    }

    public final String toString() {
        return "rfc2965";
    }
}
