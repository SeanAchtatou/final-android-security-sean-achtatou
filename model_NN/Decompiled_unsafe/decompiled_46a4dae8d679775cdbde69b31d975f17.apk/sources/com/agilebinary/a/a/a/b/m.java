package com.agilebinary.a.a.a.b;

import com.agilebinary.a.a.a.a;
import com.agilebinary.a.a.a.h;
import com.agilebinary.a.a.a.i.b;
import java.io.Serializable;

public final class m implements h, Serializable, Cloneable {
    private final a a;
    private final int b;
    private final String c;

    public m(a aVar, int i, String str) {
        if (aVar == null) {
            throw new IllegalArgumentException("Protocol version may not be null.");
        } else if (i < 0) {
            throw new IllegalArgumentException("Status code may not be negative.");
        } else {
            this.a = aVar;
            this.b = i;
            this.c = str;
        }
    }

    public final a a() {
        return this.a;
    }

    public final int b() {
        return this.b;
    }

    public final String c() {
        return this.c;
    }

    public final Object clone() {
        return super.clone();
    }

    public final String toString() {
        return q.a.a((b) null, this).toString();
    }
}
