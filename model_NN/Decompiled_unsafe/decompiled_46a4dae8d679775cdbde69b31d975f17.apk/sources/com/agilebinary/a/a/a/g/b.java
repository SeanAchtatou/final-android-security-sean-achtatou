package com.agilebinary.a.a.a.g;

import com.agilebinary.a.a.a.c;
import com.agilebinary.a.a.a.t;
import java.io.InputStream;
import java.io.OutputStream;

public class b implements c {
    protected c a;

    public b() {
    }

    public b(c cVar) {
        if (cVar == null) {
            throw new IllegalArgumentException("wrapped entity must not be null");
        }
        this.a = cVar;
    }

    public static int a(Object obj, String str) {
        return ((Integer) obj.getClass().getMethod(str, new Class[0]).invoke(obj, new Object[0])).intValue();
    }

    public void a(OutputStream outputStream) {
        this.a.a(outputStream);
    }

    public boolean a() {
        return this.a.a();
    }

    public long c() {
        return this.a.c();
    }

    public final t d() {
        return this.a.d();
    }

    public final t e() {
        return this.a.e();
    }

    public InputStream f() {
        return this.a.f();
    }

    public boolean g() {
        return this.a.g();
    }

    public boolean n_() {
        return this.a.n_();
    }
}
