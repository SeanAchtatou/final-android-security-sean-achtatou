package com.agilebinary.a.a.a.c.e;

import com.agilebinary.a.a.a.g.a;
import com.agilebinary.a.a.a.l;
import com.agilebinary.a.a.a.m;
import com.agilebinary.a.a.a.t;
import com.agilebinary.a.a.a.u;
import com.agilebinary.a.a.a.x;

public final class d implements a {
    public final long a(x xVar) {
        long j;
        if (xVar == null) {
            throw new IllegalArgumentException("HTTP message may not be null");
        }
        boolean b = xVar.g().b("http.protocol.strict-transfer-encoding");
        t c = xVar.c("Transfer-Encoding");
        t c2 = xVar.c("Content-Length");
        if (c != null) {
            try {
                m[] c3 = c.c();
                if (b) {
                    int i = 0;
                    while (i < c3.length) {
                        String a = c3[i].a();
                        if (a == null || a.length() <= 0 || a.equalsIgnoreCase("chunked") || a.equalsIgnoreCase("identity")) {
                            i++;
                        } else {
                            throw new l("Unsupported transfer encoding: " + a);
                        }
                    }
                }
                int length = c3.length;
                if ("identity".equalsIgnoreCase(c.b())) {
                    return -1;
                }
                if (length > 0 && "chunked".equalsIgnoreCase(c3[length - 1].a())) {
                    return -2;
                }
                if (!b) {
                    return -1;
                }
                throw new l("Chunk-encoding must be the last one applied");
            } catch (u e) {
                throw new l("Invalid Transfer-Encoding header value: " + c, e);
            }
        } else if (c2 == null) {
            return -1;
        } else {
            t[] b2 = xVar.b("Content-Length");
            if (!b || b2.length <= 1) {
                int length2 = b2.length - 1;
                while (true) {
                    if (length2 < 0) {
                        j = -1;
                        break;
                    }
                    t tVar = b2[length2];
                    try {
                        j = Long.parseLong(tVar.b());
                        break;
                    } catch (NumberFormatException e2) {
                        if (b) {
                            throw new l("Invalid content length: " + tVar.b());
                        }
                        length2--;
                    }
                }
                if (j < 0) {
                    return -1;
                }
                return j;
            }
            throw new l("Multiple content length headers");
        }
    }
}
