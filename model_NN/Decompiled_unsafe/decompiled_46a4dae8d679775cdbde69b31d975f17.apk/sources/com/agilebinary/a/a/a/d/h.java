package com.agilebinary.a.a.a.d;

import java.io.IOException;

public final class h extends IOException {
    public h() {
    }

    public h(String str) {
        super(str);
    }

    public h(Throwable th) {
        initCause(th);
    }
}
