package com.agilebinary.a.a.a.c.a;

import com.agilebinary.a.a.a.e.b;
import com.agilebinary.a.a.a.k.g;
import com.agilebinary.a.a.a.k.j;
import java.util.Collection;

public final class v implements g {
    public final j a(b bVar) {
        if (bVar == null) {
            return new w();
        }
        String[] strArr = null;
        Collection collection = (Collection) bVar.a("http.protocol.cookie-datepatterns");
        if (collection != null) {
            strArr = (String[]) collection.toArray(new String[collection.size()]);
        }
        return new w(strArr);
    }
}
