package com.agilebinary.a.a.a.c.a;

import com.agilebinary.a.a.a.k.b;
import com.agilebinary.a.a.a.k.e;

public final class j extends x {
    private final String[] a;

    public j(String[] strArr) {
        if (strArr == null) {
            throw new IllegalArgumentException("Array of date patterns may not be null");
        }
        this.a = strArr;
    }

    public final void a(b bVar, String str) {
        if (bVar == null) {
            throw new IllegalArgumentException("Cookie may not be null");
        } else if (str == null) {
            throw new e("Missing value for expires attribute");
        } else {
            try {
                bVar.a(l.a(str, this.a));
            } catch (k e) {
                throw new e("Unable to parse expires attribute: " + str);
            }
        }
    }
}
