package com.agilebinary.a.a.a.c.e;

import com.agilebinary.a.a.a.c.c.g;
import com.agilebinary.a.a.a.c.c.i;
import com.agilebinary.a.a.a.c.c.j;
import com.agilebinary.a.a.a.g.a;
import com.agilebinary.a.a.a.j.e;
import com.agilebinary.a.a.a.x;
import java.io.OutputStream;

public class c {
    private final a a;

    private c() {
    }

    public c(a aVar) {
        if (aVar == null) {
            throw new IllegalArgumentException("Content length strategy may not be null");
        }
        this.a = aVar;
    }

    public static int a(int i, Object obj) {
        return (obj != null ? obj.hashCode() : 0) + (i * 37);
    }

    public static boolean a(Object obj, Object obj2) {
        return obj == null ? obj2 == null : obj.equals(obj2);
    }

    public final void a(e eVar, x xVar, com.agilebinary.a.a.a.c cVar) {
        if (eVar == null) {
            throw new IllegalArgumentException("Session output buffer may not be null");
        } else if (xVar == null) {
            throw new IllegalArgumentException("HTTP message may not be null");
        } else if (cVar == null) {
            throw new IllegalArgumentException("HTTP entity may not be null");
        } else {
            long a2 = this.a.a(xVar);
            OutputStream gVar = a2 == -2 ? new g(eVar) : a2 == -1 ? new j(eVar) : new i(eVar, a2);
            cVar.a(gVar);
            gVar.close();
        }
    }
}
