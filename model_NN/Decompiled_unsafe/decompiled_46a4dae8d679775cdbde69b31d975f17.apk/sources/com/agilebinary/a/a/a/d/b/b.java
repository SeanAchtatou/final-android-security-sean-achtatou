package com.agilebinary.a.a.a.d.b;

import com.agilebinary.a.a.a.a.e;
import com.agilebinary.a.a.a.a.f;
import com.agilebinary.a.a.a.ad;
import com.agilebinary.a.a.a.d.g;
import com.agilebinary.a.a.a.f.i;
import com.agilebinary.a.a.a.j;
import com.agilebinary.a.a.b.a.a;
import org.apache.commons.logging.Log;

public final class b implements ad {
    private final Log a = a.a(getClass());

    private void a(g gVar, com.agilebinary.a.a.a.b bVar, e eVar) {
        f c;
        if (eVar != null && (c = eVar.c()) != null && c.e()) {
            String b = c.b();
            if ((!b.equalsIgnoreCase("Basic") && !b.equalsIgnoreCase("Digest")) || eVar.e() == null) {
                return;
            }
            if (eVar.d() != null) {
                if (this.a.isDebugEnabled()) {
                    this.a.debug("Caching '" + b + "' auth scheme for " + bVar);
                }
                gVar.a(bVar, c);
                return;
            }
            gVar.b(bVar);
        }
    }

    public final void a(j jVar, i iVar) {
        if (jVar == null) {
            throw new IllegalArgumentException("HTTP request may not be null");
        } else if (iVar == null) {
            throw new IllegalArgumentException("HTTP context may not be null");
        } else {
            g gVar = (g) iVar.a("http.auth.auth-cache");
            if (gVar != null) {
                a(gVar, (com.agilebinary.a.a.a.b) iVar.a("http.target_host"), (e) iVar.a("http.auth.target-scope"));
                a(gVar, (com.agilebinary.a.a.a.b) iVar.a("http.proxy_host"), (e) iVar.a("http.auth.proxy-scope"));
            }
        }
    }
}
