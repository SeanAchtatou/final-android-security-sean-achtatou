package com.agilebinary.a.a.a.d.a;

import com.agilebinary.a.a.a.c.b.p;
import com.agilebinary.a.a.a.i.b;
import com.agilebinary.a.a.a.j.e;
import com.agilebinary.a.a.a.j.f;
import java.lang.reflect.InvocationTargetException;

public class a implements e {
    private final e a;
    private final p b;
    private final String c;

    private a() {
    }

    public a(e eVar, p pVar, String str) {
        this.a = eVar;
        this.b = pVar;
        this.c = str != null ? str : "ASCII";
    }

    public static Object a(Object obj) {
        if (obj == null) {
            return null;
        }
        if (obj instanceof Cloneable) {
            try {
                try {
                    return obj.getClass().getMethod("clone", null).invoke(obj, null);
                } catch (InvocationTargetException e) {
                    Throwable cause = e.getCause();
                    if (cause instanceof CloneNotSupportedException) {
                        throw ((CloneNotSupportedException) cause);
                    }
                    throw new Error("Unexpected exception", cause);
                } catch (IllegalAccessException e2) {
                    throw new IllegalAccessError(e2.getMessage());
                }
            } catch (NoSuchMethodException e3) {
                throw new NoSuchMethodError(e3.getMessage());
            }
        } else {
            throw new CloneNotSupportedException();
        }
    }

    public void a() {
        this.a.a();
    }

    public void a(int i) {
        this.a.a(i);
        if (this.b.a()) {
            this.b.a(new byte[]{(byte) i});
        }
    }

    public void a(b bVar) {
        this.a.a(bVar);
        if (this.b.a()) {
            this.b.a((new String(bVar.b(), 0, bVar.c()) + "\r\n").getBytes(this.c));
        }
    }

    public void a(String str) {
        this.a.a(str);
        if (this.b.a()) {
            this.b.a((str + "\r\n").getBytes(this.c));
        }
    }

    public void a(byte[] bArr, int i, int i2) {
        this.a.a(bArr, i, i2);
        if (this.b.a()) {
            this.b.a(bArr, i, i2);
        }
    }

    public f b() {
        return this.a.b();
    }
}
