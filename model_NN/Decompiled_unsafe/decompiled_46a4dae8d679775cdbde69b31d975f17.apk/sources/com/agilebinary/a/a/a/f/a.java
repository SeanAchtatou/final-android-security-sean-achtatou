package com.agilebinary.a.a.a.f;

import com.agilebinary.a.a.a.aa;
import com.agilebinary.a.a.a.ab;
import com.agilebinary.a.a.a.c;
import com.agilebinary.a.a.a.e.b;
import com.agilebinary.a.a.a.f;
import com.agilebinary.a.a.a.p;

public final class a implements ab {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.a.a.a.e.b.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.agilebinary.a.a.a.e.b.a(java.lang.String, int):int
      com.agilebinary.a.a.a.e.b.a(java.lang.String, java.lang.Object):com.agilebinary.a.a.a.e.b
      com.agilebinary.a.a.a.e.b.a(java.lang.String, boolean):boolean */
    public final void a(f fVar, i iVar) {
        c h;
        if (fVar == null) {
            throw new IllegalArgumentException("HTTP request may not be null");
        } else if ((fVar instanceof p) && (h = ((p) fVar).h()) != null && h.c() != 0) {
            com.agilebinary.a.a.a.a b = fVar.a().b();
            b g = fVar.g();
            if (g == null) {
                throw new IllegalArgumentException("HTTP parameters may not be null");
            } else if (g.a("http.protocol.expect-continue", false) && !b.a(aa.c)) {
                fVar.a("Expect", "100-continue");
            }
        }
    }
}
