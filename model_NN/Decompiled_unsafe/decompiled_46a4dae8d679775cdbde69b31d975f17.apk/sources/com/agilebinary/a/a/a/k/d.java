package com.agilebinary.a.a.a.k;

import com.agilebinary.a.a.a.e.b;
import java.util.Locale;
import java.util.concurrent.ConcurrentHashMap;

public final class d {
    private final ConcurrentHashMap a = new ConcurrentHashMap();

    public final j a(String str, b bVar) {
        if (str == null) {
            throw new IllegalArgumentException("Name may not be null");
        }
        g gVar = (g) this.a.get(str.toLowerCase(Locale.ENGLISH));
        if (gVar != null) {
            return gVar.a(bVar);
        }
        throw new IllegalStateException("Unsupported cookie spec: " + str);
    }

    public final void a(String str, g gVar) {
        if (str == null) {
            throw new IllegalArgumentException("Name may not be null");
        } else if (gVar == null) {
            throw new IllegalArgumentException("Cookie spec factory may not be null");
        } else {
            this.a.put(str.toLowerCase(Locale.ENGLISH), gVar);
        }
    }
}
