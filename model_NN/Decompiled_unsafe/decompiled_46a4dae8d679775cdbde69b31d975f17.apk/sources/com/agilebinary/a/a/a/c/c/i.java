package com.agilebinary.a.a.a.c.c;

import com.agilebinary.a.a.a.j.e;
import java.io.IOException;
import java.io.OutputStream;

public final class i extends OutputStream {
    private final e a;
    private final long b;
    private long c = 0;
    private boolean d = false;

    public i(e eVar, long j) {
        if (eVar == null) {
            throw new IllegalArgumentException("Session output buffer may not be null");
        } else if (j < 0) {
            throw new IllegalArgumentException("Content length may not be negative");
        } else {
            this.a = eVar;
            this.b = j;
        }
    }

    public final void close() {
        if (!this.d) {
            this.d = true;
            this.a.a();
        }
    }

    public final void flush() {
        this.a.a();
    }

    public final void write(int i) {
        if (this.d) {
            throw new IOException("Attempted write to closed stream.");
        } else if (this.c < this.b) {
            this.a.a(i);
            this.c++;
        }
    }

    public final void write(byte[] bArr) {
        write(bArr, 0, bArr.length);
    }

    public final void write(byte[] bArr, int i, int i2) {
        if (this.d) {
            throw new IOException("Attempted write to closed stream.");
        } else if (this.c < this.b) {
            long j = this.b - this.c;
            int i3 = ((long) i2) > j ? (int) j : i2;
            this.a.a(bArr, i, i3);
            this.c += (long) i3;
        }
    }
}
