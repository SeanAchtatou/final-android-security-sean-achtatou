package com.agilebinary.a.a.a.g;

import com.agilebinary.a.a.a.c;
import com.agilebinary.a.a.a.c.b.g;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.OutputStream;

public final class e extends b {
    private final byte[] b;

    public e(c cVar) {
        super(cVar);
        if (!cVar.a() || cVar.c() < 0) {
            this.b = g.b(cVar);
        } else {
            this.b = null;
        }
    }

    public final void a(OutputStream outputStream) {
        if (outputStream == null) {
            throw new IllegalArgumentException("Output stream may not be null");
        } else if (this.b != null) {
            outputStream.write(this.b);
        } else {
            this.a.a(outputStream);
        }
    }

    public final boolean a() {
        return true;
    }

    public final long c() {
        return this.b != null ? (long) this.b.length : this.a.c();
    }

    public final InputStream f() {
        return this.b != null ? new ByteArrayInputStream(this.b) : this.a.f();
    }

    public final boolean g() {
        return this.b == null && this.a.g();
    }

    public final boolean n_() {
        return this.b == null && this.a.n_();
    }
}
