package com.agilebinary.a.a.a.b;

import com.agilebinary.a.a.a.a;
import com.agilebinary.a.a.a.d;
import com.agilebinary.a.a.a.i.b;
import java.io.Serializable;

public final class p implements d, Serializable, Cloneable {
    private final a a;
    private final String b;
    private final String c;

    public p(String str, String str2, a aVar) {
        if (str == null) {
            throw new IllegalArgumentException("Method must not be null.");
        } else if (str2 == null) {
            throw new IllegalArgumentException("URI must not be null.");
        } else if (aVar == null) {
            throw new IllegalArgumentException("Protocol version must not be null.");
        } else {
            this.b = str;
            this.c = str2;
            this.a = aVar;
        }
    }

    public final String a() {
        return this.b;
    }

    public final a b() {
        return this.a;
    }

    public final String c() {
        return this.c;
    }

    public final Object clone() {
        return super.clone();
    }

    public final String toString() {
        return q.a.a((b) null, this).toString();
    }
}
