package com.agilebinary.a.a.a.b;

import com.agilebinary.a.a.a.i.b;

public final class i {
    private final int a;
    private final int b;
    private int c;

    public i(int i, int i2) {
        if (i < 0) {
            throw new IndexOutOfBoundsException("Lower bound cannot be negative");
        } else if (i > i2) {
            throw new IndexOutOfBoundsException("Lower bound cannot be greater then upper bound");
        } else {
            this.a = i;
            this.b = i2;
            this.c = i;
        }
    }

    public final int a() {
        return this.b;
    }

    public final void a(int i) {
        if (i < this.a) {
            throw new IndexOutOfBoundsException("pos: " + i + " < lowerBound: " + this.a);
        } else if (i > this.b) {
            throw new IndexOutOfBoundsException("pos: " + i + " > upperBound: " + this.b);
        } else {
            this.c = i;
        }
    }

    public final int b() {
        return this.c;
    }

    public final boolean c() {
        return this.c >= this.b;
    }

    public final String toString() {
        b bVar = new b(16);
        bVar.a('[');
        bVar.a(Integer.toString(this.a));
        bVar.a('>');
        bVar.a(Integer.toString(this.c));
        bVar.a('>');
        bVar.a(Integer.toString(this.b));
        bVar.a(']');
        return bVar.toString();
    }
}
