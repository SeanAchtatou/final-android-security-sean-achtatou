package com.agilebinary.a.a.a.c;

import com.agilebinary.a.a.a.a;
import com.agilebinary.a.a.a.aa;
import com.agilebinary.a.a.a.b.d;
import com.agilebinary.a.a.a.f.i;
import com.agilebinary.a.a.a.j;
import com.agilebinary.a.a.a.q;
import com.agilebinary.a.a.a.s;
import com.agilebinary.a.a.a.u;
import com.agilebinary.a.a.a.w;

public final class c implements s {
    public final boolean a(j jVar, i iVar) {
        if (jVar == null) {
            throw new IllegalArgumentException("HTTP response may not be null.");
        } else if (iVar == null) {
            throw new IllegalArgumentException("HTTP context may not be null.");
        } else {
            q qVar = (q) iVar.a("http.connection");
            if (qVar != null && !qVar.l()) {
                return false;
            }
            com.agilebinary.a.a.a.c b = jVar.b();
            a a = jVar.a().a();
            if (b != null && b.c() < 0 && (!b.n_() || a.a(aa.c))) {
                return false;
            }
            w d = jVar.d("Connection");
            if (!d.hasNext()) {
                d = jVar.d("Proxy-Connection");
            }
            if (d.hasNext()) {
                try {
                    d dVar = new d(d);
                    boolean z = false;
                    while (dVar.hasNext()) {
                        String a2 = dVar.a();
                        if ("Close".equalsIgnoreCase(a2)) {
                            return false;
                        }
                        if ("Keep-Alive".equalsIgnoreCase(a2)) {
                            z = true;
                        }
                    }
                    if (z) {
                        return true;
                    }
                } catch (u e) {
                    return false;
                }
            }
            return !a.a(aa.c);
        }
    }
}
