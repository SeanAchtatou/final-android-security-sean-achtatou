package com.agilebinary.b.a.a;

public abstract class j implements q {
    protected j() {
    }

    public abstract int a();

    public boolean a(Object obj) {
        a b = b();
        if (obj == null) {
            while (b.a()) {
                if (b.b() == null) {
                    return true;
                }
            }
        } else {
            while (b.a()) {
                if (obj.equals(b.b())) {
                    return true;
                }
            }
        }
        return false;
    }

    public abstract a b();

    public boolean b(Object obj) {
        throw new UnsupportedOperationException();
    }

    public Object[] c() {
        Object[] objArr = new Object[a()];
        a b = b();
        int i = 0;
        while (b.a()) {
            objArr[i] = b.b();
            i++;
        }
        return objArr;
    }

    public void d() {
        a b = b();
        while (b.a()) {
            b.b();
            b.c();
        }
    }

    public String toString() {
        StringBuffer stringBuffer = new StringBuffer();
        a b = b();
        stringBuffer.append("[");
        int a = a() - 1;
        for (int i = 0; i <= a; i++) {
            stringBuffer.append(String.valueOf(b.b()));
            if (i < a) {
                stringBuffer.append(", ");
            }
        }
        stringBuffer.append("]");
        return stringBuffer.toString();
    }
}
