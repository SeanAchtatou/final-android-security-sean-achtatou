package com.agilebinary.b.a.a;

public abstract class g extends j implements i {
    protected g() {
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof i)) {
            return false;
        }
        q qVar = (q) obj;
        if (qVar.a() != a()) {
            return false;
        }
        a b = qVar.b();
        while (b.a()) {
            if (!a(b.b())) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        int i = 0;
        a b = b();
        while (b.a()) {
            Object b2 = b.b();
            if (b2 != null) {
                i += b2.hashCode();
            }
        }
        return i;
    }
}
