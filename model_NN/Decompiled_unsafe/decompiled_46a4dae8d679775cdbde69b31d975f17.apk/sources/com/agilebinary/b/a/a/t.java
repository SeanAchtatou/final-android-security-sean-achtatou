package com.agilebinary.b.a.a;

public abstract class t extends j implements o {
    protected transient int a = 0;

    protected t() {
    }

    private m c(int i) {
        if (i >= 0 && i <= a()) {
            return new n(this, i);
        }
        throw new IndexOutOfBoundsException("Index: " + i);
    }

    public abstract Object a(int i);

    public Object a(int i, Object obj) {
        throw new UnsupportedOperationException();
    }

    /* access modifiers changed from: protected */
    public void a(int i, int i2) {
        m c = c(0);
        int i3 = i2 - 0;
        for (int i4 = 0; i4 < i3; i4++) {
            c.b();
            c.c();
        }
    }

    public final a b() {
        return new b(this);
    }

    public Object b(int i) {
        throw new UnsupportedOperationException();
    }

    public void b(int i, Object obj) {
        throw new UnsupportedOperationException();
    }

    public boolean b(Object obj) {
        b(a(), obj);
        return true;
    }

    public int c(Object obj) {
        m c = c(0);
        if (obj == null) {
            while (c.a()) {
                if (c.b() == null) {
                    return c.j_();
                }
            }
        } else {
            while (c.a()) {
                if (obj.equals(c.b())) {
                    return c.j_();
                }
            }
        }
        return -1;
    }

    public void d() {
        a(0, a());
    }

    public final m e() {
        return c(0);
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof o)) {
            return false;
        }
        m c = c(0);
        m e = ((o) obj).e();
        while (c.a() && e.a()) {
            Object b = c.b();
            Object b2 = e.b();
            if (b == null) {
                if (b2 != null) {
                }
            } else if (!b.equals(b2)) {
            }
            return false;
        }
        return !c.a() && !e.a();
    }

    public int hashCode() {
        int i = 1;
        a b = b();
        while (b.a()) {
            Object b2 = b.b();
            i = (i * 31) + (b2 == null ? 0 : b2.hashCode());
        }
        return i;
    }
}
