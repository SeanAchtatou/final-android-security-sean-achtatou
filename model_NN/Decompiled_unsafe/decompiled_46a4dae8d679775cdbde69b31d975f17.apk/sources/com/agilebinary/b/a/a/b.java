package com.agilebinary.b.a.a;

import com.agilebinary.mobilemonitor.device.a.g.e;
import java.util.NoSuchElementException;

public class b implements a {
    int a;
    int b;
    int c;
    private /* synthetic */ t d;

    private b() {
    }

    /* synthetic */ b(t tVar) {
        this(tVar, (byte) 0);
    }

    private b(t tVar, byte b2) {
        this.d = tVar;
        this.a = 0;
        this.b = -1;
        this.c = this.d.a;
    }

    private static void a(Object[] objArr, int i, int i2) {
        Object obj = objArr[i];
        objArr[i] = objArr[i2];
        objArr[i2] = obj;
    }

    public static void a(Object[] objArr, e eVar) {
        Object[] objArr2 = new Object[objArr.length];
        System.arraycopy(objArr, 0, objArr2, 0, objArr.length);
        if (eVar == null) {
            a(objArr2, objArr, 0, objArr.length);
        } else {
            a(objArr2, objArr, 0, objArr.length, eVar);
        }
    }

    private static void a(Object[] objArr, Object[] objArr2, int i, int i2) {
        int i3;
        int i4;
        int i5 = i2 - i;
        if (i5 < 7) {
            for (int i6 = i; i6 < i2; i6++) {
                int i7 = i6;
                while (i7 > i && ((com.agilebinary.a.a.a.h.b.b) objArr2[i7 - 1]).e() > 0) {
                    a(objArr2, i7, i7 - 1);
                    i7--;
                }
            }
            return;
        }
        int i8 = (i + i2) / 2;
        a(objArr2, objArr, i, i8);
        a(objArr2, objArr, i8, i2);
        if (((com.agilebinary.a.a.a.h.b.b) objArr[i8 - 1]).e() <= 0) {
            System.arraycopy(objArr, i, objArr2, i, i5);
            return;
        }
        int i9 = i8;
        int i10 = i;
        int i11 = i;
        while (i10 < i2) {
            if (i9 >= i2 || (i11 < i8 && ((com.agilebinary.a.a.a.h.b.b) objArr[i11]).e() <= 0)) {
                objArr2[i10] = objArr[i11];
                int i12 = i9;
                i3 = i11 + 1;
                i4 = i12;
            } else {
                i4 = i9 + 1;
                objArr2[i10] = objArr[i9];
                i3 = i11;
            }
            i10++;
            i11 = i3;
            i9 = i4;
        }
    }

    private static void a(Object[] objArr, Object[] objArr2, int i, int i2, e eVar) {
        int i3 = i2 - i;
        if (i3 < 7) {
            for (int i4 = i; i4 < i2; i4++) {
                int i5 = i4;
                while (i5 > i && eVar.a(objArr2[i5 - 1], objArr2[i5]) > 0) {
                    a(objArr2, i5, i5 - 1);
                    i5--;
                }
            }
            return;
        }
        int i6 = (i + i2) / 2;
        a(objArr2, objArr, i, i6, eVar);
        a(objArr2, objArr, i6, i2, eVar);
        if (eVar.a(objArr[i6 - 1], objArr[i6]) <= 0) {
            System.arraycopy(objArr, i, objArr2, i, i3);
            return;
        }
        int i7 = i6;
        int i8 = i;
        for (int i9 = i; i9 < i2; i9++) {
            if (i7 >= i2 || (i8 < i6 && eVar.a(objArr[i8], objArr[i7]) <= 0)) {
                objArr2[i9] = objArr[i8];
                i8++;
            } else {
                objArr2[i9] = objArr[i7];
                i7++;
            }
        }
    }

    public final boolean a() {
        return this.a != this.d.a();
    }

    public final Object b() {
        try {
            Object a2 = this.d.a(this.a);
            d();
            int i = this.a;
            this.a = i + 1;
            this.b = i;
            return a2;
        } catch (IndexOutOfBoundsException e) {
            d();
            throw new NoSuchElementException();
        }
    }

    public final void c() {
        if (this.b == -1) {
            throw new IllegalStateException();
        }
        d();
        try {
            this.d.b(this.b);
            if (this.b < this.a) {
                this.a--;
            }
            this.b = -1;
            this.c = this.d.a;
        } catch (IndexOutOfBoundsException e) {
            throw new d();
        }
    }

    /* access modifiers changed from: package-private */
    public final void d() {
        if (this.d.a != this.c) {
            throw new d();
        }
    }
}
