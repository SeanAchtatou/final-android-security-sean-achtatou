package com.agilebinary.b.a.a;

import com.agilebinary.a.a.a.h.b.b;

public abstract class f implements h {
    protected f() {
    }

    public int a() {
        return b().a();
    }

    public boolean a(Object obj) {
        a b = b().b();
        if (obj == null) {
            while (b.a()) {
                if (((b) b.b()).c() == null) {
                    return true;
                }
            }
        } else {
            while (b.a()) {
                if (obj.equals(((b) b.b()).c())) {
                    return true;
                }
            }
        }
        return false;
    }

    public abstract i b();

    public Object b(Object obj) {
        a b = b().b();
        if (obj == null) {
            while (b.a()) {
                b bVar = (b) b.b();
                if (bVar.c() == null) {
                    return bVar.d();
                }
            }
        } else {
            while (b.a()) {
                b bVar2 = (b) b.b();
                if (obj.equals(bVar2.c())) {
                    return bVar2.d();
                }
            }
        }
        return null;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof h)) {
            return false;
        }
        h hVar = (h) obj;
        if (hVar.a() != a()) {
            return false;
        }
        a b = b().b();
        while (b.a()) {
            b bVar = (b) b.b();
            Object c = bVar.c();
            Object d = bVar.d();
            if (d == null) {
                if (hVar.b(c) != null || !hVar.a(c)) {
                    return false;
                }
            } else if (!d.equals(hVar.b(c))) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        int i = 0;
        a b = b().b();
        while (b.a()) {
            i += b.b().hashCode();
        }
        return i;
    }

    public String toString() {
        int a = a() - 1;
        StringBuffer stringBuffer = new StringBuffer();
        a b = b().b();
        stringBuffer.append("{");
        for (int i = 0; i <= a; i++) {
            b bVar = (b) b.b();
            stringBuffer.append(bVar.c() + "=" + bVar.d());
            if (i < a) {
                stringBuffer.append(", ");
            }
        }
        stringBuffer.append("}");
        return stringBuffer.toString();
    }
}
