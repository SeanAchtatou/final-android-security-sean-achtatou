package com.bhagya.tictactoe;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class TicTacToe extends Activity {
    final int HELP_DIALOG_ID = 2;
    int MENU_NEW_GAME = 0;
    int MENU_OPTIONS = 1;
    int MENU_QUIT = 2;
    final int NAME_DIALOG_ID = 1;
    int[][] analysis_arr = {new int[2], new int[2], new int[2], new int[2], new int[2], new int[2], new int[2], new int[2]};
    int[][] arr = {new int[3], new int[3], new int[3]};
    View.OnClickListener button_listener = new View.OnClickListener() {
        public void onClick(View v) {
            ImageButton ibutton = (ImageButton) v;
            ibutton.setClickable(false);
            TicTacToe.this.count++;
            if (TicTacToe.this.count % 2 != 0 && TicTacToe.this.game_mode == 0) {
                TicTacToe.this.player = 1;
                ibutton.setImageResource(TicTacToe.this.skin_cross);
            } else if (TicTacToe.this.count % 2 == 0 || TicTacToe.this.game_mode == 1) {
                TicTacToe.this.player = 2;
                if (TicTacToe.this.user_symbol == 0 && TicTacToe.this.game_mode == 1) {
                    ibutton.setImageResource(TicTacToe.this.skin_dot);
                } else if (TicTacToe.this.user_symbol == 1 && TicTacToe.this.game_mode == 1) {
                    ibutton.setImageResource(TicTacToe.this.skin_cross);
                } else {
                    ibutton.setImageResource(TicTacToe.this.skin_dot);
                }
            }
            TicTacToe.this.after_move(ibutton);
        }
    };
    int count = 0;
    int game_bg = 2;
    int game_mode = 1;
    int[][] map_arr = {new int[]{1, 1, 1}, new int[]{1, 1, 1}, new int[]{1, 1, 1}};
    int player = 1;
    CharSequence player_name_1 = "Player 1";
    CharSequence player_name_2 = "Player 2";
    int score_player_1 = 0;
    int score_player_2 = 0;
    int skin = 4;
    int skin_cross = R.drawable.default_cross;
    int skin_dot = R.drawable.default_dot;
    int skin_layout = R.layout.main;
    boolean sound_enabled = true;
    int user_symbol = 0;
    View.OnClickListener welcome_listener = new View.OnClickListener() {
        public void onClick(View v) {
            ImageView iv = (ImageView) v;
            if (iv.getId() == R.id.new_game) {
                TicTacToe.this.showDialog(1);
            } else if (iv.getId() == R.id.options) {
                TicTacToe.this.options_menu();
            } else if (iv.getId() == R.id.help) {
                TicTacToe.this.showDialog(2);
            } else if (iv.getId() == R.id.quit) {
                TicTacToe.this.finish();
            }
        }
    };

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.welcome);
        ((ImageView) findViewById(R.id.new_game)).setOnClickListener(this.welcome_listener);
        ((ImageView) findViewById(R.id.options)).setOnClickListener(this.welcome_listener);
        ((ImageView) findViewById(R.id.help)).setOnClickListener(this.welcome_listener);
        ((ImageView) findViewById(R.id.quit)).setOnClickListener(this.welcome_listener);
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        Dialog mdialog = new Dialog(this);
        switch (id) {
            case 1:
                mdialog.setContentView((int) R.layout.name_dialog_2);
                mdialog.setTitle("Player Names");
                mdialog.setCancelable(true);
                final EditText namep1 = (EditText) mdialog.findViewById(R.id.namep1);
                final EditText namep2 = (EditText) mdialog.findViewById(R.id.namep2);
                ((Button) mdialog.findViewById(R.id.ok)).setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        TicTacToe.this.change_skin();
                        TicTacToe.this.player_name_2 = namep1.getText();
                        TicTacToe.this.player_name_1 = namep2.getText();
                        TicTacToe.this.score_player_1 = 0;
                        TicTacToe.this.score_player_2 = 0;
                        TicTacToe.this.new_game(namep2.getText());
                        TicTacToe.this.dismissDialog(1);
                    }
                });
                return mdialog;
            case 2:
                mdialog.setContentView((int) R.layout.help);
                mdialog.setTitle("Help");
                mdialog.setCancelable(true);
                return mdialog;
            default:
                return null;
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, this.MENU_NEW_GAME, 0, "New Game");
        menu.add(0, this.MENU_OPTIONS, 0, "Options");
        menu.add(0, this.MENU_QUIT, 0, "Quit");
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == 0) {
            showDialog(1);
        } else if (item.getItemId() == 1) {
            options_menu();
        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Do You Really Want to Quit?");
            builder.setCancelable(true);
            builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                }
            });
            builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    TicTacToe.this.finish();
                }
            });
            builder.create().show();
        }
        return true;
    }

    public void options_menu() {
        final CharSequence[] options_items = {"Change Skin", "Choose Symbol", "Game Mode", "Player Name", "Help", "Go Back"};
        AlertDialog.Builder options_builder = new AlertDialog.Builder(this);
        options_builder.setTitle("Options");
        options_builder.setItems(options_items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                if (options_items[item] == "Change Skin") {
                    TicTacToe.this.select_skin();
                } else if (options_items[item] == "Choose Symbol") {
                    TicTacToe.this.symbol_select();
                } else if (options_items[item] == "Game Mode") {
                    TicTacToe.this.mode_select();
                } else if (options_items[item] == "Player Name") {
                    TicTacToe.this.showDialog(1);
                } else if (options_items[item] == "Help") {
                    TicTacToe.this.showDialog(2);
                } else {
                    if (options_items[item] == "Go Back") {
                    }
                }
            }
        });
        options_builder.show();
    }

    public void symbol_select() {
        AlertDialog.Builder symbol_builder = new AlertDialog.Builder(this);
        symbol_builder.setMessage("Select Your Symbol");
        symbol_builder.setCancelable(false);
        symbol_builder.setNegativeButton("Dot", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                TicTacToe.this.user_symbol = 0;
                TicTacToe.this.new_game(TicTacToe.this.player_name_1);
            }
        });
        symbol_builder.setPositiveButton("Cross", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                TicTacToe.this.user_symbol = 1;
                TicTacToe.this.new_game(TicTacToe.this.player_name_1);
            }
        });
        symbol_builder.show();
    }

    public void mode_select() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Choose your game mode : ").setPositiveButton("Vs Computer", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                Toast.makeText(TicTacToe.this.getApplicationContext(), "Mode changed to Vs Computer", 0).show();
                TicTacToe.this.game_mode = 1;
                TicTacToe.this.score_player_1 = 0;
                TicTacToe.this.score_player_2 = 0;
            }
        }).setNegativeButton("Vs Human", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                Toast.makeText(TicTacToe.this.getApplicationContext(), "Play against your Opponent !", 0).show();
                TicTacToe.this.game_mode = 0;
                TicTacToe.this.score_player_1 = 0;
                TicTacToe.this.score_player_2 = 0;
                TicTacToe.this.showDialog(1);
            }
        });
        builder.show();
    }

    public void select_skin() {
        final CharSequence[] skin_items = {"Neo Blue", "Sweet Pink", "Ninja", "Crimson", "Default"};
        AlertDialog.Builder skin_builder = new AlertDialog.Builder(this);
        skin_builder.setItems(skin_items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                Toast.makeText(TicTacToe.this.getApplicationContext(), "Skin changed to " + ((Object) skin_items[item]), 0).show();
                if (skin_items[item] == "Neo Blue") {
                    TicTacToe.this.skin = 0;
                    TicTacToe.this.change_skin();
                } else if (skin_items[item] == "Sweet Pink") {
                    TicTacToe.this.skin = 1;
                    TicTacToe.this.change_skin();
                } else if (skin_items[item] == "Ninja") {
                    TicTacToe.this.skin = 2;
                    TicTacToe.this.change_skin();
                } else if (skin_items[item] == "Crimson") {
                    TicTacToe.this.skin = 3;
                    TicTacToe.this.change_skin();
                } else if (skin_items[item] == "Default") {
                    TicTacToe.this.skin = 4;
                    TicTacToe.this.change_skin();
                }
            }
        });
        skin_builder.show();
    }

    public void change_skin() {
        if (this.skin == 1) {
            this.skin_dot = R.drawable.gal_dot;
            this.skin_cross = R.drawable.gal_cross;
            this.skin_layout = R.layout.gal_layout;
        } else if (this.skin == 2) {
            this.skin_dot = R.drawable.ninja_dot;
            this.skin_cross = R.drawable.ninja_cross;
            this.skin_layout = R.layout.ninja_layout;
        } else if (this.skin == 3) {
            this.skin_dot = R.drawable.red_dot;
            this.skin_cross = R.drawable.red_cross;
            this.skin_layout = R.layout.red_layout;
        } else if (this.skin == 0) {
            this.skin_dot = R.drawable.default_dot;
            this.skin_cross = R.drawable.default_cross;
            this.skin_layout = R.layout.main;
        } else if (this.skin == 4) {
            this.skin_dot = R.drawable.system_dot;
            this.skin_cross = R.drawable.system_cross;
            this.skin_layout = R.layout.system_layout;
        }
        showDialog(1);
    }

    public boolean arr_isFull() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (this.arr[i][j] == 0) {
                    return false;
                }
            }
        }
        return true;
    }

    public void new_game(CharSequence player_name) {
        setContentView(this.skin_layout);
        ImageButton b3 = (ImageButton) findViewById(R.id.b3);
        ImageButton b2 = (ImageButton) findViewById(R.id.b2);
        ImageButton b1 = (ImageButton) findViewById(R.id.b1);
        ImageButton b6 = (ImageButton) findViewById(R.id.b6);
        ImageButton b5 = (ImageButton) findViewById(R.id.b5);
        ImageButton b4 = (ImageButton) findViewById(R.id.b4);
        ImageButton b9 = (ImageButton) findViewById(R.id.b9);
        ImageButton b8 = (ImageButton) findViewById(R.id.b8);
        ImageButton b7 = (ImageButton) findViewById(R.id.b7);
        b1.setOnClickListener(this.button_listener);
        b2.setOnClickListener(this.button_listener);
        b3.setOnClickListener(this.button_listener);
        b4.setOnClickListener(this.button_listener);
        b5.setOnClickListener(this.button_listener);
        b6.setOnClickListener(this.button_listener);
        b7.setOnClickListener(this.button_listener);
        b8.setOnClickListener(this.button_listener);
        b9.setOnClickListener(this.button_listener);
        b1.setClickable(true);
        b2.setClickable(true);
        b3.setClickable(true);
        b4.setClickable(true);
        b5.setClickable(true);
        b6.setClickable(true);
        b7.setClickable(true);
        b8.setClickable(true);
        b9.setClickable(true);
        set_score(3);
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                this.arr[i][j] = 0;
            }
        }
        if (this.game_mode == 1 && this.count % 2 != 0) {
            CompGame();
        }
    }

    public void after_move(ImageButton ib) {
        int pos = ((CharSequence) ib.getTag()).charAt(0) - '0';
        if (this.player == 1) {
            if (pos < 4) {
                this.arr[0][pos - 1] = 1;
            } else if (pos < 7) {
                this.arr[1][(pos - 1) % 3] = 1;
            } else if (pos < 10) {
                this.arr[2][(pos - 1) % 3] = 1;
            }
        } else if (pos < 4) {
            this.arr[0][pos - 1] = 2;
        } else if (pos < 7) {
            this.arr[1][(pos - 1) % 3] = 2;
        } else if (pos < 10) {
            this.arr[2][(pos - 1) % 3] = 2;
        }
        boolean result = result_check(this.player);
        if (result) {
            if (this.player == 1) {
                set_score(1);
                if (this.game_mode == 0) {
                    show_result("Congrats. " + ((Object) this.player_name_1) + " wins !!");
                } else {
                    show_result("Computer Wins !!");
                }
            } else {
                set_score(2);
                if (this.game_mode == 0) {
                    show_result("Congrats. " + ((Object) this.player_name_2) + " wins !!");
                } else {
                    show_result("Congrats. You have won !!");
                }
            }
        } else if (!result && arr_isFull()) {
            show_result("    Game Draw !    ");
        } else if (this.game_mode == 1 && this.player == 2 && !result) {
            CompGame();
        }
    }

    public void set_score(int player_number) {
        TextView tv = (TextView) findViewById(R.id.scoreboard);
        if (player_number == 1) {
            this.score_player_1++;
        } else if (player_number == 2) {
            this.score_player_2++;
        }
        if (this.game_mode == 1) {
            this.player_name_1 = "Computer";
        }
        tv.setText(((Object) this.player_name_1) + " : " + this.score_player_1 + "                   " + ((Object) this.player_name_2) + " : " + this.score_player_2);
    }

    public boolean show_result(CharSequence message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(message).setPositiveButton("Continue", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                TicTacToe.this.new_game(TicTacToe.this.player_name_1);
            }
        });
        builder.create().show();
        return true;
    }

    public boolean result_check(int player_local) {
        boolean win = true;
        for (int i = 0; i < 3; i++) {
            int j = 0;
            while (true) {
                if (j >= 3) {
                    break;
                } else if (this.arr[i][j] != player_local) {
                    win = false;
                    break;
                } else {
                    j++;
                }
            }
            if (win) {
                return true;
            }
            win = true;
        }
        boolean win2 = true;
        for (int i2 = 0; i2 < 3; i2++) {
            int j2 = 0;
            while (true) {
                if (j2 >= 3) {
                    break;
                } else if (this.arr[j2][i2] != player_local) {
                    win2 = false;
                    break;
                } else {
                    j2++;
                }
            }
            if (win2) {
                return true;
            }
            win2 = true;
        }
        boolean win3 = true;
        int i3 = 0;
        int k = 0;
        while (true) {
            if (i3 >= 3) {
                break;
            }
            int k2 = k + 1;
            if (this.arr[i3][k] != player_local) {
                win3 = false;
                break;
            }
            i3++;
            k = k2;
        }
        if (win3) {
            return true;
        }
        boolean win4 = true;
        int i4 = 0;
        int k3 = 2;
        while (true) {
            if (i4 >= 3) {
                break;
            }
            int k4 = k3 - 1;
            if (this.arr[i4][k3] != player_local) {
                win4 = false;
                break;
            }
            i4++;
            k3 = k4;
        }
        if (!win4) {
            return false;
        }
        return true;
    }

    public void CompGame() {
        this.player = 1;
        this.count++;
        analysis_array();
        if (!easy_move_win() && !easy_move_block()) {
            f_e_map();
            best_move();
        }
    }

    public void best_move() {
        int highest = 0;
        int k = 0;
        int[][] pos = {new int[2], new int[2], new int[2], new int[2], new int[2], new int[2], new int[2], new int[2], new int[2]};
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (this.map_arr[i][j] > highest) {
                    highest = this.map_arr[i][j];
                }
            }
        }
        for (int i2 = 0; i2 < 3; i2++) {
            for (int j2 = 0; j2 < 3; j2++) {
                if (this.map_arr[i2][j2] == highest) {
                    pos[k][0] = i2;
                    pos[k][1] = j2;
                    k++;
                }
            }
        }
        int random_index = ((int) (Math.random() * 10.0d)) % k;
        comp_play(pos[random_index][0], pos[random_index][1]);
    }

    public void f_e_map() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                this.map_arr[i][j] = 1;
            }
        }
        for (int i2 = 0; i2 < 3; i2++) {
            for (int j2 = 0; j2 < 3; j2++) {
                if (this.arr[i2][j2] == 1 || this.arr[i2][j2] == 2) {
                    this.map_arr[i2][j2] = 0;
                }
            }
        }
        for (int i3 = 0; i3 < 8; i3++) {
            if ((this.analysis_arr[i3][0] == 1 && this.analysis_arr[i3][1] == 0) || (this.analysis_arr[i3][0] == 0 && this.analysis_arr[i3][1] == 1)) {
                if (i3 < 3) {
                    for (int j3 = 0; j3 < 3; j3++) {
                        if (this.map_arr[i3][j3] != 0) {
                            int[] iArr = this.map_arr[i3];
                            iArr[j3] = iArr[j3] + 1;
                        }
                    }
                } else if (i3 < 6) {
                    for (int j4 = 0; j4 < 3; j4++) {
                        if (this.map_arr[j4][i3 - 3] != 0) {
                            int[] iArr2 = this.map_arr[j4];
                            int i4 = i3 - 3;
                            iArr2[i4] = iArr2[i4] + 1;
                        }
                    }
                } else if (i3 == 6) {
                    int k = 0;
                    for (int m = 0; m < 3; m++) {
                        if (this.map_arr[m][k] != 0) {
                            int[] iArr3 = this.map_arr[m];
                            iArr3[k] = iArr3[k] + 1;
                        }
                        k++;
                    }
                } else if (i3 == 7) {
                    int k2 = 2;
                    for (int m2 = 0; m2 < 3; m2++) {
                        if (this.map_arr[m2][k2] != 0) {
                            int[] iArr4 = this.map_arr[m2];
                            iArr4[k2] = iArr4[k2] + 1;
                        }
                        k2--;
                    }
                }
            }
        }
    }

    public boolean easy_move_block() {
        boolean flag = false;
        int k = 0;
        int i = 0;
        while (true) {
            if (i < 8) {
                if (this.analysis_arr[i][0] == 0 && this.analysis_arr[i][1] == 2) {
                    flag = true;
                    break;
                }
                i++;
            } else {
                break;
            }
        }
        if (flag) {
            if (i < 3) {
                for (int j = 0; j < 3; j++) {
                    if (this.arr[i][j] == 0) {
                        comp_play(i, j);
                        return true;
                    }
                }
            } else if (i < 6) {
                for (int j2 = 0; j2 < 3; j2++) {
                    if (this.arr[j2][i - 3] == 0) {
                        comp_play(j2, i - 3);
                        return true;
                    }
                }
            } else if (i == 6) {
                for (int j3 = 0; j3 < 3; j3++) {
                    if (this.arr[j3][k] == 0) {
                        comp_play(j3, k);
                        return true;
                    }
                    k++;
                }
            } else if (i == 7) {
                int k2 = 2;
                for (int j4 = 0; j4 < 3; j4++) {
                    if (this.arr[j4][k2] == 0) {
                        comp_play(j4, k2);
                        return true;
                    }
                    k2--;
                }
            }
        }
        return false;
    }

    public boolean easy_move_win() {
        boolean flag = false;
        int k = 0;
        int i = 0;
        while (true) {
            if (i < 8) {
                if (this.analysis_arr[i][0] == 2 && this.analysis_arr[i][1] == 0) {
                    flag = true;
                    break;
                }
                i++;
            } else {
                break;
            }
        }
        if (flag) {
            if (i < 3) {
                for (int j = 0; j < 3; j++) {
                    if (this.arr[i][j] == 0) {
                        comp_play(i, j);
                        return true;
                    }
                }
            } else if (i < 6) {
                for (int j2 = 0; j2 < 3; j2++) {
                    if (this.arr[j2][i - 3] == 0) {
                        comp_play(j2, i - 3);
                        return true;
                    }
                }
            } else if (i == 6) {
                for (int j3 = 0; j3 < 3; j3++) {
                    if (this.arr[j3][k] == 0) {
                        comp_play(j3, k);
                        return true;
                    }
                    k++;
                }
            } else if (i == 7) {
                int k2 = 2;
                for (int j4 = 0; j4 < 3; j4++) {
                    if (this.arr[j4][k2] == 0) {
                        comp_play(j4, k2);
                        return true;
                    }
                    k2--;
                }
            }
        }
        return false;
    }

    public void comp_play(int x, int y) {
        int ib_id = ((ImageButton) findViewById(R.id.b1)).getId();
        if (!(x == 0 && y == 0)) {
            if (x == 0) {
                ib_id -= y;
            } else if (x == 1) {
                ib_id += 3 - y;
            } else if (x == 2) {
                ib_id += 6 - y;
            }
        }
        ImageButton ib = (ImageButton) findViewById(ib_id);
        if (this.user_symbol == 0) {
            ib.setImageResource(this.skin_cross);
        } else {
            ib.setImageResource(this.skin_dot);
        }
        ib.setClickable(false);
        after_move(ib);
    }

    public void analysis_array() {
        for (int i = 0; i < 8; i++) {
            int[] iArr = this.analysis_arr[i];
            this.analysis_arr[i][1] = 0;
            iArr[0] = 0;
        }
        for (int i2 = 0; i2 < 3; i2++) {
            for (int j = 0; j < 3; j++) {
                if (this.arr[i2][j] == 1) {
                    int[] iArr2 = this.analysis_arr[i2];
                    iArr2[0] = iArr2[0] + 1;
                } else if (this.arr[i2][j] == 2) {
                    int[] iArr3 = this.analysis_arr[i2];
                    iArr3[1] = iArr3[1] + 1;
                }
            }
        }
        for (int i3 = 0; i3 < 3; i3++) {
            for (int j2 = 0; j2 < 3; j2++) {
                if (this.arr[j2][i3] == 1) {
                    int[] iArr4 = this.analysis_arr[i3 + 3];
                    iArr4[0] = iArr4[0] + 1;
                } else if (this.arr[j2][i3] == 2) {
                    int[] iArr5 = this.analysis_arr[i3 + 3];
                    iArr5[1] = iArr5[1] + 1;
                }
            }
        }
        int k = 0;
        for (int i4 = 0; i4 < 3; i4++) {
            if (this.arr[i4][k] == 1) {
                int[] iArr6 = this.analysis_arr[6];
                iArr6[0] = iArr6[0] + 1;
            } else if (this.arr[i4][k] == 2) {
                int[] iArr7 = this.analysis_arr[6];
                iArr7[1] = iArr7[1] + 1;
            }
            k++;
        }
        int k2 = 2;
        for (int i5 = 0; i5 < 3; i5++) {
            if (this.arr[i5][k2] == 1) {
                int[] iArr8 = this.analysis_arr[7];
                iArr8[0] = iArr8[0] + 1;
            } else if (this.arr[i5][k2] == 2) {
                int[] iArr9 = this.analysis_arr[7];
                iArr9[1] = iArr9[1] + 1;
            }
            k2--;
        }
    }
}
