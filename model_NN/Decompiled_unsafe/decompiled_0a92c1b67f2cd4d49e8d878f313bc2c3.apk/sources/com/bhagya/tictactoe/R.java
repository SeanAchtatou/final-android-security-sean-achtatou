package com.bhagya.tictactoe;

public final class R {

    public static final class attr {
    }

    public static final class drawable {
        public static final int back = 2130837504;
        public static final int bg_main = 2130837505;
        public static final int bg_maple = 2130837506;
        public static final int default_button = 2130837507;
        public static final int default_cross = 2130837508;
        public static final int default_dot = 2130837509;
        public static final int gal_button = 2130837510;
        public static final int gal_cross = 2130837511;
        public static final int gal_dot = 2130837512;
        public static final int help = 2130837513;
        public static final int icon = 2130837514;
        public static final int new_game = 2130837515;
        public static final int ninja_button = 2130837516;
        public static final int ninja_cross = 2130837517;
        public static final int ninja_dot = 2130837518;
        public static final int options = 2130837519;
        public static final int quit = 2130837520;
        public static final int red_button = 2130837521;
        public static final int red_cross = 2130837522;
        public static final int red_dot = 2130837523;
        public static final int scoreboard_bg = 2130837524;
        public static final int system_cross = 2130837525;
        public static final int system_dot = 2130837526;
        public static final int title = 2130837527;
        public static final int ttt = 2130837528;
    }

    public static final class id {
        public static final int LinearLayout01 = 2131034125;
        public static final int b1 = 2131034116;
        public static final int b2 = 2131034115;
        public static final int b3 = 2131034114;
        public static final int b4 = 2131034119;
        public static final int b5 = 2131034118;
        public static final int b6 = 2131034117;
        public static final int b7 = 2131034122;
        public static final int b8 = 2131034121;
        public static final int b9 = 2131034120;
        public static final int help = 2131034133;
        public static final int layout_id = 2131034112;
        public static final int layout_root = 2131034126;
        public static final int name = 2131034127;
        public static final int namep1 = 2131034129;
        public static final int namep2 = 2131034130;
        public static final int new_game = 2131034131;
        public static final int ok = 2131034128;
        public static final int options = 2131034132;
        public static final int quit = 2131034134;
        public static final int scoreboard = 2131034123;
        public static final int startbutton = 2131034124;
        public static final int title = 2131034113;
    }

    public static final class layout {
        public static final int gal_layout = 2130903040;
        public static final int game_start_screen = 2130903041;
        public static final int help = 2130903042;
        public static final int main = 2130903043;
        public static final int name_dialog = 2130903044;
        public static final int name_dialog_2 = 2130903045;
        public static final int ninja_layout = 2130903046;
        public static final int red_layout = 2130903047;
        public static final int system_layout = 2130903048;
        public static final int welcome = 2130903049;
    }

    public static final class string {
        public static final int app_name = 2130968576;
    }
}
