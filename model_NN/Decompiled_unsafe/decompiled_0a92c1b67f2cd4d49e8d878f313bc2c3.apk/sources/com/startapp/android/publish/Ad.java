package com.startapp.android.publish;

import android.content.Context;
import android.os.Handler;
import com.startapp.android.publish.c.g;
import com.startapp.android.publish.model.AdPreferences;
import com.startapp.android.publish.model.GetAdRequest;

public abstract class Ad {
    protected Context context;
    protected String errorMessage = null;
    protected Object extraData = null;
    protected GetAdRequest getAdRequest = null;
    private AdState state = AdState.UN_INITIALIZED;

    public enum AdState {
        UN_INITIALIZED,
        PROCESSING,
        READY
    }

    public Ad(Context context2) {
        this.context = context2;
    }

    public String getErrorMessage() {
        return this.errorMessage;
    }

    public Object getExtraData() {
        return this.extraData;
    }

    public AdState getState() {
        return this.state;
    }

    public boolean isReady() {
        return this.state == AdState.READY;
    }

    public boolean load(AdPreferences adPreferences) {
        return load(adPreferences, null);
    }

    public boolean load(AdPreferences adPreferences, final AdEventListener adEventListener) {
        boolean z = this.state != AdState.UN_INITIALIZED;
        if (!g.a(this.context)) {
            z = true;
        }
        if (z) {
            setErrorMessage("Ad wasn't loaded");
            if (adEventListener == null) {
                return false;
            }
            new Handler().post(new Runnable() {
                public void run() {
                    adEventListener.onFailedToReceiveAd(Ad.this);
                }
            });
            return false;
        }
        setState(AdState.PROCESSING);
        this.getAdRequest = GetAdRequest.Create(this.context, adPreferences);
        return true;
    }

    public void setErrorMessage(String str) {
        this.errorMessage = str;
    }

    public void setExtraData(Object obj) {
        this.extraData = obj;
    }

    public void setState(AdState adState) {
        this.state = adState;
    }
}
