package com.startapp.android.publish.model;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.json.JSONObject;

public abstract class BaseResponse extends BaseDTO implements JsonDeserializer {
    private String errorMessage = null;
    protected Map<String, String> parameters = new HashMap();
    private boolean validResponse = true;

    public void fromJson(JSONObject jSONObject) {
        this.validResponse = jSONObject.optBoolean("validResponse");
        this.errorMessage = jSONObject.optString("errorMessage", null);
        JSONObject optJSONObject = jSONObject.optJSONObject("parameters");
        if (optJSONObject == null) {
            this.parameters = null;
            return;
        }
        this.parameters = new HashMap();
        Iterator<String> keys = optJSONObject.keys();
        while (keys.hasNext()) {
            String next = keys.next();
            this.parameters.put(next, (String) optJSONObject.opt(next));
        }
    }

    public String getErrorMessage() {
        return this.errorMessage;
    }

    public Map<String, String> getParameters() {
        return this.parameters;
    }

    public boolean isValidResponse() {
        return this.validResponse;
    }

    public void setErrorMessage(String str) {
        this.errorMessage = str;
    }

    public void setParameters(Map<String, String> map) {
        this.parameters = map;
    }

    public void setValidResponse(boolean z) {
        this.validResponse = z;
    }

    public String toString() {
        return "BaseResponse [parameters=" + this.parameters + ", validResponse=" + this.validResponse + ", errorMessage=" + this.errorMessage + "]";
    }
}
