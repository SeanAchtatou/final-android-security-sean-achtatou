package com.startapp.android.publish.a;

import android.content.Context;
import android.os.AsyncTask;
import com.startapp.android.publish.Ad;
import com.startapp.android.publish.AdEventListener;
import com.startapp.android.publish.model.GetAdRequest;

public abstract class a extends AsyncTask<Void, Void, Boolean> {
    protected Context a;
    protected GetAdRequest b;
    protected Ad c;
    protected AdEventListener d;
    protected String e = null;

    public a(Context context, GetAdRequest getAdRequest, Ad ad, AdEventListener adEventListener) {
        this.a = context;
        this.b = getAdRequest;
        this.c = ad;
        this.d = adEventListener;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Boolean doInBackground(Void... voidArr) {
        return Boolean.valueOf(a(a()));
    }

    /* access modifiers changed from: protected */
    public abstract Object a();

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void onPostExecute(Boolean bool) {
        super.onPostExecute(bool);
        this.c.setState(bool.booleanValue() ? Ad.AdState.READY : Ad.AdState.UN_INITIALIZED);
        if (!bool.booleanValue()) {
            this.c.setErrorMessage(this.e);
            if (this.d != null) {
                this.d.onFailedToReceiveAd(this.c);
            }
        }
    }

    /* access modifiers changed from: protected */
    public abstract boolean a(Object obj);
}
