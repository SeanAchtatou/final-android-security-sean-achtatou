package com.startapp.android.publish.model;

import org.json.JSONObject;

public class AdDetails implements JsonDeserializer {
    private String adId;
    private String clickUrl;
    private String description;
    private String imageUrl;
    private String title;
    private String trackingUrl;

    public void fromJson(JSONObject jSONObject) {
        this.adId = jSONObject.optString("adId", null);
        this.clickUrl = jSONObject.optString("clickUrl", null);
        this.trackingUrl = jSONObject.optString("trackingUrl", null);
        this.title = jSONObject.optString("title", null);
        this.description = jSONObject.optString("description", null);
        this.imageUrl = jSONObject.optString("imageUrl", null);
    }

    public String getAdId() {
        return this.adId;
    }

    public String getClickUrl() {
        return this.clickUrl;
    }

    public String getDescription() {
        return this.description;
    }

    public String getImageUrl() {
        return this.imageUrl;
    }

    public String getTitle() {
        return this.title;
    }

    public String getTrackingUrl() {
        return this.trackingUrl;
    }

    public String toString() {
        return "AdDetails [adId=" + this.adId + ", clickUrl=" + this.clickUrl + ", clickUrl=" + this.clickUrl + ", title=" + this.title + ", description=" + this.description + ", clickUrl=" + this.clickUrl + "]";
    }
}
