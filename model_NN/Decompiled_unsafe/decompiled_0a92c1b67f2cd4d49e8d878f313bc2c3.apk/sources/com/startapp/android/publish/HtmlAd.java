package com.startapp.android.publish;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import com.startapp.android.publish.a.c;
import com.startapp.android.publish.c.g;
import com.startapp.android.publish.model.AdPreferences;

public class HtmlAd extends Ad {
    private String launcherName = null;
    public boolean smartRedirect = true;
    public String trackingUrl = null;

    public HtmlAd(Context context) {
        super(context);
    }

    private void initDefaultLauncherName() {
        Intent intent = new Intent();
        intent.setAction("android.intent.action.MAIN");
        intent.addCategory("android.intent.category.HOME");
        ResolveInfo resolveActivity = this.context.getPackageManager().resolveActivity(intent, 0);
        if (resolveActivity != null && resolveActivity.activityInfo != null) {
            this.launcherName = resolveActivity.activityInfo.packageName;
            if (this.launcherName != null) {
                this.launcherName = this.launcherName.toLowerCase();
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0048  */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x004a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean doHome() {
        /*
            r5 = this;
            r2 = 1
            r1 = 0
            android.content.Context r0 = r5.context
            java.lang.String r3 = "activity"
            java.lang.Object r0 = r0.getSystemService(r3)
            android.app.ActivityManager r0 = (android.app.ActivityManager) r0
            r3 = 2147483647(0x7fffffff, float:NaN)
            java.util.List r0 = r0.getRunningTasks(r3)
            int r3 = r0.size()
            if (r3 <= 0) goto L_0x00e1
            java.lang.Object r0 = r0.get(r1)
            android.app.ActivityManager$RunningTaskInfo r0 = (android.app.ActivityManager.RunningTaskInfo) r0
            android.content.ComponentName r0 = r0.baseActivity
            java.lang.String r0 = r0.toShortString()
            java.lang.String r0 = r0.toLowerCase()
            java.lang.String r3 = r5.launcherName
            if (r3 == 0) goto L_0x0035
            java.lang.String r3 = r5.launcherName
            boolean r3 = r0.contains(r3)
            if (r3 != 0) goto L_0x0045
        L_0x0035:
            java.lang.String r3 = "com.android.internal.app"
            boolean r3 = r0.contains(r3)
            if (r3 != 0) goto L_0x0045
            java.lang.String r3 = "com.android.systemui.recent"
            boolean r0 = r0.contains(r3)
            if (r0 == 0) goto L_0x00e1
        L_0x0045:
            r0 = r2
        L_0x0046:
            if (r0 != 0) goto L_0x004a
            r0 = r1
        L_0x0049:
            return r0
        L_0x004a:
            android.content.Context r0 = r5.context
            android.content.Context r0 = r0.getApplicationContext()
            boolean r0 = com.startapp.android.publish.c.g.a(r0)
            if (r0 == 0) goto L_0x00de
            boolean r0 = r5.isReady()
            if (r0 == 0) goto L_0x00de
            android.content.Intent r0 = new android.content.Intent
            android.content.Context r1 = r5.context
            java.lang.Class<com.startapp.android.publish.AppWallActivity> r3 = com.startapp.android.publish.AppWallActivity.class
            r0.<init>(r1, r3)
            java.lang.String r1 = "fileUrl"
            java.lang.String r3 = "exit.html"
            r0.putExtra(r1, r3)
            java.lang.String r1 = "smartRedirect"
            boolean r3 = r5.smartRedirect
            r0.putExtra(r1, r3)
            java.lang.String r1 = "overlay"
            r0.putExtra(r1, r2)
            r1 = 343932928(0x14800000, float:1.2924697E-26)
            r0.addFlags(r1)
            android.content.Context r1 = r5.context
            r1.startActivity(r0)
            com.startapp.android.publish.a r1 = new com.startapp.android.publish.a
            android.content.Context r0 = r5.context
            android.content.Context r0 = r0.getApplicationContext()
            r1.<init>(r0)
            android.content.Context r0 = r5.context
            java.io.File r0 = r0.getFilesDir()
            java.lang.String r0 = r0.getAbsolutePath()
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "file:///"
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.StringBuilder r0 = r3.append(r0)
            java.lang.String r3 = "/"
            java.lang.StringBuilder r0 = r0.append(r3)
            java.lang.String r3 = "exit.html"
            java.lang.StringBuilder r0 = r0.append(r3)
            java.lang.String r0 = r0.toString()
            r1.setUrl(r0)
            java.lang.String r0 = r5.trackingUrl
            if (r0 == 0) goto L_0x00d0
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.StringBuilder r0 = r3.append(r0)
            java.lang.String r3 = "&position=home"
            java.lang.StringBuilder r0 = r0.append(r3)
            java.lang.String r0 = r0.toString()
        L_0x00d0:
            r1.setTrackingUrl(r0)
            boolean r0 = r5.smartRedirect
            r1.setSmartRedirect(r0)
            r1.b()
            r0 = r2
            goto L_0x0049
        L_0x00de:
            r0 = r1
            goto L_0x0049
        L_0x00e1:
            r0 = r1
            goto L_0x0046
        */
        throw new UnsupportedOperationException("Method not decompiled: com.startapp.android.publish.HtmlAd.doHome():boolean");
    }

    public boolean load(AdPreferences adPreferences, AdEventListener adEventListener) {
        initDefaultLauncherName();
        if (!super.load(adPreferences, adEventListener)) {
            return true;
        }
        new c(this.context, this.getAdRequest, this, adEventListener).execute(new Void[0]);
        return true;
    }

    public boolean show() {
        if (!g.a(this.context) || !isReady()) {
            return false;
        }
        Intent intent = new Intent(this.context, AppWallActivity.class);
        intent.putExtra("fileUrl", "exit.html");
        String str = this.trackingUrl;
        if (str != null) {
            str = str + "&position=back";
        }
        intent.putExtra("tracking", str);
        intent.putExtra("smartRedirect", this.smartRedirect);
        intent.addFlags(344457216);
        this.context.startActivity(intent);
        return true;
    }
}
