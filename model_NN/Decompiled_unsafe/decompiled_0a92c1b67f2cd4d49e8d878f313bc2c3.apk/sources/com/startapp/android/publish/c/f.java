package com.startapp.android.publish.c;

import android.content.Context;
import android.graphics.drawable.ShapeDrawable;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

public class f {
    public static View a(Context context) {
        float f = context.getResources().getDisplayMetrics().density;
        LinearLayout linearLayout = new LinearLayout(context);
        b bVar = new b();
        ShapeDrawable shapeDrawable = new ShapeDrawable();
        shapeDrawable.setShape(bVar);
        shapeDrawable.getPaint().setColor(-805306368);
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(-2, -2);
        layoutParams.gravity = 17;
        linearLayout.setLayoutParams(layoutParams);
        linearLayout.setOrientation(0);
        linearLayout.setBaselineAligned(false);
        linearLayout.setBackgroundDrawable(shapeDrawable);
        linearLayout.setPadding((int) ((8.0f * f) + 0.5f), (int) ((10.0f * f) + 0.5f), (int) ((8.0f * f) + 0.5f), (int) ((10.0f * f) + 0.5f));
        ProgressBar progressBar = new ProgressBar(context);
        LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(-2, -2);
        layoutParams2.setMargins(0, 0, (int) ((f * 12.0f) + 0.5f), 0);
        progressBar.setLayoutParams(layoutParams2);
        progressBar.setMax(10000);
        TextView textView = new TextView(context);
        LinearLayout.LayoutParams layoutParams3 = new LinearLayout.LayoutParams(-1, -2);
        layoutParams3.gravity = 16;
        textView.setLayoutParams(layoutParams3);
        textView.setText("Loading...");
        linearLayout.addView(progressBar);
        linearLayout.addView(textView);
        return linearLayout;
    }
}
