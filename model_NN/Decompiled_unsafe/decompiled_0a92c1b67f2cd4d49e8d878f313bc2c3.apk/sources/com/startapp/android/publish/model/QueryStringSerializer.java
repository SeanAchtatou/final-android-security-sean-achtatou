package com.startapp.android.publish.model;

public interface QueryStringSerializer {
    String toQueryString();
}
