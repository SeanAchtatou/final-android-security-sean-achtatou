package com.startapp.android.publish.model;

import java.util.HashMap;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public abstract class BaseRequest extends BaseDTO implements JsonSerializer, QueryStringSerializer {
    protected Map<String, String> parameters = new HashMap();

    public Map<String, String> getParameters() {
        return this.parameters;
    }

    public void setParameters(Map<String, String> map) {
        this.parameters = map;
    }

    public JSONObject toJson() {
        if (this.parameters == null || this.parameters.isEmpty()) {
            return null;
        }
        try {
            JSONObject jSONObject = new JSONObject();
            for (String next : this.parameters.keySet()) {
                jSONObject.put(next.toString(), this.parameters.get(next));
            }
            JSONObject jSONObject2 = new JSONObject();
            try {
                jSONObject2.put("parameters", jSONObject);
                return jSONObject2;
            } catch (JSONException e) {
                return jSONObject2;
            }
        } catch (JSONException e2) {
            return null;
        }
    }

    public String toString() {
        return "BaseRequest [parameters=" + this.parameters + "]";
    }
}
