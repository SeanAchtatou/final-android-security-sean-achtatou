package com.startapp.android.publish.model;

import java.util.ArrayList;
import java.util.List;

public class AdPreferences {
    public static final String TYPE_APP_WALL = "APP_WALL";
    public static final String TYPE_BANNER = "BANNER";
    public static final String TYPE_INAPP_EXIT = "INAPP_EXIT";
    public static final String TYPE_SCRINGO_TOOLBAR = "SCRINGO_TOOLBAR";
    public static final String TYPE_TEXT = "TEXT";
    private int adsNumber = 1;
    private int age = 0;
    private List<String> categories = null;
    private List<String> categoriesExclude = null;
    private String gender = null;
    private String keywords = null;
    private String productId;
    private String publisherId;
    private boolean testMode = false;
    private String type = null;

    public AdPreferences() {
    }

    public AdPreferences(String str, String str2, String str3) {
        this.publisherId = str;
        this.productId = str2;
        this.type = str3;
    }

    public void addCategory(String str) {
        if (this.categories == null) {
            this.categories = new ArrayList();
        }
        this.categories.add(str);
    }

    public void addCategoryExclude(String str) {
        if (this.categoriesExclude == null) {
            this.categoriesExclude = new ArrayList();
        }
        this.categoriesExclude.add(str);
    }

    public int getAdsNumber() {
        return this.adsNumber;
    }

    public int getAge() {
        return this.age;
    }

    public List<String> getCategories() {
        return this.categories;
    }

    public List<String> getCategoriesExclude() {
        return this.categoriesExclude;
    }

    public String getGender() {
        return this.gender;
    }

    public String getKeywords() {
        return this.keywords;
    }

    public String getProductId() {
        return this.productId;
    }

    public String getPublisherId() {
        return this.publisherId;
    }

    public String getType() {
        return this.type;
    }

    public boolean isTestMode() {
        return this.testMode;
    }

    public void setAdsNumber(int i) {
        this.adsNumber = i;
    }

    public void setAge(int i) {
        this.age = i;
    }

    public void setGender(String str) {
        this.gender = str;
    }

    public void setKeywords(String str) {
        this.keywords = str;
    }

    public void setProductId(String str) {
        this.productId = str;
    }

    public void setPublisherId(String str) {
        this.publisherId = str;
    }

    public void setTestMode(boolean z) {
        this.testMode = z;
    }

    public void setType(String str) {
        this.type = str;
    }

    public String toString() {
        return "AdPreferences [publisherId=" + this.publisherId + ", productId=" + this.productId + ", testMode=" + this.testMode + ", gender=" + this.gender + ", age=" + this.age + ", keywords=" + this.keywords + ", type=" + this.type + ", adsNumber=" + this.adsNumber + ", categories=" + this.categories + ", categoriesExclude=" + this.categoriesExclude + "]";
    }
}
