package com.startapp.android.publish.model;

import android.content.Context;
import android.os.Build;
import android.telephony.TelephonyManager;
import com.startapp.android.publish.c.a;
import com.startapp.android.publish.c.e;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public class GetAdRequest extends BaseRequest {
    private static final String OS = "android";
    private int adsNumber = 1;
    private int age;
    private List<String> categories = null;
    private List<String> categoriesExclude = null;
    private String deviceVersion;
    private String gender;
    private int height;
    private String isp;
    private String keywords;
    private double latitude = 0.0d;
    private double longitude = 0.0d;
    private String manufacturer;
    private String model;
    private String os;
    private String packageId;
    private String productId;
    private String publisherId;
    private String sdkVersion;
    private String subProductId;
    private String subPublisherId;
    private String template;
    private boolean testMode;
    private String type;
    private String userId;
    private int width;

    public static GetAdRequest Create(Context context, AdPreferences adPreferences) {
        GetAdRequest getAdRequest = new GetAdRequest();
        getAdRequest.publisherId = adPreferences.getPublisherId();
        getAdRequest.productId = adPreferences.getProductId();
        getAdRequest.type = adPreferences.getType();
        getAdRequest.age = adPreferences.getAge();
        getAdRequest.gender = adPreferences.getGender();
        getAdRequest.keywords = adPreferences.getKeywords();
        getAdRequest.testMode = adPreferences.isTestMode();
        getAdRequest.adsNumber = adPreferences.getAdsNumber();
        getAdRequest.categories = adPreferences.getCategories();
        getAdRequest.categoriesExclude = adPreferences.getCategoriesExclude();
        getAdRequest.os = OS;
        getAdRequest.sdkVersion = "1.3";
        getAdRequest.setUserId(a.a(context));
        getAdRequest.setPackageId(context.getPackageName());
        getAdRequest.setManufacturer(Build.MANUFACTURER);
        getAdRequest.setModel(Build.MODEL);
        getAdRequest.setDeviceVersion(Integer.toString(Build.VERSION.SDK_INT));
        getAdRequest.setWidth(context.getResources().getDisplayMetrics().widthPixels);
        getAdRequest.setHeight(context.getResources().getDisplayMetrics().heightPixels);
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
        if (telephonyManager != null) {
            getAdRequest.setIsp(telephonyManager.getSimOperator());
        } else {
            getAdRequest.setIsp(null);
        }
        return getAdRequest;
    }

    private StringBuilder createParam(String str, String str2, boolean z) {
        if (!z || str2 != null) {
            StringBuilder sb = new StringBuilder();
            if (str2 != null) {
                try {
                    sb.append("&").append(str).append("=").append(URLEncoder.encode(str2, "UTF-8"));
                } catch (UnsupportedEncodingException e) {
                    if (z) {
                        throw new e("failed encoding value: [" + str2 + "]", e);
                    }
                }
            }
            return sb;
        }
        throw new e("Required key: [" + str + "] is missing", null);
    }

    private StringBuilder createParam(String str, List<String> list, boolean z) {
        if (!z || list != null) {
            StringBuilder sb = new StringBuilder();
            if (list != null) {
                for (String encode : list) {
                    try {
                        sb.append("&").append(str).append("=").append(URLEncoder.encode(encode, "UTF-8"));
                    } catch (UnsupportedEncodingException e) {
                    }
                }
            }
            return sb;
        }
        throw new e("Required key: [" + str + "] is missing", null);
    }

    public void addCategory(String str) {
        if (this.categories == null) {
            this.categories = new ArrayList();
        }
        this.categories.add(str);
    }

    public void addCategoryExclude(String str) {
        if (this.categoriesExclude == null) {
            this.categoriesExclude = new ArrayList();
        }
        this.categoriesExclude.add(str);
    }

    public int getAdsNumber() {
        return this.adsNumber;
    }

    public int getAge() {
        return this.age;
    }

    public List<String> getCategories() {
        return this.categories;
    }

    public List<String> getCategoriesExclude() {
        return this.categoriesExclude;
    }

    public String getDeviceVersion() {
        return this.deviceVersion;
    }

    public String getGender() {
        return this.gender;
    }

    public int getHeight() {
        return this.height;
    }

    public String getIsp() {
        return this.isp;
    }

    public String getKeywords() {
        return this.keywords;
    }

    public double getLatitude() {
        return this.latitude;
    }

    public double getLongitude() {
        return this.longitude;
    }

    public String getManufacturer() {
        return this.manufacturer;
    }

    public String getModel() {
        return this.model;
    }

    public String getOs() {
        return this.os;
    }

    public String getPackageId() {
        return this.packageId;
    }

    public Map<String, String> getParameters() {
        return this.parameters;
    }

    public String getProductId() {
        return this.productId;
    }

    public String getPublisherId() {
        return this.publisherId;
    }

    public String getSdkVersion() {
        return this.sdkVersion;
    }

    public String getSubProductId() {
        return this.subProductId;
    }

    public String getSubPublisherId() {
        return this.subPublisherId;
    }

    public String getTemplate() {
        return this.template;
    }

    public String getType() {
        return this.type;
    }

    public String getUserId() {
        return this.userId;
    }

    public int getWidth() {
        return this.width;
    }

    public boolean isTestMode() {
        return this.testMode;
    }

    public void setAdsNumber(int i) {
        this.adsNumber = i;
    }

    public void setAge(int i) {
        this.age = i;
    }

    public void setCategories(List<String> list) {
        this.categories = list;
    }

    public void setCategoriesExclude(List<String> list) {
        this.categoriesExclude = list;
    }

    public void setDeviceVersion(String str) {
        this.deviceVersion = str;
    }

    public void setGender(String str) {
        this.gender = str;
    }

    public void setHeight(int i) {
        this.height = i;
    }

    public void setIsp(String str) {
        this.isp = str;
    }

    public void setKeywords(String str) {
        this.keywords = str;
    }

    public void setLatitude(double d) {
        this.latitude = d;
    }

    public void setLongitude(double d) {
        this.longitude = d;
    }

    public void setManufacturer(String str) {
        this.manufacturer = str;
    }

    public void setModel(String str) {
        this.model = str;
    }

    public void setOs(String str) {
        this.os = str;
    }

    public void setPackageId(String str) {
        this.packageId = str;
    }

    public void setParameters(Map<String, String> map) {
        this.parameters = map;
    }

    public void setProductId(String str) {
        this.productId = str;
    }

    public void setPublisherId(String str) {
        this.publisherId = str;
    }

    public void setSdkVersion(String str) {
        this.sdkVersion = str;
    }

    public void setSubProductId(String str) {
        this.subProductId = str;
    }

    public void setSubPublisherId(String str) {
        this.subPublisherId = str;
    }

    public void setTemplate(String str) {
        this.template = str;
    }

    public void setTestMode(boolean z) {
        this.testMode = z;
    }

    public void setType(String str) {
        this.type = str;
    }

    public void setUserId(String str) {
        this.userId = str;
    }

    public void setWidth(int i) {
        this.width = i;
    }

    public JSONObject toJson() {
        JSONObject json = super.toJson();
        if (json == null) {
            json = new JSONObject();
        }
        try {
            json.put("publisherId", this.publisherId);
            json.put("productId", this.productId);
            json.put("packageId", this.packageId);
            json.put("sdkVersion", this.sdkVersion);
            json.put("type", this.type);
            json.put("testMode", this.testMode);
            json.put("userId", this.userId);
            json.put("isp", this.isp);
            json.put("longitude", this.longitude);
            json.put("latitude", this.latitude);
            json.put("gender", this.gender);
            json.put("age", this.age);
            json.put("keywords", this.keywords);
            json.put("template", this.template);
            json.put("adsNumber", this.adsNumber);
            json.put("model", this.model);
            json.put("manufacturer", this.manufacturer);
            json.put("deviceVersion", this.deviceVersion);
            json.put("subPublisherId", this.subPublisherId);
            json.put("subProductId", this.subProductId);
            json.put("os", this.os);
            json.put("width", this.width);
            json.put("height", this.height);
        } catch (JSONException e) {
        }
        return json;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.startapp.android.publish.model.GetAdRequest.createParam(java.lang.String, java.lang.String, boolean):java.lang.StringBuilder
     arg types: [java.lang.String, java.lang.String, int]
     candidates:
      com.startapp.android.publish.model.GetAdRequest.createParam(java.lang.String, java.util.List<java.lang.String>, boolean):java.lang.StringBuilder
      com.startapp.android.publish.model.GetAdRequest.createParam(java.lang.String, java.lang.String, boolean):java.lang.StringBuilder */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.startapp.android.publish.model.GetAdRequest.createParam(java.lang.String, java.util.List<java.lang.String>, boolean):java.lang.StringBuilder
     arg types: [java.lang.String, java.util.List<java.lang.String>, int]
     candidates:
      com.startapp.android.publish.model.GetAdRequest.createParam(java.lang.String, java.lang.String, boolean):java.lang.StringBuilder
      com.startapp.android.publish.model.GetAdRequest.createParam(java.lang.String, java.util.List<java.lang.String>, boolean):java.lang.StringBuilder */
    public String toQueryString() {
        StringBuilder sb = new StringBuilder();
        try {
            sb.append("?type=").append(URLEncoder.encode(this.type, "UTF-8"));
            sb.append((CharSequence) createParam("publisherId", this.publisherId, true));
            sb.append((CharSequence) createParam("productId", this.productId, true));
            sb.append((CharSequence) createParam("os", this.os, true));
            sb.append((CharSequence) createParam("sdkVersion", this.sdkVersion, false));
            sb.append((CharSequence) createParam("packageId", this.packageId, false));
            sb.append((CharSequence) createParam("testMode", Boolean.toString(this.testMode), false));
            sb.append((CharSequence) createParam("userId", this.userId, false));
            sb.append((CharSequence) createParam("isp", this.isp, false));
            sb.append((CharSequence) createParam("longitude", Double.toString(this.longitude), false));
            sb.append((CharSequence) createParam("latitude", Double.toString(this.latitude), false));
            sb.append((CharSequence) createParam("gender", this.gender, false));
            sb.append((CharSequence) createParam("age", Integer.toString(this.age), false));
            sb.append((CharSequence) createParam("keywords", this.keywords, false));
            sb.append((CharSequence) createParam("template", this.template, false));
            sb.append((CharSequence) createParam("adsNumber", Integer.toString(this.adsNumber), false));
            sb.append((CharSequence) createParam("model", this.model, false));
            sb.append((CharSequence) createParam("manufacturer", this.manufacturer, false));
            sb.append((CharSequence) createParam("deviceVersion", this.deviceVersion, false));
            sb.append((CharSequence) createParam("subPublisherId", this.subPublisherId, false));
            sb.append((CharSequence) createParam("subProductId", this.subProductId, false));
            sb.append((CharSequence) createParam("width", Integer.toString(this.width), false));
            sb.append((CharSequence) createParam("height", Integer.toString(this.height), false));
            sb.append((CharSequence) createParam("category", this.categories, false));
            sb.append((CharSequence) createParam("categoryExclude", this.categoriesExclude, false));
            return sb.toString().replace("+", "%20");
        } catch (UnsupportedEncodingException e) {
            throw new e("failed encoding value: [" + this.type + "]", e);
        }
    }

    public String toString() {
        return "GetAdRequest [publisherId=" + this.publisherId + ", productId=" + this.productId + ", type=" + this.type + ", testMode=" + this.testMode + ", userId=" + this.userId + ", packageId=" + this.packageId + ", model=" + this.model + ", manufacturer=" + this.manufacturer + ", deviceVersion=" + this.deviceVersion + ", isp=" + this.isp + ", longitude=" + this.longitude + ", latitude=" + this.latitude + ", gender=" + this.gender + ", age=" + this.age + ", keywords=" + this.keywords + ", template=" + this.template + ", adsNumber=" + this.adsNumber + ", subPublisherId=" + this.subPublisherId + ", subProductId=" + this.subProductId + ", os=" + this.os + ", width=" + this.width + ", height=" + this.height + ", categories=" + this.categories + ", categoriesExclude=" + this.categoriesExclude + ", sdkVersion=" + this.sdkVersion + "]";
    }
}
