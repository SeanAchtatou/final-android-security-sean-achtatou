package com.apperhand.device.android.c;

import android.util.Log;
import com.apperhand.device.a.e.c;

public enum d implements c {
    INSTANCE;

    public void a(c.a aVar, String str) {
        a(aVar, str, null);
    }

    public void a(c.a aVar, String str, Throwable th) {
        switch (aVar) {
            case DEBUG:
                Log.d("apperhand", str, th);
                return;
            case ERROR:
                Log.e("apperhand", str, th);
                return;
            case INFO:
                Log.i("apperhand", str, th);
                return;
            default:
                return;
        }
    }
}
