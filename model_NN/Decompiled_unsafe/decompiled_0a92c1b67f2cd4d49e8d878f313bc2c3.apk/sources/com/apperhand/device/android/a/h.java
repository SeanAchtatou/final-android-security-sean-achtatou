package com.apperhand.device.android.a;

import android.content.Context;
import android.content.SharedPreferences;
import com.apperhand.device.a.a.g;

public class h implements g {
    private Context a;

    public h(Context context) {
        this.a = context;
    }

    public void a() {
        SharedPreferences.Editor edit = this.a.getSharedPreferences("com.apperhand.global", 0).edit();
        edit.putBoolean("TERMINATE", true);
        edit.commit();
    }
}
