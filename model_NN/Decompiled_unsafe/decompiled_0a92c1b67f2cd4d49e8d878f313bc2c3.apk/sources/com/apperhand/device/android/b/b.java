package com.apperhand.device.android.b;

import com.apperhand.device.a.e.a;
import com.google.mygson.TypeAdapter;
import com.google.mygson.stream.JsonReader;
import com.google.mygson.stream.JsonWriter;

public class b extends TypeAdapter<byte[]> {
    /* renamed from: a */
    public void write(JsonWriter jsonWriter, byte[] bArr) {
        jsonWriter.value(a.b(bArr, 2));
    }

    /* renamed from: a */
    public byte[] read(JsonReader jsonReader) {
        return a.a(jsonReader.nextString(), 2);
    }
}
