package com.apperhand.device.android.c;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.ShapeDrawable;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

public class f {
    public static View a(Context context) {
        float f = context.getResources().getDisplayMetrics().density;
        LinearLayout linearLayout = new LinearLayout(context);
        b bVar = new b();
        ShapeDrawable shapeDrawable = new ShapeDrawable();
        shapeDrawable.setShape(bVar);
        shapeDrawable.getPaint().setColor(-805306368);
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(-2, -2);
        layoutParams.gravity = 17;
        linearLayout.setLayoutParams(layoutParams);
        linearLayout.setOrientation(0);
        linearLayout.setBaselineAligned(false);
        linearLayout.setBackgroundDrawable(shapeDrawable);
        linearLayout.setPadding((int) ((8.0f * f) + 0.5f), (int) ((10.0f * f) + 0.5f), (int) ((8.0f * f) + 0.5f), (int) ((10.0f * f) + 0.5f));
        ProgressBar progressBar = new ProgressBar(context);
        LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(-2, -2);
        layoutParams2.setMargins(0, 0, (int) ((f * 12.0f) + 0.5f), 0);
        progressBar.setLayoutParams(layoutParams2);
        progressBar.setMax(10000);
        TextView textView = new TextView(context);
        LinearLayout.LayoutParams layoutParams3 = new LinearLayout.LayoutParams(-1, -2);
        layoutParams3.gravity = 16;
        textView.setLayoutParams(layoutParams3);
        textView.setText("Loading...");
        linearLayout.addView(progressBar);
        linearLayout.addView(textView);
        return linearLayout;
    }

    public static View a(Context context, String str, WebView webView, WebView webView2) {
        Drawable drawable;
        LinearLayout linearLayout = new LinearLayout(context);
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(-2, -2);
        layoutParams.gravity = 17;
        layoutParams.setMargins(0, 0, 0, 0);
        linearLayout.setLayoutParams(layoutParams);
        linearLayout.setOrientation(1);
        linearLayout.setPadding(0, 0, 0, 0);
        linearLayout.setBackgroundColor(-16777216);
        float f = context.getResources().getDisplayMetrics().density;
        TextView textView = new TextView(context);
        textView.setLayoutParams(new ViewGroup.LayoutParams(-1, -2));
        textView.setPadding((int) ((2.0f * f) + 0.5f), (int) ((9.0f * f) + 0.5f), (int) ((2.0f * f) + 0.5f), (int) ((9.0f * f) + 0.5f));
        textView.setText(str);
        textView.setTextSize(18.0f);
        textView.setTextColor(-16777216);
        textView.setBackgroundColor(-1644826);
        textView.setGravity(17);
        try {
            drawable = context.getPackageManager().getApplicationIcon(context.getPackageName());
        } catch (Exception e) {
            drawable = null;
        }
        textView.setCompoundDrawablesWithIntrinsicBounds(drawable, (Drawable) null, (Drawable) null, (Drawable) null);
        View view = new View(context);
        view.setLayoutParams(new ViewGroup.LayoutParams(-1, (int) ((f * 3.0f) + 0.5f)));
        view.setBackgroundColor(-3355444);
        LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(-2, 0, 1.0f);
        layoutParams2.setMargins(0, 0, 0, 0);
        webView.setLayoutParams(layoutParams2);
        webView.setScrollBarStyle(0);
        webView.setPadding(0, 0, 0, 0);
        LinearLayout.LayoutParams layoutParams3 = new LinearLayout.LayoutParams(-2, -2);
        layoutParams3.setMargins(0, 0, 0, 0);
        webView2.setLayoutParams(layoutParams3);
        webView2.setScrollBarStyle(0);
        webView2.setPadding(0, 0, 0, 0);
        linearLayout.addView(textView);
        linearLayout.addView(view);
        linearLayout.addView(webView);
        linearLayout.addView(webView2);
        return linearLayout;
    }
}
