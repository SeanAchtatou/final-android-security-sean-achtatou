package com.apperhand.device.android.b;

import com.apperhand.device.a.e.f;
import com.google.mygson.Gson;

public enum c implements d {
    INSTANCE;
    
    private Gson b;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.mygson.Gson.fromJson(java.lang.String, java.lang.Class):T
     arg types: [java.lang.String, java.lang.Class<T>]
     candidates:
      com.google.mygson.Gson.fromJson(com.google.mygson.JsonElement, java.lang.Class):T
      com.google.mygson.Gson.fromJson(com.google.mygson.JsonElement, java.lang.reflect.Type):T
      com.google.mygson.Gson.fromJson(com.google.mygson.stream.JsonReader, java.lang.reflect.Type):T
      com.google.mygson.Gson.fromJson(java.io.Reader, java.lang.Class):T
      com.google.mygson.Gson.fromJson(java.io.Reader, java.lang.reflect.Type):T
      com.google.mygson.Gson.fromJson(java.lang.String, java.lang.reflect.Type):T
      com.google.mygson.Gson.fromJson(java.lang.String, java.lang.Class):T */
    public <T> T a(String str, Class<T> cls) {
        try {
            return this.b.fromJson(str, (Class) cls);
        } catch (Exception e) {
            throw new f(f.a.GENERAL_ERROR, "Could not write JSON, string=[" + str.toString() + "] " + e.getMessage(), e);
        }
    }

    public String a(Object obj) {
        try {
            return this.b.toJson(obj);
        } catch (Exception e) {
            throw new f(f.a.GENERAL_ERROR, "Could not read JSON, object=[" + obj.toString() + "] " + e.getMessage(), e);
        }
    }
}
