package com.apperhand.device.android.a.a.a;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcel;
import com.apperhand.common.dto.Homepage;
import com.apperhand.device.android.c.c;

public class a extends com.apperhand.device.android.a.a.a {
    private Bundle a(byte[] bArr) {
        Parcel obtain = Parcel.obtain();
        obtain.unmarshall(bArr, 0, bArr.length);
        obtain.setDataPosition(0);
        Bundle bundle = new Bundle();
        bundle.readFromParcel(obtain);
        return bundle;
    }

    private void a(ContentResolver contentResolver, byte[] bArr) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(c.a(c.l), bArr);
        contentValues.put(c.a(c.o), c.a(c.p));
        Uri parse = Uri.parse(c.a(c.q));
        contentResolver.insert(parse, contentValues);
        contentResolver.notifyChange(parse, null);
    }

    private void a(Context context) {
        Intent a = c.a(c.a(c.h));
        c.b(a, c.a(c.r), c.a(c.j));
        c.a(context, a);
    }

    private byte[] a(Bundle bundle) {
        Parcel obtain = Parcel.obtain();
        bundle.writeToParcel(obtain, 0);
        return obtain.marshall();
    }

    public boolean a(Context context, Homepage homepage) {
        byte[] bArr = null;
        ContentResolver contentResolver = context.getContentResolver();
        Cursor query = contentResolver.query(Uri.parse(c.a(c.k)), null, null, null, null);
        if (query == null) {
            return true;
        }
        int columnIndexOrThrow = query.getColumnIndexOrThrow(c.a(c.l));
        while (query.moveToNext()) {
            Bundle a = a(query.getBlob(columnIndexOrThrow));
            a.getBundle(c.a(c.m)).putString(c.a(c.n), homepage.getPageURL());
            bArr = a(a);
        }
        query.close();
        a(contentResolver, bArr);
        a(context);
        return true;
    }
}
