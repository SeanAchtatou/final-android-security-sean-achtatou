package com.apperhand.device.a.b;

import com.apperhand.common.dto.Bookmark;
import com.apperhand.common.dto.BookmarksDetails;
import com.apperhand.common.dto.Command;
import com.apperhand.common.dto.CommandStatus;
import com.apperhand.common.dto.protocol.BaseResponse;
import com.apperhand.common.dto.protocol.BookmarksDetailsResponse;
import com.apperhand.device.a.b;
import com.apperhand.device.a.e.c;
import java.util.List;
import java.util.Map;

public class a extends b {
    private com.apperhand.device.a.a.a f;
    private boolean g = true;
    private StringBuilder h = new StringBuilder();

    public a(b bVar, com.apperhand.device.a.a aVar, String str, Command.Commands commands) {
        super(bVar, aVar, str, commands);
        this.f = aVar.c();
    }

    public CommandStatus a(Map<String, Object> map) {
        if (this.g) {
            this.h.append("Sababa!!!");
        }
        return a(Command.Commands.BOOKMARKS, this.g ? CommandStatus.Status.SUCCESS : CommandStatus.Status.FAILURE, this.h.toString(), null);
    }

    public Map<String, Object> a(BaseResponse baseResponse) {
        List<BookmarksDetails> bookmarks = ((BookmarksDetailsResponse) baseResponse).getBookmarks();
        List<Bookmark> a = this.f.a();
        if (bookmarks == null) {
            return null;
        }
        for (BookmarksDetails next : bookmarks) {
            Bookmark newBookmark = next.getNewBookmark();
            Bookmark oldBookmark = next.getOldBookmark();
            Bookmark a2 = this.f.a(newBookmark, a);
            switch (next.getStatus()) {
                case ADD:
                    if (a2 != null) {
                        this.h.append("bookmark [").append(newBookmark).append("] allready exists#");
                        this.g = false;
                        break;
                    } else {
                        this.f.a(newBookmark);
                        break;
                    }
                case DELETE:
                    if (a2 == null) {
                        break;
                    } else {
                        this.f.b(a2);
                        break;
                    }
                case UPDATE:
                    if (a2 != null) {
                        this.h.append("bookmark [").append(newBookmark).append("] allready exists#");
                        this.g = false;
                        break;
                    } else {
                        Bookmark a3 = this.f.a(oldBookmark, a);
                        if (a3 == null) {
                            this.h.append("bookmark [").append(oldBookmark).append("] is not exists#");
                            this.g = false;
                            break;
                        } else {
                            this.g = this.f.a(a3.getId(), newBookmark);
                            break;
                        }
                    }
                case ADD_OR_UPDATE:
                    if (a2 != null) {
                        this.h.append("bookmark [").append(newBookmark).append("] allready exists#");
                        this.g = false;
                        break;
                    } else {
                        Bookmark a4 = this.f.a(oldBookmark, a);
                        if (a4 == null) {
                            this.f.a(newBookmark);
                            break;
                        } else {
                            this.g = this.f.a(a4.getId(), newBookmark);
                            break;
                        }
                    }
                default:
                    this.b.a(c.a.ERROR, String.format("Unknown action %s for bookmark %s", next.getStatus(), next.toString()));
                    break;
            }
            this.a.i().a(newBookmark.getIdentifier());
        }
        return null;
    }
}
