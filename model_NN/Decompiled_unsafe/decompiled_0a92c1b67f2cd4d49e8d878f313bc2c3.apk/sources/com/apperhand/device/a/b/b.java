package com.apperhand.device.a.b;

import com.apperhand.common.dto.Command;
import com.apperhand.common.dto.CommandStatus;
import com.apperhand.common.dto.protocol.BaseResponse;
import com.apperhand.device.a.a;
import com.apperhand.device.a.e.c;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Map;

public abstract class b {
    protected a a;
    protected c b;
    protected String c;
    protected Command.Commands d;
    protected com.apperhand.device.a.b e;
    private Throwable f = null;

    public b(com.apperhand.device.a.b bVar, a aVar, String str, Command.Commands commands) {
        this.a = aVar;
        this.e = bVar;
        this.b = aVar.a();
        this.c = str;
        this.d = commands;
    }

    public CommandStatus a(Command.Commands commands, CommandStatus.Status status, String str, Map<String, Object> map) {
        CommandStatus commandStatus = new CommandStatus();
        commandStatus.setCommand(commands);
        commandStatus.setId(this.c);
        commandStatus.setMessage(str);
        commandStatus.setStatus(status);
        commandStatus.setParameters(map);
        return commandStatus;
    }

    /* access modifiers changed from: protected */
    public abstract CommandStatus a(Map<String, Object> map);

    public abstract Map<String, Object> a(BaseResponse baseResponse);

    public void a(Throwable th) {
        this.f = th;
    }

    public CommandStatus b(Map<String, Object> map) {
        return this.f != null ? c(map) : a(map);
    }

    public CommandStatus c(Map<String, Object> map) {
        StringWriter stringWriter = new StringWriter();
        this.f.printStackTrace(new PrintWriter(stringWriter));
        return a(Command.Commands.UNEXPECTED_EXCEPTION, CommandStatus.Status.EXCEPTION, "Command = [" + this.d + "]" + this.f.getMessage() + "#NL#" + stringWriter.toString(), null);
    }
}
