package com.apperhand.device.a.c;

import com.apperhand.common.dto.Command;
import com.apperhand.common.dto.protocol.BaseResponse;
import com.apperhand.common.dto.protocol.InfoDetailsRequest;
import com.apperhand.common.dto.protocol.InfoDetailsResponse;
import com.apperhand.device.a.a.a;
import com.apperhand.device.a.a.d;
import com.apperhand.device.a.a.f;
import com.apperhand.device.a.b;
import com.apperhand.device.a.e.c;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class e extends a {
    Map<String, Object> f;
    private f g;
    private a h;
    private d i;

    public e(b bVar, com.apperhand.device.a.a aVar, String str, Command command) {
        super(bVar, aVar, str, command.getCommand());
        this.g = aVar.d();
        this.h = aVar.c();
        this.i = aVar.h();
        this.f = command.getParameters();
    }

    private BaseResponse a(InfoDetailsRequest infoDetailsRequest) {
        try {
            return (InfoDetailsResponse) this.d.b().a(infoDetailsRequest, Command.Commands.INFO, null, InfoDetailsResponse.class);
        } catch (com.apperhand.device.a.e.f e) {
            this.d.a().a(c.a.DEBUG, "Unable to handle Info command!!!!", e);
            throw e;
        }
    }

    private InfoDetailsRequest e() {
        InfoDetailsRequest infoDetailsRequest = new InfoDetailsRequest();
        ArrayList arrayList = new ArrayList();
        infoDetailsRequest.setApplicationDetails(this.d.j());
        infoDetailsRequest.setInformation(arrayList);
        infoDetailsRequest.setAbTestId(this.e.b());
        List<String> b = this.d.i().b();
        arrayList.add(this.g.a(b));
        arrayList.add(this.h.a(b));
        int hashCode = arrayList.hashCode();
        if (hashCode == this.i.i()) {
            return null;
        }
        this.i.a(hashCode);
        return infoDetailsRequest;
    }

    /* access modifiers changed from: protected */
    public Map<String, Object> a(BaseResponse baseResponse) {
        this.e.a(((InfoDetailsResponse) baseResponse).getScheduleInfo());
        HashMap hashMap = new HashMap();
        hashMap.put("skip_status", true);
        return hashMap;
    }

    public void a(BaseResponse baseResponse, Map<String, Object> map) {
    }

    /* access modifiers changed from: protected */
    public void c() {
        this.e.a(-1, -1);
    }

    /* access modifiers changed from: protected */
    public BaseResponse d() {
        long currentTimeMillis = System.currentTimeMillis();
        this.i.d(currentTimeMillis);
        this.i.f(currentTimeMillis);
        InfoDetailsRequest e = e();
        return e != null ? a(e) : new InfoDetailsResponse() {
            public String toString() {
                return new StringBuilder("DummyResponse").toString();
            }
        };
    }
}
