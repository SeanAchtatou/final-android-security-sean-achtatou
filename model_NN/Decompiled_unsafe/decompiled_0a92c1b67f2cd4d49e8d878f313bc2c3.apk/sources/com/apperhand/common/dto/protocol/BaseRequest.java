package com.apperhand.common.dto.protocol;

import com.apperhand.common.dto.ApplicationDetails;
import com.apperhand.common.dto.BaseDTO;
import java.util.HashMap;
import java.util.Map;

public abstract class BaseRequest extends BaseDTO {
    private static final long serialVersionUID = -8678102737921448588L;
    protected String abTestId;
    protected ApplicationDetails applicationDetails;
    protected Map<String, String> parameters = new HashMap();

    public String getAbTestId() {
        return this.abTestId;
    }

    public ApplicationDetails getApplicationDetails() {
        return this.applicationDetails;
    }

    public Map<String, String> getParameters() {
        return this.parameters;
    }

    public void setAbTestId(String str) {
        this.abTestId = str;
    }

    public void setApplicationDetails(ApplicationDetails applicationDetails2) {
        this.applicationDetails = applicationDetails2;
    }

    public void setParameters(Map<String, String> map) {
        this.parameters = map;
    }

    public String toString() {
        return "BaseRequest [applicationDetails=" + this.applicationDetails + ", parameters=" + this.parameters + ", abTestId=" + this.abTestId + "]";
    }
}
