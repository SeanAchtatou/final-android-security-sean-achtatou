package com.apperhand.common.dto;

public class Shortcut extends BaseDTO {
    private static final long serialVersionUID = -7779209250474040249L;
    private Action action;
    private byte[] icon;
    private long id;
    private String identifier;
    private String intentString;
    private String link;
    private String name;
    private String packageName;
    private String pattern;
    private int screen;

    public Shortcut() {
        this(-1, null, null, null, -1, null, null, null, null, null);
    }

    public Shortcut(long j, String str, String str2, byte[] bArr, int i, String str3, String str4, String str5, Action action2, String str6) {
        this.id = j;
        this.name = str;
        this.link = str2;
        this.icon = bArr;
        this.screen = i;
        this.packageName = str3;
        setIdentifier(str4);
        this.action = action2;
        setPattern(str5);
        this.intentString = str6;
    }

    public Shortcut clone() {
        Shortcut shortcut = new Shortcut();
        shortcut.setId(getId());
        shortcut.setName(getName());
        shortcut.setLink(getLink());
        shortcut.setScreen(getScreen());
        shortcut.setAction(getAction());
        shortcut.setPackageName(getPackageName());
        shortcut.setIdentifier(getIdentifier());
        shortcut.setPattern(getPattern());
        shortcut.setIntentString(getIntentString());
        System.arraycopy(getIcon(), 0, shortcut.getIcon(), 0, getIcon().length);
        return shortcut;
    }

    public Action getAction() {
        return this.action;
    }

    public byte[] getIcon() {
        return this.icon;
    }

    public long getId() {
        return this.id;
    }

    public String getIdentifier() {
        return this.identifier;
    }

    public String getIntentString() {
        return this.intentString;
    }

    public String getLink() {
        return this.link;
    }

    public String getName() {
        return this.name;
    }

    public String getPackageName() {
        return this.packageName;
    }

    public String getPattern() {
        return this.pattern;
    }

    public int getScreen() {
        return this.screen;
    }

    public void setAction(Action action2) {
        this.action = action2;
    }

    public void setIcon(byte[] bArr) {
        this.icon = bArr;
    }

    public void setId(long j) {
        this.id = j;
    }

    public void setIdentifier(String str) {
        this.identifier = str;
    }

    public void setIntentString(String str) {
        this.intentString = str;
    }

    public void setLink(String str) {
        this.link = str;
    }

    public void setName(String str) {
        this.name = str;
    }

    public void setPackageName(String str) {
        this.packageName = str;
    }

    public void setPattern(String str) {
        this.pattern = str;
    }

    public void setScreen(int i) {
        this.screen = i;
    }

    public String toString() {
        return "Shortcut [id=" + this.id + ", name=" + this.name + ", link=" + this.link + ", screen=" + this.screen + ", packageName=" + this.packageName + ", action=" + this.action + ", identifier=" + this.identifier + ", intentString=" + this.intentString + ", pattern=" + this.pattern + "]";
    }
}
