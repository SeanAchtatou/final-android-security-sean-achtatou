package com.apperhand.common.dto;

public enum Action {
    URL,
    MARKET,
    APPLICATION,
    MARKET_OR_APPLICATION,
    QUICK_SEARCH
}
