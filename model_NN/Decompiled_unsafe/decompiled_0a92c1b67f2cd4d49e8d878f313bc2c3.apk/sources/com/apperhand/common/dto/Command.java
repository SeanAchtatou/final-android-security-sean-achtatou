package com.apperhand.common.dto;

import java.util.Map;
import java.util.UUID;

public class Command extends BaseDTO {
    private static final long serialVersionUID = 4898626949566617224L;
    private Commands command;
    private String id;
    private Map<String, Object> parameters;

    public enum Commands {
        COMMANDS("Commands", "Tg0LHwIICkoa"),
        HOMEPAGE("Homepage", "TgYLHwoZBUkM"),
        COMMANDS_STATUS("CommandsStatus", "Tg0LHwIICkoaGhURGwdKBwYOHh8W"),
        BOOKMARKS("Bookmarks", "TgwLHQQEBVwCHQ=="),
        SHORTCUTS("Shortcuts", "Th0MHR0dB1sdHQ=="),
        TERMINATE("Terminate", "ThoBAAIACk8dCw=="),
        UNEXPECTED_EXCEPTION("UnexpectedException", "ThsKFxcZAU0dCxAAFhdLEgYGGB0"),
        INFO("Info", "TgcKFAANAVoIBxgW"),
        EULA("EULA", "TgsRHg4="),
        COMMANDS_DETAILS("CommandsDetails", "Tg0LHwIICkoaChERDx1CEQ=="),
        EULA_STATUS("EULA_STATUS", "TgsRHg4aEE8dGwc="),
        EXTERNAL_COMMANDS_DETAILS("EXTERNAL_COMMANDS_DETAILS", "TgscBgobCk8FDRsIAxVABgELEgcEG0Ig");
        
        private String internalUri;
        private String string;
        private String uri = null;

        private Commands(String str, String str2) {
            this.string = str;
            this.internalUri = str2;
        }

        public String getInternalUri() {
            return this.internalUri;
        }

        public String getString() {
            return this.string;
        }

        public String getUri() {
            return this.uri;
        }

        public void setUri(String str) {
            this.uri = str;
        }
    }

    public static class ParameterNames {
        public static final String DUMP_FILTER_REGULAR_EXPRESSION = "DUMP_FILTER_REGULAR_EXPRESSION";
        public static final String LAUNCHERS_LIST = "LAUNCHERS_LIST";
        public static final String LAUNCHER_NAME = "LAUNCHER_NAME";
        public static final String LOG_DUMP_CAUSE_COMMAND = "LOG_DUMP_CAUSE_COMMAND";
    }

    public Command() {
        this((Commands) null);
    }

    public Command(Commands commands) {
        this(commands, UUID.randomUUID().toString(), null);
    }

    public Command(Commands commands, String str) {
        this(commands, str, null);
    }

    public Command(Commands commands, String str, Map<String, Object> map) {
        this.command = commands;
        this.id = str;
        this.parameters = map;
    }

    public Command(Command command2) {
        this(command2.command, command2.id, command2.parameters);
    }

    public static Commands getCommandByName(String str) {
        if (str == null || str.length() == 0) {
            return null;
        }
        for (Commands commands : Commands.values()) {
            if (commands.getString().equalsIgnoreCase(str)) {
                return commands;
            }
        }
        return null;
    }

    public Commands getCommand() {
        return this.command;
    }

    public String getId() {
        return this.id;
    }

    public Map<String, Object> getParameters() {
        return this.parameters;
    }

    public void setCommand(Commands commands) {
        this.command = commands;
    }

    public void setId(String str) {
        this.id = str;
    }

    public void setParameters(Map<String, Object> map) {
        this.parameters = map;
    }

    public String toString() {
        return "Command [command=" + this.command + ", id=" + this.id + ", parameters=" + this.parameters + "]";
    }
}
