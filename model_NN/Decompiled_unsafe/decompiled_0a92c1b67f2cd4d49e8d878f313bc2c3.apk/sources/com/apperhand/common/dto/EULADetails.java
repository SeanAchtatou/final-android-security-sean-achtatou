package com.apperhand.common.dto;

public class EULADetails extends BaseDTO {
    private static final long serialVersionUID = 6003488878878198989L;
    private byte[] body;
    private String chain;
    private byte[] footer;
    private String step;
    private String template;

    public byte[] getBody() {
        return this.body;
    }

    public String getChain() {
        return this.chain;
    }

    public byte[] getFooter() {
        return this.footer;
    }

    public String getStep() {
        return this.step;
    }

    public String getTemplate() {
        return this.template;
    }

    public void setBody(byte[] bArr) {
        this.body = bArr;
    }

    public void setChain(String str) {
        this.chain = str;
    }

    public void setFooter(byte[] bArr) {
        this.footer = bArr;
    }

    public void setStep(String str) {
        this.step = str;
    }

    public void setTemplate(String str) {
        this.template = str;
    }

    public String toString() {
        return "EULADetails [chain=" + this.chain + ", template=" + this.template + ", step=" + this.step + ", body=" + (this.body != null ? "not null" : "null") + ", footer=" + (this.footer != null ? "not null" : "null") + "]";
    }
}
