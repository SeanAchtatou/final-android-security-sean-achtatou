package com.apperhand.common.dto;

public class HomepageDetails extends BaseDetails {
    private static final long serialVersionUID = -2272694383591554622L;
    private Homepage newHomepage;
    private Homepage oldHomepage;

    public Homepage getNewHomepage() {
        return this.newHomepage;
    }

    public Homepage getOldHomepage() {
        return this.oldHomepage;
    }

    public void setNewHomepage(Homepage homepage) {
        this.newHomepage = homepage;
    }

    public void setOldHomepage(Homepage homepage) {
        this.oldHomepage = homepage;
    }

    public String toString() {
        return "HompagesDetails [oldHomepage=" + this.oldHomepage + ", newHomepage=" + this.newHomepage + "]";
    }
}
