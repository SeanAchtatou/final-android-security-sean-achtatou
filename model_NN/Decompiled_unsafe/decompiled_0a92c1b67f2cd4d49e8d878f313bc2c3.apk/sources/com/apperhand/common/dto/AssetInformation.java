package com.apperhand.common.dto;

import java.util.Map;

public class AssetInformation extends BaseDTO {
    private static final long serialVersionUID = 3787775034992581869L;
    private Map<String, Object> parameters;
    private int position;
    private State state;
    private String url;

    public enum State {
        EXIST,
        OPTOUT
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        AssetInformation assetInformation = (AssetInformation) obj;
        if (this.parameters == null) {
            if (assetInformation.parameters != null) {
                return false;
            }
        } else if (!this.parameters.equals(assetInformation.parameters)) {
            return false;
        }
        if (this.position != assetInformation.position) {
            return false;
        }
        if (this.state != assetInformation.state) {
            return false;
        }
        return this.url == null ? assetInformation.url == null : this.url.equals(assetInformation.url);
    }

    public Map<String, Object> getParameters() {
        return this.parameters;
    }

    public int getPosition() {
        return this.position;
    }

    public State getState() {
        return this.state;
    }

    public String getUrl() {
        return this.url;
    }

    public int hashCode() {
        int i = 0;
        int hashCode = ((this.state == null ? 0 : this.state.hashCode()) + (((((this.parameters == null ? 0 : this.parameters.hashCode()) + 31) * 31) + this.position) * 31)) * 31;
        if (this.url != null) {
            i = this.url.hashCode();
        }
        return hashCode + i;
    }

    public void setParameters(Map<String, Object> map) {
        this.parameters = map;
    }

    public void setPosition(int i) {
        this.position = i;
    }

    public void setState(State state2) {
        this.state = state2;
    }

    public void setUrl(String str) {
        this.url = str;
    }

    public String toString() {
        return "AssetInformation [url=" + this.url + ", position=" + this.position + ", state=" + this.state + ", parameters=" + this.parameters + "]";
    }
}
