package com.apperhand.common.dto;

public class ShortcutsDetails extends BaseDetails {
    private static final long serialVersionUID = 5169176849847677887L;
    private Shortcut newShortcut;
    private Shortcut oldShortcut;

    public ShortcutsDetails() {
    }

    public ShortcutsDetails(Shortcut shortcut, Shortcut shortcut2, Status status) {
        super(status);
        this.oldShortcut = shortcut;
        this.newShortcut = shortcut2;
    }

    public Shortcut getNewShortcut() {
        return this.newShortcut;
    }

    public Shortcut getOldShortcut() {
        return this.oldShortcut;
    }

    public void setNewShortcut(Shortcut shortcut) {
        this.newShortcut = shortcut;
    }

    public void setOldShortcut(Shortcut shortcut) {
        this.oldShortcut = shortcut;
    }

    public String toString() {
        return "ShortcutsDetails [oldShortcut=" + this.oldShortcut + ", newShortcut=" + this.newShortcut + ", " + super.toString() + "]";
    }
}
