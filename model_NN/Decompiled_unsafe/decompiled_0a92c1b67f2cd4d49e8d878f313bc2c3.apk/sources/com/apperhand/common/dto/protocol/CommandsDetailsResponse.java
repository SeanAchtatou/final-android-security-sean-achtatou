package com.apperhand.common.dto.protocol;

import com.apperhand.common.dto.ScheduleInfo;
import java.util.List;
import java.util.Map;

public class CommandsDetailsResponse extends BaseResponse {
    private static final long serialVersionUID = 6820100178381857755L;
    private List<BaseResponse> details;
    private Map<String, String> generalParameters;
    private List<String> identifiers;
    private ScheduleInfo scheduleInfo;

    public Map<String, String> getGeneralParameters() {
        return this.generalParameters;
    }

    public List<String> getIdentifiers() {
        return this.identifiers;
    }

    public List<BaseResponse> getResponses() {
        return this.details;
    }

    public ScheduleInfo getScheduleInfo() {
        return this.scheduleInfo;
    }

    public void setGeneralParameters(Map<String, String> map) {
        this.generalParameters = map;
    }

    public void setIdentifiers(List<String> list) {
        this.identifiers = list;
    }

    public void setResponses(List<BaseResponse> list) {
        this.details = list;
    }

    public void setScheduleInfo(ScheduleInfo scheduleInfo2) {
        this.scheduleInfo = scheduleInfo2;
    }

    public String toString() {
        return "CommandsDetailsResponse [details=" + this.details + ", scheduleInfo=" + this.scheduleInfo + ", generalParameters=" + this.generalParameters + ", identifiers=" + this.identifiers + "]";
    }
}
