package com.apperhand.common.dto.protocol;

import com.apperhand.common.dto.HomepageDetails;

public class HomepageDetailsResponse extends BaseResponse {
    private static final long serialVersionUID = 8119743496810028796L;
    private HomepageDetails homepageDetails;

    public HomepageDetails getHomepage() {
        return this.homepageDetails;
    }

    public void setHomepage(HomepageDetails homepageDetails2) {
        this.homepageDetails = homepageDetails2;
    }

    public String toString() {
        return "HomepageResponse [homepage=" + this.homepageDetails + ", toString()=" + super.toString() + "]";
    }
}
