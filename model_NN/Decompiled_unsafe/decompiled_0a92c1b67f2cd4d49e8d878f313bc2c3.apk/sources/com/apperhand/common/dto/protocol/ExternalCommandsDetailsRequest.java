package com.apperhand.common.dto.protocol;

public class ExternalCommandsDetailsRequest extends CommandsDetailsRequest {
    private static final long serialVersionUID = -594258816419191689L;
    private String data;

    public String getData() {
        return this.data;
    }

    public void setData(String str) {
        this.data = str;
    }

    public String toString() {
        return "ExternalCommandsDetailsRequest [data=" + this.data + ", super.toString()=" + super.toString() + "]";
    }
}
