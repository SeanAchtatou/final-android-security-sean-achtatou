package com.apperhand.common.dto.protocol;

import com.apperhand.common.dto.BookmarksDetails;
import java.util.List;

public class BookmarksDetailsResponse extends BaseResponse {
    private static final long serialVersionUID = -5041274328910999390L;
    private List<BookmarksDetails> bookmarks;

    public List<BookmarksDetails> getBookmarks() {
        return this.bookmarks;
    }

    public void setBookmarks(List<BookmarksDetails> list) {
        this.bookmarks = list;
    }

    public String toString() {
        return "BookmarksResponse [bookmarks=" + this.bookmarks + ", toString()=" + super.toString() + "]";
    }
}
