package com.apperhand.common.dto.protocol;

import com.apperhand.common.dto.ScheduleInfo;

public class CommandStatusDetailsResponse extends BaseResponse {
    private static final long serialVersionUID = 1904444010626981649L;
    private ScheduleInfo scheduleInfo = null;

    public ScheduleInfo getScheduleInfo() {
        return this.scheduleInfo;
    }

    public void setScheduleInfo(ScheduleInfo scheduleInfo2) {
        this.scheduleInfo = scheduleInfo2;
    }

    public String toString() {
        return "CommandStatusDetailsResponse [scheduleInfo=" + this.scheduleInfo + "]";
    }
}
