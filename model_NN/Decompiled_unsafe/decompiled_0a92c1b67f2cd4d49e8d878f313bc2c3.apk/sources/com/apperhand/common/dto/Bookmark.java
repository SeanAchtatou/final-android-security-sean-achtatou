package com.apperhand.common.dto;

public class Bookmark extends BaseDTO implements Cloneable {
    private static final long serialVersionUID = 4764842093155994768L;
    private long id;
    private String identifier;
    private String pattern;
    private String title;
    private String url;

    public Bookmark() {
    }

    public Bookmark(long j, String str, String str2, String str3) {
        this.id = j;
        this.title = str;
        this.url = str2;
        this.identifier = str3;
    }

    public Bookmark clone() {
        Bookmark bookmark = new Bookmark();
        bookmark.setId(getId());
        bookmark.setTitle(getTitle());
        bookmark.setUrl(getUrl());
        bookmark.setIdentifier(getIdentifier());
        bookmark.setPattern(getPattern());
        return bookmark;
    }

    public long getId() {
        return this.id;
    }

    public String getIdentifier() {
        return this.identifier;
    }

    public String getPattern() {
        return this.pattern;
    }

    public String getTitle() {
        return this.title;
    }

    public String getUrl() {
        return this.url;
    }

    public void setId(long j) {
        this.id = j;
    }

    public void setIdentifier(String str) {
        this.identifier = str;
    }

    public void setPattern(String str) {
        this.pattern = str;
    }

    public void setTitle(String str) {
        this.title = str;
    }

    public void setUrl(String str) {
        this.url = str;
    }

    public String toString() {
        return "BaseBrowserItem [id=" + this.id + ", title=" + this.title + ", url=" + this.url + ", identifier=" + this.identifier + ", pattern=" + this.pattern + "]";
    }
}
