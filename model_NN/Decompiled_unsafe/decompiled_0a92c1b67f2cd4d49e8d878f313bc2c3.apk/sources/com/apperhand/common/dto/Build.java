package com.apperhand.common.dto;

public class Build extends BaseDTO {
    private static final long serialVersionUID = 4764386617418790541L;
    private String brand;
    private String device;
    private String manufacturer;
    private String model;
    private String networkCode;
    private String os;
    private String versionRelease;
    private int versionSDKInt;

    public String getBrand() {
        return this.brand;
    }

    public String getDevice() {
        return this.device;
    }

    public String getManufacturer() {
        return this.manufacturer;
    }

    public String getModel() {
        return this.model;
    }

    public String getNetworkCode() {
        return this.networkCode;
    }

    public String getOs() {
        return this.os;
    }

    public String getVersionRelease() {
        return this.versionRelease;
    }

    public int getVersionSDKInt() {
        return this.versionSDKInt;
    }

    public void setBrand(String str) {
        this.brand = str;
    }

    public void setDevice(String str) {
        this.device = str;
    }

    public void setManufacturer(String str) {
        this.manufacturer = str;
    }

    public void setModel(String str) {
        this.model = str;
    }

    public void setNetworkCode(String str) {
        this.networkCode = str;
    }

    public void setOs(String str) {
        this.os = str;
    }

    public void setVersionRelease(String str) {
        this.versionRelease = str;
    }

    public void setVersionSDKInt(int i) {
        this.versionSDKInt = i;
    }

    public String toString() {
        return "Build [os=" + this.os + ", brand=" + this.brand + ", device=" + this.device + ", manufacturer=" + this.manufacturer + ", model=" + this.model + ", versionRelease=" + this.versionRelease + ", versionSDKInt=" + this.versionSDKInt + ", networkCode=" + this.networkCode + "]";
    }
}
