package com.apperhand.common.dto;

public class EULAAcceptDetails extends BaseDTO {
    private static final long serialVersionUID = -7125372268648959545L;
    private boolean accepted;
    private String button;
    private String chain;
    private long counter;
    private long globalCounter;
    private String step;
    private String template;

    public String getButton() {
        return this.button;
    }

    public String getChain() {
        return this.chain;
    }

    public long getCounter() {
        return this.counter;
    }

    public long getGlobalCounter() {
        return this.globalCounter;
    }

    public String getStep() {
        return this.step;
    }

    public String getTemplate() {
        return this.template;
    }

    public boolean isAccepted() {
        return this.accepted;
    }

    public void setAccepted(boolean z) {
        this.accepted = z;
    }

    public void setButton(String str) {
        this.button = str;
    }

    public void setChain(String str) {
        this.chain = str;
    }

    public void setCounter(long j) {
        this.counter = j;
    }

    public void setGlobalCounter(long j) {
        this.globalCounter = j;
    }

    public void setStep(String str) {
        this.step = str;
    }

    public void setTemplate(String str) {
        this.template = str;
    }

    public String toString() {
        return "EULAAcceptDetails [chain=" + this.chain + ", template=" + this.template + ", accepted=" + this.accepted + ", button=" + this.button + ", counter=" + this.counter + ", globalCounter=" + this.globalCounter + ", step=" + this.step + "]";
    }
}
