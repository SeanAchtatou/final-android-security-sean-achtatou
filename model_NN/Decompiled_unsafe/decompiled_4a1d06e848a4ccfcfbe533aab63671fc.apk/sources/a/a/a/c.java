package a.a.a;

import android.view.View;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import org.a.a;

public class c {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public static final org.a.c f1a = a.a(c.class);
    private static Class b;
    private static Class c;
    private static Method d;
    private static Method e;
    private static Method f;
    private static Method g;
    private Object h;

    static {
        try {
            Class<?> cls = Class.forName("android.widget.ZoomButtonsController");
            b = cls;
            Class<?>[] declaredClasses = cls.getDeclaredClasses();
            int length = declaredClasses.length;
            int i = 0;
            int i2 = 0;
            while (i < length) {
                Class<?> cls2 = declaredClasses[i2];
                if ("OnZoomListener".equals(cls2.getSimpleName())) {
                    c = cls2;
                }
                i2++;
                i = i2;
            }
            d = b.getMethod("setOnZoomListener", c);
            e = b.getMethod("setVisible", Boolean.TYPE);
            f = b.getMethod("setZoomInEnabled", Boolean.TYPE);
            g = b.getMethod("setZoomOutEnabled", Boolean.TYPE);
        } catch (Exception e2) {
            f1a.b(new StringBuilder().insert(0, "no zoom buttons: ").append(e2).toString());
        }
    }

    public c(View view) {
        if (b != null) {
            try {
                this.h = b.getConstructor(View.class).newInstance(view);
            } catch (Exception e2) {
                f1a.d(new StringBuilder().insert(0, "exception instantiating: ").append(e2).toString());
            }
        }
    }

    public final void a(b bVar) {
        if (this.h != null) {
            try {
                a aVar = new a(this, bVar);
                Object newProxyInstance = Proxy.newProxyInstance(c.getClassLoader(), new Class[]{c}, aVar);
                d.invoke(this.h, newProxyInstance);
            } catch (Exception e2) {
                f1a.d(new StringBuilder().insert(0, "setOnZoomListener exception: ").append(e2).toString());
            }
        }
    }

    public final void a(boolean z) {
        if (this.h != null) {
            try {
                e.invoke(this.h, Boolean.valueOf(z));
            } catch (Exception e2) {
                f1a.d(new StringBuilder().insert(0, "setVisible exception: ").append(e2).toString());
            }
        }
    }

    public final void b(boolean z) {
        if (this.h != null) {
            try {
                f.invoke(this.h, Boolean.valueOf(z));
            } catch (Exception e2) {
                f1a.d(new StringBuilder().insert(0, "setZoomInEnabled exception: ").append(e2).toString());
            }
        }
    }

    public final void c(boolean z) {
        if (this.h != null) {
            try {
                g.invoke(this.h, Boolean.valueOf(z));
            } catch (Exception e2) {
                f1a.d(new StringBuilder().insert(0, "setZoomOutEnabled exception: ").append(e2).toString());
            }
        }
    }
}
