package com.jasonkostempski.android.calendar;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import com.biige.client.android.R;
import java.util.Calendar;
import java.util.Date;

public class CalendarView extends LinearLayout {

    /* renamed from: a  reason: collision with root package name */
    private m f300a = new k(this);
    private View.OnClickListener b = new j(this);
    private View.OnClickListener c = new i(this);
    private View.OnClickListener d = new g(this);
    private View.OnClickListener e = new h(this);
    private View.OnClickListener f = new f(this);
    private final int g = 5;
    private final int h = 4;
    private final int i = 3;
    private final int j = 2;
    private final int k = 1;
    private final int l = 0;
    /* access modifiers changed from: private */
    public a m;
    private LinearLayout n;
    private TableLayout o;
    private TableLayout p;
    private Button q;
    private Button r;
    /* access modifiers changed from: private */
    public Button s;
    private b t;
    private o u;
    /* access modifiers changed from: private */
    public int v;
    /* access modifiers changed from: private */
    public int w;
    /* access modifiers changed from: private */
    public int x;

    public CalendarView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a(context);
    }

    static /* synthetic */ int a(CalendarView calendarView, int i2) {
        int i3 = calendarView.w + i2;
        calendarView.w = i3;
        return i3;
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(int i2) {
        int i3 = 8;
        boolean z = false;
        if (this.v != i2) {
            this.v = i2;
            this.p.setVisibility(this.v == 4 ? 0 : 8);
            this.o.setVisibility(this.v == 3 ? 0 : 8);
            LinearLayout linearLayout = this.n;
            if (this.v == 2) {
                i3 = 0;
            }
            linearLayout.setVisibility(i3);
            Button button = this.q;
            if (this.v != 3) {
                z = true;
            }
            button.setEnabled(z);
            c();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, com.jasonkostempski.android.calendar.CalendarView, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    private /* synthetic */ void a(Context context) {
        View inflate = LayoutInflater.from(context).inflate((int) R.layout.calendar, (ViewGroup) this, true);
        this.m = new a();
        this.n = (LinearLayout) inflate.findViewById(R.id.calendarpicker_days);
        this.o = (TableLayout) inflate.findViewById(R.id.calendarpicker_months);
        this.p = (TableLayout) inflate.findViewById(R.id.calendarpicker_years);
        this.q = (Button) inflate.findViewById(R.id.calendarpicker_up);
        this.r = (Button) inflate.findViewById(R.id.calendarpicker_previous);
        this.s = (Button) inflate.findViewById(R.id.calendarpicker_next);
        d();
        String[] d2 = this.m.d();
        int i2 = 0;
        int i3 = 0;
        while (i2 < 7) {
            ViewGroup viewGroup = (ViewGroup) this.n.getChildAt(i3);
            int i4 = 0;
            int i5 = 0;
            while (i4 < 7) {
                if (Boolean.valueOf(i3 == 0).booleanValue()) {
                    ((TextView) viewGroup.getChildAt(i5)).setText(d2[i5]);
                } else {
                    ((Button) viewGroup.getChildAt(i5)).setOnClickListener(this.c);
                }
                i4 = i5 + 1;
                i5 = i4;
            }
            i2 = i3 + 1;
            i3 = i2;
        }
        b();
        String[] e2 = this.m.e();
        int i6 = 0;
        int i7 = 0;
        int i8 = 0;
        while (i6 < 3) {
            TableRow tableRow = (TableRow) this.o.getChildAt(i7);
            int i9 = 0;
            int i10 = i8;
            int i11 = 0;
            while (i11 < 4) {
                TextView textView = (TextView) tableRow.getChildAt(i9);
                textView.setOnClickListener(this.d);
                textView.setText(e2[i10]);
                textView.setTag(Integer.valueOf(i10));
                i11 = i9 + 1;
                i10++;
                i9 = i11;
            }
            i6 = i7 + 1;
            i7 = i6;
            i8 = i10;
        }
        int i12 = 0;
        int i13 = 0;
        while (i12 < 3) {
            TableRow tableRow2 = (TableRow) this.p.getChildAt(i13);
            int i14 = 0;
            int i15 = 0;
            while (i14 < 4) {
                ((TextView) tableRow2.getChildAt(i15)).setOnClickListener(this.e);
                i14 = i15 + 1;
                i15 = i14;
            }
            i12 = i13 + 1;
            i13 = i12;
        }
        this.m.a(this.f300a);
        this.q.setOnClickListener(this.f);
        this.r.setOnClickListener(this.b);
        this.s.setOnClickListener(this.b);
        a(2);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void b() {
        int i2;
        int i3;
        int i4;
        int[] f2 = this.m.f();
        int i5 = 0;
        int i6 = 0;
        int i7 = 1;
        int i8 = -1;
        int i9 = 0;
        while (i9 < f2.length) {
            if (f2[i5] == 1) {
                i8++;
            }
            TextView textView = (TextView) ((ViewGroup) this.n.getChildAt(i7)).getChildAt(i6);
            textView.setText(f2[i5] + "");
            boolean c2 = this.m.c(i8, f2[i5]);
            textView.setEnabled(c2);
            if (!c2) {
                textView.setTextColor(getResources().getColor(R.color.text_invalid));
            } else if (i8 == 0) {
                textView.setTextColor(getResources().getColor(R.color.text));
            } else {
                textView.setTextColor(getResources().getColor(R.color.text_gray));
            }
            textView.setTag(new int[]{i8, f2[i5]});
            int i10 = i6 + 1;
            if (i10 == 7) {
                i2 = i7 + 1;
                i4 = i5;
                i3 = 0;
            } else {
                i2 = i7;
                int i11 = i5;
                i3 = i10;
                i4 = i11;
            }
            i9 = i4 + 1;
            i7 = i2;
            i6 = i3;
            i5 = i9;
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void c() {
        switch (this.v) {
            case 0:
                this.q.setText("ITEM_VIEW");
                return;
            case 1:
                this.q.setText(this.m.a("EEEE, MMMM dd, yyyy"));
                return;
            case 2:
                this.q.setText(this.m.a("MMMM yyyy"));
                return;
            case 3:
                this.q.setText(this.w + "");
                return;
            case 4:
                this.q.setText("DECADE_VIEW");
                return;
            case 5:
                this.q.setText("CENTURY_VIEW");
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void d() {
        this.w = this.m.a();
        this.x = this.m.b();
        this.m.c();
    }

    static /* synthetic */ void i(CalendarView calendarView) {
        if (calendarView.u != null) {
            calendarView.u.a();
        }
    }

    public final Calendar a() {
        return this.m.g();
    }

    public final void a(b bVar) {
        this.t = bVar;
    }

    public final void a(o oVar) {
        this.u = oVar;
    }

    public final void a(Calendar calendar) {
        this.m.a(calendar);
    }

    public final void a(Date date) {
        this.m.a(date);
        b();
    }
}
