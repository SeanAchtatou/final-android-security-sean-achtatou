package com.jasonkostempski.android.calendar;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.widget.Button;
import com.agilebinary.mobilemonitor.client.android.c.c;
import com.biige.client.android.R;
import java.util.Calendar;
import java.util.Date;

public final class n extends Dialog implements DialogInterface.OnCancelListener {

    /* renamed from: a  reason: collision with root package name */
    private l f311a;
    /* access modifiers changed from: private */
    public CalendarView b;
    private Button c;
    /* access modifiers changed from: private */
    public Calendar d = Calendar.getInstance();

    public n(Context context, l lVar) {
        super(context);
        requestWindowFeature(1);
        this.f311a = lVar;
        setOnCancelListener(this);
        setContentView((int) R.layout.calendar_dialog);
        this.b = (CalendarView) findViewById(R.id.calendar_view);
        this.c = (Button) findViewById(R.id.calendar_today);
        Button button = this.c;
        Context context2 = getContext();
        getContext();
        button.setText(context2.getString(R.string.label_calendar_today, c.a().b(this.d.getTimeInMillis())));
        this.c.setOnClickListener(new e(this, lVar));
        this.b.a(new d(this));
        this.b.a(new c(this, lVar));
    }

    public final void a(Calendar calendar, Date date) {
        this.b.a(date);
        this.b.a(calendar);
    }

    public final void onCancel(DialogInterface dialogInterface) {
        dismiss();
        this.f311a.a(null);
    }
}
