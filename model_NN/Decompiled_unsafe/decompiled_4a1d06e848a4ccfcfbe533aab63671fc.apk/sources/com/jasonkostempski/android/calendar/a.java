package com.jasonkostempski.android.calendar;

import android.text.format.DateFormat;
import android.text.format.DateUtils;
import java.util.Calendar;
import java.util.Date;

public final class a {

    /* renamed from: a  reason: collision with root package name */
    private Calendar f301a;
    private Date b;
    private Calendar c = Calendar.getInstance();
    private String[] d;
    private String[] e;
    private m f;
    private Calendar g;
    private Calendar h;

    public a() {
        int i = 0;
        this.c.set(11, 12);
        this.c.set(12, 0);
        this.f301a = Calendar.getInstance();
        this.f301a.set(11, 23);
        this.f301a.set(12, 59);
        this.d = new String[this.c.getActualMaximum(7)];
        this.e = new String[(this.c.getActualMaximum(2) + 1)];
        int i2 = 0;
        int i3 = 0;
        while (i2 < this.d.length) {
            this.d[i3] = DateUtils.getDayOfWeekString(i3 + 1, 30);
            i2 = i3 + 1;
            i3 = i2;
        }
        while (true) {
            int i4 = i;
            if (i < this.e.length) {
                this.e[i4] = DateUtils.getMonthString(i4, 30);
                i = i4 + 1;
            } else {
                return;
            }
        }
    }

    private /* synthetic */ void h() {
        if (this.f != null) {
            this.f.a(this);
        }
    }

    public final int a() {
        return this.c.get(1);
    }

    public final String a(CharSequence charSequence) {
        return DateFormat.format(charSequence, this.c).toString();
    }

    public final void a(int i) {
        if (i != 0) {
            this.c.add(2, i);
            h();
        }
    }

    public final void a(int i, int i2) {
        this.c.set(1, i);
        this.c.set(2, i2);
        h();
    }

    public final void a(m mVar) {
        this.f = mVar;
    }

    public final void a(Calendar calendar) {
        this.c = calendar;
        h();
    }

    public final void a(Date date) {
        System.out.println(this + " / setFirstValidDate: " + date);
        this.b = date;
    }

    public final int b() {
        return this.c.get(2);
    }

    public final void b(int i) {
        if (i != 0) {
            this.c.add(5, i);
            h();
        }
    }

    public final void b(int i, int i2) {
        this.c.add(2, i);
        this.c.set(5, i2);
        h();
    }

    public final int c() {
        return this.c.get(5);
    }

    public final boolean c(int i, int i2) {
        Calendar calendar = (Calendar) this.c.clone();
        calendar.add(2, i);
        calendar.set(5, i2);
        boolean z = calendar.before(this.f301a) && (this.b == null || calendar.getTimeInMillis() >= this.b.getTime());
        System.out.println(new StringBuilder().insert(0, "_firstValidDate:").append(this.b).toString());
        return z;
    }

    public final String[] d() {
        return this.d;
    }

    public final String[] e() {
        return this.e;
    }

    public final int[] f() {
        int i;
        int i2 = 0;
        this.g = null;
        this.h = null;
        int[] iArr = new int[42];
        Calendar calendar = (Calendar) this.c.clone();
        calendar.set(5, 1);
        int i3 = calendar.get(7);
        int actualMaximum = calendar.getActualMaximum(5);
        int i4 = i3 - 1;
        if (i4 > 0) {
            calendar.set(5, -1);
            int actualMaximum2 = calendar.getActualMaximum(5);
            int i5 = i4;
            int i6 = i4;
            int i7 = 0;
            while (i5 > 0) {
                int i8 = (actualMaximum2 - i6) + 1;
                if (i7 == i4) {
                    this.g = (Calendar) calendar.clone();
                    this.g.set(5, i8);
                }
                iArr[i7] = i8;
                i5 = i6 - 1;
                i7++;
                i6 = i5;
            }
            i = i7;
        } else {
            i = 0;
        }
        int i9 = 0;
        int i10 = i;
        while (i2 < actualMaximum) {
            if (i9 == 0 && this.g == null) {
                this.g = (Calendar) calendar.clone();
            }
            iArr[i10] = i9 + 1;
            i2 = i9 + 1;
            i10++;
            i9 = i2;
        }
        int i11 = i10;
        int i12 = 1;
        int i13 = i10;
        while (i11 < iArr.length) {
            if (i10 == i13) {
                iArr[i13] = i12;
            }
            i12++;
            i13++;
            i11 = i10 + 1;
            i10 = i11;
        }
        this.h = (Calendar) this.c.clone();
        this.h.add(2, 1);
        this.h.set(5, iArr[41]);
        return iArr;
    }

    public final Calendar g() {
        return (Calendar) this.c.clone();
    }
}
