package com.agilebinary.mobilemonitor.client.android;

import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import com.agilebinary.mobilemonitor.client.a.b.h;
import com.agilebinary.mobilemonitor.client.a.b.r;
import com.agilebinary.mobilemonitor.client.a.c;
import com.agilebinary.mobilemonitor.client.android.a.a.a;
import com.agilebinary.mobilemonitor.client.android.a.a.d;
import com.agilebinary.mobilemonitor.client.android.a.a.e;
import com.agilebinary.mobilemonitor.client.android.a.g;
import com.agilebinary.mobilemonitor.client.android.a.k;
import com.agilebinary.mobilemonitor.client.android.a.p;
import com.agilebinary.mobilemonitor.client.android.c.b;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public final class i implements g {

    /* renamed from: a  reason: collision with root package name */
    private static final String f188a = b.a();
    private boolean b;
    private p c;
    private k d;
    private MyApplication e;
    private SQLiteStatement f;
    private SQLiteStatement g;
    private SQLiteDatabase h;
    private com.agilebinary.a.a.a.d.c.b i;
    private Map j = new HashMap();
    private String[] k;

    public i(MyApplication myApplication) {
        this.e = myApplication;
        this.c = myApplication.b().d();
        this.d = myApplication.b().e();
    }

    private static /* synthetic */ String a(h hVar) {
        return hVar.d() + "-" + hVar.e() + "-" + hVar.b() + "-" + hVar.c();
    }

    private static /* synthetic */ String a(r rVar) {
        return rVar.b() + "-" + rVar.c() + "-" + rVar.d();
    }

    /* JADX WARNING: Removed duplicated region for block: B:30:0x010e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private /* synthetic */ com.agilebinary.mobilemonitor.client.android.a.a.b b(com.agilebinary.mobilemonitor.client.a.b.h r13) {
        /*
            r12 = this;
            r11 = 0
            r9 = 0
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            int r1 = r13.e()
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = " / "
            java.lang.StringBuilder r0 = r0.append(r1)
            int r1 = r13.b()
            java.lang.StringBuilder r0 = r0.append(r1)
            r0.toString()
            java.lang.String r10 = a(r13)
            java.util.Map r0 = r12.j
            java.lang.Object r0 = r0.get(r10)
            com.agilebinary.mobilemonitor.client.android.a.a.b r0 = (com.agilebinary.mobilemonitor.client.android.a.a.b) r0
            if (r0 != 0) goto L_0x00de
            android.database.sqlite.SQLiteDatabase r0 = r12.h     // Catch:{ SQLiteException -> 0x0101, all -> 0x0112 }
            java.lang.String r1 = com.agilebinary.mobilemonitor.client.android.c.f178a     // Catch:{ SQLiteException -> 0x0101, all -> 0x0112 }
            java.lang.String[] r2 = com.agilebinary.mobilemonitor.client.android.c.k     // Catch:{ SQLiteException -> 0x0101, all -> 0x0112 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ SQLiteException -> 0x0101, all -> 0x0112 }
            r3.<init>()     // Catch:{ SQLiteException -> 0x0101, all -> 0x0112 }
            r4 = 0
            java.lang.String r5 = com.agilebinary.mobilemonitor.client.android.c.j     // Catch:{ SQLiteException -> 0x0101, all -> 0x0112 }
            java.lang.StringBuilder r3 = r3.insert(r4, r5)     // Catch:{ SQLiteException -> 0x0101, all -> 0x0112 }
            java.lang.String r4 = "=? and "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ SQLiteException -> 0x0101, all -> 0x0112 }
            java.lang.String r4 = com.agilebinary.mobilemonitor.client.android.c.c     // Catch:{ SQLiteException -> 0x0101, all -> 0x0112 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ SQLiteException -> 0x0101, all -> 0x0112 }
            java.lang.String r4 = "=? and "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ SQLiteException -> 0x0101, all -> 0x0112 }
            java.lang.String r4 = com.agilebinary.mobilemonitor.client.android.c.d     // Catch:{ SQLiteException -> 0x0101, all -> 0x0112 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ SQLiteException -> 0x0101, all -> 0x0112 }
            java.lang.String r4 = "=? and "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ SQLiteException -> 0x0101, all -> 0x0112 }
            java.lang.String r4 = com.agilebinary.mobilemonitor.client.android.c.e     // Catch:{ SQLiteException -> 0x0101, all -> 0x0112 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ SQLiteException -> 0x0101, all -> 0x0112 }
            java.lang.String r4 = "=? and "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ SQLiteException -> 0x0101, all -> 0x0112 }
            java.lang.String r4 = com.agilebinary.mobilemonitor.client.android.c.f     // Catch:{ SQLiteException -> 0x0101, all -> 0x0112 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ SQLiteException -> 0x0101, all -> 0x0112 }
            java.lang.String r4 = "=?"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ SQLiteException -> 0x0101, all -> 0x0112 }
            java.lang.String r3 = r3.toString()     // Catch:{ SQLiteException -> 0x0101, all -> 0x0112 }
            com.agilebinary.mobilemonitor.client.android.MyApplication r4 = r12.e     // Catch:{ SQLiteException -> 0x0101, all -> 0x0112 }
            boolean r4 = r4.d()     // Catch:{ SQLiteException -> 0x0101, all -> 0x0112 }
            if (r4 == 0) goto L_0x00fa
            java.lang.String r4 = "1"
            r5 = r4
        L_0x0085:
            r4 = 5
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ SQLiteException -> 0x0101, all -> 0x0112 }
            r6 = 0
            r4[r6] = r5     // Catch:{ SQLiteException -> 0x0101, all -> 0x0112 }
            r5 = 1
            int r6 = r13.d()     // Catch:{ SQLiteException -> 0x0101, all -> 0x0112 }
            java.lang.String r6 = java.lang.String.valueOf(r6)     // Catch:{ SQLiteException -> 0x0101, all -> 0x0112 }
            r4[r5] = r6     // Catch:{ SQLiteException -> 0x0101, all -> 0x0112 }
            r5 = 2
            int r6 = r13.e()     // Catch:{ SQLiteException -> 0x0101, all -> 0x0112 }
            java.lang.String r6 = java.lang.String.valueOf(r6)     // Catch:{ SQLiteException -> 0x0101, all -> 0x0112 }
            r4[r5] = r6     // Catch:{ SQLiteException -> 0x0101, all -> 0x0112 }
            r5 = 3
            int r6 = r13.b()     // Catch:{ SQLiteException -> 0x0101, all -> 0x0112 }
            java.lang.String r6 = java.lang.String.valueOf(r6)     // Catch:{ SQLiteException -> 0x0101, all -> 0x0112 }
            r4[r5] = r6     // Catch:{ SQLiteException -> 0x0101, all -> 0x0112 }
            r5 = 4
            int r6 = r13.c()     // Catch:{ SQLiteException -> 0x0101, all -> 0x0112 }
            java.lang.String r6 = java.lang.String.valueOf(r6)     // Catch:{ SQLiteException -> 0x0101, all -> 0x0112 }
            r4[r5] = r6     // Catch:{ SQLiteException -> 0x0101, all -> 0x0112 }
            r5 = 0
            r6 = r5
            r7 = r5
            android.database.Cursor r8 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ SQLiteException -> 0x0101, all -> 0x0112 }
            boolean r0 = r8.moveToNext()     // Catch:{ SQLiteException -> 0x0118, all -> 0x0115 }
            if (r0 == 0) goto L_0x00fe
            com.agilebinary.mobilemonitor.client.android.a.a.b r1 = new com.agilebinary.mobilemonitor.client.android.a.a.b     // Catch:{ SQLiteException -> 0x0118, all -> 0x0115 }
            r0 = 0
            double r2 = r8.getDouble(r0)     // Catch:{ SQLiteException -> 0x0118, all -> 0x0115 }
            r0 = 1
            double r4 = r8.getDouble(r0)     // Catch:{ SQLiteException -> 0x0118, all -> 0x0115 }
            r0 = 2
            double r6 = r8.getDouble(r0)     // Catch:{ SQLiteException -> 0x0118, all -> 0x0115 }
            r1.<init>(r2, r4, r6)     // Catch:{ SQLiteException -> 0x0118, all -> 0x0115 }
            r0 = r1
        L_0x00d9:
            if (r8 == 0) goto L_0x011b
            r8.close()
        L_0x00de:
            if (r0 == 0) goto L_0x00e5
            java.util.Map r1 = r12.j
            r1.put(r10, r0)
        L_0x00e5:
            r12.i = r9
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = ""
            java.lang.StringBuilder r1 = r1.insert(r11, r2)
            java.lang.StringBuilder r1 = r1.append(r0)
            r1.toString()
            return r0
        L_0x00fa:
            java.lang.String r4 = "0"
            r5 = r4
            goto L_0x0085
        L_0x00fe:
            r1 = r9
            r0 = r9
            goto L_0x00d9
        L_0x0101:
            r0 = move-exception
            r1 = r9
        L_0x0103:
            java.lang.Exception r2 = new java.lang.Exception     // Catch:{ all -> 0x0109 }
            r2.<init>(r0)     // Catch:{ all -> 0x0109 }
            throw r2     // Catch:{ all -> 0x0109 }
        L_0x0109:
            r0 = move-exception
            r8 = r1
            r9 = r1
        L_0x010c:
            if (r8 == 0) goto L_0x0111
            r9.close()
        L_0x0111:
            throw r0
        L_0x0112:
            r0 = move-exception
            r8 = r9
            goto L_0x010c
        L_0x0115:
            r0 = move-exception
            r9 = r8
            goto L_0x010c
        L_0x0118:
            r0 = move-exception
            r1 = r8
            goto L_0x0103
        L_0x011b:
            r0 = r1
            goto L_0x00de
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.mobilemonitor.client.android.i.b(com.agilebinary.mobilemonitor.client.a.b.h):com.agilebinary.mobilemonitor.client.android.a.a.b");
    }

    /* JADX WARNING: Removed duplicated region for block: B:30:0x0112  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private /* synthetic */ com.agilebinary.mobilemonitor.client.android.a.a.b b(com.agilebinary.mobilemonitor.client.a.b.r r12) {
        /*
            r11 = this;
            r9 = 0
            java.lang.String r10 = a(r12)
            java.util.Map r0 = r11.j
            java.lang.Object r0 = r0.get(r10)
            com.agilebinary.mobilemonitor.client.android.a.a.b r0 = (com.agilebinary.mobilemonitor.client.android.a.a.b) r0
            if (r0 != 0) goto L_0x00f2
            com.agilebinary.mobilemonitor.client.android.MyApplication r0 = r11.e     // Catch:{ SQLiteException -> 0x0105, all -> 0x0116 }
            boolean r0 = r0.d()     // Catch:{ SQLiteException -> 0x0105, all -> 0x0116 }
            if (r0 == 0) goto L_0x00fc
            java.lang.String r0 = "1"
            r1 = r0
            r0 = r11
        L_0x001b:
            r2 = 4
            java.lang.String[] r2 = new java.lang.String[r2]     // Catch:{ SQLiteException -> 0x0105, all -> 0x0116 }
            r3 = 0
            r2[r3] = r1     // Catch:{ SQLiteException -> 0x0105, all -> 0x0116 }
            r1 = 1
            int r3 = r12.b()     // Catch:{ SQLiteException -> 0x0105, all -> 0x0116 }
            java.lang.String r3 = java.lang.String.valueOf(r3)     // Catch:{ SQLiteException -> 0x0105, all -> 0x0116 }
            r2[r1] = r3     // Catch:{ SQLiteException -> 0x0105, all -> 0x0116 }
            r1 = 2
            int r3 = r12.c()     // Catch:{ SQLiteException -> 0x0105, all -> 0x0116 }
            java.lang.String r3 = java.lang.String.valueOf(r3)     // Catch:{ SQLiteException -> 0x0105, all -> 0x0116 }
            r2[r1] = r3     // Catch:{ SQLiteException -> 0x0105, all -> 0x0116 }
            r1 = 3
            int r3 = r12.d()     // Catch:{ SQLiteException -> 0x0105, all -> 0x0116 }
            java.lang.String r3 = java.lang.String.valueOf(r3)     // Catch:{ SQLiteException -> 0x0105, all -> 0x0116 }
            r2[r1] = r3     // Catch:{ SQLiteException -> 0x0105, all -> 0x0116 }
            r0.k = r2     // Catch:{ SQLiteException -> 0x0105, all -> 0x0116 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ SQLiteException -> 0x0105, all -> 0x0116 }
            r0.<init>()     // Catch:{ SQLiteException -> 0x0105, all -> 0x0116 }
            r1 = 0
            java.lang.String r2 = "querying CDMA table, demo="
            java.lang.StringBuilder r0 = r0.insert(r1, r2)     // Catch:{ SQLiteException -> 0x0105, all -> 0x0116 }
            java.lang.String[] r1 = r11.k     // Catch:{ SQLiteException -> 0x0105, all -> 0x0116 }
            r2 = 0
            r1 = r1[r2]     // Catch:{ SQLiteException -> 0x0105, all -> 0x0116 }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ SQLiteException -> 0x0105, all -> 0x0116 }
            java.lang.String r1 = " bssid="
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ SQLiteException -> 0x0105, all -> 0x0116 }
            java.lang.String[] r1 = r11.k     // Catch:{ SQLiteException -> 0x0105, all -> 0x0116 }
            r2 = 1
            r1 = r1[r2]     // Catch:{ SQLiteException -> 0x0105, all -> 0x0116 }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ SQLiteException -> 0x0105, all -> 0x0116 }
            java.lang.String r1 = " nid="
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ SQLiteException -> 0x0105, all -> 0x0116 }
            java.lang.String[] r1 = r11.k     // Catch:{ SQLiteException -> 0x0105, all -> 0x0116 }
            r2 = 2
            r1 = r1[r2]     // Catch:{ SQLiteException -> 0x0105, all -> 0x0116 }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ SQLiteException -> 0x0105, all -> 0x0116 }
            java.lang.String r1 = " sysid="
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ SQLiteException -> 0x0105, all -> 0x0116 }
            java.lang.String[] r1 = r11.k     // Catch:{ SQLiteException -> 0x0105, all -> 0x0116 }
            r2 = 3
            r1 = r1[r2]     // Catch:{ SQLiteException -> 0x0105, all -> 0x0116 }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ SQLiteException -> 0x0105, all -> 0x0116 }
            r0.toString()     // Catch:{ SQLiteException -> 0x0105, all -> 0x0116 }
            android.database.sqlite.SQLiteDatabase r0 = r11.h     // Catch:{ SQLiteException -> 0x0105, all -> 0x0116 }
            java.lang.String r1 = com.agilebinary.mobilemonitor.client.android.b.f165a     // Catch:{ SQLiteException -> 0x0105, all -> 0x0116 }
            java.lang.String[] r2 = com.agilebinary.mobilemonitor.client.android.b.j     // Catch:{ SQLiteException -> 0x0105, all -> 0x0116 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ SQLiteException -> 0x0105, all -> 0x0116 }
            r3.<init>()     // Catch:{ SQLiteException -> 0x0105, all -> 0x0116 }
            r4 = 0
            java.lang.String r5 = com.agilebinary.mobilemonitor.client.android.b.i     // Catch:{ SQLiteException -> 0x0105, all -> 0x0116 }
            java.lang.StringBuilder r3 = r3.insert(r4, r5)     // Catch:{ SQLiteException -> 0x0105, all -> 0x0116 }
            java.lang.String r4 = "=? and "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ SQLiteException -> 0x0105, all -> 0x0116 }
            java.lang.String r4 = com.agilebinary.mobilemonitor.client.android.b.c     // Catch:{ SQLiteException -> 0x0105, all -> 0x0116 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ SQLiteException -> 0x0105, all -> 0x0116 }
            java.lang.String r4 = "=? and "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ SQLiteException -> 0x0105, all -> 0x0116 }
            java.lang.String r4 = com.agilebinary.mobilemonitor.client.android.b.d     // Catch:{ SQLiteException -> 0x0105, all -> 0x0116 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ SQLiteException -> 0x0105, all -> 0x0116 }
            java.lang.String r4 = "=? and "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ SQLiteException -> 0x0105, all -> 0x0116 }
            java.lang.String r4 = com.agilebinary.mobilemonitor.client.android.b.e     // Catch:{ SQLiteException -> 0x0105, all -> 0x0116 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ SQLiteException -> 0x0105, all -> 0x0116 }
            java.lang.String r4 = "=?"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ SQLiteException -> 0x0105, all -> 0x0116 }
            java.lang.String r3 = r3.toString()     // Catch:{ SQLiteException -> 0x0105, all -> 0x0116 }
            java.lang.String[] r4 = r11.k     // Catch:{ SQLiteException -> 0x0105, all -> 0x0116 }
            r5 = 0
            r6 = r5
            r7 = r5
            android.database.Cursor r8 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ SQLiteException -> 0x0105, all -> 0x0116 }
            boolean r0 = r8.moveToNext()     // Catch:{ SQLiteException -> 0x011c, all -> 0x0119 }
            if (r0 == 0) goto L_0x0102
            com.agilebinary.mobilemonitor.client.android.a.a.b r1 = new com.agilebinary.mobilemonitor.client.android.a.a.b     // Catch:{ SQLiteException -> 0x011c, all -> 0x0119 }
            r0 = 0
            double r2 = r8.getDouble(r0)     // Catch:{ SQLiteException -> 0x011c, all -> 0x0119 }
            r0 = 1
            double r4 = r8.getDouble(r0)     // Catch:{ SQLiteException -> 0x011c, all -> 0x0119 }
            r0 = 2
            double r6 = r8.getDouble(r0)     // Catch:{ SQLiteException -> 0x011c, all -> 0x0119 }
            r1.<init>(r2, r4, r6)     // Catch:{ SQLiteException -> 0x011c, all -> 0x0119 }
            r0 = r1
        L_0x00ed:
            if (r8 == 0) goto L_0x011f
            r8.close()
        L_0x00f2:
            if (r0 == 0) goto L_0x00f9
            java.util.Map r1 = r11.j
            r1.put(r10, r0)
        L_0x00f9:
            r11.i = r9
            return r0
        L_0x00fc:
            java.lang.String r0 = "0"
            r1 = r0
            r0 = r11
            goto L_0x001b
        L_0x0102:
            r1 = r9
            r0 = r9
            goto L_0x00ed
        L_0x0105:
            r0 = move-exception
            r1 = r9
        L_0x0107:
            java.lang.Exception r2 = new java.lang.Exception     // Catch:{ all -> 0x010d }
            r2.<init>(r0)     // Catch:{ all -> 0x010d }
            throw r2     // Catch:{ all -> 0x010d }
        L_0x010d:
            r0 = move-exception
            r8 = r1
            r9 = r1
        L_0x0110:
            if (r8 == 0) goto L_0x0115
            r9.close()
        L_0x0115:
            throw r0
        L_0x0116:
            r0 = move-exception
            r8 = r9
            goto L_0x0110
        L_0x0119:
            r0 = move-exception
            r9 = r8
            goto L_0x0110
        L_0x011c:
            r0 = move-exception
            r1 = r8
            goto L_0x0107
        L_0x011f:
            r0 = r1
            goto L_0x00f2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.mobilemonitor.client.android.i.b(com.agilebinary.mobilemonitor.client.a.b.r):com.agilebinary.mobilemonitor.client.android.a.a.b");
    }

    public final void a() {
        synchronized (this) {
            this.h = new a(this, this.e, "location_cache").getWritableDatabase();
            this.f = this.h.compileStatement(String.format("INSERT INTO %1$s (%2$s,%3$s,%4$s,%5$s,%6$s,%7$s,%8$s,%9$s,%10$s) values (?,?,?,?,?,?,?,?,?) ", c.f178a, c.b, c.j, c.c, c.d, c.e, c.f, c.g, c.h, c.i));
            this.g = this.h.compileStatement(String.format("INSERT INTO %1$s (%2$s,%3$s,%4$s,%5$s,%6$s,%7$s,%8$s,%9$s) values (?,?,?,?,?,?,?,?) ", b.f165a, b.b, b.i, b.d, b.c, b.e, b.f, b.g, b.h));
            SQLiteStatement compileStatement = this.h.compileStatement(String.format("DELETE FROM %1$s  where %2$s<?", c.f178a, c.b));
            SQLiteStatement compileStatement2 = this.h.compileStatement(String.format("DELETE FROM %1$s  where %2$s<?", b.f165a, b.b));
            compileStatement.bindLong(1, System.currentTimeMillis() - 1209600000);
            compileStatement.execute();
            compileStatement.close();
            compileStatement2.bindLong(1, System.currentTimeMillis() - 1209600000);
            compileStatement2.execute();
            compileStatement2.close();
        }
    }

    public final void a(com.agilebinary.a.a.a.d.c.b bVar) {
        this.i = bVar;
    }

    public final void a(List list) {
        synchronized (this) {
            this.c = this.e.b().d();
            this.b = false;
            try {
                ArrayList arrayList = new ArrayList();
                HashSet hashSet = new HashSet();
                HashSet hashSet2 = new HashSet();
                Iterator it = list.iterator();
                loop0:
                while (true) {
                    while (it.hasNext()) {
                        c cVar = (c) it.next();
                        if (cVar.a() && (cVar instanceof h)) {
                            h hVar = (h) cVar;
                            com.agilebinary.mobilemonitor.client.android.a.a.b b2 = b(hVar);
                            if (b2 == null) {
                                String a2 = a(hVar);
                                if (!hashSet.contains(a2)) {
                                    arrayList.add(new com.agilebinary.mobilemonitor.client.android.a.a.c(hVar.d(), hVar.e(), hVar.b(), hVar.c()));
                                    hashSet.add(a2);
                                }
                                hashSet2.add(cVar);
                            } else {
                                cVar.a(b2);
                            }
                        }
                    }
                    break loop0;
                }
                if (arrayList.size() > 0) {
                    List a3 = this.c.a(this.e.a(), arrayList, this);
                    if (!this.b && a3 != null) {
                        Iterator it2 = a3.iterator();
                        for (Iterator it3 = it2; it3.hasNext(); it3 = it2) {
                            a aVar = (a) it2.next();
                            com.agilebinary.mobilemonitor.client.android.a.a.b b3 = aVar.b();
                            new StringBuilder().insert(0, "").append(b3).toString();
                            this.f.bindLong(1, System.currentTimeMillis());
                            this.f.bindLong(2, this.e.d() ? 1 : 0);
                            this.f.bindLong(3, (long) aVar.c());
                            this.f.bindLong(4, (long) aVar.d());
                            this.f.bindLong(5, (long) aVar.e());
                            this.f.bindLong(6, (long) aVar.f());
                            this.f.bindDouble(7, b3.a());
                            this.f.bindDouble(8, b3.b());
                            this.f.bindDouble(9, b3.c());
                            this.f.executeInsert();
                        }
                        Iterator it4 = hashSet2.iterator();
                        loop3:
                        while (true) {
                            Iterator it5 = it4;
                            while (true) {
                                if (!it5.hasNext()) {
                                    break loop3;
                                }
                                c cVar2 = (c) it4.next();
                                if (cVar2 instanceof h) {
                                    cVar2.a(b((h) cVar2));
                                    it5 = it4;
                                }
                            }
                        }
                    }
                }
                ArrayList arrayList2 = new ArrayList();
                HashSet hashSet3 = new HashSet();
                HashSet hashSet4 = new HashSet();
                Iterator it6 = list.iterator();
                loop5:
                while (true) {
                    Iterator it7 = it6;
                    while (it7.hasNext()) {
                        c cVar3 = (c) it6.next();
                        if (cVar3.a() && (cVar3 instanceof r)) {
                            r rVar = (r) cVar3;
                            com.agilebinary.mobilemonitor.client.android.a.a.b b4 = b(rVar);
                            new StringBuilder().insert(0, "").append(rVar.b()).toString();
                            if (b4 == null) {
                                String a4 = a(rVar);
                                if (!hashSet3.contains(a4)) {
                                    arrayList2.add(new d(rVar.b(), rVar.c(), rVar.d()));
                                    hashSet3.add(a4);
                                }
                                hashSet4.add(cVar3);
                                it7 = it6;
                            } else {
                                cVar3.a(b4);
                                it7 = it6;
                            }
                        }
                    }
                    break loop5;
                }
                if (arrayList2.size() > 0) {
                    List a5 = this.d.a(this.e.a(), arrayList2, this);
                    if (!this.b && a5 != null) {
                        Iterator it8 = a5.iterator();
                        for (Iterator it9 = it8; it9.hasNext(); it9 = it8) {
                            e eVar = (e) it8.next();
                            com.agilebinary.mobilemonitor.client.android.a.a.b e2 = eVar.e();
                            new StringBuilder().insert(0, "").append(e2).toString();
                            this.g.bindLong(1, System.currentTimeMillis());
                            this.g.bindLong(2, this.e.d() ? 1 : 0);
                            this.g.bindLong(3, (long) eVar.c());
                            this.g.bindLong(4, (long) eVar.b());
                            this.g.bindLong(5, (long) eVar.d());
                            this.g.bindDouble(6, e2.a());
                            this.g.bindDouble(7, e2.b());
                            this.g.bindDouble(8, e2.c());
                            this.g.executeInsert();
                        }
                        Iterator it10 = hashSet4.iterator();
                        loop8:
                        while (true) {
                            while (true) {
                                if (!it10.hasNext()) {
                                    break loop8;
                                }
                                c cVar4 = (c) it10.next();
                                if (cVar4 instanceof r) {
                                    cVar4.a(b((r) cVar4));
                                }
                            }
                        }
                    }
                }
            } catch (Exception e3) {
                e3.printStackTrace();
            }
        }
        return;
    }

    public final boolean a(c cVar) {
        com.agilebinary.mobilemonitor.client.android.a.a.b bVar = null;
        try {
            if (cVar instanceof r) {
                bVar = b((r) cVar);
            }
            if (cVar instanceof h) {
                bVar = b((h) cVar);
            }
            if (bVar == null) {
                return false;
            }
            cVar.a(bVar);
            return true;
        } catch (Exception e2) {
            e2.printStackTrace();
            return false;
        }
    }

    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void b() {
        /*
            r1 = this;
            monitor-enter(r1)
            android.database.sqlite.SQLiteStatement r0 = r1.f     // Catch:{ Exception -> 0x0015, all -> 0x0012 }
            r0.close()     // Catch:{ Exception -> 0x0015, all -> 0x0012 }
            android.database.sqlite.SQLiteStatement r0 = r1.g     // Catch:{ Exception -> 0x0015, all -> 0x0012 }
            r0.close()     // Catch:{ Exception -> 0x0015, all -> 0x0012 }
            android.database.sqlite.SQLiteDatabase r0 = r1.h     // Catch:{ Exception -> 0x0015, all -> 0x0012 }
            r0.close()     // Catch:{ Exception -> 0x0015, all -> 0x0012 }
        L_0x0010:
            monitor-exit(r1)
            return
        L_0x0012:
            r0 = move-exception
            monitor-exit(r1)
            throw r0
        L_0x0015:
            r0 = move-exception
            goto L_0x0010
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.mobilemonitor.client.android.i.b():void");
    }

    public final void c() {
        synchronized (this) {
            this.b = true;
            if (this.i != null) {
                try {
                    this.i.d();
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            }
        }
        return;
    }
}
