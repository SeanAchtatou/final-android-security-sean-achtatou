package com.agilebinary.mobilemonitor.client.a.b;

import android.content.Context;
import com.agilebinary.mobilemonitor.client.a.a.c;
import com.agilebinary.mobilemonitor.client.a.e;
import com.biige.client.android.R;

public final class p extends n {
    private String c;

    public p(String str, long j, long j2, c cVar, e eVar) {
        super(str, j, j2, cVar, eVar);
        this.c = cVar.g();
    }

    public final CharSequence a(Context context) {
        return null;
    }

    public final String b(Context context) {
        return context.getString(R.string.label_event_location_type_cellbroadcast);
    }

    public final String c(Context context) {
        return this.c;
    }

    public final Double f() {
        return null;
    }

    public final boolean j() {
        return false;
    }

    public final byte k() {
        return 9;
    }
}
