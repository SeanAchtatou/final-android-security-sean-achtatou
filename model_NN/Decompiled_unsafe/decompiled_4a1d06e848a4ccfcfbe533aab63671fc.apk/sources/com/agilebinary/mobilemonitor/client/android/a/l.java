package com.agilebinary.mobilemonitor.client.android.a;

import com.agilebinary.a.a.a.c.d.i;
import com.agilebinary.a.a.a.e.e;
import com.agilebinary.a.a.a.h.b.g;
import com.agilebinary.a.a.a.h.b.h;
import com.agilebinary.a.a.a.j;
import com.agilebinary.mobilemonitor.a.a.a.a;
import com.agilebinary.mobilemonitor.client.android.c.b;
import java.util.Timer;
import java.util.concurrent.TimeUnit;

public final class l {

    /* renamed from: a  reason: collision with root package name */
    private static final String f159a = b.a();
    private static l b;
    private Timer c = new Timer("HttpTimeoutTimer");
    private i d;

    private /* synthetic */ l(e eVar) {
        l lVar;
        com.agilebinary.a.a.a.c.b.a.b bVar = new com.agilebinary.a.a.a.c.b.a.b(new h(), TimeUnit.SECONDS);
        bVar.c();
        bVar.b();
        this.d = new i(bVar, eVar);
        try {
            t.a();
            b bVar2 = new b((byte) 0);
            this.d.r().a().a(new g("http", com.agilebinary.a.a.a.h.b.e.b(), 80));
            this.d.r().a().a(new g("https", bVar2, 443));
            lVar = this;
        } catch (Exception e) {
            a.e(e);
            lVar = this;
        }
        lVar.d.a(new i(this));
        this.d.a(new h(this));
    }

    public static l a() {
        if (b == null) {
            com.agilebinary.a.a.a.e.a aVar = new com.agilebinary.a.a.a.e.a();
            aVar.b("http.connection.timeout", 20000);
            aVar.b("http.socket.timeout", 20000);
            aVar.b("http.socket.linger", 20);
            t.a();
            aVar.a("http.useragent", new StringBuilder().insert(0, "Biige Android Client").append("1.22").toString());
            b = new l(aVar);
        }
        return b;
    }

    public final j a(com.agilebinary.a.a.a.d.c.b bVar) {
        return this.d.a(bVar);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:?, code lost:
        r1.d();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x003e, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x003f, code lost:
        r2 = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:?, code lost:
        r1.d();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x004e, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x004f, code lost:
        com.agilebinary.mobilemonitor.a.a.a.a.e(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x0057, code lost:
        throw new com.agilebinary.mobilemonitor.client.android.a.q(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0018, code lost:
        r0 = e;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x001e A[SYNTHETIC, Splitter:B:12:0x001e] */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0045 A[SYNTHETIC, Splitter:B:29:0x0045] */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x004e A[Catch:{ InterruptedIOException -> 0x005c, IOException -> 0x0058, n -> 0x004e }, ExcHandler: n (r0v2 'e' com.agilebinary.a.a.a.c.b.n A[CUSTOM_DECLARE, Catch:{  }]), Splitter:B:1:0x0001] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:26:0x0040=Splitter:B:26:0x0040, B:14:0x0021=Splitter:B:14:0x0021, B:31:0x0048=Splitter:B:31:0x0048, B:9:0x0019=Splitter:B:9:0x0019} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.agilebinary.mobilemonitor.client.android.a.s a(java.lang.String r5, com.agilebinary.mobilemonitor.client.android.a.g r6) {
        /*
            r4 = this;
            r2 = 0
            com.agilebinary.a.a.a.d.c.a r1 = new com.agilebinary.a.a.a.d.c.a     // Catch:{ InterruptedIOException -> 0x005c, IOException -> 0x0058, n -> 0x004e }
            r1.<init>(r5)     // Catch:{ InterruptedIOException -> 0x005c, IOException -> 0x0058, n -> 0x004e }
            r6.a(r1)     // Catch:{ InterruptedIOException -> 0x0018, IOException -> 0x003e, n -> 0x004e }
            r2 = 300000(0x493e0, double:1.482197E-318)
            com.agilebinary.a.a.a.j r0 = com.agilebinary.mobilemonitor.client.android.a.n.a(r4, r1, r2)     // Catch:{ InterruptedIOException -> 0x0018, IOException -> 0x003e, n -> 0x004e }
            if (r0 != 0) goto L_0x0029
            com.agilebinary.mobilemonitor.client.android.a.q r0 = new com.agilebinary.mobilemonitor.client.android.a.q     // Catch:{ InterruptedIOException -> 0x0018, IOException -> 0x003e, n -> 0x004e }
            r0.<init>()     // Catch:{ InterruptedIOException -> 0x0018, IOException -> 0x003e, n -> 0x004e }
            throw r0     // Catch:{ InterruptedIOException -> 0x0018, IOException -> 0x003e, n -> 0x004e }
        L_0x0018:
            r0 = move-exception
        L_0x0019:
            com.agilebinary.mobilemonitor.a.a.a.a.e(r0)     // Catch:{ all -> 0x0027 }
            if (r1 == 0) goto L_0x0021
            r1.d()     // Catch:{ Exception -> 0x005f }
        L_0x0021:
            com.agilebinary.mobilemonitor.client.android.a.q r0 = new com.agilebinary.mobilemonitor.client.android.a.q     // Catch:{ all -> 0x0027 }
            r0.<init>()     // Catch:{ all -> 0x0027 }
            throw r0     // Catch:{ all -> 0x0027 }
        L_0x0027:
            r0 = move-exception
            throw r0
        L_0x0029:
            com.agilebinary.mobilemonitor.client.android.a.s r2 = new com.agilebinary.mobilemonitor.client.android.a.s     // Catch:{ InterruptedIOException -> 0x0018, IOException -> 0x003e, n -> 0x004e }
            r2.<init>(r0)     // Catch:{ InterruptedIOException -> 0x0018, IOException -> 0x003e, n -> 0x004e }
            boolean r0 = r2.c()     // Catch:{ InterruptedIOException -> 0x0018, IOException -> 0x003e, n -> 0x004e }
            if (r0 == 0) goto L_0x0063
            com.agilebinary.mobilemonitor.client.android.a.q r0 = new com.agilebinary.mobilemonitor.client.android.a.q     // Catch:{ InterruptedIOException -> 0x0018, IOException -> 0x003e, n -> 0x004e }
            int r2 = r2.b()     // Catch:{ InterruptedIOException -> 0x0018, IOException -> 0x003e, n -> 0x004e }
            r0.<init>(r2)     // Catch:{ InterruptedIOException -> 0x0018, IOException -> 0x003e, n -> 0x004e }
            throw r0     // Catch:{ InterruptedIOException -> 0x0018, IOException -> 0x003e, n -> 0x004e }
        L_0x003e:
            r0 = move-exception
            r2 = r0
        L_0x0040:
            com.agilebinary.mobilemonitor.a.a.a.a.e(r0)     // Catch:{ all -> 0x0027 }
            if (r1 == 0) goto L_0x0048
            r1.d()     // Catch:{ Exception -> 0x0061 }
        L_0x0048:
            com.agilebinary.mobilemonitor.client.android.a.q r0 = new com.agilebinary.mobilemonitor.client.android.a.q     // Catch:{ all -> 0x0027 }
            r0.<init>(r2)     // Catch:{ all -> 0x0027 }
            throw r0     // Catch:{ all -> 0x0027 }
        L_0x004e:
            r0 = move-exception
            com.agilebinary.mobilemonitor.a.a.a.a.e(r0)     // Catch:{ all -> 0x0027 }
            com.agilebinary.mobilemonitor.client.android.a.q r1 = new com.agilebinary.mobilemonitor.client.android.a.q     // Catch:{ all -> 0x0027 }
            r1.<init>(r0)     // Catch:{ all -> 0x0027 }
            throw r1     // Catch:{ all -> 0x0027 }
        L_0x0058:
            r0 = move-exception
            r1 = r2
            r2 = r0
            goto L_0x0040
        L_0x005c:
            r0 = move-exception
            r1 = r2
            goto L_0x0019
        L_0x005f:
            r0 = move-exception
            goto L_0x0021
        L_0x0061:
            r0 = move-exception
            goto L_0x0048
        L_0x0063:
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.mobilemonitor.client.android.a.l.a(java.lang.String, com.agilebinary.mobilemonitor.client.android.a.g):com.agilebinary.mobilemonitor.client.android.a.s");
    }

    /* JADX WARNING: Unknown top exception splitter block from list: {B:14:0x0028=Splitter:B:14:0x0028, B:29:0x004c=Splitter:B:29:0x004c} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.agilebinary.mobilemonitor.client.android.a.s a(java.lang.String r5, byte[] r6, com.agilebinary.mobilemonitor.client.android.a.g r7) {
        /*
            r4 = this;
            com.agilebinary.a.a.a.d.c.c r1 = new com.agilebinary.a.a.a.d.c.c
            r1.<init>(r5)
            r7.a(r1)
            if (r6 == 0) goto L_0x0012
            com.agilebinary.a.a.a.g.d r0 = new com.agilebinary.a.a.a.g.d
            r0.<init>(r6)
            r1.a(r0)
        L_0x0012:
            r2 = 300000(0x493e0, double:1.482197E-318)
            com.agilebinary.a.a.a.j r0 = com.agilebinary.mobilemonitor.client.android.a.n.a(r4, r1, r2)     // Catch:{ InterruptedIOException -> 0x0021, IOException -> 0x0045, n -> 0x0052 }
            if (r0 != 0) goto L_0x0030
            com.agilebinary.mobilemonitor.client.android.a.q r0 = new com.agilebinary.mobilemonitor.client.android.a.q     // Catch:{ InterruptedIOException -> 0x0021, IOException -> 0x0045, n -> 0x0052 }
            r0.<init>()     // Catch:{ InterruptedIOException -> 0x0021, IOException -> 0x0045, n -> 0x0052 }
            throw r0     // Catch:{ InterruptedIOException -> 0x0021, IOException -> 0x0045, n -> 0x0052 }
        L_0x0021:
            r0 = move-exception
            com.agilebinary.mobilemonitor.a.a.a.a.e(r0)     // Catch:{ all -> 0x002e }
            r1.d()     // Catch:{ Exception -> 0x005c }
        L_0x0028:
            com.agilebinary.mobilemonitor.client.android.a.q r0 = new com.agilebinary.mobilemonitor.client.android.a.q     // Catch:{ all -> 0x002e }
            r0.<init>()     // Catch:{ all -> 0x002e }
            throw r0     // Catch:{ all -> 0x002e }
        L_0x002e:
            r0 = move-exception
            throw r0
        L_0x0030:
            com.agilebinary.mobilemonitor.client.android.a.s r2 = new com.agilebinary.mobilemonitor.client.android.a.s     // Catch:{ InterruptedIOException -> 0x0021, IOException -> 0x0045, n -> 0x0052 }
            r2.<init>(r0)     // Catch:{ InterruptedIOException -> 0x0021, IOException -> 0x0045, n -> 0x0052 }
            boolean r0 = r2.c()     // Catch:{ InterruptedIOException -> 0x0021, IOException -> 0x0045, n -> 0x0052 }
            if (r0 == 0) goto L_0x0060
            com.agilebinary.mobilemonitor.client.android.a.q r0 = new com.agilebinary.mobilemonitor.client.android.a.q     // Catch:{ InterruptedIOException -> 0x0021, IOException -> 0x0045, n -> 0x0052 }
            int r2 = r2.b()     // Catch:{ InterruptedIOException -> 0x0021, IOException -> 0x0045, n -> 0x0052 }
            r0.<init>(r2)     // Catch:{ InterruptedIOException -> 0x0021, IOException -> 0x0045, n -> 0x0052 }
            throw r0     // Catch:{ InterruptedIOException -> 0x0021, IOException -> 0x0045, n -> 0x0052 }
        L_0x0045:
            r0 = move-exception
            com.agilebinary.mobilemonitor.a.a.a.a.e(r0)     // Catch:{ all -> 0x002e }
            r1.d()     // Catch:{ Exception -> 0x005e }
        L_0x004c:
            com.agilebinary.mobilemonitor.client.android.a.q r1 = new com.agilebinary.mobilemonitor.client.android.a.q     // Catch:{ all -> 0x002e }
            r1.<init>(r0)     // Catch:{ all -> 0x002e }
            throw r1     // Catch:{ all -> 0x002e }
        L_0x0052:
            r0 = move-exception
            com.agilebinary.mobilemonitor.a.a.a.a.e(r0)     // Catch:{ all -> 0x002e }
            com.agilebinary.mobilemonitor.client.android.a.q r1 = new com.agilebinary.mobilemonitor.client.android.a.q     // Catch:{ all -> 0x002e }
            r1.<init>(r0)     // Catch:{ all -> 0x002e }
            throw r1     // Catch:{ all -> 0x002e }
        L_0x005c:
            r0 = move-exception
            goto L_0x0028
        L_0x005e:
            r1 = move-exception
            goto L_0x004c
        L_0x0060:
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.mobilemonitor.client.android.a.l.a(java.lang.String, byte[], com.agilebinary.mobilemonitor.client.android.a.g):com.agilebinary.mobilemonitor.client.android.a.s");
    }

    public final void b() {
        this.d.r().a(1000, TimeUnit.MILLISECONDS);
    }

    public final Timer c() {
        return this.c;
    }
}
