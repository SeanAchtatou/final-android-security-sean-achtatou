package com.agilebinary.mobilemonitor.client.android.ui.a;

import android.os.AsyncTask;
import android.os.PowerManager;
import com.agilebinary.mobilemonitor.client.android.a.g;
import com.agilebinary.mobilemonitor.client.android.a.q;
import com.agilebinary.mobilemonitor.client.android.c.b;
import com.agilebinary.mobilemonitor.client.android.d;
import com.agilebinary.mobilemonitor.client.android.ui.AccountInfoActivity;
import com.agilebinary.mobilemonitor.client.android.ui.BaseActivity;

public final class a extends AsyncTask implements g {

    /* renamed from: a  reason: collision with root package name */
    public static a f208a;
    private static String b = b.a();
    private final BaseActivity c;
    private d d;
    private com.agilebinary.a.a.a.d.c.b e;

    public a(BaseActivity baseActivity) {
        this.c = baseActivity;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.mobilemonitor.client.android.d.<init>(com.agilebinary.mobilemonitor.client.android.a.a.f, boolean):void
     arg types: [com.agilebinary.mobilemonitor.client.android.a.a.f, int]
     candidates:
      com.agilebinary.mobilemonitor.client.android.d.<init>(com.agilebinary.mobilemonitor.client.android.a.a.g, boolean):void
      com.agilebinary.mobilemonitor.client.android.d.<init>(com.agilebinary.mobilemonitor.client.android.a.a.f, boolean):void */
    private /* synthetic */ Void a() {
        this.d = this.c.g.a();
        PowerManager.WakeLock newWakeLock = ((PowerManager) this.c.getSystemService("power")).newWakeLock(6, b);
        newWakeLock.acquire();
        try {
            this.d = new d(this.c.g.b().a().a(this.d.b(), this.d.c(), this), false);
            try {
                newWakeLock.release();
            } catch (Exception e2) {
            }
            f208a = null;
        } catch (q e3) {
            e3.printStackTrace();
            try {
                newWakeLock.release();
            } catch (Exception e4) {
            }
            f208a = null;
        } catch (Throwable th) {
            try {
                newWakeLock.release();
            } catch (Exception e5) {
            }
            f208a = null;
            throw th;
        }
        return null;
    }

    public final void a(com.agilebinary.a.a.a.d.c.b bVar) {
        this.e = bVar;
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ Object doInBackground(Object[] objArr) {
        return a();
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void onPostExecute(Object obj) {
        AccountInfoActivity.a(this.c, this.d);
    }
}
