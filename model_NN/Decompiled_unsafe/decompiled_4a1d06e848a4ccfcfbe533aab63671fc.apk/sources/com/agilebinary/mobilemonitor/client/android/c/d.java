package com.agilebinary.mobilemonitor.client.android.c;

public final class d {

    /* renamed from: a  reason: collision with root package name */
    private static final String f182a = System.getProperty("line.separator");
    private static char[] b = new char[64];
    private static byte[] c = new byte[128];

    static {
        int i = 0;
        char c2 = 'A';
        int i2 = 0;
        while (true) {
            char c3 = c2;
            if (c2 > 'Z') {
                break;
            }
            b[i2] = c3;
            c2 = (char) (c3 + 1);
            i2++;
        }
        char c4 = 'a';
        while (true) {
            char c5 = c4;
            if (c4 > 'z') {
                break;
            }
            b[i2] = c5;
            c4 = (char) (c5 + 1);
            i2++;
        }
        char c6 = '0';
        while (true) {
            char c7 = c6;
            if (c6 > '9') {
                break;
            }
            b[i2] = c7;
            c6 = (char) (c7 + 1);
            i2++;
        }
        b[i2] = '+';
        b[i2 + 1] = '/';
        int i3 = 0;
        int i4 = 0;
        while (i3 < c.length) {
            c[i4] = -1;
            i3 = i4 + 1;
            i4 = i3;
        }
        int i5 = 0;
        while (i < 64) {
            c[b[i5]] = (byte) i5;
            i = i5 + 1;
            i5 = i;
        }
    }

    private /* synthetic */ d() {
    }

    public static byte[] a(char[] cArr, int i) {
        char c2;
        int i2;
        char c3;
        char c4;
        int i3;
        if (i % 4 != 0) {
            throw new IllegalArgumentException("Length of Base64 encoded input string is not a multiple of 4.");
        }
        int i4 = i;
        while (i > 0 && cArr[(i4 + 0) - 1] == '=') {
            i = i4 - 1;
            i4 = i;
        }
        int i5 = (i4 * 3) / 4;
        byte[] bArr = new byte[i5];
        int i6 = i4 + 0;
        int i7 = 0;
        int i8 = 0;
        while (i8 < i6) {
            int i9 = i8 + 1;
            char c5 = cArr[i8];
            int i10 = i9 + 1;
            char c6 = cArr[i9];
            if (i10 < i6) {
                i2 = i10 + 1;
                c2 = cArr[i10];
                i10 = i2;
            } else {
                c2 = 'A';
                i2 = i10;
            }
            if (i10 < i6) {
                i8 = i2 + 1;
                c3 = cArr[i2];
                c4 = c5;
            } else {
                c3 = 'A';
                i8 = i2;
                c4 = c5;
            }
            if (c4 > 127 || c6 > 127 || c2 > 127 || c3 > 127) {
                throw new IllegalArgumentException("Illegal character in Base64 encoded data.");
            }
            byte b2 = c[c5];
            byte b3 = c[c6];
            byte b4 = c[c2];
            byte b5 = c[c3];
            if (b2 < 0 || b3 < 0 || b4 < 0 || b5 < 0) {
                throw new IllegalArgumentException("Illegal character in Base64 encoded data.");
            }
            int i11 = i7 + 1;
            bArr[i7] = (byte) ((b2 << 2) | (b3 >>> 4));
            if (i11 < i5) {
                i3 = i11 + 1;
                bArr[i11] = (byte) (((b3 & 15) << 4) | (b4 >>> 2));
            } else {
                i3 = i11;
            }
            if (i3 < i5) {
                bArr[i3] = (byte) (((b4 & 3) << 6) | b5);
                i7 = i3 + 1;
            } else {
                i7 = i3;
            }
        }
        return bArr;
    }
}
