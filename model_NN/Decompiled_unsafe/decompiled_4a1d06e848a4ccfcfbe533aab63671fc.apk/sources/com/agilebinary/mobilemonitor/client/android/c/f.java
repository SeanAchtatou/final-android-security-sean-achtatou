package com.agilebinary.mobilemonitor.client.android.c;

public final class f {

    /* renamed from: a  reason: collision with root package name */
    private static char[] f183a = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};

    public static byte[] a(char[] cArr) {
        int i = 0;
        int length = cArr.length;
        if ((length & 1) != 0) {
            throw new Exception("Odd number of characters.");
        }
        byte[] bArr = new byte[(length >> 1)];
        int i2 = 0;
        int i3 = 0;
        while (i < length) {
            int i4 = i3 + 1;
            bArr[i2] = (byte) (((Character.digit(cArr[i3], 16) << 4) | Character.digit(cArr[i4], 16)) & 255);
            i2++;
            i = i4 + 1;
            i3 = i;
        }
        return bArr;
    }

    public static char[] a(byte[] bArr) {
        int i = 0;
        int length = bArr.length;
        char[] cArr = new char[(length << 1)];
        int i2 = 0;
        int i3 = 0;
        while (i < length) {
            int i4 = i2 + 1;
            cArr[i2] = f183a[(bArr[i3] & 240) >>> 4];
            i2 = i4 + 1;
            cArr[i4] = f183a[bArr[i3] & 15];
            i = i3 + 1;
            i3 = i;
        }
        return cArr;
    }
}
