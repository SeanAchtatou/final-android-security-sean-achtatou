package com.agilebinary.mobilemonitor.client.android.ui;

import android.app.Dialog;
import android.view.View;

final class br implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ Dialog f256a;
    private /* synthetic */ MapActivity_OSM b;

    br(MapActivity_OSM mapActivity_OSM, Dialog dialog) {
        this.b = mapActivity_OSM;
        this.f256a = dialog;
    }

    public final void onClick(View view) {
        MapActivity_OSM.l(this.b);
        this.f256a.dismiss();
    }
}
