package com.agilebinary.mobilemonitor.client.android.a.a;

import com.agilebinary.mobilemonitor.client.android.c.b;
import java.io.Serializable;
import java.util.logging.Logger;

public class c implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    private static final String f145a = b.a();
    private static final Logger b = Logger.getLogger(c.class.getName());
    private int c;
    private int d;
    private int e;
    private int f;

    public c() {
    }

    public c(int i, int i2, int i3, int i4) {
        this.c = i;
        this.d = i2;
        this.e = i3;
        this.f = i4;
    }

    public String a() {
        return new StringBuilder().insert(0, "{\"mobile_country_code\":\"").append(this.c).append("\", \"").append("mobile_network_code").append("\":\"").append(this.d).append("\", \"").append("cell_id").append("\":\"").append(this.e).append("\", \"").append("location_area_code").append("\":\"").append(this.f).append("\"}").toString();
    }

    public final void b(int i) {
        this.c = i;
    }

    public final int c() {
        return this.c;
    }

    public final void c(int i) {
        this.d = i;
    }

    public final int d() {
        return this.d;
    }

    public final void d(int i) {
        this.e = i;
    }

    public final int e() {
        return this.e;
    }

    public final void e(int i) {
        this.f = i;
    }

    public final int f() {
        return this.f;
    }

    public String toString() {
        return new StringBuilder().insert(0, "GeocodeGsmCell: ").append(a()).toString();
    }
}
