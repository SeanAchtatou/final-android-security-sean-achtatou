package com.agilebinary.mobilemonitor.client.android.ui.map;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import com.agilebinary.mobilemonitor.client.a.b.s;
import java.util.List;

public final class k extends BaseAdapter {

    /* renamed from: a  reason: collision with root package name */
    private List f286a;
    private Context b;

    public k(Context context) {
        this.b = context;
    }

    public final void a(List list) {
        this.f286a = list;
        notifyDataSetChanged();
    }

    public final int getCount() {
        if (this.f286a == null) {
            return 0;
        }
        return this.f286a.size();
    }

    public final Object getItem(int i) {
        return this.f286a.get(i);
    }

    public final long getItemId(int i) {
        return ((s) this.f286a.get(i)).u();
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        MapMarkerSelectDialogRowView mapMarkerSelectDialogRowView;
        MapMarkerSelectDialogRowView mapMarkerSelectDialogRowView2;
        s sVar = (s) this.f286a.get(i);
        if (view == null) {
            mapMarkerSelectDialogRowView = new MapMarkerSelectDialogRowView(this.b, null);
            mapMarkerSelectDialogRowView2 = mapMarkerSelectDialogRowView;
        } else {
            mapMarkerSelectDialogRowView = (MapMarkerSelectDialogRowView) view;
            mapMarkerSelectDialogRowView2 = mapMarkerSelectDialogRowView;
        }
        mapMarkerSelectDialogRowView.a(sVar);
        return mapMarkerSelectDialogRowView2;
    }
}
