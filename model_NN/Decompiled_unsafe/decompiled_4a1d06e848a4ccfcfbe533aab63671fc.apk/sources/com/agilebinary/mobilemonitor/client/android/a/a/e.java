package com.agilebinary.mobilemonitor.client.android.a.a;

import java.io.Serializable;

public final class e extends d implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    private long f147a;
    private int b;
    private b c;

    public final String a() {
        return new StringBuilder().insert(0, "{\"create_date\":\"").append(this.f147a).append("\", \"geocoder_source_id\":\"").append(this.b).append("\", \"").append("base_station_id").append("\":\"").append(super.b()).append("\", \"").append("network_id").append("\":\"").append(super.c()).append("\", \"").append("system_id").append("\":\"").append(super.d()).append("\", \"").append("latitude").append("\":\"").append(this.c.f144a).append("\", \"").append("longitude").append("\":\"").append(this.c.b).append("\", \"").append("accuracy").append("\":\"").append(this.c.c).append("\", \"").append("country").append("\":\"").append(this.c.d).append("\", \"").append("country_code").append("\":\"").append(this.c.e).append("\", \"").append("region").append("\":\"").append(this.c.f).append("\", \"").append("city").append("\":\"").append(this.c.g).append("\", \"").append("street").append("\":\"").append(this.c.h).append("\"}").toString();
    }

    public final void a(long j) {
        this.f147a = j;
    }

    public final void a(b bVar) {
        this.c = bVar;
    }

    public final void d(int i) {
        this.b = i;
    }

    public final b e() {
        return this.c;
    }

    public final String toString() {
        return new StringBuilder().insert(0, "GeocodeCdmaCellLocation: ").append(a()).toString();
    }
}
