package com.agilebinary.mobilemonitor.client.android.ui;

import android.os.Bundle;
import android.widget.TextView;
import com.agilebinary.mobilemonitor.client.android.a.t;
import com.biige.client.android.R;

public class ControlPanelActivity extends BaseLoggedInActivity {
    /* access modifiers changed from: protected */
    public final int a() {
        return R.layout.controlpanel;
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        t.a();
        ((TextView) findViewById(R.id.controlpanel_url)).setText("http://m.biige.com");
    }
}
