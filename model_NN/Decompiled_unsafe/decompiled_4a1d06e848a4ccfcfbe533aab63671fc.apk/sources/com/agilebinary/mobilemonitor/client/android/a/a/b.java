package com.agilebinary.mobilemonitor.client.android.a.a;

import java.io.Serializable;

public final class b implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    double f144a;
    double b;
    double c;
    String d;
    String e;
    String f;
    String g;
    String h;

    public b() {
    }

    public b(double d2, double d3, double d4) {
        this.f144a = d2;
        this.b = d3;
        this.c = d4;
        this.d = null;
        this.e = null;
        this.f = null;
        this.g = null;
        this.h = null;
    }

    public final double a() {
        return this.f144a;
    }

    public final void a(double d2) {
        this.f144a = d2;
    }

    public final double b() {
        return this.b;
    }

    public final void b(double d2) {
        this.b = d2;
    }

    public final double c() {
        return this.c;
    }

    public final void c(double d2) {
        this.c = d2;
    }

    public final String toString() {
        return new StringBuilder().insert(0, "Location: ").append(new StringBuilder().insert(0, "{\"latitude\":\"").append(this.f144a).append("\", \"").append("longitude").append("\":\"").append(this.b).append("\", \"").append("accuracy").append("\":\"").append(this.c).append("\", \"").append("country").append("\":\"").append(this.d).append("\", \"").append("country_code").append("\":\"").append(this.e).append("\", \"").append("region").append("\":\"").append(this.f).append("\", \"").append("city").append("\":\"").append(this.g).append("\", \"").append("street").append("\":\"").append(this.h).append("\"}").toString()).toString();
    }
}
