package com.agilebinary.mobilemonitor.client.android.ui.a;

import android.os.AsyncTask;
import com.agilebinary.mobilemonitor.client.android.MyApplication;
import com.agilebinary.mobilemonitor.client.android.c.b;
import com.agilebinary.mobilemonitor.client.android.ui.BaseActivity;
import com.agilebinary.mobilemonitor.client.android.ui.MainActivity;

public final class g extends AsyncTask implements com.agilebinary.mobilemonitor.client.android.a.g {

    /* renamed from: a  reason: collision with root package name */
    public static g f214a;
    private static String b = b.a();
    private final BaseActivity c;
    private final MyApplication d;
    private com.agilebinary.a.a.a.d.c.b e;

    public g(BaseActivity baseActivity) {
        this.c = baseActivity;
        this.d = baseActivity.g;
    }

    /* JADX WARNING: Removed duplicated region for block: B:33:0x00fe A[Catch:{ q -> 0x0117, all -> 0x0120 }, LOOP:0: B:4:0x000d->B:33:0x00fe, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x008a A[EDGE_INSN: B:52:0x008a->B:16:0x008a ?: BREAK  , SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private /* synthetic */ java.lang.Void b() {
        /*
            r13 = this;
            r12 = 0
            r7 = 0
            com.agilebinary.mobilemonitor.client.android.MyApplication r0 = r13.d
            com.agilebinary.mobilemonitor.client.android.b.j r9 = r0.e()
            byte[] r10 = com.agilebinary.mobilemonitor.client.android.b.j.f174a     // Catch:{ all -> 0x0120 }
            int r11 = r10.length     // Catch:{ all -> 0x0120 }
            r0 = r7
            r8 = r7
        L_0x000d:
            if (r0 >= r11) goto L_0x008a
            byte r3 = r10[r8]
            boolean r0 = r13.isCancelled()     // Catch:{ all -> 0x0120 }
            if (r0 != 0) goto L_0x008a
            long r0 = r9.b(r3)     // Catch:{ all -> 0x0120 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x0120 }
            r2.<init>()     // Catch:{ all -> 0x0120 }
            r4 = 0
            java.lang.String r5 = "TimeOfLastssenEvent...for event type "
            java.lang.StringBuilder r2 = r2.insert(r4, r5)     // Catch:{ all -> 0x0120 }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x0120 }
            java.lang.String r4 = " = "
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ all -> 0x0120 }
            java.lang.StringBuilder r2 = r2.append(r0)     // Catch:{ all -> 0x0120 }
            r2.toString()     // Catch:{ all -> 0x0120 }
            boolean r2 = r13.isCancelled()     // Catch:{ all -> 0x0120 }
            if (r2 != 0) goto L_0x008a
            r4 = 0
            int r2 = (r0 > r4 ? 1 : (r0 == r4 ? 0 : -1))
            if (r2 >= 0) goto L_0x009b
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ q -> 0x0117 }
            r2.<init>()     // Catch:{ q -> 0x0117 }
            r4 = 0
            java.lang.String r5 = ""
            java.lang.StringBuilder r2 = r2.insert(r4, r5)     // Catch:{ q -> 0x0117 }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ q -> 0x0117 }
            r2.toString()     // Catch:{ q -> 0x0117 }
            com.agilebinary.mobilemonitor.client.android.MyApplication r2 = r13.d     // Catch:{ q -> 0x0117 }
            com.agilebinary.mobilemonitor.client.android.a.m r2 = r2.b()     // Catch:{ q -> 0x0117 }
            com.agilebinary.mobilemonitor.client.android.a.a r2 = r2.f()     // Catch:{ q -> 0x0117 }
            com.agilebinary.mobilemonitor.client.android.MyApplication r4 = r13.d     // Catch:{ q -> 0x0117 }
            com.agilebinary.mobilemonitor.client.android.d r4 = r4.a()     // Catch:{ q -> 0x0117 }
            long r0 = r2.a(r4, r3, r13)     // Catch:{ q -> 0x0117 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ q -> 0x0117 }
            r2.<init>()     // Catch:{ q -> 0x0117 }
            r4 = 0
            java.lang.String r5 = "Now, TimeOfLastssenEvent...for event type "
            java.lang.StringBuilder r2 = r2.insert(r4, r5)     // Catch:{ q -> 0x0117 }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ q -> 0x0117 }
            java.lang.String r4 = " = +ref"
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ q -> 0x0117 }
            r2.toString()     // Catch:{ q -> 0x0117 }
            boolean r2 = r13.isCancelled()     // Catch:{ q -> 0x0117 }
            if (r2 == 0) goto L_0x0092
        L_0x008a:
            com.agilebinary.mobilemonitor.client.android.MyApplication r0 = r13.d
            r0.f()
            com.agilebinary.mobilemonitor.client.android.ui.a.g.f214a = r12
            return r12
        L_0x0092:
            r9.b(r3, r0)     // Catch:{ q -> 0x0117 }
            boolean r2 = r13.isCancelled()     // Catch:{ q -> 0x0117 }
            if (r2 != 0) goto L_0x008a
        L_0x009b:
            r4 = r0
            r0 = r13
        L_0x009d:
            boolean r0 = r0.isCancelled()     // Catch:{ q -> 0x0129 }
            if (r0 != 0) goto L_0x008a
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ q -> 0x0129 }
            r0.<init>()     // Catch:{ q -> 0x0129 }
            r1 = 0
            java.lang.String r2 = ""
            java.lang.StringBuilder r0 = r0.insert(r1, r2)     // Catch:{ q -> 0x0129 }
            java.lang.StringBuilder r0 = r0.append(r3)     // Catch:{ q -> 0x0129 }
            r0.toString()     // Catch:{ q -> 0x0129 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ q -> 0x0129 }
            r0.<init>()     // Catch:{ q -> 0x0129 }
            r1 = 0
            java.lang.String r2 = " since "
            java.lang.StringBuilder r0 = r0.insert(r1, r2)     // Catch:{ q -> 0x0129 }
            java.lang.StringBuilder r0 = r0.append(r4)     // Catch:{ q -> 0x0129 }
            r0.toString()     // Catch:{ q -> 0x0129 }
            com.agilebinary.mobilemonitor.client.android.MyApplication r0 = r13.d     // Catch:{ q -> 0x0129 }
            com.agilebinary.mobilemonitor.client.android.a.m r0 = r0.b()     // Catch:{ q -> 0x0129 }
            com.agilebinary.mobilemonitor.client.android.a.c r1 = r0.c()     // Catch:{ q -> 0x0129 }
            com.agilebinary.mobilemonitor.client.android.MyApplication r0 = r13.d     // Catch:{ q -> 0x0129 }
            com.agilebinary.mobilemonitor.client.android.d r2 = r0.a()     // Catch:{ q -> 0x0129 }
            r6 = r13
            int r1 = r1.a(r2, r3, r4, r6)     // Catch:{ q -> 0x0129 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ q -> 0x0130 }
            r0.<init>()     // Catch:{ q -> 0x0130 }
            r2 = 0
            java.lang.String r4 = ""
            java.lang.StringBuilder r0 = r0.insert(r2, r4)     // Catch:{ q -> 0x0130 }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ q -> 0x0130 }
            r0.toString()     // Catch:{ q -> 0x0130 }
            boolean r0 = r13.isCancelled()     // Catch:{ q -> 0x0130 }
            if (r0 != 0) goto L_0x008a
            r0 = r13
        L_0x00f8:
            boolean r0 = r0.isCancelled()     // Catch:{ all -> 0x0120 }
            if (r0 != 0) goto L_0x008a
            r0 = 2
            java.lang.Integer[] r0 = new java.lang.Integer[r0]     // Catch:{ all -> 0x0120 }
            r2 = 0
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ all -> 0x0120 }
            r0[r2] = r3     // Catch:{ all -> 0x0120 }
            r2 = 1
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)     // Catch:{ all -> 0x0120 }
            r0[r2] = r1     // Catch:{ all -> 0x0120 }
            r13.publishProgress(r0)     // Catch:{ all -> 0x0120 }
            int r0 = r8 + 1
            r8 = r0
            goto L_0x000d
        L_0x0117:
            r2 = move-exception
            r4 = r0
            r0 = r2
            r0.printStackTrace()     // Catch:{ all -> 0x0120 }
            r0 = r13
            goto L_0x009d
        L_0x0120:
            r0 = move-exception
            com.agilebinary.mobilemonitor.client.android.MyApplication r1 = r13.d
            r1.f()
            com.agilebinary.mobilemonitor.client.android.ui.a.g.f214a = r12
            throw r0
        L_0x0129:
            r0 = move-exception
            r1 = r7
        L_0x012b:
            r0.printStackTrace()     // Catch:{ all -> 0x0120 }
            r0 = r13
            goto L_0x00f8
        L_0x0130:
            r0 = move-exception
            goto L_0x012b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.mobilemonitor.client.android.ui.a.g.b():java.lang.Void");
    }

    public final void a() {
        cancel(false);
        if (this.e != null) {
            try {
                this.e.d();
            } catch (Exception e2) {
            }
        }
    }

    public final void a(com.agilebinary.a.a.a.d.c.b bVar) {
        this.e = bVar;
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ Object doInBackground(Object[] objArr) {
        return b();
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void onProgressUpdate(Object[] objArr) {
        Integer[] numArr = (Integer[]) objArr;
        MainActivity.a(this.c, (byte) numArr[0].intValue(), numArr[1].intValue());
    }
}
