package com.agilebinary.mobilemonitor.client.android.ui;

import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import com.agilebinary.mobilemonitor.client.a.b.f;
import com.agilebinary.mobilemonitor.client.a.b.o;
import com.agilebinary.mobilemonitor.client.a.b.t;
import com.agilebinary.mobilemonitor.client.android.c.a;
import com.agilebinary.mobilemonitor.client.android.c.c;
import com.biige.client.android.R;

public class EventDetailsActivity_MMS extends EventDetailsActivity_base {

    /* renamed from: a  reason: collision with root package name */
    private TableLayout f196a;
    private TextView b;
    private TextView c;
    private TextView d;
    private TableRow e;
    private TableRow f;
    private TableRow h;
    private LayoutInflater i;
    private ViewGroup k;

    private /* synthetic */ int a(int i2, int i3, String str) {
        TableRow tableRow = (TableRow) this.i.inflate((int) R.layout.eventdetails_row, (ViewGroup) null);
        ((TextView) tableRow.getChildAt(0)).setText(i3);
        ((TextView) tableRow.getChildAt(1)).setText(str);
        this.f196a.addView(tableRow, i2);
        return 1;
    }

    private /* synthetic */ int a(int i2, int i3, String str, String str2) {
        return a(i2, i3, a.a(str, str2));
    }

    private /* synthetic */ int a(int i2, int i3, String[] strArr, String[] strArr2) {
        int i4 = 0;
        while (true) {
            int i5 = i4;
            if (i4 >= strArr2.length) {
                return strArr.length;
            }
            a(i2 + i5, i3, strArr[i5], strArr2[i5]);
            i4 = i5 + 1;
        }
    }

    private /* synthetic */ int a(int i2, t tVar) {
        int i3;
        TableRow tableRow;
        boolean z;
        boolean z2;
        boolean z3;
        TableRow tableRow2 = new TableRow(this);
        if (i2 % 2 == 0) {
            i3 = R.color.background_mms_part_even;
            tableRow = tableRow2;
        } else {
            i3 = R.color.background_mms_part_odd;
            tableRow = tableRow2;
        }
        tableRow.setBackgroundColor(i3);
        tableRow2.setLayoutParams(new TableLayout.LayoutParams(-2, -2));
        if (tVar.d()) {
            TextView textView = (TextView) this.i.inflate((int) R.layout.eventdetails_mms_textpart, (ViewGroup) null);
            TableRow.LayoutParams layoutParams = new TableRow.LayoutParams(-1, -2);
            layoutParams.span = 2;
            textView.setLayoutParams(layoutParams);
            tableRow2.addView(textView);
            textView.setText(tVar.f());
            z = true;
            z2 = true;
        } else {
            z = false;
            z2 = false;
        }
        if (!z && tVar.c() != null) {
            try {
                LinearLayout linearLayout = (LinearLayout) this.i.inflate((int) R.layout.eventdetails_mms_imagepart, (ViewGroup) null);
                TableRow.LayoutParams layoutParams2 = new TableRow.LayoutParams(-1, -2);
                layoutParams2.span = 2;
                linearLayout.setLayoutParams(layoutParams2);
                tableRow2.addView(linearLayout);
                ImageView imageView = (ImageView) linearLayout.getChildAt(0);
                imageView.setAdjustViewBounds(true);
                imageView.setImageBitmap(BitmapFactory.decodeByteArray(tVar.c(), 0, tVar.c().length));
                z3 = true;
            } catch (OutOfMemoryError e2) {
                e2.printStackTrace();
                z3 = z2;
            }
            if (!z3) {
                TextView textView2 = (TextView) this.i.inflate((int) R.layout.eventdetails_mms_textpart, (ViewGroup) null);
                TableRow.LayoutParams layoutParams3 = new TableRow.LayoutParams(-1, -2);
                layoutParams3.span = 2;
                textView2.setLayoutParams(layoutParams3);
                tableRow2.addView(textView2);
                textView2.setText(getString(R.string.label_event_mms_part_cantshow, new Object[]{tVar.a(), tVar.b(), Integer.valueOf(tVar.e())}));
            }
        }
        this.f196a.addView(tableRow2, i2);
        return 1;
    }

    private static /* synthetic */ int a(ViewGroup viewGroup, View view) {
        int childCount = viewGroup.getChildCount();
        int i2 = 0;
        int i3 = 0;
        while (i3 < childCount) {
            if (viewGroup.getChildAt(i2) == view) {
                return i2;
            }
            i2++;
            i3 = i2;
        }
        return -1;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* access modifiers changed from: protected */
    public final void a(ViewGroup viewGroup) {
        this.i = (LayoutInflater) getSystemService("layout_inflater");
        this.i.inflate((int) R.layout.eventdetails_mms, viewGroup, true);
        this.f196a = (TableLayout) findViewById(R.id.eventdetails_mms_table);
        this.b = (TextView) findViewById(R.id.eventdetails_mms_kind);
        this.c = (TextView) findViewById(R.id.eventdetails_mms_time);
        this.d = (TextView) findViewById(R.id.eventdetails_mms_speed);
        this.e = (TableRow) findViewById(R.id.eventdetails_mms_row_kind);
        this.f = (TableRow) findViewById(R.id.eventdetails_mms_row_time);
        this.h = (TableRow) findViewById(R.id.eventdetails_mms_row_speed);
        this.k = viewGroup;
    }

    /* access modifiers changed from: protected */
    public final void a(o oVar) {
        EventDetailsActivity_MMS eventDetailsActivity_MMS;
        int i2 = 0;
        super.a(oVar);
        int a2 = a(this.f196a, this.f);
        int a3 = a(this.f196a, this.h);
        int i3 = 0;
        int i4 = 0;
        while (i3 < (a3 - a2) - 1) {
            this.f196a.removeViewAt(a2 + 1);
            i3 = i4 + 1;
            i4 = i3;
        }
        f fVar = (f) oVar;
        this.b.setText(getResources().getString(fVar.x() == 1 ? R.string.label_event_mms_incoming : R.string.label_event_mms_outgoing));
        this.c.setText(c.a().c(fVar.x() == 1 ? fVar.w() : fVar.v()));
        this.d.setText(a.a(this, fVar));
        if (fVar.x() != 2 || !a.a(fVar)) {
            this.d.setTextColor(getResources().getColor(R.color.text));
            eventDetailsActivity_MMS = this;
        } else {
            this.d.setTextColor(getResources().getColor(R.color.warning));
            eventDetailsActivity_MMS = this;
        }
        int a4 = a(eventDetailsActivity_MMS.f196a, this.f) + 1;
        if (fVar.x() == 1) {
            a4 += a(a4, (int) R.string.label_event_mms_header_from, fVar.b(), fVar.c());
        }
        int a5 = a4 + a(a4, (int) R.string.label_event_mms_header_to, fVar.d(), fVar.e());
        int a6 = a5 + a(a5, (int) R.string.label_event_mms_header_cc, fVar.f(), fVar.g());
        int a7 = a6 + a(a6, (int) R.string.label_event_mms_header_bcc, fVar.h(), fVar.i());
        if (!a.a(fVar.a())) {
            a7 += a(a7, R.string.label_event_mms_header_subject, fVar.a());
        }
        t[] j = fVar.j();
        int length = j.length;
        int i5 = a7;
        int i6 = 0;
        while (i6 < length) {
            i5 += a(i5, j[i2]);
            i6 = i2 + 1;
            i2 = i6;
        }
    }
}
