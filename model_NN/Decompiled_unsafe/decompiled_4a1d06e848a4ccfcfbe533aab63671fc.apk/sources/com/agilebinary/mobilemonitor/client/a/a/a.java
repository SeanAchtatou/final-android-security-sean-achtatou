package com.agilebinary.mobilemonitor.client.a.a;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

public final class a extends FilterInputStream {

    /* renamed from: a  reason: collision with root package name */
    private volatile byte[] f123a;
    private int b;
    private int c;
    private int d = -1;
    private int e;
    private boolean f = false;

    public a(InputStream inputStream, byte[] bArr) {
        super(inputStream);
        this.f123a = bArr;
    }

    /* JADX WARNING: Removed duplicated region for block: B:22:0x0064  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x007e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private /* synthetic */ int a() {
        /*
            r6 = this;
            r3 = -1
            r0 = 0
            int r1 = r6.d
            if (r1 == r3) goto L_0x000f
            int r1 = r6.e
            int r2 = r6.d
            int r1 = r1 - r2
            int r2 = r6.c
            if (r1 < r2) goto L_0x0024
        L_0x000f:
            java.io.InputStream r1 = r6.in
            byte[] r2 = r6.f123a
            int r1 = r1.read(r2)
            if (r1 <= 0) goto L_0x0021
            r6.d = r3
            r6.e = r0
            if (r1 != r3) goto L_0x0022
        L_0x001f:
            r6.b = r0
        L_0x0021:
            return r1
        L_0x0022:
            r0 = r1
            goto L_0x001f
        L_0x0024:
            int r1 = r6.d
            if (r1 != 0) goto L_0x0069
            int r1 = r6.c
            byte[] r2 = r6.f123a
            int r2 = r2.length
            if (r1 <= r2) goto L_0x0069
            byte[] r1 = r6.f123a
            int r1 = r1.length
            int r1 = r1 * 2
            int r2 = r6.c
            if (r1 <= r2) goto L_0x003a
            int r1 = r6.c
        L_0x003a:
            byte[] r1 = new byte[r1]
            byte[] r2 = r6.f123a
            byte[] r3 = r6.f123a
            int r3 = r3.length
            java.lang.System.arraycopy(r2, r0, r1, r0, r3)
            r6.f123a = r1
        L_0x0046:
            r1 = r6
        L_0x0047:
            int r2 = r6.e
            int r3 = r6.d
            int r2 = r2 - r3
            r1.e = r2
            r6.d = r0
            r6.b = r0
            java.io.InputStream r0 = r6.in
            byte[] r1 = r6.f123a
            int r2 = r6.e
            byte[] r3 = r6.f123a
            int r3 = r3.length
            int r4 = r6.e
            int r3 = r3 - r4
            int r1 = r0.read(r1, r2, r3)
            if (r1 > 0) goto L_0x007e
            int r0 = r6.e
        L_0x0066:
            r6.b = r0
            goto L_0x0021
        L_0x0069:
            int r1 = r6.d
            if (r1 <= 0) goto L_0x0046
            byte[] r1 = r6.f123a
            int r2 = r6.d
            byte[] r3 = r6.f123a
            byte[] r4 = r6.f123a
            int r4 = r4.length
            int r5 = r6.d
            int r4 = r4 - r5
            java.lang.System.arraycopy(r1, r2, r3, r0, r4)
            r1 = r6
            goto L_0x0047
        L_0x007e:
            int r0 = r6.e
            int r0 = r0 + r1
            goto L_0x0066
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.mobilemonitor.client.a.a.a.a():int");
    }

    public final int available() {
        int i;
        int i2;
        int available;
        synchronized (this) {
            if (this.f123a == null) {
                throw new IOException("Stream is closed");
            }
            i = this.b;
            i2 = this.e;
            available = this.in.available();
        }
        return (i - i2) + available;
    }

    public final void close() {
        synchronized (this) {
            if (this.in != null) {
                super.close();
                this.in = null;
            }
            this.f123a = null;
            this.f = true;
        }
    }

    public final void mark(int i) {
        synchronized (this) {
            this.c = i;
            this.d = this.e;
        }
    }

    public final boolean markSupported() {
        return true;
    }

    public final int read() {
        byte b2 = -1;
        synchronized (this) {
            if (this.in == null) {
                throw new IOException("Stream is closed");
            } else if (this.e < this.b || a() != -1) {
                if (this.b - this.e > 0) {
                    byte[] bArr = this.f123a;
                    int i = this.e;
                    this.e = i + 1;
                    b2 = bArr[i] & 255;
                }
            }
        }
        return b2;
    }

    public final int read(byte[] bArr, int i, int i2) {
        a aVar;
        int i3;
        int i4;
        int i5;
        a aVar2;
        a aVar3;
        synchronized (this) {
            if (this.f) {
                throw new IOException("Stream is closed");
            } else if (bArr == null) {
                throw new NullPointerException("K0047");
            } else if ((i | i2) < 0 || i > bArr.length - i2) {
                throw new IndexOutOfBoundsException("K002f");
            } else if (i2 == 0) {
                i5 = 0;
            } else if (this.f123a == null) {
                throw new IOException("K0059");
            } else {
                if (this.e < this.b) {
                    if (this.b - this.e >= i2) {
                        aVar3 = this;
                        i5 = i2;
                    } else {
                        i5 = this.b - this.e;
                        aVar3 = this;
                    }
                    System.arraycopy(aVar3.f123a, this.e, bArr, i, i5);
                    this.e += i5;
                    if (!(i5 == i2 || this.in.available() == 0)) {
                        i += i5;
                        i3 = i2 - i5;
                        aVar = this;
                    }
                } else {
                    aVar = this;
                    i3 = i2;
                }
                while (true) {
                    if (aVar.d == -1 && i3 >= this.f123a.length) {
                        i4 = this.in.read(bArr, i, i3);
                        if (i4 == -1) {
                            i5 = i3 == i2 ? -1 : i2 - i3;
                        }
                    } else if (a() == -1) {
                        i5 = i3 == i2 ? -1 : i2 - i3;
                    } else {
                        if (this.b - this.e >= i3) {
                            aVar2 = this;
                            i4 = i3;
                        } else {
                            i4 = this.b - this.e;
                            aVar2 = this;
                        }
                        System.arraycopy(aVar2.f123a, this.e, bArr, i, i4);
                        this.e += i4;
                    }
                    i3 -= i4;
                    if (i3 == 0) {
                        i5 = i2;
                        break;
                    } else if (this.in.available() == 0) {
                        i5 = i2 - i3;
                        break;
                    } else {
                        i += i4;
                        aVar = this;
                    }
                }
            }
        }
        return i5;
    }

    public final void reset() {
        synchronized (this) {
            if (this.f) {
                throw new IOException("Stream is closed");
            } else if (-1 == this.d) {
                throw new IOException("Mark has been invalidated.");
            } else {
                this.e = this.d;
            }
        }
    }

    public final long skip(long j) {
        long j2;
        synchronized (this) {
            if (this.in == null) {
                throw new IOException("K0059");
            }
            if (j < 1) {
                j2 = 0;
            } else if (((long) (this.b - this.e)) >= j) {
                this.e = (int) (((long) this.e) + j);
            } else {
                j2 = (long) (this.b - this.e);
                this.e = this.b;
                if (this.d != -1) {
                    if (j > ((long) this.c)) {
                        this.d = -1;
                    } else if (a() != -1) {
                        if (((long) (this.b - this.e)) >= j - j2) {
                            this.e = (int) ((j - j2) + ((long) this.e));
                        } else {
                            j = j2 + ((long) (this.b - this.e));
                            this.e = this.b;
                        }
                    }
                }
                j = j2 + this.in.skip(j - j2);
            }
            j = j2;
        }
        return j;
    }
}
