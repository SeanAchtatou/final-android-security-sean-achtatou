package com.agilebinary.mobilemonitor.client.android.ui;

import android.app.Dialog;
import android.view.Menu;
import android.widget.Button;
import android.widget.CheckedTextView;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.agilebinary.mobilemonitor.client.a.b.s;
import com.agilebinary.mobilemonitor.client.android.MyApplication;
import com.agilebinary.mobilemonitor.client.android.b.j;
import com.agilebinary.mobilemonitor.client.android.ui.map.a;
import com.agilebinary.mobilemonitor.client.android.ui.map.d;
import com.agilebinary.mobilemonitor.client.android.ui.map.e;
import com.agilebinary.mobilemonitor.client.android.ui.map.f;
import com.agilebinary.mobilemonitor.client.android.ui.map.i;
import com.agilebinary.mobilemonitor.client.android.ui.map.m;
import com.biige.client.android.R;
import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.osmdroid.util.BoundingBoxE6;

public class MapActivity_GOOGLE extends MapActivity {

    /* renamed from: a  reason: collision with root package name */
    private RelativeLayout f204a;
    /* access modifiers changed from: private */
    public MapView b;
    private MapController c;
    /* access modifiers changed from: private */
    public i d;
    /* access modifiers changed from: private */
    public f e;
    /* access modifiers changed from: private */
    public m f;
    /* access modifiers changed from: private */
    public e g;
    /* access modifiers changed from: private */
    public List h;
    /* access modifiers changed from: private */
    public com.agilebinary.mobilemonitor.client.android.b.m i;
    private TextView j;
    private TextView k;
    private TextView l;
    private TextView m;
    private TextView n;
    private ImageButton o;
    private ImageButton p;
    private ImageButton q;
    private ImageButton r;
    private s s;
    private TextView t;
    /* access modifiers changed from: private */
    public c u;
    private MyApplication v;
    /* access modifiers changed from: private */
    public j w;
    private a x = new aa(this);
    /* access modifiers changed from: private */
    public d y;

    /* JADX WARN: Type inference failed for: r6v0, types: [android.content.Context, com.agilebinary.mobilemonitor.client.android.ui.MapActivity_GOOGLE] */
    /* access modifiers changed from: private */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public /* synthetic */ void a(com.agilebinary.mobilemonitor.client.a.b.s r7) {
        /*
            r6 = this;
            r3 = 1
            r2 = 0
            if (r7 != 0) goto L_0x0005
        L_0x0004:
            return
        L_0x0005:
            r6.s = r7
            com.agilebinary.mobilemonitor.client.android.ui.map.e r0 = r6.g
            java.util.List r1 = r6.h
            int r1 = r1.indexOf(r7)
            r0.a(r1)
            android.widget.TextView r0 = r6.j
            com.agilebinary.mobilemonitor.client.android.c.c r1 = com.agilebinary.mobilemonitor.client.android.c.c.a()
            com.agilebinary.mobilemonitor.client.a.b.s r4 = r6.s
            long r4 = r4.u()
            java.lang.String r1 = r1.d(r4)
            r0.setText(r1)
            android.widget.TextView r0 = r6.n
            r1 = 2131099910(0x7f060106, float:1.7812187E38)
            java.lang.Object[] r4 = new java.lang.Object[r3]
            java.lang.Double r5 = r7.f()
            r4[r2] = r5
            java.lang.String r1 = r6.getString(r1, r4)
            r0.setText(r1)
            r0 = r7
            com.agilebinary.mobilemonitor.client.a.d r0 = (com.agilebinary.mobilemonitor.client.a.d) r0
            java.lang.CharSequence r1 = r0.a(r6)
            android.widget.TextView r0 = r6.m
            r0.setText(r1)
            android.widget.TextView r0 = r6.t
            int r1 = r1.length()
            if (r1 != 0) goto L_0x00bf
            r1 = 4
        L_0x004e:
            r0.setVisibility(r1)
            android.widget.TextView r0 = r6.k
            java.lang.String r1 = r7.b(r6)
            r0.setText(r1)
            android.widget.TextView r0 = r6.l
            java.lang.String r1 = r7.c(r6)
            r0.setText(r1)
            android.widget.ImageButton r0 = r6.p
            com.agilebinary.mobilemonitor.client.a.b.s r1 = r6.s
            java.util.List r4 = r6.h
            int r1 = r4.indexOf(r1)
            java.util.List r4 = r6.h
            int r4 = r4.size()
            int r4 = r4 + -1
            if (r1 >= r4) goto L_0x00c1
            r1 = r3
        L_0x0078:
            r0.setEnabled(r1)
            android.widget.ImageButton r0 = r6.o
            com.agilebinary.mobilemonitor.client.a.b.s r1 = r6.s
            java.util.List r4 = r6.h
            int r1 = r4.indexOf(r1)
            if (r1 <= 0) goto L_0x00c3
            r1 = r3
        L_0x0088:
            r0.setEnabled(r1)
            android.widget.ImageButton r0 = r6.q
            com.agilebinary.mobilemonitor.client.a.b.s r1 = r6.s
            java.util.List r4 = r6.h
            int r1 = r4.indexOf(r1)
            if (r1 != 0) goto L_0x00c5
            r1 = r3
        L_0x0098:
            if (r1 != 0) goto L_0x00c7
            r1 = r3
        L_0x009b:
            r0.setEnabled(r1)
            android.widget.ImageButton r0 = r6.r
            com.agilebinary.mobilemonitor.client.a.b.s r1 = r6.s
            java.util.List r4 = r6.h
            int r1 = r4.indexOf(r1)
            java.util.List r4 = r6.h
            int r4 = r4.size()
            int r4 = r4 + -1
            if (r1 != r4) goto L_0x00c9
            r1 = r3
        L_0x00b3:
            if (r1 != 0) goto L_0x00cb
        L_0x00b5:
            r0.setEnabled(r3)
            com.google.android.maps.MapView r0 = r6.b
            r0.invalidate()
            goto L_0x0004
        L_0x00bf:
            r1 = r2
            goto L_0x004e
        L_0x00c1:
            r1 = r2
            goto L_0x0078
        L_0x00c3:
            r1 = r2
            goto L_0x0088
        L_0x00c5:
            r1 = r2
            goto L_0x0098
        L_0x00c7:
            r1 = r2
            goto L_0x009b
        L_0x00c9:
            r1 = r2
            goto L_0x00b3
        L_0x00cb:
            r3 = r2
            goto L_0x00b5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.mobilemonitor.client.android.ui.MapActivity_GOOGLE.a(com.agilebinary.mobilemonitor.client.a.b.s):void");
    }

    static /* synthetic */ void j(MapActivity_GOOGLE mapActivity_GOOGLE) {
        ArrayList arrayList = new ArrayList();
        Iterator it = mapActivity_GOOGLE.h.iterator();
        loop0:
        while (true) {
            while (true) {
                if (!it.hasNext()) {
                    break loop0;
                }
                com.agilebinary.mobilemonitor.client.a.d dVar = (com.agilebinary.mobilemonitor.client.a.d) ((s) it.next());
                if (dVar.j()) {
                    arrayList.add(dVar.h());
                }
            }
        }
        BoundingBoxE6 a2 = BoundingBoxE6.a(arrayList);
        if (arrayList.size() > 0) {
            if (a2.b() > 10) {
                mapActivity_GOOGLE.c.zoomToSpan(a2.c(), a2.d());
            } else {
                mapActivity_GOOGLE.c.zoomToSpan(10000, 10000);
            }
            mapActivity_GOOGLE.c.setCenter(new GeoPoint(a2.a().a(), a2.a().b()));
        }
    }

    /* access modifiers changed from: protected */
    public final void a() {
        a((s) this.h.get(this.h.indexOf(this.s) + 1));
    }

    /* JADX WARN: Type inference failed for: r14v0, types: [android.content.Context, com.agilebinary.mobilemonitor.client.android.ui.MapActivity_GOOGLE] */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public final boolean a(com.agilebinary.mobilemonitor.client.android.ui.ag r15) {
        /*
            r14 = this;
            com.agilebinary.mobilemonitor.client.android.ui.map.d r0 = r14.y
            if (r0 == 0) goto L_0x0006
            r0 = 0
        L_0x0005:
            return r0
        L_0x0006:
            com.agilebinary.mobilemonitor.client.a.b.s r1 = r15.f224a
            java.util.ArrayList r4 = new java.util.ArrayList
            r4.<init>()
            com.google.android.maps.MapView r0 = r14.b
            com.google.android.maps.Projection r2 = r0.getProjection()
            r0 = r1
            com.agilebinary.mobilemonitor.client.a.d r0 = (com.agilebinary.mobilemonitor.client.a.d) r0
            com.google.android.maps.GeoPoint r0 = r0.i()
            r3 = 0
            android.graphics.Point r5 = r2.toPixels(r0, r3)
            android.graphics.Point r0 = new android.graphics.Point
            r0.<init>()
            java.util.List r2 = r14.h
            java.util.Iterator r6 = r2.iterator()
            r3 = r0
        L_0x002b:
            boolean r0 = r6.hasNext()
            if (r0 == 0) goto L_0x0078
            java.lang.Object r0 = r6.next()
            com.agilebinary.mobilemonitor.client.a.b.s r0 = (com.agilebinary.mobilemonitor.client.a.b.s) r0
            r2 = r0
            com.agilebinary.mobilemonitor.client.a.d r2 = (com.agilebinary.mobilemonitor.client.a.d) r2
            if (r2 == r1) goto L_0x002b
            boolean r7 = r2.j()
            if (r7 == 0) goto L_0x002b
            com.google.android.maps.MapView r7 = r14.b
            com.google.android.maps.Projection r7 = r7.getProjection()
            com.google.android.maps.GeoPoint r2 = r2.i()
            android.graphics.Point r2 = r7.toPixels(r2, r3)
            int r3 = r5.x
            int r7 = r2.x
            int r3 = r3 - r7
            double r8 = (double) r3
            r10 = 4611686018427387904(0x4000000000000000, double:2.0)
            double r8 = java.lang.Math.pow(r8, r10)
            int r3 = r5.y
            int r7 = r2.y
            int r3 = r3 - r7
            double r10 = (double) r3
            r12 = 4611686018427387904(0x4000000000000000, double:2.0)
            double r10 = java.lang.Math.pow(r10, r12)
            double r8 = r8 + r10
            double r8 = java.lang.Math.sqrt(r8)
            r10 = 4629137466983448576(0x403e000000000000, double:30.0)
            int r3 = (r8 > r10 ? 1 : (r8 == r10 ? 0 : -1))
            if (r3 >= 0) goto L_0x00a5
            r4.add(r0)
            r3 = r2
            goto L_0x002b
        L_0x0078:
            int r0 = r4.size()
            if (r0 != 0) goto L_0x0085
            com.agilebinary.mobilemonitor.client.a.b.s r0 = r15.f224a
            r14.a(r0)
        L_0x0083:
            r0 = 0
            goto L_0x0005
        L_0x0085:
            r0 = 0
            com.agilebinary.mobilemonitor.client.a.b.s r1 = r15.f224a
            r4.add(r0, r1)
            com.agilebinary.mobilemonitor.client.android.ui.map.d r0 = new com.agilebinary.mobilemonitor.client.android.ui.map.d
            com.agilebinary.mobilemonitor.client.android.ui.map.a r1 = r14.x
            r0.<init>(r14, r1)
            r14.y = r0
            com.agilebinary.mobilemonitor.client.android.ui.map.d r0 = r14.y
            r1 = 1
            r0.setCancelable(r1)
            com.agilebinary.mobilemonitor.client.android.ui.map.d r0 = r14.y
            r0.a(r4)
            com.agilebinary.mobilemonitor.client.android.ui.map.d r0 = r14.y
            r0.show()
            goto L_0x0083
        L_0x00a5:
            r3 = r2
            goto L_0x002b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.mobilemonitor.client.android.ui.MapActivity_GOOGLE.a(com.agilebinary.mobilemonitor.client.android.ui.ag):boolean");
    }

    /* access modifiers changed from: protected */
    public final void b() {
        a((s) this.h.get(this.h.indexOf(this.s) - 1));
    }

    /* access modifiers changed from: protected */
    public final void c() {
        a((s) this.h.get(0));
    }

    /* access modifiers changed from: protected */
    public final void d() {
        a((s) this.h.get(this.h.size() - 1));
    }

    public final void e() {
        this.b.invalidate();
    }

    /* access modifiers changed from: protected */
    public boolean isRouteDisplayed() {
        return false;
    }

    /* JADX WARN: Type inference failed for: r5v0, types: [android.content.Context, com.agilebinary.mobilemonitor.client.android.ui.MapActivity_GOOGLE, com.google.android.maps.MapActivity] */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public void onCreate(android.os.Bundle r6) {
        /*
            r5 = this;
            r4 = 0
            r0 = 2130903074(0x7f030022, float:1.7412956E38)
            com.agilebinary.mobilemonitor.client.android.ui.MapActivity_GOOGLE.super.onCreate(r6)
            r5.setContentView(r0)
            android.app.Application r0 = r5.getApplication()
            com.agilebinary.mobilemonitor.client.android.MyApplication r0 = (com.agilebinary.mobilemonitor.client.android.MyApplication) r0
            r5.v = r0
            r0 = 2131296392(0x7f090088, float:1.82107E38)
            com.agilebinary.mobilemonitor.client.android.MyApplication r1 = r5.v
            com.agilebinary.mobilemonitor.client.android.b.j r1 = r1.e()
            r5.w = r1
            com.agilebinary.mobilemonitor.client.android.b.m r1 = com.agilebinary.mobilemonitor.client.android.b.m.a(r5)
            r5.i = r1
            android.view.View r0 = r5.findViewById(r0)
            android.widget.RelativeLayout r0 = (android.widget.RelativeLayout) r0
            r5.f204a = r0
            com.agilebinary.mobilemonitor.client.android.a.t.a()
            com.google.android.maps.MapView r0 = new com.google.android.maps.MapView
            java.lang.String r1 = "06R6yV4-IXDu3HQndWrKMjrsmkCjh0vI5HZIGiw"
            r0.<init>(r5, r1)
            r5.b = r0
            com.google.android.maps.MapView r0 = r5.b
            com.google.android.maps.MapController r0 = r0.getController()
            r5.c = r0
            android.widget.RelativeLayout r0 = r5.f204a
            com.google.android.maps.MapView r1 = r5.b
            android.widget.LinearLayout$LayoutParams r2 = new android.widget.LinearLayout$LayoutParams
            r3 = -1
            r2.<init>(r3, r3)
            r0.addView(r1, r2)
            r0 = 2131296319(0x7f09003f, float:1.8210551E38)
            android.view.View r0 = r5.findViewById(r0)
            android.widget.TextView r0 = (android.widget.TextView) r0
            r5.j = r0
            r0 = 2131296393(0x7f090089, float:1.8210701E38)
            android.view.View r0 = r5.findViewById(r0)
            android.widget.TextView r0 = (android.widget.TextView) r0
            r5.k = r0
            r0 = 2131296394(0x7f09008a, float:1.8210703E38)
            android.view.View r0 = r5.findViewById(r0)
            android.widget.TextView r0 = (android.widget.TextView) r0
            r5.l = r0
            r0 = 2131296397(0x7f09008d, float:1.821071E38)
            android.view.View r0 = r5.findViewById(r0)
            android.widget.TextView r0 = (android.widget.TextView) r0
            r5.m = r0
            r0 = 2131296396(0x7f09008c, float:1.8210707E38)
            android.view.View r0 = r5.findViewById(r0)
            android.widget.TextView r0 = (android.widget.TextView) r0
            r5.t = r0
            r0 = 2131296395(0x7f09008b, float:1.8210705E38)
            android.view.View r0 = r5.findViewById(r0)
            android.widget.TextView r0 = (android.widget.TextView) r0
            r5.n = r0
            r0 = 2131296318(0x7f09003e, float:1.821055E38)
            android.view.View r0 = r5.findViewById(r0)
            android.widget.ImageButton r0 = (android.widget.ImageButton) r0
            r5.o = r0
            r0 = 2131296320(0x7f090040, float:1.8210553E38)
            android.view.View r0 = r5.findViewById(r0)
            android.widget.ImageButton r0 = (android.widget.ImageButton) r0
            r5.p = r0
            r0 = 2131296317(0x7f09003d, float:1.8210547E38)
            android.view.View r0 = r5.findViewById(r0)
            android.widget.ImageButton r0 = (android.widget.ImageButton) r0
            r5.q = r0
            r0 = 2131296321(0x7f090041, float:1.8210555E38)
            android.view.View r0 = r5.findViewById(r0)
            android.widget.ImageButton r0 = (android.widget.ImageButton) r0
            r5.r = r0
            android.widget.ImageButton r0 = r5.o
            r0.setEnabled(r4)
            android.widget.ImageButton r0 = r5.p
            r0.setEnabled(r4)
            android.widget.ImageButton r0 = r5.q
            r0.setEnabled(r4)
            android.widget.ImageButton r0 = r5.r
            r0.setEnabled(r4)
            android.widget.ImageButton r0 = r5.o
            com.agilebinary.mobilemonitor.client.android.ui.ca r1 = new com.agilebinary.mobilemonitor.client.android.ui.ca
            r1.<init>(r5)
            r0.setOnClickListener(r1)
            android.widget.ImageButton r0 = r5.p
            com.agilebinary.mobilemonitor.client.android.ui.cb r1 = new com.agilebinary.mobilemonitor.client.android.ui.cb
            r1.<init>(r5)
            r0.setOnClickListener(r1)
            android.widget.ImageButton r0 = r5.q
            com.agilebinary.mobilemonitor.client.android.ui.by r1 = new com.agilebinary.mobilemonitor.client.android.ui.by
            r1.<init>(r5)
            r0.setOnClickListener(r1)
            android.widget.ImageButton r0 = r5.r
            com.agilebinary.mobilemonitor.client.android.ui.bz r1 = new com.agilebinary.mobilemonitor.client.android.ui.bz
            r1.<init>(r5)
            r0.setOnClickListener(r1)
            android.content.Intent r0 = r5.getIntent()
            java.lang.String r1 = "EXTRA_EVENT_IDS"
            java.io.Serializable r0 = r0.getSerializableExtra(r1)
            java.util.List r0 = (java.util.List) r0
            java.util.Collections.sort(r0)
            com.agilebinary.mobilemonitor.client.android.ui.c r1 = new com.agilebinary.mobilemonitor.client.android.ui.c
            r1.<init>(r5)
            r5.u = r1
            com.agilebinary.mobilemonitor.client.android.ui.c r1 = r5.u
            r2 = 1
            java.util.List[] r2 = new java.util.List[r2]
            r2[r4] = r0
            r1.execute(r2)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.mobilemonitor.client.android.ui.MapActivity_GOOGLE.onCreate(android.os.Bundle):void");
    }

    /* JADX WARN: Type inference failed for: r4v0, types: [android.content.Context, com.agilebinary.mobilemonitor.client.android.ui.MapActivity_GOOGLE] */
    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int i2) {
        if (i2 != 0) {
            return null;
        }
        Dialog dialog = new Dialog(this);
        dialog.setContentView((int) R.layout.map_menu_dialog);
        dialog.setTitle((int) R.string.label_map_dialog_title);
        ((Button) dialog.findViewById(R.id.map_menu_zoomtomarkers)).setOnClickListener(new bw(this, dialog));
        ((CheckedTextView) dialog.findViewById(R.id.map_menu_toggle_directions)).setOnClickListener(new bx(this, dialog));
        ((CheckedTextView) dialog.findViewById(R.id.map_menu_toggle_accuracy)).setOnClickListener(new bu(this, dialog));
        ((CheckedTextView) dialog.findViewById(R.id.map_menu_toggle_scale)).setOnClickListener(new bv(this, dialog));
        ((CheckedTextView) dialog.findViewById(R.id.map_menu_toggle_satellite)).setOnClickListener(new bt(this, dialog));
        return dialog;
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        showDialog(0);
        return false;
    }

    /* access modifiers changed from: protected */
    public void onPrepareDialog(int i2, Dialog dialog) {
        try {
            ((CheckedTextView) dialog.findViewById(R.id.map_menu_toggle_directions)).setChecked(this.e.a());
            ((CheckedTextView) dialog.findViewById(R.id.map_menu_toggle_accuracy)).setChecked(this.f.a());
            ((CheckedTextView) dialog.findViewById(R.id.map_menu_toggle_scale)).setChecked(this.d.a());
            ((CheckedTextView) dialog.findViewById(R.id.map_menu_toggle_satellite)).setChecked(this.i.a("MAP_SATELLITE_VIEW__BOOL", false));
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        if (this.u != null) {
            this.u.cancel(false);
        }
        MapActivity_GOOGLE.super.onStop();
    }
}
