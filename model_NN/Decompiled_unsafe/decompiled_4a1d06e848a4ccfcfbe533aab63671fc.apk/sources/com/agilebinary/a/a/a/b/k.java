package com.agilebinary.a.a.a.b;

import com.agilebinary.a.a.a.a;
import com.agilebinary.a.a.a.ac;
import com.agilebinary.a.a.a.c;
import com.agilebinary.a.a.a.h;
import com.agilebinary.a.a.a.j;
import java.util.Locale;

public final class k extends l implements j {
    private h c;
    private c d;
    private ac e;
    private Locale f;

    public k(h hVar, ac acVar, Locale locale) {
        if (hVar == null) {
            throw new IllegalArgumentException("Status line may not be null.");
        }
        this.c = hVar;
        this.e = acVar;
        this.f = locale == null ? Locale.getDefault() : locale;
    }

    public final h a() {
        return this.c;
    }

    public final void a(c cVar) {
        this.d = cVar;
    }

    public final c b() {
        return this.d;
    }

    public final a c() {
        return this.c.a();
    }

    public final String toString() {
        return this.c + " " + this.f15a;
    }
}
