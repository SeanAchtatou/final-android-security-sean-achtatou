package com.agilebinary.a.a.a.c.c;

import com.agilebinary.a.a.a.j.e;
import java.io.IOException;
import java.io.OutputStream;

public final class k extends OutputStream {

    /* renamed from: a  reason: collision with root package name */
    private final e f65a;
    private boolean b = false;

    public k(e eVar) {
        if (eVar == null) {
            throw new IllegalArgumentException("Session output buffer may not be null");
        }
        this.f65a = eVar;
    }

    public final void close() {
        if (!this.b) {
            this.b = true;
            this.f65a.b();
        }
    }

    public final void flush() {
        this.f65a.b();
    }

    public final void write(int i) {
        if (this.b) {
            throw new IOException("Attempted write to closed stream.");
        }
        this.f65a.a(i);
    }

    public final void write(byte[] bArr) {
        write(bArr, 0, bArr.length);
    }

    public final void write(byte[] bArr, int i, int i2) {
        if (this.b) {
            throw new IOException("Attempted write to closed stream.");
        }
        this.f65a.a(bArr, i, i2);
    }
}
