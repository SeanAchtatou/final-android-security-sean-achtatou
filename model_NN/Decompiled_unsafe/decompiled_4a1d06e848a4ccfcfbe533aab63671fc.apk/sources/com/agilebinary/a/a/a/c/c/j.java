package com.agilebinary.a.a.a.c.c;

import com.agilebinary.a.a.a.j.e;
import java.io.IOException;
import java.io.OutputStream;

public final class j extends OutputStream {

    /* renamed from: a  reason: collision with root package name */
    private final e f64a;
    private final long b;
    private long c = 0;
    private boolean d = false;

    public j(e eVar, long j) {
        if (eVar == null) {
            throw new IllegalArgumentException("Session output buffer may not be null");
        } else if (j < 0) {
            throw new IllegalArgumentException("Content length may not be negative");
        } else {
            this.f64a = eVar;
            this.b = j;
        }
    }

    public final void close() {
        if (!this.d) {
            this.d = true;
            this.f64a.b();
        }
    }

    public final void flush() {
        this.f64a.b();
    }

    public final void write(int i) {
        if (this.d) {
            throw new IOException("Attempted write to closed stream.");
        } else if (this.c < this.b) {
            this.f64a.a(i);
            this.c++;
        }
    }

    public final void write(byte[] bArr) {
        write(bArr, 0, bArr.length);
    }

    public final void write(byte[] bArr, int i, int i2) {
        if (this.d) {
            throw new IOException("Attempted write to closed stream.");
        } else if (this.c < this.b) {
            long j = this.b - this.c;
            if (((long) i2) > j) {
                i2 = (int) j;
            }
            this.f64a.a(bArr, i, i2);
            this.c += (long) i2;
        }
    }
}
