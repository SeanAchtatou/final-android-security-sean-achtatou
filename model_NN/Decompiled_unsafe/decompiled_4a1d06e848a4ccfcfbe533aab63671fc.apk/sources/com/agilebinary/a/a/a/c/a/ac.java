package com.agilebinary.a.a.a.c.a;

import com.agilebinary.a.a.a.k.b;
import com.agilebinary.a.a.a.k.e;
import java.util.Date;

public final class ac extends x {
    public final void a(b bVar, String str) {
        if (bVar == null) {
            throw new IllegalArgumentException("Cookie may not be null");
        } else if (str == null) {
            throw new e("Missing value for max-age attribute");
        } else {
            try {
                int parseInt = Integer.parseInt(str);
                if (parseInt < 0) {
                    throw new e(new StringBuilder().insert(0, "Negative max-age attribute: ").append(str).toString());
                }
                bVar.a(new Date(System.currentTimeMillis() + (((long) parseInt) * 1000)));
            } catch (NumberFormatException e) {
                throw new e(new StringBuilder().insert(0, "Invalid max-age attribute: ").append(str).toString());
            }
        }
    }
}
