package com.agilebinary.a.a.a.h.c;

import com.agilebinary.a.a.a.b;
import java.net.InetAddress;

public final class c implements b, Cloneable {

    /* renamed from: a  reason: collision with root package name */
    private static final b[] f106a = new b[0];
    private final b b;
    private final InetAddress c;
    private final b[] d;
    private final g e;
    private final e f;
    private final boolean g;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.a.a.a.h.c.c.<init>(java.net.InetAddress, com.agilebinary.a.a.a.b, com.agilebinary.a.a.a.b[], boolean, com.agilebinary.a.a.a.h.c.g, com.agilebinary.a.a.a.h.c.e):void
     arg types: [?[OBJECT, ARRAY], com.agilebinary.a.a.a.b, com.agilebinary.a.a.a.b[], int, com.agilebinary.a.a.a.h.c.g, com.agilebinary.a.a.a.h.c.e]
     candidates:
      com.agilebinary.a.a.a.h.c.c.<init>(com.agilebinary.a.a.a.b, java.net.InetAddress, com.agilebinary.a.a.a.b[], boolean, com.agilebinary.a.a.a.h.c.g, com.agilebinary.a.a.a.h.c.e):void
      com.agilebinary.a.a.a.h.c.c.<init>(java.net.InetAddress, com.agilebinary.a.a.a.b, com.agilebinary.a.a.a.b[], boolean, com.agilebinary.a.a.a.h.c.g, com.agilebinary.a.a.a.h.c.e):void */
    public c(b bVar) {
        this((InetAddress) null, bVar, f106a, false, g.PLAIN, e.PLAIN);
    }

    public c(b bVar, InetAddress inetAddress, b bVar2, boolean z) {
        b[] bVarArr;
        boolean z2;
        g gVar;
        boolean z3;
        e eVar;
        c cVar;
        if (bVar2 == null) {
            bVarArr = f106a;
            z2 = z;
        } else {
            bVarArr = new b[]{bVar2};
            z2 = z;
        }
        if (z2) {
            gVar = g.TUNNELLED;
            z3 = z;
        } else {
            gVar = g.PLAIN;
            z3 = z;
        }
        if (z3) {
            eVar = e.LAYERED;
            cVar = this;
        } else {
            eVar = e.PLAIN;
            cVar = this;
        }
        new c(inetAddress, bVar, bVarArr, z, gVar, eVar);
        if (bVar2 == null) {
            throw new IllegalArgumentException("Proxy host may not be null.");
        }
    }

    public c(b bVar, InetAddress inetAddress, boolean z) {
        this(inetAddress, bVar, f106a, z, g.PLAIN, e.PLAIN);
    }

    public c(b bVar, InetAddress inetAddress, b[] bVarArr, boolean z, g gVar, e eVar) {
        this(inetAddress, bVar, a(bVarArr), z, gVar, eVar);
    }

    private /* synthetic */ c(InetAddress inetAddress, b bVar, b[] bVarArr, boolean z, g gVar, e eVar) {
        if (bVar == null) {
            throw new IllegalArgumentException("Target host may not be null.");
        } else if (bVarArr == null) {
            throw new IllegalArgumentException("Proxies may not be null.");
        } else if (gVar == g.TUNNELLED && bVarArr.length == 0) {
            throw new IllegalArgumentException("Proxy required if tunnelled.");
        } else {
            gVar = gVar == null ? g.PLAIN : gVar;
            eVar = eVar == null ? e.PLAIN : eVar;
            this.b = bVar;
            this.c = inetAddress;
            this.d = bVarArr;
            this.g = z;
            this.e = gVar;
            this.f = eVar;
        }
    }

    private static /* synthetic */ b[] a(b[] bVarArr) {
        if (bVarArr == null || bVarArr.length <= 0) {
            return f106a;
        }
        int length = bVarArr.length;
        int i = 0;
        int i2 = 0;
        while (i < length) {
            if (bVarArr[i2] == null) {
                throw new IllegalArgumentException("Proxy chain may not contain null elements.");
            }
            i = i2 + 1;
            i2 = i;
        }
        b[] bVarArr2 = new b[bVarArr.length];
        System.arraycopy(bVarArr, 0, bVarArr2, 0, bVarArr.length);
        return bVarArr2;
    }

    public final b a() {
        return this.b;
    }

    public final b a(int i) {
        if (i < 0) {
            throw new IllegalArgumentException(new StringBuilder().insert(0, "Hop index must not be negative: ").append(i).toString());
        }
        int length = this.d.length + 1;
        if (i < length) {
            return i < length + -1 ? this.d[i] : this.b;
        }
        throw new IllegalArgumentException(new StringBuilder().insert(0, "Hop index ").append(i).append(" exceeds route length ").append(length).toString());
    }

    public final InetAddress b() {
        return this.c;
    }

    public final int c() {
        return this.d.length + 1;
    }

    public final Object clone() {
        return super.clone();
    }

    public final boolean d() {
        return this.e == g.TUNNELLED;
    }

    public final boolean e() {
        return this.f == e.LAYERED;
    }

    public final boolean equals(Object obj) {
        boolean z;
        c cVar;
        boolean z2;
        c cVar2;
        boolean z3;
        boolean z4;
        int i = 0;
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof c)) {
            return false;
        }
        c cVar3 = (c) obj;
        boolean equals = this.b.equals(cVar3.b);
        if (this.c == cVar3.c || (this.c != null && this.c.equals(cVar3.c))) {
            cVar = this;
            z = true;
        } else {
            cVar = this;
            z = false;
        }
        if (cVar.d == cVar3.d || this.d.length == cVar3.d.length) {
            cVar2 = this;
            z2 = true;
        } else {
            cVar2 = this;
            z2 = false;
        }
        if (cVar2.g == cVar3.g && this.e == cVar3.e && this.f == cVar3.f) {
            z4 = true;
            z3 = equals;
        } else {
            z3 = equals;
            z4 = false;
        }
        boolean z5 = z3 & z & z2 & z4;
        if (!z5 || this.d == null) {
            return z5;
        }
        boolean z6 = z5;
        while (z6 && i < this.d.length) {
            z5 = this.d[i].equals(cVar3.d[i]);
            i++;
            z6 = z5;
        }
        return z5;
    }

    public final boolean f() {
        return this.g;
    }

    public final b g() {
        if (this.d.length == 0) {
            return null;
        }
        return this.d[0];
    }

    public final int hashCode() {
        int hashCode = this.b.hashCode();
        if (this.c != null) {
            hashCode ^= this.c.hashCode();
        }
        int length = this.d.length;
        b[] bVarArr = this.d;
        int length2 = bVarArr.length;
        int i = 0;
        int i2 = length ^ hashCode;
        int i3 = 0;
        while (i3 < length2) {
            int hashCode2 = bVarArr[i].hashCode() ^ i2;
            int i4 = i + 1;
            i = i4;
            int i5 = i4;
            i2 = hashCode2;
            i3 = i5;
        }
        if (this.g) {
            i2 ^= 286331153;
        }
        return (i2 ^ this.e.hashCode()) ^ this.f.hashCode();
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder(((this.d.length + 1) * 30) + 50);
        sb.append("HttpRoute[");
        if (this.c != null) {
            sb.append(this.c);
            sb.append("->");
        }
        sb.append('{');
        if (this.e == g.TUNNELLED) {
            sb.append('t');
        }
        if (this.f == e.LAYERED) {
            sb.append('l');
        }
        if (this.g) {
            sb.append('s');
        }
        sb.append("}->");
        b[] bVarArr = this.d;
        int length = bVarArr.length;
        int i = 0;
        int i2 = 0;
        while (i < length) {
            sb.append(bVarArr[i2]);
            sb.append("->");
            i = i2 + 1;
            i2 = i;
        }
        sb.append(this.b);
        sb.append(']');
        return sb.toString();
    }
}
