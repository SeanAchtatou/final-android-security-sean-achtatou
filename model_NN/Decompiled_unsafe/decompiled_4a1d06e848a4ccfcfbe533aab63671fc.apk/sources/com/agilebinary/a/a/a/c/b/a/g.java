package com.agilebinary.a.a.a.c.b.a;

import com.agilebinary.a.a.a.c.b.a;
import com.agilebinary.a.a.a.h.c.c;
import com.agilebinary.a.a.a.h.m;
import java.util.concurrent.TimeUnit;

public final class g extends a {
    private final long d;
    private long e;
    private long f;
    private long g;

    public g(m mVar, c cVar, long j, TimeUnit timeUnit) {
        super(mVar, cVar);
        g gVar;
        if (cVar == null) {
            throw new IllegalArgumentException("HTTP route may not be null");
        }
        this.d = System.currentTimeMillis();
        if (j > 0) {
            this.f = this.d + timeUnit.toMillis(j);
            gVar = this;
        } else {
            this.f = Long.MAX_VALUE;
            gVar = this;
        }
        gVar.g = this.f;
    }

    public final void a(long j, TimeUnit timeUnit) {
        long j2;
        g gVar;
        this.e = System.currentTimeMillis();
        if (j > 0) {
            j2 = this.e + timeUnit.toMillis(j);
            gVar = this;
        } else {
            j2 = Long.MAX_VALUE;
            gVar = this;
        }
        gVar.g = Math.min(this.f, j2);
    }

    public final boolean a(long j) {
        return j >= this.g;
    }

    /* access modifiers changed from: protected */
    public final void b() {
        super.b();
    }

    /* access modifiers changed from: protected */
    public final com.agilebinary.a.a.a.h.g c() {
        return this.f35a;
    }

    /* access modifiers changed from: protected */
    public final c d() {
        return this.b;
    }

    public final long e() {
        return this.e;
    }
}
