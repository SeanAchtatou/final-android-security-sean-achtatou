package com.agilebinary.a.a.a.b;

import com.agilebinary.a.a.a.a;
import com.agilebinary.a.a.a.d;
import com.agilebinary.a.a.a.h;
import com.agilebinary.a.a.a.i.c;
import com.agilebinary.a.a.a.t;

public final class r implements c {

    /* renamed from: a  reason: collision with root package name */
    public static final r f20a = new r();

    private static /* synthetic */ int a(a aVar) {
        return aVar.a().length() + 4;
    }

    public static c a(h hVar) {
        if (hVar == null) {
            throw new IllegalArgumentException("Status line may not be null");
        }
        c a2 = a((c) null);
        int a3 = a(hVar.a()) + 1 + 3 + 1;
        String c = hVar.c();
        if (c != null) {
            a3 += c.length();
        }
        a2.b(a3);
        a(a2, hVar.a());
        a2.a(' ');
        a2.a(Integer.toString(hVar.b()));
        a2.a(' ');
        if (c != null) {
            a2.a(c);
        }
        return a2;
    }

    private static /* synthetic */ c a(c cVar) {
        if (cVar == null) {
            return new c(64);
        }
        cVar.a();
        return cVar;
    }

    private static /* synthetic */ c a(c cVar, a aVar) {
        c cVar2;
        if (aVar == null) {
            throw new IllegalArgumentException("Protocol version may not be null");
        }
        int a2 = a(aVar);
        if (cVar == null) {
            cVar = new c(a2);
            cVar2 = cVar;
        } else {
            cVar.b(a2);
            cVar2 = cVar;
        }
        cVar.a(aVar.a());
        cVar2.a('/');
        cVar2.a(Integer.toString(aVar.b()));
        cVar2.a('.');
        cVar2.a(Integer.toString(aVar.c()));
        return cVar2;
    }

    public final c a(c cVar, d dVar) {
        if (dVar == null) {
            throw new IllegalArgumentException("Request line may not be null");
        }
        c a2 = a(cVar);
        String a3 = dVar.a();
        String c = dVar.c();
        a2.b(a3.length() + 1 + c.length() + 1 + a(dVar.b()));
        a2.a(a3);
        a2.a(' ');
        a2.a(c);
        a2.a(' ');
        a(a2, dVar.b());
        return a2;
    }

    public final c a(c cVar, t tVar) {
        c cVar2;
        if (tVar == null) {
            throw new IllegalArgumentException("Header may not be null");
        }
        if (tVar instanceof com.agilebinary.a.a.a.r) {
            cVar2 = ((com.agilebinary.a.a.a.r) tVar).e();
        } else {
            c a2 = a(cVar);
            String a3 = tVar.a();
            String b = tVar.b();
            int length = a3.length() + 2;
            if (b != null) {
                length += b.length();
            }
            a2.b(length);
            a2.a(a3);
            a2.a(": ");
            if (b != null) {
                a2.a(b);
                return a2;
            }
            cVar2 = a2;
        }
        return cVar2;
    }
}
