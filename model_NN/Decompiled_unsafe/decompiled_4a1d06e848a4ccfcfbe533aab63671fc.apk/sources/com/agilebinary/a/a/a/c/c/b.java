package com.agilebinary.a.a.a.c.c;

import com.agilebinary.a.a.a.i.e;
import com.agilebinary.a.a.a.j.a;
import com.agilebinary.a.a.a.j.f;
import com.agilebinary.a.a.a.j.g;
import java.io.InputStream;

public abstract class b implements a, g {

    /* renamed from: a  reason: collision with root package name */
    private InputStream f57a;
    private byte[] b;
    private int c;
    private int d;
    private e e = null;
    private String f = "US-ASCII";
    private boolean g = true;
    private int h = -1;
    private int i = 512;
    private i j;

    private /* synthetic */ int f() {
        int i2 = this.c;
        while (true) {
            int i3 = i2;
            if (i2 >= this.d) {
                return -1;
            }
            if (this.b[i3] == 10) {
                return i3;
            }
            i2 = i3 + 1;
        }
    }

    public final int a() {
        return this.d - this.c;
    }

    /* JADX WARNING: Removed duplicated region for block: B:23:0x0068  */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x010c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final int a(com.agilebinary.a.a.a.i.c r11) {
        /*
            r10 = this;
            r9 = 13
            r4 = -1
            r2 = 0
            if (r11 != 0) goto L_0x000e
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "Char array buffer may not be null"
            r0.<init>(r1)
            throw r0
        L_0x000e:
            r3 = 1
            r1 = r2
        L_0x0010:
            if (r3 == 0) goto L_0x009b
            int r0 = r10.f()
            if (r0 == r4) goto L_0x007a
            com.agilebinary.a.a.a.i.e r3 = r10.e
            boolean r3 = r3.f()
            if (r3 == 0) goto L_0x004e
            int r1 = r10.c
            int r2 = r0 + 1
            r10.c = r2
            if (r0 <= 0) goto L_0x0032
            byte[] r2 = r10.b
            int r3 = r0 + -1
            byte r2 = r2[r3]
            if (r2 != r9) goto L_0x0032
            int r0 = r0 + -1
        L_0x0032:
            int r0 = r0 - r1
            boolean r2 = r10.g
            if (r2 == 0) goto L_0x003d
            byte[] r2 = r10.b
            r11.a(r2, r1, r0)
        L_0x003c:
            return r0
        L_0x003d:
            java.lang.String r2 = new java.lang.String
            byte[] r3 = r10.b
            java.lang.String r4 = r10.f
            r2.<init>(r3, r1, r0, r4)
            r11.a(r2)
            int r0 = r2.length()
            goto L_0x003c
        L_0x004e:
            int r3 = r10.c
            com.agilebinary.a.a.a.i.e r5 = r10.e
            byte[] r6 = r10.b
            int r7 = r10.c
            int r8 = r0 + 1
            int r3 = r8 - r3
            r5.a(r6, r7, r3)
            int r0 = r0 + 1
            r10.c = r0
            r0 = r1
            r1 = r2
        L_0x0063:
            r3 = r1
        L_0x0064:
            int r5 = r10.h
            if (r5 <= 0) goto L_0x010c
            com.agilebinary.a.a.a.i.e r1 = r10.e
            int r1 = r1.d()
            int r5 = r10.h
            if (r1 < r5) goto L_0x0109
            java.io.IOException r0 = new java.io.IOException
            java.lang.String r1 = "Maximum line length limit exceeded"
            r0.<init>(r1)
            throw r0
        L_0x007a:
            boolean r0 = r10.c()
            if (r0 == 0) goto L_0x0092
            int r0 = r10.d
            int r1 = r10.c
            com.agilebinary.a.a.a.i.e r5 = r10.e
            byte[] r6 = r10.b
            int r7 = r10.c
            int r0 = r0 - r1
            r5.a(r6, r7, r0)
            int r0 = r10.d
            r10.c = r0
        L_0x0092:
            int r0 = r10.b()
            if (r0 != r4) goto L_0x0106
            r1 = r2
            r3 = r2
            goto L_0x0064
        L_0x009b:
            if (r1 != r4) goto L_0x00a7
            com.agilebinary.a.a.a.i.e r0 = r10.e
            boolean r0 = r0.f()
            if (r0 == 0) goto L_0x00a7
            r0 = r4
            goto L_0x003c
        L_0x00a7:
            com.agilebinary.a.a.a.i.e r0 = r10.e
            int r0 = r0.d()
            if (r0 <= 0) goto L_0x00d5
            com.agilebinary.a.a.a.i.e r1 = r10.e
            int r3 = r0 + -1
            int r1 = r1.b(r3)
            r3 = 10
            if (r1 != r3) goto L_0x00c2
            int r0 = r0 + -1
            com.agilebinary.a.a.a.i.e r1 = r10.e
            r1.c(r0)
        L_0x00c2:
            if (r0 <= 0) goto L_0x00d5
            com.agilebinary.a.a.a.i.e r1 = r10.e
            int r3 = r0 + -1
            int r1 = r1.b(r3)
            if (r1 != r9) goto L_0x00d5
            com.agilebinary.a.a.a.i.e r1 = r10.e
            int r0 = r0 + -1
            r1.c(r0)
        L_0x00d5:
            com.agilebinary.a.a.a.i.e r0 = r10.e
            int r0 = r0.d()
            boolean r1 = r10.g
            if (r1 == 0) goto L_0x00f1
            com.agilebinary.a.a.a.i.e r1 = r10.e
            if (r1 == 0) goto L_0x00ea
            byte[] r1 = r1.e()
            r11.a(r1, r2, r0)
        L_0x00ea:
            com.agilebinary.a.a.a.i.e r1 = r10.e
            r1.a()
            goto L_0x003c
        L_0x00f1:
            java.lang.String r1 = new java.lang.String
            com.agilebinary.a.a.a.i.e r3 = r10.e
            byte[] r3 = r3.e()
            java.lang.String r4 = r10.f
            r1.<init>(r3, r2, r0, r4)
            int r0 = r1.length()
            r11.a(r1)
            goto L_0x00ea
        L_0x0106:
            r1 = r3
            goto L_0x0063
        L_0x0109:
            r1 = r0
            goto L_0x0010
        L_0x010c:
            r3 = r1
            r1 = r0
            goto L_0x0010
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.a.a.a.c.c.b.a(com.agilebinary.a.a.a.i.c):int");
    }

    public final int a(byte[] bArr, int i2, int i3) {
        if (bArr == null) {
            return 0;
        }
        if (c()) {
            int min = Math.min(i3, this.d - this.c);
            System.arraycopy(this.b, this.c, bArr, i2, min);
            this.c += min;
            return min;
        } else if (i3 > this.i) {
            return this.f57a.read(bArr, i2, i3);
        } else {
            while (!c()) {
                if (b() == -1) {
                    return -1;
                }
            }
            int min2 = Math.min(i3, this.d - this.c);
            System.arraycopy(this.b, this.c, bArr, i2, min2);
            this.c += min2;
            return min2;
        }
    }

    /* access modifiers changed from: protected */
    public final void a(InputStream inputStream, int i2, com.agilebinary.a.a.a.e.e eVar) {
        boolean z;
        b bVar;
        if (inputStream == null) {
            throw new IllegalArgumentException("Input stream may not be null");
        } else if (i2 <= 0) {
            throw new IllegalArgumentException("Buffer size may not be negative or zero");
        } else if (eVar == null) {
            throw new IllegalArgumentException("HTTP parameters may not be null");
        } else {
            this.f57a = inputStream;
            this.b = new byte[i2];
            this.c = 0;
            this.d = 0;
            this.e = new e(i2);
            this.f = com.agilebinary.a.a.a.e.b.a(eVar);
            if (this.f.equalsIgnoreCase("US-ASCII") || this.f.equalsIgnoreCase("ASCII")) {
                z = true;
                bVar = this;
            } else {
                z = false;
                bVar = this;
            }
            bVar.g = z;
            this.h = eVar.a("http.connection.max-line-length", -1);
            this.i = eVar.a("http.connection.min-chunk-limit", 512);
            this.j = new i();
        }
    }

    /* access modifiers changed from: protected */
    public int b() {
        if (this.c > 0) {
            int i2 = this.d - this.c;
            if (i2 > 0) {
                System.arraycopy(this.b, this.c, this.b, 0, i2);
            }
            this.c = 0;
            this.d = i2;
        }
        int i3 = this.d;
        int read = this.f57a.read(this.b, i3, this.b.length - i3);
        if (read == -1) {
            return -1;
        }
        this.d = i3 + read;
        this.j.a((long) read);
        return read;
    }

    /* access modifiers changed from: protected */
    public final boolean c() {
        return this.c < this.d;
    }

    public final int d() {
        while (!c()) {
            if (b() == -1) {
                return -1;
            }
        }
        byte[] bArr = this.b;
        int i2 = this.c;
        this.c = i2 + 1;
        return bArr[i2] & 255;
    }

    public final f e() {
        return this.j;
    }
}
