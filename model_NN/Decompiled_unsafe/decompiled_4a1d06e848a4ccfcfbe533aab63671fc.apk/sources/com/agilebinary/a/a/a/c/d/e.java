package com.agilebinary.a.a.a.c.d;

import com.agilebinary.a.a.a.b.p;
import com.agilebinary.a.a.a.h.n;
import com.agilebinary.a.a.a.j;
import com.agilebinary.a.a.a.m;

public final class e implements n {
    public final long a(j jVar) {
        if (jVar == null) {
            throw new IllegalArgumentException("HTTP response may not be null");
        }
        p pVar = new p(jVar.d("Keep-Alive"));
        while (true) {
            while (pVar.hasNext()) {
                m a2 = pVar.a();
                String a3 = a2.a();
                String b = a2.b();
                if (b != null && a3.equalsIgnoreCase("timeout")) {
                    try {
                        return Long.parseLong(b) * 1000;
                    } catch (NumberFormatException e) {
                    }
                }
            }
            return -1;
        }
    }
}
