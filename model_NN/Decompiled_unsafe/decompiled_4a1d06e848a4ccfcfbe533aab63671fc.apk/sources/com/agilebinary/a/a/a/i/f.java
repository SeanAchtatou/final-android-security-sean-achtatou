package com.agilebinary.a.a.a.i;

public final class f {
    private /* synthetic */ f() {
    }

    public static int a(int i, Object obj) {
        return (obj != null ? obj.hashCode() : 0) + (i * 37);
    }

    public static boolean a(Object obj, Object obj2) {
        return obj == null ? obj2 == null : obj.equals(obj2);
    }
}
