package com.agilebinary.a.a.a.i;

public final class d {

    /* renamed from: a  reason: collision with root package name */
    private final String f116a;
    private final String b;
    private final String c;
    private final String d;
    private final String e;

    private /* synthetic */ d(String str, String str2, String str3, String str4, String str5) {
        d dVar;
        d dVar2;
        d dVar3;
        if (str == null) {
            throw new IllegalArgumentException("Package identifier must not be null.");
        }
        this.f116a = str;
        if (str2 != null) {
            dVar = this;
        } else {
            str2 = "UNAVAILABLE";
            dVar = this;
        }
        dVar.b = str2;
        if (str3 != null) {
            dVar2 = this;
        } else {
            str3 = "UNAVAILABLE";
            dVar2 = this;
        }
        dVar2.c = str3;
        if (str4 != null) {
            dVar3 = this;
        } else {
            str4 = "UNAVAILABLE";
            dVar3 = this;
        }
        dVar3.d = str4;
        this.e = str5 == null ? "UNAVAILABLE" : str5;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x004c  */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x00bd  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final com.agilebinary.a.a.a.i.d a(java.lang.String r7, java.lang.ClassLoader r8) {
        /*
            r5 = 0
            if (r7 != 0) goto L_0x000b
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "Package identifier must not be null."
            r0.<init>(r1)
            throw r0
        L_0x000b:
            if (r8 != 0) goto L_0x00bf
            java.lang.Thread r0 = java.lang.Thread.currentThread()
            java.lang.ClassLoader r8 = r0.getContextClassLoader()
            r6 = r8
        L_0x0016:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x005b }
            r0.<init>()     // Catch:{ IOException -> 0x005b }
            r1 = 0
            r2 = 46
            r3 = 47
            java.lang.String r2 = r7.replace(r2, r3)     // Catch:{ IOException -> 0x005b }
            java.lang.StringBuilder r0 = r0.insert(r1, r2)     // Catch:{ IOException -> 0x005b }
            java.lang.String r1 = "/"
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ IOException -> 0x005b }
            java.lang.String r1 = "version.properties"
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ IOException -> 0x005b }
            java.lang.String r0 = r0.toString()     // Catch:{ IOException -> 0x005b }
            java.io.InputStream r1 = r8.getResourceAsStream(r0)     // Catch:{ IOException -> 0x005b }
            if (r1 == 0) goto L_0x005f
            java.util.Properties r0 = new java.util.Properties     // Catch:{ all -> 0x0056 }
            r0.<init>()     // Catch:{ all -> 0x0056 }
            r0.load(r1)     // Catch:{ all -> 0x0056 }
            r1.close()     // Catch:{ IOException -> 0x00c2 }
            r3 = r0
        L_0x004a:
            if (r0 == 0) goto L_0x00bd
            if (r7 != 0) goto L_0x0062
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "Package identifier must not be null."
            r0.<init>(r1)
            throw r0
        L_0x0056:
            r0 = move-exception
            r1.close()     // Catch:{ IOException -> 0x005b }
            throw r0     // Catch:{ IOException -> 0x005b }
        L_0x005b:
            r0 = move-exception
            r0 = r5
        L_0x005d:
            r3 = r0
            goto L_0x004a
        L_0x005f:
            r0 = r5
            r3 = r5
            goto L_0x004a
        L_0x0062:
            if (r3 == 0) goto L_0x00b8
            java.lang.String r0 = "info.module"
            java.lang.Object r0 = r3.get(r0)
            java.lang.String r0 = (java.lang.String) r0
            if (r0 == 0) goto L_0x00c4
            int r1 = r0.length()
            if (r1 > 0) goto L_0x00c4
            r1 = r5
        L_0x0075:
            java.lang.String r0 = "info.release"
            java.lang.Object r0 = r3.get(r0)
            java.lang.String r0 = (java.lang.String) r0
            if (r0 == 0) goto L_0x00c6
            int r2 = r0.length()
            if (r2 <= 0) goto L_0x008d
            java.lang.String r2 = "${pom.version}"
            boolean r2 = r0.equals(r2)
            if (r2 == 0) goto L_0x00c6
        L_0x008d:
            r2 = r5
        L_0x008e:
            java.lang.String r0 = "info.timestamp"
            java.lang.Object r0 = r3.get(r0)
            java.lang.String r0 = (java.lang.String) r0
            if (r0 == 0) goto L_0x00a7
            int r3 = r0.length()
            if (r3 <= 0) goto L_0x00a6
            java.lang.String r3 = "${mvn.timestamp}"
            boolean r3 = r0.equals(r3)
            if (r3 == 0) goto L_0x00a7
        L_0x00a6:
            r0 = r5
        L_0x00a7:
            r4 = r0
            r3 = r2
            r0 = r6
            r2 = r1
        L_0x00ab:
            if (r0 == 0) goto L_0x00b1
            java.lang.String r5 = r6.toString()
        L_0x00b1:
            com.agilebinary.a.a.a.i.d r0 = new com.agilebinary.a.a.a.i.d
            r1 = r7
            r0.<init>(r1, r2, r3, r4, r5)
        L_0x00b7:
            return r0
        L_0x00b8:
            r0 = r6
            r4 = r5
            r3 = r5
            r2 = r5
            goto L_0x00ab
        L_0x00bd:
            r0 = r5
            goto L_0x00b7
        L_0x00bf:
            r6 = r8
            goto L_0x0016
        L_0x00c2:
            r1 = move-exception
            goto L_0x005d
        L_0x00c4:
            r1 = r0
            goto L_0x0075
        L_0x00c6:
            r2 = r0
            goto L_0x008e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.a.a.a.i.d.a(java.lang.String, java.lang.ClassLoader):com.agilebinary.a.a.a.i.d");
    }

    public final String a() {
        return this.c;
    }

    public final String toString() {
        StringBuffer stringBuffer = new StringBuffer(this.f116a.length() + 20 + this.b.length() + this.c.length() + this.d.length() + this.e.length());
        stringBuffer.append("VersionInfo(").append(this.f116a).append(':').append(this.b);
        if (!"UNAVAILABLE".equals(this.c)) {
            stringBuffer.append(':').append(this.c);
        }
        if (!"UNAVAILABLE".equals(this.d)) {
            stringBuffer.append(':').append(this.d);
        }
        stringBuffer.append(')');
        if (!"UNAVAILABLE".equals(this.e)) {
            stringBuffer.append('@').append(this.e);
        }
        return stringBuffer.toString();
    }
}
