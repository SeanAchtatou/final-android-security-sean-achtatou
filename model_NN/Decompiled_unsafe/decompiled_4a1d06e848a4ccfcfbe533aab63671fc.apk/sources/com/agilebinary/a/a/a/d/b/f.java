package com.agilebinary.a.a.a.d.b;

import com.agilebinary.a.a.a.a.b;
import com.agilebinary.a.a.a.a.d;
import com.agilebinary.a.a.a.a.e;
import com.agilebinary.a.a.a.a.h;
import com.agilebinary.a.a.a.ab;
import com.agilebinary.a.a.a.f.k;
import com.agilebinary.a.a.b.a.a;
import org.apache.commons.logging.Log;

public final class f implements ab {

    /* renamed from: a  reason: collision with root package name */
    private final Log f88a = a.a(getClass());

    public final void a(com.agilebinary.a.a.a.f fVar, k kVar) {
        e eVar;
        h c;
        if (fVar == null) {
            throw new IllegalArgumentException("HTTP request may not be null");
        } else if (kVar == null) {
            throw new IllegalArgumentException("HTTP context may not be null");
        } else if (!fVar.a("Proxy-Authorization") && (eVar = (e) kVar.a("http.auth.proxy-scope")) != null && (c = eVar.c()) != null) {
            if (eVar.d() == null) {
                this.f88a.debug("User credentials not available");
            } else if (eVar.e() != null || !c.d()) {
                try {
                    fVar.a(c instanceof d ? ((d) c).a() : c.f());
                } catch (b e) {
                    if (this.f88a.isErrorEnabled()) {
                        this.f88a.error(new StringBuilder().insert(0, "Proxy authentication error: ").append(e.getMessage()).toString());
                    }
                }
            }
        }
    }
}
