package com.agilebinary.a.a.a.c.c;

import com.agilebinary.a.a.a.e.e;
import com.agilebinary.a.a.a.i;
import com.agilebinary.a.a.a.i.c;
import com.agilebinary.a.a.a.j.a;
import com.agilebinary.a.a.a.x;

public final class n extends f {
    private final i b;
    private final c c;

    public n(a aVar, i iVar, e eVar) {
        super(aVar, eVar);
        if (iVar == null) {
            throw new IllegalArgumentException("Response factory may not be null");
        }
        this.b = iVar;
        this.c = new c(128);
    }

    /* access modifiers changed from: protected */
    public final x a(a aVar) {
        this.c.a();
        if (aVar.a(this.c) == -1) {
            throw new com.agilebinary.a.a.a.n("The target server failed to respond");
        }
        return this.b.a(this.f61a.b(this.c, new com.agilebinary.a.a.a.b.i(0, this.c.c())));
    }
}
