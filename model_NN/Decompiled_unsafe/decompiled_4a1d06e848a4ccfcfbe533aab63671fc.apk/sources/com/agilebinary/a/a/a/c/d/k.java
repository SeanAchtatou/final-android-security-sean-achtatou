package com.agilebinary.a.a.a.c.d;

import com.agilebinary.a.a.a.c;
import com.agilebinary.a.a.a.p;
import com.agilebinary.a.a.a.t;

public final class k extends l implements p {
    private c c;
    /* access modifiers changed from: private */
    public boolean d;

    public k(p pVar) {
        super(pVar);
        h hVar;
        k kVar;
        c h = pVar.h();
        if (h != null) {
            hVar = new h(this, h);
            kVar = this;
        } else {
            hVar = null;
            kVar = this;
        }
        kVar.c = hVar;
        this.d = false;
    }

    public final boolean g_() {
        t c2 = c("Expect");
        return c2 != null && "100-continue".equalsIgnoreCase(c2.b());
    }

    public final c h() {
        return this.c;
    }

    public final boolean i() {
        return this.c == null || this.c.a() || !this.d;
    }
}
