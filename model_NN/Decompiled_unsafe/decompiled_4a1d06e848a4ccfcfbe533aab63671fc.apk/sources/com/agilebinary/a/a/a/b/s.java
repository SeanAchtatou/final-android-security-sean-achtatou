package com.agilebinary.a.a.a.b;

import com.agilebinary.a.a.a.a;
import com.agilebinary.a.a.a.aa;
import com.agilebinary.a.a.a.f.d;
import com.agilebinary.a.a.a.h;
import com.agilebinary.a.a.a.i.c;
import com.agilebinary.a.a.a.t;
import com.agilebinary.a.a.a.u;

public final class s implements t {

    /* renamed from: a  reason: collision with root package name */
    public static final s f21a = new s();
    private a b;

    public s() {
        this((byte) 0);
    }

    private /* synthetic */ s(byte b2) {
        this.b = aa.d;
    }

    private /* synthetic */ a c(c cVar, i iVar) {
        boolean z = true;
        if (cVar == null) {
            throw new IllegalArgumentException("Char array buffer may not be null");
        } else if (iVar == null) {
            throw new IllegalArgumentException("Parser cursor may not be null");
        } else {
            String a2 = this.b.a();
            int length = a2.length();
            int b2 = iVar.b();
            int a3 = iVar.a();
            d(cVar, iVar);
            int b3 = iVar.b();
            if (b3 + length + 4 > a3) {
                throw new u(new StringBuilder().insert(0, "Not a valid protocol version: ").append(cVar.a(b2, a3)).toString());
            }
            boolean z2 = true;
            int i = 0;
            boolean z3 = true;
            while (z2 && i < length) {
                z3 = cVar.a(b3 + i) == a2.charAt(i);
                i++;
                z2 = z3;
            }
            if (!z3) {
                z = z3;
            } else if (cVar.a(b3 + length) != '/') {
                z = false;
            }
            if (!z) {
                throw new u(new StringBuilder().insert(0, "Not a valid protocol version: ").append(cVar.a(b2, a3)).toString());
            }
            int i2 = length + 1 + b3;
            int a4 = cVar.a(46, i2, a3);
            if (a4 == -1) {
                throw new u(new StringBuilder().insert(0, "Invalid protocol version number: ").append(cVar.a(b2, a3)).toString());
            }
            try {
                int parseInt = Integer.parseInt(cVar.b(i2, a4));
                int i3 = a4 + 1;
                int a5 = cVar.a(32, i3, a3);
                if (a5 == -1) {
                    a5 = a3;
                }
                try {
                    int parseInt2 = Integer.parseInt(cVar.b(i3, a5));
                    iVar.a(a5);
                    return this.b.a(parseInt, parseInt2);
                } catch (NumberFormatException e) {
                    throw new u(new StringBuilder().insert(0, "Invalid protocol minor version number: ").append(cVar.a(b2, a3)).toString());
                }
            } catch (NumberFormatException e2) {
                throw new u(new StringBuilder().insert(0, "Invalid protocol major version number: ").append(cVar.a(b2, a3)).toString());
            }
        }
    }

    private static /* synthetic */ void d(c cVar, i iVar) {
        int b2 = iVar.b();
        int a2 = iVar.a();
        int i = b2;
        while (b2 < a2 && d.a(cVar.a(i))) {
            b2 = i + 1;
            i = b2;
        }
        iVar.a(i);
    }

    public final t a(c cVar) {
        return new e(cVar);
    }

    public final boolean a(c cVar, i iVar) {
        if (cVar == null) {
            throw new IllegalArgumentException("Char array buffer may not be null");
        } else if (iVar == null) {
            throw new IllegalArgumentException("Parser cursor may not be null");
        } else {
            int b2 = iVar.b();
            String a2 = this.b.a();
            int length = a2.length();
            if (cVar.c() < length + 4) {
                return false;
            }
            if (b2 < 0) {
                b2 = (cVar.c() - 4) - length;
            } else if (b2 == 0) {
                int i = b2;
                while (true) {
                    if (b2 < cVar.c()) {
                        if (!d.a(cVar.a(b2))) {
                            break;
                        }
                        i = b2 + 1;
                        b2 = i;
                    } else {
                        b2 = i;
                        break;
                    }
                }
            }
            if (b2 + length + 4 > cVar.c()) {
                return false;
            }
            boolean z = true;
            boolean z2 = true;
            int i2 = 0;
            while (z && i2 < length) {
                z2 = cVar.a(b2 + i2) == a2.charAt(i2);
                i2++;
                z = z2;
            }
            return z2 ? cVar.a(b2 + length) == '/' : z2;
        }
    }

    public final h b(c cVar, i iVar) {
        if (cVar == null) {
            throw new IllegalArgumentException("Char array buffer may not be null");
        } else if (iVar == null) {
            throw new IllegalArgumentException("Parser cursor may not be null");
        } else {
            int b2 = iVar.b();
            int a2 = iVar.a();
            try {
                a c = c(cVar, iVar);
                d(cVar, iVar);
                int b3 = iVar.b();
                int a3 = cVar.a(32, b3, a2);
                int i = a3 < 0 ? a2 : a3;
                String b4 = cVar.b(b3, i);
                int i2 = 0;
                int i3 = 0;
                while (i2 < b4.length()) {
                    if (!Character.isDigit(b4.charAt(i3))) {
                        throw new u(new StringBuilder().insert(0, "Status line contains invalid status code: ").append(cVar.a(b2, a2)).toString());
                    }
                    i2 = i3 + 1;
                    i3 = i2;
                }
                return new m(c, Integer.parseInt(b4), i < a2 ? cVar.b(i, a2) : "");
            } catch (NumberFormatException e) {
                throw new u(new StringBuilder().insert(0, "Status line contains invalid status code: ").append(cVar.a(b2, a2)).toString());
            } catch (IndexOutOfBoundsException e2) {
                throw new u(new StringBuilder().insert(0, "Invalid status line: ").append(cVar.a(b2, a2)).toString());
            }
        }
    }
}
