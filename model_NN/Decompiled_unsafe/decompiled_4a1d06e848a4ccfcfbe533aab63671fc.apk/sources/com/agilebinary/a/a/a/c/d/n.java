package com.agilebinary.a.a.a.c.d;

import com.agilebinary.a.a.a.a.c;
import com.agilebinary.a.a.a.a.g;
import com.agilebinary.a.a.a.d.e;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public final class n implements e {

    /* renamed from: a  reason: collision with root package name */
    private final ConcurrentHashMap f73a = new ConcurrentHashMap();

    private static /* synthetic */ g a(Map map, c cVar) {
        g gVar = (g) map.get(cVar);
        if (gVar != null) {
            return gVar;
        }
        c cVar2 = null;
        int i = -1;
        for (c cVar3 : map.keySet()) {
            int a2 = cVar.a(cVar3);
            if (a2 > i) {
                i = a2;
                cVar2 = cVar3;
            }
        }
        return cVar2 != null ? (g) map.get(cVar2) : gVar;
    }

    public final g a(c cVar) {
        if (cVar != null) {
            return a(this.f73a, cVar);
        }
        throw new IllegalArgumentException("Authentication scope may not be null");
    }

    public final String toString() {
        return this.f73a.toString();
    }
}
