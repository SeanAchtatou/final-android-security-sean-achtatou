package com.agilebinary.a.a.a.h.c;

import com.agilebinary.a.a.a.b;
import java.net.InetAddress;

public final class f implements b, Cloneable {

    /* renamed from: a  reason: collision with root package name */
    private final b f108a;
    private final InetAddress b;
    private boolean c;
    private b[] d;
    private g e;
    private e f;
    private boolean g;

    private /* synthetic */ f(b bVar, InetAddress inetAddress) {
        if (bVar == null) {
            throw new IllegalArgumentException("Target host may not be null.");
        }
        this.f108a = bVar;
        this.b = inetAddress;
        this.e = g.PLAIN;
        this.f = e.PLAIN;
    }

    public f(c cVar) {
        this(cVar.a(), cVar.b());
    }

    public final b a() {
        return this.f108a;
    }

    public final b a(int i) {
        if (i < 0) {
            throw new IllegalArgumentException(new StringBuilder().insert(0, "Hop index must not be negative: ").append(i).toString());
        }
        int c2 = c();
        if (i < c2) {
            return i < c2 + -1 ? this.d[i] : this.f108a;
        }
        throw new IllegalArgumentException(new StringBuilder().insert(0, "Hop index ").append(i).append(" exceeds tracked route length ").append(c2).append(".").toString());
    }

    public final void a(b bVar, boolean z) {
        if (bVar == null) {
            throw new IllegalArgumentException("Proxy host may not be null.");
        } else if (this.c) {
            throw new IllegalStateException("Already connected.");
        } else {
            this.c = true;
            this.d = new b[]{bVar};
            this.g = z;
        }
    }

    public final void a(boolean z) {
        if (this.c) {
            throw new IllegalStateException("Already connected.");
        }
        this.c = true;
        this.g = z;
    }

    public final InetAddress b() {
        return this.b;
    }

    public final void b(boolean z) {
        if (!this.c) {
            throw new IllegalStateException("No tunnel unless connected.");
        } else if (this.d == null) {
            throw new IllegalStateException("No tunnel without proxy.");
        } else {
            this.e = g.TUNNELLED;
            this.g = z;
        }
    }

    public final int c() {
        if (!this.c) {
            return 0;
        }
        if (this.d == null) {
            return 1;
        }
        return this.d.length + 1;
    }

    public final void c(boolean z) {
        if (!this.c) {
            throw new IllegalStateException("No layered protocol unless connected.");
        }
        this.f = e.LAYERED;
        this.g = z;
    }

    public final Object clone() {
        return super.clone();
    }

    public final boolean d() {
        return this.e == g.TUNNELLED;
    }

    public final boolean e() {
        return this.f == e.LAYERED;
    }

    public final boolean equals(Object obj) {
        boolean z;
        f fVar;
        boolean z2;
        f fVar2;
        boolean z3;
        boolean z4;
        int i = 0;
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof f)) {
            return false;
        }
        f fVar3 = (f) obj;
        boolean equals = this.f108a.equals(fVar3.f108a);
        if (this.b == fVar3.b || (this.b != null && this.b.equals(fVar3.b))) {
            fVar = this;
            z = true;
        } else {
            fVar = this;
            z = false;
        }
        if (fVar.d == fVar3.d || !(this.d == null || fVar3.d == null || this.d.length != fVar3.d.length)) {
            fVar2 = this;
            z2 = true;
        } else {
            fVar2 = this;
            z2 = false;
        }
        if (fVar2.c == fVar3.c && this.g == fVar3.g && this.e == fVar3.e && this.f == fVar3.f) {
            z4 = true;
            z3 = equals;
        } else {
            z3 = equals;
            z4 = false;
        }
        boolean z5 = z3 & z & z2 & z4;
        if (!z5 || this.d == null) {
            return z5;
        }
        boolean z6 = z5;
        while (z6 && i < this.d.length) {
            z5 = this.d[i].equals(fVar3.d[i]);
            i++;
            z6 = z5;
        }
        return z5;
    }

    public final boolean f() {
        return this.g;
    }

    public final boolean g() {
        return this.c;
    }

    public final c h() {
        if (!this.c) {
            return null;
        }
        return new c(this.f108a, this.b, this.d, this.g, this.e, this.f);
    }

    public final int hashCode() {
        int i;
        int hashCode = this.f108a.hashCode();
        if (this.b != null) {
            hashCode ^= this.b.hashCode();
        }
        if (this.d != null) {
            int length = this.d.length ^ hashCode;
            int i2 = 0;
            i = length;
            while (true) {
                int i3 = i2;
                if (i2 >= this.d.length) {
                    break;
                }
                i ^= this.d[i3].hashCode();
                i2 = i3 + 1;
            }
        } else {
            i = hashCode;
        }
        if (this.c) {
            i ^= 286331153;
        }
        if (this.g) {
            i ^= 572662306;
        }
        return (this.e.hashCode() ^ i) ^ this.f.hashCode();
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder((c() * 30) + 50);
        sb.append("RouteTracker[");
        if (this.b != null) {
            sb.append(this.b);
            sb.append("->");
        }
        sb.append('{');
        if (this.c) {
            sb.append('c');
        }
        if (this.e == g.TUNNELLED) {
            sb.append('t');
        }
        if (this.f == e.LAYERED) {
            sb.append('l');
        }
        if (this.g) {
            sb.append('s');
        }
        sb.append("}->");
        if (this.d != null) {
            int i = 0;
            while (true) {
                int i2 = i;
                if (i >= this.d.length) {
                    break;
                }
                sb.append(this.d[i2]);
                sb.append("->");
                i = i2 + 1;
            }
        }
        sb.append(this.f108a);
        sb.append(']');
        return sb.toString();
    }
}
