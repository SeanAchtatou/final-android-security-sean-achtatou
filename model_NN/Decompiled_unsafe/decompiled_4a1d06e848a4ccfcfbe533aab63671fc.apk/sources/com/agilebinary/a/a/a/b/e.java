package com.agilebinary.a.a.a.b;

import com.agilebinary.a.a.a.i.c;
import com.agilebinary.a.a.a.m;
import com.agilebinary.a.a.a.r;
import com.agilebinary.a.a.a.u;
import java.io.Serializable;

public final class e implements r, Serializable, Cloneable {

    /* renamed from: a  reason: collision with root package name */
    private final String f10a;
    private final c b;
    private final int c;

    public e(c cVar) {
        if (cVar == null) {
            throw new IllegalArgumentException("Char array buffer may not be null");
        }
        int c2 = cVar.c(58);
        if (c2 == -1) {
            throw new u(new StringBuilder().insert(0, "Invalid header: ").append(cVar.toString()).toString());
        }
        String b2 = cVar.b(0, c2);
        if (b2.length() == 0) {
            throw new u(new StringBuilder().insert(0, "Invalid header: ").append(cVar.toString()).toString());
        }
        this.b = cVar;
        this.f10a = b2;
        this.c = c2 + 1;
    }

    public final String a() {
        return this.f10a;
    }

    public final String b() {
        return this.b.b(this.c, this.b.c());
    }

    public final m[] c() {
        i iVar = new i(0, this.b.c());
        iVar.a(this.c);
        return f.f11a.a(this.b, iVar);
    }

    public final Object clone() {
        return super.clone();
    }

    public final int d() {
        return this.c;
    }

    public final c e() {
        return this.b;
    }

    public final String toString() {
        return this.b.toString();
    }
}
