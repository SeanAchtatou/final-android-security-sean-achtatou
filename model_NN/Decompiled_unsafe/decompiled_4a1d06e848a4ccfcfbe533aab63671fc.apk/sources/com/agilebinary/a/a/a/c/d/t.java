package com.agilebinary.a.a.a.c.d;

import com.agilebinary.a.a.a.a.h;
import com.agilebinary.a.a.a.a.i;
import com.agilebinary.a.a.a.d.b;
import com.agilebinary.a.a.a.f.d;
import com.agilebinary.a.a.a.f.k;
import com.agilebinary.a.a.a.i.c;
import com.agilebinary.a.a.a.j;
import com.agilebinary.a.a.a.r;
import com.agilebinary.a.a.b.a.a;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import org.apache.commons.logging.Log;

public abstract class t implements b {
    private static final List b = Collections.unmodifiableList(Arrays.asList("negotiate", "NTLM", "Digest", "Basic"));

    /* renamed from: a  reason: collision with root package name */
    private final Log f79a = a.a(getClass());

    protected static Map a(com.agilebinary.a.a.a.t[] tVarArr) {
        c cVar;
        int i;
        int i2;
        HashMap hashMap = new HashMap(tVarArr.length);
        int length = tVarArr.length;
        int i3 = 0;
        int i4 = 0;
        while (i3 < length) {
            com.agilebinary.a.a.a.t tVar = tVarArr[i4];
            if (tVar instanceof r) {
                c e = ((r) tVar).e();
                i2 = ((r) tVar).d();
                cVar = e;
                i = i2;
            } else {
                String b2 = tVar.b();
                if (b2 == null) {
                    throw new i("Header value is null");
                }
                c cVar2 = new c(b2.length());
                cVar2.a(b2);
                i = 0;
                cVar = cVar2;
                i2 = 0;
            }
            while (i2 < cVar.c() && d.a(cVar.a(i))) {
                i2 = i + 1;
                i = i2;
            }
            int i5 = i;
            int i6 = i;
            while (i5 < cVar.c() && !d.a(cVar.a(i6))) {
                i5 = i6 + 1;
                i6 = i5;
            }
            hashMap.put(cVar.a(i, i6).toLowerCase(Locale.ENGLISH), tVar);
            i3 = i4 + 1;
            i4 = i3;
        }
        return hashMap;
    }

    public final h a(Map map, j jVar, k kVar) {
        h hVar;
        h hVar2;
        com.agilebinary.a.a.a.a.a aVar = (com.agilebinary.a.a.a.a.a) kVar.a("http.authscheme-registry");
        if (aVar == null) {
            throw new IllegalStateException("AuthScheme registry not set in HTTP context");
        }
        List a2 = a(jVar, kVar);
        if (a2 == null) {
            a2 = b;
        }
        if (this.f79a.isDebugEnabled()) {
            this.f79a.debug(new StringBuilder().insert(0, "Authentication schemes in the order of preference: ").append(a2).toString());
        }
        Iterator it = a2.iterator();
        loop0:
        while (true) {
            while (true) {
                if (!it.hasNext()) {
                    hVar = null;
                    hVar2 = null;
                    break loop0;
                }
                String str = (String) it.next();
                if (((com.agilebinary.a.a.a.t) map.get(str.toLowerCase(Locale.ENGLISH))) == null) {
                    if (!this.f79a.isDebugEnabled()) {
                        break;
                    }
                    this.f79a.debug(new StringBuilder().insert(0, "Challenge for ").append(str).append(" authentication scheme not available").toString());
                } else {
                    if (this.f79a.isDebugEnabled()) {
                        this.f79a.debug(new StringBuilder().insert(0, str).append(" authentication scheme selected").toString());
                    }
                    try {
                        jVar.g();
                        hVar = aVar.a(str);
                        hVar2 = hVar;
                        break loop0;
                    } catch (IllegalStateException e) {
                        if (!this.f79a.isWarnEnabled()) {
                            break;
                        }
                        this.f79a.warn(new StringBuilder().insert(0, "Authentication scheme ").append(str).append(" not supported").toString());
                    }
                }
            }
        }
        if (hVar != null) {
            return hVar2;
        }
        throw new com.agilebinary.a.a.a.a.b(new StringBuilder().insert(0, "Unable to respond to any of these challenges: ").append(map).toString());
    }

    /* access modifiers changed from: protected */
    public List a(j jVar, k kVar) {
        return b;
    }
}
