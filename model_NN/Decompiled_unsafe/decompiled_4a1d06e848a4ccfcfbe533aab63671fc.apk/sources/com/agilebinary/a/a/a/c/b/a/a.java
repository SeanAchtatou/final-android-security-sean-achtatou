package com.agilebinary.a.a.a.c.b.a;

import com.agilebinary.a.a.a.h.a.b;
import com.agilebinary.a.a.a.h.c.c;
import com.agilebinary.a.a.a.h.g;
import com.agilebinary.a.a.a.h.l;
import com.agilebinary.a.a.a.h.m;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import org.apache.commons.logging.Log;

public final class a extends j {
    private final Log c = com.agilebinary.a.a.b.a.a.a(getClass());
    /* access modifiers changed from: private */
    public final Lock d;
    private m e;
    private b f;
    private Set g;
    private Queue h;
    private Queue i;
    private Map j;
    private final long k;
    private final TimeUnit l;
    private volatile boolean m;
    private volatile int n;
    private volatile int o;

    public a(m mVar, b bVar, TimeUnit timeUnit) {
        if (mVar == null) {
            throw new IllegalArgumentException("Connection operator may not be null");
        } else if (bVar == null) {
            throw new IllegalArgumentException("Connections per route may not be null");
        } else {
            this.d = this.f41a;
            this.g = this.b;
            this.e = mVar;
            this.f = bVar;
            this.n = 20;
            this.h = new LinkedList();
            this.i = new LinkedList();
            this.j = new HashMap();
            this.k = 30;
            this.l = timeUnit;
        }
    }

    private /* synthetic */ c a(c cVar) {
        this.d.lock();
        try {
            c cVar2 = (c) this.j.get(cVar);
            if (cVar2 == null) {
                cVar2 = new c(cVar, this.f);
                this.j.put(cVar, cVar2);
            }
            return cVar2;
        } finally {
            this.d.unlock();
        }
    }

    private /* synthetic */ g a(c cVar, m mVar) {
        if (this.c.isDebugEnabled()) {
            this.c.debug(new StringBuilder().insert(0, "Creating new connection [").append(cVar.a()).append("]").toString());
        }
        g gVar = new g(mVar, cVar.a(), this.k, this.l);
        this.d.lock();
        try {
            cVar.b(gVar);
            this.o++;
            this.g.add(gVar);
            return gVar;
        } finally {
            this.d.unlock();
        }
    }

    private /* synthetic */ g a(c cVar, Object obj) {
        boolean z = false;
        this.d.lock();
        g gVar = null;
        boolean z2 = false;
        while (!z) {
            try {
                g a2 = cVar.a(obj);
                if (a2 != null) {
                    if (this.c.isDebugEnabled()) {
                        this.c.debug(new StringBuilder().insert(0, "Getting free connection [").append(cVar.a()).append("][").append(obj).append("]").toString());
                    }
                    this.h.remove(a2);
                    if (a2.a(System.currentTimeMillis())) {
                        if (this.c.isDebugEnabled()) {
                            this.c.debug(new StringBuilder().insert(0, "Closing expired free connection [").append(cVar.a()).append("][").append(obj).append("]").toString());
                        }
                        a(a2);
                        cVar.e();
                        this.o--;
                        gVar = a2;
                        z = z2;
                    } else {
                        this.g.add(a2);
                        z2 = true;
                        gVar = a2;
                        z = true;
                    }
                } else if (this.c.isDebugEnabled()) {
                    this.c.debug(new StringBuilder().insert(0, "No free connections [").append(cVar.a()).append("][").append(obj).append("]").toString());
                    z2 = true;
                    gVar = a2;
                    z = true;
                } else {
                    z2 = true;
                    gVar = a2;
                    z = true;
                }
            } catch (Throwable th) {
                this.d.unlock();
                throw th;
            }
        }
        this.d.unlock();
        return gVar;
    }

    private /* synthetic */ void a(g gVar) {
        g c2 = gVar.c();
        if (c2 != null) {
            try {
                c2.k();
            } catch (IOException e2) {
                this.c.debug("I/O error closing connection", e2);
            }
        }
    }

    private /* synthetic */ void b(g gVar) {
        c d2 = gVar.d();
        if (this.c.isDebugEnabled()) {
            this.c.debug(new StringBuilder().insert(0, "Deleting connection [").append(d2).append("][").append(gVar.a()).append("]").toString());
        }
        this.d.lock();
        try {
            a(gVar);
            c a2 = a(d2);
            a2.c(gVar);
            this.o--;
            if (a2.c()) {
                this.j.remove(d2);
            }
        } finally {
            this.d.unlock();
        }
    }

    /* access modifiers changed from: protected */
    public final g a(c cVar, Object obj, long j2, TimeUnit timeUnit, f fVar) {
        Date date;
        a aVar;
        boolean z;
        a aVar2;
        a aVar3;
        if (j2 > 0) {
            date = new Date(System.currentTimeMillis() + timeUnit.toMillis(j2));
            aVar = this;
        } else {
            date = null;
            aVar = this;
        }
        aVar.d.lock();
        c a2 = a(cVar);
        k kVar = null;
        g gVar = null;
        loop0:
        while (true) {
            g gVar2 = gVar;
            while (gVar2 == null) {
                if (!this.m) {
                    if (this.c.isDebugEnabled()) {
                        this.c.debug(new StringBuilder().insert(0, "[").append(cVar).append("] total kept alive: ").append(this.h.size()).append(", total issued: ").append(this.g.size()).append(", total allocated: ").append(this.o).append(" out of ").append(this.n).toString());
                    }
                    gVar = a(a2, obj);
                    if (gVar != null) {
                        break loop0;
                    }
                    if (a2.d() > 0) {
                        z = true;
                        aVar2 = this;
                    } else {
                        z = false;
                        aVar2 = this;
                    }
                    if (aVar2.c.isDebugEnabled()) {
                        this.c.debug(new StringBuilder().insert(0, "Available capacity: ").append(a2.d()).append(" out of ").append(a2.b()).append(" [").append(cVar).append("][").append(obj).append("]").toString());
                    }
                    if (z && this.o < this.n) {
                        gVar = a(a2, this.e);
                        gVar2 = gVar;
                    } else if (!z || this.h.isEmpty()) {
                        if (this.c.isDebugEnabled()) {
                            this.c.debug(new StringBuilder().insert(0, "Need to wait for connection [").append(cVar).append("][").append(obj).append("]").toString());
                        }
                        if (kVar == null) {
                            kVar = new k(this.d.newCondition(), a2);
                            fVar.a(kVar);
                        }
                        a2.a(kVar);
                        this.i.add(kVar);
                        boolean a3 = kVar.a(date);
                        a2.b(kVar);
                        this.i.remove(kVar);
                        if (!a3 && date != null && date.getTime() <= System.currentTimeMillis()) {
                            throw new l("Timeout waiting for connection");
                        }
                    } else {
                        this.d.lock();
                        try {
                            g gVar3 = (g) this.h.remove();
                            if (gVar3 != null) {
                                b(gVar3);
                            } else if (this.c.isDebugEnabled()) {
                                this.c.debug("No free connection to delete");
                                aVar3 = this;
                                aVar3.d.unlock();
                                a2 = a(cVar);
                                gVar = a(a2, this.e);
                                gVar2 = gVar;
                            }
                            aVar3 = this;
                            aVar3.d.unlock();
                            a2 = a(cVar);
                            gVar = a(a2, this.e);
                            gVar2 = gVar;
                        } catch (Throwable th) {
                            this.d.unlock();
                            throw th;
                        }
                    }
                } else {
                    throw new IllegalStateException("Connection pool shut down");
                }
            }
            break loop0;
        }
        this.d.unlock();
        return gVar;
    }

    public final void a() {
        this.d.lock();
        try {
            if (!this.m) {
                this.m = true;
                Iterator it = this.g.iterator();
                while (it.hasNext()) {
                    it.remove();
                    a((g) it.next());
                }
                Iterator it2 = this.h.iterator();
                for (Iterator it3 = it2; it3.hasNext(); it3 = it2) {
                    g gVar = (g) it2.next();
                    it2.remove();
                    if (this.c.isDebugEnabled()) {
                        this.c.debug(new StringBuilder().insert(0, "Closing connection [").append(gVar.d()).append("][").append(gVar.a()).append("]").toString());
                    }
                    a(gVar);
                }
                Iterator it4 = this.i.iterator();
                for (Iterator it5 = it4; it5.hasNext(); it5 = it4) {
                    it4.remove();
                    ((k) it4.next()).a();
                }
                this.j.clear();
                this.d.unlock();
            }
        } finally {
            this.d.unlock();
        }
    }

    public final void a(long j2, TimeUnit timeUnit) {
        if (timeUnit == null) {
            throw new IllegalArgumentException("Time unit must not be null.");
        }
        if (j2 < 0) {
            j2 = 0;
        }
        if (this.c.isDebugEnabled()) {
            this.c.debug(new StringBuilder().insert(0, "Closing connections idle longer than ").append(j2).append(" ").append(timeUnit).toString());
        }
        long currentTimeMillis = System.currentTimeMillis();
        long millis = timeUnit.toMillis(j2);
        this.d.lock();
        try {
            Iterator it = this.h.iterator();
            while (true) {
                while (true) {
                    if (it.hasNext()) {
                        g gVar = (g) it.next();
                        if (gVar.e() <= currentTimeMillis - millis) {
                            if (this.c.isDebugEnabled()) {
                                this.c.debug(new StringBuilder().insert(0, "Closing connection last used @ ").append(new Date(gVar.e())).toString());
                            }
                            it.remove();
                            b(gVar);
                        }
                    } else {
                        return;
                    }
                }
            }
        } finally {
            this.d.unlock();
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:30:0x00f9 A[Catch:{ all -> 0x0151, all -> 0x0118 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(com.agilebinary.a.a.a.c.b.a.g r8, boolean r9, long r10, java.util.concurrent.TimeUnit r12) {
        /*
            r7 = this;
            r4 = 0
            com.agilebinary.a.a.a.h.c.c r2 = r8.d()
            org.apache.commons.logging.Log r0 = r7.c
            boolean r0 = r0.isDebugEnabled()
            if (r0 == 0) goto L_0x0039
            org.apache.commons.logging.Log r0 = r7.c
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r3 = "Releasing connection ["
            java.lang.StringBuilder r1 = r1.insert(r4, r3)
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r3 = "]["
            java.lang.StringBuilder r1 = r1.append(r3)
            java.lang.Object r3 = r8.a()
            java.lang.StringBuilder r1 = r1.append(r3)
            java.lang.String r3 = "]"
            java.lang.StringBuilder r1 = r1.append(r3)
            java.lang.String r1 = r1.toString()
            r0.debug(r1)
        L_0x0039:
            java.util.concurrent.locks.Lock r0 = r7.d
            r0.lock()
            boolean r0 = r7.m     // Catch:{ all -> 0x0118 }
            if (r0 == 0) goto L_0x004b
            r7.a(r8)     // Catch:{ all -> 0x0118 }
            java.util.concurrent.locks.Lock r0 = r7.d
            r0.unlock()
        L_0x004a:
            return
        L_0x004b:
            java.util.Set r0 = r7.g     // Catch:{ all -> 0x0118 }
            r0.remove(r8)     // Catch:{ all -> 0x0118 }
            com.agilebinary.a.a.a.c.b.a.c r3 = r7.a(r2)     // Catch:{ all -> 0x0118 }
            if (r9 == 0) goto L_0x010e
            org.apache.commons.logging.Log r0 = r7.c     // Catch:{ all -> 0x0118 }
            boolean r0 = r0.isDebugEnabled()     // Catch:{ all -> 0x0118 }
            if (r0 == 0) goto L_0x00ae
            r0 = 0
            int r0 = (r10 > r0 ? 1 : (r10 == r0 ? 0 : -1))
            if (r0 < 0) goto L_0x0108
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x0118 }
            r0.<init>()     // Catch:{ all -> 0x0118 }
            java.lang.StringBuilder r0 = r0.append(r10)     // Catch:{ all -> 0x0118 }
            java.lang.String r1 = " "
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ all -> 0x0118 }
            java.lang.StringBuilder r0 = r0.append(r12)     // Catch:{ all -> 0x0118 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0118 }
            r1 = r0
            r0 = r7
        L_0x007d:
            org.apache.commons.logging.Log r0 = r0.c     // Catch:{ all -> 0x0118 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x0118 }
            r4.<init>()     // Catch:{ all -> 0x0118 }
            r5 = 0
            java.lang.String r6 = "Pooling connection ["
            java.lang.StringBuilder r4 = r4.insert(r5, r6)     // Catch:{ all -> 0x0118 }
            java.lang.StringBuilder r2 = r4.append(r2)     // Catch:{ all -> 0x0118 }
            java.lang.String r4 = "]["
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ all -> 0x0118 }
            java.lang.Object r4 = r8.a()     // Catch:{ all -> 0x0118 }
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ all -> 0x0118 }
            java.lang.String r4 = "]; keep alive for "
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ all -> 0x0118 }
            java.lang.StringBuilder r1 = r2.append(r1)     // Catch:{ all -> 0x0118 }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x0118 }
            r0.debug(r1)     // Catch:{ all -> 0x0118 }
        L_0x00ae:
            r3.a(r8)     // Catch:{ all -> 0x0118 }
            r8.a(r10, r12)     // Catch:{ all -> 0x0118 }
            java.util.Queue r0 = r7.h     // Catch:{ all -> 0x0118 }
            r0.add(r8)     // Catch:{ all -> 0x0118 }
        L_0x00b9:
            r0 = 0
            java.util.concurrent.locks.Lock r1 = r7.d     // Catch:{ all -> 0x0118 }
            r1.lock()     // Catch:{ all -> 0x0118 }
            if (r3 == 0) goto L_0x011f
            boolean r1 = r3.f()     // Catch:{ all -> 0x0151 }
            if (r1 == 0) goto L_0x011f
            org.apache.commons.logging.Log r0 = r7.c     // Catch:{ all -> 0x0151 }
            boolean r0 = r0.isDebugEnabled()     // Catch:{ all -> 0x0151 }
            if (r0 == 0) goto L_0x00f2
            org.apache.commons.logging.Log r0 = r7.c     // Catch:{ all -> 0x0151 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x0151 }
            r1.<init>()     // Catch:{ all -> 0x0151 }
            r2 = 0
            java.lang.String r4 = "Notifying thread waiting on pool ["
            java.lang.StringBuilder r1 = r1.insert(r2, r4)     // Catch:{ all -> 0x0151 }
            com.agilebinary.a.a.a.h.c.c r2 = r3.a()     // Catch:{ all -> 0x0151 }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x0151 }
            java.lang.String r2 = "]"
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x0151 }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x0151 }
            r0.debug(r1)     // Catch:{ all -> 0x0151 }
        L_0x00f2:
            com.agilebinary.a.a.a.c.b.a.k r0 = r3.g()     // Catch:{ all -> 0x0151 }
        L_0x00f6:
            r1 = r0
        L_0x00f7:
            if (r0 == 0) goto L_0x00fc
            r1.a()     // Catch:{ all -> 0x0151 }
        L_0x00fc:
            java.util.concurrent.locks.Lock r0 = r7.d     // Catch:{ all -> 0x0118 }
            r0.unlock()     // Catch:{ all -> 0x0118 }
            java.util.concurrent.locks.Lock r0 = r7.d
            r0.unlock()
            goto L_0x004a
        L_0x0108:
            java.lang.String r0 = "ever"
            r1 = r0
            r0 = r7
            goto L_0x007d
        L_0x010e:
            r3.e()     // Catch:{ all -> 0x0118 }
            int r0 = r7.o     // Catch:{ all -> 0x0118 }
            int r0 = r0 + -1
            r7.o = r0     // Catch:{ all -> 0x0118 }
            goto L_0x00b9
        L_0x0118:
            r0 = move-exception
            java.util.concurrent.locks.Lock r1 = r7.d
            r1.unlock()
            throw r0
        L_0x011f:
            java.util.Queue r1 = r7.i     // Catch:{ all -> 0x0151 }
            boolean r1 = r1.isEmpty()     // Catch:{ all -> 0x0151 }
            if (r1 != 0) goto L_0x0140
            org.apache.commons.logging.Log r0 = r7.c     // Catch:{ all -> 0x0151 }
            boolean r0 = r0.isDebugEnabled()     // Catch:{ all -> 0x0151 }
            if (r0 == 0) goto L_0x0136
            org.apache.commons.logging.Log r0 = r7.c     // Catch:{ all -> 0x0151 }
            java.lang.String r1 = "Notifying thread waiting on any pool"
            r0.debug(r1)     // Catch:{ all -> 0x0151 }
        L_0x0136:
            java.util.Queue r0 = r7.i     // Catch:{ all -> 0x0151 }
            java.lang.Object r0 = r0.remove()     // Catch:{ all -> 0x0151 }
            com.agilebinary.a.a.a.c.b.a.k r0 = (com.agilebinary.a.a.a.c.b.a.k) r0     // Catch:{ all -> 0x0151 }
            r1 = r0
            goto L_0x00f7
        L_0x0140:
            org.apache.commons.logging.Log r1 = r7.c     // Catch:{ all -> 0x0151 }
            boolean r1 = r1.isDebugEnabled()     // Catch:{ all -> 0x0151 }
            if (r1 == 0) goto L_0x00f6
            org.apache.commons.logging.Log r1 = r7.c     // Catch:{ all -> 0x0151 }
            java.lang.String r2 = "Notifying no-one, there are no waiting threads"
            r1.debug(r2)     // Catch:{ all -> 0x0151 }
            r1 = r0
            goto L_0x00f7
        L_0x0151:
            r0 = move-exception
            java.util.concurrent.locks.Lock r1 = r7.d     // Catch:{ all -> 0x0118 }
            r1.unlock()     // Catch:{ all -> 0x0118 }
            throw r0     // Catch:{ all -> 0x0118 }
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.a.a.a.c.b.a.a.a(com.agilebinary.a.a.a.c.b.a.g, boolean, long, java.util.concurrent.TimeUnit):void");
    }

    public final void b() {
        this.d.lock();
        try {
            this.n = 3;
        } finally {
            this.d.unlock();
        }
    }
}
