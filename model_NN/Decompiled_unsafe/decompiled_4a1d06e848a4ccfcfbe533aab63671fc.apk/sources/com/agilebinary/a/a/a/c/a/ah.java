package com.agilebinary.a.a.a.c.a;

import com.agilebinary.a.a.a.k.c;
import com.agilebinary.a.a.a.k.e;
import com.agilebinary.a.a.a.k.f;
import com.agilebinary.a.a.a.k.k;
import com.agilebinary.a.a.a.m;
import com.agilebinary.a.a.a.o;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

public abstract class ah extends u {
    protected static String a(f fVar) {
        String b = fVar.b();
        int lastIndexOf = b.lastIndexOf(47);
        if (lastIndexOf < 0) {
            return b;
        }
        if (lastIndexOf == 0) {
            lastIndexOf = 1;
        }
        return b.substring(0, lastIndexOf);
    }

    /* access modifiers changed from: protected */
    public List a(m[] mVarArr, f fVar) {
        ArrayList arrayList = new ArrayList(mVarArr.length);
        int length = mVarArr.length;
        int i = 0;
        int i2 = 0;
        while (i < length) {
            m mVar = mVarArr[i2];
            String a2 = mVar.a();
            String b = mVar.b();
            if (a2 == null || a2.length() == 0) {
                throw new e("Cookie name may not be empty");
            }
            d dVar = new d(a2, b);
            dVar.c(a(fVar));
            dVar.b(fVar.a());
            o[] c = mVar.c();
            int length2 = c.length - 1;
            int i3 = length2;
            while (length2 >= 0) {
                o oVar = c[i3];
                String lowerCase = oVar.a().toLowerCase(Locale.ENGLISH);
                dVar.a(lowerCase, oVar.b());
                k a3 = a(lowerCase);
                if (a3 != null) {
                    a3.a(dVar, oVar.b());
                }
                length2 = i3 - 1;
                i3 = length2;
            }
            arrayList.add(dVar);
            i = i2 + 1;
            i2 = i;
        }
        return arrayList;
    }

    public void a(c cVar, f fVar) {
        if (cVar == null) {
            throw new IllegalArgumentException("Cookie may not be null");
        } else if (fVar == null) {
            throw new IllegalArgumentException("Cookie origin may not be null");
        } else {
            Iterator it = c().iterator();
            while (it.hasNext()) {
                ((k) it.next()).a(cVar, fVar);
            }
        }
    }

    public boolean b(c cVar, f fVar) {
        if (cVar == null) {
            throw new IllegalArgumentException("Cookie may not be null");
        } else if (fVar == null) {
            throw new IllegalArgumentException("Cookie origin may not be null");
        } else {
            for (k b : c()) {
                if (!b.b(cVar, fVar)) {
                    return false;
                }
            }
            return true;
        }
    }
}
