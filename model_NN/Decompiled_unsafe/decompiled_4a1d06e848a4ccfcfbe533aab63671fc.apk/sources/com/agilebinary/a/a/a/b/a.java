package com.agilebinary.a.a.a.b;

import com.agilebinary.a.a.a.i.c;
import com.agilebinary.a.a.a.i.f;
import com.agilebinary.a.a.a.m;
import com.agilebinary.a.a.a.o;

public final class a implements m, Cloneable {

    /* renamed from: a  reason: collision with root package name */
    private final String f7a;
    private final String b;
    private final o[] c;

    public a(String str, String str2, o[] oVarArr) {
        if (str == null) {
            throw new IllegalArgumentException("Name may not be null");
        }
        this.f7a = str;
        this.b = str2;
        if (oVarArr != null) {
            this.c = oVarArr;
        } else {
            this.c = new o[0];
        }
    }

    public final o a(String str) {
        o oVar;
        if (str == null) {
            throw new IllegalArgumentException("Name may not be null");
        }
        int i = 0;
        while (true) {
            int i2 = i;
            oVar = null;
            if (i2 >= this.c.length) {
                break;
            }
            oVar = this.c[i2];
            if (oVar.a().equalsIgnoreCase(str)) {
                break;
            }
            i = i2 + 1;
        }
        return oVar;
    }

    public final String a() {
        return this.f7a;
    }

    public final String b() {
        return this.b;
    }

    public final o[] c() {
        return (o[]) this.c.clone();
    }

    public final Object clone() {
        return super.clone();
    }

    public final boolean equals(Object obj) {
        boolean z;
        if (obj == null) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof m)) {
            return false;
        }
        a aVar = (a) obj;
        if (!this.f7a.equals(aVar.f7a) || !f.a(this.b, aVar.b)) {
            return false;
        }
        o[] oVarArr = this.c;
        o[] oVarArr2 = aVar.c;
        if (oVarArr == null) {
            z = oVarArr2 == null;
        } else {
            if (oVarArr2 != null && oVarArr.length == oVarArr2.length) {
                int i = 0;
                int i2 = 0;
                while (true) {
                    if (i < oVarArr.length) {
                        if (!f.a(oVarArr[i2], oVarArr2[i2])) {
                            break;
                        }
                        i = i2 + 1;
                        i2 = i;
                    } else {
                        z = true;
                        break;
                    }
                }
            }
            z = false;
        }
        return z;
    }

    public final int hashCode() {
        int a2 = f.a(f.a(17, this.f7a), this.b);
        int i = 0;
        while (true) {
            int i2 = i;
            if (i >= this.c.length) {
                return a2;
            }
            a2 = f.a(a2, this.c[i2]);
            i = i2 + 1;
        }
    }

    public final String toString() {
        c cVar = new c(64);
        cVar.a(this.f7a);
        if (this.b != null) {
            cVar.a("=");
            cVar.a(this.b);
        }
        int i = 0;
        while (true) {
            int i2 = i;
            if (i >= this.c.length) {
                return cVar.toString();
            }
            cVar.a("; ");
            cVar.a(String.valueOf(this.c[i2]));
            i = i2 + 1;
        }
    }
}
