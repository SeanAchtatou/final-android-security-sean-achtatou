package com.agilebinary.a.a.a.h.d;

import com.agilebinary.a.a.a.h.e.a;
import java.security.cert.CertificateParsingException;
import java.security.cert.X509Certificate;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.net.ssl.SSLException;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocket;

public abstract class d implements b {

    /* renamed from: a  reason: collision with root package name */
    private static final String[] f110a;

    static {
        String[] strArr = {"ac", "co", "com", "ed", "edu", "go", "gouv", "gov", "info", "lg", "ne", "net", "or", "org"};
        f110a = strArr;
        Arrays.sort(strArr);
    }

    private static /* synthetic */ int a(String str) {
        int i = 0;
        int i2 = 0;
        while (true) {
            int i3 = i;
            if (i >= str.length()) {
                return i2;
            }
            if (str.charAt(i3) == '.') {
                i2++;
            }
            i = i3 + 1;
        }
    }

    private /* synthetic */ void a(String str, X509Certificate x509Certificate) {
        String[] strArr;
        LinkedList linkedList = new LinkedList();
        StringTokenizer stringTokenizer = new StringTokenizer(x509Certificate.getSubjectX500Principal().toString(), ",");
        loop0:
        while (true) {
            while (true) {
                if (!stringTokenizer.hasMoreTokens()) {
                    break loop0;
                }
                String nextToken = stringTokenizer.nextToken();
                int indexOf = nextToken.indexOf("CN=");
                if (indexOf >= 0) {
                    linkedList.add(nextToken.substring(indexOf + 3));
                }
            }
        }
        if (!linkedList.isEmpty()) {
            strArr = new String[linkedList.size()];
            linkedList.toArray(strArr);
        } else {
            strArr = null;
        }
        a(str, strArr, a(x509Certificate, str));
    }

    /* JADX WARNING: Removed duplicated region for block: B:42:0x00c8  */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x010e  */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x00e2 A[EDGE_INSN: B:62:0x00e2->B:50:0x00e2 ?: BREAK  , SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void a(java.lang.String r9, java.lang.String[] r10, java.lang.String[] r11, boolean r12) {
        /*
            r8 = 46
            r2 = 1
            r1 = 0
            java.util.LinkedList r4 = new java.util.LinkedList
            r4.<init>()
            if (r10 == 0) goto L_0x0017
            int r0 = r10.length
            if (r0 <= 0) goto L_0x0017
            r0 = r10[r1]
            if (r0 == 0) goto L_0x0017
            r0 = r10[r1]
            r4.add(r0)
        L_0x0017:
            if (r11 == 0) goto L_0x0029
            int r5 = r11.length
            r0 = r1
            r3 = r1
        L_0x001c:
            if (r0 >= r5) goto L_0x0029
            r0 = r11[r3]
            if (r0 == 0) goto L_0x0025
            r4.add(r0)
        L_0x0025:
            int r0 = r3 + 1
            r3 = r0
            goto L_0x001c
        L_0x0029:
            boolean r0 = r4.isEmpty()
            if (r0 == 0) goto L_0x004e
            javax.net.ssl.SSLException r0 = new javax.net.ssl.SSLException
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Certificate for <"
            java.lang.StringBuilder r1 = r2.insert(r1, r3)
            java.lang.StringBuilder r1 = r1.append(r9)
            java.lang.String r2 = "> doesn't contain CN or DNS subjectAlt"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x004e:
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r0 = r9.trim()
            java.util.Locale r3 = java.util.Locale.ENGLISH
            java.lang.String r6 = r0.toLowerCase(r3)
            java.util.Iterator r4 = r4.iterator()
            r0 = r1
        L_0x0062:
            boolean r3 = r4.hasNext()
            if (r3 == 0) goto L_0x0115
            java.lang.Object r0 = r4.next()
            java.lang.String r0 = (java.lang.String) r0
            java.util.Locale r3 = java.util.Locale.ENGLISH
            java.lang.String r3 = r0.toLowerCase(r3)
            java.lang.String r0 = " <"
            r5.append(r0)
            r5.append(r3)
            r0 = 62
            r5.append(r0)
            boolean r0 = r4.hasNext()
            if (r0 == 0) goto L_0x008c
            java.lang.String r0 = " OR"
            r5.append(r0)
        L_0x008c:
            java.lang.String r0 = "*."
            boolean r0 = r3.startsWith(r0)
            if (r0 == 0) goto L_0x0109
            int r0 = r3.lastIndexOf(r8)
            if (r0 < 0) goto L_0x0109
            int r0 = r3.length()
            r7 = 7
            if (r0 < r7) goto L_0x0107
            r7 = 9
            if (r0 > r7) goto L_0x0107
            int r7 = r0 + -3
            char r7 = r3.charAt(r7)
            if (r7 != r8) goto L_0x0107
            r7 = 2
            int r0 = r0 + -3
            java.lang.String r0 = r3.substring(r7, r0)
            java.lang.String[] r7 = com.agilebinary.a.a.a.h.d.d.f110a
            int r0 = java.util.Arrays.binarySearch(r7, r0)
            if (r0 < 0) goto L_0x0107
            r0 = r1
        L_0x00bd:
            if (r0 == 0) goto L_0x0109
            boolean r0 = b(r9)
            if (r0 != 0) goto L_0x0109
            r0 = r2
        L_0x00c6:
            if (r0 == 0) goto L_0x010e
            java.lang.String r0 = r3.substring(r2)
            boolean r0 = r6.endsWith(r0)
            if (r0 == 0) goto L_0x00df
            if (r12 == 0) goto L_0x00df
            int r0 = a(r6)
            int r3 = a(r3)
            if (r0 != r3) goto L_0x010b
            r0 = r2
        L_0x00df:
            r3 = r0
        L_0x00e0:
            if (r3 == 0) goto L_0x0062
        L_0x00e2:
            if (r3 != 0) goto L_0x0114
            javax.net.ssl.SSLException r0 = new javax.net.ssl.SSLException
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "hostname in certificate didn't match: <"
            java.lang.StringBuilder r1 = r2.insert(r1, r3)
            java.lang.StringBuilder r1 = r1.append(r9)
            java.lang.String r2 = "> !="
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r5)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x0107:
            r0 = r2
            goto L_0x00bd
        L_0x0109:
            r0 = r1
            goto L_0x00c6
        L_0x010b:
            r0 = r1
            r3 = r1
            goto L_0x00e0
        L_0x010e:
            boolean r0 = r6.equals(r3)
            r3 = r0
            goto L_0x00e0
        L_0x0114:
            return
        L_0x0115:
            r3 = r0
            goto L_0x00e2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.a.a.a.h.d.d.a(java.lang.String, java.lang.String[], java.lang.String[], boolean):void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
     arg types: [java.util.logging.Level, java.lang.String, java.security.cert.CertificateParsingException]
     candidates:
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
    private static /* synthetic */ String[] a(X509Certificate x509Certificate, String str) {
        Collection<List<?>> collection;
        Collection<List<?>> collection2;
        int i = b(str) ? 7 : 2;
        LinkedList linkedList = new LinkedList();
        try {
            collection = x509Certificate.getSubjectAlternativeNames();
            collection2 = collection;
        } catch (CertificateParsingException e) {
            Logger.getLogger(d.class.getName()).log(Level.FINE, "Error parsing certificate.", (Throwable) e);
            collection = null;
            collection2 = null;
        }
        if (collection != null) {
            Iterator<List<?>> it = collection2.iterator();
            loop0:
            while (true) {
                while (true) {
                    if (!it.hasNext()) {
                        break loop0;
                    }
                    List next = it.next();
                    if (((Integer) next.get(0)).intValue() == i) {
                        linkedList.add((String) next.get(1));
                    }
                }
            }
        }
        if (linkedList.isEmpty()) {
            return null;
        }
        String[] strArr = new String[linkedList.size()];
        linkedList.toArray(strArr);
        return strArr;
    }

    private static /* synthetic */ boolean b(String str) {
        return str != null && (a.a(str) || a.b(str));
    }

    public final void a(String str, SSLSocket sSLSocket) {
        if (str == null) {
            throw new NullPointerException("host to verify is null");
        }
        SSLSession session = sSLSocket.getSession();
        if (session == null) {
            sSLSocket.getInputStream().available();
            session = sSLSocket.getSession();
            if (session == null) {
                sSLSocket.startHandshake();
                session = sSLSocket.getSession();
            }
        }
        a(str, (X509Certificate) session.getPeerCertificates()[0]);
    }

    public final boolean verify(String str, SSLSession sSLSession) {
        try {
            a(str, (X509Certificate) sSLSession.getPeerCertificates()[0]);
            return true;
        } catch (SSLException e) {
            return false;
        }
    }
}
