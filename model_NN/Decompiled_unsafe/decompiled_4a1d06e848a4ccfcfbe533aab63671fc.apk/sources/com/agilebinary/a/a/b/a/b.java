package com.agilebinary.a.a.b.a;

import com.agilebinary.mobilemonitor.a.a.a.a;
import org.apache.commons.logging.Log;

public final class b implements Log {

    /* renamed from: a  reason: collision with root package name */
    private String f120a;

    public b(String str) {
        this.f120a = str;
    }

    public final void debug(Object obj) {
        obj.toString();
    }

    public final void debug(Object obj, Throwable th) {
        obj.toString();
        a.b(th);
    }

    public final void error(Object obj) {
        obj.toString();
    }

    public final void error(Object obj, Throwable th) {
        obj.toString();
        a.e(th);
    }

    public final void fatal(Object obj) {
        obj.toString();
    }

    public final void fatal(Object obj, Throwable th) {
        obj.toString();
        a.e(th);
    }

    public final void info(Object obj) {
        obj.toString();
    }

    public final void info(Object obj, Throwable th) {
        obj.toString();
        a.c(th);
    }

    public final boolean isDebugEnabled() {
        if (!"org.apache.http.wire".equals(this.f120a) && "org.apache.http.headers".equals(this.f120a)) {
        }
        return false;
    }

    public final boolean isErrorEnabled() {
        return false;
    }

    public final boolean isFatalEnabled() {
        return false;
    }

    public final boolean isInfoEnabled() {
        return false;
    }

    public final boolean isTraceEnabled() {
        return false;
    }

    public final boolean isWarnEnabled() {
        return false;
    }

    public final void trace(Object obj) {
        obj.toString();
    }

    public final void trace(Object obj, Throwable th) {
        obj.toString();
        a.a(th);
    }

    public final void warn(Object obj) {
        obj.toString();
    }

    public final void warn(Object obj, Throwable th) {
        obj.toString();
        a.d(th);
    }
}
