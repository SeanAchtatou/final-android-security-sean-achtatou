package com.agilebinary.a.a.a.d.b;

import com.agilebinary.a.a.a.ad;
import com.agilebinary.a.a.a.d.d;
import com.agilebinary.a.a.a.f.k;
import com.agilebinary.a.a.a.k.e;
import com.agilebinary.a.a.a.k.f;
import com.agilebinary.a.a.a.k.j;
import com.agilebinary.a.a.a.t;
import com.agilebinary.a.a.a.w;
import com.agilebinary.a.a.b.a.a;
import java.util.Iterator;
import org.apache.commons.logging.Log;

public final class c implements ad {

    /* renamed from: a  reason: collision with root package name */
    private final Log f86a = a.a(getClass());

    private /* synthetic */ void a(w wVar, j jVar, f fVar, d dVar) {
        while (true) {
            while (wVar.hasNext()) {
                t a2 = wVar.a();
                try {
                    Iterator it = jVar.a(a2, fVar).iterator();
                    while (true) {
                        while (it.hasNext()) {
                            com.agilebinary.a.a.a.k.c cVar = (com.agilebinary.a.a.a.k.c) it.next();
                            try {
                                jVar.a(cVar, fVar);
                                dVar.a(cVar);
                                if (this.f86a.isDebugEnabled()) {
                                    this.f86a.debug(new StringBuilder().insert(0, "Cookie accepted: \"").append(cVar).append("\". ").toString());
                                }
                            } catch (e e) {
                                if (this.f86a.isWarnEnabled()) {
                                    this.f86a.warn(new StringBuilder().insert(0, "Cookie rejected: \"").append(cVar).append("\". ").append(e.getMessage()).toString());
                                }
                            }
                        }
                        break;
                    }
                } catch (e e2) {
                    if (this.f86a.isWarnEnabled()) {
                        this.f86a.warn(new StringBuilder().insert(0, "Invalid cookie header: \"").append(a2).append("\". ").append(e2.getMessage()).toString());
                    }
                }
            }
            return;
        }
    }

    public final void a(com.agilebinary.a.a.a.j jVar, k kVar) {
        if (jVar == null) {
            throw new IllegalArgumentException("HTTP request may not be null");
        } else if (kVar == null) {
            throw new IllegalArgumentException("HTTP context may not be null");
        } else {
            j jVar2 = (j) kVar.a("http.cookie-spec");
            if (jVar2 != null) {
                d dVar = (d) kVar.a("http.cookie-store");
                if (dVar == null) {
                    this.f86a.info("CookieStore not available in HTTP context");
                    return;
                }
                f fVar = (f) kVar.a("http.cookie-origin");
                if (fVar == null) {
                    this.f86a.info("CookieOrigin not available in HTTP context");
                    return;
                }
                a(jVar.d("Set-Cookie"), jVar2, fVar, dVar);
                if (jVar2.a() > 0) {
                    a(jVar.d("Set-Cookie2"), jVar2, fVar, dVar);
                }
            }
        }
    }
}
