package com.agilebinary.a.a.a.f;

import com.agilebinary.a.a.a.ab;

public final class i implements ab {
    /* JADX WARNING: Removed duplicated region for block: B:20:0x006e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(com.agilebinary.a.a.a.f r5, com.agilebinary.a.a.a.f.k r6) {
        /*
            r4 = this;
            if (r5 != 0) goto L_0x000a
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "HTTP request may not be null"
            r0.<init>(r1)
            throw r0
        L_0x000a:
            if (r6 != 0) goto L_0x0014
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "HTTP context may not be null"
            r0.<init>(r1)
            throw r0
        L_0x0014:
            com.agilebinary.a.a.a.d r0 = r5.a()
            com.agilebinary.a.a.a.a r3 = r0.b()
            com.agilebinary.a.a.a.d r0 = r5.a()
            java.lang.String r0 = r0.a()
            java.lang.String r1 = "CONNECT"
            boolean r0 = r0.equalsIgnoreCase(r1)
            if (r0 == 0) goto L_0x0035
            com.agilebinary.a.a.a.aa r0 = com.agilebinary.a.a.a.aa.c
            boolean r0 = r3.a(r0)
            if (r0 == 0) goto L_0x0035
        L_0x0034:
            return
        L_0x0035:
            java.lang.String r0 = "Host"
            boolean r0 = r5.a(r0)
            if (r0 != 0) goto L_0x0034
            java.lang.String r0 = "http.target_host"
            java.lang.Object r0 = r6.a(r0)
            com.agilebinary.a.a.a.b r0 = (com.agilebinary.a.a.a.b) r0
            if (r0 != 0) goto L_0x007e
            java.lang.String r1 = "http.connection"
            java.lang.Object r1 = r6.a(r1)
            com.agilebinary.a.a.a.q r1 = (com.agilebinary.a.a.a.q) r1
            boolean r2 = r1 instanceof com.agilebinary.a.a.a.ae
            if (r2 == 0) goto L_0x0088
            r2 = r1
            com.agilebinary.a.a.a.ae r2 = (com.agilebinary.a.a.a.ae) r2
            java.net.InetAddress r2 = r2.p()
            com.agilebinary.a.a.a.ae r1 = (com.agilebinary.a.a.a.ae) r1
            int r1 = r1.q()
            if (r2 == 0) goto L_0x0088
            com.agilebinary.a.a.a.b r0 = new com.agilebinary.a.a.a.b
            java.lang.String r2 = r2.getHostName()
            r0.<init>(r2, r1)
            r1 = r0
        L_0x006c:
            if (r1 != 0) goto L_0x007e
            com.agilebinary.a.a.a.aa r0 = com.agilebinary.a.a.a.aa.c
            boolean r0 = r3.a(r0)
            if (r0 != 0) goto L_0x0034
            com.agilebinary.a.a.a.l r0 = new com.agilebinary.a.a.a.l
            java.lang.String r1 = "Target host missing"
            r0.<init>(r1)
            throw r0
        L_0x007e:
            java.lang.String r1 = "Host"
            java.lang.String r0 = r0.d()
            r5.a(r1, r0)
            goto L_0x0034
        L_0x0088:
            r1 = r0
            goto L_0x006c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.a.a.a.f.i.a(com.agilebinary.a.a.a.f, com.agilebinary.a.a.a.f.k):void");
    }
}
