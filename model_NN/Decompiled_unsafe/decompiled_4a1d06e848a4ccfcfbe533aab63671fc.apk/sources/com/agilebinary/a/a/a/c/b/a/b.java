package com.agilebinary.a.a.a.c.b.a;

import com.agilebinary.a.a.a.h.a.c;
import com.agilebinary.a.a.a.h.b.h;
import com.agilebinary.a.a.a.h.k;
import com.agilebinary.a.a.a.h.m;
import com.agilebinary.a.a.b.a.a;
import java.util.concurrent.TimeUnit;
import org.apache.commons.logging.Log;

public final class b implements k {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public final Log f36a;
    private h b;
    private j c;
    private a d;
    private m e;
    private c f;

    public b(h hVar, TimeUnit timeUnit) {
        if (hVar == null) {
            throw new IllegalArgumentException("Scheme registry may not be null");
        }
        this.f36a = a.a(getClass());
        this.b = hVar;
        this.f = new c();
        this.e = new com.agilebinary.a.a.a.c.b.b(hVar);
        this.d = new a(this.e, this.f, timeUnit);
        this.c = this.d;
    }

    public final h a() {
        return this.b;
    }

    public final com.agilebinary.a.a.a.h.b a(com.agilebinary.a.a.a.h.c.c cVar, Object obj) {
        return new h(this, new i(this.d, new f(), cVar, obj), cVar);
    }

    public final void a(long j, TimeUnit timeUnit) {
        if (this.f36a.isDebugEnabled()) {
            this.f36a.debug(new StringBuilder().insert(0, "Closing connections idle longer than ").append(j).append(" ").append(timeUnit).toString());
        }
        this.d.a(j, timeUnit);
    }

    /* JADX WARNING: Unknown top exception splitter block from list: {B:41:0x007b=Splitter:B:41:0x007b, B:21:0x003c=Splitter:B:21:0x003c} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(com.agilebinary.a.a.a.h.a r9, long r10, java.util.concurrent.TimeUnit r12) {
        /*
            r8 = this;
            boolean r0 = r9 instanceof com.agilebinary.a.a.a.c.b.a.d
            if (r0 != 0) goto L_0x000c
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "Connection class mismatch, connection not obtained from this manager."
            r0.<init>(r1)
            throw r0
        L_0x000c:
            com.agilebinary.a.a.a.c.b.a.d r9 = (com.agilebinary.a.a.a.c.b.a.d) r9
            com.agilebinary.a.a.a.c.b.a r0 = r9.i()
            if (r0 == 0) goto L_0x0022
            com.agilebinary.a.a.a.h.k r0 = r9.h()
            if (r0 == r8) goto L_0x0022
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "Connection not obtained from this manager."
            r0.<init>(r1)
            throw r0
        L_0x0022:
            monitor-enter(r9)
            com.agilebinary.a.a.a.c.b.a r2 = r9.i()     // Catch:{ all -> 0x005f }
            com.agilebinary.a.a.a.c.b.a.g r2 = (com.agilebinary.a.a.a.c.b.a.g) r2     // Catch:{ all -> 0x005f }
            if (r2 != 0) goto L_0x002d
            monitor-exit(r9)     // Catch:{ all -> 0x005f }
        L_0x002c:
            return
        L_0x002d:
            boolean r0 = r9.l()     // Catch:{ IOException -> 0x006b }
            if (r0 == 0) goto L_0x003c
            boolean r0 = r9.r()     // Catch:{ IOException -> 0x006b }
            if (r0 != 0) goto L_0x003c
            r9.m()     // Catch:{ IOException -> 0x006b }
        L_0x003c:
            boolean r3 = r9.r()     // Catch:{ all -> 0x005f }
            org.apache.commons.logging.Log r0 = r8.f36a     // Catch:{ all -> 0x005f }
            boolean r0 = r0.isDebugEnabled()     // Catch:{ all -> 0x005f }
            if (r0 == 0) goto L_0x0051
            if (r3 == 0) goto L_0x0062
            org.apache.commons.logging.Log r0 = r8.f36a     // Catch:{ all -> 0x005f }
            java.lang.String r1 = "Released connection is reusable."
            r0.debug(r1)     // Catch:{ all -> 0x005f }
        L_0x0051:
            r0 = r9
        L_0x0052:
            r0.j()     // Catch:{ all -> 0x005f }
            com.agilebinary.a.a.a.c.b.a.a r1 = r8.d     // Catch:{ all -> 0x005f }
            r4 = r10
            r6 = r12
            r1.a(r2, r3, r4, r6)     // Catch:{ all -> 0x005f }
            r0 = r9
        L_0x005d:
            monitor-exit(r0)     // Catch:{ all -> 0x005f }
            goto L_0x002c
        L_0x005f:
            r0 = move-exception
            monitor-exit(r9)     // Catch:{ all -> 0x005f }
            throw r0
        L_0x0062:
            org.apache.commons.logging.Log r0 = r8.f36a     // Catch:{ all -> 0x005f }
            java.lang.String r1 = "Released connection is not reusable."
            r0.debug(r1)     // Catch:{ all -> 0x005f }
            r0 = r9
            goto L_0x0052
        L_0x006b:
            r0 = move-exception
            org.apache.commons.logging.Log r1 = r8.f36a     // Catch:{ all -> 0x00a6 }
            boolean r1 = r1.isDebugEnabled()     // Catch:{ all -> 0x00a6 }
            if (r1 == 0) goto L_0x007b
            org.apache.commons.logging.Log r1 = r8.f36a     // Catch:{ all -> 0x00a6 }
            java.lang.String r3 = "Exception shutting down released connection."
            r1.debug(r3, r0)     // Catch:{ all -> 0x00a6 }
        L_0x007b:
            boolean r3 = r9.r()     // Catch:{ all -> 0x005f }
            org.apache.commons.logging.Log r0 = r8.f36a     // Catch:{ all -> 0x005f }
            boolean r0 = r0.isDebugEnabled()     // Catch:{ all -> 0x005f }
            if (r0 == 0) goto L_0x0090
            if (r3 == 0) goto L_0x009d
            org.apache.commons.logging.Log r0 = r8.f36a     // Catch:{ all -> 0x005f }
            java.lang.String r1 = "Released connection is reusable."
            r0.debug(r1)     // Catch:{ all -> 0x005f }
        L_0x0090:
            r0 = r9
        L_0x0091:
            r0.j()     // Catch:{ all -> 0x005f }
            com.agilebinary.a.a.a.c.b.a.a r1 = r8.d     // Catch:{ all -> 0x005f }
            r4 = r10
            r6 = r12
            r1.a(r2, r3, r4, r6)     // Catch:{ all -> 0x005f }
            r0 = r9
            goto L_0x005d
        L_0x009d:
            org.apache.commons.logging.Log r0 = r8.f36a     // Catch:{ all -> 0x005f }
            java.lang.String r1 = "Released connection is not reusable."
            r0.debug(r1)     // Catch:{ all -> 0x005f }
            r0 = r9
            goto L_0x0091
        L_0x00a6:
            r0 = move-exception
            boolean r3 = r9.r()     // Catch:{ all -> 0x005f }
            org.apache.commons.logging.Log r1 = r8.f36a     // Catch:{ all -> 0x005f }
            boolean r1 = r1.isDebugEnabled()     // Catch:{ all -> 0x005f }
            if (r1 == 0) goto L_0x00bc
            if (r3 == 0) goto L_0x00c8
            org.apache.commons.logging.Log r1 = r8.f36a     // Catch:{ all -> 0x005f }
            java.lang.String r4 = "Released connection is reusable."
            r1.debug(r4)     // Catch:{ all -> 0x005f }
        L_0x00bc:
            r1 = r9
        L_0x00bd:
            r1.j()     // Catch:{ all -> 0x005f }
            com.agilebinary.a.a.a.c.b.a.a r1 = r8.d     // Catch:{ all -> 0x005f }
            r4 = r10
            r6 = r12
            r1.a(r2, r3, r4, r6)     // Catch:{ all -> 0x005f }
            throw r0     // Catch:{ all -> 0x005f }
        L_0x00c8:
            org.apache.commons.logging.Log r1 = r8.f36a     // Catch:{ all -> 0x005f }
            java.lang.String r4 = "Released connection is not reusable."
            r1.debug(r4)     // Catch:{ all -> 0x005f }
            r1 = r9
            goto L_0x00bd
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.a.a.a.c.b.a.b.a(com.agilebinary.a.a.a.h.a, long, java.util.concurrent.TimeUnit):void");
    }

    public final void b() {
        this.d.b();
    }

    public final void c() {
        this.f.a(3);
    }

    /* access modifiers changed from: protected */
    public final void finalize() {
        try {
            this.f36a.debug("Shutting down");
            this.d.a();
        } finally {
            super.finalize();
        }
    }
}
