package com.agilebinary.a.a.a.e;

public final class f extends a {
    public final e a(String str, Object obj) {
        e a2;
        synchronized (this) {
            a2 = super.a(str, obj);
        }
        return a2;
    }

    public final Object a(String str) {
        Object a2;
        synchronized (this) {
            a2 = super.a(str);
        }
        return a2;
    }

    public final Object clone() {
        Object clone;
        synchronized (this) {
            clone = super.clone();
        }
        return clone;
    }
}
