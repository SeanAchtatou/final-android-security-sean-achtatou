package com.agilebinary.a.a.a.b;

import com.agilebinary.a.a.a.e.a;
import com.agilebinary.a.a.a.e.e;
import com.agilebinary.a.a.a.t;
import com.agilebinary.a.a.a.w;
import com.agilebinary.a.a.a.x;

public abstract class l implements x {

    /* renamed from: a  reason: collision with root package name */
    protected b f15a;
    protected e b;

    protected l() {
        this((byte) 0);
    }

    private /* synthetic */ l(byte b2) {
        this.f15a = new b();
        this.b = null;
    }

    public final void a(e eVar) {
        if (eVar == null) {
            throw new IllegalArgumentException("HTTP parameters may not be null");
        }
        this.b = eVar;
    }

    public final void a(t tVar) {
        this.f15a.a(tVar);
    }

    public final void a(String str, String str2) {
        if (str == null) {
            throw new IllegalArgumentException("Header name may not be null");
        }
        this.f15a.a(new h(str, str2));
    }

    public final void a(t[] tVarArr) {
        this.f15a.a(tVarArr);
    }

    public final boolean a(String str) {
        return this.f15a.c(str);
    }

    public final void b(String str, String str2) {
        if (str == null) {
            throw new IllegalArgumentException("Header name may not be null");
        }
        this.f15a.b(new h(str, str2));
    }

    public final t[] b(String str) {
        return this.f15a.a(str);
    }

    public final t c(String str) {
        return this.f15a.b(str);
    }

    public final w d(String str) {
        return this.f15a.d(str);
    }

    public final t[] e() {
        return this.f15a.b();
    }

    public final w f() {
        return this.f15a.c();
    }

    public final e g() {
        if (this.b == null) {
            this.b = new a();
        }
        return this.b;
    }
}
