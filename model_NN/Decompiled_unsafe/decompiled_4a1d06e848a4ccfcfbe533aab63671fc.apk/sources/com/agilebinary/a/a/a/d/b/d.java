package com.agilebinary.a.a.a.d.b;

import com.agilebinary.a.a.a.ab;
import com.agilebinary.a.a.a.b;
import com.agilebinary.a.a.a.e.e;
import com.agilebinary.a.a.a.f;
import com.agilebinary.a.a.a.f.k;
import com.agilebinary.a.a.a.h.b.h;
import com.agilebinary.a.a.a.k.c;
import com.agilebinary.a.a.a.k.j;
import com.agilebinary.a.a.a.l;
import com.agilebinary.a.a.a.t;
import com.agilebinary.a.a.b.a.a;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import org.apache.commons.logging.Log;

public final class d implements ab {

    /* renamed from: a  reason: collision with root package name */
    private final Log f87a = a.a(getClass());

    public final void a(f fVar, k kVar) {
        URI uri;
        b bVar;
        int i;
        boolean z;
        t b;
        if (fVar == null) {
            throw new IllegalArgumentException("HTTP request may not be null");
        } else if (kVar == null) {
            throw new IllegalArgumentException("HTTP context may not be null");
        } else if (!fVar.a().a().equalsIgnoreCase("CONNECT")) {
            com.agilebinary.a.a.a.d.d dVar = (com.agilebinary.a.a.a.d.d) kVar.a("http.cookie-store");
            if (dVar == null) {
                this.f87a.info("Cookie store not available in HTTP context");
                return;
            }
            com.agilebinary.a.a.a.k.d dVar2 = (com.agilebinary.a.a.a.k.d) kVar.a("http.cookiespec-registry");
            if (dVar2 == null) {
                this.f87a.info("CookieSpec registry not available in HTTP context");
                return;
            }
            b bVar2 = (b) kVar.a("http.target_host");
            if (bVar2 == null) {
                throw new IllegalStateException("Target host not specified in HTTP context");
            }
            com.agilebinary.a.a.a.h.a aVar = (com.agilebinary.a.a.a.h.a) kVar.a("http.connection");
            if (aVar == null) {
                throw new IllegalStateException("Client connection not specified in HTTP context");
            }
            e g = fVar.g();
            if (g == null) {
                throw new IllegalArgumentException("HTTP parameters may not be null");
            }
            String str = (String) g.a("http.protocol.cookie-policy");
            String str2 = str == null ? "best-match" : str;
            if (this.f87a.isDebugEnabled()) {
                this.f87a.debug(new StringBuilder().insert(0, "CookieSpec selected: ").append(str2).toString());
            }
            if (fVar instanceof com.agilebinary.a.a.a.d.c.b) {
                uri = ((com.agilebinary.a.a.a.d.c.b) fVar).e_();
                bVar = bVar2;
            } else {
                try {
                    uri = new URI(fVar.a().c());
                    bVar = bVar2;
                } catch (URISyntaxException e) {
                    throw new l(new StringBuilder().insert(0, "Invalid request URI: ").append(fVar.a().c()).toString(), e);
                }
            }
            String a2 = bVar.a();
            int b2 = bVar2.b();
            if (b2 < 0) {
                h hVar = (h) kVar.a("http.scheme-registry");
                i = hVar != null ? hVar.b(bVar2.c()).a(b2) : aVar.q();
            } else {
                i = b2;
            }
            com.agilebinary.a.a.a.k.f fVar2 = new com.agilebinary.a.a.a.k.f(a2, i, uri.getPath(), aVar.a());
            j a3 = dVar2.a(str2, fVar.g());
            ArrayList arrayList = new ArrayList(dVar.a());
            ArrayList arrayList2 = new ArrayList();
            Date date = new Date();
            Iterator it = arrayList.iterator();
            loop0:
            while (true) {
                while (it.hasNext()) {
                    c cVar = (c) it.next();
                    if (!cVar.b(date)) {
                        if (a3.b(cVar, fVar2)) {
                            if (this.f87a.isDebugEnabled()) {
                                this.f87a.debug(new StringBuilder().insert(0, "Cookie ").append(cVar).append(" match ").append(fVar2).toString());
                            }
                            arrayList2.add(cVar);
                        }
                    } else if (this.f87a.isDebugEnabled()) {
                        this.f87a.debug(new StringBuilder().insert(0, "Cookie ").append(cVar).append(" expired").toString());
                    }
                }
                break loop0;
            }
            if (!arrayList2.isEmpty()) {
                Iterator it2 = a3.a(arrayList2).iterator();
                while (it2.hasNext()) {
                    fVar.a((t) it2.next());
                }
            }
            int a4 = a3.a();
            if (a4 > 0) {
                Iterator it3 = arrayList2.iterator();
                boolean z2 = false;
                loop3:
                while (true) {
                    z = z2;
                    while (it3.hasNext()) {
                        c cVar2 = (c) it3.next();
                        if (a4 != cVar2.h() || !(cVar2 instanceof com.agilebinary.a.a.a.k.h)) {
                            z = true;
                        } else {
                            z2 = z;
                        }
                    }
                    break loop3;
                }
                if (z && (b = a3.b()) != null) {
                    fVar.a(b);
                }
            }
            kVar.a("http.cookie-spec", a3);
            kVar.a("http.cookie-origin", fVar2);
        }
    }
}
