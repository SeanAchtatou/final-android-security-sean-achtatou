package com.agilebinary.a.a.a.i;

import com.agilebinary.a.a.a.c;
import java.io.InputStream;

public final class b {
    private /* synthetic */ b() {
    }

    public static void a(c cVar) {
        InputStream f;
        if (cVar != null && cVar.g() && (f = cVar.f()) != null) {
            f.close();
        }
    }

    /* JADX INFO: finally extract failed */
    public static byte[] b(c cVar) {
        int i = 4096;
        if (cVar == null) {
            throw new IllegalArgumentException("HTTP entity may not be null");
        }
        InputStream f = cVar.f();
        if (f == null) {
            return null;
        }
        if (cVar.c() > 2147483647L) {
            throw new IllegalArgumentException("HTTP entity too large to be buffered in memory");
        }
        int c = (int) cVar.c();
        if (c >= 0) {
            i = c;
        }
        e eVar = new e(i);
        try {
            byte[] bArr = new byte[4096];
            while (true) {
                int read = f.read(bArr);
                if (read != -1) {
                    eVar.a(bArr, 0, read);
                } else {
                    f.close();
                    return eVar.b();
                }
            }
        } catch (Throwable th) {
            f.close();
            throw th;
        }
    }
}
