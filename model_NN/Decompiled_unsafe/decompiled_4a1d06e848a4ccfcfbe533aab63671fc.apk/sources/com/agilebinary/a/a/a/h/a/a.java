package com.agilebinary.a.a.a.h.a;

import com.agilebinary.a.a.a.b;
import com.agilebinary.a.a.a.e.e;
import com.agilebinary.a.a.a.h.c.c;
import java.net.InetAddress;

public final class a {

    /* renamed from: a  reason: collision with root package name */
    private static b f98a = new b("127.0.0.255", 0, "no-host");
    private static c b = new c(f98a);

    private /* synthetic */ a() {
    }

    public static b a(e eVar) {
        if (eVar == null) {
            throw new IllegalArgumentException("Parameters must not be null.");
        }
        b bVar = (b) eVar.a("http.route.default-proxy");
        if (bVar == null || !f98a.equals(bVar)) {
            return bVar;
        }
        return null;
    }

    public static c b(e eVar) {
        if (eVar == null) {
            throw new IllegalArgumentException("Parameters must not be null.");
        }
        c cVar = (c) eVar.a("http.route.forced-route");
        if (cVar == null || !b.equals(cVar)) {
            return cVar;
        }
        return null;
    }

    public static InetAddress c(e eVar) {
        if (eVar != null) {
            return (InetAddress) eVar.a("http.route.local-address");
        }
        throw new IllegalArgumentException("Parameters must not be null.");
    }
}
