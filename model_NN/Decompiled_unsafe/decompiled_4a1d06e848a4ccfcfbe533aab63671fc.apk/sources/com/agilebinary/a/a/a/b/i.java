package com.agilebinary.a.a.a.b;

import com.agilebinary.a.a.a.i.c;

public final class i {

    /* renamed from: a  reason: collision with root package name */
    private final int f14a;
    private final int b;
    private int c;

    public i(int i, int i2) {
        if (i < 0) {
            throw new IndexOutOfBoundsException("Lower bound cannot be negative");
        } else if (i > i2) {
            throw new IndexOutOfBoundsException("Lower bound cannot be greater then upper bound");
        } else {
            this.f14a = i;
            this.b = i2;
            this.c = i;
        }
    }

    public final int a() {
        return this.b;
    }

    public final void a(int i) {
        if (i < this.f14a) {
            throw new IndexOutOfBoundsException(new StringBuilder().insert(0, "pos: ").append(i).append(" < lowerBound: ").append(this.f14a).toString());
        } else if (i > this.b) {
            throw new IndexOutOfBoundsException(new StringBuilder().insert(0, "pos: ").append(i).append(" > upperBound: ").append(this.b).toString());
        } else {
            this.c = i;
        }
    }

    public final int b() {
        return this.c;
    }

    public final boolean c() {
        return this.c >= this.b;
    }

    public final String toString() {
        c cVar = new c(16);
        cVar.a('[');
        cVar.a(Integer.toString(this.f14a));
        cVar.a('>');
        cVar.a(Integer.toString(this.c));
        cVar.a('>');
        cVar.a(Integer.toString(this.b));
        cVar.a(']');
        return cVar.toString();
    }
}
