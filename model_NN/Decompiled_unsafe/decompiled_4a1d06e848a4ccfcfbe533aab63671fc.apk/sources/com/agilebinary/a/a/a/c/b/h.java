package com.agilebinary.a.a.a.c.b;

import com.agilebinary.a.a.a.b;
import com.agilebinary.a.a.a.c.f;
import com.agilebinary.a.a.a.e.e;
import com.agilebinary.a.a.a.f.k;
import com.agilebinary.a.a.a.h.g;
import com.agilebinary.a.a.a.i;
import com.agilebinary.a.a.a.j;
import com.agilebinary.a.a.a.j.d;
import com.agilebinary.a.a.a.t;
import com.agilebinary.a.a.b.a.a;
import java.io.IOException;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.logging.Log;

public final class h extends f implements k, g {

    /* renamed from: a  reason: collision with root package name */
    private final Log f47a = a.a(getClass());
    private final Log b = a.a("org.apache.http.headers");
    private final Log c = a.a("org.apache.http.wire");
    private volatile Socket d;
    private b e;
    private boolean f;
    private volatile boolean g;
    private final Map h = new HashMap();

    /* access modifiers changed from: protected */
    public final com.agilebinary.a.a.a.j.a a(Socket socket, int i, e eVar) {
        if (i == -1) {
            i = 8192;
        }
        com.agilebinary.a.a.a.j.a a2 = super.a(socket, i, eVar);
        return this.c.isDebugEnabled() ? new k(a2, new q(this.c), com.agilebinary.a.a.a.e.b.a(eVar)) : a2;
    }

    /* access modifiers changed from: protected */
    public final d a(com.agilebinary.a.a.a.j.a aVar, i iVar, e eVar) {
        return new p(aVar, iVar, eVar);
    }

    public final Object a(String str) {
        return this.h.get(str);
    }

    public final void a(com.agilebinary.a.a.a.f fVar) {
        if (this.f47a.isDebugEnabled()) {
            this.f47a.debug(new StringBuilder().insert(0, "Sending request: ").append(fVar.a()).toString());
        }
        super.a(fVar);
        if (this.b.isDebugEnabled()) {
            this.b.debug(new StringBuilder().insert(0, ">> ").append(fVar.a().toString()).toString());
            t[] e2 = fVar.e();
            int length = e2.length;
            int i = 0;
            int i2 = 0;
            while (i < length) {
                this.b.debug(new StringBuilder().insert(0, ">> ").append(e2[i2].toString()).toString());
                i = i2 + 1;
                i2 = i;
            }
        }
    }

    public final void a(String str, Object obj) {
        this.h.put(str, obj);
    }

    public final void a(Socket socket, b bVar) {
        f();
        this.d = socket;
        this.e = bVar;
        if (this.g) {
            socket.close();
            throw new IOException("Connection already shutdown");
        }
    }

    public final void a(Socket socket, b bVar, boolean z, e eVar) {
        a();
        if (bVar == null) {
            throw new IllegalArgumentException("Target host must not be null.");
        } else if (eVar == null) {
            throw new IllegalArgumentException("Parameters must not be null.");
        } else {
            if (socket != null) {
                this.d = socket;
                a(socket, eVar);
            }
            this.e = bVar;
            this.f = z;
        }
    }

    public final void a(boolean z, e eVar) {
        f();
        if (eVar == null) {
            throw new IllegalArgumentException("Parameters must not be null.");
        }
        this.f = z;
        a(this.d, eVar);
    }

    /* access modifiers changed from: protected */
    public final com.agilebinary.a.a.a.j.e b(Socket socket, int i, e eVar) {
        if (i == -1) {
            i = 8192;
        }
        com.agilebinary.a.a.a.j.e b2 = super.b(socket, i, eVar);
        return this.c.isDebugEnabled() ? new j(b2, new q(this.c), com.agilebinary.a.a.a.e.b.a(eVar)) : b2;
    }

    public final j d() {
        j d2 = super.d();
        if (this.f47a.isDebugEnabled()) {
            this.f47a.debug(new StringBuilder().insert(0, "Receiving response: ").append(d2.a()).toString());
        }
        if (this.b.isDebugEnabled()) {
            this.b.debug(new StringBuilder().insert(0, "<< ").append(d2.a().toString()).toString());
            t[] e2 = d2.e();
            int length = e2.length;
            int i = 0;
            int i2 = 0;
            while (i < length) {
                this.b.debug(new StringBuilder().insert(0, "<< ").append(e2[i2].toString()).toString());
                i = i2 + 1;
                i2 = i;
            }
        }
        return d2;
    }

    public final boolean h_() {
        return this.f;
    }

    public final Socket i_() {
        return this.d;
    }

    public final void k() {
        try {
            super.k();
            this.f47a.debug("Connection closed");
        } catch (IOException e2) {
            this.f47a.debug("I/O error closing connection", e2);
        }
    }

    public final void m() {
        this.g = true;
        try {
            super.m();
            this.f47a.debug("Connection shut down");
            Socket socket = this.d;
            if (socket != null) {
                socket.close();
            }
        } catch (IOException e2) {
            this.f47a.debug("I/O error shutting down connection", e2);
        }
    }
}
