package com.agilebinary.a.a.a.c.a;

import com.agilebinary.a.a.a.k.g;
import com.agilebinary.a.a.a.k.j;
import java.util.Collection;

public final class e implements g {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.a.a.a.e.e.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.agilebinary.a.a.a.e.e.a(java.lang.String, int):int
      com.agilebinary.a.a.a.e.e.a(java.lang.String, java.lang.Object):com.agilebinary.a.a.a.e.e
      com.agilebinary.a.a.a.e.e.a(java.lang.String, boolean):boolean */
    public final j a(com.agilebinary.a.a.a.e.e eVar) {
        if (eVar == null) {
            return new ag();
        }
        Collection collection = (Collection) eVar.a("http.protocol.cookie-datepatterns");
        return new ag(collection != null ? (String[]) collection.toArray(new String[collection.size()]) : null, eVar.a("http.protocol.single-cookie-header", false));
    }
}
