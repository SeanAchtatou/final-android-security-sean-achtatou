package com.agilebinary.a.a.a.c.a;

import com.agilebinary.a.a.a.k.b;
import com.agilebinary.a.a.a.k.m;
import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class d implements b, m, Serializable, Cloneable {

    /* renamed from: a  reason: collision with root package name */
    private final String f27a;
    private Map b;
    private String c;
    private String d;
    private String e;
    private Date f;
    private String g;
    private boolean h;
    private int i;

    public d(String str, String str2) {
        if (str == null) {
            throw new IllegalArgumentException("Name may not be null");
        }
        this.f27a = str;
        this.b = new HashMap();
        this.c = str2;
    }

    public final String a() {
        return this.f27a;
    }

    public final void a(int i2) {
        this.i = i2;
    }

    public final void a(String str) {
        this.d = str;
    }

    public final void a(String str, String str2) {
        this.b.put(str, str2);
    }

    public final void a(Date date) {
        this.f = date;
    }

    public final String b() {
        return this.c;
    }

    public final void b(String str) {
        if (str != null) {
            this.e = str.toLowerCase(Locale.ENGLISH);
        } else {
            this.e = null;
        }
    }

    public boolean b(Date date) {
        if (date != null) {
            return this.f != null && this.f.getTime() <= date.getTime();
        }
        throw new IllegalArgumentException("Date may not be null");
    }

    public final String c() {
        return this.e;
    }

    public final void c(String str) {
        this.g = str;
    }

    public Object clone() {
        d dVar = (d) super.clone();
        dVar.b = new HashMap(this.b);
        return dVar;
    }

    public final String d() {
        return this.g;
    }

    public final String d(String str) {
        return (String) this.b.get(str);
    }

    public final boolean e() {
        return this.h;
    }

    public final boolean e(String str) {
        return this.b.get(str) != null;
    }

    public final void f() {
        this.h = true;
    }

    public int[] g() {
        return null;
    }

    public final int h() {
        return this.i;
    }

    public String toString() {
        return "[version: " + Integer.toString(this.i) + "]" + "[name: " + this.f27a + "]" + "[value: " + this.c + "]" + "[domain: " + this.e + "]" + "[path: " + this.g + "]" + "[expiry: " + this.f + "]";
    }
}
