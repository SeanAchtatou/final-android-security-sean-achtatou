package com.agilebinary.a.a.a.d.a;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Iterator;
import java.util.Stack;

public final class b {
    private /* synthetic */ b() {
    }

    private static /* synthetic */ String a(String str) {
        if (str == null) {
            return null;
        }
        StringBuilder sb = new StringBuilder(str.length());
        int i = 0;
        boolean z = false;
        int i2 = 0;
        while (i < str.length()) {
            char charAt = str.charAt(i2);
            if (charAt != '/') {
                sb.append(charAt);
                z = false;
            } else if (!z) {
                sb.append(charAt);
                z = true;
            }
            i = i2 + 1;
            i2 = i;
        }
        return sb.toString();
    }

    private static /* synthetic */ URI a(String str, String str2, int i, String str3, String str4, String str5) {
        StringBuilder sb = new StringBuilder();
        if (str2 != null) {
            if (str != null) {
                sb.append(str);
                sb.append("://");
            }
            sb.append(str2);
            if (i > 0) {
                sb.append(':');
                sb.append(i);
            }
        }
        if (str3 == null || !str3.startsWith("/")) {
            sb.append('/');
        }
        if (str3 != null) {
            sb.append(str3);
        }
        if (str4 != null) {
            sb.append('?');
            sb.append(str4);
        }
        if (str5 != null) {
            sb.append('#');
            sb.append(str5);
        }
        return new URI(sb.toString());
    }

    private static /* synthetic */ URI a(URI uri) {
        String path = uri.getPath();
        if (path == null || path.indexOf("/.") == -1) {
            return uri;
        }
        String[] split = path.split("/");
        Stack stack = new Stack();
        for (int i = 0; i < split.length; i++) {
            if (split[i].length() != 0 && !".".equals(split[i])) {
                if (!"..".equals(split[i])) {
                    stack.push(split[i]);
                } else if (!stack.isEmpty()) {
                    stack.pop();
                }
            }
        }
        StringBuilder sb = new StringBuilder();
        Iterator it = stack.iterator();
        while (it.hasNext()) {
            sb.append('/').append((String) it.next());
        }
        try {
            return new URI(uri.getScheme(), uri.getAuthority(), sb.toString(), uri.getQuery(), uri.getFragment());
        } catch (URISyntaxException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public static URI a(URI uri, com.agilebinary.a.a.a.b bVar) {
        return a(uri, bVar, false);
    }

    public static URI a(URI uri, com.agilebinary.a.a.a.b bVar, boolean z) {
        String rawFragment;
        String str;
        if (uri == null) {
            throw new IllegalArgumentException("URI may nor be null");
        } else if (bVar != null) {
            String c = bVar.c();
            String a2 = bVar.a();
            int b = bVar.b();
            String a3 = a(uri.getRawPath());
            String rawQuery = uri.getRawQuery();
            if (z) {
                String str2 = c;
                rawFragment = null;
                str = str2;
            } else {
                String str3 = c;
                rawFragment = uri.getRawFragment();
                str = str3;
            }
            return a(str, a2, b, a3, rawQuery, rawFragment);
        } else {
            return a(null, null, -1, a(uri.getRawPath()), uri.getRawQuery(), z ? null : uri.getRawFragment());
        }
    }

    public static URI a(URI uri, URI uri2) {
        boolean z;
        boolean z2;
        if (uri == null) {
            throw new IllegalArgumentException("Base URI may nor be null");
        } else if (uri2 == null) {
            throw new IllegalArgumentException("Reference URI may nor be null");
        } else {
            String uri3 = uri2.toString();
            if (uri3.startsWith("?")) {
                String uri4 = uri.toString();
                if (uri4.indexOf(63) >= 0) {
                    uri4 = uri4.substring(0, uri4.indexOf(63));
                }
                return URI.create(new StringBuilder().insert(0, uri4).append(uri2.toString()).toString());
            }
            if (uri3.length() == 0) {
                z = true;
                z2 = true;
            } else {
                z = false;
                z2 = false;
            }
            if (z) {
                uri2 = URI.create("#");
            }
            URI resolve = uri.resolve(uri2);
            if (z2) {
                String uri5 = resolve.toString();
                resolve = URI.create(uri5.substring(0, uri5.indexOf(35)));
            }
            return a(resolve);
        }
    }
}
