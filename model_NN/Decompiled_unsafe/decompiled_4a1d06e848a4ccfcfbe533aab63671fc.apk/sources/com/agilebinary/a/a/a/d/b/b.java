package com.agilebinary.a.a.a.d.b;

import com.agilebinary.a.a.a.a.e;
import com.agilebinary.a.a.a.a.h;
import com.agilebinary.a.a.a.ad;
import com.agilebinary.a.a.a.d.g;
import com.agilebinary.a.a.a.f.k;
import com.agilebinary.a.a.a.j;
import com.agilebinary.a.a.b.a.a;
import org.apache.commons.logging.Log;

public final class b implements ad {

    /* renamed from: a  reason: collision with root package name */
    private final Log f85a = a.a(getClass());

    private /* synthetic */ void a(g gVar, com.agilebinary.a.a.a.b bVar, e eVar) {
        h c;
        if (eVar != null && (c = eVar.c()) != null && c.e()) {
            String b = c.b();
            if ((!b.equalsIgnoreCase("Basic") && !b.equalsIgnoreCase("Digest")) || eVar.e() == null) {
                return;
            }
            if (eVar.d() != null) {
                if (this.f85a.isDebugEnabled()) {
                    this.f85a.debug(new StringBuilder().insert(0, "Caching '").append(b).append("' auth scheme for ").append(bVar).toString());
                }
                gVar.a(bVar, c);
                return;
            }
            gVar.b(bVar);
        }
    }

    public final void a(j jVar, k kVar) {
        if (jVar == null) {
            throw new IllegalArgumentException("HTTP request may not be null");
        } else if (kVar == null) {
            throw new IllegalArgumentException("HTTP context may not be null");
        } else {
            g gVar = (g) kVar.a("http.auth.auth-cache");
            if (gVar != null) {
                a(gVar, (com.agilebinary.a.a.a.b) kVar.a("http.target_host"), (e) kVar.a("http.auth.target-scope"));
                a(gVar, (com.agilebinary.a.a.a.b) kVar.a("http.proxy_host"), (e) kVar.a("http.auth.proxy-scope"));
            }
        }
    }
}
