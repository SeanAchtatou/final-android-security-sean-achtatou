package org.a.a;

import java.util.HashMap;
import java.util.Map;

public final class h {
    public static final b a(String str, Object[] objArr) {
        Throwable th;
        String str2;
        int i;
        if (objArr == null || objArr.length == 0) {
            str2 = str;
            th = null;
        } else {
            Object obj = objArr[objArr.length - 1];
            if (obj instanceof Throwable) {
                th = (Throwable) obj;
                str2 = str;
            } else {
                str2 = str;
                th = null;
            }
        }
        if (str2 == null) {
            return new b(null, objArr, th);
        }
        if (objArr == null) {
            return new b(str);
        }
        StringBuffer stringBuffer = new StringBuffer(str.length() + 50);
        int i2 = 0;
        int i3 = 0;
        int i4 = 0;
        while (i2 < objArr.length) {
            int indexOf = str.indexOf("{}", i4);
            if (indexOf != -1) {
                if (indexOf != 0 && str.charAt(indexOf + -1) == '\\') {
                    if (!(indexOf >= 2 && str.charAt(indexOf + -2) == '\\')) {
                        i3--;
                        stringBuffer.append(str.substring(i4, indexOf - 1));
                        stringBuffer.append('{');
                        i = indexOf + 1;
                    } else {
                        stringBuffer.append(str.substring(i4, indexOf - 1));
                        a(stringBuffer, objArr[i3], new HashMap());
                        i = indexOf + 2;
                    }
                } else {
                    stringBuffer.append(str.substring(i4, indexOf));
                    a(stringBuffer, objArr[i3], new HashMap());
                    i = indexOf + 2;
                }
                int i5 = i3;
                int i6 = i;
                i2 = i5 + 1;
                i4 = i6;
                i3 = i2;
            } else if (i4 == 0) {
                return new b(str, objArr, th);
            } else {
                stringBuffer.append(str.substring(i4, str.length()));
                return new b(stringBuffer.toString(), objArr, th);
            }
        }
        stringBuffer.append(str.substring(i4, str.length()));
        return i3 < objArr.length + -1 ? new b(stringBuffer.toString(), objArr, th) : new b(stringBuffer.toString(), objArr, null);
    }

    private static /* synthetic */ void a(StringBuffer stringBuffer, Object obj, Map map) {
        if (obj == null) {
            stringBuffer.append("null");
        } else if (!obj.getClass().isArray()) {
            try {
                stringBuffer.append(obj.toString());
            } catch (Throwable th) {
                System.err.println(new StringBuilder().insert(0, "SLF4J: Failed toString() invocation on an object of type [").append(obj.getClass().getName()).append("]").toString());
                th.printStackTrace();
                stringBuffer.append("[FAILED toString()]");
            }
        } else if (obj instanceof boolean[]) {
            a(stringBuffer, (boolean[]) obj);
        } else if (obj instanceof byte[]) {
            a(stringBuffer, (byte[]) obj);
        } else if (obj instanceof char[]) {
            a(stringBuffer, (char[]) obj);
        } else if (obj instanceof short[]) {
            a(stringBuffer, (short[]) obj);
        } else if (obj instanceof int[]) {
            a(stringBuffer, (int[]) obj);
        } else if (obj instanceof long[]) {
            a(stringBuffer, (long[]) obj);
        } else if (obj instanceof float[]) {
            a(stringBuffer, (float[]) obj);
        } else if (obj instanceof double[]) {
            a(stringBuffer, (double[]) obj);
        } else {
            a(stringBuffer, (Object[]) obj, map);
        }
    }

    private static /* synthetic */ void a(StringBuffer stringBuffer, byte[] bArr) {
        stringBuffer.append('[');
        int length = bArr.length;
        int i = 0;
        int i2 = 0;
        while (i < length) {
            stringBuffer.append((int) bArr[i2]);
            if (i2 != length - 1) {
                stringBuffer.append(", ");
            }
            i = i2 + 1;
            i2 = i;
        }
        stringBuffer.append(']');
    }

    private static /* synthetic */ void a(StringBuffer stringBuffer, char[] cArr) {
        stringBuffer.append('[');
        int length = cArr.length;
        int i = 0;
        int i2 = 0;
        while (i < length) {
            stringBuffer.append(cArr[i2]);
            if (i2 != length - 1) {
                stringBuffer.append(", ");
            }
            i = i2 + 1;
            i2 = i;
        }
        stringBuffer.append(']');
    }

    private static /* synthetic */ void a(StringBuffer stringBuffer, double[] dArr) {
        stringBuffer.append('[');
        int length = dArr.length;
        int i = 0;
        int i2 = 0;
        while (i < length) {
            stringBuffer.append(dArr[i2]);
            if (i2 != length - 1) {
                stringBuffer.append(", ");
            }
            i = i2 + 1;
            i2 = i;
        }
        stringBuffer.append(']');
    }

    private static /* synthetic */ void a(StringBuffer stringBuffer, float[] fArr) {
        stringBuffer.append('[');
        int length = fArr.length;
        int i = 0;
        int i2 = 0;
        while (i < length) {
            stringBuffer.append(fArr[i2]);
            if (i2 != length - 1) {
                stringBuffer.append(", ");
            }
            i = i2 + 1;
            i2 = i;
        }
        stringBuffer.append(']');
    }

    private static /* synthetic */ void a(StringBuffer stringBuffer, int[] iArr) {
        stringBuffer.append('[');
        int length = iArr.length;
        int i = 0;
        int i2 = 0;
        while (i < length) {
            stringBuffer.append(iArr[i2]);
            if (i2 != length - 1) {
                stringBuffer.append(", ");
            }
            i = i2 + 1;
            i2 = i;
        }
        stringBuffer.append(']');
    }

    private static /* synthetic */ void a(StringBuffer stringBuffer, long[] jArr) {
        stringBuffer.append('[');
        int length = jArr.length;
        int i = 0;
        int i2 = 0;
        while (i < length) {
            stringBuffer.append(jArr[i2]);
            if (i2 != length - 1) {
                stringBuffer.append(", ");
            }
            i = i2 + 1;
            i2 = i;
        }
        stringBuffer.append(']');
    }

    private static /* synthetic */ void a(StringBuffer stringBuffer, Object[] objArr, Map map) {
        stringBuffer.append('[');
        if (!map.containsKey(objArr)) {
            map.put(objArr, null);
            int length = objArr.length;
            int i = 0;
            int i2 = 0;
            while (i < length) {
                a(stringBuffer, objArr[i2], map);
                if (i2 != length - 1) {
                    stringBuffer.append(", ");
                }
                i = i2 + 1;
                i2 = i;
            }
            map.remove(objArr);
        } else {
            stringBuffer.append("...");
        }
        stringBuffer.append(']');
    }

    private static /* synthetic */ void a(StringBuffer stringBuffer, short[] sArr) {
        stringBuffer.append('[');
        int length = sArr.length;
        int i = 0;
        int i2 = 0;
        while (i < length) {
            stringBuffer.append((int) sArr[i2]);
            if (i2 != length - 1) {
                stringBuffer.append(", ");
            }
            i = i2 + 1;
            i2 = i;
        }
        stringBuffer.append(']');
    }

    private static /* synthetic */ void a(StringBuffer stringBuffer, boolean[] zArr) {
        stringBuffer.append('[');
        int length = zArr.length;
        int i = 0;
        int i2 = 0;
        while (i < length) {
            stringBuffer.append(zArr[i2]);
            if (i2 != length - 1) {
                stringBuffer.append(", ");
            }
            i = i2 + 1;
            i2 = i;
        }
        stringBuffer.append(']');
    }
}
