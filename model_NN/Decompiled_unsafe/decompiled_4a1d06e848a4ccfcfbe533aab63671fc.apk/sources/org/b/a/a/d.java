package org.b.a.a;

import android.util.Log;
import android.view.MotionEvent;
import java.lang.reflect.Method;

public final class d {
    private static Method A;
    private static Method B;
    private static Method C;
    private static Method D;
    private static int E;
    private static int F;
    private static final float[] G = new float[20];
    private static final float[] H = new float[20];
    private static final float[] I = new float[20];
    private static final int[] J = new int[20];
    private static boolean v;
    private static Method w;
    private static Method x;
    private static Method y;
    private static Method z;

    /* renamed from: a  reason: collision with root package name */
    private b f320a;
    private c b;
    private c c;
    private float d;
    private float e;
    private float f;
    private float g;
    private float h;
    private float i;
    private boolean j;
    private Object k;
    private a l;
    private long m;
    private long n;
    private float o;
    private float p;
    private float q;
    private float r;
    private float s;
    private float t;
    private int u;

    static {
        boolean z2 = true;
        boolean z3 = false;
        E = 6;
        F = 8;
        try {
            w = MotionEvent.class.getMethod("getPointerCount", new Class[0]);
            x = MotionEvent.class.getMethod("getPointerId", Integer.TYPE);
            y = MotionEvent.class.getMethod("getPressure", Integer.TYPE);
            z = MotionEvent.class.getMethod("getHistoricalX", Integer.TYPE, Integer.TYPE);
            A = MotionEvent.class.getMethod("getHistoricalY", Integer.TYPE, Integer.TYPE);
            B = MotionEvent.class.getMethod("getHistoricalPressure", Integer.TYPE, Integer.TYPE);
            C = MotionEvent.class.getMethod("getX", Integer.TYPE);
            D = MotionEvent.class.getMethod("getY", Integer.TYPE);
            z3 = true;
        } catch (Exception e2) {
            Log.e("MultiTouchController", "static initializer failed", e2);
            z2 = false;
        }
        v = z2;
        if (z3) {
            try {
                E = MotionEvent.class.getField("ACTION_POINTER_UP").getInt(null);
                F = MotionEvent.class.getField("ACTION_POINTER_INDEX_SHIFT").getInt(null);
            } catch (Exception e3) {
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(long, long):long}
      ClspMth{java.lang.Math.max(float, float):float} */
    private /* synthetic */ void a() {
        float d2;
        d dVar;
        float b2;
        d dVar2;
        float c2;
        d dVar3;
        float f2 = 0.0f;
        this.d = this.b.f();
        this.e = this.b.g();
        if (!this.l.g) {
            dVar = this;
            d2 = 0.0f;
        } else {
            d2 = this.b.d();
            dVar = this;
        }
        dVar.f = Math.max(21.3f, d2);
        if (!this.l.h) {
            dVar2 = this;
            b2 = 0.0f;
        } else {
            b2 = this.b.b();
            dVar2 = this;
        }
        dVar2.g = Math.max(30.0f, b2);
        if (!this.l.h) {
            dVar3 = this;
            c2 = 0.0f;
        } else {
            c2 = this.b.c();
            dVar3 = this;
        }
        dVar3.h = Math.max(30.0f, c2);
        if (this.l.i) {
            f2 = this.b.e();
        }
        this.i = f2;
    }

    private /* synthetic */ void b() {
        if (this.k != null) {
            this.f320a.a(this.l);
            float d2 = 1.0f / (!this.l.g ? 1.0f : this.l.c == 0.0f ? 1.0f : this.l.c);
            a();
            this.o = (this.d - this.l.f318a) * d2;
            this.p = d2 * (this.e - this.l.b);
            this.q = this.l.c / this.f;
            this.s = this.l.d / this.g;
            this.t = this.l.e / this.h;
            this.r = this.l.f - this.i;
        }
    }

    private /* synthetic */ void c() {
        float d2;
        d dVar;
        if (this.k != null) {
            if (!this.l.g) {
                d2 = 1.0f;
                dVar = this;
            } else if (this.l.c == 0.0f) {
                d2 = 1.0f;
                dVar = this;
            } else {
                d2 = this.l.c;
                dVar = this;
            }
            dVar.a();
            float f2 = this.d;
            float f3 = this.o;
            float f4 = this.e;
            float f5 = this.p;
            float f6 = this.q;
            float f7 = this.f;
            float f8 = this.s;
            float f9 = this.g;
            float f10 = this.t;
            this.l.a(f2 - (f3 * d2), f4 - (d2 * f5), f7 * f6, f9 * f8, this.h * f10, this.i + this.r);
            this.f320a.b(this.l);
        }
    }

    public final boolean a(MotionEvent motionEvent) {
        int i2;
        d dVar;
        boolean z2;
        int i3;
        boolean z3;
        boolean z4;
        long eventTime;
        d dVar2;
        int i4;
        Object invoke;
        float[] fArr;
        Object invoke2;
        float[] fArr2;
        Object invoke3;
        float[] fArr3;
        try {
            if (v) {
                i2 = ((Integer) w.invoke(motionEvent, new Object[0])).intValue();
                dVar = this;
            } else {
                i2 = 1;
                dVar = this;
            }
            if (dVar.u == 0 && !this.j && i2 == 1) {
                return false;
            }
            int action = motionEvent.getAction();
            int historySize = motionEvent.getHistorySize() / i2;
            int i5 = 0;
            int i6 = 0;
            while (i5 <= historySize) {
                boolean z5 = i6 < historySize;
                if (!v || i2 == 1) {
                    G[0] = z5 ? motionEvent.getHistoricalX(i6) : motionEvent.getX();
                    H[0] = z5 ? motionEvent.getHistoricalY(i6) : motionEvent.getY();
                    I[0] = z5 ? motionEvent.getHistoricalPressure(i6) : motionEvent.getPressure();
                } else {
                    int min = Math.min(i2, 20);
                    int i7 = 0;
                    int i8 = 0;
                    while (i7 < min) {
                        J[i8] = ((Integer) x.invoke(motionEvent, Integer.valueOf(i8))).intValue();
                        float[] fArr4 = G;
                        if (z5) {
                            float[] fArr5 = fArr4;
                            invoke = z.invoke(motionEvent, Integer.valueOf(i8), Integer.valueOf(i6));
                            fArr = fArr5;
                        } else {
                            float[] fArr6 = fArr4;
                            invoke = C.invoke(motionEvent, Integer.valueOf(i8));
                            fArr = fArr6;
                        }
                        fArr[i8] = ((Float) invoke).floatValue();
                        float[] fArr7 = H;
                        if (z5) {
                            float[] fArr8 = fArr7;
                            invoke2 = A.invoke(motionEvent, Integer.valueOf(i8), Integer.valueOf(i6));
                            fArr2 = fArr8;
                        } else {
                            float[] fArr9 = fArr7;
                            invoke2 = D.invoke(motionEvent, Integer.valueOf(i8));
                            fArr2 = fArr9;
                        }
                        fArr2[i8] = ((Float) invoke2).floatValue();
                        float[] fArr10 = I;
                        if (z5) {
                            float[] fArr11 = fArr10;
                            invoke3 = B.invoke(motionEvent, Integer.valueOf(i8), Integer.valueOf(i6));
                            fArr3 = fArr11;
                        } else {
                            float[] fArr12 = fArr10;
                            invoke3 = y.invoke(motionEvent, Integer.valueOf(i8));
                            fArr3 = fArr12;
                        }
                        fArr3[i8] = ((Float) invoke3).floatValue();
                        i7 = i8 + 1;
                        i8 = i7;
                    }
                }
                float[] fArr13 = G;
                float[] fArr14 = H;
                float[] fArr15 = I;
                int[] iArr = J;
                if (z5) {
                    i3 = 2;
                    z2 = z5;
                } else {
                    z2 = z5;
                    i3 = action;
                }
                if (z2) {
                    boolean z6 = z5;
                    z3 = true;
                    z4 = z6;
                } else if (action == 1 || (((1 << F) - 1) & action) == E || action == 3) {
                    boolean z7 = z5;
                    z3 = false;
                    z4 = z7;
                } else {
                    boolean z8 = z5;
                    z3 = true;
                    z4 = z8;
                }
                if (z4) {
                    eventTime = motionEvent.getHistoricalEventTime(i6);
                    dVar2 = this;
                } else {
                    eventTime = motionEvent.getEventTime();
                    dVar2 = this;
                }
                c cVar = dVar2.c;
                this.c = this.b;
                this.b = cVar;
                c.a(this.b, i2, fArr13, fArr14, fArr15, iArr, i3, z3, eventTime);
                switch (this.u) {
                    case 0:
                        if (this.b.h()) {
                            this.k = this.f320a.a();
                            if (this.k != null) {
                                this.u = 1;
                                this.f320a.a(this.k);
                                b();
                                long i9 = this.b.i();
                                this.n = i9;
                                this.m = i9;
                                i4 = i6;
                                continue;
                                i5 = i4 + 1;
                                i6 = i5;
                            }
                        }
                        break;
                    case 1:
                        if (!this.b.h()) {
                            this.u = 0;
                            b bVar = this.f320a;
                            this.k = null;
                            bVar.a((Object) null);
                            i4 = i6;
                            continue;
                        } else if (this.b.a()) {
                            this.u = 2;
                            b();
                            this.m = this.b.i();
                            this.n = this.m + 20;
                            i4 = i6;
                        } else if (this.b.i() < this.n) {
                            b();
                            i4 = i6;
                        } else {
                            c();
                            i4 = i6;
                        }
                        i5 = i4 + 1;
                        i6 = i5;
                    case 2:
                        if (!this.b.a() || !this.b.h()) {
                            if (!this.b.h()) {
                                this.u = 0;
                                b bVar2 = this.f320a;
                                this.k = null;
                                bVar2.a((Object) null);
                                i4 = i6;
                                continue;
                            } else {
                                this.u = 1;
                                b();
                                this.m = this.b.i();
                                this.n = this.m + 20;
                                i4 = i6;
                            }
                            i5 = i4 + 1;
                            i6 = i5;
                        } else if (Math.abs(this.b.f() - this.c.f()) > 30.0f || Math.abs(this.b.g() - this.c.g()) > 30.0f || Math.abs(this.b.b() - this.c.b()) * 0.5f > 40.0f || Math.abs(this.b.c() - this.c.c()) * 0.5f > 40.0f) {
                            b();
                            this.m = this.b.i();
                            this.n = this.m + 20;
                            i4 = i6;
                            i5 = i4 + 1;
                            i6 = i5;
                        } else {
                            if (this.b.t < this.n) {
                                b();
                                i4 = i6;
                            } else {
                                c();
                                i4 = i6;
                            }
                            i5 = i4 + 1;
                            i6 = i5;
                        }
                        break;
                }
                i4 = i6;
                i5 = i4 + 1;
                i6 = i5;
            }
            return true;
        } catch (Exception e2) {
            Log.e("MultiTouchController", "onTouchEvent() failed", e2);
            return false;
        }
    }
}
