package org.b.a.a;

public final class a {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public float f318a;
    /* access modifiers changed from: private */
    public float b;
    /* access modifiers changed from: private */
    public float c;
    /* access modifiers changed from: private */
    public float d;
    /* access modifiers changed from: private */
    public float e;
    /* access modifiers changed from: private */
    public float f;
    /* access modifiers changed from: private */
    public boolean g;
    /* access modifiers changed from: private */
    public boolean h;
    /* access modifiers changed from: private */
    public boolean i;

    public final float a() {
        if (!this.g) {
            return 1.0f;
        }
        return this.c;
    }

    public final void a(float f2) {
        this.f318a = 0.0f;
        this.b = 0.0f;
        this.g = true;
        if (f2 == 0.0f) {
            f2 = 1.0f;
        }
        this.c = f2;
        this.h = false;
        this.d = 1.0f;
        this.e = 1.0f;
        this.i = false;
        this.f = 0.0f;
    }

    /* access modifiers changed from: protected */
    public final void a(float f2, float f3, float f4, float f5, float f6, float f7) {
        a aVar;
        a aVar2;
        a aVar3;
        float f8 = 1.0f;
        this.f318a = f2;
        this.b = f3;
        if (f4 == 0.0f) {
            aVar = this;
            f4 = 1.0f;
        } else {
            aVar = this;
        }
        aVar.c = f4;
        if (f5 == 0.0f) {
            aVar2 = this;
            f5 = 1.0f;
        } else {
            aVar2 = this;
        }
        aVar2.d = f5;
        if (f6 == 0.0f) {
            aVar3 = this;
        } else {
            aVar3 = this;
            f8 = f6;
        }
        aVar3.e = f8;
        this.f = f7;
    }
}
