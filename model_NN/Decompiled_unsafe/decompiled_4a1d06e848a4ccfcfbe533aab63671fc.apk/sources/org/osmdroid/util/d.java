package org.osmdroid.util;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public final class d {

    /* renamed from: a  reason: collision with root package name */
    private String f367a;
    private List b;
    private List c;
    private List d;
    private HashMap e;
    private boolean f;
    private int g;

    public d(File file) {
        this(file.getAbsolutePath());
    }

    private /* synthetic */ d(String str) {
        this.b = new ArrayList();
        this.c = new ArrayList();
        this.d = new ArrayList();
        this.e = new HashMap();
        this.f = false;
        this.g = 0;
        this.f367a = str;
        b();
        c();
    }

    private /* synthetic */ void b() {
        this.b.add(new RandomAccessFile(new File(this.f367a), "r"));
        int i = 0;
        while (true) {
            i++;
            File file = new File(new StringBuilder().insert(0, this.f367a).append("-").append(i).toString());
            if (file.exists()) {
                this.b.add(new RandomAccessFile(file, "r"));
            } else {
                return;
            }
        }
    }

    private /* synthetic */ void c() {
        int i = 0;
        RandomAccessFile randomAccessFile = (RandomAccessFile) this.b.get(0);
        Iterator it = this.b.iterator();
        while (it.hasNext()) {
            this.d.add(Long.valueOf(((RandomAccessFile) it.next()).length()));
        }
        int readInt = randomAccessFile.readInt();
        if (readInt != 4) {
            throw new IOException(new StringBuilder().insert(0, "Bad file version: ").append(readInt).toString());
        }
        int readInt2 = randomAccessFile.readInt();
        if (readInt2 != 256) {
            throw new IOException(new StringBuilder().insert(0, "Bad tile size: ").append(readInt2).toString());
        }
        int readInt3 = randomAccessFile.readInt();
        int i2 = 0;
        int i3 = 0;
        while (i2 < readInt3) {
            int readInt4 = randomAccessFile.readInt();
            int readInt5 = randomAccessFile.readInt();
            byte[] bArr = new byte[readInt5];
            randomAccessFile.read(bArr, 0, readInt5);
            this.e.put(new Integer(readInt4), new String(bArr));
            i2 = i3 + 1;
            i3 = i2;
        }
        int readInt6 = randomAccessFile.readInt();
        int i4 = 0;
        while (i4 < readInt6) {
            a aVar = new a(this);
            aVar.f366a = Integer.valueOf(randomAccessFile.readInt());
            aVar.b = Integer.valueOf(randomAccessFile.readInt());
            aVar.c = Integer.valueOf(randomAccessFile.readInt());
            aVar.d = Integer.valueOf(randomAccessFile.readInt());
            aVar.e = Integer.valueOf(randomAccessFile.readInt());
            aVar.f = Integer.valueOf(randomAccessFile.readInt());
            aVar.g = Long.valueOf(randomAccessFile.readLong());
            this.c.add(aVar);
            i4 = i + 1;
            i = i4;
        }
    }

    public final InputStream a(int i, int i2, int i3) {
        a aVar;
        a aVar2;
        RandomAccessFile randomAccessFile;
        long j;
        Iterator it = this.c.iterator();
        while (true) {
            if (!it.hasNext()) {
                aVar = null;
                aVar2 = null;
                break;
            }
            aVar = (a) it.next();
            if (i3 == aVar.f366a.intValue() && i >= aVar.b.intValue() && i <= aVar.c.intValue() && i2 >= aVar.d.intValue() && i2 <= aVar.e.intValue()) {
                if (!this.f || aVar.f.intValue() == this.g) {
                    aVar2 = aVar;
                }
            }
        }
        aVar2 = aVar;
        if (aVar == null) {
            return null;
        }
        try {
            long intValue = (long) ((((aVar2.e.intValue() + 1) - aVar2.d.intValue()) * (i - aVar2.b.intValue())) + (i2 - aVar2.d.intValue()));
            long longValue = aVar2.g.longValue();
            RandomAccessFile randomAccessFile2 = (RandomAccessFile) this.b.get(0);
            randomAccessFile2.seek((intValue * 12) + longValue);
            long readLong = randomAccessFile2.readLong();
            int readInt = randomAccessFile2.readInt();
            RandomAccessFile randomAccessFile3 = (RandomAccessFile) this.b.get(0);
            if (readLong > ((Long) this.d.get(0)).longValue()) {
                int size = this.d.size();
                int i4 = 0;
                int i5 = 0;
                while (i4 < size - 1 && readLong > ((Long) this.d.get(i5)).longValue()) {
                    readLong -= ((Long) this.d.get(i5)).longValue();
                    i4 = i5 + 1;
                    i5 = i4;
                }
                randomAccessFile = (RandomAccessFile) this.b.get(i5);
                j = readLong;
            } else {
                randomAccessFile = randomAccessFile3;
                j = readLong;
            }
            byte[] bArr = new byte[readInt];
            randomAccessFile.seek(j);
            int read = randomAccessFile.read(bArr, 0, readInt);
            int i6 = read;
            while (read < readInt) {
                read = randomAccessFile.read(bArr, i6, readInt) + i6;
                i6 = read;
            }
            return new ByteArrayInputStream(bArr, 0, readInt);
        } catch (IOException e2) {
            return null;
        }
    }

    public final String a() {
        return this.f367a;
    }
}
