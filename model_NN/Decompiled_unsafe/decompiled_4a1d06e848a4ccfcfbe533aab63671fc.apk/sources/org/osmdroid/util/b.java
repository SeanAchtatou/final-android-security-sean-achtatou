package org.osmdroid.util;

import android.os.Parcel;
import android.os.Parcelable;

final class b implements Parcelable.Creator {
    b() {
    }

    public final /* bridge */ /* synthetic */ Object createFromParcel(Parcel parcel) {
        return BoundingBoxE6.a(parcel);
    }

    public final /* bridge */ /* synthetic */ Object[] newArray(int i) {
        return new BoundingBoxE6[i];
    }
}
