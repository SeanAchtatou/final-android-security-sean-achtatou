package org.osmdroid.util;

public final class c {
    private /* synthetic */ c() {
    }

    public static int a(int i, int i2) {
        if (i > 0) {
            return i % i2;
        }
        int i3 = i;
        while (i < 0) {
            i3 = i + i2;
            i = i3;
        }
        return i3;
    }
}
