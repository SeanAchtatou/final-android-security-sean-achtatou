package org.osmdroid.util;

import android.os.Parcel;
import android.os.Parcelable;

final class e implements Parcelable.Creator {
    e() {
    }

    public final /* bridge */ /* synthetic */ Object createFromParcel(Parcel parcel) {
        return new GeoPoint(parcel);
    }

    public final /* bridge */ /* synthetic */ Object[] newArray(int i) {
        return new GeoPoint[i];
    }
}
