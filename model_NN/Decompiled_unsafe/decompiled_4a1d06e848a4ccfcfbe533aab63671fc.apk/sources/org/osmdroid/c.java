package org.osmdroid;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.DisplayMetrics;
import java.lang.reflect.Field;
import org.a.a;

public class c implements b {

    /* renamed from: a  reason: collision with root package name */
    private static final org.a.c f359a = a.a(c.class);
    private DisplayMetrics b;

    public c(Context context) {
        if (context != null) {
            this.b = context.getResources().getDisplayMetrics();
        }
    }

    private /* synthetic */ BitmapFactory.Options a() {
        try {
            Field declaredField = DisplayMetrics.class.getDeclaredField("DENSITY_DEFAULT");
            Field declaredField2 = BitmapFactory.Options.class.getDeclaredField("inDensity");
            Field declaredField3 = BitmapFactory.Options.class.getDeclaredField("inTargetDensity");
            Field declaredField4 = DisplayMetrics.class.getDeclaredField("densityDpi");
            BitmapFactory.Options options = new BitmapFactory.Options();
            declaredField2.setInt(options, declaredField.getInt(null));
            declaredField3.setInt(options, declaredField4.getInt(this.b));
            return options;
        } catch (IllegalAccessException | NoSuchFieldException e) {
            return null;
        }
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Type inference failed for: r2v0 */
    /* JADX WARN: Type inference failed for: r2v1, types: [java.io.InputStream] */
    /* JADX WARN: Type inference failed for: r2v2, types: [android.graphics.BitmapFactory$Options] */
    /* JADX WARN: Type inference failed for: r2v4 */
    /* JADX WARN: Type inference failed for: r2v6 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private /* synthetic */ android.graphics.Bitmap b(org.osmdroid.e r7) {
        /*
            r6 = this;
            r2 = 0
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x0059 }
            r0.<init>()     // Catch:{ all -> 0x0059 }
            r1 = 0
            java.lang.String r3 = r7.name()     // Catch:{ all -> 0x0059 }
            java.lang.StringBuilder r0 = r0.insert(r1, r3)     // Catch:{ all -> 0x0059 }
            java.lang.String r1 = ".png"
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ all -> 0x0059 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0059 }
            java.lang.Class r1 = r6.getClass()     // Catch:{ all -> 0x0059 }
            java.io.InputStream r1 = r1.getResourceAsStream(r0)     // Catch:{ all -> 0x0059 }
            if (r1 != 0) goto L_0x0045
            java.lang.IllegalArgumentException r2 = new java.lang.IllegalArgumentException     // Catch:{ all -> 0x003d }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x003d }
            r3.<init>()     // Catch:{ all -> 0x003d }
            r4 = 0
            java.lang.String r5 = "Resource not found: "
            java.lang.StringBuilder r3 = r3.insert(r4, r5)     // Catch:{ all -> 0x003d }
            java.lang.StringBuilder r0 = r3.append(r0)     // Catch:{ all -> 0x003d }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x003d }
            r2.<init>(r0)     // Catch:{ all -> 0x003d }
            throw r2     // Catch:{ all -> 0x003d }
        L_0x003d:
            r0 = move-exception
            r2 = r1
        L_0x003f:
            if (r1 == 0) goto L_0x0044
            r2.close()     // Catch:{ IOException -> 0x005e }
        L_0x0044:
            throw r0
        L_0x0045:
            android.util.DisplayMetrics r0 = r6.b     // Catch:{ all -> 0x003d }
            if (r0 == 0) goto L_0x005c
            android.graphics.BitmapFactory$Options r2 = r6.a()     // Catch:{ all -> 0x003d }
            r0 = r1
        L_0x004e:
            r3 = 0
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeStream(r0, r3, r2)     // Catch:{ all -> 0x003d }
            if (r1 == 0) goto L_0x0058
            r1.close()     // Catch:{ IOException -> 0x0060 }
        L_0x0058:
            return r0
        L_0x0059:
            r0 = move-exception
            r1 = r2
            goto L_0x003f
        L_0x005c:
            r0 = r1
            goto L_0x004e
        L_0x005e:
            r1 = move-exception
            goto L_0x0044
        L_0x0060:
            r1 = move-exception
            goto L_0x0058
        */
        throw new UnsupportedOperationException("Method not decompiled: org.osmdroid.c.b(org.osmdroid.e):android.graphics.Bitmap");
    }

    public final Drawable a(e eVar) {
        return new BitmapDrawable(b(eVar));
    }

    public final String a(a aVar, Object... objArr) {
        String str;
        switch (d.f362a[aVar.ordinal()]) {
            case 1:
                str = "Osmarender";
                break;
            case 2:
                str = "Mapnik";
                break;
            case 3:
                str = "Cycle Map";
                break;
            case 4:
                str = "Public transport";
                break;
            case 5:
                str = "OSM base layer";
                break;
            case 6:
                str = "Topographic";
                break;
            case 7:
                str = "Hills";
                break;
            case 8:
                str = "CloudMade (Standard tiles)";
                break;
            case 9:
                str = "CloudMade (small tiles)";
                break;
            case 10:
                str = "Mapquest";
                break;
            case 11:
                str = "OpenFietsKaart overlay";
                break;
            case 12:
                str = "Netherlands base overlay";
                break;
            case 13:
                str = "Netherlands roads overlay";
                break;
            case 14:
                str = "Unknown";
                break;
            case 15:
                str = "%s m";
                break;
            case 16:
                str = "%s km";
                break;
            case 17:
                str = "%s mi";
                break;
            case 18:
                str = "%s nm";
                break;
            case 19:
                str = "%s ft";
                break;
            default:
                throw new IllegalArgumentException();
        }
        return String.format(str, objArr);
    }
}
