package org.osmdroid.b.a;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.view.MotionEvent;
import java.util.List;
import org.osmdroid.b;
import org.osmdroid.b.a;
import org.osmdroid.b.f;

public class j extends e {
    private static final Paint e;
    private static final Paint f;
    private static /* synthetic */ boolean h = (!j.class.desiredAssertionStatus());

    /* renamed from: a  reason: collision with root package name */
    protected a f352a;
    protected final List b;
    private c c;
    private int d;
    private Point g;

    static {
        Paint paint = new Paint();
        e = paint;
        paint.setStyle(Paint.Style.STROKE);
        e.setColor(-65536);
        Paint paint2 = new Paint();
        f = paint2;
        paint2.setStyle(Paint.Style.STROKE);
        f.setColor(-16776961);
    }

    private /* synthetic */ j(Context context, List list, Drawable drawable, Point point, a aVar, b bVar) {
        super(bVar);
        this.d = Integer.MAX_VALUE;
        this.g = null;
        if (!h && context == null) {
            throw new AssertionError();
        } else if (h || list != null) {
            this.c = c.a(null, null, bVar);
            this.f352a = aVar;
            this.b = list;
        } else {
            throw new AssertionError();
        }
    }

    public j(Context context, List list, a aVar, b bVar) {
        this(context, list, null, null, aVar, bVar);
    }

    private /* synthetic */ Rect a(c cVar, Rect rect, Point point) {
        Drawable a2;
        c cVar2;
        Point b2;
        Drawable drawable;
        if (cVar.a(0) == null) {
            a2 = this.c.a(0);
            cVar2 = cVar;
        } else {
            a2 = cVar.a(0);
            cVar2 = cVar;
        }
        if (cVar2.b(0) == null) {
            b2 = this.c.b(0);
            drawable = a2;
        } else {
            b2 = cVar.b(0);
            drawable = a2;
        }
        int intrinsicWidth = drawable.getIntrinsicWidth();
        int intrinsicHeight = a2.getIntrinsicHeight();
        int i = point.x - b2.x;
        int i2 = point.y - b2.y;
        rect.set(i, i2, intrinsicWidth + i, intrinsicHeight + i2);
        return rect;
    }

    private /* synthetic */ boolean a(MotionEvent motionEvent, f fVar, k kVar) {
        a e2 = fVar.e();
        Point point = new Point();
        e2.a((int) motionEvent.getX(), (int) motionEvent.getY(), point);
        this.g = point;
        Rect rect = new Rect();
        Point point2 = new Point();
        int i = 0;
        int i2 = 0;
        while (i < this.b.size()) {
            c cVar = (c) this.b.get(i2);
            e2.a(cVar.f346a, point2);
            a(cVar, rect, point2);
            if (rect.contains(point.x, point.y) && kVar.a(i2)) {
                return true;
            }
            i = i2 + 1;
            i2 = i;
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public void a(Canvas canvas) {
    }

    /* access modifiers changed from: protected */
    public void a(Canvas canvas, int i, Point point) {
        c cVar = (c) this.b.get(i);
        Drawable a2 = cVar.a(0) == null ? this.c.a(0) : cVar.a(0);
        Rect rect = new Rect();
        a(cVar, rect, point);
        a2.setBounds(rect);
        a2.draw(canvas);
    }

    public final void a(Canvas canvas, f fVar) {
        a e2 = fVar.e();
        Point point = new Point();
        int size = this.b.size() - 1;
        if (size > this.d) {
            size = this.d;
        }
        int i = size;
        while (size >= 0) {
            e2.a(((c) this.b.get(i)).f346a, point);
            a(canvas, i, point);
            size = i - 1;
            i = size;
        }
    }

    /* access modifiers changed from: protected */
    public boolean a(int i, c cVar) {
        return this.f352a.a(cVar);
    }

    public final boolean a(MotionEvent motionEvent, f fVar) {
        if (a(motionEvent, fVar, new h(this))) {
            return true;
        }
        return super.a(motionEvent, fVar);
    }

    public final boolean b(MotionEvent motionEvent, f fVar) {
        if (a(motionEvent, fVar, new g(this))) {
            return true;
        }
        return super.b(motionEvent, fVar);
    }
}
