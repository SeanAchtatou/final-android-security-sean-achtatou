package org.osmdroid.b.a;

import android.graphics.Point;
import android.graphics.drawable.Drawable;
import org.osmdroid.b;
import org.osmdroid.e;
import org.osmdroid.util.GeoPoint;

public class c {
    private static Point c = new Point(26, 94);

    /* renamed from: a  reason: collision with root package name */
    public final GeoPoint f346a;
    protected Drawable b;
    private long d;
    private String e;
    private String f;
    private Point g;
    private b h;

    private /* synthetic */ c(String str, String str2, GeoPoint geoPoint) {
        this.e = str;
        this.f = str2;
        this.f346a = geoPoint;
        this.d = -1;
    }

    public c(String str, String str2, GeoPoint geoPoint, byte b2) {
        this(str, str2, geoPoint);
    }

    private /* synthetic */ Point a() {
        if (this.h == null) {
            this.h = b.CUSTOM;
        }
        Point point = this.b == null ? c : new Point(this.b.getIntrinsicWidth(), this.b.getIntrinsicWidth());
        switch (d.f347a[this.h.ordinal()]) {
            case 1:
                if (this.g == null) {
                    this.h = b.BOTTOM_CENTER;
                    this.g = new Point(point.x / 2, point.y / 2);
                    break;
                }
                break;
            case 2:
                this.g = new Point(point.x / 2, point.y / 2);
                break;
            case 3:
                this.g = new Point(point.x / 2, point.y);
                break;
            case 4:
                this.g = new Point(point.x / 2, 0);
                break;
            case 5:
                this.g = new Point(point.x, point.y / 2);
                break;
            case 6:
                this.g = new Point(0, point.y / 2);
                break;
            case 7:
                this.g = new Point(point.x, 0);
                break;
            case 8:
                this.g = new Point(point.x, point.y);
                break;
            case 9:
                this.g = new Point(0, 0);
                break;
            case 10:
                this.g = new Point(0, point.y);
                break;
        }
        return this.g;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.osmdroid.util.GeoPoint.<init>(double, double):void
     arg types: [int, int]
     candidates:
      org.osmdroid.util.GeoPoint.<init>(int, int):void
      org.osmdroid.util.GeoPoint.<init>(android.os.Parcel, byte):void
      org.osmdroid.util.GeoPoint.<init>(double, double):void */
    public static c a(Drawable drawable, Point point, b bVar) {
        c cVar;
        c cVar2 = new c("<default>", "used when no marker is specified", new GeoPoint(0.0d, 0.0d), (byte) 0);
        if (drawable != null) {
            cVar = cVar2;
        } else {
            drawable = bVar.a(e.marker_default);
            cVar = cVar2;
        }
        cVar.b = drawable;
        if (point == null) {
            cVar2.a();
        } else {
            cVar2.g = point;
            cVar2.h = b.CUSTOM;
        }
        return cVar2;
    }

    public final Point a(b bVar) {
        if (bVar == null) {
            bVar = b.BOTTOM_CENTER;
        }
        this.h = bVar;
        a();
        return this.g;
    }

    public Drawable a(int i) {
        if (this.b == null) {
            return null;
        }
        this.b.setState(new int[]{(-i) & 1, i & 2, i & 4});
        return this.b;
    }

    public final void a(Drawable drawable) {
        this.b = drawable;
        a();
    }

    public Point b(int i) {
        return this.g == null ? a() : this.g;
    }
}
