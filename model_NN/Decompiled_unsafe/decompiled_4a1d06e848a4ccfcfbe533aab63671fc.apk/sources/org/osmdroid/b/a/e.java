package org.osmdroid.b.a;

import android.graphics.Canvas;
import android.view.MotionEvent;
import org.osmdroid.b;
import org.osmdroid.b.f;

public abstract class e {

    /* renamed from: a  reason: collision with root package name */
    private b f348a;
    private boolean b = true;

    public e(b bVar) {
        this.f348a = bVar;
    }

    /* access modifiers changed from: protected */
    public abstract void a(Canvas canvas);

    /* access modifiers changed from: protected */
    public abstract void a(Canvas canvas, f fVar);

    public final void a(boolean z) {
        this.b = z;
    }

    public final boolean a() {
        return this.b;
    }

    public boolean a(MotionEvent motionEvent, f fVar) {
        return false;
    }

    public void b() {
    }

    public final void b(Canvas canvas, f fVar) {
        if (this.b) {
            a(canvas, fVar);
            a(canvas);
        }
    }

    public boolean b(MotionEvent motionEvent, f fVar) {
        return false;
    }
}
