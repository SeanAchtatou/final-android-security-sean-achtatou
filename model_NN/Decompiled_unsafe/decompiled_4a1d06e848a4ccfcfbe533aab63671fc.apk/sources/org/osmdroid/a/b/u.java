package org.osmdroid.a.b;

import android.graphics.drawable.Drawable;
import java.io.File;
import org.osmdroid.a.d;
import org.osmdroid.a.d.a;
import org.osmdroid.a.f;

final class u extends i {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ t f336a;

    /* synthetic */ u(t tVar) {
        this(tVar, (byte) 0);
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    private /* synthetic */ u(t tVar, byte b) {
        super(tVar);
        this.f336a = tVar;
    }

    public final Drawable a(d dVar) {
        boolean z = false;
        if (this.f336a.f == null) {
            return null;
        }
        f a2 = dVar.a();
        if (!this.f336a.a()) {
            return null;
        }
        File file = new File(a.b, new StringBuilder().insert(0, this.f336a.f.b(a2)).append(".tile").toString());
        if (!file.exists()) {
            return null;
        }
        if (file.lastModified() < System.currentTimeMillis() - this.f336a.e) {
            z = true;
        }
        if (!z) {
            return this.f336a.f.b(file.getPath());
        }
        dVar.b().b(dVar, this.f336a.f.b(file.getPath()));
        return null;
    }
}
