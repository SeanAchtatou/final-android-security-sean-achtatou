package org.osmdroid.a.b;

import android.graphics.drawable.Drawable;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.LinkedHashMap;
import org.osmdroid.a.d;
import org.osmdroid.a.f;

public abstract class i implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ v f330a;

    protected i(v vVar) {
        this.f330a = vVar;
    }

    private /* synthetic */ d a() {
        LinkedHashMap linkedHashMap;
        d dVar = null;
        LinkedHashMap linkedHashMap2 = this.f330a.d;
        synchronized (linkedHashMap2) {
            Iterator it = this.f330a.d.keySet().iterator();
            f fVar = null;
            loop0:
            while (true) {
                Iterator it2 = it;
                while (it.hasNext()) {
                    try {
                        f fVar2 = (f) it2.next();
                        if (!this.f330a.g.containsKey(fVar2)) {
                            fVar = fVar2;
                            it = it2;
                        } else {
                            it = it2;
                        }
                    } catch (ConcurrentModificationException e) {
                        if (fVar != null) {
                            break loop0;
                        }
                        it = this.f330a.d.keySet().iterator();
                        it2 = it;
                    }
                }
                break loop0;
            }
            if (fVar != null) {
                this.f330a.g.put(fVar, this.f330a.d.get(fVar));
            }
            if (fVar != null) {
                dVar = (d) this.f330a.d.get(fVar);
                linkedHashMap = linkedHashMap2;
            } else {
                linkedHashMap = linkedHashMap2;
            }
            return dVar;
        }
    }

    /* access modifiers changed from: protected */
    public abstract Drawable a(d dVar);

    public final void run() {
        Drawable drawable;
        while (true) {
            d a2 = a();
            if (a2 != null) {
                Drawable drawable2 = null;
                try {
                    drawable2 = a(a2);
                    drawable = drawable2;
                } catch (p e) {
                    v.c.a("Tile loader can't continue", e);
                    this.f330a.a();
                    drawable = null;
                } catch (Throwable th) {
                    v.c.c(new StringBuilder().insert(0, "Error downloading tile: ").append(a2).toString(), th);
                    drawable = null;
                }
                if (drawable2 != null) {
                    v.a(this.f330a, a2.a());
                    a2.b().a(a2, drawable);
                } else {
                    v.a(this.f330a, a2.a());
                    a2.b().a(a2);
                }
            } else {
                return;
            }
        }
    }
}
