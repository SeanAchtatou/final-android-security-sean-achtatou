package org.osmdroid.a.b;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import org.a.a;
import org.a.c;
import org.osmdroid.a.a.f;

public class e implements m {

    /* renamed from: a  reason: collision with root package name */
    private static final c f327a = a.a(e.class);
    private final SQLiteDatabase b;

    private /* synthetic */ e(SQLiteDatabase sQLiteDatabase) {
        this.b = sQLiteDatabase;
    }

    public static e a(File file) {
        return new e(SQLiteDatabase.openOrCreateDatabase(file, (SQLiteDatabase.CursorFactory) null));
    }

    public final InputStream a(f fVar, org.osmdroid.a.f fVar2) {
        ByteArrayInputStream byteArrayInputStream;
        Cursor cursor;
        try {
            long b2 = (long) fVar2.b();
            long c = (long) fVar2.c();
            long a2 = (long) fVar2.a();
            Cursor query = this.b.query("tiles", new String[]{"tile"}, new StringBuilder().insert(0, "key = ").append(((b2 + (a2 << ((int) a2))) << ((int) a2)) + c).append(" and provider = '").append(fVar.c()).append("'").toString(), null, null, null, null);
            if (query.getCount() != 0) {
                query.moveToFirst();
                Cursor cursor2 = query;
                byteArrayInputStream = new ByteArrayInputStream(query.getBlob(0));
                cursor = cursor2;
            } else {
                Cursor cursor3 = query;
                byteArrayInputStream = null;
                cursor = cursor3;
            }
            cursor.close();
            if (byteArrayInputStream != null) {
                return byteArrayInputStream;
            }
            return null;
        } catch (Throwable th) {
            f327a.b(new StringBuilder().insert(0, "Error getting db stream: ").append(fVar2).toString(), th);
        }
    }

    public String toString() {
        return new StringBuilder().insert(0, "DatabaseFileArchive [mDatabase=").append(this.b.getPath()).append("]").toString();
    }
}
