package org.osmdroid.a.b;

import java.util.LinkedHashMap;
import java.util.concurrent.ConcurrentHashMap;
import org.a.c;
import org.osmdroid.a.d;
import org.osmdroid.a.d.a;
import org.osmdroid.a.f;

public abstract class v implements a {
    /* access modifiers changed from: private */
    public static final c c = org.a.a.a(v.class);
    final LinkedHashMap d;
    private final int e;
    private final ThreadGroup f = new ThreadGroup(f());
    /* access modifiers changed from: private */
    public final ConcurrentHashMap g;

    public v(int i) {
        this.e = i;
        this.g = new ConcurrentHashMap();
        this.d = new o(this, 42, 40);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a() {
        synchronized (this.d) {
            this.d.clear();
        }
        this.g.clear();
    }

    static /* synthetic */ void a(v vVar, f fVar) {
        synchronized (vVar.d) {
            vVar.d.remove(fVar);
        }
        vVar.g.remove(fVar);
    }

    public final void a(d dVar) {
        int activeCount = this.f.activeCount();
        synchronized (this.d) {
            this.d.put(dVar.a(), dVar);
        }
        if (activeCount < this.e) {
            new Thread(this.f, g()).start();
        }
    }

    public void b() {
        a();
        this.f.interrupt();
    }

    public abstract boolean e();

    /* access modifiers changed from: protected */
    public abstract String f();

    /* access modifiers changed from: protected */
    public abstract Runnable g();

    public abstract int h();

    public abstract int i();
}
