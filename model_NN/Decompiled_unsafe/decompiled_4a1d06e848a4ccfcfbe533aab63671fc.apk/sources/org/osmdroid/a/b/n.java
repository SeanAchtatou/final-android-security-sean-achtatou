package org.osmdroid.a.b;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

final class n extends BroadcastReceiver {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ h f332a;

    /* synthetic */ n(h hVar) {
        this(hVar, (byte) 0);
    }

    private /* synthetic */ n(h hVar, byte b) {
        this.f332a = hVar;
    }

    public final void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        this.f332a.k();
        if ("android.intent.action.MEDIA_MOUNTED".equals(action)) {
            this.f332a.c();
        } else if ("android.intent.action.MEDIA_UNMOUNTED".equals(action)) {
            this.f332a.d();
        }
    }
}
