package org.osmdroid.a.c;

import android.os.Handler;
import android.os.Message;
import android.view.View;

public final class d extends Handler {

    /* renamed from: a  reason: collision with root package name */
    private final View f340a;

    public d(View view) {
        this.f340a = view;
    }

    public final void handleMessage(Message message) {
        switch (message.what) {
            case 0:
                this.f340a.invalidate();
                return;
            default:
                return;
        }
    }
}
