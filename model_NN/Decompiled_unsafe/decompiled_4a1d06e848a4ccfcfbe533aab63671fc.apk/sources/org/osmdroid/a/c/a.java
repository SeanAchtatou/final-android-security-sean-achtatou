package org.osmdroid.a.c;

import android.content.SharedPreferences;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import org.a.c;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;

public class a implements org.osmdroid.a.d.a {
    private static final c c = org.a.a.a(a.class);
    private static String d = "android_id";
    private static String e = "";
    private static String f = "";
    private static SharedPreferences.Editor g;

    public static String a() {
        return e;
    }

    public static String b() {
        String str;
        if (f.length() == 0) {
            String str2 = f;
            synchronized (str2) {
                if (f.length() == 0) {
                    try {
                        HttpResponse execute = new DefaultHttpClient().execute(new HttpPost(new StringBuilder().insert(0, "http://auth.cloudmade.com/token/").append(e).append("?userid=").append(d).toString()));
                        if (execute.getStatusLine().getStatusCode() == 200) {
                            String trim = new BufferedReader(new InputStreamReader(execute.getEntity().getContent()), 8192).readLine().trim();
                            f = trim;
                            if (trim.length() > 0) {
                                g.putString("CLOUDMADE_TOKEN", f);
                                g.commit();
                                g = null;
                            } else {
                                c.d("No authorization token received from Cloudmade");
                                str = str2;
                            }
                        }
                    } catch (IOException e2) {
                        c.d(new StringBuilder().insert(0, "No authorization token received from Cloudmade: ").append(e2).toString());
                        str = str2;
                    }
                }
                str = str2;
            }
        }
        return f;
    }
}
