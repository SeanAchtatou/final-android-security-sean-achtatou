package org.osmdroid.a;

import android.graphics.drawable.Drawable;
import android.os.Handler;
import org.a.c;
import org.osmdroid.a.d.a;

public abstract class i implements a, g {
    private static final c c = org.a.a.a(i.class);
    protected final h d;
    private Handler e;
    private boolean f;

    private /* synthetic */ i() {
        this.f = true;
        this.d = new h((byte) 0);
        this.e = null;
    }

    public i(byte b) {
        this();
    }

    public abstract Drawable a(f fVar);

    public abstract void a();

    public final void a(int i) {
        this.d.a(i);
    }

    public final void a(Handler handler) {
        this.e = handler;
    }

    public void a(d dVar) {
        if (this.e != null) {
            this.e.sendEmptyMessage(1);
        }
    }

    public void a(d dVar, Drawable drawable) {
        f a2 = dVar.a();
        if (drawable != null) {
            this.d.a(a2, drawable);
        }
        if (this.e != null) {
            this.e.sendEmptyMessage(0);
        }
    }

    public abstract int b();

    public final void b(d dVar, Drawable drawable) {
        a(dVar, drawable);
    }

    public abstract int c();

    public final boolean d() {
        return this.f;
    }
}
