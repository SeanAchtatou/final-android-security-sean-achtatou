package com.soft.android.appinstaller;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class FinishActivity extends Activity {
    private Button buttonExit;
    private Button buttonOpen;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.finish);
        GlobalConfig.getInstance().init(this);
        OpInfo.getInstance().init(this);
        this.buttonExit = (Button) findViewById(R.id.finishExitButton);
        this.buttonOpen = (Button) findViewById(R.id.finishNextButton);
        this.buttonExit.setText(OpInfo.getInstance().getInternals().getOverridableValueForLocation("ui.finishscreen.buttons.exit.caption", this.buttonExit.getText().toString()));
        this.buttonOpen.setText(OpInfo.getInstance().getInternals().getOverridableValueForLocation("ui.finishscreen.buttons.open.caption", this.buttonOpen.getText().toString()));
        TextView textView = (TextView) findViewById(R.id.finishTextView);
        ActivityTexts texts = OpInfo.getInstance().getTextFinder().getFinishTexts();
        if (texts != null) {
            textView.setText(texts.getText());
            textView.setMovementMethod(new ScrollingMovementMethod());
            setTitle(texts.getTitle());
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == 4) {
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    public void onNextClicked(View v) {
        Log.v("log", "Next");
        startActivity(new Intent("android.intent.action.VIEW", Uri.parse(GlobalConfig.getInstance().getValue("url"))));
    }

    public void onExitClicked(View v) {
        Log.v("log", "Exit");
        finish();
    }
}
