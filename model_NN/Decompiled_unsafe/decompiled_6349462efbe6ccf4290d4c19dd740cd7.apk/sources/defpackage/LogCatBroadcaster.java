package defpackage;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/* renamed from: LogCatBroadcaster  reason: default package */
public class LogCatBroadcaster implements Runnable {
    private static boolean started = false;
    private Context context;

    private LogCatBroadcaster(Context context2) {
        this.context = context2;
    }

    public static synchronized void start(Context context2) {
        boolean z = true;
        synchronized (LogCatBroadcaster.class) {
            if (!started) {
                started = true;
                if (Build.VERSION.SDK_INT >= 16) {
                    if ((context2.getApplicationInfo().flags & 2) == 0) {
                        z = false;
                    }
                    if (z) {
                        try {
                            context2.getPackageManager().getPackageInfo("com.aide.ui", 128);
                            new Thread(new LogCatBroadcaster(context2)).start();
                        } catch (PackageManager.NameNotFoundException e) {
                        }
                    }
                }
            }
        }
    }

    public void run() {
        try {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(Runtime.getRuntime().exec("logcat -v threadtime").getInputStream()), 20);
            while (true) {
                String readLine = bufferedReader.readLine();
                if (readLine != null) {
                    Intent intent = new Intent();
                    intent.setPackage("com.aide.ui");
                    intent.setAction("com.aide.runtime.VIEW_LOGCAT_ENTRY");
                    intent.putExtra("lines", new String[]{readLine});
                    this.context.sendBroadcast(intent);
                } else {
                    return;
                }
            }
        } catch (IOException e) {
        }
    }
}
