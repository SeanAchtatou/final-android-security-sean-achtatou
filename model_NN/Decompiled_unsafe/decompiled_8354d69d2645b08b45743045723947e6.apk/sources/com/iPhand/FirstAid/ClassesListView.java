package com.iPhand.FirstAid;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import java.util.ArrayList;
import java.util.HashMap;

public class ClassesListView extends Activity {
    int[] Icons = {R.drawable.icon_01, R.drawable.icon_02, R.drawable.icon_03, R.drawable.icon_04, R.drawable.icon_05, R.drawable.icon_06, R.drawable.icon_07, R.drawable.icon_08, R.drawable.icon_09, R.drawable.icon_10};
    int Num = 0;
    String Title = null;
    private SQLiteDatabase database;
    ListView mListView;
    String[] mQ = {j.c("\r9\u0003;\u00018"), DBUtil.c("\u001c0\u00110\u0001&\u0013'\u000b>\u001c:\u0005=\u001d\""), j.c("9\u001b\"\n9\u0001$"), DBUtil.c("\u001b;\u00060\u0000;\u00139"), j.c("\u000b.\u001a3\u001c8\u000f:"), DBUtil.c("\u00144\u00110"), j.c("\t/\u00007\u000b5\u0001:\u00011\u00075\u000f:"), DBUtil.c("%\u00130\u0016<\u0013!\u0000<\u0011&"), j.c("\u001d=\u00078"), DBUtil.c("%\u0001,\u0011=\u001d9\u001d2\u001b6\u00139")};

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.dialogueclass_list);
        this.database = DBUtil.openDatabase(this);
        this.mListView = (ListView) findViewById(R.id.DialogueClassList);
        this.Num = getIntent().getExtras().getInt(j.c("\u0018\u001b;"));
        this.Title = getIntent().getExtras().getString(DBUtil.c("\u0001\u001b!\u001e0"));
        setTitle(this.Title);
        setListAdapter();
        this.mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
                Intent intent = new Intent(ClassesListView.this, DetailView.class);
                Bundle bundle = new Bundle();
                bundle.putInt(i.c("93\u001a"), ClassesListView.this.getIntent().getExtras().getInt(l.c("\u001b=8")));
                bundle.putInt(i.c(">2\u0012+93\u001a"), i);
                intent.putExtras(bundle);
                ClassesListView.this.startActivity(intent);
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.database.close();
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onRestart() {
        this.database = DBUtil.openDatabase(this);
        super.onRestart();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        this.database.close();
        super.onStop();
    }

    public void setListAdapter() {
        int i = 0;
        Cursor rawQuery = this.database.rawQuery(j.c("\u001d3\u00023\r\"N\"\u0001&\u00075N0\u001c9\u0003v\b?\u001c%\u001a7\u00072N!\u00063\u001c3N5\u000f\"\u000b1\u0001$\u0017vSi"), new String[]{this.mQ[this.Num]});
        if (rawQuery.getCount() > 0) {
            rawQuery.moveToFirst();
        }
        try {
            ArrayList arrayList = new ArrayList();
            while (true) {
                int i2 = i;
                if (i >= rawQuery.getCount()) {
                    this.mListView.setAdapter((ListAdapter) new SimpleAdapter(this, arrayList, R.layout.dialogue_details_item, new String[]{DBUtil.c("\u001c\u001f4\u00150"), j.c("\u001f\u001a3\u0003\u0002\u0007\"\u00023")}, new int[]{R.id.DialogueclassImage, R.id.DialogueclassTitle}));
                    rawQuery.close();
                    return;
                }
                HashMap hashMap = new HashMap();
                hashMap.put(DBUtil.c("\u001c\u001f4\u00150"), Integer.valueOf(this.Icons[this.Num]));
                hashMap.put(j.c("\u001f\u001a3\u0003\u0002\u0007\"\u00023"), rawQuery.getString(rawQuery.getColumnIndex(DBUtil.c("!\u001d%\u001b6"))));
                arrayList.add(hashMap);
                rawQuery.moveToNext();
                i = i2 + 1;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
