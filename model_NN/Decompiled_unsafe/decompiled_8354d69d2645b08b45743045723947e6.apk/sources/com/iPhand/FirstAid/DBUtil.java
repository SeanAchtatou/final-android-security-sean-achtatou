package com.iPhand.FirstAid;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;

public class DBUtil {
    private static final String DATABASE_FILENAME = "firstaid.sqlite";
    private static final String DATABASE_PATH = "/data/data/com.iPhand.FirstAid/databases";

    public static String c(String str) {
        int length = str.length();
        char[] cArr = new char[length];
        int i = length - 1;
        int i2 = i;
        while (i >= 0) {
            int i3 = i2 - 1;
            cArr[i2] = (char) (str.charAt(i2) ^ 'U');
            if (i3 < 0) {
                break;
            }
            i = i3 - 1;
            cArr[i3] = (char) (str.charAt(i3) ^ 'r');
            i2 = i;
        }
        return new String(cArr);
    }

    public static SQLiteDatabase openDatabase(Context context) {
        try {
            File file = new File(DATABASE_PATH);
            File file2 = new File(DATABASE_PATH);
            if (!file2.exists()) {
                file2.mkdir();
            }
            if (!file.exists()) {
                file.mkdir();
            }
            if (!new File(j.c("A2\u000f\"\u000fy\n7\u001a7A5\u0001;@?>>\u000f8\nx(?\u001c%\u001a\u0017\u00072A2\u000f\"\u000f4\u000f%\u000b%A0\u0007$\u001d\"\u000f?\nx\u001d'\u0002?\u001a3")).exists()) {
                InputStream openRawResource = context.getResources().openRawResource(R.raw.firstaid);
                FileOutputStream fileOutputStream = new FileOutputStream(R.c("O$\u00014\u0001o\u0004!\u0014!O#\u000f-N)0(\u0001.\u0004n&)\u00123\u0014\u0001\t$O$\u00014\u0001\"\u00013\u00053O&\t2\u00134\u0001)\u0004n\u00131\f)\u0014%"));
                byte[] bArr = new byte[8192];
                while (true) {
                    int read = openRawResource.read(bArr);
                    if (read <= 0) {
                        break;
                    }
                    fileOutputStream.write(bArr, 0, read);
                }
                fileOutputStream.close();
                openRawResource.close();
            }
            return SQLiteDatabase.openOrCreateDatabase(j.c("A2\u000f\"\u000fy\n7\u001a7A5\u0001;@?>>\u000f8\nx(?\u001c%\u001a\u0017\u00072A2\u000f\"\u000f4\u000f%\u000b%A0\u0007$\u001d\"\u000f?\nx\u001d'\u0002?\u001a3"), (SQLiteDatabase.CursorFactory) null);
        } catch (Exception e) {
            return null;
        }
    }
}
