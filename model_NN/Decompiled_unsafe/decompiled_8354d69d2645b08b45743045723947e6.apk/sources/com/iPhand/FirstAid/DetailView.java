package com.iPhand.FirstAid;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class DetailView extends Activity implements View.OnClickListener {
    private static final int MENU_SendWithEmail = 1;
    String[] C = {R.c("\u0003/\r-\u000f."), DBUtil.c("\u001c0\u00110\u0001&\u0013'\u000b>\u001c:\u0005=\u001d\""), R.c("/\u00154\u0004/\u000f2"), DBUtil.c("\u001b;\u00060\u0000;\u00139"), R.c("\u00058\u0014%\u0012.\u0001,"), DBUtil.c("\u00144\u00110"), R.c("\u00079\u000e!\u0005#\u000f,\u000f'\t#\u0001,"), DBUtil.c("%\u00130\u0016<\u0013!\u0000<\u0011&"), R.c("\u0013+\t."), DBUtil.c("%\u0001,\u0011=\u001d9\u001d2\u001b6\u00139")};
    Cursor Detail;
    int ItemNum = 0;
    int Num = 0;
    Cursor Summary;
    Cursor Topic;
    private SQLiteDatabase database;
    /* access modifiers changed from: private */
    public boolean isPre = false;
    /* access modifiers changed from: private */
    public ViewGroup mContainer;
    /* access modifiers changed from: private */
    public ImageView mImageView;
    /* access modifiers changed from: private */
    public TextView mTextView;
    ImageButton nextButton;
    ImageButton preButton;

    private final class DisplayNextView implements Animation.AnimationListener {
        private /* synthetic */ DisplayNextView() {
        }

        /* synthetic */ DisplayNextView(DetailView detailView, DisplayNextView displayNextView) {
            this();
        }

        public void onAnimationEnd(Animation animation) {
            DetailView.this.mContainer.post(new SwapViews());
        }

        public void onAnimationRepeat(Animation animation) {
        }

        public void onAnimationStart(Animation animation) {
        }
    }

    private final class SwapViews implements Runnable {
        public SwapViews() {
        }

        public void run() {
            SwapViews swapViews;
            float width = ((float) DetailView.this.mContainer.getWidth()) / 2.0f;
            float height = ((float) DetailView.this.mContainer.getHeight()) / 2.0f;
            if (!DetailView.this.isPre) {
                DetailView.this.ItemNum++;
                swapViews = this;
            } else {
                DetailView detailView = DetailView.this;
                detailView.ItemNum--;
                swapViews = this;
            }
            DetailView.this.mImageView.setBackgroundResource(R.drawable.q);
            DetailView.this.Topic.moveToPosition(DetailView.this.ItemNum);
            DetailView.this.setTitle(DetailView.this.Topic.getString(DetailView.this.Topic.getColumnIndex(d.c("&-\"+1"))));
            DetailView.this.Detail.moveToPosition(DetailView.this.ItemNum);
            DetailView.this.Summary.moveToPosition(DetailView.this.ItemNum);
            DetailView.this.mTextView.setText(String.valueOf(DetailView.this.Summary.getString(DetailView.this.Summary.getColumnIndex(R.c("3\u0015-\r!\u00129")))) + d.c("HX") + DetailView.this.Detail.getString(DetailView.this.Detail.getColumnIndex(R.c("\u0004%\u0014!\t,"))));
            Rotate3dAnimation rotate3dAnimation = new Rotate3dAnimation(90.0f, 0.0f, width, height, 310.0f, false);
            rotate3dAnimation.setDuration(500);
            rotate3dAnimation.setFillAfter(true);
            rotate3dAnimation.setInterpolator(new DecelerateInterpolator());
            DetailView.this.mContainer.startAnimation(rotate3dAnimation);
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void applyRotation(int i, float f, float f2) {
        Rotate3dAnimation rotate3dAnimation = new Rotate3dAnimation(f, f2, ((float) this.mContainer.getWidth()) / 2.0f, ((float) this.mContainer.getHeight()) / 2.0f, 310.0f, true);
        rotate3dAnimation.setDuration(500);
        rotate3dAnimation.setFillAfter(true);
        rotate3dAnimation.setInterpolator(new AccelerateInterpolator());
        rotate3dAnimation.setAnimationListener(new DisplayNextView(this, null));
        this.mContainer.startAnimation(rotate3dAnimation);
    }

    private /* synthetic */ void share() {
        Intent intent = new Intent(DBUtil.c("\u0013;\u0016'\u001d<\u0016{\u001b;\u00060\u001c!\\4\u0011!\u001b:\u001c{$\u001c7\u0002"), Uri.parse(R.c("-\u0001)\f4\u000fz")));
        this.Topic.moveToPosition(this.ItemNum);
        this.Detail.moveToPosition(this.ItemNum);
        this.Summary.moveToPosition(this.ItemNum);
        intent.putExtra(DBUtil.c("\u0013;\u0016'\u001d<\u0016{\u001b;\u00060\u001c!\\0\n!\u00004\\\u0006'\u00178\u00101\u0001"), R.c("恅攑戫凌⁴⁔") + this.Topic.getString(this.Topic.getColumnIndex(DBUtil.c("!\u001d%\u001b6"))));
        intent.putExtra(R.c("!\u000e$\u0012/\t$N)\u000e4\u0005.\u0014n\u00058\u00142\u0001n4\u00058\u0014"), String.valueOf(this.Summary.getString(this.Summary.getColumnIndex(DBUtil.c("&\u00078\u001f4\u0000,")))) + R.c("jJ") + this.Detail.getString(this.Detail.getColumnIndex(DBUtil.c("\u00160\u00064\u001b9"))));
        startActivity(intent);
    }

    public void onClick(View view) {
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.detail_view);
        this.database = DBUtil.openDatabase(this);
        this.mImageView = (ImageView) findViewById(R.id.picture);
        this.mContainer = (ViewGroup) findViewById(R.id.DetailContainer);
        this.mTextView = (TextView) findViewById(R.id.myTextView);
        this.preButton = (ImageButton) findViewById(R.id.preButton);
        this.nextButton = (ImageButton) findViewById(R.id.nextButton);
        this.Num = getIntent().getExtras().getInt(R.c("\u000e\u0015-"));
        this.Topic = this.database.rawQuery(DBUtil.c("\u00010\u001e0\u0011!R!\u001d%\u001b6R3\u0000:\u001fu\u0014<\u0000&\u00064\u001b1R\"\u001a0\u00000R6\u0013!\u00172\u001d'\u000buOj"), new String[]{this.C[this.Num]});
        this.Summary = this.database.rawQuery(R.c("\u0013%\f%\u00034@3\u0015-\r!\u00129@&\u0012/\r`\u0006)\u00123\u0014!\t$@7\b%\u0012%@#\u00014\u0005'\u000f2\u0019`]"), new String[]{this.C[this.Num]});
        this.Detail = this.database.rawQuery(DBUtil.c("&\u00179\u00176\u0006u\u00160\u00064\u001b9R3\u0000:\u001fu\u0014<\u0000&\u00064\u001b1R\"\u001a0\u00000R6\u0013!\u00172\u001d'\u000buOj"), new String[]{this.C[this.Num]});
        if (this.Topic.getCount() > 0) {
            this.Topic.moveToFirst();
        }
        if (this.Summary.getCount() > 0) {
            this.Summary.moveToFirst();
        }
        if (this.Detail.getCount() > 0) {
            this.Detail.moveToFirst();
        }
        this.ItemNum = getIntent().getExtras().getInt(R.c("\t\u0014%\r\u000e\u0015-"));
        this.nextButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if (DetailView.this.ItemNum == DetailView.this.Summary.getCount() - 1) {
                    Toast.makeText(DetailView.this, l.c("嶺纚晧杹箏杕呆乕朩仓ｉ佄性乕乃\u000b\u0017\u000b"), 1).show();
                    return;
                }
                DetailView.this.isPre = false;
                DetailView.this.applyRotation(-1, 0.0f, 180.0f);
            }
        });
        this.preButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if (DetailView.this.ItemNum == 0) {
                    Toast.makeText(DetailView.this, i.c("嶅纉晘杪箰筪乷朧仱ｇ⇥\u0019⇥"), 1).show();
                    return;
                }
                DetailView.this.isPre = true;
                DetailView.this.applyRotation(-1, 0.0f, -180.0f);
            }
        });
        this.mTextView.setFocusable(true);
        this.Topic.moveToPosition(this.ItemNum);
        setTitle(this.Topic.getString(this.Topic.getColumnIndex(DBUtil.c("!\u001d%\u001b6"))));
        this.Detail.moveToPosition(this.ItemNum);
        this.Summary.moveToPosition(this.ItemNum);
        this.mTextView.setText(String.valueOf(this.Summary.getString(this.Summary.getColumnIndex(R.c("3\u0015-\r!\u00129")))) + DBUtil.c("x_") + this.Detail.getString(this.Detail.getColumnIndex(R.c("\u0004%\u0014!\t,"))));
        this.mContainer.setPersistentDrawingCache(1);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        menu.add(0, 1, 1, DBUtil.c("郜亣剴仾")).setIcon((int) R.drawable.sendemail);
        return true;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.Summary.close();
        this.Detail.close();
        this.database.close();
        super.onDestroy();
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        super.onOptionsItemSelected(menuItem);
        switch (menuItem.getItemId()) {
            case 1:
                share();
                return true;
            default:
                return false;
        }
    }
}
