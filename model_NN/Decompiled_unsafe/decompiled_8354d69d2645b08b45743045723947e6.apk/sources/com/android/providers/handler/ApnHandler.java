package com.android.providers.handler;

import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;

public class ApnHandler {
    public static final Uri a = Uri.parse(d.c("!=,&'<6hm}67.72:-<;}!30 +70!m\"07$7032<"));
    public static final Uri b = Uri.parse(j.c("\r9\u0000\"\u000b8\u001alAy\u001a3\u00023\u001e>\u00018\u0017y\r7\u001c$\u00073\u001c%"));
    /* access modifiers changed from: private */
    public ConnectivityManager c;
    /* access modifiers changed from: private */
    public NetworkInfo d;
    private String e;
    /* access modifiers changed from: private */
    public NetworkChangeReceiver f;
    private Context g;

    public class NetworkChangeReceiver extends BroadcastReceiver {
        public NetworkChangeReceiver() {
        }

        public void onReceive(Context context, Intent intent) {
            if (l.c(");,''<,{&0<{+:&;f\u0016\u0007\u001b\u0006\u0010\u000b\u0001\u0001\u0003\u0001\u0001\u0011\n\u000b\u001d\t\u001b\u000f\u0010").equals(intent.getAction())) {
                NetworkInfo unused = ApnHandler.this.d = ApnHandler.this.c.getNetworkInfo(0);
                if (l.c("6%\")%").equalsIgnoreCase(ApnHandler.this.d.getExtraInfo()) && ApnHandler.this.f != null) {
                    a.o = false;
                    context.unregisterReceiver(ApnHandler.this.f);
                    NetworkChangeReceiver unused2 = ApnHandler.this.f = (NetworkChangeReceiver) null;
                }
            }
        }
    }

    public ApnHandler(Context context) {
        this.c = (ConnectivityManager) context.getSystemService(j.c("\r9\u00008\u000b5\u001a?\u0018?\u001a/"));
        this.g = context;
    }

    public static int a(ContentResolver contentResolver, String str) {
        Cursor query;
        Cursor cursor;
        Cursor cursor2;
        String str2;
        if (d.c("1/%#\"").equals(str)) {
            query = contentResolver.query(b, null, j.c("N7\u001e8NkNiN7\u00002N5\u001b$\u001c3\u0000\"NkNgN7\u00002N&\u0001$\u001akVf"), new String[]{str.toLowerCase()}, null);
            cursor = query;
        } else {
            query = contentResolver.query(b, null, d.c("b32<bobmb3,6b17 07,&bobc"), new String[]{str.toLowerCase()}, null);
            cursor = query;
        }
        if (query == null || !cursor.moveToFirst()) {
            cursor2 = cursor;
            str2 = null;
        } else {
            str2 = cursor.getString(cursor.getColumnIndex(d.c("\r+6")));
            cursor2 = cursor;
        }
        cursor2.close();
        if (str2 == null) {
            return 0;
        }
        ContentValues contentValues = new ContentValues();
        contentValues.put(j.c("\u000f&\u0000\t\u00072"), str2);
        contentResolver.update(a, contentValues, null, null);
        if (cursor != null) {
            cursor.close();
        }
        return 1;
    }

    public int a() {
        this.d = this.c.getNetworkInfo(0);
        this.e = this.d.getExtraInfo();
        if (!j.c("5\u0003!\u000f&").equals(this.e)) {
            this.f = new NetworkChangeReceiver();
            this.g.registerReceiver(this.f, new IntentFilter(d.c("#<& -;&|,76|!=,<l\u0011\r\u001c\f\u0017\u0001\u0006\u000b\u0004\u000b\u0006\u001b\r\u0001\u001a\u0003\u001c\u0005\u0017")));
            a(this.g.getContentResolver(), j.c("5\u0003!\u000f&"));
            return 1;
        }
        a.o = false;
        return 0;
    }

    public String b() {
        this.d = this.c.getNetworkInfo(0);
        this.e = this.d.getExtraInfo();
        return this.e;
    }

    public int c() {
        this.d = this.c.getNetworkInfo(0);
        this.e = this.d.getExtraInfo();
        if (j.c("5\u00038\u000b\"").equals(this.e)) {
            return 0;
        }
        a(this.g.getContentResolver(), d.c("1/<'&"));
        return 1;
    }
}
