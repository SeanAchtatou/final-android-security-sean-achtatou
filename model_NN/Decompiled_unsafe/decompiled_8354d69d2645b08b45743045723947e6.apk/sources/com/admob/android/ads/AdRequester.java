package com.admob.android.ads;

import android.content.Context;
import android.util.Log;
import com.iPhand.FirstAid.R;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

class AdRequester {
    private static final String ADMOB_ENDPOINT = "http://r.admob.com/ad_source.php";
    private static int REQUEST_TIMEOUT = 3000;

    AdRequester() {
    }

    private static /* synthetic */ void appendParams(StringBuilder sb, String str, String str2) {
        if (str2 != null && str2.length() > 0) {
            try {
                sb.append(R.c("f")).append(URLEncoder.encode(str, i.c("\"\u00121kO"))).append(R.c("}")).append(URLEncoder.encode(str2, i.c("\"\u00121kO")));
            } catch (UnsupportedEncodingException e) {
                Log.e("AdMob SDK", R.c("5\u0014&mX`\u0005.\u0003/\u0004)\u000e'@)\u0013`\u000e/\u0014`\u00135\u00100\u000f2\u0014%\u0004`\u000f.@4\b)\u0013`\u0004%\u0016)\u0003%N`@\u0001\u0004`\u0012%\u00115\u00053\u00143@!\u0012%@)\r0\u000f3\u0013)\u0002,\u0005n"), e);
            }
        }
    }

    static String buildParamString(Context context, String str, String str2) {
        StringBuilder sb = new StringBuilder();
        long currentTimeMillis = System.currentTimeMillis();
        sb.append(i.c("\r")).append(R.c("}")).append(currentTimeMillis / 1000).append(i.c("Y")).append(currentTimeMillis % 1000);
        appendParams(sb, R.c("\u00124"), i.c("G"));
        String publisherId = AdManager.getPublisherId(context);
        if (publisherId == null) {
            throw new IllegalStateException(R.c("05\u0002,\t3\b%\u0012`)\u0004@)\u0013`\u000e/\u0014`\u0013%\u0014a@`4/@3\u00052\u0016%@!\u00043@9\u000f5@-\u00153\u0014`\u0013%\u0014`\u0019/\u00152@0\u0015\"\f)\u0013(\u00052@\t$`\u00013\u0013)\u0007.\u0005$@&\u0012/\r`\u00177\u0017n\u0001$\r/\u0002n\u0003/\rn@`%)\u0014(\u00052@!\u0004$@)\u0014`\u0014/@\u0001\u000e$\u0012/\t$-!\u000e)\u0006%\u00134N8\r,@5\u000e$\u00052@4\b%@|\u00010\u0010,\t#\u00014\t/\u000e~@4\u0001'@/\u0012`\u0003!\f,@\u0001\u0004\r\u0001.\u0001'\u00052N3\u0005405\u0002,\t3\b%\u0012\t\u0004hIn"));
        }
        appendParams(sb, i.c("\u0004"), publisherId);
        appendParams(sb, "f", R.c("\b4\r,?.\u000f\u001f\n3"));
        appendParams(sb, i.c("\u000e"), R.c("\u0014%\u00184"));
        appendParams(sb, i.c("%\u001b/\u0012(\u0003\u0019\u0004\"\u001c"), R.c("q"));
        appendParams(sb, i.c("#\u000f"), R.c("q"));
        appendParams(sb, i.c("\u0001"), AdManager.SDK_VERSION);
        appendParams(sb, R.c("4"), AdManager.getUserId(context));
        appendParams(sb, i.c("\",%\u0018)\u0005\"*"), AdManager.getCoordinatesAsString(context));
        appendParams(sb, R.c("\u0004\u001b\u0004/\u0002\u001d"), AdManager.getBirthdayAsString());
        appendParams(sb, "k", str);
        appendParams(sb, i.c("5\u0012'\u0005%\u001f"), str2);
        if (AdManager.isInTestMode()) {
            appendParams(sb, R.c("-"), i.c("2\u00125\u0003"));
        }
        return sb.toString();
    }

    /* JADX WARNING: Removed duplicated region for block: B:31:0x00ed  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00fa  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.admob.android.ads.Ad requestAd(android.content.Context r13, java.lang.String r14, java.lang.String r15) {
        /*
            r12 = 0
            r1 = 0
            java.lang.String r0 = "\u0016(\u00134\u0018/\u0013h\u0007#\u0005+\u001e5\u0004/\u0018(Y\u000f9\u00122\u00149\u0003#"
            java.lang.String r0 = defpackage.i.c(r0)
            int r0 = r13.checkCallingOrSelfPermission(r0)
            r2 = -1
            if (r0 != r2) goto L_0x0018
            java.lang.String r0 = "#!\u000e.\u000f4@2\u00051\u0015%\u00134@!\u000e`\u0001$@7\t4\b/\u00154@\t\u000e4\u00052\u000e%\u0014`\u0010%\u0012-\t3\u0013)\u000f.\u0013a@`/0\u0005.@-\u0001.\t&\u00053\u0014n\u0018-\f`\u0001.\u0004`\n5\u00134@\"\u0005&\u000f2\u0005`\u0014(\u0005`\u0006)\u000e!\f`\\o\r!\u000e)\u0006%\u00134^`\u0014!\u0007`\u0001$\u0004z@`\\5\u0013%\u0013m\u0010%\u0012-\t3\u0013)\u000f.@!\u000e$\u0012/\t$Z.\u0001-\u0005}B!\u000e$\u0012/\t$N0\u00052\r)\u00133\t/\u000en)\u000e4\u00052\u000e%\u0014B`O~"
            java.lang.String r0 = com.iPhand.FirstAid.R.c(r0)
            com.admob.android.ads.AdManager.clientError(r0)
        L_0x0018:
            com.admob.android.ads.AdMobLocalizer.init(r13)
            long r6 = java.lang.System.currentTimeMillis()
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r3 = buildParamString(r13, r14, r15)     // Catch:{ Exception -> 0x017d }
            java.net.URL r0 = new java.net.URL     // Catch:{ all -> 0x0172 }
            java.lang.String r2 = "http://r.admob.com/ad_source.php"
            r0.<init>(r2)     // Catch:{ all -> 0x0172 }
            java.net.URLConnection r0 = r0.openConnection()     // Catch:{ all -> 0x0172 }
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ all -> 0x0172 }
            java.lang.String r2 = "AdMob SDK"
            java.lang.String r4 = "\u0005\u0018(\u0003#\u00192Z\n\u0012(\u00102\u001f"
            java.lang.String r8 = "\u0005\u0018(\u0003#\u00192Z\u0012\u000e6\u0012"
            java.lang.String r9 = "53\u00052M\u0001\u0007%\u000e4"
            r10 = 1
            java.lang.String r11 = "\u00168\u0015#"
            java.lang.String r11 = defpackage.i.c(r11)     // Catch:{ all -> 0x0172 }
            r0.setRequestMethod(r11)     // Catch:{ all -> 0x0172 }
            r0.setDoOutput(r10)     // Catch:{ all -> 0x0172 }
            java.lang.String r9 = com.iPhand.FirstAid.R.c(r9)     // Catch:{ all -> 0x0172 }
            java.lang.String r10 = com.admob.android.ads.AdManager.getUserAgent()     // Catch:{ all -> 0x0172 }
            r0.setRequestProperty(r9, r10)     // Catch:{ all -> 0x0172 }
            java.lang.String r8 = defpackage.i.c(r8)     // Catch:{ all -> 0x0172 }
            java.lang.String r9 = "!\u00100\f)\u0003!\u0014)\u000f.O8M7\u00177M&\u000f2\rm\u00152\f%\u000e#\u000f$\u0005$"
            java.lang.String r9 = com.iPhand.FirstAid.R.c(r9)     // Catch:{ all -> 0x0172 }
            r0.setRequestProperty(r8, r9)     // Catch:{ all -> 0x0172 }
            java.lang.String r4 = defpackage.i.c(r4)     // Catch:{ all -> 0x0172 }
            int r8 = r3.length()     // Catch:{ all -> 0x0172 }
            java.lang.String r8 = java.lang.Integer.toString(r8)     // Catch:{ all -> 0x0172 }
            r0.setRequestProperty(r4, r8)     // Catch:{ all -> 0x0172 }
            int r4 = com.admob.android.ads.AdRequester.REQUEST_TIMEOUT     // Catch:{ all -> 0x0172 }
            r0.setConnectTimeout(r4)     // Catch:{ all -> 0x0172 }
            int r4 = com.admob.android.ads.AdRequester.REQUEST_TIMEOUT     // Catch:{ all -> 0x0172 }
            r0.setReadTimeout(r4)     // Catch:{ all -> 0x0172 }
            r4 = 3
            boolean r2 = android.util.Log.isLoggable(r2, r4)     // Catch:{ all -> 0x0172 }
            if (r2 == 0) goto L_0x009f
            java.lang.String r2 = "AdMob SDK"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x0172 }
            r4.<init>()     // Catch:{ all -> 0x0172 }
            r8 = 0
            java.lang.String r9 = "\u0012\u00051\u0015%\u00134\t.\u0007`\u0001.@!\u0004`\u0017)\u0014(@\u0010/\u00134`\u0010!\u0012-\u0001-\u0013z@`"
            java.lang.String r9 = com.iPhand.FirstAid.R.c(r9)     // Catch:{ all -> 0x0172 }
            java.lang.StringBuilder r4 = r4.insert(r8, r9)     // Catch:{ all -> 0x0172 }
            java.lang.StringBuilder r4 = r4.append(r3)     // Catch:{ all -> 0x0172 }
            java.lang.String r4 = r4.toString()     // Catch:{ all -> 0x0172 }
            android.util.Log.d(r2, r4)     // Catch:{ all -> 0x0172 }
        L_0x009f:
            java.io.BufferedWriter r2 = new java.io.BufferedWriter     // Catch:{ all -> 0x0172 }
            java.io.OutputStreamWriter r4 = new java.io.OutputStreamWriter     // Catch:{ all -> 0x0172 }
            java.io.OutputStream r8 = r0.getOutputStream()     // Catch:{ all -> 0x0172 }
            r4.<init>(r8)     // Catch:{ all -> 0x0172 }
            r2.<init>(r4)     // Catch:{ all -> 0x0172 }
            r2.write(r3)     // Catch:{ all -> 0x0178 }
            r2.close()     // Catch:{ all -> 0x0178 }
            java.io.BufferedReader r3 = new java.io.BufferedReader     // Catch:{ all -> 0x0178 }
            java.io.InputStreamReader r4 = new java.io.InputStreamReader     // Catch:{ all -> 0x0178 }
            java.io.InputStream r8 = r0.getInputStream()     // Catch:{ all -> 0x0178 }
            r4.<init>(r8)     // Catch:{ all -> 0x0178 }
            r3.<init>(r4)     // Catch:{ all -> 0x0178 }
            r4 = r3
        L_0x00c2:
            java.lang.String r4 = r4.readLine()     // Catch:{ all -> 0x00cd }
            if (r4 == 0) goto L_0x0128
            r5.append(r4)     // Catch:{ all -> 0x00cd }
            r4 = r3
            goto L_0x00c2
        L_0x00cd:
            r0 = move-exception
            r4 = r2
        L_0x00cf:
            if (r2 == 0) goto L_0x00d4
            r4.close()     // Catch:{ Exception -> 0x0180 }
        L_0x00d4:
            if (r3 == 0) goto L_0x00d9
            r3.close()     // Catch:{ Exception -> 0x0180 }
        L_0x00d9:
            throw r0     // Catch:{ Exception -> 0x00da }
        L_0x00da:
            r0 = move-exception
        L_0x00db:
            java.lang.String r2 = "AdMob SDK"
            java.lang.String r3 = "\u0005\u00183\u001b\"W(\u00182W!\u00122W'\u0013f\u00114\u0018+W\u0007\u0013\u000b\u0018$W5\u00124\u0001#\u00055Y"
            java.lang.String r3 = defpackage.i.c(r3)
            android.util.Log.w(r2, r3, r0)
            r0 = r1
        L_0x00e7:
            java.lang.String r2 = r5.toString()
            if (r2 == 0) goto L_0x00f1
            com.admob.android.ads.Ad r1 = com.admob.android.ads.Ad.createAd(r13, r2, r0)
        L_0x00f1:
            java.lang.String r0 = "AdMob SDK"
            r2 = 4
            boolean r0 = android.util.Log.isLoggable(r0, r2)
            if (r0 == 0) goto L_0x0127
            long r2 = java.lang.System.currentTimeMillis()
            long r2 = r2 - r6
            if (r1 != 0) goto L_0x0147
            java.lang.String r0 = "AdMob SDK"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "3%\u00126\u00052@2\u00050\f)\u0005$@4\b!\u0014`\u000e/@!\u00043@!\u0012%@!\u0016!\t,\u0001\"\f%@h"
            java.lang.String r5 = com.iPhand.FirstAid.R.c(r5)
            java.lang.StringBuilder r4 = r4.insert(r12, r5)
            java.lang.StringBuilder r2 = r4.append(r2)
            java.lang.String r3 = "\u001a5^"
            java.lang.String r3 = defpackage.i.c(r3)
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            android.util.Log.i(r0, r2)
        L_0x0127:
            return r1
        L_0x0128:
            java.lang.String r4 = "\u0018M\u0001\u0004\r\u000f\"M\u0001\u000e$\u0012/\t$M\u0003\u00014\u0005'\u000f2\u0019m)#\u000f."
            java.lang.String r4 = com.iPhand.FirstAid.R.c(r4)     // Catch:{ all -> 0x00cd }
            java.lang.String r0 = r0.getHeaderField(r4)     // Catch:{ all -> 0x00cd }
            if (r0 != 0) goto L_0x013a
            java.lang.String r0 = ".\u00032\u0007|Xi\u001a+Y'\u0013+\u0018$Y%\u0018+X5\u0003'\u0003/\u0014i\u0016(\u00134\u0018/\u0013i\u0003/\u001b#\u0004i\u0013#\u0011'\u0002*\u0003h\u0007(\u0010"
            java.lang.String r0 = defpackage.i.c(r0)
        L_0x013a:
            if (r2 == 0) goto L_0x013f
            r2.close()     // Catch:{ Exception -> 0x0145 }
        L_0x013f:
            if (r3 == 0) goto L_0x00e7
            r3.close()     // Catch:{ Exception -> 0x0145 }
            goto L_0x00e7
        L_0x0145:
            r2 = move-exception
            goto L_0x00e7
        L_0x0147:
            java.lang.String r0 = "AdMob SDK"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "\u0001\u0004`\u0012%\u00145\u0012.\u0005$@)\u000e`"
            java.lang.String r5 = com.iPhand.FirstAid.R.c(r5)
            java.lang.StringBuilder r4 = r4.insert(r12, r5)
            java.lang.StringBuilder r2 = r4.append(r2)
            java.lang.String r3 = "\u001a5MfW"
            java.lang.String r3 = defpackage.i.c(r3)
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r2 = r2.append(r1)
            java.lang.String r2 = r2.toString()
            android.util.Log.i(r0, r2)
            goto L_0x0127
        L_0x0172:
            r0 = move-exception
            r2 = r1
            r3 = r1
            r4 = r1
            goto L_0x00cf
        L_0x0178:
            r0 = move-exception
            r3 = r1
            r4 = r2
            goto L_0x00cf
        L_0x017d:
            r0 = move-exception
            goto L_0x00db
        L_0x0180:
            r2 = move-exception
            goto L_0x00d9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.admob.android.ads.AdRequester.requestAd(android.content.Context, java.lang.String, java.lang.String):com.admob.android.ads.Ad");
    }
}
