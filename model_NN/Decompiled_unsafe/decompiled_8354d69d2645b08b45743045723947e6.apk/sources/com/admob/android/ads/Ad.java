package com.admob.android.ads;

import android.content.Context;
import android.graphics.Bitmap;
import android.text.Html;
import android.util.Log;
import com.iPhand.FirstAid.DBUtil;

class Ad {
    /* access modifiers changed from: private */
    public static int CLICK_REQUEST_TIMEOUT = 5000;
    /* access modifiers changed from: private */
    public String clickURL;
    /* access modifiers changed from: private */
    public Context context;
    private String html;
    private Bitmap icon;
    private String iconURL;
    private Bitmap image;
    private int imageHeight;
    private String imageURL;
    private int imageWidth;
    /* access modifiers changed from: private */
    public NetworkListener networkListener;
    private String text;

    interface NetworkListener {
        void onNetworkActivityEnd();

        void onNetworkActivityStart();
    }

    private /* synthetic */ Ad() {
    }

    public static Ad createAd(Context context2, String str, String str2) {
        if (str == null || str.equals("")) {
            return null;
        }
        Ad ad = new Ad();
        ad.context = context2;
        ad.html = str;
        ad.iconURL = str2;
        try {
            int indexOf = str.indexOf(d.c("n#r"));
            if (indexOf >= 0) {
                int indexOf2 = str.indexOf(DBUtil.c("u\u001a'\u00173Ow"), indexOf) + 7;
                int indexOf3 = str.indexOf(d.c("p"), indexOf2);
                ad.clickURL = str.substring(indexOf2, indexOf3);
                indexOf = skipToNext(str, indexOf3 + 1);
                if (indexOf < 0) {
                    return null;
                }
            }
            if (indexOf >= 0 && str.indexOf(DBUtil.c("N<\u001f2"), indexOf) == indexOf) {
                int indexOf4 = str.indexOf(d.c("b!01p"), indexOf) + 6;
                ad.imageURL = str.substring(indexOf4, str.indexOf(DBUtil.c("w"), indexOf4));
                int indexOf5 = str.indexOf(d.c("r*7+5*&p"), indexOf) + 9;
                ad.imageHeight = Integer.valueOf(str.substring(indexOf5, str.indexOf(DBUtil.c("w"), indexOf5))).intValue();
                int indexOf6 = str.indexOf(d.c("b%+66:p"), indexOf) + 8;
                int indexOf7 = str.indexOf(DBUtil.c("w"), indexOf6);
                ad.imageWidth = Integer.valueOf(str.substring(indexOf6, indexOf7)).intValue();
                indexOf = str.indexOf(d.c("~3"), indexOf7 + 1);
                if (indexOf >= 0) {
                    indexOf = skipToNext(str, indexOf + 2);
                }
            }
            if (indexOf >= 0) {
                ad.text = str.substring(indexOf, str.indexOf(DBUtil.c("i"), indexOf)).trim();
                ad.text = Html.fromHtml(ad.text).toString();
            }
            if (ad.hasImage() && ad.getImage() == null) {
                return null;
            }
            if (ad.iconURL != null) {
                ad.getIcon();
            }
            return ad;
        } catch (Exception e) {
            Log.e("AdMob SDK", new StringBuilder().insert(0, d.c("\u00043+>'6b&-r230!'r#6b '!2=,!'hbr")).append(str).toString(), e);
            return null;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:22:0x0054 A[SYNTHETIC, Splitter:B:22:0x0054] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static /* synthetic */ android.graphics.Bitmap fetchImage(java.lang.String r7, boolean r8) {
        /*
            r0 = 0
            if (r7 == 0) goto L_0x0027
            java.net.URL r1 = new java.net.URL     // Catch:{ Throwable -> 0x0028, all -> 0x004f }
            r1.<init>(r7)     // Catch:{ Throwable -> 0x0028, all -> 0x004f }
            java.net.URLConnection r1 = r1.openConnection()     // Catch:{ Throwable -> 0x0028, all -> 0x004f }
            r2 = 0
            r1.setConnectTimeout(r2)     // Catch:{ Throwable -> 0x0028, all -> 0x004f }
            r2 = 0
            r1.setReadTimeout(r2)     // Catch:{ Throwable -> 0x0028, all -> 0x004f }
            r1.setUseCaches(r8)     // Catch:{ Throwable -> 0x0028, all -> 0x004f }
            r1.connect()     // Catch:{ Throwable -> 0x0028, all -> 0x004f }
            java.io.InputStream r2 = r1.getInputStream()     // Catch:{ Throwable -> 0x0028, all -> 0x004f }
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeStream(r2)     // Catch:{ Throwable -> 0x005e }
            if (r2 == 0) goto L_0x0027
            r2.close()     // Catch:{ IOException -> 0x0058 }
        L_0x0027:
            return r0
        L_0x0028:
            r1 = move-exception
            r2 = r0
        L_0x002a:
            java.lang.String r3 = "AdMob SDK"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x005c }
            r4.<init>()     // Catch:{ all -> 0x005c }
            r5 = 0
            java.lang.String r6 = "\"'\u001d7\u001e0\u001fu\u00150\u0006!\u001b;\u0015u\u001b8\u00132\u0017oRu"
            java.lang.String r6 = com.iPhand.FirstAid.DBUtil.c(r6)     // Catch:{ all -> 0x005c }
            java.lang.StringBuilder r4 = r4.insert(r5, r6)     // Catch:{ all -> 0x005c }
            java.lang.StringBuilder r4 = r4.append(r7)     // Catch:{ all -> 0x005c }
            java.lang.String r4 = r4.toString()     // Catch:{ all -> 0x005c }
            android.util.Log.w(r3, r4, r1)     // Catch:{ all -> 0x005c }
            if (r2 == 0) goto L_0x0027
            r2.close()     // Catch:{ IOException -> 0x004d }
            goto L_0x0027
        L_0x004d:
            r1 = move-exception
            goto L_0x0027
        L_0x004f:
            r1 = move-exception
            r2 = r0
            r0 = r1
        L_0x0052:
            if (r2 == 0) goto L_0x0057
            r2.close()     // Catch:{ IOException -> 0x005a }
        L_0x0057:
            throw r0
        L_0x0058:
            r1 = move-exception
            goto L_0x0027
        L_0x005a:
            r1 = move-exception
            goto L_0x0057
        L_0x005c:
            r0 = move-exception
            goto L_0x0052
        L_0x005e:
            r1 = move-exception
            goto L_0x002a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.admob.android.ads.Ad.fetchImage(java.lang.String, boolean):android.graphics.Bitmap");
    }

    private static /* synthetic */ int skipToNext(String str, int i) {
        int length = str.length();
        if (i < 0 || i >= length) {
            return -1;
        }
        char charAt = str.charAt(i);
        char c = charAt;
        char c2 = charAt;
        int i2 = i;
        while (c != '>' && c2 != '<') {
            int i3 = i2 + 1;
            if (i3 >= length) {
                return -1;
            }
            char charAt2 = str.charAt(i3);
            c = charAt2;
            c2 = charAt2;
            i2 = i3;
        }
        if (c2 != '>') {
            return i2;
        }
        int i4 = i2 + 1;
        char charAt3 = str.charAt(i4);
        while (true) {
            int i5 = i4;
            if (!Character.isWhitespace(charAt3)) {
                return i4;
            }
            i4 = i5 + 1;
            if (i4 >= length) {
                return -1;
            }
            charAt3 = str.charAt(charAt3);
        }
    }

    public void clicked() {
        Log.i("AdMob SDK", d.c("\u0013&r!>+1)7&|"));
        if (this.clickURL != null) {
            new Thread() {
                /* JADX WARNING: Removed duplicated region for block: B:16:0x008a  */
                /* JADX WARNING: Removed duplicated region for block: B:25:0x00d8  */
                /* JADX WARNING: Removed duplicated region for block: B:41:? A[RETURN, SYNTHETIC] */
                /* Code decompiled incorrectly, please refer to instructions dump. */
                public void run() {
                    /*
                        r8 = this;
                        r1 = 0
                        r6 = 3
                        r5 = 0
                        com.admob.android.ads.Ad r0 = com.admob.android.ads.Ad.this     // Catch:{ MalformedURLException -> 0x00e2, IOException -> 0x0107 }
                        com.admob.android.ads.Ad$NetworkListener r0 = r0.networkListener     // Catch:{ MalformedURLException -> 0x00e2, IOException -> 0x0107 }
                        if (r0 == 0) goto L_0x0014
                        com.admob.android.ads.Ad r0 = com.admob.android.ads.Ad.this     // Catch:{ MalformedURLException -> 0x00e2, IOException -> 0x0107 }
                        com.admob.android.ads.Ad$NetworkListener r0 = r0.networkListener     // Catch:{ MalformedURLException -> 0x00e2, IOException -> 0x0107 }
                        r0.onNetworkActivityStart()     // Catch:{ MalformedURLException -> 0x00e2, IOException -> 0x0107 }
                    L_0x0014:
                        java.net.URL r2 = new java.net.URL     // Catch:{ MalformedURLException -> 0x00e2, IOException -> 0x0107 }
                        com.admob.android.ads.Ad r0 = com.admob.android.ads.Ad.this     // Catch:{ MalformedURLException -> 0x00e2, IOException -> 0x0107 }
                        java.lang.String r0 = r0.clickURL     // Catch:{ MalformedURLException -> 0x00e2, IOException -> 0x0107 }
                        r2.<init>(r0)     // Catch:{ MalformedURLException -> 0x00e2, IOException -> 0x0107 }
                        r0 = 1
                        java.net.HttpURLConnection.setFollowRedirects(r0)     // Catch:{ MalformedURLException -> 0x0154, IOException -> 0x014e }
                        java.net.URLConnection r0 = r2.openConnection()     // Catch:{ MalformedURLException -> 0x0154, IOException -> 0x014e }
                        java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ MalformedURLException -> 0x0154, IOException -> 0x014e }
                        int r1 = com.admob.android.ads.Ad.CLICK_REQUEST_TIMEOUT     // Catch:{ MalformedURLException -> 0x0154, IOException -> 0x014e }
                        r0.setConnectTimeout(r1)     // Catch:{ MalformedURLException -> 0x0154, IOException -> 0x014e }
                        int r1 = com.admob.android.ads.Ad.CLICK_REQUEST_TIMEOUT     // Catch:{ MalformedURLException -> 0x0154, IOException -> 0x014e }
                        r0.setReadTimeout(r1)     // Catch:{ MalformedURLException -> 0x0154, IOException -> 0x014e }
                        java.lang.String r1 = "'&\u0017'_\u0014\u00150\u001c!"
                        java.lang.String r1 = com.iPhand.FirstAid.DBUtil.c(r1)     // Catch:{ MalformedURLException -> 0x0154, IOException -> 0x014e }
                        java.lang.String r3 = com.admob.android.ads.AdManager.getUserAgent()     // Catch:{ MalformedURLException -> 0x0154, IOException -> 0x014e }
                        r0.setRequestProperty(r1, r3)     // Catch:{ MalformedURLException -> 0x0154, IOException -> 0x014e }
                        java.lang.String r1 = "\re\u0014\f\u0018\u0007\u0017e\u001c\u001b\u0000"
                        java.lang.String r1 = defpackage.l.c(r1)     // Catch:{ MalformedURLException -> 0x0154, IOException -> 0x014e }
                        com.admob.android.ads.Ad r3 = com.admob.android.ads.Ad.this     // Catch:{ MalformedURLException -> 0x0154, IOException -> 0x014e }
                        android.content.Context r3 = r3.context     // Catch:{ MalformedURLException -> 0x0154, IOException -> 0x014e }
                        java.lang.String r3 = com.admob.android.ads.AdManager.getUserId(r3)     // Catch:{ MalformedURLException -> 0x0154, IOException -> 0x014e }
                        r0.setRequestProperty(r1, r3)     // Catch:{ MalformedURLException -> 0x0154, IOException -> 0x014e }
                        r0.connect()     // Catch:{ MalformedURLException -> 0x0154, IOException -> 0x014e }
                        r0.getResponseCode()     // Catch:{ MalformedURLException -> 0x0154, IOException -> 0x014e }
                        java.net.URL r0 = r0.getURL()     // Catch:{ MalformedURLException -> 0x0154, IOException -> 0x014e }
                        java.lang.String r1 = "AdMob SDK"
                        r2 = 3
                        boolean r1 = android.util.Log.isLoggable(r1, r2)     // Catch:{ MalformedURLException -> 0x0157, IOException -> 0x0150 }
                        if (r1 == 0) goto L_0x0087
                        java.lang.String r1 = "AdMob SDK"
                        java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ MalformedURLException -> 0x0157, IOException -> 0x0150 }
                        r2.<init>()     // Catch:{ MalformedURLException -> 0x0157, IOException -> 0x0150 }
                        r3 = 0
                        java.lang.String r4 = "4<\u001c4\u001eu\u00119\u001b6\u0019u\u00160\u0001!\u001b;\u0013!\u001b:\u001cu'\u0007>oRu"
                        java.lang.String r4 = com.iPhand.FirstAid.DBUtil.c(r4)     // Catch:{ MalformedURLException -> 0x0157, IOException -> 0x0150 }
                        java.lang.StringBuilder r2 = r2.insert(r3, r4)     // Catch:{ MalformedURLException -> 0x0157, IOException -> 0x0150 }
                        java.lang.StringBuilder r2 = r2.append(r0)     // Catch:{ MalformedURLException -> 0x0157, IOException -> 0x0150 }
                        java.lang.String r2 = r2.toString()     // Catch:{ MalformedURLException -> 0x0157, IOException -> 0x0150 }
                        android.util.Log.d(r1, r2)     // Catch:{ MalformedURLException -> 0x0157, IOException -> 0x0150 }
                    L_0x0087:
                        r1 = r0
                    L_0x0088:
                        if (r0 == 0) goto L_0x00cf
                        java.lang.String r0 = "AdMob SDK"
                        boolean r0 = android.util.Log.isLoggable(r0, r6)
                        if (r0 == 0) goto L_0x00ae
                        java.lang.String r0 = "AdMob SDK"
                        java.lang.StringBuilder r2 = new java.lang.StringBuilder
                        r2.<init>()
                        java.lang.String r3 = "\u0007%-;!;/u"
                        java.lang.String r3 = defpackage.l.c(r3)
                        java.lang.StringBuilder r2 = r2.insert(r5, r3)
                        java.lang.StringBuilder r2 = r2.append(r1)
                        java.lang.String r2 = r2.toString()
                        android.util.Log.d(r0, r2)
                    L_0x00ae:
                        android.content.Intent r0 = new android.content.Intent
                        java.lang.String r2 = "\u0013;\u0016'\u001d<\u0016{\u001b;\u00060\u001c!\\4\u0011!\u001b:\u001c{$\u001c7\u0002"
                        java.lang.String r2 = com.iPhand.FirstAid.DBUtil.c(r2)
                        java.lang.String r3 = r1.toString()
                        android.net.Uri r3 = android.net.Uri.parse(r3)
                        r0.<init>(r2, r3)
                        r2 = 268435456(0x10000000, float:2.5243549E-29)
                        r0.addFlags(r2)
                        com.admob.android.ads.Ad r2 = com.admob.android.ads.Ad.this     // Catch:{ Exception -> 0x012f }
                        android.content.Context r2 = r2.context     // Catch:{ Exception -> 0x012f }
                        r2.startActivity(r0)     // Catch:{ Exception -> 0x012f }
                    L_0x00cf:
                        r0 = r8
                    L_0x00d0:
                        com.admob.android.ads.Ad r0 = com.admob.android.ads.Ad.this
                        com.admob.android.ads.Ad$NetworkListener r0 = r0.networkListener
                        if (r0 == 0) goto L_0x00e1
                        com.admob.android.ads.Ad r0 = com.admob.android.ads.Ad.this
                        com.admob.android.ads.Ad$NetworkListener r0 = r0.networkListener
                        r0.onNetworkActivityEnd()
                    L_0x00e1:
                        return
                    L_0x00e2:
                        r0 = move-exception
                    L_0x00e3:
                        java.lang.String r2 = "AdMob SDK"
                        java.lang.StringBuilder r3 = new java.lang.StringBuilder
                        r3.<init>()
                        java.lang.String r4 = "\u00054$3''%0,u+9!6#u\u001d\u0007\u0004{hu\u001f<$9h!:,h!'u.:$9'\"h4&,?41{hu"
                        java.lang.String r4 = defpackage.l.c(r4)
                        java.lang.StringBuilder r3 = r3.insert(r5, r4)
                        com.admob.android.ads.Ad r4 = com.admob.android.ads.Ad.this
                        java.lang.String r4 = r4.clickURL
                        java.lang.StringBuilder r3 = r3.append(r4)
                        java.lang.String r3 = r3.toString()
                        android.util.Log.w(r2, r3, r0)
                        r0 = r1
                        goto L_0x0088
                    L_0x0107:
                        r0 = move-exception
                        r2 = r1
                    L_0x0109:
                        java.lang.String r1 = "AdMob SDK"
                        java.lang.StringBuilder r3 = new java.lang.StringBuilder
                        r3.<init>()
                        java.lang.String r4 = "1:\u00079\u0016u\u001c:\u0006u\u00160\u00060\u00008\u001b;\u0017u\u0014<\u001c4\u001eu\u00119\u001b6\u0019u\u00160\u0001!\u001b;\u0013!\u001b:\u001cu'\u0007>{Ru%<\u001e9R!\u0000,R!\u001du\u0014:\u001e9\u001d\"R4\u001c,\u00054\u000b{Ru"
                        java.lang.String r4 = com.iPhand.FirstAid.DBUtil.c(r4)
                        java.lang.StringBuilder r3 = r3.insert(r5, r4)
                        com.admob.android.ads.Ad r4 = com.admob.android.ads.Ad.this
                        java.lang.String r4 = r4.clickURL
                        java.lang.StringBuilder r3 = r3.append(r4)
                        java.lang.String r3 = r3.toString()
                        android.util.Log.w(r1, r3, r0)
                        r0 = r2
                        r1 = r2
                        goto L_0x0088
                    L_0x012f:
                        r0 = move-exception
                        java.lang.String r2 = "AdMob SDK"
                        java.lang.StringBuilder r3 = new java.lang.StringBuilder
                        r3.<init>()
                        java.lang.String r4 = "\u000b:=9,u&:<u'%-;h7::?&-'h:&u)1h6$<+>h!'u"
                        java.lang.String r4 = defpackage.l.c(r4)
                        java.lang.StringBuilder r3 = r3.insert(r5, r4)
                        java.lang.StringBuilder r1 = r3.append(r1)
                        java.lang.String r1 = r1.toString()
                        android.util.Log.e(r2, r1, r0)
                        r0 = r8
                        goto L_0x00d0
                    L_0x014e:
                        r0 = move-exception
                        goto L_0x0109
                    L_0x0150:
                        r1 = move-exception
                        r2 = r0
                        r0 = r1
                        goto L_0x0109
                    L_0x0154:
                        r0 = move-exception
                        r1 = r2
                        goto L_0x00e3
                    L_0x0157:
                        r1 = move-exception
                        r7 = r1
                        r1 = r0
                        r0 = r7
                        goto L_0x00e3
                    */
                    throw new UnsupportedOperationException("Method not decompiled: com.admob.android.ads.Ad.AnonymousClass1.run():void");
                }
            }.start();
        }
    }

    public boolean equals(Object obj) {
        if (obj instanceof Ad) {
            return toString().equals(((Ad) obj).toString());
        }
        return false;
    }

    public String getClickURL() {
        return this.clickURL;
    }

    public String getHTML() {
        return this.html;
    }

    public Bitmap getIcon() {
        if (this.icon == null) {
            this.icon = fetchImage(this.iconURL, true);
            if (this.icon == null) {
                Log.w("AdMob SDK", new StringBuilder().insert(0, DBUtil.c("\u0016\u001d \u001e1R;\u001d!R2\u0017!R<\u0011:\u001cu\u0014:\u0000u\u00131R3\u0000:\u001fu")).append(this.iconURL).toString());
            }
        }
        return this.icon;
    }

    public Bitmap getImage() {
        if (this.image == null && this.imageURL != null) {
            this.image = fetchImage(this.imageURL, false);
        }
        return this.image;
    }

    public int getImageHeight() {
        return this.image != null ? this.image.getHeight() : this.imageHeight;
    }

    public String getImageURL() {
        return this.imageURL;
    }

    public int getImageWidth() {
        return this.image != null ? this.image.getWidth() : this.imageWidth;
    }

    public NetworkListener getNetworkListener() {
        return this.networkListener;
    }

    public String getText() {
        return this.text;
    }

    public boolean hasImage() {
        return this.imageURL != null;
    }

    public int hashCode() {
        return toString().hashCode();
    }

    public void setNetworkListener(NetworkListener networkListener2) {
        this.networkListener = networkListener2;
    }

    public String toString() {
        String text2 = getText();
        return text2 == null ? "" : text2;
    }
}
