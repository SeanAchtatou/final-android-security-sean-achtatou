package com.admob.android.ads;

import android.content.Context;
import android.util.Log;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Locale;
import java.util.Properties;

class AdMobLocalizer {
    /* access modifiers changed from: private */
    public static String ADMOB_LOCALIZATION_CACHE_DIR = "admob_cache";
    /* access modifiers changed from: private */
    public static String ADMOB_LOCALIZATION_URL_STRING = "http://mm.admob.com/static/android/i18n/20091123";
    private static String DEFAULT_LANGUAGE = "en";
    /* access modifiers changed from: private */
    public static String PROPS_FILE_SUFFIX = ".properties";
    private static AdMobLocalizer singleton = null;
    private static Context staticContext = null;
    /* access modifiers changed from: private */
    public Context context;
    /* access modifiers changed from: private */
    public String language = null;
    private Properties strings;

    private class InitLocalizerThread extends Thread {
        private /* synthetic */ InitLocalizerThread() {
        }

        public void run() {
            BufferedOutputStream bufferedOutputStream;
            try {
                URLConnection openConnection = new URL(AdMobLocalizer.ADMOB_LOCALIZATION_URL_STRING + l.c("z") + AdMobLocalizer.this.language + AdMobLocalizer.PROPS_FILE_SUFFIX).openConnection();
                openConnection.connect();
                BufferedInputStream bufferedInputStream = new BufferedInputStream(openConnection.getInputStream());
                File file = new File(AdMobLocalizer.this.context.getDir(AdMobLocalizer.ADMOB_LOCALIZATION_CACHE_DIR, 0), "20091123");
                if (!file.exists()) {
                    file.mkdir();
                }
                bufferedOutputStream = new BufferedOutputStream(new FileOutputStream(new File(file, new StringBuilder().insert(0, AdMobLocalizer.this.language).append(AdMobLocalizer.PROPS_FILE_SUFFIX).toString())));
                byte[] bArr = new byte[512];
                while (true) {
                    int read = bufferedInputStream.read(bArr, 0, bArr.length);
                    if (read > 0) {
                        bufferedOutputStream.write(bArr, 0, read);
                    } else {
                        bufferedOutputStream.close();
                        return;
                    }
                }
            } catch (Exception e) {
                if (Log.isLoggable("AdMob SDK", 3)) {
                    Log.d("AdMob SDK", d.c("\u0011-'.6b<-&b5'&b>-1#>+('6b!6 +<%!b40=/r6:'r\u00036\u000f= r170$' 1|"));
                }
            } catch (Throwable th) {
                bufferedOutputStream.close();
                throw th;
            }
        }
    }

    private /* synthetic */ AdMobLocalizer(Context context2) {
        this.context = context2;
        Locale locale = Locale.getDefault();
        setLanguage(locale.getLanguage());
    }

    public static void init(Context context2) {
        if (staticContext == null) {
            staticContext = context2;
        }
        initSingleton();
    }

    private static /* synthetic */ void initSingleton() {
        if (singleton == null) {
            singleton = new AdMobLocalizer(staticContext);
        }
    }

    private /* synthetic */ String internalLocalize(String str) {
        String property;
        loadStrings();
        return (this.strings == null || (property = this.strings.getProperty(str)) == null || property.equals("")) ? str : property;
    }

    private /* synthetic */ boolean loadStrings() {
        if (this.strings == null) {
            try {
                Properties properties = new Properties();
                File file = new File(this.context.getDir(ADMOB_LOCALIZATION_CACHE_DIR, 0), "20091123");
                if (!file.exists()) {
                    file.mkdir();
                }
                File file2 = new File(file, new StringBuilder().insert(0, this.language).append(PROPS_FILE_SUFFIX).toString());
                if (file2.exists()) {
                    properties.load(new FileInputStream(file2));
                    this.strings = properties;
                }
            } catch (IOException e) {
                this.strings = null;
            }
        }
        return this.strings != null;
    }

    public static String localize(String str) {
        init(staticContext);
        return singleton.internalLocalize(str);
    }

    public static AdMobLocalizer singleton() {
        initSingleton();
        return singleton;
    }

    public String getLanguage() {
        return this.language;
    }

    public void setLanguage(String str) {
        if (str != this.language) {
            this.strings = null;
            this.language = str;
            if (this.language == null || this.language.equals("")) {
                this.language = DEFAULT_LANGUAGE;
            }
            if (!loadStrings()) {
                new InitLocalizerThread().start();
            }
        }
    }
}
