package com.admob.android.ads;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import com.iPhand.FirstAid.DBUtil;
import com.iPhand.FirstAid.R;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

public class AdManager {
    private static final long LOCATION_UPDATE_INTERVAL = 900000;
    static final String LOG = "AdMob SDK";
    static final String SDK_SITE_ID = "a1496ced2842262";
    public static final String SDK_VERSION = "20091123-ANDROID-3312276cc1406347";
    private static final String SDK_VERSION_CHECKSUM = "3312276cc1406347";
    static final String SDK_VERSION_DATE = "20091123";
    private static GregorianCalendar birthday;
    /* access modifiers changed from: private */
    public static Location coordinates;
    /* access modifiers changed from: private */
    public static long coordinatesTimestamp;
    private static Gender gender;
    private static String publisherId;
    private static boolean testMode;
    private static String userAgent;
    private static String userId;

    public enum Gender {
        MALE,
        FEMALE
    }

    static {
        Log.i(LOG, j.c("/2#9\fv=\u0012%v\u00183\u001c%\u00079\u0000v\u0007%Nd^fWg_d]{/\u0018*\u0004!\u001f*{]e_d\\aX5\rgZfXeZa"));
    }

    protected static void clientError(String str) {
        Log.e(LOG, str);
        throw new IllegalArgumentException(str);
    }

    public static GregorianCalendar getBirthday() {
        return birthday;
    }

    static String getBirthdayAsString() {
        GregorianCalendar birthday2 = getBirthday();
        if (birthday2 == null) {
            return null;
        }
        return String.format(j.c("KfZ2Kf\\2Kf\\2"), Integer.valueOf(birthday2.get(1)), Integer.valueOf(birthday2.get(2) + 1), Integer.valueOf(birthday2.get(5)));
    }

    public static Location getCoordinates(Context context) {
        Context context2;
        String str;
        String str2;
        boolean z;
        final LocationManager locationManager;
        LocationManager locationManager2 = null;
        boolean z2 = false;
        if (context != null && (coordinates == null || System.currentTimeMillis() > coordinatesTimestamp + LOCATION_UPDATE_INTERVAL)) {
            synchronized (context) {
                if (coordinates == null || System.currentTimeMillis() > coordinatesTimestamp + LOCATION_UPDATE_INTERVAL) {
                    coordinatesTimestamp = System.currentTimeMillis();
                    if (context.checkCallingOrSelfPermission(j.c("7\u00002\u001c9\u00072@&\u000b$\u0003?\u001d%\u00079\u0000x/\u0015-\u0013=\u00051\u0015!\u0017<\u0005+\t\"\u0019-\u0017:\u001f!\u0018")) == 0) {
                        if (Log.isLoggable(LOG, 3)) {
                            Log.d(LOG, R.c("\u0014\u00129\t.\u0007`\u0014/@'\u00054@,\u000f#\u00014\t/\u000e3@&\u0012/\r`\u0014(\u0005`\u000e%\u00147\u000f2\u000bn"));
                        }
                        LocationManager locationManager3 = (LocationManager) context.getSystemService(j.c("\u00029\r7\u001a?\u00018"));
                        if (locationManager3 != null) {
                            Criteria criteria = new Criteria();
                            criteria.setAccuracy(2);
                            criteria.setCostAllowed(false);
                            str = locationManager3.getBestProvider(criteria, true);
                            locationManager2 = locationManager3;
                            z2 = true;
                        } else {
                            str = null;
                            locationManager2 = locationManager3;
                            z2 = true;
                        }
                    } else {
                        str = null;
                    }
                    if (str == null && context.checkCallingOrSelfPermission(R.c("!\u000e$\u0012/\t$N0\u00052\r)\u00133\t/\u000en!\u0003#\u00053\u0013?\u0006)\u000e%\u001f,\u000f#\u00014\t/\u000e")) == 0) {
                        if (Log.isLoggable(LOG, 3)) {
                            Log.d(LOG, j.c("\u0002\u001c/\u00078\tv\u001a9N1\u000b\"N:\u00015\u000f\"\u00079\u0000%N0\u001c9\u0003v)\u0006=x"));
                        }
                        locationManager = (LocationManager) context.getSystemService(R.c("\f/\u0003!\u0014)\u000f."));
                        if (locationManager != null) {
                            Criteria criteria2 = new Criteria();
                            criteria2.setAccuracy(1);
                            criteria2.setCostAllowed(false);
                            str2 = locationManager.getBestProvider(criteria2, true);
                            z = true;
                        } else {
                            str2 = str;
                            z = true;
                        }
                    } else {
                        str2 = str;
                        z = z2;
                        locationManager = locationManager2;
                    }
                    if (!z) {
                        if (Log.isLoggable(LOG, 3)) {
                            Log.d(LOG, j.c("-7\u00008\u0001\"N7\r5\u000b%\u001dv\u001b%\u000b$I%N:\u00015\u000f\"\u00079\u0000xNv>3\u001c;\u0007%\u001d?\u00018\u001dv\u000f$\u000bv\u00009\u001av\u001d3\u001ax"));
                        }
                    } else if (str2 != null) {
                        Log.i(LOG, j.c("\u001a\u00015\u000f\"\u00079\u0000v\u001e$\u0001 \u00072\u000b$N%\u000b\"\u001b&N%\u001b5\r3\u001d%\b#\u0002:\u0017x"));
                        locationManager.requestLocationUpdates(str2, 0, 0.0f, new LocationListener() {
                            public void onLocationChanged(Location location) {
                                Location unused = AdManager.coordinates = location;
                                long unused2 = AdManager.coordinatesTimestamp = System.currentTimeMillis();
                                locationManager.removeUpdates(this);
                                Log.i(AdManager.LOG, new StringBuilder().insert(0, l.c("\u00149 !'-1h9'6)!!:&u")).append(AdManager.coordinates.getLatitude()).append(DBUtil.c("y")).append(AdManager.coordinates.getLongitude()).append(l.c("h4<u")).append(new Date(AdManager.coordinatesTimestamp).toString()).append(DBUtil.c("{")).toString());
                            }

                            public void onProviderDisabled(String str) {
                            }

                            public void onProviderEnabled(String str) {
                            }

                            public void onStatusChanged(String str, int i, Bundle bundle) {
                            }
                        }, context.getMainLooper());
                        context2 = context;
                    } else if (Log.isLoggable(LOG, 3)) {
                        Log.d(LOG, R.c("./@,\u000f#\u00014\t/\u000e`\u00102\u000f6\t$\u00052\u0013`\u00012\u0005`\u00016\u0001)\f!\u0002,\u0005n@`!$\u0013`\u0017)\f,@.\u000f4@\"\u0005`\u0007%\u000f4\u00012\u0007%\u0014%\u0004n"));
                        context2 = context;
                    }
                }
                context2 = context;
            }
        }
        return coordinates;
    }

    static String getCoordinatesAsString(Context context) {
        String str = null;
        Location coordinates2 = getCoordinates(context);
        if (coordinates2 != null) {
            str = coordinates2.getLatitude() + j.c("z") + coordinates2.getLongitude();
        }
        if (Log.isLoggable(LOG, 3)) {
            Log.d(LOG, new StringBuilder().insert(0, R.c("\u0015\u0013%\u0012`\u0003/\u000f2\u0004)\u000e!\u0014%\u0013`\u00012\u0005`")).append(str).toString());
        }
        return str;
    }

    public static Gender getGender() {
        return gender;
    }

    static String getGenderAsString() {
        if (gender == Gender.MALE) {
            return R.c("-");
        }
        if (gender == Gender.FEMALE) {
            return "f";
        }
        return null;
    }

    public static String getPublisherId(Context context) {
        if (publisherId == null) {
            try {
                ApplicationInfo applicationInfo = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128);
                if (applicationInfo != null) {
                    String string = applicationInfo.metaData.getString(R.c("!\u0004-\u000f\"\u001f0\u0015\"\f)\u0013(\u00052\u001f)\u0004"));
                    Log.d(LOG, new StringBuilder().insert(0, j.c(">#\f:\u0007%\u00063\u001cv'\u0012N$\u000b7\nv\b$\u0001;N\u0017\u00002\u001c9\u00072#7\u0000?\b3\u001d\"@.\u0003:N?\u001dv")).append(string).toString());
                    if (!string.equals(SDK_SITE_ID) || (!context.getPackageName().equals(R.c("\u0003/\rn\u0001$\r/\u0002n\u0001.\u00042\u000f)\u0004n\u0014%\u00134")) && !context.getPackageName().equals(j.c("5\u0001;@3\u00167\u0003&\u00023@7\n;\u00014@:\u001b8\u000f$\u00027\u00002\u000b$")))) {
                        setPublisherId(string);
                    } else {
                        Log.i(LOG, R.c("\u0014\b)\u0013`\t3@!@3\u0001-\u0010,\u0005`\u00010\u0010,\t#\u00014\t/\u000e`\u0013/@!\f,\u000f7\t.\u0007`\u0013!\r0\f%@0\u0015\"\f)\u0013(\u00052@\t$n"));
                        publisherId = string;
                    }
                }
            } catch (Exception e) {
                Log.e(LOG, j.c("\u0015\u0001#\u00022N8\u0001\"N$\u000b7\nv/\u0012#\u0019,\t>\u0003,\u001a'\u0005&\u0013<\t'\u0012N;\u000b\"\u000f{\n7\u001a7N0\u001c9\u0003v/8\n$\u0001?\n\u001b\u000f8\u00070\u000b%\u001ax\u0016;\u0002x"), e);
            }
        }
        return publisherId;
    }

    static String getUserAgent() {
        StringBuffer stringBuffer;
        if (userAgent == null) {
            StringBuffer stringBuffer2 = new StringBuffer();
            String str = Build.VERSION.RELEASE;
            if (str.length() > 0) {
                stringBuffer2.append(str);
                stringBuffer = stringBuffer2;
            } else {
                stringBuffer2.append(R.c("qNp"));
                stringBuffer = stringBuffer2;
            }
            stringBuffer.append(R.c("[`"));
            Locale locale = Locale.getDefault();
            String language = locale.getLanguage();
            if (language != null) {
                stringBuffer2.append(language.toLowerCase());
                String country = locale.getCountry();
                if (country != null) {
                    stringBuffer2.append(j.c("{"));
                    stringBuffer2.append(country.toLowerCase());
                }
            } else {
                stringBuffer2.append("en");
            }
            String str2 = Build.MODEL;
            if (str2.length() > 0) {
                stringBuffer2.append(R.c("[`"));
                stringBuffer2.append(str2);
            }
            String str3 = Build.ID;
            if (str3.length() > 0) {
                stringBuffer2.append(j.c("v,#\u0007:\ny"));
                stringBuffer2.append(str3);
            }
            userAgent = String.format(R.c("-/\u001a)\f,\u0001oUnP`H\f\t.\u00158[`5{@\u0001\u000e$\u0012/\t$@e\u0013i@\u0001\u00100\f%7%\u0002\u000b\t4OuRuNqPk@h+\b4\r,l@,\t+\u0005`'%\u0003+\u000fi@\u0016\u00052\u0013)\u000f.OsNpNt@\r\u000f\"\t,\u0005`3!\u0006!\u0012)OuRsNqRnR`H\u0001\u0004\r\u000f\"M\u0001.\u00042\u000f)\u0004Me\u0013i"), stringBuffer2, SDK_VERSION_DATE);
            if (Log.isLoggable(LOG, 3)) {
                Log.d(LOG, new StringBuilder().insert(0, j.c(">>\u00018\u000bq\u001dv\u001b%\u000b$C7\t3\u0000\"N?\u001dlNv")).append(userAgent).toString());
            }
        }
        return userAgent;
    }

    public static String getUserId(Context context) {
        if (userId == null) {
            userId = Settings.System.getString(context.getContentResolver(), j.c("\u000f8\n$\u0001?\n\t\u00072"));
            userId = md5(userId);
            Log.i(LOG, new StringBuilder().insert(0, R.c("\u0014\b%@5\u0013%\u0012`)\u0004@)\u0013`")).append(userId).toString());
        }
        return userId;
    }

    public static boolean isInTestMode() {
        return testMode;
    }

    private static /* synthetic */ String md5(String str) {
        if (str == null || str.length() <= 0) {
            return null;
        }
        try {
            MessageDigest instance = MessageDigest.getInstance(j.c("\u001b*c"));
            instance.update(str.getBytes(), 0, str.length());
            return String.format(R.c("ePsR\u0018"), new BigInteger(1, instance.digest()));
        } catch (Exception e) {
            Log.d(LOG, new StringBuilder().insert(0, j.c("\u0015\u0001#\u00022N8\u0001\"N1\u000b8\u000b$\u000f\"\u000bv\u00067\u001d>N9\bv")).append(str).toString(), e);
            userId = userId.substring(0, 32);
            return null;
        }
    }

    public static void setBirthday(int i, int i2, int i3) {
        GregorianCalendar gregorianCalendar = new GregorianCalendar();
        gregorianCalendar.set(i, i2 - 1, i3);
        setBirthday(gregorianCalendar);
    }

    public static void setBirthday(GregorianCalendar gregorianCalendar) {
        birthday = gregorianCalendar;
    }

    public static void setGender(Gender gender2) {
        gender = gender2;
    }

    public static void setInTestMode(boolean z) {
        testMode = z;
    }

    public static void setPublisherId(String str) {
        if (str == null || str.length() != 15) {
            clientError(new StringBuilder().insert(0, R.c("3\u00054\u00150`%\u00122\u000f2z@`).\u0003/\u00122\u0005#\u0014`!$-/\u0002`\u00105\u0002,\t3\b%\u0012`)\u0004N`@\u0013\b/\u0015,\u0004`Qu@\u001b\u0001m\u0006lPmY\u001d@#\b!\u0012!\u00034\u00052\u0013z@`")).append(publisherId).toString());
        }
        if (str.equalsIgnoreCase(SDK_SITE_ID)) {
            clientError(j.c("\u0005+\u0002;\u0006N\u0013<\u0004!\u0004TvN\u0015\u000f8\u00009\u001av\u001b%\u000bv\u001a>\u000bv\u001d7\u0003&\u00023N&\u001b4\u0002?\u001d>\u000b$N\u001f*vF7_bW`\r3\ndVb\\dXdGxNv79\u001b$\u001dv\u0007%N7\u00187\u0007:\u000f4\u00023N9\u0000v\u0019!\u0019x\u000f2\u00039\fx\r9\u0003x"));
        }
        Log.i(LOG, new StringBuilder().insert(0, R.c("05\u0002,\t3\b%\u0012`)\u0004@3\u00054@4\u000f`")).append(str).toString());
        publisherId = str;
    }
}
