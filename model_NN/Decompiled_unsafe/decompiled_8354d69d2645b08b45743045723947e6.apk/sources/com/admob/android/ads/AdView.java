package com.admob.android.ads;

import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.Log;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.widget.RelativeLayout;
import com.iPhand.FirstAid.DBUtil;
import com.iPhand.FirstAid.R;
import java.util.Timer;
import java.util.TimerTask;

public class AdView extends RelativeLayout {
    private static final String ADMOB_EMULATOR_NOTICE = "http://api.admob.com/v1/pubcode/android_sdk_emulator_notice";
    private static final int ANIMATION_DURATION = 700;
    private static final float ANIMATION_Z_DEPTH_PERCENTAGE = -0.4f;
    public static final int HEIGHT = 48;
    private static boolean checkedForMessages = false;
    /* access modifiers changed from: private */
    public static Handler uiThreadHandler;
    /* access modifiers changed from: private */
    public AdContainer ad;
    private int backgroundColor;
    private boolean hideWhenNoAd;
    private boolean isOnScreen;
    /* access modifiers changed from: private */
    public String keywords;
    /* access modifiers changed from: private */
    public AdListener listener;
    /* access modifiers changed from: private */
    public int requestInterval;
    private Timer requestIntervalTimer;
    /* access modifiers changed from: private */
    public boolean requestingFreshAd;
    /* access modifiers changed from: private */
    public String searchQuery;
    private int textColor;

    public interface AdListener {
        void onFailedToReceiveAd(AdView adView);

        @Deprecated
        void onNewAd();

        void onReceiveAd(AdView adView);
    }

    private final class SwapViews implements Runnable {
        /* access modifiers changed from: private */
        public AdContainer newAd;
        /* access modifiers changed from: private */
        public AdContainer oldAd;

        public SwapViews(AdContainer adContainer) {
            this.newAd = adContainer;
        }

        public void run() {
            this.oldAd = AdView.this.ad;
            if (this.oldAd != null) {
                this.oldAd.setVisibility(8);
            }
            this.newAd.setVisibility(0);
            Rotate3dAnimation rotate3dAnimation = new Rotate3dAnimation(90.0f, 0.0f, ((float) AdView.this.getWidth()) / 2.0f, ((float) AdView.this.getHeight()) / 2.0f, AdView.ANIMATION_Z_DEPTH_PERCENTAGE * ((float) AdView.this.getWidth()), false);
            rotate3dAnimation.setDuration(700);
            rotate3dAnimation.setFillAfter(true);
            rotate3dAnimation.setInterpolator(new DecelerateInterpolator());
            rotate3dAnimation.setAnimationListener(new Animation.AnimationListener() {
                public void onAnimationEnd(Animation animation) {
                    if (SwapViews.this.oldAd != null) {
                        AdView.this.removeView(SwapViews.this.oldAd);
                        SwapViews.this.oldAd.recycleBitmaps();
                    }
                    AdContainer unused = AdView.this.ad = SwapViews.this.newAd;
                }

                public void onAnimationRepeat(Animation animation) {
                }

                public void onAnimationStart(Animation animation) {
                }
            });
            AdView.this.startAnimation(rotate3dAnimation);
        }
    }

    public AdView(Context context) {
        this(context, null, 0);
    }

    public AdView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public AdView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        int i2 = -1;
        int i3 = AdContainer.DEFAULT_BACKGROUND_COLOR;
        this.isOnScreen = false;
        setFocusable(true);
        setDescendantFocusability(262144);
        setClickable(true);
        if (attributeSet != null) {
            String sb = new StringBuilder().insert(0, i.c("\u001f2\u00036MiX5\u0014.\u0012+\u00165Y'\u0019\"\u0005)\u001e\"Y%\u0018+X'\u0007-X4\u00125X")).append(context.getPackageName()).toString();
            boolean attributeBooleanValue = attributeSet.getAttributeBooleanValue(sb, l.c("!-&<<&2"), false);
            if (attributeBooleanValue) {
                AdManager.setInTestMode(attributeBooleanValue);
            }
            i2 = attributeSet.getAttributeUnsignedIntValue(sb, i.c("\u0003#\u000f24)\u001b)\u0005"), -1);
            i3 = attributeSet.getAttributeUnsignedIntValue(sb, l.c("7)6#2::=;,\u0016'9''"), AdContainer.DEFAULT_BACKGROUND_COLOR);
            this.keywords = attributeSet.getAttributeValue(sb, i.c("-\u0012?\u0000)\u0005\"\u0004"));
            setRequestInterval(attributeSet.getAttributeIntValue(sb, l.c("'-3:0;=\u0001;<0:#)9"), 0));
            setGoneWithoutAd(attributeSet.getAttributeBooleanValue(sb, i.c("\u001e50)\u0019# /\u0003.\u00183\u0003\u0007\u0013"), isGoneWithoutAd()));
        }
        setTextColor(i2);
        setBackgroundColor(i3);
        if (super.getVisibility() == 0) {
            requestFreshAd();
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void applyFadeIn(AdContainer adContainer) {
        this.ad = adContainer;
        if (this.isOnScreen) {
            AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
            alphaAnimation.setDuration(233);
            alphaAnimation.startNow();
            alphaAnimation.setFillAfter(true);
            alphaAnimation.setInterpolator(new AccelerateInterpolator());
            startAnimation(alphaAnimation);
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void applyRotation(final AdContainer adContainer) {
        adContainer.setVisibility(8);
        Rotate3dAnimation rotate3dAnimation = new Rotate3dAnimation(0.0f, -90.0f, ((float) getWidth()) / 2.0f, ((float) getHeight()) / 2.0f, ANIMATION_Z_DEPTH_PERCENTAGE * ((float) getWidth()), true);
        rotate3dAnimation.setDuration(700);
        rotate3dAnimation.setFillAfter(true);
        rotate3dAnimation.setInterpolator(new AccelerateInterpolator());
        rotate3dAnimation.setAnimationListener(new Animation.AnimationListener() {
            public void onAnimationEnd(Animation animation) {
                AdView.this.post(new SwapViews(adContainer));
            }

            public void onAnimationRepeat(Animation animation) {
            }

            public void onAnimationStart(Animation animation) {
            }
        });
        startAnimation(rotate3dAnimation);
    }

    private /* synthetic */ void manageRequestIntervalTimer(boolean z) {
        AdView adView;
        synchronized (this) {
            if (z) {
                if (this.requestInterval > 0) {
                    if (this.requestIntervalTimer == null) {
                        this.requestIntervalTimer = new Timer();
                        this.requestIntervalTimer.schedule(new TimerTask() {
                            public void run() {
                                if (Log.isLoggable("AdMob SDK", 3)) {
                                    int access$900 = AdView.this.requestInterval / 1000;
                                    if (Log.isLoggable("AdMob SDK", 3)) {
                                        Log.d("AdMob SDK", new StringBuilder().insert(0, d.c("\u0000'#771&+<%r#r$ '!*r#6b0'1#'17b3b '#771&b;,&' 43.r231!'6bz")).append(access$900).append(DBUtil.c("R&\u00176\u001d;\u0016&[{")).toString());
                                    }
                                }
                                AdView.this.requestFreshAd();
                            }
                        }, (long) this.requestInterval, (long) this.requestInterval);
                    }
                    adView = this;
                }
            }
            if ((!z || this.requestInterval == 0) && this.requestIntervalTimer != null) {
                this.requestIntervalTimer.cancel();
                this.requestIntervalTimer = null;
                adView = this;
            }
            adView = this;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:24:0x007e A[SYNTHETIC, Splitter:B:24:0x007e] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private /* synthetic */ void retrieveDeveloperMessage(android.content.Context r6) {
        /*
            r5 = this;
            r2 = 0
            r0 = 0
            java.lang.String r0 = com.admob.android.ads.AdRequester.buildParamString(r6, r0, r0)     // Catch:{ Exception -> 0x0085, all -> 0x007a }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0085, all -> 0x007a }
            r1.<init>()     // Catch:{ Exception -> 0x0085, all -> 0x007a }
            java.lang.String r3 = "http://api.admob.com/v1/pubcode/android_sdk_emulator_notice"
            r1.append(r3)     // Catch:{ Exception -> 0x0085, all -> 0x007a }
            java.lang.String r3 = "H"
            java.lang.String r3 = defpackage.i.c(r3)     // Catch:{ Exception -> 0x0085, all -> 0x007a }
            r1.append(r3)     // Catch:{ Exception -> 0x0085, all -> 0x007a }
            r1.append(r0)     // Catch:{ Exception -> 0x0085, all -> 0x007a }
            java.net.URL r0 = new java.net.URL     // Catch:{ Exception -> 0x0085, all -> 0x007a }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x0085, all -> 0x007a }
            r0.<init>(r1)     // Catch:{ Exception -> 0x0085, all -> 0x007a }
            java.net.URLConnection r0 = r0.openConnection()     // Catch:{ Exception -> 0x0085, all -> 0x007a }
            r0.connect()     // Catch:{ Exception -> 0x0085, all -> 0x007a }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0085, all -> 0x007a }
            r3.<init>()     // Catch:{ Exception -> 0x0085, all -> 0x007a }
            java.io.BufferedReader r1 = new java.io.BufferedReader     // Catch:{ Exception -> 0x0085, all -> 0x007a }
            java.io.InputStreamReader r4 = new java.io.InputStreamReader     // Catch:{ Exception -> 0x0085, all -> 0x007a }
            java.io.InputStream r0 = r0.getInputStream()     // Catch:{ Exception -> 0x0085, all -> 0x007a }
            r4.<init>(r0)     // Catch:{ Exception -> 0x0085, all -> 0x007a }
            r1.<init>(r4)     // Catch:{ Exception -> 0x0085, all -> 0x007a }
            r0 = r1
        L_0x0040:
            java.lang.String r0 = r0.readLine()     // Catch:{ Exception -> 0x004b, all -> 0x0082 }
            if (r0 == 0) goto L_0x0053
            r3.append(r0)     // Catch:{ Exception -> 0x004b, all -> 0x0082 }
            r0 = r1
            goto L_0x0040
        L_0x004b:
            r0 = move-exception
            r0 = r1
        L_0x004d:
            if (r0 == 0) goto L_0x0052
            r1.close()     // Catch:{ Exception -> 0x0089 }
        L_0x0052:
            return
        L_0x0053:
            org.json.JSONObject r0 = new org.json.JSONObject     // Catch:{ Exception -> 0x004b, all -> 0x0082 }
            org.json.JSONTokener r2 = new org.json.JSONTokener     // Catch:{ Exception -> 0x004b, all -> 0x0082 }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x004b, all -> 0x0082 }
            r2.<init>(r3)     // Catch:{ Exception -> 0x004b, all -> 0x0082 }
            r0.<init>(r2)     // Catch:{ Exception -> 0x004b, all -> 0x0082 }
            java.lang.String r2 = ",4<4"
            java.lang.String r2 = defpackage.l.c(r2)     // Catch:{ Exception -> 0x004b, all -> 0x0082 }
            java.lang.String r0 = r0.getString(r2)     // Catch:{ Exception -> 0x004b, all -> 0x0082 }
            if (r0 == 0) goto L_0x0072
            java.lang.String r2 = "AdMob SDK"
            android.util.Log.e(r2, r0)     // Catch:{ Exception -> 0x004b, all -> 0x0082 }
        L_0x0072:
            if (r1 == 0) goto L_0x0052
            r1.close()     // Catch:{ Exception -> 0x0078 }
            goto L_0x0052
        L_0x0078:
            r0 = move-exception
            goto L_0x0052
        L_0x007a:
            r0 = move-exception
            r1 = r2
        L_0x007c:
            if (r1 == 0) goto L_0x0081
            r2.close()     // Catch:{ Exception -> 0x008b }
        L_0x0081:
            throw r0
        L_0x0082:
            r0 = move-exception
            r2 = r1
            goto L_0x007c
        L_0x0085:
            r0 = move-exception
            r0 = r2
            r1 = r2
            goto L_0x004d
        L_0x0089:
            r0 = move-exception
            goto L_0x0052
        L_0x008b:
            r1 = move-exception
            goto L_0x0081
        */
        throw new UnsupportedOperationException("Method not decompiled: com.admob.android.ads.AdView.retrieveDeveloperMessage(android.content.Context):void");
    }

    public int getBackgroundColor() {
        return this.backgroundColor;
    }

    public String getKeywords() {
        return this.keywords;
    }

    public int getRequestInterval() {
        return this.requestInterval / 1000;
    }

    public String getSearchQuery() {
        return this.searchQuery;
    }

    public int getTextColor() {
        return this.textColor;
    }

    public int getVisibility() {
        if (!this.hideWhenNoAd || hasAd()) {
            return super.getVisibility();
        }
        return 8;
    }

    public boolean hasAd() {
        return this.ad != null;
    }

    public boolean isGoneWithoutAd() {
        return this.hideWhenNoAd;
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        this.isOnScreen = true;
        super.onAttachedToWindow();
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        this.isOnScreen = false;
        super.onDetachedFromWindow();
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
        setMeasuredDimension(getMeasuredWidth(), 48);
    }

    public void onWindowFocusChanged(boolean z) {
        manageRequestIntervalTimer(z);
    }

    public void requestFreshAd() {
        Context context = getContext();
        if (AdManager.getUserId(context) == null && !checkedForMessages) {
            checkedForMessages = true;
            retrieveDeveloperMessage(context);
        }
        if (super.getVisibility() != 0) {
            Log.w("AdMob SDK", l.c("\u0016);&:<u:09 -&<\u0013:0;=\t1`|h\" 0&u<=-u\t1\u001e<-\"h<;u&:<u><;<*9-{hu\u000b4$9h\u0014,\u0003!0?{;0<\u0003!&!7!9!!1}\u001e<-\"f\u0003\u0001\u0006\u0001\u0017\u0004\u0010au.<:&<{"));
        } else if (!this.requestingFreshAd) {
            this.requestingFreshAd = true;
            if (uiThreadHandler == null) {
                uiThreadHandler = new Handler();
            }
            new Thread() {
                public void run() {
                    AnonymousClass1 r0;
                    AnonymousClass1 r02;
                    final boolean z;
                    AnonymousClass1 r03;
                    try {
                        Context context = AdView.this.getContext();
                        Ad requestAd = AdRequester.requestAd(context, AdView.this.keywords, AdView.this.searchQuery);
                        if (requestAd != null) {
                            synchronized (this) {
                                if (AdView.this.ad == null || !requestAd.equals(AdView.this.ad.getAd()) || AdManager.isInTestMode()) {
                                    if (AdView.this.ad == null) {
                                        z = true;
                                        r02 = this;
                                    } else {
                                        r02 = this;
                                        z = false;
                                    }
                                    final int access$401 = AdView.super.getVisibility();
                                    final AdContainer adContainer = new AdContainer(requestAd, context);
                                    adContainer.setBackgroundColor(AdView.this.getBackgroundColor());
                                    adContainer.setTextColor(AdView.this.getTextColor());
                                    adContainer.setVisibility(access$401);
                                    adContainer.setLayoutParams(new RelativeLayout.LayoutParams(-1, 48));
                                    if (AdView.this.listener != null) {
                                        try {
                                            AdView.this.listener.onNewAd();
                                            AdView.this.listener.onReceiveAd(AdView.this);
                                        } catch (Exception e) {
                                            Log.w("AdMob SDK", i.c("\u0013\u0019.\u0016(\u0013*\u0012\"W#\u000f%\u00126\u0003/\u0018(W4\u0016/\u0004#\u0013f\u001e(W?\u00183\u0005f6\";/\u00042\u0012(\u00124Y)\u0019\u0014\u0012%\u0012/\u0001#6\"Y"), e);
                                        }
                                    }
                                    AdView.uiThreadHandler.post(new Runnable() {
                                        public void run() {
                                            try {
                                                AdView.this.addView(adContainer);
                                                if (access$401 != 0) {
                                                    AdContainer unused = AdView.this.ad = adContainer;
                                                } else if (z) {
                                                    AdView.this.applyFadeIn(adContainer);
                                                } else {
                                                    AdView.this.applyRotation(adContainer);
                                                }
                                            } catch (Exception e) {
                                                Log.e("AdMob SDK", R.c("5.\b!\u000e$\f%\u0004`\u00058\u0003%\u00104\t/\u000e`\u0010,\u0001#\t.\u0007`!$#/\u000e4\u0001)\u000e%\u0012`\t.\u0014/@\u0001\u0004\u0016\t%\u0017n"), e);
                                            } finally {
                                                boolean unused2 = AdView.this.requestingFreshAd = false;
                                            }
                                        }
                                    });
                                    r03 = this;
                                } else {
                                    if (Log.isLoggable("AdMob SDK", 3)) {
                                        Log.d("AdMob SDK", i.c("\u0014\u0012%\u0012/\u0001#\u0013f\u0003.\u0012f\u0004'\u001a#W'\u0013f\u0000#W'\u001b4\u0012'\u0013?W.\u0016\"YfW\u0002\u001e5\u0014'\u0005\"\u001e(\u0010f\u001e2Y"));
                                    }
                                    boolean unused = AdView.this.requestingFreshAd = false;
                                    r03 = this;
                                }
                                return;
                            }
                        }
                        if (AdView.this.listener != null) {
                            try {
                                AdView.this.listener.onFailedToReceiveAd(AdView.this);
                            } catch (Exception e2) {
                                Log.w("AdMob SDK", j.c(";8\u00067\u00002\u00023\nv\u000b.\r3\u001e\"\u00079\u0000v\u001c7\u0007%\u000b2N?\u0000v\u00179\u001b$N\u0017\n\u001a\u0007%\u001a3\u00003\u001cx\u00018(7\u0007:\u000b2:9<3\r3\u0007 \u000b\u0017\nx"), e2);
                                r0 = this;
                            }
                        }
                        r0 = this;
                        boolean unused2 = AdView.this.requestingFreshAd = false;
                        return;
                    } catch (Exception e3) {
                        Log.e("AdMob SDK", j.c(";8\u00067\u00002\u00023\nv\u000b.\r3\u001e\"\u00079\u0000v\u001c3\u001f#\u000b%\u001a?\u00001N7N0\u001c3\u001d>N7\nx"), e3);
                        boolean unused3 = AdView.this.requestingFreshAd = false;
                    }
                }
            }.start();
        } else if (Log.isLoggable("AdMob SDK", 3)) {
            Log.d("AdMob SDK", i.c("\u000f\u0010(\u00184\u001e(\u0010f\u0005#\u00063\u00125\u0003\u0000\u0005#\u0004.6\"_oW$\u0012%\u00163\u0004#W1\u0012f\u00164\u0012f\u0016*\u0005#\u0016\"\u000ef\u0010#\u00032\u001e(\u0010f\u0016f\u00114\u00125\u001ff\u0016\"Y"));
        }
    }

    public void setBackgroundColor(int i) {
        this.backgroundColor = -16777216 | i;
        if (this.ad != null) {
            this.ad.setBackgroundColor(i);
        }
        invalidate();
    }

    public void setGoneWithoutAd(boolean z) {
        this.hideWhenNoAd = z;
    }

    public void setKeywords(String str) {
        this.keywords = str;
    }

    public void setListener(AdListener adListener) {
        synchronized (this) {
            this.listener = adListener;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:0x006b  */
    /* JADX WARNING: Removed duplicated region for block: B:6:0x000f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void setRequestInterval(int r6) {
        /*
            r5 = this;
            r4 = 600(0x258, float:8.41E-43)
            r3 = 15
            r0 = 0
            if (r6 > 0) goto L_0x0013
            r6 = r0
        L_0x0008:
            r1 = r5
        L_0x0009:
            int r2 = r6 * 1000
            r1.requestInterval = r2
            if (r6 != 0) goto L_0x006b
            r5.manageRequestIntervalTimer(r0)
        L_0x0012:
            return
        L_0x0013:
            if (r6 >= r3) goto L_0x003f
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "\t1\u001e<-\"f&-!\u001a09 -&<\u001c&!-'>4$}"
            java.lang.String r2 = defpackage.l.c(r2)
            java.lang.StringBuilder r1 = r1.insert(r0, r2)
            java.lang.StringBuilder r1 = r1.append(r6)
            java.lang.String r2 = "^f\u0004#\u0014)\u0019\"\u0004f\u001a3\u00042W$\u0012fI{W"
            java.lang.String r2 = defpackage.i.c(r2)
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r3)
            java.lang.String r1 = r1.toString()
            com.admob.android.ads.AdManager.clientError(r1)
            r1 = r5
            goto L_0x0009
        L_0x003f:
            if (r6 <= r4) goto L_0x0008
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "\t1\u001e<-\"f&-!\u001a09 -&<\u001c&!-'>4$}"
            java.lang.String r2 = defpackage.l.c(r2)
            java.lang.StringBuilder r1 = r1.insert(r0, r2)
            java.lang.StringBuilder r1 = r1.append(r6)
            java.lang.String r2 = "^f\u0004#\u0014)\u0019\"\u0004f\u001a3\u00042W$\u0012fK{W"
            java.lang.String r2 = defpackage.i.c(r2)
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r4)
            java.lang.String r1 = r1.toString()
            com.admob.android.ads.AdManager.clientError(r1)
            r1 = r5
            goto L_0x0009
        L_0x006b:
            java.lang.String r1 = "AdMob SDK"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "\u0007-$=0;!!;/u.'-& u)1;u-#-'1u"
            java.lang.String r3 = defpackage.l.c(r3)
            java.lang.StringBuilder r0 = r2.insert(r0, r3)
            java.lang.StringBuilder r0 = r0.append(r6)
            java.lang.String r2 = "W5\u0012%\u0018(\u00135Y"
            java.lang.String r2 = defpackage.i.c(r2)
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.String r0 = r0.toString()
            android.util.Log.i(r1, r0)
            r0 = 1
            r5.manageRequestIntervalTimer(r0)
            goto L_0x0012
        */
        throw new UnsupportedOperationException("Method not decompiled: com.admob.android.ads.AdView.setRequestInterval(int):void");
    }

    public void setSearchQuery(String str) {
        this.searchQuery = str;
    }

    public void setTextColor(int i) {
        this.textColor = -16777216 | i;
        if (this.ad != null) {
            this.ad.setTextColor(i);
        }
        invalidate();
    }

    public void setVisibility(int i) {
        AdView adView;
        if (super.getVisibility() != i) {
            synchronized (this) {
                int childCount = getChildCount();
                int i2 = 0;
                int i3 = 0;
                while (i2 < childCount) {
                    getChildAt(i3).setVisibility(i);
                    i2 = i3 + 1;
                    i3 = i2;
                }
                super.setVisibility(i);
                if (i == 0) {
                    requestFreshAd();
                    adView = this;
                } else {
                    removeView(this.ad);
                    this.ad = null;
                    invalidate();
                    adView = this;
                }
            }
        }
    }
}
