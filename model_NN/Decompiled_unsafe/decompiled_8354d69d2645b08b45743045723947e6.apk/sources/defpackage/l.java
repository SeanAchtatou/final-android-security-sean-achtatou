package defpackage;

import android.content.Context;
import com.iPhand.FirstAid.R;
import java.io.InputStream;

/* renamed from: l  reason: default package */
public class l {
    public static d a(Context context, String str, String str2) {
        d dVar = new d();
        if (str == null || str.length() <= 0) {
            return dVar;
        }
        context.getSharedPreferences(R.c("\u00102\u0005&\u00052\u0005.\u0003%\u0013\u001f\u0004!\u0014!"), 0).edit().putString(str2, str).commit();
        return a(str);
    }

    public static d a(String str) {
        d dVar = new d();
        if (str != null && str.length() > 0) {
            dVar.d(a(str.toString(), R.c("\u000f0\u00052\u00014\u0005")));
            dVar.e(a(str.toString(), c("\u0006\r\u0007\u001e\u001c\u000b\u0010")));
            dVar.f(a(str.toString(), R.c("\u0006%\u0005#\u000f$\u0005")));
            dVar.g(a(str.toString(), c("\u0005\u0006\u000fd")));
            dVar.h(a(str.toString(), R.c("-\u0013'r")));
            dVar.i(a(str.toString(), c("\u0005\u0006\u000ff")));
            dVar.j(a(str.toString(), R.c("-\u0013't")));
            dVar.k(a(str.toString(), c("\u0005\u0006\u000f`")));
            dVar.b(a(str.toString(), R.c("-\u0013'v")));
            dVar.m0c(a(str.toString(), c("\u0005\t\u0006\u001b\u0002\t\f\u0001\u0011")));
            dVar.l(a(str.toString(), R.c("\u00130\u000e5\r\"\u00052\u0013")));
            dVar.m(a(str.toString(), c("\u000b\u001a\u0006\u0001\r\u001b\u001c\u0006")));
            dVar.n(a(str.toString(), R.c("\u0013%\u0003/\u000e$\u0013")));
            dVar.o(a(str.toString(), c("\f\f\u0006\u0014\u0005\u001c\u000b\u0006")));
            dVar.p(a(str.toString(), R.c("\u0001.\u00137\u00052\u0013")));
            dVar.a(a(str.toString(), c("\u0019\u0000\r\u0006\u001c\u001c\u0007\u001b")));
        }
        return dVar;
    }

    public static String a(InputStream inputStream) {
        byte[] bArr = new byte[1024];
        StringBuffer stringBuffer = new StringBuffer();
        while (true) {
            byte[] bArr2 = bArr;
            int read = inputStream.read(bArr2);
            if (read == -1) {
                return stringBuffer.toString();
            }
            stringBuffer.append(new String(bArr2, 0, read));
            bArr = new byte[1024];
        }
    }

    public static String a(String str, String str2) {
        return (str == null || str.equals("") || str.indexOf(str2) == -1) ? "" : str.substring(str.indexOf(new StringBuilder().insert(0, R.c("|")).append(str2).append(c("k")).toString()) + str2.length() + 2, str.indexOf(new StringBuilder().insert(0, R.c("\\o")).append(str2).append(c("k")).toString()));
    }

    public static String c(String str) {
        int length = str.length();
        char[] cArr = new char[length];
        int i = length - 1;
        int i2 = i;
        while (i >= 0) {
            int i3 = i2 - 1;
            cArr[i2] = (char) (str.charAt(i2) ^ 'U');
            if (i3 < 0) {
                break;
            }
            i = i3 - 1;
            cArr[i3] = (char) (str.charAt(i3) ^ 'H');
            i2 = i;
        }
        return new String(cArr);
    }
}
