package defpackage;

import android.util.Xml;
import com.iPhand.FirstAid.R;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.xmlpull.v1.XmlPullParser;

/* renamed from: g  reason: default package */
public class g {
    public static boolean b = true;
    byte[] a = null;
    private String c = "";
    private String d = "";
    private String e = "";
    private int f = 0;
    private String g = "";
    private HttpURLConnection h = null;
    private OutputStream i;
    private InputStream j;
    private final String k = "10.0.0.172:80";
    private final String l = "http://";
    private String m;
    private String n;

    public g() {
    }

    public g(String str) {
        a(str, "", R.c("\u0007%\u0014"), 0);
    }

    public g(String str, String str2) {
        a(str, "", str2, 0);
    }

    public g(String str, String str2, int i2) {
        a(str, "", str2, i2);
    }

    private /* synthetic */ void a(String str, String str2, String str3, int i2) {
        try {
            this.c = str;
            this.d = str3;
            this.e = str2;
            this.f = i2;
            if (str != null) {
                String[] d2 = d(str);
                this.m = d2[0];
                this.n = d2[1];
                if (b) {
                    this.h = (HttpURLConnection) new URL(new StringBuilder().insert(0, R.c("\b4\u00140ZoOqPnPnPnQwRzXp")).append(this.n).toString()).openConnection();
                    this.h.setRequestProperty(i.c("/k8(\u001b/\u0019#Z\u000e\u00185\u0003"), this.m);
                } else {
                    this.h = (HttpURLConnection) new URL(str).openConnection();
                }
                this.h.setConnectTimeout(50000);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    private /* synthetic */ void b() {
        String headerField = this.h.getHeaderField(i.c("%#\u00043\u001b2Z\u0005\u0018\"\u0012"));
        if (headerField == null) {
            headerField = "";
        }
        a.l = headerField;
        String headerField2 = this.h.getHeaderField(R.c("#/\u000e4\u0005.\u0014m,%\u000e'\u0014("));
        if (headerField2 == null) {
            headerField2 = "";
        }
        a.m = headerField2;
        String headerField3 = this.h.getHeaderField(i.c("\u0015\u00122Z\u0005\u0018)\u001c/\u0012"));
        if (headerField3 == null) {
            headerField3 = "";
        }
        a.k = headerField3;
        Iterator<Map.Entry<String, List<String>>> it = this.h.getHeaderFields().entrySet().iterator();
        a.j = new StringBuffer();
        while (it.hasNext()) {
            Map.Entry next = it.next();
            Object key = next.getKey();
            a.j.append(key + R.c("z") + next.getValue() + i.c("i\u0019"));
        }
    }

    private /* synthetic */ void d(byte[] bArr) {
        if (bArr != null && bArr.length != 0) {
            this.i = this.h.getOutputStream();
            this.i.write(bArr);
            this.i.flush();
            this.i.close();
        }
    }

    private /* synthetic */ String[] d(String str) {
        String[] strArr = new String[2];
        int length = "http://".length();
        if (str.toLowerCase().indexOf("http://") == -1) {
            return null;
        }
        int indexOf = str.indexOf(R.c("o"), length);
        if (indexOf == -1) {
            strArr[0] = str.substring(length, str.length());
            strArr[1] = i.c("X");
            return strArr;
        }
        strArr[0] = str.substring(length, indexOf);
        strArr[1] = str.substring(indexOf);
        return strArr;
    }

    private /* synthetic */ void e(byte[] bArr) {
        int i2 = 0;
        try {
            this.h.setDoOutput(true);
            this.h.setDoInput(true);
            this.h.setRequestMethod(this.d);
            HashMap hashMap = new HashMap();
            hashMap.put(R.c("!#\u0003%\u00104M\u0003\b!\u00123\u00054"), i.c("3\u0003 Z~[f\u001e5\u0018kO~BZw[f\u00022\u0011kFp[f]}\u0006{Gh@"));
            hashMap.put(R.c("!#\u0003%\u00104"), i.c("'\u00076\u001b/\u0014'\u0003/\u0018(X>\u001a*['\u00076\u001b/\u0014'\u0003/\u0018(X>\u001f2\u001a*\\>\u001a*[2\u0012>\u0003i\u001f2\u001a*L7JvY[2\u0012>\u0003i\u0007*\u0016/\u0019}\u0006{GhOj\u001e+\u0016!\u0012i\u0007(\u0010j]i]}\u0006{GhB"));
            hashMap.put(R.c("\u0001\u0003#\u00050\u0014m,!\u000e'\u0015!\u0007%"), i.c("<\u001fk4\b[f\u0012(Z\u0013$"));
            hashMap.put(R.c("\u0003\u0001#\b%M\u0003\u000f.\u00142\u000f,"), i.c("(\u0018k\u00042\u00184\u0012"));
            hashMap.put(R.c("#/\u000e.\u0005#\u0014)\u000f."), i.c("\r\u0012#\u0007k6*\u001e0\u0012"));
            hashMap.put(R.c("02\u000f8\u0019m#/\u000e.\u0005#\u0014)\u000f."), i.c("\r\u0012#\u0007k6*\u001e0\u0012"));
            if (this.g != null && !this.g.equals("")) {
                if (this.g.contains(R.c("c"))) {
                    String[] split = this.g.split(i.c("T"));
                    int length = split.length;
                    int i3 = 0;
                    while (i2 < length) {
                        String[] split2 = split[i3].split(R.c("z"));
                        hashMap.put(split2[0], split2[1]);
                        i2 = i3 + 1;
                        i3 = i2;
                    }
                } else if (this.g.contains(i.c("M"))) {
                    String[] split3 = this.g.split(R.c("z"));
                    hashMap.put(split3[0], split3[1]);
                }
            }
            Iterator it = hashMap.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry entry = (Map.Entry) it.next();
                this.h.setRequestProperty((String) entry.getKey(), (String) entry.getValue());
            }
            if (!a.i.equals("")) {
                this.h.setRequestProperty(i.c("\u0013\u0004#\u0005k6!\u0012(\u0003"), a.i);
            }
            if (a.k.equals("") || !this.c.contains(R.c("\u000f0\u0003/\u0004%"))) {
            }
            if (this.e != null && !this.e.equals("")) {
                this.h.setRequestProperty(i.c("\u0007\u00142\u001e)\u0019"), this.e);
                this.h.setRequestProperty(R.c("#,\t%\u000e4(!\u0013("), a.h);
                this.h.setRequestProperty(i.c("!#\u00055\u001e)\u0019"), a.d);
                this.h.setRequestProperty(R.c("\u0015\u0013%\u0012m)$"), a.f != null ? a.f : "");
                this.h.setRequestProperty(i.c("\u0007'\u000f!#\u00055\u001e)\u0019"), R.c("qNqNp"));
                this.h.setRequestProperty(i.c(">Z3\u0007k\u0014'\u001b*\u001e(\u0010k\u001b/\u0019#Z/\u0013"), R.c("qSxVwRrRtRt"));
            }
            if (this.d.equals(i.c("\u00168\u0015#"))) {
                if (bArr == null || bArr.length == 0) {
                    this.h.setRequestProperty(i.c("\u0005\u0018(\u0003#\u00192Z*\u0012(\u00102\u001f"), R.c("p"));
                } else {
                    this.h.setRequestProperty(R.c("#/\u000e4\u0005.\u0014m49\u0010%"), i.c("\u00166\u0007*\u001e%\u00162\u001e)\u0019i\u000fk\u00001\u0000k\u0011)\u0005+Z3\u0005*\u0012(\u0014)\u0013#\u0013"));
                    this.h.setRequestProperty(R.c("#/\u000e4\u0005.\u0014m\f%\u000e'\u0014("), bArr.length + "");
                }
            }
            Iterator<Map.Entry<String, List<String>>> it2 = this.h.getRequestProperties().entrySet().iterator();
            for (Iterator<Map.Entry<String, List<String>>> it3 = it2; it3.hasNext(); it3 = it2) {
                Map.Entry next = it2.next();
                next.getKey();
                next.getValue();
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public InputStream a(byte[] bArr) {
        e(bArr);
        if (!(bArr == null || bArr.length == 0)) {
            d(bArr);
        }
        this.j = this.h.getInputStream();
        b();
        return this.j;
    }

    public void a() {
        if (this.h != null) {
            this.h.disconnect();
            this.h = null;
        }
        if (this.j != null) {
            this.j.close();
            this.j = null;
        }
        if (this.i != null) {
            this.i.close();
            this.i = null;
        }
    }

    public byte[] a(InputStream inputStream) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        if (this.f <= 0) {
            byte[] bArr = new byte[1024];
            while (true) {
                int read = inputStream.read(bArr);
                if (read == -1) {
                    break;
                }
                byteArrayOutputStream.write(bArr, 0, read);
            }
        } else {
            byte[] bArr2 = new byte[1024];
            int i2 = 1;
            while (true) {
                int i3 = i2;
                int read2 = inputStream.read(bArr2);
                if (read2 == -1 || this.f < i3) {
                    break;
                }
                byteArrayOutputStream.write(bArr2, 0, read2);
                i2 = i3 + 1;
            }
        }
        byteArrayOutputStream.flush();
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        byteArrayOutputStream.close();
        return byteArray;
    }

    public byte[] a(String str) {
        return b(str.getBytes());
    }

    public String b(String str) {
        return new String(b(str.getBytes()));
    }

    public byte[] b(byte[] bArr) {
        byte[] a2 = a(a(bArr));
        a();
        return a2;
    }

    public List c(byte[] bArr) {
        XmlPullParser xmlPullParser;
        ArrayList arrayList;
        e eVar;
        InputStream a2 = a(bArr);
        if (a.m == null || a.m.equals("") || a.m.equals(i.c("G"))) {
            return null;
        }
        XmlPullParser newPullParser = Xml.newPullParser();
        newPullParser.setInput(a2, R.c("\u00154\u0006Mx"));
        int eventType = newPullParser.getEventType();
        int i2 = eventType;
        int i3 = eventType;
        ArrayList arrayList2 = null;
        e eVar2 = null;
        while (i2 != 1) {
            switch (i3) {
                case R.styleable.com_admob_android_ads_AdView_testing:
                    arrayList = new ArrayList();
                    eVar = eVar2;
                    xmlPullParser = newPullParser;
                    continue;
                    int next = xmlPullParser.next();
                    i3 = next;
                    int i4 = next;
                    eVar2 = eVar;
                    arrayList2 = arrayList;
                    i2 = i4;
                case R.styleable.com_admob_android_ads_AdView_textColor:
                    if (i.c("\u0005#\u00063\u00125\u0003").equalsIgnoreCase(newPullParser.getName())) {
                        eVar2 = new e();
                    }
                    if (eVar2 != null) {
                        if (R.c("5\u0012,").equalsIgnoreCase(newPullParser.getName())) {
                            eVar2.b(newPullParser.nextText());
                        } else if (R.c("\r%\u0014(\u000f$").equalsIgnoreCase(newPullParser.getName())) {
                            eVar2.c(newPullParser.nextText());
                        } else if (i.c("\"\u00162\u0016").equalsIgnoreCase(newPullParser.getName())) {
                            eVar2.d(newPullParser.nextText());
                        } else if (R.c("\t32%\u00145\u0012.").equalsIgnoreCase(newPullParser.getName())) {
                            eVar2.e(newPullParser.nextText());
                        } else if (i.c("\"\u00181\u0019\u0015\u001e<\u0012").equalsIgnoreCase(newPullParser.getName())) {
                            eVar2.f(newPullParser.nextText());
                        } else if (R.c("\u0012%\u00115\u00053\u0014\u0010\u0012/\u0010%\u00124\u00193").equalsIgnoreCase(newPullParser.getName())) {
                            eVar2.a(newPullParser.nextText());
                        }
                    }
                    if (i.c(")\u0007%\u0018\"\u0012").equalsIgnoreCase(newPullParser.getName())) {
                        e.a = newPullParser.nextText();
                        arrayList = arrayList2;
                        eVar = eVar2;
                        xmlPullParser = newPullParser;
                        continue;
                    } else if (i.c("4\u00122\u00024\u0019\u0013\u0005*\u0004").equalsIgnoreCase(newPullParser.getName())) {
                        e.b = newPullParser.nextText();
                        arrayList = arrayList2;
                        eVar = eVar2;
                        xmlPullParser = newPullParser;
                    } else if (R.c("\u0012%\u00145\u0012.-3\u00073").equalsIgnoreCase(newPullParser.getName())) {
                        e.c = newPullParser.nextText();
                        arrayList = arrayList2;
                        eVar = eVar2;
                        xmlPullParser = newPullParser;
                    } else if (i.c("5\u00127\u0002#\u0019%\u0012").equalsIgnoreCase(newPullParser.getName())) {
                        e.d = newPullParser.nextText();
                        arrayList = arrayList2;
                        eVar = eVar2;
                        xmlPullParser = newPullParser;
                    } else if (R.c("\u00139\u000e#").equalsIgnoreCase(newPullParser.getName())) {
                        e.e = newPullParser.nextText();
                        arrayList = arrayList2;
                        eVar = eVar2;
                        xmlPullParser = newPullParser;
                    }
                    int next2 = xmlPullParser.next();
                    i3 = next2;
                    int i42 = next2;
                    eVar2 = eVar;
                    arrayList2 = arrayList;
                    i2 = i42;
                    break;
                case R.styleable.com_admob_android_ads_AdView_keywords:
                    if (i.c("\u0005#\u00063\u00125\u0003").equalsIgnoreCase(newPullParser.getName())) {
                        arrayList2.add(eVar2);
                        xmlPullParser = newPullParser;
                        arrayList = arrayList2;
                        eVar = null;
                        continue;
                        int next22 = xmlPullParser.next();
                        i3 = next22;
                        int i422 = next22;
                        eVar2 = eVar;
                        arrayList2 = arrayList;
                        i2 = i422;
                    }
                    break;
            }
            arrayList = arrayList2;
            eVar = eVar2;
            xmlPullParser = newPullParser;
            int next222 = xmlPullParser.next();
            i3 = next222;
            int i4222 = next222;
            eVar2 = eVar;
            arrayList2 = arrayList;
            i2 = i4222;
        }
        return arrayList2;
    }

    public void c(String str) {
        this.g = str;
    }
}
