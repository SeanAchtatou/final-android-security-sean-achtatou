package defpackage;

import android.content.Context;
import android.content.SharedPreferences;

/* renamed from: c  reason: default package */
public class c {
    static c b = null;
    public static String c = "endCallSum";
    final String a = "shared_pre_data";
    private SharedPreferences d = null;

    public c(Context context) {
        this.d = context.getSharedPreferences("shared_pre_data", 0);
    }

    public static c a(Context context) {
        if (b == null) {
            b = new c(context);
        }
        return b;
    }

    public String a(String str) {
        return this.d.getString(str, "");
    }

    public void a(String str, int i) {
        this.d.edit().putInt(str, i).commit();
    }

    public void a(String str, String str2) {
        this.d.edit().putString(str, str2).commit();
    }

    public int b(String str) {
        return this.d.getInt(str, 1);
    }
}
