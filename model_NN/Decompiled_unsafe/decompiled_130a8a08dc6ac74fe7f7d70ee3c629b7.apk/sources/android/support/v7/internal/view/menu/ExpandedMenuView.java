package android.support.v7.internal.view.menu;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

public final class ExpandedMenuView extends ListView implements p, w, AdapterView.OnItemClickListener {

    /* renamed from: a  reason: collision with root package name */
    private n f20a;
    private int b;

    public ExpandedMenuView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        setOnItemClickListener(this);
    }

    public void a(n nVar) {
        this.f20a = nVar;
    }

    /* JADX WARN: Type inference failed for: r3v0, types: [android.support.v7.internal.view.menu.r, android.view.MenuItem] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean a(android.support.v7.internal.view.menu.r r3) {
        /*
            r2 = this;
            android.support.v7.internal.view.menu.n r0 = r2.f20a
            r1 = 0
            boolean r0 = r0.a(r3, r1)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.internal.view.menu.ExpandedMenuView.a(android.support.v7.internal.view.menu.r):boolean");
    }

    public int getWindowAnimations() {
        return this.b;
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        setChildrenDrawingCacheEnabled(false);
    }

    public void onItemClick(AdapterView adapterView, View view, int i, long j) {
        a((r) getAdapter().getItem(i));
    }
}
