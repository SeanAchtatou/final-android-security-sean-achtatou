package android.support.v7.internal.view.menu;

import android.content.Context;
import android.content.res.Resources;
import android.support.v7.b.b;
import android.support.v7.b.d;
import android.support.v7.b.g;
import android.support.v7.internal.widget.t;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ListAdapter;
import android.widget.PopupWindow;

public class s implements u, View.OnKeyListener, ViewTreeObserver.OnGlobalLayoutListener, AdapterView.OnItemClickListener, PopupWindow.OnDismissListener {
    static final int b = g.abc_popup_menu_item_layout;

    /* renamed from: a  reason: collision with root package name */
    private Context f36a;
    boolean c;
    /* access modifiers changed from: private */
    public LayoutInflater d;
    private t e;
    /* access modifiers changed from: private */
    public n f;
    private int g;
    private View h;
    /* access modifiers changed from: private */
    public boolean i;
    private ViewTreeObserver j;
    private t k;
    private v l;
    private ViewGroup m;

    public s(Context context, n nVar, View view, boolean z) {
        this.f36a = context;
        this.d = LayoutInflater.from(context);
        this.f = nVar;
        this.i = z;
        Resources resources = context.getResources();
        this.g = Math.max(resources.getDisplayMetrics().widthPixels / 2, resources.getDimensionPixelSize(d.abc_config_prefDialogWidth));
        this.h = view;
        nVar.a(this);
    }

    private int a(ListAdapter listAdapter) {
        View view;
        int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, 0);
        int makeMeasureSpec2 = View.MeasureSpec.makeMeasureSpec(0, 0);
        int count = listAdapter.getCount();
        int i2 = 0;
        int i3 = 0;
        View view2 = null;
        int i4 = 0;
        while (i2 < count) {
            int itemViewType = listAdapter.getItemViewType(i2);
            if (itemViewType != i3) {
                view = null;
            } else {
                itemViewType = i3;
                view = view2;
            }
            if (this.m == null) {
                this.m = new FrameLayout(this.f36a);
            }
            view2 = listAdapter.getView(i2, view, this.m);
            view2.measure(makeMeasureSpec, makeMeasureSpec2);
            i4 = Math.max(i4, view2.getMeasuredWidth());
            i2++;
            i3 = itemViewType;
        }
        return i4;
    }

    public void a(Context context, n nVar) {
    }

    public void a(n nVar, boolean z) {
        if (nVar == this.f) {
            b();
            if (this.l != null) {
                this.l.a(nVar, z);
            }
        }
    }

    public void a(v vVar) {
        this.l = vVar;
    }

    public void a(boolean z) {
        this.c = z;
    }

    public boolean a() {
        boolean z = false;
        this.e = new t(this.f36a, null, b.popupMenuStyle);
        this.e.a((PopupWindow.OnDismissListener) this);
        this.e.a((AdapterView.OnItemClickListener) this);
        this.k = new t(this, this.f);
        this.e.a(this.k);
        this.e.a(true);
        View view = this.h;
        if (view == null) {
            return false;
        }
        if (this.j == null) {
            z = true;
        }
        this.j = view.getViewTreeObserver();
        if (z) {
            this.j.addOnGlobalLayoutListener(this);
        }
        this.e.a(view);
        this.e.e(Math.min(a(this.k), this.g));
        this.e.f(2);
        this.e.c();
        this.e.h().setOnKeyListener(this);
        return true;
    }

    public boolean a(n nVar, r rVar) {
        return false;
    }

    public boolean a(y yVar) {
        boolean z;
        if (yVar.hasVisibleItems()) {
            s sVar = new s(this.f36a, yVar, this.h, false);
            sVar.a(this.l);
            int size = yVar.size();
            int i2 = 0;
            while (true) {
                if (i2 >= size) {
                    z = false;
                    break;
                }
                MenuItem item = yVar.getItem(i2);
                if (item.isVisible() && item.getIcon() != null) {
                    z = true;
                    break;
                }
                i2++;
            }
            sVar.a(z);
            if (sVar.a()) {
                if (this.l == null) {
                    return true;
                }
                this.l.a(yVar);
                return true;
            }
        }
        return false;
    }

    public void b() {
        if (c()) {
            this.e.d();
        }
    }

    public void b(boolean z) {
        if (this.k != null) {
            this.k.notifyDataSetChanged();
        }
    }

    public boolean b(n nVar, r rVar) {
        return false;
    }

    public boolean c() {
        return this.e != null && this.e.f();
    }

    public boolean f() {
        return false;
    }

    public void onDismiss() {
        this.e = null;
        this.f.close();
        if (this.j != null) {
            if (!this.j.isAlive()) {
                this.j = this.h.getViewTreeObserver();
            }
            this.j.removeGlobalOnLayoutListener(this);
            this.j = null;
        }
    }

    public void onGlobalLayout() {
        if (c()) {
            View view = this.h;
            if (view == null || !view.isShown()) {
                b();
            } else if (c()) {
                this.e.c();
            }
        }
    }

    /* JADX WARN: Type inference failed for: r0v1, types: [android.support.v7.internal.view.menu.r, android.view.MenuItem] */
    public void onItemClick(AdapterView adapterView, View view, int i2, long j2) {
        t tVar = this.k;
        tVar.b.a((MenuItem) tVar.getItem(i2), 0);
    }

    public boolean onKey(View view, int i2, KeyEvent keyEvent) {
        if (keyEvent.getAction() != 1 || i2 != 82) {
            return false;
        }
        b();
        return true;
    }
}
