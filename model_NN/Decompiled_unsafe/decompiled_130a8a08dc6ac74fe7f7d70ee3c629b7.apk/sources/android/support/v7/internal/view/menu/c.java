package android.support.v7.internal.view.menu;

import android.content.DialogInterface;

class c extends q {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ActionMenuPresenter f24a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public c(ActionMenuPresenter actionMenuPresenter, y yVar) {
        super(yVar);
        this.f24a = actionMenuPresenter;
        actionMenuPresenter.a(actionMenuPresenter.f17a);
    }

    public void onDismiss(DialogInterface dialogInterface) {
        super.onDismiss(dialogInterface);
        c unused = this.f24a.w = (c) null;
        this.f24a.b = 0;
    }
}
