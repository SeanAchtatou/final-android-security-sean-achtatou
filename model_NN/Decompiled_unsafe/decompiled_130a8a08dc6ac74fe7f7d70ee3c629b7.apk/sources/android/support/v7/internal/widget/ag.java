package android.support.v7.internal.widget;

import android.view.View;

class ag implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ View f54a;
    final /* synthetic */ ScrollingTabContainerView b;

    ag(ScrollingTabContainerView scrollingTabContainerView, View view) {
        this.b = scrollingTabContainerView;
        this.f54a = view;
    }

    public void run() {
        this.b.smoothScrollTo(this.f54a.getLeft() - ((this.b.getWidth() - this.f54a.getWidth()) / 2), 0);
        this.b.f48a = null;
    }
}
