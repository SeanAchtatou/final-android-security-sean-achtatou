package android.support.v7.internal.view.menu;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.view.ActionProvider;
import android.support.v7.b.f;
import android.support.v7.internal.view.a;
import android.util.SparseBooleanArray;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;

public class ActionMenuPresenter extends k implements ActionProvider.SubUiVisibilityListener {

    /* renamed from: a  reason: collision with root package name */
    final g f17a;
    int b;
    private View i;
    private boolean j;
    private boolean k;
    private int l;
    private int m;
    private int n;
    private boolean o;
    private boolean p;
    private boolean q;
    private boolean r;
    private int s;
    private final SparseBooleanArray t;
    private View u;
    /* access modifiers changed from: private */
    public f v;
    /* access modifiers changed from: private */
    public c w;
    /* access modifiers changed from: private */
    public d x;

    class SavedState implements Parcelable {
        public static final Parcelable.Creator CREATOR = new h();

        /* renamed from: a  reason: collision with root package name */
        public int f18a;

        SavedState() {
        }

        SavedState(Parcel parcel) {
            this.f18a = parcel.readInt();
        }

        public int describeContents() {
            return 0;
        }

        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeInt(this.f18a);
        }
    }

    private View a(MenuItem menuItem) {
        ViewGroup viewGroup = (ViewGroup) this.h;
        if (viewGroup == null) {
            return null;
        }
        int childCount = viewGroup.getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            View childAt = viewGroup.getChildAt(i2);
            if ((childAt instanceof x) && ((x) childAt).getItemData() == menuItem) {
                return childAt;
            }
        }
        return null;
    }

    public w a(ViewGroup viewGroup) {
        w a2 = super.a(viewGroup);
        ((ActionMenuView) a2).setPresenter(this);
        return a2;
    }

    public View a(r rVar, View view, ViewGroup viewGroup) {
        View z = rVar.z();
        if (z == null || rVar.D()) {
            if (!(view instanceof ActionMenuItemView)) {
                view = null;
            }
            z = super.a(rVar, view, viewGroup);
        }
        z.setVisibility(rVar.E() ? 8 : 0);
        ActionMenuView actionMenuView = (ActionMenuView) viewGroup;
        ViewGroup.LayoutParams layoutParams = z.getLayoutParams();
        if (!actionMenuView.checkLayoutParams(layoutParams)) {
            z.setLayoutParams(actionMenuView.generateLayoutParams(layoutParams));
        }
        return z;
    }

    public void a(int i2) {
        this.n = i2;
        this.o = true;
    }

    public void a(int i2, boolean z) {
        this.l = i2;
        this.p = z;
        this.q = true;
    }

    public void a(Context context, n nVar) {
        super.a(context, nVar);
        Resources resources = context.getResources();
        a a2 = a.a(context);
        if (!this.k) {
            this.j = a2.b();
        }
        if (!this.q) {
            this.l = a2.c();
        }
        if (!this.o) {
            this.n = a2.a();
        }
        int i2 = this.l;
        if (this.j) {
            if (this.i == null) {
                this.i = new e(this, this.c);
                int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, 0);
                this.i.measure(makeMeasureSpec, makeMeasureSpec);
            }
            i2 -= this.i.getMeasuredWidth();
        } else {
            this.i = null;
        }
        this.m = i2;
        this.s = (int) (56.0f * resources.getDisplayMetrics().density);
        this.u = null;
    }

    public void a(Configuration configuration) {
        if (!this.o) {
            this.n = this.d.getResources().getInteger(f.abc_max_action_buttons);
        }
        if (this.e != null) {
            this.e.b(true);
        }
    }

    public void a(n nVar, boolean z) {
        c();
        super.a(nVar, z);
    }

    public void a(r rVar, x xVar) {
        xVar.a(rVar, 0);
        ((ActionMenuItemView) xVar).setItemInvoker((ActionMenuView) this.h);
    }

    public void a(boolean z) {
        this.r = z;
    }

    public boolean a() {
        if (!this.j || e() || this.e == null || this.h == null || this.x != null) {
            return false;
        }
        this.x = new d(this, new f(this, this.d, this.e, this.i, true));
        ((View) this.h).post(this.x);
        super.a((y) null);
        return true;
    }

    public boolean a(int i2, r rVar) {
        return rVar.v();
    }

    public boolean a(y yVar) {
        if (!yVar.hasVisibleItems()) {
            return false;
        }
        y yVar2 = yVar;
        while (yVar2.r() != this.e) {
            yVar2 = (y) yVar2.r();
        }
        if (a(yVar2.getItem()) == null) {
            if (this.i == null) {
                return false;
            }
            View view = this.i;
        }
        this.b = yVar.getItem().getItemId();
        this.w = new c(this, yVar);
        this.w.a((IBinder) null);
        super.a(yVar);
        return true;
    }

    public boolean a(ViewGroup viewGroup, int i2) {
        if (viewGroup.getChildAt(i2) == this.i) {
            return false;
        }
        return super.a(viewGroup, i2);
    }

    public void b(boolean z) {
        boolean z2 = true;
        boolean z3 = false;
        super.b(z);
        if (this.h != null) {
            if (this.e != null) {
                ArrayList j2 = this.e.j();
                int size = j2.size();
                for (int i2 = 0; i2 < size; i2++) {
                    ActionProvider A = ((r) j2.get(i2)).A();
                    if (A != null) {
                        A.setSubUiVisibilityListener(this);
                    }
                }
            }
            ArrayList k2 = this.e != null ? this.e.k() : null;
            if (this.j && k2 != null) {
                int size2 = k2.size();
                if (size2 == 1) {
                    z3 = !((r) k2.get(0)).E();
                } else {
                    if (size2 <= 0) {
                        z2 = false;
                    }
                    z3 = z2;
                }
            }
            if (z3) {
                if (this.i == null) {
                    this.i = new e(this, this.c);
                }
                ViewGroup viewGroup = (ViewGroup) this.i.getParent();
                if (viewGroup != this.h) {
                    if (viewGroup != null) {
                        viewGroup.removeView(this.i);
                    }
                    ActionMenuView actionMenuView = (ActionMenuView) this.h;
                    actionMenuView.addView(this.i, actionMenuView.b());
                }
            } else if (this.i != null && this.i.getParent() == this.h) {
                ((ViewGroup) this.h).removeView(this.i);
            }
            ((ActionMenuView) this.h).setOverflowReserved(this.j);
        }
    }

    public boolean b() {
        if (this.x == null || this.h == null) {
            f fVar = this.v;
            if (fVar == null) {
                return false;
            }
            fVar.b();
            return true;
        }
        ((View) this.h).removeCallbacks(this.x);
        this.x = null;
        return true;
    }

    public boolean c() {
        return b() | d();
    }

    public boolean d() {
        if (this.w == null) {
            return false;
        }
        this.w.a();
        return true;
    }

    public boolean e() {
        return this.v != null && this.v.c();
    }

    public boolean f() {
        int i2;
        int i3;
        int i4;
        int i5;
        boolean z;
        int i6;
        int i7;
        int i8;
        int i9;
        boolean z2;
        ArrayList h = this.e.h();
        int size = h.size();
        int i10 = this.n;
        int i11 = this.m;
        int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, 0);
        ViewGroup viewGroup = (ViewGroup) this.h;
        int i12 = 0;
        int i13 = 0;
        boolean z3 = false;
        int i14 = 0;
        while (i14 < size) {
            r rVar = (r) h.get(i14);
            if (rVar.x()) {
                i12++;
            } else if (rVar.w()) {
                i13++;
            } else {
                z3 = true;
            }
            i14++;
            i10 = (!this.r || !rVar.E()) ? i10 : 0;
        }
        if (this.j && (z3 || i12 + i13 > i10)) {
            i10--;
        }
        int i15 = i10 - i12;
        SparseBooleanArray sparseBooleanArray = this.t;
        sparseBooleanArray.clear();
        int i16 = 0;
        if (this.p) {
            i16 = i11 / this.s;
            i2 = ((i11 % this.s) / i16) + this.s;
        } else {
            i2 = 0;
        }
        int i17 = 0;
        int i18 = 0;
        int i19 = i16;
        while (i17 < size) {
            r rVar2 = (r) h.get(i17);
            if (rVar2.x()) {
                View a2 = a(rVar2, this.u, viewGroup);
                if (this.u == null) {
                    this.u = a2;
                }
                if (this.p) {
                    i19 -= ActionMenuView.a(a2, i2, i19, makeMeasureSpec, 0);
                } else {
                    a2.measure(makeMeasureSpec, makeMeasureSpec);
                }
                i3 = a2.getMeasuredWidth();
                int i20 = i11 - i3;
                if (i18 != 0) {
                    i3 = i18;
                }
                int c = rVar2.c();
                if (c != 0) {
                    sparseBooleanArray.put(c, true);
                }
                rVar2.e(true);
                i4 = i20;
                i5 = i15;
            } else if (rVar2.w()) {
                int c2 = rVar2.c();
                boolean z4 = sparseBooleanArray.get(c2);
                boolean z5 = (i15 > 0 || z4) && i11 > 0 && (!this.p || i19 > 0);
                if (z5) {
                    View a3 = a(rVar2, this.u, viewGroup);
                    if (this.u == null) {
                        this.u = a3;
                    }
                    if (this.p) {
                        int a4 = ActionMenuView.a(a3, i2, i19, makeMeasureSpec, 0);
                        int i21 = i19 - a4;
                        z2 = a4 == 0 ? false : z5;
                        i9 = i21;
                    } else {
                        a3.measure(makeMeasureSpec, makeMeasureSpec);
                        boolean z6 = z5;
                        i9 = i19;
                        z2 = z6;
                    }
                    int measuredWidth = a3.getMeasuredWidth();
                    i11 -= measuredWidth;
                    if (i18 == 0) {
                        i18 = measuredWidth;
                    }
                    if (this.p) {
                        z = z2 & (i11 >= 0);
                        i6 = i18;
                        i7 = i9;
                    } else {
                        z = z2 & (i11 + i18 > 0);
                        i6 = i18;
                        i7 = i9;
                    }
                } else {
                    z = z5;
                    i6 = i18;
                    i7 = i19;
                }
                if (z && c2 != 0) {
                    sparseBooleanArray.put(c2, true);
                    i8 = i15;
                } else if (z4) {
                    sparseBooleanArray.put(c2, false);
                    int i22 = i15;
                    for (int i23 = 0; i23 < i17; i23++) {
                        r rVar3 = (r) h.get(i23);
                        if (rVar3.c() == c2) {
                            if (rVar3.v()) {
                                i22++;
                            }
                            rVar3.e(false);
                        }
                    }
                    i8 = i22;
                } else {
                    i8 = i15;
                }
                if (z) {
                    i8--;
                }
                rVar2.e(z);
                i3 = i6;
                i4 = i11;
                int i24 = i7;
                i5 = i8;
                i19 = i24;
            } else {
                i3 = i18;
                i4 = i11;
                i5 = i15;
            }
            i17++;
            i11 = i4;
            i15 = i5;
            i18 = i3;
        }
        return true;
    }
}
