package android.support.v7.internal.widget;

import android.support.v7.a.d;
import android.support.v7.internal.widget.ScrollingTabContainerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

class ah extends BaseAdapter {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ScrollingTabContainerView f55a;

    private ah(ScrollingTabContainerView scrollingTabContainerView) {
        this.f55a = scrollingTabContainerView;
    }

    /* synthetic */ ah(ScrollingTabContainerView scrollingTabContainerView, ag agVar) {
        this(scrollingTabContainerView);
    }

    public int getCount() {
        return this.f55a.e.getChildCount();
    }

    public Object getItem(int i) {
        return ((ScrollingTabContainerView.TabView) this.f55a.e.getChildAt(i)).getTab();
    }

    public long getItemId(int i) {
        return (long) i;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            return this.f55a.a((d) getItem(i), true);
        }
        ((ScrollingTabContainerView.TabView) view).a((d) getItem(i));
        return view;
    }
}
