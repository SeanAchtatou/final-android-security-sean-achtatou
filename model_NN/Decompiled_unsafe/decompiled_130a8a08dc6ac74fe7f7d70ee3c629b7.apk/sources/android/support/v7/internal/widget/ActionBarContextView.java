package android.support.v7.internal.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.support.v7.b.b;
import android.support.v7.b.e;
import android.support.v7.b.g;
import android.support.v7.b.j;
import android.support.v7.internal.view.menu.ActionMenuView;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

public class ActionBarContextView extends a {
    private CharSequence g;
    private CharSequence h;
    private View i;
    private View j;
    private LinearLayout k;
    private TextView l;
    private TextView m;
    private int n;
    private int o;
    private Drawable p;
    private boolean q;

    public ActionBarContextView(Context context) {
        this(context, null);
    }

    public ActionBarContextView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, b.actionModeStyle);
    }

    public ActionBarContextView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, j.ActionMode, i2, 0);
        setBackgroundDrawable(obtainStyledAttributes.getDrawable(3));
        this.n = obtainStyledAttributes.getResourceId(1, 0);
        this.o = obtainStyledAttributes.getResourceId(2, 0);
        this.f = obtainStyledAttributes.getLayoutDimension(0, 0);
        this.p = obtainStyledAttributes.getDrawable(4);
        obtainStyledAttributes.recycle();
    }

    private void d() {
        int i2 = 8;
        boolean z = true;
        if (this.k == null) {
            LayoutInflater.from(getContext()).inflate(g.abc_action_bar_title_item, this);
            this.k = (LinearLayout) getChildAt(getChildCount() - 1);
            this.l = (TextView) this.k.findViewById(e.action_bar_title);
            this.m = (TextView) this.k.findViewById(e.action_bar_subtitle);
            if (this.n != 0) {
                this.l.setTextAppearance(getContext(), this.n);
            }
            if (this.o != 0) {
                this.m.setTextAppearance(getContext(), this.o);
            }
        }
        this.l.setText(this.g);
        this.m.setText(this.h);
        boolean z2 = !TextUtils.isEmpty(this.g);
        if (TextUtils.isEmpty(this.h)) {
            z = false;
        }
        this.m.setVisibility(z ? 0 : 8);
        LinearLayout linearLayout = this.k;
        if (z2 || z) {
            i2 = 0;
        }
        linearLayout.setVisibility(i2);
        if (this.k.getParent() == null) {
            addView(this.k);
        }
    }

    public boolean a() {
        if (this.b != null) {
            return this.b.a();
        }
        return false;
    }

    public /* bridge */ /* synthetic */ void b() {
        super.b();
    }

    public boolean c() {
        if (this.b != null) {
            return this.b.e();
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public ViewGroup.LayoutParams generateDefaultLayoutParams() {
        return new ViewGroup.MarginLayoutParams(-1, -2);
    }

    public ViewGroup.LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        return new ViewGroup.MarginLayoutParams(getContext(), attributeSet);
    }

    public /* bridge */ /* synthetic */ int getAnimatedVisibility() {
        return super.getAnimatedVisibility();
    }

    public /* bridge */ /* synthetic */ int getContentHeight() {
        return super.getContentHeight();
    }

    public CharSequence getSubtitle() {
        return this.h;
    }

    public CharSequence getTitle() {
        return this.g;
    }

    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (this.b != null) {
            this.b.b();
            this.b.d();
        }
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i2, int i3, int i4, int i5) {
        int i6;
        int paddingLeft = getPaddingLeft();
        int paddingTop = getPaddingTop();
        int paddingTop2 = ((i5 - i3) - getPaddingTop()) - getPaddingBottom();
        if (this.i == null || this.i.getVisibility() == 8) {
            i6 = paddingLeft;
        } else {
            ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) this.i.getLayoutParams();
            int i7 = paddingLeft + marginLayoutParams.leftMargin;
            i6 = marginLayoutParams.rightMargin + i7 + b(this.i, i7, paddingTop, paddingTop2);
        }
        if (!(this.k == null || this.j != null || this.k.getVisibility() == 8)) {
            i6 += b(this.k, i6, paddingTop, paddingTop2);
        }
        if (this.j != null) {
            int b = i6 + b(this.j, i6, paddingTop, paddingTop2);
        }
        int paddingRight = (i4 - i2) - getPaddingRight();
        if (this.f50a != null) {
            int c = paddingRight - c(this.f50a, paddingRight, paddingTop, paddingTop2);
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        int i4 = 1073741824;
        int i5 = 0;
        if (View.MeasureSpec.getMode(i2) != 1073741824) {
            throw new IllegalStateException(getClass().getSimpleName() + " can only be used " + "with android:layout_width=\"FILL_PARENT\" (or fill_parent)");
        } else if (View.MeasureSpec.getMode(i3) == 0) {
            throw new IllegalStateException(getClass().getSimpleName() + " can only be used " + "with android:layout_height=\"wrap_content\"");
        } else {
            int size = View.MeasureSpec.getSize(i2);
            int size2 = this.f > 0 ? this.f : View.MeasureSpec.getSize(i3);
            int paddingTop = getPaddingTop() + getPaddingBottom();
            int paddingLeft = (size - getPaddingLeft()) - getPaddingRight();
            int i6 = size2 - paddingTop;
            int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(i6, Integer.MIN_VALUE);
            if (this.i != null) {
                int a2 = a(this.i, paddingLeft, makeMeasureSpec, 0);
                ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) this.i.getLayoutParams();
                paddingLeft = a2 - (marginLayoutParams.rightMargin + marginLayoutParams.leftMargin);
            }
            if (this.f50a != null && this.f50a.getParent() == this) {
                paddingLeft = a(this.f50a, paddingLeft, makeMeasureSpec, 0);
            }
            if (this.k != null && this.j == null) {
                if (this.q) {
                    this.k.measure(View.MeasureSpec.makeMeasureSpec(0, 0), makeMeasureSpec);
                    int measuredWidth = this.k.getMeasuredWidth();
                    boolean z = measuredWidth <= paddingLeft;
                    if (z) {
                        paddingLeft -= measuredWidth;
                    }
                    this.k.setVisibility(z ? 0 : 8);
                } else {
                    paddingLeft = a(this.k, paddingLeft, makeMeasureSpec, 0);
                }
            }
            if (this.j != null) {
                ViewGroup.LayoutParams layoutParams = this.j.getLayoutParams();
                int i7 = layoutParams.width != -2 ? 1073741824 : Integer.MIN_VALUE;
                if (layoutParams.width >= 0) {
                    paddingLeft = Math.min(layoutParams.width, paddingLeft);
                }
                if (layoutParams.height == -2) {
                    i4 = Integer.MIN_VALUE;
                }
                this.j.measure(View.MeasureSpec.makeMeasureSpec(paddingLeft, i7), View.MeasureSpec.makeMeasureSpec(layoutParams.height >= 0 ? Math.min(layoutParams.height, i6) : i6, i4));
            }
            if (this.f <= 0) {
                int childCount = getChildCount();
                int i8 = 0;
                while (i5 < childCount) {
                    int measuredHeight = getChildAt(i5).getMeasuredHeight() + paddingTop;
                    if (measuredHeight <= i8) {
                        measuredHeight = i8;
                    }
                    i5++;
                    i8 = measuredHeight;
                }
                setMeasuredDimension(size, i8);
                return;
            }
            setMeasuredDimension(size, size2);
        }
    }

    public void setContentHeight(int i2) {
        this.f = i2;
    }

    public void setCustomView(View view) {
        if (this.j != null) {
            removeView(this.j);
        }
        this.j = view;
        if (this.k != null) {
            removeView(this.k);
            this.k = null;
        }
        if (view != null) {
            addView(view);
        }
        requestLayout();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.internal.view.menu.ActionMenuPresenter.a(int, boolean):void
     arg types: [int, int]
     candidates:
      android.support.v7.internal.view.menu.ActionMenuPresenter.a(android.support.v7.internal.view.menu.ActionMenuPresenter, android.support.v7.internal.view.menu.c):android.support.v7.internal.view.menu.c
      android.support.v7.internal.view.menu.ActionMenuPresenter.a(android.support.v7.internal.view.menu.ActionMenuPresenter, android.support.v7.internal.view.menu.d):android.support.v7.internal.view.menu.d
      android.support.v7.internal.view.menu.ActionMenuPresenter.a(android.support.v7.internal.view.menu.ActionMenuPresenter, android.support.v7.internal.view.menu.f):android.support.v7.internal.view.menu.f
      android.support.v7.internal.view.menu.ActionMenuPresenter.a(android.content.Context, android.support.v7.internal.view.menu.n):void
      android.support.v7.internal.view.menu.ActionMenuPresenter.a(android.support.v7.internal.view.menu.n, boolean):void
      android.support.v7.internal.view.menu.ActionMenuPresenter.a(android.support.v7.internal.view.menu.r, android.support.v7.internal.view.menu.x):void
      android.support.v7.internal.view.menu.ActionMenuPresenter.a(int, android.support.v7.internal.view.menu.r):boolean
      android.support.v7.internal.view.menu.ActionMenuPresenter.a(android.view.ViewGroup, int):boolean
      android.support.v7.internal.view.menu.k.a(android.content.Context, android.support.v7.internal.view.menu.n):void
      android.support.v7.internal.view.menu.k.a(android.support.v7.internal.view.menu.n, boolean):void
      android.support.v7.internal.view.menu.k.a(android.support.v7.internal.view.menu.r, android.support.v7.internal.view.menu.x):void
      android.support.v7.internal.view.menu.k.a(android.view.View, int):void
      android.support.v7.internal.view.menu.k.a(int, android.support.v7.internal.view.menu.r):boolean
      android.support.v7.internal.view.menu.k.a(android.support.v7.internal.view.menu.n, android.support.v7.internal.view.menu.r):boolean
      android.support.v7.internal.view.menu.k.a(android.view.ViewGroup, int):boolean
      android.support.v7.internal.view.menu.u.a(android.content.Context, android.support.v7.internal.view.menu.n):void
      android.support.v7.internal.view.menu.u.a(android.support.v7.internal.view.menu.n, boolean):void
      android.support.v7.internal.view.menu.u.a(android.support.v7.internal.view.menu.n, android.support.v7.internal.view.menu.r):boolean
      android.support.v7.internal.view.menu.ActionMenuPresenter.a(int, boolean):void */
    public void setSplitActionBar(boolean z) {
        if (this.d != z) {
            if (this.b != null) {
                ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(-2, -1);
                if (!z) {
                    this.f50a = (ActionMenuView) this.b.a(this);
                    this.f50a.setBackgroundDrawable(null);
                    ViewGroup viewGroup = (ViewGroup) this.f50a.getParent();
                    if (viewGroup != null) {
                        viewGroup.removeView(this.f50a);
                    }
                    addView(this.f50a, layoutParams);
                } else {
                    this.b.a(getContext().getResources().getDisplayMetrics().widthPixels, true);
                    this.b.a(Integer.MAX_VALUE);
                    layoutParams.width = -1;
                    layoutParams.height = this.f;
                    this.f50a = (ActionMenuView) this.b.a(this);
                    this.f50a.setBackgroundDrawable(this.p);
                    ViewGroup viewGroup2 = (ViewGroup) this.f50a.getParent();
                    if (viewGroup2 != null) {
                        viewGroup2.removeView(this.f50a);
                    }
                    this.c.addView(this.f50a, layoutParams);
                }
            }
            super.setSplitActionBar(z);
        }
    }

    public /* bridge */ /* synthetic */ void setSplitView(ActionBarContainer actionBarContainer) {
        super.setSplitView(actionBarContainer);
    }

    public /* bridge */ /* synthetic */ void setSplitWhenNarrow(boolean z) {
        super.setSplitWhenNarrow(z);
    }

    public void setSubtitle(CharSequence charSequence) {
        this.h = charSequence;
        d();
    }

    public void setTitle(CharSequence charSequence) {
        this.g = charSequence;
        d();
    }

    public void setTitleOptional(boolean z) {
        if (z != this.q) {
            requestLayout();
        }
        this.q = z;
    }

    public /* bridge */ /* synthetic */ void setVisibility(int i2) {
        super.setVisibility(i2);
    }
}
