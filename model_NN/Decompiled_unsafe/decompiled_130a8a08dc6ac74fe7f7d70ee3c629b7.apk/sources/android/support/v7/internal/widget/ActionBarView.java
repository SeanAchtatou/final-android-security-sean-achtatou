package android.support.v7.internal.widget;

import android.app.Activity;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.internal.view.SupportMenuItem;
import android.support.v7.a.c;
import android.support.v7.b.b;
import android.support.v7.b.e;
import android.support.v7.b.g;
import android.support.v7.b.h;
import android.support.v7.b.j;
import android.support.v7.internal.view.menu.a;
import android.support.v7.internal.view.menu.n;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.Window;
import android.view.accessibility.AccessibilityEvent;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

public class ActionBarView extends a {
    private ProgressBarICS A;
    private int B;
    private int C;
    private int D;
    private int E;
    private int F;
    private int G;
    private boolean H;
    private boolean I;
    private boolean J;
    private boolean K;
    private n L;
    private ActionBarContextView M;
    /* access modifiers changed from: private */
    public a N;
    private SpinnerAdapter O;
    /* access modifiers changed from: private */
    public c P;
    private Runnable Q;
    /* access modifiers changed from: private */
    public i R;
    private final p S = new f(this);
    private final View.OnClickListener T = new g(this);
    private final View.OnClickListener U = new h(this);
    View g;
    Window.Callback h;
    /* access modifiers changed from: private */
    public int i;
    /* access modifiers changed from: private */
    public int j = -1;
    private CharSequence k;
    private CharSequence l;
    /* access modifiers changed from: private */
    public Drawable m;
    private Drawable n;
    private Context o;
    /* access modifiers changed from: private */
    public HomeView p;
    /* access modifiers changed from: private */
    public HomeView q;
    /* access modifiers changed from: private */
    public LinearLayout r;
    private TextView s;
    private TextView t;
    private View u;
    /* access modifiers changed from: private */
    public aj v;
    private LinearLayout w;
    /* access modifiers changed from: private */
    public ScrollingTabContainerView x;
    /* access modifiers changed from: private */
    public View y;
    private ProgressBarICS z;

    class HomeView extends FrameLayout {

        /* renamed from: a  reason: collision with root package name */
        private ImageView f42a;
        private ImageView b;
        private int c;
        private int d;
        private Drawable e;

        public HomeView(Context context) {
            this(context, null);
        }

        public HomeView(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
        }

        public int a() {
            if (this.f42a.getVisibility() == 8) {
                return this.c;
            }
            return 0;
        }

        public void a(int i) {
            this.d = i;
            this.f42a.setImageDrawable(i != 0 ? getResources().getDrawable(i) : this.e);
        }

        public void a(Drawable drawable) {
            this.b.setImageDrawable(drawable);
        }

        public void a(boolean z) {
            this.f42a.setVisibility(z ? 0 : 8);
        }

        public void b(Drawable drawable) {
            ImageView imageView = this.f42a;
            if (drawable == null) {
                drawable = this.e;
            }
            imageView.setImageDrawable(drawable);
            this.d = 0;
        }

        public boolean dispatchPopulateAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
            CharSequence contentDescription = getContentDescription();
            if (TextUtils.isEmpty(contentDescription)) {
                return true;
            }
            accessibilityEvent.getText().add(contentDescription);
            return true;
        }

        /* access modifiers changed from: protected */
        public void onConfigurationChanged(Configuration configuration) {
            super.onConfigurationChanged(configuration);
            if (this.d != 0) {
                a(this.d);
            }
        }

        /* access modifiers changed from: protected */
        public void onFinishInflate() {
            this.f42a = (ImageView) findViewById(e.up);
            this.b = (ImageView) findViewById(e.home);
            this.e = this.f42a.getDrawable();
        }

        /* access modifiers changed from: protected */
        public void onLayout(boolean z, int i, int i2, int i3, int i4) {
            int i5 = 0;
            int i6 = (i4 - i2) / 2;
            int i7 = i3 - i;
            if (this.f42a.getVisibility() != 8) {
                FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) this.f42a.getLayoutParams();
                int measuredHeight = this.f42a.getMeasuredHeight();
                int measuredWidth = this.f42a.getMeasuredWidth();
                int i8 = i6 - (measuredHeight / 2);
                this.f42a.layout(0, i8, measuredWidth, measuredHeight + i8);
                int i9 = layoutParams.rightMargin + layoutParams.leftMargin + measuredWidth;
                int i10 = i7 - i9;
                i += i9;
                i5 = i9;
            }
            FrameLayout.LayoutParams layoutParams2 = (FrameLayout.LayoutParams) this.b.getLayoutParams();
            int measuredHeight2 = this.b.getMeasuredHeight();
            int measuredWidth2 = this.b.getMeasuredWidth();
            int max = i5 + Math.max(layoutParams2.leftMargin, ((i3 - i) / 2) - (measuredWidth2 / 2));
            int max2 = Math.max(layoutParams2.topMargin, i6 - (measuredHeight2 / 2));
            this.b.layout(max, max2, measuredWidth2 + max, measuredHeight2 + max2);
        }

        /* access modifiers changed from: protected */
        public void onMeasure(int i, int i2) {
            measureChildWithMargins(this.f42a, i, 0, i2, 0);
            FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) this.f42a.getLayoutParams();
            this.c = layoutParams.leftMargin + this.f42a.getMeasuredWidth() + layoutParams.rightMargin;
            int i3 = this.f42a.getVisibility() == 8 ? 0 : this.c;
            int measuredHeight = layoutParams.topMargin + this.f42a.getMeasuredHeight() + layoutParams.bottomMargin;
            measureChildWithMargins(this.b, i, i3, i2, 0);
            FrameLayout.LayoutParams layoutParams2 = (FrameLayout.LayoutParams) this.b.getLayoutParams();
            int measuredWidth = i3 + layoutParams2.leftMargin + this.b.getMeasuredWidth() + layoutParams2.rightMargin;
            int max = Math.max(measuredHeight, layoutParams2.bottomMargin + layoutParams2.topMargin + this.b.getMeasuredHeight());
            int mode = View.MeasureSpec.getMode(i);
            int mode2 = View.MeasureSpec.getMode(i2);
            int size = View.MeasureSpec.getSize(i);
            int size2 = View.MeasureSpec.getSize(i2);
            switch (mode) {
                case Integer.MIN_VALUE:
                    size = Math.min(measuredWidth, size);
                    break;
                case 1073741824:
                    break;
                default:
                    size = measuredWidth;
                    break;
            }
            switch (mode2) {
                case Integer.MIN_VALUE:
                    size2 = Math.min(max, size2);
                    break;
                case 1073741824:
                    break;
                default:
                    size2 = max;
                    break;
            }
            setMeasuredDimension(size, size2);
        }
    }

    class SavedState extends View.BaseSavedState {
        public static final Parcelable.Creator CREATOR = new j();

        /* renamed from: a  reason: collision with root package name */
        int f43a;
        boolean b;

        private SavedState(Parcel parcel) {
            super(parcel);
            this.f43a = parcel.readInt();
            this.b = parcel.readInt() != 0;
        }

        /* synthetic */ SavedState(Parcel parcel, f fVar) {
            this(parcel);
        }

        SavedState(Parcelable parcelable) {
            super(parcelable);
        }

        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeInt(this.f43a);
            parcel.writeInt(this.b ? 1 : 0);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.support.v7.internal.widget.ActionBarView, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public ActionBarView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.o = context;
        setBackgroundResource(0);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, j.ActionBar, b.actionBarStyle, 0);
        ApplicationInfo applicationInfo = context.getApplicationInfo();
        PackageManager packageManager = context.getPackageManager();
        this.i = obtainStyledAttributes.getInt(2, 0);
        this.k = obtainStyledAttributes.getText(0);
        this.l = obtainStyledAttributes.getText(4);
        this.n = obtainStyledAttributes.getDrawable(8);
        if (this.n == null && Build.VERSION.SDK_INT >= 9) {
            if (context instanceof Activity) {
                try {
                    this.n = packageManager.getActivityLogo(((Activity) context).getComponentName());
                } catch (PackageManager.NameNotFoundException e) {
                    Log.e("ActionBarView", "Activity component name not found!", e);
                }
            }
            if (this.n == null) {
                this.n = applicationInfo.loadLogo(packageManager);
            }
        }
        this.m = obtainStyledAttributes.getDrawable(7);
        if (this.m == null) {
            if (context instanceof Activity) {
                try {
                    this.m = packageManager.getActivityIcon(((Activity) context).getComponentName());
                } catch (PackageManager.NameNotFoundException e2) {
                    Log.e("ActionBarView", "Activity component name not found!", e2);
                }
            }
            if (this.m == null) {
                this.m = applicationInfo.loadIcon(packageManager);
            }
        }
        LayoutInflater from = LayoutInflater.from(context);
        int resourceId = obtainStyledAttributes.getResourceId(14, g.abc_action_bar_home);
        this.p = (HomeView) from.inflate(resourceId, (ViewGroup) this, false);
        this.q = (HomeView) from.inflate(resourceId, (ViewGroup) this, false);
        this.q.a(true);
        this.q.setOnClickListener(this.T);
        this.q.setContentDescription(getResources().getText(h.abc_action_bar_up_description));
        this.D = obtainStyledAttributes.getResourceId(5, 0);
        this.E = obtainStyledAttributes.getResourceId(6, 0);
        this.F = obtainStyledAttributes.getResourceId(15, 0);
        this.G = obtainStyledAttributes.getResourceId(16, 0);
        this.B = obtainStyledAttributes.getDimensionPixelOffset(17, 0);
        this.C = obtainStyledAttributes.getDimensionPixelOffset(18, 0);
        setDisplayOptions(obtainStyledAttributes.getInt(3, 0));
        int resourceId2 = obtainStyledAttributes.getResourceId(13, 0);
        if (resourceId2 != 0) {
            this.y = from.inflate(resourceId2, (ViewGroup) this, false);
            this.i = 0;
            setDisplayOptions(this.j | 16);
        }
        this.f = obtainStyledAttributes.getLayoutDimension(1, 0);
        obtainStyledAttributes.recycle();
        this.N = new a(context, 0, 16908332, 0, 0, this.k);
        this.p.setOnClickListener(this.U);
        this.p.setClickable(true);
        this.p.setFocusable(true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.support.v7.internal.widget.ActionBarView, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* access modifiers changed from: private */
    public void g() {
        boolean z2 = true;
        if (this.r == null) {
            this.r = (LinearLayout) LayoutInflater.from(getContext()).inflate(g.abc_action_bar_title_item, (ViewGroup) this, false);
            this.s = (TextView) this.r.findViewById(e.action_bar_title);
            this.t = (TextView) this.r.findViewById(e.action_bar_subtitle);
            this.u = this.r.findViewById(e.up);
            this.r.setOnClickListener(this.U);
            if (this.D != 0) {
                this.s.setTextAppearance(this.o, this.D);
            }
            if (this.k != null) {
                this.s.setText(this.k);
            }
            if (this.E != 0) {
                this.t.setTextAppearance(this.o, this.E);
            }
            if (this.l != null) {
                this.t.setText(this.l);
                this.t.setVisibility(0);
            }
            boolean z3 = (this.j & 4) != 0;
            boolean z4 = (this.j & 2) != 0;
            this.u.setVisibility(!z4 ? z3 ? 0 : 4 : 8);
            LinearLayout linearLayout = this.r;
            if (!z3 || z4) {
                z2 = false;
            }
            linearLayout.setEnabled(z2);
        }
        addView(this.r);
        if (this.g != null || (TextUtils.isEmpty(this.k) && TextUtils.isEmpty(this.l))) {
            this.r.setVisibility(8);
        }
    }

    private void setTitleImpl(CharSequence charSequence) {
        int i2 = 0;
        this.k = charSequence;
        if (this.s != null) {
            this.s.setText(charSequence);
            boolean z2 = this.g == null && (this.j & 8) != 0 && (!TextUtils.isEmpty(this.k) || !TextUtils.isEmpty(this.l));
            LinearLayout linearLayout = this.r;
            if (!z2) {
                i2 = 8;
            }
            linearLayout.setVisibility(i2);
        }
        if (this.N != null) {
            this.N.a(charSequence);
        }
    }

    public /* bridge */ /* synthetic */ boolean a() {
        return super.a();
    }

    public /* bridge */ /* synthetic */ void b() {
        super.b();
    }

    public /* bridge */ /* synthetic */ boolean c() {
        return super.c();
    }

    public void d() {
        this.z = new ProgressBarICS(this.o, null, 0, this.F);
        this.z.setId(e.progress_horizontal);
        this.z.setMax(10000);
        this.z.setVisibility(8);
        addView(this.z);
    }

    public void e() {
        this.A = new ProgressBarICS(this.o, null, 0, this.G);
        this.A.setId(e.progress_circular);
        this.A.setVisibility(8);
        addView(this.A);
    }

    public boolean f() {
        return this.K;
    }

    /* access modifiers changed from: protected */
    public ViewGroup.LayoutParams generateDefaultLayoutParams() {
        return new android.support.v7.a.b(19);
    }

    public ViewGroup.LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        return new android.support.v7.a.b(getContext(), attributeSet);
    }

    public ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return layoutParams == null ? generateDefaultLayoutParams() : layoutParams;
    }

    public /* bridge */ /* synthetic */ int getAnimatedVisibility() {
        return super.getAnimatedVisibility();
    }

    public /* bridge */ /* synthetic */ int getContentHeight() {
        return super.getContentHeight();
    }

    public View getCustomNavigationView() {
        return this.y;
    }

    public int getDisplayOptions() {
        return this.j;
    }

    public SpinnerAdapter getDropdownAdapter() {
        return this.O;
    }

    public int getDropdownSelectedPosition() {
        return this.v.f();
    }

    public int getNavigationMode() {
        return this.i;
    }

    public CharSequence getSubtitle() {
        return this.l;
    }

    public CharSequence getTitle() {
        return this.k;
    }

    /* access modifiers changed from: protected */
    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        this.s = null;
        this.t = null;
        this.u = null;
        if (this.r != null && this.r.getParent() == this) {
            removeView(this.r);
        }
        this.r = null;
        if ((this.j & 8) != 0) {
            g();
        }
        if (this.x != null && this.I) {
            ViewGroup.LayoutParams layoutParams = this.x.getLayoutParams();
            if (layoutParams != null) {
                layoutParams.width = -2;
                layoutParams.height = -1;
            }
            this.x.setAllowCollapse(true);
        }
        if (this.z != null) {
            removeView(this.z);
            d();
        }
        if (this.A != null) {
            removeView(this.A);
            e();
        }
    }

    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        removeCallbacks(this.Q);
        if (this.b != null) {
            this.b.b();
            this.b.d();
        }
    }

    /* access modifiers changed from: protected */
    public void onFinishInflate() {
        ViewParent parent;
        super.onFinishInflate();
        addView(this.p);
        if (this.y != null && (this.j & 16) != 0 && (parent = this.y.getParent()) != this) {
            if (parent instanceof ViewGroup) {
                ((ViewGroup) parent).removeView(this.y);
            }
            addView(this.y);
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:100:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x0099  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x009e  */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x00fa  */
    /* JADX WARNING: Removed duplicated region for block: B:75:0x0150  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onLayout(boolean r10, int r11, int r12, int r13, int r14) {
        /*
            r9 = this;
            int r1 = r9.getPaddingLeft()
            int r2 = r9.getPaddingTop()
            int r0 = r14 - r12
            int r3 = r9.getPaddingTop()
            int r0 = r0 - r3
            int r3 = r9.getPaddingBottom()
            int r3 = r0 - r3
            if (r3 > 0) goto L_0x0018
        L_0x0017:
            return
        L_0x0018:
            android.view.View r0 = r9.g
            if (r0 == 0) goto L_0x011a
            android.support.v7.internal.widget.ActionBarView$HomeView r0 = r9.q
        L_0x001e:
            int r4 = r0.getVisibility()
            r5 = 8
            if (r4 == r5) goto L_0x01c5
            int r4 = r0.a()
            int r5 = r1 + r4
            int r0 = r9.b(r0, r5, r2, r3)
            int r0 = r0 + r4
            int r0 = r0 + r1
        L_0x0032:
            android.view.View r1 = r9.g
            if (r1 != 0) goto L_0x0059
            android.widget.LinearLayout r1 = r9.r
            if (r1 == 0) goto L_0x011e
            android.widget.LinearLayout r1 = r9.r
            int r1 = r1.getVisibility()
            r4 = 8
            if (r1 == r4) goto L_0x011e
            int r1 = r9.j
            r1 = r1 & 8
            if (r1 == 0) goto L_0x011e
            r1 = 1
        L_0x004b:
            if (r1 == 0) goto L_0x0054
            android.widget.LinearLayout r4 = r9.r
            int r4 = r9.b(r4, r0, r2, r3)
            int r0 = r0 + r4
        L_0x0054:
            int r4 = r9.i
            switch(r4) {
                case 0: goto L_0x0121;
                case 1: goto L_0x0124;
                case 2: goto L_0x013a;
                default: goto L_0x0059;
            }
        L_0x0059:
            r1 = r0
        L_0x005a:
            int r0 = r13 - r11
            int r4 = r9.getPaddingRight()
            int r0 = r0 - r4
            android.support.v7.internal.view.menu.ActionMenuView r4 = r9.f50a
            if (r4 == 0) goto L_0x0079
            android.support.v7.internal.view.menu.ActionMenuView r4 = r9.f50a
            android.view.ViewParent r4 = r4.getParent()
            if (r4 != r9) goto L_0x0079
            android.support.v7.internal.view.menu.ActionMenuView r4 = r9.f50a
            r9.c(r4, r0, r2, r3)
            android.support.v7.internal.view.menu.ActionMenuView r4 = r9.f50a
            int r4 = r4.getMeasuredWidth()
            int r0 = r0 - r4
        L_0x0079:
            android.support.v7.internal.widget.ProgressBarICS r4 = r9.A
            if (r4 == 0) goto L_0x01c2
            android.support.v7.internal.widget.ProgressBarICS r4 = r9.A
            int r4 = r4.getVisibility()
            r5 = 8
            if (r4 == r5) goto L_0x01c2
            android.support.v7.internal.widget.ProgressBarICS r4 = r9.A
            r9.c(r4, r0, r2, r3)
            android.support.v7.internal.widget.ProgressBarICS r2 = r9.A
            int r2 = r2.getMeasuredWidth()
            int r0 = r0 - r2
            r2 = r0
        L_0x0094:
            r0 = 0
            android.view.View r3 = r9.g
            if (r3 == 0) goto L_0x0150
            android.view.View r0 = r9.g
            r7 = r0
        L_0x009c:
            if (r7 == 0) goto L_0x00f6
            android.view.ViewGroup$LayoutParams r0 = r7.getLayoutParams()
            boolean r3 = r0 instanceof android.support.v7.a.b
            if (r3 == 0) goto L_0x015f
            android.support.v7.a.b r0 = (android.support.v7.a.b) r0
            r5 = r0
        L_0x00a9:
            if (r5 == 0) goto L_0x0163
            int r0 = r5.f14a
        L_0x00ad:
            int r8 = r7.getMeasuredWidth()
            r4 = 0
            r3 = 0
            if (r5 == 0) goto L_0x01b9
            int r3 = r5.leftMargin
            int r4 = r1 + r3
            int r1 = r5.rightMargin
            int r3 = r2 - r1
            int r2 = r5.topMargin
            int r1 = r5.bottomMargin
            r5 = r2
            r6 = r3
            r3 = r4
            r4 = r1
        L_0x00c5:
            r1 = r0 & 7
            r2 = 1
            if (r1 != r2) goto L_0x016d
            int r2 = r9.getWidth()
            int r2 = r2 - r8
            int r2 = r2 / 2
            if (r2 >= r3) goto L_0x0167
            r1 = 3
        L_0x00d4:
            r2 = r1
        L_0x00d5:
            r1 = 0
            switch(r2) {
                case 1: goto L_0x0174;
                case 2: goto L_0x00d9;
                case 3: goto L_0x017e;
                case 4: goto L_0x00d9;
                case 5: goto L_0x0181;
                default: goto L_0x00d9;
            }
        L_0x00d9:
            r2 = r1
        L_0x00da:
            r1 = r0 & 112(0x70, float:1.57E-43)
            r6 = -1
            if (r0 != r6) goto L_0x00e2
            r0 = 16
            r1 = r0
        L_0x00e2:
            r0 = 0
            switch(r1) {
                case 16: goto L_0x0186;
                case 48: goto L_0x019e;
                case 80: goto L_0x01a5;
                default: goto L_0x00e6;
            }
        L_0x00e6:
            int r1 = r7.getMeasuredWidth()
            int r4 = r2 + r1
            int r5 = r7.getMeasuredHeight()
            int r5 = r5 + r0
            r7.layout(r2, r0, r4, r5)
            int r0 = r3 + r1
        L_0x00f6:
            android.support.v7.internal.widget.ProgressBarICS r0 = r9.z
            if (r0 == 0) goto L_0x0017
            android.support.v7.internal.widget.ProgressBarICS r0 = r9.z
            r0.bringToFront()
            android.support.v7.internal.widget.ProgressBarICS r0 = r9.z
            int r0 = r0.getMeasuredHeight()
            int r0 = r0 / 2
            android.support.v7.internal.widget.ProgressBarICS r1 = r9.z
            int r2 = r9.B
            int r3 = -r0
            int r4 = r9.B
            android.support.v7.internal.widget.ProgressBarICS r5 = r9.z
            int r5 = r5.getMeasuredWidth()
            int r4 = r4 + r5
            r1.layout(r2, r3, r4, r0)
            goto L_0x0017
        L_0x011a:
            android.support.v7.internal.widget.ActionBarView$HomeView r0 = r9.p
            goto L_0x001e
        L_0x011e:
            r1 = 0
            goto L_0x004b
        L_0x0121:
            r1 = r0
            goto L_0x005a
        L_0x0124:
            android.widget.LinearLayout r4 = r9.w
            if (r4 == 0) goto L_0x0059
            if (r1 == 0) goto L_0x012d
            int r1 = r9.C
            int r0 = r0 + r1
        L_0x012d:
            android.widget.LinearLayout r1 = r9.w
            int r1 = r9.b(r1, r0, r2, r3)
            int r4 = r9.C
            int r1 = r1 + r4
            int r0 = r0 + r1
            r1 = r0
            goto L_0x005a
        L_0x013a:
            android.support.v7.internal.widget.ScrollingTabContainerView r4 = r9.x
            if (r4 == 0) goto L_0x0059
            if (r1 == 0) goto L_0x0143
            int r1 = r9.C
            int r0 = r0 + r1
        L_0x0143:
            android.support.v7.internal.widget.ScrollingTabContainerView r1 = r9.x
            int r1 = r9.b(r1, r0, r2, r3)
            int r4 = r9.C
            int r1 = r1 + r4
            int r0 = r0 + r1
            r1 = r0
            goto L_0x005a
        L_0x0150:
            int r3 = r9.j
            r3 = r3 & 16
            if (r3 == 0) goto L_0x01bf
            android.view.View r3 = r9.y
            if (r3 == 0) goto L_0x01bf
            android.view.View r0 = r9.y
            r7 = r0
            goto L_0x009c
        L_0x015f:
            r0 = 0
            r5 = r0
            goto L_0x00a9
        L_0x0163:
            r0 = 19
            goto L_0x00ad
        L_0x0167:
            int r2 = r2 + r8
            if (r2 <= r6) goto L_0x00d4
            r1 = 5
            goto L_0x00d4
        L_0x016d:
            r2 = -1
            if (r0 != r2) goto L_0x01b6
            r1 = 3
            r2 = r1
            goto L_0x00d5
        L_0x0174:
            int r1 = r9.getWidth()
            int r1 = r1 - r8
            int r1 = r1 / 2
            r2 = r1
            goto L_0x00da
        L_0x017e:
            r2 = r3
            goto L_0x00da
        L_0x0181:
            int r1 = r6 - r8
            r2 = r1
            goto L_0x00da
        L_0x0186:
            int r0 = r9.getPaddingTop()
            int r1 = r9.getHeight()
            int r4 = r9.getPaddingBottom()
            int r1 = r1 - r4
            int r0 = r1 - r0
            int r1 = r7.getMeasuredHeight()
            int r0 = r0 - r1
            int r0 = r0 / 2
            goto L_0x00e6
        L_0x019e:
            int r0 = r9.getPaddingTop()
            int r0 = r0 + r5
            goto L_0x00e6
        L_0x01a5:
            int r0 = r9.getHeight()
            int r1 = r9.getPaddingBottom()
            int r0 = r0 - r1
            int r1 = r7.getMeasuredHeight()
            int r0 = r0 - r1
            int r0 = r0 - r4
            goto L_0x00e6
        L_0x01b6:
            r2 = r1
            goto L_0x00d5
        L_0x01b9:
            r5 = r4
            r6 = r2
            r4 = r3
            r3 = r1
            goto L_0x00c5
        L_0x01bf:
            r7 = r0
            goto L_0x009c
        L_0x01c2:
            r2 = r0
            goto L_0x0094
        L_0x01c5:
            r0 = r1
            goto L_0x0032
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.internal.widget.ActionBarView.onLayout(boolean, int, int, int, int):void");
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:114:0x031f  */
    /* JADX WARNING: Removed duplicated region for block: B:130:0x035e  */
    /* JADX WARNING: Removed duplicated region for block: B:136:0x039c  */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x019d  */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x01a4  */
    /* JADX WARNING: Removed duplicated region for block: B:93:0x025a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onMeasure(int r20, int r21) {
        /*
            r19 = this;
            int r13 = r19.getChildCount()
            r0 = r19
            boolean r1 = r0.J
            if (r1 == 0) goto L_0x0045
            r2 = 0
            r1 = 0
            r18 = r1
            r1 = r2
            r2 = r18
        L_0x0011:
            if (r2 >= r13) goto L_0x0036
            r0 = r19
            android.view.View r3 = r0.getChildAt(r2)
            int r4 = r3.getVisibility()
            r5 = 8
            if (r4 == r5) goto L_0x0033
            r0 = r19
            android.support.v7.internal.view.menu.ActionMenuView r4 = r0.f50a
            if (r3 != r4) goto L_0x0031
            r0 = r19
            android.support.v7.internal.view.menu.ActionMenuView r3 = r0.f50a
            int r3 = r3.getChildCount()
            if (r3 == 0) goto L_0x0033
        L_0x0031:
            int r1 = r1 + 1
        L_0x0033:
            int r2 = r2 + 1
            goto L_0x0011
        L_0x0036:
            if (r1 != 0) goto L_0x0045
            r1 = 0
            r2 = 0
            r0 = r19
            r0.setMeasuredDimension(r1, r2)
            r1 = 1
            r0 = r19
            r0.K = r1
        L_0x0044:
            return
        L_0x0045:
            r1 = 0
            r0 = r19
            r0.K = r1
            int r1 = android.view.View.MeasureSpec.getMode(r20)
            r2 = 1073741824(0x40000000, float:2.0)
            if (r1 == r2) goto L_0x0079
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.Class r3 = r19.getClass()
            java.lang.String r3 = r3.getSimpleName()
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r3 = " can only be used "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r3 = "with android:layout_width=\"MATCH_PARENT\" (or fill_parent)"
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            r1.<init>(r2)
            throw r1
        L_0x0079:
            int r1 = android.view.View.MeasureSpec.getMode(r21)
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            if (r1 == r2) goto L_0x00a8
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.Class r3 = r19.getClass()
            java.lang.String r3 = r3.getSimpleName()
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r3 = " can only be used "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r3 = "with android:layout_height=\"wrap_content\""
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            r1.<init>(r2)
            throw r1
        L_0x00a8:
            int r14 = android.view.View.MeasureSpec.getSize(r20)
            r0 = r19
            int r1 = r0.f
            if (r1 <= 0) goto L_0x0271
            r0 = r19
            int r1 = r0.f
            r3 = r1
        L_0x00b7:
            int r1 = r19.getPaddingTop()
            int r2 = r19.getPaddingBottom()
            int r15 = r1 + r2
            int r1 = r19.getPaddingLeft()
            int r2 = r19.getPaddingRight()
            int r10 = r3 - r15
            r4 = -2147483648(0xffffffff80000000, float:-0.0)
            int r6 = android.view.View.MeasureSpec.makeMeasureSpec(r10, r4)
            int r1 = r14 - r1
            int r5 = r1 - r2
            int r4 = r5 / 2
            r0 = r19
            android.view.View r1 = r0.g
            if (r1 == 0) goto L_0x0278
            r0 = r19
            android.support.v7.internal.widget.ActionBarView$HomeView r1 = r0.q
        L_0x00e1:
            int r2 = r1.getVisibility()
            r7 = 8
            if (r2 == r7) goto L_0x03ab
            android.view.ViewGroup$LayoutParams r2 = r1.getLayoutParams()
            int r7 = r2.width
            if (r7 >= 0) goto L_0x027e
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            int r2 = android.view.View.MeasureSpec.makeMeasureSpec(r5, r2)
        L_0x00f7:
            r7 = 1073741824(0x40000000, float:2.0)
            int r7 = android.view.View.MeasureSpec.makeMeasureSpec(r10, r7)
            r1.measure(r2, r7)
            int r2 = r1.getMeasuredWidth()
            int r1 = r1.a()
            int r1 = r1 + r2
            r2 = 0
            int r5 = r5 - r1
            int r2 = java.lang.Math.max(r2, r5)
            r5 = 0
            int r1 = r2 - r1
            int r1 = java.lang.Math.max(r5, r1)
        L_0x0116:
            r0 = r19
            android.support.v7.internal.view.menu.ActionMenuView r5 = r0.f50a
            if (r5 == 0) goto L_0x0141
            r0 = r19
            android.support.v7.internal.view.menu.ActionMenuView r5 = r0.f50a
            android.view.ViewParent r5 = r5.getParent()
            r0 = r19
            if (r5 != r0) goto L_0x0141
            r0 = r19
            android.support.v7.internal.view.menu.ActionMenuView r5 = r0.f50a
            r7 = 0
            r0 = r19
            int r2 = r0.a(r5, r2, r6, r7)
            r5 = 0
            r0 = r19
            android.support.v7.internal.view.menu.ActionMenuView r7 = r0.f50a
            int r7 = r7.getMeasuredWidth()
            int r4 = r4 - r7
            int r4 = java.lang.Math.max(r5, r4)
        L_0x0141:
            r0 = r19
            android.support.v7.internal.widget.ProgressBarICS r5 = r0.A
            if (r5 == 0) goto L_0x016c
            r0 = r19
            android.support.v7.internal.widget.ProgressBarICS r5 = r0.A
            int r5 = r5.getVisibility()
            r7 = 8
            if (r5 == r7) goto L_0x016c
            r0 = r19
            android.support.v7.internal.widget.ProgressBarICS r5 = r0.A
            r7 = 0
            r0 = r19
            int r2 = r0.a(r5, r2, r6, r7)
            r5 = 0
            r0 = r19
            android.support.v7.internal.widget.ProgressBarICS r6 = r0.A
            int r6 = r6.getMeasuredWidth()
            int r4 = r4 - r6
            int r4 = java.lang.Math.max(r5, r4)
        L_0x016c:
            r0 = r19
            android.widget.LinearLayout r5 = r0.r
            if (r5 == 0) goto L_0x0288
            r0 = r19
            android.widget.LinearLayout r5 = r0.r
            int r5 = r5.getVisibility()
            r6 = 8
            if (r5 == r6) goto L_0x0288
            r0 = r19
            int r5 = r0.j
            r5 = r5 & 8
            if (r5 == 0) goto L_0x0288
            r5 = 1
        L_0x0187:
            r0 = r19
            android.view.View r6 = r0.g
            if (r6 != 0) goto L_0x0194
            r0 = r19
            int r6 = r0.i
            switch(r6) {
                case 1: goto L_0x028b;
                case 2: goto L_0x02d5;
                default: goto L_0x0194;
            }
        L_0x0194:
            r6 = r1
            r7 = r2
        L_0x0196:
            r1 = 0
            r0 = r19
            android.view.View r2 = r0.g
            if (r2 == 0) goto L_0x031f
            r0 = r19
            android.view.View r1 = r0.g
            r12 = r1
        L_0x01a2:
            if (r12 == 0) goto L_0x022a
            android.view.ViewGroup$LayoutParams r1 = r12.getLayoutParams()
            r0 = r19
            android.view.ViewGroup$LayoutParams r2 = r0.generateLayoutParams(r1)
            boolean r1 = r2 instanceof android.support.v7.a.b
            if (r1 == 0) goto L_0x0334
            r1 = r2
            android.support.v7.a.b r1 = (android.support.v7.a.b) r1
            r11 = r1
        L_0x01b6:
            r8 = 0
            r1 = 0
            if (r11 == 0) goto L_0x01c4
            int r1 = r11.leftMargin
            int r8 = r11.rightMargin
            int r8 = r8 + r1
            int r1 = r11.topMargin
            int r9 = r11.bottomMargin
            int r1 = r1 + r9
        L_0x01c4:
            r0 = r19
            int r9 = r0.f
            if (r9 > 0) goto L_0x0338
            r9 = -2147483648(0xffffffff80000000, float:-0.0)
        L_0x01cc:
            r16 = 0
            int r0 = r2.height
            r17 = r0
            if (r17 < 0) goto L_0x01de
            int r0 = r2.height
            r17 = r0
            r0 = r17
            int r10 = java.lang.Math.min(r0, r10)
        L_0x01de:
            int r1 = r10 - r1
            r0 = r16
            int r16 = java.lang.Math.max(r0, r1)
            int r1 = r2.width
            r10 = -2
            if (r1 == r10) goto L_0x0348
            r1 = 1073741824(0x40000000, float:2.0)
        L_0x01ed:
            r17 = 0
            int r10 = r2.width
            if (r10 < 0) goto L_0x034c
            int r10 = r2.width
            int r10 = java.lang.Math.min(r10, r7)
        L_0x01f9:
            int r10 = r10 - r8
            r0 = r17
            int r10 = java.lang.Math.max(r0, r10)
            if (r11 == 0) goto L_0x034f
            int r11 = r11.f14a
        L_0x0204:
            r11 = r11 & 7
            r17 = 1
            r0 = r17
            if (r11 != r0) goto L_0x03a5
            int r2 = r2.width
            r11 = -1
            if (r2 != r11) goto L_0x03a5
            int r2 = java.lang.Math.min(r6, r4)
            int r2 = r2 * 2
        L_0x0217:
            int r1 = android.view.View.MeasureSpec.makeMeasureSpec(r2, r1)
            r0 = r16
            int r2 = android.view.View.MeasureSpec.makeMeasureSpec(r0, r9)
            r12.measure(r1, r2)
            int r1 = r12.getMeasuredWidth()
            int r1 = r1 + r8
            int r7 = r7 - r1
        L_0x022a:
            r0 = r19
            android.view.View r1 = r0.g
            if (r1 != 0) goto L_0x0254
            if (r5 == 0) goto L_0x0254
            r0 = r19
            android.widget.LinearLayout r1 = r0.r
            r0 = r19
            int r2 = r0.f
            r4 = 1073741824(0x40000000, float:2.0)
            int r2 = android.view.View.MeasureSpec.makeMeasureSpec(r2, r4)
            r4 = 0
            r0 = r19
            r0.a(r1, r7, r2, r4)
            r1 = 0
            r0 = r19
            android.widget.LinearLayout r2 = r0.r
            int r2 = r2.getMeasuredWidth()
            int r2 = r6 - r2
            java.lang.Math.max(r1, r2)
        L_0x0254:
            r0 = r19
            int r1 = r0.f
            if (r1 > 0) goto L_0x039c
            r2 = 0
            r1 = 0
            r3 = r1
        L_0x025d:
            if (r3 >= r13) goto L_0x0353
            r0 = r19
            android.view.View r1 = r0.getChildAt(r3)
            int r1 = r1.getMeasuredHeight()
            int r1 = r1 + r15
            if (r1 <= r2) goto L_0x03a2
        L_0x026c:
            int r2 = r3 + 1
            r3 = r2
            r2 = r1
            goto L_0x025d
        L_0x0271:
            int r1 = android.view.View.MeasureSpec.getSize(r21)
            r3 = r1
            goto L_0x00b7
        L_0x0278:
            r0 = r19
            android.support.v7.internal.widget.ActionBarView$HomeView r1 = r0.p
            goto L_0x00e1
        L_0x027e:
            int r2 = r2.width
            r7 = 1073741824(0x40000000, float:2.0)
            int r2 = android.view.View.MeasureSpec.makeMeasureSpec(r2, r7)
            goto L_0x00f7
        L_0x0288:
            r5 = 0
            goto L_0x0187
        L_0x028b:
            r0 = r19
            android.widget.LinearLayout r6 = r0.w
            if (r6 == 0) goto L_0x0194
            if (r5 == 0) goto L_0x02d0
            r0 = r19
            int r6 = r0.C
            int r6 = r6 * 2
        L_0x0299:
            r7 = 0
            int r2 = r2 - r6
            int r2 = java.lang.Math.max(r7, r2)
            r7 = 0
            int r1 = r1 - r6
            int r1 = java.lang.Math.max(r7, r1)
            r0 = r19
            android.widget.LinearLayout r6 = r0.w
            r7 = -2147483648(0xffffffff80000000, float:-0.0)
            int r7 = android.view.View.MeasureSpec.makeMeasureSpec(r2, r7)
            r8 = 1073741824(0x40000000, float:2.0)
            int r8 = android.view.View.MeasureSpec.makeMeasureSpec(r10, r8)
            r6.measure(r7, r8)
            r0 = r19
            android.widget.LinearLayout r6 = r0.w
            int r6 = r6.getMeasuredWidth()
            r7 = 0
            int r2 = r2 - r6
            int r2 = java.lang.Math.max(r7, r2)
            r7 = 0
            int r1 = r1 - r6
            int r1 = java.lang.Math.max(r7, r1)
            r6 = r1
            r7 = r2
            goto L_0x0196
        L_0x02d0:
            r0 = r19
            int r6 = r0.C
            goto L_0x0299
        L_0x02d5:
            r0 = r19
            android.support.v7.internal.widget.ScrollingTabContainerView r6 = r0.x
            if (r6 == 0) goto L_0x0194
            if (r5 == 0) goto L_0x031a
            r0 = r19
            int r6 = r0.C
            int r6 = r6 * 2
        L_0x02e3:
            r7 = 0
            int r2 = r2 - r6
            int r2 = java.lang.Math.max(r7, r2)
            r7 = 0
            int r1 = r1 - r6
            int r1 = java.lang.Math.max(r7, r1)
            r0 = r19
            android.support.v7.internal.widget.ScrollingTabContainerView r6 = r0.x
            r7 = -2147483648(0xffffffff80000000, float:-0.0)
            int r7 = android.view.View.MeasureSpec.makeMeasureSpec(r2, r7)
            r8 = 1073741824(0x40000000, float:2.0)
            int r8 = android.view.View.MeasureSpec.makeMeasureSpec(r10, r8)
            r6.measure(r7, r8)
            r0 = r19
            android.support.v7.internal.widget.ScrollingTabContainerView r6 = r0.x
            int r6 = r6.getMeasuredWidth()
            r7 = 0
            int r2 = r2 - r6
            int r2 = java.lang.Math.max(r7, r2)
            r7 = 0
            int r1 = r1 - r6
            int r1 = java.lang.Math.max(r7, r1)
            r6 = r1
            r7 = r2
            goto L_0x0196
        L_0x031a:
            r0 = r19
            int r6 = r0.C
            goto L_0x02e3
        L_0x031f:
            r0 = r19
            int r2 = r0.j
            r2 = r2 & 16
            if (r2 == 0) goto L_0x03a8
            r0 = r19
            android.view.View r2 = r0.y
            if (r2 == 0) goto L_0x03a8
            r0 = r19
            android.view.View r1 = r0.y
            r12 = r1
            goto L_0x01a2
        L_0x0334:
            r1 = 0
            r11 = r1
            goto L_0x01b6
        L_0x0338:
            int r9 = r2.height
            r16 = -2
            r0 = r16
            if (r9 == r0) goto L_0x0344
            r9 = 1073741824(0x40000000, float:2.0)
            goto L_0x01cc
        L_0x0344:
            r9 = -2147483648(0xffffffff80000000, float:-0.0)
            goto L_0x01cc
        L_0x0348:
            r1 = -2147483648(0xffffffff80000000, float:-0.0)
            goto L_0x01ed
        L_0x034c:
            r10 = r7
            goto L_0x01f9
        L_0x034f:
            r11 = 19
            goto L_0x0204
        L_0x0353:
            r0 = r19
            r0.setMeasuredDimension(r14, r2)
        L_0x0358:
            r0 = r19
            android.support.v7.internal.widget.ActionBarContextView r1 = r0.M
            if (r1 == 0) goto L_0x0369
            r0 = r19
            android.support.v7.internal.widget.ActionBarContextView r1 = r0.M
            int r2 = r19.getMeasuredHeight()
            r1.setContentHeight(r2)
        L_0x0369:
            r0 = r19
            android.support.v7.internal.widget.ProgressBarICS r1 = r0.z
            if (r1 == 0) goto L_0x0044
            r0 = r19
            android.support.v7.internal.widget.ProgressBarICS r1 = r0.z
            int r1 = r1.getVisibility()
            r2 = 8
            if (r1 == r2) goto L_0x0044
            r0 = r19
            android.support.v7.internal.widget.ProgressBarICS r1 = r0.z
            r0 = r19
            int r2 = r0.B
            int r2 = r2 * 2
            int r2 = r14 - r2
            r3 = 1073741824(0x40000000, float:2.0)
            int r2 = android.view.View.MeasureSpec.makeMeasureSpec(r2, r3)
            int r3 = r19.getMeasuredHeight()
            r4 = -2147483648(0xffffffff80000000, float:-0.0)
            int r3 = android.view.View.MeasureSpec.makeMeasureSpec(r3, r4)
            r1.measure(r2, r3)
            goto L_0x0044
        L_0x039c:
            r0 = r19
            r0.setMeasuredDimension(r14, r3)
            goto L_0x0358
        L_0x03a2:
            r1 = r2
            goto L_0x026c
        L_0x03a5:
            r2 = r10
            goto L_0x0217
        L_0x03a8:
            r12 = r1
            goto L_0x01a2
        L_0x03ab:
            r1 = r4
            r2 = r5
            goto L_0x0116
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.internal.widget.ActionBarView.onMeasure(int, int):void");
    }

    public void onRestoreInstanceState(Parcelable parcelable) {
        SupportMenuItem findItem;
        SavedState savedState = (SavedState) parcelable;
        super.onRestoreInstanceState(savedState.getSuperState());
        if (!(savedState.f43a == 0 || this.R == null || this.L == null || (findItem = this.L.findItem(savedState.f43a)) == null)) {
            findItem.expandActionView();
        }
        if (savedState.b) {
            b();
        }
    }

    public Parcelable onSaveInstanceState() {
        SavedState savedState = new SavedState(super.onSaveInstanceState());
        if (!(this.R == null || this.R.b == null)) {
            savedState.f43a = this.R.b.d();
        }
        savedState.b = c();
        return savedState;
    }

    public void setCallback(c cVar) {
        this.P = cVar;
    }

    public void setCollapsable(boolean z2) {
        this.J = z2;
    }

    public /* bridge */ /* synthetic */ void setContentHeight(int i2) {
        super.setContentHeight(i2);
    }

    public void setContextView(ActionBarContextView actionBarContextView) {
        this.M = actionBarContextView;
    }

    public void setCustomNavigationView(View view) {
        boolean z2 = (this.j & 16) != 0;
        if (this.y != null && z2) {
            removeView(this.y);
        }
        this.y = view;
        if (this.y != null && z2) {
            addView(this.y);
        }
    }

    public void setDisplayOptions(int i2) {
        int i3 = 8;
        int i4 = -1;
        boolean z2 = true;
        if (this.j != -1) {
            i4 = this.j ^ i2;
        }
        this.j = i2;
        if ((i4 & 31) != 0) {
            boolean z3 = (i2 & 2) != 0;
            this.p.setVisibility((!z3 || this.g != null) ? 8 : 0);
            if ((i4 & 4) != 0) {
                boolean z4 = (i2 & 4) != 0;
                this.p.a(z4);
                if (z4) {
                    setHomeButtonEnabled(true);
                }
            }
            if ((i4 & 1) != 0) {
                this.p.a(this.n != null && (i2 & 1) != 0 ? this.n : this.m);
            }
            if ((i4 & 8) != 0) {
                if ((i2 & 8) != 0) {
                    g();
                } else {
                    removeView(this.r);
                }
            }
            if (!(this.r == null || (i4 & 6) == 0)) {
                boolean z5 = (this.j & 4) != 0;
                View view = this.u;
                if (!z3) {
                    i3 = z5 ? 0 : 4;
                }
                view.setVisibility(i3);
                LinearLayout linearLayout = this.r;
                if (z3 || !z5) {
                    z2 = false;
                }
                linearLayout.setEnabled(z2);
            }
            if (!((i4 & 16) == 0 || this.y == null)) {
                if ((i2 & 16) != 0) {
                    addView(this.y);
                } else {
                    removeView(this.y);
                }
            }
            requestLayout();
        } else {
            invalidate();
        }
        if (!this.p.isEnabled()) {
            this.p.setContentDescription(null);
        } else if ((i2 & 4) != 0) {
            this.p.setContentDescription(this.o.getResources().getText(h.abc_action_bar_up_description));
        } else {
            this.p.setContentDescription(this.o.getResources().getText(h.abc_action_bar_home_description));
        }
    }

    public void setDropdownAdapter(SpinnerAdapter spinnerAdapter) {
        this.O = spinnerAdapter;
        if (this.v != null) {
            this.v.a(spinnerAdapter);
        }
    }

    public void setDropdownSelectedPosition(int i2) {
        this.v.a(i2);
    }

    public void setEmbeddedTabView(ScrollingTabContainerView scrollingTabContainerView) {
        if (this.x != null) {
            removeView(this.x);
        }
        this.x = scrollingTabContainerView;
        this.I = scrollingTabContainerView != null;
        if (this.I && this.i == 2) {
            addView(this.x);
            ViewGroup.LayoutParams layoutParams = this.x.getLayoutParams();
            layoutParams.width = -2;
            layoutParams.height = -1;
            scrollingTabContainerView.setAllowCollapse(true);
        }
    }

    public void setHomeAsUpIndicator(int i2) {
        this.p.a(i2);
    }

    public void setHomeAsUpIndicator(Drawable drawable) {
        this.p.b(drawable);
    }

    public void setHomeButtonEnabled(boolean z2) {
        this.p.setEnabled(z2);
        this.p.setFocusable(z2);
        if (!z2) {
            this.p.setContentDescription(null);
        } else if ((this.j & 4) != 0) {
            this.p.setContentDescription(this.o.getResources().getText(h.abc_action_bar_up_description));
        } else {
            this.p.setContentDescription(this.o.getResources().getText(h.abc_action_bar_home_description));
        }
    }

    public void setIcon(int i2) {
        setIcon(this.o.getResources().getDrawable(i2));
    }

    public void setIcon(Drawable drawable) {
        this.m = drawable;
        if (drawable != null && ((this.j & 1) == 0 || this.n == null)) {
            this.p.a(drawable);
        }
        if (this.g != null) {
            this.q.a(this.m.getConstantState().newDrawable(getResources()));
        }
    }

    public void setLogo(int i2) {
        setLogo(this.o.getResources().getDrawable(i2));
    }

    public void setLogo(Drawable drawable) {
        this.n = drawable;
        if (drawable != null && (this.j & 1) != 0) {
            this.p.a(drawable);
        }
    }

    public void setNavigationMode(int i2) {
        int i3 = this.i;
        if (i2 != i3) {
            switch (i3) {
                case 1:
                    if (this.w != null) {
                        removeView(this.w);
                        break;
                    }
                    break;
                case 2:
                    if (this.x != null && this.I) {
                        removeView(this.x);
                        break;
                    }
            }
            switch (i2) {
                case 1:
                    if (this.v == null) {
                        this.v = new aj(this.o, null, b.actionDropDownStyle);
                        this.w = (LinearLayout) LayoutInflater.from(this.o).inflate(g.abc_action_bar_view_list_nav_layout, (ViewGroup) null);
                        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-2, -1);
                        layoutParams.gravity = 17;
                        this.w.addView(this.v, layoutParams);
                    }
                    if (this.v.e() != this.O) {
                        this.v.a(this.O);
                    }
                    this.v.a(this.S);
                    addView(this.w);
                    break;
                case 2:
                    if (this.x != null && this.I) {
                        addView(this.x);
                        break;
                    }
            }
            this.i = i2;
            requestLayout();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.internal.view.menu.ActionMenuPresenter.a(int, boolean):void
     arg types: [int, int]
     candidates:
      android.support.v7.internal.view.menu.ActionMenuPresenter.a(android.support.v7.internal.view.menu.ActionMenuPresenter, android.support.v7.internal.view.menu.c):android.support.v7.internal.view.menu.c
      android.support.v7.internal.view.menu.ActionMenuPresenter.a(android.support.v7.internal.view.menu.ActionMenuPresenter, android.support.v7.internal.view.menu.d):android.support.v7.internal.view.menu.d
      android.support.v7.internal.view.menu.ActionMenuPresenter.a(android.support.v7.internal.view.menu.ActionMenuPresenter, android.support.v7.internal.view.menu.f):android.support.v7.internal.view.menu.f
      android.support.v7.internal.view.menu.ActionMenuPresenter.a(android.content.Context, android.support.v7.internal.view.menu.n):void
      android.support.v7.internal.view.menu.ActionMenuPresenter.a(android.support.v7.internal.view.menu.n, boolean):void
      android.support.v7.internal.view.menu.ActionMenuPresenter.a(android.support.v7.internal.view.menu.r, android.support.v7.internal.view.menu.x):void
      android.support.v7.internal.view.menu.ActionMenuPresenter.a(int, android.support.v7.internal.view.menu.r):boolean
      android.support.v7.internal.view.menu.ActionMenuPresenter.a(android.view.ViewGroup, int):boolean
      android.support.v7.internal.view.menu.k.a(android.content.Context, android.support.v7.internal.view.menu.n):void
      android.support.v7.internal.view.menu.k.a(android.support.v7.internal.view.menu.n, boolean):void
      android.support.v7.internal.view.menu.k.a(android.support.v7.internal.view.menu.r, android.support.v7.internal.view.menu.x):void
      android.support.v7.internal.view.menu.k.a(android.view.View, int):void
      android.support.v7.internal.view.menu.k.a(int, android.support.v7.internal.view.menu.r):boolean
      android.support.v7.internal.view.menu.k.a(android.support.v7.internal.view.menu.n, android.support.v7.internal.view.menu.r):boolean
      android.support.v7.internal.view.menu.k.a(android.view.ViewGroup, int):boolean
      android.support.v7.internal.view.menu.u.a(android.content.Context, android.support.v7.internal.view.menu.n):void
      android.support.v7.internal.view.menu.u.a(android.support.v7.internal.view.menu.n, boolean):void
      android.support.v7.internal.view.menu.u.a(android.support.v7.internal.view.menu.n, android.support.v7.internal.view.menu.r):boolean
      android.support.v7.internal.view.menu.ActionMenuPresenter.a(int, boolean):void */
    public void setSplitActionBar(boolean z2) {
        if (this.d != z2) {
            if (this.f50a != null) {
                ViewGroup viewGroup = (ViewGroup) this.f50a.getParent();
                if (viewGroup != null) {
                    viewGroup.removeView(this.f50a);
                }
                if (z2) {
                    if (this.c != null) {
                        this.c.addView(this.f50a);
                    }
                    this.f50a.getLayoutParams().width = -1;
                } else {
                    addView(this.f50a);
                    this.f50a.getLayoutParams().width = -2;
                }
                this.f50a.requestLayout();
            }
            if (this.c != null) {
                this.c.setVisibility(z2 ? 0 : 8);
            }
            if (this.b != null) {
                if (!z2) {
                    this.b.a(getResources().getBoolean(android.support.v7.b.c.abc_action_bar_expanded_action_views_exclusive));
                } else {
                    this.b.a(false);
                    this.b.a(getContext().getResources().getDisplayMetrics().widthPixels, true);
                    this.b.a(Integer.MAX_VALUE);
                }
            }
            super.setSplitActionBar(z2);
        }
    }

    public /* bridge */ /* synthetic */ void setSplitView(ActionBarContainer actionBarContainer) {
        super.setSplitView(actionBarContainer);
    }

    public /* bridge */ /* synthetic */ void setSplitWhenNarrow(boolean z2) {
        super.setSplitWhenNarrow(z2);
    }

    public void setSubtitle(CharSequence charSequence) {
        int i2 = 0;
        this.l = charSequence;
        if (this.t != null) {
            this.t.setText(charSequence);
            this.t.setVisibility(charSequence != null ? 0 : 8);
            boolean z2 = this.g == null && (this.j & 8) != 0 && (!TextUtils.isEmpty(this.k) || !TextUtils.isEmpty(this.l));
            LinearLayout linearLayout = this.r;
            if (!z2) {
                i2 = 8;
            }
            linearLayout.setVisibility(i2);
        }
    }

    public void setTitle(CharSequence charSequence) {
        this.H = true;
        setTitleImpl(charSequence);
    }

    public /* bridge */ /* synthetic */ void setVisibility(int i2) {
        super.setVisibility(i2);
    }

    public void setWindowCallback(Window.Callback callback) {
        this.h = callback;
    }

    public void setWindowTitle(CharSequence charSequence) {
        if (!this.H) {
            setTitleImpl(charSequence);
        }
    }

    public boolean shouldDelayChildPressedState() {
        return false;
    }
}
