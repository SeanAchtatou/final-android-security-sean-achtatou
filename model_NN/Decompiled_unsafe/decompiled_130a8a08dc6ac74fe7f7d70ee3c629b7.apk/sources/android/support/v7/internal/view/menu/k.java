package android.support.v7.internal.view.menu;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;

public abstract class k implements u {

    /* renamed from: a  reason: collision with root package name */
    private v f30a;
    private int b;
    protected Context c;
    protected Context d;
    protected n e;
    protected LayoutInflater f;
    protected LayoutInflater g;
    protected w h;
    private int i;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public w a(ViewGroup viewGroup) {
        if (this.h == null) {
            this.h = (w) this.f.inflate(this.b, viewGroup, false);
            this.h.a(this.e);
            b(true);
        }
        return this.h;
    }

    public View a(r rVar, View view, ViewGroup viewGroup) {
        x b2 = view instanceof x ? (x) view : b(viewGroup);
        a(rVar, b2);
        return (View) b2;
    }

    public void a(Context context, n nVar) {
        this.d = context;
        this.g = LayoutInflater.from(this.d);
        this.e = nVar;
    }

    public void a(n nVar, boolean z) {
        if (this.f30a != null) {
            this.f30a.a(nVar, z);
        }
    }

    public abstract void a(r rVar, x xVar);

    public void a(v vVar) {
        this.f30a = vVar;
    }

    /* access modifiers changed from: protected */
    public void a(View view, int i2) {
        ViewGroup viewGroup = (ViewGroup) view.getParent();
        if (viewGroup != null) {
            viewGroup.removeView(view);
        }
        ((ViewGroup) this.h).addView(view, i2);
    }

    public boolean a(int i2, r rVar) {
        return true;
    }

    public boolean a(n nVar, r rVar) {
        return false;
    }

    public boolean a(y yVar) {
        if (this.f30a != null) {
            return this.f30a.a(yVar);
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public boolean a(ViewGroup viewGroup, int i2) {
        viewGroup.removeViewAt(i2);
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public x b(ViewGroup viewGroup) {
        return (x) this.f.inflate(this.i, viewGroup, false);
    }

    public void b(boolean z) {
        int i2;
        int i3;
        ViewGroup viewGroup = (ViewGroup) this.h;
        if (viewGroup != null) {
            if (this.e != null) {
                this.e.i();
                ArrayList h2 = this.e.h();
                int size = h2.size();
                int i4 = 0;
                i2 = 0;
                while (i4 < size) {
                    r rVar = (r) h2.get(i4);
                    if (a(i2, rVar)) {
                        View childAt = viewGroup.getChildAt(i2);
                        r itemData = childAt instanceof x ? ((x) childAt).getItemData() : null;
                        View a2 = a(rVar, childAt, viewGroup);
                        if (rVar != itemData) {
                            a2.setPressed(false);
                        }
                        if (a2 != childAt) {
                            a(a2, i2);
                        }
                        i3 = i2 + 1;
                    } else {
                        i3 = i2;
                    }
                    i4++;
                    i2 = i3;
                }
            } else {
                i2 = 0;
            }
            while (i2 < viewGroup.getChildCount()) {
                if (!a(viewGroup, i2)) {
                    i2++;
                }
            }
        }
    }

    public boolean b(n nVar, r rVar) {
        return false;
    }

    public boolean f() {
        return false;
    }
}
