package android.support.v7.internal.view.menu;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.os.Build;
import android.support.v7.b.b;
import android.support.v7.b.j;
import android.support.v7.internal.widget.LinearLayoutICS;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityEvent;

public class ActionMenuView extends LinearLayoutICS implements p, w {

    /* renamed from: a  reason: collision with root package name */
    private n f19a;
    private boolean b;
    private ActionMenuPresenter c;
    private boolean d;
    private int e;
    private int f;
    private int g;
    private int h;
    private int i;

    public ActionMenuView(Context context) {
        this(context, null);
    }

    public ActionMenuView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        setBaselineAligned(false);
        float f2 = context.getResources().getDisplayMetrics().density;
        this.f = (int) (56.0f * f2);
        this.g = (int) (f2 * 4.0f);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, j.ActionBar, b.actionBarStyle, 0);
        this.i = obtainStyledAttributes.getDimensionPixelSize(1, 0);
        obtainStyledAttributes.recycle();
    }

    static int a(View view, int i2, int i3, int i4, int i5) {
        int i6;
        boolean z = false;
        j jVar = (j) view.getLayoutParams();
        int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(View.MeasureSpec.getSize(i4) - i5, View.MeasureSpec.getMode(i4));
        ActionMenuItemView actionMenuItemView = view instanceof ActionMenuItemView ? (ActionMenuItemView) view : null;
        boolean z2 = actionMenuItemView != null && actionMenuItemView.b();
        if (i3 <= 0 || (z2 && i3 < 2)) {
            i6 = 0;
        } else {
            view.measure(View.MeasureSpec.makeMeasureSpec(i2 * i3, Integer.MIN_VALUE), makeMeasureSpec);
            int measuredWidth = view.getMeasuredWidth();
            i6 = measuredWidth / i2;
            if (measuredWidth % i2 != 0) {
                i6++;
            }
            if (z2 && i6 < 2) {
                i6 = 2;
            }
        }
        if (!jVar.f29a && z2) {
            z = true;
        }
        jVar.d = z;
        jVar.b = i6;
        view.measure(View.MeasureSpec.makeMeasureSpec(i6 * i2, 1073741824), makeMeasureSpec);
        return i6;
    }

    /* JADX WARNING: Removed duplicated region for block: B:112:0x029a  */
    /* JADX WARNING: Removed duplicated region for block: B:86:0x01f3  */
    /* JADX WARNING: Removed duplicated region for block: B:90:0x0202  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(int r35, int r36) {
        /*
            r34 = this;
            int r24 = android.view.View.MeasureSpec.getMode(r36)
            int r7 = android.view.View.MeasureSpec.getSize(r35)
            int r20 = android.view.View.MeasureSpec.getSize(r36)
            int r6 = r34.getPaddingLeft()
            int r8 = r34.getPaddingRight()
            int r8 = r8 + r6
            int r6 = r34.getPaddingTop()
            int r9 = r34.getPaddingBottom()
            int r19 = r6 + r9
            r6 = 1073741824(0x40000000, float:2.0)
            r0 = r24
            if (r0 != r6) goto L_0x0047
            int r6 = r20 - r19
            r9 = 1073741824(0x40000000, float:2.0)
            int r6 = android.view.View.MeasureSpec.makeMeasureSpec(r6, r9)
            r9 = r6
        L_0x002e:
            int r25 = r7 - r8
            r0 = r34
            int r6 = r0.f
            int r10 = r25 / r6
            r0 = r34
            int r6 = r0.f
            int r6 = r25 % r6
            if (r10 != 0) goto L_0x0059
            r6 = 0
            r0 = r34
            r1 = r25
            r0.setMeasuredDimension(r1, r6)
        L_0x0046:
            return
        L_0x0047:
            r0 = r34
            int r6 = r0.i
            int r9 = r20 - r19
            int r6 = java.lang.Math.min(r6, r9)
            r9 = -2147483648(0xffffffff80000000, float:-0.0)
            int r6 = android.view.View.MeasureSpec.makeMeasureSpec(r6, r9)
            r9 = r6
            goto L_0x002e
        L_0x0059:
            r0 = r34
            int r7 = r0.f
            int r6 = r6 / r10
            int r26 = r7 + r6
            r17 = 0
            r16 = 0
            r11 = 0
            r7 = 0
            r12 = 0
            r14 = 0
            int r27 = r34.getChildCount()
            r6 = 0
            r18 = r6
        L_0x0070:
            r0 = r18
            r1 = r27
            if (r0 >= r1) goto L_0x0125
            r0 = r34
            r1 = r18
            android.view.View r8 = r0.getChildAt(r1)
            int r6 = r8.getVisibility()
            r13 = 8
            if (r6 != r13) goto L_0x0099
            r8 = r7
            r13 = r17
            r6 = r14
            r14 = r10
            r10 = r16
        L_0x008d:
            int r15 = r18 + 1
            r18 = r15
            r16 = r10
            r17 = r13
            r10 = r14
            r14 = r6
            r7 = r8
            goto L_0x0070
        L_0x0099:
            boolean r0 = r8 instanceof android.support.v7.internal.view.menu.ActionMenuItemView
            r21 = r0
            int r13 = r7 + 1
            if (r21 == 0) goto L_0x00b5
            r0 = r34
            int r6 = r0.g
            r7 = 0
            r0 = r34
            int r0 = r0.g
            r22 = r0
            r23 = 0
            r0 = r22
            r1 = r23
            r8.setPadding(r6, r7, r0, r1)
        L_0x00b5:
            android.view.ViewGroup$LayoutParams r6 = r8.getLayoutParams()
            android.support.v7.internal.view.menu.j r6 = (android.support.v7.internal.view.menu.j) r6
            r7 = 0
            r6.f = r7
            r7 = 0
            r6.c = r7
            r7 = 0
            r6.b = r7
            r7 = 0
            r6.d = r7
            r7 = 0
            r6.leftMargin = r7
            r7 = 0
            r6.rightMargin = r7
            if (r21 == 0) goto L_0x0121
            r7 = r8
            android.support.v7.internal.view.menu.ActionMenuItemView r7 = (android.support.v7.internal.view.menu.ActionMenuItemView) r7
            boolean r7 = r7.b()
            if (r7 == 0) goto L_0x0121
            r7 = 1
        L_0x00d9:
            r6.e = r7
            boolean r7 = r6.f29a
            if (r7 == 0) goto L_0x0123
            r7 = 1
        L_0x00e0:
            r0 = r26
            r1 = r19
            int r21 = a(r8, r0, r7, r9, r1)
            r0 = r16
            r1 = r21
            int r16 = java.lang.Math.max(r0, r1)
            boolean r7 = r6.d
            if (r7 == 0) goto L_0x0352
            int r7 = r11 + 1
        L_0x00f6:
            boolean r6 = r6.f29a
            if (r6 == 0) goto L_0x034f
            r6 = 1
        L_0x00fb:
            int r12 = r10 - r21
            int r8 = r8.getMeasuredHeight()
            r0 = r17
            int r8 = java.lang.Math.max(r0, r8)
            r10 = 1
            r0 = r21
            if (r0 != r10) goto L_0x033f
            r10 = 1
            int r10 = r10 << r18
            long r10 = (long) r10
            long r10 = r10 | r14
            r14 = r12
            r12 = r6
            r32 = r13
            r13 = r8
            r8 = r32
            r33 = r7
            r6 = r10
            r10 = r16
            r11 = r33
            goto L_0x008d
        L_0x0121:
            r7 = 0
            goto L_0x00d9
        L_0x0123:
            r7 = r10
            goto L_0x00e0
        L_0x0125:
            if (r12 == 0) goto L_0x0162
            r6 = 2
            if (r7 != r6) goto L_0x0162
            r6 = 1
            r8 = r6
        L_0x012c:
            r18 = 0
            r22 = r14
            r19 = r10
        L_0x0132:
            if (r11 <= 0) goto L_0x033b
            if (r19 <= 0) goto L_0x033b
            r13 = 2147483647(0x7fffffff, float:NaN)
            r14 = 0
            r10 = 0
            r6 = 0
            r21 = r6
        L_0x013f:
            r0 = r21
            r1 = r27
            if (r0 >= r1) goto L_0x0185
            r0 = r34
            r1 = r21
            android.view.View r6 = r0.getChildAt(r1)
            android.view.ViewGroup$LayoutParams r6 = r6.getLayoutParams()
            android.support.v7.internal.view.menu.j r6 = (android.support.v7.internal.view.menu.j) r6
            boolean r0 = r6.d
            r28 = r0
            if (r28 != 0) goto L_0x0165
            r6 = r10
            r10 = r13
        L_0x015b:
            int r13 = r21 + 1
            r21 = r13
            r13 = r10
            r10 = r6
            goto L_0x013f
        L_0x0162:
            r6 = 0
            r8 = r6
            goto L_0x012c
        L_0x0165:
            int r0 = r6.b
            r28 = r0
            r0 = r28
            if (r0 >= r13) goto L_0x0175
            int r10 = r6.b
            r6 = 1
            int r6 = r6 << r21
            long r14 = (long) r6
            r6 = 1
            goto L_0x015b
        L_0x0175:
            int r6 = r6.b
            if (r6 != r13) goto L_0x0337
            r6 = 1
            int r6 = r6 << r21
            long r0 = (long) r6
            r28 = r0
            long r14 = r14 | r28
            int r6 = r10 + 1
            r10 = r13
            goto L_0x015b
        L_0x0185:
            long r22 = r22 | r14
            r0 = r19
            if (r10 <= r0) goto L_0x0212
            r14 = r22
        L_0x018d:
            if (r12 != 0) goto L_0x0297
            r6 = 1
            if (r7 != r6) goto L_0x0297
            r6 = 1
        L_0x0193:
            if (r19 <= 0) goto L_0x032f
            r10 = 0
            int r8 = (r14 > r10 ? 1 : (r14 == r10 ? 0 : -1))
            if (r8 == 0) goto L_0x032f
            int r7 = r7 + -1
            r0 = r19
            if (r0 < r7) goto L_0x01a8
            if (r6 != 0) goto L_0x01a8
            r7 = 1
            r0 = r16
            if (r0 <= r7) goto L_0x032f
        L_0x01a8:
            int r7 = java.lang.Long.bitCount(r14)
            float r7 = (float) r7
            if (r6 != 0) goto L_0x032c
            r10 = 1
            long r10 = r10 & r14
            r12 = 0
            int r6 = (r10 > r12 ? 1 : (r10 == r12 ? 0 : -1))
            if (r6 == 0) goto L_0x01cc
            r6 = 0
            r0 = r34
            android.view.View r6 = r0.getChildAt(r6)
            android.view.ViewGroup$LayoutParams r6 = r6.getLayoutParams()
            android.support.v7.internal.view.menu.j r6 = (android.support.v7.internal.view.menu.j) r6
            boolean r6 = r6.e
            if (r6 != 0) goto L_0x01cc
            r6 = 1056964608(0x3f000000, float:0.5)
            float r7 = r7 - r6
        L_0x01cc:
            r6 = 1
            int r8 = r27 + -1
            int r6 = r6 << r8
            long r10 = (long) r6
            long r10 = r10 & r14
            r12 = 0
            int r6 = (r10 > r12 ? 1 : (r10 == r12 ? 0 : -1))
            if (r6 == 0) goto L_0x032c
            int r6 = r27 + -1
            r0 = r34
            android.view.View r6 = r0.getChildAt(r6)
            android.view.ViewGroup$LayoutParams r6 = r6.getLayoutParams()
            android.support.v7.internal.view.menu.j r6 = (android.support.v7.internal.view.menu.j) r6
            boolean r6 = r6.e
            if (r6 != 0) goto L_0x032c
            r6 = 1056964608(0x3f000000, float:0.5)
            float r6 = r7 - r6
        L_0x01ee:
            r7 = 0
            int r7 = (r6 > r7 ? 1 : (r6 == r7 ? 0 : -1))
            if (r7 <= 0) goto L_0x029a
            int r7 = r19 * r26
            float r7 = (float) r7
            float r6 = r7 / r6
            int r6 = (int) r6
            r7 = r6
        L_0x01fa:
            r6 = 0
            r10 = r6
            r8 = r18
        L_0x01fe:
            r0 = r27
            if (r10 >= r0) goto L_0x02e3
            r6 = 1
            int r6 = r6 << r10
            long r12 = (long) r6
            long r12 = r12 & r14
            r18 = 0
            int r6 = (r12 > r18 ? 1 : (r12 == r18 ? 0 : -1))
            if (r6 != 0) goto L_0x029e
            r6 = r8
        L_0x020d:
            int r8 = r10 + 1
            r10 = r8
            r8 = r6
            goto L_0x01fe
        L_0x0212:
            int r21 = r13 + 1
            r6 = 0
            r13 = r6
            r10 = r19
            r18 = r22
        L_0x021a:
            r0 = r27
            if (r13 >= r0) goto L_0x028e
            r0 = r34
            android.view.View r22 = r0.getChildAt(r13)
            android.view.ViewGroup$LayoutParams r6 = r22.getLayoutParams()
            android.support.v7.internal.view.menu.j r6 = (android.support.v7.internal.view.menu.j) r6
            r23 = 1
            int r23 = r23 << r13
            r0 = r23
            long r0 = (long) r0
            r28 = r0
            long r28 = r28 & r14
            r30 = 0
            int r23 = (r28 > r30 ? 1 : (r28 == r30 ? 0 : -1))
            if (r23 != 0) goto L_0x024e
            int r6 = r6.b
            r0 = r21
            if (r6 != r0) goto L_0x0334
            r6 = 1
            int r6 = r6 << r13
            long r0 = (long) r6
            r22 = r0
            long r18 = r18 | r22
            r6 = r10
        L_0x0249:
            int r10 = r13 + 1
            r13 = r10
            r10 = r6
            goto L_0x021a
        L_0x024e:
            if (r8 == 0) goto L_0x027b
            boolean r0 = r6.e
            r23 = r0
            if (r23 == 0) goto L_0x027b
            r23 = 1
            r0 = r23
            if (r10 != r0) goto L_0x027b
            r0 = r34
            int r0 = r0.g
            r23 = r0
            int r23 = r23 + r26
            r28 = 0
            r0 = r34
            int r0 = r0.g
            r29 = r0
            r30 = 0
            r0 = r22
            r1 = r23
            r2 = r28
            r3 = r29
            r4 = r30
            r0.setPadding(r1, r2, r3, r4)
        L_0x027b:
            int r0 = r6.b
            r22 = r0
            int r22 = r22 + 1
            r0 = r22
            r6.b = r0
            r22 = 1
            r0 = r22
            r6.f = r0
            int r6 = r10 + -1
            goto L_0x0249
        L_0x028e:
            r6 = 1
            r22 = r18
            r18 = r6
            r19 = r10
            goto L_0x0132
        L_0x0297:
            r6 = 0
            goto L_0x0193
        L_0x029a:
            r6 = 0
            r7 = r6
            goto L_0x01fa
        L_0x029e:
            r0 = r34
            android.view.View r11 = r0.getChildAt(r10)
            android.view.ViewGroup$LayoutParams r6 = r11.getLayoutParams()
            android.support.v7.internal.view.menu.j r6 = (android.support.v7.internal.view.menu.j) r6
            boolean r11 = r11 instanceof android.support.v7.internal.view.menu.ActionMenuItemView
            if (r11 == 0) goto L_0x02c1
            r6.c = r7
            r8 = 1
            r6.f = r8
            if (r10 != 0) goto L_0x02be
            boolean r8 = r6.e
            if (r8 != 0) goto L_0x02be
            int r8 = -r7
            int r8 = r8 / 2
            r6.leftMargin = r8
        L_0x02be:
            r6 = 1
            goto L_0x020d
        L_0x02c1:
            boolean r11 = r6.f29a
            if (r11 == 0) goto L_0x02d2
            r6.c = r7
            r8 = 1
            r6.f = r8
            int r8 = -r7
            int r8 = r8 / 2
            r6.rightMargin = r8
            r6 = 1
            goto L_0x020d
        L_0x02d2:
            if (r10 == 0) goto L_0x02d8
            int r11 = r7 / 2
            r6.leftMargin = r11
        L_0x02d8:
            int r11 = r27 + -1
            if (r10 == r11) goto L_0x02e0
            int r11 = r7 / 2
            r6.rightMargin = r11
        L_0x02e0:
            r6 = r8
            goto L_0x020d
        L_0x02e3:
            r6 = 0
            r10 = r6
        L_0x02e5:
            if (r8 == 0) goto L_0x0312
            r6 = 0
            r7 = r6
        L_0x02e9:
            r0 = r27
            if (r7 >= r0) goto L_0x0312
            r0 = r34
            android.view.View r8 = r0.getChildAt(r7)
            android.view.ViewGroup$LayoutParams r6 = r8.getLayoutParams()
            android.support.v7.internal.view.menu.j r6 = (android.support.v7.internal.view.menu.j) r6
            boolean r11 = r6.f
            if (r11 != 0) goto L_0x0301
        L_0x02fd:
            int r6 = r7 + 1
            r7 = r6
            goto L_0x02e9
        L_0x0301:
            int r11 = r6.b
            int r11 = r11 * r26
            int r6 = r6.c
            int r6 = r6 + r11
            r11 = 1073741824(0x40000000, float:2.0)
            int r6 = android.view.View.MeasureSpec.makeMeasureSpec(r6, r11)
            r8.measure(r6, r9)
            goto L_0x02fd
        L_0x0312:
            r6 = 1073741824(0x40000000, float:2.0)
            r0 = r24
            if (r0 == r6) goto L_0x0329
        L_0x0318:
            r0 = r34
            r1 = r25
            r2 = r17
            r0.setMeasuredDimension(r1, r2)
            int r6 = r10 * r26
            r0 = r34
            r0.h = r6
            goto L_0x0046
        L_0x0329:
            r17 = r20
            goto L_0x0318
        L_0x032c:
            r6 = r7
            goto L_0x01ee
        L_0x032f:
            r8 = r18
            r10 = r19
            goto L_0x02e5
        L_0x0334:
            r6 = r10
            goto L_0x0249
        L_0x0337:
            r6 = r10
            r10 = r13
            goto L_0x015b
        L_0x033b:
            r14 = r22
            goto L_0x018d
        L_0x033f:
            r11 = r7
            r10 = r16
            r32 = r13
            r13 = r8
            r8 = r32
            r33 = r6
            r6 = r14
            r14 = r12
            r12 = r33
            goto L_0x008d
        L_0x034f:
            r6 = r12
            goto L_0x00fb
        L_0x0352:
            r7 = r11
            goto L_0x00f6
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.internal.view.menu.ActionMenuView.a(int, int):void");
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public j generateDefaultLayoutParams() {
        j jVar = new j(-2, -2);
        jVar.gravity = 16;
        return jVar;
    }

    /* renamed from: a */
    public j generateLayoutParams(AttributeSet attributeSet) {
        return new j(getContext(), attributeSet);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public j generateLayoutParams(ViewGroup.LayoutParams layoutParams) {
        if (!(layoutParams instanceof j)) {
            return generateDefaultLayoutParams();
        }
        j jVar = new j((j) layoutParams);
        if (jVar.gravity > 0) {
            return jVar;
        }
        jVar.gravity = 16;
        return jVar;
    }

    public void a(n nVar) {
        this.f19a = nVar;
    }

    /* access modifiers changed from: protected */
    public boolean a(int i2) {
        View childAt = getChildAt(i2 - 1);
        View childAt2 = getChildAt(i2);
        boolean z = false;
        if (i2 < getChildCount() && (childAt instanceof i)) {
            z = false | ((i) childAt).d();
        }
        return (i2 <= 0 || !(childAt2 instanceof i)) ? z : ((i) childAt2).c() | z;
    }

    /* JADX WARN: Type inference failed for: r3v0, types: [android.support.v7.internal.view.menu.r, android.view.MenuItem] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean a(android.support.v7.internal.view.menu.r r3) {
        /*
            r2 = this;
            android.support.v7.internal.view.menu.n r0 = r2.f19a
            r1 = 0
            boolean r0 = r0.a(r3, r1)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.internal.view.menu.ActionMenuView.a(android.support.v7.internal.view.menu.r):boolean");
    }

    public j b() {
        j a2 = generateDefaultLayoutParams();
        a2.f29a = true;
        return a2;
    }

    /* access modifiers changed from: protected */
    public boolean checkLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return layoutParams != null && (layoutParams instanceof j);
    }

    public boolean dispatchPopulateAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
        return false;
    }

    public int getWindowAnimations() {
        return 0;
    }

    public void onConfigurationChanged(Configuration configuration) {
        if (Build.VERSION.SDK_INT >= 8) {
            super.onConfigurationChanged(configuration);
        }
        this.c.b(false);
        if (this.c != null && this.c.e()) {
            this.c.b();
            this.c.a();
        }
    }

    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.c.c();
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i2, int i3, int i4, int i5) {
        int i6;
        int i7;
        int i8;
        int i9;
        boolean z2;
        if (!this.d) {
            super.onLayout(z, i2, i3, i4, i5);
            return;
        }
        int childCount = getChildCount();
        int i10 = (i3 + i5) / 2;
        int supportDividerWidth = getSupportDividerWidth();
        int i11 = 0;
        int i12 = 0;
        int paddingRight = ((i4 - i2) - getPaddingRight()) - getPaddingLeft();
        boolean z3 = false;
        int i13 = 0;
        while (i13 < childCount) {
            View childAt = getChildAt(i13);
            if (childAt.getVisibility() == 8) {
                z2 = z3;
                i7 = paddingRight;
                i8 = i12;
                i9 = i11;
            } else {
                j jVar = (j) childAt.getLayoutParams();
                if (jVar.f29a) {
                    int measuredWidth = childAt.getMeasuredWidth();
                    if (a(i13)) {
                        measuredWidth += supportDividerWidth;
                    }
                    int measuredHeight = childAt.getMeasuredHeight();
                    int width = (getWidth() - getPaddingRight()) - jVar.rightMargin;
                    int i14 = i10 - (measuredHeight / 2);
                    childAt.layout(width - measuredWidth, i14, width, measuredHeight + i14);
                    i7 = paddingRight - measuredWidth;
                    z2 = true;
                    i8 = i12;
                    i9 = i11;
                } else {
                    int measuredWidth2 = childAt.getMeasuredWidth() + jVar.leftMargin + jVar.rightMargin;
                    int i15 = i11 + measuredWidth2;
                    int i16 = paddingRight - measuredWidth2;
                    if (a(i13)) {
                        i15 += supportDividerWidth;
                    }
                    boolean z4 = z3;
                    i7 = i16;
                    i8 = i12 + 1;
                    i9 = i15;
                    z2 = z4;
                }
            }
            i13++;
            i11 = i9;
            i12 = i8;
            paddingRight = i7;
            z3 = z2;
        }
        if (childCount != 1 || z3) {
            int i17 = i12 - (z3 ? 0 : 1);
            int max = Math.max(0, i17 > 0 ? paddingRight / i17 : 0);
            int paddingLeft = getPaddingLeft();
            int i18 = 0;
            while (i18 < childCount) {
                View childAt2 = getChildAt(i18);
                j jVar2 = (j) childAt2.getLayoutParams();
                if (childAt2.getVisibility() == 8) {
                    i6 = paddingLeft;
                } else if (jVar2.f29a) {
                    i6 = paddingLeft;
                } else {
                    int i19 = paddingLeft + jVar2.leftMargin;
                    int measuredWidth3 = childAt2.getMeasuredWidth();
                    int measuredHeight2 = childAt2.getMeasuredHeight();
                    int i20 = i10 - (measuredHeight2 / 2);
                    childAt2.layout(i19, i20, i19 + measuredWidth3, measuredHeight2 + i20);
                    i6 = jVar2.rightMargin + measuredWidth3 + max + i19;
                }
                i18++;
                paddingLeft = i6;
            }
            return;
        }
        View childAt3 = getChildAt(0);
        int measuredWidth4 = childAt3.getMeasuredWidth();
        int measuredHeight3 = childAt3.getMeasuredHeight();
        int i21 = ((i4 - i2) / 2) - (measuredWidth4 / 2);
        int i22 = i10 - (measuredHeight3 / 2);
        childAt3.layout(i21, i22, measuredWidth4 + i21, measuredHeight3 + i22);
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        boolean z = this.d;
        this.d = View.MeasureSpec.getMode(i2) == 1073741824;
        if (z != this.d) {
            this.e = 0;
        }
        int mode = View.MeasureSpec.getMode(i2);
        if (!(!this.d || this.f19a == null || mode == this.e)) {
            this.e = mode;
            this.f19a.b(true);
        }
        if (this.d) {
            a(i2, i3);
            return;
        }
        int childCount = getChildCount();
        for (int i4 = 0; i4 < childCount; i4++) {
            j jVar = (j) getChildAt(i4).getLayoutParams();
            jVar.rightMargin = 0;
            jVar.leftMargin = 0;
        }
        super.onMeasure(i2, i3);
    }

    public void setOverflowReserved(boolean z) {
        this.b = z;
    }

    public void setPresenter(ActionMenuPresenter actionMenuPresenter) {
        this.c = actionMenuPresenter;
    }
}
