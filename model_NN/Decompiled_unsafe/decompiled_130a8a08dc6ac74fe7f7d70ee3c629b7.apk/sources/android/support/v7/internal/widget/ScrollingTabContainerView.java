package android.support.v7.internal.widget;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.drawable.Drawable;
import android.support.v7.a.d;
import android.support.v7.b.b;
import android.support.v7.b.g;
import android.support.v7.internal.view.a;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.AbsListView;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class ScrollingTabContainerView extends HorizontalScrollView implements n {

    /* renamed from: a  reason: collision with root package name */
    Runnable f48a;
    int b;
    int c;
    private ai d;
    /* access modifiers changed from: private */
    public LinearLayout e;
    private aj f;
    private boolean g;
    private final LayoutInflater h;
    private int i;
    private int j;

    public class TabView extends LinearLayout {

        /* renamed from: a  reason: collision with root package name */
        private d f49a;
        private TextView b;
        private ImageView c;
        private View d;
        private ScrollingTabContainerView e;

        public TabView(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
        }

        public void a() {
            d dVar = this.f49a;
            View c2 = dVar.c();
            if (c2 != null) {
                ViewParent parent = c2.getParent();
                if (parent != this) {
                    if (parent != null) {
                        ((ViewGroup) parent).removeView(c2);
                    }
                    addView(c2);
                }
                this.d = c2;
                if (this.b != null) {
                    this.b.setVisibility(8);
                }
                if (this.c != null) {
                    this.c.setVisibility(8);
                    this.c.setImageDrawable(null);
                    return;
                }
                return;
            }
            if (this.d != null) {
                removeView(this.d);
                this.d = null;
            }
            Drawable a2 = dVar.a();
            CharSequence b2 = dVar.b();
            if (a2 != null) {
                if (this.c == null) {
                    ImageView imageView = new ImageView(getContext());
                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-2, -2);
                    layoutParams.gravity = 16;
                    imageView.setLayoutParams(layoutParams);
                    addView(imageView, 0);
                    this.c = imageView;
                }
                this.c.setImageDrawable(a2);
                this.c.setVisibility(0);
            } else if (this.c != null) {
                this.c.setVisibility(8);
                this.c.setImageDrawable(null);
            }
            if (b2 != null) {
                if (this.b == null) {
                    r rVar = new r(getContext(), null, b.actionBarTabTextStyle);
                    rVar.setEllipsize(TextUtils.TruncateAt.END);
                    LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(-2, -2);
                    layoutParams2.gravity = 16;
                    rVar.setLayoutParams(layoutParams2);
                    addView(rVar);
                    this.b = rVar;
                }
                this.b.setText(b2);
                this.b.setVisibility(0);
            } else if (this.b != null) {
                this.b.setVisibility(8);
                this.b.setText((CharSequence) null);
            }
            if (this.c != null) {
                this.c.setContentDescription(dVar.e());
            }
        }

        public void a(d dVar) {
            this.f49a = dVar;
            a();
        }

        /* access modifiers changed from: package-private */
        public void a(ScrollingTabContainerView scrollingTabContainerView, d dVar, boolean z) {
            this.e = scrollingTabContainerView;
            this.f49a = dVar;
            if (z) {
                setGravity(19);
            }
            a();
        }

        public d getTab() {
            return this.f49a;
        }

        public void onMeasure(int i, int i2) {
            super.onMeasure(i, i2);
            int i3 = this.e != null ? this.e.b : 0;
            if (i3 > 0 && getMeasuredWidth() > i3) {
                super.onMeasure(View.MeasureSpec.makeMeasureSpec(i3, 1073741824), i2);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.widget.LinearLayout, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* access modifiers changed from: private */
    public TabView a(d dVar, boolean z) {
        TabView tabView = (TabView) this.h.inflate(g.abc_action_bar_tab, (ViewGroup) this.e, false);
        tabView.a(this, dVar, z);
        if (z) {
            tabView.setBackgroundDrawable(null);
            tabView.setLayoutParams(new AbsListView.LayoutParams(-1, this.i));
        } else {
            tabView.setFocusable(true);
            if (this.d == null) {
                this.d = new ai(this, null);
            }
            tabView.setOnClickListener(this.d);
        }
        return tabView;
    }

    private boolean a() {
        return this.f != null && this.f.getParent() == this;
    }

    private void b() {
        if (!a()) {
            if (this.f == null) {
                this.f = d();
            }
            removeView(this.e);
            addView(this.f, new ViewGroup.LayoutParams(-2, -1));
            if (this.f.e() == null) {
                this.f.a(new ah(this, null));
            }
            if (this.f48a != null) {
                removeCallbacks(this.f48a);
                this.f48a = null;
            }
            this.f.a(this.j);
        }
    }

    private boolean c() {
        if (a()) {
            removeView(this.f);
            addView(this.e, new ViewGroup.LayoutParams(-2, -1));
            setTabSelected(this.f.f());
        }
        return false;
    }

    private aj d() {
        aj ajVar = new aj(getContext(), null, b.actionDropDownStyle);
        ajVar.setLayoutParams(new LinearLayout.LayoutParams(-2, -1));
        ajVar.b((n) this);
        return ajVar;
    }

    public void a(int i2) {
        View childAt = this.e.getChildAt(i2);
        if (this.f48a != null) {
            removeCallbacks(this.f48a);
        }
        this.f48a = new ag(this, childAt);
        post(this.f48a);
    }

    public void a(k kVar, View view, int i2, long j2) {
        ((TabView) view).getTab().d();
    }

    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (this.f48a != null) {
            post(this.f48a);
        }
    }

    /* access modifiers changed from: protected */
    public void onConfigurationChanged(Configuration configuration) {
        a a2 = a.a(getContext());
        setContentHeight(a2.e());
        this.c = a2.f();
    }

    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (this.f48a != null) {
            removeCallbacks(this.f48a);
        }
    }

    public void onMeasure(int i2, int i3) {
        boolean z = true;
        int mode = View.MeasureSpec.getMode(i2);
        boolean z2 = mode == 1073741824;
        setFillViewport(z2);
        int childCount = this.e.getChildCount();
        if (childCount <= 1 || !(mode == 1073741824 || mode == Integer.MIN_VALUE)) {
            this.b = -1;
        } else {
            if (childCount > 2) {
                this.b = (int) (((float) View.MeasureSpec.getSize(i2)) * 0.4f);
            } else {
                this.b = View.MeasureSpec.getSize(i2) / 2;
            }
            this.b = Math.min(this.b, this.c);
        }
        int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(this.i, 1073741824);
        if (z2 || !this.g) {
            z = false;
        }
        if (z) {
            this.e.measure(0, makeMeasureSpec);
            if (this.e.getMeasuredWidth() > View.MeasureSpec.getSize(i2)) {
                b();
            } else {
                c();
            }
        } else {
            c();
        }
        int measuredWidth = getMeasuredWidth();
        super.onMeasure(i2, makeMeasureSpec);
        int measuredWidth2 = getMeasuredWidth();
        if (z2 && measuredWidth != measuredWidth2) {
            setTabSelected(this.j);
        }
    }

    public void setAllowCollapse(boolean z) {
        this.g = z;
    }

    public void setContentHeight(int i2) {
        this.i = i2;
        requestLayout();
    }

    public void setTabSelected(int i2) {
        this.j = i2;
        int childCount = this.e.getChildCount();
        int i3 = 0;
        while (i3 < childCount) {
            View childAt = this.e.getChildAt(i3);
            boolean z = i3 == i2;
            childAt.setSelected(z);
            if (z) {
                a(i2);
            }
            i3++;
        }
        if (this.f != null && i2 >= 0) {
            this.f.a(i2);
        }
    }
}
