package android.support.v7.internal.widget;

import android.support.v7.internal.widget.ScrollingTabContainerView;
import android.view.View;

class ai implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ScrollingTabContainerView f56a;

    private ai(ScrollingTabContainerView scrollingTabContainerView) {
        this.f56a = scrollingTabContainerView;
    }

    /* synthetic */ ai(ScrollingTabContainerView scrollingTabContainerView, ag agVar) {
        this(scrollingTabContainerView);
    }

    public void onClick(View view) {
        ((ScrollingTabContainerView.TabView) view).getTab().d();
        int childCount = this.f56a.e.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = this.f56a.e.getChildAt(i);
            childAt.setSelected(childAt == view);
        }
    }
}
