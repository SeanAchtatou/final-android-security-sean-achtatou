package android.support.v7.internal.widget;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.ListAdapter;
import android.widget.SpinnerAdapter;

class an extends t implements ap {
    final /* synthetic */ aj b;
    private CharSequence c;
    /* access modifiers changed from: private */
    public ListAdapter d;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public an(aj ajVar, Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.b = ajVar;
        a(ajVar);
        a(true);
        a(0);
        a(new o(ajVar, new ao(this, ajVar)));
    }

    public void a(ListAdapter listAdapter) {
        super.a(listAdapter);
        this.d = listAdapter;
    }

    public void a(CharSequence charSequence) {
        this.c = charSequence;
    }

    public void c() {
        int paddingLeft = this.b.getPaddingLeft();
        if (this.b.E == -2) {
            int width = this.b.getWidth();
            e(Math.max(this.b.a((SpinnerAdapter) this.d, a()), (width - paddingLeft) - this.b.getPaddingRight()));
        } else if (this.b.E == -1) {
            e((this.b.getWidth() - paddingLeft) - this.b.getPaddingRight());
        } else {
            e(this.b.E);
        }
        Drawable a2 = a();
        int i = 0;
        if (a2 != null) {
            a2.getPadding(this.b.I);
            i = -this.b.I.left;
        }
        b(i + paddingLeft);
        f(2);
        super.c();
        h().setChoiceMode(1);
        g(this.b.f());
    }
}
