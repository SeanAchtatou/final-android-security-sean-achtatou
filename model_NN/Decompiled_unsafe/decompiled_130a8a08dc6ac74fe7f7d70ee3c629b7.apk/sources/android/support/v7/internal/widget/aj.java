package android.support.v7.internal.widget;

import android.content.Context;
import android.content.DialogInterface;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.support.v7.b.j;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SpinnerAdapter;

class aj extends AbsSpinnerICS implements DialogInterface.OnClickListener {
    int E;
    private ap F;
    private am G;
    private int H;
    /* access modifiers changed from: private */
    public Rect I;

    aj(Context context, AttributeSet attributeSet, int i) {
        this(context, attributeSet, i, -1);
    }

    aj(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i);
        this.I = new Rect();
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, j.Spinner, i, 0);
        switch (i2 == -1 ? obtainStyledAttributes.getInt(7, 0) : i2) {
            case 0:
                this.F = new al(this);
                break;
            case 1:
                an anVar = new an(this, context, attributeSet, i);
                this.E = obtainStyledAttributes.getLayoutDimension(3, -2);
                anVar.a(obtainStyledAttributes.getDrawable(2));
                int dimensionPixelOffset = obtainStyledAttributes.getDimensionPixelOffset(5, 0);
                if (dimensionPixelOffset != 0) {
                    anVar.c(dimensionPixelOffset);
                }
                int dimensionPixelOffset2 = obtainStyledAttributes.getDimensionPixelOffset(4, 0);
                if (dimensionPixelOffset2 != 0) {
                    anVar.b(dimensionPixelOffset2);
                }
                this.F = anVar;
                break;
        }
        this.H = obtainStyledAttributes.getInt(0, 17);
        this.F.a(obtainStyledAttributes.getString(6));
        obtainStyledAttributes.recycle();
        if (this.G != null) {
            this.F.a(this.G);
            this.G = null;
        }
    }

    private void c(View view) {
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        if (layoutParams == null) {
            layoutParams = generateDefaultLayoutParams();
        }
        addViewInLayout(view, 0, layoutParams);
        view.setSelected(hasFocus());
        view.measure(ViewGroup.getChildMeasureSpec(this.c, this.i.left + this.i.right, layoutParams.width), ViewGroup.getChildMeasureSpec(this.b, this.i.top + this.i.bottom, layoutParams.height));
        int measuredHeight = this.i.top + ((((getMeasuredHeight() - this.i.bottom) - this.i.top) - view.getMeasuredHeight()) / 2);
        view.layout(0, measuredHeight, view.getMeasuredWidth() + 0, view.getMeasuredHeight() + measuredHeight);
    }

    private View e(int i) {
        View a2;
        if (this.u || (a2 = this.j.a(i)) == null) {
            View view = this.f38a.getView(i, null, this);
            c(view);
            return view;
        }
        c(a2);
        return a2;
    }

    /* access modifiers changed from: package-private */
    public int a(SpinnerAdapter spinnerAdapter, Drawable drawable) {
        View view;
        if (spinnerAdapter == null) {
            return 0;
        }
        int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, 0);
        int makeMeasureSpec2 = View.MeasureSpec.makeMeasureSpec(0, 0);
        int max = Math.max(0, f());
        int min = Math.min(spinnerAdapter.getCount(), max + 15);
        int max2 = Math.max(0, max - (15 - (min - max)));
        View view2 = null;
        int i = 0;
        int i2 = 0;
        while (max2 < min) {
            int itemViewType = spinnerAdapter.getItemViewType(max2);
            if (itemViewType != i2) {
                view = null;
            } else {
                itemViewType = i2;
                view = view2;
            }
            view2 = spinnerAdapter.getView(max2, view, this);
            if (view2.getLayoutParams() == null) {
                view2.setLayoutParams(new ViewGroup.LayoutParams(-2, -2));
            }
            view2.measure(makeMeasureSpec, makeMeasureSpec2);
            i = Math.max(i, view2.getMeasuredWidth());
            max2++;
            i2 = itemViewType;
        }
        if (drawable == null) {
            return i;
        }
        drawable.getPadding(this.I);
        return this.I.left + this.I.right + i;
    }

    public void a(n nVar) {
        throw new RuntimeException("setOnItemClickListener cannot be used with a spinner.");
    }

    public void a(SpinnerAdapter spinnerAdapter) {
        super.a(spinnerAdapter);
        if (this.F != null) {
            this.F.a(new am(spinnerAdapter));
        } else {
            this.G = new am(spinnerAdapter);
        }
    }

    /* access modifiers changed from: package-private */
    public void b(int i, boolean z) {
        int i2 = this.i.left;
        int right = ((getRight() - getLeft()) - this.i.left) - this.i.right;
        if (this.u) {
            k();
        }
        if (this.z == 0) {
            a();
            return;
        }
        if (this.v >= 0) {
            c(this.v);
        }
        b();
        removeAllViewsInLayout();
        this.k = this.x;
        View e = e(this.x);
        int measuredWidth = e.getMeasuredWidth();
        switch (this.H & 7) {
            case 1:
                i2 = (i2 + (right / 2)) - (measuredWidth / 2);
                break;
            case 5:
                i2 = (i2 + right) - measuredWidth;
                break;
        }
        e.offsetLeftAndRight(i2);
        this.j.a();
        invalidate();
        l();
        this.u = false;
        this.p = false;
        d(this.x);
    }

    /* access modifiers changed from: package-private */
    public void b(n nVar) {
        super.a(nVar);
    }

    public int getBaseline() {
        int baseline;
        View view = null;
        if (getChildCount() > 0) {
            view = getChildAt(0);
        } else if (this.f38a != null && this.f38a.getCount() > 0) {
            view = e(0);
            this.j.a(0, view);
            removeAllViewsInLayout();
        }
        if (view == null || (baseline = view.getBaseline()) < 0) {
            return -1;
        }
        return view.getTop() + baseline;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        a(i);
        dialogInterface.dismiss();
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (this.F != null && this.F.f()) {
            this.F.d();
        }
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
        this.r = true;
        b(0, false);
        this.r = false;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
        if (this.F != null && View.MeasureSpec.getMode(i) == Integer.MIN_VALUE) {
            setMeasuredDimension(Math.min(Math.max(getMeasuredWidth(), a(e(), getBackground())), View.MeasureSpec.getSize(i)), getMeasuredHeight());
        }
    }

    public boolean performClick() {
        boolean performClick = super.performClick();
        if (!performClick) {
            performClick = true;
            if (!this.F.f()) {
                this.F.c();
            }
        }
        return performClick;
    }
}
