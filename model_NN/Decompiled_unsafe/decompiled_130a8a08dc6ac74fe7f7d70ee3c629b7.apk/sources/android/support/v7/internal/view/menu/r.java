package android.support.v7.internal.view.menu;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v4.internal.view.SupportMenuItem;
import android.support.v4.view.ActionProvider;
import android.support.v4.view.MenuItemCompat;
import android.util.Log;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.view.ViewDebug;

public final class r implements SupportMenuItem {
    private static String w;
    private static String x;
    private static String y;
    private static String z;

    /* renamed from: a  reason: collision with root package name */
    private final int f35a;
    private final int b;
    private final int c;
    private final int d;
    private CharSequence e;
    private CharSequence f;
    private Intent g;
    private char h;
    private char i;
    private Drawable j;
    private int k = 0;
    private n l;
    private y m;
    private Runnable n;
    private MenuItem.OnMenuItemClickListener o;
    private int p = 16;
    private int q = 0;
    private View r;
    private ActionProvider s;
    private MenuItemCompat.OnActionExpandListener t;
    private boolean u = false;
    private ContextMenu.ContextMenuInfo v;

    r(n nVar, int i2, int i3, int i4, int i5, CharSequence charSequence, int i6) {
        this.l = nVar;
        this.f35a = i3;
        this.b = i2;
        this.c = i4;
        this.d = i5;
        this.e = charSequence;
        this.q = i6;
    }

    public ActionProvider A() {
        return this.s;
    }

    /* JADX WARN: Type inference failed for: r2v0, types: [android.support.v7.internal.view.menu.r, android.view.MenuItem] */
    public boolean B() {
        if ((this.q & 8) == 0 || this.r == null) {
            return false;
        }
        if (this.t == null || this.t.onMenuItemActionExpand((MenuItem) this)) {
            return this.l.b((r) this);
        }
        return false;
    }

    /* JADX WARN: Type inference failed for: r2v0, types: [android.support.v7.internal.view.menu.r, android.view.MenuItem] */
    public boolean C() {
        if ((this.q & 8) == 0) {
            return false;
        }
        if (this.r == null) {
            return true;
        }
        if (this.t == null || this.t.onMenuItemActionCollapse((MenuItem) this)) {
            return this.l.c((r) this);
        }
        return false;
    }

    public boolean D() {
        return ((this.q & 8) == 0 || this.r == null) ? false : true;
    }

    public boolean E() {
        return this.u;
    }

    /* JADX WARN: Type inference failed for: r2v0, types: [android.support.v7.internal.view.menu.r, android.view.MenuItem] */
    public MenuItem a(int i2) {
        this.j = null;
        this.k = i2;
        this.l.b(false);
        return this;
    }

    /* JADX WARN: Type inference failed for: r2v0, types: [android.support.v7.internal.view.menu.r, android.view.MenuItem] */
    public MenuItem a(Drawable drawable) {
        this.k = 0;
        this.j = drawable;
        this.l.b(false);
        return this;
    }

    /* JADX WARN: Type inference failed for: r2v0, types: [android.support.v7.internal.view.menu.r, android.view.MenuItem] */
    public MenuItem a(boolean z2) {
        if (z2) {
            this.p |= 16;
        } else {
            this.p &= -17;
        }
        this.l.b(false);
        return this;
    }

    /* access modifiers changed from: package-private */
    public CharSequence a(x xVar) {
        return (xVar == null || !xVar.a()) ? m() : n();
    }

    /* access modifiers changed from: package-private */
    public void a(y yVar) {
        this.m = yVar;
        yVar.setHeaderTitle(m());
    }

    /* access modifiers changed from: package-private */
    public void a(ContextMenu.ContextMenuInfo contextMenuInfo) {
        this.v = contextMenuInfo;
    }

    /* JADX WARN: Type inference failed for: r4v0, types: [android.support.v7.internal.view.menu.r, android.view.MenuItem] */
    public boolean a() {
        if ((this.o != null && this.o.onMenuItemClick(this)) || this.l.a(this.l.o(), (MenuItem) this)) {
            return true;
        }
        if (this.n != null) {
            this.n.run();
            return true;
        }
        if (this.g != null) {
            try {
                this.l.d().startActivity(this.g);
                return true;
            } catch (ActivityNotFoundException e2) {
                Log.e("MenuItemImpl", "Can't find activity to handle intent; ignoring", e2);
            }
        }
        return this.s != null && this.s.onPerformDefaultAction();
    }

    /* JADX WARN: Type inference failed for: r4v0, types: [android.support.v7.internal.view.menu.r, android.view.MenuItem] */
    public MenuItem b(boolean z2) {
        int i2 = this.p;
        this.p = (z2 ? 1 : 0) | (this.p & -2);
        if (i2 != this.p) {
            this.l.b(false);
        }
        return this;
    }

    public boolean b() {
        return (this.p & 16) != 0;
    }

    public int c() {
        return this.b;
    }

    public void c(boolean z2) {
        this.p = (z2 ? 4 : 0) | (this.p & -5);
    }

    @ViewDebug.CapturedViewProperty
    public int d() {
        return this.f35a;
    }

    /* access modifiers changed from: package-private */
    public boolean d(boolean z2) {
        int i2 = this.p;
        this.p = (z2 ? 0 : 8) | (this.p & -9);
        return i2 != this.p;
    }

    public int e() {
        return this.d;
    }

    public void e(boolean z2) {
        if (z2) {
            this.p |= 32;
        } else {
            this.p &= -33;
        }
    }

    public char f() {
        return this.i;
    }

    public void f(boolean z2) {
        this.u = z2;
        this.l.b(false);
    }

    public char g() {
        return this.h;
    }

    /* access modifiers changed from: package-private */
    public char h() {
        return this.i;
    }

    /* access modifiers changed from: package-private */
    public String i() {
        char h2 = h();
        if (h2 == 0) {
            return "";
        }
        StringBuilder sb = new StringBuilder(w);
        switch (h2) {
            case 8:
                sb.append(y);
                break;
            case 10:
                sb.append(x);
                break;
            case ' ':
                sb.append(z);
                break;
            default:
                sb.append(h2);
                break;
        }
        return sb.toString();
    }

    /* access modifiers changed from: package-private */
    public boolean j() {
        return this.l.b() && h() != 0;
    }

    public SubMenu k() {
        return this.m;
    }

    public boolean l() {
        return this.m != null;
    }

    @ViewDebug.CapturedViewProperty
    public CharSequence m() {
        return this.e;
    }

    public CharSequence n() {
        return this.f != null ? this.f : this.e;
    }

    public Drawable o() {
        if (this.j != null) {
            return this.j;
        }
        if (this.k == 0) {
            return null;
        }
        Drawable drawable = this.l.c().getDrawable(this.k);
        this.k = 0;
        this.j = drawable;
        return drawable;
    }

    public boolean p() {
        return (this.p & 1) == 1;
    }

    public boolean q() {
        return (this.p & 4) != 0;
    }

    public boolean r() {
        return (this.p & 2) == 2;
    }

    public boolean s() {
        return (this.s == null || !this.s.overridesItemVisibility()) ? (this.p & 8) == 0 : (this.p & 8) == 0 && this.s.isVisible();
    }

    public void t() {
        this.l.a(this);
    }

    public String toString() {
        return this.e.toString();
    }

    public boolean u() {
        return this.l.p();
    }

    public boolean v() {
        return (this.p & 32) == 32;
    }

    public boolean w() {
        return (this.q & 1) == 1;
    }

    public boolean x() {
        return (this.q & 2) == 2;
    }

    public boolean y() {
        return (this.q & 4) == 4;
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [android.support.v7.internal.view.menu.r, android.view.MenuItem] */
    public View z() {
        if (this.r != null) {
            return this.r;
        }
        if (this.s == null) {
            return null;
        }
        this.r = this.s.onCreateActionView((MenuItem) this);
        return this.r;
    }
}
