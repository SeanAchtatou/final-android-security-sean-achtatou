package android.support.v7.internal.widget;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.view.View;
import android.widget.ListAdapter;

class al implements DialogInterface.OnClickListener, ap {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ aj f57a;
    private AlertDialog b;
    private ListAdapter c;
    private CharSequence d;

    private al(aj ajVar) {
        this.f57a = ajVar;
    }

    public void a(ListAdapter listAdapter) {
        this.c = listAdapter;
    }

    public void a(CharSequence charSequence) {
        this.d = charSequence;
    }

    public void c() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this.f57a.getContext());
        if (this.d != null) {
            builder.setTitle(this.d);
        }
        this.b = builder.setSingleChoiceItems(this.c, this.f57a.f(), this).show();
    }

    public void d() {
        this.b.dismiss();
        this.b = null;
    }

    public boolean f() {
        if (this.b != null) {
            return this.b.isShowing();
        }
        return false;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        this.f57a.a(i);
        if (this.f57a.t != null) {
            this.f57a.a((View) null, i, this.c.getItemId(i));
        }
        d();
    }
}
