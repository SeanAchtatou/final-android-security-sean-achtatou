package android.support.v7.internal.widget;

import android.util.SparseArray;
import android.view.View;

class d {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AbsSpinnerICS f61a;
    private final SparseArray b = new SparseArray();

    d(AbsSpinnerICS absSpinnerICS) {
        this.f61a = absSpinnerICS;
    }

    /* access modifiers changed from: package-private */
    public View a(int i) {
        View view = (View) this.b.get(i);
        if (view != null) {
            this.b.delete(i);
        }
        return view;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.internal.widget.AbsSpinnerICS.a(android.support.v7.internal.widget.AbsSpinnerICS, android.view.View, boolean):void
     arg types: [android.support.v7.internal.widget.AbsSpinnerICS, android.view.View, int]
     candidates:
      android.support.v7.internal.widget.k.a(android.view.View, int, long):boolean
      android.support.v7.internal.widget.AbsSpinnerICS.a(android.support.v7.internal.widget.AbsSpinnerICS, android.view.View, boolean):void */
    /* access modifiers changed from: package-private */
    public void a() {
        SparseArray sparseArray = this.b;
        int size = sparseArray.size();
        for (int i = 0; i < size; i++) {
            View view = (View) sparseArray.valueAt(i);
            if (view != null) {
                this.f61a.removeDetachedView(view, true);
            }
        }
        sparseArray.clear();
    }

    public void a(int i, View view) {
        this.b.put(i, view);
    }
}
