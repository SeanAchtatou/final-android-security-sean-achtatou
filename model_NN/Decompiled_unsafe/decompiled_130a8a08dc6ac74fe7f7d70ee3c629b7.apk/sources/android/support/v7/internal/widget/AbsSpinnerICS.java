package android.support.v7.internal.widget;

import android.content.Context;
import android.database.DataSetObserver;
import android.graphics.Rect;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SpinnerAdapter;

abstract class AbsSpinnerICS extends k {
    private DataSetObserver E;

    /* renamed from: a  reason: collision with root package name */
    SpinnerAdapter f38a;
    int b;
    int c;
    boolean d;
    int e = 0;
    int f = 0;
    int g = 0;
    int h = 0;
    final Rect i = new Rect();
    final d j = new d(this);

    class SavedState extends View.BaseSavedState {
        public static final Parcelable.Creator CREATOR = new e();

        /* renamed from: a  reason: collision with root package name */
        long f39a;
        int b;

        private SavedState(Parcel parcel) {
            super(parcel);
            this.f39a = parcel.readLong();
            this.b = parcel.readInt();
        }

        SavedState(Parcelable parcelable) {
            super(parcelable);
        }

        public String toString() {
            return "AbsSpinner.SavedState{" + Integer.toHexString(System.identityHashCode(this)) + " selectedId=" + this.f39a + " position=" + this.b + "}";
        }

        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeLong(this.f39a);
            parcel.writeInt(this.b);
        }
    }

    AbsSpinnerICS(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        o();
    }

    private void o() {
        setFocusable(true);
        setWillNotDraw(false);
    }

    /* access modifiers changed from: package-private */
    public int a(View view) {
        return view.getMeasuredHeight();
    }

    /* access modifiers changed from: package-private */
    public void a() {
        this.u = false;
        this.p = false;
        removeAllViewsInLayout();
        this.B = -1;
        this.C = Long.MIN_VALUE;
        c(-1);
        d(-1);
        invalidate();
    }

    public void a(int i2) {
        d(i2);
        requestLayout();
        invalidate();
    }

    public void a(SpinnerAdapter spinnerAdapter) {
        int i2 = -1;
        if (this.f38a != null) {
            this.f38a.unregisterDataSetObserver(this.E);
            a();
        }
        this.f38a = spinnerAdapter;
        this.B = -1;
        this.C = Long.MIN_VALUE;
        if (this.f38a != null) {
            this.A = this.z;
            this.z = this.f38a.getCount();
            i();
            this.E = new m(this);
            this.f38a.registerDataSetObserver(this.E);
            if (this.z > 0) {
                i2 = 0;
            }
            c(i2);
            d(i2);
            if (this.z == 0) {
                l();
            }
        } else {
            i();
            a();
            l();
        }
        requestLayout();
    }

    /* access modifiers changed from: package-private */
    public int b(View view) {
        return view.getMeasuredWidth();
    }

    /* access modifiers changed from: package-private */
    public void b() {
        int childCount = getChildCount();
        d dVar = this.j;
        int i2 = this.k;
        for (int i3 = 0; i3 < childCount; i3++) {
            dVar.a(i2 + i3, getChildAt(i3));
        }
    }

    public View c() {
        if (this.z <= 0 || this.x < 0) {
            return null;
        }
        return getChildAt(this.x - this.k);
    }

    /* renamed from: d */
    public SpinnerAdapter e() {
        return this.f38a;
    }

    /* access modifiers changed from: protected */
    public ViewGroup.LayoutParams generateDefaultLayoutParams() {
        return new ViewGroup.LayoutParams(-1, -2);
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x009d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onMeasure(int r10, int r11) {
        /*
            r9 = this;
            r5 = 1
            r4 = 0
            int r6 = android.view.View.MeasureSpec.getMode(r10)
            int r0 = r9.getPaddingLeft()
            int r1 = r9.getPaddingTop()
            int r2 = r9.getPaddingRight()
            int r3 = r9.getPaddingBottom()
            android.graphics.Rect r7 = r9.i
            int r8 = r9.e
            if (r0 <= r8) goto L_0x00d1
        L_0x001c:
            r7.left = r0
            android.graphics.Rect r7 = r9.i
            int r0 = r9.f
            if (r1 <= r0) goto L_0x00d5
            r0 = r1
        L_0x0025:
            r7.top = r0
            android.graphics.Rect r1 = r9.i
            int r0 = r9.g
            if (r2 <= r0) goto L_0x00d9
            r0 = r2
        L_0x002e:
            r1.right = r0
            android.graphics.Rect r1 = r9.i
            int r0 = r9.h
            if (r3 <= r0) goto L_0x00dd
            r0 = r3
        L_0x0037:
            r1.bottom = r0
            boolean r0 = r9.u
            if (r0 == 0) goto L_0x0040
            r9.k()
        L_0x0040:
            int r1 = r9.f()
            if (r1 < 0) goto L_0x00e1
            android.widget.SpinnerAdapter r0 = r9.f38a
            if (r0 == 0) goto L_0x00e1
            android.widget.SpinnerAdapter r0 = r9.f38a
            int r0 = r0.getCount()
            if (r1 >= r0) goto L_0x00e1
            android.support.v7.internal.widget.d r0 = r9.j
            android.view.View r0 = r0.a(r1)
            if (r0 != 0) goto L_0x0061
            android.widget.SpinnerAdapter r0 = r9.f38a
            r2 = 0
            android.view.View r0 = r0.getView(r1, r2, r9)
        L_0x0061:
            if (r0 == 0) goto L_0x0068
            android.support.v7.internal.widget.d r2 = r9.j
            r2.a(r1, r0)
        L_0x0068:
            if (r0 == 0) goto L_0x00e1
            android.view.ViewGroup$LayoutParams r1 = r0.getLayoutParams()
            if (r1 != 0) goto L_0x007b
            r9.d = r5
            android.view.ViewGroup$LayoutParams r1 = r9.generateDefaultLayoutParams()
            r0.setLayoutParams(r1)
            r9.d = r4
        L_0x007b:
            r9.measureChild(r0, r10, r11)
            int r1 = r9.a(r0)
            android.graphics.Rect r2 = r9.i
            int r2 = r2.top
            int r1 = r1 + r2
            android.graphics.Rect r2 = r9.i
            int r2 = r2.bottom
            int r1 = r1 + r2
            int r0 = r9.b(r0)
            android.graphics.Rect r2 = r9.i
            int r2 = r2.left
            int r0 = r0 + r2
            android.graphics.Rect r2 = r9.i
            int r2 = r2.right
            int r0 = r0 + r2
            r2 = r4
        L_0x009b:
            if (r2 == 0) goto L_0x00b1
            android.graphics.Rect r1 = r9.i
            int r1 = r1.top
            android.graphics.Rect r2 = r9.i
            int r2 = r2.bottom
            int r1 = r1 + r2
            if (r6 != 0) goto L_0x00b1
            android.graphics.Rect r0 = r9.i
            int r0 = r0.left
            android.graphics.Rect r2 = r9.i
            int r2 = r2.right
            int r0 = r0 + r2
        L_0x00b1:
            int r2 = r9.getSuggestedMinimumHeight()
            int r1 = java.lang.Math.max(r1, r2)
            int r2 = r9.getSuggestedMinimumWidth()
            int r0 = java.lang.Math.max(r0, r2)
            int r1 = resolveSize(r1, r11)
            int r0 = resolveSize(r0, r10)
            r9.setMeasuredDimension(r0, r1)
            r9.b = r11
            r9.c = r10
            return
        L_0x00d1:
            int r0 = r9.e
            goto L_0x001c
        L_0x00d5:
            int r0 = r9.f
            goto L_0x0025
        L_0x00d9:
            int r0 = r9.g
            goto L_0x002e
        L_0x00dd:
            int r0 = r9.h
            goto L_0x0037
        L_0x00e1:
            r2 = r5
            r0 = r4
            r1 = r4
            goto L_0x009b
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.internal.widget.AbsSpinnerICS.onMeasure(int, int):void");
    }

    public void onRestoreInstanceState(Parcelable parcelable) {
        SavedState savedState = (SavedState) parcelable;
        super.onRestoreInstanceState(savedState.getSuperState());
        if (savedState.f39a >= 0) {
            this.u = true;
            this.p = true;
            this.n = savedState.f39a;
            this.m = savedState.b;
            this.q = 0;
            requestLayout();
        }
    }

    public Parcelable onSaveInstanceState() {
        SavedState savedState = new SavedState(super.onSaveInstanceState());
        savedState.f39a = g();
        if (savedState.f39a >= 0) {
            savedState.b = f();
        } else {
            savedState.b = -1;
        }
        return savedState;
    }

    public void requestLayout() {
        if (!this.d) {
            super.requestLayout();
        }
    }
}
