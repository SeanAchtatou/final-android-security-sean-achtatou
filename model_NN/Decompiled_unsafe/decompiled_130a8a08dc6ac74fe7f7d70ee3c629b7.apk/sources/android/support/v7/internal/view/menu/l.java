package android.support.v7.internal.view.menu;

import android.content.Context;
import android.os.IBinder;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;

public class l implements u, AdapterView.OnItemClickListener {

    /* renamed from: a  reason: collision with root package name */
    Context f31a;
    LayoutInflater b;
    n c;
    int d;
    int e;
    m f;
    /* access modifiers changed from: private */
    public int g;
    private v h;

    public l(int i, int i2) {
        this.e = i;
        this.d = i2;
    }

    public ListAdapter a() {
        if (this.f == null) {
            this.f = new m(this);
        }
        return this.f;
    }

    public void a(Context context, n nVar) {
        if (this.d != 0) {
            this.f31a = new ContextThemeWrapper(context, this.d);
            this.b = LayoutInflater.from(this.f31a);
        } else if (this.f31a != null) {
            this.f31a = context;
            if (this.b == null) {
                this.b = LayoutInflater.from(this.f31a);
            }
        }
        this.c = nVar;
        if (this.f != null) {
            this.f.notifyDataSetChanged();
        }
    }

    public void a(n nVar, boolean z) {
        if (this.h != null) {
            this.h.a(nVar, z);
        }
    }

    public void a(v vVar) {
        this.h = vVar;
    }

    public boolean a(n nVar, r rVar) {
        return false;
    }

    public boolean a(y yVar) {
        if (!yVar.hasVisibleItems()) {
            return false;
        }
        new q(yVar).a((IBinder) null);
        if (this.h != null) {
            this.h.a(yVar);
        }
        return true;
    }

    public void b(boolean z) {
        if (this.f != null) {
            this.f.notifyDataSetChanged();
        }
    }

    public boolean b(n nVar, r rVar) {
        return false;
    }

    public boolean f() {
        return false;
    }

    /* JADX WARN: Type inference failed for: r1v1, types: [android.support.v7.internal.view.menu.r, android.view.MenuItem] */
    public void onItemClick(AdapterView adapterView, View view, int i, long j) {
        this.c.a((MenuItem) this.f.getItem(i), 0);
    }
}
