package android.support.v7.internal.view.menu;

import android.content.Context;

public interface u {
    void a(Context context, n nVar);

    void a(n nVar, boolean z);

    boolean a(n nVar, r rVar);

    boolean a(y yVar);

    void b(boolean z);

    boolean b(n nVar, r rVar);

    boolean f();
}
