package android.support.v7.internal.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.ActionMode;
import android.view.View;
import android.widget.LinearLayout;

public class NativeActionModeAwareLayout extends LinearLayout {

    /* renamed from: a  reason: collision with root package name */
    private ac f45a;

    public NativeActionModeAwareLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public void setActionModeForChildListener(ac acVar) {
        this.f45a = acVar;
    }

    public ActionMode startActionModeForChild(View view, ActionMode.Callback callback) {
        if (this.f45a != null) {
            callback = this.f45a.a(callback);
        }
        return super.startActionModeForChild(view, callback);
    }
}
