package android.support.v7.internal.view.menu;

import android.content.Context;
import android.view.MenuItem;

public class a {

    /* renamed from: a  reason: collision with root package name */
    private final int f22a;
    private final int b;
    private final int c;
    private final int d;
    private CharSequence e;
    private int f = 0;
    private Context g;
    private int h = 16;

    public a(Context context, int i, int i2, int i3, int i4, CharSequence charSequence) {
        this.g = context;
        this.f22a = i2;
        this.b = i;
        this.c = i3;
        this.d = i4;
        this.e = charSequence;
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [android.support.v7.internal.view.menu.a, android.view.MenuItem] */
    public MenuItem a(CharSequence charSequence) {
        this.e = charSequence;
        return this;
    }
}
