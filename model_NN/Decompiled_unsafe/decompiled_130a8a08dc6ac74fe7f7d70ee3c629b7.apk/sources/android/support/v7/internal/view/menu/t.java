package android.support.v7.internal.view.menu;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import java.util.ArrayList;

class t extends BaseAdapter {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ s f37a;
    /* access modifiers changed from: private */
    public n b;
    private int c = -1;

    public t(s sVar, n nVar) {
        this.f37a = sVar;
        this.b = nVar;
        a();
    }

    /* renamed from: a */
    public r getItem(int i) {
        ArrayList k = this.f37a.i ? this.b.k() : this.b.h();
        if (this.c >= 0 && i >= this.c) {
            i++;
        }
        return (r) k.get(i);
    }

    /* access modifiers changed from: package-private */
    public void a() {
        r q = this.f37a.f.q();
        if (q != null) {
            ArrayList k = this.f37a.f.k();
            int size = k.size();
            for (int i = 0; i < size; i++) {
                if (((r) k.get(i)) == q) {
                    this.c = i;
                    return;
                }
            }
        }
        this.c = -1;
    }

    public int getCount() {
        ArrayList k = this.f37a.i ? this.b.k() : this.b.h();
        return this.c < 0 ? k.size() : k.size() - 1;
    }

    public long getItemId(int i) {
        return (long) i;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View getView(int i, View view, ViewGroup viewGroup) {
        View inflate = view == null ? this.f37a.d.inflate(s.b, viewGroup, false) : view;
        x xVar = (x) inflate;
        if (this.f37a.c) {
            ((ListMenuItemView) inflate).setForceShowIcon(true);
        }
        xVar.a(getItem(i), 0);
        return inflate;
    }

    public void notifyDataSetChanged() {
        a();
        super.notifyDataSetChanged();
    }
}
