package android.support.v7.internal.view.menu;

import android.content.Context;
import android.view.View;

class f extends s {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ActionMenuPresenter f27a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public f(ActionMenuPresenter actionMenuPresenter, Context context, n nVar, View view, boolean z) {
        super(context, nVar, view, z);
        this.f27a = actionMenuPresenter;
        a(actionMenuPresenter.f17a);
    }

    public void onDismiss() {
        super.onDismiss();
        this.f27a.e.close();
        f unused = this.f27a.v = (f) null;
    }
}
