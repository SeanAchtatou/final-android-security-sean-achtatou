package android.support.v7.internal.widget;

import android.database.DataSetObserver;

class y extends DataSetObserver {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ t f76a;

    private y(t tVar) {
        this.f76a = tVar;
    }

    /* synthetic */ y(t tVar, u uVar) {
        this(tVar);
    }

    public void onChanged() {
        if (this.f76a.f()) {
            this.f76a.c();
        }
    }

    public void onInvalidated() {
        this.f76a.d();
    }
}
