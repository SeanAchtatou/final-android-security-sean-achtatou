package android.support.v7.internal.view.menu;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.IBinder;
import android.support.v7.b.g;
import android.support.v7.b.i;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

public class q implements DialogInterface.OnClickListener, DialogInterface.OnDismissListener, DialogInterface.OnKeyListener, v {

    /* renamed from: a  reason: collision with root package name */
    private n f34a;
    l b;
    private AlertDialog c;
    private v d;

    public q(n nVar) {
        this.f34a = nVar;
    }

    public void a() {
        if (this.c != null) {
            this.c.dismiss();
        }
    }

    public void a(IBinder iBinder) {
        n nVar = this.f34a;
        AlertDialog.Builder builder = new AlertDialog.Builder(nVar.d());
        this.b = new l(g.abc_list_menu_item_layout, i.Theme_AppCompat_CompactMenu_Dialog);
        this.b.a(this);
        this.f34a.a(this.b);
        builder.setAdapter(this.b.a(), this);
        View n = nVar.n();
        if (n != null) {
            builder.setCustomTitle(n);
        } else {
            builder.setIcon(nVar.m()).setTitle(nVar.l());
        }
        builder.setOnKeyListener(this);
        this.c = builder.create();
        this.c.setOnDismissListener(this);
        WindowManager.LayoutParams attributes = this.c.getWindow().getAttributes();
        attributes.type = 1003;
        if (iBinder != null) {
            attributes.token = iBinder;
        }
        attributes.flags |= 131072;
        this.c.show();
    }

    public void a(n nVar, boolean z) {
        if (z || nVar == this.f34a) {
            a();
        }
        if (this.d != null) {
            this.d.a(nVar, z);
        }
    }

    public boolean a(n nVar) {
        if (this.d != null) {
            return this.d.a(nVar);
        }
        return false;
    }

    /* JADX WARN: Type inference failed for: r0v3, types: [android.support.v7.internal.view.menu.r, android.view.MenuItem] */
    public void onClick(DialogInterface dialogInterface, int i) {
        this.f34a.a((MenuItem) ((r) this.b.a().getItem(i)), 0);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.internal.view.menu.l.a(android.support.v7.internal.view.menu.n, boolean):void
     arg types: [android.support.v7.internal.view.menu.n, int]
     candidates:
      android.support.v7.internal.view.menu.l.a(android.content.Context, android.support.v7.internal.view.menu.n):void
      android.support.v7.internal.view.menu.l.a(android.support.v7.internal.view.menu.n, android.support.v7.internal.view.menu.r):boolean
      android.support.v7.internal.view.menu.u.a(android.content.Context, android.support.v7.internal.view.menu.n):void
      android.support.v7.internal.view.menu.u.a(android.support.v7.internal.view.menu.n, android.support.v7.internal.view.menu.r):boolean
      android.support.v7.internal.view.menu.l.a(android.support.v7.internal.view.menu.n, boolean):void */
    public void onDismiss(DialogInterface dialogInterface) {
        this.b.a(this.f34a, true);
    }

    public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
        Window window;
        View decorView;
        KeyEvent.DispatcherState keyDispatcherState;
        View decorView2;
        KeyEvent.DispatcherState keyDispatcherState2;
        if (i == 82 || i == 4) {
            if (keyEvent.getAction() == 0 && keyEvent.getRepeatCount() == 0) {
                Window window2 = this.c.getWindow();
                if (!(window2 == null || (decorView2 = window2.getDecorView()) == null || (keyDispatcherState2 = decorView2.getKeyDispatcherState()) == null)) {
                    keyDispatcherState2.startTracking(keyEvent, this);
                    return true;
                }
            } else if (keyEvent.getAction() == 1 && !keyEvent.isCanceled() && (window = this.c.getWindow()) != null && (decorView = window.getDecorView()) != null && (keyDispatcherState = decorView.getKeyDispatcherState()) != null && keyDispatcherState.isTracking(keyEvent)) {
                this.f34a.a(true);
                dialogInterface.dismiss();
                return true;
            }
        }
        return this.f34a.performShortcut(i, keyEvent, 0);
    }
}
