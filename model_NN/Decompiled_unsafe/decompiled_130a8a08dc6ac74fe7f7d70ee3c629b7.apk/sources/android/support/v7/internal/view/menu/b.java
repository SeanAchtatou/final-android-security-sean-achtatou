package android.support.v7.internal.view.menu;

import android.graphics.Rect;
import android.text.method.TransformationMethod;
import android.view.View;
import java.util.Locale;

class b implements TransformationMethod {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ActionMenuItemView f23a;
    private Locale b;

    public b(ActionMenuItemView actionMenuItemView) {
        this.f23a = actionMenuItemView;
        this.b = actionMenuItemView.getContext().getResources().getConfiguration().locale;
    }

    public CharSequence getTransformation(CharSequence charSequence, View view) {
        if (charSequence != null) {
            return charSequence.toString().toUpperCase(this.b);
        }
        return null;
    }

    public void onFocusChanged(View view, CharSequence charSequence, boolean z, int i, Rect rect) {
    }
}
