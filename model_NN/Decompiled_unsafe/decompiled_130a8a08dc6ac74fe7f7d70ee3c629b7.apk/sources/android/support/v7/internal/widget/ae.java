package android.support.v7.internal.widget;

class ae implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ProgressBarICS f53a;
    private int b;
    private int c;
    private boolean d;

    ae(ProgressBarICS progressBarICS, int i, int i2, boolean z) {
        this.f53a = progressBarICS;
        this.b = i;
        this.c = i2;
        this.d = z;
    }

    public void a(int i, int i2, boolean z) {
        this.b = i;
        this.c = i2;
        this.d = z;
    }

    public void run() {
        this.f53a.a(this.b, this.c, this.d, true);
        ae unused = this.f53a.u = this;
    }
}
