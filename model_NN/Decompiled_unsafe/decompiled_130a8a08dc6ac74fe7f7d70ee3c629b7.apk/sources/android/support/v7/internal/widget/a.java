package android.support.v7.internal.widget;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.os.Build;
import android.support.v7.b.b;
import android.support.v7.b.c;
import android.support.v7.b.j;
import android.support.v7.internal.view.menu.ActionMenuPresenter;
import android.support.v7.internal.view.menu.ActionMenuView;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;

abstract class a extends ViewGroup {

    /* renamed from: a  reason: collision with root package name */
    protected ActionMenuView f50a;
    protected ActionMenuPresenter b;
    protected ActionBarContainer c;
    protected boolean d;
    protected boolean e;
    protected int f;

    a(Context context) {
        super(context);
    }

    a(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    a(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    /* access modifiers changed from: protected */
    public int a(View view, int i, int i2, int i3) {
        view.measure(View.MeasureSpec.makeMeasureSpec(i, Integer.MIN_VALUE), i2);
        return Math.max(0, (i - view.getMeasuredWidth()) - i3);
    }

    public boolean a() {
        if (this.b != null) {
            return this.b.a();
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public int b(View view, int i, int i2, int i3) {
        int measuredWidth = view.getMeasuredWidth();
        int measuredHeight = view.getMeasuredHeight();
        int i4 = ((i3 - measuredHeight) / 2) + i2;
        view.layout(i, i4, i + measuredWidth, measuredHeight + i4);
        return measuredWidth;
    }

    public void b() {
        post(new b(this));
    }

    /* access modifiers changed from: protected */
    public int c(View view, int i, int i2, int i3) {
        int measuredWidth = view.getMeasuredWidth();
        int measuredHeight = view.getMeasuredHeight();
        int i4 = ((i3 - measuredHeight) / 2) + i2;
        view.layout(i - measuredWidth, i4, i, measuredHeight + i4);
        return measuredWidth;
    }

    public boolean c() {
        if (this.b != null) {
            return this.b.e();
        }
        return false;
    }

    public int getAnimatedVisibility() {
        return getVisibility();
    }

    public int getContentHeight() {
        return this.f;
    }

    /* access modifiers changed from: protected */
    public void onConfigurationChanged(Configuration configuration) {
        if (Build.VERSION.SDK_INT >= 8) {
            super.onConfigurationChanged(configuration);
        }
        TypedArray obtainStyledAttributes = getContext().obtainStyledAttributes(null, j.ActionBar, b.actionBarStyle, 0);
        setContentHeight(obtainStyledAttributes.getLayoutDimension(1, 0));
        obtainStyledAttributes.recycle();
        if (this.e) {
            setSplitActionBar(getContext().getResources().getBoolean(c.abc_split_action_bar_is_narrow));
        }
        if (this.b != null) {
            this.b.a(configuration);
        }
    }

    public void setContentHeight(int i) {
        this.f = i;
        requestLayout();
    }

    public void setSplitActionBar(boolean z) {
        this.d = z;
    }

    public void setSplitView(ActionBarContainer actionBarContainer) {
        this.c = actionBarContainer;
    }

    public void setSplitWhenNarrow(boolean z) {
        this.e = z;
    }

    public void setVisibility(int i) {
        if (i != getVisibility()) {
            super.setVisibility(i);
        }
    }
}
