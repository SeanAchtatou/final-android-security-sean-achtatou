package android.support.v7.internal.widget;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v7.c.a;
import android.support.v7.internal.view.menu.n;
import android.support.v7.internal.view.menu.r;
import android.support.v7.internal.view.menu.u;
import android.support.v7.internal.view.menu.y;

class i implements u {

    /* renamed from: a  reason: collision with root package name */
    n f65a;
    r b;
    final /* synthetic */ ActionBarView c;

    public void a(Context context, n nVar) {
        if (!(this.f65a == null || this.b == null)) {
            this.f65a.c(this.b);
        }
        this.f65a = nVar;
    }

    public void a(n nVar, boolean z) {
    }

    public boolean a(n nVar, r rVar) {
        this.c.g = rVar.z();
        this.c.q.a(this.c.m.getConstantState().newDrawable(this.c.getResources()));
        this.b = rVar;
        if (this.c.g.getParent() != this.c) {
            this.c.addView(this.c.g);
        }
        if (this.c.q.getParent() != this.c) {
            this.c.addView(this.c.q);
        }
        this.c.p.setVisibility(8);
        if (this.c.r != null) {
            this.c.r.setVisibility(8);
        }
        if (this.c.x != null) {
            this.c.x.setVisibility(8);
        }
        if (this.c.v != null) {
            this.c.v.setVisibility(8);
        }
        if (this.c.y != null) {
            this.c.y.setVisibility(8);
        }
        this.c.requestLayout();
        rVar.f(true);
        if (this.c.g instanceof a) {
            ((a) this.c.g).a();
        }
        return true;
    }

    public boolean a(y yVar) {
        return false;
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x0021  */
    /* JADX WARNING: Removed duplicated region for block: B:17:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void b(boolean r6) {
        /*
            r5 = this;
            r1 = 0
            android.support.v7.internal.view.menu.r r0 = r5.b
            if (r0 == 0) goto L_0x0028
            android.support.v7.internal.view.menu.n r0 = r5.f65a
            if (r0 == 0) goto L_0x002d
            android.support.v7.internal.view.menu.n r0 = r5.f65a
            int r3 = r0.size()
            r2 = r1
        L_0x0010:
            if (r2 >= r3) goto L_0x002d
            android.support.v7.internal.view.menu.n r0 = r5.f65a
            android.view.MenuItem r0 = r0.getItem(r2)
            android.support.v4.internal.view.SupportMenuItem r0 = (android.support.v4.internal.view.SupportMenuItem) r0
            android.support.v7.internal.view.menu.r r4 = r5.b
            if (r0 != r4) goto L_0x0029
            r0 = 1
        L_0x001f:
            if (r0 != 0) goto L_0x0028
            android.support.v7.internal.view.menu.n r0 = r5.f65a
            android.support.v7.internal.view.menu.r r1 = r5.b
            r5.b(r0, r1)
        L_0x0028:
            return
        L_0x0029:
            int r0 = r2 + 1
            r2 = r0
            goto L_0x0010
        L_0x002d:
            r0 = r1
            goto L_0x001f
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.internal.widget.i.b(boolean):void");
    }

    public boolean b(n nVar, r rVar) {
        if (this.c.g instanceof a) {
            ((a) this.c.g).b();
        }
        this.c.removeView(this.c.g);
        this.c.removeView(this.c.q);
        this.c.g = null;
        if ((this.c.j & 2) != 0) {
            this.c.p.setVisibility(0);
        }
        if ((this.c.j & 8) != 0) {
            if (this.c.r == null) {
                this.c.g();
            } else {
                this.c.r.setVisibility(0);
            }
        }
        if (this.c.x != null && this.c.i == 2) {
            this.c.x.setVisibility(0);
        }
        if (this.c.v != null && this.c.i == 1) {
            this.c.v.setVisibility(0);
        }
        if (!(this.c.y == null || (this.c.j & 16) == 0)) {
            this.c.y.setVisibility(0);
        }
        this.c.q.a((Drawable) null);
        this.b = null;
        this.c.requestLayout();
        rVar.f(false);
        return true;
    }

    public boolean f() {
        return false;
    }
}
