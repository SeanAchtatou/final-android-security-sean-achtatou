package android.support.v7.internal.view.menu;

import android.content.Context;
import android.util.AttributeSet;
import android.view.ViewDebug;
import android.widget.LinearLayout;

public class j extends LinearLayout.LayoutParams {
    @ViewDebug.ExportedProperty

    /* renamed from: a  reason: collision with root package name */
    public boolean f29a;
    @ViewDebug.ExportedProperty
    public int b;
    @ViewDebug.ExportedProperty
    public int c;
    @ViewDebug.ExportedProperty
    public boolean d;
    @ViewDebug.ExportedProperty
    public boolean e;
    public boolean f;

    public j(int i, int i2) {
        super(i, i2);
        this.f29a = false;
    }

    public j(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public j(j jVar) {
        super((LinearLayout.LayoutParams) jVar);
        this.f29a = jVar.f29a;
    }
}
