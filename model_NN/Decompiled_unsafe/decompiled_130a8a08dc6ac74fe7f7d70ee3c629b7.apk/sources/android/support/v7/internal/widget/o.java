package android.support.v7.internal.widget;

import android.view.View;
import android.widget.AdapterView;

class o implements AdapterView.OnItemClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ k f68a;
    private final n b;

    public o(k kVar, n nVar) {
        this.f68a = kVar;
        this.b = nVar;
    }

    public void onItemClick(AdapterView adapterView, View view, int i, long j) {
        this.b.a(this.f68a, view, i, j);
    }
}
