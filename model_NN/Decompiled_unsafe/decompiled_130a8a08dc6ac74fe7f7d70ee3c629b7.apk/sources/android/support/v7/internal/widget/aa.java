package android.support.v7.internal.widget;

import android.view.MotionEvent;
import android.view.View;

class aa implements View.OnTouchListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ t f51a;

    private aa(t tVar) {
        this.f51a = tVar;
    }

    /* synthetic */ aa(t tVar, u uVar) {
        this(tVar);
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        int action = motionEvent.getAction();
        int x = (int) motionEvent.getX();
        int y = (int) motionEvent.getY();
        if (action == 0 && this.f51a.c != null && this.f51a.c.isShowing() && x >= 0 && x < this.f51a.c.getWidth() && y >= 0 && y < this.f51a.c.getHeight()) {
            this.f51a.y.postDelayed(this.f51a.t, 250);
            return false;
        } else if (action != 1) {
            return false;
        } else {
            this.f51a.y.removeCallbacks(this.f51a.t);
            return false;
        }
    }
}
