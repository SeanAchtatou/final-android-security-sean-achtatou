package android.support.v7.internal.widget;

import android.database.DataSetObserver;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.SpinnerAdapter;

class am implements ListAdapter, SpinnerAdapter {

    /* renamed from: a  reason: collision with root package name */
    private SpinnerAdapter f58a;
    private ListAdapter b;

    public am(SpinnerAdapter spinnerAdapter) {
        this.f58a = spinnerAdapter;
        if (spinnerAdapter instanceof ListAdapter) {
            this.b = (ListAdapter) spinnerAdapter;
        }
    }

    public boolean areAllItemsEnabled() {
        ListAdapter listAdapter = this.b;
        if (listAdapter != null) {
            return listAdapter.areAllItemsEnabled();
        }
        return true;
    }

    public int getCount() {
        if (this.f58a == null) {
            return 0;
        }
        return this.f58a.getCount();
    }

    public View getDropDownView(int i, View view, ViewGroup viewGroup) {
        if (this.f58a == null) {
            return null;
        }
        return this.f58a.getDropDownView(i, view, viewGroup);
    }

    public Object getItem(int i) {
        if (this.f58a == null) {
            return null;
        }
        return this.f58a.getItem(i);
    }

    public long getItemId(int i) {
        if (this.f58a == null) {
            return -1;
        }
        return this.f58a.getItemId(i);
    }

    public int getItemViewType(int i) {
        return 0;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        return getDropDownView(i, view, viewGroup);
    }

    public int getViewTypeCount() {
        return 1;
    }

    public boolean hasStableIds() {
        return this.f58a != null && this.f58a.hasStableIds();
    }

    public boolean isEmpty() {
        return getCount() == 0;
    }

    public boolean isEnabled(int i) {
        ListAdapter listAdapter = this.b;
        if (listAdapter != null) {
            return listAdapter.isEnabled(i);
        }
        return true;
    }

    public void registerDataSetObserver(DataSetObserver dataSetObserver) {
        if (this.f58a != null) {
            this.f58a.registerDataSetObserver(dataSetObserver);
        }
    }

    public void unregisterDataSetObserver(DataSetObserver dataSetObserver) {
        if (this.f58a != null) {
            this.f58a.unregisterDataSetObserver(dataSetObserver);
        }
    }
}
