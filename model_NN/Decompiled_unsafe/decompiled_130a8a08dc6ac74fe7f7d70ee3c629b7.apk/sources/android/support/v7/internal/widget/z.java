package android.support.v7.internal.widget;

import android.widget.AbsListView;

class z implements AbsListView.OnScrollListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ t f77a;

    private z(t tVar) {
        this.f77a = tVar;
    }

    /* synthetic */ z(t tVar, u uVar) {
        this(tVar);
    }

    public void onScroll(AbsListView absListView, int i, int i2, int i3) {
    }

    public void onScrollStateChanged(AbsListView absListView, int i) {
        if (i == 1 && !this.f77a.g() && this.f77a.c.getContentView() != null) {
            this.f77a.y.removeCallbacks(this.f77a.t);
            this.f77a.t.run();
        }
    }
}
