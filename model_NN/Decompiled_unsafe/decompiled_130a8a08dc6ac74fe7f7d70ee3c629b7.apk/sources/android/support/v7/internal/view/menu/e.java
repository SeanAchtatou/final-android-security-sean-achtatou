package android.support.v7.internal.view.menu;

import android.content.Context;
import android.support.v7.b.b;
import android.widget.ImageButton;

class e extends ImageButton implements i {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ActionMenuPresenter f26a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public e(ActionMenuPresenter actionMenuPresenter, Context context) {
        super(context, null, b.actionOverflowButtonStyle);
        this.f26a = actionMenuPresenter;
        setClickable(true);
        setFocusable(true);
        setVisibility(0);
        setEnabled(true);
    }

    public boolean c() {
        return false;
    }

    public boolean d() {
        return false;
    }

    public boolean performClick() {
        if (!super.performClick()) {
            playSoundEffect(0);
            this.f26a.a();
        }
        return true;
    }
}
