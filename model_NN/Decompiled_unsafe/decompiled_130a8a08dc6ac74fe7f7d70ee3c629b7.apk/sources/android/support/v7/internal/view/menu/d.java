package android.support.v7.internal.view.menu;

import android.view.View;

class d implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ActionMenuPresenter f25a;
    private f b;

    public d(ActionMenuPresenter actionMenuPresenter, f fVar) {
        this.f25a = actionMenuPresenter;
        this.b = fVar;
    }

    public void run() {
        this.f25a.e.e();
        View view = (View) this.f25a.h;
        if (!(view == null || view.getWindowToken() == null || !this.b.a())) {
            f unused = this.f25a.v = this.b;
        }
        d unused2 = this.f25a.x = (d) null;
    }
}
