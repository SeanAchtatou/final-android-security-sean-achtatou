package android.support.v7.internal.view.menu;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import java.util.ArrayList;

class m extends BaseAdapter {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ l f32a;
    private int b = -1;

    public m(l lVar) {
        this.f32a = lVar;
        a();
    }

    /* renamed from: a */
    public r getItem(int i) {
        ArrayList k = this.f32a.c.k();
        int a2 = this.f32a.g + i;
        if (this.b >= 0 && a2 >= this.b) {
            a2++;
        }
        return (r) k.get(a2);
    }

    /* access modifiers changed from: package-private */
    public void a() {
        r q = this.f32a.c.q();
        if (q != null) {
            ArrayList k = this.f32a.c.k();
            int size = k.size();
            for (int i = 0; i < size; i++) {
                if (((r) k.get(i)) == q) {
                    this.b = i;
                    return;
                }
            }
        }
        this.b = -1;
    }

    public int getCount() {
        int size = this.f32a.c.k().size() - this.f32a.g;
        return this.b < 0 ? size : size - 1;
    }

    public long getItemId(int i) {
        return (long) i;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View getView(int i, View view, ViewGroup viewGroup) {
        View inflate = view == null ? this.f32a.b.inflate(this.f32a.e, viewGroup, false) : view;
        ((x) inflate).a(getItem(i), 0);
        return inflate;
    }

    public void notifyDataSetChanged() {
        a();
        super.notifyDataSetChanged();
    }
}
