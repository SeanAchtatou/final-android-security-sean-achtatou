package android.support.v7.internal.view.menu;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.support.v4.internal.view.SupportMenu;
import android.support.v4.view.ActionProvider;
import android.support.v7.b.c;
import android.view.ContextMenu;
import android.view.KeyCharacterMap;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class n implements SupportMenu {
    private static final int[] d = {1, 4, 5, 3, 2, 0};

    /* renamed from: a  reason: collision with root package name */
    CharSequence f33a;
    Drawable b;
    View c;
    private final Context e;
    private final Resources f;
    private boolean g;
    private boolean h;
    private o i;
    private ArrayList j;
    private ArrayList k;
    private boolean l;
    private ArrayList m;
    private ArrayList n;
    private boolean o;
    private int p = 0;
    private ContextMenu.ContextMenuInfo q;
    private boolean r = false;
    private boolean s = false;
    private boolean t = false;
    private boolean u = false;
    private ArrayList v = new ArrayList();
    private CopyOnWriteArrayList w = new CopyOnWriteArrayList();
    private r x;

    public n(Context context) {
        this.e = context;
        this.f = context.getResources();
        this.j = new ArrayList();
        this.k = new ArrayList();
        this.l = true;
        this.m = new ArrayList();
        this.n = new ArrayList();
        this.o = true;
        d(true);
    }

    private static int a(ArrayList arrayList, int i2) {
        for (int size = arrayList.size() - 1; size >= 0; size--) {
            if (((r) arrayList.get(size)).e() <= i2) {
                return size + 1;
            }
        }
        return 0;
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [android.support.v7.internal.view.menu.r, java.lang.Object, android.view.MenuItem] */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    private android.view.MenuItem a(int r9, int r10, int r11, java.lang.CharSequence r12) {
        /*
            r8 = this;
            int r5 = c(r11)
            android.support.v7.internal.view.menu.r r0 = new android.support.v7.internal.view.menu.r
            int r7 = r8.p
            r1 = r8
            r2 = r9
            r3 = r10
            r4 = r11
            r6 = r12
            r0.<init>(r1, r2, r3, r4, r5, r6, r7)
            android.view.ContextMenu$ContextMenuInfo r1 = r8.q
            if (r1 == 0) goto L_0x0019
            android.view.ContextMenu$ContextMenuInfo r1 = r8.q
            r0.a(r1)
        L_0x0019:
            java.util.ArrayList r1 = r8.j
            java.util.ArrayList r2 = r8.j
            int r2 = a(r2, r5)
            r1.add(r2, r0)
            r1 = 1
            r8.b(r1)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.internal.view.menu.n.a(int, int, int, java.lang.CharSequence):android.view.MenuItem");
    }

    private void a(int i2, CharSequence charSequence, int i3, Drawable drawable, View view) {
        Resources c2 = c();
        if (view != null) {
            this.c = view;
            this.f33a = null;
            this.b = null;
        } else {
            if (i2 > 0) {
                this.f33a = c2.getText(i2);
            } else if (charSequence != null) {
                this.f33a = charSequence;
            }
            if (i3 > 0) {
                this.b = c2.getDrawable(i3);
            } else if (drawable != null) {
                this.b = drawable;
            }
            this.c = null;
        }
        b(false);
    }

    private void a(int i2, boolean z) {
        if (i2 >= 0 && i2 < this.j.size()) {
            this.j.remove(i2);
            if (z) {
                b(true);
            }
        }
    }

    private boolean a(y yVar) {
        boolean z = false;
        if (this.w.isEmpty()) {
            return false;
        }
        Iterator it = this.w.iterator();
        while (true) {
            boolean z2 = z;
            if (!it.hasNext()) {
                return z2;
            }
            WeakReference weakReference = (WeakReference) it.next();
            u uVar = (u) weakReference.get();
            if (uVar == null) {
                this.w.remove(weakReference);
            } else if (!z2) {
                z2 = uVar.a(yVar);
            }
            z = z2;
        }
    }

    private static int c(int i2) {
        int i3 = (-65536 & i2) >> 16;
        if (i3 >= 0 && i3 < d.length) {
            return (d[i3] << 16) | (65535 & i2);
        }
        throw new IllegalArgumentException("order does not contain a valid category.");
    }

    private void c(boolean z) {
        if (!this.w.isEmpty()) {
            f();
            Iterator it = this.w.iterator();
            while (it.hasNext()) {
                WeakReference weakReference = (WeakReference) it.next();
                u uVar = (u) weakReference.get();
                if (uVar == null) {
                    this.w.remove(weakReference);
                } else {
                    uVar.b(z);
                }
            }
            g();
        }
    }

    private void d(boolean z) {
        boolean z2 = true;
        if (!z || this.f.getConfiguration().keyboard == 1 || !this.f.getBoolean(c.abc_config_showMenuShortcutsWhenKeyboardPresent)) {
            z2 = false;
        }
        this.h = z2;
    }

    public int a(int i2) {
        int size = size();
        for (int i3 = 0; i3 < size; i3++) {
            if (((r) this.j.get(i3)).d() == i2) {
                return i3;
            }
        }
        return -1;
    }

    public int a(int i2, int i3) {
        int size = size();
        if (i3 < 0) {
            i3 = 0;
        }
        for (int i4 = i3; i4 < size; i4++) {
            if (((r) this.j.get(i4)).c() == i2) {
                return i4;
            }
        }
        return -1;
    }

    /* access modifiers changed from: protected */
    public n a(Drawable drawable) {
        a(0, null, 0, drawable, null);
        return this;
    }

    /* access modifiers changed from: protected */
    public n a(View view) {
        a(0, null, 0, null, view);
        return this;
    }

    /* access modifiers changed from: protected */
    public n a(CharSequence charSequence) {
        a(0, charSequence, 0, null, null);
        return this;
    }

    /* access modifiers changed from: package-private */
    public r a(int i2, KeyEvent keyEvent) {
        ArrayList arrayList = this.v;
        arrayList.clear();
        a(arrayList, i2, keyEvent);
        if (arrayList.isEmpty()) {
            return null;
        }
        int metaState = keyEvent.getMetaState();
        KeyCharacterMap.KeyData keyData = new KeyCharacterMap.KeyData();
        keyEvent.getKeyData(keyData);
        int size = arrayList.size();
        if (size == 1) {
            return (r) arrayList.get(0);
        }
        boolean a2 = a();
        for (int i3 = 0; i3 < size; i3++) {
            r rVar = (r) arrayList.get(i3);
            char f2 = a2 ? rVar.f() : rVar.g();
            if (f2 == keyData.meta[0] && (metaState & 2) == 0) {
                return rVar;
            }
            if (f2 == keyData.meta[2] && (metaState & 2) != 0) {
                return rVar;
            }
            if (a2 && f2 == 8 && i2 == 67) {
                return rVar;
            }
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public void a(r rVar) {
        this.o = true;
        b(true);
    }

    public void a(u uVar) {
        this.w.add(new WeakReference(uVar));
        uVar.a(this.e, this);
        this.o = true;
    }

    /* access modifiers changed from: package-private */
    public void a(List list, int i2, KeyEvent keyEvent) {
        boolean a2 = a();
        int metaState = keyEvent.getMetaState();
        KeyCharacterMap.KeyData keyData = new KeyCharacterMap.KeyData();
        if (keyEvent.getKeyData(keyData) || i2 == 67) {
            int size = this.j.size();
            for (int i3 = 0; i3 < size; i3++) {
                r rVar = (r) this.j.get(i3);
                if (rVar.l()) {
                    ((n) rVar.k()).a(list, i2, keyEvent);
                }
                char f2 = a2 ? rVar.f() : rVar.g();
                if ((metaState & 5) == 0 && f2 != 0 && ((f2 == keyData.meta[0] || f2 == keyData.meta[2] || (a2 && f2 == 8 && i2 == 67)) && rVar.b())) {
                    list.add(rVar);
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(boolean z) {
        if (!this.u) {
            this.u = true;
            Iterator it = this.w.iterator();
            while (it.hasNext()) {
                WeakReference weakReference = (WeakReference) it.next();
                u uVar = (u) weakReference.get();
                if (uVar == null) {
                    this.w.remove(weakReference);
                } else {
                    uVar.a(this, z);
                }
            }
            this.u = false;
        }
    }

    /* access modifiers changed from: package-private */
    public boolean a() {
        return this.g;
    }

    /* access modifiers changed from: package-private */
    public boolean a(n nVar, MenuItem menuItem) {
        return this.i != null && this.i.a(nVar, menuItem);
    }

    public boolean a(MenuItem menuItem, int i2) {
        r rVar = (r) menuItem;
        if (rVar == null || !rVar.b()) {
            return false;
        }
        boolean a2 = rVar.a();
        ActionProvider A = rVar.A();
        boolean z = A != null && A.hasSubMenu();
        if (rVar.D()) {
            boolean B = rVar.B() | a2;
            if (!B) {
                return B;
            }
            a(true);
            return B;
        } else if (rVar.l() || z) {
            a(false);
            if (!rVar.l()) {
                rVar.a(new y(d(), this, rVar));
            }
            y yVar = (y) rVar.k();
            if (z) {
                A.onPrepareSubMenu(yVar);
            }
            boolean a3 = a(yVar) | a2;
            if (a3) {
                return a3;
            }
            a(true);
            return a3;
        } else {
            if ((i2 & 1) == 0) {
                a(true);
            }
            return a2;
        }
    }

    public MenuItem add(int i2) {
        return a(0, 0, 0, this.f.getString(i2));
    }

    public MenuItem add(int i2, int i3, int i4, int i5) {
        return a(i2, i3, i4, this.f.getString(i5));
    }

    public MenuItem add(int i2, int i3, int i4, CharSequence charSequence) {
        return a(i2, i3, i4, charSequence);
    }

    public MenuItem add(CharSequence charSequence) {
        return a(0, 0, 0, charSequence);
    }

    public int addIntentOptions(int i2, int i3, int i4, ComponentName componentName, Intent[] intentArr, Intent intent, int i5, MenuItem[] menuItemArr) {
        PackageManager packageManager = this.e.getPackageManager();
        List<ResolveInfo> queryIntentActivityOptions = packageManager.queryIntentActivityOptions(componentName, intentArr, intent, 0);
        int size = queryIntentActivityOptions != null ? queryIntentActivityOptions.size() : 0;
        if ((i5 & 1) == 0) {
            removeGroup(i2);
        }
        for (int i6 = 0; i6 < size; i6++) {
            ResolveInfo resolveInfo = queryIntentActivityOptions.get(i6);
            Intent intent2 = new Intent(resolveInfo.specificIndex < 0 ? intent : intentArr[resolveInfo.specificIndex]);
            intent2.setComponent(new ComponentName(resolveInfo.activityInfo.applicationInfo.packageName, resolveInfo.activityInfo.name));
            MenuItem intent3 = add(i2, i3, i4, resolveInfo.loadLabel(packageManager)).setIcon(resolveInfo.loadIcon(packageManager)).setIntent(intent2);
            if (menuItemArr != null && resolveInfo.specificIndex >= 0) {
                menuItemArr[resolveInfo.specificIndex] = intent3;
            }
        }
        return size;
    }

    public SubMenu addSubMenu(int i2) {
        return addSubMenu(0, 0, 0, this.f.getString(i2));
    }

    public SubMenu addSubMenu(int i2, int i3, int i4, int i5) {
        return addSubMenu(i2, i3, i4, this.f.getString(i5));
    }

    public SubMenu addSubMenu(int i2, int i3, int i4, CharSequence charSequence) {
        r rVar = (r) a(i2, i3, i4, charSequence);
        y yVar = new y(this.e, this, rVar);
        rVar.a(yVar);
        return yVar;
    }

    public SubMenu addSubMenu(CharSequence charSequence) {
        return addSubMenu(0, 0, 0, charSequence);
    }

    public int b(int i2) {
        return a(i2, 0);
    }

    /* access modifiers changed from: package-private */
    public void b(boolean z) {
        if (!this.r) {
            if (z) {
                this.l = true;
                this.o = true;
            }
            c(z);
            return;
        }
        this.s = true;
    }

    public boolean b() {
        return this.h;
    }

    public boolean b(r rVar) {
        boolean z = false;
        if (!this.w.isEmpty()) {
            f();
            Iterator it = this.w.iterator();
            while (true) {
                boolean z2 = z;
                if (!it.hasNext()) {
                    z = z2;
                    break;
                }
                WeakReference weakReference = (WeakReference) it.next();
                u uVar = (u) weakReference.get();
                if (uVar == null) {
                    this.w.remove(weakReference);
                    z = z2;
                } else {
                    z = uVar.a(this, rVar);
                    if (z) {
                        break;
                    }
                }
            }
            g();
            if (z) {
                this.x = rVar;
            }
        }
        return z;
    }

    /* access modifiers changed from: package-private */
    public Resources c() {
        return this.f;
    }

    public boolean c(r rVar) {
        boolean z = false;
        if (!this.w.isEmpty() && this.x == rVar) {
            f();
            Iterator it = this.w.iterator();
            while (true) {
                boolean z2 = z;
                if (!it.hasNext()) {
                    z = z2;
                    break;
                }
                WeakReference weakReference = (WeakReference) it.next();
                u uVar = (u) weakReference.get();
                if (uVar == null) {
                    this.w.remove(weakReference);
                    z = z2;
                } else {
                    z = uVar.b(this, rVar);
                    if (z) {
                        break;
                    }
                }
            }
            g();
            if (z) {
                this.x = null;
            }
        }
        return z;
    }

    public void clear() {
        if (this.x != null) {
            c(this.x);
        }
        this.j.clear();
        b(true);
    }

    public void clearHeader() {
        this.b = null;
        this.f33a = null;
        this.c = null;
        b(false);
    }

    public void close() {
        a(true);
    }

    public Context d() {
        return this.e;
    }

    public void e() {
        if (this.i != null) {
            this.i.a(this);
        }
    }

    public void f() {
        if (!this.r) {
            this.r = true;
            this.s = false;
        }
    }

    /* JADX WARN: Type inference failed for: r0v4, types: [android.support.v7.internal.view.menu.r, android.view.MenuItem] */
    public MenuItem findItem(int i2) {
        MenuItem findItem;
        int size = size();
        for (int i3 = 0; i3 < size; i3++) {
            ? r0 = (r) this.j.get(i3);
            if (r0.d() == i2) {
                return r0;
            }
            if (r0.l() && (findItem = r0.k().findItem(i2)) != null) {
                return findItem;
            }
        }
        return null;
    }

    public void g() {
        this.r = false;
        if (this.s) {
            this.s = false;
            b(true);
        }
    }

    public MenuItem getItem(int i2) {
        return (MenuItem) this.j.get(i2);
    }

    /* access modifiers changed from: package-private */
    public ArrayList h() {
        if (!this.l) {
            return this.k;
        }
        this.k.clear();
        int size = this.j.size();
        for (int i2 = 0; i2 < size; i2++) {
            r rVar = (r) this.j.get(i2);
            if (rVar.s()) {
                this.k.add(rVar);
            }
        }
        this.l = false;
        this.o = true;
        return this.k;
    }

    public boolean hasVisibleItems() {
        int size = size();
        for (int i2 = 0; i2 < size; i2++) {
            if (((r) this.j.get(i2)).s()) {
                return true;
            }
        }
        return false;
    }

    public void i() {
        boolean f2;
        if (this.o) {
            Iterator it = this.w.iterator();
            boolean z = false;
            while (it.hasNext()) {
                WeakReference weakReference = (WeakReference) it.next();
                u uVar = (u) weakReference.get();
                if (uVar == null) {
                    this.w.remove(weakReference);
                    f2 = z;
                } else {
                    f2 = uVar.f() | z;
                }
                z = f2;
            }
            if (z) {
                this.m.clear();
                this.n.clear();
                ArrayList h2 = h();
                int size = h2.size();
                for (int i2 = 0; i2 < size; i2++) {
                    r rVar = (r) h2.get(i2);
                    if (rVar.v()) {
                        this.m.add(rVar);
                    } else {
                        this.n.add(rVar);
                    }
                }
            } else {
                this.m.clear();
                this.n.clear();
                this.n.addAll(h());
            }
            this.o = false;
        }
    }

    public boolean isShortcutKey(int i2, KeyEvent keyEvent) {
        return a(i2, keyEvent) != null;
    }

    /* access modifiers changed from: package-private */
    public ArrayList j() {
        i();
        return this.m;
    }

    /* access modifiers changed from: package-private */
    public ArrayList k() {
        i();
        return this.n;
    }

    public CharSequence l() {
        return this.f33a;
    }

    public Drawable m() {
        return this.b;
    }

    public View n() {
        return this.c;
    }

    public n o() {
        return this;
    }

    /* access modifiers changed from: package-private */
    public boolean p() {
        return this.t;
    }

    public boolean performIdentifierAction(int i2, int i3) {
        return a(findItem(i2), i3);
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [android.support.v7.internal.view.menu.r, android.view.MenuItem] */
    public boolean performShortcut(int i2, KeyEvent keyEvent, int i3) {
        ? a2 = a(i2, keyEvent);
        boolean z = false;
        if (a2 != 0) {
            z = a((MenuItem) a2, i3);
        }
        if ((i3 & 2) != 0) {
            a(true);
        }
        return z;
    }

    public r q() {
        return this.x;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.internal.view.menu.n.a(int, boolean):void
     arg types: [int, int]
     candidates:
      android.support.v7.internal.view.menu.n.a(java.util.ArrayList, int):int
      android.support.v7.internal.view.menu.n.a(int, int):int
      android.support.v7.internal.view.menu.n.a(int, android.view.KeyEvent):android.support.v7.internal.view.menu.r
      android.support.v7.internal.view.menu.n.a(android.support.v7.internal.view.menu.n, android.view.MenuItem):boolean
      android.support.v7.internal.view.menu.n.a(android.view.MenuItem, int):boolean
      android.support.v7.internal.view.menu.n.a(int, boolean):void */
    public void removeGroup(int i2) {
        int b2 = b(i2);
        if (b2 >= 0) {
            int size = this.j.size() - b2;
            int i3 = 0;
            while (true) {
                int i4 = i3 + 1;
                if (i3 >= size || ((r) this.j.get(b2)).c() != i2) {
                    b(true);
                } else {
                    a(b2, false);
                    i3 = i4;
                }
            }
            b(true);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.internal.view.menu.n.a(int, boolean):void
     arg types: [int, int]
     candidates:
      android.support.v7.internal.view.menu.n.a(java.util.ArrayList, int):int
      android.support.v7.internal.view.menu.n.a(int, int):int
      android.support.v7.internal.view.menu.n.a(int, android.view.KeyEvent):android.support.v7.internal.view.menu.r
      android.support.v7.internal.view.menu.n.a(android.support.v7.internal.view.menu.n, android.view.MenuItem):boolean
      android.support.v7.internal.view.menu.n.a(android.view.MenuItem, int):boolean
      android.support.v7.internal.view.menu.n.a(int, boolean):void */
    public void removeItem(int i2) {
        a(a(i2), true);
    }

    public void setGroupCheckable(int i2, boolean z, boolean z2) {
        int size = this.j.size();
        for (int i3 = 0; i3 < size; i3++) {
            r rVar = (r) this.j.get(i3);
            if (rVar.c() == i2) {
                rVar.c(z2);
                rVar.b(z);
            }
        }
    }

    public void setGroupEnabled(int i2, boolean z) {
        int size = this.j.size();
        for (int i3 = 0; i3 < size; i3++) {
            r rVar = (r) this.j.get(i3);
            if (rVar.c() == i2) {
                rVar.a(z);
            }
        }
    }

    public void setGroupVisible(int i2, boolean z) {
        int size = this.j.size();
        int i3 = 0;
        boolean z2 = false;
        while (i3 < size) {
            r rVar = (r) this.j.get(i3);
            i3++;
            z2 = (rVar.c() != i2 || !rVar.d(z)) ? z2 : true;
        }
        if (z2) {
            b(true);
        }
    }

    public void setQwertyMode(boolean z) {
        this.g = z;
        b(false);
    }

    public int size() {
        return this.j.size();
    }
}
