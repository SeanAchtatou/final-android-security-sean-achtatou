package android.support.v7.internal.view;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.os.Build;
import android.support.v7.b.b;
import android.support.v7.b.c;
import android.support.v7.b.d;
import android.support.v7.b.f;
import android.support.v7.b.j;

public class a {

    /* renamed from: a  reason: collision with root package name */
    private Context f15a;

    private a(Context context) {
        this.f15a = context;
    }

    public static a a(Context context) {
        return new a(context);
    }

    public int a() {
        return this.f15a.getResources().getInteger(f.abc_max_action_buttons);
    }

    public boolean b() {
        return Build.VERSION.SDK_INT >= 11;
    }

    public int c() {
        return this.f15a.getResources().getDisplayMetrics().widthPixels / 2;
    }

    public boolean d() {
        return this.f15a.getResources().getBoolean(c.abc_action_bar_embed_tabs_pre_jb);
    }

    public int e() {
        TypedArray obtainStyledAttributes = this.f15a.obtainStyledAttributes(null, j.ActionBar, b.actionBarStyle, 0);
        int layoutDimension = obtainStyledAttributes.getLayoutDimension(1, 0);
        Resources resources = this.f15a.getResources();
        if (!d()) {
            layoutDimension = Math.min(layoutDimension, resources.getDimensionPixelSize(d.abc_action_bar_stacked_max_height));
        }
        obtainStyledAttributes.recycle();
        return layoutDimension;
    }

    public int f() {
        return this.f15a.getResources().getDimensionPixelSize(d.abc_action_bar_stacked_tab_max_width);
    }
}
