package android.support.v7.internal.widget;

import android.database.DataSetObserver;
import android.os.Parcelable;

class m extends DataSetObserver {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ k f67a;
    private Parcelable b = null;

    m(k kVar) {
        this.f67a = kVar;
    }

    public void onChanged() {
        this.f67a.u = true;
        this.f67a.A = this.f67a.z;
        this.f67a.z = this.f67a.e().getCount();
        if (!this.f67a.e().hasStableIds() || this.b == null || this.f67a.A != 0 || this.f67a.z <= 0) {
            this.f67a.n();
        } else {
            this.f67a.onRestoreInstanceState(this.b);
            this.b = null;
        }
        this.f67a.i();
        this.f67a.requestLayout();
    }

    public void onInvalidated() {
        this.f67a.u = true;
        if (this.f67a.e().hasStableIds()) {
            this.b = this.f67a.onSaveInstanceState();
        }
        this.f67a.A = this.f67a.z;
        this.f67a.z = 0;
        this.f67a.x = -1;
        this.f67a.y = Long.MIN_VALUE;
        this.f67a.v = -1;
        this.f67a.w = Long.MIN_VALUE;
        this.f67a.p = false;
        this.f67a.i();
        this.f67a.requestLayout();
    }
}
