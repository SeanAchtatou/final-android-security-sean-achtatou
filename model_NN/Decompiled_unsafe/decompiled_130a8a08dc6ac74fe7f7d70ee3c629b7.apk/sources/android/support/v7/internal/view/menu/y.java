package android.support.v7.internal.view.menu;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;

public class y extends n implements SubMenu {
    private n d;
    private r e;

    public y(Context context, n nVar, r rVar) {
        super(context);
        this.d = nVar;
        this.e = rVar;
    }

    public boolean a() {
        return this.d.a();
    }

    public boolean a(n nVar, MenuItem menuItem) {
        return super.a(nVar, menuItem) || this.d.a(nVar, menuItem);
    }

    public boolean b() {
        return this.d.b();
    }

    public boolean b(r rVar) {
        return this.d.b(rVar);
    }

    public boolean c(r rVar) {
        return this.d.c(rVar);
    }

    public void clearHeader() {
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [android.support.v7.internal.view.menu.r, android.view.MenuItem] */
    public MenuItem getItem() {
        return this.e;
    }

    public n o() {
        return this.d;
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [android.view.Menu, android.support.v7.internal.view.menu.n] */
    public Menu r() {
        return this.d;
    }

    public SubMenu setHeaderIcon(int i) {
        super.a(d().getResources().getDrawable(i));
        return this;
    }

    public SubMenu setHeaderIcon(Drawable drawable) {
        super.a(drawable);
        return this;
    }

    public SubMenu setHeaderTitle(int i) {
        super.a(d().getResources().getString(i));
        return this;
    }

    public SubMenu setHeaderTitle(CharSequence charSequence) {
        super.a(charSequence);
        return this;
    }

    public SubMenu setHeaderView(View view) {
        super.a(view);
        return this;
    }

    public SubMenu setIcon(int i) {
        this.e.a(i);
        return this;
    }

    public SubMenu setIcon(Drawable drawable) {
        this.e.a(drawable);
        return this;
    }

    public void setQwertyMode(boolean z) {
        this.d.setQwertyMode(z);
    }
}
