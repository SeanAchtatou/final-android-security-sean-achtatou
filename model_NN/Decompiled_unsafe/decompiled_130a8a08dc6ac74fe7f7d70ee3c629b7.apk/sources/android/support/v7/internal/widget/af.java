package android.support.v7.internal.widget;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.v7.internal.widget.ProgressBarICS;

final class af implements Parcelable.Creator {
    af() {
    }

    /* renamed from: a */
    public ProgressBarICS.SavedState createFromParcel(Parcel parcel) {
        return new ProgressBarICS.SavedState(parcel);
    }

    /* renamed from: a */
    public ProgressBarICS.SavedState[] newArray(int i) {
        return new ProgressBarICS.SavedState[i];
    }
}
