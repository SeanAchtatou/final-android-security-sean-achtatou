package android.support.v7.internal.widget;

import android.content.Context;
import android.database.DataSetObserver;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.PopupWindow;
import java.util.Locale;

public class t {
    private boolean A;

    /* renamed from: a  reason: collision with root package name */
    int f71a = Integer.MAX_VALUE;
    private Context b;
    /* access modifiers changed from: private */
    public PopupWindow c;
    private ListAdapter d;
    /* access modifiers changed from: private */
    public w e;
    private int f = -2;
    private int g = -2;
    private int h;
    private int i;
    private boolean j;
    private boolean k = false;
    private boolean l = false;
    private View m;
    private int n = 0;
    private DataSetObserver o;
    private View p;
    private Drawable q;
    private AdapterView.OnItemClickListener r;
    private AdapterView.OnItemSelectedListener s;
    /* access modifiers changed from: private */
    public final ab t = new ab(this, null);
    private final aa u = new aa(this, null);
    private final z v = new z(this, null);
    private final x w = new x(this, null);
    private Runnable x;
    /* access modifiers changed from: private */
    public Handler y = new Handler();
    private Rect z = new Rect();

    public t(Context context, AttributeSet attributeSet, int i2) {
        this.b = context;
        this.c = new PopupWindow(context, attributeSet, i2);
        this.c.setInputMethodMode(1);
        Locale locale = this.b.getResources().getConfiguration().locale;
    }

    private void i() {
        if (this.m != null) {
            ViewParent parent = this.m.getParent();
            if (parent instanceof ViewGroup) {
                ((ViewGroup) parent).removeView(this.m);
            }
        }
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v19, resolved type: android.support.v7.internal.widget.w} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v20, resolved type: android.support.v7.internal.widget.w} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v21, resolved type: android.widget.LinearLayout} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v24, resolved type: android.support.v7.internal.widget.w} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private int j() {
        /*
            r10 = this;
            r9 = 1073741824(0x40000000, float:2.0)
            r8 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = -1
            r1 = 1
            r2 = 0
            android.support.v7.internal.widget.w r0 = r10.e
            if (r0 != 0) goto L_0x0103
            android.content.Context r5 = r10.b
            android.support.v7.internal.widget.u r0 = new android.support.v7.internal.widget.u
            r0.<init>(r10)
            r10.x = r0
            android.support.v7.internal.widget.w r4 = new android.support.v7.internal.widget.w
            boolean r0 = r10.A
            if (r0 != 0) goto L_0x00f2
            r0 = r1
        L_0x001b:
            r4.<init>(r5, r0)
            r10.e = r4
            android.graphics.drawable.Drawable r0 = r10.q
            if (r0 == 0) goto L_0x002b
            android.support.v7.internal.widget.w r0 = r10.e
            android.graphics.drawable.Drawable r4 = r10.q
            r0.setSelector(r4)
        L_0x002b:
            android.support.v7.internal.widget.w r0 = r10.e
            android.widget.ListAdapter r4 = r10.d
            r0.setAdapter(r4)
            android.support.v7.internal.widget.w r0 = r10.e
            android.widget.AdapterView$OnItemClickListener r4 = r10.r
            r0.setOnItemClickListener(r4)
            android.support.v7.internal.widget.w r0 = r10.e
            r0.setFocusable(r1)
            android.support.v7.internal.widget.w r0 = r10.e
            r0.setFocusableInTouchMode(r1)
            android.support.v7.internal.widget.w r0 = r10.e
            android.support.v7.internal.widget.v r4 = new android.support.v7.internal.widget.v
            r4.<init>(r10)
            r0.setOnItemSelectedListener(r4)
            android.support.v7.internal.widget.w r0 = r10.e
            android.support.v7.internal.widget.z r4 = r10.v
            r0.setOnScrollListener(r4)
            android.widget.AdapterView$OnItemSelectedListener r0 = r10.s
            if (r0 == 0) goto L_0x005f
            android.support.v7.internal.widget.w r0 = r10.e
            android.widget.AdapterView$OnItemSelectedListener r4 = r10.s
            r0.setOnItemSelectedListener(r4)
        L_0x005f:
            android.support.v7.internal.widget.w r0 = r10.e
            android.view.View r6 = r10.m
            if (r6 == 0) goto L_0x017e
            android.widget.LinearLayout r4 = new android.widget.LinearLayout
            r4.<init>(r5)
            r4.setOrientation(r1)
            android.widget.LinearLayout$LayoutParams r5 = new android.widget.LinearLayout$LayoutParams
            r7 = 1065353216(0x3f800000, float:1.0)
            r5.<init>(r3, r2, r7)
            int r7 = r10.n
            switch(r7) {
                case 0: goto L_0x00fc;
                case 1: goto L_0x00f5;
                default: goto L_0x0079;
            }
        L_0x0079:
            java.lang.String r0 = "ListPopupWindow"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r7 = "Invalid hint position "
            java.lang.StringBuilder r5 = r5.append(r7)
            int r7 = r10.n
            java.lang.StringBuilder r5 = r5.append(r7)
            java.lang.String r5 = r5.toString()
            android.util.Log.e(r0, r5)
        L_0x0093:
            int r0 = r10.g
            int r0 = android.view.View.MeasureSpec.makeMeasureSpec(r0, r8)
            r6.measure(r0, r2)
            android.view.ViewGroup$LayoutParams r0 = r6.getLayoutParams()
            android.widget.LinearLayout$LayoutParams r0 = (android.widget.LinearLayout.LayoutParams) r0
            int r5 = r6.getMeasuredHeight()
            int r6 = r0.topMargin
            int r5 = r5 + r6
            int r0 = r0.bottomMargin
            int r0 = r0 + r5
        L_0x00ac:
            android.widget.PopupWindow r5 = r10.c
            r5.setContentView(r4)
            r6 = r0
        L_0x00b2:
            android.widget.PopupWindow r0 = r10.c
            android.graphics.drawable.Drawable r0 = r0.getBackground()
            if (r0 == 0) goto L_0x0121
            android.graphics.Rect r4 = r10.z
            r0.getPadding(r4)
            android.graphics.Rect r0 = r10.z
            int r0 = r0.top
            android.graphics.Rect r4 = r10.z
            int r4 = r4.bottom
            int r0 = r0 + r4
            boolean r4 = r10.j
            if (r4 != 0) goto L_0x0178
            android.graphics.Rect r4 = r10.z
            int r4 = r4.top
            int r4 = -r4
            r10.i = r4
            r7 = r0
        L_0x00d4:
            android.widget.PopupWindow r0 = r10.c
            int r0 = r0.getInputMethodMode()
            r4 = 2
            if (r0 != r4) goto L_0x0128
        L_0x00dd:
            android.view.View r0 = r10.b()
            int r4 = r10.i
            int r4 = r10.a(r0, r4, r1)
            boolean r0 = r10.k
            if (r0 != 0) goto L_0x00ef
            int r0 = r10.f
            if (r0 != r3) goto L_0x012a
        L_0x00ef:
            int r0 = r4 + r7
        L_0x00f1:
            return r0
        L_0x00f2:
            r0 = r2
            goto L_0x001b
        L_0x00f5:
            r4.addView(r0, r5)
            r4.addView(r6)
            goto L_0x0093
        L_0x00fc:
            r4.addView(r6)
            r4.addView(r0, r5)
            goto L_0x0093
        L_0x0103:
            android.widget.PopupWindow r0 = r10.c
            android.view.View r0 = r0.getContentView()
            android.view.ViewGroup r0 = (android.view.ViewGroup) r0
            android.view.View r4 = r10.m
            if (r4 == 0) goto L_0x017b
            android.view.ViewGroup$LayoutParams r0 = r4.getLayoutParams()
            android.widget.LinearLayout$LayoutParams r0 = (android.widget.LinearLayout.LayoutParams) r0
            int r4 = r4.getMeasuredHeight()
            int r5 = r0.topMargin
            int r4 = r4 + r5
            int r0 = r0.bottomMargin
            int r0 = r0 + r4
            r6 = r0
            goto L_0x00b2
        L_0x0121:
            android.graphics.Rect r0 = r10.z
            r0.setEmpty()
            r7 = r2
            goto L_0x00d4
        L_0x0128:
            r1 = r2
            goto L_0x00dd
        L_0x012a:
            int r0 = r10.g
            switch(r0) {
                case -2: goto L_0x0142;
                case -1: goto L_0x015d;
                default: goto L_0x012f;
            }
        L_0x012f:
            int r0 = r10.g
            int r1 = android.view.View.MeasureSpec.makeMeasureSpec(r0, r9)
        L_0x0135:
            android.support.v7.internal.widget.w r0 = r10.e
            int r4 = r4 - r6
            r5 = r3
            int r0 = r0.a(r1, r2, r3, r4, r5)
            if (r0 <= 0) goto L_0x0140
            int r6 = r6 + r7
        L_0x0140:
            int r0 = r0 + r6
            goto L_0x00f1
        L_0x0142:
            android.content.Context r0 = r10.b
            android.content.res.Resources r0 = r0.getResources()
            android.util.DisplayMetrics r0 = r0.getDisplayMetrics()
            int r0 = r0.widthPixels
            android.graphics.Rect r1 = r10.z
            int r1 = r1.left
            android.graphics.Rect r5 = r10.z
            int r5 = r5.right
            int r1 = r1 + r5
            int r0 = r0 - r1
            int r1 = android.view.View.MeasureSpec.makeMeasureSpec(r0, r8)
            goto L_0x0135
        L_0x015d:
            android.content.Context r0 = r10.b
            android.content.res.Resources r0 = r0.getResources()
            android.util.DisplayMetrics r0 = r0.getDisplayMetrics()
            int r0 = r0.widthPixels
            android.graphics.Rect r1 = r10.z
            int r1 = r1.left
            android.graphics.Rect r5 = r10.z
            int r5 = r5.right
            int r1 = r1 + r5
            int r0 = r0 - r1
            int r1 = android.view.View.MeasureSpec.makeMeasureSpec(r0, r9)
            goto L_0x0135
        L_0x0178:
            r7 = r0
            goto L_0x00d4
        L_0x017b:
            r6 = r2
            goto L_0x00b2
        L_0x017e:
            r4 = r0
            r0 = r2
            goto L_0x00ac
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.internal.widget.t.j():int");
    }

    public int a(View view, int i2, boolean z2) {
        Rect rect = new Rect();
        view.getWindowVisibleDisplayFrame(rect);
        int[] iArr = new int[2];
        view.getLocationOnScreen(iArr);
        int i3 = rect.bottom;
        if (z2) {
            i3 = view.getContext().getResources().getDisplayMetrics().heightPixels;
        }
        int max = Math.max((i3 - (iArr[1] + view.getHeight())) - i2, (iArr[1] - rect.top) + i2);
        if (this.c.getBackground() == null) {
            return max;
        }
        this.c.getBackground().getPadding(this.z);
        return max - (this.z.top + this.z.bottom);
    }

    public Drawable a() {
        return this.c.getBackground();
    }

    public void a(int i2) {
        this.n = i2;
    }

    public void a(Drawable drawable) {
        this.c.setBackgroundDrawable(drawable);
    }

    public void a(View view) {
        this.p = view;
    }

    public void a(AdapterView.OnItemClickListener onItemClickListener) {
        this.r = onItemClickListener;
    }

    public void a(ListAdapter listAdapter) {
        if (this.o == null) {
            this.o = new y(this, null);
        } else if (this.d != null) {
            this.d.unregisterDataSetObserver(this.o);
        }
        this.d = listAdapter;
        if (this.d != null) {
            listAdapter.registerDataSetObserver(this.o);
        }
        if (this.e != null) {
            this.e.setAdapter(this.d);
        }
    }

    public void a(PopupWindow.OnDismissListener onDismissListener) {
        this.c.setOnDismissListener(onDismissListener);
    }

    public void a(boolean z2) {
        this.A = true;
        this.c.setFocusable(z2);
    }

    public View b() {
        return this.p;
    }

    public void b(int i2) {
        this.h = i2;
    }

    public void c() {
        int i2;
        int i3;
        boolean z2 = true;
        boolean z3 = false;
        int i4 = -1;
        int j2 = j();
        boolean g2 = g();
        if (this.c.isShowing()) {
            int width = this.g == -1 ? -1 : this.g == -2 ? b().getWidth() : this.g;
            if (this.f == -1) {
                if (!g2) {
                    j2 = -1;
                }
                if (g2) {
                    PopupWindow popupWindow = this.c;
                    if (this.g != -1) {
                        i4 = 0;
                    }
                    popupWindow.setWindowLayoutMode(i4, 0);
                } else {
                    this.c.setWindowLayoutMode(this.g == -1 ? -1 : 0, -1);
                }
            } else if (this.f != -2) {
                j2 = this.f;
            }
            PopupWindow popupWindow2 = this.c;
            if (!this.l && !this.k) {
                z3 = true;
            }
            popupWindow2.setOutsideTouchable(z3);
            this.c.update(b(), this.h, this.i, width, j2);
            return;
        }
        if (this.g == -1) {
            i2 = -1;
        } else if (this.g == -2) {
            this.c.setWidth(b().getWidth());
            i2 = 0;
        } else {
            this.c.setWidth(this.g);
            i2 = 0;
        }
        if (this.f == -1) {
            i3 = -1;
        } else if (this.f == -2) {
            this.c.setHeight(j2);
            i3 = 0;
        } else {
            this.c.setHeight(this.f);
            i3 = 0;
        }
        this.c.setWindowLayoutMode(i2, i3);
        PopupWindow popupWindow3 = this.c;
        if (this.l || this.k) {
            z2 = false;
        }
        popupWindow3.setOutsideTouchable(z2);
        this.c.setTouchInterceptor(this.u);
        this.c.showAsDropDown(b(), this.h, this.i);
        this.e.setSelection(-1);
        if (!this.A || this.e.isInTouchMode()) {
            e();
        }
        if (!this.A) {
            this.y.post(this.w);
        }
    }

    public void c(int i2) {
        this.i = i2;
        this.j = true;
    }

    public void d() {
        this.c.dismiss();
        i();
        this.c.setContentView(null);
        this.e = null;
        this.y.removeCallbacks(this.t);
    }

    public void d(int i2) {
        this.g = i2;
    }

    public void e() {
        w wVar = this.e;
        if (wVar != null) {
            boolean unused = wVar.f74a = true;
            wVar.requestLayout();
        }
    }

    public void e(int i2) {
        Drawable background = this.c.getBackground();
        if (background != null) {
            background.getPadding(this.z);
            this.g = this.z.left + this.z.right + i2;
            return;
        }
        d(i2);
    }

    public void f(int i2) {
        this.c.setInputMethodMode(i2);
    }

    public boolean f() {
        return this.c.isShowing();
    }

    public void g(int i2) {
        w wVar = this.e;
        if (f() && wVar != null) {
            boolean unused = wVar.f74a = false;
            wVar.setSelection(i2);
            if (wVar.getChoiceMode() != 0) {
                wVar.setItemChecked(i2, true);
            }
        }
    }

    public boolean g() {
        return this.c.getInputMethodMode() == 2;
    }

    public ListView h() {
        return this.e;
    }
}
