package android.support.v7.widget;

import android.app.SearchManager;
import android.app.SearchableInfo;
import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.ResourceCursorAdapter;
import android.support.v7.b.g;
import android.view.View;
import java.util.WeakHashMap;

class e extends ResourceCursorAdapter implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private SearchManager f81a = ((SearchManager) this.mContext.getSystemService("search"));
    private SearchView b;
    private SearchableInfo c;
    private Context d;
    private WeakHashMap e;
    private boolean f = false;
    private int g = 1;
    private int h = -1;
    private int i = -1;
    private int j = -1;
    private int k = -1;
    private int l = -1;
    private int m = -1;

    public e(Context context, SearchView searchView, SearchableInfo searchableInfo, WeakHashMap weakHashMap) {
        super(context, g.abc_search_dropdown_item_icons_2line, (Cursor) null, true);
        this.b = searchView;
        this.c = searchableInfo;
        this.d = context;
        this.e = weakHashMap;
    }

    public void a(int i2) {
        this.g = i2;
    }

    public void onClick(View view) {
        Object tag = view.getTag();
        if (tag instanceof CharSequence) {
            this.b.a((CharSequence) tag);
        }
    }
}
