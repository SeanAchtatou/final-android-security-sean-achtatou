package android.support.v7.widget;

import android.app.SearchableInfo;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.widget.CursorAdapter;
import android.support.v7.b.b;
import android.support.v7.b.d;
import android.support.v7.c.a;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.ImageSpan;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import java.util.WeakHashMap;

public class SearchView extends LinearLayout implements a {

    /* renamed from: a  reason: collision with root package name */
    static final a f78a = new a();
    private Bundle A;
    private Runnable B;
    private Runnable C;
    private Runnable D;
    private final Intent E;
    private final Intent F;
    private final WeakHashMap G;
    private c b;
    private b c;
    private View.OnFocusChangeListener d;
    private d e;
    private View.OnClickListener f;
    private boolean g;
    private boolean h;
    private CursorAdapter i;
    private View j;
    private View k;
    private View l;
    private ImageView m;
    private View n;
    private View o;
    private SearchAutoComplete p;
    private ImageView q;
    private boolean r;
    private CharSequence s;
    private boolean t;
    private boolean u;
    private int v;
    private boolean w;
    private boolean x;
    private int y;
    private SearchableInfo z;

    public class SearchAutoComplete extends AutoCompleteTextView {

        /* renamed from: a  reason: collision with root package name */
        private int f79a = getThreshold();
        private SearchView b;

        public SearchAutoComplete(Context context) {
            super(context);
        }

        public SearchAutoComplete(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
        }

        public SearchAutoComplete(Context context, AttributeSet attributeSet, int i) {
            super(context, attributeSet, i);
        }

        public boolean enoughToFilter() {
            return this.f79a <= 0 || super.enoughToFilter();
        }

        /* access modifiers changed from: protected */
        public void onFocusChanged(boolean z, int i, Rect rect) {
            super.onFocusChanged(z, i, rect);
            this.b.d();
        }

        public boolean onKeyPreIme(int i, KeyEvent keyEvent) {
            if (i == 4) {
                if (keyEvent.getAction() == 0 && keyEvent.getRepeatCount() == 0) {
                    KeyEvent.DispatcherState keyDispatcherState = getKeyDispatcherState();
                    if (keyDispatcherState == null) {
                        return true;
                    }
                    keyDispatcherState.startTracking(keyEvent, this);
                    return true;
                } else if (keyEvent.getAction() == 1) {
                    KeyEvent.DispatcherState keyDispatcherState2 = getKeyDispatcherState();
                    if (keyDispatcherState2 != null) {
                        keyDispatcherState2.handleUpEvent(keyEvent);
                    }
                    if (keyEvent.isTracking() && !keyEvent.isCanceled()) {
                        this.b.clearFocus();
                        this.b.setImeVisibility(false);
                        return true;
                    }
                }
            }
            return super.onKeyPreIme(i, keyEvent);
        }

        public void onWindowFocusChanged(boolean z) {
            super.onWindowFocusChanged(z);
            if (z && this.b.hasFocus() && getVisibility() == 0) {
                ((InputMethodManager) getContext().getSystemService("input_method")).showSoftInput(this, 0);
                if (SearchView.a(getContext())) {
                    SearchView.f78a.a(this, true);
                }
            }
        }

        public void performCompletion() {
        }

        /* access modifiers changed from: protected */
        public void replaceText(CharSequence charSequence) {
        }

        /* access modifiers changed from: package-private */
        public void setSearchView(SearchView searchView) {
            this.b = searchView;
        }

        public void setThreshold(int i) {
            super.setThreshold(i);
            this.f79a = i;
        }
    }

    private void a(boolean z2) {
        boolean z3 = true;
        int i2 = 8;
        this.h = z2;
        int i3 = z2 ? 0 : 8;
        boolean z4 = !TextUtils.isEmpty(this.p.getText());
        this.j.setVisibility(i3);
        b(z4);
        this.n.setVisibility(z2 ? 8 : 0);
        ImageView imageView = this.q;
        if (!this.g) {
            i2 = 0;
        }
        imageView.setVisibility(i2);
        h();
        if (z4) {
            z3 = false;
        }
        c(z3);
        g();
    }

    static boolean a(Context context) {
        return context.getResources().getConfiguration().orientation == 2;
    }

    private CharSequence b(CharSequence charSequence) {
        if (!this.g) {
            return charSequence;
        }
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder("   ");
        spannableStringBuilder.append(charSequence);
        Drawable drawable = getContext().getResources().getDrawable(getSearchIconId());
        int textSize = (int) (((double) this.p.getTextSize()) * 1.25d);
        drawable.setBounds(0, 0, textSize, textSize);
        spannableStringBuilder.setSpan(new ImageSpan(drawable), 1, 2, 33);
        return spannableStringBuilder;
    }

    private void b(boolean z2) {
        int i2 = 8;
        if (this.r && f() && hasFocus() && (z2 || !this.w)) {
            i2 = 0;
        }
        this.k.setVisibility(i2);
    }

    private void c(boolean z2) {
        int i2;
        if (!this.w || c() || !z2) {
            i2 = 8;
        } else {
            i2 = 0;
            this.k.setVisibility(8);
        }
        this.o.setVisibility(i2);
    }

    private boolean e() {
        if (this.z == null || !this.z.getVoiceSearchEnabled()) {
            return false;
        }
        Intent intent = null;
        if (this.z.getVoiceSearchLaunchWebSearch()) {
            intent = this.E;
        } else if (this.z.getVoiceSearchLaunchRecognizer()) {
            intent = this.F;
        }
        return (intent == null || getContext().getPackageManager().resolveActivity(intent, 65536) == null) ? false : true;
    }

    private boolean f() {
        return (this.r || this.w) && !c();
    }

    private void g() {
        int i2 = 8;
        if (f() && (this.k.getVisibility() == 0 || this.o.getVisibility() == 0)) {
            i2 = 0;
        }
        this.l.setVisibility(i2);
    }

    private int getPreferredWidth() {
        return getContext().getResources().getDimensionPixelSize(d.abc_search_view_preferred_width);
    }

    private int getSearchIconId() {
        TypedValue typedValue = new TypedValue();
        getContext().getTheme().resolveAttribute(b.searchViewSearchIcon, typedValue, true);
        return typedValue.resourceId;
    }

    private void h() {
        boolean z2 = true;
        int i2 = 0;
        boolean z3 = !TextUtils.isEmpty(this.p.getText());
        if (!z3 && (!this.g || this.x)) {
            z2 = false;
        }
        ImageView imageView = this.m;
        if (!z2) {
            i2 = 8;
        }
        imageView.setVisibility(i2);
        this.m.getDrawable().setState(z3 ? ENABLED_STATE_SET : EMPTY_STATE_SET);
    }

    private void i() {
        post(this.C);
    }

    private void j() {
        if (this.s != null) {
            this.p.setHint(b(this.s));
        } else if (this.z != null) {
            String str = null;
            int hintId = this.z.getHintId();
            if (hintId != 0) {
                str = getContext().getString(hintId);
            }
            if (str != null) {
                this.p.setHint(b(str));
            }
        } else {
            this.p.setHint(b(""));
        }
    }

    private void k() {
        int i2 = 1;
        this.p.setThreshold(this.z.getSuggestThreshold());
        this.p.setImeOptions(this.z.getImeOptions());
        int inputType = this.z.getInputType();
        if ((inputType & 15) == 1) {
            inputType &= -65537;
            if (this.z.getSuggestAuthority() != null) {
                inputType = inputType | 65536 | 524288;
            }
        }
        this.p.setInputType(inputType);
        if (this.i != null) {
            this.i.changeCursor((Cursor) null);
        }
        if (this.z.getSuggestAuthority() != null) {
            this.i = new e(getContext(), this, this.z, this.G);
            this.p.setAdapter(this.i);
            e eVar = this.i;
            if (this.t) {
                i2 = 2;
            }
            eVar.a(i2);
        }
    }

    private void l() {
        if (!TextUtils.isEmpty(this.p.getText())) {
            this.p.setText("");
            this.p.requestFocus();
            setImeVisibility(true);
        } else if (!this.g) {
        } else {
            if (this.c == null || !this.c.a()) {
                clearFocus();
                a(true);
            }
        }
    }

    private void m() {
        a(false);
        this.p.requestFocus();
        setImeVisibility(true);
        if (this.f != null) {
            this.f.onClick(this);
        }
    }

    private void n() {
        f78a.a(this.p);
        f78a.b(this.p);
    }

    /* access modifiers changed from: private */
    public void setImeVisibility(boolean z2) {
        if (z2) {
            post(this.B);
            return;
        }
        removeCallbacks(this.B);
        InputMethodManager inputMethodManager = (InputMethodManager) getContext().getSystemService("input_method");
        if (inputMethodManager != null) {
            inputMethodManager.hideSoftInputFromWindow(getWindowToken(), 0);
        }
    }

    private void setQuery(CharSequence charSequence) {
        this.p.setText(charSequence);
        this.p.setSelection(TextUtils.isEmpty(charSequence) ? 0 : charSequence.length());
    }

    public void a() {
        if (!this.x) {
            this.x = true;
            this.y = this.p.getImeOptions();
            this.p.setImeOptions(this.y | 33554432);
            this.p.setText("");
            setIconified(false);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(CharSequence charSequence) {
        setQuery(charSequence);
    }

    public void b() {
        clearFocus();
        a(true);
        this.p.setImeOptions(this.y);
        this.x = false;
    }

    public boolean c() {
        return this.h;
    }

    public void clearFocus() {
        this.u = true;
        setImeVisibility(false);
        super.clearFocus();
        this.p.clearFocus();
        this.u = false;
    }

    /* access modifiers changed from: package-private */
    public void d() {
        a(c());
        i();
        if (this.p.hasFocus()) {
            n();
        }
    }

    public int getImeOptions() {
        return this.p.getImeOptions();
    }

    public int getInputType() {
        return this.p.getInputType();
    }

    public int getMaxWidth() {
        return this.v;
    }

    public CharSequence getQuery() {
        return this.p.getText();
    }

    public CharSequence getQueryHint() {
        int hintId;
        if (this.s != null) {
            return this.s;
        }
        if (this.z == null || (hintId = this.z.getHintId()) == 0) {
            return null;
        }
        return getContext().getString(hintId);
    }

    public CursorAdapter getSuggestionsAdapter() {
        return this.i;
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        removeCallbacks(this.C);
        post(this.D);
        super.onDetachedFromWindow();
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (this.z == null) {
            return false;
        }
        return super.onKeyDown(i2, keyEvent);
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        if (c()) {
            super.onMeasure(i2, i3);
            return;
        }
        int mode = View.MeasureSpec.getMode(i2);
        int size = View.MeasureSpec.getSize(i2);
        switch (mode) {
            case Integer.MIN_VALUE:
                if (this.v <= 0) {
                    size = Math.min(getPreferredWidth(), size);
                    break;
                } else {
                    size = Math.min(this.v, size);
                    break;
                }
            case 0:
                if (this.v <= 0) {
                    size = getPreferredWidth();
                    break;
                } else {
                    size = this.v;
                    break;
                }
            case 1073741824:
                if (this.v > 0) {
                    size = Math.min(this.v, size);
                    break;
                }
                break;
        }
        super.onMeasure(View.MeasureSpec.makeMeasureSpec(size, 1073741824), i3);
    }

    public void onWindowFocusChanged(boolean z2) {
        super.onWindowFocusChanged(z2);
        i();
    }

    public boolean requestFocus(int i2, Rect rect) {
        if (this.u || !isFocusable()) {
            return false;
        }
        if (c()) {
            return super.requestFocus(i2, rect);
        }
        boolean requestFocus = this.p.requestFocus(i2, rect);
        if (requestFocus) {
            a(false);
        }
        return requestFocus;
    }

    public void setAppSearchData(Bundle bundle) {
        this.A = bundle;
    }

    public void setIconified(boolean z2) {
        if (z2) {
            l();
        } else {
            m();
        }
    }

    public void setIconifiedByDefault(boolean z2) {
        if (this.g != z2) {
            this.g = z2;
            a(z2);
            j();
        }
    }

    public void setImeOptions(int i2) {
        this.p.setImeOptions(i2);
    }

    public void setInputType(int i2) {
        this.p.setInputType(i2);
    }

    public void setMaxWidth(int i2) {
        this.v = i2;
        requestLayout();
    }

    public void setOnCloseListener(b bVar) {
        this.c = bVar;
    }

    public void setOnQueryTextFocusChangeListener(View.OnFocusChangeListener onFocusChangeListener) {
        this.d = onFocusChangeListener;
    }

    public void setOnQueryTextListener(c cVar) {
        this.b = cVar;
    }

    public void setOnSearchClickListener(View.OnClickListener onClickListener) {
        this.f = onClickListener;
    }

    public void setOnSuggestionListener(d dVar) {
        this.e = dVar;
    }

    public void setQueryHint(CharSequence charSequence) {
        this.s = charSequence;
        j();
    }

    public void setQueryRefinementEnabled(boolean z2) {
        this.t = z2;
        if (this.i instanceof e) {
            this.i.a(z2 ? 2 : 1);
        }
    }

    public void setSearchableInfo(SearchableInfo searchableInfo) {
        this.z = searchableInfo;
        if (this.z != null) {
            k();
            j();
        }
        this.w = e();
        if (this.w) {
            this.p.setPrivateImeOptions("nm");
        }
        a(c());
    }

    public void setSubmitButtonEnabled(boolean z2) {
        this.r = z2;
        a(c());
    }

    public void setSuggestionsAdapter(CursorAdapter cursorAdapter) {
        this.i = cursorAdapter;
        this.p.setAdapter(this.i);
    }
}
