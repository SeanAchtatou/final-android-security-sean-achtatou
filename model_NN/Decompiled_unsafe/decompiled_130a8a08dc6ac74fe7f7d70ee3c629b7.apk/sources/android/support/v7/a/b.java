package android.support.v7.a;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v7.b.j;
import android.util.AttributeSet;
import android.view.ViewGroup;

public class b extends ViewGroup.MarginLayoutParams {

    /* renamed from: a  reason: collision with root package name */
    public int f14a;

    public b(int i) {
        this(-2, -1, i);
    }

    public b(int i, int i2, int i3) {
        super(i, i2);
        this.f14a = -1;
        this.f14a = i3;
    }

    public b(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.f14a = -1;
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, j.ActionBarLayout);
        this.f14a = obtainStyledAttributes.getInt(0, -1);
        obtainStyledAttributes.recycle();
    }
}
