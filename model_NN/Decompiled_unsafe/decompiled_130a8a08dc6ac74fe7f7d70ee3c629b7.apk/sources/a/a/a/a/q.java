package a.a.a.a;

import java.util.regex.Pattern;

public class q {

    /* renamed from: a  reason: collision with root package name */
    private r f11a;

    public q(int i) {
        this.f11a = new r(i);
    }

    public Pattern a(String str) {
        Pattern pattern = (Pattern) this.f11a.a(str);
        if (pattern != null) {
            return pattern;
        }
        Pattern compile = Pattern.compile(str);
        this.f11a.a(str, compile);
        return compile;
    }
}
