package a.a.a.a;

import java.io.Externalizable;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.util.ArrayList;
import java.util.List;

public class l implements Externalizable {

    /* renamed from: a  reason: collision with root package name */
    private List f7a = new ArrayList();

    public List a() {
        return this.f7a;
    }

    public int b() {
        return this.f7a.size();
    }

    public void readExternal(ObjectInput objectInput) {
        int readInt = objectInput.readInt();
        for (int i = 0; i < readInt; i++) {
            k kVar = new k();
            kVar.readExternal(objectInput);
            this.f7a.add(kVar);
        }
    }

    public void writeExternal(ObjectOutput objectOutput) {
        int b = b();
        objectOutput.writeInt(b);
        for (int i = 0; i < b; i++) {
            ((k) this.f7a.get(i)).writeExternal(objectOutput);
        }
    }
}
