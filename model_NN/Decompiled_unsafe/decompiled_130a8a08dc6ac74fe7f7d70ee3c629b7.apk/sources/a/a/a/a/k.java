package a.a.a.a;

import java.io.Externalizable;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.util.ArrayList;
import java.util.List;

public class k implements Externalizable {
    private boolean A;
    private m B = null;
    private boolean C;
    private m D = null;
    private boolean E;
    private m F = null;
    private boolean G;
    private String H = "";
    private boolean I;
    private int J = 0;
    private boolean K;
    private String L = "";
    private boolean M;
    private String N = "";
    private boolean O;
    private String P = "";
    private boolean Q;
    private String R = "";
    private boolean S;
    private String T = "";
    private boolean U;
    private String V = "";
    private boolean W;
    private boolean X = false;
    private List Y = new ArrayList();
    private List Z = new ArrayList();

    /* renamed from: a  reason: collision with root package name */
    private boolean f6a;
    private boolean aa;
    private boolean ab = false;
    private boolean ac;
    private String ad = "";
    private boolean ae;
    private boolean af = false;
    private boolean ag;
    private boolean ah = false;
    private m b = null;
    private boolean c;
    private m d = null;
    private boolean e;
    private m f = null;
    private boolean g;
    private m h = null;
    private boolean i;
    private m j = null;
    private boolean k;
    private m l = null;
    private boolean m;
    private m n = null;
    private boolean o;
    private m p = null;
    private boolean q;
    private m r = null;
    private boolean s;
    private m t = null;
    private boolean u;
    private m v = null;
    private boolean w;
    private m x = null;
    private boolean y;
    private m z = null;

    public k a(int i2) {
        this.I = true;
        this.J = i2;
        return this;
    }

    public k a(m mVar) {
        if (mVar == null) {
            throw new NullPointerException();
        }
        this.f6a = true;
        this.b = mVar;
        return this;
    }

    public k a(String str) {
        this.G = true;
        this.H = str;
        return this;
    }

    public k a(boolean z2) {
        this.W = true;
        this.X = z2;
        return this;
    }

    public m a() {
        return this.b;
    }

    public k b(m mVar) {
        if (mVar == null) {
            throw new NullPointerException();
        }
        this.c = true;
        this.d = mVar;
        return this;
    }

    public k b(String str) {
        this.K = true;
        this.L = str;
        return this;
    }

    public k b(boolean z2) {
        this.aa = true;
        this.ab = z2;
        return this;
    }

    public m b() {
        return this.d;
    }

    public k c(m mVar) {
        if (mVar == null) {
            throw new NullPointerException();
        }
        this.e = true;
        this.f = mVar;
        return this;
    }

    public k c(String str) {
        this.M = true;
        this.N = str;
        return this;
    }

    public k c(boolean z2) {
        this.ae = true;
        this.af = z2;
        return this;
    }

    public m c() {
        return this.f;
    }

    public k d(m mVar) {
        if (mVar == null) {
            throw new NullPointerException();
        }
        this.g = true;
        this.h = mVar;
        return this;
    }

    public k d(String str) {
        this.O = true;
        this.P = str;
        return this;
    }

    public k d(boolean z2) {
        this.ag = true;
        this.ah = z2;
        return this;
    }

    public m d() {
        return this.h;
    }

    public k e(m mVar) {
        if (mVar == null) {
            throw new NullPointerException();
        }
        this.i = true;
        this.j = mVar;
        return this;
    }

    public k e(String str) {
        this.Q = true;
        this.R = str;
        return this;
    }

    public m e() {
        return this.j;
    }

    public k f(m mVar) {
        if (mVar == null) {
            throw new NullPointerException();
        }
        this.k = true;
        this.l = mVar;
        return this;
    }

    public k f(String str) {
        this.S = true;
        this.T = str;
        return this;
    }

    public m f() {
        return this.l;
    }

    public k g(m mVar) {
        if (mVar == null) {
            throw new NullPointerException();
        }
        this.m = true;
        this.n = mVar;
        return this;
    }

    public k g(String str) {
        this.U = true;
        this.V = str;
        return this;
    }

    public m g() {
        return this.n;
    }

    public k h(m mVar) {
        if (mVar == null) {
            throw new NullPointerException();
        }
        this.o = true;
        this.p = mVar;
        return this;
    }

    public k h(String str) {
        this.ac = true;
        this.ad = str;
        return this;
    }

    public m h() {
        return this.p;
    }

    public k i(m mVar) {
        if (mVar == null) {
            throw new NullPointerException();
        }
        this.q = true;
        this.r = mVar;
        return this;
    }

    public m i() {
        return this.r;
    }

    public k j(m mVar) {
        if (mVar == null) {
            throw new NullPointerException();
        }
        this.s = true;
        this.t = mVar;
        return this;
    }

    public m j() {
        return this.t;
    }

    public k k(m mVar) {
        if (mVar == null) {
            throw new NullPointerException();
        }
        this.u = true;
        this.v = mVar;
        return this;
    }

    public m k() {
        return this.x;
    }

    public int l() {
        return this.J;
    }

    public k l(m mVar) {
        if (mVar == null) {
            throw new NullPointerException();
        }
        this.w = true;
        this.x = mVar;
        return this;
    }

    public k m(m mVar) {
        if (mVar == null) {
            throw new NullPointerException();
        }
        this.y = true;
        this.z = mVar;
        return this;
    }

    public String m() {
        return this.L;
    }

    public k n(m mVar) {
        if (mVar == null) {
            throw new NullPointerException();
        }
        this.A = true;
        this.B = mVar;
        return this;
    }

    public String n() {
        return this.T;
    }

    public k o(m mVar) {
        if (mVar == null) {
            throw new NullPointerException();
        }
        this.C = true;
        this.D = mVar;
        return this;
    }

    public String o() {
        return this.V;
    }

    public k p(m mVar) {
        if (mVar == null) {
            throw new NullPointerException();
        }
        this.E = true;
        this.F = mVar;
        return this;
    }

    public boolean p() {
        return this.X;
    }

    public int q() {
        return this.Y.size();
    }

    public int r() {
        return this.Z.size();
    }

    public void readExternal(ObjectInput objectInput) {
        if (objectInput.readBoolean()) {
            m mVar = new m();
            mVar.readExternal(objectInput);
            a(mVar);
        }
        if (objectInput.readBoolean()) {
            m mVar2 = new m();
            mVar2.readExternal(objectInput);
            b(mVar2);
        }
        if (objectInput.readBoolean()) {
            m mVar3 = new m();
            mVar3.readExternal(objectInput);
            c(mVar3);
        }
        if (objectInput.readBoolean()) {
            m mVar4 = new m();
            mVar4.readExternal(objectInput);
            d(mVar4);
        }
        if (objectInput.readBoolean()) {
            m mVar5 = new m();
            mVar5.readExternal(objectInput);
            e(mVar5);
        }
        if (objectInput.readBoolean()) {
            m mVar6 = new m();
            mVar6.readExternal(objectInput);
            f(mVar6);
        }
        if (objectInput.readBoolean()) {
            m mVar7 = new m();
            mVar7.readExternal(objectInput);
            g(mVar7);
        }
        if (objectInput.readBoolean()) {
            m mVar8 = new m();
            mVar8.readExternal(objectInput);
            h(mVar8);
        }
        if (objectInput.readBoolean()) {
            m mVar9 = new m();
            mVar9.readExternal(objectInput);
            i(mVar9);
        }
        if (objectInput.readBoolean()) {
            m mVar10 = new m();
            mVar10.readExternal(objectInput);
            j(mVar10);
        }
        if (objectInput.readBoolean()) {
            m mVar11 = new m();
            mVar11.readExternal(objectInput);
            k(mVar11);
        }
        if (objectInput.readBoolean()) {
            m mVar12 = new m();
            mVar12.readExternal(objectInput);
            l(mVar12);
        }
        if (objectInput.readBoolean()) {
            m mVar13 = new m();
            mVar13.readExternal(objectInput);
            m(mVar13);
        }
        if (objectInput.readBoolean()) {
            m mVar14 = new m();
            mVar14.readExternal(objectInput);
            n(mVar14);
        }
        if (objectInput.readBoolean()) {
            m mVar15 = new m();
            mVar15.readExternal(objectInput);
            o(mVar15);
        }
        if (objectInput.readBoolean()) {
            m mVar16 = new m();
            mVar16.readExternal(objectInput);
            p(mVar16);
        }
        a(objectInput.readUTF());
        a(objectInput.readInt());
        b(objectInput.readUTF());
        if (objectInput.readBoolean()) {
            c(objectInput.readUTF());
        }
        if (objectInput.readBoolean()) {
            d(objectInput.readUTF());
        }
        if (objectInput.readBoolean()) {
            e(objectInput.readUTF());
        }
        if (objectInput.readBoolean()) {
            f(objectInput.readUTF());
        }
        if (objectInput.readBoolean()) {
            g(objectInput.readUTF());
        }
        a(objectInput.readBoolean());
        int readInt = objectInput.readInt();
        for (int i2 = 0; i2 < readInt; i2++) {
            j jVar = new j();
            jVar.readExternal(objectInput);
            this.Y.add(jVar);
        }
        int readInt2 = objectInput.readInt();
        for (int i3 = 0; i3 < readInt2; i3++) {
            j jVar2 = new j();
            jVar2.readExternal(objectInput);
            this.Z.add(jVar2);
        }
        b(objectInput.readBoolean());
        if (objectInput.readBoolean()) {
            h(objectInput.readUTF());
        }
        c(objectInput.readBoolean());
        d(objectInput.readBoolean());
    }

    public boolean s() {
        return this.ac;
    }

    public String t() {
        return this.ad;
    }

    public void writeExternal(ObjectOutput objectOutput) {
        objectOutput.writeBoolean(this.f6a);
        if (this.f6a) {
            this.b.writeExternal(objectOutput);
        }
        objectOutput.writeBoolean(this.c);
        if (this.c) {
            this.d.writeExternal(objectOutput);
        }
        objectOutput.writeBoolean(this.e);
        if (this.e) {
            this.f.writeExternal(objectOutput);
        }
        objectOutput.writeBoolean(this.g);
        if (this.g) {
            this.h.writeExternal(objectOutput);
        }
        objectOutput.writeBoolean(this.i);
        if (this.i) {
            this.j.writeExternal(objectOutput);
        }
        objectOutput.writeBoolean(this.k);
        if (this.k) {
            this.l.writeExternal(objectOutput);
        }
        objectOutput.writeBoolean(this.m);
        if (this.m) {
            this.n.writeExternal(objectOutput);
        }
        objectOutput.writeBoolean(this.o);
        if (this.o) {
            this.p.writeExternal(objectOutput);
        }
        objectOutput.writeBoolean(this.q);
        if (this.q) {
            this.r.writeExternal(objectOutput);
        }
        objectOutput.writeBoolean(this.s);
        if (this.s) {
            this.t.writeExternal(objectOutput);
        }
        objectOutput.writeBoolean(this.u);
        if (this.u) {
            this.v.writeExternal(objectOutput);
        }
        objectOutput.writeBoolean(this.w);
        if (this.w) {
            this.x.writeExternal(objectOutput);
        }
        objectOutput.writeBoolean(this.y);
        if (this.y) {
            this.z.writeExternal(objectOutput);
        }
        objectOutput.writeBoolean(this.A);
        if (this.A) {
            this.B.writeExternal(objectOutput);
        }
        objectOutput.writeBoolean(this.C);
        if (this.C) {
            this.D.writeExternal(objectOutput);
        }
        objectOutput.writeBoolean(this.E);
        if (this.E) {
            this.F.writeExternal(objectOutput);
        }
        objectOutput.writeUTF(this.H);
        objectOutput.writeInt(this.J);
        objectOutput.writeUTF(this.L);
        objectOutput.writeBoolean(this.M);
        if (this.M) {
            objectOutput.writeUTF(this.N);
        }
        objectOutput.writeBoolean(this.O);
        if (this.O) {
            objectOutput.writeUTF(this.P);
        }
        objectOutput.writeBoolean(this.Q);
        if (this.Q) {
            objectOutput.writeUTF(this.R);
        }
        objectOutput.writeBoolean(this.S);
        if (this.S) {
            objectOutput.writeUTF(this.T);
        }
        objectOutput.writeBoolean(this.U);
        if (this.U) {
            objectOutput.writeUTF(this.V);
        }
        objectOutput.writeBoolean(this.X);
        int q2 = q();
        objectOutput.writeInt(q2);
        for (int i2 = 0; i2 < q2; i2++) {
            ((j) this.Y.get(i2)).writeExternal(objectOutput);
        }
        int r2 = r();
        objectOutput.writeInt(r2);
        for (int i3 = 0; i3 < r2; i3++) {
            ((j) this.Z.get(i3)).writeExternal(objectOutput);
        }
        objectOutput.writeBoolean(this.ab);
        objectOutput.writeBoolean(this.ac);
        if (this.ac) {
            objectOutput.writeUTF(this.ad);
        }
        objectOutput.writeBoolean(this.af);
        objectOutput.writeBoolean(this.ah);
    }
}
