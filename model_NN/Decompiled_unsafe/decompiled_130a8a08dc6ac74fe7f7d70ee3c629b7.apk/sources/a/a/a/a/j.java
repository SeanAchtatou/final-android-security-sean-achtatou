package a.a.a.a;

import java.io.Externalizable;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.util.ArrayList;
import java.util.List;

public class j implements Externalizable {

    /* renamed from: a  reason: collision with root package name */
    private boolean f5a;
    private String b = "";
    private boolean c;
    private String d = "";
    private List e = new ArrayList();
    private boolean f;
    private String g = "";
    private boolean h;
    private boolean i = false;
    private boolean j;
    private String k = "";

    public int a() {
        return this.e.size();
    }

    public j a(String str) {
        this.f5a = true;
        this.b = str;
        return this;
    }

    public j a(boolean z) {
        this.h = true;
        this.i = z;
        return this;
    }

    public j b(String str) {
        this.c = true;
        this.d = str;
        return this;
    }

    public j c(String str) {
        this.f = true;
        this.g = str;
        return this;
    }

    public j d(String str) {
        this.j = true;
        this.k = str;
        return this;
    }

    public void readExternal(ObjectInput objectInput) {
        a(objectInput.readUTF());
        b(objectInput.readUTF());
        int readInt = objectInput.readInt();
        for (int i2 = 0; i2 < readInt; i2++) {
            this.e.add(objectInput.readUTF());
        }
        if (objectInput.readBoolean()) {
            c(objectInput.readUTF());
        }
        if (objectInput.readBoolean()) {
            d(objectInput.readUTF());
        }
        a(objectInput.readBoolean());
    }

    public void writeExternal(ObjectOutput objectOutput) {
        objectOutput.writeUTF(this.b);
        objectOutput.writeUTF(this.d);
        int a2 = a();
        objectOutput.writeInt(a2);
        for (int i2 = 0; i2 < a2; i2++) {
            objectOutput.writeUTF((String) this.e.get(i2));
        }
        objectOutput.writeBoolean(this.f);
        if (this.f) {
            objectOutput.writeUTF(this.g);
        }
        objectOutput.writeBoolean(this.j);
        if (this.j) {
            objectOutput.writeUTF(this.k);
        }
        objectOutput.writeBoolean(this.i);
    }
}
