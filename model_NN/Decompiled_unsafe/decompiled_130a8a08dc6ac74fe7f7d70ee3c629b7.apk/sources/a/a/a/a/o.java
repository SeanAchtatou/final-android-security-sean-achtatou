package a.a.a.a;

import java.io.Serializable;

public class o implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    private boolean f9a;
    private int b = 0;
    private boolean c;
    private long d = 0;
    private boolean e;
    private String f = "";
    private boolean g;
    private boolean h = false;
    private boolean i;
    private int j = 1;
    private boolean k;
    private String l = "";
    private boolean m;
    private p n = p.FROM_NUMBER_WITH_PLUS_SIGN;
    private boolean o;
    private String p = "";

    public int a() {
        return this.b;
    }

    public o a(int i2) {
        this.f9a = true;
        this.b = i2;
        return this;
    }

    public o a(long j2) {
        this.c = true;
        this.d = j2;
        return this;
    }

    public o a(p pVar) {
        if (pVar == null) {
            throw new NullPointerException();
        }
        this.m = true;
        this.n = pVar;
        return this;
    }

    public o a(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.e = true;
        this.f = str;
        return this;
    }

    public o a(boolean z) {
        this.g = true;
        this.h = z;
        return this;
    }

    public boolean a(o oVar) {
        if (oVar == null) {
            return false;
        }
        if (this != oVar) {
            return this.b == oVar.b && this.d == oVar.d && this.f.equals(oVar.f) && this.h == oVar.h && this.j == oVar.j && this.l.equals(oVar.l) && this.n == oVar.n && this.p.equals(oVar.p) && m() == oVar.m();
        }
        return true;
    }

    public long b() {
        return this.d;
    }

    public o b(int i2) {
        this.i = true;
        this.j = i2;
        return this;
    }

    public o b(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.k = true;
        this.l = str;
        return this;
    }

    public o c(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.o = true;
        this.p = str;
        return this;
    }

    public boolean c() {
        return this.e;
    }

    public String d() {
        return this.f;
    }

    public boolean e() {
        return this.g;
    }

    public boolean equals(Object obj) {
        return (obj instanceof o) && a((o) obj);
    }

    public boolean f() {
        return this.h;
    }

    public boolean g() {
        return this.i;
    }

    public int h() {
        return this.j;
    }

    public int hashCode() {
        int i2 = 1231;
        int a2 = ((((((((((f() ? 1231 : 1237) + ((((((a() + 2173) * 53) + Long.valueOf(b()).hashCode()) * 53) + d().hashCode()) * 53)) * 53) + h()) * 53) + i().hashCode()) * 53) + k().hashCode()) * 53) + n().hashCode()) * 53;
        if (!m()) {
            i2 = 1237;
        }
        return a2 + i2;
    }

    public String i() {
        return this.l;
    }

    public boolean j() {
        return this.m;
    }

    public p k() {
        return this.n;
    }

    public o l() {
        this.m = false;
        this.n = p.FROM_NUMBER_WITH_PLUS_SIGN;
        return this;
    }

    public boolean m() {
        return this.o;
    }

    public String n() {
        return this.p;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Country Code: ").append(this.b);
        sb.append(" National Number: ").append(this.d);
        if (e() && f()) {
            sb.append(" Leading Zero(s): true");
        }
        if (g()) {
            sb.append(" Number of leading zeros: ").append(this.j);
        }
        if (c()) {
            sb.append(" Extension: ").append(this.f);
        }
        if (j()) {
            sb.append(" Country Code Source: ").append(this.n);
        }
        if (m()) {
            sb.append(" Preferred Domestic Carrier Code: ").append(this.p);
        }
        return sb.toString();
    }
}
