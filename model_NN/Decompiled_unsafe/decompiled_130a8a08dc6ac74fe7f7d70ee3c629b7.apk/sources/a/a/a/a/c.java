package a.a.a.a;

public class c extends Exception {

    /* renamed from: a  reason: collision with root package name */
    private d f0a;
    private String b;

    public c(d dVar, String str) {
        super(str);
        this.b = str;
        this.f0a = dVar;
    }

    public d a() {
        return this.f0a;
    }

    public String toString() {
        String valueOf = String.valueOf(String.valueOf(this.f0a));
        String valueOf2 = String.valueOf(String.valueOf(this.b));
        return new StringBuilder(valueOf.length() + 14 + valueOf2.length()).append("Error type: ").append(valueOf).append(". ").append(valueOf2).toString();
    }
}
