package a.a.a.a;

import java.util.LinkedHashMap;

class r {

    /* renamed from: a  reason: collision with root package name */
    private LinkedHashMap f12a;
    /* access modifiers changed from: private */
    public int b;

    public r(int i) {
        this.b = i;
        this.f12a = new s(this, ((i * 4) / 3) + 1, 0.75f, true);
    }

    public synchronized Object a(Object obj) {
        return this.f12a.get(obj);
    }

    public synchronized void a(Object obj, Object obj2) {
        this.f12a.put(obj, obj2);
    }
}
