package a.a.a.a;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class e {
    private static final Pattern A = Pattern.compile("\\(?\\$1\\)?");
    private static e B = null;

    /* renamed from: a  reason: collision with root package name */
    static final b f2a = new f();
    static final Pattern b = Pattern.compile("[+＋]+");
    static final Pattern c = Pattern.compile("[\\\\/] *x");
    static final Pattern d = Pattern.compile("[[\\P{N}&&\\P{L}]&&[^#]]+$");
    static final String e = f("xｘ#＃~～");
    static final Pattern f = Pattern.compile("(\\D+)");
    private static final Logger g = Logger.getLogger(e.class.getName());
    private static final Map h;
    private static final Map i;
    private static final Map j;
    private static final Map k;
    private static final Map l;
    private static final Pattern m = Pattern.compile("[\\d]+(?:[~⁓∼～][\\d]+)?");
    private static final String n;
    private static final Pattern o = Pattern.compile("[-x‐-―−ー－-／  ­​⁠　()（）［］.\\[\\]/~⁓∼～]+");
    private static final Pattern p = Pattern.compile("(\\p{Nd})");
    private static final Pattern q = Pattern.compile("[+＋\\p{Nd}]");
    private static final Pattern r = Pattern.compile("(?:.*?[A-Za-z]){3}.*");
    private static final String s;
    private static final String t;
    private static final Pattern u;
    private static final Pattern v;
    private static final Pattern w = Pattern.compile("(\\$\\d)");
    private static final Pattern x = Pattern.compile("\\$NP");
    private static final Pattern y = Pattern.compile("\\$FG");
    private static final Pattern z = Pattern.compile("\\$CC");
    private final Map C;
    private final Set D = new HashSet(35);
    private final Map E = Collections.synchronizedMap(new HashMap());
    private final Map F = Collections.synchronizedMap(new HashMap());
    private final q G = new q(100);
    private final Set H = new HashSet(320);
    private final Set I = new HashSet();
    private final String J;
    private final b K;

    static {
        HashMap hashMap = new HashMap();
        hashMap.put(52, "1");
        hashMap.put(54, "9");
        h = Collections.unmodifiableMap(hashMap);
        HashMap hashMap2 = new HashMap();
        hashMap2.put('0', '0');
        hashMap2.put('1', '1');
        hashMap2.put('2', '2');
        hashMap2.put('3', '3');
        hashMap2.put('4', '4');
        hashMap2.put('5', '5');
        hashMap2.put('6', '6');
        hashMap2.put('7', '7');
        hashMap2.put('8', '8');
        hashMap2.put('9', '9');
        HashMap hashMap3 = new HashMap(40);
        hashMap3.put('A', '2');
        hashMap3.put('B', '2');
        hashMap3.put('C', '2');
        hashMap3.put('D', '3');
        hashMap3.put('E', '3');
        hashMap3.put('F', '3');
        hashMap3.put('G', '4');
        hashMap3.put('H', '4');
        hashMap3.put('I', '4');
        hashMap3.put('J', '5');
        hashMap3.put('K', '5');
        hashMap3.put('L', '5');
        hashMap3.put('M', '6');
        hashMap3.put('N', '6');
        hashMap3.put('O', '6');
        hashMap3.put('P', '7');
        hashMap3.put('Q', '7');
        hashMap3.put('R', '7');
        hashMap3.put('S', '7');
        hashMap3.put('T', '8');
        hashMap3.put('U', '8');
        hashMap3.put('V', '8');
        hashMap3.put('W', '9');
        hashMap3.put('X', '9');
        hashMap3.put('Y', '9');
        hashMap3.put('Z', '9');
        j = Collections.unmodifiableMap(hashMap3);
        HashMap hashMap4 = new HashMap(100);
        hashMap4.putAll(j);
        hashMap4.putAll(hashMap2);
        k = Collections.unmodifiableMap(hashMap4);
        HashMap hashMap5 = new HashMap();
        hashMap5.putAll(hashMap2);
        hashMap5.put('+', '+');
        hashMap5.put('*', '*');
        i = Collections.unmodifiableMap(hashMap5);
        HashMap hashMap6 = new HashMap();
        for (Character charValue : j.keySet()) {
            char charValue2 = charValue.charValue();
            hashMap6.put(Character.valueOf(Character.toLowerCase(charValue2)), Character.valueOf(charValue2));
            hashMap6.put(Character.valueOf(charValue2), Character.valueOf(charValue2));
        }
        hashMap6.putAll(hashMap2);
        hashMap6.put('-', '-');
        hashMap6.put(65293, '-');
        hashMap6.put(8208, '-');
        hashMap6.put(8209, '-');
        hashMap6.put(8210, '-');
        hashMap6.put(8211, '-');
        hashMap6.put(8212, '-');
        hashMap6.put(8213, '-');
        hashMap6.put(8722, '-');
        hashMap6.put('/', '/');
        hashMap6.put(65295, '/');
        hashMap6.put(' ', ' ');
        hashMap6.put(12288, ' ');
        hashMap6.put(8288, ' ');
        hashMap6.put('.', '.');
        hashMap6.put(65294, '.');
        l = Collections.unmodifiableMap(hashMap6);
        String valueOf = String.valueOf(Arrays.toString(j.keySet().toArray()).replaceAll("[, \\[\\]]", ""));
        String valueOf2 = String.valueOf(Arrays.toString(j.keySet().toArray()).toLowerCase().replaceAll("[, \\[\\]]", ""));
        n = valueOf2.length() != 0 ? valueOf.concat(valueOf2) : new String(valueOf);
        String valueOf3 = String.valueOf(String.valueOf("\\p{Nd}{2}|[+＋]*+(?:[-x‐-―−ー－-／  ­​⁠　()（）［］.\\[\\]/~⁓∼～*]*\\p{Nd}){3,}[-x‐-―−ー－-／  ­​⁠　()（）［］.\\[\\]/~⁓∼～*"));
        String valueOf4 = String.valueOf(String.valueOf(n));
        String valueOf5 = String.valueOf(String.valueOf("\\p{Nd}"));
        s = new StringBuilder(valueOf3.length() + 2 + valueOf4.length() + valueOf5.length()).append(valueOf3).append(valueOf4).append(valueOf5).append("]*").toString();
        String valueOf6 = String.valueOf("xｘ#＃~～");
        t = f(valueOf6.length() != 0 ? ",".concat(valueOf6) : new String(","));
        String valueOf7 = String.valueOf(String.valueOf(t));
        u = Pattern.compile(new StringBuilder(valueOf7.length() + 5).append("(?:").append(valueOf7).append(")$").toString(), 66);
        String valueOf8 = String.valueOf(String.valueOf(s));
        String valueOf9 = String.valueOf(String.valueOf(t));
        v = Pattern.compile(new StringBuilder(valueOf8.length() + 5 + valueOf9.length()).append(valueOf8).append("(?:").append(valueOf9).append(")?").toString(), 66);
    }

    e(String str, b bVar, Map map) {
        this.J = str;
        this.K = bVar;
        this.C = map;
        for (Map.Entry entry : map.entrySet()) {
            List list = (List) entry.getValue();
            if (list.size() != 1 || !"001".equals(list.get(0))) {
                this.H.addAll(list);
            } else {
                this.I.add(entry.getKey());
            }
        }
        if (this.H.remove("001")) {
            g.log(Level.WARNING, "invalid metadata (country calling code was mapped to the non-geo entity as well as specific region(s))");
        }
        this.D.addAll((Collection) map.get(1));
    }

    public static synchronized e a() {
        e eVar;
        synchronized (e.class) {
            if (B == null) {
                a(a(f2a));
            }
            eVar = B;
        }
        return eVar;
    }

    public static e a(b bVar) {
        if (bVar != null) {
            return new e("/com/google/i18n/phonenumbers/data/PhoneNumberMetadataProto", bVar, a.a());
        }
        throw new IllegalArgumentException("metadataLoader could not be null.");
    }

    private g a(String str, k kVar) {
        m a2 = kVar.a();
        return (!a2.a() || !b(str, a2)) ? g.UNKNOWN : b(str, kVar.e()) ? g.PREMIUM_RATE : b(str, kVar.d()) ? g.TOLL_FREE : b(str, kVar.f()) ? g.SHARED_COST : b(str, kVar.h()) ? g.VOIP : b(str, kVar.g()) ? g.PERSONAL_NUMBER : b(str, kVar.i()) ? g.PAGER : b(str, kVar.j()) ? g.UAN : b(str, kVar.k()) ? g.VOICEMAIL : b(str, kVar.b()) ? kVar.p() ? g.FIXED_LINE_OR_MOBILE : b(str, kVar.c()) ? g.FIXED_LINE_OR_MOBILE : g.FIXED_LINE : (kVar.p() || !b(str, kVar.c())) ? g.UNKNOWN : g.MOBILE;
    }

    private h a(Pattern pattern, String str) {
        Matcher matcher = pattern.matcher(str);
        return matcher.matches() ? h.IS_POSSIBLE : matcher.lookingAt() ? h.TOO_LONG : h.TOO_SHORT;
    }

    private k a(int i2, String str) {
        return "001".equals(str) ? a(i2) : e(str);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
     arg types: [java.util.logging.Level, java.lang.String, java.io.IOException]
     candidates:
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
    private static l a(ObjectInputStream objectInputStream) {
        l lVar = new l();
        try {
            lVar.readExternal(objectInputStream);
            try {
                objectInputStream.close();
            } catch (IOException e2) {
                g.log(Level.WARNING, "error closing input stream (ignored)", (Throwable) e2);
            } catch (Throwable th) {
            }
        } catch (IOException e3) {
            g.log(Level.WARNING, "error reading input (ignored)", (Throwable) e3);
            try {
                objectInputStream.close();
            } catch (IOException e4) {
                g.log(Level.WARNING, "error closing input stream (ignored)", (Throwable) e4);
            } catch (Throwable th2) {
            }
        } catch (Throwable th3) {
            try {
                objectInputStream.close();
            } catch (IOException e5) {
                g.log(Level.WARNING, "error closing input stream (ignored)", (Throwable) e5);
            } catch (Throwable th4) {
            }
        }
        return lVar;
    }

    private String a(o oVar, List list) {
        String a2 = a(oVar);
        Iterator it = list.iterator();
        while (it.hasNext()) {
            String str = (String) it.next();
            k e2 = e(str);
            if (e2.s()) {
                if (this.G.a(e2.t()).matcher(a2).lookingAt()) {
                    return str;
                }
            } else if (a(a2, e2) != g.UNKNOWN) {
                return str;
            }
        }
        return null;
    }

    static String a(String str) {
        Matcher matcher = q.matcher(str);
        if (!matcher.find()) {
            return "";
        }
        String substring = str.substring(matcher.start());
        Matcher matcher2 = d.matcher(substring);
        if (matcher2.find()) {
            String substring2 = substring.substring(0, matcher2.start());
            Logger logger = g;
            Level level = Level.FINER;
            String valueOf = String.valueOf(substring2);
            logger.log(level, valueOf.length() != 0 ? "Stripped trailing characters: ".concat(valueOf) : new String("Stripped trailing characters: "));
            substring = substring2;
        }
        Matcher matcher3 = c.matcher(substring);
        return matcher3.find() ? substring.substring(0, matcher3.start()) : substring;
    }

    private static String a(String str, Map map, boolean z2) {
        StringBuilder sb = new StringBuilder(str.length());
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 >= str.length()) {
                return sb.toString();
            }
            char charAt = str.charAt(i3);
            Character ch = (Character) map.get(Character.valueOf(Character.toUpperCase(charAt)));
            if (ch != null) {
                sb.append(ch);
            } else if (!z2) {
                sb.append(charAt);
            }
            i2 = i3 + 1;
        }
    }

    static StringBuilder a(String str, boolean z2) {
        StringBuilder sb = new StringBuilder(str.length());
        for (char c2 : str.toCharArray()) {
            int digit = Character.digit(c2, 10);
            if (digit != -1) {
                sb.append(digit);
            } else if (z2) {
                sb.append(c2);
            }
        }
        return sb;
    }

    static synchronized void a(e eVar) {
        synchronized (e.class) {
            B = eVar;
        }
    }

    static void a(String str, o oVar) {
        if (str.length() > 1 && str.charAt(0) == '0') {
            oVar.a(true);
            int i2 = 1;
            while (i2 < str.length() - 1 && str.charAt(i2) == '0') {
                i2++;
            }
            if (i2 != 1) {
                oVar.b(i2);
            }
        }
    }

    private void a(String str, String str2, boolean z2, boolean z3, o oVar) {
        int a2;
        if (str == null) {
            throw new c(d.NOT_A_NUMBER, "The phone number supplied was null.");
        } else if (str.length() > 250) {
            throw new c(d.TOO_LONG, "The string supplied was too long to parse.");
        } else {
            StringBuilder sb = new StringBuilder();
            a(str, sb);
            if (!b(sb.toString())) {
                throw new c(d.NOT_A_NUMBER, "The string supplied did not seem to be a phone number.");
            } else if (!z3 || b(sb.toString(), str2)) {
                if (z2) {
                    oVar.b(str);
                }
                String b2 = b(sb);
                if (b2.length() > 0) {
                    oVar.a(b2);
                }
                k e2 = e(str2);
                StringBuilder sb2 = new StringBuilder();
                try {
                    a2 = a(sb.toString(), e2, sb2, z2, oVar);
                } catch (c e3) {
                    Matcher matcher = b.matcher(sb.toString());
                    if (e3.a() != d.INVALID_COUNTRY_CODE || !matcher.lookingAt()) {
                        throw new c(e3.a(), e3.getMessage());
                    }
                    a2 = a(sb.substring(matcher.end()), e2, sb2, z2, oVar);
                    if (a2 == 0) {
                        throw new c(d.INVALID_COUNTRY_CODE, "Could not interpret numbers after plus-sign.");
                    }
                }
                if (a2 != 0) {
                    String b3 = b(a2);
                    if (!b3.equals(str2)) {
                        e2 = a(a2, b3);
                    }
                } else {
                    a(sb);
                    sb2.append((CharSequence) sb);
                    if (str2 != null) {
                        oVar.a(e2.l());
                    } else if (z2) {
                        oVar.l();
                    }
                }
                if (sb2.length() < 2) {
                    throw new c(d.TOO_SHORT_NSN, "The string supplied is too short to be a phone number.");
                }
                if (e2 != null) {
                    StringBuilder sb3 = new StringBuilder();
                    StringBuilder sb4 = new StringBuilder(sb2);
                    a(sb4, e2, sb3);
                    if (!a(e2, sb4.toString())) {
                        if (z2) {
                            oVar.c(sb3.toString());
                        }
                        sb2 = sb4;
                    }
                }
                int length = sb2.length();
                if (length < 2) {
                    throw new c(d.TOO_SHORT_NSN, "The string supplied is too short to be a phone number.");
                } else if (length > 17) {
                    throw new c(d.TOO_LONG, "The string supplied is too long to be a phone number.");
                } else {
                    a(sb2.toString(), oVar);
                    oVar.a(Long.parseLong(sb2.toString()));
                }
            } else {
                throw new c(d.INVALID_COUNTRY_CODE, "Missing or invalid default region.");
            }
        }
    }

    private void a(String str, StringBuilder sb) {
        int indexOf = str.indexOf(";phone-context=");
        if (indexOf > 0) {
            int length = ";phone-context=".length() + indexOf;
            if (str.charAt(length) == '+') {
                int indexOf2 = str.indexOf(59, length);
                if (indexOf2 > 0) {
                    sb.append(str.substring(length, indexOf2));
                } else {
                    sb.append(str.substring(length));
                }
            }
            int indexOf3 = str.indexOf("tel:");
            sb.append(str.substring(indexOf3 >= 0 ? indexOf3 + "tel:".length() : 0, indexOf));
        } else {
            sb.append(a(str));
        }
        int indexOf4 = sb.indexOf(";isub=");
        if (indexOf4 > 0) {
            sb.delete(indexOf4, sb.length());
        }
    }

    static void a(StringBuilder sb) {
        sb.replace(0, sb.length(), c(sb.toString()));
    }

    private boolean a(k kVar, String str) {
        return a(this.G.a(kVar.a().c()), str) == h.TOO_SHORT;
    }

    private boolean a(Pattern pattern, StringBuilder sb) {
        Matcher matcher = pattern.matcher(sb);
        if (!matcher.lookingAt()) {
            return false;
        }
        int end = matcher.end();
        Matcher matcher2 = p.matcher(sb.substring(end));
        if (matcher2.find() && d(matcher2.group(1)).equals("0")) {
            return false;
        }
        sb.delete(0, end);
        return true;
    }

    static boolean b(String str) {
        if (str.length() < 2) {
            return false;
        }
        return v.matcher(str).matches();
    }

    private boolean b(String str, String str2) {
        return g(str2) || !(str == null || str.length() == 0 || !b.matcher(str).lookingAt());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.a.a.a.e.a(java.lang.String, java.util.Map, boolean):java.lang.String
     arg types: [java.lang.String, java.util.Map, int]
     candidates:
      a.a.a.a.e.a(java.lang.String, java.lang.String, a.a.a.a.o):void
      a.a.a.a.e.a(java.lang.StringBuilder, a.a.a.a.k, java.lang.StringBuilder):boolean
      a.a.a.a.e.a(java.lang.String, java.util.Map, boolean):java.lang.String */
    static String c(String str) {
        return r.matcher(str).matches() ? a(str, k, true) : d(str);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.a.a.a.e.a(java.lang.String, boolean):java.lang.StringBuilder
     arg types: [java.lang.String, int]
     candidates:
      a.a.a.a.e.a(java.lang.String, a.a.a.a.k):a.a.a.a.g
      a.a.a.a.e.a(java.util.regex.Pattern, java.lang.String):a.a.a.a.h
      a.a.a.a.e.a(int, java.lang.String):a.a.a.a.k
      a.a.a.a.e.a(a.a.a.a.o, java.util.List):java.lang.String
      a.a.a.a.e.a(java.lang.String, a.a.a.a.o):void
      a.a.a.a.e.a(java.lang.String, java.lang.StringBuilder):void
      a.a.a.a.e.a(a.a.a.a.k, java.lang.String):boolean
      a.a.a.a.e.a(java.util.regex.Pattern, java.lang.StringBuilder):boolean
      a.a.a.a.e.a(java.lang.StringBuilder, java.lang.StringBuilder):int
      a.a.a.a.e.a(java.lang.String, java.lang.String):a.a.a.a.o
      a.a.a.a.e.a(java.lang.StringBuilder, java.lang.String):a.a.a.a.p
      a.a.a.a.e.a(a.a.a.a.o, java.lang.String):boolean
      a.a.a.a.e.a(java.lang.String, a.a.a.a.m):boolean
      a.a.a.a.e.a(java.lang.String, boolean):java.lang.StringBuilder */
    public static String d(String str) {
        return a(str, false).toString();
    }

    private static String f(String str) {
        String valueOf = String.valueOf(String.valueOf(";ext=(\\p{Nd}{1,7})|[  \\t,]*(?:e?xt(?:ensi(?:ó?|ó))?n?|ｅ?ｘｔｎ?|["));
        String valueOf2 = String.valueOf(String.valueOf(str));
        String valueOf3 = String.valueOf(String.valueOf("(\\p{Nd}{1,7})"));
        String valueOf4 = String.valueOf(String.valueOf("\\p{Nd}"));
        return new StringBuilder(valueOf.length() + 48 + valueOf2.length() + valueOf3.length() + valueOf4.length()).append(valueOf).append(valueOf2).append("]|int|anexo|ｉｎｔ)").append("[:\\.．]?[  \\t,-]*").append(valueOf3).append("#?|").append("[- ]+(").append(valueOf4).append("{1,5})#").toString();
    }

    private boolean g(String str) {
        return str != null && this.H.contains(str);
    }

    private int h(String str) {
        k e2 = e(str);
        if (e2 != null) {
            return e2.l();
        }
        String valueOf = String.valueOf(str);
        throw new IllegalArgumentException(valueOf.length() != 0 ? "Invalid region code: ".concat(valueOf) : new String("Invalid region code: "));
    }

    /* access modifiers changed from: package-private */
    public int a(String str, k kVar, StringBuilder sb, boolean z2, o oVar) {
        if (str.length() == 0) {
            return 0;
        }
        StringBuilder sb2 = new StringBuilder(str);
        String str2 = "NonMatch";
        if (kVar != null) {
            str2 = kVar.m();
        }
        p a2 = a(sb2, str2);
        if (z2) {
            oVar.a(a2);
        }
        if (a2 == p.FROM_DEFAULT_COUNTRY) {
            if (kVar != null) {
                int l2 = kVar.l();
                String valueOf = String.valueOf(l2);
                String sb3 = sb2.toString();
                if (sb3.startsWith(valueOf)) {
                    StringBuilder sb4 = new StringBuilder(sb3.substring(valueOf.length()));
                    m a3 = kVar.a();
                    Pattern a4 = this.G.a(a3.b());
                    a(sb4, kVar, (StringBuilder) null);
                    Pattern a5 = this.G.a(a3.c());
                    if ((!a4.matcher(sb2).matches() && a4.matcher(sb4).matches()) || a(a5, sb2.toString()) == h.TOO_LONG) {
                        sb.append((CharSequence) sb4);
                        if (z2) {
                            oVar.a(p.FROM_NUMBER_WITHOUT_PLUS_SIGN);
                        }
                        oVar.a(l2);
                        return l2;
                    }
                }
            }
            oVar.a(0);
            return 0;
        } else if (sb2.length() <= 2) {
            throw new c(d.TOO_SHORT_AFTER_IDD, "Phone number had an IDD, but after this was not long enough to be a viable phone number.");
        } else {
            int a6 = a(sb2, sb);
            if (a6 != 0) {
                oVar.a(a6);
                return a6;
            }
            throw new c(d.INVALID_COUNTRY_CODE, "Country calling code supplied was not recognised.");
        }
    }

    /* access modifiers changed from: package-private */
    public int a(StringBuilder sb, StringBuilder sb2) {
        if (sb.length() == 0 || sb.charAt(0) == '0') {
            return 0;
        }
        int length = sb.length();
        int i2 = 1;
        while (true) {
            int i3 = i2;
            if (i3 <= 3 && i3 <= length) {
                int parseInt = Integer.parseInt(sb.substring(0, i3));
                if (this.C.containsKey(Integer.valueOf(parseInt))) {
                    sb2.append(sb.substring(i3));
                    return parseInt;
                }
                i2 = i3 + 1;
            }
        }
        return 0;
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:20:?, code lost:
        return (a.a.a.a.k) r4.F.get(java.lang.Integer.valueOf(r5));
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public a.a.a.a.k a(int r5) {
        /*
            r4 = this;
            java.util.Map r1 = r4.F
            monitor-enter(r1)
            java.util.Map r0 = r4.C     // Catch:{ all -> 0x0035 }
            java.lang.Integer r2 = java.lang.Integer.valueOf(r5)     // Catch:{ all -> 0x0035 }
            boolean r0 = r0.containsKey(r2)     // Catch:{ all -> 0x0035 }
            if (r0 != 0) goto L_0x0012
            r0 = 0
            monitor-exit(r1)     // Catch:{ all -> 0x0035 }
        L_0x0011:
            return r0
        L_0x0012:
            java.util.Map r0 = r4.F     // Catch:{ all -> 0x0035 }
            java.lang.Integer r2 = java.lang.Integer.valueOf(r5)     // Catch:{ all -> 0x0035 }
            boolean r0 = r0.containsKey(r2)     // Catch:{ all -> 0x0035 }
            if (r0 != 0) goto L_0x0027
            java.lang.String r0 = r4.J     // Catch:{ all -> 0x0035 }
            java.lang.String r2 = "001"
            a.a.a.a.b r3 = r4.K     // Catch:{ all -> 0x0035 }
            r4.a(r0, r2, r5, r3)     // Catch:{ all -> 0x0035 }
        L_0x0027:
            monitor-exit(r1)     // Catch:{ all -> 0x0035 }
            java.util.Map r0 = r4.F
            java.lang.Integer r1 = java.lang.Integer.valueOf(r5)
            java.lang.Object r0 = r0.get(r1)
            a.a.a.a.k r0 = (a.a.a.a.k) r0
            goto L_0x0011
        L_0x0035:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0035 }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: a.a.a.a.e.a(int):a.a.a.a.k");
    }

    public o a(String str, String str2) {
        o oVar = new o();
        a(str, str2, oVar);
        return oVar;
    }

    /* access modifiers changed from: package-private */
    public p a(StringBuilder sb, String str) {
        if (sb.length() == 0) {
            return p.FROM_DEFAULT_COUNTRY;
        }
        Matcher matcher = b.matcher(sb);
        if (matcher.lookingAt()) {
            sb.delete(0, matcher.end());
            a(sb);
            return p.FROM_NUMBER_WITH_PLUS_SIGN;
        }
        Pattern a2 = this.G.a(str);
        a(sb);
        return a(a2, sb) ? p.FROM_NUMBER_WITH_IDD : p.FROM_DEFAULT_COUNTRY;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Arrays.fill(char[], char):void}
     arg types: [char[], int]
     candidates:
      ClspMth{java.util.Arrays.fill(double[], double):void}
      ClspMth{java.util.Arrays.fill(byte[], byte):void}
      ClspMth{java.util.Arrays.fill(long[], long):void}
      ClspMth{java.util.Arrays.fill(boolean[], boolean):void}
      ClspMth{java.util.Arrays.fill(short[], short):void}
      ClspMth{java.util.Arrays.fill(java.lang.Object[], java.lang.Object):void}
      ClspMth{java.util.Arrays.fill(int[], int):void}
      ClspMth{java.util.Arrays.fill(float[], float):void}
      ClspMth{java.util.Arrays.fill(char[], char):void} */
    public String a(o oVar) {
        StringBuilder sb = new StringBuilder();
        if (oVar.f()) {
            char[] cArr = new char[oVar.h()];
            Arrays.fill(cArr, '0');
            sb.append(new String(cArr));
        }
        sb.append(oVar.b());
        return sb.toString();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
     arg types: [java.util.logging.Level, java.lang.String, java.io.IOException]
     candidates:
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
    /* access modifiers changed from: package-private */
    public void a(String str, String str2, int i2, b bVar) {
        boolean equals = "001".equals(str2);
        String valueOf = String.valueOf(String.valueOf(str));
        String valueOf2 = String.valueOf(String.valueOf(equals ? String.valueOf(i2) : str2));
        String sb = new StringBuilder(valueOf.length() + 1 + valueOf2.length()).append(valueOf).append("_").append(valueOf2).toString();
        InputStream a2 = bVar.a(sb);
        if (a2 == null) {
            Logger logger = g;
            Level level = Level.SEVERE;
            String valueOf3 = String.valueOf(sb);
            logger.log(level, valueOf3.length() != 0 ? "missing metadata: ".concat(valueOf3) : new String("missing metadata: "));
            String valueOf4 = String.valueOf(sb);
            throw new IllegalStateException(valueOf4.length() != 0 ? "missing metadata: ".concat(valueOf4) : new String("missing metadata: "));
        }
        try {
            List a3 = a(new ObjectInputStream(a2)).a();
            if (a3.isEmpty()) {
                Logger logger2 = g;
                Level level2 = Level.SEVERE;
                String valueOf5 = String.valueOf(sb);
                logger2.log(level2, valueOf5.length() != 0 ? "empty metadata: ".concat(valueOf5) : new String("empty metadata: "));
                String valueOf6 = String.valueOf(sb);
                throw new IllegalStateException(valueOf6.length() != 0 ? "empty metadata: ".concat(valueOf6) : new String("empty metadata: "));
            }
            if (a3.size() > 1) {
                Logger logger3 = g;
                Level level3 = Level.WARNING;
                String valueOf7 = String.valueOf(sb);
                logger3.log(level3, valueOf7.length() != 0 ? "invalid metadata (too many entries): ".concat(valueOf7) : new String("invalid metadata (too many entries): "));
            }
            k kVar = (k) a3.get(0);
            if (equals) {
                this.F.put(Integer.valueOf(i2), kVar);
            } else {
                this.E.put(str2, kVar);
            }
        } catch (IOException e2) {
            IOException iOException = e2;
            Logger logger4 = g;
            Level level4 = Level.SEVERE;
            String valueOf8 = String.valueOf(sb);
            logger4.log(level4, valueOf8.length() != 0 ? "cannot load/parse metadata: ".concat(valueOf8) : new String("cannot load/parse metadata: "), (Throwable) iOException);
            String valueOf9 = String.valueOf(sb);
            throw new RuntimeException(valueOf9.length() != 0 ? "cannot load/parse metadata: ".concat(valueOf9) : new String("cannot load/parse metadata: "), iOException);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.a.a.a.e.a(java.lang.String, java.lang.String, boolean, boolean, a.a.a.a.o):void
     arg types: [java.lang.String, java.lang.String, int, int, a.a.a.a.o]
     candidates:
      a.a.a.a.e.a(java.lang.String, a.a.a.a.k, java.lang.StringBuilder, boolean, a.a.a.a.o):int
      a.a.a.a.e.a(java.lang.String, java.lang.String, boolean, boolean, a.a.a.a.o):void */
    public void a(String str, String str2, o oVar) {
        a(str, str2, false, true, oVar);
    }

    public boolean a(o oVar, String str) {
        int a2 = oVar.a();
        k a3 = a(a2, str);
        if (a3 == null || (!"001".equals(str) && a2 != h(str))) {
            return false;
        }
        m a4 = a3.a();
        String a5 = a(oVar);
        if (a4.a()) {
            return a(a5, a3) != g.UNKNOWN;
        }
        int length = a5.length();
        return length > 2 && length <= 17;
    }

    /* access modifiers changed from: package-private */
    public boolean a(String str, m mVar) {
        return this.G.a(mVar.c()).matcher(str).matches();
    }

    /* access modifiers changed from: package-private */
    public boolean a(StringBuilder sb, k kVar, StringBuilder sb2) {
        int length = sb.length();
        String n2 = kVar.n();
        if (length == 0 || n2.length() == 0) {
            return false;
        }
        Matcher matcher = this.G.a(n2).matcher(sb);
        if (!matcher.lookingAt()) {
            return false;
        }
        Pattern a2 = this.G.a(kVar.a().b());
        boolean matches = a2.matcher(sb).matches();
        int groupCount = matcher.groupCount();
        String o2 = kVar.o();
        if (o2 != null && o2.length() != 0 && matcher.group(groupCount) != null) {
            StringBuilder sb3 = new StringBuilder(sb);
            sb3.replace(0, length, matcher.replaceFirst(o2));
            if (matches && !a2.matcher(sb3.toString()).matches()) {
                return false;
            }
            if (sb2 != null && groupCount > 1) {
                sb2.append(matcher.group(1));
            }
            sb.replace(0, sb.length(), sb3.toString());
            return true;
        } else if (matches && !a2.matcher(sb.substring(matcher.end())).matches()) {
            return false;
        } else {
            if (!(sb2 == null || groupCount <= 0 || matcher.group(groupCount) == null)) {
                sb2.append(matcher.group(1));
            }
            sb.delete(0, matcher.end());
            return true;
        }
    }

    public String b(int i2) {
        List list = (List) this.C.get(Integer.valueOf(i2));
        return list == null ? "ZZ" : (String) list.get(0);
    }

    /* access modifiers changed from: package-private */
    public String b(StringBuilder sb) {
        Matcher matcher = u.matcher(sb);
        if (matcher.find() && b(sb.substring(0, matcher.start()))) {
            int groupCount = matcher.groupCount();
            for (int i2 = 1; i2 <= groupCount; i2++) {
                if (matcher.group(i2) != null) {
                    String group = matcher.group(i2);
                    sb.delete(matcher.start(), sb.length());
                    return group;
                }
            }
        }
        return "";
    }

    public boolean b(o oVar) {
        return a(oVar, c(oVar));
    }

    /* access modifiers changed from: package-private */
    public boolean b(String str, m mVar) {
        return a(str, mVar) && this.G.a(mVar.b()).matcher(str).matches();
    }

    public String c(o oVar) {
        int a2 = oVar.a();
        List list = (List) this.C.get(Integer.valueOf(a2));
        if (list != null) {
            return list.size() == 1 ? (String) list.get(0) : a(oVar, list);
        }
        String a3 = a(oVar);
        Logger logger = g;
        Level level = Level.WARNING;
        String valueOf = String.valueOf(String.valueOf(a3));
        logger.log(level, new StringBuilder(valueOf.length() + 54).append("Missing/invalid country_code (").append(a2).append(") for number ").append(valueOf).toString());
        return null;
    }

    /* access modifiers changed from: package-private */
    public k e(String str) {
        if (!g(str)) {
            return null;
        }
        synchronized (this.E) {
            if (!this.E.containsKey(str)) {
                a(this.J, str, 0, this.K);
            }
        }
        return (k) this.E.get(str);
    }
}
