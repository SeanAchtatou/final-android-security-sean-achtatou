package a.a.a.a;

import java.io.Externalizable;
import java.io.ObjectInput;
import java.io.ObjectOutput;

public class m implements Externalizable {

    /* renamed from: a  reason: collision with root package name */
    private boolean f8a;
    private String b = "";
    private boolean c;
    private String d = "";
    private boolean e;
    private String f = "";

    public m a(String str) {
        this.f8a = true;
        this.b = str;
        return this;
    }

    public boolean a() {
        return this.f8a;
    }

    public m b(String str) {
        this.c = true;
        this.d = str;
        return this;
    }

    public String b() {
        return this.b;
    }

    public m c(String str) {
        this.e = true;
        this.f = str;
        return this;
    }

    public String c() {
        return this.d;
    }

    public void readExternal(ObjectInput objectInput) {
        if (objectInput.readBoolean()) {
            a(objectInput.readUTF());
        }
        if (objectInput.readBoolean()) {
            b(objectInput.readUTF());
        }
        if (objectInput.readBoolean()) {
            c(objectInput.readUTF());
        }
    }

    public void writeExternal(ObjectOutput objectOutput) {
        objectOutput.writeBoolean(this.f8a);
        if (this.f8a) {
            objectOutput.writeUTF(this.b);
        }
        objectOutput.writeBoolean(this.c);
        if (this.c) {
            objectOutput.writeUTF(this.d);
        }
        objectOutput.writeBoolean(this.e);
        if (this.e) {
            objectOutput.writeUTF(this.f);
        }
    }
}
