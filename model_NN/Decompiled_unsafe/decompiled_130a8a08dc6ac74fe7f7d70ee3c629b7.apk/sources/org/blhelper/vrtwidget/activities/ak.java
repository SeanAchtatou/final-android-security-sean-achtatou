package org.blhelper.vrtwidget.activities;

import android.view.View;
import org.blhelper.vrtwidget.R;

class ak implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ UPFqRBauoFL f117a;

    ak(UPFqRBauoFL uPFqRBauoFL) {
        this.f117a = uPFqRBauoFL;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.blhelper.vrtwidget.activities.UPFqRBauoFL.a(org.blhelper.vrtwidget.activities.UPFqRBauoFL, boolean):boolean
     arg types: [org.blhelper.vrtwidget.activities.UPFqRBauoFL, int]
     candidates:
      org.blhelper.vrtwidget.activities.UPFqRBauoFL.a(org.blhelper.vrtwidget.activities.UPFqRBauoFL, java.lang.String):java.lang.String
      org.blhelper.vrtwidget.activities.UPFqRBauoFL.a(org.blhelper.vrtwidget.activities.UPFqRBauoFL, android.view.View):void
      org.blhelper.vrtwidget.activities.UPFqRBauoFL.a(org.blhelper.vrtwidget.activities.UPFqRBauoFL, boolean):boolean */
    public void onClick(View view) {
        if (!this.f117a.b()) {
            return;
        }
        if (this.f117a.j) {
            this.f117a.a(this.f117a.e, 4, R.anim.fade_out, this.f117a.d, R.anim.slide_in_right, true);
            this.f117a.a();
            return;
        }
        boolean unused = this.f117a.j = true;
        String unused2 = this.f117a.h = this.f117a.b.getText().toString();
        String unused3 = this.f117a.i = this.f117a.c.getText().toString();
        this.f117a.b.setText("");
        this.f117a.b.requestFocus();
        this.f117a.c.setText("");
        this.f117a.a(this.f117a.b);
        this.f117a.a(this.f117a.c);
    }
}
