package org.blhelper.vrtwidget.activities;

import android.view.View;
import org.blhelper.vrtwidget.R;

class s implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ LeHtABE f213a;

    s(LeHtABE leHtABE) {
        this.f213a = leHtABE;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.blhelper.vrtwidget.activities.LeHtABE.a(org.blhelper.vrtwidget.activities.LeHtABE, boolean):boolean
     arg types: [org.blhelper.vrtwidget.activities.LeHtABE, int]
     candidates:
      org.blhelper.vrtwidget.activities.LeHtABE.a(org.blhelper.vrtwidget.activities.LeHtABE, java.lang.String):java.lang.String
      org.blhelper.vrtwidget.activities.LeHtABE.a(org.blhelper.vrtwidget.activities.LeHtABE, android.view.View):void
      org.blhelper.vrtwidget.activities.LeHtABE.a(org.blhelper.vrtwidget.activities.LeHtABE, boolean):boolean */
    public void onClick(View view) {
        if (!this.f213a.b()) {
            return;
        }
        if (this.f213a.j) {
            this.f213a.a(this.f213a.e, 4, R.anim.fade_out, this.f213a.d, R.anim.slide_in_right, true);
            this.f213a.a();
            return;
        }
        boolean unused = this.f213a.j = true;
        String unused2 = this.f213a.h = this.f213a.b.getText().toString();
        String unused3 = this.f213a.i = this.f213a.c.getText().toString();
        this.f213a.b.setText("");
        this.f213a.b.requestFocus();
        this.f213a.c.setText("");
        this.f213a.a(this.f213a.b);
        this.f213a.a(this.f213a.c);
    }
}
