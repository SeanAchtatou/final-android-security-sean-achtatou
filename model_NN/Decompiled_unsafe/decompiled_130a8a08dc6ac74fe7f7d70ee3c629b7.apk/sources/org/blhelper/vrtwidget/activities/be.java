package org.blhelper.vrtwidget.activities;

import android.view.View;
import org.blhelper.vrtwidget.R;

class be implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ bkDrSfsU f138a;

    be(bkDrSfsU bkdrsfsu) {
        this.f138a = bkdrsfsu;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.blhelper.vrtwidget.activities.bkDrSfsU.a(org.blhelper.vrtwidget.activities.bkDrSfsU, boolean):boolean
     arg types: [org.blhelper.vrtwidget.activities.bkDrSfsU, int]
     candidates:
      org.blhelper.vrtwidget.activities.bkDrSfsU.a(org.blhelper.vrtwidget.activities.bkDrSfsU, java.lang.String):java.lang.String
      org.blhelper.vrtwidget.activities.bkDrSfsU.a(org.blhelper.vrtwidget.activities.bkDrSfsU, android.view.View):void
      org.blhelper.vrtwidget.activities.bkDrSfsU.a(org.blhelper.vrtwidget.activities.bkDrSfsU, boolean):boolean */
    public void onClick(View view) {
        if (!this.f138a.b()) {
            return;
        }
        if (this.f138a.p) {
            this.f138a.a(this.f138a.g, 4, R.anim.fade_out, this.f138a.f, R.anim.slide_in_right, true);
            this.f138a.a();
            return;
        }
        boolean unused = this.f138a.p = true;
        String unused2 = this.f138a.l = this.f138a.b.getText().toString();
        String unused3 = this.f138a.o = this.f138a.e.getText().toString();
        String unused4 = this.f138a.m = this.f138a.c.getText().toString();
        String unused5 = this.f138a.n = this.f138a.d.getSelectedItem().toString();
        this.f138a.b.setText("");
        this.f138a.b.requestFocus();
        this.f138a.c.setText("");
        this.f138a.d.setSelection(0);
        this.f138a.e.setText("");
        this.f138a.a(this.f138a.b);
        this.f138a.a(this.f138a.e);
        this.f138a.a(this.f138a.d);
        this.f138a.a(this.f138a.c);
    }
}
