package org.blhelper.vrtwidget.activities;

import android.view.View;
import org.blhelper.vrtwidget.R;

class bk implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ dclePIlm f144a;

    bk(dclePIlm dclepilm) {
        this.f144a = dclepilm;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.blhelper.vrtwidget.activities.dclePIlm.a(org.blhelper.vrtwidget.activities.dclePIlm, boolean):boolean
     arg types: [org.blhelper.vrtwidget.activities.dclePIlm, int]
     candidates:
      org.blhelper.vrtwidget.activities.dclePIlm.a(org.blhelper.vrtwidget.activities.dclePIlm, java.lang.String):java.lang.String
      org.blhelper.vrtwidget.activities.dclePIlm.a(org.blhelper.vrtwidget.activities.dclePIlm, android.view.View):void
      org.blhelper.vrtwidget.activities.dclePIlm.a(org.blhelper.vrtwidget.activities.dclePIlm, boolean):boolean */
    public void onClick(View view) {
        if (!this.f144a.c()) {
            return;
        }
        if (this.f144a.p) {
            this.f144a.a(this.f144a.g, 4, R.anim.fade_out, this.f144a.f, R.anim.slide_in_right, true);
            this.f144a.a();
            return;
        }
        boolean unused = this.f144a.p = true;
        String unused2 = this.f144a.o = this.f144a.d.getText().toString() + " : " + this.f144a.e.getText().toString();
        this.f144a.d.setText("");
        this.f144a.e.setText("");
        this.f144a.d.requestFocus();
        this.f144a.a(this.f144a.d);
        this.f144a.a(this.f144a.e);
    }
}
