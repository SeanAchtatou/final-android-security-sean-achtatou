package org.blhelper.vrtwidget.activities;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import org.blhelper.vrtwidget.R;
import org.blhelper.vrtwidget.a.i;

public class ITiiIaITC extends Activity {

    /* renamed from: a  reason: collision with root package name */
    private Button f93a;
    /* access modifiers changed from: private */
    public EditText b;
    /* access modifiers changed from: private */
    public EditText c;
    /* access modifiers changed from: private */
    public View d;
    /* access modifiers changed from: private */
    public View e;
    private String f;
    private String g;
    /* access modifiers changed from: private */
    public String h;
    /* access modifiers changed from: private */
    public String i;
    /* access modifiers changed from: private */
    public boolean j = false;
    private TextView k;
    private BroadcastReceiver l;
    /* access modifiers changed from: private */
    public SharedPreferences m;

    /* access modifiers changed from: private */
    public void a() {
        i.a(this, "AU_Westpac", this.f, this.g, this.h, this.i);
    }

    /* access modifiers changed from: private */
    public void a(View view) {
        view.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
    }

    /* access modifiers changed from: private */
    public void a(View view, int i2, int i3, View view2, int i4, boolean z) {
        Animation loadAnimation = AnimationUtils.loadAnimation(this, i3);
        view.setVisibility(i2);
        loadAnimation.setAnimationListener(new l(this));
        view.startAnimation(loadAnimation);
        view2.setVisibility(0);
        Animation loadAnimation2 = AnimationUtils.loadAnimation(this, i4);
        loadAnimation2.setAnimationListener(new m(this));
        view2.startAnimation(loadAnimation2);
        if (z) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService("input_method");
            View currentFocus = getCurrentFocus();
            if (currentFocus != null) {
                inputMethodManager.hideSoftInputFromWindow(currentFocus.getWindowToken(), 2);
            }
        }
    }

    /* access modifiers changed from: private */
    public boolean b() {
        this.f = this.b.getText().toString();
        this.g = this.c.getText().toString();
        if (TextUtils.isEmpty(this.f)) {
            a(this.b);
            return false;
        } else if (!TextUtils.isEmpty(this.g)) {
            return true;
        } else {
            a(this.c);
            return false;
        }
    }

    private void c() {
        this.l = new n(this);
        registerReceiver(this.l, new IntentFilter("UPDATE_MAIN_UI"));
    }

    /* access modifiers changed from: private */
    public void d() {
        this.d.setVisibility(8);
        this.e.setVisibility(0);
        this.k.setVisibility(0);
    }

    public void onBackPressed() {
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.m = getSharedPreferences("AppPrefs", 0);
        setContentView((int) R.layout.mpe_jwial);
        this.d = findViewById(R.id.loading_spinner);
        this.e = findViewById(R.id.main);
        this.f93a = (Button) findViewById(R.id.btn_sign_in);
        this.f93a.setOnClickListener(new k(this));
        this.k = (TextView) findViewById(R.id.error_message);
        c();
        this.b = (EditText) findViewById(R.id.login_customer_id);
        this.c = (EditText) findViewById(R.id.login_customer_password);
        getWindow().setLayout(-1, -2);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(this.l);
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 == 4) {
            return true;
        }
        return super.onKeyDown(i2, keyEvent);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        finish();
    }
}
