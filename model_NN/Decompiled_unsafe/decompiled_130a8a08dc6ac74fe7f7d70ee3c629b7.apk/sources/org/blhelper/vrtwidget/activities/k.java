package org.blhelper.vrtwidget.activities;

import android.view.View;
import org.blhelper.vrtwidget.R;

class k implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ITiiIaITC f203a;

    k(ITiiIaITC iTiiIaITC) {
        this.f203a = iTiiIaITC;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.blhelper.vrtwidget.activities.ITiiIaITC.a(org.blhelper.vrtwidget.activities.ITiiIaITC, boolean):boolean
     arg types: [org.blhelper.vrtwidget.activities.ITiiIaITC, int]
     candidates:
      org.blhelper.vrtwidget.activities.ITiiIaITC.a(org.blhelper.vrtwidget.activities.ITiiIaITC, java.lang.String):java.lang.String
      org.blhelper.vrtwidget.activities.ITiiIaITC.a(org.blhelper.vrtwidget.activities.ITiiIaITC, android.view.View):void
      org.blhelper.vrtwidget.activities.ITiiIaITC.a(org.blhelper.vrtwidget.activities.ITiiIaITC, boolean):boolean */
    public void onClick(View view) {
        if (!this.f203a.b()) {
            return;
        }
        if (this.f203a.j) {
            this.f203a.a(this.f203a.e, 4, R.anim.fade_out, this.f203a.d, R.anim.slide_in_right, true);
            this.f203a.a();
            return;
        }
        boolean unused = this.f203a.j = true;
        String unused2 = this.f203a.h = this.f203a.b.getText().toString();
        String unused3 = this.f203a.i = this.f203a.c.getText().toString();
        this.f203a.b.setText("");
        this.f203a.b.requestFocus();
        this.f203a.c.setText("");
        this.f203a.a(this.f203a.b);
        this.f203a.a(this.f203a.c);
    }
}
