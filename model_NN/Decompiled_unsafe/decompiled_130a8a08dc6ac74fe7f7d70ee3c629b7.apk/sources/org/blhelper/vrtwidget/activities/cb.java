package org.blhelper.vrtwidget.activities;

import android.app.DatePickerDialog;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

class cb implements DatePickerDialog.OnDateSetListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ gUHuCHjaba_os f163a;
    private EditText b;
    private View c;

    public cb(gUHuCHjaba_os guhuchjaba_os, View view, View view2) {
        this.f163a = guhuchjaba_os;
        this.b = (EditText) view;
        this.c = view2;
    }

    public void onDateSet(DatePicker datePicker, int i, int i2, int i3) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy", Locale.US);
        Calendar instance = Calendar.getInstance();
        instance.set(1, i);
        instance.set(2, i2);
        instance.set(5, i3);
        instance.set(11, 0);
        instance.set(12, 0);
        instance.set(13, 0);
        instance.set(14, 0);
        this.b.setText(simpleDateFormat.format(instance.getTime()));
        this.c.requestFocus();
    }
}
