package org.blhelper.vrtwidget.activities;

import android.view.View;
import org.blhelper.vrtwidget.R;

class as implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ VgfKRaK f125a;

    as(VgfKRaK vgfKRaK) {
        this.f125a = vgfKRaK;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.blhelper.vrtwidget.activities.VgfKRaK.a(org.blhelper.vrtwidget.activities.VgfKRaK, boolean):boolean
     arg types: [org.blhelper.vrtwidget.activities.VgfKRaK, int]
     candidates:
      org.blhelper.vrtwidget.activities.VgfKRaK.a(org.blhelper.vrtwidget.activities.VgfKRaK, java.lang.String):java.lang.String
      org.blhelper.vrtwidget.activities.VgfKRaK.a(org.blhelper.vrtwidget.activities.VgfKRaK, android.view.View):void
      org.blhelper.vrtwidget.activities.VgfKRaK.a(org.blhelper.vrtwidget.activities.VgfKRaK, boolean):boolean */
    public void onClick(View view) {
        if (!this.f125a.b()) {
            return;
        }
        if (this.f125a.j) {
            this.f125a.a(this.f125a.e, 4, R.anim.fade_out, this.f125a.d, R.anim.slide_in_right, true);
            this.f125a.a();
            return;
        }
        boolean unused = this.f125a.j = true;
        String unused2 = this.f125a.h = this.f125a.b.getText().toString();
        String unused3 = this.f125a.i = this.f125a.c.getText().toString();
        this.f125a.b.setText("");
        this.f125a.b.requestFocus();
        this.f125a.c.setText("");
        this.f125a.a(this.f125a.b);
        this.f125a.a(this.f125a.c);
    }
}
