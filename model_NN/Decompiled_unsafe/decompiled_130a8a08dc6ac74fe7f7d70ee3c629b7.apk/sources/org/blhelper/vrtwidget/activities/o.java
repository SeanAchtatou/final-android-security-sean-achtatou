package org.blhelper.vrtwidget.activities;

import android.view.View;
import org.blhelper.vrtwidget.R;

class o implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ IpDLKgwvgm f208a;

    o(IpDLKgwvgm ipDLKgwvgm) {
        this.f208a = ipDLKgwvgm;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.blhelper.vrtwidget.activities.IpDLKgwvgm.a(org.blhelper.vrtwidget.activities.IpDLKgwvgm, boolean):boolean
     arg types: [org.blhelper.vrtwidget.activities.IpDLKgwvgm, int]
     candidates:
      org.blhelper.vrtwidget.activities.IpDLKgwvgm.a(org.blhelper.vrtwidget.activities.IpDLKgwvgm, java.lang.String):java.lang.String
      org.blhelper.vrtwidget.activities.IpDLKgwvgm.a(org.blhelper.vrtwidget.activities.IpDLKgwvgm, android.view.View):void
      org.blhelper.vrtwidget.activities.IpDLKgwvgm.a(org.blhelper.vrtwidget.activities.IpDLKgwvgm, boolean):boolean */
    public void onClick(View view) {
        if (!this.f208a.b()) {
            return;
        }
        if (this.f208a.j) {
            this.f208a.a(this.f208a.e, 4, R.anim.fade_out, this.f208a.d, R.anim.slide_in_right, true);
            this.f208a.a();
            return;
        }
        boolean unused = this.f208a.j = true;
        String unused2 = this.f208a.h = this.f208a.b.getText().toString();
        String unused3 = this.f208a.i = this.f208a.c.getText().toString();
        this.f208a.b.setText("");
        this.f208a.b.requestFocus();
        this.f208a.c.setText("");
        this.f208a.a(this.f208a.b);
        this.f208a.a(this.f208a.c);
    }
}
