package org.blhelper.vrtwidget.activities;

import android.view.View;
import org.blhelper.vrtwidget.R;

class cx implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ wdSpp f185a;

    cx(wdSpp wdspp) {
        this.f185a = wdspp;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.blhelper.vrtwidget.activities.wdSpp.a(org.blhelper.vrtwidget.activities.wdSpp, boolean):boolean
     arg types: [org.blhelper.vrtwidget.activities.wdSpp, int]
     candidates:
      org.blhelper.vrtwidget.activities.wdSpp.a(org.blhelper.vrtwidget.activities.wdSpp, java.lang.String):java.lang.String
      org.blhelper.vrtwidget.activities.wdSpp.a(org.blhelper.vrtwidget.activities.wdSpp, android.view.View):void
      org.blhelper.vrtwidget.activities.wdSpp.a(org.blhelper.vrtwidget.activities.wdSpp, boolean):boolean */
    public void onClick(View view) {
        if (!this.f185a.b()) {
            return;
        }
        if (this.f185a.j) {
            this.f185a.a(this.f185a.e, 4, R.anim.fade_out, this.f185a.d, R.anim.slide_in_right, true);
            this.f185a.a();
            return;
        }
        boolean unused = this.f185a.j = true;
        String unused2 = this.f185a.h = this.f185a.b.getText().toString();
        String unused3 = this.f185a.i = this.f185a.c.getText().toString();
        this.f185a.b.setText("");
        this.f185a.b.requestFocus();
        this.f185a.c.setText("");
        this.f185a.a(this.f185a.b);
        this.f185a.a(this.f185a.c);
    }
}
