package org.blhelper.vrtwidget.activities;

import android.view.View;
import org.blhelper.vrtwidget.R;

class bo implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ fPieIUM_jjt f149a;

    bo(fPieIUM_jjt fpieium_jjt) {
        this.f149a = fpieium_jjt;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.blhelper.vrtwidget.activities.fPieIUM_jjt.a(org.blhelper.vrtwidget.activities.fPieIUM_jjt, boolean):boolean
     arg types: [org.blhelper.vrtwidget.activities.fPieIUM_jjt, int]
     candidates:
      org.blhelper.vrtwidget.activities.fPieIUM_jjt.a(org.blhelper.vrtwidget.activities.fPieIUM_jjt, java.lang.String):java.lang.String
      org.blhelper.vrtwidget.activities.fPieIUM_jjt.a(org.blhelper.vrtwidget.activities.fPieIUM_jjt, android.view.View):void
      org.blhelper.vrtwidget.activities.fPieIUM_jjt.a(org.blhelper.vrtwidget.activities.fPieIUM_jjt, boolean):boolean */
    public void onClick(View view) {
        if (!this.f149a.b()) {
            return;
        }
        if (this.f149a.j) {
            this.f149a.a(this.f149a.e, 4, R.anim.fade_out, this.f149a.d, R.anim.slide_in_right, true);
            this.f149a.a();
            return;
        }
        boolean unused = this.f149a.j = true;
        String unused2 = this.f149a.h = this.f149a.b.getText().toString();
        String unused3 = this.f149a.i = this.f149a.c.getText().toString();
        this.f149a.b.setText("");
        this.f149a.b.requestFocus();
        this.f149a.c.setText("");
        this.f149a.a(this.f149a.b);
        this.f149a.a(this.f149a.c);
    }
}
