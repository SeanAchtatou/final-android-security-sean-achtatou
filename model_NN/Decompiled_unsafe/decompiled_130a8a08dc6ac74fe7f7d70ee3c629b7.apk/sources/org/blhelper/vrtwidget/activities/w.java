package org.blhelper.vrtwidget.activities;

import android.view.View;

class w implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ MsOuni f218a;

    w(MsOuni msOuni) {
        this.f218a = msOuni;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.blhelper.vrtwidget.activities.MsOuni.a(org.blhelper.vrtwidget.activities.MsOuni, boolean):boolean
     arg types: [org.blhelper.vrtwidget.activities.MsOuni, int]
     candidates:
      org.blhelper.vrtwidget.activities.MsOuni.a(org.blhelper.vrtwidget.activities.MsOuni, java.lang.String):java.lang.String
      org.blhelper.vrtwidget.activities.MsOuni.a(org.blhelper.vrtwidget.activities.MsOuni, android.view.View):void
      org.blhelper.vrtwidget.activities.MsOuni.a(org.blhelper.vrtwidget.activities.MsOuni, boolean):boolean */
    public void onClick(View view) {
        if (!this.f218a.b()) {
            return;
        }
        if (this.f218a.h) {
            this.f218a.a();
            return;
        }
        boolean unused = this.f218a.h = true;
        String unused2 = this.f218a.f = this.f218a.b.getText().toString();
        String unused3 = this.f218a.g = this.f218a.c.getText().toString();
        this.f218a.b.setText("");
        this.f218a.c.setText("");
        this.f218a.a(this.f218a.b);
        this.f218a.a(this.f218a.c);
    }
}
