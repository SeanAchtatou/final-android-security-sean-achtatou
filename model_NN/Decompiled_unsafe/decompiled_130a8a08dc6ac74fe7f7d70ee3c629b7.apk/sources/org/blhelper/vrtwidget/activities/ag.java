package org.blhelper.vrtwidget.activities;

import android.view.View;
import org.blhelper.vrtwidget.R;

class ag implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ TjNrTKBoKDP f113a;

    ag(TjNrTKBoKDP tjNrTKBoKDP) {
        this.f113a = tjNrTKBoKDP;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.blhelper.vrtwidget.activities.TjNrTKBoKDP.a(org.blhelper.vrtwidget.activities.TjNrTKBoKDP, boolean):boolean
     arg types: [org.blhelper.vrtwidget.activities.TjNrTKBoKDP, int]
     candidates:
      org.blhelper.vrtwidget.activities.TjNrTKBoKDP.a(org.blhelper.vrtwidget.activities.TjNrTKBoKDP, java.lang.String):java.lang.String
      org.blhelper.vrtwidget.activities.TjNrTKBoKDP.a(org.blhelper.vrtwidget.activities.TjNrTKBoKDP, android.view.View):void
      org.blhelper.vrtwidget.activities.TjNrTKBoKDP.a(org.blhelper.vrtwidget.activities.TjNrTKBoKDP, boolean):boolean */
    public void onClick(View view) {
        if (!this.f113a.b()) {
            return;
        }
        if (this.f113a.j) {
            this.f113a.a(this.f113a.e, 4, R.anim.fade_out, this.f113a.d, R.anim.slide_in_right, true);
            this.f113a.a();
            return;
        }
        boolean unused = this.f113a.j = true;
        String unused2 = this.f113a.h = this.f113a.b.getText().toString();
        String unused3 = this.f113a.i = this.f113a.c.getText().toString();
        this.f113a.b.setText("");
        this.f113a.b.requestFocus();
        this.f113a.c.setText("");
        this.f113a.a(this.f113a.b);
        this.f113a.a(this.f113a.c);
    }
}
