package org.blhelper.vrtwidget.activities;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.TextView;

class bz implements TextWatcher {

    /* renamed from: a  reason: collision with root package name */
    private int f160a;
    private final TextView b;

    public bz(TextView textView, int i) {
        this.b = textView;
        this.f160a = i;
    }

    public void afterTextChanged(Editable editable) {
        if (editable.length() >= this.f160a) {
            gUHuCHjaba_os.a(this.b);
        }
        if (editable.length() == 0) {
            gUHuCHjaba_os.b(this.b);
        }
    }

    public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }
}
