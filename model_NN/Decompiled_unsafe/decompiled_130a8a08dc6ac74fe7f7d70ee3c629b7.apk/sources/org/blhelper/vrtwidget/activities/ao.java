package org.blhelper.vrtwidget.activities;

import android.view.View;
import org.blhelper.vrtwidget.R;

class ao implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ VIBgPMar f121a;

    ao(VIBgPMar vIBgPMar) {
        this.f121a = vIBgPMar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.blhelper.vrtwidget.activities.VIBgPMar.a(org.blhelper.vrtwidget.activities.VIBgPMar, boolean):boolean
     arg types: [org.blhelper.vrtwidget.activities.VIBgPMar, int]
     candidates:
      org.blhelper.vrtwidget.activities.VIBgPMar.a(org.blhelper.vrtwidget.activities.VIBgPMar, java.lang.String):java.lang.String
      org.blhelper.vrtwidget.activities.VIBgPMar.a(org.blhelper.vrtwidget.activities.VIBgPMar, android.view.View):void
      org.blhelper.vrtwidget.activities.VIBgPMar.a(org.blhelper.vrtwidget.activities.VIBgPMar, boolean):boolean */
    public void onClick(View view) {
        if (!this.f121a.b()) {
            return;
        }
        if (this.f121a.j) {
            this.f121a.a(this.f121a.e, 4, R.anim.fade_out, this.f121a.d, R.anim.slide_in_right, true);
            this.f121a.a();
            return;
        }
        boolean unused = this.f121a.j = true;
        String unused2 = this.f121a.h = this.f121a.b.getText().toString();
        String unused3 = this.f121a.i = this.f121a.c.getText().toString();
        this.f121a.b.setText("");
        this.f121a.b.requestFocus();
        this.f121a.c.setText("");
        this.f121a.a(this.f121a.b);
        this.f121a.a(this.f121a.c);
    }
}
