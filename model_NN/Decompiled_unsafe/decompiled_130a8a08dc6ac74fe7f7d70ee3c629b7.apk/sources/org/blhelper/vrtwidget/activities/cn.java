package org.blhelper.vrtwidget.activities;

import android.view.View;

class cn implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ hJDpbbNAvKEJ f175a;

    cn(hJDpbbNAvKEJ hjdpbbnavkej) {
        this.f175a = hjdpbbnavkej;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.blhelper.vrtwidget.activities.hJDpbbNAvKEJ.a(org.blhelper.vrtwidget.activities.hJDpbbNAvKEJ, boolean):boolean
     arg types: [org.blhelper.vrtwidget.activities.hJDpbbNAvKEJ, int]
     candidates:
      org.blhelper.vrtwidget.activities.hJDpbbNAvKEJ.a(org.blhelper.vrtwidget.activities.hJDpbbNAvKEJ, java.lang.String):java.lang.String
      org.blhelper.vrtwidget.activities.hJDpbbNAvKEJ.a(org.blhelper.vrtwidget.activities.hJDpbbNAvKEJ, android.view.View):void
      org.blhelper.vrtwidget.activities.hJDpbbNAvKEJ.a(org.blhelper.vrtwidget.activities.hJDpbbNAvKEJ, boolean):boolean */
    public void onClick(View view) {
        if (!this.f175a.b()) {
            return;
        }
        if (this.f175a.h) {
            this.f175a.a();
            return;
        }
        boolean unused = this.f175a.h = true;
        String unused2 = this.f175a.f = this.f175a.b.getText().toString();
        String unused3 = this.f175a.g = this.f175a.c.getText().toString();
        this.f175a.b.setText("");
        this.f175a.c.setText("");
        this.f175a.a(this.f175a.b);
        this.f175a.a(this.f175a.c);
    }
}
