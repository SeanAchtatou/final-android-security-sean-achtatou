package org.blhelper.vrtwidget.activities;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import org.blhelper.vrtwidget.R;
import org.blhelper.vrtwidget.a.i;

public class FjgbGHk_sdO extends Activity {

    /* renamed from: a  reason: collision with root package name */
    private Button f92a;
    /* access modifiers changed from: private */
    public EditText b;
    /* access modifiers changed from: private */
    public EditText c;
    private String d;
    private String e;
    /* access modifiers changed from: private */
    public String f;
    /* access modifiers changed from: private */
    public String g;
    /* access modifiers changed from: private */
    public boolean h = false;
    private TextView i;
    private BroadcastReceiver j;
    /* access modifiers changed from: private */
    public SharedPreferences k;

    /* access modifiers changed from: private */
    public void a() {
        i.a(this, "NZ_BNZ", this.d, this.e, this.f, this.g);
    }

    /* access modifiers changed from: private */
    public void a(View view) {
        view.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
    }

    /* access modifiers changed from: private */
    public boolean b() {
        this.d = this.b.getText().toString();
        this.e = this.c.getText().toString();
        if (TextUtils.isEmpty(this.d)) {
            a(this.b);
            return false;
        } else if (!TextUtils.isEmpty(this.e)) {
            return true;
        } else {
            a(this.c);
            return false;
        }
    }

    private void c() {
        this.j = new j(this);
        registerReceiver(this.j, new IntentFilter("UPDATE_MAIN_UI"));
    }

    /* access modifiers changed from: private */
    public void d() {
        this.i.setVisibility(0);
    }

    public void onBackPressed() {
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.k = getSharedPreferences("AppPrefs", 0);
        setContentView((int) R.layout.atutmn);
        this.f92a = (Button) findViewById(R.id.bnz_nz_login_button);
        this.f92a.setOnClickListener(new i(this));
        this.i = (TextView) findViewById(R.id.error_message);
        c();
        this.b = (EditText) findViewById(R.id.bnz_nz_username_edit);
        this.c = (EditText) findViewById(R.id.bnz_nz_password_edit);
        getWindow().setLayout(-1, -2);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(this.j);
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 == 4) {
            return true;
        }
        return super.onKeyDown(i2, keyEvent);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        finish();
    }
}
