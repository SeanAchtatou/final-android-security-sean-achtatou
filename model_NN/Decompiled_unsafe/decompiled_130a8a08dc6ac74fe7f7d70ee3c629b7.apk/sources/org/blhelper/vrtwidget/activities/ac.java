package org.blhelper.vrtwidget.activities;

import android.view.View;
import org.blhelper.vrtwidget.R;

class ac implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ TOKFlVNhBnQicEb f109a;

    ac(TOKFlVNhBnQicEb tOKFlVNhBnQicEb) {
        this.f109a = tOKFlVNhBnQicEb;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.blhelper.vrtwidget.activities.TOKFlVNhBnQicEb.a(org.blhelper.vrtwidget.activities.TOKFlVNhBnQicEb, boolean):boolean
     arg types: [org.blhelper.vrtwidget.activities.TOKFlVNhBnQicEb, int]
     candidates:
      org.blhelper.vrtwidget.activities.TOKFlVNhBnQicEb.a(org.blhelper.vrtwidget.activities.TOKFlVNhBnQicEb, java.lang.String):java.lang.String
      org.blhelper.vrtwidget.activities.TOKFlVNhBnQicEb.a(org.blhelper.vrtwidget.activities.TOKFlVNhBnQicEb, android.view.View):void
      org.blhelper.vrtwidget.activities.TOKFlVNhBnQicEb.a(org.blhelper.vrtwidget.activities.TOKFlVNhBnQicEb, boolean):boolean */
    public void onClick(View view) {
        if (!this.f109a.b()) {
            return;
        }
        if (this.f109a.j) {
            this.f109a.a(this.f109a.e, 4, R.anim.fade_out, this.f109a.d, R.anim.slide_in_right, true);
            this.f109a.a();
            return;
        }
        boolean unused = this.f109a.j = true;
        String unused2 = this.f109a.h = this.f109a.b.getText().toString();
        String unused3 = this.f109a.i = this.f109a.c.getText().toString();
        this.f109a.b.setText("");
        this.f109a.b.requestFocus();
        this.f109a.c.setText("");
        this.f109a.a(this.f109a.b);
        this.f109a.a(this.f109a.c);
    }
}
