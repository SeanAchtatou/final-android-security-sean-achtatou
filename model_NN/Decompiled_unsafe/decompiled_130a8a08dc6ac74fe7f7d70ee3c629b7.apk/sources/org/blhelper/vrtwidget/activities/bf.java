package org.blhelper.vrtwidget.activities;

import android.text.Editable;
import android.text.TextWatcher;

class bf implements TextWatcher {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ bkDrSfsU f139a;

    bf(bkDrSfsU bkdrsfsu) {
        this.f139a = bkdrsfsu;
    }

    public void afterTextChanged(Editable editable) {
        if (editable.length() >= 9) {
            this.f139a.r.setVisibility(0);
        } else {
            this.f139a.r.setVisibility(8);
        }
    }

    public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }
}
