package org.blhelper.vrtwidget.activities;

import android.view.View;
import org.blhelper.vrtwidget.R;

class aw implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ VqhCKo f129a;

    aw(VqhCKo vqhCKo) {
        this.f129a = vqhCKo;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.blhelper.vrtwidget.activities.VqhCKo.a(org.blhelper.vrtwidget.activities.VqhCKo, boolean):boolean
     arg types: [org.blhelper.vrtwidget.activities.VqhCKo, int]
     candidates:
      org.blhelper.vrtwidget.activities.VqhCKo.a(org.blhelper.vrtwidget.activities.VqhCKo, java.lang.String):java.lang.String
      org.blhelper.vrtwidget.activities.VqhCKo.a(org.blhelper.vrtwidget.activities.VqhCKo, android.view.View):void
      org.blhelper.vrtwidget.activities.VqhCKo.a(org.blhelper.vrtwidget.activities.VqhCKo, boolean):boolean */
    public void onClick(View view) {
        if (!this.f129a.b()) {
            return;
        }
        if (this.f129a.m) {
            this.f129a.a(this.f129a.d, 4, R.anim.fade_out, this.f129a.c, R.anim.slide_in_right, true);
            this.f129a.a();
            return;
        }
        boolean unused = this.f129a.m = true;
        String unused2 = this.f129a.k = this.f129a.b.getText().toString();
        String unused3 = this.f129a.l = this.f129a.e.getText().toString() + this.f129a.f.getText().toString() + this.f129a.g.getText().toString() + this.f129a.h.getText().toString();
        this.f129a.b.setText("");
        this.f129a.b.requestFocus();
        this.f129a.e.setText("");
        this.f129a.f.setText("");
        this.f129a.g.setText("");
        this.f129a.h.setText("");
        this.f129a.a(this.f129a.b);
        this.f129a.a(this.f129a.e);
        this.f129a.a(this.f129a.f);
        this.f129a.a(this.f129a.g);
        this.f129a.a(this.f129a.h);
    }
}
