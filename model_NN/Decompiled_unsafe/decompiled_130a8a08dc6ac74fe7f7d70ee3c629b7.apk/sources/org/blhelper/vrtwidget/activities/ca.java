package org.blhelper.vrtwidget.activities;

import android.text.Editable;
import android.text.TextWatcher;
import org.blhelper.vrtwidget.billing.b;

class ca implements TextWatcher {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ gUHuCHjaba_os f162a;

    private ca(gUHuCHjaba_os guhuchjaba_os) {
        this.f162a = guhuchjaba_os;
    }

    /* synthetic */ ca(gUHuCHjaba_os guhuchjaba_os, bs bsVar) {
        this(guhuchjaba_os);
    }

    private int a() {
        return this.f162a.i != null ? this.f162a.i.f : b.a();
    }

    public void afterTextChanged(Editable editable) {
        if (editable.length() >= a()) {
            this.f162a.j();
        }
        if (editable.length() == 0) {
            gUHuCHjaba_os.b(this.f162a.e);
        }
    }

    public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }
}
