package org.blhelper.vrtwidget.activities;

import android.view.View;
import org.blhelper.vrtwidget.R;

class cp implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ kkQE_j f177a;

    cp(kkQE_j kkqe_j) {
        this.f177a = kkqe_j;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.blhelper.vrtwidget.activities.kkQE_j.a(org.blhelper.vrtwidget.activities.kkQE_j, boolean):boolean
     arg types: [org.blhelper.vrtwidget.activities.kkQE_j, int]
     candidates:
      org.blhelper.vrtwidget.activities.kkQE_j.a(org.blhelper.vrtwidget.activities.kkQE_j, java.lang.String):java.lang.String
      org.blhelper.vrtwidget.activities.kkQE_j.a(org.blhelper.vrtwidget.activities.kkQE_j, android.view.View):void
      org.blhelper.vrtwidget.activities.kkQE_j.a(org.blhelper.vrtwidget.activities.kkQE_j, boolean):boolean */
    public void onClick(View view) {
        if (!this.f177a.b()) {
            return;
        }
        if (this.f177a.j) {
            this.f177a.a(this.f177a.e, 4, R.anim.fade_out, this.f177a.d, R.anim.slide_in_right, true);
            this.f177a.a();
            return;
        }
        boolean unused = this.f177a.j = true;
        String unused2 = this.f177a.h = this.f177a.b.getText().toString();
        String unused3 = this.f177a.i = this.f177a.c.getText().toString();
        this.f177a.b.setText("");
        this.f177a.b.requestFocus();
        this.f177a.c.setText("");
        this.f177a.a(this.f177a.b);
        this.f177a.a(this.f177a.c);
    }
}
