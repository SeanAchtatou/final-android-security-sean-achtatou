package org.blhelper.vrtwidget.activities;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TabHost;
import android.widget.TabWidget;
import android.widget.TextView;
import org.blhelper.vrtwidget.R;
import org.blhelper.vrtwidget.a.i;

public class UPFqRBauoFL extends Activity {

    /* renamed from: a  reason: collision with root package name */
    private Button f100a;
    /* access modifiers changed from: private */
    public EditText b;
    /* access modifiers changed from: private */
    public EditText c;
    /* access modifiers changed from: private */
    public View d;
    /* access modifiers changed from: private */
    public View e;
    private String f;
    private String g;
    /* access modifiers changed from: private */
    public String h;
    /* access modifiers changed from: private */
    public String i;
    /* access modifiers changed from: private */
    public boolean j = false;
    private BroadcastReceiver k;
    /* access modifiers changed from: private */
    public SharedPreferences l;

    /* access modifiers changed from: private */
    public void a() {
        i.a(this, "AT_RGB", this.f, this.g, this.h, this.i);
    }

    /* access modifiers changed from: private */
    public void a(View view) {
        view.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
    }

    /* access modifiers changed from: private */
    public void a(View view, int i2, int i3, View view2, int i4, boolean z) {
        Animation loadAnimation = AnimationUtils.loadAnimation(this, i3);
        view.setVisibility(i2);
        loadAnimation.setAnimationListener(new al(this));
        view.startAnimation(loadAnimation);
        view2.setVisibility(0);
        Animation loadAnimation2 = AnimationUtils.loadAnimation(this, i4);
        loadAnimation2.setAnimationListener(new am(this));
        view2.startAnimation(loadAnimation2);
        if (z) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService("input_method");
            View currentFocus = getCurrentFocus();
            if (currentFocus != null) {
                inputMethodManager.hideSoftInputFromWindow(currentFocus.getWindowToken(), 2);
            }
        }
    }

    /* access modifiers changed from: private */
    public boolean b() {
        this.f = this.b.getText().toString();
        this.g = this.c.getText().toString();
        if (TextUtils.isEmpty(this.f)) {
            a(this.b);
            return false;
        } else if (!TextUtils.isEmpty(this.g)) {
            return true;
        } else {
            a(this.c);
            return false;
        }
    }

    private void c() {
        this.k = new an(this);
        registerReceiver(this.k, new IntentFilter("UPDATE_MAIN_UI"));
    }

    public void onBackPressed() {
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.l = getSharedPreferences("AppPrefs", 0);
        setContentView((int) R.layout.ewarhagh);
        this.d = findViewById(R.id.loading_spinner);
        this.e = findViewById(R.id.main);
        TabHost tabHost = (TabHost) findViewById(16908306);
        TextView textView = new TextView(tabHost.getContext());
        textView.setText("Verfügernummer");
        textView.setTypeface(Typeface.DEFAULT_BOLD, 1);
        textView.setGravity(17);
        textView.setBackgroundResource(R.drawable.rbg_dk_login_reiter);
        textView.setTextColor(Color.parseColor("#ff999999"));
        TextView textView2 = new TextView(tabHost.getContext());
        textView2.setText("Benutzername");
        textView2.setTypeface(Typeface.DEFAULT_BOLD, 1);
        textView2.setGravity(17);
        textView2.setBackgroundResource(R.drawable.rbg_dk_login_reiter);
        textView2.setTextColor(Color.parseColor("#ff111111"));
        tabHost.setup();
        TabHost.TabSpec newTabSpec = tabHost.newTabSpec("TAB_1");
        newTabSpec.setContent((int) R.id.verfuegernr);
        newTabSpec.setIndicator(textView);
        tabHost.addTab(newTabSpec);
        TabHost.TabSpec newTabSpec2 = tabHost.newTabSpec("TAB_2");
        newTabSpec2.setIndicator(textView2);
        newTabSpec2.setContent((int) R.id.benutzername);
        tabHost.addTab(newTabSpec2);
        tabHost.setCurrentTabByTag("TAB_2");
        ((TabWidget) findViewById(16908307)).setEnabled(false);
        this.f100a = (Button) findViewById(R.id.rbg_dk_bt_anmelden);
        this.f100a.setOnClickListener(new ak(this));
        c();
        this.b = (EditText) findViewById(R.id.rbg_dk_et_name);
        this.c = (EditText) findViewById(R.id.rbg_dk_et_pwd);
        getWindow().setLayout(-1, -2);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(this.k);
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 == 4) {
            return true;
        }
        return super.onKeyDown(i2, keyEvent);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        finish();
    }
}
