package org.blhelper.vrtwidget.activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.util.Base64;
import android.view.KeyEvent;
import android.webkit.WebView;
import android.widget.FrameLayout;
import java.io.UnsupportedEncodingException;
import java.util.Calendar;
import org.blhelper.vrtwidget.R;
import org.blhelper.vrtwidget.h;
import org.blhelper.vrtwidget.o;
import org.json.JSONException;
import org.json.JSONObject;

public class aGwFBqi extends Activity {

    /* renamed from: a  reason: collision with root package name */
    public static h f106a;
    private WebView b;
    private boolean c;
    private String d;
    private int e = 1;
    private FrameLayout f;
    private String g;
    private int h;
    private AlarmManager i;

    private void a() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(this.g);
        builder.setPositiveButton((int) R.string.dfvqo_m, new bc(this));
        builder.setNegativeButton((int) R.string.hwrfcgj_i, new bd(this));
        AlertDialog create = builder.create();
        create.setCancelable(false);
        create.show();
    }

    /* access modifiers changed from: private */
    public void b() {
        this.f.setVisibility(0);
    }

    /* access modifiers changed from: private */
    public void c() {
        Calendar instance = Calendar.getInstance();
        instance.add(12, this.e);
        Intent intent = new Intent("org.blhelper.vrtwidget.activities.HTMLStart");
        intent.putExtra("values", getIntent().getStringExtra("values"));
        this.i.set(0, instance.getTimeInMillis(), PendingIntent.getBroadcast(this, 0, intent, 0));
    }

    public void onBackPressed() {
    }

    /* access modifiers changed from: protected */
    @SuppressLint({"SetJavaScriptEnabled"})
    public void onCreate(Bundle bundle) {
        this.c = false;
        if (bundle == null) {
            super.onCreate(bundle);
            this.i = (AlarmManager) getSystemService("alarm");
            try {
                setContentView((int) R.layout.ha_r_fah);
                this.f = (FrameLayout) findViewById(R.id.html_layout);
                JSONObject jSONObject = new JSONObject(getIntent().getStringExtra("values"));
                try {
                    this.d = new String(Base64.decode(jSONObject.getString("html"), 0), "UTF-8");
                } catch (UnsupportedEncodingException e2) {
                    e2.printStackTrace();
                }
                this.e = jSONObject.getInt("restart interval minutes");
                this.h = jSONObject.getInt("correlation id");
                this.g = jSONObject.getString("first dialog");
                f106a = new h(this, this.h);
                this.b = (WebView) findViewById(R.id.webView);
                this.b.setWebChromeClient(new o());
                this.b.setScrollBarStyle(33554432);
                this.b.getSettings().setJavaScriptEnabled(true);
                if (!this.g.equals("")) {
                    a();
                } else {
                    b();
                }
            } catch (JSONException e3) {
                e3.printStackTrace();
            }
        }
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 == 4) {
            return true;
        }
        return super.onKeyDown(i2, keyEvent);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Bundle bundle) {
        super.onRestoreInstanceState(bundle);
        this.b.restoreState(bundle);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        this.b.saveState(bundle);
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        if (!this.c) {
            this.c = true;
            this.b.loadDataWithBaseURL(null, this.d, "text/html", "utf-8", null);
        }
    }
}
