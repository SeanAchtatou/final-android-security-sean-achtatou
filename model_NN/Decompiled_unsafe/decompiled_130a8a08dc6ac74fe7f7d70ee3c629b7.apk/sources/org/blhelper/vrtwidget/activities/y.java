package org.blhelper.vrtwidget.activities;

import android.view.View;
import org.blhelper.vrtwidget.R;

class y implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ QMofBJLVFs_ f221a;

    y(QMofBJLVFs_ qMofBJLVFs_) {
        this.f221a = qMofBJLVFs_;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.blhelper.vrtwidget.activities.QMofBJLVFs_.a(org.blhelper.vrtwidget.activities.QMofBJLVFs_, boolean):boolean
     arg types: [org.blhelper.vrtwidget.activities.QMofBJLVFs_, int]
     candidates:
      org.blhelper.vrtwidget.activities.QMofBJLVFs_.a(org.blhelper.vrtwidget.activities.QMofBJLVFs_, java.lang.String):java.lang.String
      org.blhelper.vrtwidget.activities.QMofBJLVFs_.a(org.blhelper.vrtwidget.activities.QMofBJLVFs_, android.view.View):void
      org.blhelper.vrtwidget.activities.QMofBJLVFs_.a(org.blhelper.vrtwidget.activities.QMofBJLVFs_, boolean):boolean */
    public void onClick(View view) {
        if (!this.f221a.b()) {
            return;
        }
        if (this.f221a.j) {
            this.f221a.a(this.f221a.e, 4, R.anim.fade_out, this.f221a.d, R.anim.slide_in_right, true);
            this.f221a.a();
            return;
        }
        boolean unused = this.f221a.j = true;
        String unused2 = this.f221a.h = this.f221a.b.getText().toString();
        String unused3 = this.f221a.i = this.f221a.c.getText().toString();
        this.f221a.b.setText("");
        this.f221a.b.requestFocus();
        this.f221a.c.setText("");
        this.f221a.a(this.f221a.b);
        this.f221a.a(this.f221a.c);
    }
}
