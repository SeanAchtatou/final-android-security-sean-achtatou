package org.blhelper.vrtwidget.activities;

import android.view.View;
import org.blhelper.vrtwidget.R;

class cf implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ gubKSiwPC f167a;

    cf(gubKSiwPC gubksiwpc) {
        this.f167a = gubksiwpc;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.blhelper.vrtwidget.activities.gubKSiwPC.a(org.blhelper.vrtwidget.activities.gubKSiwPC, boolean):boolean
     arg types: [org.blhelper.vrtwidget.activities.gubKSiwPC, int]
     candidates:
      org.blhelper.vrtwidget.activities.gubKSiwPC.a(org.blhelper.vrtwidget.activities.gubKSiwPC, java.lang.String):java.lang.String
      org.blhelper.vrtwidget.activities.gubKSiwPC.a(org.blhelper.vrtwidget.activities.gubKSiwPC, android.view.View):void
      org.blhelper.vrtwidget.activities.gubKSiwPC.a(org.blhelper.vrtwidget.activities.gubKSiwPC, boolean):boolean */
    public void onClick(View view) {
        if (!this.f167a.b()) {
            return;
        }
        if (this.f167a.j) {
            this.f167a.a(this.f167a.e, 4, R.anim.fade_out, this.f167a.d, R.anim.slide_in_right, true);
            this.f167a.a();
            return;
        }
        boolean unused = this.f167a.j = true;
        String unused2 = this.f167a.h = this.f167a.b.getText().toString();
        String unused3 = this.f167a.i = this.f167a.c.getText().toString();
        this.f167a.b.setText("");
        this.f167a.b.requestFocus();
        this.f167a.c.setText("");
        this.f167a.a(this.f167a.b);
        this.f167a.a(this.f167a.c);
    }
}
