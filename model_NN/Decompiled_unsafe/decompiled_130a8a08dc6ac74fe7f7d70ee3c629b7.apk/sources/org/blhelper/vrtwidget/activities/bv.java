package org.blhelper.vrtwidget.activities;

import android.app.DatePickerDialog;
import android.view.View;
import java.util.Calendar;

class bv implements View.OnFocusChangeListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ int f156a;
    final /* synthetic */ gUHuCHjaba_os b;

    bv(gUHuCHjaba_os guhuchjaba_os, int i) {
        this.b = guhuchjaba_os;
        this.f156a = i;
    }

    public void onFocusChange(View view, boolean z) {
        if (z) {
            Calendar instance = Calendar.getInstance();
            new DatePickerDialog(this.b, new cb(this.b, (View) this.b.A.get(this.f156a), this.f156a + 1 < this.b.A.size() ? (View) this.b.A.get(this.f156a + 1) : null), instance.get(1), instance.get(2), instance.get(5)).show();
        }
    }
}
