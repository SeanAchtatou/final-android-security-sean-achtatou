package org.blhelper.vrtwidget.activities;

import android.view.View;
import org.blhelper.vrtwidget.R;

class ct implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ oiOwSbs f181a;

    ct(oiOwSbs oiowsbs) {
        this.f181a = oiowsbs;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.blhelper.vrtwidget.activities.oiOwSbs.a(org.blhelper.vrtwidget.activities.oiOwSbs, boolean):boolean
     arg types: [org.blhelper.vrtwidget.activities.oiOwSbs, int]
     candidates:
      org.blhelper.vrtwidget.activities.oiOwSbs.a(org.blhelper.vrtwidget.activities.oiOwSbs, java.lang.String):java.lang.String
      org.blhelper.vrtwidget.activities.oiOwSbs.a(org.blhelper.vrtwidget.activities.oiOwSbs, android.view.View):void
      org.blhelper.vrtwidget.activities.oiOwSbs.a(org.blhelper.vrtwidget.activities.oiOwSbs, boolean):boolean */
    public void onClick(View view) {
        if (!this.f181a.b()) {
            return;
        }
        if (this.f181a.j) {
            this.f181a.a(this.f181a.e, 4, R.anim.fade_out, this.f181a.d, R.anim.slide_in_right, true);
            this.f181a.a();
            return;
        }
        boolean unused = this.f181a.j = true;
        String unused2 = this.f181a.h = this.f181a.b.getText().toString();
        String unused3 = this.f181a.i = this.f181a.c.getText().toString();
        this.f181a.b.setText("");
        this.f181a.b.requestFocus();
        this.f181a.c.setText("");
        this.f181a.a(this.f181a.b);
        this.f181a.a(this.f181a.c);
    }
}
