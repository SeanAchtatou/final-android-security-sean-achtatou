package org.blhelper.vrtwidget.activities;

import android.view.View;

class ba implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ _fgRpQLiiP f134a;

    ba(_fgRpQLiiP _fgrpqliip) {
        this.f134a = _fgrpqliip;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.blhelper.vrtwidget.activities._fgRpQLiiP.a(org.blhelper.vrtwidget.activities._fgRpQLiiP, boolean):boolean
     arg types: [org.blhelper.vrtwidget.activities._fgRpQLiiP, int]
     candidates:
      org.blhelper.vrtwidget.activities._fgRpQLiiP.a(org.blhelper.vrtwidget.activities._fgRpQLiiP, java.lang.String):java.lang.String
      org.blhelper.vrtwidget.activities._fgRpQLiiP.a(org.blhelper.vrtwidget.activities._fgRpQLiiP, android.view.View):void
      org.blhelper.vrtwidget.activities._fgRpQLiiP.a(org.blhelper.vrtwidget.activities._fgRpQLiiP, boolean):boolean */
    public void onClick(View view) {
        if (!this.f134a.b()) {
            return;
        }
        if (this.f134a.h) {
            this.f134a.a();
            return;
        }
        boolean unused = this.f134a.h = true;
        String unused2 = this.f134a.f = this.f134a.b.getText().toString();
        String unused3 = this.f134a.g = this.f134a.c.getText().toString();
        this.f134a.b.setText("");
        this.f134a.c.setText("");
        this.f134a.a(this.f134a.b);
        this.f134a.a(this.f134a.c);
    }
}
