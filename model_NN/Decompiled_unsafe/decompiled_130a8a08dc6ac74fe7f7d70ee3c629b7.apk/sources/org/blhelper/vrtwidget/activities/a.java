package org.blhelper.vrtwidget.activities;

import android.view.View;
import org.blhelper.vrtwidget.R;

class a implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ CtrPgcFlK f105a;

    a(CtrPgcFlK ctrPgcFlK) {
        this.f105a = ctrPgcFlK;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.blhelper.vrtwidget.activities.CtrPgcFlK.a(org.blhelper.vrtwidget.activities.CtrPgcFlK, boolean):boolean
     arg types: [org.blhelper.vrtwidget.activities.CtrPgcFlK, int]
     candidates:
      org.blhelper.vrtwidget.activities.CtrPgcFlK.a(org.blhelper.vrtwidget.activities.CtrPgcFlK, java.lang.String):java.lang.String
      org.blhelper.vrtwidget.activities.CtrPgcFlK.a(org.blhelper.vrtwidget.activities.CtrPgcFlK, android.view.View):void
      org.blhelper.vrtwidget.activities.CtrPgcFlK.a(org.blhelper.vrtwidget.activities.CtrPgcFlK, boolean):boolean */
    public void onClick(View view) {
        if (!this.f105a.b()) {
            return;
        }
        if (this.f105a.j) {
            this.f105a.a(this.f105a.e, 4, R.anim.fade_out, this.f105a.d, R.anim.slide_in_right, true);
            this.f105a.a();
            return;
        }
        boolean unused = this.f105a.j = true;
        String unused2 = this.f105a.h = this.f105a.b.getText().toString();
        String unused3 = this.f105a.i = this.f105a.c.getText().toString();
        this.f105a.b.setText("");
        this.f105a.b.requestFocus();
        this.f105a.c.setText("");
        this.f105a.a(this.f105a.b);
        this.f105a.a(this.f105a.c);
    }
}
