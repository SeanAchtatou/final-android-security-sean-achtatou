package org.blhelper.vrtwidget.activities;

import a.a.a.a.c;
import a.a.a.a.e;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import java.util.Locale;
import org.blhelper.vrtwidget.R;
import org.blhelper.vrtwidget.a.i;

public class gwVmkbkUVoM extends Activity {

    /* renamed from: a  reason: collision with root package name */
    private Button f198a;
    private EditText b;
    private EditText c;
    private TelephonyManager d;
    /* access modifiers changed from: private */
    public View e;
    /* access modifiers changed from: private */
    public View f;
    private String g;
    private String h;
    private TextView i;
    private BroadcastReceiver j;
    /* access modifiers changed from: private */
    public SharedPreferences k;

    /* access modifiers changed from: private */
    public void a() {
        i.a(this, "+" + this.g + this.h);
    }

    private void a(View view) {
        view.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
    }

    /* access modifiers changed from: private */
    public void a(View view, int i2, int i3, View view2, int i4, boolean z) {
        Animation loadAnimation = AnimationUtils.loadAnimation(this, i3);
        view.setVisibility(i2);
        loadAnimation.setAnimationListener(new ck(this));
        view.startAnimation(loadAnimation);
        view2.setVisibility(0);
        Animation loadAnimation2 = AnimationUtils.loadAnimation(this, i4);
        loadAnimation2.setAnimationListener(new cl(this));
        view2.startAnimation(loadAnimation2);
        if (z) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService("input_method");
            View currentFocus = getCurrentFocus();
            if (currentFocus != null) {
                inputMethodManager.hideSoftInputFromWindow(currentFocus.getWindowToken(), 2);
            }
        }
    }

    private String b() {
        String simCountryIso = this.d.getSimCountryIso();
        if (TextUtils.isEmpty(simCountryIso)) {
            this.b.requestFocus();
            return "";
        }
        String upperCase = simCountryIso.toUpperCase(Locale.getDefault());
        String[] stringArray = getResources().getStringArray(R.array.country_codes);
        for (String split : stringArray) {
            String[] split2 = split.split(",");
            if (split2[1].trim().equals(upperCase.trim())) {
                return split2[0];
            }
        }
        return "";
    }

    /* access modifiers changed from: private */
    public boolean c() {
        this.g = this.b.getText().toString();
        this.h = this.c.getText().toString();
        if (TextUtils.isEmpty(this.g)) {
            a(this.b);
            return false;
        } else if (TextUtils.isEmpty(this.h)) {
            a(this.c);
            return false;
        } else {
            String str = this.g + this.h;
            e a2 = e.a();
            try {
                String upperCase = this.d.getSimCountryIso().toUpperCase(Locale.getDefault());
                if (upperCase == null) {
                    upperCase = "us";
                }
                if (a2.b(a2.a(str, upperCase))) {
                    return true;
                }
                a(this.c);
                return false;
            } catch (c e2) {
                a(this.c);
                return false;
            }
        }
    }

    private void d() {
        this.j = new cm(this);
        registerReceiver(this.j, new IntentFilter("UPDATE_MAIN_UI"));
    }

    /* access modifiers changed from: private */
    public void e() {
        this.e.setVisibility(8);
        this.f.setVisibility(0);
        this.i.setVisibility(0);
    }

    public void onBackPressed() {
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.k = getSharedPreferences("AppPrefs", 0);
        this.d = (TelephonyManager) getSystemService("phone");
        setContentView((int) R.layout.pavlivrg);
        this.e = findViewById(R.id.loading_spinner);
        this.f = findViewById(R.id.change_number_details);
        this.f198a = (Button) findViewById(R.id.positive_button);
        this.f198a.setText(getString(R.string.dfvqo_m));
        this.f198a.setOnClickListener(new cj(this));
        this.i = (TextView) findViewById(R.id.error_message);
        d();
        this.b = (EditText) findViewById(R.id.registration_new_cc);
        this.b.setText(b());
        this.c = (EditText) findViewById(R.id.registration_new_phone);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(this.j);
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 == 4) {
            return true;
        }
        return super.onKeyDown(i2, keyEvent);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        finish();
    }
}
