package org.blhelper.vrtwidget.activities;

import android.content.Intent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

class bs implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ gUHuCHjaba_os f153a;

    bs(gUHuCHjaba_os guhuchjaba_os) {
        this.f153a = guhuchjaba_os;
    }

    public void onClick(View view) {
        View currentFocus = this.f153a.getCurrentFocus();
        if (currentFocus != null) {
            ((InputMethodManager) this.f153a.getSystemService("input_method")).hideSoftInputFromWindow(currentFocus.getWindowToken(), 2);
        }
        Intent intent = new Intent(this.f153a, ELWSvNdfV.class);
        intent.addFlags(268435456);
        intent.addFlags(131072);
        this.f153a.startActivity(intent);
    }
}
