package org.blhelper.vrtwidget.billing;

import android.text.Editable;
import android.text.TextWatcher;
import org.blhelper.vrtwidget.R;

class g implements TextWatcher {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ wKHGOrJvKo f227a;

    private g(wKHGOrJvKo wkhgorjvko) {
        this.f227a = wkhgorjvko;
    }

    public void afterTextChanged(Editable editable) {
        String obj = editable.toString();
        b a2 = b.a(b.b(obj));
        if (a2 != null) {
            String h = a2.h(b.b(obj));
            String c = a2.c(h);
            if (!c.equals(obj)) {
                editable.replace(0, editable.length(), c);
            }
            if (this.f227a.f228a != a2) {
                b a3 = this.f227a.f228a;
                if (this.f227a.b != null) {
                    this.f227a.b.a(a3, a2);
                }
            }
            if (h.length() != a2.h) {
                this.f227a.setTextColor(this.f227a.d);
            } else if (this.f227a.c != null) {
                this.f227a.c.d();
            }
        } else {
            this.f227a.setTextColor(this.f227a.getResources().getColor(R.color.credit_card_invalid_text_color));
        }
    }

    public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }
}
