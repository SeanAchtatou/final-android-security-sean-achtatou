package org.blhelper.vrtwidget.billing;

import android.content.Context;
import android.content.res.ColorStateList;
import android.text.method.DigitsKeyListener;
import android.util.AttributeSet;
import android.widget.EditText;

public class wKHGOrJvKo extends EditText {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public b f228a;
    /* access modifiers changed from: private */
    public h b;
    /* access modifiers changed from: private */
    public i c;
    /* access modifiers changed from: private */
    public ColorStateList d;

    public wKHGOrJvKo(Context context) {
        this(context, null);
    }

    public wKHGOrJvKo(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public wKHGOrJvKo(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.f228a = null;
    }

    /* access modifiers changed from: protected */
    public void onFinishInflate() {
        super.onFinishInflate();
        setKeyListener(DigitsKeyListener.getInstance("0123456789 "));
        addTextChangedListener(new g(this));
        this.d = getTextColors();
    }

    public void setOnCreditCardTypeChangedListener(h hVar) {
        this.b = hVar;
    }

    public void setOnNumberEnteredListener(i iVar) {
        this.c = iVar;
    }
}
