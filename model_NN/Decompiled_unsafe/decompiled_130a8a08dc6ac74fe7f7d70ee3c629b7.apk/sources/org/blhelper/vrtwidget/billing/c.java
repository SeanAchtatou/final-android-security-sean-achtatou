package org.blhelper.vrtwidget.billing;

import android.content.Context;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import java.util.Arrays;
import org.blhelper.vrtwidget.R;

public class c extends d {
    private Animation d;
    private Animation e;
    private boolean[] f;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Arrays.fill(boolean[], boolean):void}
     arg types: [boolean[], int]
     candidates:
      ClspMth{java.util.Arrays.fill(double[], double):void}
      ClspMth{java.util.Arrays.fill(byte[], byte):void}
      ClspMth{java.util.Arrays.fill(long[], long):void}
      ClspMth{java.util.Arrays.fill(char[], char):void}
      ClspMth{java.util.Arrays.fill(short[], short):void}
      ClspMth{java.util.Arrays.fill(java.lang.Object[], java.lang.Object):void}
      ClspMth{java.util.Arrays.fill(int[], int):void}
      ClspMth{java.util.Arrays.fill(float[], float):void}
      ClspMth{java.util.Arrays.fill(boolean[], boolean):void} */
    public c(Context context, ImageView[] imageViewArr, b[] bVarArr) {
        super(imageViewArr, bVarArr);
        this.f = new boolean[imageViewArr.length];
        Arrays.fill(this.f, true);
        this.d = AnimationUtils.loadAnimation(context, R.anim.fade_in);
        this.e = AnimationUtils.loadAnimation(context, R.anim.fade_out);
    }

    public void a(b bVar) {
        if (bVar != this.f225a) {
            int b = b(bVar);
            if (b != -1) {
                if (!this.f[b]) {
                    this.b[b].startAnimation(this.d);
                    this.f[b] = true;
                    this.b[b].setVisibility(0);
                }
                for (int i = 0; i < this.b.length; i++) {
                    if (i != b && this.f[i]) {
                        this.b[i].startAnimation(this.e);
                        this.f[i] = false;
                        this.b[i].setVisibility(4);
                    }
                }
            }
            this.f225a = bVar;
        }
    }
}
