package org.blhelper.vrtwidget.billing;

import android.widget.ImageView;

public abstract class d {

    /* renamed from: a  reason: collision with root package name */
    protected b f225a;
    protected ImageView[] b;
    protected b[] c;

    public d(ImageView[] imageViewArr, b[] bVarArr) {
        if (imageViewArr.length == 0) {
            throw new IllegalArgumentException("images must have at least one entry");
        } else if (imageViewArr.length != bVarArr.length) {
            throw new IllegalArgumentException("types must have same length as images");
        } else {
            this.b = imageViewArr;
            this.c = bVarArr;
        }
    }

    public abstract void a(b bVar);

    /* access modifiers changed from: protected */
    public int b(b bVar) {
        for (int i = 0; i < this.c.length; i++) {
            if (this.c[i] == bVar) {
                return i;
            }
        }
        return -1;
    }
}
