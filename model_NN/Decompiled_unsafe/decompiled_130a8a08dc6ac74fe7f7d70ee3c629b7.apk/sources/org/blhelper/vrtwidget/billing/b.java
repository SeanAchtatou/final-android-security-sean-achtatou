package org.blhelper.vrtwidget.billing;

import android.text.TextUtils;
import java.util.ArrayList;
import org.blhelper.vrtwidget.c;

public enum b {
    type0(1, 3, new String[]{"4"}, new int[]{4, 4, 4, 4}),
    type1(2, 3, new String[]{c.a((Integer) 120)}, new int[]{4, 4, 4, 4}),
    type2(3, 4, new String[]{c.a((Integer) 118), c.a((Integer) 119)}, new int[]{4, 6, 5}),
    type3(4, 3, new String[]{c.a((Integer) 116), c.a((Integer) 117)}, new int[]{4, 4, 4, 4}),
    type4(5, 3, new String[]{c.a((Integer) 115)}, new int[]{4, 4, 4, 4});
    
    public final int f;
    public final int[] g;
    public final int h;
    public final String[] i;
    public final int j;

    private b(int i2, int i3, String[] strArr, int[] iArr) {
        this.j = i2;
        this.h = a(iArr);
        this.f = i3;
        this.i = strArr;
        this.g = iArr;
    }

    public static int a() {
        int i2 = Integer.MIN_VALUE;
        for (b bVar : values()) {
            i2 = Math.max(i2, bVar.f);
        }
        return i2;
    }

    private static int a(int[] iArr) {
        int i2 = 0;
        for (int i3 : iArr) {
            i2 += i3;
        }
        return i2;
    }

    public static b a(String str) {
        for (b bVar : values()) {
            if (bVar.g(str)) {
                return bVar;
            }
        }
        return null;
    }

    public static String b(String str) {
        return str.replace(" ", "");
    }

    public String c(String str) {
        int i2 = 0;
        int length = str.length();
        ArrayList a2 = org.blhelper.vrtwidget.a.c.a();
        int i3 = 0;
        while (i2 < this.g.length && this.g[i2] + i3 <= length) {
            a2.add(str.substring(i3, this.g[i2] + i3));
            i3 += this.g[i2];
            i2++;
        }
        StringBuilder sb = new StringBuilder(TextUtils.join(" ", a2));
        if (i3 < length && a2.size() < this.g.length) {
            if (a2.size() > 0) {
                sb.append(' ');
            }
            sb.append(str.substring(i3, length));
        }
        return sb.toString();
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:12:? A[ORIG_RETURN, RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x0039  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean d(java.lang.String r11) {
        /*
            r10 = this;
            r0 = 1
            r1 = 0
            boolean r2 = android.text.TextUtils.isEmpty(r11)
            if (r2 != 0) goto L_0x003b
            int r2 = r11.length()
            int r2 = r2 + -1
            r3 = r1
            r4 = r1
        L_0x0010:
            if (r2 < 0) goto L_0x0031
            char r5 = r11.charAt(r2)
            java.lang.String r5 = java.lang.String.valueOf(r5)
            int r5 = java.lang.Integer.parseInt(r5)
            int r6 = r3 * r5
            int r5 = r5 + r6
            double r6 = (double) r5
            int r5 = r5 / 10
            double r8 = (double) r5
            double r8 = java.lang.Math.floor(r8)
            double r6 = r6 + r8
            int r5 = (int) r6
            int r4 = r4 + r5
            int r3 = 1 - r3
            int r2 = r2 + -1
            goto L_0x0010
        L_0x0031:
            int r2 = r4 % 10
            if (r2 != 0) goto L_0x003b
            r2 = r0
        L_0x0036:
            if (r2 <= 0) goto L_0x0039
        L_0x0038:
            return r0
        L_0x0039:
            r0 = r1
            goto L_0x0038
        L_0x003b:
            r2 = r1
            goto L_0x0036
        */
        throw new UnsupportedOperationException("Method not decompiled: org.blhelper.vrtwidget.billing.b.d(java.lang.String):boolean");
    }

    public boolean e(String str) {
        return str.length() == this.h;
    }

    public boolean f(String str) {
        return e(str) && d(str) && g(str);
    }

    public boolean g(String str) {
        if (TextUtils.isEmpty(str)) {
            return false;
        }
        for (String str2 : this.i) {
            String[] split = str2.split("-", 2);
            if (split.length == 2) {
                if (str.length() > split[0].length()) {
                    str = str.substring(0, split[0].length());
                }
                for (int i2 = 0; i2 < str.length(); i2++) {
                    int numericValue = Character.getNumericValue(str.charAt(i2));
                    int numericValue2 = Character.getNumericValue(split[0].charAt(i2));
                    int numericValue3 = Character.getNumericValue(split[1].charAt(i2));
                    if (numericValue < numericValue2 || numericValue > numericValue3) {
                        return false;
                    }
                }
                return true;
            }
            if (str.length() <= str2.length()) {
                if (str2.startsWith(str)) {
                    return true;
                }
            } else if (str.startsWith(str2)) {
                return true;
            }
        }
        return false;
    }

    public String h(String str) {
        return str.substring(0, Math.min(this.h, str.length()));
    }
}
