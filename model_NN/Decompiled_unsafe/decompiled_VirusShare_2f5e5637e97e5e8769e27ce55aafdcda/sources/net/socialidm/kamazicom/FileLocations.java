package net.socialidm.kamazicom;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

class FileLocations {
    private String audios;
    private String docs;
    private boolean isWifiOnly = false;
    private String others;
    private String type;
    private String videos;

    FileLocations(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        if (preferences == null || preferences.equals("")) {
            Log.i(DownloadManagerActivity.TAG, "Null Values");
            return;
        }
        this.isWifiOnly = preferences.getBoolean("onlyWifi", false);
        this.audios = preferences.getString("audioPath", "/mnt/sdcard/IDM/audios");
        this.videos = preferences.getString("videoPath", "/mnt/sdcard/IDM/videos");
        this.docs = preferences.getString("docPath", "/mnt/sdcard/IDM/docs");
        this.others = preferences.getString("othersPath", "/mnt/sdcard/IDM/others");
        Log.i(DownloadManagerActivity.TAG, "Path: " + this.isWifiOnly + " : " + this.audios + " : " + this.videos + " : " + this.docs + " : " + this.others);
    }

    public String getAudios() {
        return this.audios;
    }

    public void setAudios(String audios2) {
        this.audios = audios2;
    }

    public String getVideos() {
        return this.videos;
    }

    public void setVideos(String videos2) {
        this.videos = videos2;
    }

    public String getDocs() {
        return this.docs;
    }

    public void setDocs(String docs2) {
        this.docs = docs2;
    }

    public String getOthers() {
        return this.others;
    }

    public void setOthers(String others2) {
        this.others = others2;
    }

    public boolean isWifiOnly() {
        return this.isWifiOnly;
    }

    public void setWifiOnly(boolean isWifiOnly2) {
        this.isWifiOnly = isWifiOnly2;
    }

    public String setSaveDir(String type2) {
        this.type = type2;
        return type2;
    }

    public String getSavedir() {
        if (this.type != null && this.type.equals("Audio")) {
            return this.audios;
        }
        if (this.type != null && this.type.equals("Video")) {
            return this.videos;
        }
        if (this.type != null && this.type.equals("Documents")) {
            return this.docs;
        }
        if (this.type == null || !this.type.equals("Others")) {
            return "/mnt/sdcard/IDM/";
        }
        return this.others;
    }
}
