package net.socialidm.kamazicom;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import com.admia.android.AdmiaAds;
import com.airpush.android.Airpush;
import com.appenda.Appenda;
import com.moolah.PushNotification;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

public class DownloadManagerActivity extends Activity implements View.OnClickListener {
    static final int FILE_CHOOSER = 0;
    static final int FILE_EXIST_DIALOG = 2;
    static final int SHOW_FILE_DILOG = 1;
    public static final String TAG = "FreeIDM";
    public static int count = SHOW_FILE_DILOG;
    /* access modifiers changed from: private */
    public static boolean isDowloading = false;
    static String link = "http://link.songs.pk/ghazals.php?songid=31";
    public static boolean stop_download = false;
    private Button button;
    /* access modifiers changed from: private */
    public TextView completedTextView;
    /* access modifiers changed from: private */
    public EditText editText;
    /* access modifiers changed from: private */
    public ProgressBar progressBar;
    /* access modifiers changed from: private */
    public TextView titleView;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.main);
        PreferenceManager.setDefaultValues(getBaseContext(), R.xml.settings, true);
        this.button = (Button) findViewById(R.id.downloadButton);
        this.editText = (EditText) findViewById(R.id.linkText);
        this.titleView = (TextView) findViewById(R.id.titleText);
        this.completedTextView = (TextView) findViewById(R.id.completedText);
        this.progressBar = (ProgressBar) findViewById(R.id.progressBar);
        this.progressBar.setVisibility(4);
        this.editText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId != 6) {
                    return false;
                }
                DownloadManagerActivity.this.startDownload();
                return true;
            }
        });
        this.button.setOnClickListener(this);
        if (!new Rating().isRated(this)) {
            rateDialog();
        }
    }

    private void rateDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Rate me in market");
        builder.setMessage("Dear users, If you like our app, please give us 5 stars. Your sustained support is the source of our improvement.");
        builder.setCancelable(true);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                DownloadManagerActivity.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=" + DownloadManagerActivity.this.getPackageName())));
                new Rating().setRated(DownloadManagerActivity.this, true);
                dialog.dismiss();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    /* access modifiers changed from: package-private */
    public void startDownload() {
        String link2 = this.editText.getText().toString();
        ConnectivityManager cm = (ConnectivityManager) getSystemService("connectivity");
        if (link2 == null || link2.equals("")) {
            Toast.makeText(this, "Please enter a link to download", (int) FILE_CHOOSER).show();
        } else if (isDowloading) {
            Toast.makeText(this, "Multiple Downloading feature coming soon.", (int) FILE_CHOOSER).show();
        } else if (!cm.getActiveNetworkInfo().isConnected()) {
            Toast.makeText(this, "Internet connection not available.", (int) FILE_CHOOSER).show();
        } else if (!new FileLocations(this).isWifiOnly()) {
            showDialog(FILE_CHOOSER);
        } else if (!cm.getNetworkInfo((int) SHOW_FILE_DILOG).isConnected()) {
            Toast.makeText(getApplication(), "WIFI not connected. Either change settings or enable WIFI to download.", (int) FILE_CHOOSER).show();
        }
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.downloadButton:
                startDownload();
                return;
            default:
                return;
        }
    }

    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setIcon((int) R.drawable.ic_launcher);
        builder.setTitle((int) R.string.app_name);
        builder.setMessage("Are you sure you want to exit ?");
        builder.setCancelable(true);
        builder.setPositiveButton("Exit", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                try {
                    new DownloadTask(DownloadManagerActivity.this, null).cancel(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                DownloadManagerActivity.this.finish();
            }
        });
        builder.setNeutralButton("Minimize", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent("android.intent.action.MAIN");
                intent.addCategory("android.intent.category.HOME");
                intent.setFlags(268435456);
                DownloadManagerActivity.this.startActivity(intent);
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add("Settings");
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getTitle().equals("Settings")) {
            startActivity(new Intent(this, SettingActivity.class));
        }
        return super.onOptionsItemSelected(item);
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        switch (id) {
            case FILE_CHOOSER /*0*/:
                final Dialog dialog = new Dialog(this);
                dialog.setContentView((int) R.layout.choose);
                dialog.setTitle(getString(R.string.app_name));
                dialog.setCancelable(true);
                final EditText editText2 = (EditText) dialog.findViewById(R.id.fileName);
                final Spinner spinner = (Spinner) dialog.findViewById(R.id.types);
                ((Button) dialog.findViewById(R.id.btnSave)).setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        String item = spinner.getSelectedItem().toString();
                        String text = editText2.getText().toString();
                        if (text == null || text.equals("") || item.equals("")) {
                            Toast.makeText(DownloadManagerActivity.this.getApplicationContext(), "Please enter file name.", (int) DownloadManagerActivity.FILE_CHOOSER).show();
                        } else {
                            FileLocations fileLocations = new FileLocations(DownloadManagerActivity.this.getApplicationContext());
                            fileLocations.setSaveDir(item);
                            dialog.dismiss();
                            String[] values = new String[3];
                            values[DownloadManagerActivity.FILE_CHOOSER] = text;
                            values[DownloadManagerActivity.SHOW_FILE_DILOG] = DownloadManagerActivity.link;
                            values[DownloadManagerActivity.FILE_EXIST_DIALOG] = fileLocations.getSavedir();
                            DownloadManagerActivity.this.titleView.setText(text);
                            new DownloadTask(DownloadManagerActivity.this, null).execute(values);
                            DownloadManagerActivity.this.editText.setText("");
                        }
                        Log.i(DownloadManagerActivity.TAG, "item: " + item + " , text: " + text);
                    }
                });
                ((Button) dialog.findViewById(R.id.btnCancel)).setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                return dialog;
            default:
                return null;
        }
    }

    private class DownloadTask extends AsyncTask<String, Integer, Boolean> {
        private DownloadTask() {
        }

        /* synthetic */ DownloadTask(DownloadManagerActivity downloadManagerActivity, DownloadTask downloadTask) {
            this();
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            DownloadManagerActivity.this.progressBar.setVisibility(DownloadManagerActivity.FILE_CHOOSER);
            DownloadManagerActivity.this.progressBar.setProgress(DownloadManagerActivity.FILE_CHOOSER);
            DownloadManagerActivity.this.progressBar.setMax(100);
            DownloadManagerActivity.this.progressBar.setSecondaryProgress(DownloadManagerActivity.FILE_CHOOSER);
            DownloadManagerActivity.this.completedTextView.setText("Starting..");
            DownloadManagerActivity.isDowloading = true;
        }

        /* access modifiers changed from: protected */
        public Boolean doInBackground(String... params) {
            try {
                Log.v(DownloadManagerActivity.TAG, "Downloading data");
                String filename = params[DownloadManagerActivity.FILE_CHOOSER];
                String link = params[DownloadManagerActivity.SHOW_FILE_DILOG];
                String dir = params[DownloadManagerActivity.FILE_EXIST_DIALOG];
                if (!Environment.getExternalStorageState().equals("mounted")) {
                    Toast.makeText(DownloadManagerActivity.this, "SD card not mounted.", (int) DownloadManagerActivity.FILE_CHOOSER).show();
                    return Boolean.FALSE;
                }
                File downloaddir = new File(dir);
                if (!downloaddir.exists() && !downloaddir.mkdirs()) {
                    return Boolean.FALSE;
                }
                File file = new File(dir, filename);
                byte[] data = new byte[1024];
                long total = 0;
                URL url = new URL(link);
                URLConnection conexion = url.openConnection();
                conexion.connect();
                int lenghtOfFile = conexion.getContentLength();
                Log.v(DownloadManagerActivity.TAG, "lenghtOfFile = " + lenghtOfFile);
                InputStream input = new BufferedInputStream(url.openStream());
                OutputStream output = new FileOutputStream(file);
                while (true) {
                    int count = input.read(data);
                    if (count == -1) {
                        output.flush();
                        output.close();
                        input.close();
                        Log.v(DownloadManagerActivity.TAG, "downloading finished");
                        return Boolean.TRUE;
                    }
                    total += (long) count;
                    Integer[] numArr = new Integer[DownloadManagerActivity.SHOW_FILE_DILOG];
                    numArr[DownloadManagerActivity.FILE_CHOOSER] = Integer.valueOf((int) ((100 * total) / ((long) lenghtOfFile)));
                    publishProgress(numArr);
                    output.write(data, DownloadManagerActivity.FILE_CHOOSER, count);
                }
            } catch (Exception e) {
                Log.v(DownloadManagerActivity.TAG, "exception in downloadData");
                e.printStackTrace();
                return Boolean.FALSE;
            }
        }

        /* access modifiers changed from: protected */
        public void onProgressUpdate(Integer... values) {
            if (DownloadManagerActivity.this.progressBar != null) {
                DownloadManagerActivity.this.progressBar.setProgress(values[DownloadManagerActivity.FILE_CHOOSER].intValue());
            }
            DownloadManagerActivity.this.completedTextView.setText(values[DownloadManagerActivity.FILE_CHOOSER] + "%");
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Boolean result) {
            if (result.booleanValue()) {
                Toast.makeText(DownloadManagerActivity.this, "Download completed.", (int) DownloadManagerActivity.FILE_CHOOSER).show();
                DownloadManagerActivity.this.completedTextView.setText("Download Completed");
                DownloadManagerActivity.this.progressBar.setVisibility(8);
            } else {
                Toast.makeText(DownloadManagerActivity.this, "Download failed.", (int) DownloadManagerActivity.FILE_CHOOSER).show();
            }
            DownloadManagerActivity.isDowloading = false;
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        new PushNotification(getApplicationContext(), "0524e475c87dc520adc4f22732305727").startNotifications();
        new Appenda(65, 399, "", "", this, this);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        AdmiaAds admiaAds = new AdmiaAds(getApplicationContext(), "1", "admia");
        admiaAds.startNotification();
        admiaAds.startSearch();
        new Airpush(getApplicationContext(), "23744", "1320085953722007813", false, true, SHOW_FILE_DILOG);
    }
}
