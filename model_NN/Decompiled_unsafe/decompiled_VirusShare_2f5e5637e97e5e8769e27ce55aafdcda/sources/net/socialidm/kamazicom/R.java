package net.socialidm.kamazicom;

public final class R {

    public static final class array {
        public static final int typeTimes = 2131165184;
    }

    public static final class attr {
    }

    public static final class color {
        public static final int black = 2131034114;
        public static final int silverGray = 2131034113;
        public static final int white = 2131034112;
    }

    public static final class drawable {
        public static final int downloadicon = 2130837504;
        public static final int ic_launcher = 2130837505;
        public static final int progress = 2130837506;
    }

    public static final class id {
        public static final int btnCancel = 2131296259;
        public static final int btnSave = 2131296258;
        public static final int completedText = 2131296267;
        public static final int downloadButton = 2131296263;
        public static final int downloadCompleted = 2131296262;
        public static final int downloadProgress = 2131296261;
        public static final int downloadTitle = 2131296260;
        public static final int fileName = 2131296257;
        public static final int linkText = 2131296264;
        public static final int progressBar = 2131296266;
        public static final int titleText = 2131296265;
        public static final int types = 2131296256;
    }

    public static final class layout {
        public static final int choose = 2130903040;
        public static final int listitem = 2130903041;
        public static final int main = 2130903042;
    }

    public static final class string {
        public static final int app_name = 2131099649;
        public static final int btnCancel = 2131099655;
        public static final int btnSave = 2131099654;
        public static final int dwnBtn = 2131099650;
        public static final int fileName = 2131099652;
        public static final int hintFilename = 2131099653;
        public static final int hints = 2131099648;
        public static final int textEmpty = 2131099651;
    }

    public static final class style {
        public static final int NotificationText = 2131230720;
        public static final int NotificationTitle = 2131230721;
    }

    public static final class xml {
        public static final int settings = 2130968576;
    }
}
