package net.socialidm.kamazicom;

import android.content.Context;
import android.content.SharedPreferences;

public class Rating {
    final String FIELD = "israted";
    final String FILE = "rating";

    /* access modifiers changed from: protected */
    public boolean isRated(Context context) {
        SharedPreferences preferences = context.getSharedPreferences("rating", 1);
        if (preferences.contains("israted")) {
            return preferences.getBoolean("israted", false);
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public boolean setRated(Context context, boolean israted) {
        SharedPreferences.Editor editor = context.getSharedPreferences("rating", 2).edit();
        editor.putBoolean("israted", israted);
        return editor.commit();
    }
}
