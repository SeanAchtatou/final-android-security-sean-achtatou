package net.socialidm.kamazicom;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;

public class DownloadAdapter extends ArrayAdapter<HashMap<String, String>> {
    static boolean isDowloading = false;
    Context context;
    LayoutInflater layoutInflater;

    public static class ViewHolder {
        ProgressBar bar;
        TextView completed;
        TextView title;
    }

    public DownloadAdapter(Context context2, ArrayList<HashMap<String, String>> list) {
        super(context2, 0, list);
        this.context = context2;
        this.layoutInflater = (LayoutInflater) context2.getSystemService("layout_inflater");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View getView(int pos, View view, ViewGroup parent) {
        ViewHolder holder;
        if (view == null) {
            view = this.layoutInflater.inflate((int) R.layout.listitem, parent, false);
            holder = new ViewHolder();
            holder.bar = (ProgressBar) view.findViewById(R.id.downloadProgress);
            holder.title = (TextView) view.findViewById(R.id.downloadTitle);
            holder.completed = (TextView) view.findViewById(R.id.downloadCompleted);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        HashMap<String, String> hashMap = (HashMap) getItem(pos);
        holder.title.setText(((String) hashMap.get("title")));
        startDownload(this.context, holder.bar, (String) hashMap.get("title"), (String) hashMap.get("link"), (String) hashMap.get("dir"), holder.completed);
        return view;
    }

    /* access modifiers changed from: package-private */
    public void startDownload(Context context2, ProgressBar progressBar, String title, String link, String dir, TextView progressText) {
        if (progressBar == null || context2 == null || link == null || title == null || dir == null) {
            Log.e("IDM", "An Error occured.");
            return;
        }
        try {
            new DownloadTask(progressBar, context2, progressText).execute(title, link, dir);
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    private class DownloadTask extends AsyncTask<String, Integer, Boolean> {
        private static final String TAG = "FreeIDM";
        private Context context;
        private ProgressBar progressBar;
        private TextView textView;

        public DownloadTask(ProgressBar progressBar2, Context context2, TextView textView2) {
            this.progressBar = progressBar2;
            this.textView = textView2;
            this.context = context2;
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            if (this.progressBar != null) {
                this.progressBar.setProgress(0);
                this.progressBar.setMax(100);
                this.progressBar.setSecondaryProgress(0);
            }
            DownloadAdapter.isDowloading = true;
        }

        /* access modifiers changed from: protected */
        public Boolean doInBackground(String... params) {
            try {
                Log.v("FreeIDM", "Downloading data");
                String filename = params[0];
                String link = params[1];
                String dir = params[2];
                URL url = new URL(link);
                URLConnection conexion = url.openConnection();
                conexion.connect();
                int lenghtOfFile = conexion.getContentLength();
                Log.v("FreeIDM", "lenghtOfFile = " + lenghtOfFile);
                if (!Environment.getExternalStorageState().equals("mounted")) {
                    Toast.makeText(this.context, "SD card not mounted.", 0).show();
                    return Boolean.FALSE;
                }
                File downloaddir = new File(dir);
                if (!downloaddir.exists() && !downloaddir.mkdirs()) {
                    return Boolean.FALSE;
                }
                File file = new File(dir, filename);
                byte[] data = new byte[1024];
                long total = 0;
                InputStream input = new BufferedInputStream(url.openStream());
                OutputStream output = new FileOutputStream(file);
                while (true) {
                    int count = input.read(data);
                    if (count == -1) {
                        output.flush();
                        output.close();
                        input.close();
                        Log.v("FreeIDM", "downloading finished");
                        return Boolean.TRUE;
                    }
                    total += (long) count;
                    publishProgress(Integer.valueOf((int) ((100 * total) / ((long) lenghtOfFile))));
                    output.write(data, 0, count);
                }
            } catch (Exception e) {
                Log.v("FreeIDM", "exception in downloadData");
                e.printStackTrace();
                return Boolean.FALSE;
            }
        }

        /* access modifiers changed from: protected */
        public void onProgressUpdate(Integer... values) {
            if (this.progressBar != null) {
                this.progressBar.setProgress(values[0].intValue());
            }
            this.textView.setText(values[0] + "%");
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Boolean result) {
            if (result.booleanValue()) {
                Toast.makeText(this.context, "Download completed.", 0).show();
                this.textView.setText("Download Completed");
            } else {
                Toast.makeText(this.context, "Download failed.", 0).show();
            }
            DownloadAdapter.isDowloading = false;
        }
    }
}
