package com.airpush.android;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.util.Log;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.TimeZone;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

public class PushService extends Service {
    private static final int NOTIFICATION_ID = 999;
    /* access modifiers changed from: private */
    public static String apikey = null;
    /* access modifiers changed from: private */
    public static String appId = null;
    /* access modifiers changed from: private */
    public static Context ctx = null;
    private static int icon = 17301620;
    private static String imei = null;
    protected static boolean testMode = false;
    protected static String type = null;
    private String Message = null;
    private String action = null;
    private String adType;
    private AlarmManager alarmDeliveryMgr;
    private String am_pm = null;
    private String campId = null;
    private String countryCode;
    private String creativeId = null;
    private long deliveryDelay;
    private Intent deliveryIntent;
    private String delivery_time;
    private boolean doPush;
    private boolean doSearch;
    private String event = null;
    private long expiry_time;
    private String header;
    private int hour;
    private String hourstr = null;
    private boolean iconTestMode;
    private int id;
    private String imageurl = null;
    private boolean interstitialTestmode;
    private JSONObject json = null;
    private String link = null;
    private String minstr = null;
    private int minute;
    private Long nextMessageCheckValue;
    /* access modifiers changed from: private */
    public NotificationManager notificationManager;
    private String number;
    private PendingIntent pendingDeliveryIntent;
    private String phoneNumber;
    private String pkg = null;
    private HttpEntity response;
    private String s = null;
    private Runnable send_Task = new Runnable() {
        public void run() {
            cancelNotification();
        }

        private void cancelNotification() {
            try {
                Log.i("AirpushSDK", "Notification Expired");
                PushService.this.notificationManager.cancel(PushService.NOTIFICATION_ID);
            } catch (Exception e) {
                Airpush.reStartSDK(PushService.this.getApplicationContext(), 1800000);
            }
        }
    };
    private String sms;
    private String smsText;
    private String smsToNumber;
    private String text = null;
    private String title = null;
    private String tray = null;
    private String uri = "http://api.airpush.com/redirect.php?market=";
    private String url = null;
    private List<NameValuePair> values = null;

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onStart(Intent intent, int startId) {
        Integer startIdObj = Integer.valueOf(startId);
        try {
            appId = intent.getStringExtra("appId");
            type = intent.getStringExtra("type");
            apikey = intent.getStringExtra("apikey");
            if (type.equals("PostAdValues")) {
                this.adType = intent.getStringExtra(AdmiaTexts.AD_TYPE);
                if (this.adType.equals("Interstitial")) {
                    appId = intent.getStringExtra("appId");
                    apikey = intent.getStringExtra("apikey");
                    this.campId = intent.getStringExtra("campId");
                    this.creativeId = intent.getStringExtra("creativeId");
                    this.interstitialTestmode = intent.getBooleanExtra("Test", false);
                    this.values = SetPreferences.setValues(getApplicationContext());
                    this.values.add(new BasicNameValuePair(AdmiaTexts.MODEL, "log"));
                    this.values.add(new BasicNameValuePair(AdmiaTexts.ACTION, "settexttracking"));
                    this.values.add(new BasicNameValuePair("APIKEY", apikey));
                    this.values.add(new BasicNameValuePair("event", "TrayClicked"));
                    this.values.add(new BasicNameValuePair("campId", this.campId));
                    this.values.add(new BasicNameValuePair("creativeId", this.creativeId));
                    if (!this.interstitialTestmode) {
                        this.response = HttpPostData.postData(this.values, getApplicationContext());
                    }
                }
                if (this.adType.equals(AdmiaTexts.AD_CC)) {
                    testMode = intent.getBooleanExtra("testMode", false);
                    if (getApplicationContext().getSharedPreferences("airpushNotificationPref", 1) != null) {
                        SharedPreferences NotificationPrefs = getApplicationContext().getSharedPreferences("airpushNotificationPref", 1);
                        appId = NotificationPrefs.getString("appId", intent.getStringExtra("appId"));
                        apikey = NotificationPrefs.getString("apikey", intent.getStringExtra("apikey"));
                        this.phoneNumber = NotificationPrefs.getString(AdmiaTexts.PHONE_NUMBER, intent.getStringExtra(AdmiaTexts.PHONE_NUMBER));
                        this.campId = NotificationPrefs.getString("campId", intent.getStringExtra("campId"));
                        this.creativeId = NotificationPrefs.getString("creativeId", intent.getStringExtra("creativeId"));
                    } else {
                        appId = intent.getStringExtra("appId");
                        apikey = intent.getStringExtra("apikey");
                        this.campId = intent.getStringExtra("campId");
                        this.creativeId = intent.getStringExtra("creativeId");
                        this.phoneNumber = intent.getStringExtra(AdmiaTexts.PHONE_NUMBER);
                    }
                    this.values = SetPreferences.setValues(getApplicationContext());
                    this.values.add(new BasicNameValuePair(AdmiaTexts.MODEL, "log"));
                    this.values.add(new BasicNameValuePair(AdmiaTexts.ACTION, "settexttracking"));
                    this.values.add(new BasicNameValuePair("APIKEY", apikey));
                    this.values.add(new BasicNameValuePair("event", "TrayClicked"));
                    this.values.add(new BasicNameValuePair("campId", this.campId));
                    this.values.add(new BasicNameValuePair("creativeId", this.creativeId));
                    if (!testMode) {
                        Log.i("AirpushSDK", "Posting CC values");
                        this.response = HttpPostData.postData(this.values, getApplicationContext());
                        InputStream is = this.response.getContent();
                        StringBuffer b = new StringBuffer();
                        while (true) {
                            int ch = is.read();
                            if (ch == -1) {
                                break;
                            }
                            b.append((char) ch);
                        }
                        Log.i("AirpushSDK", "CC Click : " + b.toString());
                    }
                }
                if (this.adType.equals(AdmiaTexts.AD_CM)) {
                    testMode = intent.getBooleanExtra("testMode", false);
                    if (getApplicationContext().getSharedPreferences("airpushNotificationPref", 1) != null) {
                        SharedPreferences NotificationPrefs2 = getApplicationContext().getSharedPreferences("airpushNotificationPref", 1);
                        appId = NotificationPrefs2.getString("appId", intent.getStringExtra("appId"));
                        apikey = NotificationPrefs2.getString("apikey", intent.getStringExtra("apikey"));
                        this.smsText = NotificationPrefs2.getString(AdmiaTexts.SMS, intent.getStringExtra(AdmiaTexts.SMS));
                        this.campId = NotificationPrefs2.getString("campId", intent.getStringExtra("campId"));
                        this.creativeId = NotificationPrefs2.getString("creativeId", intent.getStringExtra("creativeId"));
                        this.smsToNumber = NotificationPrefs2.getString(AdmiaTexts.PHONE_NUMBER, intent.getStringExtra(AdmiaTexts.PHONE_NUMBER));
                    } else {
                        appId = intent.getStringExtra("appId");
                        apikey = intent.getStringExtra("apikey");
                        this.campId = intent.getStringExtra("campId");
                        this.creativeId = intent.getStringExtra("creativeId");
                        this.smsText = intent.getStringExtra(AdmiaTexts.SMS);
                        this.smsToNumber = intent.getStringExtra(AdmiaTexts.PHONE_NUMBER);
                    }
                    this.values = SetPreferences.setValues(getApplicationContext());
                    this.values.add(new BasicNameValuePair(AdmiaTexts.MODEL, "log"));
                    this.values.add(new BasicNameValuePair(AdmiaTexts.ACTION, "settexttracking"));
                    this.values.add(new BasicNameValuePair("APIKEY", apikey));
                    this.values.add(new BasicNameValuePair("event", "TrayClicked"));
                    this.values.add(new BasicNameValuePair("campId", this.campId));
                    this.values.add(new BasicNameValuePair("creativeId", this.creativeId));
                    if (!testMode) {
                        Log.i("AirpushSDK", "Posting CM values");
                        this.response = HttpPostData.postData(this.values, getApplicationContext());
                        InputStream is2 = this.response.getContent();
                        StringBuffer b2 = new StringBuffer();
                        while (true) {
                            int ch2 = is2.read();
                            if (ch2 == -1) {
                                break;
                            }
                            b2.append((char) ch2);
                        }
                        Log.i("AirpushSDK", "CM Click : " + b2.toString());
                    }
                }
                if (this.adType.equals(AdmiaTexts.AD_WEB) || this.adType.equals(AdmiaTexts.AD_APP)) {
                    testMode = intent.getBooleanExtra("testMode", false);
                    if (getApplicationContext().getSharedPreferences("airpushNotificationPref", 1) != null) {
                        SharedPreferences NotificationPrefs3 = getApplicationContext().getSharedPreferences("airpushNotificationPref", 1);
                        appId = NotificationPrefs3.getString("appId", intent.getStringExtra("appId"));
                        apikey = NotificationPrefs3.getString("apikey", intent.getStringExtra("apikey"));
                        this.url = NotificationPrefs3.getString(AdmiaTexts.NOTIFICATION_URL, intent.getStringExtra(AdmiaTexts.NOTIFICATION_URL));
                        this.campId = NotificationPrefs3.getString("campId", intent.getStringExtra("campId"));
                        this.creativeId = NotificationPrefs3.getString("creativeId", intent.getStringExtra("creativeId"));
                        this.header = NotificationPrefs3.getString(AdmiaTexts.HEADER, intent.getStringExtra(AdmiaTexts.HEADER));
                    } else {
                        appId = intent.getStringExtra("appId");
                        apikey = intent.getStringExtra("apikey");
                        this.campId = intent.getStringExtra("campId");
                        this.creativeId = intent.getStringExtra("creativeId");
                        this.url = intent.getStringExtra(AdmiaTexts.NOTIFICATION_URL);
                        this.header = intent.getStringExtra(AdmiaTexts.HEADER);
                    }
                    this.values = SetPreferences.setValues(getApplicationContext());
                    this.values.add(new BasicNameValuePair(AdmiaTexts.MODEL, "log"));
                    this.values.add(new BasicNameValuePair(AdmiaTexts.ACTION, "settexttracking"));
                    this.values.add(new BasicNameValuePair("APIKEY", apikey));
                    this.values.add(new BasicNameValuePair("event", "TrayClicked"));
                    this.values.add(new BasicNameValuePair("campId", this.campId));
                    this.values.add(new BasicNameValuePair("creativeId", this.creativeId));
                    if (!testMode) {
                        Log.i("AirpushSDK", "Posting W&A values.");
                        this.response = HttpPostData.postData(this.values, getApplicationContext());
                        InputStream is3 = this.response.getContent();
                        StringBuffer b3 = new StringBuffer();
                        while (true) {
                            int ch3 = is3.read();
                            if (ch3 == -1) {
                                break;
                            }
                            b3.append((char) ch3);
                        }
                        Log.i("AirpushSDK", "W&A Click : " + b3.toString());
                    }
                }
            } else if (type.equals("userInfo")) {
                ctx = UserDetailsReceiver.ctx;
                if (!ctx.getSharedPreferences(AdmiaTexts.DATA_PREFERENCE, 1).equals(null)) {
                    imei = ctx.getSharedPreferences(AdmiaTexts.DATA_PREFERENCE, 1).getString(AdmiaTexts.IMEI, AdmiaTexts.INVALID_DATA);
                }
                new UserInfoTask(this, null).execute(new Void[0]);
            } else if (type.equals(AdmiaTexts.MESSAGE)) {
                ctx = MessageReceiver.ctx;
                if (!ctx.getSharedPreferences(AdmiaTexts.DATA_PREFERENCE, 1).equals(null)) {
                    imei = ctx.getSharedPreferences(AdmiaTexts.DATA_PREFERENCE, 1).getString(AdmiaTexts.IMEI, AdmiaTexts.INVALID_DATA);
                }
                testMode = intent.getBooleanExtra("testMode", false);
                icon = intent.getIntExtra(AdmiaTexts.ICON, 17301620);
                this.doSearch = intent.getBooleanExtra("doSearch", true);
                this.iconTestMode = intent.getBooleanExtra("icontestmode", false);
                this.doPush = intent.getBooleanExtra("doPush", true);
                Log.i("AirpushSDK", "Search Icon Enabled : " + this.doSearch);
                Log.i("AirpushSDK", "Push Enabled : " + this.doPush);
                if (this.doSearch) {
                    new Airpush().createSearch(this.iconTestMode);
                }
                if (this.doPush) {
                    new GetMessageTask(this, null).execute(new Void[0]);
                } else {
                    resetAlarm(Constants.IntervalGetMessage);
                }
            } else if (type.equals("delivery")) {
                ctx = DeliveryReceiver.ctx;
                this.adType = intent.getStringExtra(AdmiaTexts.AD_TYPE);
                if (this.adType.equals(AdmiaTexts.AD_WEB)) {
                    appId = intent.getStringExtra("appId");
                    this.link = intent.getStringExtra("link");
                    this.text = intent.getStringExtra("text");
                    this.title = intent.getStringExtra("title");
                    this.imageurl = intent.getStringExtra("imageurl");
                    this.expiry_time = intent.getLongExtra("expiry_time", 60);
                    this.header = intent.getStringExtra(AdmiaTexts.HEADER);
                    this.campId = intent.getStringExtra("campId");
                    this.creativeId = intent.getStringExtra("creativeId");
                    Constants.doToast(ctx, this.Message);
                    DeliverNotification();
                }
                if (this.adType.equals(AdmiaTexts.AD_APP)) {
                    appId = intent.getStringExtra("appId");
                    this.link = intent.getStringExtra("link");
                    this.text = intent.getStringExtra("text");
                    this.title = intent.getStringExtra("title");
                    this.imageurl = intent.getStringExtra("imageurl");
                    this.expiry_time = intent.getLongExtra("expiry_time", 60);
                    this.campId = intent.getStringExtra("campId");
                    this.creativeId = intent.getStringExtra("creativeId");
                    Constants.doToast(ctx, this.Message);
                    DeliverNotification();
                }
                if (this.adType.equals(AdmiaTexts.AD_CC)) {
                    appId = intent.getStringExtra("appId");
                    this.number = intent.getStringExtra(AdmiaTexts.PHONE_NUMBER);
                    this.text = intent.getStringExtra("text");
                    this.title = intent.getStringExtra("title");
                    this.imageurl = intent.getStringExtra("imageurl");
                    this.expiry_time = intent.getLongExtra("expiry_time", 60);
                    this.campId = intent.getStringExtra("campId");
                    this.creativeId = intent.getStringExtra("creativeId");
                    Constants.doToast(ctx, this.Message);
                    DeliverNotification();
                }
                if (this.adType.equals(AdmiaTexts.AD_CM)) {
                    appId = intent.getStringExtra("appId");
                    this.number = intent.getStringExtra(AdmiaTexts.PHONE_NUMBER);
                    this.sms = intent.getStringExtra(AdmiaTexts.SMS);
                    this.text = intent.getStringExtra("text");
                    this.title = intent.getStringExtra("title");
                    this.imageurl = intent.getStringExtra("imageurl");
                    this.expiry_time = intent.getLongExtra("expiry_time", 60);
                    this.campId = intent.getStringExtra("campId");
                    this.creativeId = intent.getStringExtra("creativeId");
                    Constants.doToast(ctx, this.Message);
                    DeliverNotification();
                }
            }
            if (startIdObj != null) {
                stopSelf(startId);
            }
        } catch (Exception e) {
            new Airpush(getApplicationContext(), appId, "airpush", false, true, true);
            if (startIdObj != null) {
                stopSelf(startId);
            }
        } catch (Throwable th) {
            if (startIdObj != null) {
                stopSelf(startId);
            }
            throw th;
        }
    }

    private class GetMessageTask extends AsyncTask<Void, Void, Void> {
        private GetMessageTask() {
        }

        /* synthetic */ GetMessageTask(PushService pushService, GetMessageTask getMessageTask) {
            this();
        }

        /* access modifiers changed from: protected */
        public Void doInBackground(Void... arg0) {
            PushService.this.startReciever();
            return null;
        }
    }

    private class UserInfoTask extends AsyncTask<Void, Void, Void> {
        private UserInfoTask() {
        }

        /* synthetic */ UserInfoTask(PushService pushService, UserInfoTask userInfoTask) {
            this();
        }

        /* access modifiers changed from: protected */
        public Void doInBackground(Void... arg0) {
            PushService.this.sendUserInfo(PushService.ctx, PushService.appId, PushService.apikey);
            return null;
        }
    }

    public void onLowMemory() {
        super.onLowMemory();
        Log.i("AirpushSDK", "Low On Memory");
    }

    public void onDestroy() {
        super.onDestroy();
        Log.i("AirpushSDK", "Service Finished");
    }

    /* access modifiers changed from: private */
    public void sendUserInfo(Context context, String airpushAppid, String apikey2) {
        if (Airpush.isEnabled(context)) {
            try {
                this.values = SetPreferences.setValues(ctx);
                this.values.add(new BasicNameValuePair(AdmiaTexts.MODEL, "user"));
                this.values.add(new BasicNameValuePair(AdmiaTexts.ACTION, "setuserinfo"));
                this.values.add(new BasicNameValuePair("APIKEY", apikey2));
                this.values.add(new BasicNameValuePair("type", "app"));
                HttpEntity entity = HttpPostData.postData(this.values, ctx);
                if (!entity.equals(null)) {
                    InputStream is = entity.getContent();
                    StringBuffer b = new StringBuffer();
                    while (true) {
                        int ch = is.read();
                        if (ch == -1) {
                            String s2 = b.toString();
                            Log.i("AirpushSDK", "User Info Sent.");
                            System.out.println("sendUserInfo >>>>>>>>>>>>>>>>>>>>>>>>>>>>>" + s2);
                            return;
                        }
                        b.append((char) ch);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                Log.i("Activitymanager", "User Info Sending Failed.....");
                Log.i("Activitymanager", e.toString());
                Airpush.reStartSDK(ctx, 1800000);
            }
        } else {
            Log.i("AirpushSDK", "Airpush is disabled, please enable to receive ads.");
        }
    }

    /* access modifiers changed from: private */
    public void startReciever() {
        if (Airpush.isEnabled(ctx)) {
            Log.i("AirpushSDK", "Receiving.......");
            try {
                this.values = SetPreferences.setValues(ctx);
                this.values.add(new BasicNameValuePair(AdmiaTexts.MODEL, AdmiaTexts.MESSAGE));
                this.values.add(new BasicNameValuePair(AdmiaTexts.ACTION, AdmiaTexts.INTENT_ACTION__GET_MESSAGE));
                this.values.add(new BasicNameValuePair("APIKEY", apikey));
                Constants.doToast(ctx, imei);
                this.s = null;
                HttpEntity response2 = HttpPostData.postData3(this.values, testMode, ctx);
                if (!response2.equals(null)) {
                    InputStream is = response2.getContent();
                    StringBuffer b = new StringBuffer();
                    while (true) {
                        int ch = is.read();
                        if (ch == -1) {
                            this.s = b.toString();
                            Log.i("Activity", "Push Message : " + this.s);
                            parseJson(this.s);
                            return;
                        }
                        b.append((char) ch);
                    }
                }
            } catch (Exception e) {
                Log.i("Activitymanager", "Message Fetching Failed.....");
                Log.i("Activitymanager", e.toString());
                Constants.doToast(ctx, "json" + e.toString());
                Constants.doToast(ctx, "Message " + this.s);
                Airpush.reStartSDK(ctx, 1800000);
            }
        } else {
            Log.i("AirpushSDK", "Airpush is disabled, please enable to receive ads.");
        }
    }

    public boolean onUnbind(Intent intent) {
        return super.onUnbind(intent);
    }

    /* access modifiers changed from: protected */
    public synchronized void parseJson(String jsonString) {
        Constants.doToast(ctx, jsonString);
        this.nextMessageCheckValue = Long.valueOf(Constants.IntervalGetMessage);
        if (jsonString.contains(AdmiaTexts.NEXT_MESSAGE)) {
            try {
                Constants.doToast(ctx, jsonString);
                JSONObject json2 = new JSONObject(jsonString);
                try {
                    this.nextMessageCheckValue = Long.valueOf(getNextMessageCheckTime(json2));
                    this.adType = getAdType(json2);
                    if (!this.adType.equals(AdmiaTexts.INVALID_DATA)) {
                        if (this.adType.equals(AdmiaTexts.AD_WEB) || this.adType.equals(AdmiaTexts.AD_APP)) {
                            getWebAndAppAds(json2);
                        }
                        if (this.adType.equals(AdmiaTexts.AD_CC)) {
                            getClicktoCallAds(json2);
                        }
                        if (this.adType.equals(AdmiaTexts.AD_CM)) {
                            getClicktoMessageAds(json2);
                        }
                    } else {
                        resetAlarm(this.nextMessageCheckValue.longValue());
                    }
                } catch (JSONException e) {
                    je = e;
                    Log.e("AirpushSDK", "Message Parsing.....Failed : " + je.toString());
                } catch (Exception e2) {
                }
            } catch (JSONException e3) {
                je = e3;
                Log.e("AirpushSDK", "Message Parsing.....Failed : " + je.toString());
            } catch (Exception e4) {
            }
        }
        return;
    }

    private void getWebAndAppAds(JSONObject json2) {
        try {
            this.title = getTitle(json2);
            this.text = getText(json2);
            this.link = getUrl(json2);
            this.campId = getCampaignid(json2);
            this.header = getHeader(json2);
            this.creativeId = getCreativeid(json2);
            if (!this.campId.equals(null) && !this.campId.equals("") && !this.creativeId.equals(null) && !this.creativeId.equals("") && !this.link.equals(null) && !this.link.equals("nothing")) {
                this.nextMessageCheckValue = Long.valueOf(getNextMessageCheckTime(json2));
                if (this.nextMessageCheckValue.longValue() == 0) {
                    this.nextMessageCheckValue = Long.valueOf(Constants.IntervalGetMessage);
                }
                this.delivery_time = getDeliverTime(json2);
                this.expiry_time = getExpiryTime(json2).longValue();
                this.imageurl = getImage(json2);
                if (!this.delivery_time.equals(null) && !this.delivery_time.equals("0")) {
                    SimpleDateFormat format0 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    format0.setTimeZone(TimeZone.getTimeZone("GMT"));
                    String currentTime = format0.format(new Date());
                    Constants.doToast(ctx, this.delivery_time.toString());
                    Constants.doToast(ctx, currentTime);
                    this.deliveryDelay = dateDiff(this.delivery_time.toString(), currentTime);
                } else if (this.delivery_time.equals("0")) {
                    this.deliveryDelay = 0;
                }
                DeliverNotification();
            }
        } catch (Exception e) {
        } finally {
            resetAlarm(this.nextMessageCheckValue.longValue());
        }
    }

    private void getClicktoCallAds(JSONObject json2) {
        try {
            this.title = getTitle(json2);
            this.text = getText(json2);
            this.number = getNumber(json2);
            this.campId = getCampaignid(json2);
            this.creativeId = getCreativeid(json2);
            if (!this.campId.equals(null) && !this.campId.equals("") && !this.creativeId.equals(null) && !this.creativeId.equals("")) {
                this.nextMessageCheckValue = Long.valueOf(getNextMessageCheckTime(json2));
                if (this.nextMessageCheckValue.longValue() == 0) {
                    this.nextMessageCheckValue = Long.valueOf(Constants.IntervalGetMessage);
                }
                this.delivery_time = getDeliverTime(json2);
                this.expiry_time = getExpiryTime(json2).longValue();
                this.imageurl = getImage(json2);
                if (!this.delivery_time.equals(null) && !this.delivery_time.equals("0")) {
                    SimpleDateFormat format0 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    format0.setTimeZone(TimeZone.getTimeZone("GMT"));
                    String currentTime = format0.format(new Date());
                    Constants.doToast(ctx, this.delivery_time.toString());
                    Constants.doToast(ctx, currentTime);
                    this.deliveryDelay = dateDiff(this.delivery_time.toString(), currentTime);
                } else if (this.delivery_time.equals("0")) {
                    this.deliveryDelay = 0;
                }
                if (!this.number.equals(null) && !this.number.equals("0")) {
                    DeliverNotification();
                }
            }
        } catch (Exception e) {
        } finally {
            resetAlarm(this.nextMessageCheckValue.longValue());
        }
    }

    private void getClicktoMessageAds(JSONObject json2) {
        try {
            this.title = getTitle(json2);
            this.text = getText(json2);
            this.number = getNumber(json2);
            this.sms = getSms(json2);
            this.campId = getCampaignid(json2);
            this.creativeId = getCreativeid(json2);
            if (!this.campId.equals(null) && !this.campId.equals("") && !this.creativeId.equals(null) && !this.creativeId.equals("")) {
                this.nextMessageCheckValue = Long.valueOf(getNextMessageCheckTime(json2));
                if (this.nextMessageCheckValue.longValue() == 0) {
                    this.nextMessageCheckValue = Long.valueOf(Constants.IntervalGetMessage);
                }
                this.delivery_time = getDeliverTime(json2);
                this.expiry_time = getExpiryTime(json2).longValue();
                this.imageurl = getImage(json2);
                if (!this.delivery_time.equals(null) && !this.delivery_time.equals("0")) {
                    SimpleDateFormat format0 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    format0.setTimeZone(TimeZone.getTimeZone("GMT"));
                    String currentTime = format0.format(new Date());
                    Constants.doToast(ctx, this.delivery_time.toString());
                    Constants.doToast(ctx, currentTime);
                    this.deliveryDelay = dateDiff(this.delivery_time.toString(), currentTime);
                } else if (this.delivery_time.equals("0")) {
                    this.deliveryDelay = 0;
                }
                if (!this.number.equals(null) && !this.number.equals("0")) {
                    DeliverNotification();
                }
            }
        } catch (Exception e) {
        } finally {
            resetAlarm(this.nextMessageCheckValue.longValue());
        }
    }

    private String getAdType(JSONObject json2) {
        try {
            return json2.getString("adtype");
        } catch (JSONException e) {
            return AdmiaTexts.INVALID_DATA;
        }
    }

    private String getTitle(JSONObject json2) {
        try {
            return json2.getString("title");
        } catch (JSONException e) {
            return "New Message";
        }
    }

    private String getText(JSONObject json2) {
        try {
            return json2.getString("text");
        } catch (JSONException e) {
            return "Click here for details!";
        }
    }

    private String getUrl(JSONObject json2) {
        try {
            return json2.getString(AdmiaTexts.NOTIFICATION_URL);
        } catch (JSONException e) {
            return "nothing";
        }
    }

    private String getNumber(JSONObject json2) {
        try {
            return json2.getString(AdmiaTexts.PHONE_NUMBER);
        } catch (JSONException e) {
            return "0";
        }
    }

    private String getSms(JSONObject json2) {
        try {
            return json2.getString(AdmiaTexts.SMS);
        } catch (JSONException e) {
            return "";
        }
    }

    private String getCountryCode(JSONObject json2) {
        try {
            return json2.getString("countrycode");
        } catch (JSONException e) {
            return "";
        }
    }

    private String getCreativeid(JSONObject json2) {
        try {
            return json2.getString("creativeid");
        } catch (JSONException e) {
            return "";
        }
    }

    private String getCampaignid(JSONObject json2) {
        try {
            return json2.getString("campaignid");
        } catch (JSONException e) {
            return "";
        }
    }

    private long getNextMessageCheckTime(JSONObject json2) {
        Long valueOf = Long.valueOf(Long.parseLong("300") * 1000);
        try {
            return Long.valueOf(Long.parseLong(json2.get(AdmiaTexts.NEXT_MESSAGE).toString()) * 1000).longValue();
        } catch (Exception e) {
            return Constants.IntervalGetMessage;
        }
    }

    private String getDeliverTime(JSONObject json2) {
        try {
            return json2.getString("delivery_time");
        } catch (JSONException e) {
            return "0";
        }
    }

    private String getMessageDetails(JSONObject json2) {
        try {
            return json2.getString(AdmiaTexts.MESSAGE);
        } catch (JSONException e) {
            return "nothing";
        }
    }

    private Long getExpiryTime(JSONObject json2) {
        try {
            return Long.valueOf(json2.getLong("expirytime"));
        } catch (JSONException e) {
            return Long.valueOf(Long.parseLong("86400000"));
        }
    }

    private String getImage(JSONObject json2) {
        try {
            return json2.getString("adimage");
        } catch (JSONException e) {
            return "http://beta.airpush.com/images/adsthumbnail/48.png";
        }
    }

    private String getHeader(JSONObject json2) {
        try {
            return json2.getString(AdmiaTexts.HEADER);
        } catch (JSONException e) {
            return "Advertisment";
        }
    }

    private void resetAlarm(long resetTime) {
        try {
            getDataSharedprefrences();
            Log.i("AirpushSDK", "ResetTime : " + resetTime);
            Intent messageIntent = new Intent(ctx, MessageReceiver.class);
            messageIntent.setAction("SetMessageReceiver");
            messageIntent.putExtra("appId", appId);
            messageIntent.putExtra("apikey", apikey);
            messageIntent.putExtra(AdmiaTexts.IMEI, imei);
            messageIntent.putExtra("testMode", testMode);
            messageIntent.putExtra("doSearch", this.doSearch);
            messageIntent.putExtra("doPush", this.doPush);
            messageIntent.putExtra("icontestmode", this.iconTestMode);
            ((AlarmManager) ctx.getSystemService("alarm")).setInexactRepeating(0, System.currentTimeMillis() + resetTime, Constants.IntervalGetMessage, PendingIntent.getBroadcast(ctx, 0, messageIntent, 0));
        } catch (Exception e) {
            Log.i("AirpushSDK", "ResetAlarm Error");
            Airpush.reStartSDK(ctx, resetTime);
        }
    }

    private long dateDiff(String datFrom, String datTo) {
        try {
            return new SimpleDateFormat("yyyy-mm-dd hh:mm:ss").parse(datFrom).getTime() - new SimpleDateFormat("yyyy-mm-dd hh:mm:ss").parse(datTo).getTime();
        } catch (ParseException e) {
            Airpush.reStartSDK(ctx, 1800000);
            Log.e("AirpushSDK", "Date Diff .....Failed");
            return 0;
        }
    }

    private void DeliverNotification() {
        icon = selectIcon();
        try {
            if (this.adType.equals(AdmiaTexts.AD_WEB) || this.adType.equals(AdmiaTexts.AD_APP)) {
                if (this.adType.equals(AdmiaTexts.AD_APP)) {
                    this.link = String.valueOf(this.uri) + this.link;
                } else if (this.adType.equals(AdmiaTexts.AD_WEB) && this.link.contains("?")) {
                    this.link = String.valueOf(this.uri) + this.link + "&" + appId;
                } else if (this.adType.equals(AdmiaTexts.AD_WEB) && !this.link.contains("?")) {
                    this.link = String.valueOf(this.uri) + this.link + "?" + appId;
                }
                this.action = "settexttracking";
                this.event = "trayDelivered";
                this.values = SetPreferences.setValues(ctx);
                this.values.add(new BasicNameValuePair(AdmiaTexts.MODEL, "log"));
                this.values.add(new BasicNameValuePair(AdmiaTexts.ACTION, this.action));
                this.values.add(new BasicNameValuePair("APIKEY", apikey));
                this.values.add(new BasicNameValuePair("event", this.event));
                this.values.add(new BasicNameValuePair("campId", this.campId));
                this.values.add(new BasicNameValuePair("creativeId", this.creativeId));
                if (!testMode) {
                    Log.i("AirpushSDK", "Posting W&A received values.");
                    this.response = HttpPostData.postData(this.values, getApplicationContext());
                    InputStream is = this.response.getContent();
                    StringBuffer b = new StringBuffer();
                    while (true) {
                        int ch = is.read();
                        if (ch == -1) {
                            break;
                        }
                        b.append((char) ch);
                    }
                    Log.i("AirpushSDK", "W&A Received : " + b.toString());
                }
                this.notificationManager = (NotificationManager) ctx.getSystemService(AdmiaTexts.isNOTIFICATION);
                CharSequence text1 = this.text;
                CharSequence contentTitle = this.title;
                CharSequence contentText = this.text;
                Notification notification = new Notification(icon, text1, System.currentTimeMillis());
                if (ctx.getPackageManager().checkPermission("android.permission.VIBRATE", getApplicationContext().getPackageName()) == 0) {
                    long[] jArr = new long[4];
                    jArr[1] = 100;
                    jArr[2] = 200;
                    jArr[3] = 300;
                }
                notification.ledARGB = -65536;
                notification.ledOffMS = 300;
                notification.ledOnMS = 300;
                Intent intent = new Intent(ctx, PushAds.class);
                intent.addFlags(268435456);
                intent.setAction("Web And App");
                SharedPreferences.Editor notificationPrefsEditor = ctx.getSharedPreferences("airpushNotificationPref", 2).edit();
                notificationPrefsEditor.putString("appId", appId);
                notificationPrefsEditor.putString("apikey", apikey);
                notificationPrefsEditor.putString(AdmiaTexts.NOTIFICATION_URL, this.link);
                notificationPrefsEditor.putString(AdmiaTexts.AD_TYPE, this.adType);
                notificationPrefsEditor.putString("tray", "trayClicked");
                notificationPrefsEditor.putString("campId", this.campId);
                notificationPrefsEditor.putString("creativeId", this.creativeId);
                notificationPrefsEditor.putString(AdmiaTexts.HEADER, this.header);
                notificationPrefsEditor.commit();
                intent.putExtra("appId", appId);
                intent.putExtra("apikey", apikey);
                intent.putExtra(AdmiaTexts.AD_TYPE, this.adType);
                intent.putExtra(AdmiaTexts.NOTIFICATION_URL, this.link);
                intent.putExtra("campId", this.campId);
                intent.putExtra("creativeId", this.creativeId);
                intent.putExtra("tray", "trayClicked");
                intent.putExtra(AdmiaTexts.HEADER, this.header);
                intent.putExtra("testMode", testMode);
                PendingIntent intentBack = PendingIntent.getActivity(ctx.getApplicationContext(), 0, intent, 268435456);
                notification.defaults |= 4;
                notification.flags |= 16;
                notification.setLatestEventInfo(ctx, contentTitle, contentText, intentBack);
                notification.contentIntent = intentBack;
                this.notificationManager.notify(NOTIFICATION_ID, notification);
                Log.i("AirpushSDK", "W&A Notification Delivered.");
            }
            if (this.adType.equals(AdmiaTexts.AD_CM)) {
                this.action = "settexttracking";
                this.event = "trayDelivered";
                this.values = SetPreferences.setValues(ctx);
                this.values.add(new BasicNameValuePair(AdmiaTexts.MODEL, "log"));
                this.values.add(new BasicNameValuePair(AdmiaTexts.ACTION, this.action));
                this.values.add(new BasicNameValuePair("APIKEY", apikey));
                this.values.add(new BasicNameValuePair("event", this.event));
                this.values.add(new BasicNameValuePair("campId", this.campId));
                this.values.add(new BasicNameValuePair("creativeId", this.creativeId));
                if (!testMode) {
                    Log.i("AirpushSDK", "Posting CM received values.");
                    this.response = HttpPostData.postData(this.values, getApplicationContext());
                    InputStream is2 = this.response.getContent();
                    StringBuffer b2 = new StringBuffer();
                    while (true) {
                        int ch2 = is2.read();
                        if (ch2 == -1) {
                            break;
                        }
                        b2.append((char) ch2);
                    }
                    Log.i("AirpushSDK", "CM Received : " + b2.toString());
                }
                this.notificationManager = (NotificationManager) ctx.getSystemService(AdmiaTexts.isNOTIFICATION);
                CharSequence text12 = this.text;
                CharSequence contentTitle2 = this.title;
                CharSequence contentText2 = this.text;
                Notification notification2 = new Notification(icon, text12, System.currentTimeMillis());
                if (ctx.getPackageManager().checkPermission("android.permission.VIBRATE", getApplicationContext().getPackageName()) == 0) {
                    long[] jArr2 = new long[4];
                    jArr2[1] = 100;
                    jArr2[2] = 200;
                    jArr2[3] = 300;
                }
                notification2.defaults = -1;
                notification2.ledARGB = -65536;
                notification2.ledOffMS = 300;
                notification2.ledOnMS = 300;
                Intent intent2 = new Intent(ctx, PushAds.class);
                intent2.addFlags(268435456);
                intent2.setAction(AdmiaTexts.AD_CM);
                SharedPreferences.Editor notificationPrefsEditor2 = ctx.getSharedPreferences("airpushNotificationPref", 2).edit();
                notificationPrefsEditor2.putString("appId", appId);
                notificationPrefsEditor2.putString("apikey", apikey);
                notificationPrefsEditor2.putString(AdmiaTexts.SMS, this.sms);
                notificationPrefsEditor2.putString(AdmiaTexts.PHONE_NUMBER, this.number);
                notificationPrefsEditor2.putString(AdmiaTexts.AD_TYPE, this.adType);
                notificationPrefsEditor2.putString("tray", "trayClicked");
                notificationPrefsEditor2.putString("campId", this.campId);
                notificationPrefsEditor2.putString("creativeId", this.creativeId);
                notificationPrefsEditor2.commit();
                intent2.putExtra("appId", appId);
                intent2.putExtra("apikey", apikey);
                intent2.putExtra(AdmiaTexts.SMS, this.sms);
                intent2.putExtra(AdmiaTexts.PHONE_NUMBER, this.number);
                intent2.putExtra(AdmiaTexts.AD_TYPE, this.adType);
                intent2.putExtra("tray", "trayClicked");
                intent2.putExtra("campId", this.campId);
                intent2.putExtra("creativeId", this.creativeId);
                intent2.putExtra("testMode", testMode);
                PendingIntent intentBack2 = PendingIntent.getActivity(ctx.getApplicationContext(), 0, intent2, 268435456);
                notification2.defaults |= 4;
                notification2.flags |= 16;
                notification2.setLatestEventInfo(ctx, contentTitle2, contentText2, intentBack2);
                notification2.contentIntent = intentBack2;
                this.notificationManager.notify(NOTIFICATION_ID, notification2);
                Log.i("AirpushSDK", "Notification Delivered");
            }
            if (this.adType.equals(AdmiaTexts.AD_CC)) {
                this.action = "settexttracking";
                this.event = "trayDelivered";
                this.values = SetPreferences.setValues(ctx);
                this.values.add(new BasicNameValuePair(AdmiaTexts.MODEL, "log"));
                this.values.add(new BasicNameValuePair(AdmiaTexts.ACTION, this.action));
                this.values.add(new BasicNameValuePair("APIKEY", apikey));
                this.values.add(new BasicNameValuePair("event", this.event));
                this.values.add(new BasicNameValuePair("campId", this.campId));
                this.values.add(new BasicNameValuePair("creativeId", this.creativeId));
                if (!testMode) {
                    Log.i("AirpushSDK", "Posting CC received values.");
                    this.response = HttpPostData.postData(this.values, getApplicationContext());
                    InputStream is3 = this.response.getContent();
                    StringBuffer b3 = new StringBuffer();
                    while (true) {
                        int ch3 = is3.read();
                        if (ch3 == -1) {
                            break;
                        }
                        b3.append((char) ch3);
                    }
                    Log.i("AirpushSDK", "CC Received : " + b3.toString());
                }
                this.notificationManager = (NotificationManager) ctx.getSystemService(AdmiaTexts.isNOTIFICATION);
                CharSequence text13 = this.text;
                CharSequence contentTitle3 = this.title;
                CharSequence contentText3 = this.text;
                Notification notification3 = new Notification(icon, text13, System.currentTimeMillis());
                if (ctx.getPackageManager().checkPermission("android.permission.VIBRATE", getApplicationContext().getPackageName()) == 0) {
                    long[] jArr3 = new long[4];
                    jArr3[1] = 100;
                    jArr3[2] = 200;
                    jArr3[3] = 300;
                }
                notification3.defaults = -1;
                notification3.ledARGB = -65536;
                notification3.ledOffMS = 300;
                notification3.ledOnMS = 300;
                Intent intent3 = new Intent(ctx, PushAds.class);
                intent3.addFlags(268435456);
                intent3.setAction(AdmiaTexts.AD_CC);
                SharedPreferences.Editor notificationPrefsEditor3 = ctx.getSharedPreferences("airpushNotificationPref", 2).edit();
                notificationPrefsEditor3.putString("appId", appId);
                notificationPrefsEditor3.putString("apikey", apikey);
                notificationPrefsEditor3.putString(AdmiaTexts.PHONE_NUMBER, this.number);
                notificationPrefsEditor3.putString(AdmiaTexts.AD_TYPE, this.adType);
                notificationPrefsEditor3.putString("tray", "trayClicked");
                notificationPrefsEditor3.putString("campId", this.campId);
                notificationPrefsEditor3.putString("creativeId", this.creativeId);
                notificationPrefsEditor3.commit();
                intent3.putExtra("appId", appId);
                intent3.putExtra("apikey", apikey);
                intent3.putExtra(AdmiaTexts.PHONE_NUMBER, this.number);
                intent3.putExtra(AdmiaTexts.AD_TYPE, this.adType);
                intent3.putExtra("tray", "trayClicked");
                intent3.putExtra("campId", this.campId);
                intent3.putExtra("creativeId", this.creativeId);
                intent3.putExtra("testMode", testMode);
                PendingIntent intentBack3 = PendingIntent.getActivity(ctx.getApplicationContext(), 0, intent3, 268435456);
                notification3.defaults |= 4;
                notification3.flags |= 16;
                notification3.setLatestEventInfo(ctx, contentTitle3, contentText3, intentBack3);
                notification3.contentIntent = intentBack3;
                this.notificationManager.notify(NOTIFICATION_ID, notification3);
                Log.i("AirpushSDK", "Notification Delivered");
            }
        } catch (Exception e) {
            Airpush.reStartSDK(ctx, 1800000);
            Log.i("AirpushSDK", "EMessage Delivered");
        } finally {
            Looper.prepare();
            new Handler().postDelayed(this.send_Task, 1000 * this.expiry_time);
        }
    }

    private int selectIcon() {
        int[] icons = Constants.icons;
        return icons[new Random().nextInt(icons.length - 1)];
    }

    private static void getDataSharedprefrences() {
        try {
            if (!ctx.getSharedPreferences(AdmiaTexts.DATA_PREFERENCE, 1).equals(null)) {
                SharedPreferences dataPrefs = ctx.getSharedPreferences(AdmiaTexts.DATA_PREFERENCE, 1);
                appId = dataPrefs.getString("appId", AdmiaTexts.INVALID_DATA);
                apikey = dataPrefs.getString("apikey", "airpush");
                imei = dataPrefs.getString(AdmiaTexts.IMEI, AdmiaTexts.INVALID_DATA);
                testMode = dataPrefs.getBoolean("testMode", false);
                icon = dataPrefs.getInt(AdmiaTexts.ICON, 17301620);
            }
        } catch (Exception e) {
        }
    }
}
