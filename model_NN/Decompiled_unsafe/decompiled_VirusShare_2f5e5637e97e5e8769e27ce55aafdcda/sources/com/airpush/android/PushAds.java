package com.airpush.android;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import java.io.InputStream;
import java.util.List;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHttpResponse;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;

public class PushAds extends Activity implements View.OnClickListener {
    private static Context context;
    private static HttpEntity entity;
    private static DefaultHttpClient httpClient;
    private static BasicHttpParams httpParameters;
    private static HttpPost httpPost;
    private static BasicHttpResponse httpResponse;
    private static String size = "1";
    private static int timeoutConnection;
    private static int timeoutSocket;
    private static String url = null;
    /* access modifiers changed from: private */
    public static List<NameValuePair> values = null;
    private String action;
    private View.OnClickListener adOnClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            try {
                PushAds.this.displayAd(PushAds.this.clickUrl);
                PushAds.values = SetPreferences.setValues(PushAds.this.getApplicationContext());
                PushAds.values.add(new BasicNameValuePair(AdmiaTexts.MODEL, "log"));
                PushAds.values.add(new BasicNameValuePair(AdmiaTexts.ACTION, "setfptracking"));
                PushAds.values.add(new BasicNameValuePair("APIKEY", PushAds.this.apikey));
                PushAds.values.add(new BasicNameValuePair("event", "fclick"));
                PushAds.values.add(new BasicNameValuePair("campId", PushAds.this.campaignId));
                PushAds.values.add(new BasicNameValuePair("creativeId", PushAds.this.creativeId));
                new Handler().postDelayed(PushAds.this.sendAdValues_Task, 5000);
            } catch (Exception e) {
                Log.i("AirpushSDK", "Display Ad Network Error, please try again later. ");
            }
        }
    };
    private String adSize;
    private String adType;
    /* access modifiers changed from: private */
    public String apikey = null;
    private String appId = null;
    private BitmapDrawable bd;
    private int btn1Height;
    private float btn1Padding;
    private int btn2Height;
    private float btn2Padding;
    private int buttonMargin;
    private String campId = null;
    /* access modifiers changed from: private */
    public String campaignId;
    /* access modifiers changed from: private */
    public String clickUrl;
    /* access modifiers changed from: private */
    public String creativeId = null;
    private int h;
    private String header;
    private int icon = 17301620;
    private float imagePadding;
    private String imageUrl;
    private Intent intent;
    private boolean more = true;
    private String moreButtonColor = "#000000";
    private String moreButtonText;
    private String moreButtonTextColor = "#FFFFFF";
    private String moreInfoClickUrl;
    private View.OnClickListener moreOnClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            try {
                PushAds.this.displayAd(PushAds.this.clickUrl);
                PushAds.values = SetPreferences.setValues(PushAds.this.getApplicationContext());
                PushAds.values.add(new BasicNameValuePair(AdmiaTexts.MODEL, "log"));
                PushAds.values.add(new BasicNameValuePair(AdmiaTexts.ACTION, "setfptracking"));
                PushAds.values.add(new BasicNameValuePair("APIKEY", PushAds.this.apikey));
                PushAds.values.add(new BasicNameValuePair("event", "fclick"));
                PushAds.values.add(new BasicNameValuePair("campId", PushAds.this.campaignId));
                PushAds.values.add(new BasicNameValuePair("creativeId", PushAds.this.creativeId));
                new Handler().postDelayed(PushAds.this.sendAdValues_Task, 3000);
            } catch (Exception e) {
                Log.i("AirpushSDK", "Display Ad Network Error, please try again later. ");
            }
        }
    };
    private String phoneNumber;
    private Runnable postData = new Runnable() {
        public void run() {
            PushAds.this.postValues();
        }
    };
    private HttpEntity response;
    /* access modifiers changed from: private */
    public Runnable sendAdValues_Task = new Runnable() {
        public void run() {
            if (!PushAds.this.showInterstitialtestAd) {
                PushAds.this.sendClickData();
            }
        }
    };
    private Runnable sendImpression_Task = new Runnable() {
        public void run() {
            if (!PushAds.this.showInterstitialtestAd) {
                PushAds.this.sendImpressionData();
            }
        }
    };
    /* access modifiers changed from: private */
    public boolean showAd = true;
    /* access modifiers changed from: private */
    public boolean showDialog = true;
    /* access modifiers changed from: private */
    public boolean showInterstitialtestAd;
    private String skipButtonColor = "#000000";
    private String skipButtonText;
    private String skipButtonTextColor = "#FFFFFF";
    private View.OnClickListener skipOnClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            PushAds.this.finish();
        }
    };
    private String smsText;
    private String smsToNumber;
    private boolean test = false;
    private int textSize;
    String url2 = "http://api.airpush.com/api.php";
    private int w;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getApplicationContext();
        this.intent = getIntent();
        this.action = this.intent.getAction();
        getWindowManager().getDefaultDisplay();
        this.h = 320;
        this.w = 350;
        this.adType = this.intent.getStringExtra(AdmiaTexts.AD_TYPE);
        if (this.adType.equals("searchad")) {
            Log.i("AirpushSDK", "Search Clicked");
            return;
        }
        if (this.adType.equals("ShoWDialog")) {
            this.appId = this.intent.getStringExtra("appId");
            this.apikey = this.intent.getStringExtra("apikey");
            this.test = this.intent.getBooleanExtra("test", false);
            this.icon = this.intent.getIntExtra(AdmiaTexts.ICON, 17301620);
            showDialog();
        }
        if (this.action.equals(AdmiaTexts.AD_CC)) {
            if (this.adType.equals(AdmiaTexts.AD_CC)) {
                Log.i("AirpushSDK", "Pushing Ads.....");
                if (context.getSharedPreferences("airpushNotificationPref", 1) != null) {
                    SharedPreferences NotificationPrefs = context.getSharedPreferences("airpushNotificationPref", 1);
                    this.appId = NotificationPrefs.getString("appId", this.intent.getStringExtra("appId"));
                    this.apikey = NotificationPrefs.getString("apikey", this.intent.getStringExtra("apikey"));
                    this.phoneNumber = NotificationPrefs.getString(AdmiaTexts.PHONE_NUMBER, this.intent.getStringExtra(AdmiaTexts.PHONE_NUMBER));
                    this.campId = NotificationPrefs.getString("campId", this.intent.getStringExtra("campId"));
                    this.creativeId = NotificationPrefs.getString("creativeId", this.intent.getStringExtra("creativeId"));
                } else {
                    this.appId = this.intent.getStringExtra("appId");
                    this.apikey = this.intent.getStringExtra("apikey");
                    this.campId = this.intent.getStringExtra("campId");
                    this.creativeId = this.intent.getStringExtra("creativeId");
                    this.phoneNumber = this.intent.getStringExtra(AdmiaTexts.PHONE_NUMBER);
                }
                Intent postAdValuesIntent = new Intent();
                postAdValuesIntent.setAction("com.airpush.android.PushServiceStart" + this.appId);
                postAdValuesIntent.putExtra("type", "PostAdValues");
                postAdValuesIntent.putExtras(this.intent);
                startService(postAdValuesIntent);
                callNumber();
            }
        } else if (this.action.equals(AdmiaTexts.AD_CM)) {
            if (this.adType.equals(AdmiaTexts.AD_CM)) {
                Log.i("AirpushSDK", "Pushing Ads.....");
                if (context.getSharedPreferences("airpushNotificationPref", 1) != null) {
                    SharedPreferences NotificationPrefs2 = context.getSharedPreferences("airpushNotificationPref", 1);
                    this.appId = NotificationPrefs2.getString("appId", this.intent.getStringExtra("appId"));
                    this.apikey = NotificationPrefs2.getString("apikey", this.intent.getStringExtra("apikey"));
                    this.smsText = NotificationPrefs2.getString(AdmiaTexts.SMS, this.intent.getStringExtra(AdmiaTexts.SMS));
                    this.campId = NotificationPrefs2.getString("campId", this.intent.getStringExtra("campId"));
                    this.creativeId = NotificationPrefs2.getString("creativeId", this.intent.getStringExtra("creativeId"));
                    this.smsToNumber = NotificationPrefs2.getString(AdmiaTexts.PHONE_NUMBER, this.intent.getStringExtra(AdmiaTexts.PHONE_NUMBER));
                } else {
                    this.appId = this.intent.getStringExtra("appId");
                    this.apikey = this.intent.getStringExtra("apikey");
                    this.campId = this.intent.getStringExtra("campId");
                    this.creativeId = this.intent.getStringExtra("creativeId");
                    this.smsText = this.intent.getStringExtra(AdmiaTexts.SMS);
                    this.smsToNumber = this.intent.getStringExtra(AdmiaTexts.PHONE_NUMBER);
                }
                Intent postAdValuesIntent2 = new Intent();
                postAdValuesIntent2.setAction("com.airpush.android.PushServiceStart" + this.appId);
                postAdValuesIntent2.putExtra("type", "PostAdValues");
                postAdValuesIntent2.putExtras(this.intent);
                startService(postAdValuesIntent2);
                sendSms();
            }
        } else if (!this.action.equals("Web And App")) {
        } else {
            if (this.adType.equals(AdmiaTexts.AD_WEB) || this.adType.equals(AdmiaTexts.AD_APP)) {
                Log.i("AirpushSDK", "Pushing Ads.....");
                if (context.getSharedPreferences("airpushNotificationPref", 1) != null) {
                    SharedPreferences NotificationPrefs3 = context.getSharedPreferences("airpushNotificationPref", 1);
                    this.appId = NotificationPrefs3.getString("appId", this.intent.getStringExtra("appId"));
                    this.apikey = NotificationPrefs3.getString("apikey", this.intent.getStringExtra("apikey"));
                    url = NotificationPrefs3.getString(AdmiaTexts.NOTIFICATION_URL, this.intent.getStringExtra(AdmiaTexts.NOTIFICATION_URL));
                    this.campId = NotificationPrefs3.getString("campId", this.intent.getStringExtra("campId"));
                    this.creativeId = NotificationPrefs3.getString("creativeId", this.intent.getStringExtra("creativeId"));
                    this.header = NotificationPrefs3.getString(AdmiaTexts.HEADER, this.intent.getStringExtra(AdmiaTexts.HEADER));
                } else {
                    this.appId = this.intent.getStringExtra("appId");
                    this.apikey = this.intent.getStringExtra("apikey");
                    this.campId = this.intent.getStringExtra("campId");
                    this.creativeId = this.intent.getStringExtra("creativeId");
                    url = this.intent.getStringExtra(AdmiaTexts.NOTIFICATION_URL);
                    this.header = this.intent.getStringExtra(AdmiaTexts.HEADER);
                }
                Intent postAdValuesIntent3 = new Intent();
                postAdValuesIntent3.setAction("com.airpush.android.PushServiceStart" + this.appId);
                postAdValuesIntent3.putExtra("type", "PostAdValues");
                postAdValuesIntent3.putExtras(this.intent);
                startService(postAdValuesIntent3);
                setTitle(this.header);
                display(url);
            }
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        finish();
        return true;
    }

    private void callNumber() {
        Log.i("AirpushSDK", "Pushing CC Ads.....");
        startActivity(new Intent("android.intent.action.DIAL", Uri.parse("tel:" + this.phoneNumber)));
    }

    private void sendSms() {
        Log.i("AirpushSDK", "Pushing CM Ads.....");
        Intent intent2 = new Intent("android.intent.action.SENDTO", Uri.parse("smsto:" + this.smsToNumber));
        intent2.putExtra("sms_body", this.smsText);
        startActivity(intent2);
    }

    private void display(String url3) {
        Log.i("AirpushSDK", "Pushing Web and App Ads.....");
        CustomWebView webView = new CustomWebView(this);
        webView.loadUrl(url3);
        setContentView(webView);
    }

    public void show(WebView webView) {
        setContentView(webView);
    }

    /* access modifiers changed from: protected */
    public void showDialog() {
        try {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setCancelable(true);
            builder.setMessage("Support the App developer by enabling ads in the notification tray, limited to 1 per day.");
            builder.setPositiveButton("I Agree", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    PushAds.this.showDialog = false;
                    PushAds.this.showAd = true;
                    PushAds.this.startAirpush();
                    PushAds.this.finish();
                }
            });
            builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    PushAds.this.showDialog = false;
                    PushAds.this.showAd = false;
                    PushAds.this.startAirpush();
                    PushAds.this.finish();
                }
            });
            builder.create();
            builder.show();
        } catch (Exception e) {
        }
    }

    /* access modifiers changed from: private */
    public void startAirpush() {
        new Airpush().startAirpush(context, this.appId, this.apikey, this.test, this.showDialog, this.icon, this.showAd);
    }

    /* access modifiers changed from: private */
    public void postValues() {
        this.response = HttpPostData.postData(values, getApplicationContext());
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        finish();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        finish();
    }

    private void displayAds() {
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        new Handler().postDelayed(this.sendImpression_Task, 5000);
        try {
            float scale = getResources().getDisplayMetrics().density;
            if (this.more) {
                LinearLayout rl = new LinearLayout(this);
                rl.setOrientation(1);
                new RelativeLayout(this);
                this.h = (int) (((float) this.h) * scale);
                this.w = (int) (((float) this.w) * scale);
                this.imagePadding = 10.0f * scale;
                this.btn1Padding = 10.0f * scale;
                this.btn2Padding = 5.0f * scale;
                this.btn1Height = (int) (50.0f * scale);
                this.btn2Height = (int) (50.0f * scale);
                ImageView iv = new ImageView(this);
                LinearLayout.LayoutParams lp0 = new LinearLayout.LayoutParams(70, 70);
                lp0.width = (int) (320.0f * scale);
                lp0.gravity = 1;
                LinearLayout.LayoutParams lp1 = new LinearLayout.LayoutParams(70, 70);
                lp1.gravity = 17;
                lp1.topMargin = (int) (5.0f * scale);
                LinearLayout.LayoutParams btn1LP = new LinearLayout.LayoutParams(70, 70);
                btn1LP.topMargin = (int) this.btn1Padding;
                btn1LP.width = (int) (320.0f * scale);
                btn1LP.gravity = 1;
                LinearLayout.LayoutParams btn2LP = new LinearLayout.LayoutParams(70, 70);
                btn2LP.topMargin = (int) this.btn2Padding;
                btn2LP.width = (int) (320.0f * scale);
                btn2LP.gravity = 1;
                lp1.height = this.h;
                lp1.width = this.w;
                lp0.height = (int) (((float) Integer.parseInt(this.adSize)) * scale);
                Log.i("AirpushSDK", "height : " + lp0.height);
                if (this.bd != null) {
                    iv.setBackgroundDrawable(this.bd);
                    Button skip = new Button(this);
                    skip.setText(this.skipButtonText);
                    skip.setHeight(this.btn1Height);
                    skip.setBackgroundColor(Color.parseColor(this.skipButtonColor));
                    skip.setTextColor(Color.parseColor(this.skipButtonTextColor));
                    skip.setTextSize((float) this.textSize);
                    skip.setTypeface(Typeface.DEFAULT_BOLD);
                    lp0.topMargin = this.buttonMargin;
                    Button moreInfo = new Button(this);
                    moreInfo.setBackgroundColor(Color.parseColor(this.moreButtonColor));
                    moreInfo.setTextColor(Color.parseColor(this.moreButtonTextColor));
                    moreInfo.setTextSize((float) this.textSize);
                    moreInfo.setTypeface(Typeface.DEFAULT_BOLD);
                    moreInfo.setText(this.moreButtonText);
                    moreInfo.setHeight(this.btn2Height);
                    rl.addView(iv, lp1);
                    rl.addView(moreInfo, btn2LP);
                    moreInfo.setOnClickListener(this.moreOnClickListener);
                    rl.addView(skip, btn1LP);
                    iv.setOnClickListener(this.adOnClickListener);
                    skip.setOnClickListener(this.skipOnClickListener);
                    setContentView(rl);
                    return;
                }
                Log.i("AirpushSDK", "Image Fetching error, please try again later.");
                return;
            }
            RelativeLayout rl2 = new RelativeLayout(this);
            new RelativeLayout(this);
            this.h = (int) (((float) this.h) * scale);
            this.w = (int) (((float) this.w) * scale);
            ImageView iv2 = new ImageView(this);
            RelativeLayout.LayoutParams lp02 = new RelativeLayout.LayoutParams(70, 70);
            lp02.width = (int) (320.0f * scale);
            lp02.addRule(12);
            if (this.bd != null) {
                iv2.setBackgroundDrawable(this.bd);
                Button skip2 = new Button(this);
                skip2.setText(this.skipButtonText);
                skip2.setBackgroundColor(Color.parseColor(this.skipButtonColor));
                skip2.setTextColor(Color.parseColor(this.skipButtonTextColor));
                skip2.setTextSize((float) this.textSize);
                skip2.setTypeface(Typeface.DEFAULT_BOLD);
                iv2.setOnClickListener(this.adOnClickListener);
                rl2.addView(iv2, this.w, this.h);
                rl2.addView(skip2, lp02);
                skip2.setOnClickListener(this.skipOnClickListener);
                setContentView(rl2);
                return;
            }
            Log.i("AirpushSDK", "Image Fetching error, please try again later.");
        } catch (Exception e) {
            Log.i("AirpushSDK", "Network Error, please try again later.");
        }
    }

    /* access modifiers changed from: private */
    public void sendImpressionData() {
        values = SetPreferences.setValues(context);
        values.add(new BasicNameValuePair(AdmiaTexts.MODEL, "log"));
        values.add(new BasicNameValuePair(AdmiaTexts.ACTION, "settexttracking"));
        values.add(new BasicNameValuePair("APIKEY", this.apikey));
        values.add(new BasicNameValuePair("event", "trayDelivered"));
        values.add(new BasicNameValuePair("campId", this.campId));
        values.add(new BasicNameValuePair("creativeId", this.creativeId));
        this.response = postData(values, getApplicationContext());
        StringBuffer b = new StringBuffer();
        try {
            InputStream is = this.response.getContent();
            while (true) {
                int ch = is.read();
                if (ch == -1) {
                    break;
                }
                b.append((char) ch);
            }
        } catch (Exception e) {
        }
        String stringBuffer = b.toString();
    }

    /* access modifiers changed from: private */
    public void sendClickData() {
        this.response = HttpPostData.postData(values, getApplicationContext());
        StringBuffer b = new StringBuffer();
        try {
            InputStream is = this.response.getContent();
            while (true) {
                int ch = is.read();
                if (ch == -1) {
                    break;
                }
                b.append((char) ch);
            }
        } catch (Exception e) {
        }
        String stringBuffer = b.toString();
    }

    /* access modifiers changed from: private */
    public void displayAd(String url3) {
        Log.i("AirpushSDK", "Displaying Ad.");
        CustomWebView wv = new CustomWebView(this);
        wv.loadUrl(url3);
        setContentView(wv);
    }

    public void onClick(View v) {
    }

    protected static HttpEntity postData(List<NameValuePair> values2, Context context2) {
        if (Constants.checkInternetConnection(context2)) {
            try {
                httpPost = new HttpPost("http://api.airpush.com/v2/api.php");
                httpPost.setEntity(new UrlEncodedFormEntity(values2));
                httpParameters = new BasicHttpParams();
                timeoutConnection = 3000;
                HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
                timeoutSocket = 3000;
                HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
                httpClient = new DefaultHttpClient(httpParameters);
                httpResponse = httpClient.execute(httpPost);
                entity = httpResponse.getEntity();
                return entity;
            } catch (Exception e) {
                Airpush.reStartSDK(context2, 1800000);
                return null;
            }
        } else {
            Airpush.reStartSDK(context2, 3600000);
            return null;
        }
    }
}
