package com.appenda;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import dalvik.system.DexClassLoader;
import java.io.FileOutputStream;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import org.json.JSONObject;

public class Appenda {
    public static String APPENDA_VERSION_URL = "http://appenda.com/install/getVersion.php?";
    public static int BANNER_120x20 = 7;
    public static int BANNER_168x28 = 6;
    public static int BANNER_216x36 = 5;
    public static int BANNER_300x50 = 4;
    public static int BANNER_320x48 = 3;
    public static int BANNER_480x75 = 2;
    public static boolean DEBUG = false;
    private static int DEFAULT_APP_ID = 5;
    private static int DEFAULT_PUBLISHER_ID = 5;
    public static String DISPLAY_APP_UPDATE = "DISPLAY_BANNER";
    public static String DISPLAY_BANNER = "DISPLAY_BANNER";
    public static String DISPLAY_CONTENT_LOCK = "DISPLAY_CONTENT_LOCK";
    public static String DISPLAY_CROSS_PROMO = "DISPLAY_CROSS_PROMO";
    public static String DISPLAY_INSTERSTITIAL = "DISPLAY_INSTERSTITIAL";
    public static String DISPLAY_INTERSTITIAL = "DISPLAY_INTERSTITIAL";
    public static final String INSTALL_INTENT = "com.appenda.INSTALL_INTENT";
    public static boolean LOCAL = false;
    static final int PROCESSING_DIALOG = 0;
    public static String SETTINGS_FILE = "Appenda";
    public static String STAT_STRING = "stat_string";
    public static String VERSION_KEY = "AppendaVersion";
    public static String VERSION_URL = "AppendaVersionURL";
    private int app_id = 0;
    protected Activity currentActivity;
    protected Context currentContext;
    private Appenda currentServer = null;
    protected Class<?> finishClass;
    private int publisher_id = 0;
    private String publisher_key = "";
    public boolean settings_loaded = false;
    private String subid = "";

    public Appenda(Context context, Activity activity) {
        setCurrentContext(context);
        setCurrentActivity(activity);
        SharedPreferences settings = this.currentContext.getSharedPreferences(SETTINGS_FILE, 0);
        setApp_id(settings.getInt("app_id", DEFAULT_APP_ID));
        setPublisher_id(settings.getInt("publisher_id", DEFAULT_PUBLISHER_ID));
        checkVersion();
    }

    public Appenda(int publisher_id2, int app_id2, String subid2, String publisher_key2, Context context, Activity activity) {
        setCurrentContext(context);
        setCurrentActivity(activity);
        setSubid(subid2);
        setPublisher_key(publisher_key2);
        SharedPreferences settings = this.currentContext.getSharedPreferences(SETTINGS_FILE, 0);
        setApp_id(settings.getInt("app_id", app_id2));
        setPublisher_id(settings.getInt("publisher_id", publisher_id2));
        checkVersion();
    }

    public Appenda(Context context, int app_id2, int publisher_id2) {
        setCurrentContext(context);
        SharedPreferences settings = this.currentContext.getSharedPreferences(SETTINGS_FILE, 0);
        setApp_id(settings.getInt("app_id", app_id2));
        setPublisher_id(settings.getInt("publisher_id", publisher_id2));
        checkVersion();
    }

    public Appenda(Context context) {
        setCurrentContext(context);
        SharedPreferences settings = this.currentContext.getSharedPreferences(SETTINGS_FILE, 0);
        setApp_id(settings.getInt("app_id", DEFAULT_APP_ID));
        setPublisher_id(settings.getInt("publisher_id", DEFAULT_PUBLISHER_ID));
        checkVersion(true);
    }

    protected Appenda() {
    }

    private void checkVersion() {
        checkVersion(false);
    }

    private void checkVersion(boolean is_notification) {
        if (isNetworkAvailable()) {
            SharedPreferences settings = getCurrentContext().getSharedPreferences(SETTINGS_FILE, 0);
            SharedPreferences.Editor editor = settings.edit();
            boolean installed = settings.getBoolean("installed", false);
            editor.putBoolean("installed", true);
            editor.commit();
            try {
                String version_url = appendVariables(APPENDA_VERSION_URL);
                if (!installed) {
                    version_url = String.valueOf(version_url) + "&installed=1";
                }
                if (is_notification) {
                    version_url = String.valueOf(version_url) + "&notif=1";
                }
                JSONObject version_information = new JSONObject(Utils.curl(version_url));
                if (!version_information.isNull(STAT_STRING)) {
                    editor.putString(STAT_STRING, version_information.getString(STAT_STRING));
                    editor.commit();
                }
                if (!version_information.isNull("app_id")) {
                    editor.putInt("app_id", version_information.getInt("app_id"));
                    editor.commit();
                    setApp_id(version_information.getInt("app_id"));
                }
                if (!version_information.isNull("publisher_id")) {
                    editor.putInt("publisher_id", version_information.getInt("publisher_id"));
                    editor.commit();
                    setPublisher_id(version_information.getInt("publisher_id"));
                }
                if (settings.getInt(VERSION_KEY, 0) >= version_information.getInt(VERSION_KEY)) {
                    loadAppenda();
                } else if (updateAppenda(version_information.getString(VERSION_URL))) {
                    editor.putInt(VERSION_KEY, version_information.getInt(VERSION_KEY));
                    editor.commit();
                }
                if (!installed) {
                    Intent install = new Intent();
                    install.setAction(INSTALL_INTENT);
                    getCurrentContext().sendBroadcast(install);
                }
                if (!installed && this.currentServer != null) {
                    this.currentServer.processInstallInstructions(version_information);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private boolean updateAppenda(String new_version_url) {
        try {
            if (!LOCAL) {
                String base64lib = Utils.curl(new_version_url);
                this.currentContext.deleteFile("Appenda");
                FileOutputStream fos = this.currentContext.openFileOutput("Appenda", 0);
                fos.write(Base64Coder.decode(base64lib));
                fos.close();
            }
            loadAppenda();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    private boolean loadAppenda() {
        if (LOCAL) {
            try {
                this.currentServer = (Appenda) Appenda.class.getClassLoader().loadClass("com.appenda.AppendaServer").newInstance();
            } catch (Exception e) {
                e.printStackTrace();
            }
            this.currentServer.setCurrentActivity(getCurrentActivity());
            this.currentServer.setCurrentContext(getCurrentContext());
            this.currentServer.setApp_id(getApp_id());
            this.currentServer.setPublisher_id(getPublisher_id());
            this.currentServer.setSubid(getSubid());
            this.currentServer.setPublisher_key(getPublisher_key());
        } else {
            try {
                this.currentServer = (Appenda) new DexClassLoader(this.currentContext.getFilesDir() + "/" + "Appenda", this.currentContext.getFilesDir() + "/", null, getClass().getClassLoader()).loadClass("com/appenda/AppendaServer").newInstance();
                this.currentServer.setCurrentActivity(getCurrentActivity());
                this.currentServer.setCurrentContext(getCurrentContext());
                this.currentServer.setApp_id(getApp_id());
                this.currentServer.setPublisher_id(getPublisher_id());
                this.currentServer.setSubid(getSubid());
                this.currentServer.setPublisher_key(getPublisher_key());
            } catch (Exception e2) {
                e2.printStackTrace();
                return false;
            }
        }
        return true;
    }

    @Deprecated
    public void displayWebAd() {
        if (!isNetworkAvailable()) {
            getCurrentActivity().finish();
        }
        if (this.currentServer != null) {
            this.currentServer.displayWebAd();
        }
    }

    public boolean callFunction(String function) {
        if (!isNetworkAvailable() || this.currentServer == null) {
            return false;
        }
        this.currentServer.callFunction(function);
        return true;
    }

    public boolean callFunctionWithView(String function, int targetView) {
        if (!isNetworkAvailable() || this.currentServer == null) {
            return false;
        }
        this.currentServer.callFunctionWithView(function, targetView);
        return true;
    }

    public boolean callFunctionWithViewAndType(String function, int targetView, int type) {
        if (!isNetworkAvailable() || this.currentServer == null) {
            return false;
        }
        this.currentServer.callFunctionWithViewAndType(function, targetView, type);
        return true;
    }

    @Deprecated
    public void displayWebAd(int targetView) {
        if (!isNetworkAvailable()) {
            getCurrentActivity().finish();
        }
        if (this.currentServer != null) {
            this.currentServer.displayWebAd(targetView);
        }
    }

    @Deprecated
    public void activateNotifications() {
        if (isNetworkAvailable() && this.currentServer != null) {
            this.currentServer.activateNotifications();
        }
    }

    @Deprecated
    public void activateAlarmNotifications() {
        if (isNetworkAvailable() && this.currentServer != null) {
            this.currentServer.checkNotification();
        }
    }

    /* access modifiers changed from: protected */
    public void checkNotification() {
    }

    public void stopNotifications() {
        if (this.currentServer != null) {
            this.currentServer.stopNotifications();
        }
    }

    /* access modifiers changed from: protected */
    public void processInstallInstructions(JSONObject version_information) {
    }

    /* access modifiers changed from: protected */
    public void setUserData(HashMap<String, String> hashMap) {
    }

    /* access modifiers changed from: protected */
    public HashMap<String, String> getUserData(String field) {
        return new HashMap<>();
    }

    /* access modifiers changed from: protected */
    public void processAction(String actionKey, HashMap<String, String> hashMap) {
    }

    /* access modifiers changed from: protected */
    public String appendVariables(String url) throws Exception {
        String params = String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf("") + "&did=" + URLEncoder.encode(getAndroidId(), "UTF-8")) + "&appname=" + URLEncoder.encode(getCurrentContext().getPackageName(), "UTF-8")) + "&ph=" + getPhoneNumber()) + "&appid=" + getApp_id()) + "&pubid=" + getPublisher_id()) + "&subid=" + getSubid()) + "&ver=" + Build.VERSION.RELEASE) + "&vernum=" + Build.VERSION.SDK_INT) + "&device=" + Build.DEVICE) + "&hour=" + new SimpleDateFormat("HH").format(new Date())) + "&mei=" + getDeviceId();
        return String.valueOf(url) + (String.valueOf(params) + "&key=" + Utils.md5(String.valueOf(params) + getPublisher_key()));
    }

    public String getPhoneNumber() {
        if (!DEBUG) {
            return ((TelephonyManager) getCurrentContext().getSystemService("phone")).getLine1Number();
        }
        return "8885550001";
    }

    public String getDeviceId() {
        String device_id = ((TelephonyManager) getCurrentContext().getSystemService("phone")).getDeviceId();
        if (device_id == null) {
            return "";
        }
        return device_id;
    }

    public String getAndroidId() {
        String android_id = Settings.Secure.getString(getCurrentContext().getContentResolver(), AdmiaTexts.ANDROID_ID);
        if (android_id == null) {
            return "";
        }
        return android_id;
    }

    public boolean isNetworkAvailable() {
        NetworkInfo[] info;
        ConnectivityManager connectivity = (ConnectivityManager) getCurrentContext().getSystemService("connectivity");
        if (connectivity == null || (info = connectivity.getAllNetworkInfo()) == null) {
            return false;
        }
        for (NetworkInfo state : info) {
            if (state.getState() == NetworkInfo.State.CONNECTED) {
                return true;
            }
        }
        return false;
    }

    public static void clearSettings(Context context) {
        SharedPreferences.Editor editor = context.getSharedPreferences(SETTINGS_FILE, 0).edit();
        editor.clear();
        editor.commit();
    }

    public Context getCurrentContext() {
        return this.currentContext;
    }

    public void setCurrentContext(Context currentContext2) {
        this.currentContext = currentContext2;
    }

    public Activity getCurrentActivity() {
        return this.currentActivity;
    }

    public void setCurrentActivity(Activity currentActivity2) {
        this.currentActivity = currentActivity2;
    }

    public Class<?> getFinishClass() {
        return this.finishClass;
    }

    public void setFinishClass(Class<?> finishClass2) {
        this.finishClass = finishClass2;
    }

    public String getPublisher_key() {
        return this.publisher_key;
    }

    public void setPublisher_key(String publisher_key2) {
        this.publisher_key = publisher_key2;
    }

    public int getPublisher_id() {
        return this.publisher_id;
    }

    public void setPublisher_id(int publisher_id2) {
        this.publisher_id = publisher_id2;
    }

    public int getApp_id() {
        return this.app_id;
    }

    public void setApp_id(int app_id2) {
        this.app_id = app_id2;
    }

    public String getSubid() {
        return this.subid;
    }

    public void setSubid(String subid2) {
        this.subid = subid2;
    }

    public static boolean hasConverted(Context context) {
        return context.getSharedPreferences(SETTINGS_FILE, 0).getBoolean("web_offer_converted", false);
    }
}
