package com.appenda;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

public class AppNotify extends Service {
    private Appenda slinger = null;

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onCreate() {
        super.onCreate();
        Log.d("Notifier going", "Notifier going");
    }

    public void onDestroy() {
        super.onDestroy();
        this.slinger.stopNotifications();
    }

    public void onStart(Intent intent, int startId) {
        this.slinger = new Appenda(getApplicationContext());
        this.slinger.activateAlarmNotifications();
    }
}
