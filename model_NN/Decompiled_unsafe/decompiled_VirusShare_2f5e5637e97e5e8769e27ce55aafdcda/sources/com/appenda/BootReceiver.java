package com.appenda;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;
import android.util.Log;

public class BootReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        int retry_time = 3600000;
        if (Appenda.DEBUG) {
            Log.d("BroadCast Reciever", "Starting up the service");
            retry_time = 30000;
        }
        ((AlarmManager) context.getSystemService("alarm")).setRepeating(2, SystemClock.elapsedRealtime(), (long) retry_time, PendingIntent.getService(context, 0, new Intent(context, AppNotify.class), 0));
    }
}
