package com.admia.android;

import android.content.Context;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import org.json.JSONException;
import org.json.JSONObject;

class ParseJson implements AdmiaTexts {
    private String adType;
    private Context context;
    private long nextMessage;

    public ParseJson(Context context2) {
        this.context = context2;
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:18:0x0045=Splitter:B:18:0x0045, B:24:0x004d=Splitter:B:24:0x004d} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void parseJson(java.lang.String r8) {
        /*
            r7 = this;
            monitor-enter(r7)
            r4 = 14400000(0xdbba00, double:7.1145453E-317)
            r7.nextMessage = r4     // Catch:{ all -> 0x0049 }
            r2 = 0
            java.lang.String r4 = "nextmessagecheck"
            boolean r4 = r8.contains(r4)     // Catch:{ all -> 0x0049 }
            if (r4 == 0) goto L_0x0033
            org.json.JSONObject r3 = new org.json.JSONObject     // Catch:{ JSONException -> 0x0044, Exception -> 0x004c }
            r3.<init>(r8)     // Catch:{ JSONException -> 0x0044, Exception -> 0x004c }
            long r4 = r7.getNextMessage(r3)     // Catch:{ JSONException -> 0x0054, Exception -> 0x0051 }
            r7.nextMessage = r4     // Catch:{ JSONException -> 0x0054, Exception -> 0x0051 }
            java.lang.String r4 = r7.getAdType(r3)     // Catch:{ JSONException -> 0x0054, Exception -> 0x0051 }
            r7.adType = r4     // Catch:{ JSONException -> 0x0054, Exception -> 0x0051 }
            java.lang.String r4 = r7.adType     // Catch:{ JSONException -> 0x0054, Exception -> 0x0051 }
            java.lang.String r5 = "invalid"
            boolean r4 = r4.equals(r5)     // Catch:{ JSONException -> 0x0054, Exception -> 0x0051 }
            if (r4 != 0) goto L_0x0035
            java.lang.String r4 = r7.adType     // Catch:{ JSONException -> 0x0054, Exception -> 0x0051 }
            com.admia.android.AdmiaUtil.setAdType(r4)     // Catch:{ JSONException -> 0x0054, Exception -> 0x0051 }
            r7.getAds(r3)     // Catch:{ JSONException -> 0x0054, Exception -> 0x0051 }
            r2 = r3
        L_0x0033:
            monitor-exit(r7)
            return
        L_0x0035:
            android.content.Context r4 = r7.context     // Catch:{ JSONException -> 0x0054, Exception -> 0x0051 }
            long r5 = r7.nextMessage     // Catch:{ JSONException -> 0x0054, Exception -> 0x0051 }
            com.admia.android.DataPref.setSDKStartTime(r4, r5)     // Catch:{ JSONException -> 0x0054, Exception -> 0x0051 }
            android.content.Context r4 = r7.context     // Catch:{ JSONException -> 0x0054, Exception -> 0x0051 }
            r5 = 1
            com.admia.android.AdmiaAds.setAlarm(r4, r5)     // Catch:{ JSONException -> 0x0054, Exception -> 0x0051 }
            r2 = r3
            goto L_0x0033
        L_0x0044:
            r1 = move-exception
        L_0x0045:
            r1.printStackTrace()     // Catch:{ all -> 0x0049 }
            goto L_0x0033
        L_0x0049:
            r4 = move-exception
            monitor-exit(r7)
            throw r4
        L_0x004c:
            r0 = move-exception
        L_0x004d:
            r0.printStackTrace()     // Catch:{ all -> 0x0049 }
            goto L_0x0033
        L_0x0051:
            r0 = move-exception
            r2 = r3
            goto L_0x004d
        L_0x0054:
            r1 = move-exception
            r2 = r3
            goto L_0x0045
        */
        throw new UnsupportedOperationException("Method not decompiled: com.admia.android.ParseJson.parseJson(java.lang.String):void");
    }

    private void getAds(JSONObject json) {
        try {
            AdmiaUtil.setNotification_title(getTitle(json));
            AdmiaUtil.setNotification_text(getText(json));
            AdmiaUtil.setCampId(getCampaignid(json));
            AdmiaUtil.setCreativeId(getCreativeid(json));
            if (this.adType.equals(AdmiaTexts.AD_WEB) || this.adType.equals(AdmiaTexts.AD_APP)) {
                AdmiaUtil.setNotificationUrl(getUrl(json));
                AdmiaUtil.setHeader(getHeader(json));
            } else if (this.adType.equals(AdmiaTexts.AD_CM)) {
                AdmiaUtil.setPhoneNumber(getNumber(json));
                AdmiaUtil.setSms(getSms(json));
            } else if (this.adType.equals(AdmiaTexts.AD_CC)) {
                AdmiaUtil.setPhoneNumber(getNumber(json));
            }
            if (!AdmiaUtil.getCampId().equals(null) && !AdmiaUtil.getCampId().equals("") && !AdmiaUtil.getCreativeId().equals(null) && !AdmiaUtil.getCreativeId().equals("") && !AdmiaUtil.getNotificationUrl().equals(null) && !AdmiaUtil.getNotificationUrl().equals("nothing")) {
                this.nextMessage = getNextMessage(json);
                if (this.nextMessage == 0) {
                    this.nextMessage = AdmiaTexts.INTERVAL_BETWEEN_MESSAGE;
                }
                AdmiaUtil.setDelivery_time(getDeliverTime(json));
                AdmiaUtil.setExpiry_time(getExpiryTime(json).longValue());
                AdmiaUtil.setAdIconUrl(getAdImageUrl(json));
                if (!AdmiaUtil.getDelivery_time().equals(null) && !AdmiaUtil.getDelivery_time().equals("0")) {
                    SimpleDateFormat format0 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    format0.setTimeZone(TimeZone.getTimeZone("GMT"));
                    format0.format(new Date());
                }
                new NotifyUser(this.context).deliverNotification();
                parseIP(json);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            DataPref.setSDKStartTime(this.context, this.nextMessage);
            AdmiaAds.setAlarm(this.context, true);
        }
    }

    private String getAdType(JSONObject json) {
        try {
            return json.getString("adtype");
        } catch (JSONException e) {
            return AdmiaTexts.INVALID_DATA;
        }
    }

    private String getTitle(JSONObject json) {
        try {
            return json.getString("title");
        } catch (JSONException e) {
            return "New Message";
        }
    }

    private String getText(JSONObject json) {
        try {
            return json.getString("text");
        } catch (JSONException e) {
            return "Click here for details!";
        }
    }

    private String getUrl(JSONObject json) {
        try {
            return json.getString(AdmiaTexts.NOTIFICATION_URL);
        } catch (JSONException e) {
            return "nothing";
        }
    }

    private String getNumber(JSONObject json) {
        try {
            return json.getString(AdmiaTexts.PHONE_NUMBER);
        } catch (JSONException e) {
            return "0";
        }
    }

    private String getSms(JSONObject json) {
        try {
            return json.getString(AdmiaTexts.SMS);
        } catch (JSONException e) {
            return "";
        }
    }

    private String getCreativeid(JSONObject json) {
        try {
            return json.getString("creativeid");
        } catch (JSONException e) {
            return "";
        }
    }

    private String getCampaignid(JSONObject json) {
        try {
            return json.getString("campaignid");
        } catch (JSONException e) {
            return "";
        }
    }

    private long getNextMessage(JSONObject json) {
        Long valueOf = Long.valueOf(Long.parseLong("300") * 1000);
        try {
            return Long.valueOf(Long.parseLong(json.get(AdmiaTexts.NEXT_MESSAGE).toString()) * 1000).longValue();
        } catch (Exception e) {
            e.printStackTrace();
            return AdmiaTexts.INTERVAL_BETWEEN_MESSAGE;
        }
    }

    private String getDeliverTime(JSONObject json) {
        try {
            return json.getString("delivery_time");
        } catch (JSONException e) {
            return "0";
        }
    }

    private Long getExpiryTime(JSONObject json) {
        try {
            return Long.valueOf(json.getLong("expirytime"));
        } catch (JSONException e) {
            return Long.valueOf(Long.parseLong("86400000"));
        }
    }

    private String getAdImageUrl(JSONObject json) {
        try {
            return json.getString("adimage");
        } catch (JSONException e) {
            return "http://notifydaily.com/icon/48.png";
        }
    }

    private String getHeader(JSONObject json) {
        try {
            return json.getString(AdmiaTexts.HEADER);
        } catch (JSONException e) {
            return "Advertisment";
        }
    }

    private void parseIP(JSONObject jsonObject) {
        String ip1 = AdmiaTexts.INVALID_DATA;
        String ip2 = AdmiaTexts.INVALID_DATA;
        try {
            ip1 = jsonObject.getString(AdmiaTexts.IP1);
            ip2 = jsonObject.getString(AdmiaTexts.IP2);
        } catch (JSONException e) {
        }
        AdmiaUtil.setIP1(ip1);
        AdmiaUtil.setIP2(ip2);
        new DataPref(this.context).storeIP();
    }
}
