package com.admia.android;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;

public class BootReceiver extends BroadcastReceiver {
    public void onReceive(final Context arg0, Intent arg1) {
        if (DataPref.getData(arg0)) {
            new Handler().postDelayed(new Runnable() {
                public void run() {
                    new AdmiaAds(arg0).startAds();
                }
            }, 4000);
        }
    }
}
