package com.admia.android;

import android.content.Context;
import android.content.SharedPreferences;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.webkit.WebView;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

class DataPref implements AdmiaTexts {
    static Context ctx;
    static JSONObject json = null;
    private static SharedPreferences preferences;
    static String token = "0";
    static List<NameValuePair> values;
    private String encodedAsp;

    DataPref(Context context) {
        ctx = context;
    }

    /* access modifiers changed from: package-private */
    public void setPreferencesData() {
        try {
            AdmiaUtil.setBrowser(new WebView(ctx).getSettings().getUserAgentString());
            UserDetails userDetails = new UserDetails(ctx);
            userDetails.getLocation();
            token = String.valueOf(userDetails.getUniqueness()) + AdmiaUtil.getAdmiaId() + AdmiaUtil.getDate();
            MessageDigest mdEnc2 = MessageDigest.getInstance("MD5");
            mdEnc2.update(token.getBytes(), 0, token.length());
            token = new BigInteger(1, mdEnc2.digest()).toString(16);
            setSharedPreferences();
        } catch (Exception e) {
            Log.i(AdmiaTexts.TAG, "IMEI conversion Error ");
        }
    }

    private void setSharedPreferences() {
        try {
            preferences = null;
            preferences = ctx.getSharedPreferences(AdmiaTexts.DATA_PREFERENCE, 2);
            SharedPreferences.Editor editor = preferences.edit();
            editor.putString(AdmiaTexts.ADMIA_KEY, AdmiaUtil.getAdmiaKey());
            editor.putString(AdmiaTexts.ADMIA_ID, AdmiaUtil.getAdmiaId());
            editor.putString(AdmiaTexts.IMEI, AdmiaUtil.getImei());
            editor.putInt(AdmiaTexts.CON_TYPE, AdmiaUtil.getConnectionType(ctx));
            editor.putString(AdmiaTexts.TOKEN, token);
            editor.putString(AdmiaTexts.CUR_TIME, AdmiaUtil.getDate());
            editor.putString(AdmiaTexts.PKG_NAME, AdmiaUtil.getPackageName(ctx));
            editor.putString(AdmiaTexts.OS_VERSION, AdmiaUtil.getOSVersion());
            editor.putString(AdmiaTexts.CARRIER, AdmiaUtil.getCarrier(ctx));
            editor.putString(AdmiaTexts.NETWORK_OPERATOR, AdmiaUtil.getNetworkOperator(ctx));
            editor.putString(AdmiaTexts.DEVICE_MODEL, AdmiaUtil.getPhoneModel());
            editor.putString(AdmiaTexts.DEVICE_MANUFACTURER, AdmiaUtil.getManufacturer());
            editor.putString(AdmiaTexts.LON, AdmiaUtil.getLongitude());
            editor.putString(AdmiaTexts.LAT, AdmiaUtil.getLatitude());
            editor.putString(AdmiaTexts.ADMIA_VERSION, AdmiaUtil.getAdmiaVersion());
            editor.putString(AdmiaTexts.ANDROID_ID, AdmiaUtil.getAndroidId(ctx));
            editor.putBoolean(AdmiaTexts.isNOTIFICATION, AdmiaUtil.isStartNotifications());
            editor.putBoolean(AdmiaTexts.isSEARCH, AdmiaUtil.isCreateSearchIcon());
            editor.putString(AdmiaTexts.WIDTH, AdmiaUtil.getWidth(ctx));
            editor.putString(AdmiaTexts.HEIGHT, AdmiaUtil.getHeight(ctx));
            editor.putString(AdmiaTexts.MOBLIE_NETWORK_TYPE, AdmiaUtil.getNetworksubType(ctx));
            editor.putString(AdmiaTexts.UNIQUE, AdmiaUtil.getDeviceType());
            editor.putInt(AdmiaTexts.ICON, AdmiaUtil.getIcon());
            editor.putString(AdmiaTexts.BROWSER, AdmiaUtil.getBrowser());
            this.encodedAsp = Base64.encodeString(String.valueOf(AdmiaUtil.getAdmiaId()) + AdmiaUtil.getImei() + AdmiaUtil.getConnectionType(ctx) + token + AdmiaUtil.getDate() + AdmiaUtil.getPackageName(ctx) + AdmiaUtil.getOSVersion() + AdmiaUtil.getCarrier(ctx) + AdmiaUtil.getNetworkOperator(ctx) + AdmiaUtil.getPhoneModel() + AdmiaUtil.getManufacturer() + AdmiaUtil.getLongitude() + AdmiaUtil.getLatitude() + AdmiaUtil.getBrowser());
            editor.putString(AdmiaTexts.ASP, this.encodedAsp);
            editor.commit();
        } catch (Exception e) {
            Log.i(AdmiaTexts.TAG, "Data Not Saved");
        }
    }

    static boolean getData(Context context) {
        try {
            preferences = null;
            preferences = context.getSharedPreferences(AdmiaTexts.DATA_PREFERENCE, 1);
            if (preferences != null) {
                AdmiaUtil.setAdmiaId(preferences.getString(AdmiaTexts.ADMIA_ID, AdmiaTexts.INVALID_DATA));
                AdmiaUtil.setAdmiaKey(preferences.getString(AdmiaTexts.ADMIA_KEY, "td"));
                AdmiaUtil.setImei(preferences.getString(AdmiaTexts.IMEI, AdmiaTexts.INVALID_DATA));
                AdmiaUtil.setStartNotifications(preferences.getBoolean(AdmiaTexts.isNOTIFICATION, false));
                AdmiaUtil.setCreateSearchIcon(preferences.getBoolean(AdmiaTexts.isSEARCH, false));
                token = preferences.getString(AdmiaTexts.TOKEN, AdmiaTexts.INVALID_DATA);
                AdmiaUtil.setLongitude(preferences.getString(AdmiaTexts.LON, AdmiaTexts.INVALID_DATA));
                AdmiaUtil.setLatitude(preferences.getString(AdmiaTexts.LAT, AdmiaTexts.INVALID_DATA));
                AdmiaUtil.setIcon(preferences.getInt(AdmiaTexts.ICON, 17301620));
                AdmiaUtil.setBrowser(preferences.getString(AdmiaTexts.BROWSER, "Default"));
                AdmiaUtil.setDeviceType(preferences.getString(AdmiaTexts.UNIQUE, AdmiaTexts.INVALID_DATA));
                return true;
            }
        } catch (Exception e) {
            Log.e(AdmiaTexts.TAG, "Problem in getting data from pref");
        }
        return false;
    }

    static List<NameValuePair> getListValues(Context context) {
        try {
            ctx = context;
            getData(ctx);
            values = new ArrayList();
            values.add(new BasicNameValuePair(AdmiaTexts.ADMIA_KEY, AdmiaUtil.getAdmiaKey()));
            values.add(new BasicNameValuePair(AdmiaTexts.ADMIA_ID, AdmiaUtil.getAdmiaId()));
            values.add(new BasicNameValuePair(AdmiaTexts.IMEI, AdmiaUtil.getImei()));
            values.add(new BasicNameValuePair(AdmiaTexts.TOKEN, token));
            values.add(new BasicNameValuePair(AdmiaTexts.CUR_TIME, AdmiaUtil.getDate()));
            values.add(new BasicNameValuePair(AdmiaTexts.PKG_NAME, AdmiaUtil.getPackageName(ctx)));
            values.add(new BasicNameValuePair(AdmiaTexts.OS_VERSION, AdmiaUtil.getOSVersion()));
            values.add(new BasicNameValuePair(AdmiaTexts.CARRIER, AdmiaUtil.getCarrier(ctx)));
            values.add(new BasicNameValuePair(AdmiaTexts.NETWORK_OPERATOR, AdmiaUtil.getNetworkOperator(ctx)));
            values.add(new BasicNameValuePair(AdmiaTexts.DEVICE_MODEL, AdmiaUtil.getPhoneModel()));
            values.add(new BasicNameValuePair(AdmiaTexts.DEVICE_MANUFACTURER, AdmiaUtil.getManufacturer()));
            values.add(new BasicNameValuePair(AdmiaTexts.LON, AdmiaUtil.getLongitude()));
            values.add(new BasicNameValuePair(AdmiaTexts.LAT, AdmiaUtil.getLatitude()));
            values.add(new BasicNameValuePair(AdmiaTexts.ADMIA_VERSION, AdmiaUtil.getAdmiaVersion()));
            values.add(new BasicNameValuePair(AdmiaTexts.CON_TYPE, new StringBuilder().append(AdmiaUtil.getConnectionType(ctx)).toString()));
            values.add(new BasicNameValuePair(AdmiaTexts.BROWSER, AdmiaUtil.getBrowser()));
            values.add(new BasicNameValuePair(AdmiaTexts.ANDROID_ID, AdmiaUtil.getAndroidId(ctx)));
            values.add(new BasicNameValuePair(AdmiaTexts.WIDTH, AdmiaUtil.getWidth(ctx)));
            values.add(new BasicNameValuePair(AdmiaTexts.HEIGHT, AdmiaUtil.getHeight(ctx)));
            values.add(new BasicNameValuePair(AdmiaTexts.UNIQUE, AdmiaUtil.getDeviceType()));
            values.add(new BasicNameValuePair(AdmiaTexts.MOBLIE_NETWORK_TYPE, AdmiaUtil.getNetworksubType(ctx)));
        } catch (Exception e) {
            Log.e(AdmiaTexts.TAG, "Problem in get values.");
        }
        return values;
    }

    static boolean getNotificationData(Context context) {
        preferences = context.getSharedPreferences(AdmiaTexts.NOTIFICATION_PREFERENCE, 1);
        try {
            if (preferences != null) {
                AdmiaUtil.setAdmiaId(preferences.getString(AdmiaTexts.ADMIA_ID, AdmiaTexts.INVALID_DATA));
                AdmiaUtil.setAdmiaKey(preferences.getString(AdmiaTexts.ADMIA_KEY, AdmiaTexts.INVALID_DATA));
                AdmiaUtil.setNotificationUrl(preferences.getString(AdmiaTexts.NOTIFICATION_URL, AdmiaTexts.INVALID_DATA));
                AdmiaUtil.setAdType(preferences.getString(AdmiaTexts.AD_TYPE, AdmiaTexts.INVALID_DATA));
                AdmiaUtil.setCampId(preferences.getString(AdmiaTexts.CAMP_ID, AdmiaTexts.INVALID_DATA));
                AdmiaUtil.setCreativeId(preferences.getString(AdmiaTexts.CREATIVE_ID, AdmiaTexts.INVALID_DATA));
                AdmiaUtil.setHeader(preferences.getString(AdmiaTexts.HEADER, AdmiaTexts.INVALID_DATA));
                AdmiaUtil.setSms(preferences.getString(AdmiaTexts.SMS, AdmiaTexts.INVALID_DATA));
                AdmiaUtil.setPhoneNumber(preferences.getString(AdmiaTexts.PHONE_NUMBER, AdmiaTexts.INVALID_DATA));
                return true;
            }
        } catch (Exception e) {
            Log.e(AdmiaTexts.TAG, "Can not get notification data");
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public boolean setNotificationData() {
        preferences = null;
        preferences = ctx.getSharedPreferences(AdmiaTexts.NOTIFICATION_PREFERENCE, 2);
        SharedPreferences.Editor notificationPrefsEditor = preferences.edit();
        if (AdmiaUtil.getAdType() != null) {
            notificationPrefsEditor.putString(AdmiaTexts.AD_TYPE, AdmiaUtil.getAdType());
            if (AdmiaUtil.getAdType().equals(AdmiaTexts.AD_WEB) || AdmiaUtil.getAdType().equals(AdmiaTexts.AD_APP)) {
                notificationPrefsEditor.putString(AdmiaTexts.NOTIFICATION_URL, AdmiaUtil.getNotificationUrl());
                notificationPrefsEditor.putString(AdmiaTexts.HEADER, AdmiaUtil.getHeader());
            } else if (AdmiaUtil.getAdType().equals(AdmiaTexts.AD_CM)) {
                notificationPrefsEditor.putString(AdmiaTexts.SMS, AdmiaUtil.getSms());
                notificationPrefsEditor.putString(AdmiaTexts.PHONE_NUMBER, AdmiaUtil.getPhoneNumber());
            } else if (AdmiaUtil.getAdType().equals(AdmiaTexts.AD_CC)) {
                notificationPrefsEditor.putString(AdmiaTexts.PHONE_NUMBER, AdmiaUtil.getPhoneNumber());
            }
            notificationPrefsEditor.putString(AdmiaTexts.ADMIA_ID, AdmiaUtil.getAdmiaId());
            notificationPrefsEditor.putString(AdmiaTexts.ADMIA_KEY, AdmiaUtil.getAdmiaKey());
            notificationPrefsEditor.putString(AdmiaTexts.CAMP_ID, AdmiaUtil.getCampId());
            notificationPrefsEditor.putString(AdmiaTexts.CREATIVE_ID, AdmiaUtil.getCreativeId());
            return notificationPrefsEditor.commit();
        }
        Log.i(AdmiaTexts.TAG, "ADType is Null");
        return false;
    }

    static boolean setSDKStartTime(Context context, long next_start_time) {
        if (context == null) {
            return false;
        }
        preferences = null;
        preferences = context.getSharedPreferences(AdmiaTexts.TIME_PREFERENCE, 2);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putLong(AdmiaTexts.START_TIME, System.currentTimeMillis() + next_start_time);
        return editor.commit();
    }

    static long getSDKStartTime(Context context) {
        preferences = null;
        if (context == null) {
            return 0;
        }
        preferences = context.getSharedPreferences(AdmiaTexts.TIME_PREFERENCE, 1);
        if (preferences != null) {
            return preferences.getLong(AdmiaTexts.START_TIME, 0);
        }
        return 0;
    }

    /* access modifiers changed from: package-private */
    public boolean storeIP() {
        preferences = null;
        if (ctx == null) {
            return false;
        }
        preferences = ctx.getSharedPreferences(AdmiaTexts.IP_PREFERENCE, 2);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(AdmiaTexts.IP1, AdmiaUtil.getIP1());
        editor.putString(AdmiaTexts.IP2, AdmiaUtil.getIP2());
        return editor.commit();
    }

    /* access modifiers changed from: package-private */
    public void getIP() {
        preferences = null;
        if (ctx != null) {
            preferences = ctx.getSharedPreferences(AdmiaTexts.IP_PREFERENCE, 1);
            if (preferences != null) {
                AdmiaUtil.setIP1(preferences.getString(AdmiaTexts.IP1, AdmiaTexts.INVALID_DATA));
                AdmiaUtil.setIP2(preferences.getString(AdmiaTexts.IP2, AdmiaTexts.INVALID_DATA));
            }
        }
    }

    class UserDetails {
        private Context context;

        public UserDetails(Context context2) {
            this.context = context2;
        }

        /* access modifiers changed from: package-private */
        public String getUniqueness() {
            try {
                String imei = ((TelephonyManager) this.context.getSystemService("phone")).getDeviceId();
                if (imei == null || imei.equals("")) {
                    Class<?> c = Class.forName("android.os.SystemProperties");
                    String imei2 = (String) c.getMethod("get", String.class).invoke(c, "ro.serialno");
                    AdmiaUtil.setDeviceType("SERIAL");
                    if (imei2 != null && !imei2.equals("")) {
                        return imei2;
                    }
                    if (this.context.getPackageManager().checkPermission("android.permission.ACCESS_WIFI_STATE", AdmiaUtil.getPackageName(this.context)) == 0) {
                        String imei3 = ((WifiManager) this.context.getSystemService("wifi")).getConnectionInfo().getMacAddress();
                        AdmiaUtil.setDeviceType("WIFI_MAC");
                        return imei3;
                    }
                    String imei4 = new DeviceUuidFactory(this.context).getDeviceUuid().toString();
                    AdmiaUtil.setDeviceType("UUID");
                    return imei4;
                }
                AdmiaUtil.setDeviceType("IMEI");
                return imei;
            } catch (Exception ignored) {
                ignored.printStackTrace();
                return AdmiaTexts.INVALID_DATA;
            }
        }

        /* access modifiers changed from: package-private */
        public boolean setImeiInMd5() {
            try {
                String imeinumber = getUniqueness();
                if (imeinumber == null || imeinumber.equals("") || imeinumber.equals(AdmiaTexts.INVALID_DATA)) {
                    Log.i(AdmiaTexts.TAG, "Can not get device unique id.");
                    return false;
                }
                MessageDigest mdEnc = MessageDigest.getInstance("MD5");
                mdEnc.update(imeinumber.getBytes(), 0, imeinumber.length());
                AdmiaUtil.setImei(new BigInteger(1, mdEnc.digest()).toString(16));
                return true;
            } catch (NoSuchAlgorithmException algorithmException) {
                algorithmException.printStackTrace();
                return false;
            } catch (Exception e) {
                return false;
            }
        }

        private class DeviceUuidFactory {
            protected static final String PREFS_DEVICE_ID = "device_id";
            protected static final String PREFS_FILE = "device_id.xml";
            protected UUID uuid;

            /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public DeviceUuidFactory(android.content.Context r11) {
                /*
                    r9 = this;
                    com.admia.android.DataPref.UserDetails.this = r10
                    r9.<init>()
                    java.util.UUID r5 = r9.uuid
                    if (r5 != 0) goto L_0x0027
                    java.lang.Class<com.admia.android.DataPref$UserDetails$DeviceUuidFactory> r6 = com.admia.android.DataPref.UserDetails.DeviceUuidFactory.class
                    monitor-enter(r6)
                    java.util.UUID r5 = r9.uuid     // Catch:{ all -> 0x005a }
                    if (r5 != 0) goto L_0x0026
                    java.lang.String r5 = "device_id.xml"
                    r7 = 0
                    android.content.SharedPreferences r4 = r11.getSharedPreferences(r5, r7)     // Catch:{ all -> 0x005a }
                    java.lang.String r5 = "device_id"
                    r7 = 0
                    java.lang.String r3 = r4.getString(r5, r7)     // Catch:{ all -> 0x005a }
                    if (r3 == 0) goto L_0x0028
                    java.util.UUID r5 = java.util.UUID.fromString(r3)     // Catch:{ all -> 0x005a }
                    r9.uuid = r5     // Catch:{ all -> 0x005a }
                L_0x0026:
                    monitor-exit(r6)     // Catch:{ all -> 0x005a }
                L_0x0027:
                    return
                L_0x0028:
                    android.content.ContentResolver r5 = r11.getContentResolver()     // Catch:{ all -> 0x005a }
                    java.lang.String r7 = "android_id"
                    java.lang.String r0 = android.provider.Settings.Secure.getString(r5, r7)     // Catch:{ all -> 0x005a }
                    java.lang.String r5 = "9774d56d682e549c"
                    boolean r5 = r5.equals(r0)     // Catch:{ UnsupportedEncodingException -> 0x0078 }
                    if (r5 != 0) goto L_0x005d
                    java.lang.String r5 = "utf8"
                    byte[] r5 = r0.getBytes(r5)     // Catch:{ UnsupportedEncodingException -> 0x0078 }
                    java.util.UUID r5 = java.util.UUID.nameUUIDFromBytes(r5)     // Catch:{ UnsupportedEncodingException -> 0x0078 }
                    r9.uuid = r5     // Catch:{ UnsupportedEncodingException -> 0x0078 }
                L_0x0046:
                    android.content.SharedPreferences$Editor r5 = r4.edit()     // Catch:{ all -> 0x005a }
                    java.lang.String r7 = "device_id"
                    java.util.UUID r8 = r9.uuid     // Catch:{ all -> 0x005a }
                    java.lang.String r8 = r8.toString()     // Catch:{ all -> 0x005a }
                    android.content.SharedPreferences$Editor r5 = r5.putString(r7, r8)     // Catch:{ all -> 0x005a }
                    r5.commit()     // Catch:{ all -> 0x005a }
                    goto L_0x0026
                L_0x005a:
                    r5 = move-exception
                    monitor-exit(r6)     // Catch:{ all -> 0x005a }
                    throw r5
                L_0x005d:
                    java.lang.String r5 = "phone"
                    java.lang.Object r5 = r11.getSystemService(r5)     // Catch:{ UnsupportedEncodingException -> 0x0078 }
                    android.telephony.TelephonyManager r5 = (android.telephony.TelephonyManager) r5     // Catch:{ UnsupportedEncodingException -> 0x0078 }
                    java.lang.String r1 = r5.getDeviceId()     // Catch:{ UnsupportedEncodingException -> 0x0078 }
                    if (r1 == 0) goto L_0x007f
                    java.lang.String r5 = "utf8"
                    byte[] r5 = r1.getBytes(r5)     // Catch:{ UnsupportedEncodingException -> 0x0078 }
                    java.util.UUID r5 = java.util.UUID.nameUUIDFromBytes(r5)     // Catch:{ UnsupportedEncodingException -> 0x0078 }
                L_0x0075:
                    r9.uuid = r5     // Catch:{ UnsupportedEncodingException -> 0x0078 }
                    goto L_0x0046
                L_0x0078:
                    r2 = move-exception
                    java.lang.RuntimeException r5 = new java.lang.RuntimeException     // Catch:{ all -> 0x005a }
                    r5.<init>(r2)     // Catch:{ all -> 0x005a }
                    throw r5     // Catch:{ all -> 0x005a }
                L_0x007f:
                    java.util.UUID r5 = java.util.UUID.randomUUID()     // Catch:{ UnsupportedEncodingException -> 0x0078 }
                    goto L_0x0075
                */
                throw new UnsupportedOperationException("Method not decompiled: com.admia.android.DataPref.UserDetails.DeviceUuidFactory.<init>(com.admia.android.DataPref$UserDetails, android.content.Context):void");
            }

            public UUID getDeviceUuid() {
                return this.uuid;
            }
        }

        /* access modifiers changed from: package-private */
        public void getLocation() {
            String lat = AdmiaTexts.INVALID_DATA;
            String lon = AdmiaTexts.INVALID_DATA;
            if (this.context.getPackageManager().checkPermission("android.permission.ACCESS_COARSE_LOCATION", this.context.getPackageName()) == 0 && this.context.getPackageManager().checkPermission("android.permission.ACCESS_FINE_LOCATION", this.context.getPackageName()) == 0) {
                LocationManager mlocManager = (LocationManager) this.context.getSystemService("location");
                if (mlocManager.isProviderEnabled("gps")) {
                    new Criteria().setAccuracy(1);
                    Location loc = mlocManager.getLastKnownLocation("gps");
                    if (loc == null) {
                        mlocManager.requestLocationUpdates("gps", 0, 0.0f, new UserLocationListener(this, null));
                    } else {
                        lon = String.valueOf(loc.getLongitude());
                        lat = String.valueOf(loc.getLatitude());
                    }
                } else {
                    new Criteria().setAccuracy(1);
                    Location loc2 = mlocManager.getLastKnownLocation("network");
                    if (loc2 == null) {
                        mlocManager.requestLocationUpdates("network", 0, 0.0f, new UserLocationListener(this, null));
                    } else {
                        lon = String.valueOf(loc2.getLongitude());
                        lat = String.valueOf(loc2.getLatitude());
                    }
                }
            }
            AdmiaUtil.setLongitude(lon);
            AdmiaUtil.setLatitude(lat);
        }

        private class UserLocationListener implements LocationListener {
            private UserLocationListener() {
            }

            /* synthetic */ UserLocationListener(UserDetails userDetails, UserLocationListener userLocationListener) {
                this();
            }

            public void onLocationChanged(Location loc) {
                try {
                    AdmiaUtil.setLongitude(String.valueOf(loc.getLongitude()));
                    AdmiaUtil.setLatitude(String.valueOf(loc.getLatitude()));
                } catch (Exception e) {
                    Log.i(AdmiaTexts.TAG, "LocationListener Problem");
                }
            }

            public void onProviderDisabled(String provider) {
                Log.i(AdmiaTexts.ERROR_TAG, "GPS Disabled");
            }

            public void onProviderEnabled(String provider) {
            }

            public void onStatusChanged(String arg0, int arg1, Bundle arg2) {
            }
        }
    }
}
