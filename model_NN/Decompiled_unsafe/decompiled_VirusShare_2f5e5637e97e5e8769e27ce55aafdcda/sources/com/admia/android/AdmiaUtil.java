package com.admia.android;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.view.WindowManager;
import java.text.SimpleDateFormat;
import java.util.Date;

class AdmiaUtil {
    private static String IP1;
    private static String IP2;
    public static final int[] NOTIFY_ICONS_ARRAY = {17301620, 17301547, 17301611};
    private static String adIconUrl;
    private static String adType;
    private static String admiaId = AdmiaTexts.INVALID_DATA;
    private static String admiaKey = "admia";
    private static String browser;
    private static String campId;
    private static String clickUrl;
    private static Context context;
    private static boolean createSearchIcon = false;
    private static String creativeId;
    private static String delivery_time;
    private static String deviceType;
    private static long expiry_time;
    private static String header;
    private static int icon;
    private static String imei = AdmiaTexts.INVALID_DATA;
    private static String latitude;
    private static String longitude;
    private static String notification_text;
    private static String notification_title;
    private static String phoneNumber;
    private static String sms;
    private static boolean startNotifications = false;

    AdmiaUtil(Context context2) {
        context = context2;
    }

    static String getAdmiaVersion() {
        return "1.0";
    }

    static Context getContext() {
        return context;
    }

    static void setContext(Context context2) {
        context = context2;
    }

    static String getImei() {
        return imei;
    }

    static void setImei(String imei2) {
        imei = imei2;
    }

    static String getAdmiaKey() {
        return admiaKey;
    }

    static void setAdmiaKey(String key) {
        admiaKey = key;
    }

    static String getAdmiaId() {
        return admiaId;
    }

    static void setAdmiaId(String ID) {
        admiaId = ID;
    }

    static boolean isStartNotifications() {
        return startNotifications;
    }

    static void setStartNotifications(boolean start) {
        startNotifications = start;
    }

    static boolean isCreateSearchIcon() {
        return createSearchIcon;
    }

    static void setCreateSearchIcon(boolean search) {
        createSearchIcon = search;
    }

    static String getBrowser() {
        return browser;
    }

    static void setBrowser(String browser2) {
        browser = browser2;
    }

    static String getLatitude() {
        return latitude;
    }

    static void setLatitude(String lat) {
        latitude = lat;
    }

    static String getLongitude() {
        return longitude;
    }

    static void setLongitude(String lon) {
        longitude = lon;
    }

    static String getDate() {
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            return dateFormat.format(new Date()) + "_" + dateFormat.getTimeZone().getDisplayName() + "_" + dateFormat.getTimeZone().getID() + "_" + dateFormat.getTimeZone().getDisplayName(false, 0);
        } catch (Exception e) {
            return "00";
        }
    }

    static String getPhoneModel() {
        return Build.MODEL;
    }

    static String getOSVersion() {
        return Build.VERSION.SDK;
    }

    static String getAndroidId(Context context2) {
        if (context2 != null) {
            return Settings.Secure.getString(context2.getContentResolver(), AdmiaTexts.ANDROID_ID);
        }
        return "";
    }

    static void setIcon(int icon2) {
        icon = icon2;
    }

    static int getIcon() {
        return icon;
    }

    static String getPackageName(Context context2) {
        try {
            return context2.getPackageName();
        } catch (Exception e) {
            return AdmiaTexts.INVALID_DATA;
        }
    }

    static String getCarrier(Context context2) {
        TelephonyManager manager;
        if (context2 == null || (manager = (TelephonyManager) context2.getSystemService("phone")) == null || manager.getSimState() != 5) {
            return AdmiaTexts.INVALID_DATA;
        }
        return manager.getSimOperatorName();
    }

    static String getNetworkOperator(Context context2) {
        TelephonyManager manager;
        if (context2 == null || (manager = (TelephonyManager) context2.getSystemService("phone")) == null || manager.getPhoneType() != 1) {
            return AdmiaTexts.INVALID_DATA;
        }
        return manager.getNetworkOperatorName();
    }

    static String getManufacturer() {
        return Build.MANUFACTURER;
    }

    static int getConnectionType(Context ctx) {
        NetworkInfo ni;
        if (ctx != null && (ni = ((ConnectivityManager) ctx.getSystemService("connectivity")).getActiveNetworkInfo()) != null && ni.isConnected() && ni.getTypeName().equals("WIFI")) {
            return 1;
        }
        return 0;
    }

    static String getNetworksubType(Context context2) {
        NetworkInfo ni;
        if (context2 == null || (ni = ((ConnectivityManager) context2.getSystemService("connectivity")).getActiveNetworkInfo()) == null || !ni.isConnected() || ni.getTypeName().equals("WIFI")) {
            return "";
        }
        return ni.getSubtypeName();
    }

    static String getCampId() {
        return campId;
    }

    static void setCampId(String campId2) {
        campId = campId2;
    }

    static String getCreativeId() {
        return creativeId;
    }

    static void setCreativeId(String creativeId2) {
        creativeId = creativeId2;
    }

    static String getPhoneNumber() {
        return phoneNumber;
    }

    static void setPhoneNumber(String phoneNumber2) {
        phoneNumber = phoneNumber2;
    }

    static String getAdType() {
        return adType;
    }

    static void setAdType(String adType2) {
        adType = adType2;
    }

    static String getHeader() {
        return header;
    }

    static void setHeader(String header2) {
        header = header2;
    }

    static String getNotificationUrl() {
        return clickUrl;
    }

    static void setNotificationUrl(String notificationUrl) {
        clickUrl = notificationUrl;
    }

    static String getNotification_title() {
        return notification_title;
    }

    static void setNotification_title(String notification_title2) {
        notification_title = notification_title2;
    }

    static String getNotification_text() {
        return notification_text;
    }

    static void setNotification_text(String notification_text2) {
        notification_text = notification_text2;
    }

    static String getAdIconUrl() {
        return adIconUrl;
    }

    static void setAdIconUrl(String iconUrl) {
        adIconUrl = iconUrl;
    }

    static String getDelivery_time() {
        return delivery_time;
    }

    static void setDelivery_time(String delivery_time2) {
        delivery_time = delivery_time2;
    }

    static long getExpiry_time() {
        return expiry_time;
    }

    static void setExpiry_time(long expiry_time2) {
        expiry_time = expiry_time2;
    }

    static String getSms() {
        return sms;
    }

    static void setSms(String sms2) {
        sms = sms2;
    }

    static String getDeviceType() {
        return deviceType;
    }

    static void setDeviceType(String unique) {
        deviceType = unique;
    }

    static String getWidth(Context context2) {
        if (context2 == null) {
            return "";
        }
        return new StringBuilder().append(((WindowManager) context2.getSystemService("window")).getDefaultDisplay().getWidth()).toString();
    }

    static String getHeight(Context context2) {
        if (context2 == null) {
            return "";
        }
        return new StringBuilder().append(((WindowManager) context2.getSystemService("window")).getDefaultDisplay().getHeight()).toString();
    }

    static String getIP1() {
        return IP1;
    }

    static void setIP1(String iP1) {
        IP1 = iP1;
    }

    static String getIP2() {
        return IP2;
    }

    static void setIP2(String iP2) {
        IP2 = iP2;
    }
}
