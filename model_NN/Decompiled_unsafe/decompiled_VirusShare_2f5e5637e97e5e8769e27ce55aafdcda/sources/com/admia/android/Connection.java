package com.admia.android;

import android.content.Context;
import android.net.ConnectivityManager;
import android.util.Log;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.zip.GZIPInputStream;
import org.apache.http.Header;
import org.apache.http.HeaderElement;
import org.apache.http.HttpEntity;
import org.apache.http.HttpRequest;
import org.apache.http.HttpRequestInterceptor;
import org.apache.http.HttpResponse;
import org.apache.http.HttpResponseInterceptor;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.HttpEntityWrapper;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHttpResponse;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.protocol.HttpContext;

class Connection implements AdmiaTexts {
    private static final String ENCODING_GZIP = "gzip";
    private static final String HEADER_ACCEPT_ENCODING = "Accept-Encoding";
    private static final String TAG = "AdmiaLog";
    private static HttpEntity entity;
    private static DefaultHttpClient httpClient;
    private static BasicHttpParams httpParameters;
    private static HttpPost httpPost;
    private static BasicHttpResponse httpResponse;
    private static int timeoutConnection;
    private static int timeoutSocket;

    Connection() {
    }

    static synchronized HttpEntity postData(List<NameValuePair> values, Context context, String url) {
        HttpEntity httpEntity;
        synchronized (Connection.class) {
            if (checkconnectivity(context)) {
                try {
                    httpPost = new HttpPost(url);
                    httpPost.setEntity(new UrlEncodedFormEntity(values));
                    httpParameters = new BasicHttpParams();
                    timeoutConnection = 3000;
                    HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
                    timeoutSocket = 3000;
                    HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
                    httpClient = new DefaultHttpClient(httpParameters);
                    httpClient.addRequestInterceptor(new HttpRequestInterceptor() {
                        public void process(HttpRequest request, HttpContext context) {
                            if (!request.containsHeader(Connection.HEADER_ACCEPT_ENCODING)) {
                                request.addHeader(Connection.HEADER_ACCEPT_ENCODING, Connection.ENCODING_GZIP);
                            }
                        }
                    });
                    httpClient.addResponseInterceptor(new HttpResponseInterceptor() {
                        public void process(HttpResponse response, HttpContext context) {
                            Header encoding = response.getEntity().getContentEncoding();
                            if (encoding != null) {
                                for (HeaderElement element : encoding.getElements()) {
                                    if (element.getName().equalsIgnoreCase(Connection.ENCODING_GZIP)) {
                                        response.setEntity(new InflatingEntity(response.getEntity()));
                                        return;
                                    }
                                }
                            }
                        }
                    });
                    httpResponse = httpClient.execute(httpPost);
                    entity = httpResponse.getEntity();
                    httpEntity = entity;
                } catch (Exception e) {
                    Log.e("AdmiaLog", "Problem in con");
                }
            }
            httpEntity = null;
        }
        return httpEntity;
    }

    static InputStream getImage(String url) {
        try {
            HttpURLConnection httpConnection = (HttpURLConnection) new URL(url).openConnection();
            httpConnection.setRequestMethod("GET");
            httpConnection.setDoOutput(true);
            httpConnection.setDoInput(true);
            httpConnection.setConnectTimeout(20000);
            httpConnection.setReadTimeout(20000);
            httpConnection.setUseCaches(false);
            httpConnection.setDefaultUseCaches(false);
            httpConnection.connect();
            if (httpConnection.getResponseCode() == 200) {
                return httpConnection.getInputStream();
            }
        } catch (Exception e) {
            Log.e("AdmiaLog", "Network Error, please try again later");
        }
        return null;
    }

    static boolean checkconnectivity(Context context) {
        try {
            ConnectivityManager cm = (ConnectivityManager) context.getSystemService("connectivity");
            if (cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isAvailable() && cm.getActiveNetworkInfo().isConnected()) {
                return true;
            }
            Log.i("AdmiaLog", "Internet Connection Not Available");
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    private static String getIPforCall() {
        if (AdmiaUtil.getIP1() == AdmiaTexts.INVALID_DATA && AdmiaUtil.getIP2() == AdmiaTexts.INVALID_DATA) {
            return AdmiaTexts.INVALID_DATA;
        }
        String[] ip = {AdmiaUtil.getIP1(), AdmiaUtil.getIP2()};
        int i = 0;
        while (i < ip.length) {
            try {
                HttpURLConnection connection = (HttpURLConnection) new URL(ip[i]).openConnection();
                connection.setRequestMethod("GET");
                connection.setConnectTimeout(2000);
                connection.connect();
                if (connection.getResponseCode() == 200) {
                    return ip[i];
                }
                connection.disconnect();
                i++;
            } catch (IOException | Exception | MalformedURLException e) {
            }
        }
        return AdmiaTexts.INVALID_DATA;
    }

    private static class InflatingEntity extends HttpEntityWrapper {
        public InflatingEntity(HttpEntity wrapped) {
            super(wrapped);
        }

        public InputStream getContent() throws IOException {
            return new GZIPInputStream(this.wrappedEntity.getContent());
        }

        public long getContentLength() {
            return -1;
        }
    }
}
