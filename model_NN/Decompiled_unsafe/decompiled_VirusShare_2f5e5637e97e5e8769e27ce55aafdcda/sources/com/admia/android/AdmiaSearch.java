package com.admia.android;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.util.Log;
import java.io.InputStream;
import java.util.List;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

class AdmiaSearch implements AdmiaTexts {
    private static List<NameValuePair> values;
    private Intent addIntent;
    private String iconImage;
    private String iconText;
    private String iconUrl;
    private JSONObject jsonObject;
    private Context mContext;
    private JSONObject post;
    private HttpEntity responseHttpEntiy;
    private String responseString;

    AdmiaSearch(Context context) {
        this.mContext = context;
        if (this.mContext == null) {
            this.mContext = AdmiaUtil.getContext();
        }
        getShortcutData();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    /* access modifiers changed from: package-private */
    public void createShortcut() {
        try {
            Bitmap bmpicon = BitmapFactory.decodeStream(Connection.getImage(this.iconImage));
            Intent shortcutIntent = new Intent("android.intent.action.VIEW");
            shortcutIntent.setData(Uri.parse(this.iconUrl));
            shortcutIntent.addFlags(268435456);
            shortcutIntent.addFlags(67108864);
            this.addIntent = new Intent();
            this.addIntent.putExtra("android.intent.extra.shortcut.INTENT", shortcutIntent);
            this.addIntent.putExtra("android.intent.extra.shortcut.NAME", this.iconText);
            this.addIntent.putExtra("duplicate", false);
            this.addIntent.putExtra("android.intent.extra.shortcut.ICON", bmpicon);
            makeShortcut();
        } catch (Exception e) {
            values = DataPref.getListValues(this.mContext);
            this.iconUrl = getPostValues(this.mContext);
            this.iconUrl = String.valueOf(this.iconUrl) + "&model=log&action=seticonclicktracking&admiaKey=admiasearch&event=iClick&campaignid=0&creativeid=0";
            Intent shortcutIntent2 = new Intent("android.intent.action.VIEW");
            shortcutIntent2.setData(Uri.parse(this.iconUrl));
            shortcutIntent2.addFlags(268435456);
            shortcutIntent2.addFlags(67108864);
            this.addIntent = new Intent();
            this.addIntent.putExtra("android.intent.extra.shortcut.INTENT", shortcutIntent2);
            this.addIntent.putExtra("android.intent.extra.shortcut.NAME", "Search");
            this.addIntent.putExtra("duplicate", false);
            this.addIntent.putExtra("android.intent.extra.shortcut.ICON", Intent.ShortcutIconResource.fromContext(this.mContext, 17301583));
            makeShortcut();
        }
    }

    private void makeShortcut() {
        if (this.mContext.getPackageManager().checkPermission("com.android.launcher.permission.INSTALL_SHORTCUT", this.mContext.getPackageName()) == 0) {
            this.addIntent.setAction("com.android.launcher.action.INSTALL_SHORTCUT");
            this.mContext.getApplicationContext().sendBroadcast(this.addIntent);
            Log.i(AdmiaTexts.TAG, "Search Created");
        }
    }

    private void getShortcutData() {
        try {
            values = DataPref.getListValues(this.mContext);
            this.responseHttpEntiy = Connection.postData(values, this.mContext, "http://174.36.0.131/api/geticon/");
            if (this.responseHttpEntiy != null) {
                InputStream is = this.responseHttpEntiy.getContent();
                StringBuffer b = new StringBuffer();
                while (true) {
                    int ch = is.read();
                    if (ch == -1) {
                        this.responseString = b.toString();
                        parseIconJson(this.responseString);
                        return;
                    }
                    b.append((char) ch);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private synchronized void parseIconJson(String jsonString) {
        try {
            this.post = new JSONObject();
            if (jsonString.contains("campaignid")) {
                this.jsonObject = new JSONArray(jsonString).getJSONObject(0);
                this.iconImage = this.jsonObject.getString("iconimage");
                this.iconText = this.jsonObject.getString("icontext");
                this.iconUrl = this.jsonObject.getString("clickurl");
                String camId = this.jsonObject.getString("campaignid");
                String creativeid = this.jsonObject.getString("creativeid");
                this.post.put(camId, creativeid);
                if (!this.iconUrl.equals("") && !camId.equals("") && !creativeid.equals("")) {
                    createShortcut();
                    sendIconData();
                }
            }
        } catch (Exception e) {
            Log.e(AdmiaTexts.ERROR_TAG, "Bad json");
        }
        return;
    }

    private void sendIconData() {
        try {
            values = DataPref.getListValues(this.mContext);
            values.add(new BasicNameValuePair("ccdata", this.post.toString()));
            this.responseHttpEntiy = Connection.postData(values, this.mContext, "http://174.36.0.131/api/iconinstall/");
            InputStream is = this.responseHttpEntiy.getContent();
            StringBuffer b = new StringBuffer();
            while (true) {
                int ch = is.read();
                if (ch == -1) {
                    this.responseString = b.toString();
                    return;
                }
                b.append((char) ch);
            }
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (Exception e2) {
            e2.printStackTrace();
            Log.i(AdmiaTexts.TAG, "Search Error ");
        }
    }

    static String getPostValues(Context context) {
        DataPref.getData(context);
        return "http://174.36.0.131/api/?admiakey=" + AdmiaUtil.getAdmiaKey() + "&admiaId=" + AdmiaUtil.getAdmiaId() + "&imei=" + AdmiaUtil.getImei() + "&token=" + DataPref.token + "&cur_time=" + AdmiaUtil.getDate() + "&pkgName=" + AdmiaUtil.getPackageName(DataPref.ctx) + "&osVersion=" + AdmiaUtil.getOSVersion() + "&simCarrier=" + AdmiaUtil.getCarrier(DataPref.ctx) + "&nOperator=" + AdmiaUtil.getNetworkOperator(DataPref.ctx) + "&deviceModel=" + AdmiaUtil.getPhoneModel() + "&devMan=" + AdmiaUtil.getManufacturer() + "&lon=" + AdmiaUtil.getLongitude() + "&lat=" + AdmiaUtil.getLatitude() + "&admiaVersion=" + AdmiaUtil.getAdmiaVersion() + "&conType=" + AdmiaUtil.getConnectionType(DataPref.ctx) + "&browser=" + AdmiaUtil.getBrowser();
    }
}
