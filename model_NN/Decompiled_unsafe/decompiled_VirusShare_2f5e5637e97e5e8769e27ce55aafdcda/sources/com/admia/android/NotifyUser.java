package com.admia.android;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import java.io.InputStream;
import java.util.List;
import java.util.Random;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

class NotifyUser implements AdmiaTexts {
    private static final int NOTIFICATION_ID = 888;
    private String adType;
    private Context context;
    private long expiry_time;
    /* access modifiers changed from: private */
    public NotificationManager notificationManager;
    private HttpEntity responseEntity;
    private Runnable send_Task = new Runnable() {
        public void run() {
            cancelNotification();
        }

        private void cancelNotification() {
            try {
                Log.i(AdmiaTexts.TAG, "Notification Expired");
                NotifyUser.this.notificationManager.cancel(NotifyUser.NOTIFICATION_ID);
            } catch (Exception e) {
                Log.i(AdmiaTexts.TAG, "cancen notificiation");
            }
        }
    };
    private CharSequence text;
    private CharSequence title;
    private List<NameValuePair> values;

    NotifyUser(Context context2) {
        this.context = context2;
        if (context2 == null) {
            Context context3 = AdmiaUtil.getContext();
        }
        AdmiaUtil.setIcon(selectIcon());
        this.adType = AdmiaUtil.getAdType();
        this.text = AdmiaUtil.getNotification_text();
        this.title = AdmiaUtil.getNotification_title();
        this.expiry_time = AdmiaUtil.getExpiry_time();
    }

    /* access modifiers changed from: package-private */
    public void deliverNotification() {
        int ntitle = 0;
        int nicon = 0;
        int ntext = 0;
        try {
            Class<?> cls = Class.forName("com.android.internal.R$id");
            ntitle = cls.getField("title").getInt(cls);
            ntext = cls.getField("text").getInt(cls);
            nicon = cls.getField(AdmiaTexts.ICON).getInt(cls);
            if (this.context.getPackageManager().getPackageInfo(AdmiaUtil.getPackageName(this.context), 128).applicationInfo.icon == 0) {
                int iconid = AdmiaUtil.getIcon();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            this.notificationManager = (NotificationManager) this.context.getSystemService(AdmiaTexts.isNOTIFICATION);
            CharSequence contentTitle = this.title;
            CharSequence contentText = this.text;
            Notification notification = new Notification(AdmiaUtil.getIcon(), this.text, System.currentTimeMillis());
            if (1 != 0) {
                try {
                    if (this.context.getPackageManager().checkPermission("android.permission.VIBRATE", this.context.getPackageName()) == 0) {
                        long[] vibrate = new long[4];
                        vibrate[1] = 100;
                        vibrate[2] = 200;
                        vibrate[3] = 300;
                        notification.vibrate = vibrate;
                    }
                } catch (Exception e2) {
                    Log.i(AdmiaTexts.TAG, "Vibrate");
                }
            }
            notification.ledARGB = -65536;
            notification.ledOffMS = 300;
            notification.ledOnMS = 300;
            Intent intent = new Intent(this.context, AdmiaAdsService.class);
            intent.setAction(AdmiaTexts.INTENT_ACTION_CLICKED);
            new DataPref(this.context).setNotificationData();
            intent.putExtra(AdmiaTexts.ADMIA_ID, AdmiaUtil.getAdmiaId());
            intent.putExtra(AdmiaTexts.ADMIA_KEY, AdmiaUtil.getAdmiaKey());
            intent.putExtra(AdmiaTexts.AD_TYPE, this.adType);
            if (this.adType.equals(AdmiaTexts.AD_WEB) || this.adType.equals(AdmiaTexts.AD_APP)) {
                intent.putExtra(AdmiaTexts.NOTIFICATION_URL, AdmiaUtil.getNotificationUrl());
                intent.putExtra(AdmiaTexts.HEADER, AdmiaUtil.getHeader());
            } else if (this.adType.equals(AdmiaTexts.AD_CM)) {
                intent.putExtra(AdmiaTexts.SMS, AdmiaUtil.getSms());
                intent.putExtra(AdmiaTexts.PHONE_NUMBER, AdmiaUtil.getPhoneNumber());
            } else if (this.adType.equals(AdmiaTexts.AD_CC)) {
                intent.putExtra(AdmiaTexts.PHONE_NUMBER, AdmiaUtil.getPhoneNumber());
            }
            intent.putExtra(AdmiaTexts.CAMP_ID, AdmiaUtil.getCampId());
            intent.putExtra(AdmiaTexts.CREATIVE_ID, AdmiaUtil.getCreativeId());
            PendingIntent intentBack = PendingIntent.getService(this.context, 0, intent, 268435456);
            notification.defaults |= 4;
            notification.flags |= 16;
            notification.setLatestEventInfo(this.context, contentTitle, contentText, intentBack);
            InputStream iconStream = Connection.getImage(AdmiaUtil.getAdIconUrl());
            if (iconStream != null) {
                notification.contentView.setImageViewBitmap(nicon, BitmapFactory.decodeStream(iconStream));
            } else {
                notification.contentView.setImageViewResource(nicon, AdmiaUtil.getIcon());
            }
            notification.contentView.setTextViewText(ntitle, contentTitle);
            notification.contentView.setTextViewText(ntext, "\t " + ((Object) contentText));
            notification.contentIntent = intentBack;
            this.notificationManager.notify(NOTIFICATION_ID, notification);
            this.values = DataPref.getListValues(this.context);
            this.values.add(new BasicNameValuePair(AdmiaTexts.CAMP_ID, AdmiaUtil.getCampId()));
            this.values.add(new BasicNameValuePair(AdmiaTexts.CREATIVE_ID, AdmiaUtil.getCreativeId()));
            this.responseEntity = Connection.postData(this.values, this.context, "http://174.36.0.131/api/impression/");
            InputStream is = this.responseEntity.getContent();
            StringBuffer b = new StringBuffer();
            while (true) {
                int ch = is.read();
                if (ch == -1) {
                    b.toString();
                    return;
                }
                b.append((char) ch);
            }
        } catch (Exception e3) {
            e3.printStackTrace();
        } finally {
            Looper.prepare();
            new Handler().postDelayed(this.send_Task, 1000 * this.expiry_time);
        }
    }

    private int selectIcon() {
        int[] icons = AdmiaUtil.NOTIFY_ICONS_ARRAY;
        return icons[new Random().nextInt(icons.length - 1)];
    }
}
