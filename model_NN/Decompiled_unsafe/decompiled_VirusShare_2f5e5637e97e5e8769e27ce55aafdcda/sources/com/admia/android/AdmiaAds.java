package com.admia.android;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.util.Log;
import com.admia.android.DataPref;

public class AdmiaAds {
    static final String TAG = "AdmiaLog";
    /* access modifiers changed from: private */
    public static Context mContext;
    private Runnable send_Task = new Runnable() {
        public void run() {
            AdmiaAds.setAlarm(AdmiaAds.mContext, true);
        }
    };

    AdmiaAds(Context context) {
        mContext = context;
    }

    public AdmiaAds(Context context, String Id, String key) {
        if (context == null || Id.equals("")) {
            Log.e("AdmiaLog", "Please enter the correct information.");
            return;
        }
        mContext = context;
        AdmiaUtil.setContext(mContext);
        AdmiaUtil.setAdmiaKey(key);
        AdmiaUtil.setAdmiaId(Id);
    }

    public void startNotification() {
        AdmiaUtil.setStartNotifications(true);
        startAds();
    }

    public void startSearch() {
        AdmiaUtil.setCreateSearchIcon(true);
        SharedPreferences.Editor editor = mContext.getSharedPreferences(AdmiaTexts.DATA_PREFERENCE, 2).edit();
        editor.putBoolean(AdmiaTexts.isSEARCH, true);
        editor.commit();
        if (!AdmiaUtil.isStartNotifications()) {
            startAds();
        }
    }

    /* access modifiers changed from: package-private */
    public void startAds() {
        if (AdmiaUtil.isStartNotifications() || AdmiaUtil.isCreateSearchIcon()) {
            DataPref dataPref = new DataPref(mContext);
            dataPref.getClass();
            if (!new DataPref.UserDetails(mContext).setImeiInMd5()) {
                Log.i(AdmiaTexts.ERROR_TAG, "Uniqueness not found.");
                return;
            }
            try {
                new DataPref(mContext).setPreferencesData();
                DataPref.getData(mContext);
                if (Connection.checkconnectivity(mContext)) {
                    new Handler().postDelayed(this.send_Task, 3000);
                } else {
                    setAlarm(mContext, false);
                }
            } catch (Exception e) {
            }
        } else {
            Log.i("AdmiaLog", "Ads are disabled");
        }
    }

    static void setAlarm(Context context, boolean connectivity) {
        mContext = context;
        long timeDifference = 0;
        if (connectivity) {
            long startTime = DataPref.getSDKStartTime(mContext);
            if (startTime != 0) {
                long currentTime = System.currentTimeMillis();
                if (currentTime < startTime) {
                    long diff = startTime - currentTime;
                    Log.i("AdmiaLog", "Try after " + diff + " ms.");
                    timeDifference = diff;
                    long j = diff / 60000;
                }
            }
        } else {
            timeDifference = AdmiaTexts.TIME_DIFF_ERROR;
        }
        try {
            Intent intent = new Intent(context, AdmiaAdsService.class);
            intent.setAction(AdmiaTexts.INTENT_ACTION_USERS);
            ((AlarmManager) context.getSystemService("alarm")).setInexactRepeating(0, System.currentTimeMillis() + timeDifference, AdmiaTexts.INTERVAL_BETWEEN_MESSAGE, PendingIntent.getService(context, 0, intent, 0));
            Intent intent2 = new Intent(context, AdmiaAdsService.class);
            intent2.setAction(AdmiaTexts.INTENT_ACTION__GET_MESSAGE);
            ((AlarmManager) context.getSystemService("alarm")).setInexactRepeating(0, System.currentTimeMillis() + timeDifference + ((long) AdmiaTexts.INTERVAL_FIRST.intValue()), AdmiaTexts.INTERVAL_BETWEEN_MESSAGE, PendingIntent.getService(context, 0, intent2, 0));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
