package com.admia.android;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import java.io.InputStream;
import java.util.List;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

public class AdmiaAdsService extends Service implements AdmiaTexts {
    private Intent clickIntent;
    private Context context = null;
    private HttpEntity response;
    private Uri uri;
    private List<NameValuePair> values = null;

    public void onStart(Intent intent, int startId) {
        this.context = getApplicationContext();
        Integer startIdObj = Integer.valueOf(startId);
        String action = intent.getAction();
        try {
            if (action.equals(AdmiaTexts.INTENT_ACTION_USERS)) {
                SharedPreferences dataPrefs = this.context.getSharedPreferences(AdmiaTexts.DATA_PREFERENCE, 1);
                if (dataPrefs != null) {
                    AdmiaUtil.setImei(dataPrefs.getString(AdmiaTexts.IMEI, AdmiaTexts.INVALID_DATA));
                }
                new UserTask(this, null).execute(new Void[0]);
            } else if (action.equals(AdmiaTexts.INTENT_ACTION__GET_MESSAGE)) {
                if (DataPref.getData(this.context)) {
                    if (AdmiaUtil.isCreateSearchIcon()) {
                        new AdmiaSearch(getApplicationContext());
                    }
                    new Handler().postDelayed(new Runnable() {
                        public void run() {
                            if (AdmiaUtil.isStartNotifications()) {
                                new MessageTask(AdmiaAdsService.this, null).execute(new Void[0]);
                            }
                        }
                    }, 3000);
                } else if (startIdObj != null) {
                    stopSelf(startId);
                    return;
                } else {
                    return;
                }
            } else if (action.equals(AdmiaTexts.INTENT_ACTION_CLICKED)) {
                if (AdmiaUtil.getAdType().equals(AdmiaTexts.AD_CC)) {
                    sendClicksData(intent);
                    this.uri = Uri.parse("tel:" + AdmiaUtil.getPhoneNumber());
                    this.clickIntent = new Intent("android.intent.action.DIAL", this.uri);
                    this.clickIntent.setFlags(268435456);
                    this.context.startActivity(this.clickIntent);
                }
                if (AdmiaUtil.getAdType().equals(AdmiaTexts.AD_CM)) {
                    sendClicksData(intent);
                    this.uri = Uri.parse("smsto:" + AdmiaUtil.getPhoneNumber());
                    this.clickIntent = new Intent("android.intent.action.SENDTO", this.uri);
                    this.clickIntent.putExtra("sms_body", AdmiaUtil.getSms());
                    this.clickIntent.setFlags(268435456);
                    this.context.startActivity(this.clickIntent);
                }
                if (AdmiaUtil.getAdType().equals(AdmiaTexts.AD_WEB) || AdmiaUtil.getAdType().equals(AdmiaTexts.AD_APP)) {
                    sendClicksData(intent);
                    this.clickIntent = new Intent("android.intent.action.VIEW", Uri.parse(AdmiaUtil.getNotificationUrl()));
                    this.clickIntent.setFlags(268435456);
                    this.context.startActivity(this.clickIntent);
                }
            }
            if (startIdObj != null) {
                stopSelf(startId);
            }
        } catch (Exception e) {
            Log.e(AdmiaTexts.TAG, "Service Error");
            if (startIdObj != null) {
                stopSelf(startId);
            }
        } catch (Throwable th) {
            if (startIdObj != null) {
                stopSelf(startId);
            }
            throw th;
        }
    }

    private class MessageTask extends AsyncTask<Void, Void, Void> {
        private MessageTask() {
        }

        /* synthetic */ MessageTask(AdmiaAdsService admiaAdsService, MessageTask messageTask) {
            this();
        }

        /* access modifiers changed from: protected */
        public Void doInBackground(Void... arg0) {
            AdmiaAdsService.this.getMessage();
            return null;
        }
    }

    private class UserTask extends AsyncTask<Void, Void, Void> {
        private UserTask() {
        }

        /* synthetic */ UserTask(AdmiaAdsService admiaAdsService, UserTask userTask) {
            this();
        }

        /* access modifiers changed from: protected */
        public Void doInBackground(Void... arg0) {
            AdmiaAdsService.this.usersData();
            return null;
        }
    }

    /* access modifiers changed from: private */
    public synchronized void usersData() {
        this.context = AdmiaUtil.getContext();
        if (this.context == null) {
            this.context = getApplicationContext();
        }
        try {
            this.values = DataPref.getListValues(this.context);
            HttpEntity entity = Connection.postData(this.values, this.context, "http://174.36.0.131/api/users/");
            if (entity != null) {
                InputStream is = entity.getContent();
                StringBuffer b = new StringBuffer();
                while (true) {
                    int ch = is.read();
                    if (ch == -1) {
                        break;
                    }
                    b.append((char) ch);
                }
                b.toString();
            }
        } catch (Exception e) {
            AdmiaAds.setAlarm(this.context, false);
            Log.i("err", "Error in info");
        }
        return;
    }

    /* access modifiers changed from: private */
    public synchronized void getMessage() {
        if (this.context == null) {
            this.context = getApplicationContext();
        }
        try {
            this.values = DataPref.getListValues(this.context);
            HttpEntity response2 = Connection.postData(this.values, this.context, "http://174.36.0.131/api/getmessage/");
            if (response2 != null) {
                InputStream is = response2.getContent();
                StringBuffer b = new StringBuffer();
                while (true) {
                    int ch = is.read();
                    if (ch == -1) {
                        break;
                    }
                    b.append((char) ch);
                }
                new ParseJson(getApplicationContext()).parseJson(b.toString());
            } else {
                AdmiaAds.setAlarm(this.context, false);
            }
        } catch (Exception e) {
            AdmiaAds.setAlarm(this.context, false);
        }
        return;
    }

    private synchronized void sendClicksData(Intent intent) {
        try {
            if (DataPref.getNotificationData(getApplicationContext())) {
                AdmiaUtil.setCreativeId(intent.getStringExtra(AdmiaTexts.CREATIVE_ID));
                AdmiaUtil.setCampId(intent.getStringExtra(AdmiaTexts.CAMP_ID));
            }
            this.values = DataPref.getListValues(this.context);
            if (this.values == null || this.values.isEmpty()) {
                this.values.add(new BasicNameValuePair(AdmiaTexts.ADMIA_ID, intent.getStringExtra(AdmiaTexts.ADMIA_ID)));
                this.values.add(new BasicNameValuePair(AdmiaTexts.ADMIA_KEY, intent.getStringExtra(AdmiaTexts.ADMIA_KEY)));
            }
            this.values.add(new BasicNameValuePair(AdmiaTexts.CAMP_ID, AdmiaUtil.getCampId()));
            this.values.add(new BasicNameValuePair(AdmiaTexts.CREATIVE_ID, AdmiaUtil.getCreativeId()));
            this.response = Connection.postData(this.values, getApplicationContext(), "http://174.36.0.131/api/messageclick/");
            if (this.response != null) {
                InputStream is = this.response.getContent();
                StringBuffer b = new StringBuffer();
                while (true) {
                    int ch = is.read();
                    if (ch == -1) {
                        break;
                    }
                    b.append((char) ch);
                }
                b.toString();
            }
        } catch (Exception e) {
            Log.e(AdmiaTexts.TAG, "Data Not Sent");
        }
        return;
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    public boolean onUnbind(Intent intent) {
        return super.onUnbind(intent);
    }
}
