package com.moolah;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class PushNotification {
    private static final int DEFAULT_REGISTER_DELAY = 10000;
    private Context mContext;
    private String mPublisherSignature;

    public PushNotification(Context context, String str) {
        this.mContext = context;
        this.mPublisherSignature = str;
    }

    static void bootNotification(Context context) {
        Intent intent = new Intent(context, MessageReceiver.class);
        intent.setAction(MessageReceiver.ACTION_BOOT);
        ((AlarmManager) context.getSystemService("alarm")).set(0, System.currentTimeMillis() + 300000, PendingIntent.getBroadcast(context, 0, intent, 0));
    }

    public void startNotifications() {
        startTestModeNotifications(0);
    }

    public void startTestModeNotifications(int i) {
        Log.i("PushNotification", "startNotifications");
        Intent intent = new Intent(this.mContext, MessageReceiver.class);
        intent.setAction(MessageReceiver.ACTION_START);
        intent.putExtra("test_mode", i);
        intent.putExtra("notif_sig", this.mPublisherSignature);
        ((AlarmManager) this.mContext.getSystemService("alarm")).set(0, System.currentTimeMillis() + 10000, PendingIntent.getBroadcast(this.mContext, 0, intent, 134217728));
    }

    public void stopNotifications() {
        Log.i("PushNotification", "stopNotifications");
        Intent intent = new Intent(this.mContext, NotificationService.class);
        intent.putExtra("notif_sig", this.mPublisherSignature);
        intent.setAction(MessageReceiver.ACTION_STOP);
        this.mContext.startService(intent);
    }
}
