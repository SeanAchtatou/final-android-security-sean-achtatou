package com.moolah;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;

public class MessageReceiver extends BroadcastReceiver {
    public static final String ACTION_BOOT = "com.molaah.ACTION_BOOT";
    public static final String ACTION_NEXT_CALL = "com.moolah.ACTION_NEXT_CALL";
    public static final String ACTION_NEXT_NOTIFICATION = "com.moolah.ACTION_NEXT_NOTIFICATION";
    public static final String ACTION_START = "com.molaah.ACTION_START";
    public static final String ACTION_STOP = "com.molaah.ACTION_STOP";

    public void onReceive(Context context, Intent intent) {
        Log.i("MoolahMessageReceiver", "onReceive");
        if (intent.getAction().equals(ACTION_NEXT_CALL) || intent.getAction().equals(ACTION_BOOT) || intent.getAction().equals(ACTION_NEXT_NOTIFICATION) || intent.getAction().equals(ACTION_STOP) || intent.getAction().equals(ACTION_START)) {
            Intent intent2 = new Intent(context, NotificationService.class);
            String stringExtra = intent.getStringExtra("notif_sig");
            if (!TextUtils.isEmpty(stringExtra)) {
                intent2.putExtra("notif_sig", stringExtra);
            }
            intent2.putExtra("test_mode", intent.getIntExtra("test_mode", 0));
            intent2.setAction(intent.getAction());
            context.startService(intent2);
        }
    }
}
