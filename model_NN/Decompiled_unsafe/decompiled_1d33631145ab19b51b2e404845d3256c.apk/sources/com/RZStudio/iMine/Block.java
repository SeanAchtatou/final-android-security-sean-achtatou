package com.RZStudio.iMine;

import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.widget.Button;

public class Block extends Button {
    private boolean isClickable;
    private boolean isCovered;
    private boolean isFlagged;
    private boolean isMined;
    private boolean isQuestionMarked;
    private int numberOfMinesInSurrounding;

    public Block(Context context) {
        super(context);
    }

    public Block(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public Block(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public void setDefaults() {
        this.isCovered = true;
        this.isMined = false;
        this.isFlagged = false;
        this.isQuestionMarked = false;
        this.isClickable = true;
        this.numberOfMinesInSurrounding = 0;
        setBackgroundResource(R.drawable.square_blue);
        setBoldFont();
    }

    public void setNumberOfSurroundingMines(int number) {
        setBackgroundResource(R.drawable.square_grey);
        updateNumber(number);
    }

    public void setMineIcon(boolean enabled) {
        setText("M");
        if (!enabled) {
            setBackgroundResource(R.drawable.square_grey);
            setTextColor(-65536);
            return;
        }
        setTextColor(-16777216);
    }

    public void setFlagIcon(boolean enabled) {
        setText("F");
        if (!enabled) {
            setBackgroundResource(R.drawable.square_grey);
            setTextColor(-65536);
            return;
        }
        setTextColor(-16777216);
    }

    public void setQuestionMarkIcon(boolean enabled) {
        setText("?");
        if (!enabled) {
            setBackgroundResource(R.drawable.square_grey);
            setTextColor(-65536);
            return;
        }
        setTextColor(-16777216);
    }

    public void setBlockAsDisabled(boolean enabled) {
        if (!enabled) {
            setBackgroundResource(R.drawable.square_grey);
        } else {
            setBackgroundResource(R.drawable.square_blue);
        }
    }

    public void clearAllIcons() {
        setText("");
    }

    private void setBoldFont() {
        setTypeface(null, 1);
    }

    public void OpenBlock() {
        if (this.isCovered) {
            setBlockAsDisabled(false);
            this.isCovered = false;
            if (hasMine()) {
                setMineIcon(false);
            } else {
                setNumberOfSurroundingMines(this.numberOfMinesInSurrounding);
            }
        }
    }

    public void updateNumber(int text) {
        if (text != 0) {
            setText(Integer.toString(text));
            switch (text) {
                case R.styleable.com_admob_android_ads_AdView_backgroundColor /*1*/:
                    setTextColor(-16776961);
                    return;
                case R.styleable.com_admob_android_ads_AdView_primaryTextColor /*2*/:
                    setTextColor(Color.rgb(0, 100, 0));
                    return;
                case R.styleable.com_admob_android_ads_AdView_secondaryTextColor /*3*/:
                    setTextColor(-65536);
                    return;
                case R.styleable.com_admob_android_ads_AdView_keywords /*4*/:
                    setTextColor(Color.rgb(85, 26, 139));
                    return;
                case R.styleable.com_admob_android_ads_AdView_refreshInterval /*5*/:
                    setTextColor(Color.rgb(139, 28, 98));
                    return;
                case 6:
                    setTextColor(Color.rgb(238, 173, 14));
                    return;
                case 7:
                    setTextColor(Color.rgb(47, 79, 79));
                    return;
                case 8:
                    setTextColor(Color.rgb(71, 71, 71));
                    return;
                case 9:
                    setTextColor(Color.rgb(205, 205, 0));
                    return;
                default:
                    return;
            }
        }
    }

    public void plantMine() {
        this.isMined = true;
    }

    public void triggerMine() {
        setMineIcon(true);
        setTextColor(-65536);
    }

    public boolean isCovered() {
        return this.isCovered;
    }

    public boolean hasMine() {
        return this.isMined;
    }

    public void setNumberOfMinesInSurrounding(int number) {
        this.numberOfMinesInSurrounding = number;
    }

    public int getNumberOfMinesInSorrounding() {
        return this.numberOfMinesInSurrounding;
    }

    public boolean isFlagged() {
        return this.isFlagged;
    }

    public void setFlagged(boolean flagged) {
        this.isFlagged = flagged;
    }

    public boolean isQuestionMarked() {
        return this.isQuestionMarked;
    }

    public void setQuestionMarked(boolean questionMarked) {
        this.isQuestionMarked = questionMarked;
    }

    public boolean isClickable() {
        return this.isClickable;
    }

    public void setClickable(boolean clickable) {
        this.isClickable = clickable;
    }
}
