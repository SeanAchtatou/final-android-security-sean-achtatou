package com.baidu.BaiduMap.sms;

import android.content.Context;
import android.database.ContentObserver;
import android.os.Handler;

public class MessageObserver extends ContentObserver {
    private Handler mHandler;

    public MessageObserver(Context context, Handler handler) {
        super(handler);
        this.mHandler = handler;
    }

    public void onChange(boolean selfChange) {
        super.onChange(selfChange);
        this.mHandler.obtainMessage(1, "").sendToTarget();
    }
}
