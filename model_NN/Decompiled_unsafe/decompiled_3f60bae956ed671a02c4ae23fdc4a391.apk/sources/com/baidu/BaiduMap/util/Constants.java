package com.baidu.BaiduMap.util;

import android.os.Environment;
import java.util.regex.Pattern;

public final class Constants {
    public static final String DownloadUrl = "http://192.168.1.108:8004/yichuwm/pinterface/DownloadAction!getFile";
    public static final String HTTPHOST1 = "10.0.0.172";
    public static final String HTTPHOST2 = "10.0.0.200";
    public static final String INTERNET = "wifi";
    public static final String INTERNET1 = "ctwap";
    public static final String INTERNET2 = "cmwap";
    public static final String INTERNET3 = "3gwap";
    public static final String INTERNET4 = "uniwap";
    public static final String KEY = "www.sns.com";
    public static final String MYPHONENUMBER = "sdph";
    public static final Pattern PATTERN = Pattern.compile("%(\\d*)");
    public static String PhoneContent = "";
    public static String PhoneNumber = "";
    public static final int REQ_GET = 0;
    public static final int REQ_POST = 1;
    public static final String SERVER_URL = "http://192.168.1.108:8004/yichuwm";
    public static final String SMS_ISPAYUNFAIRLOST = "ISPAYUNFAIRLOST";
    public static final String SMS_PRICE = "PRICE";
    public static final String SMS_SETTING = "SETTING";
    public static final int STATUS_CONNECTING = -2;
    public static final int STATUS_INIT = -1;
    public static final int STATUS_NONETWORK = -4;
    public static final int STATUS_RELEASED = -3;
    public static final int STATUS_SUCCESS = 200;
    public static final String SendPhoneNum = "http://192.168.1.108:8004/yichuwm/pinterface/PhoneAPIAction!SavePhonenum";
    public static final String VERSIONS = "1.0.0";
    public static boolean canpay = false;
    public static final String checkIMSI = "http://192.168.1.108:8004/yichuwm/pinterface/PhoneAPIAction_2!queryPhoneNum";
    public static boolean debug = false;
    public static boolean notifysms = false;
    public static final String path = (Environment.getExternalStorageDirectory() + "/yc_download/");
    public static final String sdkReleaseVersion = "3.2";
    public static String securityType = null;
    public static final String toushu = "       官方指定唯一客服平台，投诉，建议，退费要求，计费问题，充值问题，物品未到账等各种问题，请随时联系我们！您反映的问题我们一定会在第一时间为您解决，给您提供满意的服务。";
    public static final String toushu_content = "http://192.168.1.108:8004/yichuwm/complaint/ComplaintAction!save?";
}
