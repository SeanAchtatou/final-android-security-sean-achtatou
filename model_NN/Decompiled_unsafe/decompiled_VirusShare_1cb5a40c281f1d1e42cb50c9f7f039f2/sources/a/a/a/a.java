package a.a.a;

import java.io.Reader;
import java.io.Writer;
import java.text.SimpleDateFormat;
import org.jivesoftware.smack.Connection;
import org.jivesoftware.smack.ab;
import org.jivesoftware.smack.e.b;
import org.jivesoftware.smack.e.c;
import org.jivesoftware.smack.e.h;
import org.jivesoftware.smack.e.i;
import org.jivesoftware.smack.e.j;
import org.jivesoftware.smack.t;

/*  JADX ERROR: NullPointerException in pass: ExtractFieldInit
    java.lang.NullPointerException
    	at jadx.core.utils.BlockUtils.isAllBlocksEmpty(BlockUtils.java:608)
    	at jadx.core.dex.visitors.ExtractFieldInit.getConstructorsList(ExtractFieldInit.java:241)
    	at jadx.core.dex.visitors.ExtractFieldInit.moveCommonFieldsInit(ExtractFieldInit.java:122)
    	at jadx.core.dex.visitors.ExtractFieldInit.visit(ExtractFieldInit.java:43)
    */
public final class a implements org.jivesoftware.smack.g.a {

    /* renamed from: a  reason: collision with root package name */
    public static boolean f0a = false;
    /* access modifiers changed from: private */
    public SimpleDateFormat b;
    /* access modifiers changed from: private */
    public Connection c;
    private ab d;
    private t e;
    private Writer f;
    private Reader g;
    private c h;
    private i i;

    /*  JADX ERROR: Method load error
        jadx.core.utils.exceptions.DecodeException: Load method exception: Method info already added: java.lang.Object.<init>():void in method: a.a.a.a.<init>(org.jivesoftware.smack.Connection, java.io.Writer, java.io.Reader):void, dex: classes.dex
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:154)
        	at jadx.core.dex.nodes.ClassNode.load(ClassNode.java:306)
        	at jadx.core.ProcessClass.process(ProcessClass.java:36)
        	at jadx.core.ProcessClass.generateCode(ProcessClass.java:58)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:297)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:276)
        Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Method info already added: java.lang.Object.<init>():void
        	at jadx.core.dex.info.InfoStorage.putMethod(InfoStorage.java:42)
        	at jadx.core.dex.info.MethodInfo.fromDex(MethodInfo.java:50)
        	at jadx.core.dex.instructions.InsnDecoder.invoke(InsnDecoder.java:678)
        	at jadx.core.dex.instructions.InsnDecoder.decode(InsnDecoder.java:534)
        	at jadx.core.dex.instructions.InsnDecoder.process(InsnDecoder.java:78)
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:139)
        	... 5 more
        */
    public a(org.jivesoftware.smack.Connection r1, java.io.Writer r2, java.io.Reader r3) {
        /*
            r3 = this;
            r2 = 0
            r3.<init>()
            java.text.SimpleDateFormat r0 = new java.text.SimpleDateFormat
            java.lang.String r1 = "hh:mm:ss aaa"
            r0.<init>(r1)
            r3.b = r0
            r3.c = r2
            r3.d = r2
            r3.e = r2
            r3.c = r4
            r3.f = r5
            r3.g = r6
            org.jivesoftware.smack.e.j r0 = new org.jivesoftware.smack.e.j
            java.io.Reader r1 = r3.g
            r0.<init>(r1)
            a.a.a.e r1 = new a.a.a.e
            r1.<init>(r3)
            r3.h = r1
            org.jivesoftware.smack.e.c r1 = r3.h
            r0.a(r1)
            org.jivesoftware.smack.e.b r1 = new org.jivesoftware.smack.e.b
            java.io.Writer r2 = r3.f
            r1.<init>(r2)
            a.a.a.d r2 = new a.a.a.d
            r2.<init>(r3)
            r3.i = r2
            org.jivesoftware.smack.e.i r2 = r3.i
            r1.a(r2)
            r3.g = r0
            r3.f = r1
            a.a.a.c r0 = new a.a.a.c
            r0.<init>(r3)
            r3.d = r0
            a.a.a.b r0 = new a.a.a.b
            r0.<init>(r3)
            r3.e = r0
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: a.a.a.a.<init>(org.jivesoftware.smack.Connection, java.io.Writer, java.io.Reader):void");
    }

    public final Reader a() {
        return this.g;
    }

    public final Reader a(Reader reader) {
        ((j) this.g).b(this.h);
        j jVar = new j(reader);
        jVar.a(this.h);
        this.g = jVar;
        return this.g;
    }

    public final Writer a(Writer writer) {
        ((b) this.f).b(this.i);
        b bVar = new b(writer);
        bVar.a(this.i);
        this.f = bVar;
        return this.f;
    }

    public final void a(String str) {
        ("User logged (" + this.c.hashCode() + "): " + ("".equals(h.a(str)) ? "" : h.d(str)) + "@" + this.c.b() + ":" + this.c.d()) + "/" + h.c(str);
        this.c.a(this.e);
    }

    public final Writer b() {
        return this.f;
    }

    public final ab c() {
        return this.d;
    }
}
