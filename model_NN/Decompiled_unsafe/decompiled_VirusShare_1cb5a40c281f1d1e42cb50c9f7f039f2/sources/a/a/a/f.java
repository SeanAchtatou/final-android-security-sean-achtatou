package a.a.a;

import java.util.Hashtable;
import java.util.Map;
import org.a.b.a.a.a.a;
import org.a.b.a.a.b.a.g;

public final class f {
    public static a a(String[] strArr, String str, String str2, String str3, Map map, g gVar) {
        if (strArr == null) {
            throw new NullPointerException("auth.33");
        }
        Hashtable hashtable = new Hashtable();
        hashtable.put(new g(), new Object());
        hashtable.keys().nextElement();
        String[] strArr2 = {"PLAIN", "DIGEST-MD5"};
        boolean z = false;
        for (int i = 0; i < strArr2.length; i++) {
            int i2 = 0;
            while (true) {
                if (i2 >= strArr.length) {
                    break;
                } else if (strArr2[i].equals(strArr[i2])) {
                    z = true;
                    break;
                } else {
                    i2++;
                }
            }
        }
        if (z) {
            return g.a(strArr, str, str2, str3, map, gVar);
        }
        return null;
    }
}
