package a.a.a;

import java.util.Date;
import org.jivesoftware.smack.t;

final class b implements t {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ a f1a;

    b(a aVar) {
        this.f1a = aVar;
    }

    public final void a() {
        this.f1a.b.format(new Date()) + " Connection closed (" + this.f1a.c.hashCode() + ")";
    }

    public final void a(Exception exc) {
        this.f1a.b.format(new Date()) + " Connection closed due to an exception (" + this.f1a.c.hashCode() + ")";
        exc.printStackTrace();
    }

    public final void b() {
        this.f1a.b.format(new Date()) + " Connection reconnected (" + this.f1a.c.hashCode() + ")";
    }
}
