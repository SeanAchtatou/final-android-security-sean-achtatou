package android.drm.mobile1;

public class DrmRights {

    /* renamed from: a  reason: collision with root package name */
    private String f28a = "";

    static {
        try {
            System.loadLibrary("drm1_jni");
        } catch (UnsatisfiedLinkError e) {
            System.err.println("WARNING: Could not load libdrm1_jni.so");
        }
    }

    private native int nativeConsumeRights(int i);

    public final boolean a(int i) {
        return -1 != nativeConsumeRights(i);
    }
}
