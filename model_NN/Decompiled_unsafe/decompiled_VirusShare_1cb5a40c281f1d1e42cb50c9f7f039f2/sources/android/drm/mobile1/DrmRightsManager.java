package android.drm.mobile1;

import java.io.InputStream;

public class DrmRightsManager {

    /* renamed from: a  reason: collision with root package name */
    private static DrmRightsManager f29a = null;

    static {
        try {
            System.loadLibrary("drm1_jni");
        } catch (UnsatisfiedLinkError e) {
            System.err.println("WARNING: Could not load libdrm1_jni.so");
        }
    }

    protected DrmRightsManager() {
    }

    public static synchronized DrmRightsManager a() {
        DrmRightsManager drmRightsManager;
        synchronized (DrmRightsManager.class) {
            if (f29a == null) {
                f29a = new DrmRightsManager();
            }
            drmRightsManager = f29a;
        }
        return drmRightsManager;
    }

    private native int nativeInstallDrmRights(InputStream inputStream, int i, int i2, DrmRights drmRights);

    private native int nativeQueryRights(DrmRawContent drmRawContent, DrmRights drmRights);

    public final synchronized DrmRights a(DrmRawContent drmRawContent) {
        DrmRights drmRights;
        drmRights = new DrmRights();
        if (-1 == nativeQueryRights(drmRawContent, drmRights)) {
            drmRights = null;
        }
        return drmRights;
    }

    public final synchronized DrmRights a(InputStream inputStream, int i, String str) {
        int i2;
        DrmRights drmRights;
        if ("application/vnd.oma.drm.rights+xml".equals(str)) {
            i2 = 3;
        } else if ("application/vnd.oma.drm.rights+wbxml".equals(str)) {
            i2 = 4;
        } else if ("application/vnd.oma.drm.message".equals(str)) {
            i2 = 1;
        } else {
            throw new IllegalArgumentException("mimeType must be DRM_MIMETYPE_RIGHTS_XML or DRM_MIMETYPE_RIGHTS_WBXML or DRM_MIMETYPE_MESSAGE");
        }
        if (i <= 0) {
            drmRights = null;
        } else {
            DrmRights drmRights2 = new DrmRights();
            if (-1 == nativeInstallDrmRights(inputStream, i, i2, drmRights2)) {
                throw new b("nativeInstallDrmRights() returned JNI_DRM_FAILURE");
            }
            drmRights = drmRights2;
        }
        return drmRights;
    }
}
