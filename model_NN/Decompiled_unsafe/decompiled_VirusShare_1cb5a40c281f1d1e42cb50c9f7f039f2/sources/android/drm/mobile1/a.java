package android.drm.mobile1;

import java.io.IOException;
import java.io.InputStream;

final class a extends InputStream {

    /* renamed from: a  reason: collision with root package name */
    private boolean f30a = false;
    private int b = 0;
    private byte[] c = new byte[1];
    private /* synthetic */ DrmRawContent d;

    public a(DrmRawContent drmRawContent) {
        this.d = drmRawContent;
    }

    public final int available() {
        int a2 = this.d.nativeGetContentLength();
        if (-1 == a2) {
            throw new IOException();
        } else if (-3 == a2) {
            return 0;
        } else {
            int i = a2 - this.b;
            if (i >= 0) {
                return i;
            }
            throw new IOException();
        }
    }

    public final void close() {
        this.f30a = true;
    }

    public final void mark(int i) {
    }

    public final boolean markSupported() {
        return false;
    }

    public final int read() {
        if (-1 == read(this.c, 0, 1)) {
            return -1;
        }
        return this.c[0] & 255;
    }

    public final int read(byte[] bArr) {
        return read(bArr, 0, bArr.length);
    }

    public final int read(byte[] bArr, int i, int i2) {
        if (bArr == null) {
            throw new NullPointerException();
        } else if (i < 0 || i2 < 0 || i + i2 > bArr.length) {
            throw new IndexOutOfBoundsException();
        } else if (true == this.f30a) {
            throw new IOException();
        } else if (i2 == 0) {
            return 0;
        } else {
            int a2 = this.d.nativeReadContent(bArr, i, i2, this.b);
            if (-1 == a2) {
                throw new IOException();
            } else if (-2 == a2) {
                return -1;
            } else {
                this.b += a2;
                return a2;
            }
        }
    }

    public final void reset() {
        throw new IOException();
    }

    public final long skip(long j) {
        return 0;
    }
}
