package android.drm.mobile1;

import java.io.BufferedInputStream;
import java.io.InputStream;

public class DrmRawContent {

    /* renamed from: a  reason: collision with root package name */
    private BufferedInputStream f27a;
    private int b;
    private int c = -1;
    private String d;
    private String e;
    private int f;

    static {
        try {
            System.loadLibrary("drm1_jni");
        } catch (UnsatisfiedLinkError e2) {
            System.err.println("WARNING: Could not load libdrm1_jni.so");
        }
    }

    public DrmRawContent(InputStream inputStream, int i, String str) {
        int i2;
        this.f27a = new BufferedInputStream(inputStream, 1024);
        this.b = i;
        if ("application/vnd.oma.drm.message".equals(str)) {
            i2 = 1;
        } else if ("application/vnd.oma.drm.content".equals(str)) {
            i2 = 2;
        } else {
            throw new IllegalArgumentException("mimeType must be DRM_MIMETYPE_MESSAGE or DRM_MIMETYPE_CONTENT");
        }
        if (i <= 0) {
            throw new IllegalArgumentException("len must be > 0");
        }
        this.c = nativeConstructDrmContent(this.f27a, this.b, i2);
        if (-1 == this.c) {
            throw new b("nativeConstructDrmContent() returned JNI_DRM_FAILURE");
        }
        this.d = nativeGetRightsAddress();
        this.f = nativeGetDeliveryMethod();
        if (-1 == this.f) {
            throw new b("nativeGetDeliveryMethod() returned JNI_DRM_FAILURE");
        }
        this.e = nativeGetContentType();
        if (this.e == null) {
            throw new b("nativeGetContentType() returned null");
        }
    }

    private native int nativeConstructDrmContent(InputStream inputStream, int i, int i2);

    /* access modifiers changed from: private */
    public native int nativeGetContentLength();

    private native String nativeGetContentType();

    private native int nativeGetDeliveryMethod();

    private native String nativeGetRightsAddress();

    /* access modifiers changed from: private */
    public native int nativeReadContent(byte[] bArr, int i, int i2, int i3);

    public final int a() {
        return this.f;
    }

    public final InputStream a(DrmRights drmRights) {
        if (drmRights != null) {
            return new a(this);
        }
        throw new NullPointerException();
    }

    public final String b() {
        return this.e;
    }

    /* access modifiers changed from: protected */
    public native void finalize();
}
