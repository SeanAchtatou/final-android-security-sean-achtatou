package android.os;

public class SystemProperties {
    public static String a(String str) {
        if (str.length() <= 31) {
            return native_get(str);
        }
        throw new IllegalArgumentException("key.length > 31");
    }

    public static String get(String str, String str2) {
        if (str.length() <= 31) {
            return native_get(str, str2);
        }
        throw new IllegalArgumentException("key.length > 31");
    }

    private static native String native_get(String str);

    private static native String native_get(String str, String str2);
}
