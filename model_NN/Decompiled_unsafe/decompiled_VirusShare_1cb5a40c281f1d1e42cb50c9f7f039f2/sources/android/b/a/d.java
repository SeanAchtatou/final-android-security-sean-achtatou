package android.b.a;

import android.os.SystemProperties;
import android.util.Log;

final class d {

    /* renamed from: a  reason: collision with root package name */
    private final String f22a;
    private final int b;

    static /* synthetic */ boolean b(d dVar) {
        return "0".equals(SystemProperties.a("ro.secure")) && Log.isLoggable(new StringBuilder().append(dVar.f22a).append("-auth").toString(), dVar.b);
    }
}
