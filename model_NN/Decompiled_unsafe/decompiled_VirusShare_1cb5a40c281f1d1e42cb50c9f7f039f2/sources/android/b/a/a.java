package android.b.a;

import org.apache.http.HttpRequest;
import org.apache.http.HttpRequestInterceptor;
import org.apache.http.protocol.HttpContext;

final class a implements HttpRequestInterceptor {
    a() {
    }

    public final void process(HttpRequest httpRequest, HttpContext httpContext) {
        if (c.b.get() != null && ((Boolean) c.b.get()).booleanValue()) {
            throw new RuntimeException("This thread forbids HTTP requests");
        }
    }
}
