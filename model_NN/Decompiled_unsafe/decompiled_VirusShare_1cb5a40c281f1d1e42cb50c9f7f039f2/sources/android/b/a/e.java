package android.b.a;

import android.util.Log;
import org.apache.http.HttpRequest;
import org.apache.http.HttpRequestInterceptor;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.protocol.HttpContext;

final class e implements HttpRequestInterceptor {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ c f23a;

    /* synthetic */ e(c cVar) {
        this(cVar, (byte) 0);
    }

    private e(c cVar, byte b) {
        this.f23a = cVar;
    }

    public final void process(HttpRequest httpRequest, HttpContext httpContext) {
        d a2 = this.f23a.f;
        if (a2 != null && Log.isLoggable(a2.f22a, a2.b) && (httpRequest instanceof HttpUriRequest)) {
            Log.println(a2.b, a2.f22a, c.a((HttpUriRequest) httpRequest, d.b(a2)));
        }
    }
}
