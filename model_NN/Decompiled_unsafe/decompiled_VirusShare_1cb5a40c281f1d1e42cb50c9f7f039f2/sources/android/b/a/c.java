package android.b.a;

import android.util.Log;
import org.apache.http.HttpHost;
import org.apache.http.HttpRequest;
import org.apache.http.HttpRequestInterceptor;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.params.HttpClientParams;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HttpContext;

public final class c implements HttpClient {

    /* renamed from: a  reason: collision with root package name */
    private static long f21a = 256;
    /* access modifiers changed from: private */
    public static final ThreadLocal b = new ThreadLocal();
    /* access modifiers changed from: private */
    public static final HttpRequestInterceptor c = new a();
    private final HttpClient d;
    private RuntimeException e = new IllegalStateException("AndroidHttpClient created and never closed");
    /* access modifiers changed from: private */
    public volatile d f;

    private c(ClientConnectionManager clientConnectionManager, HttpParams httpParams) {
        this.d = new b(this, clientConnectionManager, httpParams);
    }

    public static c a(String str) {
        BasicHttpParams basicHttpParams = new BasicHttpParams();
        HttpConnectionParams.setStaleCheckingEnabled(basicHttpParams, false);
        HttpConnectionParams.setConnectionTimeout(basicHttpParams, 20000);
        HttpConnectionParams.setSoTimeout(basicHttpParams, 200000);
        HttpConnectionParams.setSocketBufferSize(basicHttpParams, 8192);
        HttpClientParams.setRedirecting(basicHttpParams, false);
        HttpProtocolParams.setUserAgent(basicHttpParams, str);
        SchemeRegistry schemeRegistry = new SchemeRegistry();
        schemeRegistry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
        return new c(new ThreadSafeClientConnManager(basicHttpParams, schemeRegistry), basicHttpParams);
    }

    /* JADX WARNING: Removed duplicated region for block: B:23:0x0089  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x00a9  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static /* synthetic */ java.lang.String a(org.apache.http.client.methods.HttpUriRequest r8, boolean r9) {
        /*
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r1 = "curl "
            r2.append(r1)
            org.apache.http.Header[] r1 = r8.getAllHeaders()
            int r3 = r1.length
            r4 = 0
        L_0x0010:
            if (r4 >= r3) goto L_0x0046
            r5 = r1[r4]
            if (r9 != 0) goto L_0x002e
            java.lang.String r6 = r5.getName()
            java.lang.String r7 = "Authorization"
            boolean r6 = r6.equals(r7)
            if (r6 != 0) goto L_0x0043
            java.lang.String r6 = r5.getName()
            java.lang.String r7 = "Cookie"
            boolean r6 = r6.equals(r7)
            if (r6 != 0) goto L_0x0043
        L_0x002e:
            java.lang.String r6 = "--header \""
            r2.append(r6)
            java.lang.String r5 = r5.toString()
            java.lang.String r5 = r5.trim()
            r2.append(r5)
            java.lang.String r5 = "\" "
            r2.append(r5)
        L_0x0043:
            int r4 = r4 + 1
            goto L_0x0010
        L_0x0046:
            java.net.URI r3 = r8.getURI()
            boolean r1 = r8 instanceof org.apache.http.impl.client.RequestWrapper
            if (r1 == 0) goto L_0x00af
            r0 = r8
            org.apache.http.impl.client.RequestWrapper r0 = (org.apache.http.impl.client.RequestWrapper) r0
            r1 = r0
            org.apache.http.HttpRequest r1 = r1.getOriginal()
            boolean r4 = r1 instanceof org.apache.http.client.methods.HttpUriRequest
            if (r4 == 0) goto L_0x00af
            org.apache.http.client.methods.HttpUriRequest r1 = (org.apache.http.client.methods.HttpUriRequest) r1
            java.net.URI r1 = r1.getURI()
        L_0x0060:
            java.lang.String r3 = "\""
            r2.append(r3)
            r2.append(r1)
            java.lang.String r1 = "\""
            r2.append(r1)
            boolean r1 = r8 instanceof org.apache.http.HttpEntityEnclosingRequest
            if (r1 == 0) goto L_0x00a4
            org.apache.http.HttpEntityEnclosingRequest r8 = (org.apache.http.HttpEntityEnclosingRequest) r8
            org.apache.http.HttpEntity r1 = r8.getEntity()
            if (r1 == 0) goto L_0x00a4
            boolean r3 = r1.isRepeatable()
            if (r3 == 0) goto L_0x00a4
            long r3 = r1.getContentLength()
            r5 = 1024(0x400, double:5.06E-321)
            int r3 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
            if (r3 >= 0) goto L_0x00a9
            java.io.ByteArrayOutputStream r3 = new java.io.ByteArrayOutputStream
            r3.<init>()
            r1.writeTo(r3)
            java.lang.String r1 = r3.toString()
            java.lang.String r3 = " --data-ascii \""
            java.lang.StringBuilder r3 = r2.append(r3)
            java.lang.StringBuilder r1 = r3.append(r1)
            java.lang.String r3 = "\""
            r1.append(r3)
        L_0x00a4:
            java.lang.String r1 = r2.toString()
            return r1
        L_0x00a9:
            java.lang.String r1 = " [TOO MUCH DATA TO INCLUDE]"
            r2.append(r1)
            goto L_0x00a4
        L_0x00af:
            r1 = r3
            goto L_0x0060
        */
        throw new UnsupportedOperationException("Method not decompiled: android.b.a.c.a(org.apache.http.client.methods.HttpUriRequest, boolean):java.lang.String");
    }

    public final void a() {
        if (this.e != null) {
            getConnectionManager().shutdown();
            this.e = null;
        }
    }

    public final Object execute(HttpHost httpHost, HttpRequest httpRequest, ResponseHandler responseHandler) {
        return this.d.execute(httpHost, httpRequest, responseHandler);
    }

    public final Object execute(HttpHost httpHost, HttpRequest httpRequest, ResponseHandler responseHandler, HttpContext httpContext) {
        return this.d.execute(httpHost, httpRequest, responseHandler, httpContext);
    }

    public final Object execute(HttpUriRequest httpUriRequest, ResponseHandler responseHandler) {
        return this.d.execute(httpUriRequest, responseHandler);
    }

    public final Object execute(HttpUriRequest httpUriRequest, ResponseHandler responseHandler, HttpContext httpContext) {
        return this.d.execute(httpUriRequest, responseHandler, httpContext);
    }

    public final HttpResponse execute(HttpHost httpHost, HttpRequest httpRequest) {
        return this.d.execute(httpHost, httpRequest);
    }

    public final HttpResponse execute(HttpHost httpHost, HttpRequest httpRequest, HttpContext httpContext) {
        return this.d.execute(httpHost, httpRequest, httpContext);
    }

    public final HttpResponse execute(HttpUriRequest httpUriRequest) {
        return this.d.execute(httpUriRequest);
    }

    public final HttpResponse execute(HttpUriRequest httpUriRequest, HttpContext httpContext) {
        return this.d.execute(httpUriRequest, httpContext);
    }

    /* access modifiers changed from: protected */
    public final void finalize() {
        super.finalize();
        if (this.e != null) {
            Log.e("AndroidHttpClient", "Leak found", this.e);
            this.e = null;
        }
    }

    public final ClientConnectionManager getConnectionManager() {
        return this.d.getConnectionManager();
    }

    public final HttpParams getParams() {
        return this.d.getParams();
    }
}
