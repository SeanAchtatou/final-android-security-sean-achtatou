package android.b;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.NetworkInfo;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

final class b extends BroadcastReceiver {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ a f24a;

    /* synthetic */ b(a aVar) {
        this(aVar, (byte) 0);
    }

    private b(a aVar, byte b) {
        this.f24a = aVar;
    }

    public final void onReceive(Context context, Intent intent) {
        if (!intent.getAction().equals("android.net.conn.CONNECTIVITY_CHANGE") || !this.f24a.d) {
            Log.w("NetworkConnectivityListener", "onReceived() called with " + this.f24a.c.toString() + " and " + intent);
            return;
        }
        if (intent.getBooleanExtra("noConnectivity", false)) {
            c unused = this.f24a.c = c.NOT_CONNECTED;
        } else {
            c unused2 = this.f24a.c = c.CONNECTED;
        }
        NetworkInfo unused3 = this.f24a.g = (NetworkInfo) intent.getParcelableExtra("networkInfo");
        NetworkInfo unused4 = this.f24a.h = (NetworkInfo) intent.getParcelableExtra("otherNetwork");
        String unused5 = this.f24a.e = intent.getStringExtra("reason");
        boolean unused6 = this.f24a.f = intent.getBooleanExtra("isFailover", false);
        for (Handler handler : this.f24a.b.keySet()) {
            handler.sendMessage(Message.obtain(handler, ((Integer) this.f24a.b.get(handler)).intValue()));
        }
    }
}
