package android.b;

import android.content.Context;
import android.content.IntentFilter;
import android.net.NetworkInfo;
import android.os.Handler;
import java.util.HashMap;

public final class a {

    /* renamed from: a  reason: collision with root package name */
    private Context f19a;
    /* access modifiers changed from: private */
    public HashMap b = new HashMap();
    /* access modifiers changed from: private */
    public c c = c.UNKNOWN;
    /* access modifiers changed from: private */
    public boolean d;
    /* access modifiers changed from: private */
    public String e;
    /* access modifiers changed from: private */
    public boolean f;
    /* access modifiers changed from: private */
    public NetworkInfo g;
    /* access modifiers changed from: private */
    public NetworkInfo h;
    private b i = new b(this);

    public final synchronized void a() {
        if (this.d) {
            this.f19a.unregisterReceiver(this.i);
            this.f19a = null;
            this.g = null;
            this.h = null;
            this.f = false;
            this.e = null;
            this.d = false;
        }
    }

    public final synchronized void a(Context context) {
        if (!this.d) {
            this.f19a = context;
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
            context.registerReceiver(this.i, intentFilter);
            this.d = true;
        }
    }

    public final void a(Handler handler) {
        this.b.put(handler, 2);
    }

    public final NetworkInfo b() {
        return this.g;
    }

    public final void b(Handler handler) {
        this.b.remove(handler);
    }
}
