package android.telephony;

import android.os.Parcel;
import android.os.Parcelable;

final class a implements Parcelable.Creator {
    a() {
    }

    public final /* bridge */ /* synthetic */ Object createFromParcel(Parcel parcel) {
        return new SignalStrength(parcel);
    }

    public final /* bridge */ /* synthetic */ Object[] newArray(int i) {
        return new SignalStrength[i];
    }
}
