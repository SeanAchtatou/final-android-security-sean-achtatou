package android.telephony;

import android.os.Parcel;
import android.os.Parcelable;

public class SignalStrength implements Parcelable {
    public static final Parcelable.Creator CREATOR = new a();

    /* renamed from: a  reason: collision with root package name */
    private int f31a;
    private int b;
    private int c;
    private int d;
    private int e;
    private int f;
    private int g;
    private boolean h;

    public SignalStrength() {
        this.f31a = 99;
        this.b = -1;
        this.c = -1;
        this.d = -1;
        this.e = -1;
        this.f = -1;
        this.g = -1;
        this.h = true;
    }

    public SignalStrength(Parcel parcel) {
        this.f31a = parcel.readInt();
        this.b = parcel.readInt();
        this.c = parcel.readInt();
        this.d = parcel.readInt();
        this.e = parcel.readInt();
        this.f = parcel.readInt();
        this.g = parcel.readInt();
        this.h = parcel.readInt() != 0;
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        try {
            SignalStrength signalStrength = (SignalStrength) obj;
            if (obj == null) {
                return false;
            }
            return this.f31a == signalStrength.f31a && this.b == signalStrength.b && this.c == signalStrength.c && this.d == signalStrength.d && this.e == signalStrength.e && this.f == signalStrength.f && this.g == signalStrength.g && this.h == signalStrength.h;
        } catch (ClassCastException e2) {
            return false;
        }
    }

    public int hashCode() {
        return (this.f31a * 4660) + this.b + this.c + this.d + this.e + this.f + this.g + (this.h ? 1 : 0);
    }

    public String toString() {
        return "SignalStrength: " + this.f31a + " " + this.b + " " + this.c + " " + this.d + " " + this.e + " " + this.f + " " + this.g + " " + (this.h ? "gsm" : "cdma");
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.f31a);
        parcel.writeInt(this.b);
        parcel.writeInt(this.c);
        parcel.writeInt(this.d);
        parcel.writeInt(this.e);
        parcel.writeInt(this.f);
        parcel.writeInt(this.g);
        parcel.writeInt(this.h ? 1 : 0);
    }
}
