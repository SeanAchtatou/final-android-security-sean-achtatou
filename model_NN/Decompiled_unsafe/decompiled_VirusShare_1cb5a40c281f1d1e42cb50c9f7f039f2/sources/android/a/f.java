package android.a;

import android.content.ContentResolver;
import android.net.Uri;
import android.provider.BaseColumns;

public final class f implements BaseColumns {

    /* renamed from: a  reason: collision with root package name */
    private static Uri f9a = Uri.parse("content://sms/inbox");

    public static Uri a(ContentResolver contentResolver, String str, String str2, Long l) {
        return m.a(contentResolver, f9a, str, str2, l);
    }
}
