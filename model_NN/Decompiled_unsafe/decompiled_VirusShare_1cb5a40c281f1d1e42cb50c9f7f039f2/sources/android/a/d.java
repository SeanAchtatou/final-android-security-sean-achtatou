package android.a;

import android.c.a.a;
import android.net.Uri;
import android.text.TextUtils;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class d implements l {

    /* renamed from: a  reason: collision with root package name */
    public static final Uri f7a;
    public static final Pattern b = Pattern.compile("\\s*(\"[^\"]*\"|[^<>\"]+)\\s*<([^<>]+)>\\s*");
    public static final Pattern c = Pattern.compile("\\s*\"([^\"]*)\"\\s*");
    private static Uri d;
    private static Uri e = Uri.withAppendedPath(f7a, "report-status");

    static {
        Uri parse = Uri.parse("content://mms");
        f7a = parse;
        d = Uri.withAppendedPath(parse, "report-request");
    }

    public static String a(String str) {
        Matcher matcher = b.matcher(str);
        return matcher.matches() ? matcher.group(2) : str;
    }

    public static boolean b(String str) {
        if (TextUtils.isEmpty(str)) {
            return false;
        }
        return a.f26a.matcher(a(str)).matches();
    }
}
