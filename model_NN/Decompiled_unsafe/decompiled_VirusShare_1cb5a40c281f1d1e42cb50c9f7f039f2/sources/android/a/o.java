package android.a;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.util.Log;
import com.sd.a.a.a.b.b;
import java.util.Iterator;
import java.util.Set;

public final class o implements i {

    /* renamed from: a  reason: collision with root package name */
    public static final Uri f16a;
    private static final String[] b = {"_id"};
    private static final Uri c = Uri.parse("content://mms-sms/threadID");
    private static Uri d;

    static {
        Uri withAppendedPath = Uri.withAppendedPath(e.f8a, "conversations");
        f16a = withAppendedPath;
        d = Uri.withAppendedPath(withAppendedPath, "obsolete");
    }

    private o() {
    }

    public static long a(Context context, Set set) {
        Uri.Builder buildUpon = c.buildUpon();
        Iterator it = set.iterator();
        while (it.hasNext()) {
            String str = (String) it.next();
            if (d.b(str)) {
                str = d.a(str);
            }
            buildUpon.appendQueryParameter("recipient", str);
        }
        Uri build = buildUpon.build();
        Log.v("Telephony", "getOrCreateThreadId uri: " + build);
        Cursor a2 = b.a(context, context.getContentResolver(), build, b, null, null, null);
        Log.v("Telephony", "getOrCreateThreadId cursor cnt: " + a2.getCount());
        if (a2 != null) {
            try {
                if (a2.moveToFirst()) {
                    return a2.getLong(0);
                }
                Log.e("Telephony", "getOrCreateThreadId returned no rows!");
                a2.close();
            } finally {
                a2.close();
            }
        }
        Log.e("Telephony", "getOrCreateThreadId failed with uri " + build.toString());
        throw new IllegalArgumentException("Unable to find or allocate a thread ID.");
    }
}
