package android.a;

import android.content.ContentResolver;
import android.net.Uri;
import android.provider.BaseColumns;

public final class k implements BaseColumns {

    /* renamed from: a  reason: collision with root package name */
    private static Uri f13a = Uri.parse("content://sms/sent");

    public static Uri a(ContentResolver contentResolver, String str, String str2, Long l) {
        return m.a(contentResolver, f13a, str, str2, l);
    }
}
