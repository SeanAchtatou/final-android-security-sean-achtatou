package android.a;

import android.net.Uri;
import android.provider.BaseColumns;

public final class e implements BaseColumns {

    /* renamed from: a  reason: collision with root package name */
    public static final Uri f8a = Uri.parse("content://mms-sms/");
    public static final Uri b = Uri.parse("content://mms-sms/draft");
    public static final Uri c = Uri.parse("content://mms-sms/locked");
    private static Uri d = Uri.parse("content://mms-sms/conversations");
    private static Uri e = Uri.parse("content://mms-sms/messages/byphone");
    private static Uri f = Uri.parse("content://mms-sms/undelivered");
    private static Uri g = Uri.parse("content://mms-sms/search");
}
