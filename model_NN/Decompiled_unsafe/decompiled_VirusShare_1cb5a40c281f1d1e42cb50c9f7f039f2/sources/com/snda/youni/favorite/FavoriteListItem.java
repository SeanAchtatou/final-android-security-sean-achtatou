package com.snda.youni.favorite;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.snda.youni.C0000R;
import com.snda.youni.d;
import com.snda.youni.e.m;

public class FavoriteListItem extends LinearLayout implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private Context f408a;
    private Handler b;
    private String c;
    private b d;
    private String e = "";
    private m f = m.a();

    public FavoriteListItem(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.f408a = context;
    }

    public final void a() {
        ((TextView) findViewById(C0000R.id.fav_item_body)).setText(this.f.a(this.f408a, this.e, false));
    }

    public final void a(Handler handler, b bVar, d dVar) {
        this.b = handler;
        this.d = bVar;
        if (bVar.c == 1) {
            ((TextView) findViewById(C0000R.id.fav_item_person)).setText(bVar.e);
            ((TextView) findViewById(C0000R.id.fav_item_type)).setText(this.f408a.getString(C0000R.string.favourite_from));
        } else {
            ((TextView) findViewById(C0000R.id.fav_item_person)).setText(bVar.e);
            ((TextView) findViewById(C0000R.id.fav_item_type)).setText(this.f408a.getString(C0000R.string.favourite_to));
        }
        String str = bVar.b;
        if (str.length() >= 50) {
            str = str.substring(0, 50) + "...";
        }
        this.e = str;
        ((TextView) findViewById(C0000R.id.fav_item_body)).setText(this.f.a(this.f408a, this.e, false));
        ((TextView) findViewById(C0000R.id.fav_item_time)).setText(bVar.d);
        "the head icon contactid===========" + bVar.f;
        dVar.a((ImageView) findViewById(C0000R.id.contact_photo), bVar.f, 0);
        Handler handler2 = this.b;
        String str2 = bVar.f410a;
        ((RelativeLayout) findViewById(C0000R.id.favorite_item_reply)).setOnClickListener(this);
        ((RelativeLayout) findViewById(C0000R.id.favorite_item_forward)).setOnClickListener(this);
        ((RelativeLayout) findViewById(C0000R.id.favorite_item_delete)).setOnClickListener(this);
        this.b = handler2;
        this.c = str2;
        findViewById(C0000R.id.favorite_item_btns).setVisibility(8);
    }

    public final void b() {
        ((TextView) findViewById(C0000R.id.fav_item_body)).setText(this.f.a(this.f408a, this.d.b, false));
    }

    public void onClick(View view) {
        Message obtainMessage = this.b.obtainMessage();
        Bundle bundle = new Bundle();
        bundle.putString("id", this.c);
        obtainMessage.setData(bundle);
        switch (view.getId()) {
            case C0000R.id.favorite_item_reply /*2131558509*/:
                obtainMessage.what = 0;
                obtainMessage.sendToTarget();
                "hit reply " + obtainMessage.what;
                return;
            case C0000R.id.favorite_item_forward /*2131558510*/:
                obtainMessage.what = 1;
                obtainMessage.sendToTarget();
                "hit forward " + obtainMessage.what;
                return;
            case C0000R.id.favorite_item_delete /*2131558511*/:
                obtainMessage.what = 2;
                obtainMessage.sendToTarget();
                "hit delete " + obtainMessage.what;
                return;
            default:
                return;
        }
    }
}
