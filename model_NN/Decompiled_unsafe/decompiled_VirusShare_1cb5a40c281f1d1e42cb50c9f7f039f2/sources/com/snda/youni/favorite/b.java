package com.snda.youni.favorite;

import android.content.Context;
import android.database.Cursor;
import com.snda.youni.e.j;
import com.snda.youni.providers.n;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public final class b {

    /* renamed from: a  reason: collision with root package name */
    String f410a;
    String b;
    int c;
    String d;
    String e;
    int f;
    private String g;

    public b(Context context, Cursor cursor, c cVar) {
        this.f410a = cursor.getString(cVar.f411a);
        this.b = cursor.getString(cVar.d);
        this.c = cursor.getInt(cVar.b);
        this.g = cursor.getString(cVar.c);
        this.b = cursor.getString(cVar.d);
        long j = cursor.getLong(cVar.e);
        this.f = cursor.getInt(cVar.f);
        "the favourite item contactid=========" + this.f;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yy-MM-dd HH:mm");
        Calendar instance = Calendar.getInstance();
        Calendar instance2 = Calendar.getInstance();
        instance.set(2, 0);
        instance.set(5, 1);
        instance.set(11, 0);
        instance.set(12, 0);
        instance.set(13, 0);
        instance2.set(11, 0);
        instance2.set(12, 0);
        instance2.set(13, 0);
        if (j > instance.getTime().getTime() && j < instance2.getTime().getTime()) {
            simpleDateFormat = new SimpleDateFormat("MM-dd");
        } else if (j > instance2.getTime().getTime()) {
            simpleDateFormat = new SimpleDateFormat("HH:mm");
        }
        this.d = simpleDateFormat.format(new Date(j));
        Cursor query = context.getContentResolver().query(n.f597a, new String[]{"display_name"}, "sid=?", new String[]{j.a(this.g)}, null);
        if (query == null || !query.moveToNext()) {
            if (query != null) {
                query.close();
            }
            this.e = this.g;
            return;
        }
        String string = query.getString(0);
        if (string != null) {
            this.e = string;
        } else {
            this.e = this.g;
        }
        query.close();
    }
}
