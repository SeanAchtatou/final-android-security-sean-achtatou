package com.snda.youni.favorite;

import android.content.Context;
import android.database.Cursor;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import com.snda.youni.C0000R;
import com.snda.youni.d;

public final class a extends CursorAdapter {

    /* renamed from: a  reason: collision with root package name */
    private LayoutInflater f409a;
    private c b;
    private Handler c;
    private d d;

    public a(Context context, Cursor cursor, d dVar) {
        super(context, cursor);
        this.f409a = (LayoutInflater) context.getSystemService("layout_inflater");
        this.b = new c(this, cursor);
        this.d = dVar;
    }

    public final void a(Handler handler) {
        this.c = handler;
    }

    public final void bindView(View view, Context context, Cursor cursor) {
        ((FavoriteListItem) view).a(this.c, new b(context, cursor, this.b), this.d);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public final View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
        return this.f409a.inflate((int) C0000R.layout.favorite_list_item, viewGroup, false);
    }
}
