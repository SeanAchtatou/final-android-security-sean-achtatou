package com.snda.youni.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.telephony.TelephonyManager;
import android.util.Log;
import com.snda.youni.services.YouniService;

public class BootReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        if (!intent.getAction().equals("android.intent.action.BOOT_COMPLETED")) {
            return;
        }
        if (((TelephonyManager) context.getSystemService("phone")).getSimState() == 1) {
            System.out.println("sim card not ready..................................");
            return;
        }
        Log.i("BootReceiver", "checking sim");
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
        SharedPreferences defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor edit = defaultSharedPreferences.edit();
        if (telephonyManager.getSimState() == 5) {
            String subscriberId = telephonyManager.getSubscriberId();
            "KEY_SELF_PHONE_NUMBER is: " + defaultSharedPreferences.getString("self_phone_number", "?");
            String string = defaultSharedPreferences.getString("self_sim_number", null);
            if (string == null) {
                edit.putString("self_sim_number", subscriberId);
                edit.commit();
            } else if (!string.equals(subscriberId)) {
                edit.remove("self_num_account");
                edit.remove("self_pt_account");
                edit.remove("self_phone_number");
                edit.putString("self_sim_number", subscriberId);
                edit.commit();
            }
            "KEY_SELF_PHONE_NUMBER is: " + defaultSharedPreferences.getString("self_phone_number", "?");
        } else {
            edit.remove("self_num_account");
            edit.remove("self_pt_account");
            edit.remove("self_phone_number");
        }
        context.startService(new Intent(context, YouniService.class));
    }
}
