package com.snda.youni.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.os.PowerManager;
import android.preference.PreferenceManager;
import com.snda.youni.AppContext;
import com.snda.youni.e.j;
import com.snda.youni.e.t;
import com.snda.youni.h.a.a;
import com.snda.youni.h.a.b;
import com.snda.youni.providers.l;

public class MessageReceiver extends BroadcastReceiver {
    private static PowerManager.WakeLock d;

    /* renamed from: a  reason: collision with root package name */
    private final String f600a = "MessageReceive";
    /* access modifiers changed from: private */
    public a b;
    private b c;

    private void a(Context context, com.snda.youni.modules.d.b bVar) {
        String d2;
        "doInBackground receive youni:" + bVar.b();
        ((AppContext) context.getApplicationContext()).a(t.a(bVar.a()));
        "doInBackground setListToBottom:" + t.a(bVar.a());
        if (bVar != null) {
            if (bVar.l() > 0) {
                d2 = Long.toString(bVar.l());
                com.snda.youni.attachment.b.b s = bVar.s();
                if (s != null) {
                    s.b(bVar.l());
                    com.snda.youni.attachment.b.a.b(context, s);
                    if (bVar.b() == null) {
                        bVar.b("");
                    }
                    if (s.g().equals("image/jpeg")) {
                        bVar.c("111111");
                        if ((!bVar.b().startsWith("[ http://n.sdo.com/") || !bVar.b().contains(" ]")) && ((!bVar.b().startsWith("[http://n.sdo.com/") || !bVar.b().contains("]")) && s.j() != null)) {
                            bVar.b("[" + s.j() + "]" + bVar.b());
                        }
                    } else if (s.g().equals("audio/amr")) {
                        bVar.c("111111");
                        if ((!bVar.b().startsWith("< http://n.sdo.com/") || !bVar.b().contains(" >")) && ((!bVar.b().startsWith("<http://n.sdo.com/") || !bVar.b().contains(">")) && s.j() != null)) {
                            bVar.b("<" + s.j() + ">" + bVar.b());
                        }
                    }
                }
                this.b.a(bVar, "_id=" + d2);
            } else {
                com.snda.youni.attachment.b.b s2 = bVar.s();
                if (s2 != null) {
                    if (bVar.b() == null) {
                        bVar.b("");
                    }
                    if (s2.g().equals("image/jpeg")) {
                        bVar.c("111111");
                        if ((!bVar.b().startsWith("[ http://n.sdo.com/") || !bVar.b().contains(" ]")) && ((!bVar.b().startsWith("[http://n.sdo.com/") || !bVar.b().contains("]")) && s2.j() != null)) {
                            bVar.b("[" + s2.j() + "]" + bVar.b());
                        }
                    } else if (s2.g().equals("audio/amr")) {
                        bVar.c("333333");
                        if ((!bVar.b().startsWith("< http://n.sdo.com/") || !bVar.b().contains(" >")) && ((!bVar.b().startsWith("<http://n.sdo.com/") || !bVar.b().contains(">")) && s2.j() != null)) {
                            bVar.b("<" + s2.j() + ">" + bVar.b());
                        }
                    }
                }
                d2 = this.b.d(bVar);
                String k = bVar.k();
                "removeReceiveMessageAfterDB:" + k;
                SharedPreferences.Editor edit = PreferenceManager.getDefaultSharedPreferences(context).edit();
                edit.remove(k);
                edit.commit();
                context.toString() + "receive msg@type=" + bVar.f() + ",mobile=" + bVar.a() + ",resource=" + bVar.u() + ",body=" + bVar.b();
                if (bVar.f().equals("1") && "wap".equals(bVar.u())) {
                    "begin save prefenence.....................lasttime_" + bVar.a();
                    SharedPreferences.Editor edit2 = PreferenceManager.getDefaultSharedPreferences(context).edit();
                    edit2.putLong("lasttime_" + bVar.a(), bVar.d().longValue());
                    edit2.commit();
                    Intent intent = new Intent("com.snda.youni.receive.RECEIVEWAP");
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("msg", bVar);
                    intent.putExtras(bundle);
                    context.sendBroadcast(intent);
                }
            }
            com.snda.youni.modules.d.b d3 = this.b.d(d2);
            if (d3 != null) {
                "receive and insert id:" + d2 + " msgtype:" + d3.i() + " youni_id:" + d3.k() + " threadId:" + d3.h();
                if (PreferenceManager.getDefaultSharedPreferences(context).getString("global_thread_id", "0").equalsIgnoreCase("" + d3.h())) {
                    ((AppContext) context.getApplicationContext()).a(d3.h());
                    d3.b(true);
                    this.b.a(d3, "_id=" + d3.l());
                    "SMS showNotification --getGlobalThreadId=" + d3.h() + " ,no showNotification";
                    return;
                }
                ((AppContext) context.getApplicationContext()).a(d3 == null ? "" : "" + d3.h(), d3.b(), d3.a(), d3.i(), d3.l(), d3.d().longValue());
            }
        }
    }

    private static boolean a(Context context, String str) {
        Cursor query = context.getContentResolver().query(l.f596a, new String[]{"blacker_name"}, "blacker_sid='" + j.a(str) + "'", null, null);
        int count = query.getCount();
        query.close();
        return count != 0;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.snda.youni.AppContext.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.snda.youni.AppContext.a(com.snda.youni.o, boolean):void
      com.snda.youni.AppContext.a(java.lang.String, long):void
      com.snda.youni.AppContext.a(java.lang.String, boolean):void */
    /* JADX WARNING: Removed duplicated region for block: B:124:0x0442  */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x0123  */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x0152  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onReceive(android.content.Context r11, android.content.Intent r12) {
        /*
            r10 = this;
            r8 = -1
            r7 = 0
            r5 = 1
            r6 = 0
            com.snda.youni.h.a.a r0 = r10.b
            if (r0 != 0) goto L_0x000f
            com.snda.youni.h.a.a r0 = new com.snda.youni.h.a.a
            r0.<init>(r11)
            r10.b = r0
        L_0x000f:
            com.snda.youni.h.a.b r0 = r10.c
            if (r0 != 0) goto L_0x001a
            com.snda.youni.h.a.b r0 = new com.snda.youni.h.a.b
            r0.<init>(r11)
            r10.c = r0
        L_0x001a:
            java.lang.String r0 = r12.getAction()
            if (r0 == 0) goto L_0x01f5
            java.lang.String r0 = r12.getAction()
            java.lang.String r1 = "android.provider.Telephony.SMS_RECEIVED"
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L_0x01f5
            android.content.Context r0 = r11.getApplicationContext()
            java.lang.String r1 = "youni_rec_sms"
            com.snda.youni.g.a.a(r0, r1, r7)
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            android.os.Bundle r0 = r12.getExtras()
            if (r0 == 0) goto L_0x0076
            java.lang.String r1 = "pdus"
            java.lang.Object r0 = r0.get(r1)
            java.lang.Object[] r0 = (java.lang.Object[]) r0
            if (r0 == 0) goto L_0x0076
            int r1 = r0.length
            android.telephony.SmsMessage[] r3 = new android.telephony.SmsMessage[r1]
            r4 = r6
        L_0x004e:
            int r1 = r0.length     // Catch:{ OutOfMemoryError -> 0x005f }
            if (r4 >= r1) goto L_0x0077
            r1 = r0[r4]     // Catch:{ OutOfMemoryError -> 0x005f }
            byte[] r1 = (byte[]) r1     // Catch:{ OutOfMemoryError -> 0x005f }
            android.telephony.SmsMessage r1 = android.telephony.SmsMessage.createFromPdu(r1)     // Catch:{ OutOfMemoryError -> 0x005f }
            r3[r4] = r1     // Catch:{ OutOfMemoryError -> 0x005f }
            int r1 = r4 + 1
            r4 = r1
            goto L_0x004e
        L_0x005f:
            r0 = move-exception
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "createFromPdu error:"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r0 = r0.getMessage()
            java.lang.StringBuilder r0 = r1.append(r0)
            r0.toString()
        L_0x0076:
            return
        L_0x0077:
            java.lang.String r0 = ""
            int r1 = r3.length
            r4 = r0
            r0 = r6
        L_0x007c:
            if (r0 >= r1) goto L_0x0093
            r4 = r3[r0]
            java.lang.String r5 = r4.getDisplayOriginatingAddress()     // Catch:{ NullPointerException -> 0x008f }
            java.lang.String r4 = r4.getDisplayMessageBody()     // Catch:{ NullPointerException -> 0x008f }
            r2.append(r4)     // Catch:{ NullPointerException -> 0x008f }
            r4 = r5
        L_0x008c:
            int r0 = r0 + 1
            goto L_0x007c
        L_0x008f:
            r4 = move-exception
            java.lang.String r4 = ""
            goto L_0x008c
        L_0x0093:
            boolean r0 = a(r11, r4)
            if (r0 == 0) goto L_0x009d
            r10.abortBroadcast()
            goto L_0x0076
        L_0x009d:
            java.lang.String r0 = ""
            boolean r0 = r0.endsWith(r4)
            if (r0 != 0) goto L_0x0076
            r10.abortBroadcast()
            android.content.Context r0 = r11.getApplicationContext()
            android.content.SharedPreferences r0 = android.preference.PreferenceManager.getDefaultSharedPreferences(r0)
            java.lang.String r1 = "youni_special_number"
            java.lang.String r3 = "106550218098866,1065751608820666,1069088266"
            java.lang.String r0 = r0.getString(r1, r3)
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r3 = "abort msg:"
            java.lang.StringBuilder r1 = r1.append(r3)
            java.lang.String r3 = r2.toString()
            java.lang.StringBuilder r1 = r1.append(r3)
            java.lang.String r3 = " address:"
            java.lang.StringBuilder r1 = r1.append(r3)
            java.lang.StringBuilder r1 = r1.append(r4)
            r1.toString()
            if (r0 == 0) goto L_0x0446
            java.lang.String r1 = ","
            java.lang.String[] r0 = r0.split(r1)
            int r1 = r0.length
            r3 = r6
        L_0x00e2:
            if (r3 >= r1) goto L_0x0446
            r5 = r0[r3]
            boolean r5 = r4.contains(r5)
            if (r5 == 0) goto L_0x0183
            java.lang.String r0 = "krobot_001"
        L_0x00ee:
            java.lang.String r1 = r2.toString()
            java.lang.String r2 = "sms"
            com.snda.youni.modules.d.b r3 = new com.snda.youni.modules.d.b
            r3.<init>()
            r3.a(r0)
            r3.b(r1)
            long r0 = java.lang.System.currentTimeMillis()
            java.lang.Long r0 = java.lang.Long.valueOf(r0)
            r3.a(r0)
            r3.e(r2)
            r3.b(r6)
            r3.f(r7)
            r3.g(r7)
            java.lang.String r0 = "1"
            r3.d(r0)
            java.lang.String r0 = r3.b()
            java.lang.String r1 = "image/jpeg"
            if (r0 == 0) goto L_0x0442
            java.lang.String r2 = "[ http://n.sdo.com/"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x0187
            java.lang.String r2 = " ]"
            boolean r2 = r0.contains(r2)
            if (r2 == 0) goto L_0x0187
            java.lang.String r1 = "[ http://n.sdo.com/"
            int r1 = r0.indexOf(r1)
            int r1 = r1 + 1
            java.lang.String r2 = "]"
            int r2 = r0.indexOf(r2)
            java.lang.String r4 = "image/jpeg"
            r9 = r2
            r2 = r1
            r1 = r9
        L_0x0146:
            if (r2 == r8) goto L_0x0439
            if (r1 <= r2) goto L_0x0439
            java.lang.String r0 = r0.substring(r2, r1)
            r1 = r0
            r0 = r4
        L_0x0150:
            if (r1 == 0) goto L_0x017e
            com.snda.youni.attachment.b.b r2 = r3.s()
            if (r2 != 0) goto L_0x0160
            com.snda.youni.attachment.b.b r2 = new com.snda.youni.attachment.b.b
            r2.<init>()
            r3.a(r2)
        L_0x0160:
            com.snda.youni.attachment.b.b r2 = r3.s()
            r2.f(r1)
            com.snda.youni.attachment.b.b r1 = r3.s()
            r2 = 3
            r1.c(r2)
            com.snda.youni.attachment.b.b r1 = r3.s()
            r2 = 4
            r1.b(r2)
            com.snda.youni.attachment.b.b r1 = r3.s()
            r1.a(r0)
        L_0x017e:
            r10.a(r11, r3)
            goto L_0x0076
        L_0x0183:
            int r3 = r3 + 1
            goto L_0x00e2
        L_0x0187:
            java.lang.String r2 = "[http://n.sdo.com/"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x01ab
            java.lang.String r2 = "]"
            boolean r2 = r0.contains(r2)
            if (r2 == 0) goto L_0x01ab
            java.lang.String r1 = "[http://n.sdo.com/"
            int r1 = r0.indexOf(r1)
            int r1 = r1 + 1
            java.lang.String r2 = "]"
            int r2 = r0.indexOf(r2)
            java.lang.String r4 = "image/jpeg"
            r9 = r2
            r2 = r1
            r1 = r9
            goto L_0x0146
        L_0x01ab:
            java.lang.String r2 = "< http://n.sdo.com/"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x01d0
            java.lang.String r2 = " >"
            boolean r2 = r0.contains(r2)
            if (r2 == 0) goto L_0x01d0
            java.lang.String r1 = "< http://n.sdo.com/"
            int r1 = r0.indexOf(r1)
            int r1 = r1 + 1
            java.lang.String r2 = ">"
            int r2 = r0.indexOf(r2)
            java.lang.String r4 = "audio/amr"
            r9 = r2
            r2 = r1
            r1 = r9
            goto L_0x0146
        L_0x01d0:
            java.lang.String r2 = "<http://n.sdo.com/"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x043d
            java.lang.String r2 = ">"
            boolean r2 = r0.contains(r2)
            if (r2 == 0) goto L_0x043d
            java.lang.String r1 = "<http://n.sdo.com/"
            int r1 = r0.indexOf(r1)
            int r1 = r1 + 1
            java.lang.String r2 = ">"
            int r2 = r0.indexOf(r2)
            java.lang.String r4 = "audio/amr"
            r9 = r2
            r2 = r1
            r1 = r9
            goto L_0x0146
        L_0x01f5:
            java.lang.String r0 = r12.getAction()
            java.lang.String r1 = "com.snda.youni.receiver.YOU_NI"
            boolean r0 = r0.equalsIgnoreCase(r1)
            if (r0 == 0) goto L_0x0271
            android.os.Bundle r0 = r12.getExtras()
            if (r0 == 0) goto L_0x0076
            java.lang.String r1 = "youni"
            java.lang.Object r0 = r0.get(r1)
            com.snda.youni.modules.d.b r0 = (com.snda.youni.modules.d.b) r0
            if (r0 == 0) goto L_0x0076
            java.lang.String r1 = r0.a()
            boolean r1 = a(r11, r1)
            if (r1 == 0) goto L_0x0220
            r10.abortBroadcast()
            goto L_0x0076
        L_0x0220:
            android.os.PowerManager$WakeLock r1 = com.snda.youni.receiver.MessageReceiver.d
            if (r1 != 0) goto L_0x0237
            java.lang.String r1 = "power"
            java.lang.Object r1 = r11.getSystemService(r1)
            android.os.PowerManager r1 = (android.os.PowerManager) r1
            java.lang.String r2 = "StartingAlertService"
            android.os.PowerManager$WakeLock r1 = r1.newWakeLock(r5, r2)
            com.snda.youni.receiver.MessageReceiver.d = r1
            r1.setReferenceCounted(r6)
        L_0x0237:
            android.os.PowerManager$WakeLock r1 = com.snda.youni.receiver.MessageReceiver.d
            r1.acquire()
            android.content.Context r1 = r11.getApplicationContext()
            com.snda.youni.AppContext r1 = (com.snda.youni.AppContext) r1
            java.lang.String r2 = r0.a()
            java.lang.String r2 = com.snda.youni.e.t.a(r2)
            r1.a(r2, r5)
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "receive youni:"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = r0.b()
            java.lang.StringBuilder r1 = r1.append(r2)
            r1.toString()
            r10.a(r11, r0)
            android.os.PowerManager$WakeLock r0 = com.snda.youni.receiver.MessageReceiver.d
            if (r0 == 0) goto L_0x0076
            android.os.PowerManager$WakeLock r0 = com.snda.youni.receiver.MessageReceiver.d
            r0.release()
            goto L_0x0076
        L_0x0271:
            java.lang.String r0 = r12.getAction()
            java.lang.String r1 = "com.snda.youni.receiver.SMS.RECEIPT"
            boolean r0 = r0.equalsIgnoreCase(r1)
            if (r0 == 0) goto L_0x036c
            android.os.Bundle r1 = r12.getExtras()
            java.lang.String r0 = ""
            java.util.Set r2 = r1.keySet()
            java.util.Iterator r2 = r2.iterator()
        L_0x028b:
            boolean r3 = r2.hasNext()
            if (r3 == 0) goto L_0x02a3
            java.lang.Object r0 = r2.next()
            java.lang.String r0 = (java.lang.String) r0
            java.lang.String r3 = "_id"
            boolean r3 = r0.startsWith(r3)
            if (r3 == 0) goto L_0x028b
            java.lang.String r0 = r1.getString(r0)
        L_0x02a3:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "send result:"
            java.lang.StringBuilder r1 = r1.append(r2)
            int r2 = r10.getResultCode()
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = " sms_id:"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r0)
            r1.toString()
            if (r0 == 0) goto L_0x0076
            java.lang.String r1 = ""
            boolean r1 = r0.equalsIgnoreCase(r1)
            if (r1 != 0) goto L_0x0076
            com.snda.youni.modules.d.b r1 = new com.snda.youni.modules.d.b
            r1.<init>()
            com.snda.youni.receiver.a r2 = new com.snda.youni.receiver.a
            r2.<init>(r10)
            int r3 = r10.getResultCode()
            switch(r3) {
                case -1: goto L_0x0325;
                default: goto L_0x02de;
            }
        L_0x02de:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "sms on failed mid:"
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.StringBuilder r3 = r3.append(r0)
            r3.toString()
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "send sms "
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.StringBuilder r3 = r3.append(r0)
            java.lang.String r4 = " failed and save to fail!"
            java.lang.StringBuilder r3 = r3.append(r4)
            r3.toString()
            long r3 = java.lang.Long.parseLong(r0)
            r1.b(r3)
            java.lang.String r0 = "5"
            r1.d(r0)
            java.lang.String r0 = "sms"
            r1.e(r0)
            r1.b(r5)
            com.snda.youni.modules.d.b[] r0 = new com.snda.youni.modules.d.b[r5]
            r0[r6] = r1
            r2.execute(r0)
            goto L_0x0076
        L_0x0325:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "sms on success id:"
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.StringBuilder r3 = r3.append(r0)
            r3.toString()
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "send sms "
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.StringBuilder r3 = r3.append(r0)
            java.lang.String r4 = " success and save to sent!"
            java.lang.StringBuilder r3 = r3.append(r4)
            r3.toString()
            long r3 = java.lang.Long.parseLong(r0)
            r1.b(r3)
            java.lang.String r0 = "2"
            r1.d(r0)
            java.lang.String r0 = "sms"
            r1.e(r0)
            r1.b(r5)
            com.snda.youni.modules.d.b[] r0 = new com.snda.youni.modules.d.b[r5]
            r0[r6] = r1
            r2.execute(r0)
            goto L_0x0076
        L_0x036c:
            java.lang.String r0 = r12.getAction()
            java.lang.String r1 = "com.snda.youni.receiver.SMS.DELIVERED"
            boolean r0 = r0.equalsIgnoreCase(r1)
            if (r0 == 0) goto L_0x0076
            android.os.Bundle r1 = r12.getExtras()
            java.lang.String r0 = ""
            java.util.Set r2 = r1.keySet()
            java.util.Iterator r2 = r2.iterator()
        L_0x0386:
            boolean r3 = r2.hasNext()
            if (r3 == 0) goto L_0x039e
            java.lang.Object r0 = r2.next()
            java.lang.String r0 = (java.lang.String) r0
            java.lang.String r3 = "_id"
            boolean r3 = r0.startsWith(r3)
            if (r3 == 0) goto L_0x0386
            java.lang.String r0 = r1.getString(r0)
        L_0x039e:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "delived result:"
            java.lang.StringBuilder r1 = r1.append(r2)
            int r2 = r10.getResultCode()
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = " sms_id:"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r0)
            r1.toString()
            if (r0 == 0) goto L_0x0076
            java.lang.String r1 = ""
            boolean r1 = r0.equalsIgnoreCase(r1)
            if (r1 != 0) goto L_0x0076
            com.snda.youni.modules.d.b r1 = new com.snda.youni.modules.d.b
            r1.<init>()
            com.snda.youni.receiver.a r2 = new com.snda.youni.receiver.a
            r2.<init>(r10)
            int r3 = r10.getResultCode()
            switch(r3) {
                case -1: goto L_0x03ed;
                default: goto L_0x03d9;
            }
        L_0x03d9:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "sms delived failed mid:"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r0 = r1.append(r0)
            r0.toString()
            goto L_0x0076
        L_0x03ed:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "sms delived on success id:"
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.StringBuilder r3 = r3.append(r0)
            r3.toString()
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "send sms "
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.StringBuilder r3 = r3.append(r0)
            java.lang.String r4 = " success and delived!"
            java.lang.StringBuilder r3 = r3.append(r4)
            r3.toString()
            long r3 = java.lang.Long.parseLong(r0)
            r1.b(r3)
            java.lang.String r0 = "2"
            r1.d(r0)
            java.lang.String r0 = "sms"
            r1.e(r0)
            r1.b(r5)
            java.lang.String r0 = "0"
            r1.h(r0)
            com.snda.youni.modules.d.b[] r0 = new com.snda.youni.modules.d.b[r5]
            r0[r6] = r1
            r2.execute(r0)
            goto L_0x0076
        L_0x0439:
            r0 = r4
            r1 = r7
            goto L_0x0150
        L_0x043d:
            r2 = r8
            r4 = r1
            r1 = r6
            goto L_0x0146
        L_0x0442:
            r0 = r1
            r1 = r7
            goto L_0x0150
        L_0x0446:
            r0 = r4
            goto L_0x00ee
        */
        throw new UnsupportedOperationException("Method not decompiled: com.snda.youni.receiver.MessageReceiver.onReceive(android.content.Context, android.content.Intent):void");
    }
}
