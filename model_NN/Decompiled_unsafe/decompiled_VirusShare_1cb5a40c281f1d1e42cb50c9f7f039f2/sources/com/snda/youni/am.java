package com.snda.youni;

import android.database.Cursor;
import android.text.TextUtils;
import com.snda.youni.b.ai;
import com.snda.youni.b.s;
import com.snda.youni.e.q;
import com.snda.youni.e.t;
import com.snda.youni.providers.n;
import java.util.ArrayList;
import java.util.concurrent.ConcurrentHashMap;

public final class am {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public static final ConcurrentHashMap f313a = new ConcurrentHashMap();
    /* access modifiers changed from: private */
    public static long b = 0;
    private static s c = new ao();

    public static void a(ai aiVar, boolean z) {
        Cursor query;
        "requestLoadStatus, System.currentTimeMillis()=" + System.currentTimeMillis() + ", sUpdateTime=" + b;
        if (!TextUtils.isEmpty(com.snda.youni.e.s.f403a)) {
            if (aiVar == null || !aiVar.b()) {
                "requestLoadStatus, network =" + aiVar + " network.isConnected()=" + (aiVar == null ? "null" : Boolean.valueOf(aiVar.b()));
            } else if ((z || System.currentTimeMillis() - b >= 60000) && (query = AppContext.a().getContentResolver().query(n.f597a, new String[]{"phone_number", "expand_data1"}, "contact_type=1", null, null)) != null) {
                f313a.clear();
                query.moveToPosition(-1);
                ArrayList arrayList = new ArrayList();
                while (query.moveToNext()) {
                    String a2 = q.a(t.a(query.getString(0)));
                    String string = query.getString(1);
                    arrayList.add(a2);
                    if (string != null && !TextUtils.isEmpty(a2)) {
                        f313a.put(a2, string);
                    }
                }
                query.close();
                aiVar.a(arrayList, c);
            }
        }
    }
}
