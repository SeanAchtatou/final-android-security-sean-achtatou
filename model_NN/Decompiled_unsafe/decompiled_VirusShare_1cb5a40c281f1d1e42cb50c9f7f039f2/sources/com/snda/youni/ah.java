package com.snda.youni;

import android.view.View;
import android.widget.Toast;
import com.snda.youni.modules.a.t;

final class ah implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ YouNi f309a;

    ah(YouNi youNi) {
        this.f309a = youNi;
    }

    public final void onClick(View view) {
        if (((t) this.f309a.k.c().getAdapter()).d() == 0) {
            Toast.makeText(this.f309a, (int) C0000R.string.no_select_notification, 0).show();
        } else {
            this.f309a.showDialog(16);
        }
    }
}
