package com.snda.youni;

import android.content.ContentValues;
import android.content.DialogInterface;
import android.database.Cursor;
import android.net.Uri;
import android.widget.Toast;
import com.snda.youni.e.j;
import com.snda.youni.providers.l;

final class z implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ YouNi f626a;

    z(YouNi youNi) {
        this.f626a = youNi;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        Cursor query = this.f626a.getContentResolver().query(Uri.parse("content://sms/"), new String[]{"canonical_addresses._id from canonical_addresses where PHONE_NUMBERS_EQUAL(canonical_addresses.address, '" + this.f626a.l.f518a.g + "') --"}, null, null, null);
        if (query.moveToNext()) {
            this.f626a.l.f518a.n = "" + query.getInt(0);
            "ljd addressId for #" + this.f626a.l.f518a.g + "# is " + query.getInt(0);
        }
        query.close();
        String a2 = j.a(this.f626a.l.f518a.g);
        Cursor query2 = this.f626a.getContentResolver().query(l.f596a, new String[]{"blacker_rid"}, "blacker_sid='" + a2 + "'", null, null);
        if (query2.getCount() > 0) {
            Toast.makeText(this.f626a, this.f626a.getString(C0000R.string.add_duplicate_person_to_black_list, new Object[]{this.f626a.l.f518a.f496a}), 0).show();
            query2.close();
            return;
        }
        query2.close();
        ContentValues contentValues = new ContentValues();
        contentValues.put("blacker_rid", this.f626a.l.f518a.n);
        contentValues.put("blacker_name", this.f626a.l.f518a.f496a);
        contentValues.put("blacker_phone", this.f626a.l.f518a.g);
        contentValues.put("blacker_sid", a2);
        if (this.f626a.getContentResolver().insert(l.f596a, contentValues) != null) {
            Toast.makeText(this.f626a, (int) C0000R.string.add_to_black_list_succeed, 0).show();
        } else {
            Toast.makeText(this.f626a, (int) C0000R.string.add_to_black_list_failed, 0).show();
        }
        "ljd Add " + this.f626a.l.f518a.n + " to black list";
    }
}
