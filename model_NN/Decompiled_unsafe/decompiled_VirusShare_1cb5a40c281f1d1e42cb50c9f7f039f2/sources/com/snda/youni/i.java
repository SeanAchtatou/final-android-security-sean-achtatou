package com.snda.youni;

import android.content.Intent;
import android.view.View;
import android.widget.Toast;
import com.snda.youni.modules.a.k;
import com.snda.youni.modules.m;

final class i implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ YouNi f428a;

    i(YouNi youNi) {
        this.f428a = youNi;
    }

    public final void onClick(View view) {
        if (((m) this.f428a.l.c().getAdapter()).b().size() == 0) {
            Toast.makeText(this.f428a, (int) C0000R.string.no_select_notification, 0).show();
            return;
        }
        Intent intent = new Intent();
        k kVar = new k();
        kVar.b = this.f428a.getIntent().getStringExtra("message_body");
        kVar.g = ((m) this.f428a.l.c().getAdapter()).d();
        intent.putExtra("item", kVar);
        this.f428a.setResult(-1, intent);
        this.f428a.finish();
    }
}
