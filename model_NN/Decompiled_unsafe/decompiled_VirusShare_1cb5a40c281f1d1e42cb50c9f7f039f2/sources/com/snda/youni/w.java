package com.snda.youni;

import android.content.DialogInterface;
import android.content.Intent;
import com.snda.youni.modules.a.t;

final class w implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ YouNi f623a;

    w(YouNi youNi) {
        this.f623a = youNi;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        Intent intent = new Intent();
        intent.putExtra("thread_ids", ((t) this.f623a.k.c().getAdapter()).c());
        this.f623a.setResult(300, intent);
        this.f623a.finish();
    }
}
