package com.snda.youni;

import android.content.DialogInterface;
import android.content.Intent;
import com.snda.youni.modules.m;

final class t implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ YouNi f620a;

    t(YouNi youNi) {
        this.f620a = youNi;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        Intent intent = new Intent();
        intent.putExtra("contact_id", ((m) this.f620a.l.c().getAdapter()).e());
        this.f620a.setResult(200, intent);
        this.f620a.finish();
    }
}
