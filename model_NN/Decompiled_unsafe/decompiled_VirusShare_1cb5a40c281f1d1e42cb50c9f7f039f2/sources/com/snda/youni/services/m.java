package com.snda.youni.services;

import android.database.ContentObserver;
import android.os.Handler;

final class m extends ContentObserver {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ ContactsService f618a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public m(ContactsService contactsService) {
        super(new Handler());
        this.f618a = contactsService;
    }

    public final void onChange(boolean z) {
        this.f618a.f();
    }
}
