package com.snda.youni.services;

import android.database.ContentObserver;
import android.os.Handler;
import com.snda.youni.modules.c.b;

final class g extends ContentObserver {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ YouniService f612a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public g(YouniService youniService, Handler handler) {
        super(handler);
        this.f612a = youniService;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.snda.youni.services.YouniService.a(com.snda.youni.services.YouniService, java.lang.Integer):java.lang.Integer
     arg types: [com.snda.youni.services.YouniService, int]
     candidates:
      com.snda.youni.services.YouniService.a(com.snda.youni.services.YouniService, android.content.Context):void
      com.snda.youni.services.YouniService.a(com.snda.youni.services.YouniService, java.lang.Integer):java.lang.Integer */
    public final void onChange(boolean z) {
        int intValue;
        "selfChange = " + z;
        super.onChange(z);
        synchronized (this.f612a.f605a) {
            Integer unused = this.f612a.f605a = Integer.valueOf(this.f612a.f605a.intValue() + 1);
            intValue = this.f612a.f605a.intValue();
        }
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        synchronized (this.f612a.f605a) {
            if (this.f612a.f605a.intValue() == intValue) {
                Integer unused2 = this.f612a.f605a = (Integer) 0;
                YouniService.a(this.f612a, this.f612a.getApplicationContext());
                b.b(this.f612a.getApplicationContext());
            }
        }
    }
}
