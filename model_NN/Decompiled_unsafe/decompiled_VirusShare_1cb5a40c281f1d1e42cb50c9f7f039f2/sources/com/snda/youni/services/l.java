package com.snda.youni.services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.NetworkInfo;
import android.os.Message;
import com.snda.youni.AppContext;
import com.snda.youni.YouNi;
import com.snda.youni.e.s;

final class l extends BroadcastReceiver {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ ContactsService f617a;

    l(ContactsService contactsService) {
        this.f617a = contactsService;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.snda.youni.services.ContactsService.a(com.snda.youni.services.ContactsService, boolean):boolean
     arg types: [com.snda.youni.services.ContactsService, int]
     candidates:
      com.snda.youni.services.ContactsService.a(com.snda.youni.services.ContactsService, com.snda.youni.services.a):com.snda.youni.services.a
      com.snda.youni.services.ContactsService.a(com.snda.youni.services.ContactsService, java.lang.Integer):java.lang.Integer
      com.snda.youni.services.ContactsService.a(com.snda.youni.services.ContactsService, android.os.Message):void
      com.snda.youni.services.ContactsService.a(com.snda.youni.services.ContactsService, boolean):boolean */
    public final void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if ("com.snda.youni.action.REGISTER_SUCCEEDED".equals(action)) {
            this.f617a.e();
        } else if ("com.snda.youni.ACTION_LOAD_FRIENDS_LIST".equals(action)) {
            this.f617a.e();
        } else if ("com.snda.youni.ACTION_UPDATE_TIMES_CONTACTED".equals(action)) {
            Message message = new Message();
            message.what = 2;
            this.f617a.a(message);
        } else if ("android.net.conn.CONNECTIVITY_CHANGE".equals(action)) {
            NetworkInfo networkInfo = (NetworkInfo) intent.getParcelableExtra("networkInfo");
            if (networkInfo == null || !networkInfo.isAvailable()) {
                boolean unused = this.f617a.g = false;
                this.f617a.sendBroadcast(new Intent("com.snda.youni.action.CONTACTS_CHANGED"));
                return;
            }
            boolean unused2 = this.f617a.g = true;
            this.f617a.e();
        } else if ("com.snda.youni.action.ACTION_FRIEND_LIST_NEW".equals(action)) {
            YouNi.f170a = true;
            if (YouNi.b == null || s.d == 7 || s.d == 3) {
                ((AppContext) this.f617a.getApplication()).a(intent.getIntExtra("friend_list_new_count", 1), intent.getStringExtra("friend_list_new_name"), true);
                return;
            }
            Message message2 = new Message();
            message2.what = 3;
            message2.obj = intent;
            YouNi.b.c.sendMessage(message2);
        }
    }
}
