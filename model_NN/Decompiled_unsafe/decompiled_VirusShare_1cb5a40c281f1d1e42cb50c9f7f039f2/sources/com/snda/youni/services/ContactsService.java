package com.snda.youni.services;

import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.preference.PreferenceManager;
import android.provider.ContactsContract;
import android.text.TextUtils;
import android.text.format.Time;
import com.snda.youni.c.a.b;
import com.snda.youni.c.d;
import com.snda.youni.c.t;
import com.snda.youni.e.q;
import com.snda.youni.e.u;
import com.snda.youni.f.a;
import com.snda.youni.f.c;
import com.snda.youni.providers.n;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Timer;
import java.util.concurrent.ConcurrentHashMap;

public class ContactsService extends DefaultService implements Handler.Callback {
    private static final String[] m = {"contact_id", "display_name", "data1"};
    private static final String[] n = {"contact_id", "display_name", "mimetype", "data1", "times_contacted"};
    private static final String[] o = {"contact_id", "display_name", "phone_number", "sid", "nick_name", "signature", "contact_type"};
    private static final String[] p = {"display_name", "phone_number", "emails"};
    private static final String[] q = {"data1"};
    /* access modifiers changed from: private */
    public static final String[] r = {"contact_id", "sid", "friend_timestamp", "photo_timestamp"};
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public int f603a = 0;
    /* access modifiers changed from: private */
    public Integer b = 0;
    /* access modifiers changed from: private */
    public Integer c = 0;
    private ContentObserver d = new m(this);
    /* access modifiers changed from: private */
    public ContentResolver e;
    /* access modifiers changed from: private */
    public final ConcurrentHashMap f = new ConcurrentHashMap();
    /* access modifiers changed from: private */
    public boolean g = true;
    private boolean h;
    private i i;
    private final Handler j = new Handler(this);
    /* access modifiers changed from: private */
    public Timer k;
    private BroadcastReceiver l = new l(this);
    private Binder s = new b(this);
    /* access modifiers changed from: private */
    public a t = new a(this);
    private a u = new k(this);
    private a v = new j(this);

    private int a(ArrayList arrayList) {
        Iterator it = arrayList.iterator();
        ArrayList arrayList2 = new ArrayList();
        int i2 = 0;
        while (it.hasNext()) {
            f fVar = (f) it.next();
            b bVar = fVar.f611a;
            String e2 = bVar.e();
            String b2 = bVar.b();
            String c2 = bVar.c();
            if (!TextUtils.isEmpty(e2)) {
                if (!TextUtils.isEmpty(b2) || !TextUtils.isEmpty(c2)) {
                    String a2 = u.a(b2.trim());
                    String b3 = u.b(b2.trim());
                    if (TextUtils.isEmpty(a2)) {
                        a2 = b3;
                    }
                    ContentValues contentValues = new ContentValues();
                    contentValues.put("sid", e2);
                    contentValues.put("display_name", b2);
                    contentValues.put("pinyin_name", a2);
                    contentValues.put("search_name", b3);
                    contentValues.put("phone_number", c2);
                    if (!fVar.c) {
                        contentValues.put("contact_id", Integer.valueOf(fVar.b));
                    }
                    int update = this.e.update(n.f597a, contentValues, "sid=?", new String[]{e2});
                    if (update <= 0) {
                        contentValues.put("contact_id", Integer.valueOf(fVar.b));
                        arrayList2.add(contentValues);
                    } else {
                        i2 += update;
                    }
                } else {
                    i2 = this.e.delete(n.f597a, "sid=?", new String[]{e2}) + i2;
                }
            }
        }
        if (i2 > 0) {
            sendBroadcast(new Intent("com.snda.youni.action.CONTACTS_CHANGED"));
        }
        int size = arrayList2.size();
        ContentValues[] contentValuesArr = (ContentValues[]) arrayList2.toArray(new ContentValues[size]);
        ContentValues[] contentValuesArr2 = new ContentValues[50];
        int i3 = i2;
        boolean z = false;
        for (int i4 = 0; i4 < size; i4 += 50) {
            if (i4 + 50 > size - 1) {
                ContentValues[] contentValuesArr3 = new ContentValues[(size - i4)];
                "> size-1, allInsertValues.length=" + contentValuesArr.length + ", pos=" + i4 + ", insertValues.length=" + contentValuesArr3.length;
                System.arraycopy(contentValuesArr, i4, contentValuesArr3, 0, contentValuesArr3.length);
                contentValuesArr2 = contentValuesArr3;
                z = true;
            } else {
                "<=size-1, allInsertValues.length=" + contentValuesArr.length + ", pos=" + i4 + ", insertValues.length=" + contentValuesArr2.length;
                System.arraycopy(contentValuesArr, i4, contentValuesArr2, 0, 50);
            }
            int bulkInsert = this.e.bulkInsert(n.f597a, contentValuesArr2);
            if (bulkInsert > 0) {
                i3 += bulkInsert;
                if (!z) {
                    sendBroadcast(new Intent("com.snda.youni.action.CONTACTS_CHANGED"));
                }
            }
        }
        if (i3 > 0) {
            sendBroadcast(new Intent("com.snda.youni.action.CONTACTS_CHANGED"));
            Message message = new Message();
            message.what = 2;
            a(message);
        }
        return i3;
    }

    private String a(int i2) {
        String str;
        Cursor query = this.e.query(ContactsContract.Data.CONTENT_URI, q, "contact_id=? AND mimetype='vnd.android.cursor.item/email_v2'", new String[]{String.valueOf(i2)}, null);
        if (query == null) {
            return "";
        }
        int count = query.getCount();
        if (count > 3) {
            count = 3;
        }
        if (count > 0) {
            int columnIndex = query.getColumnIndex("data1");
            query.moveToFirst();
            String str2 = "";
            for (int i3 = 0; i3 < count - 1; i3++) {
                str2 = str2.concat(query.getString(columnIndex) + ":");
                query.moveToNext();
            }
            str = str2.concat(query.getString(columnIndex));
        } else {
            str = "";
        }
        query.close();
        return str;
    }

    /* access modifiers changed from: private */
    public void a(Message message) {
        this.j.sendMessage(message);
    }

    /* access modifiers changed from: private */
    public void e() {
        "requestPendingRequests, mPendingRequests.size=" + this.f.size();
        if (this.g) {
            if (this.f.isEmpty()) {
                this.t.cancel();
                Message message = new Message();
                message.what = 4;
                a(message);
                return;
            }
            for (d a2 : this.f.keySet()) {
                t.a(a2, this.u, this);
            }
        }
    }

    /* access modifiers changed from: private */
    public void f() {
        if (!this.h) {
            this.h = true;
            this.j.sendEmptyMessage(1);
        }
    }

    static /* synthetic */ int g(ContactsService contactsService) {
        int i2 = contactsService.f603a;
        contactsService.f603a = i2 + 1;
        return i2;
    }

    private void g() {
        if (!PreferenceManager.getDefaultSharedPreferences(this).getBoolean("contacts_synced", false)) {
            SharedPreferences.Editor edit = PreferenceManager.getDefaultSharedPreferences(this).edit();
            edit.putBoolean("contacts_synced", true);
            edit.commit();
            com.snda.youni.modules.b.b.a();
        }
    }

    static /* synthetic */ void m(ContactsService contactsService) {
        SharedPreferences.Editor edit = PreferenceManager.getDefaultSharedPreferences(contactsService).edit();
        edit.putBoolean("sid_11_bit_updated", true);
        edit.commit();
    }

    public final ArrayList a() {
        String str;
        Cursor query = this.e.query(ContactsContract.Data.CONTENT_URI, m, "mimetype='vnd.android.cursor.item/phone_v2'", null, null);
        if (query != null) {
            "getModifiedContacts, lookUpCursor.count=" + query.getCount();
        }
        HashMap hashMap = new HashMap();
        ArrayList arrayList = new ArrayList();
        if (query != null) {
            query.moveToPosition(-1);
            while (query.moveToNext()) {
                int i2 = query.getInt(0);
                String stripSeparators = q.stripSeparators(query.getString(2));
                if (!TextUtils.isEmpty(stripSeparators)) {
                    String callerIDMinMatch = q.toCallerIDMinMatch(stripSeparators);
                    String string = query.getString(1);
                    if (string == null) {
                        string = "";
                    }
                    String[] split = string.split(" ");
                    int length = split.length;
                    if (length <= 1 || String.valueOf(split[length - 1].charAt(0)).getBytes().length <= 1) {
                        str = string;
                    } else {
                        int i3 = length - 2;
                        String str2 = split[length - 1];
                        for (int i4 = i3; i4 >= 0; i4--) {
                            str2 = str2.concat(split[i4]);
                        }
                        str = str2;
                    }
                    if (hashMap.get(callerIDMinMatch) == null) {
                        hashMap.put(callerIDMinMatch, new f(new b(callerIDMinMatch, str, stripSeparators, "", "phone"), i2));
                    }
                }
            }
            query.close();
        }
        "getModifiedContacts, lookUpMap.count=" + hashMap.size();
        Cursor query2 = this.e.query(n.f597a, o, null, null, null);
        if (query2 != null) {
            "getModifiedContacts, infoCursor.count=" + query2.getCount();
        }
        if (query2 != null) {
            query2.moveToFirst();
            while (!query2.isAfterLast()) {
                String string2 = query2.getString(2);
                if (!TextUtils.isEmpty(string2) && !string2.startsWith("krobot")) {
                    String string3 = query2.getString(1);
                    String string4 = query2.getString(3);
                    f fVar = (f) hashMap.get(string4);
                    if (fVar == null) {
                        "going to delete number: " + string2;
                        arrayList.add(new f(new b(string4, "", "", "", ""), 0));
                    } else {
                        b bVar = fVar.f611a;
                        int i5 = query2.getInt(0);
                        "lookupValues = " + fVar.toString() + " youni.contactId=" + i5;
                        if (!bVar.c().equals(string2) || !bVar.b().equals(string3)) {
                            "going to update number to youni db: " + string2;
                            arrayList.add(fVar);
                        } else if (fVar.b != i5) {
                            "going to update number to youni db: " + string2 + " contactId changed";
                            boolean unused = fVar.c = false;
                            arrayList.add(fVar);
                        }
                        hashMap.remove(string4);
                    }
                }
                query2.moveToNext();
            }
            for (Map.Entry entry : hashMap.entrySet()) {
                f fVar2 = (f) entry.getValue();
                "going to insert number to youni db: " + ((f) entry.getValue()).f611a.c();
                fVar2.f611a.b(a(fVar2.b));
                arrayList.add(fVar2);
            }
            query2.close();
        }
        return arrayList;
    }

    public final void a(ArrayList arrayList, boolean z, boolean z2) {
        if (arrayList == null || arrayList.isEmpty()) {
            e();
            g();
            return;
        }
        Iterator it = arrayList.iterator();
        ArrayList arrayList2 = new ArrayList();
        while (it.hasNext()) {
            f fVar = (f) it.next();
            if (fVar.c && !TextUtils.isEmpty(fVar.f611a.e())) {
                arrayList2.add(fVar.f611a);
            }
        }
        if (arrayList2.size() > 0) {
            d dVar = new d();
            dVar.a(arrayList2);
            this.f.put(dVar, 2000);
            if (this.g) {
                if (z) {
                    t.b(dVar, this.u, this);
                } else {
                    t.a(dVar, this.u, this);
                }
            }
        }
        if (z2) {
            a(arrayList);
            g();
        }
    }

    public final ArrayList b() {
        ArrayList arrayList = new ArrayList();
        HashMap hashMap = new HashMap();
        Cursor query = this.e.query(ContactsContract.Data.CONTENT_URI, n, " (mimetype='vnd.android.cursor.item/phone_v2' OR mimetype='vnd.android.cursor.item/email_v2')", null, null);
        if (query == null) {
            return null;
        }
        query.moveToPosition(-1);
        while (query.moveToNext()) {
            Integer valueOf = Integer.valueOf(query.getInt(0));
            String string = query.getString(1);
            String string2 = query.getString(2);
            int i2 = query.getInt(4);
            d dVar = (d) hashMap.get(valueOf);
            if (string2.equals("vnd.android.cursor.item/phone_v2")) {
                String string3 = query.getString(3);
                if (!TextUtils.isEmpty(string3) && !TextUtils.isEmpty(string3.trim())) {
                    if (dVar == null) {
                        ArrayList arrayList2 = new ArrayList();
                        arrayList2.add(string3);
                        hashMap.put(valueOf, new d(string, arrayList2, new StringBuilder(), i2));
                    } else if (!dVar.b.contains(string3)) {
                        dVar.b.add(string3);
                        hashMap.put(valueOf, dVar);
                    }
                }
            } else if (string2.equals("vnd.android.cursor.item/email_v2")) {
                String string4 = query.getString(3);
                if (!TextUtils.isEmpty(string4) && !TextUtils.isEmpty(string4.trim())) {
                    if (dVar == null) {
                        StringBuilder sb = new StringBuilder();
                        sb.setLength(0);
                        sb.append(string4);
                        hashMap.put(valueOf, new d(string, new ArrayList(), sb, i2));
                    } else if (dVar.c.lastIndexOf(string4) < 0) {
                        dVar.c.append(':').append(string4);
                        hashMap.put(valueOf, dVar);
                    }
                }
            }
        }
        "getAllContacts, cursor.count=" + query.getCount();
        query.close();
        "getAllContacts, lookUpMap.count=" + hashMap.size();
        for (Map.Entry entry : hashMap.entrySet()) {
            d dVar2 = (d) entry.getValue();
            int intValue = ((Integer) entry.getKey()).intValue();
            ArrayList a2 = dVar2.b;
            Iterator it = a2.iterator();
            while (it.hasNext()) {
                String stripSeparators = q.stripSeparators((String) it.next());
                arrayList.add(new f(new b(q.toCallerIDMinMatch(stripSeparators), dVar2.f609a, stripSeparators, dVar2.c.toString(), "phone"), intValue));
            }
            if (a2.size() > 1) {
                "getAllContacts, phoneList=" + TextUtils.join(",", a2.toArray());
            }
        }
        return arrayList;
    }

    public final void c() {
        String string = PreferenceManager.getDefaultSharedPreferences(this).getString("friends_list_version", null);
        "loadFriendList, friend_list_version=" + string;
        com.snda.youni.c.b bVar = new com.snda.youni.c.b();
        if (!TextUtils.isEmpty(string)) {
            bVar.b(string);
        }
        new c().a(bVar);
        if (this.g) {
            t.a(bVar, this.v, this);
        }
    }

    public boolean handleMessage(Message message) {
        if (this.i == null) {
            this.i = new i(this, this.e);
            this.i.start();
        }
        if (1 == message.what) {
            this.h = false;
        }
        this.i.a(message);
        return true;
    }

    public IBinder onBind(Intent intent) {
        return this.s;
    }

    public void onCreate() {
        super.onCreate();
        this.e = getContentResolver();
        this.e.registerContentObserver(ContactsContract.Data.CONTENT_URI, true, this.d);
        this.k = new Timer();
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) getSystemService("connectivity")).getActiveNetworkInfo();
        if (activeNetworkInfo == null || !activeNetworkInfo.isAvailable()) {
            this.g = false;
        } else {
            this.g = true;
        }
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.snda.youni.action.REGISTER_SUCCEEDED");
        intentFilter.addAction("com.snda.youni.ACTION_LOAD_FRIENDS_LIST");
        intentFilter.addAction("com.snda.youni.ACTION_UPDATE_TIMES_CONTACTED");
        intentFilter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
        intentFilter.addAction("com.snda.youni.action.ACTION_FRIEND_LIST_NEW");
        registerReceiver(this.l, intentFilter);
        h hVar = new h(this);
        long random = 7200000 + ((long) (Math.random() * 1.8E7d));
        Time time = new Time();
        time.setToNow();
        long j2 = random - ((long) ((((time.hour * 3600) + (time.minute * 60)) + time.second) * 1000));
        if (j2 < 0) {
            j2 += 86400000;
        }
        "time = " + time.toString();
        time.set(time.toMillis(true) + j2);
        "runTime=" + (random / 3600000) + " when = " + time.toString();
        this.k.schedule(hVar, j2, 86400000);
        f();
    }

    public void onDestroy() {
        super.onDestroy();
        this.e.unregisterContentObserver(this.d);
        unregisterReceiver(this.l);
        this.k.cancel();
    }
}
