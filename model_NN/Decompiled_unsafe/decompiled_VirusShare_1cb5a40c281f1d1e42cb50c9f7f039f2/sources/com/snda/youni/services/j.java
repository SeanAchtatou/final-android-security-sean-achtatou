package com.snda.youni.services;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Message;
import android.preference.PreferenceManager;
import com.snda.youni.c.i;
import com.snda.youni.f.a;
import com.snda.youni.f.c;
import com.snda.youni.f.d;
import java.util.List;

final class j implements a {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ ContactsService f615a;

    j(ContactsService contactsService) {
        this.f615a = contactsService;
    }

    public final void a(c cVar, d dVar) {
        int c = dVar.c();
        switch (c) {
            case 1:
            case 3:
            case 8:
                if (this.f615a.f603a <= 5) {
                    this.f615a.t.cancel();
                    a unused = this.f615a.t = new a(this.f615a);
                    this.f615a.k.schedule(this.f615a.t, (long) ((2 << this.f615a.f603a) * 2000));
                    ContactsService.g(this.f615a);
                    return;
                }
                return;
            case 2:
            case 5:
            case 6:
            case 7:
            default:
                i iVar = (i) dVar.b();
                if (iVar != null) {
                    switch (iVar.b()) {
                        case 0:
                            int unused2 = this.f615a.f603a = 0;
                            List d = ((i) dVar.b()).d();
                            Message message = new Message();
                            message.obj = d;
                            message.what = 3;
                            this.f615a.a(message);
                            SharedPreferences.Editor edit = PreferenceManager.getDefaultSharedPreferences(this.f615a).edit();
                            edit.putString("friends_list_version", iVar.c());
                            edit.commit();
                            "finish load friend list, friend_list_version=" + iVar.c();
                            return;
                        case 1:
                            if (this.f615a.f.isEmpty() && this.f615a.f603a <= 5) {
                                this.f615a.t.cancel();
                                a unused3 = this.f615a.t = new a(this.f615a);
                                this.f615a.k.schedule(this.f615a.t, (long) ((2 << this.f615a.f603a) * 2000));
                                ContactsService.g(this.f615a);
                                return;
                            }
                            return;
                        default:
                            return;
                    }
                } else {
                    "Network maybe not connected, result=" + c;
                    return;
                }
            case 4:
                return;
            case 9:
                this.f615a.sendBroadcast(new Intent("com.snda.youni.action_NETWORK_EXCEPTION"));
                return;
        }
    }

    public final void a(Exception exc, String str) {
    }
}
