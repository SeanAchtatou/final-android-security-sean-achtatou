package com.snda.youni.services;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import com.snda.youni.g.f;

public class DefaultService extends Service {

    /* renamed from: a  reason: collision with root package name */
    private HandlerThread f604a;
    private Handler b;

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onCreate() {
        super.onCreate();
        this.f604a = new HandlerThread("Youni");
        this.f604a.start();
        this.b = new Handler(this.f604a.getLooper());
        Thread.setDefaultUncaughtExceptionHandler(new f(this));
    }
}
