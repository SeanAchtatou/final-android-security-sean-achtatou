package com.snda.youni.services;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import com.snda.youni.c.a.a;
import com.snda.youni.e.q;
import com.snda.youni.e.u;
import com.snda.youni.providers.n;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

final class i extends HandlerThread implements Handler.Callback {

    /* renamed from: a  reason: collision with root package name */
    private Handler f614a;
    private ContentResolver b;
    private ArrayList c = new ArrayList();
    private ArrayList d = new ArrayList();
    private ArrayList e = new ArrayList();
    private /* synthetic */ ContactsService f;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public i(ContactsService contactsService, ContentResolver contentResolver) {
        super("sync_thread");
        this.f = contactsService;
        this.b = contentResolver;
    }

    private List a(List list) {
        Cursor query = this.b.query(n.f597a, ContactsService.r, "contact_type=1", null, null);
        if (query == null) {
            return null;
        }
        HashMap hashMap = new HashMap();
        query.moveToPosition(-1);
        while (query.moveToNext()) {
            int i = query.getInt(0);
            String string = query.getString(1);
            long j = query.getLong(2);
            String string2 = query.getString(3);
            if (!TextUtils.isEmpty(string)) {
                hashMap.put(string, new n(i, j, string2));
            }
        }
        if (query != null) {
            query.close();
        }
        this.e.clear();
        Iterator it = list.iterator();
        while (it.hasNext()) {
            a aVar = (a) it.next();
            String f2 = aVar.f();
            String callerIDMinMatch = f2.startsWith("krobot") ? f2 : q.toCallerIDMinMatch(aVar.f());
            n nVar = (n) hashMap.get(callerIDMinMatch);
            if (nVar == null) {
                "mSIdsWhichIconChanged.add: " + callerIDMinMatch;
                this.e.add(callerIDMinMatch);
                this.d.add(callerIDMinMatch);
            } else if (aVar.g() == nVar.b) {
                "remove from list,  moblie=" + aVar.f() + " name=" + aVar.c() + "updateTime=" + aVar.g();
                it.remove();
            } else if (aVar.e().equals(nVar.c)) {
                aVar.e(null);
            } else {
                "mContactIdsWhichIconChanged.add: " + nVar.f619a;
                this.c.add(String.valueOf(nVar.f619a));
            }
        }
        return list;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    private int b(List list) {
        String str;
        boolean z;
        int i;
        if (list == null || list.size() <= 0) {
            return 0;
        }
        int size = list.size();
        "saveFriendList, before getModifiedFriendlist, friendList.size()=" + (list == null ? null : Integer.valueOf(size));
        List<a> a2 = a(list);
        "saveFriendList, after getModifiedFriendlist, friendList.size()=" + (a2 == null ? null : Integer.valueOf(a2.size()));
        if (a2 == null || a2.size() <= 0) {
            return 0;
        }
        boolean z2 = false;
        if (size != this.e.size()) {
            z2 = true;
        }
        "friendList.size()=" + size + ", mNewFriendList.size()=" + this.e.size();
        boolean z3 = false;
        int i2 = 0;
        String str2 = null;
        for (a aVar : a2) {
            String f2 = aVar.f();
            String callerIDMinMatch = q.toCallerIDMinMatch(f2);
            String e2 = aVar.e();
            ContentValues contentValues = new ContentValues();
            if (z2 && this.e.contains(callerIDMinMatch)) {
                contentValues.put("expand_data2", "new");
            }
            contentValues.put("contact_type", (Integer) 1);
            contentValues.put("nick_name", aVar.c());
            contentValues.put("signature", aVar.d());
            contentValues.put("friend_timestamp", Long.valueOf(aVar.g()));
            if (!TextUtils.isEmpty(e2)) {
                contentValues.put("photo_timestamp", e2);
                contentValues.putNull("photo");
            }
            "saveFriendList item: u:" + aVar.c() + " m:" + f2 + " na:" + aVar.b() + " s:" + aVar.d();
            int update = this.b.update(n.f597a, contentValues, "sid='" + callerIDMinMatch + "'", null);
            if (update <= 0) {
                "update friend fail: mobile=" + f2 + " displayName=" + aVar.c();
                if (f2.startsWith("krobot")) {
                    contentValues.put("contact_id", (Integer) -1);
                    contentValues.put("pinyin_name", 0 + u.a(aVar.c()));
                    contentValues.put("search_name", u.b(aVar.c()));
                    contentValues.put("phone_number", f2);
                    contentValues.put("sid", f2);
                    int i3 = this.b.insert(n.f597a, contentValues) != null ? 1 : update;
                    z = z3;
                    i = i3;
                    str = str2;
                }
                str = str2;
                int i4 = update;
                z = z3;
                i = i4;
            } else {
                if ("new".equals(contentValues.get("expand_data2"))) {
                    str = aVar.c();
                    int i5 = update;
                    z = true;
                    i = i5;
                }
                str = str2;
                int i42 = update;
                z = z3;
                i = i42;
            }
            i2 = i + i2;
            str2 = str;
            z3 = z;
        }
        if (z3) {
            Intent intent = new Intent("com.snda.youni.action.ACTION_FRIEND_LIST_NEW");
            Cursor query = this.f.getContentResolver().query(n.f597a, new String[]{"_id"}, "expand_data2='new'", null, null);
            if (query != null && query.getCount() > 0) {
                intent.putExtra("friend_list_new_count", query.getCount());
                query.close();
            }
            intent.putExtra("friend_list_new_name", str2);
            this.f.sendBroadcast(intent);
        }
        if (i2 > 0) {
            "saveFriendList count=" + i2;
            Intent intent2 = new Intent("com.snda.youni.action.FRIEND_LIST_CHANGED");
            if (!this.c.isEmpty()) {
                intent2.putStringArrayListExtra("com.snda.youni.extra_icon_updated_contact_ids", this.c);
                "mContactIdsWhichIconChanged.size=" + this.c.size();
            }
            if (!this.d.isEmpty()) {
                intent2.putStringArrayListExtra("com.snda.youni.extra_icon_updated_sids", this.d);
                "mSIdsWhichIconChanged.size=" + this.d.size();
            }
            this.f.sendBroadcast(intent2);
            this.c.clear();
            this.d.clear();
        }
        return i2;
    }

    public final void a(Message message) {
        if (this.f614a == null) {
            this.f614a = new Handler(getLooper(), this);
        }
        this.f614a.sendMessage(Message.obtain(message));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.snda.youni.services.ContactsService.a(com.snda.youni.services.ContactsService, java.lang.Integer):java.lang.Integer
     arg types: [com.snda.youni.services.ContactsService, int]
     candidates:
      com.snda.youni.services.ContactsService.a(com.snda.youni.services.ContactsService, com.snda.youni.services.a):com.snda.youni.services.a
      com.snda.youni.services.ContactsService.a(com.snda.youni.services.ContactsService, android.os.Message):void
      com.snda.youni.services.ContactsService.a(com.snda.youni.services.ContactsService, boolean):boolean
      com.snda.youni.services.ContactsService.a(com.snda.youni.services.ContactsService, java.lang.Integer):java.lang.Integer */
    public final boolean handleMessage(Message message) {
        int intValue;
        int intValue2;
        boolean z;
        boolean z2;
        switch (message.what) {
            case 1:
                synchronized (this.f.b) {
                    Integer unused = this.f.b = Integer.valueOf(this.f.b.intValue() + 1);
                    intValue2 = this.f.b.intValue();
                }
                try {
                    Thread.sleep(1200);
                } catch (InterruptedException e2) {
                    e2.printStackTrace();
                }
                synchronized (this.f.b) {
                    if (this.f.b.intValue() == intValue2) {
                        Integer unused2 = this.f.b = 0;
                        if (!PreferenceManager.getDefaultSharedPreferences(this.f).getBoolean("sid_11_bit_updated", false)) {
                            Cursor query = this.f.e.query(n.f597a, new String[]{"sid"}, null, null, "sid DESC limit 5");
                            if (query != null) {
                                query.moveToPosition(-1);
                                boolean z3 = true;
                                while (query.moveToNext()) {
                                    if (query.getString(0).length() == 11) {
                                        z3 = false;
                                    }
                                }
                                if (z3) {
                                    this.f.e.delete(n.f597a, "contact_id>0", null);
                                    this.f.a(this.f.a(), true, true);
                                    z2 = false;
                                } else {
                                    z2 = true;
                                }
                                query.close();
                                z = z2;
                            } else {
                                z = true;
                            }
                            ContactsService.m(this.f);
                        } else {
                            z = true;
                        }
                        if (z) {
                            this.f.a(this.f.a(), false, true);
                        }
                    }
                }
                break;
            case 2:
                List<com.snda.youni.modules.d.a> d2 = com.snda.youni.h.a.a.a().d();
                if (!d2.isEmpty()) {
                    "updateTimesContacted, list.size=" + d2.size();
                    HashMap hashMap = new HashMap();
                    StringBuilder sb = new StringBuilder();
                    sb.append("sid IN ");
                    sb.append('(');
                    boolean z4 = true;
                    for (com.snda.youni.modules.d.a aVar : d2) {
                        String callerIDMinMatch = q.toCallerIDMinMatch(aVar.a());
                        if (!TextUtils.isEmpty(callerIDMinMatch) && aVar.b() != 0) {
                            Integer num = (Integer) hashMap.get(callerIDMinMatch);
                            if (num == null) {
                                hashMap.put(callerIDMinMatch, Integer.valueOf(aVar.b()));
                                if (z4) {
                                    sb.append('\'');
                                    sb.append(callerIDMinMatch);
                                    sb.append('\'');
                                    z4 = false;
                                } else {
                                    sb.append(',');
                                    sb.append('\'');
                                    sb.append(callerIDMinMatch);
                                    sb.append('\'');
                                }
                            } else {
                                hashMap.put(callerIDMinMatch, Integer.valueOf(aVar.b() + num.intValue()));
                            }
                        }
                    }
                    sb.append(')');
                    "selection=" + sb.toString();
                    Cursor query2 = this.b.query(n.f597a, new String[]{"sid"}, sb.toString(), null, null);
                    if (query2 != null) {
                        d2.clear();
                        query2.moveToPosition(-1);
                        while (query2.moveToNext()) {
                            String string = query2.getString(0);
                            Integer num2 = (Integer) hashMap.get(string);
                            if (num2 != null) {
                                com.snda.youni.modules.d.a aVar2 = new com.snda.youni.modules.d.a();
                                aVar2.a(string);
                                aVar2.a(num2.intValue());
                                d2.add(aVar2);
                                "sid=" + string + ", count=" + num2;
                            }
                        }
                        query2.close();
                        if (!d2.isEmpty()) {
                            Collections.sort(d2, new c(this));
                            ContentValues contentValues = new ContentValues();
                            contentValues.put("times_contacted", (Integer) 0);
                            this.b.update(n.f597a, contentValues, null, null);
                            Iterator it = d2.iterator();
                            int i = 0;
                            int i2 = 0;
                            while (it.hasNext() && i2 < 6) {
                                com.snda.youni.modules.d.a aVar3 = (com.snda.youni.modules.d.a) it.next();
                                ContentValues contentValues2 = new ContentValues();
                                contentValues2.put("times_contacted", Integer.valueOf(aVar3.b()));
                                "updateTimesContacted, update sid = " + aVar3.a() + " count=" + aVar3.b();
                                i2++;
                                i = this.b.update(n.f597a, contentValues2, "sid=?", new String[]{aVar3.a()}) + i;
                            }
                            if (i > 0) {
                                this.f.sendBroadcast(new Intent("com.snda.youni.action.CONTACTS_CHANGED"));
                                break;
                            }
                        }
                    }
                }
                break;
            case 3:
                Object obj = message.obj;
                "handle MESSAGE_SAVE_FRIEND_LIST, data instanceof List=" + (obj instanceof List);
                if (obj instanceof List) {
                    b((List) obj);
                    break;
                }
                break;
            case 4:
                synchronized (this.f.c) {
                    Integer unused3 = this.f.c = Integer.valueOf(this.f.c.intValue() + 1);
                    intValue = this.f.c.intValue();
                }
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e3) {
                    e3.printStackTrace();
                }
                synchronized (this.f.c) {
                    if (this.f.c.intValue() == intValue) {
                        Integer unused4 = this.f.c = (Integer) 0;
                        this.f.c();
                    }
                }
                break;
        }
        return false;
    }
}
