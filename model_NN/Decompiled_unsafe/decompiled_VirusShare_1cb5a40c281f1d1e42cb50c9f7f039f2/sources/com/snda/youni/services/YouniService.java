package com.snda.youni.services;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.preference.PreferenceManager;
import com.snda.youni.b.ai;
import com.snda.youni.b.y;
import com.snda.youni.g.a;
import org.jivesoftware.smack.f.c;

public class YouniService extends DefaultService {
    private static e b = null;
    private static ai c = null;
    private static PendingIntent d = null;
    private static boolean f = false;
    private static int g = 1;
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public Integer f605a = 0;
    private g e = null;
    private ContentResolver h;

    public static void a() {
        "setYouniSend:" + true;
        f = true;
    }

    public static void a(int i) {
        g = i;
    }

    public static void a(Context context) {
        Cursor query;
        SharedPreferences defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        if (defaultSharedPreferences.getLong("all_rec_last_id", 0) <= 0 && (query = context.getContentResolver().query(Uri.parse("content://sms/inbox"), new String[]{"_id"}, null, null, "_id desc limit 1")) != null && query.moveToFirst()) {
            defaultSharedPreferences.edit().putLong("all_rec_last_id", query.getLong(0)).commit();
            query.close();
        }
    }

    static /* synthetic */ void a(YouniService youniService, Context context) {
        Cursor query;
        SharedPreferences defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        long j = defaultSharedPreferences.getLong("all_rec_last_id", 0);
        "rec_message last_id = " + j;
        if (j <= 0) {
            query = youniService.h.query(Uri.parse("content://sms/"), new String[]{"_id", "protocol", "address", "type"}, null, null, "_id desc limit 1");
        } else {
            query = youniService.h.query(Uri.parse("content://sms/inbox"), new String[]{"_id", "protocol", "address"}, " _id > " + j, null, "_id desc");
        }
        if (query != null && query.moveToFirst()) {
            if (j > 0 || !"1".equalsIgnoreCase(query.getString(query.getColumnIndex("type")))) {
                defaultSharedPreferences.edit().putLong("all_rec_last_id", query.getLong(0)).commit();
                do {
                    a.a(context, "sys_rec_all", null);
                    String string = query.getString(query.getColumnIndex("protocol"));
                    "rev one message 1 id = " + query.getLong(0) + ", protocol = " + string;
                    if (string != null && (string.equalsIgnoreCase("youni") || string.equalsIgnoreCase("youni_offline"))) {
                        a.a(context, "rec_youni", null);
                        String string2 = query.getString(query.getColumnIndex("address"));
                        "rev one message 2 id = " + query.getLong(0) + ", protocol = " + string + ", address = " + string2;
                        if (string2 != null && string2.contains("krobot_001")) {
                            a.a(context.getApplicationContext(), "sec_rev", null);
                        }
                    }
                } while (query.moveToNext());
                query.close();
            }
        }
    }

    public static ai b() {
        return c;
    }

    public IBinder onBind(Intent intent) {
        return b;
    }

    public void onCreate() {
        super.onCreate();
        c.a().a("ping", "urn:xmpp:ping", new y());
        synchronized ("LocalBinderLock") {
            if (b == null) {
                b = new e(this);
            }
        }
        synchronized ("pintent") {
            if (d == null) {
                Intent intent = new Intent();
                intent.setClass(getApplicationContext(), YouniService.class);
                d = PendingIntent.getService(getApplicationContext(), 0, intent, 0);
                ((AlarmManager) getApplicationContext().getSystemService("alarm")).setRepeating(0, 240000, 240000, d);
            }
        }
        synchronized ("network") {
            if (c == null) {
                c = new ai(getApplicationContext());
                com.snda.youni.h.a.a.a().a(c);
            }
        }
        this.h = getContentResolver();
        HandlerThread handlerThread = new HandlerThread("smsObserver");
        handlerThread.start();
        this.e = new g(this, new Handler(handlerThread.getLooper()));
        getContentResolver().registerContentObserver(Uri.parse("content://sms/"), true, this.e);
    }

    public void onDestroy() {
        super.onDestroy();
        if (d != null) {
            ((AlarmManager) getApplicationContext().getSystemService("alarm")).cancel(d);
        }
        c.c();
        c = null;
    }

    public void onStart(Intent intent, int i) {
        super.onStart(intent, i);
        String string = PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getString("self_phone_number", null);
        "mobile number is : " + string;
        if (string == null) {
            return;
        }
        if (c.b()) {
            c.a();
        } else if (((ConnectivityManager) getApplicationContext().getSystemService("connectivity")).getActiveNetworkInfo() == null) {
            c.a(0);
        } else {
            c.a(string);
        }
    }

    public boolean onUnbind(Intent intent) {
        return super.onUnbind(intent);
    }
}
