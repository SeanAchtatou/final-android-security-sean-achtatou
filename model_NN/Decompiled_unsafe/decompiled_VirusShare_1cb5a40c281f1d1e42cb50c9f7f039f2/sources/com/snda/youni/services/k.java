package com.snda.youni.services;

import android.content.Intent;
import com.snda.youni.f.a;
import com.snda.youni.f.c;
import com.snda.youni.f.d;

final class k implements a {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ ContactsService f616a;

    k(ContactsService contactsService) {
        this.f616a = contactsService;
    }

    public final void a(c cVar, d dVar) {
        com.snda.youni.c.d dVar2 = (com.snda.youni.c.d) cVar.c();
        int c = dVar.c();
        "result=" + c;
        switch (c) {
            case 1:
            case 3:
            case 8:
                Integer num = (Integer) this.f616a.f.get(dVar2);
                "delayTimeToResend=" + num;
                if (num != null && num.intValue() <= 5 && num.intValue() >= 2000) {
                    this.f616a.k.schedule(new a(this.f616a), (long) num.intValue());
                    this.f616a.f.put(dVar2, Integer.valueOf(num.intValue() * 2));
                    return;
                }
                return;
            case 2:
            case 5:
            case 6:
            default:
                com.snda.youni.c.c cVar2 = (com.snda.youni.c.c) dVar.b();
                if (cVar2 != null) {
                    switch (cVar2.b()) {
                        case 0:
                            "ContactsCommitRespMessage.CONTACTSCOMMIT_RESULT_SUCCESS, mPendingRequests.size=" + this.f616a.f.size();
                            this.f616a.f.remove(dVar2);
                            "after remove reqMessage, mPendingRequests.size=" + this.f616a.f.size();
                            try {
                                Thread.sleep(2400);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            this.f616a.e();
                            return;
                        case 1:
                            this.f616a.f.remove(dVar2);
                            return;
                        case 2:
                            this.f616a.f.remove(dVar2);
                            return;
                        default:
                            return;
                    }
                } else {
                    "Network maybe not connected, result=" + c;
                    return;
                }
            case 4:
                this.f616a.f.remove(dVar2);
                return;
            case 7:
                return;
            case 9:
                this.f616a.sendBroadcast(new Intent("com.snda.youni.action_NETWORK_EXCEPTION"));
                return;
        }
    }

    public final void a(Exception exc, String str) {
    }
}
