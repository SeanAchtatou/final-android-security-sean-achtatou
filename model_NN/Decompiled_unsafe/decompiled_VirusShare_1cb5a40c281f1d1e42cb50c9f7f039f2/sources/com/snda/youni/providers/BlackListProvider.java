package com.snda.youni.providers;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.text.TextUtils;
import java.util.HashMap;

public class BlackListProvider extends ContentProvider {
    private static final HashMap b;
    private static final UriMatcher c;
    private static /* synthetic */ boolean d = (!BlackListProvider.class.desiredAssertionStatus());

    /* renamed from: a  reason: collision with root package name */
    private d f589a;

    static {
        UriMatcher uriMatcher = new UriMatcher(-1);
        c = uriMatcher;
        uriMatcher.addURI("com.snda.youni.providers.BlackList", "blacklist", 1);
        c.addURI("com.snda.youni.providers.BlackList", "blacklist/#", 2);
        HashMap hashMap = new HashMap();
        b = hashMap;
        hashMap.put("_id", "_id");
        b.put("blacker_rid", "blacker_rid");
        b.put("blacker_name", "blacker_name");
        b.put("blacker_phone", "blacker_phone");
        b.put("blacker_sid", "blacker_sid");
    }

    public int delete(Uri uri, String str, String[] strArr) {
        int delete;
        SQLiteDatabase writableDatabase = this.f589a.getWritableDatabase();
        switch (c.match(uri)) {
            case 1:
                delete = writableDatabase.delete("blacklist", str, strArr);
                break;
            case 2:
                delete = writableDatabase.delete("blacklist", "_id=" + uri.getPathSegments().get(1) + (!TextUtils.isEmpty(str) ? "AND (" + str + ")" : ""), strArr);
                break;
            default:
                throw new IllegalArgumentException("Unknown Uri: " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return delete;
    }

    public String getType(Uri uri) {
        switch (c.match(uri)) {
            case 1:
                return "vnd.android.cursor.dir/vnd.youni.BlackList";
            case 2:
                return "vnd.android.cursor.item/vnd.youni.BlackList";
            default:
                throw new IllegalArgumentException("Unknown Uri: " + uri);
        }
    }

    public Uri insert(Uri uri, ContentValues contentValues) {
        SQLiteDatabase writableDatabase = this.f589a.getWritableDatabase();
        ContentValues contentValues2 = contentValues == null ? new ContentValues() : contentValues;
        if (c.match(uri) == 1) {
            long insert = writableDatabase.insert("blacklist", "blacker_name", contentValues2);
            Uri withAppendedId = ContentUris.withAppendedId(c.f593a, insert);
            if (insert <= 0) {
                throw new SQLException("Failed to insert row into: " + uri);
            } else if (d || uri != null) {
                getContext().getContentResolver().notifyChange(uri, null);
                return withAppendedId;
            } else {
                throw new AssertionError();
            }
        } else {
            throw new IllegalArgumentException("Unknown Uri: " + uri);
        }
    }

    public boolean onCreate() {
        this.f589a = new d(getContext());
        return true;
    }

    /*  JADX ERROR: Method load error
        jadx.core.utils.exceptions.DecodeException: Load method exception: Method info already added: android.database.sqlite.SQLiteQueryBuilder.<init>():void in method: com.snda.youni.providers.BlackListProvider.query(android.net.Uri, java.lang.String[], java.lang.String, java.lang.String[], java.lang.String):android.database.Cursor, dex: classes.dex
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:154)
        	at jadx.core.dex.nodes.ClassNode.load(ClassNode.java:306)
        	at jadx.core.ProcessClass.process(ProcessClass.java:36)
        	at jadx.core.ProcessClass.generateCode(ProcessClass.java:58)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:297)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:276)
        Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Method info already added: android.database.sqlite.SQLiteQueryBuilder.<init>():void
        	at jadx.core.dex.info.InfoStorage.putMethod(InfoStorage.java:42)
        	at jadx.core.dex.info.MethodInfo.fromDex(MethodInfo.java:50)
        	at jadx.core.dex.instructions.InsnDecoder.invoke(InsnDecoder.java:678)
        	at jadx.core.dex.instructions.InsnDecoder.decode(InsnDecoder.java:534)
        	at jadx.core.dex.instructions.InsnDecoder.process(InsnDecoder.java:78)
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:139)
        	... 5 more
        */
    public android.database.Cursor query(android.net.Uri r1, java.lang.String[] r2, java.lang.String r3, java.lang.String[] r4, java.lang.String r5) {
        /*
            r8 = this;
            r5 = 0
            android.database.sqlite.SQLiteQueryBuilder r0 = new android.database.sqlite.SQLiteQueryBuilder
            r0.<init>()
            android.content.UriMatcher r1 = com.snda.youni.providers.BlackListProvider.c
            int r1 = r1.match(r9)
            switch(r1) {
                case 1: goto L_0x0069;
                case 2: goto L_0x0028;
                default: goto L_0x000f;
            }
        L_0x000f:
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Unknown Uri: "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r9)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x0028:
            java.lang.String r1 = "blacklist"
            r0.setTables(r1)
            java.util.HashMap r1 = com.snda.youni.providers.BlackListProvider.b
            r0.setProjectionMap(r1)
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "_id="
            java.lang.StringBuilder r2 = r1.append(r2)
            java.util.List r1 = r9.getPathSegments()
            r3 = 1
            java.lang.Object r1 = r1.get(r3)
            java.lang.String r1 = (java.lang.String) r1
            java.lang.StringBuilder r1 = r2.append(r1)
            java.lang.String r1 = r1.toString()
            r0.appendWhere(r1)
        L_0x0053:
            boolean r1 = android.text.TextUtils.isEmpty(r13)
            if (r1 == 0) goto L_0x0074
            r7 = r5
        L_0x005a:
            com.snda.youni.providers.d r1 = r8.f589a
            android.database.sqlite.SQLiteDatabase r1 = r1.getReadableDatabase()
            r2 = r10
            r3 = r11
            r4 = r12
            r6 = r5
            android.database.Cursor r0 = r0.query(r1, r2, r3, r4, r5, r6, r7)
            return r0
        L_0x0069:
            java.lang.String r1 = "blacklist"
            r0.setTables(r1)
            java.util.HashMap r1 = com.snda.youni.providers.BlackListProvider.b
            r0.setProjectionMap(r1)
            goto L_0x0053
        L_0x0074:
            r7 = r13
            goto L_0x005a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.snda.youni.providers.BlackListProvider.query(android.net.Uri, java.lang.String[], java.lang.String, java.lang.String[], java.lang.String):android.database.Cursor");
    }

    /*  JADX ERROR: Method load error
        jadx.core.utils.exceptions.DecodeException: Load method exception: Method info already added: android.database.sqlite.SQLiteDatabase.update(java.lang.String, android.content.ContentValues, java.lang.String, java.lang.String[]):int in method: com.snda.youni.providers.BlackListProvider.update(android.net.Uri, android.content.ContentValues, java.lang.String, java.lang.String[]):int, dex: classes.dex
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:154)
        	at jadx.core.dex.nodes.ClassNode.load(ClassNode.java:306)
        	at jadx.core.ProcessClass.process(ProcessClass.java:36)
        	at jadx.core.ProcessClass.generateCode(ProcessClass.java:58)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:297)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:276)
        Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Method info already added: android.database.sqlite.SQLiteDatabase.update(java.lang.String, android.content.ContentValues, java.lang.String, java.lang.String[]):int
        	at jadx.core.dex.info.InfoStorage.putMethod(InfoStorage.java:42)
        	at jadx.core.dex.info.MethodInfo.fromDex(MethodInfo.java:50)
        	at jadx.core.dex.instructions.InsnDecoder.invoke(InsnDecoder.java:678)
        	at jadx.core.dex.instructions.InsnDecoder.decode(InsnDecoder.java:540)
        	at jadx.core.dex.instructions.InsnDecoder.process(InsnDecoder.java:78)
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:139)
        	... 5 more
        */
    public int update(android.net.Uri r1, android.content.ContentValues r2, java.lang.String r3, java.lang.String[] r4) {
        /*
            r4 = this;
            com.snda.youni.providers.d r0 = r4.f589a
            android.database.sqlite.SQLiteDatabase r1 = r0.getWritableDatabase()
            android.content.UriMatcher r0 = com.snda.youni.providers.BlackListProvider.c
            int r0 = r0.match(r5)
            switch(r0) {
                case 1: goto L_0x0028;
                case 2: goto L_0x003b;
                default: goto L_0x000f;
            }
        L_0x000f:
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Unknown Uri: "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r5)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x0028:
            java.lang.String r0 = "blacklist"
            int r0 = r1.update(r0, r6, r7, r8)
        L_0x002e:
            android.content.Context r1 = r4.getContext()
            android.content.ContentResolver r1 = r1.getContentResolver()
            r2 = 0
            r1.notifyChange(r5, r2)
            return r0
        L_0x003b:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r2 = "_id="
            java.lang.StringBuilder r2 = r0.append(r2)
            java.util.List r0 = r5.getPathSegments()
            r3 = 1
            java.lang.Object r0 = r0.get(r3)
            java.lang.String r0 = (java.lang.String) r0
            java.lang.StringBuilder r0 = r2.append(r0)
            boolean r2 = android.text.TextUtils.isEmpty(r7)
            if (r2 != 0) goto L_0x0083
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = " AND ("
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r2 = r2.append(r7)
            java.lang.String r3 = ")"
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
        L_0x0074:
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.String r0 = r0.toString()
            java.lang.String r2 = "blacklist"
            int r0 = r1.update(r2, r6, r0, r8)
            goto L_0x002e
        L_0x0083:
            java.lang.String r2 = ""
            goto L_0x0074
        */
        throw new UnsupportedOperationException("Method not decompiled: com.snda.youni.providers.BlackListProvider.update(android.net.Uri, android.content.ContentValues, java.lang.String, java.lang.String[]):int");
    }
}
