package com.snda.youni.providers;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;
import java.util.HashMap;

public class LocalStorageProvider extends ContentProvider {
    private static final HashMap b;
    private static final UriMatcher c;
    private static /* synthetic */ boolean d = (!LocalStorageProvider.class.desiredAssertionStatus());

    /* renamed from: a  reason: collision with root package name */
    private m f590a;

    static {
        UriMatcher uriMatcher = new UriMatcher(-1);
        c = uriMatcher;
        uriMatcher.addURI("com.snda.youni.providers.DataStructs", "message", 1);
        c.addURI("com.snda.youni.providers.DataStructs", "message/#", 2);
        HashMap hashMap = new HashMap();
        b = hashMap;
        hashMap.put("_id", "_id");
        b.put("message_id", "message_id");
        b.put("message_body", "message_body");
        b.put("message_box", "message_box");
        b.put("message_receiver", "message_receiver");
        b.put("message_subject", "message_subject");
        b.put("message_type", "message_type");
        b.put("message_date", "message_date");
        b.put("contactid", "contactid");
    }

    public int delete(Uri uri, String str, String[] strArr) {
        SQLiteDatabase writableDatabase = this.f590a.getWritableDatabase();
        switch (c.match(uri)) {
            case 2:
                break;
            default:
                throw new IllegalArgumentException("Unknown Uri: " + uri);
        }
        int delete = writableDatabase.delete("message", "_id=" + uri.getPathSegments().get(1) + (!TextUtils.isEmpty(str) ? "AND (" + str + ")" : ""), strArr);
        getContext().getContentResolver().notifyChange(uri, null);
        return delete;
    }

    public String getType(Uri uri) {
        switch (c.match(uri)) {
            case 1:
                return "vnd.android.cursor.dir/vnd.youni.LocalStorage";
            case 2:
                return "vnd.android.cursor.item/vnd.youni.LocalStorage";
            default:
                throw new IllegalArgumentException("Unknown Uri: " + uri);
        }
    }

    public Uri insert(Uri uri, ContentValues contentValues) {
        SQLiteDatabase writableDatabase = this.f590a.getWritableDatabase();
        ContentValues contentValues2 = contentValues == null ? new ContentValues() : contentValues;
        if (c.match(uri) == 1) {
            long insert = writableDatabase.insert("message", "message_date", contentValues2);
            Uri withAppendedId = ContentUris.withAppendedId(f.f594a, insert);
            if (insert <= 0) {
                throw new SQLException("Failed to insert row into: " + uri);
            } else if (d || uri != null) {
                getContext().getContentResolver().notifyChange(uri, null);
                return withAppendedId;
            } else {
                throw new AssertionError();
            }
        } else {
            throw new IllegalArgumentException("Unknown Uri: " + uri);
        }
    }

    public boolean onCreate() {
        this.f590a = new m(getContext());
        return true;
    }

    public Cursor query(Uri uri, String[] strArr, String str, String[] strArr2, String str2) {
        SQLiteQueryBuilder sQLiteQueryBuilder = new SQLiteQueryBuilder();
        switch (c.match(uri)) {
            case 1:
                sQLiteQueryBuilder.setTables("message");
                sQLiteQueryBuilder.setProjectionMap(b);
                break;
            case 2:
                sQLiteQueryBuilder.setTables("message");
                sQLiteQueryBuilder.setProjectionMap(b);
                sQLiteQueryBuilder.appendWhere("_id=" + uri.getPathSegments().get(1));
                break;
            default:
                throw new IllegalArgumentException("Unknown Uri: " + uri);
        }
        TextUtils.isEmpty(str2);
        return sQLiteQueryBuilder.query(this.f590a.getReadableDatabase(), strArr, str, strArr2, null, null, str2);
    }

    public int update(Uri uri, ContentValues contentValues, String str, String[] strArr) {
        int update;
        SQLiteDatabase writableDatabase = this.f590a.getWritableDatabase();
        switch (c.match(uri)) {
            case 1:
                update = writableDatabase.update("message", contentValues, str, strArr);
                break;
            case 2:
                update = writableDatabase.update("message", contentValues, "_id=" + uri.getPathSegments().get(1) + (!TextUtils.isEmpty(str) ? " AND (" + str + ")" : ""), strArr);
                break;
            default:
                throw new IllegalArgumentException("Unknown Uri: " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return update;
    }
}
