package com.snda.youni.providers;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;
import java.util.HashMap;

public class YouNiProvider extends ContentProvider {

    /* renamed from: a  reason: collision with root package name */
    private static final HashMap f591a;
    private static final HashMap b;
    private static final HashMap c;
    private static final HashMap d;
    private static final UriMatcher e;
    private static /* synthetic */ boolean g = (!YouNiProvider.class.desiredAssertionStatus());
    private h f;

    static {
        UriMatcher uriMatcher = new UriMatcher(-1);
        e = uriMatcher;
        uriMatcher.addURI("com.snda.youni.providers.DataStructs", "contacts", 1);
        e.addURI("com.snda.youni.providers.DataStructs", "contacts/#", 2);
        e.addURI("com.snda.youni.providers.DataStructs", "contacts/favorites", 3);
        e.addURI("com.snda.youni.providers.DataStructs", "secretary", 4);
        e.addURI("com.snda.youni.providers.DataStructs", "attachment", 7);
        e.addURI("com.snda.youni.providers.DataStructs", "attachment/#", 8);
        e.addURI("com.snda.youni.providers.DataStructs", "blacklist", 9);
        e.addURI("com.snda.youni.providers.DataStructs", "blacklist/#", 10);
        e.addURI("com.snda.youni.providers.DataStructs", "message", 11);
        e.addURI("com.snda.youni.providers.DataStructs", "message/#", 12);
        HashMap hashMap = new HashMap();
        f591a = hashMap;
        hashMap.put("_id", "_id");
        f591a.put("sid", "sid");
        f591a.put("phone_number", "phone_number");
        f591a.put("contact_id", "contact_id");
        f591a.put("contact_type", "contact_type");
        f591a.put("display_name", "display_name");
        f591a.put("search_name", "search_name");
        f591a.put("pinyin_name", "pinyin_name");
        f591a.put("nick_name", "nick_name");
        f591a.put("signature", "signature");
        f591a.put("emails", "emails");
        f591a.put("photo", "photo");
        f591a.put("photo_timestamp", "photo_timestamp");
        f591a.put("friend_timestamp", "friend_timestamp");
        f591a.put("last_contact_time", "last_contact_time");
        f591a.put("times_contacted", "times_contacted");
        f591a.put("expand_data1", "expand_data1");
        f591a.put("expand_data2", "expand_data2");
        f591a.put("expand_data3", "expand_data3");
        f591a.put("expand_data4", "expand_data4");
        f591a.put("expand_data5", "expand_data5");
        f591a.put("_id", "_id");
        f591a.put("body", "body");
        f591a.put("date", "date");
        f591a.put("read", "read");
        f591a.put("status", "status");
        f591a.put("type", "type");
        f591a.put("expand_data1", "expand_data1");
        f591a.put("expand_data2", "expand_data2");
        f591a.put("expand_data3", "expand_data3");
        HashMap hashMap2 = new HashMap();
        b = hashMap2;
        hashMap2.put("_id", "_id");
        b.put("message_id", "message_id");
        b.put("mine_type", "mine_type");
        b.put("filename", "filename");
        b.put("local_path", "local_path");
        b.put("server_url", "server_url");
        b.put("thumbnail_local_path", "thumbnail_local_path");
        b.put("thumbnail_short_url", "thumbnail_short_url");
        b.put("thumbnail_server_url", "thumbnail_server_url");
        b.put("file_size", "file_size");
        b.put("status", "status");
        b.put("box_type", "box_type");
        b.put("transfer_channel", "transfer_channel");
        b.put("play_time_duration", "play_time_duration");
        b.put("image_width", "image_width");
        b.put("image_height", "image_height");
        HashMap hashMap3 = new HashMap();
        c = hashMap3;
        hashMap3.put("_id", "_id");
        c.put("blacker_rid", "blacker_rid");
        c.put("blacker_name", "blacker_name");
        c.put("blacker_phone", "blacker_phone");
        c.put("blacker_sid", "blacker_sid");
        HashMap hashMap4 = new HashMap();
        d = hashMap4;
        hashMap4.put("_id", "_id");
        d.put("message_id", "message_id");
        d.put("message_body", "message_body");
        d.put("message_box", "message_box");
        d.put("message_receiver", "message_receiver");
        d.put("message_subject", "message_subject");
        d.put("message_type", "message_type");
        d.put("message_date", "message_date");
        d.put("contactid", "contactid");
    }

    public int bulkInsert(Uri uri, ContentValues[] contentValuesArr) {
        SQLiteDatabase writableDatabase = this.f.getWritableDatabase();
        writableDatabase.beginTransaction();
        try {
            for (ContentValues insert : contentValuesArr) {
                insert(uri, insert);
            }
            writableDatabase.setTransactionSuccessful();
            return contentValuesArr.length;
        } finally {
            writableDatabase.endTransaction();
        }
    }

    public int delete(Uri uri, String str, String[] strArr) {
        int delete;
        SQLiteDatabase writableDatabase = this.f.getWritableDatabase();
        switch (e.match(uri)) {
            case 1:
                delete = writableDatabase.delete("contacts", str, strArr);
                break;
            case 2:
                delete = writableDatabase.delete("contacts", "phone_number=" + uri.getPathSegments().get(1) + (!TextUtils.isEmpty(str) ? " AND (" + str + ')' : ""), strArr);
                break;
            case 3:
            case 5:
            case 6:
            case 7:
            case 9:
            case 11:
            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
            case 4:
                delete = writableDatabase.delete("secretary", str, strArr);
                break;
            case 8:
                delete = writableDatabase.delete("attachment", "_id=" + uri.getPathSegments().get(1) + (!TextUtils.isEmpty(str) ? "AND (" + str + ")" : ""), strArr);
                break;
            case 10:
                delete = writableDatabase.delete("blacklist", "_id=" + uri.getPathSegments().get(1) + (!TextUtils.isEmpty(str) ? "AND (" + str + ")" : ""), strArr);
                break;
            case 12:
                delete = writableDatabase.delete("message", "_id=" + uri.getPathSegments().get(1) + (!TextUtils.isEmpty(str) ? "AND (" + str + ")" : ""), strArr);
                break;
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return delete;
    }

    public String getType(Uri uri) {
        switch (e.match(uri)) {
            case 1:
                return "vnd.android.cursor.dir/vnd.snda.contacts";
            case 2:
                return "vnd.android.cursor.item/vnd.snda.contacts";
            case 3:
                return "vnd.android.cursor.favorites/vnd.snda.contact";
            case 4:
                return "vnd.android.cursor.dir/vnd.snda.secretary";
            case 5:
            case 6:
            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
            case 7:
                return "vnd.android.cursor.dir/vnd.snda.attachment";
            case 8:
                return "vnd.android.cursor.item/vnd.snda.attachment";
            case 9:
                return "vnd.android.cursor.dir/vnd.snda.BlackList";
            case 10:
                return "vnd.android.cursor.item/vnd.snda.BlackList";
            case 11:
                return "vnd.android.cursor.dir/vnd.snda.LocalStorage";
            case 12:
                return "vnd.android.cursor.item/vnd.snda.LocalStorage";
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public Uri insert(Uri uri, ContentValues contentValues) {
        ContentValues contentValues2 = contentValues != null ? new ContentValues(contentValues) : new ContentValues();
        SQLiteDatabase writableDatabase = this.f.getWritableDatabase();
        if (e.match(uri) == 1) {
            if (!contentValues2.containsKey("sid")) {
                throw new IllegalArgumentException("Contact SID is null ");
            } else if (!contentValues2.containsKey("phone_number")) {
                throw new IllegalArgumentException("Contact phone number is null ");
            } else {
                if (!contentValues2.containsKey("contact_type")) {
                    contentValues2.put("contact_type", (Integer) 0);
                }
                if (!contentValues2.containsKey("display_name")) {
                    String asString = contentValues2.getAsString("nick_name");
                    if (TextUtils.isEmpty(asString)) {
                        contentValues2.put("display_name", contentValues2.getAsString("phone_number"));
                    } else {
                        contentValues2.put("display_name", asString);
                    }
                }
                if (!contentValues2.containsKey("search_name")) {
                    contentValues2.put("search_name", contentValues2.getAsString("display_name"));
                }
                long insert = writableDatabase.insert("contacts", "sid", contentValues2);
                if (insert > 0) {
                    Uri withAppendedId = ContentUris.withAppendedId(n.f597a, insert);
                    getContext().getContentResolver().notifyChange(withAppendedId, null);
                    return withAppendedId;
                }
            }
        } else if (e.match(uri) == 7) {
            return ContentUris.withAppendedId(b.f592a, writableDatabase.insert("attachment", "filename", contentValues2));
        } else if (e.match(uri) == 9) {
            long insert2 = writableDatabase.insert("blacklist", "blacker_name", contentValues2);
            Uri withAppendedId2 = ContentUris.withAppendedId(l.f596a, insert2);
            if (insert2 > 0) {
                if (g || uri != null) {
                    getContext().getContentResolver().notifyChange(uri, null);
                } else {
                    throw new AssertionError();
                }
            }
            return withAppendedId2;
        } else if (e.match(uri) == 11) {
            return ContentUris.withAppendedId(f.f594a, writableDatabase.insert("message", "message_date", contentValues2));
        } else {
            long insert3 = writableDatabase.insert("secretary", "body", contentValues2);
            if (insert3 > 0) {
                Uri withAppendedId3 = ContentUris.withAppendedId(g.f595a, insert3);
                getContext().getContentResolver().notifyChange(withAppendedId3, null);
                return withAppendedId3;
            }
        }
        throw new SQLException("Failed to insert row into " + uri);
    }

    public boolean onCreate() {
        this.f = new h(getContext());
        return true;
    }

    public Cursor query(Uri uri, String[] strArr, String str, String[] strArr2, String str2) {
        String str3;
        String str4;
        SQLiteQueryBuilder sQLiteQueryBuilder = new SQLiteQueryBuilder();
        sQLiteQueryBuilder.setTables("contacts");
        switch (e.match(uri)) {
            case 1:
                sQLiteQueryBuilder.setProjectionMap(f591a);
                if (!TextUtils.isEmpty(str2)) {
                    str3 = null;
                    str4 = str2;
                    break;
                } else {
                    str3 = null;
                    str4 = "pinyin_name ASC";
                    break;
                }
            case 2:
                sQLiteQueryBuilder.setProjectionMap(f591a);
                sQLiteQueryBuilder.appendWhere("phone_number=" + uri.getPathSegments().get(1));
                if (!TextUtils.isEmpty(str2)) {
                    str3 = null;
                    str4 = str2;
                    break;
                } else {
                    str3 = null;
                    str4 = "pinyin_name ASC";
                    break;
                }
            case 3:
                sQLiteQueryBuilder.setProjectionMap(f591a);
                String valueOf = String.valueOf(6);
                if (!TextUtils.isEmpty(str2)) {
                    str3 = valueOf;
                    str4 = str2;
                    break;
                } else {
                    str3 = valueOf;
                    str4 = "pinyin_name ASC";
                    break;
                }
            case 4:
                sQLiteQueryBuilder.setTables("secretary");
                sQLiteQueryBuilder.setProjectionMap(f591a);
                String str5 = TextUtils.isEmpty(str2) ? "date asc" : str2;
                if (!TextUtils.isEmpty(str5)) {
                    str3 = null;
                    str4 = str5;
                    break;
                } else {
                    str3 = null;
                    str4 = "pinyin_name ASC";
                    break;
                }
            case 5:
            case 6:
            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
            case 7:
                sQLiteQueryBuilder.setTables("attachment");
                sQLiteQueryBuilder.setProjectionMap(b);
                str3 = null;
                str4 = str2;
                break;
            case 8:
                sQLiteQueryBuilder.setTables("attachment");
                sQLiteQueryBuilder.setProjectionMap(b);
                sQLiteQueryBuilder.appendWhere("_id=" + uri.getPathSegments().get(1));
                str3 = null;
                str4 = str2;
                break;
            case 9:
                sQLiteQueryBuilder.setTables("blacklist");
                sQLiteQueryBuilder.setProjectionMap(c);
                str3 = null;
                str4 = str2;
                break;
            case 10:
                sQLiteQueryBuilder.setTables("blacklist");
                sQLiteQueryBuilder.setProjectionMap(c);
                sQLiteQueryBuilder.appendWhere("_id=" + uri.getPathSegments().get(1));
                str3 = null;
                str4 = str2;
                break;
            case 11:
                sQLiteQueryBuilder.setTables("message");
                sQLiteQueryBuilder.setProjectionMap(d);
                str3 = null;
                str4 = str2;
                break;
            case 12:
                sQLiteQueryBuilder.setTables("message");
                sQLiteQueryBuilder.setProjectionMap(d);
                sQLiteQueryBuilder.appendWhere("_id=" + uri.getPathSegments().get(1));
                str3 = null;
                str4 = str2;
                break;
        }
        Cursor query = sQLiteQueryBuilder.query(this.f.getReadableDatabase(), strArr, str, strArr2, null, null, str4, str3);
        query.setNotificationUri(getContext().getContentResolver(), uri);
        return query;
    }

    public int update(Uri uri, ContentValues contentValues, String str, String[] strArr) {
        int update;
        SQLiteDatabase writableDatabase = this.f.getWritableDatabase();
        switch (e.match(uri)) {
            case 1:
                update = writableDatabase.update("contacts", contentValues, str, strArr);
                break;
            case 2:
                update = writableDatabase.update("contacts", contentValues, "phone_number=" + uri.getPathSegments().get(1) + (!TextUtils.isEmpty(str) ? " AND (" + str + ')' : ""), strArr);
                break;
            case 3:
            case 5:
            case 6:
            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
            case 4:
                update = writableDatabase.update("secretary", contentValues, str, strArr);
                break;
            case 7:
                update = writableDatabase.update("attachment", contentValues, str, strArr);
                break;
            case 8:
                update = writableDatabase.update("attachment", contentValues, "_id=" + uri.getPathSegments().get(1) + (!TextUtils.isEmpty(str) ? " AND (" + str + ")" : ""), strArr);
                break;
            case 9:
                update = writableDatabase.update("blacklist", contentValues, str, strArr);
                break;
            case 10:
                update = writableDatabase.update("blacklist", contentValues, "_id=" + uri.getPathSegments().get(1) + (!TextUtils.isEmpty(str) ? " AND (" + str + ")" : ""), strArr);
                break;
            case 11:
                update = writableDatabase.update("message", contentValues, str, strArr);
                break;
            case 12:
                update = writableDatabase.update("message", contentValues, "_id=" + uri.getPathSegments().get(1) + (!TextUtils.isEmpty(str) ? " AND (" + str + ")" : ""), strArr);
                break;
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return update;
    }
}
