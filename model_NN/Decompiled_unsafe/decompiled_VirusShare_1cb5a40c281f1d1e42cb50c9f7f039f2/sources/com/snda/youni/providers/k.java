package com.snda.youni.providers;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

final class k extends SQLiteOpenHelper {
    public k(Context context) {
        super(context, "youni.db", (SQLiteDatabase.CursorFactory) null, 3);
    }

    public final void onCreate(SQLiteDatabase sQLiteDatabase) {
        sQLiteDatabase.execSQL("Create TABLE blacklist (_id INTEGER PRIMARY KEY, blacker_rid TEXT, blacker_name TEXT, blacker_phone TEXT, blacker_sid TEXT);");
        sQLiteDatabase.execSQL("Create TABLE attachment (_id INTEGER PRIMARY KEY, message_id LONG, mine_type TEXT, filename TEXT, local_path TEXT, server_url TEXT, thumbnail_local_path TEXT, thumbnail_short_url TEXT, thumbnail_server_url TEXT, file_size INTEGER, status INTEGER, box_type INTEGER, transfer_channel INTEGER, image_width INTEGER, image_height INTEGER, play_time_duration INTEGER);");
        sQLiteDatabase.execSQL("Create TABLE message (_id INTEGER PRIMARY KEY, message_id LONG, message_body TEXT, message_subject TEXT, message_box INTEGER, message_type TEXT, message_receiver TEXT, contactid INTEGER, message_date LONG);");
    }

    public final void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS attachment");
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS message");
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS blacklist");
        onCreate(sQLiteDatabase);
    }
}
