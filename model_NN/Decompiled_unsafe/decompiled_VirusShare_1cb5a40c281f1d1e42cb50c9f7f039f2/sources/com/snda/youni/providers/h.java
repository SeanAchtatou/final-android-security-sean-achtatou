package com.snda.youni.providers;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

final class h extends SQLiteOpenHelper {
    h(Context context) {
        super(context, "youni.db", (SQLiteDatabase.CursorFactory) null, 4);
    }

    public final void onCreate(SQLiteDatabase sQLiteDatabase) {
        sQLiteDatabase.execSQL("CREATE TABLE contacts (_id INTEGER PRIMARY KEY,sid TEXT,phone_number TEXT,contact_id INTEGER,contact_type INTEGER,display_name TEXT,pinyin_name TEXT,search_name TEXT,nick_name TEXT,signature TEXT,emails TEXT,photo TEXT,photo_timestamp TEXT,friend_timestamp INTEGER,last_contact_time INTEGER,times_contacted INTEGER,expand_data1 INTEGER,expand_data2 INTEGER,expand_data3 TEXT,expand_data4 TEXT,expand_data5 TEXT);");
        sQLiteDatabase.execSQL("CREATE TABLE secretary (_id INTEGER PRIMARY KEY,body TEXT,status TEXT,type TEXT,read INTEGER,date LONG,expand_data1 TEXT,expand_data2 TEXT,expand_data3 TEXT );");
        sQLiteDatabase.execSQL("Create TABLE attachment (_id INTEGER PRIMARY KEY, message_id LONG, mine_type TEXT, filename TEXT, local_path TEXT, server_url TEXT, thumbnail_local_path TEXT, thumbnail_short_url TEXT, thumbnail_server_url TEXT, file_size INTEGER, status INTEGER, box_type INTEGER, transfer_channel INTEGER, image_width INTEGER, image_height INTEGER, play_time_duration INTEGER);");
        sQLiteDatabase.execSQL("Create TABLE message (_id INTEGER PRIMARY KEY, message_id LONG, message_body TEXT, message_subject TEXT, message_box INTEGER, message_type TEXT, message_receiver TEXT, contactid INTEGER, message_date LONG);");
        sQLiteDatabase.execSQL("Create TABLE blacklist (_id INTEGER PRIMARY KEY, blacker_rid TEXT, blacker_name TEXT, blacker_phone TEXT, blacker_sid TEXT);");
    }

    public final void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        "Upgrading database from version " + i + " to " + i2 + ", which will change old data";
        switch (i) {
            case 1:
                sQLiteDatabase.execSQL("DROP TABLE IF EXISTS attachment");
                sQLiteDatabase.execSQL("DROP TABLE IF EXISTS contacts");
                sQLiteDatabase.execSQL("DROP TABLE IF EXISTS secretary");
                onCreate(sQLiteDatabase);
                return;
            case 2:
                Cursor query = sQLiteDatabase.query("contacts", new String[]{"_id"}, "contact_id=-1", null, null, null, null);
                if (query != null && query.getCount() > 1) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("_id").append(" IN (");
                    query.moveToPosition(-1);
                    boolean z = true;
                    while (query.moveToNext() && !query.isLast()) {
                        if (z) {
                            sb.append(query.getInt(0));
                            z = false;
                        } else {
                            sb.append(',').append(query.getInt(0));
                        }
                    }
                    query.close();
                    sb.append(')');
                    "deleteWhere=" + sb.toString();
                    sQLiteDatabase.delete("contacts", sb.toString(), null);
                }
                sQLiteDatabase.execSQL("Create TABLE attachment (_id INTEGER PRIMARY KEY, message_id LONG, mine_type TEXT, filename TEXT, local_path TEXT, server_url TEXT, thumbnail_local_path TEXT, thumbnail_short_url TEXT, thumbnail_server_url TEXT, file_size INTEGER, status INTEGER, box_type INTEGER, transfer_channel INTEGER, image_width INTEGER, image_height INTEGER, play_time_duration INTEGER);");
                sQLiteDatabase.execSQL("Create TABLE message (_id INTEGER PRIMARY KEY, message_id LONG, message_body TEXT, message_subject TEXT, message_box INTEGER, message_type TEXT, message_receiver TEXT, contactid INTEGER, message_date LONG);");
                sQLiteDatabase.execSQL("Create TABLE blacklist (_id INTEGER PRIMARY KEY, blacker_rid TEXT, blacker_name TEXT, blacker_phone TEXT, blacker_sid TEXT);");
                return;
            case 3:
                sQLiteDatabase.execSQL("DROP TABLE IF EXISTS attachment");
                sQLiteDatabase.execSQL("Create TABLE attachment (_id INTEGER PRIMARY KEY, message_id LONG, mine_type TEXT, filename TEXT, local_path TEXT, server_url TEXT, thumbnail_local_path TEXT, thumbnail_short_url TEXT, thumbnail_server_url TEXT, file_size INTEGER, status INTEGER, box_type INTEGER, transfer_channel INTEGER, image_width INTEGER, image_height INTEGER, play_time_duration INTEGER);");
                sQLiteDatabase.execSQL("Create TABLE message (_id INTEGER PRIMARY KEY, message_id LONG, message_body TEXT, message_subject TEXT, message_box INTEGER, message_type TEXT, message_receiver TEXT, contactid INTEGER, message_date LONG);");
                sQLiteDatabase.execSQL("Create TABLE blacklist (_id INTEGER PRIMARY KEY, blacker_rid TEXT, blacker_name TEXT, blacker_phone TEXT, blacker_sid TEXT);");
                return;
            default:
                Log.w("YouNiProvider", "unUpgrading database from version " + i + " to " + i2);
                return;
        }
    }
}
