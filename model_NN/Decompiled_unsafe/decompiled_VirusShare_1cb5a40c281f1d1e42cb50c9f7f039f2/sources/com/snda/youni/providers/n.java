package com.snda.youni.providers;

import android.net.Uri;
import android.provider.BaseColumns;

public final class n implements BaseColumns {

    /* renamed from: a  reason: collision with root package name */
    public static final Uri f597a;
    public static final Uri b;

    static {
        Uri parse = Uri.parse("content://com.snda.youni.providers.DataStructs/contacts");
        f597a = parse;
        b = Uri.withAppendedPath(parse, "favorites");
    }

    private n() {
    }
}
