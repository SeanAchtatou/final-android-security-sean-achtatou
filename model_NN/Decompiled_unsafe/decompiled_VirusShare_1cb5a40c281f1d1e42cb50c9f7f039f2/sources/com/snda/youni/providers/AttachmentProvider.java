package com.snda.youni.providers;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;
import java.util.HashMap;

public class AttachmentProvider extends ContentProvider {
    private static final HashMap b;
    private static final UriMatcher c;
    private static /* synthetic */ boolean d = (!AttachmentProvider.class.desiredAssertionStatus());

    /* renamed from: a  reason: collision with root package name */
    private k f588a;

    static {
        UriMatcher uriMatcher = new UriMatcher(-1);
        c = uriMatcher;
        uriMatcher.addURI("com.snda.youni.providers.DataStructs", "attachment", 1);
        c.addURI("com.snda.youni.providers.DataStructs", "attachment/#", 2);
        HashMap hashMap = new HashMap();
        b = hashMap;
        hashMap.put("_id", "_id");
        b.put("message_id", "message_id");
        b.put("mine_type", "mine_type");
        b.put("filename", "filename");
        b.put("local_path", "local_path");
        b.put("server_url", "server_url");
        b.put("thumbnail_local_path", "thumbnail_local_path");
        b.put("thumbnail_short_url", "thumbnail_short_url");
        b.put("thumbnail_server_url", "thumbnail_server_url");
        b.put("file_size", "file_size");
        b.put("status", "status");
        b.put("box_type", "box_type");
        b.put("transfer_channel", "transfer_channel");
        b.put("play_time_duration", "play_time_duration");
        b.put("image_width", "image_width");
        b.put("image_height", "image_height");
    }

    public int delete(Uri uri, String str, String[] strArr) {
        SQLiteDatabase writableDatabase = this.f588a.getWritableDatabase();
        switch (c.match(uri)) {
            case 2:
                break;
            default:
                throw new IllegalArgumentException("Unknown Uri: " + uri);
        }
        int delete = writableDatabase.delete("attachment", "_id=" + uri.getPathSegments().get(1) + (!TextUtils.isEmpty(str) ? "AND (" + str + ")" : ""), strArr);
        getContext().getContentResolver().notifyChange(uri, null);
        return delete;
    }

    public String getType(Uri uri) {
        switch (c.match(uri)) {
            case 1:
                return "vnd.android.cursor.dir/vnd.youni.attachment";
            case 2:
                return "vnd.android.cursor.item/vnd.youni.attachment";
            default:
                throw new IllegalArgumentException("Unknown Uri: " + uri);
        }
    }

    public Uri insert(Uri uri, ContentValues contentValues) {
        SQLiteDatabase writableDatabase = this.f588a.getWritableDatabase();
        ContentValues contentValues2 = contentValues == null ? new ContentValues() : contentValues;
        if (c.match(uri) == 1) {
            long insert = writableDatabase.insert("attachment", "filename", contentValues2);
            Uri withAppendedId = ContentUris.withAppendedId(b.f592a, insert);
            if (insert <= 0) {
                throw new SQLException("Failed to insert row into: " + uri);
            } else if (d || uri != null) {
                getContext().getContentResolver().notifyChange(uri, null);
                return withAppendedId;
            } else {
                throw new AssertionError();
            }
        } else {
            throw new IllegalArgumentException("Unknown Uri: " + uri);
        }
    }

    public boolean onCreate() {
        this.f588a = new k(getContext());
        return true;
    }

    public Cursor query(Uri uri, String[] strArr, String str, String[] strArr2, String str2) {
        SQLiteQueryBuilder sQLiteQueryBuilder = new SQLiteQueryBuilder();
        switch (c.match(uri)) {
            case 1:
                sQLiteQueryBuilder.setTables("attachment");
                sQLiteQueryBuilder.setProjectionMap(b);
                break;
            case 2:
                sQLiteQueryBuilder.setTables("attachment");
                sQLiteQueryBuilder.setProjectionMap(b);
                sQLiteQueryBuilder.appendWhere("_id=" + uri.getPathSegments().get(1));
                break;
            default:
                throw new IllegalArgumentException("Unknown Uri: " + uri);
        }
        TextUtils.isEmpty(str2);
        return sQLiteQueryBuilder.query(this.f588a.getReadableDatabase(), strArr, str, strArr2, null, null, str2);
    }

    public int update(Uri uri, ContentValues contentValues, String str, String[] strArr) {
        int update;
        SQLiteDatabase writableDatabase = this.f588a.getWritableDatabase();
        switch (c.match(uri)) {
            case 1:
                update = writableDatabase.update("attachment", contentValues, str, strArr);
                break;
            case 2:
                update = writableDatabase.update("attachment", contentValues, "_id=" + uri.getPathSegments().get(1) + (!TextUtils.isEmpty(str) ? " AND (" + str + ")" : ""), strArr);
                break;
            default:
                throw new IllegalArgumentException("Unknown Uri: " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return update;
    }
}
