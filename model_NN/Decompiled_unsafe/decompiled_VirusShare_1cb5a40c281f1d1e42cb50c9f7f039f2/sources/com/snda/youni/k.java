package com.snda.youni;

import android.content.Intent;
import android.view.View;
import android.widget.Toast;
import com.snda.youni.modules.m;

final class k implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ YouNi f433a;

    k(YouNi youNi) {
        this.f433a = youNi;
    }

    public final void onClick(View view) {
        if (((m) this.f433a.l.c().getAdapter()).b().size() == 0) {
            Toast.makeText(this.f433a, (int) C0000R.string.no_select_notification, 0).show();
            return;
        }
        Intent intent = new Intent();
        com.snda.youni.modules.a.k kVar = new com.snda.youni.modules.a.k();
        kVar.b = this.f433a.getIntent().getStringExtra("message_body");
        kVar.g = ((m) this.f433a.l.c().getAdapter()).d();
        intent.putExtra("item", kVar);
        this.f433a.setResult(100, intent);
        this.f433a.finish();
    }
}
