package com.snda.youni;

import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.widget.Toast;
import com.snda.youni.modules.settings.SettingsItemView;

final class f extends Handler {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ YouNi f404a;

    f(YouNi youNi) {
        this.f404a = youNi;
    }

    public final void handleMessage(Message message) {
        boolean z;
        Intent intent;
        switch (message.what) {
            case 0:
                SettingsItemView settingsItemView = (SettingsItemView) this.f404a.findViewById(C0000R.id.update);
                String string = this.f404a.e().getString("update_versionname", "");
                settingsItemView.c();
                settingsItemView.a(this.f404a.getString(C0000R.string.update_prefix));
                settingsItemView.b(string);
                settingsItemView.a();
                return;
            case 1:
                this.f404a.findViewById(C0000R.id.notify_update).setVisibility(8);
                return;
            case 2:
                this.f404a.findViewById(C0000R.id.notify_update).setVisibility(0);
                return;
            case 3:
                this.f404a.l();
                this.f404a.findViewById(C0000R.id.new_youni_contacts).setVisibility(0);
                if (this.f404a.n.getCheckedRadioButtonId() == C0000R.id.btn_contacts) {
                    if (this.f404a.l.g()) {
                        this.f404a.l.b = true;
                        Toast.makeText(this.f404a, (int) C0000R.string.new_youni_contact, 1).show();
                        z = false;
                        if (z && (intent = (Intent) message.obj) != null) {
                            ((AppContext) this.f404a.getApplication()).a(intent.getIntExtra("friend_list_new_count", 1), intent.getStringExtra("friend_list_new_name"), true);
                            return;
                        }
                        return;
                    }
                    this.f404a.p.setOnClickListener(new c(this));
                }
                z = true;
                if (z) {
                    return;
                }
                return;
            default:
                return;
        }
    }
}
