package com.snda.youni.h.a.b;

import android.content.Intent;
import com.snda.youni.AppContext;
import com.snda.youni.attachment.b.b;
import com.snda.youni.b.ap;
import com.snda.youni.b.m;
import com.snda.youni.b.x;
import com.snda.youni.e.t;

final class a implements ap {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ c f422a;

    a(c cVar) {
        this.f422a = cVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.snda.youni.AppContext.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.snda.youni.AppContext.a(com.snda.youni.o, boolean):void
      com.snda.youni.AppContext.a(java.lang.String, long):void
      com.snda.youni.AppContext.a(java.lang.String, boolean):void */
    public final int a(m mVar) {
        b bVar;
        "receive youni message:" + mVar.e() + " body:" + mVar.i() + " from:" + mVar.g();
        com.snda.youni.modules.d.b bVar2 = new com.snda.youni.modules.d.b();
        if (mVar.a()) {
            String str = mVar.g() + "_" + mVar.e();
            com.snda.youni.modules.a.a a2 = new com.snda.youni.modules.a.b(this.f422a.b).a("krobot_001");
            "info is " + a2;
            if (a2 != null) {
                "phoneNumber is " + a2.e;
            }
            if (a2 == null || a2.e == null) {
                return 0;
            }
            bVar2.a(a2.e);
            bVar2.b(mVar.b());
            if (mVar.u() != null) {
                "message:" + mVar.e() + " " + bVar2.b();
                bVar2.a(Long.getLong(mVar.u()));
            } else {
                bVar2.a(Long.valueOf(System.currentTimeMillis()));
            }
            bVar2.e("youni");
            bVar2.b(false);
            bVar2.d("1");
            bVar2.g(str);
        } else if (mVar.c()) {
            com.snda.youni.modules.d.b b = this.f422a.b("sys_" + mVar.d());
            if (b != null) {
                bVar2 = b;
            }
            com.snda.youni.modules.a.a a3 = new com.snda.youni.modules.a.b(this.f422a.b).a("krobot_001");
            "info is " + a3;
            if (a3 != null) {
                "phoneNumber is " + a3.e;
                bVar2.a(a3.e);
            }
            bVar2.b(mVar.b());
        } else if (" ".equalsIgnoreCase(mVar.i())) {
            ((AppContext) this.f422a.b.getApplicationContext()).b(t.a(mVar.g()));
            ((AppContext) this.f422a.b.getApplicationContext()).a(t.a(mVar.g()), true);
            return 0;
        } else {
            String str2 = mVar.g() + "_" + mVar.e();
            "youniId:" + str2;
            if (this.f422a.f(str2)) {
                "getReceiveNumber is " + str2 + " is in preference." + " body:" + mVar.i();
                return 0;
            }
            com.snda.youni.modules.d.b b2 = this.f422a.b(str2);
            if (b2 == null || b2.l() <= 0) {
                this.f422a.e(str2);
                String g = mVar.g();
                if (!(g == null || g.indexOf("@") == -1)) {
                    g = g.substring(0, g.indexOf("@"));
                }
                bVar2.a(g);
                bVar2.b(mVar.i());
                bVar2.c(mVar.h());
                bVar2.b(false);
                bVar2.g(str2);
                bVar2.i(mVar.j());
                bVar2.e("youni");
                bVar2.d("1");
                if (mVar.u() != null) {
                    "message:" + mVar.e() + " " + bVar2.b();
                    bVar2.a(Long.getLong(mVar.u()));
                } else {
                    bVar2.a(Long.valueOf(System.currentTimeMillis()));
                }
                if (mVar.k() != null && mVar.k().equals("img")) {
                    bVar = new b();
                    bVar.a("image/jpeg");
                    bVar.f(mVar.l());
                    bVar.g(mVar.n());
                    bVar.d(mVar.o());
                } else if (mVar.k() != null && mVar.k().equals("audio")) {
                    bVar = new b();
                    bVar.a("audio/amr");
                    bVar.f(mVar.l());
                    bVar.d(mVar.p());
                    if (mVar.m() != null) {
                        bVar.d(Integer.parseInt(mVar.m()));
                    } else {
                        bVar.d(0);
                    }
                }
                bVar2.a(bVar);
            } else {
                "receive youni message repeat,the old id is " + b2.l() + " body:" + b2.b();
                return 0;
            }
        }
        Intent intent = new Intent("com.snda.youni.receiver.YOU_NI");
        intent.putExtra("youni", bVar2);
        this.f422a.b.sendBroadcast(intent);
        "after sendBroadcast:" + mVar.e() + " " + bVar2.b();
        return 0;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.snda.youni.AppContext.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.snda.youni.AppContext.a(com.snda.youni.o, boolean):void
      com.snda.youni.AppContext.a(java.lang.String, long):void
      com.snda.youni.AppContext.a(java.lang.String, boolean):void */
    public final int a(x xVar) {
        "receive youni xiq:" + xVar.a() + " type:" + xVar.b();
        if (!"88".equalsIgnoreCase(xVar.b())) {
            return 0;
        }
        ((AppContext) this.f422a.b.getApplicationContext()).b(t.a(xVar.c()));
        ((AppContext) this.f422a.b.getApplicationContext()).a(t.a(xVar.c()), true);
        return 0;
    }
}
