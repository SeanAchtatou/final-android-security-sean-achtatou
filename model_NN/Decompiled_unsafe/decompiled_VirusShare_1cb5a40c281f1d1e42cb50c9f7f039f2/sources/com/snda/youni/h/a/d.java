package com.snda.youni.h.a;

import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import com.snda.youni.b.ai;
import com.snda.youni.modules.d.b;
import java.util.List;

public interface d {
    int a(long j);

    int a(long j, ContentValues contentValues);

    int a(long j, String str);

    int a(Uri uri, String str);

    int a(b bVar);

    int a(b bVar, String str);

    int a(String str, String str2, boolean z);

    int a(boolean z, int i);

    Cursor a(Uri uri, String[] strArr, String str, String[] strArr2, String str2);

    b a(String str);

    String a(String str, b bVar);

    List a();

    List a(Uri uri, String str, String str2);

    List a(boolean z);

    void a(Uri uri, long j);

    void a(ai aiVar);

    void a(ai aiVar, b bVar);

    boolean b(b bVar);

    int c(String str);

    int d(String str);
}
