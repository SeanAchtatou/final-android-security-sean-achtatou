package com.snda.youni.h.a.b;

import android.app.PendingIntent;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.telephony.SmsManager;
import com.snda.youni.b.ai;
import com.snda.youni.b.m;
import com.snda.youni.e.e;
import com.snda.youni.e.q;
import com.snda.youni.h.a.a.a;
import com.snda.youni.h.a.d;
import com.snda.youni.modules.a.l;
import com.snda.youni.modules.d.b;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public final class c implements d {

    /* renamed from: a  reason: collision with root package name */
    private ai f424a = null;
    /* access modifiers changed from: private */
    public Context b = null;

    public c(Context context) {
        this.b = context;
    }

    private static b a(Cursor cursor) {
        if (cursor == null || !cursor.moveToFirst()) {
            return null;
        }
        b b2 = b(cursor);
        cursor.close();
        return b2;
    }

    private static b b(Cursor cursor) {
        b bVar = new b();
        bVar.a(cursor.getString(cursor.getColumnIndex("address")));
        bVar.b(cursor.getString(cursor.getColumnIndex("body")));
        bVar.a(Long.valueOf(cursor.getLong(cursor.getColumnIndex("date"))));
        bVar.a(64 == cursor.getInt(cursor.getColumnIndex("status")));
        bVar.e("youni".equalsIgnoreCase(cursor.getString(cursor.getColumnIndex("protocol"))) ? "youni" : "sms");
        bVar.b(cursor.getInt(cursor.getColumnIndex("read")) == 1);
        bVar.c(cursor.getString(cursor.getColumnIndex("subject")));
        bVar.a(cursor.getLong(cursor.getColumnIndex("thread_id")));
        bVar.d(cursor.getString(cursor.getColumnIndex("type")));
        bVar.f(cursor.getString(cursor.getColumnIndex("person")));
        bVar.g(cursor.getString(cursor.getColumnIndex("service_center")));
        bVar.b(cursor.getLong(cursor.getColumnIndex("_id")));
        return bVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    private static ContentValues c(b bVar) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("read", bVar.g() ? 1 : 0);
        if (bVar.a() != null && !"".equalsIgnoreCase(bVar.a())) {
            contentValues.put("address", bVar.a());
        }
        if (bVar.m() != null) {
            contentValues.put("status", bVar.m());
        }
        if (bVar.d() != null) {
            contentValues.put("date", bVar.d());
        }
        if (bVar.c() != null) {
            contentValues.put("subject", bVar.c());
        }
        if (bVar.b() != null) {
            contentValues.put("body", bVar.b());
        }
        if (bVar.j() != null) {
            contentValues.put("person", bVar.j());
        }
        if (bVar.e()) {
            contentValues.put("status", (Integer) 64);
        }
        if (bVar.h() > 0) {
            contentValues.put("thread_id", Long.valueOf(bVar.h()));
        }
        if (bVar.k() != null) {
            contentValues.put("service_center", bVar.k());
        }
        if (bVar.i() != null) {
            contentValues.put("protocol", bVar.i());
        }
        if (bVar.f() != null) {
            contentValues.put("type", bVar.f());
        }
        return contentValues;
    }

    private static String g(String str) {
        return "youni".equalsIgnoreCase(str) ? "" + " and (protocol = 'youni' or protocol = 'youni_offline')" : "sms".equalsIgnoreCase(str) ? "" + " and (( protocol != 'youni' and protocol != 'youni_offline' ) or protocol is null) " : "youni_offline".equalsIgnoreCase(str) ? "" + " and protocol = 'youni_offline'" : "";
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public final int a(long j) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("read", (Integer) 1);
        return this.b.getContentResolver().update(Uri.parse("content://sms/" + j), contentValues, "_id=" + j, null);
    }

    public final int a(long j, ContentValues contentValues) {
        return this.b.getContentResolver().update(Uri.parse("content://sms/" + j), contentValues, null, null);
    }

    public final int a(long j, String str) {
        Cursor a2 = a(Uri.parse("content://sms/"), a.f420a, "thread_id = " + j + " " + ("" + g(str)), null, " date desc");
        if (a2 != null) {
            try {
                if (a2.moveToFirst()) {
                    "queryMessageCountByThreadId:" + j + " messageType:" + str + " count:" + a2.getCount();
                    int count = a2.getCount();
                    do {
                        String str2 = "";
                        for (int i = 0; i < a2.getColumnCount(); i++) {
                            str2 = str2 + ("name:" + a2.getColumnName(i) + "=" + a2.getString(i)) + " ";
                        }
                    } while (a2.moveToNext());
                    return count;
                }
            } finally {
                if (a2 != null) {
                    a2.close();
                }
            }
        }
        if (a2 != null) {
            a2.close();
        }
        return 0;
    }

    public final int a(Uri uri, String str) {
        String h;
        Context context = this.b;
        com.snda.youni.attachment.b.b b2 = com.snda.youni.attachment.b.a.b(context, str);
        if (b2 != null) {
            context.getContentResolver().delete(ContentUris.withAppendedId(com.snda.youni.providers.b.f592a, b2.a()), null, null);
            if (b2.g().equals("img")) {
                String h2 = b2.h();
                if (h2 != null) {
                    e.a(h2, com.snda.youni.attachment.e.h);
                    e.a(h2, com.snda.youni.attachment.e.i);
                }
            } else if (b2.g().equals("audio") && (h = b2.h()) != null) {
                e.a(h, com.snda.youni.attachment.e.j);
            }
        }
        try {
            return this.b.getContentResolver().delete(ContentUris.withAppendedId(uri, Long.parseLong(str)), null, null);
        } catch (NumberFormatException e) {
            e.printStackTrace();
            return 0;
        }
    }

    public final int a(b bVar) {
        ContentValues c = c(bVar);
        Uri parse = Uri.parse("content://sms/draft");
        String str = (bVar.a() == null || "".equalsIgnoreCase(bVar.a())) ? " type=3 and (address is null or address='') and thread_id is null" : " type=3 and address like '%" + bVar.a() + "%' ";
        int update = this.b.getContentResolver().update(parse, c, str, null);
        "updateDraft values:" + c.toString() + " where:" + str + " result:" + update;
        return update;
    }

    public final int a(b bVar, String str) {
        ContentValues c = c(bVar);
        String str2 = "";
        if (bVar.l() > 0) {
            str2 = Long.toString(bVar.l());
        }
        Uri parse = Uri.parse("content://sms/" + str2);
        if (bVar.l() <= 0) {
            return 0;
        }
        "update uri:" + parse + " values:" + c.toString() + " where:" + str;
        return this.b.getContentResolver().update(parse, c, str, null);
    }

    public final int a(String str, String str2, boolean z) {
        Uri parse = Uri.parse("content://sms/");
        String str3 = "";
        if (z) {
            str3 = str3 + " and read = 0 ";
        }
        Cursor a2 = a(parse, new String[]{"count(*) as count"}, "address like '%" + str + "%' " + (str3 + g(str2)), null, null);
        if (a2 == null || !a2.moveToFirst()) {
            return 0;
        }
        "queryMessageCountByNumber:" + str + " messageType:" + str2 + " count:" + a2.getInt(0);
        int i = a2.getInt(0);
        a2.close();
        return i;
    }

    public final int a(boolean z, int i) {
        Uri parse = Uri.parse("content://sms/");
        String str = "type != 3 ";
        if (z) {
            str = str + "and (protocol = 'youni' or protocol = 'youni_offline') ";
        }
        Cursor a2 = a(parse, new String[]{"_id"}, i == 1 ? str + " and type=" + "2" : i == 2 ? str + " and type=" + "1" : str, null, null);
        if (a2 != null) {
            return a2.getCount();
        }
        return 0;
    }

    public final Cursor a(Uri uri, String[] strArr, String str, String[] strArr2, String str2) {
        if (this.b != null) {
            return this.b.getContentResolver().query(uri, strArr, str, strArr2, str2);
        }
        return null;
    }

    public final b a(String str) {
        return a(a(Uri.parse("content://sms/"), a.f420a, "_id=?", new String[]{str}, null));
    }

    public final String a(String str, b bVar) {
        Cursor query;
        Uri a2 = com.sd.a.a.a.b.b.a(this.b, this.b.getContentResolver(), Uri.parse(str), c(bVar));
        "insertMessageIntoDB finish, Uri=" + (a2 == null ? null : a2.toString());
        if (a2 == null || (query = this.b.getContentResolver().query(a2, new String[]{"_id"}, null, null, null)) == null || !query.moveToFirst()) {
            return "";
        }
        String string = query.getString(query.getColumnIndex("_id"));
        "insertMessageIntoDB id:" + string;
        query.close();
        return string;
    }

    public final List a() {
        ArrayList arrayList = new ArrayList();
        Cursor query = this.b.getContentResolver().query(Uri.parse("content://sms/sent"), new String[]{"count(*) as count, address"}, "1=1) group by (address", null, "count DESC limit " + 30);
        if (query != null && query.moveToFirst()) {
            do {
                com.snda.youni.modules.d.a aVar = new com.snda.youni.modules.d.a();
                aVar.a(query.getString(1));
                aVar.a(query.getInt(0));
                arrayList.add(aVar);
            } while (query.moveToNext());
            query.close();
        }
        return arrayList;
    }

    public final List a(Uri uri, String str, String str2) {
        Cursor a2;
        if (this.b == null || (a2 = a(uri, null, str, null, str2)) == null || !a2.moveToFirst()) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        do {
            arrayList.add(b(a2));
        } while (a2.moveToNext());
        a2.close();
        return arrayList;
    }

    public final List a(boolean z) {
        String str;
        Cursor query = this.b.getContentResolver().query(Uri.parse("content://sms/"), new String[]{"count(*) as count, thread_id"}, (z ? " type!=3 and thread_id is not null " + " and (protocol = 'youni' or protocol = 'youni_offline') " : " type!=3 and thread_id is not null ") + ") group by (thread_id", null, null);
        if (query == null || !query.moveToFirst()) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        do {
            com.snda.youni.modules.d.a aVar = new com.snda.youni.modules.d.a();
            Cursor a2 = a(Uri.parse("content://mms-sms/conversations").buildUpon().appendQueryParameter("simple", "true").build(), null, " message_count>0 and _id = ?", new String[]{query.getString(1)}, "date DESC");
            if (a2 != null && a2.moveToFirst()) {
                int columnIndex = a2.getColumnIndex("recipient_ids");
                if (a2.getString(columnIndex) != null) {
                    a2.getString(columnIndex);
                    int length = a2.getString(columnIndex).split(" ").length;
                    String str2 = a2.getString(columnIndex).split(" ")[0];
                    String substring = a2.getString(columnIndex).substring(str2.length());
                    Cursor a3 = a(Uri.parse("content://sms/"), new String[]{"canonical_addresses.address from canonical_addresses where canonical_addresses._id ='" + str2 + "' --"}, null, null, null);
                    if (length == 1) {
                        if (a3 != null && a3.moveToFirst()) {
                            str = a3.getString(0);
                            aVar.a(a3.getString(0));
                        }
                    } else if (a3 != null && a3.moveToFirst()) {
                        str = a3.getString(0) + "group" + substring;
                        aVar.a(a3.getString(0) + "group" + substring);
                    }
                    a3.close();
                    if (z) {
                        str + " y:" + query.getInt(0);
                        aVar.b(query.getInt(0));
                    } else {
                        str + " a:" + query.getInt(0);
                        aVar.a(query.getInt(0));
                        aVar.c(query.getInt(0));
                    }
                    arrayList.add(aVar);
                }
            }
            a2.close();
        } while (query.moveToNext());
        query.close();
        return arrayList;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public final void a(Uri uri, long j) {
        ContentValues contentValues = new ContentValues(1);
        contentValues.put("read", (Integer) 1);
        "markConversationRead, threadId=" + j;
        new b(this, this.b.getContentResolver(), j).startUpdate(0, null, uri, contentValues, "read = 0", null);
    }

    public final void a(ai aiVar) {
        "new network:" + aiVar;
        if (aiVar != null) {
            aiVar.a(new d(this));
            aiVar.a(new a(this));
        }
    }

    public final void a(ai aiVar, b bVar) {
        if (aiVar != null && aiVar != null) {
            com.snda.youni.attachment.b.b s = bVar.s();
            m a2 = s == null ? m.a(bVar.k(), bVar.a(), bVar.c(), bVar.b()) : m.a(bVar.k(), bVar.a(), bVar.c(), bVar.b(), s.g(), s.j(), s.k(), s.i(), s.n());
            a2.a(bVar.d().toString());
            aiVar.a(a2);
        }
    }

    public final b b(String str) {
        return a(a(Uri.parse("content://sms/"), a.f420a, "service_center=?", new String[]{str}, null));
    }

    public final boolean b(b bVar) {
        "address:" + bVar.a();
        bVar.g(null);
        String l = Long.toString(bVar.l());
        SmsManager smsManager = SmsManager.getDefault();
        if (!(bVar == null || smsManager == null || bVar.a() == null || bVar.a().equals(""))) {
            if ("0".equalsIgnoreCase(l) || "".equalsIgnoreCase(l)) {
                l = a("content://sms/outbox", bVar);
                bVar.b(Long.parseLong(l));
            } else if (bVar.l() > 0) {
                l = "" + bVar.l();
            }
            "sendBySMS id:" + l;
            Intent intent = new Intent("com.snda.youni.receiver.SMS.RECEIPT");
            Bundle bundle = new Bundle();
            bundle.putString("_id" + l, l);
            intent.putExtras(bundle);
            Intent intent2 = new Intent("com.snda.youni.receiver.SMS.DELIVERED");
            intent2.putExtras(bundle);
            if (l != null && !"0".equalsIgnoreCase(l) && !"".equalsIgnoreCase(l)) {
                PendingIntent broadcast = PendingIntent.getBroadcast(this.b, Integer.parseInt(l), intent, 134217728);
                PendingIntent broadcast2 = PendingIntent.getBroadcast(this.b, Integer.parseInt(l), intent2, 134217728);
                if (!q.isWellFormedSmsAddress(bVar.a())) {
                    bVar.d("5");
                    "update to sent result " + a(bVar, "_id=" + bVar.l());
                    return false;
                }
                try {
                    if (bVar.b().length() > 70) {
                        Iterator<String> it = smsManager.divideMessage(bVar.b()).iterator();
                        while (it.hasNext()) {
                            String next = it.next();
                            try {
                                Thread.sleep(1000);
                            } catch (InterruptedException e) {
                                e.getMessage();
                            }
                            bVar.a() + ":" + next;
                            smsManager.sendTextMessage(bVar.a(), null, next, broadcast, broadcast2);
                        }
                    } else {
                        smsManager.sendTextMessage(bVar.a(), null, bVar.b(), broadcast, broadcast2);
                    }
                    return true;
                } catch (RuntimeException e2) {
                    e2.getClass() + ": " + e2.getMessage();
                    bVar.d("5");
                    "update to sent result " + a(bVar, "_id=" + bVar.l());
                }
            }
        }
        return false;
    }

    public final int c(String str) {
        return this.b.getContentResolver().delete(Uri.parse("content://sms/"), (str == null || (str != null && "".equalsIgnoreCase(str))) ? "type=3 and (address is null or address='')" : "type=3 and address like '%" + str + "%' ", null);
    }

    public final int d(String str) {
        String str2;
        String str3;
        Uri parse = Uri.parse("content://sms/");
        Uri parse2 = Uri.parse("content://mms/");
        if (str == null) {
            str2 = "type=3 and thread_id is null";
            str3 = "msg_box=3 and thread_id is null";
        } else {
            str2 = "type=3 and thread_id = " + str;
            str3 = "msg_box=3 and thread_id = " + str;
        }
        "smsWhere=" + str2 + ", mmsWhere=" + str3;
        int delete = this.b.getContentResolver().delete(parse, str2, null);
        int delete2 = this.b.getContentResolver().delete(parse2, str3, null);
        int i = delete + delete2;
        "smsResult+mmsResult=" + (delete + delete2);
        if (str != null && i > 0) {
            int parseInt = Integer.parseInt(str);
            com.sd.android.mms.d.d.a().a((long) parseInt, false);
            l.a(this.b, (long) parseInt).b();
        }
        return i;
    }

    public final void e(String str) {
        SharedPreferences.Editor edit = PreferenceManager.getDefaultSharedPreferences(this.b).edit();
        edit.putBoolean(str, true);
        edit.commit();
    }

    public final boolean f(String str) {
        return PreferenceManager.getDefaultSharedPreferences(this.b).getBoolean(str, false);
    }
}
