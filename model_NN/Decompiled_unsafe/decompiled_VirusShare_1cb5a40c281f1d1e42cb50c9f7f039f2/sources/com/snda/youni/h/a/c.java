package com.snda.youni.h.a;

import android.os.AsyncTask;
import com.snda.youni.modules.d.b;

final class c extends AsyncTask {

    /* renamed from: a  reason: collision with root package name */
    private d f426a;
    private /* synthetic */ a b;

    public c(a aVar, d dVar) {
        this.b = aVar;
        this.f426a = dVar;
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ Object doInBackground(Object[] objArr) {
        b bVar = ((b[]) objArr)[0];
        "SendSmsTask doInBackground:" + bVar.a() + " " + bVar.b();
        if (this.f426a != null) {
            return Boolean.valueOf(this.f426a.b(bVar));
        }
        return false;
    }
}
