package com.snda.youni.h.a;

import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import com.sd.android.mms.d.d;
import com.snda.youni.C0000R;
import com.snda.youni.b.ai;
import com.snda.youni.b.p;
import com.snda.youni.b.w;
import com.snda.youni.e.s;
import com.snda.youni.h.a.b.c;
import com.snda.youni.mms.ui.k;
import com.snda.youni.modules.a.l;
import com.snda.youni.modules.d.b;
import java.util.Date;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public final class a {
    private static a d;
    private static final String[] e = {"read", "address", "person", "body", "type", "protocol", "date"};

    /* renamed from: a  reason: collision with root package name */
    private d f419a = new c(this.b);
    private Context b;
    private SharedPreferences c;

    public a(Context context) {
        this.b = context;
        this.c = PreferenceManager.getDefaultSharedPreferences(context);
    }

    private int a(boolean z, int i) {
        return this.f419a.a(z, i);
    }

    public static a a() {
        return d;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    private String a(String str, b bVar) {
        Uri parse = Uri.parse(str);
        ContentValues contentValues = new ContentValues();
        contentValues.put("read", bVar.g() ? 1 : 0);
        if (bVar.a() != null) {
            contentValues.put("address", bVar.a());
        }
        if (bVar.m() != null) {
            contentValues.put("status", bVar.m());
        }
        if (bVar.d() != null) {
            contentValues.put("date", bVar.d());
        }
        if (bVar.c() != null) {
            contentValues.put("subject", bVar.c());
        }
        if (bVar.b() != null) {
            contentValues.put("body", bVar.b());
        }
        if (bVar.j() != null) {
            contentValues.put("person", bVar.j());
        }
        if (bVar.e()) {
            contentValues.put("status", (Integer) 64);
        }
        if (bVar.h() > 0) {
            contentValues.put("thread_id", Long.valueOf(bVar.h()));
        }
        if (bVar.k() != null) {
            contentValues.put("service_center", bVar.k());
        }
        if (bVar.i() != null) {
            contentValues.put("protocol", bVar.i());
        }
        if (bVar.f() != null) {
            contentValues.put("type", bVar.f());
        }
        Uri insert = this.b.getContentResolver().insert(parse, contentValues);
        "insertMessageIntoDB uri:" + insert.toString();
        if (insert != null) {
            "insertMessageIntoDB uri:" + insert.toString();
            Cursor query = this.b.getContentResolver().query(insert, new String[]{"_id"}, null, null, null);
            if (query != null && query.moveToFirst()) {
                String string = query.getString(query.getColumnIndex("_id"));
                "insertMessageIntoDB id:" + string;
                query.close();
                return string;
            }
        }
        return "";
    }

    public static void a(Context context) {
        d = new a(context);
    }

    public final int a(long j, ContentValues contentValues) {
        return this.f419a.a(j, contentValues);
    }

    public final int a(b bVar, String str) {
        return this.f419a.a(bVar, str);
    }

    public final int a(String str) {
        return this.f419a.c(str);
    }

    public final int a(String str, long j) {
        int a2 = this.f419a.a(j);
        if (a2 > 0) {
            try {
                l.a(this.b, Integer.parseInt(str));
            } catch (NumberFormatException e2) {
                e2.printStackTrace();
            }
        }
        return a2;
    }

    public final Cursor a(long j) {
        return this.f419a.a(ContentUris.withAppendedId(Uri.parse("content://mms-sms/conversations"), j), k.f471a, " (type!=3 or type is null) ", null, null);
    }

    public final Cursor a(String str, int i) {
        Uri parse = Uri.parse("content://sms/");
        return this.f419a.a(parse, null, "address=?", new String[]{str}, "date DESC limit " + i);
    }

    public final Cursor a(String str, String str2) {
        return this.f419a.a(Uri.parse("content://sms/"), null, str, null, str2);
    }

    public final Cursor a(String[] strArr) {
        Cursor query = this.b.getContentResolver().query(com.snda.youni.providers.l.f596a, new String[]{"blacker_rid"}, null, null, null);
        String str = "";
        if (query.getCount() != 0) {
            String str2 = " AND recipient_ids NOT IN (";
            while (query.moveToNext()) {
                str2 = str2 + query.getString(0) + ",";
            }
            str = str2.substring(0, str2.length() - 1) + ")";
        }
        query.close();
        return this.f419a.a(Uri.parse("content://mms-sms/conversations").buildUpon().appendQueryParameter("simple", "true").build(), strArr, "type!=3 AND date > 0 and message_count>0 " + str, null, "date DESC");
    }

    public final String a(b bVar) {
        bVar.e("youni");
        bVar.g("+666666");
        return a("content://sms/outbox", bVar);
    }

    public final void a(ai aiVar) {
        this.f419a.a(aiVar);
    }

    public final void a(ai aiVar, b bVar) {
        if (aiVar != null) {
            String hexString = Long.toHexString(Double.doubleToLongBits(Math.random()));
            bVar.g(hexString);
            ContentValues contentValues = new ContentValues();
            contentValues.put("service_center", hexString);
            this.b.getContentResolver().update(Uri.parse("content://sms/" + bVar.l()), contentValues, null, null);
        }
        if (this.c.getBoolean("chat_switch_youni", true) || bVar.r()) {
            bVar.e("youni");
            if ("0".equalsIgnoreCase("" + bVar.h())) {
                c(aiVar, bVar);
                return;
            }
            e eVar = new e(this, this.f419a);
            eVar.a(aiVar);
            eVar.execute(bVar);
            return;
        }
        bVar.e("sms");
        if (" ".equalsIgnoreCase(bVar.b())) {
            return;
        }
        if ("0".equalsIgnoreCase("" + bVar.h()) || bVar.r()) {
            b(bVar);
            return;
        }
        new c(this, this.f419a).execute(bVar);
    }

    public final int b(long j) {
        return this.f419a.a(j, "youni");
    }

    public final int b(String str) {
        return this.f419a.d(str);
    }

    public final int b(String[] strArr) {
        int i;
        StringBuffer stringBuffer = new StringBuffer();
        for (String str : strArr) {
            if (stringBuffer.length() > 0) {
                stringBuffer.append(',');
            }
            stringBuffer.append("'" + str + "'");
        }
        Cursor a2 = this.f419a.a(Uri.parse("content://sms/"), new String[]{"canonical_addresses.address from canonical_addresses where " + ("canonical_addresses.address in (" + stringBuffer.toString() + ") order by canonical_addresses._id limit 1 --")}, null, null, null);
        if (a2 == null || !a2.moveToFirst()) {
            return 0;
        }
        String string = a2.getString(0);
        "address = " + string;
        int i2 = 0;
        while (true) {
            if (i2 >= strArr.length) {
                i = 0;
                break;
            } else if (strArr[i2].equals(string)) {
                i = i2;
                break;
            } else {
                i2++;
            }
        }
        a2.close();
        return i;
    }

    public final b b(String str, String str2) {
        List a2 = this.f419a.a(Uri.parse("content://sms/draft"), str == null ? "(address is null or address='') and thread_id is null" : (str2 == null || "".equalsIgnoreCase(str2)) ? "address like '%" + str + "%'" : "address like '%" + str + "%' or thread_id=" + str2, "date DESC");
        if (a2 == null || a2.isEmpty()) {
            return null;
        }
        return (b) a2.get(0);
    }

    public final List b() {
        return this.f419a.a(Uri.parse("content://sms/inbox"), "read=0", "date DESC");
    }

    public final void b(ai aiVar, b bVar) {
        if (aiVar != null) {
            bVar.g(Long.toHexString(Double.doubleToLongBits(Math.random())));
        }
        if (this.c.getBoolean("chat_switch_youni", true) || bVar.r()) {
            bVar.e("youni");
            if (aiVar != null && !" ".equalsIgnoreCase(bVar.b())) {
                a("content://sms/outbox", bVar);
            }
            if ("0".equalsIgnoreCase("" + bVar.h())) {
                c(aiVar, bVar);
                return;
            }
            e eVar = new e(this, this.f419a);
            eVar.a(aiVar);
            eVar.execute(bVar);
            return;
        }
        bVar.e("sms");
        if (" ".equalsIgnoreCase(bVar.b())) {
            return;
        }
        if (bVar.a() != null && bVar.a().contains("krobot_001")) {
            bVar.d("5");
            a("content://sms/", bVar);
        } else if ("0".equalsIgnoreCase("" + bVar.h()) || bVar.r()) {
            b(bVar);
        } else {
            new c(this, this.f419a).execute(bVar);
        }
    }

    public final boolean b(b bVar) {
        return this.f419a.b(bVar);
    }

    public final int c(long j) {
        return this.f419a.a(j, "sms");
    }

    public final int c(String str) {
        return this.f419a.a(Uri.parse("content://sms/"), str);
    }

    public final Cursor c() {
        return a((String[]) null);
    }

    public final String c(b bVar) {
        String str;
        int a2 = this.f419a.a(bVar);
        if (a2 == 0) {
            str = this.f419a.a("content://sms/draft", bVar);
            "insertDraft:" + str;
        } else {
            "updateDraft:" + a2;
            str = "ok";
        }
        if (!TextUtils.isEmpty(str)) {
            d.a().a(bVar.h(), true);
            l.a(this.b, bVar.h()).b();
        }
        return str;
    }

    public final void c(ai aiVar, b bVar) {
        this.f419a.a(aiVar, bVar);
    }

    public final b d(String str) {
        return this.f419a.a(str);
    }

    public final String d(b bVar) {
        String a2 = this.f419a.a("content://sms/inbox", bVar);
        com.snda.youni.attachment.b.b s = bVar.s();
        if (s != null) {
            s.b(Long.parseLong(a2));
            com.snda.youni.attachment.b.a.a(this.b, s);
        }
        return a2;
    }

    public final List d() {
        return this.f419a.a();
    }

    public final void d(long j) {
        "updateToReadByThreadId, threadId=" + j;
        this.f419a.a(ContentUris.withAppendedId(Uri.parse("content://mms-sms/conversations"), j), j);
    }

    public final long e(String str) {
        long j;
        Cursor a2 = this.f419a.a(Uri.parse("content://sms/"), null, "address like '%" + str + "%'", null, null);
        if (a2 != null && a2.moveToFirst()) {
            while (true) {
                j = a2.getLong(a2.getColumnIndex("thread_id"));
                if (j <= 0) {
                    if (!a2.moveToNext()) {
                        break;
                    }
                } else {
                    break;
                }
            }
        } else {
            j = 0;
        }
        if (a2 != null) {
            a2.close();
        }
        return j;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.snda.youni.h.a.a.a(boolean, int):int
     arg types: [int, int]
     candidates:
      com.snda.youni.h.a.a.a(java.lang.String, com.snda.youni.modules.d.b):java.lang.String
      com.snda.youni.h.a.a.a(long, android.content.ContentValues):int
      com.snda.youni.h.a.a.a(com.snda.youni.modules.d.b, java.lang.String):int
      com.snda.youni.h.a.a.a(java.lang.String, long):int
      com.snda.youni.h.a.a.a(java.lang.String, int):android.database.Cursor
      com.snda.youni.h.a.a.a(java.lang.String, java.lang.String):android.database.Cursor
      com.snda.youni.h.a.a.a(com.snda.youni.b.ai, com.snda.youni.modules.d.b):void
      com.snda.youni.h.a.a.a(boolean, int):int */
    public final String e() {
        String string = PreferenceManager.getDefaultSharedPreferences(this.b).getString("self_phone_number", "");
        if (!"".equalsIgnoreCase(string)) {
            List<com.snda.youni.modules.d.a> a2 = this.f419a.a(false);
            List a3 = this.f419a.a(true);
            if (!(a2 == null || a3 == null)) {
                for (int i = 0; i < a2.size(); i++) {
                    int i2 = 0;
                    while (true) {
                        if (i2 < a3.size()) {
                            if (a2.get(i) != null && ((com.snda.youni.modules.d.a) a2.get(i)).a() != null && a3.get(i2) != null && ((com.snda.youni.modules.d.a) a2.get(i)).a().equals(((com.snda.youni.modules.d.a) a3.get(i2)).a())) {
                                int c2 = ((com.snda.youni.modules.d.a) a3.get(i2)).c();
                                ((com.snda.youni.modules.d.a) a2.get(i)).b(c2);
                                ((com.snda.youni.modules.d.a) a2.get(i)).c(((com.snda.youni.modules.d.a) a2.get(i)).b() - c2);
                                break;
                            }
                            i2++;
                        } else {
                            break;
                        }
                    }
                }
            }
            if (a2 != null) {
                JSONObject jSONObject = new JSONObject();
                JSONObject jSONObject2 = new JSONObject();
                JSONObject jSONObject3 = new JSONObject();
                JSONArray jSONArray = new JSONArray();
                for (com.snda.youni.modules.d.a aVar : a2) {
                    JSONObject jSONObject4 = new JSONObject();
                    try {
                        jSONObject4.accumulate("ad", aVar.a());
                        jSONObject4.accumulate("ac", Integer.valueOf(aVar.b()));
                        jSONObject4.accumulate("yc", Integer.valueOf(aVar.c()));
                        jSONObject4.accumulate("sc", Integer.valueOf(aVar.d()));
                        jSONArray.put(jSONObject4);
                    } catch (JSONException e2) {
                        e2.getMessage();
                        return null;
                    }
                }
                JSONObject jSONObject5 = new JSONObject();
                try {
                    int a4 = a(true, 1);
                    int a5 = a(true, 0);
                    int a6 = a(false, 0);
                    int a7 = a(true, 2);
                    jSONObject5.accumulate("ysall", Integer.valueOf(a4));
                    jSONObject5.accumulate("yrall", Integer.valueOf(a7));
                    jSONObject5.accumulate("sall", Integer.valueOf(a6 - a5));
                    jSONObject5.accumulate("allcount", Integer.valueOf(a6));
                    jSONArray.put(jSONObject5);
                } catch (JSONException e3) {
                    e3.getMessage();
                }
                try {
                    jSONObject.accumulate("did", ((TelephonyManager) this.b.getSystemService("phone")).getDeviceId());
                    jSONObject.accumulate("av", this.b.getPackageManager().getPackageInfo(this.b.getPackageName(), 0).versionName);
                } catch (JSONException e4) {
                    e4.getMessage();
                } catch (PackageManager.NameNotFoundException e5) {
                    e5.getMessage();
                }
                try {
                    jSONObject2.accumulate("tag", "smsstat");
                    jSONObject2.accumulate("label", string);
                    jSONObject2.accumulate("d", jSONArray);
                    jSONObject2.accumulate("date", Long.valueOf(new Date().getTime()));
                } catch (JSONException e6) {
                    e6.getMessage();
                }
                try {
                    jSONObject3.accumulate("header", jSONObject);
                    jSONObject3.accumulate("bd", jSONObject2);
                } catch (JSONException e7) {
                    e7.getMessage();
                }
                return "[" + jSONObject3.toString() + "]";
            }
        }
        return null;
    }

    public final String f(String str) {
        Cursor a2 = this.f419a.a(Uri.parse("content://sms/"), new String[]{"canonical_addresses.address from canonical_addresses where canonical_addresses._id ='" + str + "' --"}, null, null, null);
        if (a2 == null || !a2.moveToFirst()) {
            return null;
        }
        String string = a2.getString(0);
        a2.close();
        return string;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.snda.youni.h.a.d.a(java.lang.String, java.lang.String, boolean):int
     arg types: [java.lang.String, java.lang.String, int]
     candidates:
      com.snda.youni.h.a.d.a(android.net.Uri, java.lang.String, java.lang.String):java.util.List
      com.snda.youni.h.a.d.a(java.lang.String, java.lang.String, boolean):int */
    public final int g(String str) {
        return this.f419a.a(str, "all", true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.snda.youni.h.a.d.a(java.lang.String, java.lang.String, boolean):int
     arg types: [java.lang.String, java.lang.String, int]
     candidates:
      com.snda.youni.h.a.d.a(android.net.Uri, java.lang.String, java.lang.String):java.util.List
      com.snda.youni.h.a.d.a(java.lang.String, java.lang.String, boolean):int */
    public final int h(String str) {
        return this.f419a.a(str, "all", false);
    }

    public final void i(String str) {
        com.snda.youni.i.b bVar = new com.snda.youni.i.b();
        bVar.a(s.c + str);
        String a2 = bVar.a();
        if (a2 != null) {
            b bVar2 = new b();
            bVar2.a(str);
            String string = this.b.getString(C0000R.string.invitation_message, a2);
            "inviteMessage = " + string;
            bVar2.b(string);
            bVar2.a(Long.valueOf(System.currentTimeMillis()));
            bVar2.e("sms");
            bVar2.d("2");
            b(bVar2);
            com.snda.youni.g.a.a(this.b.getApplicationContext(), "send_invite", str);
            new w().a(this.b.getApplicationContext(), new p(s.c, str, String.valueOf(System.currentTimeMillis() / 1000)), false);
        }
    }

    public final String j(String str) {
        Cursor a2 = this.f419a.a(Uri.parse("content://mms-sms/conversations").buildUpon().appendQueryParameter("simple", "true").build(), null, "_id = " + str, null, null);
        if (a2 == null || !a2.moveToFirst()) {
            return "";
        }
        String f = f(a2.getString(a2.getColumnIndex("recipient_ids")));
        a2.close();
        return f;
    }
}
