package com.snda.youni.h.a.b;

import android.content.AsyncQueryHandler;
import android.content.ContentResolver;
import android.database.Cursor;
import com.snda.youni.modules.a.l;

final class b extends AsyncQueryHandler {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ long f423a;
    private /* synthetic */ c b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    b(c cVar, ContentResolver contentResolver, long j) {
        super(contentResolver);
        this.b = cVar;
        this.f423a = j;
    }

    /* access modifiers changed from: protected */
    public final void onQueryComplete(int i, Object obj, Cursor cursor) {
        super.onQueryComplete(i, obj, cursor);
    }

    /* access modifiers changed from: protected */
    public final void onUpdateComplete(int i, Object obj, int i2) {
        super.onUpdateComplete(i, obj, i2);
        "markConversationRead update finish, result=" + i2;
        if (i2 > 0) {
            l.a(this.b.b, (int) this.f423a);
        }
    }
}
