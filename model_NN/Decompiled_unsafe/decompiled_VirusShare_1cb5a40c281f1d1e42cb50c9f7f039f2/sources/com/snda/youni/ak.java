package com.snda.youni;

import android.content.ContentResolver;
import android.database.Cursor;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.provider.ContactsContract;
import android.text.TextUtils;
import com.snda.youni.providers.n;
import java.util.ArrayList;

final class ak extends HandlerThread implements Handler.Callback {

    /* renamed from: a  reason: collision with root package name */
    private final ContentResolver f312a;
    private final StringBuilder b = new StringBuilder();
    private final ArrayList c = new ArrayList();
    private final ArrayList d = new ArrayList();
    private Handler e;
    private /* synthetic */ d f;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ak(d dVar, ContentResolver contentResolver) {
        super("PhotoLoader");
        this.f = dVar;
        this.f312a = contentResolver;
        setPriority(1);
    }

    private void b() {
        Cursor cursor;
        int size = this.c.size();
        if (size != 0) {
            this.b.setLength(0);
            this.b.append("contact_id IN(");
            for (int i = 0; i < size; i++) {
                if (i != 0) {
                    this.b.append(',');
                }
                this.b.append('?');
            }
            this.b.append(')');
            try {
                Cursor query = this.f312a.query(n.f597a, this.f.c, "contact_type=1 AND " + this.b.toString(), (String[]) this.d.toArray(d.f388a), null);
                if (query != null) {
                    try {
                        query.moveToPosition(-1);
                        while (query.moveToNext()) {
                            Integer valueOf = Integer.valueOf(query.getInt(0));
                            if (this.f.a(valueOf.intValue(), query.getBlob(1))) {
                                this.c.remove(valueOf);
                                this.d.remove(valueOf);
                            } else {
                                String string = query.getString(2);
                                "bytes==null, call loadPhotoFromServer, url=" + string;
                                if (!TextUtils.isEmpty(string)) {
                                    this.f.a(valueOf.intValue());
                                    this.c.remove(valueOf);
                                    this.d.remove(valueOf);
                                    this.f.a(this.f312a, valueOf.intValue(), string);
                                }
                            }
                        }
                        if (query != null) {
                            query.close();
                        }
                    } catch (Throwable th) {
                        Throwable th2 = th;
                        cursor = query;
                        th = th2;
                        if (cursor != null) {
                            cursor.close();
                        }
                        throw th;
                    }
                } else if (query != null) {
                    query.close();
                }
            } catch (Throwable th3) {
                th = th3;
                cursor = null;
            }
        }
    }

    public final void a() {
        if (this.e == null) {
            this.e = new Handler(getLooper(), this);
        }
        this.e.sendEmptyMessage(0);
    }

    public final boolean handleMessage(Message message) {
        Cursor cursor;
        d.a(this.f, this.c, this.d);
        int size = this.c.size();
        if (size != 0) {
            this.b.setLength(0);
            this.b.append("contact_id IN(");
            for (int i = 0; i < size; i++) {
                if (i != 0) {
                    this.b.append(',');
                }
                this.b.append('?');
            }
            this.b.append(')');
            try {
                Cursor query = this.f312a.query(ContactsContract.Data.CONTENT_URI, this.f.b, "mimetype='vnd.android.cursor.item/photo' AND " + this.b.toString(), (String[]) this.d.toArray(d.f388a), null);
                if (query != null) {
                    while (query.moveToNext()) {
                        try {
                            Integer valueOf = Integer.valueOf(query.getInt(0));
                            if (this.f.a(valueOf.intValue(), query.getBlob(1))) {
                                this.c.remove(valueOf);
                                this.d.remove(valueOf.toString());
                            }
                        } catch (Throwable th) {
                            Throwable th2 = th;
                            cursor = query;
                            th = th2;
                            if (cursor != null) {
                                cursor.close();
                            }
                            throw th;
                        }
                    }
                }
                if (query != null) {
                    query.close();
                }
                b();
                int size2 = this.c.size();
                for (int i2 = 0; i2 < size2; i2++) {
                    boolean unused = this.f.a(((Integer) this.c.get(i2)).intValue(), (byte[]) null);
                }
            } catch (Throwable th3) {
                th = th3;
                cursor = null;
            }
        }
        this.f.h.sendEmptyMessage(2);
        return true;
    }
}
