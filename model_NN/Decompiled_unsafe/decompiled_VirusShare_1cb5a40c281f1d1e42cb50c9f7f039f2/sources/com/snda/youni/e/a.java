package com.snda.youni.e;

import android.content.Context;
import android.content.Intent;
import android.text.style.ClickableSpan;
import android.view.View;
import com.snda.youni.activities.SettingsCountActivity;

final class a extends ClickableSpan {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ Context f393a;

    a(Context context) {
        this.f393a = context;
    }

    public final void onClick(View view) {
        this.f393a.startActivity(new Intent(this.f393a, SettingsCountActivity.class));
    }
}
