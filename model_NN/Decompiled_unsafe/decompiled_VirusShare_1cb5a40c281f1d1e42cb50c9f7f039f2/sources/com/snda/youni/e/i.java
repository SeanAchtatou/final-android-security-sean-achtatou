package com.snda.youni.e;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.ref.SoftReference;

final class i {

    /* renamed from: a  reason: collision with root package name */
    private static int f398a = 21000;
    private static byte[][] b = new byte[21000][];
    private static SoftReference c = null;
    private static Integer d = new Integer(0);

    private i() {
        try {
            InputStream resourceAsStream = u.class.getResourceAsStream("/pinyindb/pinyin.txt");
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(resourceAsStream));
            int i = 0;
            while (true) {
                String readLine = bufferedReader.readLine();
                if (readLine == null || i >= f398a) {
                    f398a = i;
                    "lines of pinyin.txt:" + i;
                    bufferedReader.close();
                    resourceAsStream.close();
                } else {
                    b[i] = readLine.getBytes();
                    i++;
                }
            }
            f398a = i;
            "lines of pinyin.txt:" + i;
            bufferedReader.close();
            resourceAsStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static i a() {
        i iVar;
        synchronized (d) {
            if (c == null || c.get() == null) {
                SoftReference softReference = new SoftReference(new i());
                c = softReference;
                iVar = (i) softReference.get();
            } else {
                iVar = (i) c.get();
            }
        }
        return iVar;
    }

    static /* synthetic */ String[] a(char c2) {
        int i;
        if (c2 < 19968 || c2 > 40869 || (i = c2 - 19968) >= f398a) {
            return null;
        }
        return new String(b[i]).split(",");
    }
}
