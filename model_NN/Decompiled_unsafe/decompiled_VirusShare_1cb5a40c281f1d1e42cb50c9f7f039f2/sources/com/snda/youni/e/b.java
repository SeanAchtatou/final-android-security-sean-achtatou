package com.snda.youni.e;

import android.content.Context;
import android.os.Environment;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

public final class b {

    /* renamed from: a  reason: collision with root package name */
    private static String f394a;
    private static String b;
    private static String c;
    private static String d;
    private static final String e = (Environment.getExternalStorageDirectory() + "/DCIM/Camera");

    private b() {
    }

    public static String a() {
        return f394a;
    }

    public static void a(Context context) {
        f394a = context.getCacheDir() + "/youni_update.apk";
        b = context.getCacheDir() + "/youni_skin.apk";
        c = context.getFilesDir() + "/self_portrait.jpg";
        Date date = new Date(System.currentTimeMillis());
        d = new SimpleDateFormat("'IMG'_yyyyMMdd_HHmmss").format(date) + ".jpg";
    }

    public static String b() {
        return b;
    }

    public static String c() {
        return e;
    }

    public static String d() {
        new File(e).mkdirs();
        return e + "/" + d;
    }

    public static String e() {
        return c;
    }
}
