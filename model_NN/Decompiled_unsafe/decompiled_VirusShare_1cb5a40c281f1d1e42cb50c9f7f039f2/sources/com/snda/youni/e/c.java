package com.snda.youni.e;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;

public final class c {
    public static String a(String str) {
        byte[] bArr;
        try {
            try {
                bArr = MessageDigest.getInstance("MD5").digest(str.getBytes("utf-8"));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
                bArr = null;
            }
            StringBuffer stringBuffer = new StringBuffer();
            for (byte b : bArr) {
                byte b2 = b & 255;
                if (b2 < 16) {
                    stringBuffer.append("0");
                }
                stringBuffer.append(Integer.toHexString(b2));
            }
            return stringBuffer.toString();
        } catch (Exception e2) {
            System.out.println(e2.toString());
            e2.printStackTrace();
            return "";
        }
    }
}
