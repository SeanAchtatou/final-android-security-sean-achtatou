package com.snda.youni.e;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

public final class p {

    /* renamed from: a  reason: collision with root package name */
    private static String f402a = (Environment.getExternalStorageDirectory().getAbsolutePath() + File.pathSeparator + "youni");

    public static Bitmap a(Context context, int i, Uri uri) {
        Bitmap bitmap;
        BitmapFactory.Options options = new BitmapFactory.Options();
        try {
            InputStream openInputStream = context.getContentResolver().openInputStream(uri);
            options.inJustDecodeBounds = true;
            options.inPreferredConfig = Bitmap.Config.RGB_565;
            BitmapFactory.decodeStream(openInputStream, null, options);
            int i2 = options.outWidth * options.outHeight;
            int i3 = 1;
            while (i2 > i) {
                i2 >>= 2;
                i3 <<= 1;
            }
            try {
                openInputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            options.inJustDecodeBounds = false;
            options.inSampleSize = i3;
            InputStream openInputStream2 = context.getContentResolver().openInputStream(uri);
            Bitmap decodeStream = BitmapFactory.decodeStream(openInputStream2, null, options);
            try {
                openInputStream2.close();
                try {
                    "compressed image width and height is " + decodeStream.getWidth() + " " + decodeStream.getHeight();
                    return decodeStream;
                } catch (FileNotFoundException e2) {
                    FileNotFoundException fileNotFoundException = e2;
                    bitmap = decodeStream;
                    e = fileNotFoundException;
                }
            } catch (IOException e3) {
                e3.printStackTrace();
            }
        } catch (FileNotFoundException e4) {
            e = e4;
            bitmap = null;
        }
        e.printStackTrace();
        return bitmap;
    }

    public static Bitmap a(Bitmap bitmap) {
        int width = bitmap.getWidth();
        int i = width;
        int height = bitmap.getHeight();
        while (true) {
            if (i <= 256 && height <= 256) {
                return Bitmap.createScaledBitmap(bitmap, i, height, false);
            }
            i /= 2;
            height /= 2;
        }
    }

    public static Bitmap a(String str) {
        try {
            FileInputStream fileInputStream = new FileInputStream(new File(str));
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = Bitmap.Config.RGB_565;
            Bitmap decodeStream = BitmapFactory.decodeStream(fileInputStream, null, options);
            try {
                fileInputStream.close();
                return decodeStream;
            } catch (FileNotFoundException | IOException e) {
                return decodeStream;
            }
        } catch (FileNotFoundException e2) {
            return null;
        }
    }

    public static Bitmap a(String str, String str2) {
        try {
            FileInputStream fileInputStream = new FileInputStream(new File(str2, str));
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = Bitmap.Config.RGB_565;
            Bitmap decodeStream = BitmapFactory.decodeStream(fileInputStream, null, options);
            try {
                fileInputStream.close();
                return decodeStream;
            } catch (FileNotFoundException | IOException e) {
                return decodeStream;
            }
        } catch (FileNotFoundException e2) {
            return null;
        }
    }

    public static Uri a(Bitmap bitmap, String str, String str2) {
        Uri uri;
        Exception exc;
        try {
            File file = new File(str2, str);
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            Uri fromFile = Uri.fromFile(file);
            try {
                bitmap.compress(Bitmap.CompressFormat.JPEG, 80, fileOutputStream);
                fileOutputStream.close();
                return fromFile;
            } catch (Exception e) {
                Exception exc2 = e;
                uri = fromFile;
                exc = exc2;
            }
        } catch (Exception e2) {
            Exception exc3 = e2;
            uri = null;
            exc = exc3;
        }
        exc.printStackTrace();
        return uri;
    }
}
