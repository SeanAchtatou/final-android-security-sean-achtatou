package com.snda.youni.e;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.util.Log;
import java.io.IOException;

public final class n implements MediaPlayer.OnCompletionListener, MediaPlayer.OnErrorListener {

    /* renamed from: a  reason: collision with root package name */
    private static n f401a;
    private MediaPlayer b;
    private Context c;

    private n(Context context) {
        this.c = context.getApplicationContext();
    }

    public static synchronized n a(Context context) {
        n nVar;
        synchronized (n.class) {
            if (f401a == null) {
                f401a = new n(context);
            }
            nVar = f401a;
        }
        return nVar;
    }

    private void b() {
        if (this.b != null) {
            try {
                this.b.setOnCompletionListener(this);
                this.b.setOnErrorListener(this);
                this.b.stop();
                this.b.setAudioStreamType(2);
                this.b.prepare();
            } catch (IllegalStateException e) {
                Log.e("SoundPlayerUtil", "IllegalStateException happened.");
                e.printStackTrace();
            }
        }
    }

    private void c() {
        if (this.b != null) {
            int ringerMode = ((AudioManager) this.c.getSystemService("audio")).getRingerMode();
            "ringerMode is: " + ringerMode;
            if (2 == ringerMode) {
                try {
                    this.b.start();
                } catch (IllegalStateException e) {
                    e.printStackTrace();
                }
            } else {
                this.b.release();
            }
        }
    }

    public final void a() {
        try {
            if (this.b != null) {
                this.b.stop();
            }
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }
    }

    public final void a(int i) {
        try {
            if (this.b != null) {
                if (!this.b.isPlaying()) {
                    this.b.stop();
                } else {
                    return;
                }
            }
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }
        this.b = MediaPlayer.create(this.c, i);
        b();
        c();
    }

    public final void a(Uri uri) {
        try {
            if (this.b != null) {
                if (!this.b.isPlaying()) {
                    this.b.stop();
                } else {
                    return;
                }
            }
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }
        this.b = MediaPlayer.create(this.c, uri);
        if (this.b == null) {
            throw new IOException();
        }
        b();
        c();
        "schedule: " + String.valueOf(0);
    }

    public final void onCompletion(MediaPlayer mediaPlayer) {
        if (this.b != null) {
            this.b.release();
        }
    }

    public final boolean onError(MediaPlayer mediaPlayer, int i, int i2) {
        if (this.b == null) {
            return false;
        }
        this.b.release();
        return false;
    }
}
