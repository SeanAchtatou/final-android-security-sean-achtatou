package com.snda.youni.e;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.text.SpannableStringBuilder;
import android.text.style.ImageSpan;
import com.snda.youni.C0000R;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class m {

    /* renamed from: a  reason: collision with root package name */
    private static m f400a;
    private String[] b;
    private TypedArray c;
    private String[] d;
    private HashMap e = d();
    private final Pattern f = c();

    private m(Context context) {
        this.d = context.getResources().getStringArray(C0000R.array.emotion_texts);
        this.b = context.getResources().getStringArray(C0000R.array.emotion_texts);
        this.c = context.getResources().obtainTypedArray(C0000R.array.emotion_icons);
    }

    public static m a() {
        return f400a;
    }

    public static void a(Context context) {
        f400a = new m(context);
    }

    private Pattern c() {
        StringBuilder sb = new StringBuilder(this.d.length * 3);
        sb.append("(");
        for (String str : this.d) {
            new StringBuilder().append("[\\s.]+");
            sb.append(Pattern.quote(str));
            sb.append("|");
            new StringBuilder().append("^");
            sb.append(Pattern.quote(str));
            sb.append("|");
        }
        sb.replace(sb.length() - 1, sb.length(), ")");
        System.out.println(sb.toString());
        return Pattern.compile(sb.toString(), 2);
    }

    private HashMap d() {
        if (this.d.length != this.c.length()) {
            throw new IllegalStateException("emotion resource ID/text mismatch");
        }
        HashMap hashMap = new HashMap(this.d.length);
        for (int i = 0; i < this.d.length; i++) {
            String str = this.d[i];
            Integer valueOf = Integer.valueOf(i);
            hashMap.put(str, valueOf);
            "put@" + str + "," + valueOf;
        }
        return hashMap;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    public final CharSequence a(Context context, CharSequence charSequence, boolean z) {
        if (charSequence == null) {
            return null;
        }
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(charSequence);
        Matcher matcher = this.f.matcher(charSequence);
        while (matcher.find()) {
            Bitmap decodeResource = BitmapFactory.decodeResource(context.getResources(), this.c.getResourceId(((Integer) this.e.get(matcher.group().replaceAll("[\\s.]", "").toUpperCase())).intValue(), C0000R.drawable.e056));
            if (z) {
                int width = decodeResource.getWidth();
                int height = decodeResource.getHeight();
                Matrix matrix = new Matrix();
                matrix.setScale(0.6f, 0.6f);
                decodeResource = Bitmap.createBitmap(decodeResource, 0, 0, width, height, matrix, true);
            }
            spannableStringBuilder.setSpan(new ImageSpan(context, decodeResource, 1), matcher.start(), matcher.end(), 33);
        }
        return spannableStringBuilder;
    }

    public final String a(int i) {
        return this.d[i];
    }

    public final TypedArray b() {
        return this.c;
    }
}
