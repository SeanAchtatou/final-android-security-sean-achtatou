package com.snda.youni.e;

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import com.snda.youni.AppContext;
import java.util.Locale;

public final class h {

    /* renamed from: a  reason: collision with root package name */
    private static String f397a = Build.MANUFACTURER;
    private static String b = Build.MODEL;
    private static String c = "android";
    private static String d = Build.VERSION.SDK;
    private static String e;
    private static String f;
    private static String g = (Locale.getDefault().getLanguage() + "-" + Locale.getDefault().getCountry());

    static {
        try {
            Context a2 = AppContext.a();
            e = "" + a2.getPackageManager().getPackageInfo(a2.getPackageName(), 0).versionCode;
        } catch (PackageManager.NameNotFoundException e2) {
            e2.printStackTrace();
            e = "unknown";
        }
        try {
            Context a3 = AppContext.a();
            f = "" + a3.getPackageManager().getPackageInfo(a3.getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e3) {
            e3.printStackTrace();
            f = "unknown";
        }
    }

    public static String a() {
        return f397a + "/" + b + "/" + c + "/" + d + "/" + e + "/" + f + "/" + g;
    }
}
