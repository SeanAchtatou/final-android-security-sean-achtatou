package com.snda.youni.e;

import android.content.Context;
import android.media.AudioManager;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.util.Log;

public final class g {

    /* renamed from: a  reason: collision with root package name */
    private Vibrator f396a;
    private Context b;

    public g(Context context) {
        this.b = context;
        this.f396a = (Vibrator) context.getSystemService("vibrator");
    }

    public final void a(int i, boolean z) {
        int ringerMode = ((AudioManager) this.b.getSystemService("audio")).getRingerMode();
        Log.i("Vibrator", "ringerMode is: " + ringerMode);
        if (ringerMode != 0) {
            switch (i) {
                case 0:
                    this.f396a.cancel();
                    int i2 = PreferenceManager.getDefaultSharedPreferences(this.b).getInt("vibrator_num", 2);
                    if (z) {
                        i2 = 1;
                    }
                    long[] jArr = new long[(i2 * 2)];
                    for (int i3 = 0; i3 < i2 * 2; i3++) {
                        jArr[i3] = 200;
                    }
                    this.f396a.vibrate(jArr, -1);
                    return;
                default:
                    return;
            }
        }
    }
}
