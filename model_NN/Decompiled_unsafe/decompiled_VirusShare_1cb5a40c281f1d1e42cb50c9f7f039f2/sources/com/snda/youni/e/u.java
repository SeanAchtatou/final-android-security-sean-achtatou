package com.snda.youni.e;

import android.text.TextUtils;
import java.io.UnsupportedEncodingException;

public class u {
    public static String a(String str) {
        if (TextUtils.isEmpty(str)) {
            return str;
        }
        try {
            byte[] bytes = String.valueOf(str.charAt(0)).getBytes("utf8");
            if (1 == bytes.length) {
                if ((bytes[0] < 65 || bytes[0] > 90) && (bytes[0] < 97 || bytes[0] > 122)) {
                    return null;
                }
                return str.toLowerCase();
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        char[] cArr = new char[str.length()];
        for (int i = 0; i < str.length(); i++) {
            cArr[i] = str.charAt(i);
        }
        String str2 = "";
        for (char a2 : cArr) {
            i.a();
            String[] a3 = i.a(a2);
            if (a3 == null) {
                break;
            }
            str2 = str2 + a3[0].toString();
        }
        return str2.trim();
    }

    public static String b(String str) {
        String str2;
        UnsupportedEncodingException e;
        if (TextUtils.isEmpty(str)) {
            return str;
        }
        try {
            int length = str.length();
            String str3 = "";
            int i = 0;
            while (i < length) {
                try {
                    char charAt = str.charAt(i);
                    if (String.valueOf(charAt).getBytes("utf8").length == 1) {
                        str3 = str3.concat(String.valueOf(charAt).toLowerCase());
                    } else {
                        i.a();
                        String[] a2 = i.a(charAt);
                        if (a2 == null) {
                            return str3;
                        }
                        str3 = str3.concat(String.valueOf(a2[0].charAt(0)));
                    }
                    i++;
                } catch (UnsupportedEncodingException e2) {
                    e = e2;
                    str2 = str3;
                    e.printStackTrace();
                    return str2;
                }
            }
            return str3;
        } catch (UnsupportedEncodingException e3) {
            UnsupportedEncodingException unsupportedEncodingException = e3;
            str2 = "";
            e = unsupportedEncodingException;
            e.printStackTrace();
            return str2;
        }
    }
}
