package com.snda.youni.e;

import android.telephony.PhoneNumberUtils;
import android.text.TextUtils;

public final class q extends PhoneNumberUtils {
    public static String a(String str) {
        return (str == null || !str.startsWith("28") || str.length() <= 9 || str.length() >= 12) ? str : "+" + str;
    }

    public static boolean b(String str) {
        return str != null && (str.startsWith("+28") || str.startsWith("28"));
    }

    public static String toCallerIDMinMatch(String str) {
        if (!TextUtils.isEmpty(str) && str.startsWith("krobot")) {
            return str;
        }
        String extractNetworkPortion = PhoneNumberUtils.extractNetworkPortion(str);
        if (extractNetworkPortion == null) {
            return null;
        }
        StringBuilder sb = new StringBuilder(11);
        int length = extractNetworkPortion.length();
        int i = length - 1;
        while (i >= 0 && length - i <= 11) {
            sb.append(extractNetworkPortion.charAt(i));
            i--;
        }
        return sb.toString();
    }
}
