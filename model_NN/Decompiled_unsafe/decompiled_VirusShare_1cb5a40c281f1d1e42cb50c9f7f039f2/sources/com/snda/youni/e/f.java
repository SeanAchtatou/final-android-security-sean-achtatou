package com.snda.youni.e;

import android.content.Context;
import android.text.format.Time;
import com.snda.youni.C0000R;

public final class f {
    public static String a(long j) {
        Time time = new Time();
        time.set(j);
        return time.format("%T");
    }

    public static String a(long j, Context context) {
        long currentTimeMillis = System.currentTimeMillis();
        "currTime , overTime = " + currentTimeMillis + " " + ((int) ((currentTimeMillis - j) / 1000));
        Time time = new Time();
        time.set(currentTimeMillis);
        int i = time.year;
        int i2 = time.yearDay;
        time.set(j);
        int i3 = time.yearDay;
        return i3 == i2 ? time.format(context.getString(C0000R.string.list_today_format)) : i2 - i3 == 1 ? time.format(context.getString(C0000R.string.list_yesterday_format)) : i2 - i3 == 2 ? time.format(context.getString(C0000R.string.list_before_yesterday_format)) : time.year < i ? time.format(context.getString(C0000R.string.list_before_year_format)) : time.format(context.getString(C0000R.string.list_this_year_format));
    }

    public static String b(long j) {
        Time time = new Time();
        time.set(j);
        return time.format("%y-%m-%d");
    }
}
