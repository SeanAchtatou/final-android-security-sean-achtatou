package com.snda.youni.e;

import android.content.SharedPreferences;
import android.os.Environment;
import android.preference.PreferenceManager;
import com.snda.youni.AppContext;

public final class o {
    public static boolean a() {
        SharedPreferences defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(AppContext.a());
        return defaultSharedPreferences.getBoolean("sdcard_writable", false) && defaultSharedPreferences.getBoolean("sdcard_available", false);
    }

    public static void b() {
        SharedPreferences.Editor edit = PreferenceManager.getDefaultSharedPreferences(AppContext.a()).edit();
        String externalStorageState = Environment.getExternalStorageState();
        if ("mounted".equals(externalStorageState)) {
            edit.putBoolean("sdcard_available", true);
            edit.putBoolean("sdcard_writable", true);
        } else if ("mounted_ro".equals(externalStorageState)) {
            edit.putBoolean("sdcard_available", true);
            edit.putBoolean("sdcard_writable", false);
        } else {
            edit.putBoolean("sdcard_available", false);
            edit.putBoolean("sdcard_writable", false);
        }
        edit.commit();
    }
}
