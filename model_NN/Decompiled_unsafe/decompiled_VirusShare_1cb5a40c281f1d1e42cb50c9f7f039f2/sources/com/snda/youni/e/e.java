package com.snda.youni.e;

import android.os.Environment;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public final class e {

    /* renamed from: a  reason: collision with root package name */
    private static String f395a = (Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + "youni");

    public static void a(String str, String str2) {
        File file = new File(str2, str);
        if (file.exists()) {
            file.delete();
        }
    }

    public static void a(String str, String str2, String str3, String str4) {
        try {
            FileInputStream fileInputStream = new FileInputStream(new File(str2, str));
            FileOutputStream fileOutputStream = new FileOutputStream(new File(str4, str3));
            byte[] bArr = new byte[1024];
            while (true) {
                try {
                    int read = fileInputStream.read(bArr);
                    if (read != -1) {
                        fileOutputStream.write(bArr, 0, read);
                    } else {
                        fileInputStream.close();
                        fileOutputStream.close();
                        return;
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    return;
                }
            }
        } catch (FileNotFoundException e2) {
            e2.printStackTrace();
        }
    }

    public static boolean b(String str, String str2) {
        return new File(str2, str).exists();
    }

    public static int c(String str, String str2) {
        return (int) (new File(str2, str).length() / 1024);
    }

    public static long d(String str, String str2) {
        return new File(str2, str).length();
    }
}
