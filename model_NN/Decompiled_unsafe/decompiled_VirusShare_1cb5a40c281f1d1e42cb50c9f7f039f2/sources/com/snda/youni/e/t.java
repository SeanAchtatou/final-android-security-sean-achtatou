package com.snda.youni.e;

public final class t {
    public static String a(String str) {
        if (str == null) {
            return null;
        }
        int length = str.length();
        String str2 = "";
        for (int i = 0; i < length; i++) {
            char charAt = str.charAt(i);
            if (charAt <= '9' || charAt >= '0') {
                str2 = str2 + charAt;
            }
        }
        int length2 = str2.length();
        return (length2 <= 11 || str2.charAt(length2 - 11) != '1') ? str2 : str2.substring(length2 - 11);
    }

    public static boolean b(String str) {
        if (str == null) {
            return false;
        }
        return str.startsWith("krobot");
    }
}
