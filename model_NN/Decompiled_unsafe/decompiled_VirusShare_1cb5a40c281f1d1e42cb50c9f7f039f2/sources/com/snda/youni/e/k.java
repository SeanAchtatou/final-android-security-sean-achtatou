package com.snda.youni.e;

import android.content.Context;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ImageSpan;
import android.widget.TextView;
import com.snda.youni.C0000R;
import com.snda.youni.d.a;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.json.JSONException;
import org.json.JSONObject;

public final class k {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    public static CharSequence a(Context context, CharSequence charSequence, TextView textView, boolean z) {
        if (textView != null) {
            textView.setMovementMethod(null);
        }
        String obj = charSequence.toString();
        Matcher matcher = Pattern.compile("#(\\{.*?\\})#").matcher(obj);
        if (matcher.find()) {
            int start = matcher.start();
            int end = matcher.end();
            try {
                JSONObject jSONObject = new JSONObject(obj.substring(matcher.start(1), matcher.end(1)).replace(65292, ','));
                String string = jSONObject.getString("txt");
                if (!TextUtils.isEmpty(string)) {
                    obj = obj.substring(0, start) + string + obj.substring(end);
                }
                if (!z) {
                    return obj;
                }
                if ("xxtj".equalsIgnoreCase(jSONObject.getString("a"))) {
                    textView.setMovementMethod(LinkMovementMethod.getInstance());
                    textView.setFocusable(false);
                    int length = string.length() + start;
                    "addClickSpan s = " + ((Object) obj) + ", start = " + start + ", end = " + length;
                    SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(obj);
                    spannableStringBuilder.setSpan(new a(context), start, length, 33);
                    return spannableStringBuilder;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return charSequence;
    }

    public static CharSequence a(Context context, String str, int i) {
        if (str == null || (!str.startsWith("[ http://n.sdo.com/") && !str.startsWith("[http://n.sdo.com/"))) {
            if (str == null || (!str.startsWith("< http://n.sdo.com/") && !str.startsWith("<http://n.sdo.com/"))) {
                if (str == null || !str.contains(m.a().a(20)) || !str.contains("[http://maps.google.com/maps")) {
                    return i == 0 ? m.a().a(context, str, true) : m.a().a(context, str, false);
                }
                Matcher matcher = Pattern.compile("\\[http://maps.google.com/maps.*?\\]").matcher(str);
                if (!matcher.find()) {
                    return str;
                }
                return (((Object) str.subSequence(0, matcher.start())) + str.substring(matcher.end())).replace(":-G", context.getString(C0000R.string.conversation_snippet_location));
            } else if (i == 1 || i == 0) {
                return context.getString(C0000R.string.conversation_snippet_audio);
            } else {
                SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(str);
                if (Pattern.compile("\\<\\s*http://n.sdo.com/.*?\\>").matcher(str).find()) {
                    spannableStringBuilder.setSpan(i == 2 ? new ImageSpan(context, (int) C0000R.drawable.ic_audio_b, 1) : new ImageSpan(a.a("ic_audio"), 1), 0, str.length(), 33);
                }
                return i == 2 ? spannableStringBuilder.append((CharSequence) context.getString(C0000R.string.audio_attachment_sms_popup_text)) : spannableStringBuilder;
            }
        } else if (i == 1 || i == 0) {
            return context.getString(C0000R.string.conversation_snippet_picture);
        } else {
            SpannableStringBuilder spannableStringBuilder2 = new SpannableStringBuilder(str);
            if (Pattern.compile("\\[\\s*http://n.sdo.com/.*?\\]").matcher(str).find()) {
                spannableStringBuilder2.setSpan(i == 2 ? new ImageSpan(context, (int) C0000R.drawable.ic_picture_b, 1) : new ImageSpan(a.a("ic_picture"), 1), 0, str.length(), 33);
            }
            return i == 2 ? spannableStringBuilder2.append((CharSequence) context.getString(C0000R.string.image_attachment_sms_popup_text)) : spannableStringBuilder2;
        }
    }
}
