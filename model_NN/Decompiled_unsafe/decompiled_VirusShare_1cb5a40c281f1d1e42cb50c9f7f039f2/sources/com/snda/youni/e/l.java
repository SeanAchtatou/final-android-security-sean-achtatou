package com.snda.youni.e;

import android.text.InputFilter;
import android.text.Spanned;

public final class l implements InputFilter {

    /* renamed from: a  reason: collision with root package name */
    private static final String[] f399a = {"\n", "\r", "\r\n", "\t", "\b"};

    public final CharSequence filter(CharSequence charSequence, int i, int i2, Spanned spanned, int i3, int i4) {
        for (String equals : f399a) {
            if (equals.equals(charSequence)) {
                return "";
            }
        }
        return null;
    }
}
