package com.snda.youni;

import android.app.Activity;
import android.app.Application;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.preference.PreferenceManager;
import com.sd.android.mms.d.c;
import com.sd.android.mms.d.d;
import com.snda.youni.activities.ChatActivity;
import com.snda.youni.activities.PopupActivity;
import com.snda.youni.attachment.a.j;
import com.snda.youni.e.g;
import com.snda.youni.e.k;
import com.snda.youni.e.m;
import com.snda.youni.e.o;
import com.snda.youni.e.s;
import com.snda.youni.e.t;
import com.snda.youni.modules.a.a;
import com.snda.youni.modules.a.b;
import com.snda.youni.modules.a.l;
import com.snda.youni.modules.popup.f;
import com.snda.youni.modules.settings.i;
import com.snda.youni.modules.settings.v;
import com.snda.youni.providers.n;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Stack;

public class AppContext extends Application {
    private static Notification b = null;
    private static AppContext f;
    private static al g;
    private static Stack h = new Stack();
    private static List j = new ArrayList();

    /* renamed from: a  reason: collision with root package name */
    private final String f169a = "AppContext";
    private ChatActivity c = null;
    private boolean d = false;
    private boolean e = false;
    private HashMap i = new HashMap();
    private int k = 1;

    public AppContext() {
        f = this;
    }

    public static Context a() {
        return f;
    }

    public static void a(Activity activity) {
        j.add(new WeakReference(activity));
    }

    public static void a(al alVar) {
        g = alVar;
    }

    private void a(o oVar, boolean z) {
        if (PreferenceManager.getDefaultSharedPreferences(this).getInt("notify_name", 0) != 1) {
            String b2 = b(oVar, z);
            NotificationManager notificationManager = (NotificationManager) getSystemService("notification");
            if (b.contentView == null) {
                "notif.contentView:" + b2;
            } else {
                notificationManager.notify(0, b);
            }
        }
    }

    private String b(o oVar, boolean z) {
        String e2 = oVar.f586a;
        CharSequence a2 = k.a(getApplicationContext(), oVar.b, 1);
        String a3 = oVar.c;
        if (b == null || z) {
            b = new Notification(C0000R.drawable.icn_youni, a2, System.currentTimeMillis());
        }
        a a4 = new b(this).a(t.a(a3));
        com.snda.youni.modules.a.k kVar = new com.snda.youni.modules.a.k();
        kVar.o = "notif";
        kVar.g = a3;
        kVar.k = e2;
        kVar.f = "youni".equalsIgnoreCase(oVar.d);
        if (a4 != null) {
            kVar.i = a4.f489a;
            kVar.l = a4.d;
            kVar.f496a = a4.b;
            kVar.f = a4.f;
            "cinfo.isYouni = " + a4.f + ", nItem.type = " + oVar.d;
        }
        Intent intent = new Intent(this, ChatActivity.class);
        intent.addFlags(335544320);
        intent.putExtra("item", kVar);
        String string = getString(C0000R.string.imcoming_message_tip, new Object[]{a2});
        String str = kVar.f496a;
        if (str == null) {
            str = kVar.g;
        }
        b.setLatestEventInfo(this, str, string, PendingIntent.getActivity(this, 0, intent, 134217728));
        b.number = h.size();
        b.flags |= 1;
        b.defaults |= 4;
        if (b.contentView == null) {
            "notif.contentView null:" + string;
            b.setLatestEventInfo(this, str, string, PendingIntent.getActivity(this, 0, intent, 134217728));
        }
        return string.toString();
    }

    public static void b() {
        f.f559a.clear();
        if (g != null) {
            g.f();
        }
    }

    public static void b(Activity activity) {
        j.remove(new WeakReference(activity));
    }

    private static boolean b(long j2) {
        for (WeakReference weakReference : j) {
            if (weakReference.get() instanceof ChatActivity) {
                return ((ChatActivity) weakReference.get()).a(j2);
            }
        }
        return false;
    }

    public static boolean c() {
        return !j.isEmpty();
    }

    public static void d() {
        for (WeakReference weakReference : j) {
            if (weakReference.get() != null) {
                ((Activity) weakReference.get()).finish();
            }
        }
    }

    public final void a(int i2, String str, boolean z) {
        String string;
        if (i2 > 1) {
            string = getString(C0000R.string.notifi_has_multi_new_contact_content, new Object[]{Integer.valueOf(i2)});
        } else {
            string = getString(C0000R.string.notifi_has_one_new_contact_content, new Object[]{str});
        }
        Notification notification = new Notification(C0000R.drawable.icn_youni, string, System.currentTimeMillis());
        Intent intent = new Intent(this, YouNi.class);
        intent.addFlags(872415232);
        intent.putExtra("checked_tab_index", (int) C0000R.id.btn_contacts);
        intent.setAction("android.intent.action.MAIN");
        intent.addCategory("android.intent.category.LAUNCHER");
        notification.setLatestEventInfo(this, getString(C0000R.string.new_youni_contact), string, PendingIntent.getActivity(this, 0, intent, 134217728));
        ((NotificationManager) getSystemService("notification")).notify(this.k, notification);
        if (z) {
            a(-1);
        }
    }

    public final void a(long j2) {
        if (PreferenceManager.getDefaultSharedPreferences(this).getInt("mutable_name", 0) == 0 && j2 != -1 && !b(j2)) {
            i.a(this);
        }
        g gVar = new g(this);
        if (j2 == -1 || !b(j2)) {
            gVar.a(PreferenceManager.getDefaultSharedPreferences(this).getInt("vibrate_name", 0), false);
        } else {
            gVar.a(PreferenceManager.getDefaultSharedPreferences(this).getInt("vibrate_name", 0), true);
        }
    }

    public final void a(ChatActivity chatActivity) {
        this.c = chatActivity;
    }

    public final void a(String str) {
        if (this.c != null) {
            String i2 = this.c.i();
            if (i2 == null || str == null || !i2.contains(str)) {
                this.c.b(false);
            } else {
                this.c.b(true);
            }
        }
    }

    public final void a(String str, long j2) {
        boolean z;
        boolean z2;
        if (b != null && h != null) {
            Stack stack = (Stack) h.clone();
            int size = stack.size() - 1;
            boolean z3 = false;
            boolean z4 = true;
            while (true) {
                if (size < 0) {
                    break;
                }
                o oVar = (o) stack.get(size);
                if (z4) {
                    if (oVar == null || str == null || !str.equalsIgnoreCase(oVar.f586a) || j2 != oVar.e) {
                        z4 = false;
                    } else {
                        h.remove(size);
                        z = true;
                        z2 = false;
                        size--;
                        z4 = z2;
                        z3 = z;
                    }
                }
                if (z3) {
                    b(oVar, false);
                    z3 = false;
                }
                if (j2 == oVar.e) {
                    h.remove(size);
                    break;
                }
                z = z3;
                z2 = z4;
                size--;
                z4 = z2;
                z3 = z;
            }
            b.number = h.size();
            if (b.number == 0) {
                c(null);
            } else {
                ((NotificationManager) getSystemService("notification")).notify(0, b);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.snda.youni.AppContext.a(com.snda.youni.o, boolean):void
     arg types: [com.snda.youni.o, int]
     candidates:
      com.snda.youni.AppContext.a(java.lang.String, long):void
      com.snda.youni.AppContext.a(java.lang.String, boolean):void
      com.snda.youni.AppContext.a(com.snda.youni.o, boolean):void */
    public final void a(String str, String str2, String str3, String str4, long j2, long j3) {
        "SMS showNotification --threadId:" + str + " number:" + str3 + " content:" + str2;
        o oVar = new o(this, str, str2, str3, str4, j2, j3);
        h.add(oVar);
        if (PreferenceManager.getDefaultSharedPreferences(this).getInt("popup_name", 1) == 0 && !s.a()) {
            com.snda.youni.modules.a.k kVar = new com.snda.youni.modules.a.k();
            kVar.a(this, new b(this), oVar.c);
            kVar.b = oVar.b;
            kVar.m = "youni".equalsIgnoreCase(oVar.d);
            kVar.c = String.valueOf(oVar.e);
            kVar.k = oVar.f586a;
            long f2 = oVar.f;
            Calendar instance = Calendar.getInstance();
            instance.setTimeInMillis(f2);
            int i2 = instance.get(1);
            int i3 = instance.get(6);
            instance.setTimeInMillis(System.currentTimeMillis());
            if (i2 == instance.get(1) && i3 == instance.get(6)) {
                kVar.d = com.snda.youni.e.f.a(oVar.f);
            } else {
                kVar.d = com.snda.youni.e.f.b(oVar.f);
            }
            f.f559a.add(kVar);
            Intent intent = new Intent(this, PopupActivity.class);
            intent.putExtra("item", kVar);
            intent.addFlags(805306368);
            startActivity(intent);
            if (g != null) {
                g.f();
            }
        }
        a(oVar, true);
        a(Long.valueOf(str).longValue());
    }

    public final void a(String str, boolean z) {
        String i2;
        if (this.c != null && (i2 = this.c.i()) != null && str != null && i2.contains(str)) {
            this.c.c(z);
        }
    }

    public final void a(boolean z) {
        this.e = z;
    }

    public final void b(String str) {
        String i2;
        if (this.c != null && (i2 = this.c.i()) != null && str != null && i2.contains(str)) {
            this.c.h();
        }
    }

    public final void b(boolean z) {
        this.d = z;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.snda.youni.AppContext.a(com.snda.youni.o, boolean):void
     arg types: [com.snda.youni.o, int]
     candidates:
      com.snda.youni.AppContext.a(java.lang.String, long):void
      com.snda.youni.AppContext.a(java.lang.String, boolean):void
      com.snda.youni.AppContext.a(com.snda.youni.o, boolean):void */
    public final void c(String str) {
        NotificationManager notificationManager = (NotificationManager) getSystemService("notification");
        if (!h.isEmpty()) {
            h.peek();
            Iterator it = h.iterator();
            do {
                o oVar = (o) it.next();
                if (!(oVar.f586a == null || str == null || !oVar.f586a.equalsIgnoreCase(str))) {
                    it.remove();
                }
            } while (it.hasNext());
            if (!h.isEmpty()) {
                a((o) h.peek(), false);
                return;
            }
        }
        notificationManager.cancel(0);
    }

    public final void e() {
        ((NotificationManager) getSystemService("notification")).cancel(this.k);
    }

    public void onCreate() {
        super.onCreate();
        com.snda.youni.d.b.a(this);
        com.sd.android.mms.a.a(this);
        com.snda.youni.modules.b.b.a(this);
        com.sd.android.mms.d.i.a(this);
        d.a(this);
        l.a(this);
        com.sd.android.mms.c.a.a(this);
        m.a(this);
        com.snda.youni.h.a.a.a(this);
        c.a(this);
        com.sd.android.mms.d.l.a(this);
        j.a(this);
        com.snda.youni.attachment.k.a(this);
        List<com.snda.youni.modules.d.b> b2 = com.snda.youni.h.a.a.a().b();
        if (b2 != null && b2.size() > 0) {
            for (com.snda.youni.modules.d.b bVar : b2) {
                h.add(new o(this, "" + bVar.h(), bVar.b(), bVar.a(), bVar.i(), bVar.l(), bVar.d().longValue()));
            }
            if (PreferenceManager.getDefaultSharedPreferences(this).getInt("notify_name", 0) != 1) {
                String b3 = b((o) h.lastElement(), true);
                b.number = b2.size();
                NotificationManager notificationManager = (NotificationManager) getSystemService("notification");
                if (b.contentView == null) {
                    "notif.contentView:" + b3;
                } else {
                    notificationManager.notify(0, b);
                }
                a(((com.snda.youni.modules.d.b) b2.get(b2.size() - 1)).h());
            }
            o.b();
            com.snda.youni.e.b.a(this);
            v.a(this);
        }
        Cursor query = getContentResolver().query(n.f597a, new String[]{"_id", "nick_name"}, "expand_data2='new'", null, null);
        if (query != null && query.getCount() > 0) {
            a(query.getCount(), (query.getCount() != 1 || !query.moveToFirst()) ? null : query.getString(query.getColumnIndex("nick_name")), false);
            YouNi.f170a = true;
        }
        if (query != null) {
            query.close();
        }
        o.b();
        com.snda.youni.e.b.a(this);
        v.a(this);
    }
}
