package com.snda.youni.c;

import android.content.Context;
import android.text.TextUtils;
import com.snda.youni.e.c;
import com.snda.youni.e.d;
import com.snda.youni.e.j;
import com.snda.youni.e.s;
import com.snda.youni.f.a;
import com.snda.youni.f.b;
import java.io.UnsupportedEncodingException;

public final class t {

    /* renamed from: a  reason: collision with root package name */
    private static boolean f387a = true;
    private static String b = "123456hurrayHURRAY!@#$%^";

    private static String a() {
        if (s.c == null) {
            return "";
        }
        if (!f387a) {
            return "numAccount=" + s.c + "&crc=" + "&t=" + System.currentTimeMillis();
        }
        try {
            String a2 = d.a(d.a(b.getBytes("utf-8"), s.c.getBytes("utf-8")));
            return "numAccount=" + a2 + "&crc=" + c.a(s.c + b) + "&t=" + System.currentTimeMillis();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void a(b bVar, a aVar, Context context) {
        String a2 = a();
        if (!TextUtils.isEmpty(a2)) {
            com.snda.youni.f.c cVar = new com.snda.youni.f.c("http://mobile.api.snda.com:8080/iyouni/fl.do?" + a2);
            cVar.a((Object[]) null);
            cVar.a(bVar);
            cVar.b("application/x-www-form-urlencoded");
            String a3 = bVar.a();
            if (a3 != null) {
                try {
                    cVar.a(a3.getBytes("utf-8"));
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
            b bVar2 = new b(context);
            bVar2.a(aVar);
            bVar2.execute(cVar);
        }
    }

    public static void a(d dVar, a aVar, Context context) {
        String a2 = a();
        if (!TextUtils.isEmpty(a2)) {
            com.snda.youni.f.c cVar = new com.snda.youni.f.c("http://mobile.api.snda.com:8080/iyouni/ca.do?" + a2);
            cVar.a((Object[]) null);
            cVar.a(dVar);
            cVar.b();
            String a3 = dVar.a();
            if (a3 != null) {
                try {
                    cVar.a(j.b(a3.getBytes("utf-8")));
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            }
            b bVar = new b(context);
            bVar.a(aVar);
            bVar.execute(cVar);
        }
    }

    public static void a(g gVar, a aVar, Context context) {
        String a2 = a();
        if (!TextUtils.isEmpty(a2)) {
            com.snda.youni.f.c cVar = new com.snda.youni.f.c("http://mobile.api.snda.com:8080/iyouni/uu.do?" + a2);
            cVar.a((Object[]) null);
            cVar.a(gVar);
            cVar.b("application/octet-stream");
            cVar.a(gVar.b());
            b bVar = new b(context);
            bVar.a(aVar);
            bVar.execute(cVar);
        }
    }

    public static void a(h hVar, a aVar, Context context) {
        com.snda.youni.f.c cVar = new com.snda.youni.f.c("http://mobile.api.snda.com:8080/iyouni/ur.do");
        cVar.a((Object[]) null);
        cVar.a(hVar);
        cVar.b("application/x-www-form-urlencoded");
        String a2 = hVar.a();
        if (a2 != null) {
            try {
                cVar.a(a2.getBytes("utf-8"));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
        b bVar = new b(context);
        bVar.a(aVar);
        bVar.execute(cVar);
    }

    public static void a(j jVar, a aVar, Context context) {
        com.snda.youni.f.c cVar = new com.snda.youni.f.c("http://mobile.api.snda.com:8080/iyouni/uf.do?" + a());
        cVar.a((Object[]) null);
        cVar.b("application/x-www-form-urlencoded");
        cVar.a(jVar);
        String a2 = jVar.a();
        "data-----------------------------" + a2;
        if (a2 != null) {
            try {
                cVar.a(a2.getBytes("utf-8"));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
        b bVar = new b(context);
        bVar.a(aVar);
        bVar.execute(cVar);
    }

    public static void a(l lVar, a aVar, Context context) {
        com.snda.youni.f.c cVar = new com.snda.youni.f.c("http://y.sdo.com/smooth/update.htm");
        cVar.a((Object[]) null);
        cVar.a(lVar);
        cVar.b("application/x-www-form-urlencoded");
        String a2 = lVar.a();
        if (a2 != null) {
            try {
                cVar.a(a2.getBytes("utf-8"));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
        b bVar = new b(context);
        bVar.a(aVar);
        bVar.execute(cVar);
    }

    public static void a(o oVar, a aVar, Context context) {
        String a2 = a();
        if (!TextUtils.isEmpty(a2)) {
            com.snda.youni.f.c cVar = new com.snda.youni.f.c("http://mobile.api.snda.com:8080/iyouni/us.do?" + a2);
            cVar.a((Object[]) null);
            cVar.a(oVar);
            cVar.b("application/x-www-form-urlencoded");
            String a3 = oVar.a();
            if (a3 != null) {
                try {
                    cVar.a(a3.getBytes("utf-8"));
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
            b bVar = new b(context);
            bVar.a(aVar);
            bVar.execute(cVar);
        }
    }

    public static void a(Object obj, com.snda.youni.f.d dVar, byte[] bArr) {
        if (d.class.isInstance(obj)) {
            try {
                String str = new String(bArr, "utf-8");
                "s is: " + str;
                if (a(dVar)) {
                    dVar.a(9);
                    return;
                }
                c cVar = new c();
                cVar.a(str);
                if (cVar.b() == 99) {
                    dVar.a(7);
                } else if (cVar.b() == -1) {
                    dVar.a(8);
                }
                dVar.a(cVar);
            } catch (UnsupportedEncodingException e) {
                dVar.a(2);
                e.printStackTrace();
            } catch (com.snda.youni.k.a e2) {
                dVar.a(2);
                e2.printStackTrace();
            }
        } else if (h.class.isInstance(obj)) {
            try {
                String str2 = new String(bArr, "utf-8");
                if (a(dVar)) {
                    dVar.a(9);
                    return;
                }
                r rVar = new r();
                rVar.a(str2);
                if (rVar.b() == 99) {
                    dVar.a(7);
                } else if (rVar.b() == -1) {
                    dVar.a(8);
                }
                dVar.a(rVar);
            } catch (UnsupportedEncodingException e3) {
                dVar.a(2);
                e3.printStackTrace();
            } catch (com.snda.youni.k.a e4) {
                dVar.a(2);
                e4.printStackTrace();
            }
        } else if (b.class.isInstance(obj)) {
            try {
                String str3 = new String(bArr, "utf-8");
                "resp content=" + str3;
                if (a(dVar)) {
                    dVar.a(9);
                    return;
                }
                i iVar = new i();
                iVar.a(str3);
                if (iVar.b() == 99) {
                    dVar.a(7);
                } else if (iVar.b() == -1) {
                    dVar.a(8);
                }
                dVar.a(iVar);
            } catch (UnsupportedEncodingException e5) {
                dVar.a(2);
                e5.printStackTrace();
            } catch (com.snda.youni.k.a e6) {
                dVar.a(2);
                e6.printStackTrace();
            }
        } else if (j.class.isInstance(obj)) {
            try {
                String str4 = new String(bArr, "utf-8");
                "resp content=" + str4;
                if (a(dVar)) {
                    dVar.a(9);
                    return;
                }
                k kVar = new k();
                kVar.a(str4);
                if (kVar.b() == 99) {
                    dVar.a(7);
                } else if (kVar.b() == -1) {
                    dVar.a(8);
                }
                dVar.a(kVar);
            } catch (UnsupportedEncodingException e7) {
                dVar.a(2);
                e7.printStackTrace();
            } catch (com.snda.youni.k.a e8) {
                dVar.a(2);
                e8.printStackTrace();
            }
        } else if (f.class.isInstance(obj)) {
            n nVar = new n();
            nVar.a(bArr);
            dVar.a(nVar);
        } else if (g.class.isInstance(obj)) {
            try {
                String str5 = new String(bArr, "utf-8");
                if (a(dVar)) {
                    dVar.a(9);
                    return;
                }
                m mVar = new m();
                mVar.a(str5);
                if (mVar.b() == 99) {
                    dVar.a(7);
                } else if (mVar.b() == -1) {
                    dVar.a(8);
                }
                dVar.a(mVar);
            } catch (UnsupportedEncodingException e9) {
                dVar.a(2);
                e9.printStackTrace();
            } catch (com.snda.youni.k.a e10) {
                dVar.a(2);
                e10.printStackTrace();
            }
        } else if (o.class.isInstance(obj)) {
            try {
                String str6 = new String(bArr, "utf-8");
                if (a(dVar)) {
                    dVar.a(9);
                    return;
                }
                e eVar = new e();
                eVar.a(str6);
                if (eVar.b() == 99) {
                    dVar.a(7);
                } else if (eVar.b() == -1) {
                    dVar.a(8);
                }
                dVar.a(eVar);
            } catch (UnsupportedEncodingException e11) {
                dVar.a(2);
                e11.printStackTrace();
            } catch (com.snda.youni.k.a e12) {
                dVar.a(2);
                e12.printStackTrace();
            }
        } else if (l.class.isInstance(obj)) {
            try {
                String str7 = new String(bArr, "utf-8");
                "resp content=" + str7;
                if (a(dVar)) {
                    dVar.a(9);
                    return;
                }
                s sVar = new s();
                sVar.a(str7);
                if (sVar.b() == 99) {
                    dVar.a(7);
                } else if (sVar.b() == -1) {
                    dVar.a(8);
                }
                dVar.a(sVar);
            } catch (UnsupportedEncodingException e13) {
                dVar.a(2);
                e13.printStackTrace();
            } catch (com.snda.youni.k.a e14) {
                dVar.a(2);
                e14.printStackTrace();
            }
        } else if (p.class.isInstance(obj)) {
            try {
                String str8 = new String(bArr, "utf-8");
                a aVar = new a();
                aVar.a(str8);
                dVar.a(aVar);
            } catch (UnsupportedEncodingException e15) {
                dVar.a(2);
                e15.printStackTrace();
            } catch (com.snda.youni.k.a e16) {
                dVar.a(2);
                e16.printStackTrace();
            }
        }
    }

    public static void a(String str, f fVar, a aVar, Object[] objArr, Context context) {
        com.snda.youni.f.c cVar = new com.snda.youni.f.c(str);
        cVar.a(objArr);
        cVar.a(fVar);
        cVar.a("GET");
        String a2 = fVar.a();
        if (a2 != null) {
            try {
                cVar.a(a2.getBytes("utf-8"));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
        b bVar = new b(context);
        bVar.a(aVar);
        bVar.execute(cVar);
    }

    private static boolean a(com.snda.youni.f.d dVar) {
        return (dVar.a() == null || (dVar.a().indexOf("text/html") == -1 && dVar.a().indexOf("text/vnd.wap.wml") == -1)) ? false : true;
    }

    public static void b(d dVar, a aVar, Context context) {
        String a2 = a();
        if (!TextUtils.isEmpty(a2)) {
            com.snda.youni.f.c cVar = new com.snda.youni.f.c("http://mobile.api.snda.com:8080/iyouni/ca.do?" + a2 + "&cover=true");
            cVar.a((Object[]) null);
            cVar.a(dVar);
            cVar.b();
            String a3 = dVar.a();
            if (a3 != null) {
                try {
                    cVar.a(j.b(a3.getBytes("utf-8")));
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            }
            b bVar = new b(context);
            bVar.a(aVar);
            bVar.execute(cVar);
        }
    }

    public static void b(j jVar, a aVar, Context context) {
        com.snda.youni.f.c cVar = new com.snda.youni.f.c("http://mobile.api.snda.com:8080/iyouni/ui.do?" + a());
        cVar.a((Object[]) null);
        cVar.b("application/x-www-form-urlencoded");
        cVar.a(jVar);
        cVar.a("GET");
        b bVar = new b(context);
        bVar.a(aVar);
        bVar.execute(cVar);
    }
}
