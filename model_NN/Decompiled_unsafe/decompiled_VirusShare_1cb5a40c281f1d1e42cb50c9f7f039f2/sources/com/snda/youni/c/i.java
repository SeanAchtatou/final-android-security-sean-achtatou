package com.snda.youni.c;

import com.snda.youni.c.a.a;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public final class i extends q {

    /* renamed from: a  reason: collision with root package name */
    private int f377a;
    private String b;
    private int c;
    private List d = new ArrayList();

    public final void a(String str) {
        try {
            JSONObject jSONObject = new JSONObject(h(str));
            this.f377a = jSONObject.getInt("resultCode");
            if (this.f377a == 0) {
                this.b = jSONObject.getString("timeStamp");
                this.c = jSONObject.getInt("type");
                JSONArray jSONArray = jSONObject.getJSONArray("friends");
                int length = jSONArray.length();
                for (int i = 0; i < length; i++) {
                    try {
                        JSONObject jSONObject2 = jSONArray.getJSONObject(i);
                        a aVar = new a();
                        aVar.b(jSONObject2.getString("numAccount"));
                        aVar.c(jSONObject2.getString("nickname"));
                        aVar.d(jSONObject2.getString("signature"));
                        aVar.e(jSONObject2.getString("headImgUrl"));
                        aVar.f(jSONObject2.getString("mobile"));
                        aVar.g(jSONObject2.getString("email"));
                        aVar.a(jSONObject2.getLong("updateTime"));
                        this.d.add(aVar);
                    } catch (Exception e) {
                        e.printStackTrace();
                        "the friend index=" + i;
                    }
                }
            }
        } catch (JSONException e2) {
            e2.printStackTrace();
            throw new com.snda.youni.k.a(e2);
        }
    }

    public final int b() {
        return this.f377a;
    }

    public final String c() {
        return this.b;
    }

    public final List d() {
        return this.d;
    }
}
