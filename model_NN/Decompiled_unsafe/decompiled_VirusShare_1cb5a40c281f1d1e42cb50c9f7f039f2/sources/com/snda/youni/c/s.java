package com.snda.youni.c;

import com.snda.youni.a.m;
import org.json.JSONException;
import org.json.JSONObject;

public final class s extends q {

    /* renamed from: a  reason: collision with root package name */
    private int f386a;
    private m b;

    public final void a(String str) {
        "jsonMessage : " + str;
        m mVar = new m();
        try {
            JSONObject jSONObject = new JSONObject(str);
            this.f386a = jSONObject.getInt("success");
            "resultCode = " + this.f386a;
            if (this.f386a == 0) {
                int i = jSONObject.getInt("updateType");
                if (jSONObject.has("downloadUrl")) {
                    mVar.c = jSONObject.getString("downloadUrl");
                }
                "updateType = " + i + ", updateUrl = " + mVar.c;
                if (mVar.c == null || mVar.c == "" || i == 3) {
                    mVar.f180a = false;
                    this.b = mVar;
                }
                mVar.f180a = true;
                mVar.b = i == 1;
                mVar.d = jSONObject.getString("versionName");
                mVar.f = jSONObject.getString("versionDesc");
                mVar.e = jSONObject.getString("versionCode");
                this.b = mVar;
            }
            if (this.f386a == 1) {
                "UPDATE FAILED reason : " + jSONObject.getString("reason");
            }
            this.b = mVar;
        } catch (JSONException e) {
            e.printStackTrace();
            mVar.f180a = false;
        }
    }

    public final int b() {
        return this.f386a;
    }

    public final m c() {
        return this.b;
    }
}
