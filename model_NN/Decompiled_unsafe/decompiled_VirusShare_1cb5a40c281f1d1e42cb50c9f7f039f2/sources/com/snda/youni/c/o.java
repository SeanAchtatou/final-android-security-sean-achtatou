package com.snda.youni.c;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class o extends q {

    /* renamed from: a  reason: collision with root package name */
    private Map f383a = new HashMap();

    public final String a() {
        StringBuffer stringBuffer = new StringBuffer();
        Iterator it = this.f383a.keySet().iterator();
        while (it.hasNext()) {
            String str = (String) it.next();
            String str2 = (String) this.f383a.get(str);
            if (str2 != null) {
                try {
                    str2 = URLEncoder.encode(str2, "utf-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
            stringBuffer.append(str);
            stringBuffer.append("=");
            stringBuffer.append(str2);
            if (it.hasNext()) {
                stringBuffer.append("&");
            }
        }
        return stringBuffer.toString();
    }

    public final void a(String str, String str2) {
        this.f383a.put(str, str2);
    }
}
