package com.snda.youni.c;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;
import android.view.WindowManager;
import com.snda.youni.a.e;

public class l extends q {

    /* renamed from: a  reason: collision with root package name */
    private String f380a;
    private String b;
    private int c;
    private String d;
    private int e = 2;
    private String f;

    public l(Context context) {
        SharedPreferences defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        this.f380a = defaultSharedPreferences.getString("self_phone_number", "");
        this.b = defaultSharedPreferences.getString("self_product_id", "");
        e eVar = new e(context);
        this.c = eVar.f174a.f175a;
        this.f = eVar.b;
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((WindowManager) context.getSystemService("window")).getDefaultDisplay().getMetrics(displayMetrics);
        this.d = displayMetrics.widthPixels + "-" + displayMetrics.heightPixels;
    }

    public final String a() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("mobile=").append(this.f380a).append("&").append("productId=").append(this.b).append("&").append("versionCode=").append(this.c).append("&").append("clientOSPlatform=").append(this.e).append("&clientOSPlatformVersion=").append(this.f).append("&clientScreenResolution=").append(this.d);
        return stringBuffer.toString();
    }
}
