package com.snda.youni.c;

import com.snda.youni.k.a;
import org.json.JSONException;
import org.json.JSONObject;

public final class r extends q {

    /* renamed from: a  reason: collision with root package name */
    private int f385a;
    private String b;
    private String c;
    private String d;
    private String e;
    private String f;
    private String g;
    private String h;
    private String i;

    public final void a(String str) {
        try {
            JSONObject jSONObject = new JSONObject(h(str));
            this.f385a = jSONObject.getInt("resultCode");
            if (this.f385a == 0) {
                this.b = jSONObject.getString("ptAccount");
                this.c = jSONObject.getString("numAccount");
                this.d = jSONObject.getString("mobile");
                this.e = jSONObject.getString("nickname");
                this.f = jSONObject.getString("signature");
                this.g = jSONObject.getString("headImgUrl");
                this.h = jSONObject.getString("email");
                this.i = jSONObject.getString("updateTime");
            }
        } catch (JSONException e2) {
            throw new a(e2);
        }
    }

    public final int b() {
        return this.f385a;
    }

    public final String c() {
        return this.e;
    }

    public final String d() {
        return this.f;
    }

    public final String e() {
        return this.g;
    }
}
