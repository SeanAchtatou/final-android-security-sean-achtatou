package com.snda.youni.c;

import com.snda.youni.c.a.a;
import org.json.JSONException;
import org.json.JSONObject;

public final class k extends q {

    /* renamed from: a  reason: collision with root package name */
    private int f379a;
    private a b;

    public final void a(String str) {
        try {
            JSONObject jSONObject = new JSONObject(h(str));
            this.f379a = jSONObject.getInt("resultCode");
            if (this.f379a == 0) {
                a aVar = new a();
                aVar.b(jSONObject.getString("numAccount"));
                aVar.c(jSONObject.getString("nickname"));
                aVar.d(jSONObject.getString("signature"));
                aVar.e(jSONObject.getString("headImgUrl"));
                aVar.f(jSONObject.getString("mobile"));
                aVar.g(jSONObject.getString("email"));
                aVar.a(jSONObject.getLong("updateTime"));
            }
        } catch (JSONException e) {
            throw new com.snda.youni.k.a(e);
        }
    }

    public final int b() {
        return this.f379a;
    }

    public final a c() {
        return this.b;
    }
}
