package com.snda.youni;

import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

final class b extends HandlerThread implements Handler.Callback {

    /* renamed from: a  reason: collision with root package name */
    private final ContentResolver f336a;
    private final StringBuilder b = new StringBuilder();
    private final ArrayList c = new ArrayList();
    private Handler d;
    private /* synthetic */ e e;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public b(e eVar, ContentResolver contentResolver) {
        super("ContactNameLoader");
        this.e = eVar;
        this.f336a = contentResolver;
    }

    public final void a() {
        if (this.d == null) {
            this.d = new Handler(getLooper(), this);
        }
        this.d.sendEmptyMessage(0);
    }

    public final boolean handleMessage(Message message) {
        e.a(this.e, this.c);
        int size = this.c.size();
        if (size != 0) {
            this.b.setLength(0);
            this.b.append("thread_id IN(");
            for (int i = 0; i < size; i++) {
                if (i != 0) {
                    this.b.append(',');
                }
                this.b.append('?');
            }
            this.b.append(')');
            this.b.toString();
            Uri parse = Uri.parse("content://sms/");
            HashMap hashMap = new HashMap();
            Cursor query = this.f336a.query(parse, this.e.b, this.b.toString() + " AND " + "type" + "!=" + 3 + ") GROUP BY (" + "thread_id", (String[]) this.c.toArray(e.f392a), null);
            if (query != null) {
                while (query.moveToNext()) {
                    try {
                        Integer valueOf = Integer.valueOf(query.getInt(0));
                        int i2 = query.getInt(1);
                        u uVar = (u) hashMap.get(valueOf);
                        if (uVar == null) {
                            u uVar2 = new u();
                            int unused = uVar2.f621a = i2;
                            hashMap.put(valueOf, uVar2);
                        } else {
                            int unused2 = uVar.f621a = i2;
                        }
                    } finally {
                        query.close();
                    }
                }
            }
            Cursor query2 = this.f336a.query(parse, this.e.b, this.b.toString() + " AND " + "read" + "=0" + " AND " + "type" + "!=" + 3 + ") GROUP BY (" + "thread_id", (String[]) this.c.toArray(e.f392a), null);
            if (query2 != null) {
                while (query2.moveToNext()) {
                    try {
                        Integer valueOf2 = Integer.valueOf(query2.getInt(0));
                        int i3 = query2.getInt(1);
                        u uVar3 = (u) hashMap.get(valueOf2);
                        if (uVar3 == null) {
                            u uVar4 = new u();
                            int unused3 = uVar4.b = i3;
                            hashMap.put(valueOf2, uVar4);
                        } else {
                            int unused4 = uVar3.b = i3;
                        }
                    } finally {
                        query2.close();
                    }
                }
            }
            Cursor query3 = this.f336a.query(parse, this.e.b, this.b.toString() + " AND (" + "protocol" + "='youni' OR " + "protocol" + "='youni_offline')" + ") GROUP BY (" + "thread_id", (String[]) this.c.toArray(e.f392a), null);
            if (query3 != null) {
                while (query3.moveToNext()) {
                    try {
                        Integer valueOf3 = Integer.valueOf(query3.getInt(0));
                        int i4 = query3.getInt(1);
                        u uVar5 = (u) hashMap.get(valueOf3);
                        if (uVar5 == null) {
                            u uVar6 = new u();
                            int unused5 = uVar6.c = i4;
                            hashMap.put(valueOf3, uVar6);
                        } else {
                            int unused6 = uVar5.c = i4;
                        }
                    } finally {
                        query3.close();
                    }
                }
            }
            for (Map.Entry entry : hashMap.entrySet()) {
                this.e.a(((Integer) entry.getKey()).intValue(), (u) entry.getValue());
                this.c.remove(((Integer) entry.getKey()).toString());
                "mNameIds.count=" + this.c.size() + " from database: threadId=" + entry.getKey() + " info=" + ((u) entry.getValue()).toString();
            }
            int size2 = this.c.size();
            for (int i5 = 0; i5 < size2; i5++) {
                "loadNamesFromDatabase " + size2 + " person not in database, nameKey=" + ((String) this.c.get(i5));
                this.e.a(Integer.parseInt((String) this.c.get(i5)), null);
            }
            "loadFromdatabases finish mNameIds.count=" + this.c.size();
        }
        this.e.e.sendEmptyMessage(2);
        return true;
    }
}
