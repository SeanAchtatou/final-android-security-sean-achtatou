package com.snda.youni.j;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import com.snda.youni.C0000R;
import java.util.List;

public final class a extends BaseAdapter {

    /* renamed from: a  reason: collision with root package name */
    private final List f432a;
    private LayoutInflater b;

    public a(Context context, List list) {
        this.f432a = list;
        this.b = (LayoutInflater) context.getSystemService("layout_inflater");
    }

    public final int getCount() {
        return this.f432a.size() % 5 == 0 ? this.f432a.size() : ((this.f432a.size() / 5) + 1) * 5;
    }

    public final Object getItem(int i) {
        if (i < this.f432a.size()) {
            return this.f432a.get(i);
        }
        return null;
    }

    public final long getItemId(int i) {
        return (long) i;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public final View getView(int i, View view, ViewGroup viewGroup) {
        View inflate = view == null ? this.b.inflate((int) C0000R.layout.item_grid_emotion, viewGroup, false) : view;
        ImageView imageView = (ImageView) inflate.findViewById(C0000R.id.item_list_emotion_image);
        imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
        if (i < this.f432a.size()) {
            imageView.setImageDrawable((Drawable) this.f432a.get(i));
        } else {
            imageView.setImageDrawable(null);
        }
        return inflate;
    }
}
