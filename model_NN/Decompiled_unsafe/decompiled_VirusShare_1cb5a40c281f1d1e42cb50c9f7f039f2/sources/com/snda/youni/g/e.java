package com.snda.youni.g;

public final class e {
    /* JADX WARNING: Removed duplicated region for block: B:58:0x0178  */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x017d A[SYNTHETIC, Splitter:B:60:0x017d] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean a(android.content.Context r12, java.lang.String r13, java.lang.String r14, java.lang.String r15) {
        /*
            r11 = 0
            r10 = 1
            r9 = 0
            java.lang.String r0 = "connectivity"
            java.lang.Object r0 = r12.getSystemService(r0)
            android.net.ConnectivityManager r0 = (android.net.ConnectivityManager) r0
            android.net.NetworkInfo r0 = r0.getActiveNetworkInfo()
            if (r0 != 0) goto L_0x0013
            r0 = r9
        L_0x0012:
            return r0
        L_0x0013:
            if (r14 != 0) goto L_0x0017
            r0 = r9
            goto L_0x0012
        L_0x0017:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "upload data:"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.StringBuilder r0 = r0.append(r14)
            r0.toString()
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "Url = "
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.StringBuilder r0 = r0.append(r13)
            r0.toString()
            java.net.URL r0 = new java.net.URL     // Catch:{ MalformedURLException -> 0x0115 }
            r0.<init>(r13)     // Catch:{ MalformedURLException -> 0x0115 }
            java.lang.String r1 = "UTF-8"
            byte[] r1 = r14.getBytes(r1)     // Catch:{ UnsupportedEncodingException -> 0x012f }
            r2 = 4096(0x1000, float:5.74E-42)
            byte[] r2 = new byte[r2]
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.net.URLConnection r0 = r0.openConnection()     // Catch:{ Exception -> 0x01bd, all -> 0x0173 }
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ Exception -> 0x01bd, all -> 0x0173 }
            r4 = 1
            r0.setDoOutput(r4)     // Catch:{ Exception -> 0x01c2, all -> 0x01b1 }
            r4 = 1
            r0.setDoInput(r4)     // Catch:{ Exception -> 0x01c2, all -> 0x01b1 }
            java.lang.String r4 = "POST"
            r0.setRequestMethod(r4)     // Catch:{ Exception -> 0x01c2, all -> 0x01b1 }
            java.lang.String r4 = "Content-Length"
            int r5 = r1.length     // Catch:{ Exception -> 0x01c2, all -> 0x01b1 }
            java.lang.String r5 = java.lang.String.valueOf(r5)     // Catch:{ Exception -> 0x01c2, all -> 0x01b1 }
            r0.setRequestProperty(r4, r5)     // Catch:{ Exception -> 0x01c2, all -> 0x01b1 }
            java.lang.String r4 = "Content-Type"
            java.lang.String r5 = "application/octet-stream"
            r0.setRequestProperty(r4, r5)     // Catch:{ Exception -> 0x01c2, all -> 0x01b1 }
            java.io.OutputStream r4 = r0.getOutputStream()     // Catch:{ Exception -> 0x01c2, all -> 0x01b1 }
            r4.write(r1)     // Catch:{ Exception -> 0x0096, all -> 0x01b6 }
            r4.flush()     // Catch:{ Exception -> 0x0096, all -> 0x01b6 }
            r4.close()     // Catch:{ Exception -> 0x0096, all -> 0x01b6 }
            java.io.InputStream r1 = r0.getInputStream()     // Catch:{ Exception -> 0x0096, all -> 0x01b6 }
        L_0x0084:
            int r5 = r1.read(r2)     // Catch:{ Exception -> 0x0096, all -> 0x01b6 }
            if (r5 <= 0) goto L_0x0149
            java.lang.String r6 = new java.lang.String     // Catch:{ Exception -> 0x0096, all -> 0x01b6 }
            r7 = 0
            java.lang.String r8 = "utf-8"
            r6.<init>(r2, r7, r5, r8)     // Catch:{ Exception -> 0x0096, all -> 0x01b6 }
            r3.append(r6)     // Catch:{ Exception -> 0x0096, all -> 0x01b6 }
            goto L_0x0084
        L_0x0096:
            r1 = move-exception
            r2 = r0
            r0 = r1
            r1 = r4
        L_0x009a:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x01bb }
            r3.<init>()     // Catch:{ all -> 0x01bb }
            java.lang.String r4 = "outputstream = "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x01bb }
            java.lang.String r0 = r0.getMessage()     // Catch:{ all -> 0x01bb }
            java.lang.StringBuilder r0 = r3.append(r0)     // Catch:{ all -> 0x01bb }
            r0.toString()     // Catch:{ all -> 0x01bb }
            if (r2 == 0) goto L_0x00b5
            r2.disconnect()
        L_0x00b5:
            if (r1 == 0) goto L_0x01c8
            r1.close()     // Catch:{ IOException -> 0x016c }
            r0 = r11
        L_0x00bb:
            if (r0 == 0) goto L_0x01ae
            java.lang.String r1 = "1"
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L_0x01ae
            android.content.SharedPreferences r0 = android.preference.PreferenceManager.getDefaultSharedPreferences(r12)
            r1 = 2
            java.text.DateFormat r1 = java.text.DateFormat.getDateInstance(r1)
            java.util.Date r2 = new java.util.Date
            r2.<init>()
            java.lang.String r1 = r1.format(r2)
            java.lang.String r2 = "statistics"
            boolean r2 = r2.equalsIgnoreCase(r15)
            if (r2 == 0) goto L_0x0186
            android.content.Context r2 = r12.getApplicationContext()
            java.lang.String r3 = "Statistics"
            android.content.SharedPreferences r2 = r2.getSharedPreferences(r3, r9)
            android.content.SharedPreferences$Editor r2 = r2.edit()
            android.content.SharedPreferences$Editor r2 = r2.clear()
            r2.commit()
            android.content.SharedPreferences$Editor r0 = r0.edit()
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "upload"
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r1 = r2.append(r1)
            java.lang.String r1 = r1.toString()
            android.content.SharedPreferences$Editor r0 = r0.putBoolean(r1, r10)
            r0.commit()
        L_0x0112:
            r0 = r10
            goto L_0x0012
        L_0x0115:
            r0 = move-exception
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "url = "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r0 = r0.getMessage()
            java.lang.StringBuilder r0 = r1.append(r0)
            r0.toString()
            r0 = r9
            goto L_0x0012
        L_0x012f:
            r0 = move-exception
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "padata : "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r0 = r0.getMessage()
            java.lang.StringBuilder r0 = r1.append(r0)
            r0.toString()
            r0 = r9
            goto L_0x0012
        L_0x0149:
            org.json.JSONObject r1 = new org.json.JSONObject     // Catch:{ Exception -> 0x0096, all -> 0x01b6 }
            java.lang.String r2 = r3.toString()     // Catch:{ Exception -> 0x0096, all -> 0x01b6 }
            r1.<init>(r2)     // Catch:{ Exception -> 0x0096, all -> 0x01b6 }
            java.lang.String r2 = "returnCode"
            java.lang.String r1 = r1.getString(r2)     // Catch:{ Exception -> 0x0096, all -> 0x01b6 }
            if (r0 == 0) goto L_0x015d
            r0.disconnect()
        L_0x015d:
            if (r4 == 0) goto L_0x01cb
            r4.close()     // Catch:{ IOException -> 0x0165 }
            r0 = r1
            goto L_0x00bb
        L_0x0165:
            r0 = move-exception
            r0.printStackTrace()
            r0 = r1
            goto L_0x00bb
        L_0x016c:
            r0 = move-exception
            r0.printStackTrace()
            r0 = r11
            goto L_0x00bb
        L_0x0173:
            r0 = move-exception
            r1 = r11
            r2 = r11
        L_0x0176:
            if (r2 == 0) goto L_0x017b
            r2.disconnect()
        L_0x017b:
            if (r1 == 0) goto L_0x0180
            r1.close()     // Catch:{ IOException -> 0x0181 }
        L_0x0180:
            throw r0
        L_0x0181:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0180
        L_0x0186:
            java.lang.String r2 = "younicount"
            boolean r2 = r2.equalsIgnoreCase(r15)
            if (r2 == 0) goto L_0x0112
            android.content.SharedPreferences$Editor r0 = r0.edit()
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "upload"
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r1 = r2.append(r1)
            java.lang.String r1 = r1.toString()
            android.content.SharedPreferences$Editor r0 = r0.putBoolean(r1, r10)
            r0.commit()
            goto L_0x0112
        L_0x01ae:
            r0 = r9
            goto L_0x0012
        L_0x01b1:
            r1 = move-exception
            r2 = r0
            r0 = r1
            r1 = r11
            goto L_0x0176
        L_0x01b6:
            r1 = move-exception
            r2 = r0
            r0 = r1
            r1 = r4
            goto L_0x0176
        L_0x01bb:
            r0 = move-exception
            goto L_0x0176
        L_0x01bd:
            r0 = move-exception
            r1 = r11
            r2 = r11
            goto L_0x009a
        L_0x01c2:
            r1 = move-exception
            r2 = r0
            r0 = r1
            r1 = r11
            goto L_0x009a
        L_0x01c8:
            r0 = r11
            goto L_0x00bb
        L_0x01cb:
            r0 = r1
            goto L_0x00bb
        */
        throw new UnsupportedOperationException("Method not decompiled: com.snda.youni.g.e.a(android.content.Context, java.lang.String, java.lang.String, java.lang.String):boolean");
    }
}
