package com.snda.youni.g;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.preference.PreferenceManager;
import com.snda.youni.e.c;
import com.snda.youni.e.d;
import com.snda.youni.services.ContactsService;
import com.snda.youni.services.YouniService;
import java.lang.Thread;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.json.JSONException;
import org.json.JSONObject;

public final class f implements Thread.UncaughtExceptionHandler {

    /* renamed from: a  reason: collision with root package name */
    private Thread.UncaughtExceptionHandler f417a = Thread.getDefaultUncaughtExceptionHandler();
    private Context b;

    public f(Context context) {
        this.b = context;
    }

    public final void uncaughtException(Thread thread, Throwable th) {
        String str;
        String string = PreferenceManager.getDefaultSharedPreferences(this.b).getString("self_phone_number", null);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd\tHH:mm:ss");
        Date date = new Date(System.currentTimeMillis());
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.accumulate("Application", "youni");
            jSONObject.accumulate("date", simpleDateFormat.format(date));
            jSONObject.accumulate("mobile", string);
            jSONObject.accumulate("AppVersion", this.b.getPackageManager().getPackageInfo(this.b.getPackageName(), 0).versionName);
            jSONObject.accumulate("OSVersion", Build.VERSION.RELEASE);
            jSONObject.accumulate("Device", Build.MODEL);
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (PackageManager.NameNotFoundException e2) {
            e2.printStackTrace();
        }
        StackTraceElement[] stackTrace = th.getStackTrace();
        String str2 = th.toString() + "\r\n";
        for (int i = 0; i < stackTrace.length; i++) {
            str2 = str + "\tat " + stackTrace[i].toString() + "\r\n";
        }
        Throwable cause = th.getCause();
        if (cause != null) {
            StackTraceElement[] stackTrace2 = cause.getStackTrace();
            str = str + "Cased by: " + cause.toString() + "\r\n";
            for (int i2 = 0; i2 < stackTrace2.length; i2++) {
                str = str + "\tat " + stackTrace2[i2].toString() + "\r\n";
            }
        }
        try {
            jSONObject.accumulate("StackTrace", str);
        } catch (JSONException e3) {
            e3.printStackTrace();
        }
        try {
            String jSONObject2 = jSONObject.toString(4);
            NetworkInfo activeNetworkInfo = ((ConnectivityManager) this.b.getSystemService("connectivity")).getActiveNetworkInfo();
            if (activeNetworkInfo != null && activeNetworkInfo.getTypeName().equals("WIFI")) {
                String str3 = "http://mobile.api.snda.com:8080/iyouni/lu.do?numAccount=" + d.a(d.a("123456hurrayHURRAY!@#$%^".getBytes(), string.getBytes())) + "&crc=" + c.a(string + "123456hurrayHURRAY!@#$%^");
                "Url = " + str3;
                new c(jSONObject2, str3).start();
            }
        } catch (JSONException e4) {
            e4.printStackTrace();
        }
        if (this.b instanceof Service) {
            ((AlarmManager) this.b.getSystemService("alarm")).set(1, System.currentTimeMillis() + 2000, PendingIntent.getService(this.b, 0, this.b instanceof YouniService ? new Intent(this.b, YouniService.class) : new Intent(this.b, ContactsService.class), 0));
            if (this.f417a != null) {
                this.f417a.uncaughtException(thread, th);
            }
        } else if (this.b instanceof Activity) {
            th.getMessage();
            if (this.f417a != null) {
                this.f417a.uncaughtException(thread, th);
            }
        }
    }
}
