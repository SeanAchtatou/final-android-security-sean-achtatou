package com.snda.youni.g;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.preference.PreferenceManager;
import com.snda.youni.a.e;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public final class a {

    /* renamed from: a  reason: collision with root package name */
    private static SimpleDateFormat f413a = new SimpleDateFormat("yyyy-MM-dd-HH");

    public static String a(Context context) {
        HashMap hashMap;
        Set keySet;
        Iterator it;
        e eVar = new e(context);
        JSONObject jSONObject = new JSONObject();
        String string = PreferenceManager.getDefaultSharedPreferences(context).getString("self_phone_number", "");
        if ("".equalsIgnoreCase(string)) {
            return null;
        }
        JSONObject jSONObject2 = new JSONObject();
        try {
            jSONObject2.accumulate("imei", eVar.d);
            jSONObject2.accumulate("version", context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName);
            jSONObject2.accumulate("device", eVar.c);
            jSONObject2.accumulate("phone", string);
            jSONObject2.accumulate("date", Long.valueOf(new Date().getTime()));
        } catch (JSONException e) {
            e.getMessage();
        } catch (PackageManager.NameNotFoundException e2) {
            e2.getMessage();
        }
        JSONArray jSONArray = new JSONArray();
        SharedPreferences sharedPreferences = context.getApplicationContext().getSharedPreferences("Statistics", 0);
        if (sharedPreferences == null || (hashMap = (HashMap) sharedPreferences.getAll()) == null || hashMap.isEmpty() || (keySet = hashMap.keySet()) == null || (it = keySet.iterator()) == null) {
            try {
                jSONObject.put("header", jSONObject2);
                jSONObject.put("body", jSONArray);
            } catch (JSONException e3) {
                e3.getMessage();
            }
            "[" + jSONObject.toString() + "]";
            return "[" + jSONObject.toString() + "]";
        }
        do {
            JSONObject jSONObject3 = new JSONObject();
            String str = (String) it.next();
            try {
                jSONObject3.accumulate(str, hashMap.get(str));
            } catch (JSONException e4) {
                e4.getMessage();
            }
            jSONArray.put(jSONObject3);
        } while (it.hasNext());
        jSONObject.put("header", jSONObject2);
        jSONObject.put("body", jSONArray);
        "[" + jSONObject.toString() + "]";
        return "[" + jSONObject.toString() + "]";
    }

    public static void a(Context context, String str, String str2) {
        a(context, str, str2, 1);
    }

    public static void a(Context context, String str, String str2, int i) {
        SharedPreferences sharedPreferences = context.getApplicationContext().getSharedPreferences("Statistics", 0);
        if (str != null) {
            "".equalsIgnoreCase(str);
        }
        String str3 = str2 == null ? "" : str2;
        String str4 = f413a.format(new Date()) + "|" + ((str3 == null || str3.equalsIgnoreCase("")) ? str : str + "-" + str3);
        sharedPreferences.edit().putInt(str4, sharedPreferences.getInt(str4, 0) + i).commit();
    }

    public static void b(Context context) {
        if (((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo() != null) {
            SharedPreferences defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
            String format = DateFormat.getDateInstance(2).format(new Date());
            String string = defaultSharedPreferences.getString("sts_is_send_sms" + format, "");
            if (!defaultSharedPreferences.getBoolean("upload" + format, false)) {
                new d(context).execute(true);
            } else if ("yes".equalsIgnoreCase(string)) {
                defaultSharedPreferences.edit().putString("sts_is_send_sms" + format, "no").commit();
                new d(context).execute(false);
            }
        }
    }

    public static void b(Context context, String str, String str2) {
        SharedPreferences sharedPreferences = context.getApplicationContext().getSharedPreferences("Statistics", 0);
        if (str != null) {
            "".equalsIgnoreCase(str);
        }
        if (str2 != null) {
            "".equalsIgnoreCase(str2);
        }
        sharedPreferences.edit().putString(f413a.format(new Date()) + "|" + (!"".equalsIgnoreCase("") ? str + "-" + "" : str), str2).commit();
    }
}
