package com.snda.youni.g;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.os.Build;
import android.preference.PreferenceManager;
import com.snda.a.a.a.a;
import java.lang.Thread;
import java.lang.ref.WeakReference;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.json.JSONException;
import org.json.JSONObject;

public final class b implements Thread.UncaughtExceptionHandler {

    /* renamed from: a  reason: collision with root package name */
    private WeakReference f414a;
    private Context b;

    public b(Activity activity) {
        this.f414a = new WeakReference(activity);
    }

    public final void uncaughtException(Thread thread, Throwable th) {
        String str;
        a.a("CrashExceptionHandle", "has error to send");
        Activity activity = (Activity) this.f414a.get();
        if (activity != null) {
            this.b = activity.getApplicationContext();
            String string = PreferenceManager.getDefaultSharedPreferences(activity).getString("self_phone_number", null);
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd\tHH:mm:ss");
            Date date = new Date(System.currentTimeMillis());
            JSONObject jSONObject = new JSONObject();
            try {
                jSONObject.accumulate("date", simpleDateFormat.format(date));
                jSONObject.accumulate("mobile", string);
                jSONObject.accumulate("AppVersion", activity.getPackageManager().getPackageInfo(activity.getPackageName(), 0).versionName);
                jSONObject.accumulate("OSVersion", Build.VERSION.RELEASE);
                jSONObject.accumulate("Device", Build.MODEL);
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (PackageManager.NameNotFoundException e2) {
                e2.printStackTrace();
            }
            StackTraceElement[] stackTrace = th.getStackTrace();
            String str2 = th.toString() + "\r\n";
            for (int i = 0; i < stackTrace.length; i++) {
                str2 = str + "\tat " + stackTrace[i].toString() + "\r\n";
            }
            Throwable cause = th.getCause();
            if (cause != null) {
                StackTraceElement[] stackTrace2 = cause.getStackTrace();
                str = str + "Cased by: " + cause.toString() + "\r\n";
                for (int i2 = 0; i2 < stackTrace2.length; i2++) {
                    str = str + "\tat " + stackTrace2[i2].toString() + "\r\n";
                }
            }
            try {
                jSONObject.accumulate("StackTrace", str);
            } catch (JSONException e3) {
                e3.printStackTrace();
            }
            try {
                String jSONObject2 = jSONObject.toString(4);
                if (((ConnectivityManager) this.b.getSystemService("connectivity")).getActiveNetworkInfo() != null) {
                    new c(jSONObject2, "http://mobile.api.snda.com:8080/iyouni/lu.do?numAccount=" + string).start();
                }
                this.b = null;
            } catch (JSONException e4) {
                e4.printStackTrace();
            }
            Thread.getDefaultUncaughtExceptionHandler().uncaughtException(thread, th);
        }
    }
}
