package com.snda.youni.g;

public final class c extends Thread {
    private static String b = null;

    /* renamed from: a  reason: collision with root package name */
    private String f415a = null;

    public c(String str, String str2) {
        this.f415a = str;
        b = str2;
    }

    /* JADX WARNING: Removed duplicated region for block: B:28:0x008b  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0090 A[SYNTHETIC, Splitter:B:30:0x0090] */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x009e  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x00a3 A[SYNTHETIC, Splitter:B:39:0x00a3] */
    /* JADX WARNING: Removed duplicated region for block: B:56:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void run() {
        /*
            r5 = this;
            r3 = 0
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "errcode= "
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = r5.f415a
            java.lang.StringBuilder r0 = r0.append(r1)
            r0.toString()
            java.net.URL r0 = new java.net.URL     // Catch:{ MalformedURLException -> 0x0072 }
            java.lang.String r1 = com.snda.youni.g.c.b     // Catch:{ MalformedURLException -> 0x0072 }
            r0.<init>(r1)     // Catch:{ MalformedURLException -> 0x0072 }
        L_0x001c:
            java.lang.String r1 = r5.f415a     // Catch:{ UnsupportedEncodingException -> 0x0078 }
            java.lang.String r2 = "UTF-8"
            byte[] r1 = r1.getBytes(r2)     // Catch:{ UnsupportedEncodingException -> 0x0078 }
        L_0x0024:
            java.net.URLConnection r5 = r0.openConnection()     // Catch:{ IOException -> 0x0083, all -> 0x0099 }
            java.net.HttpURLConnection r5 = (java.net.HttpURLConnection) r5     // Catch:{ IOException -> 0x0083, all -> 0x0099 }
            r0 = 1
            r5.setDoOutput(r0)     // Catch:{ IOException -> 0x00b8, all -> 0x00ac }
            java.lang.String r0 = "POST"
            r5.setRequestMethod(r0)     // Catch:{ IOException -> 0x00b8, all -> 0x00ac }
            java.lang.String r0 = "Content-Length"
            int r2 = r1.length     // Catch:{ IOException -> 0x00b8, all -> 0x00ac }
            java.lang.String r2 = java.lang.String.valueOf(r2)     // Catch:{ IOException -> 0x00b8, all -> 0x00ac }
            r5.setRequestProperty(r0, r2)     // Catch:{ IOException -> 0x00b8, all -> 0x00ac }
            java.lang.String r0 = "Content-Type"
            java.lang.String r2 = "application/octet-stream"
            r5.setRequestProperty(r0, r2)     // Catch:{ IOException -> 0x00b8, all -> 0x00ac }
            java.io.OutputStream r0 = r5.getOutputStream()     // Catch:{ IOException -> 0x00b8, all -> 0x00ac }
            r0.write(r1)     // Catch:{ IOException -> 0x00bc, all -> 0x00b0 }
            r0.flush()     // Catch:{ IOException -> 0x00bc, all -> 0x00b0 }
            r0.close()     // Catch:{ IOException -> 0x00bc, all -> 0x00b0 }
            int r1 = r5.getResponseCode()     // Catch:{ IOException -> 0x00bc, all -> 0x00b0 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x00bc, all -> 0x00b0 }
            r2.<init>()     // Catch:{ IOException -> 0x00bc, all -> 0x00b0 }
            java.lang.String r3 = "code is : "
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ IOException -> 0x00bc, all -> 0x00b0 }
            java.lang.StringBuilder r1 = r2.append(r1)     // Catch:{ IOException -> 0x00bc, all -> 0x00b0 }
            r1.toString()     // Catch:{ IOException -> 0x00bc, all -> 0x00b0 }
            if (r5 == 0) goto L_0x006c
            r5.disconnect()
        L_0x006c:
            if (r0 == 0) goto L_0x0071
            r0.close()     // Catch:{ IOException -> 0x007e }
        L_0x0071:
            return
        L_0x0072:
            r0 = move-exception
            r0.printStackTrace()
            r0 = r3
            goto L_0x001c
        L_0x0078:
            r1 = move-exception
            r1.printStackTrace()
            r1 = r3
            goto L_0x0024
        L_0x007e:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0071
        L_0x0083:
            r0 = move-exception
            r1 = r3
            r2 = r3
        L_0x0086:
            r0.printStackTrace()     // Catch:{ all -> 0x00b6 }
            if (r2 == 0) goto L_0x008e
            r2.disconnect()
        L_0x008e:
            if (r1 == 0) goto L_0x0071
            r1.close()     // Catch:{ IOException -> 0x0094 }
            goto L_0x0071
        L_0x0094:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0071
        L_0x0099:
            r0 = move-exception
            r1 = r3
            r2 = r3
        L_0x009c:
            if (r2 == 0) goto L_0x00a1
            r2.disconnect()
        L_0x00a1:
            if (r1 == 0) goto L_0x00a6
            r1.close()     // Catch:{ IOException -> 0x00a7 }
        L_0x00a6:
            throw r0
        L_0x00a7:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x00a6
        L_0x00ac:
            r0 = move-exception
            r1 = r3
            r2 = r5
            goto L_0x009c
        L_0x00b0:
            r1 = move-exception
            r2 = r5
            r4 = r0
            r0 = r1
            r1 = r4
            goto L_0x009c
        L_0x00b6:
            r0 = move-exception
            goto L_0x009c
        L_0x00b8:
            r0 = move-exception
            r1 = r3
            r2 = r5
            goto L_0x0086
        L_0x00bc:
            r1 = move-exception
            r2 = r5
            r4 = r0
            r0 = r1
            r1 = r4
            goto L_0x0086
        */
        throw new UnsupportedOperationException("Method not decompiled: com.snda.youni.g.c.run():void");
    }
}
