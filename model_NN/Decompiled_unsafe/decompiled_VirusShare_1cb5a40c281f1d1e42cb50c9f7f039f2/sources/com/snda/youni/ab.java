package com.snda.youni;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;

final class ab implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ YouNi f182a;

    ab(YouNi youNi) {
        this.f182a = youNi;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.addCategory("android.intent.category.BROWSABLE");
        intent.setDataAndType(Uri.parse("http://y.sdo.com"), "text/html");
        this.f182a.startActivity(intent);
    }
}
