package com.snda.youni;

import android.content.ContentValues;
import android.content.DialogInterface;
import android.database.Cursor;
import android.widget.Toast;
import com.snda.youni.e.j;
import com.snda.youni.providers.l;
import com.snda.youni.providers.n;

final class y implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ YouNi f625a;

    y(YouNi youNi) {
        this.f625a = youNi;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        Cursor query = this.f625a.getContentResolver().query(l.f596a, new String[]{"blacker_rid"}, "blacker_rid='" + this.f625a.k.f492a.n + "'", null, null);
        if (query.getCount() > 0) {
            Toast.makeText(this.f625a, this.f625a.getString(C0000R.string.add_duplicate_person_to_black_list, new Object[]{this.f625a.k.f492a.f496a}), 0).show();
            query.close();
            return;
        }
        query.close();
        String a2 = j.a(this.f625a.k.f492a.g);
        Cursor query2 = this.f625a.getContentResolver().query(n.f597a, new String[]{"phone_number"}, "sid='" + a2 + "'", null, null);
        String str = this.f625a.k.f492a.g;
        while (query2.moveToNext()) {
            str = query2.getString(0);
        }
        ContentValues contentValues = new ContentValues();
        contentValues.put("blacker_rid", this.f625a.k.f492a.n);
        contentValues.put("blacker_name", this.f625a.k.f492a.f496a);
        contentValues.put("blacker_phone", str);
        contentValues.put("blacker_sid", a2);
        if (this.f625a.getContentResolver().insert(l.f596a, contentValues) != null) {
            Toast.makeText(this.f625a, (int) C0000R.string.add_to_black_list_succeed, 0).show();
        } else {
            Toast.makeText(this.f625a, (int) C0000R.string.add_to_black_list_failed, 0).show();
        }
        "ljd Add " + this.f625a.k.f492a.n + " to black list";
    }
}
