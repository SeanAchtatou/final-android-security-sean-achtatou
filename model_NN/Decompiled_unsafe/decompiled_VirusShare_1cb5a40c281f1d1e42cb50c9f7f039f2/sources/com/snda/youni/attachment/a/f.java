package com.snda.youni.attachment.a;

import android.content.ContentValues;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import com.snda.youni.C0000R;
import com.snda.youni.e.d;
import com.snda.youni.e.e;
import com.snda.youni.e.q;
import com.snda.youni.e.s;
import com.snda.youni.e.t;
import com.snda.youni.h.a.a;
import com.snda.youni.modules.d.b;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public final class f implements h {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public Handler f322a;
    private Context b;
    private long c = 0;
    /* access modifiers changed from: private */
    public b d;

    public f(Context context, Handler handler) {
        this.b = context;
        this.f322a = handler;
    }

    static /* synthetic */ void a(f fVar, b bVar) {
        com.snda.youni.attachment.b.b bVar2;
        String str;
        if (bVar.r()) {
            long[] jArr = new long[bVar.q()];
            long[] jArr2 = new long[bVar.q()];
            String[] o = bVar.o();
            com.snda.youni.attachment.b.b s = bVar.s();
            for (int i = 0; i < o.length; i++) {
                if (o[i] != null) {
                    b t = bVar.clone();
                    t.a(q.a(t.a(o[i])));
                    if (i != 0) {
                        String hexString = Long.toHexString(Double.doubleToLongBits(Math.random()));
                        if (s.g().endsWith("img")) {
                            str = hexString + ".jpg";
                            e.a(t.c(), com.snda.youni.attachment.e.i, str, com.snda.youni.attachment.e.i);
                            e.a(t.c(), com.snda.youni.attachment.e.h, str, com.snda.youni.attachment.e.h);
                        } else {
                            str = hexString + "_" + s.n() + "s.amr";
                            e.a(t.c(), com.snda.youni.attachment.e.j, str, com.snda.youni.attachment.e.j);
                        }
                        t.c(str);
                        s.b(str);
                    }
                    String a2 = a.a().a(t);
                    jArr[i] = Long.parseLong(a2);
                    s.b(Long.parseLong(a2));
                    if (i == 0) {
                        com.snda.youni.attachment.b.a.b(fVar.b, s);
                    }
                    if (i != 0) {
                        com.snda.youni.attachment.b.a.a(fVar.b, s);
                    }
                    jArr2[i] = s.a();
                }
            }
            s.c();
            s.a(jArr);
            s.b(jArr2);
            bVar.a(jArr);
            bVar2 = s;
        } else if (bVar.l() == 0) {
            String a3 = a.a().a(bVar);
            bVar.b(Long.parseLong(a3));
            com.snda.youni.attachment.b.b s2 = bVar.s();
            s2.b(Long.parseLong(a3));
            com.snda.youni.attachment.b.a.b(fVar.b, s2);
            bVar2 = s2;
        } else {
            return;
        }
        bVar2.b(1);
        bVar2.c(1);
        bVar.a(bVar2);
    }

    private void a(com.snda.youni.attachment.b.b bVar, boolean z) {
        com.snda.youni.attachment.b.a.b(this.b, bVar);
        if (!z) {
            ContentValues contentValues = new ContentValues();
            contentValues.put("service_center", "+7777777");
            this.b.getContentResolver().update(Uri.parse("content://sms/" + bVar.d()), contentValues, null, null);
            return;
        }
        long d2 = bVar.d();
        String j = bVar.j();
        b d3 = a.a().d("" + d2);
        if (d3 != null) {
            if (bVar.g().equals("audio")) {
                d3.b("<" + j + ">" + d3.b() + this.b.getString(C0000R.string.audio_attachment_sms_text));
            } else {
                d3.b("[" + j + "]" + d3.b() + this.b.getString(C0000R.string.image_attachment_sms_text));
            }
            d3.a(bVar);
        }
        if (d3 != null) {
            ContentValues contentValues2 = new ContentValues();
            contentValues2.put("service_center", "");
            contentValues2.put("body", d3.b());
            this.b.getContentResolver().update(Uri.parse("content://sms/" + bVar.d()), contentValues2, null, null);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.snda.youni.attachment.a.f.a(com.snda.youni.attachment.b.b, boolean):void
     arg types: [com.snda.youni.attachment.b.b, int]
     candidates:
      com.snda.youni.attachment.a.f.a(com.snda.youni.attachment.a.f, com.snda.youni.modules.d.b):void
      com.snda.youni.attachment.a.f.a(com.snda.youni.attachment.b.b, boolean):void */
    static /* synthetic */ void b(f fVar, b bVar) {
        com.snda.youni.attachment.b.b s;
        if (!bVar.r()) {
            s = bVar.s();
            fVar.a(s, false);
        } else {
            s = bVar.s();
            long[] e = s.e();
            long[] f = s.f();
            for (int i = 0; i < e.length; i++) {
                s.b(e[i]);
                s.a(f[i]);
                fVar.a(s, false);
            }
        }
        bVar.a(s);
    }

    public final void a(long j) {
        int i = (int) ((100 * j) / this.c);
        if (i > 100) {
            i = 100;
        }
        Message obtainMessage = this.f322a.obtainMessage();
        Bundle bundle = new Bundle();
        bundle.putSerializable("messageobject", this.d);
        obtainMessage.setData(bundle);
        obtainMessage.what = 0;
        obtainMessage.arg1 = i;
        obtainMessage.sendToTarget();
    }

    public final void a(b bVar) {
        this.d = bVar;
        new i(this).execute(new Void[0]);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.snda.youni.attachment.a.f.a(com.snda.youni.attachment.b.b, boolean):void
     arg types: [com.snda.youni.attachment.b.b, int]
     candidates:
      com.snda.youni.attachment.a.f.a(com.snda.youni.attachment.a.f, com.snda.youni.modules.d.b):void
      com.snda.youni.attachment.a.f.a(com.snda.youni.attachment.b.b, boolean):void */
    /* access modifiers changed from: package-private */
    public final boolean a(com.snda.youni.attachment.b.b bVar) {
        String str;
        HttpResponse execute;
        String h = bVar.h();
        BasicHttpParams basicHttpParams = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(basicHttpParams, 10000);
        HttpConnectionParams.setSoTimeout(basicHttpParams, 30000);
        DefaultHttpClient defaultHttpClient = new DefaultHttpClient(basicHttpParams);
        BasicHttpContext basicHttpContext = new BasicHttpContext();
        HttpPost httpPost = new HttpPost("http://img.y.sdo.com/iaudio/addAudio?t=amr&d=" + bVar.n());
        String str2 = s.f403a;
        String str3 = "";
        if (!TextUtils.isEmpty(str2)) {
            str3 = d.a(d.a("123456hurrayHURRAY!@#$%^".getBytes(), str2.getBytes()));
        }
        if (!bVar.b()) {
            String a2 = a.a().d("" + bVar.d()).a();
            bVar.a(false);
            str = d.a(d.a("123456hurrayHURRAY!@#$%^".getBytes(), a2.getBytes()));
            "encoded send and receiver number is " + str3 + " " + str;
        } else {
            long[] e = bVar.e();
            boolean[] zArr = new boolean[e.length];
            int i = 0;
            int length = e.length;
            String str4 = "";
            int i2 = 0;
            while (i2 < length) {
                long j = e[i2];
                zArr[i] = false;
                i2++;
                str4 = str4 + d.a(d.a("123456hurrayHURRAY!@#$%^".getBytes(), a.a().d("" + j).a().getBytes())) + "|";
                i++;
            }
            bVar.a(zArr);
            str = (String) str4.subSequence(0, str4.length() - 1);
        }
        this.c = e.d(h, com.snda.youni.attachment.e.j);
        e eVar = new e(this);
        try {
            eVar.a("s", new org.a.a.a.a.a.b(str3));
            eVar.a("r", new org.a.a.a.a.a.b(str));
            eVar.a("au", new org.a.a.a.a.a.e(new File(com.snda.youni.attachment.e.j, h)));
            httpPost.setEntity(eVar);
            try {
                execute = defaultHttpClient.execute(httpPost, basicHttpContext);
                execute.getStatusLine().getStatusCode();
            } catch (ClientProtocolException e2) {
                e2.printStackTrace();
                bVar.b(3);
            } catch (IOException e3) {
                e3.printStackTrace();
                bVar.b(3);
            }
            try {
                JSONArray jSONArray = new JSONArray(EntityUtils.toString(execute.getEntity()));
                int length2 = jSONArray.length();
                for (int i3 = 0; i3 < length2; i3++) {
                    JSONObject jSONObject = jSONArray.getJSONObject(0);
                    String string = jSONObject.getString("shortUrl");
                    String string2 = jSONObject.getString("longUrl");
                    if (!(i3 == 0 && length2 == 1)) {
                        bVar.b(bVar.e()[i3]);
                    }
                    bVar.f(string);
                    bVar.d(string2);
                    bVar.b(2);
                    bVar.c(2);
                    if (length2 != 1) {
                        bVar.a(bVar.f()[i3]);
                    }
                    if (!string.startsWith("http://") || !string2.startsWith("http://")) {
                        a(bVar, false);
                    } else {
                        if (length2 == 1) {
                            bVar.a(true);
                        } else {
                            bVar.a(i3);
                        }
                        a(bVar, true);
                    }
                }
                return true;
            } catch (JSONException e4) {
                bVar.b(3);
                e4.printStackTrace();
                return false;
            }
        } catch (UnsupportedEncodingException e5) {
            e5.printStackTrace();
            bVar.b(3);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.snda.youni.attachment.a.f.a(com.snda.youni.attachment.b.b, boolean):void
     arg types: [com.snda.youni.attachment.b.b, int]
     candidates:
      com.snda.youni.attachment.a.f.a(com.snda.youni.attachment.a.f, com.snda.youni.modules.d.b):void
      com.snda.youni.attachment.a.f.a(com.snda.youni.attachment.b.b, boolean):void */
    /* access modifiers changed from: package-private */
    public final boolean b(com.snda.youni.attachment.b.b bVar) {
        String str;
        HttpResponse execute;
        String h = bVar.h();
        String substring = h.substring(0, h.length() - 5);
        BasicHttpParams basicHttpParams = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(basicHttpParams, 10000);
        HttpConnectionParams.setSoTimeout(basicHttpParams, 30000);
        DefaultHttpClient defaultHttpClient = new DefaultHttpClient(basicHttpParams);
        BasicHttpContext basicHttpContext = new BasicHttpContext();
        HttpPost httpPost = new HttpPost("http://img.y.sdo.com/ImgServer/add?id=" + substring + "&type=jpg");
        String str2 = s.f403a;
        String str3 = "";
        if (!TextUtils.isEmpty(str2)) {
            str3 = d.a(d.a("123456hurrayHURRAY!@#$%^".getBytes(), str2.getBytes()));
        }
        if (!bVar.b()) {
            bVar.a(false);
            str = d.a(d.a("123456hurrayHURRAY!@#$%^".getBytes(), a.a().d("" + bVar.d()).a().getBytes()));
            "encoded send and receiver number is " + str3 + " " + str;
        } else {
            long[] e = bVar.e();
            boolean[] zArr = new boolean[e.length];
            int i = 0;
            int length = e.length;
            String str4 = "";
            int i2 = 0;
            while (i2 < length) {
                long j = e[i2];
                zArr[i] = false;
                i2++;
                str4 = str4 + d.a(d.a("123456hurrayHURRAY!@#$%^".getBytes(), a.a().d("" + j).a().getBytes())) + "|";
                i++;
            }
            str = (String) str4.subSequence(0, str4.length() - 1);
            bVar.a(zArr);
        }
        this.c = e.d(h, com.snda.youni.attachment.e.h);
        e eVar = new e(this);
        try {
            eVar.a("s", new org.a.a.a.a.a.b(str3));
            eVar.a("r", new org.a.a.a.a.a.b(str));
            eVar.a("img", new org.a.a.a.a.a.e(new File(com.snda.youni.attachment.e.h, h)));
            httpPost.setEntity(eVar);
            try {
                execute = defaultHttpClient.execute(httpPost, basicHttpContext);
                execute.getStatusLine().getStatusCode();
            } catch (ClientProtocolException e2) {
                e2.printStackTrace();
                bVar.b(3);
            } catch (IOException e3) {
                e3.printStackTrace();
                bVar.b(3);
            }
            try {
                JSONArray jSONArray = new JSONArray(EntityUtils.toString(execute.getEntity()));
                int length2 = jSONArray.length();
                for (int i3 = 0; i3 < length2; i3++) {
                    JSONObject jSONObject = jSONArray.getJSONObject(0);
                    String string = jSONObject.getString("shortUrl");
                    String string2 = jSONObject.getString("longUrl");
                    if (!(i3 == 0 && length2 == 1)) {
                        bVar.b(bVar.e()[i3]);
                    }
                    bVar.f(string);
                    bVar.g(string2);
                    if (length2 != 1) {
                        bVar.a(bVar.f()[i3]);
                    }
                    int lastIndexOf = string2.lastIndexOf("_small.jp");
                    int i4 = lastIndexOf + 6;
                    if (lastIndexOf > 0) {
                        bVar.d(string2.substring(0, lastIndexOf) + string2.substring(i4));
                    } else {
                        bVar.d(string2);
                    }
                    bVar.b(2);
                    bVar.c(2);
                    if (!string.startsWith("http://") || !string2.startsWith("http://")) {
                        a(bVar, false);
                    } else {
                        if (length2 == 1) {
                            bVar.a(true);
                        } else {
                            bVar.a(i3);
                        }
                        a(bVar, true);
                    }
                }
                return true;
            } catch (JSONException e4) {
                bVar.b(3);
                e4.printStackTrace();
                return false;
            }
        } catch (UnsupportedEncodingException e5) {
            e5.printStackTrace();
            bVar.b(3);
        }
    }
}
