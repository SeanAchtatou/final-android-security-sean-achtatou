package com.snda.youni.attachment.a;

import android.os.AsyncTask;

final class c extends AsyncTask {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ j f319a;

    /* synthetic */ c(j jVar) {
        this(jVar, (byte) 0);
    }

    private c(j jVar, byte b) {
        this.f319a = jVar;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x002e, code lost:
        if (r0.b() == null) goto L_0x004f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x003a, code lost:
        if (r0.b().startsWith("http://n.sdo.com/") == false) goto L_0x004f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0040, code lost:
        if (r0.c() != 0) goto L_0x0068;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0042, code lost:
        com.snda.youni.attachment.a.j.a(r7.f319a, r0.b(), r0.a());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x004f, code lost:
        r0 = r7.f319a.e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0055, code lost:
        monitor-enter(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:?, code lost:
        r7.f319a.e.remove(0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0060, code lost:
        monitor-exit(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x006c, code lost:
        if (r0.c() != 1) goto L_0x004f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x006e, code lost:
        com.snda.youni.attachment.a.j.b(r7.f319a, r0.b(), r0.a());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x007c, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x0083, code lost:
        monitor-enter(r7.f319a.e);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:?, code lost:
        r7.f319a.e.remove(0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x008f, code lost:
        r0.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:?, code lost:
        return false;
     */
    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.lang.Boolean a() {
        /*
            r7 = this;
            r6 = 1
            r5 = 0
        L_0x0002:
            com.snda.youni.attachment.a.j r0 = r7.f319a
            java.util.ArrayList r1 = r0.e
            monitor-enter(r1)
            com.snda.youni.attachment.a.j r0 = r7.f319a     // Catch:{ all -> 0x0065 }
            java.util.ArrayList r0 = r0.e     // Catch:{ all -> 0x0065 }
            boolean r0 = r0.isEmpty()     // Catch:{ all -> 0x0065 }
            if (r0 == 0) goto L_0x001c
            r0 = 1
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)     // Catch:{ all -> 0x0065 }
            monitor-exit(r1)     // Catch:{ all -> 0x0065 }
        L_0x001b:
            return r0
        L_0x001c:
            com.snda.youni.attachment.a.j r0 = r7.f319a     // Catch:{ all -> 0x0065 }
            java.util.ArrayList r0 = r0.e     // Catch:{ all -> 0x0065 }
            r2 = 0
            java.lang.Object r0 = r0.get(r2)     // Catch:{ all -> 0x0065 }
            com.snda.youni.attachment.a.d r0 = (com.snda.youni.attachment.a.d) r0     // Catch:{ all -> 0x0065 }
            monitor-exit(r1)     // Catch:{ all -> 0x0065 }
            java.lang.String r1 = r0.b()     // Catch:{ Exception -> 0x007c }
            if (r1 == 0) goto L_0x004f
            java.lang.String r1 = r0.b()     // Catch:{ Exception -> 0x007c }
            java.lang.String r2 = "http://n.sdo.com/"
            boolean r1 = r1.startsWith(r2)     // Catch:{ Exception -> 0x007c }
            if (r1 == 0) goto L_0x004f
            int r1 = r0.c()     // Catch:{ Exception -> 0x007c }
            if (r1 != 0) goto L_0x0068
            com.snda.youni.attachment.a.j r1 = r7.f319a     // Catch:{ Exception -> 0x007c }
            java.lang.String r2 = r0.b()     // Catch:{ Exception -> 0x007c }
            long r3 = r0.a()     // Catch:{ Exception -> 0x007c }
            com.snda.youni.attachment.a.j.a(r1, r2, r3)     // Catch:{ Exception -> 0x007c }
        L_0x004f:
            com.snda.youni.attachment.a.j r0 = r7.f319a
            java.util.ArrayList r0 = r0.e
            monitor-enter(r0)
            com.snda.youni.attachment.a.j r1 = r7.f319a     // Catch:{ all -> 0x0062 }
            java.util.ArrayList r1 = r1.e     // Catch:{ all -> 0x0062 }
            r2 = 0
            r1.remove(r2)     // Catch:{ all -> 0x0062 }
            monitor-exit(r0)     // Catch:{ all -> 0x0062 }
            goto L_0x0002
        L_0x0062:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x0062 }
            throw r1
        L_0x0065:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0065 }
            throw r0
        L_0x0068:
            int r1 = r0.c()     // Catch:{ Exception -> 0x007c }
            if (r1 != r6) goto L_0x004f
            com.snda.youni.attachment.a.j r1 = r7.f319a     // Catch:{ Exception -> 0x007c }
            java.lang.String r2 = r0.b()     // Catch:{ Exception -> 0x007c }
            long r3 = r0.a()     // Catch:{ Exception -> 0x007c }
            com.snda.youni.attachment.a.j.b(r1, r2, r3)     // Catch:{ Exception -> 0x007c }
            goto L_0x004f
        L_0x007c:
            r0 = move-exception
            com.snda.youni.attachment.a.j r1 = r7.f319a
            java.util.ArrayList r1 = r1.e
            monitor-enter(r1)
            com.snda.youni.attachment.a.j r2 = r7.f319a     // Catch:{ all -> 0x0097 }
            java.util.ArrayList r2 = r2.e     // Catch:{ all -> 0x0097 }
            r3 = 0
            r2.remove(r3)     // Catch:{ all -> 0x0097 }
            monitor-exit(r1)     // Catch:{ all -> 0x0097 }
            r0.printStackTrace()
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r5)
            goto L_0x001b
        L_0x0097:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0097 }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.snda.youni.attachment.a.c.a():java.lang.Boolean");
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ Object doInBackground(Object[] objArr) {
        return a();
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void onPostExecute(Object obj) {
        super.onPostExecute((Boolean) obj);
        c unused = this.f319a.f325a = null;
    }
}
