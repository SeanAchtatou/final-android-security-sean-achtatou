package com.snda.youni.attachment.b;

import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import com.snda.youni.providers.b;

public final class a {
    public static Uri a(Context context, b bVar) {
        Uri insert = context.getContentResolver().insert(b.f592a, bVar.q());
        bVar.a(Long.parseLong(insert.getPathSegments().get(1)));
        return insert;
    }

    public static b a(Context context, String str) {
        b bVar;
        Cursor query = context.getContentResolver().query(b.f592a, null, "thumbnail_short_url=?", new String[]{str}, null);
        if (query == null) {
            return null;
        }
        if (query.moveToNext()) {
            bVar = new b(query);
            bVar.a(Long.parseLong(query.getString(query.getColumnIndex("_id"))));
        } else {
            bVar = null;
        }
        query.close();
        return bVar;
    }

    public static int b(Context context, b bVar) {
        return context.getContentResolver().update(ContentUris.withAppendedId(b.f592a, bVar.a()), bVar.q(), null, null);
    }

    public static b b(Context context, String str) {
        b bVar;
        Cursor query = context.getContentResolver().query(b.f592a, null, "message_id=?", new String[]{str}, null);
        if (query == null) {
            return null;
        }
        if (query.moveToNext()) {
            bVar = new b(query);
            bVar.a(Long.parseLong(query.getString(query.getColumnIndex("_id"))));
        } else {
            bVar = null;
        }
        query.close();
        return bVar;
    }
}
