package com.snda.youni.attachment.a;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Message;
import com.snda.youni.attachment.b.b;

final class i extends AsyncTask {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ f f324a;

    /* synthetic */ i(f fVar) {
        this(fVar, (byte) 0);
    }

    private i(f fVar, byte b) {
        this.f324a = fVar;
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ Object doInBackground(Object[] objArr) {
        b s = this.f324a.d.s();
        f.a(this.f324a, this.f324a.d);
        boolean a2 = s.g().equals("audio") ? this.f324a.a(s) : this.f324a.b(s);
        if (!a2) {
            f.b(this.f324a, this.f324a.d);
        }
        return Boolean.valueOf(a2);
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void onPostExecute(Object obj) {
        if (((Boolean) obj).booleanValue()) {
            Message obtainMessage = this.f324a.f322a.obtainMessage();
            Bundle bundle = new Bundle();
            bundle.putSerializable("messageobject", this.f324a.d);
            obtainMessage.setData(bundle);
            obtainMessage.what = 1;
            obtainMessage.sendToTarget();
        }
    }
}
