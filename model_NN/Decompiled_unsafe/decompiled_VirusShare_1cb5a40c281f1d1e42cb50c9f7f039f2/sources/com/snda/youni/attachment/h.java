package com.snda.youni.attachment;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import com.snda.youni.e.p;
import java.lang.ref.SoftReference;

public final class h {

    /* renamed from: a  reason: collision with root package name */
    private Context f332a;
    private Uri b;
    private String c;
    private SoftReference d = new SoftReference(null);

    public h(Context context, String str) {
        this.f332a = context;
        this.c = str;
    }

    public final Bitmap a() {
        Bitmap bitmap;
        Bitmap bitmap2;
        Bitmap bitmap3 = (Bitmap) this.d.get();
        if (bitmap3 != null) {
            return bitmap3;
        }
        if (this.b != null) {
            Cursor query = this.f332a.getContentResolver().query(this.b, new String[]{"thumbnail_local_path"}, null, null, null);
            if (query != null) {
                if (query.moveToNext()) {
                    bitmap2 = p.a(query.getString(0));
                    this.d = new SoftReference(bitmap2);
                } else {
                    bitmap2 = null;
                }
                query.close();
                bitmap = bitmap2;
            } else {
                bitmap = null;
            }
            this.d = new SoftReference(bitmap);
        } else if (this.c != null) {
            this.d = new SoftReference(p.a(this.c, e.i));
        }
        return (Bitmap) this.d.get();
    }
}
