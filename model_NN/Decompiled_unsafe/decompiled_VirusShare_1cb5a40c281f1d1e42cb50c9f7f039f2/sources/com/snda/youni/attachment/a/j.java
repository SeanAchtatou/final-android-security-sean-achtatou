package com.snda.youni.attachment.a;

import android.content.ContentValues;
import android.content.Context;
import android.net.Uri;
import android.os.Handler;
import com.snda.youni.attachment.b.a;
import com.snda.youni.attachment.b.b;
import com.snda.youni.attachment.e;
import com.snda.youni.e.h;
import com.snda.youni.e.o;
import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Iterator;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

public final class j {
    /* access modifiers changed from: private */
    public static Context b;
    private static j c = null;
    private static Handler d;
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public c f325a = null;
    /* access modifiers changed from: private */
    public final ArrayList e = new ArrayList();

    private j(Context context) {
        b = context;
    }

    public static j a() {
        if (c != null) {
            return c;
        }
        throw new IllegalStateException("Uninitialized.");
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x007c A[Catch:{ IOException -> 0x0046 }] */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x00ba A[Catch:{ IOException -> 0x0046 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.lang.String a(java.lang.String r8, java.lang.String r9, int r10, int r11) {
        /*
            r7 = this;
            r6 = 0
            boolean r0 = com.snda.youni.e.o.a()
            if (r0 != 0) goto L_0x0013
            android.os.Handler r0 = com.snda.youni.attachment.a.j.d
            com.snda.youni.attachment.a.b r1 = new com.snda.youni.attachment.a.b
            r1.<init>(r7)
            r0.post(r1)
            r0 = r6
        L_0x0012:
            return r0
        L_0x0013:
            java.net.URL r0 = new java.net.URL     // Catch:{ IOException -> 0x0046 }
            r0.<init>(r8)     // Catch:{ IOException -> 0x0046 }
            java.net.URLConnection r0 = r0.openConnection()     // Catch:{ IOException -> 0x0046 }
            java.lang.String r1 = "User-Agent-YN"
            java.lang.String r2 = com.snda.youni.e.h.a()     // Catch:{ IOException -> 0x0046 }
            r0.addRequestProperty(r1, r2)     // Catch:{ IOException -> 0x0046 }
            java.lang.String r1 = r0.getContentType()     // Catch:{ IOException -> 0x0046 }
            java.io.InputStream r0 = r0.getInputStream()     // Catch:{ IOException -> 0x0046 }
            java.io.BufferedInputStream r2 = new java.io.BufferedInputStream     // Catch:{ IOException -> 0x0046 }
            r2.<init>(r0)     // Catch:{ IOException -> 0x0046 }
            java.io.ByteArrayOutputStream r0 = new java.io.ByteArrayOutputStream     // Catch:{ IOException -> 0x0046 }
            r0.<init>()     // Catch:{ IOException -> 0x0046 }
            r3 = 1024(0x400, float:1.435E-42)
            byte[] r3 = new byte[r3]     // Catch:{ IOException -> 0x0046 }
        L_0x003b:
            int r4 = r2.read(r3)     // Catch:{ IOException -> 0x0046 }
            if (r4 <= 0) goto L_0x005e
            r5 = 0
            r0.write(r3, r5, r4)     // Catch:{ IOException -> 0x0046 }
            goto L_0x003b
        L_0x0046:
            r0 = move-exception
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Error: "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r0)
            r1.toString()
            r0.printStackTrace()
            r0 = r6
            goto L_0x0012
        L_0x005e:
            byte[] r0 = r0.toByteArray()     // Catch:{ IOException -> 0x0046 }
            double r2 = java.lang.Math.random()     // Catch:{ IOException -> 0x0046 }
            long r2 = java.lang.Double.doubleToLongBits(r2)     // Catch:{ IOException -> 0x0046 }
            java.lang.String r2 = java.lang.Long.toHexString(r2)     // Catch:{ IOException -> 0x0046 }
            if (r1 == 0) goto L_0x00e5
            java.lang.String r3 = "image/jpeg"
            boolean r3 = r1.startsWith(r3)     // Catch:{ IOException -> 0x0046 }
            if (r3 == 0) goto L_0x00a4
            java.lang.String r1 = ".jpg"
        L_0x007a:
            if (r10 != 0) goto L_0x00ba
            if (r1 == 0) goto L_0x0080
            java.lang.String r1 = ".jpg"
        L_0x0080:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0046 }
            r3.<init>()     // Catch:{ IOException -> 0x0046 }
            java.lang.StringBuilder r2 = r3.append(r2)     // Catch:{ IOException -> 0x0046 }
            java.lang.StringBuilder r1 = r2.append(r1)     // Catch:{ IOException -> 0x0046 }
            java.lang.String r1 = r1.toString()     // Catch:{ IOException -> 0x0046 }
        L_0x0091:
            java.io.File r2 = new java.io.File     // Catch:{ IOException -> 0x0046 }
            r2.<init>(r9, r1)     // Catch:{ IOException -> 0x0046 }
            java.io.FileOutputStream r3 = new java.io.FileOutputStream     // Catch:{ IOException -> 0x0046 }
            r3.<init>(r2)     // Catch:{ IOException -> 0x0046 }
            r3.write(r0)     // Catch:{ IOException -> 0x0046 }
            r3.close()     // Catch:{ IOException -> 0x0046 }
            r0 = r1
            goto L_0x0012
        L_0x00a4:
            java.lang.String r3 = "image/jpeg"
            boolean r3 = r1.startsWith(r3)     // Catch:{ IOException -> 0x0046 }
            if (r3 == 0) goto L_0x00af
            java.lang.String r1 = ".jpg"
            goto L_0x007a
        L_0x00af:
            java.lang.String r3 = "audio/"
            boolean r1 = r1.startsWith(r3)     // Catch:{ IOException -> 0x0046 }
            if (r1 == 0) goto L_0x00e5
            java.lang.String r1 = ".amr"
            goto L_0x007a
        L_0x00ba:
            r3 = 1
            if (r10 != r3) goto L_0x00e3
            if (r1 == 0) goto L_0x00c1
            java.lang.String r1 = ".amr"
        L_0x00c1:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0046 }
            r3.<init>()     // Catch:{ IOException -> 0x0046 }
            java.lang.StringBuilder r2 = r3.append(r2)     // Catch:{ IOException -> 0x0046 }
            java.lang.String r3 = "_"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ IOException -> 0x0046 }
            java.lang.StringBuilder r2 = r2.append(r11)     // Catch:{ IOException -> 0x0046 }
            java.lang.String r3 = "s"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ IOException -> 0x0046 }
            java.lang.StringBuilder r1 = r2.append(r1)     // Catch:{ IOException -> 0x0046 }
            java.lang.String r1 = r1.toString()     // Catch:{ IOException -> 0x0046 }
            goto L_0x0091
        L_0x00e3:
            r1 = r2
            goto L_0x0091
        L_0x00e5:
            r1 = r6
            goto L_0x007a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.snda.youni.attachment.a.j.a(java.lang.String, java.lang.String, int, int):java.lang.String");
    }

    public static void a(Context context) {
        c = new j(context);
    }

    static /* synthetic */ void a(j jVar, String str, long j) {
        String str2;
        b a2 = a.a(b, str);
        if (a2 == null || a2.k() == null || !a2.k().startsWith("http://")) {
            try {
                try {
                    try {
                        try {
                            Document parse = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new URL(str).openConnection().getInputStream());
                            String nodeValue = parse.getElementById("thumb_url").getAttributes().getNamedItem("src").getNodeValue();
                            "img src is " + nodeValue;
                            "a href is " + parse.getElementById("image_url").getAttributes().getNamedItem("href").getNodeValue();
                            str2 = jVar.a(nodeValue, e.i, 0, 0);
                        } catch (SAXException e2) {
                            e2.printStackTrace();
                            str2 = null;
                        }
                    } catch (ParserConfigurationException e3) {
                        e3.printStackTrace();
                        str2 = null;
                    }
                } catch (IOException e4) {
                    e4.printStackTrace();
                    str2 = null;
                }
            } catch (MalformedURLException e5) {
                e5.printStackTrace();
                str2 = null;
            }
        } else {
            str2 = jVar.a(a2.k(), e.i, 0, 0);
        }
        ContentValues contentValues = new ContentValues();
        if (str2 == null) {
            contentValues.put("subject", "222222");
        } else {
            contentValues.put("subject", str2);
        }
        b.getContentResolver().update(Uri.parse("content://sms/" + j), contentValues, null, null);
    }

    private void a(String str, String str2, String str3) {
        if (!o.a()) {
            d.post(new a(this));
            throw new Exception();
        }
        try {
            URLConnection openConnection = new URL(str).openConnection();
            openConnection.addRequestProperty("User-Agent-YN", h.a());
            openConnection.setConnectTimeout(30000);
            BufferedInputStream bufferedInputStream = new BufferedInputStream(openConnection.getInputStream());
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            byte[] bArr = new byte[1024];
            while (true) {
                int read = bufferedInputStream.read(bArr);
                if (read > 0) {
                    byteArrayOutputStream.write(bArr, 0, read);
                } else {
                    byte[] byteArray = byteArrayOutputStream.toByteArray();
                    FileOutputStream fileOutputStream = new FileOutputStream(new File(str3, str2));
                    fileOutputStream.write(byteArray);
                    fileOutputStream.close();
                    return;
                }
            }
        } catch (IOException e2) {
            "Error: " + e2;
            throw new Exception();
        }
    }

    static /* synthetic */ void b(j jVar, String str, long j) {
        String str2;
        b a2 = a.a(b, str);
        if (a2 == null || a2.i() == null || !a2.i().startsWith("http://")) {
            try {
                try {
                    try {
                        try {
                            Document parse = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new URL(str).openConnection().getInputStream());
                            String nodeValue = parse.getElementById("audioUrl").getAttributes().getNamedItem("href").getNodeValue();
                            String nodeValue2 = parse.getElementById("audio_duration").getAttributes().getNamedItem("value").getNodeValue();
                            "a href is " + nodeValue;
                            "audio duration is " + nodeValue2;
                            str2 = jVar.a(nodeValue, e.j, 1, Integer.parseInt(nodeValue2));
                        } catch (SAXException e2) {
                            e2.printStackTrace();
                            str2 = null;
                        }
                    } catch (ParserConfigurationException e3) {
                        e3.printStackTrace();
                        str2 = null;
                    }
                } catch (IOException e4) {
                    e4.printStackTrace();
                    str2 = null;
                }
            } catch (MalformedURLException e5) {
                e5.printStackTrace();
                str2 = null;
            }
        } else {
            str2 = jVar.a(a2.i(), e.j, 1, a2.n());
        }
        ContentValues contentValues = new ContentValues();
        if (str2 == null) {
            contentValues.put("subject", "222222");
        } else {
            contentValues.put("subject", str2);
        }
        b.getContentResolver().update(Uri.parse("content://sms/" + j), contentValues, null, null);
    }

    public final void a(String str, long j, int i, Handler handler) {
        d dVar = new d(this, j, str, i);
        synchronized (this.e) {
            Iterator it = this.e.iterator();
            boolean z = false;
            while (it.hasNext()) {
                z = dVar.a((d) it.next()) ? true : z;
            }
            if (!z) {
                this.e.add(dVar);
            }
        }
        if (this.f325a == null) {
            this.f325a = new c(this);
            this.f325a.execute(new Void[0]);
            d = handler;
        }
    }

    public final void a(String str, String str2) {
        b a2 = a.a(b, str);
        if (a2 == null || a2.i() == null || !a2.i().startsWith("http://")) {
            try {
                try {
                    try {
                        try {
                            Document parse = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new URL(str).openConnection().getInputStream());
                            parse.getDocumentElement();
                            String nodeValue = parse.getElementById("thumb_url").getAttributes().getNamedItem("src").getNodeValue();
                            String nodeValue2 = parse.getElementById("image_url").getAttributes().getNamedItem("href").getNodeValue();
                            "img src is " + nodeValue;
                            "a href is " + nodeValue2;
                            a(nodeValue2, str2, e.h);
                        } catch (Exception e2) {
                            throw new Exception();
                        } catch (SAXException e3) {
                            e3.printStackTrace();
                            throw new Exception();
                        }
                    } catch (ParserConfigurationException e4) {
                        e4.printStackTrace();
                        throw new Exception();
                    }
                } catch (IOException e5) {
                    e5.printStackTrace();
                    throw new Exception();
                }
            } catch (MalformedURLException e6) {
                e6.printStackTrace();
                throw new Exception();
            }
        } else {
            a(a2.i(), str2, e.h);
        }
    }
}
