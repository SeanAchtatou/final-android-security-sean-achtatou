package com.snda.youni.attachment;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.widget.Toast;
import com.snda.youni.C0000R;

final class a extends AsyncTask {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ShowAttachment f316a;

    a(ShowAttachment showAttachment) {
        this.f316a = showAttachment;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000e, code lost:
        if (r1.endsWith(".jpg") == false) goto L_0x0010;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.lang.Boolean a(java.lang.String... r4) {
        /*
            r1 = 1
            r3 = 0
            r0 = r4[r3]
            r1 = r4[r1]
            if (r1 == 0) goto L_0x0010
            java.lang.String r2 = ".jpg"
            boolean r2 = r1.endsWith(r2)     // Catch:{ Exception -> 0x0059 }
            if (r2 != 0) goto L_0x001c
        L_0x0010:
            double r1 = java.lang.Math.random()     // Catch:{ Exception -> 0x0059 }
            long r1 = java.lang.Double.doubleToLongBits(r1)     // Catch:{ Exception -> 0x0059 }
            java.lang.String r1 = java.lang.Long.toHexString(r1)     // Catch:{ Exception -> 0x0059 }
        L_0x001c:
            java.lang.String r2 = com.snda.youni.attachment.e.h     // Catch:{ Exception -> 0x0059 }
            boolean r2 = com.snda.youni.e.e.b(r1, r2)     // Catch:{ Exception -> 0x0059 }
            if (r2 != 0) goto L_0x002b
            com.snda.youni.attachment.a.j r2 = com.snda.youni.attachment.a.j.a()     // Catch:{ Exception -> 0x0059 }
            r2.a(r0, r1)     // Catch:{ Exception -> 0x0059 }
        L_0x002b:
            java.lang.String r2 = com.snda.youni.attachment.e.h     // Catch:{ Exception -> 0x0059 }
            android.graphics.Bitmap r2 = com.snda.youni.e.p.a(r1, r2)     // Catch:{ Exception -> 0x0059 }
            if (r2 != 0) goto L_0x004d
            java.lang.String r2 = com.snda.youni.attachment.e.h     // Catch:{ Exception -> 0x0059 }
            com.snda.youni.e.e.a(r1, r2)     // Catch:{ Exception -> 0x0059 }
            com.snda.youni.attachment.a.j r2 = com.snda.youni.attachment.a.j.a()     // Catch:{ Exception -> 0x0059 }
            r2.a(r0, r1)     // Catch:{ Exception -> 0x0059 }
            java.lang.String r0 = com.snda.youni.attachment.e.h     // Catch:{ Exception -> 0x0059 }
            android.graphics.Bitmap r0 = com.snda.youni.e.p.a(r1, r0)     // Catch:{ Exception -> 0x0059 }
            if (r0 != 0) goto L_0x004e
            r0 = 0
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)     // Catch:{ Exception -> 0x0059 }
        L_0x004c:
            return r0
        L_0x004d:
            r0 = r2
        L_0x004e:
            if (r0 == 0) goto L_0x0053
            r0.recycle()     // Catch:{ Exception -> 0x0059 }
        L_0x0053:
            r0 = 1
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)     // Catch:{ Exception -> 0x0059 }
            goto L_0x004c
        L_0x0059:
            r0 = move-exception
            r0.printStackTrace()
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r3)
            goto L_0x004c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.snda.youni.attachment.a.a(java.lang.String[]):java.lang.Boolean");
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ Object doInBackground(Object[] objArr) {
        return a((String[]) objArr);
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void onPostExecute(Object obj) {
        Boolean bool = (Boolean) obj;
        if (!bool.booleanValue()) {
            Toast.makeText(this.f316a, this.f316a.getString(C0000R.string.show_attachment_fetch_failed), 0).show();
            this.f316a.finish();
        }
        this.f316a.runOnUiThread(new j(this));
        try {
            this.f316a.b.dismiss();
            this.f316a.b = null;
        } catch (Exception e) {
        }
        super.onPostExecute(bool);
    }

    /* access modifiers changed from: protected */
    public final void onPreExecute() {
        this.f316a.b = ProgressDialog.show(this.f316a, "", this.f316a.c.getString(C0000R.string.attachment_loading), false, true);
        this.f316a.b.setOnCancelListener(new i(this));
        super.onPreExecute();
    }
}
