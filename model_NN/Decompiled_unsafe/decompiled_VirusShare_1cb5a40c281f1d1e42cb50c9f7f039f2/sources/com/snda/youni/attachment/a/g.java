package com.snda.youni.attachment.a;

import java.io.FilterOutputStream;
import java.io.OutputStream;

public final class g extends FilterOutputStream {

    /* renamed from: a  reason: collision with root package name */
    private final h f323a;
    private long b = 0;

    public g(OutputStream outputStream, h hVar) {
        super(outputStream);
        this.f323a = hVar;
    }

    public final void write(int i) {
        this.out.write(i);
        this.b++;
        this.f323a.a(this.b);
    }

    public final void write(byte[] bArr, int i, int i2) {
        this.out.write(bArr, i, i2);
        this.b += (long) i2;
        this.f323a.a(this.b);
    }
}
