package com.snda.youni.attachment;

import android.graphics.Bitmap;
import android.widget.TextView;
import com.snda.youni.C0000R;
import com.snda.youni.attachment.c.a;
import com.snda.youni.e.f;
import com.snda.youni.e.p;
import java.io.File;

final class j implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ a f334a;

    j(a aVar) {
        this.f334a = aVar;
    }

    public final void run() {
        Bitmap a2 = p.a(this.f334a.f316a.getIntent().getStringExtra("filename"), e.h);
        if (a2 != null) {
            this.f334a.f316a.d.setImageBitmap(a2);
            TextView textView = this.f334a.f316a.e;
            ShowAttachment showAttachment = this.f334a.f316a;
            String stringExtra = this.f334a.f316a.getIntent().getStringExtra("filename");
            StringBuilder sb = new StringBuilder();
            long lastModified = new File(e.h, stringExtra).lastModified();
            String str = f.b(lastModified) + " " + f.a(lastModified);
            String a3 = a.a(stringExtra);
            String b = a.b(stringExtra);
            if (stringExtra != null) {
                sb.append(showAttachment.getString(C0000R.string.show_attachment_info_name) + stringExtra + "\n");
            }
            if (str != null) {
                sb.append(showAttachment.getString(C0000R.string.show_attachment_info_time) + str + "\n");
            }
            if (a3 != null) {
                sb.append(showAttachment.getString(C0000R.string.show_attachment_info_size) + a3 + "\n");
            }
            if (b != null) {
                sb.append(showAttachment.getString(C0000R.string.show_attachment_info_resolution) + b + "\n");
            }
            textView.setText(sb.toString());
            this.f334a.f316a.f = this.f334a.f316a.getIntent().getStringExtra("filename");
            this.f334a.f316a.setContentView(this.f334a.f316a.f315a);
            ShowAttachment.a(this.f334a.f316a);
        }
    }
}
