package com.snda.youni.attachment.b;

import android.content.ContentValues;
import android.database.Cursor;
import java.io.Serializable;

public final class b implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    private long f327a;
    private long b;
    private String c;
    private String d;
    private String e;
    private String f;
    private String g;
    private String h;
    private String i;
    private String j;
    private int k;
    private int l;
    private int m;
    private int n;
    private int o;
    private int p;
    private boolean q;
    private boolean[] r;
    private long[] s;
    private boolean t = false;
    private long[] u;

    public b() {
    }

    public b(Cursor cursor) {
        this.b = cursor.getLong(cursor.getColumnIndex("message_id"));
        this.c = cursor.getString(cursor.getColumnIndex("mine_type"));
        this.d = cursor.getString(cursor.getColumnIndex("filename"));
        this.e = cursor.getString(cursor.getColumnIndex("local_path"));
        this.f = cursor.getString(cursor.getColumnIndex("server_url"));
        this.g = cursor.getString(cursor.getColumnIndex("thumbnail_local_path"));
        this.h = cursor.getString(cursor.getColumnIndex("thumbnail_short_url"));
        this.i = cursor.getString(cursor.getColumnIndex("thumbnail_server_url"));
        this.j = cursor.getString(cursor.getColumnIndex("file_size"));
        this.k = cursor.getInt(cursor.getColumnIndex("status"));
        this.l = cursor.getInt(cursor.getColumnIndex("box_type"));
        this.m = cursor.getInt(cursor.getColumnIndex("transfer_channel"));
        this.n = cursor.getInt(cursor.getColumnIndex("play_time_duration"));
        this.o = cursor.getInt(cursor.getColumnIndex("image_width"));
        this.p = cursor.getInt(cursor.getColumnIndex("image_height"));
    }

    public final long a() {
        return this.f327a;
    }

    public final void a(int i2) {
        this.r[i2] = true;
    }

    public final void a(long j2) {
        this.f327a = j2;
    }

    public final void a(String str) {
        this.c = str;
    }

    public final void a(boolean z) {
        this.q = z;
    }

    public final void a(long[] jArr) {
        this.s = jArr;
    }

    public final void a(boolean[] zArr) {
        this.r = zArr;
    }

    public final void b(int i2) {
        this.k = i2;
    }

    public final void b(long j2) {
        this.b = j2;
    }

    public final void b(String str) {
        this.d = str;
    }

    public final void b(long[] jArr) {
        this.u = jArr;
    }

    public final boolean b() {
        return this.t;
    }

    public final void c() {
        this.t = true;
    }

    public final void c(int i2) {
        this.l = i2;
    }

    public final void c(String str) {
        this.e = str;
    }

    public final long d() {
        return this.b;
    }

    public final void d(int i2) {
        this.n = i2;
    }

    public final void d(String str) {
        this.f = str;
    }

    public final void e(int i2) {
        this.o = i2;
    }

    public final void e(String str) {
        this.g = str;
    }

    public final long[] e() {
        return this.s;
    }

    public final void f(int i2) {
        this.p = i2;
    }

    public final void f(String str) {
        this.h = str;
    }

    public final long[] f() {
        return this.u;
    }

    public final String g() {
        return this.c;
    }

    public final void g(String str) {
        this.i = str;
    }

    public final String h() {
        return this.d;
    }

    public final void h(String str) {
        this.j = str;
    }

    public final String i() {
        return this.f;
    }

    public final String j() {
        return this.h;
    }

    public final String k() {
        return this.i;
    }

    public final String l() {
        return this.j;
    }

    public final void m() {
        this.m = 0;
    }

    public final int n() {
        return this.n;
    }

    public final int o() {
        return this.o;
    }

    public final int p() {
        return this.p;
    }

    public final ContentValues q() {
        ContentValues contentValues = new ContentValues();
        if (this.b > -1) {
            contentValues.put("message_id", Long.valueOf(this.b));
        }
        if (this.c != null) {
            contentValues.put("mine_type", this.c);
        }
        if (this.d != null) {
            contentValues.put("filename", this.d);
        }
        if (this.e != null) {
            contentValues.put("local_path", this.e);
        }
        if (this.f != null) {
            contentValues.put("server_url", this.f);
        }
        if (this.g != null) {
            contentValues.put("thumbnail_local_path", this.g);
        }
        if (this.h != null) {
            contentValues.put("thumbnail_short_url", this.h);
        }
        if (this.i != null) {
            contentValues.put("thumbnail_server_url", this.i);
        }
        if (this.j != null) {
            contentValues.put("file_size", this.j);
        }
        if (this.k >= 0) {
            contentValues.put("status", Integer.valueOf(this.k));
        }
        if (this.l >= 0) {
            contentValues.put("box_type", Integer.valueOf(this.l));
        }
        if (this.m >= 0) {
            contentValues.put("transfer_channel", Integer.valueOf(this.m));
        }
        if (this.n >= 0) {
            contentValues.put("play_time_duration", Integer.valueOf(this.n));
        }
        if (this.o >= 0) {
            contentValues.put("image_width", Integer.valueOf(this.o));
        }
        if (this.p >= 0) {
            contentValues.put("image_height", Integer.valueOf(this.p));
        }
        return contentValues;
    }
}
