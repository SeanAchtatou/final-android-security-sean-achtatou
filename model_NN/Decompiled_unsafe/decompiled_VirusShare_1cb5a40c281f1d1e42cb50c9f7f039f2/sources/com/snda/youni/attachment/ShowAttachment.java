package com.snda.youni.attachment;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Matrix;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.snda.youni.C0000R;

public class ShowAttachment extends Activity implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    FrameLayout f315a;
    ProgressDialog b;
    Context c;
    ImageView d;
    TextView e;
    String f;
    private a g = new a(this);
    private Matrix h;
    private LinearLayout i;
    private LinearLayout j;

    static /* synthetic */ void a(ShowAttachment showAttachment) {
        showAttachment.i = (LinearLayout) showAttachment.findViewById(C0000R.id.show_attachment_info_layout);
        showAttachment.j = (LinearLayout) showAttachment.findViewById(C0000R.id.show_attachment_function_layout);
        showAttachment.findViewById(C0000R.id.show_attachment_info).setOnClickListener(showAttachment);
        showAttachment.findViewById(C0000R.id.show_attachment_rightrotate).setOnClickListener(showAttachment);
        showAttachment.findViewById(C0000R.id.show_attachment_leftrotate).setOnClickListener(showAttachment);
        showAttachment.findViewById(C0000R.id.show_attachment_enlarge).setOnClickListener(showAttachment);
        showAttachment.findViewById(C0000R.id.show_attachment_shrink).setOnClickListener(showAttachment);
        showAttachment.findViewById(C0000R.id.show_attachment_save).setOnClickListener(showAttachment);
        showAttachment.findViewById(C0000R.id.show_attachment_back).setOnClickListener(showAttachment);
        showAttachment.d.setOnClickListener(showAttachment);
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case C0000R.id.show_attachment_image /*2131558440*/:
                if (this.i.getVisibility() == 8) {
                    this.i.setVisibility(0);
                } else {
                    this.i.setVisibility(8);
                }
                if (this.j.getVisibility() == 8) {
                    this.j.setVisibility(0);
                    return;
                } else {
                    this.j.setVisibility(8);
                    return;
                }
            case C0000R.id.show_attachment_info_layout /*2131558441*/:
            case C0000R.id.show_attachment_info_text /*2131558443*/:
            case C0000R.id.show_attachment_function_layout /*2131558444*/:
            default:
                return;
            case C0000R.id.show_attachment_info /*2131558442*/:
                if (this.e.getVisibility() == 8) {
                    this.e.setVisibility(0);
                    return;
                } else {
                    this.e.setVisibility(8);
                    return;
                }
            case C0000R.id.show_attachment_rightrotate /*2131558445*/:
                this.d.setScaleType(ImageView.ScaleType.MATRIX);
                this.h = this.d.getImageMatrix();
                this.h.postRotate(-90.0f, (float) (this.d.getWidth() / 2), (float) (this.d.getHeight() / 2));
                this.d.setImageMatrix(this.h);
                this.d.invalidate();
                return;
            case C0000R.id.show_attachment_leftrotate /*2131558446*/:
                this.d.setScaleType(ImageView.ScaleType.MATRIX);
                this.h = this.d.getImageMatrix();
                this.h.postRotate(90.0f, (float) (this.d.getWidth() / 2), (float) (this.d.getHeight() / 2));
                this.d.setImageMatrix(this.h);
                this.d.invalidate();
                return;
            case C0000R.id.show_attachment_enlarge /*2131558447*/:
                this.d.setScaleType(ImageView.ScaleType.MATRIX);
                this.h = this.d.getImageMatrix();
                this.h.postScale(1.2f, 1.2f, (float) (this.d.getWidth() / 2), (float) (this.d.getHeight() / 2));
                this.d.setImageMatrix(this.h);
                this.d.invalidate();
                return;
            case C0000R.id.show_attachment_shrink /*2131558448*/:
                this.d.setScaleType(ImageView.ScaleType.MATRIX);
                this.h = this.d.getImageMatrix();
                this.h.postScale(0.8f, 0.8f, (float) (this.d.getWidth() / 2), (float) (this.d.getHeight() / 2));
                this.d.setImageMatrix(this.h);
                this.d.invalidate();
                return;
            case C0000R.id.show_attachment_save /*2131558449*/:
                showDialog(1);
                return;
            case C0000R.id.show_attachment_back /*2131558450*/:
                finish();
                return;
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        this.c = this;
        this.f315a = (FrameLayout) ((LayoutInflater) getSystemService("layout_inflater")).inflate((int) C0000R.layout.activity_show_attachment, (ViewGroup) null);
        this.d = (ImageView) this.f315a.findViewById(C0000R.id.show_attachment_image);
        this.e = (TextView) this.f315a.findViewById(C0000R.id.show_attachment_info_text);
        if (getIntent() != null) {
            this.g.execute(getIntent().getStringExtra("short_url"), getIntent().getStringExtra("filename"));
        }
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int i2) {
        switch (i2) {
            case 1:
                View inflate = LayoutInflater.from(this).inflate((int) C0000R.layout.show_attachment_save_dialog, (ViewGroup) null);
                EditText editText = (EditText) inflate.findViewById(C0000R.id.show_attachment_save_dialog_name);
                editText.setText(this.f);
                return new AlertDialog.Builder(this).setTitle((int) C0000R.string.show_attachment_save_dialog_title).setIcon(0).setView(inflate).setPositiveButton((int) C0000R.string.show_attachment_save_dialog_save, new d(this, editText)).setNegativeButton((int) C0000R.string.show_attachment_save_dialog_cancel, new c(this)).create();
            default:
                return null;
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        this.g.cancel(true);
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 != 4) {
            return super.onKeyDown(i2, keyEvent);
        }
        this.g.cancel(true);
        finish();
        return true;
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
    }
}
