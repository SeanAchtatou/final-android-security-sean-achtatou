package com.snda.youni.attachment;

import android.media.MediaRecorder;
import android.os.Environment;
import java.io.File;
import java.io.IOException;

public final class f {
    private static int c = -1;

    /* renamed from: a  reason: collision with root package name */
    private MediaRecorder f331a = new MediaRecorder();
    private String b;

    public f() {
        String str = "youni_audio.amr";
        str = !str.startsWith("/") ? "/" + str : str;
        this.b = Environment.getExternalStorageDirectory().getAbsolutePath() + (!str.contains(".") ? str + ".amr" : str);
    }

    public final boolean a() {
        k.a().b();
        File parentFile = new File(this.b).getParentFile();
        if (parentFile.exists() || parentFile.mkdirs()) {
            try {
                this.f331a.setAudioSource(1);
                this.f331a.setOutputFormat(1);
                this.f331a.setAudioEncoder(1);
                this.f331a.setOutputFile(this.b);
                this.f331a.prepare();
                this.f331a.start();
                return true;
            } catch (IllegalStateException e) {
                e.printStackTrace();
                return false;
            } catch (Exception e2) {
                e2.printStackTrace();
                return false;
            }
        } else {
            throw new IOException("Path to file could not be created.");
        }
    }

    public final int b() {
        return this.f331a.getMaxAmplitude();
    }

    public final void c() {
        this.f331a.stop();
        c = 0;
    }

    public final void d() {
        if (c == 0) {
            this.f331a.reset();
            this.f331a.release();
            c = -1;
            return;
        }
        this.f331a.stop();
        this.f331a.reset();
        this.f331a.release();
        c = -1;
    }
}
