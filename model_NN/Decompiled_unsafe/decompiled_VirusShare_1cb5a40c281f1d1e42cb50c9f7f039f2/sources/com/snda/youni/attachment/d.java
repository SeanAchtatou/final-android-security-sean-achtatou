package com.snda.youni.attachment;

import android.content.DialogInterface;
import android.widget.EditText;
import android.widget.Toast;
import com.snda.youni.C0000R;
import com.snda.youni.attachment.c.a;

final class d implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ EditText f329a;
    private /* synthetic */ ShowAttachment b;

    d(ShowAttachment showAttachment, EditText editText) {
        this.b = showAttachment;
        this.f329a = editText;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        if (!a.a(this.b, this.b.f, this.f329a.getText().toString()).booleanValue()) {
            Toast.makeText(this.b, this.b.getString(C0000R.string.save_attachment_failed), 0).show();
        }
    }
}
