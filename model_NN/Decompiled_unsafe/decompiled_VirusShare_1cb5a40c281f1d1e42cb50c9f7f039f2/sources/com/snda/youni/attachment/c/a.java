package com.snda.youni.attachment.c;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.widget.Toast;
import com.snda.youni.C0000R;
import com.snda.youni.attachment.b.b;
import com.snda.youni.e.e;
import com.snda.youni.e.p;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public final class a {
    public static b a(Context context, int i, String str) {
        String str2 = Long.toHexString(Double.doubleToLongBits(Math.random())) + "_" + i + "s.amr";
        e.a(str, com.snda.youni.attachment.e.g, str2, com.snda.youni.attachment.e.j);
        e.a(str, com.snda.youni.attachment.e.g);
        b bVar = new b();
        bVar.a("audio");
        bVar.b(str2);
        bVar.c(com.snda.youni.attachment.e.j + File.separator + str2);
        bVar.c(0);
        bVar.b(0);
        bVar.m();
        bVar.h("" + e.c(str2, com.snda.youni.attachment.e.j));
        bVar.d(i);
        com.snda.youni.attachment.b.a.a(context, bVar);
        return bVar;
    }

    public static b a(Context context, Bitmap bitmap) {
        if (bitmap == null) {
            return null;
        }
        String str = Long.toHexString(Double.doubleToLongBits(Math.random())) + ".jpg";
        p.a(bitmap, str, com.snda.youni.attachment.e.h);
        Bitmap a2 = p.a(bitmap);
        if (bitmap != null) {
            bitmap.recycle();
        }
        p.a(a2, str, com.snda.youni.attachment.e.i);
        if (a2 != null) {
            a2.recycle();
        }
        b bVar = new b();
        bVar.a("img");
        bVar.b(str);
        bVar.c(com.snda.youni.attachment.e.h + File.separator + str);
        bVar.e(com.snda.youni.attachment.e.i + File.separator + str);
        bVar.c(0);
        bVar.b(0);
        bVar.m();
        bVar.h("" + e.c(str, com.snda.youni.attachment.e.h));
        bVar.e(bitmap.getWidth());
        bVar.f(bitmap.getHeight());
        com.snda.youni.attachment.b.a.a(context, bVar);
        return bVar;
    }

    public static b a(Context context, Uri uri) {
        int i;
        int i2;
        int i3 = PreferenceManager.getDefaultSharedPreferences(context).getInt("image_size_name", 1);
        if (i3 == 0) {
            int i4 = com.snda.youni.attachment.e.e;
            i = i4;
            i2 = com.snda.youni.attachment.e.f;
        } else if (i3 == 2) {
            int i5 = com.snda.youni.attachment.e.f330a;
            i = i5;
            i2 = com.snda.youni.attachment.e.b;
        } else {
            int i6 = com.snda.youni.attachment.e.c;
            i = i6;
            i2 = com.snda.youni.attachment.e.d;
        }
        Bitmap a2 = p.a(context, i2 * i, uri);
        e.a("youni_camera.jpg", com.snda.youni.attachment.e.g);
        return a(context, a2);
    }

    public static Boolean a(Context context, String str, String str2) {
        try {
            FileInputStream fileInputStream = new FileInputStream(new File(com.snda.youni.attachment.e.h, str));
            FileOutputStream fileOutputStream = new FileOutputStream(new File(com.snda.youni.attachment.e.g, str2));
            byte[] bArr = new byte[1024];
            while (true) {
                try {
                    int read = fileInputStream.read(bArr);
                    if (read != -1) {
                        fileOutputStream.write(bArr, 0, read);
                    } else {
                        fileInputStream.close();
                        fileOutputStream.close();
                        Toast.makeText(context, context.getString(C0000R.string.save_attachment_success) + com.snda.youni.attachment.e.g + "/" + str2, 0).show();
                        return true;
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    return false;
                }
            }
        } catch (FileNotFoundException e2) {
            e2.printStackTrace();
            return false;
        }
    }

    public static String a(String str) {
        try {
            FileInputStream fileInputStream = new FileInputStream(new File(com.snda.youni.attachment.e.h, str));
            try {
                int available = fileInputStream.available();
                fileInputStream.close();
                return (available >> 10) + "kb";
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
        } catch (FileNotFoundException e2) {
            e2.printStackTrace();
        }
    }

    public static String b(String str) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        try {
            FileInputStream fileInputStream = new FileInputStream(new File(com.snda.youni.attachment.e.h, str));
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(fileInputStream, null, options);
            try {
                fileInputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return options.outWidth + "x" + options.outHeight;
        } catch (FileNotFoundException e2) {
            e2.printStackTrace();
            return null;
        }
    }
}
