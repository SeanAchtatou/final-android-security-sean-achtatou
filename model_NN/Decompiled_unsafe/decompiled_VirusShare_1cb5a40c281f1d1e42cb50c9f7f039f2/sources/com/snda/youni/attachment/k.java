package com.snda.youni.attachment;

import android.content.Context;
import android.media.MediaPlayer;
import android.net.Uri;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;
import com.snda.youni.C0000R;
import com.snda.youni.e.e;
import com.snda.youni.mms.ui.a;
import java.io.File;

public final class k {
    private static k c;

    /* renamed from: a  reason: collision with root package name */
    private Context f335a;
    private MediaPlayer b;
    private a d;
    private long e = 0;
    /* access modifiers changed from: private */
    public ImageButton f;

    private k(Context context) {
        this.f335a = context;
    }

    public static k a() {
        return c;
    }

    public static void a(Context context) {
        c = new k(context);
    }

    public final void a(View view) {
        this.f = (ImageButton) view;
        this.d = (a) this.f.getTag();
        this.f.setImageResource(C0000R.drawable.mms_pause_btn);
    }

    public final boolean a(long j) {
        return this.e == j && this.b != null && this.b.isPlaying();
    }

    public final void b() {
        if (this.f != null) {
            this.f.setImageResource(C0000R.drawable.mms_play_btn);
            if (((a) this.f.getTag()) != null) {
                ((a) this.f.getTag()).e();
            }
        }
        if (this.b != null) {
            "AUDIO PLAYER HAS DURATION = " + this.b.getCurrentPosition();
            this.b.stop();
        }
    }

    public final void b(View view) {
        b();
        this.f = (ImageButton) view;
        this.d = (a) this.f.getTag();
        this.f.setImageResource(C0000R.drawable.mms_pause_btn);
        this.d.d();
        this.e = this.d.g();
        if (this.b != null) {
            this.b.stop();
        }
        String k = this.d.k();
        "audio filename is " + k;
        if (this.d.k() == null || !this.d.k().endsWith(".amr")) {
            Toast.makeText(this.f335a, (int) C0000R.string.audio_attachment_unavailable, 0).show();
            this.f.setImageResource(C0000R.drawable.mms_play_btn);
        } else if (!e.b(this.d.k(), e.j)) {
            Toast.makeText(this.f335a, (int) C0000R.string.audio_attachment_unavailable, 0).show();
            this.f.setImageResource(C0000R.drawable.mms_play_btn);
            this.d.l();
        } else {
            this.b = MediaPlayer.create(this.f335a, Uri.fromFile(new File(e.j, k)));
            if (this.b != null) {
                this.b.setOnCompletionListener(new b(this));
                try {
                    this.b.start();
                } catch (IllegalStateException e2) {
                    e2.printStackTrace();
                }
            } else {
                Toast.makeText(this.f335a, (int) C0000R.string.audio_attachment_error, 0).show();
                this.f.setImageResource(C0000R.drawable.mms_play_btn);
            }
        }
    }
}
