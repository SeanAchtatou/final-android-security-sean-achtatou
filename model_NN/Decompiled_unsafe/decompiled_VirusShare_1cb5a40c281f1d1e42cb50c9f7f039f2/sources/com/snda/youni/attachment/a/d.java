package com.snda.youni.attachment.a;

final class d {

    /* renamed from: a  reason: collision with root package name */
    private long f320a;
    private String b;
    private int c;
    private /* synthetic */ j d;

    public d(j jVar, long j, String str, int i) {
        this.d = jVar;
        this.f320a = j;
        this.b = str;
        this.c = i;
    }

    public final long a() {
        return this.f320a;
    }

    public final boolean a(d dVar) {
        return this.f320a == dVar.f320a;
    }

    public final String b() {
        return this.b;
    }

    public final int c() {
        return this.c;
    }
}
