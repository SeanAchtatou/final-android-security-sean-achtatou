package com.snda.youni.f;

import java.io.Serializable;

public final class c implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    private String f406a;
    private String b;
    private String c;
    private byte[] d;
    private Object[] e;
    private Object f;
    private boolean g;

    public c() {
    }

    public c(String str) {
        this.f406a = str;
        this.b = "POST";
        this.c = "application/octet-stream";
    }

    public final void a(Object obj) {
        this.f = obj;
    }

    public final void a(String str) {
        this.b = str;
    }

    public final void a(byte[] bArr) {
        this.d = bArr;
    }

    public final void a(Object[] objArr) {
        this.e = objArr;
    }

    public final boolean a() {
        return this.g;
    }

    public final void b() {
        this.g = true;
    }

    public final void b(String str) {
        this.c = str;
    }

    public final Object c() {
        return this.f;
    }

    public final Object[] d() {
        return this.e;
    }

    public final String e() {
        return this.b;
    }

    public final byte[] f() {
        return this.d;
    }

    public final String g() {
        return this.f406a;
    }

    public final String h() {
        return this.c;
    }
}
