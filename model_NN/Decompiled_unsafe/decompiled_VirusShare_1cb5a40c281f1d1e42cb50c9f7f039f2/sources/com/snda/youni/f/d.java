package com.snda.youni.f;

import java.io.Serializable;

public final class d implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    private Object f407a;
    private String b;
    private int c;
    private int d;

    public final String a() {
        return this.b;
    }

    public final void a(int i) {
        this.d = i;
    }

    public final void a(Object obj) {
        this.f407a = obj;
    }

    public final void a(String str) {
        this.b = str;
    }

    public final Object b() {
        return this.f407a;
    }

    public final void b(int i) {
        this.c = i;
    }

    public final int c() {
        return this.d;
    }
}
