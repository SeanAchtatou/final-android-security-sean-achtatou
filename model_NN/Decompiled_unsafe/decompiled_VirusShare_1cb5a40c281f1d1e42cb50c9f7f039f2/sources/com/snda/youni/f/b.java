package com.snda.youni.f;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import com.snda.youni.C0000R;

public final class b extends AsyncTask {

    /* renamed from: a  reason: collision with root package name */
    private static int f405a = 20000;
    private static int b = 20000;
    private a c;
    private Context d;
    private ProgressDialog e;
    private c f;
    private boolean g;

    public b(Context context) {
        this.d = context;
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:100:0x029e  */
    /* JADX WARNING: Removed duplicated region for block: B:106:0x02a8 A[SYNTHETIC, Splitter:B:106:0x02a8] */
    /* JADX WARNING: Removed duplicated region for block: B:109:0x02ad A[SYNTHETIC, Splitter:B:109:0x02ad] */
    /* JADX WARNING: Removed duplicated region for block: B:120:0x02c2 A[SYNTHETIC, Splitter:B:120:0x02c2] */
    /* JADX WARNING: Removed duplicated region for block: B:123:0x02c7 A[SYNTHETIC, Splitter:B:123:0x02c7] */
    /* JADX WARNING: Removed duplicated region for block: B:135:0x02e3 A[SYNTHETIC, Splitter:B:135:0x02e3] */
    /* JADX WARNING: Removed duplicated region for block: B:138:0x02e8 A[SYNTHETIC, Splitter:B:138:0x02e8] */
    /* JADX WARNING: Removed duplicated region for block: B:150:0x0304 A[SYNTHETIC, Splitter:B:150:0x0304] */
    /* JADX WARNING: Removed duplicated region for block: B:153:0x0309 A[SYNTHETIC, Splitter:B:153:0x0309] */
    /* JADX WARNING: Removed duplicated region for block: B:165:0x0325 A[SYNTHETIC, Splitter:B:165:0x0325] */
    /* JADX WARNING: Removed duplicated region for block: B:168:0x032a A[SYNTHETIC, Splitter:B:168:0x032a] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0189 A[Catch:{ MalformedURLException -> 0x03a6, SocketException -> 0x0395, SocketTimeoutException -> 0x0384, ConnectTimeoutException -> 0x0373, IOException -> 0x0365, Exception -> 0x0357, all -> 0x034b }] */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x01c5 A[SYNTHETIC, Splitter:B:40:0x01c5] */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x01ca A[SYNTHETIC, Splitter:B:43:0x01ca] */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x0209 A[SYNTHETIC, Splitter:B:57:0x0209] */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x020e A[SYNTHETIC, Splitter:B:60:0x020e] */
    /* JADX WARNING: Removed duplicated region for block: B:64:0x0217  */
    /* JADX WARNING: Removed duplicated region for block: B:90:0x0285 A[Catch:{ all -> 0x02a3 }] */
    /* JADX WARNING: Removed duplicated region for block: B:93:0x028e A[SYNTHETIC, Splitter:B:93:0x028e] */
    /* JADX WARNING: Removed duplicated region for block: B:96:0x0293 A[SYNTHETIC, Splitter:B:96:0x0293] */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.snda.youni.f.d doInBackground(com.snda.youni.f.c... r10) {
        /*
            r9 = this;
            r7 = 0
            r6 = 0
            r5 = 1
            java.lang.String r0 = "http.keepAlive"
            java.lang.String r1 = "false"
            java.lang.System.setProperty(r0, r1)
            com.snda.youni.f.d r1 = new com.snda.youni.f.d
            r1.<init>()
            r0 = 0
            r0 = r10[r0]     // Catch:{ MalformedURLException -> 0x01e2, SocketException -> 0x0275, SocketTimeoutException -> 0x02b6, ConnectTimeoutException -> 0x02d7, IOException -> 0x02f8, Exception -> 0x0319, all -> 0x0346 }
            r9.f = r0     // Catch:{ MalformedURLException -> 0x01e2, SocketException -> 0x0275, SocketTimeoutException -> 0x02b6, ConnectTimeoutException -> 0x02d7, IOException -> 0x02f8, Exception -> 0x0319, all -> 0x0346 }
            java.net.URL r2 = new java.net.URL     // Catch:{ MalformedURLException -> 0x01e2, SocketException -> 0x0275, SocketTimeoutException -> 0x02b6, ConnectTimeoutException -> 0x02d7, IOException -> 0x02f8, Exception -> 0x0319, all -> 0x0346 }
            com.snda.youni.f.c r0 = r9.f     // Catch:{ MalformedURLException -> 0x01e2, SocketException -> 0x0275, SocketTimeoutException -> 0x02b6, ConnectTimeoutException -> 0x02d7, IOException -> 0x02f8, Exception -> 0x0319, all -> 0x0346 }
            java.lang.String r0 = r0.g()     // Catch:{ MalformedURLException -> 0x01e2, SocketException -> 0x0275, SocketTimeoutException -> 0x02b6, ConnectTimeoutException -> 0x02d7, IOException -> 0x02f8, Exception -> 0x0319, all -> 0x0346 }
            r2.<init>(r0)     // Catch:{ MalformedURLException -> 0x01e2, SocketException -> 0x0275, SocketTimeoutException -> 0x02b6, ConnectTimeoutException -> 0x02d7, IOException -> 0x02f8, Exception -> 0x0319, all -> 0x0346 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ MalformedURLException -> 0x01e2, SocketException -> 0x0275, SocketTimeoutException -> 0x02b6, ConnectTimeoutException -> 0x02d7, IOException -> 0x02f8, Exception -> 0x0319, all -> 0x0346 }
            r0.<init>()     // Catch:{ MalformedURLException -> 0x01e2, SocketException -> 0x0275, SocketTimeoutException -> 0x02b6, ConnectTimeoutException -> 0x02d7, IOException -> 0x02f8, Exception -> 0x0319, all -> 0x0346 }
            java.lang.String r3 = "request url="
            java.lang.StringBuilder r0 = r0.append(r3)     // Catch:{ MalformedURLException -> 0x01e2, SocketException -> 0x0275, SocketTimeoutException -> 0x02b6, ConnectTimeoutException -> 0x02d7, IOException -> 0x02f8, Exception -> 0x0319, all -> 0x0346 }
            com.snda.youni.f.c r3 = r9.f     // Catch:{ MalformedURLException -> 0x01e2, SocketException -> 0x0275, SocketTimeoutException -> 0x02b6, ConnectTimeoutException -> 0x02d7, IOException -> 0x02f8, Exception -> 0x0319, all -> 0x0346 }
            java.lang.String r3 = r3.g()     // Catch:{ MalformedURLException -> 0x01e2, SocketException -> 0x0275, SocketTimeoutException -> 0x02b6, ConnectTimeoutException -> 0x02d7, IOException -> 0x02f8, Exception -> 0x0319, all -> 0x0346 }
            java.lang.StringBuilder r0 = r0.append(r3)     // Catch:{ MalformedURLException -> 0x01e2, SocketException -> 0x0275, SocketTimeoutException -> 0x02b6, ConnectTimeoutException -> 0x02d7, IOException -> 0x02f8, Exception -> 0x0319, all -> 0x0346 }
            r0.toString()     // Catch:{ MalformedURLException -> 0x01e2, SocketException -> 0x0275, SocketTimeoutException -> 0x02b6, ConnectTimeoutException -> 0x02d7, IOException -> 0x02f8, Exception -> 0x0319, all -> 0x0346 }
            boolean r0 = r9.g     // Catch:{ MalformedURLException -> 0x01e2, SocketException -> 0x0275, SocketTimeoutException -> 0x02b6, ConnectTimeoutException -> 0x02d7, IOException -> 0x02f8, Exception -> 0x0319, all -> 0x0346 }
            if (r0 == 0) goto L_0x0049
            r0 = 1
            java.lang.Integer[] r0 = new java.lang.Integer[r0]     // Catch:{ MalformedURLException -> 0x01e2, SocketException -> 0x0275, SocketTimeoutException -> 0x02b6, ConnectTimeoutException -> 0x02d7, IOException -> 0x02f8, Exception -> 0x0319, all -> 0x0346 }
            r3 = 0
            r4 = 1
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ MalformedURLException -> 0x01e2, SocketException -> 0x0275, SocketTimeoutException -> 0x02b6, ConnectTimeoutException -> 0x02d7, IOException -> 0x02f8, Exception -> 0x0319, all -> 0x0346 }
            r0[r3] = r4     // Catch:{ MalformedURLException -> 0x01e2, SocketException -> 0x0275, SocketTimeoutException -> 0x02b6, ConnectTimeoutException -> 0x02d7, IOException -> 0x02f8, Exception -> 0x0319, all -> 0x0346 }
            r9.publishProgress(r0)     // Catch:{ MalformedURLException -> 0x01e2, SocketException -> 0x0275, SocketTimeoutException -> 0x02b6, ConnectTimeoutException -> 0x02d7, IOException -> 0x02f8, Exception -> 0x0319, all -> 0x0346 }
        L_0x0049:
            android.content.Context r0 = r9.d     // Catch:{ MalformedURLException -> 0x01e2, SocketException -> 0x0275, SocketTimeoutException -> 0x02b6, ConnectTimeoutException -> 0x02d7, IOException -> 0x02f8, Exception -> 0x0319, all -> 0x0346 }
            java.lang.String r3 = "wifi"
            java.lang.Object r0 = r0.getSystemService(r3)     // Catch:{ MalformedURLException -> 0x01e2, SocketException -> 0x0275, SocketTimeoutException -> 0x02b6, ConnectTimeoutException -> 0x02d7, IOException -> 0x02f8, Exception -> 0x0319, all -> 0x0346 }
            android.net.wifi.WifiManager r0 = (android.net.wifi.WifiManager) r0     // Catch:{ MalformedURLException -> 0x01e2, SocketException -> 0x0275, SocketTimeoutException -> 0x02b6, ConnectTimeoutException -> 0x02d7, IOException -> 0x02f8, Exception -> 0x0319, all -> 0x0346 }
            java.lang.String r3 = android.net.Proxy.getDefaultHost()     // Catch:{ MalformedURLException -> 0x01e2, SocketException -> 0x0275, SocketTimeoutException -> 0x02b6, ConnectTimeoutException -> 0x02d7, IOException -> 0x02f8, Exception -> 0x0319, all -> 0x0346 }
            boolean r0 = r0.isWifiEnabled()     // Catch:{ MalformedURLException -> 0x01e2, SocketException -> 0x0275, SocketTimeoutException -> 0x02b6, ConnectTimeoutException -> 0x02d7, IOException -> 0x02f8, Exception -> 0x0319, all -> 0x0346 }
            if (r0 != 0) goto L_0x03ae
            if (r3 == 0) goto L_0x03ae
            java.lang.String r0 = ""
            boolean r0 = r0.equals(r3)     // Catch:{ MalformedURLException -> 0x01e2, SocketException -> 0x0275, SocketTimeoutException -> 0x02b6, ConnectTimeoutException -> 0x02d7, IOException -> 0x02f8, Exception -> 0x0319, all -> 0x0346 }
            if (r0 != 0) goto L_0x03ae
            r0 = r5
        L_0x0068:
            if (r0 == 0) goto L_0x01ce
            java.util.Properties r0 = java.lang.System.getProperties()     // Catch:{ MalformedURLException -> 0x01e2, SocketException -> 0x0275, SocketTimeoutException -> 0x02b6, ConnectTimeoutException -> 0x02d7, IOException -> 0x02f8, Exception -> 0x0319, all -> 0x0346 }
            java.lang.String r3 = "http.proxyHost"
            java.lang.String r4 = android.net.Proxy.getDefaultHost()     // Catch:{ MalformedURLException -> 0x01e2, SocketException -> 0x0275, SocketTimeoutException -> 0x02b6, ConnectTimeoutException -> 0x02d7, IOException -> 0x02f8, Exception -> 0x0319, all -> 0x0346 }
            r0.setProperty(r3, r4)     // Catch:{ MalformedURLException -> 0x01e2, SocketException -> 0x0275, SocketTimeoutException -> 0x02b6, ConnectTimeoutException -> 0x02d7, IOException -> 0x02f8, Exception -> 0x0319, all -> 0x0346 }
            java.util.Properties r0 = java.lang.System.getProperties()     // Catch:{ MalformedURLException -> 0x01e2, SocketException -> 0x0275, SocketTimeoutException -> 0x02b6, ConnectTimeoutException -> 0x02d7, IOException -> 0x02f8, Exception -> 0x0319, all -> 0x0346 }
            java.lang.String r3 = "http.proxyPort"
            int r4 = android.net.Proxy.getDefaultPort()     // Catch:{ MalformedURLException -> 0x01e2, SocketException -> 0x0275, SocketTimeoutException -> 0x02b6, ConnectTimeoutException -> 0x02d7, IOException -> 0x02f8, Exception -> 0x0319, all -> 0x0346 }
            java.lang.String r4 = java.lang.String.valueOf(r4)     // Catch:{ MalformedURLException -> 0x01e2, SocketException -> 0x0275, SocketTimeoutException -> 0x02b6, ConnectTimeoutException -> 0x02d7, IOException -> 0x02f8, Exception -> 0x0319, all -> 0x0346 }
            r0.setProperty(r3, r4)     // Catch:{ MalformedURLException -> 0x01e2, SocketException -> 0x0275, SocketTimeoutException -> 0x02b6, ConnectTimeoutException -> 0x02d7, IOException -> 0x02f8, Exception -> 0x0319, all -> 0x0346 }
        L_0x0088:
            java.net.URLConnection r0 = r2.openConnection()     // Catch:{ MalformedURLException -> 0x01e2, SocketException -> 0x0275, SocketTimeoutException -> 0x02b6, ConnectTimeoutException -> 0x02d7, IOException -> 0x02f8, Exception -> 0x0319, all -> 0x0346 }
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ MalformedURLException -> 0x01e2, SocketException -> 0x0275, SocketTimeoutException -> 0x02b6, ConnectTimeoutException -> 0x02d7, IOException -> 0x02f8, Exception -> 0x0319, all -> 0x0346 }
            r2 = 1
            r0.setDoOutput(r2)     // Catch:{ MalformedURLException -> 0x01e2, SocketException -> 0x0275, SocketTimeoutException -> 0x02b6, ConnectTimeoutException -> 0x02d7, IOException -> 0x02f8, Exception -> 0x0319, all -> 0x0346 }
            r2 = 1
            r0.setDoInput(r2)     // Catch:{ MalformedURLException -> 0x01e2, SocketException -> 0x0275, SocketTimeoutException -> 0x02b6, ConnectTimeoutException -> 0x02d7, IOException -> 0x02f8, Exception -> 0x0319, all -> 0x0346 }
            r2 = 0
            r0.setUseCaches(r2)     // Catch:{ MalformedURLException -> 0x01e2, SocketException -> 0x0275, SocketTimeoutException -> 0x02b6, ConnectTimeoutException -> 0x02d7, IOException -> 0x02f8, Exception -> 0x0319, all -> 0x0346 }
            com.snda.youni.f.c r2 = r9.f     // Catch:{ MalformedURLException -> 0x01e2, SocketException -> 0x0275, SocketTimeoutException -> 0x02b6, ConnectTimeoutException -> 0x02d7, IOException -> 0x02f8, Exception -> 0x0319, all -> 0x0346 }
            java.lang.String r2 = r2.e()     // Catch:{ MalformedURLException -> 0x01e2, SocketException -> 0x0275, SocketTimeoutException -> 0x02b6, ConnectTimeoutException -> 0x02d7, IOException -> 0x02f8, Exception -> 0x0319, all -> 0x0346 }
            r0.setRequestMethod(r2)     // Catch:{ MalformedURLException -> 0x01e2, SocketException -> 0x0275, SocketTimeoutException -> 0x02b6, ConnectTimeoutException -> 0x02d7, IOException -> 0x02f8, Exception -> 0x0319, all -> 0x0346 }
            int r2 = com.snda.youni.f.b.f405a     // Catch:{ MalformedURLException -> 0x01e2, SocketException -> 0x0275, SocketTimeoutException -> 0x02b6, ConnectTimeoutException -> 0x02d7, IOException -> 0x02f8, Exception -> 0x0319, all -> 0x0346 }
            r0.setConnectTimeout(r2)     // Catch:{ MalformedURLException -> 0x01e2, SocketException -> 0x0275, SocketTimeoutException -> 0x02b6, ConnectTimeoutException -> 0x02d7, IOException -> 0x02f8, Exception -> 0x0319, all -> 0x0346 }
            int r2 = com.snda.youni.f.b.b     // Catch:{ MalformedURLException -> 0x01e2, SocketException -> 0x0275, SocketTimeoutException -> 0x02b6, ConnectTimeoutException -> 0x02d7, IOException -> 0x02f8, Exception -> 0x0319, all -> 0x0346 }
            r0.setReadTimeout(r2)     // Catch:{ MalformedURLException -> 0x01e2, SocketException -> 0x0275, SocketTimeoutException -> 0x02b6, ConnectTimeoutException -> 0x02d7, IOException -> 0x02f8, Exception -> 0x0319, all -> 0x0346 }
            r2 = 1
            java.net.HttpURLConnection.setFollowRedirects(r2)     // Catch:{ MalformedURLException -> 0x01e2, SocketException -> 0x0275, SocketTimeoutException -> 0x02b6, ConnectTimeoutException -> 0x02d7, IOException -> 0x02f8, Exception -> 0x0319, all -> 0x0346 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ MalformedURLException -> 0x01e2, SocketException -> 0x0275, SocketTimeoutException -> 0x02b6, ConnectTimeoutException -> 0x02d7, IOException -> 0x02f8, Exception -> 0x0319, all -> 0x0346 }
            r2.<init>()     // Catch:{ MalformedURLException -> 0x01e2, SocketException -> 0x0275, SocketTimeoutException -> 0x02b6, ConnectTimeoutException -> 0x02d7, IOException -> 0x02f8, Exception -> 0x0319, all -> 0x0346 }
            java.lang.String r3 = "mReq zipped="
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ MalformedURLException -> 0x01e2, SocketException -> 0x0275, SocketTimeoutException -> 0x02b6, ConnectTimeoutException -> 0x02d7, IOException -> 0x02f8, Exception -> 0x0319, all -> 0x0346 }
            com.snda.youni.f.c r3 = r9.f     // Catch:{ MalformedURLException -> 0x01e2, SocketException -> 0x0275, SocketTimeoutException -> 0x02b6, ConnectTimeoutException -> 0x02d7, IOException -> 0x02f8, Exception -> 0x0319, all -> 0x0346 }
            boolean r3 = r3.a()     // Catch:{ MalformedURLException -> 0x01e2, SocketException -> 0x0275, SocketTimeoutException -> 0x02b6, ConnectTimeoutException -> 0x02d7, IOException -> 0x02f8, Exception -> 0x0319, all -> 0x0346 }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ MalformedURLException -> 0x01e2, SocketException -> 0x0275, SocketTimeoutException -> 0x02b6, ConnectTimeoutException -> 0x02d7, IOException -> 0x02f8, Exception -> 0x0319, all -> 0x0346 }
            r2.toString()     // Catch:{ MalformedURLException -> 0x01e2, SocketException -> 0x0275, SocketTimeoutException -> 0x02b6, ConnectTimeoutException -> 0x02d7, IOException -> 0x02f8, Exception -> 0x0319, all -> 0x0346 }
            com.snda.youni.f.c r2 = r9.f     // Catch:{ MalformedURLException -> 0x01e2, SocketException -> 0x0275, SocketTimeoutException -> 0x02b6, ConnectTimeoutException -> 0x02d7, IOException -> 0x02f8, Exception -> 0x0319, all -> 0x0346 }
            boolean r2 = r2.a()     // Catch:{ MalformedURLException -> 0x01e2, SocketException -> 0x0275, SocketTimeoutException -> 0x02b6, ConnectTimeoutException -> 0x02d7, IOException -> 0x02f8, Exception -> 0x0319, all -> 0x0346 }
            if (r2 == 0) goto L_0x00d8
            java.lang.String r2 = "Accept-Encoding"
            java.lang.String r3 = "gzip,deflate"
            r0.setRequestProperty(r2, r3)     // Catch:{ MalformedURLException -> 0x01e2, SocketException -> 0x0275, SocketTimeoutException -> 0x02b6, ConnectTimeoutException -> 0x02d7, IOException -> 0x02f8, Exception -> 0x0319, all -> 0x0346 }
        L_0x00d8:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ MalformedURLException -> 0x01e2, SocketException -> 0x0275, SocketTimeoutException -> 0x02b6, ConnectTimeoutException -> 0x02d7, IOException -> 0x02f8, Exception -> 0x0319, all -> 0x0346 }
            r2.<init>()     // Catch:{ MalformedURLException -> 0x01e2, SocketException -> 0x0275, SocketTimeoutException -> 0x02b6, ConnectTimeoutException -> 0x02d7, IOException -> 0x02f8, Exception -> 0x0319, all -> 0x0346 }
            java.lang.String r3 = "request method="
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ MalformedURLException -> 0x01e2, SocketException -> 0x0275, SocketTimeoutException -> 0x02b6, ConnectTimeoutException -> 0x02d7, IOException -> 0x02f8, Exception -> 0x0319, all -> 0x0346 }
            com.snda.youni.f.c r3 = r9.f     // Catch:{ MalformedURLException -> 0x01e2, SocketException -> 0x0275, SocketTimeoutException -> 0x02b6, ConnectTimeoutException -> 0x02d7, IOException -> 0x02f8, Exception -> 0x0319, all -> 0x0346 }
            java.lang.String r3 = r3.e()     // Catch:{ MalformedURLException -> 0x01e2, SocketException -> 0x0275, SocketTimeoutException -> 0x02b6, ConnectTimeoutException -> 0x02d7, IOException -> 0x02f8, Exception -> 0x0319, all -> 0x0346 }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ MalformedURLException -> 0x01e2, SocketException -> 0x0275, SocketTimeoutException -> 0x02b6, ConnectTimeoutException -> 0x02d7, IOException -> 0x02f8, Exception -> 0x0319, all -> 0x0346 }
            r2.toString()     // Catch:{ MalformedURLException -> 0x01e2, SocketException -> 0x0275, SocketTimeoutException -> 0x02b6, ConnectTimeoutException -> 0x02d7, IOException -> 0x02f8, Exception -> 0x0319, all -> 0x0346 }
            com.snda.youni.f.c r2 = r9.f     // Catch:{ MalformedURLException -> 0x01e2, SocketException -> 0x0275, SocketTimeoutException -> 0x02b6, ConnectTimeoutException -> 0x02d7, IOException -> 0x02f8, Exception -> 0x0319, all -> 0x0346 }
            java.lang.String r2 = r2.e()     // Catch:{ MalformedURLException -> 0x01e2, SocketException -> 0x0275, SocketTimeoutException -> 0x02b6, ConnectTimeoutException -> 0x02d7, IOException -> 0x02f8, Exception -> 0x0319, all -> 0x0346 }
            java.lang.String r3 = "POST"
            boolean r2 = r2.equals(r3)     // Catch:{ MalformedURLException -> 0x01e2, SocketException -> 0x0275, SocketTimeoutException -> 0x02b6, ConnectTimeoutException -> 0x02d7, IOException -> 0x02f8, Exception -> 0x0319, all -> 0x0346 }
            if (r2 == 0) goto L_0x03ab
            java.lang.String r2 = "Content-Type"
            com.snda.youni.f.c r3 = r9.f     // Catch:{ MalformedURLException -> 0x01e2, SocketException -> 0x0275, SocketTimeoutException -> 0x02b6, ConnectTimeoutException -> 0x02d7, IOException -> 0x02f8, Exception -> 0x0319, all -> 0x0346 }
            java.lang.String r3 = r3.h()     // Catch:{ MalformedURLException -> 0x01e2, SocketException -> 0x0275, SocketTimeoutException -> 0x02b6, ConnectTimeoutException -> 0x02d7, IOException -> 0x02f8, Exception -> 0x0319, all -> 0x0346 }
            r0.setRequestProperty(r2, r3)     // Catch:{ MalformedURLException -> 0x01e2, SocketException -> 0x0275, SocketTimeoutException -> 0x02b6, ConnectTimeoutException -> 0x02d7, IOException -> 0x02f8, Exception -> 0x0319, all -> 0x0346 }
            com.snda.youni.f.c r2 = r9.f     // Catch:{ MalformedURLException -> 0x01e2, SocketException -> 0x0275, SocketTimeoutException -> 0x02b6, ConnectTimeoutException -> 0x02d7, IOException -> 0x02f8, Exception -> 0x0319, all -> 0x0346 }
            byte[] r2 = r2.f()     // Catch:{ MalformedURLException -> 0x01e2, SocketException -> 0x0275, SocketTimeoutException -> 0x02b6, ConnectTimeoutException -> 0x02d7, IOException -> 0x02f8, Exception -> 0x0319, all -> 0x0346 }
            if (r2 == 0) goto L_0x03ab
            java.lang.String r2 = "Content-Length"
            com.snda.youni.f.c r3 = r9.f     // Catch:{ MalformedURLException -> 0x01e2, SocketException -> 0x0275, SocketTimeoutException -> 0x02b6, ConnectTimeoutException -> 0x02d7, IOException -> 0x02f8, Exception -> 0x0319, all -> 0x0346 }
            byte[] r3 = r3.f()     // Catch:{ MalformedURLException -> 0x01e2, SocketException -> 0x0275, SocketTimeoutException -> 0x02b6, ConnectTimeoutException -> 0x02d7, IOException -> 0x02f8, Exception -> 0x0319, all -> 0x0346 }
            int r3 = r3.length     // Catch:{ MalformedURLException -> 0x01e2, SocketException -> 0x0275, SocketTimeoutException -> 0x02b6, ConnectTimeoutException -> 0x02d7, IOException -> 0x02f8, Exception -> 0x0319, all -> 0x0346 }
            java.lang.String r3 = java.lang.String.valueOf(r3)     // Catch:{ MalformedURLException -> 0x01e2, SocketException -> 0x0275, SocketTimeoutException -> 0x02b6, ConnectTimeoutException -> 0x02d7, IOException -> 0x02f8, Exception -> 0x0319, all -> 0x0346 }
            r0.setRequestProperty(r2, r3)     // Catch:{ MalformedURLException -> 0x01e2, SocketException -> 0x0275, SocketTimeoutException -> 0x02b6, ConnectTimeoutException -> 0x02d7, IOException -> 0x02f8, Exception -> 0x0319, all -> 0x0346 }
            java.io.BufferedOutputStream r2 = new java.io.BufferedOutputStream     // Catch:{ MalformedURLException -> 0x01e2, SocketException -> 0x0275, SocketTimeoutException -> 0x02b6, ConnectTimeoutException -> 0x02d7, IOException -> 0x02f8, Exception -> 0x0319, all -> 0x0346 }
            java.io.OutputStream r3 = r0.getOutputStream()     // Catch:{ MalformedURLException -> 0x01e2, SocketException -> 0x0275, SocketTimeoutException -> 0x02b6, ConnectTimeoutException -> 0x02d7, IOException -> 0x02f8, Exception -> 0x0319, all -> 0x0346 }
            r2.<init>(r3)     // Catch:{ MalformedURLException -> 0x01e2, SocketException -> 0x0275, SocketTimeoutException -> 0x02b6, ConnectTimeoutException -> 0x02d7, IOException -> 0x02f8, Exception -> 0x0319, all -> 0x0346 }
            com.snda.youni.f.c r3 = r9.f     // Catch:{ MalformedURLException -> 0x03a6, SocketException -> 0x0395, SocketTimeoutException -> 0x0384, ConnectTimeoutException -> 0x0373, IOException -> 0x0365, Exception -> 0x0357, all -> 0x034b }
            byte[] r3 = r3.f()     // Catch:{ MalformedURLException -> 0x03a6, SocketException -> 0x0395, SocketTimeoutException -> 0x0384, ConnectTimeoutException -> 0x0373, IOException -> 0x0365, Exception -> 0x0357, all -> 0x034b }
            r2.write(r3)     // Catch:{ MalformedURLException -> 0x03a6, SocketException -> 0x0395, SocketTimeoutException -> 0x0384, ConnectTimeoutException -> 0x0373, IOException -> 0x0365, Exception -> 0x0357, all -> 0x034b }
            r2.flush()     // Catch:{ MalformedURLException -> 0x03a6, SocketException -> 0x0395, SocketTimeoutException -> 0x0384, ConnectTimeoutException -> 0x0373, IOException -> 0x0365, Exception -> 0x0357, all -> 0x034b }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ MalformedURLException -> 0x03a6, SocketException -> 0x0395, SocketTimeoutException -> 0x0384, ConnectTimeoutException -> 0x0373, IOException -> 0x0365, Exception -> 0x0357, all -> 0x034b }
            r3.<init>()     // Catch:{ MalformedURLException -> 0x03a6, SocketException -> 0x0395, SocketTimeoutException -> 0x0384, ConnectTimeoutException -> 0x0373, IOException -> 0x0365, Exception -> 0x0357, all -> 0x034b }
            java.lang.String r4 = "request post data length="
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ MalformedURLException -> 0x03a6, SocketException -> 0x0395, SocketTimeoutException -> 0x0384, ConnectTimeoutException -> 0x0373, IOException -> 0x0365, Exception -> 0x0357, all -> 0x034b }
            com.snda.youni.f.c r4 = r9.f     // Catch:{ MalformedURLException -> 0x03a6, SocketException -> 0x0395, SocketTimeoutException -> 0x0384, ConnectTimeoutException -> 0x0373, IOException -> 0x0365, Exception -> 0x0357, all -> 0x034b }
            byte[] r4 = r4.f()     // Catch:{ MalformedURLException -> 0x03a6, SocketException -> 0x0395, SocketTimeoutException -> 0x0384, ConnectTimeoutException -> 0x0373, IOException -> 0x0365, Exception -> 0x0357, all -> 0x034b }
            int r4 = r4.length     // Catch:{ MalformedURLException -> 0x03a6, SocketException -> 0x0395, SocketTimeoutException -> 0x0384, ConnectTimeoutException -> 0x0373, IOException -> 0x0365, Exception -> 0x0357, all -> 0x034b }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ MalformedURLException -> 0x03a6, SocketException -> 0x0395, SocketTimeoutException -> 0x0384, ConnectTimeoutException -> 0x0373, IOException -> 0x0365, Exception -> 0x0357, all -> 0x034b }
            r3.toString()     // Catch:{ MalformedURLException -> 0x03a6, SocketException -> 0x0395, SocketTimeoutException -> 0x0384, ConnectTimeoutException -> 0x0373, IOException -> 0x0365, Exception -> 0x0357, all -> 0x034b }
        L_0x014f:
            int r3 = r0.getResponseCode()     // Catch:{ MalformedURLException -> 0x03a6, SocketException -> 0x0395, SocketTimeoutException -> 0x0384, ConnectTimeoutException -> 0x0373, IOException -> 0x0365, Exception -> 0x0357, all -> 0x034b }
            r1.b(r3)     // Catch:{ MalformedURLException -> 0x03a6, SocketException -> 0x0395, SocketTimeoutException -> 0x0384, ConnectTimeoutException -> 0x0373, IOException -> 0x0365, Exception -> 0x0357, all -> 0x034b }
            java.lang.String r4 = r0.getContentType()     // Catch:{ MalformedURLException -> 0x03a6, SocketException -> 0x0395, SocketTimeoutException -> 0x0384, ConnectTimeoutException -> 0x0373, IOException -> 0x0365, Exception -> 0x0357, all -> 0x034b }
            r1.a(r4)     // Catch:{ MalformedURLException -> 0x03a6, SocketException -> 0x0395, SocketTimeoutException -> 0x0384, ConnectTimeoutException -> 0x0373, IOException -> 0x0365, Exception -> 0x0357, all -> 0x034b }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ MalformedURLException -> 0x03a6, SocketException -> 0x0395, SocketTimeoutException -> 0x0384, ConnectTimeoutException -> 0x0373, IOException -> 0x0365, Exception -> 0x0357, all -> 0x034b }
            r4.<init>()     // Catch:{ MalformedURLException -> 0x03a6, SocketException -> 0x0395, SocketTimeoutException -> 0x0384, ConnectTimeoutException -> 0x0373, IOException -> 0x0365, Exception -> 0x0357, all -> 0x034b }
            java.lang.String r5 = "req="
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ MalformedURLException -> 0x03a6, SocketException -> 0x0395, SocketTimeoutException -> 0x0384, ConnectTimeoutException -> 0x0373, IOException -> 0x0365, Exception -> 0x0357, all -> 0x034b }
            com.snda.youni.f.c r5 = r9.f     // Catch:{ MalformedURLException -> 0x03a6, SocketException -> 0x0395, SocketTimeoutException -> 0x0384, ConnectTimeoutException -> 0x0373, IOException -> 0x0365, Exception -> 0x0357, all -> 0x034b }
            java.lang.String r5 = r5.g()     // Catch:{ MalformedURLException -> 0x03a6, SocketException -> 0x0395, SocketTimeoutException -> 0x0384, ConnectTimeoutException -> 0x0373, IOException -> 0x0365, Exception -> 0x0357, all -> 0x034b }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ MalformedURLException -> 0x03a6, SocketException -> 0x0395, SocketTimeoutException -> 0x0384, ConnectTimeoutException -> 0x0373, IOException -> 0x0365, Exception -> 0x0357, all -> 0x034b }
            java.lang.String r5 = "response code="
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ MalformedURLException -> 0x03a6, SocketException -> 0x0395, SocketTimeoutException -> 0x0384, ConnectTimeoutException -> 0x0373, IOException -> 0x0365, Exception -> 0x0357, all -> 0x034b }
            java.lang.StringBuilder r4 = r4.append(r3)     // Catch:{ MalformedURLException -> 0x03a6, SocketException -> 0x0395, SocketTimeoutException -> 0x0384, ConnectTimeoutException -> 0x0373, IOException -> 0x0365, Exception -> 0x0357, all -> 0x034b }
            java.lang.String r5 = "  xxxxxxx  "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ MalformedURLException -> 0x03a6, SocketException -> 0x0395, SocketTimeoutException -> 0x0384, ConnectTimeoutException -> 0x0373, IOException -> 0x0365, Exception -> 0x0357, all -> 0x034b }
            r4.toString()     // Catch:{ MalformedURLException -> 0x03a6, SocketException -> 0x0395, SocketTimeoutException -> 0x0384, ConnectTimeoutException -> 0x0373, IOException -> 0x0365, Exception -> 0x0357, all -> 0x034b }
            r4 = 200(0xc8, float:2.8E-43)
            if (r3 != r4) goto L_0x0217
            boolean r3 = r9.g     // Catch:{ MalformedURLException -> 0x03a6, SocketException -> 0x0395, SocketTimeoutException -> 0x0384, ConnectTimeoutException -> 0x0373, IOException -> 0x0365, Exception -> 0x0357, all -> 0x034b }
            if (r3 == 0) goto L_0x019b
            r3 = 1
            java.lang.Integer[] r3 = new java.lang.Integer[r3]     // Catch:{ MalformedURLException -> 0x03a6, SocketException -> 0x0395, SocketTimeoutException -> 0x0384, ConnectTimeoutException -> 0x0373, IOException -> 0x0365, Exception -> 0x0357, all -> 0x034b }
            r4 = 0
            r5 = 2
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)     // Catch:{ MalformedURLException -> 0x03a6, SocketException -> 0x0395, SocketTimeoutException -> 0x0384, ConnectTimeoutException -> 0x0373, IOException -> 0x0365, Exception -> 0x0357, all -> 0x034b }
            r3[r4] = r5     // Catch:{ MalformedURLException -> 0x03a6, SocketException -> 0x0395, SocketTimeoutException -> 0x0384, ConnectTimeoutException -> 0x0373, IOException -> 0x0365, Exception -> 0x0357, all -> 0x034b }
            r9.publishProgress(r3)     // Catch:{ MalformedURLException -> 0x03a6, SocketException -> 0x0395, SocketTimeoutException -> 0x0384, ConnectTimeoutException -> 0x0373, IOException -> 0x0365, Exception -> 0x0357, all -> 0x034b }
        L_0x019b:
            java.io.BufferedInputStream r3 = new java.io.BufferedInputStream     // Catch:{ MalformedURLException -> 0x03a6, SocketException -> 0x0395, SocketTimeoutException -> 0x0384, ConnectTimeoutException -> 0x0373, IOException -> 0x0365, Exception -> 0x0357, all -> 0x034b }
            java.io.InputStream r0 = r0.getInputStream()     // Catch:{ MalformedURLException -> 0x03a6, SocketException -> 0x0395, SocketTimeoutException -> 0x0384, ConnectTimeoutException -> 0x0373, IOException -> 0x0365, Exception -> 0x0357, all -> 0x034b }
            r3.<init>(r0)     // Catch:{ MalformedURLException -> 0x03a6, SocketException -> 0x0395, SocketTimeoutException -> 0x0384, ConnectTimeoutException -> 0x0373, IOException -> 0x0365, Exception -> 0x0357, all -> 0x034b }
            java.io.ByteArrayOutputStream r0 = new java.io.ByteArrayOutputStream     // Catch:{ MalformedURLException -> 0x01b8, SocketException -> 0x039a, SocketTimeoutException -> 0x0389, ConnectTimeoutException -> 0x0378, IOException -> 0x0369, Exception -> 0x035b, all -> 0x034f }
            r0.<init>()     // Catch:{ MalformedURLException -> 0x01b8, SocketException -> 0x039a, SocketTimeoutException -> 0x0389, ConnectTimeoutException -> 0x0378, IOException -> 0x0369, Exception -> 0x035b, all -> 0x034f }
            r4 = 1024(0x400, float:1.435E-42)
            byte[] r4 = new byte[r4]     // Catch:{ MalformedURLException -> 0x01b8, SocketException -> 0x039a, SocketTimeoutException -> 0x0389, ConnectTimeoutException -> 0x0378, IOException -> 0x0369, Exception -> 0x035b, all -> 0x034f }
        L_0x01ad:
            int r5 = r3.read(r4)     // Catch:{ MalformedURLException -> 0x01b8, SocketException -> 0x039a, SocketTimeoutException -> 0x0389, ConnectTimeoutException -> 0x0378, IOException -> 0x0369, Exception -> 0x035b, all -> 0x034f }
            if (r5 <= 0) goto L_0x01e6
            r6 = 0
            r0.write(r4, r6, r5)     // Catch:{ MalformedURLException -> 0x01b8, SocketException -> 0x039a, SocketTimeoutException -> 0x0389, ConnectTimeoutException -> 0x0378, IOException -> 0x0369, Exception -> 0x035b, all -> 0x034f }
            goto L_0x01ad
        L_0x01b8:
            r0 = move-exception
            r8 = r3
            r3 = r2
            r2 = r8
        L_0x01bc:
            r4 = 4
            r1.a(r4)     // Catch:{ all -> 0x02a3 }
            r0.printStackTrace()     // Catch:{ all -> 0x02a3 }
            if (r2 == 0) goto L_0x01c8
            r2.close()     // Catch:{ Exception -> 0x0269 }
        L_0x01c8:
            if (r3 == 0) goto L_0x01cd
            r3.close()     // Catch:{ IOException -> 0x026f }
        L_0x01cd:
            return r1
        L_0x01ce:
            java.util.Properties r0 = java.lang.System.getProperties()     // Catch:{ MalformedURLException -> 0x01e2, SocketException -> 0x0275, SocketTimeoutException -> 0x02b6, ConnectTimeoutException -> 0x02d7, IOException -> 0x02f8, Exception -> 0x0319, all -> 0x0346 }
            java.lang.String r3 = "http.proxyHost"
            r0.remove(r3)     // Catch:{ MalformedURLException -> 0x01e2, SocketException -> 0x0275, SocketTimeoutException -> 0x02b6, ConnectTimeoutException -> 0x02d7, IOException -> 0x02f8, Exception -> 0x0319, all -> 0x0346 }
            java.util.Properties r0 = java.lang.System.getProperties()     // Catch:{ MalformedURLException -> 0x01e2, SocketException -> 0x0275, SocketTimeoutException -> 0x02b6, ConnectTimeoutException -> 0x02d7, IOException -> 0x02f8, Exception -> 0x0319, all -> 0x0346 }
            java.lang.String r3 = "http.proxyPort"
            r0.remove(r3)     // Catch:{ MalformedURLException -> 0x01e2, SocketException -> 0x0275, SocketTimeoutException -> 0x02b6, ConnectTimeoutException -> 0x02d7, IOException -> 0x02f8, Exception -> 0x0319, all -> 0x0346 }
            goto L_0x0088
        L_0x01e2:
            r0 = move-exception
            r2 = r7
            r3 = r7
            goto L_0x01bc
        L_0x01e6:
            byte[] r4 = r0.toByteArray()     // Catch:{ MalformedURLException -> 0x01b8, SocketException -> 0x039a, SocketTimeoutException -> 0x0389, ConnectTimeoutException -> 0x0378, IOException -> 0x0369, Exception -> 0x035b, all -> 0x034f }
            com.snda.youni.f.c r5 = r9.f     // Catch:{ MalformedURLException -> 0x01b8, SocketException -> 0x039a, SocketTimeoutException -> 0x0389, ConnectTimeoutException -> 0x0378, IOException -> 0x0369, Exception -> 0x035b, all -> 0x034f }
            boolean r5 = r5.a()     // Catch:{ MalformedURLException -> 0x01b8, SocketException -> 0x039a, SocketTimeoutException -> 0x0389, ConnectTimeoutException -> 0x0378, IOException -> 0x0369, Exception -> 0x035b, all -> 0x034f }
            if (r5 == 0) goto L_0x01fa
            byte[] r4 = r0.toByteArray()     // Catch:{ MalformedURLException -> 0x01b8, SocketException -> 0x039a, SocketTimeoutException -> 0x0389, ConnectTimeoutException -> 0x0378, IOException -> 0x0369, Exception -> 0x035b, all -> 0x034f }
            byte[] r4 = com.snda.youni.e.j.a(r4)     // Catch:{ MalformedURLException -> 0x01b8, SocketException -> 0x039a, SocketTimeoutException -> 0x0389, ConnectTimeoutException -> 0x0378, IOException -> 0x0369, Exception -> 0x035b, all -> 0x034f }
        L_0x01fa:
            com.snda.youni.f.c r5 = r9.f     // Catch:{ MalformedURLException -> 0x01b8, SocketException -> 0x039a, SocketTimeoutException -> 0x0389, ConnectTimeoutException -> 0x0378, IOException -> 0x0369, Exception -> 0x035b, all -> 0x034f }
            java.lang.Object r5 = r5.c()     // Catch:{ MalformedURLException -> 0x01b8, SocketException -> 0x039a, SocketTimeoutException -> 0x0389, ConnectTimeoutException -> 0x0378, IOException -> 0x0369, Exception -> 0x035b, all -> 0x034f }
            com.snda.youni.c.t.a(r5, r1, r4)     // Catch:{ MalformedURLException -> 0x01b8, SocketException -> 0x039a, SocketTimeoutException -> 0x0389, ConnectTimeoutException -> 0x0378, IOException -> 0x0369, Exception -> 0x035b, all -> 0x034f }
            r0.close()     // Catch:{ MalformedURLException -> 0x01b8, SocketException -> 0x039a, SocketTimeoutException -> 0x0389, ConnectTimeoutException -> 0x0378, IOException -> 0x0369, Exception -> 0x035b, all -> 0x034f }
            r0 = r3
        L_0x0207:
            if (r0 == 0) goto L_0x020c
            r0.close()     // Catch:{ Exception -> 0x0264 }
        L_0x020c:
            if (r2 == 0) goto L_0x01cd
            r2.close()     // Catch:{ IOException -> 0x0212 }
            goto L_0x01cd
        L_0x0212:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x01cd
        L_0x0217:
            r4 = 302(0x12e, float:4.23E-43)
            if (r3 != r4) goto L_0x0222
            r0 = 9
            r1.a(r0)     // Catch:{ MalformedURLException -> 0x03a6, SocketException -> 0x0395, SocketTimeoutException -> 0x0384, ConnectTimeoutException -> 0x0373, IOException -> 0x0365, Exception -> 0x0357, all -> 0x034b }
            r0 = r7
            goto L_0x0207
        L_0x0222:
            java.io.BufferedInputStream r3 = new java.io.BufferedInputStream     // Catch:{ MalformedURLException -> 0x03a6, SocketException -> 0x0395, SocketTimeoutException -> 0x0384, ConnectTimeoutException -> 0x0373, IOException -> 0x0365, Exception -> 0x0357, all -> 0x034b }
            java.io.InputStream r0 = r0.getInputStream()     // Catch:{ MalformedURLException -> 0x03a6, SocketException -> 0x0395, SocketTimeoutException -> 0x0384, ConnectTimeoutException -> 0x0373, IOException -> 0x0365, Exception -> 0x0357, all -> 0x034b }
            r3.<init>(r0)     // Catch:{ MalformedURLException -> 0x03a6, SocketException -> 0x0395, SocketTimeoutException -> 0x0384, ConnectTimeoutException -> 0x0373, IOException -> 0x0365, Exception -> 0x0357, all -> 0x034b }
            java.io.ByteArrayOutputStream r0 = new java.io.ByteArrayOutputStream     // Catch:{ MalformedURLException -> 0x023f, SocketException -> 0x03a0, SocketTimeoutException -> 0x038f, ConnectTimeoutException -> 0x037e, IOException -> 0x036e, Exception -> 0x0360, all -> 0x0353 }
            r0.<init>()     // Catch:{ MalformedURLException -> 0x023f, SocketException -> 0x03a0, SocketTimeoutException -> 0x038f, ConnectTimeoutException -> 0x037e, IOException -> 0x036e, Exception -> 0x0360, all -> 0x0353 }
            r4 = 1024(0x400, float:1.435E-42)
            byte[] r4 = new byte[r4]     // Catch:{ MalformedURLException -> 0x023f, SocketException -> 0x03a0, SocketTimeoutException -> 0x038f, ConnectTimeoutException -> 0x037e, IOException -> 0x036e, Exception -> 0x0360, all -> 0x0353 }
        L_0x0234:
            int r5 = r3.read(r4)     // Catch:{ MalformedURLException -> 0x023f, SocketException -> 0x03a0, SocketTimeoutException -> 0x038f, ConnectTimeoutException -> 0x037e, IOException -> 0x036e, Exception -> 0x0360, all -> 0x0353 }
            if (r5 <= 0) goto L_0x0245
            r6 = 0
            r0.write(r4, r6, r5)     // Catch:{ MalformedURLException -> 0x023f, SocketException -> 0x03a0, SocketTimeoutException -> 0x038f, ConnectTimeoutException -> 0x037e, IOException -> 0x036e, Exception -> 0x0360, all -> 0x0353 }
            goto L_0x0234
        L_0x023f:
            r0 = move-exception
            r8 = r3
            r3 = r2
            r2 = r8
            goto L_0x01bc
        L_0x0245:
            byte[] r0 = r0.toByteArray()     // Catch:{ MalformedURLException -> 0x023f, SocketException -> 0x03a0, SocketTimeoutException -> 0x038f, ConnectTimeoutException -> 0x037e, IOException -> 0x036e, Exception -> 0x0360, all -> 0x0353 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ MalformedURLException -> 0x023f, SocketException -> 0x03a0, SocketTimeoutException -> 0x038f, ConnectTimeoutException -> 0x037e, IOException -> 0x036e, Exception -> 0x0360, all -> 0x0353 }
            r4.<init>()     // Catch:{ MalformedURLException -> 0x023f, SocketException -> 0x03a0, SocketTimeoutException -> 0x038f, ConnectTimeoutException -> 0x037e, IOException -> 0x036e, Exception -> 0x0360, all -> 0x0353 }
            java.lang.String r5 = "content="
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ MalformedURLException -> 0x023f, SocketException -> 0x03a0, SocketTimeoutException -> 0x038f, ConnectTimeoutException -> 0x037e, IOException -> 0x036e, Exception -> 0x0360, all -> 0x0353 }
            java.lang.String r5 = new java.lang.String     // Catch:{ MalformedURLException -> 0x023f, SocketException -> 0x03a0, SocketTimeoutException -> 0x038f, ConnectTimeoutException -> 0x037e, IOException -> 0x036e, Exception -> 0x0360, all -> 0x0353 }
            java.lang.String r6 = "utf-8"
            r5.<init>(r0, r6)     // Catch:{ MalformedURLException -> 0x023f, SocketException -> 0x03a0, SocketTimeoutException -> 0x038f, ConnectTimeoutException -> 0x037e, IOException -> 0x036e, Exception -> 0x0360, all -> 0x0353 }
            java.lang.StringBuilder r0 = r4.append(r5)     // Catch:{ MalformedURLException -> 0x023f, SocketException -> 0x03a0, SocketTimeoutException -> 0x038f, ConnectTimeoutException -> 0x037e, IOException -> 0x036e, Exception -> 0x0360, all -> 0x0353 }
            r0.toString()     // Catch:{ MalformedURLException -> 0x023f, SocketException -> 0x03a0, SocketTimeoutException -> 0x038f, ConnectTimeoutException -> 0x037e, IOException -> 0x036e, Exception -> 0x0360, all -> 0x0353 }
            r0 = r3
            goto L_0x0207
        L_0x0264:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x020c
        L_0x0269:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x01c8
        L_0x026f:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x01cd
        L_0x0275:
            r0 = move-exception
            r2 = r7
            r3 = r7
        L_0x0278:
            java.lang.String r4 = r0.getMessage()     // Catch:{ all -> 0x02a3 }
            java.lang.String r5 = "unreachable"
            int r4 = r4.indexOf(r5)     // Catch:{ all -> 0x02a3 }
            r5 = -1
            if (r4 == r5) goto L_0x029e
            r4 = 6
            r1.a(r4)     // Catch:{ all -> 0x02a3 }
        L_0x0289:
            r0.printStackTrace()     // Catch:{ all -> 0x02a3 }
            if (r2 == 0) goto L_0x0291
            r2.close()     // Catch:{ Exception -> 0x02b1 }
        L_0x0291:
            if (r3 == 0) goto L_0x01cd
            r3.close()     // Catch:{ IOException -> 0x0298 }
            goto L_0x01cd
        L_0x0298:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x01cd
        L_0x029e:
            r4 = 3
            r1.a(r4)     // Catch:{ all -> 0x02a3 }
            goto L_0x0289
        L_0x02a3:
            r0 = move-exception
            r1 = r2
            r2 = r3
        L_0x02a6:
            if (r1 == 0) goto L_0x02ab
            r1.close()     // Catch:{ Exception -> 0x033a }
        L_0x02ab:
            if (r2 == 0) goto L_0x02b0
            r2.close()     // Catch:{ IOException -> 0x0340 }
        L_0x02b0:
            throw r0
        L_0x02b1:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0291
        L_0x02b6:
            r0 = move-exception
            r2 = r7
            r3 = r7
        L_0x02b9:
            r4 = 1
            r1.a(r4)     // Catch:{ all -> 0x02a3 }
            r0.printStackTrace()     // Catch:{ all -> 0x02a3 }
            if (r2 == 0) goto L_0x02c5
            r2.close()     // Catch:{ Exception -> 0x02d2 }
        L_0x02c5:
            if (r3 == 0) goto L_0x01cd
            r3.close()     // Catch:{ IOException -> 0x02cc }
            goto L_0x01cd
        L_0x02cc:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x01cd
        L_0x02d2:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x02c5
        L_0x02d7:
            r0 = move-exception
            r2 = r7
            r3 = r7
        L_0x02da:
            r4 = 1
            r1.a(r4)     // Catch:{ all -> 0x02a3 }
            r0.printStackTrace()     // Catch:{ all -> 0x02a3 }
            if (r2 == 0) goto L_0x02e6
            r2.close()     // Catch:{ Exception -> 0x02f3 }
        L_0x02e6:
            if (r3 == 0) goto L_0x01cd
            r3.close()     // Catch:{ IOException -> 0x02ed }
            goto L_0x01cd
        L_0x02ed:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x01cd
        L_0x02f3:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x02e6
        L_0x02f8:
            r0 = move-exception
            r2 = r7
            r3 = r7
        L_0x02fb:
            r4 = 3
            r1.a(r4)     // Catch:{ all -> 0x02a3 }
            r0.printStackTrace()     // Catch:{ all -> 0x02a3 }
            if (r2 == 0) goto L_0x0307
            r2.close()     // Catch:{ Exception -> 0x0314 }
        L_0x0307:
            if (r3 == 0) goto L_0x01cd
            r3.close()     // Catch:{ IOException -> 0x030e }
            goto L_0x01cd
        L_0x030e:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x01cd
        L_0x0314:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0307
        L_0x0319:
            r0 = move-exception
            r2 = r7
            r3 = r7
        L_0x031c:
            r4 = 5
            r1.a(r4)     // Catch:{ all -> 0x02a3 }
            r0.printStackTrace()     // Catch:{ all -> 0x02a3 }
            if (r2 == 0) goto L_0x0328
            r2.close()     // Catch:{ Exception -> 0x0335 }
        L_0x0328:
            if (r3 == 0) goto L_0x01cd
            r3.close()     // Catch:{ IOException -> 0x032f }
            goto L_0x01cd
        L_0x032f:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x01cd
        L_0x0335:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0328
        L_0x033a:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x02ab
        L_0x0340:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x02b0
        L_0x0346:
            r0 = move-exception
            r1 = r7
            r2 = r7
            goto L_0x02a6
        L_0x034b:
            r0 = move-exception
            r1 = r7
            goto L_0x02a6
        L_0x034f:
            r0 = move-exception
            r1 = r3
            goto L_0x02a6
        L_0x0353:
            r0 = move-exception
            r1 = r3
            goto L_0x02a6
        L_0x0357:
            r0 = move-exception
            r3 = r2
            r2 = r7
            goto L_0x031c
        L_0x035b:
            r0 = move-exception
            r8 = r3
            r3 = r2
            r2 = r8
            goto L_0x031c
        L_0x0360:
            r0 = move-exception
            r8 = r3
            r3 = r2
            r2 = r8
            goto L_0x031c
        L_0x0365:
            r0 = move-exception
            r3 = r2
            r2 = r7
            goto L_0x02fb
        L_0x0369:
            r0 = move-exception
            r8 = r3
            r3 = r2
            r2 = r8
            goto L_0x02fb
        L_0x036e:
            r0 = move-exception
            r8 = r3
            r3 = r2
            r2 = r8
            goto L_0x02fb
        L_0x0373:
            r0 = move-exception
            r3 = r2
            r2 = r7
            goto L_0x02da
        L_0x0378:
            r0 = move-exception
            r8 = r3
            r3 = r2
            r2 = r8
            goto L_0x02da
        L_0x037e:
            r0 = move-exception
            r8 = r3
            r3 = r2
            r2 = r8
            goto L_0x02da
        L_0x0384:
            r0 = move-exception
            r3 = r2
            r2 = r7
            goto L_0x02b9
        L_0x0389:
            r0 = move-exception
            r8 = r3
            r3 = r2
            r2 = r8
            goto L_0x02b9
        L_0x038f:
            r0 = move-exception
            r8 = r3
            r3 = r2
            r2 = r8
            goto L_0x02b9
        L_0x0395:
            r0 = move-exception
            r3 = r2
            r2 = r7
            goto L_0x0278
        L_0x039a:
            r0 = move-exception
            r8 = r3
            r3 = r2
            r2 = r8
            goto L_0x0278
        L_0x03a0:
            r0 = move-exception
            r8 = r3
            r3 = r2
            r2 = r8
            goto L_0x0278
        L_0x03a6:
            r0 = move-exception
            r3 = r2
            r2 = r7
            goto L_0x01bc
        L_0x03ab:
            r2 = r7
            goto L_0x014f
        L_0x03ae:
            r0 = r6
            goto L_0x0068
        */
        throw new UnsupportedOperationException("Method not decompiled: com.snda.youni.f.b.doInBackground(com.snda.youni.f.c[]):com.snda.youni.f.d");
    }

    public final void a(a aVar) {
        this.c = aVar;
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void onPostExecute(Object obj) {
        d dVar = (d) obj;
        try {
            if (this.c != null) {
                this.c.a(this.f, dVar);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
            if (this.c != null) {
                this.c.a(e2, "http response execute error");
            }
        }
        if (this.g) {
            publishProgress(3);
        }
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void onProgressUpdate(Object[] objArr) {
        Integer[] numArr = (Integer[]) objArr;
        super.onProgressUpdate(numArr);
        try {
            if (this.d != null) {
                if (this.e == null) {
                    this.e = new ProgressDialog(this.d);
                }
                this.e.setIndeterminate(true);
                this.e.setCancelable(true);
                switch (numArr[0].intValue()) {
                    case 1:
                        this.e.setMessage(this.d.getResources().getString(C0000R.string.connecting_to_network));
                        break;
                    case 2:
                        this.e.setMessage(this.d.getResources().getString(C0000R.string.reading_data));
                        break;
                    case 3:
                        this.e.dismiss();
                        return;
                }
                if (!this.e.isShowing()) {
                    this.e.show();
                }
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }
}
