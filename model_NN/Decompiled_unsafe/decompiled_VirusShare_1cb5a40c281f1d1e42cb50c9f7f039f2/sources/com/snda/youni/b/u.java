package com.snda.youni.b;

import android.net.ConnectivityManager;
import org.jivesoftware.smack.b.o;

final class u extends Thread {

    /* renamed from: a  reason: collision with root package name */
    private o f363a;
    private /* synthetic */ ai b;

    /* synthetic */ u(ai aiVar) {
        this(aiVar, (byte) 0);
    }

    private u(ai aiVar, byte b2) {
        this.b = aiVar;
        this.f363a = null;
    }

    private m a() {
        m mVar = null;
        try {
            m mVar2 = (m) this.b.g.take();
            try {
                this.f363a = m.a(mVar2);
                return mVar2;
            } catch (InterruptedException e) {
                InterruptedException interruptedException = e;
                mVar = mVar2;
                e = interruptedException;
            }
        } catch (InterruptedException e2) {
            e = e2;
            e.printStackTrace();
            return mVar;
        }
    }

    private void a(m mVar) {
        try {
            this.b.h.put(mVar);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public final void run() {
        while (true) {
            m a2 = a();
            if (((ConnectivityManager) this.b.v.getSystemService("connectivity")).getActiveNetworkInfo() == null) {
                this.b.c(this.f363a.f());
                this.b.a(0);
            } else if (this.b.b()) {
                try {
                    "packet id: " + this.f363a.f();
                    this.b.p.a(this.f363a);
                } catch (Exception e) {
                    this.b.a(1);
                }
                a(a2);
            } else {
                this.b.a(1);
                a(a2);
            }
        }
    }
}
