package com.snda.youni.b;

import android.os.SystemClock;
import java.util.ArrayList;
import java.util.List;

public final class f {

    /* renamed from: a  reason: collision with root package name */
    private static String f351a = "ListResource";
    private int b;
    private List c;

    public f() {
        this.b = 20;
        this.c = null;
        this.c = new ArrayList();
    }

    public final String a(String str) {
        if (str == null) {
            return null;
        }
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= this.c.size()) {
                return null;
            }
            if (str.equals(((c) this.c.get(i2)).f349a)) {
                return ((c) this.c.get(i2)).b;
            }
            i = i2 + 1;
        }
    }

    public final void a() {
        this.b = 20;
    }

    public final void a(String str, String str2) {
        int i;
        if (str != null && str2 != null) {
            if (this.c.size() == this.b) {
                long j = ((c) this.c.get(0)).c;
                int i2 = -1;
                int i3 = 0;
                while (i3 < this.b) {
                    if (j < ((c) this.c.get(i3)).c) {
                        j = ((c) this.c.get(i3)).c;
                        i = i3;
                    } else {
                        i = i2;
                    }
                    i3++;
                    i2 = i;
                }
                c cVar = new c(this);
                cVar.f349a = str;
                cVar.b = str2;
                cVar.c = SystemClock.elapsedRealtime();
                this.c.set(i2, cVar);
                "maxsize = phone " + cVar + " resource " + str2;
                return;
            }
            int i4 = 0;
            while (i4 < this.c.size() && !str.equals(((c) this.c.get(i4)).f349a)) {
                i4++;
            }
            if (i4 < this.c.size()) {
                c cVar2 = new c(this);
                cVar2.f349a = str;
                cVar2.b = str2;
                cVar2.c = SystemClock.elapsedRealtime();
                this.c.set(i4, cVar2);
                "index < size = phone " + cVar2 + " resource " + str2;
                return;
            }
            c cVar3 = new c(this);
            cVar3.f349a = str;
            cVar3.b = str2;
            cVar3.c = SystemClock.elapsedRealtime();
            this.c.add(cVar3);
            "index == size = phone " + cVar3 + " resource " + str2;
        }
    }
}
