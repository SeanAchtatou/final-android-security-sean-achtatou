package com.snda.youni.b;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import com.snda.youni.g.a;

final class q extends BroadcastReceiver {

    /* renamed from: a  reason: collision with root package name */
    private q f360a = null;
    private /* synthetic */ ai b;

    public q(ai aiVar) {
        this.b = aiVar;
        if (this.f360a != null) {
            a();
            this.f360a = null;
        }
        this.f360a = this;
        q qVar = this.f360a;
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
        qVar.b.v.registerReceiver(qVar.f360a, intentFilter);
    }

    public final void a() {
        this.b.v.unregisterReceiver(this.f360a);
    }

    public final void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals("android.net.conn.CONNECTIVITY_CHANGE")) {
            a.a(this.b.v, "network_switch", null);
            if (this.b.q == null) {
                "has not init... UserJid : " + this.b.q;
                return;
            }
            NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
            if (activeNetworkInfo == null) {
                "onReceive mLastNetworkState " + this.b.x + " ni == null";
                if (this.b.x != null) {
                    String unused = this.b.x = (String) null;
                    String unused2 = this.b.w = null;
                    this.b.a(0);
                    return;
                }
                return;
            }
            if (this.b.y != null) {
                t.a(this.b.v);
            }
            "ni != null && network is : " + activeNetworkInfo.getTypeName();
            if (activeNetworkInfo.getTypeName().equals("WIFI")) {
                String unused3 = this.b.x = "WIFI";
                String macAddress = ((WifiManager) this.b.v.getSystemService("wifi")).getConnectionInfo().getMacAddress();
                "last wifi is : " + this.b.w;
                "now wifi is : " + macAddress;
                if (macAddress == null || !macAddress.equals(this.b.w)) {
                    String unused4 = this.b.w = macAddress;
                } else {
                    return;
                }
            } else {
                String unused5 = this.b.w = null;
                "mLastNetworkState : " + this.b.x;
                "now network state : " + activeNetworkInfo.getTypeName();
                if (this.b.x == null || activeNetworkInfo.getTypeName() == null || !this.b.x.equals(activeNetworkInfo.getTypeName())) {
                    String unused6 = this.b.x = activeNetworkInfo.getTypeName();
                } else {
                    return;
                }
            }
            if (this.b.F == null) {
                w unused7 = this.b.F = new w();
            }
            this.b.F.a(this.b.v, null, true);
            this.b.a(1);
        }
    }
}
