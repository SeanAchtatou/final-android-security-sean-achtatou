package com.snda.youni.b;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import org.jivesoftware.smack.Connection;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.f;
import org.jivesoftware.smack.f.c;

final class v extends Thread {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ai f364a;

    /* synthetic */ v(ai aiVar) {
        this(aiVar, (byte) 0);
    }

    private v(ai aiVar, byte b) {
        this.f364a = aiVar;
    }

    public final void run() {
        if (this.f364a.b()) {
            boolean unused = this.f364a.c(0);
            return;
        }
        if (this.f364a.y == null) {
            t unused2 = this.f364a.y = new t(this.f364a.q, this.f364a.v);
        }
        for (int i = 0; i < 3; i++) {
            if (!this.f364a.y.e()) {
                t.a(this.f364a.v);
                if (i == 2) {
                    this.f364a.f();
                    boolean unused3 = this.f364a.c(0);
                    return;
                }
                ai.d(10000);
            } else {
                String unused4 = this.f364a.s = t.b();
                int unused5 = this.f364a.t = t.c();
                String unused6 = this.f364a.u = t.a();
                String unused7 = this.f364a.r = t.d();
                "dispatch " + this.f364a.s;
                "dispatch " + String.valueOf(this.f364a.s);
                "dispatch " + this.f364a.u;
                "dispatch " + this.f364a.r;
                if (this.f364a.A == null) {
                    al unused8 = this.f364a.A = new al(this.f364a);
                    this.f364a.A.setName("WaitArriveThread");
                    this.f364a.A.start();
                }
                if (this.f364a.z == null) {
                    aj unused9 = this.f364a.z = new aj(this.f364a);
                    this.f364a.z.setName("WaitSendThread");
                    this.f364a.z.start();
                }
                if (this.f364a.B == null) {
                    u unused10 = this.f364a.B = new u(this.f364a);
                    this.f364a.B.start();
                }
                ai.a(this.f364a, this.f364a.s, this.f364a.t, this.f364a.u);
                if (this.f364a.G != null) {
                    Connection.b(this.f364a.G);
                }
                f unused11 = this.f364a.G = new an(this);
                Connection.a(this.f364a.G);
                XMPPConnection unused12 = this.f364a.p = new XMPPConnection(this.f364a.o);
                if (!this.f364a.d()) {
                    t.a(this.f364a.v);
                    if (i == 2) {
                        if (this.f364a.p.f()) {
                            this.f364a.p.o();
                        }
                        this.f364a.f();
                        boolean unused13 = this.f364a.c(0);
                        return;
                    }
                    if (this.f364a.p.f()) {
                        this.f364a.p.o();
                    }
                    ai.d(10000);
                } else if (this.f364a.p == null || !this.f364a.p.m() || !this.f364a.p.f()) {
                    t.a(this.f364a.v);
                    if (i == 2) {
                        if (this.f364a.p.f()) {
                            this.f364a.p.o();
                        }
                        this.f364a.f();
                        boolean unused14 = this.f364a.c(0);
                        return;
                    }
                    if (this.f364a.p.f()) {
                        this.f364a.p.o();
                    }
                    ai.d(10000);
                } else {
                    this.f364a.p.a(new ao(this));
                    c.a().a("reply", "reply.iccs.sdo", new ac());
                    c.a().b("n", "sd:push:n", new l());
                    c.a().a("query", "sd:iccs:u:s", new aq());
                    c.a().a("query", "sd:iccs:u:ss", new ah());
                    c.a().b("m", "sd:iccs:m", new b());
                    this.f364a.p.a(new z(this.f364a), null);
                    NetworkInfo activeNetworkInfo = ((ConnectivityManager) this.f364a.v.getSystemService("connectivity")).getActiveNetworkInfo();
                    if (activeNetworkInfo == null) {
                        String unused15 = this.f364a.x = (String) null;
                    } else {
                        String unused16 = this.f364a.x = activeNetworkInfo.getTypeName();
                    }
                    ai.B(this.f364a);
                    this.f364a.n.clear();
                    new Thread(new am(this), "getnotice").start();
                    this.f364a.f();
                    boolean unused17 = this.f364a.c(0);
                    return;
                }
            }
        }
    }
}
