package com.snda.youni.b;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

final class k extends Thread {

    /* renamed from: a  reason: collision with root package name */
    private String f355a = null;
    private String b = null;
    private Context c = null;
    private /* synthetic */ w d;

    public k(w wVar, Context context, String str, String str2) {
        this.d = wVar;
        this.c = context;
        this.f355a = str;
        this.b = str2;
    }

    public final void run() {
        if (!w.b(this.f355a, this.b)) {
            SharedPreferences.Editor edit = PreferenceManager.getDefaultSharedPreferences(this.c).edit();
            edit.putString("invite_data", this.b);
            edit.commit();
        }
    }
}
