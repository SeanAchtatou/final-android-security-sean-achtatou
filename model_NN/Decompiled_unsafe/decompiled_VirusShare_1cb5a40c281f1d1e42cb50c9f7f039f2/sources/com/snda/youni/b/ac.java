package com.snda.youni.b;

import org.jivesoftware.smack.b.p;
import org.jivesoftware.smack.f.b;
import org.xmlpull.v1.XmlPullParser;

public final class ac implements b {
    public final p a(XmlPullParser xmlPullParser) {
        ag agVar = new ag();
        if (xmlPullParser.getName().equals("reply")) {
            agVar.f341a = xmlPullParser.getAttributeValue("", "mid");
            agVar.b = xmlPullParser.getAttributeValue("", "to");
            agVar.c = xmlPullParser.getAttributeValue("", "status");
        }
        boolean z = false;
        while (!z) {
            if (xmlPullParser.next() == 3 && xmlPullParser.getName().equals("reply")) {
                z = true;
            }
        }
        return agVar;
    }
}
