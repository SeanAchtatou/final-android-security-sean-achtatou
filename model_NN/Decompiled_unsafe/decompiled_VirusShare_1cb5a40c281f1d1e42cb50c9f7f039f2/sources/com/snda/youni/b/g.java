package com.snda.youni.b;

import org.jivesoftware.smack.b.s;

public final class g extends s {

    /* renamed from: a  reason: collision with root package name */
    public String f352a = "";

    public g() {
        super("m", "sd:iccs:m");
    }

    public final String c() {
        StringBuilder sb = new StringBuilder();
        sb.append("<").append(a()).append(" xmlns=\"").append(b()).append("\"").append(" type=\"").append(this.f352a).append("\">");
        for (String str : d()) {
            String a2 = a(str);
            sb.append("<").append(str).append(">");
            sb.append(a2);
            sb.append("</").append(str).append(">");
        }
        sb.append("</").append(a()).append(">");
        return sb.toString();
    }
}
