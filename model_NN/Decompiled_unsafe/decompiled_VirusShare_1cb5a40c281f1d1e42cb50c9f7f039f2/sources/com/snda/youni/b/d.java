package com.snda.youni.b;

import java.util.ArrayList;
import java.util.List;
import org.jivesoftware.smack.b.p;

public final class d extends p {

    /* renamed from: a  reason: collision with root package name */
    List f350a = new ArrayList();

    public final String b() {
        StringBuilder sb = new StringBuilder();
        sb.append("<query xmlns=\"sd:iccs:u:ss\">");
        for (String str : this.f350a) {
            sb.append("<item jid=\"" + str + "@mim.snda\"/>");
        }
        sb.append("</query>");
        return sb.toString();
    }
}
