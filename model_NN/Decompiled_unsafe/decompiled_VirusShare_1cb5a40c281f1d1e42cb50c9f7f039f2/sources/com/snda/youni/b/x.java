package com.snda.youni.b;

public final class x extends e {

    /* renamed from: a  reason: collision with root package name */
    private String f365a = "";
    private String b = "";
    private String c = "";
    private String d = "";

    public x(String str) {
        this.f365a = str;
        this.b = str;
    }

    public final String a() {
        return this.f365a;
    }

    public final void a(String str) {
        this.c = str;
    }

    public final String b() {
        return this.c;
    }

    public final void b(String str) {
        this.d = str;
    }

    public final String c() {
        if (this.d == null || "".equals(this.d)) {
            return null;
        }
        int indexOf = this.d.indexOf("@");
        return indexOf == -1 ? this.d : this.d.substring(0, indexOf);
    }

    public final String d() {
        if (this.d == null || "".equals(this.d)) {
            return null;
        }
        int lastIndexOf = this.d.lastIndexOf("/");
        if (lastIndexOf == -1 || lastIndexOf == this.d.length() - 1) {
            return null;
        }
        return this.d.substring(lastIndexOf + 1);
    }

    public final String toString() {
        return "Mid = " + this.f365a + " Replyto = " + this.b + " Code = " + this.c + " From = " + this.d;
    }
}
