package com.snda.youni.b;

import org.json.JSONException;
import org.json.JSONObject;

public final class p {

    /* renamed from: a  reason: collision with root package name */
    private String f359a = null;
    private String b = null;
    private String c = null;

    public p(String str, String str2, String str3) {
        this.f359a = str;
        this.b = str2;
        this.c = str3;
    }

    public final JSONObject a() {
        if (this.f359a == null || this.b == null || this.c == null) {
            return null;
        }
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.accumulate("send", this.f359a);
            jSONObject.accumulate("recv", this.b);
            jSONObject.accumulate("time", this.c);
            return jSONObject;
        } catch (JSONException e) {
            e.getMessage();
            return null;
        }
    }
}
