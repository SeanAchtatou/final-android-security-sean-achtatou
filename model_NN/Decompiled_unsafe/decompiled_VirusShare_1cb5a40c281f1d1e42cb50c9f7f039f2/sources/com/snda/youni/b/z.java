package com.snda.youni.b;

import org.jivesoftware.smack.ab;
import org.jivesoftware.smack.b.l;
import org.jivesoftware.smack.b.o;
import org.jivesoftware.smack.b.p;
import org.jivesoftware.smack.b.w;

final class z implements ab {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ ai f366a;

    z(ai aiVar) {
        this.f366a = aiVar;
    }

    public final void a(w wVar) {
        if (wVar instanceof o) {
            m a2 = m.a((o) wVar);
            if (!a2.a() && !a2.c()) {
                ag agVar = new ag();
                agVar.a(l.b);
                agVar.c = "200";
                agVar.g(this.f366a.q + "@mim.snda/android");
                agVar.b = wVar.h();
                agVar.f341a = wVar.f();
                this.f366a.p.a(agVar);
                String g = a2.g();
                String j = a2.j();
                if (!(g == null || j == null)) {
                    "insert resource into map from = " + g + " resource = " + j;
                    this.f366a.l.a(g, j);
                }
            }
            "Body:" + ((o) wVar).d();
            ai.a(this.f366a, a2);
        } else if (!(wVar instanceof p)) {
        } else {
            if (wVar instanceof ag) {
                "packet is ReceiptIQ: " + ((ag) wVar).f341a + " status " + ((ag) wVar).c;
                this.f366a.m.put(((ag) wVar).f341a, ((ag) wVar).c);
                if (((ag) wVar).c.equals("0")) {
                    this.f366a.c(((ag) wVar).f341a);
                } else if (((ag) wVar).c.equals("88")) {
                    x xVar = new x(((ag) wVar).f341a);
                    xVar.a(((ag) wVar).c);
                    xVar.b(((ag) wVar).h());
                    "XIQ = " + xVar.toString();
                    String c = xVar.c();
                    String d = xVar.d();
                    if (!(c == null || d == null)) {
                        "insert resource into map from = " + c + " resource = " + d;
                        this.f366a.l.a(c, d);
                    }
                    ai.a(this.f366a, xVar);
                } else {
                    x xVar2 = new x(((ag) wVar).f341a);
                    xVar2.a(((ag) wVar).c);
                    xVar2.b(((ag) wVar).h());
                    "XIQ = " + xVar2.toString();
                    String c2 = xVar2.c();
                    String d2 = xVar2.d();
                    if (!(c2 == null || d2 == null)) {
                        "insert resource into map from = " + c2 + " resource = " + d2;
                        this.f366a.l.a(c2, d2);
                    }
                    ai.a(this.f366a, ((ag) wVar).f341a, ((ag) wVar).c);
                }
            } else if (wVar instanceof ak) {
                "jid = " + ((ak) wVar).f344a + " code = " + ((ak) wVar).b;
                ai.b(this.f366a, ((ak) wVar).f344a, ((ak) wVar).b.equals("0") ? "OFFLINE0" : "ONLINEnot0");
            } else if (wVar instanceof r) {
                ai.a(this.f366a, ((r) wVar).f361a);
            } else if (!((p) wVar).d().equals(l.c)) {
                "found other IQ type: " + wVar.getClass().getName();
            } else if (this.f366a.n.contains(((p) wVar).f())) {
                "remove ping packet: " + ((p) wVar).f();
                this.f366a.n.remove(((p) wVar).f());
            }
        }
    }
}
