package com.snda.youni.b;

import android.net.ConnectivityManager;
import android.os.SystemClock;

final class aj extends Thread {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ ai f343a;

    /* synthetic */ aj(ai aiVar) {
        this(aiVar, (byte) 0);
    }

    private aj(ai aiVar, byte b) {
        this.f343a = aiVar;
    }

    private void a(m mVar) {
        if (((ConnectivityManager) this.f343a.v.getSystemService("connectivity")).getActiveNetworkInfo() == null) {
            this.f343a.c(mVar.e());
            this.f343a.a(0);
            return;
        }
        this.f343a.c(mVar.e());
        this.f343a.a(1);
    }

    public final void run() {
        while (true) {
            m mVar = null;
            try {
                mVar = (m) this.f343a.h.take();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            try {
                Thread.sleep(30000);
            } catch (InterruptedException e2) {
                e2.printStackTrace();
            }
            "packet id: " + mVar.e();
            if (this.f343a.m.containsKey(mVar.e())) {
                "from: " + mVar.g() + " to: " + mVar.f();
                String str = (String) this.f343a.m.get(mVar.e());
                "tmpStatus == " + str;
                if (str.equals("0") || str.startsWith("2") || str.equals("10")) {
                    this.f343a.m.remove(mVar.e());
                } else if (str.equals("11")) {
                    "Jid == " + mVar.e() + " tmpStatus == XType.WAITSECONDIQ";
                    if (mVar.f().startsWith("krobot")) {
                        this.f343a.m.remove(mVar.e());
                    } else {
                        this.f343a.k.put(mVar.e(), 0);
                    }
                } else {
                    "what happened " + str;
                }
                while (true) {
                    m mVar2 = (m) this.f343a.h.peek();
                    if (mVar2 == null || !this.f343a.m.containsKey(mVar2.e())) {
                        break;
                    }
                    try {
                        this.f343a.h.take();
                        String str2 = (String) this.f343a.m.get(mVar2.e());
                        "tmpStatus == " + str2;
                        if (str2.equals("10") || str2.equals("0") || str2.startsWith("2")) {
                            this.f343a.m.remove(mVar2.e());
                            "from: " + mVar2.g() + " to: " + mVar2.f();
                        } else {
                            if (str2.equals("11")) {
                                "Jid == " + mVar2.e() + " tmpStatus == XType.WAITSECONDIQ";
                                this.f343a.k.put(mVar2.e(), 0);
                            }
                            "from: " + mVar2.g() + " to: " + mVar2.f();
                        }
                    } catch (InterruptedException e3) {
                        e3.printStackTrace();
                    }
                }
            } else {
                long elapsedRealtime = SystemClock.elapsedRealtime() - mVar.t();
                "Message Jid = " + mVar.e() + " Time = " + String.valueOf(elapsedRealtime);
                if (elapsedRealtime > 180000) {
                    a(mVar);
                    while (true) {
                        m mVar3 = (m) this.f343a.h.peek();
                        if (mVar3 == null) {
                            break;
                        }
                        long elapsedRealtime2 = SystemClock.elapsedRealtime() - mVar3.t();
                        "Message Jid = " + mVar3.e() + " Time = " + String.valueOf(elapsedRealtime2);
                        if (elapsedRealtime2 < 180000) {
                            break;
                        }
                        try {
                            this.f343a.h.take();
                        } catch (InterruptedException e4) {
                            e4.printStackTrace();
                        }
                        ai.d(10000);
                        a(mVar3);
                    }
                } else {
                    mVar.q();
                    if (mVar.r() < 4) {
                        try {
                            this.f343a.g.put(mVar);
                        } catch (InterruptedException e5) {
                            e5.printStackTrace();
                        }
                    } else {
                        a(mVar);
                    }
                }
            }
        }
    }
}
