package com.snda.youni.b;

import java.util.Iterator;
import java.util.Set;

final class n extends Thread {

    /* renamed from: a  reason: collision with root package name */
    private m f357a;
    private x b;
    private e c;
    private Set d;
    private Iterator e;
    private /* synthetic */ ai f;

    /* synthetic */ n(ai aiVar) {
        this(aiVar, (byte) 0);
    }

    private n(ai aiVar, byte b2) {
        this.f = aiVar;
        this.f357a = null;
        this.b = null;
        this.c = null;
        this.d = null;
        this.e = null;
    }

    public final void run() {
        while (true) {
            try {
                this.c = (e) this.f.i.take();
            } catch (InterruptedException e2) {
                e2.printStackTrace();
            }
            if (this.c instanceof m) {
                this.f357a = (m) this.c;
                this.d = this.f.c.keySet();
                this.e = this.d.iterator();
                while (this.e.hasNext()) {
                    ((ap) this.e.next()).a(this.f357a);
                }
            } else if (this.c instanceof x) {
                this.b = (x) this.c;
                this.d = this.f.c.keySet();
                this.e = this.d.iterator();
                while (this.e.hasNext()) {
                    ((ap) this.e.next()).a(this.b);
                }
            }
        }
    }
}
