package com.snda.youni.b;

import android.os.SystemClock;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import org.jivesoftware.a.a.a;
import org.jivesoftware.smack.b.f;
import org.jivesoftware.smack.b.j;
import org.jivesoftware.smack.b.o;

public final class m extends e {
    private static Date e = null;

    /* renamed from: a  reason: collision with root package name */
    private int f356a;
    private boolean b;
    private boolean c;
    private Map d;
    private long f;

    public m() {
        this.f356a = 0;
        this.b = false;
        this.c = false;
        this.d = null;
        this.f = 0;
        this.d = new HashMap();
        this.f356a = 0;
        e = null;
    }

    public static m a(String str, String str2, String str3, String str4) {
        m mVar = new m();
        mVar.b(str);
        mVar.c(str2 + "@mim.snda");
        mVar.d("zh-cn");
        mVar.e(str3);
        mVar.f(str4);
        mVar.g("Chat");
        return mVar;
    }

    public static m a(String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, int i) {
        m mVar = new m();
        mVar.b(str);
        mVar.c(str2 + "@mim.snda");
        mVar.d("zh-cn");
        mVar.e(str3);
        mVar.f(str4);
        mVar.g("Chat");
        mVar.h(str5);
        mVar.i(str6);
        mVar.k(str7);
        mVar.l(str8);
        mVar.m(str8);
        mVar.j("" + i);
        return mVar;
    }

    public static m a(o oVar) {
        String substring;
        f b2;
        m mVar = new m();
        if (oVar.b().equals(j.headline)) {
            if ("mim.snda".equals(oVar.h()) && (b2 = oVar.b("n", "sd:push:n")) != null && (b2 instanceof i)) {
                i iVar = (i) b2;
                "notification app = " + iVar.f354a;
                "notification content = " + iVar.a("content");
                if (iVar.f354a.equals("op")) {
                    mVar.b = true;
                    mVar.d.put("content", iVar.a("content"));
                } else if (iVar.f354a.equals("opa")) {
                    mVar.c = true;
                    mVar.d.put("content", iVar.a("content"));
                    mVar.d.put("start-time", iVar.a("start-time"));
                    mVar.d.put("end-time", iVar.a("end-time"));
                    mVar.d.put("id", iVar.a("id"));
                }
            }
            return mVar;
        }
        mVar.b(oVar.f());
        mVar.c(oVar.g());
        mVar.d(oVar.e());
        mVar.e(oVar.c());
        mVar.f(oVar.d());
        String h = oVar.h();
        if (h == null) {
            h = null;
        } else {
            int indexOf = h.indexOf("@");
            if (indexOf != -1) {
                h = h.substring(0, indexOf);
            }
        }
        mVar.d.put("From", h);
        String h2 = oVar.h();
        if (h2 == null) {
            substring = null;
        } else {
            int lastIndexOf = h2.lastIndexOf("/");
            substring = (lastIndexOf == -1 || lastIndexOf == h2.length() - 1) ? null : h2.substring(lastIndexOf + 1);
        }
        mVar.d.put("resource", substring);
        if (oVar.b() == j.chat) {
            mVar.g("Chat");
        }
        f b3 = oVar.b("delay", "urn:xmpp:delay");
        if (b3 instanceof a) {
            e = ((a) b3).d();
        } else {
            e = new Date();
        }
        String str = (String) oVar.h("sendTime");
        if (str != null) {
            mVar.a(str);
        }
        f b4 = oVar.b("m", "sd:iccs:m");
        if (b4 != null && (b4 instanceof g)) {
            g gVar = (g) b4;
            String str2 = gVar.f352a;
            String a2 = gVar.a("mme-shorturl");
            String a3 = gVar.a("mme-thumburl");
            String a4 = gVar.a("mme-imageurl");
            String a5 = gVar.a("mme-audiourl");
            String a6 = gVar.a("mme-playduration");
            mVar.h(str2);
            mVar.i(a2);
            mVar.k(a3);
            mVar.l(a4);
            mVar.m(a5);
            mVar.j(a6);
        }
        return mVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.jivesoftware.smack.b.w.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      org.jivesoftware.smack.b.o.a(java.lang.String, java.lang.String):org.jivesoftware.smack.b.v
      org.jivesoftware.smack.b.w.a(java.lang.String, java.lang.Object):void */
    public static o a(m mVar) {
        o oVar = new o();
        oVar.e(mVar.e());
        oVar.f(mVar.f());
        oVar.d((String) mVar.d.get("Language"));
        oVar.a(mVar.h());
        String i = mVar.i();
        if (i == null) {
            oVar.b("");
        } else {
            oVar.a((String) null, i);
        }
        oVar.g(mVar.g());
        if (((String) mVar.d.get("Type")) == "Chat") {
            oVar.a(j.chat);
        }
        String u = mVar.u();
        if (u != null) {
            oVar.a("sendTime", (Object) u);
        }
        if (mVar.k() != null && mVar.k().equals("img")) {
            g gVar = new g();
            gVar.f352a = "img";
            gVar.a("mme-shorturl", mVar.l());
            gVar.a("mme-thumburl", mVar.n().replaceAll("&", "&amp;"));
            gVar.a("mme-imageurl", mVar.o().replaceAll("&", "&amp;"));
            oVar.a(gVar);
        } else if (mVar.k() != null && mVar.k().equals("audio")) {
            g gVar2 = new g();
            gVar2.f352a = "audio";
            gVar2.a("mme-shorturl", mVar.l());
            gVar2.a("mme-audiourl", mVar.p().replaceAll("&", "&amp;"));
            gVar2.a("mme-playduration", mVar.m());
            oVar.a(gVar2);
        }
        oVar.b_();
        return oVar;
    }

    private void b(String str) {
        this.d.put("PacketID", str);
    }

    private void c(String str) {
        this.d.put("To", str);
    }

    private void d(String str) {
        this.d.put("Language", str);
    }

    private void e(String str) {
        this.d.put("Subject", str);
    }

    private void f(String str) {
        this.d.put("Body", str);
    }

    private void g(String str) {
        this.d.put("Type", str);
    }

    private void h(String str) {
        this.d.put("mme-type", str);
    }

    private void i(String str) {
        this.d.put("mme-shorturl", str);
    }

    private void j(String str) {
        this.d.put("mme-playduration", str);
    }

    private void k(String str) {
        this.d.put("mme-thumburl", str);
    }

    private void l(String str) {
        this.d.put("mme-imageurl", str);
    }

    private void m(String str) {
        this.d.put("mme-audiourl", str);
    }

    public final m a(String str) {
        this.d.put("sendTime", str);
        return this;
    }

    public final boolean a() {
        return this.b;
    }

    public final String b() {
        return (String) this.d.get("content");
    }

    public final boolean c() {
        return this.c;
    }

    public final String d() {
        return (String) this.d.get("id");
    }

    public final String e() {
        return (String) this.d.get("PacketID");
    }

    public final String f() {
        return (String) this.d.get("To");
    }

    public final String g() {
        return (String) this.d.get("From");
    }

    public final String h() {
        return (String) this.d.get("Subject");
    }

    public final String i() {
        return (String) this.d.get("Body");
    }

    public final String j() {
        return (String) this.d.get("resource");
    }

    public final String k() {
        return (String) this.d.get("mme-type");
    }

    public final String l() {
        return (String) this.d.get("mme-shorturl");
    }

    public final String m() {
        return (String) this.d.get("mme-playduration");
    }

    public final String n() {
        return (String) this.d.get("mme-thumburl");
    }

    public final String o() {
        return (String) this.d.get("mme-imageurl");
    }

    public final String p() {
        return (String) this.d.get("mme-audiourl");
    }

    public final void q() {
        this.f356a++;
    }

    public final int r() {
        int i = this.f356a;
        this.f356a = i + 1;
        return i;
    }

    public final void s() {
        this.f = SystemClock.elapsedRealtime();
    }

    public final long t() {
        return this.f;
    }

    public final String u() {
        return (String) this.d.get("sendTime");
    }
}
