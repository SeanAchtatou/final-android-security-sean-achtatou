package com.snda.youni.b;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.PowerManager;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import com.snda.youni.AppContext;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.ad;
import org.jivesoftware.smack.b.l;
import org.jivesoftware.smack.d;
import org.jivesoftware.smack.f;
import org.jivesoftware.smack.q;

public final class ai {

    /* renamed from: a  reason: collision with root package name */
    private static long f342a = 0;
    private static PowerManager.WakeLock b = null;
    /* access modifiers changed from: private */
    public al A;
    /* access modifiers changed from: private */
    public u B;
    private n C;
    private ad D;
    private v E;
    /* access modifiers changed from: private */
    public w F;
    /* access modifiers changed from: private */
    public f G;
    private q H;
    private int I;
    /* access modifiers changed from: private */
    public ConcurrentHashMap c;
    /* access modifiers changed from: private */
    public ConcurrentHashMap d;
    private af e;
    private s f;
    /* access modifiers changed from: private */
    public BlockingQueue g;
    /* access modifiers changed from: private */
    public BlockingQueue h;
    /* access modifiers changed from: private */
    public BlockingQueue i;
    /* access modifiers changed from: private */
    public BlockingQueue j;
    /* access modifiers changed from: private */
    public Map k;
    /* access modifiers changed from: private */
    public f l;
    /* access modifiers changed from: private */
    public Map m;
    /* access modifiers changed from: private */
    public Set n;
    /* access modifiers changed from: private */
    public d o;
    /* access modifiers changed from: private */
    public XMPPConnection p;
    /* access modifiers changed from: private */
    public String q;
    /* access modifiers changed from: private */
    public String r;
    /* access modifiers changed from: private */
    public String s;
    /* access modifiers changed from: private */
    public int t;
    /* access modifiers changed from: private */
    public String u;
    /* access modifiers changed from: private */
    public Context v;
    /* access modifiers changed from: private */
    public String w;
    /* access modifiers changed from: private */
    public String x;
    /* access modifiers changed from: private */
    public t y;
    /* access modifiers changed from: private */
    public aj z;

    public ai(Context context) {
        this.c = null;
        this.d = null;
        this.e = null;
        this.f = null;
        this.g = null;
        this.h = null;
        this.i = null;
        this.j = null;
        this.k = null;
        this.l = null;
        this.m = null;
        this.n = null;
        this.o = null;
        this.p = null;
        this.q = null;
        this.r = null;
        this.s = null;
        this.t = 5222;
        this.u = null;
        this.v = null;
        this.w = null;
        this.x = null;
        this.y = null;
        this.z = null;
        this.A = null;
        this.B = null;
        this.C = null;
        this.D = null;
        this.E = null;
        this.F = null;
        this.G = null;
        this.H = null;
        this.I = 0;
        this.d = new ConcurrentHashMap();
        this.c = new ConcurrentHashMap();
        this.g = new ArrayBlockingQueue(100);
        this.h = new ArrayBlockingQueue(100);
        this.i = new ArrayBlockingQueue(100);
        this.j = new ArrayBlockingQueue(100);
        this.k = new HashMap();
        this.l = new f();
        this.l.a();
        this.m = new HashMap();
        this.n = new HashSet();
        this.v = context;
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) this.v.getSystemService("connectivity")).getActiveNetworkInfo();
        this.w = null;
        this.x = null;
        if (activeNetworkInfo != null) {
            this.x = activeNetworkInfo.getTypeName();
            if (this.x.equals("WIFI")) {
                this.w = ((WifiManager) this.v.getSystemService("wifi")).getConnectionInfo().getMacAddress();
            }
        }
        this.H = new q(this);
        f342a = SystemClock.elapsedRealtime();
        if (this.C == null) {
            this.C = new n(this);
            this.C.start();
        }
        if (this.D == null) {
            this.D = new ad(this);
            this.D.start();
        }
    }

    static /* synthetic */ void B(ai aiVar) {
        Intent intent = new Intent("com.snda.youni.ACTION_XNETWORK_CONNECTED");
        intent.setPackage(AppContext.a().getPackageName());
        aiVar.v.sendBroadcast(intent);
    }

    static /* synthetic */ String C(ai aiVar) {
        if (aiVar.q == null || aiVar.p == null || !aiVar.p.f()) {
            return "OFFLINE0";
        }
        ae aeVar = new ae();
        aeVar.a(l.f667a);
        aeVar.g(aiVar.q + "@mim.snda/android");
        aeVar.f("mim.snda");
        aiVar.p.a(aeVar);
        return "unknow";
    }

    static /* synthetic */ void a(ai aiVar, m mVar) {
        try {
            aiVar.i.put(mVar);
        } catch (InterruptedException e2) {
            e2.printStackTrace();
        }
    }

    static /* synthetic */ void a(ai aiVar, x xVar) {
        try {
            aiVar.i.put(xVar);
        } catch (InterruptedException e2) {
            e2.printStackTrace();
        }
    }

    static /* synthetic */ void a(ai aiVar, String str, int i2, String str2) {
        aiVar.o = new d(str, i2, str2);
        aiVar.o.r();
        aiVar.o.t();
        aiVar.o.a(q.disabled);
        aiVar.o.q();
    }

    static /* synthetic */ void a(ai aiVar, String str, String str2) {
        "notify send success:" + str;
        try {
            aiVar.j.put(new o(str, str2, true));
        } catch (InterruptedException e2) {
            e2.printStackTrace();
        }
    }

    static /* synthetic */ void a(ai aiVar, HashMap hashMap) {
        if (aiVar.f != null) {
            aiVar.f.a(hashMap);
        }
    }

    static /* synthetic */ void a(ai aiVar, XMPPConnection xMPPConnection) {
        if (xMPPConnection.f() && !xMPPConnection.m()) {
            synchronized ("login") {
                d(1000);
                if (!aiVar.p.m()) {
                    try {
                        aiVar.p.a(aiVar.q, aiVar.r, "android");
                    } catch (ad e2) {
                    } catch (IllegalStateException e3) {
                        e3.getMessage();
                    }
                    return;
                }
                return;
            }
        }
        return;
    }

    static /* synthetic */ void b(ai aiVar, String str, String str2) {
        if (aiVar.e != null) {
            aiVar.e.a(str, str2);
        }
    }

    /* access modifiers changed from: private */
    public void c(String str) {
        "notify send failed:" + str;
        try {
            this.j.put(new o(str, null, false));
        } catch (InterruptedException e2) {
            e2.printStackTrace();
        }
    }

    /* access modifiers changed from: private */
    public synchronized boolean c(int i2) {
        boolean z2;
        if (i2 == 0) {
            this.I = 0;
            z2 = true;
        } else if (i2 == 2) {
            if (this.I == 1) {
                z2 = false;
            } else {
                if (this.p != null) {
                    this.p.o();
                }
                z2 = true;
            }
        } else if (this.I == 0) {
            this.I = 1;
            "mIsCanCreateConnection == 0 mConnection=" + this.p;
            if (this.p != null) {
                this.p.o();
            }
            z2 = true;
        } else {
            z2 = false;
        }
        return z2;
    }

    /* access modifiers changed from: private */
    public static void d(int i2) {
        try {
            Thread.sleep((long) i2);
        } catch (InterruptedException e2) {
            e2.printStackTrace();
        }
    }

    /* access modifiers changed from: private */
    public boolean d() {
        if (this.p != null && this.p.f()) {
            return true;
        }
        try {
            this.p.s();
            return true;
        } catch (ad e2) {
            "connect error:" + e2.getMessage();
            return false;
        }
    }

    private synchronized void e() {
        if (b == null) {
            b = ((PowerManager) this.v.getSystemService("power")).newWakeLock(1, getClass().getCanonicalName());
        } else if (b.isHeld()) {
            b.release();
        }
        b.acquire();
    }

    /* access modifiers changed from: private */
    public synchronized void f() {
        if (b != null && b.isHeld()) {
            b.release();
        }
    }

    public final int a(int i2) {
        if (this.q == null) {
            String string = PreferenceManager.getDefaultSharedPreferences(this.v).getString("self_phone_number", null);
            if (string == null) {
                return 0;
            }
            this.q = string;
        }
        if (i2 == 1) {
            synchronized (this) {
                if (c(2)) {
                    a(this.q);
                }
            }
        } else if (i2 == 0) {
            c(2);
        } else if (i2 == 2) {
            c(2);
        }
        return 0;
    }

    public final int a(ap apVar) {
        this.c.put(apVar, apVar);
        return 1;
    }

    public final String a(String str, af afVar) {
        if (str == null || this.q == null || this.p == null || !this.p.f()) {
            return "OFFLINE0";
        }
        this.e = afVar;
        ak akVar = new ak();
        akVar.f344a = str + "@mim.snda/android";
        akVar.a(l.f667a);
        akVar.g(this.q + "@mim.snda/android");
        this.p.a(akVar);
        return "unknow";
    }

    public final String a(List list, s sVar) {
        if (list == null || this.q == null || this.p == null || !this.p.f()) {
            return "OFFLINE0";
        }
        this.f = sVar;
        d dVar = new d();
        dVar.f350a = list;
        dVar.a(l.f667a);
        dVar.g(this.q + "@mim.snda/android");
        dVar.f("mim.snda");
        this.p.a(dVar);
        return "unknow";
    }

    public final synchronized void a() {
        synchronized ("ping") {
            if (this.p.f() && this.n.isEmpty()) {
                e();
                new Thread(new aa(this), "pingthread").start();
            }
        }
    }

    public final void a(j jVar) {
        this.d.put(jVar, jVar);
    }

    public final void a(m mVar) {
        if (this.B == null) {
            c(mVar.e());
            "sendthread == null, mUserJid = " + this.q;
            if (this.q != null) {
                a(1);
                return;
            }
            return;
        }
        try {
            mVar.s();
            this.g.put(mVar);
        } catch (InterruptedException e2) {
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(java.lang.String r4) {
        /*
            r3 = this;
            r2 = 1
            java.lang.String r0 = "createConnection"
            monitor-enter(r0)
            r1 = 1
            boolean r1 = r3.c(r1)     // Catch:{ all -> 0x0049 }
            if (r1 == 0) goto L_0x0011
            boolean r1 = r3.b()     // Catch:{ all -> 0x0049 }
            if (r1 != r2) goto L_0x0013
        L_0x0011:
            monitor-exit(r0)     // Catch:{ all -> 0x0049 }
        L_0x0012:
            return
        L_0x0013:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x0049 }
            r1.<init>()     // Catch:{ all -> 0x0049 }
            java.lang.String r2 = "createConnection: mobileNumber = "
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x0049 }
            java.lang.StringBuilder r1 = r1.append(r4)     // Catch:{ all -> 0x0049 }
            r1.toString()     // Catch:{ all -> 0x0049 }
            r3.q = r4     // Catch:{ all -> 0x0049 }
            long r1 = android.os.SystemClock.elapsedRealtime()     // Catch:{ all -> 0x0049 }
            com.snda.youni.b.ai.f342a = r1     // Catch:{ all -> 0x0049 }
            java.lang.String r1 = r3.q     // Catch:{ all -> 0x0049 }
            if (r1 == 0) goto L_0x0047
            r3.e()     // Catch:{ all -> 0x0049 }
            com.snda.youni.b.v r1 = new com.snda.youni.b.v     // Catch:{ all -> 0x0049 }
            r1.<init>(r3)     // Catch:{ all -> 0x0049 }
            r3.E = r1     // Catch:{ all -> 0x0049 }
            com.snda.youni.b.v r1 = r3.E     // Catch:{ all -> 0x0049 }
            java.lang.String r2 = "createconnectionthread"
            r1.setName(r2)     // Catch:{ all -> 0x0049 }
            com.snda.youni.b.v r1 = r3.E     // Catch:{ all -> 0x0049 }
            r1.start()     // Catch:{ all -> 0x0049 }
        L_0x0047:
            monitor-exit(r0)     // Catch:{ all -> 0x0049 }
            goto L_0x0012
        L_0x0049:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x0049 }
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.snda.youni.b.ai.a(java.lang.String):void");
    }

    public final void b(String str) {
        String a2;
        if (this.q != null && str != null && this.p != null && this.p.f() && (a2 = this.l.a(str)) != null) {
            ag agVar = new ag();
            agVar.a(l.b);
            agVar.c = "88";
            agVar.g(this.q + "@mim.snda/android");
            agVar.b = str + "@mim.snda/" + a2;
            agVar.f341a = "0";
            "send iq : " + agVar.toString();
            this.p.a(agVar);
        }
    }

    public final boolean b() {
        "mConnection=" + this.p;
        if (this.p != null) {
            "mConnection.isConnected()=" + this.p.f();
            "mConnection.isAuthenticated()=" + this.p.m();
        }
        return this.p != null && this.p.f() && this.p.m();
    }

    public final void c() {
        if (this.H != null) {
            this.H.a();
        }
    }
}
