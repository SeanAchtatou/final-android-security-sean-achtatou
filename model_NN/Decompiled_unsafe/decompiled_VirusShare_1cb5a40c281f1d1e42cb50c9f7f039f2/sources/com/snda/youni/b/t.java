package com.snda.youni.b;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.telephony.TelephonyManager;

public final class t {

    /* renamed from: a  reason: collision with root package name */
    private static a f362a = null;
    /* access modifiers changed from: private */
    public static String b = null;
    /* access modifiers changed from: private */
    public static Context c = null;
    private static ab d = null;
    private static String e = null;

    public t(String str, Context context) {
        c = context;
        b = str;
        if (d == null) {
            a(c);
            ab abVar = new ab(this);
            d = abVar;
            abVar.setName("GetICCSOjectThread");
            d.start();
        }
    }

    public static String a() {
        return f362a.d();
    }

    public static void a(Context context) {
        SharedPreferences.Editor edit = context.getSharedPreferences("dispatch_object_pref", 0).edit();
        edit.remove("host");
        edit.remove("port");
        edit.remove("domain");
        edit.remove("ticket");
        edit.remove("token");
        edit.commit();
    }

    public static String b() {
        return f362a.b();
    }

    public static int c() {
        return f362a.c();
    }

    public static String d() {
        return f362a.a();
    }

    /* access modifiers changed from: private */
    public static void i() {
        "first line of get config from web server ime = " + e;
        a a2 = h.a(b, e);
        f362a = a2;
        if (a2 != null) {
            a aVar = f362a;
            SharedPreferences.Editor edit = c.getSharedPreferences("dispatch_object_pref", 0).edit();
            edit.putString("host", aVar.b());
            edit.putInt("port", aVar.c());
            edit.putString("domain", aVar.d());
            edit.putString("ticket", aVar.e());
            edit.putString("token", aVar.a());
            edit.commit();
            "send broadcast " + aVar.a();
            Intent intent = new Intent("com.snda.youni.dispatch_changed");
            intent.putExtra("token", aVar.a());
            intent.setPackage(c.getPackageName());
            c.sendBroadcast(intent);
        }
    }

    public final synchronized boolean e() {
        boolean z;
        if (e == null) {
            SharedPreferences sharedPreferences = c.getSharedPreferences("dispatch_object_pref", 0);
            String string = sharedPreferences.getString("xime", null);
            e = string;
            if (string == null) {
                e = ((TelephonyManager) c.getSystemService("phone")).getDeviceId();
                "imei == " + e;
                if (!h.b(e, b)) {
                    e = null;
                    z = false;
                } else {
                    sharedPreferences.edit().putString("xime", e);
                    sharedPreferences.edit().commit();
                }
            }
        }
        f362a = null;
        SharedPreferences sharedPreferences2 = c.getSharedPreferences("dispatch_object_pref", 0);
        a aVar = new a();
        aVar.c(sharedPreferences2.getString("domain", null));
        aVar.b(sharedPreferences2.getString("host", null));
        aVar.a(sharedPreferences2.getInt("port", 0));
        aVar.d(sharedPreferences2.getString("ticket", null));
        aVar.a(sharedPreferences2.getString("token", null));
        a aVar2 = aVar.b() == null ? null : aVar;
        f362a = aVar2;
        if (aVar2 == null || f362a.d() == null || f362a.b() == null) {
            i();
        }
        z = f362a != null;
        return z;
    }
}
