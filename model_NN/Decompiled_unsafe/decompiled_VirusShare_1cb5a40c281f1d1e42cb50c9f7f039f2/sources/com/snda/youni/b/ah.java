package com.snda.youni.b;

import com.snda.a.a.a.a;
import org.jivesoftware.smack.b.p;
import org.jivesoftware.smack.f.b;
import org.xmlpull.v1.XmlPullParser;

public final class ah implements b {
    public final p a(XmlPullParser xmlPullParser) {
        a.c("FriendsStatusIQProvider", "parseIQ");
        r rVar = new r();
        String name = xmlPullParser.getName();
        a.c("FriendsStatusIQProvider", "elementName=" + name);
        String str = name;
        int eventType = xmlPullParser.getEventType();
        while (true) {
            if (eventType == 3 && str.equals("query")) {
                return rVar;
            }
            a.c("FriendsStatusIQProvider", "eventType=" + eventType + ",elementName=" + str);
            if (eventType == 2 && str.equals("item")) {
                String str2 = xmlPullParser.getAttributeValue("", "jid").split("@")[0];
                if (str2.startsWith("krobot")) {
                    rVar.f361a.put(str2, "1");
                } else {
                    rVar.f361a.put(str2, xmlPullParser.getAttributeValue("", "code"));
                }
            }
            xmlPullParser.next();
            eventType = xmlPullParser.getEventType();
            str = xmlPullParser.getName();
        }
    }
}
