package com.snda.youni.i;

import javax.crypto.Cipher;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import org.a.d.a.a.a;

public final class c {

    /* renamed from: a  reason: collision with root package name */
    private static String f430a = "2W5PhrM+nrA=";

    public static String a(String str) {
        String str2 = f430a;
        byte[] bytes = str.getBytes();
        Cipher instance = Cipher.getInstance("DES");
        instance.init(1, SecretKeyFactory.getInstance("DES").generateSecret(new DESKeySpec(str2.getBytes())));
        return new String(a.a(instance.doFinal(bytes)));
    }
}
