package com.snda.youni.i;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;

public final class b {

    /* renamed from: a  reason: collision with root package name */
    private static Map f429a = null;
    private static Map b = null;
    private BigInteger c = null;
    private String d = null;
    private String e = null;

    public b() {
        if (f429a == null && b == null) {
            f429a = new HashMap();
            b = new HashMap();
            int i = 0;
            for (int i2 = 0; i2 < 10; i2++) {
                f429a.put(Character.valueOf((char) (i2 + 48)), Integer.valueOf(i));
                b.put(Integer.valueOf(i), Character.valueOf((char) (i2 + 48)));
                i++;
            }
            for (int i3 = 0; i3 < 26; i3++) {
                f429a.put(Character.valueOf((char) (i3 + 97)), Integer.valueOf(i));
                b.put(Integer.valueOf(i), Character.valueOf((char) (i3 + 97)));
                i++;
            }
            for (int i4 = 0; i4 < 26; i4++) {
                f429a.put(Character.valueOf((char) (i4 + 65)), Integer.valueOf(i));
                b.put(Integer.valueOf(i), Character.valueOf((char) (i4 + 65)));
                i++;
            }
            f429a.put('!', Integer.valueOf(i));
            b.put(Integer.valueOf(i), '!');
            int i5 = i + 1;
            f429a.put('$', Integer.valueOf(i5));
            b.put(Integer.valueOf(i5), '$');
            int i6 = i5 + 1;
            f429a.put('(', Integer.valueOf(i6));
            b.put(Integer.valueOf(i6), '(');
            int i7 = i6 + 1;
            f429a.put(')', Integer.valueOf(i7));
            b.put(Integer.valueOf(i7), ')');
            int i8 = i7 + 1;
            f429a.put('}', Integer.valueOf(i8));
            b.put(Integer.valueOf(i8), '}');
            int i9 = i8 + 1;
            f429a.put('{', Integer.valueOf(i9));
            b.put(Integer.valueOf(i9), '{');
            int i10 = i9 + 1;
            f429a.put('[', Integer.valueOf(i10));
            b.put(Integer.valueOf(i10), '[');
            int i11 = i10 + 1;
            f429a.put(']', Integer.valueOf(i11));
            b.put(Integer.valueOf(i11), ']');
            int i12 = i11 + 1;
            f429a.put('_', Integer.valueOf(i12));
            b.put(Integer.valueOf(i12), '_');
            int i13 = i12 + 1;
            f429a.put('-', Integer.valueOf(i13));
            b.put(Integer.valueOf(i13), '-');
        }
    }

    public final String a() {
        int intValue;
        StringBuilder sb = new StringBuilder();
        while (!this.c.equals(BigInteger.ZERO)) {
            if (this.c == null) {
                intValue = 0;
            } else {
                BigInteger[] divideAndRemainder = this.c.divideAndRemainder(BigInteger.valueOf(62));
                this.c = divideAndRemainder[0];
                intValue = divideAndRemainder[1].intValue();
            }
            char charValue = ((Character) b.get(Integer.valueOf(intValue))).charValue();
            String.valueOf(charValue);
            sb.append(charValue);
        }
        this.d = sb.toString();
        "encrypt_text = " + this.d;
        return this.d;
    }

    public final void a(String str) {
        if (str != null) {
            "xsb = " + str;
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < str.length(); i++) {
                char charAt = str.charAt(i);
                if (charAt <= '9' && charAt >= '0') {
                    sb.append(charAt);
                }
            }
            try {
                this.c = new BigInteger("1" + sb.toString());
            } catch (Exception e2) {
            }
        }
    }
}
