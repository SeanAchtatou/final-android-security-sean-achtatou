package com.snda.youni.activities;

import android.a.j;
import android.a.m;
import android.a.o;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.text.ClipboardManager;
import android.text.TextUtils;
import android.view.ContextMenu;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.TranslateAnimation;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CursorAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;
import com.sd.a.a.a.a.w;
import com.sd.android.mms.a.e;
import com.sd.android.mms.a.p;
import com.snda.youni.AppContext;
import com.snda.youni.C0000R;
import com.snda.youni.YouNi;
import com.snda.youni.attachment.b.b;
import com.snda.youni.attachment.g;
import com.snda.youni.b.ai;
import com.snda.youni.e.n;
import com.snda.youni.e.q;
import com.snda.youni.e.s;
import com.snda.youni.e.t;
import com.snda.youni.mms.ui.au;
import com.snda.youni.mms.ui.f;
import com.snda.youni.mms.ui.i;
import com.snda.youni.mms.ui.l;
import com.snda.youni.modules.InputView;
import com.snda.youni.modules.WarningTipView;
import com.snda.youni.modules.a.d;
import com.snda.youni.modules.a.h;
import com.snda.youni.modules.a.k;
import com.snda.youni.modules.chat.LinearLayoutThatDetectsSoftKeyboard;
import com.snda.youni.modules.chat.a;
import com.snda.youni.services.YouniService;
import java.io.File;
import java.util.Arrays;
import java.util.HashSet;

public class ChatActivity extends Activity implements View.OnClickListener, l, d, a, com.snda.youni.modules.l {
    private static final String[] aq = {"_id", "sub", "sub_cs"};
    private static final String[] e = {"_id", "body"};
    /* access modifiers changed from: private */
    public RadioButton A;
    private ImageView B;
    /* access modifiers changed from: private */
    public TextView C;
    private com.snda.youni.d D;
    /* access modifiers changed from: private */
    public ImageButton E;
    private Button F;
    private Button G;
    private Button H;
    private ImageButton I;
    private ImageButton J;
    private ImageButton K;
    private ImageButton L;
    private int M;
    private int N;
    private boolean O = true;
    /* access modifiers changed from: private */
    public boolean P = true;
    /* access modifiers changed from: private */
    public boolean Q = false;
    private boolean R = true;
    /* access modifiers changed from: private */
    public boolean S = false;
    /* access modifiers changed from: private */
    public boolean T = false;
    /* access modifiers changed from: private */
    public al U;
    private cr V;
    /* access modifiers changed from: private */
    public b W;
    /* access modifiers changed from: private */
    public com.snda.youni.modules.d.b X = new com.snda.youni.modules.d.b();
    private boolean Y = false;
    /* access modifiers changed from: private */
    public ai Z = null;

    /* renamed from: a  reason: collision with root package name */
    int f184a = -1;
    private ServiceConnection aa = new de(this);
    /* access modifiers changed from: private */
    public com.snda.youni.mms.ui.a ab = null;
    private AbsListView.OnScrollListener ac = new dc(this);
    private AdapterView.OnItemLongClickListener ad = new db(this);
    private AdapterView.OnItemClickListener ae = new da(this);
    /* access modifiers changed from: private */
    public Handler af = new cz(this);
    private Handler ag = new cy(this);
    private Handler ah = new cx(this);
    private RadioGroup.OnCheckedChangeListener ai = new ao(this);
    private int aj = 0;
    private int ak;
    /* access modifiers changed from: private */
    public boolean al;
    private int am = 0;
    private cn an;
    private final i ao = new bb(this);
    private final Handler ap = new bl(this);
    private final Handler ar = new bk(this);
    private final Handler as = new bi(this);
    int b = 0;
    private boolean c;
    private int d;
    /* access modifiers changed from: private */
    public com.sd.android.mms.a.a f;
    /* access modifiers changed from: private */
    public Uri g;
    private EditText h;
    private String i;
    private f j;
    private com.sd.a.a.a.a.d k;
    private CharSequence l;
    /* access modifiers changed from: private */
    public long m;
    /* access modifiers changed from: private */
    public ListView n;
    /* access modifiers changed from: private */
    public ListAdapter o;
    /* access modifiers changed from: private */
    public ProgressDialog p;
    /* access modifiers changed from: private */
    public long q = -1;
    /* access modifiers changed from: private */
    public InputView r;
    /* access modifiers changed from: private */
    public com.snda.youni.h.a.a s;
    /* access modifiers changed from: private */
    public k t;
    /* access modifiers changed from: private */
    public h u;
    private String v;
    private LinearLayoutThatDetectsSoftKeyboard w;
    private RadioGroup x;
    /* access modifiers changed from: private */
    public RadioButton y;
    /* access modifiers changed from: private */
    public RadioButton z;

    static /* synthetic */ void D(ChatActivity chatActivity) {
        AlertDialog.Builder builder = new AlertDialog.Builder(chatActivity);
        builder.setIcon((int) C0000R.drawable.ic_dialog_attach);
        builder.setTitle((int) C0000R.string.add_attachment);
        builder.setAdapter(new com.snda.youni.mms.ui.h(chatActivity), new be(chatActivity));
        builder.show();
    }

    /* access modifiers changed from: private */
    public long a(String[] strArr) {
        HashSet hashSet = new HashSet();
        hashSet.addAll(Arrays.asList(strArr));
        return o.a(this, hashSet);
    }

    /* access modifiers changed from: private */
    public b a(int i2, String str) {
        try {
            return com.snda.youni.attachment.c.a.a(this, i2, str);
        } catch (g e2) {
            e2.printStackTrace();
            return null;
        }
    }

    /* access modifiers changed from: private */
    public b a(Uri uri) {
        try {
            return com.snda.youni.attachment.c.a.a(this, uri);
        } catch (g e2) {
            e2.printStackTrace();
            return null;
        }
    }

    private void a(Intent intent) {
        String str;
        String obj;
        String str2;
        int i2;
        String str3;
        int i3;
        int i4;
        Bundle extras = intent.getExtras();
        TextView textView = (TextView) findViewById(C0000R.id.txt_contact_name);
        TextView textView2 = (TextView) findViewById(C0000R.id.txt_sign);
        ImageView imageView = (ImageView) findViewById(C0000R.id.img_head);
        if (extras == null || extras.get("item") == null) {
            this.Y = true;
            com.snda.youni.modules.a.b bVar = new com.snda.youni.modules.a.b(getApplicationContext());
            if ("android.intent.action.SENDTO".equals(intent.getAction())) {
                if (intent.getDataString() != null) {
                    String replaceAll = intent.getDataString().replaceAll("%20", "");
                    String a2 = t.a(q.stripSeparators(replaceAll.substring(replaceAll.contains("smsto") ? replaceAll.indexOf("smsto") + 6 : intent.getDataString().contains("sms") ? replaceAll.indexOf("sms") + 4 : 0)));
                    com.snda.youni.modules.a.a a3 = bVar.a(a2);
                    String str4 = "";
                    if (a3 != null) {
                        int i5 = a3.f489a;
                        String str5 = a3.e;
                        textView.setText(a3.b);
                        String str6 = a3.d;
                        i3 = i5;
                        str4 = str6;
                        str3 = str5;
                    } else {
                        textView.setText(a2);
                        str3 = a2;
                        i3 = 0;
                    }
                    if (str3 != null && str3.contains("krobot")) {
                        this.X.c(true);
                    }
                    if (str3 != null) {
                        this.X.a(str3);
                    }
                    this.m = a(new String[]{a2});
                    textView2.setText(str4);
                    this.D.a(imageView, i3, 0);
                    this.M = i3;
                }
            } else if ("android.intent.action.VIEW".equalsIgnoreCase(intent.getAction())) {
                String dataString = intent.getDataString();
                "newIntent.getDataString() = " + dataString;
                if (!TextUtils.isEmpty(dataString)) {
                    String str7 = "conversations/";
                    int indexOf = dataString.indexOf(str7);
                    if (indexOf == -1) {
                        str7 = "threadID/";
                        indexOf = dataString.indexOf(str7);
                    }
                    if (indexOf != -1) {
                        str = dataString.substring(str7.length() + indexOf);
                    }
                    str = null;
                } else {
                    if (extras != null) {
                        str = ((Long) extras.get("thread_id")) != null ? ((Long) extras.get("thread_id")).toString() : null;
                        if (str == null && (obj = extras.get("sms_body").toString()) != null) {
                            Intent intent2 = new Intent(this, RecipientsActivity.class);
                            k kVar = new k();
                            kVar.b = obj;
                            intent2.putExtra("item", kVar);
                            startActivity(intent2);
                            finish();
                        }
                    }
                    str = null;
                }
                "threadId == " + str;
                if (str != null) {
                    this.m = Long.parseLong(str);
                    String a4 = t.a(q.stripSeparators(this.s.j(str)));
                    com.snda.youni.modules.a.a a5 = bVar.a(a4);
                    if (a5 != null) {
                        int i6 = a5.f489a;
                        String str8 = TextUtils.isEmpty(a5.e) ? a4 : a5.e;
                        if (TextUtils.isEmpty(a5.b)) {
                            textView.setText(str8);
                        } else {
                            textView.setText(a5.b);
                        }
                        textView2.setText(a5.d);
                        this.Q = a5.f;
                        str2 = str8;
                        i2 = i6;
                    } else {
                        textView.setText(a4);
                        str2 = a4;
                        i2 = 0;
                    }
                    if (str2 != null && str2.contains("krobot")) {
                        this.X.c(true);
                    }
                    "mOriginName:" + str2;
                    this.X.a(a4);
                    this.D.a(imageView, i2, 0);
                    this.M = i2;
                    ((NotificationManager) getSystemService("notification")).cancelAll();
                }
            } else {
                startActivity(new Intent(this, YouNi.class));
                finish();
            }
        } else {
            this.Y = false;
            this.t = (k) extras.get("item");
            if (this.t == null || this.t.o == null || !"notif".equalsIgnoreCase(this.t.o)) {
                com.snda.youni.g.a.a(getApplicationContext(), "click_main", null);
            } else {
                com.snda.youni.g.a.a(getApplicationContext(), "click_notif", null);
                this.Y = true;
            }
            if (this.t.n != null && this.t.n.split(" ").length > 1) {
                "addressIds:" + this.t.n;
                String[] split = this.t.n.split(" ");
                if (split != null) {
                    String[] strArr = new String[split.length];
                    for (int i7 = 0; i7 < split.length; i7++) {
                        String f2 = this.s.f(split[i7]);
                        if (f2 != null) {
                            strArr[i7] = f2;
                        }
                    }
                    this.X.a(strArr);
                    "address address:" + strArr.toString();
                }
                this.X.d(true);
            }
            if (this.t.p != null && this.t.p.length > 1) {
                this.X.a(this.t.p);
                this.X.d(true);
            }
            String a6 = q.a(t.a(this.t.g));
            this.N = this.t.f ? 1 : 0;
            this.Q = this.t.f;
            if (this.t.k == null) {
                this.t.k = "" + a(new String[]{a6});
            }
            com.snda.youni.modules.a.a a7 = new com.snda.youni.modules.a.b(getApplicationContext()).a(a6);
            if (a6 != null && a6.contains("krobot")) {
                this.X.c(true);
            }
            int i8 = this.t.i;
            if (this.X.r() || !(this.t.f496a == null || this.t.i == 0)) {
                i4 = i8;
            } else if (a7 != null) {
                int i9 = a7.f489a;
                this.t.f496a = a7.b;
                i4 = i9;
            } else {
                i4 = this.t.i;
                this.t.f496a = a6;
            }
            if (this.t.f496a != null && this.t.f496a.startsWith("krobot")) {
                this.t.f496a = getString(C0000R.string.youni_robot);
            }
            this.X.a(a6);
            this.m = Long.parseLong(this.t.k);
            textView.setText(this.t.f496a);
            textView2.setText(this.t.l);
            this.M = i4;
            if (this.X.r()) {
                this.D.a(imageView, 0, (int) C0000R.drawable.broadcast_portrait);
            } else {
                this.D.a(imageView, i4, 0);
            }
            com.snda.youni.modules.d.b bVar2 = (com.snda.youni.modules.d.b) extras.get("messageobject");
            if (bVar2 != null) {
                new com.snda.youni.attachment.a.f(this, this.ar).a(bVar2);
            }
        }
    }

    static /* synthetic */ void a(ChatActivity chatActivity, int i2) {
        String str;
        String str2;
        chatActivity.u.a(chatActivity);
        chatActivity.p = new ProgressDialog(chatActivity);
        chatActivity.p.setTitle("");
        chatActivity.p.setMessage("");
        chatActivity.p.setIndeterminate(true);
        chatActivity.p.setCancelable(false);
        chatActivity.p.show();
        switch (i2) {
            case 5:
                ((CursorAdapter) chatActivity.o).changeCursor(null);
                chatActivity.U.startDelete(1, null, ContentUris.withAppendedId(Uri.parse("content://mms-sms/conversations"), chatActivity.m), null, null);
                return;
            case 6:
                Uri.parse("content://mms/");
                if (((com.snda.youni.mms.ui.k) chatActivity.o).e()) {
                    String str3 = "thread_id=" + chatActivity.m + " AND " + "_id" + " NOT IN ( ";
                    String str4 = "thread_id=" + chatActivity.m + " AND " + "_id" + " NOT IN ( ";
                    for (au auVar : ((com.snda.youni.mms.ui.k) chatActivity.o).c()) {
                        if ("mms".equals(auVar.b)) {
                            str3 = str3 + auVar.f463a + ",";
                        } else {
                            str4 = "sms".equals(auVar.b) ? str4 + auVar.f463a + "," : str4;
                        }
                    }
                    str = str3;
                    str2 = str4;
                } else {
                    String str5 = "_id IN ( ";
                    String str6 = "_id IN ( ";
                    for (au auVar2 : ((com.snda.youni.mms.ui.k) chatActivity.o).b()) {
                        if ("mms".equals(auVar2.b)) {
                            str5 = str5 + auVar2.f463a + ",";
                        } else {
                            str6 = "sms".equals(auVar2.b) ? str6 + auVar2.f463a + "," : str6;
                        }
                    }
                    str = str5;
                    str2 = str6;
                }
                chatActivity.U.startDelete(3, null, ContentUris.withAppendedId(Uri.parse("content://mms-sms/conversations"), chatActivity.m), (str2.substring(0, str2.length() - 1) + ")") + " OR " + (str.substring(0, str.length() - 1) + ")"), null);
                return;
            default:
                return;
        }
    }

    private void a(com.snda.youni.modules.d.b bVar) {
        int i2;
        com.snda.youni.h.a.a aVar = new com.snda.youni.h.a.a(this);
        if (bVar != null && bVar.a() != null) {
            if (!"".equalsIgnoreCase(bVar.b()) || (bVar.c() != null && !"".equalsIgnoreCase(bVar.c()))) {
                aVar.c(bVar);
                "SaveDraftTask doInBackground end, m.getThreadId()=" + bVar.h();
                return;
            }
            if (this.m > 0) {
                Uri withAppendedId = ContentUris.withAppendedId(m.f14a, this.X.l());
                "saveOrdiscardDraft, uri:" + withAppendedId.toString();
                i2 = getContentResolver().delete(withAppendedId, null, null);
                "saveOrdiscardDraft, num=" + i2;
                if (i2 > 0) {
                    com.sd.android.mms.d.d.a().a(this.m, false);
                    com.snda.youni.modules.a.l.a(this, this.m).b();
                }
            } else {
                i2 = 0;
            }
            "SaveDraftTask delete draft:" + (i2 <= 0 ? aVar.a(bVar.a()) : i2);
        }
    }

    /* access modifiers changed from: private */
    public void a(String str) {
        this.X.b(str);
        this.X.b(true);
        this.X.a(Long.valueOf(System.currentTimeMillis()));
        this.X.a(this.m);
        this.X.c(this.W.h());
        "mDraftMessgae getSubject==========" + this.W.h();
        this.X.a(this.W);
        this.X.b(0);
        new com.snda.youni.attachment.a.f(this, this.ar).a(this.X);
        this.X = this.X.clone();
        this.W = new b();
    }

    private String b(int i2, String str) {
        return getResources().getString(i2, str);
    }

    static /* synthetic */ void b(ChatActivity chatActivity, int i2) {
        switch (i2) {
            case 0:
                com.snda.youni.mms.ui.m.c(chatActivity);
                return;
            case 1:
                chatActivity.startActivityForResult(new Intent("android.media.action.IMAGE_CAPTURE"), 11);
                return;
            case 2:
            case 3:
            default:
                return;
            case 4:
                com.snda.youni.mms.ui.m.a(chatActivity);
                return;
            case 5:
                com.snda.youni.mms.ui.m.b(chatActivity);
                return;
        }
    }

    private void b(String str) {
        String[] strArr = {this.X.a()};
        if (this.X.r()) {
            strArr = this.X.o();
        }
        if (strArr.length > 1) {
            com.snda.youni.g.a.a(getApplicationContext(), "group_message", null);
            YouniService.a(strArr.length);
        }
        SharedPreferences defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        for (int i2 = 0; i2 < strArr.length; i2++) {
            if (strArr[i2] != null) {
                com.snda.youni.modules.d.b bVar = new com.snda.youni.modules.d.b();
                bVar.a(q.a(t.a(strArr[i2])));
                this.ak = defaultSharedPreferences.getInt("wapprompt_" + this.X.a(), -1);
                "wapprompt value=" + this.ak + ",wapswitch=" + defaultSharedPreferences.getInt("wap_allowed", 0) + ",isYouni=" + this.Q + ",isYouniUser" + this.al + "@sendaddress" + i2 + "=" + strArr[i2];
                bVar.b(str);
                bVar.b(str);
                bVar.b(true);
                bVar.d(this.X.r());
                bVar.a(Long.valueOf(System.currentTimeMillis()));
                bVar.a(this.m);
                bVar.c(this.X.n());
                YouniService.a();
                this.s.b(this.Z, bVar);
                if (this.s.b(this.m + "") == 0) {
                    this.s.a(this.X.a());
                }
                m();
            }
        }
        if (this.o != null) {
            ((CursorAdapter) this.o).notifyDataSetChanged();
        }
    }

    private String c(int i2) {
        return getResources().getString(i2);
    }

    /* access modifiers changed from: private */
    public void d(boolean z2) {
        "setScrollToButton:" + z2;
        this.R = z2;
        if (this.n == null) {
            return;
        }
        if (this.R) {
            this.n.setTranscriptMode(2);
        } else {
            this.n.setTranscriptMode(0);
        }
    }

    static /* synthetic */ void e(ChatActivity chatActivity) {
        String a2 = chatActivity.Z.a(chatActivity.X.a(), new dd(chatActivity));
        "status:" + a2;
        if ("OFFLINE0".equalsIgnoreCase(a2)) {
            chatActivity.af.sendEmptyMessage(0);
        }
    }

    /* access modifiers changed from: private */
    public void e(boolean z2) {
        if (z2) {
            this.T = true;
            this.r.a((Context) this);
            this.x.setVisibility(8);
            this.r.setVisibility(8);
            findViewById(C0000R.id.message_batch_operations).setVisibility(0);
            this.E.setVisibility(8);
            return;
        }
        this.T = false;
        this.x.setVisibility(0);
        this.r.setVisibility(0);
        findViewById(C0000R.id.message_batch_operations).setVisibility(8);
        ((com.snda.youni.mms.ui.k) this.o).b().clear();
    }

    static /* synthetic */ void f(ChatActivity chatActivity) {
        if (chatActivity.B != null && !chatActivity.X.r()) {
            chatActivity.B.setVisibility(0);
            chatActivity.B.setBackgroundResource(C0000R.drawable.on_line);
            chatActivity.r.b(true);
        }
        com.snda.youni.modules.chat.b.a(true);
    }

    private void f(boolean z2) {
        w[] wVarArr;
        if (z2) {
            try {
                if (this.g != null) {
                    this.g = this.k.a(this.g, j.f12a);
                    this.f = com.sd.android.mms.a.a.a(this, this.g);
                } else {
                    com.sd.android.mms.a.a a2 = com.sd.android.mms.a.a.a(this);
                    com.sd.android.mms.a.h hVar = new com.sd.android.mms.a.h(a2, (byte) 0);
                    hVar.add((e) new p(this, "text/plain", "text_0.txt", a2.c().b()));
                    a2.add(hVar);
                    this.f = a2;
                    if (this.l != null) {
                        this.f.get(0).m().a(this.l);
                    }
                    com.sd.a.a.a.a.j jVar = new com.sd.a.a.a.a.j();
                    String[] strArr = {this.X.a()};
                    int length = strArr.length;
                    if (length > 0) {
                        w[] wVarArr2 = new w[length];
                        for (int i2 = 0; i2 < length; i2++) {
                            wVarArr2[i2] = new w(strArr[i2]);
                        }
                        wVarArr = wVarArr2;
                    } else {
                        wVarArr = null;
                    }
                    if (wVarArr != null) {
                        jVar.a(wVarArr);
                    }
                    if (this.i != null) {
                        jVar.b(new w(this.i));
                    }
                    jVar.b(System.currentTimeMillis() / 1000);
                    com.sd.a.a.a.a.e a3 = this.f.a();
                    jVar.a(a3);
                    Uri a4 = this.k.a(jVar, j.f12a);
                    this.f.a(a3);
                    this.g = a4;
                }
                this.j = new f(this, this.ap, findViewById(C0000R.id.attachment_editor));
                this.j.a(this);
                int a5 = com.snda.youni.mms.ui.m.a(this.f);
                com.sd.android.mms.a.a aVar = this.f;
                if (aVar.size() == 0) {
                    aVar.add(new com.sd.android.mms.a.h(aVar, (byte) 0));
                }
                if (!aVar.get(0).e()) {
                    aVar.get(0).add((e) new p(this, "text/plain", "text_0.txt", aVar.c().b()));
                }
                if (a5 == -1) {
                    a5 = 0;
                }
                this.j.a(this.f, a5);
                if (a5 > 0) {
                    g(true);
                }
            } catch (com.sd.a.a.a.b e2) {
                e2.getMessage() + " " + e2;
                finish();
            }
        } else {
            n();
        }
    }

    static /* synthetic */ void g(ChatActivity chatActivity) {
        if (chatActivity.B != null) {
            chatActivity.B.setVisibility(8);
            chatActivity.r.b(false);
        }
        com.snda.youni.modules.chat.b.a(false);
    }

    private void g(boolean z2) {
        if (z2) {
            this.d |= 4;
        } else {
            this.d &= -5;
        }
    }

    private void h(boolean z2) {
        boolean z3;
        int i2 = this.d;
        g(z2);
        if (i2 == 0 && this.d != 0) {
            z3 = true;
        } else if (i2 != 0 && this.d == 0) {
            z3 = false;
        } else {
            return;
        }
        if (com.sd.android.mms.a.a() || !z3) {
            f(z3);
            return;
        }
        throw new IllegalStateException("Message converted to MMS with DISABLE_MMS set");
    }

    /* access modifiers changed from: private */
    public void m() {
        if (this.m == 0) {
            this.m = a(new String[]{this.X.a()});
            Cursor a2 = this.s.a(this.m);
            if (!(a2 == null || this.o != null || this.n == null)) {
                this.o = new com.snda.youni.mms.ui.k(this, a2, this.n, this.X.r());
                this.n.setAdapter(this.o);
                ((com.snda.youni.mms.ui.k) this.o).a(this.as);
            }
            "after send currentThreadId:" + this.m;
        }
    }

    private synchronized void n() {
        com.sd.android.mms.a.h b2;
        p m2;
        if (!(this.j == null || this.f == null || this.j.a() != 0 || this.f == null || (b2 = this.f.get(0)) == null || (m2 = b2.m()) == null)) {
            this.l = m2.x();
        }
        this.d = 0;
        this.f = null;
        if (this.g != null && this.g.toString().startsWith(j.f12a.toString())) {
            this.g = null;
        }
        if (this.h != null) {
            this.h.setText("");
            this.h.setVisibility(8);
            this.h = null;
        }
        this.i = null;
        this.j = null;
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x0033  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0016  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void o() {
        /*
            r3 = this;
            r2 = 1
            r1 = 0
            boolean r0 = r3.p()
            if (r0 != 0) goto L_0x0013
            com.snda.youni.modules.InputView r0 = r3.r
            int r0 = r0.g()
            if (r0 <= 0) goto L_0x0029
            r0 = r2
        L_0x0011:
            if (r0 == 0) goto L_0x002b
        L_0x0013:
            r0 = r2
        L_0x0014:
            if (r0 == 0) goto L_0x0033
            com.snda.youni.mms.ui.f r0 = r3.j
            if (r0 == 0) goto L_0x0023
            com.snda.youni.mms.ui.f r0 = r3.j
            int r0 = r0.a()
            r1 = 4
            if (r0 == r1) goto L_0x002d
        L_0x0023:
            com.snda.youni.modules.InputView r0 = r3.r
            r0.a(r2)
        L_0x0028:
            return
        L_0x0029:
            r0 = r1
            goto L_0x0011
        L_0x002b:
            r0 = r1
            goto L_0x0014
        L_0x002d:
            com.snda.youni.mms.ui.f r0 = r3.j
            r0.a(r2)
            goto L_0x0023
        L_0x0033:
            com.snda.youni.mms.ui.f r0 = r3.j
            if (r0 == 0) goto L_0x003c
            com.snda.youni.mms.ui.f r0 = r3.j
            r0.a(r1)
        L_0x003c:
            com.snda.youni.modules.InputView r0 = r3.r
            r0.a(r1)
            goto L_0x0028
        */
        throw new UnsupportedOperationException("Method not decompiled: com.snda.youni.activities.ChatActivity.o():void");
    }

    private boolean p() {
        return this.j != null && this.j.a() > 0;
    }

    static /* synthetic */ void q(ChatActivity chatActivity) {
        Cursor a2 = chatActivity.s.a("service_center like '%sys_%'", "date DESC");
        if (a2 != null && a2.moveToFirst()) {
            ContentValues contentValues = new ContentValues();
            contentValues.put("date", Long.valueOf(System.currentTimeMillis()));
            do {
                chatActivity.s.a(a2.getLong(a2.getColumnIndex("_id")), contentValues);
            } while (a2.moveToNext());
            a2.close();
        }
    }

    private boolean q() {
        return this.d > 0;
    }

    private boolean r() {
        if (this.X.a() == null || com.snda.youni.modules.chat.b.a() == null || !this.X.a().contains(com.snda.youni.modules.chat.b.a())) {
            return false;
        }
        WarningTipView warningTipView = (WarningTipView) findViewById(C0000R.id.chat_warning);
        warningTipView.a(getString(C0000R.string.not_send_to_self));
        warningTipView.setVisibility(0);
        warningTipView.a(0);
        warningTipView.a();
        this.r.a(false);
        return true;
    }

    public final void a() {
        if (this.X.a() == null || com.snda.youni.modules.chat.b.a() == null || !this.X.a().contains(com.snda.youni.modules.chat.b.a())) {
            String obj = this.r.a().toString();
            this.i = obj;
            this.r.a("");
            if (!TextUtils.isEmpty(obj.trim()) || this.j != null) {
                int indexOf = obj.indexOf(com.snda.youni.e.m.a().a(20));
                if (indexOf >= 0) {
                    String string = PreferenceManager.getDefaultSharedPreferences(this).getString("postion", "");
                    if ("".equalsIgnoreCase(string)) {
                        Toast.makeText(this, (int) C0000R.string.emotion_location_failed, 1).show();
                    } else if (!obj.contains("[http://maps.google.com/maps")) {
                        obj = obj.substring(0, indexOf) + string + obj.substring(indexOf);
                    }
                }
                if (q()) {
                    a(this.r.a().toString());
                    if (this.j != null) {
                        this.j.c();
                    }
                    this.d &= 1;
                    this.f = null;
                    this.g = null;
                    this.W = null;
                    this.X = null;
                    this.l = "";
                    this.j.b();
                    this.j = null;
                    this.r.a(false);
                    return;
                }
                SharedPreferences defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
                if (!this.X.r()) {
                    this.ak = defaultSharedPreferences.getInt("wapprompt_" + this.X.a(), -1);
                } else {
                    this.ak = 1;
                }
                b(obj);
                return;
            }
            Toast.makeText(this, (int) C0000R.string.empty_message_warning, 0).show();
            return;
        }
        WarningTipView warningTipView = (WarningTipView) findViewById(C0000R.id.chat_warning);
        warningTipView.a(getString(C0000R.string.not_send_to_self));
        warningTipView.setVisibility(0);
        warningTipView.a(0);
        warningTipView.a();
        this.r.a(false);
    }

    public final void a(int i2) {
        if (i2 > 0) {
            g(true);
        } else {
            h(false);
        }
        o();
    }

    public final void a(boolean z2) {
        if (z2) {
            d(true);
            this.E.setVisibility(8);
            findViewById(C0000R.id.btn_panel_above).setVisibility(0);
            this.r.b();
            return;
        }
        this.r.c();
        findViewById(C0000R.id.btn_panel_above).setVisibility(8);
        if (this.T) {
            this.E.setVisibility(8);
        }
    }

    public final boolean a(long j2) {
        return s.e == 0 && j2 == this.m;
    }

    public final void b() {
        this.E.setVisibility(8);
        WarningTipView warningTipView = (WarningTipView) findViewById(C0000R.id.chat_warning);
        if (this.O) {
            if (Settings.System.getInt(getApplicationContext().getContentResolver(), "airplane_mode_on", 0) != 0) {
                if (!this.P || !this.Q) {
                    warningTipView.a(getString(C0000R.string.air_mode));
                    warningTipView.setVisibility(0);
                    warningTipView.a();
                    this.O = false;
                }
            } else if (!this.P) {
                warningTipView.a(getString(C0000R.string.no_network));
                warningTipView.setVisibility(0);
                warningTipView.a();
                this.O = false;
            }
            if (com.snda.youni.e.j.a(getApplicationContext())) {
                warningTipView.a(getString(C0000R.string.tip_for_wap));
                warningTipView.setVisibility(0);
                warningTipView.a();
                this.O = false;
            }
        }
        if (!this.P && q.b(this.X.a())) {
            warningTipView.a(getString(C0000R.string.send_may_failed));
            warningTipView.setVisibility(0);
            warningTipView.a(0);
            warningTipView.a();
        }
    }

    public final void b(int i2) {
        String obj = this.r.a().toString();
        this.r.a("");
        this.s.b(this.m + "");
        String str = Long.toHexString(Double.doubleToLongBits(Math.random())) + ".amr";
        new File(com.snda.youni.attachment.e.g, "youni_audio.amr").renameTo(new File(com.snda.youni.attachment.e.g, str));
        new Thread(new bj(this, i2, str, obj)).start();
        if (com.snda.youni.e.o.a() && !this.T) {
            this.E.setVisibility(0);
            this.E.setBackgroundResource(C0000R.drawable.btn_bottom_record_audio_normal);
        } else if (!com.snda.youni.e.o.a()) {
            Toast.makeText(this, (int) C0000R.string.sdcard_not_working, 0).show();
        }
        d(true);
    }

    public final void b(boolean z2) {
        if (z2) {
            this.ag.sendEmptyMessage(1);
        } else {
            this.ag.sendEmptyMessage(0);
        }
    }

    public final void c() {
        if (this.Z != null) {
            this.Z.b(this.X.a());
        }
    }

    public final void c(boolean z2) {
        "resetStatus:" + z2;
        if (z2) {
            this.af.sendEmptyMessage(1);
        } else {
            this.af.sendEmptyMessage(0);
        }
    }

    public final void d() {
        o();
    }

    public final void e() {
        this.E.setVisibility(8);
        d(true);
        if (this.V != null && this.V.isShowing()) {
            this.V.b();
        }
    }

    public final void f() {
        this.r.setVisibility(0);
        this.E.setBackgroundResource(C0000R.drawable.btn_bottom_record_audio_normal);
        com.snda.youni.e.e.a("youni_audio.amr", com.snda.youni.attachment.e.g);
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:6:0x0027  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void g() {
        /*
            r5 = this;
            r4 = 1
            r3 = 0
            r2 = 8
            android.content.SharedPreferences r0 = android.preference.PreferenceManager.getDefaultSharedPreferences(r5)
            java.lang.String r1 = "chat_switch_youni"
            boolean r0 = r0.getBoolean(r1, r4)
            if (r0 == 0) goto L_0x0032
            android.widget.ImageView r0 = r5.B
            r0.setVisibility(r3)
            com.snda.youni.modules.InputView r0 = r5.r
            r0.b(r4)
        L_0x001a:
            android.widget.RadioButton r0 = r5.y
            r0.setVisibility(r2)
        L_0x001f:
            com.snda.youni.modules.d.b r0 = r5.X
            boolean r0 = r0.r()
            if (r0 == 0) goto L_0x0031
            android.widget.RadioGroup r0 = r5.x
            r0.setVisibility(r2)
            android.widget.RadioButton r0 = r5.z
            r0.setVisibility(r2)
        L_0x0031:
            return
        L_0x0032:
            boolean r0 = r5.Q
            if (r0 != 0) goto L_0x001a
            android.widget.ImageView r0 = r5.B
            r0.setVisibility(r2)
            com.snda.youni.modules.InputView r0 = r5.r
            r0.b(r3)
            android.widget.RadioButton r0 = r5.y
            r0.setVisibility(r3)
            goto L_0x001f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.snda.youni.activities.ChatActivity.g():void");
    }

    public final void h() {
        this.ah.sendEmptyMessage(1);
    }

    public final String i() {
        return this.X.a();
    }

    public final boolean j() {
        return this.T;
    }

    public final void k() {
        this.f184a = this.n.getFirstVisiblePosition();
        View childAt = this.n.getChildAt(0);
        this.b = childAt == null ? 0 : childAt.getTop();
        "scroll idle with item = " + this.f184a + ", top=" + this.b;
    }

    public final void l() {
        this.n.post(new bc(this));
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        Uri data;
        this.c = false;
        if (i3 != -1) {
            h(p());
            return;
        }
        if (!q()) {
            f(true);
        }
        switch (i2) {
            case 10:
                com.snda.youni.g.a.a(getApplicationContext(), "send_image", null);
                Uri data2 = intent.getData();
                String obj = this.r.a().toString();
                int indexOf = obj.indexOf(com.snda.youni.e.m.a().a(20));
                if (indexOf != -1) {
                    int indexOf2 = obj.indexOf("[http://maps.google.com/maps");
                    this.r.a((indexOf2 == -1 || indexOf2 >= indexOf) ? obj.substring(0, indexOf) + obj.substring(indexOf + com.snda.youni.e.m.a().a(20).length()) : obj.substring(0, indexOf2) + obj.substring(indexOf + com.snda.youni.e.m.a().a(20).length()));
                }
                String obj2 = this.r.a().toString();
                this.r.a("");
                this.s.b(this.m + "");
                new Thread(new bm(this, data2, obj2)).start();
                break;
            case 11:
                com.snda.youni.g.a.a(getApplicationContext(), "send_image", null);
                String obj3 = this.r.a().toString();
                int indexOf3 = obj3.indexOf(com.snda.youni.e.m.a().a(20));
                if (indexOf3 != -1) {
                    int indexOf4 = obj3.indexOf("[http://maps.google.com/maps");
                    this.r.a((indexOf4 == -1 || indexOf4 >= indexOf3) ? obj3.substring(0, indexOf3) + obj3.substring(indexOf3 + com.snda.youni.e.m.a().a(20).length()) : obj3.substring(0, indexOf4) + obj3.substring(indexOf3 + com.snda.youni.e.m.a().a(20).length()));
                }
                String obj4 = this.r.a().toString();
                this.r.a("");
                this.s.b(this.m + "");
                new Thread(new bf(this, obj4)).start();
                break;
            case 14:
            case 15:
                if (i2 == 14) {
                    data = (Uri) intent.getParcelableExtra("android.intent.extra.ringtone.PICKED_URI");
                    if (Settings.System.DEFAULT_RINGTONE_URI.equals(data)) {
                        data = null;
                    }
                } else {
                    data = intent.getData();
                }
                if (data != null) {
                    try {
                        this.j.b(data);
                        this.j.a(this.f, 3);
                        break;
                    } catch (com.sd.a.a.a.b e2) {
                        "add audio failed " + e2;
                        Toast.makeText(this, b((int) C0000R.string.failed_to_add_media, c((int) C0000R.string.type_audio)), 0).show();
                        break;
                    } catch (com.sd.android.mms.b e3) {
                        com.snda.youni.mms.ui.m.a(this, b((int) C0000R.string.unsupported_media_format, c((int) C0000R.string.type_audio)), b((int) C0000R.string.select_different_media, c((int) C0000R.string.type_audio)));
                        break;
                    } catch (com.sd.android.mms.d e4) {
                        com.snda.youni.mms.ui.m.a(this, c((int) C0000R.string.exceed_message_size_limitation), b((int) C0000R.string.failed_to_add_media, c((int) C0000R.string.type_audio)));
                        break;
                    }
                } else {
                    h(p());
                    return;
                }
        }
        if (!q()) {
            f(false);
        }
        if (this.j != null) {
            TranslateAnimation translateAnimation = new TranslateAnimation(1, 0.0f, 1, 0.0f, 1, 150.0f, 1, 0.0f);
            translateAnimation.setDuration(500);
            ((LinearLayout) findViewById(C0000R.id.attachment_editor)).startAnimation(translateAnimation);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public void onClick(View view) {
        switch (view.getId()) {
            case C0000R.id.btn_above_smile /*2131558475*/:
                if (r()) {
                    return;
                }
                if (!com.snda.youni.e.o.a()) {
                    Toast.makeText(this, (int) C0000R.string.sdcard_not_working, 1).show();
                    return;
                } else {
                    this.r.e();
                    return;
                }
            case C0000R.id.btn_above_camera /*2131558476*/:
                if (r()) {
                    return;
                }
                if (!com.snda.youni.e.o.a()) {
                    Toast.makeText(this, (int) C0000R.string.sdcard_not_working, 1).show();
                    return;
                }
                this.r.a((Context) this);
                d(true);
                Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
                intent.putExtra("output", Uri.fromFile(new File(com.snda.youni.attachment.e.g, "youni_camera.jpg")));
                startActivityForResult(intent, 11);
                return;
            case C0000R.id.btn_above_gallery /*2131558477*/:
                if (r()) {
                    return;
                }
                if (!com.snda.youni.e.o.a()) {
                    Toast.makeText(this, (int) C0000R.string.sdcard_not_working, 1).show();
                    return;
                }
                this.r.a((Context) this);
                d(true);
                Intent intent2 = new Intent("android.intent.action.PICK");
                intent2.setType("image/*");
                startActivityForResult(Intent.createChooser(intent2, getString(C0000R.string.title_choose_gallery)), 10);
                return;
            case C0000R.id.btn_above_sound /*2131558478*/:
                if (r()) {
                    return;
                }
                if (!com.snda.youni.e.o.a()) {
                    Toast.makeText(this, (int) C0000R.string.sdcard_not_working, 1).show();
                    return;
                } else {
                    this.r.f();
                    return;
                }
            case C0000R.id.input /*2131558479*/:
            case C0000R.id.message_batch_operations /*2131558481*/:
            default:
                return;
            case C0000R.id.btn_record_sound /*2131558480*/:
                if (this.aj != 1) {
                    this.aj = 1;
                    if (r()) {
                        return;
                    }
                    if (!com.snda.youni.e.o.a()) {
                        Toast.makeText(this, (int) C0000R.string.sdcard_not_working, 1).show();
                        return;
                    }
                    if (this.V == null || !this.V.isShowing()) {
                        this.r.a((Context) this);
                        ((ImageButton) view).setBackgroundResource(C0000R.drawable.btn_bottom_record_audio_pressed);
                        View inflate = ((LayoutInflater) getSystemService("layout_inflater")).inflate((int) C0000R.layout.record_audio_window, (ViewGroup) null);
                        this.r.setVisibility(8);
                        d(true);
                        this.V = new cr(this, inflate, this.w);
                        this.V.a();
                        com.snda.youni.g.a.a(getApplicationContext(), "make_voice", null);
                    } else {
                        int b2 = this.V.b();
                        this.r.setVisibility(0);
                        ((ImageButton) view).setBackgroundResource(C0000R.drawable.btn_bottom_record_audio_normal);
                        if (b2 < 2) {
                            Toast.makeText(this, (int) C0000R.string.record_audio_too_short, 0).show();
                            com.snda.youni.e.e.a("youni_audio.amr", com.snda.youni.attachment.e.g);
                        } else {
                            b(b2);
                        }
                    }
                    this.aj = 0;
                    return;
                }
                return;
            case C0000R.id.message_batch_check /*2131558482*/:
                ((com.snda.youni.mms.ui.k) this.o).a(view);
                this.n.invalidateViews();
                return;
            case C0000R.id.message_batch_operations_forward /*2131558483*/:
                if (((com.snda.youni.mms.ui.k) this.o).e()) {
                    if (((com.snda.youni.mms.ui.k) this.o).c().size() == this.o.getCount()) {
                        Toast.makeText(this, (int) C0000R.string.no_select_notification, 0).show();
                        return;
                    }
                } else if (((com.snda.youni.mms.ui.k) this.o).b().size() == 0) {
                    Toast.makeText(this, (int) C0000R.string.no_select_notification, 0).show();
                    return;
                }
                Intent intent3 = new Intent(this, RecipientsActivity.class);
                intent3.putExtra("message_body", ((com.snda.youni.mms.ui.k) this.o).d());
                intent3.putExtra("mms", false);
                startActivity(intent3);
                e(false);
                return;
            case C0000R.id.message_batch_delete_message /*2131558484*/:
                if (((com.snda.youni.mms.ui.k) this.o).e()) {
                    if (((com.snda.youni.mms.ui.k) this.o).c().size() == this.o.getCount()) {
                        Toast.makeText(this, (int) C0000R.string.no_select_notification, 0).show();
                        return;
                    }
                } else if (((com.snda.youni.mms.ui.k) this.o).b().size() == 0) {
                    Toast.makeText(this, (int) C0000R.string.no_select_notification, 0).show();
                    return;
                }
                showDialog(6);
                return;
        }
    }

    public boolean onContextItemSelected(MenuItem menuItem) {
        b bVar;
        b bVar2;
        switch (menuItem.getItemId()) {
            case C0000R.id.menu_message_delete_message /*2131558690*/:
                k();
                if (this.q != -1) {
                    "delete one number:" + getContentResolver().delete(ContentUris.withAppendedId(o.f16a, this.m), "_id=" + this.q, null);
                    this.q = -1;
                }
                return true;
            case C0000R.id.menu_message_batch_opertations /*2131558691*/:
                e(true);
                return true;
            case C0000R.id.menu_message_resend /*2131558692*/:
                ContentValues contentValues = new ContentValues();
                contentValues.put("service_center", "+666666");
                getContentResolver().update(Uri.parse("content://sms/" + this.ab.g()), contentValues, null, null);
                String k2 = this.ab.k();
                if (k2 != null && (k2.endsWith(".amr") || k2.endsWith(".jpg"))) {
                    Cursor query = getContentResolver().query(com.snda.youni.providers.b.f592a, null, "filename=?", new String[]{this.ab.k()}, null);
                    if (query != null) {
                        if (query.moveToNext()) {
                            bVar2 = new b(query);
                            bVar2.a(Long.parseLong(query.getString(query.getColumnIndex("_id"))));
                        } else {
                            bVar2 = null;
                        }
                        query.close();
                        bVar = bVar2;
                    } else {
                        bVar = null;
                    }
                    if (bVar != null) {
                        com.snda.youni.attachment.a.f fVar = new com.snda.youni.attachment.a.f(this, this.ar);
                        this.X.a(bVar);
                        this.X.b(this.ab.g());
                        fVar.a(this.X);
                    }
                }
                return true;
            case C0000R.id.menu_message_reload /*2131558693*/:
                ContentValues contentValues2 = new ContentValues();
                contentValues2.put("subject", "111111");
                getContentResolver().update(Uri.parse("content://sms/" + this.ab.g()), contentValues2, null, null);
                return true;
            case C0000R.id.menu_message_forward /*2131558694*/:
                com.snda.youni.g.a.a(getApplicationContext(), "transfer_message", null);
                Intent intent = new Intent(this, RecipientsActivity.class);
                intent.putExtra("_id", this.ab.g());
                intent.putExtra("mms", this.ab.m());
                startActivity(intent);
                return true;
            case C0000R.id.menu_message_copy /*2131558695*/:
                com.snda.youni.modules.d.b d2 = this.s.d(Long.toString(this.q));
                if (d2 != null) {
                    ((ClipboardManager) getSystemService("clipboard")).setText(d2.b());
                    Toast.makeText(getApplicationContext(), (int) C0000R.string.copy_success, 1).show();
                } else {
                    Toast.makeText(getApplicationContext(), (int) C0000R.string.copy_fail, 1).show();
                }
                return true;
            case C0000R.id.menu_message_favorite /*2131558696*/:
                int b2 = this.ab.b();
                Intent intent2 = new Intent(this, FavoriteActivity.class);
                intent2.putExtra("box_type", b2);
                intent2.putExtra("contactid", this.M);
                "put favorite contactid=============" + this.M;
                intent2.setData(ContentUris.withAppendedId(Uri.parse("content://sms/"), this.ab.g()));
                startActivity(intent2);
                return true;
            default:
                return false;
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        getApplicationContext();
        AppContext.b();
        getWindow().setSoftInputMode(18);
        Thread.currentThread().setUncaughtExceptionHandler(new com.snda.youni.g.b(this));
        com.snda.youni.g.a.a(getApplicationContext(), "click_chat", null);
        com.snda.youni.g.a.b(getApplicationContext());
        requestWindowFeature(1);
        setContentView((int) C0000R.layout.compose_message_activity);
        if (!(bundle == null || bundle.getInt("mMessageLongClickedIndex") == -1)) {
            this.q = bundle.getLong("mMessageLongClickedIndex");
        }
        this.U = new al(this, getContentResolver());
        this.s = new com.snda.youni.h.a.a(getApplicationContext());
        this.u = new h(this.s);
        this.D = new com.snda.youni.d(this);
        Intent intent = new Intent(this, YouniService.class);
        intent.putExtra("Reconnect", "reconnect");
        bindService(intent, this.aa, 1);
        this.w = (LinearLayoutThatDetectsSoftKeyboard) findViewById(C0000R.id.chat_activity_root);
        this.w.a(this);
        this.C = (TextView) findViewById(C0000R.id.txt_chatting);
        this.r = (InputView) findViewById(C0000R.id.input);
        this.r.a((com.snda.youni.modules.l) this);
        this.x = (RadioGroup) findViewById(C0000R.id.group_chat_btns);
        this.y = (RadioButton) this.x.findViewById(C0000R.id.btn_invite);
        this.z = (RadioButton) this.x.findViewById(C0000R.id.btn_phone);
        this.B = (ImageView) findViewById(C0000R.id.img_status);
        this.A = (RadioButton) this.x.findViewById(C0000R.id.btn_notification);
        this.n = (ListView) findViewById(C0000R.id.history);
        this.n.setOnItemLongClickListener(this.ad);
        this.n.setOnItemClickListener(this.ae);
        this.n.setOnScrollListener(this.ac);
        registerForContextMenu(this.n);
        this.x.setOnCheckedChangeListener(this.ai);
        this.F = (Button) findViewById(C0000R.id.message_batch_check);
        this.F.setOnClickListener(this);
        this.G = (Button) findViewById(C0000R.id.message_batch_delete_message);
        this.G.setOnClickListener(this);
        this.H = (Button) findViewById(C0000R.id.message_batch_operations_forward);
        this.H.setOnClickListener(this);
        this.E = (ImageButton) findViewById(C0000R.id.btn_record_sound);
        this.E.setOnClickListener(this);
        this.I = (ImageButton) findViewById(C0000R.id.btn_above_camera);
        this.J = (ImageButton) findViewById(C0000R.id.btn_above_gallery);
        this.K = (ImageButton) findViewById(C0000R.id.btn_above_smile);
        this.L = (ImageButton) findViewById(C0000R.id.btn_above_sound);
        this.I.setOnClickListener(this);
        this.J.setOnClickListener(this);
        this.K.setOnClickListener(this);
        this.L.setOnClickListener(this);
        a(getIntent());
        this.k = com.sd.a.a.a.a.d.a(this);
        this.an = new cn(this);
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.snda.youni.receive.RECEIVEWAP");
        registerReceiver(this.an, intentFilter);
    }

    public void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {
        MenuInflater menuInflater = getMenuInflater();
        com.snda.youni.modules.d.b d2 = this.s.d(Long.toString(this.q));
        if (this.ab != null) {
            if (d2 != null && d2.f().equalsIgnoreCase("5")) {
                new AlertDialog.Builder(this).setItems((int) C0000R.array.chat_failed_long_click, new bd(this, d2)).show();
            } else if (this.ab.n()) {
                switch (this.ab.j()) {
                    case 0:
                        menuInflater.inflate(C0000R.menu.message_context_menu_chat_attachment, contextMenu);
                        break;
                    case 1:
                    case 3:
                    case 5:
                        menuInflater.inflate(C0000R.menu.message_context_menu_chat_working_attachment, contextMenu);
                        break;
                    case 2:
                        menuInflater.inflate(C0000R.menu.message_context_menu_chat_failed_send_attachment, contextMenu);
                        break;
                    case 4:
                        menuInflater.inflate(C0000R.menu.message_context_menu_chat_failed_download_attachment, contextMenu);
                        break;
                    default:
                        menuInflater.inflate(C0000R.menu.message_context_menu_chat_success, contextMenu);
                        break;
                }
            } else {
                menuInflater.inflate(C0000R.menu.message_context_menu_chat_mms_success, contextMenu);
            }
            super.onCreateContextMenu(contextMenu, view, contextMenuInfo);
        }
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int i2) {
        switch (i2) {
            case 2:
                return new AlertDialog.Builder(this).setTitle((int) C0000R.string.operation_title).setMessage((int) C0000R.string.invite_message).setPositiveButton((int) C0000R.string.invite, new au(this)).setNegativeButton((int) C0000R.string.alert_dialog_cancel, new at(this)).create();
            case 3:
                String a2 = this.X.a();
                if (this.X.n()) {
                    a2 = getString(C0000R.string.snda_services_phone_number);
                }
                return new AlertDialog.Builder(this).setTitle((int) C0000R.string.operation_title).setMessage(getString(C0000R.string.dail_confirmation) + "\n" + a2 + "?").setPositiveButton((int) C0000R.string.alert_dialog_dail, new as(this, a2)).setNegativeButton((int) C0000R.string.alert_dialog_cancel, new ar(this)).create();
            case 4:
            default:
                return null;
            case 5:
                return new AlertDialog.Builder(this).setTitle((int) C0000R.string.inbox_delete_title).setMessage((int) C0000R.string.inbox_delete_item).setPositiveButton((int) C0000R.string.alert_dialog_ok, new ay(this)).setNegativeButton((int) C0000R.string.alert_dialog_cancel, new ax(this)).create();
            case 6:
                return new AlertDialog.Builder(this).setTitle((int) C0000R.string.inbox_delete_title).setMessage((int) C0000R.string.chat_delete_mutil_message).setPositiveButton((int) C0000R.string.alert_dialog_ok, new aw(this)).setNegativeButton((int) C0000R.string.alert_dialog_cancel, new av(this)).create();
            case 7:
                return new AlertDialog.Builder(this).setTitle((int) C0000R.string.menu_add_blacak_list).setIcon(17301543).setMessage(getString(C0000R.string.add_to_black_list_notification, new Object[]{this.t.f496a})).setNegativeButton(17039360, (DialogInterface.OnClickListener) null).setPositiveButton(17039370, new bh(this)).setCancelable(false).create();
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, 0, 0, (int) C0000R.string.menu_batch_operations).setIcon((int) C0000R.drawable.menu_batch_operations);
        menu.add(0, 2, 2, (int) C0000R.string.menu_my_favorite).setIcon((int) C0000R.drawable.menu_favorite);
        if (this.t != null && ((this.t.n != null && this.t.n.split(" ").length > 1) || getString(C0000R.string.Youni_robot_id).equals(this.t.g) || this.t.p != null)) {
            return true;
        }
        menu.add(0, 3, 3, (int) C0000R.string.menu_add_blacak_list).setIcon((int) C0000R.drawable.menu_black_list);
        return true;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        ((AppContext) getApplicationContext()).a((ChatActivity) null);
        this.s.d(this.m);
        s.e = 7;
        AppContext.b(this);
        unbindService(this.aa);
        this.n = null;
        if (this.o != null) {
            Cursor cursor = ((com.snda.youni.mms.ui.k) this.o).getCursor();
            if (cursor != null) {
                cursor.close();
            }
            ((com.snda.youni.mms.ui.k) this.o).a();
            this.o = null;
        }
        this.ae = null;
        this.ad = null;
        this.r.d();
        this.D.a();
        com.snda.youni.attachment.k.a().b();
        System.gc();
        if (this.an != null) {
            unregisterReceiver(this.an);
        }
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 == 4) {
            if (this.T) {
                e(false);
                return true;
            } else if (YouNi.b == null) {
                startActivity(new Intent(this, YouNi.class));
                finish();
            }
        }
        return super.onKeyDown(i2, keyEvent);
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        "chat activity new intent@" + intent;
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case 0:
                e(true);
                break;
            case 2:
                startActivity(new Intent(this, FavoriteActivity.class));
                break;
            case 3:
                removeDialog(7);
                showDialog(7);
                break;
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        ((AppContext) getApplicationContext()).a(false);
        n.a(this).a();
        s.e = 1;
        this.D.b();
        com.snda.youni.modules.chat.b.a("-1");
        this.D.b();
        if (this.V != null) {
            this.V.dismiss();
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        ((AppContext) getApplicationContext()).a(true);
        n.a(this).a();
        if (this.m > 0) {
            com.snda.youni.modules.chat.b.a(Long.toString(this.m));
            this.s.d(this.m);
            if (this.o == null) {
                this.o = new com.snda.youni.mms.ui.k(this, this.s.a(this.m), this.n, this.X.r());
                if (!(this.o == null || this.n == null)) {
                    this.n.setAdapter(this.o);
                    ((com.snda.youni.mms.ui.k) this.o).a(this.as);
                }
            }
        }
        s.e = 0;
        this.D.c();
        this.r.requestFocus();
        ((AppContext) getApplicationContext()).a(this);
        com.snda.youni.modules.chat.b.a(this.m + "");
        "notification chat activity resume@" + this;
        "cancelNotification:" + this.m;
        ((AppContext) getApplicationContext()).c(this.m + "");
        com.snda.youni.modules.d.b b2 = this.s.b(this.X.a(), this.m + "");
        if (!(b2 == null || b2.b() == null)) {
            b2.toString();
            if (this.r != null) {
                this.r.a(b2.b());
                this.r.a(this.r.g());
                this.v = b2.b();
                this.X.b(b2.l());
                "mLastDraftText:" + this.v;
            }
        }
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            "notification onResume@bundle=" + extras + ",id=" + extras.getInt("notificationId");
        }
        if (!(extras == null || extras.getString("source") == null)) {
            "clear notification id=" + extras.getInt("notificationId");
        }
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) getSystemService("connectivity")).getActiveNetworkInfo();
        if (activeNetworkInfo == null || !activeNetworkInfo.isConnected()) {
            this.P = false;
        } else {
            if (1 == this.N) {
                this.Q = true;
            }
            this.P = true;
        }
        if (this.X.n()) {
            this.Q = true;
        }
        if (!this.Q) {
            com.snda.youni.c.t.a(new com.snda.youni.c.j(this.X.a()), new bg(this), this);
        } else if (Settings.System.getInt(getApplicationContext().getContentResolver(), "airplane_mode_on", 0) != 0 && (!this.P || !this.Q)) {
            WarningTipView warningTipView = (WarningTipView) findViewById(C0000R.id.chat_warning);
            warningTipView.a(getString(C0000R.string.air_mode));
            warningTipView.setVisibility(0);
            warningTipView.a();
        }
        if (this.Q) {
            com.snda.youni.modules.chat.b.a(true);
        } else {
            com.snda.youni.modules.chat.b.a(false);
        }
        g();
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putLong("mMessageLongClickedIndex", this.q);
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        s.e = 1;
        AppContext.a((Activity) this);
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        if (this.o != null) {
            ((com.snda.youni.mms.ui.k) this.o).a();
        }
        "inputView.text=" + ((Object) this.r.a()) + ", mLastDraftText=" + this.v + " isGroup=" + this.X.r();
        s.e = 3;
        if (this.r != null && this.r.a() != null && !this.X.r()) {
            String obj = this.r.a().toString();
            this.i = obj;
            String c2 = this.X.c();
            if (obj != null) {
                com.snda.youni.modules.d.b bVar = new com.snda.youni.modules.d.b();
                bVar.b(obj);
                bVar.b(true);
                bVar.a(Long.valueOf(System.currentTimeMillis()));
                bVar.e("sms");
                bVar.a(this.X.a());
                bVar.c(this.X.c());
                bVar.a(this.m);
                if ("".equalsIgnoreCase(obj) && c2 == null && "".equalsIgnoreCase(c2)) {
                    a(bVar);
                } else if (obj.length() > 0) {
                    if (this.v == null || !obj.equalsIgnoreCase(this.v)) {
                        a(bVar);
                    }
                } else if (obj.length() == 0 && this.v != null) {
                    a(bVar);
                }
            }
        }
    }

    public void startActivityForResult(Intent intent, int i2) {
        if (i2 >= 0) {
            this.c = true;
        }
        super.startActivityForResult(intent, i2);
    }
}
