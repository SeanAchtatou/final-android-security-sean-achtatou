package com.snda.youni.activities;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;
import com.snda.youni.b.ai;
import com.snda.youni.services.YouniService;

final class ck implements ServiceConnection {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ MessageActivityTest f261a;

    ck(MessageActivityTest messageActivityTest) {
        this.f261a = messageActivityTest;
    }

    public final void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        ai unused = this.f261a.b = YouniService.b();
        "on Service Connected network:" + this.f261a.b;
    }

    public final void onServiceDisconnected(ComponentName componentName) {
        ai unused = this.f261a.b = null;
    }
}
