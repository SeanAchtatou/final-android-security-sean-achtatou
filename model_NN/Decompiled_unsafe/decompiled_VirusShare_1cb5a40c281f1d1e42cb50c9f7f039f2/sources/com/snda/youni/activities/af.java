package com.snda.youni.activities;

import android.view.View;
import android.widget.Toast;
import com.snda.youni.C0000R;
import com.snda.youni.e.o;

final class af implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ bw f202a;

    af(bw bwVar) {
        this.f202a = bwVar;
    }

    public final void onClick(View view) {
        if (!o.a()) {
            Toast.makeText(this.f202a.getContext(), (int) C0000R.string.sdcard_not_working, 1).show();
            this.f202a.cancel();
        }
        this.f202a.a();
    }
}
