package com.snda.youni.activities;

import android.os.AsyncTask;
import com.snda.youni.g.a;
import com.snda.youni.modules.d.b;
import com.snda.youni.services.YouniService;
import java.util.List;

final class cv extends AsyncTask {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ RecipientsActivity f271a;

    /* synthetic */ cv(RecipientsActivity recipientsActivity) {
        this(recipientsActivity, (byte) 0);
    }

    private cv(RecipientsActivity recipientsActivity, byte b) {
        this.f271a = recipientsActivity;
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public Void doInBackground(List... listArr) {
        List<b> list = listArr[0];
        if (list != null && list.size() > 1) {
            YouniService.a(list.size());
            a.a(this.f271a.getApplicationContext(), "group_message", null);
        }
        for (b bVar : list) {
            this.f271a.a(true);
            this.f271a.j.b(this.f271a.F, bVar);
            if (bVar != null) {
                this.f271a.j.a((String) null);
            }
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
            }
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void onPostExecute(Object obj) {
        Void voidR = (Void) obj;
        try {
            if (this.f271a.o != null) {
                this.f271a.o.dismiss();
            }
        } catch (Exception e) {
            e.getMessage();
        }
        super.onPostExecute(voidR);
    }
}
