package com.snda.youni.activities;

import android.text.Editable;
import android.text.TextWatcher;
import com.snda.youni.providers.n;

final class e implements TextWatcher {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ NewChatActivity f283a;

    e(NewChatActivity newChatActivity) {
        this.f283a = newChatActivity;
    }

    public final void afterTextChanged(Editable editable) {
        String[] split = editable.toString().split(",");
        if (split != null && split.length != 0) {
            String replaceAll = split[split.length - 1].replaceAll("'", "");
            "Search String is: " + replaceAll;
            this.f283a.C.startQuery(0, null, n.f597a, NewChatActivity.z, "search_name LIKE '" + replaceAll + "%'" + " OR " + "pinyin_name LIKE '" + replaceAll + "%'" + " OR " + "display_name LIKE '" + replaceAll + "%'" + " OR " + "phone_number LIKE '" + replaceAll + "%'", null, "times_contacted DESC");
            this.f283a.l();
        }
    }

    public final void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    public final void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }
}
