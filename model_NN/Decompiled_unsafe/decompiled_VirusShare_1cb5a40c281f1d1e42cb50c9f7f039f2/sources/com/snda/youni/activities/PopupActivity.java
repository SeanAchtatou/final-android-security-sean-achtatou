package com.snda.youni.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import com.snda.youni.AppContext;
import com.snda.youni.C0000R;
import com.snda.youni.al;
import com.snda.youni.e.r;
import com.snda.youni.e.t;
import com.snda.youni.h.a.a;
import com.snda.youni.modules.a.k;
import com.snda.youni.modules.popup.c;
import com.snda.youni.modules.popup.d;
import com.snda.youni.modules.popup.e;
import com.snda.youni.modules.popup.f;

public class PopupActivity extends Activity implements al, d {

    /* renamed from: a  reason: collision with root package name */
    private static PopupActivity f189a;
    private static int e = 0;
    private c b;
    private com.snda.youni.d c;
    private e d;

    private void a(Intent intent) {
        if (intent.hasExtra("close") && intent.getBooleanExtra("close", false)) {
            finish();
        }
    }

    private void g() {
        "remove: " + String.valueOf(e);
        if (f.f559a.size() > 0 && f.f559a.size() > e) {
            f.f559a.remove(e);
            int size = f.f559a.size();
            if (size == 0) {
                finish();
                return;
            }
            if (e >= size) {
                e = (e + size) % size;
            }
            h();
        }
    }

    /* access modifiers changed from: private */
    public void h() {
        if (f.f559a.size() <= 0) {
            finish();
            return;
        }
        this.b.a(e, f.f559a.size());
        k kVar = (k) f.f559a.get(e);
        this.b.a(kVar, this.c);
        this.d.a(kVar);
    }

    private void i() {
        String str = ((k) f.f559a.get(e)).k;
        long longValue = Long.valueOf(((k) f.f559a.get(e)).c).longValue();
        ((AppContext) getApplicationContext()).a(str, longValue);
        new a(this).a(str, longValue);
    }

    public final void a() {
        i();
        finish();
    }

    public final void a(String str) {
        com.snda.youni.g.a.a(getApplicationContext(), "popup_reply", null);
        i();
        k kVar = (k) f.f559a.get(e);
        if (this.d.a(kVar.g, str, kVar.f ? 1 : 0)) {
            g();
        }
    }

    public final void a(boolean z) {
        "flip: to right: " + String.valueOf(z);
        int size = f.f559a.size();
        if (size <= 1) {
            return;
        }
        if (z && e == 0) {
            return;
        }
        if (z || e != size - 1) {
            i();
            if (!z) {
                e = (e + 1) % size;
            } else {
                e = ((e - 1) + size) % size;
            }
            this.b.a(this, (k) f.f559a.get(e), e, size, this.c, z);
            com.snda.youni.g.a.a(getApplicationContext(), "popup_slide", null);
        }
    }

    public final void b() {
        if (f.f559a.size() > 0) {
            com.snda.youni.g.a.a(getApplicationContext(), "popup_goin", null);
            Intent intent = new Intent(this, ChatActivity.class);
            intent.putExtra("item", (k) f.f559a.get(e));
            startActivity(intent);
            f.f559a.clear();
            finish();
        }
    }

    public final void c() {
        k kVar;
        String str;
        if (f.f559a.size() > 0 && (str = (kVar = (k) f.f559a.get(e)).g) != null) {
            if (t.b(str)) {
                str = getString(C0000R.string.snda_services_phone_number);
            }
            if (kVar.g != null) {
                r.a(str, this);
            }
        }
    }

    public final void d() {
        i();
        if (f.f559a.size() > 0) {
            k kVar = (k) f.f559a.get(e);
            "delete: " + kVar.c;
            new a(this).c(kVar.c);
            g();
        }
    }

    public final void e() {
        if (e >= 0 && e <= f.f559a.size() - 1) {
            this.d.a(((k) f.f559a.get(e)).g);
        }
    }

    public final void f() {
        runOnUiThread(new cw(this));
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        getWindow().setSoftInputMode(18);
        a(getIntent());
        requestWindowFeature(1);
        setContentView((int) C0000R.layout.view_popup);
        k kVar = (k) getIntent().getSerializableExtra("item");
        if (kVar == null) {
            kVar = null;
        }
        if (kVar != null) {
            this.d = new e(this);
            this.b = new c(this);
            AppContext.a((al) this);
            this.b.a(this);
            this.c = new com.snda.youni.d(getApplicationContext());
            f189a = this;
            h();
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.c != null) {
            this.c.a();
        }
        AppContext.a((al) null);
        if (this.d != null) {
            this.d.a();
        }
        f.f559a.clear();
        e = 0;
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        a(intent);
        super.onNewIntent(intent);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        if (this.c != null) {
            this.c.b();
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (AppContext.c()) {
            AppContext.d();
        }
        if (this.c != null) {
            this.c.c();
        }
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        this.b.a();
    }
}
