package com.snda.youni.activities;

import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import com.snda.youni.C0000R;

final class cl implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ MessageActivityTest f262a;

    cl(MessageActivityTest messageActivityTest) {
        this.f262a = messageActivityTest;
    }

    public final void onClick(View view) {
        String obj = ((EditText) this.f262a.findViewById(C0000R.id.txt_number)).getText().toString();
        Toast.makeText(this.f262a, "number:" + obj + " has " + this.f262a.f186a.c(this.f262a.f186a.e(obj)) + " sms messages!", 1).show();
    }
}
