package com.snda.youni.activities;

import android.os.AsyncTask;
import android.widget.Toast;
import com.snda.youni.C0000R;
import com.snda.youni.a.a;
import java.io.File;
import java.io.IOException;

final class cd extends AsyncTask {

    /* renamed from: a  reason: collision with root package name */
    private File f254a;
    private /* synthetic */ SettingsSkinActivity b;

    /* synthetic */ cd(SettingsSkinActivity settingsSkinActivity) {
        this(settingsSkinActivity, (byte) 0);
    }

    private cd(SettingsSkinActivity settingsSkinActivity, byte b2) {
        this.b = settingsSkinActivity;
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public Integer doInBackground(String... strArr) {
        try {
            this.f254a = this.b.b(strArr[0]);
            return 0;
        } catch (a e) {
            e.printStackTrace();
            return 1;
        } catch (IOException e2) {
            e2.printStackTrace();
            return 2;
        }
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void onPostExecute(Object obj) {
        Integer num = (Integer) obj;
        "Download finish: " + String.valueOf(num);
        if (num.intValue() == 0) {
            SettingsSkinActivity.b(this.b);
        }
        if (num.intValue() == 1) {
            Toast.makeText(this.b, this.b.getString(C0000R.string.update_network_error), 0).show();
            SettingsSkinActivity.e();
        }
    }
}
