package com.snda.youni.activities;

import android.content.ContentUris;
import android.content.res.Resources;
import android.widget.TextView;
import android.widget.Toast;
import com.sd.a.a.a.a.i;
import com.sd.a.a.a.b;
import com.sd.android.mms.c;
import com.snda.youni.C0000R;
import com.snda.youni.mms.ui.af;
import com.snda.youni.mms.ui.m;

final class d implements af {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ NewChatActivity f276a;

    d(NewChatActivity newChatActivity) {
        this.f276a = newChatActivity;
    }

    public final void a(i iVar) {
        NewChatActivity newChatActivity = this.f276a;
        Resources resources = newChatActivity.getResources();
        if (iVar == null) {
            Toast.makeText(newChatActivity, resources.getString(C0000R.string.failed_to_add_media, this.f276a.c((int) C0000R.string.type_picture)), 0).show();
            return;
        }
        this.f276a.d(true);
        try {
            this.f276a.r.a(this.f276a.s.a(iVar, ContentUris.parseId(this.f276a.o)));
            this.f276a.r.a(this.f276a.n, 1);
            ((TextView) this.f276a.findViewById(C0000R.id.attachment_info)).setText(this.f276a.getString(C0000R.string.attachment_preview_info, new Object[]{this.f276a.x.l(), Integer.valueOf(this.f276a.x.o()), Integer.valueOf(this.f276a.x.p())}));
        } catch (b e) {
            Toast.makeText(newChatActivity, resources.getString(C0000R.string.failed_to_add_media, this.f276a.c((int) C0000R.string.type_picture)), 0).show();
        } catch (com.sd.android.mms.b e2) {
            m.a(newChatActivity, resources.getString(C0000R.string.unsupported_media_format, this.f276a.c((int) C0000R.string.type_picture)), resources.getString(C0000R.string.select_different_media, this.f276a.c((int) C0000R.string.type_picture)));
        } catch (c e3) {
            m.a(newChatActivity, resources.getString(C0000R.string.failed_to_resize_image), resources.getString(C0000R.string.resize_image_error_information));
        } catch (com.sd.android.mms.d e4) {
            m.a(newChatActivity, resources.getString(C0000R.string.exceed_message_size_limitation), resources.getString(C0000R.string.failed_to_add_media, this.f276a.c((int) C0000R.string.type_picture)));
        }
    }
}
