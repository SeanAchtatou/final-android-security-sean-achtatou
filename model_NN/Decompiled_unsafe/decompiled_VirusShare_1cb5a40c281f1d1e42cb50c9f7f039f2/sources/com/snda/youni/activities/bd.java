package com.snda.youni.activities;

import android.content.DialogInterface;
import android.widget.CursorAdapter;
import com.snda.youni.modules.d.b;

final class bd implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ b f227a;
    private /* synthetic */ ChatActivity b;

    bd(ChatActivity chatActivity, b bVar) {
        this.b = chatActivity;
        this.f227a = bVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.snda.youni.activities.ChatActivity.a(com.snda.youni.activities.ChatActivity, long):long
     arg types: [com.snda.youni.activities.ChatActivity, int]
     candidates:
      com.snda.youni.activities.ChatActivity.a(com.snda.youni.activities.ChatActivity, java.lang.String[]):long
      com.snda.youni.activities.ChatActivity.a(int, java.lang.String):com.snda.youni.attachment.b.b
      com.snda.youni.activities.ChatActivity.a(com.snda.youni.activities.ChatActivity, android.net.Uri):com.snda.youni.attachment.b.b
      com.snda.youni.activities.ChatActivity.a(com.snda.youni.activities.ChatActivity, com.snda.youni.attachment.b.b):com.snda.youni.attachment.b.b
      com.snda.youni.activities.ChatActivity.a(com.snda.youni.activities.ChatActivity, com.snda.youni.b.ai):com.snda.youni.b.ai
      com.snda.youni.activities.ChatActivity.a(com.snda.youni.activities.ChatActivity, com.snda.youni.mms.ui.a):com.snda.youni.mms.ui.a
      com.snda.youni.activities.ChatActivity.a(com.snda.youni.activities.ChatActivity, int):void
      com.snda.youni.activities.ChatActivity.a(com.snda.youni.activities.ChatActivity, java.lang.String):void
      com.snda.youni.activities.ChatActivity.a(com.snda.youni.activities.ChatActivity, boolean):void
      com.snda.youni.activities.ChatActivity.a(com.snda.youni.activities.ChatActivity, long):long */
    public final void onClick(DialogInterface dialogInterface, int i) {
        if (i == 0) {
            this.f227a.b(0);
            this.f227a.d("4");
            this.f227a.a(Long.valueOf(System.currentTimeMillis()));
            this.b.s.b(this.b.Z, this.f227a);
            ((CursorAdapter) this.b.o).notifyDataSetChanged();
        } else if (i == 1) {
            "delete one number:" + this.b.s.c(Long.toString(this.b.q));
            long unused = this.b.q = -1L;
        }
    }
}
