package com.snda.youni.activities;

import android.content.DialogInterface;

final class y implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ SettingsSoundActivity f303a;

    y(SettingsSoundActivity settingsSoundActivity) {
        this.f303a = settingsSoundActivity;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.f303a.b.edit().putInt("sound_youni_name", this.f303a.g).commit();
        this.f303a.b.edit().putInt("sound_type_name", 0).commit();
        this.f303a.c();
    }
}
