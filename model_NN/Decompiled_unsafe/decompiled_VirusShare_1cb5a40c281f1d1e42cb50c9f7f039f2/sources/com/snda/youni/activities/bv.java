package com.snda.youni.activities;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;
import com.snda.youni.b.ai;
import com.snda.youni.services.YouniService;

final class bv implements ServiceConnection {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ RecipientsActivity f245a;

    bv(RecipientsActivity recipientsActivity) {
        this.f245a = recipientsActivity;
    }

    public final void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        ai unused = this.f245a.F = YouniService.b();
        "onServiceConnected, network.isConnected=" + this.f245a.F.b();
    }

    public final void onServiceDisconnected(ComponentName componentName) {
        ai unused = this.f245a.F = (ai) null;
    }
}
