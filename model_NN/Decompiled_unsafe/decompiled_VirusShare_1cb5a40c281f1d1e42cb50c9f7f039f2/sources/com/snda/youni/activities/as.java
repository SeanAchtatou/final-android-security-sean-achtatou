package com.snda.youni.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;

final class as implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ String f215a;
    private /* synthetic */ ChatActivity b;

    as(ChatActivity chatActivity, String str) {
        this.b = chatActivity;
        this.f215a = str;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.b.startActivity(new Intent("android.intent.action.CALL", Uri.parse("tel:" + this.f215a)));
        dialogInterface.cancel();
    }
}
