package com.snda.youni.activities;

import android.content.AsyncQueryHandler;
import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import android.text.TextUtils;
import com.snda.youni.am;

final class ap extends AsyncQueryHandler {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ RecipientsActivity f212a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ap(RecipientsActivity recipientsActivity, ContentResolver contentResolver) {
        super(contentResolver);
        this.f212a = recipientsActivity;
    }

    /* access modifiers changed from: protected */
    public final void onQueryComplete(int i, Object obj, Cursor cursor) {
        if (cursor != null) {
            "cursor's size is: " + cursor.getCount();
        }
        if (TextUtils.isEmpty(this.f212a.b)) {
            this.f212a.i.a(true);
        } else {
            this.f212a.i.a(false);
        }
        am.a(this.f212a.F, false);
        this.f212a.i.changeCursor(cursor);
    }

    public final void startQuery(int i, Object obj, Uri uri, String[] strArr, String str, String[] strArr2, String str2) {
        "startQuery, token=" + i + ", orderBy=" + "expand_data2 DESC, expand_data1 DESC, pinyin_name ASC";
        super.startQuery(i, obj, uri, strArr, str, strArr2, "expand_data2 DESC, expand_data1 DESC, pinyin_name ASC");
    }
}
