package com.snda.youni.activities;

import android.app.Activity;
import android.os.Bundle;
import com.snda.youni.AppContext;
import com.snda.youni.C0000R;
import com.snda.youni.e.s;

public class PrivacyActivity extends Activity {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        setContentView((int) C0000R.layout.activity_privacy);
        findViewById(C0000R.id.ok).setOnClickListener(new ak(this));
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        s.l = 7;
        AppContext.b(this);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        s.l = 1;
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        s.l = 0;
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        s.h = 1;
        AppContext.a(this);
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        s.h = 3;
    }
}
