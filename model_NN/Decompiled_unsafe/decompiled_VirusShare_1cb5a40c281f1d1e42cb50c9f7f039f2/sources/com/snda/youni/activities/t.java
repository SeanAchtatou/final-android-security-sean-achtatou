package com.snda.youni.activities;

import android.os.CountDownTimer;
import android.widget.TextView;
import com.snda.youni.C0000R;

final class t extends CountDownTimer {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ cr f298a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    t(cr crVar) {
        super(60000, 200);
        this.f298a = crVar;
    }

    public final void onFinish() {
        ((TextView) this.f298a.b.findViewById(C0000R.id.record_sound_timer2)).setText((int) C0000R.string.audio_record_finish);
        try {
            this.f298a.b.findViewById(C0000R.id.volume_level_indicator2).scrollTo(0, -160);
            int unused = this.f298a.e = 60;
            this.f298a.d.c();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public final void onTick(long j) {
        int unused = this.f298a.e = (60000 - ((int) j)) / 1000;
        int b = this.f298a.d.b() / 100;
        "current volume level is " + b;
        ((TextView) this.f298a.b.findViewById(C0000R.id.record_sound_timer2)).setText(this.f298a.f267a.getString(C0000R.string.record_time_remaining, Long.valueOf(j / 1000)));
        int i = -(300 - b);
        if (i > 0) {
            i = 0;
        }
        this.f298a.b.findViewById(C0000R.id.volume_level_indicator2).scrollTo(0, i);
    }
}
