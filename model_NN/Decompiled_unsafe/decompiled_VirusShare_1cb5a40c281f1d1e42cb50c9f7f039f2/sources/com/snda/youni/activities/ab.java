package com.snda.youni.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import com.snda.youni.e.m;
import java.util.List;

final class ab implements AdapterView.OnItemClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ List f198a;
    private /* synthetic */ bw b;

    ab(bw bwVar, List list) {
        this.b = bwVar;
        this.f198a = list;
    }

    public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
        "item click@" + i;
        if (i < this.f198a.size()) {
            Bundle bundle = new Bundle();
            bundle.putString("smiletext", m.a().a(i));
            this.b.e.setData(bundle);
            this.b.dismiss();
        }
    }
}
