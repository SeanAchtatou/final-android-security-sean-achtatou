package com.snda.youni.activities;

import android.a.o;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.text.InputFilter;
import android.text.TextUtils;
import android.view.View;
import android.view.animation.TranslateAnimation;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.sd.a.a.a.a.d;
import com.sd.a.a.a.a.j;
import com.sd.a.a.a.a.w;
import com.sd.android.mms.a.e;
import com.sd.android.mms.a.h;
import com.sd.android.mms.a.p;
import com.sd.android.mms.c;
import com.sd.android.mms.transaction.n;
import com.snda.youni.AppContext;
import com.snda.youni.C0000R;
import com.snda.youni.attachment.b.b;
import com.snda.youni.attachment.g;
import com.snda.youni.b.ai;
import com.snda.youni.e.m;
import com.snda.youni.e.q;
import com.snda.youni.e.s;
import com.snda.youni.e.t;
import com.snda.youni.mms.ui.af;
import com.snda.youni.mms.ui.f;
import com.snda.youni.mms.ui.l;
import com.snda.youni.modules.InputView;
import com.snda.youni.modules.WarningTipView;
import com.snda.youni.modules.a;
import com.snda.youni.modules.a.k;
import com.snda.youni.services.YouniService;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class NewChatActivity extends Activity implements AdapterView.OnItemClickListener, l, com.snda.youni.modules.l {
    /* access modifiers changed from: private */
    public static final String[] z = {"_id", "contact_id", "display_name", "phone_number", "contact_type"};
    /* access modifiers changed from: private */
    public ai A = null;
    private ServiceConnection B = new f(this);
    /* access modifiers changed from: private */
    public df C;
    private final Handler D = new a(this);
    private final af E = new d(this);
    private final Handler F = new c(this);

    /* renamed from: a  reason: collision with root package name */
    a f188a;
    Uri b;
    /* access modifiers changed from: private */
    public EditText c;
    /* access modifiers changed from: private */
    public InputView d;
    /* access modifiers changed from: private */
    public com.snda.youni.h.a.a e;
    private String f;
    private String g;
    private int h;
    private boolean i = true;
    private boolean j = false;
    private boolean k = false;
    /* access modifiers changed from: private */
    public ProgressDialog l;
    private int m = 0;
    /* access modifiers changed from: private */
    public com.sd.android.mms.a.a n;
    /* access modifiers changed from: private */
    public Uri o;
    private EditText p;
    private String q;
    /* access modifiers changed from: private */
    public f r;
    /* access modifiers changed from: private */
    public d s;
    private CharSequence t;
    private int u;
    private long v;
    private boolean w = false;
    /* access modifiers changed from: private */
    public b x;
    private com.snda.youni.modules.d.b y;

    private long a(String[] strArr) {
        HashSet hashSet = new HashSet();
        hashSet.addAll(Arrays.asList(strArr));
        return o.a(this, hashSet);
    }

    private static com.sd.android.mms.a.a a(Context context) {
        com.sd.android.mms.a.a a2 = com.sd.android.mms.a.a.a(context);
        h hVar = new h(a2, (byte) 0);
        hVar.add((e) new p(context, "text/plain", "text_0.txt", a2.c().b()));
        a2.add(hVar);
        return a2;
    }

    private b a(int i2, String str) {
        try {
            return com.snda.youni.attachment.c.a.a(this, i2, str);
        } catch (g e2) {
            e2.printStackTrace();
            return null;
        }
    }

    private b a(Bitmap bitmap) {
        try {
            return com.snda.youni.attachment.c.a.a(this, bitmap);
        } catch (g e2) {
            return null;
        }
    }

    private b a(Uri uri) {
        try {
            return com.snda.youni.attachment.c.a.a(this, uri);
        } catch (g e2) {
            e2.printStackTrace();
            return null;
        }
    }

    private List a(String[] strArr, boolean z2, String str, long j2) {
        String str2;
        ArrayList arrayList = new ArrayList();
        int i2 = 0;
        while (i2 < strArr.length) {
            com.snda.youni.modules.d.b bVar = new com.snda.youni.modules.d.b();
            bVar.d(z2);
            if (strArr[i2] != null) {
                String[] split = strArr[i2].split(";");
                if (split.length > 0) {
                    strArr[i2] = split[0];
                }
            }
            WarningTipView warningTipView = (WarningTipView) findViewById(C0000R.id.chat_warning);
            if (strArr[i2] == null || g() == null || !strArr[i2].contains(g())) {
                warningTipView.setVisibility(8);
                String a2 = q.a(t.a(strArr[i2]));
                "address:" + a2 + " body:" + str;
                bVar.a(a2);
                bVar.a(j2);
                int indexOf = str.indexOf(m.a().a(20));
                if (indexOf >= 0) {
                    String string = PreferenceManager.getDefaultSharedPreferences(this).getString("postion", "");
                    if ("".equalsIgnoreCase(string)) {
                        Toast.makeText(this, (int) C0000R.string.emotion_location_failed, 1).show();
                        str2 = str;
                    } else if (!str.contains("[http://maps.google.com/maps")) {
                        str2 = str.substring(0, indexOf) + string + str.substring(indexOf);
                    }
                    bVar.b(str2);
                    bVar.a(Long.valueOf(System.currentTimeMillis()));
                    bVar.e("youni");
                    arrayList.add(bVar);
                    i2++;
                }
                str2 = str;
                bVar.b(str2);
                bVar.a(Long.valueOf(System.currentTimeMillis()));
                bVar.e("youni");
                arrayList.add(bVar);
                i2++;
            } else {
                warningTipView.a(getString(C0000R.string.not_send_to_self));
                warningTipView.setVisibility(0);
                warningTipView.a(0);
                warningTipView.a();
                return null;
            }
        }
        return arrayList;
    }

    private void a(Intent intent) {
        k kVar;
        if (intent != null && intent.getExtras() != null && (kVar = (k) intent.getExtras().get("item")) != null) {
            this.f = kVar.g;
            if (!TextUtils.isEmpty(this.f)) {
                if (this.f != null && this.f.split(",").length > 1) {
                    "isGroup:" + this.j;
                    this.j = true;
                }
                this.c.setText(this.f);
            }
            if (!TextUtils.isEmpty(kVar.b)) {
                this.d.a(kVar.b);
            }
        }
    }

    private void a(com.sd.a.a.a.b bVar) {
        "add image failed " + bVar;
        Toast.makeText(this, b(C0000R.string.failed_to_add_media, c((int) C0000R.string.type_picture)), 0).show();
    }

    private void a(com.sd.android.mms.a.a aVar) {
        if (aVar.size() == 0) {
            aVar.add(new h(aVar, (byte) 0));
        }
        if (!aVar.get(0).e()) {
            aVar.get(0).add((e) new p(this, "text/plain", "text_0.txt", aVar.c().b()));
        }
    }

    static /* synthetic */ void a(NewChatActivity newChatActivity, int i2) {
        switch (i2) {
            case 0:
                com.snda.youni.mms.ui.m.c(newChatActivity);
                return;
            case 1:
                newChatActivity.startActivityForResult(new Intent("android.media.action.IMAGE_CAPTURE"), 11);
                return;
            case 2:
            case 3:
            default:
                return;
            case 4:
                com.snda.youni.mms.ui.m.a(newChatActivity);
                return;
            case 5:
                com.snda.youni.mms.ui.m.b(newChatActivity);
                return;
        }
    }

    static /* synthetic */ void a(NewChatActivity newChatActivity, String[] strArr, Uri uri, d dVar, com.sd.android.mms.a.a aVar, j jVar) {
        long a2 = newChatActivity.a(strArr);
        dVar.a(uri, jVar);
        com.sd.a.a.a.a.e a3 = aVar.a();
        try {
            dVar.a(uri, a3);
        } catch (com.sd.a.a.a.b e2) {
            "updateTemporaryMmsMessage: cannot update message " + uri;
        }
        aVar.a(a3);
        com.sd.a.a.a.b.b.a(newChatActivity, newChatActivity.getContentResolver(), ContentUris.withAppendedId(android.a.a.f5a, a2), "type=3");
        try {
            new n(newChatActivity, uri).a(a2);
        } catch (Exception e3) {
            "Failed to send message: " + uri + ", threadId=" + a2 + " " + e3;
        }
    }

    /* access modifiers changed from: private */
    public void a(boolean z2) {
        SharedPreferences.Editor edit = PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit();
        edit.putBoolean("chat_switch_youni", z2);
        edit.commit();
    }

    private String[] a(j jVar) {
        w[] wVarArr;
        if (this.f == null) {
            this.f = "111111";
        }
        String[] strArr = {this.f};
        int length = strArr.length;
        if (length > 0) {
            w[] wVarArr2 = new w[length];
            for (int i2 = 0; i2 < length; i2++) {
                wVarArr2[i2] = new w(strArr[i2]);
            }
            wVarArr = wVarArr2;
        } else {
            wVarArr = null;
        }
        if (wVarArr != null) {
            jVar.a(wVarArr);
        }
        if (this.q != null) {
            jVar.b(new w(this.q));
        }
        jVar.b(System.currentTimeMillis() / 1000);
        return strArr;
    }

    private String b(int i2, String str) {
        return getResources().getString(i2, str);
    }

    private void b(Uri uri) {
        try {
            this.r.a(uri);
            this.r.a(this.n, 1);
            ((TextView) findViewById(C0000R.id.attachment_info)).setText(getString(C0000R.string.attachment_preview_info, new Object[]{this.x.l(), Integer.valueOf(this.x.o()), Integer.valueOf(this.x.p())}));
        } catch (com.sd.a.a.a.b e2) {
            a(e2);
        } catch (com.sd.android.mms.b e3) {
            com.snda.youni.mms.ui.m.a(this, b(C0000R.string.unsupported_media_format, c((int) C0000R.string.type_picture)), b(C0000R.string.select_different_media, c((int) C0000R.string.type_picture)));
        } catch (c e4) {
            com.snda.youni.mms.ui.m.a(this, uri, this.D, this.E);
        } catch (com.sd.android.mms.d e5) {
            com.snda.youni.mms.ui.m.a(this, c((int) C0000R.string.exceed_message_size_limitation), b(C0000R.string.failed_to_add_media, c((int) C0000R.string.type_picture)));
        }
    }

    private void b(boolean z2) {
        if (z2) {
            try {
                if (this.o != null) {
                    this.o = this.s.a(this.o, android.a.j.f12a);
                    this.n = com.sd.android.mms.a.a.a(this, this.o);
                } else {
                    this.n = com.sd.android.mms.a.a.a(this, ((com.sd.a.a.a.a.l) this.s.a(ContentUris.withAppendedId(android.a.d.f7a, this.v))).i());
                    this.n = com.sd.android.mms.a.a.a(this, this.n.b(this));
                    this.u = com.snda.youni.mms.ui.m.a(this.n);
                    this.o = j();
                }
                this.r = new f(this, this.D, findViewById(C0000R.id.attachment_editor));
                this.r.a(this);
                if (!this.k) {
                    this.d.a(this.q);
                }
                int a2 = com.snda.youni.mms.ui.m.a(this.n);
                if (a2 == -1) {
                    a2 = 0;
                }
                if (this.n != null) {
                    a(this.n);
                    this.r.a(this.n, a2);
                }
                if (a2 > 0) {
                    c(true);
                }
            } catch (com.sd.a.a.a.b e2) {
                e2.getMessage() + " " + e2;
                finish();
            }
        } else {
            k();
        }
    }

    /* access modifiers changed from: private */
    public String c(int i2) {
        return getResources().getString(i2);
    }

    private void c(boolean z2) {
        if (z2) {
            this.m |= 4;
        } else {
            this.m &= -5;
        }
    }

    /* access modifiers changed from: private */
    public void d(boolean z2) {
        boolean z3;
        int i2 = this.m;
        c(z2);
        if (i2 == 0 && this.m != 0) {
            z3 = true;
        } else if (i2 != 0 && this.m == 0) {
            z3 = false;
        } else {
            return;
        }
        if (com.sd.android.mms.a.a() || !z3) {
            b(z3);
            return;
        }
        throw new IllegalStateException("Message converted to MMS with DISABLE_MMS set");
    }

    private String g() {
        return PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getString("self_phone_number", null);
    }

    private boolean h() {
        this.f = this.c.getText().toString();
        String[] split = this.f.split(",");
        if (split != null && split.length > 1) {
            this.j = true;
        }
        for (int i2 = 0; i2 < split.length; i2++) {
            Matcher matcher = Pattern.compile("<(.+)>").matcher(split[i2]);
            if (matcher.find()) {
                split[i2] = matcher.group(1);
            }
            "btn_send onclick, matcher addresses[i]=" + split[i2];
            if (!t.b(split[i2])) {
                split[i2] = q.stripSeparators(split[i2]);
            }
            "btn_send onclick, stripSeparators addresses[i]=" + split[i2];
            if (!q.isWellFormedSmsAddress(split[i2]) && !t.b(split[i2])) {
                return false;
            }
        }
        this.f = split[0];
        String obj = this.d.a().toString();
        if (!this.k) {
            return false;
        }
        this.y = new com.snda.youni.modules.d.b();
        this.y.a(this.f);
        this.y.a(split);
        this.y.b(obj);
        this.y.b(true);
        this.y.d(this.j);
        this.y.a(Long.valueOf(System.currentTimeMillis()));
        this.y.c(false);
        this.y.c(this.x.h());
        this.x.b(Long.parseLong(this.e.a(this.y)));
        this.x.b(1);
        this.x.c(1);
        new com.snda.youni.attachment.a.f(this, this.F).a(this.y);
        return true;
    }

    private boolean i() {
        return this.r != null && this.r.a() > 0;
    }

    private Uri j() {
        j jVar = new j();
        a(jVar);
        if (this.n == null) {
            return null;
        }
        com.sd.a.a.a.a.e a2 = this.n.a();
        jVar.a(a2);
        Uri a3 = this.s.a(jVar, android.a.j.f12a);
        this.n.a(a2);
        return a3;
    }

    private synchronized void k() {
        h b2;
        p m2;
        if (!(this.r == null || this.n == null || this.r.a() != 0 || this.n == null || (b2 = this.n.get(0)) == null || (m2 = b2.m()) == null)) {
            this.t = m2.x();
        }
        this.m = 0;
        this.n = null;
        if (this.o != null && this.o.toString().startsWith(android.a.j.f12a.toString())) {
            this.o = null;
        }
        if (this.p != null) {
            this.p.setText("");
            this.p.setVisibility(8);
            this.p = null;
        }
        this.q = null;
        this.r = null;
    }

    static /* synthetic */ void k(NewChatActivity newChatActivity) {
        AlertDialog.Builder builder = new AlertDialog.Builder(newChatActivity);
        builder.setIcon((int) C0000R.drawable.ic_dialog_attach);
        builder.setTitle((int) C0000R.string.add_attachment);
        builder.setAdapter(new com.snda.youni.mms.ui.h(newChatActivity), new b(newChatActivity));
        builder.show();
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0011, code lost:
        if ((r3.d.g() > 0) != false) goto L_0x0013;
     */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0025  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0044  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void l() {
        /*
            r3 = this;
            r2 = 1
            r1 = 0
            boolean r0 = r3.i()
            if (r0 != 0) goto L_0x0013
            com.snda.youni.modules.InputView r0 = r3.d
            int r0 = r0.g()
            if (r0 <= 0) goto L_0x0038
            r0 = r2
        L_0x0011:
            if (r0 == 0) goto L_0x003c
        L_0x0013:
            android.widget.EditText r0 = r3.c
            android.text.Editable r0 = r0.getText()
            boolean r0 = android.text.TextUtils.isEmpty(r0)
            if (r0 != 0) goto L_0x003a
            r0 = r2
        L_0x0020:
            if (r0 == 0) goto L_0x003c
            r0 = r2
        L_0x0023:
            if (r0 == 0) goto L_0x0044
            com.snda.youni.mms.ui.f r0 = r3.r
            if (r0 == 0) goto L_0x0032
            com.snda.youni.mms.ui.f r0 = r3.r
            int r0 = r0.a()
            r1 = 4
            if (r0 == r1) goto L_0x003e
        L_0x0032:
            com.snda.youni.modules.InputView r0 = r3.d
            r0.a(r2)
        L_0x0037:
            return
        L_0x0038:
            r0 = r1
            goto L_0x0011
        L_0x003a:
            r0 = r1
            goto L_0x0020
        L_0x003c:
            r0 = r1
            goto L_0x0023
        L_0x003e:
            com.snda.youni.mms.ui.f r0 = r3.r
            r0.a(r2)
            goto L_0x0032
        L_0x0044:
            com.snda.youni.mms.ui.f r0 = r3.r
            if (r0 == 0) goto L_0x004d
            com.snda.youni.mms.ui.f r0 = r3.r
            r0.a(r1)
        L_0x004d:
            com.snda.youni.modules.InputView r0 = r3.d
            r0.a(r1)
            goto L_0x0037
        */
        throw new UnsupportedOperationException("Method not decompiled: com.snda.youni.activities.NewChatActivity.l():void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:67:0x0277  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a() {
        /*
            r10 = this;
            r4 = 0
            r9 = 1
            r8 = 0
            android.widget.EditText r0 = r10.c
            android.text.Editable r0 = r0.getText()
            java.lang.String r0 = r0.toString()
            r10.f = r0
            java.lang.String r0 = r10.f
            java.lang.String r1 = ","
            java.lang.String[] r6 = r0.split(r1)
            if (r6 == 0) goto L_0x001e
            int r0 = r6.length
            if (r0 <= r9) goto L_0x001e
            r10.j = r9
        L_0x001e:
            r0 = r8
        L_0x001f:
            int r1 = r6.length
            if (r0 >= r1) goto L_0x0090
            java.lang.String r1 = "<(.+)>"
            java.util.regex.Pattern r1 = java.util.regex.Pattern.compile(r1)
            r2 = r6[r0]
            java.util.regex.Matcher r1 = r1.matcher(r2)
            boolean r2 = r1.find()
            if (r2 == 0) goto L_0x003a
            java.lang.String r1 = r1.group(r9)
            r6[r0] = r1
        L_0x003a:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "btn_send onclick, matcher addresses[i]="
            java.lang.StringBuilder r1 = r1.append(r2)
            r2 = r6[r0]
            java.lang.StringBuilder r1 = r1.append(r2)
            r1.toString()
            r1 = r6[r0]
            boolean r1 = com.snda.youni.e.t.b(r1)
            if (r1 != 0) goto L_0x005e
            r1 = r6[r0]
            java.lang.String r1 = com.snda.youni.e.q.stripSeparators(r1)
            r6[r0] = r1
        L_0x005e:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "btn_send onclick, stripSeparators addresses[i]="
            java.lang.StringBuilder r1 = r1.append(r2)
            r2 = r6[r0]
            java.lang.StringBuilder r1 = r1.append(r2)
            r1.toString()
            r1 = r6[r0]
            boolean r1 = com.snda.youni.e.q.isWellFormedSmsAddress(r1)
            if (r1 != 0) goto L_0x008d
            r1 = r6[r0]
            boolean r1 = com.snda.youni.e.t.b(r1)
            if (r1 != 0) goto L_0x008d
            r0 = 2131361847(0x7f0a0037, float:1.8343458E38)
            android.widget.Toast r0 = android.widget.Toast.makeText(r10, r0, r8)
            r0.show()
        L_0x008c:
            return
        L_0x008d:
            int r0 = r0 + 1
            goto L_0x001f
        L_0x0090:
            com.snda.youni.modules.InputView r0 = r10.d
            android.text.Editable r0 = r0.a()
            java.lang.String r7 = r0.toString()
            r10.q = r7
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "btn_send onclick, addresses[i]="
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.StringBuilder r0 = r0.append(r6)
            java.lang.String r1 = " message="
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.StringBuilder r0 = r0.append(r7)
            r0.toString()
            java.lang.String r0 = r7.trim()
            boolean r0 = android.text.TextUtils.isEmpty(r0)
            if (r0 == 0) goto L_0x00d1
            com.snda.youni.mms.ui.f r0 = r10.r
            if (r0 != 0) goto L_0x00d1
            r0 = 2131361848(0x7f0a0038, float:1.834346E38)
            android.widget.Toast r0 = android.widget.Toast.makeText(r10, r0, r8)
            r0.show()
            goto L_0x008c
        L_0x00d1:
            int r0 = r10.m
            if (r0 <= 0) goto L_0x0169
            r0 = r9
        L_0x00d6:
            if (r0 == 0) goto L_0x01c1
            r0 = r6[r8]
            r10.f = r0
            java.lang.String[] r2 = new java.lang.String[r9]
            java.lang.String r0 = r10.f
            r2[r8] = r0
            int r0 = r6.length
            if (r0 <= r9) goto L_0x00f3
            boolean r0 = r10.k
            if (r0 != 0) goto L_0x016c
            r0 = 2131361997(0x7f0a00cd, float:1.8343762E38)
            android.widget.Toast r0 = android.widget.Toast.makeText(r10, r0, r8)
            r0.show()
        L_0x00f3:
            boolean r0 = r10.k
            if (r0 == 0) goto L_0x0192
            com.snda.youni.modules.d.b r0 = new com.snda.youni.modules.d.b
            r0.<init>()
            r10.y = r0
            com.snda.youni.modules.d.b r0 = r10.y
            java.lang.String r1 = r10.f
            r0.a(r1)
            com.snda.youni.modules.d.b r0 = r10.y
            r0.b(r7)
            com.snda.youni.modules.d.b r0 = r10.y
            r0.b(r9)
            com.snda.youni.modules.d.b r0 = r10.y
            boolean r1 = r10.j
            r0.d(r1)
            com.snda.youni.modules.d.b r0 = r10.y
            long r1 = java.lang.System.currentTimeMillis()
            java.lang.Long r1 = java.lang.Long.valueOf(r1)
            r0.a(r1)
            com.snda.youni.modules.d.b r0 = r10.y
            r0.c(r8)
            com.snda.youni.modules.d.b r0 = r10.y
            com.snda.youni.attachment.b.b r1 = r10.x
            java.lang.String r1 = r1.h()
            r0.c(r1)
            com.snda.youni.h.a.a r0 = r10.e
            com.snda.youni.modules.d.b r1 = r10.y
            java.lang.String r0 = r0.a(r1)
            com.snda.youni.attachment.b.b r1 = r10.x
            long r2 = java.lang.Long.parseLong(r0)
            r1.b(r2)
            com.snda.youni.attachment.b.b r0 = r10.x
            r0.b(r9)
            com.snda.youni.attachment.b.b r0 = r10.x
            r0.c(r9)
            com.snda.youni.attachment.a.f r0 = new com.snda.youni.attachment.a.f
            android.os.Handler r1 = r10.F
            r0.<init>(r10, r1)
            com.snda.youni.modules.d.b r1 = r10.y
            r0.a(r1)
        L_0x015a:
            com.snda.youni.modules.InputView r0 = r10.d
            r0.a(r8)
        L_0x015f:
            com.snda.youni.modules.InputView r0 = r10.d
            r0.a(r10)
            r10.finish()
            goto L_0x008c
        L_0x0169:
            r0 = r8
            goto L_0x00d6
        L_0x016c:
            com.snda.youni.attachment.b.b r0 = r10.x
            java.lang.String r0 = r0.g()
            java.lang.String r1 = "image/jpeg"
            boolean r0 = r0.endsWith(r1)
            if (r0 == 0) goto L_0x0186
            r0 = 2131361998(0x7f0a00ce, float:1.8343764E38)
            android.widget.Toast r0 = android.widget.Toast.makeText(r10, r0, r8)
            r0.show()
            goto L_0x00f3
        L_0x0186:
            r0 = 2131361999(0x7f0a00cf, float:1.8343766E38)
            android.widget.Toast r0 = android.widget.Toast.makeText(r10, r0, r8)
            r0.show()
            goto L_0x00f3
        L_0x0192:
            r10.b(r9)
            android.net.Uri r3 = r10.o
            com.sd.a.a.a.a.d r4 = r10.s
            com.sd.android.mms.a.a r5 = r10.n
            com.sd.a.a.a.a.j r6 = new com.sd.a.a.a.a.j
            r6.<init>()
            r10.a(r6)
            r5.f()
            r10.w = r9
            java.lang.Thread r7 = new java.lang.Thread
            com.snda.youni.activities.g r0 = new com.snda.youni.activities.g
            r1 = r10
            r0.<init>(r1, r2, r3, r4, r5, r6)
            r7.<init>(r0)
            r7.start()
            r0 = 2131362000(0x7f0a00d0, float:1.8343768E38)
            android.widget.Toast r0 = android.widget.Toast.makeText(r10, r0, r8)
            r0.show()
            goto L_0x015a
        L_0x01c1:
            boolean r0 = r10.j
            if (r0 != 0) goto L_0x02be
            int r0 = r10.h
            if (r0 != r9) goto L_0x01cc
            r10.a(r9)
        L_0x01cc:
            java.lang.String r0 = r10.f
            if (r0 == 0) goto L_0x02be
            java.lang.String r0 = r10.f
            java.lang.String r0 = com.snda.youni.e.q.toCallerIDMinMatch(r0)
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "sid='"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r0 = r1.append(r0)
            java.lang.String r1 = "'"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r3 = r0.toString()
            android.content.ContentResolver r0 = r10.getContentResolver()
            android.net.Uri r1 = com.snda.youni.providers.n.f597a
            java.lang.String[] r2 = com.snda.youni.modules.t.f578a
            r5 = r4
            android.database.Cursor r1 = r0.query(r1, r2, r3, r4, r5)
            if (r1 == 0) goto L_0x02ba
            boolean r0 = r1.moveToFirst()
            if (r0 == 0) goto L_0x02ba
            java.lang.String r0 = "contact_type"
            int r0 = r1.getColumnIndex(r0)
            int r0 = r1.getInt(r0)
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "mAddress:"
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r3 = r10.f
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r3 = " contactType:"
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r2 = r2.append(r0)
            r2.toString()
            if (r9 != r0) goto L_0x02b6
            r10.h = r9
            java.lang.String r0 = "connectivity"
            java.lang.Object r0 = r10.getSystemService(r0)
            android.net.ConnectivityManager r0 = (android.net.ConnectivityManager) r0
            android.net.NetworkInfo r0 = r0.getActiveNetworkInfo()
            if (r0 == 0) goto L_0x02b2
            boolean r0 = r0.isConnected()
            if (r0 == 0) goto L_0x02b2
            r10.a(r9)
        L_0x0247:
            if (r1 == 0) goto L_0x024c
            r1.close()
        L_0x024c:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "btn_send onclick, addresses[i]="
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.StringBuilder r0 = r0.append(r6)
            java.lang.String r1 = " message="
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.StringBuilder r0 = r0.append(r7)
            r0.toString()
            long r4 = r10.a(r6)
            boolean r2 = r10.j
            r0 = r10
            r1 = r6
            r3 = r7
            java.util.List r0 = r0.a(r1, r2, r3, r4)
            if (r0 == 0) goto L_0x015f
            android.app.ProgressDialog r1 = new android.app.ProgressDialog
            r1.<init>(r10)
            r10.l = r1
            android.app.ProgressDialog r1 = r10.l
            java.lang.String r2 = ""
            r1.setTitle(r2)
            android.app.ProgressDialog r1 = r10.l
            r2 = 2131361872(0x7f0a0050, float:1.8343509E38)
            java.lang.String r2 = r10.getString(r2)
            r1.setMessage(r2)
            android.app.ProgressDialog r1 = r10.l
            r1.setIndeterminate(r9)
            android.app.ProgressDialog r1 = r10.l
            r1.setCancelable(r8)
            android.app.ProgressDialog r1 = r10.l
            r1.show()
            com.snda.youni.activities.p r1 = new com.snda.youni.activities.p
            r1.<init>(r10)
            java.util.List[] r2 = new java.util.List[r9]
            r2[r8] = r0
            r1.execute(r2)
            r0 = -1
            r10.setResult(r0)
            goto L_0x015f
        L_0x02b2:
            r10.a(r8)
            goto L_0x0247
        L_0x02b6:
            r10.a(r8)
            goto L_0x0247
        L_0x02ba:
            r10.a(r8)
            goto L_0x0247
        L_0x02be:
            r10.a(r8)
            goto L_0x024c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.snda.youni.activities.NewChatActivity.a():void");
    }

    public final void a(int i2) {
        if (i2 > 0) {
            c(true);
        } else {
            d(false);
        }
        l();
    }

    public final void b() {
        if (this.i) {
            NetworkInfo activeNetworkInfo = ((ConnectivityManager) getSystemService("connectivity")).getActiveNetworkInfo();
            if (activeNetworkInfo == null || !activeNetworkInfo.isConnected()) {
                if (Settings.System.getInt(getApplicationContext().getContentResolver(), "airplane_mode_on", 0) != 0) {
                    Toast.makeText(getApplicationContext(), (int) C0000R.string.air_mode, 1).show();
                } else {
                    Toast.makeText(getApplicationContext(), (int) C0000R.string.no_network, 1).show();
                }
                this.i = false;
            }
            if (com.snda.youni.e.j.a(getApplicationContext())) {
                Toast.makeText(getApplicationContext(), (int) C0000R.string.tip_for_wap, 1).show();
                this.i = false;
            }
            NetworkInfo activeNetworkInfo2 = ((ConnectivityManager) getSystemService("connectivity")).getActiveNetworkInfo();
            if ((activeNetworkInfo2 == null || !activeNetworkInfo2.isConnected()) && q.b(this.f)) {
                Toast.makeText(getApplicationContext(), (int) C0000R.string.send_may_failed, 1).show();
            }
        }
    }

    public final void b(int i2) {
        String obj = this.d.a().toString();
        int indexOf = obj.indexOf(m.a().a(20));
        if (indexOf != -1) {
            int indexOf2 = obj.indexOf("[http://maps.google.com/maps");
            this.d.a((indexOf2 == -1 || indexOf2 >= indexOf) ? obj.substring(0, indexOf) + obj.substring(indexOf + m.a().a(20).length()) : obj.substring(0, indexOf2) + obj.substring(indexOf + m.a().a(20).length()));
        }
        String str = Long.toHexString(Double.doubleToLongBits(Math.random())) + ".amr";
        new File(com.snda.youni.attachment.e.g, "youni_audio.amr").renameTo(new File(com.snda.youni.attachment.e.g, str));
        this.x = a(i2, str);
        if (this.x == null) {
            Toast.makeText(this, (int) C0000R.string.unsupported_attachment_format, 0).show();
            return;
        }
        this.k = true;
        if (h()) {
            finish();
        }
        if (this.r == null) {
            this.r = new f(this, this.D, findViewById(C0000R.id.attachment_editor));
            this.r.a(this);
            this.n = a((Context) this);
            a(this.n);
            try {
                this.o = j();
            } catch (com.sd.a.a.a.b e2) {
                e2.printStackTrace();
            }
            this.r.a(this.n, 3);
        }
        try {
            this.r.b(Uri.fromFile(new File(com.snda.youni.attachment.e.j, this.x.h())));
            this.r.a(this.n, 1);
            ((TextView) findViewById(C0000R.id.attachment_info)).setText(getString(C0000R.string.audio_preview_info, new Object[]{this.x.l(), Integer.valueOf(this.x.n())}));
        } catch (com.sd.a.a.a.b e3) {
        } catch (com.sd.android.mms.b e4) {
            com.snda.youni.mms.ui.m.a(this, b(C0000R.string.unsupported_media_format, c((int) C0000R.string.type_audio)), b(C0000R.string.select_different_media, c((int) C0000R.string.type_audio)));
        } catch (com.sd.android.mms.d e5) {
            com.snda.youni.mms.ui.m.a(this, c((int) C0000R.string.exceed_message_size_limitation), b(C0000R.string.failed_to_add_media, c((int) C0000R.string.type_audio)));
        }
    }

    public final void c() {
        if (this.A != null) {
            this.A.b(this.f);
        }
    }

    public final void d() {
        this.q = this.d.a().toString();
        l();
    }

    public final void e() {
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        Uri data;
        "requestCode=" + i2;
        if (i3 == -1) {
            switch (i2) {
                case 0:
                    a(intent);
                    break;
                case 10:
                    com.snda.youni.g.a.a(getApplicationContext(), "send_image", null);
                    String obj = this.d.a().toString();
                    int indexOf = obj.indexOf(m.a().a(20));
                    if (indexOf != -1) {
                        int indexOf2 = obj.indexOf("[http://maps.google.com/maps");
                        this.d.a((indexOf2 == -1 || indexOf2 >= indexOf) ? obj.substring(0, indexOf) + obj.substring(indexOf + m.a().a(20).length()) : obj.substring(0, indexOf2) + obj.substring(indexOf + m.a().a(20).length()));
                    }
                    this.x = a(intent.getData());
                    if (this.x != null) {
                        this.k = true;
                        if (h()) {
                            finish();
                        }
                        if (this.r == null) {
                            this.r = new f(this, this.D, findViewById(C0000R.id.attachment_editor));
                            this.r.a(this);
                            this.n = a((Context) this);
                            a(this.n);
                            try {
                                this.o = j();
                            } catch (com.sd.a.a.a.b e2) {
                                e2.printStackTrace();
                            }
                            this.r.a(this.n, 1);
                        }
                        b(intent.getData());
                        break;
                    } else {
                        Toast.makeText(this, (int) C0000R.string.unsupported_attachment_format, 0).show();
                        return;
                    }
                case 11:
                    com.snda.youni.g.a.a(getApplicationContext(), "send_image", null);
                    String obj2 = this.d.a().toString();
                    int indexOf3 = obj2.indexOf(m.a().a(20));
                    if (indexOf3 != -1) {
                        int indexOf4 = obj2.indexOf("[http://maps.google.com/maps");
                        this.d.a((indexOf4 == -1 || indexOf4 >= indexOf3) ? obj2.substring(0, indexOf3) + obj2.substring(indexOf3 + m.a().a(20).length()) : obj2.substring(0, indexOf4) + obj2.substring(indexOf3 + m.a().a(20).length()));
                    }
                    this.x = a(Uri.fromFile(new File(com.snda.youni.attachment.e.g, "youni_camera.jpg")));
                    if (this.x != null) {
                        this.k = true;
                        if (h()) {
                            finish();
                        }
                        if (this.r == null) {
                            this.r = new f(this, this.D, findViewById(C0000R.id.attachment_editor));
                            this.r.a(this);
                            this.n = a((Context) this);
                            a(this.n);
                            try {
                                this.o = j();
                            } catch (com.sd.a.a.a.b e3) {
                                e3.printStackTrace();
                            }
                            this.r.a(this.n, 1);
                        }
                        b(Uri.fromFile(new File(com.snda.youni.attachment.e.h, this.x.h())));
                        break;
                    } else {
                        Toast.makeText(this, (int) C0000R.string.unsupported_attachment_format, 0).show();
                        return;
                    }
                case 14:
                case 15:
                    if (i2 == 14) {
                        data = (Uri) intent.getParcelableExtra("android.intent.extra.ringtone.PICKED_URI");
                        if (Settings.System.DEFAULT_RINGTONE_URI.equals(data)) {
                            data = null;
                        }
                    } else {
                        data = intent.getData();
                    }
                    if (data != null) {
                        try {
                            this.r.b(data);
                            this.r.a(this.n, 3);
                            break;
                        } catch (com.sd.a.a.a.b e4) {
                            "add audio failed " + e4;
                            Toast.makeText(this, b(C0000R.string.failed_to_add_media, c((int) C0000R.string.type_audio)), 0).show();
                            break;
                        } catch (com.sd.android.mms.b e5) {
                            com.snda.youni.mms.ui.m.a(this, b(C0000R.string.unsupported_media_format, c((int) C0000R.string.type_audio)), b(C0000R.string.select_different_media, c((int) C0000R.string.type_audio)));
                            break;
                        } catch (com.sd.android.mms.d e6) {
                            com.snda.youni.mms.ui.m.a(this, c((int) C0000R.string.exceed_message_size_limitation), b(C0000R.string.failed_to_add_media, c((int) C0000R.string.type_audio)));
                            break;
                        }
                    } else {
                        d(i());
                        return;
                    }
            }
            if (this.r != null) {
                TranslateAnimation translateAnimation = new TranslateAnimation(1, 0.0f, 1, 0.0f, 1, 150.0f, 1, 0.0f);
                translateAnimation.setDuration(500);
                ((LinearLayout) findViewById(C0000R.id.attachment_editor)).startAnimation(translateAnimation);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        int indexOf;
        int indexOf2;
        super.onCreate(bundle);
        getWindow().setSoftInputMode(18);
        this.e = new com.snda.youni.h.a.a(getApplicationContext());
        setContentView((int) C0000R.layout.new_chat);
        this.d = (InputView) findViewById(C0000R.id.input);
        this.d.a((com.snda.youni.modules.l) this);
        bindService(new Intent(this, YouniService.class), this.B, 1);
        this.C = new df(this, getContentResolver());
        ListView listView = (ListView) findViewById(C0000R.id.recipients_list);
        this.f188a = new a(this);
        listView.setAdapter((ListAdapter) this.f188a);
        listView.setOnItemClickListener(this);
        this.c = (EditText) findViewById(C0000R.id.search_input_box);
        this.c.setFilters(new InputFilter[]{new com.snda.youni.e.l()});
        this.c.addTextChangedListener(new e(this));
        findViewById(C0000R.id.add_recipient).setOnClickListener(new h(this));
        this.s = d.a(this);
        Intent intent = getIntent();
        if (intent != null && intent.getExtras() != null) {
            if (((k) intent.getExtras().get("item")) != null) {
                a(intent);
                return;
            }
            String string = intent.getExtras().getString("message_body");
            if (string != null) {
                this.d.a(string);
                return;
            }
            long j2 = intent.getExtras().getLong("_id");
            if (!intent.getExtras().getBoolean("mms")) {
                com.snda.youni.modules.d.b d2 = com.snda.youni.h.a.a.a().d(Long.toString(j2));
                if (d2.c() != null) {
                    Bitmap a2 = com.snda.youni.e.p.a(d2.c(), com.snda.youni.attachment.e.h);
                    if (a2 == null) {
                        a2 = com.snda.youni.e.p.a(d2.c(), com.snda.youni.attachment.e.i);
                    }
                    if (a2 != null) {
                        if (this.r == null) {
                            this.r = new f(this, this.D, findViewById(C0000R.id.attachment_editor));
                            this.r.a(this);
                            this.n = a((Context) this);
                            a(this.n);
                            try {
                                this.o = j();
                            } catch (com.sd.a.a.a.b e2) {
                                e2.printStackTrace();
                            }
                            this.r.a(this.n, 1);
                            c(true);
                        }
                        this.x = a(a2);
                        try {
                            b(com.snda.youni.mms.ui.m.a(this, this.o, a2));
                        } catch (com.sd.a.a.a.b e3) {
                            a(e3);
                        }
                        this.k = true;
                    }
                }
                if (d2.b() != null) {
                    String b2 = d2.b();
                    if (b2 != null && this.k && b2 != null && b2.contains("[ http://n.sdo.com/") && b2.contains(" ]") && (indexOf2 = b2.indexOf(" ]") + 1) > (indexOf = b2.indexOf("[ http://n.sdo.com/") + 1)) {
                        b2 = b2.substring(0, indexOf - 1) + b2.substring(indexOf2 + 1);
                        int lastIndexOf = b2.lastIndexOf("(");
                        if (lastIndexOf > 0) {
                            b2 = b2.substring(0, lastIndexOf);
                        } else if (lastIndexOf != -1) {
                            b2 = "";
                        }
                        this.k = true;
                    }
                    this.d.a(b2);
                }
                if (!this.k) {
                    b(false);
                    return;
                }
                return;
            }
            Uri withAppendedId = ContentUris.withAppendedId(android.a.d.f7a, j2);
            this.b = withAppendedId;
            try {
                com.sd.a.a.a.a.l lVar = (com.sd.a.a.a.a.l) this.s.a(withAppendedId);
                if (lVar.j() != null) {
                    this.q = lVar.j().c();
                }
                this.v = j2;
                b(true);
                this.r = new f(this, this.D, findViewById(C0000R.id.attachment_editor));
                this.r.a(this);
                this.d.a(this.q);
                int a3 = com.snda.youni.mms.ui.m.a(this.n);
                if (a3 == -1) {
                    a3 = 0;
                }
                if (this.n != null) {
                    a(this.n);
                    this.r.a(this.n, a3);
                }
                if (a3 > 0) {
                    c(true);
                    this.k = false;
                }
            } catch (com.sd.a.a.a.b e4) {
                e4.printStackTrace();
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (!this.w && this.o != null) {
            getContentResolver().delete(this.o, null, null);
        }
        s.f = 7;
        AppContext.b(this);
        this.d.d();
        unbindService(this.B);
        if (this.l != null) {
            this.l.dismiss();
        }
        this.l = null;
        Cursor cursor = this.f188a.getCursor();
        if (cursor != null) {
            cursor.close();
        }
    }

    public void onItemClick(AdapterView adapterView, View view, int i2, long j2) {
        String sb;
        com.snda.youni.modules.c cVar = (com.snda.youni.modules.c) view.getTag();
        this.f = cVar.b.getText().toString();
        this.g = cVar.f530a.getText().toString();
        this.h = cVar.c;
        String[] split = this.c.getText().toString().split(",");
        int length = split.length;
        if (length == 1) {
            sb = "";
        } else {
            StringBuilder sb2 = new StringBuilder();
            for (int i3 = 0; i3 < length - 1; i3++) {
                sb2.append(split[i3]).append(',');
            }
            sb = sb2.toString();
        }
        this.c.setText(sb + " " + this.g + " <" + this.f + ">,");
        this.f188a.changeCursor(null);
        if (this.f != null && this.f.split(",").length > 1) {
            "isGroup:" + this.j;
            this.j = true;
        }
        l();
        "btn_send onclick, mAddress=" + this.f;
        this.d.requestFocus();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        s.f = 1;
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        s.f = 0;
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        s.f = 1;
        AppContext.a(this);
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        s.f = 3;
    }
}
