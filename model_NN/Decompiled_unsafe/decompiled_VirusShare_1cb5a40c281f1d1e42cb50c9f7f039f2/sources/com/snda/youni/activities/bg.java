package com.snda.youni.activities;

import android.provider.Settings;
import com.snda.youni.C0000R;
import com.snda.youni.c.k;
import com.snda.youni.f.a;
import com.snda.youni.f.c;
import com.snda.youni.f.d;
import com.snda.youni.modules.WarningTipView;
import com.snda.youni.modules.chat.b;

final class bg implements a {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ ChatActivity f230a;

    bg(ChatActivity chatActivity) {
        this.f230a = chatActivity;
    }

    public final void a(c cVar, d dVar) {
        k kVar = (k) dVar.b();
        if (kVar == null || kVar.b() != 0) {
            b.a(false);
            this.f230a.g();
            WarningTipView warningTipView = (WarningTipView) this.f230a.findViewById(C0000R.id.chat_warning);
            warningTipView.a(this.f230a.getString(C0000R.string.chat_youni_ad));
            warningTipView.a(15000);
            warningTipView.setVisibility(0);
            warningTipView.a();
        } else {
            ChatActivity.e(this.f230a);
            boolean unused = this.f230a.Q = true;
            boolean unused2 = this.f230a.al = true;
            this.f230a.g();
        }
        if (Settings.System.getInt(this.f230a.getApplicationContext().getContentResolver(), "airplane_mode_on", 0) == 0) {
            return;
        }
        if (!this.f230a.P || !this.f230a.Q) {
            WarningTipView warningTipView2 = (WarningTipView) this.f230a.findViewById(C0000R.id.chat_warning);
            warningTipView2.a(this.f230a.getString(C0000R.string.air_mode));
            warningTipView2.setVisibility(0);
            warningTipView2.a();
        }
    }

    public final void a(Exception exc, String str) {
        "onExceptionCaught:" + str;
    }
}
