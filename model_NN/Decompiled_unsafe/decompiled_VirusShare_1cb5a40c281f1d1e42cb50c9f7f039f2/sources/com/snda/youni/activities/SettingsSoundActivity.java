package com.snda.youni.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.TypedArray;
import android.database.Cursor;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.view.View;
import com.snda.youni.AppContext;
import com.snda.youni.C0000R;
import com.snda.youni.e.n;
import com.snda.youni.e.s;
import com.snda.youni.g.a;
import com.snda.youni.modules.settings.SettingsItemView;
import com.snda.youni.modules.settings.i;

public class SettingsSoundActivity extends Activity implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private static final String[] f194a = {"_id", "title"};
    /* access modifiers changed from: private */
    public SharedPreferences b;
    private SettingsItemView c;
    private SettingsItemView d;
    private SettingsItemView e;
    private SettingsItemView f;
    /* access modifiers changed from: private */
    public int g;

    private int a() {
        return this.b.getInt("sound_type_name", 0);
    }

    private String b() {
        return this.b.getString("sound_title_name", "");
    }

    /* access modifiers changed from: private */
    public void c() {
        int i;
        int i2;
        String str;
        int i3;
        int a2 = a();
        String string = getString(C0000R.string.settings_message_subtitle_currentring);
        switch (a2) {
            case 0:
                TypedArray obtainTypedArray = getResources().obtainTypedArray(C0000R.array.sound_details);
                String str2 = string + obtainTypedArray.getString(this.b.getInt("sound_youni_name", 0));
                obtainTypedArray.recycle();
                i3 = 4;
                i2 = 0;
                str = str2;
                i = 4;
                break;
            case 1:
                i = 0;
                i2 = 4;
                str = string + b();
                i3 = 4;
                break;
            case 2:
                i = 4;
                i2 = 4;
                str = string + b();
                i3 = 0;
                break;
            default:
                i3 = 4;
                i2 = 4;
                str = string;
                i = 4;
                break;
        }
        this.d.b(i2);
        this.e.b(i);
        this.f.b(i3);
        this.c.a(str);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        String str;
        Uri uri;
        int i3;
        SettingsSoundActivity settingsSoundActivity;
        super.onActivityResult(i, i2, intent);
        if (i2 == -1) {
            switch (i) {
                case 1000:
                    Uri uri2 = (Uri) intent.getParcelableExtra("android.intent.extra.ringtone.PICKED_URI");
                    "ringtone uri: " + uri2.toString();
                    i3 = 1;
                    settingsSoundActivity = this;
                    Uri uri3 = uri2;
                    str = RingtoneManager.getRingtone(this, uri2).getTitle(this);
                    uri = uri3;
                    break;
                case 1001:
                    Uri data = intent.getData();
                    String format = String.format("_id='" + data.getLastPathSegment() + "'", new Object[0]);
                    Cursor query = getContentResolver().query(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, f194a, format, null, null);
                    if (!query.moveToFirst()) {
                        Cursor query2 = getContentResolver().query(MediaStore.Audio.Media.INTERNAL_CONTENT_URI, f194a, format, null, null);
                        if (!query2.moveToFirst()) {
                            str = null;
                            uri = data;
                            i3 = 2;
                            settingsSoundActivity = this;
                            break;
                        } else {
                            str = query2.getString(query2.getColumnIndex("title"));
                            uri = data;
                            i3 = 2;
                            settingsSoundActivity = this;
                            break;
                        }
                    } else {
                        str = query.getString(query.getColumnIndex("title"));
                        uri = data;
                        i3 = 2;
                        settingsSoundActivity = this;
                        break;
                    }
                default:
                    return;
            }
            if (uri != null) {
                settingsSoundActivity.b.edit().putInt("sound_type_name", i3).commit();
                settingsSoundActivity.b.edit().putString("sound_uri_name", uri.toString()).commit();
                settingsSoundActivity.b.edit().putString("sound_title_name", str).commit();
            }
            c();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public void onClick(View view) {
        switch (view.getId()) {
            case C0000R.id.back /*2131558409*/:
                finish();
                return;
            case C0000R.id.now /*2131558436*/:
                i.a(this);
                return;
            case C0000R.id.system /*2131558437*/:
                Intent intent = new Intent();
                intent.setAction("android.intent.action.RINGTONE_PICKER");
                intent.putExtra("android.intent.extra.ringtone.TYPE", 2);
                intent.putExtra("android.intent.extra.ringtone.SHOW_SILENT", false);
                intent.putExtra("android.intent.extra.ringtone.SHOW_DEFAULT", false);
                if (a() == 1) {
                    intent.putExtra("android.intent.extra.ringtone.EXISTING_URI", Uri.parse(this.b.getString("sound_uri_name", "")));
                }
                startActivityForResult(intent, 1000);
                return;
            case C0000R.id.youni /*2131558438*/:
                this.g = this.b.getInt("sound_youni_name", 0);
                new AlertDialog.Builder(this).setTitle((int) C0000R.string.settings_sound_youni).setSingleChoiceItems((int) C0000R.array.sound_details, this.g, new z(this)).setPositiveButton((int) C0000R.string.alert_dialog_ok, new y(this)).setNegativeButton((int) C0000R.string.alert_dialog_cancel, (DialogInterface.OnClickListener) null).show();
                return;
            case C0000R.id.music /*2131558439*/:
                Intent intent2 = new Intent();
                intent2.setAction("android.intent.action.GET_CONTENT");
                intent2.setType("audio/*");
                startActivityForResult(intent2, 1001);
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        setContentView((int) C0000R.layout.activity_settings_sound);
        this.d = (SettingsItemView) findViewById(C0000R.id.youni);
        this.e = (SettingsItemView) findViewById(C0000R.id.system);
        this.f = (SettingsItemView) findViewById(C0000R.id.music);
        this.d.a((int) C0000R.drawable.checked);
        this.e.a((int) C0000R.drawable.checked);
        this.f.a((int) C0000R.drawable.checked);
        this.c = (SettingsItemView) findViewById(C0000R.id.now);
        findViewById(C0000R.id.back).setOnClickListener(this);
        this.e.setOnClickListener(this);
        this.d.setOnClickListener(this);
        this.f.setOnClickListener(this);
        this.c.setOnClickListener(this);
        this.b = PreferenceManager.getDefaultSharedPreferences(this);
        c();
        a.a(getApplicationContext(), "message_sound", null);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        s.i = 7;
        AppContext.b(this);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        n.a(this).a();
        s.i = 1;
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        s.i = 0;
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        s.i = 1;
        AppContext.a(this);
        findViewById(C0000R.id.windows).setBackgroundDrawable(com.snda.youni.d.a.a("bg_set"));
        findViewById(C0000R.id.tab_title).setBackgroundDrawable(com.snda.youni.d.a.a("bg_tab_title"));
        findViewById(C0000R.id.back).setBackgroundDrawable(com.snda.youni.d.a.a("btn_return"));
        this.c.setBackgroundDrawable(com.snda.youni.d.a.a("bg_list_item_center"));
        this.d.setBackgroundDrawable(com.snda.youni.d.a.a("bg_list_item_center"));
        this.e.setBackgroundDrawable(com.snda.youni.d.a.a("bg_list_item_center"));
        this.f.setBackgroundDrawable(com.snda.youni.d.a.a("bg_list_item_center"));
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        s.i = 3;
    }
}
