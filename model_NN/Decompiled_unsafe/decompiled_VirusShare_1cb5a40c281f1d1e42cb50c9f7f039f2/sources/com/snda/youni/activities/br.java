package com.snda.youni.activities;

import android.content.ContentUris;
import android.content.res.Resources;
import android.widget.TextView;
import android.widget.Toast;
import com.sd.a.a.a.a.i;
import com.sd.a.a.a.b;
import com.sd.android.mms.c;
import com.sd.android.mms.d;
import com.snda.youni.C0000R;
import com.snda.youni.mms.ui.af;
import com.snda.youni.mms.ui.m;

final class br implements af {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ RecipientsActivity f241a;

    br(RecipientsActivity recipientsActivity) {
        this.f241a = recipientsActivity;
    }

    public final void a(i iVar) {
        RecipientsActivity recipientsActivity = this.f241a;
        Resources resources = recipientsActivity.getResources();
        if (iVar == null) {
            Toast.makeText(recipientsActivity, resources.getString(C0000R.string.failed_to_add_media, this.f241a.d((int) C0000R.string.type_picture)), 0).show();
            return;
        }
        this.f241a.d(true);
        try {
            this.f241a.w.a(this.f241a.x.a(iVar, ContentUris.parseId(this.f241a.t)));
            this.f241a.w.a(this.f241a.s, 1);
            ((TextView) this.f241a.findViewById(C0000R.id.attachment_info)).setText(this.f241a.getString(C0000R.string.attachment_preview_info, new Object[]{this.f241a.C.l(), Integer.valueOf(this.f241a.C.o()), Integer.valueOf(this.f241a.C.p())}));
        } catch (b e) {
            Toast.makeText(recipientsActivity, resources.getString(C0000R.string.failed_to_add_media, this.f241a.d((int) C0000R.string.type_picture)), 0).show();
        } catch (com.sd.android.mms.b e2) {
        } catch (c e3) {
            m.a(recipientsActivity, resources.getString(C0000R.string.failed_to_resize_image), resources.getString(C0000R.string.resize_image_error_information));
        } catch (d e4) {
            m.a(recipientsActivity, resources.getString(C0000R.string.exceed_message_size_limitation), resources.getString(C0000R.string.failed_to_add_media, this.f241a.d((int) C0000R.string.type_picture)));
        }
    }
}
