package com.snda.youni.activities;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RemoteViews;
import com.snda.youni.AppContext;
import com.snda.youni.C0000R;
import com.snda.youni.a.a;
import com.snda.youni.d.c;
import com.snda.youni.e.b;
import com.snda.youni.e.s;
import com.snda.youni.modules.d;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class SettingsSkinActivity extends Activity implements View.OnClickListener {
    private static RemoteViews d;
    private static PendingIntent e;
    private static NotificationManager f;
    private static Notification g;
    private static TimerTask h;
    private static Timer i;

    /* renamed from: a  reason: collision with root package name */
    private ListView f193a;
    private d b;
    private ArrayList c;
    /* access modifiers changed from: private */
    public int j;

    static /* synthetic */ void a(int i2) {
        d.setProgressBar(C0000R.id.update_pb, 100, i2, false);
        d.setTextViewText(C0000R.id.update_tv, i2 + "%");
        g.contentView = d;
        g.contentIntent = e;
        f.notify(0, g);
    }

    /* access modifiers changed from: private */
    public File b(String str) {
        "url: " + str;
        try {
            HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(str).openConnection();
            httpURLConnection.setConnectTimeout(20000);
            httpURLConnection.setReadTimeout(20000);
            if (200 == httpURLConnection.getResponseCode()) {
                int contentLength = httpURLConnection.getContentLength();
                if (contentLength <= 0) {
                    throw new a();
                }
                InputStream inputStream = httpURLConnection.getInputStream();
                File file = new File(b.b());
                if (!file.exists()) {
                    file.createNewFile();
                }
                try {
                    Runtime.getRuntime().exec("chmod 777 " + b.b());
                } catch (IOException e2) {
                    e2.printStackTrace();
                }
                FileOutputStream fileOutputStream = new FileOutputStream(file);
                "ljd skin apk path: " + file.getAbsolutePath();
                byte[] bArr = new byte[65535];
                int i2 = 0;
                while (true) {
                    int read = inputStream.read(bArr);
                    if (read == -1) {
                        break;
                    }
                    fileOutputStream.write(bArr, 0, read);
                    i2 += read;
                    this.j = (i2 * 100) / contentLength;
                }
                inputStream.close();
                fileOutputStream.close();
                httpURLConnection.disconnect();
                if (contentLength != 0 && i2 == contentLength) {
                    return file;
                }
                throw new IOException();
            }
            throw new a();
        } catch (MalformedURLException e3) {
            e3.printStackTrace();
            throw new a();
        } catch (SocketTimeoutException e4) {
            e4.printStackTrace();
            throw new a();
        }
    }

    static /* synthetic */ void b(SettingsSkinActivity settingsSkinActivity) {
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.setDataAndType(Uri.fromFile(new File(b.b())), "application/vnd.android.package-archive");
        intent.setFlags(268435456);
        settingsSkinActivity.startActivity(intent);
        e();
    }

    private void c() {
        this.c = new ArrayList();
        c b2 = com.snda.youni.d.b.b(getPackageName());
        b2.f391a = "天蓝";
        b2.d = getResources().getDrawable(C0000R.drawable.icn_skin_blue);
        this.c.add(b2);
        c b3 = com.snda.youni.d.b.b("com.snda.youni.skin.pink");
        if (b3 == null) {
            b3 = new c("com.snda.youni.skin.pink");
            b3.f391a = "粉红";
            b3.e = "http://y.sdo.com/skins/YouNi_Skin_Pink.apk";
        }
        this.c.add(b3);
    }

    private void d() {
        findViewById(C0000R.id.windows).setBackgroundDrawable(com.snda.youni.d.a.a("bg_set"));
        findViewById(C0000R.id.tab_title).setBackgroundDrawable(com.snda.youni.d.a.a("bg_tab_title"));
        findViewById(C0000R.id.back).setBackgroundDrawable(com.snda.youni.d.a.a("btn_return"));
    }

    /* access modifiers changed from: private */
    public static void e() {
        if (h != null) {
            h.cancel();
        }
        if (i != null) {
            i.cancel();
            i.purge();
        }
        if (f != null) {
            f.cancel(0);
        }
    }

    public final void a() {
        d();
        this.f193a.invalidateViews();
    }

    public final void a(String str) {
        new cd(this).execute(str);
        f = (NotificationManager) getSystemService("notification");
        e();
        d = new RemoteViews(getPackageName(), (int) C0000R.layout.update_remote);
        e = PendingIntent.getService(this, 0, new Intent(this, Notification.class), 134217728);
        Notification notification = new Notification();
        g = notification;
        notification.icon = C0000R.drawable.icn_youni;
        d.setImageViewResource(C0000R.id.update_image, C0000R.drawable.icn_youni);
        if (i != null) {
            i.cancel();
        }
        i = new Timer();
        h = new o(this);
        i.schedule(h, 0, 2000);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        switch (i2) {
            case 0:
                c();
                this.b.a(this.c);
                a();
                return;
            default:
                return;
        }
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case C0000R.id.back /*2131558409*/:
                finish();
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        setContentView((int) C0000R.layout.activity_settings_skin);
        c();
        findViewById(C0000R.id.back).setOnClickListener(this);
        this.f193a = (ListView) findViewById(C0000R.id.list_skin);
        this.b = new d(this, this.c);
        this.f193a.setAdapter((ListAdapter) this.b);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        s.o = 7;
        AppContext.b(this);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        s.o = 1;
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        s.o = 0;
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        s.o = 1;
        AppContext.a(this);
        c();
        this.b.a(this.c);
        d();
        a();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        s.o = 3;
    }
}
