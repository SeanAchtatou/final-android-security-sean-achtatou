package com.snda.youni.activities;

import android.text.Editable;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import com.snda.youni.modules.newchat.RecipientsEditor;
import com.snda.youni.providers.n;

final class bp implements TextWatcher {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ RecipientsActivity f239a;

    bp(RecipientsActivity recipientsActivity) {
        this.f239a = recipientsActivity;
    }

    public final void afterTextChanged(Editable editable) {
        "Search s is: " + ((Object) editable);
        String b = RecipientsEditor.b(editable, this.f239a.d.getSelectionStart());
        "Search String is: " + b;
        if (!this.f239a.b.equalsIgnoreCase(b)) {
            String unused = this.f239a.b = b;
            if (TextUtils.isEmpty(b)) {
                this.f239a.H.startQuery(1, null, n.f597a, RecipientsActivity.E, null, null, null);
            } else {
                this.f239a.H.startQuery(1, null, n.f597a, RecipientsActivity.E, "search_name LIKE '" + b + "%'" + " OR " + "pinyin_name LIKE '" + b + "%'" + " OR " + "display_name LIKE '" + b + "%'" + " OR " + "phone_number LIKE '" + b + "%'", null, "times_contacted DESC");
            }
            this.f239a.g();
        }
    }

    public final void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        if ((i > 0 || i == i3) && i2 > i3) {
            String a2 = RecipientsEditor.a((Spanned) charSequence, i, i + i2);
            String obj = charSequence.subSequence(i, i + i2).toString();
            "beforeTextChanged phoneNumber = " + a2 + ", displayName = " + obj;
            this.f239a.e.b(obj, a2);
            this.f239a.i.notifyDataSetChanged();
            this.f239a.e.a();
            this.f239a.g();
        }
    }

    public final void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }
}
