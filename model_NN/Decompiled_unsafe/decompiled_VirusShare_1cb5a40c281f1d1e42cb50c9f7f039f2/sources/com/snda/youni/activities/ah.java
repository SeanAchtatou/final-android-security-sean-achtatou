package com.snda.youni.activities;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Toast;
import com.snda.youni.C0000R;
import com.snda.youni.e.m;
import com.snda.youni.g.a;
import java.math.BigDecimal;

final class ah implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ bw f204a;

    ah(bw bwVar) {
        this.f204a = bwVar;
    }

    public final void onClick(View view) {
        a.a(this.f204a.getContext().getApplicationContext(), "my_postion", null);
        ProgressDialog unused = this.f204a.i = ProgressDialog.show(this.f204a.getContext(), null, this.f204a.getContext().getString(C0000R.string.emotion_location_waiting));
        Location i = this.f204a.c();
        Bundle bundle = new Bundle();
        if (i != null) {
            double latitude = i.getLatitude();
            double longitude = i.getLongitude();
            double altitude = i.getAltitude();
            BigDecimal scale = new BigDecimal(latitude).setScale(6, 4);
            BigDecimal scale2 = new BigDecimal(longitude).setScale(6, 4);
            "latitude:" + latitude + " longitude:" + longitude + " altitude:" + altitude;
            SharedPreferences.Editor edit = PreferenceManager.getDefaultSharedPreferences(this.f204a.getContext()).edit();
            edit.putString("postion", "[http://maps.google.com/maps?q=" + scale.toString() + "," + scale2.toString() + "]");
            edit.commit();
            bundle.putString("smiletext", m.a().a(20));
        } else {
            Toast.makeText(this.f204a.getContext(), (int) C0000R.string.emotion_location_failed, 1).show();
        }
        this.f204a.i.dismiss();
        this.f204a.e.setData(bundle);
        this.f204a.dismiss();
    }
}
