package com.snda.youni.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.database.Cursor;
import android.os.Bundle;
import android.widget.AbsListView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.snda.youni.AppContext;
import com.snda.youni.C0000R;
import com.snda.youni.d;
import com.snda.youni.d.a;
import com.snda.youni.e.s;
import com.snda.youni.modules.ah;
import com.snda.youni.providers.l;

public class SettingsBlackListActivity extends Activity implements AbsListView.OnScrollListener {

    /* renamed from: a  reason: collision with root package name */
    private static final String[] f191a = {"_id", "blacker_name", "blacker_phone"};
    private q b;
    private ListView c;
    /* access modifiers changed from: private */
    public ah d;
    /* access modifiers changed from: private */
    public TextView e;
    private d f;

    public final void a() {
        Cursor query = getContentResolver().query(l.f596a, f191a, null, null, null);
        if (query.getCount() == 0) {
            findViewById(C0000R.id.no_black_list).setVisibility(0);
            this.e.setText((int) C0000R.string.empty_black_list);
            findViewById(C0000R.id.black_list_introduction).setVisibility(0);
            return;
        }
        this.d.changeCursor(query);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        setContentView((int) C0000R.layout.activity_settings_black_list);
        findViewById(C0000R.id.back).setOnClickListener(new u(this));
        this.f = new d(this);
        this.e = (TextView) findViewById(C0000R.id.empty_black_list);
        this.c = (ListView) findViewById(C0000R.id.list_black_list);
        this.d = new ah(this, this.f);
        this.c.setAdapter((ListAdapter) this.d);
        this.c.setOnScrollListener(this);
        this.b = new q(this, getContentResolver());
        this.b.startQuery(0, null, l.f596a, f191a, null, null, null);
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int i) {
        switch (i) {
            case 0:
                return new AlertDialog.Builder(this).setTitle((int) C0000R.string.menu_remove_black_list).setIcon(17301543).setMessage(getString(C0000R.string.remove_from_black_list_notification, new Object[]{ah.b})).setNegativeButton(17039360, (DialogInterface.OnClickListener) null).setPositiveButton(17039370, new v(this)).setCancelable(false).create();
            default:
                return super.onCreateDialog(i);
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        Cursor cursor = this.d.getCursor();
        if (cursor != null) {
            cursor.close();
        }
        this.f.a();
        s.g = 7;
        AppContext.b(this);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.f.b();
        s.g = 1;
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.f.c();
        s.g = 0;
    }

    public void onScroll(AbsListView absListView, int i, int i2, int i3) {
    }

    public void onScrollStateChanged(AbsListView absListView, int i) {
        if (i == 2) {
            this.f.b();
        } else {
            this.f.c();
        }
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        s.g = 1;
        AppContext.a(this);
        findViewById(C0000R.id.windows).setBackgroundDrawable(a.a("bg_set"));
        findViewById(C0000R.id.tab_title).setBackgroundDrawable(a.a("bg_tab_title"));
        findViewById(C0000R.id.back).setBackgroundDrawable(a.a("btn_return"));
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        s.g = 3;
    }
}
