package com.snda.youni.activities;

import android.content.DialogInterface;
import android.widget.SeekBar;
import com.snda.youni.C0000R;
import com.snda.youni.modules.settings.u;

final class m implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ SeekBar f291a;
    private /* synthetic */ SettingsMessageActivity b;

    m(SettingsMessageActivity settingsMessageActivity, SeekBar seekBar) {
        this.b = settingsMessageActivity;
        this.f291a = seekBar;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        ((u) this.b.c.get(Integer.valueOf((int) C0000R.id.vibrator_number))).a(this.f291a.getProgress() + 1);
    }
}
