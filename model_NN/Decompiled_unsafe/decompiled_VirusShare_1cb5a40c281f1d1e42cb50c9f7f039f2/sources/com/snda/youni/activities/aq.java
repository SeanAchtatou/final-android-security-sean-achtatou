package com.snda.youni.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.snda.youni.am;

final class aq extends BroadcastReceiver {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ RecipientsActivity f213a;

    aq(RecipientsActivity recipientsActivity) {
        this.f213a = recipientsActivity;
    }

    public final void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        "Receivier action is: " + action;
        if ("com.snda.youni.ACTION_STATUS_LOADED".equals(action)) {
            this.f213a.l();
        } else if ("com.snda.youni.ACTION_XNETWORK_CONNECTED".equals(action)) {
            am.a(this.f213a.F, true);
        }
    }
}
