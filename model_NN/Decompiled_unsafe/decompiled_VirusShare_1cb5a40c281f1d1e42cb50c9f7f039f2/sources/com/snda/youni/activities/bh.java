package com.snda.youni.activities;

import android.content.ContentValues;
import android.content.DialogInterface;
import android.database.Cursor;
import android.net.Uri;
import android.text.TextUtils;
import android.widget.Toast;
import com.snda.youni.C0000R;
import com.snda.youni.e.j;
import com.snda.youni.providers.l;

final class bh implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ ChatActivity f231a;

    bh(ChatActivity chatActivity) {
        this.f231a = chatActivity;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        if (TextUtils.isEmpty(this.f231a.t.n)) {
            Cursor query = this.f231a.getContentResolver().query(Uri.parse("content://sms/"), new String[]{"canonical_addresses._id from canonical_addresses where PHONE_NUMBERS_EQUAL(canonical_addresses.address, '" + this.f231a.t.g + "') --"}, null, null, null);
            if (query.moveToNext()) {
                this.f231a.t.n = "" + query.getInt(0);
                "ljd addressId for *" + this.f231a.t.g + "* is " + query.getInt(0);
                query.close();
            } else {
                Toast.makeText(this.f231a, (int) C0000R.string.add_to_black_list_failed, 0).show();
                query.close();
                return;
            }
        }
        Cursor query2 = this.f231a.getContentResolver().query(l.f596a, new String[]{"blacker_rid"}, "blacker_rid='" + this.f231a.t.n + "'", null, null);
        if (query2.getCount() > 0) {
            Toast.makeText(this.f231a, this.f231a.getString(C0000R.string.add_duplicate_person_to_black_list, new Object[]{this.f231a.t.f496a}), 0).show();
            query2.close();
            return;
        }
        query2.close();
        String a2 = j.a(this.f231a.t.g);
        ContentValues contentValues = new ContentValues();
        contentValues.put("blacker_rid", this.f231a.t.n);
        contentValues.put("blacker_name", this.f231a.t.f496a);
        contentValues.put("blacker_phone", this.f231a.t.g);
        contentValues.put("blacker_sid", a2);
        if (this.f231a.getContentResolver().insert(l.f596a, contentValues) != null) {
            Toast.makeText(this.f231a, (int) C0000R.string.add_to_black_list_succeed, 0).show();
        } else {
            Toast.makeText(this.f231a, (int) C0000R.string.add_to_black_list_failed, 0).show();
        }
        "ljd Add " + this.f231a.t.n + " to black list";
    }
}
