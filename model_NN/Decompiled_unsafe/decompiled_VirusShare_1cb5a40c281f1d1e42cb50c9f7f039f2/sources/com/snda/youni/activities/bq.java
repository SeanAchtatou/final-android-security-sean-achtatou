package com.snda.youni.activities;

import android.os.Handler;
import android.os.Message;
import com.snda.youni.AppContext;
import com.snda.youni.h.a.a;
import com.snda.youni.modules.d.b;

final class bq extends Handler {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ RecipientsActivity f240a;

    bq(RecipientsActivity recipientsActivity) {
        this.f240a = recipientsActivity;
    }

    public final void handleMessage(Message message) {
        b bVar = (b) message.getData().getSerializable("messageobject");
        if (!bVar.r()) {
            b d = a.a().d("" + bVar.l());
            d.a(com.snda.youni.attachment.b.a.b(AppContext.a(), d.l() + ""));
            this.f240a.j.a(this.f240a.F, d);
            return;
        }
        long[] p = bVar.p();
        for (int i = 0; i < p.length; i++) {
            b d2 = a.a().d("" + p[i]);
            d2.a(com.snda.youni.attachment.b.a.b(AppContext.a(), p[i] + ""));
            this.f240a.j.a(this.f240a.F, d2);
        }
    }
}
