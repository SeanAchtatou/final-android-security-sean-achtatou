package com.snda.youni.activities;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import com.snda.youni.h.a.a;

public class MessageViewAcitivityTest extends ListActivity {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public a f187a;
    /* access modifiers changed from: private */
    public String b = "";
    private String c = "";

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        Cursor cursor;
        am amVar;
        super.onCreate(bundle);
        this.f187a = new a(getApplicationContext());
        if (getIntent().getExtras() == null || getIntent().getExtras().getInt("number") <= 0) {
            Cursor c2 = this.f187a.c();
            cursor = c2;
            amVar = new am(this, c2, true);
        } else {
            int i = getIntent().getExtras().getInt("number");
            "limit:" + i;
            Cursor a2 = this.f187a.a("10086", i);
            cursor = a2;
            amVar = new am(this, a2, false);
        }
        startManagingCursor(cursor);
        setListAdapter(amVar);
    }

    /* access modifiers changed from: protected */
    public void onListItemClick(ListView listView, View view, int i, long j) {
        super.onListItemClick(listView, view, i, j);
        String str = (String) view.getTag();
        if (str != null) {
            this.b = str;
            this.c = this.f187a.d(this.b).b();
            new AlertDialog.Builder(this);
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("消息处理").setMessage(this.c).setPositiveButton("delete", new cq(this)).setNegativeButton("cancel", new cp(this));
            builder.create().show();
        }
    }
}
