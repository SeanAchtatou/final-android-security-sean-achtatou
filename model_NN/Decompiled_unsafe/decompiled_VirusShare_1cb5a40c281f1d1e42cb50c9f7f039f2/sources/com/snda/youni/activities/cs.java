package com.snda.youni.activities;

import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import com.snda.youni.C0000R;

final class cs implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ MessageActivityTest f268a;

    cs(MessageActivityTest messageActivityTest) {
        this.f268a = messageActivityTest;
    }

    public final void onClick(View view) {
        String obj = ((EditText) this.f268a.findViewById(C0000R.id.txt_number)).getText().toString();
        Toast.makeText(this.f268a, "number:" + obj + " has " + this.f268a.f186a.g(obj) + " unread messages!", 1).show();
    }
}
