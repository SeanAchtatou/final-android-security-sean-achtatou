package com.snda.youni.activities;

import android.app.Activity;
import android.os.Bundle;
import com.snda.youni.AppContext;
import com.snda.youni.C0000R;
import com.snda.youni.d.a;
import com.snda.youni.e.s;

public class SettingsHelpActivity extends Activity {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        setContentView((int) C0000R.layout.activity_settings_help);
        findViewById(C0000R.id.back).setOnClickListener(new az(this));
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        s.j = 7;
        AppContext.b(this);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        s.j = 1;
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        s.j = 0;
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        s.j = 1;
        AppContext.a(this);
        findViewById(C0000R.id.windows).setBackgroundDrawable(a.a("bg_set"));
        findViewById(C0000R.id.tab_title).setBackgroundDrawable(a.a("bg_tab_title"));
        findViewById(C0000R.id.back).setBackgroundDrawable(a.a("btn_return"));
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        s.j = 3;
    }
}
