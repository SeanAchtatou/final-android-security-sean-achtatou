package com.snda.youni.activities;

import android.a.o;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.text.TextUtils;
import android.view.View;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.sd.a.a.a.a.j;
import com.sd.a.a.a.a.w;
import com.sd.android.mms.a.e;
import com.sd.android.mms.a.h;
import com.sd.android.mms.a.p;
import com.sd.android.mms.transaction.n;
import com.snda.youni.AppContext;
import com.snda.youni.C0000R;
import com.snda.youni.am;
import com.snda.youni.attachment.b.b;
import com.snda.youni.attachment.g;
import com.snda.youni.b.ai;
import com.snda.youni.d;
import com.snda.youni.e.m;
import com.snda.youni.e.q;
import com.snda.youni.e.s;
import com.snda.youni.e.t;
import com.snda.youni.h.a.a;
import com.snda.youni.mms.ui.af;
import com.snda.youni.mms.ui.f;
import com.snda.youni.mms.ui.l;
import com.snda.youni.modules.InputView;
import com.snda.youni.modules.WarningTipView;
import com.snda.youni.modules.a.k;
import com.snda.youni.modules.newchat.RecipientsEditor;
import com.snda.youni.modules.newchat.c;
import com.snda.youni.services.YouniService;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

public class RecipientsActivity extends Activity implements AdapterView.OnItemClickListener, l, com.snda.youni.modules.l {
    /* access modifiers changed from: private */
    public static final String[] E = {"_id", "contact_id", "display_name", "phone_number", "contact_type", "signature", "expand_data1", "expand_data2"};
    private long A;
    private boolean B = false;
    /* access modifiers changed from: private */
    public b C;
    private com.snda.youni.modules.d.b D;
    /* access modifiers changed from: private */
    public ai F = null;
    private ServiceConnection G = new bv(this);
    /* access modifiers changed from: private */
    public ap H;
    private final Handler I = new bs(this);
    private final af J = new br(this);
    private final Handler K = new bq(this);

    /* renamed from: a  reason: collision with root package name */
    Uri f190a;
    /* access modifiers changed from: private */
    public String b = "";
    /* access modifiers changed from: private */
    public ListView c;
    /* access modifiers changed from: private */
    public RecipientsEditor d;
    /* access modifiers changed from: private */
    public com.snda.youni.modules.newchat.b e;
    /* access modifiers changed from: private */
    public EditText f;
    private String[] g;
    private InputView h;
    /* access modifiers changed from: private */
    public c i;
    /* access modifiers changed from: private */
    public a j;
    private int k;
    private boolean l = true;
    private boolean m = false;
    private boolean n = false;
    /* access modifiers changed from: private */
    public ProgressDialog o;
    private d p;
    private BroadcastReceiver q;
    private int r = 0;
    /* access modifiers changed from: private */
    public com.sd.android.mms.a.a s;
    /* access modifiers changed from: private */
    public Uri t;
    private EditText u;
    private String v;
    /* access modifiers changed from: private */
    public f w;
    /* access modifiers changed from: private */
    public com.sd.a.a.a.a.d x;
    private CharSequence y;
    private int z;

    private long a(String[] strArr) {
        HashSet hashSet = new HashSet();
        hashSet.addAll(Arrays.asList(strArr));
        return o.a(this, hashSet);
    }

    private static com.sd.android.mms.a.a a(Context context) {
        com.sd.android.mms.a.a a2 = com.sd.android.mms.a.a.a(context);
        h hVar = new h(a2, (byte) 0);
        hVar.add((e) new p(context, "text/plain", "text_0.txt", a2.c().b()));
        a2.add(hVar);
        return a2;
    }

    private b a(int i2, String str) {
        try {
            return com.snda.youni.attachment.c.a.a(this, i2, str);
        } catch (g e2) {
            e2.printStackTrace();
            return null;
        }
    }

    private b a(Bitmap bitmap) {
        try {
            return com.snda.youni.attachment.c.a.a(this, bitmap);
        } catch (g e2) {
            return null;
        }
    }

    private b a(Uri uri) {
        try {
            return com.snda.youni.attachment.c.a.a(this, uri);
        } catch (g e2) {
            e2.printStackTrace();
            return null;
        }
    }

    private String a(String str) {
        int indexOf = str.indexOf(m.a().a(20));
        if (indexOf >= 0) {
            String string = PreferenceManager.getDefaultSharedPreferences(this).getString("postion", "");
            if ("".equalsIgnoreCase(string)) {
                Toast.makeText(this, (int) C0000R.string.emotion_location_failed, 1).show();
                return str;
            } else if (!str.contains("[http://maps.google.com/maps")) {
                return str.substring(0, indexOf) + string + str.substring(indexOf);
            }
        }
        return str;
    }

    private static List a(String[] strArr, boolean z2, String str, long j2) {
        ArrayList arrayList = new ArrayList();
        for (String a2 : strArr) {
            com.snda.youni.modules.d.b bVar = new com.snda.youni.modules.d.b();
            bVar.d(z2);
            String a3 = q.a(t.a(a2));
            "address:" + a3 + " body:" + str;
            bVar.a(a3);
            bVar.a(j2);
            bVar.b(str);
            bVar.a(Long.valueOf(System.currentTimeMillis()));
            bVar.e("youni");
            arrayList.add(bVar);
        }
        return arrayList;
    }

    private void a(com.sd.a.a.a.b bVar) {
        "add image failed " + bVar;
        Toast.makeText(this, b(d((int) C0000R.string.type_picture)), 0).show();
    }

    private void a(com.sd.android.mms.a.a aVar) {
        if (aVar.size() == 0) {
            aVar.add(new h(aVar, (byte) 0));
        }
        if (!aVar.get(0).e()) {
            aVar.get(0).add((e) new p(this, "text/plain", "text_0.txt", aVar.c().b()));
        }
    }

    static /* synthetic */ void a(RecipientsActivity recipientsActivity, int i2) {
        switch (i2) {
            case 0:
                com.snda.youni.mms.ui.m.c(recipientsActivity);
                return;
            case 1:
                recipientsActivity.startActivityForResult(new Intent("android.media.action.IMAGE_CAPTURE"), 11);
                return;
            case 2:
            case 3:
            default:
                return;
            case 4:
                com.snda.youni.mms.ui.m.a(recipientsActivity);
                return;
            case 5:
                com.snda.youni.mms.ui.m.b(recipientsActivity);
                return;
        }
    }

    static /* synthetic */ void a(RecipientsActivity recipientsActivity, String[] strArr, Uri uri, com.sd.a.a.a.a.d dVar, com.sd.android.mms.a.a aVar, j jVar) {
        long a2 = recipientsActivity.a(strArr);
        dVar.a(uri, jVar);
        com.sd.a.a.a.a.e a3 = aVar.a();
        try {
            dVar.a(uri, a3);
        } catch (com.sd.a.a.a.b e2) {
            "updateTemporaryMmsMessage: cannot update message " + uri;
        }
        aVar.a(a3);
        com.sd.a.a.a.b.b.a(recipientsActivity, recipientsActivity.getContentResolver(), ContentUris.withAppendedId(android.a.a.f5a, a2), "type=3");
        try {
            new n(recipientsActivity, uri).a(a2);
        } catch (Exception e3) {
            "Failed to send message: " + uri + ", threadId=" + a2 + " " + e3;
        }
    }

    /* access modifiers changed from: private */
    public void a(boolean z2) {
        SharedPreferences.Editor edit = PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit();
        edit.putBoolean("chat_switch_youni", z2);
        edit.commit();
    }

    private void a(String[] strArr, String str) {
        "threadId = " + str;
        k kVar = new k();
        kVar.k = str;
        int i2 = 0;
        if (strArr.length > 1) {
            kVar.p = strArr;
            i2 = this.j.b(strArr);
            "index = " + i2;
        }
        int i3 = i2;
        kVar.g = strArr[i3];
        kVar.f496a = ((com.snda.youni.modules.newchat.g) this.e.get(i3)).a();
        kVar.a(this, new com.snda.youni.modules.a.b(this), strArr[i3]);
        Intent intent = new Intent(this, ChatActivity.class);
        intent.putExtra("item", kVar);
        startActivity(intent);
        finish();
    }

    private String[] a(j jVar) {
        w[] wVarArr;
        String str = "111111";
        this.g = p();
        if (this.g != null) {
            str = this.g[0];
        }
        String[] strArr = {str};
        int length = strArr.length;
        if (length > 0) {
            w[] wVarArr2 = new w[length];
            for (int i2 = 0; i2 < length; i2++) {
                wVarArr2[i2] = new w(strArr[i2]);
            }
            wVarArr = wVarArr2;
        } else {
            wVarArr = null;
        }
        if (wVarArr != null) {
            jVar.a(wVarArr);
        }
        if (this.v != null) {
            jVar.b(new w(this.v));
        }
        jVar.b(System.currentTimeMillis() / 1000);
        return strArr;
    }

    private String b(String str) {
        return getResources().getString(C0000R.string.failed_to_add_media, str);
    }

    private void b(Uri uri) {
        try {
            this.w.a(uri);
            this.w.a(this.s, 1);
            ((TextView) findViewById(C0000R.id.attachment_info)).setText(getString(C0000R.string.attachment_preview_info, new Object[]{this.C.l(), Integer.valueOf(this.C.o()), Integer.valueOf(this.C.p())}));
        } catch (com.sd.a.a.a.b e2) {
            a(e2);
        } catch (com.sd.android.mms.b e3) {
        } catch (com.sd.android.mms.c e4) {
            com.snda.youni.mms.ui.m.a(this, uri, this.I, this.J);
        } catch (com.sd.android.mms.d e5) {
            com.snda.youni.mms.ui.m.a(this, d((int) C0000R.string.exceed_message_size_limitation), b(d((int) C0000R.string.type_picture)));
        }
    }

    private void b(boolean z2) {
        if (z2) {
            try {
                if (this.t != null) {
                    this.t = this.x.a(this.t, android.a.j.f12a);
                    this.s = com.sd.android.mms.a.a.a(this, this.t);
                } else {
                    this.s = com.sd.android.mms.a.a.a(this, ((com.sd.a.a.a.a.l) this.x.a(ContentUris.withAppendedId(android.a.d.f7a, this.A))).i());
                    this.s = com.sd.android.mms.a.a.a(this, this.s.b(this));
                    this.z = com.snda.youni.mms.ui.m.a(this.s);
                    this.t = r();
                }
                this.w = new f(this, this.I, findViewById(C0000R.id.attachment_editor));
                this.w.a(this);
                if (!this.n) {
                    this.h.a(this.v);
                }
                int a2 = com.snda.youni.mms.ui.m.a(this.s);
                if (a2 == -1) {
                    a2 = 0;
                }
                if (this.s != null) {
                    a(this.s);
                    this.w.a(this.s, a2);
                }
                if (a2 > 0) {
                    c(true);
                }
            } catch (com.sd.a.a.a.b e2) {
                e2.getMessage() + " " + e2;
                finish();
            }
        } else {
            s();
        }
    }

    private void c(boolean z2) {
        if (z2) {
            this.r |= 4;
        } else {
            this.r &= -5;
        }
    }

    /* access modifiers changed from: private */
    public String d(int i2) {
        return getResources().getString(i2);
    }

    /* access modifiers changed from: private */
    public void d(boolean z2) {
        boolean z3;
        int i2 = this.r;
        c(z2);
        if (i2 == 0 && this.r != 0) {
            z3 = true;
        } else if (i2 != 0 && this.r == 0) {
            z3 = false;
        } else {
            return;
        }
        if (com.sd.android.mms.a.a() || !z3) {
            b(z3);
            return;
        }
        throw new IllegalStateException("Message converted to MMS with DISABLE_MMS set");
    }

    static /* synthetic */ void i(RecipientsActivity recipientsActivity) {
        AlertDialog.Builder builder = new AlertDialog.Builder(recipientsActivity);
        builder.setIcon((int) C0000R.drawable.ic_dialog_attach);
        builder.setTitle((int) C0000R.string.add_attachment);
        builder.setAdapter(new com.snda.youni.mms.ui.h(recipientsActivity), new bt(recipientsActivity));
        builder.show();
    }

    private String o() {
        String obj = this.h.a().toString();
        int indexOf = obj.indexOf(m.a().a(20));
        if (indexOf != -1) {
            int indexOf2 = obj.indexOf("[http://maps.google.com/maps");
            obj = (indexOf2 == -1 || indexOf2 >= indexOf) ? obj.substring(0, indexOf) + obj.substring(indexOf + m.a().a(20).length()) : obj.substring(0, indexOf2) + obj.substring(indexOf + m.a().a(20).length());
            this.h.a(obj);
        }
        return obj;
    }

    private String[] p() {
        String[] strArr;
        com.snda.youni.modules.newchat.b bVar = this.e;
        if (bVar.size() <= 0) {
            strArr = null;
        } else {
            strArr = new String[bVar.size()];
            Iterator it = bVar.iterator();
            int i2 = 0;
            while (it.hasNext()) {
                strArr[i2] = ((com.snda.youni.modules.newchat.g) it.next()).b();
                i2++;
            }
        }
        if (strArr == null || strArr.length == 0) {
            return null;
        }
        String string = PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getString("self_phone_number", null);
        WarningTipView warningTipView = (WarningTipView) findViewById(C0000R.id.chat_warning);
        int i3 = 0;
        while (i3 < strArr.length) {
            "btn_send onclick, matcher addresses[i]=" + strArr[i3];
            if (!t.b(strArr[i3])) {
                strArr[i3] = q.stripSeparators(strArr[i3]);
            }
            "btn_send onclick, stripSeparators addresses[i]=" + strArr[i3];
            if (!q.isWellFormedSmsAddress(strArr[i3]) && !t.b(strArr[i3])) {
                Toast.makeText(this, (int) C0000R.string.invalid_phonenumber_warning, 0).show();
                return null;
            } else if (string == null || !strArr[i3].contains(string)) {
                warningTipView.setVisibility(8);
                i3++;
            } else {
                warningTipView.a(getString(C0000R.string.not_send_to_self));
                warningTipView.setVisibility(0);
                warningTipView.a(0);
                warningTipView.a();
                return null;
            }
        }
        return strArr;
    }

    private boolean q() {
        return this.w != null && this.w.a() > 0;
    }

    private Uri r() {
        j jVar = new j();
        a(jVar);
        if (this.s == null) {
            return null;
        }
        com.sd.a.a.a.a.e a2 = this.s.a();
        jVar.a(a2);
        Uri a3 = this.x.a(jVar, android.a.j.f12a);
        this.s.a(a2);
        return a3;
    }

    private synchronized void s() {
        h b2;
        p m2;
        if (!(this.w == null || this.s == null || this.w.a() != 0 || this.s == null || (b2 = this.s.get(0)) == null || (m2 = b2.m()) == null)) {
            this.y = m2.x();
        }
        this.r = 0;
        this.s = null;
        if (this.t != null && this.t.toString().startsWith(android.a.j.f12a.toString())) {
            this.t = null;
        }
        if (this.u != null) {
            this.u.setText("");
            this.u.setVisibility(8);
            this.u = null;
        }
        this.v = null;
        this.w = null;
    }

    /* JADX WARNING: Removed duplicated region for block: B:60:0x0260  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a() {
        /*
            r11 = this;
            r4 = 0
            r9 = 1
            r10 = 0
            com.snda.youni.modules.InputView r0 = r11.h
            android.text.Editable r0 = r0.a()
            java.lang.String r0 = r0.toString()
            java.lang.String r1 = r0.trim()
            boolean r1 = android.text.TextUtils.isEmpty(r1)
            if (r1 == 0) goto L_0x0026
            com.snda.youni.mms.ui.f r1 = r11.w
            if (r1 != 0) goto L_0x0026
            r0 = 2131361848(0x7f0a0038, float:1.834346E38)
            android.widget.Toast r0 = android.widget.Toast.makeText(r11, r0, r10)
            r0.show()
        L_0x0025:
            return
        L_0x0026:
            int r1 = r11.r
            if (r1 <= 0) goto L_0x0154
            r1 = r9
        L_0x002b:
            if (r1 == 0) goto L_0x0193
            java.lang.String[] r0 = r11.p()
            if (r0 == 0) goto L_0x0025
            int r1 = r0.length
            if (r1 <= r9) goto L_0x0157
            r1 = r9
        L_0x0037:
            r11.m = r1
            r11.g = r0
            java.lang.String[] r7 = r11.p()
            if (r7 == 0) goto L_0x014d
            r11.g = r7
            r0 = r7[r10]
            com.snda.youni.modules.InputView r1 = r11.h
            android.text.Editable r1 = r1.a()
            java.lang.String r1 = r1.toString()
            java.lang.String r1 = r11.a(r1)
            r11.v = r1
            int r2 = r7.length
            if (r2 <= r9) goto L_0x015a
            r2 = r9
        L_0x0059:
            r11.m = r2
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            long r3 = r11.a(r7)
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r3 = ""
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r8 = r2.toString()
            boolean r2 = r11.n
            if (r2 == 0) goto L_0x015d
            com.snda.youni.modules.d.b r2 = new com.snda.youni.modules.d.b
            r2.<init>()
            r11.D = r2
            com.snda.youni.modules.d.b r2 = r11.D
            r2.a(r0)
            com.snda.youni.modules.d.b r0 = r11.D
            r0.a(r7)
            com.snda.youni.modules.d.b r0 = r11.D
            r0.b(r1)
            com.snda.youni.modules.d.b r0 = r11.D
            r0.b(r9)
            com.snda.youni.modules.d.b r0 = r11.D
            boolean r1 = r11.m
            r0.d(r1)
            com.snda.youni.modules.d.b r0 = r11.D
            long r1 = java.lang.System.currentTimeMillis()
            java.lang.Long r1 = java.lang.Long.valueOf(r1)
            r0.a(r1)
            com.snda.youni.modules.d.b r0 = r11.D
            r0.c(r10)
            com.snda.youni.modules.d.b r0 = r11.D
            com.snda.youni.attachment.b.b r1 = r11.C
            java.lang.String r1 = r1.h()
            r0.c(r1)
            com.snda.youni.modules.d.b r0 = r11.D
            r0.a(r7)
            boolean r0 = r11.m
            if (r0 == 0) goto L_0x00c7
            com.snda.youni.modules.d.b r0 = r11.D
            long r1 = java.lang.Long.parseLong(r8)
            r0.a(r1)
        L_0x00c7:
            com.snda.youni.modules.d.b r0 = r11.D
            com.snda.youni.attachment.b.b r1 = r11.C
            r0.a(r1)
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.StringBuilder r0 = r0.append(r8)
            java.lang.String r1 = ""
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "threadId = "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r0)
            r1.toString()
            com.snda.youni.modules.a.k r1 = new com.snda.youni.modules.a.k
            r1.<init>()
            r1.k = r0
            int r0 = r7.length
            if (r0 <= r9) goto L_0x02c0
            r1.p = r7
            com.snda.youni.h.a.a r0 = r11.j
            int r0 = r0.b(r7)
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "index = "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r2 = r2.append(r0)
            r2.toString()
            r2 = r0
        L_0x0118:
            r0 = r7[r2]
            r1.g = r0
            com.snda.youni.modules.newchat.b r0 = r11.e
            java.lang.Object r0 = r0.get(r2)
            com.snda.youni.modules.newchat.g r0 = (com.snda.youni.modules.newchat.g) r0
            java.lang.String r0 = r0.a()
            r1.f496a = r0
            com.snda.youni.modules.a.b r0 = new com.snda.youni.modules.a.b
            r0.<init>(r11)
            r2 = r7[r2]
            r1.a(r11, r0, r2)
            android.content.Intent r0 = new android.content.Intent
            java.lang.Class<com.snda.youni.activities.ChatActivity> r2 = com.snda.youni.activities.ChatActivity.class
            r0.<init>(r11, r2)
            java.lang.String r2 = "item"
            r0.putExtra(r2, r1)
            java.lang.String r1 = "messageobject"
            com.snda.youni.modules.d.b r2 = r11.D
            r0.putExtra(r1, r2)
            r11.startActivity(r0)
            r11.finish()
        L_0x014d:
            com.snda.youni.modules.InputView r0 = r11.h
            r0.a(r11)
            goto L_0x0025
        L_0x0154:
            r1 = r10
            goto L_0x002b
        L_0x0157:
            r1 = r10
            goto L_0x0037
        L_0x015a:
            r2 = r10
            goto L_0x0059
        L_0x015d:
            java.lang.String[] r2 = new java.lang.String[r9]
            r2[r10] = r0
            r11.b(r9)
            android.net.Uri r3 = r11.t
            com.sd.a.a.a.a.d r4 = r11.x
            com.sd.android.mms.a.a r5 = r11.s
            com.sd.a.a.a.a.j r6 = new com.sd.a.a.a.a.j
            r6.<init>()
            r11.a(r6)
            r5.f()
            r11.B = r9
            java.lang.Thread r9 = new java.lang.Thread
            com.snda.youni.activities.bu r0 = new com.snda.youni.activities.bu
            r1 = r11
            r0.<init>(r1, r2, r3, r4, r5, r6)
            r9.<init>(r0)
            r9.start()
            r0 = 2131362000(0x7f0a00d0, float:1.8343768E38)
            android.widget.Toast r0 = android.widget.Toast.makeText(r11, r0, r10)
            r0.show()
            r11.a(r7, r8)
            goto L_0x014d
        L_0x0193:
            java.lang.String[] r6 = r11.p()
            if (r6 == 0) goto L_0x0025
            int r1 = r6.length
            if (r1 <= r9) goto L_0x02ad
            r1 = r9
        L_0x019d:
            r11.m = r1
            r11.g = r6
            java.lang.String r7 = r11.a(r0)
            r11.v = r7
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "btn_send onclick, addresses[i]="
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.StringBuilder r0 = r0.append(r6)
            java.lang.String r1 = " message="
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.StringBuilder r0 = r0.append(r7)
            r0.toString()
            boolean r0 = r11.m
            if (r0 != 0) goto L_0x02bc
            int r0 = r11.k
            if (r0 != r9) goto L_0x01ce
            r11.a(r9)
        L_0x01ce:
            java.lang.String[] r0 = r11.g
            r0 = r0[r10]
            if (r0 == 0) goto L_0x02bc
            java.lang.String[] r0 = r11.g
            r0 = r0[r10]
            java.lang.String r0 = com.snda.youni.e.q.toCallerIDMinMatch(r0)
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "sid='"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r0 = r1.append(r0)
            java.lang.String r1 = "'"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r3 = r0.toString()
            android.content.ContentResolver r0 = r11.getContentResolver()
            android.net.Uri r1 = com.snda.youni.providers.n.f597a
            java.lang.String[] r2 = com.snda.youni.modules.t.f578a
            r5 = r4
            android.database.Cursor r1 = r0.query(r1, r2, r3, r4, r5)
            if (r1 == 0) goto L_0x02b8
            boolean r0 = r1.moveToFirst()
            if (r0 == 0) goto L_0x02b8
            java.lang.String r0 = "contact_type"
            int r0 = r1.getColumnIndex(r0)
            int r0 = r1.getInt(r0)
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "mAddress:"
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String[] r3 = r11.g
            r3 = r3[r10]
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r3 = " contactType:"
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r2 = r2.append(r0)
            r2.toString()
            if (r9 != r0) goto L_0x02b4
            r11.k = r9
            java.lang.String r0 = "connectivity"
            java.lang.Object r0 = r11.getSystemService(r0)
            android.net.ConnectivityManager r0 = (android.net.ConnectivityManager) r0
            android.net.NetworkInfo r0 = r0.getActiveNetworkInfo()
            if (r0 == 0) goto L_0x02b0
            boolean r0 = r0.isConnected()
            if (r0 == 0) goto L_0x02b0
            r11.a(r9)
        L_0x024f:
            if (r1 == 0) goto L_0x0254
            r1.close()
        L_0x0254:
            long r0 = r11.a(r6)
            boolean r2 = r11.m
            java.util.List r2 = a(r6, r2, r7, r0)
            if (r2 == 0) goto L_0x014d
            android.app.ProgressDialog r3 = new android.app.ProgressDialog
            r3.<init>(r11)
            r11.o = r3
            android.app.ProgressDialog r3 = r11.o
            java.lang.String r4 = ""
            r3.setTitle(r4)
            android.app.ProgressDialog r3 = r11.o
            r4 = 2131361872(0x7f0a0050, float:1.8343509E38)
            java.lang.String r4 = r11.getString(r4)
            r3.setMessage(r4)
            android.app.ProgressDialog r3 = r11.o
            r3.setIndeterminate(r9)
            android.app.ProgressDialog r3 = r11.o
            r3.setCancelable(r10)
            android.app.ProgressDialog r3 = r11.o
            r3.show()
            com.snda.youni.activities.cv r3 = new com.snda.youni.activities.cv
            r3.<init>(r11)
            java.util.List[] r4 = new java.util.List[r9]
            r4[r10] = r2
            r3.execute(r4)
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r1 = ""
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            r11.a(r6, r0)
            goto L_0x014d
        L_0x02ad:
            r1 = r10
            goto L_0x019d
        L_0x02b0:
            r11.a(r10)
            goto L_0x024f
        L_0x02b4:
            r11.a(r10)
            goto L_0x024f
        L_0x02b8:
            r11.a(r10)
            goto L_0x024f
        L_0x02bc:
            r11.a(r10)
            goto L_0x0254
        L_0x02c0:
            r2 = r10
            goto L_0x0118
        */
        throw new UnsupportedOperationException("Method not decompiled: com.snda.youni.activities.RecipientsActivity.a():void");
    }

    public final void a(int i2) {
        if (i2 > 0) {
            c(true);
        } else {
            d(false);
        }
        g();
    }

    public final void b() {
        if (this.l) {
            NetworkInfo activeNetworkInfo = ((ConnectivityManager) getSystemService("connectivity")).getActiveNetworkInfo();
            if (activeNetworkInfo == null || !activeNetworkInfo.isConnected()) {
                if (Settings.System.getInt(getApplicationContext().getContentResolver(), "airplane_mode_on", 0) != 0) {
                    Toast.makeText(getApplicationContext(), (int) C0000R.string.air_mode, 1).show();
                } else {
                    Toast.makeText(getApplicationContext(), (int) C0000R.string.no_network, 1).show();
                }
                this.l = false;
            }
            if (com.snda.youni.e.j.a(getApplicationContext())) {
                Toast.makeText(getApplicationContext(), (int) C0000R.string.tip_for_wap, 1).show();
                this.l = false;
            }
            NetworkInfo activeNetworkInfo2 = ((ConnectivityManager) getSystemService("connectivity")).getActiveNetworkInfo();
            if (activeNetworkInfo2 == null || !activeNetworkInfo2.isConnected()) {
                if (q.b(this.g == null ? null : this.g[0])) {
                    Toast.makeText(getApplicationContext(), (int) C0000R.string.send_may_failed, 1).show();
                }
            }
        }
    }

    public final void b(int i2) {
        o();
        String str = Long.toHexString(Double.doubleToLongBits(Math.random())) + ".amr";
        new File(com.snda.youni.attachment.e.g, "youni_audio.amr").renameTo(new File(com.snda.youni.attachment.e.g, str));
        this.C = a(i2, str);
        if (this.C == null) {
            Toast.makeText(this, (int) C0000R.string.unsupported_attachment_format, 0).show();
            return;
        }
        this.n = true;
        if (this.w == null) {
            this.w = new f(this, this.I, findViewById(C0000R.id.attachment_editor));
            this.w.a(this);
            this.s = a((Context) this);
            a(this.s);
            try {
                this.t = r();
            } catch (com.sd.a.a.a.b e2) {
                e2.printStackTrace();
            }
            this.w.a(this.s, 3);
        }
        try {
            this.w.b(Uri.fromFile(new File(com.snda.youni.attachment.e.j, this.C.h())));
            this.w.a(this.s, 1);
            ((TextView) findViewById(C0000R.id.attachment_info)).setText(getString(C0000R.string.audio_preview_info, new Object[]{this.C.l(), Integer.valueOf(this.C.n())}));
        } catch (com.sd.a.a.a.b | com.sd.android.mms.b e3) {
        } catch (com.sd.android.mms.d e4) {
            com.snda.youni.mms.ui.m.a(this, d((int) C0000R.string.exceed_message_size_limitation), b(d((int) C0000R.string.type_audio)));
        }
    }

    public final void c() {
        if (this.F != null) {
            this.F.b(this.g == null ? null : this.g[0]);
        }
    }

    public final void c(int i2) {
        this.e.remove(i2);
        this.i.notifyDataSetChanged();
        g();
    }

    public final void d() {
        this.v = this.h.a().toString();
        g();
    }

    public final void e() {
        this.c.setVisibility(8);
        this.d.setVisibility(8);
        this.f.setVisibility(0);
    }

    public final boolean f() {
        return this.w != null;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0011, code lost:
        if ((r3.h.g() > 0) != false) goto L_0x0013;
     */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0021  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0040  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void g() {
        /*
            r3 = this;
            r2 = 1
            r1 = 0
            boolean r0 = r3.q()
            if (r0 != 0) goto L_0x0013
            com.snda.youni.modules.InputView r0 = r3.h
            int r0 = r0.g()
            if (r0 <= 0) goto L_0x0034
            r0 = r2
        L_0x0011:
            if (r0 == 0) goto L_0x0038
        L_0x0013:
            com.snda.youni.modules.newchat.b r0 = r3.e
            boolean r0 = r0.isEmpty()
            if (r0 != 0) goto L_0x0036
            r0 = r2
        L_0x001c:
            if (r0 == 0) goto L_0x0038
            r0 = r2
        L_0x001f:
            if (r0 == 0) goto L_0x0040
            com.snda.youni.mms.ui.f r0 = r3.w
            if (r0 == 0) goto L_0x002e
            com.snda.youni.mms.ui.f r0 = r3.w
            int r0 = r0.a()
            r1 = 4
            if (r0 == r1) goto L_0x003a
        L_0x002e:
            com.snda.youni.modules.InputView r0 = r3.h
            r0.a(r2)
        L_0x0033:
            return
        L_0x0034:
            r0 = r1
            goto L_0x0011
        L_0x0036:
            r0 = r1
            goto L_0x001c
        L_0x0038:
            r0 = r1
            goto L_0x001f
        L_0x003a:
            com.snda.youni.mms.ui.f r0 = r3.w
            r0.a(r2)
            goto L_0x002e
        L_0x0040:
            com.snda.youni.mms.ui.f r0 = r3.w
            if (r0 == 0) goto L_0x0049
            com.snda.youni.mms.ui.f r0 = r3.w
            r0.a(r1)
        L_0x0049:
            com.snda.youni.modules.InputView r0 = r3.h
            r0.a(r1)
            goto L_0x0033
        */
        throw new UnsupportedOperationException("Method not decompiled: com.snda.youni.activities.RecipientsActivity.g():void");
    }

    public final ListView h() {
        return this.c;
    }

    public final com.snda.youni.modules.newchat.b i() {
        return this.e;
    }

    public final boolean j() {
        View peekDecorView = getWindow().peekDecorView();
        if ((peekDecorView == null || peekDecorView.getWindowToken() == null) ? false : true) {
            return ((InputMethodManager) getSystemService("input_method")).hideSoftInputFromWindow(peekDecorView.getWindowToken(), 0);
        }
        return false;
    }

    public final String k() {
        return this.b;
    }

    public final void l() {
        this.c.invalidateViews();
    }

    public final ai m() {
        return this.F;
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        Uri data;
        "requestCode=" + i2;
        if (i3 == -1) {
            o();
            switch (i2) {
                case 10:
                    this.C = a(intent.getData());
                    if (this.C != null) {
                        this.n = true;
                        if (this.w == null) {
                            this.w = new f(this, this.I, findViewById(C0000R.id.attachment_editor));
                            this.w.a(this);
                            this.s = a((Context) this);
                            a(this.s);
                            try {
                                this.t = r();
                            } catch (com.sd.a.a.a.b e2) {
                                e2.printStackTrace();
                            }
                            this.w.a(this.s, 1);
                        }
                        b(intent.getData());
                        break;
                    } else {
                        Toast.makeText(this, (int) C0000R.string.unsupported_attachment_format, 0).show();
                        return;
                    }
                case 11:
                    this.C = a(Uri.fromFile(new File(com.snda.youni.attachment.e.g, "youni_camera.jpg")));
                    if (this.C != null) {
                        this.n = true;
                        if (this.w == null) {
                            this.w = new f(this, this.I, findViewById(C0000R.id.attachment_editor));
                            this.w.a(this);
                            this.s = a((Context) this);
                            a(this.s);
                            try {
                                this.t = r();
                            } catch (com.sd.a.a.a.b e3) {
                                e3.printStackTrace();
                            }
                            this.w.a(this.s, 1);
                        }
                        b(Uri.fromFile(new File(com.snda.youni.attachment.e.h, this.C.h())));
                        break;
                    } else {
                        Toast.makeText(this, (int) C0000R.string.unsupported_attachment_format, 0).show();
                        return;
                    }
                case 14:
                case 15:
                    if (i2 == 14) {
                        data = (Uri) intent.getParcelableExtra("android.intent.extra.ringtone.PICKED_URI");
                        if (Settings.System.DEFAULT_RINGTONE_URI.equals(data)) {
                            data = null;
                        }
                    } else {
                        data = intent.getData();
                    }
                    if (data != null) {
                        try {
                            this.w.b(data);
                            this.w.a(this.s, 3);
                            break;
                        } catch (com.sd.a.a.a.b e4) {
                            "add audio failed " + e4;
                            Toast.makeText(this, b(d((int) C0000R.string.type_audio)), 0).show();
                            break;
                        } catch (com.sd.android.mms.b e5) {
                            break;
                        } catch (com.sd.android.mms.d e6) {
                            com.snda.youni.mms.ui.m.a(this, d((int) C0000R.string.exceed_message_size_limitation), b(d((int) C0000R.string.type_audio)));
                            break;
                        }
                    } else {
                        d(q());
                        return;
                    }
            }
            if (this.w != null) {
                TranslateAnimation translateAnimation = new TranslateAnimation(1, 0.0f, 1, 0.0f, 1, 150.0f, 1, 0.0f);
                translateAnimation.setDuration(500);
                ((LinearLayout) findViewById(C0000R.id.attachment_editor)).startAnimation(translateAnimation);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        int indexOf;
        int indexOf2;
        super.onCreate(bundle);
        getWindow().setSoftInputMode(18);
        this.j = new a(getApplicationContext());
        setContentView((int) C0000R.layout.recipients_list);
        this.h = (InputView) findViewById(C0000R.id.input);
        this.h.a((com.snda.youni.modules.l) this);
        bindService(new Intent(this, YouniService.class), this.G, 1);
        this.H = new ap(this, getContentResolver());
        this.c = (ListView) findViewById(C0000R.id.recipient_list);
        this.c.setItemsCanFocus(false);
        this.c.setChoiceMode(2);
        this.p = new d(this);
        this.i = new c(this, this.p);
        this.c.setOnItemClickListener(this);
        this.c.setAdapter((ListAdapter) this.i);
        this.c.setOnScrollListener(this.i);
        this.e = new com.snda.youni.modules.newchat.b();
        this.f = (EditText) findViewById(C0000R.id.search_input_box);
        this.f.setOnFocusChangeListener(new bn(this));
        this.d = (RecipientsEditor) findViewById(C0000R.id.recipients_editor);
        this.d.setOnFocusChangeListener(new bo(this));
        this.d.addTextChangedListener(new bp(this));
        this.H.startQuery(1, null, com.snda.youni.providers.n.f597a, E, null, null, null);
        this.q = new aq(this);
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.snda.youni.ACTION_STATUS_LOADED");
        intentFilter.addAction("com.snda.youni.ACTION_XNETWORK_CONNECTED");
        registerReceiver(this.q, intentFilter);
        this.x = com.sd.a.a.a.a.d.a(this);
        Intent intent = getIntent();
        if (intent != null && intent.getExtras() != null) {
            k kVar = (k) intent.getExtras().get("item");
            if (kVar == null) {
                String string = intent.getExtras().getString("message_body");
                if (string != null) {
                    this.h.a(string);
                    return;
                }
                long j2 = intent.getExtras().getLong("_id");
                if (!intent.getExtras().getBoolean("mms")) {
                    com.snda.youni.modules.d.b d2 = a.a().d(Long.toString(j2));
                    if (!(d2 == null || d2.c() == null)) {
                        Bitmap a2 = com.snda.youni.e.p.a(d2.c(), com.snda.youni.attachment.e.h);
                        if (a2 == null) {
                            a2 = com.snda.youni.e.p.a(d2.c(), com.snda.youni.attachment.e.i);
                        }
                        if (a2 != null) {
                            if (this.w == null) {
                                this.w = new f(this, this.I, findViewById(C0000R.id.attachment_editor));
                                this.w.a(this);
                                this.s = a((Context) this);
                                a(this.s);
                                try {
                                    this.t = r();
                                } catch (com.sd.a.a.a.b e2) {
                                    e2.printStackTrace();
                                }
                                this.w.a(this.s, 1);
                                c(true);
                            }
                            this.C = a(a2);
                            try {
                                b(com.snda.youni.mms.ui.m.a(this, this.t, a2));
                            } catch (com.sd.a.a.a.b e3) {
                                a(e3);
                            }
                            this.n = true;
                        }
                    }
                    if (d2.b() != null) {
                        String b2 = d2.b();
                        if (b2 != null && this.n && b2 != null && b2.contains("[ http://n.sdo.com/") && b2.contains(" ]") && (indexOf2 = b2.indexOf(" ]") + 1) > (indexOf = b2.indexOf("[ http://n.sdo.com/") + 1)) {
                            b2 = b2.substring(0, indexOf - 1) + b2.substring(indexOf2 + 1);
                            int lastIndexOf = b2.lastIndexOf("(");
                            if (lastIndexOf > 0) {
                                b2 = b2.substring(0, lastIndexOf);
                            } else if (lastIndexOf != -1) {
                                b2 = "";
                            }
                            this.n = true;
                        }
                        this.h.a(b2);
                    }
                    if (!this.n) {
                        b(false);
                        return;
                    }
                    return;
                }
                Uri withAppendedId = ContentUris.withAppendedId(android.a.d.f7a, j2);
                this.f190a = withAppendedId;
                try {
                    com.sd.a.a.a.a.l lVar = (com.sd.a.a.a.a.l) this.x.a(withAppendedId);
                    if (lVar.j() != null) {
                        this.v = lVar.j().c();
                    }
                    this.A = j2;
                    b(true);
                    this.w = new f(this, this.I, findViewById(C0000R.id.attachment_editor));
                    this.w.a(this);
                    this.h.a(this.v);
                    int a3 = com.snda.youni.mms.ui.m.a(this.s);
                    if (a3 == -1) {
                        a3 = 0;
                    }
                    if (this.s != null) {
                        a(this.s);
                        this.w.a(this.s, a3);
                    }
                    if (a3 > 0) {
                        c(true);
                        this.n = false;
                    }
                } catch (com.sd.a.a.a.b e4) {
                    e4.printStackTrace();
                }
            } else if (!TextUtils.isEmpty(kVar.b)) {
                this.h.a(kVar.b);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (!this.B && this.t != null) {
            getContentResolver().delete(this.t, null, null);
        }
        s.m = 7;
        AppContext.b(this);
        this.h.d();
        unbindService(this.G);
        unregisterReceiver(this.q);
        if (this.o != null) {
            this.o.dismiss();
            this.o = null;
        }
        this.p.a();
        Cursor cursor = this.i.getCursor();
        if (cursor != null) {
            cursor.close();
        }
        this.d = null;
        this.f = null;
        if (this.e != null) {
            this.e.clear();
            this.e = null;
        }
    }

    public void onItemClick(AdapterView adapterView, View view, int i2, long j2) {
        j();
        Cursor cursor = (Cursor) this.i.getItem(i2);
        String string = cursor.getString(2);
        String string2 = cursor.getString(3);
        int a2 = this.c.isItemChecked(i2) ? this.e.a(string, string2) : this.e.b(string, string2);
        if (this.d.getVisibility() == 8) {
            this.d.setVisibility(0);
            this.f.setVisibility(8);
            this.d.requestFocus();
        }
        if (a2 == 0 || a2 == 2) {
            this.d.a(this.e);
            g();
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        s.m = 1;
        this.p.b();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        s.m = 0;
        am.a(this.F, false);
        this.p.c();
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        s.m = 1;
        AppContext.a(this);
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        s.m = 3;
    }
}
