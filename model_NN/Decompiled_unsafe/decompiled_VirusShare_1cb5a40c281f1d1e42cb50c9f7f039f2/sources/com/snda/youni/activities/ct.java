package com.snda.youni.activities;

import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import com.snda.youni.C0000R;

final class ct implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ MessageActivityTest f269a;

    ct(MessageActivityTest messageActivityTest) {
        this.f269a = messageActivityTest;
    }

    public final void onClick(View view) {
        String obj = ((EditText) this.f269a.findViewById(C0000R.id.txt_number)).getText().toString();
        int h = this.f269a.f186a.h(obj);
        new n(this.f269a).execute(new Void[0]);
        Toast.makeText(this.f269a, "number:" + obj + " has " + h + " messages!", 1).show();
    }
}
