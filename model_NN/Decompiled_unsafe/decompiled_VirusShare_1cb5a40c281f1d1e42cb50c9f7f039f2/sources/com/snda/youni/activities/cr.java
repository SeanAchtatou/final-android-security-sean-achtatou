package com.snda.youni.activities;

import android.content.Context;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.PopupWindow;
import android.widget.Toast;
import com.snda.youni.C0000R;
import com.snda.youni.attachment.f;
import com.snda.youni.e.e;
import java.io.IOException;

public final class cr extends PopupWindow {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public Context f267a;
    /* access modifiers changed from: private */
    public View b;
    private View c;
    /* access modifiers changed from: private */
    public f d;
    /* access modifiers changed from: private */
    public int e;
    private CountDownTimer f;

    public cr(Context context, View view, View view2) {
        super(view, -2, -2);
        this.f267a = context;
        this.b = view;
        this.c = view2;
        view.findViewById(C0000R.id.cancel_record2).setOnClickListener(new r(this));
        setOnDismissListener(new s(this));
    }

    /* access modifiers changed from: private */
    public void c() {
        String str;
        try {
            if (this.f != null) {
                this.f.cancel();
            }
            this.d.d();
            ((ChatActivity) this.f267a).getWindow().clearFlags(128);
        } catch (IOException e2) {
            e2.printStackTrace();
        } catch (Exception e3) {
            e3.printStackTrace();
        } finally {
            this.d = null;
            dismiss();
            ((ChatActivity) this.f267a).f();
            str = "youni_audio.amr";
            e.a(str, com.snda.youni.attachment.e.g);
        }
    }

    static /* synthetic */ void d(cr crVar) {
        try {
            if (crVar.f != null) {
                crVar.f.cancel();
            }
            crVar.d.d();
            ((ChatActivity) crVar.f267a).getWindow().clearFlags(128);
        } catch (IOException e2) {
            e2.printStackTrace();
        } catch (Exception e3) {
            e3.printStackTrace();
        }
    }

    public final void a() {
        showAtLocation(this.c, 17, 0, 0);
        try {
            this.d = new f();
            if (!this.d.a()) {
                Toast.makeText(this.f267a, (int) C0000R.string.audio_record_start_error, 0).show();
                c();
            }
            this.e = 0;
            ((ChatActivity) this.f267a).getWindow().addFlags(128);
            this.f = new t(this);
            this.f.start();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
    }

    public final int b() {
        try {
            if (this.f != null) {
                this.f.cancel();
            }
            this.d.d();
            ((ChatActivity) this.f267a).getWindow().clearFlags(128);
            this.d = null;
            dismiss();
            return this.e;
        } catch (IOException e2) {
            e2.printStackTrace();
            this.d = null;
            dismiss();
            return this.e;
        } catch (Exception e3) {
            e3.printStackTrace();
            this.d = null;
            dismiss();
            return this.e;
        } catch (Throwable th) {
            this.d = null;
            dismiss();
            return this.e;
        }
    }
}
