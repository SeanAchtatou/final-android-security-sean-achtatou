package com.snda.youni.activities;

import android.content.DialogInterface;
import com.snda.youni.g.a;
import com.snda.youni.modules.settings.e;
import com.snda.youni.modules.settings.m;

final class j implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ m f288a;
    private /* synthetic */ SettingsMessageActivity b;

    j(SettingsMessageActivity settingsMessageActivity, m mVar) {
        this.b = settingsMessageActivity;
        this.f288a = mVar;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.f288a.a(this.b.f192a);
        if (this.f288a instanceof e) {
            a.b(this.b.getApplicationContext(), "word_size", "" + this.b.f192a);
        }
    }
}
