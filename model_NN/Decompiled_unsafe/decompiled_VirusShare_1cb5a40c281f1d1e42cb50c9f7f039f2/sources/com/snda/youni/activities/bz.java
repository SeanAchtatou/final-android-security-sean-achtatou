package com.snda.youni.activities;

import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;

final class bz implements LocationListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ bw f249a;

    bz(bw bwVar) {
        this.f249a = bwVar;
    }

    public final void onLocationChanged(Location location) {
        if (location != null) {
            "Location changed : Lat: " + location.getLatitude() + " Lng: " + location.getLongitude();
        }
    }

    public final void onProviderDisabled(String str) {
    }

    public final void onProviderEnabled(String str) {
    }

    public final void onStatusChanged(String str, int i, Bundle bundle) {
    }
}
