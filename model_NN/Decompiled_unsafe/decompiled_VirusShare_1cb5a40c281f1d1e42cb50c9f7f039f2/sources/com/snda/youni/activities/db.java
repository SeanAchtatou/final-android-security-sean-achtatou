package com.snda.youni.activities;

import android.database.Cursor;
import android.view.View;
import android.widget.AdapterView;
import com.snda.youni.mms.ui.a;
import com.snda.youni.mms.ui.k;

final class db implements AdapterView.OnItemLongClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ ChatActivity f278a;

    db(ChatActivity chatActivity) {
        this.f278a = chatActivity;
    }

    public final boolean onItemLongClick(AdapterView adapterView, View view, int i, long j) {
        if (this.f278a.T) {
            return true;
        }
        "OnItemLongClick " + i;
        a unused = this.f278a.ab = ((k) this.f278a.o).a((Cursor) adapterView.getItemAtPosition(i));
        if (this.f278a.ab != null) {
            long unused2 = this.f278a.q = j;
            if (this.f278a.ab.m() && this.f278a.ab.h() == 130) {
                return true;
            }
        }
        return false;
    }
}
