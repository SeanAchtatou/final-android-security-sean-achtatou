package com.snda.youni.activities;

import android.os.Handler;
import android.os.Message;
import android.widget.CursorAdapter;
import com.snda.youni.AppContext;
import com.snda.youni.mms.ui.a;
import com.snda.youni.mms.ui.k;
import com.snda.youni.modules.d.b;

final class bk extends Handler {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ ChatActivity f234a;

    bk(ChatActivity chatActivity) {
        this.f234a = chatActivity;
    }

    public final void handleMessage(Message message) {
        b bVar = (b) message.getData().getSerializable("messageobject");
        switch (message.what) {
            case 0:
                int i = message.arg1;
                "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! update progress " + i;
                if (bVar == null) {
                    return;
                }
                if (!bVar.r()) {
                    "update progress message id = " + bVar.l();
                    if (this.f234a.o != null) {
                        a a2 = ((k) this.f234a.o).a("sms", bVar.l(), null);
                        ((k) this.f234a.o).b.put(Long.valueOf(bVar.l()), Integer.valueOf(i));
                        if (a2 != null) {
                            a2.a(i);
                            "find update progress message id = " + a2.g();
                            ((CursorAdapter) this.f234a.o).notifyDataSetChanged();
                            return;
                        }
                        return;
                    }
                    return;
                }
                long[] p = bVar.p();
                for (int i2 = 0; i2 < p.length; i2++) {
                    ((k) this.f234a.o).b.put(Long.valueOf(p[i2]), Integer.valueOf(i));
                    a a3 = ((k) this.f234a.o).a("sms", p[i2], null);
                    if (a3 != null) {
                        a3.a(i);
                        "find update progress message id = " + a3.g();
                        ((CursorAdapter) this.f234a.o).notifyDataSetChanged();
                    }
                }
                return;
            case 1:
                if (bVar == null) {
                    return;
                }
                if (!bVar.r()) {
                    b d = com.snda.youni.h.a.a.a().d("" + bVar.l());
                    if (d != null) {
                        d.a(com.snda.youni.attachment.b.a.b(AppContext.a(), d.l() + ""));
                        this.f234a.s.a(this.f234a.Z, d);
                        return;
                    }
                    return;
                }
                long[] p2 = bVar.p();
                for (int i3 = 0; i3 < p2.length; i3++) {
                    b d2 = com.snda.youni.h.a.a.a().d("" + p2[i3]);
                    if (d2 != null) {
                        d2.a(com.snda.youni.attachment.b.a.b(AppContext.a(), d2.l() + ""));
                        this.f234a.s.a(this.f234a.Z, d2);
                    }
                }
                return;
            default:
                return;
        }
    }
}
