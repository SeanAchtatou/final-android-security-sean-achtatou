package com.snda.youni.activities;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;
import com.snda.youni.b.ai;
import com.snda.youni.services.YouniService;

final class f implements ServiceConnection {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ NewChatActivity f284a;

    f(NewChatActivity newChatActivity) {
        this.f284a = newChatActivity;
    }

    public final void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        ai unused = this.f284a.A = YouniService.b();
    }

    public final void onServiceDisconnected(ComponentName componentName) {
        ai unused = this.f284a.A = (ai) null;
    }
}
