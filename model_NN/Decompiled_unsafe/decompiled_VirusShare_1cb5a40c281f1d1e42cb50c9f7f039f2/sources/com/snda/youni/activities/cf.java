package com.snda.youni.activities;

import android.content.Intent;
import android.view.View;

final class cf implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ MessageActivityTest f256a;

    cf(MessageActivityTest messageActivityTest) {
        this.f256a = messageActivityTest;
    }

    public final void onClick(View view) {
        Intent intent = new Intent(this.f256a, MessageViewAcitivityTest.class);
        intent.putExtra("number", 10);
        this.f256a.startActivity(intent);
    }
}
