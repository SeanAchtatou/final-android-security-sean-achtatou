package com.snda.youni.activities;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;
import com.snda.youni.C0000R;

final class am extends CursorAdapter {

    /* renamed from: a  reason: collision with root package name */
    private final LayoutInflater f209a;
    private boolean b = false;

    public am(Context context, Cursor cursor, boolean z) {
        super(context, cursor);
        this.f209a = LayoutInflater.from(context);
        this.b = z;
    }

    public final void bindView(View view, Context context, Cursor cursor) {
        TextView textView = (TextView) view.findViewById(C0000R.id.txt_read);
        TextView textView2 = (TextView) view.findViewById(C0000R.id.txt_title);
        cursor.moveToFirst();
        if (!this.b) {
            int i = cursor.getInt(cursor.getColumnIndex("read"));
            String string = cursor.getString(cursor.getColumnIndex("body"));
            textView.setText(i == 0 ? "未读" : "已读");
            String str = string.length() > 30 ? string.substring(0, 30) + "..." : string;
            textView2.setText(str);
            view.setTag(cursor.getString(cursor.getColumnIndex("_id")));
            "bindview " + str;
            return;
        }
        int i2 = cursor.getInt(cursor.getColumnIndex("message_count"));
        int i3 = cursor.getColumnIndex("unread_count") >= 0 ? cursor.getInt(cursor.getColumnIndex("unread_count")) : 0;
        String string2 = cursor.getString(cursor.getColumnIndex("snippet"));
        textView.setText("" + i2 + " " + i3);
        String str2 = (string2 == null || string2.length() <= 30) ? string2 : string2.substring(0, 30) + "...";
        textView2.setText(str2);
        view.setTag(cursor.getString(cursor.getColumnIndex("_id")));
        "bindview " + str2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public final View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
        return this.f209a.inflate((int) C0000R.layout.message_item, viewGroup, false);
    }
}
