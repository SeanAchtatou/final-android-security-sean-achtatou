package com.snda.youni.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SeekBar;
import android.widget.TextView;
import com.snda.youni.AppContext;
import com.snda.youni.C0000R;
import com.snda.youni.e.s;
import com.snda.youni.g.a;
import com.snda.youni.modules.settings.SettingsItemView;
import com.snda.youni.modules.settings.e;
import com.snda.youni.modules.settings.l;
import com.snda.youni.modules.settings.m;
import com.snda.youni.modules.settings.o;
import com.snda.youni.modules.settings.p;
import com.snda.youni.modules.settings.q;
import com.snda.youni.modules.settings.r;
import com.snda.youni.modules.settings.t;
import com.snda.youni.modules.settings.u;
import com.snda.youni.modules.settings.x;
import com.snda.youni.modules.settings.y;
import java.util.HashMap;
import java.util.Map;

public class SettingsMessageActivity extends Activity implements View.OnClickListener, SeekBar.OnSeekBarChangeListener {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public int f192a;
    private int b;
    /* access modifiers changed from: private */
    public Map c;
    /* access modifiers changed from: private */
    public TextView d;

    private SettingsItemView a(int i) {
        SettingsItemView settingsItemView = (SettingsItemView) findViewById(i);
        settingsItemView.setOnClickListener(this);
        return settingsItemView;
    }

    public void onClick(View view) {
        y yVar = (y) this.c.get(Integer.valueOf(view.getId()));
        if (yVar instanceof m) {
            m mVar = (m) yVar;
            this.f192a = mVar.a();
            new AlertDialog.Builder(this).setTitle(mVar.a_()).setSingleChoiceItems(mVar.d(), mVar.a(), new k(this, mVar)).setPositiveButton((int) C0000R.string.alert_dialog_ok, new j(this, mVar)).setNegativeButton((int) C0000R.string.alert_dialog_cancel, (DialogInterface.OnClickListener) null).show();
        } else if (yVar instanceof x) {
            ((x) yVar).a();
            String f = yVar.f();
            if ("notify_name".equalsIgnoreCase(f)) {
                a.b(this, "notif_status", "" + ((x) yVar).d());
            } else if ("mutable_name".equalsIgnoreCase(f)) {
                ((r) this.c.get(Integer.valueOf((int) C0000R.id.sound))).a(((x) yVar).b());
            }
        } else if (yVar instanceof r) {
            ((r) yVar).a();
        } else if (yVar instanceof u) {
            showDialog(0);
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        this.c = new HashMap();
        this.b = getIntent().getIntExtra("type", 0);
        if (this.b == 0) {
            setContentView((int) C0000R.layout.activity_settings_message);
            t tVar = new t(a((int) C0000R.id.shape));
            l lVar = new l(a((int) C0000R.id.color), tVar);
            tVar.a(lVar);
            p pVar = new p(a((int) C0000R.id.imagesize));
            this.c.put(Integer.valueOf((int) C0000R.id.shape), tVar);
            this.c.put(Integer.valueOf((int) C0000R.id.color), lVar);
            this.c.put(Integer.valueOf((int) C0000R.id.size), new e(a((int) C0000R.id.size)));
            this.c.put(Integer.valueOf((int) C0000R.id.imagesize), pVar);
        } else {
            setContentView((int) C0000R.layout.activity_settings_remind);
            this.c.put(Integer.valueOf((int) C0000R.id.notify), new x(a((int) C0000R.id.notify), "notify_name"));
            this.c.put(Integer.valueOf((int) C0000R.id.popup), new q(a((int) C0000R.id.popup)));
            this.c.put(Integer.valueOf((int) C0000R.id.vib), new o(a((int) C0000R.id.vib)));
            this.c.put(Integer.valueOf((int) C0000R.id.vibrator_number), new u(a((int) C0000R.id.vibrator_number), "vibrator_num"));
            this.c.put(Integer.valueOf((int) C0000R.id.mutable), new x(a((int) C0000R.id.mutable), "mutable_name"));
            this.c.put(Integer.valueOf((int) C0000R.id.sound), new r(a((int) C0000R.id.sound), SettingsSoundActivity.class));
        }
        findViewById(C0000R.id.back).setOnClickListener(new i(this));
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int i) {
        switch (i) {
            case 0:
                View inflate = getLayoutInflater().inflate((int) C0000R.layout.popup_settings_vibrator, (ViewGroup) null);
                SharedPreferences defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
                SeekBar seekBar = (SeekBar) inflate.findViewById(C0000R.id.settings_vibrator_seekBar);
                seekBar.setProgress(defaultSharedPreferences.getInt("vibrator_num", 2) - 1);
                seekBar.setOnSeekBarChangeListener(this);
                this.d = (TextView) inflate.findViewById(C0000R.id.settings_vibrator_number);
                this.d.setText("" + defaultSharedPreferences.getInt("vibrator_num", 2));
                return new AlertDialog.Builder(this).setTitle(getString(C0000R.string.settings_message_vib_popup)).setView(inflate).setPositiveButton((int) C0000R.string.alert_dialog_ok, new m(this, seekBar)).setNegativeButton((int) C0000R.string.alert_dialog_cancel, new l(this, defaultSharedPreferences)).create();
            default:
                return super.onCreateDialog(i);
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        Object[] array = this.c.keySet().toArray();
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < array.length) {
                ((y) this.c.get(array[i2])).e();
                i = i2 + 1;
            } else {
                s.k = 7;
                AppContext.b(this);
                return;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        s.k = 1;
    }

    public void onProgressChanged(SeekBar seekBar, int i, boolean z) {
        this.d.setText("" + (i + 1));
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        String str;
        super.onResume();
        if (this.b == 1) {
            SharedPreferences defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
            int i = defaultSharedPreferences.getInt("sound_type_name", 0);
            String string = getString(C0000R.string.settings_message_subtitle_currentring);
            switch (i) {
                case 0:
                    TypedArray obtainTypedArray = getResources().obtainTypedArray(C0000R.array.sound_details);
                    obtainTypedArray.recycle();
                    str = string + obtainTypedArray.getString(defaultSharedPreferences.getInt("sound_youni_name", 0));
                    break;
                case 1:
                    str = string + defaultSharedPreferences.getString("sound_title_name", "");
                    break;
                case 2:
                    str = string + defaultSharedPreferences.getString("sound_title_name", "");
                    break;
                default:
                    str = string;
                    break;
            }
            ((SettingsItemView) findViewById(C0000R.id.sound)).c(str);
            ((SettingsItemView) findViewById(C0000R.id.sound)).setClickable(((x) this.c.get(Integer.valueOf((int) C0000R.id.mutable))).b());
        }
        s.k = 0;
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        s.k = 1;
        AppContext.a(this);
        findViewById(C0000R.id.windows).setBackgroundDrawable(com.snda.youni.d.a.a("bg_set"));
        findViewById(C0000R.id.tab_title).setBackgroundDrawable(com.snda.youni.d.a.a("bg_tab_title"));
        findViewById(C0000R.id.back).setBackgroundDrawable(com.snda.youni.d.a.a("btn_return"));
        for (Integer intValue : this.c.keySet()) {
            a(intValue.intValue()).setBackgroundDrawable(com.snda.youni.d.a.a("bg_list_item_center"));
        }
    }

    public void onStartTrackingTouch(SeekBar seekBar) {
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        s.k = 3;
    }

    public void onStopTrackingTouch(SeekBar seekBar) {
    }
}
