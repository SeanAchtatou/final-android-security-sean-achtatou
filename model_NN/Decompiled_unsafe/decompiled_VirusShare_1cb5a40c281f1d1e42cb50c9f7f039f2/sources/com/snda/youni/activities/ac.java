package com.snda.youni.activities;

import android.content.DialogInterface;

final class ac implements DialogInterface.OnDismissListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ bw f199a;

    ac(bw bwVar) {
        this.f199a = bwVar;
    }

    public final void onDismiss(DialogInterface dialogInterface) {
        if (this.f199a.f246a != null) {
            bw.b(this.f199a);
        }
        this.f199a.e.sendToTarget();
    }
}
