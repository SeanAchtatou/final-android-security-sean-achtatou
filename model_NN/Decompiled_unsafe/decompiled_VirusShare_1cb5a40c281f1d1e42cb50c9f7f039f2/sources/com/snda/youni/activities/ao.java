package com.snda.youni.activities;

import android.widget.RadioGroup;
import android.widget.Toast;
import com.snda.youni.C0000R;
import com.snda.youni.e.r;
import com.snda.youni.g.a;

final class ao implements RadioGroup.OnCheckedChangeListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ ChatActivity f211a;

    ao(ChatActivity chatActivity) {
        this.f211a = chatActivity;
    }

    public final void onCheckedChanged(RadioGroup radioGroup, int i) {
        "RadioGroup's checkedId is: " + i;
        switch (i) {
            case C0000R.id.btn_notification /*2131558459*/:
                Toast.makeText(this.f211a, (int) C0000R.string.new_activity, 1).show();
                ChatActivity.q(this.f211a);
                this.f211a.A.setChecked(false);
                return;
            case C0000R.id.btn_invite /*2131558460*/:
                this.f211a.showDialog(2);
                this.f211a.y.setChecked(false);
                return;
            case C0000R.id.btn_phone /*2131558461*/:
                a.a(this.f211a.getApplicationContext(), "chat_call", null);
                if (this.f211a.X.n()) {
                    r.a(this.f211a.getString(C0000R.string.snda_services_phone_number), this.f211a);
                } else {
                    r.a(this.f211a.X.a(), this.f211a.getApplicationContext());
                }
                this.f211a.z.setChecked(false);
                return;
            default:
                this.f211a.showDialog(2);
                return;
        }
    }
}
