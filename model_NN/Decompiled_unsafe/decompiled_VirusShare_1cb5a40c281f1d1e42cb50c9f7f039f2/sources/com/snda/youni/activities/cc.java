package com.snda.youni.activities;

import android.content.ContentUris;
import android.content.Intent;
import android.database.Cursor;
import android.os.Handler;
import android.os.Message;
import android.widget.TextView;
import android.widget.Toast;
import com.snda.youni.C0000R;
import com.snda.youni.e.j;
import com.snda.youni.modules.a.b;
import com.snda.youni.modules.a.k;
import com.snda.youni.providers.f;

final class cc extends Handler {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ FavoriteActivity f253a;

    cc(FavoriteActivity favoriteActivity) {
        this.f253a = favoriteActivity;
    }

    public final void handleMessage(Message message) {
        switch (message.what) {
            case 0:
                Cursor query = this.f253a.getContentResolver().query(ContentUris.withAppendedId(f.f594a, Long.parseLong(message.getData().getString("id"))), null, null, null, null);
                if (query == null || !query.moveToNext()) {
                    Toast.makeText(this.f253a, (int) C0000R.string.favorite_reply_failed, 0).show();
                    return;
                }
                String string = query.getString(query.getColumnIndex("message_receiver"));
                j.a(string);
                Intent intent = new Intent(this.f253a, ChatActivity.class);
                k kVar = new k();
                kVar.a(this.f253a, new b(this.f253a), string);
                intent.putExtra("item", kVar);
                intent.addFlags(805306368);
                this.f253a.startActivity(intent);
                return;
            case 1:
                Cursor query2 = this.f253a.getContentResolver().query(ContentUris.withAppendedId(f.f594a, Long.parseLong(message.getData().getString("id"))), null, null, null, null);
                if (query2 == null || !query2.moveToNext()) {
                    Toast.makeText(this.f253a, (int) C0000R.string.favorite_forward_failed, 0).show();
                    return;
                }
                String string2 = query2.getString(query2.getColumnIndex("message_body"));
                String string3 = query2.getString(query2.getColumnIndex("message_body"));
                Intent intent2 = new Intent(this.f253a, RecipientsActivity.class);
                intent2.putExtra("message_body", string2);
                intent2.putExtra("message_subject", string3);
                this.f253a.startActivity(intent2);
                return;
            case 2:
                this.f253a.getContentResolver().delete(ContentUris.withAppendedId(f.f594a, Long.parseLong(message.getData().getString("id"))), null, null);
                this.f253a.i.requery();
                ((TextView) this.f253a.findViewById(C0000R.id.favorite_total_number)).setText(this.f253a.getString(C0000R.string.favorite_total_number, new Object[]{Integer.valueOf(this.f253a.i.getCount())}));
                return;
            case 3:
                int height = this.f253a.f.getHeight();
                int top = this.f253a.f.getTop();
                int bottom = this.f253a.g.getBottom() - this.f253a.g.getTop();
                if (top + height > bottom) {
                    this.f253a.g.setSelectionFromTop(message.arg1, bottom - height);
                    return;
                }
                return;
            default:
                return;
        }
    }
}
