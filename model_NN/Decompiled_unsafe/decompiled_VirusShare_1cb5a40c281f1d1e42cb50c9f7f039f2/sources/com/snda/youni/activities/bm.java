package com.snda.youni.activities;

import android.net.Uri;
import com.snda.youni.attachment.b.b;

final class bm implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ChatActivity f236a;
    private /* synthetic */ Uri b;
    private /* synthetic */ String c;

    bm(ChatActivity chatActivity, Uri uri, String str) {
        this.f236a = chatActivity;
        this.b = uri;
        this.c = str;
    }

    public final void run() {
        b unused = this.f236a.W = this.f236a.a(this.b);
        if (this.f236a.W == null) {
            this.f236a.runOnUiThread(new an(this));
        } else {
            this.f236a.a(this.c);
        }
    }
}
