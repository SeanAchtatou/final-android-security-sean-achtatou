package com.snda.youni.activities;

import android.content.Context;
import android.widget.AbsListView;

final class dc implements AbsListView.OnScrollListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ ChatActivity f279a;

    dc(ChatActivity chatActivity) {
        this.f279a = chatActivity;
    }

    public final void onScroll(AbsListView absListView, int i, int i2, int i3) {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.snda.youni.activities.ChatActivity.a(com.snda.youni.activities.ChatActivity, boolean):void
     arg types: [com.snda.youni.activities.ChatActivity, int]
     candidates:
      com.snda.youni.activities.ChatActivity.a(com.snda.youni.activities.ChatActivity, long):long
      com.snda.youni.activities.ChatActivity.a(com.snda.youni.activities.ChatActivity, java.lang.String[]):long
      com.snda.youni.activities.ChatActivity.a(int, java.lang.String):com.snda.youni.attachment.b.b
      com.snda.youni.activities.ChatActivity.a(com.snda.youni.activities.ChatActivity, android.net.Uri):com.snda.youni.attachment.b.b
      com.snda.youni.activities.ChatActivity.a(com.snda.youni.activities.ChatActivity, com.snda.youni.attachment.b.b):com.snda.youni.attachment.b.b
      com.snda.youni.activities.ChatActivity.a(com.snda.youni.activities.ChatActivity, com.snda.youni.b.ai):com.snda.youni.b.ai
      com.snda.youni.activities.ChatActivity.a(com.snda.youni.activities.ChatActivity, com.snda.youni.mms.ui.a):com.snda.youni.mms.ui.a
      com.snda.youni.activities.ChatActivity.a(com.snda.youni.activities.ChatActivity, int):void
      com.snda.youni.activities.ChatActivity.a(com.snda.youni.activities.ChatActivity, java.lang.String):void
      com.snda.youni.activities.ChatActivity.a(com.snda.youni.activities.ChatActivity, boolean):void */
    public final void onScrollStateChanged(AbsListView absListView, int i) {
        if (i == 1 || i == 2) {
            this.f279a.d(false);
            this.f279a.r.a((Context) this.f279a);
        }
    }
}
