package com.snda.youni.activities;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.widget.Toast;
import com.snda.youni.C0000R;
import com.snda.youni.e.o;
import com.snda.youni.g.a;

final class bi extends Handler {

    /* renamed from: a  reason: collision with root package name */
    private String f232a;
    private /* synthetic */ ChatActivity b;

    bi(ChatActivity chatActivity) {
        this.b = chatActivity;
    }

    public final void handleMessage(Message message) {
        switch (message.what) {
            case 2:
                this.f232a = "attachment";
                return;
            case 10:
                this.b.k();
                if (!o.a() || this.b.T) {
                    if (!o.a()) {
                        Toast.makeText(this.b, (int) C0000R.string.sdcard_not_working, 0).show();
                    }
                } else if (this.b.E.getVisibility() == 8) {
                    if (message.arg1 == this.b.n.getLastVisiblePosition()) {
                        this.b.f184a++;
                    }
                    this.b.E.setBackgroundResource(C0000R.drawable.btn_bottom_record_audio_normal);
                    this.b.E.setVisibility(0);
                    this.b.l();
                }
                a.a(this.b.getApplicationContext(), "play_voice", null);
                return;
            case 11:
                this.b.r.a((Context) this.b);
                return;
            default:
                return;
        }
    }
}
