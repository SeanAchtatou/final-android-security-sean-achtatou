package com.snda.youni.activities;

import android.app.Activity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.widget.Button;
import android.widget.TextView;
import com.snda.youni.C0000R;
import com.snda.youni.a.m;

public class UpdateRefActivity extends Activity {
    /* access modifiers changed from: private */
    public static co b;
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public m f195a;

    public static void a(co coVar) {
        b = coVar;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        setContentView((int) C0000R.layout.update_ref);
        this.f195a = (m) getIntent().getSerializableExtra("update_info");
        String string = getString(C0000R.string.update_prefix);
        String string2 = getString(C0000R.string.update_prefix_force);
        String string3 = getString(C0000R.string.update_version);
        StringBuilder sb = new StringBuilder();
        if (this.f195a.b) {
            sb.append(string2);
            ((Button) findViewById(C0000R.id.cancel)).setText((int) C0000R.string.alert_dialog_exit);
        } else {
            sb.append(string);
        }
        ((TextView) findViewById(C0000R.id.title)).setText(sb);
        ((TextView) findViewById(C0000R.id.body)).setText(string3 + this.f195a.d + 10 + this.f195a.f);
        findViewById(C0000R.id.ok).setOnClickListener(new bx(this));
        findViewById(C0000R.id.cancel).setOnClickListener(new by(this));
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        return true;
    }
}
