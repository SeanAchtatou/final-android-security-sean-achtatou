package com.snda.youni.activities;

import android.app.Activity;
import android.content.ContentValues;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.TextView;
import com.snda.youni.AppContext;
import com.snda.youni.C0000R;
import com.snda.youni.d;
import com.snda.youni.e.j;
import com.snda.youni.e.s;
import com.snda.youni.favorite.FavoriteListItem;
import com.snda.youni.favorite.a;
import com.snda.youni.providers.f;
import com.snda.youni.providers.n;

public class FavoriteActivity extends Activity implements AdapterView.OnItemClickListener {

    /* renamed from: a  reason: collision with root package name */
    private long f185a = 0;
    private int b = 0;
    private View c = null;
    private View d = null;
    private d e;
    /* access modifiers changed from: private */
    public View f = null;
    /* access modifiers changed from: private */
    public ListView g;
    private Handler h = new cc(this);
    /* access modifiers changed from: private */
    public Cursor i;
    private a j;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        Cursor query;
        super.onCreate(bundle);
        setContentView(C0000R.layout.activity_favorite);
        this.g = (ListView) findViewById(C0000R.id.favorite_list);
        if (!(getIntent().getData() == null || (query = getContentResolver().query(getIntent().getData(), new String[]{"_id", "body", "date", "subject", "address", "person"}, null, null, null)) == null || !query.moveToNext())) {
            long j2 = query.getLong(0);
            String string = query.getString(4);
            String string2 = query.getString(1);
            String string3 = query.getString(3);
            long j3 = query.getLong(2);
            query.getLong(5);
            int intExtra = getIntent().getIntExtra("box_type", 0);
            Cursor query2 = getContentResolver().query(n.f597a, new String[]{"contact_id", "sid"}, "sid='" + j.a(string) + "'", null, null);
            int i2 = 0;
            if (query2 != null && query2.moveToNext()) {
                i2 = query2.getInt(0);
            }
            query2.close();
            query.close();
            ContentValues contentValues = new ContentValues();
            contentValues.put("message_id", Long.valueOf(j2));
            contentValues.put("message_body", string2);
            contentValues.put("message_receiver", string);
            contentValues.put("message_box", Integer.valueOf(intExtra));
            contentValues.put("message_date", Long.valueOf(j3));
            contentValues.put("message_subject", string3);
            contentValues.put("contactid", Integer.valueOf(i2));
            "save favorite contactid=============" + i2;
            getContentResolver().insert(f.f594a, contentValues);
        }
        this.e = new d(this);
        this.i = managedQuery(f.f594a, null, null, null, null);
        this.j = new a(this, this.i, this.e);
        this.j.a(this.h);
        ((ListView) findViewById(C0000R.id.favorite_list)).setAdapter((ListAdapter) this.j);
        ((ListView) findViewById(C0000R.id.favorite_list)).setOnItemClickListener(this);
        ((RadioGroup) findViewById(C0000R.id.group_favorite_btns)).setOnCheckedChangeListener(new cb(this));
        ((TextView) findViewById(C0000R.id.favorite_total_number)).setText(getString(C0000R.string.favorite_total_number, new Object[]{Integer.valueOf(this.i.getCount())}));
        if (this.i != null && this.i.getCount() != 0) {
            ((TextView) findViewById(C0000R.id.list_hint_empty)).setVisibility(8);
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        this.e.a();
        s.h = 7;
        AppContext.b(this);
    }

    public void onItemClick(AdapterView adapterView, View view, int i2, long j2) {
        "OnItemClick " + i2;
        View findViewById = view.findViewById(C0000R.id.favorite_item_btns);
        if (findViewById.getVisibility() == 8) {
            findViewById.setVisibility(0);
            ((FavoriteListItem) view).b();
        } else {
            findViewById.setVisibility(8);
            ((FavoriteListItem) view).a();
        }
        if (!(this.c == null || j2 == this.f185a)) {
            this.c.findViewById(C0000R.id.favorite_item_btns).setVisibility(8);
            ((FavoriteListItem) this.c).a();
        }
        this.f185a = j2;
        this.b = i2;
        this.c = view;
        Message message = new Message();
        message.arg1 = i2;
        message.what = 3;
        this.h.sendMessageDelayed(message, 300);
        this.f = view;
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.e.b();
        s.h = 1;
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.e.c();
        s.h = 0;
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        s.h = 1;
        AppContext.a(this);
        findViewById(C0000R.id.windows).setBackgroundDrawable(com.snda.youni.d.a.a("bg_set"));
        findViewById(C0000R.id.tab_title).setBackgroundDrawable(com.snda.youni.d.a.a("bg_tab_title"));
        findViewById(C0000R.id.btn_back).setBackgroundDrawable(com.snda.youni.d.a.a("btn_return"));
        this.g.setSelector(com.snda.youni.d.a.a("bg_list_item"));
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        s.h = 3;
    }
}
