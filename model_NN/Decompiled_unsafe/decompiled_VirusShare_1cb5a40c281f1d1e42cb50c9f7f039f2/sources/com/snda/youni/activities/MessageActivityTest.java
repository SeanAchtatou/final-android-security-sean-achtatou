package com.snda.youni.activities;

import android.app.Activity;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.widget.Button;
import com.snda.youni.C0000R;
import com.snda.youni.b.ai;
import com.snda.youni.h.a.a;
import com.snda.youni.services.YouniService;

public class MessageActivityTest extends Activity {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public a f186a;
    /* access modifiers changed from: private */
    public ai b = null;
    private ServiceConnection c = new ck(this);

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) C0000R.layout.message);
        this.f186a = new a(getApplicationContext());
        bindService(new Intent(this, YouniService.class), this.c, 1);
        ((Button) findViewById(C0000R.id.btn_insert)).setOnClickListener(new ci(this));
        ((Button) findViewById(C0000R.id.btn_update)).setOnClickListener(new cj(this));
        ((Button) findViewById(C0000R.id.btn_send_sms)).setOnClickListener(new cg(this));
        ((Button) findViewById(C0000R.id.btn_send_youni)).setOnClickListener(new ch(this));
        ((Button) findViewById(C0000R.id.btn_query_all)).setOnClickListener(new ce(this));
        ((Button) findViewById(C0000R.id.btn_query10)).setOnClickListener(new cf(this));
        ((Button) findViewById(C0000R.id.btn_query_sms_count)).setOnClickListener(new cl(this));
        ((Button) findViewById(C0000R.id.btn_query_youni_count)).setOnClickListener(new cm(this));
        ((Button) findViewById(C0000R.id.btn_query_unread_count)).setOnClickListener(new cs(this));
        ((Button) findViewById(C0000R.id.btn_query_all_count)).setOnClickListener(new ct(this));
    }
}
