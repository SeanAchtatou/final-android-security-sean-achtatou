package com.snda.youni.activities;

import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import com.snda.youni.C0000R;
import com.snda.youni.modules.d.b;
import java.util.Date;

final class cg implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ MessageActivityTest f257a;

    cg(MessageActivityTest messageActivityTest) {
        this.f257a = messageActivityTest;
    }

    public final void onClick(View view) {
        b bVar = new b();
        String obj = ((EditText) this.f257a.findViewById(C0000R.id.txt_insert)).getText().toString();
        bVar.a(((EditText) this.f257a.findViewById(C0000R.id.txt_number)).getText().toString());
        long currentTimeMillis = System.currentTimeMillis();
        bVar.b(new Date() + obj);
        bVar.c("test");
        bVar.a(Long.valueOf(currentTimeMillis));
        bVar.e("sms");
        this.f257a.f186a.b(bVar);
        Toast.makeText(this.f257a, "send sms over!", 1).show();
    }
}
