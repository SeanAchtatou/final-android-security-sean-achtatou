package com.snda.youni.activities;

import android.os.Handler;
import android.os.Message;

final class cy extends Handler {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ ChatActivity f274a;

    cy(ChatActivity chatActivity) {
        this.f274a = chatActivity;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.snda.youni.activities.ChatActivity.a(com.snda.youni.activities.ChatActivity, boolean):void
     arg types: [com.snda.youni.activities.ChatActivity, int]
     candidates:
      com.snda.youni.activities.ChatActivity.a(com.snda.youni.activities.ChatActivity, long):long
      com.snda.youni.activities.ChatActivity.a(com.snda.youni.activities.ChatActivity, java.lang.String[]):long
      com.snda.youni.activities.ChatActivity.a(int, java.lang.String):com.snda.youni.attachment.b.b
      com.snda.youni.activities.ChatActivity.a(com.snda.youni.activities.ChatActivity, android.net.Uri):com.snda.youni.attachment.b.b
      com.snda.youni.activities.ChatActivity.a(com.snda.youni.activities.ChatActivity, com.snda.youni.attachment.b.b):com.snda.youni.attachment.b.b
      com.snda.youni.activities.ChatActivity.a(com.snda.youni.activities.ChatActivity, com.snda.youni.b.ai):com.snda.youni.b.ai
      com.snda.youni.activities.ChatActivity.a(com.snda.youni.activities.ChatActivity, com.snda.youni.mms.ui.a):com.snda.youni.mms.ui.a
      com.snda.youni.activities.ChatActivity.a(com.snda.youni.activities.ChatActivity, int):void
      com.snda.youni.activities.ChatActivity.a(com.snda.youni.activities.ChatActivity, java.lang.String):void
      com.snda.youni.activities.ChatActivity.a(com.snda.youni.activities.ChatActivity, boolean):void */
    public final void handleMessage(Message message) {
        if (message == null) {
            return;
        }
        if (message.what == 1) {
            this.f274a.d(true);
        } else {
            this.f274a.d(false);
        }
    }
}
