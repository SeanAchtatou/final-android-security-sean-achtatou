package com.snda.youni.activities;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.widget.TextView;
import com.snda.youni.AppContext;
import com.snda.youni.C0000R;
import com.snda.youni.d.a;
import com.snda.youni.e.s;
import com.snda.youni.modules.c.b;

public class SettingsCountActivity extends Activity {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        setContentView((int) C0000R.layout.activity_settings_count);
        findViewById(C0000R.id.back).setOnClickListener(new ba(this));
        SharedPreferences defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        int[] a2 = b.a(defaultSharedPreferences);
        int[] b = b.b(defaultSharedPreferences);
        TextView textView = (TextView) findViewById(C0000R.id.count_send_youni);
        TextView textView2 = (TextView) findViewById(C0000R.id.count_all_money);
        TextView textView3 = (TextView) findViewById(C0000R.id.count_send_sms);
        TextView textView4 = (TextView) findViewById(C0000R.id.count_send_month_all);
        TextView textView5 = (TextView) findViewById(C0000R.id.count_send_month_youni);
        TextView textView6 = (TextView) findViewById(C0000R.id.count_month_money);
        TextView textView7 = (TextView) findViewById(C0000R.id.count_send_month_sms);
        ((TextView) findViewById(C0000R.id.count_send_all)).setText(getString(C0000R.string.count_send_all_msg, new Object[]{Integer.valueOf(a2[0])}));
        if (a2[1] < 10) {
            textView.setVisibility(8);
            textView2.setVisibility(8);
        } else {
            if (textView.getVisibility() != 0) {
                textView.setVisibility(0);
                textView2.setVisibility(0);
            }
            textView.setText(getString(C0000R.string.count_send_youni_msg, new Object[]{Integer.valueOf(a2[1])}));
            textView2.setText(getString(C0000R.string.count_all_money, new Object[]{(a2[1] / 10) + (a2[1] % 10 == 0 ? "" : "." + (a2[1] % 10))}));
        }
        textView3.setText(getString(C0000R.string.count_send_sms_msg, new Object[]{Integer.valueOf(a2[0] - a2[1])}));
        textView4.setText(getString(C0000R.string.count_send_all_msg, new Object[]{Integer.valueOf(b[0])}));
        if (b[1] < 10) {
            textView5.setVisibility(8);
            textView6.setVisibility(8);
        } else {
            if (textView5.getVisibility() != 0) {
                textView5.setVisibility(0);
                textView6.setVisibility(0);
            }
            textView5.setText(getString(C0000R.string.count_send_youni_msg, new Object[]{Integer.valueOf(b[1])}));
            textView6.setText(getString(C0000R.string.count_month_money, new Object[]{(b[1] / 10) + (b[1] % 10 == 0 ? "" : "." + (b[1] % 10))}));
        }
        textView7.setText(getString(C0000R.string.count_send_sms_msg, new Object[]{Integer.valueOf(b[0] - b[1])}));
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        s.n = 7;
        AppContext.b(this);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        s.n = 1;
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        s.n = 0;
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        s.n = 1;
        AppContext.a(this);
        findViewById(C0000R.id.windows).setBackgroundDrawable(a.a("bg_set"));
        findViewById(C0000R.id.tab_title).setBackgroundDrawable(a.a("bg_tab_title"));
        findViewById(C0000R.id.back).setBackgroundDrawable(a.a("btn_return"));
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        s.n = 3;
    }
}
