package com.snda.youni.activities;

import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import com.snda.youni.C0000R;

final class cm implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ MessageActivityTest f263a;

    cm(MessageActivityTest messageActivityTest) {
        this.f263a = messageActivityTest;
    }

    public final void onClick(View view) {
        String obj = ((EditText) this.f263a.findViewById(C0000R.id.txt_number)).getText().toString();
        Toast.makeText(this.f263a, "number:" + obj + " has " + this.f263a.f186a.b(this.f263a.f186a.e(obj)) + " youni messages!", 1).show();
    }
}
