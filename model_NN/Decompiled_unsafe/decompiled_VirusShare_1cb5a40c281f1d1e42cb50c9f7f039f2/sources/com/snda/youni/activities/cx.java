package com.snda.youni.activities;

import android.os.Handler;
import android.os.Message;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import com.snda.youni.C0000R;

final class cx extends Handler {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ChatActivity f273a;

    cx(ChatActivity chatActivity) {
        this.f273a = chatActivity;
    }

    public final void handleMessage(Message message) {
        if (message == null) {
            return;
        }
        if (message.what != 1 || this.f273a.X.r()) {
            this.f273a.C.setVisibility(8);
            return;
        }
        this.f273a.C.setVisibility(0);
        Animation loadAnimation = AnimationUtils.loadAnimation(this.f273a.getApplicationContext(), C0000R.anim.inputting_tip);
        loadAnimation.setDuration(10000);
        loadAnimation.setAnimationListener(new aj(this));
        if (!this.f273a.S) {
            this.f273a.C.setAnimation(loadAnimation);
        }
    }
}
