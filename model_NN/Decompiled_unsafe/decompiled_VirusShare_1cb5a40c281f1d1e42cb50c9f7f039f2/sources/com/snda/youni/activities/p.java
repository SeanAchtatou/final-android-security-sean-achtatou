package com.snda.youni.activities;

import android.os.AsyncTask;
import com.snda.youni.g.a;
import com.snda.youni.modules.d.b;
import com.snda.youni.services.YouniService;
import java.util.List;

final class p extends AsyncTask {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ NewChatActivity f294a;

    /* synthetic */ p(NewChatActivity newChatActivity) {
        this(newChatActivity, (byte) 0);
    }

    private p(NewChatActivity newChatActivity, byte b) {
        this.f294a = newChatActivity;
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public Void doInBackground(List... listArr) {
        List<b> list = listArr[0];
        if (list != null && list.size() > 1) {
            YouniService.a(list.size());
            a.a(this.f294a.getApplicationContext(), "group_message", null);
        }
        for (b bVar : list) {
            this.f294a.a(true);
            this.f294a.e.b(this.f294a.A, bVar);
            if (bVar != null) {
                this.f294a.e.a((String) null);
            }
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
            }
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void onPostExecute(Object obj) {
        Void voidR = (Void) obj;
        try {
            if (this.f294a.l != null) {
                this.f294a.l.dismiss();
            }
        } catch (Exception e) {
            e.getMessage();
        }
        super.onPostExecute(voidR);
        this.f294a.finish();
    }
}
