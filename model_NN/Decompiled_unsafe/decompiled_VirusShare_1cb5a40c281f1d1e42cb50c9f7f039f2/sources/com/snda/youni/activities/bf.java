package com.snda.youni.activities;

import android.net.Uri;
import com.snda.youni.attachment.b.b;
import com.snda.youni.attachment.e;
import java.io.File;

final class bf implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ChatActivity f229a;
    private /* synthetic */ String b;

    bf(ChatActivity chatActivity, String str) {
        this.f229a = chatActivity;
        this.b = str;
    }

    public final void run() {
        b unused = this.f229a.W = this.f229a.a(Uri.fromFile(new File(e.g, "youni_camera.jpg")));
        if (this.f229a.W == null) {
            this.f229a.runOnUiThread(new w(this));
        } else {
            this.f229a.a(this.b);
        }
    }
}
