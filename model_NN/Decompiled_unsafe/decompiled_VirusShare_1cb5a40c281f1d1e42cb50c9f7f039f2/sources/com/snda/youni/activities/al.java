package com.snda.youni.activities;

import android.content.AsyncQueryHandler;
import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import android.widget.CursorAdapter;
import com.snda.youni.modules.a.d;

final class al extends AsyncQueryHandler {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ ChatActivity f208a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public al(ChatActivity chatActivity, ContentResolver contentResolver) {
        super(contentResolver);
        this.f208a = chatActivity;
    }

    /* access modifiers changed from: protected */
    public final void onDeleteComplete(int i, Object obj, int i2) {
        switch (i) {
            case 1:
                "onDelete: delete " + i2 + " messages";
                if (this.f208a.p != null) {
                    this.f208a.p.dismiss();
                }
                if (this.f208a.u != null) {
                    this.f208a.u.a((d) null);
                }
                long unused = this.f208a.m = this.f208a.a(new String[]{this.f208a.X.a()});
                Cursor a2 = this.f208a.s.a(this.f208a.m);
                if (!(a2 == null || this.f208a.o == null)) {
                    ((CursorAdapter) this.f208a.o).changeCursor(a2);
                }
                "after send currentThreadId:" + this.f208a.m;
                return;
            case 2:
                this.f208a.k();
                this.f208a.U.startDelete(3, null, Uri.parse("content://sms/"), (String) obj, null);
                return;
            case 3:
                this.f208a.k();
                if (this.f208a.p != null) {
                    this.f208a.p.dismiss();
                }
                this.f208a.e(false);
                return;
            default:
                return;
        }
    }
}
