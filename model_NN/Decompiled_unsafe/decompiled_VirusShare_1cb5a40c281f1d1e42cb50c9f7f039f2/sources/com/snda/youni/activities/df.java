package com.snda.youni.activities;

import android.content.AsyncQueryHandler;
import android.content.ContentResolver;
import android.database.Cursor;

final class df extends AsyncQueryHandler {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ NewChatActivity f282a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public df(NewChatActivity newChatActivity, ContentResolver contentResolver) {
        super(contentResolver);
        this.f282a = newChatActivity;
    }

    /* access modifiers changed from: protected */
    public final void onQueryComplete(int i, Object obj, Cursor cursor) {
        if (cursor != null) {
            "cursor's size is: " + cursor.getCount();
        }
        this.f282a.f188a.changeCursor(cursor);
    }
}
