package com.snda.youni.activities;

import android.view.View;

final class bn implements View.OnFocusChangeListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ RecipientsActivity f237a;

    bn(RecipientsActivity recipientsActivity) {
        this.f237a = recipientsActivity;
    }

    public final void onFocusChange(View view, boolean z) {
        if (z) {
            this.f237a.d.setVisibility(0);
            this.f237a.f.setVisibility(8);
            this.f237a.c.setVisibility(0);
        }
    }
}
