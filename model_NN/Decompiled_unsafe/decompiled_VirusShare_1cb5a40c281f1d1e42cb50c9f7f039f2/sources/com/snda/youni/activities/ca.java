package com.snda.youni.activities;

import android.os.CountDownTimer;
import android.widget.TextView;
import com.snda.youni.C0000R;

final class ca extends CountDownTimer {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ bw f251a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    ca(bw bwVar) {
        super(60000, 200);
        this.f251a = bwVar;
    }

    public final void onFinish() {
        ((TextView) this.f251a.findViewById(C0000R.id.record_sound_timer)).setText((int) C0000R.string.audio_record_finish);
        try {
            this.f251a.findViewById(C0000R.id.volume_level_indicator).scrollTo(0, -160);
            int unused = this.f251a.b = 60;
            if (this.f251a.f246a != null) {
                this.f251a.f246a.c();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public final void onTick(long j) {
        int unused = this.f251a.b = (60000 - ((int) j)) / 1000;
        if (this.f251a.f246a != null) {
            int b = this.f251a.f246a.b() / 100;
            "current volume level is " + b;
            ((TextView) this.f251a.findViewById(C0000R.id.record_sound_timer)).setText(this.f251a.f.getString(C0000R.string.record_time_remaining, Long.valueOf(j / 1000)));
            int i = -(300 - b);
            if (i > 0) {
                i = 0;
            }
            this.f251a.findViewById(C0000R.id.volume_level_indicator).scrollTo(0, i);
        }
    }
}
