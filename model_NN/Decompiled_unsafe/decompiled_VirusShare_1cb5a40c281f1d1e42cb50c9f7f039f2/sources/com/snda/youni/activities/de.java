package com.snda.youni.activities;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;
import com.snda.youni.b.ai;
import com.snda.youni.modules.chat.b;
import com.snda.youni.services.YouniService;

final class de implements ServiceConnection {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ChatActivity f281a;

    de(ChatActivity chatActivity) {
        this.f281a = chatActivity;
    }

    public final void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        ai unused = this.f281a.Z = YouniService.b();
        if (this.f281a.Z != null && !this.f281a.Z.b()) {
            this.f281a.Z.a(1);
        }
        "on Service Connected network:" + this.f281a.Z;
        if (this.f281a.Z != null) {
            this.f281a.Z.a(this.f281a.X.a(), new x(this));
        }
        ChatActivity.e(this.f281a);
        if (this.f281a.X.n()) {
            if (this.f281a.Z.b()) {
                ChatActivity.f(this.f281a);
            } else {
                ChatActivity.g(this.f281a);
            }
            b.a(true);
        }
    }

    public final void onServiceDisconnected(ComponentName componentName) {
        ai unused = this.f281a.Z = (ai) null;
    }
}
