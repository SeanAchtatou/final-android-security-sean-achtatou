package com.snda.youni.activities;

import android.view.View;

final class bo implements View.OnFocusChangeListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ RecipientsActivity f238a;

    bo(RecipientsActivity recipientsActivity) {
        this.f238a = recipientsActivity;
    }

    public final void onFocusChange(View view, boolean z) {
        if (!z) {
            this.f238a.d.a(this.f238a.d.getText(), this.f238a.d.length());
            this.f238a.d.setVisibility(8);
            this.f238a.f.setVisibility(0);
            this.f238a.e.a(this.f238a.f, this.f238a);
            this.f238a.c.setVisibility(8);
        }
    }
}
