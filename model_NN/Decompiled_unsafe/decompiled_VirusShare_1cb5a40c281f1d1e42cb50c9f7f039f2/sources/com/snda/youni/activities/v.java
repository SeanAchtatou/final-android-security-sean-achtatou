package com.snda.youni.activities;

import android.content.DialogInterface;
import android.net.Uri;
import com.snda.youni.modules.ah;
import com.snda.youni.providers.l;

final class v implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ SettingsBlackListActivity f300a;

    v(SettingsBlackListActivity settingsBlackListActivity) {
        this.f300a = settingsBlackListActivity;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.f300a.getContentResolver().delete(Uri.withAppendedPath(l.f596a, ah.f516a), null, null);
        this.f300a.a();
    }
}
