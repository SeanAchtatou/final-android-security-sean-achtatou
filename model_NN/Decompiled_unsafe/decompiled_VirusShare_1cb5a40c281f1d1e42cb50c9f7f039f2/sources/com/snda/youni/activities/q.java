package com.snda.youni.activities;

import android.content.AsyncQueryHandler;
import android.content.ContentResolver;
import android.database.Cursor;
import com.snda.youni.C0000R;

final class q extends AsyncQueryHandler {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ SettingsBlackListActivity f295a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public q(SettingsBlackListActivity settingsBlackListActivity, ContentResolver contentResolver) {
        super(contentResolver);
        this.f295a = settingsBlackListActivity;
    }

    /* access modifiers changed from: protected */
    public final void onQueryComplete(int i, Object obj, Cursor cursor) {
        "There are " + cursor.getCount() + " blacker in the list";
        if (cursor.getCount() == 0) {
            this.f295a.e.setText((int) C0000R.string.empty_black_list);
            this.f295a.findViewById(C0000R.id.black_list_introduction).setVisibility(0);
            return;
        }
        this.f295a.d.changeCursor(cursor);
        this.f295a.findViewById(C0000R.id.no_black_list).setVisibility(8);
    }
}
