package com.snda.youni.activities;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.widget.Toast;
import com.snda.youni.C0000R;
import com.snda.youni.attachment.e;
import com.snda.youni.e.o;
import java.io.File;

final class ae implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ bw f201a;

    ae(bw bwVar) {
        this.f201a = bwVar;
    }

    public final void onClick(View view) {
        if (!o.a()) {
            Toast.makeText(this.f201a.getContext(), (int) C0000R.string.sdcard_not_working, 1).show();
            this.f201a.cancel();
        }
        Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
        intent.putExtra("output", Uri.fromFile(new File(e.g, "youni_camera.jpg")));
        ((Activity) this.f201a.f).startActivityForResult(intent, 11);
        this.f201a.dismiss();
    }
}
