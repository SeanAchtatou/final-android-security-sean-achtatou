package com.snda.youni.activities;

import android.app.AlertDialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ListAdapter;
import android.widget.Toast;
import android.widget.ViewSwitcher;
import com.snda.youni.C0000R;
import com.snda.youni.attachment.f;
import com.snda.youni.e.e;
import com.snda.youni.e.m;
import com.snda.youni.e.o;
import com.snda.youni.g.a;
import java.io.IOException;
import java.util.ArrayList;

public final class bw extends AlertDialog {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public f f246a;
    /* access modifiers changed from: private */
    public int b;
    private CountDownTimer c;
    private FrameLayout d;
    /* access modifiers changed from: private */
    public Message e;
    /* access modifiers changed from: private */
    public Context f;
    private int g;
    /* access modifiers changed from: private */
    public boolean h = false;
    /* access modifiers changed from: private */
    public ProgressDialog i;
    private final LocationListener j = new bz(this);

    public bw(Context context, Handler handler, int i2) {
        super(context);
        this.f = context;
        this.e = new Message();
        this.e.setTarget(handler);
        this.g = i2;
        setOnDismissListener(new ac(this));
        setOnCancelListener(new aa(this));
    }

    /* access modifiers changed from: private */
    public void a() {
        ((ViewSwitcher) findViewById(C0000R.id.smileDialogSwitcher)).showNext();
        ((ViewSwitcher) findViewById(C0000R.id.recordButtonSwitcher)).showNext();
        try {
            this.f246a = new f();
            if (!this.f246a.a()) {
                Toast.makeText(this.f, (int) C0000R.string.audio_record_start_error, 0).show();
                b();
            }
            this.b = 0;
            getWindow().addFlags(128);
            this.c = new ca(this);
            this.c.start();
            a.a(getContext().getApplicationContext(), "make_voice", null);
            this.h = true;
        } catch (IOException e2) {
            e2.printStackTrace();
        }
    }

    /* access modifiers changed from: private */
    public void b() {
        try {
            if (this.c != null) {
                this.c.cancel();
            }
            this.f246a.d();
            getWindow().clearFlags(128);
            Bundle bundle = new Bundle();
            bundle.putInt("audio", this.b);
            if (this.b < 2) {
                Toast.makeText(this.f, (int) C0000R.string.record_audio_too_short, 0).show();
                e.a("youni_audio.amr", com.snda.youni.attachment.e.g);
            } else {
                this.e.setData(bundle);
            }
            this.f246a = null;
            dismiss();
        } catch (IOException e2) {
            e2.printStackTrace();
            Bundle bundle2 = new Bundle();
            bundle2.putInt("audio", this.b);
            if (this.b < 2) {
                Toast.makeText(this.f, (int) C0000R.string.record_audio_too_short, 0).show();
                e.a("youni_audio.amr", com.snda.youni.attachment.e.g);
            } else {
                this.e.setData(bundle2);
            }
            this.f246a = null;
            dismiss();
        } catch (Exception e3) {
            e3.printStackTrace();
            Bundle bundle3 = new Bundle();
            bundle3.putInt("audio", this.b);
            if (this.b < 2) {
                Toast.makeText(this.f, (int) C0000R.string.record_audio_too_short, 0).show();
                e.a("youni_audio.amr", com.snda.youni.attachment.e.g);
            } else {
                this.e.setData(bundle3);
            }
            this.f246a = null;
            dismiss();
        } catch (Throwable th) {
            Bundle bundle4 = new Bundle();
            bundle4.putInt("audio", this.b);
            if (this.b < 2) {
                Toast.makeText(this.f, (int) C0000R.string.record_audio_too_short, 0).show();
                e.a("youni_audio.amr", com.snda.youni.attachment.e.g);
            } else {
                this.e.setData(bundle4);
            }
            this.f246a = null;
            dismiss();
            throw th;
        }
    }

    static /* synthetic */ void b(bw bwVar) {
        try {
            if (bwVar.c != null) {
                bwVar.c.cancel();
            }
            bwVar.f246a.d();
            bwVar.getWindow().clearFlags(128);
        } catch (IOException e2) {
            e2.printStackTrace();
        } catch (Exception e3) {
            e3.printStackTrace();
        }
    }

    /* access modifiers changed from: private */
    public Location c() {
        if (!(((LocationManager) getContext().getSystemService("location")).isProviderEnabled("gps"))) {
            d();
        }
        LocationManager locationManager = (LocationManager) getContext().getSystemService("location");
        locationManager.requestLocationUpdates("network", 1000, 50.0f, this.j);
        locationManager.requestLocationUpdates("gps", 1000, 10.0f, this.j);
        Location lastKnownLocation = locationManager.getLastKnownLocation("network");
        Location lastKnownLocation2 = locationManager.getLastKnownLocation("gps");
        d();
        return (lastKnownLocation == null || lastKnownLocation2 == null) ? lastKnownLocation != null ? lastKnownLocation : lastKnownLocation2 : lastKnownLocation.getTime() > lastKnownLocation2.getTime() ? lastKnownLocation : lastKnownLocation2;
    }

    private void d() {
        Intent intent = new Intent();
        intent.setClassName("com.android.settings", "com.android.settings.widget.SettingsAppWidgetProvider");
        intent.addCategory("android.intent.category.ALTERNATIVE");
        intent.setData(Uri.parse("custom:3"));
        try {
            PendingIntent.getBroadcast(getContext(), 0, intent, 0).send();
        } catch (PendingIntent.CanceledException e2) {
            e2.printStackTrace();
        }
    }

    static /* synthetic */ void f(bw bwVar) {
        String str;
        try {
            if (bwVar.c != null) {
                bwVar.c.cancel();
            }
            bwVar.f246a.d();
            bwVar.getWindow().clearFlags(128);
        } catch (IOException e2) {
            e2.printStackTrace();
        } catch (Exception e3) {
            e3.printStackTrace();
        } finally {
            bwVar.f246a = null;
            bwVar.dismiss();
            str = "youni_audio.amr";
            e.a(str, com.snda.youni.attachment.e.g);
        }
    }

    /* access modifiers changed from: protected */
    public final void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) C0000R.layout.dialog_smile);
        if ((this.f instanceof RecipientsActivity) && ((RecipientsActivity) this.f).f()) {
            findViewById(C0000R.id.location).setVisibility(8);
        }
        if (!o.a()) {
            findViewById(C0000R.id.gallery).setVisibility(8);
            findViewById(C0000R.id.camera).setVisibility(8);
            findViewById(C0000R.id.record).setVisibility(8);
            Toast.makeText(this.f, (int) C0000R.string.sdcard_not_working, 0).show();
        } else {
            findViewById(C0000R.id.gallery).setVisibility(0);
            findViewById(C0000R.id.camera).setVisibility(0);
            findViewById(C0000R.id.record).setVisibility(0);
        }
        this.d = (FrameLayout) findViewById(C0000R.id.volume_level_indicator);
        GridView gridView = (GridView) findViewById(C0000R.id.emotion_grid);
        TypedArray b2 = m.a().b();
        ArrayList arrayList = new ArrayList();
        for (int i2 = 0; i2 < b2.length() - 1; i2++) {
            arrayList.add(b2.getDrawable(i2));
        }
        gridView.setAdapter((ListAdapter) new com.snda.youni.j.a(getContext(), arrayList));
        gridView.setOnItemClickListener(new ab(this, arrayList));
        findViewById(C0000R.id.record).setOnClickListener(new af(this));
        findViewById(C0000R.id.stop_record).setOnClickListener(new ag(this));
        findViewById(C0000R.id.cancel_record).setOnClickListener(new ad(this));
        findViewById(C0000R.id.camera).setOnClickListener(new ae(this));
        findViewById(C0000R.id.gallery).setOnClickListener(new ai(this));
        findViewById(C0000R.id.location).setOnClickListener(new ah(this));
        if (this.g == 1) {
            a();
        }
    }

    public final boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 == 4) {
            Bundle bundle = new Bundle();
            bundle.putString("smiletext", null);
            this.e.setData(bundle);
            setDismissMessage(this.e);
        }
        return super.onKeyDown(i2, keyEvent);
    }
}
