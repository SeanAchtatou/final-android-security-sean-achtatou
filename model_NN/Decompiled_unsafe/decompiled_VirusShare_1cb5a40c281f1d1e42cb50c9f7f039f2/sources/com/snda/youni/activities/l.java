package com.snda.youni.activities;

import android.content.DialogInterface;
import android.content.SharedPreferences;

final class l implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ SharedPreferences f290a;
    private /* synthetic */ SettingsMessageActivity b;

    l(SettingsMessageActivity settingsMessageActivity, SharedPreferences sharedPreferences) {
        this.b = settingsMessageActivity;
        this.f290a = sharedPreferences;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.b.d.setText("" + this.f290a.getInt("vibrator_num", 2));
    }
}
