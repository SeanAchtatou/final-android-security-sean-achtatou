package com.snda.youni.activities;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.widget.Toast;
import com.snda.youni.C0000R;
import com.snda.youni.e.o;

final class ai implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ bw f205a;

    ai(bw bwVar) {
        this.f205a = bwVar;
    }

    public final void onClick(View view) {
        if (!o.a()) {
            Toast.makeText(this.f205a.getContext(), (int) C0000R.string.sdcard_not_working, 1).show();
            this.f205a.cancel();
        }
        Intent intent = new Intent("android.intent.action.PICK");
        intent.setType("image/*");
        ((Activity) this.f205a.f).startActivityForResult(Intent.createChooser(intent, this.f205a.f.getString(C0000R.string.title_choose_gallery)), 10);
        this.f205a.dismiss();
    }
}
