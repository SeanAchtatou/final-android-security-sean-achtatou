package com.snda.youni;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;
import com.snda.youni.b.ai;
import com.snda.youni.services.YouniService;

final class h implements ServiceConnection {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ YouNi f418a;

    h(YouNi youNi) {
        this.f418a = youNi;
    }

    public final void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        ai unused = this.f418a.x = YouniService.b();
        "onServiceConnected, network.isConnected=" + this.f418a.x.b();
    }

    public final void onServiceDisconnected(ComponentName componentName) {
        ai unused = this.f418a.x = null;
    }
}
