package com.snda.youni;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import com.snda.youni.modules.a.n;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public final class e implements Handler.Callback {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public static final String[] f392a = new String[0];
    /* access modifiers changed from: private */
    public final String[] b = {"thread_id", "count(*)"};
    /* access modifiers changed from: private */
    public final ConcurrentHashMap c = new ConcurrentHashMap();
    /* access modifiers changed from: private */
    public final ConcurrentHashMap d = new ConcurrentHashMap();
    /* access modifiers changed from: private */
    public final Handler e = new Handler(this);
    private b f;
    private boolean g;
    private boolean h;
    private final Context i;
    private BroadcastReceiver j;

    public e(Context context) {
        this.i = context;
        this.j = new p(this);
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.snda.youni.action.conversation_changed");
        this.i.registerReceiver(this.j, intentFilter);
    }

    /* access modifiers changed from: private */
    public void a(int i2, u uVar) {
        if (!this.h) {
            "cacheName, id=" + i2 + ",  info=" + (uVar == null ? null : uVar.toString());
            ap apVar = new ap();
            apVar.f314a = 2;
            if (uVar != null) {
                apVar.b = uVar;
            }
            this.c.put(Integer.valueOf(i2), apVar);
        }
    }

    static /* synthetic */ void a(e eVar, ArrayList arrayList) {
        arrayList.clear();
        for (Integer num : eVar.d.values()) {
            ap apVar = (ap) eVar.c.get(num);
            if (apVar != null && apVar.f314a == 0) {
                apVar.f314a = 1;
                arrayList.add(num.toString());
            }
        }
    }

    private void b(n nVar, int i2) {
        this.d.remove(nVar);
        Iterator it = this.d.entrySet().iterator();
        while (it.hasNext()) {
            if (i2 == ((Integer) ((Map.Entry) it.next()).getValue()).intValue()) {
                it.remove();
            }
        }
    }

    private boolean c(n nVar, int i2) {
        "loadCachedName , viewHolder=" + nVar + " threadId=" + i2;
        "Cache,size=" + this.c.size() + ", Cache=" + TextUtils.join(",", this.c.keySet());
        ap apVar = (ap) this.c.get(Integer.valueOf(i2));
        if (apVar == null) {
            nVar.c.a(0);
            nVar.e.setText("");
            apVar = new ap();
            this.c.put(Integer.valueOf(i2), apVar);
        } else if (apVar.f314a == 2) {
            u uVar = apVar.b;
            "loadCachedName , info=" + (uVar == null ? null : uVar.toString());
            if (uVar == null) {
                a(i2, new u());
                return true;
            }
            nVar.c.a(uVar.b);
            nVar.e.setText("(" + String.valueOf(uVar.c) + "/" + String.valueOf(uVar.f621a) + ")");
            return true;
        }
        apVar.f314a = 0;
        return false;
    }

    private void e() {
        if (!this.g) {
            this.g = true;
            this.e.sendEmptyMessage(1);
        }
    }

    public final void a() {
        this.h = true;
        if (this.f != null) {
            this.f.quit();
            this.f = null;
        }
        this.d.clear();
        this.c.clear();
        this.i.unregisterReceiver(this.j);
    }

    public final void a(n nVar, int i2) {
        "threadId, threadId=" + i2;
        b(nVar, i2);
        if (i2 == 0) {
            this.d.remove(nVar);
            return;
        }
        boolean c2 = c(nVar, i2);
        "loadName , loaded=" + c2 + ", mPaused=" + this.h + ", mPendingRequests.size=" + this.d.size();
        if (c2) {
            this.d.remove(nVar);
            return;
        }
        this.d.put(nVar, Integer.valueOf(i2));
        "mPaused=" + this.h + ", mPendingRequests.size=" + this.d.size();
        if (!this.h) {
            e();
        }
    }

    public final void b() {
        this.h = true;
    }

    public final void c() {
        this.h = false;
        if (!this.d.isEmpty()) {
            e();
        }
    }

    public final boolean handleMessage(Message message) {
        switch (message.what) {
            case 1:
                this.g = false;
                if (!this.h) {
                    if (this.f == null) {
                        this.f = new b(this, this.i.getContentResolver());
                        this.f.start();
                    }
                    this.f.a();
                }
                return true;
            case 2:
                if (!this.h) {
                    "processLoadedImages mPendingRequests.size=" + this.d.size();
                    Iterator it = this.d.keySet().iterator();
                    while (it.hasNext()) {
                        n nVar = (n) it.next();
                        boolean c2 = c(nVar, ((Integer) this.d.get(nVar)).intValue());
                        "processLoadedImages, loaded=" + c2;
                        if (c2) {
                            it.remove();
                        }
                        "processLoadedImages mPendingRequests.size=" + this.d.size();
                    }
                    "processLoadedImages mPendingRequests.size=" + this.d.size();
                    if (!this.d.isEmpty()) {
                        e();
                    }
                }
                return true;
            default:
                return false;
        }
    }
}
