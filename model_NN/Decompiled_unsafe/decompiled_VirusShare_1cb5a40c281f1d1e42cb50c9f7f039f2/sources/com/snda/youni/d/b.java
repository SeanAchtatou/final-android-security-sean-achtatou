package com.snda.youni.d;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import com.snda.a.a.a.a;

public final class b {

    /* renamed from: a  reason: collision with root package name */
    private static Context f390a;

    public static void a() {
        SharedPreferences defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(f390a);
        a.a(f390a.getResources(), f390a.getPackageName());
        defaultSharedPreferences.edit().remove("skin").commit();
    }

    public static void a(Context context) {
        f390a = context;
        SharedPreferences defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        String string = defaultSharedPreferences.getString("skin", null);
        try {
            if (c(string)) {
                a.a(f390a.getPackageManager().getResourcesForApplication(string), string);
                return;
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        a.a(f390a.getResources(), f390a.getPackageName());
        defaultSharedPreferences.edit().remove("skin").commit();
    }

    public static boolean a(String str) {
        SharedPreferences defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(f390a);
        try {
            if (!c(str)) {
                return false;
            }
            a.a(f390a.getPackageManager().getResourcesForApplication(str), str);
            defaultSharedPreferences.edit().putString("skin", str).commit();
            a.a("SkinManager", "ljd set " + str + " as new skin");
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return false;
        }
    }

    public static c b(String str) {
        try {
            PackageInfo packageInfo = f390a.getPackageManager().getPackageInfo(str, 64);
            Resources resourcesForApplication = f390a.getPackageManager().getResourcesForApplication(str);
            c cVar = new c(packageInfo.packageName);
            cVar.f391a = packageInfo.versionName;
            cVar.b = packageInfo.versionCode;
            cVar.c = Integer.valueOf(resourcesForApplication.getString(resourcesForApplication.getIdentifier("skin_manager_version", "string", packageInfo.packageName))).intValue();
            cVar.d = resourcesForApplication.getDrawable(packageInfo.applicationInfo.icon);
            return cVar;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static boolean c(String str) {
        try {
            if (TextUtils.isEmpty(str)) {
                return false;
            }
            Resources resourcesForApplication = f390a.getPackageManager().getResourcesForApplication(str);
            if (resourcesForApplication == null) {
                return false;
            }
            return Integer.valueOf(resourcesForApplication.getString(resourcesForApplication.getIdentifier("skin_manager_version", "string", str))).intValue() == 0;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return false;
        }
    }
}
