package com.snda.youni.d;

import android.content.res.Resources;
import android.graphics.drawable.Drawable;

public final class a {

    /* renamed from: a  reason: collision with root package name */
    private static Resources f389a;
    private static String b;

    public static Drawable a(String str) {
        return f389a.getDrawable(f389a.getIdentifier(str, "drawable", b));
    }

    public static void a(Resources resources, String str) {
        f389a = resources;
        b = str;
    }

    public static int b(String str) {
        if (!b.c(b)) {
            b.a();
        }
        return f389a.getColor(f389a.getIdentifier(str, "color", b));
    }
}
