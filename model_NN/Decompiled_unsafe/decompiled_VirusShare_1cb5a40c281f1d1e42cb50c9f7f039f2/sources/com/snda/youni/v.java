package com.snda.youni;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

final class v extends BroadcastReceiver {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ YouNi f622a;

    v(YouNi youNi) {
        this.f622a = youNi;
    }

    public final void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        "Receivier action is: " + action;
        if ("com.snda.youni.ACTION_STATUS_LOADED".equals(action)) {
            if (this.f622a.n.getCheckedRadioButtonId() == C0000R.id.btn_inbox || this.f622a.n.getCheckedRadioButtonId() == C0000R.id.btn_contacts) {
                this.f622a.n();
            }
        } else if ("com.snda.youni.action.CONTACTS_CHANGED".equals(action)) {
            this.f622a.n();
        } else if ("com.snda.youni.action.FRIEND_LIST_CHANGED".equals(action)) {
            am.a(this.f622a.c(), false);
            this.f622a.n();
        } else if ("com.snda.youni.action.draft_changed".equals(action)) {
            if (this.f622a.n.getCheckedRadioButtonId() == C0000R.id.btn_inbox) {
                this.f622a.n();
            }
        } else if ("com.snda.youni.action_NETWORK_EXCEPTION".equals(action)) {
            this.f622a.showDialog(11);
        } else if ("com.snda.youni.action_NETWORK_UNREACHABLE".equals(action)) {
            this.f622a.showDialog(12);
        } else if ("com.snda.youni.ACTION_XNETWORK_CONNECTED".equals(action) || "com.snda.youni.action.ACTION_FRIEND_LIST_NEW".equals(action)) {
            am.a(this.f622a.c(), true);
            this.f622a.n();
        }
    }
}
