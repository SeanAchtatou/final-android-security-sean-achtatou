package com.snda.youni;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import com.snda.youni.a.b;
import com.snda.youni.activities.FavoriteActivity;
import com.snda.youni.activities.RecipientsActivity;
import com.snda.youni.b.ai;
import com.snda.youni.e.n;
import com.snda.youni.h.a.a;
import com.snda.youni.modules.a.e;
import com.snda.youni.modules.aj;
import com.snda.youni.modules.m;
import com.snda.youni.modules.q;
import com.snda.youni.modules.settings.k;
import com.snda.youni.modules.settings.s;
import com.snda.youni.modules.settings.v;
import com.snda.youni.services.YouniService;
import java.io.File;

public class YouNi extends Activity implements RadioGroup.OnCheckedChangeListener, k {

    /* renamed from: a  reason: collision with root package name */
    public static boolean f170a = false;
    public static YouNi b = null;
    public Handler c = new f(this);
    private View d;
    private View e;
    private View f;
    private View g;
    private d h;
    private e i;
    private b j;
    /* access modifiers changed from: private */
    public e k;
    /* access modifiers changed from: private */
    public aj l;
    private s m;
    /* access modifiers changed from: private */
    public RadioGroup n;
    private RadioButton o;
    /* access modifiers changed from: private */
    public RadioButton p;
    private RadioButton q;
    /* access modifiers changed from: private */
    public MediaScannerConnection r;
    private SharedPreferences s;
    private BroadcastReceiver t;
    private a u;
    private int v = 0;
    private int w = 0;
    /* access modifiers changed from: private */
    public ai x = null;
    private ServiceConnection y = new h(this);

    private void a(int i2) {
        switch (i2) {
            case 0:
                try {
                    Intent intent = new Intent("android.media.action.IMAGE_CAPTURE", (Uri) null);
                    intent.putExtra("output", Uri.fromFile(new File(com.snda.youni.e.b.d())));
                    startActivityForResult(intent, 0);
                    return;
                } catch (ActivityNotFoundException e2) {
                    new AlertDialog.Builder(this).setTitle(17039380).setMessage((int) C0000R.string.settings_photo_picker_not_found).setPositiveButton(17039370, (DialogInterface.OnClickListener) null).show();
                }
            case 1:
                Intent intent2 = new Intent();
                intent2.setAction("android.intent.action.GET_CONTENT");
                intent2.addCategory("android.intent.category.OPENABLE");
                intent2.setType("image/*");
                a(intent2);
                startActivityForResult(intent2, 1);
                return;
            case 2:
                Intent intent3 = new Intent("com.android.camera.action.CROP");
                intent3.setDataAndType(Uri.fromFile(new File(com.snda.youni.e.b.d())), "image/*");
                a(intent3);
                startActivityForResult(intent3, 1);
                return;
            default:
                return;
        }
        new AlertDialog.Builder(this).setTitle(17039380).setMessage((int) C0000R.string.settings_photo_picker_not_found).setPositiveButton(17039370, (DialogInterface.OnClickListener) null).show();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    private static void a(Intent intent) {
        intent.putExtra("crop", "true");
        intent.putExtra("aspectX", 1);
        intent.putExtra("aspectY", 1);
        intent.putExtra("outputX", 100);
        intent.putExtra("outputY", 100);
        intent.putExtra("return-data", true);
    }

    /* access modifiers changed from: private */
    public void n() {
        switch (this.n.getCheckedRadioButtonId()) {
            case C0000R.id.btn_inbox /*2131558663*/:
                this.k.b();
                return;
            case C0000R.id.btn_contacts /*2131558664*/:
                this.l.a();
                return;
            default:
                return;
        }
    }

    public final void a() {
        a(0);
    }

    public final void b() {
        a(1);
    }

    public final ai c() {
        return this.x;
    }

    public final e d() {
        return this.k;
    }

    public final SharedPreferences e() {
        return this.s;
    }

    public final ServiceConnection f() {
        return this.y;
    }

    public final s g() {
        return this.m;
    }

    public final String h() {
        EditText editText = (EditText) this.f.findViewById(C0000R.id.search_input_box);
        "getSearchString : " + editText.getText().toString();
        return editText.getText().toString();
    }

    public final int i() {
        "mContactsMode=" + this.v;
        return this.v;
    }

    public final ListView j() {
        return this.l.c();
    }

    public final ListView k() {
        return this.k.c();
    }

    public final void l() {
        int i2;
        int i3 = getResources().getConfiguration().orientation;
        RelativeLayout relativeLayout = (RelativeLayout) findViewById(C0000R.id.frame_new_youni_contacts);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -2);
        float f2 = getResources().getDisplayMetrics().density;
        "HHHHHHHHHHHHHHHHHHHHHHH---ori=" + i3;
        if (i3 == 2) {
            i2 = (int) ((-50.0f * f2) + 0.5f);
            "HHHHHHHHHHHHHHHHHHHHHHH---heng ping marginLeft=" + i2;
        } else if (i3 == 1) {
            i2 = (int) ((30.0f * f2) + 0.5f);
            "SSSSSSSSSSSSSSSSSSSSSSS---shu ping marginLeft=" + i2;
        } else {
            i2 = 0;
        }
        layoutParams.setMargins(i2, 0, 0, 0);
        relativeLayout.setLayoutParams(layoutParams);
    }

    public final void m() {
        this.p.setOnClickListener(null);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        if (i3 == -1 || i3 == 100 || i3 == 200 || i3 == 300) {
            switch (i2) {
                case 0:
                    this.r = new MediaScannerConnection(this, new af(this));
                    this.r.connect();
                    a(2);
                    return;
                case 1:
                    Bitmap bitmap = (Bitmap) intent.getParcelableExtra("data");
                    if (bitmap != null) {
                        this.m.a(this, bitmap);
                        return;
                    }
                    return;
                case 1314:
                    switch (i3) {
                        case 100:
                            this.l.a(intent);
                            return;
                        case 200:
                            this.l.a(intent.getIntArrayExtra("contact_id"));
                            return;
                        case 300:
                            this.k.a(intent);
                            return;
                        default:
                            return;
                    }
                case 9527:
                    if (intent != null && intent.getBooleanExtra("add_recipient", false)) {
                        this.p.setChecked(true);
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    }

    public void onCheckedChanged(RadioGroup radioGroup, int i2) {
        "RadioGroup's checkedId is: " + i2;
        n.a(this).a();
        switch (i2) {
            case C0000R.id.btn_inbox /*2131558663*/:
                this.e.setVisibility(0);
                this.f.setVisibility(8);
                this.g.setVisibility(8);
                am.a(this.x, false);
                this.k.b();
                break;
            case C0000R.id.btn_contacts /*2131558664*/:
                this.e.setVisibility(8);
                this.f.setVisibility(0);
                this.g.setVisibility(8);
                this.l.a();
                if (findViewById(C0000R.id.new_youni_contacts).getVisibility() == 0) {
                    if (this.l.g()) {
                        this.l.b = true;
                        ((AppContext) getApplication()).e();
                    }
                    this.l.a((int) C0000R.id.btn_youni);
                    break;
                }
                break;
            case C0000R.id.btn_settings /*2131558665*/:
                this.e.setVisibility(8);
                this.f.setVisibility(8);
                this.g.setVisibility(0);
                this.c.sendEmptyMessage(1);
                this.s.edit().putBoolean("update_notification", false).commit();
                break;
            default:
                this.e.setVisibility(0);
                this.f.setVisibility(8);
                this.g.setVisibility(8);
                this.o.setChecked(true);
                this.p.setChecked(false);
                this.q.setChecked(false);
                break;
        }
        if (i2 != C0000R.id.btn_contacts) {
            this.l.f();
        }
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        l();
    }

    public boolean onContextItemSelected(MenuItem menuItem) {
        if (this.k.a(menuItem)) {
            return true;
        }
        if (this.l.a(menuItem)) {
            return true;
        }
        return super.onContextItemSelected(menuItem);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public void onCreate(Bundle bundle) {
        int intExtra;
        super.onCreate(bundle);
        "ljd onCreate, taskId = " + getTaskId() + " this = " + this;
        setContentView((int) C0000R.layout.youni);
        com.snda.a.a.c.a.a();
        SharedPreferences.Editor edit = PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit();
        edit.putBoolean("inner_test", false);
        edit.commit();
        this.u = new a(getApplicationContext());
        this.d = findViewById(C0000R.id.tab_content);
        this.e = this.d.findViewById(C0000R.id.tab_inbox);
        this.f = this.d.findViewById(C0000R.id.tab_contacts);
        this.g = this.d.findViewById(C0000R.id.tab_settings);
        v.a(this);
        this.s = PreferenceManager.getDefaultSharedPreferences(this);
        this.j = new b(this);
        this.h = new d(this);
        this.i = new e(this);
        Intent intent = getIntent();
        if (intent != null) {
            if (intent.getBooleanExtra("add_recipient", false)) {
                this.v = 1;
                this.w = 1;
                ListView listView = (ListView) this.f.findViewById(C0000R.id.list_contacts);
                listView.setItemsCanFocus(false);
                listView.setChoiceMode(2);
            } else if (intent.getBooleanExtra("contacts_batch_operations", false)) {
                this.v = 1;
                this.w = 2;
                ListView listView2 = (ListView) this.f.findViewById(C0000R.id.list_contacts);
                listView2.setItemsCanFocus(false);
                listView2.setChoiceMode(2);
            } else if (intent.getBooleanExtra("inbox_batch_operations", false)) {
                this.v = 1;
                this.w = 3;
                ListView listView3 = (ListView) this.e.findViewById(C0000R.id.list_inbox);
                listView3.setItemsCanFocus(false);
                listView3.setChoiceMode(2);
            }
        }
        this.k = new e(this, this.e, this.h, this.i, this.u);
        this.l = new aj(this, this.f, this.h);
        this.m = new s(this, this.g, this.j);
        if (!this.s.getBoolean("is_create_short_cut", false)) {
            Intent intent2 = new Intent("com.android.launcher.action.UNINSTALL_SHORTCUT");
            intent2.putExtra("android.intent.extra.shortcut.NAME", getString(C0000R.string.app_name));
            intent2.putExtra("android.intent.extra.shortcut.INTENT", new Intent("android.intent.action.MAIN").setComponent(new ComponentName(getPackageName(), getPackageName() + "." + getLocalClassName())));
            sendBroadcast(intent2);
            Intent intent3 = new Intent("com.android.launcher.action.INSTALL_SHORTCUT");
            intent3.putExtra("android.intent.extra.shortcut.NAME", getString(C0000R.string.app_name));
            intent3.putExtra("duplicate", false);
            intent3.putExtra("android.intent.extra.shortcut.INTENT", new Intent("android.intent.action.MAIN").setComponent(new ComponentName(getPackageName(), "." + getLocalClassName())));
            intent3.putExtra("android.intent.extra.shortcut.ICON_RESOURCE", Intent.ShortcutIconResource.fromContext(this, C0000R.drawable.icn_youni));
            sendBroadcast(intent3);
            SharedPreferences.Editor edit2 = this.s.edit();
            edit2.putBoolean("is_create_short_cut", true);
            edit2.commit();
        }
        registerForContextMenu(this.k.c());
        registerForContextMenu(this.l.c());
        this.e.findViewById(C0000R.id.new_chat).setOnClickListener(new g(this));
        this.n = (RadioGroup) findViewById(C0000R.id.group_tabs);
        this.n.setOnCheckedChangeListener(this);
        this.o = (RadioButton) this.n.findViewById(C0000R.id.btn_inbox);
        this.p = (RadioButton) this.n.findViewById(C0000R.id.btn_contacts);
        this.q = (RadioButton) this.n.findViewById(C0000R.id.btn_settings);
        if (intent != null && (intExtra = intent.getIntExtra("checked_tab_index", 0)) > 0) {
            this.n.check(intExtra);
            this.l.a((int) C0000R.id.btn_youni);
        }
        setVolumeControlStream(2);
        this.t = new v(this);
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.snda.youni.ACTION_STATUS_LOADED");
        intentFilter.addAction("com.snda.youni.action.CONTACTS_CHANGED");
        intentFilter.addAction("com.snda.youni.action.FRIEND_LIST_CHANGED");
        intentFilter.addAction("com.snda.youni.PORTRAIT_UPDATED");
        intentFilter.addAction("com.snda.youni.action_NETWORK_EXCEPTION");
        intentFilter.addAction("com.snda.youni.action_NETWORK_UNREACHABLE");
        intentFilter.addAction("com.snda.youni.action.draft_changed");
        intentFilter.addAction("com.snda.youni.ACTION_XNETWORK_CONNECTED");
        intentFilter.addAction("com.snda.youni.action.ACTION_FRIEND_LIST_NEW");
        registerReceiver(this.t, intentFilter);
        switch (this.w) {
            case 1:
                "onCreate, mContactsMode=" + this.v;
                this.p.setChecked(true);
                this.n.setVisibility(8);
                findViewById(C0000R.id.add_recipients_confirm).setVisibility(0);
                findViewById(C0000R.id.add_recipients_cancel).setOnClickListener(new j(this));
                findViewById(C0000R.id.add_recipients_ok).setOnClickListener(new i(this));
                ((m) this.l.c().getAdapter()).a(intent.getStringExtra("selected_recipients"));
                bindService(new Intent(this, YouniService.class), this.y, 1);
                break;
            case 2:
                "onCreate, mContactsMode=" + this.v;
                this.l.a(intent.getIntExtra("contacts_tab", 0));
                this.p.setChecked(true);
                this.n.setVisibility(8);
                findViewById(C0000R.id.contacts_batch_operations).setVisibility(0);
                findViewById(C0000R.id.inbox_batch_operations).setVisibility(8);
                findViewById(C0000R.id.btn_new_contact).setVisibility(8);
                findViewById(C0000R.id.contacts_batch_operations_cancel).setOnClickListener(new l(this));
                findViewById(C0000R.id.contacts_batch_send_message).setOnClickListener(new k(this));
                findViewById(C0000R.id.contacts_batch_delete_contacts).setOnClickListener(new m(this));
                bindService(new Intent(this, YouniService.class), this.y, 1);
                break;
            case 3:
                "onCreate, mContactsMode=" + this.v;
                this.o.setChecked(true);
                this.n.setVisibility(8);
                findViewById(C0000R.id.contacts_batch_operations).setVisibility(8);
                findViewById(C0000R.id.inbox_batch_operations).setVisibility(0);
                findViewById(C0000R.id.new_chat).setVisibility(8);
                findViewById(C0000R.id.inbox_batch_operations_cancel).setOnClickListener(new n(this));
                findViewById(C0000R.id.inbox_batch_check).setOnClickListener(new aa(this));
                findViewById(C0000R.id.inbox_batch_delete_message).setOnClickListener(new ah(this));
                this.y = null;
                break;
            default:
                new q(this, this.j);
                if (!TextUtils.isEmpty(com.snda.youni.e.s.f403a)) {
                    sendBroadcast(new Intent("com.snda.youni.ACTION_LOAD_FRIENDS_LIST"));
                }
                com.snda.youni.modules.c.b.a(getApplicationContext());
                YouniService.a(getApplicationContext());
                break;
        }
        PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit().putString("youni_special_number", "106550218098866,1065751608820666,1069088266").commit();
        b = this;
    }

    public void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {
        MenuInflater menuInflater = getMenuInflater();
        if (view == this.k.c()) {
            if (this.v != 1) {
                this.k.a(menuInflater, contextMenu, contextMenuInfo);
            }
        } else if (view == this.l.c() && this.v != 1) {
            this.l.a(menuInflater, contextMenu, contextMenuInfo);
        }
        super.onCreateContextMenu(contextMenu, view, contextMenuInfo);
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int i2) {
        switch (i2) {
            case 10:
                return new AlertDialog.Builder(this).setMessage((int) C0000R.string.tip_for_invite).setCancelable(false).setNegativeButton((int) C0000R.string.alert_dialog_close, new ad(this)).setOnKeyListener(new ag(this)).create();
            case 11:
                return new AlertDialog.Builder(this).setTitle((int) C0000R.string.network_exception_title).setMessage((int) C0000R.string.network_exception).setPositiveButton((int) C0000R.string.check_network, new ab(this)).setNegativeButton((int) C0000R.string.alert_dialog_cancel, new ae(this)).create();
            case 12:
                return new AlertDialog.Builder(this).setTitle((int) C0000R.string.network_unreachable_title).setMessage((int) C0000R.string.network_unreachable).setPositiveButton((int) C0000R.string.set_network, new ai(this)).setNegativeButton((int) C0000R.string.alert_dialog_cancel, new ac(this)).create();
            case 13:
                return new AlertDialog.Builder(this).setTitle((int) C0000R.string.deleteConfirmation_title).setIcon(17301543).setMessage((int) C0000R.string.deleteConfirmation).setNegativeButton(17039360, (DialogInterface.OnClickListener) null).setPositiveButton(17039370, new s(this)).setCancelable(false).create();
            case 14:
                return new AlertDialog.Builder(this).setTitle((int) C0000R.string.operation_title).setMessage((int) C0000R.string.contact_invite).setPositiveButton((int) C0000R.string.invite, new x(this)).setNegativeButton((int) C0000R.string.alert_dialog_cancel, (DialogInterface.OnClickListener) null).create();
            case 15:
                return new AlertDialog.Builder(this).setTitle((int) C0000R.string.deleteConfirmation_title).setIcon(17301543).setMessage((int) C0000R.string.delete_mutil_contacts_Confirmation).setNegativeButton(17039360, (DialogInterface.OnClickListener) null).setPositiveButton(17039370, new t(this)).setCancelable(false).create();
            case 16:
                return new AlertDialog.Builder(this).setTitle((int) C0000R.string.deleteConfirmation_title).setIcon(17301543).setMessage((int) C0000R.string.delete_mutil_conversation_confirmation).setNegativeButton(17039360, (DialogInterface.OnClickListener) null).setPositiveButton(17039370, new w(this)).setCancelable(false).create();
            case 17:
                return new AlertDialog.Builder(this).setTitle((int) C0000R.string.menu_add_blacak_list).setIcon(17301543).setMessage(getString(C0000R.string.add_to_black_list_notification, new Object[]{this.k.f492a.f496a})).setNegativeButton(17039360, (DialogInterface.OnClickListener) null).setPositiveButton(17039370, new y(this)).setCancelable(false).create();
            case 18:
                return new AlertDialog.Builder(this).setTitle((int) C0000R.string.menu_add_blacak_list).setIcon(17301543).setMessage(getString(C0000R.string.add_to_black_list_notification, new Object[]{this.l.f518a.f496a})).setNegativeButton(17039360, (DialogInterface.OnClickListener) null).setPositiveButton(17039370, new z(this)).setCancelable(false).create();
            default:
                return null;
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        "ljd onDestroy, taskId = " + getTaskId() + " this = " + this;
        this.l.f();
        if (b == this) {
            com.snda.youni.e.s.d = 7;
            b = null;
        }
        AppContext.b(this);
        if (this.y != null) {
            unbindService(this.y);
        }
        unregisterReceiver(this.t);
        this.h.a();
        this.i.a();
        this.l.e();
        this.k.d();
        this.k.g();
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        int intExtra;
        super.onNewIntent(intent);
        "ljd onNewIntent, taskId = " + getTaskId() + " this = " + this;
        if (intent != null && (intExtra = intent.getIntExtra("checked_tab_index", 0)) > 0) {
            this.n.check(intExtra);
            this.l.a((int) C0000R.id.btn_youni);
        }
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case 0:
                startActivityForResult(new Intent(this, RecipientsActivity.class), 9527);
                return true;
            case 1:
                com.snda.youni.g.a.a(getApplicationContext(), "add_contact", null);
                Intent intent = new Intent("android.intent.action.INSERT");
                intent.setType("vnd.android.cursor.dir/contact");
                intent.setFlags(268435456);
                startActivity(intent);
                return true;
            case 2:
                if (this.o.isChecked()) {
                    this.k.a();
                    return true;
                } else if (!this.p.isChecked()) {
                    return true;
                } else {
                    this.l.b();
                    return true;
                }
            case 3:
                startActivity(new Intent(this, FavoriteActivity.class));
                return true;
            case 4:
                this.j.a();
                return true;
            default:
                return true;
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        "ljd onPause, taskId = " + getTaskId() + " this = " + this;
        ((AppContext) getApplicationContext()).b(false);
        n.a(this).a();
        this.k.e();
        this.h.b();
        this.i.b();
        com.snda.youni.e.s.d = 1;
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        if (this.v == 1) {
            return false;
        }
        menu.clear();
        switch (this.n.getCheckedRadioButtonId()) {
            case C0000R.id.btn_inbox /*2131558663*/:
                menu.add(0, 0, 0, (int) C0000R.string.menu_add_new_chat).setIcon((int) C0000R.drawable.menu_new_chat);
                menu.add(0, 3, 3, (int) C0000R.string.menu_my_favorite).setIcon((int) C0000R.drawable.menu_favorite);
                menu.add(0, 4, 4, (int) C0000R.string.settings_update).setIcon((int) C0000R.drawable.menu_update);
                break;
            case C0000R.id.btn_contacts /*2131558664*/:
                menu.add(0, 1, 1, (int) C0000R.string.menu_add_new_contact).setIcon((int) C0000R.drawable.menu_new_contact);
                break;
            default:
                return false;
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        "ljd onResume, taskId = " + getTaskId() + " this = " + this;
        com.snda.youni.g.a.a(getApplicationContext(), "start_icon", null);
        com.snda.youni.g.a.b(getApplicationContext());
        n.a(this).a();
        ((AppContext) getApplicationContext()).b(true);
        com.snda.youni.e.s.d = 0;
        am.a(this.x, false);
        this.k.f();
        this.h.c();
        this.i.c();
        this.l.d();
        n();
        l();
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        com.snda.youni.e.s.d = 1;
        AppContext.a(this);
        "ljd onStart, taskId = " + getTaskId() + " this = " + this;
        if (f170a) {
            l();
            findViewById(C0000R.id.new_youni_contacts).setVisibility(0);
        }
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        if (b == this) {
            com.snda.youni.e.s.d = 3;
        }
        "ljd onStop, taskId = " + getTaskId() + " this = " + this;
    }
}
