package com.snda.youni;

import android.view.View;
import android.widget.Toast;

final class m implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ YouNi f435a;

    m(YouNi youNi) {
        this.f435a = youNi;
    }

    public final void onClick(View view) {
        if (((com.snda.youni.modules.m) this.f435a.l.c().getAdapter()).b().size() == 0) {
            Toast.makeText(this.f435a, (int) C0000R.string.no_select_notification, 0).show();
        } else {
            this.f435a.showDialog(15);
        }
    }
}
