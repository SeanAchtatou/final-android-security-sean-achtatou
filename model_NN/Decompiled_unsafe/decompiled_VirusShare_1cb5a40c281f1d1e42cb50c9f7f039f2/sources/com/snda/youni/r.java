package com.snda.youni;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import com.snda.youni.providers.n;
import java.util.ArrayList;
import java.util.Iterator;

final class r extends BroadcastReceiver {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ d f599a;

    r(d dVar) {
        this.f599a = dVar;
    }

    public final void onReceive(Context context, Intent intent) {
        if ("com.snda.youni.action.FRIEND_LIST_CHANGED".equals(intent.getAction())) {
            ArrayList arrayList = new ArrayList();
            ArrayList<String> stringArrayListExtra = intent.getStringArrayListExtra("com.snda.youni.extra_icon_updated_sids");
            if (stringArrayListExtra != null) {
                Iterator<String> it = stringArrayListExtra.iterator();
                StringBuilder sb = new StringBuilder();
                sb.append("sid IN ");
                sb.append('(');
                boolean z = true;
                while (it.hasNext()) {
                    String next = it.next();
                    if (z) {
                        sb.append('\'');
                        sb.append(next);
                        sb.append('\'');
                        z = false;
                    } else {
                        sb.append(',');
                        sb.append('\'');
                        sb.append(next);
                        sb.append('\'');
                    }
                }
                sb.append(')');
                stringArrayListExtra.clear();
                "selection=" + sb.toString();
                Cursor query = this.f599a.l.getContentResolver().query(n.f597a, new String[]{"contact_id"}, sb.toString(), null, null);
                if (query != null) {
                    query.moveToPosition(-1);
                    while (query.moveToNext()) {
                        arrayList.add(query.getString(0));
                    }
                    query.close();
                }
                Iterator it2 = arrayList.iterator();
                while (it2.hasNext()) {
                    this.f599a.e.remove(Integer.valueOf((String) it2.next()));
                }
            }
            ArrayList<String> stringArrayListExtra2 = intent.getStringArrayListExtra("com.snda.youni.extra_icon_updated_contact_ids");
            if (stringArrayListExtra2 != null) {
                Iterator<String> it3 = stringArrayListExtra2.iterator();
                while (it3.hasNext()) {
                    this.f599a.e.remove(Integer.valueOf(it3.next()));
                }
                stringArrayListExtra2.clear();
            }
        }
    }
}
