package com.snda.youni;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.ListView;
import com.snda.youni.modules.a.n;

final class p extends BroadcastReceiver {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ e f587a;

    p(e eVar) {
        this.f587a = eVar;
    }

    public final void onReceive(Context context, Intent intent) {
        if ("com.snda.youni.action.conversation_changed".equals(intent.getAction()) && context != null) {
            int intExtra = intent.getIntExtra("extra_conversation_changed_thread_id", 0);
            "Conversation_CHANGED, thread_id=" + intExtra;
            if (intExtra > 0) {
                ListView c = ((YouNi) context).d().c();
                int childCount = c.getChildCount();
                "onReceive, itemCount=" + childCount;
                for (int i = 0; i < childCount; i++) {
                    n nVar = (n) c.getChildAt(i).getTag();
                    "onReceive, threadId=" + intExtra + ", viewHolder.threadId=" + nVar.f499a;
                    if (nVar.f499a == intExtra) {
                        this.f587a.c.remove(Integer.valueOf(intExtra));
                        this.f587a.d.remove(nVar);
                        this.f587a.d.put(nVar, Integer.valueOf(intExtra));
                        this.f587a.c();
                        return;
                    }
                }
            }
        }
    }
}
