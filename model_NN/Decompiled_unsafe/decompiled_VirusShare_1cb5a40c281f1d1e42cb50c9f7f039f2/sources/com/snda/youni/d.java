package com.snda.youni;

import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Message;
import android.widget.ImageView;
import com.snda.youni.c.f;
import com.snda.youni.c.t;
import com.snda.youni.e.v;
import com.snda.youni.f.a;
import com.snda.youni.providers.n;
import java.lang.ref.SoftReference;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public final class d implements Handler.Callback {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public static final String[] f388a = new String[0];
    /* access modifiers changed from: private */
    public final String[] b = {"contact_id", "data15"};
    /* access modifiers changed from: private */
    public final String[] c = {"contact_id", "photo", "photo_timestamp"};
    private final int d = C0000R.drawable.default_portrait;
    /* access modifiers changed from: private */
    public final ConcurrentHashMap e = new ConcurrentHashMap();
    private final ConcurrentHashMap f = new ConcurrentHashMap();
    private final ConcurrentHashMap g = new ConcurrentHashMap();
    /* access modifiers changed from: private */
    public final Handler h = new Handler(this);
    private ak i;
    private boolean j;
    private boolean k;
    /* access modifiers changed from: private */
    public final Context l;
    private BroadcastReceiver m;
    private a n = new q(this);

    public d(Context context) {
        this.l = context;
        this.m = new r(this);
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.snda.youni.action.FRIEND_LIST_CHANGED");
        this.l.registerReceiver(this.m, intentFilter);
    }

    static /* synthetic */ void a(d dVar, ContentResolver contentResolver, Integer num, byte[] bArr) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("photo", bArr);
        int update = contentResolver.update(n.f597a, contentValues, "contact_id=" + num, null);
        dVar.e.remove(num);
        if (update > 0 && dVar.b(num.intValue()) > 0) {
            dVar.e();
        }
    }

    static /* synthetic */ void a(d dVar, ArrayList arrayList, ArrayList arrayList2) {
        arrayList.clear();
        arrayList2.clear();
        for (Integer num : dVar.f.values()) {
            aj ajVar = (aj) dVar.e.get(num);
            if (ajVar != null && ajVar.f311a == 0) {
                ajVar.f311a = 1;
                arrayList.add(num);
                arrayList2.add(num.toString());
            }
        }
    }

    /* access modifiers changed from: private */
    public boolean a(int i2, byte[] bArr) {
        if (this.k) {
            return false;
        }
        aj ajVar = new aj();
        ajVar.f311a = 2;
        if (bArr != null) {
            try {
                Bitmap decodeByteArray = BitmapFactory.decodeByteArray(bArr, 0, bArr.length, null);
                if (decodeByteArray != null) {
                    Bitmap a2 = v.a(decodeByteArray);
                    if (a2 != null) {
                        ajVar.b = new SoftReference(new BitmapDrawable(this.l.getResources(), a2));
                    } else {
                        ajVar.b = null;
                    }
                } else {
                    ajVar.b = null;
                }
            } catch (OutOfMemoryError e2) {
            }
        }
        this.e.put(Integer.valueOf(i2), ajVar);
        return ajVar.b != null;
    }

    private boolean a(ImageView imageView, int i2) {
        aj ajVar = (aj) this.e.get(Integer.valueOf(i2));
        if (ajVar == null) {
            ajVar = new aj();
            this.e.put(Integer.valueOf(i2), ajVar);
        } else if (ajVar.f311a == 2) {
            if (ajVar.b == null) {
                imageView.setImageResource(this.d);
                return true;
            }
            Drawable drawable = (Drawable) ajVar.b.get();
            if (drawable != null) {
                imageView.setImageDrawable(drawable);
                return true;
            }
            ajVar.b = null;
        }
        imageView.setImageResource(this.d);
        ajVar.f311a = 0;
        return false;
    }

    private int b(int i2) {
        int i3;
        int i4 = 0;
        for (Map.Entry entry : this.g.entrySet()) {
            if (((Integer) entry.getValue()).intValue() == i2) {
                ImageView imageView = (ImageView) entry.getKey();
                aj ajVar = (aj) this.e.get(imageView);
                if (ajVar == null) {
                    aj ajVar2 = new aj();
                    ajVar2.f311a = 0;
                    this.e.put(Integer.valueOf(i2), ajVar2);
                } else {
                    ajVar.f311a = 0;
                }
                this.g.remove(imageView);
                this.f.put(imageView, Integer.valueOf(i2));
                i3 = i4 + 1;
            } else {
                i3 = i4;
            }
            i4 = i3;
        }
        return i4;
    }

    private void e() {
        if (!this.j) {
            this.j = true;
            this.h.sendEmptyMessage(1);
        }
    }

    public final void a() {
        this.k = true;
        if (this.i != null) {
            this.i.quit();
            this.i = null;
        }
        this.f.clear();
        this.e.clear();
        this.l.unregisterReceiver(this.m);
    }

    public final void a(int i2) {
        for (Map.Entry entry : this.f.entrySet()) {
            if (((Integer) entry.getValue()).intValue() == i2) {
                this.f.remove(entry.getKey());
                this.g.put(entry.getKey(), Integer.valueOf(i2));
            }
        }
    }

    public final void a(ContentResolver contentResolver, int i2, String str) {
        t.a(str, new f(), this.n, new Object[]{contentResolver, Integer.valueOf(i2)}, this.l);
    }

    public final void a(ImageView imageView, int i2, int i3) {
        this.f.remove(imageView);
        this.g.remove(imageView);
        if (i3 != 0) {
            imageView.setImageResource(i3);
        } else if (i2 == 0) {
            imageView.setImageResource(this.d);
        } else if (a(imageView, i2)) {
            this.f.remove(imageView);
        } else {
            this.f.put(imageView, Integer.valueOf(i2));
            if (!this.k) {
                e();
            }
        }
    }

    public final void b() {
        this.k = true;
    }

    public final void c() {
        this.k = false;
        if (!this.f.isEmpty()) {
            e();
        }
    }

    public final boolean handleMessage(Message message) {
        switch (message.what) {
            case 1:
                this.j = false;
                if (!this.k) {
                    if (this.i == null) {
                        this.i = new ak(this, this.l.getContentResolver());
                        this.i.start();
                    }
                    this.i.a();
                }
                return true;
            case 2:
                if (!this.k) {
                    Iterator it = this.f.keySet().iterator();
                    while (it.hasNext()) {
                        ImageView imageView = (ImageView) it.next();
                        if (a(imageView, ((Integer) this.f.get(imageView)).intValue())) {
                            it.remove();
                        }
                    }
                    if (!this.f.isEmpty()) {
                        e();
                    }
                }
                return true;
            default:
                return false;
        }
    }
}
