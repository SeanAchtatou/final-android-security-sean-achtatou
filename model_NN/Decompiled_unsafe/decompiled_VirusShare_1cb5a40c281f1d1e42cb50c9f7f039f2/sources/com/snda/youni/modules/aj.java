package com.snda.youni.modules;

import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.provider.ContactsContract;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import com.snda.youni.AppContext;
import com.snda.youni.C0000R;
import com.snda.youni.YouNi;
import com.snda.youni.activities.ChatActivity;
import com.snda.youni.activities.RecipientsActivity;
import com.snda.youni.d;
import com.snda.youni.e.j;
import com.snda.youni.e.l;
import com.snda.youni.e.r;
import com.snda.youni.e.t;
import com.snda.youni.g.a;
import com.snda.youni.modules.a.k;
import com.snda.youni.providers.n;

public final class aj implements AdapterView.OnItemClickListener, RadioGroup.OnCheckedChangeListener {

    /* renamed from: a  reason: collision with root package name */
    public k f518a;
    public boolean b = false;
    /* access modifiers changed from: private */
    public RadioGroup c;
    private RadioButton d;
    private RadioButton e;
    private RadioButton f;
    /* access modifiers changed from: private */
    public EditText g;
    private View h;
    /* access modifiers changed from: private */
    public View i;
    private ListView j;
    /* access modifiers changed from: private */
    public m k;
    /* access modifiers changed from: private */
    public YouNi l;
    /* access modifiers changed from: private */
    public ai m;
    private k n;
    /* access modifiers changed from: private */
    public ProgressDialog o;
    private final TextWatcher p;

    public aj(YouNi youNi, View view, d dVar) {
        this.l = youNi;
        this.c = (RadioGroup) view.findViewById(C0000R.id.group_contacts_btns);
        this.d = (RadioButton) this.c.findViewById(C0000R.id.btn_all);
        this.e = (RadioButton) this.c.findViewById(C0000R.id.btn_youni);
        this.f = (RadioButton) this.c.findViewById(C0000R.id.btn_favorites);
        this.g = (EditText) view.findViewById(C0000R.id.search_input_box);
        this.g.setFilters(new InputFilter[]{new l()});
        this.h = view.findViewById(C0000R.id.btn_new_contact);
        this.i = view.findViewById(C0000R.id.list_hint_empty);
        this.j = (ListView) view.findViewById(C0000R.id.list_contacts);
        this.k = new m(this.l, dVar);
        this.j.setAdapter((ListAdapter) this.k);
        this.j.setOnItemClickListener(this);
        this.j.setOnScrollListener(this.k);
        this.m = new ai(this, youNi.getContentResolver());
        this.m.startQuery(1, null, n.f597a, t.f578a, "contact_id > 0 ", null, null);
        this.p = new ak(this);
        this.c.setOnCheckedChangeListener(this);
        this.g.addTextChangedListener(this.p);
        this.h.setOnClickListener(new al(this));
    }

    private static k a(Cursor cursor) {
        String string = cursor.getString(2);
        String string2 = cursor.getString(3);
        k kVar = new k();
        kVar.f496a = string;
        kVar.g = string2;
        int i2 = cursor.getInt(1);
        String string3 = cursor.getString(5);
        kVar.i = Integer.valueOf(i2).intValue();
        kVar.l = string3;
        if (cursor.getInt(4) == 0) {
            kVar.f = false;
        } else {
            kVar.f = true;
        }
        return kVar;
    }

    private k a(ContextMenu.ContextMenuInfo contextMenuInfo) {
        AdapterView.AdapterContextMenuInfo adapterContextMenuInfo = (AdapterView.AdapterContextMenuInfo) contextMenuInfo;
        "long click pos: " + String.valueOf(adapterContextMenuInfo.position);
        return a((Cursor) this.k.getItem(adapterContextMenuInfo.position));
    }

    public final void a() {
        switch (this.c.getCheckedRadioButtonId()) {
            case C0000R.id.btn_all /*2131558642*/:
                this.m.startQuery(1, null, n.f597a, t.f578a, "contact_id > 0 ", null, null);
                return;
            case C0000R.id.btn_youni /*2131558643*/:
                this.m.startQuery(2, null, n.f597a, t.f578a, "contact_type=1", null, null);
                return;
            case C0000R.id.btn_favorites /*2131558644*/:
                this.m.startQuery(3, null, n.b, t.f578a, "times_contacted > 0 AND contact_id > 0 ", null, "times_contacted DESC");
                return;
            default:
                return;
        }
    }

    public final void a(int i2) {
        switch (i2) {
            case C0000R.id.btn_all /*2131558642*/:
                this.d.setChecked(true);
                return;
            case C0000R.id.btn_youni /*2131558643*/:
                this.e.setChecked(true);
                return;
            case C0000R.id.btn_favorites /*2131558644*/:
                this.f.setChecked(true);
                return;
            default:
                return;
        }
    }

    public final void a(Intent intent) {
        Intent intent2 = new Intent(this.l, RecipientsActivity.class);
        intent2.putExtra("item", intent.getSerializableExtra("item"));
        this.l.startActivity(intent2);
    }

    public final void a(MenuInflater menuInflater, ContextMenu contextMenu, ContextMenu.ContextMenuInfo contextMenuInfo) {
        k a2 = a(contextMenuInfo);
        if (a2 != null) {
            contextMenu.setHeaderTitle(a2.f496a);
            if (t.b(a2.g)) {
                menuInflater.inflate(C0000R.menu.contact_context_menu_robot, contextMenu);
            } else if (a2.f) {
                menuInflater.inflate(C0000R.menu.contact_context_menu_youni, contextMenu);
            } else {
                menuInflater.inflate(C0000R.menu.contact_context_menu_not_youni, contextMenu);
            }
        }
    }

    public final void a(k kVar, boolean z) {
        if (z) {
            ContentResolver contentResolver = this.l.getContentResolver();
            contentResolver.delete(n.f597a, "sid='" + j.a(this.n.g) + "'", null);
            String str = "contact_id=" + this.n.i + " AND " + "mimetype" + "='" + "vnd.android.cursor.item/phone_v2" + "'";
            Cursor query = contentResolver.query(ContactsContract.Data.CONTENT_URI, new String[]{"_id"}, str, null, null);
            "cursor.count=" + query.getCount();
            if (query == null || query.getCount() <= 1) {
                contentResolver.delete(ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, (long) this.n.i), null, null);
            } else {
                contentResolver.delete(ContactsContract.Data.CONTENT_URI, str + " AND PHONE_NUMBERS_EQUAL(" + "data1" + ",?)", new String[]{this.n.g});
            }
            if (query != null) {
                query.close();
            }
            a();
            return;
        }
        this.l.showDialog(13);
        this.n = kVar;
    }

    public final void a(int[] iArr) {
        String str = " IN (";
        for (int i2 = 0; i2 < iArr.length; i2++) {
            str = str + iArr[i2] + ",";
        }
        String str2 = str.substring(0, str.length() - 1) + ")";
        ContentResolver contentResolver = this.l.getContentResolver();
        Cursor query = contentResolver.query(ContactsContract.Contacts.CONTENT_URI, new String[]{"_id", "display_name"}, "_id" + str2, null, null);
        while (query.moveToNext()) {
            "" + query.getInt(0);
            query.getString(1);
        }
        contentResolver.delete(ContactsContract.Contacts.CONTENT_URI, "_id" + str2, null);
        contentResolver.delete(n.f597a, "contact_id" + str2, null);
        a();
    }

    public final boolean a(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case C0000R.id.menu_contact_call /*2131558680*/:
                String str = a(menuItem.getMenuInfo()).g;
                YouNi youNi = this.l;
                if (!(youNi == null || youNi.isFinishing() || str == null)) {
                    r.a(t.b(str) ? youNi.getString(C0000R.string.snda_services_phone_number) : str, this.l);
                }
                return true;
            case C0000R.id.menu_send_contact /*2131558681*/:
                k a2 = a(menuItem.getMenuInfo());
                k kVar = new k();
                kVar.b = a2.f496a + " " + a2.g;
                Intent intent = new Intent(this.l, RecipientsActivity.class);
                intent.putExtra("item", kVar);
                this.l.startActivity(intent);
                return true;
            case C0000R.id.menu_contact_edit /*2131558682*/:
                k a3 = a(menuItem.getMenuInfo());
                YouNi youNi2 = this.l;
                Intent intent2 = new Intent("android.intent.action.EDIT", ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, (long) a3.i));
                intent2.setFlags(268435456);
                youNi2.startActivity(intent2);
                return true;
            case C0000R.id.menu_contact_delete /*2131558683*/:
                a(a(menuItem.getMenuInfo()), false);
                return true;
            case C0000R.id.menu_contact_black_list_add /*2131558684*/:
                this.f518a = a(menuItem.getMenuInfo());
                this.l.removeDialog(18);
                this.l.showDialog(18);
                return true;
            default:
                return false;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public final void b() {
        Intent intent = new Intent(this.l, YouNi.class);
        intent.putExtra("contacts_batch_operations", true);
        switch (this.c.getCheckedRadioButtonId()) {
            case C0000R.id.btn_all /*2131558642*/:
                intent.putExtra("contacts_tab", 0);
                break;
            case C0000R.id.btn_youni /*2131558643*/:
                intent.putExtra("contacts_tab", 1);
                break;
            case C0000R.id.btn_favorites /*2131558644*/:
                intent.putExtra("contacts_tab", 2);
                break;
        }
        this.l.startActivityForResult(intent, 1314);
    }

    public final ListView c() {
        return this.j;
    }

    public final void d() {
        this.g.removeTextChangedListener(this.p);
        this.g.setText("");
        this.g.addTextChangedListener(this.p);
    }

    public final void e() {
        Cursor cursor = this.k.getCursor();
        if (cursor != null) {
            cursor.close();
        }
    }

    public final void f() {
        if (YouNi.f170a && this.b) {
            ContentValues contentValues = new ContentValues();
            contentValues.putNull("expand_data2");
            this.l.getContentResolver().update(n.f597a, contentValues, "expand_data2=?", new String[]{"new"});
            YouNi.f170a = false;
            this.l.findViewById(C0000R.id.new_youni_contacts).setVisibility(8);
            this.b = false;
        }
    }

    public final boolean g() {
        return this.c.getCheckedRadioButtonId() == C0000R.id.btn_youni;
    }

    public final void h() {
        this.c.check(C0000R.id.btn_youni);
    }

    public final void onCheckedChanged(RadioGroup radioGroup, int i2) {
        "RadioGroup's checkedId is: " + i2;
        switch (i2) {
            case C0000R.id.btn_all /*2131558642*/:
                d();
                this.m.startQuery(1, null, n.f597a, t.f578a, "contact_id > 0 ", null, null);
                break;
            case C0000R.id.btn_youni /*2131558643*/:
                a.a(this.l.getApplicationContext(), "youni_contact", null);
                d();
                this.m.startQuery(2, null, n.f597a, t.f578a, "contact_type=1", null, null);
                break;
            case C0000R.id.btn_favorites /*2131558644*/:
                a.a(this.l.getApplicationContext(), "common_contact", null);
                this.l.sendBroadcast(new Intent("com.snda.youni.ACTION_UPDATE_TIMES_CONTACTED"));
                d();
                this.m.startQuery(3, null, n.b, t.f578a, "times_contacted > 0 AND contact_id > 0 ", null, "times_contacted DESC");
                break;
            default:
                this.d.setChecked(true);
                this.e.setChecked(false);
                this.f.setChecked(false);
                break;
        }
        if (i2 != C0000R.id.btn_youni) {
            f();
        } else if (YouNi.f170a && !this.b) {
            ((AppContext) this.l.getApplication()).e();
            this.b = true;
            this.l.m();
        }
    }

    public final void onItemClick(AdapterView adapterView, View view, int i2, long j2) {
        "Contacts View's item is clicked: " + String.valueOf(i2);
        k a2 = a((Cursor) this.k.getItem(i2));
        if (this.l.i() == 1) {
            if (this.j.isItemChecked(i2)) {
                this.k.a(a2.f496a, a2.g, a2.i);
            } else {
                this.k.b(a2.f496a, a2.g, a2.i);
            }
            Intent intent = new Intent();
            a2.b = this.l.getIntent().getStringExtra("message_body");
            intent.putExtra("item", a2);
        } else {
            Intent intent2 = new Intent(this.l, ChatActivity.class);
            intent2.putExtra("item", a2);
            this.l.startActivity(intent2);
        }
        this.k.c();
    }
}
