package com.snda.youni.modules.popup;

import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.widget.Toast;
import com.snda.youni.C0000R;
import com.snda.youni.b.ai;
import com.snda.youni.e.q;
import com.snda.youni.e.t;
import com.snda.youni.h.a.a;
import com.snda.youni.modules.a.k;
import com.snda.youni.modules.d.b;
import com.snda.youni.services.YouniService;

public final class e {

    /* renamed from: a  reason: collision with root package name */
    private final a f558a;
    /* access modifiers changed from: private */
    public ai b = null;
    private final Context c;
    private ServiceConnection d = new b(this);

    public e(Context context) {
        this.c = context;
        this.f558a = new a(this.c.getApplicationContext());
        this.c.bindService(new Intent(this.c, YouniService.class), this.d, 1);
    }

    private void a(boolean z) {
        SharedPreferences.Editor edit = PreferenceManager.getDefaultSharedPreferences(this.c).edit();
        edit.putBoolean("chat_switch_youni", z);
        edit.commit();
    }

    public final void a() {
        this.c.unbindService(this.d);
    }

    public final void a(k kVar) {
        if (t.b(kVar.g)) {
            a(true);
        } else if (kVar.m) {
            a(true);
        } else {
            a(false);
        }
    }

    public final void a(String str) {
        if (this.b != null) {
            this.b.b(str);
        }
    }

    public final boolean a(String str, String str2, int i) {
        String stripSeparators = !t.b(str) ? q.stripSeparators(str) : str;
        "btn_send onclick, mAddress=" + stripSeparators + " message=" + str2;
        if (TextUtils.isEmpty(str2.trim())) {
            Toast.makeText(this.c, (int) C0000R.string.empty_message_warning, 0).show();
            return false;
        }
        "btn_send onclick, mAddress=" + stripSeparators + " message=" + str2;
        b bVar = new b();
        bVar.a(stripSeparators);
        bVar.b(str2);
        bVar.a(Long.valueOf(System.currentTimeMillis()));
        bVar.e(i == 1 ? "youni" : "sms");
        this.f558a.b(this.b, bVar);
        return true;
    }
}
