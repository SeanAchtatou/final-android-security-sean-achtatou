package com.snda.youni.modules.d;

import java.io.Serializable;

public final class b implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    private long f536a;
    private long b;
    private String c;
    private String d;
    private String e;
    private Long f;
    private boolean g;
    private boolean h;
    private String i;
    private String j;
    private String k;
    private boolean l = false;
    private boolean m = false;
    private String n;
    private String[] o;
    private long[] p;
    private com.snda.youni.attachment.b.b q = null;
    private String r = "";
    private String s;
    private int t;
    private String u;
    private String v;
    private String w;

    public final String a() {
        return this.c;
    }

    public final void a(long j2) {
        this.b = j2;
    }

    public final void a(com.snda.youni.attachment.b.b bVar) {
        this.q = bVar;
    }

    public final void a(Long l2) {
        this.f = l2;
    }

    public final void a(String str) {
        this.c = str;
    }

    public final void a(boolean z) {
        this.g = z;
    }

    public final void a(long[] jArr) {
        this.p = jArr;
    }

    public final void a(String[] strArr) {
        this.o = strArr;
    }

    public final String b() {
        return this.d;
    }

    public final void b(long j2) {
        this.f536a = j2;
    }

    public final void b(String str) {
        this.d = str;
    }

    public final void b(boolean z) {
        this.h = z;
    }

    public final String c() {
        return this.e;
    }

    public final void c(String str) {
        this.e = str;
    }

    public final void c(boolean z) {
        this.l = z;
    }

    public final Long d() {
        return this.f;
    }

    public final void d(String str) {
        this.v = str;
    }

    public final void d(boolean z) {
        this.m = z;
    }

    public final void e(String str) {
        this.w = str;
    }

    public final boolean e() {
        return this.g;
    }

    public final String f() {
        return this.v;
    }

    public final void f(String str) {
        this.i = str;
    }

    public final void g(String str) {
        this.j = str;
    }

    public final boolean g() {
        return this.h;
    }

    public final long h() {
        return this.b;
    }

    public final void h(String str) {
        this.k = str;
    }

    public final String i() {
        return this.w;
    }

    public final void i(String str) {
        this.s = str;
    }

    public final String j() {
        return this.i;
    }

    public final String k() {
        return this.j;
    }

    public final long l() {
        return this.f536a;
    }

    public final String m() {
        return this.k;
    }

    public final boolean n() {
        return this.l;
    }

    public final String[] o() {
        return this.o;
    }

    public final long[] p() {
        return this.p;
    }

    public final int q() {
        if (this.o != null) {
            return this.o.length;
        }
        if (this.p != null) {
            return this.p.length;
        }
        return 1;
    }

    public final boolean r() {
        return this.m;
    }

    public final com.snda.youni.attachment.b.b s() {
        return this.q;
    }

    /* renamed from: t */
    public final b clone() {
        b bVar = new b();
        bVar.b = this.b;
        bVar.c = this.c;
        bVar.d = this.d;
        bVar.e = this.e;
        bVar.f = this.f;
        bVar.g = this.g;
        bVar.h = this.h;
        bVar.i = this.i;
        bVar.j = this.j;
        bVar.k = this.k;
        bVar.l = this.l;
        bVar.m = this.m;
        bVar.o = this.o;
        bVar.p = this.p;
        return bVar;
    }

    public final String toString() {
        return "MessageObject [address=" + this.c + ", body=" + this.d + ", date=" + this.f + ", deliveryReport=" + this.g + ", id=" + this.f536a + ", isSecretary=" + this.l + ", messageType=" + this.w + ", msgCount=" + this.t + ", person=" + this.i + ", read=" + this.h + ", snippet=" + this.u + ", status=" + this.k + ", subject=" + this.e + ", location=" + this.n + ", threadId=" + this.b + ", type=" + this.v + ", youniId=" + this.j + "]";
    }

    public final String u() {
        return this.s;
    }
}
