package com.snda.youni.modules.c;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.text.format.Time;

final class a extends AsyncTask {

    /* renamed from: a  reason: collision with root package name */
    private Context f531a;
    private SharedPreferences b;

    a(Context context, SharedPreferences sharedPreferences) {
        this.f531a = context;
        this.b = sharedPreferences;
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ Object doInBackground(Object[] objArr) {
        Uri parse = Uri.parse("content://sms/");
        SharedPreferences.Editor edit = this.b.edit();
        Cursor query = this.f531a.getContentResolver().query(parse, new String[]{"count(*) as count "}, b.a(true, false), null, null);
        if (query != null && query.moveToFirst()) {
            "send_youni_count = " + query.getInt(0);
            edit.putInt("send_youni_count", query.getInt(0));
            query.close();
        }
        Cursor query2 = this.f531a.getContentResolver().query(parse, new String[]{"count(*) as count "}, b.a(true, true), null, null);
        if (query2 != null && query2.moveToFirst()) {
            "send_youni_month_count = " + query2.getInt(0);
            edit.putInt("send_youni_month_count", query2.getInt(0));
            query2.close();
        }
        Cursor query3 = this.f531a.getContentResolver().query(parse, new String[]{"count(*) as count "}, b.a(false, false), null, null);
        if (query3 != null && query3.moveToFirst()) {
            "SEND_ALL_COUNT = " + query3.getInt(0);
            edit.putInt("send_all_count", query3.getInt(0));
            query3.close();
        }
        Cursor query4 = this.f531a.getContentResolver().query(parse, new String[]{"count(*) as count "}, b.a(false, true), null, null);
        if (query4 != null && query4.moveToFirst()) {
            "send_all_month_count = " + query4.getInt(0);
            edit.putInt("send_all_month_count", query4.getInt(0));
            query4.close();
        }
        Cursor query5 = this.f531a.getContentResolver().query(parse, new String[]{"_id"}, " type=2 and thread_id is not null ", null, " _id desc limit 1");
        if (query5 != null && query5.moveToFirst()) {
            "send_stat_last_id = " + query5.getLong(0);
            edit.putLong("send_stat_last_id", query5.getLong(0));
            query5.close();
        }
        Cursor query6 = this.f531a.getContentResolver().query(parse, new String[]{"_id"}, " (type=4 or (type=2 and status = -1 and (protocol = 'youni' or protocol = 'youni_offline') )) and thread_id is not null ", null, " _id asc");
        if (query6 != null && query6.moveToFirst()) {
            StringBuffer stringBuffer = new StringBuffer();
            do {
                if (stringBuffer.length() > 0) {
                    stringBuffer.append(',');
                }
                stringBuffer.append(query6.getLong(0));
            } while (query6.moveToNext());
            "outbox_ids = " + stringBuffer.toString();
            edit.putString("outbox_ids", stringBuffer.toString());
            query6.close();
        }
        edit.putInt("send_stat_update_code", 2);
        Time time = new Time();
        time.set(System.currentTimeMillis());
        edit.putString("send_year_month", time.format("%Y%m"));
        edit.commit();
        return null;
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void onPostExecute(Object obj) {
        super.onPostExecute((Void) obj);
        boolean unused = b.f532a = false;
    }
}
