package com.snda.youni.modules;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.snda.youni.C0000R;
import com.snda.youni.d;
import com.snda.youni.d.a;
import com.snda.youni.providers.n;

public final class ah extends CursorAdapter {

    /* renamed from: a  reason: collision with root package name */
    public static String f516a;
    public static String b;
    Context c;
    private d d;

    public ah(Context context, d dVar) {
        super(context, null);
        this.c = context;
        this.d = dVar;
    }

    public final void bindView(View view, Context context, Cursor cursor) {
        String string = cursor.getString(1);
        String string2 = cursor.getString(2);
        ((TextView) view.findViewById(C0000R.id.blacker_name)).setText(string);
        ((TextView) view.findViewById(C0000R.id.blacker_phone)).setText(context.getString(C0000R.string.black_list_phone, string2));
        ImageView imageView = (ImageView) view.findViewById(C0000R.id.contact_photo);
        Cursor query = this.c.getContentResolver().query(n.f597a, new String[]{"contact_id"}, "PHONE_NUMBERS_EQUAL(phone_number,?)", new String[]{string2}, null);
        if (query.moveToFirst()) {
            this.d.a(imageView, query.getInt(0), 0);
        } else {
            this.d.a(imageView, 0, 0);
        }
        view.findViewById(C0000R.id.btn_remove_from_black_list).setOnClickListener(new e(this, cursor));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public final View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
        View inflate = LayoutInflater.from(context).inflate((int) C0000R.layout.item_list_black_list, viewGroup, false);
        inflate.findViewById(C0000R.id.btn_remove_from_black_list).setBackgroundDrawable(a.a("btn_bg"));
        return inflate;
    }
}
