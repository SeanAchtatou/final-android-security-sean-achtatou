package com.snda.youni.modules;

import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import com.snda.youni.activities.SettingsSkinActivity;
import com.snda.youni.d.b;
import com.snda.youni.d.c;

final class ag implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private int f515a;
    private /* synthetic */ d b;

    public ag(d dVar, int i) {
        this.b = dVar;
        this.f515a = i;
    }

    public final void onClick(View view) {
        if (view instanceof RadioButton) {
            if (this.f515a != this.b.c) {
                d dVar = this.b;
                if (b.a(((c) dVar.b.get(this.f515a)).a())) {
                    this.b.c = this.f515a;
                    ((SettingsSkinActivity) this.b.f534a).a();
                }
            }
        } else if (view instanceof Button) {
            ((SettingsSkinActivity) this.b.f534a).a(((c) this.b.b.get(this.f515a)).e);
        }
    }
}
