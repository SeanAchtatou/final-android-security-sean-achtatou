package com.snda.youni.modules;

import android.content.Intent;
import android.view.View;
import com.snda.youni.g.a;

final class al implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ aj f520a;

    al(aj ajVar) {
        this.f520a = ajVar;
    }

    public final void onClick(View view) {
        a.a(this.f520a.l.getApplicationContext(), "add_contact", null);
        Intent intent = new Intent("android.intent.action.INSERT");
        intent.setType("vnd.android.cursor.dir/contact");
        intent.setFlags(268435456);
        this.f520a.l.startActivity(intent);
    }
}
