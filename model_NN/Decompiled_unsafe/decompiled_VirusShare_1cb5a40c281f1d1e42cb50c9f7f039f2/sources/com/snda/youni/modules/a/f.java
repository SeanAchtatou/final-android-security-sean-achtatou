package com.snda.youni.modules.a;

import android.view.View;

final class f implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ k f493a;
    private /* synthetic */ t b;

    f(t tVar, k kVar) {
        this.b = tVar;
        this.f493a = kVar;
    }

    public final void onClick(View view) {
        if (this.b.b == null) {
            return;
        }
        if (this.f493a.e) {
            this.b.b.b(this.f493a);
        } else {
            this.b.b.a(this.f493a);
        }
    }
}
