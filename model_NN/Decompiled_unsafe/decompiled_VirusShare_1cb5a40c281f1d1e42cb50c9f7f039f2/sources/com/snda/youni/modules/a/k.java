package com.snda.youni.modules.a;

import android.content.Context;
import android.preference.PreferenceManager;
import com.snda.youni.C0000R;
import com.snda.youni.e.t;
import java.io.Serializable;

public final class k implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    public String f496a;
    public String b;
    public String c;
    public String d;
    public boolean e;
    public boolean f;
    public String g;
    public String h;
    public int i;
    public String j;
    public String k;
    public String l;
    public boolean m;
    public String n;
    public String o;
    public String[] p;
    private String q;

    public final void a(Context context, b bVar, String str) {
        this.g = str;
        a a2 = bVar.a(t.a(str));
        if (str == null || !str.startsWith("krobot")) {
            if (a2 != null) {
                this.i = a2.f489a;
                this.l = a2.d;
                if (a2.b != null && a2.b != "") {
                    this.f496a = a2.b;
                } else if (a2.c == null || a2.c == "") {
                    this.f496a = str;
                } else {
                    this.f496a = a2.c;
                }
                this.e = true;
                this.f = a2.f;
            } else {
                this.f496a = str;
                if (PreferenceManager.getDefaultSharedPreferences(context).getBoolean("contacts_synced", false)) {
                    this.e = false;
                } else {
                    this.e = true;
                }
                this.f = false;
            }
            if (this.n != null && this.n.split(" ").length > 1) {
                this.f496a = context.getString(C0000R.string.inbox_multi_name, this.f496a, Integer.valueOf(this.n.split(" ").length));
            }
            if (this.p != null) {
                this.f496a = context.getString(C0000R.string.inbox_multi_name, this.f496a, Integer.valueOf(this.p.length));
                return;
            }
            return;
        }
        this.e = true;
        this.f = true;
        this.f496a = context.getString(C0000R.string.youni_robot);
        if (a2 != null) {
            this.l = a2.d;
            this.i = a2.f489a;
        }
    }

    public final String toString() {
        return this.f496a + "~~" + this.g + "~~" + this.h + "~~" + this.k + "~~" + this.b + "~~" + this.d + "~~" + this.l + "~~" + this.q + "~~" + this.e;
    }
}
