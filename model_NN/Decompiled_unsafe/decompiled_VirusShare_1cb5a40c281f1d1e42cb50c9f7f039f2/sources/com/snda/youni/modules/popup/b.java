package com.snda.youni.modules.popup;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;
import com.snda.youni.b.ai;
import com.snda.youni.services.YouniService;

final class b implements ServiceConnection {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ e f556a;

    b(e eVar) {
        this.f556a = eVar;
    }

    public final void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        ai unused = this.f556a.b = YouniService.b();
        "on Service Connected network:" + this.f556a.b;
    }

    public final void onServiceDisconnected(ComponentName componentName) {
        ai unused = this.f556a.b = null;
    }
}
