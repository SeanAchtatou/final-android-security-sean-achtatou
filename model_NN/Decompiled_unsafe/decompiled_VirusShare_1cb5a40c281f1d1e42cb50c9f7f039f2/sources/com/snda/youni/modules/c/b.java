package com.snda.youni.modules.c;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.telephony.SmsManager;
import android.text.TextUtils;
import android.text.format.Time;
import com.snda.youni.g.a;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;

public final class b {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public static boolean f532a = false;

    static /* synthetic */ String a(boolean z, boolean z2) {
        int i;
        int i2;
        String str = " type=2 and (status=0 or (protocol != 'youni' and protocol != 'youni_offline')) and thread_id is not null ";
        if (z) {
            str = str + " and (protocol = 'youni' or protocol = 'youni_offline') ";
        }
        if (!z2) {
            return str;
        }
        Calendar instance = Calendar.getInstance();
        int i3 = instance.get(1);
        int i4 = instance.get(2);
        "year = " + i3 + ", month = " + i4;
        instance.clear();
        instance.set(i3, i4, 0);
        long timeInMillis = instance.getTimeInMillis();
        int i5 = i4 + 1;
        if (i5 == 12) {
            i = i3 + 1;
            i2 = 0;
        } else {
            int i6 = i5;
            i = i3;
            i2 = i6;
        }
        instance.set(i, i2, 0);
        long timeInMillis2 = instance.getTimeInMillis();
        "curMonthTime = " + timeInMillis + ", nextMonthTime = " + timeInMillis2 + ", currTime = " + System.currentTimeMillis();
        return str + " and date>=" + timeInMillis + " and date<" + timeInMillis2;
    }

    public static void a(Context context) {
        SharedPreferences defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        if (defaultSharedPreferences.getInt("send_stat_update_code", 0) < 2 && !f532a) {
            f532a = true;
            new a(context, defaultSharedPreferences).execute(new String[0]);
        }
    }

    public static int[] a(SharedPreferences sharedPreferences) {
        return new int[]{sharedPreferences.getInt("send_all_count", 0), sharedPreferences.getInt("send_youni_count", 0)};
    }

    public static void b(Context context) {
        SharedPreferences defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        if (defaultSharedPreferences.getInt("send_stat_update_code", 0) >= 2) {
            long j = defaultSharedPreferences.getLong("send_stat_last_id", 0);
            String string = defaultSharedPreferences.getString("outbox_ids", "");
            String str = " (type = 2 or type = 4) and thread_id is not null and (_id > " + j;
            Cursor query = context.getContentResolver().query(Uri.parse("content://sms/"), new String[]{"_id", "type", "protocol", "body", "status", "address"}, (!TextUtils.isEmpty(string) ? str + " or _id in (" + string + ")" : str) + ")", null, "_id asc");
            "send_stat_last_id = " + j + ", cursor count = " + (query == null ? null : Integer.valueOf(query.getCount()));
            if (query != null && query.moveToFirst()) {
                StringBuffer stringBuffer = new StringBuffer();
                boolean z = false;
                long j2 = j;
                do {
                    String string2 = query.getString(query.getColumnIndex("type"));
                    long j3 = query.getLong(query.getColumnIndex("_id"));
                    int i = query.getInt(query.getColumnIndex("status"));
                    String string3 = query.getString(query.getColumnIndex("protocol"));
                    boolean z2 = false;
                    if (string3 != null && (string3.equalsIgnoreCase("youni") || string3.equalsIgnoreCase("youni_offline"))) {
                        z2 = true;
                    }
                    if (!"2".equalsIgnoreCase(string2) || (i != 0 && z2)) {
                        if (stringBuffer.length() > 0) {
                            stringBuffer.append(',');
                        }
                        stringBuffer.append(j3);
                    } else {
                        if (j3 > j2) {
                            j2 = j3;
                        }
                        int i2 = 1;
                        if (!z2) {
                            String string4 = query.getString(query.getColumnIndex("body"));
                            if (string4.length() > 70) {
                                i2 = SmsManager.getDefault().divideMessage(string4).size();
                            }
                        }
                        SharedPreferences.Editor edit = defaultSharedPreferences.edit();
                        edit.putInt("send_all_count", defaultSharedPreferences.getInt("send_all_count", 0) + i2);
                        if (z2) {
                            edit.putInt("send_youni_count", defaultSharedPreferences.getInt("send_youni_count", 0) + i2);
                        }
                        Time time = new Time();
                        time.set(System.currentTimeMillis());
                        String format = time.format("%Y%m");
                        "currYearMonth = " + format;
                        if (format.equalsIgnoreCase(defaultSharedPreferences.getString("send_year_month", ""))) {
                            edit.putInt("send_all_month_count", defaultSharedPreferences.getInt("send_all_month_count", 0) + i2);
                            if (z2) {
                                edit.putInt("send_youni_month_count", defaultSharedPreferences.getInt("send_youni_month_count", 0) + i2);
                            }
                        } else {
                            edit.putInt("send_all_month_count", i2);
                            if (z2) {
                                edit.putInt("send_youni_month_count", i2);
                            }
                            edit.putString("send_year_month", format);
                        }
                        edit.commit();
                        "sendstat----add------ protocol: " + string3 + ", last_id = " + j3 + ", type = " + string2 + ", num = " + i2 + ", status = " + i;
                        a.a(context, "sys_send_all", null, i2);
                        if (z2) {
                            a.a(context, "send_youni", null);
                            String string5 = query.getString(query.getColumnIndex("address"));
                            if (string5 != null && string5.contains("krobot_001")) {
                                a.a(context, "sec_send", null);
                            }
                        } else {
                            a.a(context, "youni_send_sms", null);
                            z = true;
                        }
                    }
                } while (query.moveToNext());
                query.close();
                SharedPreferences.Editor edit2 = defaultSharedPreferences.edit();
                edit2.putLong("send_stat_last_id", j2);
                edit2.putString("outbox_ids", stringBuffer.toString());
                if (z) {
                    String format2 = DateFormat.getDateInstance(2).format(new Date());
                    if ("".equalsIgnoreCase(defaultSharedPreferences.getString("sts_is_send_sms" + format2, ""))) {
                        edit2.putString("sts_is_send_sms" + format2, "yes");
                    }
                }
                edit2.commit();
                "outbox_ids = " + stringBuffer.toString();
            }
        }
    }

    public static int[] b(SharedPreferences sharedPreferences) {
        int[] iArr = new int[2];
        Time time = new Time();
        time.set(System.currentTimeMillis());
        String format = time.format("%Y%m");
        if (format.equalsIgnoreCase(sharedPreferences.getString("send_year_month", ""))) {
            iArr[0] = sharedPreferences.getInt("send_all_month_count", 0);
            iArr[1] = sharedPreferences.getInt("send_youni_month_count", 0);
        } else {
            SharedPreferences.Editor edit = sharedPreferences.edit();
            edit.putInt("send_all_month_count", 0);
            edit.putInt("send_youni_month_count", 0);
            edit.putString("send_year_month", format);
            edit.commit();
        }
        return iArr;
    }
}
