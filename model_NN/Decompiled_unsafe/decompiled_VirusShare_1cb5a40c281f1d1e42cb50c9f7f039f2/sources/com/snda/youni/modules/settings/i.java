package com.snda.youni.modules.settings;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.net.Uri;
import android.preference.PreferenceManager;
import com.snda.youni.C0000R;
import com.snda.youni.e.n;
import java.io.IOException;

public final class i {
    public static void a(Context context) {
        SharedPreferences defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        if (defaultSharedPreferences.getInt("sound_type_name", 0) == 0) {
            a(context, defaultSharedPreferences.getInt("sound_youni_name", 0));
            return;
        }
        try {
            n.a(context).a(Uri.parse(defaultSharedPreferences.getString("sound_uri_name", "")));
        } catch (IOException e) {
            e.printStackTrace();
            a(context, 0);
        }
    }

    public static void a(Context context, int i) {
        Resources resources = context.getResources();
        TypedArray obtainTypedArray = resources.obtainTypedArray(C0000R.array.sound_details_file_name);
        int identifier = resources.getIdentifier(obtainTypedArray.getString(i), "raw", context.getPackageName());
        if (identifier > 0) {
            try {
                n.a(context).a(identifier);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        obtainTypedArray.recycle();
    }
}
