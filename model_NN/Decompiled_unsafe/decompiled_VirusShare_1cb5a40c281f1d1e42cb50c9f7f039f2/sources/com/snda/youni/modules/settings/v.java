package com.snda.youni.modules.settings;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public final class v {
    public static void a(Context context) {
        SharedPreferences defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor edit = defaultSharedPreferences.edit();
        a(defaultSharedPreferences, edit, "color_name", 0);
        a(defaultSharedPreferences, edit, "shape_name", 0);
        a(defaultSharedPreferences, edit, "sound_youni_name", 0);
        a(defaultSharedPreferences, edit, "sound_type_name", 0);
        a(defaultSharedPreferences, edit, "vibrate_name", 0);
        a(defaultSharedPreferences, edit, "vibrator_num", 2);
        a(defaultSharedPreferences, edit, "remind_name", 2);
        a(defaultSharedPreferences, edit, "popup_name", 1);
        a(defaultSharedPreferences, edit, "notify_name", 0);
        a(defaultSharedPreferences, edit, "chat_size_name", 14);
        a(defaultSharedPreferences, edit, "image_size_name", 1);
        edit.commit();
    }

    private static void a(SharedPreferences sharedPreferences, SharedPreferences.Editor editor, String str, int i) {
        if (!sharedPreferences.contains(str)) {
            editor.putInt(str, i);
        }
    }
}
