package com.snda.youni.modules.settings;

public abstract class m extends y {

    /* renamed from: a  reason: collision with root package name */
    protected int f572a;
    private int c;

    public m(SettingsItemView settingsItemView, String str, int i, int i2) {
        super(settingsItemView, str);
        this.c = i;
        this.f572a = i2;
    }

    public int a() {
        return c();
    }

    public void a(int i) {
        a_(i);
    }

    public final int a_() {
        return this.c;
    }

    public final int d() {
        return this.f572a;
    }
}
