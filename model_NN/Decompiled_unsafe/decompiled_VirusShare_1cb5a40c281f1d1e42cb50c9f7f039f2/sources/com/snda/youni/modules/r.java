package com.snda.youni.modules;

public final class r {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public String f561a;
    /* access modifiers changed from: private */
    public String b;
    /* access modifiers changed from: private */
    public int c;
    private /* synthetic */ m d;

    public r(m mVar, String str, String str2, int i) {
        this.d = mVar;
        this.f561a = str;
        this.b = str2;
        this.c = i;
    }

    public final boolean equals(Object obj) {
        if (!(obj instanceof r)) {
            return false;
        }
        r rVar = (r) obj;
        return this.f561a.equals(rVar.f561a) && this.b.equals(rVar.b);
    }
}
