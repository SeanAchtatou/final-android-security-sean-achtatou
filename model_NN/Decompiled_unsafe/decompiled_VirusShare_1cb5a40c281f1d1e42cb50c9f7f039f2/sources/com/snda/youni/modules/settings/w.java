package com.snda.youni.modules.settings;

import android.widget.Toast;
import com.snda.youni.C0000R;
import com.snda.youni.c.m;
import com.snda.youni.f.a;
import com.snda.youni.f.c;
import com.snda.youni.f.d;

final class w implements a {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ d f576a;

    w(d dVar) {
        this.f576a = dVar;
    }

    public final void a(c cVar, d dVar) {
        m mVar = (m) dVar.b();
        if (dVar.c() == 0 && mVar != null && mVar.b() == 0) {
            this.f576a.b.a();
            Toast.makeText(this.f576a.f567a, (int) C0000R.string.settings_modification_succeeded, 0).show();
            return;
        }
        this.f576a.b.f.setImageBitmap(this.f576a.b.c);
        Toast.makeText(this.f576a.f567a, (int) C0000R.string.settings_modification_failed, 0).show();
    }

    public final void a(Exception exc, String str) {
        this.f576a.b.f.setImageBitmap(this.f576a.b.c);
        Toast.makeText(this.f576a.f567a, (int) C0000R.string.settings_modification_failed, 0).show();
    }
}
