package com.snda.youni.modules.a;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;

final class g extends AsyncTask {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ e f494a;

    /* synthetic */ g(e eVar) {
        this(eVar, (byte) 0);
    }

    private g(e eVar, byte b) {
        this.f494a = eVar;
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ Object doInBackground(Object[] objArr) {
        return this.f494a.k.a(l.f497a);
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void onPostExecute(Object obj) {
        Cursor cursor = (Cursor) obj;
        Context context = (Context) this.f494a.e.get();
        if (context != null && !((Activity) context).isFinishing()) {
            ((Activity) context).runOnUiThread(new o(this, cursor));
        }
    }
}
