package com.snda.youni.modules.chat;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import com.snda.youni.AppContext;

public final class b {
    public static String a() {
        return PreferenceManager.getDefaultSharedPreferences(AppContext.a()).getString("self_phone_number", null);
    }

    public static void a(String str) {
        SharedPreferences.Editor edit = PreferenceManager.getDefaultSharedPreferences(AppContext.a()).edit();
        edit.putString("global_thread_id", str);
        edit.commit();
    }

    public static void a(boolean z) {
        "setPrefSendMethod:" + z;
        SharedPreferences.Editor edit = PreferenceManager.getDefaultSharedPreferences(AppContext.a()).edit();
        edit.putBoolean("chat_switch_youni", z);
        edit.commit();
    }
}
