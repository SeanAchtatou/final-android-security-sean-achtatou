package com.snda.youni.modules.settings;

import android.content.res.TypedArray;
import com.snda.youni.C0000R;
import com.snda.youni.g.a;

public final class q extends x {
    public q(SettingsItemView settingsItemView) {
        super(settingsItemView, "popup_name");
    }

    /* access modifiers changed from: protected */
    public final void a(boolean z) {
        TypedArray obtainTypedArray = this.b.getContext().getResources().obtainTypedArray(C0000R.array.popup_details);
        int i = z ? 0 : 1;
        a.b(this.b.getContext(), "popup_window", "" + z);
        this.b.c(obtainTypedArray.getString(i));
        obtainTypedArray.recycle();
        super.a(z);
    }
}
