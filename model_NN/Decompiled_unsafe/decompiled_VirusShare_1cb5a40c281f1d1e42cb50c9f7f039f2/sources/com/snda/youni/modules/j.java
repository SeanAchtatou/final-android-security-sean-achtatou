package com.snda.youni.modules;

import android.view.MotionEvent;
import android.view.View;

final class j implements View.OnTouchListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ InputView f542a;

    j(InputView inputView) {
        this.f542a = inputView;
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        if (this.f542a.f == null) {
            return false;
        }
        this.f542a.f.b();
        return false;
    }
}
