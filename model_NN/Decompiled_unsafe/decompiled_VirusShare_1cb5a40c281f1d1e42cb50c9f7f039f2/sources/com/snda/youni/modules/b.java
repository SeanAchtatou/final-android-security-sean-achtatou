package com.snda.youni.modules;

import android.graphics.BitmapFactory;
import com.snda.youni.c.n;
import com.snda.youni.f.a;
import com.snda.youni.f.c;
import com.snda.youni.f.d;

final class b implements a {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ q f522a;

    /* synthetic */ b(q qVar) {
        this(qVar, (byte) 0);
    }

    private b(q qVar, byte b) {
        this.f522a = qVar;
    }

    public final void a(c cVar, d dVar) {
        n nVar = (n) dVar.b();
        if (dVar.c() == 0 && nVar != null) {
            byte[] b = nVar.b();
            this.f522a.f560a.g().a(BitmapFactory.decodeByteArray(b, 0, b.length));
        }
    }

    public final void a(Exception exc, String str) {
    }
}
