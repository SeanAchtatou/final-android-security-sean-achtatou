package com.snda.youni.modules.newchat;

import android.content.Context;
import android.graphics.Paint;
import android.graphics.Rect;
import android.text.Annotation;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Selection;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import com.snda.youni.C0000R;
import com.snda.youni.activities.RecipientsActivity;
import com.snda.youni.e.q;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

public class RecipientsEditor extends EditText {

    /* renamed from: a  reason: collision with root package name */
    private static final char[] f546a = {' ', 10, 13, 9, ',', 65292, ';', 65307};
    private Context b;
    private LayoutInflater c = ((LayoutInflater) this.b.getSystemService("layout_inflater"));
    private int d;
    private int e;

    public RecipientsEditor(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.b = context;
        addTextChangedListener(new e(this));
        setImeOptions(5);
        setFilters(new InputFilter[]{new d(this)});
        this.d = (int) context.getResources().getDimension(C0000R.dimen.newchat_body_cannel_width);
        this.e = (int) context.getResources().getDimension(C0000R.dimen.newchat_body_over_width);
    }

    private f a(String str) {
        String str2;
        float measureText;
        TextView textView = (TextView) this.c.inflate((int) C0000R.layout.recipients_view, (ViewGroup) null);
        Paint paint = new Paint();
        paint.setTextSize(textView.getTextSize());
        int width = (getWidth() - getPaddingLeft()) - getPaddingRight();
        float measureText2 = paint.measureText(str);
        if (measureText2 > ((float) (width - this.e))) {
            float measureText3 = paint.measureText("...");
            "noPaddingWidth = " + width + ", measureWidth = " + measureText2 + ", dw = " + measureText3;
            int length = str.length();
            do {
                length--;
                measureText = paint.measureText(str, 0, length);
            } while (measureText > ((float) (width - this.e)) - measureText3);
            " end = " + length + ", measureWidth = " + measureText;
            if (length < str.length()) {
                str2 = str.substring(0, length) + "...";
                "displayName = " + str2;
                textView.setText(str2);
                return new f(this.b, textView, getTextSize());
            }
        }
        str2 = str;
        "displayName = " + str2;
        textView.setText(str2);
        return new f(this.b, textView, getTextSize());
    }

    public static String a(Spanned spanned, int i, int i2) {
        String str;
        Annotation[] annotationArr = (Annotation[]) spanned.getSpans(i, i2, Annotation.class);
        int i3 = 0;
        while (true) {
            if (i3 >= annotationArr.length) {
                str = "";
                break;
            } else if (annotationArr[i3].getKey().equals("number")) {
                str = annotationArr[i3].getValue();
                break;
            } else {
                i3++;
            }
        }
        return TextUtils.isEmpty(str) ? TextUtils.substring(spanned, i, i2) : str;
    }

    public static boolean a(char c2) {
        for (char c3 : f546a) {
            if (c2 == c3) {
                return true;
            }
        }
        return false;
    }

    public static boolean a(CharSequence charSequence, int i) {
        if (i <= 0) {
            return false;
        }
        if (a(charSequence.charAt(i - 1))) {
            return false;
        }
        f[] fVarArr = (f[]) ((Spanned) charSequence).getSpans(0, charSequence.length(), f.class);
        if (fVarArr.length > 0) {
            for (f spanEnd : fVarArr) {
                if (i == ((Spanned) charSequence).getSpanEnd(spanEnd)) {
                    return false;
                }
            }
        }
        return true;
    }

    public static String b(CharSequence charSequence, int i) {
        int c2 = c(charSequence, i);
        int d2 = d(charSequence, i);
        return c2 >= d2 ? "" : charSequence.subSequence(c2, d2).toString();
    }

    private static int c(CharSequence charSequence, int i) {
        if (i <= 0) {
            return 0;
        }
        f[] fVarArr = (f[]) ((Spanned) charSequence).getSpans(0, charSequence.length(), f.class);
        if (fVarArr.length == 0) {
            int i2 = i;
            while (i2 > 0 && !a(charSequence.charAt(i2 - 1))) {
                i2--;
            }
            return i2;
        }
        ArrayList arrayList = new ArrayList();
        for (f fVar : fVarArr) {
            int spanStart = ((Spanned) charSequence).getSpanStart(fVar);
            int spanEnd = ((Spanned) charSequence).getSpanEnd(fVar);
            arrayList.add(Integer.valueOf(spanStart));
            arrayList.add(Integer.valueOf(spanEnd));
        }
        Collections.sort(arrayList);
        if (i < ((Integer) arrayList.get(0)).intValue()) {
            int i3 = i;
            while (i3 > 0 && !a(charSequence.charAt(i3 - 1))) {
                i3--;
            }
            return i3;
        }
        int intValue = ((Integer) arrayList.get(arrayList.size() - 1)).intValue();
        if (i >= intValue) {
            int i4 = i;
            while (i4 > intValue && !a(charSequence.charAt(i4 - 1))) {
                i4--;
            }
            return i4;
        }
        int size = arrayList.size();
        int i5 = 1;
        while (i5 < size - 1) {
            int intValue2 = ((Integer) arrayList.get(i5)).intValue();
            int intValue3 = ((Integer) arrayList.get(i5 + 1)).intValue();
            if (i <= intValue2 || i >= intValue3) {
                i5 += 2;
            } else {
                int i6 = i;
                while (i6 >= intValue2 && !a(charSequence.charAt(i6 - 1))) {
                    i6--;
                }
                return i6;
            }
        }
        return i;
    }

    private static int d(CharSequence charSequence, int i) {
        if (i <= 0) {
            return 0;
        }
        f[] fVarArr = (f[]) ((Spanned) charSequence).getSpans(0, charSequence.length(), f.class);
        if (fVarArr.length == 0) {
            int length = charSequence.length();
            int i2 = i;
            while (i2 < length && !a(charSequence.charAt(i2))) {
                i2++;
            }
            return i2;
        }
        ArrayList arrayList = new ArrayList();
        for (f fVar : fVarArr) {
            int spanStart = ((Spanned) charSequence).getSpanStart(fVar);
            int spanEnd = ((Spanned) charSequence).getSpanEnd(fVar);
            arrayList.add(Integer.valueOf(spanStart));
            arrayList.add(Integer.valueOf(spanEnd));
        }
        Collections.sort(arrayList);
        int intValue = ((Integer) arrayList.get(0)).intValue();
        if (i <= intValue) {
            int i3 = i;
            while (i3 < intValue && !a(charSequence.charAt(i3))) {
                i3++;
            }
            return i3;
        } else if (i > ((Integer) arrayList.get(arrayList.size() - 1)).intValue()) {
            int length2 = charSequence.length();
            int i4 = i;
            while (i4 < length2 && !a(charSequence.charAt(i4))) {
                i4++;
            }
            return i4;
        } else {
            int size = arrayList.size();
            int i5 = 1;
            while (i5 < size - 1) {
                int intValue2 = ((Integer) arrayList.get(i5)).intValue();
                int intValue3 = ((Integer) arrayList.get(i5 + 1)).intValue();
                if (i <= intValue2 || i > intValue3) {
                    i5 += 2;
                } else {
                    int i6 = i;
                    while (i6 < intValue3 && !a(charSequence.charAt(i6))) {
                        i6++;
                    }
                    return i6;
                }
            }
            return i;
        }
    }

    public final void a(Editable editable, int i) {
        int c2 = c(editable, i);
        int d2 = d(editable, i);
        if (c2 < d2) {
            CharSequence subSequence = editable.subSequence(c2, d2);
            String stripSeparators = q.stripSeparators(subSequence.toString());
            if (q.isWellFormedSmsAddress(stripSeparators)) {
                b i2 = ((RecipientsActivity) this.b).i();
                if (i2.a(subSequence.toString(), stripSeparators) == 1) {
                    a(i2);
                    ((RecipientsActivity) this.b).g();
                    return;
                }
                String obj = subSequence.toString();
                SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(obj);
                spannableStringBuilder.setSpan(a(obj), 0, obj.length(), 33);
                editable.replace(c2, d2, spannableStringBuilder, 0, spannableStringBuilder.length());
                int i3 = c2;
                while (i3 > 0 && a(editable.charAt(i3 - 1))) {
                    i3--;
                }
                editable.replace(i3, c2, "");
                int i4 = d2;
                while (i4 < editable.length() && a(editable.charAt(i4))) {
                    i4++;
                }
                editable.replace(d2, i4, "");
                return;
            }
            int i5 = c2;
            while (i5 > 0 && a(editable.charAt(i5 - 1))) {
                i5--;
            }
            int i6 = d2;
            while (i6 < editable.length() && a(editable.charAt(i6))) {
                i6++;
            }
            editable.replace(i5, i6, "");
        }
    }

    public final void a(ArrayList arrayList) {
        if (getSelectionStart() != getText().length()) {
            setSelection(getText().length());
        }
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder();
        Iterator it = arrayList.iterator();
        while (it.hasNext()) {
            g gVar = (g) it.next();
            String a2 = gVar.a();
            SpannableString spannableString = new SpannableString(a2);
            f a3 = a(a2);
            int length = a2.length();
            spannableString.setSpan(a3, 0, length, 33);
            spannableString.setSpan(new Annotation("number", gVar.b()), 0, length, 33);
            spannableStringBuilder.append((CharSequence) spannableString);
        }
        setText(spannableStringBuilder);
        Editable text = getText();
        Selection.setSelection(text, text.length());
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i != 67 || getSelectionStart() == getText().length()) {
            return super.onKeyDown(i, keyEvent);
        }
        setSelection(getText().length());
        return true;
    }

    /* access modifiers changed from: protected */
    public void onSelectionChanged(int i, int i2) {
        if (i == getText().length()) {
            setCursorVisible(true);
        } else {
            setCursorVisible(false);
        }
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        int x = (int) motionEvent.getX();
        int y = (int) motionEvent.getY();
        if (motionEvent.getAction() == 0) {
            Editable text = getText();
            int scrollX = getScrollX() + x;
            int scrollY = y + getScrollY();
            Rect rect = new Rect();
            getLineBounds(0, rect);
            int i = rect.right - rect.left;
            "lineWidth = " + i;
            int i2 = 0;
            f[] fVarArr = (f[]) text.getSpans(0, text.length(), f.class);
            if (fVarArr != null) {
                int i3 = 0;
                int i4 = 0;
                while (true) {
                    if (i4 >= fVarArr.length) {
                        break;
                    }
                    f fVar = fVarArr[i4];
                    Rect bounds = fVar.getDrawable().getBounds();
                    if ((bounds.right - bounds.left) + i2 > i) {
                        i3++;
                        i2 = 0;
                    }
                    if (i3 >= 0) {
                        Rect rect2 = new Rect();
                        getLineBounds(i3, rect2);
                        int i5 = ((rect2.left + i2) + bounds.right) - this.d;
                        int i6 = rect2.left + i2 + bounds.right;
                        int i7 = ((rect2.bottom - rect2.top) - (bounds.bottom - bounds.top)) >> 1;
                        Rect rect3 = new Rect(i5, rect2.top + i7, i6, rect2.bottom - i7);
                        "rect = " + rect3 + ", offsetY = " + i7 + ", x = " + scrollX + ", y = " + scrollY;
                        if (rect3.contains(scrollX, scrollY)) {
                            "click wdspan " + fVar + " i = " + i4;
                            ((RecipientsActivity) this.b).c(i4);
                            text.replace(text.getSpanStart(fVar), text.getSpanEnd(fVar), "");
                            break;
                        }
                    }
                    i2 += bounds.right - bounds.left;
                    i4++;
                }
            }
        }
        return super.onTouchEvent(motionEvent);
    }
}
