package com.snda.youni.modules.settings;

import android.content.Context;
import android.content.DialogInterface;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import com.snda.youni.c.o;
import com.snda.youni.c.t;

final class g implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Context f569a;
    final /* synthetic */ int b;
    final /* synthetic */ j c;
    private /* synthetic */ EditText d;

    g(j jVar, Context context, EditText editText, int i) {
        this.c = jVar;
        this.f569a = context;
        this.d = editText;
        this.b = i;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        InputMethodManager inputMethodManager = (InputMethodManager) this.f569a.getSystemService("input_method");
        if (inputMethodManager != null) {
            inputMethodManager.hideSoftInputFromWindow(this.d.getWindowToken(), 2);
        }
        String obj = this.d.getText().toString();
        o oVar = new o();
        if (this.b == 0) {
            String unused = this.c.d = this.c.g.getText().toString();
            this.c.g.setText(obj);
            oVar.a("nickname", obj);
        } else {
            this.c.h.setText(obj);
            String unused2 = this.c.e = this.c.h.getText().toString();
            oVar.a("signature", obj);
        }
        "Going to upload new text: " + obj;
        t.a(oVar, new n(this), this.f569a);
    }
}
