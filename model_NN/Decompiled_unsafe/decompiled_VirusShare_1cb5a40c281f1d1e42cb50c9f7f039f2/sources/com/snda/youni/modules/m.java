package com.snda.youni.modules;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.BackgroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.CheckBox;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import com.snda.youni.C0000R;
import com.snda.youni.YouNi;
import com.snda.youni.b.ai;
import com.snda.youni.d;
import com.snda.youni.h.a.a;
import java.util.ArrayList;
import java.util.List;

public final class m extends CursorAdapter implements AbsListView.OnScrollListener {

    /* renamed from: a  reason: collision with root package name */
    private boolean f544a = false;
    private d b;
    private Context c;
    private a d;
    private boolean e = false;
    private String f;
    private int g;
    private List h;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.widget.CursorAdapter.<init>(android.content.Context, android.database.Cursor, boolean):void}
     arg types: [android.content.Context, ?[OBJECT, ARRAY], int]
     candidates:
      ClspMth{android.widget.CursorAdapter.<init>(android.content.Context, android.database.Cursor, int):void}
      ClspMth{android.widget.CursorAdapter.<init>(android.content.Context, android.database.Cursor, boolean):void} */
    public m(Context context, d dVar) {
        super(context, (Cursor) null, false);
        this.c = context;
        this.b = dVar;
        this.d = new a(this.c.getApplicationContext());
        this.g = ((YouNi) this.c).i();
        this.h = new ArrayList();
        "mContactsMode=" + this.g;
    }

    private static void a(TextView textView, String str, String str2) {
        String lowerCase = str.toLowerCase();
        String lowerCase2 = str2.toLowerCase();
        int indexOf = lowerCase.indexOf(lowerCase2);
        if (indexOf >= 0) {
            SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(lowerCase);
            spannableStringBuilder.setSpan(new BackgroundColorSpan(-256), indexOf, lowerCase2.length() + indexOf, 33);
            textView.setText(spannableStringBuilder);
        }
    }

    public final void a() {
        this.e = true;
    }

    public final void a(String str) {
        if (!TextUtils.isEmpty(str)) {
            for (String str2 : str.trim().split(",")) {
                int indexOf = str2.indexOf(60);
                this.h.add(new r(this, str2.substring(0, indexOf).trim(), str2.substring(indexOf + 1, str2.indexOf(62)).trim(), -1));
            }
        }
    }

    public final void a(String str, String str2, int i) {
        this.h.add(new r(this, str, str2, i));
    }

    public final void a(String str, boolean z) {
        if (!z) {
            this.f = str;
            ((Activity) this.c).showDialog(14);
            return;
        }
        this.d.i(this.f);
    }

    public final void a(boolean z) {
        this.f544a = z;
    }

    public final List b() {
        return this.h;
    }

    public final void b(String str, String str2, int i) {
        this.h.remove(new r(this, str, str2, i));
    }

    public final void bindView(View view, Context context, Cursor cursor) {
        String string = cursor.getString(2);
        String string2 = cursor.getString(3);
        int i = cursor.getInt(1);
        s sVar = new s(this);
        ImageView unused = sVar.f562a = (ImageView) view.findViewById(C0000R.id.contact_photo);
        TextView unused2 = sVar.b = (TextView) view.findViewById(C0000R.id.contact_name);
        View unused3 = sVar.c = view.findViewById(C0000R.id.contact_sig_icn);
        TextView unused4 = sVar.d = (TextView) view.findViewById(C0000R.id.contact_detail);
        View unused5 = sVar.e = view.findViewById(C0000R.id.contact_invite_btn);
        "view=" + view + ", photoview=" + sVar.f562a;
        this.b.a(sVar.f562a, cursor.getInt(1), 0);
        YouNi youNi = (YouNi) this.c;
        String h2 = youNi.h();
        sVar.b.setText(string);
        if (!TextUtils.isEmpty(h2)) {
            a(sVar.b, string, h2);
        }
        if (this.f544a) {
            sVar.c.setVisibility(0);
            String string3 = cursor.getString(5);
            sVar.d.setText(string3);
            if (!TextUtils.isEmpty(h2)) {
                a(sVar.d, string3, h2);
            }
            if ("new".equals(cursor.getString(7))) {
                view.findViewById(C0000R.id.youni_contact_new).setVisibility(0);
            } else {
                view.findViewById(C0000R.id.youni_contact_new).setVisibility(8);
            }
        } else {
            sVar.c.setVisibility(8);
            view.findViewById(C0000R.id.youni_contact_new).setVisibility(8);
            sVar.d.setText(string2);
            if (!TextUtils.isEmpty(h2)) {
                a(sVar.d, string2, h2);
            }
        }
        ai c2 = youNi.c();
        if (!this.f544a || c2 == null || !c2.b() || !"1".equals(cursor.getString(6))) {
            view.findViewById(C0000R.id.contact_online_btn).setVisibility(8);
        } else {
            view.findViewById(C0000R.id.contact_online_btn).setVisibility(0);
        }
        if (this.g == 1) {
            CheckBox checkBox = (CheckBox) view.findViewById(16908308);
            checkBox.setButtonDrawable((int) C0000R.drawable.checkbox);
            ListView j = youNi.j();
            int position = cursor.getPosition();
            if (this.h.contains(new r(this, string, string2, i))) {
                checkBox.setChecked(true);
                j.setItemChecked(position, true);
                return;
            }
            checkBox.setChecked(false);
            j.setItemChecked(position, false);
        } else if (cursor.getInt(4) == 0) {
            sVar.e.setBackgroundDrawable(com.snda.youni.d.a.a("btn_contact_invite"));
            view.findViewById(C0000R.id.contact_btn).setVisibility(0);
            view.findViewById(C0000R.id.contact_btn).setOnClickListener(new o(this, string2));
            sVar.e.setOnClickListener(new n(this, string2));
        } else {
            view.findViewById(C0000R.id.contact_btn).setVisibility(8);
        }
    }

    public final void c() {
        for (r rVar : this.h) {
            "name: " + rVar.f561a + "phone:" + rVar.b;
        }
    }

    public final String d() {
        StringBuilder sb = new StringBuilder();
        for (r rVar : this.h) {
            sb.append(" " + rVar.f561a + " <" + rVar.b + ">,");
        }
        "getSelectedContactsAsAdresses :" + sb.toString();
        return sb.toString();
    }

    public final int[] e() {
        int[] iArr = new int[this.h.size()];
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= iArr.length) {
                return iArr;
            }
            iArr[i2] = ((r) this.h.get(i2)).c;
            i = i2 + 1;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public final View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
        View inflate = LayoutInflater.from(context).inflate((int) C0000R.layout.item_list_contacts, viewGroup, false);
        if (this.g == 1) {
            "mContactsMode=" + this.g;
            inflate.findViewById(16908308).setVisibility(0);
            inflate.findViewById(C0000R.id.contact_invite_btn).setVisibility(8);
        }
        return inflate;
    }

    public final void onScroll(AbsListView absListView, int i, int i2, int i3) {
    }

    public final void onScrollStateChanged(AbsListView absListView, int i) {
        if (i == 2) {
            this.b.b();
        } else {
            this.b.c();
        }
    }
}
