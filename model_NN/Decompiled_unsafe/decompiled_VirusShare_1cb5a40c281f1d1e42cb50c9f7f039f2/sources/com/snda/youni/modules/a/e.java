package com.snda.youni.modules.a;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Handler;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.snda.youni.C0000R;
import com.snda.youni.YouNi;
import com.snda.youni.activities.ChatActivity;
import com.snda.youni.activities.FavoriteActivity;
import com.snda.youni.d;
import com.snda.youni.e.r;
import com.snda.youni.e.t;
import com.snda.youni.h.a.a;
import com.snda.youni.modules.b.b;
import com.snda.youni.modules.b.k;
import com.snda.youni.providers.l;
import java.lang.ref.WeakReference;

public final class e implements AbsListView.OnScrollListener, AdapterView.OnItemClickListener, d, j, w {
    private static String[] b = {"_id", "date", "recipient_ids"};
    private static final String[] p = {"contact_id", "sid", "friend_timestamp", "photo_timestamp"};
    private static final String[] q = {"_id", "date", "message_count", "recipient_ids", "snippet", "snippet_cs", "read", "error", "has_attachment"};

    /* renamed from: a  reason: collision with root package name */
    public k f492a;
    private d c;
    private com.snda.youni.e d;
    /* access modifiers changed from: private */
    public WeakReference e;
    private ListView f;
    /* access modifiers changed from: private */
    public t g;
    /* access modifiers changed from: private */
    public h h;
    private g i;
    /* access modifiers changed from: private */
    public ProgressDialog j;
    /* access modifiers changed from: private */
    public a k;
    /* access modifiers changed from: private */
    public YouNi l;
    private boolean m;
    /* access modifiers changed from: private */
    public m n;
    private ContentObserver o = new s(this, new Handler());
    private k r = new p(this);

    public e(Context context, View view, d dVar, com.snda.youni.e eVar, a aVar) {
        this.l = (YouNi) context;
        this.e = new WeakReference(context);
        this.h = new h(aVar);
        this.h.a((j) this);
        this.c = dVar;
        this.d = eVar;
        this.k = aVar;
        this.f = (ListView) view.findViewById(C0000R.id.list_inbox);
        this.g = new t(context, this.c, this.d);
        if (this.l.i() != 1) {
            this.g.a(this);
        }
        this.f.setAdapter((ListAdapter) this.g);
        this.f.setOnScrollListener(this);
        this.f.setOnItemClickListener(this);
        this.l.getContentResolver().registerContentObserver(l.f596a, true, this.o);
        h();
        this.n = new m(this, this.l.getContentResolver());
        b.a(this.r);
    }

    private k a(ContextMenu.ContextMenuInfo contextMenuInfo) {
        Object item = this.g.getItem(((AdapterView.AdapterContextMenuInfo) contextMenuInfo).position);
        if (item == null) {
            return null;
        }
        return (k) item;
    }

    static /* synthetic */ void a(e eVar, q qVar, boolean z) {
        View inflate = View.inflate(eVar.l, C0000R.layout.delete_thread_dialog_view, null);
        ((TextView) inflate.findViewById(C0000R.id.message)).setText((int) C0000R.string.inbox_delete_item);
        CheckBox checkBox = (CheckBox) inflate.findViewById(C0000R.id.delete_locked);
        if (!z) {
            checkBox.setVisibility(8);
        } else {
            qVar.a(checkBox.isChecked());
            checkBox.setOnClickListener(new r(eVar, qVar, checkBox));
        }
        new AlertDialog.Builder(eVar.l).setTitle((int) C0000R.string.inbox_delete_title).setCancelable(true).setPositiveButton((int) C0000R.string.inbox_ok, qVar).setNegativeButton((int) C0000R.string.inbox_cancel, (DialogInterface.OnClickListener) null).setView(inflate).show();
    }

    private void c(k kVar) {
        "open conversation of: " + (kVar == null ? "null" : kVar.toString());
        Context context = (Context) this.e.get();
        if (context != null && !((Activity) context).isFinishing()) {
            Intent intent = new Intent(context, ChatActivity.class);
            intent.putExtra("item", kVar);
            context.startActivity(intent);
        }
    }

    private void d(k kVar) {
        Context context = (Context) this.e.get();
        if (context != null && !((Activity) context).isFinishing()) {
            Intent intent = new Intent("android.intent.action.INSERT");
            intent.setType("vnd.android.cursor.dir/contact");
            intent.setFlags(268435456);
            intent.putExtra("name", kVar.g.equalsIgnoreCase(kVar.f496a) ? "" : kVar.f496a);
            intent.putExtra("phone", kVar.g);
            context.startActivity(intent);
        }
    }

    /* access modifiers changed from: private */
    public void h() {
        if (this.i == null || this.i.getStatus() == AsyncTask.Status.FINISHED) {
            this.i = new g(this);
        }
        if (this.i.getStatus() == AsyncTask.Status.PENDING) {
            this.i.execute(new Void[0]);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public final void a() {
        Intent intent = new Intent(this.l, YouNi.class);
        intent.putExtra("inbox_batch_operations", true);
        this.l.startActivityForResult(intent, 1314);
    }

    public final void a(Intent intent) {
        int[] intArrayExtra = intent.getIntArrayExtra("thread_ids");
        Context context = (Context) this.e.get();
        if (context != null && !((Activity) context).isFinishing()) {
            this.h.a((d) this);
            this.j = ProgressDialog.show(context, null, null);
            this.j.setCancelable(false);
            Uri parse = Uri.parse("content://mms-sms/conversations");
            String str = "thread_id IN (";
            for (int i2 = 0; i2 < intArrayExtra.length; i2++) {
                str = str + intArrayExtra[i2] + ",";
            }
            this.n.startDelete(1, null, parse, str.substring(0, str.length() - 1) + ")", null);
        }
    }

    public final void a(MenuInflater menuInflater, ContextMenu contextMenu, ContextMenu.ContextMenuInfo contextMenuInfo) {
        k a2;
        if (this.l.i() != 1 && (a2 = a(contextMenuInfo)) != null) {
            contextMenu.setHeaderTitle(a2.f496a);
            if ((a2.n != null && a2.n.split(" ").length > 1) || this.l.getString(C0000R.string.Youni_robot_id).equals(a2.g)) {
                menuInflater.inflate(C0000R.menu.inbox_context_menu_mutil_repient, contextMenu);
            } else if (a2.e) {
                menuInflater.inflate(C0000R.menu.inbox_context_menu_contact, contextMenu);
            } else {
                menuInflater.inflate(C0000R.menu.inbox_context_menu_not_contact, contextMenu);
            }
        }
    }

    public final void a(View view) {
        this.m = !this.m;
        if (this.m) {
            Cursor cursor = this.g.getCursor();
            cursor.moveToPosition(-1);
            while (cursor.moveToNext()) {
                this.g.a(cursor.getString(0));
            }
            ((Button) view).setText(this.l.getString(C0000R.string.tab_select_cancel));
        } else {
            this.g.b();
            ((Button) view).setText(this.l.getString(C0000R.string.tab_select_all));
        }
        this.f.invalidateViews();
    }

    public final void a(k kVar) {
        d(kVar);
    }

    public final boolean a(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case C0000R.id.menu_inbox_call /*2131558685*/:
                k a2 = a(menuItem.getMenuInfo());
                "make a call to: " + (a2 == null ? "null" : a2.toString());
                Context context = (Context) this.e.get();
                if (!(context == null || ((Activity) context).isFinishing() || a2.g == null)) {
                    String str = a2.g;
                    if (t.b(str)) {
                        str = ((Activity) context).getString(C0000R.string.snda_services_phone_number);
                    }
                    r.a(str, context);
                }
                return true;
            case C0000R.id.menu_inbox_black_list /*2131558686*/:
                this.f492a = a(menuItem.getMenuInfo());
                this.l.removeDialog(17);
                this.l.showDialog(17);
                return true;
            case C0000R.id.menu_inbox_delete /*2131558687*/:
                k a3 = a(menuItem.getMenuInfo());
                this.n.cancelOperation(0);
                Uri uri = android.a.e.c;
                long longValue = Long.valueOf(a3.k).longValue();
                this.n.startQuery(0, new Long(longValue), longValue != -1 ? ContentUris.withAppendedId(uri, longValue) : uri, q, null, null, "date DESC");
                return true;
            case C0000R.id.menu_inbox_favorite /*2131558688*/:
                this.l.startActivity(new Intent(this.l, FavoriteActivity.class));
                return true;
            case C0000R.id.menu_inbox_add /*2131558689*/:
                d(a(menuItem.getMenuInfo()));
                return true;
            default:
                return false;
        }
    }

    public final void b() {
        this.f.invalidateViews();
    }

    public final void b(k kVar) {
        c(kVar);
    }

    public final ListView c() {
        return this.f;
    }

    public final void d() {
        if (this.g == null) {
            this.l.getContentResolver().unregisterContentObserver(this.o);
            return;
        }
        Cursor cursor = this.g.getCursor();
        if (cursor != null) {
            cursor.close();
        }
        this.l.getContentResolver().unregisterContentObserver(this.o);
    }

    public final void e() {
        this.g.a(false);
    }

    public final void f() {
        this.g.e();
        this.g.a(true);
        this.g.a();
    }

    public final void g() {
        if (this.i.getStatus() == AsyncTask.Status.RUNNING) {
            this.i.cancel(true);
        }
        b.b(this.r);
    }

    public final void onItemClick(AdapterView adapterView, View view, int i2, long j2) {
        k kVar = (k) this.g.getItem(i2);
        if (this.l.i() != 1) {
            c(kVar);
        } else if (this.f.isItemChecked(i2)) {
            this.g.a(kVar.k);
        } else {
            this.g.b(kVar.k);
        }
    }

    public final void onScroll(AbsListView absListView, int i2, int i3, int i4) {
    }

    public final void onScrollStateChanged(AbsListView absListView, int i2) {
        if (i2 == 2) {
            this.c.b();
            this.d.b();
            return;
        }
        this.c.c();
        this.d.c();
    }
}
