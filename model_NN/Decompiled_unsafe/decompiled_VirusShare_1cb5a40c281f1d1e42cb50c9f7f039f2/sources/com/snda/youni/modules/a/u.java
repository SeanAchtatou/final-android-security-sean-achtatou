package com.snda.youni.modules.a;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

final class u {

    /* renamed from: a  reason: collision with root package name */
    private static u f506a = new u();
    private final HashSet b = new HashSet(10);

    private u() {
    }

    static l a(long j) {
        synchronized (f506a) {
            Iterator it = f506a.b.iterator();
            while (it.hasNext()) {
                l lVar = (l) it.next();
                if (lVar.a() == j) {
                    return lVar;
                }
            }
            return null;
        }
    }

    static u a() {
        return f506a;
    }

    static void a(l lVar) {
        synchronized (f506a) {
            if (f506a.b.contains(lVar)) {
                throw new IllegalStateException("cache already contains " + lVar + " threadId: " + lVar.f);
            }
            f506a.b.add(lVar);
        }
    }

    static void a(Set set) {
        synchronized (f506a) {
            Iterator it = f506a.b.iterator();
            while (it.hasNext()) {
                if (!set.contains(Long.valueOf(((l) it.next()).a()))) {
                    it.remove();
                }
            }
        }
    }

    static void b(long j) {
        Iterator it = f506a.b.iterator();
        while (it.hasNext()) {
            l lVar = (l) it.next();
            if (lVar.a() == j) {
                f506a.b.remove(lVar);
                return;
            }
        }
    }
}
