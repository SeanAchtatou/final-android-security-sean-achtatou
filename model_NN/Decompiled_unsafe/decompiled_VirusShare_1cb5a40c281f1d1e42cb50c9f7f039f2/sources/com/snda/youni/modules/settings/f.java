package com.snda.youni.modules.settings;

import android.app.AlertDialog;
import android.view.View;

final class f implements View.OnFocusChangeListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ AlertDialog f568a;
    private /* synthetic */ j b;

    f(j jVar, AlertDialog alertDialog) {
        this.b = jVar;
        this.f568a = alertDialog;
    }

    public final void onFocusChange(View view, boolean z) {
        if (z) {
            this.f568a.getWindow().setSoftInputMode(5);
        }
    }
}
