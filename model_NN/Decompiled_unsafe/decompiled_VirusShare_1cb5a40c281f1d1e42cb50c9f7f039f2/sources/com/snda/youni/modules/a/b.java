package com.snda.youni.modules.a;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import com.snda.youni.e.j;
import com.snda.youni.e.t;
import com.snda.youni.providers.n;

public final class b {

    /* renamed from: a  reason: collision with root package name */
    private static final String[] f490a = {"contact_id", "display_name", "nick_name", "signature", "phone_number", "contact_type", "expand_data1"};
    private ContentResolver b;

    public b(Context context) {
        this.b = context.getContentResolver();
    }

    private static boolean a(Cursor cursor, String str) {
        if (!cursor.moveToFirst()) {
            return false;
        }
        String string = cursor.getString(cursor.getColumnIndex("phone_number"));
        while (!t.a(string).equals(str)) {
            if (!cursor.moveToNext()) {
                return false;
            }
        }
        return true;
    }

    private a b(String str) {
        String a2 = j.a(str);
        "num: " + str + "    sid: " + a2;
        try {
            Cursor query = this.b.query(n.f597a, f490a, "sid='" + a2 + "'", null, null);
            if (query == null) {
                return null;
            }
            if (!a(query, str)) {
                query.close();
                return null;
            }
            a aVar = new a(this);
            aVar.f489a = query.getInt(0);
            aVar.b = query.getString(1);
            aVar.c = query.getString(2);
            aVar.d = query.getString(3);
            aVar.e = query.getString(4);
            if (query.getInt(5) == 0) {
                aVar.f = false;
            } else {
                aVar.f = true;
                aVar.g = query.getString(6);
            }
            query.close();
            return aVar;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public final a a(String str) {
        if (str == null) {
            return null;
        }
        return b(str);
    }
}
