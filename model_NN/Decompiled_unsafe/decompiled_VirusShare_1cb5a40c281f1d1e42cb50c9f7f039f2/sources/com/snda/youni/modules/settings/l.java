package com.snda.youni.modules.settings;

import android.content.res.TypedArray;
import com.snda.youni.C0000R;

public final class l extends m {
    private t c;

    public l(SettingsItemView settingsItemView, t tVar) {
        super(settingsItemView, "color_name", C0000R.string.settings_message_color, C0000R.array.color_details);
        b(a());
        this.c = tVar;
    }

    private void b(int i) {
        TypedArray obtainTypedArray = this.b.getContext().getResources().obtainTypedArray(this.f572a);
        this.b.b(obtainTypedArray.getString(i));
        obtainTypedArray.recycle();
    }

    public final void a(int i) {
        super.a(i);
        b(i);
        this.c.b();
    }

    public final int b() {
        TypedArray obtainTypedArray = this.b.getContext().getResources().obtainTypedArray(this.f572a);
        int length = obtainTypedArray.length();
        obtainTypedArray.recycle();
        return length;
    }
}
