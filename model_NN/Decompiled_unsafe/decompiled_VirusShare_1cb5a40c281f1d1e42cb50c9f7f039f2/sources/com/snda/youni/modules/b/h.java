package com.snda.youni.modules.b;

import android.text.TextUtils;
import java.util.ArrayList;
import java.util.Iterator;

public final class h extends ArrayList {
    public static h a(String str, boolean z) {
        h hVar = new h();
        for (j jVar : a.a(str)) {
            if (jVar != null && !TextUtils.isEmpty(jVar.b)) {
                b a2 = b.a(jVar.b, z);
                a2.a(jVar.f529a);
                hVar.add(a2);
            }
        }
        return hVar;
    }

    public final boolean equals(Object obj) {
        try {
            h hVar = (h) obj;
            if (size() != hVar.size()) {
                return false;
            }
            Iterator it = iterator();
            while (it.hasNext()) {
                if (!hVar.contains((b) it.next())) {
                    return false;
                }
            }
            return true;
        } catch (ClassCastException e) {
            return false;
        }
    }
}
