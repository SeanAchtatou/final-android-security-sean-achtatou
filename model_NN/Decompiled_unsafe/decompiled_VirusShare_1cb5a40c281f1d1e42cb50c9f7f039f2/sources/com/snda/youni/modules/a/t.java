package com.snda.youni.modules.a;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.preference.PreferenceManager;
import android.telephony.PhoneNumberUtils;
import android.text.TextUtils;
import android.text.format.Time;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import com.snda.youni.C0000R;
import com.snda.youni.YouNi;
import com.snda.youni.b.ai;
import com.snda.youni.d;
import com.snda.youni.d.a;
import com.snda.youni.e;
import com.snda.youni.e.k;
import com.snda.youni.e.q;
import com.snda.youni.modules.NumPhotoView;
import com.snda.youni.modules.b.b;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public final class t extends CursorAdapter implements i {

    /* renamed from: a  reason: collision with root package name */
    private Context f505a;
    /* access modifiers changed from: private */
    public w b;
    private LayoutInflater c;
    private b d;
    private d e;
    private int f;
    private List g;
    private e h;
    private final String i;
    private SharedPreferences j;
    private boolean k = true;
    private Cursor l;
    private Drawable m = a.a("add_contact_bkg");

    public t(Context context, d dVar, e eVar) {
        super(context, null);
        this.f505a = context;
        this.l = null;
        this.i = context.getString(C0000R.string.youni_robot);
        this.c = (LayoutInflater) context.getSystemService("layout_inflater");
        this.d = new b(context);
        this.e = dVar;
        this.h = eVar;
        this.j = PreferenceManager.getDefaultSharedPreferences(context);
        this.f = ((YouNi) context).i();
        this.g = new ArrayList();
    }

    private k a(Cursor cursor) {
        String format;
        k kVar = new k();
        kVar.k = cursor.getString(0);
        long j2 = cursor.getLong(1);
        kVar.n = cursor.getString(2);
        Context context = this.f505a;
        long currentTimeMillis = System.currentTimeMillis();
        int i2 = (int) ((currentTimeMillis - j2) / 1000);
        if (i2 <= 0) {
            format = context.getString(C0000R.string.just_now);
        } else if (i2 / 3600 == 0) {
            format = i2 / 60 == 0 ? i2 + context.getString(C0000R.string.second_by) : (i2 / 60) + context.getString(C0000R.string.minute_by);
        } else {
            Time time = new Time();
            time.set(currentTimeMillis);
            int i3 = time.year;
            int i4 = time.yearDay;
            time.set(j2);
            int i5 = time.yearDay;
            format = i5 == i4 ? time.format(context.getString(C0000R.string.today_format)) : i4 - i5 == 1 ? time.format(context.getString(C0000R.string.yesterday_format)) : i4 - i5 == 2 ? time.format(context.getString(C0000R.string.before_yesterday_format)) : time.year < i3 ? time.format(context.getString(C0000R.string.before_year_format)) : time.format(context.getString(C0000R.string.this_year_format));
        }
        kVar.d = format;
        return kVar;
    }

    private void a(l lVar, k kVar) {
        String str;
        Iterator it = lVar.c().iterator();
        while (true) {
            if (!it.hasNext()) {
                str = null;
                break;
            }
            b bVar = (b) it.next();
            if (!TextUtils.isEmpty(bVar.b())) {
                str = bVar.b();
                break;
            }
        }
        if (str != null) {
            kVar.h = str;
        }
        if (!"krobot_001".equalsIgnoreCase(kVar.h)) {
            kVar.g = PhoneNumberUtils.stripSeparators(kVar.h);
            kVar.g = com.snda.youni.e.t.a(kVar.g);
        } else {
            kVar.g = com.snda.youni.e.t.a(kVar.h);
        }
        kVar.g = q.a(kVar.g);
        kVar.b = lVar.e();
        "threadId=" + lVar.a() + ", hasDraft=" + lVar.d();
        if (lVar.d()) {
            kVar.b = this.f505a.getString(C0000R.string.Draft) + kVar.b;
        }
        a a2 = this.d.a(kVar.g);
        if (kVar.g != null && kVar.g.startsWith("krobot")) {
            kVar.e = true;
            kVar.f = true;
            kVar.f496a = this.i;
            if (a2 != null) {
                kVar.l = a2.d;
                kVar.i = a2.f489a;
            }
        }
        if (a2 != null) {
            kVar.i = a2.f489a;
            kVar.l = a2.d;
            if (a2.b != null && a2.b != "") {
                kVar.f496a = a2.b;
            } else if (a2.c == null || a2.c == "") {
                kVar.f496a = kVar.g;
            } else {
                kVar.f496a = a2.c;
            }
            kVar.e = true;
            kVar.f = a2.f;
            kVar.j = a2.g;
        } else {
            kVar.f496a = kVar.g;
            if (this.j.getBoolean("contacts_synced", false)) {
                kVar.e = false;
            } else {
                kVar.e = true;
            }
            kVar.f = false;
        }
        if (kVar.n != null && kVar.n.split(" ").length > 1) {
            kVar.f496a = this.f505a.getString(C0000R.string.inbox_multi_name, kVar.f496a, Integer.valueOf(kVar.n.split(" ").length));
        }
    }

    public final void a() {
        if (this.l != null) {
            this.l.requery();
        }
    }

    public final void a(w wVar) {
        this.b = wVar;
    }

    public final void a(String str) {
        this.g.add(new c(this, str));
    }

    public final void a(boolean z) {
        this.k = z;
    }

    public final void b() {
        this.g.clear();
    }

    public final void b(String str) {
        this.g.remove(new c(this, str));
    }

    public final void bindView(View view, Context context, Cursor cursor) {
        "bindView, ========view=" + view;
        YouNi youNi = (YouNi) context;
        ai c2 = youNi.c();
        n nVar = (n) view.getTag();
        k a2 = a(cursor);
        nVar.f499a = Integer.valueOf(a2.k).intValue();
        a(l.a(this.f505a, cursor), a2);
        ImageView a3 = nVar.c.a();
        TextView b2 = nVar.c.b();
        b2.setVisibility(8);
        if (a2.n != null && a2.n.split(" ").length > 1) {
            this.e.a(a3, 0, (int) C0000R.drawable.broadcast_portrait);
        } else if (a2.e) {
            this.e.a(a3, a2.i, 0);
        } else {
            this.e.a(a3, 0, 0);
            b2.setBackgroundDrawable(this.m);
            b2.setVisibility(0);
        }
        String str = a2.b;
        if ((a2.n == null || a2.n.split(" ").length == 1) && a2.h.contains("krobot")) {
            str = k.a(this.f505a, str, null, false).toString();
        }
        CharSequence a4 = k.a(nVar.f.getContext(), str, 0);
        if (a4.equals(this.f505a.getResources().getString(C0000R.string.conversation_snippet_picture))) {
            Drawable drawable = this.f505a.getResources().getDrawable(C0000R.drawable.ic_inbox_picture);
            drawable.setBounds(0, 0, 30, 20);
            nVar.f.setCompoundDrawables(drawable, null, null, null);
            nVar.f.setText("");
        } else if (a4.equals(this.f505a.getResources().getString(C0000R.string.conversation_snippet_audio))) {
            Drawable drawable2 = this.f505a.getResources().getDrawable(C0000R.drawable.ic_inbox_audio);
            drawable2.setBounds(0, 0, 30, 20);
            nVar.f.setCompoundDrawables(drawable2, null, null, null);
            nVar.f.setText("");
        } else {
            nVar.f.setCompoundDrawables(null, null, null, null);
            nVar.f.setText(a4);
        }
        nVar.b.setText(a2.f496a);
        nVar.g.setText(a2.d);
        if (!a2.f || c2 == null || !c2.b()) {
            nVar.d.setVisibility(8);
        } else if ("1".equals(a2.j)) {
            nVar.d.setVisibility(0);
        } else {
            nVar.d.setVisibility(8);
        }
        this.h.a(nVar, Integer.parseInt(a2.k));
        nVar.c.setOnClickListener(new f(this, a2));
        if (this.f == 1) {
            CheckBox checkBox = (CheckBox) view.findViewById(16908308);
            checkBox.setButtonDrawable((int) C0000R.drawable.checkbox);
            ListView k2 = youNi.k();
            int position = cursor.getPosition();
            if (this.g.contains(new c(this, a2.k))) {
                checkBox.setChecked(true);
                k2.setItemChecked(position, true);
                return;
            }
            checkBox.setChecked(false);
            k2.setItemChecked(position, false);
        }
    }

    public final int[] c() {
        int[] iArr = new int[this.g.size()];
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 >= iArr.length) {
                return iArr;
            }
            iArr[i3] = ((c) this.g.get(i3)).f491a;
            i2 = i3 + 1;
        }
    }

    public final void changeCursor(Cursor cursor) {
        this.l = cursor;
        super.changeCursor(cursor);
    }

    public final int d() {
        return this.g.size();
    }

    public final void e() {
        this.m = a.a("add_contact_bkg");
    }

    public final Object getItem(int i2) {
        Object item = super.getItem(i2);
        if (item == null) {
            return null;
        }
        Cursor cursor = (Cursor) item;
        k a2 = a(cursor);
        a(l.a(this.f505a, cursor), a2);
        return a2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public final View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
        View inflate = this.c.inflate((int) C0000R.layout.item_list_inbox, viewGroup, false);
        n nVar = new n();
        nVar.f = (TextView) inflate.findViewById(C0000R.id.item_list_inbox_last);
        nVar.g = (TextView) inflate.findViewById(C0000R.id.item_list_inbox_time);
        nVar.b = (TextView) inflate.findViewById(C0000R.id.item_list_inbox_name);
        nVar.e = (TextView) inflate.findViewById(C0000R.id.item_list_inbox_num);
        nVar.c = (NumPhotoView) inflate.findViewById(C0000R.id.item_list_inbox_photo);
        nVar.d = inflate.findViewById(C0000R.id.contact_online_btn);
        nVar.h = inflate.findViewById(C0000R.id.item_list_inbox_right);
        nVar.i = inflate.findViewById(C0000R.id.item_list_inbox_mid);
        nVar.j = inflate.findViewById(C0000R.id.item_list_inbox);
        inflate.setTag(nVar);
        if (this.f == 1) {
            "mContactsMode=" + this.f;
            inflate.findViewById(16908308).setVisibility(0);
            inflate.findViewById(C0000R.id.item_list_inbox_time).setVisibility(8);
        }
        return inflate;
    }

    /* access modifiers changed from: protected */
    public final void onContentChanged() {
        if (this.k) {
            super.onContentChanged();
        }
    }
}
