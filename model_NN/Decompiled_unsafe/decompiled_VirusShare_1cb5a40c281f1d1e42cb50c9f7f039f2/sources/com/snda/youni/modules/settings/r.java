package com.snda.youni.modules.settings;

import android.content.Context;
import android.content.Intent;

public final class r extends y {

    /* renamed from: a  reason: collision with root package name */
    private Class f574a;

    public r(SettingsItemView settingsItemView, Class cls) {
        super(settingsItemView, null);
        this.f574a = cls;
    }

    public final void a() {
        Context context = this.b.getContext();
        context.startActivity(new Intent(context, this.f574a));
    }

    public final void a(boolean z) {
        this.b.setClickable(z);
    }
}
