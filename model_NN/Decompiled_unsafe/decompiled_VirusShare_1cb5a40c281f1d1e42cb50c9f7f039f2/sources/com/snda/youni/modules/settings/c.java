package com.snda.youni.modules.settings;

import android.content.Context;
import android.content.DialogInterface;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

final class c implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ Context f566a;
    private /* synthetic */ EditText b;
    private /* synthetic */ j c;

    c(j jVar, Context context, EditText editText) {
        this.c = jVar;
        this.f566a = context;
        this.b = editText;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        InputMethodManager inputMethodManager = (InputMethodManager) this.f566a.getSystemService("input_method");
        if (inputMethodManager != null) {
            inputMethodManager.hideSoftInputFromWindow(this.b.getWindowToken(), 2);
        }
    }
}
