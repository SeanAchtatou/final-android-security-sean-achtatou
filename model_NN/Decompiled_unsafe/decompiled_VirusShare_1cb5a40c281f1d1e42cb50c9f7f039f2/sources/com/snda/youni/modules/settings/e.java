package com.snda.youni.modules.settings;

import android.content.res.TypedArray;
import com.snda.youni.C0000R;

public final class e extends m {
    private static final int[] c = {18, 14, 12};

    public e(SettingsItemView settingsItemView) {
        super(settingsItemView, "chat_size_name", C0000R.string.settings_message_size, C0000R.array.size_details);
        b(a());
    }

    private void b(int i) {
        TypedArray obtainTypedArray = this.b.getContext().getResources().obtainTypedArray(this.f572a);
        this.b.b(obtainTypedArray.getString(i));
        obtainTypedArray.recycle();
    }

    public final int a() {
        int c2 = c();
        for (int i = 0; i < c.length; i++) {
            if (c[i] == c2) {
                return i;
            }
        }
        return c[1];
    }

    public final void a(int i) {
        super.a(c[i]);
        b(i);
    }
}
