package com.snda.youni.modules.settings;

import android.app.AlertDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.preference.PreferenceManager;
import android.text.InputFilter;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import com.snda.youni.C0000R;
import com.snda.youni.e.b;
import com.snda.youni.e.v;
import com.snda.youni.g.a;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public final class j {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public Context f571a;
    /* access modifiers changed from: private */
    public k b;
    /* access modifiers changed from: private */
    public Bitmap c;
    /* access modifiers changed from: private */
    public String d;
    /* access modifiers changed from: private */
    public String e;
    /* access modifiers changed from: private */
    public final ImageView f;
    /* access modifiers changed from: private */
    public final EditText g;
    /* access modifiers changed from: private */
    public final EditText h;

    public j(k kVar, View view) {
        Bitmap decodeFile;
        this.b = kVar;
        this.f = (ImageView) view.findViewById(C0000R.id.icn_selected_portrait);
        this.g = (EditText) view.findViewById(C0000R.id.text_nick_name);
        this.h = (EditText) view.findViewById(C0000R.id.text_signature);
        this.f571a = view.getContext().getApplicationContext();
        SharedPreferences defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this.f571a);
        String string = defaultSharedPreferences.getString("nick_name", "");
        String string2 = defaultSharedPreferences.getString("signature", "");
        a(string);
        b(string2);
        if (new File(b.e()).exists() && (decodeFile = BitmapFactory.decodeFile(b.e())) != null) {
            this.f.setImageBitmap(v.a(decodeFile));
        }
    }

    /* access modifiers changed from: private */
    public void a() {
        "saveSelfPortrait() is called, mSelfPortraitPath is: " + b.e();
        File file = new File(b.e());
        Bitmap bitmap = ((BitmapDrawable) this.f.getDrawable()).getBitmap();
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            if (bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fileOutputStream)) {
                fileOutputStream.flush();
                fileOutputStream.close();
            }
        } catch (FileNotFoundException e2) {
            e2.printStackTrace();
        } catch (IOException e3) {
            e3.printStackTrace();
        }
    }

    private void a(Context context, int i) {
        String obj;
        int i2 = i == 0 ? C0000R.string.settings_nick_title : C0000R.string.settings_sig_title;
        View inflate = LayoutInflater.from(context).inflate((int) C0000R.layout.edit_dialog_view, (ViewGroup) null);
        EditText editText = (EditText) inflate.findViewById(C0000R.id.edit);
        if (i == 0) {
            editText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(10)});
            obj = this.g.getText().toString();
        } else {
            editText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(30)});
            obj = this.h.getText().toString();
        }
        if (!TextUtils.isEmpty(obj)) {
            editText.setText(obj);
            editText.setSelection(obj.length());
        }
        AlertDialog show = new AlertDialog.Builder(context).setTitle(i2).setView(inflate).setPositiveButton((int) C0000R.string.alert_dialog_ok, new g(this, context, editText, i)).setNegativeButton(context.getString(C0000R.string.alert_dialog_cancel), new c(this, context, editText)).show();
        editText.setOnFocusChangeListener(new f(this, show));
        editText.addTextChangedListener(new h(this, show));
    }

    static /* synthetic */ void a(j jVar, int i) {
        if (i == 0) {
            if (jVar.g != null) {
                jVar.g.setText(jVar.d);
            }
        } else if (jVar.h != null) {
            jVar.h.setText(jVar.e);
        }
    }

    /* access modifiers changed from: private */
    public void b() {
        PreferenceManager.getDefaultSharedPreferences(this.f571a).edit().putString("nick_name", this.g.getText().toString()).commit();
    }

    /* access modifiers changed from: private */
    public void c() {
        PreferenceManager.getDefaultSharedPreferences(this.f571a).edit().putString("signature", this.h.getText().toString()).commit();
    }

    public final void a(Context context) {
        if (this.f571a != null) {
            a.a(this.f571a.getApplicationContext(), "name_setting", null);
        }
        a(context, 0);
    }

    public final void a(Context context, Bitmap bitmap) {
        this.c = ((BitmapDrawable) this.f.getDrawable()).getBitmap();
        this.f.setImageBitmap(bitmap);
        new AlertDialog.Builder(context).setTitle((int) C0000R.string.settings_confirm_pic_title).setMessage((int) C0000R.string.settings_confirm_pic).setPositiveButton((int) C0000R.string.alert_dialog_ok, new d(this, context)).setNegativeButton((int) C0000R.string.alert_dialog_cancel, new a(this)).show();
    }

    public final void a(Bitmap bitmap) {
        this.f.setImageBitmap(bitmap);
        a();
    }

    public final void a(String str) {
        this.g.setText(str);
        b();
    }

    public final void b(Context context) {
        if (this.f571a != null) {
            a.a(this.f571a.getApplicationContext(), "sig_setting", null);
        }
        a(context, 1);
    }

    public final void b(String str) {
        this.h.setText(str);
        c();
    }

    public final void c(Context context) {
        if (this.f571a != null) {
            a.a(this.f571a.getApplicationContext(), "update_headimage", null);
        }
        new AlertDialog.Builder(context).setTitle((int) C0000R.string.settings_person_title).setItems((int) C0000R.array.set_portrait_selections, new b(this)).show();
    }
}
