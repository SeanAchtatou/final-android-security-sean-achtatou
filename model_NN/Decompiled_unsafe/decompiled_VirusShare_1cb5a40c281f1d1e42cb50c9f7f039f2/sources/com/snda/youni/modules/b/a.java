package com.snda.youni.modules.b;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.text.TextUtils;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class a {

    /* renamed from: a  reason: collision with root package name */
    private static Uri f523a = Uri.parse("content://mms-sms/canonical-addresses");
    private static Uri b = Uri.parse("content://mms-sms/canonical-address");
    private static a c;
    private final Map d = new HashMap();
    private final Context e;

    private a(Context context) {
        this.e = context;
    }

    public static List a(String str) {
        ArrayList arrayList;
        String str2;
        synchronized (c) {
            arrayList = new ArrayList();
            for (String parseLong : str.split(" ")) {
                try {
                    long parseLong2 = Long.parseLong(parseLong);
                    String str3 = (String) c.d.get(Long.valueOf(parseLong2));
                    if (str3 == null) {
                        "RecipientId " + parseLong2 + " not in cache!";
                        a();
                        str2 = (String) c.d.get(Long.valueOf(parseLong2));
                    } else {
                        str2 = str3;
                    }
                    if (TextUtils.isEmpty(str2)) {
                        "RecipientId " + parseLong2 + " has empty number!";
                    } else {
                        arrayList.add(new j(parseLong2, str2));
                    }
                } catch (NumberFormatException e2) {
                }
            }
        }
        return arrayList;
    }

    public static void a() {
        Cursor query = c.e.getContentResolver().query(f523a, null, null, null, null);
        if (query != null) {
            try {
                synchronized (c) {
                    c.d.clear();
                    while (query.moveToNext()) {
                        long j = query.getLong(0);
                        c.d.put(Long.valueOf(j), query.getString(1));
                    }
                }
            } finally {
                query.close();
            }
        }
    }

    static void a(Context context) {
        c = new a(context);
        new Thread(new d()).start();
    }
}
