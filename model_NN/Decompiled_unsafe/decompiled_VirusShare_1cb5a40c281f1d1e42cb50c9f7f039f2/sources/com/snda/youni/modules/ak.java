package com.snda.youni.modules;

import android.text.Editable;
import android.text.TextWatcher;
import com.snda.youni.C0000R;
import com.snda.youni.providers.n;

final class ak implements TextWatcher {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ aj f519a;

    ak(aj ajVar) {
        this.f519a = ajVar;
    }

    public final void afterTextChanged(Editable editable) {
        "Search String is: " + editable.toString();
        switch (this.f519a.c.getCheckedRadioButtonId()) {
            case C0000R.id.btn_all /*2131558642*/:
                this.f519a.m.startQuery(1, null, n.f597a, t.f578a, "contact_id > 0  AND (search_name LIKE '" + editable.toString() + "%'" + " OR " + "pinyin_name LIKE '" + editable.toString() + "%'" + " OR " + "phone_number LIKE '" + editable.toString() + "%'" + " OR " + "display_name LIKE '" + editable.toString() + "%'" + ")", null, null);
                return;
            case C0000R.id.btn_youni /*2131558643*/:
                this.f519a.m.startQuery(2, null, n.f597a, t.f578a, "contact_type=1 AND (search_name LIKE '" + editable.toString() + "%'" + " OR " + "pinyin_name LIKE '" + editable.toString() + "%'" + " OR " + "phone_number LIKE '" + editable.toString() + "%'" + " OR " + "display_name LIKE '" + editable.toString() + "%'" + ")", null, null);
                return;
            case C0000R.id.btn_favorites /*2131558644*/:
                this.f519a.m.startQuery(3, null, n.b, t.f578a, "times_contacted > 0 AND contact_id > 0  AND (search_name LIKE '" + editable.toString() + "%'" + " OR " + "pinyin_name LIKE '" + editable.toString() + "%'" + " OR " + "phone_number LIKE '" + editable.toString() + "%'" + " OR " + "display_name LIKE '" + editable.toString() + "%')", null, "times_contacted DESC");
                return;
            default:
                this.f519a.m.startQuery(1, null, n.f597a, t.f578a, "search_name LIKE '" + editable.toString() + "%'" + " OR " + "pinyin_name LIKE '" + editable.toString() + "%'" + " OR " + "display_name LIKE '" + editable.toString() + "%'", null, null);
                return;
        }
    }

    public final void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    public final void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }
}
