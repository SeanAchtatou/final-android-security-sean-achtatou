package com.snda.youni.modules;

import android.content.ContentValues;
import com.snda.youni.C0000R;
import com.snda.youni.b.s;
import com.snda.youni.e.q;
import com.snda.youni.providers.n;
import java.util.HashMap;
import java.util.Map;

final class p implements s {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ ai f554a;

    p(ai aiVar) {
        this.f554a = aiVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public final void a(HashMap hashMap) {
        if (this.f554a.f517a.c.getCheckedRadioButtonId() == C0000R.id.btn_youni) {
            "IMultiStatusInterface,onStatus, map.count=" + hashMap.size();
            "statusMap=" + hashMap;
            int size = this.f554a.d.size();
            for (int i = 0; i < size; i++) {
                String str = (String) this.f554a.d.get(i);
                String str2 = (String) this.f554a.e.get(i);
                "statusMap.get(phoneNumber)=" + ((String) hashMap.get(str)) + " status=" + str2;
                if (str2 != null && ((String) hashMap.get(str)).equals(str2)) {
                    hashMap.remove(str);
                }
            }
            "IMultiStatusInterface,after remove, map.count=" + hashMap.size();
            hashMap.size();
            StringBuilder sb = new StringBuilder();
            sb.append("sid IN (");
            StringBuilder sb2 = new StringBuilder();
            sb2.append("sid IN (");
            boolean z = true;
            boolean z2 = true;
            for (Map.Entry entry : hashMap.entrySet()) {
                String callerIDMinMatch = q.toCallerIDMinMatch((String) entry.getKey());
                if ("1".equals(entry.getValue())) {
                    if (z) {
                        sb.append('\'').append(callerIDMinMatch).append('\'');
                        z = false;
                    } else {
                        sb.append(',').append('\'').append(callerIDMinMatch).append('\'');
                    }
                } else if ("0".equals(entry.getValue())) {
                    if (z2) {
                        sb2.append('\'').append(callerIDMinMatch).append('\'');
                        z2 = false;
                    } else {
                        sb2.append(',').append('\'').append(callerIDMinMatch).append('\'');
                    }
                }
            }
            sb.append(')');
            sb2.append(')');
            String unused = this.f554a.b = sb.toString();
            String unused2 = this.f554a.c = sb2.toString();
            "mOnlinePhoneSelection=" + this.f554a.b;
            "mOfflinePhoneSelection=" + this.f554a.c;
            ContentValues contentValues = new ContentValues();
            contentValues.put("expand_data1", (Integer) 1);
            this.f554a.startUpdate(5, null, n.f597a, contentValues, this.f554a.b, null);
        }
    }
}
