package com.snda.youni.modules.settings;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public abstract class y {

    /* renamed from: a  reason: collision with root package name */
    private SharedPreferences f577a;
    protected SettingsItemView b;
    private String c;
    private int d;

    public y(SettingsItemView settingsItemView, String str) {
        this.b = settingsItemView;
        this.f577a = PreferenceManager.getDefaultSharedPreferences(settingsItemView.getContext());
        this.c = str;
    }

    /* access modifiers changed from: protected */
    public void a_(int i) {
        SharedPreferences.Editor edit = this.f577a.edit();
        edit.putInt(this.c, i);
        edit.commit();
    }

    /* access modifiers changed from: protected */
    public int c() {
        return this.f577a.getInt(this.c, this.d);
    }

    public final void e() {
        this.b = null;
        this.f577a = null;
    }

    public final String f() {
        return this.c;
    }
}
