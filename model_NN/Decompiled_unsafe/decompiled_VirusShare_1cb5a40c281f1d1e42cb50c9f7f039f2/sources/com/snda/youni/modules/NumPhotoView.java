package com.snda.youni.modules;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.snda.youni.C0000R;
import com.snda.youni.a;

public class NumPhotoView extends RelativeLayout {

    /* renamed from: a  reason: collision with root package name */
    private ImageView f487a;
    private TextView b;
    private int c;
    private TextView d;

    public NumPhotoView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        RelativeLayout relativeLayout = (RelativeLayout) ((LayoutInflater) context.getSystemService("layout_inflater")).inflate((int) C0000R.layout.model_num_photo, this);
        this.b = (TextView) relativeLayout.findViewById(C0000R.id.view_num_photo_num);
        this.f487a = (ImageView) relativeLayout.findViewById(C0000R.id.view_num_photo_image);
        this.d = (TextView) relativeLayout.findViewById(C0000R.id.text_add_contact);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, a.b);
        String string = obtainStyledAttributes.getString(1);
        if (obtainStyledAttributes.getBoolean(0, true)) {
            this.b.setVisibility(0);
        } else {
            this.b.setVisibility(4);
        }
        this.b.setText(string);
        obtainStyledAttributes.recycle();
    }

    public NumPhotoView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    public final ImageView a() {
        return this.f487a;
    }

    public final void a(int i) {
        this.c = i;
        if (i <= 0) {
            this.b.setVisibility(4);
        } else if (i > 99) {
            this.b.setVisibility(0);
            this.b.setText("99+");
        } else {
            this.b.setVisibility(0);
            this.b.setText(String.valueOf(i));
        }
    }

    public final TextView b() {
        return this.d;
    }
}
