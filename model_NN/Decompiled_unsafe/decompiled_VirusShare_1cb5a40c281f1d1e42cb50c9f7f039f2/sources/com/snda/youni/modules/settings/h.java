package com.snda.youni.modules.settings;

import android.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;

final class h implements TextWatcher {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ AlertDialog f570a;
    private /* synthetic */ j b;

    h(j jVar, AlertDialog alertDialog) {
        this.b = jVar;
        this.f570a = alertDialog;
    }

    public final void afterTextChanged(Editable editable) {
        if (editable.length() == 0) {
            this.f570a.getButton(-1).setEnabled(false);
        } else {
            this.f570a.getButton(-1).setEnabled(true);
        }
    }

    public final void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    public final void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }
}
