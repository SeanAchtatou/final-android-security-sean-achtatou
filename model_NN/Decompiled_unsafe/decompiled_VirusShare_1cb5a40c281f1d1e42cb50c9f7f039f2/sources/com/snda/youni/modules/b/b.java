package com.snda.youni.modules.b;

import android.a.d;
import android.content.Context;
import android.database.ContentObserver;
import android.graphics.drawable.BitmapDrawable;
import android.os.Handler;
import android.telephony.PhoneNumberUtils;
import android.text.TextUtils;
import java.util.HashSet;

public final class b {
    private static f e;
    private static final ContentObserver f = new i(new Handler());
    /* access modifiers changed from: private */
    public static final HashSet g = new HashSet();

    /* renamed from: a  reason: collision with root package name */
    public int f524a;
    public String b;
    public String c;
    public boolean d;
    /* access modifiers changed from: private */
    public String h;
    /* access modifiers changed from: private */
    public String i;
    private String j;
    private boolean k;
    private long l;
    /* access modifiers changed from: private */
    public long m;
    /* access modifiers changed from: private */
    public int n;
    /* access modifiers changed from: private */
    public String o;
    /* access modifiers changed from: private */
    public BitmapDrawable p;
    /* access modifiers changed from: private */
    public byte[] q;
    /* access modifiers changed from: private */
    public boolean r;
    /* access modifiers changed from: private */
    public boolean s;

    /* synthetic */ b(String str) {
        this(str, (byte) 0);
    }

    private b(String str, byte b2) {
        this.i = "";
        b(str);
        this.k = false;
        this.m = 0;
        this.n = 0;
        this.r = true;
        this.f524a = 0;
        this.b = "";
        this.c = "";
        this.d = false;
    }

    public static b a(String str, boolean z) {
        return e.a(str, z);
    }

    static /* synthetic */ String a(String str) {
        return str != null ? str : "";
    }

    public static void a() {
        e.a();
    }

    public static void a(Context context) {
        e = new f(context);
        a.a(context);
    }

    public static void a(k kVar) {
        synchronized (g) {
            g.add(kVar);
        }
    }

    public static void b(k kVar) {
        synchronized (g) {
            g.remove(kVar);
        }
    }

    private synchronized void b(String str) {
        this.h = str;
        d();
        this.k = true;
    }

    /* access modifiers changed from: private */
    public void d() {
        String str = this.i;
        String str2 = this.h;
        String formatNumber = !d.b(str2) ? PhoneNumberUtils.formatNumber(str2) : str2;
        this.j = (TextUtils.isEmpty(str) || str.equals(str2)) ? formatNumber : str + " <" + formatNumber + ">";
    }

    public final synchronized void a(long j2) {
        this.l = j2;
    }

    public final synchronized String b() {
        return this.h;
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder();
        Object[] objArr = new Object[4];
        objArr[0] = this.h != null ? this.h : "null";
        objArr[1] = this.i != null ? this.i : "null";
        objArr[2] = this.j != null ? this.j : "null";
        objArr[3] = this.b != null ? this.b : "null";
        return sb.append(String.format("{ number=%s, name=%s, nameAndNumber=%s, nickName=%s}", objArr)).append(", isStale=").append(this.r).toString();
    }
}
