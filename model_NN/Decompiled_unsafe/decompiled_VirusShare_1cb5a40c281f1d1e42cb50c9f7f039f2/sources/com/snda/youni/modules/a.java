package com.snda.youni.modules;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;
import com.snda.youni.C0000R;

public final class a extends CursorAdapter {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.widget.CursorAdapter.<init>(android.content.Context, android.database.Cursor, boolean):void}
     arg types: [android.content.Context, ?[OBJECT, ARRAY], int]
     candidates:
      ClspMth{android.widget.CursorAdapter.<init>(android.content.Context, android.database.Cursor, int):void}
      ClspMth{android.widget.CursorAdapter.<init>(android.content.Context, android.database.Cursor, boolean):void} */
    public a(Context context) {
        super(context, (Cursor) null, false);
    }

    public final void bindView(View view, Context context, Cursor cursor) {
        int columnIndex = cursor.getColumnIndex("display_name");
        int columnIndex2 = cursor.getColumnIndex("phone_number");
        int columnIndex3 = cursor.getColumnIndex("contact_type");
        String string = cursor.getString(columnIndex);
        String string2 = cursor.getString(columnIndex2);
        int i = cursor.getInt(columnIndex3);
        c cVar = (c) view.getTag();
        cVar.f530a.setText(string);
        cVar.b.setText(string2);
        cVar.c = i;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public final View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
        View inflate = LayoutInflater.from(context).inflate((int) C0000R.layout.item_list_recipients, viewGroup, false);
        c cVar = new c();
        cVar.f530a = (TextView) inflate.findViewById(C0000R.id.contact_name);
        cVar.b = (TextView) inflate.findViewById(C0000R.id.contact_detail);
        inflate.setTag(cVar);
        return inflate;
    }
}
