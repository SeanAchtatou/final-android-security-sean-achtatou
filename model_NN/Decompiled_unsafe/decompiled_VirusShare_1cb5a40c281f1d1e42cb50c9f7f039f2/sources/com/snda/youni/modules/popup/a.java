package com.snda.youni.modules.popup;

import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.snda.youni.C0000R;
import com.snda.youni.d;
import com.snda.youni.modules.a.k;

final class a {

    /* renamed from: a  reason: collision with root package name */
    private View f555a;
    private TextView b;
    private TextView c;
    private TextView d;
    private TextView e;
    /* access modifiers changed from: private */
    public Button f;
    private ImageView g;
    private View h;
    private View i;
    private /* synthetic */ c j;

    public a(c cVar, View view) {
        this.j = cVar;
        this.f555a = view;
        this.b = (TextView) view.findViewById(C0000R.id.name);
        this.c = (TextView) view.findViewById(C0000R.id.count);
        this.d = (TextView) view.findViewById(C0000R.id.time);
        this.e = (TextView) view.findViewById(C0000R.id.text);
        this.g = (ImageView) view.findViewById(C0000R.id.head);
        this.f = (Button) view.findViewById(C0000R.id.cancel);
        this.h = view.findViewById(C0000R.id.left_arrow);
        this.i = view.findViewById(C0000R.id.right_arrow);
        this.f.setOnTouchListener(cVar);
        this.e.setOnTouchListener(cVar);
        this.g.setOnTouchListener(cVar);
        this.b.setOnTouchListener(cVar);
        this.f555a.setOnTouchListener(cVar);
        this.d.setOnTouchListener(cVar);
        this.h.setOnTouchListener(cVar);
        this.i.setOnTouchListener(cVar);
    }

    private void a(boolean z, boolean z2) {
        if (z) {
            this.h.setVisibility(0);
        } else {
            this.h.setVisibility(4);
        }
        if (z2) {
            this.i.setVisibility(0);
        } else {
            this.i.setVisibility(4);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.snda.youni.modules.popup.a.a(boolean, boolean):void
     arg types: [int, int]
     candidates:
      com.snda.youni.modules.popup.a.a(int, int):void
      com.snda.youni.modules.popup.a.a(com.snda.youni.modules.a.k, com.snda.youni.d):void
      com.snda.youni.modules.popup.a.a(boolean, boolean):void */
    public final void a(int i2, int i3) {
        this.c.setText("(" + String.valueOf(i2 + 1) + "/" + String.valueOf(i3) + ")");
        if (i3 <= 1) {
            a(false, false);
        } else if (i2 == 0) {
            a(false, true);
        } else if (i2 == i3 - 1) {
            a(true, false);
        } else {
            a(true, true);
        }
    }

    public final void a(k kVar, d dVar) {
        this.b.setText(Html.fromHtml("<u>" + kVar.f496a + "</u>"));
        this.d.setText(kVar.d);
        this.e.setText(com.snda.youni.e.k.a(this.e.getContext(), kVar.b, 2));
        dVar.a(this.g, kVar.i, 0);
    }
}
