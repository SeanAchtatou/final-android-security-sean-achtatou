package com.snda.youni.modules.settings;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.snda.youni.C0000R;
import com.snda.youni.a.b;
import com.snda.youni.activities.SettingsBlackListActivity;
import com.snda.youni.activities.SettingsCountActivity;
import com.snda.youni.activities.SettingsHelpActivity;
import com.snda.youni.activities.SettingsMessageActivity;
import com.snda.youni.activities.SettingsSkinActivity;
import com.snda.youni.g.a;

public final class s implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private final j f575a;
    private final b b;
    private final View c;

    public s(k kVar, View view, b bVar) {
        String str;
        this.b = bVar;
        this.c = view;
        this.f575a = new j(kVar, view.findViewById(C0000R.id.person));
        Context context = view.getContext();
        try {
            str = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            str = "unknown";
        }
        SettingsItemView settingsItemView = (SettingsItemView) view.findViewById(C0000R.id.update);
        settingsItemView.b(str);
        settingsItemView.b();
        view.findViewById(C0000R.id.btn_modify_portrait).setOnClickListener(this);
        view.findViewById(C0000R.id.text_nick_name).setOnClickListener(this);
        view.findViewById(C0000R.id.text_signature).setOnClickListener(this);
        view.findViewById(C0000R.id.message).setOnClickListener(this);
        view.findViewById(C0000R.id.remind).setOnClickListener(this);
        view.findViewById(C0000R.id.count).setOnClickListener(this);
        view.findViewById(C0000R.id.recommend).setOnClickListener(this);
        view.findViewById(C0000R.id.black_list).setOnClickListener(this);
        view.findViewById(C0000R.id.help).setOnClickListener(this);
        view.findViewById(C0000R.id.update).setOnClickListener(this);
        view.findViewById(C0000R.id.about).setOnClickListener(this);
        view.findViewById(C0000R.id.skins).setOnClickListener(this);
    }

    public final void a(Context context, Bitmap bitmap) {
        this.f575a.a(context, bitmap);
    }

    public final void a(Bitmap bitmap) {
        this.f575a.a(bitmap);
    }

    public final void a(String str) {
        this.f575a.a(str);
    }

    public final void b(String str) {
        this.f575a.b(str);
    }

    public final void onClick(View view) {
        String str;
        switch (view.getId()) {
            case C0000R.id.remind /*2131558430*/:
                Context context = view.getContext();
                Intent intent = new Intent(context, SettingsMessageActivity.class);
                intent.putExtra("type", 1);
                context.startActivity(intent);
                return;
            case C0000R.id.message /*2131558485*/:
                Context context2 = view.getContext();
                Intent intent2 = new Intent(context2, SettingsMessageActivity.class);
                intent2.putExtra("type", 0);
                context2.startActivity(intent2);
                return;
            case C0000R.id.count /*2131558591*/:
                Context context3 = view.getContext();
                context3.startActivity(new Intent(context3, SettingsCountActivity.class));
                return;
            case C0000R.id.btn_modify_portrait /*2131558618*/:
                this.f575a.c(view.getContext());
                return;
            case C0000R.id.text_nick_name /*2131558619*/:
                this.f575a.a(view.getContext());
                return;
            case C0000R.id.text_signature /*2131558621*/:
                this.f575a.b(view.getContext());
                return;
            case C0000R.id.update /*2131558652*/:
                this.b.a();
                return;
            case C0000R.id.skins /*2131558653*/:
                Context context4 = view.getContext();
                context4.startActivity(new Intent(context4, SettingsSkinActivity.class));
                return;
            case C0000R.id.black_list /*2131558654*/:
                Context context5 = view.getContext();
                context5.startActivity(new Intent(context5, SettingsBlackListActivity.class));
                return;
            case C0000R.id.help /*2131558655*/:
                Context context6 = view.getContext();
                a.a(context6.getApplicationContext(), "help_click", null);
                context6.startActivity(new Intent(context6, SettingsHelpActivity.class));
                return;
            case C0000R.id.about /*2131558657*/:
                Context context7 = view.getContext();
                try {
                    str = context7.getPackageManager().getPackageInfo(context7.getPackageName(), 0).versionName;
                } catch (PackageManager.NameNotFoundException e) {
                    e.printStackTrace();
                    str = "unknown";
                }
                View inflate = LayoutInflater.from(context7).inflate((int) C0000R.layout.view_settings_about, (ViewGroup) null);
                inflate.findViewById(C0000R.id.title);
                String string = context7.getString(C0000R.string.settings_about_body);
                String str2 = context7.getString(C0000R.string.snda_services_phone) + ": " + context7.getString(C0000R.string.snda_services_phone_number);
                String string2 = context7.getString(C0000R.string.settings_about_copyright);
                ((TextView) inflate.findViewById(C0000R.id.body)).setText(string + "\n" + str2 + "\n" + string2);
                new AlertDialog.Builder(this.c.getContext()).setTitle((int) C0000R.string.settings_about).setMessage(String.format(context7.getString(C0000R.string.settings_about_title), str) + "\n" + string + "\n" + str2 + "\n" + string2).setPositiveButton((int) C0000R.string.alert_dialog_ok, (DialogInterface.OnClickListener) null).show();
                return;
            default:
                return;
        }
    }
}
