package com.snda.youni.modules;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import com.snda.youni.c.k;
import com.snda.youni.f.a;
import com.snda.youni.f.c;
import com.snda.youni.f.d;

final class v implements a {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ q f580a;

    v(q qVar) {
        this.f580a = qVar;
    }

    public final void a(c cVar, d dVar) {
        k kVar = (k) dVar.b();
        if (dVar.c() == 0 && kVar.b() == 0) {
            if (PreferenceManager.getDefaultSharedPreferences(this.f580a.f560a).getBoolean("inner_test", false)) {
                this.f580a.a();
            }
            q.d(this.f580a);
            q.a(this.f580a, kVar);
        } else if (dVar.c() == 0 && 5 == kVar.b()) {
            SharedPreferences defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this.f580a.f560a);
            if (defaultSharedPreferences.getBoolean("inner_test", false)) {
                SharedPreferences.Editor edit = defaultSharedPreferences.edit();
                edit.putBoolean("isActivitied", false);
                edit.commit();
                this.f580a.f560a.showDialog(10);
            }
        }
    }

    public final void a(Exception exc, String str) {
        exc.printStackTrace();
    }
}
