package com.snda.youni.modules.settings;

import android.widget.Toast;
import com.snda.youni.C0000R;
import com.snda.youni.c.e;
import com.snda.youni.f.a;
import com.snda.youni.f.c;
import com.snda.youni.f.d;

final class n implements a {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ g f573a;

    n(g gVar) {
        this.f573a = gVar;
    }

    public final void a(c cVar, d dVar) {
        e eVar = (e) dVar.b();
        if (dVar.c() == 0 && eVar != null && eVar.b() == 0) {
            if (this.f573a.b == 0) {
                this.f573a.c.b();
            } else {
                this.f573a.c.c();
            }
            Toast.makeText(this.f573a.c.f571a, (int) C0000R.string.settings_modification_succeeded, 0).show();
            return;
        }
        j.a(this.f573a.c, this.f573a.b);
        Toast.makeText(this.f573a.f569a, (int) C0000R.string.settings_modification_failed, 0).show();
    }

    public final void a(Exception exc, String str) {
        j.a(this.f573a.c, this.f573a.b);
        Toast.makeText(this.f573a.c.f571a, (int) C0000R.string.settings_modification_failed, 0).show();
    }
}
