package com.snda.youni.modules.newchat;

import android.text.Annotation;
import android.text.Editable;
import android.text.Spanned;
import android.text.TextWatcher;

final class e implements TextWatcher {

    /* renamed from: a  reason: collision with root package name */
    private Annotation[] f550a;
    private int b = -1;
    private /* synthetic */ RecipientsEditor c;

    e(RecipientsEditor recipientsEditor) {
        this.c = recipientsEditor;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.snda.youni.modules.newchat.RecipientsEditor.a(java.lang.CharSequence, int):boolean
     arg types: [android.text.Editable, int]
     candidates:
      com.snda.youni.modules.newchat.RecipientsEditor.a(android.text.Editable, int):void
      com.snda.youni.modules.newchat.RecipientsEditor.a(java.lang.CharSequence, int):boolean */
    public final void afterTextChanged(Editable editable) {
        if (this.f550a != null) {
            for (Annotation removeSpan : this.f550a) {
                editable.removeSpan(removeSpan);
            }
        }
        this.f550a = null;
        int i = this.b;
        this.b = -1;
        if (i > 0) {
            "n = " + i;
            if (RecipientsEditor.a((CharSequence) editable, i)) {
                this.c.a(editable, i);
            }
        }
    }

    public final void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        if ((i > 0 || i == i3) && i2 > i3) {
            this.f550a = (Annotation[]) ((Spanned) charSequence).getSpans(i, i + i2, Annotation.class);
        }
    }

    public final void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        if (i2 == 0 && i3 == 1 && RecipientsEditor.a(charSequence.charAt(i))) {
            this.b = i;
        }
    }
}
