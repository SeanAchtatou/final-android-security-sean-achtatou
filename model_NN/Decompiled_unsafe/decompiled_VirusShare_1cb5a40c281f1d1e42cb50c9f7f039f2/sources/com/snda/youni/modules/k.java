package com.snda.youni.modules;

import android.content.SharedPreferences;
import com.snda.a.a.b.b;
import com.snda.youni.c.h;
import com.snda.youni.c.t;
import com.snda.youni.e.s;
import org.json.JSONException;
import org.json.JSONObject;

final class k extends b {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ q f543a;

    /* synthetic */ k(q qVar) {
        this(qVar, (byte) 0);
    }

    private k(q qVar, byte b) {
        this.f543a = qVar;
    }

    public final void a(String str) {
        try {
            JSONObject jSONObject = new JSONObject(str);
            String string = jSONObject.getString("MobileNum");
            String string2 = jSONObject.getString("PTAccount");
            String string3 = jSONObject.getString("NumAccount");
            String string4 = jSONObject.getString("ProductId");
            "MobileNum is: " + string;
            "PTAccount is: " + string2;
            "NumAccount is: " + string3;
            "ProductID is: " + string4;
            "Message is: " + jSONObject.getString("Message");
            s.f403a = string;
            s.c = string3;
            s.b = string2;
            s.q = string4;
            SharedPreferences.Editor edit = this.f543a.f560a.e().edit();
            edit.putString("self_phone_number", string);
            edit.putString("self_pt_account", string2);
            edit.putString("self_num_account", string3);
            edit.putString("self_product_id", string4);
            edit.commit();
            this.f543a.b.b();
            t.a(new h(string2, string3, string, string4), new am(this), this.f543a.f560a);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public final void b(String str) {
        super.b(str);
        try {
            "Register failure message is: " + new JSONObject(str).getString("Message");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public final void c(String str) {
        super.c(str);
    }
}
