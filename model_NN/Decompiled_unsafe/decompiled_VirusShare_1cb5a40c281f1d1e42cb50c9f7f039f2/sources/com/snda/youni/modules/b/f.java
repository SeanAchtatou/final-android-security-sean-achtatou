package com.snda.youni.modules.b;

import android.a.d;
import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.provider.ContactsContract;
import android.telephony.PhoneNumberUtils;
import android.text.TextUtils;
import com.snda.youni.C0000R;
import com.snda.youni.mms.ui.m;
import com.snda.youni.providers.n;
import java.nio.CharBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;

final class f {
    private static final Uri b = n.f597a;
    private static final String[] c = {"contact_id", "display_name", "nick_name", "signature", "phone_number", "contact_type"};
    private static final Uri d = ContactsContract.Data.CONTENT_URI;
    private static final String[] e = {"data4", "contact_presence", "contact_id", "display_name"};
    private static CharBuffer h = CharBuffer.allocate(5);

    /* renamed from: a  reason: collision with root package name */
    private final g f527a;
    private final Context f;
    private final HashMap g;

    /* synthetic */ f(Context context) {
        this(context, (byte) 0);
    }

    private f(Context context, byte b2) {
        this.f527a = new g();
        this.g = new HashMap();
        this.f = context;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:20:?, code lost:
        r1 = a(r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x005c, code lost:
        monitor-enter(r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:?, code lost:
        byte[] unused = r6.q = r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0060, code lost:
        monitor-exit(r6);
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private com.snda.youni.modules.b.b a(java.lang.String r10) {
        /*
            r9 = this;
            r8 = 1
            r7 = 0
            com.snda.youni.modules.b.b r6 = new com.snda.youni.modules.b.b
            r6.<init>(r10)
            android.content.Context r0 = r9.f
            android.content.ContentResolver r0 = r0.getContentResolver()
            android.net.Uri r1 = com.snda.youni.modules.b.f.d
            java.lang.String[] r2 = com.snda.youni.modules.b.f.e
            java.lang.String r3 = "UPPER(data1)=UPPER(?) AND mimetype='vnd.android.cursor.item/email_v2'"
            java.lang.String[] r4 = new java.lang.String[r8]
            r4[r7] = r10
            r5 = 0
            android.database.Cursor r0 = r0.query(r1, r2, r3, r4, r5)
            if (r0 == 0) goto L_0x0064
        L_0x001e:
            boolean r1 = r0.moveToNext()     // Catch:{ all -> 0x006a }
            if (r1 == 0) goto L_0x0061
            monitor-enter(r6)     // Catch:{ all -> 0x006a }
            r1 = 1
            int r1 = r0.getInt(r1)     // Catch:{ all -> 0x0067 }
            if (r1 == 0) goto L_0x0065
            int r1 = android.provider.ContactsContract.Presence.getPresenceIconResourceId(r1)     // Catch:{ all -> 0x0067 }
        L_0x0030:
            int unused = r6.n = r1     // Catch:{ all -> 0x0067 }
            r1 = 2
            long r1 = r0.getLong(r1)     // Catch:{ all -> 0x0067 }
            long unused = r6.m = r1     // Catch:{ all -> 0x0067 }
            r1 = 0
            java.lang.String r1 = r0.getString(r1)     // Catch:{ all -> 0x0067 }
            boolean r2 = android.text.TextUtils.isEmpty(r1)     // Catch:{ all -> 0x0067 }
            if (r2 == 0) goto L_0x004b
            r1 = 3
            java.lang.String r1 = r0.getString(r1)     // Catch:{ all -> 0x0067 }
        L_0x004b:
            boolean r2 = android.text.TextUtils.isEmpty(r1)     // Catch:{ all -> 0x0067 }
            if (r2 != 0) goto L_0x0072
            java.lang.String unused = r6.i = r1     // Catch:{ all -> 0x0067 }
            r1 = r8
        L_0x0055:
            monitor-exit(r6)     // Catch:{ all -> 0x0067 }
            if (r1 == 0) goto L_0x001e
            byte[] r1 = r9.a(r6)     // Catch:{ all -> 0x006a }
            monitor-enter(r6)     // Catch:{ all -> 0x006a }
            byte[] unused = r6.q = r1     // Catch:{ all -> 0x006f }
            monitor-exit(r6)     // Catch:{ all -> 0x006f }
        L_0x0061:
            r0.close()
        L_0x0064:
            return r6
        L_0x0065:
            r1 = r7
            goto L_0x0030
        L_0x0067:
            r1 = move-exception
            monitor-exit(r6)     // Catch:{ all -> 0x0067 }
            throw r1     // Catch:{ all -> 0x006a }
        L_0x006a:
            r1 = move-exception
            r0.close()
            throw r1
        L_0x006f:
            r1 = move-exception
            monitor-exit(r6)     // Catch:{ all -> 0x006f }
            throw r1     // Catch:{ all -> 0x006a }
        L_0x0072:
            r1 = r7
            goto L_0x0055
        */
        throw new UnsupportedOperationException("Method not decompiled: com.snda.youni.modules.b.f.a(java.lang.String):com.snda.youni.modules.b.b");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.snda.youni.modules.b.b.b(com.snda.youni.modules.b.b, boolean):boolean
     arg types: [com.snda.youni.modules.b.b, int]
     candidates:
      com.snda.youni.modules.b.b.b(com.snda.youni.modules.b.b, java.lang.String):java.lang.String
      com.snda.youni.modules.b.b.b(com.snda.youni.modules.b.b, boolean):boolean */
    static /* synthetic */ void a(f fVar, b bVar) {
        HashSet hashSet;
        if (bVar != null) {
            String g2 = bVar.h;
            b a2 = d.b(g2) ? fVar.a(g2) : new b(PhoneNumberUtils.stripSeparators(g2));
            synchronized (bVar) {
                if (!b.a(bVar.i).equals(b.a(a2.i)) ? true : bVar.m != a2.m ? true : bVar.n != a2.n ? true : !Arrays.equals(bVar.q, a2.q) ? true : bVar.f524a != a2.f524a ? true : bVar.f524a != a2.f524a ? true : !b.a(bVar.b).equals(b.a(a2.b)) ? true : !b.a(bVar.c).equals(b.a(a2.c)) ? true : bVar.d != a2.d) {
                    long unused = bVar.m = a2.m;
                    int unused2 = bVar.n = a2.n;
                    String unused3 = bVar.o = a2.o;
                    byte[] unused4 = bVar.q = a2.q;
                    BitmapDrawable unused5 = bVar.p = a2.p;
                    bVar.b = a2.b;
                    bVar.c = a2.c;
                    bVar.d = a2.d;
                    bVar.f524a = a2.f524a;
                    if (m.a(fVar.f, bVar.h)) {
                        String unused6 = bVar.i = fVar.f.getString(C0000R.string.me);
                    } else {
                        String unused7 = bVar.i = a2.i;
                    }
                    bVar.d();
                    synchronized (b.g) {
                        hashSet = (HashSet) b.g.clone();
                    }
                    Iterator it = hashSet.iterator();
                    while (it.hasNext()) {
                        ((k) it.next()).a();
                    }
                }
                synchronized (bVar) {
                    boolean unused8 = bVar.s = false;
                    bVar.notifyAll();
                }
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:22:?, code lost:
        r0.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x004a, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x004b, code lost:
        if (r0 != null) goto L_0x004d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:?, code lost:
        r0.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:?, code lost:
        return r1;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0042 A[SYNTHETIC, Splitter:B:21:0x0042] */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x004a A[ExcHandler: all (r1v5 'th' java.lang.Throwable A[CUSTOM_DECLARE]), Splitter:B:7:0x0029] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private byte[] a(com.snda.youni.modules.b.b r6) {
        /*
            r5 = this;
            r4 = 0
            long r0 = r6.m
            r2 = 0
            int r0 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r0 == 0) goto L_0x0011
            android.graphics.drawable.BitmapDrawable r0 = r6.p
            if (r0 == 0) goto L_0x0013
        L_0x0011:
            r0 = r4
        L_0x0012:
            return r0
        L_0x0013:
            android.net.Uri r0 = android.provider.ContactsContract.Contacts.CONTENT_URI
            long r1 = r6.m
            android.net.Uri r0 = android.content.ContentUris.withAppendedId(r0, r1)
            android.content.Context r1 = r5.f
            android.content.ContentResolver r1 = r1.getContentResolver()
            java.io.InputStream r0 = android.provider.ContactsContract.Contacts.openContactPhotoInputStream(r1, r0)
            if (r0 == 0) goto L_0x0055
            int r1 = r0.available()     // Catch:{ IOException -> 0x003e, all -> 0x004a }
            byte[] r1 = new byte[r1]     // Catch:{ IOException -> 0x003e, all -> 0x004a }
            r2 = 0
            int r3 = r1.length     // Catch:{ IOException -> 0x0053, all -> 0x004a }
            r0.read(r1, r2, r3)     // Catch:{ IOException -> 0x0053, all -> 0x004a }
        L_0x0034:
            if (r0 == 0) goto L_0x0039
            r0.close()     // Catch:{ IOException -> 0x003b }
        L_0x0039:
            r0 = r1
            goto L_0x0012
        L_0x003b:
            r0 = move-exception
            r0 = r1
            goto L_0x0012
        L_0x003e:
            r1 = move-exception
            r1 = r4
        L_0x0040:
            if (r0 == 0) goto L_0x0045
            r0.close()     // Catch:{ IOException -> 0x0047 }
        L_0x0045:
            r0 = r1
            goto L_0x0012
        L_0x0047:
            r0 = move-exception
            r0 = r1
            goto L_0x0012
        L_0x004a:
            r1 = move-exception
            if (r0 == 0) goto L_0x0050
            r0.close()     // Catch:{ IOException -> 0x0051 }
        L_0x0050:
            throw r1
        L_0x0051:
            r0 = move-exception
            goto L_0x0050
        L_0x0053:
            r2 = move-exception
            goto L_0x0040
        L_0x0055:
            r1 = r4
            goto L_0x0034
        */
        throw new UnsupportedOperationException("Method not decompiled: com.snda.youni.modules.b.f.a(com.snda.youni.modules.b.b):byte[]");
    }

    private b b(String str) {
        String charBuffer;
        synchronized (this) {
            boolean b2 = d.b(str);
            if (b2) {
                charBuffer = str;
            } else {
                CharBuffer charBuffer2 = h;
                charBuffer2.clear();
                charBuffer2.mark();
                int length = str.length();
                int i = 0;
                while (true) {
                    length--;
                    if (length < 0) {
                        break;
                    }
                    char charAt = str.charAt(length);
                    if (Character.isDigit(charAt)) {
                        charBuffer2.put(charAt);
                        i++;
                        if (i == 5) {
                            break;
                        }
                    }
                }
                charBuffer2.reset();
                charBuffer = i > 0 ? charBuffer2.toString() : str;
            }
            ArrayList arrayList = (ArrayList) this.g.get(charBuffer);
            if (arrayList != null) {
                int size = arrayList.size();
                for (int i2 = 0; i2 < size; i2++) {
                    b bVar = (b) arrayList.get(i2);
                    if (b2) {
                        if (str.equals(bVar.h)) {
                            return bVar;
                        }
                    } else if (PhoneNumberUtils.compare(str, bVar.h)) {
                        return bVar;
                    }
                }
            } else {
                arrayList = new ArrayList();
            }
            b bVar2 = new b(str);
            arrayList.add(bVar2);
            this.g.put(charBuffer, arrayList);
            return bVar2;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.snda.youni.modules.b.b.a(com.snda.youni.modules.b.b, boolean):boolean
     arg types: [com.snda.youni.modules.b.b, int]
     candidates:
      com.snda.youni.modules.b.b.a(com.snda.youni.modules.b.b, int):int
      com.snda.youni.modules.b.b.a(com.snda.youni.modules.b.b, long):long
      com.snda.youni.modules.b.b.a(com.snda.youni.modules.b.b, android.graphics.drawable.BitmapDrawable):android.graphics.drawable.BitmapDrawable
      com.snda.youni.modules.b.b.a(java.lang.String, boolean):com.snda.youni.modules.b.b
      com.snda.youni.modules.b.b.a(com.snda.youni.modules.b.b, java.lang.String):java.lang.String
      com.snda.youni.modules.b.b.a(com.snda.youni.modules.b.b, byte[]):byte[]
      com.snda.youni.modules.b.b.a(com.snda.youni.modules.b.b, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.snda.youni.modules.b.b.b(com.snda.youni.modules.b.b, boolean):boolean
     arg types: [com.snda.youni.modules.b.b, int]
     candidates:
      com.snda.youni.modules.b.b.b(com.snda.youni.modules.b.b, java.lang.String):java.lang.String
      com.snda.youni.modules.b.b.b(com.snda.youni.modules.b.b, boolean):boolean */
    public final b a(String str, boolean z) {
        b b2 = b(TextUtils.isEmpty(str) ? "" : str);
        c cVar = null;
        synchronized (b2) {
            while (z) {
                if (b2.s) {
                    try {
                        b2.wait();
                    } catch (InterruptedException e2) {
                    }
                }
            }
            if (b2.r && !b2.s) {
                boolean unused = b2.r = false;
                cVar = new c(this, b2);
                boolean unused2 = b2.s = true;
            }
        }
        if (cVar != null) {
            if (z) {
                cVar.run();
            } else {
                this.f527a.a(cVar);
            }
        }
        return b2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.snda.youni.modules.b.b.a(com.snda.youni.modules.b.b, boolean):boolean
     arg types: [com.snda.youni.modules.b.b, int]
     candidates:
      com.snda.youni.modules.b.b.a(com.snda.youni.modules.b.b, int):int
      com.snda.youni.modules.b.b.a(com.snda.youni.modules.b.b, long):long
      com.snda.youni.modules.b.b.a(com.snda.youni.modules.b.b, android.graphics.drawable.BitmapDrawable):android.graphics.drawable.BitmapDrawable
      com.snda.youni.modules.b.b.a(java.lang.String, boolean):com.snda.youni.modules.b.b
      com.snda.youni.modules.b.b.a(com.snda.youni.modules.b.b, java.lang.String):java.lang.String
      com.snda.youni.modules.b.b.a(com.snda.youni.modules.b.b, byte[]):byte[]
      com.snda.youni.modules.b.b.a(com.snda.youni.modules.b.b, boolean):boolean */
    /* access modifiers changed from: package-private */
    public final void a() {
        synchronized (this) {
            for (ArrayList it : this.g.values()) {
                Iterator it2 = it.iterator();
                while (true) {
                    if (it2.hasNext()) {
                        b bVar = (b) it2.next();
                        synchronized (bVar) {
                            boolean unused = bVar.r = true;
                        }
                    }
                }
            }
        }
    }
}
