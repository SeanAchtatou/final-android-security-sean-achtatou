package com.snda.youni.modules.b;

import java.util.ArrayList;

final class g {

    /* renamed from: a  reason: collision with root package name */
    private Thread f528a = new Thread(new e(this));
    /* access modifiers changed from: private */
    public final ArrayList b = new ArrayList();

    public g() {
        this.f528a.start();
    }

    public final void a(Runnable runnable) {
        synchronized (this.b) {
            this.b.add(runnable);
            this.b.notify();
        }
    }
}
