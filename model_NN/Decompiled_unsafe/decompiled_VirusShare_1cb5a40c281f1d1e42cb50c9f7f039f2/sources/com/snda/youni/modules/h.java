package com.snda.youni.modules;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.Toast;
import com.snda.youni.C0000R;

final class h implements TextWatcher {

    /* renamed from: a  reason: collision with root package name */
    private boolean f540a = false;
    private CharSequence b;
    private int c;
    private /* synthetic */ InputView d;

    h(InputView inputView) {
        this.d = inputView;
    }

    public final void afterTextChanged(Editable editable) {
        "afterTextChanged: " + editable.toString();
        "afterTextChanged: " + editable.length() + " Stop is " + this.f540a;
        this.d.f.d();
        if (this.d.f486a.getLineCount() > 2) {
            this.d.d.setText("" + editable.length());
            this.d.d.setVisibility(0);
        } else {
            this.d.d.setVisibility(8);
        }
        if (this.f540a) {
            if (System.currentTimeMillis() - this.d.g > 3000) {
                Toast.makeText(this.d.e, (int) C0000R.string.max_char_warning, 0).show();
                long unused = this.d.g = System.currentTimeMillis();
            }
            editable.replace(0, editable.length(), this.b);
            this.d.f486a.setSelection(this.c);
        }
    }

    public final void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        "beforeTextChanged：" + ((Object) charSequence) + " " + i + " " + i2 + " " + i3;
        if ((charSequence.length() + i3) - i2 > 500) {
            this.b = charSequence.toString();
            this.c = i;
            this.f540a = true;
        } else {
            this.f540a = false;
        }
        if (charSequence == null || (charSequence != null && "".equalsIgnoreCase(charSequence.toString()))) {
            this.d.f.c();
        }
    }

    public final void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }
}
