package com.snda.youni.modules.settings;

import android.content.res.TypedArray;
import com.snda.youni.C0000R;

public final class t extends m {
    private l c;

    public t(SettingsItemView settingsItemView) {
        super(settingsItemView, "shape_name", C0000R.string.settings_message_shape, C0000R.array.shape_details);
    }

    public final void a(int i) {
        super.a(i);
        b();
    }

    public final void a(l lVar) {
        this.c = lVar;
        b();
    }

    public final void b() {
        TypedArray obtainTypedArray = this.b.getResources().obtainTypedArray(C0000R.array.bg_out_details);
        int b = this.c.b();
        this.b.a(obtainTypedArray.getResourceId((b * a()) + this.c.c(), 0));
        obtainTypedArray.recycle();
    }
}
