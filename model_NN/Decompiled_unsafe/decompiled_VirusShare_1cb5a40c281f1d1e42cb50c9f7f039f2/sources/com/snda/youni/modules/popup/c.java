package com.snda.youni.modules.popup;

import android.app.Activity;
import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ViewFlipper;
import com.snda.youni.C0000R;
import com.snda.youni.d;
import com.snda.youni.d.a;
import com.snda.youni.e.n;
import com.snda.youni.modules.a.k;

public final class c implements TextWatcher, View.OnClickListener, View.OnTouchListener {

    /* renamed from: a  reason: collision with root package name */
    private d f557a;
    private EditText b;
    private Button c;
    private Button d;
    private Button e;
    private ViewFlipper f;
    private a g = a(this.j);
    private float h;
    private boolean i;
    private final LayoutInflater j;
    private Context k;

    public c(Activity activity) {
        this.j = (LayoutInflater) activity.getSystemService("layout_inflater");
        this.f = (ViewFlipper) activity.findViewById(C0000R.id.flipper);
        this.b = (EditText) activity.findViewById(C0000R.id.input);
        this.c = (Button) activity.findViewById(C0000R.id.call);
        this.d = (Button) activity.findViewById(C0000R.id.delete);
        this.e = (Button) activity.findViewById(C0000R.id.reply);
        this.c.setOnClickListener(this);
        this.d.setOnClickListener(this);
        this.e.setOnClickListener(this);
        this.b.setOnClickListener(this);
        this.b.addTextChangedListener(this);
        this.k = activity;
    }

    private a a(LayoutInflater layoutInflater) {
        View inflate = layoutInflater.inflate((int) C0000R.layout.model_popup_flip, (ViewGroup) null);
        this.f.addView(inflate);
        return new a(this, inflate);
    }

    public final void a() {
        this.e.setBackgroundDrawable(a.a("btn_bg_popup_blue"));
        this.g.f.setBackgroundDrawable(a.a("btn_popup_cancel"));
        this.c.setTextColor(a.b("popup_white_btn"));
        this.d.setTextColor(a.b("popup_white_btn"));
    }

    public final void a(int i2, int i3) {
        this.g.a(i2, i3);
    }

    public final void a(Context context, k kVar, int i2, int i3, d dVar, boolean z) {
        a a2 = a(this.j);
        a2.a(kVar, dVar);
        a2.a(i2, i3);
        if (z) {
            this.f.setInAnimation(AnimationUtils.loadAnimation(context, C0000R.anim.push_right_in));
            this.f.setOutAnimation(AnimationUtils.loadAnimation(context, C0000R.anim.push_right_out));
        } else {
            this.f.setInAnimation(AnimationUtils.loadAnimation(context, C0000R.anim.push_left_in));
            this.f.setOutAnimation(AnimationUtils.loadAnimation(context, C0000R.anim.push_left_out));
        }
        this.f.showNext();
        this.g = a2;
    }

    public final void a(k kVar, d dVar) {
        this.g.a(kVar, dVar);
    }

    public final void a(d dVar) {
        this.f557a = dVar;
    }

    public final void afterTextChanged(Editable editable) {
    }

    public final void beforeTextChanged(CharSequence charSequence, int i2, int i3, int i4) {
        "beforeTextChanged：" + ((Object) charSequence) + " " + i2 + " " + i3 + " " + i4;
        if (charSequence == null || (charSequence != null && "".equalsIgnoreCase(charSequence.toString()))) {
            this.f557a.e();
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public final void onClick(View view) {
        n.a(view.getContext()).a();
        if (this.f557a != null) {
            switch (view.getId()) {
                case C0000R.id.text /*2131558581*/:
                    this.f557a.b();
                    return;
                case C0000R.id.head /*2131558589*/:
                case C0000R.id.name /*2131558590*/:
                default:
                    return;
                case C0000R.id.cancel /*2131558592*/:
                    com.snda.youni.g.a.a(this.k.getApplicationContext(), "popup_x", null);
                    break;
                case C0000R.id.time /*2131558593*/:
                    break;
                case C0000R.id.left_arrow /*2131558594*/:
                    this.f557a.a(true);
                    return;
                case C0000R.id.right_arrow /*2131558595*/:
                    this.f557a.a(false);
                    return;
                case C0000R.id.call /*2131558638*/:
                    this.f557a.c();
                    return;
                case C0000R.id.delete /*2131558639*/:
                    this.f557a.d();
                    return;
                case C0000R.id.reply /*2131558640*/:
                    this.f557a.a(this.b.getText().toString());
                    this.b.setText("");
                    return;
            }
            this.f557a.a();
        }
    }

    public final void onTextChanged(CharSequence charSequence, int i2, int i3, int i4) {
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        n.a(view.getContext()).a();
        if (this.f557a == null) {
            return false;
        }
        if (motionEvent.getAction() == 0) {
            this.i = true;
            this.h = motionEvent.getX();
            return true;
        }
        if (this.i && motionEvent.getAction() == 1) {
            if (motionEvent.getX() > this.h + 20.0f) {
                this.i = false;
                this.f557a.a(true);
                return true;
            } else if (motionEvent.getX() < this.h - 20.0f) {
                this.i = false;
                this.f557a.a(false);
                return true;
            } else {
                onClick(view);
                this.i = false;
            }
        }
        return false;
    }
}
