package com.snda.youni.modules.settings;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import com.snda.youni.c.g;
import com.snda.youni.c.t;
import java.io.ByteArrayOutputStream;

final class d implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Context f567a;
    final /* synthetic */ j b;

    d(j jVar, Context context) {
        this.b = jVar;
        this.f567a = context;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        g gVar = new g();
        Bitmap createScaledBitmap = Bitmap.createScaledBitmap(((BitmapDrawable) this.b.f.getDrawable()).getBitmap(), 100, 100, false);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        createScaledBitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
        gVar.a(byteArrayOutputStream.toByteArray());
        t.a(gVar, new w(this), this.f567a);
    }
}
