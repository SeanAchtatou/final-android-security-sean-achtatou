package com.snda.youni.modules;

import android.view.View;
import com.snda.youni.activities.bw;
import com.snda.youni.g.a;

final class i implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ InputView f541a;

    i(InputView inputView) {
        this.f541a = inputView;
    }

    public final void onClick(View view) {
        view.setEnabled(false);
        this.f541a.a(this.f541a.e);
        new bw(this.f541a.getContext(), this.f541a.h, 0).show();
        this.f541a.f.e();
        a.a(this.f541a.e.getApplicationContext(), "plus_button", null);
    }
}
