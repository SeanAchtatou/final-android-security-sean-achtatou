package com.snda.youni.modules;

import android.content.Context;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import com.snda.youni.C0000R;
import com.snda.youni.d.a;
import com.snda.youni.d.b;
import com.snda.youni.d.c;
import java.util.Iterator;
import java.util.List;

public final class d extends BaseAdapter {

    /* renamed from: a  reason: collision with root package name */
    Context f534a;
    List b;
    int c;

    public d(Context context, List list) {
        this.f534a = context;
        this.b = list;
        String string = PreferenceManager.getDefaultSharedPreferences(this.f534a).getString("skin", null);
        "ljd userSkin is " + string;
        Iterator it = list.iterator();
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            c cVar = (c) it.next();
            if (cVar.a().equals(string)) {
                this.c = this.b.indexOf(cVar);
                break;
            }
        }
        "ljd mSelectedPosition is " + this.c;
    }

    public final void a(List list) {
        this.b = list;
    }

    public final int getCount() {
        return this.b.size();
    }

    public final Object getItem(int i) {
        return this.b.get(i);
    }

    public final long getItemId(int i) {
        return (long) i;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public final View getView(int i, View view, ViewGroup viewGroup) {
        View inflate = LayoutInflater.from(this.f534a).inflate((int) C0000R.layout.item_list_skin, viewGroup, false);
        c cVar = (c) this.b.get(i);
        if (b.c(cVar.a())) {
            ((ImageView) inflate.findViewById(C0000R.id.skin_preview)).setImageDrawable(cVar.d);
            ((TextView) inflate.findViewById(C0000R.id.skin_name)).setText(cVar.f391a);
            inflate.findViewById(C0000R.id.skin_download).setVisibility(8);
            RadioButton radioButton = (RadioButton) inflate.findViewById(C0000R.id.skin_select);
            radioButton.setVisibility(0);
            radioButton.setOnClickListener(new ag(this, i));
            if (this.c == i) {
                radioButton.setChecked(true);
            } else {
                radioButton.setChecked(false);
            }
        } else {
            ((TextView) inflate.findViewById(C0000R.id.skin_name)).setText(cVar.f391a);
            inflate.findViewById(C0000R.id.skin_select).setVisibility(8);
            Button button = (Button) inflate.findViewById(C0000R.id.skin_download);
            button.setBackgroundDrawable(a.a("btn_bg"));
            button.setVisibility(0);
            button.setOnClickListener(new ag(this, i));
        }
        return inflate;
    }
}
