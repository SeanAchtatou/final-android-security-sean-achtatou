package com.snda.youni.modules.chat;

import android.app.Activity;
import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;

public class LinearLayoutThatDetectsSoftKeyboard extends LinearLayout {

    /* renamed from: a  reason: collision with root package name */
    private a f533a;

    public LinearLayoutThatDetectsSoftKeyboard(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public final void a(a aVar) {
        this.f533a = aVar;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        int size = View.MeasureSpec.getSize(i2);
        Activity activity = (Activity) getContext();
        Rect rect = new Rect();
        activity.getWindow().getDecorView().getWindowVisibleDisplayFrame(rect);
        int height = (activity.getWindowManager().getDefaultDisplay().getHeight() - rect.top) - size;
        if (this.f533a != null) {
            this.f533a.a(height > 128);
        }
        super.onMeasure(i, i2);
    }
}
