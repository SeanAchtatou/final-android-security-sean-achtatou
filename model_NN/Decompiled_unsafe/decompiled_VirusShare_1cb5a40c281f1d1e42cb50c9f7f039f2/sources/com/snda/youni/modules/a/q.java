package com.snda.youni.modules.a;

import android.a.o;
import android.app.ProgressDialog;
import android.content.AsyncQueryHandler;
import android.content.ContentUris;
import android.content.DialogInterface;

final class q implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private final long f502a;
    private final AsyncQueryHandler b;
    private boolean c;
    private /* synthetic */ e d;

    public q(e eVar, long j, AsyncQueryHandler asyncQueryHandler) {
        this.d = eVar;
        this.f502a = j;
        this.b = asyncQueryHandler;
    }

    public final void a(boolean z) {
        this.c = z;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        ProgressDialog unused = this.d.j = ProgressDialog.show(this.d.l, null, null);
        this.d.j.setCancelable(false);
        if (this.f502a == -1) {
            this.b.startDelete(0, Integer.valueOf((int) this.f502a), o.f16a, this.c ? null : "locked=0", null);
        } else {
            this.b.startDelete(0, Integer.valueOf((int) this.f502a), ContentUris.withAppendedId(o.f16a, this.f502a), this.c ? null : "locked=0", null);
        }
    }
}
