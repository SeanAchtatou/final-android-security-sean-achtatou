package com.snda.youni.modules;

import android.database.Cursor;
import android.view.View;
import com.snda.youni.activities.SettingsBlackListActivity;

final class e implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private String f537a;
    private String b;
    private /* synthetic */ ah c;

    public e(ah ahVar, Cursor cursor) {
        this.c = ahVar;
        this.f537a = cursor.getString(0);
        this.b = cursor.getString(1);
    }

    public final void onClick(View view) {
        ah.f516a = this.f537a;
        ah.b = this.b;
        ((SettingsBlackListActivity) this.c.c).removeDialog(0);
        ((SettingsBlackListActivity) this.c.c).showDialog(0);
    }
}
