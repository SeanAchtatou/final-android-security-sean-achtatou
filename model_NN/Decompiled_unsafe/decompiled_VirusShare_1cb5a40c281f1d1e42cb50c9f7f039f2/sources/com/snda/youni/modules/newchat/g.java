package com.snda.youni.modules.newchat;

public final class g {

    /* renamed from: a  reason: collision with root package name */
    private String f552a;
    private String b;

    public g(String str, String str2) {
        this.f552a = str;
        this.b = str2;
    }

    public final String a() {
        return this.f552a;
    }

    public final void a(String str) {
        this.f552a = str;
    }

    public final String b() {
        return this.b;
    }

    public final void b(String str) {
        this.b = str;
    }

    public final boolean equals(Object obj) {
        if (!(obj instanceof g)) {
            return false;
        }
        return this.b.equals(((g) obj).b);
    }
}
