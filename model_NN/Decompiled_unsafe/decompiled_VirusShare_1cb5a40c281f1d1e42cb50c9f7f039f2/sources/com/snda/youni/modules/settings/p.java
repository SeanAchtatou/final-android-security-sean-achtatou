package com.snda.youni.modules.settings;

import android.content.res.TypedArray;
import com.snda.youni.C0000R;

public final class p extends m {
    private static final int[] c = {2, 1, 0};

    public p(SettingsItemView settingsItemView) {
        super(settingsItemView, "image_size_name", C0000R.string.settings_message_imagesize, C0000R.array.image_size_details);
        int i;
        b(a());
        switch (c()) {
            case 0:
                i = 20;
                break;
            case 1:
                i = 50;
                break;
            case 2:
                i = 200;
                break;
            default:
                i = 50;
                break;
        }
        this.b.c(this.b.getContext().getString(C0000R.string.settings_message_imagesize_sub, Integer.valueOf(i)));
    }

    private void b(int i) {
        TypedArray obtainTypedArray = this.b.getContext().getResources().obtainTypedArray(this.f572a);
        this.b.b(obtainTypedArray.getString(i));
        obtainTypedArray.recycle();
    }

    public final int a() {
        int c2 = c();
        for (int i = 0; i < c.length; i++) {
            if (c[i] == c2) {
                return i;
            }
        }
        return c[1];
    }

    public final void a(int i) {
        int i2;
        super.a(c[i]);
        b(i);
        int c2 = c();
        a_(c2);
        switch (c2) {
            case 0:
                i2 = 20;
                break;
            case 1:
                i2 = 50;
                break;
            case 2:
                i2 = 200;
                break;
            default:
                i2 = 50;
                break;
        }
        this.b.c(this.b.getContext().getString(C0000R.string.settings_message_imagesize_sub, Integer.valueOf(i2)));
    }
}
