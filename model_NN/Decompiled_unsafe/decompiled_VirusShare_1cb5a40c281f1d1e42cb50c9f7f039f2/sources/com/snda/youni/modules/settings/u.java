package com.snda.youni.modules.settings;

import com.snda.youni.C0000R;

public final class u extends y {
    public u(SettingsItemView settingsItemView, String str) {
        super(settingsItemView, str);
        this.b.c(this.b.getContext().getString(C0000R.string.settings_message_vib_sub, Integer.valueOf(c())));
    }

    public final void a(int i) {
        a_(i);
        this.b.c(this.b.getContext().getString(C0000R.string.settings_message_vib_sub, Integer.valueOf(i)));
    }
}
