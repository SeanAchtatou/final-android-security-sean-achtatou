package com.snda.youni.modules.newchat;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.CheckBox;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import com.snda.youni.C0000R;
import com.snda.youni.activities.RecipientsActivity;
import com.snda.youni.b.ai;
import com.snda.youni.d;

public final class c extends CursorAdapter implements AbsListView.OnScrollListener {

    /* renamed from: a  reason: collision with root package name */
    private d f548a;
    private Context b;
    private LayoutInflater c;
    private boolean d = false;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.widget.CursorAdapter.<init>(android.content.Context, android.database.Cursor, boolean):void}
     arg types: [android.content.Context, ?[OBJECT, ARRAY], int]
     candidates:
      ClspMth{android.widget.CursorAdapter.<init>(android.content.Context, android.database.Cursor, int):void}
      ClspMth{android.widget.CursorAdapter.<init>(android.content.Context, android.database.Cursor, boolean):void} */
    public c(Context context, d dVar) {
        super(context, (Cursor) null, false);
        this.b = context;
        this.f548a = dVar;
        this.c = (LayoutInflater) context.getSystemService("layout_inflater");
    }

    private static void a(TextView textView, String str, String str2) {
        String lowerCase = str.toLowerCase();
        String lowerCase2 = str2.toLowerCase();
        int indexOf = lowerCase.indexOf(lowerCase2);
        if (indexOf >= 0) {
            SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(lowerCase);
            spannableStringBuilder.setSpan(new ForegroundColorSpan(Color.parseColor("#ff7e00")), indexOf, lowerCase2.length() + indexOf, 33);
            textView.setText(spannableStringBuilder);
        }
    }

    public final void a(boolean z) {
        this.d = z;
    }

    public final void bindView(View view, Context context, Cursor cursor) {
        a aVar;
        String string = cursor.getString(2);
        String string2 = cursor.getString(3);
        if (view.getTag() == null) {
            aVar = new a(this);
            aVar.f547a = (ImageView) view.findViewById(C0000R.id.recipients_photo);
            aVar.b = (TextView) view.findViewById(C0000R.id.recipients_name);
            aVar.c = (TextView) view.findViewById(C0000R.id.recipients_number);
            aVar.d = (CheckBox) view.findViewById(C0000R.id.recipients_checkBox);
            aVar.d.setButtonDrawable((int) C0000R.drawable.checkbox);
        } else {
            aVar = (a) view.getTag();
        }
        this.f548a.a(aVar.f547a, cursor.getInt(1), 0);
        aVar.b.setText(string);
        if (string2.contains("krobot")) {
            aVar.c.setText(this.b.getString(C0000R.string.youni_online_service));
        } else {
            aVar.c.setText(string2);
        }
        RecipientsActivity recipientsActivity = (RecipientsActivity) this.b;
        String k = recipientsActivity.k();
        if (!TextUtils.isEmpty(k)) {
            a(aVar.b, string, k);
            a(aVar.c, string2, k);
        }
        ListView h = recipientsActivity.h();
        int position = cursor.getPosition();
        if (recipientsActivity.i().c(string, string2)) {
            aVar.d.setChecked(true);
            h.setItemChecked(position, true);
            aVar.d.setVisibility(0);
        } else {
            aVar.d.setChecked(false);
            h.setItemChecked(position, false);
            if (this.d) {
                aVar.d.setVisibility(0);
            } else {
                aVar.d.setVisibility(4);
            }
        }
        ai m = recipientsActivity.m();
        if (m == null || !m.b() || !"1".equals(cursor.getString(6))) {
            view.findViewById(C0000R.id.recipients_online_btn).setVisibility(8);
        } else {
            view.findViewById(C0000R.id.recipients_online_btn).setVisibility(0);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public final View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
        return this.c.inflate((int) C0000R.layout.recipients_list_item, viewGroup, false);
    }

    public final void onScroll(AbsListView absListView, int i, int i2, int i3) {
    }

    public final void onScrollStateChanged(AbsListView absListView, int i) {
        if (i == 1 || i == 2) {
            ((RecipientsActivity) this.b).j();
        }
        if (i == 2) {
            this.f548a.b();
        } else {
            this.f548a.c();
        }
    }
}
