package com.snda.youni.modules.b;

final class e implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ g f526a;

    e(g gVar) {
        this.f526a = gVar;
    }

    public final void run() {
        while (true) {
            Runnable runnable = null;
            synchronized (this.f526a.b) {
                if (this.f526a.b.size() == 0) {
                    try {
                        this.f526a.b.wait();
                    } catch (InterruptedException e) {
                    }
                }
                if (this.f526a.b.size() > 0) {
                    runnable = (Runnable) this.f526a.b.remove(0);
                }
            }
            if (runnable != null) {
                runnable.run();
            }
        }
    }
}
