package com.snda.youni.modules;

import android.content.Context;
import android.os.Handler;
import android.text.Editable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.snda.youni.C0000R;
import com.snda.youni.activities.bw;

public class InputView extends LinearLayout {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public EditText f486a;
    /* access modifiers changed from: private */
    public ImageView b;
    private Button c;
    /* access modifiers changed from: private */
    public TextView d;
    /* access modifiers changed from: private */
    public Context e;
    /* access modifiers changed from: private */
    public l f;
    /* access modifiers changed from: private */
    public long g;
    /* access modifiers changed from: private */
    public Handler h = new f(this);

    public InputView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.e = context;
        View inflate = ((LayoutInflater) context.getSystemService("layout_inflater")).inflate((int) C0000R.layout.model_input, this);
        this.f486a = (EditText) inflate.findViewById(C0000R.id.edit_text);
        this.b = (ImageView) inflate.findViewById(C0000R.id.smiles);
        this.c = (Button) inflate.findViewById(C0000R.id.btn_send);
        this.c.setEnabled(false);
        this.d = (TextView) inflate.findViewById(C0000R.id.txt_mcount);
        this.c.setOnClickListener(new g(this));
        this.b.setOnClickListener(new i(this));
        this.f486a.setOnTouchListener(new j(this));
        this.f486a.addTextChangedListener(new h(this));
    }

    public final Editable a() {
        return this.f486a.getText();
    }

    public final void a(int i) {
        this.f486a.setSelection(i);
    }

    public final void a(Context context) {
        ((InputMethodManager) context.getSystemService("input_method")).hideSoftInputFromWindow(this.f486a.getWindowToken(), 0);
    }

    public final void a(l lVar) {
        this.f = lVar;
    }

    public final void a(CharSequence charSequence) {
        this.f486a.setText(charSequence);
    }

    public final void a(boolean z) {
        this.c.setEnabled(z);
    }

    public final void b() {
        this.b.setVisibility(8);
    }

    public final void b(boolean z) {
        if (z) {
            this.c.setText(this.e.getString(C0000R.string.chat_youni_send));
        } else {
            this.c.setText(this.e.getString(C0000R.string.chat_send));
        }
        if (z) {
            this.f486a.setHint((int) C0000R.string.hint_input_youni_bar);
        } else {
            this.f486a.setHint((int) C0000R.string.hint_input_bar);
        }
    }

    public final void c() {
        this.b.setVisibility(0);
    }

    public final void d() {
        this.h = null;
        this.f = null;
    }

    public final void e() {
        new bw(getContext(), this.h, 0).show();
    }

    public final void f() {
        new bw(getContext(), this.h, 1).show();
    }

    public final int g() {
        return this.f486a.length();
    }
}
