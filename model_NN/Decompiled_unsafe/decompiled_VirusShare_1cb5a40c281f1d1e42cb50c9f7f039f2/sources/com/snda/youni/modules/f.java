package com.snda.youni.modules;

import android.os.Handler;
import android.os.Message;
import com.snda.youni.e.m;

final class f extends Handler {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ InputView f538a;

    f(InputView inputView) {
        this.f538a = inputView;
    }

    public final void handleMessage(Message message) {
        String str = (String) message.getData().getCharSequence("smiletext");
        "smile=" + str;
        if (str != null) {
            int selectionStart = this.f538a.f486a.getSelectionStart();
            int selectionEnd = this.f538a.f486a.getSelectionEnd();
            if (selectionStart <= selectionEnd) {
                int i = selectionEnd;
                selectionEnd = selectionStart;
                selectionStart = i;
            }
            "s: " + String.valueOf(selectionEnd) + "   e: " + String.valueOf(selectionStart);
            StringBuilder sb = new StringBuilder(this.f538a.f486a.getText());
            sb.replace(selectionEnd, selectionStart, str);
            if (sb.length() > 500) {
                this.f538a.b.setEnabled(true);
                return;
            }
            sb.toString();
            this.f538a.f486a.setText(m.a().a(this.f538a.e, sb, false));
            this.f538a.f486a.setSelection(str.length() + selectionEnd);
        } else {
            int i2 = message.getData().getInt("audio");
            if (i2 > 0 && this.f538a.f != null) {
                this.f538a.f.b(i2);
            }
        }
        this.f538a.b.setEnabled(true);
    }
}
