package com.snda.youni.modules.settings;

import android.content.Context;
import android.content.res.TypedArray;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.snda.youni.C0000R;
import com.snda.youni.a;

public class SettingsItemView extends LinearLayout {

    /* renamed from: a  reason: collision with root package name */
    private static int f563a;
    private TextView b;
    private TextView c;
    private TextView d;
    private ImageView e;
    private ImageView f;
    private ImageView g;
    private ImageView h;
    private CheckBox i;
    private Context j;
    private boolean k;

    public SettingsItemView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        f563a = context.getResources().getDimensionPixelSize(C0000R.dimen.settings_item_min_height);
        setWillNotDraw(false);
        View inflate = ((LayoutInflater) context.getSystemService("layout_inflater")).inflate((int) C0000R.layout.model_item_settings, this);
        this.b = (TextView) inflate.findViewById(C0000R.id.title);
        this.c = (TextView) inflate.findViewById(C0000R.id.text);
        this.d = (TextView) inflate.findViewById(C0000R.id.subtitle);
        this.e = (ImageView) inflate.findViewById(C0000R.id.image);
        this.g = (ImageView) inflate.findViewById(C0000R.id.icon);
        this.f = (ImageView) inflate.findViewById(C0000R.id.arrow);
        this.i = (CheckBox) inflate.findViewById(C0000R.id.check);
        if (inflate.findViewById(C0000R.id.notify) instanceof ImageView) {
            this.h = (ImageView) inflate.findViewById(C0000R.id.notify);
        }
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, a.f171a);
        String string = obtainStyledAttributes.getString(1);
        String string2 = obtainStyledAttributes.getString(2);
        String string3 = obtainStyledAttributes.getString(0);
        String string4 = obtainStyledAttributes.getString(3);
        int resourceId = obtainStyledAttributes.getResourceId(4, 0);
        this.k = obtainStyledAttributes.getBoolean(5, true);
        obtainStyledAttributes.recycle();
        if (resourceId == 0) {
            this.g.setVisibility(8);
        } else {
            this.g.setVisibility(0);
            this.g.setImageResource(resourceId);
        }
        this.b.setText(string);
        if (string2 != null) {
            this.d.setText(string2);
            this.d.setVisibility(0);
        } else {
            this.d.setVisibility(8);
        }
        if (this.k) {
            if (string4 != null) {
                this.c.setText(string4);
                this.e.setVisibility(8);
                this.f.setVisibility(8);
                this.i.setVisibility(8);
                this.c.setVisibility(0);
            } else {
                this.e.setVisibility(8);
                this.f.setVisibility(0);
                this.c.setVisibility(8);
                this.i.setVisibility(8);
            }
            if (string3 != null) {
            }
        } else {
            this.e.setVisibility(8);
            if (TextUtils.isEmpty(string2)) {
                this.f.setVisibility(4);
            } else {
                this.f.setVisibility(8);
            }
            this.c.setVisibility(8);
            this.i.setVisibility(8);
        }
        this.j = context;
    }

    public final void a() {
        this.b.setTextAppearance(this.j, C0000R.style.Setting_RED_Title);
    }

    public final void a(int i2) {
        this.e.setVisibility(0);
        this.f.setVisibility(8);
        this.c.setVisibility(8);
        this.i.setVisibility(8);
        this.e.setBackgroundResource(i2);
    }

    public final void a(String str) {
        this.b.setText(str);
    }

    public final void a(boolean z) {
        this.e.setVisibility(8);
        this.f.setVisibility(8);
        this.c.setVisibility(8);
        this.i.setVisibility(0);
        this.i.setChecked(z);
    }

    public final void b() {
        this.c.setTextColor((int) C0000R.color.settings_sub);
    }

    public final void b(int i2) {
        this.e.setVisibility(i2);
    }

    public final void b(String str) {
        this.e.setVisibility(8);
        this.f.setVisibility(8);
        this.i.setVisibility(8);
        this.c.setVisibility(0);
        this.c.setText(str);
    }

    public final void c() {
        this.g.setImageResource(C0000R.drawable.update_notify_setting);
    }

    public final void c(String str) {
        this.d.setVisibility(0);
        this.d.setText(str);
        if (this.k) {
            return;
        }
        if (TextUtils.isEmpty(str)) {
            this.f.setVisibility(4);
        } else {
            this.f.setVisibility(8);
        }
    }

    public final boolean d() {
        return this.i.isChecked();
    }

    public void setClickable(boolean z) {
        if (z) {
            this.b.setTextAppearance(this.j, C0000R.style.Setting_Small_Title);
        } else {
            this.b.setTextAppearance(this.j, C0000R.style.Setting_SubTitle);
        }
        super.setClickable(z);
    }
}
