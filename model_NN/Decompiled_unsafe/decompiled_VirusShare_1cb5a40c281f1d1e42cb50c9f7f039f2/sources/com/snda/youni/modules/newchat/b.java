package com.snda.youni.modules.newchat;

import android.content.Context;
import android.graphics.Paint;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.widget.TextView;
import com.snda.youni.C0000R;
import java.util.ArrayList;
import java.util.Iterator;

public final class b extends ArrayList {
    public final int a(String str, String str2) {
        Iterator it = iterator();
        while (it.hasNext()) {
            g gVar = (g) it.next();
            if (gVar.b().equals(str2)) {
                if (!gVar.a().equals(gVar.b()) || gVar.a().equals(str)) {
                    return 1;
                }
                gVar.a(str);
                return 2;
            } else if (gVar.a().equals(str)) {
                if (str.equals(str2)) {
                    return 1;
                }
                if (gVar.a().equals(gVar.b())) {
                    gVar.b(str2);
                    return 2;
                }
            }
        }
        return add(new g(str, str2)) ? 0 : -1;
    }

    public final void a() {
        Iterator it = iterator();
        while (it.hasNext()) {
            g gVar = (g) it.next();
            "name: " + gVar.a() + "phone:" + gVar.b();
        }
    }

    public final void a(TextView textView, Context context) {
        String[] strArr;
        CharSequence charSequence;
        int i;
        if (size() <= 0) {
            textView.setText("");
            return;
        }
        if (size() <= 0) {
            strArr = null;
        } else {
            String[] strArr2 = new String[size()];
            Iterator it = iterator();
            int i2 = 0;
            while (it.hasNext()) {
                strArr2[i2] = ((g) it.next()).a();
                i2++;
            }
            strArr = strArr2;
        }
        StringBuffer stringBuffer = new StringBuffer();
        for (String str : strArr) {
            if (stringBuffer.length() != 0) {
                stringBuffer.append(',');
            }
            stringBuffer.append(str);
        }
        String string = context.getString(C0000R.string.send_to);
        Paint paint = new Paint();
        paint.setTextSize(textView.getTextSize());
        float measureText = paint.measureText(string + ((Object) stringBuffer));
        int measuredWidth = (textView.getMeasuredWidth() - textView.getPaddingLeft()) - textView.getPaddingRight();
        "width = " + measureText + ", textViewNoPadWidth = " + measuredWidth;
        int length = string.length() + 0;
        int length2 = stringBuffer.length() + length;
        "1 start = " + length + ", end = " + length2;
        if (measureText > ((float) measuredWidth)) {
            CharSequence string2 = context.getString(C0000R.string.inbox_multi_name, strArr[0], "" + size());
            i = strArr[0].length() + length;
            charSequence = string2;
        } else {
            charSequence = stringBuffer;
            i = length2;
        }
        "2 start = " + length + ", end = " + i;
        SpannableString spannableString = new SpannableString(string + ((Object) charSequence));
        spannableString.setSpan(new ForegroundColorSpan(context.getResources().getColor(C0000R.color.recipients_text_color)), length, i, 33);
        textView.setText(spannableString);
    }

    public final int b(String str, String str2) {
        return remove(new g(str, str2)) ? 0 : -1;
    }

    public final boolean c(String str, String str2) {
        return super.contains(new g(str, str2));
    }
}
