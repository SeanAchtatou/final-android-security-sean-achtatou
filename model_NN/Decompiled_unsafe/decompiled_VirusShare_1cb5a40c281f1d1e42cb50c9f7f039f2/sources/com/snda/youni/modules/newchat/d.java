package com.snda.youni.modules.newchat;

import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextUtils;

final class d implements InputFilter {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ RecipientsEditor f549a;

    d(RecipientsEditor recipientsEditor) {
        this.f549a = recipientsEditor;
    }

    public final CharSequence filter(CharSequence charSequence, int i, int i2, Spanned spanned, int i3, int i4) {
        if (TextUtils.isEmpty(charSequence) || this.f549a.getSelectionStart() == this.f549a.getText().length()) {
            return null;
        }
        this.f549a.setSelection(this.f549a.getText().length());
        return "";
    }
}
