package com.snda.youni.modules.newchat;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.text.style.DynamicDrawableSpan;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

public class f extends DynamicDrawableSpan {

    /* renamed from: a  reason: collision with root package name */
    private static final int f551a = View.MeasureSpec.makeMeasureSpec(0, 0);
    private final FrameLayout b;
    private final Resources c;
    private float d;

    public f(Context context, View view, float f) {
        this.c = context.getResources();
        this.b = new FrameLayout(context);
        this.b.addView(view);
        this.b.setDrawingCacheEnabled(true);
        this.d = f;
    }

    public Drawable getDrawable() {
        this.b.measure(f551a, f551a);
        this.b.layout(0, 0, this.b.getMeasuredWidth(), this.b.getMeasuredHeight());
        BitmapDrawable bitmapDrawable = new BitmapDrawable(this.c, this.b.getDrawingCache());
        bitmapDrawable.setBounds(0, 0, bitmapDrawable.getIntrinsicWidth(), bitmapDrawable.getIntrinsicHeight());
        return bitmapDrawable;
    }

    public int getSize(Paint paint, CharSequence charSequence, int i, int i2, Paint.FontMetricsInt fontMetricsInt) {
        int size = super.getSize(paint, charSequence, i, i2, fontMetricsInt);
        if (fontMetricsInt != null && (this.b.getChildAt(0) instanceof TextView)) {
            "fm 1 bottom = " + fontMetricsInt.bottom + ", top = " + fontMetricsInt.top + ", ascent = " + fontMetricsInt.ascent + ", descent = " + fontMetricsInt.descent;
            Paint paint2 = new Paint();
            paint2.setTextSize(this.d);
            Paint.FontMetricsInt fontMetricsInt2 = paint2.getFontMetricsInt();
            int i3 = (fontMetricsInt.descent - fontMetricsInt.top) - ((fontMetricsInt2.descent - fontMetricsInt2.top) + 2);
            fontMetricsInt.bottom += i3;
            fontMetricsInt.descent += i3;
            fontMetricsInt.ascent += i3;
            fontMetricsInt.top = i3 + fontMetricsInt.top;
            "fm 2 bottom = " + fontMetricsInt.bottom + ", top = " + fontMetricsInt.top + ", ascent = " + fontMetricsInt.ascent + ", descent = " + fontMetricsInt.descent;
        }
        return size;
    }
}
