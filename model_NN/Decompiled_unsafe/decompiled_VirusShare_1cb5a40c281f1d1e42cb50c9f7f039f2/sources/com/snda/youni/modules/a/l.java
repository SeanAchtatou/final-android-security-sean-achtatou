package com.snda.youni.modules.a;

import android.a.o;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.text.TextUtils;
import com.sd.android.mms.d.d;
import com.snda.youni.modules.b.b;
import com.snda.youni.modules.b.h;
import java.util.ArrayList;
import java.util.Iterator;

public final class l {

    /* renamed from: a  reason: collision with root package name */
    public static final String[] f497a = {"_id", "date", "recipient_ids", "snippet", "snippet_cs", "read", "error", "has_attachment"};
    private static final Uri b = o.f16a.buildUpon().appendQueryParameter("simple", "true").build();
    private static final String[] c = {"_id", "read"};
    private static final String[] d = {"seen"};
    private static boolean m;
    private final Context e;
    /* access modifiers changed from: private */
    public long f;
    private h g;
    private long h;
    private String i;
    private boolean j;
    private boolean k;
    private boolean l;
    private Object n = new Object();

    private l(Context context, long j2) {
        this.e = context;
        if (!a(j2)) {
            this.g = new h();
            this.f = 0;
        }
    }

    private l(Context context, Cursor cursor, boolean z) {
        this.e = context;
        a(context, this, cursor, z);
    }

    public static l a(Context context, long j2) {
        l a2 = u.a(j2);
        if (a2 == null) {
            a2 = new l(context, j2);
            try {
                u.a(a2);
            } catch (IllegalStateException e2) {
            }
        }
        return a2;
    }

    public static l a(Context context, Cursor cursor) {
        l lVar;
        long j2 = cursor.getLong(0);
        if (j2 <= 0 || (lVar = u.a(j2)) == null) {
            lVar = new l(context, cursor, false);
            try {
                u.a(lVar);
            } catch (IllegalStateException e2) {
            }
        } else {
            a(context, lVar, cursor, false);
        }
        return lVar;
    }

    public static void a(Context context) {
        new Thread(new v(context)).start();
    }

    public static void a(Context context, int i2) {
        Intent intent = new Intent("com.snda.youni.action.conversation_changed");
        intent.setPackage(context.getPackageName());
        intent.putExtra("extra_conversation_changed_thread_id", i2);
        context.sendBroadcast(intent);
    }

    /* JADX WARNING: Removed duplicated region for block: B:38:0x00b9 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void a(android.content.Context r6, com.snda.youni.modules.a.l r7, android.database.Cursor r8, boolean r9) {
        /*
            r5 = 0
            r4 = 1
            monitor-enter(r7)
            r0 = 0
            long r0 = r8.getLong(r0)     // Catch:{ all -> 0x00c2 }
            r7.f = r0     // Catch:{ all -> 0x00c2 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x00c2 }
            r0.<init>()     // Catch:{ all -> 0x00c2 }
            java.lang.String r1 = "conv.mDate="
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ all -> 0x00c2 }
            long r1 = r7.h     // Catch:{ all -> 0x00c2 }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ all -> 0x00c2 }
            java.lang.String r1 = ", c.getLong(DATE)="
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ all -> 0x00c2 }
            r1 = 1
            long r1 = r8.getLong(r1)     // Catch:{ all -> 0x00c2 }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ all -> 0x00c2 }
            r0.toString()     // Catch:{ all -> 0x00c2 }
            long r0 = r7.h     // Catch:{ all -> 0x00c2 }
            r2 = 0
            int r0 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r0 <= 0) goto L_0x00cc
            long r0 = r7.h     // Catch:{ all -> 0x00c2 }
            r2 = 1
            long r2 = r8.getLong(r2)     // Catch:{ all -> 0x00c2 }
            int r0 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r0 == 0) goto L_0x00cc
            r0 = r4
        L_0x0041:
            r1 = 1
            long r1 = r8.getLong(r1)     // Catch:{ all -> 0x00c2 }
            r7.h = r1     // Catch:{ all -> 0x00c2 }
            java.lang.String r1 = com.snda.youni.mms.ui.m.a(r8)     // Catch:{ all -> 0x00c2 }
            boolean r2 = android.text.TextUtils.isEmpty(r1)     // Catch:{ all -> 0x00c2 }
            if (r2 == 0) goto L_0x0059
            r1 = 2131362037(0x7f0a00f5, float:1.8343843E38)
            java.lang.String r1 = r6.getString(r1)     // Catch:{ all -> 0x00c2 }
        L_0x0059:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x00c2 }
            r2.<init>()     // Catch:{ all -> 0x00c2 }
            java.lang.String r3 = "conv.mSnippet = "
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x00c2 }
            java.lang.String r3 = r7.i     // Catch:{ all -> 0x00c2 }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x00c2 }
            java.lang.String r3 = ",snippet = "
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x00c2 }
            java.lang.StringBuilder r2 = r2.append(r1)     // Catch:{ all -> 0x00c2 }
            r2.toString()     // Catch:{ all -> 0x00c2 }
            java.lang.String r2 = r7.i     // Catch:{ all -> 0x00c2 }
            if (r2 == 0) goto L_0x0084
            java.lang.String r2 = r7.i     // Catch:{ all -> 0x00c2 }
            boolean r2 = r1.equals(r2)     // Catch:{ all -> 0x00c2 }
            if (r2 != 0) goto L_0x0084
            r0 = r4
        L_0x0084:
            r7.i = r1     // Catch:{ all -> 0x00c2 }
            r1 = 5
            int r1 = r8.getInt(r1)     // Catch:{ all -> 0x00c2 }
            if (r1 != 0) goto L_0x00bd
            r1 = r4
        L_0x008e:
            monitor-enter(r7)     // Catch:{ all -> 0x00c2 }
            r7.j = r1     // Catch:{ all -> 0x00bf }
            monitor-exit(r7)     // Catch:{ all -> 0x00bf }
            r1 = 6
            int r1 = r8.getInt(r1)     // Catch:{ all -> 0x00c2 }
            if (r1 == 0) goto L_0x00c5
            r1 = r4
        L_0x009a:
            r7.l = r1     // Catch:{ all -> 0x00c2 }
            r1 = 7
            int r1 = r8.getInt(r1)     // Catch:{ all -> 0x00c2 }
            if (r1 == 0) goto L_0x00c7
            r1 = r4
        L_0x00a4:
            r7.k = r1     // Catch:{ all -> 0x00c2 }
            if (r0 == 0) goto L_0x00ae
            long r0 = r7.f     // Catch:{ all -> 0x00c2 }
            int r0 = (int) r0     // Catch:{ all -> 0x00c2 }
            a(r6, r0)     // Catch:{ all -> 0x00c2 }
        L_0x00ae:
            monitor-exit(r7)     // Catch:{ all -> 0x00c2 }
            r0 = 2
            java.lang.String r0 = r8.getString(r0)
            com.snda.youni.modules.b.h r0 = com.snda.youni.modules.b.h.a(r0, r9)
            monitor-enter(r7)
            r7.g = r0     // Catch:{ all -> 0x00c9 }
            monitor-exit(r7)     // Catch:{ all -> 0x00c9 }
            return
        L_0x00bd:
            r1 = r5
            goto L_0x008e
        L_0x00bf:
            r0 = move-exception
            monitor-exit(r7)     // Catch:{ all -> 0x00bf }
            throw r0     // Catch:{ all -> 0x00c2 }
        L_0x00c2:
            r0 = move-exception
            monitor-exit(r7)     // Catch:{ all -> 0x00c2 }
            throw r0
        L_0x00c5:
            r1 = r5
            goto L_0x009a
        L_0x00c7:
            r1 = r5
            goto L_0x00a4
        L_0x00c9:
            r0 = move-exception
            monitor-exit(r7)     // Catch:{ all -> 0x00c9 }
            throw r0
        L_0x00cc:
            r0 = r5
            goto L_0x0041
        */
        throw new UnsupportedOperationException("Method not decompiled: com.snda.youni.modules.a.l.a(android.content.Context, com.snda.youni.modules.a.l, android.database.Cursor, boolean):void");
    }

    /* JADX INFO: finally extract failed */
    private boolean a(long j2) {
        Cursor query = this.e.getContentResolver().query(b, f497a, "_id=" + Long.toString(j2), null, null);
        try {
            if (query.moveToFirst()) {
                a(this.e, this, query, false);
                if (j2 != this.f) {
                    "loadFromThreadId: fillFromCursor returned differnt thread_id! threadId=" + j2 + ", mThreadId=" + this.f;
                }
                query.close();
                return true;
            }
            query.close();
            return false;
        } catch (Throwable th) {
            query.close();
            throw th;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0029, code lost:
        if (r0.moveToNext() == false) goto L_0x0073;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x002b, code lost:
        r1 = r0.getLong(0);
        r6.add(java.lang.Long.valueOf(r1));
        r3 = com.snda.youni.modules.a.u.a();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x003b, code lost:
        monitor-enter(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:?, code lost:
        r1 = com.snda.youni.modules.a.u.a(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0040, code lost:
        monitor-exit(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0041, code lost:
        if (r1 != null) goto L_0x006e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:?, code lost:
        r1 = new com.snda.youni.modules.a.l(r7, r0, true);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:?, code lost:
        r2 = com.snda.youni.modules.a.u.a();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x004d, code lost:
        monitor-enter(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:?, code lost:
        com.snda.youni.modules.a.u.a(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0051, code lost:
        monitor-exit(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x005e, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x005f, code lost:
        if (r0 != null) goto L_0x0061;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x0061, code lost:
        r0.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x0068, code lost:
        monitor-enter(com.snda.youni.modules.a.u.a());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:?, code lost:
        com.snda.youni.modules.a.l.m = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x006d, code lost:
        throw r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:?, code lost:
        a(r7, r1, r0, true);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x0073, code lost:
        if (r0 == null) goto L_0x0078;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x0075, code lost:
        r0.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x0078, code lost:
        r0 = com.snda.youni.modules.a.u.a();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x007c, code lost:
        monitor-enter(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:?, code lost:
        com.snda.youni.modules.a.l.m = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x0080, code lost:
        monitor-exit(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x0081, code lost:
        com.snda.youni.modules.a.u.a(r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:89:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0010, code lost:
        r6 = new java.util.HashSet();
        r0 = r7.getContentResolver().query(com.snda.youni.modules.a.l.b, com.snda.youni.modules.a.l.f497a, null, null, null);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0023, code lost:
        if (r0 == null) goto L_0x0073;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static /* synthetic */ void b(android.content.Context r7) {
        /*
            r3 = 0
            com.snda.youni.modules.a.u r0 = com.snda.youni.modules.a.u.a()
            monitor-enter(r0)
            boolean r1 = com.snda.youni.modules.a.l.m     // Catch:{ all -> 0x0058 }
            if (r1 == 0) goto L_0x000c
            monitor-exit(r0)     // Catch:{ all -> 0x0058 }
        L_0x000b:
            return
        L_0x000c:
            r1 = 1
            com.snda.youni.modules.a.l.m = r1     // Catch:{ all -> 0x0058 }
            monitor-exit(r0)     // Catch:{ all -> 0x0058 }
            java.util.HashSet r6 = new java.util.HashSet
            r6.<init>()
            android.content.ContentResolver r0 = r7.getContentResolver()
            android.net.Uri r1 = com.snda.youni.modules.a.l.b
            java.lang.String[] r2 = com.snda.youni.modules.a.l.f497a
            r4 = r3
            r5 = r3
            android.database.Cursor r0 = r0.query(r1, r2, r3, r4, r5)
            if (r0 == 0) goto L_0x0073
        L_0x0025:
            boolean r1 = r0.moveToNext()     // Catch:{ all -> 0x005e }
            if (r1 == 0) goto L_0x0073
            r1 = 0
            long r1 = r0.getLong(r1)     // Catch:{ all -> 0x005e }
            java.lang.Long r3 = java.lang.Long.valueOf(r1)     // Catch:{ all -> 0x005e }
            r6.add(r3)     // Catch:{ all -> 0x005e }
            com.snda.youni.modules.a.u r3 = com.snda.youni.modules.a.u.a()     // Catch:{ all -> 0x005e }
            monitor-enter(r3)     // Catch:{ all -> 0x005e }
            com.snda.youni.modules.a.l r1 = com.snda.youni.modules.a.u.a(r1)     // Catch:{ all -> 0x005b }
            monitor-exit(r3)     // Catch:{ all -> 0x005b }
            if (r1 != 0) goto L_0x006e
            com.snda.youni.modules.a.l r1 = new com.snda.youni.modules.a.l     // Catch:{ all -> 0x005e }
            r2 = 1
            r1.<init>(r7, r0, r2)     // Catch:{ all -> 0x005e }
            com.snda.youni.modules.a.u r2 = com.snda.youni.modules.a.u.a()     // Catch:{ IllegalStateException -> 0x0056 }
            monitor-enter(r2)     // Catch:{ IllegalStateException -> 0x0056 }
            com.snda.youni.modules.a.u.a(r1)     // Catch:{ all -> 0x0053 }
            monitor-exit(r2)     // Catch:{ all -> 0x0053 }
            goto L_0x0025
        L_0x0053:
            r1 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x0053 }
            throw r1     // Catch:{ IllegalStateException -> 0x0056 }
        L_0x0056:
            r1 = move-exception
            goto L_0x0025
        L_0x0058:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x0058 }
            throw r1
        L_0x005b:
            r1 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x005b }
            throw r1     // Catch:{ all -> 0x005e }
        L_0x005e:
            r1 = move-exception
            if (r0 == 0) goto L_0x0064
            r0.close()
        L_0x0064:
            com.snda.youni.modules.a.u r0 = com.snda.youni.modules.a.u.a()
            monitor-enter(r0)
            r2 = 0
            com.snda.youni.modules.a.l.m = r2     // Catch:{ all -> 0x0088 }
            monitor-exit(r0)     // Catch:{ all -> 0x0088 }
            throw r1
        L_0x006e:
            r2 = 1
            a(r7, r1, r0, r2)     // Catch:{ all -> 0x005e }
            goto L_0x0025
        L_0x0073:
            if (r0 == 0) goto L_0x0078
            r0.close()
        L_0x0078:
            com.snda.youni.modules.a.u r0 = com.snda.youni.modules.a.u.a()
            monitor-enter(r0)
            r1 = 0
            com.snda.youni.modules.a.l.m = r1     // Catch:{ all -> 0x0085 }
            monitor-exit(r0)     // Catch:{ all -> 0x0085 }
            com.snda.youni.modules.a.u.a(r6)
            goto L_0x000b
        L_0x0085:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x0085 }
            throw r1
        L_0x0088:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x0088 }
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.snda.youni.modules.a.l.b(android.content.Context):void");
    }

    public final synchronized long a() {
        return this.f;
    }

    public final synchronized void b() {
        u.b(this.f);
        this.f = 0;
    }

    public final synchronized h c() {
        return this.g;
    }

    public final synchronized boolean d() {
        return this.f <= 0 ? false : d.a().a(this.f);
    }

    public final synchronized String e() {
        return this.i;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:6:0x000e, code lost:
        r0 = false;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized boolean equals(java.lang.Object r3) {
        /*
            r2 = this;
            monitor-enter(r2)
            com.snda.youni.modules.a.l r3 = (com.snda.youni.modules.a.l) r3     // Catch:{ ClassCastException -> 0x000d, all -> 0x0010 }
            com.snda.youni.modules.b.h r0 = r2.g     // Catch:{ ClassCastException -> 0x000d, all -> 0x0010 }
            com.snda.youni.modules.b.h r1 = r3.g     // Catch:{ ClassCastException -> 0x000d, all -> 0x0010 }
            boolean r0 = r0.equals(r1)     // Catch:{ ClassCastException -> 0x000d, all -> 0x0010 }
        L_0x000b:
            monitor-exit(r2)
            return r0
        L_0x000d:
            r0 = move-exception
            r0 = 0
            goto L_0x000b
        L_0x0010:
            r0 = move-exception
            monitor-exit(r2)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.snda.youni.modules.a.l.equals(java.lang.Object):boolean");
    }

    public final synchronized int hashCode() {
        return this.g.hashCode();
    }

    public final synchronized String toString() {
        Object[] objArr;
        objArr = new Object[2];
        h hVar = this.g;
        ArrayList arrayList = new ArrayList();
        Iterator it = hVar.iterator();
        while (it.hasNext()) {
            String b2 = ((b) it.next()).b();
            if (!TextUtils.isEmpty(b2) && !arrayList.contains(b2)) {
                arrayList.add(b2);
            }
        }
        objArr[0] = TextUtils.join(";", (String[]) arrayList.toArray(new String[arrayList.size()]));
        objArr[1] = Long.valueOf(this.f);
        return String.format("[%s] (tid %d)", objArr);
    }
}
