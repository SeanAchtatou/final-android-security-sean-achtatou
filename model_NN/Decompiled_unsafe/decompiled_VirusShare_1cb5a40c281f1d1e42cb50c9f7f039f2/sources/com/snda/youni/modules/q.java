package com.snda.youni.modules;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.media.RingtoneManager;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import com.snda.a.a.c.a;
import com.snda.youni.C0000R;
import com.snda.youni.YouNi;
import com.snda.youni.a.b;
import com.snda.youni.c.f;
import com.snda.youni.c.j;
import com.snda.youni.c.k;
import com.snda.youni.c.r;
import com.snda.youni.c.t;
import com.snda.youni.e.s;
import com.snda.youni.services.YouniService;

public final class q {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public final YouNi f560a;
    /* access modifiers changed from: private */
    public b b;
    /* access modifiers changed from: private */
    public boolean c;

    public q(YouNi youNi, b bVar) {
        this.f560a = youNi;
        this.b = bVar;
        Context applicationContext = this.f560a.getApplicationContext();
        this.f560a.getApplicationContext().startService(new Intent("com.snda.youni.START_CONTACTS_SERVICE"));
        Intent intent = new Intent(applicationContext, YouniService.class);
        applicationContext.startService(intent);
        this.f560a.bindService(intent, this.f560a.f(), 1);
        if (((TelephonyManager) this.f560a.getSystemService("phone")).getSimState() != 1) {
            TelephonyManager telephonyManager = (TelephonyManager) this.f560a.getSystemService("phone");
            if (telephonyManager.getSimState() == 5) {
                String subscriberId = telephonyManager.getSubscriberId();
                SharedPreferences e = this.f560a.e();
                SharedPreferences.Editor edit = e.edit();
                String string = e.getString("current_sim_id", null);
                if (string == null) {
                    edit.putString("current_sim_id", subscriberId);
                    edit.commit();
                } else if (!string.equals(subscriberId)) {
                    edit.remove("self_num_account");
                    edit.remove("self_pt_account");
                    edit.remove("self_phone_number");
                    edit.putString("current_sim_id", subscriberId);
                    edit.commit();
                }
            }
            SharedPreferences e2 = youNi.e();
            s.f403a = e2.getString("self_phone_number", "");
            s.c = e2.getString("self_num_account", "");
            s.b = e2.getString("self_pt_account", "");
            this.c = false;
            "mobile=" + s.f403a;
            "numaccount=" + s.c;
            "ptaccount=" + s.b;
            int i = 9999;
            try {
                i = youNi.getPackageManager().getPackageInfo(youNi.getPackageName(), 0).versionCode;
            } catch (PackageManager.NameNotFoundException e3) {
                e3.printStackTrace();
            }
            "YouNi version code is: " + i;
            a.a(String.valueOf(i), this.f560a.getApplicationContext(), new af(this));
            if (PreferenceManager.getDefaultSharedPreferences(this.f560a).getBoolean("first_open", true)) {
                Uri uri = Settings.System.DEFAULT_NOTIFICATION_URI;
                String title = RingtoneManager.getRingtone(this.f560a, uri).getTitle(this.f560a);
                SharedPreferences e4 = this.f560a.e();
                e4.edit().putInt("sound_type_name", 1).commit();
                e4.edit().putString("sound_uri_name", uri.toString()).commit();
                e4.edit().putString("sound_title_name", title).commit();
                YouNi youNi2 = this.f560a;
                if (youNi2 != null && !youNi2.isFinishing()) {
                    StringBuilder sb = new StringBuilder(youNi2.getString(C0000R.string.app_name));
                    AlertDialog.Builder builder = new AlertDialog.Builder(youNi2);
                    View inflate = this.f560a.getLayoutInflater().inflate((int) C0000R.layout.dialog_privacy, (ViewGroup) null);
                    AlertDialog show = builder.setTitle(sb.toString()).setView(inflate).setCancelable(false).setPositiveButton(youNi2.getString(C0000R.string.alert_dialog_ok), new z(this)).setNegativeButton(youNi2.getString(C0000R.string.alert_dialog_cancel), new w(this)).setOnKeyListener(new x(this)).show();
                    inflate.findViewById(C0000R.id.link).setOnClickListener(new y(this));
                    ((CheckBox) inflate.findViewById(C0000R.id.check)).setOnCheckedChangeListener(new aa(this, show));
                }
            }
            if (!TextUtils.isEmpty(s.f403a)) {
                String string2 = PreferenceManager.getDefaultSharedPreferences(youNi).getString("nick_name", null);
                "local nickname====" + string2;
                if (string2 == null || string2.length() <= 0) {
                    e2.getString("self_product_id", "");
                    t.b(new j(null), new u(this), this.f560a);
                }
                SharedPreferences defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this.f560a.getApplicationContext());
                if (defaultSharedPreferences.getBoolean("inner_test", false) && !defaultSharedPreferences.getBoolean("isActivitied", false)) {
                    t.b(new j(null), new v(this), this.f560a);
                }
            }
        }
    }

    static /* synthetic */ void a(q qVar, k kVar) {
        String c2 = kVar.c().c();
        String d = kVar.c().d();
        String e = kVar.c().e();
        "init nickName is: " + c2;
        "init signature is: " + d;
        "init ImgUrl is: " + e;
        com.snda.youni.modules.settings.s g = qVar.f560a.g();
        g.a(c2);
        g.b(d);
        f fVar = new f();
        if (!TextUtils.isEmpty(e)) {
            t.a(e, fVar, new b(qVar), null, qVar.f560a);
        }
    }

    static /* synthetic */ void a(q qVar, r rVar) {
        String c2 = rVar.c();
        String d = rVar.d();
        String e = rVar.e();
        "init nickName is: " + c2;
        "init signature is: " + d;
        "init ImgUrl is: " + e;
        com.snda.youni.modules.settings.s g = qVar.f560a.g();
        g.a(c2);
        g.b(d);
        f fVar = new f();
        if (!TextUtils.isEmpty(e)) {
            t.a(e, fVar, new b(qVar), null, qVar.f560a);
        }
    }

    static /* synthetic */ void b(q qVar) {
        "" + TextUtils.isEmpty(s.f403a);
        "" + (!PreferenceManager.getDefaultSharedPreferences(qVar.f560a).getBoolean("first_open", true));
        "" + qVar.c;
        if (TextUtils.isEmpty(s.f403a) && !PreferenceManager.getDefaultSharedPreferences(qVar.f560a).getBoolean("first_open", true) && qVar.c) {
            a.a(qVar.f560a, new k(qVar));
        } else if (!TextUtils.isEmpty(s.f403a)) {
            qVar.b.b();
        }
    }

    static /* synthetic */ void d(q qVar) {
        Context applicationContext = qVar.f560a.getApplicationContext();
        Intent intent = new Intent(applicationContext, YouniService.class);
        applicationContext.startService(intent);
        qVar.f560a.bindService(intent, qVar.f560a.f(), 1);
        qVar.f560a.sendBroadcast(new Intent("com.snda.youni.action.REGISTER_SUCCEEDED"));
    }

    public final void a() {
        SharedPreferences.Editor edit = PreferenceManager.getDefaultSharedPreferences(this.f560a).edit();
        edit.putBoolean("isActivitied", true);
        edit.commit();
    }
}
