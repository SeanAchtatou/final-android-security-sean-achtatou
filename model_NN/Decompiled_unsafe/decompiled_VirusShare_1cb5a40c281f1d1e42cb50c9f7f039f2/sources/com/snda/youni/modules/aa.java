package com.snda.youni.modules;

import android.app.AlertDialog;
import android.widget.CompoundButton;

final class aa implements CompoundButton.OnCheckedChangeListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ AlertDialog f509a;
    private /* synthetic */ q b;

    aa(q qVar, AlertDialog alertDialog) {
        this.b = qVar;
        this.f509a = alertDialog;
    }

    public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
        this.f509a.getButton(-1).setEnabled(z);
    }
}
