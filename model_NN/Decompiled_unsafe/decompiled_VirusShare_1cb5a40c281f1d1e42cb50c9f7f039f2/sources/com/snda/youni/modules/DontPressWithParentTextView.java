package com.snda.youni.modules;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;

public class DontPressWithParentTextView extends TextView {
    public DontPressWithParentTextView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public void setPressed(boolean z) {
        if (!z || !((View) getParent()).isPressed()) {
            super.setPressed(z);
        }
    }
}
