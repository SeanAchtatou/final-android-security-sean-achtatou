package com.snda.youni.modules;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.snda.youni.C0000R;
import com.snda.youni.a;
import java.util.Timer;
import java.util.TimerTask;

public class WarningTipView extends RelativeLayout {

    /* renamed from: a  reason: collision with root package name */
    private TextView f488a;
    private ImageButton b;
    private int c;
    private Timer d;
    private TimerTask e;
    /* access modifiers changed from: private */
    public Handler f = new ae(this);

    public WarningTipView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a(context, attributeSet);
    }

    public WarningTipView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        a(context, attributeSet);
    }

    private void a(Context context, AttributeSet attributeSet) {
        View inflate = ((LayoutInflater) context.getSystemService("layout_inflater")).inflate((int) C0000R.layout.model_warning_tip, this);
        this.f488a = (TextView) inflate.findViewById(C0000R.id.text);
        this.b = (ImageButton) inflate.findViewById(C0000R.id.btn);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, a.c);
        String string = obtainStyledAttributes.getString(1);
        String string2 = obtainStyledAttributes.getString(0);
        obtainStyledAttributes.recycle();
        this.f488a.setText(string);
        if (string2 == null) {
            this.c = 0;
        } else if ("long".equals(string2)) {
            this.c = 10000;
        } else if ("short".equals(string2)) {
            this.c = 5000;
        } else {
            this.c = Integer.valueOf(string2).intValue();
        }
        this.b.setOnClickListener(new ac(this));
    }

    static /* synthetic */ void a(WarningTipView warningTipView) {
        if (warningTipView.getVisibility() == 0) {
            Animation loadAnimation = AnimationUtils.loadAnimation(warningTipView.getContext(), C0000R.anim.warning_tip);
            loadAnimation.setAnimationListener(new ab(warningTipView));
            warningTipView.startAnimation(loadAnimation);
        }
    }

    public final WarningTipView a() {
        setVisibility(0);
        if (this.c != 0) {
            if (this.d != null) {
                this.d.cancel();
            }
            this.d = new Timer();
            if (this.e != null) {
                this.e.cancel();
            }
            this.e = new ad(this);
            this.d.schedule(this.e, (long) this.c);
        }
        return this;
    }

    public final WarningTipView a(int i) {
        this.c = i;
        return this;
    }

    public final WarningTipView a(String str) {
        this.f488a.setText(str);
        return this;
    }
}
