package com.snda.youni.modules.a;

import android.content.AsyncQueryHandler;
import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;

final class m extends AsyncQueryHandler {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ e f498a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public m(e eVar, ContentResolver contentResolver) {
        super(contentResolver);
        this.f498a = eVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.snda.youni.modules.a.l.a(android.content.Context, int):void
     arg types: [com.snda.youni.YouNi, int]
     candidates:
      com.snda.youni.modules.a.l.a(android.content.Context, long):com.snda.youni.modules.a.l
      com.snda.youni.modules.a.l.a(android.content.Context, android.database.Cursor):com.snda.youni.modules.a.l
      com.snda.youni.modules.a.l.a(android.content.Context, int):void */
    /* access modifiers changed from: protected */
    public final void onDeleteComplete(int i, Object obj, int i2) {
        "onDelete: delete " + i2 + " messages";
        if (this.f498a.j != null) {
            this.f498a.j.dismiss();
        }
        this.f498a.h.a((d) null);
        l.a((Context) this.f498a.l, ((Integer) obj).intValue());
    }

    /* access modifiers changed from: protected */
    public final void onQueryComplete(int i, Object obj, Cursor cursor) {
        switch (i) {
            case 0:
                e.a(this.f498a, new q(this.f498a, ((Long) obj).longValue(), this.f498a.n), cursor != null && cursor.getCount() > 0);
                return;
            default:
                return;
        }
    }
}
