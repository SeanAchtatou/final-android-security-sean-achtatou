package com.snda.youni.modules;

import android.content.DialogInterface;
import android.content.SharedPreferences;

final class z implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ q f584a;

    z(q qVar) {
        this.f584a = qVar;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        SharedPreferences.Editor edit = this.f584a.f560a.e().edit();
        edit.putBoolean("first_open", false);
        edit.commit();
        q.b(this.f584a);
    }
}
