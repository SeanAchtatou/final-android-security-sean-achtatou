package com.snda.youni.modules.popup;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.ScrollView;
import android.widget.TextView;
import com.snda.a.a.a.a;

public class PopupScrollView extends ScrollView {
    public PopupScrollView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        a.a("PopupScrollView", "onLayout changed" + z + " l = " + i + ", t = " + i2 + ", r = " + i3 + ", b = " + i4);
        super.onLayout(z, i, i2, i3, i4);
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        int i3;
        TextView textView = (TextView) getChildAt(0);
        measureChildWithMargins(textView, i, 0, i2, 0);
        int max = Math.max(0, textView.getMeasuredWidth());
        int max2 = Math.max(0, textView.getMeasuredHeight());
        int paddingLeft = max + getPaddingLeft() + getPaddingRight();
        int max3 = Math.max(max2 + getPaddingTop() + getPaddingBottom(), getSuggestedMinimumHeight());
        int max4 = Math.max(paddingLeft, getSuggestedMinimumWidth());
        Drawable foreground = getForeground();
        if (foreground != null) {
            Math.max(max3, foreground.getMinimumHeight());
            max4 = Math.max(max4, foreground.getMinimumWidth());
        }
        int measuredHeight = textView.getMeasuredHeight();
        int lineCount = textView.getLineCount();
        a.a("PopupScrollView", "childMeasuredWidth = " + textView.getMeasuredWidth() + ", childMeasuredHeight = " + textView.getMeasuredHeight() + ", lineCount = " + textView.getLineCount());
        if (textView.getLineCount() > 5) {
            i3 = (measuredHeight * 5) / lineCount;
            a.a("PopupScrollView", "childMeasuredHeight = " + i3);
        } else {
            i3 = measuredHeight;
        }
        setMeasuredDimension(resolveSize(max4, i), resolveSize(i3, i2));
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        a.a("PopupScrollView", "w = " + i + ", h = " + i2 + ", oldw = " + i3 + ", oldh = " + i4);
        super.onSizeChanged(i, i2, i3, i4);
    }
}
