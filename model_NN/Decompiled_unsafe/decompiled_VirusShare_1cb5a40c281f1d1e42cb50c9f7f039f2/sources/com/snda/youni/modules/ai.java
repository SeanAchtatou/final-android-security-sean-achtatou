package com.snda.youni.modules;

import android.content.AsyncQueryHandler;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.widget.TextView;
import com.snda.youni.C0000R;
import com.snda.youni.am;
import com.snda.youni.b.s;
import com.snda.youni.providers.n;
import java.util.ArrayList;
import java.util.List;

final class ai extends AsyncQueryHandler {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ aj f517a;
    /* access modifiers changed from: private */
    public String b;
    /* access modifiers changed from: private */
    public String c;
    /* access modifiers changed from: private */
    public List d = new ArrayList();
    /* access modifiers changed from: private */
    public List e = new ArrayList();
    private s f = new p(this);

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ai(aj ajVar, ContentResolver contentResolver) {
        super(contentResolver);
        this.f517a = ajVar;
    }

    /* access modifiers changed from: protected */
    public final void onDeleteComplete(int i, Object obj, int i2) {
        "onDelete: delete " + i2 + " messages";
        if (this.f517a.o != null) {
            this.f517a.o.dismiss();
        }
    }

    /* access modifiers changed from: protected */
    public final void onQueryComplete(int i, Object obj, Cursor cursor) {
        "token=" + i + ", cursor.size is: " + cursor.getCount();
        if (!(i == 2 || i == 7)) {
            this.f517a.k.changeCursor(cursor);
        }
        switch (i) {
            case 1:
                this.f517a.k.a(false);
                if (cursor == null || cursor.getCount() <= 0) {
                    if (!PreferenceManager.getDefaultSharedPreferences(this.f517a.l).getBoolean("contacts_synced", false)) {
                        return;
                    }
                    ((TextView) this.f517a.i).setText((int) C0000R.string.hint_no_contacts);
                    break;
                } else {
                    this.f517a.i.setVisibility(8);
                    return;
                }
                break;
            case 2:
                this.f517a.k.a(true);
                if (cursor == null || cursor.getCount() <= 0) {
                    if (TextUtils.isEmpty(this.f517a.g.getText().toString())) {
                        ((TextView) this.f517a.i).setText((int) C0000R.string.hint_contacts_loading);
                    } else {
                        ((TextView) this.f517a.i).setText((int) C0000R.string.hint_no_contacts);
                    }
                    this.f517a.i.setVisibility(0);
                } else {
                    this.f517a.i.setVisibility(8);
                }
                am.a(this.f517a.l.c(), false);
                this.f517a.k.changeCursor(cursor);
                return;
            case 3:
                this.f517a.k.a(false);
                if (cursor == null || cursor.getCount() <= 0) {
                    ((TextView) this.f517a.i).setText((int) C0000R.string.hint_no_contacts);
                    break;
                } else {
                    this.f517a.i.setVisibility(8);
                    return;
                }
                break;
            case 4:
            case 5:
            case 6:
            default:
                return;
            case 7:
                if (this.f517a.c.getCheckedRadioButtonId() != C0000R.id.btn_youni) {
                    this.f517a.onCheckedChanged(null, this.f517a.c.getCheckedRadioButtonId());
                    return;
                }
                this.f517a.k.a();
                this.f517a.k.changeCursor(cursor);
                if (cursor == null || cursor.getCount() <= 0) {
                    if (TextUtils.isEmpty(this.f517a.g.getText().toString())) {
                        ((TextView) this.f517a.i).setText((int) C0000R.string.hint_contacts_loading);
                        break;
                    }
                    ((TextView) this.f517a.i).setText((int) C0000R.string.hint_no_contacts);
                    break;
                } else {
                    this.f517a.i.setVisibility(8);
                    return;
                }
                break;
        }
        this.f517a.i.setVisibility(0);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    /* access modifiers changed from: protected */
    public final void onUpdateComplete(int i, Object obj, int i2) {
        "onUpdateComplete, token=" + i + ", result=" + i2;
        switch (i) {
            case 5:
                if (this.f517a.c.getCheckedRadioButtonId() == C0000R.id.btn_youni) {
                    ContentValues contentValues = new ContentValues();
                    contentValues.put("expand_data1", (Integer) 0);
                    startUpdate(6, null, n.f597a, contentValues, this.c, null);
                    return;
                }
                return;
            case 6:
                if (this.f517a.c.getCheckedRadioButtonId() == C0000R.id.btn_youni) {
                    String obj2 = this.f517a.g.getText().toString();
                    startQuery(7, null, n.f597a, t.f578a, !TextUtils.isEmpty(obj2) ? "contact_type=1" + " AND (search_name LIKE '" + obj2.toString() + "%'" + " OR " + "pinyin_name LIKE '" + obj2.toString() + "%'" + " OR " + "phone_number LIKE '" + obj2.toString() + "%'" + " OR " + "display_name LIKE '" + obj2.toString() + "%'" + ")" : "contact_type=1", null, null);
                    return;
                }
                return;
            default:
                return;
        }
    }

    public final void startQuery(int i, Object obj, Uri uri, String[] strArr, String str, String[] strArr2, String str2) {
        if (i != 7) {
            cancelOperation(7);
        }
        cancelOperation(i);
        ((TextView) this.f517a.i).setText((int) C0000R.string.hint_contacts_loading);
        this.f517a.i.setVisibility(0);
        com.snda.youni.b.ai c2 = this.f517a.l.c();
        String str3 = ((i == 7 || i == 2) && c2 != null && c2.b()) ? "expand_data2 DESC, expand_data1 DESC, pinyin_name ASC" : "expand_data2 DESC, pinyin_name ASC";
        "startQuery, token=" + i + ", orderBy=" + str3;
        super.startQuery(i, obj, uri, strArr, str, strArr2, str3);
    }
}
