package com.snda.youni;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Intent;
import com.snda.youni.b.s;
import com.snda.youni.e.q;
import com.snda.youni.providers.n;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

final class ao implements s {
    ao() {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public final void a(HashMap hashMap) {
        "IMultiStatusInterface,onStatus, map.count=" + hashMap.size();
        "statusMap=" + hashMap;
        if (hashMap.size() > 0) {
            Iterator it = hashMap.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry entry = (Map.Entry) it.next();
                String str = (String) entry.getKey();
                "key=" + ((String) entry.getKey()) + ", value=" + ((String) entry.getValue()) + "cache.get=" + ((String) am.f313a.get(str));
                if (!((String) entry.getValue()).equals(am.f313a.get(str))) {
                    "sStatusCache.put(" + str + ", value=" + ((String) entry.getValue());
                    am.f313a.put(str, entry.getValue());
                } else {
                    it.remove();
                }
                "after Cached, cache.get=" + ((String) am.f313a.get(str));
            }
            "IMultiStatusInterface,onStatus, sStatusCache.count=" + am.f313a.size();
            if (hashMap.size() > 0) {
                StringBuilder sb = new StringBuilder();
                sb.append("sid IN (");
                StringBuilder sb2 = new StringBuilder();
                sb2.append("sid IN (");
                boolean z = true;
                boolean z2 = true;
                for (Map.Entry entry2 : hashMap.entrySet()) {
                    String callerIDMinMatch = q.toCallerIDMinMatch((String) entry2.getKey());
                    if ("1".equals(entry2.getValue())) {
                        if (z2) {
                            sb.append('\'').append(callerIDMinMatch).append('\'');
                            z2 = false;
                        } else {
                            sb.append(',').append('\'').append(callerIDMinMatch).append('\'');
                        }
                    } else if ("0".equals(entry2.getValue())) {
                        if (z) {
                            sb2.append('\'').append(callerIDMinMatch).append('\'');
                            z = false;
                        } else {
                            sb2.append(',').append('\'').append(callerIDMinMatch).append('\'');
                        }
                    }
                }
                sb.append(')');
                sb2.append(')');
                String sb3 = sb.toString();
                String sb4 = sb2.toString();
                "mOnlinePhoneSelection=" + sb3;
                "mOfflinePhoneSelection=" + sb4;
                ContentValues contentValues = new ContentValues();
                contentValues.put("expand_data1", (Integer) 1);
                ContentResolver contentResolver = AppContext.a().getContentResolver();
                contentResolver.update(n.f597a, contentValues, sb3, null);
                contentValues.put("expand_data1", (Integer) 0);
                contentResolver.update(n.f597a, contentValues, sb4, null);
                long unused = am.b = System.currentTimeMillis();
                Intent intent = new Intent("com.snda.youni.ACTION_STATUS_LOADED");
                intent.setPackage(AppContext.a().getPackageName());
                AppContext.a().sendBroadcast(intent);
            }
        }
    }
}
