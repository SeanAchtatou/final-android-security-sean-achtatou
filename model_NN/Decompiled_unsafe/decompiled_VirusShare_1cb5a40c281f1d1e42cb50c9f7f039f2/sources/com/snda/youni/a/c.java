package com.snda.youni.a;

import android.os.AsyncTask;
import com.snda.youni.e.s;
import java.io.File;
import java.io.IOException;

final class c extends AsyncTask {

    /* renamed from: a  reason: collision with root package name */
    private File f173a;
    private /* synthetic */ l b;

    /* synthetic */ c(l lVar) {
        this(lVar, (byte) 0);
    }

    private c(l lVar, byte b2) {
        this.b = lVar;
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public Integer doInBackground(m... mVarArr) {
        try {
            this.f173a = this.b.b.a(mVarArr[0]);
            return 0;
        } catch (a e) {
            e.printStackTrace();
            return 1;
        } catch (IOException e2) {
            e2.printStackTrace();
            return 2;
        }
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void onPostExecute(Object obj) {
        Integer num = (Integer) obj;
        "Download finish: " + String.valueOf(num);
        s.p = false;
        "onPostExecute updating: " + String.valueOf(s.p);
        if (this.b.f179a != null) {
            this.b.f179a.a(num.intValue());
        }
    }

    /* access modifiers changed from: protected */
    public final void onPreExecute() {
        s.p = true;
        "onPreExecute updating: " + String.valueOf(s.p);
    }
}
