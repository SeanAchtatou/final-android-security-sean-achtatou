package com.snda.youni.a;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.widget.RemoteViews;
import android.widget.Toast;
import com.snda.youni.C0000R;
import com.snda.youni.YouNi;
import com.snda.youni.activities.UpdateRefActivity;
import com.snda.youni.activities.co;
import java.io.File;
import java.util.Timer;
import java.util.TimerTask;

public final class b implements j, co {
    private static RemoteViews d;
    private static PendingIntent e;
    private static NotificationManager f;
    private static Notification g;
    private static TimerTask h;
    private static Timer i;

    /* renamed from: a  reason: collision with root package name */
    private i f172a;
    private boolean b = false;
    private boolean c = true;
    /* access modifiers changed from: private */
    public int j;
    private final YouNi k;
    private final Context l;

    public b(Context context) {
        this.k = (YouNi) context;
        this.l = context.getApplicationContext();
        this.f172a = new l(context);
        this.f172a.a(this);
        e eVar = new e(this.l);
        if (eVar.f174a.f175a < this.k.e().getInt("update_versioncode", 0)) {
            this.k.c.sendEmptyMessage(0);
            if (this.k.e().getBoolean("update_notification", true)) {
                this.k.c.sendEmptyMessage(2);
            }
        }
    }

    private void b(m mVar) {
        Context context = this.l;
        UpdateRefActivity.a(this);
        Intent intent = new Intent(context, UpdateRefActivity.class);
        intent.addFlags(268435456);
        intent.putExtra("update_info", mVar);
        context.startActivity(intent);
    }

    private static void c() {
        if (h != null) {
            h.cancel();
        }
        if (i != null) {
            i.cancel();
            i.purge();
        }
        if (f != null) {
            f.cancel(0);
        }
    }

    static /* synthetic */ void c(int i2) {
        d.setProgressBar(C0000R.id.update_pb, 100, i2, false);
        d.setTextViewText(C0000R.id.update_tv, i2 + "%");
        g.contentView = d;
        g.contentIntent = e;
        f.notify(0, g);
    }

    public final void a() {
        if (this.l != null) {
            this.b = true;
            this.c = false;
            e eVar = new e(this.l);
            if (eVar.f174a.f175a < this.k.e().getInt("update_versioncode", 0)) {
                m mVar = new m();
                mVar.b = false;
                mVar.f180a = true;
                mVar.c = this.k.e().getString("update_downloaduir", "");
                mVar.f = this.k.e().getString("update_description", "");
                mVar.e = "" + this.k.e().getInt("update_versioncode", 0);
                mVar.d = this.k.e().getString("update_versionname", "");
                b(mVar);
                return;
            }
            Toast.makeText(this.l, this.l.getString(C0000R.string.update_waiting), 0).show();
            this.f172a.a(this.l);
        }
    }

    public final void a(int i2) {
        if (i2 == 0) {
            Context context = this.l;
            Intent intent = new Intent("android.intent.action.VIEW");
            intent.setDataAndType(Uri.fromFile(new File(com.snda.youni.e.b.a())), "application/vnd.android.package-archive");
            intent.setFlags(268435456);
            context.startActivity(intent);
            c();
            this.f172a.a((j) null);
        }
        Context context2 = this.l;
        if (i2 == 1) {
            Toast.makeText(context2, context2.getString(C0000R.string.update_network_error), 0).show();
            c();
        }
    }

    public final void a(int i2, m mVar) {
        "rst: " + String.valueOf(i2) + "   info: " + (mVar == null ? "null" : String.valueOf(mVar.f180a));
        Context context = this.l;
        if (i2 == 0 && mVar.f180a) {
            if (Integer.valueOf(mVar.e).intValue() > this.k.e().getInt("update_versioncode", 0)) {
                this.k.c.sendEmptyMessage(2);
                SharedPreferences.Editor edit = this.k.e().edit();
                edit.putInt("update_versioncode", Integer.valueOf(mVar.e).intValue());
                edit.putString("update_versionname", mVar.d);
                edit.putString("update_downloaduir", mVar.c);
                edit.putString("update_description", mVar.f);
                edit.commit();
                this.k.c.sendEmptyMessage(0);
            }
            if (!this.c || mVar.b) {
                b(mVar);
            }
        } else if (!this.b) {
            c();
        } else if (i2 == 0) {
            Toast.makeText(context, context.getString(C0000R.string.update_no), 0).show();
            c();
        } else if (i2 == 3) {
            Toast.makeText(context, context.getString(C0000R.string.update_running), 0).show();
        } else if (i2 == 1) {
            Toast.makeText(context, context.getString(C0000R.string.update_network_error), 0).show();
            c();
        }
    }

    public final void a(Activity activity, m mVar) {
        this.f172a.a((j) null);
        if (mVar.b) {
            activity.finish();
            this.k.finish();
        }
    }

    public final void a(m mVar) {
        this.f172a.a(this);
        this.f172a.a(mVar);
        Context context = this.l;
        f = (NotificationManager) context.getSystemService("notification");
        c();
        d = new RemoteViews(context.getPackageName(), (int) C0000R.layout.update_remote);
        e = PendingIntent.getService(context, 0, new Intent(context, Notification.class), 134217728);
        Notification notification = new Notification();
        g = notification;
        notification.icon = C0000R.drawable.icn_youni;
        d.setImageViewResource(C0000R.id.update_image, C0000R.drawable.icn_youni);
        if (i != null) {
            i.cancel();
        }
        i = new Timer();
        h = new k(this);
        i.schedule(h, 0, 2000);
    }

    public final void b() {
        this.f172a.a(this.l);
    }

    public final void b(int i2) {
        this.j = i2;
    }
}
