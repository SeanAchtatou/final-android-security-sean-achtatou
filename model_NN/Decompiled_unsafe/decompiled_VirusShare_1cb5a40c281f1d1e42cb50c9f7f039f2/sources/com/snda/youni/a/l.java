package com.snda.youni.a;

import android.content.Context;
import android.os.AsyncTask;
import com.snda.youni.c.t;
import com.snda.youni.e.s;
import com.snda.youni.f.a;
import java.lang.ref.WeakReference;

public final class l implements d, i {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public j f179a;
    /* access modifiers changed from: private */
    public h b;
    private c c;
    private WeakReference d;
    private a e = new g(this);

    public l(Context context) {
        this.d = new WeakReference(context);
        this.b = new h();
        this.b.a(this);
    }

    public final void a(int i) {
        if (this.f179a != null) {
            this.f179a.b(i);
        }
    }

    public final void a(Context context) {
        "checkUpdate updating: " + String.valueOf(s.p);
        if (s.p.booleanValue()) {
            this.f179a.a(3, null);
        } else {
            t.a(new com.snda.youni.c.l(context), this.e, context);
        }
    }

    public final void a(j jVar) {
        this.f179a = jVar;
    }

    public final void a(m mVar) {
        "downloadApk updating: " + String.valueOf(s.p);
        if (!s.p.booleanValue()) {
            if (this.c == null || this.c.getStatus() == AsyncTask.Status.FINISHED) {
                this.c = new c(this);
            }
            if (this.c.getStatus() == AsyncTask.Status.PENDING) {
                this.c.execute(mVar);
            }
        }
    }
}
