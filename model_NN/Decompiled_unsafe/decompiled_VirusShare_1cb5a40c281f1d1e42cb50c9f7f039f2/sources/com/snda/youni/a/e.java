package com.snda.youni.a;

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import java.io.Serializable;

public final class e implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    public f f174a;
    public String b;
    public String c;
    public String d;
    private String e;

    public e() {
    }

    public e(Context context) {
        String deviceId;
        this.f174a = a(context);
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
        if (telephonyManager == null) {
            deviceId = "";
        } else {
            deviceId = telephonyManager.getDeviceId();
            if (TextUtils.isEmpty(deviceId)) {
                deviceId = "";
            }
        }
        this.d = deviceId;
        this.c = Build.MODEL;
        this.b = Build.VERSION.RELEASE;
        this.e = context.getPackageName();
    }

    private f a(Context context) {
        f fVar = new f(this);
        try {
            fVar.b = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
            fVar.f175a = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionCode;
        } catch (PackageManager.NameNotFoundException e2) {
            fVar.b = "unknown";
            fVar.f175a = -1;
        }
        return fVar;
    }
}
