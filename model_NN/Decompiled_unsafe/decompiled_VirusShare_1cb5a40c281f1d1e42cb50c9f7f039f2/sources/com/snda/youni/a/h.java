package com.snda.youni.a;

import com.snda.youni.e.b;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;

public final class h {

    /* renamed from: a  reason: collision with root package name */
    private d f177a;

    private File a(String str) {
        "url: " + str;
        try {
            HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(str).openConnection();
            httpURLConnection.setConnectTimeout(20000);
            httpURLConnection.setReadTimeout(20000);
            if (200 == httpURLConnection.getResponseCode()) {
                int contentLength = httpURLConnection.getContentLength();
                if (contentLength <= 0) {
                    throw new a();
                }
                InputStream inputStream = httpURLConnection.getInputStream();
                File file = new File(b.a());
                if (!file.exists()) {
                    file.createNewFile();
                }
                try {
                    Runtime.getRuntime().exec("chmod 777 " + b.a());
                } catch (IOException e) {
                    e.printStackTrace();
                }
                FileOutputStream fileOutputStream = new FileOutputStream(file);
                byte[] bArr = new byte[65535];
                int i = 0;
                while (true) {
                    int read = inputStream.read(bArr);
                    if (read == -1) {
                        break;
                    }
                    fileOutputStream.write(bArr, 0, read);
                    i += read;
                    if (this.f177a != null) {
                        this.f177a.a((i * 100) / contentLength);
                    }
                }
                inputStream.close();
                fileOutputStream.close();
                httpURLConnection.disconnect();
                if (contentLength != 0 && i == contentLength) {
                    return file;
                }
                throw new IOException();
            }
            throw new a();
        } catch (MalformedURLException e2) {
            e2.printStackTrace();
            throw new a();
        } catch (SocketTimeoutException e3) {
            e3.printStackTrace();
            throw new a();
        }
    }

    public final File a(m mVar) {
        return a(mVar.c);
    }

    public final void a(d dVar) {
        this.f177a = dVar;
    }
}
