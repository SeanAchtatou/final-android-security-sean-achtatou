package com.snda.youni.mms.ui;

import android.content.Intent;
import android.view.View;
import com.snda.youni.attachment.ShowAttachment;

final class aj implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ a f454a;
    private /* synthetic */ MessageListItem b;

    aj(MessageListItem messageListItem, a aVar) {
        this.b = messageListItem;
        this.f454a = aVar;
    }

    public final void onClick(View view) {
        Intent intent = new Intent(this.b.s, ShowAttachment.class);
        intent.putExtra("short_url", this.f454a.t);
        intent.putExtra("filename", this.f454a.o);
        this.b.s.startActivity(intent);
    }
}
