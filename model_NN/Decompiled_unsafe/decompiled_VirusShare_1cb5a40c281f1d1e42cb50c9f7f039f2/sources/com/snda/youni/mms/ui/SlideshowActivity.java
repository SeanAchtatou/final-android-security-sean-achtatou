package com.snda.youni.mms.ui;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.widget.MediaController;
import com.sd.a.a.a.b;
import com.sd.android.mms.a.a;
import com.sd.android.mms.b.a.g;
import com.snda.youni.C0000R;
import org.b.a.a.o;
import org.b.a.b.c;
import org.b.a.b.d;
import org.b.a.b.e;

public class SlideshowActivity extends Activity implements d {

    /* renamed from: a  reason: collision with root package name */
    private MediaController f443a;
    /* access modifiers changed from: private */
    public g b;
    private Handler c;
    /* access modifiers changed from: private */
    public o d;
    private SlideView e;
    /* access modifiers changed from: private */
    public SlideshowPresenter f;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.widget.MediaController.<init>(android.content.Context, boolean):void}
     arg types: [com.snda.youni.mms.ui.SlideshowActivity, int]
     candidates:
      ClspMth{android.widget.MediaController.<init>(android.content.Context, android.util.AttributeSet):void}
      ClspMth{android.widget.MediaController.<init>(android.content.Context, boolean):void} */
    static /* synthetic */ void b(SlideshowActivity slideshowActivity) {
        slideshowActivity.f443a = new MediaController((Context) slideshowActivity, false);
        slideshowActivity.f443a.setMediaPlayer(new b(slideshowActivity, slideshowActivity.b));
        slideshowActivity.f443a.setPrevNextListeners(new u(slideshowActivity), new g(slideshowActivity));
        slideshowActivity.f443a.setAnchorView(slideshowActivity.findViewById(C0000R.id.slide_view));
    }

    public final void a(c cVar) {
        this.c.post(new n(this, cVar));
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.c = new Handler();
        requestWindowFeature(1);
        getWindow().setFormat(-3);
        setContentView((int) C0000R.layout.slideshow);
        try {
            a a2 = a.a(this, getIntent().getData());
            this.e = (SlideView) findViewById(C0000R.id.slide_view);
            this.f = (SlideshowPresenter) ab.a("SlideshowPresenter", this, this.e, a2);
            this.c.post(new o(this, a2));
        } catch (b e2) {
            finish();
        }
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        switch (i) {
            case 4:
            case 19:
            case 20:
            case 21:
            case 22:
            case 24:
            case 25:
            case 82:
                break;
            default:
                if (!(this.b == null || this.f443a == null)) {
                    this.f443a.show();
                    break;
                }
        }
        return super.onKeyDown(i, keyEvent);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        if (this.d != null) {
            ((e) this.d).b("SimlDocumentEnd", this, false);
        }
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        if (this.b == null) {
            return;
        }
        if (isFinishing()) {
            this.b.h();
        } else {
            this.b.i();
        }
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (this.b == null || this.f443a == null) {
            return false;
        }
        this.f443a.show();
        return false;
    }
}
