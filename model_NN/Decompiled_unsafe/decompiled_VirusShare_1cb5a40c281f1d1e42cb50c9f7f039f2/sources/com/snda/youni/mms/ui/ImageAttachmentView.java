package com.snda.youni.mms.ui;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.snda.youni.C0000R;

public class ImageAttachmentView extends LinearLayout implements ao {

    /* renamed from: a  reason: collision with root package name */
    private ImageView f437a;

    public ImageAttachmentView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public final void a() {
    }

    public final void a(int i) {
    }

    public final void a(Uri uri) {
    }

    public final void a(Uri uri, String str) {
    }

    public final void a(String str) {
    }

    public final void a(String str, Bitmap bitmap) {
        this.f437a.setImageBitmap(bitmap == null ? BitmapFactory.decodeResource(getResources(), C0000R.drawable.ic_missing_thumbnail_picture) : bitmap);
    }

    public final void a(boolean z) {
    }

    public final void b() {
    }

    public final void b(int i) {
    }

    public final void b(boolean z) {
    }

    public final void c() {
    }

    public final void c(boolean z) {
    }

    public final void d() {
    }

    public final void e() {
    }

    public final void f() {
    }

    public final void g() {
        this.f437a.setImageDrawable(null);
    }

    /* access modifiers changed from: protected */
    public void onFinishInflate() {
        this.f437a = (ImageView) findViewById(C0000R.id.image_content);
    }
}
