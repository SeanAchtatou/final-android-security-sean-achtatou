package com.snda.youni.mms.ui;

import android.database.Cursor;

public final class au {

    /* renamed from: a  reason: collision with root package name */
    public long f463a;
    public String b;
    public String c = "";
    private /* synthetic */ k d;

    public au(k kVar, long j, String str, a aVar) {
        this.d = kVar;
        this.f463a = j;
        this.b = str;
        if ("sms".equals(str) && aVar.n == -1) {
            this.c = aVar.g;
        }
    }

    public au(k kVar, Cursor cursor) {
        this.d = kVar;
        if (!cursor.isAfterLast() && !cursor.isBeforeFirst()) {
            this.f463a = cursor.getLong(kVar.g.b);
            this.b = cursor.getString(kVar.g.f462a);
            a a2 = kVar.a(this.b, this.f463a, cursor);
            if (a2 != null && "sms".equals(this.b) && a2.c() == 0) {
                this.c = a2.g;
            }
        }
    }

    public final boolean equals(Object obj) {
        if (!(obj instanceof au)) {
            return false;
        }
        au auVar = (au) obj;
        return this.f463a == auVar.f463a && this.b.equals(auVar.b);
    }
}
