package com.snda.youni.mms.ui;

import android.widget.CompoundButton;

final class q implements CompoundButton.OnCheckedChangeListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ long f476a;
    private /* synthetic */ String b;
    private /* synthetic */ a c;
    private /* synthetic */ k d;

    q(k kVar, long j, String str, a aVar) {
        this.d = kVar;
        this.f476a = j;
        this.b = str;
        this.c = aVar;
    }

    public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
        "On check " + z + ":" + this.f476a;
        if (this.d.o) {
            if (z) {
                this.d.q.remove(new au(this.d, this.f476a, this.b, this.c));
            } else {
                this.d.q.add(new au(this.d, this.f476a, this.b, this.c));
            }
        } else if (z) {
            this.d.p.add(new au(this.d, this.f476a, this.b, this.c));
        } else {
            this.d.p.remove(new au(this.d, this.f476a, this.b, this.c));
        }
        "ljd number of list is " + this.d.p.size();
    }
}
