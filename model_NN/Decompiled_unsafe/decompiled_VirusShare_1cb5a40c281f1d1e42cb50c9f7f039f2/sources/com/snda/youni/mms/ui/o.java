package com.snda.youni.mms.ui;

import com.sd.android.mms.a.a;
import com.sd.android.mms.a.m;
import com.sd.android.mms.b.a.g;
import org.b.a.b.e;

final class o implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ a f474a;
    private /* synthetic */ SlideshowActivity b;

    o(SlideshowActivity slideshowActivity, a aVar) {
        this.b = slideshowActivity;
        this.f474a = aVar;
    }

    public final void run() {
        g unused = this.b.b = g.a();
        SlideshowActivity.b(this.b);
        org.b.a.a.o unused2 = this.b.d = m.a(this.f474a);
        ((e) this.b.d).a("SimlDocumentEnd", this.b, false);
        this.b.b.a(this.b.d);
        if (this.b.b.d() || this.b.b.b() || this.b.b.c()) {
            this.b.b.j();
        } else {
            this.b.b.e();
        }
    }
}
