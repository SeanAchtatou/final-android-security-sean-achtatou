package com.snda.youni.mms.ui;

public final class ab {
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0060 A[ExcHandler: IllegalAccessException | InstantiationException | NoSuchMethodException | InvocationTargetException (e java.lang.Throwable), Splitter:B:1:0x0002] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.snda.youni.mms.ui.Presenter a(java.lang.String r5, android.content.Context r6, com.snda.youni.mms.ui.at r7, com.sd.android.mms.a.c r8) {
        /*
            java.lang.String r0 = "."
            int r0 = r5.indexOf(r0)     // Catch:{ ClassNotFoundException -> 0x004a, NoSuchMethodException -> 0x0066, InvocationTargetException -> 0x0064, IllegalAccessException -> 0x0062, InstantiationException -> 0x0060 }
            r1 = -1
            if (r0 != r1) goto L_0x006a
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ ClassNotFoundException -> 0x004a, NoSuchMethodException -> 0x0066, InvocationTargetException -> 0x0064, IllegalAccessException -> 0x0062, InstantiationException -> 0x0060 }
            r0.<init>()     // Catch:{ ClassNotFoundException -> 0x004a, NoSuchMethodException -> 0x0066, InvocationTargetException -> 0x0064, IllegalAccessException -> 0x0062, InstantiationException -> 0x0060 }
            java.lang.String r1 = "com.snda.youni.mms.ui."
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ ClassNotFoundException -> 0x004a, NoSuchMethodException -> 0x0066, InvocationTargetException -> 0x0064, IllegalAccessException -> 0x0062, InstantiationException -> 0x0060 }
            java.lang.StringBuilder r0 = r0.append(r5)     // Catch:{ ClassNotFoundException -> 0x004a, NoSuchMethodException -> 0x0066, InvocationTargetException -> 0x0064, IllegalAccessException -> 0x0062, InstantiationException -> 0x0060 }
            java.lang.String r0 = r0.toString()     // Catch:{ ClassNotFoundException -> 0x004a, NoSuchMethodException -> 0x0066, InvocationTargetException -> 0x0064, IllegalAccessException -> 0x0062, InstantiationException -> 0x0060 }
        L_0x001c:
            java.lang.Class r1 = java.lang.Class.forName(r0)     // Catch:{ ClassNotFoundException -> 0x0068, NoSuchMethodException -> 0x0066, InvocationTargetException -> 0x0064, IllegalAccessException -> 0x0062, InstantiationException -> 0x0060 }
            r2 = 3
            java.lang.Class[] r2 = new java.lang.Class[r2]     // Catch:{ ClassNotFoundException -> 0x0068, NoSuchMethodException -> 0x0066, InvocationTargetException -> 0x0064, IllegalAccessException -> 0x0062, InstantiationException -> 0x0060 }
            r3 = 0
            java.lang.Class<android.content.Context> r4 = android.content.Context.class
            r2[r3] = r4     // Catch:{ ClassNotFoundException -> 0x0068, NoSuchMethodException -> 0x0066, InvocationTargetException -> 0x0064, IllegalAccessException -> 0x0062, InstantiationException -> 0x0060 }
            r3 = 1
            java.lang.Class<com.snda.youni.mms.ui.at> r4 = com.snda.youni.mms.ui.at.class
            r2[r3] = r4     // Catch:{ ClassNotFoundException -> 0x0068, NoSuchMethodException -> 0x0066, InvocationTargetException -> 0x0064, IllegalAccessException -> 0x0062, InstantiationException -> 0x0060 }
            r3 = 2
            java.lang.Class<com.sd.android.mms.a.c> r4 = com.sd.android.mms.a.c.class
            r2[r3] = r4     // Catch:{ ClassNotFoundException -> 0x0068, NoSuchMethodException -> 0x0066, InvocationTargetException -> 0x0064, IllegalAccessException -> 0x0062, InstantiationException -> 0x0060 }
            java.lang.reflect.Constructor r1 = r1.getConstructor(r2)     // Catch:{ ClassNotFoundException -> 0x0068, NoSuchMethodException -> 0x0066, InvocationTargetException -> 0x0064, IllegalAccessException -> 0x0062, InstantiationException -> 0x0060 }
            r2 = 3
            java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch:{ ClassNotFoundException -> 0x0068, NoSuchMethodException -> 0x0066, InvocationTargetException -> 0x0064, IllegalAccessException -> 0x0062, InstantiationException -> 0x0060 }
            r3 = 0
            r2[r3] = r6     // Catch:{ ClassNotFoundException -> 0x0068, NoSuchMethodException -> 0x0066, InvocationTargetException -> 0x0064, IllegalAccessException -> 0x0062, InstantiationException -> 0x0060 }
            r3 = 1
            r2[r3] = r7     // Catch:{ ClassNotFoundException -> 0x0068, NoSuchMethodException -> 0x0066, InvocationTargetException -> 0x0064, IllegalAccessException -> 0x0062, InstantiationException -> 0x0060 }
            r3 = 2
            r2[r3] = r8     // Catch:{ ClassNotFoundException -> 0x0068, NoSuchMethodException -> 0x0066, InvocationTargetException -> 0x0064, IllegalAccessException -> 0x0062, InstantiationException -> 0x0060 }
            java.lang.Object r5 = r1.newInstance(r2)     // Catch:{ ClassNotFoundException -> 0x0068, NoSuchMethodException -> 0x0066, InvocationTargetException -> 0x0064, IllegalAccessException -> 0x0062, InstantiationException -> 0x0060 }
            com.snda.youni.mms.ui.Presenter r5 = (com.snda.youni.mms.ui.Presenter) r5     // Catch:{ ClassNotFoundException -> 0x0068, NoSuchMethodException -> 0x0066, InvocationTargetException -> 0x0064, IllegalAccessException -> 0x0062, InstantiationException -> 0x0060 }
            r0 = r5
        L_0x0049:
            return r0
        L_0x004a:
            r0 = move-exception
            r0 = r5
        L_0x004c:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Type not found: "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r0 = r1.append(r0)
            r0.toString()
        L_0x005e:
            r0 = 0
            goto L_0x0049
        L_0x0060:
            r0 = move-exception
            goto L_0x005e
        L_0x0062:
            r0 = move-exception
            goto L_0x005e
        L_0x0064:
            r0 = move-exception
            goto L_0x005e
        L_0x0066:
            r0 = move-exception
            goto L_0x005e
        L_0x0068:
            r1 = move-exception
            goto L_0x004c
        L_0x006a:
            r0 = r5
            goto L_0x001c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.snda.youni.mms.ui.ab.a(java.lang.String, android.content.Context, com.snda.youni.mms.ui.at, com.sd.android.mms.a.c):com.snda.youni.mms.ui.Presenter");
    }
}
