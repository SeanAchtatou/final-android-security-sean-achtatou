package com.snda.youni.mms.ui;

import android.content.Context;
import android.database.Cursor;
import android.os.Handler;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CursorAdapter;
import android.widget.ListView;
import com.snda.youni.C0000R;
import com.snda.youni.activities.ChatActivity;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

public final class k extends CursorAdapter {

    /* renamed from: a  reason: collision with root package name */
    public static final String[] f471a = {"transport_type", "_id", "thread_id", "address", "body", "date", "read", "type", "status", "subject", "service_center", "sub", "sub_cs", "date", "read", "m_type", "msg_box", "d_rpt", "rr", "err_type"};
    public ConcurrentHashMap b = new ConcurrentHashMap();
    private boolean c = false;
    private LayoutInflater d;
    private final ListView e;
    private final LinkedHashMap f;
    /* access modifiers changed from: private */
    public final as g;
    private final int h = 0;
    private Handler i;
    private Context j;
    private SimpleDateFormat k = new SimpleDateFormat("yy-MM-dd HH:mm");
    private Calendar l = Calendar.getInstance();
    private Calendar m = Calendar.getInstance();
    private boolean n;
    /* access modifiers changed from: private */
    public boolean o;
    /* access modifiers changed from: private */
    public List p;
    /* access modifiers changed from: private */
    public List q;

    public k(Context context, Cursor cursor, ListView listView, boolean z) {
        super(context, cursor);
        this.d = (LayoutInflater) context.getSystemService("layout_inflater");
        this.e = listView;
        this.f = new p(this);
        this.n = z;
        this.j = context;
        this.p = new ArrayList();
        this.q = new ArrayList();
        this.g = new as(this, cursor);
    }

    private static long a(String str, long j2) {
        return str.equals("mms") ? -j2 : j2;
    }

    public final a a(Cursor cursor) {
        return a(cursor.getString(this.g.f462a), cursor.getLong(this.g.b), cursor);
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x0062  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.snda.youni.mms.ui.a a(java.lang.String r7, long r8, android.database.Cursor r10) {
        /*
            r6 = this;
            java.util.LinkedHashMap r0 = r6.f
            long r1 = a(r7, r8)
            java.lang.Long r1 = java.lang.Long.valueOf(r1)
            java.lang.Object r0 = r0.get(r1)
            com.snda.youni.mms.ui.a r0 = (com.snda.youni.mms.ui.a) r0
            if (r0 != 0) goto L_0x0067
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "messageitem cache miss "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r8)
            r1.toString()
            if (r10 != 0) goto L_0x0028
            r0 = 0
        L_0x0027:
            return r0
        L_0x0028:
            com.snda.youni.mms.ui.a r1 = new com.snda.youni.mms.ui.a     // Catch:{ b -> 0x0058 }
            android.content.Context r2 = r6.j     // Catch:{ b -> 0x0058 }
            com.snda.youni.mms.ui.as r3 = r6.g     // Catch:{ b -> 0x0058 }
            r1.<init>(r2, r7, r10, r3)     // Catch:{ b -> 0x0058 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ b -> 0x007a }
            r0.<init>()     // Catch:{ b -> 0x007a }
            java.lang.String r2 = "put message item into cache "
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ b -> 0x007a }
            long r2 = r1.b     // Catch:{ b -> 0x007a }
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ b -> 0x007a }
            r0.toString()     // Catch:{ b -> 0x007a }
            java.util.LinkedHashMap r0 = r6.f     // Catch:{ b -> 0x007a }
            java.lang.String r2 = r1.f446a     // Catch:{ b -> 0x007a }
            long r3 = r1.b     // Catch:{ b -> 0x007a }
            long r2 = a(r2, r3)     // Catch:{ b -> 0x007a }
            java.lang.Long r2 = java.lang.Long.valueOf(r2)     // Catch:{ b -> 0x007a }
            r0.put(r2, r1)     // Catch:{ b -> 0x007a }
            r0 = r1
            goto L_0x0027
        L_0x0058:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
        L_0x005c:
            java.lang.String r2 = r0.getMessage()
            if (r2 == 0) goto L_0x0065
            r0.getMessage()
        L_0x0065:
            r0 = r1
            goto L_0x0027
        L_0x0067:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "messageitem cache hit "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r8)
            r1.toString()
            goto L_0x0027
        L_0x007a:
            r0 = move-exception
            goto L_0x005c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.snda.youni.mms.ui.k.a(java.lang.String, long, android.database.Cursor):com.snda.youni.mms.ui.a");
    }

    public final void a() {
        if (this.b != null) {
            this.b.clear();
        }
        if (this.f != null) {
            this.f.clear();
        }
    }

    public final void a(Handler handler) {
        this.i = handler;
    }

    public final void a(View view) {
        this.o = !this.o;
        if (this.o) {
            this.q.clear();
            ((Button) view).setText(this.j.getString(C0000R.string.tab_select_cancel));
            return;
        }
        this.p.clear();
        ((Button) view).setText(this.j.getString(C0000R.string.tab_select_all));
    }

    public final List b() {
        return this.p;
    }

    /* JADX WARNING: Removed duplicated region for block: B:35:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x004c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void bindView(android.view.View r12, android.content.Context r13, android.database.Cursor r14) {
        /*
            r11 = this;
            boolean r1 = r12 instanceof com.snda.youni.mms.ui.MessageListItem
            if (r1 == 0) goto L_0x00fb
            com.snda.youni.mms.ui.as r1 = r11.g
            int r1 = r1.f462a
            java.lang.String r5 = r14.getString(r1)
            com.snda.youni.mms.ui.as r1 = r11.g
            int r1 = r1.b
            long r3 = r14.getLong(r1)
            r1 = 2131558561(0x7f0d00a1, float:1.8742441E38)
            android.view.View r1 = r12.findViewById(r1)
            r0 = r1
            android.widget.CheckBox r0 = (android.widget.CheckBox) r0
            r7 = r0
            r1 = 0
            com.snda.youni.mms.ui.a r6 = r11.a(r14)
            boolean r2 = r14.moveToPrevious()
            if (r2 == 0) goto L_0x0129
            com.snda.youni.mms.ui.a r2 = r11.a(r14)
            android.text.format.Time r8 = new android.text.format.Time
            r8.<init>()
            long r9 = r2.j
            r8.set(r9)
            android.text.format.Time r2 = new android.text.format.Time
            r2.<init>()
            long r9 = r6.j
            r2.set(r9)
            int r8 = r8.yearDay
            int r2 = r2.yearDay
            if (r8 == r2) goto L_0x0129
            r1 = 1
            r8 = r1
        L_0x004a:
            if (r6 == 0) goto L_0x00fb
            com.snda.youni.activities.ChatActivity r13 = (com.snda.youni.activities.ChatActivity) r13
            boolean r1 = r13.j()
            if (r1 == 0) goto L_0x0115
            r1 = 0
            r7.setVisibility(r1)
            r1 = 2130837694(0x7f0200be, float:1.728035E38)
            r7.setButtonDrawable(r1)
            com.snda.youni.mms.ui.q r1 = new com.snda.youni.mms.ui.q
            r2 = r11
            r1.<init>(r2, r3, r5, r6)
            r7.setOnCheckedChangeListener(r1)
            boolean r1 = r11.o
            if (r1 == 0) goto L_0x00fc
            java.util.List r1 = r11.q
            com.snda.youni.mms.ui.au r2 = new com.snda.youni.mms.ui.au
            r2.<init>(r11, r14)
            boolean r1 = r1.contains(r2)
            if (r1 != 0) goto L_0x010f
            r1 = 1
            r7.setChecked(r1)
        L_0x007c:
            boolean r1 = r11.n
            r6.k = r1
            int r1 = r6.n
            r2 = 12
            if (r1 != r2) goto L_0x0093
            com.snda.youni.attachment.k r1 = com.snda.youni.attachment.k.a()
            boolean r1 = r1.a(r3)
            if (r1 == 0) goto L_0x0093
            r6.d()
        L_0x0093:
            java.util.concurrent.ConcurrentHashMap r1 = r11.b
            long r2 = r6.b
            java.lang.Long r2 = java.lang.Long.valueOf(r2)
            java.lang.Object r13 = r1.get(r2)
            java.lang.Integer r13 = (java.lang.Integer) r13
            if (r13 == 0) goto L_0x00aa
            int r1 = r13.intValue()
            r6.a(r1)
        L_0x00aa:
            r0 = r12
            com.snda.youni.mms.ui.MessageListItem r0 = (com.snda.youni.mms.ui.MessageListItem) r0
            r13 = r0
            r13.a(r6)
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "bindview over:"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = r6.g
            java.lang.StringBuilder r1 = r1.append(r2)
            r1.toString()
            r0 = r12
            com.snda.youni.mms.ui.MessageListItem r0 = (com.snda.youni.mms.ui.MessageListItem) r0
            r13 = r0
            android.os.Handler r1 = r11.i
            r13.a(r1)
            if (r8 == 0) goto L_0x011c
            r1 = 2131558558(0x7f0d009e, float:1.8742435E38)
            android.view.View r1 = r12.findViewById(r1)
            java.lang.String r2 = "message_list_divider_line"
            android.graphics.drawable.Drawable r2 = com.snda.youni.d.a.a(r2)
            r1.setBackgroundDrawable(r2)
            r1 = 2131558559(0x7f0d009f, float:1.8742437E38)
            android.view.View r1 = r12.findViewById(r1)
            java.lang.String r2 = "message_list_divider"
            android.graphics.drawable.Drawable r2 = com.snda.youni.d.a.a(r2)
            r1.setBackgroundDrawable(r2)
            r1 = 2131558557(0x7f0d009d, float:1.8742433E38)
            android.view.View r1 = r12.findViewById(r1)
            r2 = 0
            r1.setVisibility(r2)
        L_0x00fb:
            return
        L_0x00fc:
            java.util.List r1 = r11.p
            com.snda.youni.mms.ui.au r2 = new com.snda.youni.mms.ui.au
            r2.<init>(r11, r14)
            boolean r1 = r1.contains(r2)
            if (r1 == 0) goto L_0x010f
            r1 = 1
            r7.setChecked(r1)
            goto L_0x007c
        L_0x010f:
            r1 = 0
            r7.setChecked(r1)
            goto L_0x007c
        L_0x0115:
            r1 = 8
            r7.setVisibility(r1)
            goto L_0x007c
        L_0x011c:
            r1 = 2131558557(0x7f0d009d, float:1.8742433E38)
            android.view.View r1 = r12.findViewById(r1)
            r2 = 8
            r1.setVisibility(r2)
            goto L_0x00fb
        L_0x0129:
            r8 = r1
            goto L_0x004a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.snda.youni.mms.ui.k.bindView(android.view.View, android.content.Context, android.database.Cursor):void");
    }

    public final List c() {
        return this.q;
    }

    public final String d() {
        String str = "";
        for (au auVar : this.p) {
            if (!TextUtils.isEmpty(auVar.c)) {
                str = str + auVar.c + "\n";
            }
        }
        "ljd forward message is " + str;
        return str;
    }

    public final boolean e() {
        return this.o;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public final View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
        return this.d.inflate((int) C0000R.layout.message_list_item, viewGroup, false);
    }

    public final void notifyDataSetChanged() {
        super.notifyDataSetChanged();
        "MessageListAdapter.notifyDataSetChanged():" + this.e.getCount();
        "MessageListAdapter.notifyDataSetChanged() over :" + this.e.getCount();
    }

    /* access modifiers changed from: protected */
    public final void onContentChanged() {
        int count = getCount();
        this.f.clear();
        super.onContentChanged();
        int count2 = getCount();
        if (count2 > count) {
            "onContentChanged from " + count + " to " + count2;
            this.c = true;
            ((ChatActivity) this.j).b(true);
            return;
        }
        "onContentChanged from " + count + " to " + count2;
        this.c = false;
    }
}
