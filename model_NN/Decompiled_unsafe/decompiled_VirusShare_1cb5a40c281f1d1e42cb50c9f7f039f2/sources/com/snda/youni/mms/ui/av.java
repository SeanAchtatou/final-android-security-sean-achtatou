package com.snda.youni.mms.ui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.snda.youni.C0000R;
import java.util.List;

public class av extends ArrayAdapter {

    /* renamed from: a  reason: collision with root package name */
    private LayoutInflater f464a;

    public av(Context context, List list) {
        super(context, (int) C0000R.layout.icon_list_item, list);
        this.f464a = (LayoutInflater) context.getSystemService("layout_inflater");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View getView(int i, View view, ViewGroup viewGroup) {
        View inflate = view == null ? this.f464a.inflate((int) C0000R.layout.icon_list_item, viewGroup, false) : view;
        ((TextView) inflate.findViewById(C0000R.id.text1)).setText(((ad) getItem(i)).a());
        ((ImageView) inflate.findViewById(C0000R.id.icon)).setImageResource(((ad) getItem(i)).b());
        return inflate;
    }
}
