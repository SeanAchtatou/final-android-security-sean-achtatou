package com.snda.youni.mms.ui;

import android.content.Context;
import android.net.Uri;
import android.os.Handler;
import android.view.View;
import android.view.ViewStub;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import com.sd.android.mms.a.a;
import com.sd.android.mms.a.e;
import com.sd.android.mms.a.h;
import com.sd.android.mms.a.q;
import com.sd.android.mms.a.r;
import com.snda.youni.C0000R;

public final class f {

    /* renamed from: a  reason: collision with root package name */
    private final Context f469a;
    /* access modifiers changed from: private */
    public final Handler b;
    private final View c;
    /* access modifiers changed from: private */
    public ao d;
    private a e;
    private Presenter f;
    /* access modifiers changed from: private */
    public int g;
    /* access modifiers changed from: private */
    public l h;
    private boolean i;

    public f(Context context, Handler handler, View view) {
        this.f469a = context;
        this.b = handler;
        this.c = view;
        view.setOnTouchListener(new d(this));
    }

    private View a(int i2, int i3) {
        View findViewById = this.c.findViewById(i3);
        return findViewById == null ? ((ViewStub) this.c.findViewById(i2)).inflate() : findViewById;
    }

    private ao a(int i2, int i3, int i4, int i5, int i6) {
        LinearLayout linearLayout = (LinearLayout) a(i2, i3);
        linearLayout.setVisibility(0);
        if (i4 != 0) {
            ((Button) linearLayout.findViewById(i4)).setOnClickListener(new ae(this, i6));
        }
        ((Button) linearLayout.findViewById(i5)).setOnClickListener(new e(this));
        return (ao) linearLayout;
    }

    public final int a() {
        return this.g;
    }

    public final void a(Uri uri) {
        q qVar = new q(this.f469a, uri, this.e.c().a());
        h b2 = this.e.get(0);
        b2.l();
        b2.k();
        b2.add((e) qVar);
    }

    public final void a(a aVar, int i2) {
        ao aoVar;
        if (i2 == -1) {
            throw new IllegalArgumentException("Type of the attachment may not be EMPTY.");
        }
        this.e = aVar;
        int i3 = this.g;
        this.g = i2;
        if (this.d != null) {
            ((View) this.d).setVisibility(8);
            this.d = null;
        }
        if (i2 != 0) {
            switch (this.g) {
                case 1:
                    aoVar = a(C0000R.id.image_attachment_view_stub, C0000R.id.image_attachment_view, 0, C0000R.id.remove_image_button, 9);
                    break;
                case 2:
                    aoVar = a(C0000R.id.video_attachment_view_stub, C0000R.id.video_attachment_view, C0000R.id.view_video_button, C0000R.id.remove_video_button, 7);
                    break;
                case 3:
                    aoVar = a(C0000R.id.audio_attachment_view_stub, C0000R.id.audio_attachment_view, 0, C0000R.id.remove_audio_button, 8);
                    break;
                case 4:
                    LinearLayout linearLayout = (LinearLayout) a((int) C0000R.id.slideshow_attachment_view_stub, (int) C0000R.id.slideshow_attachment_view);
                    linearLayout.setVisibility(0);
                    ((ImageButton) linearLayout.findViewById(C0000R.id.play_slideshow_button)).setOnClickListener(new ae(this, 3));
                    aoVar = (ao) linearLayout;
                    break;
                default:
                    throw new IllegalArgumentException();
            }
            this.d = aoVar;
            if (this.f == null || !this.e.equals(this.f.d())) {
                this.f = ab.a("MmsThumbnailPresenter", this.f469a, this.d, this.e);
            } else {
                this.f.a(this.d);
            }
            this.f.a();
        }
        if (this.h != null && this.g != i3) {
            this.h.a(this.g);
        }
    }

    public final void a(l lVar) {
        this.h = lVar;
    }

    public final void a(boolean z) {
        if (this.i != z) {
            this.i = z;
        }
    }

    public final void b() {
        h b2 = this.e.get(0);
        b2.j();
        b2.l();
        b2.k();
    }

    public final void b(Uri uri) {
        r rVar = new r(this.f469a, uri);
        h b2 = this.e.get(0);
        b2.j();
        b2.l();
        b2.add((e) rVar);
        b2.a(rVar.e());
    }

    public final void c() {
        if (this.d != null) {
            ((View) this.d).setVisibility(8);
        }
    }
}
