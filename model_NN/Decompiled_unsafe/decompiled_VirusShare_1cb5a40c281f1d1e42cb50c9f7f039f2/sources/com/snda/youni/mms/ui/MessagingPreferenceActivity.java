package com.snda.youni.mms.ui;

import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuItem;
import com.snda.youni.C0000R;

public class MessagingPreferenceActivity extends PreferenceActivity {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        addPreferencesFromResource(C0000R.xml.preferences);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        menu.clear();
        menu.add(0, 1, 0, (int) C0000R.string.restore_default);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case 1:
                PreferenceManager.getDefaultSharedPreferences(this).edit().clear().commit();
                setPreferenceScreen(null);
                addPreferencesFromResource(C0000R.xml.preferences);
                return true;
            default:
                return false;
        }
    }
}
