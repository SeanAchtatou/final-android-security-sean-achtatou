package com.snda.youni.mms.ui;

import android.a.f;
import android.a.k;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.AsyncQueryHandler;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.ContentObserver;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import com.sd.a.a.a.b.b;
import com.sd.android.mms.transaction.q;
import com.snda.youni.C0000R;

public class ManageSimMessages extends Activity implements View.OnCreateContextMenuListener {

    /* renamed from: a  reason: collision with root package name */
    private static final Uri f438a = Uri.parse("content://sms/icc");
    private int b;
    private ContentResolver c;
    /* access modifiers changed from: private */
    public Cursor d = null;
    /* access modifiers changed from: private */
    public ListView e;
    private TextView f;
    /* access modifiers changed from: private */
    public k g = null;
    private AsyncQueryHandler h = null;
    private final ContentObserver i = new aq(this, new Handler());

    private void a() {
        q.a(getApplicationContext(), 234);
        a(2);
        b();
    }

    /* access modifiers changed from: private */
    public void a(int i2) {
        if (this.b != i2) {
            this.b = i2;
            switch (i2) {
                case 0:
                    this.e.setVisibility(0);
                    this.f.setVisibility(8);
                    setTitle(getString(C0000R.string.sim_manage_messages_title));
                    setProgressBarIndeterminateVisibility(false);
                    return;
                case 1:
                    this.e.setVisibility(8);
                    this.f.setVisibility(0);
                    setTitle(getString(C0000R.string.sim_manage_messages_title));
                    setProgressBarIndeterminateVisibility(false);
                    return;
                case 2:
                    this.e.setVisibility(8);
                    this.f.setVisibility(8);
                    setTitle(getString(C0000R.string.refreshing));
                    setProgressBarIndeterminateVisibility(true);
                    return;
                default:
                    return;
            }
        }
    }

    private void a(DialogInterface.OnClickListener onClickListener, int i2) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle((int) C0000R.string.confirm_dialog_title);
        builder.setIcon(17301543);
        builder.setCancelable(true);
        builder.setPositiveButton((int) C0000R.string.yes, onClickListener);
        builder.setNegativeButton((int) C0000R.string.no, (DialogInterface.OnClickListener) null);
        builder.setMessage(i2);
        builder.show();
    }

    /* access modifiers changed from: private */
    public void a(Cursor cursor) {
        b.a(this, this.c, f438a.buildUpon().appendPath(cursor.getString(cursor.getColumnIndexOrThrow("index_on_icc"))).build(), (String) null);
    }

    static /* synthetic */ void a(ManageSimMessages manageSimMessages) {
        manageSimMessages.a(2);
        if (manageSimMessages.d != null) {
            manageSimMessages.stopManagingCursor(manageSimMessages.d);
            manageSimMessages.d.close();
        }
        manageSimMessages.b();
    }

    private void b() {
        try {
            this.h.startQuery(0, null, f438a, null, null, null, null);
        } catch (SQLiteException e2) {
            b.a(this, e2);
        }
    }

    /* access modifiers changed from: private */
    public void c() {
        this.c.registerContentObserver(f438a, true, this.i);
    }

    static /* synthetic */ void f(ManageSimMessages manageSimMessages) {
        Cursor cursor = manageSimMessages.g.getCursor();
        if (cursor != null && cursor.moveToFirst()) {
            int count = cursor.getCount();
            for (int i2 = 0; i2 < count; i2++) {
                manageSimMessages.a(cursor);
                cursor.moveToNext();
            }
        }
    }

    public boolean onContextItemSelected(MenuItem menuItem) {
        try {
            Cursor cursor = (Cursor) this.g.getItem(((AdapterView.AdapterContextMenuInfo) menuItem.getMenuInfo()).position);
            switch (menuItem.getItemId()) {
                case 0:
                    String string = cursor.getString(cursor.getColumnIndexOrThrow("address"));
                    String string2 = cursor.getString(cursor.getColumnIndexOrThrow("body"));
                    Long valueOf = Long.valueOf(cursor.getLong(cursor.getColumnIndexOrThrow("date")));
                    try {
                        int i2 = cursor.getInt(cursor.getColumnIndexOrThrow("status"));
                        if (i2 == 1 || i2 == 3) {
                            f.a(this.c, string, string2, valueOf);
                        } else {
                            k.a(this.c, string, string2, valueOf);
                        }
                    } catch (SQLiteException e2) {
                        b.a(this, e2);
                    }
                    return true;
                case 1:
                    a(new ap(this, cursor), (int) C0000R.string.confirm_delete_SIM_message);
                    return true;
                case 2:
                    return true;
                default:
                    return super.onContextItemSelected(menuItem);
            }
        } catch (ClassCastException e3) {
            return false;
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(5);
        this.c = getContentResolver();
        this.h = new aw(this, this.c, this);
        setContentView((int) C0000R.layout.sim_list);
        this.e = (ListView) findViewById(C0000R.id.messages);
        this.f = (TextView) findViewById(C0000R.id.empty_message);
        a();
    }

    public void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {
        contextMenu.add(0, 0, 0, (int) C0000R.string.sim_copy_to_phone_memory);
        contextMenu.add(0, 1, 0, (int) C0000R.string.sim_delete);
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        setIntent(intent);
        a();
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case 0:
                a(new ar(this), (int) C0000R.string.confirm_delete_all_SIM_messages);
                return true;
            default:
                return true;
        }
    }

    public void onPause() {
        super.onPause();
        this.c.unregisterContentObserver(this.i);
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.clear();
        if (this.d == null || this.d.getCount() <= 0 || this.b != 0) {
            return true;
        }
        menu.add(0, 0, 0, (int) C0000R.string.menu_delete_messages).setIcon(17301564);
        return true;
    }

    public void onResume() {
        super.onResume();
        c();
    }
}
