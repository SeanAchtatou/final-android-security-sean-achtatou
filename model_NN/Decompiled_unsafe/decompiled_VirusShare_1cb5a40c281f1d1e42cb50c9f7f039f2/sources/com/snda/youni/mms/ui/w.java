package com.snda.youni.mms.ui;

import android.drm.mobile1.b;
import android.widget.Toast;
import com.sd.android.mms.a.c;
import com.sd.android.mms.a.r;
import com.snda.youni.C0000R;

final class w implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ ao f482a;
    private /* synthetic */ c b;
    private /* synthetic */ boolean c;
    private /* synthetic */ SlideshowPresenter d;

    w(SlideshowPresenter slideshowPresenter, ao aoVar, c cVar, boolean z) {
        this.d = slideshowPresenter;
        this.f482a = aoVar;
        this.b = cVar;
        this.c = z;
    }

    public final void run() {
        try {
            SlideshowPresenter.a(this.f482a, (r) this.b, this.c);
        } catch (b e) {
            e.getMessage();
            Toast.makeText(this.d.c, this.d.c.getString(C0000R.string.insufficient_drm_rights), 0).show();
        }
    }
}
