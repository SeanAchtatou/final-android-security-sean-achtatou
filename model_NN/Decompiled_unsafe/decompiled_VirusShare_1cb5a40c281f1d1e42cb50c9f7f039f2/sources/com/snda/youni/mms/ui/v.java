package com.snda.youni.mms.ui;

import android.media.MediaPlayer;

final class v implements MediaPlayer.OnPreparedListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ SlideView f481a;

    v(SlideView slideView) {
        this.f481a = slideView;
    }

    public final void onPrepared(MediaPlayer mediaPlayer) {
        boolean unused = this.f481a.h = true;
        if (this.f481a.j > 0) {
            this.f481a.g.seekTo(this.f481a.j);
            int unused2 = this.f481a.j = 0;
        }
        if (this.f481a.i) {
            this.f481a.g.start();
            boolean unused3 = this.f481a.i = false;
            this.f481a.h();
        }
        if (this.f481a.k) {
            this.f481a.g.stop();
            this.f481a.g.release();
            MediaPlayer unused4 = this.f481a.g = null;
            boolean unused5 = this.f481a.k = false;
            this.f481a.i();
        }
    }
}
