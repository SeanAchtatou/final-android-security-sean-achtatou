package com.snda.youni.mms.ui;

import android.content.Context;
import android.graphics.BitmapFactory;
import com.sd.android.mms.a.a;
import com.sd.android.mms.a.c;
import com.sd.android.mms.a.h;
import com.sd.android.mms.a.l;
import com.sd.android.mms.a.q;
import com.sd.android.mms.a.r;
import com.snda.youni.C0000R;

public class MmsThumbnailPresenter extends Presenter {
    public MmsThumbnailPresenter(Context context, at atVar, c cVar) {
        super(context, atVar, cVar);
    }

    private void a(ao aoVar, String str) {
        aoVar.a(str, BitmapFactory.decodeResource(this.c.getResources(), C0000R.drawable.ic_mms_drm_protected));
    }

    public final void a() {
        h b = ((a) this.e).get(0);
        if (b != null) {
            ao aoVar = (ao) this.d;
            aoVar.g();
            if (b.f()) {
                q n = b.n();
                if (n.q()) {
                    a(aoVar, n.k());
                } else {
                    aoVar.a(n.k(), n.x());
                }
            } else if (b.i()) {
                l p = b.p();
                if (p.q()) {
                    a(aoVar, p.k());
                } else {
                    aoVar.a(p.h());
                }
            } else if (b.h()) {
                r o = b.o();
                if (o.q()) {
                    a(aoVar, o.k());
                } else {
                    aoVar.a(o.h(), o.k());
                }
            }
        }
    }

    public final void a(c cVar, boolean z) {
    }
}
