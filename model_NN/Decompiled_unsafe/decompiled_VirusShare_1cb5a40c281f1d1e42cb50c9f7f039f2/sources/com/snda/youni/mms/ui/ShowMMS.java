package com.snda.youni.mms.ui;

import android.app.Activity;
import android.app.ProgressDialog;
import android.database.Cursor;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.LinearLayout;
import com.sd.a.a.a.a.d;
import com.sd.a.a.a.a.w;
import com.sd.android.mms.c.a;

public class ShowMMS extends Activity {

    /* renamed from: a  reason: collision with root package name */
    LinearLayout f441a;
    ProgressDialog b;
    String c;
    private r d = new r(this);

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        this.f441a = new LinearLayout(this);
        this.f441a.setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
        this.f441a.setOrientation(1);
        a.a(this);
        Cursor query = getContentResolver().query(getIntent().getData(), null, null, null, null);
        if (query != null && query.moveToNext()) {
            for (int i = 0; i < query.getColumnCount(); i++) {
                query.getColumnName(i) + "=" + query.getString(i);
            }
            this.c = query.getString(query.getColumnIndex("sub"));
            if (!TextUtils.isEmpty(this.c)) {
                this.c = new w(query.getInt(query.getColumnIndex("sub_cs")), d.a(this.c)).c();
            }
            query.close();
        }
        this.d.execute(getIntent().getData());
    }
}
