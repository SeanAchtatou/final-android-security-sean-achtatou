package com.snda.youni.mms.ui;

import android.widget.MediaController;
import com.sd.android.mms.b.a.g;

final class b implements MediaController.MediaPlayerControl {

    /* renamed from: a  reason: collision with root package name */
    private final g f466a;
    private /* synthetic */ SlideshowActivity b;

    public b(SlideshowActivity slideshowActivity, g gVar) {
        this.b = slideshowActivity;
        this.f466a = gVar;
    }

    public final boolean canPause() {
        return true;
    }

    public final boolean canSeekBackward() {
        return true;
    }

    public final boolean canSeekForward() {
        return true;
    }

    public final int getBufferPercentage() {
        return 100;
    }

    public final int getCurrentPosition() {
        return this.f466a.l();
    }

    public final int getDuration() {
        return this.f466a.k();
    }

    public final boolean isPlaying() {
        if (this.f466a != null) {
            return this.f466a.b();
        }
        return false;
    }

    public final void pause() {
        if (this.f466a != null) {
            this.f466a.f();
        }
    }

    public final void seekTo(int i) {
    }

    public final void start() {
        if (this.f466a != null) {
            this.f466a.g();
        }
    }
}
