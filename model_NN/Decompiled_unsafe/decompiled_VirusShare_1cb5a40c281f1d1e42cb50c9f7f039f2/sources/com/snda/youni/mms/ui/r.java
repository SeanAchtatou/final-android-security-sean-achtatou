package com.snda.youni.mms.ui;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import com.sd.a.a.a.b;
import com.sd.android.mms.a.a;
import com.sd.android.mms.a.h;
import com.sd.android.mms.a.p;
import com.sd.android.mms.a.q;
import com.snda.youni.C0000R;
import java.util.Iterator;

final class r extends AsyncTask {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ ShowMMS f477a;

    r(ShowMMS showMMS) {
        this.f477a = showMMS;
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public Void doInBackground(Uri... uriArr) {
        try {
            a a2 = a.a(this.f477a, uriArr[0]);
            Context context = this.f477a.f441a.getContext();
            boolean z = true;
            Iterator it = a2.iterator();
            while (it.hasNext()) {
                h hVar = (h) it.next();
                q n = hVar.n();
                p m = hVar.m();
                if (n != null) {
                    ImageView imageView = new ImageView(context);
                    imageView.setImageBitmap(n.x());
                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -1);
                    if (z) {
                        layoutParams.setMargins(10, 10, 10, 5);
                        z = false;
                    } else {
                        layoutParams.setMargins(10, 5, 10, 5);
                    }
                    imageView.setLayoutParams(layoutParams);
                    this.f477a.f441a.addView(imageView);
                }
                if (m != null) {
                    TextView textView = new TextView(context);
                    textView.setText(m.x().replace("\r", ""));
                    textView.setTextColor(-16777216);
                    textView.setTextSize(18.0f);
                    LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(-1, -1);
                    if (z) {
                        layoutParams2.setMargins(10, 10, 10, 5);
                        z = false;
                    } else {
                        layoutParams2.setMargins(10, 5, 10, 5);
                    }
                    textView.setLayoutParams(layoutParams2);
                    this.f477a.f441a.addView(textView);
                }
                boolean z2 = z;
                ImageView imageView2 = new ImageView(context);
                imageView2.setImageResource(C0000R.drawable.list_seperator);
                LinearLayout.LayoutParams layoutParams3 = new LinearLayout.LayoutParams(-1, -1);
                layoutParams3.setMargins(0, 10, 0, 10);
                imageView2.setLayoutParams(layoutParams3);
                this.f477a.f441a.addView(imageView2);
                z = z2;
            }
            return null;
        } catch (b e) {
            e.printStackTrace();
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void onPostExecute(Object obj) {
        ScrollView scrollView = new ScrollView(this.f477a);
        scrollView.addView(this.f477a.f441a);
        LinearLayout linearLayout = (LinearLayout) ((LayoutInflater) this.f477a.getSystemService("layout_inflater")).inflate((int) C0000R.layout.activity_showmms, (ViewGroup) null);
        ((TextView) linearLayout.findViewById(C0000R.id.show_mms_subject)).setText(this.f477a.c);
        linearLayout.addView(scrollView);
        this.f477a.setContentView(linearLayout);
        this.f477a.b.dismiss();
        super.onPostExecute((Void) obj);
    }

    /* access modifiers changed from: protected */
    public final void onPreExecute() {
        this.f477a.b = ProgressDialog.show(this.f477a, "", "loading");
        super.onPreExecute();
    }
}
