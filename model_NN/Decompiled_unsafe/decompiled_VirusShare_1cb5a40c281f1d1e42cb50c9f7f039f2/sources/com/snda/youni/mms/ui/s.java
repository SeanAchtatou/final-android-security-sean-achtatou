package com.snda.youni.mms.ui;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.Uri;
import android.os.Handler;
import com.sd.a.a.a.a.i;
import com.sd.android.mms.f.a;

final class s implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ af f478a;
    private /* synthetic */ Context b;
    private /* synthetic */ Uri c;
    private /* synthetic */ Handler d;
    private /* synthetic */ Runnable e;
    private /* synthetic */ ProgressDialog f;

    s(Context context, Uri uri, Handler handler, Runnable runnable, ProgressDialog progressDialog, af afVar) {
        this.b = context;
        this.c = uri;
        this.d = handler;
        this.e = runnable;
        this.f = progressDialog;
        this.f478a = afVar;
    }

    /* JADX INFO: finally extract failed */
    public final void run() {
        try {
            i a2 = new a(this.b, this.c).a(com.sd.android.mms.a.e(), com.sd.android.mms.a.d());
            this.d.removeCallbacks(this.e);
            this.f.dismiss();
            this.d.post(new ac(this, a2));
        } catch (Throwable th) {
            this.d.removeCallbacks(this.e);
            this.f.dismiss();
            throw th;
        }
    }
}
