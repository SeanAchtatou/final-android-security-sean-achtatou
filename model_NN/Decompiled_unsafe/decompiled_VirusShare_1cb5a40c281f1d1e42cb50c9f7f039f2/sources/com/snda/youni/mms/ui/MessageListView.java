package com.snda.youni.mms.ui;

import android.content.Context;
import android.text.ClipboardManager;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.widget.ListView;

public final class MessageListView extends ListView {
    public MessageListView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public final boolean onKeyShortcut(int i, KeyEvent keyEvent) {
        a h;
        switch (i) {
            case 31:
                MessageListItem messageListItem = (MessageListItem) getSelectedView();
                if (!(messageListItem == null || (h = messageListItem.h()) == null || !h.n())) {
                    ((ClipboardManager) getContext().getSystemService("clipboard")).setText(h.g);
                    return true;
                }
        }
        return super.onKeyShortcut(i, keyEvent);
    }
}
