package com.snda.youni.mms.ui;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.media.MediaPlayer;
import android.net.Uri;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import com.snda.youni.C0000R;

public class AudioAttachmentView extends LinearLayout implements ao {

    /* renamed from: a  reason: collision with root package name */
    private final Resources f436a;
    private Uri b;
    private MediaPlayer c;
    private boolean d;
    private Context e;

    public AudioAttachmentView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.f436a = context.getResources();
        this.e = context;
    }

    static /* synthetic */ void a(AudioAttachmentView audioAttachmentView) {
        audioAttachmentView.f436a.getString(C0000R.string.cannot_play_audio);
        audioAttachmentView.b();
    }

    public final synchronized void a() {
        if (!this.d && this.b != null) {
            this.c = MediaPlayer.create(this.e, this.b);
            if (this.c != null) {
                this.c.setAudioStreamType(3);
                this.c.setOnCompletionListener(new an(this));
                this.c.setOnErrorListener(new am(this));
                this.d = true;
                this.c.start();
            }
        }
    }

    public final void a(int i) {
    }

    public final void a(Uri uri) {
    }

    public final void a(Uri uri, String str) {
        synchronized (this) {
            this.b = uri;
        }
    }

    public final void a(String str) {
    }

    public final void a(String str, Bitmap bitmap) {
    }

    public final void a(boolean z) {
    }

    public final synchronized void b() {
        try {
            if (this.c != null) {
                this.c.stop();
                this.c.release();
                this.c = null;
            }
            this.d = false;
        } catch (Throwable th) {
            this.d = false;
            throw th;
        }
    }

    public final void b(int i) {
    }

    public final void b(boolean z) {
    }

    public final void c() {
    }

    public final void c(boolean z) {
    }

    public final void d() {
    }

    public final void e() {
    }

    public final void f() {
    }

    public final void g() {
        synchronized (this) {
            if (this.d) {
                b();
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onFinishInflate() {
    }
}
