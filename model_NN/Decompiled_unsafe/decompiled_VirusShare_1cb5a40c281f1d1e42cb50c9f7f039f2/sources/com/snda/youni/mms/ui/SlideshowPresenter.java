package com.snda.youni.mms.ui;

import android.content.Context;
import android.os.Handler;
import android.widget.Toast;
import com.sd.android.mms.a.a;
import com.sd.android.mms.a.b;
import com.sd.android.mms.a.c;
import com.sd.android.mms.a.e;
import com.sd.android.mms.a.h;
import com.sd.android.mms.a.j;
import com.sd.android.mms.a.k;
import com.sd.android.mms.a.l;
import com.sd.android.mms.a.p;
import com.sd.android.mms.a.q;
import com.sd.android.mms.a.r;
import com.snda.youni.C0000R;
import java.util.Iterator;

public class SlideshowPresenter extends Presenter {

    /* renamed from: a  reason: collision with root package name */
    protected float f445a;
    protected float b;
    private int f = 0;
    private int g = ((a) this.e).size();
    private Handler h = new Handler();
    private final c i = new z(this);

    public SlideshowPresenter(Context context, at atVar, c cVar) {
        super(context, atVar, cVar);
        if (atVar instanceof j) {
            ((j) atVar).a(this.i);
        }
    }

    private int a(int i2) {
        return (int) (((float) i2) / this.f445a);
    }

    protected static void a(ao aoVar, r rVar, boolean z) {
        if (z) {
            aoVar.a(rVar.i(), rVar.k());
        }
        k u = rVar.u();
        if (u == k.START) {
            aoVar.a();
        } else if (u == k.PAUSE) {
            aoVar.c();
        } else if (u == k.STOP) {
            aoVar.b();
        } else if (u == k.SEEK) {
            aoVar.a(rVar.t());
        }
    }

    private int b(int i2) {
        return (int) (((float) i2) / this.b);
    }

    public final void a() {
        a((ao) this.d, ((a) this.e).get(this.f));
    }

    public final void a(c cVar, boolean z) {
        ao aoVar = (ao) this.d;
        if (cVar instanceof a) {
            return;
        }
        if (cVar instanceof h) {
            if (((h) cVar).c()) {
                this.h.post(new aa(this, aoVar, cVar));
            } else {
                this.h.post(new x(this));
            }
        } else if (!(cVar instanceof e)) {
        } else {
            if (cVar instanceof b) {
                this.h.post(new y(this, aoVar, cVar, z));
            } else if (((e) cVar).p()) {
                this.h.post(new w(this, aoVar, cVar, z));
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void a(ao aoVar, b bVar, boolean z) {
        j a2 = bVar.a();
        if (bVar.m()) {
            p pVar = (p) bVar;
            if (z) {
                aoVar.a(pVar.x());
            }
            if (aoVar instanceof j) {
                ((j) aoVar).b(a(a2.c()), b(a2.e()), a(a2.f()), b(a2.h()));
            }
            aoVar.b(pVar.b());
        } else if (bVar.n()) {
            q qVar = (q) bVar;
            if (z) {
                aoVar.a(qVar.k(), qVar.y());
            }
            if (aoVar instanceof j) {
                ((j) aoVar).a(a(a2.c()), b(a2.e()), a(a2.f()), b(a2.h()));
            }
            aoVar.a(qVar.b());
        } else if (bVar.o()) {
            l lVar = (l) bVar;
            if (z) {
                aoVar.a(lVar.i());
            }
            if (aoVar instanceof j) {
                ((j) aoVar).c(a(a2.c()), b(a2.e()), a(a2.f()), b(a2.h()));
            }
            aoVar.c(lVar.b());
            k u = lVar.u();
            if (u == k.START) {
                aoVar.d();
            } else if (u == k.PAUSE) {
                aoVar.f();
            } else if (u == k.STOP) {
                aoVar.e();
            } else if (u == k.SEEK) {
                aoVar.b(lVar.t());
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.snda.youni.mms.ui.SlideshowPresenter.a(com.snda.youni.mms.ui.ao, com.sd.android.mms.a.b, boolean):void
     arg types: [com.snda.youni.mms.ui.ao, com.sd.android.mms.a.b, int]
     candidates:
      com.snda.youni.mms.ui.SlideshowPresenter.a(com.snda.youni.mms.ui.ao, com.sd.android.mms.a.r, boolean):void
      com.snda.youni.mms.ui.SlideshowPresenter.a(com.snda.youni.mms.ui.ao, com.sd.android.mms.a.b, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.snda.youni.mms.ui.SlideshowPresenter.a(com.snda.youni.mms.ui.ao, com.sd.android.mms.a.r, boolean):void
     arg types: [com.snda.youni.mms.ui.ao, com.sd.android.mms.a.r, int]
     candidates:
      com.snda.youni.mms.ui.SlideshowPresenter.a(com.snda.youni.mms.ui.ao, com.sd.android.mms.a.b, boolean):void
      com.snda.youni.mms.ui.SlideshowPresenter.a(com.snda.youni.mms.ui.ao, com.sd.android.mms.a.r, boolean):void */
    /* access modifiers changed from: protected */
    public final void a(ao aoVar, h hVar) {
        aoVar.g();
        try {
            Iterator it = hVar.iterator();
            while (it.hasNext()) {
                e eVar = (e) it.next();
                if (eVar instanceof b) {
                    a(aoVar, (b) eVar, true);
                } else if (eVar.p()) {
                    a(aoVar, (r) eVar, true);
                }
            }
        } catch (android.drm.mobile1.b e) {
            e.getMessage();
            Toast.makeText(this.c, this.c.getString(C0000R.string.insufficient_drm_rights), 0).show();
        }
    }

    public final void b() {
        if (this.f > 0) {
            this.f--;
        }
    }

    public final void c() {
        if (this.f < this.g - 1) {
            this.f++;
        }
    }
}
