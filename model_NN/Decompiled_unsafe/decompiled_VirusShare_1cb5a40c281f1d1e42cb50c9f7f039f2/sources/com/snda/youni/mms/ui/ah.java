package com.snda.youni.mms.ui;

import android.content.Intent;
import android.net.Uri;
import android.view.View;

final class ah implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ String f452a;
    private /* synthetic */ MessageListItem b;

    ah(MessageListItem messageListItem, String str) {
        this.b = messageListItem;
        this.f452a = str;
    }

    public final void onClick(View view) {
        Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(this.f452a));
        intent.setFlags(268435456);
        this.b.s.startActivity(intent);
    }
}
