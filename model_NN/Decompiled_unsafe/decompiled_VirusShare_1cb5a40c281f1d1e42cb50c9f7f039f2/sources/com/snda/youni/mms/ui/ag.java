package com.snda.youni.mms.ui;

import android.content.Intent;
import android.view.View;
import com.sd.android.mms.transaction.MyTransactionService;

final class ag implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ a f451a;
    private /* synthetic */ MessageListItem b;

    ag(MessageListItem messageListItem, a aVar) {
        this.b = messageListItem;
        this.f451a = aVar;
    }

    public final void onClick(View view) {
        this.b.k.setVisibility(0);
        this.b.j.setVisibility(8);
        Intent intent = new Intent(this.b.s, MyTransactionService.class);
        intent.putExtra("uri", this.f451a.l.toString());
        intent.putExtra("type", 1);
        this.b.s.startService(intent);
    }
}
