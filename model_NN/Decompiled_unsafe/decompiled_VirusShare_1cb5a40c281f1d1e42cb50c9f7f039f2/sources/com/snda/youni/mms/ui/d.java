package com.snda.youni.mms.ui;

import android.view.MotionEvent;
import android.view.View;

final class d implements View.OnTouchListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ f f467a;

    d(f fVar) {
        this.f467a = fVar;
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        return motionEvent.getAction() == 0;
    }
}
