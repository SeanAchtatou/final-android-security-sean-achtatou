package com.snda.youni.mms.ui;

import android.content.AsyncQueryHandler;
import android.content.ContentResolver;
import android.database.Cursor;
import android.widget.ListAdapter;

final class aw extends AsyncQueryHandler {

    /* renamed from: a  reason: collision with root package name */
    private final ManageSimMessages f465a;
    private /* synthetic */ ManageSimMessages b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public aw(ManageSimMessages manageSimMessages, ContentResolver contentResolver, ManageSimMessages manageSimMessages2) {
        super(contentResolver);
        this.b = manageSimMessages;
        this.f465a = manageSimMessages2;
    }

    /* access modifiers changed from: protected */
    public final void onQueryComplete(int i, Object obj, Cursor cursor) {
        Cursor unused = this.b.d = cursor;
        if (this.b.d != null) {
            if (!this.b.d.moveToFirst()) {
                this.b.a(1);
            } else if (this.b.g == null) {
                k unused2 = this.b.g = new k(this.f465a, this.b.d, this.b.e, false);
                this.b.e.setAdapter((ListAdapter) this.b.g);
                this.b.e.setOnCreateContextMenuListener(this.f465a);
                this.b.a(0);
            } else {
                this.b.g.changeCursor(this.b.d);
                this.b.a(0);
            }
            this.b.startManagingCursor(this.b.d);
            this.b.c();
        }
    }
}
