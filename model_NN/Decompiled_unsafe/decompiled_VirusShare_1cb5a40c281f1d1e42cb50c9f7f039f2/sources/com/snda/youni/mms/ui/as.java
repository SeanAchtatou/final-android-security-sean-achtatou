package com.snda.youni.mms.ui;

import android.database.Cursor;

public final class as {

    /* renamed from: a  reason: collision with root package name */
    int f462a;
    int b;
    int c;
    int d;
    int e;
    int f;
    int g;
    int h;
    int i;
    int j;
    int k;
    int l;
    int m;
    private int n;
    private int o;
    private int p;
    private int q;
    private int r;
    private int s;
    private /* synthetic */ k t;

    as(k kVar, Cursor cursor) {
        this.t = kVar;
        try {
            this.f462a = cursor.getColumnIndexOrThrow("transport_type");
        } catch (IllegalArgumentException e2) {
            e2.getMessage();
        }
        try {
            this.b = cursor.getColumnIndexOrThrow("_id");
        } catch (IllegalArgumentException e3) {
            e3.getMessage();
        }
        try {
            this.c = cursor.getColumnIndexOrThrow("address");
        } catch (IllegalArgumentException e4) {
            e4.getMessage();
        }
        try {
            this.d = cursor.getColumnIndexOrThrow("body");
        } catch (IllegalArgumentException e5) {
            e5.getMessage();
        }
        try {
            this.e = cursor.getColumnIndexOrThrow("date");
        } catch (IllegalArgumentException e6) {
            e6.getMessage();
        }
        try {
            this.n = cursor.getColumnIndexOrThrow("read");
        } catch (IllegalArgumentException e7) {
            e7.getMessage();
        }
        try {
            this.f = cursor.getColumnIndexOrThrow("type");
        } catch (IllegalArgumentException e8) {
            e8.getMessage();
        }
        try {
            this.g = cursor.getColumnIndexOrThrow("status");
        } catch (IllegalArgumentException e9) {
            e9.getMessage();
        }
        try {
            this.h = cursor.getColumnIndexOrThrow("subject");
        } catch (IllegalArgumentException e10) {
            e10.getMessage();
        }
        try {
            this.i = cursor.getColumnIndexOrThrow("service_center");
        } catch (IllegalArgumentException e11) {
            e11.getMessage();
        }
        try {
            this.j = cursor.getColumnIndexOrThrow("sub");
        } catch (IllegalArgumentException e12) {
            e12.getMessage();
        }
        try {
            this.k = cursor.getColumnIndexOrThrow("sub_cs");
        } catch (IllegalArgumentException e13) {
            e13.getMessage();
        }
        try {
            this.o = cursor.getColumnIndexOrThrow("date");
        } catch (IllegalArgumentException e14) {
            e14.getMessage();
        }
        try {
            this.p = cursor.getColumnIndexOrThrow("read");
        } catch (IllegalArgumentException e15) {
            e15.getMessage();
        }
        try {
            this.l = cursor.getColumnIndexOrThrow("m_type");
        } catch (IllegalArgumentException e16) {
            e16.getMessage();
        }
        try {
            this.m = cursor.getColumnIndexOrThrow("msg_box");
        } catch (IllegalArgumentException e17) {
            e17.getMessage();
        }
        try {
            this.q = cursor.getColumnIndexOrThrow("d_rpt");
        } catch (IllegalArgumentException e18) {
            e18.getMessage();
        }
        try {
            this.r = cursor.getColumnIndexOrThrow("rr");
        } catch (IllegalArgumentException e19) {
            e19.getMessage();
        }
        try {
            this.s = cursor.getColumnIndexOrThrow("err_type");
        } catch (IllegalArgumentException e20) {
            e20.getMessage();
        }
    }
}
