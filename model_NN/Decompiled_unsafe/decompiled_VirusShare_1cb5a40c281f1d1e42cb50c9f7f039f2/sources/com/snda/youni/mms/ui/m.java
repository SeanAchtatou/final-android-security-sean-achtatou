package com.snda.youni.mms.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Handler;
import android.telephony.PhoneNumberUtils;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import com.sd.a.a.a.a.d;
import com.sd.a.a.a.a.i;
import com.sd.a.a.a.a.w;
import com.sd.android.mms.a.a;
import com.sd.android.mms.a.e;
import com.sd.android.mms.a.h;
import com.snda.youni.C0000R;
import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public final class m {

    /* renamed from: a  reason: collision with root package name */
    private static String f472a;
    private static final Map b = new ConcurrentHashMap(20);
    private static final char[] c = {'-', '.', ',', '(', ')', ' ', '/', '\\', '*', '#', '+'};
    private static HashMap d = new HashMap(c.length);

    static {
        for (int i = 0; i < c.length; i++) {
            d.put(Character.valueOf(c[i]), Character.valueOf(c[i]));
        }
    }

    private m() {
    }

    public static int a(a aVar) {
        if (aVar == null) {
            return -1;
        }
        int size = aVar.size();
        if (size > 1) {
            return 4;
        }
        if (size == 1) {
            h b2 = aVar.get(0);
            if (b2.i()) {
                return 2;
            }
            if (b2.h() && b2.f()) {
                return 4;
            }
            if (b2.h()) {
                return 3;
            }
            if (b2.f()) {
                return 1;
            }
            if (b2.e()) {
                return 0;
            }
        }
        return -1;
    }

    public static Uri a(Context context, Uri uri, Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 80, byteArrayOutputStream);
        i iVar = new i();
        iVar.e("image/jpeg".getBytes());
        String str = "Image" + System.currentTimeMillis();
        iVar.c((str + ".jpg").getBytes());
        iVar.b(str.getBytes());
        iVar.a(byteArrayOutputStream.toByteArray());
        return d.a(context).a(iVar, ContentUris.parseId(uri));
    }

    public static String a(Cursor cursor) {
        String string = cursor.getString(3);
        int i = cursor.getInt(4);
        return TextUtils.isEmpty(string) ? "" : i != 0 ? new w(i, d.a(string)).c() : string;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public static void a(Context context) {
        if (context instanceof Activity) {
            Intent intent = new Intent("android.intent.action.RINGTONE_PICKER");
            intent.putExtra("android.intent.extra.ringtone.SHOW_DEFAULT", false);
            intent.putExtra("android.intent.extra.ringtone.SHOW_SILENT", false);
            intent.putExtra("android.intent.extra.ringtone.TITLE", context.getString(C0000R.string.select_audio));
            ((Activity) context).startActivityForResult(intent, 14);
        }
    }

    public static void a(Context context, Uri uri, Handler handler, af afVar) {
        ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setTitle(context.getText(C0000R.string.image_too_large));
        progressDialog.setMessage(context.getText(C0000R.string.compressing));
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        t tVar = new t(progressDialog);
        handler.postDelayed(tVar, 1000);
        new Thread(new s(context, uri, handler, tVar, progressDialog, afVar)).start();
    }

    public static void a(Context context, Uri uri, a aVar) {
        if (!(aVar == null ? false : aVar.e())) {
            Intent intent = new Intent(context, ShowMMS.class);
            intent.setData(uri);
            context.startActivity(intent);
        } else if (!aVar.e()) {
            throw new IllegalArgumentException("viewSimpleSlideshow() called on a non-simple slideshow");
        } else {
            h b2 = aVar.get(0);
            e n = b2.f() ? b2.n() : b2.i() ? b2.p() : b2.h() ? b2.o() : null;
            Intent intent2 = new Intent("android.intent.action.VIEW");
            intent2.addFlags(1);
            intent2.setData(n.h());
            context.startActivity(intent2);
        }
    }

    public static void a(Context context, String str, String str2) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setIcon((int) C0000R.drawable.ic_sms_mms_not_delivered);
        builder.setTitle(str);
        builder.setMessage(str2);
        builder.setPositiveButton(17039370, (DialogInterface.OnClickListener) null);
        builder.show();
    }

    public static boolean a(Context context, String str) {
        if (f472a == null) {
            f472a = ((TelephonyManager) context.getSystemService("phone")).getLine1Number();
        }
        return PhoneNumberUtils.compare(str, f472a);
    }

    public static void b(Context context) {
        if (context instanceof Activity) {
            Intent intent = new Intent("android.intent.action.GET_CONTENT");
            intent.setType("audio/amr");
            intent.setClassName("com.android.soundrecorder", "com.android.soundrecorder.SoundRecorder");
            ((Activity) context).startActivityForResult(intent, 15);
        }
    }

    public static void c(Context context) {
        if (context instanceof Activity) {
            Intent intent = new Intent("android.intent.action.GET_CONTENT");
            intent.setType("image/*");
            ((Activity) context).startActivityForResult(Intent.createChooser(intent, null), 10);
        }
    }
}
