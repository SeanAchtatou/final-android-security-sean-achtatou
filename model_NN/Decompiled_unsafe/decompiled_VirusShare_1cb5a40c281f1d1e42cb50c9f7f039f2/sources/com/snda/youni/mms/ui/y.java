package com.snda.youni.mms.ui;

import android.widget.Toast;
import com.sd.android.mms.a.b;
import com.sd.android.mms.a.c;
import com.snda.youni.C0000R;

final class y implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ ao f484a;
    private /* synthetic */ c b;
    private /* synthetic */ boolean c;
    private /* synthetic */ SlideshowPresenter d;

    y(SlideshowPresenter slideshowPresenter, ao aoVar, c cVar, boolean z) {
        this.d = slideshowPresenter;
        this.f484a = aoVar;
        this.b = cVar;
        this.c = z;
    }

    public final void run() {
        try {
            this.d.a(this.f484a, (b) this.b, this.c);
        } catch (android.drm.mobile1.b e) {
            e.getMessage();
            Toast.makeText(this.d.c, this.d.c.getString(C0000R.string.insufficient_drm_rights), 0).show();
        }
    }
}
