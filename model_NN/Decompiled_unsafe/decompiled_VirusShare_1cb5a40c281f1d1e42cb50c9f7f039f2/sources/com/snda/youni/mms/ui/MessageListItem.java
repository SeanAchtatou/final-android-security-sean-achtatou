package com.snda.youni.mms.ui;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.telephony.PhoneNumberUtils;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.method.HideReturnsTransformationMethod;
import android.text.style.StyleSpan;
import android.text.style.TextAppearanceSpan;
import android.text.util.Linkify;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewParent;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.sd.android.mms.d.l;
import com.snda.youni.C0000R;
import com.snda.youni.attachment.a.j;
import com.snda.youni.attachment.k;
import com.snda.youni.e.m;
import com.snda.youni.e.t;
import com.snda.youni.modules.a.a;
import com.snda.youni.modules.a.b;
import java.util.regex.Pattern;

public class MessageListItem extends LinearLayout implements View.OnClickListener, ao {

    /* renamed from: a  reason: collision with root package name */
    private static final StyleSpan f439a = new StyleSpan(1);
    private static ImageButton u;
    private View b;
    private View c;
    private ImageView d;
    private ImageView e;
    private ImageButton f;
    private TextView g;
    private TextView h;
    private TextView i;
    /* access modifiers changed from: private */
    public Button j;
    /* access modifiers changed from: private */
    public TextView k;
    private Handler l;
    private a m;
    private LinearLayout n;
    private LinearLayout o;
    private FrameLayout p;
    private ImageView q;
    private b r = new b(this.s);
    /* access modifiers changed from: private */
    public Context s;
    private SharedPreferences t = PreferenceManager.getDefaultSharedPreferences(this.s);

    public MessageListItem(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.s = context;
    }

    private CharSequence a(int i2, String str, String str2, String str3, String str4, int i3) {
        String str5;
        ColorStateList colorStateList;
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder();
        if (i2 == 0) {
            if (i3 > 0) {
                if (i3 < 10) {
                    spannableStringBuilder.append((CharSequence) (" 00:0" + i3 + " "));
                } else if (i3 >= 60) {
                    spannableStringBuilder.append((CharSequence) " 01:00 ");
                } else {
                    spannableStringBuilder.append((CharSequence) (" 00:" + i3 + " "));
                }
            }
            str5 = "";
        } else {
            str5 = str2;
        }
        boolean z = !TextUtils.isEmpty(str5);
        if (z) {
            spannableStringBuilder.append((CharSequence) str5);
        }
        if (!TextUtils.isEmpty(str)) {
            if (z) {
                spannableStringBuilder.append((CharSequence) " - ");
            }
            spannableStringBuilder.append(m.a().a(this.s, str.replaceAll(m.a().a(20).toString(), this.s.getString(C0000R.string.emotion_location_mylocation)), false));
        }
        float f2 = this.s.getResources().getDisplayMetrics().density;
        int i4 = (int) ((((float) PreferenceManager.getDefaultSharedPreferences(this.s).getInt("chat_size_name", 26)) * f2) + (f2 / 3.0f));
        try {
            colorStateList = ColorStateList.createFromXml(getResources(), getResources().getXml(C0000R.drawable.list_item_content_color));
        } catch (Exception e2) {
            colorStateList = null;
        }
        spannableStringBuilder.setSpan(new TextAppearanceSpan("serif", 0, i4, colorStateList, colorStateList), 0, spannableStringBuilder.length(), 33);
        String str6 = " (" + str3 + ")";
        SpannableStringBuilder spannableStringBuilder2 = new SpannableStringBuilder(str6);
        ColorStateList valueOf = "youni".equalsIgnoreCase(str4) ? ColorStateList.valueOf(Color.parseColor("#3366ff")) : ColorStateList.valueOf(-7829368);
        spannableStringBuilder2.setSpan(new TextAppearanceSpan("serif", 0, (int) (((float) i4) - (4.0f * f2)), valueOf, valueOf), 0, str6.length(), 33);
        spannableStringBuilder.append((CharSequence) spannableStringBuilder2);
        return spannableStringBuilder;
    }

    private void b(a aVar) {
        if (this.c != null) {
            this.f = (ImageButton) findViewById(C0000R.id.play_slideshow_button);
            switch (aVar.n) {
                case 2:
                case 3:
                case 4:
                    this.f.setTag(aVar);
                    this.f.setOnClickListener(this);
                    this.f.setVisibility(0);
                    setLongClickable(true);
                    return;
                case 12:
                    this.f.setTag(aVar);
                    this.f.setOnClickListener(this);
                    this.f.setVisibility(0);
                    if (!aVar.a()) {
                        if (aVar.j() == 3 || aVar.j() == 5 || aVar.j() == 4) {
                            this.f.setImageResource(C0000R.drawable.mms_download_btn);
                        }
                        this.f.setImageResource(C0000R.drawable.mms_play_btn);
                    } else if (k.a().a(aVar.g())) {
                        this.f.setImageResource(C0000R.drawable.mms_pause_btn);
                        u = this.f;
                        k.a().a(u);
                    } else {
                        aVar.e();
                        this.f.setImageResource(C0000R.drawable.mms_play_btn);
                    }
                    this.f.setFocusable(false);
                    return;
                default:
                    if (this.f != null) {
                        this.f.setVisibility(8);
                        return;
                    }
                    return;
            }
        }
    }

    private void c(a aVar) {
        switch (aVar.n) {
            case 1:
            case 2:
                this.d.setOnClickListener(new ak(this, aVar));
                this.d.setOnLongClickListener(new ai(this));
                break;
        }
        if (aVar.i() && aVar.c() == 11) {
            this.d.setOnClickListener(new aj(this, aVar));
            this.d.setOnLongClickListener(new al(this));
        }
    }

    private void i() {
        if (this.c != null) {
            this.c.setVisibility(8);
        }
    }

    private void j() {
        if (this.c == null) {
            findViewById(C0000R.id.mms_layout_view_stub).setVisibility(0);
            this.c = findViewById(C0000R.id.mms_view);
            this.d = (ImageView) findViewById(C0000R.id.image_view);
            this.f = (ImageButton) findViewById(C0000R.id.play_slideshow_button);
        }
        this.c.setVisibility(0);
    }

    private void k() {
        if (this.j == null) {
            findViewById(C0000R.id.mms_downloading_view_stub).setVisibility(0);
            this.j = (Button) findViewById(C0000R.id.btn_download_msg);
            this.k = (TextView) findViewById(C0000R.id.label_downloading);
        }
    }

    public final void a() {
    }

    public final void a(int i2) {
    }

    public final void a(Uri uri) {
        j();
        this.d.setVisibility(0);
    }

    public final void a(Uri uri, String str) {
    }

    public final void a(Handler handler) {
        this.l = handler;
    }

    public final void a(a aVar) {
        String str;
        int indexOf;
        int indexOf2;
        this.m = aVar;
        setLongClickable(false);
        switch (aVar.m) {
            case 130:
                i();
                this.n.setOrientation(1);
                this.n.setGravity(1);
                String str2 = this.s.getString(C0000R.string.message_size_label) + String.valueOf((aVar.q + 1023) / 1024) + this.s.getString(C0000R.string.kilobyte);
                CharSequence a2 = aVar.f446a.equals("sms") ? a(0, null, "", this.s.getString(C0000R.string.Fail_to_download_MMS) + str2 + "\n" + aVar.e, aVar.h, aVar.r) : a(1, null, aVar.o, this.s.getString(C0000R.string.Fail_to_download_MMS) + str2 + "\n" + aVar.e, aVar.h, -1);
                if (aVar.c == 1 && aVar.f.startsWith("krobot")) {
                    a2 = com.snda.youni.e.k.a(this.s, a2, this.g, true);
                }
                this.g.setText(a2);
                if (aVar.d != null) {
                    this.p.setVisibility(0);
                    this.i.setText(aVar.d);
                } else {
                    this.p.setVisibility(8);
                }
                switch (l.b().a(aVar.l)) {
                    case 129:
                        k();
                        this.k.setVisibility(0);
                        this.j.setVisibility(8);
                        break;
                    default:
                        setLongClickable(true);
                        k();
                        this.k.setVisibility(8);
                        this.j.setVisibility(0);
                        this.j.setOnClickListener(new ag(this, aVar));
                        break;
                }
                if (aVar.c == 1) {
                    this.n.setBackgroundDrawable(this.s.getResources().obtainTypedArray(C0000R.array.bg_in_details).getDrawable(this.t.getInt("shape_name", 0)));
                    ((RelativeLayout.LayoutParams) this.n.getLayoutParams()).addRule(11, 0);
                    ((RelativeLayout.LayoutParams) this.n.getLayoutParams()).addRule(9);
                    ((RelativeLayout.LayoutParams) this.o.getLayoutParams()).addRule(0, 0);
                    ((RelativeLayout.LayoutParams) this.o.getLayoutParams()).addRule(1, C0000R.id.img_attach);
                    ((RelativeLayout.LayoutParams) this.q.getLayoutParams()).addRule(0, 0);
                    ((RelativeLayout.LayoutParams) this.q.getLayoutParams()).addRule(1, C0000R.id.mms_layout_view_parent);
                } else {
                    if ((!"4".equalsIgnoreCase("" + aVar.c) && !"5".equalsIgnoreCase("" + aVar.c)) || "0".equalsIgnoreCase(aVar.i)) {
                        this.n.setBackgroundDrawable(this.s.getResources().obtainTypedArray(C0000R.array.bg_out_details).getDrawable((this.t.getInt("shape_name", 0) * this.s.getResources().obtainTypedArray(C0000R.array.color_details).length()) + this.t.getInt("color_name", 0)));
                    } else if (this.t.getInt("shape_name", 0) == 0) {
                        this.n.setBackgroundResource(C0000R.drawable.bg_out_rectangle_brown);
                    } else {
                        this.n.setBackgroundResource(C0000R.drawable.bg_out_circle_brown);
                    }
                    ((RelativeLayout.LayoutParams) this.n.getLayoutParams()).addRule(9, 0);
                    ((RelativeLayout.LayoutParams) this.n.getLayoutParams()).addRule(11);
                    ((RelativeLayout.LayoutParams) this.h.getLayoutParams()).addRule(1, 0);
                    ((RelativeLayout.LayoutParams) this.h.getLayoutParams()).addRule(0, C0000R.id.mms_layout_view_parent);
                    ((RelativeLayout.LayoutParams) this.q.getLayoutParams()).addRule(1, 0);
                    ((RelativeLayout.LayoutParams) this.q.getLayoutParams()).addRule(0, C0000R.id.mms_layout_view_parent);
                }
                this.h.setVisibility(8);
                this.e.setVisibility(8);
                this.g.setLongClickable(false);
                return;
            default:
                if (this.j != null) {
                    this.j.setVisibility(8);
                    this.k.setVisibility(8);
                }
                if (aVar.n == 12) {
                    this.n.setOrientation(0);
                    this.n.setGravity(16);
                } else {
                    this.n.setOrientation(1);
                    this.n.setGravity(1);
                }
                this.g.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                CharSequence o2 = aVar.o();
                this.q.setVisibility(8);
                String str3 = aVar.g;
                if (str3 != null && (!(str3.indexOf(m.a().a(20)) == -1 && str3.indexOf(this.s.getString(C0000R.string.emotion_location_mylocation)) == -1) && str3.contains("[http://maps.google.com/maps") && str3.contains("]") && (indexOf2 = str3.indexOf("]")) > (indexOf = str3.indexOf("[http://maps.google.com/maps") + 1))) {
                    String substring = aVar.g.substring(indexOf, indexOf2);
                    "deal:" + substring;
                    this.q.setVisibility(0);
                    this.q.setOnClickListener(new ah(this, substring));
                    str3 = aVar.g.substring(0, indexOf - 1) + aVar.g.substring(indexOf2 + 1);
                }
                if (!aVar.k || this.r == null) {
                    str = str3;
                } else if (aVar.f == null || !aVar.f.startsWith("krobot")) {
                    String a3 = t.a(PhoneNumberUtils.stripSeparators(aVar.f));
                    a a4 = this.r.a(a3);
                    str = a4 != null ? "-> " + a4.b + ": " + str3 : "-> " + a3 + ": " + str3;
                } else {
                    str = "-> " + this.s.getString(C0000R.string.youni_robot) + ": " + str3;
                }
                if (o2 == null) {
                    o2 = aVar.f446a.equals("sms") ? a(0, str, aVar.o, aVar.e, aVar.h, aVar.r) : a(1, str, aVar.o, aVar.e, aVar.h, -1);
                }
                if (aVar.c == 1 && aVar.f.startsWith("krobot")) {
                    o2 = com.snda.youni.e.k.a(this.s, o2, this.g, true);
                }
                if (aVar.d != null) {
                    this.p.setVisibility(0);
                    this.i.setText(aVar.d);
                } else {
                    this.p.setVisibility(8);
                }
                this.g.setText(o2);
                Linkify.addLinks(this.g, 3);
                Linkify.addLinks(this.g, Pattern.compile("[0-9]{5,20}+"), "tel:");
                "formattedMessage=" + ((Object) o2);
                String str4 = "" + aVar.c;
                if ("2".equalsIgnoreCase(str4)) {
                    this.h.setTextAppearance(this.s, C0000R.style.Chat_Status);
                    this.h.setText((int) C0000R.string.send_status_send_success);
                } else if ("5".equalsIgnoreCase(str4)) {
                    this.h.setTextAppearance(this.s, C0000R.style.Chat_Time_Status_Failed);
                    this.h.setText((int) C0000R.string.send_status_send_failed);
                } else if ("4".equalsIgnoreCase(str4)) {
                    this.h.setTextAppearance(this.s, C0000R.style.Chat_Status);
                    this.h.setText((int) C0000R.string.send_status_sending);
                } else {
                    this.h.setTextAppearance(this.s, C0000R.style.Chat_Status);
                    this.h.setText("");
                }
                if ("0".equalsIgnoreCase(aVar.i)) {
                    this.h.setTextAppearance(this.s, C0000R.style.Chat_Status);
                    this.h.setText("");
                }
                if (aVar.n()) {
                    i();
                    if (aVar.s != null) {
                        "load image " + aVar.o + " " + aVar.g;
                        Bitmap a5 = aVar.s.a();
                        if (a5 != null) {
                            "display image " + aVar.o;
                            a("attachment", a5);
                            if (aVar.u != null) {
                                this.h.setText(aVar.u);
                            }
                            c(aVar);
                        }
                    } else if (aVar.o == null || aVar.t == null) {
                        if (aVar.o == null || !aVar.o.endsWith("s.amr")) {
                            i();
                        } else {
                            a("audio", (Bitmap) null);
                            if (aVar.u != null) {
                                this.h.setText(aVar.u);
                            }
                        }
                    } else if (aVar.o.equals("111111")) {
                        this.h.setText(aVar.u);
                        if (aVar.c() == 11) {
                            a("attachment", (Bitmap) null);
                            j.a().a(aVar.t, aVar.b, 0, this.l);
                        } else {
                            a("audio", (Bitmap) null);
                            j.a().a(aVar.t, aVar.b, 1, this.l);
                        }
                    } else if (aVar.o.equals("222222")) {
                        this.h.setText(aVar.u);
                        if (aVar.c() == 11) {
                            a("attachment", (Bitmap) null);
                        } else {
                            a("audio", (Bitmap) null);
                        }
                    } else if (aVar.o.equals("888888")) {
                        if (aVar.c() == 11) {
                            a("thumb_lost", (Bitmap) null);
                        } else {
                            a("audio", (Bitmap) null);
                        }
                    } else if (aVar.c() == 12) {
                        a("audio", (Bitmap) null);
                    }
                    if (aVar.v == 1 && aVar.f() != -1) {
                        this.h.setText(aVar.f() + "%");
                    }
                    b(aVar);
                } else {
                    ab.a("MmsThumbnailPresenter", this.s, this, aVar.p).a();
                    if (aVar.n != 0) {
                        j();
                        this.c.setVisibility(0);
                        c(aVar);
                        b(aVar);
                    } else {
                        i();
                    }
                }
                if (aVar.c == 1) {
                    if ((aVar.c() != 11 && aVar.c() != 12) || (aVar.j() != 3 && aVar.j() != 4)) {
                        this.n.setBackgroundDrawable(this.s.getResources().obtainTypedArray(C0000R.array.bg_in_details).getDrawable(this.t.getInt("shape_name", 0)));
                    } else if (this.t.getInt("shape_name", 0) == 0) {
                        this.n.setBackgroundResource(C0000R.drawable.bg_in_rectangle_brown);
                    } else {
                        this.n.setBackgroundResource(C0000R.drawable.bg_in_circle_brown);
                    }
                    ((RelativeLayout.LayoutParams) this.n.getLayoutParams()).addRule(11, 0);
                    ((RelativeLayout.LayoutParams) this.n.getLayoutParams()).addRule(0, 0);
                    ((RelativeLayout.LayoutParams) this.n.getLayoutParams()).addRule(9);
                    ((RelativeLayout.LayoutParams) this.h.getLayoutParams()).addRule(0, 0);
                    ((RelativeLayout.LayoutParams) this.h.getLayoutParams()).addRule(1, C0000R.id.img_attach);
                    ((RelativeLayout.LayoutParams) this.q.getLayoutParams()).addRule(0, 0);
                    ((RelativeLayout.LayoutParams) this.q.getLayoutParams()).addRule(1, C0000R.id.mms_layout_view_parent);
                    return;
                }
                if (((aVar.c() != 11 && aVar.c() != 12) || (aVar.j() != 1 && aVar.j() != 2)) && ((!"4".equalsIgnoreCase("" + aVar.c) && !"5".equalsIgnoreCase("" + aVar.c)) || "0".equalsIgnoreCase(aVar.i))) {
                    this.n.setBackgroundDrawable(this.s.getResources().obtainTypedArray(C0000R.array.bg_out_details).getDrawable((this.t.getInt("shape_name", 0) * this.s.getResources().obtainTypedArray(C0000R.array.color_details).length()) + this.t.getInt("color_name", 0)));
                } else if (this.t.getInt("shape_name", 0) == 0) {
                    this.n.setBackgroundResource(C0000R.drawable.bg_out_rectangle_brown);
                } else {
                    this.n.setBackgroundResource(C0000R.drawable.bg_out_circle_brown);
                }
                ((RelativeLayout.LayoutParams) this.n.getLayoutParams()).addRule(9, 0);
                if (findViewById(C0000R.id.message_checkbox).getVisibility() == 0) {
                    ((RelativeLayout.LayoutParams) this.n.getLayoutParams()).addRule(11, 0);
                    ((RelativeLayout.LayoutParams) this.n.getLayoutParams()).addRule(0, C0000R.id.message_checkbox);
                } else {
                    ((RelativeLayout.LayoutParams) this.n.getLayoutParams()).addRule(11);
                }
                ((RelativeLayout.LayoutParams) this.h.getLayoutParams()).addRule(1, 0);
                ((RelativeLayout.LayoutParams) this.h.getLayoutParams()).addRule(0, C0000R.id.mms_layout_view_parent);
                ((RelativeLayout.LayoutParams) this.q.getLayoutParams()).addRule(1, 0);
                ((RelativeLayout.LayoutParams) this.q.getLayoutParams()).addRule(0, C0000R.id.mms_layout_view_parent);
                return;
        }
    }

    public final void a(String str) {
    }

    public final void a(String str, Bitmap bitmap) {
        j();
        if (bitmap != null) {
            this.d.setImageBitmap(bitmap);
        } else if (str != null && str.endsWith("audio")) {
            this.d.setImageBitmap(null);
        } else if (str == null || !str.endsWith("thumb_lost")) {
            this.d.setImageResource(C0000R.drawable.attachment_thumbnail_default);
        } else {
            this.d.setImageResource(C0000R.drawable.attachment_thumbnail_lost);
        }
        this.d.setVisibility(0);
    }

    public final void a(boolean z) {
    }

    public final void b() {
    }

    public final void b(int i2) {
    }

    public final void b(boolean z) {
    }

    public final void c() {
    }

    public final void c(boolean z) {
    }

    public final void d() {
    }

    public final void e() {
    }

    public final void f() {
    }

    public final void g() {
        if (this.d != null) {
            this.d.setVisibility(8);
        }
    }

    public final a h() {
        return this.m;
    }

    public void onClick(View view) {
        a aVar = (a) view.getTag();
        switch (aVar.n) {
            case 2:
            case 3:
            case 4:
                m.a(this.s, aVar.l, aVar.p);
                return;
            case 12:
                if (aVar.a()) {
                    u = null;
                    Message obtainMessage = this.l.obtainMessage();
                    obtainMessage.what = 11;
                    obtainMessage.sendToTarget();
                    k.a().b();
                    return;
                } else if (aVar.j() == 3) {
                    return;
                } else {
                    if (aVar.j() == 5 || aVar.j() == 4) {
                        aVar.l();
                        return;
                    }
                    u = (ImageButton) view;
                    Message obtainMessage2 = this.l.obtainMessage();
                    obtainMessage2.what = 10;
                    ViewParent parent = getParent();
                    if (parent instanceof ListView) {
                        obtainMessage2.arg1 = ((ListView) parent).getPositionForView(view);
                    } else {
                        obtainMessage2.arg1 = -1;
                    }
                    obtainMessage2.sendToTarget();
                    k.a().b(view);
                    return;
                }
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onFinishInflate() {
        super.onFinishInflate();
        this.n = (LinearLayout) findViewById(C0000R.id.mms_layout_view_parent);
        this.o = (LinearLayout) findViewById(C0000R.id.mms_status_layout_view_parent);
        this.b = findViewById(C0000R.id.msg_list_item);
        this.g = (TextView) findViewById(C0000R.id.text_view);
        this.h = (TextView) findViewById(C0000R.id.send_status);
        this.i = (TextView) findViewById(C0000R.id.message_list_divider);
        this.e = (ImageView) findViewById(C0000R.id.right_status_indicator);
        this.h.setTextAppearance(this.s, C0000R.style.Chat_Status);
        this.q = (ImageView) findViewById(C0000R.id.img_attach);
        this.p = (FrameLayout) findViewById(C0000R.id.message_list_divider_layout);
    }
}
