package com.snda.youni.mms.ui;

import android.database.ContentObserver;
import android.os.Handler;

final class aq extends ContentObserver {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ ManageSimMessages f460a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    aq(ManageSimMessages manageSimMessages, Handler handler) {
        super(handler);
        this.f460a = manageSimMessages;
    }

    public final void onChange(boolean z) {
        ManageSimMessages.a(this.f460a);
    }
}
