package com.snda.youni.mms.ui;

import android.a.d;
import android.a.m;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.text.TextUtils;
import com.sd.a.a.a.a.j;
import com.sd.a.a.a.a.l;
import com.sd.a.a.a.a.p;
import com.sd.a.a.a.a.t;
import com.sd.a.a.a.a.w;
import com.sd.android.mms.d.i;
import com.snda.youni.C0000R;
import com.snda.youni.attachment.h;
import com.snda.youni.e.e;
import com.snda.youni.e.f;
import com.snda.youni.modules.d.b;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public final class a {
    private static String w = "JB-YouNi-MessageItem";
    private boolean A = false;
    private int B = -1;
    private SimpleDateFormat C = new SimpleDateFormat("yy-MM-dd HH:mm");
    private Calendar D = Calendar.getInstance();
    private Calendar E = Calendar.getInstance();

    /* renamed from: a  reason: collision with root package name */
    final String f446a;
    final long b;
    final int c;
    String d;
    String e;
    String f;
    String g;
    String h;
    String i;
    long j;
    boolean k;
    Uri l;
    int m;
    int n;
    String o;
    com.sd.android.mms.a.a p;
    int q;
    int r;
    h s;
    String t;
    String u;
    int v = -1;
    private Context x;
    private String y;
    private CharSequence z;

    public a(Context context, String str, Cursor cursor, as asVar) {
        int i2;
        int i3;
        this.x = context;
        this.b = cursor.getLong(asVar.b);
        this.i = cursor.getString(asVar.g);
        this.r = -1;
        if ("sms".equals(str)) {
            this.n = -1;
            this.l = ContentUris.withAppendedId(m.f14a, this.b);
            this.c = cursor.getInt(asVar.f);
            this.f = cursor.getString(asVar.c);
            this.o = cursor.getString(asVar.h);
            this.v = -1;
            this.g = cursor.getString(asVar.d);
            "body = " + this.g;
            this.s = null;
            if (this.o != null) {
                if (this.g != null) {
                    if (this.g.startsWith("[http://n.sdo.com/")) {
                        this.n = 11;
                        int indexOf = this.g.indexOf("]");
                        i3 = this.g.indexOf("[http://n.sdo.com/") + 1;
                        i2 = indexOf;
                    } else if (this.g.startsWith("[ http://n.sdo.com/")) {
                        this.n = 11;
                        int indexOf2 = this.g.indexOf("]");
                        i3 = this.g.indexOf("[ http://n.sdo.com/") + 1;
                        i2 = indexOf2;
                    } else if (this.g.startsWith("<http://n.sdo.com/")) {
                        this.n = 12;
                        int indexOf3 = this.g.indexOf(">");
                        i3 = this.g.indexOf("<http://n.sdo.com/") + 1;
                        i2 = indexOf3;
                    } else if (this.g.startsWith("< http://n.sdo.com/")) {
                        this.n = 12;
                        int indexOf4 = this.g.indexOf(">");
                        i3 = this.g.indexOf("< http://n.sdo.com/") + 1;
                        i2 = indexOf4;
                    } else {
                        i2 = 0;
                        i3 = -1;
                    }
                    if (i3 == -1 || i2 <= i3) {
                        this.n = -1;
                    } else {
                        this.t = this.g.substring(i3, i2).trim();
                        this.g = this.g.substring(0, i3 - 1) + this.g.substring(i2 + 1);
                        int lastIndexOf = this.g.lastIndexOf("(");
                        if (lastIndexOf > 0) {
                            this.g = this.g.substring(0, lastIndexOf);
                        } else if (lastIndexOf != -1) {
                            this.g = "";
                        }
                    }
                }
                if (this.o.equals("111111") || this.o.equals("333333")) {
                    this.o = "111111";
                    this.u = this.x.getString(C0000R.string.attachment_status_downloading);
                    this.v = 3;
                } else if (this.o.equals("222222") || this.o.equals("444444")) {
                    this.o = "222222";
                    this.v = 4;
                    this.u = this.x.getString(C0000R.string.attachment_status_download_failed);
                } else {
                    String str2 = this.o;
                    if (str2.endsWith(".amr")) {
                        this.n = 12;
                        int lastIndexOf2 = this.o.lastIndexOf(95);
                        int length = this.o.length();
                        if (lastIndexOf2 != -1 && lastIndexOf2 + 1 < length - 5) {
                            this.r = Integer.parseInt(this.o.substring(lastIndexOf2 + 1, length - 5));
                        }
                        if (e.b(str2, com.snda.youni.attachment.e.j)) {
                            this.v = 0;
                        } else {
                            this.o = "888888";
                            this.v = 5;
                        }
                    } else {
                        if (str2.endsWith(".jpg")) {
                            this.n = 11;
                            if (e.b(str2, com.snda.youni.attachment.e.i)) {
                                this.s = new h(this.x, str2);
                                if (this.t != null) {
                                    this.v = 0;
                                }
                            } else {
                                this.o = "888888";
                                this.v = 5;
                            }
                        } else {
                            this.o = null;
                            this.g = cursor.getString(asVar.d);
                            this.t = null;
                        }
                    }
                }
            }
            this.h = cursor.getString(asVar.i);
            if (this.h != null) {
                if (this.h.equals("+666666")) {
                    this.u = this.x.getString(C0000R.string.attachment_status_uploading);
                    this.h = "youni";
                    this.v = 1;
                } else if (this.h.equals("+7777777")) {
                    this.u = this.x.getString(C0000R.string.attachment_status_upload_failed);
                    this.v = 2;
                    this.h = "youni";
                } else if (!this.h.startsWith("+")) {
                    b d2 = com.snda.youni.h.a.a.a().d(Long.toString(this.b));
                    if (d2 != null) {
                        this.h = d2.i();
                    } else {
                        this.h = "sms";
                    }
                }
                this.j = cursor.getLong(asVar.e);
                this.e = a(this.j);
                this.d = f.a(this.j, context);
                "dividerdate = " + this.d;
            }
            this.h = "sms";
            this.j = cursor.getLong(asVar.e);
            this.e = a(this.j);
            this.d = f.a(this.j, context);
            "dividerdate = " + this.d;
        } else if ("mms".equals(str)) {
            this.l = ContentUris.withAppendedId(d.f7a, this.b);
            this.c = cursor.getInt(asVar.m);
            this.m = cursor.getInt(asVar.l);
            this.h = "mms";
            this.o = cursor.getString(asVar.j);
            if (!TextUtils.isEmpty(this.o)) {
                this.o = new w(cursor.getInt(asVar.k), com.sd.a.a.a.a.d.a(this.o)).c();
            }
            if (this.o == null) {
                this.o = this.x.getString(C0000R.string.mms_subject_default);
            }
            this.i = cursor.getString(asVar.m);
            com.sd.a.a.a.a.d a2 = com.sd.a.a.a.a.d.a(this.x);
            if (130 == this.m) {
                p pVar = (p) a2.a(this.l);
                a(pVar.a());
                this.g = new String(pVar.b());
                this.q = (int) pVar.d();
                pVar.c();
            } else {
                "load message:" + this.l;
                l lVar = (l) a2.a(this.l);
                this.p = com.sd.android.mms.a.a.a(context, lVar.i());
                this.n = m.a(this.p);
                if (this.m == 132) {
                    t tVar = (t) lVar;
                    a(tVar.a());
                    this.j = tVar.l() * 1000;
                } else {
                    String string = context.getString(C0000R.string.messagelist_sender_self);
                    this.f = string;
                    this.y = string;
                    this.j = ((j) lVar).l() * 1000;
                }
                com.sd.android.mms.a.h b2 = this.p.get(0);
                if (b2 != null && b2.e()) {
                    if (b2.m().q()) {
                        this.g = this.x.getString(C0000R.string.drm_protected_text);
                    } else {
                        this.g = b2.m().x();
                    }
                }
                this.q = this.p.b();
            }
            this.e = a(this.j);
            this.d = f.a(this.j, context);
        } else {
            throw new com.sd.a.a.a.b("Unknown type of the message: " + str);
        }
        this.f446a = str;
    }

    private String a(long j2) {
        this.D.set(2, 0);
        this.D.set(5, 1);
        this.D.set(11, 0);
        this.D.set(12, 0);
        this.D.set(13, 0);
        this.E.set(11, 0);
        this.E.set(12, 0);
        this.E.set(13, 0);
        if (j2 > this.D.getTime().getTime() && j2 < this.E.getTime().getTime()) {
            this.C = new SimpleDateFormat("MM-dd HH:mm");
        } else if (j2 > this.E.getTime().getTime()) {
            this.C = new SimpleDateFormat("HH:mm:ss");
        }
        return this.C.format(new Date(j2));
    }

    private void a(w wVar) {
        if (wVar != null) {
            this.f = wVar.c();
            this.y = i.a().a(this.x, this.f);
            return;
        }
        String string = this.x.getString(C0000R.string.anonymous_recipient);
        this.f = string;
        this.y = string;
    }

    public final void a(int i2) {
        this.B = i2;
    }

    public final boolean a() {
        return this.A;
    }

    public final int b() {
        return this.c;
    }

    public final int c() {
        return this.n;
    }

    public final void d() {
        this.A = true;
    }

    public final void e() {
        this.A = false;
    }

    public final int f() {
        return this.B;
    }

    public final long g() {
        return this.b;
    }

    public final int h() {
        return this.m;
    }

    public final boolean i() {
        return this.s != null;
    }

    public final int j() {
        return this.v;
    }

    public final String k() {
        return this.o;
    }

    public final void l() {
        this.o = "111111";
        this.u = this.x.getString(C0000R.string.attachment_status_downloading);
        this.v = 3;
        ContentValues contentValues = new ContentValues();
        contentValues.put("subject", this.o);
        this.x.getContentResolver().update(Uri.parse("content://sms/" + this.b), contentValues, null, null);
    }

    public final boolean m() {
        return this.f446a.equals("mms");
    }

    public final boolean n() {
        return this.f446a.equals("sms");
    }

    public final CharSequence o() {
        return this.z;
    }
}
