package com.snda.youni.mms.ui;

import android.content.Context;
import com.sd.android.mms.a.c;
import com.sd.android.mms.a.o;

public abstract class Presenter implements o {

    /* renamed from: a  reason: collision with root package name */
    private int f440a;
    private int b;
    protected final Context c;
    protected at d;
    protected c e;

    public Presenter(Context context, at atVar, c cVar) {
        this.c = context;
        this.d = atVar;
        this.f440a = atVar.getWidth();
        this.b = atVar.getHeight();
        this.e = cVar;
        this.e.c(this);
    }

    public abstract void a();

    public final void a(at atVar) {
        this.d = atVar;
    }

    public final c d() {
        return this.e;
    }
}
