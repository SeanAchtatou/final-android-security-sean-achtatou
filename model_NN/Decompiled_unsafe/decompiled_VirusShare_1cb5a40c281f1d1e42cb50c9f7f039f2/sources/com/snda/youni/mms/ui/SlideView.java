package com.snda.youni.mms.ui;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.net.Uri;
import android.text.method.HideReturnsTransformationMethod;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsoluteLayout;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.VideoView;
import com.snda.youni.C0000R;
import java.io.IOException;

public class SlideView extends AbsoluteLayout implements j {

    /* renamed from: a  reason: collision with root package name */
    private View f442a;
    private ImageView b;
    private VideoView c;
    private ScrollView d;
    private TextView e;
    private c f;
    /* access modifiers changed from: private */
    public MediaPlayer g;
    /* access modifiers changed from: private */
    public boolean h;
    /* access modifiers changed from: private */
    public boolean i;
    /* access modifiers changed from: private */
    public int j;
    /* access modifiers changed from: private */
    public boolean k;
    private MediaPlayer.OnPreparedListener l = new v(this);
    private Context m;

    public SlideView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.m = context;
    }

    /* access modifiers changed from: private */
    public void h() {
        if (this.f442a != null) {
            this.f442a.setVisibility(0);
        }
    }

    /* access modifiers changed from: private */
    public void i() {
        if (this.f442a != null) {
            this.f442a.setVisibility(8);
        }
    }

    public final void a() {
        if (this.g == null || !this.h) {
            this.i = true;
            return;
        }
        this.g.start();
        this.i = false;
        h();
    }

    public final void a(int i2) {
        if (this.g == null || !this.h) {
            this.j = i2;
        } else {
            this.g.seekTo(i2);
        }
    }

    public final void a(int i2, int i3, int i4, int i5) {
        if (this.b != null) {
            this.b.setLayoutParams(new AbsoluteLayout.LayoutParams(i4, i5, i2, i3));
        }
    }

    public final void a(Uri uri) {
        if (this.c == null) {
            this.c = new VideoView(this.m);
            addView(this.c, new AbsoluteLayout.LayoutParams(0, 0, -2, -2));
        }
        this.c.setVideoURI(uri);
    }

    public final void a(Uri uri, String str) {
        if (uri == null) {
            throw new IllegalArgumentException("Audio URI may not be null.");
        }
        if (this.g != null) {
            this.g.reset();
            this.g.release();
            this.g = null;
        }
        this.h = false;
        try {
            this.g = new MediaPlayer();
            this.g.setOnPreparedListener(this.l);
            this.g.setDataSource(this.m, uri);
            this.g.prepareAsync();
        } catch (IOException e2) {
            this.g.release();
            this.g = null;
        }
        if (this.f442a == null) {
            this.f442a = LayoutInflater.from(getContext()).inflate((int) C0000R.layout.playing_audio_info, (ViewGroup) null);
            this.f442a.getHeight();
            ((TextView) this.f442a.findViewById(C0000R.id.name)).setText(str);
            getHeight();
            addView(this.f442a, new AbsoluteLayout.LayoutParams(-1, 82, 0, 0));
        }
        this.f442a.setVisibility(8);
    }

    public final void a(c cVar) {
        this.f = cVar;
    }

    public final void a(String str) {
        if (this.d == null) {
            this.d = new ScrollView(this.m);
            this.d.setScrollBarStyle(50331648);
            addView(this.d, new AbsoluteLayout.LayoutParams(0, 0, -2, -2));
        }
        if (this.e == null) {
            this.e = new TextView(this.m);
            this.e.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
            this.d.addView(this.e);
        }
        this.d.requestFocus();
        this.e.setText(str);
    }

    public final void a(String str, Bitmap bitmap) {
        if (this.b == null) {
            this.b = new ImageView(this.m);
            addView(this.b, new AbsoluteLayout.LayoutParams(0, 0, -2, -2));
        }
        this.b.setImageBitmap(bitmap == null ? BitmapFactory.decodeResource(getResources(), C0000R.drawable.ic_missing_thumbnail_picture) : bitmap);
    }

    public final void a(boolean z) {
        if (this.b != null) {
            this.b.setVisibility(z ? 0 : 4);
        }
    }

    public final void b() {
        if (this.g == null || !this.h) {
            this.k = true;
            return;
        }
        this.g.stop();
        this.g.release();
        this.g = null;
        i();
    }

    public final void b(int i2) {
        if (this.c != null && i2 > 0) {
            this.c.seekTo(i2);
        }
    }

    public final void b(int i2, int i3, int i4, int i5) {
        if (this.d != null) {
            this.d.setLayoutParams(new AbsoluteLayout.LayoutParams(i4, i5, i2, i3));
        }
    }

    public final void b(boolean z) {
        if (this.d != null) {
            this.d.setVisibility(z ? 0 : 4);
        }
    }

    public final void c() {
        if (this.g != null && this.h && this.g.isPlaying()) {
            this.g.pause();
        }
        this.i = false;
    }

    public final void c(int i2, int i3, int i4, int i5) {
        if (this.c != null) {
            this.c.setLayoutParams(new AbsoluteLayout.LayoutParams(i4, i5, i2, i3));
        }
    }

    public final void c(boolean z) {
        if (this.c != null) {
            this.c.setVisibility(z ? 0 : 4);
        }
    }

    public final void d() {
        if (this.c != null) {
            this.c.start();
        }
    }

    public final void e() {
        if (this.c != null) {
            this.c.stopPlayback();
        }
    }

    public final void f() {
        if (this.c != null) {
            this.c.pause();
        }
    }

    public final void g() {
        if (this.d != null) {
            this.d.setVisibility(8);
        }
        if (this.b != null) {
            this.b.setVisibility(8);
        }
        if (this.g != null) {
            b();
        }
        if (this.c != null) {
            e();
            this.c.setVisibility(8);
        }
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int i2, int i3, int i4, int i5) {
        super.onSizeChanged(i2, i3, i4, i5);
        if (this.f != null) {
            this.f.a(i2, i3 - 82);
        }
    }
}
