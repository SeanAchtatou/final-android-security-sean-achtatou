package com.snda.youni.mms.ui;

import com.sd.android.mms.a.a;
import com.sd.android.mms.a.f;

final class z implements c {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ SlideshowPresenter f485a;

    z(SlideshowPresenter slideshowPresenter) {
        this.f485a = slideshowPresenter;
    }

    public final void a(int i, int i2) {
        f c = ((a) this.f485a.e).c();
        this.f485a.f445a = i > 0 ? ((float) c.e()) / ((float) i) : 1.0f;
        this.f485a.b = i2 > 0 ? ((float) c.f()) / ((float) i2) : 1.0f;
    }
}
