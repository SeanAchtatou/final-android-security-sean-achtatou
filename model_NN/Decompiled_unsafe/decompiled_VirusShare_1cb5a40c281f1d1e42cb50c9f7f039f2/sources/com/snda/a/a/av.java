package com.snda.a.a;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import com.snda.a.a.a.a;

public final class av {

    /* renamed from: a  reason: collision with root package name */
    private static final Uri f144a = Uri.parse("content://telephony/carriers/preferapn");
    private static final Uri b = Uri.parse("content://telephony/carriers");
    private static au c;
    private static boolean d = true;

    public static au a(Context context) {
        au b2 = b(context);
        a.c("ApnUtil", "APNWapUtil：preferAPN:" + b2);
        c = b2;
        return b2;
    }

    public static boolean a(String str, au auVar) {
        if ("1".equals(str)) {
            if (auVar != null && "10.0.0.172".equals(auVar.i)) {
                a.a("ApnUtil", "APNWapUtil:current apn is cmwap!");
                d = false;
                return true;
            }
        } else if ("2".equals(str)) {
            if (auVar != null && "10.0.0.172".equals(auVar.i)) {
                a.c("ApnUtil", "APNWapUtil:current apn is uniwap!");
                d = false;
                return true;
            }
        } else if ("3".equals(str)) {
            if (auVar != null && "10.0.0.200".equals(auVar.i)) {
                a.c("ApnUtil", "APNWapUtil:current apn is ctwap!");
                d = false;
                return true;
            }
        }
        return false;
    }

    private static au b(Context context) {
        au auVar;
        au auVar2;
        au auVar3;
        au auVar4;
        try {
            Cursor query = context.getContentResolver().query(f144a, null, null, null, null);
            if (query.moveToFirst()) {
                auVar = null;
                while (true) {
                    try {
                        auVar4 = new au();
                        try {
                            auVar4.f143a = query.getString(query.getColumnIndex("_id"));
                            auVar4.b = query.getString(query.getColumnIndex("name"));
                            auVar4.c = query.getString(query.getColumnIndex("numeric"));
                            auVar4.d = query.getString(query.getColumnIndex("mcc"));
                            auVar4.e = query.getString(query.getColumnIndex("mnc"));
                            auVar4.f = query.getString(query.getColumnIndex("apn"));
                            auVar4.g = query.getString(query.getColumnIndex("user"));
                            auVar4.h = query.getString(query.getColumnIndex("password"));
                            auVar4.i = query.getString(query.getColumnIndex("proxy"));
                            auVar4.j = query.getString(query.getColumnIndex("port"));
                            auVar4.k = query.getString(query.getColumnIndex("mmsproxy"));
                            auVar4.l = query.getString(query.getColumnIndex("mmsport"));
                            auVar4.m = query.getString(query.getColumnIndex("mmsc"));
                            auVar4.n = query.getString(query.getColumnIndex("type"));
                            auVar4.o = query.getString(query.getColumnIndex("current"));
                            if (!query.moveToNext()) {
                                break;
                            }
                            auVar = auVar4;
                        } catch (Exception e) {
                            e = e;
                            auVar = auVar4;
                        }
                    } catch (Exception e2) {
                        e = e2;
                        e.printStackTrace();
                        auVar2 = auVar;
                        a.c("ApnUtil", "APNWapUtil:PREFER APN" + auVar2);
                        return auVar2;
                    }
                }
                auVar3 = auVar4;
            } else {
                auVar3 = null;
            }
            query.close();
            auVar2 = auVar;
        } catch (Exception e3) {
            e = e3;
            auVar = null;
        }
        a.c("ApnUtil", "APNWapUtil:PREFER APN" + auVar2);
        return auVar2;
    }
}
