package com.snda.a.a;

import android.content.Context;
import com.snda.a.a.b.b;
import com.snda.a.a.b.c;

public final class v implements c {

    /* renamed from: a  reason: collision with root package name */
    private String f165a;
    private String b;
    private boolean c = true;
    private Context d;
    private b e;

    public v(String str, String str2, Context context, b bVar) {
        this.f165a = str;
        this.b = str2;
        this.d = context;
        this.e = bVar;
    }

    public final void a(String[] strArr) {
        String a2 = s.a(strArr, 0);
        if ("0".equals(a2)) {
            o.a("Account", this.f165a);
            o.a("Password", this.b);
            String a3 = at.a(this.f165a);
            o.a(this.d, "PTAccount", a3);
            o.a(this.d, "LoginName", a3);
            o.a(this.d, "Mars_Account", a3);
            new aq(this.d, this.e).a();
        } else if ("2".equals(a2)) {
            String a4 = at.a(this.f165a);
            o.a(this.d, "LoginName", a4);
            o.a(this.d, "Mars_Account", a4);
            new aq(this.d, this.e).a();
        } else if ("-2".equals(a2)) {
            this.e.c("{'Message':'" + s.a(strArr, 1) + "'}");
        } else {
            this.e.b("{'Message':'" + s.a(strArr, 1) + "'}");
        }
    }
}
