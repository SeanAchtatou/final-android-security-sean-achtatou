package com.snda.a.a;

import android.content.Context;
import com.snda.a.a.b.a;
import com.snda.a.a.b.c;

public final class w implements c {

    /* renamed from: a  reason: collision with root package name */
    private Context f166a;
    private a b;

    public w(Context context, a aVar) {
        this.f166a = context;
        this.b = aVar;
    }

    public final void a(String[] strArr) {
        String a2 = s.a(strArr, 0);
        if ("-2".equals(a2)) {
            this.b.c("{'Message':'" + s.a(strArr, 1) + "'}");
        } else if ("1".equals(a2) || "-4".equals(a2)) {
            this.b.b("{'Message':'" + s.a(strArr, 1) + "'}");
        } else {
            this.b.a(a2);
        }
    }
}
