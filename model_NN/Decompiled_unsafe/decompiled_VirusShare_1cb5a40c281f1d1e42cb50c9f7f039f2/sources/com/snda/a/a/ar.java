package com.snda.a.a;

import android.content.Context;
import com.snda.a.a.b.c;
import java.util.ArrayList;
import org.apache.http.message.BasicNameValuePair;

public final class ar extends g {
    public ar(Context context, c cVar) {
        super(context, cVar);
    }

    public final void a(String str) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("ProductID", ag.f133a));
        arrayList.add(new BasicNameValuePair("OSName", an.e));
        arrayList.add(new BasicNameValuePair("OSVersion", an.f));
        arrayList.add(new BasicNameValuePair("AppVersion", str));
        arrayList.add(new BasicNameValuePair("Resolution", ax.c(this.f154a)));
        arrayList.add(new BasicNameValuePair("UA", an.c));
        if (b.f147a) {
            arrayList.add(new BasicNameValuePair("pkgType", "b"));
        }
        a("http://211.99.197.56/HurrayWirelessOA/api/query/getUpdateInfo", arrayList);
    }

    public final void a(String str, String[] strArr) {
        if ("http://211.99.197.56/HurrayWirelessOA/api/query/getUpdateInfo".equals(str) && s.a(s.a(strArr, 0))) {
            a(strArr);
        }
    }
}
