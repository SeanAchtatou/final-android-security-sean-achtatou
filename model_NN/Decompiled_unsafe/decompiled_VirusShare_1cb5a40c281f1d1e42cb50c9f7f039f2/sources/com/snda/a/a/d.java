package com.snda.a.a;

import android.content.Context;
import android.os.AsyncTask;
import com.snda.a.a.b.a;

public final class d extends AsyncTask {

    /* renamed from: a  reason: collision with root package name */
    private Context f151a;
    private a b;
    private String c;
    private int d;

    public d(Context context, a aVar) {
        this.f151a = context;
        this.b = aVar;
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ Object doInBackground(Object[] objArr) {
        u a2 = u.a(this.f151a);
        this.d = ((Integer) objArr[0]).intValue();
        this.c = (String) objArr[1];
        com.snda.a.a.a.a.c("aaa", "doInBackground========" + this.c);
        return a2;
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void onPostExecute(Object obj) {
        com.snda.a.a.a.a.c("aaa", "onPostExecute========");
        switch (this.d) {
            case 0:
                String str = this.c;
                Context context = this.f151a;
                a aVar = this.b;
                y.a("login");
                if (context != null) {
                    new ai(context, aVar).a(str);
                    return;
                }
                return;
            case 1:
                new ah(this.f151a, this.b).a(this.c);
                return;
            default:
                return;
        }
    }
}
