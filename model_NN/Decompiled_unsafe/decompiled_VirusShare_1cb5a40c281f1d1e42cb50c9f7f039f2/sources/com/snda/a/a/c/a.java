package com.snda.a.a.c;

import android.content.Context;
import com.snda.a.a.ac;
import com.snda.a.a.an;
import com.snda.a.a.at;
import com.snda.a.a.av;
import com.snda.a.a.ax;
import com.snda.a.a.b;
import com.snda.a.a.d;
import com.snda.a.a.o;
import com.snda.a.a.p;
import com.snda.a.a.q;
import com.snda.a.a.s;
import com.snda.a.a.u;
import com.snda.a.a.y;

public final class a {
    public static void a() {
        b.f147a = true;
    }

    public static void a(Context context, com.snda.a.a.b.b bVar) {
        u.a(context);
        y.a("register");
        String a2 = o.a(context, "Phone");
        if (s.b(a2)) {
            String a3 = o.a(context, "User");
            com.snda.a.a.a.a.c("OpenAPIDelegate", "OpenAPIDelegate:user:" + a3);
            if (s.a(a3)) {
                a2 = ac.a(a3, "PHONE");
            }
            com.snda.a.a.a.a.c("OpenAPIDelegate", "OpenAPIDelegate:phoneNum:" + a2);
        }
        if (s.a(a2)) {
            a2 = at.b(a2);
            if (!ax.a(a2)) {
                com.snda.a.a.a.a.a("OpenAPIDelegate", "OpenAPIDelegate:get mobileNum is messy code or is not phone number");
                a2 = null;
                o.b(context, "User");
                p.a();
            }
        }
        com.snda.a.a.a.a.c("OpenAPIDelegate", "registerForPhoneNum:NatDesEncrypt:" + a2);
        com.snda.a.a.a.a.a("OpenAPIDelegate", "isRegisterForIMSI:" + an.b);
        if (s.a(a2)) {
            y.a("register");
            new q(context, bVar).a(a2, context);
        } else if (an.b) {
            y.a("register");
            new q(context, bVar).a(context);
        } else {
            new q(context, bVar).a();
        }
    }

    public static void a(String str, Context context, com.snda.a.a.b.a aVar) {
        new d(context, aVar).execute(0, str);
    }

    public static boolean a(Context context) {
        u.a(context);
        return av.a(ax.a(context), av.a(context));
    }
}
