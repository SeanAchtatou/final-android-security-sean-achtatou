package com.snda.a.a;

import android.content.Context;
import com.snda.a.a.b.a;
import com.snda.a.a.b.c;

public final class ab implements c {

    /* renamed from: a  reason: collision with root package name */
    private Context f129a;
    private a b;

    public ab(Context context, a aVar) {
        this.f129a = context;
        this.b = aVar;
    }

    public final void a(String[] strArr) {
        String a2 = s.a(strArr, 0);
        if ("1".equals(a2) || "-4".equals(a2)) {
            this.b.b(s.a(strArr, 1));
        } else if ("-2".equals(a2)) {
            this.b.c(s.a(strArr, 1));
        } else {
            this.b.a(a2);
        }
    }
}
