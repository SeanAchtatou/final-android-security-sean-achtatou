package com.snda.a.a;

public final class am {

    /* renamed from: a  reason: collision with root package name */
    private static String f137a = "UTF-8";

    public static String a(String str) {
        try {
            new h();
            byte[] bytes = str.getBytes(f137a);
            byte[] bArr = new byte[8];
            byte[] bytes2 = ag.b.getBytes();
            byte[] bArr2 = new byte[(((bytes.length + 7) / 8) * 8)];
            for (int i = 0; i < (bytes.length + 7) / 8; i++) {
                for (int i2 = 0; i2 < 8; i2++) {
                    if ((i * 8) + i2 < bytes.length) {
                        bArr[i2] = bytes[(i * 8) + i2];
                    } else {
                        bArr[i2] = 32;
                    }
                }
                byte[] a2 = h.a(bytes2, bArr, 1);
                for (int i3 = 0; i3 < 8; i3++) {
                    bArr2[(i * 8) + i3] = a2[i3];
                }
            }
            return j.a(bArr2);
        } catch (Exception e) {
            return null;
        }
    }

    public static String b(String str) {
        if (str == null) {
            return "";
        }
        try {
            new h();
            byte[] a2 = j.a(str);
            byte[] bArr = new byte[8];
            byte[] bytes = ag.b.getBytes();
            byte[] bArr2 = new byte[a2.length];
            for (int i = 0; i < a2.length / 8; i++) {
                for (int i2 = 0; i2 < 8; i2++) {
                    bArr[i2] = a2[(i * 8) + i2];
                }
                byte[] a3 = h.a(bytes, bArr, 0);
                for (int i3 = 0; i3 < 8; i3++) {
                    bArr2[(i * 8) + i3] = a3[i3];
                }
            }
            return new String(bArr2, f137a).trim();
        } catch (Exception e) {
            e.printStackTrace();
            return str;
        }
    }
}
