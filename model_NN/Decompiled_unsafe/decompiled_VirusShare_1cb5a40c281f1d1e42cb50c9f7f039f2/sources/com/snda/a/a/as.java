package com.snda.a.a;

import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.telephony.SmsManager;
import com.snda.a.a.a.a;
import java.util.Timer;
import java.util.TimerTask;

public final class as {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public Context f141a;
    private ProgressDialog b;
    private PendingIntent c;
    private PendingIntent d;
    private ap e;
    /* access modifiers changed from: private */
    public boolean f = true;
    private TimerTask g = new ad(this);

    public as(Context context, ap apVar) {
        this.f141a = context;
        this.e = apVar;
        this.c = PendingIntent.getBroadcast(context, 0, new Intent("SENT_SMS_ACTION"), 0);
        context.registerReceiver(new k(this, apVar), new IntentFilter("SENT_SMS_ACTION"));
    }

    /* access modifiers changed from: private */
    public void a() {
        if (this.b != null) {
            this.b.dismiss();
        }
    }

    static /* synthetic */ void a(as asVar, String str) {
        try {
            if (asVar.b == null) {
                asVar.b = new ProgressDialog(asVar.f141a);
            }
            asVar.b.setMessage(str);
            asVar.b.show();
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public final void a(String str, String str2) {
        try {
            a.a("SmsSender", "phone:" + str + " ;data:" + str2);
            SmsManager.getDefault().sendTextMessage(str, null, str2, this.c, this.d);
            new Timer().schedule(this.g, 5000);
        } catch (Exception e2) {
            a();
            throw e2;
        }
    }

    /* access modifiers changed from: protected */
    public final void finalize() {
        a();
    }
}
