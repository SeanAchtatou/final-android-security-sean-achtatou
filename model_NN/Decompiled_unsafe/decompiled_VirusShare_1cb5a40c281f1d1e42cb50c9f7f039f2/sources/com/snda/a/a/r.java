package com.snda.a.a;

import android.content.Context;
import com.snda.a.a.b.b;
import com.snda.a.a.b.c;

public final class r implements c {

    /* renamed from: a  reason: collision with root package name */
    private String f162a;
    private String b;
    private boolean c = true;
    private Context d;
    private b e;

    public r(String str, String str2, Context context, b bVar) {
        this.f162a = str;
        this.b = str2;
        this.d = context;
        this.e = bVar;
    }

    public final void a(String[] strArr) {
        String a2 = s.a(strArr, 0);
        if (a2 != null && a2.length() > 0) {
            if ("0".equals(a2)) {
                o.a("Password", this.b);
                if (ax.a(this.f162a)) {
                    o.a(this.d, "Phone", at.a(this.f162a));
                    o.a(this.d, "LoginName", at.a(this.f162a));
                }
                o.a(this.d, "NUMAccount", at.a(am.b(s.a(strArr, 1))));
                new aq(this.d, this.e).a();
            } else if ("1".equals(a2)) {
                if (this.c) {
                    new q(this.d, this.e).a();
                } else {
                    this.e.b("{'Message':'register fail!'}");
                }
            } else if ("2".equals(a2)) {
                if (ax.a(this.f162a)) {
                    o.a(this.d, "Phone", at.a(this.f162a));
                }
                new aq(this.d, this.e).a();
            } else if ("-2".equals(a2)) {
                this.e.c("{'Message':'" + s.a(strArr, 1) + "'}");
            } else {
                this.e.b("{'Message':'" + s.a(strArr, 1) + "'}");
            }
        }
    }
}
