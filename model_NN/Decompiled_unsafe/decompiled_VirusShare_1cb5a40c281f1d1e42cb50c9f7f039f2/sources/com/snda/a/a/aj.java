package com.snda.a.a;

import android.os.Environment;
import android.os.StatFs;
import com.snda.a.a.a.a;
import java.io.File;

public final class aj {
    public static File a(String str, boolean z) {
        try {
            if (!a()) {
                return null;
            }
            String str2 = b() + "/snda/sdw/woa/youni/";
            File file = new File(str2);
            if (!file.exists() && z) {
                file.mkdirs();
            }
            String str3 = str2 + str;
            File file2 = new File(str3);
            if (!file2.exists()) {
                if (z) {
                    file2.createNewFile();
                }
                a.c("SDCardUtil", file2.toString());
                Runtime.getRuntime().exec("chmod 777 " + str3);
            }
            a.a("SDCardUtil", new StringBuilder().append("SDCard:write to file:").append(file2).toString() != null ? file2.toString() : "file is null");
            return file2;
        } catch (Exception e) {
            e.printStackTrace();
            throw new e(e.getMessage());
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:19:0x0031 A[SYNTHETIC, Splitter:B:19:0x0031] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String a(java.io.File r5) {
        /*
            r0 = 0
            if (r5 != 0) goto L_0x0009
            r1 = 1
        L_0x0004:
            if (r1 == 0) goto L_0x000b
            java.lang.String r0 = ""
        L_0x0008:
            return r0
        L_0x0009:
            r1 = 0
            goto L_0x0004
        L_0x000b:
            java.io.BufferedReader r1 = new java.io.BufferedReader     // Catch:{ Exception -> 0x0046, all -> 0x0041 }
            java.io.InputStreamReader r2 = new java.io.InputStreamReader     // Catch:{ Exception -> 0x0046, all -> 0x0041 }
            java.io.FileInputStream r3 = new java.io.FileInputStream     // Catch:{ Exception -> 0x0046, all -> 0x0041 }
            r3.<init>(r5)     // Catch:{ Exception -> 0x0046, all -> 0x0041 }
            r2.<init>(r3)     // Catch:{ Exception -> 0x0046, all -> 0x0041 }
            r1.<init>(r2)     // Catch:{ Exception -> 0x0046, all -> 0x0041 }
            java.lang.StringBuffer r0 = new java.lang.StringBuffer     // Catch:{ Exception -> 0x0029 }
            r0.<init>()     // Catch:{ Exception -> 0x0029 }
        L_0x001f:
            java.lang.String r2 = r1.readLine()     // Catch:{ Exception -> 0x0029 }
            if (r2 == 0) goto L_0x0035
            r0.append(r2)     // Catch:{ Exception -> 0x0029 }
            goto L_0x001f
        L_0x0029:
            r0 = move-exception
        L_0x002a:
            r0.printStackTrace()     // Catch:{ all -> 0x002e }
            throw r0     // Catch:{ all -> 0x002e }
        L_0x002e:
            r0 = move-exception
        L_0x002f:
            if (r1 == 0) goto L_0x0034
            r1.close()     // Catch:{ Exception -> 0x003f }
        L_0x0034:
            throw r0
        L_0x0035:
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x0029 }
            r1.close()     // Catch:{ Exception -> 0x003d }
            goto L_0x0008
        L_0x003d:
            r0 = move-exception
            throw r0
        L_0x003f:
            r0 = move-exception
            throw r0
        L_0x0041:
            r1 = move-exception
            r4 = r1
            r1 = r0
            r0 = r4
            goto L_0x002f
        L_0x0046:
            r1 = move-exception
            r4 = r1
            r1 = r0
            r0 = r4
            goto L_0x002a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.snda.a.a.aj.a(java.io.File):java.lang.String");
    }

    /* JADX WARNING: Removed duplicated region for block: B:22:0x0033 A[SYNTHETIC, Splitter:B:22:0x0033] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void a(java.lang.String r4, java.io.File r5) {
        /*
            r0 = 0
            if (r5 != 0) goto L_0x000d
            r1 = 1
        L_0x0004:
            if (r1 != 0) goto L_0x000c
            boolean r1 = com.snda.a.a.s.b(r4)     // Catch:{ Exception -> 0x0028, all -> 0x003c }
            if (r1 == 0) goto L_0x000f
        L_0x000c:
            return
        L_0x000d:
            r1 = 0
            goto L_0x0004
        L_0x000f:
            java.io.BufferedWriter r1 = new java.io.BufferedWriter     // Catch:{ Exception -> 0x0028, all -> 0x003c }
            java.io.PrintWriter r2 = new java.io.PrintWriter     // Catch:{ Exception -> 0x0028, all -> 0x003c }
            r2.<init>(r5)     // Catch:{ Exception -> 0x0028, all -> 0x003c }
            r1.<init>(r2)     // Catch:{ Exception -> 0x0028, all -> 0x003c }
            r1.write(r4)     // Catch:{ Exception -> 0x0041 }
            r1.flush()     // Catch:{ Exception -> 0x0041 }
            r1.close()     // Catch:{ Exception -> 0x0023 }
            goto L_0x000c
        L_0x0023:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x000c
        L_0x0028:
            r1 = move-exception
            r3 = r1
            r1 = r0
            r0 = r3
        L_0x002c:
            r0.printStackTrace()     // Catch:{ all -> 0x0030 }
            throw r0     // Catch:{ all -> 0x0030 }
        L_0x0030:
            r0 = move-exception
        L_0x0031:
            if (r1 == 0) goto L_0x0036
            r1.close()     // Catch:{ Exception -> 0x0037 }
        L_0x0036:
            throw r0
        L_0x0037:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0036
        L_0x003c:
            r1 = move-exception
            r3 = r1
            r1 = r0
            r0 = r3
            goto L_0x0031
        L_0x0041:
            r0 = move-exception
            goto L_0x002c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.snda.a.a.aj.a(java.lang.String, java.io.File):void");
    }

    public static boolean a() {
        return Environment.getExternalStorageState().equals("mounted");
    }

    public static boolean a(String str) {
        if (!a()) {
            return false;
        }
        String str2 = b() + "/snda/sdw/woa/youni/";
        if (!new File(str2).exists()) {
            return false;
        }
        return new File(new StringBuilder().append(str2).append(str).toString()).exists();
    }

    private static String b() {
        if (!a()) {
            return null;
        }
        String absolutePath = Environment.getExternalStorageDirectory().getAbsolutePath();
        if (new StatFs(absolutePath).getAvailableBlocks() < 100) {
            return null;
        }
        return absolutePath;
    }
}
