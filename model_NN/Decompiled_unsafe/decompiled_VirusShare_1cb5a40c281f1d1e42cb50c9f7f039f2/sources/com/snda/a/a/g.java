package com.snda.a.a;

import android.content.Context;
import com.snda.a.a.b.c;
import java.util.List;
import org.apache.http.message.BasicNameValuePair;

public abstract class g implements i {
    /* access modifiers changed from: protected */

    /* renamed from: a  reason: collision with root package name */
    public Context f154a;
    private c b;

    protected g(Context context, c cVar) {
        this.f154a = context;
        this.b = cVar;
    }

    /* access modifiers changed from: protected */
    public final void a(String str, List list) {
        a(str, list, "connection .... ");
    }

    /* access modifiers changed from: protected */
    public final void a(String str, List list, String str2) {
        if (an.d != null) {
            list.add(new BasicNameValuePair("muuid", an.d));
        }
        new a(this.f154a, this).execute(str, list, str2, false);
    }

    public void a(String str, String[] strArr) {
        a(strArr);
    }

    /* access modifiers changed from: protected */
    public final void a(String[] strArr) {
        this.b.a(strArr);
    }

    /* access modifiers changed from: protected */
    public final void b(String str, List list) {
        a(str, list, "connection .... ");
    }
}
