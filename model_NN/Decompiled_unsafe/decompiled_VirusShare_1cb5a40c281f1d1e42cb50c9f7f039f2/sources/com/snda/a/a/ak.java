package com.snda.a.a;

import android.content.Context;
import com.snda.a.a.b.b;
import com.snda.a.a.b.c;
import java.util.HashMap;

public final class ak implements c {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public Context f136a;
    private b b;

    public ak(Context context, b bVar) {
        this.f136a = context;
        this.b = bVar;
    }

    private String a(String str, String str2, String str3) {
        HashMap hashMap = new HashMap();
        hashMap.put("PTAccount", str);
        hashMap.put("NumAccount", str3);
        hashMap.put("MobileNum", str2);
        hashMap.put("ProductId", ag.f133a);
        hashMap.put("Message", "query user info success!");
        new Thread(new t(this)).start();
        return al.a(hashMap);
    }

    public final void a(String[] strArr) {
        String a2 = s.a(strArr, 0);
        if ("0".equals(a2)) {
            String b2 = am.b(s.a(strArr, 1));
            if (ax.a(b2)) {
                o.a(this.f136a, "Phone", at.a(b2));
                o.a(this.f136a, "LoginName", at.a(b2));
            }
            String b3 = am.b(s.a(strArr, 2));
            o.a(this.f136a, "PTAccount", at.a(b3));
            this.b.a(a(b3, b2, am.b(s.a(strArr, 3))));
        } else if ("3".equals(a2)) {
            String b4 = am.b(s.a(strArr, 1));
            if (ax.a(b4)) {
                o.a("Account", b4);
                o.a(this.f136a, "Phone", at.a(b4));
                o.a(this.f136a, "LoginName", at.a(b4));
            }
            new aq(this.f136a, this.b).a();
        } else if ("4".equals(a2)) {
            String b5 = am.b(s.a(strArr, 1));
            if (ax.a(b5)) {
                o.a("Account", b5);
                o.a(this.f136a, "Phone", at.a(b5));
                o.a(this.f136a, "LoginName", at.a(b5));
            }
            String b6 = am.b(s.a(strArr, 3));
            o.a(this.f136a, "PTAccount", at.a(b6));
            this.b.a(a(b6, b5, am.b(s.a(strArr, 4))));
        } else if ("-2".equals(a2) || "-4".equals(a2)) {
            this.b.c("{'Message':'" + s.a(strArr, 1) + "'}");
        } else {
            this.b.b("{'Message':'" + s.a(strArr, 1) + "'}");
        }
    }
}
