package com.snda.a.a;

import android.content.Context;
import android.util.Log;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public final class ac {

    /* renamed from: a  reason: collision with root package name */
    private static List f130a;
    private static String b = "USER";
    private static String c = "carrier";

    public static String a(Context context) {
        List list;
        String str = "";
        try {
            String str2 = (String) o.a("imsi_carrier");
            if (str2 == null) {
                str2 = o.a(context, "imsi_carrier");
            }
            if (str2 != null) {
                list = a(context, new String[]{"IMSI", "imsi", str2});
            } else {
                list = null;
            }
            if (list != null) {
                for (int i = 0; i < list.size(); i++) {
                    String str3 = new String(((String) list.get(i)).getBytes("UTF-8"));
                    if (s.a(str3)) {
                        str = new JSONObject(str3).optString("carrier");
                    }
                }
            }
            return str;
        } catch (Exception e) {
            throw e;
        }
    }

    public static String a(String str, String str2) {
        try {
            if (s.a(str)) {
                JSONArray optJSONArray = new JSONObject(str).optJSONArray(b);
                if (optJSONArray != null && !"".equals(optJSONArray) && !"null".equals(optJSONArray) && !" ".equals(optJSONArray)) {
                    int length = optJSONArray.length();
                    JSONObject jSONObject = null;
                    int i = 0;
                    while (i < length) {
                        i++;
                        jSONObject = (JSONObject) optJSONArray.opt(i);
                    }
                    if (jSONObject != null) {
                        return jSONObject.optString(str2);
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return "";
    }

    private static List a(Context context, String[] strArr) {
        try {
            if (f130a != null) {
                f130a.clear();
            }
            f130a = new ArrayList();
            String b2 = b(context);
            if (s.b(b2)) {
                return null;
            }
            JSONArray optJSONArray = new JSONObject(b2).optJSONArray(strArr[0]);
            if (optJSONArray == null) {
                return null;
            }
            int length = optJSONArray.length();
            for (int i = 0; i < length; i++) {
                String optString = optJSONArray.optString(i);
                if (s.b(optString)) {
                    break;
                }
                if (new JSONObject(optString).optString(strArr[1]).equals(strArr[2])) {
                    f130a.add(optString);
                    Log.i("JSONUtil", "get ConfigJson List:" + optString);
                }
            }
            return f130a;
        } catch (Exception e) {
            throw e;
        }
    }

    public static void a(String str, File file) {
        try {
            if (!s.b(str)) {
                JSONObject jSONObject = new JSONObject(str);
                if (jSONObject.has(p.f160a)) {
                    jSONObject.remove(p.f160a);
                }
                aj.a(jSONObject.toString(), file);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static String b(Context context) {
        try {
            String a2 = o.a(context, "configJsonData");
            if (!s.b(a2)) {
                return a2;
            }
            o.a(context, "configJsonData", "{'mobileCard':[{'carrier':'1','cost':3000,'bid':'33280'},{'carrier':'1','cost':5000,'bid':'33281'},{'carrier':'1','cost':10000,'bid':'33282'},{'carrier':'1','cost':30000,'bid':'33283'},{'carrier':'1','cost':50000,'bid':'33284'},{'carrier':'2','cost':2000,'bid':'33285'},{'carrier':'2','cost':3000,'bid':'33286'},{'carrier':'2','cost':5000,'bid':'33287'},{'carrier':'2','cost':10000,'bid':'33288'},{'carrier':'2','cost':20000,'bid':'33289'},{'carrier':'2','cost':30000,'bid':'33290'},{'carrier':'2','cost':50000,'bid':'33291'},{'carrier':'3','cost':5000,'bid':'33292'},{'carrier':'3','cost':10000,'bid':'33293'}],'mobileFee':[{'real':500,'carrier':'1','cost':800,'bid':'134'},{'real':1300,'carrier':'1','cost':2000,'bid':'123'},{'real':2000,'carrier':'1','cost':3000,'bid':'106'},{'real':500,'carrier':'2','cost':800,'bid':'2739'},{'real':1300,'carrier':'2','cost':2000,'bid':'2740'},{'real':2000,'carrier':'2','cost':3000,'bid':'2741'}],'smsCode':{'mobileEditPwd':[{'cmd':'SJM','spNo':'106575160882','carrier':'1'},{'cmd':'SJM','spNo':'1065502180988','carrier':'2'},{'cmd':'SJM','spNo':'10690882','carrier':'3'}],'mobileBind':[{'cmd':'SJB','spNo':'106575160882','carrier':'1'},{'cmd':'SJB','spNo':'1065502180988','carrier':'2'},{'cmd':'SJB','spNo':'10690882','carrier':'3'}]},'wl':[{'decodeMethod':'CMWL1','parameter':'Location','resPos':0,'carrierId':'1','resCode':'302','needDecode':1,'url':'http://202.152.188.7/moua_red.php?url=http://3g.3guu.com/'},{'decodeMethod':'CMWL1','parameter':'Location','resPos':0,'carrierId':'2','resCode':'302','needDecode':1,'url':'http://202.152.188.7/moua_red.php?url=http://3g.3guu.com/'},{'decodeMethod':' ','parameter':' ','resPos':1,'carrierId':'3','resCode':'200','needDecode':0,'url':'http://test.woa.hurray.com.cn/HurrayWirelessOA/api/whitelist/getctnum'}],'IMSI':[{'carrier':'1','imsi':'20'},{'carrier':'3','imsi':'05'},{'carrier':'2','imsi':'06'},{'carrier':'1','imsi':'07'},{'carrier':'1','imsi':'00'},{'carrier':'2','imsi':'01'},{'carrier':'1','imsi':'02'},{'carrier':'3','imsi':'03'}],'feeNotice':50,'version':{'UpdateType':'3','DownloadUrl':''}}");
            return o.a(context, "configJsonData");
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }
}
