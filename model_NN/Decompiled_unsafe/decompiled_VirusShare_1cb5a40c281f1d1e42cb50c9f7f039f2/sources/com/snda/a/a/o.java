package com.snda.a.a;

import android.content.Context;
import android.content.SharedPreferences;
import com.snda.a.a.a.a;
import java.util.Hashtable;

public final class o {

    /* renamed from: a  reason: collision with root package name */
    private static final Hashtable f159a = new Hashtable();
    private static final Hashtable b = new Hashtable();

    public static Object a(String str) {
        return b.get(str);
    }

    public static String a(Context context, String str) {
        return context.getSharedPreferences("UserInfo", 2).getString(str, null);
    }

    public static void a() {
        b.clear();
    }

    public static void a(Context context) {
        SharedPreferences.Editor edit = context.getSharedPreferences("UserInfo", 2).edit();
        edit.clear();
        edit.commit();
    }

    public static synchronized void a(Context context, String str, String str2) {
        synchronized (o.class) {
            a.a("CacheUtil", "save permanent data. key(" + str + "); value(" + str2 + ")");
            SharedPreferences.Editor edit = context.getSharedPreferences("UserInfo", 2).edit();
            edit.putString(str, str2);
            edit.commit();
        }
    }

    public static void a(String str, Object obj) {
        if (obj != null && obj.toString().trim().length() != 0) {
            b.put(str, obj);
        }
    }

    public static void b() {
        f159a.clear();
    }

    public static void b(Context context, String str) {
        SharedPreferences.Editor edit = context.getSharedPreferences("UserInfo", 2).edit();
        edit.remove(str);
        edit.commit();
    }
}
