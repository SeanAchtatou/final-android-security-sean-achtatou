package com.snda.a.a;

import android.telephony.CellLocation;

public final class z extends CellLocation {

    /* renamed from: a  reason: collision with root package name */
    private int f168a;
    private int b;
    private int c;
    private int d;
    private int e;

    public z() {
        this.f168a = -1;
        this.b = Integer.MAX_VALUE;
        this.c = Integer.MAX_VALUE;
        this.d = -1;
        this.e = -1;
        this.f168a = -1;
        this.b = Integer.MAX_VALUE;
        this.c = Integer.MAX_VALUE;
        this.d = -1;
        this.e = -1;
    }

    private static boolean a(Object obj, Object obj2) {
        return obj == null ? obj2 == null : obj.equals(obj2);
    }

    public final int a() {
        return this.f168a;
    }

    public final boolean equals(Object obj) {
        try {
            z zVar = (z) obj;
            if (obj == null) {
                return false;
            }
            return a(Integer.valueOf(this.f168a), Integer.valueOf(zVar.f168a)) && a(Integer.valueOf(this.b), Integer.valueOf(zVar.b)) && a(Integer.valueOf(this.c), Integer.valueOf(zVar.c)) && a(Integer.valueOf(this.d), Integer.valueOf(zVar.d)) && a(Integer.valueOf(this.e), Integer.valueOf(zVar.e));
        } catch (ClassCastException e2) {
            return false;
        }
    }

    public final int hashCode() {
        return (((this.f168a ^ this.b) ^ this.c) ^ this.d) ^ this.e;
    }

    public final String toString() {
        return "[" + this.f168a + "," + this.b + "," + this.c + "," + this.d + "," + this.e + "]";
    }
}
