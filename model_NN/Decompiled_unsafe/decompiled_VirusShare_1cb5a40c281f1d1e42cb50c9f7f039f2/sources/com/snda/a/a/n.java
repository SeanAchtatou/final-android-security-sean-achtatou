package com.snda.a.a;

import android.content.Context;
import com.snda.a.a.a.a;
import com.snda.a.a.b.c;
import java.util.ArrayList;
import org.apache.http.message.BasicNameValuePair;

public final class n extends g {
    boolean b = true;
    private boolean c = false;
    private String d;
    private String e;
    /* access modifiers changed from: private */
    public String f;
    private boolean g;
    private int h;

    public n(Context context, c cVar) {
        super(context, cVar);
    }

    private void b() {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("ProductID", ag.f133a));
        arrayList.add(new BasicNameValuePair("UA", an.c));
        a("http://211.99.197.56/HurrayWirelessOA/api/reg/getseq", arrayList);
    }

    static /* synthetic */ void d(n nVar) {
        String a2 = o.a(nVar.f154a, "Reg_SMS_ThreeTime");
        if (s.b(a2)) {
            o.a(nVar.f154a, "Reg_SMS_ThreeTime", String.valueOf(1));
            return;
        }
        int intValue = Integer.valueOf(a2).intValue() + 1;
        a.c("RegisterHandle", "RegisterSMSHandle::Send registered SMS is ok :[" + intValue + "]times");
        o.a(nVar.f154a, "Reg_SMS_ThreeTime", String.valueOf(intValue));
    }

    public final void a() {
        this.c = true;
        b();
    }

    public final void a(String str, String str2) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("ProductID", ag.f133a));
        arrayList.add(new BasicNameValuePair("Phone", am.a(str)));
        arrayList.add(new BasicNameValuePair("Pwd", am.a(str2)));
        arrayList.add(new BasicNameValuePair("UA", an.c));
        a("http://211.99.197.56/HurrayWirelessOA/api/reg/phone", arrayList);
    }

    public final void a(String str, boolean z) {
        this.g = z;
        this.h++;
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("ProductID", ag.f133a));
        if (s.a(str)) {
            this.e = am.a(str);
        }
        if (s.b(this.e)) {
            this.c = true;
            b();
        }
        arrayList.add(new BasicNameValuePair("UUID", this.e));
        a.a("RegisterHandle", "registerQuery:params:UUID=" + this.e);
        a("http://211.99.197.56/HurrayWirelessOA/api/reg/query", arrayList, "正在获取注册信息...");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.snda.a.a.n.a(java.lang.String, boolean):void
     arg types: [?[OBJECT, ARRAY], int]
     candidates:
      com.snda.a.a.n.a(com.snda.a.a.n, java.lang.String[]):void
      com.snda.a.a.n.a(java.lang.String, java.lang.String):void
      com.snda.a.a.n.a(java.lang.String, java.lang.String[]):void
      com.snda.a.a.g.a(java.lang.String, java.util.List):void
      com.snda.a.a.g.a(java.lang.String, java.lang.String[]):void
      com.snda.a.a.i.a(java.lang.String, java.lang.String[]):void
      com.snda.a.a.n.a(java.lang.String, boolean):void */
    public final void a(String str, String[] strArr) {
        String str2;
        String str3;
        String a2 = s.a(strArr, 0);
        if (!s.a(a2)) {
            return;
        }
        if ("http://211.99.197.56/HurrayWirelessOA/api/reg/getseq".equals(str)) {
            if (!"0".equals(a2) || !this.c) {
                a(strArr);
                return;
            }
            this.f = s.a(strArr, 1);
            this.e = am.a(this.f);
            this.d = s.a(strArr, 8);
            String a3 = ax.a(this.f154a);
            o.a("carriersId", a3);
            if ("1".equals(a3) || "4".equals(a3)) {
                str2 = s.a(strArr, 2);
                str3 = s.a(strArr, 3) + " " + this.f + " " + this.d + " " + ag.f133a;
            } else if ("2".equals(a3)) {
                str2 = s.a(strArr, 4);
                str3 = s.a(strArr, 5) + " " + this.f + " " + this.d + " " + ag.f133a;
            } else if ("3".equals(a3)) {
                str2 = s.a(strArr, 6);
                str3 = s.a(strArr, 7) + " " + this.f + " " + this.d + " " + ag.f133a;
            } else {
                str3 = null;
                str2 = null;
            }
            if (str2 == null) {
                a(new String[]{"-3", "无法获取运营商信息"});
                return;
            }
            a.c("RegisterHandle", "RegisterSMSHandle::start Send registered SMS !");
            try {
                new as(this.f154a, new aa(this)).a(str2, str3);
            } catch (Exception e2) {
                a(new String[]{"1", "sendSMS error!"});
            }
        } else if ("http://211.99.197.56/HurrayWirelessOA/api/reg/phone".equals(str)) {
            a(strArr);
        } else if (!"http://211.99.197.56/HurrayWirelessOA/api/reg/query".equals(str)) {
        } else {
            if ("0".equals(a2)) {
                o.a("Password", am.b(this.d));
                a(strArr);
            } else if ("3".equals(a2) || "4".equals(a2)) {
                o.a("Password", am.b(this.d));
                a(strArr);
            } else if ("2".equals(a2)) {
                if (!this.g) {
                    a.c("RegisterHandle", "RegisterSMSHandld:Unregistered please wait:one times[" + (!this.g) + "]");
                    a(strArr);
                } else if (this.h == 30) {
                    o.a("UUID", this.e);
                    a(strArr);
                } else {
                    try {
                        Thread.sleep(3000);
                    } catch (InterruptedException e3) {
                    }
                    a.c("RegisterHandle", ":Unregistered please wait:ten times[" + (!this.g) + "]");
                    a((String) null, true);
                }
            } else if (!"-4".equals(a2)) {
                a(strArr);
            } else if (!this.g) {
                a.c("RegisterHandle", "RegisterSMSHandld:Unregistered please wait:one times[" + (!this.g) + "]");
                a(strArr);
            } else if (this.h == 5) {
                o.a("UUID", this.e);
                a(strArr);
            } else {
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException e4) {
                }
                a.c("RegisterHandle", "RegisterSMSHandld:Unregistered please wait:ten times[" + (!this.g) + "]");
                a((String) null, true);
            }
        }
    }
}
