package com.snda.a.a;

import android.content.Context;
import com.snda.a.a.a.a;
import java.io.IOException;
import java.io.InputStream;

public final class ag {

    /* renamed from: a  reason: collision with root package name */
    public static String f133a;
    public static String b = "G96Q1K7s";

    public static void a(Context context) {
        a.c("InitSecret", "start license.key :[" + an.c + "]");
        try {
            if (f133a == null || b == null) {
                String[] b2 = b(context);
                if (b2 != null && b2.length > 1) {
                    f133a = b2[0].split("=")[1];
                    b = b2[1].split("=")[1];
                }
                a.a("InitSecret", "productId:" + f133a + " ; staticKey:" + b);
            }
            a.c("InitSecret", "finish license.key :[" + f133a + ":" + b + "]");
        } catch (Exception e) {
            a.b("InitSecret", "LicenceKeyException:" + e.getMessage());
            throw new aw("LicenceKeyException:" + e.getMessage());
        }
    }

    private static String[] b(Context context) {
        try {
            InputStream open = context.getAssets().open("license.key");
            byte[] bArr = new byte[open.available()];
            open.read(bArr);
            String b2 = am.b(new String(bArr));
            a.a("InitSecret", b2);
            if (b2 == null) {
                return null;
            }
            String[] split = b2.split("\n");
            if (split.length <= 1) {
                return null;
            }
            return split;
        } catch (IOException e) {
            e.printStackTrace();
            a.b("InitSecret", "LicenceKeyException:" + e.getMessage());
            throw new aw("LicenceKeyException:" + e.getMessage());
        }
    }
}
