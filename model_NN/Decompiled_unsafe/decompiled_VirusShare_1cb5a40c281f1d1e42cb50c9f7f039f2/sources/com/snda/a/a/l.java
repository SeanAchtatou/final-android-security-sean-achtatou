package com.snda.a.a;

import android.content.Context;
import com.snda.a.a.b.c;
import java.util.ArrayList;
import org.apache.http.message.BasicNameValuePair;

public final class l extends g {
    private static volatile c d = c.PENDING;
    /* access modifiers changed from: private */
    public Context b;
    private c c;

    public l(Context context, c cVar) {
        super(context, cVar);
        this.c = cVar;
        this.b = context;
    }

    public final void a(String str) {
        d = c.PENDING;
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("ProductID", ag.f133a));
        arrayList.add(new BasicNameValuePair("ReqStr", an.c));
        arrayList.add(new BasicNameValuePair("OSName", an.e));
        arrayList.add(new BasicNameValuePair("OSVersion", an.f));
        arrayList.add(new BasicNameValuePair("AppVersion", str));
        arrayList.add(new BasicNameValuePair("Resolution", ax.c(this.b)));
        arrayList.add(new BasicNameValuePair("OAVersion", "1.0"));
        a("http://211.99.197.56/HurrayWirelessOA/api/query/config_1_2_9", arrayList);
    }

    public final void a(String str, String[] strArr) {
        if ("http://211.99.197.56/HurrayWirelessOA/api/query/config_1_2_9".equals(str)) {
            d = c.RUNNING;
            String a2 = s.a(strArr, 0);
            if (s.a(a2)) {
                if (!"-2".equals(a2) && !"1".equals(a2) && !"-4".equals(a2)) {
                    new Thread(new f(this, strArr[0])).start();
                    d = c.FINISHED;
                }
                a(strArr);
            }
        }
    }
}
