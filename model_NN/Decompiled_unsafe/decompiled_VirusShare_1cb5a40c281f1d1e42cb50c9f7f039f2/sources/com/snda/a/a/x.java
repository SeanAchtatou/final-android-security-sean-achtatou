package com.snda.a.a;

import android.content.Context;
import com.snda.a.a.b.c;
import java.util.ArrayList;
import org.apache.http.message.BasicNameValuePair;

public final class x extends g {
    private String b;
    private String c;

    public x(Context context, c cVar) {
        super(context, cVar);
    }

    public final void a(String str, String str2) {
        this.b = str;
        this.c = str2;
        an.f138a = true;
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("ProductID", ag.f133a));
        arrayList.add(new BasicNameValuePair("Account", am.a(str)));
        arrayList.add(new BasicNameValuePair("Pwd", am.a(str2)));
        arrayList.add(new BasicNameValuePair("UA", an.c));
        a("http://211.99.197.56/HurrayWirelessOA/api/reg/pt", arrayList);
    }

    public final void a(String str, String[] strArr) {
        if ("http://211.99.197.56/HurrayWirelessOA/api/reg/pt".equals(str)) {
            if (s.a(s.a(strArr, 0))) {
                a(strArr);
            }
        } else if ("http://211.99.197.56/HurrayWirelessOA/api/reg/abroadphone".equals(str) && s.a(s.a(strArr, 0))) {
            a(strArr);
        }
    }
}
