package com.snda.a.a;

public final class j {

    /* renamed from: a  reason: collision with root package name */
    private static String f156a = "12345678901234567890";

    public static final String a(byte[] bArr) {
        StringBuffer stringBuffer = new StringBuffer();
        for (byte b : bArr) {
            char[] cArr = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
            stringBuffer.append(new String(new char[]{cArr[(b >>> 4) & 15], cArr[b & 15]}));
        }
        return stringBuffer.toString();
    }

    public static byte[] a(String str) {
        if (str.length() % 2 == 0) {
            byte[] bytes = str.getBytes();
            int length = str.length() >> 1;
            byte[] bArr = new byte[length];
            for (int i = 0; i < length * 2; i++) {
                int i2 = i >> 1;
                bArr[i2] = (byte) ((Character.digit((char) bytes[i + 0], 16) << (i % 2 == 1 ? 0 : 4)) | bArr[i2]);
            }
            return bArr;
        }
        throw new RuntimeException("Uneven number(" + str.length() + ") of hex digits passed to hex2byte.");
    }
}
