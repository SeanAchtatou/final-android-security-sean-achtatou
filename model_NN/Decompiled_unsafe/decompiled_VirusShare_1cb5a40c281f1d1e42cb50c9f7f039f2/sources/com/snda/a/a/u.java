package com.snda.a.a;

import android.content.Context;

public final class u {

    /* renamed from: a  reason: collision with root package name */
    private static u f164a;

    private u() {
    }

    public static synchronized u a(Context context) {
        u uVar;
        synchronized (u.class) {
            if (f164a == null) {
                f164a = new u();
            }
            if (context != null) {
                if (ag.f133a == null || ag.b == null) {
                    ag.a(context);
                    at.a();
                }
                if (an.c == null) {
                    an.a(context);
                }
            }
            uVar = f164a;
        }
        return uVar;
    }
}
