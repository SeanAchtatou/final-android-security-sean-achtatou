package com.snda.a.a;

import android.content.Context;
import android.telephony.TelephonyManager;
import com.snda.a.a.a.a;
import java.util.regex.Pattern;

public final class ax {

    /* renamed from: a  reason: collision with root package name */
    private static boolean f146a = false;

    public static String a() {
        return String.valueOf(Math.random()).substring(2, 8);
    }

    public static String a(Context context) {
        String str = null;
        try {
            String str2 = (String) o.a("carriersId");
            try {
                if (!s.b(str2)) {
                    return str2;
                }
                String a2 = o.a(context, "carriersId");
                return s.b(a2) ? ac.a(context) : a2;
            } catch (Exception e) {
                Exception exc = e;
                str = str2;
                e = exc;
                e.printStackTrace();
                return str;
            }
        } catch (Exception e2) {
            e = e2;
            e.printStackTrace();
            return str;
        }
    }

    public static boolean a(String str) {
        if (s.b(str)) {
            return false;
        }
        return Pattern.compile("1\\d{10}").matcher(str).matches();
    }

    public static String b(Context context) {
        String a2 = o.a(context, "imsi");
        if (!s.b(a2)) {
            return a2;
        }
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
        return telephonyManager.getSimState() == 5 ? telephonyManager.getSubscriberId() : a2;
    }

    public static String c(Context context) {
        int i = context.getResources().getDisplayMetrics().widthPixels;
        return i + "|" + context.getResources().getDisplayMetrics().heightPixels;
    }

    public static boolean d(Context context) {
        String a2 = o.a(context, "UUID_TimeStamp");
        Long valueOf = Long.valueOf(System.currentTimeMillis());
        if (s.b(a2)) {
            a.c("OAUtil", "OAUtil:@@@@@@@@@@@@@@@@@:uuid is null");
            return false;
        }
        if (valueOf.longValue() - Long.valueOf(a2).longValue() > 3600000) {
            o.b(context, "UUID");
            o.b(context, "UUID_TimeStamp");
            a.c("OAUtil", "OAUtil:@@@@@@@@@@@@@@@@@:uuid  more than an hour");
            return false;
        }
        a.c("OAUtil", "OAUtil:@@@@@@@@@@@@@@@@@:uuid in an hour");
        return true;
    }

    public static boolean e(Context context) {
        String a2 = o.a(context, "Reg_SMS_ThreeTime");
        if (s.b(a2)) {
            a.c("OAUtil", "OAUtil:@@@@@@@@@@@@@@@@@:Send registered SMS zero times ");
            return true;
        }
        int intValue = Integer.valueOf(a2).intValue();
        if (intValue >= 4) {
            return false;
        }
        a.c("OAUtil", "OAUtil:@@@@@@@@@@@@@@@@@:Send registered SMS  : [" + intValue + " ] times");
        return true;
    }
}
