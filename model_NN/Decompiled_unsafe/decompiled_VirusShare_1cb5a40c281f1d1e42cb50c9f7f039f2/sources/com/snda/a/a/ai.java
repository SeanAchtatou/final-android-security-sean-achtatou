package com.snda.a.a;

import android.content.Context;
import com.snda.a.a.b.a;
import java.lang.ref.WeakReference;

public final class ai {

    /* renamed from: a  reason: collision with root package name */
    private WeakReference f135a;
    private a b;

    public ai(Context context, a aVar) {
        this.f135a = new WeakReference(context);
        this.b = aVar;
    }

    public final void a(String str) {
        new l((Context) this.f135a.get(), new w((Context) this.f135a.get(), this.b)).a(str);
    }
}
