package com.snda.a.a;

import com.snda.a.a.a.a;

public final class at {

    /* renamed from: a  reason: collision with root package name */
    private static String f142a = "NatDesEncrypt";
    private static String b = "UTF-8";
    private static String c;
    private static String d = "G96Q1K7s";
    private static String e = "B8DB53889B32D45DEE4DB30CD60D717AF02926D376BCC5F0D1299548FD496C4A0051DB72ACA1955D";

    public static String a(String str) {
        try {
            new h();
            byte[] bytes = str.getBytes(b);
            byte[] bArr = new byte[8];
            byte[] bytes2 = d.getBytes();
            byte[] bArr2 = new byte[(((bytes.length + 7) / 8) * 8)];
            for (int i = 0; i < (bytes.length + 7) / 8; i++) {
                for (int i2 = 0; i2 < 8; i2++) {
                    if ((i * 8) + i2 < bytes.length) {
                        bArr[i2] = bytes[(i * 8) + i2];
                    } else {
                        bArr[i2] = 32;
                    }
                }
                byte[] a2 = h.a(bytes2, bArr, 1);
                for (int i3 = 0; i3 < 8; i3++) {
                    bArr2[(i * 8) + i3] = a2[i3];
                }
            }
            return j.a(bArr2);
        } catch (Exception e2) {
            return null;
        }
    }

    public static void a() {
        try {
            if (c == null || d == null) {
                String[] b2 = b();
                if (b2 != null && b2.length > 1) {
                    c = b2[0].split("=")[1];
                    d = b2[1].split("=")[1];
                }
                a.a(f142a, "DesEncryptId:" + c + " ; DesEncryptIdKey:" + d);
            }
            a.c(f142a, "finish DesEncrypt Data :[" + c + ":" + d + "]");
        } catch (Exception e2) {
            a.b(f142a, "LicenceKeyException:" + e2.getMessage());
            throw new aw("LicenceKeyException:" + e2.getMessage());
        }
    }

    public static String b(String str) {
        if (str == null) {
            return "";
        }
        try {
            new h();
            byte[] a2 = j.a(str);
            byte[] bArr = new byte[8];
            byte[] bytes = d.getBytes();
            byte[] bArr2 = new byte[a2.length];
            for (int i = 0; i < a2.length / 8; i++) {
                for (int i2 = 0; i2 < 8; i2++) {
                    bArr[i2] = a2[(i * 8) + i2];
                }
                byte[] a3 = h.a(bytes, bArr, 0);
                for (int i3 = 0; i3 < 8; i3++) {
                    bArr2[(i * 8) + i3] = a3[i3];
                }
            }
            return new String(bArr2, b).trim();
        } catch (Exception e2) {
            e2.printStackTrace();
            return str;
        }
    }

    private static String[] b() {
        String[] strArr;
        try {
            String b2 = b(e);
            a.a(f142a, b2);
            if (b2 == null) {
                return null;
            }
            String[] split = b2.split("\n");
            try {
                if (split.length <= 1) {
                    return null;
                }
                return split;
            } catch (Exception e2) {
                Exception exc = e2;
                strArr = split;
                e = exc;
                e.printStackTrace();
                return strArr;
            }
        } catch (Exception e3) {
            e = e3;
            strArr = null;
            e.printStackTrace();
            return strArr;
        }
    }
}
