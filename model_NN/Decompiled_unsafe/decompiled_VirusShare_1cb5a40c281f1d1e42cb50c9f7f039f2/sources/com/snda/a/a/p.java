package com.snda.a.a;

import android.content.Context;
import com.snda.a.a.a.a;
import java.io.File;
import org.json.JSONObject;

public final class p {

    /* renamed from: a  reason: collision with root package name */
    public static String f160a = "USER";
    private static String b = "SDCardData";

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.snda.a.a.aj.a(java.lang.String, boolean):java.io.File
     arg types: [java.lang.String, int]
     candidates:
      com.snda.a.a.aj.a(java.lang.String, java.io.File):void
      com.snda.a.a.aj.a(java.lang.String, boolean):java.io.File */
    public static void a() {
        try {
            File a2 = aj.a("sndawoa.dat", false);
            String a3 = aj.a(a2);
            if (!s.b(a3) && !new JSONObject(a3).isNull(f160a)) {
                ac.a(a3, a2);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.snda.a.a.aj.a(java.lang.String, boolean):java.io.File
     arg types: [java.lang.String, int]
     candidates:
      com.snda.a.a.aj.a(java.lang.String, java.io.File):void
      com.snda.a.a.aj.a(java.lang.String, boolean):java.io.File */
    public static void a(Context context) {
        try {
            File a2 = aj.a("sndawoa.dat", true);
            String a3 = aj.a(a2);
            String a4 = o.a(context, "PTAccount");
            String a5 = o.a(context, "NUMAccount");
            String a6 = o.a(context, "Phone");
            String b2 = ax.b(context);
            if (s.a(a6)) {
                if (ax.a(at.b(a6))) {
                    a3 = "{'USER':[{'PTACCOUNT':'" + a4 + "','NUMACCOUNT':'" + a5 + "','PHONE':'" + a6 + "','IMSI':'" + b2 + "','PRODUCTID':'" + ag.f133a + "','STATUS':'" + "1" + "'}]}";
                    aj.a(a3, a2);
                    o.a(context, "User", a3);
                } else {
                    return;
                }
            }
            a.c(b, a3);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.snda.a.a.aj.a(java.lang.String, boolean):java.io.File
     arg types: [java.lang.String, int]
     candidates:
      com.snda.a.a.aj.a(java.lang.String, java.io.File):void
      com.snda.a.a.aj.a(java.lang.String, boolean):java.io.File */
    public static String b() {
        try {
            return !aj.a() ? "" : !aj.a("sndawoa.dat") ? "" : aj.a(aj.a("sndawoa.dat", false));
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }
}
