package com.snda.a.a;

import android.telephony.TelephonyManager;
import android.telephony.gsm.GsmCellLocation;
import com.snda.a.a.a.a;

public final class an {

    /* renamed from: a  reason: collision with root package name */
    public static boolean f138a = true;
    public static boolean b = false;
    public static String c;
    public static String d;
    public static String e = "1";
    public static String f;

    private static int a(String str, TelephonyManager telephonyManager) {
        int i;
        GsmCellLocation gsmCellLocation;
        int i2 = 0;
        try {
            if (s.b(str)) {
                return 0;
            }
            if ("3".equals(str)) {
                z zVar = (z) telephonyManager.getCellLocation();
                if (zVar != null) {
                    i = zVar.a();
                }
                i = 0;
            } else {
                if (("1".equals(str) || "2".equals(str)) && (gsmCellLocation = (GsmCellLocation) telephonyManager.getCellLocation()) != null) {
                    i = gsmCellLocation.getCid();
                }
                i = 0;
            }
            try {
                a.c("InitConfig", "InitConfig:cid:" + i);
                return i;
            } catch (Exception e2) {
                Exception exc = e2;
                i2 = i;
                e = exc;
            }
        } catch (Exception e3) {
            e = e3;
            a.b("InitConfig", "Cell id not found!!!" + e.getMessage());
            return i2;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:34:0x0174 A[Catch:{ Exception -> 0x02a6 }] */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x023a A[Catch:{ Exception -> 0x02a6 }] */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x0252 A[Catch:{ Exception -> 0x02a6 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void a(android.content.Context r10) {
        /*
            r6 = 5
            r8 = 1
            r7 = 0
            java.lang.String r0 = "configJsonData"
            java.lang.String r1 = "{'mobileCard':[{'carrier':'1','cost':3000,'bid':'33280'},{'carrier':'1','cost':5000,'bid':'33281'},{'carrier':'1','cost':10000,'bid':'33282'},{'carrier':'1','cost':30000,'bid':'33283'},{'carrier':'1','cost':50000,'bid':'33284'},{'carrier':'2','cost':2000,'bid':'33285'},{'carrier':'2','cost':3000,'bid':'33286'},{'carrier':'2','cost':5000,'bid':'33287'},{'carrier':'2','cost':10000,'bid':'33288'},{'carrier':'2','cost':20000,'bid':'33289'},{'carrier':'2','cost':30000,'bid':'33290'},{'carrier':'2','cost':50000,'bid':'33291'},{'carrier':'3','cost':5000,'bid':'33292'},{'carrier':'3','cost':10000,'bid':'33293'}],'mobileFee':[{'real':500,'carrier':'1','cost':800,'bid':'134'},{'real':1300,'carrier':'1','cost':2000,'bid':'123'},{'real':2000,'carrier':'1','cost':3000,'bid':'106'},{'real':500,'carrier':'2','cost':800,'bid':'2739'},{'real':1300,'carrier':'2','cost':2000,'bid':'2740'},{'real':2000,'carrier':'2','cost':3000,'bid':'2741'}],'smsCode':{'mobileEditPwd':[{'cmd':'SJM','spNo':'106575160882','carrier':'1'},{'cmd':'SJM','spNo':'1065502180988','carrier':'2'},{'cmd':'SJM','spNo':'10690882','carrier':'3'}],'mobileBind':[{'cmd':'SJB','spNo':'106575160882','carrier':'1'},{'cmd':'SJB','spNo':'1065502180988','carrier':'2'},{'cmd':'SJB','spNo':'10690882','carrier':'3'}]},'wl':[{'decodeMethod':'CMWL1','parameter':'Location','resPos':0,'carrierId':'1','resCode':'302','needDecode':1,'url':'http://202.152.188.7/moua_red.php?url=http://3g.3guu.com/'},{'decodeMethod':'CMWL1','parameter':'Location','resPos':0,'carrierId':'2','resCode':'302','needDecode':1,'url':'http://202.152.188.7/moua_red.php?url=http://3g.3guu.com/'},{'decodeMethod':' ','parameter':' ','resPos':1,'carrierId':'3','resCode':'200','needDecode':0,'url':'http://test.woa.hurray.com.cn/HurrayWirelessOA/api/whitelist/getctnum'}],'IMSI':[{'carrier':'1','imsi':'20'},{'carrier':'3','imsi':'05'},{'carrier':'2','imsi':'06'},{'carrier':'1','imsi':'07'},{'carrier':'1','imsi':'00'},{'carrier':'2','imsi':'01'},{'carrier':'1','imsi':'02'},{'carrier':'3','imsi':'03'}],'feeNotice':50,'version':{'UpdateType':'3','DownloadUrl':''}}"
            com.snda.a.a.o.a(r10, r0, r1)
            java.lang.String r0 = "User"
            java.lang.String r0 = com.snda.a.a.o.a(r10, r0)
            boolean r1 = com.snda.a.a.s.b(r0)
            if (r1 == 0) goto L_0x005b
            java.lang.String r0 = com.snda.a.a.p.b()     // Catch:{ JSONException -> 0x0291 }
            boolean r1 = com.snda.a.a.s.a(r0)     // Catch:{ JSONException -> 0x0291 }
            if (r1 == 0) goto L_0x0050
            int r1 = r0.length()     // Catch:{ JSONException -> 0x0291 }
            if (r1 <= 0) goto L_0x0050
            org.json.JSONObject r1 = new org.json.JSONObject     // Catch:{ JSONException -> 0x0291 }
            r1.<init>(r0)     // Catch:{ JSONException -> 0x0291 }
            java.lang.String r2 = com.snda.a.a.p.f160a     // Catch:{ JSONException -> 0x0291 }
            boolean r1 = r1.isNull(r2)     // Catch:{ JSONException -> 0x0291 }
            if (r1 != 0) goto L_0x0050
            java.lang.String r1 = "InitConfig"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ JSONException -> 0x0291 }
            r2.<init>()     // Catch:{ JSONException -> 0x0291 }
            java.lang.String r3 = "loadSdcard:save user:"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ JSONException -> 0x0291 }
            java.lang.StringBuilder r2 = r2.append(r0)     // Catch:{ JSONException -> 0x0291 }
            java.lang.String r2 = r2.toString()     // Catch:{ JSONException -> 0x0291 }
            com.snda.a.a.a.a.a(r1, r2)     // Catch:{ JSONException -> 0x0291 }
            java.lang.String r1 = "User"
            com.snda.a.a.o.a(r10, r1, r0)     // Catch:{ JSONException -> 0x0291 }
        L_0x0050:
            java.lang.String r1 = "IMSI"
            java.lang.String r0 = com.snda.a.a.ac.a(r0, r1)
            java.lang.String r1 = "imsi"
            com.snda.a.a.o.a(r10, r1, r0)
        L_0x005b:
            java.lang.String r0 = "phone"
            java.lang.Object r0 = r10.getSystemService(r0)
            android.telephony.TelephonyManager r0 = (android.telephony.TelephonyManager) r0
            if (r0 == 0) goto L_0x029f
            int r1 = r0.getSimState()
            if (r1 != r6) goto L_0x029f
            java.lang.String r1 = r0.getSimCountryIso()
            java.lang.String r2 = "InitConfig"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "simISO:"
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.StringBuilder r3 = r3.append(r1)
            java.lang.String r3 = r3.toString()
            com.snda.a.a.a.a.c(r2, r3)
            java.lang.String r2 = r0.getSimOperator()
            java.lang.String r3 = "InitConfig"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "simOperator:"
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.StringBuilder r4 = r4.append(r2)
            java.lang.String r4 = r4.toString()
            com.snda.a.a.a.a.c(r3, r4)
            java.lang.String r0 = r0.getNetworkOperator()
            java.lang.String r3 = "InitConfig"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "networkOperator:"
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.StringBuilder r4 = r4.append(r0)
            java.lang.String r4 = r4.toString()
            com.snda.a.a.a.a.c(r3, r4)
            boolean r3 = com.snda.a.a.s.b(r0)
            if (r3 != 0) goto L_0x00d7
            char r3 = r0.charAt(r7)
            boolean r3 = java.lang.Character.isDigit(r3)
            if (r3 == 0) goto L_0x00d7
            java.lang.String r3 = "460"
            boolean r0 = r0.startsWith(r3)
            if (r0 == 0) goto L_0x029b
        L_0x00d7:
            boolean r0 = com.snda.a.a.s.a(r2)
            if (r0 == 0) goto L_0x00e5
            java.lang.String r0 = "460"
            boolean r0 = r2.startsWith(r0)
            if (r0 != 0) goto L_0x029f
        L_0x00e5:
            boolean r0 = com.snda.a.a.s.a(r1)
            if (r0 == 0) goto L_0x00f7
            java.lang.String r0 = r1.toLowerCase()
            java.lang.String r1 = "cn"
            boolean r0 = r0.startsWith(r1)
            if (r0 != 0) goto L_0x029f
        L_0x00f7:
            com.snda.a.a.an.b = r8
        L_0x00f9:
            java.lang.String r0 = "InitConfig"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "InitConfig:isRegisterForIMSI:"
            java.lang.StringBuilder r1 = r1.append(r2)
            boolean r2 = com.snda.a.a.an.b
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            com.snda.a.a.a.a.a(r0, r1)
            java.lang.StringBuffer r1 = new java.lang.StringBuffer
            r1.<init>()
            java.lang.String r0 = android.os.Build.BRAND
            r1.append(r0)
            java.lang.String r0 = "/"
            r1.append(r0)
            java.lang.String r0 = android.os.Build.MODEL
            r1.append(r0)
            java.lang.String r0 = "/"
            r1.append(r0)
            java.lang.String r0 = "Android"
            r1.append(r0)
            java.lang.String r0 = "/"
            r1.append(r0)
            java.lang.String r0 = android.os.Build.VERSION.RELEASE
            r1.append(r0)
            java.lang.String r2 = "/"
            r1.append(r2)
            com.snda.a.a.an.f = r0
            java.lang.String r0 = "InitConfig"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "osVersion:"
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r3 = com.snda.a.a.an.f
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            com.snda.a.a.a.a.c(r0, r2)
            java.lang.String r0 = "1.0"
            r1.append(r0)
            java.lang.String r0 = "/"
            r1.append(r0)
            java.lang.String r0 = "phone"
            java.lang.Object r0 = r10.getSystemService(r0)
            android.telephony.TelephonyManager r0 = (android.telephony.TelephonyManager) r0
            int r2 = r0.getSimState()     // Catch:{ Exception -> 0x02a6 }
            if (r2 != r6) goto L_0x0219
            java.lang.String r2 = r0.getSubscriberId()     // Catch:{ Exception -> 0x02a6 }
            java.lang.String r3 = "InitConfig"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x02a6 }
            r4.<init>()     // Catch:{ Exception -> 0x02a6 }
            java.lang.String r5 = "imsi:"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x02a6 }
            java.lang.StringBuilder r4 = r4.append(r2)     // Catch:{ Exception -> 0x02a6 }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x02a6 }
            com.snda.a.a.a.a.c(r3, r4)     // Catch:{ Exception -> 0x02a6 }
            if (r2 == 0) goto L_0x0216
            java.lang.String r3 = ""
            boolean r3 = r3.equals(r2)     // Catch:{ Exception -> 0x02a6 }
            if (r3 != 0) goto L_0x0216
            java.lang.String r3 = "imsi"
            java.lang.String r3 = com.snda.a.a.o.a(r10, r3)     // Catch:{ Exception -> 0x02a6 }
            java.lang.String r4 = "InitConfig"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x02a6 }
            r5.<init>()     // Catch:{ Exception -> 0x02a6 }
            java.lang.String r6 = "from PermanentData:"
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ Exception -> 0x02a6 }
            java.lang.StringBuilder r5 = r5.append(r3)     // Catch:{ Exception -> 0x02a6 }
            java.lang.String r5 = r5.toString()     // Catch:{ Exception -> 0x02a6 }
            com.snda.a.a.a.a.a(r4, r5)     // Catch:{ Exception -> 0x02a6 }
            java.lang.String r4 = "InitConfig"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x02a6 }
            r5.<init>()     // Catch:{ Exception -> 0x02a6 }
            java.lang.String r6 = "change Sim:"
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ Exception -> 0x02a6 }
            boolean r6 = r2.equals(r3)     // Catch:{ Exception -> 0x02a6 }
            if (r6 != 0) goto L_0x02a3
            r6 = r8
        L_0x01cc:
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ Exception -> 0x02a6 }
            java.lang.String r5 = r5.toString()     // Catch:{ Exception -> 0x02a6 }
            com.snda.a.a.a.a.c(r4, r5)     // Catch:{ Exception -> 0x02a6 }
            boolean r3 = r2.equals(r3)     // Catch:{ Exception -> 0x02a6 }
            if (r3 != 0) goto L_0x01ee
            com.snda.a.a.o.a()     // Catch:{ Exception -> 0x02a6 }
            com.snda.a.a.o.a(r10)     // Catch:{ Exception -> 0x02a6 }
            com.snda.a.a.o.b()     // Catch:{ Exception -> 0x02a6 }
            com.snda.a.a.p.a()     // Catch:{ Exception -> 0x02a6 }
            java.lang.String r3 = "imsi"
            com.snda.a.a.o.a(r10, r3, r2)     // Catch:{ Exception -> 0x02a6 }
        L_0x01ee:
            r3 = 3
            r4 = 5
            java.lang.String r3 = r2.substring(r3, r4)     // Catch:{ Exception -> 0x02a6 }
            java.lang.String r4 = "InitConfig"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x02a6 }
            r5.<init>()     // Catch:{ Exception -> 0x02a6 }
            java.lang.String r6 = "carrier:"
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ Exception -> 0x02a6 }
            java.lang.StringBuilder r5 = r5.append(r3)     // Catch:{ Exception -> 0x02a6 }
            java.lang.String r5 = r5.toString()     // Catch:{ Exception -> 0x02a6 }
            com.snda.a.a.a.a.c(r4, r5)     // Catch:{ Exception -> 0x02a6 }
            java.lang.String r4 = "imsi_carrier"
            com.snda.a.a.o.a(r4, r3)     // Catch:{ Exception -> 0x02a6 }
            java.lang.String r4 = "imsi_carrier"
            com.snda.a.a.o.a(r10, r4, r3)     // Catch:{ Exception -> 0x02a6 }
        L_0x0216:
            r1.append(r2)     // Catch:{ Exception -> 0x02a6 }
        L_0x0219:
            java.lang.String r2 = "/"
            r1.append(r2)     // Catch:{ Exception -> 0x02a6 }
            java.lang.String r2 = r0.getDeviceId()     // Catch:{ Exception -> 0x02a6 }
            com.snda.a.a.an.d = r2     // Catch:{ Exception -> 0x02a6 }
            java.lang.String r2 = com.snda.a.a.an.d     // Catch:{ Exception -> 0x02a6 }
            r1.append(r2)     // Catch:{ Exception -> 0x02a6 }
            java.lang.String r2 = "/"
            r1.append(r2)     // Catch:{ Exception -> 0x02a6 }
            java.lang.String r2 = "Phone"
            java.lang.String r2 = com.snda.a.a.o.a(r10, r2)     // Catch:{ Exception -> 0x02a6 }
            boolean r3 = com.snda.a.a.s.a(r2)     // Catch:{ Exception -> 0x02a6 }
            if (r3 == 0) goto L_0x0241
            java.lang.String r2 = com.snda.a.a.at.b(r2)     // Catch:{ Exception -> 0x02a6 }
            r1.append(r2)     // Catch:{ Exception -> 0x02a6 }
        L_0x0241:
            java.lang.String r2 = "/"
            r1.append(r2)     // Catch:{ Exception -> 0x02a6 }
            java.lang.String r2 = "carriersId"
            java.lang.String r2 = com.snda.a.a.o.a(r10, r2)     // Catch:{ Exception -> 0x02a6 }
            boolean r3 = com.snda.a.a.s.a(r2)     // Catch:{ Exception -> 0x02a6 }
            if (r3 == 0) goto L_0x025f
            java.lang.String r3 = "carriersId"
            com.snda.a.a.o.a(r3, r2)     // Catch:{ Exception -> 0x02a6 }
            java.lang.String r3 = "carriersId"
            com.snda.a.a.o.a(r10, r3, r2)     // Catch:{ Exception -> 0x02a6 }
            r1.append(r2)     // Catch:{ Exception -> 0x02a6 }
        L_0x025f:
            java.lang.String r3 = "/"
            r1.append(r3)     // Catch:{ Exception -> 0x02a6 }
            int r0 = a(r2, r0)     // Catch:{ Exception -> 0x02a6 }
            r1.append(r0)     // Catch:{ Exception -> 0x02a6 }
            java.lang.String r0 = "/"
            r1.append(r0)     // Catch:{ Exception -> 0x02a6 }
        L_0x0270:
            java.lang.String r0 = r1.toString()
            com.snda.a.a.an.c = r0
            java.lang.String r0 = "InitConfig"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "UA:"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = com.snda.a.a.an.c
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            com.snda.a.a.a.a.c(r0, r1)
            return
        L_0x0291:
            r1 = move-exception
            r9 = r1
            r1 = r0
            r0 = r9
            r0.printStackTrace()
            r0 = r1
            goto L_0x0050
        L_0x029b:
            com.snda.a.a.an.b = r8
            goto L_0x00f9
        L_0x029f:
            com.snda.a.a.an.b = r7
            goto L_0x00f9
        L_0x02a3:
            r6 = r7
            goto L_0x01cc
        L_0x02a6:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0270
        */
        throw new UnsupportedOperationException("Method not decompiled: com.snda.a.a.an.a(android.content.Context):void");
    }
}
