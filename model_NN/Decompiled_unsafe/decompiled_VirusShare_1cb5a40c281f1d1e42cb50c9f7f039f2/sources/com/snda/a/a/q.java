package com.snda.a.a;

import android.content.Context;
import com.snda.a.a.a.a;
import com.snda.a.a.b.b;
import java.lang.ref.WeakReference;

public final class q {

    /* renamed from: a  reason: collision with root package name */
    private b f161a;
    private WeakReference b;

    public q(Context context, b bVar) {
        this.b = new WeakReference(context);
        this.f161a = bVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.snda.a.a.n.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.snda.a.a.n.a(com.snda.a.a.n, java.lang.String[]):void
      com.snda.a.a.n.a(java.lang.String, java.lang.String):void
      com.snda.a.a.n.a(java.lang.String, java.lang.String[]):void
      com.snda.a.a.g.a(java.lang.String, java.util.List):void
      com.snda.a.a.g.a(java.lang.String, java.lang.String[]):void
      com.snda.a.a.i.a(java.lang.String, java.lang.String[]):void
      com.snda.a.a.n.a(java.lang.String, boolean):void */
    public final void a() {
        String str;
        if (ax.d((Context) this.b.get())) {
            String b2 = at.b(o.a((Context) this.b.get(), "UUID"));
            a.c("RegisterAction", "uuid in an hour UUID:[" + b2 + "]");
            str = b2;
        } else {
            str = null;
        }
        n nVar = new n((Context) this.b.get(), new ak((Context) this.b.get(), this.f161a));
        if (s.a(str)) {
            nVar.a(str, false);
        } else if (ax.e((Context) this.b.get())) {
            a.c("RegisterAction", "registerForSMS() is start ");
            nVar.a();
        } else {
            a.c("RegisterAction", "发短信次数超过三次，不能再使用！");
        }
    }

    public final void a(Context context) {
        String b2 = ax.b(context);
        if (s.b(b2) || b2.length() < 3) {
            a.c("RegisterAction", "RegisterAction:imsiRegister:no get imsiNumber[" + b2 + "]");
            return;
        }
        String str = "HY" + b2.substring(3);
        String a2 = ax.a();
        a.b("RegisterAction", "RegisterAction:password:" + a2);
        new x((Context) this.b.get(), new v(str, a2, (Context) this.b.get(), this.f161a)).a(str, a2);
    }

    public final void a(String str, Context context) {
        if (s.a(str)) {
            String a2 = ax.a();
            o.a(context, "LoginName", at.a(str));
            new n((Context) this.b.get(), new r(str, a2, (Context) this.b.get(), this.f161a)).a(str, a2);
            return;
        }
        this.f161a.b("{'Message':'register fail!'}");
    }
}
