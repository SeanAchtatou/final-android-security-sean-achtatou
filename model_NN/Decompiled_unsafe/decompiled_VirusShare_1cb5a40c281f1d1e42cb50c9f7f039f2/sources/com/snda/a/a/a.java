package com.snda.a.a;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import java.util.List;

public final class a extends AsyncTask {

    /* renamed from: a  reason: collision with root package name */
    private i f126a;
    private String b;
    private List c;
    private String d;
    private Context e;
    private ProgressDialog f;
    private boolean g;
    private boolean h = false;

    public a(Context context, i iVar) {
        this.f126a = iVar;
        this.e = context;
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0182 A[SYNTHETIC, Splitter:B:24:0x0182] */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0187  */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x02e8 A[SYNTHETIC, Splitter:B:68:0x02e8] */
    /* JADX WARNING: Removed duplicated region for block: B:71:0x02ed  */
    /* JADX WARNING: Removed duplicated region for block: B:79:0x031a A[SYNTHETIC, Splitter:B:79:0x031a] */
    /* JADX WARNING: Removed duplicated region for block: B:82:0x031f  */
    /* JADX WARNING: Removed duplicated region for block: B:88:0x0332 A[SYNTHETIC, Splitter:B:88:0x0332] */
    /* JADX WARNING: Removed duplicated region for block: B:91:0x0337  */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:21:0x0169=Splitter:B:21:0x0169, B:65:0x02cc=Splitter:B:65:0x02cc, B:76:0x02fe=Splitter:B:76:0x02fe} */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String[] doInBackground(java.lang.Object... r13) {
        /*
            r12 = this;
            r3 = 1
            r10 = 0
            r0 = 0
            r0 = r13[r0]
            java.lang.String r0 = (java.lang.String) r0
            r12.b = r0
            java.lang.String r0 = "HttpAsyncTask"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "HttpAsyncTask:mReqUrl:"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = r12.b
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            com.snda.a.a.a.a.c(r0, r1)
            r0 = r13[r3]
            java.util.List r0 = (java.util.List) r0
            r12.c = r0
            r0 = 2
            r0 = r13[r0]
            java.lang.String r0 = (java.lang.String) r0
            r12.d = r0
            r0 = 3
            r0 = r13[r0]
            java.lang.Boolean r0 = (java.lang.Boolean) r0
            boolean r0 = r0.booleanValue()
            r12.g = r0
            java.lang.String r2 = ""
            android.content.Context r0 = r12.e     // Catch:{ SocketTimeoutException -> 0x038c, IOException -> 0x02c9, Exception -> 0x02fb, all -> 0x032d }
            java.lang.String r1 = "connectivity"
            java.lang.Object r0 = r0.getSystemService(r1)     // Catch:{ SocketTimeoutException -> 0x038c, IOException -> 0x02c9, Exception -> 0x02fb, all -> 0x032d }
            android.net.ConnectivityManager r0 = (android.net.ConnectivityManager) r0     // Catch:{ SocketTimeoutException -> 0x038c, IOException -> 0x02c9, Exception -> 0x02fb, all -> 0x032d }
            android.content.Context r1 = r12.e     // Catch:{ SocketTimeoutException -> 0x038c, IOException -> 0x02c9, Exception -> 0x02fb, all -> 0x032d }
            java.lang.String r3 = "wifi"
            java.lang.Object r1 = r1.getSystemService(r3)     // Catch:{ SocketTimeoutException -> 0x038c, IOException -> 0x02c9, Exception -> 0x02fb, all -> 0x032d }
            android.net.wifi.WifiManager r1 = (android.net.wifi.WifiManager) r1     // Catch:{ SocketTimeoutException -> 0x038c, IOException -> 0x02c9, Exception -> 0x02fb, all -> 0x032d }
            android.net.NetworkInfo r0 = r0.getActiveNetworkInfo()     // Catch:{ SocketTimeoutException -> 0x038c, IOException -> 0x02c9, Exception -> 0x02fb, all -> 0x032d }
            if (r0 == 0) goto L_0x005d
            boolean r0 = r0.isAvailable()     // Catch:{ SocketTimeoutException -> 0x038c, IOException -> 0x02c9, Exception -> 0x02fb, all -> 0x032d }
            if (r0 != 0) goto L_0x007e
        L_0x005d:
            java.lang.String r0 = "-2^$^networkinfo is null!"
            java.lang.String r1 = "HttpAsyncTask"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ SocketTimeoutException -> 0x038c, IOException -> 0x02c9, Exception -> 0x02fb, all -> 0x032d }
            r2.<init>()     // Catch:{ SocketTimeoutException -> 0x038c, IOException -> 0x02c9, Exception -> 0x02fb, all -> 0x032d }
            java.lang.String r3 = "HttpAsyncTask:result="
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ SocketTimeoutException -> 0x038c, IOException -> 0x02c9, Exception -> 0x02fb, all -> 0x032d }
            java.lang.StringBuilder r2 = r2.append(r0)     // Catch:{ SocketTimeoutException -> 0x038c, IOException -> 0x02c9, Exception -> 0x02fb, all -> 0x032d }
            java.lang.String r2 = r2.toString()     // Catch:{ SocketTimeoutException -> 0x038c, IOException -> 0x02c9, Exception -> 0x02fb, all -> 0x032d }
            com.snda.a.a.a.a.c(r1, r2)     // Catch:{ SocketTimeoutException -> 0x038c, IOException -> 0x02c9, Exception -> 0x02fb, all -> 0x032d }
            java.lang.String r1 = "\\^\\$\\^"
            java.lang.String[] r0 = r0.split(r1)     // Catch:{ SocketTimeoutException -> 0x038c, IOException -> 0x02c9, Exception -> 0x02fb, all -> 0x032d }
        L_0x007d:
            return r0
        L_0x007e:
            org.apache.http.impl.client.DefaultHttpClient r3 = new org.apache.http.impl.client.DefaultHttpClient     // Catch:{ SocketTimeoutException -> 0x038c, IOException -> 0x02c9, Exception -> 0x02fb, all -> 0x032d }
            r3.<init>()     // Catch:{ SocketTimeoutException -> 0x038c, IOException -> 0x02c9, Exception -> 0x02fb, all -> 0x032d }
            java.lang.String r0 = android.net.Proxy.getDefaultHost()     // Catch:{ SocketTimeoutException -> 0x0166, IOException -> 0x0372, Exception -> 0x035c, all -> 0x0344 }
            java.lang.String r4 = "HttpAsyncTask"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ SocketTimeoutException -> 0x0166, IOException -> 0x0372, Exception -> 0x035c, all -> 0x0344 }
            r5.<init>()     // Catch:{ SocketTimeoutException -> 0x0166, IOException -> 0x0372, Exception -> 0x035c, all -> 0x0344 }
            java.lang.String r6 = "proxyHost : "
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ SocketTimeoutException -> 0x0166, IOException -> 0x0372, Exception -> 0x035c, all -> 0x0344 }
            java.lang.StringBuilder r5 = r5.append(r0)     // Catch:{ SocketTimeoutException -> 0x0166, IOException -> 0x0372, Exception -> 0x035c, all -> 0x0344 }
            java.lang.String r5 = r5.toString()     // Catch:{ SocketTimeoutException -> 0x0166, IOException -> 0x0372, Exception -> 0x035c, all -> 0x0344 }
            com.snda.a.a.a.a.c(r4, r5)     // Catch:{ SocketTimeoutException -> 0x0166, IOException -> 0x0372, Exception -> 0x035c, all -> 0x0344 }
            boolean r1 = r1.isWifiEnabled()     // Catch:{ SocketTimeoutException -> 0x0166, IOException -> 0x0372, Exception -> 0x035c, all -> 0x0344 }
            if (r1 != 0) goto L_0x00c7
            if (r0 == 0) goto L_0x00c7
            java.lang.String r1 = ""
            boolean r0 = r1.equals(r0)     // Catch:{ SocketTimeoutException -> 0x0166, IOException -> 0x0372, Exception -> 0x035c, all -> 0x0344 }
            if (r0 != 0) goto L_0x00c7
            org.apache.http.HttpHost r0 = new org.apache.http.HttpHost     // Catch:{ SocketTimeoutException -> 0x0166, IOException -> 0x0372, Exception -> 0x035c, all -> 0x0344 }
            java.lang.String r1 = android.net.Proxy.getDefaultHost()     // Catch:{ SocketTimeoutException -> 0x0166, IOException -> 0x0372, Exception -> 0x035c, all -> 0x0344 }
            int r4 = android.net.Proxy.getDefaultPort()     // Catch:{ SocketTimeoutException -> 0x0166, IOException -> 0x0372, Exception -> 0x035c, all -> 0x0344 }
            java.lang.String r5 = "http"
            r0.<init>(r1, r4, r5)     // Catch:{ SocketTimeoutException -> 0x0166, IOException -> 0x0372, Exception -> 0x035c, all -> 0x0344 }
            org.apache.http.params.HttpParams r1 = r3.getParams()     // Catch:{ SocketTimeoutException -> 0x0166, IOException -> 0x0372, Exception -> 0x035c, all -> 0x0344 }
            java.lang.String r4 = "http.route.default-proxy"
            r1.setParameter(r4, r0)     // Catch:{ SocketTimeoutException -> 0x0166, IOException -> 0x0372, Exception -> 0x035c, all -> 0x0344 }
        L_0x00c7:
            java.lang.String r0 = new java.lang.String     // Catch:{ SocketTimeoutException -> 0x0166, IOException -> 0x0372, Exception -> 0x035c, all -> 0x0344 }
            java.lang.String r1 = "name:password"
            byte[] r1 = r1.getBytes()     // Catch:{ SocketTimeoutException -> 0x0166, IOException -> 0x0372, Exception -> 0x035c, all -> 0x0344 }
            r0.<init>(r1)     // Catch:{ SocketTimeoutException -> 0x0166, IOException -> 0x0372, Exception -> 0x035c, all -> 0x0344 }
            org.apache.http.client.methods.HttpPost r1 = new org.apache.http.client.methods.HttpPost     // Catch:{ SocketTimeoutException -> 0x0166, IOException -> 0x0372, Exception -> 0x035c, all -> 0x0344 }
            java.lang.String r4 = r12.b     // Catch:{ SocketTimeoutException -> 0x0166, IOException -> 0x0372, Exception -> 0x035c, all -> 0x0344 }
            r1.<init>(r4)     // Catch:{ SocketTimeoutException -> 0x0166, IOException -> 0x0372, Exception -> 0x035c, all -> 0x0344 }
            org.apache.http.message.BasicHeader r4 = new org.apache.http.message.BasicHeader     // Catch:{ SocketTimeoutException -> 0x0166, IOException -> 0x0372, Exception -> 0x035c, all -> 0x0344 }
            java.lang.String r5 = "authorization"
            r4.<init>(r5, r0)     // Catch:{ SocketTimeoutException -> 0x0166, IOException -> 0x0372, Exception -> 0x035c, all -> 0x0344 }
            r1.addHeader(r4)     // Catch:{ SocketTimeoutException -> 0x0166, IOException -> 0x0372, Exception -> 0x035c, all -> 0x0344 }
            java.lang.String r0 = "Connection"
            java.lang.String r4 = "close"
            r1.setHeader(r0, r4)     // Catch:{ SocketTimeoutException -> 0x0166, IOException -> 0x0372, Exception -> 0x035c, all -> 0x0344 }
            java.lang.String r0 = "Cache-Control"
            java.lang.String r4 = "no-cache"
            r1.setHeader(r0, r4)     // Catch:{ SocketTimeoutException -> 0x0166, IOException -> 0x0372, Exception -> 0x035c, all -> 0x0344 }
            org.apache.http.params.HttpParams r0 = r3.getParams()     // Catch:{ SocketTimeoutException -> 0x0166, IOException -> 0x0372, Exception -> 0x035c, all -> 0x0344 }
            java.lang.String r4 = "http.protocol.single-cookie-header"
            r5 = 0
            java.lang.Boolean r5 = java.lang.Boolean.valueOf(r5)     // Catch:{ SocketTimeoutException -> 0x0166, IOException -> 0x0372, Exception -> 0x035c, all -> 0x0344 }
            r0.setParameter(r4, r5)     // Catch:{ SocketTimeoutException -> 0x0166, IOException -> 0x0372, Exception -> 0x035c, all -> 0x0344 }
            org.apache.http.params.HttpParams r0 = r1.getParams()     // Catch:{ SocketTimeoutException -> 0x0166, IOException -> 0x0372, Exception -> 0x035c, all -> 0x0344 }
            r4 = 30000(0x7530, float:4.2039E-41)
            org.apache.http.params.HttpConnectionParams.setConnectionTimeout(r0, r4)     // Catch:{ SocketTimeoutException -> 0x0166, IOException -> 0x0372, Exception -> 0x035c, all -> 0x0344 }
            r4 = 30000(0x7530, float:4.2039E-41)
            org.apache.http.params.HttpConnectionParams.setSoTimeout(r0, r4)     // Catch:{ SocketTimeoutException -> 0x0166, IOException -> 0x0372, Exception -> 0x035c, all -> 0x0344 }
            r4 = 8954(0x22fa, float:1.2547E-41)
            org.apache.http.params.HttpConnectionParams.setSocketBufferSize(r0, r4)     // Catch:{ SocketTimeoutException -> 0x0166, IOException -> 0x0372, Exception -> 0x035c, all -> 0x0344 }
            java.lang.String r4 = "UTF-8"
            org.apache.http.params.HttpProtocolParams.setContentCharset(r0, r4)     // Catch:{ SocketTimeoutException -> 0x0166, IOException -> 0x0372, Exception -> 0x035c, all -> 0x0344 }
            org.apache.http.HttpVersion r4 = org.apache.http.HttpVersion.HTTP_1_0     // Catch:{ SocketTimeoutException -> 0x0166, IOException -> 0x0372, Exception -> 0x035c, all -> 0x0344 }
            org.apache.http.params.HttpProtocolParams.setVersion(r0, r4)     // Catch:{ SocketTimeoutException -> 0x0166, IOException -> 0x0372, Exception -> 0x035c, all -> 0x0344 }
            java.lang.String r0 = "HttpAsyncTask"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ SocketTimeoutException -> 0x0166, IOException -> 0x0372, Exception -> 0x035c, all -> 0x0344 }
            r4.<init>()     // Catch:{ SocketTimeoutException -> 0x0166, IOException -> 0x0372, Exception -> 0x035c, all -> 0x0344 }
            java.lang.String r5 = "HttpAsyncTask:mReqEntityParams:"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ SocketTimeoutException -> 0x0166, IOException -> 0x0372, Exception -> 0x035c, all -> 0x0344 }
            java.util.List r5 = r12.c     // Catch:{ SocketTimeoutException -> 0x0166, IOException -> 0x0372, Exception -> 0x035c, all -> 0x0344 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ SocketTimeoutException -> 0x0166, IOException -> 0x0372, Exception -> 0x035c, all -> 0x0344 }
            java.lang.String r4 = r4.toString()     // Catch:{ SocketTimeoutException -> 0x0166, IOException -> 0x0372, Exception -> 0x035c, all -> 0x0344 }
            com.snda.a.a.a.a.c(r0, r4)     // Catch:{ SocketTimeoutException -> 0x0166, IOException -> 0x0372, Exception -> 0x035c, all -> 0x0344 }
            java.lang.StringBuffer r4 = new java.lang.StringBuffer     // Catch:{ SocketTimeoutException -> 0x0166, IOException -> 0x0372, Exception -> 0x035c, all -> 0x0344 }
            r4.<init>()     // Catch:{ SocketTimeoutException -> 0x0166, IOException -> 0x0372, Exception -> 0x035c, all -> 0x0344 }
            java.util.List r0 = r12.c     // Catch:{ SocketTimeoutException -> 0x0166, IOException -> 0x0372, Exception -> 0x035c, all -> 0x0344 }
            java.util.Iterator r5 = r0.iterator()     // Catch:{ SocketTimeoutException -> 0x0166, IOException -> 0x0372, Exception -> 0x035c, all -> 0x0344 }
        L_0x0141:
            boolean r0 = r5.hasNext()     // Catch:{ SocketTimeoutException -> 0x0166, IOException -> 0x0372, Exception -> 0x035c, all -> 0x0344 }
            if (r0 == 0) goto L_0x01ae
            java.lang.Object r0 = r5.next()     // Catch:{ SocketTimeoutException -> 0x0166, IOException -> 0x0372, Exception -> 0x035c, all -> 0x0344 }
            org.apache.http.NameValuePair r0 = (org.apache.http.NameValuePair) r0     // Catch:{ SocketTimeoutException -> 0x0166, IOException -> 0x0372, Exception -> 0x035c, all -> 0x0344 }
            java.lang.String r6 = "&"
            r4.append(r6)     // Catch:{ SocketTimeoutException -> 0x0166, IOException -> 0x0372, Exception -> 0x035c, all -> 0x0344 }
            java.lang.String r6 = r0.getName()     // Catch:{ SocketTimeoutException -> 0x0166, IOException -> 0x0372, Exception -> 0x035c, all -> 0x0344 }
            r4.append(r6)     // Catch:{ SocketTimeoutException -> 0x0166, IOException -> 0x0372, Exception -> 0x035c, all -> 0x0344 }
            java.lang.String r6 = "="
            r4.append(r6)     // Catch:{ SocketTimeoutException -> 0x0166, IOException -> 0x0372, Exception -> 0x035c, all -> 0x0344 }
            java.lang.String r0 = r0.getValue()     // Catch:{ SocketTimeoutException -> 0x0166, IOException -> 0x0372, Exception -> 0x035c, all -> 0x0344 }
            r4.append(r0)     // Catch:{ SocketTimeoutException -> 0x0166, IOException -> 0x0372, Exception -> 0x035c, all -> 0x0344 }
            goto L_0x0141
        L_0x0166:
            r0 = move-exception
            r1 = r10
            r2 = r3
        L_0x0169:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x035a }
            r3.<init>()     // Catch:{ all -> 0x035a }
            java.lang.String r4 = "-4^$^"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x035a }
            java.lang.String r0 = r0.getMessage()     // Catch:{ all -> 0x035a }
            java.lang.StringBuilder r0 = r3.append(r0)     // Catch:{ all -> 0x035a }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x035a }
            if (r1 == 0) goto L_0x0185
            r1.close()     // Catch:{ IOException -> 0x02c3 }
        L_0x0185:
            if (r2 == 0) goto L_0x018e
            org.apache.http.conn.ClientConnectionManager r1 = r2.getConnectionManager()
            r1.shutdown()
        L_0x018e:
            java.lang.String r1 = "HttpAsyncTask"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "HttpAsyncTask:result="
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r2 = r2.append(r0)
            java.lang.String r2 = r2.toString()
            com.snda.a.a.a.a.c(r1, r2)
            java.lang.String r1 = "\\^\\$\\^"
            java.lang.String[] r0 = r0.split(r1)
            goto L_0x007d
        L_0x01ae:
            org.apache.http.entity.StringEntity r0 = new org.apache.http.entity.StringEntity     // Catch:{ SocketTimeoutException -> 0x0166, IOException -> 0x0372, Exception -> 0x035c, all -> 0x0344 }
            java.lang.String r4 = r4.toString()     // Catch:{ SocketTimeoutException -> 0x0166, IOException -> 0x0372, Exception -> 0x035c, all -> 0x0344 }
            java.lang.String r5 = "UTF-8"
            r0.<init>(r4, r5)     // Catch:{ SocketTimeoutException -> 0x0166, IOException -> 0x0372, Exception -> 0x035c, all -> 0x0344 }
            java.lang.String r4 = "application/x-www-form-urlencoded"
            r0.setContentType(r4)     // Catch:{ SocketTimeoutException -> 0x0166, IOException -> 0x0372, Exception -> 0x035c, all -> 0x0344 }
            r1.setEntity(r0)     // Catch:{ SocketTimeoutException -> 0x0166, IOException -> 0x0372, Exception -> 0x035c, all -> 0x0344 }
            boolean r0 = r12.g     // Catch:{ SocketTimeoutException -> 0x0166, IOException -> 0x0372, Exception -> 0x035c, all -> 0x0344 }
            if (r0 == 0) goto L_0x01d0
            r0 = 1
            java.lang.String[] r0 = new java.lang.String[r0]     // Catch:{ SocketTimeoutException -> 0x0166, IOException -> 0x0372, Exception -> 0x035c, all -> 0x0344 }
            r4 = 0
            java.lang.String r5 = r12.d     // Catch:{ SocketTimeoutException -> 0x0166, IOException -> 0x0372, Exception -> 0x035c, all -> 0x0344 }
            r0[r4] = r5     // Catch:{ SocketTimeoutException -> 0x0166, IOException -> 0x0372, Exception -> 0x035c, all -> 0x0344 }
            r12.publishProgress(r0)     // Catch:{ SocketTimeoutException -> 0x0166, IOException -> 0x0372, Exception -> 0x035c, all -> 0x0344 }
        L_0x01d0:
            long r4 = java.lang.System.currentTimeMillis()     // Catch:{ SocketTimeoutException -> 0x0166, IOException -> 0x0372, Exception -> 0x035c, all -> 0x0344 }
            org.apache.http.HttpResponse r0 = r3.execute(r1)     // Catch:{ SocketTimeoutException -> 0x0166, IOException -> 0x0372, Exception -> 0x035c, all -> 0x0344 }
            long r6 = java.lang.System.currentTimeMillis()     // Catch:{ SocketTimeoutException -> 0x0166, IOException -> 0x0372, Exception -> 0x035c, all -> 0x0344 }
            java.lang.String r1 = "HttpAsyncTask"
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ SocketTimeoutException -> 0x0166, IOException -> 0x0372, Exception -> 0x035c, all -> 0x0344 }
            r8.<init>()     // Catch:{ SocketTimeoutException -> 0x0166, IOException -> 0x0372, Exception -> 0x035c, all -> 0x0344 }
            java.lang.String r9 = "[HTTPCostTime:"
            java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ SocketTimeoutException -> 0x0166, IOException -> 0x0372, Exception -> 0x035c, all -> 0x0344 }
            long r4 = r6 - r4
            java.lang.StringBuilder r4 = r8.append(r4)     // Catch:{ SocketTimeoutException -> 0x0166, IOException -> 0x0372, Exception -> 0x035c, all -> 0x0344 }
            java.lang.String r5 = "ms]"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ SocketTimeoutException -> 0x0166, IOException -> 0x0372, Exception -> 0x035c, all -> 0x0344 }
            java.lang.String r4 = r4.toString()     // Catch:{ SocketTimeoutException -> 0x0166, IOException -> 0x0372, Exception -> 0x035c, all -> 0x0344 }
            com.snda.a.a.a.a.c(r1, r4)     // Catch:{ SocketTimeoutException -> 0x0166, IOException -> 0x0372, Exception -> 0x035c, all -> 0x0344 }
            org.apache.http.HttpEntity r1 = r0.getEntity()     // Catch:{ SocketTimeoutException -> 0x0166, IOException -> 0x0372, Exception -> 0x035c, all -> 0x0344 }
            long r4 = r1.getContentLength()     // Catch:{ SocketTimeoutException -> 0x0166, IOException -> 0x0372, Exception -> 0x035c, all -> 0x0344 }
            java.lang.String r6 = "HttpAsyncTask"
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ SocketTimeoutException -> 0x0166, IOException -> 0x0372, Exception -> 0x035c, all -> 0x0344 }
            r7.<init>()     // Catch:{ SocketTimeoutException -> 0x0166, IOException -> 0x0372, Exception -> 0x035c, all -> 0x0344 }
            java.lang.String r8 = "HttpAsyncTask:ContentLength:"
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ SocketTimeoutException -> 0x0166, IOException -> 0x0372, Exception -> 0x035c, all -> 0x0344 }
            java.lang.StringBuilder r4 = r7.append(r4)     // Catch:{ SocketTimeoutException -> 0x0166, IOException -> 0x0372, Exception -> 0x035c, all -> 0x0344 }
            java.lang.String r4 = r4.toString()     // Catch:{ SocketTimeoutException -> 0x0166, IOException -> 0x0372, Exception -> 0x035c, all -> 0x0344 }
            com.snda.a.a.a.a.c(r6, r4)     // Catch:{ SocketTimeoutException -> 0x0166, IOException -> 0x0372, Exception -> 0x035c, all -> 0x0344 }
            java.io.InputStream r1 = r1.getContent()     // Catch:{ SocketTimeoutException -> 0x0166, IOException -> 0x0372, Exception -> 0x035c, all -> 0x0344 }
            if (r1 == 0) goto L_0x02b8
            java.lang.String r4 = "Accept-Encoding"
            org.apache.http.Header r0 = r0.getFirstHeader(r4)     // Catch:{ SocketTimeoutException -> 0x0166, IOException -> 0x0372, Exception -> 0x035c, all -> 0x0344 }
            java.lang.String r4 = "HttpAsyncTask"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ SocketTimeoutException -> 0x0166, IOException -> 0x0372, Exception -> 0x035c, all -> 0x0344 }
            r5.<init>()     // Catch:{ SocketTimeoutException -> 0x0166, IOException -> 0x0372, Exception -> 0x035c, all -> 0x0344 }
            java.lang.String r6 = "Accept-Encoding:"
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ SocketTimeoutException -> 0x0166, IOException -> 0x0372, Exception -> 0x035c, all -> 0x0344 }
            java.lang.StringBuilder r0 = r5.append(r0)     // Catch:{ SocketTimeoutException -> 0x0166, IOException -> 0x0372, Exception -> 0x035c, all -> 0x0344 }
            java.lang.String r0 = r0.toString()     // Catch:{ SocketTimeoutException -> 0x0166, IOException -> 0x0372, Exception -> 0x035c, all -> 0x0344 }
            com.snda.a.a.a.a.c(r4, r0)     // Catch:{ SocketTimeoutException -> 0x0166, IOException -> 0x0372, Exception -> 0x035c, all -> 0x0344 }
            java.lang.String r0 = "http://211.99.197.56/HurrayWirelessOA/api/query/config_1_2_9"
            java.lang.String r4 = r12.b     // Catch:{ SocketTimeoutException -> 0x0166, IOException -> 0x0372, Exception -> 0x035c, all -> 0x0344 }
            boolean r0 = r0.equals(r4)     // Catch:{ SocketTimeoutException -> 0x0166, IOException -> 0x0372, Exception -> 0x035c, all -> 0x0344 }
            if (r0 == 0) goto L_0x0275
            java.io.BufferedReader r0 = new java.io.BufferedReader     // Catch:{ SocketTimeoutException -> 0x0166, IOException -> 0x0372, Exception -> 0x035c, all -> 0x0344 }
            java.io.InputStreamReader r4 = new java.io.InputStreamReader     // Catch:{ SocketTimeoutException -> 0x0166, IOException -> 0x0372, Exception -> 0x035c, all -> 0x0344 }
            java.util.zip.GZIPInputStream r5 = new java.util.zip.GZIPInputStream     // Catch:{ SocketTimeoutException -> 0x0166, IOException -> 0x0372, Exception -> 0x035c, all -> 0x0344 }
            r5.<init>(r1)     // Catch:{ SocketTimeoutException -> 0x0166, IOException -> 0x0372, Exception -> 0x035c, all -> 0x0344 }
            java.lang.String r6 = "UTF-8"
            r4.<init>(r5, r6)     // Catch:{ SocketTimeoutException -> 0x0166, IOException -> 0x0372, Exception -> 0x035c, all -> 0x0344 }
            r5 = 10240(0x2800, float:1.4349E-41)
            r0.<init>(r4, r5)     // Catch:{ SocketTimeoutException -> 0x0166, IOException -> 0x0372, Exception -> 0x035c, all -> 0x0344 }
        L_0x025d:
            java.lang.String r4 = r0.readLine()     // Catch:{ SocketTimeoutException -> 0x0391, IOException -> 0x0377, Exception -> 0x0360, all -> 0x0348 }
            if (r4 == 0) goto L_0x0299
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ SocketTimeoutException -> 0x0391, IOException -> 0x0377, Exception -> 0x0360, all -> 0x0348 }
            r5.<init>()     // Catch:{ SocketTimeoutException -> 0x0391, IOException -> 0x0377, Exception -> 0x0360, all -> 0x0348 }
            java.lang.StringBuilder r2 = r5.append(r2)     // Catch:{ SocketTimeoutException -> 0x0391, IOException -> 0x0377, Exception -> 0x0360, all -> 0x0348 }
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ SocketTimeoutException -> 0x0391, IOException -> 0x0377, Exception -> 0x0360, all -> 0x0348 }
            java.lang.String r2 = r2.toString()     // Catch:{ SocketTimeoutException -> 0x0391, IOException -> 0x0377, Exception -> 0x0360, all -> 0x0348 }
            goto L_0x025d
        L_0x0275:
            java.io.BufferedReader r0 = new java.io.BufferedReader     // Catch:{ SocketTimeoutException -> 0x0166, IOException -> 0x0372, Exception -> 0x035c, all -> 0x0344 }
            java.io.InputStreamReader r4 = new java.io.InputStreamReader     // Catch:{ SocketTimeoutException -> 0x0166, IOException -> 0x0372, Exception -> 0x035c, all -> 0x0344 }
            r4.<init>(r1)     // Catch:{ SocketTimeoutException -> 0x0166, IOException -> 0x0372, Exception -> 0x035c, all -> 0x0344 }
            r5 = 10240(0x2800, float:1.4349E-41)
            r0.<init>(r4, r5)     // Catch:{ SocketTimeoutException -> 0x0166, IOException -> 0x0372, Exception -> 0x035c, all -> 0x0344 }
        L_0x0281:
            java.lang.String r4 = r0.readLine()     // Catch:{ SocketTimeoutException -> 0x0398, IOException -> 0x037e, Exception -> 0x0366, all -> 0x034e }
            if (r4 == 0) goto L_0x0299
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ SocketTimeoutException -> 0x0398, IOException -> 0x037e, Exception -> 0x0366, all -> 0x034e }
            r5.<init>()     // Catch:{ SocketTimeoutException -> 0x0398, IOException -> 0x037e, Exception -> 0x0366, all -> 0x034e }
            java.lang.StringBuilder r2 = r5.append(r2)     // Catch:{ SocketTimeoutException -> 0x0398, IOException -> 0x037e, Exception -> 0x0366, all -> 0x034e }
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ SocketTimeoutException -> 0x0398, IOException -> 0x037e, Exception -> 0x0366, all -> 0x034e }
            java.lang.String r2 = r2.toString()     // Catch:{ SocketTimeoutException -> 0x0398, IOException -> 0x037e, Exception -> 0x0366, all -> 0x034e }
            goto L_0x0281
        L_0x0299:
            if (r1 == 0) goto L_0x029e
            r1.close()     // Catch:{ SocketTimeoutException -> 0x039f, IOException -> 0x0385, Exception -> 0x036c, all -> 0x0354 }
        L_0x029e:
            org.apache.http.conn.ClientConnectionManager r1 = r3.getConnectionManager()     // Catch:{ SocketTimeoutException -> 0x039f, IOException -> 0x0385, Exception -> 0x036c, all -> 0x0354 }
            r1.shutdown()     // Catch:{ SocketTimeoutException -> 0x039f, IOException -> 0x0385, Exception -> 0x036c, all -> 0x0354 }
            r1 = r2
            r2 = r10
        L_0x02a7:
            if (r0 == 0) goto L_0x02ac
            r0.close()     // Catch:{ IOException -> 0x02be }
        L_0x02ac:
            if (r2 == 0) goto L_0x03a6
            org.apache.http.conn.ClientConnectionManager r0 = r2.getConnectionManager()
            r0.shutdown()
            r0 = r1
            goto L_0x018e
        L_0x02b8:
            java.lang.String r0 = "1^$^error"
            r1 = r0
            r2 = r3
            r0 = r10
            goto L_0x02a7
        L_0x02be:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x02ac
        L_0x02c3:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0185
        L_0x02c9:
            r0 = move-exception
            r1 = r10
            r2 = r10
        L_0x02cc:
            r0.printStackTrace()     // Catch:{ all -> 0x035a }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x035a }
            r3.<init>()     // Catch:{ all -> 0x035a }
            java.lang.String r4 = "1^$^"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x035a }
            java.lang.String r0 = r0.getMessage()     // Catch:{ all -> 0x035a }
            java.lang.StringBuilder r0 = r3.append(r0)     // Catch:{ all -> 0x035a }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x035a }
            if (r1 == 0) goto L_0x02eb
            r1.close()     // Catch:{ IOException -> 0x02f6 }
        L_0x02eb:
            if (r2 == 0) goto L_0x018e
            org.apache.http.conn.ClientConnectionManager r1 = r2.getConnectionManager()
            r1.shutdown()
            goto L_0x018e
        L_0x02f6:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x02eb
        L_0x02fb:
            r0 = move-exception
            r1 = r10
            r2 = r10
        L_0x02fe:
            r0.printStackTrace()     // Catch:{ all -> 0x035a }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x035a }
            r3.<init>()     // Catch:{ all -> 0x035a }
            java.lang.String r4 = "-2^$^"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x035a }
            java.lang.String r0 = r0.getMessage()     // Catch:{ all -> 0x035a }
            java.lang.StringBuilder r0 = r3.append(r0)     // Catch:{ all -> 0x035a }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x035a }
            if (r1 == 0) goto L_0x031d
            r1.close()     // Catch:{ IOException -> 0x0328 }
        L_0x031d:
            if (r2 == 0) goto L_0x018e
            org.apache.http.conn.ClientConnectionManager r1 = r2.getConnectionManager()
            r1.shutdown()
            goto L_0x018e
        L_0x0328:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x031d
        L_0x032d:
            r0 = move-exception
            r1 = r10
            r2 = r10
        L_0x0330:
            if (r1 == 0) goto L_0x0335
            r1.close()     // Catch:{ IOException -> 0x033f }
        L_0x0335:
            if (r2 == 0) goto L_0x033e
            org.apache.http.conn.ClientConnectionManager r1 = r2.getConnectionManager()
            r1.shutdown()
        L_0x033e:
            throw r0
        L_0x033f:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0335
        L_0x0344:
            r0 = move-exception
            r1 = r10
            r2 = r3
            goto L_0x0330
        L_0x0348:
            r1 = move-exception
            r2 = r3
            r11 = r0
            r0 = r1
            r1 = r11
            goto L_0x0330
        L_0x034e:
            r1 = move-exception
            r2 = r3
            r11 = r0
            r0 = r1
            r1 = r11
            goto L_0x0330
        L_0x0354:
            r1 = move-exception
            r2 = r3
            r11 = r0
            r0 = r1
            r1 = r11
            goto L_0x0330
        L_0x035a:
            r0 = move-exception
            goto L_0x0330
        L_0x035c:
            r0 = move-exception
            r1 = r10
            r2 = r3
            goto L_0x02fe
        L_0x0360:
            r1 = move-exception
            r2 = r3
            r11 = r0
            r0 = r1
            r1 = r11
            goto L_0x02fe
        L_0x0366:
            r1 = move-exception
            r2 = r3
            r11 = r0
            r0 = r1
            r1 = r11
            goto L_0x02fe
        L_0x036c:
            r1 = move-exception
            r2 = r3
            r11 = r0
            r0 = r1
            r1 = r11
            goto L_0x02fe
        L_0x0372:
            r0 = move-exception
            r1 = r10
            r2 = r3
            goto L_0x02cc
        L_0x0377:
            r1 = move-exception
            r2 = r3
            r11 = r0
            r0 = r1
            r1 = r11
            goto L_0x02cc
        L_0x037e:
            r1 = move-exception
            r2 = r3
            r11 = r0
            r0 = r1
            r1 = r11
            goto L_0x02cc
        L_0x0385:
            r1 = move-exception
            r2 = r3
            r11 = r0
            r0 = r1
            r1 = r11
            goto L_0x02cc
        L_0x038c:
            r0 = move-exception
            r1 = r10
            r2 = r10
            goto L_0x0169
        L_0x0391:
            r1 = move-exception
            r2 = r3
            r11 = r0
            r0 = r1
            r1 = r11
            goto L_0x0169
        L_0x0398:
            r1 = move-exception
            r2 = r3
            r11 = r0
            r0 = r1
            r1 = r11
            goto L_0x0169
        L_0x039f:
            r1 = move-exception
            r2 = r3
            r11 = r0
            r0 = r1
            r1 = r11
            goto L_0x0169
        L_0x03a6:
            r0 = r1
            goto L_0x018e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.snda.a.a.a.doInBackground(java.lang.Object[]):java.lang.String[]");
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void onPostExecute(Object obj) {
        String[] strArr = (String[]) obj;
        super.onPostExecute(strArr);
        publishProgress(new String[0]);
        this.f126a.a(this.b, strArr);
    }

    /* access modifiers changed from: protected */
    public final void onPreExecute() {
        super.onPreExecute();
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void onProgressUpdate(Object[] objArr) {
        String[] strArr = (String[]) objArr;
        super.onProgressUpdate(strArr);
        try {
            if (this.f == null) {
                this.f = new ProgressDialog(this.e);
            }
            if (strArr == null || strArr.length == 0) {
                this.f.dismiss();
                return;
            }
            this.f.setMessage(strArr[0]);
            this.f.show();
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }
}
