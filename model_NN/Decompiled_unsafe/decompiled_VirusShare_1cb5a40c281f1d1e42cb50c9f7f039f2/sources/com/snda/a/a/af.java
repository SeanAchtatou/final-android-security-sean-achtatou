package com.snda.a.a;

import android.content.Context;
import com.snda.a.a.b.a;
import com.snda.a.a.b.c;
import java.util.HashMap;

public final class af implements c {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public Context f132a;
    private a b;

    public af(Context context, a aVar) {
        this.f132a = context;
        this.b = aVar;
    }

    private static boolean a(String str) {
        if (s.a(str)) {
            return str.startsWith("HY");
        }
        return false;
    }

    public final void a(String[] strArr) {
        String a2 = s.a(strArr, 0);
        if ("0".equals(a2)) {
            String a3 = s.a(strArr, 1);
            if (a3 != null) {
                a3 = am.b(a3);
                o.a(this.f132a, "PTAccount", at.a(a3));
            }
            String a4 = s.a(strArr, 2);
            if (a4 != null) {
                a4 = am.b(a4);
                o.a(this.f132a, "NUMAccount", at.a(a4));
            }
            String a5 = o.a(this.f132a, "Phone");
            if (s.a(a5)) {
                a5 = at.b(a5);
            }
            HashMap hashMap = new HashMap();
            hashMap.put("PTAccount", a3);
            hashMap.put("NumAccount", a4);
            if (s.a(a5) && ax.a(a5) && !a(a3)) {
                hashMap.put("MobileNum", a5);
            } else if (a(a3)) {
                hashMap.put("MobileNum", "+28" + a4);
            } else {
                com.snda.a.a.a.a.a("QueryAccInfoHCB", "query user info success but no get mobileNum");
                this.b.b("{'Message':'no get mobileNum'}");
                return;
            }
            hashMap.put("ProductId", ag.f133a);
            hashMap.put("Message", "query user info success!");
            this.b.a(al.a(hashMap));
            com.snda.a.a.a.a.c("QueryAccInfoHCB", "write user info to sdcard;such as:mobileNum=" + a5);
            new Thread(new m(this)).start();
        } else if ("-2".equals(a2) || "-4".equals(a2)) {
            this.b.c("{'Message':'" + s.a(strArr, 1) + "'}");
        } else {
            this.b.b("{'Message':'" + s.a(strArr, 1) + "'}");
        }
    }
}
