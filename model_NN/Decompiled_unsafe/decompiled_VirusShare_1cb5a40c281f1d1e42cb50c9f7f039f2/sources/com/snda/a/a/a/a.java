package com.snda.a.a.a;

import android.util.Log;

public final class a {

    /* renamed from: a  reason: collision with root package name */
    private static boolean f127a;

    public static void a(String str, String str2) {
        if (f127a) {
            Log.d(str, "[ " + str2 + " ]");
        }
    }

    public static void b(String str, String str2) {
        if (f127a) {
            Log.e(str, "[ " + str2 + " ]");
        }
    }

    public static void c(String str, String str2) {
        if (f127a) {
            Log.i(str, "[ " + str2 + " ]");
        }
    }
}
