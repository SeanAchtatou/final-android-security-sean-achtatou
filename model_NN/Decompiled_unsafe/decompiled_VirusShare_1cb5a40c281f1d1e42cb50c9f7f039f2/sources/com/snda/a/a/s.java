package com.snda.a.a;

public final class s {
    public static String a(String[] strArr, int i) {
        if (strArr == null) {
            return null;
        }
        if (strArr.length <= i) {
            return null;
        }
        return strArr[i];
    }

    public static boolean a(String str) {
        return !b(str);
    }

    public static boolean b(String str) {
        return str == null || "".equals(str) || "null".equals(str) || " ".equals(str) || str.length() <= 0;
    }
}
