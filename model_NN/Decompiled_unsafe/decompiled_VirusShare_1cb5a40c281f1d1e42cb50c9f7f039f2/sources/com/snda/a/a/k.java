package com.snda.a.a;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.snda.a.a.a.a;

final class k extends BroadcastReceiver {

    /* renamed from: a  reason: collision with root package name */
    private ap f157a;
    private /* synthetic */ as b;

    public k(as asVar, ap apVar) {
        this.b = asVar;
        this.f157a = apVar;
    }

    public final void onReceive(Context context, Intent intent) {
        context.unregisterReceiver(this);
        boolean unused = this.b.f = false;
        int resultCode = getResultCode();
        a.a("SENT_SMS_ACTION", "result code : " + resultCode);
        switch (resultCode) {
            case -1:
                a.c("SmsSender", "send sms is success!");
                as.a(this.b, "sms send success !");
                if (this.f157a != null) {
                    a.c("SmsSender", "send sms is ok!");
                    this.f157a.a();
                    break;
                }
                break;
            case 0:
            default:
                if (this.f157a != null) {
                    this.f157a.a(new String[]{"1", "error_sms_generic_failure"});
                    break;
                }
                break;
            case 1:
                if (this.f157a != null) {
                    this.f157a.a(new String[]{"1", "error_sms_generic_failure"});
                    break;
                }
                break;
            case 2:
                if (this.f157a != null) {
                    this.f157a.a(new String[]{"1", "error_sms_radio_off"});
                    break;
                }
                break;
            case 3:
                if (this.f157a != null) {
                    this.f157a.a(new String[]{"1", "error_sms_null_pdu"});
                    break;
                }
                break;
            case 4:
                if (this.f157a != null) {
                    this.f157a.a(new String[]{"1", "error_sms_no_service"});
                    break;
                }
                break;
        }
        this.b.a();
    }
}
