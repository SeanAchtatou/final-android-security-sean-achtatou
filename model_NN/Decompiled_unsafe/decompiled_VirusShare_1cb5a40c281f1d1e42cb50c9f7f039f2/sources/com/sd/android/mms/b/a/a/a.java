package com.sd.android.mms.b.a.a;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import org.b.a.a.i;
import org.b.a.a.o;
import org.w3c.dom.Attr;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;

public final class a {
    private static void a(Writer writer, Element element) {
        writer.write(60);
        writer.write(element.getTagName());
        if (element.hasAttributes()) {
            NamedNodeMap attributes = element.getAttributes();
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 >= attributes.getLength()) {
                    break;
                }
                Attr attr = (Attr) attributes.item(i2);
                writer.write(" " + attr.getName());
                writer.write("=\"" + attr.getValue() + "\"");
                i = i2 + 1;
            }
        }
        i iVar = (i) element.getFirstChild();
        if (iVar != null) {
            writer.write(62);
            do {
                a(writer, iVar);
                iVar = (i) iVar.getNextSibling();
            } while (iVar != null);
            writer.write("</");
            writer.write(element.getTagName());
            writer.write(62);
            return;
        }
        writer.write("/>");
    }

    public static void a(o oVar, OutputStream outputStream) {
        try {
            BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"), 2048);
            a(bufferedWriter, oVar.getDocumentElement());
            bufferedWriter.flush();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
    }
}
