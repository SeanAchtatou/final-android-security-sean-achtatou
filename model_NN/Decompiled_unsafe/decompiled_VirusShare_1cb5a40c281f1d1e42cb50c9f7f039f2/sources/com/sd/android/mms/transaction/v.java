package com.sd.android.mms.transaction;

import java.util.ArrayList;
import java.util.Iterator;

public abstract class v {

    /* renamed from: a  reason: collision with root package name */
    private final ArrayList f125a = new ArrayList();
    private Iterator b;

    public final void a(b bVar) {
        this.f125a.add(bVar);
    }

    public final void b(b bVar) {
        if (this.b != null) {
            this.b.remove();
        } else {
            this.f125a.remove(bVar);
        }
    }

    public final void f() {
        this.b = this.f125a.iterator();
        while (this.b.hasNext()) {
            try {
                ((b) this.b.next()).a(this);
            } finally {
                this.b = null;
            }
        }
    }
}
