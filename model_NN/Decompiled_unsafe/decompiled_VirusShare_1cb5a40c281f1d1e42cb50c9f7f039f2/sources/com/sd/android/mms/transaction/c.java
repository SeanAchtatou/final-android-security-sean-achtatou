package com.sd.android.mms.transaction;

import android.os.Handler;
import android.os.Message;
import android.widget.Toast;
import com.snda.youni.C0000R;

final class c extends Handler {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ MyTransactionService f111a;

    c(MyTransactionService myTransactionService) {
        this.f111a = myTransactionService;
    }

    public final void handleMessage(Message message) {
        String str = null;
        if (message.what == 1) {
            str = this.f111a.getString(C0000R.string.message_queued);
        } else if (message.what == 2) {
            str = this.f111a.getString(C0000R.string.download_later);
        }
        if (str != null) {
            Toast.makeText(this.f111a, str, 1).show();
        }
    }
}
