package com.sd.android.mms.d;

import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;
import android.os.SystemProperties;
import android.preference.PreferenceManager;
import android.util.Log;
import com.sd.a.a.a.a.d;
import com.sd.a.a.a.a.p;
import com.sd.a.a.a.a.w;
import com.sd.a.a.a.b.b;
import com.snda.youni.C0000R;

public final class l {
    /* access modifiers changed from: private */
    public static l g;
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public final Context f102a;
    private final Handler b;
    /* access modifiers changed from: private */
    public final SharedPreferences c;
    /* access modifiers changed from: private */
    public boolean d;
    private final SharedPreferences.OnSharedPreferenceChangeListener e = new m(this);
    private final BroadcastReceiver f = new o(this);

    private l(Context context) {
        this.f102a = context;
        this.b = new Handler();
        this.c = PreferenceManager.getDefaultSharedPreferences(context);
        this.c.registerOnSharedPreferenceChangeListener(this.e);
        context.registerReceiver(this.f, new IntentFilter("android.intent.action.SERVICE_STATE"));
        this.d = a(this.c);
    }

    static /* synthetic */ String a(l lVar, Uri uri) {
        p pVar = (p) d.a(lVar.f102a).a(uri);
        w e2 = pVar.e();
        String c2 = e2 != null ? e2.c() : lVar.f102a.getString(C0000R.string.no_subject);
        w a2 = pVar.a();
        return lVar.f102a.getString(C0000R.string.dl_failure_notification, c2, a2 != null ? i.a().a(lVar.f102a, a2.c()) : lVar.f102a.getString(C0000R.string.unknown_sender));
    }

    public static void a(Context context) {
        if (g != null) {
            Log.w("DownloadManager", "Already initialized.");
        }
        g = new l(context);
    }

    static boolean a(SharedPreferences sharedPreferences) {
        return a(sharedPreferences, "true".equals(SystemProperties.get("gsm.operator.isroaming", null)));
    }

    static boolean a(SharedPreferences sharedPreferences, boolean z) {
        if (sharedPreferences.getBoolean("pref_key_mms_auto_retrieval", true)) {
            boolean z2 = sharedPreferences.getBoolean("pref_key_mms_retrieval_during_roaming", false);
            if (!z || z2) {
                return true;
            }
        }
        return false;
    }

    public static l b() {
        if (g != null) {
            return g;
        }
        throw new IllegalStateException("Uninitialized.");
    }

    /* JADX INFO: finally extract failed */
    public final int a(Uri uri) {
        Cursor a2 = b.a(this.f102a, this.f102a.getContentResolver(), uri, new String[]{"st"}, null, null, null);
        if (a2 != null) {
            try {
                if (a2.moveToFirst()) {
                    int i = a2.getInt(0) & -5;
                    a2.close();
                    return i;
                }
                a2.close();
            } catch (Throwable th) {
                a2.close();
                throw th;
            }
        }
        return 128;
    }

    public final void a(Uri uri, int i) {
        int i2;
        if (i == 135) {
            this.b.post(new n(this, uri));
            i2 = i;
        } else {
            i2 = !this.d ? i | 4 : i;
        }
        ContentValues contentValues = new ContentValues(1);
        contentValues.put("st", Integer.valueOf(i2));
        b.a(this.f102a, this.f102a.getContentResolver(), uri, contentValues, null);
    }

    public final boolean a() {
        return this.d;
    }
}
