package com.sd.android.mms.b.a;

import android.util.Log;
import java.util.ArrayList;
import org.b.a.a.g;
import org.b.a.a.i;
import org.b.a.a.l;

public abstract class k implements l {

    /* renamed from: a  reason: collision with root package name */
    final i f77a;

    k(i iVar) {
        this.f77a = iVar;
    }

    public float a() {
        try {
            String attribute = this.f77a.getAttribute("dur");
            if (attribute != null) {
                return j.a(attribute) / 1000.0f;
            }
            return 0.0f;
        } catch (IllegalArgumentException e) {
            return 0.0f;
        }
    }

    public final void b(float f) {
        this.f77a.setAttribute("dur", Integer.toString((int) (1000.0f * f)) + "ms");
    }

    /* access modifiers changed from: package-private */
    public abstract l c_();

    public g f() {
        String[] split = this.f77a.getAttribute("begin").split(";");
        ArrayList arrayList = new ArrayList();
        for (int i = 0; i < split.length; i++) {
            try {
                arrayList.add(new j(split[i]));
            } catch (IllegalArgumentException e) {
            }
        }
        if (arrayList.size() == 0) {
            arrayList.add(new j("0"));
        }
        return new u(arrayList);
    }

    public final g h() {
        ArrayList arrayList = new ArrayList();
        String[] split = this.f77a.getAttribute("end").split(";");
        int length = split.length;
        if (!(length == 1 && split[0].length() == 0)) {
            for (int i = 0; i < length; i++) {
                try {
                    arrayList.add(new j(split[i]));
                } catch (IllegalArgumentException e) {
                    Log.e("ElementTimeImpl", "Malformed time value.", e);
                }
            }
        }
        if (arrayList.size() == 0) {
            float a2 = a();
            if (a2 < 0.0f) {
                arrayList.add(new j("indefinite"));
            } else {
                g f = f();
                for (int i2 = 0; i2 < f.a(); i2++) {
                    arrayList.add(new j((f.a(i2).b() + ((double) a2)) + "s"));
                }
            }
        }
        return new u(arrayList);
    }

    public final short i() {
        short s;
        String attribute = this.f77a.getAttribute("fill");
        if (attribute.equalsIgnoreCase("freeze")) {
            return 1;
        }
        if (attribute.equalsIgnoreCase("remove")) {
            return 0;
        }
        if (attribute.equalsIgnoreCase("hold")) {
            return 1;
        }
        if (attribute.equalsIgnoreCase("transition")) {
            return 1;
        }
        if (!attribute.equalsIgnoreCase("auto")) {
            k kVar = this;
            while (true) {
                String attribute2 = kVar.f77a.getAttribute("fillDefault");
                if (attribute2.equalsIgnoreCase("remove")) {
                    s = 0;
                    break;
                } else if (attribute2.equalsIgnoreCase("freeze")) {
                    s = 1;
                    break;
                } else if (attribute2.equalsIgnoreCase("auto")) {
                    s = 2;
                    break;
                } else if (attribute2.equalsIgnoreCase("hold")) {
                    s = 1;
                    break;
                } else if (attribute2.equalsIgnoreCase("transition")) {
                    s = 1;
                    break;
                } else {
                    l c_ = kVar.c_();
                    if (c_ == null) {
                        s = 2;
                        break;
                    }
                    kVar = (k) c_;
                }
            }
            if (s != 2) {
                return s;
            }
        }
        return (this.f77a.getAttribute("dur").length() == 0 && this.f77a.getAttribute("end").length() == 0 && this.f77a.getAttribute("repeatCount").length() == 0 && this.f77a.getAttribute("repeatDur").length() == 0) ? (short) 1 : 0;
    }
}
