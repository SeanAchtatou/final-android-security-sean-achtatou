package com.sd.android.mms.a;

import java.util.ArrayList;
import java.util.Iterator;

public class c {
    protected ArrayList b = new ArrayList();

    /* access modifiers changed from: protected */
    public void a(o oVar) {
    }

    /* access modifiers changed from: protected */
    public final void a(boolean z) {
        Iterator it = this.b.iterator();
        while (it.hasNext()) {
            ((o) it.next()).a(this, z);
        }
    }

    /* access modifiers changed from: protected */
    public void b(o oVar) {
    }

    public final void c(o oVar) {
        if (!this.b.contains(oVar)) {
            this.b.add(oVar);
            a(oVar);
        }
    }

    /* access modifiers changed from: protected */
    public void d() {
    }

    public final void d(o oVar) {
        this.b.remove(oVar);
        b(oVar);
    }

    public final void g() {
        d();
        this.b.clear();
    }
}
