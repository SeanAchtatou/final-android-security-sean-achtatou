package com.sd.android.mms.b;

import java.util.ArrayList;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public final class a implements NodeList {

    /* renamed from: a  reason: collision with root package name */
    private ArrayList f69a;
    private ArrayList b;
    private Node c;
    private String d;
    private boolean e;

    public a(ArrayList arrayList) {
        this.b = arrayList;
    }

    public a(Node node, String str, boolean z) {
        this.c = node;
        this.d = str;
        this.e = z;
    }

    private void a(Node node) {
        if (node == this.c) {
            this.f69a = new ArrayList();
        } else if (this.d == null || node.getNodeName().equals(this.d)) {
            this.f69a.add(node);
        }
        for (Node firstChild = node.getFirstChild(); firstChild != null; firstChild = firstChild.getNextSibling()) {
            if (this.e) {
                a(firstChild);
            } else if (this.d == null || firstChild.getNodeName().equals(this.d)) {
                this.f69a.add(firstChild);
            }
        }
    }

    public final int getLength() {
        if (this.b != null) {
            return this.b.size();
        }
        a(this.c);
        return this.f69a.size();
    }

    public final Node item(int i) {
        if (this.b == null) {
            a(this.c);
            try {
                return (Node) this.f69a.get(i);
            } catch (IndexOutOfBoundsException e2) {
                return null;
            }
        } else {
            try {
                return (Node) this.b.get(i);
            } catch (IndexOutOfBoundsException e3) {
                return null;
            }
        }
    }
}
