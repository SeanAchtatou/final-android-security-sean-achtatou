package com.sd.android.mms.transaction;

import android.a.d;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.os.PowerManager;
import android.util.Log;
import com.sd.a.a.a.a.c;
import com.sd.a.a.a.a.m;
import com.sd.a.a.a.a.p;
import com.sd.a.a.a.a.r;
import com.sd.a.a.a.b.b;

public class PushReceiver extends BroadcastReceiver {
    /* JADX INFO: finally extract failed */
    /* access modifiers changed from: private */
    public static long b(Context context, r rVar, int i) {
        String str = i == 134 ? new String(((m) rVar).b()) : new String(((c) rVar).b());
        StringBuilder sb = new StringBuilder(40);
        sb.append("m_id");
        sb.append('=');
        sb.append(DatabaseUtils.sqlEscapeString(str));
        sb.append(" AND ");
        sb.append("m_type");
        sb.append('=');
        sb.append(128);
        Cursor a2 = b.a(context, context.getContentResolver(), d.f7a, new String[]{"thread_id"}, sb.toString(), null, null);
        if (a2 != null) {
            try {
                if (a2.getCount() != 1 || !a2.moveToFirst()) {
                    a2.close();
                } else {
                    long j = a2.getLong(0);
                    a2.close();
                    return j;
                }
            } catch (Throwable th) {
                a2.close();
                throw th;
            }
        }
        return -1;
    }

    /* JADX INFO: finally extract failed */
    /* access modifiers changed from: private */
    public static boolean b(Context context, p pVar) {
        Cursor a2;
        byte[] b = pVar.b();
        if (!(b == null || (a2 = b.a(context, context.getContentResolver(), d.f7a, new String[]{"_id"}, "ct_l = ?", new String[]{new String(b)}, null)) == null)) {
            try {
                if (a2.getCount() > 0) {
                    a2.close();
                    return true;
                }
                a2.close();
            } catch (Throwable th) {
                a2.close();
                throw th;
            }
        }
        return false;
    }

    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals("android.provider.Telephony.WAP_PUSH_RECEIVED") && "application/vnd.wap.mms-message".equals(intent.getType())) {
            Log.v("PushReceiver", "Received PUSH Intent: " + intent);
            ((PowerManager) context.getSystemService("power")).newWakeLock(1, "MMS PushReceiver").acquire(5000);
            new i(this, context).execute(intent);
        }
    }
}
