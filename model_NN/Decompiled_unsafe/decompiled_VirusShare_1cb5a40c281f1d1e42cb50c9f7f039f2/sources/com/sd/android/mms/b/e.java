package com.sd.android.mms.b;

import org.w3c.dom.Attr;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.TypeInfo;
import org.w3c.dom.UserDataHandler;

public final class e extends f implements Attr {

    /* renamed from: a  reason: collision with root package name */
    private String f88a;
    private String c;

    protected e(c cVar, String str) {
        super(cVar);
        this.f88a = str;
    }

    public final short compareDocumentPosition(Node node) {
        return 0;
    }

    public final String getBaseURI() {
        return null;
    }

    public final Object getFeature(String str, String str2) {
        return null;
    }

    public final String getName() {
        return this.f88a;
    }

    public final Node getNextSibling() {
        return null;
    }

    public final String getNodeName() {
        return this.f88a;
    }

    public final short getNodeType() {
        return 2;
    }

    public final Element getOwnerElement() {
        return null;
    }

    public final Node getParentNode() {
        return null;
    }

    public final Node getPreviousSibling() {
        return null;
    }

    public final TypeInfo getSchemaTypeInfo() {
        return null;
    }

    public final boolean getSpecified() {
        return this.c != null;
    }

    public final String getTextContent() {
        return null;
    }

    public final Object getUserData(String str) {
        return null;
    }

    public final String getValue() {
        return this.c;
    }

    public final boolean isDefaultNamespace(String str) {
        return false;
    }

    public final boolean isEqualNode(Node node) {
        return false;
    }

    public final boolean isId() {
        return false;
    }

    public final boolean isSameNode(Node node) {
        return false;
    }

    public final String lookupNamespaceURI(String str) {
        return null;
    }

    public final String lookupPrefix(String str) {
        return null;
    }

    public final void setNodeValue(String str) {
        this.c = str;
    }

    public final void setTextContent(String str) {
    }

    public final Object setUserData(String str, Object obj, UserDataHandler userDataHandler) {
        return null;
    }

    public final void setValue(String str) {
        this.c = str;
    }
}
