package com.sd.android.mms.b.a.a;

import com.sd.a.a.a.b;
import java.io.InputStream;
import org.b.a.a.o;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

public final class c {

    /* renamed from: a  reason: collision with root package name */
    private XMLReader f71a;
    private b b;

    public c() {
        System.setProperty("org.xml.sax.driver", "org.xmlpull.v1.sax2.Driver");
        try {
            this.f71a = XMLReaderFactory.createXMLReader();
            this.b = new b();
            this.f71a.setContentHandler(this.b);
        } catch (SAXException e) {
            throw new b(e);
        }
    }

    public final o a(InputStream inputStream) {
        this.b.a();
        this.f71a.parse(new InputSource(inputStream));
        o b2 = this.b.b();
        b2.j();
        b2.k();
        return b2;
    }
}
