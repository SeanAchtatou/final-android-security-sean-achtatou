package com.sd.android.mms.d;

import android.a.e;
import android.content.Context;
import android.database.Cursor;
import com.sd.a.a.a.b.b;
import java.util.HashSet;
import java.util.Iterator;

public final class d {

    /* renamed from: a  reason: collision with root package name */
    private static d f94a;
    private static String[] e = {"_id", "thread_id"};
    private final Context b;
    private HashSet c = new HashSet(4);
    private final HashSet d = new HashSet(1);

    private d(Context context) {
        this.b = context;
        new Thread(new j(this)).start();
    }

    public static d a() {
        return f94a;
    }

    public static void a(Context context) {
        f94a = new d(context);
    }

    /* access modifiers changed from: private */
    public synchronized void b() {
        HashSet hashSet = this.c;
        HashSet hashSet2 = new HashSet(hashSet.size());
        Cursor a2 = b.a(this.b, this.b.getContentResolver(), e.b, e, null, null, null);
        try {
            if (a2.moveToFirst()) {
                while (!a2.isAfterLast()) {
                    hashSet2.add(Long.valueOf(a2.getLong(1)));
                    a2.moveToNext();
                }
            }
            a2.close();
            this.c = hashSet2;
            if (this.d.size() > 0) {
                HashSet<Long> hashSet3 = new HashSet<>(hashSet2);
                hashSet3.removeAll(hashSet);
                HashSet<Long> hashSet4 = new HashSet<>(hashSet);
                hashSet4.removeAll(hashSet2);
                Iterator it = this.d.iterator();
                while (it.hasNext()) {
                    it.next();
                    for (Long longValue : hashSet3) {
                        longValue.longValue();
                    }
                    for (Long longValue2 : hashSet4) {
                        longValue2.longValue();
                    }
                }
            }
        } catch (Throwable th) {
            a2.close();
            throw th;
        }
    }

    public final synchronized void a(long j, boolean z) {
        if (j > 0) {
            if (z ? this.c.add(Long.valueOf(j)) : this.c.remove(Long.valueOf(j))) {
                Iterator it = this.d.iterator();
                while (it.hasNext()) {
                    it.next();
                }
            }
        }
    }

    public final synchronized boolean a(long j) {
        return this.c.contains(Long.valueOf(j));
    }
}
