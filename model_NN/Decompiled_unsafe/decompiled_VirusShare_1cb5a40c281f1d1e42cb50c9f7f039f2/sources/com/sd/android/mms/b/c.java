package com.sd.android.mms.b;

import org.w3c.dom.Attr;
import org.w3c.dom.CDATASection;
import org.w3c.dom.Comment;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.w3c.dom.DocumentFragment;
import org.w3c.dom.DocumentType;
import org.w3c.dom.Element;
import org.w3c.dom.EntityReference;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.ProcessingInstruction;
import org.w3c.dom.Text;

public abstract class c extends f implements Document {
    public c() {
        super(null);
    }

    public Attr createAttribute(String str) {
        return new e(this, str);
    }

    public Attr createAttributeNS(String str, String str2) {
        return null;
    }

    public CDATASection createCDATASection(String str) {
        return null;
    }

    public Comment createComment(String str) {
        return null;
    }

    public DocumentFragment createDocumentFragment() {
        return null;
    }

    public Element createElementNS(String str, String str2) {
        return null;
    }

    public EntityReference createEntityReference(String str) {
        return null;
    }

    public ProcessingInstruction createProcessingInstruction(String str, String str2) {
        return null;
    }

    public Text createTextNode(String str) {
        return null;
    }

    public DocumentType getDoctype() {
        return null;
    }

    public Element getElementById(String str) {
        return null;
    }

    public NodeList getElementsByTagName(String str) {
        return null;
    }

    public NodeList getElementsByTagNameNS(String str, String str2) {
        return null;
    }

    public DOMImplementation getImplementation() {
        return null;
    }

    public String getNodeName() {
        return "#document";
    }

    public short getNodeType() {
        return 9;
    }

    public Node importNode(Node node, boolean z) {
        return null;
    }
}
