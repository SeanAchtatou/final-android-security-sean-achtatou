package com.sd.android.mms.a;

import android.content.Context;
import android.media.MediaPlayer;
import android.net.Uri;
import android.util.Log;
import com.sd.android.mms.e.a;
import com.sd.android.mms.e.b;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import org.b.a.b.d;

public abstract class e extends c implements d {

    /* renamed from: a  reason: collision with root package name */
    private int f62a;
    protected Context c;
    protected String d;
    protected String e;
    protected short f;
    protected int g;
    private int h;
    private String i;
    private Uri j;
    private byte[] k;
    private int l;
    private b m;
    private final ArrayList n;

    public e(Context context, String str, String str2, String str3, Uri uri) {
        this.c = context;
        this.i = str;
        this.e = str2;
        this.d = str3;
        this.j = uri;
        InputStream inputStream = null;
        try {
            inputStream = this.c.getContentResolver().openInputStream(this.j);
            if (inputStream instanceof FileInputStream) {
                this.l = (int) ((FileInputStream) inputStream).getChannel().size();
            } else {
                while (-1 != inputStream.read()) {
                    this.l++;
                }
            }
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e2) {
                    Log.e("MediaModel", "IOException caught while closing stream", e2);
                }
            }
        } catch (IOException e3) {
            Log.e("MediaModel", "IOException caught while opening or reading stream", e3);
            if (e3 instanceof FileNotFoundException) {
                throw new com.sd.a.a.a.b(e3.getMessage());
            } else if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e4) {
                    Log.e("MediaModel", "IOException caught while closing stream", e4);
                }
            }
        } catch (Throwable th) {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e5) {
                    Log.e("MediaModel", "IOException caught while closing stream", e5);
                }
            }
            throw th;
        }
        this.n = new ArrayList();
    }

    public e(Context context, String str, String str2, String str3, b bVar) {
        this.c = context;
        this.i = str;
        this.e = str2;
        this.d = str3;
        this.m = bVar;
        this.j = a.a(context, bVar);
        this.l = bVar.f().length;
        this.n = new ArrayList();
    }

    public e(Context context, String str, String str2, String str3, byte[] bArr) {
        if (bArr == null) {
            throw new IllegalArgumentException("data may not be null.");
        }
        this.c = context;
        this.i = str;
        this.e = str2;
        this.d = str3;
        this.k = bArr;
        this.l = bArr.length;
        this.n = new ArrayList();
    }

    public static boolean b(Uri uri) {
        return uri.getAuthority().startsWith("mms");
    }

    public final void a(int i2) {
        this.f62a = i2;
        a(true);
    }

    /* access modifiers changed from: package-private */
    public final void a(Uri uri) {
        this.j = uri;
    }

    public final void a(k kVar) {
        this.n.add(kVar);
    }

    public final void a(short s) {
        this.f = s;
        a(true);
    }

    public final void b(int i2) {
        if (!v() || i2 >= 0) {
            this.h = i2;
        } else {
            try {
                s();
            } catch (com.sd.a.a.a.b e2) {
                Log.e("MediaModel", e2.getMessage(), e2);
                return;
            }
        }
        a(true);
    }

    public final int c() {
        return this.f62a;
    }

    public final int e() {
        return this.h;
    }

    public final String f() {
        return this.e;
    }

    public final Uri h() {
        return this.j;
    }

    public final Uri i() {
        if (this.j == null || !q() || this.m.b()) {
            return this.j;
        }
        throw new android.drm.mobile1.b("Insufficient DRM rights.");
    }

    public final byte[] j() {
        if (this.k == null) {
            return null;
        }
        if (!q() || this.m.b()) {
            byte[] bArr = new byte[this.k.length];
            System.arraycopy(this.k, 0, bArr, 0, this.k.length);
            return bArr;
        }
        throw new android.drm.mobile1.b("DRM 权限不足");
    }

    public final String k() {
        return this.d;
    }

    public final int l() {
        return this.l;
    }

    public final boolean m() {
        return this.i.equals("text");
    }

    public final boolean n() {
        return this.i.equals("img");
    }

    public final boolean o() {
        return this.i.equals("video");
    }

    public final boolean p() {
        return this.i.equals("audio");
    }

    public final boolean q() {
        return this.m != null;
    }

    public final boolean r() {
        return this.m.c();
    }

    /* access modifiers changed from: protected */
    public final void s() {
        if (this.j == null) {
            throw new IllegalArgumentException("Uri may not be null.");
        }
        MediaPlayer mediaPlayer = new MediaPlayer();
        try {
            mediaPlayer.setDataSource(this.c, this.j);
            mediaPlayer.prepare();
            this.h = mediaPlayer.getDuration();
            mediaPlayer.release();
        } catch (IOException e2) {
            Log.e("MediaModel", "Unexpected IOException.", e2);
            throw new com.sd.a.a.a.b(e2);
        } catch (Throwable th) {
            mediaPlayer.release();
            throw th;
        }
    }

    public final int t() {
        return this.g;
    }

    public final k u() {
        return this.n.size() == 0 ? k.NO_ACTIVE_ACTION : (k) this.n.remove(0);
    }

    /* access modifiers changed from: protected */
    public boolean v() {
        return false;
    }

    public final b w() {
        return this.m;
    }
}
