package com.sd.android.mms.d;

import android.util.Log;
import java.util.HashMap;

public final class k {

    /* renamed from: a  reason: collision with root package name */
    private static final HashMap f101a = new HashMap();

    public static synchronized long a(Object obj) {
        long longValue;
        synchronized (k.class) {
            Long l = (Long) f101a.get(obj);
            Log.v("SendingProgressTokenManager", "TokenManager.get(" + obj + ") -> " + l);
            longValue = l != null ? l.longValue() : -1;
        }
        return longValue;
    }

    public static synchronized void a(Object obj, long j) {
        synchronized (k.class) {
            Log.v("SendingProgressTokenManager", "TokenManager.put(" + obj + ", " + j + ")");
            f101a.put(obj, Long.valueOf(j));
        }
    }

    public static synchronized void b(Object obj) {
        synchronized (k.class) {
            Log.v("SendingProgressTokenManager", "TokenManager.remove(" + obj + ")");
            f101a.remove(obj);
        }
    }
}
