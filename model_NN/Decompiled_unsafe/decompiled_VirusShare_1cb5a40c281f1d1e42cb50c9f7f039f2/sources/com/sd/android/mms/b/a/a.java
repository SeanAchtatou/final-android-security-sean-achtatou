package com.sd.android.mms.b.a;

import java.util.ArrayList;
import org.b.a.a.b;
import org.b.a.a.f;
import org.b.a.a.g;
import org.b.a.a.i;
import org.b.a.a.l;
import org.w3c.dom.DOMException;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public abstract class a extends w implements f {
    a(i iVar) {
        super(iVar);
    }

    private void a(String str) {
        if ("first".equals(str) || "last".equals(str) || "all".equals(str) || "media".equals(str)) {
            this.f77a.setAttribute("endsync", str);
            return;
        }
        throw new DOMException(9, "Unsupported endsync value" + str);
    }

    public final float a() {
        float a2 = super.a();
        if (a2 != 0.0f) {
            return a2;
        }
        String attribute = this.f77a.getAttribute("endsync");
        if (attribute == null || attribute.length() == 0) {
            a("last");
            attribute = "last";
        } else if (!"first".equals(attribute) && !"last".equals(attribute) && !"all".equals(attribute) && !"media".equals(attribute)) {
            a("last");
            attribute = "last";
        }
        if (!"last".equals(attribute)) {
            return -1.0f;
        }
        NodeList g = g();
        int i = 0;
        float f = -1.0f;
        while (i < g.getLength()) {
            g h = ((l) g.item(i)).h();
            float f2 = f;
            for (int i2 = 0; i2 < h.a(); i2++) {
                b a3 = h.a(i2);
                if (a3.c() == 0) {
                    return -1.0f;
                }
                if (a3.a()) {
                    float b = (float) a3.b();
                    if (b > f2) {
                        f2 = b;
                    }
                }
            }
            i++;
            f = f2;
        }
        return f;
    }

    public final NodeList a(float f) {
        ArrayList arrayList = new ArrayList();
        NodeList g = g();
        int length = g.getLength();
        for (int i = 0; i < length; i++) {
            l lVar = (l) g.item(i);
            g f2 = lVar.f();
            int a2 = f2.a();
            double d = 0.0d;
            boolean z = false;
            for (int i2 = 0; i2 < a2; i2++) {
                b a3 = f2.a(i2);
                if (a3.a()) {
                    double b = a3.b() * 1000.0d;
                    if (b <= ((double) f) && b >= d) {
                        z = true;
                        d = b;
                    }
                }
            }
            g h = lVar.h();
            int a4 = h.a();
            boolean z2 = z;
            double d2 = d;
            for (int i3 = 0; i3 < a4; i3++) {
                b a5 = h.a(i3);
                if (a5.a()) {
                    double b2 = a5.b() * 1000.0d;
                    if (b2 <= ((double) f) && b2 >= d2) {
                        z2 = false;
                        d2 = b2;
                    }
                }
            }
            if (z2) {
                arrayList.add((Node) lVar);
            }
        }
        return new com.sd.android.mms.b.a(arrayList);
    }
}
