package com.sd.android.mms.transaction;

import android.a.p;
import android.content.Context;
import android.net.Uri;
import com.sd.a.a.a.a.d;
import com.sd.a.a.a.a.k;
import com.sd.a.a.a.a.o;
import com.sd.a.a.a.b;
import java.io.IOException;

public final class u extends e {
    private final Uri d;

    public u(Context context, int i, j jVar, String str) {
        super(context, i, jVar);
        this.d = Uri.parse(str);
        this.b = str;
        a(l.a(context));
    }

    public final void b() {
        d a2 = d.a(this.f113a);
        try {
            a(new k(this.f113a, (o) a2.a(this.d)).a());
            Uri a3 = a2.a(this.d, p.f17a);
            this.c.a(1);
            this.c.a(a3);
            if (this.c.a() != 1) {
                this.c.a(2);
                this.c.a(this.d);
            }
            f();
        } catch (IOException e) {
            if (this.c.a() != 1) {
                this.c.a(2);
                this.c.a(this.d);
            }
            f();
        } catch (b e2) {
            if (this.c.a() != 1) {
                this.c.a(2);
                this.c.a(this.d);
            }
            f();
        } catch (RuntimeException e3) {
            if (this.c.a() != 1) {
                this.c.a(2);
                this.c.a(this.d);
            }
            f();
        } catch (Throwable th) {
            if (this.c.a() != 1) {
                this.c.a(2);
                this.c.a(this.d);
            }
            f();
            throw th;
        }
    }

    public final int e() {
        return 3;
    }
}
