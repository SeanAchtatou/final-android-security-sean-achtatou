package com.sd.android.mms.d;

import android.a.d;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.text.TextUtils;
import com.sd.a.a.a.a.w;
import com.snda.youni.C0000R;

public final class b {
    private b() {
    }

    /* JADX INFO: finally extract failed */
    public static String a(Context context, Uri uri) {
        String lastPathSegment = uri.getLastPathSegment();
        Uri.Builder buildUpon = d.f7a.buildUpon();
        buildUpon.appendPath(lastPathSegment).appendPath("addr");
        Cursor a2 = com.sd.a.a.a.b.b.a(context, context.getContentResolver(), buildUpon.build(), new String[]{"address", "charset"}, "type=137", null, null);
        if (a2 != null) {
            try {
                if (a2.moveToFirst()) {
                    String string = a2.getString(0);
                    if (!TextUtils.isEmpty(string)) {
                        String c = new w(a2.getInt(1), com.sd.a.a.a.a.d.a(string)).c();
                        a2.close();
                        return c;
                    }
                }
                a2.close();
            } catch (Throwable th) {
                a2.close();
                throw th;
            }
        }
        return context.getString(C0000R.string.hidden_sender_address);
    }
}
