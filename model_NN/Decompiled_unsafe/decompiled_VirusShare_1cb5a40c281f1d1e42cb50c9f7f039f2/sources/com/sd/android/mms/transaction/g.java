package com.sd.android.mms.transaction;

public abstract class g {

    /* renamed from: a  reason: collision with root package name */
    protected int f115a;
    private int b;
    private int c;

    private g() {
        this(1, 0);
    }

    public g(int i, int i2) {
        this.b = i;
        this.f115a = i2;
        this.c = 1;
    }
}
