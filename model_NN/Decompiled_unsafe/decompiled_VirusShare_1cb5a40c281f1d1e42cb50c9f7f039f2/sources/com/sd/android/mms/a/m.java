package com.sd.android.mms.a;

import android.text.TextUtils;
import android.util.Log;
import com.sd.a.a.a.a.e;
import com.sd.a.a.a.a.i;
import com.sd.a.a.a.b;
import com.sd.android.mms.b.a.a.c;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import org.b.a.a.h;
import org.b.a.a.j;
import org.b.a.a.k;
import org.b.a.a.n;
import org.b.a.a.o;
import org.xml.sax.SAXException;

public final class m {
    private m() {
    }

    private static j a(String str, o oVar, String str2) {
        j jVar = (j) oVar.createElement(str);
        jVar.a(str2);
        return jVar;
    }

    private static n a(o oVar) {
        n nVar = (n) oVar.createElement("par");
        nVar.b(8.0f);
        oVar.j().appendChild(nVar);
        return nVar;
    }

    public static o a(e eVar) {
        i iVar;
        int a2 = eVar.a();
        int i = 0;
        while (true) {
            if (i >= a2) {
                iVar = null;
                break;
            }
            i a3 = eVar.a(i);
            if (Arrays.equals(a3.g(), "application/smil".getBytes())) {
                iVar = a3;
                break;
            }
            i++;
        }
        o a4 = iVar != null ? a(iVar) : null;
        return a4 == null ? b(eVar) : a4;
    }

    private static o a(i iVar) {
        try {
            byte[] a2 = iVar.a();
            if (a2 != null) {
                return new c().a(new ByteArrayInputStream(a2));
            }
        } catch (IOException e) {
            Log.e("SmilHelper", "Failed to parse SMIL document.", e);
        } catch (SAXException e2) {
            Log.e("SmilHelper", "Failed to parse SMIL document.", e2);
        } catch (b e3) {
            Log.e("SmilHelper", "Failed to parse SMIL document.", e3);
        }
        return null;
    }

    public static o a(a aVar) {
        j a2;
        boolean z;
        boolean z2;
        com.sd.android.mms.b.a.e eVar = new com.sd.android.mms.b.a.e();
        org.b.a.a.i iVar = (org.b.a.a.i) eVar.createElement("smil");
        eVar.appendChild(iVar);
        org.b.a.a.i iVar2 = (org.b.a.a.i) eVar.createElement("head");
        iVar.appendChild(iVar2);
        h hVar = (h) eVar.createElement("layout");
        iVar2.appendChild(hVar);
        k kVar = (k) eVar.createElement("root-layout");
        f c = aVar.c();
        kVar.b(c.e());
        kVar.a(c.f());
        String h = c.h();
        if (!TextUtils.isEmpty(h)) {
            kVar.a(h);
        }
        hVar.appendChild(kVar);
        ArrayList c2 = c.c();
        ArrayList arrayList = new ArrayList();
        Iterator it = c2.iterator();
        while (it.hasNext()) {
            j jVar = (j) it.next();
            org.b.a.a.m mVar = (org.b.a.a.m) eVar.createElement("region");
            mVar.b(jVar.a());
            mVar.c(jVar.c());
            mVar.d(jVar.e());
            mVar.b(jVar.f());
            mVar.a(jVar.h());
            mVar.c(jVar.b());
            arrayList.add(mVar);
        }
        iVar.appendChild((org.b.a.a.i) eVar.createElement("body"));
        Iterator it2 = aVar.iterator();
        boolean z3 = false;
        boolean z4 = false;
        while (it2.hasNext()) {
            h hVar2 = (h) it2.next();
            n a3 = a(eVar);
            a3.b(((float) hVar2.a()) / 1000.0f);
            a((org.b.a.b.e) a3, hVar2);
            Iterator it3 = hVar2.iterator();
            boolean z5 = z4;
            boolean z6 = z3;
            while (it3.hasNext()) {
                e eVar2 = (e) it3.next();
                String k = eVar2.k();
                if (eVar2 instanceof p) {
                    if (!TextUtils.isEmpty(((p) eVar2).x())) {
                        j a4 = a("text", eVar, k);
                        j jVar2 = a4;
                        z = z6;
                        z2 = a((org.b.a.a.e) a4, arrayList, hVar, "Text", z5);
                        a2 = jVar2;
                    }
                } else if (eVar2 instanceof q) {
                    j a5 = a("img", eVar, k);
                    boolean a6 = a((org.b.a.a.e) a5, arrayList, hVar, "Image", z6);
                    z2 = z5;
                    boolean z7 = a6;
                    a2 = a5;
                    z = z7;
                } else if (eVar2 instanceof l) {
                    j a7 = a("video", eVar, k);
                    boolean a8 = a((org.b.a.a.e) a7, arrayList, hVar, "Image", z6);
                    z2 = z5;
                    boolean z8 = a8;
                    a2 = a7;
                    z = z8;
                } else if (eVar2 instanceof r) {
                    a2 = a("audio", eVar, k);
                    z = z6;
                    z2 = z5;
                } else {
                    Log.w("SmilHelper", "Unsupport media: " + eVar2);
                }
                int c3 = eVar2.c();
                if (c3 != 0) {
                    a2.setAttribute("begin", String.valueOf(c3 / 1000));
                }
                int e = eVar2.e();
                if (e != 0) {
                    a2.b(((float) e) / 1000.0f);
                }
                a3.appendChild(a2);
                a((org.b.a.b.e) a2, eVar2);
                z5 = z2;
                z6 = z;
            }
            z3 = z6;
            z4 = z5;
        }
        return eVar;
    }

    static void a(org.b.a.b.e eVar, e eVar2) {
        eVar.a("SmilMediaStart", eVar2, false);
        eVar.a("SmilMediaEnd", eVar2, false);
        eVar.a("SmilMediaPause", eVar2, false);
        eVar.a("SmilMediaSeek", eVar2, false);
    }

    static void a(org.b.a.b.e eVar, h hVar) {
        eVar.a("SmilSlideStart", hVar, false);
        eVar.a("SmilSlideEnd", hVar, false);
    }

    private static boolean a(org.b.a.a.e eVar, ArrayList arrayList, h hVar, String str, boolean z) {
        org.b.a.a.m mVar;
        Iterator it = arrayList.iterator();
        while (true) {
            if (!it.hasNext()) {
                mVar = null;
                break;
            }
            mVar = (org.b.a.a.m) it.next();
            if (mVar.j().equals(str)) {
                break;
            }
        }
        if (z || mVar == null) {
            return false;
        }
        eVar.a(mVar);
        hVar.appendChild(mVar);
        return true;
    }

    private static o b(e eVar) {
        com.sd.android.mms.b.a.e eVar2 = new com.sd.android.mms.b.a.e();
        org.b.a.a.i iVar = (org.b.a.a.i) eVar2.createElement("smil");
        iVar.setAttribute("xmlns", "http://www.w3.org/2001/SMIL20/Language");
        eVar2.appendChild(iVar);
        org.b.a.a.i iVar2 = (org.b.a.a.i) eVar2.createElement("head");
        iVar.appendChild(iVar2);
        iVar2.appendChild((h) eVar2.createElement("layout"));
        iVar.appendChild((org.b.a.a.i) eVar2.createElement("body"));
        n a2 = a(eVar2);
        int a3 = eVar.a();
        if (a3 == 0) {
            return eVar2;
        }
        boolean z = false;
        boolean z2 = false;
        n nVar = a2;
        for (int i = 0; i < a3; i++) {
            if (nVar == null || (z && z2)) {
                z2 = false;
                nVar = a(eVar2);
                z = false;
            }
            i a4 = eVar.a(i);
            String str = new String(a4.g());
            if (com.sd.a.a.a.c.e(str)) {
                try {
                    str = new com.sd.android.mms.e.b(str, a4.b(), a4.a()).d();
                } catch (android.drm.mobile1.b e) {
                    Log.e("SmilHelper", e.getMessage(), e);
                } catch (IOException e2) {
                    Log.e("SmilHelper", e2.getMessage(), e2);
                }
            }
            if (str.equals("text/plain") || str.equalsIgnoreCase("application/vnd.wap.xhtml+xml")) {
                nVar.appendChild(a("text", eVar2, a4.k()));
                z2 = true;
            } else if (com.sd.a.a.a.c.b(str)) {
                nVar.appendChild(a("img", eVar2, a4.k()));
                z = true;
            } else if (com.sd.a.a.a.c.d(str)) {
                nVar.appendChild(a("video", eVar2, a4.k()));
                z = true;
            } else if (com.sd.a.a.a.c.c(str)) {
                nVar.appendChild(a("audio", eVar2, a4.k()));
                z = true;
            } else {
                Log.w("SmilHelper", "unsupport media type");
            }
        }
        return eVar2;
    }
}
