package com.sd.android.mms.b;

import com.sd.android.mms.b.b.a;
import java.util.NoSuchElementException;
import java.util.Vector;
import org.b.a.b.c;
import org.b.a.b.d;
import org.b.a.b.e;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public abstract class f implements e, Node {

    /* renamed from: a  reason: collision with root package name */
    private Node f89a;
    c b;
    private final Vector c = new Vector();
    private final e d = new a(this);

    protected f(c cVar) {
        this.b = cVar;
    }

    public final void a(String str, d dVar, boolean z) {
        this.d.a(str, dVar, z);
    }

    public final boolean a(c cVar) {
        return this.d.a(cVar);
    }

    public Node appendChild(Node node) {
        ((f) node).f89a = this;
        this.c.remove(node);
        this.c.add(node);
        return node;
    }

    public final void b(String str, d dVar, boolean z) {
        this.d.b(str, dVar, z);
    }

    public Node cloneNode(boolean z) {
        return null;
    }

    public NamedNodeMap getAttributes() {
        return null;
    }

    public NodeList getChildNodes() {
        return new a(this, null, false);
    }

    public Node getFirstChild() {
        try {
            return (Node) this.c.firstElement();
        } catch (NoSuchElementException e) {
            return null;
        }
    }

    public Node getLastChild() {
        try {
            return (Node) this.c.lastElement();
        } catch (NoSuchElementException e) {
            return null;
        }
    }

    public String getLocalName() {
        return null;
    }

    public String getNamespaceURI() {
        return null;
    }

    public Node getNextSibling() {
        if (this.f89a == null || this == this.f89a.getLastChild()) {
            return null;
        }
        Vector vector = ((f) this.f89a).c;
        return (Node) vector.elementAt(vector.indexOf(this) + 1);
    }

    public String getNodeValue() {
        return null;
    }

    public Document getOwnerDocument() {
        return this.b;
    }

    public Node getParentNode() {
        return this.f89a;
    }

    public String getPrefix() {
        return null;
    }

    public Node getPreviousSibling() {
        if (this.f89a == null || this == this.f89a.getFirstChild()) {
            return null;
        }
        Vector vector = ((f) this.f89a).c;
        return (Node) vector.elementAt(vector.indexOf(this) - 1);
    }

    public boolean hasAttributes() {
        return false;
    }

    public boolean hasChildNodes() {
        return !this.c.isEmpty();
    }

    public Node insertBefore(Node node, Node node2) {
        return null;
    }

    public boolean isSupported(String str, String str2) {
        return false;
    }

    public void normalize() {
    }

    public Node removeChild(Node node) {
        if (this.c.contains(node)) {
            this.c.remove(node);
            ((f) node).f89a = null;
            return null;
        }
        throw new DOMException(8, "Child does not exist");
    }

    public Node replaceChild(Node node, Node node2) {
        if (this.c.contains(node2)) {
            try {
                this.c.remove(node);
            } catch (DOMException e) {
            }
            this.c.setElementAt(node, this.c.indexOf(node2));
            ((f) node).f89a = this;
            ((f) node2).f89a = null;
            return node2;
        }
        throw new DOMException(8, "Old child does not exist");
    }

    public void setNodeValue(String str) {
    }

    public void setPrefix(String str) {
    }
}
