package com.sd.android.mms.transaction;

import android.net.Uri;

public final class d {

    /* renamed from: a  reason: collision with root package name */
    private Uri f112a = null;
    private int b = 0;

    public final synchronized int a() {
        return this.b;
    }

    /* access modifiers changed from: package-private */
    public final synchronized void a(int i) {
        if (i >= 0 || i <= 2) {
            this.b = i;
        } else {
            throw new IllegalArgumentException("Bad state: " + i);
        }
    }

    /* access modifiers changed from: package-private */
    public final synchronized void a(Uri uri) {
        this.f112a = uri;
    }

    public final synchronized Uri b() {
        return this.f112a;
    }
}
