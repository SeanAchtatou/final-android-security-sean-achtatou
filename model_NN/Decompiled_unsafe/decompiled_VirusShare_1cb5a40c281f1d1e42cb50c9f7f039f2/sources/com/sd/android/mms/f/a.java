package com.sd.android.mms.f;

import android.content.Context;
import android.net.Uri;
import android.util.Log;
import com.sd.a.a.a.a.i;

public final class a {

    /* renamed from: a  reason: collision with root package name */
    private final Context f108a;
    private final Uri b;
    private String c;
    private String d;
    private String e;
    private int f;
    private int g;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x0175 A[SYNTHETIC, Splitter:B:63:0x0175] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public a(android.content.Context r10, android.net.Uri r11) {
        /*
            r9 = this;
            r7 = 1
            r3 = 0
            r9.<init>()
            if (r10 == 0) goto L_0x0009
            if (r11 != 0) goto L_0x000f
        L_0x0009:
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            r0.<init>()
            throw r0
        L_0x000f:
            java.lang.String r0 = r11.getScheme()
            java.lang.String r1 = "content"
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L_0x0109
            android.content.ContentResolver r1 = r10.getContentResolver()
            r0 = r10
            r2 = r11
            r4 = r3
            r5 = r3
            r6 = r3
            android.database.Cursor r0 = com.sd.a.a.a.b.b.a(r0, r1, r2, r3, r4, r5, r6)
            if (r0 != 0) goto L_0x0049
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Query on "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r11)
            java.lang.String r2 = " returns null result."
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x0049:
            int r1 = r0.getCount()     // Catch:{ all -> 0x0074 }
            if (r1 != r7) goto L_0x0055
            boolean r1 = r0.moveToFirst()     // Catch:{ all -> 0x0074 }
            if (r1 != 0) goto L_0x0079
        L_0x0055:
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException     // Catch:{ all -> 0x0074 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x0074 }
            r2.<init>()     // Catch:{ all -> 0x0074 }
            java.lang.String r3 = "Query on "
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x0074 }
            java.lang.StringBuilder r2 = r2.append(r11)     // Catch:{ all -> 0x0074 }
            java.lang.String r3 = " returns 0 or multiple rows."
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x0074 }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x0074 }
            r1.<init>(r2)     // Catch:{ all -> 0x0074 }
            throw r1     // Catch:{ all -> 0x0074 }
        L_0x0074:
            r1 = move-exception
            r0.close()
            throw r1
        L_0x0079:
            boolean r1 = com.sd.android.mms.a.q.b(r11)     // Catch:{ all -> 0x0074 }
            if (r1 == 0) goto L_0x00f2
            java.lang.String r1 = "fn"
            int r1 = r0.getColumnIndexOrThrow(r1)     // Catch:{ all -> 0x0074 }
            java.lang.String r1 = r0.getString(r1)     // Catch:{ all -> 0x0074 }
            boolean r2 = android.text.TextUtils.isEmpty(r1)     // Catch:{ all -> 0x0074 }
            if (r2 == 0) goto L_0x0099
            java.lang.String r1 = "_data"
            int r1 = r0.getColumnIndexOrThrow(r1)     // Catch:{ all -> 0x0074 }
            java.lang.String r1 = r0.getString(r1)     // Catch:{ all -> 0x0074 }
        L_0x0099:
            java.lang.String r2 = "ct"
            int r2 = r0.getColumnIndexOrThrow(r2)     // Catch:{ all -> 0x0074 }
            java.lang.String r2 = r0.getString(r2)     // Catch:{ all -> 0x0074 }
            r9.c = r2     // Catch:{ all -> 0x0074 }
        L_0x00a5:
            r9.d = r1     // Catch:{ all -> 0x0074 }
            r0.close()
        L_0x00aa:
            java.lang.String r0 = r9.d
            java.lang.String r1 = r9.d
            r2 = 47
            int r1 = r1.lastIndexOf(r2)
            int r1 = r1 + 1
            java.lang.String r0 = r0.substring(r1)
            r9.e = r0
            java.lang.String r0 = r9.e
            r1 = 32
            r2 = 95
            java.lang.String r0 = r0.replace(r1, r2)
            r9.e = r0
            r9.f108a = r10
            r9.b = r11
            android.content.Context r0 = r9.f108a     // Catch:{ FileNotFoundException -> 0x0159, all -> 0x0171 }
            android.content.ContentResolver r0 = r0.getContentResolver()     // Catch:{ FileNotFoundException -> 0x0159, all -> 0x0171 }
            android.net.Uri r1 = r9.b     // Catch:{ FileNotFoundException -> 0x0159, all -> 0x0171 }
            java.io.InputStream r0 = r0.openInputStream(r1)     // Catch:{ FileNotFoundException -> 0x0159, all -> 0x0171 }
            android.graphics.BitmapFactory$Options r1 = new android.graphics.BitmapFactory$Options     // Catch:{ FileNotFoundException -> 0x0189, all -> 0x0182 }
            r1.<init>()     // Catch:{ FileNotFoundException -> 0x0189, all -> 0x0182 }
            r2 = 1
            r1.inJustDecodeBounds = r2     // Catch:{ FileNotFoundException -> 0x0189, all -> 0x0182 }
            r2 = 0
            android.graphics.BitmapFactory.decodeStream(r0, r2, r1)     // Catch:{ FileNotFoundException -> 0x0189, all -> 0x0182 }
            int r2 = r1.outWidth     // Catch:{ FileNotFoundException -> 0x0189, all -> 0x0182 }
            r9.f = r2     // Catch:{ FileNotFoundException -> 0x0189, all -> 0x0182 }
            int r1 = r1.outHeight     // Catch:{ FileNotFoundException -> 0x0189, all -> 0x0182 }
            r9.g = r1     // Catch:{ FileNotFoundException -> 0x0189, all -> 0x0182 }
            if (r0 == 0) goto L_0x00f1
            r0.close()     // Catch:{ IOException -> 0x0150 }
        L_0x00f1:
            return
        L_0x00f2:
            java.lang.String r1 = "_data"
            int r1 = r0.getColumnIndexOrThrow(r1)     // Catch:{ all -> 0x0074 }
            java.lang.String r1 = r0.getString(r1)     // Catch:{ all -> 0x0074 }
            java.lang.String r2 = "mime_type"
            int r2 = r0.getColumnIndexOrThrow(r2)     // Catch:{ all -> 0x0074 }
            java.lang.String r2 = r0.getString(r2)     // Catch:{ all -> 0x0074 }
            r9.c = r2     // Catch:{ all -> 0x0074 }
            goto L_0x00a5
        L_0x0109:
            java.lang.String r0 = r11.getScheme()
            java.lang.String r1 = "file"
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L_0x00aa
            android.webkit.MimeTypeMap r0 = android.webkit.MimeTypeMap.getSingleton()
            java.lang.String r1 = r11.toString()
            java.lang.String r1 = android.webkit.MimeTypeMap.getFileExtensionFromUrl(r1)
            java.lang.String r0 = r0.getMimeTypeFromExtension(r1)
            r9.c = r0
            java.lang.String r0 = r9.c
            if (r0 != 0) goto L_0x0148
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Unable to determine extension for "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = r11.toString()
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x0148:
            java.lang.String r0 = r11.getPath()
            r9.d = r0
            goto L_0x00aa
        L_0x0150:
            r0 = move-exception
            java.lang.String r1 = "UriImage"
            java.lang.String r2 = "IOException caught while closing stream"
            android.util.Log.e(r1, r2, r0)
            goto L_0x00f1
        L_0x0159:
            r0 = move-exception
            r1 = r3
        L_0x015b:
            java.lang.String r2 = "UriImage"
            java.lang.String r3 = "IOException caught while opening stream"
            android.util.Log.e(r2, r3, r0)     // Catch:{ all -> 0x0187 }
            if (r1 == 0) goto L_0x00f1
            r1.close()     // Catch:{ IOException -> 0x0168 }
            goto L_0x00f1
        L_0x0168:
            r0 = move-exception
            java.lang.String r1 = "UriImage"
            java.lang.String r2 = "IOException caught while closing stream"
            android.util.Log.e(r1, r2, r0)
            goto L_0x00f1
        L_0x0171:
            r0 = move-exception
            r1 = r3
        L_0x0173:
            if (r1 == 0) goto L_0x0178
            r1.close()     // Catch:{ IOException -> 0x0179 }
        L_0x0178:
            throw r0
        L_0x0179:
            r1 = move-exception
            java.lang.String r2 = "UriImage"
            java.lang.String r3 = "IOException caught while closing stream"
            android.util.Log.e(r2, r3, r1)
            goto L_0x0178
        L_0x0182:
            r1 = move-exception
            r8 = r1
            r1 = r0
            r0 = r8
            goto L_0x0173
        L_0x0187:
            r0 = move-exception
            goto L_0x0173
        L_0x0189:
            r1 = move-exception
            r8 = r1
            r1 = r0
            r0 = r8
            goto L_0x015b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.sd.android.mms.f.a.<init>(android.content.Context, android.net.Uri):void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:40:0x00a3 A[SYNTHETIC, Splitter:B:40:0x00a3] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private byte[] b(int r8, int r9) {
        /*
            r7 = this;
            r6 = 0
            int r0 = r7.f
            int r1 = r7.g
            r2 = 1
        L_0x0006:
            int r3 = r0 / r2
            if (r3 > r8) goto L_0x000e
            int r3 = r1 / r2
            if (r3 <= r9) goto L_0x0011
        L_0x000e:
            int r2 = r2 * 2
            goto L_0x0006
        L_0x0011:
            java.lang.String r3 = "UriImage"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "outWidth="
            java.lang.StringBuilder r4 = r4.append(r5)
            int r0 = r0 / r2
            java.lang.StringBuilder r0 = r4.append(r0)
            java.lang.String r4 = " outHeight="
            java.lang.StringBuilder r0 = r0.append(r4)
            int r1 = r1 / r2
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            android.util.Log.v(r3, r0)
            android.graphics.BitmapFactory$Options r0 = new android.graphics.BitmapFactory$Options
            r0.<init>()
            r0.inSampleSize = r2
            android.content.Context r1 = r7.f108a     // Catch:{ FileNotFoundException -> 0x0082, all -> 0x009f }
            android.content.ContentResolver r1 = r1.getContentResolver()     // Catch:{ FileNotFoundException -> 0x0082, all -> 0x009f }
            android.net.Uri r2 = r7.b     // Catch:{ FileNotFoundException -> 0x0082, all -> 0x009f }
            java.io.InputStream r1 = r1.openInputStream(r2)     // Catch:{ FileNotFoundException -> 0x0082, all -> 0x009f }
            r2 = 0
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeStream(r1, r2, r0)     // Catch:{ FileNotFoundException -> 0x00b4 }
            if (r0 != 0) goto L_0x0061
            if (r1 == 0) goto L_0x0054
            r1.close()     // Catch:{ IOException -> 0x0056 }
        L_0x0054:
            r0 = r6
        L_0x0055:
            return r0
        L_0x0056:
            r0 = move-exception
            java.lang.String r1 = "UriImage"
            java.lang.String r2 = r0.getMessage()
            android.util.Log.e(r1, r2, r0)
            goto L_0x0054
        L_0x0061:
            java.io.ByteArrayOutputStream r2 = new java.io.ByteArrayOutputStream     // Catch:{ FileNotFoundException -> 0x00b4 }
            r2.<init>()     // Catch:{ FileNotFoundException -> 0x00b4 }
            android.graphics.Bitmap$CompressFormat r3 = android.graphics.Bitmap.CompressFormat.JPEG     // Catch:{ FileNotFoundException -> 0x00b4 }
            r4 = 80
            r0.compress(r3, r4, r2)     // Catch:{ FileNotFoundException -> 0x00b4 }
            byte[] r0 = r2.toByteArray()     // Catch:{ FileNotFoundException -> 0x00b4 }
            if (r1 == 0) goto L_0x0055
            r1.close()     // Catch:{ IOException -> 0x0077 }
            goto L_0x0055
        L_0x0077:
            r1 = move-exception
            java.lang.String r2 = "UriImage"
            java.lang.String r3 = r1.getMessage()
            android.util.Log.e(r2, r3, r1)
            goto L_0x0055
        L_0x0082:
            r0 = move-exception
            r1 = r6
        L_0x0084:
            java.lang.String r2 = "UriImage"
            java.lang.String r3 = r0.getMessage()     // Catch:{ all -> 0x00b2 }
            android.util.Log.e(r2, r3, r0)     // Catch:{ all -> 0x00b2 }
            if (r1 == 0) goto L_0x0092
            r1.close()     // Catch:{ IOException -> 0x0094 }
        L_0x0092:
            r0 = r6
            goto L_0x0055
        L_0x0094:
            r0 = move-exception
            java.lang.String r1 = "UriImage"
            java.lang.String r2 = r0.getMessage()
            android.util.Log.e(r1, r2, r0)
            goto L_0x0092
        L_0x009f:
            r0 = move-exception
            r1 = r6
        L_0x00a1:
            if (r1 == 0) goto L_0x00a6
            r1.close()     // Catch:{ IOException -> 0x00a7 }
        L_0x00a6:
            throw r0
        L_0x00a7:
            r1 = move-exception
            java.lang.String r2 = "UriImage"
            java.lang.String r3 = r1.getMessage()
            android.util.Log.e(r2, r3, r1)
            goto L_0x00a6
        L_0x00b2:
            r0 = move-exception
            goto L_0x00a1
        L_0x00b4:
            r0 = move-exception
            goto L_0x0084
        */
        throw new UnsupportedOperationException("Method not decompiled: com.sd.android.mms.f.a.b(int, int):byte[]");
    }

    public final i a(int i, int i2) {
        i iVar = new i();
        byte[] b2 = b(i, i2);
        if (b2 == null) {
            Log.v("UriImage", "Resize image failed.");
            return null;
        }
        iVar.a(b2);
        iVar.e(this.c.getBytes());
        String str = this.e;
        byte[] bytes = str.getBytes();
        iVar.c(bytes);
        iVar.h(bytes);
        if (str.lastIndexOf(".") != -1) {
            iVar.b(str.substring(0, str.lastIndexOf(".")).getBytes());
            return iVar;
        }
        iVar.b(str.getBytes());
        return iVar;
    }

    public final String a() {
        return this.c;
    }

    public final String b() {
        return this.e;
    }

    public final int c() {
        return this.f;
    }

    public final int d() {
        return this.g;
    }
}
