package com.sd.android.mms.b.b;

import android.util.Log;
import java.util.ArrayList;
import org.b.a.b.b;
import org.b.a.b.c;
import org.b.a.b.d;
import org.b.a.b.e;

public final class a implements e {

    /* renamed from: a  reason: collision with root package name */
    private ArrayList f84a;
    private e b;

    public a(e eVar) {
        this.b = eVar;
    }

    public final void a(String str, d dVar, boolean z) {
        if (str != null && !str.equals("") && dVar != null) {
            b(str, dVar, z);
            if (this.f84a == null) {
                this.f84a = new ArrayList();
            }
            this.f84a.add(new b(str, dVar, z));
        }
    }

    public final boolean a(c cVar) {
        c cVar2 = (c) cVar;
        if (!cVar2.c()) {
            throw new b("Event not initialized");
        } else if (cVar2.a() == null || cVar2.a().equals("")) {
            throw new b("Unspecified even type");
        } else {
            cVar2.a(this.b);
            cVar2.f();
            cVar2.b(this.b);
            if (!cVar2.e() && this.f84a != null) {
                int i = 0;
                while (true) {
                    int i2 = i;
                    if (i2 >= this.f84a.size()) {
                        break;
                    }
                    b bVar = (b) this.f84a.get(i2);
                    if (!bVar.c && bVar.f85a.equals(cVar2.a())) {
                        try {
                            bVar.b.a(cVar2);
                        } catch (Exception e) {
                            Log.w("EventTargetImpl", "Catched EventListener exception", e);
                        }
                    }
                    i = i2 + 1;
                }
            }
            return cVar2.d();
        }
    }

    public final void b(String str, d dVar, boolean z) {
        if (this.f84a != null) {
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 < this.f84a.size()) {
                    b bVar = (b) this.f84a.get(i2);
                    if (bVar.c == z && bVar.b == dVar && bVar.f85a.equals(str)) {
                        this.f84a.remove(i2);
                        return;
                    }
                    i = i2 + 1;
                } else {
                    return;
                }
            }
        }
    }
}
