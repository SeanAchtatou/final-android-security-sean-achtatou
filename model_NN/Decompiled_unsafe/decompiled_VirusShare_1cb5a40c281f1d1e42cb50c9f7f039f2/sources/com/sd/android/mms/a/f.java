package com.sd.android.mms.a;

import com.sd.android.mms.c.a;
import com.sd.android.mms.c.c;
import java.util.ArrayList;
import java.util.Iterator;

public final class f extends c {

    /* renamed from: a  reason: collision with root package name */
    private int f63a = 0;
    private j c;
    private j d;
    private j e;
    private ArrayList f;
    private c g = a.a().b();

    public f() {
        i();
        j();
        k();
    }

    public f(j jVar, ArrayList arrayList) {
        this.c = jVar;
        this.f = new ArrayList();
        Iterator it = arrayList.iterator();
        while (it.hasNext()) {
            j jVar2 = (j) it.next();
            String a2 = jVar2.a();
            if (a2.equals("Image")) {
                this.d = jVar2;
            } else if (a2.equals("Text")) {
                this.e = jVar2;
            } else {
                this.f.add(jVar2);
            }
        }
        if (this.c == null) {
            i();
        }
        if (this.d == null) {
            j();
        }
        if (this.e == null) {
            k();
        }
    }

    private void i() {
        this.c = new j(null, 0, this.g.a(), this.g.b());
    }

    private void j() {
        if (this.c == null) {
            throw new IllegalStateException("Root-Layout uninitialized.");
        }
        this.d = new j("Image", 0, this.c.f(), this.g.c());
    }

    private void k() {
        if (this.c == null) {
            throw new IllegalStateException("Root-Layout uninitialized.");
        }
        this.e = new j("Text", this.g.c(), this.c.f(), this.g.d());
    }

    public final j a() {
        return this.d;
    }

    public final j a(String str) {
        if ("Image".equals(str)) {
            return this.d;
        }
        if ("Text".equals(str)) {
            return this.e;
        }
        Iterator it = this.f.iterator();
        while (it.hasNext()) {
            j jVar = (j) it.next();
            if (jVar.a().equals(str)) {
                return jVar;
            }
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public final void a(o oVar) {
        if (this.c != null) {
            this.c.c(oVar);
        }
        if (this.d != null) {
            this.d.c(oVar);
        }
        if (this.e != null) {
            this.e.c(oVar);
        }
    }

    public final j b() {
        return this.e;
    }

    /* access modifiers changed from: protected */
    public final void b(o oVar) {
        if (this.c != null) {
            this.c.d(oVar);
        }
        if (this.d != null) {
            this.d.d(oVar);
        }
        if (this.e != null) {
            this.e.d(oVar);
        }
    }

    public final ArrayList c() {
        ArrayList arrayList = new ArrayList();
        if (this.d != null) {
            arrayList.add(this.d);
        }
        if (this.e != null) {
            arrayList.add(this.e);
        }
        return arrayList;
    }

    /* access modifiers changed from: protected */
    public final void d() {
        if (this.c != null) {
            this.c.g();
        }
        if (this.d != null) {
            this.d.g();
        }
        if (this.e != null) {
            this.e.g();
        }
    }

    public final int e() {
        return this.c.f();
    }

    public final int f() {
        return this.c.h();
    }

    public final String h() {
        return this.c.i();
    }
}
