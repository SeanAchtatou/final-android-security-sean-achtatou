package com.sd.android.mms.b.a;

import org.b.a.a.g;
import org.b.a.a.j;

public class l extends o implements j {

    /* renamed from: a  reason: collision with root package name */
    private org.b.a.a.l f78a = new b(this, this);

    l(e eVar, String str) {
        super(eVar, str);
    }

    public final float a() {
        return this.f78a.a();
    }

    public final void a(String str) {
        setAttribute("src", str);
    }

    public final void a_(float f) {
        this.f78a.a_(f);
    }

    public final void b(float f) {
        this.f78a.b(f);
    }

    public final boolean b() {
        return this.f78a.b();
    }

    public final boolean c() {
        return this.f78a.c();
    }

    public final void d() {
        this.f78a.d();
    }

    public final void e() {
        this.f78a.e();
    }

    public final g f() {
        return this.f78a.f();
    }

    public final String g() {
        return getAttribute("src");
    }

    public final g h() {
        return this.f78a.h();
    }

    public final short i() {
        return this.f78a.i();
    }
}
