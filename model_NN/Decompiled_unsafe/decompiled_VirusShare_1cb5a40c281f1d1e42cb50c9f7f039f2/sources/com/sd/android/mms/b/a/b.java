package com.sd.android.mms.b.a;

import android.util.Log;
import org.b.a.a.i;
import org.b.a.a.l;
import org.b.a.b.a;
import org.b.a.b.c;

final class b extends k {
    private /* synthetic */ l b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    b(l lVar, i iVar) {
        super(iVar);
        this.b = lVar;
    }

    private c a(String str) {
        c a2 = ((a) this.b.getOwnerDocument()).a("Event");
        a2.a(str);
        return a2;
    }

    public final float a() {
        float a2 = super.a();
        if (a2 != 0.0f) {
            return a2;
        }
        String tagName = this.b.getTagName();
        if (tagName.equals("video") || tagName.equals("audio")) {
            return -1.0f;
        }
        if (tagName.equals("text") || tagName.equals("img")) {
            return 0.0f;
        }
        Log.w("SmilMediaElementImpl", "Unknown media type");
        return a2;
    }

    public final void a_(float f) {
        c a2 = ((a) this.b.getOwnerDocument()).a("Event");
        a2.a("SmilMediaSeek", (int) f);
        this.b.a(a2);
    }

    public final boolean b() {
        this.b.a(a("SmilMediaStart"));
        return true;
    }

    public final boolean c() {
        this.b.a(a("SmilMediaEnd"));
        return true;
    }

    /* access modifiers changed from: package-private */
    public final l c_() {
        return ((v) this.f77a.getParentNode()).f82a;
    }

    public final void d() {
        this.b.a(a("SmilMediaStart"));
    }

    public final void e() {
        this.b.a(a("SmilMediaPause"));
    }
}
