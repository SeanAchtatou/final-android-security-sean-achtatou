package com.sd.android.mms.b.b;

import org.b.a.b.e;

public final class c implements org.b.a.b.c {

    /* renamed from: a  reason: collision with root package name */
    private String f86a;
    private boolean b;
    private boolean c;
    private boolean d;
    private e e;
    private short f;
    private boolean g;
    private boolean h;
    private e i;
    private int j;
    private final long k = System.currentTimeMillis();

    public final String a() {
        return this.f86a;
    }

    public final void a(String str) {
        this.f86a = str;
        this.b = false;
        this.c = false;
        this.d = true;
    }

    public final void a(String str, int i2) {
        this.j = i2;
        a(str);
    }

    /* access modifiers changed from: package-private */
    public final void a(e eVar) {
        this.e = eVar;
    }

    public final int b() {
        return this.j;
    }

    /* access modifiers changed from: package-private */
    public final void b(e eVar) {
        this.i = eVar;
    }

    /* access modifiers changed from: package-private */
    public final boolean c() {
        return this.d;
    }

    /* access modifiers changed from: package-private */
    public final boolean d() {
        return this.h;
    }

    /* access modifiers changed from: package-private */
    public final boolean e() {
        return this.g;
    }

    /* access modifiers changed from: package-private */
    public final void f() {
        this.f = 2;
    }
}
