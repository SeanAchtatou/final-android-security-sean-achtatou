package com.sd.android.mms.d;

import android.content.SharedPreferences;

final class m implements SharedPreferences.OnSharedPreferenceChangeListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ l f103a;

    m(l lVar) {
        this.f103a = lVar;
    }

    public final void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String str) {
        if ("pref_key_mms_auto_retrieval".equals(str) || "pref_key_mms_retrieval_during_roaming".equals(str)) {
            synchronized (l.g) {
                boolean unused = this.f103a.d = l.a(sharedPreferences);
            }
        }
    }
}
