package com.sd.android.mms.d;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

final class h extends BroadcastReceiver {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ c f98a;

    h(c cVar) {
        this.f98a = cVar;
    }

    public final void onReceive(Context context, Intent intent) {
        if ("com.android.mms.RATE_LIMIT_CONFIRMED".equals(intent.getAction())) {
            synchronized (this) {
                int unused = this.f98a.d = intent.getBooleanExtra("answer", false) ? 1 : 2;
                notifyAll();
            }
        }
    }
}
