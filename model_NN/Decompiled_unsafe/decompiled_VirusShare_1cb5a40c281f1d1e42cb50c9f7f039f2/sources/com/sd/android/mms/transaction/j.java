package com.sd.android.mms.transaction;

import android.a.c;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.text.TextUtils;
import android.util.Log;
import com.sd.a.a.a.b.b;

public final class j {
    private static final String[] e = {"type", "mmsc", "mmsproxy", "mmsport", "name"};

    /* renamed from: a  reason: collision with root package name */
    private String f118a;
    private String b;
    private String c;
    private int d = -1;

    public j(Context context, String str) {
        boolean z;
        String string;
        Cursor a2 = b.a(context, context.getContentResolver(), Uri.withAppendedPath(c.f6a, "current"), e, str != null ? "apn='" + str.trim() + "'" : null, null, null);
        if (a2 == null) {
            Log.e("JB-TransactionSettings", "Apn is not found in Database!");
            return;
        }
        "number of apn queried is " + a2.getCount();
        "device information is " + Build.DEVICE + " " + Build.MODEL;
        boolean z2 = false;
        while (a2.moveToNext() && (this.f118a == null || TextUtils.isEmpty(this.f118a))) {
            try {
                "apn COLUMN_TYPE=" + a2.getString(0) + " COLUMN_MMSC=" + a2.getString(1) + " COLUMN_MMSPROXY=" + a2.getString(2) + " COLUMN_MMSPORT=" + a2.getString(3);
                if (a(a2.getString(0), "mms")) {
                    this.c = a2.getString(4).trim();
                    if (this.c.equals("ctwap")) {
                        this.f118a = "http://mmsc.vnet.mobi";
                        this.b = "10.0.0.200";
                        this.d = 80;
                    } else {
                        this.f118a = a2.getString(1).trim();
                        this.b = a2.getString(2);
                    }
                    if (d()) {
                        string = a2.getString(3);
                        if (string != null) {
                            if (string.length() != 0) {
                                this.d = Integer.parseInt(string);
                            }
                        }
                        z2 = true;
                    } else {
                        z2 = true;
                    }
                }
            } catch (NumberFormatException e2) {
                if (!TextUtils.isEmpty(string)) {
                    "bad port number format: " + string + " exception:" + e2;
                }
                Log.e("JB-TransactionSettings", "Bad port number format: " + string, e2);
                z2 = true;
            } catch (Throwable th) {
                a2.close();
                throw th;
            }
        }
        a2.close();
        if (z2 || !"XT800".equals(Build.MODEL)) {
            z = z2;
        } else {
            this.f118a = "http://mmsc.vnet.mobi";
            this.b = "10.0.0.200";
            this.d = 80;
            z = true;
        }
        if (z && TextUtils.isEmpty(this.f118a)) {
            Log.e("JB-TransactionSettings", "Invalid APN setting: MMSC is empty");
        }
    }

    public j(String str, String str2, int i) {
        this.f118a = str;
        this.b = str2;
        this.d = i;
    }

    private static boolean a(String str, String str2) {
        if (TextUtils.isEmpty(str)) {
            return true;
        }
        for (String str3 : str.split(",")) {
            if (str3.equals(str2) || str3.equals("*")) {
                return true;
            }
        }
        return false;
    }

    public final String a() {
        return this.f118a;
    }

    public final String b() {
        return this.b;
    }

    public final int c() {
        return this.d;
    }

    public final boolean d() {
        return (this.b == null || this.b.trim().length() == 0) ? false : true;
    }
}
