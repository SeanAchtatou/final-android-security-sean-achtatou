package com.sd.android.mms.transaction;

import android.a.h;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.preference.PreferenceManager;
import com.sd.a.a.a.a.d;
import com.sd.a.a.a.a.j;
import com.sd.a.a.a.a.r;
import com.sd.a.a.a.b;
import com.sd.android.mms.d.k;

public final class n implements o {

    /* renamed from: a  reason: collision with root package name */
    private final Context f121a;
    private final Uri b;

    public n(Context context, Uri uri) {
        this.f121a = context;
        this.b = uri;
        if (this.b == null) {
            throw new IllegalArgumentException("Null message URI.");
        }
    }

    public final boolean a(long j) {
        d a2 = d.a(this.f121a);
        r a3 = a2.a(this.b);
        if (a3.m() != 128) {
            throw new b("Invalid message: " + a3.m());
        }
        j jVar = (j) a3;
        SharedPreferences defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this.f121a);
        jVar.a(defaultSharedPreferences.getLong("pref_key_mms_expiry", 604800));
        jVar.c(defaultSharedPreferences.getInt("pref_key_mms_priority", 129));
        jVar.a(defaultSharedPreferences.getBoolean("pref_key_mms_delivery_reports", true) ? 128 : 129);
        jVar.b(defaultSharedPreferences.getBoolean("pref_key_mms_read_reports", true) ? 128 : 129);
        jVar.a("personal".getBytes());
        jVar.b(System.currentTimeMillis() / 1000);
        a2.a(this.b, jVar);
        a2.a(this.b, h.f11a);
        k.a(Long.valueOf(ContentUris.parseId(this.b)), j);
        this.f121a.startService(new Intent(this.f121a, MyTransactionService.class));
        return true;
    }
}
