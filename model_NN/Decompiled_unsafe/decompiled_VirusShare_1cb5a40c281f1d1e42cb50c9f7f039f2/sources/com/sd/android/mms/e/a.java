package com.sd.android.mms.e;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.net.Uri;
import android.util.Log;
import com.sd.a.a.a.b.b;
import java.io.IOException;
import java.io.OutputStream;

public final class a {

    /* renamed from: a  reason: collision with root package name */
    private static final Uri f106a = Uri.parse("content://mms/drm");

    private a() {
    }

    public static Uri a(Context context, b bVar) {
        OutputStream outputStream;
        ContentResolver contentResolver = context.getContentResolver();
        Uri a2 = b.a(context, contentResolver, f106a, new ContentValues(0));
        try {
            OutputStream openOutputStream = contentResolver.openOutputStream(a2);
            try {
                byte[] a3 = bVar.a();
                if (a3 != null) {
                    openOutputStream.write(a3);
                }
                if (openOutputStream != null) {
                    try {
                        openOutputStream.close();
                    } catch (IOException e) {
                        Log.e("DrmUtils", e.getMessage(), e);
                    }
                }
                return a2;
            } catch (Throwable th) {
                Throwable th2 = th;
                outputStream = openOutputStream;
                th = th2;
                if (outputStream != null) {
                    try {
                        outputStream.close();
                    } catch (IOException e2) {
                        Log.e("DrmUtils", e2.getMessage(), e2);
                    }
                }
                throw th;
            }
        } catch (Throwable th3) {
            th = th3;
            outputStream = null;
        }
    }
}
