package com.sd.android.mms.a;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import org.b.a.b.c;
import org.b.a.b.d;

public final class h extends c implements List, d {

    /* renamed from: a  reason: collision with root package name */
    private final ArrayList f64a;
    private e c;
    private e d;
    private e e;
    private e f;
    private boolean g;
    private boolean h;
    private boolean i;
    private int j;
    private boolean k;
    private short l;
    private int m;
    private a n;

    public h(int i2, ArrayList arrayList) {
        this.f64a = new ArrayList();
        this.g = true;
        this.h = true;
        this.i = true;
        this.k = true;
        this.j = i2;
        int i3 = 0;
        Iterator it = arrayList.iterator();
        while (true) {
            int i4 = i3;
            if (it.hasNext()) {
                e eVar = (e) it.next();
                b(eVar);
                i3 = eVar.e();
                if (i3 <= i4) {
                    i3 = i4;
                }
            } else {
                a(i4);
                return;
            }
        }
    }

    private h(a aVar) {
        this.f64a = new ArrayList();
        this.g = true;
        this.h = true;
        this.i = true;
        this.k = true;
        this.j = 5000;
        this.n = aVar;
    }

    public h(a aVar, byte b) {
        this(aVar);
    }

    private void a(e eVar, e eVar2) {
        int l2 = eVar2.l();
        if (eVar == null) {
            if (this.n != null) {
                this.n.c(l2);
            }
            this.f64a.add(eVar2);
            b(l2);
            d(l2);
        } else {
            int l3 = eVar.l();
            if (l2 > l3) {
                if (this.n != null) {
                    this.n.c(l2 - l3);
                }
                b(l2 - l3);
                d(l2 - l3);
            } else {
                c(l3 - l2);
                e(l3 - l2);
            }
            this.f64a.set(this.f64a.indexOf(eVar), eVar2);
            eVar.g();
        }
        Iterator it = this.b.iterator();
        while (it.hasNext()) {
            eVar2.c((o) it.next());
        }
    }

    private boolean a(Object obj) {
        if (!this.f64a.remove(obj)) {
            return false;
        }
        if (obj instanceof p) {
            this.c = null;
        } else if (obj instanceof q) {
            this.d = null;
            this.i = true;
        } else if (obj instanceof r) {
            this.e = null;
            this.i = true;
        } else if (obj instanceof l) {
            this.f = null;
            this.g = true;
            this.h = true;
        }
        int l2 = ((e) obj).l();
        c(l2);
        e(l2);
        ((c) obj).g();
        return true;
    }

    private void b(int i2) {
        if (i2 > 0) {
            this.m += i2;
        }
    }

    private void b(e eVar) {
        if (eVar != null) {
            if (eVar.m()) {
                a(this.c, eVar);
                this.c = eVar;
            } else if (eVar.n()) {
                if (this.g) {
                    a(this.d, eVar);
                    this.d = eVar;
                    this.i = false;
                    return;
                }
                throw new IllegalStateException();
            } else if (eVar.p()) {
                if (this.h) {
                    a(this.e, eVar);
                    this.e = eVar;
                    this.i = false;
                    return;
                }
                throw new IllegalStateException();
            } else if (!eVar.o()) {
            } else {
                if (this.i) {
                    a(this.f, eVar);
                    this.f = eVar;
                    this.g = false;
                    this.h = false;
                    return;
                }
                throw new IllegalStateException();
            }
        }
    }

    private void c(int i2) {
        if (i2 > 0) {
            this.m -= i2;
        }
    }

    private void d(int i2) {
        if (i2 > 0 && this.n != null) {
            this.n.a(this.n.b() + i2);
        }
    }

    private void e(int i2) {
        if (i2 > 0 && this.n != null) {
            this.n.a(this.n.b() - i2);
        }
    }

    public final int a() {
        return this.j;
    }

    public final void a(int i2) {
        if (i2 > 0) {
            if (i2 > this.j || this.j == 5000) {
                this.j = i2;
            }
        }
    }

    public final void a(a aVar) {
        this.n = aVar;
    }

    /* access modifiers changed from: protected */
    public final void a(o oVar) {
        Iterator it = this.f64a.iterator();
        while (it.hasNext()) {
            ((e) it.next()).c(oVar);
        }
    }

    public final void a(c cVar) {
        if (cVar.a().equals("SmilSlideStart")) {
            this.k = true;
        } else if (this.l != 1) {
            this.k = false;
        }
        a(false);
    }

    public final void a(short s) {
        this.l = s;
        a(true);
    }

    /* renamed from: a */
    public final boolean add(e eVar) {
        b(eVar);
        a(true);
        return true;
    }

    public final /* bridge */ /* synthetic */ void add(int i2, Object obj) {
        throw new UnsupportedOperationException("Operation not supported.");
    }

    public final boolean addAll(int i2, Collection collection) {
        throw new UnsupportedOperationException("Operation not supported.");
    }

    public final boolean addAll(Collection collection) {
        throw new UnsupportedOperationException("Operation not supported.");
    }

    public final int b() {
        return this.m;
    }

    /* access modifiers changed from: protected */
    public final void b(o oVar) {
        Iterator it = this.f64a.iterator();
        while (it.hasNext()) {
            ((e) it.next()).d(oVar);
        }
    }

    public final boolean c() {
        return this.k;
    }

    public final void clear() {
        if (this.f64a.size() > 0) {
            Iterator it = this.f64a.iterator();
            while (it.hasNext()) {
                e eVar = (e) it.next();
                eVar.g();
                int l2 = eVar.l();
                c(l2);
                e(l2);
            }
            this.f64a.clear();
            this.c = null;
            this.d = null;
            this.e = null;
            this.f = null;
            this.g = true;
            this.h = true;
            this.i = true;
            a(true);
        }
    }

    public final boolean contains(Object obj) {
        return this.f64a.contains(obj);
    }

    public final boolean containsAll(Collection collection) {
        return this.f64a.containsAll(collection);
    }

    /* access modifiers changed from: protected */
    public final void d() {
        Iterator it = this.f64a.iterator();
        while (it.hasNext()) {
            ((e) it.next()).g();
        }
    }

    public final boolean e() {
        return this.c != null;
    }

    public final boolean f() {
        return this.d != null;
    }

    public final /* bridge */ /* synthetic */ Object get(int i2) {
        return (e) this.f64a.get(i2);
    }

    public final boolean h() {
        return this.e != null;
    }

    public final boolean i() {
        return this.f != null;
    }

    public final int indexOf(Object obj) {
        return this.f64a.indexOf(obj);
    }

    public final boolean isEmpty() {
        return this.f64a.isEmpty();
    }

    public final Iterator iterator() {
        return this.f64a.iterator();
    }

    public final boolean j() {
        return remove(this.d);
    }

    public final boolean k() {
        return remove(this.e);
    }

    public final boolean l() {
        return remove(this.f);
    }

    public final int lastIndexOf(Object obj) {
        return this.f64a.lastIndexOf(obj);
    }

    public final ListIterator listIterator() {
        return this.f64a.listIterator();
    }

    public final ListIterator listIterator(int i2) {
        return this.f64a.listIterator(i2);
    }

    public final p m() {
        return (p) this.c;
    }

    public final q n() {
        return (q) this.d;
    }

    public final r o() {
        return (r) this.e;
    }

    public final l p() {
        return (l) this.f;
    }

    public final /* bridge */ /* synthetic */ Object remove(int i2) {
        e eVar = (e) this.f64a.get(i2);
        if (eVar != null && a((Object) eVar)) {
            a(true);
        }
        return eVar;
    }

    public final boolean remove(Object obj) {
        if (obj == null || !(obj instanceof e) || !a(obj)) {
            return false;
        }
        a(true);
        return true;
    }

    public final boolean removeAll(Collection collection) {
        throw new UnsupportedOperationException("Operation not supported.");
    }

    public final boolean retainAll(Collection collection) {
        throw new UnsupportedOperationException("Operation not supported.");
    }

    public final /* bridge */ /* synthetic */ Object set(int i2, Object obj) {
        throw new UnsupportedOperationException("Operation not supported.");
    }

    public final int size() {
        return this.f64a.size();
    }

    public final List subList(int i2, int i3) {
        return this.f64a.subList(i2, i3);
    }

    public final Object[] toArray() {
        return this.f64a.toArray();
    }

    public final Object[] toArray(Object[] objArr) {
        return this.f64a.toArray(objArr);
    }
}
