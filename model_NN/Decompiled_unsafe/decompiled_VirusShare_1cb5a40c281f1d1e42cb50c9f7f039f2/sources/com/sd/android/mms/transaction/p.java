package com.sd.android.mms.transaction;

import android.os.Bundle;

public final class p {

    /* renamed from: a  reason: collision with root package name */
    private final Bundle f122a;

    private p(int i) {
        this.f122a = new Bundle();
        this.f122a.putInt("type", i);
    }

    public p(int i, String str) {
        this(i);
        this.f122a.putString("uri", str);
    }

    public p(Bundle bundle) {
        this.f122a = bundle;
    }

    public final int a() {
        return this.f122a.getInt("type");
    }

    public final String b() {
        return this.f122a.getString("uri");
    }

    public final byte[] c() {
        return this.f122a.getByteArray("mms-push-data");
    }

    public final String d() {
        return this.f122a.getString("mmsc-url");
    }

    public final String e() {
        return this.f122a.getString("proxy-address");
    }

    public final int f() {
        return this.f122a.getInt("proxy-port");
    }
}
