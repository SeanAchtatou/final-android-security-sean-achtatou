package com.sd.android.mms.a;

import android.content.Context;
import android.util.Log;
import com.sd.android.mms.e.b;
import java.io.UnsupportedEncodingException;
import org.b.a.b.c;

public final class p extends b {
    private CharSequence h;
    private final int i;

    public p(Context context, String str, String str2, int i2, b bVar, j jVar) {
        super(context, "text", str, str2, bVar, jVar);
        this.i = i2 == 0 ? 4 : i2;
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public p(Context context, String str, String str2, int i2, byte[] bArr, j jVar) {
        super(context, "text", str, str2, bArr != null ? bArr : new byte[0], jVar);
        this.i = i2 == 0 ? 4 : i2;
        this.h = a(bArr);
    }

    public p(Context context, String str, String str2, j jVar) {
        this(context, str, str2, 106, new byte[0], jVar);
    }

    private CharSequence a(byte[] bArr) {
        if (bArr == null) {
            return "";
        }
        try {
            return this.i == 0 ? new String(bArr) : new String(bArr, com.sd.a.a.a.a.b.a(this.i));
        } catch (UnsupportedEncodingException e) {
            Log.e("TextModel", "Unsupported encoding: " + this.i, e);
            return new String(bArr);
        }
    }

    public final void a(CharSequence charSequence) {
        this.h = charSequence;
        a(true);
    }

    public final void a(c cVar) {
        if (cVar.a().equals("SmilMediaStart")) {
            this.f60a = true;
        } else if (this.f != 1) {
            this.f60a = false;
        }
        a(false);
    }

    public final String x() {
        if (this.h == null) {
            try {
                this.h = a(j());
            } catch (android.drm.mobile1.b e) {
                Log.e("TextModel", e.getMessage(), e);
                this.h = e.getMessage();
            }
        }
        if (!(this.h instanceof String)) {
            this.h = this.h.toString();
        }
        return this.h.toString();
    }

    public final void y() {
        this.h = new String(this.h.toString());
    }

    public final int z() {
        return this.i;
    }
}
