package com.sd.android.mms.b.a;

import org.b.a.a.b;

public final class j implements b {

    /* renamed from: a  reason: collision with root package name */
    private short f76a;
    private boolean b;
    private double c;

    j(String str) {
        int i;
        String str2;
        if (str.equals("indefinite")) {
            this.f76a = 0;
            return;
        }
        if (str.startsWith("+")) {
            str2 = str.substring(1);
            i = 1;
        } else if (str.startsWith("-")) {
            str2 = str.substring(1);
            i = -1;
        } else {
            i = 1;
            str2 = str;
        }
        this.c = ((double) (((float) i) * a(str2))) / 1000.0d;
        this.b = true;
        this.f76a = 1;
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static float a(java.lang.String r8) {
        /*
            r7 = 1198153728(0x476a6000, float:60000.0)
            r4 = 1148846080(0x447a0000, float:1000.0)
            r6 = 0
            r3 = 0
            r5 = 1
            java.lang.String r0 = r8.trim()     // Catch:{ NumberFormatException -> 0x00ab }
            java.lang.String r1 = "ms"
            boolean r1 = r0.endsWith(r1)     // Catch:{ NumberFormatException -> 0x00ab }
            if (r1 == 0) goto L_0x001b
            r1 = 2
            r2 = 1
            float r0 = a(r0, r1, r2)     // Catch:{ NumberFormatException -> 0x00ab }
        L_0x001a:
            return r0
        L_0x001b:
            java.lang.String r1 = "s"
            boolean r1 = r0.endsWith(r1)     // Catch:{ NumberFormatException -> 0x00ab }
            if (r1 == 0) goto L_0x002b
            r1 = 1
            r2 = 1
            float r0 = a(r0, r1, r2)     // Catch:{ NumberFormatException -> 0x00ab }
            float r0 = r0 * r4
            goto L_0x001a
        L_0x002b:
            java.lang.String r1 = "min"
            boolean r1 = r0.endsWith(r1)     // Catch:{ NumberFormatException -> 0x00ab }
            if (r1 == 0) goto L_0x003b
            r1 = 3
            r2 = 1
            float r0 = a(r0, r1, r2)     // Catch:{ NumberFormatException -> 0x00ab }
            float r0 = r0 * r7
            goto L_0x001a
        L_0x003b:
            java.lang.String r1 = "h"
            boolean r1 = r0.endsWith(r1)     // Catch:{ NumberFormatException -> 0x00ab }
            if (r1 == 0) goto L_0x004e
            r1 = 1247525376(0x4a5bba00, float:3600000.0)
            r2 = 1
            r3 = 1
            float r0 = a(r0, r2, r3)     // Catch:{ NumberFormatException -> 0x00ab }
            float r0 = r0 * r1
            goto L_0x001a
        L_0x004e:
            r1 = 0
            r2 = 1
            float r0 = a(r0, r1, r2)     // Catch:{ NumberFormatException -> 0x0056 }
            float r0 = r0 * r4
            goto L_0x001a
        L_0x0056:
            r1 = move-exception
            java.lang.String r1 = ":"
            java.lang.String[] r0 = r0.split(r1)     // Catch:{ NumberFormatException -> 0x00ab }
            int r1 = r0.length     // Catch:{ NumberFormatException -> 0x00ab }
            r2 = 2
            if (r1 != r2) goto L_0x008f
            r1 = r3
            r2 = r6
        L_0x0063:
            r3 = r0[r1]     // Catch:{ NumberFormatException -> 0x00ab }
            r4 = 0
            r5 = 0
            float r3 = a(r3, r4, r5)     // Catch:{ NumberFormatException -> 0x00ab }
            int r3 = (int) r3     // Catch:{ NumberFormatException -> 0x00ab }
            if (r3 < 0) goto L_0x00b2
            r4 = 59
            if (r3 > r4) goto L_0x00b2
            r4 = 60000(0xea60, float:8.4078E-41)
            int r3 = r3 * r4
            float r3 = (float) r3     // Catch:{ NumberFormatException -> 0x00ab }
            float r2 = r2 + r3
            int r1 = r1 + 1
            r0 = r0[r1]     // Catch:{ NumberFormatException -> 0x00ab }
            r1 = 0
            r3 = 1
            float r0 = a(r0, r1, r3)     // Catch:{ NumberFormatException -> 0x00ab }
            int r1 = (r0 > r6 ? 1 : (r0 == r6 ? 0 : -1))
            if (r1 < 0) goto L_0x00b8
            r1 = 1114636288(0x42700000, float:60.0)
            int r1 = (r0 > r1 ? 1 : (r0 == r1 ? 0 : -1))
            if (r1 >= 0) goto L_0x00b8
            float r0 = r0 * r7
            float r0 = r0 + r2
            goto L_0x001a
        L_0x008f:
            int r1 = r0.length     // Catch:{ NumberFormatException -> 0x00ab }
            r2 = 3
            if (r1 != r2) goto L_0x00a5
            r1 = 3600000(0x36ee80, float:5.044674E-39)
            r2 = 0
            r2 = r0[r2]     // Catch:{ NumberFormatException -> 0x00ab }
            r3 = 0
            r4 = 0
            float r2 = a(r2, r3, r4)     // Catch:{ NumberFormatException -> 0x00ab }
            int r2 = (int) r2     // Catch:{ NumberFormatException -> 0x00ab }
            int r1 = r1 * r2
            float r1 = (float) r1     // Catch:{ NumberFormatException -> 0x00ab }
            r2 = r1
            r1 = r5
            goto L_0x0063
        L_0x00a5:
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException     // Catch:{ NumberFormatException -> 0x00ab }
            r0.<init>()     // Catch:{ NumberFormatException -> 0x00ab }
            throw r0     // Catch:{ NumberFormatException -> 0x00ab }
        L_0x00ab:
            r0 = move-exception
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            r0.<init>()
            throw r0
        L_0x00b2:
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException     // Catch:{ NumberFormatException -> 0x00ab }
            r0.<init>()     // Catch:{ NumberFormatException -> 0x00ab }
            throw r0     // Catch:{ NumberFormatException -> 0x00ab }
        L_0x00b8:
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException     // Catch:{ NumberFormatException -> 0x00ab }
            r0.<init>()     // Catch:{ NumberFormatException -> 0x00ab }
            throw r0     // Catch:{ NumberFormatException -> 0x00ab }
        */
        throw new UnsupportedOperationException("Method not decompiled: com.sd.android.mms.b.a.j.a(java.lang.String):float");
    }

    private static float a(String str, int i, boolean z) {
        String substring = str.substring(0, str.length() - i);
        int indexOf = substring.indexOf(46);
        if (indexOf == -1) {
            return (float) Integer.parseInt(substring);
        }
        if (!z) {
            throw new IllegalArgumentException("int value contains decimal");
        }
        String str2 = substring + "000";
        return (Float.parseFloat(str2.substring(indexOf + 1, indexOf + 4)) / 1000.0f) + Float.parseFloat(str2.substring(0, indexOf));
    }

    public final boolean a() {
        return this.b;
    }

    public final double b() {
        return this.c;
    }

    public final short c() {
        return this.f76a;
    }
}
