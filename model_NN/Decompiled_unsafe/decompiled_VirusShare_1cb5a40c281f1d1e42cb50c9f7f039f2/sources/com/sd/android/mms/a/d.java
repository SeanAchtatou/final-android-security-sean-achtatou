package com.sd.android.mms.a;

import com.sd.a.a.a.c;
import com.sd.android.mms.a;
import com.sd.android.mms.b;
import com.sd.android.mms.e;
import java.util.ArrayList;

public final class d implements g {

    /* renamed from: a  reason: collision with root package name */
    private static final ArrayList f61a = c.a();
    private static final ArrayList b = c.b();
    private static final ArrayList c = c.c();

    public final void a(int i, int i2) {
        if (i < 0 || i2 < 0) {
            throw new e("Negative message size or increase size");
        }
        int i3 = i + i2;
        if (i3 < 0 || i3 > a.b()) {
            throw new com.sd.android.mms.d("Exceed message size limitation");
        }
    }

    public final void a(String str) {
        if (str == null) {
            throw new e("Null content type to be check");
        } else if (!f61a.contains(str)) {
            throw new b("Unsupported image content type : " + str);
        }
    }

    public final void b(int i, int i2) {
        if (i > a.e() || i2 > a.d()) {
            throw new com.sd.android.mms.c("content resolution exceeds restriction.");
        }
    }

    public final void b(String str) {
        if (str == null) {
            throw new e("Null content type to be check");
        } else if (!b.contains(str)) {
            throw new b("Unsupported audio content type : " + str);
        }
    }
}
