package com.sd.android.mms.transaction;

import android.app.Service;
import android.b.a;
import android.content.ContentUris;
import android.content.Intent;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.PowerManager;
import android.util.Log;
import com.sd.a.a.a.a.d;
import com.sd.android.mms.d.c;
import java.io.IOException;
import java.util.ArrayList;

public class MyTransactionService extends Service implements b {

    /* renamed from: a  reason: collision with root package name */
    private t f110a;
    private Looper b;
    /* access modifiers changed from: private */
    public final ArrayList c = new ArrayList();
    /* access modifiers changed from: private */
    public final ArrayList d = new ArrayList();
    private ConnectivityManager e;
    /* access modifiers changed from: private */
    public a f;
    private PowerManager.WakeLock g;
    private Handler h = new c(this);

    private void a(int i) {
        synchronized (this.c) {
            if (this.c.isEmpty() && this.d.isEmpty()) {
                Log.v("JB-MyTransactionService", "stopSelfIfIdle: STOP!");
                stopSelf(i);
            }
        }
    }

    private void a(int i, int i2) {
        int i3 = 1;
        if (i2 == 1) {
            i3 = 2;
        } else if (i2 != 2) {
            i3 = -1;
        }
        if (i3 != -1) {
            this.h.sendEmptyMessage(i3);
        }
        stopSelf(i);
    }

    private void a(int i, p pVar, boolean z) {
        if (z) {
            Log.w("JB-MyTransactionService", "launchTransaction: no network error!");
            a(i, pVar.a());
            return;
        }
        Message obtainMessage = this.f110a.obtainMessage(1);
        obtainMessage.arg1 = i;
        obtainMessage.obj = pVar;
        Log.v("JB-MyTransactionService", "Sending: " + obtainMessage);
        if (this.f110a.sendMessage(obtainMessage)) {
            Log.i("launchTransaction", "result is ok");
        } else {
            Log.i("launchTransaction", "result is ok");
        }
    }

    private synchronized void c() {
        if (this.g == null) {
            this.g = ((PowerManager) getSystemService("power")).newWakeLock(1, "MMS Connectivity");
            this.g.setReferenceCounted(false);
        }
    }

    private void d() {
        if (this.g != null && this.g.isHeld()) {
            this.g.release();
        }
    }

    /* access modifiers changed from: protected */
    public final int a() {
        c();
        int startUsingNetworkFeature = this.e.startUsingNetworkFeature(0, "enableMMS");
        Log.i("JB-MyTransactionService", "beginMmsConnectivity " + startUsingNetworkFeature + "(0:Phone.APN_ALREADY_ACTIVE, 1:Phone:APN_REQUEST_STARTED)");
        switch (startUsingNetworkFeature) {
            case 0:
                Log.i("JB-MyTransactionService", "phone apn already active");
                this.g.acquire();
                break;
            case 1:
                Log.i("JB-MyTransactionService", "phone apn request started");
                this.g.acquire();
                break;
            default:
                Log.i("JB-MyTransactionService", "cannot establish MMS connectivity");
                throw new IOException("Cannot establish MMS connectivity");
        }
        return startUsingNetworkFeature;
    }

    public final void a(v vVar) {
        e eVar = (e) vVar;
        int c2 = eVar.c();
        try {
            synchronized (this.c) {
                this.c.remove(eVar);
                if (this.d.size() > 0) {
                    this.f110a.sendMessage(this.f110a.obtainMessage(4, eVar.d()));
                } else {
                    b();
                }
            }
            Intent intent = new Intent("android.intent.action.TRANSACTION_COMPLETED_ACTION");
            d a2 = eVar.a();
            int a3 = a2.a();
            intent.putExtra("state", a3);
            switch (a3) {
                case 1:
                    Log.v("JB-MyTransactionService", "Transaction complete: " + c2);
                    intent.putExtra("uri", a2.b());
                    switch (eVar.e()) {
                        case 0:
                        case 1:
                            q.a(this);
                            q.d(this);
                            break;
                        case 2:
                            c.a().b();
                            break;
                    }
                case 2:
                    Log.v("JB-MyTransactionService", "Transaction failed: " + c2);
                    break;
                default:
                    Log.v("JB-MyTransactionService", "Transaction state unknown: " + c2 + " " + a3);
                    break;
            }
            sendBroadcast(intent);
        } finally {
            eVar.b(this);
            stopSelf(c2);
        }
    }

    /* access modifiers changed from: protected */
    public final void b() {
        try {
            this.f110a.removeMessages(3);
            if (this.e != null) {
                this.e.stopUsingNetworkFeature(0, "enableMMS");
            }
        } finally {
            d();
        }
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onCreate() {
        Log.v("JB-MyTransactionService", "Creating TransactionService");
        HandlerThread handlerThread = new HandlerThread("MyTransactionService");
        handlerThread.start();
        this.b = handlerThread.getLooper();
        this.f110a = new t(this, this.b);
        this.f = new a();
        this.f.a(this.f110a);
        this.f.a(this);
    }

    public void onDestroy() {
        Log.v("JB-MyTransactionService", "Destroying TransactionService");
        if (!this.d.isEmpty()) {
            Log.w("JB-MyTransactionService", "TransactionService exiting with transaction still pending");
        }
        d();
        this.f.b(this.f110a);
        this.f.a();
        this.f = null;
        this.f110a.sendEmptyMessage(100);
    }

    public void onStart(Intent intent, int i) {
        int i2;
        if (intent != null) {
            Log.v("JB-MyTransactionService", "onStart: #" + i + ": " + intent.getExtras());
            this.e = (ConnectivityManager) getSystemService("connectivity");
            boolean z = !this.e.getNetworkInfo(0).isAvailable();
            if ("android.intent.action.ACTION_ONALARM".equals(intent.getAction()) || intent.getExtras() == null) {
                Cursor a2 = d.a(this).a(System.currentTimeMillis());
                if (a2 != null) {
                    try {
                        if (a2.getCount() == 0) {
                            Log.v("JB-MyTransactionService", "onStart: No pending messages. Stopping service.");
                            l.b(this);
                            a(i);
                            return;
                        }
                        int columnIndexOrThrow = a2.getColumnIndexOrThrow("msg_id");
                        int columnIndexOrThrow2 = a2.getColumnIndexOrThrow("msg_type");
                        while (a2.moveToNext()) {
                            int i3 = a2.getInt(columnIndexOrThrow2);
                            switch (i3) {
                                case 128:
                                    i2 = 2;
                                    break;
                                case 130:
                                    i2 = 1;
                                    break;
                                case 135:
                                    i2 = 3;
                                    break;
                                default:
                                    Log.w("JB-MyTransactionService", "Unrecognized MESSAGE_TYPE: " + i3);
                                    i2 = -1;
                                    break;
                            }
                            if (z) {
                                a(i, i2);
                                a2.close();
                                return;
                            }
                            switch (i2) {
                                case 1:
                                    int i4 = a2.getInt(a2.getColumnIndexOrThrow("err_type"));
                                    if (!(i4 < 10 && i4 > 0)) {
                                        continue;
                                    }
                                    break;
                            }
                            a(i, new p(i2, ContentUris.withAppendedId(android.a.d.f7a, a2.getLong(columnIndexOrThrow)).toString()), false);
                        }
                        a2.close();
                    } finally {
                        a2.close();
                    }
                } else {
                    Log.v("JB-MyTransactionService", "onStart: No pending messages. Stopping service.");
                    l.b(this);
                    a(i);
                }
            } else {
                Log.v("JB-MyTransactionService", "onStart: launch transaction...");
                a(i, new p(intent.getExtras()), z);
            }
        }
    }
}
