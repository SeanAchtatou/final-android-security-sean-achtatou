package com.sd.android.mms.d;

import android.a.q;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.util.Log;
import com.sd.a.a.a.b.b;

public final class c {

    /* renamed from: a  reason: collision with root package name */
    private static c f93a;
    private static boolean b;
    private final Context c;
    /* access modifiers changed from: private */
    public int d;
    private final BroadcastReceiver e = new h(this);

    private c(Context context) {
        this.c = context;
    }

    public static c a() {
        if (f93a != null) {
            return f93a;
        }
        throw new IllegalStateException("Uninitialized.");
    }

    public static void a(Context context) {
        if (f93a != null) {
            Log.w("RateController", "Already initialized.");
        }
        f93a = new c(context);
    }

    private synchronized int e() {
        int i = 0;
        while (this.d == 0 && i < 20000) {
            try {
                wait(1000);
            } catch (InterruptedException e2) {
            }
            i += 1000;
        }
        return this.d;
    }

    public final void b() {
        ContentValues contentValues = new ContentValues(1);
        contentValues.put("sent_time", Long.valueOf(System.currentTimeMillis()));
        b.a(this.c, this.c.getContentResolver(), q.f18a, contentValues);
    }

    /* JADX INFO: finally extract failed */
    public final boolean c() {
        Cursor a2 = b.a(this.c, this.c.getContentResolver(), q.f18a, new String[]{"COUNT(*) AS rate"}, "sent_time>" + (System.currentTimeMillis() - 3600000), null, null);
        if (a2 != null) {
            try {
                if (a2.moveToFirst()) {
                    boolean z = a2.getInt(0) >= 100;
                    a2.close();
                    return z;
                }
                a2.close();
            } catch (Throwable th) {
                a2.close();
                throw th;
            }
        }
        return false;
    }

    public final synchronized boolean d() {
        while (b) {
            try {
                wait();
            } catch (InterruptedException e2) {
            }
        }
        b = true;
        this.c.registerReceiver(this.e, new IntentFilter("com.android.mms.RATE_LIMIT_CONFIRMED"));
        this.d = 0;
        try {
            Intent intent = new Intent("com.android.mms.RATE_LIMIT_SURPASSED");
            intent.addFlags(268435456);
            this.c.startActivity(intent);
        } finally {
            this.c.unregisterReceiver(this.e);
            b = false;
            notifyAll();
        }
        return e() == 1;
    }
}
