package com.sd.android.mms.transaction;

import android.content.Context;
import android.content.Intent;
import java.io.OutputStream;
import org.apache.http.entity.ByteArrayEntity;

public final class k extends ByteArrayEntity {

    /* renamed from: a  reason: collision with root package name */
    private final Context f119a;
    private final byte[] b;
    private final long c;

    public k(Context context, long j, byte[] bArr) {
        super(bArr);
        this.f119a = context;
        this.b = bArr;
        this.c = j;
    }

    private void a(int i) {
        if (this.c > 0) {
            Intent intent = new Intent("com.android.mms.PROGRESS_STATUS");
            intent.putExtra("progress", i);
            intent.putExtra("token", this.c);
            this.f119a.sendBroadcast(intent);
        }
    }

    public final void writeTo(OutputStream outputStream) {
        if (outputStream == null) {
            throw new IllegalArgumentException("Output stream may not be null");
        }
        try {
            a(-1);
            int i = 0;
            int length = this.b.length;
            while (i < length) {
                int i2 = length - i;
                if (i2 > 4096) {
                    i2 = 4096;
                }
                outputStream.write(this.b, i, i2);
                outputStream.flush();
                i += i2;
                a((i * 100) / length);
            }
            a(100);
        } catch (Throwable th) {
            a(-2);
            throw th;
        }
    }
}
