package com.sd.android.mms.a;

import android.content.ContentResolver;
import android.content.Context;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;
import com.sd.a.a.a.a.d;
import com.sd.a.a.a.a.e;
import com.sd.a.a.a.a.i;
import com.sd.a.a.a.a.l;
import com.sd.a.a.a.a.r;
import com.sd.android.mms.e.b;
import com.snda.youni.C0000R;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import org.b.a.a.h;
import org.b.a.a.j;
import org.b.a.a.k;
import org.b.a.a.m;
import org.b.a.a.n;
import org.b.a.a.o;
import org.w3c.dom.NodeList;

public final class a extends c implements o, List {

    /* renamed from: a  reason: collision with root package name */
    private final f f59a;
    private final ArrayList c;
    private o d;
    private e e;
    private int f;
    private ContentResolver g;

    private a(ContentResolver contentResolver) {
        this.f59a = new f();
        this.c = new ArrayList();
        this.g = contentResolver;
    }

    private a(f fVar, ArrayList arrayList, o oVar, e eVar, ContentResolver contentResolver) {
        this.f59a = fVar;
        this.c = arrayList;
        this.g = contentResolver;
        this.d = oVar;
        this.e = eVar;
        Iterator it = this.c.iterator();
        while (it.hasNext()) {
            h hVar = (h) it.next();
            d(hVar.b());
            hVar.a(this);
        }
    }

    private static int a(o oVar) {
        NodeList b = oVar.k().b();
        int length = b.getLength();
        int i = 0;
        for (int i2 = 0; i2 < length; i2++) {
            m mVar = (m) b.item(i2);
            if (mVar.e() + mVar.c() > i) {
                i = mVar.e() + mVar.c();
            }
        }
        return i;
    }

    private e a(Context context, o oVar, boolean z) {
        o oVar2;
        e eVar = new e();
        Iterator it = this.c.iterator();
        boolean z2 = false;
        while (it.hasNext()) {
            Iterator it2 = ((h) it.next()).iterator();
            boolean z3 = z2;
            while (it2.hasNext()) {
                e eVar2 = (e) it2.next();
                if (!z || !eVar2.q() || eVar2.r()) {
                    i iVar = new i();
                    if (eVar2.m()) {
                        p pVar = (p) eVar2;
                        if (!TextUtils.isEmpty(pVar.x())) {
                            iVar.a(pVar.z());
                        }
                    }
                    iVar.e(eVar2.f().getBytes());
                    String k = eVar2.k();
                    boolean startsWith = k.startsWith("cid:");
                    if (startsWith) {
                        k = k.substring("cid:".length());
                    }
                    iVar.c(k.getBytes());
                    if (startsWith) {
                        iVar.b(k.getBytes());
                    } else {
                        int lastIndexOf = k.lastIndexOf(".");
                        if (lastIndexOf != -1) {
                            k = k.substring(0, lastIndexOf);
                        }
                        iVar.b(k.getBytes());
                    }
                    if (eVar2.q()) {
                        b w = eVar2.w();
                        iVar.a(w.e());
                        iVar.a(w.f());
                    } else if (eVar2.m()) {
                        iVar.a(((p) eVar2).x().getBytes());
                    } else if (eVar2.n() || eVar2.o() || eVar2.p()) {
                        iVar.a(eVar2.h());
                    } else {
                        Log.w("JB-SlideshowModel", "Unsupport media: " + eVar2);
                    }
                    eVar.a(iVar);
                } else {
                    z3 = true;
                }
            }
            z2 = z3;
        }
        if (!z2 || !z || context == null) {
            oVar2 = oVar;
        } else {
            Toast.makeText(context, context.getString(C0000R.string.cannot_forward_drm_obj), 1).show();
            oVar2 = m.a(eVar);
        }
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        com.sd.android.mms.b.a.a.a.a(oVar2, byteArrayOutputStream);
        i iVar2 = new i();
        iVar2.b("smil".getBytes());
        iVar2.c("smil.xml".getBytes());
        iVar2.e("application/smil".getBytes());
        iVar2.a(byteArrayOutputStream.toByteArray());
        eVar.b(iVar2);
        return eVar;
    }

    public static a a(Context context) {
        return new a(context.getContentResolver());
    }

    public static a a(Context context, Uri uri) {
        r a2 = d.a(context).a(uri);
        int m = a2.m();
        if (m == 128 || m == 132) {
            return a(context, ((l) a2).i());
        }
        throw new com.sd.a.a.a.b();
    }

    public static a a(Context context, e eVar) {
        o a2 = m.a(eVar);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        com.sd.android.mms.b.a.a.a.a(a2, byteArrayOutputStream);
        Log.v("JB-SlideshowModel", byteArrayOutputStream.toString());
        h k = a2.k();
        k a3 = k.a();
        a3.c();
        a3.b();
        int a4 = a(a2);
        int b = b(a2);
        if (a4 == 0 || b == 0) {
            a4 = com.sd.android.mms.c.a.a().b().a();
            b = com.sd.android.mms.c.a.a().b().b();
            a3.b(a4);
            a3.a(b);
        }
        j jVar = new j(null, 0, a4, b);
        ArrayList arrayList = new ArrayList();
        NodeList b2 = k.b();
        int length = b2.getLength();
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= length) {
                break;
            }
            m mVar = (m) b2.item(i2);
            arrayList.add(new j(mVar.j(), mVar.d(), mVar.e(), mVar.f(), mVar.c(), mVar.b(), mVar.a()));
            i = i2 + 1;
        }
        f fVar = new f(jVar, arrayList);
        NodeList childNodes = a2.j().getChildNodes();
        int length2 = childNodes.getLength();
        ArrayList arrayList2 = new ArrayList(length2);
        for (int i3 = 0; i3 < length2; i3++) {
            n nVar = (n) childNodes.item(i3);
            NodeList childNodes2 = nVar.getChildNodes();
            int length3 = childNodes2.getLength();
            ArrayList arrayList3 = new ArrayList(length3);
            for (int i4 = 0; i4 < length3; i4++) {
                j jVar2 = (j) childNodes2.item(i4);
                try {
                    e a5 = i.a(context, jVar2, fVar, eVar);
                    m.a((org.b.a.b.e) jVar2, a5);
                    arrayList3.add(a5);
                } catch (android.drm.mobile1.b e2) {
                    Log.e("JB-SlideshowModel", e2.getMessage(), e2);
                } catch (IOException e3) {
                    Log.e("JB-SlideshowModel", e3.getMessage(), e3);
                } catch (IllegalArgumentException e4) {
                    Log.e("JB-SlideshowModel", e4.getMessage(), e4);
                }
            }
            h hVar = new h((int) (nVar.a() * 1000.0f), arrayList3);
            hVar.a(nVar.i());
            m.a((org.b.a.b.e) nVar, hVar);
            arrayList2.add(hVar);
        }
        a aVar = new a(fVar, arrayList2, a2, eVar, context.getContentResolver());
        aVar.c(aVar);
        return aVar;
    }

    private static int b(o oVar) {
        NodeList b = oVar.k().b();
        int length = b.getLength();
        int i = 0;
        for (int i2 = 0; i2 < length; i2++) {
            m mVar = (m) b.item(i2);
            if (mVar.f() + mVar.b() > i) {
                i = mVar.f() + mVar.b();
            }
        }
        return i;
    }

    private void d(int i) {
        if (i > 0) {
            this.f += i;
        }
    }

    private void e(int i) {
        if (i > 0) {
            this.f -= i;
        }
    }

    public final e a() {
        if (this.e == null) {
            this.d = m.a(this);
            this.e = a(null, this.d, false);
        }
        return this.e;
    }

    public final void a(int i) {
        this.f = i;
    }

    public final void a(e eVar) {
        Iterator it = this.c.iterator();
        while (it.hasNext()) {
            Iterator it2 = ((h) it.next()).iterator();
            while (it2.hasNext()) {
                e eVar2 = (e) it2.next();
                i b = eVar.b(eVar2.k());
                if (b != null) {
                    eVar2.a(b.b());
                }
            }
        }
    }

    public final void a(c cVar, boolean z) {
        if (z) {
            this.d = null;
            this.e = null;
        }
    }

    /* access modifiers changed from: protected */
    public final void a(o oVar) {
        this.f59a.c(oVar);
        Iterator it = this.c.iterator();
        while (it.hasNext()) {
            ((h) it.next()).c(oVar);
        }
    }

    /* renamed from: a */
    public final boolean add(h hVar) {
        int b = hVar.b();
        c(b);
        if (hVar == null || !this.c.add(hVar)) {
            return false;
        }
        d(b);
        hVar.c(this);
        Iterator it = this.b.iterator();
        while (it.hasNext()) {
            hVar.c((o) it.next());
        }
        a(true);
        return true;
    }

    public final /* bridge */ /* synthetic */ void add(int i, Object obj) {
        h hVar = (h) obj;
        if (hVar != null) {
            int b = hVar.b();
            c(b);
            this.c.add(i, hVar);
            d(b);
            hVar.c(this);
            Iterator it = this.b.iterator();
            while (it.hasNext()) {
                hVar.c((o) it.next());
            }
            a(true);
        }
    }

    public final boolean addAll(int i, Collection collection) {
        throw new UnsupportedOperationException("Operation not supported.");
    }

    public final boolean addAll(Collection collection) {
        throw new UnsupportedOperationException("Operation not supported.");
    }

    public final int b() {
        return this.f;
    }

    public final e b(Context context) {
        return a(context, m.a(this), true);
    }

    /* renamed from: b */
    public final h get(int i) {
        return (h) this.c.get(i);
    }

    /* access modifiers changed from: protected */
    public final void b(o oVar) {
        this.f59a.d(oVar);
        Iterator it = this.c.iterator();
        while (it.hasNext()) {
            ((h) it.next()).d(oVar);
        }
    }

    public final f c() {
        return this.f59a;
    }

    public final void c(int i) {
        n.a().a(this.f, i);
    }

    public final void clear() {
        if (this.c.size() > 0) {
            Iterator it = this.c.iterator();
            while (it.hasNext()) {
                h hVar = (h) it.next();
                hVar.d(this);
                Iterator it2 = this.b.iterator();
                while (it2.hasNext()) {
                    hVar.d((o) it2.next());
                }
            }
            this.f = 0;
            this.c.clear();
            a(true);
        }
    }

    public final boolean contains(Object obj) {
        return this.c.contains(obj);
    }

    public final boolean containsAll(Collection collection) {
        return this.c.containsAll(collection);
    }

    /* access modifiers changed from: protected */
    public final void d() {
        this.f59a.g();
        Iterator it = this.c.iterator();
        while (it.hasNext()) {
            ((h) it.next()).g();
        }
    }

    public final boolean e() {
        if (size() != 1) {
            return false;
        }
        h b = get(0);
        int i = b.h() ? 0 + 1 : 0;
        if (b.f()) {
            i++;
        }
        return (b.i() ? i + 1 : i) == 1;
    }

    public final void f() {
        p m;
        if (size() == 1 && (m = get(0).m()) != null) {
            m.y();
        }
    }

    public final int indexOf(Object obj) {
        return this.c.indexOf(obj);
    }

    public final boolean isEmpty() {
        return this.c.isEmpty();
    }

    public final Iterator iterator() {
        return this.c.iterator();
    }

    public final int lastIndexOf(Object obj) {
        return this.c.lastIndexOf(obj);
    }

    public final ListIterator listIterator() {
        return this.c.listIterator();
    }

    public final ListIterator listIterator(int i) {
        return this.c.listIterator(i);
    }

    public final /* bridge */ /* synthetic */ Object remove(int i) {
        h hVar = (h) this.c.remove(i);
        if (hVar != null) {
            e(hVar.b());
            hVar.g();
            a(true);
        }
        return hVar;
    }

    public final boolean remove(Object obj) {
        if (obj == null || !this.c.remove(obj)) {
            return false;
        }
        h hVar = (h) obj;
        e(hVar.b());
        hVar.g();
        a(true);
        return true;
    }

    public final boolean removeAll(Collection collection) {
        throw new UnsupportedOperationException("Operation not supported.");
    }

    public final boolean retainAll(Collection collection) {
        throw new UnsupportedOperationException("Operation not supported.");
    }

    public final /* bridge */ /* synthetic */ Object set(int i, Object obj) {
        h hVar = (h) obj;
        h hVar2 = (h) this.c.get(i);
        if (hVar != null) {
            int b = hVar.b();
            int b2 = hVar2 != null ? hVar2.b() : 0;
            if (b > b2) {
                c(b - b2);
                d(b - b2);
            } else {
                e(b2 - b);
            }
        }
        h hVar3 = (h) this.c.set(i, hVar);
        if (hVar3 != null) {
            hVar3.g();
        }
        if (hVar != null) {
            hVar.c(this);
            Iterator it = this.b.iterator();
            while (it.hasNext()) {
                hVar.c((o) it.next());
            }
        }
        a(true);
        return hVar3;
    }

    public final int size() {
        return this.c.size();
    }

    public final List subList(int i, int i2) {
        return this.c.subList(i, i2);
    }

    public final Object[] toArray() {
        return this.c.toArray();
    }

    public final Object[] toArray(Object[] objArr) {
        return this.c.toArray(objArr);
    }
}
