package com.sd.android.mms.c;

public final class b implements c {

    /* renamed from: a  reason: collision with root package name */
    private int f91a = -1;

    public b(int i) {
        if (i == 10 || i == 11) {
            this.f91a = i;
            return;
        }
        throw new IllegalArgumentException("Bad layout type detected: " + i);
    }

    public final int a() {
        return this.f91a == 10 ? 480 : 320;
    }

    public final int b() {
        return this.f91a == 10 ? 320 : 480;
    }

    public final int c() {
        return this.f91a == 10 ? 240 : 320;
    }

    public final int d() {
        return this.f91a == 10 ? 80 : 160;
    }
}
