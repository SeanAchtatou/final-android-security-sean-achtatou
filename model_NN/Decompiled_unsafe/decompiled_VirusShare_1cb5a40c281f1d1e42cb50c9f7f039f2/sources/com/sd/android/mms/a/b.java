package com.sd.android.mms.a;

import android.content.Context;
import android.net.Uri;

public abstract class b extends e {

    /* renamed from: a  reason: collision with root package name */
    protected boolean f60a;
    private j h;

    public b(Context context, String str, Uri uri, j jVar) {
        this(context, str, (String) null, (String) null, uri, jVar);
    }

    public b(Context context, String str, String str2, String str3, Uri uri, j jVar) {
        super(context, str, str2, str3, uri);
        this.f60a = true;
        this.h = jVar;
    }

    public b(Context context, String str, String str2, String str3, com.sd.android.mms.e.b bVar, j jVar) {
        super(context, str, str2, str3, bVar);
        this.f60a = true;
        this.h = jVar;
    }

    public b(Context context, String str, String str2, String str3, byte[] bArr, j jVar) {
        super(context, str, str2, str3, bArr);
        this.f60a = true;
        this.h = jVar;
    }

    public final j a() {
        return this.h;
    }

    public final boolean b() {
        return this.f60a;
    }
}
