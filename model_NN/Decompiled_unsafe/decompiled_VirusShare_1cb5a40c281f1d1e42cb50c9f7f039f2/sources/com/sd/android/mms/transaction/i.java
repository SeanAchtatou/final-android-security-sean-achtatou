package com.sd.android.mms.transaction;

import android.a.g;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;
import com.sd.a.a.a.a.d;
import com.sd.a.a.a.a.n;
import com.sd.a.a.a.a.p;
import com.sd.a.a.a.a.r;
import com.sd.a.a.a.b.b;

final class i extends AsyncTask {

    /* renamed from: a  reason: collision with root package name */
    private Context f117a;
    private /* synthetic */ PushReceiver b;

    public i(PushReceiver pushReceiver, Context context) {
        this.b = pushReceiver;
        this.f117a = context;
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public Void doInBackground(Intent... intentArr) {
        r a2 = new n(intentArr[0].getByteArrayExtra("data")).a();
        if (a2 == null) {
            Log.e("PushReceiver", "Invalid PUSH data");
            return null;
        }
        d a3 = d.a(this.f117a);
        ContentResolver contentResolver = this.f117a.getContentResolver();
        int m = a2.m();
        switch (m) {
            case 130:
                p pVar = (p) a2;
                if (PushReceiver.b(this.f117a, pVar)) {
                    Log.v("PushReceiver", "Skip downloading duplicate message: " + new String(pVar.b()));
                    break;
                } else {
                    Uri a4 = a3.a(a2, g.f10a);
                    Intent intent = new Intent(this.f117a, MyTransactionService.class);
                    intent.putExtra("uri", a4.toString());
                    intent.putExtra("type", 0);
                    this.f117a.startService(intent);
                    break;
                }
            case 134:
            case 136:
                long a5 = PushReceiver.b(this.f117a, a2, m);
                if (a5 != -1) {
                    Uri a6 = a3.a(a2, g.f10a);
                    ContentValues contentValues = new ContentValues(1);
                    contentValues.put("thread_id", Long.valueOf(a5));
                    b.a(this.f117a, contentResolver, a6, contentValues, null);
                    break;
                }
                break;
            default:
                try {
                    Log.e("PushReceiver", "Received unrecognized PDU.");
                    break;
                } catch (com.sd.a.a.a.b e) {
                    Log.e("PushReceiver", "Failed to save the data from PUSH: type=" + m, e);
                    break;
                } catch (RuntimeException e2) {
                    Log.e("PushReceiver", "Unexpected RuntimeException.", e2);
                    break;
                }
        }
        Log.v("PushReceiver", "PUSH Intent processed.");
        return null;
    }
}
