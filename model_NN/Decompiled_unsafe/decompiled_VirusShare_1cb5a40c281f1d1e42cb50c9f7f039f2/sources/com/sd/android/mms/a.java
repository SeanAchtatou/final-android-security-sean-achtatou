package com.sd.android.mms;

import android.content.Context;
import android.content.res.XmlResourceParser;
import android.util.Log;
import com.snda.youni.C0000R;
import java.io.IOException;
import org.xmlpull.v1.XmlPullParserException;

public final class a {

    /* renamed from: a  reason: collision with root package name */
    private static int f58a = -1;
    private static int b = 0;
    private static String c = null;
    private static int d = 0;
    private static int e = 0;

    public static void a(Context context) {
        int next;
        XmlResourceParser xml = context.getResources().getXml(C0000R.xml.mms_config);
        do {
            try {
                next = xml.next();
                if (next == 2) {
                    break;
                }
            } catch (XmlPullParserException e2) {
            } catch (NumberFormatException e3) {
            } catch (IOException e4) {
            } finally {
                xml.close();
            }
        } while (next != 1);
        if (next != 2) {
            throw new XmlPullParserException("No start tag found");
        } else if (!xml.getName().equals("mms_config")) {
            throw new XmlPullParserException("Unexpected start tag: found " + xml.getName() + ", expected " + "mms_config");
        } else {
            while (true) {
                com.sd.android.a.a.a.a(xml);
                String name = xml.getName();
                if (name == null) {
                    break;
                }
                String attributeName = xml.getAttributeName(0);
                String attributeValue = xml.getAttributeValue(0);
                String text = xml.next() == 4 ? xml.getText() : null;
                if ("name".equalsIgnoreCase(attributeName)) {
                    if ("bool".equals(name)) {
                        if ("enabledMMS".equalsIgnoreCase(attributeValue)) {
                            f58a = "true".equalsIgnoreCase(text) ? 1 : 0;
                        }
                    } else if ("int".equals(name)) {
                        if ("maxMessageSize".equalsIgnoreCase(attributeValue)) {
                            b = Integer.parseInt(text);
                        } else if ("maxImageHeight".equalsIgnoreCase(attributeValue)) {
                            d = Integer.parseInt(text);
                        } else if ("maxImageWidth".equalsIgnoreCase(attributeValue)) {
                            e = Integer.parseInt(text);
                        }
                    } else if ("string".equals(name) && "uaProfUrl".equalsIgnoreCase(attributeValue)) {
                        c = text;
                    }
                }
            }
            String str = f58a == -1 ? "enableMMS" : null;
            if (b == 0) {
                str = "maxMessageSize";
            }
            if (d == 0) {
                str = "maxImageHeight";
            }
            if (e == 0) {
                str = "maxImageWidth";
            }
            if (a() && c == null) {
                str = "uaProfUrl";
            }
            if (str != null) {
                String format = String.format("MmsConfig.loadMmsSettings mms_config.xml missing %s setting", str);
                Log.e("MmsConfig", format);
                throw new e(format);
            }
        }
    }

    public static boolean a() {
        return f58a == 1;
    }

    public static int b() {
        return b;
    }

    public static String c() {
        return c;
    }

    public static int d() {
        return d;
    }

    public static int e() {
        return e;
    }
}
