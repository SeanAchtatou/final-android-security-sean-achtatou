package com.sd.android.mms.transaction;

import android.b.d;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Build;
import android.util.Log;
import java.io.IOException;

public abstract class e extends v {

    /* renamed from: a  reason: collision with root package name */
    protected Context f113a;
    protected String b;
    protected d c = new d();
    private final int d;
    private j e;

    public e(Context context, int i, j jVar) {
        this.f113a = context;
        this.d = i;
        this.e = jVar;
    }

    private void a(String str, j jVar) {
        ConnectivityManager connectivityManager = (ConnectivityManager) this.f113a.getSystemService("connectivity");
        if (jVar.d()) {
            String b2 = jVar.b();
            Log.i("JB-Transaction", "lookuphost with proxyaddr=" + b2);
            int a2 = d.a(b2);
            Log.i("JB-Transaction", "return inetaddr=" + a2);
            if (a2 == -1) {
                Log.i("JB-Transaction", "Cannot establish route for " + str + ": Unknown host");
                throw new IOException("Cannot establish route for " + str + ": Unknown host");
            }
            int i = Build.VERSION.SDK_INT >= 8 ? 2 : 0;
            "requestRouteToHost with sdk_int = " + Build.VERSION.SDK_INT + " network_type=" + i + " inetAddr=" + a2;
            if (!connectivityManager.requestRouteToHost(i, a2)) {
                "Cannot establish route to proxy " + a2;
                int i2 = i == 2 ? 0 : 2;
                "requestRouteToHost with sdk_int = " + Build.VERSION.SDK_INT + " network_type=" + i2 + " inetAddr=" + a2;
                if (!connectivityManager.requestRouteToHost(i2, a2)) {
                    "Cannot establish route to proxy " + a2;
                }
                throw new IOException("Cannot establish route to proxy " + a2);
            }
            return;
        }
        Uri parse = Uri.parse(str);
        Log.i("JB-Transaction", "lookuphost with uri.gethost=" + parse.getHost());
        int a3 = d.a(parse.getHost());
        Log.i("JB-Transaction", "return inetaddr=" + a3);
        if (a3 == -1) {
            Log.i("JB-Transaction", "Cannot establish route for " + str + ": Unknown host");
            throw new IOException("Cannot establish route for " + str + ": Unknown host");
        }
        int i3 = Build.VERSION.SDK_INT >= 8 ? 2 : 0;
        "requestRouteToHost with sdk_int = " + Build.VERSION.SDK_INT + " network_type=" + i3 + " inetAddr=" + a3;
        if (!connectivityManager.requestRouteToHost(i3, a3)) {
            "Cannot establish route to proxy " + a3;
            throw new IOException("Cannot establish route to " + a3 + " for " + str);
        }
    }

    public final d a() {
        return this.c;
    }

    public final void a(j jVar) {
        this.e = jVar;
    }

    public final boolean a(e eVar) {
        return getClass().equals(eVar.getClass()) && this.b.equals(eVar.b);
    }

    /* access modifiers changed from: protected */
    public final byte[] a(long j, byte[] bArr) {
        String a2 = this.e.a();
        "transaction sendPdu with mmscuri = " + a2;
        a(a2, this.e);
        return h.a(this.f113a, j, a2, bArr, 1, this.e.d(), this.e.b(), this.e.c());
    }

    /* access modifiers changed from: protected */
    public final byte[] a(String str) {
        a(str, this.e);
        return h.a(this.f113a, -1, str, null, 2, this.e.d(), this.e.b(), this.e.c());
    }

    /* access modifiers changed from: protected */
    public final byte[] a(byte[] bArr) {
        String a2 = this.e.a();
        "transaction sendPdu with mmscuri = " + a2;
        a(a2, this.e);
        return h.a(this.f113a, -1, a2, bArr, 1, this.e.d(), this.e.b(), this.e.c());
    }

    public abstract void b();

    public final int c() {
        return this.d;
    }

    public final j d() {
        return this.e;
    }

    public abstract int e();

    public String toString() {
        return getClass().getName() + ": serviceId=" + this.d;
    }
}
