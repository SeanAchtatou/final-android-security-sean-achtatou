package com.sd.android.mms.transaction;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import java.io.IOException;
import java.util.Iterator;

final class t extends Handler {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ MyTransactionService f124a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public t(MyTransactionService myTransactionService, Looper looper) {
        super(looper);
        this.f124a = myTransactionService;
    }

    private void a(e eVar, j jVar) {
        e eVar2;
        int size;
        synchronized (this.f124a.c) {
            eVar2 = this.f124a.d.size() != 0 ? (e) this.f124a.d.remove(0) : eVar;
            size = this.f124a.c.size();
        }
        if (eVar2 != null) {
            if (jVar != null) {
                eVar2.a(jVar);
            }
            try {
                a(eVar2);
                Log.v("JB-MyTransactionService", "Started deferred processing of transaction: " + eVar2);
            } catch (IOException e) {
                Log.w("JB-MyTransactionService", e.getMessage(), e);
            }
        } else if (size == 0) {
            this.f124a.b();
        }
    }

    private boolean a(e eVar) {
        synchronized (this.f124a.c) {
            Iterator it = this.f124a.d.iterator();
            while (it.hasNext()) {
                if (((e) it.next()).a(eVar)) {
                    Log.v("JB-MyTransactionService", "Transaction already pending: " + eVar.c());
                    return true;
                }
            }
            Iterator it2 = this.f124a.c.iterator();
            while (it2.hasNext()) {
                if (((e) it2.next()).a(eVar)) {
                    Log.v("JB-MyTransactionService", "Duplicated transaction: " + eVar.c());
                    return true;
                }
            }
            if (this.f124a.a() == 1) {
                this.f124a.d.add(eVar);
                Log.v("JB-MyTransactionService", "Defer txn processing pending MMS connectivity");
                return true;
            }
            Log.v("JB-MyTransactionService", "Adding transaction to list: " + eVar);
            this.f124a.c.add(eVar);
            Log.v("JB-MyTransactionService", "Starting transaction: " + eVar);
            sendMessageDelayed(obtainMessage(3), 30000);
            eVar.a(this.f124a);
            eVar.b();
            return true;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:103:0x02d6, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:104:0x02d7, code lost:
        r2 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:118:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:119:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:130:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x005d, code lost:
        android.util.Log.v("JB-MyTransactionService", "Extending MMS connectivity - still processing txn");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:?, code lost:
        r0 = r10.f124a.a();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x006a, code lost:
        if (r0 == 0) goto L_0x0094;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x006c, code lost:
        android.util.Log.i("JB-MyTransactionService", "Extending MMS connectivity returned " + r0 + " instead of APN_ALREADY_ACTIVE");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x008c, code lost:
        android.util.Log.w("JB-MyTransactionService", "Attempt to extend use of MMS connectivity failed");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0094, code lost:
        sendMessageDelayed(obtainMessage(3), 30000);
     */
    /* JADX WARNING: Removed duplicated region for block: B:106:0x02da  */
    /* JADX WARNING: Removed duplicated region for block: B:126:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x01fc  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void handleMessage(android.os.Message r11) {
        /*
            r10 = this;
            r7 = 30000(0x7530, double:1.4822E-319)
            r4 = 3
            r6 = 0
            java.lang.String r0 = "JB-MyTransactionService"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Handling incoming message: "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r11)
            java.lang.String r1 = r1.toString()
            android.util.Log.v(r0, r1)
            int r0 = r11.what
            switch(r0) {
                case 1: goto L_0x0143;
                case 2: goto L_0x009c;
                case 3: goto L_0x0044;
                case 4: goto L_0x02fd;
                case 100: goto L_0x003c;
                default: goto L_0x0021;
            }
        L_0x0021:
            java.lang.String r0 = "JB-MyTransactionService"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "what="
            java.lang.StringBuilder r1 = r1.append(r2)
            int r2 = r11.what
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            android.util.Log.w(r0, r1)
        L_0x003b:
            return
        L_0x003c:
            android.os.Looper r0 = r10.getLooper()
            r0.quit()
            goto L_0x003b
        L_0x0044:
            com.sd.android.mms.transaction.MyTransactionService r0 = r10.f124a
            java.util.ArrayList r0 = r0.c
            monitor-enter(r0)
            com.sd.android.mms.transaction.MyTransactionService r1 = r10.f124a     // Catch:{ all -> 0x0059 }
            java.util.ArrayList r1 = r1.c     // Catch:{ all -> 0x0059 }
            boolean r1 = r1.isEmpty()     // Catch:{ all -> 0x0059 }
            if (r1 == 0) goto L_0x005c
            monitor-exit(r0)     // Catch:{ all -> 0x0059 }
            goto L_0x003b
        L_0x0059:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x0059 }
            throw r1
        L_0x005c:
            monitor-exit(r0)     // Catch:{ all -> 0x0059 }
            java.lang.String r0 = "JB-MyTransactionService"
            java.lang.String r1 = "Extending MMS connectivity - still processing txn"
            android.util.Log.v(r0, r1)
            com.sd.android.mms.transaction.MyTransactionService r0 = r10.f124a     // Catch:{ IOException -> 0x008b }
            int r0 = r0.a()     // Catch:{ IOException -> 0x008b }
            if (r0 == 0) goto L_0x0094
            java.lang.String r1 = "JB-MyTransactionService"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x008b }
            r2.<init>()     // Catch:{ IOException -> 0x008b }
            java.lang.String r3 = "Extending MMS connectivity returned "
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ IOException -> 0x008b }
            java.lang.StringBuilder r0 = r2.append(r0)     // Catch:{ IOException -> 0x008b }
            java.lang.String r2 = " instead of APN_ALREADY_ACTIVE"
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ IOException -> 0x008b }
            java.lang.String r0 = r0.toString()     // Catch:{ IOException -> 0x008b }
            android.util.Log.i(r1, r0)     // Catch:{ IOException -> 0x008b }
            goto L_0x003b
        L_0x008b:
            r0 = move-exception
            java.lang.String r0 = "JB-MyTransactionService"
            java.lang.String r1 = "Attempt to extend use of MMS connectivity failed"
            android.util.Log.w(r0, r1)
            goto L_0x003b
        L_0x0094:
            android.os.Message r0 = r10.obtainMessage(r4)
            r10.sendMessageDelayed(r0, r7)
            goto L_0x003b
        L_0x009c:
            com.sd.android.mms.transaction.MyTransactionService r0 = r10.f124a
            android.b.a r0 = r0.f
            if (r0 == 0) goto L_0x003b
            com.sd.android.mms.transaction.MyTransactionService r0 = r10.f124a
            android.b.a r0 = r0.f
            android.net.NetworkInfo r0 = r0.b()
            java.lang.String r1 = "JB-MyTransactionService"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Got DATA_STATE_CHANGED event: "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r2 = r2.append(r0)
            java.lang.String r2 = r2.toString()
            android.util.Log.v(r1, r2)
            int r1 = android.os.Build.VERSION.SDK_INT
            r2 = 7
            if (r1 < r2) goto L_0x0108
            r1 = 2
        L_0x00cc:
            if (r0 == 0) goto L_0x00d4
            int r2 = r0.getType()
            if (r2 == r1) goto L_0x010a
        L_0x00d4:
            java.lang.String r2 = "JB-MyTransactionService"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "   connection type is not correct! sdk version is ="
            java.lang.StringBuilder r3 = r3.append(r4)
            int r4 = android.os.Build.VERSION.SDK_INT
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r4 = " info.getType()="
            java.lang.StringBuilder r3 = r3.append(r4)
            int r0 = r0.getType()
            java.lang.StringBuilder r0 = r3.append(r0)
            java.lang.String r3 = " network_type="
            java.lang.StringBuilder r0 = r0.append(r3)
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            android.util.Log.v(r2, r0)
            goto L_0x003b
        L_0x0108:
            r1 = 0
            goto L_0x00cc
        L_0x010a:
            boolean r1 = r0.isConnected()
            if (r1 != 0) goto L_0x0119
            java.lang.String r0 = "JB-MyTransactionService"
            java.lang.String r1 = "   not connected"
            android.util.Log.v(r0, r1)
            goto L_0x003b
        L_0x0119:
            com.sd.android.mms.transaction.j r1 = new com.sd.android.mms.transaction.j
            com.sd.android.mms.transaction.MyTransactionService r2 = r10.f124a
            java.lang.String r0 = r0.getExtraInfo()
            r1.<init>(r2, r0)
            java.lang.String r0 = r1.a()
            boolean r0 = android.text.TextUtils.isEmpty(r0)
            if (r0 == 0) goto L_0x0137
            java.lang.String r0 = "JB-MyTransactionService"
            java.lang.String r1 = "   empty MMSC url, bail"
            android.util.Log.v(r0, r1)
            goto L_0x003b
        L_0x0137:
            android.os.Message r0 = r10.obtainMessage(r4)
            r10.sendMessageDelayed(r0, r7)
            r10.a(r6, r1)
            goto L_0x003b
        L_0x0143:
            int r1 = r11.arg1
            java.lang.Object r0 = r11.obj     // Catch:{ Exception -> 0x01bb }
            com.sd.android.mms.transaction.p r0 = (com.sd.android.mms.transaction.p) r0     // Catch:{ Exception -> 0x01bb }
            java.lang.String r2 = r0.d()     // Catch:{ Exception -> 0x01bb }
            if (r2 == 0) goto L_0x01b2
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01bb }
            r3.<init>()     // Catch:{ Exception -> 0x01bb }
            java.lang.String r4 = "mmsc is provided by transaction bundle as "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x01bb }
            java.lang.StringBuilder r3 = r3.append(r2)     // Catch:{ Exception -> 0x01bb }
            r3.toString()     // Catch:{ Exception -> 0x01bb }
            com.sd.android.mms.transaction.j r3 = new com.sd.android.mms.transaction.j     // Catch:{ Exception -> 0x01bb }
            java.lang.String r4 = r0.e()     // Catch:{ Exception -> 0x01bb }
            int r5 = r0.f()     // Catch:{ Exception -> 0x01bb }
            r3.<init>(r2, r4, r5)     // Catch:{ Exception -> 0x01bb }
            r2 = r3
        L_0x016f:
            int r3 = r0.a()     // Catch:{ Exception -> 0x01bb }
            switch(r3) {
                case 0: goto L_0x0220;
                case 1: goto L_0x029d;
                case 2: goto L_0x02aa;
                case 3: goto L_0x02b8;
                default: goto L_0x0176;
            }     // Catch:{ Exception -> 0x01bb }
        L_0x0176:
            java.lang.String r0 = "JB-MyTransactionService"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01bb }
            r2.<init>()     // Catch:{ Exception -> 0x01bb }
            java.lang.String r3 = "Invalid transaction type: "
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x01bb }
            java.lang.StringBuilder r2 = r2.append(r1)     // Catch:{ Exception -> 0x01bb }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x01bb }
            android.util.Log.w(r0, r2)     // Catch:{ Exception -> 0x01bb }
            java.lang.String r0 = "JB-MyTransactionService"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Transaction was null. Stopping self: "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r2 = r2.append(r1)
            java.lang.String r2 = r2.toString()
            android.util.Log.v(r0, r2)
            com.sd.android.mms.transaction.MyTransactionService r0 = r10.f124a
            r0.b()
            com.sd.android.mms.transaction.MyTransactionService r0 = r10.f124a
            r0.stopSelf(r1)
            goto L_0x003b
        L_0x01b2:
            com.sd.android.mms.transaction.j r2 = new com.sd.android.mms.transaction.j     // Catch:{ Exception -> 0x01bb }
            com.sd.android.mms.transaction.MyTransactionService r3 = r10.f124a     // Catch:{ Exception -> 0x01bb }
            r4 = 0
            r2.<init>(r3, r4)     // Catch:{ Exception -> 0x01bb }
            goto L_0x016f
        L_0x01bb:
            r0 = move-exception
            r2 = r6
        L_0x01bd:
            java.lang.String r3 = "JB-MyTransactionService"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x030b }
            r4.<init>()     // Catch:{ all -> 0x030b }
            java.lang.String r5 = "Exception occurred while handling message: "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x030b }
            java.lang.StringBuilder r4 = r4.append(r11)     // Catch:{ all -> 0x030b }
            java.lang.String r4 = r4.toString()     // Catch:{ all -> 0x030b }
            android.util.Log.w(r3, r4, r0)     // Catch:{ all -> 0x030b }
            if (r2 == 0) goto L_0x030d
            com.sd.android.mms.transaction.MyTransactionService r0 = r10.f124a     // Catch:{ Throwable -> 0x02c9 }
            r2.b(r0)     // Catch:{ Throwable -> 0x02c9 }
            com.sd.android.mms.transaction.MyTransactionService r0 = r10.f124a     // Catch:{ Throwable -> 0x02c9 }
            java.util.ArrayList r0 = r0.c     // Catch:{ Throwable -> 0x02c9 }
            boolean r0 = r0.contains(r2)     // Catch:{ Throwable -> 0x02c9 }
            if (r0 == 0) goto L_0x01f9
            com.sd.android.mms.transaction.MyTransactionService r0 = r10.f124a     // Catch:{ Throwable -> 0x02c9 }
            java.util.ArrayList r0 = r0.c     // Catch:{ Throwable -> 0x02c9 }
            monitor-enter(r0)     // Catch:{ Throwable -> 0x02c9 }
            com.sd.android.mms.transaction.MyTransactionService r3 = r10.f124a     // Catch:{ all -> 0x02c6 }
            java.util.ArrayList r3 = r3.c     // Catch:{ all -> 0x02c6 }
            r3.remove(r2)     // Catch:{ all -> 0x02c6 }
            monitor-exit(r0)     // Catch:{ all -> 0x02c6 }
        L_0x01f9:
            r0 = r6
        L_0x01fa:
            if (r0 != 0) goto L_0x003b
            java.lang.String r0 = "JB-MyTransactionService"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Transaction was null. Stopping self: "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r2 = r2.append(r1)
            java.lang.String r2 = r2.toString()
            android.util.Log.v(r0, r2)
            com.sd.android.mms.transaction.MyTransactionService r0 = r10.f124a
            r0.b()
            com.sd.android.mms.transaction.MyTransactionService r0 = r10.f124a
            r0.stopSelf(r1)
            goto L_0x003b
        L_0x0220:
            java.lang.String r3 = r0.b()     // Catch:{ Exception -> 0x01bb }
            if (r3 == 0) goto L_0x0250
            com.sd.android.mms.transaction.m r0 = new com.sd.android.mms.transaction.m     // Catch:{ Exception -> 0x01bb }
            com.sd.android.mms.transaction.MyTransactionService r4 = r10.f124a     // Catch:{ Exception -> 0x01bb }
            r0.<init>(r4, r1, r2, r3)     // Catch:{ Exception -> 0x01bb }
        L_0x022d:
            r10.a(r0)     // Catch:{ Exception -> 0x024a, all -> 0x0306 }
            java.lang.String r2 = "JB-MyTransactionService"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x024a, all -> 0x0306 }
            r3.<init>()     // Catch:{ Exception -> 0x024a, all -> 0x0306 }
            java.lang.String r4 = "Started processing of incoming message: "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x024a, all -> 0x0306 }
            java.lang.StringBuilder r3 = r3.append(r11)     // Catch:{ Exception -> 0x024a, all -> 0x0306 }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x024a, all -> 0x0306 }
            android.util.Log.v(r2, r3)     // Catch:{ Exception -> 0x024a, all -> 0x0306 }
            goto L_0x003b
        L_0x024a:
            r2 = move-exception
            r9 = r2
            r2 = r0
            r0 = r9
            goto L_0x01bd
        L_0x0250:
            byte[] r0 = r0.c()     // Catch:{ Exception -> 0x01bb }
            com.sd.a.a.a.a.n r3 = new com.sd.a.a.a.a.n     // Catch:{ Exception -> 0x01bb }
            r3.<init>(r0)     // Catch:{ Exception -> 0x01bb }
            com.sd.a.a.a.a.r r0 = r3.a()     // Catch:{ Exception -> 0x01bb }
            if (r0 == 0) goto L_0x0272
            int r3 = r0.m()     // Catch:{ Exception -> 0x01bb }
            r4 = 130(0x82, float:1.82E-43)
            if (r3 != r4) goto L_0x0272
            com.sd.android.mms.transaction.m r3 = new com.sd.android.mms.transaction.m     // Catch:{ Exception -> 0x01bb }
            com.sd.android.mms.transaction.MyTransactionService r4 = r10.f124a     // Catch:{ Exception -> 0x01bb }
            com.sd.a.a.a.a.p r0 = (com.sd.a.a.a.a.p) r0     // Catch:{ Exception -> 0x01bb }
            r3.<init>(r4, r1, r2, r0)     // Catch:{ Exception -> 0x01bb }
            r0 = r3
            goto L_0x022d
        L_0x0272:
            java.lang.String r0 = "JB-MyTransactionService"
            java.lang.String r2 = "Invalid PUSH data."
            android.util.Log.e(r0, r2)     // Catch:{ Exception -> 0x01bb }
            java.lang.String r0 = "JB-MyTransactionService"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Transaction was null. Stopping self: "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r2 = r2.append(r1)
            java.lang.String r2 = r2.toString()
            android.util.Log.v(r0, r2)
            com.sd.android.mms.transaction.MyTransactionService r0 = r10.f124a
            r0.b()
            com.sd.android.mms.transaction.MyTransactionService r0 = r10.f124a
            r0.stopSelf(r1)
            goto L_0x003b
        L_0x029d:
            com.sd.android.mms.transaction.r r3 = new com.sd.android.mms.transaction.r     // Catch:{ Exception -> 0x01bb }
            com.sd.android.mms.transaction.MyTransactionService r4 = r10.f124a     // Catch:{ Exception -> 0x01bb }
            java.lang.String r0 = r0.b()     // Catch:{ Exception -> 0x01bb }
            r3.<init>(r4, r1, r2, r0)     // Catch:{ Exception -> 0x01bb }
            r0 = r3
            goto L_0x022d
        L_0x02aa:
            com.sd.android.mms.transaction.s r3 = new com.sd.android.mms.transaction.s     // Catch:{ Exception -> 0x01bb }
            com.sd.android.mms.transaction.MyTransactionService r4 = r10.f124a     // Catch:{ Exception -> 0x01bb }
            java.lang.String r0 = r0.b()     // Catch:{ Exception -> 0x01bb }
            r3.<init>(r4, r1, r2, r0)     // Catch:{ Exception -> 0x01bb }
            r0 = r3
            goto L_0x022d
        L_0x02b8:
            com.sd.android.mms.transaction.u r3 = new com.sd.android.mms.transaction.u     // Catch:{ Exception -> 0x01bb }
            com.sd.android.mms.transaction.MyTransactionService r4 = r10.f124a     // Catch:{ Exception -> 0x01bb }
            java.lang.String r0 = r0.b()     // Catch:{ Exception -> 0x01bb }
            r3.<init>(r4, r1, r2, r0)     // Catch:{ Exception -> 0x01bb }
            r0 = r3
            goto L_0x022d
        L_0x02c6:
            r2 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x02c6 }
            throw r2     // Catch:{ Throwable -> 0x02c9 }
        L_0x02c9:
            r0 = move-exception
            java.lang.String r2 = "JB-MyTransactionService"
            java.lang.String r3 = "Unexpected Throwable."
            android.util.Log.e(r2, r3, r0)     // Catch:{ all -> 0x02d4 }
            r0 = r6
            goto L_0x01fa
        L_0x02d4:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x02d6 }
        L_0x02d6:
            r0 = move-exception
            r2 = r6
        L_0x02d8:
            if (r2 != 0) goto L_0x02fc
            java.lang.String r2 = "JB-MyTransactionService"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "Transaction was null. Stopping self: "
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.StringBuilder r3 = r3.append(r1)
            java.lang.String r3 = r3.toString()
            android.util.Log.v(r2, r3)
            com.sd.android.mms.transaction.MyTransactionService r2 = r10.f124a
            r2.b()
            com.sd.android.mms.transaction.MyTransactionService r2 = r10.f124a
            r2.stopSelf(r1)
        L_0x02fc:
            throw r0
        L_0x02fd:
            java.lang.Object r0 = r11.obj
            com.sd.android.mms.transaction.j r0 = (com.sd.android.mms.transaction.j) r0
            r10.a(r6, r0)
            goto L_0x003b
        L_0x0306:
            r2 = move-exception
            r9 = r2
            r2 = r0
            r0 = r9
            goto L_0x02d8
        L_0x030b:
            r0 = move-exception
            goto L_0x02d8
        L_0x030d:
            r0 = r2
            goto L_0x01fa
        */
        throw new UnsupportedOperationException("Method not decompiled: com.sd.android.mms.transaction.t.handleMessage(android.os.Message):void");
    }
}
