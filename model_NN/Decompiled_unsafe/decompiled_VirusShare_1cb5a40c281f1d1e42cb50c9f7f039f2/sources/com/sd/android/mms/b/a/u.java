package com.sd.android.mms.b.a;

import java.util.ArrayList;
import org.b.a.a.b;
import org.b.a.a.g;

public final class u implements g {

    /* renamed from: a  reason: collision with root package name */
    private final ArrayList f81a;

    u(ArrayList arrayList) {
        this.f81a = arrayList;
    }

    public final int a() {
        return this.f81a.size();
    }

    public final b a(int i) {
        try {
            return (b) this.f81a.get(i);
        } catch (IndexOutOfBoundsException e) {
            return null;
        }
    }
}
