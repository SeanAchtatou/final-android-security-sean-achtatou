package com.sd.android.mms.a;

public final class j extends c {

    /* renamed from: a  reason: collision with root package name */
    private final String f65a;
    private String c;
    private int d;
    private int e;
    private int f;
    private int g;
    private String h;

    public j(String str, int i, int i2, int i3) {
        this(str, "meet", i, i2, i3);
    }

    private j(String str, String str2, int i, int i2, int i3) {
        this(str, str2, 0, i, i2, i3, null);
    }

    public j(String str, String str2, int i, int i2, int i3, int i4, String str3) {
        this.f65a = str;
        this.c = str2;
        this.d = i;
        this.e = i2;
        this.f = i3;
        this.g = i4;
        this.h = str3;
    }

    public final String a() {
        return this.f65a;
    }

    public final String b() {
        return this.c;
    }

    public final int c() {
        return this.d;
    }

    public final int e() {
        return this.e;
    }

    public final int f() {
        return this.f;
    }

    public final int h() {
        return this.g;
    }

    public final String i() {
        return this.h;
    }
}
