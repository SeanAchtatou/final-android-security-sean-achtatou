package com.sd.android.mms.transaction;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.ConnectivityManager;
import com.sd.a.a.a.a.d;

public final class l implements b {
    private static l c;

    /* renamed from: a  reason: collision with root package name */
    private final Context f120a;
    private final ContentResolver b;

    private l(Context context) {
        this.f120a = context;
        this.b = context.getContentResolver();
    }

    public static l a(Context context) {
        if (c == null) {
            c = new l(context);
        }
        return c;
    }

    private boolean a() {
        return ((ConnectivityManager) this.f120a.getSystemService("connectivity")).getNetworkInfo(0).isConnected();
    }

    public static void b(Context context) {
        Cursor a2 = d.a(context).a(Long.MAX_VALUE);
        if (a2 != null) {
            try {
                if (a2.moveToFirst()) {
                    ((AlarmManager) context.getSystemService("alarm")).set(1, a2.getLong(a2.getColumnIndexOrThrow("due_time")), PendingIntent.getService(context, 0, new Intent("android.intent.action.ACTION_ONALARM", null, context, MyTransactionService.class), 1073741824));
                }
            } finally {
                a2.close();
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x00b4 A[Catch:{ all -> 0x0178, all -> 0x017d }] */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x00bb A[Catch:{ all -> 0x0178, all -> 0x017d }] */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x0130  */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x0132  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(com.sd.android.mms.transaction.v r15) {
        /*
            r14 = this;
            com.sd.android.mms.transaction.e r15 = (com.sd.android.mms.transaction.e) r15     // Catch:{ all -> 0x0120 }
            boolean r0 = r15 instanceof com.sd.android.mms.transaction.m     // Catch:{ all -> 0x0120 }
            if (r0 != 0) goto L_0x0012
            boolean r0 = r15 instanceof com.sd.android.mms.transaction.r     // Catch:{ all -> 0x0120 }
            if (r0 != 0) goto L_0x0012
            boolean r0 = r15 instanceof com.sd.android.mms.transaction.u     // Catch:{ all -> 0x0120 }
            if (r0 != 0) goto L_0x0012
            boolean r0 = r15 instanceof com.sd.android.mms.transaction.s     // Catch:{ all -> 0x0120 }
            if (r0 == 0) goto L_0x0087
        L_0x0012:
            com.sd.android.mms.transaction.d r0 = r15.a()     // Catch:{ all -> 0x011b }
            int r1 = r0.a()     // Catch:{ all -> 0x011b }
            r2 = 2
            if (r1 != r2) goto L_0x0084
            android.net.Uri r7 = r0.b()     // Catch:{ all -> 0x011b }
            if (r7 == 0) goto L_0x0084
            long r0 = android.content.ContentUris.parseId(r7)     // Catch:{ all -> 0x011b }
            android.net.Uri r2 = android.a.n.f15a     // Catch:{ all -> 0x011b }
            android.net.Uri$Builder r2 = r2.buildUpon()     // Catch:{ all -> 0x011b }
            java.lang.String r3 = "protocol"
            java.lang.String r4 = "mms"
            r2.appendQueryParameter(r3, r4)     // Catch:{ all -> 0x011b }
            java.lang.String r3 = "message"
            java.lang.String r0 = java.lang.String.valueOf(r0)     // Catch:{ all -> 0x011b }
            r2.appendQueryParameter(r3, r0)     // Catch:{ all -> 0x011b }
            android.content.Context r0 = r14.f120a     // Catch:{ all -> 0x011b }
            android.content.ContentResolver r1 = r14.b     // Catch:{ all -> 0x011b }
            android.net.Uri r2 = r2.build()     // Catch:{ all -> 0x011b }
            r3 = 0
            r4 = 0
            r5 = 0
            r6 = 0
            android.database.Cursor r8 = com.sd.a.a.a.b.b.a(r0, r1, r2, r3, r4, r5, r6)     // Catch:{ all -> 0x011b }
            if (r8 == 0) goto L_0x0084
            int r0 = r8.getCount()     // Catch:{ all -> 0x017d }
            r1 = 1
            if (r0 != r1) goto L_0x0116
            boolean r0 = r8.moveToFirst()     // Catch:{ all -> 0x017d }
            if (r0 == 0) goto L_0x0116
            java.lang.String r0 = "msg_type"
            int r0 = r8.getColumnIndexOrThrow(r0)     // Catch:{ all -> 0x017d }
            int r0 = r8.getInt(r0)     // Catch:{ all -> 0x017d }
            switch(r0) {
                case 128: goto L_0x012d;
                case 130: goto L_0x0093;
                case 135: goto L_0x012d;
                default: goto L_0x0069;
            }     // Catch:{ all -> 0x017d }
        L_0x0069:
            java.lang.String r1 = "RetryScheduler"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x017d }
            r2.<init>()     // Catch:{ all -> 0x017d }
            java.lang.String r3 = "Bad message type found: "
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x017d }
            java.lang.StringBuilder r0 = r2.append(r0)     // Catch:{ all -> 0x017d }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x017d }
            android.util.Log.w(r1, r0)     // Catch:{ all -> 0x017d }
            r8.close()     // Catch:{ all -> 0x011b }
        L_0x0084:
            r15.b(r14)     // Catch:{ all -> 0x0120 }
        L_0x0087:
            boolean r0 = r14.a()
            if (r0 == 0) goto L_0x0092
            android.content.Context r0 = r14.f120a
            b(r0)
        L_0x0092:
            return
        L_0x0093:
            r1 = 2
        L_0x0094:
            java.lang.String r2 = "retry_index"
            int r2 = r8.getColumnIndexOrThrow(r2)     // Catch:{ all -> 0x017d }
            int r2 = r8.getInt(r2)     // Catch:{ all -> 0x017d }
            int r9 = r2 + 1
            r2 = 1
            com.sd.android.mms.transaction.a r3 = new com.sd.android.mms.transaction.a     // Catch:{ all -> 0x017d }
            r3.<init>(r1, r9)     // Catch:{ all -> 0x017d }
            android.content.ContentValues r10 = new android.content.ContentValues     // Catch:{ all -> 0x017d }
            r1 = 4
            r10.<init>(r1)     // Catch:{ all -> 0x017d }
            long r11 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x017d }
            r1 = 130(0x82, float:1.82E-43)
            if (r0 != r1) goto L_0x0130
            r0 = 1
        L_0x00b5:
            int r1 = com.sd.android.mms.transaction.a.a()     // Catch:{ all -> 0x017d }
            if (r9 >= r1) goto L_0x0132
            long r3 = r3.b()     // Catch:{ all -> 0x017d }
            long r3 = r3 + r11
            java.lang.String r1 = "due_time"
            java.lang.Long r3 = java.lang.Long.valueOf(r3)     // Catch:{ all -> 0x017d }
            r10.put(r1, r3)     // Catch:{ all -> 0x017d }
            if (r0 == 0) goto L_0x00d4
            com.sd.android.mms.d.l r0 = com.sd.android.mms.d.l.b()     // Catch:{ all -> 0x017d }
            r1 = 130(0x82, float:1.82E-43)
            r0.a(r7, r1)     // Catch:{ all -> 0x017d }
        L_0x00d4:
            r0 = r2
        L_0x00d5:
            java.lang.String r1 = "err_type"
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)     // Catch:{ all -> 0x017d }
            r10.put(r1, r0)     // Catch:{ all -> 0x017d }
            java.lang.String r0 = "retry_index"
            java.lang.Integer r1 = java.lang.Integer.valueOf(r9)     // Catch:{ all -> 0x017d }
            r10.put(r0, r1)     // Catch:{ all -> 0x017d }
            java.lang.String r0 = "last_try"
            java.lang.Long r1 = java.lang.Long.valueOf(r11)     // Catch:{ all -> 0x017d }
            r10.put(r0, r1)     // Catch:{ all -> 0x017d }
            java.lang.String r0 = "_id"
            int r0 = r8.getColumnIndexOrThrow(r0)     // Catch:{ all -> 0x017d }
            long r0 = r8.getLong(r0)     // Catch:{ all -> 0x017d }
            android.content.Context r2 = r14.f120a     // Catch:{ all -> 0x017d }
            android.content.ContentResolver r3 = r14.b     // Catch:{ all -> 0x017d }
            android.net.Uri r4 = android.a.n.f15a     // Catch:{ all -> 0x017d }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x017d }
            r5.<init>()     // Catch:{ all -> 0x017d }
            java.lang.String r6 = "_id="
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ all -> 0x017d }
            java.lang.StringBuilder r0 = r5.append(r0)     // Catch:{ all -> 0x017d }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x017d }
            com.sd.a.a.a.b.b.a(r2, r3, r4, r10, r0)     // Catch:{ all -> 0x017d }
        L_0x0116:
            r8.close()     // Catch:{ all -> 0x011b }
            goto L_0x0084
        L_0x011b:
            r0 = move-exception
            r15.b(r14)     // Catch:{ all -> 0x0120 }
            throw r0     // Catch:{ all -> 0x0120 }
        L_0x0120:
            r0 = move-exception
            boolean r1 = r14.a()
            if (r1 == 0) goto L_0x012c
            android.content.Context r1 = r14.f120a
            b(r1)
        L_0x012c:
            throw r0
        L_0x012d:
            r1 = 1
            goto L_0x0094
        L_0x0130:
            r0 = 0
            goto L_0x00b5
        L_0x0132:
            r13 = 10
            if (r0 == 0) goto L_0x0182
            android.content.Context r0 = r14.f120a     // Catch:{ all -> 0x017d }
            android.content.Context r1 = r14.f120a     // Catch:{ all -> 0x017d }
            android.content.ContentResolver r1 = r1.getContentResolver()     // Catch:{ all -> 0x017d }
            r2 = 1
            java.lang.String[] r3 = new java.lang.String[r2]     // Catch:{ all -> 0x017d }
            r2 = 0
            java.lang.String r4 = "thread_id"
            r3[r2] = r4     // Catch:{ all -> 0x017d }
            r4 = 0
            r5 = 0
            r6 = 0
            r2 = r7
            android.database.Cursor r0 = com.sd.a.a.a.b.b.a(r0, r1, r2, r3, r4, r5, r6)     // Catch:{ all -> 0x017d }
            r1 = -1
            if (r0 == 0) goto L_0x01a6
            boolean r3 = r0.moveToFirst()     // Catch:{ all -> 0x0178 }
            if (r3 == 0) goto L_0x015d
            r1 = 0
            long r1 = r0.getLong(r1)     // Catch:{ all -> 0x0178 }
        L_0x015d:
            r0.close()     // Catch:{ all -> 0x017d }
            r0 = r1
        L_0x0161:
            r2 = -1
            int r0 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r0 == 0) goto L_0x016c
            android.content.Context r0 = r14.f120a     // Catch:{ all -> 0x017d }
            com.sd.android.mms.transaction.q.b(r0)     // Catch:{ all -> 0x017d }
        L_0x016c:
            com.sd.android.mms.d.l r0 = com.sd.android.mms.d.l.b()     // Catch:{ all -> 0x017d }
            r1 = 135(0x87, float:1.89E-43)
            r0.a(r7, r1)     // Catch:{ all -> 0x017d }
            r0 = r13
            goto L_0x00d5
        L_0x0178:
            r1 = move-exception
            r0.close()     // Catch:{ all -> 0x017d }
            throw r1     // Catch:{ all -> 0x017d }
        L_0x017d:
            r0 = move-exception
            r8.close()     // Catch:{ all -> 0x011b }
            throw r0     // Catch:{ all -> 0x011b }
        L_0x0182:
            android.content.ContentValues r0 = new android.content.ContentValues     // Catch:{ all -> 0x017d }
            r1 = 1
            r0.<init>(r1)     // Catch:{ all -> 0x017d }
            java.lang.String r1 = "read"
            r2 = 0
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)     // Catch:{ all -> 0x017d }
            r0.put(r1, r2)     // Catch:{ all -> 0x017d }
            android.content.Context r1 = r14.f120a     // Catch:{ all -> 0x017d }
            android.content.Context r2 = r14.f120a     // Catch:{ all -> 0x017d }
            android.content.ContentResolver r2 = r2.getContentResolver()     // Catch:{ all -> 0x017d }
            r3 = 0
            com.sd.a.a.a.b.b.a(r1, r2, r7, r0, r3)     // Catch:{ all -> 0x017d }
            android.content.Context r0 = r14.f120a     // Catch:{ all -> 0x017d }
            com.sd.android.mms.transaction.q.c(r0)     // Catch:{ all -> 0x017d }
            r0 = r13
            goto L_0x00d5
        L_0x01a6:
            r0 = r1
            goto L_0x0161
        */
        throw new UnsupportedOperationException("Method not decompiled: com.sd.android.mms.transaction.l.a(com.sd.android.mms.transaction.v):void");
    }
}
