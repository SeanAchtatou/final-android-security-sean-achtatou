package com.sd.android.mms.b.a;

import com.sd.android.mms.b.c;
import org.b.a.a.g;
import org.b.a.a.h;
import org.b.a.a.i;
import org.b.a.a.o;
import org.b.a.a.p;
import org.b.a.b.a;
import org.w3c.dom.DOMConfiguration;
import org.w3c.dom.DOMException;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.UserDataHandler;

public final class e extends c implements o, a {

    /* renamed from: a  reason: collision with root package name */
    p f73a;

    /* access modifiers changed from: private */
    /* renamed from: l */
    public i getDocumentElement() {
        Element element;
        Node firstChild = getFirstChild();
        if (firstChild == null || !(firstChild instanceof i)) {
            Element createElement = createElement("smil");
            appendChild(createElement);
            element = createElement;
        } else {
            element = firstChild;
        }
        return (i) element;
    }

    private i m() {
        Element element;
        i l = getDocumentElement();
        Node firstChild = l.getFirstChild();
        if (firstChild == null || !(firstChild instanceof i)) {
            Element createElement = createElement("head");
            l.appendChild(createElement);
            element = createElement;
        } else {
            element = firstChild;
        }
        return (i) element;
    }

    public final float a() {
        return this.f73a.a();
    }

    public final org.b.a.b.c a(String str) {
        if ("Event".equals(str)) {
            return new com.sd.android.mms.b.b.c();
        }
        throw new DOMException(9, "Not supported interface");
    }

    public final NodeList a(float f) {
        return this.f73a.a(f);
    }

    public final void a_(float f) {
        this.f73a.a_(f);
    }

    public final Node adoptNode(Node node) {
        return null;
    }

    public final void b(float f) {
        this.f73a.b(f);
    }

    public final boolean b() {
        return this.f73a.b();
    }

    public final boolean c() {
        return this.f73a.c();
    }

    public final short compareDocumentPosition(Node node) {
        return 0;
    }

    public final Element createElement(String str) {
        String lowerCase = str.toLowerCase();
        return (lowerCase.equals("text") || lowerCase.equals("img") || lowerCase.equals("video")) ? new f(this, lowerCase) : lowerCase.equals("audio") ? new l(this, lowerCase) : lowerCase.equals("layout") ? new s(this, lowerCase) : lowerCase.equals("root-layout") ? new p(this, lowerCase) : lowerCase.equals("region") ? new r(this, lowerCase) : lowerCase.equals("ref") ? new h(this, lowerCase) : lowerCase.equals("par") ? new v(this, lowerCase) : new o(this, lowerCase);
    }

    public final void d() {
        this.f73a.d();
    }

    public final void e() {
        this.f73a.e();
    }

    public final g f() {
        return this.f73a.f();
    }

    public final NodeList g() {
        return this.f73a.g();
    }

    public final String getBaseURI() {
        return null;
    }

    public final String getDocumentURI() {
        return null;
    }

    public final DOMConfiguration getDomConfig() {
        return null;
    }

    public final Object getFeature(String str, String str2) {
        return null;
    }

    public final String getInputEncoding() {
        return null;
    }

    public final boolean getStrictErrorChecking() {
        return false;
    }

    public final String getTextContent() {
        return null;
    }

    public final Object getUserData(String str) {
        return null;
    }

    public final String getXmlEncoding() {
        return null;
    }

    public final boolean getXmlStandalone() {
        return false;
    }

    public final String getXmlVersion() {
        return null;
    }

    public final g h() {
        return this.f73a.h();
    }

    public final short i() {
        return this.f73a.i();
    }

    public final boolean isDefaultNamespace(String str) {
        return false;
    }

    public final boolean isEqualNode(Node node) {
        return false;
    }

    public final boolean isSameNode(Node node) {
        return false;
    }

    public final i j() {
        i l = getDocumentElement();
        Node nextSibling = m().getNextSibling();
        if (nextSibling == null || !(nextSibling instanceof i)) {
            nextSibling = createElement("body");
            l.appendChild(nextSibling);
        }
        this.f73a = new i(this, (i) nextSibling);
        return (i) nextSibling;
    }

    public final h k() {
        s sVar;
        i m = m();
        Node firstChild = m.getFirstChild();
        while (firstChild != null && !(firstChild instanceof h)) {
            firstChild = firstChild.getNextSibling();
        }
        if (firstChild == null) {
            s sVar2 = new s(this, "layout");
            m.appendChild(sVar2);
            sVar = sVar2;
        } else {
            sVar = firstChild;
        }
        return (h) sVar;
    }

    public final String lookupNamespaceURI(String str) {
        return null;
    }

    public final String lookupPrefix(String str) {
        return null;
    }

    public final void normalizeDocument() {
    }

    public final Node renameNode(Node node, String str, String str2) {
        return null;
    }

    public final void setDocumentURI(String str) {
    }

    public final void setStrictErrorChecking(boolean z) {
    }

    public final void setTextContent(String str) {
    }

    public final Object setUserData(String str, Object obj, UserDataHandler userDataHandler) {
        return null;
    }

    public final void setXmlStandalone(boolean z) {
    }

    public final void setXmlVersion(String str) {
    }
}
