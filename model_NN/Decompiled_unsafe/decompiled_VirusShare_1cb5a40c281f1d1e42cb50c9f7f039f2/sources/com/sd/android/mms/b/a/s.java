package com.sd.android.mms.b.a;

import com.sd.android.mms.c.a;
import org.b.a.a.h;
import org.b.a.a.k;
import org.w3c.dom.NodeList;

public final class s extends o implements h {
    s(e eVar, String str) {
        super(eVar, str);
    }

    public final k a() {
        NodeList childNodes = getChildNodes();
        k kVar = null;
        int length = childNodes.getLength();
        for (int i = 0; i < length; i++) {
            if (childNodes.item(i).getNodeName().equals("root-layout")) {
                kVar = (k) childNodes.item(i);
            }
        }
        if (kVar != null) {
            return kVar;
        }
        k kVar2 = (k) getOwnerDocument().createElement("root-layout");
        kVar2.b(a.a().b().a());
        kVar2.a(a.a().b().b());
        appendChild(kVar2);
        return kVar2;
    }

    public final NodeList b() {
        return getElementsByTagName("region");
    }
}
