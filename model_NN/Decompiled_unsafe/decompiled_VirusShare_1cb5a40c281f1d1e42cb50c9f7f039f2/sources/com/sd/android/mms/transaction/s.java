package com.sd.android.mms.transaction;

import android.a.p;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.net.Uri;
import android.util.Log;
import com.sd.a.a.a.a.d;
import com.sd.a.a.a.a.j;
import com.sd.a.a.a.a.n;
import com.sd.a.a.a.b.b;
import com.sd.android.mms.d.c;
import com.sd.android.mms.d.k;
import java.util.Arrays;

public final class s extends e implements Runnable {
    private Thread d;
    private final Uri e;

    public s(Context context, int i, j jVar, String str) {
        super(context, i, jVar);
        this.e = Uri.parse(str);
        this.b = str;
        a(l.a(context));
    }

    public final void b() {
        this.d = new Thread(this);
        this.d.start();
    }

    public final int e() {
        return 2;
    }

    public final void run() {
        try {
            c a2 = c.a();
            if (!a2.c() || a2.d()) {
                d a3 = d.a(this.f113a);
                j jVar = (j) a3.a(this.e);
                long currentTimeMillis = System.currentTimeMillis() / 1000;
                jVar.b(currentTimeMillis);
                ContentValues contentValues = new ContentValues(1);
                contentValues.put("date", Long.valueOf(currentTimeMillis));
                b.a(this.f113a, this.f113a.getContentResolver(), this.e, contentValues, null);
                long parseId = ContentUris.parseId(this.e);
                byte[] a4 = a(k.a(Long.valueOf(parseId)), new com.sd.a.a.a.a.k(this.f113a, jVar).a());
                k.b(Long.valueOf(parseId));
                com.sd.a.a.a.a.s sVar = (com.sd.a.a.a.a.s) new n(a4).a();
                if (sVar == null) {
                    Log.e("JB-SendTransaction", "No M-Send.conf received.");
                }
                byte[] h = jVar.h();
                byte[] d2 = sVar.d();
                if (!Arrays.equals(h, d2)) {
                    Log.e("JB-SendTransaction", "Inconsistent Transaction-ID: req=" + new String(h) + ", conf=" + new String(d2));
                    if (this.c.a() != 1) {
                        this.c.a(2);
                        this.c.a(this.e);
                        Log.e("JB-SendTransaction", "Delivery failed.");
                    }
                    f();
                    return;
                }
                ContentValues contentValues2 = new ContentValues(2);
                int c = sVar.c();
                contentValues2.put("resp_st", Integer.valueOf(c));
                if (c != 128) {
                    b.a(this.f113a, this.f113a.getContentResolver(), this.e, contentValues2, null);
                    Log.e("JB-SendTransaction", "Server returned an error code: " + c);
                    if (this.c.a() != 1) {
                        this.c.a(2);
                        this.c.a(this.e);
                        Log.e("JB-SendTransaction", "Delivery failed.");
                    }
                    f();
                    return;
                }
                contentValues2.put("m_id", d.a(sVar.b()));
                b.a(this.f113a, this.f113a.getContentResolver(), this.e, contentValues2, null);
                Uri a5 = a3.a(this.e, p.f17a);
                this.c.a(1);
                this.c.a(a5);
                if (this.c.a() != 1) {
                    this.c.a(2);
                    this.c.a(this.e);
                    Log.e("JB-SendTransaction", "Delivery failed.");
                }
                f();
                return;
            }
            Log.e("JB-SendTransaction", "Sending rate limit surpassed.");
            if (this.c.a() != 1) {
                this.c.a(2);
                this.c.a(this.e);
                Log.e("JB-SendTransaction", "Delivery failed.");
            }
            f();
        } catch (Throwable th) {
            if (this.c.a() != 1) {
                this.c.a(2);
                this.c.a(this.e);
                Log.e("JB-SendTransaction", "Delivery failed.");
            }
            f();
            throw th;
        }
    }
}
