package com.sd.android.mms.transaction;

import android.content.BroadcastReceiver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.telephony.SmsMessage;
import android.util.Log;
import com.sd.a.a.a.b.b;

public class MessageStatusReceiver extends BroadcastReceiver {

    /* renamed from: a  reason: collision with root package name */
    private static final String[] f109a = {"_id"};
    private static final Uri b = Uri.parse("content://sms/status");
    private Context c;

    public void onReceive(Context context, Intent intent) {
        this.c = context;
        if ("com.sd.android.mms.transaction.MessageStatusReceiver.MESSAGE_STATUS_RECEIVED".equals(intent.getAction())) {
            Uri data = intent.getData();
            byte[] bArr = (byte[]) intent.getExtras().get("pdu");
            Cursor a2 = b.a(context, context.getContentResolver(), data, f109a, null, null, null);
            if (a2 == null || !a2.moveToFirst()) {
                Log.e("MessageStatusReceiver", "[MessageStatusReceiver] " + ("Can't find message for status update: " + data));
            } else {
                int i = a2.getInt(0);
                a2.close();
                Uri withAppendedId = ContentUris.withAppendedId(b, (long) i);
                int status = SmsMessage.createFromPdu(bArr).getStatus();
                ContentValues contentValues = new ContentValues(1);
                contentValues.put("status", Integer.valueOf(status));
                b.a(context, context.getContentResolver(), withAppendedId, contentValues, null);
            }
            q.a(context);
        }
    }
}
