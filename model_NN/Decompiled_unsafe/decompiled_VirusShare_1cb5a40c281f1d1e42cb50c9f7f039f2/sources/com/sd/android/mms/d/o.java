package com.sd.android.mms.d;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcel;
import android.telephony.ServiceState;

final class o extends BroadcastReceiver {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ l f105a;

    o(l lVar) {
        this.f105a = lVar;
    }

    public final void onReceive(Context context, Intent intent) {
        if ("android.intent.action.SERVICE_STATE".equals(intent.getAction())) {
            Bundle extras = intent.getExtras();
            Parcel obtain = Parcel.obtain();
            obtain.writeBundle(extras);
            ServiceState serviceState = new ServiceState(obtain);
            obtain.recycle();
            boolean roaming = serviceState.getRoaming();
            synchronized (l.g) {
                boolean unused = this.f105a.d = l.a(this.f105a.c, roaming);
            }
        }
    }
}
