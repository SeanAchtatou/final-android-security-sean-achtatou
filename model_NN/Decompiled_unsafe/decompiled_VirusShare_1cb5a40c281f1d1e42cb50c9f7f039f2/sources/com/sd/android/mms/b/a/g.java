package com.sd.android.mms.b.a;

import android.util.Log;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import org.b.a.a.b;
import org.b.a.a.f;
import org.b.a.a.l;
import org.b.a.a.p;
import org.b.a.b.a;
import org.b.a.b.c;
import org.b.a.b.e;
import org.w3c.dom.NodeList;

public final class g implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private static final Comparator f75a = new m();
    private static g b;
    private long c;
    private int d;
    private int e;
    private ArrayList f;
    private l g;
    private Thread h;
    private n i = n.INITIALIZED;
    private c j = c.NO_ACTIVE_ACTION;
    private ArrayList k;
    private c l;

    private g() {
    }

    private synchronized void A() {
        u();
        this.c = 0;
        this.d = 0;
        this.e = 0;
        this.i = n.STOPPED;
        this.j = c.NO_ACTIVE_ACTION;
    }

    private synchronized void B() {
        r();
        this.j = c.NO_ACTIVE_ACTION;
    }

    public static g a() {
        if (b == null) {
            b = new g();
        }
        return b;
    }

    private static ArrayList a(l lVar, double d2, double d3) {
        if (lVar instanceof f) {
            f fVar = (f) lVar;
            ArrayList arrayList = new ArrayList();
            double b2 = fVar.f().a(0).b() + d2;
            if (b2 > d3) {
                return arrayList;
            }
            arrayList.add(new q(b2, fVar, 0));
            double b3 = fVar.h().a(0).b() + d2;
            if (b3 > d3) {
                b3 = d3;
            }
            q qVar = new q(b3, fVar, 1);
            NodeList g2 = fVar.g();
            for (int i2 = 0; i2 < g2.getLength(); i2++) {
                arrayList.addAll(a((l) g2.item(i2), d2, b3));
            }
            Collections.sort(arrayList, f75a);
            NodeList a2 = fVar.a(((float) (b3 - d2)) * 1000.0f);
            for (int i3 = 0; i3 < a2.getLength(); i3++) {
                arrayList.add(new q(b3, (l) a2.item(i3), 1));
            }
            arrayList.add(qVar);
            return arrayList;
        } else if (lVar instanceof p) {
            return a((p) lVar, d2, d3);
        } else {
            ArrayList arrayList2 = new ArrayList();
            org.b.a.a.g f2 = lVar.f();
            for (int i4 = 0; i4 < f2.a(); i4++) {
                b a3 = f2.a(i4);
                if (a3.a()) {
                    double b4 = a3.b() + d2;
                    if (b4 <= d3) {
                        arrayList2.add(new q(b4, lVar, 0));
                    }
                }
            }
            org.b.a.a.g h2 = lVar.h();
            for (int i5 = 0; i5 < h2.a(); i5++) {
                b a4 = h2.a(i5);
                if (a4.a()) {
                    double b5 = a4.b() + d2;
                    if (b5 <= d3) {
                        arrayList2.add(new q(b5, lVar, 1));
                    }
                }
            }
            Collections.sort(arrayList2, f75a);
            return arrayList2;
        }
    }

    private static ArrayList a(p pVar, double d2, double d3) {
        ArrayList arrayList = new ArrayList();
        double b2 = pVar.f().a(0).b() + d2;
        if (b2 > d3) {
            return arrayList;
        }
        arrayList.add(new q(b2, pVar, 0));
        double b3 = pVar.h().a(0).b() + d2;
        if (b3 > d3) {
            b3 = d3;
        }
        q qVar = new q(b3, pVar, 1);
        NodeList g2 = pVar.g();
        double d4 = d2;
        for (int i2 = 0; i2 < g2.getLength(); i2++) {
            ArrayList a2 = a((l) g2.item(i2), d4, b3);
            arrayList.addAll(a2);
            d4 = ((q) a2.get(a2.size() - 1)).a();
        }
        NodeList a3 = pVar.a((float) (b3 - d2));
        for (int i3 = 0; i3 < a3.getLength(); i3++) {
            arrayList.add(new q(b3, (l) a3.item(i3), 1));
        }
        arrayList.add(qVar);
        return arrayList;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(long, long):long}
     arg types: [long, int]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(float, float):float}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(long, long):long} */
    private synchronized void a(long j2) {
        long j3;
        long j4 = 0;
        long j5 = j2;
        while (j5 > 0) {
            long currentTimeMillis = System.currentTimeMillis();
            long min = Math.min(j5, 200L);
            if (j4 < min) {
                wait(min - j4);
                this.c += min;
                j3 = min;
            } else {
                this.c = j4 + this.c;
                j3 = 0;
            }
            if (p() || q() || n()) {
                break;
            }
            ((e) this.g).a(this.l);
            j5 -= 200;
            j4 = (System.currentTimeMillis() - currentTimeMillis) - j3;
        }
    }

    private synchronized boolean a(q qVar) {
        return qVar.c() == 0 && (qVar.b() instanceof v);
    }

    private synchronized double b(l lVar) {
        double d2;
        int i2 = this.e;
        while (true) {
            int i3 = i2;
            if (i3 >= this.d) {
                d2 = -1.0d;
                break;
            }
            q qVar = (q) this.f.get(i3);
            if (lVar.equals(qVar.b())) {
                d2 = qVar.a() * 1000.0d;
                break;
            }
            i2 = i3 + 1;
        }
        return d2;
    }

    private synchronized void b(q qVar) {
        switch (qVar.c()) {
            case 0:
                qVar.b().b();
                this.k.add(qVar.b());
                break;
            case 1:
                qVar.b().c();
                this.k.remove(qVar.b());
                break;
        }
    }

    private synchronized boolean m() {
        return this.i == n.STOPPED;
    }

    private synchronized boolean n() {
        return this.j == c.PAUSE;
    }

    private synchronized boolean o() {
        return this.j == c.START;
    }

    private synchronized boolean p() {
        return this.j == c.STOP;
    }

    private synchronized boolean q() {
        return this.j == c.RELOAD;
    }

    private synchronized void r() {
        this.k.clear();
        s();
        for (int i2 = this.e; i2 < this.d; i2++) {
            b((q) this.f.get(i2));
        }
        t();
    }

    private synchronized void s() {
        b((q) this.f.get(0));
    }

    private synchronized void t() {
        for (int size = this.k.size() - 1; size >= 0; size--) {
            l lVar = (l) this.k.get(size);
            if (lVar instanceof v) {
                break;
            }
            double b2 = b(lVar);
            if (b2 >= 0.0d && b2 <= ((double) this.c)) {
                lVar.a_((float) (((double) this.c) - b2));
            }
        }
    }

    private synchronized void u() {
        for (int size = this.k.size() - 1; size >= 0; size--) {
            ((l) this.k.get(size)).c();
        }
    }

    private synchronized void v() {
        for (int size = this.k.size() - 1; size >= 0; size--) {
            ((l) this.k.get(size)).e();
        }
    }

    private synchronized void w() {
        int size = this.k.size();
        for (int i2 = 0; i2 < size; i2++) {
            ((l) this.k.get(i2)).d();
        }
    }

    private synchronized void x() {
        while (!o() && !p() && !q()) {
            try {
                wait(200);
            } catch (InterruptedException e2) {
                Log.e("SmilPlayer", "Unexpected InterruptedException.", e2);
            }
        }
        if (o()) {
            this.j = c.NO_ACTIVE_ACTION;
            this.i = n.PLAYING;
        }
        return;
    }

    private synchronized q y() {
        return (q) this.f.get(this.d);
    }

    private synchronized void z() {
        v();
        this.i = n.PAUSED;
        this.j = c.NO_ACTIVE_ACTION;
    }

    public final synchronized void a(l lVar) {
        this.g = lVar;
        this.f = a(this.g, 0.0d, 9.223372036854776E18d);
        this.l = ((a) this.g).a("Event");
        this.l.a("mediaTimeUpdated");
        this.k = new ArrayList();
    }

    public final synchronized boolean b() {
        return this.i == n.PLAYING;
    }

    public final synchronized boolean c() {
        return this.i == n.PLAYED;
    }

    public final synchronized boolean d() {
        return this.i == n.PAUSED;
    }

    public final synchronized void e() {
        if (!b()) {
            this.c = 0;
            this.d = 0;
            this.e = 0;
            this.h = new Thread(this);
            this.i = n.PLAYING;
            this.h.start();
        } else {
            Log.w("SmilPlayer", "Error State: Playback is playing!");
        }
    }

    public final synchronized void f() {
        if (b()) {
            this.j = c.PAUSE;
            notifyAll();
        } else {
            Log.w("SmilPlayer", "Error State: Playback is not playing!");
        }
    }

    public final synchronized void g() {
        if (d()) {
            w();
            this.j = c.START;
            notifyAll();
        } else if (c()) {
            e();
        } else {
            Log.w("SmilPlayer", "Error State: Playback can not be started!");
        }
    }

    public final synchronized void h() {
        if (b() || d()) {
            this.j = c.STOP;
            notifyAll();
        } else if (c()) {
            A();
        }
    }

    public final synchronized void i() {
        u();
    }

    public final synchronized void j() {
        if (b() || d()) {
            this.j = c.RELOAD;
            notifyAll();
        } else if (c()) {
            B();
        }
    }

    public final synchronized int k() {
        return (this.f == null || this.f.isEmpty()) ? 0 : ((int) ((q) this.f.get(this.f.size() - 1)).f80a) * 1000;
    }

    public final synchronized int l() {
        return (int) this.c;
    }

    public final void run() {
        if (!m()) {
            int size = this.f.size();
            this.d = 0;
            while (this.d < size) {
                q qVar = (q) this.f.get(this.d);
                if (a(qVar)) {
                    this.e = this.d;
                }
                long a2 = (long) (qVar.a() * 1000.0d);
                while (a2 > this.c) {
                    try {
                        a(a2 - this.c);
                    } catch (InterruptedException e2) {
                        Log.e("SmilPlayer", "Unexpected InterruptedException.", e2);
                    }
                    while (true) {
                        if (n() || p() || q()) {
                            if (n()) {
                                z();
                                x();
                            }
                            if (p()) {
                                A();
                                return;
                            } else if (q()) {
                                B();
                                qVar = y();
                                if (d()) {
                                    this.j = c.PAUSE;
                                }
                            }
                        }
                    }
                }
                this.c = a2;
                b(qVar);
                this.d++;
            }
            this.i = n.PLAYED;
        }
    }
}
