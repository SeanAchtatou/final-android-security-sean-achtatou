package com.sd.android.mms.b.a;

import org.b.a.a.m;
import org.b.a.a.o;

public final class r extends o implements m {
    r(e eVar, String str) {
        super(eVar, str);
    }

    private int a(String str, boolean z) {
        if (str.endsWith("px")) {
            return Integer.parseInt(str.substring(0, str.indexOf("px")));
        }
        if (!str.endsWith("%")) {
            return Integer.parseInt(str);
        }
        double parseInt = 0.01d * ((double) Integer.parseInt(str.substring(0, str.length() - 1)));
        return (int) Math.round(z ? parseInt * ((double) ((o) getOwnerDocument()).k().a().c()) : parseInt * ((double) ((o) getOwnerDocument()).k().a().b()));
    }

    public final String a() {
        return getAttribute("backgroundColor");
    }

    public final void a(int i) {
        setAttribute("height", String.valueOf(i) + "px");
    }

    public final void a(String str) {
        setAttribute("backgroundColor", str);
    }

    public final int b() {
        try {
            return a(getAttribute("height"), false);
        } catch (NumberFormatException e) {
            int b = ((o) getOwnerDocument()).k().a().b();
            try {
                b -= a(getAttribute("top"), false);
            } catch (NumberFormatException e2) {
            }
            try {
                return b - a(getAttribute("bottom"), false);
            } catch (NumberFormatException e3) {
                return b;
            }
        }
    }

    public final void b(int i) {
        setAttribute("width", String.valueOf(i) + "px");
    }

    public final void b(String str) {
        setAttribute("id", str);
    }

    public final int c() {
        try {
            return a(getAttribute("width"), true);
        } catch (NumberFormatException e) {
            int c = ((o) getOwnerDocument()).k().a().c();
            try {
                c -= a(getAttribute("left"), true);
            } catch (NumberFormatException e2) {
            }
            try {
                return c - a(getAttribute("right"), true);
            } catch (NumberFormatException e3) {
                return c;
            }
        }
    }

    public final void c(int i) {
        setAttribute("left", String.valueOf(i));
    }

    public final void c(String str) {
        if (str.equalsIgnoreCase("fill") || str.equalsIgnoreCase("meet") || str.equalsIgnoreCase("scroll") || str.equalsIgnoreCase("slice")) {
            setAttribute("fit", str.toLowerCase());
        } else {
            setAttribute("fit", "hidden");
        }
    }

    public final String d() {
        String attribute = getAttribute("fit");
        return "fill".equalsIgnoreCase(attribute) ? "fill" : "meet".equalsIgnoreCase(attribute) ? "meet" : "scroll".equalsIgnoreCase(attribute) ? "scroll" : "slice".equalsIgnoreCase(attribute) ? "slice" : "hidden";
    }

    public final void d(int i) {
        setAttribute("top", String.valueOf(i));
    }

    public final int e() {
        try {
            return a(getAttribute("left"), true);
        } catch (NumberFormatException e) {
            try {
                return (((o) getOwnerDocument()).k().a().c() - a(getAttribute("right"), true)) - a(getAttribute("width"), true);
            } catch (NumberFormatException e2) {
                return 0;
            }
        }
    }

    public final int f() {
        try {
            return a(getAttribute("top"), false);
        } catch (NumberFormatException e) {
            try {
                return (((o) getOwnerDocument()).k().a().b() - a(getAttribute("bottom"), false)) - a(getAttribute("height"), false);
            } catch (NumberFormatException e2) {
                return 0;
            }
        }
    }

    public final String j() {
        return getAttribute("id");
    }

    public final String toString() {
        return super.toString() + ": id=" + getAttribute("id") + ", width=" + c() + ", height=" + b() + ", left=" + e() + ", top=" + f();
    }
}
