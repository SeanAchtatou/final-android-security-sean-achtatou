package com.sd.android.mms.transaction;

import android.content.Intent;

final class f {

    /* renamed from: a  reason: collision with root package name */
    public Intent f114a;
    public String b;
    public int c;
    public CharSequence d;
    public long e;
    public String f;
    public int g;

    public f(Intent intent, String str, int i, CharSequence charSequence, long j, String str2, int i2) {
        this.f114a = intent;
        this.b = str;
        this.c = i;
        this.d = charSequence;
        this.e = j;
        this.f = str2;
        this.g = i2;
    }
}
