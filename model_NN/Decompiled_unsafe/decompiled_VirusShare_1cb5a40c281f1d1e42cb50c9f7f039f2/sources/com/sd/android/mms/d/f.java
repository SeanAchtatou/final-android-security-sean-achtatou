package com.sd.android.mms.d;

import android.database.ContentObserver;
import android.os.Handler;

final class f extends ContentObserver {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ i f96a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    f(i iVar, Handler handler) {
        super(handler);
        this.f96a = iVar;
    }

    public final void onChange(boolean z) {
        synchronized (this.f96a.k) {
            boolean unused = this.f96a.m = true;
            i.c(this.f96a);
        }
    }
}
