package com.sd.android.mms.c;

import android.content.Context;
import android.util.Log;

public final class a {
    private static a c;

    /* renamed from: a  reason: collision with root package name */
    private final Context f90a;
    private c b;

    private a(Context context) {
        b bVar;
        this.f90a = context;
        switch (context.getResources().getConfiguration().orientation == 1 ? true : true) {
            case true:
                bVar = new b(10);
                break;
            default:
                bVar = new b(11);
                break;
        }
        this.b = bVar;
    }

    public static a a() {
        if (c != null) {
            return c;
        }
        throw new IllegalStateException("Uninitialized.");
    }

    public static void a(Context context) {
        if (c != null) {
            Log.w("LayoutManager", "Already initialized.");
        }
        c = new a(context);
    }

    public final c b() {
        return this.b;
    }
}
