package com.sd.android.mms.a;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.text.TextUtils;
import com.sd.a.a.a.b.b;
import java.util.HashMap;
import org.b.a.b.c;

public final class r extends e {

    /* renamed from: a  reason: collision with root package name */
    private final HashMap f68a;

    /* JADX INFO: finally extract failed */
    public r(Context context, Uri uri) {
        this(context, (String) null, (String) null, uri);
        String string;
        Cursor a2 = b.a(this.c, this.c.getContentResolver(), uri, null, null, null, null);
        if (a2 != null) {
            try {
                if (a2.moveToFirst()) {
                    if (b(uri)) {
                        string = a2.getString(a2.getColumnIndexOrThrow("_data"));
                        this.e = a2.getString(a2.getColumnIndexOrThrow("ct"));
                    } else {
                        string = a2.getString(a2.getColumnIndexOrThrow("_data"));
                        this.e = a2.getString(a2.getColumnIndexOrThrow("mime_type"));
                        String string2 = a2.getString(a2.getColumnIndexOrThrow("album"));
                        if (!TextUtils.isEmpty(string2)) {
                            this.f68a.put("album", string2);
                        }
                        String string3 = a2.getString(a2.getColumnIndexOrThrow("artist"));
                        if (!TextUtils.isEmpty(string3)) {
                            this.f68a.put("artist", string3);
                        }
                    }
                    this.d = string.substring(string.lastIndexOf(47) + 1);
                    if (TextUtils.isEmpty(this.e)) {
                        throw new com.sd.a.a.a.b("Type of media is unknown.");
                    }
                    a2.close();
                    s();
                    n.a().b(this.e);
                    return;
                }
                throw new com.sd.a.a.a.b("Nothing found: " + uri);
            } catch (Throwable th) {
                a2.close();
                throw th;
            }
        } else {
            throw new com.sd.a.a.a.b("Bad URI: " + uri);
        }
    }

    public r(Context context, String str, String str2, Uri uri) {
        super(context, "audio", str, str2, uri);
        this.f68a = new HashMap();
    }

    public r(Context context, String str, String str2, com.sd.android.mms.e.b bVar) {
        super(context, "audio", str, str2, bVar);
        this.f68a = new HashMap();
    }

    public final void a(c cVar) {
        k kVar;
        String a2 = cVar.a();
        k kVar2 = k.NO_ACTIVE_ACTION;
        if (a2.equals("SmilMediaStart")) {
            kVar = k.START;
        } else if (a2.equals("SmilMediaEnd")) {
            kVar = k.STOP;
        } else if (a2.equals("SmilMediaPause")) {
            kVar = k.PAUSE;
        } else if (a2.equals("SmilMediaSeek")) {
            kVar = k.SEEK;
            this.g = cVar.b();
        } else {
            kVar = kVar2;
        }
        a(kVar);
        a(false);
    }

    /* access modifiers changed from: protected */
    public final boolean v() {
        return true;
    }
}
