package com.sd.android.mms.a;

import android.content.Context;
import com.sd.a.a.a.a.e;
import com.sd.a.a.a.c;
import com.sd.android.mms.e.b;
import org.b.a.a.g;
import org.b.a.a.j;
import org.b.a.a.m;

public final class i {
    private static e a(Context context, String str, String str2, j jVar, com.sd.a.a.a.a.i iVar, j jVar2) {
        e rVar;
        int i;
        g h;
        byte[] g = iVar.g();
        if (g == null) {
            throw new IllegalArgumentException("Content-Type of the part may not be null.");
        }
        String str3 = new String(g);
        if (c.e(str3)) {
            b bVar = new b(str3, iVar.b(), iVar.a());
            if (str.equals("text")) {
                rVar = new p(context, str3, str2, iVar.d(), bVar, jVar2);
            } else if (str.equals("img")) {
                rVar = new q(context, str3, str2, bVar, jVar2);
            } else if (str.equals("video")) {
                rVar = new l(context, str3, str2, bVar, jVar2);
            } else if (str.equals("audio")) {
                rVar = new r(context, str3, str2, bVar);
            } else if (str.equals("ref")) {
                String d = bVar.d();
                if (c.a(d)) {
                    rVar = new p(context, str3, str2, iVar.d(), bVar, jVar2);
                } else if (c.b(d)) {
                    rVar = new q(context, str3, str2, bVar, jVar2);
                } else if (c.d(d)) {
                    rVar = new l(context, str3, str2, bVar, jVar2);
                } else if (c.c(d)) {
                    rVar = new r(context, str3, str2, bVar);
                } else {
                    throw new com.sd.android.mms.b("Unsupported Content-Type: " + d);
                }
            } else {
                throw new IllegalArgumentException("Unsupported TAG: " + str);
            }
        } else if (str.equals("text")) {
            rVar = new p(context, str3, str2, iVar.d(), iVar.a(), jVar2);
        } else if (str.equals("img")) {
            rVar = new q(context, str3, str2, iVar.b(), jVar2);
        } else if (str.equals("video")) {
            rVar = new l(context, str3, str2, iVar.b(), jVar2);
        } else if (str.equals("audio")) {
            rVar = new r(context, str3, str2, iVar.b());
        } else if (!str.equals("ref")) {
            throw new IllegalArgumentException("Unsupported TAG: " + str);
        } else if (c.a(str3)) {
            rVar = new p(context, str3, str2, iVar.d(), iVar.a(), jVar2);
        } else if (c.b(str3)) {
            rVar = new q(context, str3, str2, iVar.b(), jVar2);
        } else if (c.d(str3)) {
            rVar = new l(context, str3, str2, iVar.b(), jVar2);
        } else if (c.c(str3)) {
            rVar = new r(context, str3, str2, iVar.b());
        } else {
            throw new com.sd.android.mms.b("Unsupported Content-Type: " + str3);
        }
        int i2 = 0;
        g f = jVar.f();
        if (f != null && f.a() > 0) {
            i2 = (int) (f.a(0).b() * 1000.0d);
        }
        rVar.a(i2);
        int a2 = (int) (jVar.a() * 1000.0f);
        if (a2 <= 0 && (h = jVar.h()) != null && h.a() > 0) {
            org.b.a.a.b a3 = h.a(0);
            if (a3.c() != 0) {
                i = ((int) (a3.b() * 1000.0d)) - i2;
                rVar.b(i);
                rVar.a(jVar.i());
                return rVar;
            }
        }
        i = a2;
        rVar.b(i);
        rVar.a(jVar.i());
        return rVar;
    }

    public static e a(Context context, j jVar, f fVar, e eVar) {
        com.sd.a.a.a.a.i iVar;
        String tagName = jVar.getTagName();
        String g = jVar.g();
        if (g == null) {
            iVar = null;
        } else if (g.startsWith("cid:")) {
            iVar = eVar.a("<" + g.substring("cid:".length()) + ">");
        } else {
            com.sd.a.a.a.a.i c = eVar.c(g);
            iVar = (c == null && (c = eVar.d(g)) == null) ? eVar.b(g) : c;
        }
        if (iVar == null) {
            throw new IllegalArgumentException("No part found for the model.");
        } else if (!(jVar instanceof org.b.a.a.e)) {
            return a(context, tagName, g, jVar, iVar, null);
        } else {
            org.b.a.a.e eVar2 = (org.b.a.a.e) jVar;
            m d_ = eVar2.d_();
            if (d_ != null) {
                j a2 = fVar.a(d_.j());
                if (a2 != null) {
                    return a(context, tagName, g, eVar2, iVar, a2);
                }
            } else {
                j a3 = fVar.a(tagName.equals("text") ? "Text" : "Image");
                if (a3 != null) {
                    return a(context, tagName, g, eVar2, iVar, a3);
                }
            }
            throw new IllegalArgumentException("Region not found or bad region ID.");
        }
    }
}
