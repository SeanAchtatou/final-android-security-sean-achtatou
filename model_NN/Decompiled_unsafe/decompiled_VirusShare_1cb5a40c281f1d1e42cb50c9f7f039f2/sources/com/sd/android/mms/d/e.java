package com.sd.android.mms.d;

import android.database.ContentObserver;
import android.os.Handler;

final class e extends ContentObserver {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ i f95a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    e(i iVar, Handler handler) {
        super(handler);
        this.f95a = iVar;
    }

    public final void onChange(boolean z) {
        synchronized (this.f95a.k) {
            boolean unused = this.f95a.l = true;
            i.c(this.f95a);
        }
    }
}
