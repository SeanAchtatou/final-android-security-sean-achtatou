package com.sd.android.mms.transaction;

import android.a.d;
import android.a.g;
import android.a.m;
import android.a.o;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.StyleSpan;
import com.sd.a.a.a.a.w;
import com.sd.a.a.a.b.b;
import com.sd.android.mms.d.i;
import com.snda.youni.C0000R;
import com.snda.youni.activities.ChatActivity;
import java.util.HashSet;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

public final class q {

    /* renamed from: a  reason: collision with root package name */
    private static final String[] f123a = {"thread_id", "date", "_id", "sub", "sub_cs"};
    private static final String[] b = {"thread_id", "date", "address", "subject", "body"};
    private static final w c = new w();
    private static final Uri d = Uri.parse("content://mms-sms/undelivered");

    private q() {
    }

    private static int a(Context context, long[] jArr) {
        Cursor a2 = b.a(context, context.getContentResolver(), d, new String[]{"thread_id"}, "read=0", null, null);
        if (a2 == null) {
            return 0;
        }
        int count = a2.getCount();
        if (jArr != null) {
            try {
                if (a2.moveToFirst()) {
                    jArr[0] = a2.getLong(0);
                    if (jArr.length >= 2) {
                        long j = jArr[0];
                        while (true) {
                            if (a2.moveToNext()) {
                                if (a2.getLong(0) != j) {
                                    j = 0;
                                    break;
                                }
                            } else {
                                break;
                            }
                        }
                        jArr[1] = j;
                    }
                }
            } catch (Throwable th) {
                a2.close();
                throw th;
            }
        }
        a2.close();
        return count;
    }

    private static final int a(SortedSet sortedSet, f fVar) {
        if (fVar == null) {
            return 0;
        }
        sortedSet.add(fVar);
        return fVar.g;
    }

    private static Intent a() {
        Intent intent = new Intent("android.intent.action.MAIN", o.f16a);
        intent.setFlags(335544320);
        return intent;
    }

    /* JADX INFO: finally extract failed */
    private static f a(Context context, Set set) {
        Cursor a2 = b.a(context, context.getContentResolver(), d.f7a, f123a, "(msg_box=1 AND read=0 AND (m_type=130 OR m_type=132))", null, "date desc");
        if (a2 == null) {
            return null;
        }
        try {
            if (!a2.moveToFirst()) {
                a2.close();
                return null;
            }
            String a3 = com.sd.android.mms.d.b.a(context, d.f7a.buildUpon().appendPath(Long.toString(a2.getLong(2))).build());
            String string = a2.getString(3);
            String c2 = TextUtils.isEmpty(string) ? "" : new w(a2.getInt(4), com.sd.a.a.a.a.d.a(string)).c();
            long j = a2.getLong(0);
            f a4 = a(a3, c2, context, C0000R.drawable.stat_notify_mms, j, 1000 * a2.getLong(1), a2.getCount());
            set.add(Long.valueOf(j));
            while (a2.moveToNext()) {
                set.add(Long.valueOf(a2.getLong(0)));
            }
            a2.close();
            return a4;
        } catch (Throwable th) {
            a2.close();
            throw th;
        }
    }

    private static final f a(String str, String str2, Context context, int i, long j, long j2, int i2) {
        Intent a2 = a();
        a2.setData(Uri.withAppendedPath(a2.getData(), Long.toString(j)));
        a2.setAction("android.intent.action.VIEW");
        String obj = a(context, str, (String) null).toString();
        return new f(a2, str2, i, a(context, str, str2), j2, obj.substring(0, obj.length() - 2), i2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    private static CharSequence a(Context context, String str, String str2) {
        String str3 = null;
        String a2 = i.a().a(context, str);
        StringBuilder sb = new StringBuilder(a2 == null ? "" : a2.replace(10, ' ').replace(13, ' '));
        sb.append(':').append(' ');
        int length = sb.length();
        if (!TextUtils.isEmpty(str3)) {
            sb.append(str3.replace(10, ' ').replace(13, ' '));
            sb.append(' ');
        }
        if (!TextUtils.isEmpty(str2)) {
            sb.append(str2.replace(10, ' ').replace(13, ' '));
        }
        SpannableString spannableString = new SpannableString(sb.toString());
        spannableString.setSpan(new StyleSpan(1), 0, length, 33);
        return spannableString;
    }

    public static void a(Context context) {
        Intent intent;
        TreeSet treeSet = new TreeSet(c);
        HashSet hashSet = new HashSet(4);
        int a2 = a(treeSet, a(context, hashSet)) + 0 + a(treeSet, b(context, hashSet));
        a(context, 123);
        if (!treeSet.isEmpty()) {
            f fVar = (f) treeSet.first();
            int size = hashSet.size();
            Intent intent2 = fVar.f114a;
            String str = fVar.b;
            int i = fVar.c;
            CharSequence charSequence = fVar.d;
            long j = fVar.e;
            String str2 = fVar.f;
            SharedPreferences defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
            if (defaultSharedPreferences.getBoolean("pref_key_enable_notifications", true)) {
                Notification notification = new Notification(i, charSequence, j);
                if (size > 1) {
                    str2 = context.getString(C0000R.string.notification_multiple_title);
                    intent = a();
                    intent.setAction("android.intent.action.MAIN");
                    intent.setType("vnd.android-dir/mms-sms");
                } else {
                    intent = intent2;
                }
                notification.setLatestEventInfo(context, str2, a2 > 1 ? context.getString(C0000R.string.notification_multiple, Integer.toString(a2)) : str, PendingIntent.getActivity(context, 0, intent, 134217728));
                if (defaultSharedPreferences.getBoolean("pref_key_vibrate", true)) {
                    notification.defaults |= 2;
                }
                String string = defaultSharedPreferences.getString("pref_key_ringtone", null);
                notification.sound = TextUtils.isEmpty(string) ? null : Uri.parse(string);
                notification.flags |= 1;
                notification.ledARGB = -16711936;
                notification.ledOnMS = 500;
                notification.ledOffMS = 2000;
                ((NotificationManager) context.getSystemService("notification")).notify(123, notification);
            }
        }
    }

    public static void a(Context context, int i) {
        ((NotificationManager) context.getSystemService("notification")).cancel(i);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    private static void a(Context context, boolean z, boolean z2) {
        String string;
        Intent intent;
        String str;
        SharedPreferences defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        if (defaultSharedPreferences.getBoolean("pref_key_enable_notifications", true)) {
            NotificationManager notificationManager = (NotificationManager) context.getSystemService("notification");
            long[] jArr = {0};
            int a2 = a(context, jArr);
            Notification notification = new Notification();
            if (a2 > 1) {
                str = context.getString(C0000R.string.notification_failed_multiple, Integer.toString(a2));
                string = context.getString(C0000R.string.notification_failed_multiple_title);
                intent = new Intent(context, ChatActivity.class);
            } else {
                string = z ? context.getString(C0000R.string.message_download_failed_title) : context.getString(C0000R.string.message_send_failed_title);
                String string2 = context.getString(C0000R.string.message_failed_body);
                long j = jArr[0] != 0 ? jArr[0] : 0;
                Intent intent2 = new Intent(context, ChatActivity.class);
                intent2.putExtra("thread_id", j);
                intent2.putExtra("undelivered_flag", true);
                String str2 = string2;
                intent = intent2;
                str = str2;
            }
            intent.setFlags(335544320);
            PendingIntent activity = PendingIntent.getActivity(context, 0, intent, 134217728);
            notification.icon = C0000R.drawable.stat_notify_sms_failed;
            notification.tickerText = string;
            notification.setLatestEventInfo(context, string, str, activity);
            if (z2) {
                if (defaultSharedPreferences.getBoolean("pref_key_vibrate", true)) {
                    notification.defaults |= 2;
                }
                String string3 = defaultSharedPreferences.getString("pref_key_ringtone", null);
                notification.sound = TextUtils.isEmpty(string3) ? null : Uri.parse(string3);
            }
            if (z) {
                notificationManager.notify(531, notification);
            } else {
                notificationManager.notify(789, notification);
            }
        }
    }

    /* JADX INFO: finally extract failed */
    private static f b(Context context, Set set) {
        Cursor a2 = b.a(context, context.getContentResolver(), m.f14a, b, "(type = 1 AND read = 0)", null, "date desc");
        if (a2 == null) {
            return null;
        }
        try {
            if (!a2.moveToFirst()) {
                a2.close();
                return null;
            }
            String string = a2.getString(2);
            String string2 = a2.getString(4);
            long j = a2.getLong(0);
            f a3 = a(string, string2, context, C0000R.drawable.stat_notify_sms, j, a2.getLong(1), a2.getCount());
            set.add(Long.valueOf(j));
            while (a2.moveToNext()) {
                set.add(Long.valueOf(a2.getLong(0)));
            }
            a2.close();
            return a3;
        } catch (Throwable th) {
            a2.close();
            throw th;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.sd.android.mms.transaction.q.a(android.content.Context, boolean, boolean):void
     arg types: [android.content.Context, int, int]
     candidates:
      com.sd.android.mms.transaction.q.a(android.content.Context, java.lang.String, java.lang.String):java.lang.CharSequence
      com.sd.android.mms.transaction.q.a(android.content.Context, boolean, boolean):void */
    public static void b(Context context) {
        a(context, true, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.sd.android.mms.transaction.q.a(android.content.Context, boolean, boolean):void
     arg types: [android.content.Context, int, int]
     candidates:
      com.sd.android.mms.transaction.q.a(android.content.Context, java.lang.String, java.lang.String):java.lang.CharSequence
      com.sd.android.mms.transaction.q.a(android.content.Context, boolean, boolean):void */
    public static void c(Context context) {
        a(context, false, true);
    }

    public static void d(Context context) {
        int i;
        Cursor a2 = b.a(context, context.getContentResolver(), g.f10a, null, "m_type=" + String.valueOf(130) + " AND " + "st" + "=" + String.valueOf(135), null, null);
        if (a2 == null) {
            i = 0;
        } else {
            int count = a2.getCount();
            a2.close();
            i = count;
        }
        if (i <= 0) {
            a(context, 531);
        }
    }
}
