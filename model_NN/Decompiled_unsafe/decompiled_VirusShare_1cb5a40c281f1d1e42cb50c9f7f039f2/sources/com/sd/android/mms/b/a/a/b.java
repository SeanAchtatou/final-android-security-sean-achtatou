package com.sd.android.mms.b.a.a;

import com.sd.android.mms.b.a.e;
import org.b.a.a.o;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

public final class b extends DefaultHandler {

    /* renamed from: a  reason: collision with root package name */
    private o f70a;
    private Node b;

    public final void a() {
        this.f70a = new e();
        this.b = this.f70a;
    }

    public final o b() {
        return this.f70a;
    }

    public final void characters(char[] cArr, int i, int i2) {
    }

    public final void endElement(String str, String str2, String str3) {
        this.b = this.b.getParentNode();
    }

    public final void startElement(String str, String str2, String str3, Attributes attributes) {
        Element createElement = this.f70a.createElement(str2);
        if (attributes != null) {
            for (int i = 0; i < attributes.getLength(); i++) {
                createElement.setAttribute(attributes.getLocalName(i), attributes.getValue(i));
            }
        }
        this.b.appendChild(createElement);
        this.b = createElement;
    }
}
