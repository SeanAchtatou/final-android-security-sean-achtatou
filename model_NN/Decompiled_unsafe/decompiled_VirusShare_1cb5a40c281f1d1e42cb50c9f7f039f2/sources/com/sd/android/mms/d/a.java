package com.sd.android.mms.d;

public final class a {

    /* renamed from: a  reason: collision with root package name */
    public String f92a;
    public String b;
    public String c;
    public long d;
    public int e;
    private boolean f;
    private /* synthetic */ i g;

    public a(i iVar) {
        this.g = iVar;
    }

    public final boolean a() {
        return this.f;
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder("name=" + this.c);
        sb.append(", phone=" + this.f92a);
        sb.append(", pid=" + this.d);
        sb.append(", presence=" + this.e);
        sb.append(", stale=" + this.f);
        return sb.toString();
    }
}
