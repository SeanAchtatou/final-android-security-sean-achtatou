package com.sd.android.mms.b.a;

import org.b.a.a.k;

public final class p extends o implements k {
    p(e eVar, String str) {
        super(eVar, str);
    }

    private static int c(String str) {
        try {
            return Integer.parseInt(str.endsWith("px") ? str.substring(0, str.indexOf("px")) : str);
        } catch (NumberFormatException e) {
            return 0;
        }
    }

    public final String a() {
        return getAttribute("backgroundColor");
    }

    public final void a(int i) {
        setAttribute("height", String.valueOf(i) + "px");
    }

    public final void a(String str) {
        setAttribute("backgroundColor", str);
    }

    public final int b() {
        return c(getAttribute("height"));
    }

    public final void b(int i) {
        setAttribute("width", String.valueOf(i) + "px");
    }

    public final int c() {
        return c(getAttribute("width"));
    }
}
