package com.sd.android.mms.b;

import java.util.Vector;
import org.w3c.dom.DOMException;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

public final class b implements NamedNodeMap {

    /* renamed from: a  reason: collision with root package name */
    private Vector f83a = new Vector();

    public final int getLength() {
        return this.f83a.size();
    }

    public final Node getNamedItem(String str) {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= this.f83a.size()) {
                return null;
            }
            if (str.equals(((Node) this.f83a.elementAt(i2)).getNodeName())) {
                return (Node) this.f83a.elementAt(i2);
            }
            i = i2 + 1;
        }
    }

    public final Node getNamedItemNS(String str, String str2) {
        return null;
    }

    public final Node item(int i) {
        if (i < this.f83a.size()) {
            return (Node) this.f83a.elementAt(i);
        }
        return null;
    }

    public final Node removeNamedItem(String str) {
        Node namedItem = getNamedItem(str);
        if (namedItem == null) {
            throw new DOMException(8, "Not found");
        }
        this.f83a.remove(namedItem);
        return namedItem;
    }

    public final Node removeNamedItemNS(String str, String str2) {
        return null;
    }

    public final Node setNamedItem(Node node) {
        Node namedItem = getNamedItem(node.getNodeName());
        if (namedItem != null) {
            this.f83a.remove(namedItem);
        }
        this.f83a.add(node);
        return namedItem;
    }

    public final Node setNamedItemNS(Node node) {
        return null;
    }
}
