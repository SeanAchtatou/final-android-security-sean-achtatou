package com.sd.android.mms.transaction;

public final class a extends g {
    private static final int[] b = {0, 60000, 300000, 600000, 1800000};

    public a(int i, int i2) {
        super(i, i2);
        this.f115a = this.f115a < 0 ? 0 : this.f115a;
        this.f115a = this.f115a >= b.length ? b.length - 1 : this.f115a;
    }

    public static int a() {
        return b.length;
    }

    public final long b() {
        return (long) b[this.f115a];
    }
}
