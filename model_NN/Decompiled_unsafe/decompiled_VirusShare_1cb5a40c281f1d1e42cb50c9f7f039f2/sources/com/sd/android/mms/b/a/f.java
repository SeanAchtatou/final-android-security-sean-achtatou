package com.sd.android.mms.b.a;

import org.b.a.a.e;
import org.b.a.a.m;
import org.b.a.a.o;
import org.w3c.dom.NodeList;

public class f extends l implements e {

    /* renamed from: a  reason: collision with root package name */
    private m f74a;

    f(e eVar, String str) {
        super(eVar, str);
    }

    public final void a(m mVar) {
        setAttribute("region", mVar.j());
        this.f74a = mVar;
    }

    public final m d_() {
        if (this.f74a == null) {
            NodeList elementsByTagName = ((o) getOwnerDocument()).k().getElementsByTagName("region");
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 >= elementsByTagName.getLength()) {
                    break;
                }
                m mVar = (m) elementsByTagName.item(i2);
                if (mVar.j().equals(getAttribute("region"))) {
                    this.f74a = mVar;
                }
                i = i2 + 1;
            }
        }
        return this.f74a;
    }
}
