package com.sd.android.mms.transaction;

import android.a.d;
import android.a.g;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.util.Log;
import com.sd.a.a.a.a.a;
import com.sd.a.a.a.a.k;
import com.sd.a.a.a.a.n;
import com.sd.a.a.a.a.t;
import com.sd.a.a.a.b.b;
import com.sd.android.mms.d.l;

public final class r extends e implements Runnable {
    private final Uri d;
    private final String e;

    public r(Context context, int i, j jVar, String str) {
        super(context, i, jVar);
        if (str.startsWith("content://")) {
            this.d = Uri.parse(str);
            String a2 = a(context, this.d);
            this.e = a2;
            this.b = a2;
            a(l.a(context));
            return;
        }
        throw new IllegalArgumentException("Initializing from X-Mms-Content-Location is abandoned!");
    }

    private static String a(Context context, Uri uri) {
        Cursor a2 = b.a(context, context.getContentResolver(), uri, new String[]{"ct_l"}, null, null, null);
        if (a2 != null) {
            try {
                if (a2.getCount() == 1 && a2.moveToFirst()) {
                    return a2.getString(0);
                }
                a2.close();
            } finally {
                a2.close();
            }
        }
        throw new com.sd.a.a.a.b("Cannot get X-Mms-Content-Location from: " + uri);
    }

    /* JADX INFO: finally extract failed */
    private static boolean a(Context context, t tVar) {
        Cursor a2;
        byte[] c = tVar.c();
        if (!(c == null || (a2 = b.a(context, context.getContentResolver(), d.f7a, new String[]{"_id"}, "(m_id = ? AND m_type = ?)", new String[]{new String(c), String.valueOf(132)}, null)) == null)) {
            try {
                if (a2.getCount() > 0) {
                    a2.close();
                    return true;
                }
                a2.close();
            } catch (Throwable th) {
                a2.close();
                throw th;
            }
        }
        return false;
    }

    public final void b() {
        new Thread(this).start();
    }

    public final int e() {
        return 1;
    }

    public final void run() {
        try {
            l.b().a(this.d, 129);
            t tVar = (t) new n(a(this.e)).a();
            if (tVar == null) {
                throw new com.sd.a.a.a.b("Invalid M-Retrieve.conf PDU.");
            }
            if (a(this.f113a, tVar)) {
                this.c.a(2);
                this.c.a(this.d);
            } else {
                Uri a2 = com.sd.a.a.a.a.d.a(this.f113a).a(tVar, g.f10a);
                this.c.a(1);
                this.c.a(a2);
                Context context = this.f113a;
                String str = this.e;
                ContentValues contentValues = new ContentValues(1);
                contentValues.put("ct_l", str);
                b.a(context, context.getContentResolver(), a2, contentValues, null);
            }
            b.a(this.f113a, this.f113a.getContentResolver(), this.d, (String) null);
            byte[] d2 = tVar.d();
            if (d2 != null) {
                a(new k(this.f113a, new a(d2)).a());
            }
            if (this.c.a() != 1) {
                this.c.a(2);
                this.c.a(this.d);
                Log.e("RetrieveTransaction", "Retrieval failed.");
            }
            f();
        } catch (Throwable th) {
            if (this.c.a() != 1) {
                this.c.a(2);
                this.c.a(this.d);
                Log.e("RetrieveTransaction", "Retrieval failed.");
            }
            f();
            throw th;
        }
    }
}
