package com.sd.android.mms.b.a;

import org.b.a.a.f;
import org.b.a.a.g;
import org.b.a.a.n;
import org.w3c.dom.NodeList;

public final class v extends o implements n {

    /* renamed from: a  reason: collision with root package name */
    f f82a = new t(this, this);

    v(e eVar, String str) {
        super(eVar, str.toUpperCase());
    }

    public final float a() {
        return this.f82a.a();
    }

    public final NodeList a(float f) {
        return this.f82a.a(f);
    }

    public final void a_(float f) {
        this.f82a.a_(f);
    }

    public final void b(float f) {
        this.f82a.b(f);
    }

    public final boolean b() {
        return this.f82a.b();
    }

    public final boolean c() {
        return this.f82a.c();
    }

    public final void d() {
        this.f82a.d();
    }

    public final void e() {
        this.f82a.e();
    }

    public final g f() {
        return this.f82a.f();
    }

    public final NodeList g() {
        return this.f82a.g();
    }

    public final g h() {
        return this.f82a.h();
    }

    public final short i() {
        return this.f82a.i();
    }
}
