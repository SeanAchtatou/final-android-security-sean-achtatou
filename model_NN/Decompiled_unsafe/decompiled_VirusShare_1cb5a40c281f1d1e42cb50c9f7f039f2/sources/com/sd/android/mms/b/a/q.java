package com.sd.android.mms.b.a;

import org.b.a.a.l;

final class q {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public final double f80a;
    private final l b;
    private final int c;

    public q(double d, l lVar, int i) {
        this.f80a = d;
        this.b = lVar;
        this.c = i;
    }

    public final double a() {
        return this.f80a;
    }

    public final l b() {
        return this.b;
    }

    public final int c() {
        return this.c;
    }
}
