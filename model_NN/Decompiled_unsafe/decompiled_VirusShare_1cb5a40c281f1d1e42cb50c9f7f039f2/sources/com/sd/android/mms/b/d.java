package com.sd.android.mms.b;

import org.w3c.dom.Attr;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.TypeInfo;
import org.w3c.dom.UserDataHandler;

public class d extends f implements Element {

    /* renamed from: a  reason: collision with root package name */
    private String f87a;
    private NamedNodeMap c = new b();

    protected d(c cVar, String str) {
        super(cVar);
        this.f87a = str;
    }

    public short compareDocumentPosition(Node node) {
        return 0;
    }

    public String getAttribute(String str) {
        Attr attributeNode = getAttributeNode(str);
        return attributeNode != null ? attributeNode.getValue() : "";
    }

    public String getAttributeNS(String str, String str2) {
        return null;
    }

    public Attr getAttributeNode(String str) {
        return (Attr) this.c.getNamedItem(str);
    }

    public Attr getAttributeNodeNS(String str, String str2) {
        return null;
    }

    public NamedNodeMap getAttributes() {
        return this.c;
    }

    public String getBaseURI() {
        return null;
    }

    public NodeList getElementsByTagName(String str) {
        return new a(this, str, true);
    }

    public NodeList getElementsByTagNameNS(String str, String str2) {
        return null;
    }

    public Object getFeature(String str, String str2) {
        return null;
    }

    public String getNodeName() {
        return this.f87a;
    }

    public short getNodeType() {
        return 1;
    }

    public TypeInfo getSchemaTypeInfo() {
        return null;
    }

    public String getTagName() {
        return this.f87a;
    }

    public String getTextContent() {
        return null;
    }

    public Object getUserData(String str) {
        return null;
    }

    public boolean hasAttribute(String str) {
        return getAttributeNode(str) != null;
    }

    public boolean hasAttributeNS(String str, String str2) {
        return false;
    }

    public boolean hasAttributes() {
        return this.c.getLength() > 0;
    }

    public boolean isDefaultNamespace(String str) {
        return false;
    }

    public boolean isEqualNode(Node node) {
        return false;
    }

    public boolean isSameNode(Node node) {
        return false;
    }

    public String lookupNamespaceURI(String str) {
        return null;
    }

    public String lookupPrefix(String str) {
        return null;
    }

    public void removeAttribute(String str) {
    }

    public void removeAttributeNS(String str, String str2) {
    }

    public Attr removeAttributeNode(Attr attr) {
        return null;
    }

    public void setAttribute(String str, String str2) {
        Attr attributeNode = getAttributeNode(str);
        if (attributeNode == null) {
            attributeNode = this.b.createAttribute(str);
        }
        attributeNode.setNodeValue(str2);
        this.c.setNamedItem(attributeNode);
    }

    public void setAttributeNS(String str, String str2, String str3) {
    }

    public Attr setAttributeNode(Attr attr) {
        return null;
    }

    public Attr setAttributeNodeNS(Attr attr) {
        return null;
    }

    public void setIdAttribute(String str, boolean z) {
    }

    public void setIdAttributeNS(String str, String str2, boolean z) {
    }

    public void setIdAttributeNode(Attr attr, boolean z) {
    }

    public void setTextContent(String str) {
    }

    public Object setUserData(String str, Object obj, UserDataHandler userDataHandler) {
        return null;
    }
}
