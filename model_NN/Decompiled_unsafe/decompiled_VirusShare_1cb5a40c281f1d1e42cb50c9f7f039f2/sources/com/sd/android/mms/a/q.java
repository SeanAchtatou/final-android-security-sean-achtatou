package com.sd.android.mms.a;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.text.TextUtils;
import com.sd.a.a.a.b;
import com.sd.android.mms.f.a;
import java.lang.ref.SoftReference;
import org.b.a.b.c;

public final class q extends b {
    private int h;
    private int i;
    private SoftReference j = new SoftReference(null);

    public q(Context context, Uri uri, j jVar) {
        super(context, "img", uri, jVar);
        a aVar = new a(this.c, uri);
        this.e = aVar.a();
        if (TextUtils.isEmpty(this.e)) {
            throw new b("Type of media is unknown.");
        }
        this.d = aVar.b();
        this.h = aVar.c();
        this.i = aVar.d();
        g a2 = n.a();
        a2.a(this.e);
        a2.b(this.h, this.i);
    }

    public q(Context context, String str, String str2, Uri uri, j jVar) {
        super(context, "img", str, str2, uri, jVar);
        a aVar = new a(this.c, i());
        this.h = aVar.c();
        this.i = aVar.d();
    }

    public q(Context context, String str, String str2, com.sd.android.mms.e.b bVar, j jVar) {
        super(context, "img", str, str2, bVar, jVar);
    }

    /* JADX WARNING: Removed duplicated region for block: B:31:0x005b A[SYNTHETIC, Splitter:B:31:0x005b] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private android.graphics.Bitmap c(android.net.Uri r7) {
        /*
            r6 = this;
            r5 = 480(0x1e0, float:6.73E-43)
            r4 = 0
            int r0 = r6.h
            int r1 = r6.i
            r2 = 1
        L_0x0008:
            int r3 = r0 / r2
            if (r3 > r5) goto L_0x0010
            int r3 = r1 / r2
            if (r3 <= r5) goto L_0x0013
        L_0x0010:
            int r2 = r2 * 2
            goto L_0x0008
        L_0x0013:
            android.graphics.BitmapFactory$Options r0 = new android.graphics.BitmapFactory$Options
            r0.<init>()
            r0.inSampleSize = r2
            android.content.Context r1 = r6.c     // Catch:{ FileNotFoundException -> 0x003a, all -> 0x0057 }
            android.content.ContentResolver r1 = r1.getContentResolver()     // Catch:{ FileNotFoundException -> 0x003a, all -> 0x0057 }
            java.io.InputStream r1 = r1.openInputStream(r7)     // Catch:{ FileNotFoundException -> 0x003a, all -> 0x0057 }
            r2 = 0
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeStream(r1, r2, r0)     // Catch:{ FileNotFoundException -> 0x006c }
            if (r1 == 0) goto L_0x002e
            r1.close()     // Catch:{ IOException -> 0x002f }
        L_0x002e:
            return r0
        L_0x002f:
            r1 = move-exception
            java.lang.String r2 = "ImageModel"
            java.lang.String r3 = r1.getMessage()
            android.util.Log.e(r2, r3, r1)
            goto L_0x002e
        L_0x003a:
            r0 = move-exception
            r1 = r4
        L_0x003c:
            java.lang.String r2 = "ImageModel"
            java.lang.String r3 = r0.getMessage()     // Catch:{ all -> 0x006a }
            android.util.Log.e(r2, r3, r0)     // Catch:{ all -> 0x006a }
            if (r1 == 0) goto L_0x004a
            r1.close()     // Catch:{ IOException -> 0x004c }
        L_0x004a:
            r0 = r4
            goto L_0x002e
        L_0x004c:
            r0 = move-exception
            java.lang.String r1 = "ImageModel"
            java.lang.String r2 = r0.getMessage()
            android.util.Log.e(r1, r2, r0)
            goto L_0x004a
        L_0x0057:
            r0 = move-exception
            r1 = r4
        L_0x0059:
            if (r1 == 0) goto L_0x005e
            r1.close()     // Catch:{ IOException -> 0x005f }
        L_0x005e:
            throw r0
        L_0x005f:
            r1 = move-exception
            java.lang.String r2 = "ImageModel"
            java.lang.String r3 = r1.getMessage()
            android.util.Log.e(r2, r3, r1)
            goto L_0x005e
        L_0x006a:
            r0 = move-exception
            goto L_0x0059
        L_0x006c:
            r0 = move-exception
            goto L_0x003c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.sd.android.mms.a.q.c(android.net.Uri):android.graphics.Bitmap");
    }

    public final void a(c cVar) {
        if (cVar.a().equals("SmilMediaStart")) {
            this.f60a = true;
        } else if (this.f != 1) {
            this.f60a = false;
        }
        a(false);
    }

    public final Bitmap x() {
        Bitmap bitmap = (Bitmap) this.j.get();
        if (bitmap != null) {
            return bitmap;
        }
        Bitmap c = c(h());
        this.j = new SoftReference(c);
        return c;
    }

    public final Bitmap y() {
        Bitmap bitmap = (Bitmap) this.j.get();
        if (bitmap != null) {
            return bitmap;
        }
        Bitmap c = c(i());
        this.j = new SoftReference(c);
        return c;
    }
}
