package com.sd.android.mms.b.a;

import java.util.ArrayList;
import org.b.a.a.g;
import org.b.a.a.i;
import org.b.a.a.l;
import org.b.a.b.a;
import org.b.a.b.c;
import org.w3c.dom.NodeList;

final class t extends a {
    private /* synthetic */ v b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    t(v vVar, i iVar) {
        super(iVar);
        this.b = vVar;
    }

    public final void a_(float f) {
    }

    public final boolean b() {
        c a2 = ((a) this.b.getOwnerDocument()).a("Event");
        a2.a("SmilSlideStart");
        this.b.a(a2);
        return true;
    }

    public final boolean c() {
        c a2 = ((a) this.b.getOwnerDocument()).a("Event");
        a2.a("SmilSlideEnd");
        this.b.a(a2);
        return true;
    }

    /* access modifiers changed from: package-private */
    public final l c_() {
        return ((e) this.f77a.getOwnerDocument()).f73a;
    }

    public final void d() {
    }

    public final void e() {
    }

    public final g f() {
        g f = super.f();
        if (f.a() <= 1) {
            return f;
        }
        ArrayList arrayList = new ArrayList();
        arrayList.add(f.a(0));
        return new u(arrayList);
    }

    public final NodeList g() {
        return this.b.getChildNodes();
    }
}
