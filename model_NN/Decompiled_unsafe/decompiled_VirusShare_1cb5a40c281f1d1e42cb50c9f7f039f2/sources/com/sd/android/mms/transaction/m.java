package com.sd.android.mms.transaction;

import android.a.g;
import android.content.Context;
import android.net.Uri;
import android.util.Log;
import com.sd.a.a.a.a.d;
import com.sd.a.a.a.a.k;
import com.sd.a.a.a.a.n;
import com.sd.a.a.a.a.p;
import com.sd.a.a.a.a.r;
import com.sd.a.a.a.a.v;
import com.sd.a.a.a.b;
import com.sd.android.mms.d.l;
import java.io.IOException;

public final class m extends e implements Runnable {
    private Uri d;
    private p e;

    public m(Context context, int i, j jVar, p pVar) {
        super(context, i, jVar);
        try {
            this.d = d.a(context).a(pVar, g.f10a);
            this.e = pVar;
            this.b = new String(pVar.f());
        } catch (b e2) {
            Log.e("NotificationTransaction", "Failed to save NotificationInd in constructor.", e2);
            throw new IllegalArgumentException();
        }
    }

    public m(Context context, int i, j jVar, String str) {
        super(context, i, jVar);
        this.d = Uri.parse(str);
        try {
            this.e = (p) d.a(context).a(this.d);
            this.b = new String(this.e.f());
            a(l.a(context));
        } catch (b e2) {
            Log.e("NotificationTransaction", "Failed to load NotificationInd from: " + str, e2);
            throw new IllegalArgumentException();
        }
    }

    private void a(int i) {
        a(new k(this.f113a, new v(this.e.f(), i)).a());
    }

    public final void b() {
        new Thread(this).start();
    }

    public final int e() {
        return 0;
    }

    public final void run() {
        int i;
        l b = l.b();
        boolean a2 = b.a();
        if (!a2) {
            try {
                b.a(this.d, 128);
                a(131);
                this.c.a(this.d);
                if (!a2) {
                    this.c.a(1);
                }
                if (this.c.a() != 1) {
                    this.c.a(2);
                    Log.e("NotificationTransaction", "NotificationTransaction failed.");
                }
                f();
            } catch (Throwable th) {
                this.c.a(this.d);
                if (!a2) {
                    this.c.a(1);
                }
                if (this.c.a() != 1) {
                    this.c.a(2);
                    Log.e("NotificationTransaction", "NotificationTransaction failed.");
                }
                f();
                throw th;
            }
        } else {
            b.a(this.d, 129);
            byte[] b2 = this.e.b();
            if (b2 == null) {
                throw new b("Content-Location may not be null.");
            }
            String str = new String(b2);
            byte[] bArr = null;
            try {
                bArr = a(str);
            } catch (IOException e2) {
                this.c.a(2);
            }
            if (bArr != null) {
                r a3 = new n(bArr).a();
                if (a3 == null || a3.m() != 132) {
                    Log.e("NotificationTransaction", "Invalid M-RETRIEVE.CONF PDU.");
                    this.c.a(2);
                    i = 132;
                } else {
                    Uri a4 = d.a(this.f113a).a(a3, g.f10a);
                    com.sd.a.a.a.b.b.a(this.f113a, this.f113a.getContentResolver(), this.d, (String) null);
                    this.d = a4;
                    i = 129;
                }
            } else {
                i = 131;
            }
            switch (i) {
                case 129:
                    this.c.a(1);
                    break;
                case 131:
                    if (this.c.a() == 0) {
                        this.c.a(1);
                        break;
                    }
                    break;
            }
            a(i);
            this.c.a(this.d);
            if (!a2) {
                this.c.a(1);
            }
            if (this.c.a() != 1) {
                this.c.a(2);
                Log.e("NotificationTransaction", "NotificationTransaction failed.");
            }
            f();
        }
    }
}
