package com.sd.android.mms.b.a;

import com.sd.android.mms.b.a;
import java.util.ArrayList;
import org.b.a.a.i;
import org.b.a.a.l;
import org.b.a.a.p;
import org.w3c.dom.NodeList;

public abstract class d extends w implements p {
    d(i iVar) {
        super(iVar);
    }

    public final float a() {
        float a2 = super.a();
        if (a2 != 0.0f) {
            return a2;
        }
        NodeList g = g();
        float f = a2;
        for (int i = 0; i < g.getLength(); i++) {
            l lVar = (l) g.item(i);
            if (lVar.a() < 0.0f) {
                return -1.0f;
            }
            f += lVar.a();
        }
        return f;
    }

    public final NodeList a(float f) {
        NodeList g = g();
        ArrayList arrayList = new ArrayList();
        float f2 = f;
        for (int i = 0; i < g.getLength(); i++) {
            f2 -= ((l) g.item(i)).a();
            if (f2 < 0.0f) {
                arrayList.add(g.item(i));
                return new a(arrayList);
            }
        }
        return new a(arrayList);
    }
}
