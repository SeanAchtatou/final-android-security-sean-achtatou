package com.sd.android.mms.d;

import android.a.d;
import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;
import android.provider.Contacts;
import android.text.TextUtils;
import com.sd.a.a.a.b.b;
import com.snda.youni.C0000R;
import com.snda.youni.mms.ui.m;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;

public final class i {

    /* renamed from: a  reason: collision with root package name */
    private static final Uri f99a = Uri.parse(Contacts.Phones.CONTENT_URI + "_with_presence");
    private static final Uri b = Contacts.Phones.CONTENT_URI;
    private static final String[] c = {"number", "label", "name", "person"};
    private static final Uri d = Uri.withAppendedPath(Contacts.ContactMethods.CONTENT_URI, "with_presence");
    private static final String[] e = {"name", "person"};
    private static i f;
    private final Context g;
    private String[] h = new String[1];
    private final HashMap i = new HashMap();
    private Thread j = null;
    /* access modifiers changed from: private */
    public Object k = new Object();
    /* access modifiers changed from: private */
    public boolean l = false;
    /* access modifiers changed from: private */
    public boolean m = false;

    private i(Context context) {
        this.g = context;
        ContentResolver contentResolver = context.getContentResolver();
        contentResolver.registerContentObserver(Contacts.Phones.CONTENT_URI, true, new e(this, new Handler()));
        contentResolver.registerContentObserver(Contacts.ContactMethods.CONTENT_EMAIL_URI, true, new f(this, new Handler()));
    }

    public static i a() {
        return f;
    }

    public static void a(Context context) {
        f = new i(context);
    }

    private void a(List list, List list2) {
        synchronized (this.i) {
            for (String str : this.i.keySet()) {
                if (d.b(str)) {
                    if (list2 != null) {
                        list2.add(str);
                    }
                } else if (list != null) {
                    list.add(str);
                } else {
                    continue;
                }
            }
        }
    }

    private a b(Context context, String str) {
        String sb;
        a c2;
        if (str == null) {
            sb = null;
        } else {
            int length = str.length();
            StringBuilder sb2 = new StringBuilder(length);
            for (int i2 = 0; i2 < length; i2++) {
                char charAt = str.charAt(i2);
                if (" ()-./".indexOf(charAt) == -1) {
                    sb2.append(charAt);
                }
            }
            sb = sb2.toString();
        }
        synchronized (this.i) {
            if (this.i.containsKey(sb)) {
                c2 = (a) this.i.get(sb);
                if (!c2.a()) {
                }
            }
            c2 = c(context, sb);
            this.i.put(sb, c2);
        }
        return c2;
    }

    private a c(Context context, String str) {
        a aVar = new a(this);
        aVar.f92a = str;
        this.h[0] = str;
        Cursor query = context.getContentResolver().query(b, c, "PHONE_NUMBERS_EQUAL(number,?)", this.h, null);
        try {
            if (query.moveToFirst()) {
                aVar.b = query.getString(1);
                aVar.c = query.getString(2);
                aVar.d = query.getLong(3);
                aVar.e = C0000R.drawable.icn_youni;
            }
            return aVar;
        } finally {
            query.close();
        }
    }

    static /* synthetic */ void c(i iVar) {
        if (iVar.j == null) {
            iVar.j = new Thread(new g(iVar));
            iVar.j.start();
        }
    }

    private a d(Context context, String str) {
        a e2;
        synchronized (this.i) {
            if (this.i.containsKey(str)) {
                e2 = (a) this.i.get(str);
                if (!e2.a()) {
                }
            }
            e2 = e(context, str);
            this.i.put(str, e2);
        }
        return e2;
    }

    private a e(Context context, String str) {
        a aVar = new a(this);
        this.h[0] = str;
        Cursor a2 = b.a(context, context.getContentResolver(), d, e, "data=?", this.h, null);
        if (a2 != null) {
            while (true) {
                try {
                    if (!a2.moveToNext()) {
                        break;
                    }
                    a2.getInt(1);
                    aVar.e = C0000R.drawable.icn_youni;
                    aVar.d = a2.getLong(2);
                    String string = a2.getString(0);
                    if (!TextUtils.isEmpty(string)) {
                        aVar.c = string;
                        break;
                    }
                } finally {
                    a2.close();
                }
            }
        }
        return aVar;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:21:0x002f, code lost:
        r7.a(r1, r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0032, code lost:
        if (r1 == null) goto L_0x005a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0034, code lost:
        r1 = r1.iterator();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x003c, code lost:
        if (r1.hasNext() == false) goto L_0x005a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x003e, code lost:
        r0 = (java.lang.String) r1.next();
        r3 = r7.i;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0046, code lost:
        monitor-enter(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:?, code lost:
        r7.i.put(r0, r7.c(r7.g, r0));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0052, code lost:
        monitor-exit(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x005a, code lost:
        if (r2 == null) goto L_0x0001;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x005c, code lost:
        r1 = r2.iterator();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x0064, code lost:
        if (r1.hasNext() == false) goto L_0x0001;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x0066, code lost:
        r0 = (java.lang.String) r1.next();
        r2 = r7.i;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x006e, code lost:
        monitor-enter(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:?, code lost:
        r7.i.put(r0, r7.e(r7.g, r0));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x007a, code lost:
        monitor-exit(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x0001, code lost:
        continue;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static /* synthetic */ void e(com.sd.android.mms.d.i r7) {
        /*
            r6 = 0
        L_0x0001:
            r0 = 5000(0x1388, double:2.4703E-320)
            java.lang.Thread.sleep(r0)     // Catch:{ InterruptedException -> 0x007f }
        L_0x0006:
            java.lang.Object r0 = r7.k
            monitor-enter(r0)
            boolean r1 = r7.l     // Catch:{ all -> 0x0057 }
            if (r1 != 0) goto L_0x0016
            boolean r1 = r7.m     // Catch:{ all -> 0x0057 }
            if (r1 != 0) goto L_0x0016
            r1 = 0
            r7.j = r1     // Catch:{ all -> 0x0057 }
            monitor-exit(r0)     // Catch:{ all -> 0x0057 }
            return
        L_0x0016:
            boolean r1 = r7.l     // Catch:{ all -> 0x0057 }
            if (r1 == 0) goto L_0x0083
            java.util.ArrayList r1 = new java.util.ArrayList     // Catch:{ all -> 0x0057 }
            r1.<init>()     // Catch:{ all -> 0x0057 }
            r2 = 0
            r7.l = r2     // Catch:{ all -> 0x0057 }
        L_0x0022:
            boolean r2 = r7.m     // Catch:{ all -> 0x0057 }
            if (r2 == 0) goto L_0x0081
            java.util.ArrayList r2 = new java.util.ArrayList     // Catch:{ all -> 0x0057 }
            r2.<init>()     // Catch:{ all -> 0x0057 }
            r3 = 0
            r7.m = r3     // Catch:{ all -> 0x0057 }
        L_0x002e:
            monitor-exit(r0)     // Catch:{ all -> 0x0057 }
            r7.a(r1, r2)
            if (r1 == 0) goto L_0x005a
            java.util.Iterator r1 = r1.iterator()
        L_0x0038:
            boolean r0 = r1.hasNext()
            if (r0 == 0) goto L_0x005a
            java.lang.Object r0 = r1.next()
            java.lang.String r0 = (java.lang.String) r0
            java.util.HashMap r3 = r7.i
            monitor-enter(r3)
            android.content.Context r4 = r7.g     // Catch:{ all -> 0x0054 }
            com.sd.android.mms.d.a r4 = r7.c(r4, r0)     // Catch:{ all -> 0x0054 }
            java.util.HashMap r5 = r7.i     // Catch:{ all -> 0x0054 }
            r5.put(r0, r4)     // Catch:{ all -> 0x0054 }
            monitor-exit(r3)     // Catch:{ all -> 0x0054 }
            goto L_0x0038
        L_0x0054:
            r0 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x0054 }
            throw r0
        L_0x0057:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x0057 }
            throw r1
        L_0x005a:
            if (r2 == 0) goto L_0x0001
            java.util.Iterator r1 = r2.iterator()
        L_0x0060:
            boolean r0 = r1.hasNext()
            if (r0 == 0) goto L_0x0001
            java.lang.Object r0 = r1.next()
            java.lang.String r0 = (java.lang.String) r0
            java.util.HashMap r2 = r7.i
            monitor-enter(r2)
            android.content.Context r3 = r7.g     // Catch:{ all -> 0x007c }
            com.sd.android.mms.d.a r3 = r7.e(r3, r0)     // Catch:{ all -> 0x007c }
            java.util.HashMap r4 = r7.i     // Catch:{ all -> 0x007c }
            r4.put(r0, r3)     // Catch:{ all -> 0x007c }
            monitor-exit(r2)     // Catch:{ all -> 0x007c }
            goto L_0x0060
        L_0x007c:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x007c }
            throw r0
        L_0x007f:
            r0 = move-exception
            goto L_0x0006
        L_0x0081:
            r2 = r6
            goto L_0x002e
        L_0x0083:
            r1 = r6
            goto L_0x0022
        */
        throw new UnsupportedOperationException("Method not decompiled: com.sd.android.mms.d.i.e(com.sd.android.mms.d.i):void");
    }

    public final String a(Context context, String str) {
        if (TextUtils.isEmpty(str)) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        for (String str2 : str.split(";")) {
            if (str2.length() > 0) {
                sb.append(";");
                if (m.a(context, str2)) {
                    sb.append("me");
                } else if (d.b(str2)) {
                    Matcher matcher = d.b.matcher(str2);
                    if (matcher.matches()) {
                        str2 = matcher.group(1);
                        Matcher matcher2 = d.c.matcher(str2);
                        if (matcher2.matches()) {
                            str2 = matcher2.group(1);
                        }
                    } else {
                        a d2 = d(context, str2);
                        if (!(d2 == null || d2.c == null)) {
                            str2 = d2.c;
                        }
                    }
                    sb.append(str2);
                } else {
                    a d3 = d.b(str2) ? d(context, str2) : b(context, str2);
                    if (d3 != null && !TextUtils.isEmpty(d3.c)) {
                        str2 = d3.c;
                    }
                    sb.append(str2);
                }
            }
        }
        return sb.length() > 0 ? sb.substring(1) : "";
    }
}
