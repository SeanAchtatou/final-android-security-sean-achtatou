package com.sd.android.mms.a;

import android.content.Context;
import android.net.Uri;
import com.sd.android.mms.e.b;
import org.b.a.b.c;

public final class l extends b {
    public l(Context context, String str, String str2, Uri uri, j jVar) {
        super(context, "video", str, str2, uri, jVar);
    }

    public l(Context context, String str, String str2, b bVar, j jVar) {
        super(context, "video", str, str2, bVar, jVar);
    }

    public final void a(c cVar) {
        k kVar;
        String a2 = cVar.a();
        k kVar2 = k.NO_ACTIVE_ACTION;
        if (a2.equals("SmilMediaStart")) {
            kVar = k.START;
            this.f60a = true;
        } else if (a2.equals("SmilMediaEnd")) {
            kVar = k.STOP;
            if (this.f != 1) {
                this.f60a = false;
            }
        } else if (a2.equals("SmilMediaPause")) {
            kVar = k.PAUSE;
            this.f60a = true;
        } else if (a2.equals("SmilMediaSeek")) {
            kVar = k.SEEK;
            this.g = cVar.b();
            this.f60a = true;
        } else {
            kVar = kVar2;
        }
        a(kVar);
        a(false);
    }

    /* access modifiers changed from: protected */
    public final boolean v() {
        return true;
    }
}
