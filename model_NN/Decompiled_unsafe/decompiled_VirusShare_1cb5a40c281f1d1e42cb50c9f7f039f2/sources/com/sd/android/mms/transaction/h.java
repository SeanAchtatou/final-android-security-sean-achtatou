package com.sd.android.mms.transaction;

import android.util.Log;
import java.io.IOException;
import java.util.Locale;

public final class h {

    /* renamed from: a  reason: collision with root package name */
    private static final String f116a;

    static {
        Locale locale = Locale.getDefault();
        StringBuilder sb = new StringBuilder();
        a(sb, locale);
        if (!locale.equals(Locale.US)) {
            if (sb.length() > 0) {
                sb.append(", ");
            }
            a(sb, Locale.US);
        }
        f116a = sb.toString();
    }

    private h() {
    }

    private static void a(Exception exc) {
        StringBuilder sb = new StringBuilder("exception message ");
        if (!(exc == null || exc.getMessage() == null)) {
            sb.append(exc.getMessage());
        }
        Log.e("JB-HttpUtils", sb.toString());
        throw new IOException(sb.toString());
    }

    private static void a(StringBuilder sb, Locale locale) {
        String language = locale.getLanguage();
        if (language != null) {
            sb.append(language);
            String country = locale.getCountry();
            if (country != null) {
                sb.append("-");
                sb.append(country);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.b.a.c.execute(org.apache.http.HttpHost, org.apache.http.HttpRequest):org.apache.http.HttpResponse
     arg types: [org.apache.http.HttpHost, org.apache.http.client.methods.HttpPost]
     candidates:
      android.b.a.c.execute(org.apache.http.client.methods.HttpUriRequest, org.apache.http.client.ResponseHandler):java.lang.Object
      android.b.a.c.execute(org.apache.http.client.methods.HttpUriRequest, org.apache.http.protocol.HttpContext):org.apache.http.HttpResponse
      android.b.a.c.execute(org.apache.http.HttpHost, org.apache.http.HttpRequest):org.apache.http.HttpResponse */
    /* JADX WARNING: Removed duplicated region for block: B:104:0x025b  */
    /* JADX WARNING: Removed duplicated region for block: B:108:0x0264  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x01b1  */
    /* JADX WARNING: Removed duplicated region for block: B:79:0x0215  */
    /* JADX WARNING: Removed duplicated region for block: B:92:0x0243  */
    /* JADX WARNING: Removed duplicated region for block: B:98:0x024f  */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:76:0x0210=Splitter:B:76:0x0210, B:101:0x0256=Splitter:B:101:0x0256, B:34:0x019a=Splitter:B:34:0x019a, B:95:0x024a=Splitter:B:95:0x024a, B:43:0x01ac=Splitter:B:43:0x01ac, B:89:0x023e=Splitter:B:89:0x023e} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    protected static byte[] a(android.content.Context r5, long r6, java.lang.String r8, byte[] r9, int r10, boolean r11, java.lang.String r12, int r13) {
        /*
            if (r8 != 0) goto L_0x000a
            java.lang.IllegalArgumentException r5 = new java.lang.IllegalArgumentException
            java.lang.String r6 = "URL must not be null."
            r5.<init>(r6)
            throw r5
        L_0x000a:
            java.lang.String r0 = "JB-HttpUtils"
            java.lang.String r1 = "httpConnection: params list"
            android.util.Log.v(r0, r1)
            java.lang.String r0 = "JB-HttpUtils"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "\ttoken\t\t= "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r6)
            java.lang.String r1 = r1.toString()
            android.util.Log.v(r0, r1)
            java.lang.String r0 = "JB-HttpUtils"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "\turl\t\t= "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r8)
            java.lang.String r1 = r1.toString()
            android.util.Log.v(r0, r1)
            java.lang.String r0 = "JB-HttpUtils"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "\tmethod\t\t= "
            java.lang.StringBuilder r1 = r1.append(r2)
            r2 = 1
            if (r10 != r2) goto L_0x0124
            java.lang.String r2 = "POST"
        L_0x0053:
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            android.util.Log.v(r0, r1)
            java.lang.String r0 = "JB-HttpUtils"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "\tisProxySet\t= "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r11)
            java.lang.String r1 = r1.toString()
            android.util.Log.v(r0, r1)
            java.lang.String r0 = "JB-HttpUtils"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "\tproxyHost\t= "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r12)
            java.lang.String r1 = r1.toString()
            android.util.Log.v(r0, r1)
            java.lang.String r0 = "JB-HttpUtils"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "\tproxyPort\t= "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r13)
            java.lang.String r1 = r1.toString()
            android.util.Log.v(r0, r1)
            java.lang.String r0 = "JB-HttpUtils"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "\tpdu\t\t= "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = java.util.Arrays.toString(r9)
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            android.util.Log.v(r0, r1)
            r0 = 0
            java.net.URI r1 = new java.net.URI     // Catch:{ URISyntaxException -> 0x027d, IllegalStateException -> 0x0279, IllegalArgumentException -> 0x0276, SocketException -> 0x023c, SocketTimeoutException -> 0x0248, Exception -> 0x0254, all -> 0x0260 }
            r1.<init>(r8)     // Catch:{ URISyntaxException -> 0x027d, IllegalStateException -> 0x0279, IllegalArgumentException -> 0x0276, SocketException -> 0x023c, SocketTimeoutException -> 0x0248, Exception -> 0x0254, all -> 0x0260 }
            org.apache.http.HttpHost r2 = new org.apache.http.HttpHost     // Catch:{ URISyntaxException -> 0x027d, IllegalStateException -> 0x0279, IllegalArgumentException -> 0x0276, SocketException -> 0x023c, SocketTimeoutException -> 0x0248, Exception -> 0x0254, all -> 0x0260 }
            java.lang.String r3 = r1.getHost()     // Catch:{ URISyntaxException -> 0x027d, IllegalStateException -> 0x0279, IllegalArgumentException -> 0x0276, SocketException -> 0x023c, SocketTimeoutException -> 0x0248, Exception -> 0x0254, all -> 0x0260 }
            int r1 = r1.getPort()     // Catch:{ URISyntaxException -> 0x027d, IllegalStateException -> 0x0279, IllegalArgumentException -> 0x0276, SocketException -> 0x023c, SocketTimeoutException -> 0x0248, Exception -> 0x0254, all -> 0x0260 }
            java.lang.String r4 = "http"
            r2.<init>(r3, r1, r4)     // Catch:{ URISyntaxException -> 0x027d, IllegalStateException -> 0x0279, IllegalArgumentException -> 0x0276, SocketException -> 0x023c, SocketTimeoutException -> 0x0248, Exception -> 0x0254, all -> 0x0260 }
            java.lang.String r1 = "Android-Mms/0.1"
            android.b.a.c r1 = android.b.a.c.a(r1)     // Catch:{ URISyntaxException -> 0x027d, IllegalStateException -> 0x0279, IllegalArgumentException -> 0x0276, SocketException -> 0x023c, SocketTimeoutException -> 0x0248, Exception -> 0x0254, all -> 0x0260 }
            org.apache.http.params.HttpParams r3 = r1.getParams()     // Catch:{ URISyntaxException -> 0x027d, IllegalStateException -> 0x0279, IllegalArgumentException -> 0x0276, SocketException -> 0x023c, SocketTimeoutException -> 0x0248, Exception -> 0x0254, all -> 0x0260 }
            java.lang.String r4 = "UTF-8"
            org.apache.http.params.HttpProtocolParams.setContentCharset(r3, r4)     // Catch:{ URISyntaxException -> 0x027d, IllegalStateException -> 0x0279, IllegalArgumentException -> 0x0276, SocketException -> 0x023c, SocketTimeoutException -> 0x0248, Exception -> 0x0254, all -> 0x0260 }
            switch(r10) {
                case 1: goto L_0x012f;
                case 2: goto L_0x01a4;
                default: goto L_0x00e9;
            }
        L_0x00e9:
            java.lang.String r5 = "JB-HttpUtils"
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ URISyntaxException -> 0x0198, IllegalStateException -> 0x01aa, IllegalArgumentException -> 0x020e, SocketException -> 0x0273, SocketTimeoutException -> 0x0270, Exception -> 0x026d, all -> 0x0268 }
            r6.<init>()     // Catch:{ URISyntaxException -> 0x0198, IllegalStateException -> 0x01aa, IllegalArgumentException -> 0x020e, SocketException -> 0x0273, SocketTimeoutException -> 0x0270, Exception -> 0x026d, all -> 0x0268 }
            java.lang.String r7 = "Unknown HTTP method: "
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ URISyntaxException -> 0x0198, IllegalStateException -> 0x01aa, IllegalArgumentException -> 0x020e, SocketException -> 0x0273, SocketTimeoutException -> 0x0270, Exception -> 0x026d, all -> 0x0268 }
            java.lang.StringBuilder r6 = r6.append(r10)     // Catch:{ URISyntaxException -> 0x0198, IllegalStateException -> 0x01aa, IllegalArgumentException -> 0x020e, SocketException -> 0x0273, SocketTimeoutException -> 0x0270, Exception -> 0x026d, all -> 0x0268 }
            java.lang.String r7 = ". Must be one of POST["
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ URISyntaxException -> 0x0198, IllegalStateException -> 0x01aa, IllegalArgumentException -> 0x020e, SocketException -> 0x0273, SocketTimeoutException -> 0x0270, Exception -> 0x026d, all -> 0x0268 }
            r7 = 1
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ URISyntaxException -> 0x0198, IllegalStateException -> 0x01aa, IllegalArgumentException -> 0x020e, SocketException -> 0x0273, SocketTimeoutException -> 0x0270, Exception -> 0x026d, all -> 0x0268 }
            java.lang.String r7 = "] or GET["
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ URISyntaxException -> 0x0198, IllegalStateException -> 0x01aa, IllegalArgumentException -> 0x020e, SocketException -> 0x0273, SocketTimeoutException -> 0x0270, Exception -> 0x026d, all -> 0x0268 }
            r7 = 2
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ URISyntaxException -> 0x0198, IllegalStateException -> 0x01aa, IllegalArgumentException -> 0x020e, SocketException -> 0x0273, SocketTimeoutException -> 0x0270, Exception -> 0x026d, all -> 0x0268 }
            java.lang.String r7 = "]."
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ URISyntaxException -> 0x0198, IllegalStateException -> 0x01aa, IllegalArgumentException -> 0x020e, SocketException -> 0x0273, SocketTimeoutException -> 0x0270, Exception -> 0x026d, all -> 0x0268 }
            java.lang.String r6 = r6.toString()     // Catch:{ URISyntaxException -> 0x0198, IllegalStateException -> 0x01aa, IllegalArgumentException -> 0x020e, SocketException -> 0x0273, SocketTimeoutException -> 0x0270, Exception -> 0x026d, all -> 0x0268 }
            android.util.Log.e(r5, r6)     // Catch:{ URISyntaxException -> 0x0198, IllegalStateException -> 0x01aa, IllegalArgumentException -> 0x020e, SocketException -> 0x0273, SocketTimeoutException -> 0x0270, Exception -> 0x026d, all -> 0x0268 }
            if (r1 == 0) goto L_0x0122
            r1.a()
        L_0x0122:
            r5 = 0
        L_0x0123:
            return r5
        L_0x0124:
            r2 = 2
            if (r10 != r2) goto L_0x012b
            java.lang.String r2 = "GET"
            goto L_0x0053
        L_0x012b:
            java.lang.String r2 = "UNKNOWN"
            goto L_0x0053
        L_0x012f:
            com.sd.android.mms.transaction.k r10 = new com.sd.android.mms.transaction.k     // Catch:{ URISyntaxException -> 0x0198, IllegalStateException -> 0x01aa, IllegalArgumentException -> 0x020e, SocketException -> 0x0273, SocketTimeoutException -> 0x0270, Exception -> 0x026d, all -> 0x0268 }
            r10.<init>(r5, r6, r9)     // Catch:{ URISyntaxException -> 0x0198, IllegalStateException -> 0x01aa, IllegalArgumentException -> 0x020e, SocketException -> 0x0273, SocketTimeoutException -> 0x0270, Exception -> 0x026d, all -> 0x0268 }
            java.lang.String r5 = "application/vnd.wap.mms-message"
            r10.setContentType(r5)     // Catch:{ URISyntaxException -> 0x0198, IllegalStateException -> 0x01aa, IllegalArgumentException -> 0x020e, SocketException -> 0x0273, SocketTimeoutException -> 0x0270, Exception -> 0x026d, all -> 0x0268 }
            org.apache.http.client.methods.HttpPost r5 = new org.apache.http.client.methods.HttpPost     // Catch:{ URISyntaxException -> 0x0198, IllegalStateException -> 0x01aa, IllegalArgumentException -> 0x020e, SocketException -> 0x0273, SocketTimeoutException -> 0x0270, Exception -> 0x026d, all -> 0x0268 }
            r5.<init>(r8)     // Catch:{ URISyntaxException -> 0x0198, IllegalStateException -> 0x01aa, IllegalArgumentException -> 0x020e, SocketException -> 0x0273, SocketTimeoutException -> 0x0270, Exception -> 0x026d, all -> 0x0268 }
            r5.setEntity(r10)     // Catch:{ URISyntaxException -> 0x0198, IllegalStateException -> 0x01aa, IllegalArgumentException -> 0x020e, SocketException -> 0x0273, SocketTimeoutException -> 0x0270, Exception -> 0x026d, all -> 0x0268 }
        L_0x0141:
            org.apache.http.params.HttpParams r6 = r1.getParams()     // Catch:{ URISyntaxException -> 0x0198, IllegalStateException -> 0x01aa, IllegalArgumentException -> 0x020e, SocketException -> 0x0273, SocketTimeoutException -> 0x0270, Exception -> 0x026d, all -> 0x0268 }
            if (r11 == 0) goto L_0x014f
            org.apache.http.HttpHost r7 = new org.apache.http.HttpHost     // Catch:{ URISyntaxException -> 0x0198, IllegalStateException -> 0x01aa, IllegalArgumentException -> 0x020e, SocketException -> 0x0273, SocketTimeoutException -> 0x0270, Exception -> 0x026d, all -> 0x0268 }
            r7.<init>(r12, r13)     // Catch:{ URISyntaxException -> 0x0198, IllegalStateException -> 0x01aa, IllegalArgumentException -> 0x020e, SocketException -> 0x0273, SocketTimeoutException -> 0x0270, Exception -> 0x026d, all -> 0x0268 }
            org.apache.http.conn.params.ConnRouteParams.setDefaultProxy(r6, r7)     // Catch:{ URISyntaxException -> 0x0198, IllegalStateException -> 0x01aa, IllegalArgumentException -> 0x020e, SocketException -> 0x0273, SocketTimeoutException -> 0x0270, Exception -> 0x026d, all -> 0x0268 }
        L_0x014f:
            r5.setParams(r6)     // Catch:{ URISyntaxException -> 0x0198, IllegalStateException -> 0x01aa, IllegalArgumentException -> 0x020e, SocketException -> 0x0273, SocketTimeoutException -> 0x0270, Exception -> 0x026d, all -> 0x0268 }
            java.lang.String r6 = "Accept"
            java.lang.String r7 = "*/*, application/vnd.wap.mms-message, application/vnd.wap.sic"
            r5.addHeader(r6, r7)     // Catch:{ URISyntaxException -> 0x0198, IllegalStateException -> 0x01aa, IllegalArgumentException -> 0x020e, SocketException -> 0x0273, SocketTimeoutException -> 0x0270, Exception -> 0x026d, all -> 0x0268 }
            java.lang.String r6 = com.sd.android.mms.a.c()     // Catch:{ URISyntaxException -> 0x0198, IllegalStateException -> 0x01aa, IllegalArgumentException -> 0x020e, SocketException -> 0x0273, SocketTimeoutException -> 0x0270, Exception -> 0x026d, all -> 0x0268 }
            if (r6 == 0) goto L_0x0164
            java.lang.String r7 = "x-wap-profile"
            r5.addHeader(r7, r6)     // Catch:{ URISyntaxException -> 0x0198, IllegalStateException -> 0x01aa, IllegalArgumentException -> 0x020e, SocketException -> 0x0273, SocketTimeoutException -> 0x0270, Exception -> 0x026d, all -> 0x0268 }
        L_0x0164:
            java.lang.String r6 = "Accept-Language"
            java.lang.String r7 = com.sd.android.mms.transaction.h.f116a     // Catch:{ URISyntaxException -> 0x0198, IllegalStateException -> 0x01aa, IllegalArgumentException -> 0x020e, SocketException -> 0x0273, SocketTimeoutException -> 0x0270, Exception -> 0x026d, all -> 0x0268 }
            r5.addHeader(r6, r7)     // Catch:{ URISyntaxException -> 0x0198, IllegalStateException -> 0x01aa, IllegalArgumentException -> 0x020e, SocketException -> 0x0273, SocketTimeoutException -> 0x0270, Exception -> 0x026d, all -> 0x0268 }
            org.apache.http.HttpResponse r5 = r1.execute(r2, r5)     // Catch:{ URISyntaxException -> 0x0198, IllegalStateException -> 0x01aa, IllegalArgumentException -> 0x020e, SocketException -> 0x0273, SocketTimeoutException -> 0x0270, Exception -> 0x026d, all -> 0x0268 }
            org.apache.http.StatusLine r6 = r5.getStatusLine()     // Catch:{ URISyntaxException -> 0x0198, IllegalStateException -> 0x01aa, IllegalArgumentException -> 0x020e, SocketException -> 0x0273, SocketTimeoutException -> 0x0270, Exception -> 0x026d, all -> 0x0268 }
            int r7 = r6.getStatusCode()     // Catch:{ URISyntaxException -> 0x0198, IllegalStateException -> 0x01aa, IllegalArgumentException -> 0x020e, SocketException -> 0x0273, SocketTimeoutException -> 0x0270, Exception -> 0x026d, all -> 0x0268 }
            r8 = 200(0xc8, float:2.8E-43)
            if (r7 == r8) goto L_0x01b5
            java.io.IOException r5 = new java.io.IOException     // Catch:{ URISyntaxException -> 0x0198, IllegalStateException -> 0x01aa, IllegalArgumentException -> 0x020e, SocketException -> 0x0273, SocketTimeoutException -> 0x0270, Exception -> 0x026d, all -> 0x0268 }
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ URISyntaxException -> 0x0198, IllegalStateException -> 0x01aa, IllegalArgumentException -> 0x020e, SocketException -> 0x0273, SocketTimeoutException -> 0x0270, Exception -> 0x026d, all -> 0x0268 }
            r7.<init>()     // Catch:{ URISyntaxException -> 0x0198, IllegalStateException -> 0x01aa, IllegalArgumentException -> 0x020e, SocketException -> 0x0273, SocketTimeoutException -> 0x0270, Exception -> 0x026d, all -> 0x0268 }
            java.lang.String r8 = "HTTP error: "
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ URISyntaxException -> 0x0198, IllegalStateException -> 0x01aa, IllegalArgumentException -> 0x020e, SocketException -> 0x0273, SocketTimeoutException -> 0x0270, Exception -> 0x026d, all -> 0x0268 }
            java.lang.String r6 = r6.getReasonPhrase()     // Catch:{ URISyntaxException -> 0x0198, IllegalStateException -> 0x01aa, IllegalArgumentException -> 0x020e, SocketException -> 0x0273, SocketTimeoutException -> 0x0270, Exception -> 0x026d, all -> 0x0268 }
            java.lang.StringBuilder r6 = r7.append(r6)     // Catch:{ URISyntaxException -> 0x0198, IllegalStateException -> 0x01aa, IllegalArgumentException -> 0x020e, SocketException -> 0x0273, SocketTimeoutException -> 0x0270, Exception -> 0x026d, all -> 0x0268 }
            java.lang.String r6 = r6.toString()     // Catch:{ URISyntaxException -> 0x0198, IllegalStateException -> 0x01aa, IllegalArgumentException -> 0x020e, SocketException -> 0x0273, SocketTimeoutException -> 0x0270, Exception -> 0x026d, all -> 0x0268 }
            r5.<init>(r6)     // Catch:{ URISyntaxException -> 0x0198, IllegalStateException -> 0x01aa, IllegalArgumentException -> 0x020e, SocketException -> 0x0273, SocketTimeoutException -> 0x0270, Exception -> 0x026d, all -> 0x0268 }
            throw r5     // Catch:{ URISyntaxException -> 0x0198, IllegalStateException -> 0x01aa, IllegalArgumentException -> 0x020e, SocketException -> 0x0273, SocketTimeoutException -> 0x0270, Exception -> 0x026d, all -> 0x0268 }
        L_0x0198:
            r5 = move-exception
            r6 = r1
        L_0x019a:
            a(r5)     // Catch:{ all -> 0x026b }
            if (r6 == 0) goto L_0x01a2
            r6.a()
        L_0x01a2:
            r5 = 0
            goto L_0x0123
        L_0x01a4:
            org.apache.http.client.methods.HttpGet r5 = new org.apache.http.client.methods.HttpGet     // Catch:{ URISyntaxException -> 0x0198, IllegalStateException -> 0x01aa, IllegalArgumentException -> 0x020e, SocketException -> 0x0273, SocketTimeoutException -> 0x0270, Exception -> 0x026d, all -> 0x0268 }
            r5.<init>(r8)     // Catch:{ URISyntaxException -> 0x0198, IllegalStateException -> 0x01aa, IllegalArgumentException -> 0x020e, SocketException -> 0x0273, SocketTimeoutException -> 0x0270, Exception -> 0x026d, all -> 0x0268 }
            goto L_0x0141
        L_0x01aa:
            r5 = move-exception
            r6 = r1
        L_0x01ac:
            a(r5)     // Catch:{ all -> 0x026b }
            if (r6 == 0) goto L_0x01a2
            r6.a()
            goto L_0x01a2
        L_0x01b5:
            org.apache.http.HttpEntity r5 = r5.getEntity()     // Catch:{ URISyntaxException -> 0x0198, IllegalStateException -> 0x01aa, IllegalArgumentException -> 0x020e, SocketException -> 0x0273, SocketTimeoutException -> 0x0270, Exception -> 0x026d, all -> 0x0268 }
            r6 = 0
            if (r5 == 0) goto L_0x0284
            long r7 = r5.getContentLength()     // Catch:{ all -> 0x0207 }
            r9 = 0
            int r7 = (r7 > r9 ? 1 : (r7 == r9 ? 0 : -1))
            if (r7 <= 0) goto L_0x01dc
            long r6 = r5.getContentLength()     // Catch:{ all -> 0x0207 }
            int r6 = (int) r6     // Catch:{ all -> 0x0207 }
            byte[] r6 = new byte[r6]     // Catch:{ all -> 0x0207 }
            java.io.DataInputStream r7 = new java.io.DataInputStream     // Catch:{ all -> 0x0207 }
            java.io.InputStream r8 = r5.getContent()     // Catch:{ all -> 0x0207 }
            r7.<init>(r8)     // Catch:{ all -> 0x0207 }
            r7.readFully(r6)     // Catch:{ all -> 0x0219 }
            r7.close()     // Catch:{ IOException -> 0x01e9 }
        L_0x01dc:
            if (r5 == 0) goto L_0x0281
            r5.consumeContent()     // Catch:{ URISyntaxException -> 0x0198, IllegalStateException -> 0x01aa, IllegalArgumentException -> 0x020e, SocketException -> 0x0273, SocketTimeoutException -> 0x0270, Exception -> 0x026d, all -> 0x0268 }
            r5 = r6
        L_0x01e2:
            if (r1 == 0) goto L_0x0123
            r1.a()
            goto L_0x0123
        L_0x01e9:
            r7 = move-exception
            java.lang.String r8 = "JB-HttpUtils"
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ all -> 0x0207 }
            r9.<init>()     // Catch:{ all -> 0x0207 }
            java.lang.String r10 = "Error closing input stream: "
            java.lang.StringBuilder r9 = r9.append(r10)     // Catch:{ all -> 0x0207 }
            java.lang.String r7 = r7.getMessage()     // Catch:{ all -> 0x0207 }
            java.lang.StringBuilder r7 = r9.append(r7)     // Catch:{ all -> 0x0207 }
            java.lang.String r7 = r7.toString()     // Catch:{ all -> 0x0207 }
            android.util.Log.e(r8, r7)     // Catch:{ all -> 0x0207 }
            goto L_0x01dc
        L_0x0207:
            r6 = move-exception
            if (r5 == 0) goto L_0x020d
            r5.consumeContent()     // Catch:{ URISyntaxException -> 0x0198, IllegalStateException -> 0x01aa, IllegalArgumentException -> 0x020e, SocketException -> 0x0273, SocketTimeoutException -> 0x0270, Exception -> 0x026d, all -> 0x0268 }
        L_0x020d:
            throw r6     // Catch:{ URISyntaxException -> 0x0198, IllegalStateException -> 0x01aa, IllegalArgumentException -> 0x020e, SocketException -> 0x0273, SocketTimeoutException -> 0x0270, Exception -> 0x026d, all -> 0x0268 }
        L_0x020e:
            r5 = move-exception
            r6 = r1
        L_0x0210:
            a(r5)     // Catch:{ all -> 0x026b }
            if (r6 == 0) goto L_0x01a2
            r6.a()
            goto L_0x01a2
        L_0x0219:
            r6 = move-exception
            r7.close()     // Catch:{ IOException -> 0x021e }
        L_0x021d:
            throw r6     // Catch:{ all -> 0x0207 }
        L_0x021e:
            r7 = move-exception
            java.lang.String r8 = "JB-HttpUtils"
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ all -> 0x0207 }
            r9.<init>()     // Catch:{ all -> 0x0207 }
            java.lang.String r10 = "Error closing input stream: "
            java.lang.StringBuilder r9 = r9.append(r10)     // Catch:{ all -> 0x0207 }
            java.lang.String r7 = r7.getMessage()     // Catch:{ all -> 0x0207 }
            java.lang.StringBuilder r7 = r9.append(r7)     // Catch:{ all -> 0x0207 }
            java.lang.String r7 = r7.toString()     // Catch:{ all -> 0x0207 }
            android.util.Log.e(r8, r7)     // Catch:{ all -> 0x0207 }
            goto L_0x021d
        L_0x023c:
            r5 = move-exception
            r6 = r0
        L_0x023e:
            a(r5)     // Catch:{ all -> 0x026b }
            if (r6 == 0) goto L_0x01a2
            r6.a()
            goto L_0x01a2
        L_0x0248:
            r5 = move-exception
            r6 = r0
        L_0x024a:
            a(r5)     // Catch:{ all -> 0x026b }
            if (r6 == 0) goto L_0x01a2
            r6.a()
            goto L_0x01a2
        L_0x0254:
            r5 = move-exception
            r6 = r0
        L_0x0256:
            a(r5)     // Catch:{ all -> 0x026b }
            if (r6 == 0) goto L_0x01a2
            r6.a()
            goto L_0x01a2
        L_0x0260:
            r5 = move-exception
            r6 = r0
        L_0x0262:
            if (r6 == 0) goto L_0x0267
            r6.a()
        L_0x0267:
            throw r5
        L_0x0268:
            r5 = move-exception
            r6 = r1
            goto L_0x0262
        L_0x026b:
            r5 = move-exception
            goto L_0x0262
        L_0x026d:
            r5 = move-exception
            r6 = r1
            goto L_0x0256
        L_0x0270:
            r5 = move-exception
            r6 = r1
            goto L_0x024a
        L_0x0273:
            r5 = move-exception
            r6 = r1
            goto L_0x023e
        L_0x0276:
            r5 = move-exception
            r6 = r0
            goto L_0x0210
        L_0x0279:
            r5 = move-exception
            r6 = r0
            goto L_0x01ac
        L_0x027d:
            r5 = move-exception
            r6 = r0
            goto L_0x019a
        L_0x0281:
            r5 = r6
            goto L_0x01e2
        L_0x0284:
            r5 = r6
            goto L_0x01e2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.sd.android.mms.transaction.h.a(android.content.Context, long, java.lang.String, byte[], int, boolean, java.lang.String, int):byte[]");
    }
}
