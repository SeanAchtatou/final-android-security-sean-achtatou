package com.sd.android.mms.e;

import android.drm.mobile1.DrmRawContent;
import android.drm.mobile1.DrmRights;
import android.drm.mobile1.DrmRightsManager;
import android.net.Uri;
import android.util.Log;
import com.sd.a.a.a.c;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

public final class b {

    /* renamed from: a  reason: collision with root package name */
    private DrmRights f107a;
    private final DrmRawContent b;
    private final Uri c;
    private final byte[] d;
    private byte[] e;

    public b(String str, Uri uri, byte[] bArr) {
        boolean z;
        if (str == null || bArr == null) {
            throw new IllegalArgumentException("Content-Type or data may not be null.");
        }
        this.c = uri;
        this.d = bArr;
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bArr);
        this.b = new DrmRawContent(byteArrayInputStream, byteArrayInputStream.available(), str);
        if (this.f107a != null) {
            z = true;
        } else {
            this.f107a = DrmRightsManager.a().a(this.b);
            z = this.f107a != null;
        }
        if (z) {
            return;
        }
        if (bArr == null) {
            throw new android.drm.mobile1.b("Right data may not be null.");
        }
        this.f107a = DrmRightsManager.a().a(new ByteArrayInputStream(bArr), bArr.length, "application/vnd.oma.drm.message");
    }

    public final byte[] a() {
        if (this.e == null && this.f107a != null) {
            InputStream a2 = this.b.a(this.f107a);
            try {
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                byte[] bArr = new byte[256];
                while (true) {
                    int read = a2.read(bArr);
                    if (read <= 0) {
                        break;
                    }
                    byteArrayOutputStream.write(bArr, 0, read);
                }
                this.e = byteArrayOutputStream.toByteArray();
                try {
                } catch (IOException e2) {
                    Log.e("DrmWrapper", e2.getMessage(), e2);
                }
            } finally {
                try {
                    a2.close();
                } catch (IOException e3) {
                    Log.e("DrmWrapper", e3.getMessage(), e3);
                }
            }
        }
        if (this.e == null) {
            return null;
        }
        byte[] bArr2 = new byte[this.e.length];
        System.arraycopy(this.e, 0, bArr2, 0, this.e.length);
        return bArr2;
    }

    public final boolean b() {
        if (this.f107a == null) {
            return false;
        }
        DrmRights drmRights = this.f107a;
        String b2 = this.b.b();
        return drmRights.a((c.c(b2) || c.d(b2)) ? 1 : 2);
    }

    public final boolean c() {
        return 3 == this.b.a();
    }

    public final String d() {
        return this.b.b();
    }

    public final Uri e() {
        return this.c;
    }

    public final byte[] f() {
        return this.d;
    }
}
