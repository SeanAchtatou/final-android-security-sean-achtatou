package com.sd.a.a.a.a;

import android.util.Log;
import com.sd.a.a.a.a;

public final class j extends l {
    public j() {
        try {
            d(128);
            n();
            this.f48a.a("application/vnd.wap.multipart.related".getBytes(), 132);
            a(new w("insert-address-token".getBytes()));
            this.f48a.a(("T" + Long.toHexString(System.currentTimeMillis())).getBytes(), 152);
        } catch (a e) {
            Log.e("SendReq", "Unexpected InvalidHeaderValueException.", e);
            throw new RuntimeException(e);
        }
    }

    j(g gVar, e eVar) {
        super(gVar, eVar);
    }

    public final void a(int i) {
        this.f48a.a(i, 134);
    }

    public final void a(long j) {
        this.f48a.a(j, 136);
    }

    public final void a(byte[] bArr) {
        this.f48a.a(bArr, 138);
    }

    public final void a(w[] wVarArr) {
        this.f48a.a(wVarArr);
    }

    public final void b(int i) {
        this.f48a.a(i, 144);
    }

    public final byte[] b() {
        return this.f48a.b(132);
    }

    public final int c() {
        return this.f48a.a(134);
    }

    public final long d() {
        return this.f48a.e(136);
    }

    public final long e() {
        return this.f48a.e(142);
    }

    public final byte[] f() {
        return this.f48a.b(138);
    }

    public final int g() {
        return this.f48a.a(144);
    }

    public final byte[] h() {
        return this.f48a.b(152);
    }
}
