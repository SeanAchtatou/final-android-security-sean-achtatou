package com.sd.a.a.a.a;

import android.content.ContentResolver;
import android.content.Context;
import android.text.TextUtils;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.HashMap;

public class k {
    private static HashMap g;
    private static /* synthetic */ boolean h = (!k.class.desiredAssertionStatus());

    /* renamed from: a  reason: collision with root package name */
    protected ByteArrayOutputStream f45a = null;
    protected int b = 0;
    private r c = null;
    /* access modifiers changed from: private */
    public y d = null;
    private final ContentResolver e;
    private g f = null;

    static {
        g = null;
        g = new HashMap();
        for (int i = 0; i < f.f41a.length; i++) {
            g.put(f.f41a[i], Integer.valueOf(i));
        }
    }

    public k(Context context, r rVar) {
        this.c = rVar;
        this.e = context.getContentResolver();
        this.f = rVar.f48a;
        this.d = new y(this);
        this.f45a = new ByteArrayOutputStream();
        this.b = 0;
    }

    private void a(int i) {
        this.f45a.write(i);
        this.b++;
    }

    private void a(long j) {
        long j2 = j;
        int i = 0;
        while (j2 != 0 && i < 8) {
            j2 >>>= 8;
            i++;
        }
        a(i);
        int i2 = (i - 1) * 8;
        for (int i3 = 0; i3 < i; i3++) {
            a((int) ((j >>> i2) & 255));
            i2 -= 8;
        }
    }

    private void a(w wVar) {
        if (h || wVar != null) {
            int a2 = wVar.a();
            byte[] b2 = wVar.b();
            if (b2 != null) {
                this.d.a();
                h d2 = this.d.d();
                b(a2);
                a(b2);
                int a3 = d2.a();
                this.d.b();
                d((long) a3);
                this.d.c();
                return;
            }
            return;
        }
        throw new AssertionError();
    }

    private void a(byte[] bArr) {
        if ((bArr[0] & 255) > Byte.MAX_VALUE) {
            a(127);
        }
        a(bArr, bArr.length);
        a(0);
    }

    private int b() {
        int i;
        this.d.a();
        h d2 = this.d.d();
        Integer num = (Integer) g.get(new String(this.f.b(132)));
        if (num == null) {
            return 1;
        }
        b(num.intValue());
        e i2 = ((j) this.c).i();
        if (i2 == null || i2.a() == 0) {
            b(0L);
            this.d.b();
            this.d.c();
            return 0;
        }
        try {
            i a2 = i2.a(0);
            byte[] c2 = a2.c();
            if (c2 != null) {
                a(138);
                if (60 == c2[0] && 62 == c2[c2.length - 1]) {
                    a(c2);
                } else {
                    a(("<" + new String(c2) + ">").getBytes());
                }
            }
            a(137);
            a(a2.g());
        } catch (ArrayIndexOutOfBoundsException e2) {
            e2.printStackTrace();
        }
        int a3 = d2.a();
        this.d.b();
        d((long) a3);
        this.d.c();
        int a4 = i2.a();
        b((long) a4);
        for (int i3 = 0; i3 < a4; i3++) {
            i a5 = i2.a(i3);
            this.d.a();
            h d3 = this.d.d();
            this.d.a();
            h d4 = this.d.d();
            byte[] g2 = a5.g();
            if (g2 == null) {
                return 1;
            }
            Integer num2 = (Integer) g.get(new String(g2));
            if (num2 == null) {
                a(g2);
            } else {
                b(num2.intValue());
            }
            byte[] i4 = a5.i();
            if (i4 == null && (i4 = a5.j()) == null && (i4 = a5.e()) == null) {
                return 1;
            }
            a(133);
            a(i4);
            int d5 = a5.d();
            if (d5 != 0) {
                a(129);
                b(d5);
            }
            int a6 = d4.a();
            this.d.b();
            d((long) a6);
            this.d.c();
            byte[] c3 = a5.c();
            if (c3 != null) {
                a(192);
                if (60 == c3[0] && 62 == c3[c3.length - 1]) {
                    b(c3);
                } else {
                    b(("<" + new String(c3) + ">").getBytes());
                }
            }
            byte[] e3 = a5.e();
            if (e3 != null) {
                a(142);
                a(e3);
            }
            int a7 = d3.a();
            byte[] a8 = a5.a();
            if (a8 != null) {
                a(a8, a8.length);
                i = a8.length;
            } else {
                try {
                    byte[] bArr = new byte[1024];
                    InputStream openInputStream = this.e.openInputStream(a5.b());
                    int i5 = 0;
                    while (true) {
                        int read = openInputStream.read(bArr);
                        if (read == -1) {
                            break;
                        }
                        this.f45a.write(bArr, 0, read);
                        this.b += read;
                        i5 += read;
                    }
                    i = i5;
                } catch (FileNotFoundException e4) {
                    return 1;
                } catch (IOException e5) {
                    return 1;
                } catch (RuntimeException e6) {
                    return 1;
                }
            }
            if (i != d3.a() - a7) {
                throw new RuntimeException("BUG: Length sanity check failed");
            }
            this.d.b();
            b((long) a7);
            b((long) i);
            this.d.c();
        }
        return 0;
    }

    /* JADX WARNING: Removed duplicated region for block: B:21:0x0043 A[Catch:{ NullPointerException -> 0x004f }] */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x0018 A[Catch:{ NullPointerException -> 0x004f }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static com.sd.a.a.a.a.w b(com.sd.a.a.a.a.w r5) {
        /*
            r4 = 4
            r3 = 3
            r2 = 1
            java.lang.String r0 = r5.c()     // Catch:{ NullPointerException -> 0x004f }
            if (r0 == 0) goto L_0x0041
            java.lang.String r1 = "[0-9]{1,3}\\.{1}[0-9]{1,3}\\.{1}[0-9]{1,3}\\.{1}[0-9]{1,3}"
            boolean r1 = r0.matches(r1)     // Catch:{ NullPointerException -> 0x004f }
            if (r1 == 0) goto L_0x0023
            r0 = r3
        L_0x0012:
            com.sd.a.a.a.a.w r1 = com.sd.a.a.a.a.w.a(r5)     // Catch:{ NullPointerException -> 0x004f }
            if (r2 != r0) goto L_0x0043
            java.lang.String r0 = "/TYPE=PLMN"
            byte[] r0 = r0.getBytes()     // Catch:{ NullPointerException -> 0x004f }
            r1.b(r0)     // Catch:{ NullPointerException -> 0x004f }
        L_0x0021:
            r0 = r1
        L_0x0022:
            return r0
        L_0x0023:
            java.lang.String r1 = "\\+?[0-9|\\.|\\-]+"
            boolean r1 = r0.matches(r1)     // Catch:{ NullPointerException -> 0x004f }
            if (r1 == 0) goto L_0x002d
            r0 = r2
            goto L_0x0012
        L_0x002d:
            java.lang.String r1 = "[a-zA-Z| ]*\\<{0,1}[a-zA-Z| ]+@{1}[a-zA-Z| ]+\\.{1}[a-zA-Z| ]+\\>{0,1}"
            boolean r1 = r0.matches(r1)     // Catch:{ NullPointerException -> 0x004f }
            if (r1 == 0) goto L_0x0037
            r0 = 2
            goto L_0x0012
        L_0x0037:
            java.lang.String r1 = "[a-fA-F]{4}\\:{1}[a-fA-F0-9]{4}\\:{1}[a-fA-F0-9]{4}\\:{1}[a-fA-F0-9]{4}\\:{1}[a-fA-F0-9]{4}\\:{1}[a-fA-F0-9]{4}\\:{1}[a-fA-F0-9]{4}\\:{1}[a-fA-F0-9]{4}"
            boolean r0 = r0.matches(r1)     // Catch:{ NullPointerException -> 0x004f }
            if (r0 == 0) goto L_0x0041
            r0 = r4
            goto L_0x0012
        L_0x0041:
            r0 = 5
            goto L_0x0012
        L_0x0043:
            if (r3 != r0) goto L_0x0052
            java.lang.String r0 = "/TYPE=IPV4"
            byte[] r0 = r0.getBytes()     // Catch:{ NullPointerException -> 0x004f }
            r1.b(r0)     // Catch:{ NullPointerException -> 0x004f }
            goto L_0x0021
        L_0x004f:
            r0 = move-exception
            r0 = 0
            goto L_0x0022
        L_0x0052:
            if (r4 != r0) goto L_0x0021
            java.lang.String r0 = "/TYPE=IPV6"
            byte[] r0 = r0.getBytes()     // Catch:{ NullPointerException -> 0x004f }
            r1.b(r0)     // Catch:{ NullPointerException -> 0x004f }
            goto L_0x0021
        */
        throw new UnsupportedOperationException("Method not decompiled: com.sd.a.a.a.a.k.b(com.sd.a.a.a.a.w):com.sd.a.a.a.a.w");
    }

    private void b(int i) {
        a((i | 128) & 255);
    }

    private void b(long j) {
        int i = 0;
        long j2 = 127;
        while (i < 5 && j >= j2) {
            j2 = (j2 << 7) | 127;
            i++;
        }
        for (int i2 = i; i2 > 0; i2--) {
            a((int) ((((j >>> (i2 * 7)) & 127) | 128) & 255));
        }
        a((int) (j & 127));
    }

    private void b(byte[] bArr) {
        a(34);
        a(bArr, bArr.length);
        a(0);
    }

    private int c(int i) {
        switch (i) {
            case 129:
            case 130:
            case 151:
                w[] d2 = this.f.d(i);
                if (d2 != null) {
                    for (w b2 : d2) {
                        w b3 = b(b2);
                        if (b3 == null) {
                            return 1;
                        }
                        a(i);
                        a(b3);
                    }
                    break;
                } else {
                    return 2;
                }
            case 131:
            case 132:
            case 135:
            case 140:
            case 142:
            case 146:
            case 147:
            case 148:
            case 153:
            case 154:
            default:
                return 3;
            case 133:
                long e2 = this.f.e(i);
                if (-1 != e2) {
                    a(i);
                    c(e2);
                    break;
                } else {
                    return 2;
                }
            case 134:
            case 143:
            case 144:
            case 145:
            case 149:
            case 155:
                int a2 = this.f.a(i);
                if (a2 != 0) {
                    a(i);
                    a(a2);
                    break;
                } else {
                    return 2;
                }
            case 136:
                long e3 = this.f.e(i);
                if (-1 != e3) {
                    a(i);
                    this.d.a();
                    h d3 = this.d.d();
                    a(129);
                    a(e3);
                    int a3 = d3.a();
                    this.d.b();
                    d((long) a3);
                    this.d.c();
                    break;
                } else {
                    return 2;
                }
            case 137:
                a(i);
                w c2 = this.f.c(i);
                if (c2 != null && !TextUtils.isEmpty(c2.c()) && !new String(c2.b()).equals("insert-address-token")) {
                    this.d.a();
                    h d4 = this.d.d();
                    a(128);
                    w b4 = b(c2);
                    if (b4 != null) {
                        a(b4);
                        int a4 = d4.a();
                        this.d.b();
                        d((long) a4);
                        this.d.c();
                        break;
                    } else {
                        return 1;
                    }
                } else {
                    a(1);
                    a(129);
                    break;
                }
            case 138:
                byte[] b5 = this.f.b(i);
                if (b5 != null) {
                    a(i);
                    if (!Arrays.equals(b5, "advertisement".getBytes())) {
                        if (!Arrays.equals(b5, "auto".getBytes())) {
                            if (!Arrays.equals(b5, "personal".getBytes())) {
                                if (!Arrays.equals(b5, "informational".getBytes())) {
                                    a(b5);
                                    break;
                                } else {
                                    a(130);
                                    break;
                                }
                            } else {
                                a(128);
                                break;
                            }
                        } else {
                            a(131);
                            break;
                        }
                    } else {
                        a(129);
                        break;
                    }
                } else {
                    return 2;
                }
            case 139:
            case 152:
                byte[] b6 = this.f.b(i);
                if (b6 != null) {
                    a(i);
                    a(b6);
                    break;
                } else {
                    return 2;
                }
            case 141:
                a(i);
                int a5 = this.f.a(i);
                if (a5 != 0) {
                    b(a5);
                    break;
                } else {
                    b(18);
                    break;
                }
            case 150:
                w c3 = this.f.c(i);
                if (c3 != null) {
                    a(i);
                    a(c3);
                    break;
                } else {
                    return 2;
                }
        }
        return 0;
    }

    private void c(long j) {
        a(j);
    }

    private void d(long j) {
        if (j < 31) {
            a((int) j);
            return;
        }
        a(31);
        b(j);
    }

    /* access modifiers changed from: protected */
    public final void a(byte[] bArr, int i) {
        this.f45a.write(bArr, 0, i);
        this.b += i;
    }

    public final byte[] a() {
        boolean z;
        boolean z2;
        boolean z3;
        switch (this.c.m()) {
            case 128:
                if (this.f45a == null) {
                    this.f45a = new ByteArrayOutputStream();
                    this.b = 0;
                }
                a(140);
                a(128);
                a(152);
                byte[] b2 = this.f.b(152);
                if (b2 == null) {
                    throw new IllegalArgumentException("Transaction-ID is null.");
                }
                a(b2);
                if (c(141) != 0) {
                    z3 = true;
                } else {
                    c(133);
                    if (c(137) != 0) {
                        z3 = true;
                    } else {
                        boolean z4 = c(151) != 1;
                        if (c(130) != 1) {
                            z4 = true;
                        }
                        if (c(129) != 1) {
                            z4 = true;
                        }
                        if (!z4) {
                            z3 = true;
                        } else {
                            c(150);
                            c(138);
                            c(136);
                            c(143);
                            c(134);
                            c(144);
                            a(132);
                            b();
                            z3 = false;
                        }
                    }
                }
                if (z3) {
                    return null;
                }
                break;
            case 129:
            case 130:
            case 132:
            case 134:
            default:
                return null;
            case 131:
                if (this.f45a == null) {
                    this.f45a = new ByteArrayOutputStream();
                    this.b = 0;
                }
                a(140);
                a(131);
                if (c(152) != 0 ? true : c(141) != 0 ? true : c(149) != 0) {
                    return null;
                }
                break;
            case 133:
                if (this.f45a == null) {
                    this.f45a = new ByteArrayOutputStream();
                    this.b = 0;
                }
                a(140);
                a(133);
                if (c(152) != 0) {
                    z2 = true;
                } else if (c(141) != 0) {
                    z2 = true;
                } else {
                    c(145);
                    z2 = false;
                }
                if (z2) {
                    return null;
                }
                break;
            case 135:
                if (this.f45a == null) {
                    this.f45a = new ByteArrayOutputStream();
                    this.b = 0;
                }
                a(140);
                a(135);
                if (c(141) != 0) {
                    z = true;
                } else if (c(139) != 0) {
                    z = true;
                } else if (c(151) != 0) {
                    z = true;
                } else if (c(137) != 0) {
                    z = true;
                } else {
                    c(133);
                    z = c(155) != 0;
                }
                if (z) {
                    return null;
                }
                break;
        }
        return this.f45a.toByteArray();
    }
}
