package com.sd.a.a.a;

import java.util.ArrayList;

public final class c {

    /* renamed from: a  reason: collision with root package name */
    private static final ArrayList f57a = new ArrayList();
    private static final ArrayList b = new ArrayList();
    private static final ArrayList c = new ArrayList();
    private static final ArrayList d = new ArrayList();

    static {
        f57a.add("text/plain");
        f57a.add("text/html");
        f57a.add("text/x-vCalendar");
        f57a.add("text/x-vCard");
        f57a.add("image/jpeg");
        f57a.add("image/gif");
        f57a.add("image/vnd.wap.wbmp");
        f57a.add("image/png");
        f57a.add("image/jpg");
        f57a.add("audio/aac");
        f57a.add("audio/amr");
        f57a.add("audio/imelody");
        f57a.add("audio/mid");
        f57a.add("audio/midi");
        f57a.add("audio/mp3");
        f57a.add("audio/mpeg3");
        f57a.add("audio/mpeg");
        f57a.add("audio/mpg");
        f57a.add("audio/x-mid");
        f57a.add("audio/x-midi");
        f57a.add("audio/x-mp3");
        f57a.add("audio/x-mpeg3");
        f57a.add("audio/x-mpeg");
        f57a.add("audio/x-mpg");
        f57a.add("audio/3gpp");
        f57a.add("application/ogg");
        f57a.add("video/3gpp");
        f57a.add("video/3gpp2");
        f57a.add("video/h263");
        f57a.add("video/mp4");
        f57a.add("application/smil");
        f57a.add("application/vnd.wap.xhtml+xml");
        f57a.add("application/xhtml+xml");
        f57a.add("application/vnd.oma.drm.content");
        f57a.add("application/vnd.oma.drm.message");
        b.add("image/jpeg");
        b.add("image/gif");
        b.add("image/vnd.wap.wbmp");
        b.add("image/png");
        b.add("image/jpg");
        c.add("audio/aac");
        c.add("audio/amr");
        c.add("audio/imelody");
        c.add("audio/mid");
        c.add("audio/midi");
        c.add("audio/mp3");
        c.add("audio/mpeg3");
        c.add("audio/mpeg");
        c.add("audio/mpg");
        c.add("audio/mp4");
        c.add("audio/x-mid");
        c.add("audio/x-midi");
        c.add("audio/x-mp3");
        c.add("audio/x-mpeg3");
        c.add("audio/x-mpeg");
        c.add("audio/x-mpg");
        c.add("audio/3gpp");
        c.add("application/ogg");
        d.add("video/3gpp");
        d.add("video/3gpp2");
        d.add("video/h263");
        d.add("video/mp4");
    }

    private c() {
    }

    public static ArrayList a() {
        return (ArrayList) b.clone();
    }

    public static boolean a(String str) {
        return str != null && str.startsWith("text/");
    }

    public static ArrayList b() {
        return (ArrayList) c.clone();
    }

    public static boolean b(String str) {
        return str != null && str.startsWith("image/");
    }

    public static ArrayList c() {
        return (ArrayList) d.clone();
    }

    public static boolean c(String str) {
        return str != null && str.startsWith("audio/");
    }

    public static boolean d(String str) {
        return str != null && str.startsWith("video/");
    }

    public static boolean e(String str) {
        return str != null && (str.equals("application/vnd.oma.drm.content") || str.equals("application/vnd.oma.drm.message"));
    }
}
