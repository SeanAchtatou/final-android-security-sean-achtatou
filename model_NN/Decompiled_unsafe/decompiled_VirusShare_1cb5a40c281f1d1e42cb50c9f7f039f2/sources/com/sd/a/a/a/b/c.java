package com.sd.a.a.a.b;

import com.sd.a.a.a.a.r;

public final class c {

    /* renamed from: a  reason: collision with root package name */
    private final r f54a;
    private final int b;
    private final long c;

    public c(r rVar, int i, long j) {
        this.f54a = rVar;
        this.b = i;
        this.c = j;
    }

    public final r a() {
        return this.f54a;
    }

    public final int b() {
        return this.b;
    }

    public final long c() {
        return this.c;
    }
}
