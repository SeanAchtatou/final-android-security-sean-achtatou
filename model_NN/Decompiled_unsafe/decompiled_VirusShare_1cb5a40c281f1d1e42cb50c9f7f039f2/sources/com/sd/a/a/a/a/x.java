package com.sd.a.a.a.a;

public final class x {

    /* renamed from: a  reason: collision with root package name */
    private static byte[] f51a = new byte[255];

    static {
        for (int i = 0; i < 255; i++) {
            f51a[i] = -1;
        }
        for (int i2 = 90; i2 >= 65; i2--) {
            f51a[i2] = (byte) (i2 - 65);
        }
        for (int i3 = 122; i3 >= 97; i3--) {
            f51a[i3] = (byte) ((i3 - 97) + 26);
        }
        for (int i4 = 57; i4 >= 48; i4--) {
            f51a[i4] = (byte) ((i4 - 48) + 52);
        }
        f51a[43] = 62;
        f51a[47] = 63;
    }

    public static byte[] a(byte[] bArr) {
        byte[] bArr2 = new byte[bArr.length];
        int i = 0;
        for (int i2 = 0; i2 < bArr.length; i2++) {
            byte b = bArr[i2];
            if (b == 61 || f51a[b] != -1) {
                bArr2[i] = bArr[i2];
                i++;
            }
        }
        byte[] bArr3 = new byte[i];
        System.arraycopy(bArr2, 0, bArr3, 0, i);
        if (bArr3.length == 0) {
            return new byte[0];
        }
        int length = bArr3.length / 4;
        int length2 = bArr3.length;
        while (bArr3[length2 - 1] == 61) {
            length2--;
            if (length2 == 0) {
                return new byte[0];
            }
        }
        byte[] bArr4 = new byte[(length2 - length)];
        int i3 = 0;
        for (int i4 = 0; i4 < length; i4++) {
            int i5 = i4 * 4;
            byte b2 = bArr3[i5 + 2];
            byte b3 = bArr3[i5 + 3];
            byte b4 = f51a[bArr3[i5]];
            byte b5 = f51a[bArr3[i5 + 1]];
            if (b2 != 61 && b3 != 61) {
                byte b6 = f51a[b2];
                byte b7 = f51a[b3];
                bArr4[i3] = (byte) ((b4 << 2) | (b5 >> 4));
                bArr4[i3 + 1] = (byte) (((b5 & 15) << 4) | ((b6 >> 2) & 15));
                bArr4[i3 + 2] = (byte) ((b6 << 6) | b7);
            } else if (b2 == 61) {
                bArr4[i3] = (byte) ((b5 >> 4) | (b4 << 2));
            } else if (b3 == 61) {
                byte b8 = f51a[b2];
                bArr4[i3] = (byte) ((b4 << 2) | (b5 >> 4));
                bArr4[i3 + 1] = (byte) (((b5 & 15) << 4) | ((b8 >> 2) & 15));
            }
            i3 += 3;
        }
        return bArr4;
    }
}
