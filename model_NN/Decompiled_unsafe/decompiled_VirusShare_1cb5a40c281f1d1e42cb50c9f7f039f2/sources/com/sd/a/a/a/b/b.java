package com.sd.a.a.a.b;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.net.Uri;
import android.util.Log;
import android.widget.Toast;

public final class b {
    private b() {
    }

    public static int a(Context context, ContentResolver contentResolver, Uri uri, ContentValues contentValues, String str) {
        try {
            return contentResolver.update(uri, contentValues, str, null);
        } catch (SQLiteException e) {
            Log.e("SqliteWrapper", "Catch a SQLiteException when update: ", e);
            a(context, e);
            return -1;
        }
    }

    public static int a(Context context, ContentResolver contentResolver, Uri uri, String str) {
        try {
            return contentResolver.delete(uri, str, null);
        } catch (SQLiteException e) {
            Log.e("SqliteWrapper", "Catch a SQLiteException when delete: ", e);
            a(context, e);
            return -1;
        }
    }

    public static Cursor a(Context context, ContentResolver contentResolver, Uri uri, String[] strArr, String str, String[] strArr2, String str2) {
        try {
            return contentResolver.query(uri, strArr, str, strArr2, str2);
        } catch (SQLiteException e) {
            Log.e("SqliteWrapper", "Catch a SQLiteException when query: ", e);
            a(context, e);
            return null;
        }
    }

    public static Uri a(Context context, ContentResolver contentResolver, Uri uri, ContentValues contentValues) {
        try {
            return contentResolver.insert(uri, contentValues);
        } catch (SQLiteException e) {
            Log.e("SqliteWrapper", "Catch a SQLiteException when insert: ", e);
            a(context, e);
            return null;
        }
    }

    public static void a(Context context, SQLiteException sQLiteException) {
        if (sQLiteException.getMessage().equals("unable to open database file")) {
            Toast.makeText(context, "内存不足", 0).show();
            return;
        }
        throw sQLiteException;
    }
}
