package com.sd.a.a.a.a;

import android.util.Log;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

public final class w implements Cloneable {

    /* renamed from: a  reason: collision with root package name */
    private int f50a;
    private byte[] b;

    public w(int i, byte[] bArr) {
        if (bArr == null) {
            throw new NullPointerException("EncodedStringValue: Text-string is null.");
        }
        this.f50a = i;
        this.b = new byte[bArr.length];
        System.arraycopy(bArr, 0, this.b, 0, bArr.length);
    }

    public w(String str) {
        try {
            this.b = str.getBytes("utf-8");
            this.f50a = 106;
        } catch (UnsupportedEncodingException e) {
            Log.e("EncodedStringValue", "Default encoding must be supported.", e);
        }
    }

    public w(byte[] bArr) {
        this(106, bArr);
    }

    public static w a(w wVar) {
        if (wVar == null) {
            return null;
        }
        return new w(wVar.f50a, wVar.b);
    }

    public final int a() {
        return this.f50a;
    }

    public final void a(byte[] bArr) {
        if (bArr == null) {
            throw new NullPointerException("EncodedStringValue: Text-string is null.");
        }
        this.b = new byte[bArr.length];
        System.arraycopy(bArr, 0, this.b, 0, bArr.length);
    }

    public final void b(byte[] bArr) {
        if (bArr == null) {
            throw new NullPointerException("Text-string is null.");
        } else if (this.b == null) {
            this.b = new byte[bArr.length];
            System.arraycopy(bArr, 0, this.b, 0, bArr.length);
        } else {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            try {
                byteArrayOutputStream.write(this.b);
                byteArrayOutputStream.write(bArr);
                this.b = byteArrayOutputStream.toByteArray();
            } catch (IOException e) {
                e.printStackTrace();
                throw new NullPointerException("appendTextString: failed when write a new Text-string");
            }
        }
    }

    public final byte[] b() {
        byte[] bArr = new byte[this.b.length];
        System.arraycopy(this.b, 0, bArr, 0, this.b.length);
        return bArr;
    }

    public final String c() {
        if (this.f50a == 0) {
            return new String(this.b);
        }
        try {
            return new String(this.b, b.a(this.f50a));
        } catch (UnsupportedEncodingException e) {
            try {
                return new String(this.b, "iso-8859-1");
            } catch (UnsupportedEncodingException e2) {
                return new String(this.b);
            }
        }
    }

    public final Object clone() {
        super.clone();
        int length = this.b.length;
        byte[] bArr = new byte[length];
        System.arraycopy(this.b, 0, bArr, 0, length);
        try {
            return new w(this.f50a, bArr);
        } catch (Exception e) {
            Log.e("EncodedStringValue", "failed to clone an EncodedStringValue: " + this);
            e.printStackTrace();
            throw new CloneNotSupportedException(e.getMessage());
        }
    }
}
