package com.sd.a.a.a.b;

import java.util.HashMap;

public abstract class a {

    /* renamed from: a  reason: collision with root package name */
    private final HashMap f53a = new HashMap();

    protected a() {
    }

    public final Object a(Object obj) {
        e eVar;
        if (obj == null || (eVar = (e) this.f53a.get(obj)) == null) {
            return null;
        }
        eVar.f56a++;
        return eVar.b;
    }

    public void a() {
        this.f53a.clear();
    }

    public boolean a(Object obj, Object obj2) {
        if (this.f53a.size() >= 500) {
            return false;
        }
        if (obj == null) {
            return false;
        }
        e eVar = new e();
        eVar.b = obj2;
        this.f53a.put(obj, eVar);
        return true;
    }

    public Object b(Object obj) {
        e eVar = (e) this.f53a.remove(obj);
        if (eVar != null) {
            return eVar.b;
        }
        return null;
    }
}
