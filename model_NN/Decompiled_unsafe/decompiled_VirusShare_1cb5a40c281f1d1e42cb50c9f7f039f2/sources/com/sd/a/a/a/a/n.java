package com.sd.a.a.a.a;

import android.util.Log;
import com.sd.a.a.a.a;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.HashMap;

public class n {
    private static byte[] d = null;
    private static byte[] e = null;
    private static /* synthetic */ boolean f = (!n.class.desiredAssertionStatus());

    /* renamed from: a  reason: collision with root package name */
    private ByteArrayInputStream f46a = null;
    private g b = null;
    private e c = null;

    public n(byte[] bArr) {
        this.f46a = new ByteArrayInputStream(bArr);
    }

    private static g a(ByteArrayInputStream byteArrayInputStream) {
        w wVar;
        byte[] b2;
        if (byteArrayInputStream == null) {
            return null;
        }
        g gVar = new g();
        boolean z = true;
        while (z && byteArrayInputStream.available() > 0) {
            int e2 = e(byteArrayInputStream);
            switch (e2) {
                case 129:
                case 130:
                case 151:
                    w d2 = d(byteArrayInputStream);
                    if (d2 == null) {
                        break;
                    } else {
                        byte[] b3 = d2.b();
                        if (b3 != null) {
                            String str = new String(b3);
                            int indexOf = str.indexOf("/");
                            try {
                                d2.a((indexOf > 0 ? str.substring(0, indexOf) : str).getBytes());
                            } catch (NullPointerException e3) {
                                return null;
                            }
                        }
                        try {
                            gVar.b(d2, e2);
                            break;
                        } catch (NullPointerException e4) {
                            break;
                        } catch (RuntimeException e5) {
                            e2 + "is not Encoded-String-Value header field!";
                            return null;
                        }
                    }
                case 131:
                case 139:
                case 152:
                case 158:
                case 183:
                case 184:
                case 185:
                case 189:
                case 190:
                    byte[] a2 = a(byteArrayInputStream, 0);
                    if (a2 == null) {
                        break;
                    } else {
                        try {
                            gVar.a(a2, e2);
                            break;
                        } catch (NullPointerException e6) {
                            break;
                        } catch (RuntimeException e7) {
                            e2 + "is not Text-String header field!";
                            return null;
                        }
                    }
                case 132:
                    HashMap hashMap = new HashMap();
                    byte[] a3 = a(byteArrayInputStream, hashMap);
                    if (a3 != null) {
                        try {
                            gVar.a(a3, 132);
                        } catch (NullPointerException e8) {
                        } catch (RuntimeException e9) {
                            e2 + "is not Text-String header field!";
                            return null;
                        }
                    }
                    e = (byte[]) hashMap.get(153);
                    d = (byte[]) hashMap.get(131);
                    z = false;
                    break;
                case 133:
                case 142:
                case 159:
                    try {
                        gVar.a(g(byteArrayInputStream), e2);
                        break;
                    } catch (RuntimeException e10) {
                        e2 + "is not Long-Integer header field!";
                        return null;
                    }
                case 134:
                case 143:
                case 144:
                case 145:
                case 146:
                case 148:
                case 149:
                case 153:
                case 155:
                case 156:
                case 162:
                case 163:
                case 165:
                case 167:
                case 169:
                case 171:
                case 177:
                case 180:
                case 186:
                case 187:
                case 188:
                case 191:
                    int e11 = e(byteArrayInputStream);
                    try {
                        gVar.a(e11, e2);
                        break;
                    } catch (a e12) {
                        "Set invalid Octet value: " + e11 + " into the header filed: " + e2;
                        return null;
                    } catch (RuntimeException e13) {
                        e2 + "is not Octet header field!";
                        return null;
                    }
                case 135:
                case 136:
                case 157:
                    c(byteArrayInputStream);
                    int e14 = e(byteArrayInputStream);
                    try {
                        long g = g(byteArrayInputStream);
                        try {
                            gVar.a(129 == e14 ? (System.currentTimeMillis() / 1000) + g : g, e2);
                            break;
                        } catch (RuntimeException e15) {
                            e2 + "is not Long-Integer header field!";
                            return null;
                        }
                    } catch (RuntimeException e16) {
                        e2 + "is not Long-Integer header field!";
                        return null;
                    }
                case 137:
                    c(byteArrayInputStream);
                    if (128 == e(byteArrayInputStream)) {
                        wVar = d(byteArrayInputStream);
                        if (!(wVar == null || (b2 = wVar.b()) == null)) {
                            String str2 = new String(b2);
                            int indexOf2 = str2.indexOf("/");
                            try {
                                wVar.a((indexOf2 > 0 ? str2.substring(0, indexOf2) : str2).getBytes());
                            } catch (NullPointerException e17) {
                                return null;
                            }
                        }
                    } else {
                        try {
                            wVar = new w("insert-address-token".getBytes());
                        } catch (NullPointerException e18) {
                            e2 + "is not Encoded-String-Value header field!";
                            return null;
                        }
                    }
                    try {
                        gVar.a(wVar, 137);
                        break;
                    } catch (NullPointerException e19) {
                        break;
                    } catch (RuntimeException e20) {
                        e2 + "is not Encoded-String-Value header field!";
                        return null;
                    }
                case 138:
                    byteArrayInputStream.mark(1);
                    int e21 = e(byteArrayInputStream);
                    if (e21 >= 128) {
                        if (128 != e21) {
                            if (129 != e21) {
                                if (130 != e21) {
                                    if (131 != e21) {
                                        break;
                                    } else {
                                        gVar.a("auto".getBytes(), 138);
                                        break;
                                    }
                                } else {
                                    gVar.a("informational".getBytes(), 138);
                                    break;
                                }
                            } else {
                                gVar.a("advertisement".getBytes(), 138);
                                break;
                            }
                        } else {
                            try {
                                gVar.a("personal".getBytes(), 138);
                                break;
                            } catch (NullPointerException e22) {
                                break;
                            } catch (RuntimeException e23) {
                                e2 + "is not Text-String header field!";
                                return null;
                            }
                        }
                    } else {
                        byteArrayInputStream.reset();
                        byte[] a4 = a(byteArrayInputStream, 0);
                        if (a4 == null) {
                            break;
                        } else {
                            try {
                                gVar.a(a4, 138);
                                break;
                            } catch (NullPointerException e24) {
                                break;
                            } catch (RuntimeException e25) {
                                e2 + "is not Text-String header field!";
                                return null;
                            }
                        }
                    }
                case 140:
                    int e26 = e(byteArrayInputStream);
                    switch (e26) {
                        case 137:
                        case 138:
                        case 139:
                        case 140:
                        case 141:
                        case 142:
                        case 143:
                        case 144:
                        case 145:
                        case 146:
                        case 147:
                        case 148:
                        case 149:
                        case 150:
                        case 151:
                            return null;
                        default:
                            try {
                                gVar.a(e26, e2);
                                continue;
                            } catch (a e27) {
                                "Set invalid Octet value: " + e26 + " into the header filed: " + e2;
                                return null;
                            } catch (RuntimeException e28) {
                                e2 + "is not Octet header field!";
                                return null;
                            }
                    }
                case 141:
                    int f2 = f(byteArrayInputStream);
                    try {
                        gVar.a(f2, 141);
                        break;
                    } catch (a e29) {
                        "Set invalid Octet value: " + f2 + " into the header filed: " + e2;
                        return null;
                    } catch (RuntimeException e30) {
                        e2 + "is not Octet header field!";
                        return null;
                    }
                case 147:
                case 150:
                case 154:
                case 166:
                case 181:
                case 182:
                    w d3 = d(byteArrayInputStream);
                    if (d3 == null) {
                        break;
                    } else {
                        try {
                            gVar.a(d3, e2);
                            break;
                        } catch (NullPointerException e31) {
                            break;
                        } catch (RuntimeException e32) {
                            e2 + "is not Encoded-String-Value header field!";
                            return null;
                        }
                    }
                case 160:
                    c(byteArrayInputStream);
                    try {
                        h(byteArrayInputStream);
                        w d4 = d(byteArrayInputStream);
                        if (d4 == null) {
                            break;
                        } else {
                            try {
                                gVar.a(d4, 160);
                                break;
                            } catch (NullPointerException e33) {
                                break;
                            } catch (RuntimeException e34) {
                                e2 + "is not Encoded-String-Value header field!";
                                return null;
                            }
                        }
                    } catch (RuntimeException e35) {
                        e2 + " is not Integer-Value";
                        return null;
                    }
                case 161:
                    c(byteArrayInputStream);
                    try {
                        h(byteArrayInputStream);
                        try {
                            gVar.a(g(byteArrayInputStream), 161);
                            break;
                        } catch (RuntimeException e36) {
                            e2 + "is not Long-Integer header field!";
                            return null;
                        }
                    } catch (RuntimeException e37) {
                        e2 + " is not Integer-Value";
                        return null;
                    }
                case 164:
                    c(byteArrayInputStream);
                    e(byteArrayInputStream);
                    d(byteArrayInputStream);
                    break;
                case 170:
                case 172:
                    c(byteArrayInputStream);
                    e(byteArrayInputStream);
                    try {
                        h(byteArrayInputStream);
                        break;
                    } catch (RuntimeException e38) {
                        e2 + " is not Integer-Value";
                        return null;
                    }
                case 173:
                case 175:
                case 179:
                    try {
                        gVar.a(h(byteArrayInputStream), e2);
                        break;
                    } catch (RuntimeException e39) {
                        e2 + "is not Long-Integer header field!";
                        return null;
                    }
                case 178:
                    a(byteArrayInputStream, (HashMap) null);
                    break;
            }
        }
        return gVar;
    }

    private static void a(ByteArrayInputStream byteArrayInputStream, HashMap hashMap, Integer num) {
        if (!f && byteArrayInputStream == null) {
            throw new AssertionError();
        } else if (f || num.intValue() > 0) {
            int available = byteArrayInputStream.available();
            int intValue = num.intValue();
            while (intValue > 0) {
                int read = byteArrayInputStream.read();
                if (f || -1 != read) {
                    intValue--;
                    switch (read) {
                        case 129:
                            byteArrayInputStream.mark(1);
                            int e2 = e(byteArrayInputStream);
                            byteArrayInputStream.reset();
                            if ((e2 <= 32 || e2 >= 127) && e2 != 0) {
                                int h = (int) h(byteArrayInputStream);
                                if (hashMap != null) {
                                    hashMap.put(129, Integer.valueOf(h));
                                }
                            } else {
                                byte[] a2 = a(byteArrayInputStream, 0);
                                try {
                                    hashMap.put(129, Integer.valueOf(b.a(new String(a2))));
                                } catch (UnsupportedEncodingException e3) {
                                    Log.e("PduParser", Arrays.toString(a2), e3);
                                    hashMap.put(129, 0);
                                }
                            }
                            intValue = num.intValue() - (available - byteArrayInputStream.available());
                            break;
                        case 131:
                        case 137:
                            byteArrayInputStream.mark(1);
                            int e4 = e(byteArrayInputStream);
                            byteArrayInputStream.reset();
                            if (e4 > 127) {
                                int f2 = f(byteArrayInputStream);
                                if (f2 < f.f41a.length) {
                                    hashMap.put(131, f.f41a[f2].getBytes());
                                }
                            } else {
                                byte[] a3 = a(byteArrayInputStream, 0);
                                if (!(a3 == null || hashMap == null)) {
                                    hashMap.put(131, a3);
                                }
                            }
                            intValue = num.intValue() - (available - byteArrayInputStream.available());
                            break;
                        case 133:
                        case 151:
                            byte[] a4 = a(byteArrayInputStream, 0);
                            if (!(a4 == null || hashMap == null)) {
                                hashMap.put(151, a4);
                            }
                            intValue = num.intValue() - (available - byteArrayInputStream.available());
                            break;
                        case 138:
                        case 153:
                            byte[] a5 = a(byteArrayInputStream, 0);
                            if (!(a5 == null || hashMap == null)) {
                                hashMap.put(153, a5);
                            }
                            intValue = num.intValue() - (available - byteArrayInputStream.available());
                            break;
                        default:
                            if (-1 != c(byteArrayInputStream, intValue)) {
                                intValue = 0;
                                break;
                            } else {
                                Log.e("PduParser", "Corrupt Content-Type");
                                break;
                            }
                    }
                } else {
                    throw new AssertionError();
                }
            }
            if (intValue != 0) {
                Log.e("PduParser", "Corrupt Content-Type");
            }
        } else {
            throw new AssertionError();
        }
    }

    private static boolean a(ByteArrayInputStream byteArrayInputStream, i iVar, int i) {
        if (!f && byteArrayInputStream == null) {
            throw new AssertionError();
        } else if (!f && iVar == null) {
            throw new AssertionError();
        } else if (f || i > 0) {
            int available = byteArrayInputStream.available();
            int i2 = i;
            while (i2 > 0) {
                int read = byteArrayInputStream.read();
                if (f || -1 != read) {
                    int i3 = i2 - 1;
                    if (read > 127) {
                        switch (read) {
                            case 142:
                                byte[] a2 = a(byteArrayInputStream, 0);
                                if (a2 != null) {
                                    iVar.c(a2);
                                }
                                i2 = i - (available - byteArrayInputStream.available());
                                continue;
                            case 174:
                            case 197:
                                int c2 = c(byteArrayInputStream);
                                byteArrayInputStream.mark(1);
                                int available2 = byteArrayInputStream.available();
                                int read2 = byteArrayInputStream.read();
                                if (read2 == 128) {
                                    iVar.d(i.f44a);
                                } else if (read2 == 129) {
                                    iVar.d(i.b);
                                } else if (read2 == 130) {
                                    iVar.d(i.c);
                                } else {
                                    byteArrayInputStream.reset();
                                    iVar.d(a(byteArrayInputStream, 0));
                                }
                                if (available2 - byteArrayInputStream.available() < c2) {
                                    if (byteArrayInputStream.read() == 152) {
                                        iVar.h(a(byteArrayInputStream, 0));
                                    }
                                    int available3 = byteArrayInputStream.available();
                                    if (available2 - available3 < c2) {
                                        int i4 = c2 - (available2 - available3);
                                        byteArrayInputStream.read(new byte[i4], 0, i4);
                                    }
                                }
                                i2 = i - (available - byteArrayInputStream.available());
                                continue;
                            case 192:
                                byte[] a3 = a(byteArrayInputStream, 1);
                                if (a3 != null) {
                                    iVar.b(a3);
                                }
                                i2 = i - (available - byteArrayInputStream.available());
                                continue;
                            default:
                                if (-1 == c(byteArrayInputStream, i3)) {
                                    Log.e("PduParser", "Corrupt Part headers");
                                    return false;
                                }
                                i2 = 0;
                                continue;
                        }
                    } else if (read >= 32 && read <= 127) {
                        byte[] a4 = a(byteArrayInputStream, 0);
                        byte[] a5 = a(byteArrayInputStream, 0);
                        if (true == "Content-Transfer-Encoding".equalsIgnoreCase(new String(a4))) {
                            iVar.f(a5);
                        }
                        i2 = i - (available - byteArrayInputStream.available());
                    } else if (-1 == c(byteArrayInputStream, i3)) {
                        Log.e("PduParser", "Corrupt Part headers");
                        return false;
                    } else {
                        i2 = 0;
                    }
                } else {
                    throw new AssertionError();
                }
            }
            if (i2 == 0) {
                return true;
            }
            Log.e("PduParser", "Corrupt Part headers");
            return false;
        } else {
            throw new AssertionError();
        }
    }

    private static byte[] a(ByteArrayInputStream byteArrayInputStream, int i) {
        if (f || byteArrayInputStream != null) {
            byteArrayInputStream.mark(1);
            int read = byteArrayInputStream.read();
            if (f || -1 != read) {
                if (1 == i && 34 == read) {
                    byteArrayInputStream.mark(1);
                } else if (i == 0 && 127 == read) {
                    byteArrayInputStream.mark(1);
                } else {
                    byteArrayInputStream.reset();
                }
                return b(byteArrayInputStream, i);
            }
            throw new AssertionError();
        }
        throw new AssertionError();
    }

    private static byte[] a(ByteArrayInputStream byteArrayInputStream, HashMap hashMap) {
        byte[] a2;
        if (f || byteArrayInputStream != null) {
            byteArrayInputStream.mark(1);
            int read = byteArrayInputStream.read();
            if (f || -1 != read) {
                byteArrayInputStream.reset();
                int i = read & 255;
                if (i >= 32) {
                    return i <= 127 ? a(byteArrayInputStream, 0) : f.f41a[f(byteArrayInputStream)].getBytes();
                }
                int c2 = c(byteArrayInputStream);
                int available = byteArrayInputStream.available();
                byteArrayInputStream.mark(1);
                int read2 = byteArrayInputStream.read();
                if (f || -1 != read2) {
                    byteArrayInputStream.reset();
                    int i2 = read2 & 255;
                    if (i2 >= 32 && i2 <= 127) {
                        a2 = a(byteArrayInputStream, 0);
                    } else if (i2 > 127) {
                        int f2 = f(byteArrayInputStream);
                        if (f2 < f.f41a.length) {
                            a2 = f.f41a[f2].getBytes();
                        } else {
                            byteArrayInputStream.reset();
                            a2 = a(byteArrayInputStream, 0);
                        }
                    } else {
                        Log.e("PduParser", "Corrupt content-type");
                        return f.f41a[0].getBytes();
                    }
                    int available2 = c2 - (available - byteArrayInputStream.available());
                    if (available2 > 0) {
                        a(byteArrayInputStream, hashMap, Integer.valueOf(available2));
                    }
                    if (available2 >= 0) {
                        return a2;
                    }
                    Log.e("PduParser", "Corrupt MMS message");
                    return f.f41a[0].getBytes();
                }
                throw new AssertionError();
            }
            throw new AssertionError();
        }
        throw new AssertionError();
    }

    private static int b(ByteArrayInputStream byteArrayInputStream) {
        if (f || byteArrayInputStream != null) {
            int i = 0;
            int read = byteArrayInputStream.read();
            if (read == -1) {
                return read;
            }
            do {
                int i2 = read;
                int i3 = i;
                int i4 = i2;
                if ((i4 & 128) != 0) {
                    i = (i4 & 127) | (i3 << 7);
                    read = byteArrayInputStream.read();
                } else {
                    return (i4 & 127) | (i3 << 7);
                }
            } while (read != -1);
            return read;
        }
        throw new AssertionError();
    }

    private static byte[] b(ByteArrayInputStream byteArrayInputStream, int i) {
        boolean z;
        boolean z2;
        if (f || byteArrayInputStream != null) {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            int read = byteArrayInputStream.read();
            if (f || -1 != read) {
                while (-1 != read && read != 0) {
                    if (i == 2) {
                        if (read >= 33 && read <= 126) {
                            switch (read) {
                                case 34:
                                case 40:
                                case 41:
                                case 44:
                                case 47:
                                case 58:
                                case 59:
                                case 60:
                                case 61:
                                case 62:
                                case 63:
                                case 64:
                                case 91:
                                case 92:
                                case 93:
                                case 123:
                                case 125:
                                    z2 = false;
                                    break;
                                default:
                                    z2 = true;
                                    break;
                            }
                        } else {
                            z2 = false;
                        }
                        if (z2) {
                            byteArrayOutputStream.write(read);
                        }
                    } else {
                        if ((read < 32 || read > 126) && (read < 128 || read > 255)) {
                            switch (read) {
                                case 9:
                                case 10:
                                case 13:
                                    z = true;
                                    break;
                                case 11:
                                case 12:
                                default:
                                    z = false;
                                    break;
                            }
                        } else {
                            z = true;
                        }
                        if (z) {
                            byteArrayOutputStream.write(read);
                        }
                    }
                    read = byteArrayInputStream.read();
                    if (!f && -1 == read) {
                        throw new AssertionError();
                    }
                }
                if (byteArrayOutputStream.size() > 0) {
                    return byteArrayOutputStream.toByteArray();
                }
                return null;
            }
            throw new AssertionError();
        }
        throw new AssertionError();
    }

    private static int c(ByteArrayInputStream byteArrayInputStream) {
        if (f || byteArrayInputStream != null) {
            int read = byteArrayInputStream.read();
            if (f || -1 != read) {
                int i = read & 255;
                if (i <= 30) {
                    return i;
                }
                if (i == 31) {
                    return b(byteArrayInputStream);
                }
                throw new RuntimeException("Value length > LENGTH_QUOTE!");
            }
            throw new AssertionError();
        }
        throw new AssertionError();
    }

    private static int c(ByteArrayInputStream byteArrayInputStream, int i) {
        if (f || byteArrayInputStream != null) {
            int read = byteArrayInputStream.read(new byte[i], 0, i);
            if (read < i) {
                return -1;
            }
            return read;
        }
        throw new AssertionError();
    }

    private static w d(ByteArrayInputStream byteArrayInputStream) {
        int i;
        if (f || byteArrayInputStream != null) {
            byteArrayInputStream.mark(1);
            int read = byteArrayInputStream.read();
            if (f || -1 != read) {
                int i2 = read & 255;
                byteArrayInputStream.reset();
                if (i2 < 32) {
                    c(byteArrayInputStream);
                    i = f(byteArrayInputStream);
                } else {
                    i = 0;
                }
                byte[] a2 = a(byteArrayInputStream, 0);
                if (i == 0) {
                    return new w(a2);
                }
                try {
                    return new w(i, a2);
                } catch (Exception e2) {
                    return null;
                }
            } else {
                throw new AssertionError();
            }
        } else {
            throw new AssertionError();
        }
    }

    private static int e(ByteArrayInputStream byteArrayInputStream) {
        if (f || byteArrayInputStream != null) {
            int read = byteArrayInputStream.read();
            if (f || -1 != read) {
                return read & 255;
            }
            throw new AssertionError();
        }
        throw new AssertionError();
    }

    private static int f(ByteArrayInputStream byteArrayInputStream) {
        if (f || byteArrayInputStream != null) {
            int read = byteArrayInputStream.read();
            if (f || -1 != read) {
                return read & 127;
            }
            throw new AssertionError();
        }
        throw new AssertionError();
    }

    private static long g(ByteArrayInputStream byteArrayInputStream) {
        if (f || byteArrayInputStream != null) {
            int read = byteArrayInputStream.read();
            if (f || -1 != read) {
                int i = read & 255;
                if (i > 8) {
                    throw new RuntimeException("Octet count greater than 8 and I can't represent that!");
                }
                long j = 0;
                int i2 = 0;
                while (i2 < i) {
                    int read2 = byteArrayInputStream.read();
                    if (f || -1 != read2) {
                        j = (j << 8) + ((long) (read2 & 255));
                        i2++;
                    } else {
                        throw new AssertionError();
                    }
                }
                return j;
            }
            throw new AssertionError();
        }
        throw new AssertionError();
    }

    private static long h(ByteArrayInputStream byteArrayInputStream) {
        if (f || byteArrayInputStream != null) {
            byteArrayInputStream.mark(1);
            int read = byteArrayInputStream.read();
            if (f || -1 != read) {
                byteArrayInputStream.reset();
                return read > 127 ? (long) f(byteArrayInputStream) : g(byteArrayInputStream);
            }
            throw new AssertionError();
        }
        throw new AssertionError();
    }

    /* JADX WARNING: Removed duplicated region for block: B:166:0x0272  */
    /* JADX WARNING: Removed duplicated region for block: B:176:0x0290  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.sd.a.a.a.a.r a() {
        /*
            r14 = this;
            r13 = 1
            r5 = -1
            r4 = 152(0x98, float:2.13E-43)
            r12 = 0
            r11 = 0
            java.io.ByteArrayInputStream r0 = r14.f46a
            if (r0 != 0) goto L_0x000d
            r0 = r12
        L_0x000c:
            return r0
        L_0x000d:
            java.io.ByteArrayInputStream r0 = r14.f46a
            com.sd.a.a.a.a.g r0 = a(r0)
            r14.b = r0
            com.sd.a.a.a.a.g r0 = r14.b
            if (r0 != 0) goto L_0x001b
            r0 = r12
            goto L_0x000c
        L_0x001b:
            com.sd.a.a.a.a.g r0 = r14.b
            r1 = 140(0x8c, float:1.96E-43)
            int r1 = r0.a(r1)
            com.sd.a.a.a.a.g r0 = r14.b
            if (r0 != 0) goto L_0x002c
            r0 = r11
        L_0x0028:
            if (r0 != 0) goto L_0x016e
            r0 = r12
            goto L_0x000c
        L_0x002c:
            r2 = 140(0x8c, float:1.96E-43)
            int r2 = r0.a(r2)
            r3 = 141(0x8d, float:1.98E-43)
            int r3 = r0.a(r3)
            if (r3 != 0) goto L_0x003c
            r0 = r11
            goto L_0x0028
        L_0x003c:
            switch(r2) {
                case 128: goto L_0x0041;
                case 129: goto L_0x005d;
                case 130: goto L_0x006f;
                case 131: goto L_0x00a3;
                case 132: goto L_0x00b7;
                case 133: goto L_0x00fd;
                case 134: goto L_0x00cf;
                case 135: goto L_0x013f;
                case 136: goto L_0x0106;
                default: goto L_0x003f;
            }
        L_0x003f:
            r0 = r11
            goto L_0x0028
        L_0x0041:
            r2 = 132(0x84, float:1.85E-43)
            byte[] r2 = r0.b(r2)
            if (r2 != 0) goto L_0x004b
            r0 = r11
            goto L_0x0028
        L_0x004b:
            r2 = 137(0x89, float:1.92E-43)
            com.sd.a.a.a.a.w r2 = r0.c(r2)
            if (r2 != 0) goto L_0x0055
            r0 = r11
            goto L_0x0028
        L_0x0055:
            byte[] r0 = r0.b(r4)
            if (r0 != 0) goto L_0x016b
            r0 = r11
            goto L_0x0028
        L_0x005d:
            r2 = 146(0x92, float:2.05E-43)
            int r2 = r0.a(r2)
            if (r2 != 0) goto L_0x0067
            r0 = r11
            goto L_0x0028
        L_0x0067:
            byte[] r0 = r0.b(r4)
            if (r0 != 0) goto L_0x016b
            r0 = r11
            goto L_0x0028
        L_0x006f:
            r2 = 131(0x83, float:1.84E-43)
            byte[] r2 = r0.b(r2)
            if (r2 != 0) goto L_0x0079
            r0 = r11
            goto L_0x0028
        L_0x0079:
            r2 = 136(0x88, float:1.9E-43)
            long r2 = r0.e(r2)
            int r2 = (r5 > r2 ? 1 : (r5 == r2 ? 0 : -1))
            if (r2 != 0) goto L_0x0085
            r0 = r11
            goto L_0x0028
        L_0x0085:
            r2 = 138(0x8a, float:1.93E-43)
            byte[] r2 = r0.b(r2)
            if (r2 != 0) goto L_0x008f
            r0 = r11
            goto L_0x0028
        L_0x008f:
            r2 = 142(0x8e, float:1.99E-43)
            long r2 = r0.e(r2)
            int r2 = (r5 > r2 ? 1 : (r5 == r2 ? 0 : -1))
            if (r2 != 0) goto L_0x009b
            r0 = r11
            goto L_0x0028
        L_0x009b:
            byte[] r0 = r0.b(r4)
            if (r0 != 0) goto L_0x016b
            r0 = r11
            goto L_0x0028
        L_0x00a3:
            r2 = 149(0x95, float:2.09E-43)
            int r2 = r0.a(r2)
            if (r2 != 0) goto L_0x00ae
            r0 = r11
            goto L_0x0028
        L_0x00ae:
            byte[] r0 = r0.b(r4)
            if (r0 != 0) goto L_0x016b
            r0 = r11
            goto L_0x0028
        L_0x00b7:
            r2 = 132(0x84, float:1.85E-43)
            byte[] r2 = r0.b(r2)
            if (r2 != 0) goto L_0x00c2
            r0 = r11
            goto L_0x0028
        L_0x00c2:
            r2 = 133(0x85, float:1.86E-43)
            long r2 = r0.e(r2)
            int r0 = (r5 > r2 ? 1 : (r5 == r2 ? 0 : -1))
            if (r0 != 0) goto L_0x016b
            r0 = r11
            goto L_0x0028
        L_0x00cf:
            r2 = 133(0x85, float:1.86E-43)
            long r2 = r0.e(r2)
            int r2 = (r5 > r2 ? 1 : (r5 == r2 ? 0 : -1))
            if (r2 != 0) goto L_0x00dc
            r0 = r11
            goto L_0x0028
        L_0x00dc:
            r2 = 139(0x8b, float:1.95E-43)
            byte[] r2 = r0.b(r2)
            if (r2 != 0) goto L_0x00e7
            r0 = r11
            goto L_0x0028
        L_0x00e7:
            r2 = 149(0x95, float:2.09E-43)
            int r2 = r0.a(r2)
            if (r2 != 0) goto L_0x00f2
            r0 = r11
            goto L_0x0028
        L_0x00f2:
            r2 = 151(0x97, float:2.12E-43)
            com.sd.a.a.a.a.w[] r0 = r0.d(r2)
            if (r0 != 0) goto L_0x016b
            r0 = r11
            goto L_0x0028
        L_0x00fd:
            byte[] r0 = r0.b(r4)
            if (r0 != 0) goto L_0x016b
            r0 = r11
            goto L_0x0028
        L_0x0106:
            r2 = 133(0x85, float:1.86E-43)
            long r2 = r0.e(r2)
            int r2 = (r5 > r2 ? 1 : (r5 == r2 ? 0 : -1))
            if (r2 != 0) goto L_0x0113
            r0 = r11
            goto L_0x0028
        L_0x0113:
            r2 = 137(0x89, float:1.92E-43)
            com.sd.a.a.a.a.w r2 = r0.c(r2)
            if (r2 != 0) goto L_0x011e
            r0 = r11
            goto L_0x0028
        L_0x011e:
            r2 = 139(0x8b, float:1.95E-43)
            byte[] r2 = r0.b(r2)
            if (r2 != 0) goto L_0x0129
            r0 = r11
            goto L_0x0028
        L_0x0129:
            r2 = 155(0x9b, float:2.17E-43)
            int r2 = r0.a(r2)
            if (r2 != 0) goto L_0x0134
            r0 = r11
            goto L_0x0028
        L_0x0134:
            r2 = 151(0x97, float:2.12E-43)
            com.sd.a.a.a.a.w[] r0 = r0.d(r2)
            if (r0 != 0) goto L_0x016b
            r0 = r11
            goto L_0x0028
        L_0x013f:
            r2 = 137(0x89, float:1.92E-43)
            com.sd.a.a.a.a.w r2 = r0.c(r2)
            if (r2 != 0) goto L_0x014a
            r0 = r11
            goto L_0x0028
        L_0x014a:
            r2 = 139(0x8b, float:1.95E-43)
            byte[] r2 = r0.b(r2)
            if (r2 != 0) goto L_0x0155
            r0 = r11
            goto L_0x0028
        L_0x0155:
            r2 = 155(0x9b, float:2.17E-43)
            int r2 = r0.a(r2)
            if (r2 != 0) goto L_0x0160
            r0 = r11
            goto L_0x0028
        L_0x0160:
            r2 = 151(0x97, float:2.12E-43)
            com.sd.a.a.a.a.w[] r0 = r0.d(r2)
            if (r0 != 0) goto L_0x016b
            r0 = r11
            goto L_0x0028
        L_0x016b:
            r0 = r13
            goto L_0x0028
        L_0x016e:
            r0 = 128(0x80, float:1.794E-43)
            if (r0 == r1) goto L_0x0176
            r0 = 132(0x84, float:1.85E-43)
            if (r0 != r1) goto L_0x0297
        L_0x0176:
            java.io.ByteArrayInputStream r2 = r14.f46a
            if (r2 != 0) goto L_0x0184
            r0 = r12
        L_0x017b:
            r14.c = r0
            com.sd.a.a.a.a.e r0 = r14.c
            if (r0 != 0) goto L_0x0297
            r0 = r12
            goto L_0x000c
        L_0x0184:
            int r3 = b(r2)
            com.sd.a.a.a.a.e r4 = new com.sd.a.a.a.a.e
            r4.<init>()
            r5 = r11
        L_0x018e:
            if (r5 >= r3) goto L_0x0294
            int r6 = b(r2)
            int r7 = b(r2)
            com.sd.a.a.a.a.i r8 = new com.sd.a.a.a.a.i
            r8.<init>()
            int r9 = r2.available()
            if (r9 > 0) goto L_0x01a5
            r0 = r12
            goto L_0x017b
        L_0x01a5:
            java.util.HashMap r10 = new java.util.HashMap
            r10.<init>()
            byte[] r0 = a(r2, r10)
            if (r0 == 0) goto L_0x01eb
            r8.e(r0)
        L_0x01b3:
            r0 = 151(0x97, float:2.12E-43)
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            java.lang.Object r0 = r10.get(r0)
            byte[] r0 = (byte[]) r0
            if (r0 == 0) goto L_0x01c4
            r8.g(r0)
        L_0x01c4:
            r0 = 129(0x81, float:1.81E-43)
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            java.lang.Object r0 = r10.get(r0)
            java.lang.Integer r0 = (java.lang.Integer) r0
            if (r0 == 0) goto L_0x01d9
            int r0 = r0.intValue()
            r8.a(r0)
        L_0x01d9:
            int r0 = r2.available()
            int r0 = r9 - r0
            int r0 = r6 - r0
            if (r0 <= 0) goto L_0x01f7
            boolean r0 = a(r2, r8, r0)
            if (r0 != 0) goto L_0x01fb
            r0 = r12
            goto L_0x017b
        L_0x01eb:
            java.lang.String[] r0 = com.sd.a.a.a.a.f.f41a
            r0 = r0[r11]
            byte[] r0 = r0.getBytes()
            r8.e(r0)
            goto L_0x01b3
        L_0x01f7:
            if (r0 >= 0) goto L_0x01fb
            r0 = r12
            goto L_0x017b
        L_0x01fb:
            byte[] r0 = r8.e()
            if (r0 != 0) goto L_0x0222
            byte[] r0 = r8.i()
            if (r0 != 0) goto L_0x0222
            byte[] r0 = r8.j()
            if (r0 != 0) goto L_0x0222
            byte[] r0 = r8.c()
            if (r0 != 0) goto L_0x0222
            long r9 = java.lang.System.currentTimeMillis()
            java.lang.String r0 = java.lang.Long.toOctalString(r9)
            byte[] r0 = r0.getBytes()
            r8.c(r0)
        L_0x0222:
            if (r7 <= 0) goto L_0x0255
            byte[] r0 = new byte[r7]
            r2.read(r0, r11, r7)
            byte[] r6 = r8.h()
            if (r6 == 0) goto L_0x0240
            java.lang.String r7 = new java.lang.String
            r7.<init>(r6)
            java.lang.String r6 = "base64"
            boolean r6 = r7.equalsIgnoreCase(r6)
            if (r6 == 0) goto L_0x0245
            byte[] r0 = com.sd.a.a.a.a.x.a(r0)
        L_0x0240:
            if (r0 != 0) goto L_0x0252
            r0 = r12
            goto L_0x017b
        L_0x0245:
            java.lang.String r6 = "quoted-printable"
            boolean r6 = r7.equalsIgnoreCase(r6)
            if (r6 == 0) goto L_0x0240
            byte[] r0 = com.sd.a.a.a.a.u.a(r0)
            goto L_0x0240
        L_0x0252:
            r8.a(r0)
        L_0x0255:
            byte[] r0 = com.sd.a.a.a.a.n.d
            if (r0 != 0) goto L_0x025d
            byte[] r0 = com.sd.a.a.a.a.n.e
            if (r0 == 0) goto L_0x028e
        L_0x025d:
            byte[] r0 = com.sd.a.a.a.a.n.e
            if (r0 == 0) goto L_0x027a
            byte[] r0 = r8.c()
            if (r0 == 0) goto L_0x027a
            byte[] r6 = com.sd.a.a.a.a.n.e
            boolean r0 = java.util.Arrays.equals(r6, r0)
            if (r13 != r0) goto L_0x027a
            r0 = r11
        L_0x0270:
            if (r0 != 0) goto L_0x0290
            r4.b(r8)
        L_0x0275:
            int r0 = r5 + 1
            r5 = r0
            goto L_0x018e
        L_0x027a:
            byte[] r0 = com.sd.a.a.a.a.n.d
            if (r0 == 0) goto L_0x028e
            byte[] r0 = r8.g()
            if (r0 == 0) goto L_0x028e
            byte[] r6 = com.sd.a.a.a.a.n.d
            boolean r0 = java.util.Arrays.equals(r6, r0)
            if (r13 != r0) goto L_0x028e
            r0 = r11
            goto L_0x0270
        L_0x028e:
            r0 = r13
            goto L_0x0270
        L_0x0290:
            r4.a(r8)
            goto L_0x0275
        L_0x0294:
            r0 = r4
            goto L_0x017b
        L_0x0297:
            switch(r1) {
                case 128: goto L_0x029d;
                case 129: goto L_0x02a8;
                case 130: goto L_0x02b1;
                case 131: goto L_0x02ba;
                case 132: goto L_0x02c3;
                case 133: goto L_0x02f6;
                case 134: goto L_0x02ed;
                case 135: goto L_0x0308;
                case 136: goto L_0x02ff;
                default: goto L_0x029a;
            }
        L_0x029a:
            r0 = r12
            goto L_0x000c
        L_0x029d:
            com.sd.a.a.a.a.j r0 = new com.sd.a.a.a.a.j
            com.sd.a.a.a.a.g r1 = r14.b
            com.sd.a.a.a.a.e r2 = r14.c
            r0.<init>(r1, r2)
            goto L_0x000c
        L_0x02a8:
            com.sd.a.a.a.a.s r0 = new com.sd.a.a.a.a.s
            com.sd.a.a.a.a.g r1 = r14.b
            r0.<init>(r1)
            goto L_0x000c
        L_0x02b1:
            com.sd.a.a.a.a.p r0 = new com.sd.a.a.a.a.p
            com.sd.a.a.a.a.g r1 = r14.b
            r0.<init>(r1)
            goto L_0x000c
        L_0x02ba:
            com.sd.a.a.a.a.v r0 = new com.sd.a.a.a.a.v
            com.sd.a.a.a.a.g r1 = r14.b
            r0.<init>(r1)
            goto L_0x000c
        L_0x02c3:
            com.sd.a.a.a.a.t r0 = new com.sd.a.a.a.a.t
            com.sd.a.a.a.a.g r1 = r14.b
            com.sd.a.a.a.a.e r2 = r14.c
            r0.<init>(r1, r2)
            byte[] r1 = r0.b()
            if (r1 != 0) goto L_0x02d5
            r0 = r12
            goto L_0x000c
        L_0x02d5:
            java.lang.String r2 = new java.lang.String
            r2.<init>(r1)
            java.lang.String r1 = "application/vnd.wap.multipart.mixed"
            boolean r1 = r2.equals(r1)
            if (r1 != 0) goto L_0x000c
            java.lang.String r1 = "application/vnd.wap.multipart.related"
            boolean r1 = r2.equals(r1)
            if (r1 != 0) goto L_0x000c
            r0 = r12
            goto L_0x000c
        L_0x02ed:
            com.sd.a.a.a.a.m r0 = new com.sd.a.a.a.a.m
            com.sd.a.a.a.a.g r1 = r14.b
            r0.<init>(r1)
            goto L_0x000c
        L_0x02f6:
            com.sd.a.a.a.a.a r0 = new com.sd.a.a.a.a.a
            com.sd.a.a.a.a.g r1 = r14.b
            r0.<init>(r1)
            goto L_0x000c
        L_0x02ff:
            com.sd.a.a.a.a.c r0 = new com.sd.a.a.a.a.c
            com.sd.a.a.a.a.g r1 = r14.b
            r0.<init>(r1)
            goto L_0x000c
        L_0x0308:
            com.sd.a.a.a.a.o r0 = new com.sd.a.a.a.a.o
            com.sd.a.a.a.a.g r1 = r14.b
            r0.<init>(r1)
            goto L_0x000c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.sd.a.a.a.a.n.a():com.sd.a.a.a.a.r");
    }
}
