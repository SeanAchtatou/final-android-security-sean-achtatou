package com.sd.a.a.a.a;

import java.io.ByteArrayOutputStream;

public final class u {

    /* renamed from: a  reason: collision with root package name */
    private static byte f49a = 61;

    public static final byte[] a(byte[] bArr) {
        if (bArr == null) {
            return null;
        }
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        int i = 0;
        while (i < bArr.length) {
            byte b = bArr[i];
            if (b == f49a) {
                try {
                    if (13 == ((char) bArr[i + 1]) && 10 == ((char) bArr[i + 2])) {
                        i += 2;
                    } else {
                        int i2 = i + 1;
                        int digit = Character.digit((char) bArr[i2], 16);
                        i = i2 + 1;
                        int digit2 = Character.digit((char) bArr[i], 16);
                        if (digit == -1 || digit2 == -1) {
                            return null;
                        }
                        byteArrayOutputStream.write((char) ((digit << 4) + digit2));
                    }
                } catch (ArrayIndexOutOfBoundsException e) {
                    return null;
                }
            } else {
                byteArrayOutputStream.write(b);
            }
            i++;
        }
        return byteArrayOutputStream.toByteArray();
    }
}
