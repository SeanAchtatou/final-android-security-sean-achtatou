package com.sd.a.a.a.b;

import android.content.ContentUris;
import android.content.UriMatcher;
import android.net.Uri;
import android.util.Log;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;

public final class d extends a {

    /* renamed from: a  reason: collision with root package name */
    private static final UriMatcher f55a;
    private static final HashMap b;
    private static d c;
    private final HashMap d = new HashMap();
    private final HashMap e = new HashMap();

    static {
        UriMatcher uriMatcher = new UriMatcher(-1);
        f55a = uriMatcher;
        uriMatcher.addURI("mms", null, 0);
        f55a.addURI("mms", "#", 1);
        f55a.addURI("mms", "inbox", 2);
        f55a.addURI("mms", "inbox/#", 3);
        f55a.addURI("mms", "sent", 4);
        f55a.addURI("mms", "sent/#", 5);
        f55a.addURI("mms", "drafts", 6);
        f55a.addURI("mms", "drafts/#", 7);
        f55a.addURI("mms", "outbox", 8);
        f55a.addURI("mms", "outbox/#", 9);
        f55a.addURI("mms-sms", "conversations", 10);
        f55a.addURI("mms-sms", "conversations/#", 11);
        HashMap hashMap = new HashMap();
        b = hashMap;
        hashMap.put(2, 1);
        b.put(4, 2);
        b.put(6, 3);
        b.put(8, 4);
    }

    private d() {
    }

    private void a(long j) {
        Log.v("PduCache", "Purge cache in thread: " + j);
        HashSet hashSet = (HashSet) this.e.remove(Long.valueOf(j));
        if (hashSet != null) {
            Iterator it = hashSet.iterator();
            while (it.hasNext()) {
                Uri uri = (Uri) it.next();
                c cVar = (c) super.b((Object) uri);
                if (cVar != null) {
                    c(uri, cVar);
                }
            }
        }
    }

    private c b(Uri uri) {
        c cVar = (c) super.b((Object) uri);
        if (cVar == null) {
            return null;
        }
        b(uri, cVar);
        c(uri, cVar);
        return cVar;
    }

    public static final synchronized d b() {
        d dVar;
        synchronized (d.class) {
            if (c == null) {
                Log.v("PduCache", "Constructing new PduCache instance.");
                c = new d();
            }
            dVar = c;
        }
        return dVar;
    }

    private void b(Uri uri, c cVar) {
        HashSet hashSet = (HashSet) this.e.get(Long.valueOf(cVar.c()));
        if (hashSet != null) {
            hashSet.remove(uri);
        }
    }

    private void c(Uri uri, c cVar) {
        HashSet hashSet = (HashSet) this.e.get(Integer.valueOf(cVar.b()));
        if (hashSet != null) {
            hashSet.remove(uri);
        }
    }

    /* renamed from: a */
    public final synchronized c b(Uri uri) {
        c cVar;
        HashSet hashSet;
        int match = f55a.match(uri);
        switch (match) {
            case 0:
            case 10:
                a();
                cVar = null;
                break;
            case 1:
                cVar = b(uri);
                break;
            case 2:
            case 4:
            case 6:
            case 8:
                Integer num = (Integer) b.get(Integer.valueOf(match));
                Log.v("PduCache", "Purge cache in message box: " + num);
                if (!(num == null || (hashSet = (HashSet) this.d.remove(num)) == null)) {
                    Iterator it = hashSet.iterator();
                    while (it.hasNext()) {
                        Uri uri2 = (Uri) it.next();
                        c cVar2 = (c) super.b((Object) uri2);
                        if (cVar2 != null) {
                            b(uri2, cVar2);
                        }
                    }
                }
                cVar = null;
                break;
            case 3:
            case 5:
            case 7:
            case 9:
                cVar = b(Uri.withAppendedPath(android.a.d.f7a, uri.getLastPathSegment()));
                break;
            case 11:
                a(ContentUris.parseId(uri));
                cVar = null;
                break;
            default:
                cVar = null;
                break;
        }
        return cVar;
    }

    public final synchronized void a() {
        super.a();
        this.d.clear();
        this.e.clear();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.sd.a.a.a.b.a.a(java.lang.Object, java.lang.Object):boolean
     arg types: [android.net.Uri, com.sd.a.a.a.b.c]
     candidates:
      com.sd.a.a.a.b.d.a(android.net.Uri, com.sd.a.a.a.b.c):boolean
      com.sd.a.a.a.b.a.a(java.lang.Object, java.lang.Object):boolean */
    public final synchronized boolean a(Uri uri, c cVar) {
        HashSet hashSet;
        Uri withAppendedPath;
        boolean a2;
        int b2 = cVar.b();
        HashSet hashSet2 = (HashSet) this.d.get(Integer.valueOf(b2));
        if (hashSet2 == null) {
            HashSet hashSet3 = new HashSet();
            this.d.put(Integer.valueOf(b2), hashSet3);
            hashSet = hashSet3;
        } else {
            hashSet = hashSet2;
        }
        long c2 = cVar.c();
        HashSet hashSet4 = (HashSet) this.e.get(Long.valueOf(c2));
        if (hashSet4 == null) {
            hashSet4 = new HashSet();
            this.e.put(Long.valueOf(c2), hashSet4);
        }
        switch (f55a.match(uri)) {
            case 1:
                withAppendedPath = uri;
                Log.v("PduCache", uri + " -> " + withAppendedPath);
                break;
            case 2:
            case 4:
            case 6:
            case 8:
            default:
                withAppendedPath = null;
                break;
            case 3:
            case 5:
            case 7:
            case 9:
                withAppendedPath = Uri.withAppendedPath(android.a.d.f7a, uri.getLastPathSegment());
                Log.v("PduCache", uri + " -> " + withAppendedPath);
                break;
        }
        a2 = super.a((Object) withAppendedPath, (Object) cVar);
        if (a2) {
            hashSet.add(withAppendedPath);
            hashSet4.add(withAppendedPath);
        }
        return a2;
    }
}
