package com.sd.a.a.a.a;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;

public class b {

    /* renamed from: a  reason: collision with root package name */
    private static final int[] f38a = {0, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 17, 106, 2026, 1000, 1015};
    private static final String[] b = {"*", "us-ascii", "iso-8859-1", "iso-8859-2", "iso-8859-3", "iso-8859-4", "iso-8859-5", "iso-8859-6", "iso-8859-7", "iso-8859-8", "iso-8859-9", "shift_JIS", "utf-8", "big5", "iso-10646-ucs-2", "utf-16"};
    private static final HashMap c = new HashMap();
    private static final HashMap d = new HashMap();
    private static /* synthetic */ boolean e = (!b.class.desiredAssertionStatus());

    static {
        if (e || f38a.length == b.length) {
            int length = f38a.length - 1;
            for (int i = 0; i <= length; i++) {
                c.put(Integer.valueOf(f38a[i]), b[i]);
                d.put(b[i], Integer.valueOf(f38a[i]));
            }
            return;
        }
        throw new AssertionError();
    }

    private b() {
    }

    public static int a(String str) {
        if (str == null) {
            return -1;
        }
        Integer num = (Integer) d.get(str);
        if (num != null) {
            return num.intValue();
        }
        throw new UnsupportedEncodingException();
    }

    public static String a(int i) {
        String str = (String) c.get(Integer.valueOf(i));
        if (str != null) {
            return str;
        }
        throw new UnsupportedEncodingException();
    }
}
