package com.sd.a.a.a.a;

import android.net.Uri;
import java.util.HashMap;
import java.util.Map;

public final class i {

    /* renamed from: a  reason: collision with root package name */
    static final byte[] f44a = "from-data".getBytes();
    static final byte[] b = "attachment".getBytes();
    static final byte[] c = "inline".getBytes();
    private Map d;
    private Uri e;
    private byte[] f;

    public i() {
        this.d = null;
        this.e = null;
        this.f = null;
        this.d = new HashMap();
    }

    public final void a(int i) {
        this.d.put(129, Integer.valueOf(i));
    }

    public final void a(Uri uri) {
        this.e = uri;
    }

    public final void a(byte[] bArr) {
        if (bArr != null) {
            this.f = new byte[bArr.length];
            System.arraycopy(bArr, 0, this.f, 0, bArr.length);
        }
    }

    public final byte[] a() {
        if (this.f == null) {
            return null;
        }
        byte[] bArr = new byte[this.f.length];
        System.arraycopy(this.f, 0, bArr, 0, this.f.length);
        return bArr;
    }

    public final Uri b() {
        return this.e;
    }

    public final void b(byte[] bArr) {
        if (bArr == null || bArr.length == 0) {
            throw new IllegalArgumentException("Content-Id may not be null or empty.");
        } else if (bArr.length > 1 && ((char) bArr[0]) == '<' && ((char) bArr[bArr.length - 1]) == '>') {
            this.d.put(192, bArr);
        } else {
            byte[] bArr2 = new byte[(bArr.length + 2)];
            bArr2[0] = 60;
            bArr2[bArr2.length - 1] = 62;
            System.arraycopy(bArr, 0, bArr2, 1, bArr.length);
            this.d.put(192, bArr2);
        }
    }

    public final void c(byte[] bArr) {
        if (bArr == null) {
            throw new NullPointerException("null content-location");
        }
        this.d.put(142, bArr);
    }

    public final byte[] c() {
        return (byte[]) this.d.get(192);
    }

    public final int d() {
        Integer num = (Integer) this.d.get(129);
        if (num == null) {
            return 0;
        }
        return num.intValue();
    }

    public final void d(byte[] bArr) {
        if (bArr == null) {
            throw new NullPointerException("null content-disposition");
        }
        this.d.put(197, bArr);
    }

    public final void e(byte[] bArr) {
        if (bArr == null) {
            throw new NullPointerException("null content-type");
        }
        this.d.put(145, bArr);
    }

    public final byte[] e() {
        return (byte[]) this.d.get(142);
    }

    public final void f(byte[] bArr) {
        if (bArr == null) {
            throw new NullPointerException("null content-transfer-encoding");
        }
        this.d.put(200, bArr);
    }

    public final byte[] f() {
        return (byte[]) this.d.get(197);
    }

    public final void g(byte[] bArr) {
        if (bArr == null) {
            throw new NullPointerException("null content-id");
        }
        this.d.put(151, bArr);
    }

    public final byte[] g() {
        return (byte[]) this.d.get(145);
    }

    public final void h(byte[] bArr) {
        if (bArr == null) {
            throw new NullPointerException("null content-id");
        }
        this.d.put(152, bArr);
    }

    public final byte[] h() {
        return (byte[]) this.d.get(200);
    }

    public final byte[] i() {
        return (byte[]) this.d.get(151);
    }

    public final byte[] j() {
        return (byte[]) this.d.get(152);
    }

    public final String k() {
        byte[] bArr = (byte[]) this.d.get(151);
        if (bArr == null && (bArr = (byte[]) this.d.get(152)) == null) {
            bArr = (byte[]) this.d.get(142);
        }
        return bArr == null ? "cid:" + new String((byte[]) this.d.get(192)) : new String(bArr);
    }
}
