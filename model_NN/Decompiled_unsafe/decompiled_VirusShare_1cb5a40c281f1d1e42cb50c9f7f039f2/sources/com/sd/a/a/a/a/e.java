package com.sd.a.a.a.a;

import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

public final class e {

    /* renamed from: a  reason: collision with root package name */
    private Vector f40a;
    private Map b;
    private Map c;
    private Map d;
    private Map e;

    public e() {
        this.f40a = null;
        this.b = null;
        this.c = null;
        this.d = null;
        this.e = null;
        this.f40a = new Vector();
        this.b = new HashMap();
        this.c = new HashMap();
        this.d = new HashMap();
        this.e = new HashMap();
    }

    private void c(i iVar) {
        byte[] c2 = iVar.c();
        if (c2 != null) {
            this.b.put(new String(c2), iVar);
        }
        byte[] e2 = iVar.e();
        if (e2 != null) {
            this.c.put(new String(e2), iVar);
        }
        byte[] i = iVar.i();
        if (i != null) {
            this.d.put(new String(i), iVar);
        }
        byte[] j = iVar.j();
        if (j != null) {
            this.e.put(new String(j), iVar);
        }
    }

    public final int a() {
        return this.f40a.size();
    }

    public final i a(int i) {
        return (i) this.f40a.get(i);
    }

    public final i a(String str) {
        return (i) this.b.get(str);
    }

    public final boolean a(i iVar) {
        if (iVar == null) {
            throw new NullPointerException();
        }
        c(iVar);
        return this.f40a.add(iVar);
    }

    public final i b(String str) {
        return (i) this.c.get(str);
    }

    public final void b(i iVar) {
        if (iVar == null) {
            throw new NullPointerException();
        }
        c(iVar);
        this.f40a.add(0, iVar);
    }

    public final i c(String str) {
        return (i) this.d.get(str);
    }

    public final i d(String str) {
        return (i) this.e.get(str);
    }
}
