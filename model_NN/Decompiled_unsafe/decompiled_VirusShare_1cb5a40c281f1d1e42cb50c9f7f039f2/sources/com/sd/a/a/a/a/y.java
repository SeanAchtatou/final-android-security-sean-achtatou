package com.sd.a.a.a.a;

import java.io.ByteArrayOutputStream;

final class y {

    /* renamed from: a  reason: collision with root package name */
    int f52a;
    private q b;
    private q c;
    private /* synthetic */ k d;

    /* synthetic */ y(k kVar) {
        this(kVar, (byte) 0);
    }

    private y(k kVar, byte b2) {
        this.d = kVar;
        this.b = null;
        this.c = null;
        this.f52a = 0;
    }

    /* access modifiers changed from: package-private */
    public final void a() {
        if (this.c != null) {
            throw new RuntimeException("BUG: Invalid newbuf() before copy()");
        }
        q qVar = new q();
        qVar.f47a = this.d.f45a;
        qVar.b = this.d.b;
        qVar.c = this.b;
        this.b = qVar;
        this.f52a++;
        this.d.f45a = new ByteArrayOutputStream();
        this.d.b = 0;
    }

    /* access modifiers changed from: package-private */
    public final void b() {
        ByteArrayOutputStream byteArrayOutputStream = this.d.f45a;
        int i = this.d.b;
        this.d.f45a = this.b.f47a;
        this.d.b = this.b.b;
        this.c = this.b;
        this.b = this.b.c;
        this.f52a--;
        this.c.f47a = byteArrayOutputStream;
        this.c.b = i;
    }

    /* access modifiers changed from: package-private */
    public final void c() {
        this.d.a(this.c.f47a.toByteArray(), this.c.b);
        this.c = null;
    }

    /* access modifiers changed from: package-private */
    public final h d() {
        h hVar = new h(this.d);
        int unused = hVar.f43a = this.d.b;
        int unused2 = hVar.b = this.f52a;
        return hVar;
    }
}
