package com.sd.a.a.a.a;

import com.sd.a.a.a.a;
import java.util.ArrayList;
import java.util.HashMap;

public final class g {

    /* renamed from: a  reason: collision with root package name */
    private HashMap f42a;

    public g() {
        this.f42a = null;
        this.f42a = new HashMap();
    }

    /* access modifiers changed from: protected */
    public final int a(int i) {
        Integer num = (Integer) this.f42a.get(Integer.valueOf(i));
        if (num == null) {
            return 0;
        }
        return num.intValue();
    }

    /* access modifiers changed from: protected */
    public final void a(int i, int i2) {
        int i3;
        switch (i2) {
            case 134:
            case 144:
            case 145:
            case 148:
            case 162:
            case 167:
            case 169:
            case 171:
            case 177:
            case 187:
            case 188:
                if (!(128 == i || 129 == i)) {
                    throw new a("Invalid Octet value!");
                }
                i3 = i;
                break;
            case 135:
            case 136:
            case 137:
            case 138:
            case 139:
            case 142:
            case 147:
            case 150:
            case 151:
            case 152:
            case 154:
            case 157:
            case 158:
            case 159:
            case 160:
            case 161:
            case 164:
            case 166:
            case 168:
            case 170:
            case 172:
            case 173:
            case 174:
            case 175:
            case 176:
            case 178:
            case 179:
            case 181:
            case 182:
            case 183:
            case 184:
            case 185:
            case 189:
            case 190:
            default:
                throw new RuntimeException("Invalid header field!");
            case 140:
                if (i < 128 || i > 151) {
                    throw new a("Invalid Octet value!");
                }
                i3 = i;
                break;
            case 141:
                if (i < 16 || i > 19) {
                    i3 = 18;
                    break;
                }
                i3 = i;
                break;
            case 143:
                if (i < 128 || i > 130) {
                    throw new a("Invalid Octet value!");
                }
                i3 = i;
                break;
            case 146:
                if (i > 196 && i < 224) {
                    i3 = 192;
                    break;
                } else {
                    if ((i > 235 && i <= 255) || i < 128 || ((i > 136 && i < 192) || i > 255)) {
                        i3 = 224;
                        break;
                    }
                    i3 = i;
                    break;
                }
            case 149:
                if (i < 128 || i > 135) {
                    throw new a("Invalid Octet value!");
                }
                i3 = i;
                break;
            case 153:
                if (i <= 194 || i >= 224) {
                    if (i > 227 && i <= 255) {
                        i3 = 224;
                        break;
                    } else {
                        if (i < 128 || ((i > 128 && i < 192) || i > 255)) {
                            i3 = 224;
                            break;
                        }
                        i3 = i;
                        break;
                    }
                } else {
                    i3 = 192;
                    break;
                }
            case 155:
                if (!(128 == i || 129 == i)) {
                    throw new a("Invalid Octet value!");
                }
                i3 = i;
                break;
            case 156:
                if (i < 128 || i > 131) {
                    throw new a("Invalid Octet value!");
                }
                i3 = i;
                break;
            case 163:
                if (i < 128 || i > 132) {
                    throw new a("Invalid Octet value!");
                }
                i3 = i;
                break;
            case 165:
                if (i <= 193 || i >= 224) {
                    if (i > 228 && i <= 255) {
                        i3 = 224;
                        break;
                    } else {
                        if (i < 128 || ((i > 128 && i < 192) || i > 255)) {
                            i3 = 224;
                            break;
                        }
                        i3 = i;
                        break;
                    }
                } else {
                    i3 = 192;
                    break;
                }
            case 180:
                if (128 != i) {
                    throw new a("Invalid Octet value!");
                }
                i3 = i;
                break;
            case 186:
                if (i < 128 || i > 135) {
                    throw new a("Invalid Octet value!");
                }
                i3 = i;
                break;
            case 191:
                if (!(128 == i || 129 == i)) {
                    throw new a("Invalid Octet value!");
                }
                i3 = i;
                break;
        }
        this.f42a.put(Integer.valueOf(i2), Integer.valueOf(i3));
    }

    /* access modifiers changed from: protected */
    public final void a(long j, int i) {
        switch (i) {
            case 133:
            case 135:
            case 136:
            case 142:
            case 157:
            case 159:
            case 161:
            case 173:
            case 175:
            case 179:
                break;
            default:
                throw new RuntimeException("Invalid header field!");
        }
        this.f42a.put(Integer.valueOf(i), Long.valueOf(j));
    }

    /* access modifiers changed from: protected */
    public final void a(w wVar, int i) {
        if (wVar == null) {
            throw new NullPointerException();
        }
        switch (i) {
            case 137:
            case 147:
            case 150:
            case 154:
            case 160:
            case 164:
            case 166:
            case 181:
            case 182:
                break;
            default:
                throw new RuntimeException("Invalid header field!");
        }
        this.f42a.put(Integer.valueOf(i), wVar);
    }

    /* access modifiers changed from: protected */
    public final void a(byte[] bArr, int i) {
        if (bArr == null) {
            throw new NullPointerException();
        }
        switch (i) {
            case 131:
            case 132:
            case 138:
            case 139:
            case 152:
            case 158:
            case 183:
            case 184:
            case 185:
            case 189:
            case 190:
                break;
            default:
                throw new RuntimeException("Invalid header field!");
        }
        this.f42a.put(Integer.valueOf(i), bArr);
    }

    /* access modifiers changed from: protected */
    public final void a(w[] wVarArr) {
        if (wVarArr == null) {
            throw new NullPointerException();
        }
        ArrayList arrayList = new ArrayList();
        for (w add : wVarArr) {
            arrayList.add(add);
        }
        this.f42a.put(151, arrayList);
    }

    /* access modifiers changed from: protected */
    public final void b(w wVar, int i) {
        if (wVar == null) {
            throw new NullPointerException();
        }
        switch (i) {
            case 129:
            case 130:
            case 151:
                break;
            default:
                throw new RuntimeException("Invalid header field!");
        }
        ArrayList arrayList = (ArrayList) this.f42a.get(Integer.valueOf(i));
        if (arrayList == null) {
            arrayList = new ArrayList();
        }
        arrayList.add(wVar);
        this.f42a.put(Integer.valueOf(i), arrayList);
    }

    /* access modifiers changed from: protected */
    public final byte[] b(int i) {
        return (byte[]) this.f42a.get(Integer.valueOf(i));
    }

    /* access modifiers changed from: protected */
    public final w c(int i) {
        return (w) this.f42a.get(Integer.valueOf(i));
    }

    /* access modifiers changed from: protected */
    public final w[] d(int i) {
        ArrayList arrayList = (ArrayList) this.f42a.get(Integer.valueOf(i));
        if (arrayList == null) {
            return null;
        }
        return (w[]) arrayList.toArray(new w[arrayList.size()]);
    }

    /* access modifiers changed from: protected */
    public final long e(int i) {
        Long l = (Long) this.f42a.get(Integer.valueOf(i));
        if (l == null) {
            return -1;
        }
        return l.longValue();
    }
}
