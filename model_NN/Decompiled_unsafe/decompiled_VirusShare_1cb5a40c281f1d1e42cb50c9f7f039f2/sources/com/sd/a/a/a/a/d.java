package com.sd.a.a.a.a;

import android.a.g;
import android.a.h;
import android.a.j;
import android.a.n;
import android.a.o;
import android.a.p;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;
import com.sd.a.a.a.b.b;
import com.sd.a.a.a.b.c;
import com.sd.android.mms.a.m;
import com.sd.android.mms.b.a.a.a;
import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;

public final class d {

    /* renamed from: a  reason: collision with root package name */
    private static d f39a;
    private static final com.sd.a.a.a.b.d b = com.sd.a.a.a.b.d.b();
    private static final int[] c = {129, 130, 137, 151};
    private static final String[] d = {"_id", "msg_box", "thread_id", "retr_txt", "sub", "ct_l", "ct_t", "m_cls", "m_id", "resp_txt", "tr_id", "ct_cls", "d_rpt", "m_type", "v", "pri", "rr", "read_status", "rpt_a", "retr_st", "st", "date", "d_tm", "exp", "m_size", "sub_cs", "retr_txt_cs"};
    private static final String[] e = {"_id", "chset", "cd", "cid", "cl", "ct", "fn", "name", "text"};
    private static final HashMap f;
    private static final HashMap g;
    private static final HashMap h;
    private static final HashMap i;
    private static final HashMap j;
    private static final HashMap k;
    private static final HashMap l;
    private static final HashMap m;
    private static final HashMap n;
    private static final HashMap o;
    private static final HashMap p;
    private final Context q;
    private final ContentResolver r;

    static {
        HashMap hashMap = new HashMap();
        f = hashMap;
        hashMap.put(g.f10a, 1);
        f.put(p.f17a, 2);
        f.put(j.f12a, 3);
        f.put(h.f11a, 4);
        HashMap hashMap2 = new HashMap();
        g = hashMap2;
        hashMap2.put(150, 25);
        g.put(154, 26);
        HashMap hashMap3 = new HashMap();
        l = hashMap3;
        hashMap3.put(150, "sub_cs");
        l.put(154, "retr_txt_cs");
        HashMap hashMap4 = new HashMap();
        h = hashMap4;
        hashMap4.put(154, 3);
        h.put(150, 4);
        HashMap hashMap5 = new HashMap();
        m = hashMap5;
        hashMap5.put(154, "retr_txt");
        m.put(150, "sub");
        HashMap hashMap6 = new HashMap();
        i = hashMap6;
        hashMap6.put(131, 5);
        i.put(132, 6);
        i.put(138, 7);
        i.put(139, 8);
        i.put(147, 9);
        i.put(152, 10);
        HashMap hashMap7 = new HashMap();
        n = hashMap7;
        hashMap7.put(131, "ct_l");
        n.put(132, "ct_t");
        n.put(138, "m_cls");
        n.put(139, "m_id");
        n.put(147, "resp_txt");
        n.put(152, "tr_id");
        HashMap hashMap8 = new HashMap();
        j = hashMap8;
        hashMap8.put(186, 11);
        j.put(134, 12);
        j.put(140, 13);
        j.put(141, 14);
        j.put(143, 15);
        j.put(144, 16);
        j.put(155, 17);
        j.put(145, 18);
        j.put(153, 19);
        j.put(149, 20);
        HashMap hashMap9 = new HashMap();
        o = hashMap9;
        hashMap9.put(186, "ct_cls");
        o.put(134, "d_rpt");
        o.put(140, "m_type");
        o.put(141, "v");
        o.put(143, "pri");
        o.put(144, "rr");
        o.put(155, "read_status");
        o.put(145, "rpt_a");
        o.put(153, "retr_st");
        o.put(149, "st");
        HashMap hashMap10 = new HashMap();
        k = hashMap10;
        hashMap10.put(133, 21);
        k.put(135, 22);
        k.put(136, 23);
        k.put(142, 24);
        HashMap hashMap11 = new HashMap();
        p = hashMap11;
        hashMap11.put(133, "date");
        p.put(135, "d_tm");
        p.put(136, "exp");
        p.put(142, "m_size");
    }

    private d(Context context) {
        this.q = context;
        this.r = context.getContentResolver();
    }

    public static d a(Context context) {
        if (f39a == null || !context.equals(f39a.q)) {
            f39a = new d(context);
        }
        return f39a;
    }

    public static String a(byte[] bArr) {
        try {
            return new String(bArr, "iso-8859-1");
        } catch (UnsupportedEncodingException e2) {
            Log.e("JB-PduPersister", "ISO_8859_1 must be supported!", e2);
            return "";
        }
    }

    private void a(long j2, int i2, w[] wVarArr) {
        ContentValues contentValues = new ContentValues(3);
        for (w wVar : wVarArr) {
            contentValues.clear();
            contentValues.put("address", a(wVar.b()));
            contentValues.put("charset", Integer.valueOf(wVar.a()));
            contentValues.put("type", Integer.valueOf(i2));
            b.a(this.q, this.r, Uri.parse("content://mms/" + j2 + "/addr"), contentValues);
        }
    }

    private void a(long j2, g gVar) {
        Cursor a2 = b.a(this.q, this.r, Uri.parse("content://mms/" + j2 + "/addr"), new String[]{"address", "charset", "type"}, null, null, null);
        if (a2 != null) {
            while (a2.moveToNext()) {
                try {
                    String string = a2.getString(0);
                    if (!TextUtils.isEmpty(string)) {
                        int i2 = a2.getInt(2);
                        switch (i2) {
                            case 129:
                            case 130:
                            case 151:
                                gVar.b(new w(a2.getInt(1), a(string)), i2);
                                continue;
                            case 137:
                                gVar.a(new w(a2.getInt(1), a(string)), i2);
                                continue;
                            default:
                                Log.e("JB-PduPersister", "Unknown address type: " + i2);
                                continue;
                        }
                    }
                } finally {
                    a2.close();
                }
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:18:0x0063 A[SYNTHETIC, Splitter:B:18:0x0063] */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0068 A[SYNTHETIC, Splitter:B:21:0x0068] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(com.sd.a.a.a.a.i r9, android.net.Uri r10, java.lang.String r11) {
        /*
            r8 = this;
            r5 = 0
            byte[] r0 = r9.a()     // Catch:{ FileNotFoundException -> 0x0050, IOException -> 0x0139, all -> 0x017f }
            java.lang.String r1 = "text/plain"
            boolean r1 = r1.equals(r11)     // Catch:{ FileNotFoundException -> 0x0050, IOException -> 0x0139, all -> 0x017f }
            if (r1 != 0) goto L_0x0015
            java.lang.String r1 = "application/smil"
            boolean r1 = r1.equals(r11)     // Catch:{ FileNotFoundException -> 0x0050, IOException -> 0x0139, all -> 0x017f }
            if (r1 == 0) goto L_0x0079
        L_0x0015:
            android.content.ContentValues r1 = new android.content.ContentValues     // Catch:{ FileNotFoundException -> 0x0050, IOException -> 0x0139, all -> 0x017f }
            r1.<init>()     // Catch:{ FileNotFoundException -> 0x0050, IOException -> 0x0139, all -> 0x017f }
            java.lang.String r2 = "text"
            com.sd.a.a.a.a.w r3 = new com.sd.a.a.a.a.w     // Catch:{ FileNotFoundException -> 0x0050, IOException -> 0x0139, all -> 0x017f }
            r3.<init>(r0)     // Catch:{ FileNotFoundException -> 0x0050, IOException -> 0x0139, all -> 0x017f }
            java.lang.String r0 = r3.c()     // Catch:{ FileNotFoundException -> 0x0050, IOException -> 0x0139, all -> 0x017f }
            r1.put(r2, r0)     // Catch:{ FileNotFoundException -> 0x0050, IOException -> 0x0139, all -> 0x017f }
            android.content.ContentResolver r0 = r8.r     // Catch:{ FileNotFoundException -> 0x0050, IOException -> 0x0139, all -> 0x017f }
            r2 = 0
            r3 = 0
            int r0 = r0.update(r10, r1, r2, r3)     // Catch:{ FileNotFoundException -> 0x0050, IOException -> 0x0139, all -> 0x017f }
            r1 = 1
            if (r0 == r1) goto L_0x006c
            com.sd.a.a.a.b r0 = new com.sd.a.a.a.b     // Catch:{ FileNotFoundException -> 0x0050, IOException -> 0x0139, all -> 0x017f }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x0050, IOException -> 0x0139, all -> 0x017f }
            r1.<init>()     // Catch:{ FileNotFoundException -> 0x0050, IOException -> 0x0139, all -> 0x017f }
            java.lang.String r2 = "unable to update "
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ FileNotFoundException -> 0x0050, IOException -> 0x0139, all -> 0x017f }
            java.lang.String r2 = r10.toString()     // Catch:{ FileNotFoundException -> 0x0050, IOException -> 0x0139, all -> 0x017f }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ FileNotFoundException -> 0x0050, IOException -> 0x0139, all -> 0x017f }
            java.lang.String r1 = r1.toString()     // Catch:{ FileNotFoundException -> 0x0050, IOException -> 0x0139, all -> 0x017f }
            r0.<init>(r1)     // Catch:{ FileNotFoundException -> 0x0050, IOException -> 0x0139, all -> 0x017f }
            throw r0     // Catch:{ FileNotFoundException -> 0x0050, IOException -> 0x0139, all -> 0x017f }
        L_0x0050:
            r0 = move-exception
            r1 = r5
            r2 = r5
        L_0x0053:
            java.lang.String r3 = "JB-PduPersister"
            java.lang.String r4 = "Failed to open Input/Output stream."
            android.util.Log.e(r3, r4, r0)     // Catch:{ all -> 0x0060 }
            com.sd.a.a.a.b r3 = new com.sd.a.a.a.b     // Catch:{ all -> 0x0060 }
            r3.<init>(r0)     // Catch:{ all -> 0x0060 }
            throw r3     // Catch:{ all -> 0x0060 }
        L_0x0060:
            r0 = move-exception
        L_0x0061:
            if (r2 == 0) goto L_0x0066
            r2.close()     // Catch:{ IOException -> 0x0149 }
        L_0x0066:
            if (r1 == 0) goto L_0x006b
            r1.close()     // Catch:{ IOException -> 0x0164 }
        L_0x006b:
            throw r0
        L_0x006c:
            r0 = r5
            r1 = r5
        L_0x006e:
            if (r1 == 0) goto L_0x0073
            r1.close()     // Catch:{ IOException -> 0x0103 }
        L_0x0073:
            if (r0 == 0) goto L_0x0078
            r0.close()     // Catch:{ IOException -> 0x011e }
        L_0x0078:
            return
        L_0x0079:
            android.content.ContentResolver r1 = r8.r     // Catch:{ FileNotFoundException -> 0x0050, IOException -> 0x0139, all -> 0x017f }
            java.io.OutputStream r1 = r1.openOutputStream(r10)     // Catch:{ FileNotFoundException -> 0x0050, IOException -> 0x0139, all -> 0x017f }
            if (r0 != 0) goto L_0x00e5
            android.net.Uri r0 = r9.b()     // Catch:{ FileNotFoundException -> 0x019a, IOException -> 0x0190, all -> 0x0184 }
            if (r0 == 0) goto L_0x0089
            if (r0 != r10) goto L_0x00b0
        L_0x0089:
            java.lang.String r0 = "JB-PduPersister"
            java.lang.String r2 = "Can't find data for this part."
            android.util.Log.w(r0, r2)     // Catch:{ FileNotFoundException -> 0x019a, IOException -> 0x0190, all -> 0x0184 }
            if (r1 == 0) goto L_0x0078
            r1.close()     // Catch:{ IOException -> 0x0096 }
            goto L_0x0078
        L_0x0096:
            r0 = move-exception
            java.lang.String r2 = "JB-PduPersister"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "IOException while closing: "
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.StringBuilder r1 = r3.append(r1)
            java.lang.String r1 = r1.toString()
            android.util.Log.e(r2, r1, r0)
            goto L_0x0078
        L_0x00b0:
            android.content.ContentResolver r2 = r8.r     // Catch:{ FileNotFoundException -> 0x019a, IOException -> 0x0190, all -> 0x0184 }
            java.io.InputStream r0 = r2.openInputStream(r0)     // Catch:{ FileNotFoundException -> 0x019a, IOException -> 0x0190, all -> 0x0184 }
            java.lang.String r2 = "JB-PduPersister"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x00de, IOException -> 0x0194, all -> 0x0189 }
            r3.<init>()     // Catch:{ FileNotFoundException -> 0x00de, IOException -> 0x0194, all -> 0x0189 }
            java.lang.String r4 = "Saving data to: "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ FileNotFoundException -> 0x00de, IOException -> 0x0194, all -> 0x0189 }
            java.lang.StringBuilder r3 = r3.append(r10)     // Catch:{ FileNotFoundException -> 0x00de, IOException -> 0x0194, all -> 0x0189 }
            java.lang.String r3 = r3.toString()     // Catch:{ FileNotFoundException -> 0x00de, IOException -> 0x0194, all -> 0x0189 }
            android.util.Log.v(r2, r3)     // Catch:{ FileNotFoundException -> 0x00de, IOException -> 0x0194, all -> 0x0189 }
            r2 = 256(0x100, float:3.59E-43)
            byte[] r2 = new byte[r2]     // Catch:{ FileNotFoundException -> 0x00de, IOException -> 0x0194, all -> 0x0189 }
        L_0x00d2:
            int r3 = r0.read(r2)     // Catch:{ FileNotFoundException -> 0x00de, IOException -> 0x0194, all -> 0x0189 }
            r4 = -1
            if (r3 == r4) goto L_0x006e
            r4 = 0
            r1.write(r2, r4, r3)     // Catch:{ FileNotFoundException -> 0x00de, IOException -> 0x0194, all -> 0x0189 }
            goto L_0x00d2
        L_0x00de:
            r2 = move-exception
            r7 = r2
            r2 = r1
            r1 = r0
            r0 = r7
            goto L_0x0053
        L_0x00e5:
            java.lang.String r2 = "JB-PduPersister"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x019a, IOException -> 0x0190, all -> 0x0184 }
            r3.<init>()     // Catch:{ FileNotFoundException -> 0x019a, IOException -> 0x0190, all -> 0x0184 }
            java.lang.String r4 = "Saving data to: "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ FileNotFoundException -> 0x019a, IOException -> 0x0190, all -> 0x0184 }
            java.lang.StringBuilder r3 = r3.append(r10)     // Catch:{ FileNotFoundException -> 0x019a, IOException -> 0x0190, all -> 0x0184 }
            java.lang.String r3 = r3.toString()     // Catch:{ FileNotFoundException -> 0x019a, IOException -> 0x0190, all -> 0x0184 }
            android.util.Log.v(r2, r3)     // Catch:{ FileNotFoundException -> 0x019a, IOException -> 0x0190, all -> 0x0184 }
            r1.write(r0)     // Catch:{ FileNotFoundException -> 0x019a, IOException -> 0x0190, all -> 0x0184 }
            r0 = r5
            goto L_0x006e
        L_0x0103:
            r2 = move-exception
            java.lang.String r3 = "JB-PduPersister"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "IOException while closing: "
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.StringBuilder r1 = r4.append(r1)
            java.lang.String r1 = r1.toString()
            android.util.Log.e(r3, r1, r2)
            goto L_0x0073
        L_0x011e:
            r1 = move-exception
            java.lang.String r2 = "JB-PduPersister"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "IOException while closing: "
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.StringBuilder r0 = r3.append(r0)
            java.lang.String r0 = r0.toString()
            android.util.Log.e(r2, r0, r1)
            goto L_0x0078
        L_0x0139:
            r0 = move-exception
            r1 = r5
            r2 = r5
        L_0x013c:
            java.lang.String r3 = "JB-PduPersister"
            java.lang.String r4 = "Failed to read/write data."
            android.util.Log.e(r3, r4, r0)     // Catch:{ all -> 0x0060 }
            com.sd.a.a.a.b r3 = new com.sd.a.a.a.b     // Catch:{ all -> 0x0060 }
            r3.<init>(r0)     // Catch:{ all -> 0x0060 }
            throw r3     // Catch:{ all -> 0x0060 }
        L_0x0149:
            r3 = move-exception
            java.lang.String r4 = "JB-PduPersister"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r6 = "IOException while closing: "
            java.lang.StringBuilder r5 = r5.append(r6)
            java.lang.StringBuilder r2 = r5.append(r2)
            java.lang.String r2 = r2.toString()
            android.util.Log.e(r4, r2, r3)
            goto L_0x0066
        L_0x0164:
            r2 = move-exception
            java.lang.String r3 = "JB-PduPersister"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "IOException while closing: "
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.StringBuilder r1 = r4.append(r1)
            java.lang.String r1 = r1.toString()
            android.util.Log.e(r3, r1, r2)
            goto L_0x006b
        L_0x017f:
            r0 = move-exception
            r1 = r5
            r2 = r5
            goto L_0x0061
        L_0x0184:
            r0 = move-exception
            r2 = r1
            r1 = r5
            goto L_0x0061
        L_0x0189:
            r2 = move-exception
            r7 = r2
            r2 = r1
            r1 = r0
            r0 = r7
            goto L_0x0061
        L_0x0190:
            r0 = move-exception
            r2 = r1
            r1 = r5
            goto L_0x013c
        L_0x0194:
            r2 = move-exception
            r7 = r2
            r2 = r1
            r1 = r0
            r0 = r7
            goto L_0x013c
        L_0x019a:
            r0 = move-exception
            r2 = r1
            r1 = r5
            goto L_0x0053
        */
        throw new UnsupportedOperationException("Method not decompiled: com.sd.a.a.a.a.d.a(com.sd.a.a.a.a.i, android.net.Uri, java.lang.String):void");
    }

    private static byte[] a(Cursor cursor, int i2) {
        if (!cursor.isNull(i2)) {
            return a(cursor.getString(i2));
        }
        return null;
    }

    public static byte[] a(String str) {
        try {
            return str.getBytes("iso-8859-1");
        } catch (UnsupportedEncodingException e2) {
            Log.e("JB-PduPersister", "ISO_8859_1 must be supported!", e2);
            return new byte[0];
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:77:0x017a A[SYNTHETIC, Splitter:B:77:0x017a] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private com.sd.a.a.a.a.i[] b(long r11) {
        /*
            r10 = this;
            r7 = 0
            r4 = 0
            android.content.Context r0 = r10.q
            android.content.ContentResolver r1 = r10.r
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "content://mms/"
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r2 = r2.append(r11)
            java.lang.String r3 = "/part"
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            android.net.Uri r2 = android.net.Uri.parse(r2)
            java.lang.String[] r3 = com.sd.a.a.a.a.d.e
            r5 = r4
            r6 = r4
            android.database.Cursor r0 = com.sd.a.a.a.b.b.a(r0, r1, r2, r3, r4, r5, r6)
            if (r0 == 0) goto L_0x0033
            int r1 = r0.getCount()     // Catch:{ all -> 0x0136 }
            if (r1 != 0) goto L_0x0058
        L_0x0033:
            java.lang.String r1 = "JB-PduPersister"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x0136 }
            r2.<init>()     // Catch:{ all -> 0x0136 }
            java.lang.String r3 = "loadParts("
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x0136 }
            java.lang.StringBuilder r2 = r2.append(r11)     // Catch:{ all -> 0x0136 }
            java.lang.String r3 = "): no part to load."
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x0136 }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x0136 }
            android.util.Log.v(r1, r2)     // Catch:{ all -> 0x0136 }
            if (r0 == 0) goto L_0x0056
            r0.close()
        L_0x0056:
            r0 = r4
        L_0x0057:
            return r0
        L_0x0058:
            int r1 = r0.getCount()     // Catch:{ all -> 0x0136 }
            com.sd.a.a.a.a.i[] r1 = new com.sd.a.a.a.a.i[r1]     // Catch:{ all -> 0x0136 }
            r2 = r7
        L_0x005f:
            boolean r3 = r0.moveToNext()     // Catch:{ all -> 0x0136 }
            if (r3 == 0) goto L_0x0187
            com.sd.a.a.a.a.i r3 = new com.sd.a.a.a.a.i     // Catch:{ all -> 0x0136 }
            r3.<init>()     // Catch:{ all -> 0x0136 }
            r5 = 1
            boolean r5 = r0.isNull(r5)     // Catch:{ all -> 0x0136 }
            if (r5 != 0) goto L_0x012b
            r5 = 1
            int r5 = r0.getInt(r5)     // Catch:{ all -> 0x0136 }
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)     // Catch:{ all -> 0x0136 }
        L_0x007a:
            if (r5 == 0) goto L_0x0083
            int r5 = r5.intValue()     // Catch:{ all -> 0x0136 }
            r3.a(r5)     // Catch:{ all -> 0x0136 }
        L_0x0083:
            r5 = 2
            byte[] r5 = a(r0, r5)     // Catch:{ all -> 0x0136 }
            if (r5 == 0) goto L_0x008d
            r3.d(r5)     // Catch:{ all -> 0x0136 }
        L_0x008d:
            r5 = 3
            byte[] r5 = a(r0, r5)     // Catch:{ all -> 0x0136 }
            if (r5 == 0) goto L_0x0097
            r3.b(r5)     // Catch:{ all -> 0x0136 }
        L_0x0097:
            r5 = 4
            byte[] r5 = a(r0, r5)     // Catch:{ all -> 0x0136 }
            if (r5 == 0) goto L_0x00a1
            r3.c(r5)     // Catch:{ all -> 0x0136 }
        L_0x00a1:
            r5 = 5
            byte[] r5 = a(r0, r5)     // Catch:{ all -> 0x0136 }
            if (r5 == 0) goto L_0x012e
            r3.e(r5)     // Catch:{ all -> 0x0136 }
            r6 = 6
            byte[] r6 = a(r0, r6)     // Catch:{ all -> 0x0136 }
            if (r6 == 0) goto L_0x00b5
            r3.h(r6)     // Catch:{ all -> 0x0136 }
        L_0x00b5:
            r6 = 7
            byte[] r6 = a(r0, r6)     // Catch:{ all -> 0x0136 }
            if (r6 == 0) goto L_0x00bf
            r3.g(r6)     // Catch:{ all -> 0x0136 }
        L_0x00bf:
            r6 = 0
            long r6 = r0.getLong(r6)     // Catch:{ all -> 0x0136 }
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ all -> 0x0136 }
            r8.<init>()     // Catch:{ all -> 0x0136 }
            java.lang.String r9 = "content://mms/part/"
            java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ all -> 0x0136 }
            java.lang.StringBuilder r6 = r8.append(r6)     // Catch:{ all -> 0x0136 }
            java.lang.String r6 = r6.toString()     // Catch:{ all -> 0x0136 }
            android.net.Uri r6 = android.net.Uri.parse(r6)     // Catch:{ all -> 0x0136 }
            r3.a(r6)     // Catch:{ all -> 0x0136 }
            java.lang.String r5 = a(r5)     // Catch:{ all -> 0x0136 }
            boolean r7 = com.sd.a.a.a.c.b(r5)     // Catch:{ all -> 0x0136 }
            if (r7 != 0) goto L_0x0124
            boolean r7 = com.sd.a.a.a.c.c(r5)     // Catch:{ all -> 0x0136 }
            if (r7 != 0) goto L_0x0124
            boolean r7 = com.sd.a.a.a.c.d(r5)     // Catch:{ all -> 0x0136 }
            if (r7 != 0) goto L_0x0124
            java.io.ByteArrayOutputStream r7 = new java.io.ByteArrayOutputStream     // Catch:{ all -> 0x0136 }
            r7.<init>()     // Catch:{ all -> 0x0136 }
            java.lang.String r8 = "text/plain"
            boolean r8 = r8.equals(r5)     // Catch:{ all -> 0x0136 }
            if (r8 != 0) goto L_0x0109
            java.lang.String r8 = "application/smil"
            boolean r5 = r8.equals(r5)     // Catch:{ all -> 0x0136 }
            if (r5 == 0) goto L_0x013d
        L_0x0109:
            r5 = 8
            java.lang.String r5 = r0.getString(r5)     // Catch:{ all -> 0x0136 }
            com.sd.a.a.a.a.w r6 = new com.sd.a.a.a.a.w     // Catch:{ all -> 0x0136 }
            r6.<init>(r5)     // Catch:{ all -> 0x0136 }
            byte[] r5 = r6.b()     // Catch:{ all -> 0x0136 }
            r6 = 0
            int r8 = r5.length     // Catch:{ all -> 0x0136 }
            r7.write(r5, r6, r8)     // Catch:{ all -> 0x0136 }
        L_0x011d:
            byte[] r5 = r7.toByteArray()     // Catch:{ all -> 0x0136 }
            r3.a(r5)     // Catch:{ all -> 0x0136 }
        L_0x0124:
            int r5 = r2 + 1
            r1[r2] = r3     // Catch:{ all -> 0x0136 }
            r2 = r5
            goto L_0x005f
        L_0x012b:
            r5 = r4
            goto L_0x007a
        L_0x012e:
            com.sd.a.a.a.b r1 = new com.sd.a.a.a.b     // Catch:{ all -> 0x0136 }
            java.lang.String r2 = "Content-Type must be set."
            r1.<init>(r2)     // Catch:{ all -> 0x0136 }
            throw r1     // Catch:{ all -> 0x0136 }
        L_0x0136:
            r1 = move-exception
            if (r0 == 0) goto L_0x013c
            r0.close()
        L_0x013c:
            throw r1
        L_0x013d:
            android.content.ContentResolver r5 = r10.r     // Catch:{ IOException -> 0x0165, all -> 0x018f }
            java.io.InputStream r5 = r5.openInputStream(r6)     // Catch:{ IOException -> 0x0165, all -> 0x018f }
            r6 = 256(0x100, float:3.59E-43)
            byte[] r6 = new byte[r6]     // Catch:{ IOException -> 0x0195, all -> 0x0192 }
            int r8 = r5.read(r6)     // Catch:{ IOException -> 0x0195, all -> 0x0192 }
        L_0x014b:
            if (r8 < 0) goto L_0x0156
            r9 = 0
            r7.write(r6, r9, r8)     // Catch:{ IOException -> 0x0195, all -> 0x0192 }
            int r8 = r5.read(r6)     // Catch:{ IOException -> 0x0195, all -> 0x0192 }
            goto L_0x014b
        L_0x0156:
            if (r5 == 0) goto L_0x011d
            r5.close()     // Catch:{ IOException -> 0x015c }
            goto L_0x011d
        L_0x015c:
            r5 = move-exception
            java.lang.String r6 = "JB-PduPersister"
            java.lang.String r8 = "Failed to close stream"
            android.util.Log.e(r6, r8, r5)     // Catch:{ all -> 0x0136 }
            goto L_0x011d
        L_0x0165:
            r1 = move-exception
            r2 = r4
        L_0x0167:
            java.lang.String r3 = "JB-PduPersister"
            java.lang.String r4 = "Failed to load part data"
            android.util.Log.e(r3, r4, r1)     // Catch:{ all -> 0x0177 }
            r0.close()     // Catch:{ all -> 0x0177 }
            com.sd.a.a.a.b r3 = new com.sd.a.a.a.b     // Catch:{ all -> 0x0177 }
            r3.<init>(r1)     // Catch:{ all -> 0x0177 }
            throw r3     // Catch:{ all -> 0x0177 }
        L_0x0177:
            r1 = move-exception
        L_0x0178:
            if (r2 == 0) goto L_0x017d
            r2.close()     // Catch:{ IOException -> 0x017e }
        L_0x017d:
            throw r1     // Catch:{ all -> 0x0136 }
        L_0x017e:
            r2 = move-exception
            java.lang.String r3 = "JB-PduPersister"
            java.lang.String r4 = "Failed to close stream"
            android.util.Log.e(r3, r4, r2)     // Catch:{ all -> 0x0136 }
            goto L_0x017d
        L_0x0187:
            if (r0 == 0) goto L_0x018c
            r0.close()
        L_0x018c:
            r0 = r1
            goto L_0x0057
        L_0x018f:
            r1 = move-exception
            r2 = r4
            goto L_0x0178
        L_0x0192:
            r1 = move-exception
            r2 = r5
            goto L_0x0178
        L_0x0195:
            r1 = move-exception
            r2 = r5
            goto L_0x0167
        */
        throw new UnsupportedOperationException("Method not decompiled: com.sd.a.a.a.a.d.b(long):com.sd.a.a.a.a.i[]");
    }

    public final Cursor a(long j2) {
        Uri.Builder buildUpon = n.f15a.buildUpon();
        buildUpon.appendQueryParameter("protocol", "mms");
        return b.a(this.q, this.r, buildUpon.build(), null, "err_type < ? AND due_time <= ?", new String[]{String.valueOf(10), String.valueOf(j2)}, "due_time");
    }

    public final Uri a(Uri uri, Uri uri2) {
        long parseId = ContentUris.parseId(uri);
        if (parseId == -1) {
            throw new com.sd.a.a.a.b("Error! ID of the message: -1.");
        }
        Integer num = (Integer) f.get(uri2);
        if (num == null) {
            throw new com.sd.a.a.a.b("Bad destination, must be one of content://mms/inbox, content://mms/sent, content://mms/drafts, content://mms/outbox, content://mms/temp.");
        }
        ContentValues contentValues = new ContentValues(1);
        contentValues.put("msg_box", num);
        b.a(this.q, this.r, uri, contentValues, null);
        return ContentUris.withAppendedId(uri2, parseId);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public final Uri a(i iVar, long j2) {
        Uri parse = Uri.parse("content://mms/" + j2 + "/part");
        ContentValues contentValues = new ContentValues(8);
        int d2 = iVar.d();
        if (d2 != 0) {
            contentValues.put("chset", Integer.valueOf(d2));
        }
        if (iVar.g() != null) {
            String a2 = a(iVar.g());
            contentValues.put("ct", a2);
            if ("application/smil".equals(a2)) {
                contentValues.put("seq", (Integer) -1);
            }
            if (iVar.j() != null) {
                contentValues.put("fn", new String(iVar.j()));
            }
            if (iVar.i() != null) {
                contentValues.put("name", new String(iVar.i()));
            }
            if (iVar.f() != null) {
                contentValues.put("cd", a(iVar.f()));
            }
            if (iVar.c() != null) {
                contentValues.put("cid", a(iVar.c()));
            }
            if (iVar.e() != null) {
                contentValues.put("cl", a(iVar.e()));
            }
            Uri a3 = b.a(this.q, this.r, parse, contentValues);
            if (a3 == null) {
                throw new com.sd.a.a.a.b("Failed to persist part, return null.");
            }
            a(iVar, a3, a2);
            iVar.a(a3);
            return a3;
        }
        throw new com.sd.a.a.a.b("MIME type of the part must be set.");
    }

    public final Uri a(r rVar, Uri uri) {
        e i2;
        w[] wVarArr;
        w[] d2;
        if (uri == null) {
            throw new com.sd.a.a.a.b("Uri may not be null.");
        } else if (((Integer) f.get(uri)) == null) {
            throw new com.sd.a.a.a.b("Bad destination, must be one of content://mms/inbox, content://mms/sent, content://mms/drafts, content://mms/outbox, content://mms/temp.");
        } else {
            "purge pdu cache of " + uri;
            b.b(uri);
            g gVar = rVar.f48a;
            ContentValues contentValues = new ContentValues();
            for (Map.Entry entry : m.entrySet()) {
                int intValue = ((Integer) entry.getKey()).intValue();
                w c2 = gVar.c(intValue);
                if (c2 != null) {
                    contentValues.put((String) entry.getValue(), a(c2.b()));
                    contentValues.put((String) l.get(Integer.valueOf(intValue)), Integer.valueOf(c2.a()));
                }
            }
            for (Map.Entry entry2 : n.entrySet()) {
                byte[] b2 = gVar.b(((Integer) entry2.getKey()).intValue());
                if (b2 != null) {
                    contentValues.put((String) entry2.getValue(), a(b2));
                }
            }
            for (Map.Entry entry3 : o.entrySet()) {
                int a2 = gVar.a(((Integer) entry3.getKey()).intValue());
                if (a2 != 0) {
                    contentValues.put((String) entry3.getValue(), Integer.valueOf(a2));
                }
            }
            for (Map.Entry entry4 : p.entrySet()) {
                long e2 = gVar.e(((Integer) entry4.getKey()).intValue());
                if (e2 != -1) {
                    contentValues.put((String) entry4.getValue(), Long.valueOf(e2));
                }
            }
            HashMap hashMap = new HashMap(c.length);
            for (int i3 : c) {
                if (i3 == 137) {
                    w c3 = gVar.c(i3);
                    d2 = c3 != null ? new w[]{c3} : null;
                } else {
                    d2 = gVar.d(i3);
                }
                hashMap.put(Integer.valueOf(i3), d2);
            }
            HashSet hashSet = new HashSet();
            long j2 = Long.MAX_VALUE;
            int m2 = rVar.m();
            if (m2 == 130 || m2 == 132 || m2 == 128) {
                switch (m2) {
                    case 128:
                        wVarArr = (w[]) hashMap.get(151);
                        break;
                    case 129:
                    case 131:
                    default:
                        wVarArr = null;
                        break;
                    case 130:
                    case 132:
                        wVarArr = (w[]) hashMap.get(137);
                        break;
                }
                if (wVarArr != null) {
                    for (w wVar : wVarArr) {
                        if (wVar != null) {
                            hashSet.add(wVar.c());
                        }
                    }
                }
                j2 = o.a(this.q, hashSet);
            }
            contentValues.put("thread_id", Long.valueOf(j2));
            long currentTimeMillis = System.currentTimeMillis();
            if ((rVar instanceof l) && (i2 = ((l) rVar).i()) != null) {
                int a3 = i2.a();
                for (int i4 = 0; i4 < a3; i4++) {
                    a(i2.a(i4), currentTimeMillis);
                }
            }
            Uri a4 = b.a(this.q, this.r, uri, contentValues);
            if (a4 == null) {
                throw new com.sd.a.a.a.b("persist() failed: return null.");
            }
            long parseId = ContentUris.parseId(a4);
            ContentValues contentValues2 = new ContentValues(1);
            contentValues2.put("mid", Long.valueOf(parseId));
            b.a(this.q, this.r, Uri.parse("content://mms/" + currentTimeMillis + "/part"), contentValues2, null);
            Uri parse = Uri.parse(uri + "/" + parseId);
            for (int i5 : c) {
                w[] wVarArr2 = (w[]) hashMap.get(Integer.valueOf(i5));
                if (wVarArr2 != null) {
                    a(parseId, i5, wVarArr2);
                }
            }
            return parse;
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public final r a(Uri uri) {
        r oVar;
        "load pdu by uri: " + uri;
        c cVar = (c) b.a((Object) uri);
        if (cVar != null) {
            "load pdu cache entry: uri=" + uri + " msgBox=" + cVar.b() + " threaId=" + cVar.c();
            cVar.toString() + " \n " + cVar.a().toString() + " " + uri;
            if (cVar.a() instanceof l) {
                org.b.a.a.o a2 = m.a(((l) cVar.a()).i());
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                a.a(a2, byteArrayOutputStream);
                Log.i("JB-PduPersister", byteArrayOutputStream.toString());
            }
            return cVar.a();
        }
        "pdu not hit in cache " + uri;
        Cursor a3 = b.a(this.q, this.r, uri, d, null, null, null);
        g gVar = new g();
        long parseId = ContentUris.parseId(uri);
        if (a3 != null) {
            try {
                if (a3.getCount() == 1 && a3.moveToFirst()) {
                    int i2 = a3.getInt(1);
                    long j2 = a3.getLong(2);
                    for (Map.Entry entry : h.entrySet()) {
                        int intValue = ((Integer) entry.getValue()).intValue();
                        int intValue2 = ((Integer) entry.getKey()).intValue();
                        String string = a3.getString(intValue);
                        if (string != null && string.length() > 0) {
                            gVar.a(new w(a3.getInt(((Integer) g.get(Integer.valueOf(intValue2))).intValue()), a(string)), intValue2);
                        }
                    }
                    for (Map.Entry entry2 : i.entrySet()) {
                        int intValue3 = ((Integer) entry2.getValue()).intValue();
                        int intValue4 = ((Integer) entry2.getKey()).intValue();
                        String string2 = a3.getString(intValue3);
                        if (string2 != null) {
                            gVar.a(a(string2), intValue4);
                        }
                    }
                    for (Map.Entry entry3 : j.entrySet()) {
                        int intValue5 = ((Integer) entry3.getValue()).intValue();
                        int intValue6 = ((Integer) entry3.getKey()).intValue();
                        if (!a3.isNull(intValue5)) {
                            gVar.a(a3.getInt(intValue5), intValue6);
                        }
                    }
                    for (Map.Entry entry4 : k.entrySet()) {
                        int intValue7 = ((Integer) entry4.getValue()).intValue();
                        int intValue8 = ((Integer) entry4.getKey()).intValue();
                        if (!a3.isNull(intValue7)) {
                            gVar.a(a3.getLong(intValue7), intValue8);
                        }
                    }
                    if (parseId == -1) {
                        throw new com.sd.a.a.a.b("Error! ID of the message: -1. " + uri);
                    }
                    a(parseId, gVar);
                    int a4 = gVar.a(140);
                    e eVar = new e();
                    boolean z = true;
                    if (a4 == 132 || a4 == 128) {
                        i[] iVarArr = null;
                        boolean z2 = true;
                        int i3 = 0;
                        do {
                            if (iVarArr != null && iVarArr.length != 0) {
                                break;
                            }
                            "load pdu body at time " + i3;
                            i3++;
                            iVarArr = b(parseId);
                            if (iVarArr != null) {
                                boolean z3 = z2;
                                for (i a5 : iVarArr) {
                                    eVar.a(a5);
                                    z3 = true;
                                }
                                z2 = r11 == 0 ? false : z3;
                            } else {
                                z2 = false;
                            }
                        } while (i3 <= 50);
                        z = z2;
                    }
                    switch (a4) {
                        case 128:
                            oVar = new j(gVar, eVar);
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_SEND_REQ " + uri);
                            break;
                        case 129:
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_SEND_CONF " + uri);
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_FORWARD_REQ " + uri);
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_FORWARD_CONF " + uri);
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_MBOX_STORE_REQ " + uri);
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_MBOX_STORE_CONF " + uri);
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_MBOX_VIEW_REQ " + uri);
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_MBOX_VIEW_CONF " + uri);
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_MBOX_UPLOAD_REQ " + uri);
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_MBOX_UPLOAD_CONF " + uri);
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_MBOX_DELETE_REQ " + uri);
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_MBOX_DELETE_CONF " + uri);
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_MBOX_DESCR " + uri);
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_DELETE_REQ " + uri);
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_DELETE_CONF " + uri);
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_CANCEL_REQ " + uri);
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_CANCEL_CONF " + uri);
                            throw new com.sd.a.a.a.b("Unsupported PDU type: " + Integer.toHexString(a4) + " " + uri);
                        case 130:
                            oVar = new p(gVar);
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_NOTIFICATION_IND " + uri);
                            break;
                        case 131:
                            oVar = new v(gVar);
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_NOTIFYRESP_IND " + uri);
                            break;
                        case 132:
                            oVar = new t(gVar, eVar);
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_RETRIEVE_CONF " + uri);
                            break;
                        case 133:
                            oVar = new a(gVar);
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_ACKNOWLEDGE_IND " + uri);
                            break;
                        case 134:
                            oVar = new m(gVar);
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_DELIVERY_IND " + uri);
                            break;
                        case 135:
                            oVar = new o(gVar);
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_READ_REC_IND " + uri);
                            break;
                        case 136:
                            oVar = new c(gVar);
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_READ_ORIG_IND " + uri);
                            break;
                        case 137:
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_FORWARD_REQ " + uri);
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_FORWARD_CONF " + uri);
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_MBOX_STORE_REQ " + uri);
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_MBOX_STORE_CONF " + uri);
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_MBOX_VIEW_REQ " + uri);
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_MBOX_VIEW_CONF " + uri);
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_MBOX_UPLOAD_REQ " + uri);
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_MBOX_UPLOAD_CONF " + uri);
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_MBOX_DELETE_REQ " + uri);
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_MBOX_DELETE_CONF " + uri);
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_MBOX_DESCR " + uri);
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_DELETE_REQ " + uri);
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_DELETE_CONF " + uri);
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_CANCEL_REQ " + uri);
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_CANCEL_CONF " + uri);
                            throw new com.sd.a.a.a.b("Unsupported PDU type: " + Integer.toHexString(a4) + " " + uri);
                        case 138:
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_FORWARD_CONF " + uri);
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_MBOX_STORE_REQ " + uri);
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_MBOX_STORE_CONF " + uri);
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_MBOX_VIEW_REQ " + uri);
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_MBOX_VIEW_CONF " + uri);
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_MBOX_UPLOAD_REQ " + uri);
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_MBOX_UPLOAD_CONF " + uri);
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_MBOX_DELETE_REQ " + uri);
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_MBOX_DELETE_CONF " + uri);
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_MBOX_DESCR " + uri);
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_DELETE_REQ " + uri);
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_DELETE_CONF " + uri);
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_CANCEL_REQ " + uri);
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_CANCEL_CONF " + uri);
                            throw new com.sd.a.a.a.b("Unsupported PDU type: " + Integer.toHexString(a4) + " " + uri);
                        case 139:
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_MBOX_STORE_REQ " + uri);
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_MBOX_STORE_CONF " + uri);
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_MBOX_VIEW_REQ " + uri);
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_MBOX_VIEW_CONF " + uri);
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_MBOX_UPLOAD_REQ " + uri);
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_MBOX_UPLOAD_CONF " + uri);
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_MBOX_DELETE_REQ " + uri);
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_MBOX_DELETE_CONF " + uri);
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_MBOX_DESCR " + uri);
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_DELETE_REQ " + uri);
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_DELETE_CONF " + uri);
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_CANCEL_REQ " + uri);
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_CANCEL_CONF " + uri);
                            throw new com.sd.a.a.a.b("Unsupported PDU type: " + Integer.toHexString(a4) + " " + uri);
                        case 140:
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_MBOX_STORE_CONF " + uri);
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_MBOX_VIEW_REQ " + uri);
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_MBOX_VIEW_CONF " + uri);
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_MBOX_UPLOAD_REQ " + uri);
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_MBOX_UPLOAD_CONF " + uri);
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_MBOX_DELETE_REQ " + uri);
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_MBOX_DELETE_CONF " + uri);
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_MBOX_DESCR " + uri);
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_DELETE_REQ " + uri);
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_DELETE_CONF " + uri);
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_CANCEL_REQ " + uri);
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_CANCEL_CONF " + uri);
                            throw new com.sd.a.a.a.b("Unsupported PDU type: " + Integer.toHexString(a4) + " " + uri);
                        case 141:
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_MBOX_VIEW_REQ " + uri);
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_MBOX_VIEW_CONF " + uri);
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_MBOX_UPLOAD_REQ " + uri);
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_MBOX_UPLOAD_CONF " + uri);
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_MBOX_DELETE_REQ " + uri);
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_MBOX_DELETE_CONF " + uri);
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_MBOX_DESCR " + uri);
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_DELETE_REQ " + uri);
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_DELETE_CONF " + uri);
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_CANCEL_REQ " + uri);
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_CANCEL_CONF " + uri);
                            throw new com.sd.a.a.a.b("Unsupported PDU type: " + Integer.toHexString(a4) + " " + uri);
                        case 142:
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_MBOX_VIEW_CONF " + uri);
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_MBOX_UPLOAD_REQ " + uri);
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_MBOX_UPLOAD_CONF " + uri);
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_MBOX_DELETE_REQ " + uri);
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_MBOX_DELETE_CONF " + uri);
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_MBOX_DESCR " + uri);
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_DELETE_REQ " + uri);
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_DELETE_CONF " + uri);
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_CANCEL_REQ " + uri);
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_CANCEL_CONF " + uri);
                            throw new com.sd.a.a.a.b("Unsupported PDU type: " + Integer.toHexString(a4) + " " + uri);
                        case 143:
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_MBOX_UPLOAD_REQ " + uri);
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_MBOX_UPLOAD_CONF " + uri);
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_MBOX_DELETE_REQ " + uri);
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_MBOX_DELETE_CONF " + uri);
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_MBOX_DESCR " + uri);
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_DELETE_REQ " + uri);
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_DELETE_CONF " + uri);
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_CANCEL_REQ " + uri);
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_CANCEL_CONF " + uri);
                            throw new com.sd.a.a.a.b("Unsupported PDU type: " + Integer.toHexString(a4) + " " + uri);
                        case 144:
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_MBOX_UPLOAD_CONF " + uri);
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_MBOX_DELETE_REQ " + uri);
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_MBOX_DELETE_CONF " + uri);
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_MBOX_DESCR " + uri);
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_DELETE_REQ " + uri);
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_DELETE_CONF " + uri);
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_CANCEL_REQ " + uri);
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_CANCEL_CONF " + uri);
                            throw new com.sd.a.a.a.b("Unsupported PDU type: " + Integer.toHexString(a4) + " " + uri);
                        case 145:
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_MBOX_DELETE_REQ " + uri);
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_MBOX_DELETE_CONF " + uri);
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_MBOX_DESCR " + uri);
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_DELETE_REQ " + uri);
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_DELETE_CONF " + uri);
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_CANCEL_REQ " + uri);
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_CANCEL_CONF " + uri);
                            throw new com.sd.a.a.a.b("Unsupported PDU type: " + Integer.toHexString(a4) + " " + uri);
                        case 146:
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_MBOX_DELETE_CONF " + uri);
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_MBOX_DESCR " + uri);
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_DELETE_REQ " + uri);
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_DELETE_CONF " + uri);
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_CANCEL_REQ " + uri);
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_CANCEL_CONF " + uri);
                            throw new com.sd.a.a.a.b("Unsupported PDU type: " + Integer.toHexString(a4) + " " + uri);
                        case 147:
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_MBOX_DESCR " + uri);
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_DELETE_REQ " + uri);
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_DELETE_CONF " + uri);
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_CANCEL_REQ " + uri);
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_CANCEL_CONF " + uri);
                            throw new com.sd.a.a.a.b("Unsupported PDU type: " + Integer.toHexString(a4) + " " + uri);
                        case 148:
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_DELETE_REQ " + uri);
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_DELETE_CONF " + uri);
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_CANCEL_REQ " + uri);
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_CANCEL_CONF " + uri);
                            throw new com.sd.a.a.a.b("Unsupported PDU type: " + Integer.toHexString(a4) + " " + uri);
                        case 149:
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_DELETE_CONF " + uri);
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_CANCEL_REQ " + uri);
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_CANCEL_CONF " + uri);
                            throw new com.sd.a.a.a.b("Unsupported PDU type: " + Integer.toHexString(a4) + " " + uri);
                        case 150:
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_CANCEL_REQ " + uri);
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_CANCEL_CONF " + uri);
                            throw new com.sd.a.a.a.b("Unsupported PDU type: " + Integer.toHexString(a4) + " " + uri);
                        case 151:
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_CANCEL_CONF " + uri);
                            throw new com.sd.a.a.a.b("Unsupported PDU type: " + Integer.toHexString(a4) + " " + uri);
                        default:
                            Log.i("JB-PduPersister", "MESSAGE_TYPE_DEFAULT");
                            throw new com.sd.a.a.a.b("Unrecognized PDU type: " + Integer.toHexString(a4) + " " + uri);
                    }
                    c cVar2 = new c(oVar, i2, j2);
                    "put pdu cache entry: uri=" + uri + " msgBox=" + i2 + " threaId=" + j2;
                    if (cVar2.a() instanceof l) {
                        org.b.a.a.o a6 = m.a(((l) cVar2.a()).i());
                        ByteArrayOutputStream byteArrayOutputStream2 = new ByteArrayOutputStream();
                        a.a(a6, byteArrayOutputStream2);
                        Log.i("JB-PduPersister", byteArrayOutputStream2.toString());
                    }
                    if (!z) {
                        return oVar;
                    }
                    b.a(uri, cVar2);
                    return oVar;
                }
            } finally {
                if (a3 != null) {
                    a3.close();
                }
            }
        }
        throw new com.sd.a.a.a.b("Bad uri: " + uri);
    }

    public final void a(Uri uri, e eVar) {
        "update parts of " + uri;
        c cVar = (c) b.a((Object) uri);
        if (cVar != null) {
            ((l) cVar.a()).a(eVar);
        }
        ArrayList arrayList = new ArrayList();
        HashMap hashMap = new HashMap();
        int a2 = eVar.a();
        StringBuilder append = new StringBuilder().append('(');
        for (int i2 = 0; i2 < a2; i2++) {
            i a3 = eVar.a(i2);
            Uri b2 = a3.b();
            if (b2 == null || !b2.getAuthority().startsWith("mms")) {
                arrayList.add(a3);
            } else {
                hashMap.put(b2, a3);
                if (append.length() > 1) {
                    append.append(" AND ");
                }
                append.append("_id");
                append.append("!=");
                DatabaseUtils.appendEscapedSQLString(append, b2.getLastPathSegment());
            }
        }
        append.append(')');
        long parseId = ContentUris.parseId(uri);
        b.a(this.q, this.r, Uri.parse(android.a.d.f7a + "/" + parseId + "/part"), append.length() > 2 ? append.toString() : null);
        Iterator it = arrayList.iterator();
        while (it.hasNext()) {
            a((i) it.next(), parseId);
        }
        for (Map.Entry entry : hashMap.entrySet()) {
            Uri uri2 = (Uri) entry.getKey();
            i iVar = (i) entry.getValue();
            ContentValues contentValues = new ContentValues(7);
            int d2 = iVar.d();
            if (d2 != 0) {
                contentValues.put("chset", Integer.valueOf(d2));
            }
            if (iVar.g() != null) {
                String a4 = a(iVar.g());
                contentValues.put("ct", a4);
                if (iVar.j() != null) {
                    contentValues.put("fn", new String(iVar.j()));
                }
                if (iVar.i() != null) {
                    contentValues.put("name", new String(iVar.i()));
                }
                if (iVar.f() != null) {
                    contentValues.put("cd", a(iVar.f()));
                }
                if (iVar.c() != null) {
                    contentValues.put("cid", a(iVar.c()));
                }
                if (iVar.e() != null) {
                    contentValues.put("cl", a(iVar.e()));
                }
                b.a(this.q, this.r, uri2, contentValues, null);
                if (iVar.a() != null || uri2 != iVar.b()) {
                    a(iVar, uri2, a4);
                }
            } else {
                throw new com.sd.a.a.a.b("MIME type of the part must be set.");
            }
        }
    }

    public final void a(Uri uri, j jVar) {
        "purge pdu cache of " + uri;
        b.b(uri);
        ContentValues contentValues = new ContentValues(10);
        byte[] b2 = jVar.b();
        if (b2 != null) {
            contentValues.put("ct_t", a(b2));
        }
        long l2 = jVar.l();
        if (l2 != -1) {
            contentValues.put("date", Long.valueOf(l2));
        }
        int c2 = jVar.c();
        if (c2 != 0) {
            contentValues.put("d_rpt", Integer.valueOf(c2));
        }
        long d2 = jVar.d();
        if (d2 != -1) {
            contentValues.put("exp", Long.valueOf(d2));
        }
        byte[] f2 = jVar.f();
        if (f2 != null) {
            contentValues.put("m_cls", a(f2));
        }
        int k2 = jVar.k();
        if (k2 != 0) {
            contentValues.put("pri", Integer.valueOf(k2));
        }
        int g2 = jVar.g();
        if (g2 != 0) {
            contentValues.put("rr", Integer.valueOf(g2));
        }
        byte[] h2 = jVar.h();
        if (h2 != null) {
            contentValues.put("tr_id", a(h2));
        }
        w j2 = jVar.j();
        if (j2 != null) {
            contentValues.put("sub", a(j2.b()));
            contentValues.put("sub_cs", Integer.valueOf(j2.a()));
        } else {
            contentValues.put("sub", "");
        }
        long e2 = jVar.e();
        if (e2 > 0) {
            contentValues.put("m_size", Long.valueOf(e2));
        }
        g gVar = jVar.f48a;
        HashSet hashSet = new HashSet();
        for (int i2 : c) {
            w[] wVarArr = null;
            if (i2 == 137) {
                w c3 = gVar.c(i2);
                if (c3 != null) {
                    wVarArr = new w[]{c3};
                }
            } else {
                wVarArr = gVar.d(i2);
            }
            if (wVarArr != null) {
                long parseId = ContentUris.parseId(uri);
                b.a(this.q, this.r, Uri.parse("content://mms/" + parseId + "/addr"), "type=" + i2);
                a(parseId, i2, wVarArr);
                if (i2 == 151) {
                    for (w wVar : wVarArr) {
                        if (wVar != null) {
                            hashSet.add(wVar.c());
                        }
                    }
                }
            }
        }
        contentValues.put("thread_id", Long.valueOf(o.a(this.q, hashSet)));
        b.a(this.q, this.r, uri, contentValues, null);
    }
}
