package com.a.a.a;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Iterator;
import org.a.b.a.a.a.c;

final class e {

    /* renamed from: a  reason: collision with root package name */
    private int f36a = 0;
    private int b = -1;
    private String c;
    private int d = 1;
    private ArrayList e = new ArrayList(10);
    private int f = 0;

    e(byte[] bArr) {
        try {
            this.c = new String(bArr, "UTF-8");
        } catch (UnsupportedEncodingException e2) {
            this.d = 9;
        }
    }

    private void a(String str, boolean z) {
        String str2;
        if (!z) {
            str2 = this.c.substring(this.f, this.f36a);
        } else {
            StringBuffer stringBuffer = new StringBuffer(this.f36a - this.f);
            int i = 0;
            int i2 = this.f;
            while (i2 < this.f36a) {
                if ('\\' == this.c.charAt(i2)) {
                    i2++;
                }
                stringBuffer.setCharAt(i, this.c.charAt(i2));
                i++;
                i2++;
            }
            str2 = new String(stringBuffer);
        }
        this.e.add(new d(str, str2, this.d == 7 ? 1 : 2));
    }

    private static boolean a(char c2) {
        return (c2 < 0 || c2 > ' ') && (c2 < ':' || c2 > '@') && !((c2 >= '[' && c2 <= ']') || ',' == c2 || '%' == c2 || '(' == c2 || ')' == c2 || '{' == c2 || '}' == c2 || 127 == c2);
    }

    private static boolean b(char c2) {
        return 9 == c2 || 10 == c2 || 13 == c2 || ' ' == c2;
    }

    /* access modifiers changed from: package-private */
    public final void a() {
        String str = "<no name>";
        if (this.d == 9) {
            throw new c("No UTF-8 support on platform");
        }
        boolean z = false;
        char c2 = 0;
        while (this.f36a < this.c.length()) {
            char charAt = this.c.charAt(this.f36a);
            switch (this.d) {
                case 1:
                case 2:
                    if (b(charAt)) {
                        continue;
                    } else if (a(charAt)) {
                        this.f = this.f36a;
                        this.d = 3;
                        break;
                    } else {
                        this.b = this.f36a;
                        throw new c("Parse error: Invalid name character");
                    }
                case 3:
                    if (a(charAt)) {
                        continue;
                    } else if (b(charAt)) {
                        str = this.c.substring(this.f, this.f36a);
                        this.d = 4;
                        break;
                    } else if ('=' == charAt) {
                        str = this.c.substring(this.f, this.f36a);
                        this.d = 5;
                        break;
                    } else {
                        this.b = this.f36a;
                        throw new c("Parse error: Invalid name character");
                    }
                case 4:
                    if (b(charAt)) {
                        continue;
                    } else if ('=' == charAt) {
                        this.d = 5;
                        break;
                    } else {
                        this.b = this.f36a;
                        throw new c("Parse error: Expected equals sign '='.");
                    }
                case 5:
                    if (b(charAt)) {
                        continue;
                    } else if ('\"' == charAt) {
                        this.f = this.f36a + 1;
                        this.d = 7;
                        break;
                    } else if (a(charAt)) {
                        this.f = this.f36a;
                        this.d = 8;
                        break;
                    } else {
                        this.b = this.f36a;
                        throw new c("Parse error: Unexpected character");
                    }
                case 6:
                    if (b(charAt)) {
                        continue;
                    } else if (charAt == ',') {
                        this.d = 2;
                        break;
                    } else {
                        this.b = this.f36a;
                        throw new c("Parse error: Expected a comma.");
                    }
                case 7:
                    if ('\\' == charAt) {
                        z = true;
                    }
                    if ('\"' == charAt && '\\' != c2) {
                        a(str, z);
                        this.d = 6;
                        z = false;
                        break;
                    }
                case 8:
                    if (a(charAt)) {
                        continue;
                    } else if (b(charAt)) {
                        a(str, false);
                        this.d = 6;
                        break;
                    } else if (',' == charAt) {
                        a(str, false);
                        this.d = 2;
                        break;
                    } else {
                        this.b = this.f36a;
                        throw new c("Parse error: Invalid value character");
                    }
            }
            this.f36a++;
            c2 = charAt;
        }
        switch (this.d) {
            case 1:
            case 6:
            default:
                return;
            case 2:
                throw new c("Parse error: Trailing comma.");
            case 3:
            case 4:
            case 5:
                throw new c("Parse error: Missing value.");
            case 7:
                throw new c("Parse error: Missing closing quote.");
            case 8:
                a(str, false);
                return;
        }
    }

    /* access modifiers changed from: package-private */
    public final Iterator b() {
        return this.e.iterator();
    }
}
