package com.a.a.a;

final class c {

    /* renamed from: a  reason: collision with root package name */
    private int f34a = 0;
    private int b = 0;
    private int c = 1;
    private String d;

    c(String str) {
        this.d = str;
    }

    private static boolean a(char c2) {
        return (c2 < 0 || c2 > ' ') && (c2 < ':' || c2 > '@') && !((c2 >= '[' && c2 <= ']') || ',' == c2 || '%' == c2 || '(' == c2 || ')' == c2 || '{' == c2 || '}' == c2 || 127 == c2);
    }

    private static boolean b(char c2) {
        return 9 == c2 || 10 == c2 || 13 == c2 || ' ' == c2;
    }

    /* access modifiers changed from: package-private */
    public final String a() {
        if (this.c == 6) {
            return null;
        }
        String str = null;
        while (this.f34a < this.d.length() && str == null) {
            char charAt = this.d.charAt(this.f34a);
            switch (this.c) {
                case 1:
                case 2:
                    if (b(charAt)) {
                        continue;
                    } else if (a(charAt)) {
                        this.b = this.f34a;
                        this.c = 3;
                        break;
                    } else {
                        this.c = 5;
                        throw new org.a.b.a.a.a.c("Invalid token character at position " + this.f34a);
                    }
                case 3:
                    if (a(charAt)) {
                        continue;
                    } else if (b(charAt)) {
                        str = this.d.substring(this.b, this.f34a);
                        this.c = 4;
                        break;
                    } else if (',' == charAt) {
                        str = this.d.substring(this.b, this.f34a);
                        this.c = 2;
                        break;
                    } else {
                        this.c = 5;
                        throw new org.a.b.a.a.a.c("Invalid token character at position " + this.f34a);
                    }
                case 4:
                    if (b(charAt)) {
                        continue;
                    } else if (charAt == ',') {
                        this.c = 2;
                        break;
                    } else {
                        this.c = 5;
                        throw new org.a.b.a.a.a.c("Expected a comma, found '" + charAt + "' at postion " + this.f34a);
                    }
            }
            this.f34a++;
        }
        if (str != null) {
            return str;
        }
        switch (this.c) {
            case 1:
            case 4:
            default:
                return str;
            case 2:
                throw new org.a.b.a.a.a.c("Trialing comma");
            case 3:
                String substring = this.d.substring(this.b);
                this.c = 6;
                return substring;
        }
    }
}
