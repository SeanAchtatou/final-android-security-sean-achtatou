package com.a.a.a;

import java.util.Iterator;
import org.a.b.a.a.a.c;

final class b {

    /* renamed from: a  reason: collision with root package name */
    private String f33a = null;

    b(byte[] bArr) {
        e eVar = new e(bArr);
        try {
            eVar.a();
            a(eVar);
        } catch (c e) {
        }
    }

    private void a(e eVar) {
        Iterator b = eVar.b();
        while (b.hasNext()) {
            d dVar = (d) b.next();
            if (dVar.b().equals("rspauth")) {
                this.f33a = dVar.a();
            }
        }
        if (this.f33a == null) {
            throw new c("Missing response-auth directive.");
        }
    }

    public final String a() {
        return this.f33a;
    }
}
