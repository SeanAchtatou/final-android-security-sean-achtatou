package com.a.a.a;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Map;
import org.a.b.a.a.a.b;
import org.a.b.a.a.a.c;
import org.a.b.a.a.a.d;
import org.a.b.a.a.b.a.e;
import org.a.b.a.a.b.a.f;
import org.a.b.a.a.b.a.g;

public final class a implements org.a.b.a.a.a.a {

    /* renamed from: a  reason: collision with root package name */
    private String f32a = "";
    private String b = "";
    private String c = "";
    private Map d;
    private g e;
    private int f;
    private String g = "";
    private char[] h = null;
    private String i;
    private f j;
    private String k = "";
    private String l = "";
    private String m = "";

    private a(String str, String str2, String str3, Map map, g gVar) {
        this.f32a = str;
        this.b = str2;
        this.c = str3;
        this.d = map;
        this.e = gVar;
        this.f = 0;
    }

    private static char a(byte b2) {
        switch (b2) {
            case 0:
                return '0';
            case 1:
                return '1';
            case 2:
                return '2';
            case 3:
                return '3';
            case 4:
                return '4';
            case 5:
                return '5';
            case 6:
                return '6';
            case 7:
                return '7';
            case 8:
                return '8';
            case 9:
                return '9';
            case 10:
                return 'a';
            case 11:
                return 'b';
            case 12:
                return 'c';
            case 13:
                return 'd';
            case 14:
                return 'e';
            case 15:
                return 'f';
            default:
                return 'Z';
        }
    }

    public static org.a.b.a.a.a.a a(String str, String str2, String str3, Map map, g gVar) {
        String str4 = (String) map.get("javax.security.sasl.qop");
        map.get("javax.security.sasl.strength");
        String str5 = (String) map.get("javax.security.sasl.server.authentication");
        if (str4 != null && !"auth".equals(str4)) {
            return null;
        }
        if (str5 != null && !"false".equals(str5)) {
            return null;
        }
        if (gVar == null) {
            return null;
        }
        return new a(str, str2, str3, map, gVar);
    }

    private static char[] a(String str, String str2, String str3, String str4, String str5, String str6) {
        byte[] bArr;
        try {
            MessageDigest instance = MessageDigest.getInstance("MD5");
            instance.update(str2.getBytes("UTF-8"));
            instance.update(":".getBytes("UTF-8"));
            instance.update(str3.getBytes("UTF-8"));
            instance.update(":".getBytes("UTF-8"));
            instance.update(str4.getBytes("UTF-8"));
            byte[] digest = instance.digest();
            if ("md5-sess".equals(str)) {
                instance.update(digest);
                instance.update(":".getBytes("UTF-8"));
                instance.update(str5.getBytes("UTF-8"));
                instance.update(":".getBytes("UTF-8"));
                instance.update(str6.getBytes("UTF-8"));
                bArr = instance.digest();
            } else {
                bArr = digest;
            }
            return b(bArr);
        } catch (NoSuchAlgorithmException e2) {
            throw new c("No provider found for MD5 hash", e2);
        } catch (UnsupportedEncodingException e3) {
            throw new c("UTF-8 encoding not supported by platform.", e3);
        }
    }

    private static char[] a(char[] cArr, String str, String str2, String str3, String str4, String str5, String str6, boolean z) {
        try {
            MessageDigest instance = MessageDigest.getInstance("MD5");
            if (z) {
                instance.update(str5.getBytes("UTF-8"));
            }
            instance.update(":".getBytes("UTF-8"));
            instance.update(str6.getBytes("UTF-8"));
            if ("auth-int".equals(str4)) {
                instance.update(":".getBytes("UTF-8"));
                instance.update("00000000000000000000000000000000".getBytes("UTF-8"));
            }
            char[] b2 = b(instance.digest());
            instance.update(new String(cArr).getBytes("UTF-8"));
            instance.update(":".getBytes("UTF-8"));
            instance.update(str.getBytes("UTF-8"));
            instance.update(":".getBytes("UTF-8"));
            if (str4.length() > 0) {
                instance.update(str2.getBytes("UTF-8"));
                instance.update(":".getBytes("UTF-8"));
                instance.update(str3.getBytes("UTF-8"));
                instance.update(":".getBytes("UTF-8"));
                instance.update(str4.getBytes("UTF-8"));
                instance.update(":".getBytes("UTF-8"));
            }
            instance.update(new String(b2).getBytes("UTF-8"));
            return b(instance.digest());
        } catch (NoSuchAlgorithmException e2) {
            throw new c("No provider found for MD5 hash", e2);
        } catch (UnsupportedEncodingException e3) {
            throw new c("UTF-8 encoding not supported by platform.", e3);
        }
    }

    private static String b() {
        byte[] bArr = new byte[32];
        char[] cArr = new char[64];
        try {
            SecureRandom.getInstance("SHA1PRNG").nextBytes(bArr);
            for (int i2 = 0; i2 < 32; i2++) {
                cArr[i2 * 2] = a((byte) (bArr[i2] & 15));
                cArr[(i2 * 2) + 1] = a((byte) ((bArr[i2] & 240) >> 4));
            }
            return new String(cArr);
        } catch (NoSuchAlgorithmException e2) {
            throw new c("No random number generator available", e2);
        }
    }

    private static char[] b(byte[] bArr) {
        char[] cArr = new char[32];
        for (int i2 = 0; i2 < 16; i2++) {
            cArr[i2 * 2] = a((byte) ((bArr[i2] & 240) >> 4));
            cArr[(i2 * 2) + 1] = a((byte) (bArr[i2] & 15));
        }
        return cArr;
    }

    private String c(byte[] bArr) {
        StringBuffer stringBuffer = new StringBuffer(512);
        this.j = new f(bArr);
        this.i = this.b + "/" + this.c;
        if ((this.j.c() & 1) == 1) {
            this.g = "auth";
            f[] fVarArr = new f[3];
            ArrayList a2 = this.j.a();
            int size = a2.size();
            if (size == 0) {
                fVarArr[0] = new b("Realm");
            } else if (size == 1) {
                fVarArr[0] = new b("Realm", (String) a2.get(0));
            } else {
                fVarArr[0] = new d("Realm", (String[]) a2.toArray(new String[size]));
            }
            fVarArr[1] = new org.a.b.a.a.b.a.c("Password");
            if (this.f32a == null || this.f32a.length() == 0) {
                fVarArr[2] = new e("Name");
            } else {
                fVarArr[2] = new e("Name", this.f32a);
            }
            try {
                this.e.a(fVarArr);
                if (size > 1) {
                    int[] b2 = ((d) fVarArr[0]).b();
                    if (b2.length > 0) {
                        this.l = ((d) fVarArr[0]).a()[b2[0]];
                    } else {
                        this.l = ((d) fVarArr[0]).a()[0];
                    }
                } else {
                    this.l = ((b) fVarArr[0]).a();
                }
                this.k = b();
                this.m = ((e) fVarArr[2]).b();
                if (this.m == null) {
                    this.m = ((e) fVarArr[2]).a();
                }
                if (this.m == null) {
                    throw new c("No user name was specified.");
                }
                this.h = a(this.j.d(), this.m, this.l, new String(((org.a.b.a.a.b.a.c) fVarArr[1]).a()), this.j.b(), this.k);
                char[] a3 = a(this.h, this.j.b(), "00000001", this.k, this.g, "AUTHENTICATE", this.i, true);
                stringBuffer.append("username=\"");
                stringBuffer.append(this.f32a);
                if (this.l.length() != 0) {
                    stringBuffer.append("\",realm=\"");
                    stringBuffer.append(this.l);
                }
                stringBuffer.append("\",cnonce=\"");
                stringBuffer.append(this.k);
                stringBuffer.append("\",nc=");
                stringBuffer.append("00000001");
                stringBuffer.append(",qop=");
                stringBuffer.append(this.g);
                stringBuffer.append(",digest-uri=\"");
                stringBuffer.append(this.i);
                stringBuffer.append("\",response=");
                stringBuffer.append(a3);
                stringBuffer.append(",charset=utf-8,nonce=\"");
                stringBuffer.append(this.j.b());
                stringBuffer.append("\"");
                return stringBuffer.toString();
            } catch (org.a.b.a.a.b.a.a e2) {
                throw new c("Handler does not support necessary callbacks", e2);
            } catch (IOException e3) {
                throw new c("IO exception in CallbackHandler.", e3);
            }
        } else {
            throw new c("Client only supports qop of 'auth'");
        }
    }

    public final boolean a() {
        return false;
    }

    public final byte[] a(byte[] bArr) {
        switch (this.f) {
            case 0:
                if (bArr.length == 0) {
                    throw new c("response = byte[0]");
                }
                try {
                    byte[] bytes = c(bArr).getBytes("UTF-8");
                    this.f = 1;
                    return bytes;
                } catch (UnsupportedEncodingException e2) {
                    throw new c("UTF-8 encoding not suppported by platform", e2);
                }
            case 1:
                if (new String(a(this.h, this.j.b(), "00000001", this.k, this.g, "AUTHENTICATE", this.i, false)).equals(new b(bArr).a())) {
                    this.f = 2;
                    return null;
                }
                this.f = 3;
                throw new c("Could not validate response-auth value from server");
            case 2:
            case 3:
                throw new c("Authentication sequence is complete");
            case 4:
                throw new c("Client has been disposed");
            default:
                throw new c("Unknown client state.");
        }
    }
}
