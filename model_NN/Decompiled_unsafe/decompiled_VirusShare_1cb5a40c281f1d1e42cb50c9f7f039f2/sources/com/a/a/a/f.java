package com.a.a.a;

import java.util.ArrayList;
import java.util.Iterator;
import org.a.b.a.a.a.c;

final class f {

    /* renamed from: a  reason: collision with root package name */
    private ArrayList f37a = new ArrayList(5);
    private String b = null;
    private int c = 0;
    private boolean d = false;
    private int e = -1;
    private String f = null;
    private String g = null;
    private int h = 0;

    f(byte[] bArr) {
        e eVar = new e(bArr);
        try {
            eVar.a();
            a(eVar);
        } catch (c e2) {
        }
    }

    private void a(d dVar) {
        if (this.h != 0) {
            throw new c("Too many cipher directives.");
        }
        c cVar = new c(dVar.a());
        cVar.a();
        for (String a2 = cVar.a(); a2 != null; a2 = cVar.a()) {
            if ("3des".equals(a2)) {
                this.h |= 1;
            } else if ("des".equals(a2)) {
                this.h |= 2;
            } else if ("rc4-40".equals(a2)) {
                this.h |= 4;
            } else if ("rc4".equals(a2)) {
                this.h |= 8;
            } else if ("rc4-56".equals(a2)) {
                this.h |= 16;
            } else {
                this.h |= 32;
            }
        }
        if (this.h == 0) {
            this.h = 32;
        }
    }

    private void a(e eVar) {
        Iterator b2 = eVar.b();
        while (b2.hasNext()) {
            d dVar = (d) b2.next();
            String b3 = dVar.b();
            if (b3.equals("realm")) {
                this.f37a.add(dVar.a());
            } else if (b3.equals("nonce")) {
                if (this.b != null) {
                    throw new c("Too many nonce values.");
                }
                this.b = dVar.a();
            } else if (b3.equals("qop")) {
                if (this.c != 0) {
                    throw new c("Too many qop directives.");
                }
                c cVar = new c(dVar.a());
                for (String a2 = cVar.a(); a2 != null; a2 = cVar.a()) {
                    if (a2.equals("auth")) {
                        this.c |= 1;
                    } else if (a2.equals("auth-int")) {
                        this.c |= 2;
                    } else if (a2.equals("auth-conf")) {
                        this.c |= 4;
                    } else {
                        this.c |= 8;
                    }
                }
            } else if (b3.equals("maxbuf")) {
                if (-1 != this.e) {
                    throw new c("Too many maxBuf directives.");
                }
                this.e = Integer.parseInt(dVar.a());
                if (this.e == 0) {
                    throw new c("Max buf value must be greater than zero.");
                }
            } else if (b3.equals("charset")) {
                if (this.f != null) {
                    throw new c("Too many charset directives.");
                }
                this.f = dVar.a();
                if (!this.f.equals("utf-8")) {
                    throw new c("Invalid character encoding directive");
                }
            } else if (b3.equals("algorithm")) {
                if (this.g != null) {
                    throw new c("Too many algorithm directives.");
                }
                this.g = dVar.a();
                if (!"md5-sess".equals(this.g)) {
                    throw new c("Invalid algorithm directive value: " + this.g);
                }
            } else if (b3.equals("cipher")) {
                a(dVar);
            } else if (!b3.equals("stale")) {
                continue;
            } else if (this.d) {
                throw new c("Too many stale directives.");
            } else if ("true".equals(dVar.a())) {
                this.d = true;
            } else {
                throw new c("Invalid stale directive value: " + dVar.a());
            }
        }
        if (-1 == this.e) {
            this.e = 65536;
        }
        if (this.c == 0) {
            this.c = 1;
        } else if ((this.c & 1) != 1) {
            throw new c("Only qop-auth is supported by client");
        } else if ((this.c & 4) == 4 && (this.h & 31) == 0) {
            throw new c("Invalid cipher options");
        } else if (this.b == null) {
            throw new c("Missing nonce directive");
        } else if (this.d) {
            throw new c("Unexpected stale flag");
        } else if (this.g == null) {
            throw new c("Missing algorithm directive");
        }
    }

    public final ArrayList a() {
        return this.f37a;
    }

    public final String b() {
        return this.b;
    }

    public final int c() {
        return this.c;
    }

    public final String d() {
        return this.g;
    }
}
