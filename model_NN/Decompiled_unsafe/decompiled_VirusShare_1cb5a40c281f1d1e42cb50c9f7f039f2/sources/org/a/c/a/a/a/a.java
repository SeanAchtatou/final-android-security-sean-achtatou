package org.a.c.a.a.a;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import org.a.b.a.a.a.c;
import org.a.b.a.a.b.a.e;
import org.a.b.a.a.b.a.f;
import org.a.b.a.a.b.a.g;

public final class a implements org.a.b.a.a.a.a {
    private static byte f = 0;

    /* renamed from: a  reason: collision with root package name */
    private boolean f644a = false;
    private g b;
    private String c;
    private String d;
    private byte[] e;

    public a(String str, g gVar) {
        this.b = gVar;
        Object[] c2 = c();
        this.c = str;
        this.d = (String) c2[0];
        this.e = (byte[]) c2[1];
        if (this.d == null || this.e == null) {
            throw new c("PLAIN: authenticationID and password must be specified");
        }
    }

    private void b() {
        if (this.e != null) {
            for (int i = 0; i < this.e.length; i++) {
                this.e[i] = 0;
            }
            this.e = null;
        }
    }

    private Object[] c() {
        byte[] bArr;
        try {
            e eVar = new e("PLAIN authentication id: ");
            org.a.b.a.a.b.a.c cVar = new org.a.b.a.a.b.a.c("PLAIN password: ");
            this.b.a(new f[]{eVar, cVar});
            String b2 = eVar.b();
            char[] a2 = cVar.a();
            if (a2 != null) {
                byte[] bytes = new String(a2).getBytes("UTF8");
                cVar.b();
                bArr = bytes;
            } else {
                bArr = null;
            }
            return new Object[]{b2, bArr};
        } catch (IOException e2) {
            throw new c("Cannot get password", e2);
        } catch (org.a.b.a.a.b.a.a e3) {
            throw new c("Cannot get userid/password", e3);
        }
    }

    public final boolean a() {
        return true;
    }

    public final byte[] a(byte[] bArr) {
        int i;
        if (this.f644a) {
            throw new IllegalStateException("PLAIN: authentication already completed");
        }
        this.f644a = true;
        try {
            byte[] bytes = this.c == null ? null : this.c.getBytes("UTF8");
            byte[] bytes2 = this.d.getBytes("UTF8");
            byte[] bArr2 = new byte[(this.e.length + bytes2.length + 2 + (bytes != null ? bytes.length : 0))];
            if (bytes != null) {
                System.arraycopy(bytes, 0, bArr2, 0, bytes.length);
                i = bytes.length;
            } else {
                i = 0;
            }
            int i2 = i + 1;
            bArr2[i] = f;
            System.arraycopy(bytes2, 0, bArr2, i2, bytes2.length);
            int length = bytes2.length + i2;
            bArr2[length] = f;
            System.arraycopy(this.e, 0, bArr2, length + 1, this.e.length);
            b();
            return bArr2;
        } catch (UnsupportedEncodingException e2) {
            throw new c("PLAIN: Cannot get UTF-8 encoding of ids", e2);
        }
    }

    /* access modifiers changed from: protected */
    public final void finalize() {
        b();
    }
}
