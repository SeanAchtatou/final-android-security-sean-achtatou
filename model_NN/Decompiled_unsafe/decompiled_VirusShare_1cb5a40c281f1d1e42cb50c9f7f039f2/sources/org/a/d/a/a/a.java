package org.a.d.a.a;

public final class a {

    /* renamed from: a  reason: collision with root package name */
    private static byte[] f645a = "\r\n".getBytes();
    private static byte[] b = new byte[255];
    private static byte[] c = new byte[64];

    static {
        for (int i = 0; i < 255; i++) {
            b[i] = -1;
        }
        for (int i2 = 90; i2 >= 65; i2--) {
            b[i2] = (byte) (i2 - 65);
        }
        for (int i3 = 122; i3 >= 97; i3--) {
            b[i3] = (byte) ((i3 - 97) + 26);
        }
        for (int i4 = 57; i4 >= 48; i4--) {
            b[i4] = (byte) ((i4 - 48) + 52);
        }
        b[43] = 62;
        b[47] = 63;
        for (int i5 = 0; i5 <= 25; i5++) {
            c[i5] = (byte) (i5 + 65);
        }
        int i6 = 26;
        int i7 = 0;
        while (i6 <= 51) {
            c[i6] = (byte) (i7 + 97);
            i6++;
            i7++;
        }
        int i8 = 52;
        int i9 = 0;
        while (i8 <= 61) {
            c[i8] = (byte) (i9 + 48);
            i8++;
            i9++;
        }
        c[62] = 43;
        c[63] = 47;
    }

    public static byte[] a(byte[] bArr) {
        int i = 0;
        int length = bArr.length * 8;
        int i2 = length % 24;
        int i3 = length / 24;
        byte[] bArr2 = new byte[(i2 != 0 ? (i3 + 1) * 4 : i3 * 4)];
        int i4 = 0;
        while (i < i3) {
            int i5 = i * 3;
            byte b2 = bArr[i5];
            byte b3 = bArr[i5 + 1];
            byte b4 = bArr[i5 + 2];
            byte b5 = (byte) (b3 & 15);
            byte b6 = (byte) (b2 & 3);
            byte b7 = (b2 & Byte.MIN_VALUE) == 0 ? (byte) (b2 >> 2) : (byte) ((b2 >> 2) ^ 192);
            byte b8 = (b3 & Byte.MIN_VALUE) == 0 ? (byte) (b3 >> 4) : (byte) ((b3 >> 4) ^ 240);
            int i6 = (b4 & Byte.MIN_VALUE) == 0 ? b4 >> 6 : (b4 >> 6) ^ 252;
            bArr2[i4] = c[b7];
            bArr2[i4 + 1] = c[b8 | (b6 << 4)];
            bArr2[i4 + 2] = c[(b5 << 2) | ((byte) i6)];
            bArr2[i4 + 3] = c[b4 & 63];
            i4 += 4;
            i++;
        }
        int i7 = i * 3;
        if (i2 == 8) {
            byte b9 = bArr[i7];
            byte b10 = (byte) (b9 & 3);
            bArr2[i4] = c[(b9 & Byte.MIN_VALUE) == 0 ? (byte) (b9 >> 2) : (byte) ((b9 >> 2) ^ 192)];
            bArr2[i4 + 1] = c[b10 << 4];
            bArr2[i4 + 2] = 61;
            bArr2[i4 + 3] = 61;
        } else if (i2 == 16) {
            byte b11 = bArr[i7];
            byte b12 = bArr[i7 + 1];
            byte b13 = (byte) (b12 & 15);
            byte b14 = (byte) (b11 & 3);
            byte b15 = (b11 & Byte.MIN_VALUE) == 0 ? (byte) (b11 >> 2) : (byte) ((b11 >> 2) ^ 192);
            byte b16 = (b12 & Byte.MIN_VALUE) == 0 ? (byte) (b12 >> 4) : (byte) ((b12 >> 4) ^ 240);
            bArr2[i4] = c[b15];
            bArr2[i4 + 1] = c[b16 | (b14 << 4)];
            bArr2[i4 + 2] = c[b13 << 2];
            bArr2[i4 + 3] = 61;
        }
        return bArr2;
    }

    public static byte[] b(byte[] bArr) {
        byte[] bArr2 = new byte[bArr.length];
        int i = 0;
        for (int i2 = 0; i2 < bArr.length; i2++) {
            byte b2 = bArr[i2];
            if (b2 == 61 || b[b2] != -1) {
                bArr2[i] = bArr[i2];
                i++;
            }
        }
        byte[] bArr3 = new byte[i];
        System.arraycopy(bArr2, 0, bArr3, 0, i);
        if (bArr3.length == 0) {
            return new byte[0];
        }
        int length = bArr3.length / 4;
        int length2 = bArr3.length;
        while (bArr3[length2 - 1] == 61) {
            length2--;
            if (length2 == 0) {
                return new byte[0];
            }
        }
        byte[] bArr4 = new byte[(length2 - length)];
        int i3 = 0;
        for (int i4 = 0; i4 < length; i4++) {
            int i5 = i4 * 4;
            byte b3 = bArr3[i5 + 2];
            byte b4 = bArr3[i5 + 3];
            byte b5 = b[bArr3[i5]];
            byte b6 = b[bArr3[i5 + 1]];
            if (b3 != 61 && b4 != 61) {
                byte b7 = b[b3];
                byte b8 = b[b4];
                bArr4[i3] = (byte) ((b5 << 2) | (b6 >> 4));
                bArr4[i3 + 1] = (byte) (((b6 & 15) << 4) | ((b7 >> 2) & 15));
                bArr4[i3 + 2] = (byte) ((b7 << 6) | b8);
            } else if (b3 == 61) {
                bArr4[i3] = (byte) ((b6 >> 4) | (b5 << 2));
            } else if (b4 == 61) {
                byte b9 = b[b3];
                bArr4[i3] = (byte) ((b5 << 2) | (b6 >> 4));
                bArr4[i3 + 1] = (byte) (((b6 & 15) << 4) | ((b9 >> 2) & 15));
            }
            i3 += 3;
        }
        return bArr4;
    }
}
