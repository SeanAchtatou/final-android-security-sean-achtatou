package org.a.b.a.a.a;

import java.io.IOException;

public final class c extends IOException {

    /* renamed from: a  reason: collision with root package name */
    private Throwable f638a;

    public c() {
    }

    public c(String str) {
        super(str);
    }

    public c(String str, Throwable th) {
        super(str);
        if (th != null) {
            super.initCause(th);
            this.f638a = th;
        }
    }

    public final Throwable getCause() {
        return this.f638a;
    }

    public final Throwable initCause(Throwable th) {
        super.initCause(th);
        this.f638a = th;
        return this;
    }

    public final String toString() {
        if (this.f638a == null) {
            return super.toString();
        }
        return super.toString() + ", caused by: " + this.f638a.toString();
    }
}
