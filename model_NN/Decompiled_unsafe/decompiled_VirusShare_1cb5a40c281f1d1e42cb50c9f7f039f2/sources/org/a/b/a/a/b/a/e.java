package org.a.b.a.a.b.a;

import java.io.Serializable;

public final class e implements Serializable, f {

    /* renamed from: a  reason: collision with root package name */
    private String f643a;
    private String b;
    private String c;

    public e(String str) {
        b(str);
    }

    public e(String str, String str2) {
        b(str);
        if (str2 == null || str2.length() == 0) {
            throw new IllegalArgumentException("auth.1E");
        }
        this.b = str2;
    }

    private void b(String str) {
        if (str == null || str.length() == 0) {
            throw new IllegalArgumentException("auth.14");
        }
        this.f643a = str;
    }

    public final String a() {
        return this.b;
    }

    public final void a(String str) {
        this.c = str;
    }

    public final String b() {
        return this.c;
    }
}
