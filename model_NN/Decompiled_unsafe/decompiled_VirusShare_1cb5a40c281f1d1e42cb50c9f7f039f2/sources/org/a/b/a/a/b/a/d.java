package org.a.b.a.a.b.a;

import java.io.Serializable;

public class d implements Serializable, f {

    /* renamed from: a  reason: collision with root package name */
    private String f642a;
    private String b;
    private String c;

    public d(String str) {
        b(str);
    }

    public d(String str, String str2) {
        b(str);
        if (str2 == null || str2.length() == 0) {
            throw new IllegalArgumentException("auth.15");
        }
        this.f642a = str2;
    }

    private void b(String str) {
        if (str == null || str.length() == 0) {
            throw new IllegalArgumentException("auth.14");
        }
        this.b = str;
    }

    public final String a() {
        return this.c;
    }

    public final void a(String str) {
        this.c = str;
    }
}
