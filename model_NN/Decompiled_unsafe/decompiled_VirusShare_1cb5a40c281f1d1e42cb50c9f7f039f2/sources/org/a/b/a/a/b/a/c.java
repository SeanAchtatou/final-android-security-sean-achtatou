package org.a.b.a.a.b.a;

import java.io.Serializable;
import java.util.Arrays;

public final class c implements Serializable, f {

    /* renamed from: a  reason: collision with root package name */
    private String f641a;
    private boolean b;
    private char[] c;

    public c(String str) {
        if (str == null || str.length() == 0) {
            throw new IllegalArgumentException("auth.14");
        }
        this.f641a = str;
        this.b = false;
    }

    public final void a(char[] cArr) {
        if (cArr == null) {
            this.c = cArr;
            return;
        }
        this.c = new char[cArr.length];
        System.arraycopy(cArr, 0, this.c, 0, this.c.length);
    }

    public final char[] a() {
        if (this.c == null) {
            return null;
        }
        char[] cArr = new char[this.c.length];
        System.arraycopy(this.c, 0, cArr, 0, cArr.length);
        return cArr;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Arrays.fill(char[], char):void}
     arg types: [char[], int]
     candidates:
      ClspMth{java.util.Arrays.fill(double[], double):void}
      ClspMth{java.util.Arrays.fill(byte[], byte):void}
      ClspMth{java.util.Arrays.fill(long[], long):void}
      ClspMth{java.util.Arrays.fill(boolean[], boolean):void}
      ClspMth{java.util.Arrays.fill(short[], short):void}
      ClspMth{java.util.Arrays.fill(java.lang.Object[], java.lang.Object):void}
      ClspMth{java.util.Arrays.fill(int[], int):void}
      ClspMth{java.util.Arrays.fill(float[], float):void}
      ClspMth{java.util.Arrays.fill(char[], char):void} */
    public final void b() {
        if (this.c != null) {
            Arrays.fill(this.c, 0);
        }
    }
}
