package org.a.b.a.a.b.a;

import java.io.Serializable;

public class b implements Serializable, f {

    /* renamed from: a  reason: collision with root package name */
    private int f640a;
    private String b;
    private boolean c;
    private String[] d;
    private int[] e;

    public b(String str, String[] strArr) {
        if (str == null || str.length() == 0) {
            throw new IllegalArgumentException("auth.14");
        }
        this.b = str;
        a(strArr);
        if (this.d.length <= 0) {
            throw new IllegalArgumentException("auth.1D");
        }
        this.f640a = 0;
        this.c = false;
    }

    private void a(String[] strArr) {
        if (strArr == null || strArr.length == 0) {
            throw new IllegalArgumentException("auth.1C");
        }
        for (int i = 0; i < strArr.length; i++) {
            if (strArr[i] == null || strArr[i].length() == 0) {
                throw new IllegalArgumentException("auth.1C");
            }
        }
        this.d = strArr;
    }

    public final String[] a() {
        return this.d;
    }

    public final int[] b() {
        return this.e;
    }
}
