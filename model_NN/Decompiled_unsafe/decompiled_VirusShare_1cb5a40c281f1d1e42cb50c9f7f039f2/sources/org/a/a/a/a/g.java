package org.a.a.a.a;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.Random;
import org.a.a.a.a.a.d;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.message.BasicHeader;

public class g implements HttpEntity {

    /* renamed from: a  reason: collision with root package name */
    private static final char[] f636a = "-_1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();
    private final h b;
    private final Header c;
    private long d;
    private volatile boolean e;

    public g() {
        this(e.STRICT);
    }

    private g(e eVar) {
        String a2 = a();
        this.b = new h("form-data", a2);
        this.c = new BasicHeader("Content-Type", "multipart/form-data; boundary=" + a2);
        this.e = true;
        this.b.a(eVar == null ? e.STRICT : eVar);
    }

    private static String a() {
        StringBuilder sb = new StringBuilder();
        Random random = new Random();
        int nextInt = random.nextInt(11) + 30;
        for (int i = 0; i < nextInt; i++) {
            sb.append(f636a[random.nextInt(f636a.length)]);
        }
        return sb.toString();
    }

    public final void a(String str, d dVar) {
        this.b.a(new a(str, dVar));
        this.e = true;
    }

    public void consumeContent() {
        if (isStreaming()) {
            throw new UnsupportedOperationException("Streaming entity does not implement #consumeContent()");
        }
    }

    public InputStream getContent() {
        throw new UnsupportedOperationException("Multipart form entity does not implement #getContent()");
    }

    public Header getContentEncoding() {
        return null;
    }

    public long getContentLength() {
        if (this.e) {
            this.d = this.b.b();
            this.e = false;
        }
        return this.d;
    }

    public Header getContentType() {
        return this.c;
    }

    public boolean isChunked() {
        return !isRepeatable();
    }

    public boolean isRepeatable() {
        for (a a2 : this.b.a()) {
            if (a2.a().d() < 0) {
                return false;
            }
        }
        return true;
    }

    public boolean isStreaming() {
        return !isRepeatable();
    }

    public void writeTo(OutputStream outputStream) {
        this.b.a(outputStream);
    }
}
