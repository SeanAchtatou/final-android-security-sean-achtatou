package org.a.a.a.a;

public final class c {

    /* renamed from: a  reason: collision with root package name */
    private final String f632a;
    private final String b;

    c(String str, String str2) {
        this.f632a = str;
        this.b = str2;
    }

    public final String a() {
        return this.f632a;
    }

    public final String b() {
        return this.b;
    }

    public final String toString() {
        return this.f632a + ": " + this.b;
    }
}
