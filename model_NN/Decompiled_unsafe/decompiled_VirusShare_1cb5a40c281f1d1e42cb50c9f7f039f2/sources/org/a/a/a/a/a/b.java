package org.a.a.a.a.a;

import java.io.ByteArrayInputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;

public final class b extends a {

    /* renamed from: a  reason: collision with root package name */
    private final byte[] f629a;
    private final Charset b;

    public b(String str) {
        this(str, "text/plain");
    }

    private b(String str, String str2) {
        super(str2);
        if (str == null) {
            throw new IllegalArgumentException("Text may not be null");
        }
        Charset defaultCharset = Charset.defaultCharset();
        this.f629a = str.getBytes(defaultCharset.name());
        this.b = defaultCharset;
    }

    public final void a(OutputStream outputStream) {
        if (outputStream == null) {
            throw new IllegalArgumentException("Output stream may not be null");
        }
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(this.f629a);
        byte[] bArr = new byte[4096];
        while (true) {
            int read = byteArrayInputStream.read(bArr);
            if (read != -1) {
                outputStream.write(bArr, 0, read);
            } else {
                outputStream.flush();
                return;
            }
        }
    }

    public final String b() {
        return "8bit";
    }

    public final String c() {
        return this.b.name();
    }

    public final long d() {
        return (long) this.f629a.length;
    }

    public final String e() {
        return null;
    }
}
