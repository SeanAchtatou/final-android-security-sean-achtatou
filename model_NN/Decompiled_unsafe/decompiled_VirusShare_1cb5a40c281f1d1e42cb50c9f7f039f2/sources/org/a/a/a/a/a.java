package org.a.a.a.a;

import org.a.a.a.a.a.d;

public final class a {

    /* renamed from: a  reason: collision with root package name */
    private final String f627a;
    private final b b;
    private d c;

    public a(String str, d dVar) {
        if (str == null) {
            throw new IllegalArgumentException("Name may not be null");
        } else if (dVar == null) {
            throw new IllegalArgumentException("Body may not be null");
        } else {
            this.f627a = str;
            this.c = dVar;
            this.b = new b();
            StringBuilder sb = new StringBuilder();
            sb.append("form-data; name=\"");
            sb.append(this.f627a);
            sb.append("\"");
            if (dVar.e() != null) {
                sb.append("; filename=\"");
                sb.append(dVar.e());
                sb.append("\"");
            }
            a("Content-Disposition", sb.toString());
            if (dVar.a() != null) {
                StringBuilder sb2 = new StringBuilder();
                sb2.append(dVar.a());
                if (dVar.c() != null) {
                    sb2.append("; charset=");
                    sb2.append(dVar.c());
                }
                a("Content-Type", sb2.toString());
            }
            if (dVar.b() != null) {
                a("Content-Transfer-Encoding", dVar.b());
            }
        }
    }

    private void a(String str, String str2) {
        if (str == null) {
            throw new IllegalArgumentException("Field name may not be null");
        }
        this.b.a(new c(str, str2));
    }

    public final d a() {
        return this.c;
    }

    public final b b() {
        return this.b;
    }
}
