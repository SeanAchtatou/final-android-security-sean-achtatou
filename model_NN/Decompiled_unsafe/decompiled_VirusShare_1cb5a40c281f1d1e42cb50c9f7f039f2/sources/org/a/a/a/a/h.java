package org.a.a.a.a;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.apache.http.util.ByteArrayBuffer;

public final class h {

    /* renamed from: a  reason: collision with root package name */
    private static final ByteArrayBuffer f637a = a(f.f635a, ": ");
    private static final ByteArrayBuffer b = a(f.f635a, "\r\n");
    private static final ByteArrayBuffer c = a(f.f635a, "--");
    private final String d;
    private final Charset e;
    private final String f;
    private final List g;
    private e h;

    public h(String str, String str2) {
        if (str == null) {
            throw new IllegalArgumentException("Multipart subtype may not be null");
        } else if (str2 == null) {
            throw new IllegalArgumentException("Multipart boundary may not be null");
        } else {
            this.d = str;
            this.e = f.f635a;
            this.f = str2;
            this.g = new ArrayList();
            this.h = e.STRICT;
        }
    }

    private static ByteArrayBuffer a(Charset charset, String str) {
        ByteBuffer encode = charset.encode(CharBuffer.wrap(str));
        ByteArrayBuffer byteArrayBuffer = new ByteArrayBuffer(encode.remaining());
        byteArrayBuffer.append(encode.array(), encode.position(), encode.remaining());
        return byteArrayBuffer;
    }

    private static void a(String str, OutputStream outputStream) {
        a(a(f.f635a, str), outputStream);
    }

    private static void a(String str, Charset charset, OutputStream outputStream) {
        a(a(charset, str), outputStream);
    }

    private static void a(c cVar, Charset charset, OutputStream outputStream) {
        a(cVar.a(), charset, outputStream);
        a(f637a, outputStream);
        a(cVar.b(), charset, outputStream);
        a(b, outputStream);
    }

    private void a(e eVar, OutputStream outputStream, boolean z) {
        ByteArrayBuffer a2 = a(this.e, this.f);
        for (a aVar : this.g) {
            a(c, outputStream);
            a(a2, outputStream);
            a(b, outputStream);
            b b2 = aVar.b();
            switch (d.f633a[eVar.ordinal()]) {
                case 1:
                    Iterator it = b2.iterator();
                    while (it.hasNext()) {
                        c cVar = (c) it.next();
                        a(cVar.a(), outputStream);
                        a(f637a, outputStream);
                        a(cVar.b(), outputStream);
                        a(b, outputStream);
                    }
                    break;
                case 2:
                    a(aVar.b().a("Content-Disposition"), this.e, outputStream);
                    if (aVar.a().e() != null) {
                        a(aVar.b().a("Content-Type"), this.e, outputStream);
                        break;
                    }
                    break;
            }
            a(b, outputStream);
            if (z) {
                aVar.a().a(outputStream);
            }
            a(b, outputStream);
        }
        a(c, outputStream);
        a(a2, outputStream);
        a(c, outputStream);
        a(b, outputStream);
    }

    private static void a(ByteArrayBuffer byteArrayBuffer, OutputStream outputStream) {
        outputStream.write(byteArrayBuffer.buffer(), 0, byteArrayBuffer.length());
    }

    public final List a() {
        return this.g;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.a.a.a.a.h.a(org.a.a.a.a.e, java.io.OutputStream, boolean):void
     arg types: [org.a.a.a.a.e, java.io.OutputStream, int]
     candidates:
      org.a.a.a.a.h.a(java.lang.String, java.nio.charset.Charset, java.io.OutputStream):void
      org.a.a.a.a.h.a(org.a.a.a.a.c, java.nio.charset.Charset, java.io.OutputStream):void
      org.a.a.a.a.h.a(org.a.a.a.a.e, java.io.OutputStream, boolean):void */
    public final void a(OutputStream outputStream) {
        a(this.h, outputStream, true);
    }

    public final void a(a aVar) {
        if (aVar != null) {
            this.g.add(aVar);
        }
    }

    public final void a(e eVar) {
        this.h = eVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.a.a.a.a.h.a(org.a.a.a.a.e, java.io.OutputStream, boolean):void
     arg types: [org.a.a.a.a.e, java.io.ByteArrayOutputStream, int]
     candidates:
      org.a.a.a.a.h.a(java.lang.String, java.nio.charset.Charset, java.io.OutputStream):void
      org.a.a.a.a.h.a(org.a.a.a.a.c, java.nio.charset.Charset, java.io.OutputStream):void
      org.a.a.a.a.h.a(org.a.a.a.a.e, java.io.OutputStream, boolean):void */
    public final long b() {
        long j = 0;
        for (a a2 : this.g) {
            long d2 = a2.a().d();
            if (d2 < 0) {
                return -1;
            }
            j += d2;
        }
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try {
            a(this.h, (OutputStream) byteArrayOutputStream, false);
            return ((long) byteArrayOutputStream.toByteArray().length) + j;
        } catch (IOException e2) {
            return -1;
        }
    }
}
