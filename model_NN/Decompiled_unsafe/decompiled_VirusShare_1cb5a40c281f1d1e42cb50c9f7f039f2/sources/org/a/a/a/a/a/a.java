package org.a.a.a.a.a;

public abstract class a implements d {

    /* renamed from: a  reason: collision with root package name */
    private final String f628a;
    private final String b;
    private final String c;

    public a(String str) {
        if (str == null) {
            throw new IllegalArgumentException("MIME type may not be null");
        }
        this.f628a = str;
        int indexOf = str.indexOf(47);
        if (indexOf != -1) {
            this.b = str.substring(0, indexOf);
            this.c = str.substring(indexOf + 1);
            return;
        }
        this.b = str;
        this.c = null;
    }

    public final String a() {
        return this.f628a;
    }
}
