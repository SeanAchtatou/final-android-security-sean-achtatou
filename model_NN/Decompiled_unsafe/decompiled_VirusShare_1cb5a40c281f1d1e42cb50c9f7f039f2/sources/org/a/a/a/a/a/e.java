package org.a.a.a.a.a;

import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;

public final class e extends a {

    /* renamed from: a  reason: collision with root package name */
    private final File f630a;
    private final String b;
    private final String c;

    public e(File file) {
        this(file, "application/octet-stream");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.a.a.a.a.a.e.<init>(java.io.File, java.lang.String, char):void
     arg types: [java.io.File, java.lang.String, int]
     candidates:
      org.a.a.a.a.a.e.<init>(java.io.File, java.lang.String, byte):void
      org.a.a.a.a.a.e.<init>(java.io.File, java.lang.String, char):void */
    private e(File file, String str) {
        this(file, str, 0);
    }

    private e(File file, String str, byte b2) {
        super(str);
        if (file == null) {
            throw new IllegalArgumentException("File may not be null");
        }
        this.f630a = file;
        this.b = file.getName();
        this.c = null;
    }

    private e(File file, String str, char c2) {
        this(file, str, (byte) 0);
    }

    public final void a(OutputStream outputStream) {
        if (outputStream == null) {
            throw new IllegalArgumentException("Output stream may not be null");
        }
        FileInputStream fileInputStream = new FileInputStream(this.f630a);
        try {
            byte[] bArr = new byte[4096];
            while (true) {
                int read = fileInputStream.read(bArr);
                if (read != -1) {
                    outputStream.write(bArr, 0, read);
                } else {
                    outputStream.flush();
                    return;
                }
            }
        } finally {
            fileInputStream.close();
        }
    }

    public final String b() {
        return "binary";
    }

    public final String c() {
        return this.c;
    }

    public final long d() {
        return this.f630a.length();
    }

    public final String e() {
        return this.b;
    }
}
