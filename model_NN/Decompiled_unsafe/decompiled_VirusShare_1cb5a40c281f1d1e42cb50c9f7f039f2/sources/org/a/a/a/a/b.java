package org.a.a.a.a;

import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public final class b implements Iterable {

    /* renamed from: a  reason: collision with root package name */
    private final List f631a = new LinkedList();
    private final Map b = new HashMap();

    public final c a(String str) {
        if (str == null) {
            return null;
        }
        List list = (List) this.b.get(str.toLowerCase(Locale.US));
        if (list == null || list.isEmpty()) {
            return null;
        }
        return (c) list.get(0);
    }

    public final void a(c cVar) {
        if (cVar != null) {
            String lowerCase = cVar.a().toLowerCase(Locale.US);
            Object obj = (List) this.b.get(lowerCase);
            if (obj == null) {
                obj = new LinkedList();
                this.b.put(lowerCase, obj);
            }
            obj.add(cVar);
            this.f631a.add(cVar);
        }
    }

    public final Iterator iterator() {
        return Collections.unmodifiableList(this.f631a).iterator();
    }

    public final String toString() {
        return this.f631a.toString();
    }
}
