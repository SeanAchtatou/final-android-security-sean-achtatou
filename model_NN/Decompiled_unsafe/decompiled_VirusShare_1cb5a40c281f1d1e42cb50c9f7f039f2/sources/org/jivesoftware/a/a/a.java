package org.jivesoftware.a.a;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import org.jivesoftware.smack.b.f;

public final class a implements f {

    /* renamed from: a  reason: collision with root package name */
    private static SimpleDateFormat f647a = new SimpleDateFormat("yyyyMMdd'T'HH:mm:ss");
    private static SimpleDateFormat b = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
    private Date c;
    private String d;
    private String e;

    static {
        f647a.setTimeZone(TimeZone.getTimeZone("GMT+0"));
        b.setTimeZone(TimeZone.getTimeZone("UTC"));
    }

    public final String a() {
        return "x";
    }

    public final String b() {
        return "jabber:x:delay";
    }

    public final String c() {
        StringBuilder sb = new StringBuilder();
        sb.append("<").append("x").append(" xmlns=\"").append("jabber:x:delay").append("\"");
        sb.append(" stamp=\"");
        synchronized (f647a) {
            sb.append(f647a.format(this.c));
        }
        sb.append("\"");
        if (this.d != null && this.d.length() > 0) {
            sb.append(" from=\"").append(this.d).append("\"");
        }
        sb.append(">");
        if (this.e != null && this.e.length() > 0) {
            sb.append(this.e);
        }
        sb.append("</").append("x").append(">");
        return sb.toString();
    }

    public final Date d() {
        return this.c;
    }
}
