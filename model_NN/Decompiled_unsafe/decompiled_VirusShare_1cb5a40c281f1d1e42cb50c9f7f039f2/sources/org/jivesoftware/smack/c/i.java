package org.jivesoftware.smack.c;

import org.jivesoftware.smack.b.w;

public final class i extends w {

    /* renamed from: a  reason: collision with root package name */
    private final String f684a;

    public i(String str) {
        this.f684a = str;
    }

    public final String b() {
        return this.f684a;
    }

    public final String b_() {
        StringBuilder sb = new StringBuilder();
        sb.append("<failure xmlns=\"urn:ietf:params:xml:ns:xmpp-sasl\">");
        if (this.f684a != null && this.f684a.trim().length() > 0) {
            sb.append("<").append(this.f684a).append("/>");
        }
        sb.append("</failure>");
        return sb.toString();
    }
}
