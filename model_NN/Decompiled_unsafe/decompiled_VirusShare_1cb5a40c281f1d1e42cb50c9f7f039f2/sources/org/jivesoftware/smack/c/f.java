package org.jivesoftware.smack.c;

import org.jivesoftware.smack.b.w;

public final class f extends w {

    /* renamed from: a  reason: collision with root package name */
    private final String f681a;

    public f(String str) {
        this.f681a = str;
    }

    public final String b_() {
        StringBuilder sb = new StringBuilder();
        sb.append("<challenge xmlns=\"urn:ietf:params:xml:ns:xmpp-sasl\">");
        if (this.f681a != null && this.f681a.trim().length() > 0) {
            sb.append(this.f681a);
        }
        sb.append("</challenge>");
        return sb.toString();
    }
}
