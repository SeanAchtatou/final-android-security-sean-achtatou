package org.jivesoftware.smack.c;

import org.jivesoftware.smack.b.w;

public final class k extends w {

    /* renamed from: a  reason: collision with root package name */
    private final String f685a;
    private final String b;
    private /* synthetic */ g c;

    public k(g gVar, String str, String str2) {
        this.c = gVar;
        if (str == null) {
            throw new NullPointerException("SASL mechanism name shouldn't be null.");
        }
        this.f685a = str;
        this.b = str2;
    }

    public final String b_() {
        StringBuilder sb = new StringBuilder();
        sb.append("<auth mechanism=\"").append(this.f685a);
        sb.append("\" xmlns=\"urn:ietf:params:xml:ns:xmpp-sasl\">");
        if (this.b != null && this.b.trim().length() > 0) {
            sb.append(this.b);
        }
        sb.append("</auth>");
        return sb.toString();
    }
}
