package org.jivesoftware.smack.c;

import org.jivesoftware.smack.b.w;

public final class h extends w {

    /* renamed from: a  reason: collision with root package name */
    private final String f683a;
    private /* synthetic */ g b;

    public h(g gVar) {
        this.b = gVar;
        this.f683a = null;
    }

    public h(g gVar, String str) {
        this.b = gVar;
        if (str == null || str.trim().length() == 0) {
            this.f683a = null;
        } else {
            this.f683a = str;
        }
    }

    public final String b_() {
        StringBuilder sb = new StringBuilder();
        sb.append("<response xmlns=\"urn:ietf:params:xml:ns:xmpp-sasl\">");
        if (this.f683a != null) {
            sb.append(this.f683a);
        }
        sb.append("</response>");
        return sb.toString();
    }
}
