package org.jivesoftware.smack.c;

import a.a.a.f;
import java.util.HashMap;
import org.a.b.a.a.a.a;
import org.a.b.a.a.a.b;
import org.a.b.a.a.a.d;
import org.a.b.a.a.b.a.c;
import org.a.b.a.a.b.a.e;
import org.jivesoftware.smack.ad;
import org.jivesoftware.smack.k;

public abstract class g implements org.a.b.a.a.b.a.g {

    /* renamed from: a  reason: collision with root package name */
    protected a f682a;
    private k b;
    private String c;
    private String d;
    private String e;

    /* access modifiers changed from: protected */
    public abstract String a();

    public void a(String str) {
        byte[] a2 = str != null ? this.f682a.a(org.jivesoftware.smack.e.g.a(str)) : this.f682a.a(new byte[0]);
        this.b.a(a2 == null ? new h(this) : new h(this, org.jivesoftware.smack.e.g.b(a2)));
    }

    public void a(String str, String str2, String str3) {
        this.c = str;
        this.d = str3;
        this.e = str2;
        this.f682a = f.a(new String[]{a()}, str, "xmpp", str2, new HashMap(), this);
        b();
    }

    public void a(String str, String str2, org.a.b.a.a.b.a.g gVar) {
        this.f682a = f.a(new String[]{a()}, str, "xmpp", str2, new HashMap(), gVar);
        b();
    }

    public final void a(org.a.b.a.a.b.a.f[] fVarArr) {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < fVarArr.length) {
                if (fVarArr[i2] instanceof e) {
                    ((e) fVarArr[i2]).a(this.c);
                } else if (fVarArr[i2] instanceof c) {
                    ((c) fVarArr[i2]).a(this.d.toCharArray());
                } else if (fVarArr[i2] instanceof b) {
                    ((b) fVarArr[i2]).a(this.e);
                } else if (!(fVarArr[i2] instanceof d)) {
                    throw new org.a.b.a.a.b.a.a(fVarArr[i2]);
                }
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void b() {
        String str = null;
        try {
            if (this.f682a.a()) {
                str = org.jivesoftware.smack.e.g.b(this.f682a.a(new byte[0]));
            }
            this.b.a(new k(this, a(), str));
        } catch (org.a.b.a.a.a.c e2) {
            throw new ad("SASL authentication failed", e2);
        }
    }

    /* access modifiers changed from: protected */
    public final k c() {
        return this.b;
    }
}
