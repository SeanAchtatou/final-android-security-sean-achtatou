package org.jivesoftware.smack.c;

import org.jivesoftware.smack.b.w;

public final class d extends w {

    /* renamed from: a  reason: collision with root package name */
    private final String f680a;

    public d(String str) {
        this.f680a = str;
    }

    public final String b_() {
        StringBuilder sb = new StringBuilder();
        sb.append("<success xmlns=\"urn:ietf:params:xml:ns:xmpp-sasl\">");
        if (this.f680a != null && this.f680a.trim().length() > 0) {
            sb.append(this.f680a);
        }
        sb.append("</success>");
        return sb.toString();
    }
}
