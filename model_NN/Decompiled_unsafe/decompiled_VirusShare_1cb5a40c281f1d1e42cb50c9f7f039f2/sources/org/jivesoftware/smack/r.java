package org.jivesoftware.smack;

import java.util.ArrayList;
import org.jivesoftware.smack.b.b;
import org.jivesoftware.smack.b.c;
import org.jivesoftware.smack.b.t;
import org.jivesoftware.smack.b.w;
import org.jivesoftware.smack.e.h;

final class r implements ab {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ ac f709a;

    /* synthetic */ r(ac acVar) {
        this(acVar, (byte) 0);
    }

    private r(ac acVar, byte b) {
        this.f709a = acVar;
    }

    public final void a(w wVar) {
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        ArrayList arrayList3 = new ArrayList();
        for (b bVar : ((t) wVar).c()) {
            p pVar = new p(bVar.a(), bVar.b(), bVar.c(), bVar.d(), this.f709a.c);
            if (c.remove.equals(bVar.c())) {
                if (this.f709a.e.containsKey(bVar.a())) {
                    this.f709a.e.remove(bVar.a());
                }
                if (this.f709a.f.contains(pVar)) {
                    this.f709a.f.remove(pVar);
                }
                this.f709a.h.remove(h.a(bVar.a()) + "@" + h.b(bVar.a()));
                arrayList3.add(bVar.a());
            } else {
                if (!this.f709a.e.containsKey(bVar.a())) {
                    this.f709a.e.put(bVar.a(), pVar);
                    arrayList.add(bVar.a());
                } else {
                    this.f709a.e.put(bVar.a(), pVar);
                    arrayList2.add(bVar.a());
                }
                if (!bVar.e().isEmpty()) {
                    this.f709a.f.remove(pVar);
                } else if (!this.f709a.f.contains(pVar)) {
                    this.f709a.f.add(pVar);
                }
            }
            ArrayList<String> arrayList4 = new ArrayList<>();
            for (x xVar : this.f709a.b()) {
                if (xVar.a(pVar)) {
                    arrayList4.add(xVar.a());
                }
            }
            if (!c.remove.equals(bVar.c())) {
                ArrayList<String> arrayList5 = new ArrayList<>();
                for (String str : bVar.e()) {
                    arrayList5.add(str);
                    x b = this.f709a.b(str);
                    if (b == null) {
                        b = this.f709a.a(str);
                        this.f709a.d.put(str, b);
                    }
                    b.b(pVar);
                }
                for (String remove : arrayList5) {
                    arrayList4.remove(remove);
                }
            }
            for (String str2 : arrayList4) {
                x b2 = this.f709a.b(str2);
                b2.c(pVar);
                if (b2.b() == 0) {
                    this.f709a.d.remove(str2);
                }
            }
            for (x xVar2 : this.f709a.b()) {
                if (xVar2.b() == 0) {
                    this.f709a.d.remove(xVar2.a());
                }
            }
        }
        synchronized (this.f709a) {
            this.f709a.f653a = true;
            this.f709a.notifyAll();
        }
        ac.a(this.f709a, arrayList, arrayList2, arrayList3);
    }
}
