package org.jivesoftware.smack.d;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.StringReader;
import java.net.InetAddress;
import java.net.Socket;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.net.SocketFactory;
import org.jivesoftware.smack.e.g;

final class b extends SocketFactory {
    private static final Pattern b = Pattern.compile("HTTP/\\S+\\s(\\d+)\\s(.*)\\s*");

    /* renamed from: a  reason: collision with root package name */
    private f f688a;

    public b(f fVar) {
        this.f688a = fVar;
    }

    private Socket a(String str, int i) {
        String str2;
        String a2 = this.f688a.a();
        Socket socket = new Socket(a2, this.f688a.b());
        String str3 = "CONNECT " + str + ":" + i;
        String c = this.f688a.c();
        if (c == null) {
            str2 = "";
        } else {
            str2 = "\r\nProxy-Authorization: Basic " + new String(g.a((c + ":" + this.f688a.d()).getBytes("UTF-8")));
        }
        socket.getOutputStream().write((str3 + " HTTP/1.1\r\nHost: " + str3 + str2 + "\r\n\r\n").getBytes("UTF-8"));
        InputStream inputStream = socket.getInputStream();
        StringBuilder sb = new StringBuilder(100);
        int i2 = 0;
        do {
            char read = (char) inputStream.read();
            sb.append(read);
            if (sb.length() > 1024) {
                throw new e(a.HTTP, "Recieved header of >1024 characters from " + a2 + ", cancelling connection");
            } else if (read == 65535) {
                throw new e(a.HTTP);
            } else if ((i2 == 0 || i2 == 2) && read == 13) {
                i2++;
                continue;
            } else if ((i2 == 1 || i2 == 3) && read == 10) {
                i2++;
                continue;
            } else {
                i2 = 0;
                continue;
            }
        } while (i2 != 4);
        if (i2 != 4) {
            throw new e(a.HTTP, "Never received blank line from " + a2 + ", cancelling connection");
        }
        String readLine = new BufferedReader(new StringReader(sb.toString())).readLine();
        if (readLine == null) {
            throw new e(a.HTTP, "Empty proxy response from " + a2 + ", cancelling");
        }
        Matcher matcher = b.matcher(readLine);
        if (!matcher.matches()) {
            throw new e(a.HTTP, "Unexpected proxy response from " + a2 + ": " + readLine);
        } else if (Integer.parseInt(matcher.group(1)) == 200) {
            return socket;
        } else {
            throw new e(a.HTTP);
        }
    }

    public final Socket createSocket(String str, int i) {
        return a(str, i);
    }

    public final Socket createSocket(String str, int i, InetAddress inetAddress, int i2) {
        return a(str, i);
    }

    public final Socket createSocket(InetAddress inetAddress, int i) {
        return a(inetAddress.getHostAddress(), i);
    }

    public final Socket createSocket(InetAddress inetAddress, int i, InetAddress inetAddress2, int i2) {
        return a(inetAddress.getHostAddress(), i);
    }
}
