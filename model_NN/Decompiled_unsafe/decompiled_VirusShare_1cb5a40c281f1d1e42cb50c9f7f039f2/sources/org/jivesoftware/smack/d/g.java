package org.jivesoftware.smack.d;

import java.net.InetAddress;
import java.net.Socket;
import javax.net.SocketFactory;

public final class g extends SocketFactory {

    /* renamed from: a  reason: collision with root package name */
    private f f691a;

    public g(f fVar) {
        this.f691a = fVar;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x005d, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0069, code lost:
        throw new org.jivesoftware.smack.d.e(org.jivesoftware.smack.d.a.SOCKS4, r0.toString(), r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x006a, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x006b, code lost:
        throw r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x009d, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x009e, code lost:
        r1 = r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:?, code lost:
        r1.close();
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x006a A[ExcHandler: RuntimeException (r0v4 'e' java.lang.RuntimeException A[CUSTOM_DECLARE]), Splitter:B:1:0x0015] */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x00a1 A[SYNTHETIC, Splitter:B:29:0x00a1] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.net.Socket a(java.lang.String r14, int r15) {
        /*
            r13 = this;
            r11 = 6
            r10 = 0
            r0 = 0
            org.jivesoftware.smack.d.f r1 = r13.f691a
            java.lang.String r1 = r1.a()
            org.jivesoftware.smack.d.f r2 = r13.f691a
            int r2 = r2.b()
            org.jivesoftware.smack.d.f r3 = r13.f691a
            java.lang.String r3 = r3.c()
            java.net.Socket r4 = new java.net.Socket     // Catch:{ RuntimeException -> 0x006a, Exception -> 0x010a }
            r4.<init>(r1, r2)     // Catch:{ RuntimeException -> 0x006a, Exception -> 0x010a }
            java.io.InputStream r0 = r4.getInputStream()     // Catch:{ RuntimeException -> 0x006a, Exception -> 0x009d }
            java.io.OutputStream r1 = r4.getOutputStream()     // Catch:{ RuntimeException -> 0x006a, Exception -> 0x009d }
            r2 = 1
            r4.setTcpNoDelay(r2)     // Catch:{ RuntimeException -> 0x006a, Exception -> 0x009d }
            r2 = 1024(0x400, float:1.435E-42)
            byte[] r2 = new byte[r2]     // Catch:{ RuntimeException -> 0x006a, Exception -> 0x009d }
            r5 = 0
            int r6 = r10 + 1
            r7 = 4
            r2[r5] = r7     // Catch:{ RuntimeException -> 0x006a, Exception -> 0x009d }
            r5 = 1
            int r6 = r6 + 1
            r7 = 1
            r2[r5] = r7     // Catch:{ RuntimeException -> 0x006a, Exception -> 0x009d }
            r5 = 2
            int r6 = r6 + 1
            int r7 = r15 >>> 8
            byte r7 = (byte) r7     // Catch:{ RuntimeException -> 0x006a, Exception -> 0x009d }
            r2[r5] = r7     // Catch:{ RuntimeException -> 0x006a, Exception -> 0x009d }
            r5 = 3
            int r6 = r6 + 1
            r7 = r15 & 255(0xff, float:3.57E-43)
            byte r7 = (byte) r7     // Catch:{ RuntimeException -> 0x006a, Exception -> 0x009d }
            r2[r5] = r7     // Catch:{ RuntimeException -> 0x006a, Exception -> 0x009d }
            java.net.InetAddress r5 = java.net.InetAddress.getByName(r14)     // Catch:{ UnknownHostException -> 0x005d }
            byte[] r5 = r5.getAddress()     // Catch:{ UnknownHostException -> 0x005d }
            r7 = r10
        L_0x004f:
            int r8 = r5.length     // Catch:{ UnknownHostException -> 0x005d }
            if (r7 >= r8) goto L_0x006c
            int r8 = r6 + 1
            byte r9 = r5[r7]     // Catch:{ UnknownHostException -> 0x005d }
            r2[r6] = r9     // Catch:{ UnknownHostException -> 0x005d }
            int r6 = r7 + 1
            r7 = r6
            r6 = r8
            goto L_0x004f
        L_0x005d:
            r0 = move-exception
            org.jivesoftware.smack.d.e r1 = new org.jivesoftware.smack.d.e     // Catch:{ RuntimeException -> 0x006a, Exception -> 0x009d }
            org.jivesoftware.smack.d.a r2 = org.jivesoftware.smack.d.a.SOCKS4     // Catch:{ RuntimeException -> 0x006a, Exception -> 0x009d }
            java.lang.String r3 = r0.toString()     // Catch:{ RuntimeException -> 0x006a, Exception -> 0x009d }
            r1.<init>(r2, r3, r0)     // Catch:{ RuntimeException -> 0x006a, Exception -> 0x009d }
            throw r1     // Catch:{ RuntimeException -> 0x006a, Exception -> 0x009d }
        L_0x006a:
            r0 = move-exception
            throw r0
        L_0x006c:
            if (r3 == 0) goto L_0x010f
            byte[] r5 = r3.getBytes()     // Catch:{ RuntimeException -> 0x006a, Exception -> 0x009d }
            r7 = 0
            int r8 = r3.length()     // Catch:{ RuntimeException -> 0x006a, Exception -> 0x009d }
            java.lang.System.arraycopy(r5, r7, r2, r6, r8)     // Catch:{ RuntimeException -> 0x006a, Exception -> 0x009d }
            int r3 = r3.length()     // Catch:{ RuntimeException -> 0x006a, Exception -> 0x009d }
            int r3 = r3 + r6
        L_0x007f:
            int r5 = r3 + 1
            r6 = 0
            r2[r3] = r6     // Catch:{ RuntimeException -> 0x006a, Exception -> 0x009d }
            r3 = 0
            r1.write(r2, r3, r5)     // Catch:{ RuntimeException -> 0x006a, Exception -> 0x009d }
            r1 = r10
        L_0x0089:
            if (r1 >= r11) goto L_0x00b2
            int r3 = r11 - r1
            int r3 = r0.read(r2, r1, r3)     // Catch:{ RuntimeException -> 0x006a, Exception -> 0x009d }
            if (r3 > 0) goto L_0x00b0
            org.jivesoftware.smack.d.e r0 = new org.jivesoftware.smack.d.e     // Catch:{ RuntimeException -> 0x006a, Exception -> 0x009d }
            org.jivesoftware.smack.d.a r1 = org.jivesoftware.smack.d.a.SOCKS4     // Catch:{ RuntimeException -> 0x006a, Exception -> 0x009d }
            java.lang.String r2 = "stream is closed"
            r0.<init>(r1, r2)     // Catch:{ RuntimeException -> 0x006a, Exception -> 0x009d }
            throw r0     // Catch:{ RuntimeException -> 0x006a, Exception -> 0x009d }
        L_0x009d:
            r0 = move-exception
            r1 = r4
        L_0x009f:
            if (r1 == 0) goto L_0x00a4
            r1.close()     // Catch:{ Exception -> 0x0108 }
        L_0x00a4:
            org.jivesoftware.smack.d.e r1 = new org.jivesoftware.smack.d.e
            org.jivesoftware.smack.d.a r2 = org.jivesoftware.smack.d.a.SOCKS4
            java.lang.String r0 = r0.toString()
            r1.<init>(r2, r0)
            throw r1
        L_0x00b0:
            int r1 = r1 + r3
            goto L_0x0089
        L_0x00b2:
            r1 = 0
            byte r1 = r2[r1]     // Catch:{ RuntimeException -> 0x006a, Exception -> 0x009d }
            if (r1 == 0) goto L_0x00d5
            org.jivesoftware.smack.d.e r0 = new org.jivesoftware.smack.d.e     // Catch:{ RuntimeException -> 0x006a, Exception -> 0x009d }
            org.jivesoftware.smack.d.a r1 = org.jivesoftware.smack.d.a.SOCKS4     // Catch:{ RuntimeException -> 0x006a, Exception -> 0x009d }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ RuntimeException -> 0x006a, Exception -> 0x009d }
            r3.<init>()     // Catch:{ RuntimeException -> 0x006a, Exception -> 0x009d }
            java.lang.String r5 = "server returns VN "
            java.lang.StringBuilder r3 = r3.append(r5)     // Catch:{ RuntimeException -> 0x006a, Exception -> 0x009d }
            r5 = 0
            byte r2 = r2[r5]     // Catch:{ RuntimeException -> 0x006a, Exception -> 0x009d }
            java.lang.StringBuilder r2 = r3.append(r2)     // Catch:{ RuntimeException -> 0x006a, Exception -> 0x009d }
            java.lang.String r2 = r2.toString()     // Catch:{ RuntimeException -> 0x006a, Exception -> 0x009d }
            r0.<init>(r1, r2)     // Catch:{ RuntimeException -> 0x006a, Exception -> 0x009d }
            throw r0     // Catch:{ RuntimeException -> 0x006a, Exception -> 0x009d }
        L_0x00d5:
            r1 = 1
            byte r1 = r2[r1]     // Catch:{ RuntimeException -> 0x006a, Exception -> 0x009d }
            r3 = 90
            if (r1 == r3) goto L_0x00fd
            r4.close()     // Catch:{ Exception -> 0x0106, RuntimeException -> 0x006a }
        L_0x00df:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ RuntimeException -> 0x006a, Exception -> 0x009d }
            r0.<init>()     // Catch:{ RuntimeException -> 0x006a, Exception -> 0x009d }
            java.lang.String r1 = "ProxySOCKS4: server returns CD "
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ RuntimeException -> 0x006a, Exception -> 0x009d }
            r1 = 1
            byte r1 = r2[r1]     // Catch:{ RuntimeException -> 0x006a, Exception -> 0x009d }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ RuntimeException -> 0x006a, Exception -> 0x009d }
            java.lang.String r0 = r0.toString()     // Catch:{ RuntimeException -> 0x006a, Exception -> 0x009d }
            org.jivesoftware.smack.d.e r1 = new org.jivesoftware.smack.d.e     // Catch:{ RuntimeException -> 0x006a, Exception -> 0x009d }
            org.jivesoftware.smack.d.a r2 = org.jivesoftware.smack.d.a.SOCKS4     // Catch:{ RuntimeException -> 0x006a, Exception -> 0x009d }
            r1.<init>(r2, r0)     // Catch:{ RuntimeException -> 0x006a, Exception -> 0x009d }
            throw r1     // Catch:{ RuntimeException -> 0x006a, Exception -> 0x009d }
        L_0x00fd:
            r1 = 2
            byte[] r1 = new byte[r1]     // Catch:{ RuntimeException -> 0x006a, Exception -> 0x009d }
            r2 = 0
            r3 = 2
            r0.read(r1, r2, r3)     // Catch:{ RuntimeException -> 0x006a, Exception -> 0x009d }
            return r4
        L_0x0106:
            r0 = move-exception
            goto L_0x00df
        L_0x0108:
            r1 = move-exception
            goto L_0x00a4
        L_0x010a:
            r1 = move-exception
            r12 = r1
            r1 = r0
            r0 = r12
            goto L_0x009f
        L_0x010f:
            r3 = r6
            goto L_0x007f
        */
        throw new UnsupportedOperationException("Method not decompiled: org.jivesoftware.smack.d.g.a(java.lang.String, int):java.net.Socket");
    }

    public final Socket createSocket(String str, int i) {
        return a(str, i);
    }

    public final Socket createSocket(String str, int i, InetAddress inetAddress, int i2) {
        return a(str, i);
    }

    public final Socket createSocket(InetAddress inetAddress, int i) {
        return a(inetAddress.getHostAddress(), i);
    }

    public final Socket createSocket(InetAddress inetAddress, int i, InetAddress inetAddress2, int i2) {
        return a(inetAddress.getHostAddress(), i);
    }
}
