package org.jivesoftware.smack.d;

import java.io.InputStream;
import java.net.InetAddress;
import java.net.Socket;
import javax.net.SocketFactory;

public final class c extends SocketFactory {

    /* renamed from: a  reason: collision with root package name */
    private f f689a;

    public c(f fVar) {
        this.f689a = fVar;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0061, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0062, code lost:
        throw r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x010d, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x010e, code lost:
        r1 = r5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x010f, code lost:
        if (r1 != null) goto L_0x0111;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:?, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x0132, code lost:
        throw new org.jivesoftware.smack.d.e(org.jivesoftware.smack.d.a.SOCKS5, "ProxySOCKS5: " + r0.toString(), r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x015d, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x015e, code lost:
        r12 = r1;
        r1 = null;
        r0 = r12;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0061 A[ExcHandler: RuntimeException (r0v3 'e' java.lang.RuntimeException A[CUSTOM_DECLARE]), Splitter:B:1:0x001b] */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x0111 A[SYNTHETIC, Splitter:B:34:0x0111] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:11:0x0057=Splitter:B:11:0x0057, B:28:0x00ef=Splitter:B:28:0x00ef} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.net.Socket a(java.lang.String r14, int r15) {
        /*
            r13 = this;
            r11 = 1
            r10 = 0
            r0 = 0
            org.jivesoftware.smack.d.f r1 = r13.f689a
            java.lang.String r1 = r1.a()
            org.jivesoftware.smack.d.f r2 = r13.f689a
            int r2 = r2.b()
            org.jivesoftware.smack.d.f r3 = r13.f689a
            java.lang.String r3 = r3.c()
            org.jivesoftware.smack.d.f r4 = r13.f689a
            java.lang.String r4 = r4.d()
            java.net.Socket r5 = new java.net.Socket     // Catch:{ RuntimeException -> 0x0061, Exception -> 0x015d }
            r5.<init>(r1, r2)     // Catch:{ RuntimeException -> 0x0061, Exception -> 0x015d }
            java.io.InputStream r0 = r5.getInputStream()     // Catch:{ RuntimeException -> 0x0061, Exception -> 0x010d }
            java.io.OutputStream r1 = r5.getOutputStream()     // Catch:{ RuntimeException -> 0x0061, Exception -> 0x010d }
            r2 = 1
            r5.setTcpNoDelay(r2)     // Catch:{ RuntimeException -> 0x0061, Exception -> 0x010d }
            r2 = 1024(0x400, float:1.435E-42)
            byte[] r2 = new byte[r2]     // Catch:{ RuntimeException -> 0x0061, Exception -> 0x010d }
            r6 = 0
            r7 = 5
            r2[r6] = r7     // Catch:{ RuntimeException -> 0x0061, Exception -> 0x010d }
            r6 = 1
            r7 = 2
            r2[r6] = r7     // Catch:{ RuntimeException -> 0x0061, Exception -> 0x010d }
            r6 = 2
            r7 = 0
            r2[r6] = r7     // Catch:{ RuntimeException -> 0x0061, Exception -> 0x010d }
            r6 = 3
            r7 = 2
            r2[r6] = r7     // Catch:{ RuntimeException -> 0x0061, Exception -> 0x010d }
            r6 = 0
            r7 = 4
            r1.write(r2, r6, r7)     // Catch:{ RuntimeException -> 0x0061, Exception -> 0x010d }
            r6 = 2
            a(r0, r2, r6)     // Catch:{ RuntimeException -> 0x0061, Exception -> 0x010d }
            r6 = 1
            byte r6 = r2[r6]     // Catch:{ RuntimeException -> 0x0061, Exception -> 0x010d }
            r6 = r6 & 255(0xff, float:3.57E-43)
            switch(r6) {
                case 0: goto L_0x0063;
                case 1: goto L_0x0051;
                case 2: goto L_0x0065;
                default: goto L_0x0051;
            }
        L_0x0051:
            r3 = r10
        L_0x0052:
            if (r3 != 0) goto L_0x00b1
            r5.close()     // Catch:{ Exception -> 0x0156, RuntimeException -> 0x0061 }
        L_0x0057:
            org.jivesoftware.smack.d.e r0 = new org.jivesoftware.smack.d.e     // Catch:{ RuntimeException -> 0x0061, Exception -> 0x010d }
            org.jivesoftware.smack.d.a r1 = org.jivesoftware.smack.d.a.SOCKS5     // Catch:{ RuntimeException -> 0x0061, Exception -> 0x010d }
            java.lang.String r2 = "fail in SOCKS5 proxy"
            r0.<init>(r1, r2)     // Catch:{ RuntimeException -> 0x0061, Exception -> 0x010d }
            throw r0     // Catch:{ RuntimeException -> 0x0061, Exception -> 0x010d }
        L_0x0061:
            r0 = move-exception
            throw r0
        L_0x0063:
            r3 = r11
            goto L_0x0052
        L_0x0065:
            if (r3 == 0) goto L_0x0051
            if (r4 == 0) goto L_0x0051
            r6 = 0
            r7 = 1
            r2[r6] = r7     // Catch:{ RuntimeException -> 0x0061, Exception -> 0x010d }
            r6 = 1
            int r7 = r3.length()     // Catch:{ RuntimeException -> 0x0061, Exception -> 0x010d }
            byte r7 = (byte) r7     // Catch:{ RuntimeException -> 0x0061, Exception -> 0x010d }
            r2[r6] = r7     // Catch:{ RuntimeException -> 0x0061, Exception -> 0x010d }
            byte[] r6 = r3.getBytes()     // Catch:{ RuntimeException -> 0x0061, Exception -> 0x010d }
            r7 = 0
            r8 = 2
            int r9 = r3.length()     // Catch:{ RuntimeException -> 0x0061, Exception -> 0x010d }
            java.lang.System.arraycopy(r6, r7, r2, r8, r9)     // Catch:{ RuntimeException -> 0x0061, Exception -> 0x010d }
            int r3 = r3.length()     // Catch:{ RuntimeException -> 0x0061, Exception -> 0x010d }
            int r3 = r3 + 2
            int r6 = r3 + 1
            int r7 = r4.length()     // Catch:{ RuntimeException -> 0x0061, Exception -> 0x010d }
            byte r7 = (byte) r7     // Catch:{ RuntimeException -> 0x0061, Exception -> 0x010d }
            r2[r3] = r7     // Catch:{ RuntimeException -> 0x0061, Exception -> 0x010d }
            byte[] r3 = r4.getBytes()     // Catch:{ RuntimeException -> 0x0061, Exception -> 0x010d }
            r7 = 0
            int r8 = r4.length()     // Catch:{ RuntimeException -> 0x0061, Exception -> 0x010d }
            java.lang.System.arraycopy(r3, r7, r2, r6, r8)     // Catch:{ RuntimeException -> 0x0061, Exception -> 0x010d }
            int r3 = r4.length()     // Catch:{ RuntimeException -> 0x0061, Exception -> 0x010d }
            int r3 = r3 + r6
            r4 = 0
            r1.write(r2, r4, r3)     // Catch:{ RuntimeException -> 0x0061, Exception -> 0x010d }
            r3 = 2
            a(r0, r2, r3)     // Catch:{ RuntimeException -> 0x0061, Exception -> 0x010d }
            r3 = 1
            byte r3 = r2[r3]     // Catch:{ RuntimeException -> 0x0061, Exception -> 0x010d }
            if (r3 != 0) goto L_0x0051
            r3 = r11
            goto L_0x0052
        L_0x00b1:
            r3 = 0
            r4 = 5
            r2[r3] = r4     // Catch:{ RuntimeException -> 0x0061, Exception -> 0x010d }
            r3 = 1
            r4 = 1
            r2[r3] = r4     // Catch:{ RuntimeException -> 0x0061, Exception -> 0x010d }
            r3 = 2
            r4 = 0
            r2[r3] = r4     // Catch:{ RuntimeException -> 0x0061, Exception -> 0x010d }
            byte[] r3 = r14.getBytes()     // Catch:{ RuntimeException -> 0x0061, Exception -> 0x010d }
            int r4 = r3.length     // Catch:{ RuntimeException -> 0x0061, Exception -> 0x010d }
            r6 = 3
            r7 = 3
            r2[r6] = r7     // Catch:{ RuntimeException -> 0x0061, Exception -> 0x010d }
            r6 = 4
            byte r7 = (byte) r4     // Catch:{ RuntimeException -> 0x0061, Exception -> 0x010d }
            r2[r6] = r7     // Catch:{ RuntimeException -> 0x0061, Exception -> 0x010d }
            r6 = 0
            r7 = 5
            java.lang.System.arraycopy(r3, r6, r2, r7, r4)     // Catch:{ RuntimeException -> 0x0061, Exception -> 0x010d }
            int r3 = r4 + 5
            int r4 = r3 + 1
            int r6 = r15 >>> 8
            byte r6 = (byte) r6     // Catch:{ RuntimeException -> 0x0061, Exception -> 0x010d }
            r2[r3] = r6     // Catch:{ RuntimeException -> 0x0061, Exception -> 0x010d }
            int r3 = r4 + 1
            r6 = r15 & 255(0xff, float:3.57E-43)
            byte r6 = (byte) r6     // Catch:{ RuntimeException -> 0x0061, Exception -> 0x010d }
            r2[r4] = r6     // Catch:{ RuntimeException -> 0x0061, Exception -> 0x010d }
            r4 = 0
            r1.write(r2, r4, r3)     // Catch:{ RuntimeException -> 0x0061, Exception -> 0x010d }
            r1 = 4
            a(r0, r2, r1)     // Catch:{ RuntimeException -> 0x0061, Exception -> 0x010d }
            r1 = 1
            byte r1 = r2[r1]     // Catch:{ RuntimeException -> 0x0061, Exception -> 0x010d }
            if (r1 == 0) goto L_0x0133
            r5.close()     // Catch:{ Exception -> 0x0159, RuntimeException -> 0x0061 }
        L_0x00ef:
            org.jivesoftware.smack.d.e r0 = new org.jivesoftware.smack.d.e     // Catch:{ RuntimeException -> 0x0061, Exception -> 0x010d }
            org.jivesoftware.smack.d.a r1 = org.jivesoftware.smack.d.a.SOCKS5     // Catch:{ RuntimeException -> 0x0061, Exception -> 0x010d }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ RuntimeException -> 0x0061, Exception -> 0x010d }
            r3.<init>()     // Catch:{ RuntimeException -> 0x0061, Exception -> 0x010d }
            java.lang.String r4 = "server returns "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ RuntimeException -> 0x0061, Exception -> 0x010d }
            r4 = 1
            byte r2 = r2[r4]     // Catch:{ RuntimeException -> 0x0061, Exception -> 0x010d }
            java.lang.StringBuilder r2 = r3.append(r2)     // Catch:{ RuntimeException -> 0x0061, Exception -> 0x010d }
            java.lang.String r2 = r2.toString()     // Catch:{ RuntimeException -> 0x0061, Exception -> 0x010d }
            r0.<init>(r1, r2)     // Catch:{ RuntimeException -> 0x0061, Exception -> 0x010d }
            throw r0     // Catch:{ RuntimeException -> 0x0061, Exception -> 0x010d }
        L_0x010d:
            r0 = move-exception
            r1 = r5
        L_0x010f:
            if (r1 == 0) goto L_0x0114
            r1.close()     // Catch:{ Exception -> 0x015b }
        L_0x0114:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "ProxySOCKS5: "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = r0.toString()
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            org.jivesoftware.smack.d.e r2 = new org.jivesoftware.smack.d.e
            org.jivesoftware.smack.d.a r3 = org.jivesoftware.smack.d.a.SOCKS5
            r2.<init>(r3, r1, r0)
            throw r2
        L_0x0133:
            r1 = 3
            byte r1 = r2[r1]     // Catch:{ RuntimeException -> 0x0061, Exception -> 0x010d }
            r1 = r1 & 255(0xff, float:3.57E-43)
            switch(r1) {
                case 1: goto L_0x013c;
                case 2: goto L_0x013b;
                case 3: goto L_0x0141;
                case 4: goto L_0x0150;
                default: goto L_0x013b;
            }     // Catch:{ RuntimeException -> 0x0061, Exception -> 0x010d }
        L_0x013b:
            return r5
        L_0x013c:
            r1 = 6
            a(r0, r2, r1)     // Catch:{ RuntimeException -> 0x0061, Exception -> 0x010d }
            goto L_0x013b
        L_0x0141:
            r1 = 1
            a(r0, r2, r1)     // Catch:{ RuntimeException -> 0x0061, Exception -> 0x010d }
            r1 = 0
            byte r1 = r2[r1]     // Catch:{ RuntimeException -> 0x0061, Exception -> 0x010d }
            r1 = r1 & 255(0xff, float:3.57E-43)
            int r1 = r1 + 2
            a(r0, r2, r1)     // Catch:{ RuntimeException -> 0x0061, Exception -> 0x010d }
            goto L_0x013b
        L_0x0150:
            r1 = 18
            a(r0, r2, r1)     // Catch:{ RuntimeException -> 0x0061, Exception -> 0x010d }
            goto L_0x013b
        L_0x0156:
            r0 = move-exception
            goto L_0x0057
        L_0x0159:
            r0 = move-exception
            goto L_0x00ef
        L_0x015b:
            r1 = move-exception
            goto L_0x0114
        L_0x015d:
            r1 = move-exception
            r12 = r1
            r1 = r0
            r0 = r12
            goto L_0x010f
        */
        throw new UnsupportedOperationException("Method not decompiled: org.jivesoftware.smack.d.c.a(java.lang.String, int):java.net.Socket");
    }

    private static void a(InputStream inputStream, byte[] bArr, int i) {
        int i2 = 0;
        while (i2 < i) {
            int read = inputStream.read(bArr, i2, i - i2);
            if (read <= 0) {
                throw new e(a.SOCKS5, "stream is closed");
            }
            i2 += read;
        }
    }

    public final Socket createSocket(String str, int i) {
        return a(str, i);
    }

    public final Socket createSocket(String str, int i, InetAddress inetAddress, int i2) {
        return a(str, i);
    }

    public final Socket createSocket(InetAddress inetAddress, int i) {
        return a(inetAddress.getHostAddress(), i);
    }

    public final Socket createSocket(InetAddress inetAddress, int i, InetAddress inetAddress2, int i2) {
        return a(inetAddress.getHostAddress(), i);
    }
}
