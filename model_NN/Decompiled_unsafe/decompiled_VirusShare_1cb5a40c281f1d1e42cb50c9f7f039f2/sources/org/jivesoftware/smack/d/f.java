package org.jivesoftware.smack.d;

import javax.net.SocketFactory;

public final class f {

    /* renamed from: a  reason: collision with root package name */
    private String f690a = null;
    private int b = 0;
    private String c = null;
    private String d = null;
    private a e;

    public f(a aVar) {
        this.e = aVar;
    }

    public final String a() {
        return this.f690a;
    }

    public final int b() {
        return this.b;
    }

    public final String c() {
        return this.c;
    }

    public final String d() {
        return this.d;
    }

    public final SocketFactory e() {
        if (this.e == a.NONE) {
            return new d();
        }
        if (this.e == a.HTTP) {
            return new b(this);
        }
        if (this.e == a.SOCKS4) {
            return new g(this);
        }
        if (this.e == a.SOCKS5) {
            return new c(this);
        }
        return null;
    }
}
