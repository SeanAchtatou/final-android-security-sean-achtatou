package org.jivesoftware.smack.d;

import java.io.IOException;

public final class e extends IOException {
    public e(a aVar) {
        super("Proxy Exception " + aVar.toString() + " : " + "Unknown Error");
    }

    public e(a aVar, String str) {
        super("Proxy Exception " + aVar.toString() + " : " + str);
    }

    public e(a aVar, String str, Throwable th) {
        super("Proxy Exception " + aVar.toString() + " : " + str + ", " + th);
    }
}
