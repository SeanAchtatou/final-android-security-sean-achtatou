package org.jivesoftware.smack;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.security.KeyStore;
import java.security.Provider;
import java.security.SecureRandom;
import java.security.Security;
import java.util.Collection;
import javax.net.ssl.KeyManager;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.TrustManager;
import org.a.b.a.a.b.a.c;
import org.a.b.a.a.b.a.f;
import org.jivesoftware.smack.b.d;
import org.jivesoftware.smack.b.i;
import org.jivesoftware.smack.b.q;
import org.jivesoftware.smack.b.r;
import org.jivesoftware.smack.b.w;
import org.jivesoftware.smack.e.h;

public class XMPPConnection extends Connection {
    String l = null;
    b m;
    z n;
    private Socket o;
    private String p = null;
    private boolean q = false;
    private boolean r = false;
    private boolean s = false;
    private boolean t = false;
    private boolean u = false;
    private ac v = null;
    private Collection w;
    private boolean x;

    public XMPPConnection(d dVar) {
        super(dVar);
    }

    private void b(boolean z) {
        if (!this.s) {
            this.s = z;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:27:0x006c A[SYNTHETIC, Splitter:B:27:0x006c] */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0077 A[SYNTHETIC, Splitter:B:32:0x0077] */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x0082 A[SYNTHETIC, Splitter:B:37:0x0082] */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x008d A[SYNTHETIC, Splitter:B:42:0x008d] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void t() {
        /*
            r5 = this;
            r1 = 1
            r4 = 0
            r3 = 0
            org.jivesoftware.smack.z r0 = r5.n
            if (r0 == 0) goto L_0x000b
            org.jivesoftware.smack.b r0 = r5.m
            if (r0 != 0) goto L_0x009e
        L_0x000b:
            r0 = r1
        L_0x000c:
            if (r0 != 0) goto L_0x0010
            r5.x = r4
        L_0x0010:
            r5.u()
            if (r0 == 0) goto L_0x00a1
            org.jivesoftware.smack.b r1 = new org.jivesoftware.smack.b     // Catch:{ ad -> 0x005c }
            r1.<init>(r5)     // Catch:{ ad -> 0x005c }
            r5.m = r1     // Catch:{ ad -> 0x005c }
            org.jivesoftware.smack.z r1 = new org.jivesoftware.smack.z     // Catch:{ ad -> 0x005c }
            r1.<init>(r5)     // Catch:{ ad -> 0x005c }
            r5.n = r1     // Catch:{ ad -> 0x005c }
            org.jivesoftware.smack.d r1 = r5.k     // Catch:{ ad -> 0x005c }
            boolean r1 = r1.p()     // Catch:{ ad -> 0x005c }
            if (r1 == 0) goto L_0x0035
            org.jivesoftware.smack.g.a r1 = r5.f     // Catch:{ ad -> 0x005c }
            org.jivesoftware.smack.ab r1 = r1.c()     // Catch:{ ad -> 0x005c }
            r2 = 0
            r5.a(r1, r2)     // Catch:{ ad -> 0x005c }
        L_0x0035:
            org.jivesoftware.smack.b r1 = r5.m     // Catch:{ ad -> 0x005c }
            r1.b()     // Catch:{ ad -> 0x005c }
            org.jivesoftware.smack.z r1 = r5.n     // Catch:{ ad -> 0x005c }
            r1.b()     // Catch:{ ad -> 0x005c }
            r1 = 1
            r5.q = r1     // Catch:{ ad -> 0x005c }
            if (r0 == 0) goto L_0x00ac
            java.util.Collection r0 = j()     // Catch:{ ad -> 0x005c }
            java.util.Iterator r1 = r0.iterator()     // Catch:{ ad -> 0x005c }
        L_0x004c:
            boolean r0 = r1.hasNext()     // Catch:{ ad -> 0x005c }
            if (r0 == 0) goto L_0x00b1
            java.lang.Object r0 = r1.next()     // Catch:{ ad -> 0x005c }
            org.jivesoftware.smack.f r0 = (org.jivesoftware.smack.f) r0     // Catch:{ ad -> 0x005c }
            r0.a(r5)     // Catch:{ ad -> 0x005c }
            goto L_0x004c
        L_0x005c:
            r0 = move-exception
            org.jivesoftware.smack.b r1 = r5.m
            if (r1 == 0) goto L_0x0068
            org.jivesoftware.smack.b r1 = r5.m     // Catch:{ Throwable -> 0x00ba }
            r1.c()     // Catch:{ Throwable -> 0x00ba }
        L_0x0066:
            r5.m = r3
        L_0x0068:
            org.jivesoftware.smack.z r1 = r5.n
            if (r1 == 0) goto L_0x0073
            org.jivesoftware.smack.z r1 = r5.n     // Catch:{ Throwable -> 0x00b8 }
            r1.c()     // Catch:{ Throwable -> 0x00b8 }
        L_0x0071:
            r5.n = r3
        L_0x0073:
            java.io.Reader r1 = r5.g
            if (r1 == 0) goto L_0x007e
            java.io.Reader r1 = r5.g     // Catch:{ Throwable -> 0x00b6 }
            r1.close()     // Catch:{ Throwable -> 0x00b6 }
        L_0x007c:
            r5.g = r3
        L_0x007e:
            java.io.Writer r1 = r5.h
            if (r1 == 0) goto L_0x0089
            java.io.Writer r1 = r5.h     // Catch:{ Throwable -> 0x00b4 }
            r1.close()     // Catch:{ Throwable -> 0x00b4 }
        L_0x0087:
            r5.h = r3
        L_0x0089:
            java.net.Socket r1 = r5.o
            if (r1 == 0) goto L_0x0094
            java.net.Socket r1 = r5.o     // Catch:{ Exception -> 0x00b2 }
            r1.close()     // Catch:{ Exception -> 0x00b2 }
        L_0x0092:
            r5.o = r3
        L_0x0094:
            boolean r1 = r5.r
            r5.b(r1)
            r5.r = r4
            r5.q = r4
            throw r0
        L_0x009e:
            r0 = r4
            goto L_0x000c
        L_0x00a1:
            org.jivesoftware.smack.b r1 = r5.m     // Catch:{ ad -> 0x005c }
            r1.a()     // Catch:{ ad -> 0x005c }
            org.jivesoftware.smack.z r1 = r5.n     // Catch:{ ad -> 0x005c }
            r1.a()     // Catch:{ ad -> 0x005c }
            goto L_0x0035
        L_0x00ac:
            org.jivesoftware.smack.z r0 = r5.n     // Catch:{ ad -> 0x005c }
            r0.e()     // Catch:{ ad -> 0x005c }
        L_0x00b1:
            return
        L_0x00b2:
            r1 = move-exception
            goto L_0x0092
        L_0x00b4:
            r1 = move-exception
            goto L_0x0087
        L_0x00b6:
            r1 = move-exception
            goto L_0x007c
        L_0x00b8:
            r1 = move-exception
            goto L_0x0071
        L_0x00ba:
            r1 = move-exception
            goto L_0x0066
        */
        throw new UnsupportedOperationException("Method not decompiled: org.jivesoftware.smack.XMPPConnection.t():void");
    }

    private void u() {
        try {
            if (!this.x) {
                this.g = new m(new InputStreamReader(this.o.getInputStream(), "UTF-8"));
                this.h = new BufferedWriter(new OutputStreamWriter(this.o.getOutputStream(), "UTF-8"));
            } else {
                try {
                    Class<?> cls = Class.forName("com.jcraft.jzlib.ZOutputStream");
                    Object newInstance = cls.getConstructor(OutputStream.class, Integer.TYPE).newInstance(this.o.getOutputStream(), 9);
                    cls.getMethod("setFlushMode", Integer.TYPE).invoke(newInstance, 2);
                    this.h = new BufferedWriter(new OutputStreamWriter((OutputStream) newInstance, "UTF-8"));
                    Class<?> cls2 = Class.forName("com.jcraft.jzlib.ZInputStream");
                    Object newInstance2 = cls2.getConstructor(InputStream.class).newInstance(this.o.getInputStream());
                    cls2.getMethod("setFlushMode", Integer.TYPE).invoke(newInstance2, 2);
                    this.g = new BufferedReader(new InputStreamReader((InputStream) newInstance2, "UTF-8"));
                } catch (Exception e) {
                    e.printStackTrace();
                    this.g = new BufferedReader(new InputStreamReader(this.o.getInputStream(), "UTF-8"));
                    this.h = new BufferedWriter(new OutputStreamWriter(this.o.getOutputStream(), "UTF-8"));
                }
            }
            initDebugger();
        } catch (IOException e2) {
            throw new ad("XMPPError establishing connection with server.", new d(i.p, "XMPPError establishing connection with server."), e2);
        }
    }

    private boolean v() {
        if (this.r) {
            throw new IllegalStateException("Compression should be negotiated before authentication.");
        }
        try {
            Class.forName("com.jcraft.jzlib.ZOutputStream");
            if (!(this.w != null && this.w.contains("zlib"))) {
                return false;
            }
            try {
                this.h.write("<compress xmlns='http://jabber.org/protocol/compress'>");
                this.h.write("<method>zlib</method></compress>");
                this.h.flush();
            } catch (IOException e) {
                this.n.a(e);
            }
            synchronized (this) {
                try {
                    wait((long) (g.a() * 5));
                } catch (InterruptedException e2) {
                }
            }
            return this.x;
        } catch (ClassNotFoundException e3) {
            throw new IllegalStateException("Cannot use compression. Add smackx.jar to the classpath");
        }
    }

    public final synchronized void a(String str, String str2, String str3) {
        if (!this.q) {
            throw new IllegalStateException("Not connected to server.");
        } else if (this.r) {
            throw new IllegalStateException("Already logged in to server.");
        } else {
            String trim = str.toLowerCase().trim();
            String a2 = (!this.k.o() || !this.i.a()) ? new a(this).a(trim, str2, str3) : str2 != null ? this.i.a(trim, str2, str3) : this.i.a(trim, str3, this.k.u());
            if (a2 != null) {
                this.p = a2;
                this.k.a(h.b(a2));
            } else {
                this.p = trim + "@" + b();
                if (str3 != null) {
                    this.p += "/" + str3;
                }
            }
            if (this.k.n()) {
                v();
            }
            if (this.v == null) {
                this.v = new ac(this);
            }
            if (this.k.s()) {
                this.v.a();
            }
            if (this.k.w()) {
                this.m.a(new q(r.available));
            }
            this.r = true;
            this.t = false;
            this.k.a(trim, str2, str3);
            if (this.k.p() && this.f != null) {
                this.f.a(this.p);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(Collection collection) {
        this.w = collection;
    }

    public final void a(w wVar) {
        if (!this.q) {
            throw new IllegalStateException("Not connected to server.");
        } else if (wVar == null) {
            throw new NullPointerException("Packet is null.");
        } else {
            if (wVar.h() == null || wVar.h().equals("")) {
                wVar.g(!this.r ? null : this.p);
            }
            this.m.a(wVar);
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(boolean z) {
        if (z && this.k.d() == q.disabled) {
            this.n.a(new IllegalStateException("TLS required by server but not allowed by connection configuration"));
        } else if (this.k.d() != q.disabled) {
            try {
                this.h.write("<starttls xmlns=\"urn:ietf:params:xml:ns:xmpp-tls\"/>");
                this.h.flush();
            } catch (IOException e) {
                this.n.a(e);
            }
        }
    }

    public final String e() {
        if (!this.q) {
            return null;
        }
        return this.l;
    }

    public final boolean f() {
        return this.q;
    }

    public final ac h() {
        if (this.v == null) {
            return null;
        }
        if (!this.k.s()) {
            this.v.a();
        }
        if (!this.v.f653a) {
            try {
                synchronized (this.v) {
                    long currentTimeMillis = System.currentTimeMillis();
                    long a2 = (long) g.a();
                    long j = currentTimeMillis;
                    while (!this.v.f653a && a2 > 0) {
                        this.v.wait(a2);
                        long currentTimeMillis2 = System.currentTimeMillis();
                        a2 -= currentTimeMillis2 - j;
                        j = currentTimeMillis2;
                    }
                }
            } catch (InterruptedException e) {
            }
        }
        return this.v;
    }

    public final boolean m() {
        return this.r;
    }

    /* access modifiers changed from: protected */
    public final void n() {
        b(this.r);
        this.r = false;
        this.q = false;
        this.n.c();
        this.m.c();
        try {
            Thread.sleep(150);
        } catch (Exception e) {
        }
        if (this.g != null) {
            this.g = null;
        }
        if (this.h != null) {
            this.h = null;
        }
        try {
            this.o.close();
        } catch (Exception e2) {
        }
        this.i.e();
    }

    public final void o() {
        if (this.n != null && this.m != null) {
            n();
            if (this.v != null) {
                this.v.c();
                this.v = null;
            }
            this.s = false;
            this.m.d();
            this.m = null;
            this.n.d();
            this.n = null;
        }
    }

    public final boolean p() {
        return this.u;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{javax.net.ssl.SSLSocketFactory.createSocket(java.net.Socket, java.lang.String, int, boolean):java.net.Socket throws java.io.IOException}
     arg types: [java.net.Socket, java.lang.String, int, int]
     candidates:
      ClspMth{javax.net.SocketFactory.createSocket(java.lang.String, int, java.net.InetAddress, int):java.net.Socket throws java.io.IOException, java.net.UnknownHostException}
      ClspMth{javax.net.SocketFactory.createSocket(java.net.InetAddress, int, java.net.InetAddress, int):java.net.Socket throws java.io.IOException}
      ClspMth{javax.net.ssl.SSLSocketFactory.createSocket(java.net.Socket, java.lang.String, int, boolean):java.net.Socket throws java.io.IOException} */
    /* access modifiers changed from: package-private */
    public final void q() {
        KeyManager[] keyManagerArr;
        c cVar;
        KeyStore keyStore;
        SSLContext instance = SSLContext.getInstance("TLS");
        if (this.k.u() != null) {
            if (this.k.f().equals("NONE")) {
                cVar = null;
                keyStore = null;
            } else if (this.k.f().equals("PKCS11")) {
                try {
                    Provider provider = (Provider) Class.forName("sun.security.pkcs11.SunPKCS11").getConstructor(InputStream.class).newInstance(new ByteArrayInputStream(("name = SmartCard\nlibrary = " + this.k.g()).getBytes()));
                    Security.addProvider(provider);
                    KeyStore instance2 = KeyStore.getInstance("PKCS11", provider);
                    c cVar2 = new c("PKCS11 Password: ");
                    this.k.u().a(new f[]{cVar2});
                    instance2.load(null, cVar2.a());
                    c cVar3 = cVar2;
                    keyStore = instance2;
                    cVar = cVar3;
                } catch (Exception e) {
                    cVar = null;
                    keyStore = null;
                }
            } else if (this.k.f().equals("Apple")) {
                KeyStore instance3 = KeyStore.getInstance("KeychainStore", "Apple");
                instance3.load(null, null);
                keyStore = instance3;
                cVar = null;
            } else {
                KeyStore instance4 = KeyStore.getInstance(this.k.f());
                try {
                    c cVar4 = new c("Keystore Password: ");
                    this.k.u().a(new f[]{cVar4});
                    instance4.load(new FileInputStream(this.k.e()), cVar4.a());
                    c cVar5 = cVar4;
                    keyStore = instance4;
                    cVar = cVar5;
                } catch (Exception e2) {
                    cVar = null;
                    keyStore = null;
                }
            }
            KeyManagerFactory instance5 = KeyManagerFactory.getInstance("SunX509");
            if (cVar == null) {
                try {
                    instance5.init(keyStore, null);
                } catch (NullPointerException e3) {
                    keyManagerArr = null;
                }
            } else {
                instance5.init(keyStore, cVar.a());
                cVar.b();
            }
            keyManagerArr = instance5.getKeyManagers();
        } else {
            keyManagerArr = null;
        }
        instance.init(keyManagerArr, new TrustManager[]{new e(b(), this.k)}, new SecureRandom());
        Socket socket = this.o;
        this.o = instance.getSocketFactory().createSocket(socket, socket.getInetAddress().getHostName(), socket.getPort(), true);
        this.o.setSoTimeout(0);
        this.o.setKeepAlive(true);
        u();
        ((SSLSocket) this.o).startHandshake();
        this.u = true;
        this.m.a(this.h);
        this.m.e();
    }

    /* access modifiers changed from: package-private */
    public final void r() {
        this.x = true;
        u();
        this.m.a(this.h);
        this.m.e();
        synchronized (this) {
            notify();
        }
    }

    public final void s() {
        d dVar = this.k;
        String b = dVar.b();
        int c = dVar.c();
        try {
            if (dVar.v() == null) {
                this.o = new Socket(b, c);
            } else {
                this.o = dVar.v().createSocket(b, c);
            }
            t();
        } catch (UnknownHostException e) {
            String str = "Could not connect to " + b + ":" + c + ".";
            throw new ad(str, new d(i.r, str), e);
        } catch (IOException e2) {
            String str2 = "XMPPError connecting to " + b + ":" + c + ".";
            throw new ad(str2, new d(i.p, str2), e2);
        }
    }
}
