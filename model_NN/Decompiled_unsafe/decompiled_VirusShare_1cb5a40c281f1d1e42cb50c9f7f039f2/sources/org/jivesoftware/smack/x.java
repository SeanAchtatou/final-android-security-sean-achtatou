package org.jivesoftware.smack;

import java.util.ArrayList;
import java.util.List;

public final class x {

    /* renamed from: a  reason: collision with root package name */
    private String f713a;
    private Connection b;
    private final List c = new ArrayList();

    x(String str, Connection connection) {
        this.f713a = str;
        this.b = connection;
    }

    public final String a() {
        return this.f713a;
    }

    public final boolean a(p pVar) {
        boolean contains;
        synchronized (this.c) {
            contains = this.c.contains(pVar);
        }
        return contains;
    }

    public final int b() {
        int size;
        synchronized (this.c) {
            size = this.c.size();
        }
        return size;
    }

    /* access modifiers changed from: package-private */
    public final void b(p pVar) {
        synchronized (this.c) {
            this.c.remove(pVar);
            this.c.add(pVar);
        }
    }

    /* access modifiers changed from: package-private */
    public final void c(p pVar) {
        synchronized (this.c) {
            if (this.c.contains(pVar)) {
                this.c.remove(pVar);
            }
        }
    }
}
