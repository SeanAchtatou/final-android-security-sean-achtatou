package org.jivesoftware.smack.b;

import org.jivesoftware.smack.e.h;

public class q extends w {

    /* renamed from: a  reason: collision with root package name */
    private r f672a = r.available;
    private String b = null;
    private int c = Integer.MIN_VALUE;
    private n e = null;
    private String f;

    public q(r rVar) {
        if (rVar == null) {
            throw new NullPointerException("Type cannot be null");
        }
        this.f672a = rVar;
    }

    public final void a(int i) {
        if (i < -128 || i > 128) {
            throw new IllegalArgumentException("Priority value " + i + " is not valid. Valid range is -128 through 128.");
        }
        this.c = i;
    }

    public final void a(String str) {
        this.b = str;
    }

    public final void a(n nVar) {
        this.e = nVar;
    }

    public final r b() {
        return this.f672a;
    }

    public final void b(String str) {
        this.f = str;
    }

    public final String b_() {
        StringBuilder sb = new StringBuilder();
        sb.append("<presence");
        if (k() != null) {
            sb.append(" xmlns=\"").append(k()).append("\"");
        }
        if (this.f != null) {
            sb.append(" xml:lang=\"").append(this.f).append("\"");
        }
        if (f() != null) {
            sb.append(" id=\"").append(f()).append("\"");
        }
        if (g() != null) {
            sb.append(" to=\"").append(h.e(g())).append("\"");
        }
        if (h() != null) {
            sb.append(" from=\"").append(h.e(h())).append("\"");
        }
        if (this.f672a != r.available) {
            sb.append(" type=\"").append(this.f672a).append("\"");
        }
        sb.append(">");
        if (this.b != null) {
            sb.append("<status>").append(h.e(this.b)).append("</status>");
        }
        if (this.c != Integer.MIN_VALUE) {
            sb.append("<priority>").append(this.c).append("</priority>");
        }
        if (!(this.e == null || this.e == n.available)) {
            sb.append("<show>").append(this.e).append("</show>");
        }
        sb.append(j());
        d i = i();
        if (i != null) {
            sb.append(i.a());
        }
        sb.append("</presence>");
        return sb.toString();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.f672a);
        if (this.e != null) {
            sb.append(": ").append(this.e);
        }
        if (this.b != null) {
            sb.append(" (").append(this.b).append(")");
        }
        return sb.toString();
    }
}
