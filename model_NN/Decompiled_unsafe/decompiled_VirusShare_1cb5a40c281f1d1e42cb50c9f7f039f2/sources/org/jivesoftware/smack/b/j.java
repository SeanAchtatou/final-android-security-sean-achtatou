package org.jivesoftware.smack.b;

public enum j {
    normal,
    chat,
    groupchat,
    headline,
    error;

    public static j a(String str) {
        try {
            return valueOf(str);
        } catch (Exception e) {
            return normal;
        }
    }
}
