package org.jivesoftware.smack.b;

import org.jivesoftware.smack.e.h;

public abstract class p extends w {

    /* renamed from: a  reason: collision with root package name */
    private l f671a = l.f667a;

    public final void a(l lVar) {
        if (lVar == null) {
            this.f671a = l.f667a;
        } else {
            this.f671a = lVar;
        }
    }

    public abstract String b();

    public final String b_() {
        StringBuilder sb = new StringBuilder();
        sb.append("<iq ");
        if (f() != null) {
            sb.append("id=\"" + f() + "\" ");
        }
        if (g() != null) {
            sb.append("to=\"").append(h.e(g())).append("\" ");
        }
        if (h() != null) {
            sb.append("from=\"").append(h.e(h())).append("\" ");
        }
        if (this.f671a == null) {
            sb.append("type=\"get\">");
        } else {
            sb.append("type=\"").append(this.f671a).append("\">");
        }
        String b = b();
        if (b != null) {
            sb.append(b);
        }
        d i = i();
        if (i != null) {
            sb.append(i.a());
        }
        sb.append("</iq>");
        return sb.toString();
    }

    public final l d() {
        return this.f671a;
    }
}
