package org.jivesoftware.smack.b;

import org.jivesoftware.smack.e.h;

public final class g extends p {

    /* renamed from: a  reason: collision with root package name */
    private String f662a = null;
    private String b = null;
    private String c = null;
    private String e = null;

    public g() {
        a(l.b);
    }

    public final String a() {
        return this.b;
    }

    public final void a(String str) {
        this.f662a = str;
    }

    public final void a(String str, String str2) {
        this.c = h.f(str + str2);
    }

    public final String b() {
        StringBuilder sb = new StringBuilder();
        sb.append("<query xmlns=\"jabber:iq:auth\">");
        if (this.f662a != null) {
            if (this.f662a.equals("")) {
                sb.append("<username/>");
            } else {
                sb.append("<username>").append(this.f662a).append("</username>");
            }
        }
        if (this.c != null) {
            if (this.c.equals("")) {
                sb.append("<digest/>");
            } else {
                sb.append("<digest>").append(this.c).append("</digest>");
            }
        }
        if (this.b != null && this.c == null) {
            if (this.b.equals("")) {
                sb.append("<password/>");
            } else {
                sb.append("<password>").append(h.e(this.b)).append("</password>");
            }
        }
        if (this.e != null) {
            if (this.e.equals("")) {
                sb.append("<resource/>");
            } else {
                sb.append("<resource>").append(this.e).append("</resource>");
            }
        }
        sb.append("</query>");
        return sb.toString();
    }

    public final void b(String str) {
        this.b = str;
    }

    public final String c() {
        return this.c;
    }

    public final void c(String str) {
        this.c = str;
    }

    public final void d(String str) {
        this.e = str;
    }
}
