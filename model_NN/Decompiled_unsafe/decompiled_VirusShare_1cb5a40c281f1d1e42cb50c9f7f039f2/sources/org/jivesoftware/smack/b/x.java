package org.jivesoftware.smack.b;

import java.util.HashMap;
import java.util.Map;

final class x {
    private static Map d;

    /* renamed from: a  reason: collision with root package name */
    private int f678a;
    private k b;
    private i c;

    static {
        HashMap hashMap = new HashMap(22);
        hashMap.put(i.f664a, new x(i.f664a, k.WAIT, 500));
        hashMap.put(i.b, new x(i.b, k.AUTH, 403));
        hashMap.put(i.c, new x(i.c, k.MODIFY, 400));
        hashMap.put(i.g, new x(i.g, k.CANCEL, 404));
        hashMap.put(i.d, new x(i.d, k.CANCEL, 409));
        hashMap.put(i.e, new x(i.e, k.CANCEL, 501));
        hashMap.put(i.f, new x(i.f, k.MODIFY, 302));
        hashMap.put(i.h, new x(i.h, k.MODIFY, 400));
        hashMap.put(i.i, new x(i.i, k.MODIFY, 406));
        hashMap.put(i.j, new x(i.j, k.CANCEL, 405));
        hashMap.put(i.k, new x(i.k, k.AUTH, 401));
        hashMap.put(i.l, new x(i.l, k.AUTH, 402));
        hashMap.put(i.m, new x(i.m, k.WAIT, 404));
        hashMap.put(i.n, new x(i.n, k.MODIFY, 302));
        hashMap.put(i.o, new x(i.o, k.AUTH, 407));
        hashMap.put(i.q, new x(i.q, k.CANCEL, 404));
        hashMap.put(i.r, new x(i.r, k.WAIT, 504));
        hashMap.put(i.p, new x(i.p, k.CANCEL, 502));
        hashMap.put(i.s, new x(i.s, k.WAIT, 500));
        hashMap.put(i.t, new x(i.t, k.CANCEL, 503));
        hashMap.put(i.u, new x(i.u, k.AUTH, 407));
        hashMap.put(i.v, new x(i.v, k.WAIT, 500));
        hashMap.put(i.w, new x(i.w, k.WAIT, 400));
        hashMap.put(i.x, new x(i.x, k.CANCEL, 408));
        d = hashMap;
    }

    private x(i iVar, k kVar, int i) {
        this.f678a = i;
        this.b = kVar;
        this.c = iVar;
    }

    protected static x a(i iVar) {
        return (x) d.get(iVar);
    }

    /* access modifiers changed from: protected */
    public final k a() {
        return this.b;
    }

    /* access modifiers changed from: protected */
    public final int b() {
        return this.f678a;
    }
}
