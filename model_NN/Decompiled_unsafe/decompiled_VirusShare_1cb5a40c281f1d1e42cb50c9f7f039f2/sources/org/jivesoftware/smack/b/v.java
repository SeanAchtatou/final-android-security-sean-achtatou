package org.jivesoftware.smack.b;

public final class v {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public String f676a;
    /* access modifiers changed from: private */
    public String b;

    /* synthetic */ v(String str, String str2) {
        this(str, str2, (byte) 0);
    }

    private v(String str, String str2, byte b2) {
        if (str2 == null) {
            throw new NullPointerException("Message cannot be null.");
        }
        this.b = str;
        this.f676a = str2;
    }

    public final String a() {
        if (w.d.equals(this.b)) {
            return null;
        }
        return this.b;
    }

    public final String b() {
        return this.f676a;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        v vVar = (v) obj;
        if (this.b == null ? vVar.b != null : !this.b.equals(vVar.b)) {
            return false;
        }
        return this.f676a.equals(vVar.f676a);
    }

    public final int hashCode() {
        return (this.f676a.hashCode() * 31) + (this.b != null ? this.b.hashCode() : 0);
    }
}
