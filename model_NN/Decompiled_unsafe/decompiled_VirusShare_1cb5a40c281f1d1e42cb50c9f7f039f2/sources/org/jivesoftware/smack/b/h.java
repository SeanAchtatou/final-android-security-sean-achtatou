package org.jivesoftware.smack.b;

public final class h {

    /* renamed from: a  reason: collision with root package name */
    private static h f663a = new h("subscribe");
    private static h b = new h("unsubscribe");
    private String c;

    private h(String str) {
        this.c = str;
    }

    public static h a(String str) {
        if (str == null) {
            return null;
        }
        String lowerCase = str.toLowerCase();
        if ("unsubscribe".equals(lowerCase)) {
            return b;
        }
        if ("subscribe".equals(lowerCase)) {
            return f663a;
        }
        return null;
    }

    public final String toString() {
        return this.c;
    }
}
