package org.jivesoftware.smack.b;

import java.util.Collections;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;
import org.jivesoftware.smack.e.h;

public final class b {

    /* renamed from: a  reason: collision with root package name */
    private String f658a;
    private String b;
    private c c = null;
    private h d = null;
    private final Set e = new CopyOnWriteArraySet();

    public b(String str, String str2) {
        this.f658a = str.toLowerCase();
        this.b = str2;
    }

    public final String a() {
        return this.f658a;
    }

    public final void a(String str) {
        this.e.add(str);
    }

    public final void a(c cVar) {
        this.c = cVar;
    }

    public final void a(h hVar) {
        this.d = hVar;
    }

    public final String b() {
        return this.b;
    }

    public final c c() {
        return this.c;
    }

    public final h d() {
        return this.d;
    }

    public final Set e() {
        return Collections.unmodifiableSet(this.e);
    }

    public final String f() {
        StringBuilder sb = new StringBuilder();
        sb.append("<item jid=\"").append(this.f658a).append("\"");
        if (this.b != null) {
            sb.append(" name=\"").append(h.e(this.b)).append("\"");
        }
        if (this.c != null) {
            sb.append(" subscription=\"").append(this.c).append("\"");
        }
        if (this.d != null) {
            sb.append(" ask=\"").append(this.d).append("\"");
        }
        sb.append(">");
        for (String e2 : this.e) {
            sb.append("<group>").append(h.e(e2)).append("</group>");
        }
        sb.append("</item>");
        return sb.toString();
    }
}
