package org.jivesoftware.smack.b;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class t extends p {

    /* renamed from: a  reason: collision with root package name */
    private final List f675a = new ArrayList();

    public final void a(b bVar) {
        synchronized (this.f675a) {
            this.f675a.add(bVar);
        }
    }

    public final String b() {
        StringBuilder sb = new StringBuilder();
        sb.append("<query xmlns=\"jabber:iq:roster\">");
        synchronized (this.f675a) {
            for (b f : this.f675a) {
                sb.append(f.f());
            }
        }
        sb.append("</query>");
        return sb.toString();
    }

    public final Collection c() {
        List unmodifiableList;
        synchronized (this.f675a) {
            unmodifiableList = Collections.unmodifiableList(new ArrayList(this.f675a));
        }
        return unmodifiableList;
    }
}
