package org.jivesoftware.smack.b;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class s implements f {

    /* renamed from: a  reason: collision with root package name */
    private String f674a;
    private String b;
    private Map c;

    public s(String str, String str2) {
        this.f674a = str;
        this.b = str2;
    }

    public final String a() {
        return this.f674a;
    }

    public final synchronized String a(String str) {
        return this.c == null ? null : (String) this.c.get(str);
    }

    public final synchronized void a(String str, String str2) {
        if (this.c == null) {
            this.c = new HashMap();
        }
        this.c.put(str, str2);
    }

    public final String b() {
        return this.b;
    }

    public String c() {
        StringBuilder sb = new StringBuilder();
        sb.append("<").append(this.f674a).append(" xmlns=\"").append(this.b).append("\">");
        for (String str : d()) {
            String a2 = a(str);
            sb.append("<").append(str).append(">");
            sb.append(a2);
            sb.append("</").append(str).append(">");
        }
        sb.append("</").append(this.f674a).append(">");
        return sb.toString();
    }

    public final synchronized Collection d() {
        return this.c == null ? Collections.emptySet() : Collections.unmodifiableSet(new HashMap(this.c).keySet());
    }
}
