package org.jivesoftware.smack.b;

public final class a extends p {

    /* renamed from: a  reason: collision with root package name */
    private String f657a = null;
    private String b = null;

    public a() {
        a(l.b);
    }

    public final String a() {
        return this.b;
    }

    public final void a(String str) {
        this.f657a = str;
    }

    public final String b() {
        StringBuilder sb = new StringBuilder();
        sb.append("<bind xmlns=\"urn:ietf:params:xml:ns:xmpp-bind\">");
        if (this.f657a != null) {
            sb.append("<resource>").append(this.f657a).append("</resource>");
        }
        if (this.b != null) {
            sb.append("<jid>").append(this.b).append("</jid>");
        }
        sb.append("</bind>");
        return sb.toString();
    }

    public final void b(String str) {
        this.b = str;
    }
}
