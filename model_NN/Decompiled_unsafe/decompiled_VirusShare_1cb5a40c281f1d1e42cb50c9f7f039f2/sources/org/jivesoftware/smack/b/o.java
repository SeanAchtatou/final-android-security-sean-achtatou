package org.jivesoftware.smack.b;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import org.jivesoftware.smack.e.h;

public final class o extends w {

    /* renamed from: a  reason: collision with root package name */
    private j f670a = j.normal;
    private String b = null;
    private String c = null;
    private String e;
    private final Set f = new HashSet();

    public final v a(String str, String str2) {
        if (str2 == null) {
            throw new NullPointerException("Body must be specified");
        }
        v vVar = new v(i(str), str2);
        this.f.add(vVar);
        return vVar;
    }

    public final void a(String str) {
        this.b = str;
    }

    public final void a(j jVar) {
        if (jVar == null) {
            throw new IllegalArgumentException("Type cannot be null.");
        }
        this.f670a = jVar;
    }

    public final j b() {
        return this.f670a;
    }

    public final boolean b(String str) {
        String i = i(str);
        for (v vVar : this.f) {
            if (i.equals(vVar.b)) {
                return this.f.remove(vVar);
            }
        }
        return false;
    }

    public final String b_() {
        d i;
        StringBuilder sb = new StringBuilder();
        sb.append("<message");
        if (k() != null) {
            sb.append(" xmlns=\"").append(k()).append("\"");
        }
        if (this.e != null) {
            sb.append(" xml:lang=\"").append(this.e).append("\"");
        }
        if (f() != null) {
            sb.append(" id=\"").append(f()).append("\"");
        }
        if (g() != null) {
            sb.append(" to=\"").append(h.e(g())).append("\"");
        }
        if (h() != null) {
            sb.append(" from=\"").append(h.e(h())).append("\"");
        }
        if (this.f670a != j.normal) {
            sb.append(" type=\"").append(this.f670a).append("\"");
        }
        sb.append(">");
        if (this.b != null) {
            sb.append("<subject>").append(h.e(this.b)).append("</subject>");
        }
        if (d() != null) {
            sb.append("<body>").append(h.e(d())).append("</body>");
        }
        for (v vVar : Collections.unmodifiableCollection(this.f)) {
            if (!d.equals(vVar.a()) && vVar.a() != null) {
                sb.append("<body xml:lang=\"").append(vVar.a()).append("\">");
                sb.append(h.e(vVar.b()));
                sb.append("</body>");
            }
        }
        if (this.c != null) {
            sb.append("<thread>").append(this.c).append("</thread>");
        }
        if (this.f670a == j.error && (i = i()) != null) {
            sb.append(i.a());
        }
        sb.append(j());
        sb.append("</message>");
        return sb.toString();
    }

    public final String c() {
        return this.b;
    }

    public final void c(String str) {
        this.c = str;
    }

    public final String d() {
        String i = i(null);
        for (v vVar : this.f) {
            if ((vVar.b == null && i == null) || (vVar != null && vVar.b.equals(i))) {
                return vVar.f676a;
            }
        }
        return null;
    }

    public final void d(String str) {
        this.e = str;
    }

    public final String e() {
        return this.e;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        o oVar = (o) obj;
        if (!super.equals(oVar)) {
            return false;
        }
        if (this.f.size() != oVar.f.size() || !this.f.containsAll(oVar.f)) {
            return false;
        }
        if (this.e == null ? oVar.e != null : !this.e.equals(oVar.e)) {
            return false;
        }
        if (this.b == null ? oVar.b != null : !this.b.equals(oVar.b)) {
            return false;
        }
        if (this.c == null ? oVar.c != null : !this.c.equals(oVar.c)) {
            return false;
        }
        return this.f670a == oVar.f670a;
    }

    public final int hashCode() {
        return ((((((((this.f670a != null ? this.f670a.hashCode() : 0) * 31) + (this.b != null ? this.b.hashCode() : 0)) * 31) + (this.c != null ? this.c.hashCode() : 0)) * 31) + (this.e != null ? this.e.hashCode() : 0)) * 31) + this.f.hashCode();
    }
}
