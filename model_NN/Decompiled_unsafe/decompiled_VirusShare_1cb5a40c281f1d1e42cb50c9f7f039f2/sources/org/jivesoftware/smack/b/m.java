package org.jivesoftware.smack.b;

import java.util.Map;

public final class m extends p {

    /* renamed from: a  reason: collision with root package name */
    private String f668a = null;
    private Map b = null;

    public final void a(String str) {
        this.f668a = str;
    }

    public final void a(Map map) {
        this.b = map;
    }

    public final String b() {
        StringBuilder sb = new StringBuilder();
        sb.append("<query xmlns=\"jabber:iq:register\">");
        if (this.f668a != null) {
            sb.append("<instructions>").append(this.f668a).append("</instructions>");
        }
        if (this.b != null && this.b.size() > 0) {
            for (String str : this.b.keySet()) {
                sb.append("<").append(str).append(">");
                sb.append((String) this.b.get(str));
                sb.append("</").append(str).append(">");
            }
        }
        sb.append(j());
        sb.append("</query>");
        return sb.toString();
    }
}
