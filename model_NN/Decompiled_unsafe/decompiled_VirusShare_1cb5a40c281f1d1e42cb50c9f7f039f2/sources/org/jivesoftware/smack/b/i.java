package org.jivesoftware.smack.b;

public final class i {

    /* renamed from: a  reason: collision with root package name */
    public static final i f664a = new i("internal-server-error");
    public static final i b = new i("forbidden");
    public static final i c = new i("bad-request");
    public static final i d = new i("conflict");
    public static final i e = new i("feature-not-implemented");
    public static final i f = new i("gone");
    public static final i g = new i("item-not-found");
    public static final i h = new i("jid-malformed");
    public static final i i = new i("not-acceptable");
    public static final i j = new i("not-allowed");
    public static final i k = new i("not-authorized");
    public static final i l = new i("payment-required");
    public static final i m = new i("recipient-unavailable");
    public static final i n = new i("redirect");
    public static final i o = new i("registration-required");
    public static final i p = new i("remote-server-error");
    public static final i q = new i("remote-server-not-found");
    public static final i r = new i("remote-server-timeout");
    public static final i s = new i("resource-constraint");
    public static final i t = new i("service-unavailable");
    public static final i u = new i("subscription-required");
    public static final i v = new i("undefined-condition");
    public static final i w = new i("unexpected-request");
    public static final i x = new i("request-timeout");
    /* access modifiers changed from: private */
    public String y;

    private i(String str) {
        this.y = str;
    }

    public final String toString() {
        return this.y;
    }
}
