package org.jivesoftware.smack.b;

public enum r {
    available,
    unavailable,
    subscribe,
    subscribed,
    unsubscribe,
    unsubscribed,
    error
}
