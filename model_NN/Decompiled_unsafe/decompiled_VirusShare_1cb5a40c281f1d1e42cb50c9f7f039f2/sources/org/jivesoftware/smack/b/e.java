package org.jivesoftware.smack.b;

public final class e {

    /* renamed from: a  reason: collision with root package name */
    private String f661a;

    public e(String str) {
        this.f661a = str;
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("stream:error (").append(this.f661a).append(")");
        return sb.toString();
    }
}
