package org.jivesoftware.smack.b;

import java.util.Collections;
import java.util.List;

public final class d {

    /* renamed from: a  reason: collision with root package name */
    private int f660a;
    private k b;
    private String c;
    private String d;
    private List e = null;

    public d(int i, k kVar, String str, String str2, List list) {
        this.f660a = i;
        this.b = kVar;
        this.c = str;
        this.d = str2;
        this.e = list;
    }

    public d(i iVar) {
        a(iVar);
        this.d = null;
    }

    public d(i iVar, String str) {
        a(iVar);
        this.d = str;
    }

    private void a(i iVar) {
        x a2 = x.a(iVar);
        this.c = iVar.y;
        if (a2 != null) {
            this.b = a2.a();
            this.f660a = a2.b();
        }
    }

    private synchronized List b() {
        return this.e == null ? Collections.emptyList() : Collections.unmodifiableList(this.e);
    }

    public final String a() {
        StringBuilder sb = new StringBuilder();
        sb.append("<error code=\"").append(this.f660a).append("\"");
        if (this.b != null) {
            sb.append(" type=\"");
            sb.append(this.b.name());
            sb.append("\"");
        }
        sb.append(">");
        if (this.c != null) {
            sb.append("<").append(this.c);
            sb.append(" xmlns=\"urn:ietf:params:xml:ns:xmpp-stanzas\"/>");
        }
        if (this.d != null) {
            sb.append("<text xml:lang=\"en\" xmlns=\"urn:ietf:params:xml:ns:xmpp-stanzas\">");
            sb.append(this.d);
            sb.append("</text>");
        }
        for (f c2 : b()) {
            sb.append(c2.c());
        }
        sb.append("</error>");
        return sb.toString();
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder();
        if (this.c != null) {
            sb.append(this.c);
        }
        sb.append("(").append(this.f660a).append(")");
        if (this.d != null) {
            sb.append(" ").append(this.d);
        }
        return sb.toString();
    }
}
