package org.jivesoftware.smack.b;

public final class l {

    /* renamed from: a  reason: collision with root package name */
    public static final l f667a = new l("get");
    public static final l b = new l("set");
    public static final l c = new l("result");
    public static final l d = new l("error");
    private String e;

    private l(String str) {
        this.e = str;
    }

    public static l a(String str) {
        if (str == null) {
            return null;
        }
        String lowerCase = str.toLowerCase();
        if (f667a.toString().equals(lowerCase)) {
            return f667a;
        }
        if (b.toString().equals(lowerCase)) {
            return b;
        }
        if (d.toString().equals(lowerCase)) {
            return d;
        }
        if (c.toString().equals(lowerCase)) {
            return c;
        }
        return null;
    }

    public final String toString() {
        return this.e;
    }
}
