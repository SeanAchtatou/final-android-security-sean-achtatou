package org.jivesoftware.smack.b;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;
import org.jivesoftware.smack.e.h;

public abstract class w {

    /* renamed from: a  reason: collision with root package name */
    private static String f677a = null;
    private static String b = (h.a() + "-");
    private static long c = 0;
    protected static final String d = Locale.getDefault().getLanguage().toLowerCase();
    private String e = f677a;
    private String f = null;
    private String g = null;
    private String h = null;
    private final List i = new CopyOnWriteArrayList();
    private final Map j = new HashMap();
    private d k = null;

    private static synchronized String b() {
        String sb;
        synchronized (w.class) {
            StringBuilder append = new StringBuilder().append(b);
            long j2 = c;
            c = 1 + j2;
            sb = append.append(Long.toString(j2)).toString();
        }
        return sb;
    }

    private synchronized Collection c() {
        return this.i == null ? Collections.emptyList() : Collections.unmodifiableList(new ArrayList(this.i));
    }

    private synchronized Collection d() {
        return this.j == null ? Collections.emptySet() : Collections.unmodifiableSet(new HashSet(this.j.keySet()));
    }

    protected static String i(String str) {
        return (str == null || "".equals(str)) ? d : str;
    }

    public final synchronized void a(String str, Object obj) {
        if (!(obj instanceof Serializable)) {
            throw new IllegalArgumentException("Value must be serialiazble");
        }
        this.j.put(str, obj);
    }

    public final void a(d dVar) {
        this.k = dVar;
    }

    public final void a(f fVar) {
        this.i.add(fVar);
    }

    public final f b(String str, String str2) {
        if (str2 == null) {
            return null;
        }
        for (f fVar : this.i) {
            if ((str == null || str.equals(fVar.a())) && str2.equals(fVar.b())) {
                return fVar;
            }
        }
        return null;
    }

    public abstract String b_();

    public final void e(String str) {
        this.f = str;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        w wVar = (w) obj;
        if (this.k == null ? wVar.k != null : !this.k.equals(wVar.k)) {
            return false;
        }
        if (this.h == null ? wVar.h != null : !this.h.equals(wVar.h)) {
            return false;
        }
        if (!this.i.equals(wVar.i)) {
            return false;
        }
        if (this.f == null ? wVar.f != null : !this.f.equals(wVar.f)) {
            return false;
        }
        if (this.j == null ? wVar.j != null : !this.j.equals(wVar.j)) {
            return false;
        }
        if (this.g == null ? wVar.g != null : !this.g.equals(wVar.g)) {
            return false;
        }
        return this.e == null ? wVar.e == null : this.e.equals(wVar.e);
    }

    public final String f() {
        if ("ID_NOT_AVAILABLE".equals(this.f)) {
            return null;
        }
        if (this.f == null) {
            this.f = b();
        }
        return this.f;
    }

    public final void f(String str) {
        this.g = str;
    }

    public final String g() {
        return this.g;
    }

    public final void g(String str) {
        this.h = str;
    }

    public final synchronized Object h(String str) {
        return this.j == null ? null : this.j.get(str);
    }

    public final String h() {
        return this.h;
    }

    public int hashCode() {
        return ((((((((((((this.e != null ? this.e.hashCode() : 0) * 31) + (this.f != null ? this.f.hashCode() : 0)) * 31) + (this.g != null ? this.g.hashCode() : 0)) * 31) + (this.h != null ? this.h.hashCode() : 0)) * 31) + this.i.hashCode()) * 31) + this.j.hashCode()) * 31) + (this.k != null ? this.k.hashCode() : 0);
    }

    public final d i() {
        return this.k;
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x0125 A[SYNTHETIC, Splitter:B:53:0x0125] */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x012a A[SYNTHETIC, Splitter:B:56:0x012a] */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x0137 A[SYNTHETIC, Splitter:B:62:0x0137] */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x013c A[SYNTHETIC, Splitter:B:65:0x013c] */
    /* JADX WARNING: Removed duplicated region for block: B:93:0x007f A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized java.lang.String j() {
        /*
            r9 = this;
            r7 = 0
            monitor-enter(r9)
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x0023 }
            r3.<init>()     // Catch:{ all -> 0x0023 }
            java.util.Collection r1 = r9.c()     // Catch:{ all -> 0x0023 }
            java.util.Iterator r2 = r1.iterator()     // Catch:{ all -> 0x0023 }
        L_0x000f:
            boolean r1 = r2.hasNext()     // Catch:{ all -> 0x0023 }
            if (r1 == 0) goto L_0x0026
            java.lang.Object r1 = r2.next()     // Catch:{ all -> 0x0023 }
            org.jivesoftware.smack.b.f r1 = (org.jivesoftware.smack.b.f) r1     // Catch:{ all -> 0x0023 }
            java.lang.String r1 = r1.c()     // Catch:{ all -> 0x0023 }
            r3.append(r1)     // Catch:{ all -> 0x0023 }
            goto L_0x000f
        L_0x0023:
            r1 = move-exception
            monitor-exit(r9)
            throw r1
        L_0x0026:
            java.util.Map r1 = r9.j     // Catch:{ all -> 0x0023 }
            if (r1 == 0) goto L_0x0145
            java.util.Map r1 = r9.j     // Catch:{ all -> 0x0023 }
            boolean r1 = r1.isEmpty()     // Catch:{ all -> 0x0023 }
            if (r1 != 0) goto L_0x0145
            java.lang.String r1 = "<properties xmlns=\"http://www.jivesoftware.com/xmlns/xmpp/properties\">"
            r3.append(r1)     // Catch:{ all -> 0x0023 }
            java.util.Collection r1 = r9.d()     // Catch:{ all -> 0x0023 }
            java.util.Iterator r4 = r1.iterator()     // Catch:{ all -> 0x0023 }
        L_0x003f:
            boolean r1 = r4.hasNext()     // Catch:{ all -> 0x0023 }
            if (r1 == 0) goto L_0x0140
            java.lang.Object r1 = r4.next()     // Catch:{ all -> 0x0023 }
            java.lang.String r1 = (java.lang.String) r1     // Catch:{ all -> 0x0023 }
            java.lang.Object r2 = r9.h(r1)     // Catch:{ all -> 0x0023 }
            java.lang.String r5 = "<property>"
            r3.append(r5)     // Catch:{ all -> 0x0023 }
            java.lang.String r5 = "<name>"
            java.lang.StringBuilder r5 = r3.append(r5)     // Catch:{ all -> 0x0023 }
            java.lang.String r1 = org.jivesoftware.smack.e.h.e(r1)     // Catch:{ all -> 0x0023 }
            java.lang.StringBuilder r1 = r5.append(r1)     // Catch:{ all -> 0x0023 }
            java.lang.String r5 = "</name>"
            r1.append(r5)     // Catch:{ all -> 0x0023 }
            java.lang.String r1 = "<value type=\""
            r3.append(r1)     // Catch:{ all -> 0x0023 }
            boolean r1 = r2 instanceof java.lang.Integer     // Catch:{ all -> 0x0023 }
            if (r1 == 0) goto L_0x0085
            java.lang.String r1 = "integer\">"
            java.lang.StringBuilder r1 = r3.append(r1)     // Catch:{ all -> 0x0023 }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x0023 }
            java.lang.String r2 = "</value>"
            r1.append(r2)     // Catch:{ all -> 0x0023 }
        L_0x007f:
            java.lang.String r1 = "</property>"
            r3.append(r1)     // Catch:{ all -> 0x0023 }
            goto L_0x003f
        L_0x0085:
            boolean r1 = r2 instanceof java.lang.Long     // Catch:{ all -> 0x0023 }
            if (r1 == 0) goto L_0x0099
            java.lang.String r1 = "long\">"
            java.lang.StringBuilder r1 = r3.append(r1)     // Catch:{ all -> 0x0023 }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x0023 }
            java.lang.String r2 = "</value>"
            r1.append(r2)     // Catch:{ all -> 0x0023 }
            goto L_0x007f
        L_0x0099:
            boolean r1 = r2 instanceof java.lang.Float     // Catch:{ all -> 0x0023 }
            if (r1 == 0) goto L_0x00ad
            java.lang.String r1 = "float\">"
            java.lang.StringBuilder r1 = r3.append(r1)     // Catch:{ all -> 0x0023 }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x0023 }
            java.lang.String r2 = "</value>"
            r1.append(r2)     // Catch:{ all -> 0x0023 }
            goto L_0x007f
        L_0x00ad:
            boolean r1 = r2 instanceof java.lang.Double     // Catch:{ all -> 0x0023 }
            if (r1 == 0) goto L_0x00c1
            java.lang.String r1 = "double\">"
            java.lang.StringBuilder r1 = r3.append(r1)     // Catch:{ all -> 0x0023 }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x0023 }
            java.lang.String r2 = "</value>"
            r1.append(r2)     // Catch:{ all -> 0x0023 }
            goto L_0x007f
        L_0x00c1:
            boolean r1 = r2 instanceof java.lang.Boolean     // Catch:{ all -> 0x0023 }
            if (r1 == 0) goto L_0x00d5
            java.lang.String r1 = "boolean\">"
            java.lang.StringBuilder r1 = r3.append(r1)     // Catch:{ all -> 0x0023 }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x0023 }
            java.lang.String r2 = "</value>"
            r1.append(r2)     // Catch:{ all -> 0x0023 }
            goto L_0x007f
        L_0x00d5:
            boolean r1 = r2 instanceof java.lang.String     // Catch:{ all -> 0x0023 }
            if (r1 == 0) goto L_0x00ef
            java.lang.String r1 = "string\">"
            r3.append(r1)     // Catch:{ all -> 0x0023 }
            r0 = r2
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ all -> 0x0023 }
            r1 = r0
            java.lang.String r1 = org.jivesoftware.smack.e.h.e(r1)     // Catch:{ all -> 0x0023 }
            r3.append(r1)     // Catch:{ all -> 0x0023 }
            java.lang.String r1 = "</value>"
            r3.append(r1)     // Catch:{ all -> 0x0023 }
            goto L_0x007f
        L_0x00ef:
            java.io.ByteArrayOutputStream r1 = new java.io.ByteArrayOutputStream     // Catch:{ Exception -> 0x011d, all -> 0x0132 }
            r1.<init>()     // Catch:{ Exception -> 0x011d, all -> 0x0132 }
            java.io.ObjectOutputStream r5 = new java.io.ObjectOutputStream     // Catch:{ Exception -> 0x0160, all -> 0x0153 }
            r5.<init>(r1)     // Catch:{ Exception -> 0x0160, all -> 0x0153 }
            r5.writeObject(r2)     // Catch:{ Exception -> 0x0165, all -> 0x0158 }
            java.lang.String r2 = "java-object\">"
            r3.append(r2)     // Catch:{ Exception -> 0x0165, all -> 0x0158 }
            byte[] r2 = r1.toByteArray()     // Catch:{ Exception -> 0x0165, all -> 0x0158 }
            java.lang.String r2 = org.jivesoftware.smack.e.h.a(r2)     // Catch:{ Exception -> 0x0165, all -> 0x0158 }
            java.lang.StringBuilder r2 = r3.append(r2)     // Catch:{ Exception -> 0x0165, all -> 0x0158 }
            java.lang.String r6 = "</value>"
            r2.append(r6)     // Catch:{ Exception -> 0x0165, all -> 0x0158 }
            r5.close()     // Catch:{ Exception -> 0x014b }
        L_0x0115:
            r1.close()     // Catch:{ Exception -> 0x011a }
            goto L_0x007f
        L_0x011a:
            r1 = move-exception
            goto L_0x007f
        L_0x011d:
            r1 = move-exception
            r2 = r7
            r5 = r7
        L_0x0120:
            r1.printStackTrace()     // Catch:{ all -> 0x015d }
            if (r2 == 0) goto L_0x0128
            r2.close()     // Catch:{ Exception -> 0x014d }
        L_0x0128:
            if (r5 == 0) goto L_0x007f
            r5.close()     // Catch:{ Exception -> 0x012f }
            goto L_0x007f
        L_0x012f:
            r1 = move-exception
            goto L_0x007f
        L_0x0132:
            r1 = move-exception
            r2 = r7
            r3 = r7
        L_0x0135:
            if (r2 == 0) goto L_0x013a
            r2.close()     // Catch:{ Exception -> 0x014f }
        L_0x013a:
            if (r3 == 0) goto L_0x013f
            r3.close()     // Catch:{ Exception -> 0x0151 }
        L_0x013f:
            throw r1     // Catch:{ all -> 0x0023 }
        L_0x0140:
            java.lang.String r1 = "</properties>"
            r3.append(r1)     // Catch:{ all -> 0x0023 }
        L_0x0145:
            java.lang.String r1 = r3.toString()     // Catch:{ all -> 0x0023 }
            monitor-exit(r9)
            return r1
        L_0x014b:
            r2 = move-exception
            goto L_0x0115
        L_0x014d:
            r1 = move-exception
            goto L_0x0128
        L_0x014f:
            r2 = move-exception
            goto L_0x013a
        L_0x0151:
            r2 = move-exception
            goto L_0x013f
        L_0x0153:
            r2 = move-exception
            r3 = r1
            r1 = r2
            r2 = r7
            goto L_0x0135
        L_0x0158:
            r2 = move-exception
            r3 = r1
            r1 = r2
            r2 = r5
            goto L_0x0135
        L_0x015d:
            r1 = move-exception
            r3 = r5
            goto L_0x0135
        L_0x0160:
            r2 = move-exception
            r5 = r1
            r1 = r2
            r2 = r7
            goto L_0x0120
        L_0x0165:
            r2 = move-exception
            r8 = r2
            r2 = r5
            r5 = r1
            r1 = r8
            goto L_0x0120
        */
        throw new UnsupportedOperationException("Method not decompiled: org.jivesoftware.smack.b.w.j():java.lang.String");
    }

    public final String k() {
        return this.e;
    }
}
