package org.jivesoftware.smack;

import java.io.Reader;
import java.io.Writer;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.concurrent.atomic.AtomicInteger;
import org.jivesoftware.smack.b.w;
import org.jivesoftware.smack.g.a;

public abstract class Connection {

    /* renamed from: a  reason: collision with root package name */
    public static boolean f648a;
    private static final AtomicInteger l = new AtomicInteger(0);
    private static final Set m = new CopyOnWriteArraySet();
    protected final Collection b = new ConcurrentLinkedQueue();
    protected final Map c = new ConcurrentHashMap();
    protected final Map d = new ConcurrentHashMap();
    protected final Map e = new ConcurrentHashMap();
    protected a f = null;
    protected Reader g;
    protected Writer h;
    protected k i = new k(this);
    protected final int j = l.getAndIncrement();
    protected final d k;
    private Collection n = new CopyOnWriteArrayList();
    private v o = null;
    private c p = null;

    static {
        f648a = false;
        try {
            f648a = Boolean.getBoolean("smack.debugEnabled");
        } catch (Exception e2) {
        }
    }

    protected Connection(d dVar) {
        this.k = dVar;
    }

    public static void a(f fVar) {
        m.add(fVar);
    }

    public static void b(f fVar) {
        m.remove(fVar);
    }

    protected static Collection j() {
        return Collections.unmodifiableCollection(m);
    }

    /* access modifiers changed from: protected */
    public final d a() {
        return this.k;
    }

    public final l a(org.jivesoftware.smack.a.a aVar) {
        l lVar = new l(this, aVar);
        this.b.add(lVar);
        return lVar;
    }

    public final void a(ab abVar, org.jivesoftware.smack.a.a aVar) {
        if (abVar == null) {
            throw new NullPointerException("Packet listener is null.");
        }
        this.c.put(abVar, new w(abVar, aVar));
    }

    public abstract void a(w wVar);

    /* access modifiers changed from: protected */
    public final void a(l lVar) {
        this.b.remove(lVar);
    }

    public final void a(t tVar) {
        if (!f()) {
            throw new IllegalStateException("Not connected to server.");
        } else if (tVar != null && !this.n.contains(tVar)) {
            this.n.add(tVar);
        }
    }

    public final String b() {
        return this.k.a();
    }

    /* access modifiers changed from: protected */
    public final void b(w wVar) {
        for (w a2 : this.d.values()) {
            a2.a(wVar);
        }
    }

    public final String c() {
        return this.k.b();
    }

    /* access modifiers changed from: protected */
    public final void c(w wVar) {
        if (wVar != null) {
            for (j a2 : this.e.values()) {
                a2.a(wVar);
            }
        }
    }

    public final int d() {
        return this.k.c();
    }

    public abstract String e();

    public abstract boolean f();

    public final v g() {
        if (this.o == null) {
            this.o = new v(this);
        }
        return this.o;
    }

    public abstract ac h();

    public final k i() {
        return this.i;
    }

    /* access modifiers changed from: protected */
    public void initDebugger() {
        if (this.g == null || this.h == null) {
            throw new NullPointerException("Reader or writer isn't initialized.");
        } else if (!this.k.p()) {
        } else {
            if (this.f == null) {
                try {
                    System.getProperty("smack.debuggerClass");
                } catch (Throwable th) {
                }
                try {
                    this.f = new a.a.a.a(this, this.h, this.g);
                    this.g = this.f.a();
                    this.h = this.f.b();
                } catch (Exception e2) {
                    throw new IllegalArgumentException("Can't initialize the configured debugger!", e2);
                }
            } else {
                this.g = this.f.a(this.g);
                this.h = this.f.a(this.h);
            }
        }
    }

    /* access modifiers changed from: protected */
    public final Collection k() {
        return this.n;
    }

    /* access modifiers changed from: protected */
    public final Collection l() {
        return this.b;
    }
}
