package org.jivesoftware.smack.a;

import org.jivesoftware.smack.b.w;

public final class c implements a {

    /* renamed from: a  reason: collision with root package name */
    private String f651a;

    public c(String str) {
        if (str == null) {
            throw new IllegalArgumentException("Packet ID cannot be null.");
        }
        this.f651a = str;
    }

    public final boolean a(w wVar) {
        return this.f651a.equals(wVar.f());
    }

    public final String toString() {
        return "PacketIDFilter by id: " + this.f651a;
    }
}
