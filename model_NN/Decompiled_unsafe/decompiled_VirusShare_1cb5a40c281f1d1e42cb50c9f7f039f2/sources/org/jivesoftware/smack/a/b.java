package org.jivesoftware.smack.a;

import org.jivesoftware.smack.b.w;

public final class b implements a {

    /* renamed from: a  reason: collision with root package name */
    private Class f650a;

    public b(Class cls) {
        if (!w.class.isAssignableFrom(cls)) {
            throw new IllegalArgumentException("Packet type must be a sub-class of Packet.");
        }
        this.f650a = cls;
    }

    public final boolean a(w wVar) {
        return this.f650a.isInstance(wVar);
    }

    public final String toString() {
        return "PacketTypeFilter: " + this.f650a.getName();
    }
}
