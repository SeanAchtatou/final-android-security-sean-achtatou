package org.jivesoftware.smack;

import android.os.SystemClock;
import java.io.BufferedReader;
import java.io.Reader;

public final class m extends BufferedReader {
    public m(Reader reader) {
        super(reader);
    }

    public final int read(char[] cArr, int i, int i2) {
        h.f700a = SystemClock.elapsedRealtime();
        return super.read(cArr, i, i2);
    }
}
