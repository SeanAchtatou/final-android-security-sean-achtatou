package org.jivesoftware.smack.f;

import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import org.jivesoftware.smack.b.f;
import org.jivesoftware.smack.b.p;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

public class c {

    /* renamed from: a  reason: collision with root package name */
    private static c f698a;
    private Map b = new ConcurrentHashMap();
    private Map c = new ConcurrentHashMap();

    private c() {
        b();
    }

    public static synchronized c a() {
        c cVar;
        synchronized (c.class) {
            if (f698a == null) {
                f698a = new c();
            }
            cVar = f698a;
        }
        return cVar;
    }

    private void b() {
        InputStream inputStream;
        try {
            ClassLoader[] classLoaderArr = {c.class.getClassLoader(), Thread.currentThread().getContextClassLoader()};
            ArrayList arrayList = new ArrayList();
            for (ClassLoader classLoader : classLoaderArr) {
                if (classLoader != null) {
                    arrayList.add(classLoader);
                }
            }
            for (ClassLoader resources : (ClassLoader[]) arrayList.toArray(new ClassLoader[arrayList.size()])) {
                Enumeration<URL> resources2 = resources.getResources("META-INF/smack.providers");
                while (resources2.hasMoreElements()) {
                    try {
                        inputStream = resources2.nextElement().openStream();
                        try {
                            XmlPullParser newPullParser = XmlPullParserFactory.newInstance().newPullParser();
                            newPullParser.setFeature("http://xmlpull.org/v1/doc/features.html#process-namespaces", true);
                            newPullParser.setInput(inputStream, "UTF-8");
                            int eventType = newPullParser.getEventType();
                            do {
                                if (eventType == 2) {
                                    if (newPullParser.getName().equals("iqProvider")) {
                                        newPullParser.next();
                                        newPullParser.next();
                                        String nextText = newPullParser.nextText();
                                        newPullParser.next();
                                        newPullParser.next();
                                        String nextText2 = newPullParser.nextText();
                                        newPullParser.next();
                                        newPullParser.next();
                                        String nextText3 = newPullParser.nextText();
                                        String c2 = c(nextText, nextText2);
                                        if (!this.c.containsKey(c2)) {
                                            Class<?> cls = Class.forName(nextText3);
                                            if (b.class.isAssignableFrom(cls)) {
                                                this.c.put(c2, cls.newInstance());
                                            } else if (p.class.isAssignableFrom(cls)) {
                                                this.c.put(c2, cls);
                                            }
                                        }
                                    } else if (newPullParser.getName().equals("extensionProvider")) {
                                        newPullParser.next();
                                        newPullParser.next();
                                        String nextText4 = newPullParser.nextText();
                                        newPullParser.next();
                                        newPullParser.next();
                                        String nextText5 = newPullParser.nextText();
                                        newPullParser.next();
                                        newPullParser.next();
                                        String nextText6 = newPullParser.nextText();
                                        String c3 = c(nextText4, nextText5);
                                        if (!this.b.containsKey(c3)) {
                                            try {
                                                Class<?> cls2 = Class.forName(nextText6);
                                                if (a.class.isAssignableFrom(cls2)) {
                                                    this.b.put(c3, cls2.newInstance());
                                                } else if (f.class.isAssignableFrom(cls2)) {
                                                    this.b.put(c3, cls2);
                                                }
                                            } catch (ClassNotFoundException e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    }
                                }
                                eventType = newPullParser.next();
                            } while (eventType != 1);
                        } catch (ClassNotFoundException e2) {
                            e2.printStackTrace();
                        } catch (Throwable th) {
                            th = th;
                        }
                        try {
                            inputStream.close();
                        } catch (Exception e3) {
                        }
                    } catch (Throwable th2) {
                        th = th2;
                        inputStream = null;
                    }
                }
            }
            return;
            try {
                inputStream.close();
            } catch (Exception e4) {
            }
            throw th;
            throw th;
        } catch (Exception e5) {
            e5.printStackTrace();
        }
    }

    private static String c(String str, String str2) {
        StringBuilder sb = new StringBuilder();
        sb.append("<").append(str).append("/><").append(str2).append("/>");
        return sb.toString();
    }

    public final Object a(String str, String str2) {
        return this.c.get(c(str, str2));
    }

    public final void a(String str, String str2, Object obj) {
        if ((obj instanceof b) || ((obj instanceof Class) && p.class.isAssignableFrom((Class) obj))) {
            this.c.put(c(str, str2), obj);
            return;
        }
        throw new IllegalArgumentException("Provider must be an IQProvider or a Class instance.");
    }

    public final Object b(String str, String str2) {
        return this.b.get(c(str, str2));
    }

    public final void b(String str, String str2, Object obj) {
        if ((obj instanceof a) || (obj instanceof Class)) {
            this.b.put(c(str, str2), obj);
            return;
        }
        throw new IllegalArgumentException("Provider must be a PacketExtensionProvider or a Class instance.");
    }
}
