package org.jivesoftware.smack;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import org.jivesoftware.smack.b.c;
import org.jivesoftware.smack.b.h;

public final class p {

    /* renamed from: a  reason: collision with root package name */
    private String f707a;
    private String b;
    private c c;
    private h d;
    private Connection e;

    p(String str, String str2, c cVar, h hVar, Connection connection) {
        this.f707a = str;
        this.b = str2;
        this.c = cVar;
        this.d = hVar;
        this.e = connection;
    }

    private Collection a() {
        ArrayList arrayList = new ArrayList();
        for (x xVar : this.e.h().b()) {
            if (xVar.a(this)) {
                arrayList.add(xVar);
            }
        }
        return Collections.unmodifiableCollection(arrayList);
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || !(obj instanceof p)) {
            return false;
        }
        return this.f707a.equals(((p) obj).f707a);
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder();
        if (this.b != null) {
            sb.append(this.b).append(": ");
        }
        sb.append(this.f707a);
        Collection a2 = a();
        if (!a2.isEmpty()) {
            sb.append(" [");
            Iterator it = a2.iterator();
            sb.append(((x) it.next()).a());
            while (it.hasNext()) {
                sb.append(", ");
                sb.append(((x) it.next()).a());
            }
            sb.append("]");
        }
        return sb.toString();
    }
}
