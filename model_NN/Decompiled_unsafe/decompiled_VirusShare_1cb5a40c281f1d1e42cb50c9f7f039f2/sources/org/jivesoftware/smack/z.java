package org.jivesoftware.smack;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import org.jivesoftware.smack.b.w;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

final class z {

    /* renamed from: a  reason: collision with root package name */
    private Thread f715a;
    private ExecutorService b;
    /* access modifiers changed from: private */
    public XMPPConnection c;
    private XmlPullParser d;
    private boolean e;
    private String f = null;
    private Semaphore g;

    protected z(XMPPConnection xMPPConnection) {
        this.c = xMPPConnection;
        a();
    }

    private void a(w wVar) {
        if (wVar != null) {
            for (l a2 : this.c.l()) {
                a2.a(wVar);
            }
            this.b.submit(new i(this, wVar));
        }
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static /* synthetic */ void a(org.jivesoftware.smack.z r13, java.lang.Thread r14) {
        /*
            r12 = 3
            r11 = 2
            r10 = 1
            r9 = 0
            org.xmlpull.v1.XmlPullParser r0 = r13.d     // Catch:{ Exception -> 0x004e }
            int r0 = r0.getEventType()     // Catch:{ Exception -> 0x004e }
        L_0x000a:
            if (r0 != r11) goto L_0x0313
            org.xmlpull.v1.XmlPullParser r0 = r13.d     // Catch:{ Exception -> 0x004e }
            java.lang.String r0 = r0.getName()     // Catch:{ Exception -> 0x004e }
            java.lang.String r1 = "message"
            boolean r0 = r0.equals(r1)     // Catch:{ Exception -> 0x004e }
            if (r0 == 0) goto L_0x0034
            org.xmlpull.v1.XmlPullParser r0 = r13.d     // Catch:{ Exception -> 0x004e }
            org.jivesoftware.smack.b.w r0 = org.jivesoftware.smack.e.f.a(r0)     // Catch:{ Exception -> 0x004e }
            r13.a(r0)     // Catch:{ Exception -> 0x004e }
        L_0x0023:
            org.xmlpull.v1.XmlPullParser r0 = r13.d     // Catch:{ Exception -> 0x004e }
            int r0 = r0.next()     // Catch:{ Exception -> 0x004e }
            boolean r1 = r13.e     // Catch:{ Exception -> 0x004e }
            if (r1 != 0) goto L_0x0033
            if (r0 == r10) goto L_0x0033
            java.lang.Thread r1 = r13.f715a     // Catch:{ Exception -> 0x004e }
            if (r14 == r1) goto L_0x000a
        L_0x0033:
            return
        L_0x0034:
            org.xmlpull.v1.XmlPullParser r0 = r13.d     // Catch:{ Exception -> 0x004e }
            java.lang.String r0 = r0.getName()     // Catch:{ Exception -> 0x004e }
            java.lang.String r1 = "iq"
            boolean r0 = r0.equals(r1)     // Catch:{ Exception -> 0x004e }
            if (r0 == 0) goto L_0x0057
            org.xmlpull.v1.XmlPullParser r0 = r13.d     // Catch:{ Exception -> 0x004e }
            org.jivesoftware.smack.XMPPConnection r1 = r13.c     // Catch:{ Exception -> 0x004e }
            org.jivesoftware.smack.b.p r0 = org.jivesoftware.smack.e.f.a(r0, r1)     // Catch:{ Exception -> 0x004e }
            r13.a(r0)     // Catch:{ Exception -> 0x004e }
            goto L_0x0023
        L_0x004e:
            r0 = move-exception
            boolean r1 = r13.e
            if (r1 != 0) goto L_0x0033
            r13.a(r0)
            goto L_0x0033
        L_0x0057:
            org.xmlpull.v1.XmlPullParser r0 = r13.d     // Catch:{ Exception -> 0x004e }
            java.lang.String r0 = r0.getName()     // Catch:{ Exception -> 0x004e }
            java.lang.String r1 = "presence"
            boolean r0 = r0.equals(r1)     // Catch:{ Exception -> 0x004e }
            if (r0 == 0) goto L_0x006f
            org.xmlpull.v1.XmlPullParser r0 = r13.d     // Catch:{ Exception -> 0x004e }
            org.jivesoftware.smack.b.q r0 = org.jivesoftware.smack.e.f.b(r0)     // Catch:{ Exception -> 0x004e }
            r13.a(r0)     // Catch:{ Exception -> 0x004e }
            goto L_0x0023
        L_0x006f:
            org.xmlpull.v1.XmlPullParser r0 = r13.d     // Catch:{ Exception -> 0x004e }
            java.lang.String r0 = r0.getName()     // Catch:{ Exception -> 0x004e }
            java.lang.String r1 = "stream"
            boolean r0 = r0.equals(r1)     // Catch:{ Exception -> 0x004e }
            if (r0 == 0) goto L_0x00e1
            java.lang.String r0 = "jabber:client"
            org.xmlpull.v1.XmlPullParser r1 = r13.d     // Catch:{ Exception -> 0x004e }
            r2 = 0
            java.lang.String r1 = r1.getNamespace(r2)     // Catch:{ Exception -> 0x004e }
            boolean r0 = r0.equals(r1)     // Catch:{ Exception -> 0x004e }
            if (r0 == 0) goto L_0x0023
            r0 = r9
        L_0x008d:
            org.xmlpull.v1.XmlPullParser r1 = r13.d     // Catch:{ Exception -> 0x004e }
            int r1 = r1.getAttributeCount()     // Catch:{ Exception -> 0x004e }
            if (r0 >= r1) goto L_0x0023
            org.xmlpull.v1.XmlPullParser r1 = r13.d     // Catch:{ Exception -> 0x004e }
            java.lang.String r1 = r1.getAttributeName(r0)     // Catch:{ Exception -> 0x004e }
            java.lang.String r2 = "id"
            boolean r1 = r1.equals(r2)     // Catch:{ Exception -> 0x004e }
            if (r1 == 0) goto L_0x00c5
            org.xmlpull.v1.XmlPullParser r1 = r13.d     // Catch:{ Exception -> 0x004e }
            java.lang.String r1 = r1.getAttributeValue(r0)     // Catch:{ Exception -> 0x004e }
            r13.f = r1     // Catch:{ Exception -> 0x004e }
            java.lang.String r1 = "1.0"
            org.xmlpull.v1.XmlPullParser r2 = r13.d     // Catch:{ Exception -> 0x004e }
            java.lang.String r3 = ""
            java.lang.String r4 = "version"
            java.lang.String r2 = r2.getAttributeValue(r3, r4)     // Catch:{ Exception -> 0x004e }
            boolean r1 = r1.equals(r2)     // Catch:{ Exception -> 0x004e }
            if (r1 != 0) goto L_0x00c2
            java.util.concurrent.Semaphore r1 = r13.g     // Catch:{ Exception -> 0x004e }
            r1.release()     // Catch:{ Exception -> 0x004e }
        L_0x00c2:
            int r0 = r0 + 1
            goto L_0x008d
        L_0x00c5:
            org.xmlpull.v1.XmlPullParser r1 = r13.d     // Catch:{ Exception -> 0x004e }
            java.lang.String r1 = r1.getAttributeName(r0)     // Catch:{ Exception -> 0x004e }
            java.lang.String r2 = "from"
            boolean r1 = r1.equals(r2)     // Catch:{ Exception -> 0x004e }
            if (r1 == 0) goto L_0x00c2
            org.jivesoftware.smack.XMPPConnection r1 = r13.c     // Catch:{ Exception -> 0x004e }
            org.jivesoftware.smack.d r1 = r1.k     // Catch:{ Exception -> 0x004e }
            org.xmlpull.v1.XmlPullParser r2 = r13.d     // Catch:{ Exception -> 0x004e }
            java.lang.String r2 = r2.getAttributeValue(r0)     // Catch:{ Exception -> 0x004e }
            r1.a(r2)     // Catch:{ Exception -> 0x004e }
            goto L_0x00c2
        L_0x00e1:
            org.xmlpull.v1.XmlPullParser r0 = r13.d     // Catch:{ Exception -> 0x004e }
            java.lang.String r0 = r0.getName()     // Catch:{ Exception -> 0x004e }
            java.lang.String r1 = "error"
            boolean r0 = r0.equals(r1)     // Catch:{ Exception -> 0x004e }
            if (r0 == 0) goto L_0x00fb
            org.jivesoftware.smack.ad r0 = new org.jivesoftware.smack.ad     // Catch:{ Exception -> 0x004e }
            org.xmlpull.v1.XmlPullParser r1 = r13.d     // Catch:{ Exception -> 0x004e }
            org.jivesoftware.smack.b.e r1 = org.jivesoftware.smack.e.f.e(r1)     // Catch:{ Exception -> 0x004e }
            r0.<init>(r1)     // Catch:{ Exception -> 0x004e }
            throw r0     // Catch:{ Exception -> 0x004e }
        L_0x00fb:
            org.xmlpull.v1.XmlPullParser r0 = r13.d     // Catch:{ Exception -> 0x004e }
            java.lang.String r0 = r0.getName()     // Catch:{ Exception -> 0x004e }
            java.lang.String r1 = "features"
            boolean r0 = r0.equals(r1)     // Catch:{ Exception -> 0x004e }
            if (r0 == 0) goto L_0x023a
            org.xmlpull.v1.XmlPullParser r0 = r13.d     // Catch:{ Exception -> 0x004e }
            r1 = r9
            r2 = r9
            r3 = r9
        L_0x010e:
            if (r1 != 0) goto L_0x01fc
            int r4 = r0.next()     // Catch:{ Exception -> 0x004e }
            if (r4 != r11) goto L_0x01c7
            java.lang.String r4 = r0.getName()     // Catch:{ Exception -> 0x004e }
            java.lang.String r5 = "starttls"
            boolean r4 = r4.equals(r5)     // Catch:{ Exception -> 0x004e }
            if (r4 == 0) goto L_0x0124
            r3 = r10
            goto L_0x010e
        L_0x0124:
            java.lang.String r4 = r0.getName()     // Catch:{ Exception -> 0x004e }
            java.lang.String r5 = "mechanisms"
            boolean r4 = r4.equals(r5)     // Catch:{ Exception -> 0x004e }
            if (r4 == 0) goto L_0x016c
            org.jivesoftware.smack.XMPPConnection r4 = r13.c     // Catch:{ Exception -> 0x004e }
            org.jivesoftware.smack.k r4 = r4.i()     // Catch:{ Exception -> 0x004e }
            java.util.ArrayList r5 = new java.util.ArrayList     // Catch:{ Exception -> 0x004e }
            r5.<init>()     // Catch:{ Exception -> 0x004e }
            r6 = r9
        L_0x013c:
            if (r6 != 0) goto L_0x0168
            int r7 = r0.next()     // Catch:{ Exception -> 0x004e }
            if (r7 != r11) goto L_0x0158
            java.lang.String r7 = r0.getName()     // Catch:{ Exception -> 0x004e }
            java.lang.String r8 = "mechanism"
            boolean r7 = r7.equals(r8)     // Catch:{ Exception -> 0x004e }
            if (r7 == 0) goto L_0x013c
            java.lang.String r7 = r0.nextText()     // Catch:{ Exception -> 0x004e }
            r5.add(r7)     // Catch:{ Exception -> 0x004e }
            goto L_0x013c
        L_0x0158:
            if (r7 != r12) goto L_0x013c
            java.lang.String r7 = r0.getName()     // Catch:{ Exception -> 0x004e }
            java.lang.String r8 = "mechanisms"
            boolean r7 = r7.equals(r8)     // Catch:{ Exception -> 0x004e }
            if (r7 == 0) goto L_0x013c
            r6 = r10
            goto L_0x013c
        L_0x0168:
            r4.a(r5)     // Catch:{ Exception -> 0x004e }
            goto L_0x010e
        L_0x016c:
            java.lang.String r4 = r0.getName()     // Catch:{ Exception -> 0x004e }
            java.lang.String r5 = "bind"
            boolean r4 = r4.equals(r5)     // Catch:{ Exception -> 0x004e }
            if (r4 == 0) goto L_0x0182
            org.jivesoftware.smack.XMPPConnection r4 = r13.c     // Catch:{ Exception -> 0x004e }
            org.jivesoftware.smack.k r4 = r4.i()     // Catch:{ Exception -> 0x004e }
            r4.c()     // Catch:{ Exception -> 0x004e }
            goto L_0x010e
        L_0x0182:
            java.lang.String r4 = r0.getName()     // Catch:{ Exception -> 0x004e }
            java.lang.String r5 = "session"
            boolean r4 = r4.equals(r5)     // Catch:{ Exception -> 0x004e }
            if (r4 == 0) goto L_0x0199
            org.jivesoftware.smack.XMPPConnection r4 = r13.c     // Catch:{ Exception -> 0x004e }
            org.jivesoftware.smack.k r4 = r4.i()     // Catch:{ Exception -> 0x004e }
            r4.d()     // Catch:{ Exception -> 0x004e }
            goto L_0x010e
        L_0x0199:
            java.lang.String r4 = r0.getName()     // Catch:{ Exception -> 0x004e }
            java.lang.String r5 = "compression"
            boolean r4 = r4.equals(r5)     // Catch:{ Exception -> 0x004e }
            if (r4 == 0) goto L_0x01b0
            org.jivesoftware.smack.XMPPConnection r4 = r13.c     // Catch:{ Exception -> 0x004e }
            java.util.Collection r5 = org.jivesoftware.smack.e.f.c(r0)     // Catch:{ Exception -> 0x004e }
            r4.a(r5)     // Catch:{ Exception -> 0x004e }
            goto L_0x010e
        L_0x01b0:
            java.lang.String r4 = r0.getName()     // Catch:{ Exception -> 0x004e }
            java.lang.String r5 = "register"
            boolean r4 = r4.equals(r5)     // Catch:{ Exception -> 0x004e }
            if (r4 == 0) goto L_0x010e
            org.jivesoftware.smack.XMPPConnection r4 = r13.c     // Catch:{ Exception -> 0x004e }
            org.jivesoftware.smack.v r4 = r4.g()     // Catch:{ Exception -> 0x004e }
            r4.a()     // Catch:{ Exception -> 0x004e }
            goto L_0x010e
        L_0x01c7:
            if (r4 != r12) goto L_0x010e
            java.lang.String r4 = r0.getName()     // Catch:{ Exception -> 0x004e }
            java.lang.String r5 = "starttls"
            boolean r4 = r4.equals(r5)     // Catch:{ Exception -> 0x004e }
            if (r4 == 0) goto L_0x01dc
            org.jivesoftware.smack.XMPPConnection r4 = r13.c     // Catch:{ Exception -> 0x004e }
            r4.a(r2)     // Catch:{ Exception -> 0x004e }
            goto L_0x010e
        L_0x01dc:
            java.lang.String r4 = r0.getName()     // Catch:{ Exception -> 0x004e }
            java.lang.String r5 = "required"
            boolean r4 = r4.equals(r5)     // Catch:{ Exception -> 0x004e }
            if (r4 == 0) goto L_0x01ed
            if (r3 == 0) goto L_0x01ed
            r2 = r10
            goto L_0x010e
        L_0x01ed:
            java.lang.String r4 = r0.getName()     // Catch:{ Exception -> 0x004e }
            java.lang.String r5 = "features"
            boolean r4 = r4.equals(r5)     // Catch:{ Exception -> 0x004e }
            if (r4 == 0) goto L_0x010e
            r1 = r10
            goto L_0x010e
        L_0x01fc:
            org.jivesoftware.smack.XMPPConnection r0 = r13.c     // Catch:{ Exception -> 0x004e }
            boolean r0 = r0.p()     // Catch:{ Exception -> 0x004e }
            if (r0 != 0) goto L_0x0223
            if (r3 != 0) goto L_0x0223
            org.jivesoftware.smack.XMPPConnection r0 = r13.c     // Catch:{ Exception -> 0x004e }
            org.jivesoftware.smack.d r0 = r0.a()     // Catch:{ Exception -> 0x004e }
            org.jivesoftware.smack.q r0 = r0.d()     // Catch:{ Exception -> 0x004e }
            org.jivesoftware.smack.q r1 = org.jivesoftware.smack.q.required     // Catch:{ Exception -> 0x004e }
            if (r0 != r1) goto L_0x0223
            org.jivesoftware.smack.ad r0 = new org.jivesoftware.smack.ad     // Catch:{ Exception -> 0x004e }
            java.lang.String r1 = "Server does not support security (TLS), but security required by connection configuration."
            org.jivesoftware.smack.b.d r2 = new org.jivesoftware.smack.b.d     // Catch:{ Exception -> 0x004e }
            org.jivesoftware.smack.b.i r3 = org.jivesoftware.smack.b.i.b     // Catch:{ Exception -> 0x004e }
            r2.<init>(r3)     // Catch:{ Exception -> 0x004e }
            r0.<init>(r1, r2)     // Catch:{ Exception -> 0x004e }
            throw r0     // Catch:{ Exception -> 0x004e }
        L_0x0223:
            if (r3 == 0) goto L_0x0233
            org.jivesoftware.smack.XMPPConnection r0 = r13.c     // Catch:{ Exception -> 0x004e }
            org.jivesoftware.smack.d r0 = r0.a()     // Catch:{ Exception -> 0x004e }
            org.jivesoftware.smack.q r0 = r0.d()     // Catch:{ Exception -> 0x004e }
            org.jivesoftware.smack.q r1 = org.jivesoftware.smack.q.disabled     // Catch:{ Exception -> 0x004e }
            if (r0 != r1) goto L_0x0023
        L_0x0233:
            java.util.concurrent.Semaphore r0 = r13.g     // Catch:{ Exception -> 0x004e }
            r0.release()     // Catch:{ Exception -> 0x004e }
            goto L_0x0023
        L_0x023a:
            org.xmlpull.v1.XmlPullParser r0 = r13.d     // Catch:{ Exception -> 0x004e }
            java.lang.String r0 = r0.getName()     // Catch:{ Exception -> 0x004e }
            java.lang.String r1 = "proceed"
            boolean r0 = r0.equals(r1)     // Catch:{ Exception -> 0x004e }
            if (r0 == 0) goto L_0x0252
            org.jivesoftware.smack.XMPPConnection r0 = r13.c     // Catch:{ Exception -> 0x004e }
            r0.q()     // Catch:{ Exception -> 0x004e }
            r13.f()     // Catch:{ Exception -> 0x004e }
            goto L_0x0023
        L_0x0252:
            org.xmlpull.v1.XmlPullParser r0 = r13.d     // Catch:{ Exception -> 0x004e }
            java.lang.String r0 = r0.getName()     // Catch:{ Exception -> 0x004e }
            java.lang.String r1 = "failure"
            boolean r0 = r0.equals(r1)     // Catch:{ Exception -> 0x004e }
            if (r0 == 0) goto L_0x02a3
            org.xmlpull.v1.XmlPullParser r0 = r13.d     // Catch:{ Exception -> 0x004e }
            r1 = 0
            java.lang.String r0 = r0.getNamespace(r1)     // Catch:{ Exception -> 0x004e }
            java.lang.String r1 = "urn:ietf:params:xml:ns:xmpp-tls"
            boolean r1 = r1.equals(r0)     // Catch:{ Exception -> 0x004e }
            if (r1 == 0) goto L_0x0277
            java.lang.Exception r0 = new java.lang.Exception     // Catch:{ Exception -> 0x004e }
            java.lang.String r1 = "TLS negotiation has failed"
            r0.<init>(r1)     // Catch:{ Exception -> 0x004e }
            throw r0     // Catch:{ Exception -> 0x004e }
        L_0x0277:
            java.lang.String r1 = "http://jabber.org/protocol/compress"
            boolean r0 = r1.equals(r0)     // Catch:{ Exception -> 0x004e }
            if (r0 == 0) goto L_0x028b
            org.jivesoftware.smack.XMPPConnection r0 = r13.c     // Catch:{ Exception -> 0x004e }
            monitor-enter(r0)     // Catch:{ Exception -> 0x004e }
            r0.notify()     // Catch:{ all -> 0x0288 }
            monitor-exit(r0)     // Catch:{ all -> 0x0288 }
            goto L_0x0023
        L_0x0288:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x0288 }
            throw r1     // Catch:{ Exception -> 0x004e }
        L_0x028b:
            org.xmlpull.v1.XmlPullParser r0 = r13.d     // Catch:{ Exception -> 0x004e }
            org.jivesoftware.smack.c.i r0 = org.jivesoftware.smack.e.f.d(r0)     // Catch:{ Exception -> 0x004e }
            r13.a(r0)     // Catch:{ Exception -> 0x004e }
            org.jivesoftware.smack.XMPPConnection r1 = r13.c     // Catch:{ Exception -> 0x004e }
            org.jivesoftware.smack.k r1 = r1.i()     // Catch:{ Exception -> 0x004e }
            java.lang.String r0 = r0.b()     // Catch:{ Exception -> 0x004e }
            r1.b(r0)     // Catch:{ Exception -> 0x004e }
            goto L_0x0023
        L_0x02a3:
            org.xmlpull.v1.XmlPullParser r0 = r13.d     // Catch:{ Exception -> 0x004e }
            java.lang.String r0 = r0.getName()     // Catch:{ Exception -> 0x004e }
            java.lang.String r1 = "challenge"
            boolean r0 = r0.equals(r1)     // Catch:{ Exception -> 0x004e }
            if (r0 == 0) goto L_0x02ca
            org.xmlpull.v1.XmlPullParser r0 = r13.d     // Catch:{ Exception -> 0x004e }
            java.lang.String r0 = r0.nextText()     // Catch:{ Exception -> 0x004e }
            org.jivesoftware.smack.c.f r1 = new org.jivesoftware.smack.c.f     // Catch:{ Exception -> 0x004e }
            r1.<init>(r0)     // Catch:{ Exception -> 0x004e }
            r13.a(r1)     // Catch:{ Exception -> 0x004e }
            org.jivesoftware.smack.XMPPConnection r1 = r13.c     // Catch:{ Exception -> 0x004e }
            org.jivesoftware.smack.k r1 = r1.i()     // Catch:{ Exception -> 0x004e }
            r1.a(r0)     // Catch:{ Exception -> 0x004e }
            goto L_0x0023
        L_0x02ca:
            org.xmlpull.v1.XmlPullParser r0 = r13.d     // Catch:{ Exception -> 0x004e }
            java.lang.String r0 = r0.getName()     // Catch:{ Exception -> 0x004e }
            java.lang.String r1 = "success"
            boolean r0 = r0.equals(r1)     // Catch:{ Exception -> 0x004e }
            if (r0 == 0) goto L_0x02fb
            org.jivesoftware.smack.c.d r0 = new org.jivesoftware.smack.c.d     // Catch:{ Exception -> 0x004e }
            org.xmlpull.v1.XmlPullParser r1 = r13.d     // Catch:{ Exception -> 0x004e }
            java.lang.String r1 = r1.nextText()     // Catch:{ Exception -> 0x004e }
            r0.<init>(r1)     // Catch:{ Exception -> 0x004e }
            r13.a(r0)     // Catch:{ Exception -> 0x004e }
            org.jivesoftware.smack.XMPPConnection r0 = r13.c     // Catch:{ Exception -> 0x004e }
            org.jivesoftware.smack.b r0 = r0.m     // Catch:{ Exception -> 0x004e }
            r0.e()     // Catch:{ Exception -> 0x004e }
            r13.f()     // Catch:{ Exception -> 0x004e }
            org.jivesoftware.smack.XMPPConnection r0 = r13.c     // Catch:{ Exception -> 0x004e }
            org.jivesoftware.smack.k r0 = r0.i()     // Catch:{ Exception -> 0x004e }
            r0.b()     // Catch:{ Exception -> 0x004e }
            goto L_0x0023
        L_0x02fb:
            org.xmlpull.v1.XmlPullParser r0 = r13.d     // Catch:{ Exception -> 0x004e }
            java.lang.String r0 = r0.getName()     // Catch:{ Exception -> 0x004e }
            java.lang.String r1 = "compressed"
            boolean r0 = r0.equals(r1)     // Catch:{ Exception -> 0x004e }
            if (r0 == 0) goto L_0x0023
            org.jivesoftware.smack.XMPPConnection r0 = r13.c     // Catch:{ Exception -> 0x004e }
            r0.r()     // Catch:{ Exception -> 0x004e }
            r13.f()     // Catch:{ Exception -> 0x004e }
            goto L_0x0023
        L_0x0313:
            if (r0 != r12) goto L_0x0023
            org.xmlpull.v1.XmlPullParser r0 = r13.d     // Catch:{ Exception -> 0x004e }
            java.lang.String r0 = r0.getName()     // Catch:{ Exception -> 0x004e }
            java.lang.String r1 = "stream"
            r0.equals(r1)     // Catch:{ Exception -> 0x004e }
            goto L_0x0023
        */
        throw new UnsupportedOperationException("Method not decompiled: org.jivesoftware.smack.z.a(org.jivesoftware.smack.z, java.lang.Thread):void");
    }

    private void f() {
        try {
            this.d = XmlPullParserFactory.newInstance().newPullParser();
            this.d.setFeature("http://xmlpull.org/v1/doc/features.html#process-namespaces", true);
            this.d.setInput(this.c.g);
        } catch (XmlPullParserException e2) {
            e2.printStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.e = false;
        this.f = null;
        this.f715a = new o(this);
        this.f715a.setName("Smack Packet Reader (" + this.c.j + ")");
        this.f715a.setDaemon(true);
        this.b = Executors.newSingleThreadExecutor(new n(this));
        f();
    }

    /* access modifiers changed from: package-private */
    public final void a(Exception exc) {
        this.e = true;
        this.c.n();
        exc.printStackTrace();
        for (t a2 : this.c.k()) {
            try {
                a2.a(exc);
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }

    public final void b() {
        this.g = new Semaphore(1);
        this.f715a.start();
        try {
            this.g.acquire();
            this.g.tryAcquire((long) (g.a() * 3), TimeUnit.MILLISECONDS);
        } catch (InterruptedException e2) {
        }
        if (this.f == null) {
            throw new ad("Connection failed. No response from server.");
        }
        this.c.l = this.f;
    }

    public final void c() {
        if (!this.e) {
            for (t a2 : this.c.k()) {
                try {
                    a2.a();
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            }
        }
        this.e = true;
        this.b.shutdown();
    }

    /* access modifiers changed from: package-private */
    public final void d() {
        this.c.c.clear();
        this.c.b.clear();
    }

    /* access modifiers changed from: protected */
    public final void e() {
        for (t b2 : this.c.k()) {
            try {
                b2.b();
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }
}
