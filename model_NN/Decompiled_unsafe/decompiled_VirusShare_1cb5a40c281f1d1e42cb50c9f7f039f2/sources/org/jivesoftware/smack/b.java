package org.jivesoftware.smack;

import android.os.SystemClock;
import java.io.IOException;
import java.io.Writer;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import org.jivesoftware.smack.b.w;

final class b {

    /* renamed from: a  reason: collision with root package name */
    private Thread f656a;
    private Writer b;
    private XMPPConnection c;
    private final BlockingQueue d = new ArrayBlockingQueue(500, true);
    private boolean e;
    private long f = SystemClock.elapsedRealtime();

    protected b(XMPPConnection xMPPConnection) {
        this.c = xMPPConnection;
        a();
    }

    static /* synthetic */ void a(b bVar, Thread thread) {
        try {
            bVar.e();
            while (!bVar.e && bVar.f656a == thread) {
                w f2 = bVar.f();
                if (f2 != null) {
                    synchronized (bVar.b) {
                        bVar.b.write(f2.b_());
                        bVar.b.flush();
                        bVar.f = SystemClock.elapsedRealtime();
                    }
                }
            }
            try {
                synchronized (bVar.b) {
                    while (!bVar.d.isEmpty()) {
                        bVar.b.write(((w) bVar.d.remove()).b_());
                    }
                    bVar.b.flush();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
            bVar.d.clear();
            bVar.b.write("</stream:stream>");
            bVar.b.flush();
            try {
                bVar.b.close();
            } catch (Exception e3) {
            }
        } catch (Exception e4) {
            try {
                bVar.b.close();
            } catch (Exception e5) {
            }
        } catch (IOException e6) {
            if (!bVar.e) {
                bVar.e = true;
                bVar.c.n.a(e6);
            }
        } catch (Throwable th) {
            try {
                bVar.b.close();
            } catch (Exception e7) {
            }
            throw th;
        }
    }

    private w f() {
        w wVar = null;
        while (!this.e && (wVar = (w) this.d.poll()) == null) {
            try {
                synchronized (this.d) {
                    this.d.wait();
                }
            } catch (InterruptedException e2) {
            }
        }
        return wVar;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.b = this.c.h;
        this.e = false;
        this.f656a = new u(this);
        this.f656a.setName("Smack Packet Writer (" + this.c.j + ")");
        this.f656a.setDaemon(true);
    }

    /* access modifiers changed from: package-private */
    public final void a(Writer writer) {
        this.b = writer;
    }

    public final void a(w wVar) {
        if (!this.e) {
            this.c.c(wVar);
            try {
                this.d.put(wVar);
                synchronized (this.d) {
                    this.d.notifyAll();
                }
                this.c.b(wVar);
            } catch (InterruptedException e2) {
                e2.printStackTrace();
            }
        }
    }

    public final void b() {
        this.f656a.start();
    }

    public final void c() {
        this.e = true;
        synchronized (this.d) {
            this.d.notifyAll();
        }
    }

    /* access modifiers changed from: package-private */
    public final void d() {
        this.c.e.clear();
        this.c.d.clear();
    }

    /* access modifiers changed from: package-private */
    public final void e() {
        StringBuilder sb = new StringBuilder();
        sb.append("<stream:stream");
        sb.append(" to=\"").append(this.c.b()).append("\"");
        sb.append(" xmlns=\"jabber:client\"");
        sb.append(" xmlns:stream=\"http://etherx.jabber.org/streams\"");
        sb.append(" version=\"1.0\">");
        this.b.write(sb.toString());
        this.b.flush();
    }
}
