package org.jivesoftware.smack;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import org.jivesoftware.smack.b.q;
import org.jivesoftware.smack.b.r;
import org.jivesoftware.smack.b.w;
import org.jivesoftware.smack.e.h;

final class ae implements ab {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ ac f655a;

    /* synthetic */ ae(ac acVar) {
        this(acVar, (byte) 0);
    }

    private ae(ac acVar, byte b) {
        this.f655a = acVar;
    }

    public final void a(w wVar) {
        Map map;
        Object obj;
        Map map2;
        q qVar = (q) wVar;
        String h = qVar.h();
        String a2 = ac.a(this.f655a, h);
        if (qVar.b() == r.available) {
            if (this.f655a.h.get(a2) == null) {
                map2 = new ConcurrentHashMap();
                this.f655a.h.put(a2, map2);
            } else {
                map2 = (Map) this.f655a.h.get(a2);
            }
            map2.remove("");
            map2.put(h.c(h), qVar);
            if (((p) this.f655a.e.get(a2)) != null) {
                ac.d(this.f655a);
            }
        } else if (qVar.b() == r.unavailable) {
            if ("".equals(h.c(h))) {
                if (this.f655a.h.get(a2) == null) {
                    obj = new ConcurrentHashMap();
                    this.f655a.h.put(a2, obj);
                } else {
                    obj = (Map) this.f655a.h.get(a2);
                }
                obj.put("", qVar);
            } else if (this.f655a.h.get(a2) != null) {
                ((Map) this.f655a.h.get(a2)).put(h.c(h), qVar);
            }
            if (((p) this.f655a.e.get(a2)) != null) {
                ac.d(this.f655a);
            }
        } else if (qVar.b() == r.subscribe) {
            if (this.f655a.j == y.accept_all) {
                q qVar2 = new q(r.subscribed);
                qVar2.f(qVar.h());
                this.f655a.c.a(qVar2);
            } else if (this.f655a.j == y.reject_all) {
                q qVar3 = new q(r.unsubscribed);
                qVar3.f(qVar.h());
                this.f655a.c.a(qVar3);
            }
        } else if (qVar.b() == r.unsubscribe) {
            if (this.f655a.j != y.manual) {
                q qVar4 = new q(r.unsubscribed);
                qVar4.f(qVar.h());
                this.f655a.c.a(qVar4);
            }
        } else if (qVar.b() == r.error && "".equals(h.c(h))) {
            if (!this.f655a.h.containsKey(a2)) {
                map = new ConcurrentHashMap();
                this.f655a.h.put(a2, map);
            } else {
                map = (Map) this.f655a.h.get(a2);
                map.clear();
            }
            map.put("", qVar);
            if (((p) this.f655a.e.get(a2)) != null) {
                ac.d(this.f655a);
            }
        }
    }
}
