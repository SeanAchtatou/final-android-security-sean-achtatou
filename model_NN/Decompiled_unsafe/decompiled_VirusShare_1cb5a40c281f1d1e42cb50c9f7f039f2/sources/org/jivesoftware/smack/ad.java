package org.jivesoftware.smack;

import java.io.PrintStream;
import java.io.PrintWriter;
import org.jivesoftware.smack.b.d;
import org.jivesoftware.smack.b.e;

public final class ad extends Exception {

    /* renamed from: a  reason: collision with root package name */
    private e f654a = null;
    private d b = null;
    private Throwable c = null;

    public ad() {
    }

    public ad(String str) {
        super(str);
    }

    public ad(String str, Throwable th) {
        super(str);
        this.c = th;
    }

    public ad(String str, d dVar) {
        super(str);
        this.b = dVar;
    }

    public ad(String str, d dVar, Throwable th) {
        super(str);
        this.b = dVar;
        this.c = th;
    }

    public ad(d dVar) {
        this.b = dVar;
    }

    public ad(e eVar) {
        this.f654a = eVar;
    }

    public final String getMessage() {
        String message = super.getMessage();
        return (message != null || this.b == null) ? (message != null || this.f654a == null) ? message : this.f654a.toString() : this.b.toString();
    }

    public final void printStackTrace() {
        printStackTrace(System.err);
    }

    public final void printStackTrace(PrintStream printStream) {
        super.printStackTrace(printStream);
        if (this.c != null) {
            printStream.println("Nested Exception: ");
            this.c.printStackTrace(printStream);
        }
    }

    public final void printStackTrace(PrintWriter printWriter) {
        super.printStackTrace(printWriter);
        if (this.c != null) {
            printWriter.println("Nested Exception: ");
            this.c.printStackTrace(printWriter);
        }
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder();
        String message = super.getMessage();
        if (message != null) {
            sb.append(message).append(": ");
        }
        if (this.b != null) {
            sb.append(this.b);
        }
        if (this.f654a != null) {
            sb.append(this.f654a);
        }
        if (this.c != null) {
            sb.append("\n  -- caused by: ").append(this.c);
        }
        return sb.toString();
    }
}
