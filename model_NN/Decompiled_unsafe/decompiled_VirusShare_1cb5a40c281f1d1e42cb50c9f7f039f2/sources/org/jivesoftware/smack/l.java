package org.jivesoftware.smack;

import java.util.LinkedList;
import org.jivesoftware.smack.a.a;
import org.jivesoftware.smack.b.w;

public final class l {

    /* renamed from: a  reason: collision with root package name */
    private a f704a;
    private LinkedList b;
    private Connection c;
    private boolean d = false;

    protected l(Connection connection, a aVar) {
        this.c = connection;
        this.f704a = aVar;
        this.b = new LinkedList();
    }

    public final synchronized w a(long j) {
        w wVar;
        if (this.b.isEmpty()) {
            long currentTimeMillis = System.currentTimeMillis();
            long j2 = j;
            while (this.b.isEmpty() && j2 > 0) {
                try {
                    wait(j2);
                    long currentTimeMillis2 = System.currentTimeMillis();
                    j2 -= currentTimeMillis2 - currentTimeMillis;
                    currentTimeMillis = currentTimeMillis2;
                } catch (InterruptedException e) {
                }
            }
            wVar = this.b.isEmpty() ? null : (w) this.b.removeLast();
        } else {
            wVar = (w) this.b.removeLast();
        }
        return wVar;
    }

    public final void a() {
        if (!this.d) {
            this.d = true;
            this.c.a(this);
        }
    }

    /* access modifiers changed from: protected */
    public final synchronized void a(w wVar) {
        if (wVar != null) {
            if (this.f704a == null || this.f704a.a(wVar)) {
                if (this.b.size() == 65536) {
                    this.b.removeLast();
                }
                this.b.addFirst(wVar);
                notifyAll();
            }
        }
    }
}
