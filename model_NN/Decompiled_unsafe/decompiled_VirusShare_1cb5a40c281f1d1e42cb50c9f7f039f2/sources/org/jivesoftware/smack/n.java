package org.jivesoftware.smack;

import com.snda.a.a.a.a;
import java.util.concurrent.ThreadFactory;

final class n implements ThreadFactory {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ z f705a;

    n(z zVar) {
        this.f705a = zVar;
    }

    public final Thread newThread(Runnable runnable) {
        a.a("SMACK", "listenerExecutor +++++ new thread");
        Thread thread = new Thread(runnable, "Smack Listener Processor (" + this.f705a.c.j + ")");
        thread.setDaemon(true);
        return thread;
    }
}
