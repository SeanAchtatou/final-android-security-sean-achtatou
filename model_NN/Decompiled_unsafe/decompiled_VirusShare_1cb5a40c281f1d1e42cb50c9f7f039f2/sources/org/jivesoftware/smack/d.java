package org.jivesoftware.smack;

import java.io.File;
import javax.net.SocketFactory;
import org.a.b.a.a.b.a.g;
import org.jivesoftware.smack.d.a;
import org.jivesoftware.smack.d.f;

public final class d implements Cloneable {
    private f A;

    /* renamed from: a  reason: collision with root package name */
    private String f686a;
    private String b;
    private int c;
    private String d;
    private String e;
    private String f;
    private String g;
    private String h;
    private String i;
    private boolean j = false;
    private boolean k = false;
    private boolean l = false;
    private boolean m = false;
    private boolean n = false;
    private boolean o = false;
    private boolean p = true;
    private g q;
    private boolean r = Connection.f648a;
    private boolean s = true;
    private SocketFactory t;
    private String u;
    private String v;
    private String w;
    private boolean x = true;
    private boolean y = true;
    private q z = q.enabled;

    public d(String str, int i2, String str2) {
        f fVar = new f(a.NONE);
        this.b = str;
        this.c = i2;
        this.f686a = str2;
        this.A = fVar;
        String property = System.getProperty("java.home");
        StringBuilder sb = new StringBuilder();
        sb.append(property).append(File.separator).append("lib");
        sb.append(File.separator).append("security");
        sb.append(File.separator).append("cacerts");
        this.d = sb.toString();
        this.e = "jks";
        this.f = "changeit";
        this.g = System.getProperty("javax.net.ssl.keyStore");
        this.h = "jks";
        this.i = "pkcs11.config";
        this.t = fVar.e();
    }

    public final String a() {
        return this.f686a;
    }

    public final void a(String str) {
        this.f686a = str;
    }

    /* access modifiers changed from: package-private */
    public final void a(String str, String str2, String str3) {
        this.u = str;
        this.v = str2;
        this.w = str3;
    }

    public final void a(q qVar) {
        this.z = qVar;
    }

    public final String b() {
        return this.b;
    }

    public final int c() {
        return this.c;
    }

    public final q d() {
        return this.z;
    }

    public final String e() {
        return this.g;
    }

    public final String f() {
        return this.h;
    }

    public final String g() {
        return this.i;
    }

    public final boolean h() {
        return this.j;
    }

    public final boolean i() {
        return this.k;
    }

    public final void j() {
        this.k = false;
    }

    public final boolean k() {
        return this.l;
    }

    public final boolean l() {
        return this.m;
    }

    public final boolean m() {
        return this.n;
    }

    public final boolean n() {
        return this.o;
    }

    public final boolean o() {
        return this.p;
    }

    public final boolean p() {
        return this.r;
    }

    public final void q() {
        this.r = true;
    }

    public final void r() {
        this.s = false;
    }

    public final boolean s() {
        return this.y;
    }

    public final void t() {
        this.y = false;
    }

    public final g u() {
        return this.q;
    }

    public final SocketFactory v() {
        return this.t;
    }

    /* access modifiers changed from: package-private */
    public final boolean w() {
        return this.x;
    }
}
