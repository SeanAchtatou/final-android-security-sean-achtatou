package org.jivesoftware.smack;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.jivesoftware.smack.b.p;
import org.jivesoftware.smack.b.u;
import org.jivesoftware.smack.b.w;
import org.jivesoftware.smack.c.a;
import org.jivesoftware.smack.c.b;
import org.jivesoftware.smack.c.c;
import org.jivesoftware.smack.c.e;
import org.jivesoftware.smack.c.g;
import org.jivesoftware.smack.c.j;
import org.jivesoftware.smack.c.l;

public class k {

    /* renamed from: a  reason: collision with root package name */
    private static Map f703a = new HashMap();
    private static List b = new ArrayList();
    private Connection c;
    private Collection d = new ArrayList();
    private g e = null;
    private boolean f;
    private boolean g;
    private boolean h;
    private boolean i;
    private String j;

    static {
        a("EXTERNAL", a.class);
        a("GSSAPI", j.class);
        a("DIGEST-MD5", l.class);
        a("CRAM-MD5", b.class);
        a("PLAIN", e.class);
        a("ANONYMOUS", c.class);
        a("DIGEST-MD5", 0);
        a("PLAIN", 1);
        a("ANONYMOUS", 2);
    }

    k(Connection connection) {
        this.c = connection;
        e();
    }

    private static void a(String str, int i2) {
        b.add(i2, str);
    }

    private static void a(String str, Class cls) {
        f703a.put(str, cls);
    }

    private String c(String str) {
        synchronized (this) {
            if (!this.h) {
                try {
                    wait(30000);
                } catch (InterruptedException e2) {
                }
            }
        }
        if (!this.h) {
            throw new ad("Resource binding not offered by server");
        }
        org.jivesoftware.smack.b.a aVar = new org.jivesoftware.smack.b.a();
        aVar.a(str);
        l a2 = this.c.a(new org.jivesoftware.smack.a.c(aVar.f()));
        this.c.a(aVar);
        org.jivesoftware.smack.b.a aVar2 = (org.jivesoftware.smack.b.a) a2.a((long) g.a());
        a2.a();
        if (aVar2 == null) {
            throw new ad("No response from the server.");
        } else if (aVar2.d() == org.jivesoftware.smack.b.l.d) {
            throw new ad(aVar2.i());
        } else {
            String a3 = aVar2.a();
            if (this.i) {
                u uVar = new u();
                l a4 = this.c.a(new org.jivesoftware.smack.a.c(uVar.f()));
                this.c.a(uVar);
                p pVar = (p) a4.a((long) g.a());
                a4.a();
                if (pVar == null) {
                    throw new ad("No response from the server.");
                } else if (pVar.d() != org.jivesoftware.smack.b.l.d) {
                    return a3;
                } else {
                    throw new ad(pVar.i());
                }
            } else {
                throw new ad("Session establishment not offered by server");
            }
        }
    }

    public final String a(String str, String str2, String str3) {
        String str4 = null;
        Iterator it = b.iterator();
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            String str5 = (String) it.next();
            if (f703a.containsKey(str5) && this.d.contains(str5)) {
                str4 = str5;
                break;
            }
        }
        if (str4 == null) {
            return new a(this.c).a(str, str2, str3);
        }
        try {
            this.e = (g) ((Class) f703a.get(str4)).getConstructor(k.class).newInstance(this);
            this.e.a(str, this.c.b(), str2);
            synchronized (this) {
                if (!this.f && !this.g) {
                    try {
                        wait(30000);
                    } catch (InterruptedException e2) {
                    }
                }
            }
            if (!this.g) {
                return this.f ? c(str3) : new a(this.c).a(str, str2, str3);
            }
            if (this.j != null) {
                throw new ad("SASL authentication " + str4 + " failed: " + this.j);
            }
            throw new ad("SASL authentication failed using mechanism " + str4);
        } catch (ad e3) {
            throw e3;
        } catch (Exception e4) {
            e4.printStackTrace();
            return new a(this.c).a(str, str2, str3);
        }
    }

    public final String a(String str, String str2, org.a.b.a.a.b.a.g gVar) {
        String str3 = null;
        Iterator it = b.iterator();
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            String str4 = (String) it.next();
            if (f703a.containsKey(str4) && this.d.contains(str4)) {
                str3 = str4;
                break;
            }
        }
        if (str3 != null) {
            try {
                this.e = (g) ((Class) f703a.get(str3)).getConstructor(k.class).newInstance(this);
                this.e.a(str, this.c.c(), gVar);
                synchronized (this) {
                    if (!this.f && !this.g) {
                        try {
                            wait(30000);
                        } catch (InterruptedException e2) {
                        }
                    }
                }
                if (!this.g) {
                    if (this.f) {
                        return c(str2);
                    }
                    throw new ad("SASL authentication failed");
                } else if (this.j != null) {
                    throw new ad("SASL authentication " + str3 + " failed: " + this.j);
                } else {
                    throw new ad("SASL authentication failed using mechanism " + str3);
                }
            } catch (ad e3) {
                throw e3;
            } catch (Exception e4) {
                e4.printStackTrace();
            }
        } else {
            throw new ad("SASL Authentication failed. No known authentication mechanisims.");
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(String str) {
        this.e.a(str);
    }

    /* access modifiers changed from: package-private */
    public final void a(Collection collection) {
        this.d = collection;
    }

    public final void a(w wVar) {
        this.c.a(wVar);
    }

    public final boolean a() {
        return !this.d.isEmpty() && (this.d.size() != 1 || !this.d.contains("ANONYMOUS"));
    }

    /* access modifiers changed from: package-private */
    public final void b() {
        synchronized (this) {
            this.f = true;
            notify();
        }
    }

    /* access modifiers changed from: package-private */
    public final void b(String str) {
        synchronized (this) {
            this.g = true;
            this.j = str;
            notify();
        }
    }

    /* access modifiers changed from: package-private */
    public final void c() {
        synchronized (this) {
            this.h = true;
            notify();
        }
    }

    /* access modifiers changed from: package-private */
    public final void d() {
        this.i = true;
    }

    /* access modifiers changed from: protected */
    public final void e() {
        this.f = false;
        this.g = false;
        this.h = false;
        this.i = false;
    }
}
