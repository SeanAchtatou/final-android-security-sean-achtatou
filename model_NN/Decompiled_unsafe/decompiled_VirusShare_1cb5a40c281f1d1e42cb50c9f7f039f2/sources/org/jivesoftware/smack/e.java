package org.jivesoftware.smack;

import java.security.KeyStore;
import java.security.cert.CertificateParsingException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Pattern;
import javax.net.ssl.X509TrustManager;

final class e implements X509TrustManager {

    /* renamed from: a  reason: collision with root package name */
    private static Pattern f692a = Pattern.compile("(?i)(cn=)([^,]*)");
    private d b;
    private String c;
    private KeyStore d;

    public e(String str, d dVar) {
        this.b = dVar;
        this.c = str;
        dVar.j();
    }

    private static List a(X509Certificate x509Certificate) {
        ArrayList arrayList = new ArrayList();
        try {
            return x509Certificate.getSubjectAlternativeNames() == null ? Collections.emptyList() : arrayList;
        } catch (CertificateParsingException e) {
            e.printStackTrace();
            return arrayList;
        }
    }

    public final void checkClientTrusted(X509Certificate[] x509CertificateArr, String str) {
    }

    /* JADX WARNING: Removed duplicated region for block: B:37:0x00d2  */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x00fc  */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x0168  */
    /* JADX WARNING: Removed duplicated region for block: B:74:? A[ORIG_RETURN, RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void checkServerTrusted(java.security.cert.X509Certificate[] r10, java.lang.String r11) {
        /*
            r9 = this;
            r7 = 1
            r6 = 0
            int r1 = r10.length
            r0 = r10[r6]
            java.util.List r2 = a(r0)
            boolean r3 = r2.isEmpty()
            if (r3 == 0) goto L_0x0030
            java.security.Principal r0 = r0.getSubjectDN()
            java.lang.String r0 = r0.getName()
            java.util.regex.Pattern r2 = org.jivesoftware.smack.e.f692a
            java.util.regex.Matcher r2 = r2.matcher(r0)
            boolean r3 = r2.find()
            if (r3 == 0) goto L_0x0028
            r0 = 2
            java.lang.String r0 = r2.group(r0)
        L_0x0028:
            java.util.ArrayList r2 = new java.util.ArrayList
            r2.<init>()
            r2.add(r0)
        L_0x0030:
            org.jivesoftware.smack.d r0 = r9.b
            boolean r0 = r0.h()
            if (r0 == 0) goto L_0x0096
            r0 = 0
            int r3 = r1 - r7
            r8 = r3
            r3 = r0
            r0 = r8
        L_0x003e:
            if (r0 < 0) goto L_0x0096
            r4 = r10[r0]
            java.security.Principal r5 = r4.getIssuerDN()
            java.security.Principal r4 = r4.getSubjectDN()
            if (r3 == 0) goto L_0x005f
            boolean r3 = r5.equals(r3)
            if (r3 == 0) goto L_0x007d
            int r3 = r0 + 1
            r3 = r10[r3]     // Catch:{ GeneralSecurityException -> 0x0063 }
            java.security.PublicKey r3 = r3.getPublicKey()     // Catch:{ GeneralSecurityException -> 0x0063 }
            r5 = r10[r0]     // Catch:{ GeneralSecurityException -> 0x0063 }
            r5.verify(r3)     // Catch:{ GeneralSecurityException -> 0x0063 }
        L_0x005f:
            int r0 = r0 + -1
            r3 = r4
            goto L_0x003e
        L_0x0063:
            r0 = move-exception
            java.security.cert.CertificateException r0 = new java.security.cert.CertificateException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r3 = "signature verification failed of "
            java.lang.StringBuilder r1 = r1.append(r3)
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x007d:
            java.security.cert.CertificateException r0 = new java.security.cert.CertificateException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r3 = "subject/issuer verification failed of "
            java.lang.StringBuilder r1 = r1.append(r3)
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x0096:
            org.jivesoftware.smack.d r0 = r9.b
            boolean r0 = r0.i()
            if (r0 == 0) goto L_0x00f4
            java.security.KeyStore r0 = r9.d     // Catch:{ KeyStoreException -> 0x00ed }
            int r3 = r1 - r7
            r3 = r10[r3]     // Catch:{ KeyStoreException -> 0x00ed }
            java.lang.String r0 = r0.getCertificateAlias(r3)     // Catch:{ KeyStoreException -> 0x00ed }
            if (r0 == 0) goto L_0x00eb
            r0 = r7
        L_0x00ab:
            if (r0 != 0) goto L_0x00d0
            if (r1 != r7) goto L_0x00d0
            org.jivesoftware.smack.d r3 = r9.b     // Catch:{ KeyStoreException -> 0x0195 }
            boolean r3 = r3.k()     // Catch:{ KeyStoreException -> 0x0195 }
            if (r3 == 0) goto L_0x00d0
            java.io.PrintStream r3 = java.lang.System.out     // Catch:{ KeyStoreException -> 0x0195 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ KeyStoreException -> 0x0195 }
            r4.<init>()     // Catch:{ KeyStoreException -> 0x0195 }
            java.lang.String r5 = "Accepting self-signed certificate of remote server: "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ KeyStoreException -> 0x0195 }
            java.lang.StringBuilder r4 = r4.append(r2)     // Catch:{ KeyStoreException -> 0x0195 }
            java.lang.String r4 = r4.toString()     // Catch:{ KeyStoreException -> 0x0195 }
            r3.println(r4)     // Catch:{ KeyStoreException -> 0x0195 }
            r0 = r7
        L_0x00d0:
            if (r0 != 0) goto L_0x00f4
            java.security.cert.CertificateException r0 = new java.security.cert.CertificateException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r3 = "root certificate not trusted of "
            java.lang.StringBuilder r1 = r1.append(r3)
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x00eb:
            r0 = r6
            goto L_0x00ab
        L_0x00ed:
            r0 = move-exception
            r3 = r6
        L_0x00ef:
            r0.printStackTrace()
            r0 = r3
            goto L_0x00d0
        L_0x00f4:
            org.jivesoftware.smack.d r0 = r9.b
            boolean r0 = r0.m()
            if (r0 == 0) goto L_0x0160
            int r0 = r2.size()
            if (r0 != r7) goto L_0x013f
            java.lang.Object r0 = r2.get(r6)
            java.lang.String r0 = (java.lang.String) r0
            java.lang.String r3 = "*."
            boolean r0 = r0.startsWith(r3)
            if (r0 == 0) goto L_0x013f
            java.lang.Object r0 = r2.get(r6)
            java.lang.String r0 = (java.lang.String) r0
            java.lang.String r3 = "*."
            java.lang.String r4 = ""
            java.lang.String r0 = r0.replace(r3, r4)
            java.lang.String r3 = r9.c
            boolean r0 = r3.endsWith(r0)
            if (r0 != 0) goto L_0x0160
            java.security.cert.CertificateException r0 = new java.security.cert.CertificateException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r3 = "target verification failed of "
            java.lang.StringBuilder r1 = r1.append(r3)
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x013f:
            java.lang.String r0 = r9.c
            boolean r0 = r2.contains(r0)
            if (r0 != 0) goto L_0x0160
            java.security.cert.CertificateException r0 = new java.security.cert.CertificateException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r3 = "target verification failed of "
            java.lang.StringBuilder r1 = r1.append(r3)
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x0160:
            org.jivesoftware.smack.d r0 = r9.b
            boolean r0 = r0.l()
            if (r0 == 0) goto L_0x0194
            java.util.Date r0 = new java.util.Date
            r0.<init>()
            r2 = r6
        L_0x016e:
            if (r2 >= r1) goto L_0x0194
            r3 = r10[r2]     // Catch:{ GeneralSecurityException -> 0x0178 }
            r3.checkValidity(r0)     // Catch:{ GeneralSecurityException -> 0x0178 }
            int r2 = r2 + 1
            goto L_0x016e
        L_0x0178:
            r0 = move-exception
            java.security.cert.CertificateException r0 = new java.security.cert.CertificateException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "invalid date of "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = r9.c
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x0194:
            return
        L_0x0195:
            r3 = move-exception
            r8 = r3
            r3 = r0
            r0 = r8
            goto L_0x00ef
        */
        throw new UnsupportedOperationException("Method not decompiled: org.jivesoftware.smack.e.checkServerTrusted(java.security.cert.X509Certificate[], java.lang.String):void");
    }

    public final X509Certificate[] getAcceptedIssuers() {
        return new X509Certificate[0];
    }
}
