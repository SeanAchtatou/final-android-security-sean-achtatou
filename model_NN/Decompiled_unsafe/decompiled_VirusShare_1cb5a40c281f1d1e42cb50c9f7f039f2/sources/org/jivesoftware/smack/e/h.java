package org.jivesoftware.smack.e;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Random;

public final class h {

    /* renamed from: a  reason: collision with root package name */
    private static final char[] f696a = "&quot;".toCharArray();
    private static final char[] b = "&apos;".toCharArray();
    private static final char[] c = "&amp;".toCharArray();
    private static final char[] d = "&lt;".toCharArray();
    private static final char[] e = "&gt;".toCharArray();
    private static MessageDigest f = null;
    private static Random g = new Random();
    private static char[] h = "0123456789abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();

    private h() {
    }

    public static String a() {
        char[] cArr = new char[5];
        for (int i = 0; i < cArr.length; i++) {
            cArr[i] = h[g.nextInt(71)];
        }
        return new String(cArr);
    }

    public static String a(String str) {
        if (str == null) {
            return null;
        }
        int lastIndexOf = str.lastIndexOf("@");
        return lastIndexOf <= 0 ? "" : str.substring(0, lastIndexOf);
    }

    public static String a(byte[] bArr) {
        return g.a(bArr, bArr.length, 8);
    }

    public static String b(String str) {
        if (str == null) {
            return null;
        }
        int lastIndexOf = str.lastIndexOf("@");
        if (lastIndexOf + 1 > str.length()) {
            return "";
        }
        int indexOf = str.indexOf("/");
        return (indexOf <= 0 || indexOf <= lastIndexOf) ? str.substring(lastIndexOf + 1) : str.substring(lastIndexOf + 1, indexOf);
    }

    public static String c(String str) {
        if (str == null) {
            return null;
        }
        int indexOf = str.indexOf("/");
        return (indexOf + 1 > str.length() || indexOf < 0) ? "" : str.substring(indexOf + 1);
    }

    public static String d(String str) {
        if (str == null) {
            return null;
        }
        int indexOf = str.indexOf("/");
        return indexOf < 0 ? str : indexOf == 0 ? "" : str.substring(0, indexOf);
    }

    public static String e(String str) {
        if (str == null) {
            return null;
        }
        char[] charArray = str.toCharArray();
        int length = charArray.length;
        StringBuilder sb = new StringBuilder((int) (((double) length) * 1.3d));
        int i = 0;
        int i2 = 0;
        while (i2 < length) {
            char c2 = charArray[i2];
            if (c2 <= '>') {
                if (c2 == '<') {
                    if (i2 > i) {
                        sb.append(charArray, i, i2 - i);
                    }
                    i = i2 + 1;
                    sb.append(d);
                } else if (c2 == '>') {
                    if (i2 > i) {
                        sb.append(charArray, i, i2 - i);
                    }
                    i = i2 + 1;
                    sb.append(e);
                } else if (c2 == '&') {
                    if (i2 > i) {
                        sb.append(charArray, i, i2 - i);
                    }
                    if (length <= i2 + 5 || charArray[i2 + 1] != '#' || !Character.isDigit(charArray[i2 + 2]) || !Character.isDigit(charArray[i2 + 3]) || !Character.isDigit(charArray[i2 + 4]) || charArray[i2 + 5] != ';') {
                        i = i2 + 1;
                        sb.append(c);
                    }
                } else if (c2 == '\"') {
                    if (i2 > i) {
                        sb.append(charArray, i, i2 - i);
                    }
                    i = i2 + 1;
                    sb.append(f696a);
                } else if (c2 == '\'') {
                    if (i2 > i) {
                        sb.append(charArray, i, i2 - i);
                    }
                    i = i2 + 1;
                    sb.append(b);
                }
            }
            i2++;
        }
        if (i == 0) {
            return str;
        }
        if (i2 > i) {
            sb.append(charArray, i, i2 - i);
        }
        return sb.toString();
    }

    public static synchronized String f(String str) {
        String sb;
        synchronized (h.class) {
            if (f == null) {
                try {
                    f = MessageDigest.getInstance("SHA-1");
                } catch (NoSuchAlgorithmException e2) {
                    System.err.println("Failed to load the SHA-1 MessageDigest. Jive will be unable to function normally.");
                }
            }
            try {
                f.update(str.getBytes("UTF-8"));
            } catch (UnsupportedEncodingException e3) {
                System.err.println(e3);
            }
            byte[] digest = f.digest();
            StringBuilder sb2 = new StringBuilder(digest.length * 2);
            for (byte b2 : digest) {
                if ((b2 & 255) < 16) {
                    sb2.append("0");
                }
                sb2.append(Integer.toString(b2 & 255, 16));
            }
            sb = sb2.toString();
        }
        return sb;
    }

    public static byte[] g(String str) {
        return g.a(str);
    }
}
