package org.jivesoftware.smack.e;

import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

public final class b extends Writer {

    /* renamed from: a  reason: collision with root package name */
    private Writer f694a = null;
    private List b = new ArrayList();

    public b(Writer writer) {
        this.f694a = writer;
    }

    private void a(String str) {
        i[] iVarArr;
        synchronized (this.b) {
            iVarArr = new i[this.b.size()];
            this.b.toArray(iVarArr);
        }
        for (i a2 : iVarArr) {
            a2.a(str);
        }
    }

    public final void a(i iVar) {
        if (iVar != null) {
            synchronized (this.b) {
                if (!this.b.contains(iVar)) {
                    this.b.add(iVar);
                }
            }
        }
    }

    public final void b(i iVar) {
        synchronized (this.b) {
            this.b.remove(iVar);
        }
    }

    public final void close() {
        this.f694a.close();
    }

    public final void flush() {
        this.f694a.flush();
    }

    public final void write(int i) {
        this.f694a.write(i);
    }

    public final void write(String str) {
        this.f694a.write(str);
        a(str);
    }

    public final void write(String str, int i, int i2) {
        this.f694a.write(str, i, i2);
        a(str.substring(i, i + i2));
    }

    public final void write(char[] cArr) {
        this.f694a.write(cArr);
        a(new String(cArr));
    }

    public final void write(char[] cArr, int i, int i2) {
        this.f694a.write(cArr, i, i2);
        a(new String(cArr, i, i2));
    }
}
