package org.jivesoftware.smack.e;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

public final class g {

    /* renamed from: a  reason: collision with root package name */
    private static final byte[] f695a = {65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 43, 47};
    private static final byte[] b = {-9, -9, -9, -9, -9, -9, -9, -9, -9, -5, -5, -9, -9, -5, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -5, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, 62, -9, -9, -9, 63, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, -9, -9, -9, -1, -9, -9, -9, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, -9, -9, -9, -9, -9, -9, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, -9, -9, -9, -9};
    private static final byte[] c = {65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 45, 95};
    private static final byte[] d = {-9, -9, -9, -9, -9, -9, -9, -9, -9, -5, -5, -9, -9, -5, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -5, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, 62, -9, -9, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, -9, -9, -9, -1, -9, -9, -9, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, -9, -9, -9, -9, 63, -9, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, -9, -9, -9, -9};
    private static final byte[] e = {45, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 95, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122};
    private static final byte[] f = {-9, -9, -9, -9, -9, -9, -9, -9, -9, -5, -5, -9, -9, -5, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -5, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, 0, -9, -9, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, -9, -9, -9, -1, -9, -9, -9, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, -9, -9, -9, -9, 37, -9, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, -9, -9, -9, -9};

    private g() {
    }

    public static String a(byte[] bArr) {
        return a(bArr, bArr.length, 0);
    }

    public static String a(byte[] bArr, int i, int i2) {
        int i3;
        int i4;
        ByteArrayOutputStream byteArrayOutputStream;
        GZIPOutputStream gZIPOutputStream;
        a aVar;
        int i5 = i2 & 8;
        if ((i2 & 2) == 2) {
            try {
                ByteArrayOutputStream byteArrayOutputStream2 = new ByteArrayOutputStream();
                try {
                    aVar = new a(byteArrayOutputStream2, i2 | 1);
                    try {
                        gZIPOutputStream = new GZIPOutputStream(aVar);
                        try {
                            gZIPOutputStream.write(bArr, 0, i);
                            gZIPOutputStream.close();
                            try {
                                gZIPOutputStream.close();
                            } catch (Exception e2) {
                            }
                            try {
                                aVar.close();
                            } catch (Exception e3) {
                            }
                            try {
                                byteArrayOutputStream2.close();
                            } catch (Exception e4) {
                            }
                            try {
                                return new String(byteArrayOutputStream2.toByteArray(), "UTF-8");
                            } catch (UnsupportedEncodingException e5) {
                                return new String(byteArrayOutputStream2.toByteArray());
                            }
                        } catch (IOException e6) {
                            IOException iOException = e6;
                            byteArrayOutputStream = byteArrayOutputStream2;
                            e = iOException;
                            try {
                                e.printStackTrace();
                                try {
                                    gZIPOutputStream.close();
                                } catch (Exception e7) {
                                }
                                try {
                                    aVar.close();
                                } catch (Exception e8) {
                                }
                                try {
                                    byteArrayOutputStream.close();
                                } catch (Exception e9) {
                                }
                                return null;
                            } catch (Throwable th) {
                                th = th;
                                try {
                                    gZIPOutputStream.close();
                                } catch (Exception e10) {
                                }
                                try {
                                    aVar.close();
                                } catch (Exception e11) {
                                }
                                try {
                                    byteArrayOutputStream.close();
                                } catch (Exception e12) {
                                }
                                throw th;
                            }
                        } catch (Throwable th2) {
                            Throwable th3 = th2;
                            byteArrayOutputStream = byteArrayOutputStream2;
                            th = th3;
                            gZIPOutputStream.close();
                            aVar.close();
                            byteArrayOutputStream.close();
                            throw th;
                        }
                    } catch (IOException e13) {
                        byteArrayOutputStream = byteArrayOutputStream2;
                        e = e13;
                        gZIPOutputStream = null;
                        e.printStackTrace();
                        gZIPOutputStream.close();
                        aVar.close();
                        byteArrayOutputStream.close();
                        return null;
                    } catch (Throwable th4) {
                        byteArrayOutputStream = byteArrayOutputStream2;
                        th = th4;
                        gZIPOutputStream = null;
                        gZIPOutputStream.close();
                        aVar.close();
                        byteArrayOutputStream.close();
                        throw th;
                    }
                } catch (IOException e14) {
                    gZIPOutputStream = null;
                    byteArrayOutputStream = byteArrayOutputStream2;
                    e = e14;
                    aVar = null;
                    e.printStackTrace();
                    gZIPOutputStream.close();
                    aVar.close();
                    byteArrayOutputStream.close();
                    return null;
                } catch (Throwable th5) {
                    gZIPOutputStream = null;
                    byteArrayOutputStream = byteArrayOutputStream2;
                    th = th5;
                    aVar = null;
                    gZIPOutputStream.close();
                    aVar.close();
                    byteArrayOutputStream.close();
                    throw th;
                }
            } catch (IOException e15) {
                e = e15;
                aVar = null;
                gZIPOutputStream = null;
                byteArrayOutputStream = null;
                e.printStackTrace();
                gZIPOutputStream.close();
                aVar.close();
                byteArrayOutputStream.close();
                return null;
            } catch (Throwable th6) {
                th = th6;
                aVar = null;
                gZIPOutputStream = null;
                byteArrayOutputStream = null;
                gZIPOutputStream.close();
                aVar.close();
                byteArrayOutputStream.close();
                throw th;
            }
        } else {
            boolean z = i5 == 0;
            int i6 = (i * 4) / 3;
            byte[] bArr2 = new byte[((z ? i6 / 76 : 0) + (i % 3 > 0 ? 4 : 0) + i6)];
            int i7 = i - 2;
            int i8 = 0;
            int i9 = 0;
            int i10 = 0;
            while (i9 < i7) {
                a(bArr, i9 + 0, 3, bArr2, i10, i2);
                int i11 = i8 + 4;
                if (!z || i11 != 76) {
                    i4 = i10;
                } else {
                    bArr2[i10 + 4] = 10;
                    i4 = i10 + 1;
                    i11 = 0;
                }
                i8 = i11;
                i9 += 3;
                i10 = i4 + 4;
            }
            if (i9 < i) {
                a(bArr, i9 + 0, i - i9, bArr2, i10, i2);
                i3 = i10 + 4;
            } else {
                i3 = i10;
            }
            try {
                return new String(bArr2, 0, i3, "UTF-8");
            } catch (UnsupportedEncodingException e16) {
                return new String(bArr2, 0, i3);
            }
        }
    }

    public static byte[] a(String str) {
        return b(str);
    }

    /* access modifiers changed from: private */
    public static byte[] a(byte[] bArr, int i, int i2, byte[] bArr2, int i3, int i4) {
        byte[] c2 = c(i4);
        int i5 = (i2 > 0 ? (bArr[i] << 24) >>> 8 : 0) | (i2 > 1 ? (bArr[i + 1] << 24) >>> 16 : 0) | (i2 > 2 ? (bArr[i + 2] << 24) >>> 24 : 0);
        switch (i2) {
            case 1:
                bArr2[i3] = c2[i5 >>> 18];
                bArr2[i3 + 1] = c2[(i5 >>> 12) & 63];
                bArr2[i3 + 2] = 61;
                bArr2[i3 + 3] = 61;
                return bArr2;
            case 2:
                bArr2[i3] = c2[i5 >>> 18];
                bArr2[i3 + 1] = c2[(i5 >>> 12) & 63];
                bArr2[i3 + 2] = c2[(i5 >>> 6) & 63];
                bArr2[i3 + 3] = 61;
                return bArr2;
            case 3:
                bArr2[i3] = c2[i5 >>> 18];
                bArr2[i3 + 1] = c2[(i5 >>> 12) & 63];
                bArr2[i3 + 2] = c2[(i5 >>> 6) & 63];
                bArr2[i3 + 3] = c2[i5 & 63];
                return bArr2;
            default:
                return bArr2;
        }
    }

    /* access modifiers changed from: private */
    public static int b(byte[] bArr, byte[] bArr2, int i, int i2) {
        byte[] d2 = d(i2);
        if (bArr[2] == 61) {
            bArr2[i] = (byte) ((((d2[bArr[1]] & 255) << 12) | ((d2[bArr[0]] & 255) << 18)) >>> 16);
            return 1;
        } else if (bArr[3] == 61) {
            int i3 = ((d2[bArr[2]] & 255) << 6) | ((d2[bArr[0]] & 255) << 18) | ((d2[bArr[1]] & 255) << 12);
            bArr2[i] = (byte) (i3 >>> 16);
            bArr2[i + 1] = (byte) (i3 >>> 8);
            return 2;
        } else {
            try {
                byte b2 = ((d2[bArr[0]] & 255) << 18) | ((d2[bArr[1]] & 255) << 12) | ((d2[bArr[2]] & 255) << 6) | (d2[bArr[3]] & 255);
                bArr2[i] = (byte) (b2 >> 16);
                bArr2[i + 1] = (byte) (b2 >> 8);
                bArr2[i + 2] = (byte) b2;
                return 3;
            } catch (Exception e2) {
                System.out.println("" + ((int) bArr[0]) + ": " + ((int) d2[bArr[0]]));
                System.out.println("" + ((int) bArr[1]) + ": " + ((int) d2[bArr[1]]));
                System.out.println("" + ((int) bArr[2]) + ": " + ((int) d2[bArr[2]]));
                System.out.println("" + ((int) bArr[3]) + ": " + ((int) d2[bArr[3]]));
                return -1;
            }
        }
    }

    public static String b(byte[] bArr) {
        return a(bArr, bArr.length, 8);
    }

    private static byte[] b(String str) {
        byte[] bytes;
        int i;
        byte[] bArr;
        ByteArrayOutputStream byteArrayOutputStream;
        GZIPInputStream gZIPInputStream;
        ByteArrayInputStream byteArrayInputStream;
        GZIPInputStream gZIPInputStream2;
        ByteArrayOutputStream byteArrayOutputStream2;
        try {
            bytes = str.getBytes("UTF-8");
        } catch (UnsupportedEncodingException e2) {
            bytes = str.getBytes();
        }
        int length = bytes.length;
        byte[] d2 = d(0);
        byte[] bArr2 = new byte[((length * 3) / 4)];
        byte[] bArr3 = new byte[4];
        int i2 = 0;
        int i3 = 0;
        int i4 = 0;
        while (true) {
            if (i2 >= length + 0) {
                i = i4;
                break;
            }
            byte b2 = (byte) (bytes[i2] & Byte.MAX_VALUE);
            byte b3 = d2[b2];
            if (b3 < -5) {
                System.err.println("Bad Base64 input character at " + i2 + ": " + ((int) bytes[i2]) + "(decimal)");
                bArr = null;
                break;
            }
            if (b3 >= -1) {
                int i5 = i3 + 1;
                bArr3[i3] = b2;
                if (i5 > 3) {
                    int b4 = b(bArr3, bArr2, i4, 0) + i4;
                    if (b2 == 61) {
                        i = b4;
                        break;
                    }
                    i4 = b4;
                    i3 = 0;
                } else {
                    i3 = i5;
                }
            }
            i2++;
        }
        byte[] bArr4 = new byte[i];
        System.arraycopy(bArr2, 0, bArr4, 0, i);
        bArr = bArr4;
        if (bArr != null && bArr.length >= 4 && 35615 == ((bArr[0] & 255) | ((bArr[1] << 8) & 65280))) {
            byte[] bArr5 = new byte[2048];
            try {
                ByteArrayOutputStream byteArrayOutputStream3 = new ByteArrayOutputStream();
                try {
                    byteArrayInputStream = new ByteArrayInputStream(bArr);
                    try {
                        GZIPInputStream gZIPInputStream3 = new GZIPInputStream(byteArrayInputStream);
                        while (true) {
                            try {
                                int read = gZIPInputStream3.read(bArr5);
                                if (read < 0) {
                                    break;
                                }
                                byteArrayOutputStream3.write(bArr5, 0, read);
                            } catch (IOException e3) {
                                byteArrayOutputStream = byteArrayOutputStream3;
                                gZIPInputStream = gZIPInputStream3;
                            } catch (Throwable th) {
                                th = th;
                                byteArrayOutputStream2 = byteArrayOutputStream3;
                                gZIPInputStream2 = gZIPInputStream3;
                                try {
                                    byteArrayOutputStream2.close();
                                } catch (Exception e4) {
                                }
                                try {
                                    gZIPInputStream2.close();
                                } catch (Exception e5) {
                                }
                                try {
                                    byteArrayInputStream.close();
                                } catch (Exception e6) {
                                }
                                throw th;
                            }
                        }
                        bArr = byteArrayOutputStream3.toByteArray();
                        try {
                            byteArrayOutputStream3.close();
                        } catch (Exception e7) {
                        }
                        try {
                            gZIPInputStream3.close();
                        } catch (Exception e8) {
                        }
                        try {
                            byteArrayInputStream.close();
                        } catch (Exception e9) {
                        }
                    } catch (IOException e10) {
                        byteArrayOutputStream = byteArrayOutputStream3;
                        gZIPInputStream = null;
                        try {
                            byteArrayOutputStream.close();
                        } catch (Exception e11) {
                        }
                        try {
                            gZIPInputStream.close();
                        } catch (Exception e12) {
                        }
                        try {
                            byteArrayInputStream.close();
                        } catch (Exception e13) {
                        }
                        return bArr;
                    } catch (Throwable th2) {
                        th = th2;
                        byteArrayOutputStream2 = byteArrayOutputStream3;
                        gZIPInputStream2 = null;
                        byteArrayOutputStream2.close();
                        gZIPInputStream2.close();
                        byteArrayInputStream.close();
                        throw th;
                    }
                } catch (IOException e14) {
                    byteArrayOutputStream = byteArrayOutputStream3;
                    byteArrayInputStream = null;
                    gZIPInputStream = null;
                    byteArrayOutputStream.close();
                    gZIPInputStream.close();
                    byteArrayInputStream.close();
                    return bArr;
                } catch (Throwable th3) {
                    th = th3;
                    byteArrayOutputStream2 = byteArrayOutputStream3;
                    byteArrayInputStream = null;
                    gZIPInputStream2 = null;
                    byteArrayOutputStream2.close();
                    gZIPInputStream2.close();
                    byteArrayInputStream.close();
                    throw th;
                }
            } catch (IOException e15) {
                byteArrayOutputStream = null;
                gZIPInputStream = null;
                byteArrayInputStream = null;
                byteArrayOutputStream.close();
                gZIPInputStream.close();
                byteArrayInputStream.close();
                return bArr;
            } catch (Throwable th4) {
                th = th4;
                byteArrayOutputStream2 = null;
                gZIPInputStream2 = null;
                byteArrayInputStream = null;
                byteArrayOutputStream2.close();
                gZIPInputStream2.close();
                byteArrayInputStream.close();
                throw th;
            }
        }
        return bArr;
    }

    /* access modifiers changed from: private */
    public static final byte[] c(int i) {
        return (i & 16) == 16 ? c : (i & 32) == 32 ? e : f695a;
    }

    /* access modifiers changed from: private */
    public static final byte[] d(int i) {
        return (i & 16) == 16 ? d : (i & 32) == 32 ? f : b;
    }
}
