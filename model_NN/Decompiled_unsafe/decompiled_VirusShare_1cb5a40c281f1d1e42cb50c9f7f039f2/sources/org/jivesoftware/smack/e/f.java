package org.jivesoftware.smack.e;

import java.io.ByteArrayInputStream;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import org.jivesoftware.smack.Connection;
import org.jivesoftware.smack.b.b;
import org.jivesoftware.smack.b.d;
import org.jivesoftware.smack.b.e;
import org.jivesoftware.smack.b.g;
import org.jivesoftware.smack.b.h;
import org.jivesoftware.smack.b.i;
import org.jivesoftware.smack.b.j;
import org.jivesoftware.smack.b.k;
import org.jivesoftware.smack.b.l;
import org.jivesoftware.smack.b.m;
import org.jivesoftware.smack.b.n;
import org.jivesoftware.smack.b.o;
import org.jivesoftware.smack.b.p;
import org.jivesoftware.smack.b.q;
import org.jivesoftware.smack.b.r;
import org.jivesoftware.smack.b.s;
import org.jivesoftware.smack.b.t;
import org.jivesoftware.smack.b.w;
import org.jivesoftware.smack.f.a;
import org.jivesoftware.smack.f.c;
import org.xmlpull.v1.XmlPullParser;

public final class f {
    /* JADX INFO: additional move instructions added (1) to help type inference */
    private static Object a(String str, Class cls, XmlPullParser xmlPullParser) {
        Object newInstance = cls.newInstance();
        boolean z = false;
        while (!z) {
            int next = xmlPullParser.next();
            if (next == 2) {
                String name = xmlPullParser.getName();
                String nextText = xmlPullParser.nextText();
                Class<?> returnType = newInstance.getClass().getMethod("get" + Character.toUpperCase(name.charAt(0)) + name.substring(1), new Class[0]).getReturnType();
                Object obj = nextText;
                if (!returnType.getName().equals("java.lang.String")) {
                    obj = returnType.getName().equals("boolean") ? Boolean.valueOf(nextText) : returnType.getName().equals("int") ? Integer.valueOf(nextText) : returnType.getName().equals("long") ? Long.valueOf(nextText) : returnType.getName().equals("float") ? Float.valueOf(nextText) : returnType.getName().equals("double") ? Double.valueOf(nextText) : returnType.getName().equals("java.lang.Class") ? Class.forName(nextText) : null;
                }
                newInstance.getClass().getMethod("set" + Character.toUpperCase(name.charAt(0)) + name.substring(1), returnType).invoke(newInstance, obj);
            } else if (next == 3 && xmlPullParser.getName().equals(str)) {
                z = true;
            }
        }
        return newInstance;
    }

    private static org.jivesoftware.smack.b.f a(String str, String str2, XmlPullParser xmlPullParser) {
        Object b = c.a().b(str, str2);
        if (b != null) {
            if (b instanceof a) {
                return ((a) b).a(xmlPullParser);
            }
            if (b instanceof Class) {
                return (org.jivesoftware.smack.b.f) a(str, (Class) b, xmlPullParser);
            }
        }
        s sVar = new s(str, str2);
        boolean z = false;
        while (!z) {
            int next = xmlPullParser.next();
            if (next == 2) {
                String name = xmlPullParser.getName();
                if (xmlPullParser.isEmptyElementTag()) {
                    sVar.a(name, "");
                } else if (xmlPullParser.next() == 4) {
                    sVar.a(name, xmlPullParser.getText());
                }
            } else if (next == 3 && xmlPullParser.getName().equals(str)) {
                z = true;
            }
        }
        return sVar;
    }

    public static p a(XmlPullParser xmlPullParser, Connection connection) {
        p pVar;
        p pVar2;
        String nextText;
        String attributeValue = xmlPullParser.getAttributeValue("", "id");
        String attributeValue2 = xmlPullParser.getAttributeValue("", "to");
        String attributeValue3 = xmlPullParser.getAttributeValue("", "from");
        l a2 = l.a(xmlPullParser.getAttributeValue("", "type"));
        g gVar = null;
        boolean z = false;
        d dVar = null;
        while (!z) {
            int next = xmlPullParser.next();
            if (next == 2) {
                String name = xmlPullParser.getName();
                String namespace = xmlPullParser.getNamespace();
                if (name.equals("error")) {
                    dVar = h(xmlPullParser);
                } else if (name.equals("query") && namespace.equals("jabber:iq:auth")) {
                    g gVar2 = new g();
                    boolean z2 = false;
                    while (!z2) {
                        int next2 = xmlPullParser.next();
                        if (next2 == 2) {
                            if (xmlPullParser.getName().equals("username")) {
                                gVar2.a(xmlPullParser.nextText());
                            } else if (xmlPullParser.getName().equals("password")) {
                                gVar2.b(xmlPullParser.nextText());
                            } else if (xmlPullParser.getName().equals("digest")) {
                                gVar2.c(xmlPullParser.nextText());
                            } else if (xmlPullParser.getName().equals("resource")) {
                                gVar2.d(xmlPullParser.nextText());
                            }
                        } else if (next2 == 3 && xmlPullParser.getName().equals("query")) {
                            z2 = true;
                        }
                    }
                    gVar = gVar2;
                } else if (name.equals("query") && namespace.equals("jabber:iq:roster")) {
                    t tVar = new t();
                    boolean z3 = false;
                    b bVar = null;
                    while (!z3) {
                        int next3 = xmlPullParser.next();
                        if (next3 == 2) {
                            if (xmlPullParser.getName().equals("item")) {
                                b bVar2 = new b(xmlPullParser.getAttributeValue("", "jid"), xmlPullParser.getAttributeValue("", "name"));
                                bVar2.a(h.a(xmlPullParser.getAttributeValue("", "ask")));
                                String attributeValue4 = xmlPullParser.getAttributeValue("", "subscription");
                                if (attributeValue4 == null) {
                                    attributeValue4 = "none";
                                }
                                bVar2.a(org.jivesoftware.smack.b.c.valueOf(attributeValue4));
                                bVar = bVar2;
                            }
                            if (xmlPullParser.getName().equals("group") && bVar != null && (nextText = xmlPullParser.nextText()) != null && nextText.trim().length() > 0) {
                                bVar.a(nextText);
                            }
                        } else if (next3 == 3) {
                            if (xmlPullParser.getName().equals("item")) {
                                tVar.a(bVar);
                            }
                            if (xmlPullParser.getName().equals("query")) {
                                z3 = true;
                            }
                        }
                    }
                    gVar = tVar;
                } else if (name.equals("query") && namespace.equals("jabber:iq:register")) {
                    m mVar = new m();
                    HashMap hashMap = null;
                    boolean z4 = false;
                    while (!z4) {
                        int next4 = xmlPullParser.next();
                        if (next4 == 2) {
                            if (xmlPullParser.getNamespace().equals("jabber:iq:register")) {
                                String name2 = xmlPullParser.getName();
                                String str = "";
                                if (hashMap == null) {
                                    hashMap = new HashMap();
                                }
                                if (xmlPullParser.next() == 4) {
                                    str = xmlPullParser.getText();
                                }
                                if (!name2.equals("instructions")) {
                                    hashMap.put(name2, str);
                                } else {
                                    mVar.a(str);
                                }
                            } else {
                                mVar.a(a(xmlPullParser.getName(), xmlPullParser.getNamespace(), xmlPullParser));
                            }
                        } else if (next4 == 3 && xmlPullParser.getName().equals("query")) {
                            z4 = true;
                        }
                    }
                    mVar.a(hashMap);
                    gVar = mVar;
                } else if (!name.equals("bind") || !namespace.equals("urn:ietf:params:xml:ns:xmpp-bind")) {
                    Object a3 = c.a().a(name, namespace);
                    if (a3 != null) {
                        if (a3 instanceof org.jivesoftware.smack.f.b) {
                            gVar = ((org.jivesoftware.smack.f.b) a3).a(xmlPullParser);
                        } else if (a3 instanceof Class) {
                            pVar2 = (p) a(name, (Class) a3, xmlPullParser);
                            gVar = pVar2;
                        }
                    }
                    pVar2 = gVar;
                    gVar = pVar2;
                } else {
                    gVar = f(xmlPullParser);
                }
            } else {
                z = (next != 3 || !xmlPullParser.getName().equals("iq")) ? z : true;
            }
        }
        "iqpcket is : " + gVar;
        if (gVar != null) {
            pVar = gVar;
        } else if (l.f667a == a2 || l.b == a2) {
            d dVar2 = new d();
            dVar2.e(attributeValue);
            dVar2.f(attributeValue3);
            dVar2.g(attributeValue2);
            dVar2.a(l.d);
            dVar2.a(new d(i.e));
            connection.a(dVar2);
            return null;
        } else {
            pVar = new e();
        }
        pVar.e(attributeValue);
        pVar.f(attributeValue2);
        pVar.g(attributeValue3);
        pVar.a(a2);
        pVar.a(dVar);
        return pVar;
    }

    public static w a(XmlPullParser xmlPullParser) {
        o oVar = new o();
        String attributeValue = xmlPullParser.getAttributeValue("", "id");
        if (attributeValue == null) {
            attributeValue = "ID_NOT_AVAILABLE";
        }
        oVar.e(attributeValue);
        oVar.f(xmlPullParser.getAttributeValue("", "to"));
        oVar.g(xmlPullParser.getAttributeValue("", "from"));
        oVar.a(j.a(xmlPullParser.getAttributeValue("", "type")));
        String i = i(xmlPullParser);
        if (i != null && !"".equals(i.trim())) {
            oVar.d(i);
        }
        String str = null;
        String str2 = null;
        boolean z = false;
        Map map = null;
        while (!z) {
            int next = xmlPullParser.next();
            if (next == 2) {
                String name = xmlPullParser.getName();
                String namespace = xmlPullParser.getNamespace();
                if (name.equals("subject")) {
                    if (str2 == null) {
                        str2 = xmlPullParser.nextText();
                    }
                } else if (name.equals("body")) {
                    oVar.a(i(xmlPullParser), xmlPullParser.nextText());
                } else if (name.equals("thread")) {
                    if (str == null) {
                        str = xmlPullParser.nextText();
                    }
                } else if (name.equals("error")) {
                    oVar.a(h(xmlPullParser));
                } else if (!name.equals("properties") || !namespace.equals("http://www.jivesoftware.com/xmlns/xmpp/properties")) {
                    oVar.a(a(name, namespace, xmlPullParser));
                } else {
                    map = g(xmlPullParser);
                }
            } else if (next == 3 && xmlPullParser.getName().equals("message")) {
                z = true;
            }
        }
        oVar.a(str2);
        oVar.c(str);
        if (map != null) {
            for (String str3 : map.keySet()) {
                oVar.a(str3, map.get(str3));
            }
        }
        return oVar;
    }

    public static q b(XmlPullParser xmlPullParser) {
        r rVar = r.available;
        String attributeValue = xmlPullParser.getAttributeValue("", "type");
        if (attributeValue != null && !attributeValue.equals("")) {
            try {
                rVar = r.valueOf(attributeValue);
            } catch (IllegalArgumentException e) {
                System.err.println("Found invalid presence type " + attributeValue);
            }
        }
        q qVar = new q(rVar);
        qVar.f(xmlPullParser.getAttributeValue("", "to"));
        qVar.g(xmlPullParser.getAttributeValue("", "from"));
        String attributeValue2 = xmlPullParser.getAttributeValue("", "id");
        qVar.e(attributeValue2 == null ? "ID_NOT_AVAILABLE" : attributeValue2);
        String i = i(xmlPullParser);
        if (i != null && !"".equals(i.trim())) {
            qVar.b(i);
        }
        if (attributeValue2 == null) {
            attributeValue2 = "ID_NOT_AVAILABLE";
        }
        qVar.e(attributeValue2);
        boolean z = false;
        while (!z) {
            int next = xmlPullParser.next();
            if (next == 2) {
                String name = xmlPullParser.getName();
                String namespace = xmlPullParser.getNamespace();
                if (name.equals("status")) {
                    qVar.a(xmlPullParser.nextText());
                } else if (name.equals("priority")) {
                    try {
                        qVar.a(Integer.parseInt(xmlPullParser.nextText()));
                    } catch (NumberFormatException e2) {
                    } catch (IllegalArgumentException e3) {
                        qVar.a(0);
                    }
                } else if (name.equals("show")) {
                    String nextText = xmlPullParser.nextText();
                    try {
                        qVar.a(n.valueOf(nextText));
                    } catch (IllegalArgumentException e4) {
                        System.err.println("Found invalid presence mode " + nextText);
                    }
                } else if (name.equals("error")) {
                    qVar.a(h(xmlPullParser));
                } else if (!name.equals("properties") || !namespace.equals("http://www.jivesoftware.com/xmlns/xmpp/properties")) {
                    qVar.a(a(name, namespace, xmlPullParser));
                } else {
                    Map g = g(xmlPullParser);
                    for (String str : g.keySet()) {
                        qVar.a(str, g.get(str));
                    }
                }
            } else {
                z = (next != 3 || !xmlPullParser.getName().equals("presence")) ? z : true;
            }
        }
        return qVar;
    }

    public static Collection c(XmlPullParser xmlPullParser) {
        ArrayList arrayList = new ArrayList();
        boolean z = false;
        while (!z) {
            int next = xmlPullParser.next();
            if (next == 2) {
                if (xmlPullParser.getName().equals("method")) {
                    arrayList.add(xmlPullParser.nextText());
                }
            } else if (next == 3 && xmlPullParser.getName().equals("compression")) {
                z = true;
            }
        }
        return arrayList;
    }

    public static org.jivesoftware.smack.c.i d(XmlPullParser xmlPullParser) {
        String str = null;
        boolean z = false;
        while (!z) {
            int next = xmlPullParser.next();
            if (next == 2) {
                if (!xmlPullParser.getName().equals("failure")) {
                    str = xmlPullParser.getName();
                }
            } else if (next == 3 && xmlPullParser.getName().equals("failure")) {
                z = true;
            }
        }
        return new org.jivesoftware.smack.c.i(str);
    }

    public static e e(XmlPullParser xmlPullParser) {
        e eVar = null;
        boolean z = false;
        while (!z) {
            int next = xmlPullParser.next();
            if (next == 2) {
                eVar = new e(xmlPullParser.getName());
            } else if (next == 3 && xmlPullParser.getName().equals("error")) {
                z = true;
            }
        }
        return eVar;
    }

    private static org.jivesoftware.smack.b.a f(XmlPullParser xmlPullParser) {
        org.jivesoftware.smack.b.a aVar = new org.jivesoftware.smack.b.a();
        boolean z = false;
        while (!z) {
            int next = xmlPullParser.next();
            if (next == 2) {
                if (xmlPullParser.getName().equals("resource")) {
                    aVar.a(xmlPullParser.nextText());
                } else if (xmlPullParser.getName().equals("jid")) {
                    aVar.b(xmlPullParser.nextText());
                }
            } else if (next == 3 && xmlPullParser.getName().equals("bind")) {
                z = true;
            }
        }
        return aVar;
    }

    private static Map g(XmlPullParser xmlPullParser) {
        HashMap hashMap = new HashMap();
        while (true) {
            int next = xmlPullParser.next();
            if (next == 2 && xmlPullParser.getName().equals("property")) {
                String str = null;
                String str2 = null;
                String str3 = null;
                boolean z = false;
                Object obj = null;
                while (!z) {
                    int next2 = xmlPullParser.next();
                    if (next2 == 2) {
                        String name = xmlPullParser.getName();
                        if (name.equals("name")) {
                            str3 = xmlPullParser.nextText();
                        } else if (name.equals("value")) {
                            str2 = xmlPullParser.getAttributeValue("", "type");
                            str = xmlPullParser.nextText();
                        }
                    } else if (next2 == 3 && xmlPullParser.getName().equals("property")) {
                        if ("integer".equals(str2)) {
                            obj = Integer.valueOf(str);
                        } else if ("long".equals(str2)) {
                            obj = Long.valueOf(str);
                        } else if ("float".equals(str2)) {
                            obj = Float.valueOf(str);
                        } else if ("double".equals(str2)) {
                            obj = Double.valueOf(str);
                        } else if ("boolean".equals(str2)) {
                            obj = Boolean.valueOf(str);
                        } else if ("string".equals(str2)) {
                            obj = str;
                        } else if ("java-object".equals(str2)) {
                            try {
                                obj = new ObjectInputStream(new ByteArrayInputStream(h.g(str))).readObject();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        if (!(str3 == null || obj == null)) {
                            hashMap.put(str3, obj);
                        }
                        z = true;
                    }
                }
            } else if (next == 3 && xmlPullParser.getName().equals("properties")) {
                return hashMap;
            }
        }
    }

    private static d h(XmlPullParser xmlPullParser) {
        k kVar;
        ArrayList arrayList = new ArrayList();
        String str = null;
        String str2 = "-1";
        int i = 0;
        while (i < xmlPullParser.getAttributeCount()) {
            String attributeValue = xmlPullParser.getAttributeName(i).equals("code") ? xmlPullParser.getAttributeValue("", "code") : str2;
            if (xmlPullParser.getAttributeName(i).equals("type")) {
                str = xmlPullParser.getAttributeValue("", "type");
            }
            i++;
            str2 = attributeValue;
        }
        boolean z = false;
        String str3 = null;
        String str4 = null;
        while (!z) {
            int next = xmlPullParser.next();
            if (next == 2) {
                if (xmlPullParser.getName().equals("text")) {
                    str4 = xmlPullParser.nextText();
                } else {
                    String name = xmlPullParser.getName();
                    String namespace = xmlPullParser.getNamespace();
                    if ("urn:ietf:params:xml:ns:xmpp-stanzas".equals(namespace)) {
                        str3 = name;
                    } else {
                        arrayList.add(a(name, namespace, xmlPullParser));
                    }
                }
            } else if (next == 3 && xmlPullParser.getName().equals("error")) {
                z = true;
            }
        }
        k kVar2 = k.CANCEL;
        if (str != null) {
            try {
                kVar2 = k.valueOf(str.toUpperCase());
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
                kVar = kVar2;
            }
        }
        kVar = kVar2;
        return new d(Integer.parseInt(str2), kVar, str3, str4, arrayList);
    }

    private static String i(XmlPullParser xmlPullParser) {
        for (int i = 0; i < xmlPullParser.getAttributeCount(); i++) {
            String attributeName = xmlPullParser.getAttributeName(i);
            if ("xml:lang".equals(attributeName) || ("lang".equals(attributeName) && "xml".equals(xmlPullParser.getAttributePrefix(i)))) {
                return xmlPullParser.getAttributeValue(i);
            }
        }
        return null;
    }
}
