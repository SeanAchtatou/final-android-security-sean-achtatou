package org.jivesoftware.smack.e;

import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

public final class j extends Reader {

    /* renamed from: a  reason: collision with root package name */
    private Reader f697a = null;
    private List b = new ArrayList();

    public j(Reader reader) {
        this.f697a = reader;
    }

    public final void a(c cVar) {
        if (cVar != null) {
            synchronized (this.b) {
                if (!this.b.contains(cVar)) {
                    this.b.add(cVar);
                }
            }
        }
    }

    public final void b(c cVar) {
        synchronized (this.b) {
            this.b.remove(cVar);
        }
    }

    public final void close() {
        this.f697a.close();
    }

    public final void mark(int i) {
        this.f697a.mark(i);
    }

    public final boolean markSupported() {
        return this.f697a.markSupported();
    }

    public final int read() {
        return this.f697a.read();
    }

    public final int read(char[] cArr) {
        return this.f697a.read(cArr);
    }

    public final int read(char[] cArr, int i, int i2) {
        c[] cVarArr;
        int read = this.f697a.read(cArr, i, i2);
        if (read > 0) {
            String str = new String(cArr, i, read);
            synchronized (this.b) {
                cVarArr = new c[this.b.size()];
                this.b.toArray(cVarArr);
            }
            for (c a2 : cVarArr) {
                a2.a(str);
            }
        }
        return read;
    }

    public final boolean ready() {
        return this.f697a.ready();
    }

    public final void reset() {
        this.f697a.reset();
    }

    public final long skip(long j) {
        return this.f697a.skip(j);
    }
}
