package org.jivesoftware.smack;

import org.jivesoftware.smack.a.a;
import org.jivesoftware.smack.b.w;

public final class j {

    /* renamed from: a  reason: collision with root package name */
    private s f702a;
    private a b;

    public final void a(w wVar) {
        if (this.b != null) {
            this.b.a(wVar);
        }
    }

    public final boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj instanceof j) {
            return ((j) obj).f702a.equals(this.f702a);
        }
        if (obj instanceof s) {
            return obj.equals(this.f702a);
        }
        return false;
    }
}
