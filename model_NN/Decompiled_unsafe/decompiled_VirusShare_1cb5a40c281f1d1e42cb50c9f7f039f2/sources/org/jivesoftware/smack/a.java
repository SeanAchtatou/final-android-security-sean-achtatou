package org.jivesoftware.smack;

import org.jivesoftware.smack.a.c;
import org.jivesoftware.smack.b.g;
import org.jivesoftware.smack.b.l;
import org.jivesoftware.smack.b.p;

final class a {

    /* renamed from: a  reason: collision with root package name */
    private Connection f649a;

    public a(Connection connection) {
        this.f649a = connection;
    }

    public final String a(String str, String str2, String str3) {
        g gVar = new g();
        gVar.a(l.f667a);
        gVar.a(str);
        l a2 = this.f649a.a(new c(gVar.f()));
        this.f649a.a(gVar);
        p pVar = (p) a2.a((long) g.a());
        if (pVar == null) {
            throw new ad("No response from the server.");
        } else if (pVar.d() == l.d) {
            throw new ad(pVar.i());
        } else {
            g gVar2 = (g) pVar;
            a2.a();
            g gVar3 = new g();
            gVar3.a(str);
            if (gVar2.c() != null) {
                gVar3.a(this.f649a.e(), str2);
            } else if (gVar2.a() != null) {
                gVar3.b(str2);
            } else {
                throw new ad("Server does not support compatible authentication mechanism.");
            }
            gVar3.d(str3);
            l a3 = this.f649a.a(new c(gVar3.f()));
            this.f649a.a(gVar3);
            p pVar2 = (p) a3.a((long) g.a());
            if (pVar2 == null) {
                throw new ad("Authentication failed.");
            } else if (pVar2.d() == l.d) {
                throw new ad(pVar2.i());
            } else {
                a3.a();
                return pVar2.g();
            }
        }
    }
}
