package org.jivesoftware.smack;

import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import org.jivesoftware.smack.a.b;
import org.jivesoftware.smack.b.q;
import org.jivesoftware.smack.b.r;
import org.jivesoftware.smack.b.t;
import org.jivesoftware.smack.e.h;

public final class ac {
    private static y b = y.accept_all;

    /* renamed from: a  reason: collision with root package name */
    boolean f653a = false;
    /* access modifiers changed from: private */
    public Connection c;
    /* access modifiers changed from: private */
    public final Map d;
    /* access modifiers changed from: private */
    public final Map e;
    /* access modifiers changed from: private */
    public final List f;
    private final List g;
    /* access modifiers changed from: private */
    public Map h;
    private ae i;
    /* access modifiers changed from: private */
    public y j = b;

    ac(Connection connection) {
        this.c = connection;
        this.d = new ConcurrentHashMap();
        this.f = new CopyOnWriteArrayList();
        this.e = new ConcurrentHashMap();
        this.g = new CopyOnWriteArrayList();
        this.h = new ConcurrentHashMap();
        connection.a(new r(this), new b(t.class));
        b bVar = new b(q.class);
        this.i = new ae(this);
        connection.a(this.i, bVar);
        connection.a(new aa(this));
    }

    static /* synthetic */ String a(ac acVar, String str) {
        p pVar = null;
        if (str == null) {
            return null;
        }
        if (str != null) {
            pVar = (p) acVar.e.get(str.toLowerCase());
        }
        return (!(pVar != null) ? h.d(str) : str).toLowerCase();
    }

    static /* synthetic */ void a(ac acVar) {
        for (String str : acVar.h.keySet()) {
            Map map = (Map) acVar.h.get(str);
            if (map != null) {
                for (String str2 : map.keySet()) {
                    q qVar = new q(r.unavailable);
                    qVar.g(str + "/" + str2);
                    acVar.i.a(qVar);
                }
            }
        }
    }

    static /* synthetic */ void a(ac acVar, Collection collection, Collection collection2, Collection collection3) {
        Iterator it = acVar.g.iterator();
        while (it.hasNext()) {
            it.next();
            collection.isEmpty();
            collection2.isEmpty();
            collection3.isEmpty();
        }
    }

    static /* synthetic */ void d(ac acVar) {
        Iterator it = acVar.g.iterator();
        while (it.hasNext()) {
            it.next();
        }
    }

    public final x a(String str) {
        if (this.d.containsKey(str)) {
            throw new IllegalArgumentException("Group with name " + str + " alread exists.");
        }
        x xVar = new x(str, this.c);
        this.d.put(str, xVar);
        return xVar;
    }

    public final void a() {
        this.c.a(new t());
    }

    public final Collection b() {
        return Collections.unmodifiableCollection(this.d.values());
    }

    public final x b(String str) {
        return (x) this.d.get(str);
    }

    /* access modifiers changed from: package-private */
    public final void c() {
        this.g.clear();
    }
}
