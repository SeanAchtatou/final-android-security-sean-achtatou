package org.jivesoftware.smack;

import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Vector;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

public final class g {

    /* renamed from: a  reason: collision with root package name */
    private static int f699a;
    private static int b;
    private static Vector c = new Vector();
    private static int d = 120000;

    static {
        InputStream inputStream;
        InputStream inputStream2;
        f699a = 5000;
        b = 240000;
        try {
            for (ClassLoader resources : b()) {
                Enumeration<URL> resources2 = resources.getResources("META-INF/smack-config.xml");
                while (resources2.hasMoreElements()) {
                    inputStream = null;
                    try {
                        InputStream openStream = resources2.nextElement().openStream();
                        try {
                            XmlPullParser newPullParser = XmlPullParserFactory.newInstance().newPullParser();
                            newPullParser.setFeature("http://xmlpull.org/v1/doc/features.html#process-namespaces", true);
                            newPullParser.setInput(openStream, "UTF-8");
                            int eventType = newPullParser.getEventType();
                            do {
                                if (eventType == 2) {
                                    if (newPullParser.getName().equals("className")) {
                                        String nextText = newPullParser.nextText();
                                        try {
                                            Class.forName(nextText);
                                        } catch (ClassNotFoundException e) {
                                            System.err.println("Error! A startup class specified in smack-config.xml could not be loaded: " + nextText);
                                        }
                                    } else if (newPullParser.getName().equals("packetReplyTimeout")) {
                                        f699a = a(newPullParser, f699a);
                                    } else if (newPullParser.getName().equals("keepAliveInterval")) {
                                        b = a(newPullParser, b);
                                    } else if (newPullParser.getName().equals("mechName")) {
                                        c.add(newPullParser.nextText());
                                    }
                                }
                                eventType = newPullParser.next();
                            } while (eventType != 1);
                            try {
                                openStream.close();
                            } catch (Exception e2) {
                            }
                        } catch (Exception e3) {
                            Exception exc = e3;
                            inputStream = openStream;
                            e = exc;
                        } catch (Throwable th) {
                            Throwable th2 = th;
                            inputStream2 = openStream;
                            th = th2;
                            try {
                                inputStream2.close();
                            } catch (Exception e4) {
                            }
                            throw th;
                        }
                    } catch (Exception e5) {
                        e = e5;
                    } catch (Throwable th3) {
                        th = th3;
                        inputStream2 = null;
                        inputStream2.close();
                        throw th;
                    }
                }
            }
            return;
            try {
                e.printStackTrace();
                try {
                    inputStream.close();
                } catch (Exception e6) {
                }
            } catch (Throwable th4) {
                th = th4;
                inputStream2 = inputStream;
                inputStream2.close();
                throw th;
            }
        } catch (Exception e7) {
            e7.printStackTrace();
        }
    }

    private g() {
    }

    public static int a() {
        if (f699a <= 0) {
            f699a = 5000;
        }
        return f699a;
    }

    private static int a(XmlPullParser xmlPullParser, int i) {
        try {
            return Integer.parseInt(xmlPullParser.nextText());
        } catch (NumberFormatException e) {
            e.printStackTrace();
            return i;
        }
    }

    private static ClassLoader[] b() {
        ClassLoader[] classLoaderArr = {g.class.getClassLoader(), Thread.currentThread().getContextClassLoader()};
        ArrayList arrayList = new ArrayList();
        for (ClassLoader classLoader : classLoaderArr) {
            if (classLoader != null) {
                arrayList.add(classLoader);
            }
        }
        return (ClassLoader[]) arrayList.toArray(new ClassLoader[arrayList.size()]);
    }
}
