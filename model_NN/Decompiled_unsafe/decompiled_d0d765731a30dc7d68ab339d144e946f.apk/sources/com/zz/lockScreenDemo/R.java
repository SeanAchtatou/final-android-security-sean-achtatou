package com.zz.lockScreenDemo;

public final class R {

    public static final class attr {
    }

    public static final class dimen {
        public static final int activity_horizontal_margin = 2131034112;
        public static final int activity_vertical_margin = 2131034113;
    }

    public static final class drawable {
        public static final int ic_launcher = 2130837504;
        public static final int test1 = 2130837505;
    }

    public static final class id {
        public static final int action_settings = 2131296256;
    }

    public static final class layout {
        public static final int activity_main = 2130903040;
    }

    public static final class menu {
        public static final int main = 2131230720;
    }

    public static final class string {
        public static final int action_settings = 2131099650;
        public static final int app_name = 2131099648;
        public static final int device = 2131099651;
        public static final int device_des = 2131099652;
        public static final int hello_world = 2131099649;
    }

    public static final class style {
        public static final int AppBaseTheme = 2131165184;
        public static final int AppTheme = 2131165185;
    }

    public static final class xml {
        public static final int demo = 2130968576;
    }
}
