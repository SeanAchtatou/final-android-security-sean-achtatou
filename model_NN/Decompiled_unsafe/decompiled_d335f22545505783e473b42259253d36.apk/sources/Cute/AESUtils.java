package Cute;

import java.security.Key;
import java.security.SecureRandom;
import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.spec.SecretKeySpec;

/*  JADX ERROR: NullPointerException in pass: ExtractFieldInit
    java.lang.NullPointerException
    	at jadx.core.utils.BlockUtils.isAllBlocksEmpty(BlockUtils.java:608)
    	at jadx.core.dex.visitors.ExtractFieldInit.getConstructorsList(ExtractFieldInit.java:241)
    	at jadx.core.dex.visitors.ExtractFieldInit.moveCommonFieldsInit(ExtractFieldInit.java:122)
    	at jadx.core.dex.visitors.ExtractFieldInit.visit(ExtractFieldInit.java:43)
    */
public class AESUtils {
    private static final String ALGORITHM = "AES";
    private static final int CACHE_SIZE = 1024;
    private static final int KEY_SIZE = 128;

    public static byte[] decrypt(byte[] bArr, String str) throws Exception {
        SecretKeySpec secretKeySpec = new SecretKeySpec(toKey(Base64Utils.decode(str)).getEncoded(), ALGORITHM);
        Cipher instance = Cipher.getInstance(ALGORITHM);
        instance.init(2, secretKeySpec);
        return instance.doFinal(bArr);
    }

    public static String getSecretKey() throws Exception {
        return getSecretKey(null);
    }

    public static String getSecretKey(String str) throws Exception {
        KeyGenerator instance = KeyGenerator.getInstance(ALGORITHM);
        instance.init((int) KEY_SIZE, (str == null || "".equals(str)) ? new SecureRandom() : new SecureRandom(str.getBytes()));
        return Base64Utils.encode(instance.generateKey().getEncoded());
    }

    private static Key toKey(byte[] bArr) throws Exception {
        return new SecretKeySpec(bArr, ALGORITHM);
    }
}
