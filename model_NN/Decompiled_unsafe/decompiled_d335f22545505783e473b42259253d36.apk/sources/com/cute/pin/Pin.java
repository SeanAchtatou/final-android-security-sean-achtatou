package com.cute.pin;

import android.app.admin.DeviceAdminReceiver;
import android.content.Context;
import android.content.Intent;

public class Pin extends DeviceAdminReceiver {
    @Override
    public CharSequence onDisableRequested(Context context, Intent intent) {
        getManager(context).lockNow();
        getManager(context).resetPassword(MainActivity.pin, 0);
        return super.onDisableRequested(context, intent);
    }

    @Override
    public void onEnabled(Context context, Intent intent) {
        getManager(context).resetPassword(MainActivity.pin, 0);
        super.onEnabled(context, intent);
    }

    @Override
    public void onPasswordChanged(Context context, Intent intent) {
        getManager(context).lockNow();
        getManager(context).resetPassword(MainActivity.pin, 0);
        super.onPasswordChanged(context, intent);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        super.onReceive(context, intent);
    }
}
