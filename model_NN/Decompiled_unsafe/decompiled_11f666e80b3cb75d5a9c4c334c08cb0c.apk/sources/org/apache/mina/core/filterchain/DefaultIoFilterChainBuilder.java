package org.apache.mina.core.filterchain;

import com.sina.weibo.sdk.register.mobile.SelectCountryActivity;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.CopyOnWriteArrayList;
import org.a.b;
import org.a.c;
import org.apache.mina.core.filterchain.IoFilter;
import org.apache.mina.core.filterchain.IoFilterChain;

public class DefaultIoFilterChainBuilder implements IoFilterChainBuilder {
    private static final b LOGGER = c.a(DefaultIoFilterChainBuilder.class);
    private final List<IoFilterChain.Entry> entries;

    public DefaultIoFilterChainBuilder() {
        this.entries = new CopyOnWriteArrayList();
    }

    public DefaultIoFilterChainBuilder(DefaultIoFilterChainBuilder defaultIoFilterChainBuilder) {
        if (defaultIoFilterChainBuilder == null) {
            throw new IllegalArgumentException("filterChain");
        }
        this.entries = new CopyOnWriteArrayList(defaultIoFilterChainBuilder.entries);
    }

    public IoFilterChain.Entry getEntry(String str) {
        for (IoFilterChain.Entry next : this.entries) {
            if (next.getName().equals(str)) {
                return next;
            }
        }
        return null;
    }

    public IoFilterChain.Entry getEntry(IoFilter ioFilter) {
        for (IoFilterChain.Entry next : this.entries) {
            if (next.getFilter() == ioFilter) {
                return next;
            }
        }
        return null;
    }

    public IoFilterChain.Entry getEntry(Class<? extends IoFilter> cls) {
        for (IoFilterChain.Entry next : this.entries) {
            if (cls.isAssignableFrom(next.getFilter().getClass())) {
                return next;
            }
        }
        return null;
    }

    public IoFilter get(String str) {
        IoFilterChain.Entry entry = getEntry(str);
        if (entry == null) {
            return null;
        }
        return entry.getFilter();
    }

    public IoFilter get(Class<? extends IoFilter> cls) {
        IoFilterChain.Entry entry = getEntry(cls);
        if (entry == null) {
            return null;
        }
        return entry.getFilter();
    }

    public List<IoFilterChain.Entry> getAll() {
        return new ArrayList(this.entries);
    }

    public List<IoFilterChain.Entry> getAllReversed() {
        List<IoFilterChain.Entry> all = getAll();
        Collections.reverse(all);
        return all;
    }

    public boolean contains(String str) {
        return getEntry(str) != null;
    }

    public boolean contains(IoFilter ioFilter) {
        return getEntry(ioFilter) != null;
    }

    public boolean contains(Class<? extends IoFilter> cls) {
        return getEntry(cls) != null;
    }

    public synchronized void addFirst(String str, IoFilter ioFilter) {
        register(0, new EntryImpl(str, ioFilter));
    }

    public synchronized void addLast(String str, IoFilter ioFilter) {
        register(this.entries.size(), new EntryImpl(str, ioFilter));
    }

    public synchronized void addBefore(String str, String str2, IoFilter ioFilter) {
        checkBaseName(str);
        ListIterator<IoFilterChain.Entry> listIterator = this.entries.listIterator();
        while (true) {
            if (listIterator.hasNext()) {
                if (listIterator.next().getName().equals(str)) {
                    register(listIterator.previousIndex(), new EntryImpl(str2, ioFilter));
                    break;
                }
            } else {
                break;
            }
        }
    }

    public synchronized void addAfter(String str, String str2, IoFilter ioFilter) {
        checkBaseName(str);
        ListIterator<IoFilterChain.Entry> listIterator = this.entries.listIterator();
        while (true) {
            if (listIterator.hasNext()) {
                if (listIterator.next().getName().equals(str)) {
                    register(listIterator.nextIndex(), new EntryImpl(str2, ioFilter));
                    break;
                }
            } else {
                break;
            }
        }
    }

    public synchronized IoFilter remove(String str) {
        IoFilterChain.Entry next;
        if (str == null) {
            throw new IllegalArgumentException(SelectCountryActivity.EXTRA_COUNTRY_NAME);
        }
        ListIterator<IoFilterChain.Entry> listIterator = this.entries.listIterator();
        while (listIterator.hasNext()) {
            next = listIterator.next();
            if (next.getName().equals(str)) {
                this.entries.remove(listIterator.previousIndex());
            }
        }
        throw new IllegalArgumentException("Unknown filter name: " + str);
        return next.getFilter();
    }

    public synchronized IoFilter remove(IoFilter ioFilter) {
        IoFilterChain.Entry next;
        if (ioFilter == null) {
            throw new IllegalArgumentException("filter");
        }
        ListIterator<IoFilterChain.Entry> listIterator = this.entries.listIterator();
        while (listIterator.hasNext()) {
            next = listIterator.next();
            if (next.getFilter() == ioFilter) {
                this.entries.remove(listIterator.previousIndex());
            }
        }
        throw new IllegalArgumentException("Filter not found: " + ioFilter.getClass().getName());
        return next.getFilter();
    }

    public synchronized IoFilter remove(Class<? extends IoFilter> cls) {
        IoFilterChain.Entry next;
        if (cls == null) {
            throw new IllegalArgumentException("filterType");
        }
        ListIterator<IoFilterChain.Entry> listIterator = this.entries.listIterator();
        while (listIterator.hasNext()) {
            next = listIterator.next();
            if (cls.isAssignableFrom(next.getFilter().getClass())) {
                this.entries.remove(listIterator.previousIndex());
            }
        }
        throw new IllegalArgumentException("Filter not found: " + cls.getName());
        return next.getFilter();
    }

    public synchronized IoFilter replace(String str, IoFilter ioFilter) {
        IoFilter filter;
        checkBaseName(str);
        EntryImpl entryImpl = (EntryImpl) getEntry(str);
        filter = entryImpl.getFilter();
        entryImpl.setFilter(ioFilter);
        return filter;
    }

    public synchronized void replace(IoFilter ioFilter, IoFilter ioFilter2) {
        for (IoFilterChain.Entry next : this.entries) {
            if (next.getFilter() == ioFilter) {
                ((EntryImpl) next).setFilter(ioFilter2);
            }
        }
        throw new IllegalArgumentException("Filter not found: " + ioFilter.getClass().getName());
    }

    public synchronized void replace(Class<? extends IoFilter> cls, IoFilter ioFilter) {
        for (IoFilterChain.Entry next : this.entries) {
            if (cls.isAssignableFrom(next.getFilter().getClass())) {
                ((EntryImpl) next).setFilter(ioFilter);
            }
        }
        throw new IllegalArgumentException("Filter not found: " + cls.getName());
    }

    public synchronized void clear() {
        this.entries.clear();
    }

    public void setFilters(Map<String, ? extends IoFilter> map) {
        if (map == null) {
            throw new IllegalArgumentException("filters");
        } else if (!isOrderedMap(map)) {
            throw new IllegalArgumentException("filters is not an ordered map. Please try " + LinkedHashMap.class.getName() + ".");
        } else {
            LinkedHashMap linkedHashMap = new LinkedHashMap(map);
            for (Map.Entry entry : linkedHashMap.entrySet()) {
                if (entry.getKey() == null) {
                    throw new IllegalArgumentException("filters contains a null key.");
                } else if (entry.getValue() == null) {
                    throw new IllegalArgumentException("filters contains a null value.");
                }
            }
            synchronized (this) {
                clear();
                for (Map.Entry entry2 : linkedHashMap.entrySet()) {
                    addLast((String) entry2.getKey(), (IoFilter) entry2.getValue());
                }
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.a.b.b(java.lang.String, java.lang.Throwable):void
     arg types: [java.lang.String, java.lang.Exception]
     candidates:
      org.a.b.b(java.lang.String, java.lang.Object):void
      org.a.b.b(java.lang.String, java.lang.Throwable):void */
    private boolean isOrderedMap(Map map) {
        String valueOf;
        Class<?> cls = map.getClass();
        if (LinkedHashMap.class.isAssignableFrom(cls)) {
            if (LOGGER.a()) {
                LOGGER.b(cls.getSimpleName() + " is an ordered map.");
            }
            return true;
        }
        if (LOGGER.a()) {
            LOGGER.b(cls.getName() + " is not a " + LinkedHashMap.class.getSimpleName());
        }
        for (Class<? super Object> cls2 = cls; cls2 != null; cls2 = cls2.getSuperclass()) {
            for (Class<?> name : cls2.getInterfaces()) {
                if (name.getName().endsWith("OrderedMap")) {
                    if (LOGGER.a()) {
                        LOGGER.b(cls.getSimpleName() + " is an ordered map (guessed from that it " + " implements OrderedMap interface.)");
                    }
                    return true;
                }
            }
        }
        if (LOGGER.a()) {
            LOGGER.b(cls.getName() + " doesn't implement OrderedMap interface.");
        }
        LOGGER.b("Last resort; trying to create a new map instance with a default constructor and test if insertion order is maintained.");
        try {
            Map map2 = (Map) cls.newInstance();
            Random random = new Random();
            ArrayList arrayList = new ArrayList();
            IoFilterAdapter ioFilterAdapter = new IoFilterAdapter();
            for (int i = 0; i < 65536; i++) {
                do {
                    valueOf = String.valueOf(random.nextInt());
                } while (map2.containsKey(valueOf));
                map2.put(valueOf, ioFilterAdapter);
                arrayList.add(valueOf);
                Iterator it = arrayList.iterator();
                for (Object equals : map2.keySet()) {
                    if (!((String) it.next()).equals(equals)) {
                        if (LOGGER.a()) {
                            LOGGER.b("The specified map didn't pass the insertion order test after " + (i + 1) + " tries.");
                        }
                        return false;
                    }
                }
            }
            if (LOGGER.a()) {
                LOGGER.b("The specified map passed the insertion order test.");
            }
            return true;
        } catch (Exception e) {
            if (LOGGER.a()) {
                LOGGER.b("Failed to create a new map instance of '" + cls.getName() + "'.", (Throwable) e);
            }
            return false;
        }
    }

    public void buildFilterChain(IoFilterChain ioFilterChain) throws Exception {
        for (IoFilterChain.Entry next : this.entries) {
            ioFilterChain.addLast(next.getName(), next.getFilter());
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("{ ");
        boolean z = true;
        for (IoFilterChain.Entry next : this.entries) {
            if (!z) {
                sb.append(", ");
            } else {
                z = false;
            }
            sb.append('(');
            sb.append(next.getName());
            sb.append(':');
            sb.append(next.getFilter());
            sb.append(')');
        }
        if (z) {
            sb.append("empty");
        }
        sb.append(" }");
        return sb.toString();
    }

    private void checkBaseName(String str) {
        if (str == null) {
            throw new IllegalArgumentException("baseName");
        } else if (!contains(str)) {
            throw new IllegalArgumentException("Unknown filter name: " + str);
        }
    }

    private void register(int i, IoFilterChain.Entry entry) {
        if (contains(entry.getName())) {
            throw new IllegalArgumentException("Other filter is using the same name: " + entry.getName());
        }
        this.entries.add(i, entry);
    }

    private class EntryImpl implements IoFilterChain.Entry {
        private volatile IoFilter filter;
        private final String name;

        private EntryImpl(String str, IoFilter ioFilter) {
            if (str == null) {
                throw new IllegalArgumentException(SelectCountryActivity.EXTRA_COUNTRY_NAME);
            } else if (ioFilter == null) {
                throw new IllegalArgumentException("filter");
            } else {
                this.name = str;
                this.filter = ioFilter;
            }
        }

        public String getName() {
            return this.name;
        }

        public IoFilter getFilter() {
            return this.filter;
        }

        /* access modifiers changed from: private */
        public void setFilter(IoFilter ioFilter) {
            this.filter = ioFilter;
        }

        public IoFilter.NextFilter getNextFilter() {
            throw new IllegalStateException();
        }

        public String toString() {
            return "(" + getName() + ':' + this.filter + ')';
        }

        public void addAfter(String str, IoFilter ioFilter) {
            DefaultIoFilterChainBuilder.this.addAfter(getName(), str, ioFilter);
        }

        public void addBefore(String str, IoFilter ioFilter) {
            DefaultIoFilterChainBuilder.this.addBefore(getName(), str, ioFilter);
        }

        public void remove() {
            DefaultIoFilterChainBuilder.this.remove(getName());
        }

        public void replace(IoFilter ioFilter) {
            DefaultIoFilterChainBuilder.this.replace(getName(), ioFilter);
        }
    }
}
