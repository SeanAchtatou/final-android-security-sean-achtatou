package org.apache.mina.util.byteaccess;

import java.util.ArrayList;
import java.util.Stack;
import org.apache.mina.core.buffer.IoBuffer;

public class ByteArrayPool implements ByteArrayFactory {
    private final int MAX_BITS = 32;
    private final boolean direct;
    /* access modifiers changed from: private */
    public int freeBufferCount = 0;
    /* access modifiers changed from: private */
    public ArrayList<Stack<DirectBufferByteArray>> freeBuffers;
    /* access modifiers changed from: private */
    public long freeMemory = 0;
    private boolean freed;
    /* access modifiers changed from: private */
    public final int maxFreeBuffers;
    /* access modifiers changed from: private */
    public final int maxFreeMemory;

    static /* synthetic */ int access$208(ByteArrayPool byteArrayPool) {
        int i = byteArrayPool.freeBufferCount;
        byteArrayPool.freeBufferCount = i + 1;
        return i;
    }

    static /* synthetic */ long access$414(ByteArrayPool byteArrayPool, long j) {
        long j2 = byteArrayPool.freeMemory + j;
        byteArrayPool.freeMemory = j2;
        return j2;
    }

    public ByteArrayPool(boolean z, int i, int i2) {
        this.direct = z;
        this.freeBuffers = new ArrayList<>();
        for (int i3 = 0; i3 < 32; i3++) {
            this.freeBuffers.add(new Stack());
        }
        this.maxFreeBuffers = i;
        this.maxFreeMemory = i2;
        this.freed = false;
    }

    public ByteArray create(int i) {
        if (i < 1) {
            throw new IllegalArgumentException("Buffer size must be at least 1: " + i);
        }
        int bits = bits(i);
        synchronized (this) {
            if (!this.freeBuffers.get(bits).isEmpty()) {
                DirectBufferByteArray directBufferByteArray = (DirectBufferByteArray) this.freeBuffers.get(bits).pop();
                directBufferByteArray.setFreed(false);
                directBufferByteArray.getSingleIoBuffer().limit(i);
                return directBufferByteArray;
            }
            IoBuffer allocate = IoBuffer.allocate(1 << bits, this.direct);
            allocate.limit(i);
            DirectBufferByteArray directBufferByteArray2 = new DirectBufferByteArray(allocate);
            directBufferByteArray2.setFreed(false);
            return directBufferByteArray2;
        }
    }

    /* access modifiers changed from: private */
    public int bits(int i) {
        int i2 = 0;
        while ((1 << i2) < i) {
            i2++;
        }
        return i2;
    }

    public void free() {
        synchronized (this) {
            if (this.freed) {
                throw new IllegalStateException("Already freed.");
            }
            this.freed = true;
            this.freeBuffers.clear();
            this.freeBuffers = null;
        }
    }

    private class DirectBufferByteArray extends BufferByteArray {
        public boolean freed;

        public DirectBufferByteArray(IoBuffer ioBuffer) {
            super(ioBuffer);
        }

        public void setFreed(boolean z) {
            this.freed = z;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:30:?, code lost:
            return;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void free() {
            /*
                r6 = this;
                monitor-enter(r6)
                boolean r0 = r6.freed     // Catch:{ all -> 0x000d }
                if (r0 == 0) goto L_0x0010
                java.lang.IllegalStateException r0 = new java.lang.IllegalStateException     // Catch:{ all -> 0x000d }
                java.lang.String r1 = "Already freed."
                r0.<init>(r1)     // Catch:{ all -> 0x000d }
                throw r0     // Catch:{ all -> 0x000d }
            L_0x000d:
                r0 = move-exception
                monitor-exit(r6)     // Catch:{ all -> 0x000d }
                throw r0
            L_0x0010:
                r0 = 1
                r6.freed = r0     // Catch:{ all -> 0x000d }
                monitor-exit(r6)     // Catch:{ all -> 0x000d }
                org.apache.mina.util.byteaccess.ByteArrayPool r0 = org.apache.mina.util.byteaccess.ByteArrayPool.this
                int r1 = r6.last()
                int r0 = r0.bits(r1)
                org.apache.mina.util.byteaccess.ByteArrayPool r1 = org.apache.mina.util.byteaccess.ByteArrayPool.this
                monitor-enter(r1)
                org.apache.mina.util.byteaccess.ByteArrayPool r2 = org.apache.mina.util.byteaccess.ByteArrayPool.this     // Catch:{ all -> 0x0070 }
                java.util.ArrayList r2 = r2.freeBuffers     // Catch:{ all -> 0x0070 }
                if (r2 == 0) goto L_0x006e
                org.apache.mina.util.byteaccess.ByteArrayPool r2 = org.apache.mina.util.byteaccess.ByteArrayPool.this     // Catch:{ all -> 0x0070 }
                int r2 = r2.freeBufferCount     // Catch:{ all -> 0x0070 }
                org.apache.mina.util.byteaccess.ByteArrayPool r3 = org.apache.mina.util.byteaccess.ByteArrayPool.this     // Catch:{ all -> 0x0070 }
                int r3 = r3.maxFreeBuffers     // Catch:{ all -> 0x0070 }
                if (r2 >= r3) goto L_0x006e
                org.apache.mina.util.byteaccess.ByteArrayPool r2 = org.apache.mina.util.byteaccess.ByteArrayPool.this     // Catch:{ all -> 0x0070 }
                long r2 = r2.freeMemory     // Catch:{ all -> 0x0070 }
                int r4 = r6.last()     // Catch:{ all -> 0x0070 }
                long r4 = (long) r4     // Catch:{ all -> 0x0070 }
                long r2 = r2 + r4
                org.apache.mina.util.byteaccess.ByteArrayPool r4 = org.apache.mina.util.byteaccess.ByteArrayPool.this     // Catch:{ all -> 0x0070 }
                int r4 = r4.maxFreeMemory     // Catch:{ all -> 0x0070 }
                long r4 = (long) r4     // Catch:{ all -> 0x0070 }
                int r2 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
                if (r2 > 0) goto L_0x006e
                org.apache.mina.util.byteaccess.ByteArrayPool r2 = org.apache.mina.util.byteaccess.ByteArrayPool.this     // Catch:{ all -> 0x0070 }
                java.util.ArrayList r2 = r2.freeBuffers     // Catch:{ all -> 0x0070 }
                java.lang.Object r0 = r2.get(r0)     // Catch:{ all -> 0x0070 }
                java.util.Stack r0 = (java.util.Stack) r0     // Catch:{ all -> 0x0070 }
                r0.push(r6)     // Catch:{ all -> 0x0070 }
                org.apache.mina.util.byteaccess.ByteArrayPool r0 = org.apache.mina.util.byteaccess.ByteArrayPool.this     // Catch:{ all -> 0x0070 }
                org.apache.mina.util.byteaccess.ByteArrayPool.access$208(r0)     // Catch:{ all -> 0x0070 }
                org.apache.mina.util.byteaccess.ByteArrayPool r0 = org.apache.mina.util.byteaccess.ByteArrayPool.this     // Catch:{ all -> 0x0070 }
                int r2 = r6.last()     // Catch:{ all -> 0x0070 }
                long r2 = (long) r2     // Catch:{ all -> 0x0070 }
                org.apache.mina.util.byteaccess.ByteArrayPool.access$414(r0, r2)     // Catch:{ all -> 0x0070 }
                monitor-exit(r1)     // Catch:{ all -> 0x0070 }
            L_0x006d:
                return
            L_0x006e:
                monitor-exit(r1)     // Catch:{ all -> 0x0070 }
                goto L_0x006d
            L_0x0070:
                r0 = move-exception
                monitor-exit(r1)     // Catch:{ all -> 0x0070 }
                throw r0
            */
            throw new UnsupportedOperationException("Method not decompiled: org.apache.mina.util.byteaccess.ByteArrayPool.DirectBufferByteArray.free():void");
        }
    }
}
