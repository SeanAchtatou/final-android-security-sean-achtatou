package org.apache.mina.util;

public interface ExpirationListener<E> {
    void expired(Object obj);
}
