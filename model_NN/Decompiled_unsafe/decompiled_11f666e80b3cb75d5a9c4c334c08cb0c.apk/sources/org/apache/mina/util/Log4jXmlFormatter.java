package org.apache.mina.util;

import java.util.Arrays;
import java.util.Map;
import java.util.Set;
import java.util.logging.Formatter;
import java.util.logging.LogRecord;
import org.a.d;
import org.apache.mina.proxy.handlers.http.HttpProxyConstants;

public class Log4jXmlFormatter extends Formatter {
    private final int DEFAULT_SIZE = 256;
    private final int UPPER_LIMIT = 2048;
    private StringBuffer buf = new StringBuffer(256);
    private boolean locationInfo = false;
    private boolean properties = false;

    public void setLocationInfo(boolean z) {
        this.locationInfo = z;
    }

    public boolean getLocationInfo() {
        return this.locationInfo;
    }

    public void setProperties(boolean z) {
        this.properties = z;
    }

    public boolean getProperties() {
        return this.properties;
    }

    public String format(LogRecord logRecord) {
        Map a2;
        Set keySet;
        String[] throwableStrRep;
        if (this.buf.capacity() > 2048) {
            this.buf = new StringBuffer(256);
        } else {
            this.buf.setLength(0);
        }
        this.buf.append("<log4j:event logger=\"");
        this.buf.append(Transform.escapeTags(logRecord.getLoggerName()));
        this.buf.append("\" timestamp=\"");
        this.buf.append(logRecord.getMillis());
        this.buf.append("\" level=\"");
        this.buf.append(Transform.escapeTags(logRecord.getLevel().getName()));
        this.buf.append("\" thread=\"");
        this.buf.append(String.valueOf(logRecord.getThreadID()));
        this.buf.append("\">\r\n");
        this.buf.append("<log4j:message><![CDATA[");
        Transform.appendEscapingCDATA(this.buf, logRecord.getMessage());
        this.buf.append("]]></log4j:message>\r\n");
        if (!(logRecord.getThrown() == null || (throwableStrRep = Transform.getThrowableStrRep(logRecord.getThrown())) == null)) {
            this.buf.append("<log4j:throwable><![CDATA[");
            for (String appendEscapingCDATA : throwableStrRep) {
                Transform.appendEscapingCDATA(this.buf, appendEscapingCDATA);
                this.buf.append(HttpProxyConstants.CRLF);
            }
            this.buf.append("]]></log4j:throwable>\r\n");
        }
        if (this.locationInfo) {
            this.buf.append("<log4j:locationInfo class=\"");
            this.buf.append(Transform.escapeTags(logRecord.getSourceClassName()));
            this.buf.append("\" method=\"");
            this.buf.append(Transform.escapeTags(logRecord.getSourceMethodName()));
            this.buf.append("\" file=\"?\" line=\"?\"/>\r\n");
        }
        if (this.properties && (a2 = d.a()) != null && (keySet = a2.keySet()) != null && keySet.size() > 0) {
            this.buf.append("<log4j:properties>\r\n");
            Object[] array = keySet.toArray();
            Arrays.sort(array);
            int length = array.length;
            for (int i = 0; i < length; i++) {
                Object obj = array[i];
                String obj2 = obj == null ? "" : obj.toString();
                Object obj3 = a2.get(obj2);
                if (obj3 != null) {
                    this.buf.append("<log4j:data name=\"");
                    this.buf.append(Transform.escapeTags(obj2));
                    this.buf.append("\" value=\"");
                    this.buf.append(Transform.escapeTags(String.valueOf(obj3)));
                    this.buf.append("\"/>\r\n");
                }
            }
            this.buf.append("</log4j:properties>\r\n");
        }
        this.buf.append("</log4j:event>\r\n\r\n");
        return this.buf.toString();
    }
}
