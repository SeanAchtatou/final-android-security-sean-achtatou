package org.apache.mina.filter.codec.statemachine;

import java.util.ArrayList;
import java.util.List;
import org.a.b;
import org.a.c;
import org.apache.mina.core.filterchain.IoFilter;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.ProtocolDecoderOutput;

public abstract class DecodingStateMachine implements DecodingState {
    private final ProtocolDecoderOutput childOutput = new ProtocolDecoderOutput() {
        public void flush(IoFilter.NextFilter nextFilter, IoSession ioSession) {
        }

        public void write(Object obj) {
            DecodingStateMachine.this.childProducts.add(obj);
        }
    };
    /* access modifiers changed from: private */
    public final List<Object> childProducts = new ArrayList();
    private DecodingState currentState;
    private boolean initialized;
    private final b log = c.a(DecodingStateMachine.class);

    /* access modifiers changed from: protected */
    public abstract void destroy() throws Exception;

    /* access modifiers changed from: protected */
    public abstract DecodingState finishDecode(List<Object> list, ProtocolDecoderOutput protocolDecoderOutput) throws Exception;

    /* access modifiers changed from: protected */
    public abstract DecodingState init() throws Exception;

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0025, code lost:
        r6.currentState = r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0028, code lost:
        if (r1 != null) goto L_0x002d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x002a, code lost:
        cleanup();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x003f, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0048, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0049, code lost:
        r5 = r1;
        r1 = r0;
        r0 = r5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:?, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:?, code lost:
        r0 = finishDecode(r6.childProducts, r8);
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public org.apache.mina.filter.codec.statemachine.DecodingState decode(org.apache.mina.core.buffer.IoBuffer r7, org.apache.mina.filter.codec.ProtocolDecoderOutput r8) throws java.lang.Exception {
        /*
            r6 = this;
            org.apache.mina.filter.codec.statemachine.DecodingState r1 = r6.getCurrentState()
            int r4 = r7.limit()
            int r0 = r7.position()
            r2 = r0
            r0 = r1
        L_0x000e:
            if (r2 != r4) goto L_0x0018
        L_0x0010:
            r6.currentState = r0
            if (r0 != 0) goto L_0x0017
            r6.cleanup()
        L_0x0017:
            return r6
        L_0x0018:
            org.apache.mina.filter.codec.ProtocolDecoderOutput r1 = r6.childOutput     // Catch:{ Exception -> 0x003c, all -> 0x0048 }
            org.apache.mina.filter.codec.statemachine.DecodingState r1 = r0.decode(r7, r1)     // Catch:{ Exception -> 0x003c, all -> 0x0048 }
            if (r1 != 0) goto L_0x002f
            java.util.List<java.lang.Object> r0 = r6.childProducts     // Catch:{ Exception -> 0x003c }
            org.apache.mina.filter.codec.statemachine.DecodingState r0 = r6.finishDecode(r0, r8)     // Catch:{ Exception -> 0x003c }
            r6.currentState = r1
            if (r1 != 0) goto L_0x002d
            r6.cleanup()
        L_0x002d:
            r6 = r0
            goto L_0x0017
        L_0x002f:
            int r3 = r7.position()     // Catch:{ Exception -> 0x003c }
            if (r3 != r2) goto L_0x0039
            if (r0 != r1) goto L_0x0039
            r0 = r1
            goto L_0x0010
        L_0x0039:
            r2 = r3
            r0 = r1
            goto L_0x000e
        L_0x003c:
            r0 = move-exception
            r1 = 0
            throw r0     // Catch:{ all -> 0x003f }
        L_0x003f:
            r0 = move-exception
        L_0x0040:
            r6.currentState = r1
            if (r1 != 0) goto L_0x0047
            r6.cleanup()
        L_0x0047:
            throw r0
        L_0x0048:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x0040
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.mina.filter.codec.statemachine.DecodingStateMachine.decode(org.apache.mina.core.buffer.IoBuffer, org.apache.mina.filter.codec.ProtocolDecoderOutput):org.apache.mina.filter.codec.statemachine.DecodingState");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.a.b.b(java.lang.String, java.lang.Throwable):void
     arg types: [java.lang.String, java.lang.Exception]
     candidates:
      org.a.b.b(java.lang.String, java.lang.Object):void
      org.a.b.b(java.lang.String, java.lang.Throwable):void */
    public DecodingState finishDecode(ProtocolDecoderOutput protocolDecoderOutput) throws Exception {
        DecodingState finishDecode;
        DecodingState finishDecode2;
        DecodingState currentState2 = getCurrentState();
        while (true) {
            try {
                finishDecode2 = currentState2.finishDecode(this.childOutput);
                if (!(finishDecode2 == null || currentState2 == finishDecode2)) {
                    currentState2 = finishDecode2;
                }
            } catch (Exception e) {
                this.log.b("Ignoring the exception caused by a closed session.", (Throwable) e);
                this.currentState = null;
                finishDecode = finishDecode(this.childProducts, protocolDecoderOutput);
                if (0 == 0) {
                    cleanup();
                }
            } catch (Throwable th) {
                this.currentState = null;
                finishDecode(this.childProducts, protocolDecoderOutput);
                if (0 == 0) {
                    cleanup();
                }
                throw th;
            }
        }
        this.currentState = finishDecode2;
        finishDecode = finishDecode(this.childProducts, protocolDecoderOutput);
        if (finishDecode2 == null) {
            cleanup();
        }
        return finishDecode;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.a.b.d(java.lang.String, java.lang.Throwable):void
     arg types: [java.lang.String, java.lang.Exception]
     candidates:
      org.a.b.d(java.lang.String, java.lang.Object):void
      org.a.b.d(java.lang.String, java.lang.Throwable):void */
    private void cleanup() {
        if (!this.initialized) {
            throw new IllegalStateException();
        }
        this.initialized = false;
        this.childProducts.clear();
        try {
            destroy();
        } catch (Exception e) {
            this.log.d("Failed to destroy a decoding state machine.", (Throwable) e);
        }
    }

    private DecodingState getCurrentState() throws Exception {
        DecodingState decodingState = this.currentState;
        if (decodingState != null) {
            return decodingState;
        }
        DecodingState init = init();
        this.initialized = true;
        return init;
    }
}
