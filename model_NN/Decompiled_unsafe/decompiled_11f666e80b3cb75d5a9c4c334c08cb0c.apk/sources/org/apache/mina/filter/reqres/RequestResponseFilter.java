package org.apache.mina.filter.reqres;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import org.a.b;
import org.a.c;
import org.apache.mina.core.filterchain.IoFilter;
import org.apache.mina.core.filterchain.IoFilterChain;
import org.apache.mina.core.session.AttributeKey;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.core.write.WriteRequest;
import org.apache.mina.filter.util.WriteRequestFilter;

public class RequestResponseFilter extends WriteRequestFilter {
    private static final b LOGGER = c.a(RequestResponseFilter.class);
    private final AttributeKey REQUEST_STORE = new AttributeKey(getClass(), "requestStore");
    private final AttributeKey RESPONSE_INSPECTOR = new AttributeKey(getClass(), "responseInspector");
    private final AttributeKey UNRESPONDED_REQUEST_STORE = new AttributeKey(getClass(), "unrespondedRequestStore");
    private final ResponseInspectorFactory responseInspectorFactory;
    private final ScheduledExecutorService timeoutScheduler;

    public RequestResponseFilter(final ResponseInspector responseInspector, ScheduledExecutorService scheduledExecutorService) {
        if (responseInspector == null) {
            throw new IllegalArgumentException("responseInspector");
        } else if (scheduledExecutorService == null) {
            throw new IllegalArgumentException("timeoutScheduler");
        } else {
            this.responseInspectorFactory = new ResponseInspectorFactory() {
                public ResponseInspector getResponseInspector() {
                    return responseInspector;
                }
            };
            this.timeoutScheduler = scheduledExecutorService;
        }
    }

    public RequestResponseFilter(ResponseInspectorFactory responseInspectorFactory2, ScheduledExecutorService scheduledExecutorService) {
        if (responseInspectorFactory2 == null) {
            throw new IllegalArgumentException("responseInspectorFactory");
        } else if (scheduledExecutorService == null) {
            throw new IllegalArgumentException("timeoutScheduler");
        } else {
            this.responseInspectorFactory = responseInspectorFactory2;
            this.timeoutScheduler = scheduledExecutorService;
        }
    }

    public void onPreAdd(IoFilterChain ioFilterChain, String str, IoFilter.NextFilter nextFilter) throws Exception {
        if (ioFilterChain.contains(this)) {
            throw new IllegalArgumentException("You can't add the same filter instance more than once.  Create another instance and add it.");
        }
        IoSession session = ioFilterChain.getSession();
        session.setAttribute(this.RESPONSE_INSPECTOR, this.responseInspectorFactory.getResponseInspector());
        session.setAttribute(this.REQUEST_STORE, createRequestStore(session));
        session.setAttribute(this.UNRESPONDED_REQUEST_STORE, createUnrespondedRequestStore(session));
    }

    public void onPostRemove(IoFilterChain ioFilterChain, String str, IoFilter.NextFilter nextFilter) throws Exception {
        IoSession session = ioFilterChain.getSession();
        destroyUnrespondedRequestStore(getUnrespondedRequestStore(session));
        destroyRequestStore(getRequestStore(session));
        session.removeAttribute(this.UNRESPONDED_REQUEST_STORE);
        session.removeAttribute(this.REQUEST_STORE);
        session.removeAttribute(this.RESPONSE_INSPECTOR);
    }

    public void messageReceived(IoFilter.NextFilter nextFilter, IoSession ioSession, Object obj) throws Exception {
        Request request;
        ScheduledFuture<?> timeoutFuture;
        ResponseInspector responseInspector = (ResponseInspector) ioSession.getAttribute(this.RESPONSE_INSPECTOR);
        Object requestId = responseInspector.getRequestId(obj);
        if (requestId == null) {
            nextFilter.messageReceived(ioSession, obj);
            return;
        }
        ResponseType responseType = responseInspector.getResponseType(obj);
        if (responseType == null) {
            nextFilter.exceptionCaught(ioSession, new IllegalStateException(responseInspector.getClass().getName() + "#getResponseType() may not return null."));
        }
        Map<Object, Request> requestStore = getRequestStore(ioSession);
        switch (responseType) {
            case WHOLE:
            case PARTIAL_LAST:
                synchronized (requestStore) {
                    request = requestStore.remove(requestId);
                    break;
                }
            case PARTIAL:
                synchronized (requestStore) {
                    request = requestStore.get(requestId);
                }
                break;
            default:
                throw new InternalError();
        }
        if (request != null) {
            if (!(responseType == ResponseType.PARTIAL || (timeoutFuture = request.getTimeoutFuture()) == null)) {
                timeoutFuture.cancel(false);
                Set<Request> unrespondedRequestStore = getUnrespondedRequestStore(ioSession);
                synchronized (unrespondedRequestStore) {
                    unrespondedRequestStore.remove(request);
                }
            }
            Response response = new Response(request, obj, responseType);
            request.signal(response);
            nextFilter.messageReceived(ioSession, response);
        } else if (LOGGER.b()) {
            LOGGER.d("Unknown request ID '" + requestId + "' for the response message. Timed out already?: " + obj);
        }
    }

    /* access modifiers changed from: protected */
    public Object doFilterWrite(IoFilter.NextFilter nextFilter, IoSession ioSession, WriteRequest writeRequest) throws Exception {
        Request request;
        Object message = writeRequest.getMessage();
        if (!(message instanceof Request)) {
            return null;
        }
        Request request2 = (Request) message;
        if (request2.getTimeoutFuture() != null) {
            throw new IllegalArgumentException("Request can not be reused.");
        }
        Map<Object, Request> requestStore = getRequestStore(ioSession);
        Object id = request2.getId();
        synchronized (requestStore) {
            request = requestStore.get(id);
            if (request == null) {
                requestStore.put(id, request2);
            }
        }
        if (request != null) {
            throw new IllegalStateException("Duplicate request ID: " + request2.getId());
        }
        TimeoutTask timeoutTask = new TimeoutTask(nextFilter, request2, ioSession);
        ScheduledFuture<?> schedule = this.timeoutScheduler.schedule(timeoutTask, request2.getTimeoutMillis(), TimeUnit.MILLISECONDS);
        request2.setTimeoutTask(timeoutTask);
        request2.setTimeoutFuture(schedule);
        Set<Request> unrespondedRequestStore = getUnrespondedRequestStore(ioSession);
        synchronized (unrespondedRequestStore) {
            unrespondedRequestStore.add(request2);
        }
        return request2.getMessage();
    }

    public void sessionClosed(IoFilter.NextFilter nextFilter, IoSession ioSession) throws Exception {
        ArrayList<Request> arrayList;
        Set<Request> unrespondedRequestStore = getUnrespondedRequestStore(ioSession);
        synchronized (unrespondedRequestStore) {
            arrayList = new ArrayList<>(unrespondedRequestStore);
            unrespondedRequestStore.clear();
        }
        for (Request request : arrayList) {
            if (request.getTimeoutFuture().cancel(false)) {
                request.getTimeoutTask().run();
            }
        }
        Map<Object, Request> requestStore = getRequestStore(ioSession);
        synchronized (requestStore) {
            requestStore.clear();
        }
        nextFilter.sessionClosed(ioSession);
    }

    /* access modifiers changed from: private */
    public Map<Object, Request> getRequestStore(IoSession ioSession) {
        return (Map) ioSession.getAttribute(this.REQUEST_STORE);
    }

    /* access modifiers changed from: private */
    public Set<Request> getUnrespondedRequestStore(IoSession ioSession) {
        return (Set) ioSession.getAttribute(this.UNRESPONDED_REQUEST_STORE);
    }

    /* access modifiers changed from: protected */
    public Map<Object, Request> createRequestStore(IoSession ioSession) {
        return new ConcurrentHashMap();
    }

    /* access modifiers changed from: protected */
    public Set<Request> createUnrespondedRequestStore(IoSession ioSession) {
        return new LinkedHashSet();
    }

    /* access modifiers changed from: protected */
    public void destroyRequestStore(Map<Object, Request> map) {
    }

    /* access modifiers changed from: protected */
    public void destroyUnrespondedRequestStore(Set<Request> set) {
    }

    private class TimeoutTask implements Runnable {
        private final IoFilter.NextFilter filter;
        private final Request request;
        private final IoSession session;

        private TimeoutTask(IoFilter.NextFilter nextFilter, Request request2, IoSession ioSession) {
            this.filter = nextFilter;
            this.request = request2;
            this.session = ioSession;
        }

        public void run() {
            boolean z;
            Set access$100 = RequestResponseFilter.this.getUnrespondedRequestStore(this.session);
            if (access$100 != null) {
                synchronized (access$100) {
                    access$100.remove(this.request);
                }
            }
            Map access$200 = RequestResponseFilter.this.getRequestStore(this.session);
            Object id = this.request.getId();
            synchronized (access$200) {
                if (access$200.get(id) == this.request) {
                    access$200.remove(id);
                    z = true;
                } else {
                    z = false;
                }
            }
            if (z) {
                RequestTimeoutException requestTimeoutException = new RequestTimeoutException(this.request);
                this.request.signal(requestTimeoutException);
                this.filter.exceptionCaught(this.session, requestTimeoutException);
            }
        }
    }
}
