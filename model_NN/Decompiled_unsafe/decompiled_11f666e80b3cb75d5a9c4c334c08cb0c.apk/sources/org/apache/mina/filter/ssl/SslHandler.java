package org.apache.mina.filter.ssl;

import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import javax.net.ssl.SSLEngine;
import javax.net.ssl.SSLEngineResult;
import javax.net.ssl.SSLException;
import javax.net.ssl.SSLHandshakeException;
import org.a.b;
import org.a.c;
import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.filterchain.IoFilter;
import org.apache.mina.core.filterchain.IoFilterEvent;
import org.apache.mina.core.future.DefaultWriteFuture;
import org.apache.mina.core.future.WriteFuture;
import org.apache.mina.core.session.IoEventType;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.core.write.DefaultWriteRequest;
import org.apache.mina.core.write.WriteRequest;

class SslHandler {
    private static final b LOGGER = c.a(SslHandler.class);
    private IoBuffer appBuffer;
    private final IoBuffer emptyBuffer = IoBuffer.allocate(0);
    private final Queue<IoFilterEvent> filterWriteEventQueue = new ConcurrentLinkedQueue();
    private boolean firstSSLNegociation;
    private boolean handshakeComplete;
    private SSLEngineResult.HandshakeStatus handshakeStatus;
    private IoBuffer inNetBuffer;
    private final Queue<IoFilterEvent> messageReceivedEventQueue = new ConcurrentLinkedQueue();
    private IoBuffer outNetBuffer;
    private final Queue<IoFilterEvent> preHandshakeEventQueue = new ConcurrentLinkedQueue();
    private final IoSession session;
    private SSLEngine sslEngine;
    private final SslFilter sslFilter;
    private boolean writingEncryptedData;

    SslHandler(SslFilter sslFilter2, IoSession ioSession) throws SSLException {
        this.sslFilter = sslFilter2;
        this.session = ioSession;
    }

    /* access modifiers changed from: package-private */
    public void init() throws SSLException {
        if (this.sslEngine == null) {
            LOGGER.b("{} Initializing the SSL Handler", this.sslFilter.getSessionInfo(this.session));
            InetSocketAddress inetSocketAddress = (InetSocketAddress) this.session.getAttribute(SslFilter.PEER_ADDRESS);
            if (inetSocketAddress == null) {
                this.sslEngine = this.sslFilter.sslContext.createSSLEngine();
            } else {
                this.sslEngine = this.sslFilter.sslContext.createSSLEngine(inetSocketAddress.getHostName(), inetSocketAddress.getPort());
            }
            this.sslEngine.setUseClientMode(this.sslFilter.isUseClientMode());
            if (!this.sslEngine.getUseClientMode()) {
                if (this.sslFilter.isWantClientAuth()) {
                    this.sslEngine.setWantClientAuth(true);
                }
                if (this.sslFilter.isNeedClientAuth()) {
                    this.sslEngine.setNeedClientAuth(true);
                }
            }
            if (this.sslFilter.getEnabledCipherSuites() != null) {
                this.sslEngine.setEnabledCipherSuites(this.sslFilter.getEnabledCipherSuites());
            }
            if (this.sslFilter.getEnabledProtocols() != null) {
                this.sslEngine.setEnabledProtocols(this.sslFilter.getEnabledProtocols());
            }
            this.sslEngine.beginHandshake();
            this.handshakeStatus = this.sslEngine.getHandshakeStatus();
            this.writingEncryptedData = false;
            this.firstSSLNegociation = true;
            this.handshakeComplete = false;
            if (LOGGER.a()) {
                LOGGER.b("{} SSL Handler Initialization done.", this.sslFilter.getSessionInfo(this.session));
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.a.b.b(java.lang.String, java.lang.Throwable):void
     arg types: [java.lang.String, javax.net.ssl.SSLException]
     candidates:
      org.a.b.b(java.lang.String, java.lang.Object):void
      org.a.b.b(java.lang.String, java.lang.Throwable):void */
    /* access modifiers changed from: package-private */
    public void destroy() {
        if (this.sslEngine != null) {
            try {
                this.sslEngine.closeInbound();
            } catch (SSLException e) {
                LOGGER.b("Unexpected exception from SSLEngine.closeInbound().", (Throwable) e);
            }
            if (this.outNetBuffer != null) {
                this.outNetBuffer.capacity(this.sslEngine.getSession().getPacketBufferSize());
            } else {
                createOutNetBuffer(0);
            }
            do {
                try {
                    this.outNetBuffer.clear();
                } catch (SSLException e2) {
                } finally {
                    destroyOutNetBuffer();
                }
            } while (this.sslEngine.wrap(this.emptyBuffer.buf(), this.outNetBuffer.buf()).bytesProduced() > 0);
            this.sslEngine.closeOutbound();
            this.sslEngine = null;
            this.preHandshakeEventQueue.clear();
        }
    }

    private void destroyOutNetBuffer() {
        this.outNetBuffer.free();
        this.outNetBuffer = null;
    }

    /* access modifiers changed from: package-private */
    public SslFilter getSslFilter() {
        return this.sslFilter;
    }

    /* access modifiers changed from: package-private */
    public IoSession getSession() {
        return this.session;
    }

    /* access modifiers changed from: package-private */
    public boolean isWritingEncryptedData() {
        return this.writingEncryptedData;
    }

    /* access modifiers changed from: package-private */
    public boolean isHandshakeComplete() {
        return this.handshakeComplete;
    }

    /* access modifiers changed from: package-private */
    public boolean isInboundDone() {
        return this.sslEngine == null || this.sslEngine.isInboundDone();
    }

    /* access modifiers changed from: package-private */
    public boolean isOutboundDone() {
        return this.sslEngine == null || this.sslEngine.isOutboundDone();
    }

    /* access modifiers changed from: package-private */
    public boolean needToCompleteHandshake() {
        return this.handshakeStatus == SSLEngineResult.HandshakeStatus.NEED_WRAP && !isInboundDone();
    }

    /* access modifiers changed from: package-private */
    public void schedulePreHandshakeWriteRequest(IoFilter.NextFilter nextFilter, WriteRequest writeRequest) {
        this.preHandshakeEventQueue.add(new IoFilterEvent(nextFilter, IoEventType.WRITE, this.session, writeRequest));
    }

    /* access modifiers changed from: package-private */
    public void flushPreHandshakeEvents() throws SSLException {
        while (true) {
            IoFilterEvent poll = this.preHandshakeEventQueue.poll();
            if (poll != null) {
                this.sslFilter.filterWrite(poll.getNextFilter(), this.session, (WriteRequest) poll.getParameter());
            } else {
                return;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void scheduleFilterWrite(IoFilter.NextFilter nextFilter, WriteRequest writeRequest) {
        this.filterWriteEventQueue.add(new IoFilterEvent(nextFilter, IoEventType.WRITE, this.session, writeRequest));
    }

    /* access modifiers changed from: package-private */
    public void scheduleMessageReceived(IoFilter.NextFilter nextFilter, Object obj) {
        this.messageReceivedEventQueue.add(new IoFilterEvent(nextFilter, IoEventType.MESSAGE_RECEIVED, this.session, obj));
    }

    /* access modifiers changed from: package-private */
    public void flushScheduledEvents() {
        if (!Thread.holdsLock(this)) {
            synchronized (this) {
                while (true) {
                    IoFilterEvent poll = this.filterWriteEventQueue.poll();
                    if (poll == null) {
                        break;
                    }
                    poll.getNextFilter().filterWrite(this.session, (WriteRequest) poll.getParameter());
                }
            }
            while (true) {
                IoFilterEvent poll2 = this.messageReceivedEventQueue.poll();
                if (poll2 != null) {
                    poll2.getNextFilter().messageReceived(this.session, poll2.getParameter());
                } else {
                    return;
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void messageReceived(IoFilter.NextFilter nextFilter, ByteBuffer byteBuffer) throws SSLException {
        if (LOGGER.a()) {
            if (!isOutboundDone()) {
                LOGGER.b("{} Processing the received message", this.sslFilter.getSessionInfo(this.session));
            } else {
                LOGGER.b("{} Processing the received message", this.sslFilter.getSessionInfo(this.session));
            }
        }
        if (this.inNetBuffer == null) {
            this.inNetBuffer = IoBuffer.allocate(byteBuffer.remaining()).setAutoExpand(true);
        }
        this.inNetBuffer.put(byteBuffer);
        if (!this.handshakeComplete) {
            handshake(nextFilter);
        } else {
            this.inNetBuffer.flip();
            if (this.inNetBuffer.hasRemaining()) {
                SSLEngineResult unwrap = unwrap();
                if (this.inNetBuffer.hasRemaining()) {
                    this.inNetBuffer.compact();
                } else {
                    this.inNetBuffer = null;
                }
                checkStatus(unwrap);
                renegotiateIfNeeded(nextFilter, unwrap);
            } else {
                return;
            }
        }
        if (isInboundDone()) {
            byteBuffer.position(byteBuffer.position() - (this.inNetBuffer == null ? 0 : this.inNetBuffer.position()));
            this.inNetBuffer = null;
        }
    }

    /* access modifiers changed from: package-private */
    public IoBuffer fetchAppBuffer() {
        IoBuffer flip = this.appBuffer.flip();
        this.appBuffer = null;
        return flip;
    }

    /* access modifiers changed from: package-private */
    public IoBuffer fetchOutNetBuffer() {
        IoBuffer ioBuffer = this.outNetBuffer;
        if (ioBuffer == null) {
            return this.emptyBuffer;
        }
        this.outNetBuffer = null;
        return ioBuffer.shrink();
    }

    /* access modifiers changed from: package-private */
    public void encrypt(ByteBuffer byteBuffer) throws SSLException {
        if (!this.handshakeComplete) {
            throw new IllegalStateException();
        } else if (byteBuffer.hasRemaining()) {
            createOutNetBuffer(byteBuffer.remaining());
            while (byteBuffer.hasRemaining()) {
                SSLEngineResult wrap = this.sslEngine.wrap(byteBuffer, this.outNetBuffer.buf());
                if (wrap.getStatus() == SSLEngineResult.Status.OK) {
                    if (wrap.getHandshakeStatus() == SSLEngineResult.HandshakeStatus.NEED_TASK) {
                        doTasks();
                    }
                } else if (wrap.getStatus() == SSLEngineResult.Status.BUFFER_OVERFLOW) {
                    this.outNetBuffer.capacity(this.outNetBuffer.capacity() << 1);
                    this.outNetBuffer.limit(this.outNetBuffer.capacity());
                } else {
                    throw new SSLException("SSLEngine error during encrypt: " + wrap.getStatus() + " src: " + byteBuffer + "outNetBuffer: " + this.outNetBuffer);
                }
            }
            this.outNetBuffer.flip();
        } else if (this.outNetBuffer == null) {
            this.outNetBuffer = this.emptyBuffer;
        }
    }

    /* access modifiers changed from: package-private */
    public boolean closeOutbound() throws SSLException {
        SSLEngineResult wrap;
        if (this.sslEngine == null || this.sslEngine.isOutboundDone()) {
            return false;
        }
        this.sslEngine.closeOutbound();
        createOutNetBuffer(0);
        while (true) {
            wrap = this.sslEngine.wrap(this.emptyBuffer.buf(), this.outNetBuffer.buf());
            if (wrap.getStatus() != SSLEngineResult.Status.BUFFER_OVERFLOW) {
                break;
            }
            this.outNetBuffer.capacity(this.outNetBuffer.capacity() << 1);
            this.outNetBuffer.limit(this.outNetBuffer.capacity());
        }
        if (wrap.getStatus() != SSLEngineResult.Status.CLOSED) {
            throw new SSLException("Improper close state: " + wrap);
        }
        this.outNetBuffer.flip();
        return true;
    }

    private void checkStatus(SSLEngineResult sSLEngineResult) throws SSLException {
        SSLEngineResult.Status status = sSLEngineResult.getStatus();
        if (status == SSLEngineResult.Status.BUFFER_OVERFLOW) {
            throw new SSLException("SSLEngine error during decrypt: " + status + " inNetBuffer: " + this.inNetBuffer + "appBuffer: " + this.appBuffer);
        }
    }

    /* renamed from: org.apache.mina.filter.ssl.SslHandler$1  reason: invalid class name */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$javax$net$ssl$SSLEngineResult$HandshakeStatus = new int[SSLEngineResult.HandshakeStatus.values().length];

        static {
            try {
                $SwitchMap$javax$net$ssl$SSLEngineResult$HandshakeStatus[SSLEngineResult.HandshakeStatus.FINISHED.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                $SwitchMap$javax$net$ssl$SSLEngineResult$HandshakeStatus[SSLEngineResult.HandshakeStatus.NOT_HANDSHAKING.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                $SwitchMap$javax$net$ssl$SSLEngineResult$HandshakeStatus[SSLEngineResult.HandshakeStatus.NEED_TASK.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                $SwitchMap$javax$net$ssl$SSLEngineResult$HandshakeStatus[SSLEngineResult.HandshakeStatus.NEED_UNWRAP.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
            try {
                $SwitchMap$javax$net$ssl$SSLEngineResult$HandshakeStatus[SSLEngineResult.HandshakeStatus.NEED_WRAP.ordinal()] = 5;
            } catch (NoSuchFieldError e5) {
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void handshake(IoFilter.NextFilter nextFilter) throws SSLException {
        while (true) {
            switch (AnonymousClass1.$SwitchMap$javax$net$ssl$SSLEngineResult$HandshakeStatus[this.handshakeStatus.ordinal()]) {
                case 1:
                case 2:
                    if (LOGGER.a()) {
                        LOGGER.b("{} processing the FINISHED state", this.sslFilter.getSessionInfo(this.session));
                    }
                    this.session.setAttribute(SslFilter.SSL_SESSION, this.sslEngine.getSession());
                    this.handshakeComplete = true;
                    if (this.firstSSLNegociation && this.session.containsAttribute(SslFilter.USE_NOTIFICATION)) {
                        this.firstSSLNegociation = false;
                        scheduleMessageReceived(nextFilter, SslFilter.SESSION_SECURED);
                    }
                    if (!LOGGER.a()) {
                        return;
                    }
                    if (!isOutboundDone()) {
                        LOGGER.b("{} is now secured", this.sslFilter.getSessionInfo(this.session));
                        return;
                    } else {
                        LOGGER.b("{} is not secured yet", this.sslFilter.getSessionInfo(this.session));
                        return;
                    }
                case 3:
                    if (LOGGER.a()) {
                        LOGGER.b("{} processing the NEED_TASK state", this.sslFilter.getSessionInfo(this.session));
                    }
                    this.handshakeStatus = doTasks();
                    break;
                case 4:
                    if (LOGGER.a()) {
                        LOGGER.b("{} processing the NEED_UNWRAP state", this.sslFilter.getSessionInfo(this.session));
                    }
                    if ((unwrapHandshake(nextFilter) != SSLEngineResult.Status.BUFFER_UNDERFLOW || this.handshakeStatus == SSLEngineResult.HandshakeStatus.FINISHED) && !isInboundDone()) {
                        break;
                    } else {
                        return;
                    }
                case 5:
                    if (LOGGER.a()) {
                        LOGGER.b("{} processing the NEED_WRAP state", this.sslFilter.getSessionInfo(this.session));
                    }
                    if (this.outNetBuffer == null || !this.outNetBuffer.hasRemaining()) {
                        createOutNetBuffer(0);
                        while (true) {
                            SSLEngineResult wrap = this.sslEngine.wrap(this.emptyBuffer.buf(), this.outNetBuffer.buf());
                            if (wrap.getStatus() != SSLEngineResult.Status.BUFFER_OVERFLOW) {
                                this.outNetBuffer.flip();
                                this.handshakeStatus = wrap.getHandshakeStatus();
                                writeNetBuffer(nextFilter);
                                break;
                            } else {
                                this.outNetBuffer.capacity(this.outNetBuffer.capacity() << 1);
                                this.outNetBuffer.limit(this.outNetBuffer.capacity());
                            }
                        }
                    } else {
                        return;
                    }
                    break;
                default:
                    String str = "Invalid Handshaking State" + this.handshakeStatus + " while processing the Handshake for session " + this.session.getId();
                    LOGGER.e(str);
                    throw new IllegalStateException(str);
            }
        }
    }

    private void createOutNetBuffer(int i) {
        int max = Math.max(i, this.sslEngine.getSession().getPacketBufferSize());
        if (this.outNetBuffer != null) {
            this.outNetBuffer.capacity(max);
        } else {
            this.outNetBuffer = IoBuffer.allocate(max).minimumCapacity(0);
        }
    }

    /* access modifiers changed from: package-private */
    public WriteFuture writeNetBuffer(IoFilter.NextFilter nextFilter) throws SSLException {
        if (this.outNetBuffer == null || !this.outNetBuffer.hasRemaining()) {
            return null;
        }
        this.writingEncryptedData = true;
        try {
            IoBuffer fetchOutNetBuffer = fetchOutNetBuffer();
            DefaultWriteFuture defaultWriteFuture = new DefaultWriteFuture(this.session);
            this.sslFilter.filterWrite(nextFilter, this.session, new DefaultWriteRequest(fetchOutNetBuffer, defaultWriteFuture));
            while (needToCompleteHandshake()) {
                handshake(nextFilter);
                IoBuffer fetchOutNetBuffer2 = fetchOutNetBuffer();
                if (fetchOutNetBuffer2 != null && fetchOutNetBuffer2.hasRemaining()) {
                    defaultWriteFuture = new DefaultWriteFuture(this.session);
                    this.sslFilter.filterWrite(nextFilter, this.session, new DefaultWriteRequest(fetchOutNetBuffer2, defaultWriteFuture));
                }
            }
            this.writingEncryptedData = false;
            return defaultWriteFuture;
        } catch (SSLException e) {
            SSLHandshakeException sSLHandshakeException = new SSLHandshakeException("SSL handshake failed.");
            sSLHandshakeException.initCause(e);
            throw sSLHandshakeException;
        } catch (Throwable th) {
            this.writingEncryptedData = false;
            throw th;
        }
    }

    private SSLEngineResult.Status unwrapHandshake(IoFilter.NextFilter nextFilter) throws SSLException {
        if (this.inNetBuffer != null) {
            this.inNetBuffer.flip();
        }
        if (this.inNetBuffer == null || !this.inNetBuffer.hasRemaining()) {
            return SSLEngineResult.Status.BUFFER_UNDERFLOW;
        }
        SSLEngineResult unwrap = unwrap();
        this.handshakeStatus = unwrap.getHandshakeStatus();
        checkStatus(unwrap);
        if (this.handshakeStatus == SSLEngineResult.HandshakeStatus.FINISHED && unwrap.getStatus() == SSLEngineResult.Status.OK && this.inNetBuffer.hasRemaining()) {
            unwrap = unwrap();
            if (this.inNetBuffer.hasRemaining()) {
                this.inNetBuffer.compact();
            } else {
                this.inNetBuffer = null;
            }
            renegotiateIfNeeded(nextFilter, unwrap);
        } else if (this.inNetBuffer.hasRemaining()) {
            this.inNetBuffer.compact();
        } else {
            this.inNetBuffer = null;
        }
        return unwrap.getStatus();
    }

    private void renegotiateIfNeeded(IoFilter.NextFilter nextFilter, SSLEngineResult sSLEngineResult) throws SSLException {
        if (sSLEngineResult.getStatus() != SSLEngineResult.Status.CLOSED && sSLEngineResult.getStatus() != SSLEngineResult.Status.BUFFER_UNDERFLOW && sSLEngineResult.getHandshakeStatus() != SSLEngineResult.HandshakeStatus.NOT_HANDSHAKING) {
            this.handshakeComplete = false;
            this.handshakeStatus = sSLEngineResult.getHandshakeStatus();
            handshake(nextFilter);
        }
    }

    private SSLEngineResult unwrap() throws SSLException {
        SSLEngineResult unwrap;
        if (this.appBuffer == null) {
            this.appBuffer = IoBuffer.allocate(this.inNetBuffer.remaining());
        } else {
            this.appBuffer.expand(this.inNetBuffer.remaining());
        }
        while (true) {
            unwrap = this.sslEngine.unwrap(this.inNetBuffer.buf(), this.appBuffer.buf());
            SSLEngineResult.Status status = unwrap.getStatus();
            SSLEngineResult.HandshakeStatus handshakeStatus2 = unwrap.getHandshakeStatus();
            if (status == SSLEngineResult.Status.BUFFER_OVERFLOW) {
                this.appBuffer.capacity(this.appBuffer.capacity() << 1);
                this.appBuffer.limit(this.appBuffer.capacity());
            }
            if ((status == SSLEngineResult.Status.OK || status == SSLEngineResult.Status.BUFFER_OVERFLOW) && (handshakeStatus2 == SSLEngineResult.HandshakeStatus.NOT_HANDSHAKING || handshakeStatus2 == SSLEngineResult.HandshakeStatus.NEED_UNWRAP)) {
            }
        }
        return unwrap;
    }

    private SSLEngineResult.HandshakeStatus doTasks() {
        while (true) {
            Runnable delegatedTask = this.sslEngine.getDelegatedTask();
            if (delegatedTask == null) {
                return this.sslEngine.getHandshakeStatus();
            }
            delegatedTask.run();
        }
    }

    static IoBuffer copy(ByteBuffer byteBuffer) {
        IoBuffer allocate = IoBuffer.allocate(byteBuffer.remaining());
        allocate.put(byteBuffer);
        allocate.flip();
        return allocate;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("SSLStatus <");
        if (this.handshakeComplete) {
            sb.append("SSL established");
        } else {
            sb.append("Processing Handshake").append("; ");
            sb.append("Status : ").append(this.handshakeStatus).append("; ");
        }
        sb.append(", ");
        sb.append("HandshakeComplete :").append(this.handshakeComplete).append(", ");
        sb.append(">");
        return sb.toString();
    }
}
