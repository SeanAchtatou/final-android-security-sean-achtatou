package org.apache.mina.filter.statistic;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import org.apache.mina.core.filterchain.IoFilter;
import org.apache.mina.core.filterchain.IoFilterAdapter;
import org.apache.mina.core.session.IdleStatus;
import org.apache.mina.core.session.IoEventType;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.core.write.WriteRequest;

public class ProfilerTimerFilter extends IoFilterAdapter {
    private TimerWorker messageReceivedTimerWorker;
    private TimerWorker messageSentTimerWorker;
    private boolean profileMessageReceived;
    private boolean profileMessageSent;
    private boolean profileSessionClosed;
    private boolean profileSessionCreated;
    private boolean profileSessionIdle;
    private boolean profileSessionOpened;
    private TimerWorker sessionClosedTimerWorker;
    private TimerWorker sessionCreatedTimerWorker;
    private TimerWorker sessionIdleTimerWorker;
    private TimerWorker sessionOpenedTimerWorker;
    private volatile TimeUnit timeUnit;

    public ProfilerTimerFilter() {
        this(TimeUnit.MILLISECONDS, IoEventType.MESSAGE_RECEIVED, IoEventType.MESSAGE_SENT);
    }

    public ProfilerTimerFilter(TimeUnit timeUnit2) {
        this(timeUnit2, IoEventType.MESSAGE_RECEIVED, IoEventType.MESSAGE_SENT);
    }

    public ProfilerTimerFilter(TimeUnit timeUnit2, IoEventType... ioEventTypeArr) {
        this.profileMessageReceived = false;
        this.profileMessageSent = false;
        this.profileSessionCreated = false;
        this.profileSessionOpened = false;
        this.profileSessionIdle = false;
        this.profileSessionClosed = false;
        this.timeUnit = timeUnit2;
        setProfilers(ioEventTypeArr);
    }

    private void setProfilers(IoEventType... ioEventTypeArr) {
        for (IoEventType ioEventType : ioEventTypeArr) {
            switch (ioEventType) {
                case MESSAGE_RECEIVED:
                    this.messageReceivedTimerWorker = new TimerWorker();
                    this.profileMessageReceived = true;
                    break;
                case MESSAGE_SENT:
                    this.messageSentTimerWorker = new TimerWorker();
                    this.profileMessageSent = true;
                    break;
                case SESSION_CREATED:
                    this.sessionCreatedTimerWorker = new TimerWorker();
                    this.profileSessionCreated = true;
                    break;
                case SESSION_OPENED:
                    this.sessionOpenedTimerWorker = new TimerWorker();
                    this.profileSessionOpened = true;
                    break;
                case SESSION_IDLE:
                    this.sessionIdleTimerWorker = new TimerWorker();
                    this.profileSessionIdle = true;
                    break;
                case SESSION_CLOSED:
                    this.sessionClosedTimerWorker = new TimerWorker();
                    this.profileSessionClosed = true;
                    break;
            }
        }
    }

    public void setTimeUnit(TimeUnit timeUnit2) {
        this.timeUnit = timeUnit2;
    }

    public void profile(IoEventType ioEventType) {
        switch (ioEventType) {
            case MESSAGE_RECEIVED:
                this.profileMessageReceived = true;
                if (this.messageReceivedTimerWorker == null) {
                    this.messageReceivedTimerWorker = new TimerWorker();
                    return;
                }
                return;
            case MESSAGE_SENT:
                this.profileMessageSent = true;
                if (this.messageSentTimerWorker == null) {
                    this.messageSentTimerWorker = new TimerWorker();
                    return;
                }
                return;
            case SESSION_CREATED:
                this.profileSessionCreated = true;
                if (this.sessionCreatedTimerWorker == null) {
                    this.sessionCreatedTimerWorker = new TimerWorker();
                    return;
                }
                return;
            case SESSION_OPENED:
                this.profileSessionOpened = true;
                if (this.sessionOpenedTimerWorker == null) {
                    this.sessionOpenedTimerWorker = new TimerWorker();
                    return;
                }
                return;
            case SESSION_IDLE:
                this.profileSessionIdle = true;
                if (this.sessionIdleTimerWorker == null) {
                    this.sessionIdleTimerWorker = new TimerWorker();
                    return;
                }
                return;
            case SESSION_CLOSED:
                this.profileSessionClosed = true;
                if (this.sessionClosedTimerWorker == null) {
                    this.sessionClosedTimerWorker = new TimerWorker();
                    return;
                }
                return;
            default:
                return;
        }
    }

    public void stopProfile(IoEventType ioEventType) {
        switch (ioEventType) {
            case MESSAGE_RECEIVED:
                this.profileMessageReceived = false;
                return;
            case MESSAGE_SENT:
                this.profileMessageSent = false;
                return;
            case SESSION_CREATED:
                this.profileSessionCreated = false;
                return;
            case SESSION_OPENED:
                this.profileSessionOpened = false;
                return;
            case SESSION_IDLE:
                this.profileSessionIdle = false;
                return;
            case SESSION_CLOSED:
                this.profileSessionClosed = false;
                return;
            default:
                return;
        }
    }

    public Set<IoEventType> getEventsToProfile() {
        HashSet hashSet = new HashSet();
        if (this.profileMessageReceived) {
            hashSet.add(IoEventType.MESSAGE_RECEIVED);
        }
        if (this.profileMessageSent) {
            hashSet.add(IoEventType.MESSAGE_SENT);
        }
        if (this.profileSessionCreated) {
            hashSet.add(IoEventType.SESSION_CREATED);
        }
        if (this.profileSessionOpened) {
            hashSet.add(IoEventType.SESSION_OPENED);
        }
        if (this.profileSessionIdle) {
            hashSet.add(IoEventType.SESSION_IDLE);
        }
        if (this.profileSessionClosed) {
            hashSet.add(IoEventType.SESSION_CLOSED);
        }
        return hashSet;
    }

    public void setEventsToProfile(IoEventType... ioEventTypeArr) {
        setProfilers(ioEventTypeArr);
    }

    public void messageReceived(IoFilter.NextFilter nextFilter, IoSession ioSession, Object obj) throws Exception {
        if (this.profileMessageReceived) {
            long timeNow = timeNow();
            nextFilter.messageReceived(ioSession, obj);
            this.messageReceivedTimerWorker.addNewDuration(timeNow() - timeNow);
            return;
        }
        nextFilter.messageReceived(ioSession, obj);
    }

    public void messageSent(IoFilter.NextFilter nextFilter, IoSession ioSession, WriteRequest writeRequest) throws Exception {
        if (this.profileMessageSent) {
            long timeNow = timeNow();
            nextFilter.messageSent(ioSession, writeRequest);
            this.messageSentTimerWorker.addNewDuration(timeNow() - timeNow);
            return;
        }
        nextFilter.messageSent(ioSession, writeRequest);
    }

    public void sessionCreated(IoFilter.NextFilter nextFilter, IoSession ioSession) throws Exception {
        if (this.profileSessionCreated) {
            long timeNow = timeNow();
            nextFilter.sessionCreated(ioSession);
            this.sessionCreatedTimerWorker.addNewDuration(timeNow() - timeNow);
            return;
        }
        nextFilter.sessionCreated(ioSession);
    }

    public void sessionOpened(IoFilter.NextFilter nextFilter, IoSession ioSession) throws Exception {
        if (this.profileSessionOpened) {
            long timeNow = timeNow();
            nextFilter.sessionOpened(ioSession);
            this.sessionOpenedTimerWorker.addNewDuration(timeNow() - timeNow);
            return;
        }
        nextFilter.sessionOpened(ioSession);
    }

    public void sessionIdle(IoFilter.NextFilter nextFilter, IoSession ioSession, IdleStatus idleStatus) throws Exception {
        if (this.profileSessionIdle) {
            long timeNow = timeNow();
            nextFilter.sessionIdle(ioSession, idleStatus);
            this.sessionIdleTimerWorker.addNewDuration(timeNow() - timeNow);
            return;
        }
        nextFilter.sessionIdle(ioSession, idleStatus);
    }

    public void sessionClosed(IoFilter.NextFilter nextFilter, IoSession ioSession) throws Exception {
        if (this.profileSessionClosed) {
            long timeNow = timeNow();
            nextFilter.sessionClosed(ioSession);
            this.sessionClosedTimerWorker.addNewDuration(timeNow() - timeNow);
            return;
        }
        nextFilter.sessionClosed(ioSession);
    }

    public double getAverageTime(IoEventType ioEventType) {
        switch (ioEventType) {
            case MESSAGE_RECEIVED:
                if (this.profileMessageReceived) {
                    return this.messageReceivedTimerWorker.getAverage();
                }
                break;
            case MESSAGE_SENT:
                if (this.profileMessageSent) {
                    return this.messageSentTimerWorker.getAverage();
                }
                break;
            case SESSION_CREATED:
                if (this.profileSessionCreated) {
                    return this.sessionCreatedTimerWorker.getAverage();
                }
                break;
            case SESSION_OPENED:
                if (this.profileSessionOpened) {
                    return this.sessionOpenedTimerWorker.getAverage();
                }
                break;
            case SESSION_IDLE:
                if (this.profileSessionIdle) {
                    return this.sessionIdleTimerWorker.getAverage();
                }
                break;
            case SESSION_CLOSED:
                if (this.profileSessionClosed) {
                    return this.sessionClosedTimerWorker.getAverage();
                }
                break;
        }
        throw new IllegalArgumentException("You are not monitoring this event.  Please add this event first.");
    }

    public long getTotalCalls(IoEventType ioEventType) {
        switch (ioEventType) {
            case MESSAGE_RECEIVED:
                if (this.profileMessageReceived) {
                    return this.messageReceivedTimerWorker.getCallsNumber();
                }
                break;
            case MESSAGE_SENT:
                if (this.profileMessageSent) {
                    return this.messageSentTimerWorker.getCallsNumber();
                }
                break;
            case SESSION_CREATED:
                if (this.profileSessionCreated) {
                    return this.sessionCreatedTimerWorker.getCallsNumber();
                }
                break;
            case SESSION_OPENED:
                if (this.profileSessionOpened) {
                    return this.sessionOpenedTimerWorker.getCallsNumber();
                }
                break;
            case SESSION_IDLE:
                if (this.profileSessionIdle) {
                    return this.sessionIdleTimerWorker.getCallsNumber();
                }
                break;
            case SESSION_CLOSED:
                if (this.profileSessionClosed) {
                    return this.sessionClosedTimerWorker.getCallsNumber();
                }
                break;
        }
        throw new IllegalArgumentException("You are not monitoring this event.  Please add this event first.");
    }

    public long getTotalTime(IoEventType ioEventType) {
        switch (ioEventType) {
            case MESSAGE_RECEIVED:
                if (this.profileMessageReceived) {
                    return this.messageReceivedTimerWorker.getTotal();
                }
                break;
            case MESSAGE_SENT:
                if (this.profileMessageSent) {
                    return this.messageSentTimerWorker.getTotal();
                }
                break;
            case SESSION_CREATED:
                if (this.profileSessionCreated) {
                    return this.sessionCreatedTimerWorker.getTotal();
                }
                break;
            case SESSION_OPENED:
                if (this.profileSessionOpened) {
                    return this.sessionOpenedTimerWorker.getTotal();
                }
                break;
            case SESSION_IDLE:
                if (this.profileSessionIdle) {
                    return this.sessionIdleTimerWorker.getTotal();
                }
                break;
            case SESSION_CLOSED:
                if (this.profileSessionClosed) {
                    return this.sessionClosedTimerWorker.getTotal();
                }
                break;
        }
        throw new IllegalArgumentException("You are not monitoring this event.  Please add this event first.");
    }

    public long getMinimumTime(IoEventType ioEventType) {
        switch (ioEventType) {
            case MESSAGE_RECEIVED:
                if (this.profileMessageReceived) {
                    return this.messageReceivedTimerWorker.getMinimum();
                }
                break;
            case MESSAGE_SENT:
                if (this.profileMessageSent) {
                    return this.messageSentTimerWorker.getMinimum();
                }
                break;
            case SESSION_CREATED:
                if (this.profileSessionCreated) {
                    return this.sessionCreatedTimerWorker.getMinimum();
                }
                break;
            case SESSION_OPENED:
                if (this.profileSessionOpened) {
                    return this.sessionOpenedTimerWorker.getMinimum();
                }
                break;
            case SESSION_IDLE:
                if (this.profileSessionIdle) {
                    return this.sessionIdleTimerWorker.getMinimum();
                }
                break;
            case SESSION_CLOSED:
                if (this.profileSessionClosed) {
                    return this.sessionClosedTimerWorker.getMinimum();
                }
                break;
        }
        throw new IllegalArgumentException("You are not monitoring this event.  Please add this event first.");
    }

    public long getMaximumTime(IoEventType ioEventType) {
        switch (ioEventType) {
            case MESSAGE_RECEIVED:
                if (this.profileMessageReceived) {
                    return this.messageReceivedTimerWorker.getMaximum();
                }
                break;
            case MESSAGE_SENT:
                if (this.profileMessageSent) {
                    return this.messageSentTimerWorker.getMaximum();
                }
                break;
            case SESSION_CREATED:
                if (this.profileSessionCreated) {
                    return this.sessionCreatedTimerWorker.getMaximum();
                }
                break;
            case SESSION_OPENED:
                if (this.profileSessionOpened) {
                    return this.sessionOpenedTimerWorker.getMaximum();
                }
                break;
            case SESSION_IDLE:
                if (this.profileSessionIdle) {
                    return this.sessionIdleTimerWorker.getMaximum();
                }
                break;
            case SESSION_CLOSED:
                if (this.profileSessionClosed) {
                    return this.sessionClosedTimerWorker.getMaximum();
                }
                break;
        }
        throw new IllegalArgumentException("You are not monitoring this event.  Please add this event first.");
    }

    private class TimerWorker {
        private final AtomicLong callsNumber = new AtomicLong();
        private final Object lock = new Object();
        private final AtomicLong maximum = new AtomicLong();
        private final AtomicLong minimum = new AtomicLong();
        private final AtomicLong total = new AtomicLong();

        public TimerWorker() {
        }

        public void addNewDuration(long j) {
            this.callsNumber.incrementAndGet();
            this.total.addAndGet(j);
            synchronized (this.lock) {
                if (j < this.minimum.longValue()) {
                    this.minimum.set(j);
                }
                if (j > this.maximum.longValue()) {
                    this.maximum.set(j);
                }
            }
        }

        public double getAverage() {
            double longValue;
            synchronized (this.lock) {
                longValue = (double) (this.total.longValue() / this.callsNumber.longValue());
            }
            return longValue;
        }

        public long getCallsNumber() {
            return this.callsNumber.longValue();
        }

        public long getTotal() {
            return this.total.longValue();
        }

        public long getMinimum() {
            return this.minimum.longValue();
        }

        public long getMaximum() {
            return this.maximum.longValue();
        }
    }

    /* renamed from: org.apache.mina.filter.statistic.ProfilerTimerFilter$1  reason: invalid class name */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$java$util$concurrent$TimeUnit = new int[TimeUnit.values().length];

        static {
            try {
                $SwitchMap$java$util$concurrent$TimeUnit[TimeUnit.SECONDS.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                $SwitchMap$java$util$concurrent$TimeUnit[TimeUnit.MICROSECONDS.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                $SwitchMap$java$util$concurrent$TimeUnit[TimeUnit.NANOSECONDS.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            $SwitchMap$org$apache$mina$core$session$IoEventType = new int[IoEventType.values().length];
            try {
                $SwitchMap$org$apache$mina$core$session$IoEventType[IoEventType.MESSAGE_RECEIVED.ordinal()] = 1;
            } catch (NoSuchFieldError e4) {
            }
            try {
                $SwitchMap$org$apache$mina$core$session$IoEventType[IoEventType.MESSAGE_SENT.ordinal()] = 2;
            } catch (NoSuchFieldError e5) {
            }
            try {
                $SwitchMap$org$apache$mina$core$session$IoEventType[IoEventType.SESSION_CREATED.ordinal()] = 3;
            } catch (NoSuchFieldError e6) {
            }
            try {
                $SwitchMap$org$apache$mina$core$session$IoEventType[IoEventType.SESSION_OPENED.ordinal()] = 4;
            } catch (NoSuchFieldError e7) {
            }
            try {
                $SwitchMap$org$apache$mina$core$session$IoEventType[IoEventType.SESSION_IDLE.ordinal()] = 5;
            } catch (NoSuchFieldError e8) {
            }
            try {
                $SwitchMap$org$apache$mina$core$session$IoEventType[IoEventType.SESSION_CLOSED.ordinal()] = 6;
            } catch (NoSuchFieldError e9) {
            }
        }
    }

    private long timeNow() {
        switch (AnonymousClass1.$SwitchMap$java$util$concurrent$TimeUnit[this.timeUnit.ordinal()]) {
            case 1:
                return System.currentTimeMillis() / 1000;
            case 2:
                return System.nanoTime() / 1000;
            case 3:
                return System.nanoTime();
            default:
                return System.currentTimeMillis();
        }
    }
}
