package org.apache.mina.filter.executor;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import org.a.b;
import org.a.c;
import org.apache.mina.core.session.AttributeKey;
import org.apache.mina.core.session.DummySession;
import org.apache.mina.core.session.IoEvent;
import org.apache.mina.core.session.IoSession;

public class OrderedThreadPoolExecutor extends ThreadPoolExecutor {
    private static final int DEFAULT_INITIAL_THREAD_POOL_SIZE = 0;
    private static final int DEFAULT_KEEP_ALIVE = 30;
    private static final int DEFAULT_MAX_THREAD_POOL = 16;
    /* access modifiers changed from: private */
    public static final IoSession EXIT_SIGNAL = new DummySession();
    static b LOGGER = c.a(OrderedThreadPoolExecutor.class);
    private final AttributeKey TASKS_QUEUE;
    private long completedTaskCount;
    /* access modifiers changed from: private */
    public final IoEventQueueHandler eventQueueHandler;
    /* access modifiers changed from: private */
    public final AtomicInteger idleWorkers;
    private volatile int largestPoolSize;
    private volatile boolean shutdown;
    /* access modifiers changed from: private */
    public final BlockingQueue<IoSession> waitingSessions;
    /* access modifiers changed from: private */
    public final Set<Worker> workers;

    static /* synthetic */ long access$914(OrderedThreadPoolExecutor orderedThreadPoolExecutor, long j) {
        long j2 = orderedThreadPoolExecutor.completedTaskCount + j;
        orderedThreadPoolExecutor.completedTaskCount = j2;
        return j2;
    }

    public OrderedThreadPoolExecutor() {
        this(0, 16, 30, TimeUnit.SECONDS, Executors.defaultThreadFactory(), null);
    }

    public OrderedThreadPoolExecutor(int i) {
        this(0, i, 30, TimeUnit.SECONDS, Executors.defaultThreadFactory(), null);
    }

    public OrderedThreadPoolExecutor(int i, int i2) {
        this(i, i2, 30, TimeUnit.SECONDS, Executors.defaultThreadFactory(), null);
    }

    public OrderedThreadPoolExecutor(int i, int i2, long j, TimeUnit timeUnit) {
        this(i, i2, j, timeUnit, Executors.defaultThreadFactory(), null);
    }

    public OrderedThreadPoolExecutor(int i, int i2, long j, TimeUnit timeUnit, IoEventQueueHandler ioEventQueueHandler) {
        this(i, i2, j, timeUnit, Executors.defaultThreadFactory(), ioEventQueueHandler);
    }

    public OrderedThreadPoolExecutor(int i, int i2, long j, TimeUnit timeUnit, ThreadFactory threadFactory) {
        this(i, i2, j, timeUnit, threadFactory, null);
    }

    public OrderedThreadPoolExecutor(int i, int i2, long j, TimeUnit timeUnit, ThreadFactory threadFactory, IoEventQueueHandler ioEventQueueHandler) {
        super(0, 1, j, timeUnit, new SynchronousQueue(), threadFactory, new ThreadPoolExecutor.AbortPolicy());
        this.TASKS_QUEUE = new AttributeKey(getClass(), "tasksQueue");
        this.waitingSessions = new LinkedBlockingQueue();
        this.workers = new HashSet();
        this.idleWorkers = new AtomicInteger();
        if (i < 0) {
            throw new IllegalArgumentException("corePoolSize: " + i);
        } else if (i2 == 0 || i2 < i) {
            throw new IllegalArgumentException("maximumPoolSize: " + i2);
        } else {
            super.setCorePoolSize(i);
            super.setMaximumPoolSize(i2);
            if (ioEventQueueHandler == null) {
                this.eventQueueHandler = IoEventQueueHandler.NOOP;
            } else {
                this.eventQueueHandler = ioEventQueueHandler;
            }
        }
    }

    /* access modifiers changed from: private */
    public SessionTasksQueue getSessionTasksQueue(IoSession ioSession) {
        SessionTasksQueue sessionTasksQueue = (SessionTasksQueue) ioSession.getAttribute(this.TASKS_QUEUE);
        if (sessionTasksQueue != null) {
            return sessionTasksQueue;
        }
        SessionTasksQueue sessionTasksQueue2 = new SessionTasksQueue();
        SessionTasksQueue sessionTasksQueue3 = (SessionTasksQueue) ioSession.setAttributeIfAbsent(this.TASKS_QUEUE, sessionTasksQueue2);
        return sessionTasksQueue3 != null ? sessionTasksQueue3 : sessionTasksQueue2;
    }

    public IoEventQueueHandler getQueueHandler() {
        return this.eventQueueHandler;
    }

    public void setRejectedExecutionHandler(RejectedExecutionHandler rejectedExecutionHandler) {
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void addWorker() {
        /*
            r4 = this;
            java.util.Set<org.apache.mina.filter.executor.OrderedThreadPoolExecutor$Worker> r1 = r4.workers
            monitor-enter(r1)
            java.util.Set<org.apache.mina.filter.executor.OrderedThreadPoolExecutor$Worker> r0 = r4.workers     // Catch:{ all -> 0x0040 }
            int r0 = r0.size()     // Catch:{ all -> 0x0040 }
            int r2 = super.getMaximumPoolSize()     // Catch:{ all -> 0x0040 }
            if (r0 < r2) goto L_0x0011
            monitor-exit(r1)     // Catch:{ all -> 0x0040 }
        L_0x0010:
            return
        L_0x0011:
            org.apache.mina.filter.executor.OrderedThreadPoolExecutor$Worker r0 = new org.apache.mina.filter.executor.OrderedThreadPoolExecutor$Worker     // Catch:{ all -> 0x0040 }
            r2 = 0
            r0.<init>()     // Catch:{ all -> 0x0040 }
            java.util.concurrent.ThreadFactory r2 = r4.getThreadFactory()     // Catch:{ all -> 0x0040 }
            java.lang.Thread r2 = r2.newThread(r0)     // Catch:{ all -> 0x0040 }
            java.util.concurrent.atomic.AtomicInteger r3 = r4.idleWorkers     // Catch:{ all -> 0x0040 }
            r3.incrementAndGet()     // Catch:{ all -> 0x0040 }
            r2.start()     // Catch:{ all -> 0x0040 }
            java.util.Set<org.apache.mina.filter.executor.OrderedThreadPoolExecutor$Worker> r2 = r4.workers     // Catch:{ all -> 0x0040 }
            r2.add(r0)     // Catch:{ all -> 0x0040 }
            java.util.Set<org.apache.mina.filter.executor.OrderedThreadPoolExecutor$Worker> r0 = r4.workers     // Catch:{ all -> 0x0040 }
            int r0 = r0.size()     // Catch:{ all -> 0x0040 }
            int r2 = r4.largestPoolSize     // Catch:{ all -> 0x0040 }
            if (r0 <= r2) goto L_0x003e
            java.util.Set<org.apache.mina.filter.executor.OrderedThreadPoolExecutor$Worker> r0 = r4.workers     // Catch:{ all -> 0x0040 }
            int r0 = r0.size()     // Catch:{ all -> 0x0040 }
            r4.largestPoolSize = r0     // Catch:{ all -> 0x0040 }
        L_0x003e:
            monitor-exit(r1)     // Catch:{ all -> 0x0040 }
            goto L_0x0010
        L_0x0040:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0040 }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.mina.filter.executor.OrderedThreadPoolExecutor.addWorker():void");
    }

    private void addWorkerIfNecessary() {
        if (this.idleWorkers.get() == 0) {
            synchronized (this.workers) {
                if (this.workers.isEmpty() || this.idleWorkers.get() == 0) {
                    addWorker();
                }
            }
        }
    }

    private void removeWorker() {
        synchronized (this.workers) {
            if (this.workers.size() > super.getCorePoolSize()) {
                this.waitingSessions.offer(EXIT_SIGNAL);
            }
        }
    }

    public int getMaximumPoolSize() {
        return super.getMaximumPoolSize();
    }

    public void setMaximumPoolSize(int i) {
        if (i <= 0 || i < super.getCorePoolSize()) {
            throw new IllegalArgumentException("maximumPoolSize: " + i);
        }
        synchronized (this.workers) {
            super.setMaximumPoolSize(i);
            for (int size = this.workers.size() - i; size > 0; size--) {
                removeWorker();
            }
        }
    }

    public boolean awaitTermination(long j, TimeUnit timeUnit) throws InterruptedException {
        long currentTimeMillis = System.currentTimeMillis() + timeUnit.toMillis(j);
        synchronized (this.workers) {
            while (!isTerminated()) {
                long currentTimeMillis2 = currentTimeMillis - System.currentTimeMillis();
                if (currentTimeMillis2 <= 0) {
                    break;
                }
                this.workers.wait(currentTimeMillis2);
            }
        }
        return isTerminated();
    }

    public boolean isShutdown() {
        return this.shutdown;
    }

    public boolean isTerminated() {
        boolean isEmpty;
        if (!this.shutdown) {
            return false;
        }
        synchronized (this.workers) {
            isEmpty = this.workers.isEmpty();
        }
        return isEmpty;
    }

    public void shutdown() {
        if (!this.shutdown) {
            this.shutdown = true;
            synchronized (this.workers) {
                for (int size = this.workers.size(); size > 0; size--) {
                    this.waitingSessions.offer(EXIT_SIGNAL);
                }
            }
        }
    }

    public List<Runnable> shutdownNow() {
        shutdown();
        ArrayList arrayList = new ArrayList();
        while (true) {
            IoSession poll = this.waitingSessions.poll();
            if (poll == null) {
                return arrayList;
            }
            if (poll == EXIT_SIGNAL) {
                this.waitingSessions.offer(EXIT_SIGNAL);
                Thread.yield();
            } else {
                SessionTasksQueue sessionTasksQueue = (SessionTasksQueue) poll.getAttribute(this.TASKS_QUEUE);
                synchronized (sessionTasksQueue.tasksQueue) {
                    for (Runnable runnable : sessionTasksQueue.tasksQueue) {
                        getQueueHandler().polled(this, (IoEvent) runnable);
                        arrayList.add(runnable);
                    }
                    sessionTasksQueue.tasksQueue.clear();
                }
            }
        }
    }

    private void print(Queue<Runnable> queue, IoEvent ioEvent) {
        StringBuilder sb = new StringBuilder();
        sb.append("Adding event ").append(ioEvent.getType()).append(" to session ").append(ioEvent.getSession().getId());
        sb.append("\nQueue : [");
        boolean z = true;
        for (Runnable next : queue) {
            if (z) {
                z = false;
            } else {
                sb.append(", ");
            }
            sb.append(((IoEvent) next).getType()).append(", ");
        }
        sb.append("]\n");
        LOGGER.b(sb.toString());
    }

    public void execute(Runnable runnable) {
        boolean z = false;
        if (this.shutdown) {
            rejectTask(runnable);
        }
        checkTaskType(runnable);
        IoEvent ioEvent = (IoEvent) runnable;
        IoSession session = ioEvent.getSession();
        SessionTasksQueue sessionTasksQueue = getSessionTasksQueue(session);
        Queue access$200 = sessionTasksQueue.tasksQueue;
        boolean accept = this.eventQueueHandler.accept(this, ioEvent);
        if (accept) {
            synchronized (access$200) {
                access$200.offer(ioEvent);
                if (sessionTasksQueue.processingCompleted) {
                    boolean unused = sessionTasksQueue.processingCompleted = false;
                    z = true;
                }
                if (LOGGER.a()) {
                    print(access$200, ioEvent);
                }
            }
        }
        if (z) {
            this.waitingSessions.offer(session);
        }
        addWorkerIfNecessary();
        if (accept) {
            this.eventQueueHandler.offered(this, ioEvent);
        }
    }

    private void rejectTask(Runnable runnable) {
        getRejectedExecutionHandler().rejectedExecution(runnable, this);
    }

    private void checkTaskType(Runnable runnable) {
        if (!(runnable instanceof IoEvent)) {
            throw new IllegalArgumentException("task must be an IoEvent or its subclass.");
        }
    }

    public int getActiveCount() {
        int size;
        synchronized (this.workers) {
            size = this.workers.size() - this.idleWorkers.get();
        }
        return size;
    }

    public long getCompletedTaskCount() {
        long j;
        synchronized (this.workers) {
            long j2 = this.completedTaskCount;
            j = j2;
            for (Worker access$400 : this.workers) {
                j = access$400.completedTaskCount + j;
            }
        }
        return j;
    }

    public int getLargestPoolSize() {
        return this.largestPoolSize;
    }

    public int getPoolSize() {
        int size;
        synchronized (this.workers) {
            size = this.workers.size();
        }
        return size;
    }

    public long getTaskCount() {
        return getCompletedTaskCount();
    }

    public boolean isTerminating() {
        boolean z;
        synchronized (this.workers) {
            z = isShutdown() && !isTerminated();
        }
        return z;
    }

    public int prestartAllCoreThreads() {
        int i = 0;
        synchronized (this.workers) {
            for (int corePoolSize = super.getCorePoolSize() - this.workers.size(); corePoolSize > 0; corePoolSize--) {
                addWorker();
                i++;
            }
        }
        return i;
    }

    public boolean prestartCoreThread() {
        boolean z;
        synchronized (this.workers) {
            if (this.workers.size() < super.getCorePoolSize()) {
                addWorker();
                z = true;
            } else {
                z = false;
            }
        }
        return z;
    }

    public BlockingQueue<Runnable> getQueue() {
        throw new UnsupportedOperationException();
    }

    public void purge() {
    }

    public boolean remove(Runnable runnable) {
        boolean remove;
        checkTaskType(runnable);
        IoEvent ioEvent = (IoEvent) runnable;
        SessionTasksQueue sessionTasksQueue = (SessionTasksQueue) ioEvent.getSession().getAttribute(this.TASKS_QUEUE);
        Queue access$200 = sessionTasksQueue.tasksQueue;
        if (sessionTasksQueue == null) {
            return false;
        }
        synchronized (access$200) {
            remove = access$200.remove(runnable);
        }
        if (remove) {
            getQueueHandler().polled(this, ioEvent);
        }
        return remove;
    }

    public int getCorePoolSize() {
        return super.getCorePoolSize();
    }

    public void setCorePoolSize(int i) {
        if (i < 0) {
            throw new IllegalArgumentException("corePoolSize: " + i);
        } else if (i > super.getMaximumPoolSize()) {
            throw new IllegalArgumentException("corePoolSize exceeds maximumPoolSize");
        } else {
            synchronized (this.workers) {
                if (super.getCorePoolSize() > i) {
                    for (int corePoolSize = super.getCorePoolSize() - i; corePoolSize > 0; corePoolSize--) {
                        removeWorker();
                    }
                }
                super.setCorePoolSize(i);
            }
        }
    }

    private class Worker implements Runnable {
        /* access modifiers changed from: private */
        public volatile long completedTaskCount;
        private Thread thread;

        private Worker() {
        }

        public void run() {
            this.thread = Thread.currentThread();
            while (true) {
                try {
                    IoSession fetchSession = fetchSession();
                    OrderedThreadPoolExecutor.this.idleWorkers.decrementAndGet();
                    if (fetchSession == null) {
                        synchronized (OrderedThreadPoolExecutor.this.workers) {
                            if (OrderedThreadPoolExecutor.this.workers.size() > OrderedThreadPoolExecutor.this.getCorePoolSize()) {
                                OrderedThreadPoolExecutor.this.workers.remove(this);
                                break;
                            }
                        }
                    }
                    if (fetchSession == OrderedThreadPoolExecutor.EXIT_SIGNAL) {
                        break;
                    }
                    if (fetchSession != null) {
                        runTasks(OrderedThreadPoolExecutor.this.getSessionTasksQueue(fetchSession));
                    }
                    OrderedThreadPoolExecutor.this.idleWorkers.incrementAndGet();
                } catch (Throwable th) {
                    synchronized (OrderedThreadPoolExecutor.this.workers) {
                        OrderedThreadPoolExecutor.this.workers.remove(this);
                        OrderedThreadPoolExecutor.access$914(OrderedThreadPoolExecutor.this, this.completedTaskCount);
                        OrderedThreadPoolExecutor.this.workers.notifyAll();
                        throw th;
                    }
                }
            }
            synchronized (OrderedThreadPoolExecutor.this.workers) {
                OrderedThreadPoolExecutor.this.workers.remove(this);
                OrderedThreadPoolExecutor.access$914(OrderedThreadPoolExecutor.this, this.completedTaskCount);
                OrderedThreadPoolExecutor.this.workers.notifyAll();
            }
        }

        private IoSession fetchSession() {
            long j;
            IoSession ioSession = null;
            long currentTimeMillis = System.currentTimeMillis();
            long keepAliveTime = currentTimeMillis + OrderedThreadPoolExecutor.this.getKeepAliveTime(TimeUnit.MILLISECONDS);
            while (true) {
                long j2 = keepAliveTime - currentTimeMillis;
                if (j2 <= 0) {
                    return ioSession;
                }
                try {
                    IoSession ioSession2 = (IoSession) OrderedThreadPoolExecutor.this.waitingSessions.poll(j2, TimeUnit.MILLISECONDS);
                    if (ioSession2 != null) {
                        return ioSession2;
                    }
                    try {
                        System.currentTimeMillis();
                        return ioSession2;
                    } catch (InterruptedException e) {
                        ioSession = ioSession2;
                    }
                } catch (InterruptedException e2) {
                    currentTimeMillis = j;
                } catch (Throwable th) {
                    Throwable th2 = th;
                    if (ioSession == null) {
                        try {
                            j = System.currentTimeMillis();
                        } catch (InterruptedException e3) {
                        }
                    } else {
                        j = currentTimeMillis;
                    }
                    throw th2;
                }
            }
        }

        private void runTasks(SessionTasksQueue sessionTasksQueue) {
            Runnable runnable;
            while (true) {
                Queue access$200 = sessionTasksQueue.tasksQueue;
                synchronized (access$200) {
                    runnable = (Runnable) access$200.poll();
                    if (runnable == null) {
                        boolean unused = sessionTasksQueue.processingCompleted = true;
                        return;
                    }
                }
                OrderedThreadPoolExecutor.this.eventQueueHandler.polled(OrderedThreadPoolExecutor.this, (IoEvent) runnable);
                runTask(runnable);
            }
            while (true) {
            }
        }

        private void runTask(Runnable runnable) {
            OrderedThreadPoolExecutor.this.beforeExecute(this.thread, runnable);
            boolean z = false;
            try {
                runnable.run();
                z = true;
                OrderedThreadPoolExecutor.this.afterExecute(runnable, null);
                this.completedTaskCount++;
            } catch (RuntimeException e) {
                if (!z) {
                    OrderedThreadPoolExecutor.this.afterExecute(runnable, e);
                }
                throw e;
            }
        }
    }

    private class SessionTasksQueue {
        /* access modifiers changed from: private */
        public boolean processingCompleted;
        /* access modifiers changed from: private */
        public final Queue<Runnable> tasksQueue;

        private SessionTasksQueue() {
            this.tasksQueue = new ConcurrentLinkedQueue();
            this.processingCompleted = true;
        }
    }
}
