package org.apache.mina.proxy.handlers.http;

import java.util.List;
import java.util.Map;

public class HttpProxyResponse {
    public String body;
    public final Map<String, List<String>> headers;
    public final String httpVersion;
    public final int statusCode;
    public final String statusLine;

    protected HttpProxyResponse(String str, String str2, Map<String, List<String>> map) {
        this.httpVersion = str;
        this.statusLine = str2;
        this.statusCode = str2.charAt(0) == ' ' ? Integer.parseInt(str2.substring(1, 4)) : Integer.parseInt(str2.substring(0, 3));
        this.headers = map;
    }

    public final String getHttpVersion() {
        return this.httpVersion;
    }

    public final int getStatusCode() {
        return this.statusCode;
    }

    public final String getStatusLine() {
        return this.statusLine;
    }

    public String getBody() {
        return this.body;
    }

    public void setBody(String str) {
        this.body = str;
    }

    public final Map<String, List<String>> getHeaders() {
        return this.headers;
    }
}
