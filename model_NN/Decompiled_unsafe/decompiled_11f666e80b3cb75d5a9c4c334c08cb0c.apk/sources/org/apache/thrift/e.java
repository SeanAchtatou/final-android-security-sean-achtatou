package org.apache.thrift;

import org.apache.thrift.protocol.a;
import org.apache.thrift.protocol.f;
import org.apache.thrift.protocol.h;
import org.apache.thrift.transport.c;

public class e {

    /* renamed from: a  reason: collision with root package name */
    private final f f3018a;
    private final c b;

    public e() {
        this(new a.C0075a());
    }

    public e(h hVar) {
        this.b = new c();
        this.f3018a = hVar.a(this.b);
    }

    public void a(b bVar, byte[] bArr) {
        try {
            this.b.a(bArr);
            bVar.a(this.f3018a);
        } finally {
            this.f3018a.y();
        }
    }
}
