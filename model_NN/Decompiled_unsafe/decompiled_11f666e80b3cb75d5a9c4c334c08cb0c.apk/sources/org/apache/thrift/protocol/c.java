package org.apache.thrift.protocol;

public class c {

    /* renamed from: a  reason: collision with root package name */
    public final String f3028a;
    public final byte b;
    public final short c;

    public c() {
        this("", (byte) 0, 0);
    }

    public c(String str, byte b2, short s) {
        this.f3028a = str;
        this.b = b2;
        this.c = s;
    }
}
