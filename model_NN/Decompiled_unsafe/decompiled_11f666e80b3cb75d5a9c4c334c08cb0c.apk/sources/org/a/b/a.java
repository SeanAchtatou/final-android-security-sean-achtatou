package org.a.b;

import android.util.Log;
import org.a.a.b;
import org.a.a.c;

/* compiled from: AndroidLogger */
public class a extends b {
    a(String str) {
        this.b = str;
    }

    public void a(String str) {
        Log.v(this.b, str);
    }

    public void a(String str, Object obj) {
        Log.v(this.b, c(str, obj, null));
    }

    public void a(String str, Throwable th) {
        Log.v(this.b, str, th);
    }

    public boolean a() {
        return Log.isLoggable(this.b, 3);
    }

    public void b(String str) {
        Log.d(this.b, str);
    }

    public void b(String str, Object obj) {
        Log.d(this.b, c(str, obj, null));
    }

    public void a(String str, Object obj, Object obj2) {
        Log.d(this.b, c(str, obj, obj2));
    }

    public void b(String str, Throwable th) {
        Log.d(this.b, str, th);
    }

    public void c(String str) {
        Log.i(this.b, str);
    }

    public void c(String str, Object obj) {
        Log.i(this.b, c(str, obj, null));
    }

    public void c(String str, Throwable th) {
        Log.i(this.b, str, th);
    }

    public boolean b() {
        return Log.isLoggable(this.b, 5);
    }

    public void d(String str) {
        Log.w(this.b, str);
    }

    public void d(String str, Object obj) {
        Log.w(this.b, c(str, obj, null));
    }

    public void b(String str, Object obj, Object obj2) {
        Log.w(this.b, c(str, obj, obj2));
    }

    public void d(String str, Throwable th) {
        Log.w(this.b, str, th);
    }

    public void e(String str) {
        Log.e(this.b, str);
    }

    public void e(String str, Object obj) {
        Log.e(this.b, c(str, obj, null));
    }

    public void e(String str, Throwable th) {
        Log.e(this.b, str, th);
    }

    private String c(String str, Object obj, Object obj2) {
        return c.a(str, obj, obj2).a();
    }
}
