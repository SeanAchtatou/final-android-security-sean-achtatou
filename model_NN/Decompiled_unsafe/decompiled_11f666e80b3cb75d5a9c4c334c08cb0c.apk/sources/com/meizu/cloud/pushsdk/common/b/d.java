package com.meizu.cloud.pushsdk.common.b;

import android.text.TextUtils;
import com.meizu.cloud.pushsdk.common.b.e;

class d {

    /* renamed from: a  reason: collision with root package name */
    private static e.c<String> f1131a;

    public static synchronized e.c<String> a() {
        e.c<String> cVar;
        synchronized (d.class) {
            if (f1131a == null) {
                f1131a = new e.c<>();
            }
            if (!f1131a.f1133a || TextUtils.isEmpty((CharSequence) f1131a.b)) {
                f1131a = e.a("android.telephony.MzTelephonyManager").b("getDeviceId").a();
            }
            cVar = f1131a;
        }
        return cVar;
    }
}
