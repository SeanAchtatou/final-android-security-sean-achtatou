package com.meizu.cloud.pushsdk.common.base;

import com.meizu.cloud.pushsdk.common.b.c;
import java.util.concurrent.atomic.AtomicInteger;

public abstract class a<T> {

    /* renamed from: a  reason: collision with root package name */
    protected T f1137a;
    protected String b;
    private AtomicInteger c = new AtomicInteger(0);

    public a(String str) {
        this.b = str;
    }

    /* access modifiers changed from: protected */
    public abstract void a();

    public final void a(Object obj) {
        synchronized (this.c) {
            if (this.c.incrementAndGet() == 1) {
                c.b(this.b, "call onInit");
                this.f1137a = obj;
                a();
            }
        }
    }

    /* access modifiers changed from: protected */
    public abstract void b();

    public final void c() {
        synchronized (this.c) {
            if (this.c.decrementAndGet() == 0) {
                c.b(this.b, "call onDestroy");
                b();
            }
        }
    }
}
