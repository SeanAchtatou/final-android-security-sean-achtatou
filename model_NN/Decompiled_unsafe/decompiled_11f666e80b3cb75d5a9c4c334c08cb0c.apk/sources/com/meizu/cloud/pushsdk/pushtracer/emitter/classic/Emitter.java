package com.meizu.cloud.pushsdk.pushtracer.emitter.classic;

import com.meizu.cloud.pushsdk.a.d.i;
import com.meizu.cloud.pushsdk.pushtracer.dataload.DataLoad;
import com.meizu.cloud.pushsdk.pushtracer.emitter.Emitter;
import com.meizu.cloud.pushsdk.pushtracer.emitter.ReadyRequest;
import com.meizu.cloud.pushsdk.pushtracer.emitter.RequestResult;
import com.meizu.cloud.pushsdk.pushtracer.storage.EventStore;
import com.meizu.cloud.pushsdk.pushtracer.utils.Logger;
import com.meizu.cloud.pushsdk.pushtracer.utils.Util;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class Emitter extends com.meizu.cloud.pushsdk.pushtracer.emitter.Emitter {
    private final String TAG = Emitter.class.getSimpleName();
    private int emptyCount;
    /* access modifiers changed from: private */
    public EventStore eventStore = new EventStore(this.context, this.sendLimit);

    public Emitter(Emitter.EmitterBuilder emitterBuilder) {
        super(emitterBuilder);
    }

    /* access modifiers changed from: private */
    public void attemptEmit() {
        int size;
        int i;
        if (!Util.isOnline(this.context)) {
            Logger.e(this.TAG, "Emitter loop stopping: emitter offline.", new Object[0]);
            this.isRunning.compareAndSet(true, false);
        } else if (this.eventStore.getSize() > 0) {
            this.emptyCount = 0;
            LinkedList<RequestResult> performAsyncEmit = performAsyncEmit(buildRequests(this.eventStore.getEmittableEvents()));
            Logger.i(this.TAG, "Processing emitter results.", new Object[0]);
            LinkedList linkedList = new LinkedList();
            Iterator<RequestResult> it = performAsyncEmit.iterator();
            int i2 = 0;
            int i3 = 0;
            while (it.hasNext()) {
                RequestResult next = it.next();
                if (next.getSuccess()) {
                    Iterator<Long> it2 = next.getEventIds().iterator();
                    while (it2.hasNext()) {
                        linkedList.add(it2.next());
                    }
                    i = i3 + next.getEventIds().size();
                    size = i2;
                } else {
                    size = next.getEventIds().size() + i2;
                    Logger.e(this.TAG, "Request sending failed but we will retry later.", new Object[0]);
                    i = i3;
                }
                i2 = size;
                i3 = i;
            }
            performAsyncEventRemoval(linkedList);
            Logger.d(this.TAG, "Success Count: %s", Integer.valueOf(i3));
            Logger.d(this.TAG, "Failure Count: %s", Integer.valueOf(i2));
            if (this.requestCallback != null) {
                if (i2 != 0) {
                    this.requestCallback.onFailure(i3, i2);
                } else {
                    this.requestCallback.onSuccess(i3);
                }
            }
            if (i2 <= 0 || i3 != 0) {
                attemptEmit();
                return;
            }
            if (Util.isOnline(this.context)) {
                Logger.e(this.TAG, "Ensure collector path is valid: %s", getEmitterUri());
            }
            Logger.e(this.TAG, "Emitter loop stopping: failures.", new Object[0]);
            this.isRunning.compareAndSet(true, false);
        } else if (this.emptyCount >= this.emptyLimit) {
            Logger.e(this.TAG, "Emitter loop stopping: empty limit reached.", new Object[0]);
            this.isRunning.compareAndSet(true, false);
        } else {
            this.emptyCount++;
            Logger.e(this.TAG, "Emitter database empty: " + this.emptyCount, new Object[0]);
            try {
                this.timeUnit.sleep((long) this.emitterTick);
            } catch (InterruptedException e) {
                Logger.e(this.TAG, "Emitter thread sleep interrupted: " + e.toString(), new Object[0]);
            }
            attemptEmit();
        }
    }

    private Callable<Boolean> getRemoveCallable(final Long l) {
        return new Callable<Boolean>() {
            public Boolean call() {
                return Boolean.valueOf(Emitter.this.eventStore.removeEvent(l.longValue()));
            }
        };
    }

    private Callable<Integer> getRequestCallable(final i iVar) {
        return new Callable<Integer>() {
            public Integer call() {
                return Integer.valueOf(Emitter.this.requestSender(iVar));
            }
        };
    }

    private LinkedList<RequestResult> performAsyncEmit(LinkedList<ReadyRequest> linkedList) {
        LinkedList<RequestResult> linkedList2 = new LinkedList<>();
        LinkedList linkedList3 = new LinkedList();
        Iterator<ReadyRequest> it = linkedList.iterator();
        while (it.hasNext()) {
            linkedList3.add(Executor.futureCallable(getRequestCallable(it.next().getRequest())));
        }
        Logger.d(this.TAG, "Request Futures: %s", Integer.valueOf(linkedList3.size()));
        for (int i = 0; i < linkedList3.size(); i++) {
            int i2 = -1;
            try {
                i2 = ((Integer) ((Future) linkedList3.get(i)).get(5, TimeUnit.SECONDS)).intValue();
            } catch (InterruptedException e) {
                Logger.e(this.TAG, "Request Future was interrupted: %s", e.getMessage());
            } catch (ExecutionException e2) {
                Logger.e(this.TAG, "Request Future failed: %s", e2.getMessage());
            } catch (TimeoutException e3) {
                Logger.e(this.TAG, "Request Future had a timeout: %s", e3.getMessage());
            }
            if (linkedList.get(i).isOversize()) {
                linkedList2.add(new RequestResult(true, linkedList.get(i).getEventIds()));
            } else {
                linkedList2.add(new RequestResult(isSuccessfulSend(i2), linkedList.get(i).getEventIds()));
            }
        }
        return linkedList2;
    }

    private LinkedList<Boolean> performAsyncEventRemoval(LinkedList<Long> linkedList) {
        boolean z;
        LinkedList<Boolean> linkedList2 = new LinkedList<>();
        LinkedList linkedList3 = new LinkedList();
        Iterator<Long> it = linkedList.iterator();
        while (it.hasNext()) {
            linkedList3.add(Executor.futureCallable(getRemoveCallable(it.next())));
        }
        Logger.d(this.TAG, "Removal Futures: %s", Integer.valueOf(linkedList3.size()));
        for (int i = 0; i < linkedList3.size(); i++) {
            try {
                z = ((Boolean) ((Future) linkedList3.get(i)).get(5, TimeUnit.SECONDS)).booleanValue();
            } catch (InterruptedException e) {
                Logger.e(this.TAG, "Removal Future was interrupted: %s", e.getMessage());
                z = false;
            } catch (ExecutionException e2) {
                Logger.e(this.TAG, "Removal Future failed: %s", e2.getMessage());
                z = false;
            } catch (TimeoutException e3) {
                Logger.e(this.TAG, "Removal Future had a timeout: %s", e3.getMessage());
                z = false;
            }
            linkedList2.add(Boolean.valueOf(z));
        }
        return linkedList2;
    }

    public void add(DataLoad dataLoad) {
        this.eventStore.add(dataLoad);
        Logger.e(this.TAG, "isRunning " + this.isRunning, new Object[0]);
        if (this.isRunning.compareAndSet(false, true)) {
            attemptEmit();
        }
    }

    public void flush() {
        Executor.execute(new Runnable() {
            public void run() {
                if (Emitter.this.isRunning.compareAndSet(false, true)) {
                    Emitter.this.attemptEmit();
                }
            }
        });
    }

    public boolean getEmitterStatus() {
        return this.isRunning.get();
    }

    public EventStore getEventStore() {
        return this.eventStore;
    }

    public void shutdown() {
        Logger.d(this.TAG, "Shutting down emitter.", new Object[0]);
        this.isRunning.compareAndSet(true, false);
        Executor.shutdown();
    }
}
