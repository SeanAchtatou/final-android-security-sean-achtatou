package com.meizu.cloud.pushsdk.platform.message;

import cn.banshenggua.aichang.room.message.SocketMessage;
import org.json.JSONObject;

public class UnRegisterStatus extends BasicPushStatus {
    private boolean isUnRegisterSuccess;

    public UnRegisterStatus() {
    }

    public UnRegisterStatus(String str) {
        super(str);
    }

    public boolean isUnRegisterSuccess() {
        return this.isUnRegisterSuccess;
    }

    public void parseValueData(JSONObject jSONObject) {
        if (!jSONObject.isNull(SocketMessage.MSG_RESULE_KEY)) {
            setIsUnRegisterSuccess(jSONObject.getBoolean(SocketMessage.MSG_RESULE_KEY));
        }
    }

    public void setIsUnRegisterSuccess(boolean z) {
        this.isUnRegisterSuccess = z;
    }

    public String toString() {
        return super.toString() + " UnRegisterStatus{" + "isUnRegisterSuccess=" + this.isUnRegisterSuccess + '}';
    }
}
