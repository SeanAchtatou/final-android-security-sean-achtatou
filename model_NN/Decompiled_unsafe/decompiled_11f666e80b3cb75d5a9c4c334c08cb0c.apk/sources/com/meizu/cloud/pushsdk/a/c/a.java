package com.meizu.cloud.pushsdk.a.c;

import com.meizu.cloud.pushsdk.a.d.k;

public class a extends Exception {

    /* renamed from: a  reason: collision with root package name */
    private String f1079a;
    private int b = 0;
    private String c;
    private k d;

    public a() {
    }

    public a(k kVar) {
        this.d = kVar;
    }

    public a(Throwable th) {
        super(th);
    }

    public k a() {
        return this.d;
    }

    public void a(int i) {
        this.b = i;
    }

    public void a(String str) {
        this.c = str;
    }

    public int b() {
        return this.b;
    }

    public void b(String str) {
        this.f1079a = str;
    }

    public void c() {
        this.c = "requestCancelledError";
    }
}
