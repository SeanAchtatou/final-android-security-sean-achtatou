package com.meizu.cloud.pushsdk.notification;

import android.app.Notification;
import android.content.Context;
import android.graphics.Bitmap;
import android.widget.RemoteViews;
import com.meizu.cloud.pushinternal.R;
import com.meizu.cloud.pushsdk.handler.MessageV3;
import com.meizu.cloud.pushsdk.notification.model.styleenum.InnerStyleLayout;
import com.meizu.cloud.pushsdk.util.MinSdkChecker;

public class d extends f {
    public d(Context context, PushNotificationBuilder pushNotificationBuilder) {
        super(context, pushNotificationBuilder);
    }

    /* access modifiers changed from: protected */
    public void b(Notification notification, MessageV3 messageV3) {
        if (MinSdkChecker.isSupportNotificationBuild()) {
            Bitmap a2 = a(messageV3.getmNotificationStyle().getBannerImageUrl());
            if (!a() && a2 != null) {
                RemoteViews remoteViews = new RemoteViews(this.f1141a.getPackageName(), R.layout.push_pure_pic_notification);
                remoteViews.setImageViewBitmap(R.id.push_pure_bigview_banner, a2);
                remoteViews.setViewVisibility(R.id.push_pure_bigview_expanded, 8);
                remoteViews.setViewVisibility(R.id.push_pure_bigview_banner, 0);
                notification.contentView = remoteViews;
                if (messageV3.getmNotificationStyle().getInnerStyle() == InnerStyleLayout.EXPANDABLE_PIC.getCode()) {
                    Bitmap a3 = a(messageV3.getmNotificationStyle().getExpandableImageUrl());
                    if (!a() && a3 != null) {
                        RemoteViews remoteViews2 = new RemoteViews(this.f1141a.getPackageName(), R.layout.push_pure_pic_notification);
                        remoteViews2.setImageViewBitmap(R.id.push_pure_bigview_expanded, a3);
                        remoteViews2.setViewVisibility(R.id.push_pure_bigview_expanded, 0);
                        remoteViews2.setViewVisibility(R.id.push_pure_bigview_banner, 8);
                        notification.bigContentView = remoteViews2;
                    }
                }
            }
        }
    }
}
