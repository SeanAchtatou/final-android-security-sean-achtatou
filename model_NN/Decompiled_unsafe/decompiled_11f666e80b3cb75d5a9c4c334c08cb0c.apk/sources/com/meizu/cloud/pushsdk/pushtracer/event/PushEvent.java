package com.meizu.cloud.pushsdk.pushtracer.event;

import com.meizu.cloud.pushsdk.pushtracer.constant.Parameters;
import com.meizu.cloud.pushsdk.pushtracer.dataload.TrackerDataload;
import com.meizu.cloud.pushsdk.pushtracer.event.Event;

public class PushEvent extends Event {
    private String deviceId;
    private String eventName;
    private String messageSeq;
    private String packageName;
    private String pushsdkVersion;
    private String seqId;
    private String taskId;

    public abstract class Builder<T extends Builder<T>> extends Event.Builder<T> {
        /* access modifiers changed from: private */
        public String deviceId;
        /* access modifiers changed from: private */
        public String eventName;
        /* access modifiers changed from: private */
        public String messageSeq;
        /* access modifiers changed from: private */
        public String packageName;
        /* access modifiers changed from: private */
        public String pushsdkVersion;
        /* access modifiers changed from: private */
        public String seqId;
        /* access modifiers changed from: private */
        public String taskId;

        public PushEvent build() {
            return new PushEvent(this);
        }

        public T deviceId(String str) {
            this.deviceId = str;
            return (Builder) self();
        }

        public T eventName(String str) {
            this.eventName = str;
            return (Builder) self();
        }

        public T messageSeq(String str) {
            this.messageSeq = str;
            return (Builder) self();
        }

        public T packageName(String str) {
            this.packageName = str;
            return (Builder) self();
        }

        public T pushsdkVersion(String str) {
            this.pushsdkVersion = str;
            return (Builder) self();
        }

        public T seqId(String str) {
            this.seqId = str;
            return (Builder) self();
        }

        public T taskId(String str) {
            this.taskId = str;
            return (Builder) self();
        }
    }

    class Builder2 extends Builder<Builder2> {
        private Builder2() {
        }

        /* access modifiers changed from: protected */
        public Builder2 self() {
            return this;
        }
    }

    protected PushEvent(Builder<?> builder) {
        super(builder);
        this.taskId = ((Builder) builder).taskId;
        this.deviceId = ((Builder) builder).deviceId;
        this.eventName = ((Builder) builder).eventName;
        this.pushsdkVersion = builder.pushsdkVersion;
        this.packageName = builder.packageName;
        this.seqId = builder.seqId;
        this.messageSeq = builder.messageSeq;
    }

    public static Builder<?> builder() {
        return new Builder2();
    }

    public TrackerDataload getDataLoad() {
        TrackerDataload trackerDataload = new TrackerDataload();
        trackerDataload.add(Parameters.EVENT_NAME, this.eventName);
        trackerDataload.add("task_id", this.taskId);
        trackerDataload.add(Parameters.DEVICE_ID, this.deviceId);
        trackerDataload.add(Parameters.PUSH_SDK_VERSION, this.pushsdkVersion);
        trackerDataload.add(Parameters.PACKAGE_NAME, this.packageName);
        trackerDataload.add(Parameters.SEQ_ID, this.seqId);
        trackerDataload.add(Parameters.MESSAGE_SEQ, this.messageSeq);
        return putDefaultParams(trackerDataload);
    }
}
