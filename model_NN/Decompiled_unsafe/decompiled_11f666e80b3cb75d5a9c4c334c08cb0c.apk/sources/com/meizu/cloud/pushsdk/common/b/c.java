package com.meizu.cloud.pushsdk.common.b;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.Process;
import android.util.Log;
import com.meizu.cloud.pushsdk.common.b.g;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;

public class c {

    /* renamed from: a  reason: collision with root package name */
    private static Handler f1125a = new e(Looper.getMainLooper());
    /* access modifiers changed from: private */
    public static a b;
    /* access modifiers changed from: private */
    public static LinkedList<b> c = new LinkedList<>();
    private static a.C0029a d = a.C0029a.DEBUG;
    private static a.C0029a e = a.C0029a.DEBUG;
    private static C0030c f = new C0030c();

    public interface a {

        /* renamed from: com.meizu.cloud.pushsdk.common.b.c$a$a  reason: collision with other inner class name */
        public enum C0029a {
            DEBUG,
            INFO,
            WARN,
            ERROR,
            NULL
        }

        void a(C0029a aVar, String str, String str2);
    }

    class b {

        /* renamed from: a  reason: collision with root package name */
        static SimpleDateFormat f1127a = new SimpleDateFormat("MM-dd HH:mm:ss ");
        static String b = String.valueOf(Process.myPid());
        a.C0029a c;
        String d;
        String e;

        b(a.C0029a aVar, String str, String str2) {
            this.c = aVar;
            this.d = f1127a.format(new Date()) + b + "-" + String.valueOf(Thread.currentThread().getId()) + " " + str;
            this.e = str2;
        }
    }

    /* renamed from: com.meizu.cloud.pushsdk.common.b.c$c  reason: collision with other inner class name */
    public class C0030c {

        /* renamed from: a  reason: collision with root package name */
        int f1128a = 100;
        int b = 120000;
    }

    public enum d {
        CONSOLE,
        FILE
    }

    class e extends Handler {
        e(Looper looper) {
            super(looper);
        }

        public void handleMessage(Message message) {
            if (c.b != null) {
                g.a(new g.a() {
                    public void a() {
                        LinkedList linkedList;
                        synchronized (c.c) {
                            linkedList = new LinkedList(c.c);
                            c.c.clear();
                        }
                        Iterator it = linkedList.iterator();
                        while (it.hasNext()) {
                            b bVar = (b) it.next();
                            c.b.a(bVar.c, bVar.d, bVar.e);
                        }
                    }
                });
            }
        }
    }

    public static void a() {
        synchronized (c) {
            f1125a.removeMessages(1);
            f1125a.obtainMessage(1).sendToTarget();
        }
    }

    private static void a(a.C0029a aVar, String str, String str2) {
        if (b != null && e.ordinal() <= aVar.ordinal()) {
            synchronized (c) {
                c.addLast(new b(aVar, str, str2));
                if (c.size() >= f.f1128a || f.b <= 0) {
                    a();
                } else if (!f1125a.hasMessages(1)) {
                    f1125a.sendMessageDelayed(f1125a.obtainMessage(1), (long) f.b);
                }
            }
        }
    }

    public static void a(a aVar) {
        b = aVar;
    }

    public static void a(d dVar, a.C0029a aVar) {
        if (dVar == d.CONSOLE) {
            d = aVar;
        } else if (dVar == d.FILE) {
            e = aVar;
        }
    }

    public static void a(String str, String str2) {
        if (d.ordinal() <= a.C0029a.DEBUG.ordinal()) {
            Log.d(str, str2);
        }
        a(a.C0029a.DEBUG, str, str2);
    }

    public static void a(String str, Throwable th) {
        d(str, Log.getStackTraceString(th));
    }

    public static void b(String str, String str2) {
        if (d.ordinal() <= a.C0029a.INFO.ordinal()) {
            Log.i(str, str2);
        }
        a(a.C0029a.INFO, str, str2);
    }

    public static void c(String str, String str2) {
        if (d.ordinal() <= a.C0029a.WARN.ordinal()) {
            Log.w(str, str2);
        }
        a(a.C0029a.WARN, str, str2);
    }

    public static void d(String str, String str2) {
        if (d.ordinal() <= a.C0029a.ERROR.ordinal()) {
            Log.e(str, str2);
        }
        a(a.C0029a.ERROR, str, str2);
    }
}
