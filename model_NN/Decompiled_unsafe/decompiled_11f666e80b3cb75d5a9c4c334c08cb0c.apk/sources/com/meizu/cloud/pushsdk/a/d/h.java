package com.meizu.cloud.pushsdk.a.d;

import com.meizu.cloud.pushsdk.a.h.d;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public final class h extends j {

    /* renamed from: a  reason: collision with root package name */
    public static final g f1091a = g.a("multipart/mixed");
    public static final g b = g.a("multipart/alternative");
    public static final g c = g.a("multipart/digest");
    public static final g d = g.a("multipart/parallel");
    public static final g e = g.a("multipart/form-data");
    private static final byte[] f = {58, 32};
    private static final byte[] g = {13, 10};
    private static final byte[] h = {45, 45};
    private final d i;
    private final g j;
    private final g k;
    private final List<b> l;
    private long m = -1;

    public final class a {

        /* renamed from: a  reason: collision with root package name */
        private final d f1092a;
        private g b;
        private final List<b> c;

        public a() {
            this(UUID.randomUUID().toString());
        }

        public a(String str) {
            this.b = h.f1091a;
            this.c = new ArrayList();
            this.f1092a = d.a(str);
        }

        public a a(c cVar, j jVar) {
            return a(b.a(cVar, jVar));
        }

        public a a(g gVar) {
            if (gVar == null) {
                throw new NullPointerException("type == null");
            } else if (!gVar.a().equals("multipart")) {
                throw new IllegalArgumentException("multipart != " + gVar);
            } else {
                this.b = gVar;
                return this;
            }
        }

        public a a(b bVar) {
            if (bVar == null) {
                throw new NullPointerException("part == null");
            }
            this.c.add(bVar);
            return this;
        }

        public h a() {
            if (!this.c.isEmpty()) {
                return new h(this.f1092a, this.b, this.c);
            }
            throw new IllegalStateException("Multipart body must have at least one part.");
        }
    }

    public final class b {
        /* access modifiers changed from: private */

        /* renamed from: a  reason: collision with root package name */
        public final c f1093a;
        /* access modifiers changed from: private */
        public final j b;

        private b(c cVar, j jVar) {
            this.f1093a = cVar;
            this.b = jVar;
        }

        public static b a(c cVar, j jVar) {
            if (jVar == null) {
                throw new NullPointerException("body == null");
            } else if (cVar != null && cVar.a("Content-Type") != null) {
                throw new IllegalArgumentException("Unexpected header: Content-Type");
            } else if (cVar == null || cVar.a("Content-Length") == null) {
                return new b(cVar, jVar);
            } else {
                throw new IllegalArgumentException("Unexpected header: Content-Length");
            }
        }
    }

    h(d dVar, g gVar, List<b> list) {
        this.i = dVar;
        this.j = gVar;
        this.k = g.a(gVar + "; boundary=" + dVar.a());
        this.l = m.a(list);
    }

    private long a(com.meizu.cloud.pushsdk.a.h.b bVar, boolean z) {
        com.meizu.cloud.pushsdk.a.h.a aVar;
        long j2 = 0;
        if (z) {
            com.meizu.cloud.pushsdk.a.h.a aVar2 = new com.meizu.cloud.pushsdk.a.h.a();
            aVar = aVar2;
            bVar = aVar2;
        } else {
            aVar = null;
        }
        int size = this.l.size();
        for (int i2 = 0; i2 < size; i2++) {
            b bVar2 = this.l.get(i2);
            c a2 = bVar2.f1093a;
            j b2 = bVar2.b;
            bVar.c(h);
            bVar.b(this.i);
            bVar.c(g);
            if (a2 != null) {
                int a3 = a2.a();
                for (int i3 = 0; i3 < a3; i3++) {
                    bVar.b(a2.a(i3)).c(f).b(a2.b(i3)).c(g);
                }
            }
            g a4 = b2.a();
            if (a4 != null) {
                bVar.b("Content-Type: ").b(a4.toString()).c(g);
            }
            long b3 = b2.b();
            if (b3 != -1) {
                bVar.b("Content-Length: ").e(b3).c(g);
            } else if (z) {
                aVar.j();
                return -1;
            }
            bVar.c(g);
            if (z) {
                j2 += b3;
            } else {
                b2.a(bVar);
            }
            bVar.c(g);
        }
        bVar.c(h);
        bVar.b(this.i);
        bVar.c(h);
        bVar.c(g);
        if (!z) {
            return j2;
        }
        long a5 = j2 + aVar.a();
        aVar.j();
        return a5;
    }

    public g a() {
        return this.k;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.meizu.cloud.pushsdk.a.d.h.a(com.meizu.cloud.pushsdk.a.h.b, boolean):long
     arg types: [com.meizu.cloud.pushsdk.a.h.b, int]
     candidates:
      com.meizu.cloud.pushsdk.a.d.j.a(com.meizu.cloud.pushsdk.a.d.g, java.io.File):com.meizu.cloud.pushsdk.a.d.j
      com.meizu.cloud.pushsdk.a.d.j.a(com.meizu.cloud.pushsdk.a.d.g, java.lang.String):com.meizu.cloud.pushsdk.a.d.j
      com.meizu.cloud.pushsdk.a.d.j.a(com.meizu.cloud.pushsdk.a.d.g, byte[]):com.meizu.cloud.pushsdk.a.d.j
      com.meizu.cloud.pushsdk.a.d.h.a(com.meizu.cloud.pushsdk.a.h.b, boolean):long */
    public void a(com.meizu.cloud.pushsdk.a.h.b bVar) {
        a(bVar, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.meizu.cloud.pushsdk.a.d.h.a(com.meizu.cloud.pushsdk.a.h.b, boolean):long
     arg types: [?[OBJECT, ARRAY], int]
     candidates:
      com.meizu.cloud.pushsdk.a.d.j.a(com.meizu.cloud.pushsdk.a.d.g, java.io.File):com.meizu.cloud.pushsdk.a.d.j
      com.meizu.cloud.pushsdk.a.d.j.a(com.meizu.cloud.pushsdk.a.d.g, java.lang.String):com.meizu.cloud.pushsdk.a.d.j
      com.meizu.cloud.pushsdk.a.d.j.a(com.meizu.cloud.pushsdk.a.d.g, byte[]):com.meizu.cloud.pushsdk.a.d.j
      com.meizu.cloud.pushsdk.a.d.h.a(com.meizu.cloud.pushsdk.a.h.b, boolean):long */
    public long b() {
        long j2 = this.m;
        if (j2 != -1) {
            return j2;
        }
        long a2 = a((com.meizu.cloud.pushsdk.a.h.b) null, true);
        this.m = a2;
        return a2;
    }
}
