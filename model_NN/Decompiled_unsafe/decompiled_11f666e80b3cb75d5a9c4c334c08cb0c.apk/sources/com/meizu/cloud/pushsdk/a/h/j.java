package com.meizu.cloud.pushsdk.a.h;

import android.support.v4.media.session.PlaybackStateCompat;

final class j {

    /* renamed from: a  reason: collision with root package name */
    static i f1120a;
    static long b;

    private j() {
    }

    static i a() {
        synchronized (j.class) {
            if (f1120a == null) {
                return new i();
            }
            i iVar = f1120a;
            f1120a = iVar.f;
            iVar.f = null;
            b -= PlaybackStateCompat.ACTION_PLAY_FROM_SEARCH;
            return iVar;
        }
    }

    static void a(i iVar) {
        if (iVar.f != null || iVar.g != null) {
            throw new IllegalArgumentException();
        } else if (!iVar.d) {
            synchronized (j.class) {
                if (b + PlaybackStateCompat.ACTION_PLAY_FROM_SEARCH <= PlaybackStateCompat.ACTION_PREPARE_FROM_SEARCH) {
                    b += PlaybackStateCompat.ACTION_PLAY_FROM_SEARCH;
                    iVar.f = f1120a;
                    iVar.c = 0;
                    iVar.b = 0;
                    f1120a = iVar;
                }
            }
        }
    }
}
