package com.meizu.cloud.pushsdk.a.b;

import com.meizu.cloud.pushsdk.a.a.d;
import com.meizu.cloud.pushsdk.a.f.c;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class a extends ThreadPoolExecutor {

    /* renamed from: com.meizu.cloud.pushsdk.a.b.a$a  reason: collision with other inner class name */
    final class C0027a extends FutureTask<c> implements Comparable<C0027a> {

        /* renamed from: a  reason: collision with root package name */
        private final c f1073a;

        public C0027a(c cVar) {
            super(cVar, null);
            this.f1073a = cVar;
        }

        /* renamed from: a */
        public int compareTo(C0027a aVar) {
            d a2 = this.f1073a.a();
            d a3 = aVar.f1073a.a();
            return a2 == a3 ? this.f1073a.f1103a - aVar.f1073a.f1103a : a3.ordinal() - a2.ordinal();
        }
    }

    a(int i, ThreadFactory threadFactory) {
        super(i, i, 0, TimeUnit.MILLISECONDS, new PriorityBlockingQueue(), threadFactory);
    }

    public Future<?> submit(Runnable runnable) {
        C0027a aVar = new C0027a((c) runnable);
        execute(aVar);
        return aVar;
    }
}
