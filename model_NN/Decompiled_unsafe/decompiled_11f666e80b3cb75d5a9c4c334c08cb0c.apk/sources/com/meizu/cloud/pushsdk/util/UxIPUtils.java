package com.meizu.cloud.pushsdk.util;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import com.meizu.cloud.pushinternal.DebugLogger;
import com.meizu.cloud.pushsdk.constants.PushConstants;
import com.meizu.cloud.pushsdk.notification.MPushMessage;
import com.meizu.cloud.pushsdk.pushtracer.constant.Parameters;
import java.util.HashMap;

public class UxIPUtils {
    private static final String TAG = "UxIPUtils";

    public static String getTaskId(Intent intent) {
        String stringExtra = intent.getStringExtra(PushConstants.EXTRA_APP_PUSH_TASK_ID);
        if (TextUtils.isEmpty(stringExtra)) {
            try {
                MPushMessage mPushMessage = (MPushMessage) intent.getSerializableExtra(PushConstants.MZ_PUSH_PRIVATE_MESSAGE);
                if (mPushMessage != null) {
                    return mPushMessage.getTaskId();
                }
            } catch (Exception e) {
                DebugLogger.e(TAG, "paese MessageV2 error " + e.getMessage());
                return "no push platform task";
            }
        }
        return stringExtra;
    }

    public static void init(Context context) {
    }

    public static void notificationEvent(Context context, Intent intent, String str, int i) {
        notificationEvent(context, intent, "3.3.161226", str, i);
    }

    public static void notificationEvent(Context context, Intent intent, String str, String str2, int i) {
        if (!TextUtils.isEmpty(getTaskId(intent))) {
            onRecordMessageFlow(context, context.getPackageName(), intent.getStringExtra(PushConstants.MZ_PUSH_MESSAGE_STATISTICS_IMEI_KEY), getTaskId(intent), str, str2, i);
        }
    }

    public static void notificationEvent(Context context, String str, int i, String str2, String str3) {
        if (!TextUtils.isEmpty(str2)) {
            onRecordMessageFlow(context, context.getPackageName(), str3, str2, "3.3.161226", str, i);
        }
    }

    public static void onClickPushMessageEvent(Context context, String str, String str2, String str3, String str4, String str5) {
        onLogEvent(context, str, str2, str3, str4, "click_push_message", str5);
    }

    public static void onDeletePushMessageEvent(Context context, String str, String str2, String str3, String str4, String str5) {
        onLogEvent(context, str, str2, str3, str4, "delete_push_message", str5);
    }

    public static void onInvalidPushMessage(Context context, String str, String str2, String str3, String str4, String str5) {
        onLogEvent(context, str, str2, str3, str4, "invalid_push_message", str5);
    }

    public static void onLogEvent(Context context, String str, String str2, String str3, String str4, String str5, String str6) {
        HashMap hashMap = new HashMap();
        hashMap.put("taskId", str3);
        hashMap.put("deviceId", str2);
        if (TextUtils.isEmpty(str6)) {
            str6 = String.valueOf(System.currentTimeMillis() / 1000);
        }
        hashMap.put(Parameters.TIMESTAMP, str6);
        hashMap.put(Parameters.PACKAGE_NAME, str);
        hashMap.put(Parameters.PUSH_SDK_VERSION, "3.3.161226");
        if (!TextUtils.isEmpty(str4)) {
            hashMap.put(Parameters.SEQ_ID, str4);
        }
        onLogEvent(context, str5, hashMap);
    }

    /* JADX WARN: Type inference failed for: r1v6, types: [com.meizu.cloud.pushsdk.pushtracer.event.PushEvent$Builder] */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public static void onLogEvent(android.content.Context r6, java.lang.String r7, java.util.Map<java.lang.String, java.lang.String> r8) {
        /*
            java.lang.String r0 = "UxIPUtils"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "onLogEvent eventName ["
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r7)
            java.lang.String r2 = "] properties = "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r8)
            java.lang.String r1 = r1.toString()
            com.meizu.cloud.pushinternal.DebugLogger.e(r0, r1)
            java.lang.String r0 = "notification_service_message"
            boolean r0 = r0.equals(r7)
            if (r0 == 0) goto L_0x002b
        L_0x002a:
            return
        L_0x002b:
            r0 = 0
            com.meizu.cloud.pushsdk.pushtracer.tracker.Tracker r2 = com.meizu.cloud.pushsdk.pushtracer.QuickTracker.getAndroidTrackerClassic(r6, r0)
            com.meizu.cloud.pushsdk.pushtracer.event.PushEvent$Builder r0 = com.meizu.cloud.pushsdk.pushtracer.event.PushEvent.builder()
            com.meizu.cloud.pushsdk.pushtracer.event.PushEvent$Builder r1 = r0.eventName(r7)
            java.lang.String r0 = "timestamp"
            java.lang.Object r0 = r8.get(r0)
            java.lang.String r0 = (java.lang.String) r0
            java.lang.Long r0 = java.lang.Long.valueOf(r0)
            long r4 = r0.longValue()
            com.meizu.cloud.pushsdk.pushtracer.event.Event$Builder r0 = r1.timestamp(r4)
            com.meizu.cloud.pushsdk.pushtracer.event.PushEvent$Builder r0 = (com.meizu.cloud.pushsdk.pushtracer.event.PushEvent.Builder) r0
            java.lang.String r1 = "deviceId"
            java.lang.Object r1 = r8.get(r1)
            java.lang.String r1 = (java.lang.String) r1
            com.meizu.cloud.pushsdk.pushtracer.event.PushEvent$Builder r1 = r0.deviceId(r1)
            java.lang.String r0 = "package_name"
            java.lang.Object r0 = r8.get(r0)
            java.lang.String r0 = (java.lang.String) r0
            com.meizu.cloud.pushsdk.pushtracer.event.PushEvent$Builder r1 = r1.packageName(r0)
            java.lang.String r0 = "pushsdk_version"
            java.lang.Object r0 = r8.get(r0)
            java.lang.String r0 = (java.lang.String) r0
            com.meizu.cloud.pushsdk.pushtracer.event.PushEvent$Builder r1 = r1.pushsdkVersion(r0)
            java.lang.String r0 = "taskId"
            java.lang.Object r0 = r8.get(r0)
            java.lang.String r0 = (java.lang.String) r0
            com.meizu.cloud.pushsdk.pushtracer.event.PushEvent$Builder r1 = r1.taskId(r0)
            java.lang.String r0 = "seq_id"
            java.lang.Object r0 = r8.get(r0)
            java.lang.CharSequence r0 = (java.lang.CharSequence) r0
            boolean r0 = android.text.TextUtils.isEmpty(r0)
            if (r0 == 0) goto L_0x00af
            java.lang.String r0 = "null"
        L_0x008e:
            com.meizu.cloud.pushsdk.pushtracer.event.PushEvent$Builder r1 = r1.seqId(r0)
            java.lang.String r0 = "package_name"
            java.lang.Object r0 = r8.get(r0)
            java.lang.String r0 = (java.lang.String) r0
            int r0 = com.meizu.cloud.pushsdk.util.PushPreferencesUtils.getMessageSeqInCrease(r6, r0)
            java.lang.String r0 = java.lang.String.valueOf(r0)
            com.meizu.cloud.pushsdk.pushtracer.event.PushEvent$Builder r0 = r1.messageSeq(r0)
            com.meizu.cloud.pushsdk.pushtracer.event.PushEvent r0 = r0.build()
            r2.track(r0)
            goto L_0x002a
        L_0x00af:
            java.lang.String r0 = "seq_id"
            java.lang.Object r0 = r8.get(r0)
            java.lang.String r0 = (java.lang.String) r0
            goto L_0x008e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.meizu.cloud.pushsdk.util.UxIPUtils.onLogEvent(android.content.Context, java.lang.String, java.util.Map):void");
    }

    public static void onReceivePushMessageEvent(Context context, String str, String str2, String str3, String str4, String str5) {
        onLogEvent(context, str, str2, str3, str4, "receive_push_event", str5);
    }

    public static void onReceiveServerMessage(Context context, String str, String str2, String str3, String str4, String str5) {
        onLogEvent(context, str, str2, str3, str4, "receive_server_message", str5);
    }

    public static void onReceiveThroughMessage(Context context, String str, String str2, String str3, String str4, String str5) {
        onLogEvent(context, str, str2, str3, str4, "receive_push_event", str5);
    }

    public static void onRecordMessageFlow(Context context, String str, String str2, String str3, String str4, String str5, int i) {
        HashMap hashMap = new HashMap();
        hashMap.put("taskId", str3);
        hashMap.put("deviceId", str2);
        hashMap.put(Parameters.TIMESTAMP, String.valueOf(System.currentTimeMillis() / 1000));
        hashMap.put(Parameters.PACKAGE_NAME, str);
        hashMap.put(Parameters.PUSH_SDK_VERSION, str4);
        hashMap.put("push_info", str5);
        hashMap.put("push_info_type", String.valueOf(i));
        onLogEvent(context, "notification_service_message", hashMap);
    }

    public static void onShowPushMessageEvent(Context context, String str, String str2, String str3, String str4, String str5) {
        onLogEvent(context, str, str2, str3, str4, "show_push_message", str5);
    }
}
