package com.meizu.cloud.pushsdk.a.d;

import com.meizu.cloud.pushsdk.a.d.c;
import org.apache.mina.proxy.handlers.http.HttpProxyConstants;

public class i {

    /* renamed from: a  reason: collision with root package name */
    private final f f1094a;
    private final String b;
    private final c c;
    private final j d;
    private final Object e;

    public class a {
        /* access modifiers changed from: private */

        /* renamed from: a  reason: collision with root package name */
        public f f1095a;
        /* access modifiers changed from: private */
        public String b = "GET";
        /* access modifiers changed from: private */
        public c.a c = new c.a();
        /* access modifiers changed from: private */
        public j d;
        /* access modifiers changed from: private */
        public Object e;

        public a a() {
            return a("GET", (j) null);
        }

        public a a(c cVar) {
            this.c = cVar.c();
            return this;
        }

        public a a(f fVar) {
            if (fVar == null) {
                throw new IllegalArgumentException("url == null");
            }
            this.f1095a = fVar;
            return this;
        }

        public a a(j jVar) {
            return a("POST", jVar);
        }

        public a a(String str) {
            if (str == null) {
                throw new IllegalArgumentException("url == null");
            }
            if (str.regionMatches(true, 0, "ws:", 0, 3)) {
                str = "http:" + str.substring(3);
            } else if (str.regionMatches(true, 0, "wss:", 0, 4)) {
                str = "https:" + str.substring(4);
            }
            f c2 = f.c(str);
            if (c2 != null) {
                return a(c2);
            }
            throw new IllegalArgumentException("unexpected url: " + str);
        }

        public a a(String str, j jVar) {
            if (str == null || str.length() == 0) {
                throw new IllegalArgumentException("method == null || method.length() == 0");
            } else if (jVar != null && !d.b(str)) {
                throw new IllegalArgumentException("method " + str + " must not have a request body.");
            } else if (jVar != null || !d.a(str)) {
                this.b = str;
                this.d = jVar;
                return this;
            } else {
                throw new IllegalArgumentException("method " + str + " must have a request body.");
            }
        }

        public a a(String str, String str2) {
            this.c.a(str, str2);
            return this;
        }

        public a b() {
            return a("HEAD", (j) null);
        }

        public a b(j jVar) {
            return a("DELETE", jVar);
        }

        public a c(j jVar) {
            return a(HttpProxyConstants.PUT, jVar);
        }

        public i c() {
            if (this.f1095a != null) {
                return new i(this);
            }
            throw new IllegalStateException("url == null");
        }

        public a d(j jVar) {
            return a("PATCH", jVar);
        }
    }

    private i(a aVar) {
        this.f1094a = aVar.f1095a;
        this.b = aVar.b;
        this.c = aVar.c.a();
        this.d = aVar.d;
        this.e = aVar.e != null ? aVar.e : this;
    }

    public f a() {
        return this.f1094a;
    }

    public String a(String str) {
        return this.c.a(str);
    }

    public String b() {
        return this.b;
    }

    public int c() {
        if ("GET".equals(b())) {
            return 0;
        }
        if ("POST".equals(b())) {
            return 1;
        }
        if (HttpProxyConstants.PUT.equals(b())) {
            return 2;
        }
        if ("DELETE".equals(b())) {
            return 3;
        }
        if ("HEAD".equals(b())) {
            return 4;
        }
        return "PATCH".equals(b()) ? 5 : 0;
    }

    public c d() {
        return this.c;
    }

    public j e() {
        return this.d;
    }

    public boolean f() {
        return this.f1094a.a();
    }

    public String toString() {
        return "Request{method=" + this.b + ", url=" + this.f1094a + ", tag=" + (this.e != this ? this.e : null) + '}';
    }
}
