package com.meizu.cloud.pushsdk.common.b;

import com.meizu.cloud.pushsdk.common.b.e;

class a {

    /* renamed from: a  reason: collision with root package name */
    private static String f1123a = "android.os.BuildExt";
    private static e.c<Boolean> b;

    public static synchronized e.c<Boolean> a() {
        e.c<Boolean> cVar;
        synchronized (a.class) {
            if (b == null) {
                b = new e.c<>();
            }
            if (!b.f1133a) {
                b = e.a(f1123a).b("isProductInternational").a();
            }
            cVar = b;
        }
        return cVar;
    }
}
