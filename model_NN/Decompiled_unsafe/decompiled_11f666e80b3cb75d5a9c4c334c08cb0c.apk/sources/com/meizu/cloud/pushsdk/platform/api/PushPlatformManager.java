package com.meizu.cloud.pushsdk.platform.api;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import com.meizu.cloud.pushinternal.DebugLogger;
import com.meizu.cloud.pushsdk.a.c.a;
import com.meizu.cloud.pushsdk.a.e.k;
import com.meizu.cloud.pushsdk.platform.PlatformMessageSender;
import com.meizu.cloud.pushsdk.platform.message.BasicPushStatus;
import com.meizu.cloud.pushsdk.platform.message.PushSwitchStatus;
import com.meizu.cloud.pushsdk.platform.message.RegisterStatus;
import com.meizu.cloud.pushsdk.platform.message.SubAliasStatus;
import com.meizu.cloud.pushsdk.platform.message.SubTagsStatus;
import com.meizu.cloud.pushsdk.platform.message.UnRegisterStatus;
import com.meizu.cloud.pushsdk.util.MzSystemUtils;
import com.meizu.cloud.pushsdk.util.PushPreferencesUtils;
import com.sina.weibo.sdk.constant.WBConstants;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class PushPlatformManager {
    private static final String TAG = "PushPlatformManager";
    private static PushPlatformManager mInstance;
    private ExecutorService executorService = Executors.newSingleThreadExecutor();
    /* access modifiers changed from: private */
    public Context mContext;
    /* access modifiers changed from: private */
    public Handler mainHandler = new Handler(this.mContext.getMainLooper()) {
        public void handleMessage(Message message) {
            String string = message.getData().getString("deviceId");
            String string2 = message.getData().getString("appId");
            String string3 = message.getData().getString(WBConstants.SSO_APP_KEY);
            switch (message.what) {
                case 0:
                    PushPlatformManager.this.register(string2, string3, string);
                    return;
                case 1:
                    PushPlatformManager.this.unRegister(string2, string3, string);
                    return;
                case 2:
                    PushPlatformManager.this.unRegisterAdvance(message.getData().getString("packageName"), string);
                    return;
                default:
                    return;
            }
        }
    };
    private PushAPI pushAPI;

    public PushPlatformManager(Context context) {
        this.mContext = context;
        this.pushAPI = new PushAPI(context);
    }

    private void executeAfterGetDeviceId(final int i, final String str, final String str2) {
        this.executorService.execute(new Runnable() {
            public void run() {
                String deviceId = MzSystemUtils.getDeviceId(PushPlatformManager.this.mContext);
                Message obtainMessage = PushPlatformManager.this.mainHandler.obtainMessage();
                Bundle bundle = new Bundle();
                bundle.putString("deviceId", deviceId);
                bundle.putString("appId", str);
                bundle.putString(WBConstants.SSO_APP_KEY, str2);
                obtainMessage.setData(bundle);
                obtainMessage.what = i;
                DebugLogger.e(PushPlatformManager.TAG, "deviceId " + deviceId);
                PushPlatformManager.this.mainHandler.sendMessage(obtainMessage);
                if (!TextUtils.isEmpty(deviceId)) {
                    DebugLogger.i(PushPlatformManager.TAG, "put deviceId " + deviceId + " to preference");
                    PushPreferencesUtils.putDeviceId(PushPlatformManager.this.mContext, deviceId);
                }
            }
        });
    }

    public static synchronized PushPlatformManager getInstance(Context context) {
        PushPlatformManager pushPlatformManager;
        synchronized (PushPlatformManager.class) {
            if (mInstance == null) {
                mInstance = new PushPlatformManager(context);
            }
            pushPlatformManager = mInstance;
        }
        return pushPlatformManager;
    }

    public void checkPush(String str, String str2, String str3) {
        this.pushAPI.checkPush(str, str2, str3, new k() {
            public void onError(a aVar) {
                PushSwitchStatus pushSwitchStatus = new PushSwitchStatus();
                pushSwitchStatus.setCode("400");
                pushSwitchStatus.setMessage("some network error " + aVar.getMessage());
                DebugLogger.e(PushPlatformManager.TAG, "pushSwitchStatus " + pushSwitchStatus);
                PlatformMessageSender.sendPushStatus(PushPlatformManager.this.mContext, pushSwitchStatus);
            }

            public void onResponse(com.meizu.cloud.pushsdk.a.d.k kVar, String str) {
                DebugLogger.e(PushPlatformManager.TAG, "checkPush" + str.toString());
                PushSwitchStatus pushSwitchStatus = new PushSwitchStatus(str.toString());
                DebugLogger.e(PushPlatformManager.TAG, "pushSwitchStatus " + pushSwitchStatus);
                PlatformMessageSender.sendPushStatus(PushPlatformManager.this.mContext, pushSwitchStatus);
            }
        });
    }

    public void checkSubScribeAlias(String str, String str2, String str3) {
        this.pushAPI.checkSubScribeAlias(str, str2, str3, new k() {
            public void onError(a aVar) {
                SubAliasStatus subAliasStatus = new SubAliasStatus();
                subAliasStatus.setCode("400");
                subAliasStatus.setMessage("some network error " + aVar.getMessage());
                DebugLogger.e(PushPlatformManager.TAG, "subScribeAlias " + subAliasStatus);
                PlatformMessageSender.sendSubAlias(PushPlatformManager.this.mContext, subAliasStatus);
            }

            public void onResponse(com.meizu.cloud.pushsdk.a.d.k kVar, String str) {
                DebugLogger.e(PushPlatformManager.TAG, "checkSubScribeAlias " + str.toString());
                SubAliasStatus subAliasStatus = new SubAliasStatus(str.toString());
                DebugLogger.e(PushPlatformManager.TAG, "checkSubScribeAlias " + subAliasStatus);
                PlatformMessageSender.sendSubAlias(PushPlatformManager.this.mContext, subAliasStatus);
            }
        });
    }

    public void checkSubScribeTags(String str, String str2, String str3) {
        this.pushAPI.checkSubScribeTags(str, str2, str3, new k() {
            public void onError(a aVar) {
                SubTagsStatus subTagsStatus = new SubTagsStatus();
                subTagsStatus.setCode("400");
                subTagsStatus.setMessage("some network error " + aVar.getMessage());
                DebugLogger.e(PushPlatformManager.TAG, "subScribeTags " + subTagsStatus);
                PlatformMessageSender.sendSubTags(PushPlatformManager.this.mContext, subTagsStatus);
            }

            public void onResponse(com.meizu.cloud.pushsdk.a.d.k kVar, String str) {
                DebugLogger.e(PushPlatformManager.TAG, "checkSubScribeTags " + str.toString());
                SubTagsStatus subTagsStatus = new SubTagsStatus(str.toString());
                DebugLogger.e(PushPlatformManager.TAG, "checkSubScribeTags " + subTagsStatus);
                PlatformMessageSender.sendSubTags(PushPlatformManager.this.mContext, subTagsStatus);
            }
        });
    }

    public void register(String str, String str2) {
        String pushId = PushPreferencesUtils.getPushId(this.mContext);
        int pushIdExpireTime = PushPreferencesUtils.getPushIdExpireTime(this.mContext);
        if (TextUtils.isEmpty(pushId) || System.currentTimeMillis() / 1000 >= ((long) pushIdExpireTime)) {
            executeAfterGetDeviceId(0, str, str2);
            return;
        }
        RegisterStatus registerStatus = new RegisterStatus();
        registerStatus.setCode(BasicPushStatus.SUCCESS_CODE);
        registerStatus.setMessage("already register PushId,dont register frequently");
        registerStatus.setPushId(pushId);
        registerStatus.setExpireTime((int) (((long) pushIdExpireTime) - (System.currentTimeMillis() / 1000)));
        PlatformMessageSender.sendRegisterStatus(this.mContext, registerStatus);
    }

    public void register(String str, String str2, String str3) {
        this.pushAPI.register(str, str2, str3, new k() {
            public void onError(a aVar) {
                if (aVar.a() != null) {
                    DebugLogger.e(PushPlatformManager.TAG, "status code=" + aVar.b() + " data=" + aVar.a());
                }
                RegisterStatus registerStatus = new RegisterStatus();
                registerStatus.setCode("400");
                registerStatus.setMessage("some network error " + aVar.getMessage());
                DebugLogger.e(PushPlatformManager.TAG, "registerStatus " + registerStatus);
                PlatformMessageSender.sendRegisterStatus(PushPlatformManager.this.mContext, registerStatus);
            }

            public void onResponse(com.meizu.cloud.pushsdk.a.d.k kVar, String str) {
                DebugLogger.e(PushPlatformManager.TAG, str.toString());
                RegisterStatus registerStatus = new RegisterStatus(str.toString());
                DebugLogger.e(PushPlatformManager.TAG, "registerStatus " + registerStatus);
                PlatformMessageSender.sendRegisterStatus(PushPlatformManager.this.mContext, registerStatus);
            }
        });
    }

    public void register0(String str, String str2, String str3) {
        DebugLogger.e(TAG, "PushService default package start register");
        this.pushAPI.register(str, str2, str3, new k() {
            public void onError(a aVar) {
                DebugLogger.e(PushPlatformManager.TAG, "error code " + aVar.getMessage());
            }

            public void onResponse(com.meizu.cloud.pushsdk.a.d.k kVar, String str) {
                DebugLogger.e(PushPlatformManager.TAG, "Push Service" + str.toString());
            }
        });
    }

    public void subScribeAlias(String str, String str2, String str3, String str4) {
        this.pushAPI.subScribeAlias(str, str2, str3, str4, new k() {
            public void onError(a aVar) {
                SubAliasStatus subAliasStatus = new SubAliasStatus();
                subAliasStatus.setCode("400");
                subAliasStatus.setMessage("some network error " + aVar.getMessage());
                DebugLogger.e(PushPlatformManager.TAG, "subScribeAlias " + subAliasStatus);
                PlatformMessageSender.sendSubAlias(PushPlatformManager.this.mContext, subAliasStatus);
            }

            public void onResponse(com.meizu.cloud.pushsdk.a.d.k kVar, String str) {
                DebugLogger.e(PushPlatformManager.TAG, "subScribeAlias " + str.toString());
                SubAliasStatus subAliasStatus = new SubAliasStatus(str.toString());
                DebugLogger.e(PushPlatformManager.TAG, "subScribeAlias " + subAliasStatus);
                PlatformMessageSender.sendSubAlias(PushPlatformManager.this.mContext, subAliasStatus);
            }
        });
    }

    public void subScribeTags(String str, String str2, String str3, String str4) {
        this.pushAPI.subScribeTags(str, str2, str3, str4, new k() {
            public void onError(a aVar) {
                SubTagsStatus subTagsStatus = new SubTagsStatus();
                subTagsStatus.setCode("400");
                subTagsStatus.setMessage("some network error " + aVar.getMessage());
                DebugLogger.e(PushPlatformManager.TAG, "subScribeTags " + subTagsStatus);
                PlatformMessageSender.sendSubTags(PushPlatformManager.this.mContext, subTagsStatus);
            }

            public void onResponse(com.meizu.cloud.pushsdk.a.d.k kVar, String str) {
                DebugLogger.e(PushPlatformManager.TAG, "subScribeTags" + str.toString());
                SubTagsStatus subTagsStatus = new SubTagsStatus(str.toString());
                DebugLogger.e(PushPlatformManager.TAG, "subScribeTags " + subTagsStatus.getTagList());
                PlatformMessageSender.sendSubTags(PushPlatformManager.this.mContext, subTagsStatus);
            }
        });
    }

    public void switchPush(String str, String str2, String str3, int i, boolean z) {
        if (i == 0) {
            PushPreferencesUtils.setNotificationMessageSwitchStatus(this.mContext, this.mContext.getPackageName(), z);
        } else if (i == 1) {
            PushPreferencesUtils.setThroughMessageSwitchStatus(this.mContext, this.mContext.getPackageName(), z);
        }
        this.pushAPI.switchPush(str, str2, TextUtils.isEmpty(str3) ? "0" : str3, i, z, new k() {
            public void onError(a aVar) {
                PushSwitchStatus pushSwitchStatus = new PushSwitchStatus();
                pushSwitchStatus.setCode("400");
                pushSwitchStatus.setMessage("some network error " + aVar.getMessage());
                DebugLogger.e(PushPlatformManager.TAG, "pushSwitchStatus " + pushSwitchStatus);
                PlatformMessageSender.sendPushStatus(PushPlatformManager.this.mContext, pushSwitchStatus);
            }

            public void onResponse(com.meizu.cloud.pushsdk.a.d.k kVar, String str) {
                DebugLogger.e(PushPlatformManager.TAG, "switchPush" + str.toString());
                PushSwitchStatus pushSwitchStatus = new PushSwitchStatus(str.toString());
                DebugLogger.e(PushPlatformManager.TAG, "switchPush" + pushSwitchStatus);
                PlatformMessageSender.sendPushStatus(PushPlatformManager.this.mContext, pushSwitchStatus);
            }
        });
        PlatformMessageSender.switchPushMessageSetting(this.mContext, i, z, this.mContext.getPackageName());
    }

    public void unRegister(String str, String str2) {
        if (TextUtils.isEmpty(PushPreferencesUtils.getPushId(this.mContext))) {
            UnRegisterStatus unRegisterStatus = new UnRegisterStatus();
            unRegisterStatus.setCode(BasicPushStatus.SUCCESS_CODE);
            unRegisterStatus.setMessage("already unRegister PushId,dont unRegister frequently");
            unRegisterStatus.setIsUnRegisterSuccess(true);
            PlatformMessageSender.sendUnRegisterStatus(this.mContext, unRegisterStatus);
            return;
        }
        executeAfterGetDeviceId(1, str, str2);
    }

    public void unRegister(String str, String str2, String str3) {
        this.pushAPI.unRegister(str, str2, str3, new k() {
            public void onError(a aVar) {
                UnRegisterStatus unRegisterStatus = new UnRegisterStatus();
                unRegisterStatus.setCode("400");
                unRegisterStatus.setIsUnRegisterSuccess(false);
                unRegisterStatus.setMessage("some network error " + aVar.getMessage());
                DebugLogger.e(PushPlatformManager.TAG, "unRegisterStatus " + unRegisterStatus);
                PlatformMessageSender.sendUnRegisterStatus(PushPlatformManager.this.mContext, unRegisterStatus);
            }

            public void onResponse(com.meizu.cloud.pushsdk.a.d.k kVar, String str) {
                DebugLogger.e(PushPlatformManager.TAG, "unRegister" + str.toString());
                UnRegisterStatus unRegisterStatus = new UnRegisterStatus(str.toString());
                DebugLogger.e(PushPlatformManager.TAG, "unRegisterStatus " + unRegisterStatus);
                PlatformMessageSender.sendUnRegisterStatus(PushPlatformManager.this.mContext, unRegisterStatus);
            }
        });
    }

    public void unRegisterAdvance(final String str) {
        this.executorService.execute(new Runnable() {
            public void run() {
                String deviceId = MzSystemUtils.getDeviceId(PushPlatformManager.this.mContext);
                Message obtainMessage = PushPlatformManager.this.mainHandler.obtainMessage();
                Bundle bundle = new Bundle();
                bundle.putString("deviceId", deviceId);
                bundle.putString("packageName", str);
                obtainMessage.setData(bundle);
                obtainMessage.what = 2;
                DebugLogger.e(PushPlatformManager.TAG, "deviceId " + deviceId + "packageName " + str);
                PushPlatformManager.this.mainHandler.sendMessage(obtainMessage);
            }
        });
    }

    public void unRegisterAdvance(final String str, String str2) {
        this.pushAPI.unRegister(str, str2, new k() {
            public void onError(a aVar) {
                DebugLogger.e(PushPlatformManager.TAG, "unregisetr advance pakcage " + str + " error " + aVar.getMessage());
            }

            public void onResponse(com.meizu.cloud.pushsdk.a.d.k kVar, String str) {
                DebugLogger.e(PushPlatformManager.TAG, "unregisetr advance pakcage " + str + " result " + str);
            }
        });
    }

    public void unSubScribeAlias(String str, String str2, String str3, String str4) {
        this.pushAPI.unSubScribeAlias(str, str2, str3, str4, new k() {
            public void onError(a aVar) {
                SubAliasStatus subAliasStatus = new SubAliasStatus();
                subAliasStatus.setCode("400");
                subAliasStatus.setMessage("some network error " + aVar.getMessage());
                DebugLogger.e(PushPlatformManager.TAG, "subScribeAlias " + subAliasStatus);
                PlatformMessageSender.sendSubAlias(PushPlatformManager.this.mContext, subAliasStatus);
            }

            public void onResponse(com.meizu.cloud.pushsdk.a.d.k kVar, String str) {
                DebugLogger.e(PushPlatformManager.TAG, "unSubScribeAlias " + str.toString());
                SubAliasStatus subAliasStatus = new SubAliasStatus(str.toString());
                DebugLogger.e(PushPlatformManager.TAG, "unSubScribeAlias " + subAliasStatus);
                PlatformMessageSender.sendSubAlias(PushPlatformManager.this.mContext, subAliasStatus);
            }
        });
    }

    public void unSubScribeTags(String str, String str2, String str3, String str4) {
        this.pushAPI.unSubScribeTags(str, str2, str3, str4, new k() {
            public void onError(a aVar) {
                SubTagsStatus subTagsStatus = new SubTagsStatus();
                subTagsStatus.setCode("400");
                subTagsStatus.setMessage("some network error " + aVar.getMessage());
                DebugLogger.e(PushPlatformManager.TAG, "subScribeTags " + subTagsStatus);
                PlatformMessageSender.sendSubTags(PushPlatformManager.this.mContext, subTagsStatus);
            }

            public void onResponse(com.meizu.cloud.pushsdk.a.d.k kVar, String str) {
                DebugLogger.e(PushPlatformManager.TAG, "unSubScribeTags " + str.toString());
                SubTagsStatus subTagsStatus = new SubTagsStatus(str.toString());
                DebugLogger.e(PushPlatformManager.TAG, "unSubScribeTags " + subTagsStatus);
                PlatformMessageSender.sendSubTags(PushPlatformManager.this.mContext, subTagsStatus);
            }
        });
    }
}
