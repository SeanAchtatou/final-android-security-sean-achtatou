package com.meizu.cloud.pushsdk.notification;

import android.app.Notification;
import android.content.Context;
import android.graphics.Bitmap;
import android.widget.RemoteViews;
import com.meizu.cloud.pushinternal.R;
import com.meizu.cloud.pushsdk.handler.MessageV3;
import com.meizu.cloud.pushsdk.util.MinSdkChecker;

public class f extends a {
    public f(Context context, PushNotificationBuilder pushNotificationBuilder) {
        super(context, pushNotificationBuilder);
    }

    /* access modifiers changed from: protected */
    public void a(Notification notification, MessageV3 messageV3) {
        if (MinSdkChecker.isSupportNotificationBuild()) {
            RemoteViews remoteViews = new RemoteViews(this.f1141a.getPackageName(), R.layout.push_expandable_big_image_notification);
            remoteViews.setTextViewText(R.id.push_big_notification_title, messageV3.getTitle());
            remoteViews.setTextViewText(R.id.push_big_notification_content, messageV3.getContent());
            remoteViews.setLong(R.id.push_big_notification_date, "setTime", System.currentTimeMillis());
            a(remoteViews, messageV3);
            remoteViews.setViewVisibility(R.id.push_big_bigview_defaultView, 8);
            remoteViews.setViewVisibility(R.id.push_big_bigtext_defaultView, 8);
            notification.contentView = remoteViews;
        }
    }

    /* access modifiers changed from: protected */
    public void a(RemoteViews remoteViews, MessageV3 messageV3) {
        if (messageV3.getmAppIconSetting() == null || a()) {
            remoteViews.setImageViewBitmap(R.id.push_big_notification_icon, a(this.f1141a, messageV3.getPackageName()));
        } else if (!messageV3.getmAppIconSetting().isDefaultLargeIcon()) {
            Bitmap a2 = a(messageV3.getmAppIconSetting().getLargeIconUrl());
            if (a2 != null) {
                remoteViews.setImageViewBitmap(R.id.push_big_notification_icon, a2);
            } else {
                remoteViews.setImageViewBitmap(R.id.push_big_notification_icon, a(this.f1141a, messageV3.getPackageName()));
            }
        } else {
            remoteViews.setImageViewBitmap(R.id.push_big_notification_icon, a(this.f1141a, messageV3.getPackageName()));
        }
    }
}
