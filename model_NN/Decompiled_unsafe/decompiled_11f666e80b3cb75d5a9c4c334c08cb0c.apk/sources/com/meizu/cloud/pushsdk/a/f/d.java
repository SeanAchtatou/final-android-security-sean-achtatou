package com.meizu.cloud.pushsdk.a.f;

import com.meizu.cloud.pushsdk.a.d.g;
import com.meizu.cloud.pushsdk.a.d.j;
import com.meizu.cloud.pushsdk.a.e.o;
import com.meizu.cloud.pushsdk.a.h.a;
import com.meizu.cloud.pushsdk.a.h.b;
import com.meizu.cloud.pushsdk.a.h.e;
import com.meizu.cloud.pushsdk.a.h.f;
import com.meizu.cloud.pushsdk.a.h.k;

public class d extends j {

    /* renamed from: a  reason: collision with root package name */
    private final j f1105a;
    private b b;
    /* access modifiers changed from: private */
    public f c;

    public d(j jVar, o oVar) {
        this.f1105a = jVar;
        if (oVar != null) {
            this.c = new f(oVar);
        }
    }

    private k a(k kVar) {
        return new e(kVar) {

            /* renamed from: a  reason: collision with root package name */
            long f1106a = 0;
            long b = 0;

            public void a(a aVar, long j) {
                super.a(aVar, j);
                if (this.b == 0) {
                    this.b = d.this.b();
                }
                this.f1106a += j;
                if (d.this.c != null) {
                    d.this.c.obtainMessage(1, new com.meizu.cloud.pushsdk.a.g.a(this.f1106a, this.b)).sendToTarget();
                }
            }
        };
    }

    public g a() {
        return this.f1105a.a();
    }

    public void a(b bVar) {
        if (this.b == null) {
            this.b = f.a(a((k) bVar));
        }
        this.f1105a.a(this.b);
        this.b.flush();
    }

    public long b() {
        return this.f1105a.b();
    }
}
