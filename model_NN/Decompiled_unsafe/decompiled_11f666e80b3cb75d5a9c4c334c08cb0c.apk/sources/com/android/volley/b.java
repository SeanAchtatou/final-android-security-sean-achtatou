package com.android.volley;

import java.util.Collections;
import java.util.Map;

/* compiled from: Cache */
public interface b {
    a a(String str);

    void a();

    void a(String str, a aVar);

    /* compiled from: Cache */
    public static class a {

        /* renamed from: a  reason: collision with root package name */
        public byte[] f327a;
        public String b;
        public long c;
        public long d;
        public long e;
        public Map<String, String> f = Collections.emptyMap();

        public boolean a() {
            return this.d < System.currentTimeMillis();
        }

        public boolean b() {
            return this.e < System.currentTimeMillis();
        }
    }
}
