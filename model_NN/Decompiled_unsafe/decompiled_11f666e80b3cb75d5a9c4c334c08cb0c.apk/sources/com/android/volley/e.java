package com.android.volley;

import android.os.Handler;
import java.util.concurrent.Executor;

/* compiled from: ExecutorDelivery */
public class e implements o {

    /* renamed from: a  reason: collision with root package name */
    private final Executor f331a;

    public e(final Handler handler) {
        this.f331a = new Executor() {
            public void execute(Runnable runnable) {
                handler.post(runnable);
            }
        };
    }

    public void a(l<?> lVar, n<?> nVar) {
        a(lVar, nVar, null);
    }

    public void a(l<?> lVar, n<?> nVar, Runnable runnable) {
        lVar.v();
        lVar.a("post-response");
        this.f331a.execute(new a(lVar, nVar, runnable));
    }

    public void a(l<?> lVar, s sVar) {
        lVar.a("post-error");
        this.f331a.execute(new a(lVar, n.a(sVar), null));
    }

    /* compiled from: ExecutorDelivery */
    private class a implements Runnable {
        private final l b;
        private final n c;
        private final Runnable d;

        public a(l lVar, n nVar, Runnable runnable) {
            this.b = lVar;
            this.c = nVar;
            this.d = runnable;
        }

        public void run() {
            if (this.b.h()) {
                this.b.b("canceled-at-delivery");
                return;
            }
            if (this.c.a()) {
                this.b.b((Object) this.c.f341a);
            } else {
                this.b.b(this.c.c);
            }
            if (this.c.d) {
                this.b.a("intermediate-response");
            } else {
                this.b.b("done");
            }
            if (this.d != null) {
                this.d.run();
            }
        }
    }
}
