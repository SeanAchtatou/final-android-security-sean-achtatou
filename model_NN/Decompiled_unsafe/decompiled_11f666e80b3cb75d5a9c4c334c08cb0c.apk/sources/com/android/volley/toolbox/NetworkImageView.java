package com.android.volley.toolbox;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.widget.ImageView;
import com.android.volley.s;
import com.android.volley.toolbox.h;

public class NetworkImageView extends ImageView {

    /* renamed from: a  reason: collision with root package name */
    private String f346a;
    /* access modifiers changed from: private */
    public int b;
    /* access modifiers changed from: private */
    public int c;
    private h d;
    private h.c e;

    public NetworkImageView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public NetworkImageView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    public void setDefaultImageResId(int i) {
        this.b = i;
    }

    public void setErrorImageResId(int i) {
        this.c = i;
    }

    private void a(final boolean z) {
        int width = getWidth();
        int height = getHeight();
        boolean z2 = getLayoutParams() != null && getLayoutParams().height == -2 && getLayoutParams().width == -2;
        if (width != 0 || height != 0 || z2) {
            if (TextUtils.isEmpty(this.f346a)) {
                if (this.e != null) {
                    this.e.a();
                    this.e = null;
                }
                a();
                return;
            }
            if (!(this.e == null || this.e.c() == null)) {
                if (!this.e.c().equals(this.f346a)) {
                    this.e.a();
                    a();
                } else {
                    return;
                }
            }
            this.e = this.d.a(this.f346a, new h.d() {
                public void onErrorResponse(s sVar) {
                    if (NetworkImageView.this.c != 0) {
                        NetworkImageView.this.setImageResource(NetworkImageView.this.c);
                    }
                }

                public void a(final h.c cVar, boolean z) {
                    if (z && z) {
                        NetworkImageView.this.post(new Runnable() {
                            public void run() {
                                AnonymousClass1.this.a(cVar, false);
                            }
                        });
                    } else if (cVar.b() != null) {
                        NetworkImageView.this.setImageBitmap(cVar.b());
                    } else if (NetworkImageView.this.b != 0) {
                        NetworkImageView.this.setImageResource(NetworkImageView.this.b);
                    }
                }
            });
        }
    }

    private void a() {
        if (this.b != 0) {
            setImageResource(this.b);
        } else {
            setImageBitmap(null);
        }
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
        a(true);
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        if (this.e != null) {
            this.e.a();
            setImageBitmap(null);
            this.e = null;
        }
        super.onDetachedFromWindow();
    }

    /* access modifiers changed from: protected */
    public void drawableStateChanged() {
        super.drawableStateChanged();
        invalidate();
    }
}
