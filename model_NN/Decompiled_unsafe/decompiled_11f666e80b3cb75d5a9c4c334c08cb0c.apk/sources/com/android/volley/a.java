package com.android.volley;

import android.content.Intent;

/* compiled from: AuthFailureError */
public class a extends s {
    private Intent b;

    public a() {
    }

    public a(i iVar) {
        super(iVar);
    }

    public String getMessage() {
        if (this.b != null) {
            return "User needs to (re)enter credentials.";
        }
        return super.getMessage();
    }
}
