package com.qhad.ads.sdk.adcore;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.SharedPreferences;
import cn.banshenggua.aichang.utils.StringUtil;
import com.qhad.ads.sdk.adcore.HttpRequester;
import com.qhad.ads.sdk.interfaces.DynamicObject;
import com.qhad.ads.sdk.interfaces.IBridge;
import com.qhad.ads.sdk.log.QHADLog;
import com.qhad.ads.sdk.log.QhAdErrorCode;
import com.qhad.ads.sdk.log.Utils;
import com.tencent.stat.DeviceInfo;
import dalvik.system.DexClassLoader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URLEncoder;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.security.MessageDigest;
import org.json.JSONObject;
import org.json.JSONTokener;

public class UpdateBridge {
    private static final String SP_SDK_VER = "ad_sdk_ver";
    private static IBridge bridge;
    /* access modifiers changed from: private */
    public static Context context = null;
    /* access modifiers changed from: private */
    public static String md5 = null;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException}
     arg types: [java.io.File, int]
     candidates:
      ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException}
      ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException} */
    @TargetApi(14)
    public static synchronized IBridge getBridge(Context context2) {
        IBridge iBridge;
        synchronized (UpdateBridge.class) {
            if (bridge != null) {
                iBridge = bridge;
            } else {
                context = context2.getApplicationContext();
                QHADLog.init(context);
                try {
                    QHADLog.e("path: " + context.getFilesDir().getAbsolutePath() + Config.UPDATE_DIR);
                    File file = new File(context.getFilesDir().getAbsolutePath() + Config.UPDATE_DIR);
                    File file2 = new File(context.getFilesDir().getAbsolutePath() + Config.OPT_DIR);
                    if (!file.exists()) {
                        file.mkdirs();
                    }
                    if (!file2.exists()) {
                        file2.mkdirs();
                    }
                    File file3 = new File(file.getAbsolutePath() + "/" + Config.NEW_JAR);
                    File file4 = new File(file.getAbsolutePath() + "/" + Config.DEFAULT_JAR);
                    if (file3.exists()) {
                        if (file4.exists()) {
                            file4.delete();
                        }
                        file3.renameTo(file4);
                        QHADLog.d("new jar replaced old jar");
                    } else if (!file4.exists()) {
                        file4.createNewFile();
                        InputStream open = context.getAssets().open(Config.DEFAULT_JAR);
                        byte[] bArr = new byte[open.available()];
                        open.read(bArr);
                        open.close();
                        FileOutputStream fileOutputStream = new FileOutputStream(file4, false);
                        fileOutputStream.write(bArr);
                        fileOutputStream.close();
                    }
                    bridge = new BridgeProxy((DynamicObject) new DexClassLoader(file4.getAbsolutePath(), file2.getAbsolutePath(), null, context.getClassLoader()).loadClass(Config.PACKAGE_URI).newInstance());
                    getNewJar();
                    iBridge = bridge;
                } catch (ClassNotFoundException e) {
                    QHADLog.e(QhAdErrorCode.UPDATE_LOAD_ERROR, "getBridge ClassNotFoundException", e);
                    File file5 = new File(context.getFilesDir().getAbsolutePath() + Config.UPDATE_DIR);
                    File file6 = new File(file5.getAbsolutePath() + "/" + Config.NEW_JAR);
                    File file7 = new File(file5.getAbsolutePath() + "/" + Config.DEFAULT_JAR);
                    if (file6.exists()) {
                        file6.delete();
                    }
                    if (file7.exists()) {
                        file7.delete();
                    }
                    iBridge = null;
                    return iBridge;
                } catch (Exception e2) {
                    QHADLog.e(QhAdErrorCode.UPDATE_LOAD_ERROR, "getBridge Exception", e2);
                    iBridge = null;
                    return iBridge;
                }
            }
        }
        return iBridge;
    }

    private static void getNewJar() {
        final String updateUrl = getUpdateUrl();
        HttpRequester.getAsynData(context, updateUrl, false, new HttpRequester.Listener() {
            public void onGetDataSucceed(byte[] bArr) {
                UpdateBridge.parserJson(bArr);
            }

            public void onGetDataFailed(String str) {
                if (!"204".equals(str)) {
                    QHADLog.e((int) QhAdErrorCode.UPDATE_REQUEST_ERROR, "Update Jar Error:" + updateUrl + ",code:" + str);
                }
            }
        });
    }

    private static String getUpdateUrl() {
        String str = "";
        try {
            str = "&nsdkv=" + getVer(context) + "&imei=" + URLEncoder.encode(Utils.getIMEI(), StringUtil.Encoding) + "&model=" + URLEncoder.encode(Utils.getProductModel(), StringUtil.Encoding).replace("+", "%20") + "&channelid=" + URLEncoder.encode("4", StringUtil.Encoding) + "&appv=" + URLEncoder.encode(Utils.getAppVersion(), StringUtil.Encoding) + "&appvc=" + URLEncoder.encode(Utils.getAppVersionCode(), StringUtil.Encoding) + "&apppkg=" + URLEncoder.encode(Utils.getAppPackageName(), StringUtil.Encoding) + "&brand=" + URLEncoder.encode(Utils.getBrand(), StringUtil.Encoding) + "&channelname=" + URLEncoder.encode(Config.CHANNEL_NAME, StringUtil.Encoding).replace("+", "%20");
        } catch (Exception e) {
            QHADLog.d("URLEncoder Encode Error:" + e.getMessage());
        }
        return "http://show.m.mediav.com/update?sdkv=1113" + str;
    }

    /* access modifiers changed from: private */
    public static void parserJson(byte[] bArr) {
        try {
            JSONObject jSONObject = (JSONObject) new JSONTokener(new String(bArr)).nextValue();
            final int i = jSONObject.getInt(DeviceInfo.TAG_VERSION);
            int parseInt = Integer.parseInt(getVer(context));
            md5 = jSONObject.getString("md5");
            String string = jSONObject.getString("sdk_url");
            if (i > parseInt) {
                QHADLog.d("new version sdk found");
                HttpRequester.getAsynData(context, string, false, new HttpRequester.Listener() {
                    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                     method: ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException}
                     arg types: [java.io.File, int]
                     candidates:
                      ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException}
                      ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException} */
                    public void onGetDataSucceed(byte[] bArr) {
                        QHADLog.d("download jar");
                        File file = new File(new File(UpdateBridge.context.getFilesDir().getAbsolutePath() + Config.UPDATE_DIR).getAbsolutePath() + "/" + Config.NEW_JAR);
                        if (file.exists()) {
                            file.delete();
                        }
                        try {
                            file.createNewFile();
                            FileOutputStream fileOutputStream = new FileOutputStream(file, false);
                            fileOutputStream.write(bArr);
                            fileOutputStream.close();
                            if (UpdateBridge.md5 == null) {
                                return;
                            }
                            if (!UpdateBridge.getMd5ByFile(file).equals(UpdateBridge.md5)) {
                                QHADLog.e((int) QhAdErrorCode.UPDATE_MD5_ERROR, "MD5 check error: " + UpdateBridge.md5);
                                file.delete();
                                return;
                            }
                            QHADLog.d("new jar saved");
                            UpdateBridge.setVer(UpdateBridge.context, i + "");
                        } catch (Exception e) {
                            QHADLog.e("写入新包错误:" + e.getMessage());
                        }
                    }

                    public void onGetDataFailed(String str) {
                        QHADLog.e((int) QhAdErrorCode.UPDATE_DOWNLOAD_ERROR, "Download new jar error:" + str);
                    }
                });
                return;
            }
            QHADLog.d("sdk update to latest");
        } catch (Exception e) {
            QHADLog.e(QhAdErrorCode.UPDATE_PARSE_ERROR, "parse update error" + new String(bArr), e);
        }
    }

    /* access modifiers changed from: private */
    public static void setVer(Context context2, String str) {
        SharedPreferences.Editor edit = context2.getSharedPreferences("ver_info", 0).edit();
        edit.putString(SP_SDK_VER, str);
        edit.commit();
    }

    private static String getVer(Context context2) {
        SharedPreferences sharedPreferences = context2.getSharedPreferences("ver_info", 0);
        String string = sharedPreferences.getString(SP_SDK_VER, null);
        if (string != null) {
            return string;
        }
        sharedPreferences.edit().putString(SP_SDK_VER, Config.DEFAULT_SDK_VER).commit();
        return sharedPreferences.getString(SP_SDK_VER, null);
    }

    private static String getString(byte[] bArr) {
        char[] cArr = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
        char[] cArr2 = new char[(r3 * 2)];
        int i = 0;
        for (byte b : bArr) {
            int i2 = i + 1;
            cArr2[i] = cArr[(b >>> 4) & 15];
            i = i2 + 1;
            cArr2[i2] = cArr[b & 15];
        }
        return new String(cArr2);
    }

    public static String getMd5ByFile(File file) {
        Exception e;
        String str;
        try {
            FileInputStream fileInputStream = new FileInputStream(file);
            MappedByteBuffer map = fileInputStream.getChannel().map(FileChannel.MapMode.READ_ONLY, 0, file.length());
            MessageDigest instance = MessageDigest.getInstance("MD5");
            instance.update(map);
            str = getString(instance.digest());
            try {
                fileInputStream.close();
            } catch (Exception e2) {
                e = e2;
                e.printStackTrace();
                return str;
            }
        } catch (Exception e3) {
            e = e3;
            str = null;
        }
        return str;
    }
}
