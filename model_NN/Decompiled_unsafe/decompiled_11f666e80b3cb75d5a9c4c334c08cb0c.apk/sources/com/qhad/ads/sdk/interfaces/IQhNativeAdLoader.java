package com.qhad.ads.sdk.interfaces;

import java.util.HashSet;

public interface IQhNativeAdLoader {
    void clearAdAttributes();

    void clearKeywords();

    void loadAds();

    void loadAds(int i);

    void setAdAttributes(IQhAdAttributes iQhAdAttributes);

    void setKeywords(HashSet<String> hashSet);
}
