package com.qhad.ads.sdk.service;

import android.app.Service;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.IBinder;
import com.qhad.ads.sdk.adcore.UpdateBridge;
import com.qhad.ads.sdk.interfaces.IBridge;
import com.qhad.ads.sdk.interfaces.ServiceBridge;
import com.qhad.ads.sdk.log.QHADLog;
import java.io.FileDescriptor;
import java.io.PrintWriter;

public final class QhAdService extends Service {
    private ServiceBridge serviceBridge;

    public IBinder onBind(Intent intent) {
        return this.serviceBridge.onBind(intent);
    }

    public void onCreate() {
        IBridge bridge = UpdateBridge.getBridge(this);
        if (bridge == null) {
            QHADLog.e(1, "unable get updateBridge.");
            return;
        }
        this.serviceBridge = bridge.getServiceBridge(this);
        this.serviceBridge.onCreate();
    }

    public int onStartCommand(Intent intent, int i, int i2) {
        return this.serviceBridge.onStartCommand(intent, i, i2);
    }

    public void onDestroy() {
        this.serviceBridge.onDestroy();
        this.serviceBridge = null;
    }

    public void onConfigurationChanged(Configuration configuration) {
        this.serviceBridge.onConfigurationChanged(configuration);
    }

    public void onLowMemory() {
        this.serviceBridge.onLowMemory();
    }

    public void onTrimMemory(int i) {
        this.serviceBridge.onTrimMemory(i);
    }

    public boolean onUnbind(Intent intent) {
        return this.serviceBridge.onUnbind(intent);
    }

    public void onRebind(Intent intent) {
        this.serviceBridge.onRebind(intent);
    }

    public void onTaskRemoved(Intent intent) {
        this.serviceBridge.onTaskRemoved(intent);
    }

    /* access modifiers changed from: protected */
    public void dump(FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        this.serviceBridge.dump(fileDescriptor, printWriter, strArr);
    }
}
