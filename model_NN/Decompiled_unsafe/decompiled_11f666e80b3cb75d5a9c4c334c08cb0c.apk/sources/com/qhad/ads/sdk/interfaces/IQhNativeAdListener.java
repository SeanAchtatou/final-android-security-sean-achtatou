package com.qhad.ads.sdk.interfaces;

import java.util.ArrayList;

public interface IQhNativeAdListener {
    void onNativeAdLoadFailed();

    void onNativeAdLoadSucceeded(ArrayList<IQhNativeAd> arrayList);
}
