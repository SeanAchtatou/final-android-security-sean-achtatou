package com.qhad.ads.sdk.adcore;

public class Config {
    public static final String CHANNEL_ID = "4";
    public static final String CHANNEL_NAME = "qhad";
    public static final String DEFAULT_JAR = "qhad_dynamic1113.jar";
    public static final String DEFAULT_SDK_VER = "1113";
    public static final String ERROR_LOG_FILE_NAME = "qhad_updatesdk_error";
    public static final String ERROR_LOG_URL = "http://tran.mediav.com/t?type=15";
    public static final String NEW_JAR = "n1113.jar";
    public static final String OPT_DIR = "/qhad/opt/";
    public static final String PACKAGE_URI = "com.qhad.ads.sdk.core.Bridge";
    public static final String UPDATE_DIR = "/qhad/update/";
    public static final String UPDATE_SERVICE_URL = "http://show.m.mediav.com/update?sdkv=";
}
