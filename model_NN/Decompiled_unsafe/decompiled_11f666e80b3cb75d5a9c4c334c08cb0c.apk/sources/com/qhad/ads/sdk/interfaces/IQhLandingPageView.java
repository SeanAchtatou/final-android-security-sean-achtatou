package com.qhad.ads.sdk.interfaces;

import android.content.Context;

public interface IQhLandingPageView {
    void open(Context context, String str, IQhLandingPageListener iQhLandingPageListener);
}
