package com.qhad.ads.sdk.interfaces;

public interface DynamicObject {
    Object invoke(int i, Object obj);
}
