package com.qhad.ads.sdk.interfaces;

public interface IQhVideoAdLoader {
    void clearAdAttributes();

    void loadAds();

    void setAdAttributes(IQhAdAttributes iQhAdAttributes);
}
