package com.qhad.ads.sdk.interfaces;

import java.util.ArrayList;

public interface IQhVideoAdListener {
    void onVideoAdLoadFailed();

    void onVideoAdLoadSucceeded(ArrayList<IQhVideoAd> arrayList);
}
