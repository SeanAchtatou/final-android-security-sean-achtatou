package com.qhad.ads.sdk.interfaces;

public interface ObjectDescriptor {
    Object getDescriptor();
}
