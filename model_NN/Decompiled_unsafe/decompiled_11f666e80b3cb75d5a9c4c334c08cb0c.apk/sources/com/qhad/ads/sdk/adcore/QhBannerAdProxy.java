package com.qhad.ads.sdk.adcore;

import android.app.Activity;
import com.qhad.ads.sdk.interfaces.DynamicObject;
import com.qhad.ads.sdk.interfaces.IQhAdEventListener;
import com.qhad.ads.sdk.interfaces.IQhBannerAd;
import com.qhad.ads.sdk.log.QHADLog;

class QhBannerAdProxy implements IQhBannerAd {
    private final DynamicObject dynamicObject;

    public QhBannerAdProxy(DynamicObject dynamicObject2) {
        this.dynamicObject = dynamicObject2;
    }

    public void closeAds() {
        QHADLog.d("ADSUPDATE", "QHBANNERAD_CLOSEADS");
        this.dynamicObject.invoke(1, null);
    }

    public void showAds(Activity activity) {
        QHADLog.d("ADSUPDATE", "QHBANNERAD_SHOWADS");
        this.dynamicObject.invoke(2, activity);
    }

    public void setAdEventListener(Object obj) {
        QHADLog.d("ADSUPDATE", "QHBANNERAD_SETADEVENTLISTENER");
        this.dynamicObject.invoke(3, new QhAdEventListenerProxy((IQhAdEventListener) obj));
    }
}
