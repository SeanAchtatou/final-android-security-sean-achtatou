package com.qhad.ads.sdk.adcore;

import android.app.Activity;
import com.qhad.ads.sdk.interfaces.DynamicObject;
import com.qhad.ads.sdk.interfaces.IQhAdEventListener;
import com.qhad.ads.sdk.interfaces.IQhInterstitialAd;
import com.qhad.ads.sdk.log.QHADLog;

class QhInterstitialAdProxy implements IQhInterstitialAd {
    private final DynamicObject dynamicObject;

    public QhInterstitialAdProxy(DynamicObject dynamicObject2) {
        this.dynamicObject = dynamicObject2;
    }

    public void closeAds() {
        QHADLog.d("ADSUPDATE", "QHINTERSTITIALAD_closeAds");
        this.dynamicObject.invoke(23, null);
    }

    public void showAds(Activity activity) {
        QHADLog.d("ADSUPDATE", "QHINTERSTITIALAD_showAds");
        this.dynamicObject.invoke(24, activity);
    }

    public void setAdEventListener(Object obj) {
        QHADLog.d("ADSUPDATE", "QHINTERSTITIALAD_setAdEventListener");
        this.dynamicObject.invoke(25, new QhAdEventListenerProxy((IQhAdEventListener) obj));
    }
}
