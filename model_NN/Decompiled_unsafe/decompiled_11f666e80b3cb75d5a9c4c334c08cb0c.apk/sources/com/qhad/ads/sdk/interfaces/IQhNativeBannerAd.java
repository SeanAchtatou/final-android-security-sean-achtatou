package com.qhad.ads.sdk.interfaces;

import android.app.Activity;

public interface IQhNativeBannerAd {
    void closeAds();

    void setAdEventListener(Object obj);

    void showAds(Activity activity);
}
