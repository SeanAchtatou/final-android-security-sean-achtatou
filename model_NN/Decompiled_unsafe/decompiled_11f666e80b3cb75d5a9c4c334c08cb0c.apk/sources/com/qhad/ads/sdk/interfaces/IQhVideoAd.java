package com.qhad.ads.sdk.interfaces;

import android.app.Activity;
import org.json.JSONObject;

public interface IQhVideoAd {
    JSONObject getContent();

    void onAdClicked(Activity activity, int i, IQhVideoAdOnClickListener iQhVideoAdOnClickListener);

    void onAdPlayExit(int i);

    void onAdPlayFinshed(int i);

    void onAdPlayStarted();
}
