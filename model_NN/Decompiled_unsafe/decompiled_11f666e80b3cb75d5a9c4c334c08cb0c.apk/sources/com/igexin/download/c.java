package com.igexin.download;

import android.database.CrossProcessCursor;
import android.database.Cursor;
import android.database.CursorWindow;
import android.database.CursorWrapper;

class c extends CursorWrapper implements CrossProcessCursor {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ DownloadProvider f840a;
    private CrossProcessCursor b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public c(DownloadProvider downloadProvider, Cursor cursor) {
        super(cursor);
        this.f840a = downloadProvider;
        this.b = (CrossProcessCursor) cursor;
    }

    public void fillWindow(int i, CursorWindow cursorWindow) {
        this.b.fillWindow(i, cursorWindow);
    }

    public CursorWindow getWindow() {
        return this.b.getWindow();
    }

    public boolean onMove(int i, int i2) {
        return this.b.onMove(i, i2);
    }
}
