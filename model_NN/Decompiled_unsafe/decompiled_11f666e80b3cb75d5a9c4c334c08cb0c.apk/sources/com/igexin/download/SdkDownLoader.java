package com.igexin.download;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import com.baidu.mobads.interfaces.utils.IXAdSystemUtils;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class SdkDownLoader {

    /* renamed from: a  reason: collision with root package name */
    static int f837a = 3;
    static String b = "/libs/tmp";
    static SdkDownLoader c;
    Handler d;
    String[] e = {"_id", Downloads._DATA, Downloads.COLUMN_FILE_NAME_HINT, "status", Downloads.COLUMN_TOTAL_BYTES, Downloads.COLUMN_CURRENT_BYTES};
    private Context f;
    /* access modifiers changed from: private */
    public List g = new ArrayList();
    /* access modifiers changed from: private */
    public final Object h = new Object();
    public Map updateData = new HashMap();

    private SdkDownLoader(Context context) {
        this.f = context;
        this.d = new j(this, context.getMainLooper());
    }

    private int a(ContentValues contentValues) {
        try {
            ContentResolver contentResolver = this.f.getContentResolver();
            contentValues.put(Downloads.COLUMN_DATA10, String.valueOf(System.currentTimeMillis()));
            Uri insert = contentResolver.insert(Downloads.f836a, contentValues);
            if (insert != null) {
                return Integer.parseInt(insert.getPathSegments().get(1));
            }
        } catch (Throwable th) {
        }
        return -1;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    private int a(String str, String str2, ContentValues contentValues, String str3) {
        ContentValues contentValues2 = new ContentValues();
        if (contentValues != null) {
            contentValues2.putAll(contentValues);
        }
        contentValues2.put(Downloads.COLUMN_DESTINATION, (Integer) 0);
        if (str != null) {
            contentValues2.put(Downloads.COLUMN_URI, str);
        }
        if (str2 != null) {
            contentValues2.put(Downloads.COLUMN_FILE_NAME_HINT, str2.replaceAll("\\*", ""));
        }
        if (str3 != null) {
            contentValues2.put("description", str3);
        }
        return a(contentValues2);
    }

    public static SdkDownLoader getInstantiate(Context context) {
        if (c == null) {
            c = new SdkDownLoader(context);
        }
        return c;
    }

    /* access modifiers changed from: package-private */
    public IDownloadCallback a(String str) {
        if (str == null) {
            return null;
        }
        for (IDownloadCallback iDownloadCallback : this.g) {
            if (str.equals(iDownloadCallback.getName())) {
                return iDownloadCallback;
            }
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public void a(Collection collection) {
        synchronized (this.h) {
            if (collection != null) {
                if (!collection.isEmpty()) {
                    HashMap hashMap = new HashMap();
                    Iterator it = collection.iterator();
                    while (it.hasNext()) {
                        DownloadInfo downloadInfo = (DownloadInfo) it.next();
                        if (this.updateData.containsKey(Integer.valueOf(downloadInfo.mId))) {
                            DownloadInfo downloadInfo2 = (DownloadInfo) this.updateData.get(Integer.valueOf(downloadInfo.mId));
                            if (downloadInfo2 != null) {
                                downloadInfo2.copyFrom(downloadInfo);
                                hashMap.put(Integer.valueOf(downloadInfo2.mId), downloadInfo2);
                            }
                        } else {
                            DownloadInfo clone = downloadInfo.clone();
                            if (clone != null) {
                                hashMap.put(Integer.valueOf(downloadInfo.mId), clone);
                            }
                        }
                    }
                    this.updateData = hashMap;
                } else {
                    this.updateData.clear();
                }
            }
        }
    }

    public boolean deleteTask(int i) {
        try {
            this.f.getContentResolver().delete(ContentUris.withAppendedId(Downloads.f836a, (long) i), null, null);
            return true;
        } catch (Throwable th) {
            return false;
        }
    }

    public boolean deleteTask(int[] iArr) {
        try {
            ContentResolver contentResolver = this.f.getContentResolver();
            String[] strArr = new String[iArr.length];
            for (int i = 0; i < strArr.length; i++) {
                strArr[i] = String.valueOf(iArr[i]);
            }
            contentResolver.delete(Downloads.f836a, "_id=?", strArr);
            return true;
        } catch (Throwable th) {
            return false;
        }
    }

    public IDownloadCallback getCallback(String str) {
        if (str == null) {
            return null;
        }
        for (IDownloadCallback iDownloadCallback : this.g) {
            if (str.equals(iDownloadCallback.getName())) {
                return iDownloadCallback;
            }
        }
        return null;
    }

    public boolean isRegistered(String str) {
        for (IDownloadCallback name : this.g) {
            String name2 = name.getName();
            if (name2 != null && name2.equals(str)) {
                return true;
            }
        }
        return false;
    }

    public int newTask(String str, String str2, String str3, boolean z, String str4) {
        ContentValues contentValues = new ContentValues();
        if (str3 != null) {
            contentValues.put(Downloads.COLUMN_DATA6, str3);
        }
        if (z) {
            contentValues.put(Downloads.COLUMN_DATA9, IXAdSystemUtils.NT_WIFI);
        }
        contentValues.put(Downloads.COLUMN_DATA8, str4);
        return a(str, str2, contentValues, null);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public boolean pauseAllTask() {
        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put(Downloads.COLUMN_CONTROL, (Integer) 1);
            contentValues.put("status", Integer.valueOf((int) Downloads.STATUS_RUNNING_PAUSED));
            this.f.getContentResolver().update(Downloads.f836a, contentValues, "status=? OR status=? OR(status=? AND control<>?)", new String[]{String.valueOf((int) Downloads.STATUS_RUNNING), String.valueOf((int) Downloads.STATUS_PENDING), String.valueOf((int) Downloads.STATUS_RUNNING_PAUSED), String.valueOf(1)});
            return true;
        } catch (Throwable th) {
            return false;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public boolean pauseTask(int i) {
        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put(Downloads.COLUMN_CONTROL, (Integer) 1);
            this.f.getContentResolver().update(ContentUris.withAppendedId(Downloads.f836a, (long) i), contentValues, null, null);
            return true;
        } catch (Throwable th) {
            return false;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:22:0x0041  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean queryTask(java.lang.String r10) {
        /*
            r9 = this;
            r6 = 1
            r8 = 0
            r7 = 0
            android.net.Uri r0 = com.igexin.download.Downloads.f836a
            if (r0 != 0) goto L_0x0008
        L_0x0007:
            return r7
        L_0x0008:
            android.content.Context r0 = r9.f
            android.content.ContentResolver r0 = r0.getContentResolver()
            android.net.Uri r1 = com.igexin.download.Downloads.f836a     // Catch:{ Throwable -> 0x0036, all -> 0x003e }
            r2 = 0
            java.lang.String r3 = "data_8 = ? "
            r4 = 1
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ Throwable -> 0x0036, all -> 0x003e }
            r5 = 0
            r4[r5] = r10     // Catch:{ Throwable -> 0x0036, all -> 0x003e }
            r5 = 0
            android.database.Cursor r1 = r0.query(r1, r2, r3, r4, r5)     // Catch:{ Throwable -> 0x0036, all -> 0x003e }
            if (r1 == 0) goto L_0x0030
            int r0 = r1.getCount()     // Catch:{ Throwable -> 0x0048, all -> 0x0045 }
            if (r0 <= 0) goto L_0x002e
            r0 = r6
        L_0x0027:
            if (r1 == 0) goto L_0x002c
            r1.close()
        L_0x002c:
            r7 = r0
            goto L_0x0007
        L_0x002e:
            r0 = r7
            goto L_0x0027
        L_0x0030:
            if (r1 == 0) goto L_0x0007
            r1.close()
            goto L_0x0007
        L_0x0036:
            r0 = move-exception
            r0 = r8
        L_0x0038:
            if (r0 == 0) goto L_0x0007
            r0.close()
            goto L_0x0007
        L_0x003e:
            r0 = move-exception
        L_0x003f:
            if (r8 == 0) goto L_0x0044
            r8.close()
        L_0x0044:
            throw r0
        L_0x0045:
            r0 = move-exception
            r8 = r1
            goto L_0x003f
        L_0x0048:
            r0 = move-exception
            r0 = r1
            goto L_0x0038
        */
        throw new UnsupportedOperationException("Method not decompiled: com.igexin.download.SdkDownLoader.queryTask(java.lang.String):boolean");
    }

    public void refreshList() {
        Message message = new Message();
        message.what = 2;
        this.d.sendMessage(message);
    }

    public void registerDownloadCallback(IDownloadCallback iDownloadCallback) {
        if (!this.g.contains(iDownloadCallback)) {
            this.g.add(iDownloadCallback);
        }
    }

    public void setDownloadDir(String str) {
        b = str;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public boolean startTask(int i) {
        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put(Downloads.COLUMN_CONTROL, (Integer) 0);
            contentValues.put("numfailed", (Integer) 0);
            this.f.getContentResolver().update(ContentUris.withAppendedId(Downloads.f836a, (long) i), contentValues, null, null);
            return true;
        } catch (Throwable th) {
            return false;
        }
    }

    public void unregisterDownloadCallback(IDownloadCallback iDownloadCallback) {
        this.g.remove(iDownloadCallback);
    }

    public boolean updateTask(int i, String str, String str2) {
        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put(str, str2);
            this.f.getContentResolver().update(ContentUris.withAppendedId(Downloads.f836a, (long) i), contentValues, null, null);
            return true;
        } catch (Throwable th) {
            return false;
        }
    }
}
