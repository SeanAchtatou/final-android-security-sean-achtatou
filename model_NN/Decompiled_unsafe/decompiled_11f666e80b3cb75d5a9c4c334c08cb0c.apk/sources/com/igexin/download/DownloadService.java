package com.igexin.download;

import android.app.Service;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Intent;
import android.database.CharArrayBuffer;
import android.database.Cursor;
import android.os.IBinder;
import com.baidu.mobads.interfaces.utils.IXAdSystemUtils;
import com.meizu.cloud.pushsdk.constants.PushConstants;
import java.io.File;
import java.util.ArrayList;

public class DownloadService extends Service {

    /* renamed from: a  reason: collision with root package name */
    static boolean f835a = false;
    private d b;
    /* access modifiers changed from: private */
    public ArrayList c;
    /* access modifiers changed from: private */
    public f d;
    /* access modifiers changed from: private */
    public boolean e;
    /* access modifiers changed from: private */
    public e f;
    /* access modifiers changed from: private */
    public boolean g;
    /* access modifiers changed from: private */
    public Object h;
    /* access modifiers changed from: private */
    public CharArrayBuffer i;
    /* access modifiers changed from: private */
    public CharArrayBuffer j;

    /* access modifiers changed from: private */
    public long a(int i2, long j2) {
        DownloadInfo downloadInfo = (DownloadInfo) this.c.get(i2);
        if (Downloads.isStatusCompleted(downloadInfo.mStatus)) {
            return -1;
        }
        if (downloadInfo.mStatus != 193) {
            return 0;
        }
        if (downloadInfo.mNumFailed == 0) {
            return 0;
        }
        long restartTime = downloadInfo.restartTime();
        if (restartTime <= j2) {
            return 0;
        }
        return restartTime - j2;
    }

    private String a(String str, Cursor cursor, String str2) {
        int columnIndexOrThrow = cursor.getColumnIndexOrThrow(str2);
        if (str == null) {
            return cursor.getString(columnIndexOrThrow);
        }
        if (this.j == null) {
            this.j = new CharArrayBuffer(128);
        }
        cursor.copyStringToBuffer(columnIndexOrThrow, this.j);
        int i2 = this.j.sizeCopied;
        if (i2 != str.length()) {
            return cursor.getString(columnIndexOrThrow);
        }
        if (this.i == null || this.i.sizeCopied < i2) {
            this.i = new CharArrayBuffer(i2);
        }
        char[] cArr = this.i.data;
        char[] cArr2 = this.j.data;
        str.getChars(0, i2, cArr, 0);
        for (int i3 = i2 - 1; i3 >= 0; i3--) {
            if (cArr[i3] != cArr2[i3]) {
                return new String(cArr2, 0, i2);
            }
        }
        return str;
    }

    /* access modifiers changed from: private */
    public void a() {
        synchronized (this) {
            this.e = true;
            if (this.d == null) {
                this.d = new f(this);
                this.d.start();
            }
        }
    }

    /* access modifiers changed from: private */
    public void a(Cursor cursor, int i2, boolean z, boolean z2, long j2) {
        int columnIndexOrThrow = cursor.getColumnIndexOrThrow("status");
        int columnIndexOrThrow2 = cursor.getColumnIndexOrThrow("numfailed");
        int i3 = cursor.getInt(cursor.getColumnIndexOrThrow(PushConstants.MZ_PUSH_MESSAGE_METHOD));
        DownloadInfo downloadInfo = new DownloadInfo(cursor.getInt(cursor.getColumnIndexOrThrow("_id")), cursor.getString(cursor.getColumnIndexOrThrow(Downloads.COLUMN_URI)), cursor.getInt(cursor.getColumnIndexOrThrow(Downloads.COLUMN_NO_INTEGRITY)) == 1, cursor.getString(cursor.getColumnIndexOrThrow(Downloads.COLUMN_FILE_NAME_HINT)), cursor.getString(cursor.getColumnIndexOrThrow(Downloads._DATA)), cursor.getString(cursor.getColumnIndexOrThrow(Downloads.COLUMN_MIME_TYPE)), cursor.getInt(cursor.getColumnIndexOrThrow(Downloads.COLUMN_DESTINATION)), cursor.getInt(cursor.getColumnIndexOrThrow(Downloads.COLUMN_VISIBILITY)), cursor.getInt(cursor.getColumnIndexOrThrow(Downloads.COLUMN_CONTROL)), cursor.getInt(columnIndexOrThrow), cursor.getInt(columnIndexOrThrow2), 268435455 & i3, i3 >> 28, cursor.getLong(cursor.getColumnIndexOrThrow(Downloads.COLUMN_LAST_MODIFICATION)), cursor.getLong(cursor.getColumnIndexOrThrow(Downloads.COLUMN_CREATE_MODIFICATION)), cursor.getString(cursor.getColumnIndexOrThrow(Downloads.COLUMN_EXTRAS)), cursor.getString(cursor.getColumnIndexOrThrow(Downloads.COLUMN_COOKIE_DATA)), cursor.getString(cursor.getColumnIndexOrThrow(Downloads.COLUMN_USER_AGENT)), cursor.getString(cursor.getColumnIndexOrThrow(Downloads.COLUMN_REFERER)), cursor.getInt(cursor.getColumnIndexOrThrow(Downloads.COLUMN_TOTAL_BYTES)), cursor.getInt(cursor.getColumnIndexOrThrow(Downloads.COLUMN_CURRENT_BYTES)), cursor.getString(cursor.getColumnIndexOrThrow("etag")), cursor.getString(cursor.getColumnIndexOrThrow(Downloads.COLUMN_DATA1)), cursor.getString(cursor.getColumnIndexOrThrow(Downloads.COLUMN_DATA2)), cursor.getString(cursor.getColumnIndexOrThrow(Downloads.COLUMN_DATA3)), cursor.getString(cursor.getColumnIndexOrThrow(Downloads.COLUMN_DATA4)), cursor.getString(cursor.getColumnIndexOrThrow(Downloads.COLUMN_DATA5)), cursor.getString(cursor.getColumnIndexOrThrow(Downloads.COLUMN_DATA6)), cursor.getString(cursor.getColumnIndexOrThrow(Downloads.COLUMN_DATA7)), cursor.getString(cursor.getColumnIndexOrThrow(Downloads.COLUMN_DATA8)), cursor.getString(cursor.getColumnIndexOrThrow(Downloads.COLUMN_DATA9)), cursor.getLong(cursor.getColumnIndexOrThrow(Downloads.COLUMN_DATA10)), cursor.getInt(cursor.getColumnIndexOrThrow(Downloads.COLUMN_IS_WEB_ICON)), cursor.getInt(cursor.getColumnIndexOrThrow("scanned")) == 1);
        this.c.add(i2, downloadInfo);
        if (!downloadInfo.canUseNetwork(z, z2)) {
            return;
        }
        if ((IXAdSystemUtils.NT_WIFI.equals(downloadInfo.mData9) && !h.b(this)) || !downloadInfo.isReadyToStart(j2)) {
            return;
        }
        if (a(SdkDownLoader.f837a)) {
            if (!downloadInfo.mHasActiveThread) {
                if (downloadInfo.mStatus != 192) {
                    downloadInfo.mStatus = Downloads.STATUS_RUNNING;
                    ContentValues contentValues = new ContentValues();
                    contentValues.put("status", Integer.valueOf(downloadInfo.mStatus));
                    getContentResolver().update(ContentUris.withAppendedId(Downloads.f836a, (long) downloadInfo.mId), contentValues, null, null);
                }
                g gVar = new g(this, downloadInfo);
                downloadInfo.mHasActiveThread = true;
                gVar.start();
                downloadInfo.mNotice = false;
            }
        } else if (downloadInfo.mStatus != 190) {
            downloadInfo.mStatus = Downloads.STATUS_PENDING;
            ContentValues contentValues2 = new ContentValues();
            contentValues2.put("status", Integer.valueOf(downloadInfo.mStatus));
            getContentResolver().update(ContentUris.withAppendedId(Downloads.f836a, (long) downloadInfo.mId), contentValues2, null, null);
        }
    }

    private boolean a(int i2) {
        Cursor query = getContentResolver().query(Downloads.f836a, new String[]{"_id"}, "status == '192'", null, null);
        if (query == null) {
            return false;
        }
        boolean z = query.getCount() < i2;
        query.close();
        return z;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    /* access modifiers changed from: private */
    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean a(android.database.Cursor r9, int r10) {
        /*
            r8 = this;
            r2 = 0
            r1 = 1
            java.util.ArrayList r0 = r8.c
            java.lang.Object r0 = r0.get(r10)
            com.igexin.download.DownloadInfo r0 = (com.igexin.download.DownloadInfo) r0
            monitor-enter(r8)
            java.lang.Object r3 = r8.h     // Catch:{ all -> 0x006e }
            if (r3 == 0) goto L_0x006b
            java.lang.Object r3 = r8.h     // Catch:{ Exception -> 0x006a }
            java.lang.Class r3 = r3.getClass()     // Catch:{ Exception -> 0x006a }
            java.lang.String r4 = "scanFile"
            r5 = 2
            java.lang.Class[] r5 = new java.lang.Class[r5]     // Catch:{ Exception -> 0x006a }
            r6 = 0
            java.lang.Class<java.lang.String> r7 = java.lang.String.class
            r5[r6] = r7     // Catch:{ Exception -> 0x006a }
            r6 = 1
            java.lang.Class<java.lang.String> r7 = java.lang.String.class
            r5[r6] = r7     // Catch:{ Exception -> 0x006a }
            java.lang.reflect.Method r3 = r3.getMethod(r4, r5)     // Catch:{ Exception -> 0x006a }
            java.lang.Object r4 = r8.h     // Catch:{ Exception -> 0x006a }
            r5 = 2
            java.lang.Object[] r5 = new java.lang.Object[r5]     // Catch:{ Exception -> 0x006a }
            r6 = 0
            java.lang.String r7 = r0.mFileName     // Catch:{ Exception -> 0x006a }
            r5[r6] = r7     // Catch:{ Exception -> 0x006a }
            r6 = 1
            java.lang.String r7 = r0.mMimeType     // Catch:{ Exception -> 0x006a }
            r5[r6] = r7     // Catch:{ Exception -> 0x006a }
            r3.invoke(r4, r5)     // Catch:{ Exception -> 0x006a }
            r3 = 1
            r0.mMediaScanned = r3     // Catch:{ Exception -> 0x006a }
            if (r9 == 0) goto L_0x0067
            android.content.ContentValues r0 = new android.content.ContentValues     // Catch:{ Exception -> 0x006a }
            r0.<init>()     // Catch:{ Exception -> 0x006a }
            java.lang.String r3 = "scanned"
            r4 = 1
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ Exception -> 0x006a }
            r0.put(r3, r4)     // Catch:{ Exception -> 0x006a }
            android.content.ContentResolver r3 = r8.getContentResolver()     // Catch:{ Exception -> 0x006a }
            android.net.Uri r4 = com.igexin.download.Downloads.f836a     // Catch:{ Exception -> 0x006a }
            java.lang.String r5 = "_id"
            int r5 = r9.getColumnIndexOrThrow(r5)     // Catch:{ Exception -> 0x006a }
            long r6 = r9.getLong(r5)     // Catch:{ Exception -> 0x006a }
            android.net.Uri r4 = android.content.ContentUris.withAppendedId(r4, r6)     // Catch:{ Exception -> 0x006a }
            r5 = 0
            r6 = 0
            r3.update(r4, r0, r5, r6)     // Catch:{ Exception -> 0x006a }
        L_0x0067:
            monitor-exit(r8)     // Catch:{ all -> 0x006e }
            r0 = r1
        L_0x0069:
            return r0
        L_0x006a:
            r0 = move-exception
        L_0x006b:
            monitor-exit(r8)     // Catch:{ all -> 0x006e }
            r0 = r2
            goto L_0x0069
        L_0x006e:
            r0 = move-exception
            monitor-exit(r8)     // Catch:{ all -> 0x006e }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.igexin.download.DownloadService.a(android.database.Cursor, int):boolean");
    }

    private boolean a(String str) {
        return true;
    }

    /* access modifiers changed from: private */
    public void b(int i2) {
        DownloadInfo downloadInfo = (DownloadInfo) this.c.get(i2);
        if (downloadInfo.mStatus == 192) {
            downloadInfo.mStatus = Downloads.STATUS_CANCELED;
        } else if (!(downloadInfo.mDestination == 0 || downloadInfo.mFileName == null)) {
            new File(downloadInfo.mFileName).delete();
        }
        this.c.remove(i2);
    }

    /* access modifiers changed from: private */
    public void b(Cursor cursor, int i2, boolean z, boolean z2, long j2) {
        DownloadInfo downloadInfo = (DownloadInfo) this.c.get(i2);
        int columnIndexOrThrow = cursor.getColumnIndexOrThrow("status");
        int columnIndexOrThrow2 = cursor.getColumnIndexOrThrow("numfailed");
        downloadInfo.mId = cursor.getInt(cursor.getColumnIndexOrThrow("_id"));
        downloadInfo.mUri = a(downloadInfo.mUri, cursor, Downloads.COLUMN_URI);
        downloadInfo.mNoIntegrity = cursor.getInt(cursor.getColumnIndexOrThrow(Downloads.COLUMN_NO_INTEGRITY)) == 1;
        downloadInfo.mHint = a(downloadInfo.mHint, cursor, Downloads.COLUMN_FILE_NAME_HINT);
        downloadInfo.mFileName = a(downloadInfo.mFileName, cursor, Downloads._DATA);
        downloadInfo.mMimeType = a(downloadInfo.mMimeType, cursor, Downloads.COLUMN_MIME_TYPE);
        downloadInfo.mDestination = cursor.getInt(cursor.getColumnIndexOrThrow(Downloads.COLUMN_DESTINATION));
        downloadInfo.mVisibility = cursor.getInt(cursor.getColumnIndexOrThrow(Downloads.COLUMN_VISIBILITY));
        synchronized (downloadInfo) {
            downloadInfo.mControl = cursor.getInt(cursor.getColumnIndexOrThrow(Downloads.COLUMN_CONTROL));
        }
        downloadInfo.mStatus = cursor.getInt(columnIndexOrThrow);
        downloadInfo.mNumFailed = cursor.getInt(columnIndexOrThrow2);
        int i3 = cursor.getInt(cursor.getColumnIndexOrThrow(PushConstants.MZ_PUSH_MESSAGE_METHOD));
        downloadInfo.mRetryAfter = 268435455 & i3;
        downloadInfo.mRedirectCount = i3 >> 28;
        downloadInfo.mLastMod = cursor.getLong(cursor.getColumnIndexOrThrow(Downloads.COLUMN_LAST_MODIFICATION));
        downloadInfo.mCreateMod = cursor.getLong(cursor.getColumnIndexOrThrow(Downloads.COLUMN_CREATE_MODIFICATION));
        downloadInfo.mCookies = a(downloadInfo.mCookies, cursor, Downloads.COLUMN_COOKIE_DATA);
        downloadInfo.mExtras = a(downloadInfo.mExtras, cursor, Downloads.COLUMN_EXTRAS);
        downloadInfo.mUserAgent = a(downloadInfo.mUserAgent, cursor, Downloads.COLUMN_USER_AGENT);
        downloadInfo.mReferer = a(downloadInfo.mReferer, cursor, Downloads.COLUMN_REFERER);
        downloadInfo.mTotalBytes = cursor.getInt(cursor.getColumnIndexOrThrow(Downloads.COLUMN_TOTAL_BYTES));
        downloadInfo.mCurrentBytes = cursor.getInt(cursor.getColumnIndexOrThrow(Downloads.COLUMN_CURRENT_BYTES));
        downloadInfo.mETag = a(downloadInfo.mETag, cursor, "etag");
        if (!downloadInfo.canUseNetwork(z, z2)) {
            return;
        }
        if ((IXAdSystemUtils.NT_WIFI.equals(downloadInfo.mData9) && !h.b(this)) || !downloadInfo.isReadyToRestart(j2)) {
            return;
        }
        if (a(SdkDownLoader.f837a)) {
            if (!downloadInfo.mHasActiveThread) {
                downloadInfo.mStatus = Downloads.STATUS_RUNNING;
                ContentValues contentValues = new ContentValues();
                contentValues.put("status", Integer.valueOf(downloadInfo.mStatus));
                getContentResolver().update(ContentUris.withAppendedId(Downloads.f836a, (long) downloadInfo.mId), contentValues, null, null);
                g gVar = new g(this, downloadInfo);
                downloadInfo.mHasActiveThread = true;
                gVar.start();
                downloadInfo.mNotice = false;
            }
        } else if (downloadInfo.mStatus != 190) {
            downloadInfo.mStatus = Downloads.STATUS_PENDING;
            ContentValues contentValues2 = new ContentValues();
            contentValues2.put("status", Integer.valueOf(downloadInfo.mStatus));
            getContentResolver().update(ContentUris.withAppendedId(Downloads.f836a, (long) downloadInfo.mId), contentValues2, null, null);
        }
    }

    /* access modifiers changed from: private */
    public boolean b() {
        return this.h != null;
    }

    /* access modifiers changed from: private */
    public boolean c(int i2) {
        return ((DownloadInfo) this.c.get(i2)).hasCompletionNotification();
    }

    /* access modifiers changed from: private */
    public boolean d(int i2) {
        DownloadInfo downloadInfo = (DownloadInfo) this.c.get(i2);
        return !downloadInfo.mMediaScanned && downloadInfo.mDestination == 0 && Downloads.isStatusSuccess(downloadInfo.mStatus) && !a(downloadInfo.mMimeType);
    }

    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Cannot bind to Download Manager Service");
    }

    public void onCreate() {
        super.onCreate();
        try {
            this.c = (ArrayList) Class.forName("com.google.android.collect.Lists").getMethod("newArrayList", new Class[0]).invoke(null, new Object[0]);
        } catch (Exception e2) {
        }
        this.b = new d(this);
        if (Downloads.f836a == null) {
            DownloadProvider.a("downloads." + getPackageName());
        }
        getContentResolver().registerContentObserver(Downloads.f836a, true, this.b);
        this.h = null;
        this.g = false;
        this.f = new e(this);
    }

    public void onDestroy() {
        getContentResolver().unregisterContentObserver(this.b);
        super.onDestroy();
    }

    public void onStart(Intent intent, int i2) {
        super.onStart(intent, i2);
        a();
    }
}
