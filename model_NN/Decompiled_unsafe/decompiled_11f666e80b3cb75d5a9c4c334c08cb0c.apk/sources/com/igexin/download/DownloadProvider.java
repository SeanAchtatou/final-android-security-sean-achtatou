package com.igexin.download;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.UriMatcher;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import android.os.Binder;
import android.os.Process;
import java.util.HashSet;

public final class DownloadProvider extends ContentProvider {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public static String f834a = "tmpd8.db";
    private static final UriMatcher b = new UriMatcher(-1);
    private static final String[] c = {"_id", Downloads.COLUMN_APP_DATA, Downloads._DATA, Downloads.COLUMN_MIME_TYPE, Downloads.COLUMN_VISIBILITY, Downloads.COLUMN_DESTINATION, Downloads.COLUMN_CONTROL, "status", Downloads.COLUMN_LAST_MODIFICATION, Downloads.COLUMN_CREATE_MODIFICATION, Downloads.COLUMN_TOTAL_BYTES, Downloads.COLUMN_CURRENT_BYTES, "title", "description", Downloads.COLUMN_DATA8, Downloads.COLUMN_DATA10};
    private static HashSet d = new HashSet();
    private SQLiteOpenHelper e = null;

    static {
        for (String add : c) {
            d.add(add);
        }
    }

    /* access modifiers changed from: private */
    public void a(SQLiteDatabase sQLiteDatabase) {
        try {
            sQLiteDatabase.execSQL("CREATE TABLE downloads(_id INTEGER PRIMARY KEY AUTOINCREMENT,uri TEXT, method INTEGER, entity TEXT, no_integrity BOOLEAN, hint TEXT, otaupdate BOOLEAN, _data TEXT, mimetype TEXT, destination INTEGER, no_system BOOLEAN, visibility INTEGER, control INTEGER default 0, status INTEGER, numfailed INTEGER, lastmod BIGINT, createmod BIGINT, extras TEXT, cookiedata TEXT, useragent TEXT, referer TEXT, total_bytes INTEGER, current_bytes INTEGER, etag TEXT, uid INTEGER, otheruid INTEGER, title TEXT, description TEXT, scanned BOOLEAN,data_1 TEXT, data_2 TEXT, data_3 TEXT, data_4 TEXT, data_5 TEXT, data_6 TEXT, data_7 TEXT, data_8 TEXT, data_9 TEXT, data_10 BIGINT, iswebicon INTEGER);");
        } catch (SQLException e2) {
            throw e2;
        }
    }

    public static final void a(String str) {
        b.addURI(str, "download", 1);
        b.addURI(str, "download/#", 2);
        b.addURI(str, "download/full/item", 3);
        Downloads.setContentUrl(str);
    }

    private static final void a(String str, ContentValues contentValues, ContentValues contentValues2) {
        Long asLong = contentValues.getAsLong(str);
        if (asLong != null) {
            contentValues2.put(str, asLong);
        }
    }

    /* access modifiers changed from: private */
    public void b(SQLiteDatabase sQLiteDatabase) {
        try {
            sQLiteDatabase.execSQL("DROP TABLE IF EXISTS downloads");
        } catch (SQLException e2) {
            throw e2;
        }
    }

    private static final void b(String str, ContentValues contentValues, ContentValues contentValues2) {
        Integer asInteger = contentValues.getAsInteger(str);
        if (asInteger != null) {
            contentValues2.put(str, asInteger);
        }
    }

    private static final void c(String str, ContentValues contentValues, ContentValues contentValues2) {
        Boolean asBoolean = contentValues.getAsBoolean(str);
        if (asBoolean != null) {
            contentValues2.put(str, asBoolean);
        }
    }

    private static final void d(String str, ContentValues contentValues, ContentValues contentValues2) {
        String asString = contentValues.getAsString(str);
        if (asString != null) {
            contentValues2.put(str, asString);
        }
    }

    public int delete(Uri uri, String str, String[] strArr) {
        h.a(str, d);
        SQLiteDatabase writableDatabase = this.e.getWritableDatabase();
        int match = b.match(uri);
        switch (match) {
            case 1:
            case 2:
                break;
            default:
                throw new UnsupportedOperationException("Cannot delete URI: " + uri);
        }
        String str2 = str != null ? match == 1 ? "( " + str + " )" : "( " + str + " ) AND " : "";
        String str3 = match == 2 ? str2 + " ( _id = " + Long.parseLong(uri.getPathSegments().get(1)) + " ) " : str2;
        if (!(Binder.getCallingPid() == Process.myPid() || Binder.getCallingUid() == 0)) {
            str3 = str3 + " AND ( uid=" + Binder.getCallingUid() + " OR " + Downloads.COLUMN_OTHER_UID + "=" + Binder.getCallingUid() + " )";
        }
        int delete = writableDatabase.delete("downloads", str3, strArr);
        getContext().getContentResolver().notifyChange(uri, null);
        return delete;
    }

    public String getType(Uri uri) {
        switch (b.match(uri)) {
            case 1:
                return "vnd.android.cursor.dir/download";
            case 2:
                return "vnd.android.cursor.item/download";
            case 3:
                return "vnd.android.cursor.sql/download";
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public Uri insert(Uri uri, ContentValues contentValues) {
        try {
            SQLiteDatabase writableDatabase = this.e.getWritableDatabase();
            if (b.match(uri) != 1) {
                throw new IllegalArgumentException("Unknown/Invalid URI " + uri);
            }
            ContentValues contentValues2 = new ContentValues();
            d(Downloads._DATA, contentValues, contentValues2);
            d(Downloads.COLUMN_URI, contentValues, contentValues2);
            d(Downloads.COLUMN_APP_DATA, contentValues, contentValues2);
            c(Downloads.COLUMN_NO_INTEGRITY, contentValues, contentValues2);
            d(Downloads.COLUMN_FILE_NAME_HINT, contentValues, contentValues2);
            d(Downloads.COLUMN_MIME_TYPE, contentValues, contentValues2);
            Integer asInteger = contentValues.getAsInteger(Downloads.COLUMN_DESTINATION);
            if (asInteger != null) {
                contentValues2.put(Downloads.COLUMN_DESTINATION, asInteger);
            }
            Integer asInteger2 = contentValues.getAsInteger(Downloads.COLUMN_VISIBILITY);
            if (asInteger2 != null) {
                contentValues2.put(Downloads.COLUMN_VISIBILITY, asInteger2);
            } else if (asInteger.intValue() == 0) {
                contentValues2.put(Downloads.COLUMN_VISIBILITY, (Integer) 1);
            } else {
                contentValues2.put(Downloads.COLUMN_VISIBILITY, (Integer) 2);
            }
            b(Downloads.COLUMN_CONTROL, contentValues, contentValues2);
            if (!contentValues.containsKey("status")) {
                contentValues2.put("status", Integer.valueOf((int) Downloads.STATUS_PENDING));
            } else {
                b("status", contentValues, contentValues2);
            }
            contentValues2.put(Downloads.COLUMN_LAST_MODIFICATION, Long.valueOf(System.currentTimeMillis()));
            contentValues2.put(Downloads.COLUMN_CREATE_MODIFICATION, Long.valueOf(System.currentTimeMillis()));
            d(Downloads.COLUMN_EXTRAS, contentValues, contentValues2);
            d(Downloads.COLUMN_DATA1, contentValues, contentValues2);
            d(Downloads.COLUMN_DATA2, contentValues, contentValues2);
            d(Downloads.COLUMN_DATA3, contentValues, contentValues2);
            d(Downloads.COLUMN_DATA4, contentValues, contentValues2);
            d(Downloads.COLUMN_DATA5, contentValues, contentValues2);
            d(Downloads.COLUMN_DATA6, contentValues, contentValues2);
            d(Downloads.COLUMN_DATA7, contentValues, contentValues2);
            d(Downloads.COLUMN_DATA8, contentValues, contentValues2);
            d(Downloads.COLUMN_DATA9, contentValues, contentValues2);
            a(Downloads.COLUMN_DATA10, contentValues, contentValues2);
            b(Downloads.COLUMN_IS_WEB_ICON, contentValues, contentValues2);
            d(Downloads.COLUMN_COOKIE_DATA, contentValues, contentValues2);
            d(Downloads.COLUMN_USER_AGENT, contentValues, contentValues2);
            d(Downloads.COLUMN_REFERER, contentValues, contentValues2);
            contentValues2.put("uid", Integer.valueOf(Binder.getCallingUid()));
            if (Binder.getCallingUid() == 0) {
                b("uid", contentValues, contentValues2);
            }
            d("title", contentValues, contentValues2);
            d("description", contentValues, contentValues2);
            Context context = getContext();
            context.startService(new Intent(context, DownloadService.class));
            long insert = writableDatabase.insert("downloads", null, contentValues2);
            if (insert == -1) {
                return null;
            }
            context.startService(new Intent(context, DownloadService.class));
            Uri parse = Uri.parse(Downloads.f836a + "/" + insert);
            context.getContentResolver().notifyChange(uri, null);
            return parse;
        } catch (Exception e2) {
            return null;
        }
    }

    public boolean onCreate() {
        this.e = new b(this, getContext());
        a("downloads." + getContext().getPackageName());
        return true;
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x0040  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0068  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x007b  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.os.ParcelFileDescriptor openFile(android.net.Uri r10, java.lang.String r11) {
        /*
            r9 = this;
            r8 = 1
            r7 = 0
            r6 = 0
            r0 = 1
            java.lang.String[] r2 = new java.lang.String[r0]     // Catch:{ Exception -> 0x00e1, all -> 0x00df }
            r0 = 0
            java.lang.String r1 = "_data"
            r2[r0] = r1     // Catch:{ Exception -> 0x00e1, all -> 0x00df }
            r3 = 0
            r4 = 0
            r5 = 0
            r0 = r9
            r1 = r10
            android.database.Cursor r0 = r0.query(r1, r2, r3, r4, r5)     // Catch:{ Exception -> 0x00e1, all -> 0x00df }
            if (r0 == 0) goto L_0x0048
            int r1 = r0.getCount()     // Catch:{ Exception -> 0x0037, all -> 0x0063 }
        L_0x001a:
            if (r1 == r8) goto L_0x006c
            if (r1 != 0) goto L_0x004a
            java.io.FileNotFoundException r1 = new java.io.FileNotFoundException     // Catch:{ Exception -> 0x0037, all -> 0x0063 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0037, all -> 0x0063 }
            r2.<init>()     // Catch:{ Exception -> 0x0037, all -> 0x0063 }
            java.lang.String r3 = "No entry for "
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x0037, all -> 0x0063 }
            java.lang.StringBuilder r2 = r2.append(r10)     // Catch:{ Exception -> 0x0037, all -> 0x0063 }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x0037, all -> 0x0063 }
            r1.<init>(r2)     // Catch:{ Exception -> 0x0037, all -> 0x0063 }
            throw r1     // Catch:{ Exception -> 0x0037, all -> 0x0063 }
        L_0x0037:
            r1 = move-exception
        L_0x0038:
            if (r0 == 0) goto L_0x00e5
            r0.close()
            r0 = r6
        L_0x003e:
            if (r0 != 0) goto L_0x007b
            java.io.FileNotFoundException r0 = new java.io.FileNotFoundException
            java.lang.String r1 = "No filename found."
            r0.<init>(r1)
            throw r0
        L_0x0048:
            r1 = r7
            goto L_0x001a
        L_0x004a:
            java.io.FileNotFoundException r1 = new java.io.FileNotFoundException     // Catch:{ Exception -> 0x0037, all -> 0x0063 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0037, all -> 0x0063 }
            r2.<init>()     // Catch:{ Exception -> 0x0037, all -> 0x0063 }
            java.lang.String r3 = "Multiple items at "
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x0037, all -> 0x0063 }
            java.lang.StringBuilder r2 = r2.append(r10)     // Catch:{ Exception -> 0x0037, all -> 0x0063 }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x0037, all -> 0x0063 }
            r1.<init>(r2)     // Catch:{ Exception -> 0x0037, all -> 0x0063 }
            throw r1     // Catch:{ Exception -> 0x0037, all -> 0x0063 }
        L_0x0063:
            r1 = move-exception
            r6 = r0
            r0 = r1
        L_0x0066:
            if (r6 == 0) goto L_0x006b
            r6.close()
        L_0x006b:
            throw r0
        L_0x006c:
            r0.moveToFirst()     // Catch:{ Exception -> 0x0037, all -> 0x0063 }
            r1 = 0
            java.lang.String r1 = r0.getString(r1)     // Catch:{ Exception -> 0x0037, all -> 0x0063 }
            if (r0 == 0) goto L_0x00e8
            r0.close()
            r0 = r1
            goto L_0x003e
        L_0x007b:
            boolean r1 = com.igexin.download.h.a(r0)
            if (r1 != 0) goto L_0x0089
            java.io.FileNotFoundException r0 = new java.io.FileNotFoundException
            java.lang.String r1 = "Invalid filename."
            r0.<init>(r1)
            throw r0
        L_0x0089:
            java.lang.String r1 = "r"
            boolean r1 = r1.equals(r11)
            if (r1 != 0) goto L_0x00b4
            java.io.FileNotFoundException r0 = new java.io.FileNotFoundException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Bad mode for "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r10)
            java.lang.String r2 = ": "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r11)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x00b4:
            java.io.File r1 = new java.io.File
            r1.<init>(r0)
            r0 = 268435456(0x10000000, float:2.5243549E-29)
            android.os.ParcelFileDescriptor r0 = android.os.ParcelFileDescriptor.open(r1, r0)
            if (r0 != 0) goto L_0x00c9
            java.io.FileNotFoundException r0 = new java.io.FileNotFoundException
            java.lang.String r1 = "couldn't open file"
            r0.<init>(r1)
            throw r0
        L_0x00c9:
            android.content.ContentValues r1 = new android.content.ContentValues
            r1.<init>()
            java.lang.String r2 = "lastmod"
            long r4 = java.lang.System.currentTimeMillis()
            java.lang.Long r3 = java.lang.Long.valueOf(r4)
            r1.put(r2, r3)
            r9.update(r10, r1, r6, r6)
            return r0
        L_0x00df:
            r0 = move-exception
            goto L_0x0066
        L_0x00e1:
            r0 = move-exception
            r0 = r6
            goto L_0x0038
        L_0x00e5:
            r0 = r6
            goto L_0x003e
        L_0x00e8:
            r0 = r1
            goto L_0x003e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.igexin.download.DownloadProvider.openFile(android.net.Uri, java.lang.String):android.os.ParcelFileDescriptor");
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x0099 A[FALL_THROUGH] */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x00a3  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x00aa  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0109 A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x0129  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.database.Cursor query(android.net.Uri r10, java.lang.String[] r11, java.lang.String r12, java.lang.String[] r13, java.lang.String r14) {
        /*
            r9 = this;
            r2 = 1
            r5 = 0
            r3 = 0
            java.util.HashSet r0 = com.igexin.download.DownloadProvider.d
            com.igexin.download.h.a(r12, r0)
            android.database.sqlite.SQLiteOpenHelper r0 = r9.e
            android.database.sqlite.SQLiteDatabase r1 = r0.getReadableDatabase()
            android.database.sqlite.SQLiteQueryBuilder r0 = new android.database.sqlite.SQLiteQueryBuilder
            r0.<init>()
            android.content.UriMatcher r4 = com.igexin.download.DownloadProvider.b
            int r4 = r4.match(r10)
            switch(r4) {
                case 1: goto L_0x0035;
                case 2: goto L_0x00b6;
                case 3: goto L_0x00d0;
                default: goto L_0x001c;
            }
        L_0x001c:
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Unknown URI: "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r10)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x0035:
            java.lang.String r6 = "downloads"
            r0.setTables(r6)
        L_0x003a:
            int r6 = android.os.Binder.getCallingPid()
            int r7 = android.os.Process.myPid()
            if (r6 == r7) goto L_0x012c
            int r6 = android.os.Binder.getCallingUid()
            if (r6 == 0) goto L_0x012c
            boolean r6 = android.os.Process.supportsProcesses()
            if (r6 == 0) goto L_0x012c
            if (r2 != 0) goto L_0x0057
            java.lang.String r2 = " AND "
            r0.appendWhere(r2)
        L_0x0057:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r6 = "( uid="
            java.lang.StringBuilder r2 = r2.append(r6)
            int r6 = android.os.Binder.getCallingUid()
            java.lang.StringBuilder r2 = r2.append(r6)
            java.lang.String r6 = " OR "
            java.lang.StringBuilder r2 = r2.append(r6)
            java.lang.String r6 = "otheruid"
            java.lang.StringBuilder r2 = r2.append(r6)
            java.lang.String r6 = "="
            java.lang.StringBuilder r2 = r2.append(r6)
            int r6 = android.os.Binder.getCallingUid()
            java.lang.StringBuilder r2 = r2.append(r6)
            java.lang.String r6 = " )"
            java.lang.StringBuilder r2 = r2.append(r6)
            java.lang.String r2 = r2.toString()
            r0.appendWhere(r2)
            if (r11 != 0) goto L_0x00d7
            java.lang.String[] r11 = com.igexin.download.DownloadProvider.c
            r2 = r11
        L_0x0096:
            switch(r4) {
                case 3: goto L_0x0109;
                default: goto L_0x0099;
            }
        L_0x0099:
            r3 = r12
            r4 = r13
            r6 = r5
            r7 = r14
            android.database.Cursor r1 = r0.query(r1, r2, r3, r4, r5, r6, r7)
        L_0x00a1:
            if (r1 == 0) goto L_0x0129
            com.igexin.download.c r0 = new com.igexin.download.c
            r0.<init>(r9, r1)
        L_0x00a8:
            if (r0 == 0) goto L_0x00b5
            android.content.Context r1 = r9.getContext()
            android.content.ContentResolver r1 = r1.getContentResolver()
            r0.setNotificationUri(r1, r10)
        L_0x00b5:
            return r0
        L_0x00b6:
            java.lang.String r6 = "downloads"
            r0.setTables(r6)
            java.lang.String r6 = "_id="
            r0.appendWhere(r6)
            java.util.List r6 = r10.getPathSegments()
            java.lang.Object r2 = r6.get(r2)
            java.lang.CharSequence r2 = (java.lang.CharSequence) r2
            r0.appendWhere(r2)
            r2 = r3
            goto L_0x003a
        L_0x00d0:
            java.lang.String r6 = "downloads"
            r0.setTables(r6)
            goto L_0x003a
        L_0x00d7:
            r2 = r3
        L_0x00d8:
            int r6 = r11.length
            if (r2 >= r6) goto L_0x012c
            java.util.HashSet r6 = com.igexin.download.DownloadProvider.d
            r7 = r11[r2]
            boolean r6 = r6.contains(r7)
            if (r6 != 0) goto L_0x0106
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r3 = "column "
            java.lang.StringBuilder r1 = r1.append(r3)
            r2 = r11[r2]
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = " is not allowed in queries"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x0106:
            int r2 = r2 + 1
            goto L_0x00d8
        L_0x0109:
            if (r13 == 0) goto L_0x0099
            int r4 = r13.length
            if (r4 <= 0) goto L_0x0099
            r8 = r13[r3]
            int r4 = r13.length
            int r6 = r4 + -1
            java.lang.String[] r4 = new java.lang.String[r6]
        L_0x0115:
            if (r3 >= r6) goto L_0x0120
            int r7 = r3 + 1
            r7 = r13[r7]
            r4[r3] = r7
            int r3 = r3 + 1
            goto L_0x0115
        L_0x0120:
            r3 = r12
            r6 = r5
            r7 = r14
            android.database.Cursor r1 = r0.query(r1, r2, r3, r4, r5, r6, r7, r8)
            goto L_0x00a1
        L_0x0129:
            r0 = r1
            goto L_0x00a8
        L_0x012c:
            r2 = r11
            goto L_0x0096
        */
        throw new UnsupportedOperationException("Method not decompiled: com.igexin.download.DownloadProvider.query(android.net.Uri, java.lang.String[], java.lang.String, java.lang.String[], java.lang.String):android.database.Cursor");
    }

    public int update(Uri uri, ContentValues contentValues, String str, String[] strArr) {
        boolean z;
        boolean z2;
        int i = 0;
        h.a(str, d);
        SQLiteDatabase writableDatabase = this.e.getWritableDatabase();
        if (Binder.getCallingPid() != Process.myPid()) {
            ContentValues contentValues2 = new ContentValues();
            d(Downloads.COLUMN_APP_DATA, contentValues, contentValues2);
            b(Downloads.COLUMN_VISIBILITY, contentValues, contentValues2);
            Integer asInteger = contentValues.getAsInteger(Downloads.COLUMN_CONTROL);
            if (asInteger != null) {
                contentValues2.put(Downloads.COLUMN_CONTROL, asInteger);
                z2 = true;
            } else {
                z2 = false;
            }
            b(Downloads.COLUMN_CONTROL, contentValues, contentValues2);
            d("title", contentValues, contentValues2);
            d("description", contentValues, contentValues2);
            contentValues = contentValues2;
            z = z2;
        } else {
            z = false;
        }
        int match = b.match(uri);
        switch (match) {
            case 1:
            case 2:
                break;
            default:
                throw new UnsupportedOperationException("Cannot update URI: " + uri);
        }
        String str2 = str != null ? match == 1 ? "( " + str + " )" : "( " + str + " ) AND " : "";
        String str3 = match == 2 ? str2 + " ( _id = " + Long.parseLong(uri.getPathSegments().get(1)) + " ) " : str2;
        if (!(Binder.getCallingPid() == Process.myPid() || Binder.getCallingUid() == 0)) {
            str3 = str3 + " AND ( uid=" + Binder.getCallingUid() + " OR " + Downloads.COLUMN_OTHER_UID + "=" + Binder.getCallingUid() + " )";
        }
        if (contentValues.size() > 0) {
            i = writableDatabase.update("downloads", contentValues, str3, strArr);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        if (z) {
            Context context = getContext();
            context.startService(new Intent(context, DownloadService.class));
        }
        return i;
    }
}
