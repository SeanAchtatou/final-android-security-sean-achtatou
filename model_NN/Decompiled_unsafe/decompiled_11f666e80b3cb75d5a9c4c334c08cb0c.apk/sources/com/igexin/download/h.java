package com.igexin.download;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Environment;
import android.os.StatFs;
import android.os.SystemClock;
import android.telephony.TelephonyManager;
import android.webkit.MimeTypeMap;
import java.io.File;
import java.io.FileOutputStream;
import java.util.Random;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class h {

    /* renamed from: a  reason: collision with root package name */
    public static Random f845a = new Random(SystemClock.uptimeMillis());
    private static final Pattern b = Pattern.compile("attachment;\\s*filename\\s*=\\s*\"([^\"]*)\"");

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.igexin.download.h.a(java.lang.String, boolean):java.lang.String
     arg types: [java.lang.String, int]
     candidates:
      com.igexin.download.h.a(java.lang.String, java.util.Set):void
      com.igexin.download.h.a(android.content.Context, long):boolean
      com.igexin.download.h.a(java.lang.String, boolean):java.lang.String */
    public static a a(Context context, String str, String str2, String str3, String str4, String str5, int i, int i2) {
        String a2;
        String substring;
        String a3 = a(str, str2, str3, str4, i);
        int indexOf = a3.indexOf(46);
        if (indexOf < 0) {
            a2 = a(str5, true);
            substring = a3;
        } else {
            a2 = a(str5, i, a3, indexOf);
            substring = a3.substring(0, indexOf);
        }
        if (a2 != null && a2.equals(".bin")) {
            int indexOf2 = str.indexOf("?");
            if (indexOf2 >= 0) {
                str = str.substring(0, indexOf2);
            }
            int lastIndexOf = str.lastIndexOf(".");
            int lastIndexOf2 = str.lastIndexOf("/");
            if (lastIndexOf >= 0 && lastIndexOf2 >= 0 && lastIndexOf > lastIndexOf2) {
                a2 = str.substring(lastIndexOf);
            }
        }
        if (!Environment.getExternalStorageState().equals("mounted")) {
            return new a(null, null, Downloads.STATUS_FILE_ERROR);
        }
        String path = Environment.getExternalStorageDirectory().getPath();
        File file = new File(path + SdkDownLoader.b);
        if (!file.isDirectory()) {
            String[] split = SdkDownLoader.b.split("/");
            for (int i3 = 0; i3 < split.length; i3++) {
                if (split[i3] != null && split[i3].length() > 0) {
                    File file2 = new File(path + "/" + split[i3]);
                    if (!file2.exists()) {
                        file2.mkdir();
                    }
                }
            }
            if (!file.mkdir()) {
                return new a(null, null, Downloads.STATUS_FILE_ERROR);
            }
        }
        StatFs statFs = new StatFs(file.getPath());
        if (((long) statFs.getBlockSize()) * (((long) statFs.getAvailableBlocks()) - 4) < ((long) i2)) {
            return new a(null, null, Downloads.STATUS_FILE_ERROR);
        }
        String a4 = a(i, file.getPath() + File.separator + substring, a2, "recovery".equalsIgnoreCase(substring + a2));
        return a4 != null ? new a(a4, new FileOutputStream(a4), 0) : new a(null, null, Downloads.STATUS_FILE_ERROR);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0075, code lost:
        r3 = r3 * 10;
        r1 = r2;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.lang.String a(int r6, java.lang.String r7, java.lang.String r8, boolean r9) {
        /*
            r1 = 1
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.StringBuilder r0 = r0.append(r7)
            java.lang.StringBuilder r0 = r0.append(r8)
            java.lang.String r0 = r0.toString()
            java.io.File r2 = new java.io.File
            r2.<init>(r0)
            boolean r2 = r2.exists()
            if (r2 != 0) goto L_0x0028
            if (r9 == 0) goto L_0x0027
            if (r6 == r1) goto L_0x0028
            r2 = 2
            if (r6 == r2) goto L_0x0028
            r2 = 3
            if (r6 == r2) goto L_0x0028
        L_0x0027:
            return r0
        L_0x0028:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.StringBuilder r0 = r0.append(r7)
            java.lang.String r2 = "-"
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.String r4 = r0.toString()
            r3 = r1
        L_0x003c:
            r0 = 1000000000(0x3b9aca00, float:0.0047237873)
            if (r3 >= r0) goto L_0x007a
            r0 = 0
            r2 = r1
            r1 = r0
        L_0x0044:
            r0 = 9
            if (r1 >= r0) goto L_0x0075
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.StringBuilder r0 = r0.append(r4)
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.StringBuilder r0 = r0.append(r8)
            java.lang.String r0 = r0.toString()
            java.io.File r5 = new java.io.File
            r5.<init>(r0)
            boolean r5 = r5.exists()
            if (r5 == 0) goto L_0x0027
            java.util.Random r0 = com.igexin.download.h.f845a
            int r0 = r0.nextInt(r3)
            int r0 = r0 + 1
            int r2 = r2 + r0
            int r0 = r1 + 1
            r1 = r0
            goto L_0x0044
        L_0x0075:
            int r0 = r3 * 10
            r3 = r0
            r1 = r2
            goto L_0x003c
        L_0x007a:
            r0 = 0
            goto L_0x0027
        */
        throw new UnsupportedOperationException("Method not decompiled: com.igexin.download.h.a(int, java.lang.String, java.lang.String, boolean):java.lang.String");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.igexin.download.h.a(java.lang.String, boolean):java.lang.String
     arg types: [java.lang.String, int]
     candidates:
      com.igexin.download.h.a(java.lang.String, java.util.Set):void
      com.igexin.download.h.a(android.content.Context, long):boolean
      com.igexin.download.h.a(java.lang.String, boolean):java.lang.String */
    private static String a(String str, int i, String str2, int i2) {
        String str3 = null;
        if (str != null) {
            String mimeTypeFromExtension = MimeTypeMap.getSingleton().getMimeTypeFromExtension(str2.substring(str2.lastIndexOf(46) + 1));
            if ((mimeTypeFromExtension == null || !mimeTypeFromExtension.equalsIgnoreCase(str)) && (str3 = a(str, false)) != null) {
            }
        }
        return str3 == null ? str2.substring(i2) : str3;
    }

    private static String a(String str, String str2, String str3, String str4, int i) {
        String str5;
        String decode;
        int lastIndexOf;
        int lastIndexOf2;
        String str6 = null;
        if (0 == 0 && str2 != null && !str2.endsWith("/") && str2.length() > 0) {
            int lastIndexOf3 = str2.lastIndexOf(47) + 1;
            str6 = lastIndexOf3 > 0 ? str2.substring(lastIndexOf3) : str2;
        }
        if (str6 == null && str3 != null && (str6 = b(str3)) != null && (lastIndexOf2 = str6.lastIndexOf(47) + 1) > 0) {
            str6 = str6.substring(lastIndexOf2);
        }
        if (str6 != null || str4 == null || (str5 = Uri.decode(str4)) == null || str5.endsWith("/") || str5.indexOf(63) >= 0) {
            str5 = str6;
        } else {
            int lastIndexOf4 = str5.lastIndexOf(47) + 1;
            if (lastIndexOf4 > 0) {
                str5 = str5.substring(lastIndexOf4);
            }
        }
        if (str5 == null && (decode = Uri.decode(str)) != null && !decode.endsWith("/") && decode.indexOf(63) < 0 && (lastIndexOf = decode.lastIndexOf(47) + 1) > 0) {
            str5 = decode.substring(lastIndexOf);
        }
        return str5 == null ? "downloadfile" : str5;
    }

    private static String a(String str, boolean z) {
        String str2 = null;
        if (!(str == null || (str2 = MimeTypeMap.getSingleton().getExtensionFromMimeType(str)) == null)) {
            str2 = "." + str2;
        }
        return str2 == null ? (str == null || !str.toLowerCase().startsWith("text/")) ? z ? ".bin" : str2 : str.equalsIgnoreCase("text/html") ? ".html" : z ? ".txt" : str2 : str2;
    }

    private static void a(i iVar) {
        while (true) {
            if (iVar.a() == 1) {
                iVar.b();
                a(iVar);
                if (iVar.a() != 2) {
                    throw new IllegalArgumentException("syntax error, unmatched parenthese");
                }
                iVar.b();
            } else {
                b(iVar);
            }
            if (iVar.a() == 3) {
                iVar.b();
            } else {
                return;
            }
        }
    }

    public static void a(String str, Set set) {
        if (str != null) {
            try {
                i iVar = new i(str, set);
                a(iVar);
                if (iVar.a() != 9) {
                    throw new IllegalArgumentException("syntax error");
                }
            } catch (RuntimeException e) {
                throw e;
            }
        }
    }

    public static boolean a(Context context) {
        NetworkInfo[] allNetworkInfo;
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
        if (connectivityManager == null || (allNetworkInfo = connectivityManager.getAllNetworkInfo()) == null) {
            return false;
        }
        for (NetworkInfo state : allNetworkInfo) {
            if (state.getState() == NetworkInfo.State.CONNECTED) {
                return true;
            }
        }
        return false;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0072, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0073, code lost:
        r7 = r0;
        r0 = r1;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0064  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0070  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0072 A[ExcHandler: all (r1v2 'th' java.lang.Throwable A[CUSTOM_DECLARE]), Splitter:B:5:0x001a] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final boolean a(android.content.Context r11, long r12) {
        /*
            r8 = 0
            r6 = 0
            r7 = 0
            android.content.ContentResolver r0 = r11.getContentResolver()     // Catch:{ Exception -> 0x0076, all -> 0x006b }
            android.net.Uri r1 = com.igexin.download.Downloads.f836a     // Catch:{ Exception -> 0x0076, all -> 0x006b }
            r2 = 0
            java.lang.String r3 = "( status = '200' AND destination = '2' )"
            r4 = 0
            java.lang.String r5 = "lastmod"
            android.database.Cursor r0 = r0.query(r1, r2, r3, r4, r5)     // Catch:{ Exception -> 0x0076, all -> 0x006b }
            if (r0 != 0) goto L_0x001a
            r0.close()
        L_0x0019:
            return r6
        L_0x001a:
            r0.moveToFirst()     // Catch:{ Exception -> 0x007a, all -> 0x0072 }
            r2 = r8
        L_0x001e:
            boolean r1 = r0.isAfterLast()     // Catch:{ Exception -> 0x005c, all -> 0x0072 }
            if (r1 != 0) goto L_0x0067
            int r1 = (r2 > r12 ? 1 : (r2 == r12 ? 0 : -1))
            if (r1 >= 0) goto L_0x0067
            java.io.File r1 = new java.io.File     // Catch:{ Exception -> 0x005c, all -> 0x0072 }
            java.lang.String r4 = "_data"
            int r4 = r0.getColumnIndex(r4)     // Catch:{ Exception -> 0x005c, all -> 0x0072 }
            java.lang.String r4 = r0.getString(r4)     // Catch:{ Exception -> 0x005c, all -> 0x0072 }
            r1.<init>(r4)     // Catch:{ Exception -> 0x005c, all -> 0x0072 }
            long r4 = r1.length()     // Catch:{ Exception -> 0x005c, all -> 0x0072 }
            long r2 = r2 + r4
            r1.delete()     // Catch:{ Exception -> 0x005c, all -> 0x0072 }
            java.lang.String r1 = "_id"
            int r1 = r0.getColumnIndex(r1)     // Catch:{ Exception -> 0x005c, all -> 0x0072 }
            long r4 = r0.getLong(r1)     // Catch:{ Exception -> 0x005c, all -> 0x0072 }
            android.content.ContentResolver r1 = r11.getContentResolver()     // Catch:{ Exception -> 0x005c, all -> 0x0072 }
            android.net.Uri r7 = com.igexin.download.Downloads.f836a     // Catch:{ Exception -> 0x005c, all -> 0x0072 }
            android.net.Uri r4 = android.content.ContentUris.withAppendedId(r7, r4)     // Catch:{ Exception -> 0x005c, all -> 0x0072 }
            r5 = 0
            r7 = 0
            r1.delete(r4, r5, r7)     // Catch:{ Exception -> 0x005c, all -> 0x0072 }
            r0.moveToNext()     // Catch:{ Exception -> 0x005c, all -> 0x0072 }
            goto L_0x001e
        L_0x005c:
            r1 = move-exception
        L_0x005d:
            r0.close()
        L_0x0060:
            int r0 = (r2 > r8 ? 1 : (r2 == r8 ? 0 : -1))
            if (r0 <= 0) goto L_0x0070
            r0 = 1
        L_0x0065:
            r6 = r0
            goto L_0x0019
        L_0x0067:
            r0.close()
            goto L_0x0060
        L_0x006b:
            r0 = move-exception
        L_0x006c:
            r7.close()
            throw r0
        L_0x0070:
            r0 = r6
            goto L_0x0065
        L_0x0072:
            r1 = move-exception
            r7 = r0
            r0 = r1
            goto L_0x006c
        L_0x0076:
            r0 = move-exception
            r0 = r7
            r2 = r8
            goto L_0x005d
        L_0x007a:
            r1 = move-exception
            r2 = r8
            goto L_0x005d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.igexin.download.h.a(android.content.Context, long):boolean");
    }

    public static boolean a(String str) {
        File parentFile = new File(str).getParentFile();
        return parentFile.equals(Environment.getDownloadCacheDirectory()) || parentFile.equals(new File(new StringBuilder().append(Environment.getExternalStorageDirectory()).append("/libs/tmp").toString()));
    }

    private static String b(String str) {
        try {
            Matcher matcher = b.matcher(str);
            if (matcher.find()) {
                return matcher.group(1);
            }
        } catch (IllegalStateException e) {
        }
        return null;
    }

    private static void b(i iVar) {
        if (iVar.a() != 4) {
            throw new IllegalArgumentException("syntax error, expected column name");
        }
        iVar.b();
        if (iVar.a() == 5) {
            iVar.b();
            if (iVar.a() != 6) {
                throw new IllegalArgumentException("syntax error, expected quoted string");
            }
            iVar.b();
        } else if (iVar.a() == 7) {
            iVar.b();
            if (iVar.a() != 8) {
                throw new IllegalArgumentException("syntax error, expected NULL");
            }
            iVar.b();
        } else {
            throw new IllegalArgumentException("syntax error after column name");
        }
    }

    public static boolean b(Context context) {
        NetworkInfo activeNetworkInfo;
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
        return (connectivityManager == null || (activeNetworkInfo = connectivityManager.getActiveNetworkInfo()) == null || activeNetworkInfo.getType() != 1) ? false : true;
    }

    public static boolean c(Context context) {
        NetworkInfo activeNetworkInfo;
        TelephonyManager telephonyManager;
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
        return (connectivityManager == null || (activeNetworkInfo = connectivityManager.getActiveNetworkInfo()) == null || activeNetworkInfo.getType() != 0 || (telephonyManager = (TelephonyManager) context.getSystemService("phone")) == null || !telephonyManager.isNetworkRoaming()) ? false : true;
    }
}
