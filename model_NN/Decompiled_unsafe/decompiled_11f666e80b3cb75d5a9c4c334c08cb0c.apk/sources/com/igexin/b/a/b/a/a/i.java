package com.igexin.b.a.b.a.a;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import com.igexin.b.a.c.a;
import com.igexin.push.core.a.e;
import java.net.Socket;

class i extends Handler {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ d f801a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public i(d dVar) {
        super(Looper.getMainLooper());
        this.f801a = dVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.igexin.b.a.b.a.a.d.a(com.igexin.b.a.b.a.a.d, boolean):boolean
     arg types: [com.igexin.b.a.b.a.a.d, int]
     candidates:
      com.igexin.b.a.b.a.a.d.a(com.igexin.b.a.b.a.a.d, com.igexin.b.a.b.a.a.k):void
      com.igexin.b.a.b.a.a.d.a(com.igexin.b.a.b.a.a.d, java.net.Socket):void
      com.igexin.b.a.b.a.a.d.a(com.igexin.b.a.b.a.a.d, boolean):boolean */
    public void handleMessage(Message message) {
        switch (message.what) {
            case 0:
                if (!this.f801a.k()) {
                    this.f801a.f();
                    return;
                }
                return;
            case 1:
                boolean unused = this.f801a.l = false;
                return;
            case 2:
                this.f801a.a((Socket) message.obj);
                return;
            case 3:
                if (this.f801a.j()) {
                    this.f801a.h();
                    return;
                }
                return;
            case 4:
                this.f801a.l();
                if (this.f801a.k == null && this.f801a.j == null && this.f801a.i == null) {
                    e.a().e(false);
                    return;
                } else if (this.f801a.j()) {
                    this.f801a.h();
                    return;
                } else {
                    this.f801a.g();
                    return;
                }
            case 5:
                if (this.f801a.j()) {
                    a.b(d.e + "|tcp already close reconnect immediately");
                    e.a().e(((Boolean) message.obj).booleanValue());
                    return;
                }
                a.b(d.e + "|reconnect will run after close");
                return;
            default:
                return;
        }
    }
}
