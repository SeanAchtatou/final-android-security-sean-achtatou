package com.igexin.b.a.b.a.a;

import android.os.Message;
import com.igexin.b.a.b.b;
import com.igexin.b.a.b.c;
import com.igexin.b.a.c.a;
import com.igexin.b.a.d.e;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public final class d {
    /* access modifiers changed from: private */
    public static final String e = d.class.getName();
    private static d m;
    private static final Object n = new Object();

    /* renamed from: a  reason: collision with root package name */
    public Lock f796a = new ReentrantLock();
    public Condition b = this.f796a.newCondition();
    public ConcurrentLinkedQueue<k> c = new ConcurrentLinkedQueue<>();
    public List<k> d = new ArrayList();
    private b f;
    private com.igexin.b.a.b.d g;
    private Socket h;
    /* access modifiers changed from: private */
    public j i;
    /* access modifiers changed from: private */
    public l j;
    /* access modifiers changed from: private */
    public c k;
    /* access modifiers changed from: private */
    public boolean l;
    /* access modifiers changed from: private */
    public i o = new i(this);
    private long p;
    private final Comparator<k> q = new h(this);

    private d() {
    }

    public static synchronized d a() {
        d dVar;
        synchronized (d.class) {
            if (m == null) {
                m = new d();
            }
            dVar = m;
        }
        return dVar;
    }

    /* access modifiers changed from: private */
    public void a(Socket socket) {
        try {
            if (!this.k.g()) {
                this.h = socket;
                this.g = new com.igexin.b.a.b.d();
                b(socket);
                c(socket);
            }
        } catch (Exception e2) {
            a.b(e + "|" + e2.toString());
            b();
        }
    }

    /* access modifiers changed from: private */
    public void b(k kVar) {
        if (kVar.y <= 0 || kVar.D == null) {
            kVar.p();
            return;
        }
        long currentTimeMillis = System.currentTimeMillis();
        kVar.b(currentTimeMillis);
        synchronized (n) {
            this.d.add(kVar);
            Collections.sort(this.d, this.q);
            this.p = TimeUnit.SECONDS.toMillis((long) this.d.get(0).y);
            if (this.p > 0 && this.d.size() == 1) {
                a.b(e + "|add : " + kVar.toString() + " --- " + kVar.c.getClass().getName() + " set alarm " + "delay = " + (this.p + e.u));
                c.b().b(currentTimeMillis + this.p + e.u);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.igexin.b.a.d.e.a(com.igexin.b.a.d.d, boolean):boolean
     arg types: [com.igexin.b.a.b.a.a.j, int]
     candidates:
      com.igexin.b.a.d.e.a(com.igexin.b.a.d.d, com.igexin.b.a.d.d):int
      com.igexin.b.a.d.e.a(com.igexin.b.a.d.a.e, com.igexin.b.a.d.a.b):boolean
      com.igexin.b.a.d.e.a(com.igexin.b.a.d.d, boolean):boolean */
    private void b(Socket socket) {
        this.i = new j(new m(socket.getInputStream()), this.f, this.g);
        this.i.a(new f(this));
        c.b().a((com.igexin.b.a.d.d) this.i, true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.igexin.b.a.d.e.a(com.igexin.b.a.d.d, boolean):boolean
     arg types: [com.igexin.b.a.b.a.a.l, int]
     candidates:
      com.igexin.b.a.d.e.a(com.igexin.b.a.d.d, com.igexin.b.a.d.d):int
      com.igexin.b.a.d.e.a(com.igexin.b.a.d.a.e, com.igexin.b.a.d.a.b):boolean
      com.igexin.b.a.d.e.a(com.igexin.b.a.d.d, boolean):boolean */
    private void c(Socket socket) {
        this.j = new l(new n(socket.getOutputStream()), this.f, this.g);
        this.j.a(new g(this));
        c.b().a((com.igexin.b.a.d.d) this.j, true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.igexin.b.a.d.e.a(com.igexin.b.a.d.d, boolean):boolean
     arg types: [com.igexin.b.a.b.a.a.c, int]
     candidates:
      com.igexin.b.a.d.e.a(com.igexin.b.a.d.d, com.igexin.b.a.d.d):int
      com.igexin.b.a.d.e.a(com.igexin.b.a.d.a.e, com.igexin.b.a.d.a.b):boolean
      com.igexin.b.a.d.e.a(com.igexin.b.a.d.d, boolean):boolean */
    /* access modifiers changed from: private */
    public void f() {
        a.b(e + "|disconnect = true, reconnect");
        this.k = new c(new e(this));
        c.b().a((com.igexin.b.a.d.d) this.k, true);
    }

    /* access modifiers changed from: private */
    public void g() {
        if (this.k != null) {
            this.k.h();
        }
        if (this.j != null) {
            this.j.h();
            if (this.j.j != null) {
                try {
                    this.j.j.a();
                } catch (Exception e2) {
                }
            }
        }
        if (this.i != null) {
            this.i.h();
            if (this.i.i != null) {
                try {
                    this.i.i.a();
                } catch (Exception e3) {
                }
            }
        }
        if (this.h != null) {
            try {
                if (!this.h.isClosed()) {
                    this.h.close();
                }
            } catch (Exception e4) {
            }
        }
    }

    /* access modifiers changed from: private */
    public void h() {
        if (!this.l) {
            this.l = true;
            i();
            com.igexin.push.core.a.e.a().b(false);
        }
    }

    private void i() {
        if (this.j != null) {
            this.j.j = null;
            this.j = null;
        }
        if (this.i != null) {
            this.i.i = null;
            this.i = null;
        }
        this.k = null;
        this.h = null;
        this.g = null;
    }

    /* access modifiers changed from: private */
    public boolean j() {
        return (this.k == null || this.k.e) && (this.i == null || this.i.e) && (this.j == null || this.j.e);
    }

    /* access modifiers changed from: private */
    public boolean k() {
        return this.h != null && !this.h.isClosed();
    }

    /* access modifiers changed from: private */
    public void l() {
        c.b().e();
        a.b(e + "|cancel alrm");
        synchronized (n) {
            if (!this.d.isEmpty()) {
                for (k p2 : this.d) {
                    p2.p();
                }
                this.d.clear();
            }
        }
        if (!m.c.isEmpty()) {
            Iterator<k> it = m.c.iterator();
            while (it.hasNext()) {
                it.next().p();
            }
            m.c.clear();
        }
    }

    public void a(k kVar) {
        try {
            this.f796a.lock();
            this.c.offer(kVar);
            this.b.signalAll();
            try {
                this.f796a.unlock();
            } catch (Exception e2) {
            }
        } catch (Exception e3) {
            try {
                this.f796a.unlock();
            } catch (Exception e4) {
            }
        } catch (Throwable th) {
            try {
                this.f796a.unlock();
            } catch (Exception e5) {
            }
            throw th;
        }
    }

    public void a(b bVar) {
        this.f = bVar;
        if (this.i != null) {
            this.i.j = bVar;
        }
        if (this.j != null) {
            this.j.i = bVar;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:38:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(java.lang.String r13) {
        /*
            r12 = this;
            r10 = 0
            r1 = 0
            long r2 = java.lang.System.currentTimeMillis()
            java.lang.Object r4 = com.igexin.b.a.b.a.a.d.n
            monitor-enter(r4)
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x008d }
            r0.<init>()     // Catch:{ all -> 0x008d }
            java.lang.String r5 = com.igexin.b.a.b.a.a.d.e     // Catch:{ all -> 0x008d }
            java.lang.StringBuilder r0 = r0.append(r5)     // Catch:{ all -> 0x008d }
            java.lang.String r5 = "|"
            java.lang.StringBuilder r0 = r0.append(r5)     // Catch:{ all -> 0x008d }
            java.lang.StringBuilder r0 = r0.append(r13)     // Catch:{ all -> 0x008d }
            java.lang.String r5 = " -- resp"
            java.lang.StringBuilder r0 = r0.append(r5)     // Catch:{ all -> 0x008d }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x008d }
            com.igexin.b.a.c.a.b(r0)     // Catch:{ all -> 0x008d }
            java.util.List<com.igexin.b.a.b.a.a.k> r0 = r12.d     // Catch:{ all -> 0x008d }
            java.util.Iterator r5 = r0.iterator()     // Catch:{ all -> 0x008d }
        L_0x0032:
            boolean r0 = r5.hasNext()     // Catch:{ all -> 0x008d }
            if (r0 == 0) goto L_0x013c
            java.lang.Object r0 = r5.next()     // Catch:{ all -> 0x008d }
            com.igexin.b.a.b.a.a.k r0 = (com.igexin.b.a.b.a.a.k) r0     // Catch:{ all -> 0x008d }
            com.igexin.b.a.d.a.f r6 = r0.D     // Catch:{ all -> 0x008d }
            boolean r6 = r6.a(r2, r0)     // Catch:{ all -> 0x008d }
            if (r6 == 0) goto L_0x0078
            r0.p()     // Catch:{ all -> 0x008d }
            com.igexin.b.a.d.a.f r1 = r0.D     // Catch:{ all -> 0x008d }
            r1.a(r0)     // Catch:{ all -> 0x008d }
            r0 = 1
            r5.remove()     // Catch:{ all -> 0x008d }
        L_0x0052:
            com.igexin.b.a.b.c r1 = com.igexin.b.a.b.c.b()     // Catch:{ all -> 0x008d }
            r1.e()     // Catch:{ all -> 0x008d }
            if (r0 == 0) goto L_0x0090
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x008d }
            r0.<init>()     // Catch:{ all -> 0x008d }
            java.lang.String r1 = com.igexin.b.a.b.a.a.d.e     // Catch:{ all -> 0x008d }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ all -> 0x008d }
            java.lang.String r1 = "|time out"
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ all -> 0x008d }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x008d }
            com.igexin.b.a.c.a.b(r0)     // Catch:{ all -> 0x008d }
            r12.d()     // Catch:{ all -> 0x008d }
            monitor-exit(r4)     // Catch:{ all -> 0x008d }
        L_0x0077:
            return
        L_0x0078:
            com.igexin.b.a.d.a.f r6 = r0.D     // Catch:{ all -> 0x008d }
            long r6 = r6.b(r2, r0)     // Catch:{ all -> 0x008d }
            long r8 = r12.p     // Catch:{ all -> 0x008d }
            int r0 = (r8 > r10 ? 1 : (r8 == r10 ? 0 : -1))
            if (r0 < 0) goto L_0x008a
            long r8 = r12.p     // Catch:{ all -> 0x008d }
            int r0 = (r8 > r6 ? 1 : (r8 == r6 ? 0 : -1))
            if (r0 <= 0) goto L_0x0032
        L_0x008a:
            r12.p = r6     // Catch:{ all -> 0x008d }
            goto L_0x0032
        L_0x008d:
            r0 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x008d }
            throw r0
        L_0x0090:
            java.util.List<com.igexin.b.a.b.a.a.k> r0 = r12.d     // Catch:{ all -> 0x008d }
            int r0 = r0.size()     // Catch:{ all -> 0x008d }
            if (r0 <= 0) goto L_0x00e4
            java.util.List<com.igexin.b.a.b.a.a.k> r0 = r12.d     // Catch:{ all -> 0x008d }
            r1 = 0
            java.lang.Object r0 = r0.get(r1)     // Catch:{ all -> 0x008d }
            com.igexin.b.a.b.a.a.k r0 = (com.igexin.b.a.b.a.a.k) r0     // Catch:{ all -> 0x008d }
            r0.p()     // Catch:{ all -> 0x008d }
            com.igexin.b.a.b.c r1 = com.igexin.b.a.b.c.b()     // Catch:{ all -> 0x008d }
            r1.a(r0)     // Catch:{ all -> 0x008d }
            java.util.List<com.igexin.b.a.b.a.a.k> r1 = r12.d     // Catch:{ all -> 0x008d }
            r1.remove(r0)     // Catch:{ all -> 0x008d }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x008d }
            r1.<init>()     // Catch:{ all -> 0x008d }
            java.lang.String r5 = com.igexin.b.a.b.a.a.d.e     // Catch:{ all -> 0x008d }
            java.lang.StringBuilder r1 = r1.append(r5)     // Catch:{ all -> 0x008d }
            java.lang.String r5 = "|remove : "
            java.lang.StringBuilder r1 = r1.append(r5)     // Catch:{ all -> 0x008d }
            java.lang.String r5 = r0.toString()     // Catch:{ all -> 0x008d }
            java.lang.StringBuilder r1 = r1.append(r5)     // Catch:{ all -> 0x008d }
            java.lang.String r5 = " -- "
            java.lang.StringBuilder r1 = r1.append(r5)     // Catch:{ all -> 0x008d }
            java.lang.Object r0 = r0.c     // Catch:{ all -> 0x008d }
            java.lang.Class r0 = r0.getClass()     // Catch:{ all -> 0x008d }
            java.lang.String r0 = r0.getSimpleName()     // Catch:{ all -> 0x008d }
            java.lang.StringBuilder r0 = r1.append(r0)     // Catch:{ all -> 0x008d }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x008d }
            com.igexin.b.a.c.a.b(r0)     // Catch:{ all -> 0x008d }
        L_0x00e4:
            java.util.List<com.igexin.b.a.b.a.a.k> r0 = r12.d     // Catch:{ all -> 0x008d }
            int r0 = r0.size()     // Catch:{ all -> 0x008d }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x008d }
            r1.<init>()     // Catch:{ all -> 0x008d }
            java.lang.String r5 = com.igexin.b.a.b.a.a.d.e     // Catch:{ all -> 0x008d }
            java.lang.StringBuilder r1 = r1.append(r5)     // Catch:{ all -> 0x008d }
            java.lang.String r5 = "|r, size = "
            java.lang.StringBuilder r1 = r1.append(r5)     // Catch:{ all -> 0x008d }
            java.lang.StringBuilder r1 = r1.append(r0)     // Catch:{ all -> 0x008d }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x008d }
            com.igexin.b.a.c.a.b(r1)     // Catch:{ all -> 0x008d }
            if (r0 <= 0) goto L_0x0139
            long r0 = r12.p     // Catch:{ all -> 0x008d }
            int r0 = (r0 > r10 ? 1 : (r0 == r10 ? 0 : -1))
            if (r0 <= 0) goto L_0x0139
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x008d }
            r0.<init>()     // Catch:{ all -> 0x008d }
            java.lang.String r1 = com.igexin.b.a.b.a.a.d.e     // Catch:{ all -> 0x008d }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ all -> 0x008d }
            java.lang.String r1 = "|set alarm = "
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ all -> 0x008d }
            long r6 = r12.p     // Catch:{ all -> 0x008d }
            java.lang.StringBuilder r0 = r0.append(r6)     // Catch:{ all -> 0x008d }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x008d }
            com.igexin.b.a.c.a.b(r0)     // Catch:{ all -> 0x008d }
            com.igexin.b.a.b.c r0 = com.igexin.b.a.b.c.b()     // Catch:{ all -> 0x008d }
            long r6 = r12.p     // Catch:{ all -> 0x008d }
            long r2 = r2 + r6
            long r6 = com.igexin.b.a.d.e.u     // Catch:{ all -> 0x008d }
            long r2 = r2 + r6
            r0.b(r2)     // Catch:{ all -> 0x008d }
        L_0x0139:
            monitor-exit(r4)     // Catch:{ all -> 0x008d }
            goto L_0x0077
        L_0x013c:
            r0 = r1
            goto L_0x0052
        */
        throw new UnsupportedOperationException("Method not decompiled: com.igexin.b.a.b.a.a.d.a(java.lang.String):void");
    }

    public synchronized void a(boolean z) {
        a.b(e + "|reconnect, reset = " + z);
        Message obtain = Message.obtain();
        obtain.obj = Boolean.valueOf(z);
        obtain.what = 5;
        this.o.sendMessage(obtain);
    }

    public synchronized void b() {
        this.o.sendEmptyMessage(4);
    }

    public void c() {
        this.o.sendEmptyMessage(0);
    }

    public synchronized void d() {
        a.b(e + "|alarm timeout disconnect");
        this.o.sendEmptyMessage(4);
    }
}
