package com.igexin.b.a.c;

import android.app.Activity;
import android.util.Log;
import com.igexin.push.config.q;
import java.lang.Thread;

public class a extends Activity implements Thread.UncaughtExceptionHandler {

    /* renamed from: a  reason: collision with root package name */
    public static boolean f806a = q.f887a.equals("debug");

    public static synchronized void a(String str) {
        synchronized (a.class) {
            b(str);
        }
    }

    public static void a(String str, String str2) {
        if (f806a) {
            Log.d(str, str2);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException}
     arg types: [java.io.File, int]
     candidates:
      ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException}
      ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException} */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x00e5 A[SYNTHETIC, Splitter:B:41:0x00e5] */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x00f3 A[SYNTHETIC, Splitter:B:47:0x00f3] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static synchronized void b(java.lang.String r8) {
        /*
            java.lang.Class<com.igexin.b.a.c.a> r2 = com.igexin.b.a.c.a.class
            monitor-enter(r2)
            boolean r0 = com.igexin.b.a.c.a.f806a     // Catch:{ all -> 0x00f7 }
            if (r0 != 0) goto L_0x0017
            boolean r0 = com.igexin.push.core.g.O     // Catch:{ all -> 0x00f7 }
            if (r0 == 0) goto L_0x0015
            long r0 = com.igexin.push.core.g.P     // Catch:{ all -> 0x00f7 }
            long r4 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x00f7 }
            int r0 = (r0 > r4 ? 1 : (r0 == r4 ? 0 : -1))
            if (r0 >= 0) goto L_0x0017
        L_0x0015:
            monitor-exit(r2)
            return
        L_0x0017:
            java.text.SimpleDateFormat r0 = new java.text.SimpleDateFormat     // Catch:{ all -> 0x00f7 }
            java.lang.String r1 = "yyyy-MM-dd"
            java.util.Locale r3 = java.util.Locale.getDefault()     // Catch:{ all -> 0x00f7 }
            r0.<init>(r1, r3)     // Catch:{ all -> 0x00f7 }
            java.util.Date r1 = new java.util.Date     // Catch:{ all -> 0x00f7 }
            r1.<init>()     // Catch:{ all -> 0x00f7 }
            java.lang.String r1 = r0.format(r1)     // Catch:{ all -> 0x00f7 }
            java.lang.String r3 = com.igexin.push.core.g.e     // Catch:{ all -> 0x00f7 }
            boolean r0 = android.text.TextUtils.isEmpty(r3)     // Catch:{ all -> 0x00f7 }
            if (r0 != 0) goto L_0x0015
            java.lang.String r0 = android.os.Environment.getExternalStorageState()     // Catch:{ all -> 0x00f7 }
            java.lang.String r4 = "mounted"
            boolean r0 = r0.equals(r4)     // Catch:{ all -> 0x00f7 }
            if (r0 == 0) goto L_0x0015
            java.lang.String r4 = "/sdcard/libs//"
            java.io.File r0 = new java.io.File     // Catch:{ all -> 0x00f7 }
            r0.<init>(r4)     // Catch:{ all -> 0x00f7 }
            boolean r5 = r0.exists()     // Catch:{ all -> 0x00f7 }
            if (r5 != 0) goto L_0x0052
            boolean r0 = r0.mkdir()     // Catch:{ all -> 0x00f7 }
            if (r0 == 0) goto L_0x0015
        L_0x0052:
            r0 = 0
            java.io.File r5 = new java.io.File     // Catch:{ Exception -> 0x00e2, all -> 0x00ed }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00e2, all -> 0x00ed }
            r6.<init>()     // Catch:{ Exception -> 0x00e2, all -> 0x00ed }
            java.lang.StringBuilder r4 = r6.append(r4)     // Catch:{ Exception -> 0x00e2, all -> 0x00ed }
            java.lang.StringBuilder r3 = r4.append(r3)     // Catch:{ Exception -> 0x00e2, all -> 0x00ed }
            java.lang.String r4 = "."
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x00e2, all -> 0x00ed }
            java.lang.StringBuilder r1 = r3.append(r1)     // Catch:{ Exception -> 0x00e2, all -> 0x00ed }
            java.lang.String r3 = ".log"
            java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ Exception -> 0x00e2, all -> 0x00ed }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x00e2, all -> 0x00ed }
            r5.<init>(r1)     // Catch:{ Exception -> 0x00e2, all -> 0x00ed }
            boolean r1 = r5.exists()     // Catch:{ Exception -> 0x00e2, all -> 0x00ed }
            if (r1 != 0) goto L_0x008d
            boolean r1 = r5.createNewFile()     // Catch:{ Exception -> 0x00e2, all -> 0x00ed }
            if (r1 != 0) goto L_0x008d
            if (r0 == 0) goto L_0x0015
            r0.close()     // Catch:{ IOException -> 0x008b }
            goto L_0x0015
        L_0x008b:
            r0 = move-exception
            goto L_0x0015
        L_0x008d:
            java.io.FileOutputStream r1 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x00e2, all -> 0x00ed }
            r3 = 1
            r1.<init>(r5, r3)     // Catch:{ Exception -> 0x00e2, all -> 0x00ed }
            java.text.SimpleDateFormat r0 = new java.text.SimpleDateFormat     // Catch:{ Exception -> 0x00fe, all -> 0x00fc }
            java.lang.String r3 = "yyyy-MM-dd HH:mm:ss:SSS"
            java.util.Locale r4 = java.util.Locale.getDefault()     // Catch:{ Exception -> 0x00fe, all -> 0x00fc }
            r0.<init>(r3, r4)     // Catch:{ Exception -> 0x00fe, all -> 0x00fc }
            java.util.Date r3 = new java.util.Date     // Catch:{ Exception -> 0x00fe, all -> 0x00fc }
            r3.<init>()     // Catch:{ Exception -> 0x00fe, all -> 0x00fc }
            java.lang.String r0 = r0.format(r3)     // Catch:{ Exception -> 0x00fe, all -> 0x00fc }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00fe, all -> 0x00fc }
            r3.<init>()     // Catch:{ Exception -> 0x00fe, all -> 0x00fc }
            java.lang.StringBuilder r0 = r3.append(r0)     // Catch:{ Exception -> 0x00fe, all -> 0x00fc }
            java.lang.String r3 = "|"
            java.lang.StringBuilder r0 = r0.append(r3)     // Catch:{ Exception -> 0x00fe, all -> 0x00fc }
            java.lang.StringBuilder r0 = r0.append(r8)     // Catch:{ Exception -> 0x00fe, all -> 0x00fc }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x00fe, all -> 0x00fc }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00fe, all -> 0x00fc }
            r3.<init>()     // Catch:{ Exception -> 0x00fe, all -> 0x00fc }
            java.lang.StringBuilder r0 = r3.append(r0)     // Catch:{ Exception -> 0x00fe, all -> 0x00fc }
            java.lang.String r3 = "\r\n"
            java.lang.StringBuilder r0 = r0.append(r3)     // Catch:{ Exception -> 0x00fe, all -> 0x00fc }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x00fe, all -> 0x00fc }
            byte[] r0 = r0.getBytes()     // Catch:{ Exception -> 0x00fe, all -> 0x00fc }
            r1.write(r0)     // Catch:{ Exception -> 0x00fe, all -> 0x00fc }
            if (r1 == 0) goto L_0x0015
            r1.close()     // Catch:{ IOException -> 0x00df }
            goto L_0x0015
        L_0x00df:
            r0 = move-exception
            goto L_0x0015
        L_0x00e2:
            r1 = move-exception
        L_0x00e3:
            if (r0 == 0) goto L_0x0015
            r0.close()     // Catch:{ IOException -> 0x00ea }
            goto L_0x0015
        L_0x00ea:
            r0 = move-exception
            goto L_0x0015
        L_0x00ed:
            r1 = move-exception
            r7 = r1
            r1 = r0
            r0 = r7
        L_0x00f1:
            if (r1 == 0) goto L_0x00f6
            r1.close()     // Catch:{ IOException -> 0x00fa }
        L_0x00f6:
            throw r0     // Catch:{ all -> 0x00f7 }
        L_0x00f7:
            r0 = move-exception
            monitor-exit(r2)
            throw r0
        L_0x00fa:
            r1 = move-exception
            goto L_0x00f6
        L_0x00fc:
            r0 = move-exception
            goto L_0x00f1
        L_0x00fe:
            r0 = move-exception
            r0 = r1
            goto L_0x00e3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.igexin.b.a.c.a.b(java.lang.String):void");
    }

    public static void b(String str, String str2) {
        if (f806a) {
            Log.i(str, str2);
        }
    }

    public void uncaughtException(Thread thread, Throwable th) {
    }
}
