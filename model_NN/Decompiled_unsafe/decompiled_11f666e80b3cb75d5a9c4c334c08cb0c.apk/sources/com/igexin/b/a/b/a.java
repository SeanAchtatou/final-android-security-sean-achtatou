package com.igexin.b.a.b;

import java.io.OutputStream;
import org.apache.mina.proxy.handlers.http.HttpProxyConstants;

public class a extends OutputStream {

    /* renamed from: a  reason: collision with root package name */
    private OutputStream f794a = null;
    private int b = 0;
    private int c = 0;
    private int d = 0;
    private int e = 0;

    public a(OutputStream outputStream, int i) {
        this.f794a = outputStream;
        this.e = i;
    }

    /* access modifiers changed from: protected */
    public void a() {
        char c2 = '=';
        if (this.c > 0) {
            if (this.e > 0 && this.d == this.e) {
                this.f794a.write(HttpProxyConstants.CRLF.getBytes());
                this.d = 0;
            }
            char charAt = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/".charAt((this.b << 8) >>> 26);
            char charAt2 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/".charAt((this.b << 14) >>> 26);
            char charAt3 = this.c < 2 ? '=' : "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/".charAt((this.b << 20) >>> 26);
            if (this.c >= 3) {
                c2 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/".charAt((this.b << 26) >>> 26);
            }
            this.f794a.write(charAt);
            this.f794a.write(charAt2);
            this.f794a.write(charAt3);
            this.f794a.write(c2);
            this.d += 4;
            this.c = 0;
            this.b = 0;
        }
    }

    public void close() {
        a();
        this.f794a.close();
    }

    public void write(int i) {
        this.b = ((i & 255) << (16 - (this.c * 8))) | this.b;
        this.c++;
        if (this.c == 3) {
            a();
        }
    }
}
