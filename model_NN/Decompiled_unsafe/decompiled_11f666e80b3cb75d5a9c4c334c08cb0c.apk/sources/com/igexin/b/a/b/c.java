package com.igexin.b.a.b;

import com.igexin.b.a.d.a.a;
import com.igexin.b.a.d.a.f;
import com.igexin.b.a.d.e;
import java.util.concurrent.TimeUnit;

public class c extends e {

    /* renamed from: a  reason: collision with root package name */
    static c f804a;
    public volatile long b;
    public volatile long c;
    public volatile long d;
    public volatile long e;
    a<String, Integer, b, e> f;
    private byte[] v;
    private byte[] w;

    public static c b() {
        if (f804a == null) {
            f804a = new c();
        }
        return f804a;
    }

    public static void d() {
        f804a.b = 0;
        f804a.d = 0;
        f804a.c = 0;
        f804a.e = 0;
    }

    public e a(String str, int i, b bVar, Object obj, boolean z) {
        return a(str, i, bVar, obj, z, -1, -1, (byte) 0, null, null);
    }

    public e a(String str, int i, b bVar, Object obj, boolean z, int i2, long j, byte b2, Object obj2, com.igexin.b.a.d.a.c cVar) {
        return a(str, i, bVar, obj, z, i2, j, b2, obj2, cVar, 0, null);
    }

    public e a(String str, int i, b bVar, Object obj, boolean z, int i2, long j, byte b2, Object obj2, com.igexin.b.a.d.a.c cVar, int i3, f fVar) {
        e a2;
        if (this.f == null || (a2 = this.f.a(str, Integer.valueOf(i), bVar)) == null || a2.r()) {
            return null;
        }
        if (fVar != null) {
            a2.a(i3, fVar);
        }
        a(a2, obj, z, i2, j, b2, obj2, cVar);
        return a2;
    }

    public e a(String str, int i, b bVar, Object obj, boolean z, int i2, f fVar) {
        return a(str, i, bVar, obj, z, -1, -1, (byte) 0, null, null, i2, fVar);
    }

    public void a(a<String, Integer, b, e> aVar) {
        this.f = aVar;
    }

    public void a(byte[] bArr) {
        this.v = bArr;
        this.w = com.igexin.b.b.a.a(bArr);
        if (this.w != null) {
        }
    }

    /* access modifiers changed from: package-private */
    public boolean a(e eVar, Object obj, boolean z, int i, long j, byte b2, Object obj2, com.igexin.b.a.d.a.c cVar) {
        eVar.c = obj;
        eVar.a(j, TimeUnit.MILLISECONDS);
        eVar.x = i;
        eVar.a(b2);
        eVar.C = obj2;
        eVar.a(cVar);
        return a(eVar, z);
    }

    public byte[] a() {
        return this.w;
    }

    public final void c() {
        f();
    }
}
