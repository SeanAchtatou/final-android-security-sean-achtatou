package com.igexin.b.a.d;

import com.igexin.b.a.d.a.c;
import com.igexin.b.a.d.a.f;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public abstract class d extends a {
    protected static e E;
    public int A;
    public Exception B;
    public Object C;
    public f D;
    protected final ReentrantLock F;
    protected final Condition G;
    Thread H;
    protected volatile boolean I;
    int J;
    protected c K;

    /* renamed from: a  reason: collision with root package name */
    private byte f811a;
    protected volatile boolean k;
    protected volatile boolean m;
    protected volatile boolean n;
    protected volatile boolean o;
    protected volatile boolean p;
    protected volatile boolean q;
    protected volatile boolean r;
    protected volatile boolean s;
    protected volatile boolean t;
    protected volatile long u;
    volatile int v;
    public long w;
    public int x;
    public int y;
    public int z;

    public d(int i) {
        this(i, null);
    }

    public d(int i, c cVar) {
        this.z = i;
        this.K = cVar;
        this.F = new ReentrantLock();
        this.G = this.F.newCondition();
    }

    public final int a(long j, TimeUnit timeUnit) {
        if (j <= 0) {
            return 0;
        }
        switch (E.k.a(this, j, timeUnit)) {
            case -2:
                return -2;
            case -1:
                this.u = System.currentTimeMillis() + TimeUnit.MILLISECONDS.convert(j, timeUnit);
                return -1;
            case 0:
            default:
                return 0;
            case 1:
                return 1;
        }
    }

    public long a(TimeUnit timeUnit) {
        return timeUnit.convert(o(), TimeUnit.MILLISECONDS);
    }

    public final void a(int i) {
        this.f811a = (byte) (this.f811a & 15);
        this.f811a = (byte) (this.f811a | ((i & 15) << 4));
    }

    public final void a(int i, f fVar) {
        if (i < 0) {
            throw new IllegalArgumentException("second must > 0");
        }
        this.y = i;
        this.D = fVar;
    }

    public final void a(c cVar) {
        this.K = cVar;
    }

    public void a_() {
        this.H = Thread.currentThread();
        this.p = true;
    }

    public final void b(long j) {
        this.w = j;
    }

    public void c() {
        if (this.k || this.m) {
            f();
        }
    }

    public void d() {
        this.s = true;
    }

    /* access modifiers changed from: protected */
    public abstract void e();

    public void f() {
        this.C = null;
        this.B = null;
        this.H = null;
    }

    /* access modifiers changed from: package-private */
    public final void n() {
        this.J++;
        this.J &= 1090519038;
    }

    /* access modifiers changed from: package-private */
    public long o() {
        return this.u - System.currentTimeMillis();
    }

    public final void p() {
        this.k = true;
    }

    public final boolean q() {
        return this.n;
    }

    public final boolean r() {
        return this.m;
    }

    public final boolean s() {
        return this.k;
    }

    /* access modifiers changed from: protected */
    public void t() {
    }

    public final void u() {
        this.m = true;
    }

    /* access modifiers changed from: protected */
    public void v() {
        if (!this.o && !this.q && !this.r) {
            this.k = true;
            this.p = false;
        } else if (this.q && !this.k) {
            this.p = false;
        } else if (this.o && !this.n && !this.k) {
            this.p = false;
        }
    }

    /* access modifiers changed from: protected */
    public void w() {
        if (this.K != null) {
            this.K.a(com.igexin.b.a.d.a.d.f808a);
        }
    }
}
