package com.igexin.assist.action;

import android.text.TextUtils;
import com.igexin.assist.MessageBean;
import com.igexin.assist.sdk.AssistPushConsts;

class b extends Thread {

    /* renamed from: a  reason: collision with root package name */
    MessageBean f785a;
    final /* synthetic */ MessageManger b;

    b(MessageManger messageManger, MessageBean messageBean) {
        this.b = messageManger;
        this.f785a = messageBean;
    }

    public void run() {
        try {
            if (this.f785a == null) {
                return;
            }
            if (this.f785a.getMessageType().equals(AssistPushConsts.MSG_TYPE_TOKEN)) {
                this.b.a(this.f785a.getStringMessage());
            } else if (this.f785a.getMessageType().equals(AssistPushConsts.MSG_TYPE_PAYLOAD) && !TextUtils.isEmpty(this.f785a.getStringMessage())) {
                d dVar = new d();
                dVar.a(this.f785a);
                if (dVar.a() && dVar.f().equals(AssistPushConsts.MSG_VALUE_PAYLOAD)) {
                    this.b.a(dVar, this.f785a.getContext());
                }
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }
}
