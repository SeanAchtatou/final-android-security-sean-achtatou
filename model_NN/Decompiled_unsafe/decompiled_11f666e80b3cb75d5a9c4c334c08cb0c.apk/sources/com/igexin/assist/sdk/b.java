package com.igexin.assist.sdk;

import android.content.Context;
import com.igexin.assist.control.AbstractPushManager;

public class b {

    /* renamed from: a  reason: collision with root package name */
    private AbstractPushManager f792a;

    private b() {
    }

    public static b a() {
        return d.f793a;
    }

    public void a(Context context) {
        this.f792a = a.a(context);
    }

    public void a(Context context, int i, int i2) {
        if (this.f792a != null) {
            this.f792a.setSilentTime(context, i, i2);
        }
    }

    public void b(Context context) {
        if (this.f792a != null) {
            this.f792a.register(context);
        }
    }

    public void c(Context context) {
        if (this.f792a != null) {
            this.f792a.turnOnPush(context);
        }
    }

    public void d(Context context) {
        if (this.f792a != null) {
            this.f792a.turnOffPush(context);
        }
    }
}
