package com.igexin.push.core.a;

import com.igexin.b.a.c.a;
import com.igexin.push.core.b.d;
import org.json.JSONObject;

public class r extends b {
    public boolean a(Object obj, JSONObject jSONObject) {
        try {
            if (!jSONObject.has("action") || !jSONObject.getString("action").equals("received")) {
                return true;
            }
            String string = jSONObject.getString("id");
            a.b("ReceivedAction received, cmd id :" + string);
            try {
                if (!d.a().a(Long.parseLong(string))) {
                    return true;
                }
                e.a().h();
                return true;
            } catch (NumberFormatException e) {
                a.b("ReceivedAction|" + e.toString());
                return true;
            }
        } catch (Exception e2) {
            a.b("ReceivedAction|" + e2.toString());
            return true;
        }
    }
}
