package com.igexin.push.core.b;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import com.igexin.b.a.c.a;
import org.json.JSONObject;

public class c {

    /* renamed from: a  reason: collision with root package name */
    private SharedPreferences f918a;

    public c(Context context) {
        if (context != null) {
            this.f918a = context.getSharedPreferences("gx_msg_sp", 0);
        }
    }

    private JSONObject b() {
        try {
            if (this.f918a != null) {
                String string = this.f918a.getString("taskIdList", "");
                if (!TextUtils.isEmpty(string)) {
                    return new JSONObject(string);
                }
            }
        } catch (Throwable th) {
        }
        return null;
    }

    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void a() {
        /*
            r14 = this;
            monitor-enter(r14)
            org.json.JSONObject r8 = r14.b()     // Catch:{ Throwable -> 0x0066, all -> 0x0063 }
            android.content.SharedPreferences r0 = r14.f918a     // Catch:{ Throwable -> 0x0066, all -> 0x0063 }
            if (r0 == 0) goto L_0x0061
            if (r8 == 0) goto L_0x0061
            int r0 = r8.length()     // Catch:{ Throwable -> 0x0066, all -> 0x0063 }
            r1 = 150(0x96, float:2.1E-43)
            if (r0 <= r1) goto L_0x0061
            r1 = 0
            r2 = 9223372036854775807(0x7fffffffffffffff, double:NaN)
            r0 = 0
            java.util.Iterator r9 = r8.keys()     // Catch:{ Throwable -> 0x0066, all -> 0x0063 }
            r4 = r2
            r2 = r0
        L_0x0020:
            boolean r0 = r9.hasNext()     // Catch:{ Throwable -> 0x0066, all -> 0x0063 }
            if (r0 == 0) goto L_0x0048
            java.lang.Object r0 = r9.next()     // Catch:{ Throwable -> 0x0066, all -> 0x0063 }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ Throwable -> 0x0066, all -> 0x0063 }
            long r6 = r8.getLong(r0)     // Catch:{ Throwable -> 0x0066, all -> 0x0063 }
            int r3 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r3 <= 0) goto L_0x0036
            r2 = r0
            r4 = r6
        L_0x0036:
            long r10 = java.lang.System.currentTimeMillis()     // Catch:{ Throwable -> 0x0066, all -> 0x0063 }
            r12 = 432000000(0x19bfcc00, double:2.13436359E-315)
            long r10 = r10 - r12
            int r0 = (r6 > r10 ? 1 : (r6 == r10 ? 0 : -1))
            if (r0 >= 0) goto L_0x0068
            r9.remove()     // Catch:{ Throwable -> 0x0066, all -> 0x0063 }
            r0 = 1
        L_0x0046:
            r1 = r0
            goto L_0x0020
        L_0x0048:
            if (r1 != 0) goto L_0x004f
            if (r2 == 0) goto L_0x004f
            r8.remove(r2)     // Catch:{ Throwable -> 0x0066, all -> 0x0063 }
        L_0x004f:
            android.content.SharedPreferences r0 = r14.f918a     // Catch:{ Throwable -> 0x0066, all -> 0x0063 }
            android.content.SharedPreferences$Editor r0 = r0.edit()     // Catch:{ Throwable -> 0x0066, all -> 0x0063 }
            java.lang.String r1 = "taskIdList"
            java.lang.String r2 = r8.toString()     // Catch:{ Throwable -> 0x0066, all -> 0x0063 }
            r0.putString(r1, r2)     // Catch:{ Throwable -> 0x0066, all -> 0x0063 }
            r0.apply()     // Catch:{ Throwable -> 0x0066, all -> 0x0063 }
        L_0x0061:
            monitor-exit(r14)
            return
        L_0x0063:
            r0 = move-exception
            monitor-exit(r14)
            throw r0
        L_0x0066:
            r0 = move-exception
            goto L_0x0061
        L_0x0068:
            r0 = r1
            goto L_0x0046
        */
        throw new UnsupportedOperationException("Method not decompiled: com.igexin.push.core.b.c.a():void");
    }

    public boolean a(String str) {
        try {
            JSONObject b = b();
            if (b != null && b.has(str)) {
                a.b("sp task " + str + "already Exists");
                return true;
            }
        } catch (Throwable th) {
        }
        return false;
    }

    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void b(java.lang.String r5) {
        /*
            r4 = this;
            monitor-enter(r4)
            org.json.JSONObject r0 = r4.b()     // Catch:{ Throwable -> 0x002e, all -> 0x002b }
            android.content.SharedPreferences r1 = r4.f918a     // Catch:{ Throwable -> 0x002e, all -> 0x002b }
            if (r1 == 0) goto L_0x0029
            if (r0 != 0) goto L_0x0010
            org.json.JSONObject r0 = new org.json.JSONObject     // Catch:{ Throwable -> 0x002e, all -> 0x002b }
            r0.<init>()     // Catch:{ Throwable -> 0x002e, all -> 0x002b }
        L_0x0010:
            long r2 = java.lang.System.currentTimeMillis()     // Catch:{ Throwable -> 0x002e, all -> 0x002b }
            r0.put(r5, r2)     // Catch:{ Throwable -> 0x002e, all -> 0x002b }
            android.content.SharedPreferences r1 = r4.f918a     // Catch:{ Throwable -> 0x002e, all -> 0x002b }
            android.content.SharedPreferences$Editor r1 = r1.edit()     // Catch:{ Throwable -> 0x002e, all -> 0x002b }
            java.lang.String r2 = "taskIdList"
            java.lang.String r0 = r0.toString()     // Catch:{ Throwable -> 0x002e, all -> 0x002b }
            r1.putString(r2, r0)     // Catch:{ Throwable -> 0x002e, all -> 0x002b }
            r1.apply()     // Catch:{ Throwable -> 0x002e, all -> 0x002b }
        L_0x0029:
            monitor-exit(r4)
            return
        L_0x002b:
            r0 = move-exception
            monitor-exit(r4)
            throw r0
        L_0x002e:
            r0 = move-exception
            goto L_0x0029
        */
        throw new UnsupportedOperationException("Method not decompiled: com.igexin.push.core.b.c.b(java.lang.String):void");
    }
}
