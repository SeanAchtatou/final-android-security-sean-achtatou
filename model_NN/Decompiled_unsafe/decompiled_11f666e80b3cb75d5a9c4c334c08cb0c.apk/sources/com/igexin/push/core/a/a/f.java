package com.igexin.push.core.a.a;

import com.igexin.b.a.c.a;
import com.igexin.push.core.a.e;
import com.igexin.push.core.b;
import com.igexin.push.core.bean.BaseAction;
import com.igexin.push.core.bean.PushTaskBean;
import com.igexin.push.core.g;
import org.json.JSONException;
import org.json.JSONObject;

public class f implements a {
    public b a(PushTaskBean pushTaskBean, BaseAction baseAction) {
        return b.success;
    }

    public BaseAction a(JSONObject jSONObject) {
        try {
            BaseAction baseAction = new BaseAction();
            baseAction.setType("null");
            baseAction.setActionId(jSONObject.getString("actionid"));
            return baseAction;
        } catch (JSONException e) {
            return null;
        }
    }

    public boolean b(PushTaskBean pushTaskBean, BaseAction baseAction) {
        String c = e.a().c(pushTaskBean.getTaskId(), pushTaskBean.getMessageId());
        a.b("EndAction execute, remove pushMessage from pushMessageMap, key = " + c);
        try {
            g.ai.remove(c);
            return true;
        } catch (Exception e) {
            a.b("EndAction|" + e.toString());
            return true;
        }
    }
}
