package com.igexin.push.core.bean;

public class f {

    /* renamed from: a  reason: collision with root package name */
    private int f949a;
    private String b;
    private String c;
    private String d;
    private String e;
    private String f;
    private boolean g;
    private long h;
    private long i = 0;
    private String j;

    public int a() {
        return this.f949a;
    }

    public void a(int i2) {
        this.f949a = i2;
    }

    public void a(long j2) {
        this.h = j2;
    }

    public void a(String str) {
        this.b = str;
    }

    public void a(boolean z) {
        this.g = z;
    }

    public String b() {
        return this.b;
    }

    public void b(long j2) {
        this.i = j2;
    }

    public void b(String str) {
        this.c = str;
    }

    public String c() {
        return this.c;
    }

    public void c(String str) {
        this.d = str;
    }

    public String d() {
        return this.d;
    }

    public void d(String str) {
        this.e = str;
    }

    public String e() {
        return this.e;
    }

    public void e(String str) {
        this.f = str;
    }

    public String f() {
        return this.f;
    }

    public void f(String str) {
        this.j = str;
    }

    public boolean g() {
        return this.g;
    }

    public long h() {
        return this.h;
    }

    public long i() {
        return this.i;
    }

    public String j() {
        return this.j;
    }
}
