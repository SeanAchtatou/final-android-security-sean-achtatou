package com.igexin.push.core;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import com.igexin.b.a.c.a;
import com.igexin.push.config.m;

public class i {
    private static i e;

    /* renamed from: a  reason: collision with root package name */
    public long f975a = 240000;
    private l b = l.DETECT;
    private long c = 0;
    private ConnectivityManager d = f.a().j();

    private i() {
    }

    public static i a() {
        if (e == null) {
            e = new i();
        }
        return e;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(long, long):long}
     arg types: [long, int]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(float, float):float}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(long, long):long} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(long, long):long}
     arg types: [long, int]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(float, float):float}
      ClspMth{java.lang.Math.max(long, long):long} */
    private void b(k kVar) {
        switch (j.b[kVar.ordinal()]) {
            case 1:
                a(Math.min(this.f975a + 60000, 420000L));
                a(l.DETECT);
                return;
            case 2:
            case 3:
                this.c++;
                if (this.c >= 2) {
                    a(Math.max(this.f975a - 60000, 240000L));
                    a(l.STABLE);
                    return;
                }
                return;
            case 4:
                a(240000);
                a(l.DETECT);
                return;
            default:
                return;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(long, long):long}
     arg types: [long, int]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(float, float):float}
      ClspMth{java.lang.Math.max(long, long):long} */
    private void c(k kVar) {
        switch (j.b[kVar.ordinal()]) {
            case 1:
                a(l.STABLE);
                return;
            case 2:
            case 3:
                a(Math.max(this.f975a - 60000, 240000L));
                this.c++;
                if (this.c >= 2) {
                    a(240000);
                    a(l.PENDING);
                    return;
                }
                return;
            case 4:
                a(240000);
                a(l.DETECT);
                return;
            default:
                return;
        }
    }

    private void d(k kVar) {
        switch (j.b[kVar.ordinal()]) {
            case 1:
                a(240000);
                a(l.DETECT);
                return;
            case 2:
            case 3:
                a(l.PENDING);
                return;
            case 4:
                a(240000);
                a(l.DETECT);
                return;
            default:
                return;
        }
    }

    public void a(long j) {
        this.f975a = j;
    }

    public void a(k kVar) {
        switch (j.f976a[this.b.ordinal()]) {
            case 1:
                b(kVar);
                return;
            case 2:
                c(kVar);
                return;
            case 3:
                d(kVar);
                return;
            default:
                return;
        }
    }

    public void a(l lVar) {
        this.b = lVar;
        this.c = 0;
    }

    public long b() {
        long j = this.f975a;
        if (m.d > 0) {
            j = (long) (m.d * 1000);
        }
        NetworkInfo activeNetworkInfo = this.d.getActiveNetworkInfo();
        if (activeNetworkInfo == null || !activeNetworkInfo.isAvailable()) {
            j = 3600000;
        } else if (!g.l) {
            j = 3600000;
        } else if (!f.a().g().a()) {
            j = 3600000;
        }
        a.b("HeartBeatIntervalGenerator|getHeartbeatInterval final interval = " + j);
        return j;
    }
}
