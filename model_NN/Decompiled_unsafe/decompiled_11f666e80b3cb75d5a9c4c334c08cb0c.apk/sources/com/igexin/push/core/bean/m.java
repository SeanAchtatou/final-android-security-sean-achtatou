package com.igexin.push.core.bean;

import com.igexin.push.core.f;
import com.igexin.push.core.g;

public class m extends BaseAction {

    /* renamed from: a  reason: collision with root package name */
    private String f956a;
    private boolean b;
    private boolean c;
    private String d;

    public String a() {
        return this.f956a;
    }

    public void a(String str) {
        this.f956a = str;
    }

    public void a(boolean z) {
        this.b = z;
    }

    public String b() {
        return this.d;
    }

    public void b(String str) {
        this.d = str;
    }

    public void b(boolean z) {
        this.c = z;
    }

    public String c() {
        String m;
        String str = this.f956a;
        if (this.b) {
            str = str.indexOf("?") > 0 ? str + "&cid=" + g.r : str + "?cid=" + g.r;
        }
        return (!this.c || (m = f.a().m()) == null) ? str : str.indexOf("?") > 0 ? str + "&nettype=" + m : str + "?nettype=" + m;
    }
}
