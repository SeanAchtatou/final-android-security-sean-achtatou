package com.igexin.push.core.bean;

import java.util.List;
import java.util.Map;

public class PushTaskBean {

    /* renamed from: a  reason: collision with root package name */
    private String f943a;
    private String b;
    private String c;
    private String d;
    private String e;
    private List<BaseAction> f;
    private byte[] g;
    private String h;
    private String i;
    private int j;
    private int k;
    private boolean l = false;
    private boolean m = false;
    private boolean n = false;
    private Map<String, String> o;
    private int p;
    private int q;

    public String getAction() {
        return this.f943a;
    }

    public List<BaseAction> getActionChains() {
        return this.f;
    }

    public String getAppKey() {
        return this.i;
    }

    public String getAppid() {
        return this.b;
    }

    public BaseAction getBaseAction(String str) {
        for (BaseAction next : getActionChains()) {
            if (next.getActionId().equals(str)) {
                return next;
            }
        }
        return null;
    }

    public Map<String, String> getConditionMap() {
        return this.o;
    }

    public int getCurrentActionid() {
        return this.j;
    }

    public int getExecuteTimes() {
        return this.q;
    }

    public String getId() {
        return this.c;
    }

    public String getMessageId() {
        return this.d;
    }

    public String getMsgAddress() {
        return this.h;
    }

    public byte[] getMsgExtra() {
        return this.g;
    }

    public int getPerActionid() {
        return this.k;
    }

    public int getStatus() {
        return this.p;
    }

    public String getTaskId() {
        return this.e;
    }

    public boolean isCDNType() {
        return this.n;
    }

    public boolean isHttpImg() {
        return this.l;
    }

    public boolean isStop() {
        return this.m;
    }

    public void setAction(String str) {
        this.f943a = str;
    }

    public void setActionChains(List<BaseAction> list) {
        this.f = list;
    }

    public void setAppKey(String str) {
        this.i = str;
    }

    public void setAppid(String str) {
        this.b = str;
    }

    public void setCDNType(boolean z) {
        this.n = z;
    }

    public void setConditionMap(Map<String, String> map) {
        this.o = map;
    }

    public void setCurrentActionid(int i2) {
        this.j = i2;
    }

    public void setExecuteTimes(int i2) {
        this.q = i2;
    }

    public void setHttpImg(boolean z) {
        this.l = z;
    }

    public void setId(String str) {
        this.c = str;
    }

    public void setMessageId(String str) {
        this.d = str;
    }

    public void setMsgAddress(String str) {
        this.h = str;
    }

    public void setMsgExtra(byte[] bArr) {
        this.g = bArr;
    }

    public void setPerActionid(int i2) {
        this.k = i2;
    }

    public void setStatus(int i2) {
        this.p = i2;
    }

    public void setStop(boolean z) {
        this.m = z;
    }

    public void setTaskId(String str) {
        this.e = str;
    }
}
