package com.igexin.push.core.c;

import android.os.Message;
import cn.banshenggua.aichang.room.message.SocketMessage;
import com.baidu.mobads.interfaces.IXAdRequestInfo;
import com.igexin.b.a.b.c;
import com.igexin.b.a.c.a;
import com.igexin.push.config.m;
import com.igexin.push.core.a.e;
import com.igexin.push.core.f;
import com.igexin.push.f.a.b;
import com.igexin.push.util.i;
import com.igexin.push.util.q;
import com.igexin.sdk.PushBuildConfig;
import org.json.JSONObject;

public class g extends b {

    /* renamed from: a  reason: collision with root package name */
    public static final String f964a = g.class.getName();

    public g(String str) {
        super(str);
        a();
    }

    private void c(byte[] bArr) {
        String string;
        String string2;
        String string3;
        com.igexin.push.core.bean.g a2;
        boolean z = false;
        try {
            JSONObject jSONObject = new JSONObject(new String(bArr));
            a.b(f964a + "|parse sdk config from server resp = " + jSONObject);
            com.igexin.push.core.b.g.a().e(System.currentTimeMillis());
            if (jSONObject.has(SocketMessage.MSG_RESULE_KEY) && "ok".equals(jSONObject.getString(SocketMessage.MSG_RESULE_KEY)) && jSONObject.has("config")) {
                if (jSONObject.has(SocketMessage.MSG_FINGER_KEY)) {
                    m.F = jSONObject.getString(SocketMessage.MSG_FINGER_KEY);
                }
                JSONObject jSONObject2 = new JSONObject(jSONObject.getString("config"));
                if (jSONObject2.has("sdk.uploadapplist.enable")) {
                    String string4 = jSONObject2.getString("sdk.uploadapplist.enable");
                    if (string4.equals("true") || string4.equals("false")) {
                        m.h = Boolean.valueOf(string4).booleanValue();
                    }
                }
                if (jSONObject2.has("sdk.feature.sendmessage.enable")) {
                    String string5 = jSONObject2.getString("sdk.feature.sendmessage.enable");
                    if (string5.equals("true") || string5.equals("false")) {
                        m.i = Boolean.valueOf(string5).booleanValue();
                    }
                }
                if (jSONObject2.has("sdk.readlocalcell.enable")) {
                    String string6 = jSONObject2.getString("sdk.readlocalcell.enable");
                    if (string6.equals("true") || string6.equals("false")) {
                        m.g = Boolean.valueOf(string6).booleanValue();
                    }
                }
                if (jSONObject2.has("sdk.snl.enable")) {
                    String string7 = jSONObject2.getString("sdk.snl.enable");
                    if (string7.equals("true") || string7.equals("false")) {
                        m.n = Boolean.valueOf(string7).booleanValue();
                    }
                }
                if (jSONObject2.has("sdk.domainbackup.enable")) {
                    String string8 = jSONObject2.getString("sdk.domainbackup.enable");
                    if (string8.equals("true") || string8.equals("false")) {
                        m.f = Boolean.valueOf(string8).booleanValue();
                    }
                }
                if (jSONObject2.has("sdk.feature.setsilenttime.enable")) {
                    String string9 = jSONObject2.getString("sdk.feature.setsilenttime.enable");
                    if (string9.equals("true") || string9.equals("false")) {
                        m.k = Boolean.valueOf(string9).booleanValue();
                        if (!m.k && m.b != 0) {
                            e.a().a(12, 0, "server");
                        }
                    }
                }
                if (jSONObject2.has("sdk.snl.maxactiveflow")) {
                    try {
                        m.o = Long.parseLong(jSONObject2.getString("sdk.snl.maxactiveflow"));
                    } catch (Exception e) {
                    }
                }
                if (jSONObject2.has("sdk.feature.settag.enable")) {
                    String string10 = jSONObject2.getString("sdk.feature.settag.enable");
                    if (string10.equals("true") || string10.equals("false")) {
                        m.j = Boolean.valueOf(string10).booleanValue();
                    }
                }
                if (jSONObject2.has("sdk.feature.setheartbeatinterval.enable")) {
                    String string11 = jSONObject2.getString("sdk.feature.setheartbeatinterval.enable");
                    if (string11.equals("true") || string11.equals("false")) {
                        m.l = Boolean.valueOf(string11).booleanValue();
                    }
                }
                if (jSONObject2.has("sdk.feature.setsockettimeout.enable")) {
                    String string12 = jSONObject2.getString("sdk.feature.setsockettimeout.enable");
                    if (string12.equals("true") || string12.equals("false")) {
                        m.m = Boolean.valueOf(string12).booleanValue();
                    }
                }
                if (jSONObject2.has("sdk.guard.enable")) {
                    String string13 = jSONObject2.getString("sdk.guard.enable");
                    if (string13.equals("true") || string13.equals("false")) {
                        m.p = Boolean.valueOf(string13).booleanValue();
                    }
                }
                if (jSONObject2.has("sdk.needlook.enable")) {
                    String string14 = jSONObject2.getString("sdk.needlook.enable");
                    if (string14.equals("true") || string14.equals("false")) {
                        m.w = Boolean.valueOf(string14).booleanValue();
                        d(string14.getBytes());
                    }
                }
                if (jSONObject2.has("sdk.report.initialize.enable")) {
                    String string15 = jSONObject2.getString("sdk.report.initialize.enable");
                    if (string15.equals("true") || string15.equals("false")) {
                        m.x = Boolean.valueOf(string15).booleanValue();
                    }
                }
                if (jSONObject2.has("sdk.wakeupsdk.enable")) {
                    String string16 = jSONObject2.getString("sdk.wakeupsdk.enable");
                    if (string16.equals("true") || string16.equals("false")) {
                        m.q = Boolean.valueOf(string16).booleanValue();
                    }
                }
                if (jSONObject2.has("sdk.feature.feedback.enable")) {
                    String string17 = jSONObject2.getString("sdk.feature.feedback.enable");
                    if (string17.equals("true") || string17.equals("false")) {
                        m.r = Boolean.valueOf(string17).booleanValue();
                    }
                }
                if (jSONObject2.has("sdk.watchout.app")) {
                    m.t = jSONObject2.getString("sdk.watchout.app");
                }
                if (jSONObject2.has("sdk.watchout.service")) {
                    m.u = jSONObject2.getString("sdk.watchout.service");
                }
                if (jSONObject2.has("sdk.daemon.enable")) {
                    String string18 = jSONObject2.getString("sdk.daemon.enable");
                    if (string18.equals("true") || string18.equals("false")) {
                        m.v = Boolean.valueOf(string18).booleanValue();
                    }
                }
                if (jSONObject2.has("sdk.guardactivity.first")) {
                    String string19 = jSONObject2.getString("sdk.guardactivity.first");
                    if (string19.equals("true") || string19.equals("false")) {
                        m.G = Boolean.valueOf(string19).booleanValue();
                    }
                }
                if (jSONObject2.has("sdk.polling.dis.cnt")) {
                    m.H = Integer.valueOf(jSONObject2.getString("sdk.polling.dis.cnt")).intValue();
                }
                if (jSONObject2.has("sdk.polling.login.interval")) {
                    m.I = (long) (Integer.valueOf(jSONObject2.getString("sdk.polling.login.interval")).intValue() * 1000);
                }
                if (jSONObject2.has("sdk.polling.exit.heartbeat.cnt")) {
                    m.J = Integer.valueOf(jSONObject2.getString("sdk.polling.exit.heartbeat.cnt")).intValue();
                }
                if (jSONObject2.has("sdk.reset.reconnect.delay")) {
                    try {
                        m.y = Long.parseLong(jSONObject2.getString("sdk.reset.reconnect.delay")) * 1000;
                    } catch (Exception e2) {
                    }
                }
                if (jSONObject2.has("sdk.guard.maxcnt")) {
                    try {
                        m.E = Integer.parseInt(jSONObject2.getString("sdk.guard.maxcnt"));
                    } catch (Exception e3) {
                    }
                }
                if (jSONObject2.has("ext_infos") && (string3 = jSONObject2.getString("ext_infos")) != null && !"".equals(string3)) {
                    JSONObject jSONObject3 = new JSONObject(string3);
                    if (jSONObject3.has("version")) {
                        String string20 = jSONObject3.getString("version");
                        if (m.s == null || !string20.equals(m.s.a())) {
                            z = true;
                        }
                        if (z && (a2 = e.a().a(jSONObject3)) != null) {
                            Message obtain = Message.obtain();
                            obtain.what = com.igexin.push.core.a.h;
                            obtain.obj = a2;
                            f.a().a(obtain);
                        }
                    }
                }
                if (jSONObject2.has("sdk.httpdata.maxsize")) {
                    m.K = Integer.valueOf(jSONObject2.getString("sdk.httpdata.maxsize")).intValue();
                }
                if (jSONObject2.has("sdk.hide.righticon.blacklist")) {
                    m.L = jSONObject2.getString("sdk.hide.righticon.blacklist");
                }
                if (jSONObject2.has("sdk.miuipush.enable")) {
                    String string21 = jSONObject2.getString("sdk.miuipush.enable");
                    if (string21.equals("true") || string21.equals("false")) {
                        m.M = Boolean.valueOf(string21).booleanValue();
                    }
                }
                if (jSONObject2.has("sdk.flymepush.enable")) {
                    String string22 = jSONObject2.getString("sdk.flymepush.enable");
                    if (string22.equals("true") || string22.equals("false")) {
                        m.N = Boolean.valueOf(string22).booleanValue();
                    }
                }
                if (jSONObject2.has("sdk.hmspush.enable")) {
                    String string23 = jSONObject2.getString("sdk.hmspush.enable");
                    if (string23.equals("true") || string23.equals("false")) {
                        m.O = Boolean.valueOf(string23).booleanValue();
                    }
                }
                if (jSONObject2.has("sdk.dms.conf") && (string2 = jSONObject2.getString("sdk.dms.conf")) != null) {
                    q.a(com.igexin.push.core.g.f, "dmsconf", new String(i.c(com.igexin.b.a.a.a.d(string2.getBytes("UTF-8"), "dj1om0z0za9kwzxrphkqxsu9oc21tez1"), 0)));
                }
                if (jSONObject2.has("sdk.dms.conf.account") && (string = jSONObject2.getString("sdk.dms.conf.account")) != null) {
                    q.a(com.igexin.push.core.g.f, "dms_act", new String(i.c(com.igexin.b.a.a.a.d(string.getBytes("UTF-8"), "dj1om0z0za9kwzxrphkqxsu9oc21tez1"), 0)));
                }
                com.igexin.push.config.a.a().f();
            }
        } catch (Throwable th) {
            a.b(f964a + "|" + th.toString());
            e.a().c(th.toString());
        }
    }

    private void d(byte[] bArr) {
        try {
            c.b().a(new h(this, bArr), false, true);
        } catch (Exception e) {
        }
    }

    public void a() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("action", "sdkconfig");
            jSONObject.put(IXAdRequestInfo.CELL_ID, com.igexin.push.core.g.r);
            jSONObject.put("appid", com.igexin.push.core.g.f974a);
            jSONObject.put("sdk_version", PushBuildConfig.sdk_conf_version);
            jSONObject.put(SocketMessage.MSG_FINGER_KEY, m.F);
            b(jSONObject.toString().getBytes());
        } catch (Exception e) {
        }
    }

    public void a(byte[] bArr) {
        if (bArr != null) {
            c(bArr);
        }
    }

    public int b() {
        return 0;
    }
}
