package com.igexin.push.core.c;

import cn.banshenggua.aichang.utils.StringUtil;
import com.igexin.push.core.a.e;
import com.igexin.push.core.bean.PushTaskBean;
import com.igexin.push.core.f;
import com.igexin.push.core.g;
import com.igexin.push.d.c.a;
import com.igexin.push.f.a.b;
import com.igexin.push.util.r;
import java.util.Timer;
import org.json.JSONObject;

public class c extends b {

    /* renamed from: a  reason: collision with root package name */
    private String f960a;
    /* access modifiers changed from: private */
    public String g;
    /* access modifiers changed from: private */
    public a h;
    /* access modifiers changed from: private */
    public PushTaskBean i;

    public c(String str, a aVar, PushTaskBean pushTaskBean) {
        super(str);
        this.g = str;
        this.f960a = pushTaskBean.getMessageId();
        this.h = aVar;
        this.i = pushTaskBean;
    }

    /* access modifiers changed from: protected */
    public void a(PushTaskBean pushTaskBean, a aVar) {
        com.igexin.push.d.c.c cVar = new com.igexin.push.d.c.c();
        cVar.a();
        cVar.c = "RTV" + pushTaskBean.getMessageId() + "@" + pushTaskBean.getTaskId();
        cVar.d = g.r;
        cVar.f990a = (int) System.currentTimeMillis();
        f.a().g().a("C-" + g.r, cVar);
        com.igexin.b.a.c.a.b("cdnRetrieve|" + pushTaskBean.getMessageId() + "|" + pushTaskBean.getTaskId());
        if (aVar.c() < 2) {
            long a2 = r.a();
            Timer timer = new Timer();
            timer.schedule(new e(this, pushTaskBean, aVar), a2);
            g.ak.put(pushTaskBean.getTaskId(), timer);
        }
    }

    public void a(Exception exc) {
        if (this.h.a() < 2) {
            new Timer().schedule(new d(this), r.a());
            return;
        }
        a(this.i, this.h);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{org.json.JSONObject.put(java.lang.String, double):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, long):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, int):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, java.lang.Object):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.igexin.push.core.a.e.a(org.json.JSONObject, byte[], boolean):boolean
     arg types: [org.json.JSONObject, byte[], int]
     candidates:
      com.igexin.push.core.a.e.a(int, java.lang.String, java.lang.String):void
      com.igexin.push.core.a.e.a(com.igexin.push.core.a.e, java.lang.String, java.lang.String):void
      com.igexin.push.core.a.e.a(int, int, java.lang.String):void
      com.igexin.push.core.a.e.a(com.igexin.push.core.bean.PushTaskBean, java.lang.String, java.lang.String):void
      com.igexin.push.core.a.e.a(java.lang.String, com.igexin.push.d.c.a, com.igexin.push.core.bean.PushTaskBean):void
      com.igexin.push.core.a.e.a(java.lang.String, java.lang.String, java.lang.String):boolean
      com.igexin.push.core.a.e.a(org.json.JSONObject, byte[], boolean):boolean */
    public void a(byte[] bArr) {
        if (bArr != null) {
            byte[] d = com.igexin.b.a.b.f.d(com.igexin.b.a.a.a.c(bArr, g.c));
            if (d != null) {
                JSONObject jSONObject = new JSONObject(new String(d, StringUtil.Encoding));
                jSONObject.put("id", this.f960a);
                jSONObject.put("messageid", this.f960a);
                jSONObject.put("cdnType", true);
                byte[] bArr2 = null;
                try {
                    if ("pushmessage".equals(jSONObject.getString("action"))) {
                        if (jSONObject.has("extraData")) {
                            bArr2 = jSONObject.getString("extraData").getBytes();
                        }
                        e.a().a(jSONObject, bArr2, true);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                throw new Exception("Get error CDNData, can not UnGzip it...");
            }
        }
    }

    public int b() {
        return 0;
    }
}
