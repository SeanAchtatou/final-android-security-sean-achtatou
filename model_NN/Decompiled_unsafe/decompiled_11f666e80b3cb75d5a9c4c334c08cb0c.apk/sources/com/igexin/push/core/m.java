package com.igexin.push.core;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Message;

public class m extends BroadcastReceiver {

    /* renamed from: a  reason: collision with root package name */
    private static m f979a;

    private m() {
    }

    public static m a() {
        if (f979a == null) {
            f979a = new m();
        }
        return f979a;
    }

    public void onReceive(Context context, Intent intent) {
        if (f.a() != null) {
            Message message = new Message();
            message.what = a.d;
            message.obj = intent;
            f.a().a(message);
        }
    }
}
