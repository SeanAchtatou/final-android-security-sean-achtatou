package com.igexin.push.core.d;

import android.content.Intent;
import com.igexin.push.core.g;
import com.igexin.sdk.PushActivity;
import com.tencent.open.GameAppOperation;
import java.util.HashMap;
import java.util.Map;
import org.apache.mina.proxy.handlers.http.ntlm.NTLMConstants;

public class b {
    private static b b;

    /* renamed from: a  reason: collision with root package name */
    private Map<Long, a> f971a = new HashMap();

    private b() {
    }

    public static b a() {
        if (b == null) {
            b = new b();
        }
        return b;
    }

    private void d(a aVar) {
        if (aVar != null) {
            this.f971a.put(aVar.a(), aVar);
        }
    }

    public a a(Long l) {
        return this.f971a.get(l);
    }

    public void a(a aVar) {
        if (aVar != null) {
            d(aVar);
            Intent intent = new Intent(g.f, PushActivity.class);
            intent.putExtra(GameAppOperation.SHARE_PRIZE_ACTIVITY_ID, aVar.a());
            intent.setFlags(NTLMConstants.FLAG_UNIDENTIFIED_11);
            g.f.startActivity(intent);
        }
    }

    public void b(a aVar) {
        if (aVar != null) {
            aVar.i();
            c(aVar);
        }
    }

    public void c(a aVar) {
        if (aVar != null) {
            this.f971a.remove(aVar.a());
        }
    }
}
