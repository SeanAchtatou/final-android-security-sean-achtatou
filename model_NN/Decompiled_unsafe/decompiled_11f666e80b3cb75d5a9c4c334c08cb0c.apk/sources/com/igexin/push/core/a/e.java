package com.igexin.push.core.a;

import android.annotation.TargetApi;
import android.app.NotificationManager;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ProviderInfo;
import android.content.pm.ServiceInfo;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Message;
import android.os.PowerManager;
import android.os.Process;
import android.text.TextUtils;
import android.util.Log;
import cn.banshenggua.aichang.room.message.SocketMessage;
import cn.banshenggua.aichang.utils.StringUtil;
import com.baidu.mobads.interfaces.IXAdRequestInfo;
import com.baidu.mobads.interfaces.utils.IXAdSystemUtils;
import com.igexin.assist.sdk.AssistPushConsts;
import com.igexin.push.config.SDKUrlConfig;
import com.igexin.push.config.m;
import com.igexin.push.core.a.a.a;
import com.igexin.push.core.a.a.b;
import com.igexin.push.core.a.a.c;
import com.igexin.push.core.a.a.d;
import com.igexin.push.core.a.a.f;
import com.igexin.push.core.a.a.g;
import com.igexin.push.core.a.a.h;
import com.igexin.push.core.a.a.i;
import com.igexin.push.core.a.a.j;
import com.igexin.push.core.a.a.l;
import com.igexin.push.core.b.ae;
import com.igexin.push.core.bean.BaseAction;
import com.igexin.push.core.bean.PushTaskBean;
import com.igexin.push.core.bean.n;
import com.igexin.push.d.c.o;
import com.igexin.push.d.c.q;
import com.igexin.push.e.k;
import com.igexin.push.extension.stub.IPushExtension;
import com.igexin.push.util.EncryptUtils;
import com.igexin.push.util.r;
import com.igexin.sdk.GTServiceManager;
import com.igexin.sdk.PushConsts;
import com.igexin.sdk.message.FeedbackCmdMessage;
import com.igexin.sdk.message.GTTransmitMessage;
import com.igexin.sdk.message.SetTagCmdMessage;
import com.meizu.cloud.pushsdk.pushtracer.constant.Parameters;
import com.sina.weibo.sdk.register.mobile.SelectCountryActivity;
import com.tencent.connect.common.Constants;
import com.xiaomi.mipush.sdk.MiPushClient;
import java.io.File;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.atomic.AtomicBoolean;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class e extends a implements k {

    /* renamed from: a  reason: collision with root package name */
    private static Map<Integer, a> f895a;
    private static Map<String, a> b;
    private static e c;
    private boolean d;
    private AtomicBoolean e = new AtomicBoolean(false);
    private long f;

    private e() {
        f895a = new HashMap();
        f895a.put(0, new k());
        f895a.put(5, new m());
        f895a.put(37, new p());
        f895a.put(9, new t());
        f895a.put(26, new j());
        f895a.put(28, new d());
        f895a.put(97, new l());
        b = new HashMap();
        b.put("goto", new g());
        b.put("notification", new h());
        b.put("startapp", new com.igexin.push.core.a.a.k());
        b.put("null", new f());
        b.put("wakeupsdk", new l());
        b.put("startweb", new j());
        b.put("checkapp", new b());
        b.put("cleanext", new c());
        b.put("enablelog", new com.igexin.push.core.a.a.e());
        b.put("disablelog", new d());
        b.put("reportext", new i());
    }

    private void C() {
        if (com.igexin.push.core.g.l) {
            com.igexin.push.core.g.l = false;
            com.igexin.b.a.c.a.b("CoreAction|broadcast online state = offline");
            l();
        }
        com.igexin.push.c.a e2 = com.igexin.push.c.i.a().e();
        com.igexin.push.core.i.a().a(com.igexin.push.core.k.NETWORK_ERROR);
        e2.i();
        if (B()) {
            com.igexin.b.a.c.a.b("CoreAction|sdkOn = false or pushOn = false, disconect|user");
        } else {
            com.igexin.b.a.c.a.b("CoreAction|disconnect|network");
        }
        com.igexin.b.a.b.c.b().a(com.igexin.b.a.b.a.a.k.class);
    }

    private void D() {
        boolean z = false;
        com.igexin.push.core.i.a().a(com.igexin.push.core.k.NETWORK_SWITCH);
        ConnectivityManager j = com.igexin.push.core.f.a().j();
        if (j != null) {
            NetworkInfo activeNetworkInfo = j.getActiveNetworkInfo();
            com.igexin.push.core.g.h = activeNetworkInfo != null && activeNetworkInfo.isAvailable();
            if (activeNetworkInfo != null) {
                int type = activeNetworkInfo.getType();
                com.igexin.b.a.c.a.b("CoreAction|type = " + (type == 0 ? "mobile" : type == 1 ? IXAdSystemUtils.NT_WIFI : "none"));
            }
        } else {
            com.igexin.push.core.g.h = false;
        }
        com.igexin.b.a.c.a.b("CoreAction|network changed, available = " + com.igexin.push.core.g.h);
        if (!com.igexin.push.core.g.l || !com.igexin.push.core.g.h) {
            com.igexin.push.d.b.a().b();
            com.igexin.b.a.c.a.b("CoreAction|network changed, disconnect +++");
            com.igexin.push.core.f.a().i().f();
            if (com.igexin.push.core.g.h) {
                if (!com.igexin.push.c.i.a().e().j()) {
                    com.igexin.b.a.c.a.b("CoreAction|network changed, domain = backup or trynormal");
                } else {
                    z = com.igexin.push.c.i.a().f();
                }
            }
            if (z) {
                com.igexin.b.a.c.a.b("CoreAction|detect result  = true, reconnect will run after detect");
            } else {
                com.igexin.b.a.b.a.a.d.a().a(true);
            }
        } else {
            com.igexin.b.a.c.a.b("CoreAction|network changed, online = true, networkAvailable = true");
            if (System.currentTimeMillis() - com.igexin.push.core.g.Q > 5000) {
                com.igexin.b.a.c.a.b("CoreAction|network changed, online = true, networkAvailable = true, send heart beat ....");
                com.igexin.push.core.g.Q = System.currentTimeMillis();
                if (g() == -2) {
                    com.igexin.push.core.g.l = false;
                    l();
                }
            }
        }
        if (t()) {
            com.igexin.b.a.c.a.b("CoreAction|network changed check condition status");
            s();
        }
    }

    @TargetApi(12)
    private Intent E() {
        Intent intent = new Intent();
        if (Build.VERSION.SDK_INT >= 12) {
            intent.addFlags(32);
        }
        intent.setAction("com.igexin.sdk.action." + com.igexin.push.core.g.f974a);
        return intent;
    }

    /* JADX WARNING: Removed duplicated region for block: B:45:0x0134  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean F() {
        /*
            r11 = this;
            r6 = 0
            java.util.Map<java.lang.String, com.igexin.push.core.bean.PushTaskBean> r0 = com.igexin.push.core.g.ai
            boolean r0 = r0.isEmpty()
            if (r0 == 0) goto L_0x0107
            boolean r0 = com.igexin.push.core.g.n
            if (r0 == 0) goto L_0x0107
            com.igexin.push.core.f r0 = com.igexin.push.core.f.a()     // Catch:{ Throwable -> 0x010e, all -> 0x0130 }
            com.igexin.push.b.b r0 = r0.k()     // Catch:{ Throwable -> 0x010e, all -> 0x0130 }
            java.lang.String r1 = "message"
            r2 = 1
            java.lang.String[] r2 = new java.lang.String[r2]     // Catch:{ Throwable -> 0x010e, all -> 0x0130 }
            r3 = 0
            java.lang.String r4 = "status"
            r2[r3] = r4     // Catch:{ Throwable -> 0x010e, all -> 0x0130 }
            r3 = 1
            java.lang.String[] r3 = new java.lang.String[r3]     // Catch:{ Throwable -> 0x010e, all -> 0x0130 }
            r4 = 0
            java.lang.String r5 = "0"
            r3[r4] = r5     // Catch:{ Throwable -> 0x010e, all -> 0x0130 }
            r4 = 0
            r5 = 0
            android.database.Cursor r1 = r0.a(r1, r2, r3, r4, r5)     // Catch:{ Throwable -> 0x010e, all -> 0x0130 }
            if (r1 == 0) goto L_0x00ff
        L_0x002f:
            boolean r0 = r1.moveToNext()     // Catch:{ Throwable -> 0x013a }
            if (r0 == 0) goto L_0x00ff
            java.lang.String r0 = "msgextra"
            int r0 = r1.getColumnIndex(r0)     // Catch:{ Throwable -> 0x013a }
            byte[] r0 = r1.getBlob(r0)     // Catch:{ Throwable -> 0x013a }
            java.lang.String r2 = "info"
            int r2 = r1.getColumnIndex(r2)     // Catch:{ Throwable -> 0x013a }
            byte[] r2 = r1.getBlob(r2)     // Catch:{ Throwable -> 0x013a }
            org.json.JSONObject r3 = new org.json.JSONObject     // Catch:{ JSONException -> 0x00fc }
            java.lang.String r4 = new java.lang.String     // Catch:{ JSONException -> 0x00fc }
            byte[] r2 = com.igexin.b.b.a.c(r2)     // Catch:{ JSONException -> 0x00fc }
            r4.<init>(r2)     // Catch:{ JSONException -> 0x00fc }
            r3.<init>(r4)     // Catch:{ JSONException -> 0x00fc }
            java.lang.String r2 = "id"
            java.lang.String r2 = r3.getString(r2)     // Catch:{ JSONException -> 0x00fc }
            java.lang.String r4 = "appid"
            java.lang.String r4 = r3.getString(r4)     // Catch:{ JSONException -> 0x00fc }
            java.lang.String r5 = "messageid"
            java.lang.String r5 = r3.getString(r5)     // Catch:{ JSONException -> 0x00fc }
            java.lang.String r6 = "taskid"
            java.lang.String r6 = r3.getString(r6)     // Catch:{ JSONException -> 0x00fc }
            java.lang.String r7 = "appkey"
            java.lang.String r7 = r3.getString(r7)     // Catch:{ JSONException -> 0x00fc }
            java.lang.String r8 = "action_chains"
            org.json.JSONArray r8 = r3.getJSONArray(r8)     // Catch:{ JSONException -> 0x00fc }
            com.igexin.push.core.a.e r9 = a()     // Catch:{ JSONException -> 0x00fc }
            java.lang.String r9 = r9.c(r6, r5)     // Catch:{ JSONException -> 0x00fc }
            com.igexin.push.core.bean.PushTaskBean r10 = new com.igexin.push.core.bean.PushTaskBean     // Catch:{ JSONException -> 0x00fc }
            r10.<init>()     // Catch:{ JSONException -> 0x00fc }
            r10.setAppid(r4)     // Catch:{ JSONException -> 0x00fc }
            r10.setMessageId(r5)     // Catch:{ JSONException -> 0x00fc }
            r10.setTaskId(r6)     // Catch:{ JSONException -> 0x00fc }
            r10.setId(r2)     // Catch:{ JSONException -> 0x00fc }
            r10.setAppKey(r7)     // Catch:{ JSONException -> 0x00fc }
            r2 = 1
            r10.setCurrentActionid(r2)     // Catch:{ JSONException -> 0x00fc }
            java.lang.String r2 = "status"
            int r2 = r1.getColumnIndex(r2)     // Catch:{ JSONException -> 0x00fc }
            int r2 = r1.getInt(r2)     // Catch:{ JSONException -> 0x00fc }
            r10.setStatus(r2)     // Catch:{ JSONException -> 0x00fc }
            if (r0 == 0) goto L_0x00ad
            r10.setMsgExtra(r0)     // Catch:{ JSONException -> 0x00fc }
        L_0x00ad:
            java.lang.String r0 = "cdnType"
            boolean r0 = r3.has(r0)     // Catch:{ JSONException -> 0x00fc }
            if (r0 == 0) goto L_0x00be
            java.lang.String r0 = "cdnType"
            boolean r0 = r3.getBoolean(r0)     // Catch:{ JSONException -> 0x00fc }
            r10.setCDNType(r0)     // Catch:{ JSONException -> 0x00fc }
        L_0x00be:
            java.lang.String r0 = "condition"
            boolean r0 = r3.has(r0)     // Catch:{ JSONException -> 0x00fc }
            if (r0 == 0) goto L_0x00c9
            r11.b(r3, r10)     // Catch:{ JSONException -> 0x00fc }
        L_0x00c9:
            if (r8 == 0) goto L_0x00f5
            int r0 = r8.length()     // Catch:{ JSONException -> 0x00fc }
            if (r0 <= 0) goto L_0x00f5
            com.igexin.push.core.a.e r0 = a()     // Catch:{ JSONException -> 0x00fc }
            boolean r0 = r0.a(r3, r10)     // Catch:{ JSONException -> 0x00fc }
            if (r0 != 0) goto L_0x00f5
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ JSONException -> 0x00fc }
            r0.<init>()     // Catch:{ JSONException -> 0x00fc }
            java.lang.String r2 = "CoreAction|load task from db parseActionChains result = false ####### "
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ JSONException -> 0x00fc }
            java.lang.String r2 = r3.toString()     // Catch:{ JSONException -> 0x00fc }
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ JSONException -> 0x00fc }
            java.lang.String r0 = r0.toString()     // Catch:{ JSONException -> 0x00fc }
            com.igexin.b.a.c.a.b(r0)     // Catch:{ JSONException -> 0x00fc }
        L_0x00f5:
            java.util.Map<java.lang.String, com.igexin.push.core.bean.PushTaskBean> r0 = com.igexin.push.core.g.ai     // Catch:{ JSONException -> 0x00fc }
            r0.put(r9, r10)     // Catch:{ JSONException -> 0x00fc }
            goto L_0x002f
        L_0x00fc:
            r0 = move-exception
            goto L_0x002f
        L_0x00ff:
            r0 = 0
            com.igexin.push.core.g.n = r0     // Catch:{ Throwable -> 0x013a }
            if (r1 == 0) goto L_0x0107
            r1.close()
        L_0x0107:
            java.util.Map<java.lang.String, com.igexin.push.core.bean.PushTaskBean> r0 = com.igexin.push.core.g.ai
            boolean r0 = r0.isEmpty()
            return r0
        L_0x010e:
            r0 = move-exception
            r1 = r6
        L_0x0110:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x0138 }
            r2.<init>()     // Catch:{ all -> 0x0138 }
            java.lang.String r3 = "CoreAction|checkPushMessageMapValue error:"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x0138 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0138 }
            java.lang.StringBuilder r0 = r2.append(r0)     // Catch:{ all -> 0x0138 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0138 }
            com.igexin.b.a.c.a.b(r0)     // Catch:{ all -> 0x0138 }
            if (r1 == 0) goto L_0x0107
            r1.close()
            goto L_0x0107
        L_0x0130:
            r0 = move-exception
            r1 = r6
        L_0x0132:
            if (r1 == 0) goto L_0x0137
            r1.close()
        L_0x0137:
            throw r0
        L_0x0138:
            r0 = move-exception
            goto L_0x0132
        L_0x013a:
            r0 = move-exception
            goto L_0x0110
        */
        throw new UnsupportedOperationException("Method not decompiled: com.igexin.push.core.a.e.F():boolean");
    }

    public static e a() {
        if (c == null) {
            c = new e();
        }
        return c;
    }

    private void a(int i, String str, String str2) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("status", Integer.valueOf(i));
        com.igexin.push.core.f.a().k().a("message", contentValues, new String[]{"taskid"}, new String[]{str});
    }

    private void a(com.igexin.push.d.c.c cVar, PushTaskBean pushTaskBean, String str, String str2) {
        cVar.a(new com.igexin.push.f.b.b(pushTaskBean, str, r.a()));
        com.igexin.push.core.g.al.put(str2, cVar);
    }

    private void a(String str, String str2, String str3, byte[] bArr) {
        Intent intent = new Intent();
        if (Build.VERSION.SDK_INT >= 12) {
            intent.addFlags(32);
        }
        intent.setAction("com.igexin.sdk.action." + str3);
        Bundle bundle = new Bundle();
        bundle.putInt("action", 10001);
        bundle.putString("taskid", str);
        bundle.putString("messageid", str2);
        bundle.putString("appid", str3);
        bundle.putString("payloadid", str2 + ":" + str);
        bundle.putString("packagename", com.igexin.push.core.g.e);
        bundle.putByteArray(AssistPushConsts.MSG_TYPE_PAYLOAD, bArr);
        intent.putExtras(bundle);
        com.igexin.push.core.g.f.sendBroadcast(intent);
    }

    private void a(List<n> list) {
        int i = 0;
        h hVar = new h(this);
        PackageManager packageManager = com.igexin.push.core.g.f.getPackageManager();
        List<PackageInfo> installedPackages = packageManager.getInstalledPackages(0);
        while (true) {
            int i2 = i;
            if (i2 < installedPackages.size()) {
                try {
                    PackageInfo packageInfo = installedPackages.get(i2);
                    ApplicationInfo applicationInfo = packageInfo.applicationInfo;
                    if ((applicationInfo.flags & 1) <= 0) {
                        n nVar = new n();
                        nVar.b(applicationInfo.loadLabel(packageManager).toString());
                        nVar.d(applicationInfo.packageName);
                        nVar.c(String.valueOf(packageInfo.versionCode));
                        nVar.a(packageInfo.versionName);
                        list.add(nVar);
                    }
                } catch (Exception e2) {
                }
                i = i2 + 1;
            } else {
                Collections.sort(list, hVar);
                return;
            }
        }
    }

    public static boolean a(long j) {
        Date date = new Date(j);
        Calendar instance = Calendar.getInstance();
        instance.setTime(date);
        int i = instance.get(11);
        int i2 = m.f885a + m.b;
        if (i2 >= 24) {
            i2 -= 24;
        }
        if (m.b == 0) {
            return false;
        }
        if (m.f885a < i2) {
            if (i >= m.f885a && i < i2) {
                return true;
            }
        } else if (m.f885a > i2) {
            if (i >= 0 && i < i2) {
                return true;
            }
            if (i >= m.f885a && i < 24) {
                return true;
            }
        }
        return false;
    }

    private void b(JSONObject jSONObject, PushTaskBean pushTaskBean) {
        try {
            JSONObject jSONObject2 = jSONObject.getJSONObject("condition");
            HashMap hashMap = new HashMap();
            if (jSONObject2.has(IXAdSystemUtils.NT_WIFI)) {
                hashMap.put(IXAdSystemUtils.NT_WIFI, jSONObject2.getString(IXAdSystemUtils.NT_WIFI));
            }
            if (jSONObject2.has("screenOn")) {
                hashMap.put("screenOn", jSONObject2.getString("screenOn"));
            }
            if (jSONObject2.has("ssid")) {
                hashMap.put("ssid", jSONObject2.getString("ssid"));
                if (jSONObject2.has("bssid")) {
                    hashMap.put("bssid", jSONObject2.getString("bssid"));
                }
            }
            if (jSONObject2.has("duration")) {
                String string = jSONObject2.getString("duration");
                if (string.contains("-")) {
                    int indexOf = string.indexOf("-");
                    String substring = string.substring(0, indexOf);
                    String substring2 = string.substring(indexOf + 1, string.length());
                    hashMap.put("startTime", substring);
                    hashMap.put("endTime", substring2);
                }
            }
            pushTaskBean.setConditionMap(hashMap);
        } catch (Exception e2) {
        }
    }

    private void e(String str) {
        if (str != null && str.startsWith("package:")) {
            String substring = str.substring(8);
            if (com.igexin.push.core.b.g.a().d().containsKey(substring)) {
                com.igexin.push.core.b.g.a().d().remove(substring);
            }
        }
    }

    /* access modifiers changed from: private */
    public void e(String str, String str2) {
        try {
            Intent intent = new Intent();
            intent.setPackage(str);
            intent.setAction("com.igexin.sdk.action.service.message");
            if (com.igexin.push.util.a.a(intent, com.igexin.push.core.g.f)) {
                com.igexin.push.core.g.f.startService(intent);
                com.igexin.b.a.c.a.b("CoreAction|startService by action");
                return;
            }
            Intent intent2 = new Intent();
            intent2.setClassName(str, str2);
            com.igexin.push.core.g.f.startService(intent2);
            com.igexin.b.a.c.a.b("CoreAction|startService by service name");
        } catch (Throwable th) {
            com.igexin.b.a.c.a.b("CoreAction|startService pkgName = " + str + " srvName = " + str2 + ", exception : " + th.toString());
        }
    }

    private void f(Intent intent) {
        String stringExtra = intent.getStringExtra("taskid");
        String stringExtra2 = intent.getStringExtra("messageid");
        String stringExtra3 = intent.getStringExtra("actionid");
        String stringExtra4 = intent.getStringExtra("accesstoken");
        int intExtra = intent.getIntExtra("notifID", 0);
        NotificationManager notificationManager = (NotificationManager) com.igexin.push.core.g.f.getSystemService("notification");
        if (intExtra != 0) {
            notificationManager.cancel(intExtra);
        } else if (com.igexin.push.core.g.aj.get(stringExtra) != null) {
            notificationManager.cancel(com.igexin.push.core.g.aj.get(stringExtra).intValue());
        }
        if (stringExtra4.equals(com.igexin.push.core.g.au)) {
            b(stringExtra, stringExtra2, stringExtra3);
        }
    }

    private void f(String str) {
        if (!TextUtils.isEmpty(str) && str.startsWith("package:")) {
            try {
                String substring = str.substring(8);
                PackageInfo packageInfo = com.igexin.push.core.g.f.getPackageManager().getPackageInfo(substring, 4);
                if (packageInfo != null && packageInfo.services != null) {
                    for (ServiceInfo serviceInfo : packageInfo.services) {
                        if (com.igexin.push.util.a.a(serviceInfo, packageInfo)) {
                            com.igexin.push.core.b.g.a().d().put(substring, serviceInfo.name);
                            return;
                        }
                    }
                }
            } catch (Throwable th) {
            }
        }
    }

    private void f(boolean z) {
        com.igexin.push.d.a.b.b = -1;
        if (!EncryptUtils.isLoadSuccess()) {
            com.igexin.b.a.c.a.b("CoreAction|so error ++++++++");
        } else if (com.igexin.push.core.g.aB) {
            com.igexin.push.core.f.a().g().c(z);
        } else {
            com.igexin.b.a.c.a.b("CoreAction|autoReconnect CoreRuntimeInfo.initSuccess = false");
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:30:0x00d1  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void g(android.content.Intent r12) {
        /*
            r11 = this;
            r6 = 0
            java.lang.String r0 = "taskid"
            java.lang.String r7 = r12.getStringExtra(r0)
            java.lang.String r0 = "messageid"
            java.lang.String r8 = r12.getStringExtra(r0)
            java.lang.String r0 = "appid"
            java.lang.String r0 = r12.getStringExtra(r0)
            java.lang.String r1 = "pkgname"
            java.lang.String r9 = r12.getStringExtra(r1)
            android.content.ContentValues r10 = new android.content.ContentValues
            r10.<init>()
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "EXEC_"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r7)
            java.lang.String r4 = r1.toString()
            java.lang.String r1 = "taskid"
            r10.put(r1, r7)
            java.lang.String r1 = "appid"
            r10.put(r1, r0)
            java.lang.String r0 = "key"
            r10.put(r0, r4)
            java.lang.String r0 = "createtime"
            long r2 = java.lang.System.currentTimeMillis()
            java.lang.Long r1 = java.lang.Long.valueOf(r2)
            r10.put(r0, r1)
            com.igexin.push.core.f r0 = com.igexin.push.core.f.a()     // Catch:{ Throwable -> 0x00ab, all -> 0x00cd }
            com.igexin.push.b.b r0 = r0.k()     // Catch:{ Throwable -> 0x00ab, all -> 0x00cd }
            java.lang.String r1 = "message"
            r2 = 1
            java.lang.String[] r2 = new java.lang.String[r2]     // Catch:{ Throwable -> 0x00ab, all -> 0x00cd }
            r3 = 0
            java.lang.String r5 = "key"
            r2[r3] = r5     // Catch:{ Throwable -> 0x00ab, all -> 0x00cd }
            r3 = 1
            java.lang.String[] r3 = new java.lang.String[r3]     // Catch:{ Throwable -> 0x00ab, all -> 0x00cd }
            r5 = 0
            r3[r5] = r4     // Catch:{ Throwable -> 0x00ab, all -> 0x00cd }
            r4 = 0
            r5 = 0
            android.database.Cursor r1 = r0.a(r1, r2, r3, r4, r5)     // Catch:{ Throwable -> 0x00ab, all -> 0x00cd }
            if (r1 == 0) goto L_0x00a5
            int r0 = r1.getCount()     // Catch:{ Throwable -> 0x00d7 }
            if (r0 != 0) goto L_0x00a5
            com.igexin.push.core.f r0 = com.igexin.push.core.f.a()     // Catch:{ Throwable -> 0x00d7 }
            com.igexin.push.b.b r0 = r0.k()     // Catch:{ Throwable -> 0x00d7 }
            java.lang.String r2 = "message"
            r0.a(r2, r10)     // Catch:{ Throwable -> 0x00d7 }
            java.lang.String r0 = com.igexin.push.core.g.e     // Catch:{ Throwable -> 0x00d7 }
            boolean r0 = r9.equals(r0)     // Catch:{ Throwable -> 0x00d7 }
            if (r0 == 0) goto L_0x00a5
            if (r8 == 0) goto L_0x008c
            if (r7 != 0) goto L_0x0092
        L_0x008c:
            if (r1 == 0) goto L_0x0091
            r1.close()
        L_0x0091:
            return
        L_0x0092:
            com.igexin.push.core.f r0 = com.igexin.push.core.f.a()     // Catch:{ Throwable -> 0x00d7 }
            if (r0 == 0) goto L_0x00a5
            com.igexin.push.core.b r0 = r11.d(r7, r8)     // Catch:{ Throwable -> 0x00d7 }
            com.igexin.push.core.b r2 = com.igexin.push.core.b.success     // Catch:{ Throwable -> 0x00d7 }
            if (r0 != r2) goto L_0x00a5
            java.lang.String r0 = "1"
            r11.a(r7, r8, r0)     // Catch:{ Throwable -> 0x00d7 }
        L_0x00a5:
            if (r1 == 0) goto L_0x0091
            r1.close()
            goto L_0x0091
        L_0x00ab:
            r0 = move-exception
            r1 = r6
        L_0x00ad:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x00d5 }
            r2.<init>()     // Catch:{ all -> 0x00d5 }
            java.lang.String r3 = "CoreAction|"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x00d5 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x00d5 }
            java.lang.StringBuilder r0 = r2.append(r0)     // Catch:{ all -> 0x00d5 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x00d5 }
            com.igexin.b.a.c.a.b(r0)     // Catch:{ all -> 0x00d5 }
            if (r1 == 0) goto L_0x0091
            r1.close()
            goto L_0x0091
        L_0x00cd:
            r0 = move-exception
            r1 = r6
        L_0x00cf:
            if (r1 == 0) goto L_0x00d4
            r1.close()
        L_0x00d4:
            throw r0
        L_0x00d5:
            r0 = move-exception
            goto L_0x00cf
        L_0x00d7:
            r0 = move-exception
            goto L_0x00ad
        */
        throw new UnsupportedOperationException("Method not decompiled: com.igexin.push.core.a.e.g(android.content.Intent):void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:20:0x0057 A[SYNTHETIC, Splitter:B:20:0x0057] */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0063 A[SYNTHETIC, Splitter:B:26:0x0063] */
    /* JADX WARNING: Removed duplicated region for block: B:37:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void g(java.lang.String r6) {
        /*
            r5 = this;
            r0 = 0
            java.io.File r1 = new java.io.File     // Catch:{ Exception -> 0x0054, all -> 0x005d }
            java.lang.String r2 = com.igexin.push.core.g.Z     // Catch:{ Exception -> 0x0054, all -> 0x005d }
            r1.<init>(r2)     // Catch:{ Exception -> 0x0054, all -> 0x005d }
            boolean r2 = r1.exists()     // Catch:{ Exception -> 0x0054, all -> 0x005d }
            if (r2 != 0) goto L_0x003a
            boolean r2 = r1.createNewFile()     // Catch:{ Exception -> 0x0054, all -> 0x005d }
            if (r2 != 0) goto L_0x003a
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0054, all -> 0x005d }
            r2.<init>()     // Catch:{ Exception -> 0x0054, all -> 0x005d }
            java.lang.String r3 = "create file "
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x0054, all -> 0x005d }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x0054, all -> 0x005d }
            java.lang.StringBuilder r1 = r2.append(r1)     // Catch:{ Exception -> 0x0054, all -> 0x005d }
            java.lang.String r2 = " failed######"
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x0054, all -> 0x005d }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x0054, all -> 0x005d }
            com.igexin.b.a.c.a.b(r1)     // Catch:{ Exception -> 0x0054, all -> 0x005d }
            if (r0 == 0) goto L_0x0039
            r0.close()     // Catch:{ Exception -> 0x0067 }
        L_0x0039:
            return
        L_0x003a:
            java.io.FileOutputStream r1 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x0054, all -> 0x005d }
            java.lang.String r2 = com.igexin.push.core.g.Z     // Catch:{ Exception -> 0x0054, all -> 0x005d }
            r1.<init>(r2)     // Catch:{ Exception -> 0x0054, all -> 0x005d }
            java.lang.String r0 = com.igexin.b.b.a.a(r6)     // Catch:{ Exception -> 0x006d, all -> 0x006b }
            byte[] r0 = r0.getBytes()     // Catch:{ Exception -> 0x006d, all -> 0x006b }
            r1.write(r0)     // Catch:{ Exception -> 0x006d, all -> 0x006b }
            if (r1 == 0) goto L_0x0039
            r1.close()     // Catch:{ Exception -> 0x0052 }
            goto L_0x0039
        L_0x0052:
            r0 = move-exception
            goto L_0x0039
        L_0x0054:
            r1 = move-exception
        L_0x0055:
            if (r0 == 0) goto L_0x0039
            r0.close()     // Catch:{ Exception -> 0x005b }
            goto L_0x0039
        L_0x005b:
            r0 = move-exception
            goto L_0x0039
        L_0x005d:
            r1 = move-exception
            r4 = r1
            r1 = r0
            r0 = r4
        L_0x0061:
            if (r1 == 0) goto L_0x0066
            r1.close()     // Catch:{ Exception -> 0x0069 }
        L_0x0066:
            throw r0
        L_0x0067:
            r0 = move-exception
            goto L_0x0039
        L_0x0069:
            r1 = move-exception
            goto L_0x0066
        L_0x006b:
            r0 = move-exception
            goto L_0x0061
        L_0x006d:
            r0 = move-exception
            r0 = r1
            goto L_0x0055
        */
        throw new UnsupportedOperationException("Method not decompiled: com.igexin.push.core.a.e.g(java.lang.String):void");
    }

    public void A() {
        if (!com.igexin.push.core.g.af) {
            com.igexin.push.core.g.af = com.igexin.b.a.b.c.b().a(com.igexin.push.f.b.c.g(), false, true);
        }
        if (!com.igexin.push.core.g.ag) {
            com.igexin.push.core.g.ag = com.igexin.b.a.b.c.b().a(com.igexin.push.f.b.g.g(), true, true);
        }
        if (!com.igexin.push.core.g.ah) {
            com.igexin.push.core.f.a().c();
        }
    }

    public boolean B() {
        return !com.igexin.push.core.g.i || !com.igexin.push.core.g.j;
    }

    public com.igexin.push.core.bean.g a(JSONObject jSONObject) {
        com.igexin.push.core.bean.g gVar = new com.igexin.push.core.bean.g();
        gVar.a(jSONObject.getString("version"));
        JSONArray jSONArray = jSONObject.getJSONArray("extensions");
        if (jSONArray == null || jSONArray.length() <= 0) {
            gVar.a(new HashMap());
        } else {
            HashMap hashMap = new HashMap();
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 >= jSONArray.length()) {
                    break;
                }
                JSONObject jSONObject2 = (JSONObject) jSONArray.get(i2);
                com.igexin.push.core.bean.f fVar = new com.igexin.push.core.bean.f();
                fVar.a(jSONObject2.getInt("id"));
                fVar.a(jSONObject2.getString("version"));
                fVar.b(jSONObject2.getString(SelectCountryActivity.EXTRA_COUNTRY_NAME));
                fVar.c(jSONObject2.getString("cls_name"));
                fVar.d(jSONObject2.getString("url"));
                fVar.e(jSONObject2.getString("checksum"));
                fVar.f(jSONObject2.getString("key"));
                if (jSONObject2.has("isdestroy")) {
                    fVar.a(jSONObject2.getBoolean("isdestroy"));
                }
                if (jSONObject2.has("effective")) {
                    String string = jSONObject2.getString("effective");
                    long j = 0;
                    if (string != null && string.length() <= 13) {
                        j = Long.parseLong(string);
                    }
                    fVar.a(j);
                }
                if (jSONObject2.has("loadTime")) {
                    fVar.b(jSONObject2.getLong("loadTime"));
                }
                hashMap.put(Integer.valueOf(fVar.a()), fVar);
                i = i2 + 1;
            }
            gVar.a(hashMap);
        }
        return gVar;
    }

    public Class a(Context context) {
        return GTServiceManager.getInstance().getUserPushService(context);
    }

    public String a(com.igexin.push.core.bean.g gVar) {
        JSONObject jSONObject = new JSONObject();
        try {
            String a2 = gVar.a();
            Map<Integer, com.igexin.push.core.bean.f> b2 = gVar.b();
            String str = "[]";
            if (a2 != null) {
                jSONObject.put("version", a2);
            }
            if (b2 != null && b2.size() > 0) {
                String str2 = "[";
                for (Map.Entry<Integer, com.igexin.push.core.bean.f> value : b2.entrySet()) {
                    com.igexin.push.core.bean.f fVar = (com.igexin.push.core.bean.f) value.getValue();
                    JSONObject jSONObject2 = new JSONObject();
                    jSONObject2.put("id", fVar.a());
                    jSONObject2.put("version", fVar.b());
                    jSONObject2.put(SelectCountryActivity.EXTRA_COUNTRY_NAME, fVar.c());
                    jSONObject2.put("cls_name", fVar.d());
                    jSONObject2.put("url", fVar.e());
                    jSONObject2.put("checksum", fVar.f());
                    jSONObject2.put("isdestroy", fVar.g());
                    jSONObject2.put("effective", fVar.h());
                    jSONObject2.put("loadTime", fVar.i());
                    jSONObject2.put("key", fVar.j());
                    str2 = (str2 + jSONObject2.toString()) + MiPushClient.ACCEPT_TIME_SEPARATOR;
                }
                str = (str2.endsWith(MiPushClient.ACCEPT_TIME_SEPARATOR) ? str2.substring(0, str2.length() - 1) : str2) + "]";
            }
            jSONObject.put("extensions", new JSONArray(str));
            return jSONObject.toString();
        } catch (Exception e2) {
            com.igexin.b.a.c.a.b(e2.toString());
            return null;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:44:0x0469  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String a(boolean r13, int r14) {
        /*
            r12 = this;
            r3 = 1
            r6 = 0
            java.text.SimpleDateFormat r0 = new java.text.SimpleDateFormat
            java.lang.String r1 = "yyyy-MM-dd HH:mm:ss"
            java.util.Locale r2 = java.util.Locale.getDefault()
            r0.<init>(r1, r2)
            java.util.Date r1 = new java.util.Date
            r1.<init>()
            java.lang.String r2 = r0.format(r1)
            r0 = -1
            if (r14 != r0) goto L_0x0050
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x045c, all -> 0x0465 }
            r0.<init>()     // Catch:{ Exception -> 0x045c, all -> 0x0465 }
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ Exception -> 0x045c, all -> 0x0465 }
            java.lang.String r1 = "|"
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Exception -> 0x045c, all -> 0x0465 }
            java.lang.String r1 = com.igexin.push.core.g.B     // Catch:{ Exception -> 0x045c, all -> 0x0465 }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Exception -> 0x045c, all -> 0x0465 }
            java.lang.String r1 = "|"
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Exception -> 0x045c, all -> 0x0465 }
            java.lang.String r1 = "register"
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Exception -> 0x045c, all -> 0x0465 }
            java.lang.String r1 = "|"
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Exception -> 0x045c, all -> 0x0465 }
            java.lang.String r1 = com.igexin.push.core.g.s     // Catch:{ Exception -> 0x045c, all -> 0x0465 }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Exception -> 0x045c, all -> 0x0465 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x045c, all -> 0x0465 }
        L_0x004a:
            if (r6 == 0) goto L_0x004f
            r6.close()
        L_0x004f:
            return r0
        L_0x0050:
            if (r14 != 0) goto L_0x0389
            if (r13 == 0) goto L_0x0220
            com.igexin.push.core.f r0 = com.igexin.push.core.f.a()     // Catch:{ Exception -> 0x045c, all -> 0x0465 }
            com.igexin.push.b.b r0 = r0.k()     // Catch:{ Exception -> 0x045c, all -> 0x0465 }
            java.lang.String r1 = "bi"
            r2 = 1
            java.lang.String[] r2 = new java.lang.String[r2]     // Catch:{ Exception -> 0x045c, all -> 0x0465 }
            r3 = 0
            java.lang.String r4 = "type"
            r2[r3] = r4     // Catch:{ Exception -> 0x045c, all -> 0x0465 }
            r3 = 2
            java.lang.String[] r3 = new java.lang.String[r3]     // Catch:{ Exception -> 0x045c, all -> 0x0465 }
            r4 = 0
            java.lang.String r5 = "1"
            r3[r4] = r5     // Catch:{ Exception -> 0x045c, all -> 0x0465 }
            r4 = 1
            java.lang.String r5 = "2"
            r3[r4] = r5     // Catch:{ Exception -> 0x045c, all -> 0x0465 }
            r4 = 0
            r5 = 0
            android.database.Cursor r1 = r0.a(r1, r2, r3, r4, r5)     // Catch:{ Exception -> 0x045c, all -> 0x0465 }
        L_0x0079:
            if (r1 == 0) goto L_0x0385
            r0 = r6
        L_0x007c:
            boolean r2 = r1.moveToNext()     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            if (r2 == 0) goto L_0x0386
            java.lang.String r2 = "start_service_count"
            int r2 = r1.getColumnIndexOrThrow(r2)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            int r2 = r1.getInt(r2)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.String r3 = "login_count"
            int r3 = r1.getColumnIndexOrThrow(r3)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            int r3 = r1.getInt(r3)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.String r4 = "loginerror_nonetwork_count"
            int r4 = r1.getColumnIndexOrThrow(r4)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            int r4 = r1.getInt(r4)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.String r5 = "loginerror_connecterror_count"
            int r5 = r1.getColumnIndexOrThrow(r5)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            int r5 = r1.getInt(r5)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.String r6 = "online_time"
            int r6 = r1.getColumnIndexOrThrow(r6)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            int r6 = r1.getInt(r6)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.String r7 = "network_time"
            int r7 = r1.getColumnIndexOrThrow(r7)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            int r7 = r1.getInt(r7)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.String r8 = "running_time"
            int r8 = r1.getColumnIndexOrThrow(r8)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            int r8 = r1.getInt(r8)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            r9.<init>()     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.String r10 = "create_time"
            int r10 = r1.getColumnIndexOrThrow(r10)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.String r10 = r1.getString(r10)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.StringBuilder r9 = r9.append(r10)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.String r10 = " 00:00:00"
            java.lang.StringBuilder r9 = r9.append(r10)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.String r9 = r9.toString()     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            if (r0 != 0) goto L_0x0242
            java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            r10.<init>()     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.StringBuilder r10 = r10.append(r9)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.String r11 = "|"
            java.lang.StringBuilder r10 = r10.append(r11)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.String r11 = com.igexin.push.core.g.B     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.StringBuilder r10 = r10.append(r11)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.String r11 = "|"
            java.lang.StringBuilder r10 = r10.append(r11)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.String r11 = "startservice"
            java.lang.StringBuilder r10 = r10.append(r11)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.String r11 = "|"
            java.lang.StringBuilder r10 = r10.append(r11)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.StringBuilder r2 = r10.append(r2)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.String r10 = "\n"
            java.lang.StringBuilder r2 = r2.append(r10)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.StringBuilder r2 = r2.append(r9)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.String r10 = "|"
            java.lang.StringBuilder r2 = r2.append(r10)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.String r10 = com.igexin.push.core.g.B     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.StringBuilder r2 = r2.append(r10)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.String r10 = "|"
            java.lang.StringBuilder r2 = r2.append(r10)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.String r10 = "login"
            java.lang.StringBuilder r2 = r2.append(r10)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.String r10 = "|"
            java.lang.StringBuilder r2 = r2.append(r10)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.String r3 = "\n"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.StringBuilder r2 = r2.append(r9)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.String r3 = "|"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.String r3 = com.igexin.push.core.g.B     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.String r3 = "|"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.String r3 = "loginerror-nonetwork"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.String r3 = "|"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.String r3 = "\n"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.StringBuilder r2 = r2.append(r9)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.String r3 = "|"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.String r3 = com.igexin.push.core.g.B     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.String r3 = "|"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.String r3 = "loginerror-connecterror"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.String r3 = "|"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.StringBuilder r2 = r2.append(r5)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.String r3 = "\n"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.StringBuilder r2 = r2.append(r9)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.String r3 = "|"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.String r3 = com.igexin.push.core.g.B     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.String r3 = "|"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.String r3 = "online"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.String r3 = "|"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.StringBuilder r2 = r2.append(r6)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.String r3 = "\n"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.StringBuilder r2 = r2.append(r9)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.String r3 = "|"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.String r3 = com.igexin.push.core.g.B     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.String r3 = "|"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.String r3 = "network"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.String r3 = "|"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.StringBuilder r2 = r2.append(r7)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.String r3 = "\n"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.StringBuilder r2 = r2.append(r9)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.String r3 = "|"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.String r3 = com.igexin.push.core.g.B     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.String r3 = "|"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.String r3 = "running"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.String r3 = "|"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.StringBuilder r2 = r2.append(r8)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.String r0 = r2.toString()     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            goto L_0x007c
        L_0x0220:
            com.igexin.push.core.f r0 = com.igexin.push.core.f.a()     // Catch:{ Exception -> 0x045c, all -> 0x0465 }
            com.igexin.push.b.b r0 = r0.k()     // Catch:{ Exception -> 0x045c, all -> 0x0465 }
            java.lang.String r1 = "bi"
            r2 = 1
            java.lang.String[] r2 = new java.lang.String[r2]     // Catch:{ Exception -> 0x045c, all -> 0x0465 }
            r3 = 0
            java.lang.String r4 = "type"
            r2[r3] = r4     // Catch:{ Exception -> 0x045c, all -> 0x0465 }
            r3 = 1
            java.lang.String[] r3 = new java.lang.String[r3]     // Catch:{ Exception -> 0x045c, all -> 0x0465 }
            r4 = 0
            java.lang.String r5 = "2"
            r3[r4] = r5     // Catch:{ Exception -> 0x045c, all -> 0x0465 }
            r4 = 0
            r5 = 0
            android.database.Cursor r1 = r0.a(r1, r2, r3, r4, r5)     // Catch:{ Exception -> 0x045c, all -> 0x0465 }
            goto L_0x0079
        L_0x0242:
            java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            r10.<init>()     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.StringBuilder r10 = r10.append(r0)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.String r11 = "\n"
            java.lang.StringBuilder r10 = r10.append(r11)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.StringBuilder r10 = r10.append(r9)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.String r11 = "|"
            java.lang.StringBuilder r10 = r10.append(r11)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.String r11 = com.igexin.push.core.g.B     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.StringBuilder r10 = r10.append(r11)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.String r11 = "|"
            java.lang.StringBuilder r10 = r10.append(r11)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.String r11 = "startservice"
            java.lang.StringBuilder r10 = r10.append(r11)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.String r11 = "|"
            java.lang.StringBuilder r10 = r10.append(r11)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.StringBuilder r2 = r10.append(r2)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.String r10 = "\n"
            java.lang.StringBuilder r2 = r2.append(r10)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.StringBuilder r2 = r2.append(r9)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.String r10 = "|"
            java.lang.StringBuilder r2 = r2.append(r10)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.String r10 = com.igexin.push.core.g.B     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.StringBuilder r2 = r2.append(r10)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.String r10 = "|"
            java.lang.StringBuilder r2 = r2.append(r10)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.String r10 = "login"
            java.lang.StringBuilder r2 = r2.append(r10)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.String r10 = "|"
            java.lang.StringBuilder r2 = r2.append(r10)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.String r3 = "\n"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.StringBuilder r2 = r2.append(r9)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.String r3 = "|"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.String r3 = com.igexin.push.core.g.B     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.String r3 = "|"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.String r3 = "loginerror-nonetwork"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.String r3 = "|"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.String r3 = "\n"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.StringBuilder r2 = r2.append(r9)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.String r3 = "|"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.String r3 = com.igexin.push.core.g.B     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.String r3 = "|"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.String r3 = "loginerror-connecterror"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.String r3 = "|"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.StringBuilder r2 = r2.append(r5)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.String r3 = "\n"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.StringBuilder r2 = r2.append(r9)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.String r3 = "|"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.String r3 = com.igexin.push.core.g.B     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.String r3 = "|"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.String r3 = "online"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.String r3 = "|"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.StringBuilder r2 = r2.append(r6)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.String r3 = "\n"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.StringBuilder r2 = r2.append(r9)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.String r3 = "|"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.String r3 = com.igexin.push.core.g.B     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.String r3 = "|"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.String r3 = "network"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.String r3 = "|"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.StringBuilder r2 = r2.append(r7)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.String r3 = "\n"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.StringBuilder r2 = r2.append(r9)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.String r3 = "|"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.String r3 = com.igexin.push.core.g.B     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.String r3 = "|"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.String r3 = "running"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.String r3 = "|"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.StringBuilder r2 = r2.append(r8)     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            java.lang.String r0 = r2.toString()     // Catch:{ Exception -> 0x046f, all -> 0x046d }
            goto L_0x007c
        L_0x0385:
            r0 = r6
        L_0x0386:
            r6 = r1
            goto L_0x004a
        L_0x0389:
            if (r14 != r3) goto L_0x0402
            com.igexin.push.core.i r0 = com.igexin.push.core.i.a()     // Catch:{ Exception -> 0x045c, all -> 0x0465 }
            long r0 = r0.f975a     // Catch:{ Exception -> 0x045c, all -> 0x0465 }
            int r3 = com.igexin.push.config.m.d     // Catch:{ Exception -> 0x045c, all -> 0x0465 }
            if (r3 <= 0) goto L_0x039a
            int r0 = com.igexin.push.config.m.d     // Catch:{ Exception -> 0x045c, all -> 0x0465 }
            int r0 = r0 * 1000
            long r0 = (long) r0     // Catch:{ Exception -> 0x045c, all -> 0x0465 }
        L_0x039a:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x045c, all -> 0x0465 }
            r3.<init>()     // Catch:{ Exception -> 0x045c, all -> 0x0465 }
            int r4 = com.igexin.push.config.m.f885a     // Catch:{ Exception -> 0x045c, all -> 0x0465 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x045c, all -> 0x0465 }
            java.lang.String r4 = ","
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x045c, all -> 0x0465 }
            int r4 = com.igexin.push.config.m.b     // Catch:{ Exception -> 0x045c, all -> 0x0465 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x045c, all -> 0x0465 }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x045c, all -> 0x0465 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x045c, all -> 0x0465 }
            r4.<init>()     // Catch:{ Exception -> 0x045c, all -> 0x0465 }
            java.lang.StringBuilder r2 = r4.append(r2)     // Catch:{ Exception -> 0x045c, all -> 0x0465 }
            java.lang.String r4 = "|"
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ Exception -> 0x045c, all -> 0x0465 }
            java.lang.String r4 = com.igexin.push.core.g.r     // Catch:{ Exception -> 0x045c, all -> 0x0465 }
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ Exception -> 0x045c, all -> 0x0465 }
            java.lang.String r4 = "|"
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ Exception -> 0x045c, all -> 0x0465 }
            java.lang.String r4 = com.igexin.push.core.g.f974a     // Catch:{ Exception -> 0x045c, all -> 0x0465 }
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ Exception -> 0x045c, all -> 0x0465 }
            java.lang.String r4 = "|"
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ Exception -> 0x045c, all -> 0x0465 }
            boolean r4 = com.igexin.push.core.g.i     // Catch:{ Exception -> 0x045c, all -> 0x0465 }
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ Exception -> 0x045c, all -> 0x0465 }
            java.lang.String r4 = "|"
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ Exception -> 0x045c, all -> 0x0465 }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x045c, all -> 0x0465 }
            java.lang.String r3 = "|"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x045c, all -> 0x0465 }
            java.lang.StringBuilder r0 = r2.append(r0)     // Catch:{ Exception -> 0x045c, all -> 0x0465 }
            java.lang.String r1 = "|"
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Exception -> 0x045c, all -> 0x0465 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x045c, all -> 0x0465 }
            goto L_0x004a
        L_0x0402:
            r0 = 4
            if (r14 != r0) goto L_0x0432
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x045c, all -> 0x0465 }
            r0.<init>()     // Catch:{ Exception -> 0x045c, all -> 0x0465 }
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ Exception -> 0x045c, all -> 0x0465 }
            java.lang.String r1 = "|"
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Exception -> 0x045c, all -> 0x0465 }
            java.lang.String r1 = com.igexin.push.core.g.r     // Catch:{ Exception -> 0x045c, all -> 0x0465 }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Exception -> 0x045c, all -> 0x0465 }
            java.lang.String r1 = "|"
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Exception -> 0x045c, all -> 0x0465 }
            java.lang.String r1 = com.igexin.push.core.g.f974a     // Catch:{ Exception -> 0x045c, all -> 0x0465 }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Exception -> 0x045c, all -> 0x0465 }
            java.lang.String r1 = "|"
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Exception -> 0x045c, all -> 0x0465 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x045c, all -> 0x0465 }
            goto L_0x004a
        L_0x0432:
            r0 = 5
            if (r14 != r0) goto L_0x0472
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x045c, all -> 0x0465 }
            r0.<init>()     // Catch:{ Exception -> 0x045c, all -> 0x0465 }
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ Exception -> 0x045c, all -> 0x0465 }
            java.lang.String r1 = "|"
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Exception -> 0x045c, all -> 0x0465 }
            java.lang.String r1 = com.igexin.push.core.g.r     // Catch:{ Exception -> 0x045c, all -> 0x0465 }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Exception -> 0x045c, all -> 0x0465 }
            java.lang.String r1 = "|"
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Exception -> 0x045c, all -> 0x0465 }
            java.lang.String r1 = com.igexin.push.core.g.f974a     // Catch:{ Exception -> 0x045c, all -> 0x0465 }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Exception -> 0x045c, all -> 0x0465 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x045c, all -> 0x0465 }
            goto L_0x004a
        L_0x045c:
            r0 = move-exception
            r0 = r6
        L_0x045e:
            if (r6 == 0) goto L_0x004f
            r6.close()
            goto L_0x004f
        L_0x0465:
            r0 = move-exception
            r1 = r6
        L_0x0467:
            if (r1 == 0) goto L_0x046c
            r1.close()
        L_0x046c:
            throw r0
        L_0x046d:
            r0 = move-exception
            goto L_0x0467
        L_0x046f:
            r2 = move-exception
            r6 = r1
            goto L_0x045e
        L_0x0472:
            r0 = r6
            goto L_0x004a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.igexin.push.core.a.e.a(boolean, int):java.lang.String");
    }

    public void a(int i) {
        if (com.igexin.push.core.g.f != null) {
            try {
                Class b2 = b(com.igexin.push.core.g.f);
                if (b2 != null) {
                    Intent intent = new Intent(com.igexin.push.core.g.f, b2);
                    Bundle bundle = new Bundle();
                    bundle.putInt("action", PushConsts.GET_SDKSERVICEPID);
                    bundle.putInt(PushConsts.KEY_SERVICE_PIT, i);
                    intent.putExtras(bundle);
                    com.igexin.push.core.g.f.startService(intent);
                }
            } catch (Throwable th) {
                com.igexin.b.a.c.a.b("CoreAction|" + th.toString());
            }
            Intent E = E();
            Bundle bundle2 = new Bundle();
            bundle2.putInt("action", PushConsts.GET_SDKSERVICEPID);
            bundle2.putInt(PushConsts.KEY_SERVICE_PIT, i);
            E.putExtras(bundle2);
            com.igexin.push.core.g.f.sendBroadcast(E);
        }
    }

    public void a(int i, int i2, String str) {
        m.f885a = i;
        m.b = i2;
        com.igexin.push.config.a.a().b();
        com.igexin.push.a.a.c.c().d();
    }

    public void a(int i, String str) {
        m.d = i;
        com.igexin.push.config.a.a().c();
        if (com.igexin.push.core.g.l) {
            com.igexin.b.a.c.a.b("setHeartbeatInterval heartbeatReq");
            if (System.currentTimeMillis() - com.igexin.push.core.g.Q > 5000) {
                com.igexin.push.core.g.Q = System.currentTimeMillis();
                g();
            }
        }
    }

    public void a(Intent intent) {
        com.igexin.b.a.c.a.b("CoreAction|onServiceInitialize ##");
        if (intent != null) {
            com.igexin.push.core.f.a().a(false);
            if (intent.hasExtra("op_app")) {
                com.igexin.push.core.g.D = intent.getStringExtra("op_app");
            } else {
                com.igexin.push.core.g.D = "";
            }
            com.igexin.push.core.g.m = false;
            if (com.igexin.push.core.g.l) {
                k();
                com.igexin.push.core.g.m = true;
            }
            if (GTServiceManager.getInstance().isUserPushServiceSet(com.igexin.push.core.g.f) && com.igexin.push.core.g.ad != null) {
                com.igexin.b.a.b.c.b().a(new f(this), false, true);
            }
        }
    }

    public void a(Bundle bundle) {
        String string = bundle.getString("action");
        if (!TextUtils.isEmpty(string)) {
            if (string.equals("setTag")) {
                if (m.j) {
                    a(bundle.getString("tags"), bundle.getString(IXAdRequestInfo.SN));
                }
            } else if (string.equals("setSilentTime")) {
                if (m.k) {
                    int i = bundle.getInt("beginHour", 0);
                    int i2 = bundle.getInt("duration", 0);
                    a(i, i2, com.igexin.push.core.g.f.getPackageName());
                    com.igexin.assist.sdk.b.a().a(com.igexin.push.core.g.f, i, i2);
                }
            } else if (string.equals("sendMessage")) {
                com.igexin.b.a.c.a.b("CoreAction onPushManagerMessage recevie action : sendMessage");
                if (m.i) {
                    String string2 = bundle.getString("taskid");
                    byte[] byteArray = bundle.getByteArray("extraData");
                    com.igexin.b.a.c.a.b("CoreAction receive broadcast msg data , task id : " + string2 + " ######@##@@@#");
                    a(string2, byteArray);
                }
            } else if (string.equals("stopService")) {
                com.igexin.push.core.f.a().a(com.igexin.push.core.g.f.getPackageName());
            } else if (string.equals("setHeartbeatInterval")) {
                if (m.l) {
                    a(bundle.getInt("interval", 0), com.igexin.push.core.g.f.getPackageName());
                }
            } else if (string.equals("setSocketTimeout")) {
                if (m.m) {
                    b(bundle.getInt("timeout", 0), com.igexin.push.core.g.f.getPackageName());
                }
            } else if (string.equals("sendFeedbackMessage")) {
                if (m.r && com.igexin.push.core.g.an <= 200) {
                    String string3 = bundle.getString("taskid");
                    String string4 = bundle.getString("messageid");
                    String string5 = bundle.getString("actionid");
                    String str = string3 + ":" + string4 + ":" + string5;
                    if (com.igexin.push.core.g.am.get(str) == null) {
                        long currentTimeMillis = System.currentTimeMillis();
                        PushTaskBean pushTaskBean = new PushTaskBean();
                        pushTaskBean.setTaskId(string3);
                        pushTaskBean.setMessageId(string4);
                        pushTaskBean.setAppid(com.igexin.push.core.g.f974a);
                        pushTaskBean.setAppKey(com.igexin.push.core.g.b);
                        a(pushTaskBean, string5);
                        com.igexin.push.core.g.an++;
                        com.igexin.push.core.g.am.put(str, Long.valueOf(currentTimeMillis));
                    }
                }
            } else if (string.equals("turnOffPush")) {
                com.igexin.push.core.f.a().b(com.igexin.push.core.g.f.getPackageName());
                com.igexin.assist.sdk.b.a().d(com.igexin.push.core.g.f);
            } else if (string.equals("bindAlias")) {
                String string6 = bundle.getString("alias");
                com.igexin.b.a.c.a.b("CoreAction|onPushManagerMessage bindAlias...");
                b(string6);
            } else if (string.equals("unbindAlias")) {
                String string7 = bundle.getString("alias");
                boolean z = bundle.getBoolean("isSeft");
                com.igexin.b.a.c.a.b("CoreAction|onPushManagerMessage unbindAlias...");
                a(string7, z);
            }
        }
    }

    public void a(PushTaskBean pushTaskBean) {
        com.igexin.push.d.c.c cVar = new com.igexin.push.d.c.c();
        cVar.a();
        cVar.c = "RCV" + pushTaskBean.getMessageId();
        cVar.d = com.igexin.push.core.g.r;
        cVar.f990a = (int) System.currentTimeMillis();
        com.igexin.push.core.f.a().g().a("C-" + com.igexin.push.core.g.r, cVar);
        com.igexin.b.a.c.a.b("CoreAction|cdnreceive " + pushTaskBean.getTaskId() + "|" + pushTaskBean.getMessageId());
    }

    public void a(PushTaskBean pushTaskBean, String str) {
        if (pushTaskBean.isCDNType()) {
            b(pushTaskBean, str);
        } else {
            a(pushTaskBean, str, "ok");
        }
    }

    public void a(PushTaskBean pushTaskBean, String str, String str2) {
        long currentTimeMillis = System.currentTimeMillis();
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("action", "pushmessage_feedback");
            jSONObject.put("appid", pushTaskBean.getAppid());
            jSONObject.put("id", String.valueOf(currentTimeMillis));
            jSONObject.put(LogBuilder.KEY_APPKEY, pushTaskBean.getAppKey());
            jSONObject.put("messageid", pushTaskBean.getMessageId());
            jSONObject.put("taskid", pushTaskBean.getTaskId());
            jSONObject.put("actionid", str);
            jSONObject.put(SocketMessage.MSG_RESULE_KEY, str2);
            jSONObject.put(Parameters.TIMESTAMP, String.valueOf(System.currentTimeMillis()));
        } catch (Exception e2) {
        }
        String jSONObject2 = jSONObject.toString();
        com.igexin.push.d.c.d dVar = new com.igexin.push.d.c.d();
        dVar.a();
        dVar.f991a = (int) currentTimeMillis;
        dVar.d = "17258000";
        dVar.e = jSONObject2;
        dVar.g = com.igexin.push.core.g.r;
        com.igexin.push.core.f.a().g().a("C-" + com.igexin.push.core.g.r, dVar);
        com.igexin.push.core.b.d a2 = com.igexin.push.core.b.d.a();
        if (a2 != null) {
            a2.a(new com.igexin.push.core.bean.j(currentTimeMillis, jSONObject2, (byte) 3, currentTimeMillis));
        }
        com.igexin.b.a.c.a.b("feedback|" + pushTaskBean.getTaskId() + "|" + pushTaskBean.getMessageId() + "|" + str);
    }

    public void a(String str) {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("action", "received");
            jSONObject.put("id", str);
        } catch (JSONException e2) {
        }
        String jSONObject2 = jSONObject.toString();
        com.igexin.push.d.c.d dVar = new com.igexin.push.d.c.d();
        dVar.a();
        dVar.f991a = (int) System.currentTimeMillis();
        dVar.d = "17258000";
        dVar.e = jSONObject2;
        dVar.g = com.igexin.push.core.g.r;
        com.igexin.push.core.f.a().g().a("C-" + com.igexin.push.core.g.r, dVar);
    }

    public void a(String str, com.igexin.push.d.c.a aVar, PushTaskBean pushTaskBean) {
        com.igexin.b.a.b.c.b().a(new com.igexin.push.f.a.a(new com.igexin.push.core.c.c(str, aVar, pushTaskBean)), false, true);
    }

    public void a(String str, String str2) {
        if (!TextUtils.isEmpty(com.igexin.push.core.g.r)) {
            try {
                long currentTimeMillis = System.currentTimeMillis();
                JSONObject jSONObject = new JSONObject();
                try {
                    jSONObject.put("action", "set_tag");
                    jSONObject.put("id", String.valueOf(currentTimeMillis));
                    jSONObject.put(IXAdRequestInfo.CELL_ID, com.igexin.push.core.g.r);
                    jSONObject.put("appid", com.igexin.push.core.g.f974a);
                    jSONObject.put("tags", URLEncoder.encode(str, StringUtil.Encoding));
                    jSONObject.put(IXAdRequestInfo.SN, str2);
                } catch (Exception e2) {
                }
                String jSONObject2 = jSONObject.toString();
                com.igexin.push.core.b.d a2 = com.igexin.push.core.b.d.a();
                if (a2 != null) {
                    a2.a(new com.igexin.push.core.bean.j(currentTimeMillis, jSONObject2, (byte) 2, currentTimeMillis));
                }
                com.igexin.push.d.c.d dVar = new com.igexin.push.d.c.d();
                dVar.a();
                dVar.d = "17258000";
                dVar.e = jSONObject2;
                com.igexin.b.a.b.c.b().a(SDKUrlConfig.getCmAddress(), 3, com.igexin.push.core.f.a().f(), dVar, false);
                com.igexin.b.a.c.a.b("settag");
            } catch (Exception e3) {
            }
        }
    }

    @TargetApi(12)
    public void a(String str, String str2, String str3, String str4) {
        if (com.igexin.push.core.g.f != null) {
            com.igexin.b.a.c.a.b("startapp|broadcastPayload");
            byte[] bArr = null;
            if (str4 != null) {
                bArr = str4.getBytes();
            } else {
                PushTaskBean pushTaskBean = com.igexin.push.core.g.ai.get(c(str, str2));
                if (pushTaskBean != null) {
                    bArr = pushTaskBean.getMsgExtra();
                }
            }
            if (bArr != null) {
                com.igexin.b.a.c.a.b("startapp|broadcast|payload = " + new String(bArr));
            } else {
                com.igexin.b.a.c.a.b("startapp|broadcast|payload is empty!");
            }
            try {
                Class b2 = b(com.igexin.push.core.g.f);
                if (!(b2 == null || com.igexin.push.core.g.f974a == null || !com.igexin.push.core.g.f974a.equals(str3))) {
                    Intent intent = new Intent(com.igexin.push.core.g.f, b2);
                    Bundle bundle = new Bundle();
                    bundle.putInt("action", 10001);
                    bundle.putSerializable(PushConsts.KEY_MESSAGE_DATA, new GTTransmitMessage(str, str2, str2 + ":" + str, bArr));
                    intent.putExtras(bundle);
                    com.igexin.push.core.g.f.startService(intent);
                }
            } catch (Throwable th) {
                com.igexin.b.a.c.a.b("CoreAction|" + th.toString());
            }
            a(str, str2, str3, bArr);
        }
    }

    public void a(String str, String str2, String str3, String str4, long j) {
        if (com.igexin.push.core.g.f != null) {
            try {
                Class b2 = b(com.igexin.push.core.g.f);
                if (!(b2 == null || com.igexin.push.core.g.f974a == null || !com.igexin.push.core.g.f974a.equals(str))) {
                    Intent intent = new Intent(com.igexin.push.core.g.f, b2);
                    Bundle bundle = new Bundle();
                    bundle.putInt("action", PushConsts.KEY_CMD_RESULT);
                    bundle.putSerializable(PushConsts.KEY_CMD_MSG, new FeedbackCmdMessage(str2, str3, str4, j, PushConsts.THIRDPART_FEEDBACK));
                    intent.putExtras(bundle);
                    com.igexin.push.core.g.f.startService(intent);
                }
            } catch (Throwable th) {
                com.igexin.b.a.c.a.b("CoreAction|" + th.toString());
            }
            Intent E = E();
            Bundle bundle2 = new Bundle();
            bundle2.putInt("action", PushConsts.THIRDPART_FEEDBACK);
            bundle2.putString("appid", str);
            bundle2.putString("taskid", str2);
            bundle2.putString("actionid", str3);
            bundle2.putString(SocketMessage.MSG_RESULE_KEY, str4);
            bundle2.putLong(Parameters.TIMESTAMP, j);
            E.putExtras(bundle2);
            com.igexin.push.core.g.f.sendBroadcast(E);
        }
    }

    public void a(String str, boolean z) {
        if (!z || !TextUtils.isEmpty(com.igexin.push.core.g.r)) {
            long currentTimeMillis = System.currentTimeMillis();
            if (currentTimeMillis - com.igexin.push.core.g.S > 5000) {
                String format = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date(currentTimeMillis));
                if (!format.equals(com.igexin.push.core.g.R)) {
                    com.igexin.push.core.b.g.a().f(format);
                    com.igexin.push.core.b.g.a().a(0);
                }
                if (com.igexin.push.core.g.T < 100) {
                    com.igexin.push.core.g.S = currentTimeMillis;
                    com.igexin.push.core.b.g.a().a(com.igexin.push.core.g.T + 1);
                    com.igexin.b.a.b.c.b().a(new com.igexin.push.f.a.e(new com.igexin.push.core.c.j(SDKUrlConfig.getAmpServiceUrl(), str, z)), false, true);
                }
            }
        }
    }

    public void a(String str, byte[] bArr) {
        if (com.igexin.push.core.g.r != null) {
            JSONObject jSONObject = new JSONObject();
            long currentTimeMillis = System.currentTimeMillis();
            try {
                jSONObject.put("action", "sendmessage");
                jSONObject.put("id", String.valueOf(currentTimeMillis));
                jSONObject.put(IXAdRequestInfo.CELL_ID, com.igexin.push.core.g.r);
                jSONObject.put("appid", com.igexin.push.core.g.f974a);
                jSONObject.put("taskid", str);
                jSONObject.put("extraData", com.igexin.push.util.i.b(bArr, 0));
                String jSONObject2 = jSONObject.toString();
                com.igexin.push.core.b.d.a().a(new com.igexin.push.core.bean.j(currentTimeMillis, jSONObject2, (byte) 6, currentTimeMillis));
                com.igexin.push.d.c.d dVar = new com.igexin.push.d.c.d();
                dVar.a();
                dVar.f991a = (int) currentTimeMillis;
                dVar.d = com.igexin.push.core.g.r;
                dVar.e = jSONObject2;
                dVar.f = bArr;
                dVar.g = com.igexin.push.core.g.r;
                com.igexin.push.core.f.a().g().a("C-" + com.igexin.push.core.g.r, dVar);
                if (str != null && str.startsWith("4T5@S_")) {
                    com.igexin.b.a.c.a.b("CoreAction sending lbs report message : " + jSONObject2);
                }
            } catch (Throwable th) {
                com.igexin.b.a.c.a.b("CoreAction|" + th.toString());
            }
        }
    }

    public void a(boolean z) {
    }

    public boolean a(com.igexin.b.a.d.d dVar) {
        return false;
    }

    public boolean a(com.igexin.push.d.c.e eVar) {
        if (eVar == null) {
            return false;
        }
        a aVar = f895a.get(Integer.valueOf(eVar.i));
        if ((eVar instanceof com.igexin.push.d.c.j) || (eVar instanceof com.igexin.push.d.c.m) || (eVar instanceof o) || (eVar instanceof q) || (eVar instanceof com.igexin.push.d.c.h)) {
            com.igexin.b.a.c.a.b("CoreAction|receive : " + eVar.getClass().getName() + " resp ~~~~");
            com.igexin.b.a.b.a.a.d.a().a(eVar.getClass().getName());
        }
        if ((eVar instanceof com.igexin.push.d.c.m) || (eVar instanceof o) || (eVar instanceof q)) {
            com.igexin.push.core.g.E = 0;
            com.igexin.push.c.i.a().e().b();
        }
        if (aVar != null) {
            aVar.a(eVar);
        }
        com.igexin.push.f.b.c.g().h();
        return true;
    }

    public boolean a(Object obj) {
        com.igexin.push.e.j g = com.igexin.push.core.f.a().g();
        if ((obj instanceof com.igexin.push.d.c.e) && g != null) {
            g.a((com.igexin.push.d.c.e) obj);
        } else if (obj instanceof com.igexin.push.d.b.a) {
            com.igexin.b.a.b.a.a.d.a().a(false);
        } else if (obj instanceof com.igexin.push.d.b.b) {
            com.igexin.b.a.c.a.b("CoreAction|ReconnectCheckNotifyType ###");
            com.igexin.b.a.b.a.a.d.a().a(((com.igexin.push.d.b.b) obj).a());
        } else if (obj instanceof com.igexin.push.d.b.d) {
            C();
        } else if (obj instanceof com.igexin.push.d.b.c) {
            com.igexin.b.a.c.a.b("CoreAction|ReconnectNotifyType ###");
            f(((com.igexin.push.d.b.c) obj).a());
        }
        return false;
    }

    public boolean a(String str, String str2, String str3) {
        Bundle bundle = new Bundle();
        bundle.putString("taskid", str);
        bundle.putString("messageid", str2);
        bundle.putString("actionid", str3);
        Message message = new Message();
        message.what = com.igexin.push.core.a.g;
        message.obj = bundle;
        return com.igexin.push.core.f.a().a(message);
    }

    /* JADX WARNING: Removed duplicated region for block: B:26:0x008f A[Catch:{ Throwable -> 0x00c1 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean a(org.json.JSONObject r11, com.igexin.push.core.bean.PushTaskBean r12) {
        /*
            r10 = this;
            r3 = 1
            r2 = 0
            java.util.ArrayList r6 = new java.util.ArrayList
            r6.<init>()
            java.lang.String r0 = "action_chains"
            org.json.JSONArray r7 = r11.getJSONArray(r0)     // Catch:{ Throwable -> 0x00c1 }
            r1 = r2
        L_0x000e:
            int r0 = r7.length()     // Catch:{ Throwable -> 0x00c1 }
            if (r1 >= r0) goto L_0x0067
            java.lang.Object r0 = r7.get(r1)     // Catch:{ Throwable -> 0x00c1 }
            org.json.JSONObject r0 = (org.json.JSONObject) r0     // Catch:{ Throwable -> 0x00c1 }
            java.lang.String r4 = "type"
            java.lang.String r4 = r0.getString(r4)     // Catch:{ Throwable -> 0x00c1 }
            if (r4 == 0) goto L_0x0063
            com.igexin.push.extension.a r0 = com.igexin.push.extension.a.a()     // Catch:{ Throwable -> 0x00c1 }
            java.util.List r0 = r0.c()     // Catch:{ Throwable -> 0x00c1 }
            java.util.Iterator r5 = r0.iterator()     // Catch:{ Throwable -> 0x00c1 }
        L_0x002e:
            boolean r0 = r5.hasNext()     // Catch:{ Throwable -> 0x00c1 }
            if (r0 == 0) goto L_0x00e3
            java.lang.Object r0 = r5.next()     // Catch:{ Throwable -> 0x00c1 }
            com.igexin.push.extension.stub.IPushExtension r0 = (com.igexin.push.extension.stub.IPushExtension) r0     // Catch:{ Throwable -> 0x00c1 }
            boolean r0 = r0.isActionSupported(r4)     // Catch:{ Throwable -> 0x00c1 }
            if (r0 == 0) goto L_0x002e
            r0 = r3
        L_0x0041:
            if (r0 != 0) goto L_0x0063
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x00c1 }
            r0.<init>()     // Catch:{ Throwable -> 0x00c1 }
            java.lang.String r5 = "CoreAction|extension not suport type = "
            java.lang.StringBuilder r0 = r0.append(r5)     // Catch:{ Throwable -> 0x00c1 }
            java.lang.StringBuilder r0 = r0.append(r4)     // Catch:{ Throwable -> 0x00c1 }
            java.lang.String r0 = r0.toString()     // Catch:{ Throwable -> 0x00c1 }
            com.igexin.b.a.c.a.b(r0)     // Catch:{ Throwable -> 0x00c1 }
            java.util.Map<java.lang.String, com.igexin.push.core.a.a.a> r0 = com.igexin.push.core.a.e.b     // Catch:{ Throwable -> 0x00c1 }
            java.lang.Object r0 = r0.get(r4)     // Catch:{ Throwable -> 0x00c1 }
            if (r0 != 0) goto L_0x0063
            r0 = r2
        L_0x0062:
            return r0
        L_0x0063:
            int r0 = r1 + 1
            r1 = r0
            goto L_0x000e
        L_0x0067:
            r5 = r2
        L_0x0068:
            int r0 = r7.length()     // Catch:{ Throwable -> 0x00c1 }
            if (r5 >= r0) goto L_0x00dc
            java.lang.Object r0 = r7.get(r5)     // Catch:{ Throwable -> 0x00c1 }
            org.json.JSONObject r0 = (org.json.JSONObject) r0     // Catch:{ Throwable -> 0x00c1 }
            java.lang.String r1 = "type"
            java.lang.String r8 = r0.getString(r1)     // Catch:{ Throwable -> 0x00c1 }
            if (r8 == 0) goto L_0x00bd
            com.igexin.push.extension.a r1 = com.igexin.push.extension.a.a()     // Catch:{ Throwable -> 0x00c1 }
            java.util.List r1 = r1.c()     // Catch:{ Throwable -> 0x00c1 }
            r4 = 0
            java.util.Iterator r9 = r1.iterator()     // Catch:{ Throwable -> 0x00c1 }
        L_0x0089:
            boolean r1 = r9.hasNext()     // Catch:{ Throwable -> 0x00c1 }
            if (r1 == 0) goto L_0x009b
            java.lang.Object r1 = r9.next()     // Catch:{ Throwable -> 0x00c1 }
            com.igexin.push.extension.stub.IPushExtension r1 = (com.igexin.push.extension.stub.IPushExtension) r1     // Catch:{ Throwable -> 0x00c1 }
            com.igexin.push.core.bean.BaseAction r4 = r1.parseAction(r0)     // Catch:{ Throwable -> 0x00c1 }
            if (r4 == 0) goto L_0x0089
        L_0x009b:
            if (r4 != 0) goto L_0x00e1
            java.util.Map<java.lang.String, com.igexin.push.core.a.a.a> r1 = com.igexin.push.core.a.e.b     // Catch:{ Throwable -> 0x00c1 }
            java.lang.Object r1 = r1.get(r8)     // Catch:{ Throwable -> 0x00c1 }
            com.igexin.push.core.a.a.a r1 = (com.igexin.push.core.a.a.a) r1     // Catch:{ Throwable -> 0x00c1 }
            if (r1 == 0) goto L_0x00e1
            com.igexin.push.core.bean.BaseAction r0 = r1.a(r0)     // Catch:{ Throwable -> 0x00c1 }
            if (r0 == 0) goto L_0x00b1
            r1 = 0
            r0.setSupportExt(r1)     // Catch:{ Throwable -> 0x00c1 }
        L_0x00b1:
            if (r0 != 0) goto L_0x00ba
            java.lang.String r0 = "CoreAction|action chains can't parse, throw whole ++++++"
            com.igexin.b.a.c.a.b(r0)     // Catch:{ Throwable -> 0x00c1 }
            r0 = r2
            goto L_0x0062
        L_0x00ba:
            r6.add(r0)     // Catch:{ Throwable -> 0x00c1 }
        L_0x00bd:
            int r0 = r5 + 1
            r5 = r0
            goto L_0x0068
        L_0x00c1:
            r0 = move-exception
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "CoreAction|"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r0 = r0.toString()
            java.lang.StringBuilder r0 = r1.append(r0)
            java.lang.String r0 = r0.toString()
            com.igexin.b.a.c.a.b(r0)
        L_0x00dc:
            r12.setActionChains(r6)
            r0 = r3
            goto L_0x0062
        L_0x00e1:
            r0 = r4
            goto L_0x00b1
        L_0x00e3:
            r0 = r2
            goto L_0x0041
        */
        throw new UnsupportedOperationException("Method not decompiled: com.igexin.push.core.a.e.a(org.json.JSONObject, com.igexin.push.core.bean.PushTaskBean):boolean");
    }

    /* JADX WARNING: Removed duplicated region for block: B:65:0x0211 A[SYNTHETIC, Splitter:B:65:0x0211] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean a(org.json.JSONObject r13, byte[] r14, boolean r15) {
        /*
            r12 = this;
            java.lang.String r0 = "action"
            boolean r0 = r13.has(r0)     // Catch:{ Exception -> 0x01e6 }
            if (r0 == 0) goto L_0x01ca
            java.lang.String r0 = "action"
            java.lang.String r0 = r13.getString(r0)     // Catch:{ Exception -> 0x01e6 }
            java.lang.String r1 = "pushmessage"
            boolean r0 = r0.equals(r1)     // Catch:{ Exception -> 0x01e6 }
            if (r0 == 0) goto L_0x01ca
            java.lang.String r0 = "id"
            java.lang.String r0 = r13.getString(r0)     // Catch:{ Exception -> 0x01e6 }
            java.lang.String r1 = "appid"
            java.lang.String r1 = r13.getString(r1)     // Catch:{ Exception -> 0x01e6 }
            java.lang.String r2 = "messageid"
            java.lang.String r7 = r13.getString(r2)     // Catch:{ Exception -> 0x01e6 }
            java.lang.String r2 = "taskid"
            java.lang.String r8 = r13.getString(r2)     // Catch:{ Exception -> 0x01e6 }
            java.lang.String r2 = "appkey"
            java.lang.String r2 = r13.getString(r2)     // Catch:{ Exception -> 0x01e6 }
            java.lang.String r3 = "action_chains"
            org.json.JSONArray r3 = r13.getJSONArray(r3)     // Catch:{ Exception -> 0x01e6 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01e6 }
            r4.<init>()     // Catch:{ Exception -> 0x01e6 }
            java.lang.String r5 = "pushmessage|"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x01e6 }
            java.lang.StringBuilder r4 = r4.append(r8)     // Catch:{ Exception -> 0x01e6 }
            java.lang.String r5 = "|"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x01e6 }
            java.lang.StringBuilder r4 = r4.append(r7)     // Catch:{ Exception -> 0x01e6 }
            java.lang.String r5 = "|"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x01e6 }
            java.lang.StringBuilder r4 = r4.append(r1)     // Catch:{ Exception -> 0x01e6 }
            java.lang.String r5 = "|"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x01e6 }
            java.lang.StringBuilder r4 = r4.append(r15)     // Catch:{ Exception -> 0x01e6 }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x01e6 }
            com.igexin.b.a.c.a.b(r4)     // Catch:{ Exception -> 0x01e6 }
            if (r1 == 0) goto L_0x0233
            if (r0 == 0) goto L_0x0233
            if (r7 == 0) goto L_0x0233
            if (r8 == 0) goto L_0x0233
            if (r3 == 0) goto L_0x0233
            java.lang.String r4 = com.igexin.push.core.g.f974a     // Catch:{ Exception -> 0x01e6 }
            boolean r4 = r1.equals(r4)     // Catch:{ Exception -> 0x01e6 }
            if (r4 == 0) goto L_0x0233
            com.igexin.push.core.bean.PushTaskBean r9 = new com.igexin.push.core.bean.PushTaskBean     // Catch:{ Exception -> 0x01e6 }
            r9.<init>()     // Catch:{ Exception -> 0x01e6 }
            r9.setAppid(r1)     // Catch:{ Exception -> 0x01e6 }
            r9.setMessageId(r7)     // Catch:{ Exception -> 0x01e6 }
            r9.setTaskId(r8)     // Catch:{ Exception -> 0x01e6 }
            r9.setId(r0)     // Catch:{ Exception -> 0x01e6 }
            r9.setAppKey(r2)     // Catch:{ Exception -> 0x01e6 }
            r0 = 1
            r9.setCurrentActionid(r0)     // Catch:{ Exception -> 0x01e6 }
            java.lang.String r0 = "cdnType"
            boolean r0 = r13.has(r0)     // Catch:{ Exception -> 0x01e6 }
            if (r0 == 0) goto L_0x00a9
            java.lang.String r0 = "cdnType"
            boolean r0 = r13.getBoolean(r0)     // Catch:{ Exception -> 0x01e6 }
            r9.setCDNType(r0)     // Catch:{ Exception -> 0x01e6 }
        L_0x00a9:
            com.igexin.push.core.a.e r0 = a()     // Catch:{ Exception -> 0x01e6 }
            java.lang.String r10 = r0.c(r8, r7)     // Catch:{ Exception -> 0x01e6 }
            if (r15 == 0) goto L_0x00d5
            com.igexin.push.core.a.e r0 = a()     // Catch:{ Exception -> 0x01e6 }
            java.lang.String r2 = "0"
            r0.a(r9, r2)     // Catch:{ Exception -> 0x01e6 }
            long r4 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x01e6 }
            boolean r0 = com.igexin.push.util.a.a(r4)     // Catch:{ Exception -> 0x01e6 }
            if (r0 == 0) goto L_0x00c8
            r0 = 1
        L_0x00c7:
            return r0
        L_0x00c8:
            boolean r0 = com.igexin.push.util.a.a(r13)     // Catch:{ Exception -> 0x01e6 }
            if (r0 == 0) goto L_0x00d5
            java.lang.String r0 = "CoreAction|message have loop"
            com.igexin.b.a.c.a.b(r0)     // Catch:{ Exception -> 0x01e6 }
            r0 = 1
            goto L_0x00c7
        L_0x00d5:
            android.content.ContentValues r11 = new android.content.ContentValues     // Catch:{ Exception -> 0x01e6 }
            r11.<init>()     // Catch:{ Exception -> 0x01e6 }
            java.lang.String r0 = "messageid"
            r11.put(r0, r7)     // Catch:{ Exception -> 0x01e6 }
            java.lang.String r0 = "taskid"
            r11.put(r0, r8)     // Catch:{ Exception -> 0x01e6 }
            java.lang.String r0 = "appid"
            r11.put(r0, r1)     // Catch:{ Exception -> 0x01e6 }
            java.lang.String r0 = "key"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01e6 }
            r1.<init>()     // Catch:{ Exception -> 0x01e6 }
            java.lang.String r2 = "CACHE_"
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x01e6 }
            java.lang.StringBuilder r1 = r1.append(r10)     // Catch:{ Exception -> 0x01e6 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x01e6 }
            r11.put(r0, r1)     // Catch:{ Exception -> 0x01e6 }
            java.lang.String r0 = "info"
            java.lang.String r1 = r13.toString()     // Catch:{ Exception -> 0x01e6 }
            byte[] r1 = r1.getBytes()     // Catch:{ Exception -> 0x01e6 }
            byte[] r1 = com.igexin.b.b.a.b(r1)     // Catch:{ Exception -> 0x01e6 }
            r11.put(r0, r1)     // Catch:{ Exception -> 0x01e6 }
            java.lang.String r0 = "createtime"
            long r4 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x01e6 }
            java.lang.Long r1 = java.lang.Long.valueOf(r4)     // Catch:{ Exception -> 0x01e6 }
            r11.put(r0, r1)     // Catch:{ Exception -> 0x01e6 }
            if (r14 == 0) goto L_0x0129
            java.lang.String r0 = "msgextra"
            r11.put(r0, r14)     // Catch:{ Exception -> 0x01e6 }
            r9.setMsgExtra(r14)     // Catch:{ Exception -> 0x01e6 }
        L_0x0129:
            int r0 = r3.length()     // Catch:{ Exception -> 0x01e6 }
            if (r0 <= 0) goto L_0x0140
            com.igexin.push.core.a.e r0 = a()     // Catch:{ Exception -> 0x01e6 }
            boolean r0 = r0.a(r13, r9)     // Catch:{ Exception -> 0x01e6 }
            if (r0 != 0) goto L_0x0140
            java.lang.String r0 = "CoreAction parseActionChains result = false #######"
            com.igexin.b.a.c.a.b(r0)     // Catch:{ Exception -> 0x01e6 }
            r0 = 1
            goto L_0x00c7
        L_0x0140:
            if (r15 == 0) goto L_0x021d
            r6 = 0
            com.igexin.push.core.f r0 = com.igexin.push.core.f.a()     // Catch:{ Exception -> 0x023c, all -> 0x0239 }
            com.igexin.push.b.b r0 = r0.k()     // Catch:{ Exception -> 0x023c, all -> 0x0239 }
            java.lang.String r1 = "message"
            r2 = 1
            java.lang.String[] r2 = new java.lang.String[r2]     // Catch:{ Exception -> 0x023c, all -> 0x0239 }
            r3 = 0
            java.lang.String r4 = "taskid"
            r2[r3] = r4     // Catch:{ Exception -> 0x023c, all -> 0x0239 }
            r3 = 1
            java.lang.String[] r3 = new java.lang.String[r3]     // Catch:{ Exception -> 0x023c, all -> 0x0239 }
            r4 = 0
            r3[r4] = r8     // Catch:{ Exception -> 0x023c, all -> 0x0239 }
            r4 = 0
            r5 = 0
            android.database.Cursor r1 = r0.a(r1, r2, r3, r4, r5)     // Catch:{ Exception -> 0x023c, all -> 0x0239 }
            if (r1 == 0) goto L_0x01c5
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01de, all -> 0x020e }
            r0.<init>()     // Catch:{ Exception -> 0x01de, all -> 0x020e }
            java.lang.String r2 = "CoreAction|taskid = "
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ Exception -> 0x01de, all -> 0x020e }
            java.lang.StringBuilder r0 = r0.append(r8)     // Catch:{ Exception -> 0x01de, all -> 0x020e }
            java.lang.String r2 = ", db cnt = "
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ Exception -> 0x01de, all -> 0x020e }
            int r2 = r1.getCount()     // Catch:{ Exception -> 0x01de, all -> 0x020e }
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ Exception -> 0x01de, all -> 0x020e }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x01de, all -> 0x020e }
            com.igexin.b.a.c.a.b(r0)     // Catch:{ Exception -> 0x01de, all -> 0x020e }
            int r0 = r1.getCount()     // Catch:{ Exception -> 0x01de, all -> 0x020e }
            if (r0 != 0) goto L_0x0215
            java.lang.String r0 = "condition"
            boolean r0 = r13.has(r0)     // Catch:{ Exception -> 0x01de, all -> 0x020e }
            if (r0 == 0) goto L_0x01cd
            r12.b(r13, r9)     // Catch:{ Exception -> 0x01de, all -> 0x020e }
            int r0 = com.igexin.push.core.a.k     // Catch:{ Exception -> 0x01de, all -> 0x020e }
            r9.setStatus(r0)     // Catch:{ Exception -> 0x01de, all -> 0x020e }
            java.lang.String r0 = "status"
            int r2 = com.igexin.push.core.a.k     // Catch:{ Exception -> 0x01de, all -> 0x020e }
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)     // Catch:{ Exception -> 0x01de, all -> 0x020e }
            r11.put(r0, r2)     // Catch:{ Exception -> 0x01de, all -> 0x020e }
        L_0x01a8:
            com.igexin.push.core.f r0 = com.igexin.push.core.f.a()     // Catch:{ Exception -> 0x01de, all -> 0x020e }
            com.igexin.push.b.b r0 = r0.k()     // Catch:{ Exception -> 0x01de, all -> 0x020e }
            java.lang.String r2 = "message"
            r0.a(r2, r11)     // Catch:{ Exception -> 0x01de, all -> 0x020e }
            java.util.Map<java.lang.String, com.igexin.push.core.bean.PushTaskBean> r0 = com.igexin.push.core.g.ai     // Catch:{ Exception -> 0x01de, all -> 0x020e }
            r0.put(r10, r9)     // Catch:{ Exception -> 0x01de, all -> 0x020e }
            java.lang.String r0 = "condition"
            boolean r0 = r13.has(r0)     // Catch:{ Exception -> 0x01de, all -> 0x020e }
            if (r0 == 0) goto L_0x0202
            r12.s()     // Catch:{ Exception -> 0x01de, all -> 0x020e }
        L_0x01c5:
            if (r1 == 0) goto L_0x01ca
            r1.close()     // Catch:{ Exception -> 0x01e6 }
        L_0x01ca:
            r0 = 1
            goto L_0x00c7
        L_0x01cd:
            int r0 = com.igexin.push.core.a.l     // Catch:{ Exception -> 0x01de, all -> 0x020e }
            r9.setStatus(r0)     // Catch:{ Exception -> 0x01de, all -> 0x020e }
            java.lang.String r0 = "status"
            int r2 = com.igexin.push.core.a.l     // Catch:{ Exception -> 0x01de, all -> 0x020e }
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)     // Catch:{ Exception -> 0x01de, all -> 0x020e }
            r11.put(r0, r2)     // Catch:{ Exception -> 0x01de, all -> 0x020e }
            goto L_0x01a8
        L_0x01de:
            r0 = move-exception
            r0 = r1
        L_0x01e0:
            if (r0 == 0) goto L_0x01ca
            r0.close()     // Catch:{ Exception -> 0x01e6 }
            goto L_0x01ca
        L_0x01e6:
            r0 = move-exception
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "CoreAction "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r0 = r0.toString()
            java.lang.StringBuilder r0 = r1.append(r0)
            java.lang.String r0 = r0.toString()
            com.igexin.b.a.c.a.b(r0)
            goto L_0x01ca
        L_0x0202:
            com.igexin.push.core.a.e r0 = a()     // Catch:{ Exception -> 0x01de, all -> 0x020e }
            java.lang.String r2 = com.igexin.push.core.g.f974a     // Catch:{ Exception -> 0x01de, all -> 0x020e }
            java.lang.String r3 = com.igexin.push.core.g.e     // Catch:{ Exception -> 0x01de, all -> 0x020e }
            r0.b(r8, r7, r2, r3)     // Catch:{ Exception -> 0x01de, all -> 0x020e }
            goto L_0x01c5
        L_0x020e:
            r0 = move-exception
        L_0x020f:
            if (r1 == 0) goto L_0x0214
            r1.close()     // Catch:{ Exception -> 0x01e6 }
        L_0x0214:
            throw r0     // Catch:{ Exception -> 0x01e6 }
        L_0x0215:
            r0 = 1
            if (r1 == 0) goto L_0x00c7
            r1.close()     // Catch:{ Exception -> 0x01e6 }
            goto L_0x00c7
        L_0x021d:
            java.lang.String r0 = "condition"
            boolean r0 = r13.has(r0)     // Catch:{ Exception -> 0x01e6 }
            if (r0 == 0) goto L_0x0228
            r12.b(r13, r9)     // Catch:{ Exception -> 0x01e6 }
        L_0x0228:
            int r0 = com.igexin.push.core.a.l     // Catch:{ Exception -> 0x01e6 }
            r9.setStatus(r0)     // Catch:{ Exception -> 0x01e6 }
            java.util.Map<java.lang.String, com.igexin.push.core.bean.PushTaskBean> r0 = com.igexin.push.core.g.ai     // Catch:{ Exception -> 0x01e6 }
            r0.put(r10, r9)     // Catch:{ Exception -> 0x01e6 }
            goto L_0x01ca
        L_0x0233:
            java.lang.String r0 = "CoreAction receieve error pushmessage +++++++++++++++++++"
            com.igexin.b.a.c.a.b(r0)     // Catch:{ Exception -> 0x01e6 }
            goto L_0x01ca
        L_0x0239:
            r0 = move-exception
            r1 = r6
            goto L_0x020f
        L_0x023c:
            r0 = move-exception
            r0 = r6
            goto L_0x01e0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.igexin.push.core.a.e.a(org.json.JSONObject, byte[], boolean):boolean");
    }

    public Class b(Context context) {
        return GTServiceManager.getInstance().getUserIntentService(context);
    }

    public void b() {
        d();
    }

    public void b(int i, String str) {
        m.e = i;
        com.igexin.push.config.a.a().d();
    }

    public void b(Intent intent) {
        if (intent != null && intent.hasExtra("isSlave")) {
            boolean booleanExtra = intent.getBooleanExtra("isSlave", false);
            com.igexin.b.a.c.a.b("CoreAction|onServiceInitializeForSlave isSlave =" + booleanExtra);
            if (booleanExtra) {
                com.igexin.push.core.f.a().a(true);
                if (intent.hasExtra("op_app")) {
                    com.igexin.push.core.g.D = intent.getStringExtra("op_app");
                } else {
                    com.igexin.push.core.g.D = "";
                }
                if (com.igexin.push.core.g.l) {
                    k();
                }
            }
        }
    }

    public void b(PushTaskBean pushTaskBean, String str) {
        if (pushTaskBean != null && !TextUtils.isEmpty(pushTaskBean.getMessageId())) {
            String str2 = pushTaskBean.getMessageId() + "|" + str;
            if (com.igexin.push.core.g.al.containsKey(str2)) {
                com.igexin.push.d.c.c cVar = com.igexin.push.core.g.al.get(str2);
                if (cVar.c() < 2) {
                    com.igexin.push.core.f.a().g().a("C-" + com.igexin.push.core.g.r, cVar);
                    cVar.a(cVar.c() + 1);
                    a(cVar, pushTaskBean, str, str2);
                }
            } else {
                com.igexin.push.d.c.c cVar2 = new com.igexin.push.d.c.c();
                long currentTimeMillis = System.currentTimeMillis();
                cVar2.a();
                cVar2.c = "FDB" + pushTaskBean.getMessageId() + "|" + pushTaskBean.getTaskId() + "|" + str + "|" + "ok" + "|" + currentTimeMillis;
                cVar2.d = com.igexin.push.core.g.r;
                cVar2.f990a = (int) currentTimeMillis;
                com.igexin.push.core.f.a().g().a("C-" + com.igexin.push.core.g.r, cVar2);
                a(cVar2, pushTaskBean, str, str2);
            }
            com.igexin.b.a.c.a.b("cdnfeedback|" + pushTaskBean.getTaskId() + "|" + pushTaskBean.getMessageId() + "|" + str);
        }
    }

    public void b(String str) {
        long currentTimeMillis = System.currentTimeMillis();
        if (currentTimeMillis - com.igexin.push.core.g.S > 5000) {
            String format = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date(currentTimeMillis));
            if (!format.equals(com.igexin.push.core.g.R)) {
                com.igexin.push.core.b.g.a().f(format);
                com.igexin.push.core.b.g.a().a(0);
            }
            com.igexin.b.a.c.a.b("-> CoreRuntimeInfo.opAliasTimes:" + com.igexin.push.core.g.T);
            if (com.igexin.push.core.g.T < 100) {
                com.igexin.b.a.c.a.b("requestService bindAlias HttpTask url : " + SDKUrlConfig.getAmpServiceUrl());
                com.igexin.push.core.g.S = currentTimeMillis;
                com.igexin.push.core.b.g.a().a(com.igexin.push.core.g.T + 1);
                com.igexin.b.a.b.c.b().a(new com.igexin.push.f.a.e(new com.igexin.push.core.c.b(SDKUrlConfig.getAmpServiceUrl(), str)), false, true);
            }
        }
    }

    public void b(String str, String str2) {
        if (com.igexin.push.core.g.f != null) {
            try {
                Class b2 = b(com.igexin.push.core.g.f);
                if (b2 != null) {
                    Intent intent = new Intent(com.igexin.push.core.g.f, b2);
                    Bundle bundle = new Bundle();
                    bundle.putInt("action", PushConsts.KEY_CMD_RESULT);
                    bundle.putSerializable(PushConsts.KEY_CMD_MSG, new SetTagCmdMessage(str, str2, PushConsts.SET_TAG_RESULT));
                    intent.putExtras(bundle);
                    com.igexin.push.core.g.f.startService(intent);
                }
            } catch (Throwable th) {
                com.igexin.b.a.c.a.b("CoreAction|" + th.toString());
            }
            Intent E = E();
            Bundle bundle2 = new Bundle();
            bundle2.putInt("action", PushConsts.SET_TAG_RESULT);
            bundle2.putString(IXAdRequestInfo.SN, str);
            bundle2.putString("code", str2);
            E.putExtras(bundle2);
            com.igexin.push.core.g.f.sendBroadcast(E);
        }
    }

    public void b(String str, String str2, String str3, String str4) {
        if (com.igexin.push.core.g.f != null) {
            Intent intent = new Intent("com.igexin.sdk.action.execute");
            intent.putExtra("taskid", str);
            intent.putExtra("messageid", str2);
            intent.putExtra("appid", com.igexin.push.core.g.f974a);
            intent.putExtra("pkgname", com.igexin.push.core.g.e);
            com.igexin.push.core.g.f.sendBroadcast(intent);
        }
    }

    public void b(boolean z) {
        com.igexin.b.a.b.c.b().a(new com.igexin.push.d.b.d());
        com.igexin.b.a.b.c.b().c();
        e(z);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.igexin.push.core.a.e.a(org.json.JSONObject, byte[], boolean):boolean
     arg types: [org.json.JSONObject, byte[], int]
     candidates:
      com.igexin.push.core.a.e.a(int, java.lang.String, java.lang.String):void
      com.igexin.push.core.a.e.a(com.igexin.push.core.a.e, java.lang.String, java.lang.String):void
      com.igexin.push.core.a.e.a(int, int, java.lang.String):void
      com.igexin.push.core.a.e.a(com.igexin.push.core.bean.PushTaskBean, java.lang.String, java.lang.String):void
      com.igexin.push.core.a.e.a(java.lang.String, com.igexin.push.d.c.a, com.igexin.push.core.bean.PushTaskBean):void
      com.igexin.push.core.a.e.a(java.lang.String, java.lang.String, java.lang.String):boolean
      com.igexin.push.core.a.e.a(org.json.JSONObject, byte[], boolean):boolean */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x00a9 A[SYNTHETIC, Splitter:B:24:0x00a9] */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x00d6  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x00f6  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean b(java.lang.String r12, java.lang.String r13, java.lang.String r14) {
        /*
            r11 = this;
            r9 = 0
            r8 = 1
            r7 = 0
            java.lang.String r10 = r11.c(r12, r13)
            java.util.Map<java.lang.String, com.igexin.push.core.bean.PushTaskBean> r0 = com.igexin.push.core.g.ai
            java.lang.Object r0 = r0.get(r10)
            r6 = r0
            com.igexin.push.core.bean.PushTaskBean r6 = (com.igexin.push.core.bean.PushTaskBean) r6
            if (r6 != 0) goto L_0x00a1
            com.igexin.push.core.f r0 = com.igexin.push.core.f.a()     // Catch:{ Throwable -> 0x00b0, all -> 0x00d2 }
            com.igexin.push.b.b r0 = r0.k()     // Catch:{ Throwable -> 0x00b0, all -> 0x00d2 }
            java.lang.String r1 = "message"
            r2 = 2
            java.lang.String[] r2 = new java.lang.String[r2]     // Catch:{ Throwable -> 0x00b0, all -> 0x00d2 }
            r3 = 0
            java.lang.String r4 = "taskid"
            r2[r3] = r4     // Catch:{ Throwable -> 0x00b0, all -> 0x00d2 }
            r3 = 1
            java.lang.String r4 = "messageid"
            r2[r3] = r4     // Catch:{ Throwable -> 0x00b0, all -> 0x00d2 }
            r3 = 2
            java.lang.String[] r3 = new java.lang.String[r3]     // Catch:{ Throwable -> 0x00b0, all -> 0x00d2 }
            r4 = 0
            r3[r4] = r12     // Catch:{ Throwable -> 0x00b0, all -> 0x00d2 }
            r4 = 1
            r3[r4] = r13     // Catch:{ Throwable -> 0x00b0, all -> 0x00d2 }
            r4 = 0
            r5 = 0
            android.database.Cursor r1 = r0.a(r1, r2, r3, r4, r5)     // Catch:{ Throwable -> 0x00b0, all -> 0x00d2 }
            if (r1 == 0) goto L_0x0040
            int r0 = r1.getCount()     // Catch:{ Throwable -> 0x0170 }
            if (r0 > 0) goto L_0x0048
        L_0x0040:
            if (r1 == 0) goto L_0x0045
            r1.close()
        L_0x0045:
            r0 = r7
        L_0x0046:
            return r0
        L_0x0047:
            r6 = r0
        L_0x0048:
            boolean r0 = r1.moveToNext()     // Catch:{ Throwable -> 0x0170 }
            if (r0 == 0) goto L_0x009c
            java.lang.String r0 = "info"
            int r0 = r1.getColumnIndexOrThrow(r0)     // Catch:{ Throwable -> 0x0170 }
            byte[] r0 = r1.getBlob(r0)     // Catch:{ Throwable -> 0x0170 }
            java.lang.String r2 = "msgextra"
            int r2 = r1.getColumnIndexOrThrow(r2)     // Catch:{ Throwable -> 0x0170 }
            byte[] r2 = r1.getBlob(r2)     // Catch:{ Throwable -> 0x0170 }
            java.lang.String r3 = new java.lang.String     // Catch:{ Throwable -> 0x0170 }
            byte[] r0 = com.igexin.b.b.a.c(r0)     // Catch:{ Throwable -> 0x0170 }
            r3.<init>(r0)     // Catch:{ Throwable -> 0x0170 }
            org.json.JSONObject r0 = new org.json.JSONObject     // Catch:{ Throwable -> 0x0170 }
            r0.<init>(r3)     // Catch:{ Throwable -> 0x0170 }
            r3 = 0
            r11.a(r0, r2, r3)     // Catch:{ Throwable -> 0x0170 }
            java.util.Map<java.lang.String, com.igexin.push.core.bean.PushTaskBean> r0 = com.igexin.push.core.g.ai     // Catch:{ Throwable -> 0x0170 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0170 }
            r2.<init>()     // Catch:{ Throwable -> 0x0170 }
            java.lang.StringBuilder r2 = r2.append(r12)     // Catch:{ Throwable -> 0x0170 }
            java.lang.String r3 = ":"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Throwable -> 0x0170 }
            java.lang.StringBuilder r2 = r2.append(r13)     // Catch:{ Throwable -> 0x0170 }
            java.lang.String r2 = r2.toString()     // Catch:{ Throwable -> 0x0170 }
            java.lang.Object r0 = r0.get(r2)     // Catch:{ Throwable -> 0x0170 }
            com.igexin.push.core.bean.PushTaskBean r0 = (com.igexin.push.core.bean.PushTaskBean) r0     // Catch:{ Throwable -> 0x0170 }
            if (r0 != 0) goto L_0x0047
            if (r1 == 0) goto L_0x009a
            r1.close()
        L_0x009a:
            r0 = r7
            goto L_0x0046
        L_0x009c:
            if (r1 == 0) goto L_0x00a1
            r1.close()
        L_0x00a1:
            int r0 = r6.getExecuteTimes()
            r1 = 50
            if (r0 < r1) goto L_0x00f6
            java.util.Map<java.lang.String, com.igexin.push.core.bean.PushTaskBean> r0 = com.igexin.push.core.g.ai     // Catch:{ Exception -> 0x00da }
            r0.remove(r10)     // Catch:{ Exception -> 0x00da }
        L_0x00ae:
            r0 = r8
            goto L_0x0046
        L_0x00b0:
            r0 = move-exception
            r1 = r9
        L_0x00b2:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x016d }
            r2.<init>()     // Catch:{ all -> 0x016d }
            java.lang.String r3 = "CoreAction|"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x016d }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x016d }
            java.lang.StringBuilder r0 = r2.append(r0)     // Catch:{ all -> 0x016d }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x016d }
            com.igexin.b.a.c.a.b(r0)     // Catch:{ all -> 0x016d }
            if (r1 == 0) goto L_0x00a1
            r1.close()
            goto L_0x00a1
        L_0x00d2:
            r0 = move-exception
            r1 = r9
        L_0x00d4:
            if (r1 == 0) goto L_0x00d9
            r1.close()
        L_0x00d9:
            throw r0
        L_0x00da:
            r0 = move-exception
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "CoreAction|"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r0 = r0.toString()
            java.lang.StringBuilder r0 = r1.append(r0)
            java.lang.String r0 = r0.toString()
            com.igexin.b.a.c.a.b(r0)
            goto L_0x00ae
        L_0x00f6:
            int r0 = r0 + 1
            r6.setExecuteTimes(r0)
            com.igexin.push.core.a.e r0 = a()
            r0.a(r6, r14)
            com.igexin.push.core.bean.BaseAction r1 = r6.getBaseAction(r14)     // Catch:{ Throwable -> 0x014f }
            if (r1 != 0) goto L_0x010b
            r0 = r7
            goto L_0x0046
        L_0x010b:
            boolean r0 = r1.isSupportExt()     // Catch:{ Throwable -> 0x014f }
            if (r0 == 0) goto L_0x0132
            com.igexin.push.extension.a r0 = com.igexin.push.extension.a.a()     // Catch:{ Throwable -> 0x014f }
            java.util.List r0 = r0.c()     // Catch:{ Throwable -> 0x014f }
            java.util.Iterator r2 = r0.iterator()     // Catch:{ Throwable -> 0x014f }
        L_0x011d:
            boolean r0 = r2.hasNext()     // Catch:{ Throwable -> 0x014f }
            if (r0 == 0) goto L_0x0132
            java.lang.Object r0 = r2.next()     // Catch:{ Throwable -> 0x014f }
            com.igexin.push.extension.stub.IPushExtension r0 = (com.igexin.push.extension.stub.IPushExtension) r0     // Catch:{ Throwable -> 0x014f }
            boolean r0 = r0.executeAction(r6, r1)     // Catch:{ Throwable -> 0x014f }
            if (r0 == 0) goto L_0x011d
            r0 = r8
            goto L_0x0046
        L_0x0132:
            java.util.Map<java.lang.String, com.igexin.push.core.a.a.a> r0 = com.igexin.push.core.a.e.b     // Catch:{ Throwable -> 0x014f }
            java.lang.String r2 = r1.getType()     // Catch:{ Throwable -> 0x014f }
            java.lang.Object r0 = r0.get(r2)     // Catch:{ Throwable -> 0x014f }
            com.igexin.push.core.a.a.a r0 = (com.igexin.push.core.a.a.a) r0     // Catch:{ Throwable -> 0x014f }
            if (r0 == 0) goto L_0x0146
            boolean r2 = r6.isStop()     // Catch:{ Throwable -> 0x014f }
            if (r2 == 0) goto L_0x0149
        L_0x0146:
            r0 = r7
            goto L_0x0046
        L_0x0149:
            boolean r0 = r0.b(r6, r1)     // Catch:{ Throwable -> 0x014f }
            goto L_0x0046
        L_0x014f:
            r0 = move-exception
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "CoreAction|"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r0 = r0.toString()
            java.lang.StringBuilder r0 = r1.append(r0)
            java.lang.String r0 = r0.toString()
            com.igexin.b.a.c.a.b(r0)
            r0 = r7
            goto L_0x0046
        L_0x016d:
            r0 = move-exception
            goto L_0x00d4
        L_0x0170:
            r0 = move-exception
            goto L_0x00b2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.igexin.push.core.a.e.b(java.lang.String, java.lang.String, java.lang.String):boolean");
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x004b A[SYNTHETIC, Splitter:B:14:0x004b] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0087 A[Catch:{ Throwable -> 0x008a }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.igexin.push.d.c.k c() {
        /*
            r6 = this;
            r5 = 1
            com.igexin.push.d.c.k r2 = new com.igexin.push.d.c.k
            r2.<init>()
            long r0 = com.igexin.push.core.g.q
            r2.f996a = r0
            r0 = 0
            r2.b = r0
            r0 = 65280(0xff00, float:9.1477E-41)
            r2.c = r0
            java.lang.String r0 = com.igexin.push.core.g.f974a
            r2.d = r0
            boolean r0 = com.igexin.push.util.a.a()     // Catch:{ Throwable -> 0x008a }
            r1 = -1
            if (r0 == 0) goto L_0x0089
            java.util.ArrayList r3 = new java.util.ArrayList     // Catch:{ Throwable -> 0x008a }
            r3.<init>()     // Catch:{ Throwable -> 0x008a }
            android.content.Context r0 = com.igexin.push.core.g.f     // Catch:{ Throwable -> 0x008e }
            java.lang.String r4 = "connectivity"
            java.lang.Object r0 = r0.getSystemService(r4)     // Catch:{ Throwable -> 0x008e }
            android.net.ConnectivityManager r0 = (android.net.ConnectivityManager) r0     // Catch:{ Throwable -> 0x008e }
            if (r0 == 0) goto L_0x0093
            android.net.NetworkInfo r0 = r0.getActiveNetworkInfo()     // Catch:{ Throwable -> 0x008e }
            if (r0 == 0) goto L_0x0093
            int r0 = r0.getType()     // Catch:{ Throwable -> 0x008e }
            com.igexin.push.d.c.l r1 = new com.igexin.push.d.c.l     // Catch:{ Throwable -> 0x0091 }
            r1.<init>()     // Catch:{ Throwable -> 0x0091 }
            r4 = 2
            r1.f997a = r4     // Catch:{ Throwable -> 0x0091 }
            java.lang.String r4 = java.lang.String.valueOf(r0)     // Catch:{ Throwable -> 0x0091 }
            r1.b = r4     // Catch:{ Throwable -> 0x0091 }
            r3.add(r1)     // Catch:{ Throwable -> 0x0091 }
        L_0x0049:
            if (r0 != r5) goto L_0x0081
            android.content.Context r0 = com.igexin.push.core.g.f     // Catch:{ Throwable -> 0x008c }
            java.lang.String r1 = "wifi"
            java.lang.Object r0 = r0.getSystemService(r1)     // Catch:{ Throwable -> 0x008c }
            android.net.wifi.WifiManager r0 = (android.net.wifi.WifiManager) r0     // Catch:{ Throwable -> 0x008c }
            android.net.wifi.WifiInfo r0 = r0.getConnectionInfo()     // Catch:{ Throwable -> 0x008c }
            if (r0 == 0) goto L_0x0081
            java.lang.String r1 = r0.getSSID()     // Catch:{ Throwable -> 0x008c }
            java.lang.String r0 = r0.getBSSID()     // Catch:{ Throwable -> 0x008c }
            if (r1 == 0) goto L_0x0072
            com.igexin.push.d.c.l r4 = new com.igexin.push.d.c.l     // Catch:{ Throwable -> 0x008c }
            r4.<init>()     // Catch:{ Throwable -> 0x008c }
            r5 = 1
            r4.f997a = r5     // Catch:{ Throwable -> 0x008c }
            r4.b = r1     // Catch:{ Throwable -> 0x008c }
            r3.add(r4)     // Catch:{ Throwable -> 0x008c }
        L_0x0072:
            if (r0 == 0) goto L_0x0081
            com.igexin.push.d.c.l r1 = new com.igexin.push.d.c.l     // Catch:{ Throwable -> 0x008c }
            r1.<init>()     // Catch:{ Throwable -> 0x008c }
            r4 = 4
            r1.f997a = r4     // Catch:{ Throwable -> 0x008c }
            r1.b = r0     // Catch:{ Throwable -> 0x008c }
            r3.add(r1)     // Catch:{ Throwable -> 0x008c }
        L_0x0081:
            boolean r0 = r3.isEmpty()     // Catch:{ Throwable -> 0x008a }
            if (r0 != 0) goto L_0x0089
            r2.e = r3     // Catch:{ Throwable -> 0x008a }
        L_0x0089:
            return r2
        L_0x008a:
            r0 = move-exception
            goto L_0x0089
        L_0x008c:
            r0 = move-exception
            goto L_0x0081
        L_0x008e:
            r0 = move-exception
            r0 = r1
            goto L_0x0049
        L_0x0091:
            r1 = move-exception
            goto L_0x0049
        L_0x0093:
            r0 = r1
            goto L_0x0049
        */
        throw new UnsupportedOperationException("Method not decompiled: com.igexin.push.core.a.e.c():com.igexin.push.d.c.k");
    }

    public String c(String str, String str2) {
        return str + ":" + str2;
    }

    public void c(Intent intent) {
        if (intent != null && intent.getAction() != null) {
            try {
                String action = intent.getAction();
                if (PushConsts.ACTION_BROADCAST_NETWORK_CHANGE.equals(action)) {
                    if (com.igexin.b.a.b.c.b() != null) {
                        D();
                    }
                } else if ("com.igexin.sdk.action.snlrefresh".equals(action) || com.igexin.push.core.g.V.equals(action) || "com.igexin.sdk.action.snlretire".equals(action)) {
                    com.igexin.push.core.f.a().h().a(intent);
                } else if ("com.igexin.sdk.action.execute".equals(action)) {
                    g(intent);
                } else if ("com.igexin.sdk.action.doaction".equals(action)) {
                    f(intent);
                } else if ("android.intent.action.TIME_SET".equals(action)) {
                    if (m.b != 0) {
                        com.igexin.push.a.a.c.c().d();
                    }
                } else if ("android.intent.action.SCREEN_ON".equals(action)) {
                    com.igexin.push.core.g.p = 1;
                    if (t()) {
                        s();
                    }
                } else if ("android.intent.action.SCREEN_OFF".equals(action)) {
                    com.igexin.push.core.g.p = 0;
                } else if ("android.intent.action.PACKAGE_ADDED".equals(action)) {
                    f(intent.getDataString());
                } else if ("android.intent.action.PACKAGE_REMOVED".equals(action)) {
                    e(intent.getDataString());
                } else if ("com.igexin.sdk.action.core.clearmsg".equals(action)) {
                    com.igexin.push.core.f.a().k().a("message", (String) null);
                } else if ("android.net.wifi.WIFI_STATE_CHANGED".equals(action) && intent.getIntExtra("wifi_state", 0) == 3) {
                    com.igexin.push.core.f.a().d();
                }
            } catch (Throwable th) {
                com.igexin.b.a.c.a.b("CoreAction" + th.toString());
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.igexin.push.core.a.e.a(boolean, int):java.lang.String
     arg types: [int, int]
     candidates:
      com.igexin.push.core.a.e.a(int, java.lang.String):void
      com.igexin.push.core.a.e.a(com.igexin.push.core.bean.PushTaskBean, java.lang.String):void
      com.igexin.push.core.a.e.a(java.lang.String, java.lang.String):void
      com.igexin.push.core.a.e.a(java.lang.String, boolean):void
      com.igexin.push.core.a.e.a(java.lang.String, byte[]):void
      com.igexin.push.core.a.e.a(org.json.JSONObject, com.igexin.push.core.bean.PushTaskBean):boolean
      com.igexin.push.core.a.e.a(boolean, int):java.lang.String */
    public void c(String str) {
        com.igexin.b.a.b.c.b().a(new com.igexin.push.f.a.c(new com.igexin.push.core.c.k(SDKUrlConfig.getBiUploadServiceUrl(), ((a(true, 4) + "2.10.1.0|sdkconfig-error|") + str).getBytes(), 0, true)), false, true);
    }

    public void c(boolean z) {
        f();
        d(z);
    }

    public int d() {
        if (!com.igexin.push.core.g.i || !com.igexin.push.core.g.j || com.igexin.push.util.a.a(System.currentTimeMillis()) || !com.igexin.push.util.a.b()) {
            com.igexin.b.a.c.a.b("CoreAction|keyNegotiate stop ++++++++++");
            return -1;
        }
        com.igexin.push.d.c.i iVar = new com.igexin.push.d.c.i();
        iVar.f994a = com.igexin.push.core.g.f974a;
        return !(com.igexin.push.core.f.a().g().a("K-", iVar, true) >= 0) ? 0 : 1;
    }

    public com.igexin.push.core.b d(String str, String str2) {
        com.igexin.push.core.b bVar;
        com.igexin.push.core.b bVar2 = com.igexin.push.core.b.success;
        int i = 0;
        PushTaskBean pushTaskBean = com.igexin.push.core.g.ai.get(c(str, str2));
        if (pushTaskBean == null) {
            return com.igexin.push.core.b.c;
        }
        Iterator<BaseAction> it = pushTaskBean.getActionChains().iterator();
        while (true) {
            int i2 = i;
            com.igexin.push.core.b bVar3 = bVar2;
            if (it.hasNext()) {
                BaseAction next = it.next();
                com.igexin.push.core.b bVar4 = com.igexin.push.core.b.c;
                if (next == null) {
                    return bVar4;
                }
                Iterator<IPushExtension> it2 = com.igexin.push.extension.a.a().c().iterator();
                while (true) {
                    if (!it2.hasNext()) {
                        bVar = bVar4;
                        break;
                    }
                    bVar4 = it2.next().prepareExecuteAction(pushTaskBean, next);
                    if (bVar4 != com.igexin.push.core.b.c) {
                        bVar = bVar4;
                        break;
                    }
                }
                if (bVar == com.igexin.push.core.b.c) {
                    a aVar = b.get(next.getType());
                    if (aVar == null) {
                        return bVar;
                    }
                    bVar = aVar.a(pushTaskBean, next);
                    if (bVar == com.igexin.push.core.b.c) {
                        return bVar;
                    }
                }
                bVar2 = bVar3 == com.igexin.push.core.b.success ? bVar : bVar3;
                i = bVar == com.igexin.push.core.b.wait ? i2 + 1 : i2;
            } else {
                if (i2 != 0 && !com.igexin.push.core.g.a(str, Integer.valueOf(i2), true)) {
                    bVar3 = com.igexin.push.core.b.success;
                }
                return bVar3;
            }
        }
    }

    public String d(String str) {
        if (com.igexin.push.core.g.b() == null) {
            return null;
        }
        return com.igexin.push.core.g.b().get(str);
    }

    public void d(Intent intent) {
        Map map;
        boolean z;
        if (intent != null) {
            try {
                int intExtra = intent.getIntExtra("id", -1);
                boolean booleanExtra = intent.getBooleanExtra(SocketMessage.MSG_RESULE_KEY, false);
                if (intExtra != -1) {
                    com.igexin.push.core.g.ar++;
                    if (booleanExtra) {
                        if (intent.getBooleanExtra("isReload", false)) {
                            Process.killProcess(Process.myPid());
                            return;
                        }
                        com.igexin.push.core.g.aq++;
                        Map<Integer, com.igexin.push.core.bean.f> b2 = com.igexin.push.core.g.as != null ? com.igexin.push.core.g.as.b() : null;
                        if (b2 != null) {
                            if (m.s != null) {
                                Map b3 = m.s.b();
                                if (b3 == null) {
                                    return;
                                }
                                if (b3.containsKey(Integer.valueOf(intExtra))) {
                                    z = true;
                                    com.igexin.push.core.bean.f fVar = (com.igexin.push.core.bean.f) b3.get(Integer.valueOf(intExtra));
                                    if (fVar != null) {
                                        com.igexin.push.util.e.b(fVar.c());
                                    }
                                    b3.remove(Integer.valueOf(intExtra));
                                    map = b3;
                                } else {
                                    z = false;
                                    map = b3;
                                }
                            } else {
                                Map hashMap = new HashMap();
                                com.igexin.push.core.bean.g gVar = new com.igexin.push.core.bean.g();
                                gVar.a("0");
                                gVar.a(hashMap);
                                m.s = gVar;
                                map = hashMap;
                                z = false;
                            }
                            com.igexin.push.core.bean.f fVar2 = (com.igexin.push.core.bean.f) b2.get(Integer.valueOf(intExtra));
                            if (fVar2 != null) {
                                String str = com.igexin.push.core.g.ac + "/" + fVar2.c();
                                if (new File(str).exists()) {
                                    map.put(Integer.valueOf(intExtra), fVar2);
                                    if (com.igexin.push.core.g.aq == com.igexin.push.core.g.ap) {
                                        m.s.a(com.igexin.push.core.g.as.a());
                                    }
                                    if (!z && com.igexin.push.extension.a.a().a(com.igexin.push.core.g.f, str, fVar2.d(), fVar2.j(), fVar2.c())) {
                                        com.igexin.b.a.c.a.b("CoreAction|load " + fVar2.d() + " success");
                                        fVar2.b(System.currentTimeMillis());
                                        if (fVar2.g()) {
                                            com.igexin.push.util.e.b(fVar2.c());
                                            map.remove(Integer.valueOf(intExtra));
                                        }
                                    }
                                    com.igexin.push.config.a.a().g();
                                }
                            } else {
                                return;
                            }
                        } else {
                            return;
                        }
                    }
                    if (com.igexin.push.core.g.ar == com.igexin.push.core.g.ap && com.igexin.push.core.g.at) {
                        com.igexin.b.a.c.a.b("CoreActiondownload ext success, restart service ###");
                        Process.killProcess(Process.myPid());
                    }
                }
            } catch (Exception e2) {
            }
        }
    }

    public void d(boolean z) {
        com.igexin.b.a.b.c.b().a(new com.igexin.push.d.b.b(z));
        com.igexin.b.a.b.c.b().c();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.igexin.push.e.j.a(java.lang.String, com.igexin.push.d.c.e, boolean):int
     arg types: [java.lang.String, com.igexin.push.d.c.f, int]
     candidates:
      com.igexin.push.e.j.a(android.content.Context, com.igexin.b.a.b.c, com.igexin.push.e.k):void
      com.igexin.push.e.j.a(java.lang.String, com.igexin.push.d.c.e, boolean):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.igexin.push.e.j.a(java.lang.String, com.igexin.push.d.c.e, boolean):int
     arg types: [java.lang.String, com.igexin.push.d.c.k, int]
     candidates:
      com.igexin.push.e.j.a(android.content.Context, com.igexin.b.a.b.c, com.igexin.push.e.k):void
      com.igexin.push.e.j.a(java.lang.String, com.igexin.push.d.c.e, boolean):int */
    public void e() {
        boolean z = false;
        if (com.igexin.push.core.g.k) {
            com.igexin.push.core.g.k = !com.igexin.push.core.g.k;
            com.igexin.push.core.g.L = (((long) Math.abs(new Random().nextInt() % 24)) * 3600000) + System.currentTimeMillis();
        }
        com.igexin.push.c.i.a().e().g();
        if (com.igexin.push.core.g.q == 0) {
            com.igexin.b.a.c.a.b("registerReq #####");
            if (com.igexin.push.core.f.a().g().a("R-" + com.igexin.push.core.g.B, (com.igexin.push.d.c.e) new com.igexin.push.d.c.f(com.igexin.push.core.g.t, com.igexin.push.core.g.u, com.igexin.push.core.g.B, com.igexin.push.core.g.f974a), true) >= 0) {
                z = true;
            }
            com.igexin.b.a.c.a.b("registerReq|" + z + "|" + com.igexin.push.core.g.B);
            return;
        }
        com.igexin.push.d.c.k c2 = c();
        com.igexin.b.a.c.a.b("loginReqBefore|" + c2.f996a);
        if (com.igexin.push.core.f.a().g().a("S-" + String.valueOf(com.igexin.push.core.g.q), (com.igexin.push.d.c.e) c2, true) >= 0) {
            z = true;
        }
        if (z) {
            com.igexin.b.a.c.a.b("CoreAction|loginReq|" + com.igexin.push.core.g.r);
        }
    }

    public void e(Intent intent) {
        try {
            String stringExtra = intent.getStringExtra("from");
            String stringExtra2 = intent.getStringExtra("did");
            if (TextUtils.isEmpty(stringExtra) || TextUtils.isEmpty(stringExtra2)) {
                com.igexin.b.a.c.a.b("CoreAction|doThirdGuardSt from or did is empty");
                return;
            }
            StringBuilder sb = new StringBuilder();
            sb.append(stringExtra).append("|").append(com.igexin.push.core.g.f.getPackageName()).append("|").append(stringExtra2).append("|").append(com.igexin.push.core.g.y).append("|").append(com.igexin.push.core.g.f974a).append("|").append(com.igexin.push.core.g.r).append("|").append(System.currentTimeMillis());
            ae.a().a(Constants.VIA_REPORT_TYPE_QQFAVORITES, sb.toString());
        } catch (Throwable th) {
            com.igexin.b.a.c.a.b("CoreAction|doThirdGuardSt exception: " + th.toString());
        }
    }

    public void e(boolean z) {
        com.igexin.b.a.b.c.b().a(new com.igexin.push.d.b.c(z));
        com.igexin.b.a.b.c.b().c();
    }

    public void f() {
        com.igexin.b.a.b.a.a.d.a().b();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.igexin.push.e.j.a(java.lang.String, com.igexin.push.d.c.e, boolean):int
     arg types: [java.lang.String, com.igexin.push.d.c.h, int]
     candidates:
      com.igexin.push.e.j.a(android.content.Context, com.igexin.b.a.b.c, com.igexin.push.e.k):void
      com.igexin.push.e.j.a(java.lang.String, com.igexin.push.d.c.e, boolean):int */
    public int g() {
        com.igexin.b.a.c.a.a("CoreAction|send heart beat data ........");
        return com.igexin.push.core.f.a().g().a("H-" + com.igexin.push.core.g.r, (com.igexin.push.d.c.e) new com.igexin.push.d.c.h(), true);
    }

    public void h() {
        try {
            for (com.igexin.push.core.bean.j next : com.igexin.push.core.b.d.a().b()) {
                if (next.d() + 20000 <= System.currentTimeMillis()) {
                    long currentTimeMillis = System.currentTimeMillis();
                    JSONObject jSONObject = new JSONObject(next.b());
                    com.igexin.push.d.c.d dVar = new com.igexin.push.d.c.d();
                    dVar.a();
                    dVar.f991a = (int) currentTimeMillis;
                    dVar.d = "17258000";
                    if (jSONObject.has("extraData")) {
                        dVar.f = com.igexin.push.util.i.a(jSONObject.optString("extraData").getBytes(), 0);
                        jSONObject.remove("extraData");
                    }
                    dVar.e = next.b();
                    dVar.g = com.igexin.push.core.g.r;
                    com.igexin.push.core.f.a().g().a("C-" + com.igexin.push.core.g.r, dVar);
                    com.igexin.b.a.c.a.b("freshral|" + next.b());
                    com.igexin.push.core.b.d.a().a(next.a());
                    next.a(System.currentTimeMillis() + 20000);
                    com.igexin.push.core.b.d.a().a(next);
                    return;
                }
            }
        } catch (Throwable th) {
            com.igexin.b.a.c.a.b("CoreActionfreshRAL error :" + th.toString());
        }
    }

    public void i() {
        long currentTimeMillis = System.currentTimeMillis();
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("action", "request_deviceid");
            jSONObject.put("id", String.valueOf(currentTimeMillis));
        } catch (JSONException e2) {
        }
        String jSONObject2 = jSONObject.toString();
        com.igexin.push.d.c.d dVar = new com.igexin.push.d.c.d();
        dVar.a();
        dVar.f991a = (int) currentTimeMillis;
        dVar.d = "17258000";
        dVar.e = jSONObject2;
        dVar.g = com.igexin.push.core.g.r;
        com.igexin.push.core.f.a().g().a("C-" + com.igexin.push.core.g.r, dVar);
        com.igexin.b.a.c.a.b("CoreAction|deviceidReq");
    }

    public void j() {
        boolean z = true;
        try {
            if ((System.currentTimeMillis() - com.igexin.push.core.g.G) - LogBuilder.MAX_INTERVAL <= 0) {
                z = false;
            }
            if (z) {
                com.igexin.push.core.b.g.a().b(0);
            }
            if (com.igexin.push.core.g.aC <= 5) {
                com.igexin.push.core.b.g.a().b(com.igexin.push.core.g.aC + 1);
                com.igexin.b.a.b.c.b().a(new g(this), false, true);
            }
        } catch (Throwable th) {
        }
    }

    public void k() {
        if (com.igexin.push.core.g.f != null) {
            Log.d("PushService", "clientid is " + com.igexin.push.core.g.r);
            com.igexin.b.a.c.a.b("broadcastClientid|" + com.igexin.push.core.g.r);
            try {
                Class b2 = b(com.igexin.push.core.g.f);
                if (b2 != null) {
                    Intent intent = new Intent(com.igexin.push.core.g.f, b2);
                    Bundle bundle = new Bundle();
                    bundle.putInt("action", PushConsts.GET_CLIENTID);
                    bundle.putString(PushConsts.KEY_CLIENT_ID, com.igexin.push.core.g.r);
                    intent.putExtras(bundle);
                    com.igexin.push.core.g.f.startService(intent);
                }
            } catch (Throwable th) {
                com.igexin.b.a.c.a.b("CoreAction|" + th.toString());
            }
            Intent E = E();
            Bundle bundle2 = new Bundle();
            bundle2.putInt("action", PushConsts.GET_CLIENTID);
            bundle2.putString(PushConsts.KEY_CLIENT_ID, com.igexin.push.core.g.r);
            E.putExtras(bundle2);
            com.igexin.push.core.g.f.sendBroadcast(E);
        }
    }

    public void l() {
        if (com.igexin.push.core.g.f != null) {
            try {
                Class b2 = b(com.igexin.push.core.g.f);
                if (b2 != null) {
                    Intent intent = new Intent(com.igexin.push.core.g.f, b2);
                    Bundle bundle = new Bundle();
                    bundle.putInt("action", PushConsts.GET_SDKONLINESTATE);
                    bundle.putBoolean(PushConsts.KEY_ONLINE_STATE, com.igexin.push.core.g.l);
                    intent.putExtras(bundle);
                    com.igexin.push.core.g.f.startService(intent);
                }
            } catch (Throwable th) {
                com.igexin.b.a.c.a.b("CoreAction|" + th.toString());
            }
            Intent E = E();
            Bundle bundle2 = new Bundle();
            bundle2.putInt("action", PushConsts.GET_SDKONLINESTATE);
            bundle2.putBoolean(PushConsts.KEY_ONLINE_STATE, com.igexin.push.core.g.l);
            E.putExtras(bundle2);
            com.igexin.push.core.g.f.sendBroadcast(E);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x002f A[SYNTHETIC, Splitter:B:14:0x002f] */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0034 A[SYNTHETIC, Splitter:B:17:0x0034] */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x0057 A[SYNTHETIC, Splitter:B:34:0x0057] */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x005c A[SYNTHETIC, Splitter:B:37:0x005c] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String m() {
        /*
            r6 = this;
            r1 = 0
            java.io.File r0 = new java.io.File
            java.lang.String r2 = com.igexin.push.core.g.Z
            r0.<init>(r2)
            boolean r0 = r0.exists()
            if (r0 == 0) goto L_0x0075
            r0 = 1024(0x400, float:1.435E-42)
            byte[] r0 = new byte[r0]
            java.io.FileInputStream r3 = new java.io.FileInputStream     // Catch:{ Exception -> 0x006d, all -> 0x0052 }
            java.lang.String r2 = com.igexin.push.core.g.Z     // Catch:{ Exception -> 0x006d, all -> 0x0052 }
            r3.<init>(r2)     // Catch:{ Exception -> 0x006d, all -> 0x0052 }
            java.io.ByteArrayOutputStream r2 = new java.io.ByteArrayOutputStream     // Catch:{ Exception -> 0x0071, all -> 0x0068 }
            r2.<init>()     // Catch:{ Exception -> 0x0071, all -> 0x0068 }
        L_0x001e:
            int r4 = r3.read(r0)     // Catch:{ Exception -> 0x002a, all -> 0x006b }
            r5 = -1
            if (r4 == r5) goto L_0x0039
            r5 = 0
            r2.write(r0, r5, r4)     // Catch:{ Exception -> 0x002a, all -> 0x006b }
            goto L_0x001e
        L_0x002a:
            r0 = move-exception
            r0 = r2
            r2 = r3
        L_0x002d:
            if (r2 == 0) goto L_0x0032
            r2.close()     // Catch:{ Exception -> 0x0062 }
        L_0x0032:
            if (r0 == 0) goto L_0x0075
            r0.close()     // Catch:{ Exception -> 0x004f }
            r0 = r1
        L_0x0038:
            return r0
        L_0x0039:
            byte[] r4 = r2.toByteArray()     // Catch:{ Exception -> 0x002a, all -> 0x006b }
            java.lang.String r0 = new java.lang.String     // Catch:{ Exception -> 0x002a, all -> 0x006b }
            r0.<init>(r4)     // Catch:{ Exception -> 0x002a, all -> 0x006b }
            if (r3 == 0) goto L_0x0047
            r3.close()     // Catch:{ Exception -> 0x0060 }
        L_0x0047:
            if (r2 == 0) goto L_0x0038
            r2.close()     // Catch:{ Exception -> 0x004d }
            goto L_0x0038
        L_0x004d:
            r1 = move-exception
            goto L_0x0038
        L_0x004f:
            r0 = move-exception
            r0 = r1
            goto L_0x0038
        L_0x0052:
            r0 = move-exception
            r2 = r1
            r3 = r1
        L_0x0055:
            if (r3 == 0) goto L_0x005a
            r3.close()     // Catch:{ Exception -> 0x0064 }
        L_0x005a:
            if (r2 == 0) goto L_0x005f
            r2.close()     // Catch:{ Exception -> 0x0066 }
        L_0x005f:
            throw r0
        L_0x0060:
            r1 = move-exception
            goto L_0x0047
        L_0x0062:
            r2 = move-exception
            goto L_0x0032
        L_0x0064:
            r1 = move-exception
            goto L_0x005a
        L_0x0066:
            r1 = move-exception
            goto L_0x005f
        L_0x0068:
            r0 = move-exception
            r2 = r1
            goto L_0x0055
        L_0x006b:
            r0 = move-exception
            goto L_0x0055
        L_0x006d:
            r0 = move-exception
            r0 = r1
            r2 = r1
            goto L_0x002d
        L_0x0071:
            r0 = move-exception
            r0 = r1
            r2 = r3
            goto L_0x002d
        L_0x0075:
            r0 = r1
            goto L_0x0038
        */
        throw new UnsupportedOperationException("Method not decompiled: com.igexin.push.core.a.e.m():java.lang.String");
    }

    public void n() {
        ArrayList arrayList = new ArrayList();
        a((List<n>) arrayList);
        if (!arrayList.isEmpty()) {
            JSONObject jSONObject = new JSONObject();
            try {
                jSONObject.put("action", "reportapplist");
                jSONObject.put("session_last", com.igexin.push.core.g.q);
                JSONArray jSONArray = new JSONArray();
                int size = arrayList.size();
                for (int i = 0; i < size; i++) {
                    JSONObject jSONObject2 = new JSONObject();
                    jSONObject2.put("appid", ((n) arrayList.get(i)).d());
                    jSONObject2.put(SelectCountryActivity.EXTRA_COUNTRY_NAME, ((n) arrayList.get(i)).b());
                    jSONObject2.put("version", ((n) arrayList.get(i)).c());
                    jSONObject2.put("versionName", ((n) arrayList.get(i)).a());
                    jSONArray.put(jSONObject2);
                }
                jSONObject.put("applist", jSONArray);
            } catch (Exception e2) {
            }
            com.igexin.b.a.b.c.b().a(new com.igexin.push.f.a.c(new com.igexin.push.core.c.a(SDKUrlConfig.getBiUploadServiceUrl(), jSONObject.toString().getBytes())), false, true);
            g(o());
            com.igexin.b.a.c.a.b("reportapplist");
        }
    }

    public String o() {
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        a((List<n>) arrayList2);
        int size = arrayList2.size();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                arrayList.add(((n) arrayList2.get(i)).d());
            }
        }
        return arrayList.toString();
    }

    public boolean p() {
        boolean z;
        boolean z2;
        boolean z3;
        boolean z4;
        boolean z5;
        boolean z6 = false;
        if (this.e.get()) {
            return this.d;
        }
        if (com.igexin.push.core.g.f == null) {
        }
        String packageName = com.igexin.push.core.g.f.getApplicationContext().getPackageName();
        try {
            ServiceInfo[] serviceInfoArr = com.igexin.push.core.g.f.getPackageManager().getPackageInfo(packageName, 4).services;
            if (serviceInfoArr != null) {
                int length = serviceInfoArr.length;
                int i = 0;
                while (true) {
                    if (i >= length) {
                        break;
                    } else if (serviceInfoArr[i].name.contains("DownloadService")) {
                        z3 = true;
                        break;
                    } else {
                        i++;
                    }
                }
            }
            z3 = false;
            try {
                ProviderInfo[] providerInfoArr = com.igexin.push.core.g.f.getPackageManager().getPackageInfo(packageName, 8).providers;
                if (providerInfoArr != null) {
                    int length2 = providerInfoArr.length;
                    int i2 = 0;
                    while (true) {
                        if (i2 >= length2) {
                            break;
                        } else if (providerInfoArr[i2].name.contains("DownloadProvider")) {
                            z4 = true;
                            break;
                        } else {
                            i2++;
                        }
                    }
                }
                z4 = false;
            } catch (Exception e2) {
                z2 = false;
                z = z3;
                z3 = z;
                z4 = z2;
                z5 = false;
                z6 = true;
                this.d = z6;
                this.e.set(true);
                return this.d;
            }
            try {
                ActivityInfo[] activityInfoArr = com.igexin.push.core.g.f.getPackageManager().getPackageInfo(packageName, 2).receivers;
                if (activityInfoArr != null) {
                    int length3 = activityInfoArr.length;
                    int i3 = 0;
                    while (true) {
                        if (i3 >= length3) {
                            break;
                        } else if (activityInfoArr[i3].name.contains("DownloadReceiver")) {
                            z5 = true;
                            break;
                        } else {
                            i3++;
                        }
                    }
                }
                z5 = false;
            } catch (Exception e3) {
                z2 = z4;
                z = z3;
                z3 = z;
                z4 = z2;
                z5 = false;
                z6 = true;
                this.d = z6;
                this.e.set(true);
                return this.d;
            }
        } catch (Exception e4) {
            z2 = false;
            z = false;
            z3 = z;
            z4 = z2;
            z5 = false;
            z6 = true;
            this.d = z6;
            this.e.set(true);
            return this.d;
        }
        if (z3 && z4 && z5) {
            z6 = true;
        }
        this.d = z6;
        this.e.set(true);
        return this.d;
    }

    public void q() {
        com.igexin.push.core.f.a().k().a("message", "createtime <= " + (System.currentTimeMillis() - 604800000));
    }

    public void r() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        String format = simpleDateFormat.format(new Date());
        File file = new File("/sdcard/libs//");
        String str = com.igexin.push.core.g.e;
        if (str == null) {
            str = "unknowPacageName";
        }
        if (file.exists()) {
            String[] list = file.list();
            int length = list.length;
            for (int i = 0; i < length; i++) {
                int length2 = list[i].length();
                if (list[i].startsWith(str) && list[i].endsWith(".log") && length2 > str.length() + 14 && str.equals(list[i].substring(0, length2 - 15))) {
                    try {
                        if (Math.abs((simpleDateFormat.parse(format).getTime() - simpleDateFormat.parse(list[i].substring(str.length() + 1, length2 - 4)).getTime()) / LogBuilder.MAX_INTERVAL) > 6) {
                            File file2 = new File("/sdcard/libs//" + list[i]);
                            if (file2.exists()) {
                                file2.delete();
                            }
                        }
                    } catch (Exception e2) {
                    }
                }
            }
        }
    }

    public void s() {
        try {
            if (!F()) {
                for (Map.Entry next : com.igexin.push.core.g.ai.entrySet()) {
                    String str = (String) next.getKey();
                    PushTaskBean pushTaskBean = (PushTaskBean) next.getValue();
                    String str2 = "";
                    if (pushTaskBean != null && pushTaskBean.getStatus() == com.igexin.push.core.a.k) {
                        String taskId = pushTaskBean.getTaskId();
                        Map<String, String> conditionMap = pushTaskBean.getConditionMap();
                        if (conditionMap == null) {
                            return;
                        }
                        if (!conditionMap.containsKey("endTime") || Long.valueOf(conditionMap.get("endTime")).longValue() >= System.currentTimeMillis()) {
                            if (conditionMap.containsKey(IXAdSystemUtils.NT_WIFI)) {
                                int intValue = Integer.valueOf(conditionMap.get(IXAdSystemUtils.NT_WIFI)).intValue();
                                v();
                                if (intValue != com.igexin.push.core.g.o) {
                                }
                            }
                            if (conditionMap.containsKey("screenOn")) {
                                int intValue2 = Integer.valueOf(conditionMap.get("screenOn")).intValue();
                                u();
                                if (intValue2 != com.igexin.push.core.g.p) {
                                }
                            }
                            if (conditionMap.containsKey("ssid")) {
                                String str3 = conditionMap.get("ssid");
                                w();
                                if (com.igexin.push.core.g.ao.containsValue(str3)) {
                                    str2 = str3;
                                }
                            }
                            if (conditionMap.containsKey("bssid")) {
                                String str4 = conditionMap.get("bssid");
                                if (com.igexin.push.core.g.ao.containsKey(str4)) {
                                    if (!com.igexin.push.core.g.ao.get(str4).equals(str2)) {
                                    }
                                }
                            }
                            if (!conditionMap.containsKey("startTime") || Long.valueOf(conditionMap.get("startTime")).longValue() <= System.currentTimeMillis()) {
                                a().b(taskId, pushTaskBean.getMessageId(), com.igexin.push.core.g.f974a, com.igexin.push.core.g.e);
                                a(com.igexin.push.core.a.l, taskId, str);
                                pushTaskBean.setStatus(com.igexin.push.core.a.l);
                            }
                        } else {
                            a(com.igexin.push.core.a.m, taskId, str);
                            pushTaskBean.setStatus(com.igexin.push.core.a.l);
                        }
                    }
                }
            }
        } catch (Exception e2) {
            com.igexin.b.a.c.a.b("CoreAction|" + e2.toString());
        }
    }

    public boolean t() {
        long currentTimeMillis = System.currentTimeMillis();
        if (com.igexin.push.core.g.I <= 0) {
            com.igexin.push.core.g.I = currentTimeMillis - 60000;
            return true;
        } else if (currentTimeMillis - com.igexin.push.core.g.I <= 60000) {
            return false;
        } else {
            com.igexin.push.core.g.I = currentTimeMillis;
            return true;
        }
    }

    public void u() {
        if (((PowerManager) com.igexin.push.core.g.f.getSystemService("power")).isScreenOn()) {
            com.igexin.push.core.g.p = 1;
        } else {
            com.igexin.push.core.g.p = 0;
        }
    }

    public void v() {
        NetworkInfo.State state = ((ConnectivityManager) com.igexin.push.core.g.f.getSystemService("connectivity")).getNetworkInfo(1).getState();
        if (state == NetworkInfo.State.CONNECTED || state == NetworkInfo.State.CONNECTING) {
            com.igexin.push.core.g.o = 1;
        } else {
            com.igexin.push.core.g.o = 0;
        }
    }

    public void w() {
        try {
            List<ScanResult> scanResults = ((WifiManager) com.igexin.push.core.g.f.getSystemService(IXAdSystemUtils.NT_WIFI)).getScanResults();
            com.igexin.push.core.g.ao.clear();
            if (scanResults != null && !scanResults.isEmpty()) {
                int i = 0;
                while (true) {
                    int i2 = i;
                    if (i2 < scanResults.size()) {
                        com.igexin.push.core.g.ao.put(scanResults.get(i2).BSSID, scanResults.get(i2).SSID);
                        i = i2 + 1;
                    } else {
                        return;
                    }
                }
            }
        } catch (Throwable th) {
            com.igexin.b.a.c.a.b("CoreAction|" + th.toString());
        }
    }

    public void x() {
        if (m.p && System.currentTimeMillis() - this.f >= 300000) {
            this.f = System.currentTimeMillis();
            Map<String, String> d2 = com.igexin.push.core.b.g.a().d();
            if (!d2.isEmpty() && m.E > 0) {
                int i = 0;
                Iterator<Map.Entry<String, String>> it = d2.entrySet().iterator();
                while (true) {
                    int i2 = i;
                    if (it.hasNext()) {
                        Map.Entry next = it.next();
                        if (i2 < m.E) {
                            String str = (String) next.getKey();
                            String str2 = (String) next.getValue();
                            if (!m.G || !com.igexin.push.util.a.a(str, "com.igexin.sdk.GActivity")) {
                                e(str, str2);
                            } else {
                                try {
                                    com.igexin.push.core.f.a().a(new i(this, (long) ((new Random().nextInt(6) + 1) * 1000), str, str2));
                                } catch (Exception e2) {
                                }
                            }
                            i = i2 + 1;
                        } else {
                            return;
                        }
                    } else {
                        return;
                    }
                }
            }
        }
    }

    public void y() {
        int i = com.igexin.push.core.g.an - 100;
        if (i < 0) {
            com.igexin.push.core.g.an = 0;
        } else {
            com.igexin.push.core.g.an = i;
        }
        long currentTimeMillis = System.currentTimeMillis();
        Iterator<Map.Entry<String, Long>> it = com.igexin.push.core.g.am.entrySet().iterator();
        while (it.hasNext()) {
            if (currentTimeMillis - ((Long) it.next().getValue()).longValue() > 3600000) {
                it.remove();
            }
        }
    }

    public void z() {
        if (com.igexin.push.core.g.P < System.currentTimeMillis()) {
            com.igexin.push.core.b.g.a().a(false);
        }
    }
}
