package com.igexin.push.core.a.a;

import com.igexin.b.a.b.c;
import com.igexin.b.a.c.a;
import com.igexin.push.config.SDKUrlConfig;
import com.igexin.push.config.l;
import com.igexin.push.config.m;
import com.igexin.push.core.b;
import com.igexin.push.core.bean.BaseAction;
import com.igexin.push.core.bean.PushTaskBean;
import com.igexin.push.core.bean.f;
import com.igexin.push.core.g;
import com.xiaomi.mipush.sdk.MiPushClient;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public class i implements a {

    /* renamed from: a  reason: collision with root package name */
    private static final String f890a = l.f884a;

    public b a(PushTaskBean pushTaskBean, BaseAction baseAction) {
        return b.success;
    }

    public BaseAction a(JSONObject jSONObject) {
        try {
            BaseAction baseAction = new BaseAction();
            baseAction.setType("reportext");
            baseAction.setActionId(jSONObject.getString("actionid"));
            baseAction.setDoActionId(jSONObject.getString("do"));
            return baseAction;
        } catch (JSONException e) {
            return null;
        }
    }

    public boolean b(PushTaskBean pushTaskBean, BaseAction baseAction) {
        boolean z;
        if (!(pushTaskBean == null || baseAction == null || m.s == null || m.s.b() == null || m.s.b().isEmpty())) {
            StringBuilder sb = new StringBuilder();
            try {
                File[] listFiles = new File(g.ac).listFiles();
                if (!(listFiles == null || listFiles.length == 0)) {
                    Map<Integer, f> b = m.s.b();
                    a.b(f890a + "|DynamicConfig.extInfos");
                    boolean z2 = true;
                    for (Map.Entry next : b.entrySet()) {
                        int intValue = ((Integer) next.getKey()).intValue();
                        f fVar = (f) next.getValue();
                        if (z2) {
                            sb.append(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).format(new Date()));
                            sb.append("|");
                            sb.append(g.r);
                            sb.append("|");
                            sb.append(g.f974a);
                            sb.append("|");
                            sb.append(g.N);
                            sb.append("|");
                            z = false;
                        } else {
                            z = z2;
                        }
                        for (File name : listFiles) {
                            if (name.getName().equals(fVar.c())) {
                                sb.append(intValue);
                                sb.append(MiPushClient.ACCEPT_TIME_SEPARATOR);
                                sb.append(fVar.b());
                                sb.append(MiPushClient.ACCEPT_TIME_SEPARATOR);
                                sb.append(fVar.c());
                                sb.append("|");
                            }
                        }
                        z2 = z;
                    }
                    a.b(f890a + "check ext data : " + sb.toString());
                    if (sb.length() > 0) {
                        c.b().a(new com.igexin.push.f.a.c(new com.igexin.push.core.c.l(SDKUrlConfig.getStatServiceUrl(), sb.toString().getBytes(), 17)), false, true);
                    }
                }
            } catch (Exception e) {
                a.b(f890a + " | " + e.toString());
            }
        }
        return true;
    }
}
