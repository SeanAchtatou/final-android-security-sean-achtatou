package com.igexin.push.core.a;

import com.baidu.mobads.interfaces.IXAdRequestInfo;
import com.igexin.b.a.c.a;
import com.igexin.push.config.l;
import org.json.JSONObject;

public class w extends b {

    /* renamed from: a  reason: collision with root package name */
    private static final String f909a = (l.f884a + "_SetTagResultAction");

    public boolean a(Object obj, JSONObject jSONObject) {
        a.b(f909a + "|set tag result resp data = " + jSONObject);
        if (jSONObject == null) {
            return true;
        }
        try {
            if (!jSONObject.has("action") || !jSONObject.getString("action").equals("settag_result")) {
                return true;
            }
            e.a().b(jSONObject.getString(IXAdRequestInfo.SN), jSONObject.getString("error_code"));
            return true;
        } catch (Exception e) {
            a.b(f909a + "|" + e.toString());
            return true;
        }
    }
}
