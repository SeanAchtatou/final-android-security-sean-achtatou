package com.igexin.push.core.bean;

public class j {

    /* renamed from: a  reason: collision with root package name */
    long f953a;
    String b;
    byte c;
    long d;

    public j(long j, String str, byte b2, long j2) {
        this.f953a = j;
        this.b = str;
        this.c = b2;
        this.d = j2;
    }

    public long a() {
        return this.f953a;
    }

    public void a(long j) {
        this.d = j;
    }

    public String b() {
        return this.b;
    }

    public byte c() {
        return this.c;
    }

    public long d() {
        return this.d;
    }
}
