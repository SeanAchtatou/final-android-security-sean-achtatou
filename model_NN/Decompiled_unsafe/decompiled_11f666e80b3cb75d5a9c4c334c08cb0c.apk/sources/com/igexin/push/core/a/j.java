package com.igexin.push.core.a;

import com.igexin.b.a.d.d;
import com.igexin.push.config.l;
import com.igexin.push.d.c.p;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONObject;

public class j extends a {

    /* renamed from: a  reason: collision with root package name */
    private static final String f900a = l.f884a;
    private static Map<String, b> b;

    public j() {
        b = new HashMap();
        b.put("redirect_server", new s());
        b.put("response_deviceid", new u());
        b.put("pushmessage", new q());
        b.put("received", new r());
        b.put("sendmessage_feedback", new v());
        b.put("block_client", new c());
        b.put("settag_result", new w());
    }

    public boolean a(d dVar) {
        return false;
    }

    public boolean a(Object obj) {
        b bVar;
        if (obj instanceof p) {
            p pVar = (p) obj;
            if (pVar.a() && pVar.e != null) {
                try {
                    JSONObject jSONObject = new JSONObject((String) pVar.e);
                    if (jSONObject.has("action") && !jSONObject.getString("action").equals("received") && jSONObject.has("id")) {
                        e.a().a(jSONObject.getString("id"));
                    }
                    if (jSONObject.has("action") && (bVar = b.get(jSONObject.getString("action"))) != null) {
                        return bVar.a(obj, jSONObject);
                    }
                } catch (Exception e) {
                }
            }
        }
        return false;
    }
}
