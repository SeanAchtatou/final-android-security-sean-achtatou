package com.igexin.push.core.c;

import android.content.Context;
import android.content.Intent;
import cn.banshenggua.aichang.room.message.SocketMessage;
import com.igexin.b.a.c.a;
import com.igexin.push.core.a.e;
import com.igexin.push.core.bean.f;
import com.igexin.push.core.g;

public class i implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private Context f966a;
    private f b;
    private boolean c;
    private int d;

    public i(Context context, f fVar, boolean z) {
        this.f966a = context;
        this.b = fVar;
        this.c = z;
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Type inference failed for: r3v0 */
    /* JADX WARN: Type inference failed for: r3v1, types: [java.net.HttpURLConnection] */
    /* JADX WARN: Type inference failed for: r3v2, types: [java.net.HttpURLConnection] */
    /* JADX WARN: Type inference failed for: r3v3, types: [java.io.InputStream] */
    /* JADX WARN: Type inference failed for: r3v4 */
    /* JADX WARN: Type inference failed for: r3v5 */
    /* JADX WARN: Type inference failed for: r3v6 */
    /* JADX WARN: Type inference failed for: r3v7 */
    /* JADX WARN: Type inference failed for: r3v8 */
    /* JADX WARN: Type inference failed for: r3v9 */
    /* JADX WARN: Type inference failed for: r3v11 */
    /* JADX WARN: Type inference failed for: r3v12 */
    /* JADX WARN: Type inference failed for: r3v13 */
    /* JADX WARN: Type inference failed for: r3v32 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x012d A[SYNTHETIC, Splitter:B:43:0x012d] */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x0132 A[SYNTHETIC, Splitter:B:46:0x0132] */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x0137  */
    /* JADX WARNING: Removed duplicated region for block: B:80:0x024a A[SYNTHETIC, Splitter:B:80:0x024a] */
    /* JADX WARNING: Removed duplicated region for block: B:83:0x024f A[SYNTHETIC, Splitter:B:83:0x024f] */
    /* JADX WARNING: Removed duplicated region for block: B:86:0x0254  */
    /* JADX WARNING: Removed duplicated region for block: B:92:0x027c A[SYNTHETIC, Splitter:B:92:0x027c] */
    /* JADX WARNING: Removed duplicated region for block: B:95:0x0281 A[SYNTHETIC, Splitter:B:95:0x0281] */
    /* JADX WARNING: Removed duplicated region for block: B:98:0x0286  */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean a(java.lang.String r12, java.lang.String r13, java.lang.String r14) {
        /*
            r11 = this;
            r3 = 0
            r2 = 3
            r1 = 0
            boolean r0 = android.text.TextUtils.isEmpty(r13)
            if (r0 == 0) goto L_0x0021
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r2 = "HttpExtensionDownload|downLoad ext name is invalid name = "
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.StringBuilder r0 = r0.append(r13)
            java.lang.String r0 = r0.toString()
            com.igexin.b.a.c.a.b(r0)
            r0 = r1
        L_0x0020:
            return r0
        L_0x0021:
            boolean r0 = android.text.TextUtils.isEmpty(r12)
            if (r0 == 0) goto L_0x004a
            r11.d = r2
            com.igexin.push.core.a.e r0 = com.igexin.push.core.a.e.a()
            java.lang.String r2 = "url is invalid"
            r0.c(r2)
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r2 = "HttpExtensionDownload|downLoad ext url is invalid, url = "
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.StringBuilder r0 = r0.append(r12)
            java.lang.String r0 = r0.toString()
            com.igexin.b.a.c.a.b(r0)
            r0 = r1
            goto L_0x0020
        L_0x004a:
            java.lang.String r0 = "http://"
            boolean r0 = r12.startsWith(r0)
            if (r0 != 0) goto L_0x0094
            java.lang.String r0 = "https://"
            boolean r0 = r12.startsWith(r0)
            if (r0 != 0) goto L_0x0094
            r11.d = r2
            com.igexin.push.core.a.e r0 = com.igexin.push.core.a.e.a()
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "httpUrl : "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r2 = r2.append(r12)
            java.lang.String r3 = " is invalid ..."
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            r0.c(r2)
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r2 = "HttpExtensionDownload|downLoad ext url is invalid url = "
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.StringBuilder r0 = r0.append(r12)
            java.lang.String r0 = r0.toString()
            com.igexin.b.a.c.a.b(r0)
            r0 = r1
            goto L_0x0020
        L_0x0094:
            r0 = 10
            android.os.Process.setThreadPriority(r0)
            r2 = 0
            r4 = 0
            java.net.URL r0 = new java.net.URL     // Catch:{ IllegalArgumentException -> 0x030c, Exception -> 0x02fa, all -> 0x02e4 }
            r0.<init>(r12)     // Catch:{ IllegalArgumentException -> 0x030c, Exception -> 0x02fa, all -> 0x02e4 }
            java.net.URLConnection r0 = r0.openConnection()     // Catch:{ IllegalArgumentException -> 0x030c, Exception -> 0x02fa, all -> 0x02e4 }
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ IllegalArgumentException -> 0x030c, Exception -> 0x02fa, all -> 0x02e4 }
            int r5 = r0.getResponseCode()     // Catch:{ IllegalArgumentException -> 0x0311, Exception -> 0x02ff, all -> 0x02e8 }
            r6 = 200(0xc8, float:2.8E-43)
            if (r5 == r6) goto L_0x00c6
            int r5 = r11.d     // Catch:{ IllegalArgumentException -> 0x0311, Exception -> 0x02ff, all -> 0x02e8 }
            int r5 = r5 + 1
            r11.d = r5     // Catch:{ IllegalArgumentException -> 0x0311, Exception -> 0x02ff, all -> 0x02e8 }
            if (r3 == 0) goto L_0x00b9
            r4.close()     // Catch:{ IOException -> 0x02c4 }
        L_0x00b9:
            if (r3 == 0) goto L_0x00be
            r2.close()     // Catch:{ IOException -> 0x02c7 }
        L_0x00be:
            if (r0 == 0) goto L_0x00c3
            r0.disconnect()
        L_0x00c3:
            r0 = r1
            goto L_0x0020
        L_0x00c6:
            java.io.InputStream r5 = r0.getInputStream()     // Catch:{ IllegalArgumentException -> 0x0311, Exception -> 0x02ff, all -> 0x02e8 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ IllegalArgumentException -> 0x0317, Exception -> 0x0306, all -> 0x02ee }
            r2.<init>()     // Catch:{ IllegalArgumentException -> 0x0317, Exception -> 0x0306, all -> 0x02ee }
            java.lang.String r4 = com.igexin.push.core.g.ac     // Catch:{ IllegalArgumentException -> 0x0317, Exception -> 0x0306, all -> 0x02ee }
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ IllegalArgumentException -> 0x0317, Exception -> 0x0306, all -> 0x02ee }
            java.lang.String r4 = "/"
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ IllegalArgumentException -> 0x0317, Exception -> 0x0306, all -> 0x02ee }
            java.lang.StringBuilder r2 = r2.append(r13)     // Catch:{ IllegalArgumentException -> 0x0317, Exception -> 0x0306, all -> 0x02ee }
            java.lang.String r2 = r2.toString()     // Catch:{ IllegalArgumentException -> 0x0317, Exception -> 0x0306, all -> 0x02ee }
            java.io.File r6 = new java.io.File     // Catch:{ IllegalArgumentException -> 0x0317, Exception -> 0x0306, all -> 0x02ee }
            r6.<init>(r2)     // Catch:{ IllegalArgumentException -> 0x0317, Exception -> 0x0306, all -> 0x02ee }
            java.io.FileOutputStream r4 = new java.io.FileOutputStream     // Catch:{ IllegalArgumentException -> 0x0317, Exception -> 0x0306, all -> 0x02ee }
            r4.<init>(r6)     // Catch:{ IllegalArgumentException -> 0x0317, Exception -> 0x0306, all -> 0x02ee }
            r3 = 1024(0x400, float:1.435E-42)
            byte[] r3 = new byte[r3]     // Catch:{ IllegalArgumentException -> 0x00fd, Exception -> 0x0225, all -> 0x0277 }
        L_0x00f1:
            int r7 = r5.read(r3)     // Catch:{ IllegalArgumentException -> 0x00fd, Exception -> 0x0225, all -> 0x0277 }
            r8 = -1
            if (r7 == r8) goto L_0x013d
            r8 = 0
            r4.write(r3, r8, r7)     // Catch:{ IllegalArgumentException -> 0x00fd, Exception -> 0x0225, all -> 0x0277 }
            goto L_0x00f1
        L_0x00fd:
            r2 = move-exception
            r3 = r5
            r10 = r4
            r4 = r0
            r0 = r2
            r2 = r10
        L_0x0103:
            r5 = 3
            r11.d = r5     // Catch:{ all -> 0x02f3 }
            com.igexin.push.core.a.e r5 = com.igexin.push.core.a.e.a()     // Catch:{ all -> 0x02f3 }
            java.lang.String r6 = r0.toString()     // Catch:{ all -> 0x02f3 }
            r5.c(r6)     // Catch:{ all -> 0x02f3 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x02f3 }
            r5.<init>()     // Catch:{ all -> 0x02f3 }
            java.lang.String r6 = "HttpExtensionDownload|"
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ all -> 0x02f3 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x02f3 }
            java.lang.StringBuilder r0 = r5.append(r0)     // Catch:{ all -> 0x02f3 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x02f3 }
            com.igexin.b.a.c.a.b(r0)     // Catch:{ all -> 0x02f3 }
            if (r2 == 0) goto L_0x0130
            r2.close()     // Catch:{ IOException -> 0x02d4 }
        L_0x0130:
            if (r3 == 0) goto L_0x0135
            r3.close()     // Catch:{ IOException -> 0x02d7 }
        L_0x0135:
            if (r4 == 0) goto L_0x013a
            r4.disconnect()
        L_0x013a:
            r0 = r1
            goto L_0x0020
        L_0x013d:
            android.content.Context r3 = r11.f966a     // Catch:{ IllegalArgumentException -> 0x00fd, Exception -> 0x0225, all -> 0x0277 }
            java.lang.String r2 = com.igexin.b.b.a.a(r3, r2)     // Catch:{ IllegalArgumentException -> 0x00fd, Exception -> 0x0225, all -> 0x0277 }
            boolean r2 = r2.equals(r14)     // Catch:{ IllegalArgumentException -> 0x00fd, Exception -> 0x0225, all -> 0x0277 }
            if (r2 == 0) goto L_0x028a
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ IllegalArgumentException -> 0x00fd, Exception -> 0x0225, all -> 0x0277 }
            r2.<init>()     // Catch:{ IllegalArgumentException -> 0x00fd, Exception -> 0x0225, all -> 0x0277 }
            java.lang.String r3 = com.igexin.push.core.g.ac     // Catch:{ IllegalArgumentException -> 0x00fd, Exception -> 0x0225, all -> 0x0277 }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ IllegalArgumentException -> 0x00fd, Exception -> 0x0225, all -> 0x0277 }
            java.lang.String r3 = "/"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ IllegalArgumentException -> 0x00fd, Exception -> 0x0225, all -> 0x0277 }
            com.igexin.push.core.bean.f r3 = r11.b     // Catch:{ IllegalArgumentException -> 0x00fd, Exception -> 0x0225, all -> 0x0277 }
            java.lang.String r3 = r3.c()     // Catch:{ IllegalArgumentException -> 0x00fd, Exception -> 0x0225, all -> 0x0277 }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ IllegalArgumentException -> 0x00fd, Exception -> 0x0225, all -> 0x0277 }
            java.lang.String r2 = r2.toString()     // Catch:{ IllegalArgumentException -> 0x00fd, Exception -> 0x0225, all -> 0x0277 }
            java.io.File r3 = new java.io.File     // Catch:{ IllegalArgumentException -> 0x00fd, Exception -> 0x0225, all -> 0x0277 }
            r3.<init>(r2)     // Catch:{ IllegalArgumentException -> 0x00fd, Exception -> 0x0225, all -> 0x0277 }
            r6.renameTo(r3)     // Catch:{ IllegalArgumentException -> 0x00fd, Exception -> 0x0225, all -> 0x0277 }
            com.igexin.push.core.bean.f r2 = r11.b     // Catch:{ IllegalArgumentException -> 0x00fd, Exception -> 0x0225, all -> 0x0277 }
            boolean r2 = r2.g()     // Catch:{ IllegalArgumentException -> 0x00fd, Exception -> 0x0225, all -> 0x0277 }
            if (r2 != 0) goto L_0x0259
            com.igexin.push.core.bean.f r2 = r11.b     // Catch:{ IllegalArgumentException -> 0x00fd, Exception -> 0x0225, all -> 0x0277 }
            long r6 = r2.h()     // Catch:{ IllegalArgumentException -> 0x00fd, Exception -> 0x0225, all -> 0x0277 }
            r8 = 0
            int r2 = (r6 > r8 ? 1 : (r6 == r8 ? 0 : -1))
            if (r2 != 0) goto L_0x0259
            java.io.File r2 = new java.io.File     // Catch:{ IllegalArgumentException -> 0x00fd, Exception -> 0x0225, all -> 0x0277 }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ IllegalArgumentException -> 0x00fd, Exception -> 0x0225, all -> 0x0277 }
            r6.<init>()     // Catch:{ IllegalArgumentException -> 0x00fd, Exception -> 0x0225, all -> 0x0277 }
            java.lang.String r7 = com.igexin.push.core.g.ab     // Catch:{ IllegalArgumentException -> 0x00fd, Exception -> 0x0225, all -> 0x0277 }
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ IllegalArgumentException -> 0x00fd, Exception -> 0x0225, all -> 0x0277 }
            java.lang.String r7 = "/"
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ IllegalArgumentException -> 0x00fd, Exception -> 0x0225, all -> 0x0277 }
            com.igexin.push.core.bean.f r7 = r11.b     // Catch:{ IllegalArgumentException -> 0x00fd, Exception -> 0x0225, all -> 0x0277 }
            java.lang.String r7 = r7.c()     // Catch:{ IllegalArgumentException -> 0x00fd, Exception -> 0x0225, all -> 0x0277 }
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ IllegalArgumentException -> 0x00fd, Exception -> 0x0225, all -> 0x0277 }
            java.lang.String r6 = r6.toString()     // Catch:{ IllegalArgumentException -> 0x00fd, Exception -> 0x0225, all -> 0x0277 }
            r2.<init>(r6)     // Catch:{ IllegalArgumentException -> 0x00fd, Exception -> 0x0225, all -> 0x0277 }
            boolean r6 = r2.exists()     // Catch:{ IllegalArgumentException -> 0x00fd, Exception -> 0x0225, all -> 0x0277 }
            if (r6 == 0) goto L_0x01ff
            android.content.Context r6 = com.igexin.push.core.g.f     // Catch:{ IllegalArgumentException -> 0x00fd, Exception -> 0x0225, all -> 0x0277 }
            java.lang.String r7 = r2.getAbsolutePath()     // Catch:{ IllegalArgumentException -> 0x00fd, Exception -> 0x0225, all -> 0x0277 }
            java.lang.String r6 = com.igexin.b.b.a.a(r6, r7)     // Catch:{ IllegalArgumentException -> 0x00fd, Exception -> 0x0225, all -> 0x0277 }
            com.igexin.push.core.bean.f r7 = r11.b     // Catch:{ IllegalArgumentException -> 0x00fd, Exception -> 0x0225, all -> 0x0277 }
            java.lang.String r7 = r7.f()     // Catch:{ IllegalArgumentException -> 0x00fd, Exception -> 0x0225, all -> 0x0277 }
            boolean r6 = r6.equals(r7)     // Catch:{ IllegalArgumentException -> 0x00fd, Exception -> 0x0225, all -> 0x0277 }
            if (r6 != 0) goto L_0x01ec
            r2.delete()     // Catch:{ IllegalArgumentException -> 0x00fd, Exception -> 0x0225, all -> 0x0277 }
            com.igexin.push.core.bean.f r6 = r11.b     // Catch:{ IllegalArgumentException -> 0x00fd, Exception -> 0x0225, all -> 0x0277 }
            java.lang.String r6 = r6.f()     // Catch:{ IllegalArgumentException -> 0x00fd, Exception -> 0x0225, all -> 0x0277 }
            com.igexin.push.util.e.a(r3, r2, r6)     // Catch:{ IllegalArgumentException -> 0x00fd, Exception -> 0x0225, all -> 0x0277 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ IllegalArgumentException -> 0x00fd, Exception -> 0x0225, all -> 0x0277 }
            r2.<init>()     // Catch:{ IllegalArgumentException -> 0x00fd, Exception -> 0x0225, all -> 0x0277 }
            java.lang.String r3 = "HttpExtensionDownload|downLoadFile success delete local tmp and copy ext name = "
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ IllegalArgumentException -> 0x00fd, Exception -> 0x0225, all -> 0x0277 }
            com.igexin.push.core.bean.f r3 = r11.b     // Catch:{ IllegalArgumentException -> 0x00fd, Exception -> 0x0225, all -> 0x0277 }
            java.lang.String r3 = r3.c()     // Catch:{ IllegalArgumentException -> 0x00fd, Exception -> 0x0225, all -> 0x0277 }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ IllegalArgumentException -> 0x00fd, Exception -> 0x0225, all -> 0x0277 }
            java.lang.String r2 = r2.toString()     // Catch:{ IllegalArgumentException -> 0x00fd, Exception -> 0x0225, all -> 0x0277 }
            com.igexin.b.a.c.a.b(r2)     // Catch:{ IllegalArgumentException -> 0x00fd, Exception -> 0x0225, all -> 0x0277 }
        L_0x01ec:
            r1 = 1
            if (r4 == 0) goto L_0x01f2
            r4.close()     // Catch:{ IOException -> 0x02ca }
        L_0x01f2:
            if (r5 == 0) goto L_0x01f7
            r5.close()     // Catch:{ IOException -> 0x02cd }
        L_0x01f7:
            if (r0 == 0) goto L_0x01fc
            r0.disconnect()
        L_0x01fc:
            r0 = r1
            goto L_0x0020
        L_0x01ff:
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ IllegalArgumentException -> 0x00fd, Exception -> 0x0225, all -> 0x0277 }
            r6.<init>()     // Catch:{ IllegalArgumentException -> 0x00fd, Exception -> 0x0225, all -> 0x0277 }
            java.lang.String r7 = "HttpExtensionDownload|downLoadFile success cope ext to local tmp name = "
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ IllegalArgumentException -> 0x00fd, Exception -> 0x0225, all -> 0x0277 }
            com.igexin.push.core.bean.f r7 = r11.b     // Catch:{ IllegalArgumentException -> 0x00fd, Exception -> 0x0225, all -> 0x0277 }
            java.lang.String r7 = r7.c()     // Catch:{ IllegalArgumentException -> 0x00fd, Exception -> 0x0225, all -> 0x0277 }
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ IllegalArgumentException -> 0x00fd, Exception -> 0x0225, all -> 0x0277 }
            java.lang.String r6 = r6.toString()     // Catch:{ IllegalArgumentException -> 0x00fd, Exception -> 0x0225, all -> 0x0277 }
            com.igexin.b.a.c.a.b(r6)     // Catch:{ IllegalArgumentException -> 0x00fd, Exception -> 0x0225, all -> 0x0277 }
            com.igexin.push.core.bean.f r6 = r11.b     // Catch:{ IllegalArgumentException -> 0x00fd, Exception -> 0x0225, all -> 0x0277 }
            java.lang.String r6 = r6.f()     // Catch:{ IllegalArgumentException -> 0x00fd, Exception -> 0x0225, all -> 0x0277 }
            com.igexin.push.util.e.a(r3, r2, r6)     // Catch:{ IllegalArgumentException -> 0x00fd, Exception -> 0x0225, all -> 0x0277 }
            goto L_0x01ec
        L_0x0225:
            r2 = move-exception
            r3 = r0
            r0 = r2
        L_0x0228:
            int r2 = r11.d     // Catch:{ all -> 0x02f8 }
            int r2 = r2 + 1
            r11.d = r2     // Catch:{ all -> 0x02f8 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x02f8 }
            r2.<init>()     // Catch:{ all -> 0x02f8 }
            java.lang.String r6 = "HttpExtensionDownload|"
            java.lang.StringBuilder r2 = r2.append(r6)     // Catch:{ all -> 0x02f8 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x02f8 }
            java.lang.StringBuilder r0 = r2.append(r0)     // Catch:{ all -> 0x02f8 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x02f8 }
            com.igexin.b.a.c.a.b(r0)     // Catch:{ all -> 0x02f8 }
            if (r4 == 0) goto L_0x024d
            r4.close()     // Catch:{ IOException -> 0x02da }
        L_0x024d:
            if (r5 == 0) goto L_0x0252
            r5.close()     // Catch:{ IOException -> 0x02dd }
        L_0x0252:
            if (r3 == 0) goto L_0x013a
            r3.disconnect()
            goto L_0x013a
        L_0x0259:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ IllegalArgumentException -> 0x00fd, Exception -> 0x0225, all -> 0x0277 }
            r2.<init>()     // Catch:{ IllegalArgumentException -> 0x00fd, Exception -> 0x0225, all -> 0x0277 }
            java.lang.String r3 = "HttpExtensionDownload|downLoadFile success do not copy ext to local tmp name = "
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ IllegalArgumentException -> 0x00fd, Exception -> 0x0225, all -> 0x0277 }
            com.igexin.push.core.bean.f r3 = r11.b     // Catch:{ IllegalArgumentException -> 0x00fd, Exception -> 0x0225, all -> 0x0277 }
            java.lang.String r3 = r3.c()     // Catch:{ IllegalArgumentException -> 0x00fd, Exception -> 0x0225, all -> 0x0277 }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ IllegalArgumentException -> 0x00fd, Exception -> 0x0225, all -> 0x0277 }
            java.lang.String r2 = r2.toString()     // Catch:{ IllegalArgumentException -> 0x00fd, Exception -> 0x0225, all -> 0x0277 }
            com.igexin.b.a.c.a.b(r2)     // Catch:{ IllegalArgumentException -> 0x00fd, Exception -> 0x0225, all -> 0x0277 }
            goto L_0x01ec
        L_0x0277:
            r1 = move-exception
            r3 = r0
            r0 = r1
        L_0x027a:
            if (r4 == 0) goto L_0x027f
            r4.close()     // Catch:{ IOException -> 0x02e0 }
        L_0x027f:
            if (r5 == 0) goto L_0x0284
            r5.close()     // Catch:{ IOException -> 0x02e2 }
        L_0x0284:
            if (r3 == 0) goto L_0x0289
            r3.disconnect()
        L_0x0289:
            throw r0
        L_0x028a:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ IllegalArgumentException -> 0x00fd, Exception -> 0x0225, all -> 0x0277 }
            r2.<init>()     // Catch:{ IllegalArgumentException -> 0x00fd, Exception -> 0x0225, all -> 0x0277 }
            java.lang.String r3 = "HttpExtensionDownload|download ext failed CheckSum error name = "
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ IllegalArgumentException -> 0x00fd, Exception -> 0x0225, all -> 0x0277 }
            com.igexin.push.core.bean.f r3 = r11.b     // Catch:{ IllegalArgumentException -> 0x00fd, Exception -> 0x0225, all -> 0x0277 }
            java.lang.String r3 = r3.c()     // Catch:{ IllegalArgumentException -> 0x00fd, Exception -> 0x0225, all -> 0x0277 }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ IllegalArgumentException -> 0x00fd, Exception -> 0x0225, all -> 0x0277 }
            java.lang.String r2 = r2.toString()     // Catch:{ IllegalArgumentException -> 0x00fd, Exception -> 0x0225, all -> 0x0277 }
            com.igexin.b.a.c.a.b(r2)     // Catch:{ IllegalArgumentException -> 0x00fd, Exception -> 0x0225, all -> 0x0277 }
            boolean r2 = r6.exists()     // Catch:{ IllegalArgumentException -> 0x00fd, Exception -> 0x0225, all -> 0x0277 }
            if (r2 == 0) goto L_0x02af
            r6.delete()     // Catch:{ IllegalArgumentException -> 0x00fd, Exception -> 0x0225, all -> 0x0277 }
        L_0x02af:
            r2 = 4
            r11.d = r2     // Catch:{ IllegalArgumentException -> 0x00fd, Exception -> 0x0225, all -> 0x0277 }
            if (r4 == 0) goto L_0x02b7
            r4.close()     // Catch:{ IOException -> 0x02d0 }
        L_0x02b7:
            if (r5 == 0) goto L_0x02bc
            r5.close()     // Catch:{ IOException -> 0x02d2 }
        L_0x02bc:
            if (r0 == 0) goto L_0x02c1
            r0.disconnect()
        L_0x02c1:
            r0 = r1
            goto L_0x0020
        L_0x02c4:
            r4 = move-exception
            goto L_0x00b9
        L_0x02c7:
            r2 = move-exception
            goto L_0x00be
        L_0x02ca:
            r2 = move-exception
            goto L_0x01f2
        L_0x02cd:
            r2 = move-exception
            goto L_0x01f7
        L_0x02d0:
            r2 = move-exception
            goto L_0x02b7
        L_0x02d2:
            r2 = move-exception
            goto L_0x02bc
        L_0x02d4:
            r0 = move-exception
            goto L_0x0130
        L_0x02d7:
            r0 = move-exception
            goto L_0x0135
        L_0x02da:
            r0 = move-exception
            goto L_0x024d
        L_0x02dd:
            r0 = move-exception
            goto L_0x0252
        L_0x02e0:
            r1 = move-exception
            goto L_0x027f
        L_0x02e2:
            r1 = move-exception
            goto L_0x0284
        L_0x02e4:
            r0 = move-exception
            r4 = r3
            r5 = r3
            goto L_0x027a
        L_0x02e8:
            r1 = move-exception
            r4 = r3
            r5 = r3
            r3 = r0
            r0 = r1
            goto L_0x027a
        L_0x02ee:
            r1 = move-exception
            r4 = r3
            r3 = r0
            r0 = r1
            goto L_0x027a
        L_0x02f3:
            r0 = move-exception
            r5 = r3
            r3 = r4
            r4 = r2
            goto L_0x027a
        L_0x02f8:
            r0 = move-exception
            goto L_0x027a
        L_0x02fa:
            r0 = move-exception
            r4 = r3
            r5 = r3
            goto L_0x0228
        L_0x02ff:
            r2 = move-exception
            r4 = r3
            r5 = r3
            r3 = r0
            r0 = r2
            goto L_0x0228
        L_0x0306:
            r2 = move-exception
            r4 = r3
            r3 = r0
            r0 = r2
            goto L_0x0228
        L_0x030c:
            r0 = move-exception
            r2 = r3
            r4 = r3
            goto L_0x0103
        L_0x0311:
            r2 = move-exception
            r4 = r0
            r0 = r2
            r2 = r3
            goto L_0x0103
        L_0x0317:
            r2 = move-exception
            r4 = r0
            r0 = r2
            r2 = r3
            r3 = r5
            goto L_0x0103
        */
        throw new UnsupportedOperationException("Method not decompiled: com.igexin.push.core.c.i.a(java.lang.String, java.lang.String, java.lang.String):boolean");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public void run() {
        do {
            try {
                a.b("HttpExtensionDownload|downloading " + this.b.c() + ".tmp");
                if (a(this.b.e(), this.b.c() + ".tmp", this.b.f())) {
                    a.b("HttpExtensionDownload|download " + this.b.c() + ".tmp, success ########");
                    Intent intent = new Intent(this.f966a, e.a().a(g.f));
                    intent.putExtra("action", "com.igexin.sdk.action.extdownloadsuccess");
                    intent.putExtra("id", this.b.a());
                    intent.putExtra(SocketMessage.MSG_RESULE_KEY, true);
                    intent.putExtra("isReload", this.c);
                    this.f966a.startService(intent);
                    return;
                }
                a.b("HttpExtensionDownload | download ext = " + this.b.c() + ", downloadFailedTimes = " + this.d);
            } catch (Throwable th) {
                a.b("HttpExtensionDownload|" + th.toString());
                return;
            }
        } while (this.d < 3);
        Intent intent2 = new Intent(this.f966a, e.a().a(g.f));
        intent2.putExtra("action", "com.igexin.sdk.action.extdownloadsuccess");
        intent2.putExtra("id", this.b.a());
        intent2.putExtra(SocketMessage.MSG_RESULE_KEY, false);
        this.f966a.startService(intent2);
    }
}
