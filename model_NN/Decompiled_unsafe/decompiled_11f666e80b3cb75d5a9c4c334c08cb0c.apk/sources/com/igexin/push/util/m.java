package com.igexin.push.util;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Looper;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import com.igexin.push.core.g;

public class m {
    public static String a() {
        try {
            return Build.VERSION.SDK_INT < 21 ? Build.CPU_ABI : Build.SUPPORTED_ABIS[0];
        } catch (Throwable th) {
            return "";
        }
    }

    public static String a(Context context) {
        try {
            return ((TelephonyManager) context.getSystemService("phone")).getDeviceId();
        } catch (Exception e) {
            return "";
        }
    }

    public static int b(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).applicationInfo.targetSdkVersion;
        } catch (Exception e) {
            return 0;
        }
    }

    public static String b() {
        try {
            return Settings.Secure.getString(g.f.getContentResolver(), "android_id");
        } catch (Throwable th) {
            return "";
        }
    }

    public static String c() {
        o oVar;
        String str = "";
        try {
            if (Looper.myLooper() != Looper.getMainLooper()) {
                try {
                    g.f.getPackageManager().getPackageInfo("com.android.vending", 0);
                    Intent intent = new Intent("com.google.android.gms.ads.identifier.service.START");
                    intent.setPackage("com.google.android.gms");
                    oVar = new o();
                    if (g.f.bindService(intent, oVar, 1)) {
                        str = new p(oVar.a()).a();
                        g.f.unbindService(oVar);
                    }
                } catch (Exception e) {
                }
            }
        } catch (Exception e2) {
            g.f.unbindService(oVar);
        } catch (Throwable th) {
        }
        return str;
    }
}
