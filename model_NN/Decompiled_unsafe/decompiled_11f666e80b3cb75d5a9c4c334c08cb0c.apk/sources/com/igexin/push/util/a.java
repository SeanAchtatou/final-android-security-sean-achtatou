package com.igexin.push.util;

import android.app.ActivityManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.pm.ServiceInfo;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Process;
import android.text.TextUtils;
import android.util.Log;
import com.igexin.push.config.m;
import com.igexin.push.core.bean.c;
import com.igexin.push.core.bean.f;
import com.igexin.push.core.bean.g;
import com.meizu.cloud.pushsdk.constants.PushConstants;
import com.xiaomi.mipush.sdk.MiPushClient;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;

public class a {

    /* renamed from: a  reason: collision with root package name */
    private static final String f1027a = a.class.getName();

    public static String a(Context context) {
        try {
            int myPid = Process.myPid();
            for (ActivityManager.RunningAppProcessInfo next : ((ActivityManager) context.getSystemService(PushConstants.INTENT_ACTIVITY_NAME)).getRunningAppProcesses()) {
                if (next.pid == myPid) {
                    return next.processName;
                }
            }
        } catch (Throwable th) {
        }
        return null;
    }

    public static void a(g gVar) {
        boolean z;
        com.igexin.push.core.g.ap = 0;
        com.igexin.push.core.g.aq = 0;
        com.igexin.push.core.g.as = gVar;
        Map<Integer, f> b = gVar.b();
        if (m.s != null) {
            Map<Integer, f> b2 = m.s.b();
            ArrayList<Integer> arrayList = new ArrayList<>();
            for (Map.Entry next : b2.entrySet()) {
                int intValue = ((Integer) next.getKey()).intValue();
                f fVar = (f) next.getValue();
                if (!b.containsKey(Integer.valueOf(intValue))) {
                    com.igexin.push.core.g.at = true;
                    e.b(fVar.c());
                    arrayList.add(Integer.valueOf(intValue));
                }
            }
            if (!arrayList.isEmpty()) {
                for (Integer intValue2 : arrayList) {
                    b2.remove(Integer.valueOf(intValue2.intValue()));
                }
                com.igexin.push.config.a.a().g();
            }
            boolean z2 = true;
            for (Map.Entry next2 : b.entrySet()) {
                int intValue3 = ((Integer) next2.getKey()).intValue();
                f fVar2 = (f) next2.getValue();
                if (b2.containsKey(Integer.valueOf(intValue3))) {
                    if (!b2.get(Integer.valueOf(intValue3)).b().equals(fVar2.b())) {
                        com.igexin.push.core.g.at = true;
                        com.igexin.push.core.g.ap++;
                        e.a(fVar2);
                        z2 = false;
                    }
                    z = z2;
                } else {
                    com.igexin.push.core.g.ap++;
                    e.a(fVar2);
                    z = false;
                }
                z2 = z;
            }
            if (z2) {
                m.s.a(gVar.a());
                com.igexin.push.config.a.a().g();
                Process.killProcess(Process.myPid());
                return;
            }
            return;
        }
        for (Map.Entry<Integer, f> value : b.entrySet()) {
            com.igexin.push.core.g.ap++;
            e.a((f) value.getValue());
        }
    }

    private static void a(Map<String, c> map, String str) {
        map.remove(str);
        for (String next : map.get(str).b()) {
            c cVar = map.get(next);
            if (cVar != null) {
                cVar.e();
                if (cVar.c() == 0) {
                    a(map, next);
                }
            }
        }
    }

    public static boolean a() {
        try {
            if ("none".equals(m.t)) {
                return false;
            }
            for (String b : m.t.split(MiPushClient.ACCEPT_TIME_SEPARATOR)) {
                if (b(b)) {
                    return false;
                }
            }
            if ("none".equals(m.u)) {
                return false;
            }
            String[] split = m.u.split(MiPushClient.ACCEPT_TIME_SEPARATOR);
            Class<?> cls = Class.forName("android.os.ServiceManager");
            Method method = cls.getMethod("getService", String.class);
            method.setAccessible(true);
            for (String a2 : split) {
                if (a(cls, method, a2)) {
                    return false;
                }
            }
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public static boolean a(long j) {
        Date date = new Date(j);
        Calendar instance = Calendar.getInstance();
        instance.setTime(date);
        int i = instance.get(11);
        int i2 = m.f885a + m.b;
        if (i2 >= 24) {
            i2 -= 24;
        }
        if (m.b == 0) {
            return false;
        }
        if (m.f885a < i2) {
            if (i >= m.f885a && i < i2) {
                return true;
            }
        } else if (m.f885a > i2) {
            if (i >= 0 && i < i2) {
                return true;
            }
            if (i >= m.f885a && i < 24) {
                return true;
            }
        }
        return false;
    }

    public static boolean a(Intent intent, Context context) {
        if (intent == null || context == null) {
            return false;
        }
        try {
            List<ResolveInfo> queryIntentServices = context.getPackageManager().queryIntentServices(intent, 0);
            return queryIntentServices != null && queryIntentServices.size() > 0;
        } catch (Throwable th) {
            return false;
        }
    }

    public static boolean a(ServiceInfo serviceInfo, PackageInfo packageInfo) {
        return com.igexin.push.core.a.o.equals(serviceInfo.name) || com.igexin.push.core.a.n.equals(serviceInfo.name) || com.igexin.push.core.a.p.equals(serviceInfo.name);
    }

    private static boolean a(Class<?> cls, Method method, String str) {
        try {
            return method.invoke(cls, new Object[]{str}) != null;
        } catch (Exception e) {
            return true;
        }
    }

    public static boolean a(String str) {
        try {
            return com.igexin.push.core.g.f.getPackageManager().getLaunchIntentForPackage(str) != null;
        } catch (Exception e) {
            return false;
        }
    }

    public static <T extends Service> boolean a(String str, Context context, Class cls) {
        if (cls == null) {
            try {
                if (!b(new Intent(context, Class.forName(com.igexin.push.core.a.n)), context)) {
                    Log.e(str, "call - > initialize, parameter [userServiceName] is null use default PushService, but didn't find class \"com.igexin.sdk.PushService\", please check your AndroidManifest");
                    return false;
                }
            } catch (Throwable th) {
                com.igexin.b.a.c.a.b(f1027a + "|" + th.toString());
                return false;
            }
        }
        if (cls == null || !com.igexin.push.core.a.n.equals(cls.getName()) || b(new Intent(context, cls), context)) {
            if (cls != null) {
                if (!b(new Intent(context, cls), context)) {
                    Log.e(str, "call - > initialize, parameter [userServiceName] is set, but didn't find class \"" + cls.getName() + "\", please check your AndroidManifest");
                    return false;
                }
            }
            if (cls != null) {
                Class.forName(cls.getName());
            }
            return true;
        }
        Log.e(str, "call - > initialize, parameter [userServiceName] is default PushService, but didn't find class \"com.igexin.sdk.PushService\", please check your AndroidManifest");
        return false;
    }

    public static boolean a(String str, String str2) {
        try {
            if (TextUtils.isEmpty(str) || TextUtils.isEmpty(str2)) {
                return false;
            }
            Intent intent = new Intent();
            intent.setClassName(str, str2);
            return com.igexin.push.core.g.f.getPackageManager().resolveActivity(intent, 0) != null;
        } catch (Exception e) {
            return false;
        }
    }

    public static boolean a(JSONObject jSONObject) {
        try {
            HashMap hashMap = new HashMap();
            JSONArray jSONArray = jSONObject.getJSONArray("action_chains");
            for (int i = 0; i < jSONArray.length(); i++) {
                JSONObject jSONObject2 = (JSONObject) jSONArray.get(i);
                c cVar = new c();
                if (jSONObject2.has("actionid")) {
                    cVar.a(jSONObject2.getString("actionid"));
                    ArrayList arrayList = new ArrayList();
                    if (jSONObject2.has("type")) {
                        String string = jSONObject2.getString("type");
                        if ("popup".equals(string)) {
                            if (jSONObject2.has("buttons")) {
                                JSONArray jSONArray2 = jSONObject2.getJSONArray("buttons");
                                for (int i2 = 0; i2 < jSONArray2.length(); i2++) {
                                    if (((JSONObject) jSONArray2.get(i2)).has("do")) {
                                        arrayList.add(((JSONObject) jSONArray2.get(i2)).getString("do"));
                                    }
                                }
                            }
                            if (jSONObject2.has("do")) {
                                arrayList.add(jSONObject2.getString("do"));
                            }
                        } else if ("startapp".equals(string)) {
                            if (jSONObject2.has("noinstall_action")) {
                                arrayList.add(jSONObject2.getString("noinstall_action"));
                            }
                            if (jSONObject2.has("do")) {
                                arrayList.add(jSONObject2.getString("do"));
                            }
                        } else if ("checkapp".equals(string)) {
                            if (jSONObject2.has("do_installed")) {
                                arrayList.add(jSONObject2.getString("do_installed"));
                            }
                            if (jSONObject2.has("do_uninstalled")) {
                                arrayList.add(jSONObject2.getString("do_uninstalled"));
                            }
                        } else if ("checkversions".equals(string)) {
                            if (jSONObject2.has("do_match")) {
                                arrayList.add(jSONObject2.getString("do_match"));
                            }
                            if (jSONObject2.has("do_dismatch")) {
                                arrayList.add(jSONObject2.getString("do_dismatch"));
                            }
                            if (jSONObject2.has("do")) {
                                arrayList.add(jSONObject2.getString("do"));
                            }
                        } else if ("startintent".equals(string)) {
                            if (jSONObject2.has("do_failed")) {
                                arrayList.add(jSONObject2.getString("do_failed"));
                            }
                            if (jSONObject2.has("do")) {
                                arrayList.add(jSONObject2.getString("do"));
                            }
                        } else if (!"null".equals(string) && jSONObject2.has("do")) {
                            arrayList.add(jSONObject2.getString("do"));
                        }
                        cVar.a(arrayList);
                        hashMap.put(cVar.a(), cVar);
                    }
                }
            }
            ArrayList<c> arrayList2 = new ArrayList<>(hashMap.values());
            for (Map.Entry value : hashMap.entrySet()) {
                List<String> b = ((c) value.getValue()).b();
                if (b != null) {
                    for (String str : b) {
                        c cVar2 = (c) hashMap.get(str);
                        if (cVar2 != null) {
                            cVar2.d();
                            if (arrayList2.contains(cVar2)) {
                                arrayList2.remove(cVar2);
                            }
                        }
                    }
                }
            }
            for (c a2 : arrayList2) {
                a(hashMap, a2.a());
            }
            if (hashMap.size() > 0) {
                com.igexin.b.a.c.a.b(f1027a + "|action_chains have loop nodeMap not empty");
                return true;
            }
        } catch (Throwable th) {
            com.igexin.b.a.c.a.b(f1027a + "|isHaveLoop exception :" + th.toString());
        }
        return false;
    }

    public static boolean b() {
        return System.currentTimeMillis() > m.c;
    }

    public static boolean b(Intent intent, Context context) {
        if (intent == null || context == null) {
            return false;
        }
        try {
            List<ResolveInfo> queryIntentServices = context.getPackageManager().queryIntentServices(intent, 0);
            return queryIntentServices != null && queryIntentServices.size() > 0;
        } catch (Throwable th) {
            com.igexin.b.a.c.a.b(f1027a + "|" + th.toString());
            return false;
        }
    }

    private static boolean b(String str) {
        try {
            com.igexin.push.core.g.f.getPackageManager().getPackageInfo(str, 0);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public static boolean c() {
        try {
            PackageManager packageManager = com.igexin.push.core.g.f.getPackageManager();
            String packageName = com.igexin.push.core.g.f.getPackageName();
            return packageManager.checkPermission("android.permission.ACCESS_WIFI_STATE", packageName) == 0 && packageManager.checkPermission("android.permission.CHANGE_WIFI_STATE", packageName) == 0;
        } catch (Throwable th) {
            return false;
        }
    }

    public static boolean d() {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) com.igexin.push.core.g.f.getSystemService("connectivity")).getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.getType() == 1;
    }

    public static boolean e() {
        try {
            Class.forName("android.support.v4.content.LocalBroadcastManager");
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public static boolean f() {
        try {
            for (String lowerCase : m.L.split(MiPushClient.ACCEPT_TIME_SEPARATOR)) {
                if (Build.MODEL.toLowerCase().contains(lowerCase.toLowerCase())) {
                    return false;
                }
            }
        } catch (Throwable th) {
        }
        return true;
    }
}
