package com.igexin.push.util;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import cn.banshenggua.aichang.room.message.SocketMessage;
import cn.banshenggua.aichang.utils.StringUtil;
import com.igexin.b.a.b.c;
import com.igexin.b.b.a;
import com.igexin.push.config.m;
import com.igexin.push.core.bean.f;
import com.igexin.push.core.c.i;
import com.igexin.push.core.g;
import com.igexin.sdk.a.b;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class e {

    /* renamed from: a  reason: collision with root package name */
    private static final Object f1029a = new Object();

    /* JADX WARNING: Removed duplicated region for block: B:22:0x00b7 A[SYNTHETIC, Splitter:B:22:0x00b7] */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x00c2 A[SYNTHETIC, Splitter:B:27:0x00c2] */
    /* JADX WARNING: Removed duplicated region for block: B:39:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void a() {
        /*
            r1 = 0
            java.io.File r0 = new java.io.File     // Catch:{ Exception -> 0x009a }
            java.lang.String r2 = com.igexin.push.core.g.X     // Catch:{ Exception -> 0x009a }
            r0.<init>(r2)     // Catch:{ Exception -> 0x009a }
            boolean r2 = r0.exists()     // Catch:{ Exception -> 0x009a }
            if (r2 != 0) goto L_0x003a
            boolean r2 = r0.createNewFile()     // Catch:{ Exception -> 0x009a }
            if (r2 != 0) goto L_0x003a
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x009a }
            r2.<init>()     // Catch:{ Exception -> 0x009a }
            java.lang.String r3 = "FileUtils | create file : "
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x009a }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x009a }
            java.lang.StringBuilder r0 = r2.append(r0)     // Catch:{ Exception -> 0x009a }
            java.lang.String r2 = " failed !!!"
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ Exception -> 0x009a }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x009a }
            com.igexin.b.a.c.a.b(r0)     // Catch:{ Exception -> 0x009a }
            if (r1 == 0) goto L_0x0039
            r1.close()     // Catch:{ IOException -> 0x00c6 }
        L_0x0039:
            return
        L_0x003a:
            java.io.FileOutputStream r2 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x009a }
            java.lang.String r0 = com.igexin.push.core.g.X     // Catch:{ Exception -> 0x009a }
            r2.<init>(r0)     // Catch:{ Exception -> 0x009a }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00ce, all -> 0x00cb }
            r0.<init>()     // Catch:{ Exception -> 0x00ce, all -> 0x00cb }
            java.lang.String r1 = "v01"
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Exception -> 0x00ce, all -> 0x00cb }
            java.lang.String r1 = com.igexin.push.core.g.w     // Catch:{ Exception -> 0x00ce, all -> 0x00cb }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Exception -> 0x00ce, all -> 0x00cb }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x00ce, all -> 0x00cb }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00ce, all -> 0x00cb }
            r1.<init>()     // Catch:{ Exception -> 0x00ce, all -> 0x00cb }
            java.lang.StringBuilder r0 = r1.append(r0)     // Catch:{ Exception -> 0x00ce, all -> 0x00cb }
            long r4 = com.igexin.push.core.g.q     // Catch:{ Exception -> 0x00ce, all -> 0x00cb }
            java.lang.String r1 = java.lang.String.valueOf(r4)     // Catch:{ Exception -> 0x00ce, all -> 0x00cb }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Exception -> 0x00ce, all -> 0x00cb }
            java.lang.String r1 = "|"
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Exception -> 0x00ce, all -> 0x00cb }
            java.lang.String r1 = com.igexin.push.core.g.f974a     // Catch:{ Exception -> 0x00ce, all -> 0x00cb }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Exception -> 0x00ce, all -> 0x00cb }
            java.lang.String r1 = "|"
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Exception -> 0x00ce, all -> 0x00cb }
            java.lang.String r1 = com.igexin.push.core.g.r     // Catch:{ Exception -> 0x00ce, all -> 0x00cb }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Exception -> 0x00ce, all -> 0x00cb }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x00ce, all -> 0x00cb }
            byte[] r0 = r0.getBytes()     // Catch:{ Exception -> 0x00ce, all -> 0x00cb }
            java.lang.String r1 = com.igexin.push.core.g.C     // Catch:{ Exception -> 0x00ce, all -> 0x00cb }
            byte[] r0 = com.igexin.b.a.a.a.d(r0, r1)     // Catch:{ Exception -> 0x00ce, all -> 0x00cb }
            r2.write(r0)     // Catch:{ Exception -> 0x00ce, all -> 0x00cb }
            if (r2 == 0) goto L_0x0039
            r2.close()     // Catch:{ IOException -> 0x0098 }
            goto L_0x0039
        L_0x0098:
            r0 = move-exception
            goto L_0x0039
        L_0x009a:
            r0 = move-exception
        L_0x009b:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x00bf }
            r2.<init>()     // Catch:{ all -> 0x00bf }
            java.lang.String r3 = "FileUtils | "
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x00bf }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x00bf }
            java.lang.StringBuilder r0 = r2.append(r0)     // Catch:{ all -> 0x00bf }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x00bf }
            com.igexin.b.a.c.a.b(r0)     // Catch:{ all -> 0x00bf }
            if (r1 == 0) goto L_0x0039
            r1.close()     // Catch:{ IOException -> 0x00bc }
            goto L_0x0039
        L_0x00bc:
            r0 = move-exception
            goto L_0x0039
        L_0x00bf:
            r0 = move-exception
        L_0x00c0:
            if (r1 == 0) goto L_0x00c5
            r1.close()     // Catch:{ IOException -> 0x00c9 }
        L_0x00c5:
            throw r0
        L_0x00c6:
            r0 = move-exception
            goto L_0x0039
        L_0x00c9:
            r1 = move-exception
            goto L_0x00c5
        L_0x00cb:
            r0 = move-exception
            r1 = r2
            goto L_0x00c0
        L_0x00ce:
            r0 = move-exception
            r1 = r2
            goto L_0x009b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.igexin.push.util.e.a():void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public static void a(f fVar) {
        File file = new File(g.ab + "/" + fVar.c());
        File file2 = new File(g.ac + "/" + fVar.c());
        if (file2.exists()) {
            if (!a.a(g.f, file2.getAbsolutePath()).equals(fVar.f())) {
                file2.delete();
            } else {
                com.igexin.b.a.c.a.b("FileUtils|downloadExt ext is exsit " + file2);
                Intent intent = new Intent(g.f, com.igexin.push.core.a.e.a().a(g.f));
                intent.putExtra("action", "com.igexin.sdk.action.extdownloadsuccess");
                intent.putExtra("id", fVar.a());
                intent.putExtra(SocketMessage.MSG_RESULE_KEY, true);
                g.f.startService(intent);
                return;
            }
        }
        if (file.exists() && a.a(g.f, file.getAbsolutePath()).equals(fVar.f())) {
            com.igexin.b.a.c.a.b("FileUtils|downloadExt ext is exsit do copy " + file);
            if (a(file, file2, fVar.f())) {
                Intent intent2 = new Intent(g.f, com.igexin.push.core.a.e.a().a(g.f));
                intent2.putExtra("action", "com.igexin.sdk.action.extdownloadsuccess");
                intent2.putExtra("id", fVar.a());
                intent2.putExtra(SocketMessage.MSG_RESULE_KEY, true);
                g.f.startService(intent2);
                return;
            }
        }
        com.igexin.b.a.c.a.b("FileUtils|ext is not exsit start download name = " + fVar.c());
        new Thread(new i(g.f, fVar, false)).start();
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:0x0018 A[SYNTHETIC, Splitter:B:11:0x0018] */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0021 A[SYNTHETIC, Splitter:B:16:0x0021] */
    /* JADX WARNING: Removed duplicated region for block: B:26:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void a(byte[] r4, java.lang.String r5, boolean r6) {
        /*
            r1 = 0
            java.io.FileOutputStream r0 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x0014, all -> 0x001e }
            java.io.File r2 = new java.io.File     // Catch:{ Exception -> 0x0014, all -> 0x001e }
            r2.<init>(r5)     // Catch:{ Exception -> 0x0014, all -> 0x001e }
            r0.<init>(r2, r6)     // Catch:{ Exception -> 0x0014, all -> 0x001e }
            r0.write(r4)     // Catch:{ Exception -> 0x002e, all -> 0x0029 }
            if (r0 == 0) goto L_0x0013
            r0.close()     // Catch:{ Exception -> 0x0025 }
        L_0x0013:
            return
        L_0x0014:
            r0 = move-exception
            r0 = r1
        L_0x0016:
            if (r0 == 0) goto L_0x0013
            r0.close()     // Catch:{ Exception -> 0x001c }
            goto L_0x0013
        L_0x001c:
            r0 = move-exception
            goto L_0x0013
        L_0x001e:
            r0 = move-exception
        L_0x001f:
            if (r1 == 0) goto L_0x0024
            r1.close()     // Catch:{ Exception -> 0x0027 }
        L_0x0024:
            throw r0
        L_0x0025:
            r0 = move-exception
            goto L_0x0013
        L_0x0027:
            r1 = move-exception
            goto L_0x0024
        L_0x0029:
            r1 = move-exception
            r3 = r1
            r1 = r0
            r0 = r3
            goto L_0x001f
        L_0x002e:
            r1 = move-exception
            goto L_0x0016
        */
        throw new UnsupportedOperationException("Method not decompiled: com.igexin.push.util.e.a(byte[], java.lang.String, boolean):void");
    }

    public static boolean a(Context context) {
        return !new b(context).b();
    }

    /* JADX WARNING: Removed duplicated region for block: B:18:0x004a A[Catch:{ all -> 0x00eb }] */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x004f A[SYNTHETIC, Splitter:B:20:0x004f] */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0054 A[SYNTHETIC, Splitter:B:23:0x0054] */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0059 A[SYNTHETIC, Splitter:B:26:0x0059] */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x00be A[SYNTHETIC, Splitter:B:59:0x00be] */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x00c3 A[SYNTHETIC, Splitter:B:62:0x00c3] */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x00c8 A[SYNTHETIC, Splitter:B:65:0x00c8] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean a(java.io.File r9, java.io.File r10, java.lang.String r11) {
        /*
            r3 = 0
            r0 = 0
            java.io.FileInputStream r5 = new java.io.FileInputStream     // Catch:{ Throwable -> 0x00ef, all -> 0x00b8 }
            r5.<init>(r9)     // Catch:{ Throwable -> 0x00ef, all -> 0x00b8 }
            java.io.FileOutputStream r4 = new java.io.FileOutputStream     // Catch:{ Throwable -> 0x00f4, all -> 0x00e2 }
            r4.<init>(r10)     // Catch:{ Throwable -> 0x00f4, all -> 0x00e2 }
            java.io.BufferedOutputStream r2 = new java.io.BufferedOutputStream     // Catch:{ Throwable -> 0x00f9, all -> 0x00e6 }
            r2.<init>(r4)     // Catch:{ Throwable -> 0x00f9, all -> 0x00e6 }
            r1 = 1024(0x400, float:1.435E-42)
            byte[] r1 = new byte[r1]     // Catch:{ Throwable -> 0x0027, all -> 0x00e9 }
        L_0x0015:
            int r3 = r5.read(r1)     // Catch:{ Throwable -> 0x0027, all -> 0x00e9 }
            r6 = -1
            if (r3 == r6) goto L_0x005d
            byte[] r6 = new byte[r3]     // Catch:{ Throwable -> 0x0027, all -> 0x00e9 }
            r7 = 0
            r8 = 0
            java.lang.System.arraycopy(r1, r7, r6, r8, r3)     // Catch:{ Throwable -> 0x0027, all -> 0x00e9 }
            r2.write(r6)     // Catch:{ Throwable -> 0x0027, all -> 0x00e9 }
            goto L_0x0015
        L_0x0027:
            r1 = move-exception
            r3 = r4
            r4 = r5
        L_0x002a:
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x00eb }
            r5.<init>()     // Catch:{ all -> 0x00eb }
            java.lang.String r6 = "FileUtils"
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ all -> 0x00eb }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x00eb }
            java.lang.StringBuilder r1 = r5.append(r1)     // Catch:{ all -> 0x00eb }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x00eb }
            com.igexin.b.a.c.a.b(r1)     // Catch:{ all -> 0x00eb }
            boolean r1 = r10.exists()     // Catch:{ all -> 0x00eb }
            if (r1 == 0) goto L_0x004d
            r10.delete()     // Catch:{ all -> 0x00eb }
        L_0x004d:
            if (r4 == 0) goto L_0x0052
            r4.close()     // Catch:{ IOException -> 0x00d4 }
        L_0x0052:
            if (r2 == 0) goto L_0x0057
            r2.close()     // Catch:{ IOException -> 0x00d7 }
        L_0x0057:
            if (r3 == 0) goto L_0x005c
            r3.close()     // Catch:{ IOException -> 0x00da }
        L_0x005c:
            return r0
        L_0x005d:
            r2.flush()     // Catch:{ Throwable -> 0x0027, all -> 0x00e9 }
            android.content.Context r1 = com.igexin.push.core.g.f     // Catch:{ Throwable -> 0x0027, all -> 0x00e9 }
            java.lang.String r3 = r10.getAbsolutePath()     // Catch:{ Throwable -> 0x0027, all -> 0x00e9 }
            java.lang.String r1 = com.igexin.b.b.a.a(r1, r3)     // Catch:{ Throwable -> 0x0027, all -> 0x00e9 }
            boolean r1 = r1.equals(r11)     // Catch:{ Throwable -> 0x0027, all -> 0x00e9 }
            if (r1 != 0) goto L_0x0085
            r10.delete()     // Catch:{ Throwable -> 0x0027, all -> 0x00e9 }
            if (r5 == 0) goto L_0x0078
            r5.close()     // Catch:{ IOException -> 0x00cc }
        L_0x0078:
            if (r2 == 0) goto L_0x007d
            r2.close()     // Catch:{ IOException -> 0x00ce }
        L_0x007d:
            if (r4 == 0) goto L_0x005c
            r4.close()     // Catch:{ IOException -> 0x0083 }
            goto L_0x005c
        L_0x0083:
            r1 = move-exception
            goto L_0x005c
        L_0x0085:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0027, all -> 0x00e9 }
            r1.<init>()     // Catch:{ Throwable -> 0x0027, all -> 0x00e9 }
            java.lang.String r3 = "FileUtils|copyFile success from : "
            java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ Throwable -> 0x0027, all -> 0x00e9 }
            java.lang.StringBuilder r1 = r1.append(r9)     // Catch:{ Throwable -> 0x0027, all -> 0x00e9 }
            java.lang.String r3 = " to : "
            java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ Throwable -> 0x0027, all -> 0x00e9 }
            java.lang.StringBuilder r1 = r1.append(r10)     // Catch:{ Throwable -> 0x0027, all -> 0x00e9 }
            java.lang.String r1 = r1.toString()     // Catch:{ Throwable -> 0x0027, all -> 0x00e9 }
            com.igexin.b.a.c.a.b(r1)     // Catch:{ Throwable -> 0x0027, all -> 0x00e9 }
            r0 = 1
            if (r5 == 0) goto L_0x00ab
            r5.close()     // Catch:{ IOException -> 0x00d0 }
        L_0x00ab:
            if (r2 == 0) goto L_0x00b0
            r2.close()     // Catch:{ IOException -> 0x00d2 }
        L_0x00b0:
            if (r4 == 0) goto L_0x005c
            r4.close()     // Catch:{ IOException -> 0x00b6 }
            goto L_0x005c
        L_0x00b6:
            r1 = move-exception
            goto L_0x005c
        L_0x00b8:
            r0 = move-exception
            r2 = r3
            r4 = r3
            r5 = r3
        L_0x00bc:
            if (r5 == 0) goto L_0x00c1
            r5.close()     // Catch:{ IOException -> 0x00dc }
        L_0x00c1:
            if (r2 == 0) goto L_0x00c6
            r2.close()     // Catch:{ IOException -> 0x00de }
        L_0x00c6:
            if (r4 == 0) goto L_0x00cb
            r4.close()     // Catch:{ IOException -> 0x00e0 }
        L_0x00cb:
            throw r0
        L_0x00cc:
            r1 = move-exception
            goto L_0x0078
        L_0x00ce:
            r1 = move-exception
            goto L_0x007d
        L_0x00d0:
            r1 = move-exception
            goto L_0x00ab
        L_0x00d2:
            r1 = move-exception
            goto L_0x00b0
        L_0x00d4:
            r1 = move-exception
            goto L_0x0052
        L_0x00d7:
            r1 = move-exception
            goto L_0x0057
        L_0x00da:
            r1 = move-exception
            goto L_0x005c
        L_0x00dc:
            r1 = move-exception
            goto L_0x00c1
        L_0x00de:
            r1 = move-exception
            goto L_0x00c6
        L_0x00e0:
            r1 = move-exception
            goto L_0x00cb
        L_0x00e2:
            r0 = move-exception
            r2 = r3
            r4 = r3
            goto L_0x00bc
        L_0x00e6:
            r0 = move-exception
            r2 = r3
            goto L_0x00bc
        L_0x00e9:
            r0 = move-exception
            goto L_0x00bc
        L_0x00eb:
            r0 = move-exception
            r5 = r4
            r4 = r3
            goto L_0x00bc
        L_0x00ef:
            r1 = move-exception
            r2 = r3
            r4 = r3
            goto L_0x002a
        L_0x00f4:
            r1 = move-exception
            r2 = r3
            r4 = r5
            goto L_0x002a
        L_0x00f9:
            r1 = move-exception
            r2 = r3
            r3 = r4
            r4 = r5
            goto L_0x002a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.igexin.push.util.e.a(java.io.File, java.io.File, java.lang.String):boolean");
    }

    public static boolean a(String str, String str2, boolean z) {
        com.igexin.b.a.c.a.b("FileUtils|rename file...");
        if (TextUtils.isEmpty(str) || TextUtils.isEmpty(str2)) {
            return false;
        }
        String substring = a.a(str2).substring(12, 20);
        try {
            com.igexin.b.a.c.a.b("FileUtils|rename file reverseName:" + substring + " isForward:" + z);
            if (z) {
                File file = new File(str + "/" + str2 + ".dex");
                if (file.exists()) {
                    File file2 = new File(str + "/" + substring);
                    if (file2.exists()) {
                        file2.delete();
                    }
                    com.igexin.b.a.c.a.b("FileUtils|rename file from " + file.getAbsolutePath() + " --> " + file2.getAbsolutePath());
                    file.renameTo(file2);
                    return true;
                }
            } else {
                File file3 = new File(str + "/" + substring);
                if (file3.exists()) {
                    File file4 = new File(str + "/" + str2 + ".dex");
                    com.igexin.b.a.c.a.b("FileUtils|rename file from " + file3.getAbsolutePath() + " --> " + file4.getAbsolutePath());
                    if (file4.exists()) {
                        file4.delete();
                    }
                    file3.renameTo(file4);
                    return true;
                }
            }
        } catch (Throwable th) {
            com.igexin.b.a.c.a.b("FileUtils|renameGenerateFile error:" + th.toString());
            File file5 = new File(str + "/" + str2 + ".dex");
            File file6 = new File(str + "/" + substring);
            if (file5.exists()) {
                file5.delete();
            }
            if (file6.exists()) {
                file6.delete();
            }
        }
        return false;
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x0060 A[SYNTHETIC, Splitter:B:17:0x0060] */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0065 A[SYNTHETIC, Splitter:B:20:0x0065] */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x0082 A[SYNTHETIC, Splitter:B:35:0x0082] */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x0087 A[SYNTHETIC, Splitter:B:38:0x0087] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static byte[] a(java.lang.String r6) {
        /*
            r0 = 0
            java.io.File r1 = new java.io.File
            r1.<init>(r6)
            boolean r1 = r1.exists()
            if (r1 != 0) goto L_0x0029
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "FileUtils|get data from file = "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r6)
            java.lang.String r2 = " file not exist ######"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            com.igexin.b.a.c.a.b(r1)
        L_0x0028:
            return r0
        L_0x0029:
            r1 = 1024(0x400, float:1.435E-42)
            byte[] r1 = new byte[r1]
            java.io.FileInputStream r3 = new java.io.FileInputStream     // Catch:{ Exception -> 0x0099, all -> 0x007c }
            r3.<init>(r6)     // Catch:{ Exception -> 0x0099, all -> 0x007c }
            java.io.ByteArrayOutputStream r2 = new java.io.ByteArrayOutputStream     // Catch:{ Exception -> 0x009d, all -> 0x0093 }
            r2.<init>()     // Catch:{ Exception -> 0x009d, all -> 0x0093 }
        L_0x0037:
            int r4 = r3.read(r1)     // Catch:{ Exception -> 0x0043 }
            r5 = -1
            if (r4 == r5) goto L_0x006b
            r5 = 0
            r2.write(r1, r5, r4)     // Catch:{ Exception -> 0x0043 }
            goto L_0x0037
        L_0x0043:
            r1 = move-exception
        L_0x0044:
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x0097 }
            r4.<init>()     // Catch:{ all -> 0x0097 }
            java.lang.String r5 = "FileUtils|"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x0097 }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x0097 }
            java.lang.StringBuilder r1 = r4.append(r1)     // Catch:{ all -> 0x0097 }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x0097 }
            com.igexin.b.a.c.a.b(r1)     // Catch:{ all -> 0x0097 }
            if (r3 == 0) goto L_0x0063
            r3.close()     // Catch:{ Exception -> 0x008d }
        L_0x0063:
            if (r2 == 0) goto L_0x0028
            r2.close()     // Catch:{ Exception -> 0x0069 }
            goto L_0x0028
        L_0x0069:
            r1 = move-exception
            goto L_0x0028
        L_0x006b:
            byte[] r0 = r2.toByteArray()     // Catch:{ Exception -> 0x0043 }
            if (r3 == 0) goto L_0x0074
            r3.close()     // Catch:{ Exception -> 0x008b }
        L_0x0074:
            if (r2 == 0) goto L_0x0028
            r2.close()     // Catch:{ Exception -> 0x007a }
            goto L_0x0028
        L_0x007a:
            r1 = move-exception
            goto L_0x0028
        L_0x007c:
            r1 = move-exception
            r2 = r0
            r3 = r0
            r0 = r1
        L_0x0080:
            if (r3 == 0) goto L_0x0085
            r3.close()     // Catch:{ Exception -> 0x008f }
        L_0x0085:
            if (r2 == 0) goto L_0x008a
            r2.close()     // Catch:{ Exception -> 0x0091 }
        L_0x008a:
            throw r0
        L_0x008b:
            r1 = move-exception
            goto L_0x0074
        L_0x008d:
            r1 = move-exception
            goto L_0x0063
        L_0x008f:
            r1 = move-exception
            goto L_0x0085
        L_0x0091:
            r1 = move-exception
            goto L_0x008a
        L_0x0093:
            r1 = move-exception
            r2 = r0
            r0 = r1
            goto L_0x0080
        L_0x0097:
            r0 = move-exception
            goto L_0x0080
        L_0x0099:
            r1 = move-exception
            r2 = r0
            r3 = r0
            goto L_0x0044
        L_0x009d:
            r1 = move-exception
            r2 = r0
            goto L_0x0044
        */
        throw new UnsupportedOperationException("Method not decompiled: com.igexin.push.util.e.a(java.lang.String):byte[]");
    }

    public static String b() {
        String str = null;
        try {
            byte[] a2 = a(g.X);
            if (a2 == null) {
                com.igexin.b.a.c.a.b("FileUtils | read file cid id = null");
                return str;
            }
            String[] split = new String(com.igexin.b.a.a.a.c(a2, g.C)).split("\\|");
            if (split.length > 2) {
                String str2 = split[2];
                if (str2 != null) {
                    try {
                        if (!str2.equals("null")) {
                            str = str2;
                        }
                    } catch (Exception e) {
                        str = str2;
                    }
                } else {
                    str = str2;
                }
            }
            com.igexin.b.a.c.a.b("FileUtils|get cid from file cid = " + str);
            return str;
        } catch (Exception e2) {
        }
    }

    public static void b(Context context) {
        File file = new File(context.getFilesDir().getPath() + "/" + "init_c.pid");
        if (file.exists()) {
            file.delete();
        }
        if (!m.x) {
            com.igexin.b.a.c.a.b("FileUtils|isReportInitialize = false");
        } else if (g.g.get()) {
            c.b().a(new g(context), false, true);
        } else {
            new Thread(new h(context)).start();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.igexin.push.util.e.a(byte[], java.lang.String, boolean):void
     arg types: [byte[], java.lang.String, int]
     candidates:
      com.igexin.push.util.e.a(java.io.File, java.io.File, java.lang.String):boolean
      com.igexin.push.util.e.a(java.lang.String, java.lang.String, boolean):boolean
      com.igexin.push.util.e.a(byte[], java.lang.String, boolean):void */
    /* access modifiers changed from: private */
    public static void b(Context context, String str) {
        if (context != null && str != null) {
            String str2 = context.getFilesDir().getPath() + "/" + "init_c1.pid";
            synchronized (f1029a) {
                if (str.length() == 0) {
                    a(str.getBytes(), str2, false);
                } else {
                    a((str + "|").getBytes(), str2, true);
                }
            }
        }
    }

    /* JADX WARN: Type inference failed for: r1v0 */
    /* JADX WARN: Type inference failed for: r1v1, types: [java.io.BufferedOutputStream] */
    /* JADX WARN: Type inference failed for: r1v4, types: [java.io.FileOutputStream] */
    /* JADX WARN: Type inference failed for: r1v5 */
    /* JADX WARN: Type inference failed for: r1v6 */
    /* JADX WARN: Type inference failed for: r1v9 */
    /* JADX WARN: Type inference failed for: r1v11 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0033 A[Catch:{ all -> 0x00a3 }] */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0038 A[SYNTHETIC, Splitter:B:20:0x0038] */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x003d A[SYNTHETIC, Splitter:B:23:0x003d] */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0042 A[SYNTHETIC, Splitter:B:26:0x0042] */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x007c A[SYNTHETIC, Splitter:B:54:0x007c] */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x0081 A[SYNTHETIC, Splitter:B:57:0x0081] */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x0086 A[SYNTHETIC, Splitter:B:60:0x0086] */
    /* JADX WARNING: Removed duplicated region for block: B:83:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void b(java.io.File r9, java.io.File r10, java.lang.String r11) {
        /*
            r1 = 0
            java.io.FileInputStream r3 = new java.io.FileInputStream     // Catch:{ Exception -> 0x00aa, all -> 0x0077 }
            r3.<init>(r9)     // Catch:{ Exception -> 0x00aa, all -> 0x0077 }
            java.io.FileOutputStream r2 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x00ae, all -> 0x0099 }
            r2.<init>(r10)     // Catch:{ Exception -> 0x00ae, all -> 0x0099 }
            java.io.BufferedOutputStream r0 = new java.io.BufferedOutputStream     // Catch:{ Exception -> 0x00b3, all -> 0x009c }
            r0.<init>(r2)     // Catch:{ Exception -> 0x00b3, all -> 0x009c }
            r1 = 1024(0x400, float:1.435E-42)
            byte[] r1 = new byte[r1]     // Catch:{ Exception -> 0x002a, all -> 0x009e }
        L_0x0014:
            int r4 = r3.read(r1)     // Catch:{ Exception -> 0x002a, all -> 0x009e }
            r5 = -1
            if (r4 == r5) goto L_0x0046
            byte[] r5 = new byte[r4]     // Catch:{ Exception -> 0x002a, all -> 0x009e }
            r6 = 0
            r7 = 0
            java.lang.System.arraycopy(r1, r6, r5, r7, r4)     // Catch:{ Exception -> 0x002a, all -> 0x009e }
            byte[] r4 = com.igexin.b.a.a.a.c(r5, r11)     // Catch:{ Exception -> 0x002a, all -> 0x009e }
            r0.write(r4)     // Catch:{ Exception -> 0x002a, all -> 0x009e }
            goto L_0x0014
        L_0x002a:
            r1 = move-exception
            r1 = r2
            r2 = r3
        L_0x002d:
            boolean r3 = r10.exists()     // Catch:{ all -> 0x00a3 }
            if (r3 == 0) goto L_0x0036
            r10.delete()     // Catch:{ all -> 0x00a3 }
        L_0x0036:
            if (r2 == 0) goto L_0x003b
            r2.close()     // Catch:{ IOException -> 0x0068 }
        L_0x003b:
            if (r0 == 0) goto L_0x0040
            r0.close()     // Catch:{ IOException -> 0x006d }
        L_0x0040:
            if (r1 == 0) goto L_0x0045
            r1.close()     // Catch:{ IOException -> 0x0072 }
        L_0x0045:
            return
        L_0x0046:
            r0.flush()     // Catch:{ Exception -> 0x002a, all -> 0x009e }
            if (r3 == 0) goto L_0x004e
            r3.close()     // Catch:{ IOException -> 0x005e }
        L_0x004e:
            if (r0 == 0) goto L_0x0053
            r0.close()     // Catch:{ IOException -> 0x0063 }
        L_0x0053:
            if (r2 == 0) goto L_0x0045
            r2.close()     // Catch:{ IOException -> 0x0059 }
            goto L_0x0045
        L_0x0059:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0045
        L_0x005e:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x004e
        L_0x0063:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0053
        L_0x0068:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x003b
        L_0x006d:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0040
        L_0x0072:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0045
        L_0x0077:
            r0 = move-exception
            r2 = r1
            r3 = r1
        L_0x007a:
            if (r3 == 0) goto L_0x007f
            r3.close()     // Catch:{ IOException -> 0x008a }
        L_0x007f:
            if (r1 == 0) goto L_0x0084
            r1.close()     // Catch:{ IOException -> 0x008f }
        L_0x0084:
            if (r2 == 0) goto L_0x0089
            r2.close()     // Catch:{ IOException -> 0x0094 }
        L_0x0089:
            throw r0
        L_0x008a:
            r3 = move-exception
            r3.printStackTrace()
            goto L_0x007f
        L_0x008f:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0084
        L_0x0094:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0089
        L_0x0099:
            r0 = move-exception
            r2 = r1
            goto L_0x007a
        L_0x009c:
            r0 = move-exception
            goto L_0x007a
        L_0x009e:
            r1 = move-exception
            r8 = r1
            r1 = r0
            r0 = r8
            goto L_0x007a
        L_0x00a3:
            r3 = move-exception
            r8 = r3
            r3 = r2
            r2 = r1
            r1 = r0
            r0 = r8
            goto L_0x007a
        L_0x00aa:
            r0 = move-exception
            r0 = r1
            r2 = r1
            goto L_0x002d
        L_0x00ae:
            r0 = move-exception
            r0 = r1
            r2 = r3
            goto L_0x002d
        L_0x00b3:
            r0 = move-exception
            r0 = r1
            r1 = r2
            r2 = r3
            goto L_0x002d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.igexin.push.util.e.b(java.io.File, java.io.File, java.lang.String):void");
    }

    public static boolean b(String str) {
        boolean z = true;
        com.igexin.b.a.c.a.b("FileUtils|removeExt " + str);
        try {
            String str2 = g.ac + "/" + str;
            com.igexin.b.a.c.a.b("FileUtils|removeExt delete ext path = " + str2);
            File file = new File(str2);
            boolean delete = file.exists() ? file.delete() : true;
            String str3 = g.ac + "/" + str + ".dex";
            com.igexin.b.a.c.a.b("FileUtils|removeExt delete ext dex path = " + str3);
            File file2 = new File(str3);
            if (!file2.exists()) {
                z = delete;
            } else if (!delete || !file2.delete()) {
                z = false;
            }
            String substring = a.a(str).substring(12, 20);
            com.igexin.b.a.c.a.b("FileUtils|removeExt renamedExtName = " + substring);
            File file3 = new File(g.ac + "/" + substring);
            if (!file3.exists()) {
                return z;
            }
            com.igexin.b.a.c.a.b("FileUtils|removeExt, delete ext rename Ext path = " + file3.getAbsolutePath());
            boolean delete2 = file3.delete();
            boolean z2 = z & delete2;
            com.igexin.b.a.c.a.b("FileUtils|removeExt, delete ext renamedExt succeed = " + delete2);
            return z2;
        } catch (Throwable th) {
            com.igexin.b.a.c.a.b("FileUtils|" + th.toString());
            return false;
        }
    }

    public static String c() {
        String str;
        Exception e;
        try {
            com.igexin.b.a.c.a.b("FileUtils|get device id from file : " + g.Y);
            byte[] a2 = a(g.Y);
            if (a2 == null) {
                com.igexin.b.a.c.a.b("FileUtils|read file device id = null");
                return null;
            }
            str = new String(a2, StringUtil.Encoding);
            try {
                com.igexin.b.a.c.a.b("FileUtils|read file device id = " + str);
                return str;
            } catch (Exception e2) {
                e = e2;
            }
        } catch (Exception e3) {
            Exception exc = e3;
            str = null;
            e = exc;
            com.igexin.b.a.c.a.b("FileUtils|get device id from file : " + e.toString());
            return str;
        }
    }

    private static String c(String str) {
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).format(new Date()) + "|" + g.r + "|" + g.f974a + "|" + "1" + "|" + str;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x003b, code lost:
        if (r0 != 0) goto L_0x003d;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static long d() {
        /*
            r2 = 0
            java.lang.String r0 = com.igexin.push.core.g.X     // Catch:{ Exception -> 0x005c }
            byte[] r0 = a(r0)     // Catch:{ Exception -> 0x005c }
            if (r0 != 0) goto L_0x0010
            java.lang.String r0 = "FileUtils|read session from file, not exist"
            com.igexin.b.a.c.a.b(r0)     // Catch:{ Exception -> 0x005c }
        L_0x000f:
            return r2
        L_0x0010:
            java.lang.String r1 = new java.lang.String     // Catch:{ Exception -> 0x005c }
            java.lang.String r4 = com.igexin.push.core.g.C     // Catch:{ Exception -> 0x005c }
            byte[] r0 = com.igexin.b.a.a.a.c(r0, r4)     // Catch:{ Exception -> 0x005c }
            r1.<init>(r0)     // Catch:{ Exception -> 0x005c }
            java.lang.String r0 = "null"
            boolean r0 = r1.contains(r0)     // Catch:{ Exception -> 0x005c }
            if (r0 == 0) goto L_0x0055
            r0 = 7
            java.lang.String r0 = r1.substring(r0)     // Catch:{ Exception -> 0x005c }
        L_0x0028:
            java.lang.String r1 = "|"
            int r1 = r0.indexOf(r1)     // Catch:{ Exception -> 0x005c }
            if (r1 < 0) goto L_0x0035
            r4 = 0
            java.lang.String r0 = r0.substring(r4, r1)     // Catch:{ Exception -> 0x005c }
        L_0x0035:
            long r0 = java.lang.Long.parseLong(r0)     // Catch:{ Exception -> 0x005c }
            int r4 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r4 == 0) goto L_0x0077
        L_0x003d:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "FileUtils|session : "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r2 = r2.append(r0)
            java.lang.String r2 = r2.toString()
            com.igexin.b.a.c.a.b(r2)
            r2 = r0
            goto L_0x000f
        L_0x0055:
            r0 = 20
            java.lang.String r0 = r1.substring(r0)     // Catch:{ Exception -> 0x005c }
            goto L_0x0028
        L_0x005c:
            r0 = move-exception
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r4 = "FileUtils|"
            java.lang.StringBuilder r1 = r1.append(r4)
            java.lang.String r0 = r0.toString()
            java.lang.StringBuilder r0 = r1.append(r0)
            java.lang.String r0 = r0.toString()
            com.igexin.b.a.c.a.b(r0)
        L_0x0077:
            r0 = r2
            goto L_0x003d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.igexin.push.util.e.d():long");
    }

    /* JADX WARNING: Removed duplicated region for block: B:27:0x00a1 A[SYNTHETIC, Splitter:B:27:0x00a1] */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x00ac A[SYNTHETIC, Splitter:B:32:0x00ac] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void e() {
        /*
            java.lang.String r0 = com.igexin.push.core.g.y
            if (r0 != 0) goto L_0x0005
        L_0x0004:
            return
        L_0x0005:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "FileUtils|save device id to file : "
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = com.igexin.push.core.g.Y
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            com.igexin.b.a.c.a.b(r0)
            r1 = 0
            java.util.concurrent.locks.ReentrantReadWriteLock r0 = new java.util.concurrent.locks.ReentrantReadWriteLock
            r0.<init>()
            java.util.concurrent.locks.ReentrantReadWriteLock$WriteLock r3 = r0.writeLock()
            boolean r0 = r3.tryLock()     // Catch:{ Exception -> 0x0084 }
            if (r0 == 0) goto L_0x00c1
            java.io.File r0 = new java.io.File     // Catch:{ Exception -> 0x0084 }
            java.lang.String r2 = com.igexin.push.core.g.Y     // Catch:{ Exception -> 0x0084 }
            r0.<init>(r2)     // Catch:{ Exception -> 0x0084 }
            boolean r2 = r0.exists()     // Catch:{ Exception -> 0x0084 }
            if (r2 != 0) goto L_0x0069
            boolean r2 = r0.createNewFile()     // Catch:{ Exception -> 0x0084 }
            if (r2 != 0) goto L_0x0069
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0084 }
            r2.<init>()     // Catch:{ Exception -> 0x0084 }
            java.lang.String r4 = "FileUtils|create file : "
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ Exception -> 0x0084 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x0084 }
            java.lang.StringBuilder r0 = r2.append(r0)     // Catch:{ Exception -> 0x0084 }
            java.lang.String r2 = " failed !!!"
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ Exception -> 0x0084 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x0084 }
            com.igexin.b.a.c.a.b(r0)     // Catch:{ Exception -> 0x0084 }
            if (r1 == 0) goto L_0x0065
            r1.close()     // Catch:{ IOException -> 0x00b3 }
        L_0x0065:
            r3.unlock()
            goto L_0x0004
        L_0x0069:
            java.io.FileOutputStream r2 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x0084 }
            java.lang.String r0 = com.igexin.push.core.g.Y     // Catch:{ Exception -> 0x0084 }
            r2.<init>(r0)     // Catch:{ Exception -> 0x0084 }
            java.lang.String r0 = com.igexin.push.core.g.y     // Catch:{ Exception -> 0x00be, all -> 0x00bb }
            java.lang.String r1 = "utf-8"
            byte[] r0 = r0.getBytes(r1)     // Catch:{ Exception -> 0x00be, all -> 0x00bb }
            r2.write(r0)     // Catch:{ Exception -> 0x00be, all -> 0x00bb }
        L_0x007b:
            if (r2 == 0) goto L_0x0080
            r2.close()     // Catch:{ IOException -> 0x00b5 }
        L_0x0080:
            r3.unlock()
            goto L_0x0004
        L_0x0084:
            r0 = move-exception
        L_0x0085:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x00a9 }
            r2.<init>()     // Catch:{ all -> 0x00a9 }
            java.lang.String r4 = "FileUtils|"
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ all -> 0x00a9 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x00a9 }
            java.lang.StringBuilder r0 = r2.append(r0)     // Catch:{ all -> 0x00a9 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x00a9 }
            com.igexin.b.a.c.a.b(r0)     // Catch:{ all -> 0x00a9 }
            if (r1 == 0) goto L_0x00a4
            r1.close()     // Catch:{ IOException -> 0x00b7 }
        L_0x00a4:
            r3.unlock()
            goto L_0x0004
        L_0x00a9:
            r0 = move-exception
        L_0x00aa:
            if (r1 == 0) goto L_0x00af
            r1.close()     // Catch:{ IOException -> 0x00b9 }
        L_0x00af:
            r3.unlock()
            throw r0
        L_0x00b3:
            r0 = move-exception
            goto L_0x0065
        L_0x00b5:
            r0 = move-exception
            goto L_0x0080
        L_0x00b7:
            r0 = move-exception
            goto L_0x00a4
        L_0x00b9:
            r1 = move-exception
            goto L_0x00af
        L_0x00bb:
            r0 = move-exception
            r1 = r2
            goto L_0x00aa
        L_0x00be:
            r0 = move-exception
            r1 = r2
            goto L_0x0085
        L_0x00c1:
            r2 = r1
            goto L_0x007b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.igexin.push.util.e.e():void");
    }

    public static void f() {
        c.b().a(new f(), false, true);
    }

    public static void g() {
        b(g.f, "");
    }

    public static String h() {
        byte[] a2;
        String str = g.f.getFilesDir().getPath() + "/" + "init_c1.pid";
        try {
            synchronized (f1029a) {
                a2 = a(str);
            }
            if (a2 == null) {
                return null;
            }
            String str2 = new String(a2);
            if (TextUtils.isEmpty(str2)) {
                return null;
            }
            if (str2.endsWith("|")) {
                str2 = str2.substring(0, str2.length() - 1);
            }
            String[] split = str2.split("\\|");
            if (split.length <= 300 || System.currentTimeMillis() - Long.parseLong(split[0]) <= 604800000) {
                StringBuilder sb = new StringBuilder();
                for (String parseLong : split) {
                    sb.append(c(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).format(new Date(Long.parseLong(parseLong))))).append("\n");
                }
                if (sb.length() > 0) {
                    sb.deleteCharAt(sb.length() - 1);
                }
                return sb.toString();
            }
            g();
            return null;
        } catch (Throwable th) {
            com.igexin.b.a.c.a.b("FileUtils|upload init data error = " + th.toString());
            g();
            return null;
        }
    }
}
