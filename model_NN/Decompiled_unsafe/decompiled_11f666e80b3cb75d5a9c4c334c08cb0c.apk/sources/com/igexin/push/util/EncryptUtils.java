package com.igexin.push.util;

import android.text.TextUtils;
import com.igexin.b.a.b.f;
import com.igexin.b.a.c.a;
import com.igexin.download.Downloads;
import com.igexin.push.core.g;
import com.igexin.push.d.c.b;
import com.igexin.push.extension.mod.SecurityUtils;
import java.security.MessageDigest;

public class EncryptUtils {

    /* renamed from: a  reason: collision with root package name */
    private static final String f1026a = EncryptUtils.class.getName();
    private static boolean b;
    private static int c;
    private static byte[] d;
    private static byte[] e;
    public static String errorMsg;

    static {
        errorMsg = "";
        try {
            if (SecurityUtils.b) {
                d = initSocketAESKey();
                e = initHttpAESKey();
                b = (d == null || e == null || getSocketAESKey() == null || getHttpAESKey() == null || getRSAKeyId() == null || getVersion() == null) ? false : true;
            }
        } catch (Throwable th) {
            a.b(f1026a + "|load so error = " + th.toString());
            b = false;
            errorMsg = th.getMessage();
        }
        if (TextUtils.isEmpty(errorMsg)) {
            errorMsg = SecurityUtils.c;
        }
        if (!b) {
            a.b(f1026a + "|load so error ++++++++");
            if (TextUtils.isEmpty(errorMsg)) {
                errorMsg = "value = null, normal error";
                return;
            }
            return;
        }
        a.b(f1026a + "|load so success ~~~~~~~");
    }

    public static byte[] aesDecHttp(byte[] bArr, byte[] bArr2) {
        return SecurityUtils.c(e, bArr, bArr2);
    }

    public static byte[] aesDecSocket(byte[] bArr, byte[] bArr2) {
        return SecurityUtils.g(d, bArr, bArr2);
    }

    public static byte[] aesEncHttp(byte[] bArr, byte[] bArr2) {
        return SecurityUtils.b(e, bArr, bArr2);
    }

    public static byte[] aesEncSocket(byte[] bArr, byte[] bArr2) {
        return SecurityUtils.f(d, bArr, bArr2);
    }

    public static byte[] altAesDecSocket(byte[] bArr, byte[] bArr2) {
        return SecurityUtils.m(bArr, bArr2);
    }

    public static byte[] altAesEncSocket(byte[] bArr, byte[] bArr2) {
        return SecurityUtils.l(bArr, bArr2);
    }

    public static byte[] getBytesEncrypted(byte[] bArr) {
        return com.igexin.b.a.a.a.d(bArr, g.C);
    }

    public static byte[] getHttpAESKey() {
        return SecurityUtils.d(e);
    }

    public static String getHttpGTCV() {
        byte[] httpAESKey = getHttpAESKey();
        byte[] bytes = r.a(16).getBytes();
        byte[] bArr = new byte[(bytes.length + httpAESKey.length)];
        int a2 = f.a(bytes, 0, bArr, 0, bytes.length);
        int a3 = f.a(httpAESKey, 0, bArr, a2, httpAESKey.length) + a2;
        return i.b(bArr, 2);
    }

    public static String getHttpSignature(String str, byte[] bArr) {
        byte[] bytes = str.getBytes();
        byte[] bArr2 = new byte[(bytes.length + bArr.length)];
        int a2 = f.a(bytes, 0, bArr2, 0, bytes.length);
        if (bArr.length > 0) {
            int a3 = a2 + f.a(bArr, 0, bArr2, a2, bArr.length);
        }
        return i.b(sha1(bArr2), 2);
    }

    public static byte[] getIV(byte[] bArr) {
        return md5(bArr);
    }

    public static int getPacketId() {
        int i = c;
        c = i + 1;
        return i;
    }

    public static byte[] getRSAKeyId() {
        return SecurityUtils.j();
    }

    public static byte[] getSocketAESKey() {
        return SecurityUtils.h(d);
    }

    public static byte[] getSocketSignature(b bVar, int i, int i2) {
        byte[] bArr = new byte[(bVar.f989a + 11)];
        int a2 = f.a(i, bArr, 0);
        int a3 = a2 + f.a(i2, bArr, a2);
        int b2 = a3 + f.b((short) bVar.f989a, bArr, a3);
        int c2 = b2 + f.c(bVar.b, bArr, b2);
        int a4 = c2 + f.a(bVar.e, 0, bArr, c2, bVar.f989a);
        return sha1(bArr);
    }

    public static String getVersion() {
        byte[] k = SecurityUtils.k();
        if (k != null) {
            return new String(k);
        }
        return null;
    }

    public static byte[] initHttpAESKey() {
        return SecurityUtils.a();
    }

    public static byte[] initSocketAESKey() {
        return SecurityUtils.e();
    }

    public static boolean isLoadSuccess() {
        return b;
    }

    public static byte[] md5(byte[] bArr) {
        try {
            MessageDigest instance = MessageDigest.getInstance("MD5");
            instance.update(bArr);
            return instance.digest();
        } catch (Exception e2) {
            return null;
        }
    }

    public static boolean reset() {
        try {
            if (SecurityUtils.b) {
                d = initSocketAESKey();
                e = initHttpAESKey();
                b = (d == null || e == null || getSocketAESKey() == null || getHttpAESKey() == null || getRSAKeyId() == null || getVersion() == null) ? false : true;
            }
        } catch (Throwable th) {
            a.b(f1026a + "|load so error = " + th.toString());
            b = false;
        }
        if (!b) {
            a.b(f1026a + "|load so error ++++++++");
        } else {
            a.b(f1026a + "|load so success ~~~~~~~");
        }
        return b;
    }

    public static byte[] rsaEnc(byte[] bArr) {
        int length = bArr.length;
        if (length <= 214) {
            return SecurityUtils.i(bArr);
        }
        int i = length % Downloads.STATUS_SUCCESS == 0 ? length / Downloads.STATUS_SUCCESS : (length / Downloads.STATUS_SUCCESS) + 1;
        byte[] bArr2 = new byte[(i * 256)];
        int i2 = 0;
        int i3 = 0;
        while (i2 < i) {
            int i4 = i2 < i + -1 ? Downloads.STATUS_SUCCESS : length - (i2 * Downloads.STATUS_SUCCESS);
            byte[] bArr3 = new byte[i4];
            f.a(bArr, i2 * Downloads.STATUS_SUCCESS, bArr3, 0, i4);
            byte[] i5 = SecurityUtils.i(bArr3);
            i3 += f.a(i5, 0, bArr2, i3, i5.length);
            i2++;
        }
        return bArr2;
    }

    public static byte[] sha1(byte[] bArr) {
        try {
            return MessageDigest.getInstance("SHA-1").digest(bArr);
        } catch (Exception e2) {
            return null;
        }
    }
}
