package com.igexin.push.config;

import com.igexin.b.a.c.a;
import com.igexin.push.c.i;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class o {
    public static void a(String str, boolean z) {
        JSONObject jSONObject;
        String[] a2;
        String[] a3;
        String[] a4;
        String[] a5;
        String[] a6;
        String[] a7;
        String[] a8;
        String[] a9;
        String[] a10;
        a.b("IDCConfigParse parse idc config data : " + str);
        try {
            jSONObject = new JSONObject(str);
        } catch (Exception e) {
            jSONObject = null;
        }
        if (jSONObject != null) {
            if (jSONObject.has("N")) {
                try {
                    SDKUrlConfig.setLocation(jSONObject.getString("N"));
                } catch (JSONException e2) {
                }
            }
            if (jSONObject.has("X1") && (a10 = a(jSONObject, "X1")) != null && a10.length > 0) {
                SDKUrlConfig.setXfrAddressIps(a10);
                a.b("Detect_IDCConfigParse parse idc success, set new xfr address, reset and redetect +++++++++++++++++");
                if (z) {
                    i.a().h();
                }
            }
            if (jSONObject.has("X2") && (a9 = a(jSONObject, "X2")) != null && a9.length > 0) {
                SDKUrlConfig.XFR_ADDRESS_BAK = a9;
            }
            if (jSONObject.has("B") && (a8 = a(jSONObject, "B")) != null && a8.length > 0) {
                SDKUrlConfig.BI_ADDRESS_IPS = a8;
            }
            if (jSONObject.has("C") && (a7 = a(jSONObject, "C")) != null && a7.length > 0) {
                SDKUrlConfig.CONFIG_ADDRESS_IPS = a7;
            }
            if (jSONObject.has("S") && (a6 = a(jSONObject, "S")) != null && a6.length > 0) {
                SDKUrlConfig.STATE_ADDRESS_IPS = a6;
            }
            if (jSONObject.has("LO") && (a5 = a(jSONObject, "LO")) != null && a5.length > 0) {
                SDKUrlConfig.LOG_ADDRESS_IPS = a5;
            }
            if (jSONObject.has("A") && (a4 = a(jSONObject, "A")) != null && a4.length > 0) {
                SDKUrlConfig.AMP_ADDRESS_IPS = a4;
            }
            if (jSONObject.has("LB") && (a3 = a(jSONObject, "LB")) != null && a3.length > 0) {
                SDKUrlConfig.LBS_ADDRESS_IPS = a3;
            }
            if (jSONObject.has("I") && (a2 = a(jSONObject, "I")) != null && a2.length > 0) {
                SDKUrlConfig.INC_ADDRESS_IPS = a2;
            }
        }
    }

    private static String[] a(JSONObject jSONObject, String str) {
        try {
            JSONArray jSONArray = jSONObject.getJSONArray(str);
            int length = jSONArray.length();
            String[] strArr = new String[length];
            for (int i = 0; i < length; i++) {
                if (str.equals("X1") || str.equals("X2")) {
                    strArr[i] = "socket://" + jSONArray.getString(i);
                } else {
                    strArr[i] = "http://" + jSONArray.getString(i);
                }
            }
            return strArr;
        } catch (Exception e) {
            return null;
        }
    }
}
