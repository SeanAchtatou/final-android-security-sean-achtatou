package com.igexin.push.c;

import com.igexin.b.a.b.c;
import com.igexin.push.config.SDKUrlConfig;
import com.igexin.push.util.a;
import java.util.List;

public class i {

    /* renamed from: a  reason: collision with root package name */
    public static boolean f865a;
    private static final String b = i.class.getName();
    private static i c;
    private static h d;

    private i() {
        b();
        if (a.d()) {
            d = h.WIFI;
        } else {
            d = h.MOBILE;
        }
    }

    public static synchronized i a() {
        i iVar;
        synchronized (i.class) {
            if (c == null) {
                c = new i();
            }
            iVar = c;
        }
        return iVar;
    }

    public void b() {
        f865a = SDKUrlConfig.realXfrListIsOnly();
    }

    public void c() {
        if (f865a) {
            com.igexin.b.a.c.a.b(b + "|xfr len = 1, detect = false");
        } else {
            c.b().a(k.b_(), false, true);
        }
    }

    public void d() {
        try {
            i().d();
        } catch (Throwable th) {
            com.igexin.b.a.c.a.b(b + "|" + th.toString());
        }
    }

    public a e() {
        return i().d;
    }

    public boolean f() {
        if (f865a) {
            return false;
        }
        m i = i();
        if (!i.m()) {
            com.igexin.b.a.c.a.b(b + "|startDetect detect = false");
            return false;
        }
        com.igexin.b.a.c.a.b(b + "|network changed detect = true, reset detect delay");
        i.f();
        return true;
    }

    public String g() {
        j g;
        if (f865a) {
            return SDKUrlConfig.getXfrAddress()[0];
        }
        m i = i();
        if (i == null || (g = i.g()) == null) {
            return null;
        }
        return g.g();
    }

    public void h() {
        b();
        if (f865a) {
            k.b_().g();
            try {
                l.a().d.a((List<e>) null);
                r.a().d.a((List<e>) null);
                r.a().i();
                l.a().i();
                r.a().l();
            } catch (Throwable th) {
                com.igexin.b.a.c.a.b(b + "|" + th.toString());
            }
        } else {
            try {
                r.a().l();
                r.a().h();
                l.a().h();
                m i = i();
                if (i != null) {
                    i.j();
                }
            } catch (Throwable th2) {
                com.igexin.b.a.c.a.b(b + "|" + th2.toString());
            }
        }
    }

    public m i() {
        m a2;
        if (a.d()) {
            com.igexin.b.a.c.a.b(b + "|wifi state");
            a2 = r.a();
        } else {
            com.igexin.b.a.c.a.b(b + "|mobile state");
            a2 = l.a();
        }
        h b2 = a2.b();
        if (b2 != d) {
            if (b2 == h.WIFI) {
                l.a().e();
            } else if (b2 == h.MOBILE) {
                r.a().e();
            }
        }
        d = b2;
        return a2;
    }
}
