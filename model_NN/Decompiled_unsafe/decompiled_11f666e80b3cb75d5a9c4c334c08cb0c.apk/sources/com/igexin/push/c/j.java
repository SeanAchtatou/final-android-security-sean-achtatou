package com.igexin.push.c;

import com.igexin.b.a.c.a;
import com.meizu.cloud.pushsdk.pushtracer.constant.Parameters;
import org.json.JSONException;
import org.json.JSONObject;

public class j {

    /* renamed from: a  reason: collision with root package name */
    private static final String f866a = j.class.getName();
    private String b;
    private String c;
    private int d;
    private long e = 2147483647L;
    private long f = -1;
    private boolean g = true;
    private int h;
    private int i;
    private int j = 3;

    public j() {
    }

    public j(String str, int i2) {
        this.b = str;
        this.d = i2;
    }

    private void j() {
        this.c = null;
        this.h = 0;
        this.g = true;
    }

    private boolean k() {
        return this.c != null && System.currentTimeMillis() - this.f <= f.b && this.h < this.j;
    }

    public synchronized String a() {
        return this.b;
    }

    public void a(int i2) {
        this.d = i2;
    }

    public void a(long j2) {
        this.e = j2;
    }

    public synchronized void a(String str) {
        this.b = str;
    }

    public synchronized void a(String str, long j2, long j3) {
        this.c = str;
        this.e = j2;
        this.f = j3;
        this.h = 0;
        this.i = 0;
        this.g = false;
    }

    public void a(boolean z) {
        this.g = z;
    }

    public synchronized String b(boolean z) {
        String str;
        if (k()) {
            if (z) {
                this.h++;
                a.b(f866a + "|disc network, ipFailedCnt++  = " + this.h);
            } else {
                a.b(f866a + "|disc user, ipFailedCnt =  " + this.h);
            }
            a.b(f866a + "|disc, ip is valid, use ip = " + this.c);
            this.g = false;
            str = this.c;
        } else {
            j();
            a.b(f866a + "|disc, ip is invalid, use domain = " + this.b);
            if (z) {
                this.i++;
                a.b(f866a + "|disc network, domainFailedCnt++ = " + this.i);
            } else {
                a.b(f866a + "|disc user, domainFailedCnt =  " + this.i);
            }
            str = this.b;
        }
        return str;
    }

    public synchronized void b() {
        this.c = null;
        this.e = 2147483647L;
        this.f = -1;
        this.g = true;
        this.h = 0;
    }

    public synchronized void b(int i2) {
        if (i2 < 1) {
            i2 = 1;
        }
        this.j = i2;
    }

    public void b(long j2) {
        this.f = j2;
    }

    public void b(String str) {
        this.c = str;
    }

    public String c() {
        return this.c;
    }

    public int d() {
        return this.d;
    }

    public synchronized long e() {
        return this.e;
    }

    public synchronized boolean f() {
        boolean z = true;
        synchronized (this) {
            if (!k()) {
                if (this.i >= this.j) {
                    this.i = 0;
                    z = false;
                }
            }
        }
        return z;
    }

    public synchronized String g() {
        String str;
        if (k()) {
            this.g = false;
            str = this.c;
        } else {
            j();
            str = this.b;
        }
        return str;
    }

    public synchronized void h() {
        a.b(f866a + "|register or login success, reset ipFailedCnt + domainFailedCnt");
        this.h = 0;
        this.i = 0;
    }

    public JSONObject i() {
        if (this.b == null || this.c == null) {
            return null;
        }
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("domain", this.b);
            jSONObject.put(Parameters.IP_ADDRESS, this.c);
            if (this.e != 2147483647L) {
                jSONObject.put("consumeTime", this.e);
            }
            jSONObject.put("port", this.d);
            if (this.f != -1) {
                jSONObject.put("detectSuccessTime", this.f);
            }
            jSONObject.put("isDomain", this.g);
            jSONObject.put("connectTryCnt", this.j);
            return jSONObject;
        } catch (JSONException e2) {
            a.b(f866a + e2.toString());
            return null;
        }
    }
}
