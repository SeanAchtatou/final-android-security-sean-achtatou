package com.igexin.push.c;

import android.text.TextUtils;
import cn.banshenggua.aichang.player.PlayerService;
import com.igexin.b.a.c.a;
import com.igexin.push.core.g;

public class l extends m implements o {
    private static final String e = l.class.getName();
    private static l f;

    private l() {
        super(g.ax, g.az);
    }

    public static synchronized l a() {
        l lVar;
        synchronized (l.class) {
            if (f == null) {
                f = new l();
            }
            lVar = f;
        }
        return lVar;
    }

    public void a(g gVar, j jVar) {
        p a2;
        if (jVar != null && !TextUtils.isEmpty(jVar.a()) && (a2 = a(jVar.a())) != null) {
            b(jVar);
            if (gVar == g.SUCCESS) {
                a2.g();
                a2.j();
                k();
                n();
            } else if (gVar == g.EXCEPTION || gVar == g.FAILED) {
                o();
                a.b(e + "|detect" + c(jVar) + "failed --------");
                if (q()) {
                    a.b(e + "|pool is not empty, detect task " + c(jVar) + PlayerService.ACTION_STOP);
                    a2.g();
                    return;
                }
                a2.a(false);
            }
        }
    }

    public void a(j jVar) {
    }

    public h b() {
        return h.MOBILE;
    }

    public o c() {
        return this;
    }
}
