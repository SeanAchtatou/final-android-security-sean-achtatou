package com.igexin.push.c;

import com.igexin.b.a.c.a;
import com.igexin.push.core.g;
import com.igexin.push.f.b.h;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class p extends h {

    /* renamed from: a  reason: collision with root package name */
    public static final int f870a = ((int) a(20150601L));
    /* access modifiers changed from: private */
    public static final String b = p.class.getName();
    /* access modifiers changed from: private */
    public volatile boolean L = true;
    /* access modifiers changed from: private */
    public volatile boolean M;
    /* access modifiers changed from: private */
    public j c;
    /* access modifiers changed from: private */
    public Thread e;
    /* access modifiers changed from: private */
    public o f;
    private long g = -1;
    /* access modifiers changed from: private */
    public Lock h = new ReentrantLock();
    /* access modifiers changed from: private */
    public Condition i = this.h.newCondition();
    /* access modifiers changed from: private */
    public final List<Object> j = new ArrayList(1);

    public p() {
        super(604800000);
        this.o = true;
    }

    private void A() {
        try {
            if (this.e != null) {
                this.e.interrupt();
            }
        } catch (Exception e2) {
        }
    }

    private void B() {
        try {
            this.h.lock();
            this.j.add(new Object());
            this.i.signalAll();
            try {
                this.h.unlock();
            } catch (Exception e2) {
            }
        } catch (Exception e3) {
            try {
                this.h.unlock();
            } catch (Exception e4) {
            }
        } catch (Throwable th) {
            try {
                this.h.unlock();
            } catch (Exception e5) {
            }
            throw th;
        }
    }

    private static long a(long j2) {
        long j3 = j2 / 10;
        return ((long) (((Math.random() * ((double) j3)) * 2.0d) - ((double) j3))) + j2;
    }

    private void x() {
        this.e = new Thread(new q(this));
        this.e.start();
    }

    private void y() {
        a(1800000, TimeUnit.MILLISECONDS);
        if (this.e == null) {
            this.L = true;
            this.j.add(new Object());
            x();
            return;
        }
        try {
            this.h.lock();
            a.b(b + "|detect " + z() + "running, start");
            this.j.add(new Object());
            this.i.signalAll();
            try {
                this.h.unlock();
            } catch (Exception e2) {
            }
        } catch (Exception e3) {
            try {
                this.h.unlock();
            } catch (Exception e4) {
            }
        } catch (Throwable th) {
            try {
                this.h.unlock();
            } catch (Exception e5) {
            }
            throw th;
        }
    }

    /* access modifiers changed from: private */
    public String z() {
        return this.c.a() + "[" + this.c.c() + "] ";
    }

    /* access modifiers changed from: protected */
    public void a() {
        if (!this.M) {
            y();
        }
    }

    public void a(j jVar) {
        this.c = jVar;
    }

    public void a(o oVar) {
        this.f = oVar;
    }

    public void a(boolean z) {
        if (z) {
            j();
            a.b(b + "|detect " + z() + "reset delay = 0");
        }
        a.b(b + "|network available : " + g.h);
        if (!g.h) {
            this.g = 604800000;
        } else {
            if (this.g <= 2000) {
                this.g += 500;
            } else if (this.g <= 15000) {
                this.g += 5000;
            } else if (this.g <= 60000) {
                this.g += 15000;
            } else {
                this.g += 120000;
            }
            if (this.g > 3600000) {
                this.g = 3600000;
            }
            this.g = a(this.g);
            a.b(b + "|detect " + z() + "redetect delay = " + this.g);
        }
        a(this.g, TimeUnit.MILLISECONDS);
    }

    public final int b() {
        return f870a;
    }

    public void c() {
        super.c();
    }

    public j c_() {
        return this.c;
    }

    public void d() {
    }

    public void g() {
        a.b(b + "|detect " + z() + "finish, task stop");
        a(604800000, TimeUnit.MILLISECONDS);
    }

    public void h() {
        this.M = true;
        this.L = false;
        this.o = false;
        this.f = null;
        B();
        p();
        A();
    }

    public void i() {
        a.b(b + "|detect " + z() + "start");
        this.g = 50;
        a(this.g, TimeUnit.MILLISECONDS);
    }

    public void j() {
        this.g = 0;
    }
}
