package com.igexin.push.d.c;

import com.igexin.b.a.b.f;
import org.apache.mina.proxy.handlers.socks.SocksProxyConstants;

public class d extends e {

    /* renamed from: a  reason: collision with root package name */
    public int f991a;
    public int b;
    public long c;
    public String d;
    public Object e;
    public Object f;
    public String g = "";
    public String h = "UTF-8";

    public d() {
        this.i = 25;
        this.j = 20;
    }

    public final void a() {
        this.b = 128;
    }

    public void a(byte[] bArr) {
        int i;
        int i2;
        this.f991a = f.c(bArr, 0);
        this.b = bArr[2] & 192;
        this.h = a(bArr[2]);
        this.c = f.e(bArr, 3);
        byte b2 = bArr[11] & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD;
        try {
            this.d = new String(bArr, 12, b2, this.h);
        } catch (Exception e2) {
        }
        int i3 = b2 + 12;
        int i4 = 0;
        while (true) {
            i = i4 | (bArr[i3] & Byte.MAX_VALUE);
            if ((bArr[i3] & 128) == 0) {
                break;
            }
            i4 = i << 7;
            i3++;
        }
        int i5 = i3 + 1;
        if (i > 0) {
            if (this.b == 192) {
                this.e = new byte[i];
                System.arraycopy(bArr, i5, this.e, 0, i);
            } else {
                try {
                    this.e = new String(bArr, i5, i, this.h);
                } catch (Exception e3) {
                }
            }
        }
        int i6 = i + i5;
        int i7 = 0;
        while (true) {
            i2 = i7 | (bArr[i6] & Byte.MAX_VALUE);
            if ((bArr[i6] & 128) == 0) {
                break;
            }
            i7 = i2 << 7;
            i6++;
        }
        int i8 = i6 + 1;
        if (i2 > 0) {
            this.f = new byte[i2];
            System.arraycopy(bArr, i8, this.f, 0, i2);
        }
        int i9 = i2 + i8;
        if (bArr.length > i9) {
            int i10 = i9 + 1;
            byte b3 = bArr[i9] & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD;
            try {
                this.g = new String(bArr, i10, b3, this.h);
            } catch (Exception e4) {
            }
            int i11 = b3 + i10;
        }
    }

    public byte[] d() {
        byte[] bArr;
        int i = 0;
        try {
            byte[] bytes = this.d.getBytes(this.h);
            byte[] bytes2 = !"".equals(this.e) ? this.b == 192 ? (byte[]) this.e : ((String) this.e).getBytes(this.h) : null;
            byte[] bArr2 = this.f != null ? (byte[]) this.f : null;
            byte[] bytes3 = this.g.getBytes(this.h);
            int length = bytes2 == null ? 0 : bytes2.length;
            if (bArr2 != null) {
                i = bArr2.length;
            }
            byte[] a2 = f.a(length);
            byte[] a3 = f.a(i);
            bArr = new byte[(bytes.length + 13 + a2.length + length + a3.length + i + bytes3.length)];
            try {
                int b2 = f.b(this.f991a, bArr, 0);
                int c2 = b2 + f.c(this.b | a(this.h), bArr, b2);
                int a4 = c2 + f.a(this.c, bArr, c2);
                int c3 = a4 + f.c(bytes.length, bArr, a4);
                int a5 = c3 + f.a(bytes, 0, bArr, c3, bytes.length);
                int a6 = a5 + f.a(a2, 0, bArr, a5, a2.length);
                if (length > 0) {
                    a6 += f.a(bytes2, 0, bArr, a6, length);
                }
                int a7 = a6 + f.a(a3, 0, bArr, a6, a3.length);
                if (i > 0) {
                    a7 += f.a(bArr2, 0, bArr, a7, i);
                }
                int c4 = a7 + f.c(bytes3.length, bArr, a7);
                int a8 = c4 + f.a(bytes3, 0, bArr, c4, bytes3.length);
            } catch (Exception e2) {
            }
        } catch (Exception e3) {
            bArr = null;
        }
        if (bArr != null && bArr.length >= 512) {
            this.j = (byte) (this.j | 128);
        }
        return bArr;
    }
}
