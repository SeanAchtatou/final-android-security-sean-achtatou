package com.igexin.push.d;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import com.igexin.b.a.b.a.a.k;
import com.igexin.b.a.b.b;
import com.igexin.b.a.b.e;

public class a implements com.igexin.b.a.d.a.a<String, Integer, b, e> {

    /* renamed from: a  reason: collision with root package name */
    public ConnectivityManager f983a;
    public Context b;

    public a(Context context, ConnectivityManager connectivityManager) {
        this.f983a = connectivityManager;
        this.b = context;
    }

    public e a(String str, Integer num, b bVar) {
        NetworkInfo activeNetworkInfo;
        if (!str.startsWith("socket") || this.f983a == null || (activeNetworkInfo = this.f983a.getActiveNetworkInfo()) == null || !activeNetworkInfo.isAvailable()) {
            return null;
        }
        return new k(str, bVar);
    }
}
