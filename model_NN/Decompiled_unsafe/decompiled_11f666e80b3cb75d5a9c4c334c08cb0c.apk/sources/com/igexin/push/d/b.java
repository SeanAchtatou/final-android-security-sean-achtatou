package com.igexin.push.d;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import com.igexin.push.config.m;
import com.igexin.push.core.g;
import com.igexin.push.f.b.e;
import com.igexin.push.util.a;

public class b {

    /* renamed from: a  reason: collision with root package name */
    private int f985a;
    private int b;
    private boolean c;
    private int d;
    private long e;
    private int f;
    private long g;
    private d h;
    private i i;

    private b() {
        this.f985a = m.H;
        this.b = m.J;
        this.i = new f();
        this.h = a.d() ? d.WIFI : d.MOBILE;
    }

    public static b a() {
        return e.f1004a;
    }

    private void a(int i2) {
        if (g.f != null) {
            try {
                Intent intent = new Intent();
                intent.setAction("com.igexin.sdk.action.polling");
                Bundle bundle = new Bundle();
                bundle.putInt("code", i2);
                intent.putExtras(bundle);
                if (a.e()) {
                    LocalBroadcastManager.getInstance(g.f.getApplicationContext()).sendBroadcast(intent);
                } else {
                    g.f.sendBroadcast(intent);
                }
            } catch (Throwable th) {
            }
        }
    }

    private void h() {
        com.igexin.b.a.c.a.b("ConnectModelCoordinator|reset current model = normal");
        if (this.i != null && !(this.i instanceof f)) {
            this.i = new f();
        }
        e.g().i();
        this.f = 0;
        this.d = 0;
        this.c = false;
        com.igexin.push.core.b.g.a().b(this.c);
    }

    private void i() {
        a(0);
    }

    private void j() {
        a(1);
    }

    public void a(boolean z) {
        this.c = z;
        com.igexin.b.a.c.a.b("ConnectModelCoordinator|init, current is polling model = " + z);
        if (z) {
            e.g().h();
        }
    }

    public synchronized void b() {
        d dVar = a.d() ? d.WIFI : d.MOBILE;
        if (dVar != this.h) {
            com.igexin.b.a.c.a.b("ConnectModelCoordinator|net type changed " + this.h + "->" + dVar);
            h();
            this.h = dVar;
        }
    }

    public i c() {
        return this.i;
    }

    public synchronized void d() {
        if (!this.c) {
            long currentTimeMillis = System.currentTimeMillis() - this.e;
            if (currentTimeMillis > 20000 && currentTimeMillis < 200000) {
                this.d++;
                com.igexin.b.a.c.a.b("ConnectModelCoordinator|read len = -1, interval = " + currentTimeMillis + ", disconnect =" + this.d);
                if (this.d >= this.f985a) {
                    com.igexin.b.a.c.a.b("ConnectModelCoordinator|enter polling mode ####");
                    i();
                    this.c = true;
                    this.i = new g();
                    e.g().h();
                    com.igexin.push.core.b.g.a().b(this.c);
                }
            }
        }
    }

    public synchronized void e() {
        if (this.c) {
            if (System.currentTimeMillis() - this.g >= 120000) {
                this.f++;
                com.igexin.b.a.c.a.b("ConnectModelCoordinator|polling mode, cur hearbeat =" + this.f);
                if (this.f >= this.b) {
                    com.igexin.b.a.c.a.b("ConnectModelCoordinator|enter normal mode ####");
                    j();
                    g.E = 0;
                    h();
                }
            }
            this.g = System.currentTimeMillis();
        }
    }

    public void f() {
        this.e = System.currentTimeMillis();
        if (this.c) {
            this.i = new g();
            e.g().h();
            this.f = 0;
        }
    }

    public void g() {
        if (this.c && this.i != null && !(this.i instanceof f)) {
            this.i = new f();
        }
    }
}
