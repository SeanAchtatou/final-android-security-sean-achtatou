package com.igexin.push.f.b;

import com.igexin.b.a.d.d;
import java.util.concurrent.TimeUnit;

public abstract class h extends d {
    long d;

    public h(long j) {
        this(0, j);
    }

    public h(long j, long j2) {
        super(5);
        this.d = j > 0 ? j2 + (j - System.currentTimeMillis()) : j2;
        a(this.d, TimeUnit.MILLISECONDS);
    }

    /* access modifiers changed from: protected */
    public abstract void a();

    public final void a_() {
        super.a_();
        a();
    }

    /* access modifiers changed from: protected */
    public void e() {
    }
}
