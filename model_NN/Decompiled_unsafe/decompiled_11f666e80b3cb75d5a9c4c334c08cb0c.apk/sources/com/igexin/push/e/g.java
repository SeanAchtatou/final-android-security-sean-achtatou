package com.igexin.push.e;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;
import com.igexin.push.core.d;
import com.igexin.push.core.f;
import com.igexin.push.d.c.e;
import com.igexin.push.d.c.n;
import com.igexin.sdk.aidl.a;
import java.util.List;

class g implements ServiceConnection {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ b f1011a;
    final /* synthetic */ String b;
    final /* synthetic */ c c;

    g(c cVar, b bVar, String str) {
        this.c = cVar;
        this.f1011a = bVar;
        this.b = str;
    }

    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        if (this.c.b == d.active) {
            try {
                this.f1011a.a(a.a(iBinder));
                this.c.g.put(this.b, this.f1011a);
                if (this.f1011a.c().onASNLConnected(this.f1011a.a(), this.f1011a.b(), this.b, 0) == -1) {
                    this.c.g.remove(this.b);
                } else if (com.igexin.push.core.g.l) {
                    this.f1011a.c().onASNLNetworkConnected();
                }
            } catch (Exception e) {
                this.c.g.remove(this.b);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.igexin.push.e.j.a(java.lang.String, com.igexin.push.d.c.e, boolean):int
     arg types: [java.lang.String, com.igexin.push.d.c.n, int]
     candidates:
      com.igexin.push.e.j.a(android.content.Context, com.igexin.b.a.b.c, com.igexin.push.e.k):void
      com.igexin.push.e.j.a(java.lang.String, com.igexin.push.d.c.e, boolean):int */
    public void onServiceDisconnected(ComponentName componentName) {
        if (this.c.b == d.active) {
            this.c.g.remove(this.b);
            List a2 = this.c.c(((b) this.c.g.get(this.b)).e());
            if (a2.size() != 0) {
                int i = 0;
                while (true) {
                    int i2 = i;
                    if (i2 < a2.size()) {
                        String str = (String) a2.get(i2);
                        if (str.startsWith("S-")) {
                            n nVar = new n();
                            nVar.f999a = Long.valueOf(str.substring(2)).longValue();
                            f.a().g().a("S-" + String.valueOf(nVar.f999a), (e) nVar, true);
                        }
                        this.c.h.remove(str);
                        i = i2 + 1;
                    } else {
                        return;
                    }
                }
            }
        }
    }
}
