package com.igexin.push.e;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;
import com.igexin.push.core.c;
import com.igexin.push.core.d;
import com.igexin.sdk.aidl.a;

class f implements ServiceConnection {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ c f1010a;

    f(c cVar) {
        this.f1010a = cVar;
    }

    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        if (this.f1010a.b == d.prepare) {
            this.f1010a.e.a(a.a(iBinder));
            a aVar = new a();
            aVar.a(c.connectASNL);
            this.f1010a.a(aVar);
        }
    }

    public void onServiceDisconnected(ComponentName componentName) {
        if (this.f1010a.b == d.passive) {
            com.igexin.push.core.f.a().g().b(true);
            this.f1010a.c();
        }
    }
}
