package com.igexin.push.e;

import android.content.Context;
import android.content.Intent;
import android.os.RemoteException;
import com.igexin.push.core.d;
import com.igexin.push.core.g;
import com.igexin.push.d.c.a;
import com.igexin.push.d.c.b;
import com.igexin.push.d.c.e;
import com.igexin.push.d.c.f;
import com.igexin.push.d.c.i;
import com.igexin.push.d.c.k;
import com.igexin.push.d.c.m;
import com.igexin.push.d.c.n;
import com.igexin.push.d.c.o;
import com.igexin.push.d.c.p;
import com.igexin.push.d.c.q;
import com.igexin.push.f.b.h;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class c {

    /* renamed from: a  reason: collision with root package name */
    private Context f1007a;
    /* access modifiers changed from: private */
    public d b;
    private h c;
    private h d;
    /* access modifiers changed from: private */
    public b e;
    private List<i> f;
    /* access modifiers changed from: private */
    public Map<String, b> g;
    /* access modifiers changed from: private */
    public Map<String, b> h;
    private boolean i;

    private e a(byte[] bArr) {
        b b2 = b(bArr);
        e eVar = null;
        switch (b2.b) {
            case 4:
                eVar = new k();
                break;
            case 5:
                eVar = new m();
                break;
            case 6:
                eVar = new f();
                break;
            case 9:
                eVar = new q();
                break;
            case 25:
                eVar = new com.igexin.push.d.c.d();
                break;
            case 26:
                eVar = new p();
                break;
            case 27:
                eVar = new com.igexin.push.d.c.c();
                break;
            case 28:
                eVar = new a();
                break;
            case 36:
                eVar = new n();
                break;
            case 37:
                eVar = new o();
                break;
            case 96:
                eVar = new i();
                break;
        }
        if (eVar != null) {
            eVar.a(b2.e);
        }
        return eVar;
    }

    private b b(byte[] bArr) {
        b bVar = new b();
        bVar.f989a = com.igexin.b.a.b.f.c(bArr, 0);
        bVar.b = bArr[2];
        bVar.e = new byte[bVar.f989a];
        com.igexin.b.a.b.f.a(bArr, 3, bVar.e, 0, bVar.f989a);
        return bVar;
    }

    private void b(a aVar) {
        switch (h.b[aVar.f1005a.ordinal()]) {
            case 1:
                this.f.clear();
                this.g.clear();
                this.h.clear();
                if (this.c != null) {
                    this.c.u();
                }
                if (this.d != null) {
                    this.d.u();
                }
                this.i = false;
                boolean z = g.i;
                boolean z2 = g.j;
                boolean a2 = com.igexin.push.util.a.a(System.currentTimeMillis());
                boolean b2 = com.igexin.push.util.a.b();
                if (z && z2 && !a2 && b2) {
                    if (com.igexin.push.config.m.n) {
                        Intent intent = new Intent();
                        intent.setAction("com.igexin.sdk.action.snlrefresh");
                        intent.putExtra("groupid", g.d);
                        intent.putExtra("branch", "open");
                        intent.putExtra("responseSNLAction", g.V);
                        this.f1007a.sendBroadcast(intent);
                        this.c = new d(this, 1000);
                        if (!com.igexin.push.core.f.a().a(this.c)) {
                        }
                        return;
                    }
                    com.igexin.b.a.c.a.b("SNLCoordinator|isSnl = false, doActive");
                    d();
                    return;
                }
                return;
            case 2:
                int size = this.f.size();
                if (size == 0) {
                    d();
                    return;
                }
                int i2 = 1;
                i iVar = this.f.get(0);
                while (i2 < size) {
                    i iVar2 = this.f.get(i2);
                    if (iVar2.c() >= iVar.c()) {
                        iVar2 = iVar;
                    }
                    i2++;
                    iVar = iVar2;
                }
                if (this.f1007a.getPackageName().equals(iVar.a())) {
                    d();
                    return;
                }
                this.b = d.prepare;
                this.e = new b();
                this.e.a(iVar.a());
                this.e.b(iVar.b());
                a aVar2 = new a();
                aVar2.a(com.igexin.push.core.c.determine);
                a(aVar2);
                return;
            case 3:
            case 4:
                if (this.c != null) {
                    this.c.u();
                }
                c();
                return;
            default:
                return;
        }
    }

    private void b(String str) {
        b bVar = this.g.get(str);
        bVar.a(new g(this, bVar, str));
        try {
            Context createPackageContext = this.f1007a.createPackageContext(bVar.a(), 3);
            this.f1007a.bindService(new Intent(createPackageContext, createPackageContext.getClassLoader().loadClass(bVar.b())), bVar.d(), 0);
        } catch (Exception e2) {
        }
        this.g.put(str, bVar);
    }

    /* access modifiers changed from: private */
    public List<String> c(String str) {
        ArrayList arrayList = new ArrayList();
        for (Map.Entry next : this.h.entrySet()) {
            if (((b) next.getValue()).e().equals(str)) {
                arrayList.add(next.getKey());
            }
        }
        return arrayList;
    }

    /* access modifiers changed from: private */
    public void c() {
        this.b = d.init;
        a aVar = new a();
        aVar.a(com.igexin.push.core.c.start);
        a(aVar);
    }

    private void c(a aVar) {
        switch (h.b[aVar.f1005a.ordinal()]) {
            case 3:
            case 4:
                if (this.d != null) {
                    this.d.u();
                }
                if (this.e.c() != null) {
                    try {
                        this.f1007a.unbindService(this.e.d());
                    } catch (Exception e2) {
                    }
                }
                c();
                return;
            case 5:
                this.d = new e(this, 5000);
                if (!com.igexin.push.core.f.a().a(this.d)) {
                }
                this.e.a(new f(this));
                try {
                    Context createPackageContext = this.f1007a.createPackageContext(this.e.a(), 3);
                    this.f1007a.bindService(new Intent(createPackageContext, createPackageContext.getClassLoader().loadClass(this.e.b())), this.e.d(), 0);
                    return;
                } catch (Exception e3) {
                    return;
                }
            case 6:
                try {
                    this.e.c().onPSNLConnected(this.f1007a.getPackageName(), com.igexin.push.core.a.e.a().a(g.f).getName(), "", 0);
                    return;
                } catch (Exception e4) {
                    return;
                }
            default:
                return;
        }
    }

    /* access modifiers changed from: private */
    public void d() {
        this.f.clear();
        this.b = d.active;
        if (this.i) {
            for (Map.Entry<String, b> key : this.g.entrySet()) {
                b((String) key.getKey());
            }
            this.i = false;
        }
        com.igexin.b.a.c.a.b("SNLCoordinator|doActive, state = " + this.b + ", setASNL = true");
        com.igexin.push.core.f.a().g().a(true);
    }

    private void d(a aVar) {
        switch (h.b[aVar.f1005a.ordinal()]) {
            case 3:
                com.igexin.b.a.c.a.b("SNLCoordinator|event = stop, setASNL(false)");
                com.igexin.push.core.f.a().g().a(false);
                com.igexin.push.core.f.a().g().b(true);
                if (com.igexin.push.config.m.n) {
                    Intent intent = new Intent();
                    intent.setAction("com.igexin.sdk.action.snlretire");
                    intent.putExtra("groupid", g.d);
                    intent.putExtra("branch", "open");
                    this.f1007a.sendBroadcast(intent);
                    return;
                }
                a aVar2 = new a();
                aVar2.a(com.igexin.push.core.c.retire);
                com.igexin.push.core.f.a().h().a(aVar2);
                return;
            case 4:
                g.U = System.currentTimeMillis();
                for (Map.Entry value : this.g.entrySet()) {
                    try {
                        this.f1007a.unbindService(((b) value.getValue()).d());
                    } catch (Exception e2) {
                    }
                }
                com.igexin.b.a.c.a.b("SNLCoordinator | state = " + d.passive + ", setASNL(false)");
                com.igexin.push.core.f.a().g().a(false);
                c();
                return;
            case 5:
            case 6:
            default:
                return;
            case 7:
                Intent intent2 = new Intent();
                intent2.setAction("com.igexin.sdk.action.snlretire");
                intent2.putExtra("groupid", g.d);
                intent2.putExtra("branch", "open");
                this.f1007a.sendBroadcast(intent2);
                return;
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.igexin.push.e.j.a(java.lang.String, com.igexin.push.d.c.e, boolean):int
     arg types: [java.lang.String, com.igexin.push.d.c.n, int]
     candidates:
      com.igexin.push.e.j.a(android.content.Context, com.igexin.b.a.b.c, com.igexin.push.e.k):void
      com.igexin.push.e.j.a(java.lang.String, com.igexin.push.d.c.e, boolean):int */
    private void e(a aVar) {
        switch (h.b[aVar.f1005a.ordinal()]) {
            case 3:
                if (g.l) {
                    n nVar = new n();
                    nVar.f999a = g.q;
                    com.igexin.push.core.f.a().g().a("S-" + String.valueOf(g.q), (e) nVar, true);
                    break;
                }
                break;
            case 4:
                break;
            default:
                return;
        }
        g.U = System.currentTimeMillis();
        com.igexin.push.core.f.a().g().b(true);
        try {
            this.f1007a.unbindService(this.e.d());
        } catch (Exception e2) {
        }
        c();
    }

    public int a(String str) {
        if (this.b != d.prepare || str.equals("")) {
            return -1;
        }
        this.d.u();
        this.g.clear();
        this.h.clear();
        this.i = false;
        this.f.clear();
        this.e.c(str);
        this.b = d.passive;
        com.igexin.b.a.c.a.b("SNLCoordinator | state = " + d.passive + ", setASNL(false)");
        com.igexin.push.core.f.a().g().a(false);
        return 0;
    }

    public int a(String str, e eVar) {
        b bVar = new b();
        bVar.e = eVar.d();
        if (bVar.e != null) {
            bVar.f989a = bVar.e.length;
            bVar.b = (byte) eVar.i;
            byte[] a2 = bVar.a();
            if (!(this.e == null || this.e.c() == null)) {
                try {
                    return this.e.c().sendByASNL(this.e.e(), str, a2);
                } catch (Exception e2) {
                    c();
                }
            }
        }
        return -1;
    }

    public int a(String str, String str2) {
        if (this.b == d.prepare || this.b == d.passive) {
            return -1;
        }
        b bVar = new b();
        bVar.a(str);
        bVar.b(str2);
        bVar.c(str);
        this.g.put(str, bVar);
        if (this.b == d.active) {
            b(str);
        } else {
            this.i = true;
        }
        return 0;
    }

    public int a(String str, String str2, byte[] bArr) {
        e a2 = a(bArr);
        b bVar = this.g.get(str);
        if (bVar == null || !g.l) {
            return -1;
        }
        this.h.put(str2, bVar);
        return com.igexin.push.core.f.a().g().a(str2, a2);
    }

    public void a(Context context) {
        this.f1007a = context;
        this.b = d.init;
        this.f = new ArrayList();
        this.g = new HashMap();
        this.h = new HashMap();
    }

    public void a(Intent intent) {
        if (intent.getAction().equals("com.igexin.sdk.action.snlrefresh") && com.igexin.push.config.m.n) {
            String stringExtra = intent.getStringExtra("groupid");
            String stringExtra2 = intent.getStringExtra("responseSNLAction");
            boolean z = g.i;
            boolean z2 = g.j;
            String stringExtra3 = intent.getStringExtra("branch");
            boolean a2 = com.igexin.push.util.a.a(System.currentTimeMillis());
            long a3 = com.igexin.push.core.q.a() + com.igexin.push.core.q.b();
            if (g.d.equals(stringExtra) && "open".equals(stringExtra3) && z && z2 && !a2) {
                Intent intent2 = new Intent();
                intent2.setAction(stringExtra2);
                intent2.putExtra("groupid", g.d);
                intent2.putExtra("branch", "open");
                intent2.putExtra("pkgname", g.f.getPackageName());
                intent2.putExtra("classname", com.igexin.push.core.a.e.a().a(g.f));
                intent2.putExtra("startup_time", g.U);
                intent2.putExtra("network_traffic", a3);
                g.f.sendBroadcast(intent2);
            }
        } else if (intent.getAction().equals(g.V) && com.igexin.push.config.m.n) {
            String stringExtra4 = intent.getStringExtra("groupid");
            String stringExtra5 = intent.getStringExtra("branch");
            if (g.d.equals(stringExtra4) && "open".equals(stringExtra5)) {
                i iVar = new i();
                iVar.a(intent.getStringExtra("groupid"));
                iVar.b(intent.getStringExtra("pkgname"));
                iVar.c(intent.getStringExtra("classname"));
                iVar.a(intent.getLongExtra("startup_time", 0));
                iVar.b(intent.getLongExtra("network_traffic", 0));
                this.f.add(iVar);
            }
        } else if (intent.getAction().equals("com.igexin.sdk.action.snlretire") && com.igexin.push.config.m.n) {
            String stringExtra6 = intent.getStringExtra("groupid");
            String stringExtra7 = intent.getStringExtra("branch");
            if (g.d.equals(stringExtra6) && "open".equals(stringExtra7)) {
                a aVar = new a();
                aVar.a(com.igexin.push.core.c.retire);
                com.igexin.push.core.f.a().h().a(aVar);
            }
        }
    }

    public void a(a aVar) {
        com.igexin.b.a.c.a.b("SNLCoordinator|state = " + this.b + ", doEvent = " + aVar.f1005a);
        switch (h.f1012a[this.b.ordinal()]) {
            case 1:
                b(aVar);
                return;
            case 2:
                c(aVar);
                return;
            case 3:
                d(aVar);
                return;
            case 4:
                e(aVar);
                return;
            default:
                return;
        }
    }

    public boolean a() {
        if (this.b != d.active) {
            return false;
        }
        for (Map.Entry value : this.g.entrySet()) {
            try {
                ((b) value.getValue()).c().onASNLNetworkConnected();
            } catch (Exception e2) {
            }
        }
        return true;
    }

    public int b(String str, e eVar) {
        b bVar = new b();
        bVar.e = eVar.d();
        bVar.f989a = bVar.e.length;
        bVar.b = (byte) eVar.i;
        byte[] a2 = bVar.a();
        b bVar2 = this.h.get(str);
        if (bVar2 != null) {
            try {
                if (str.startsWith("S-")) {
                    this.h.put("C-" + com.igexin.b.b.a.a(str.substring(2, str.length())), bVar2);
                }
                return bVar2.c().receiveToPSNL(bVar2.e(), str, a2);
            } catch (RemoteException e2) {
                this.g.remove(bVar2.e());
                this.h.remove(str);
            }
        }
        return -1;
    }

    public int b(String str, String str2, byte[] bArr) {
        com.igexin.push.core.f.a().g().a(a(bArr));
        return 0;
    }

    public boolean b() {
        if (this.b != d.active) {
            return false;
        }
        for (Map.Entry value : this.g.entrySet()) {
            try {
                ((b) value.getValue()).c().onASNLNetworkDisconnected();
            } catch (Exception e2) {
            }
        }
        return true;
    }
}
