package com.igexin.sdk.message;

public class GTCmdMessage extends BaseMessage {

    /* renamed from: a  reason: collision with root package name */
    private int f1053a;

    public GTCmdMessage() {
    }

    public GTCmdMessage(int i) {
        this.f1053a = i;
    }

    public int getAction() {
        return this.f1053a;
    }

    public void setAction(int i) {
        this.f1053a = i;
    }
}
