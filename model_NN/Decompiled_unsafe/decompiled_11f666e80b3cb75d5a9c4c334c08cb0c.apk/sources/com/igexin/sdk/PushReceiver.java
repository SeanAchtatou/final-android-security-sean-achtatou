package com.igexin.sdk;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.igexin.b.a.c.a;
import com.igexin.push.core.a.e;
import com.igexin.sdk.a.d;

public class PushReceiver extends BroadcastReceiver {

    /* renamed from: a  reason: collision with root package name */
    private static final String f1040a = PushReceiver.class.getName();

    public void onReceive(Context context, Intent intent) {
        if (intent != null && intent.getAction() != null) {
            try {
                String d = e.a().d("ss");
                if (d == null || !d.equals("1") || new d(context).c()) {
                    String action = intent.getAction();
                    if (PushConsts.ACTION_BROADCAST_PUSHMANAGER.equals(action)) {
                        if (intent.getExtras() != null) {
                            Intent intent2 = new Intent(context.getApplicationContext(), e.a().a(context));
                            intent2.putExtra("action", PushConsts.ACTION_BROADCAST_PUSHMANAGER);
                            intent2.putExtra("bundle", intent.getExtras());
                            context.getApplicationContext().startService(intent2);
                        }
                    } else if (PushConsts.ACTION_BROADCAST_REFRESHLS.equals(action)) {
                        String stringExtra = intent.getStringExtra("callback_pkgname");
                        String stringExtra2 = intent.getStringExtra("callback_classname");
                        Intent intent3 = new Intent(context.getApplicationContext(), e.a().a(context));
                        intent3.putExtra("action", PushConsts.ACTION_BROADCAST_REFRESHLS);
                        intent3.putExtra("callback_pkgname", stringExtra);
                        intent3.putExtra("callback_classname", stringExtra2);
                        context.startService(intent3);
                    } else if (PushConsts.ACTION_BROADCAST_TO_BOOT.equals(action) || action.equals("android.intent.action.ACTION_POWER_CONNECTED") || action.equals("android.intent.action.ACTION_POWER_DISCONNECTED") || action.equals("android.intent.action.MEDIA_MOUNTED")) {
                        context.startService(new Intent(context.getApplicationContext(), e.a().a(context)));
                    } else if (PushConsts.ACTION_BROADCAST_NETWORK_CHANGE.equals(action) || PushConsts.ACTION_BROADCAST_USER_PRESENT.equals(action)) {
                        Intent intent4 = new Intent(context.getApplicationContext(), e.a().a(context));
                        intent4.putExtra("action", action);
                        context.startService(intent4);
                    }
                }
            } catch (Throwable th) {
                a.b(f1040a + "|" + th.toString());
            }
        }
    }
}
