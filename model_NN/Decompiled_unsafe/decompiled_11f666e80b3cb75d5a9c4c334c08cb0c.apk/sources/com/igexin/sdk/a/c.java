package com.igexin.sdk.a;

import android.content.Context;
import com.igexin.b.a.c.a;
import java.io.File;
import java.io.IOException;

public class c {

    /* renamed from: a  reason: collision with root package name */
    private String f1046a;

    public c(Context context) {
        if (context != null) {
            this.f1046a = context.getFilesDir().getPath() + "/" + "push.pid";
        }
    }

    public void a() {
        if (!c() && this.f1046a != null) {
            try {
                new File(this.f1046a).createNewFile();
            } catch (IOException e) {
                a.b("SdkPushSwitch | switchOn, create file = " + this.f1046a + " exception, " + e.toString());
            }
        }
    }

    public void b() {
        if (c() && this.f1046a != null && !new File(this.f1046a).delete()) {
            a.b("SdkPushSwitch | switchOff, delete file = " + this.f1046a + " failed !!!!!!!!!!!!");
        }
    }

    public boolean c() {
        if (this.f1046a != null) {
            return new File(this.f1046a).exists();
        }
        return false;
    }
}
