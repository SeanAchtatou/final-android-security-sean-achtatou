package com.igexin.sdk.a;

import android.content.Context;
import java.io.File;
import java.io.IOException;

public class b {

    /* renamed from: a  reason: collision with root package name */
    private String f1045a;

    public b(Context context) {
        if (context != null) {
            this.f1045a = context.getFilesDir().getPath() + "/" + "init.pid";
        }
    }

    public void a() {
        if (!b() && this.f1045a != null) {
            try {
                new File(this.f1045a).createNewFile();
            } catch (IOException e) {
            }
        }
    }

    public boolean b() {
        if (this.f1045a != null) {
            return new File(this.f1045a).exists();
        }
        return false;
    }
}
