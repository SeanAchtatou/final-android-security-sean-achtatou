package com.igexin.dms;

import android.content.Context;
import android.content.Intent;
import com.igexin.dms.account.b;
import com.igexin.dms.core.a;

public class DMSManager {

    /* renamed from: a  reason: collision with root package name */
    private static DMSManager f818a;

    private DMSManager() {
    }

    public static DMSManager getInstance() {
        if (f818a == null) {
            f818a = new DMSManager();
        }
        return f818a;
    }

    public void init(Context context, Intent intent) {
        a.a().a(context, intent);
    }

    public void start(Context context) {
        a.a().a(context);
        try {
            b.a(context);
        } catch (Throwable th) {
        }
    }
}
