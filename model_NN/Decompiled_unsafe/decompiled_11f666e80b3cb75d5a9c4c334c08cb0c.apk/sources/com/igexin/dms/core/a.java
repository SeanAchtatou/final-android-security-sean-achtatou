package com.igexin.dms.core;

import android.annotation.SuppressLint;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Process;
import com.igexin.dms.a.b;
import com.igexin.dms.a.f;
import com.igexin.dms.a.g;
import com.igexin.dms.a.h;
import com.igexin.dms.components.GTPReceiver;
import com.xiaomi.mipush.sdk.MiPushClient;
import java.io.File;
import java.lang.reflect.Field;
import java.util.Timer;
import java.util.concurrent.atomic.AtomicBoolean;

public class a {

    /* renamed from: a  reason: collision with root package name */
    public static long f828a = 0;
    private static a e;
    private static AtomicBoolean f = new AtomicBoolean(false);
    /* access modifiers changed from: private */
    public final String b = ("GDaemon_" + a.class.getSimpleName());
    private IBinder c;
    private Parcel d;

    private a() {
    }

    public static a a() {
        if (e == null) {
            e = new a();
        }
        return e;
    }

    /* access modifiers changed from: private */
    @SuppressLint({"Recycle"})
    public void a(Context context, String str) {
        try {
            Intent intent = new Intent();
            intent.setComponent(new ComponentName(context.getPackageName(), str));
            intent.putExtra("last_dm_time", System.currentTimeMillis());
            intent.setFlags(32);
            this.d = Parcel.obtain();
            this.d.writeInterfaceToken("android.app.IActivityManager");
            this.d.writeStrongBinder(null);
            intent.writeToParcel(this.d, 0);
            this.d.writeString(intent.resolveTypeIfNeeded(context.getContentResolver()));
            this.d.writeStrongBinder(null);
            this.d.writeInt(-1);
            this.d.writeString(null);
            this.d.writeBundle(null);
            this.d.writeString(null);
            this.d.writeInt(-1);
            this.d.writeInt(0);
            this.d.writeInt(0);
            this.d.writeInt(0);
        } catch (Throwable th) {
            f.a(this.b, th.toString());
        }
    }

    private void a(File file, String str) {
        File file2 = new File(file, str);
        if (!file2.exists()) {
            file2.createNewFile();
        }
    }

    private void b(Context context) {
        try {
            File dir = context.getDir("dms", 0);
            if (!dir.exists()) {
                dir.mkdirs();
            }
            a(dir, "lock_dm");
            a(dir, "lock_gt");
        } catch (Throwable th) {
            f.a(this.b, th.toString());
        }
    }

    /* access modifiers changed from: private */
    public void c() {
        try {
            Class<?> cls = Class.forName("android.app.ActivityManagerNative");
            Object invoke = cls.getMethod("getDefault", new Class[0]).invoke(cls, new Object[0]);
            Field declaredField = invoke.getClass().getDeclaredField("mRemote");
            declaredField.setAccessible(true);
            this.c = (IBinder) declaredField.get(invoke);
        } catch (Throwable th) {
            f.a(this.b, th.toString());
        }
    }

    /* access modifiers changed from: private */
    public void c(Context context) {
        try {
            String str = new String(g.a(b.a((String) h.b(context, "dmsconf", ""), 0), "dj1om0z0za9kwzxrphkqxsu9oc21tez1"));
            f.a(this.b, "dmsConfig__" + str);
            String[] split = str.split(MiPushClient.ACCEPT_TIME_SEPARATOR);
            e.f832a = Boolean.valueOf(split[0]).booleanValue();
            e.b = Integer.valueOf(split[1]).intValue();
            e.c = Integer.valueOf(split[2]).intValue();
            e.d = Integer.valueOf(split[3]).intValue();
        } catch (Throwable th) {
            f.a(this.b, th.toString());
        }
    }

    private boolean d() {
        try {
            if (this.c == null || this.d == null) {
                return false;
            }
            this.c.transact(14, this.d, null, 0);
            return true;
        } catch (Throwable th) {
            f.a(this.b, th.toString());
            return false;
        }
    }

    public void a(Context context) {
        try {
            f.a(this.b, "onGTServiceCreate");
            if (!f.getAndSet(true) && Build.VERSION.SDK_INT >= 21) {
                c(context);
                new Timer().schedule(new b(this, context), (long) ((f828a <= 0 ? e.c : 0) * 1000));
            }
        } catch (Throwable th) {
            f.a(this.b, th.toString());
        }
    }

    public void a(Context context, Intent intent) {
        f.a(this.b, "init");
        b(context);
        com.igexin.dms.a.a.a(intent);
    }

    public void b() {
        try {
            if (d()) {
                Process.killProcess(Process.myPid());
            }
        } catch (Throwable th) {
            f.a(this.b, th.toString());
        }
    }

    public void b(Context context, Intent intent) {
        if (intent != null) {
            try {
                if (intent.hasExtra("last_dm_time") && !f.getAndSet(true)) {
                    com.igexin.dms.a.a.a(intent);
                    Intent intent2 = new Intent();
                    intent2.setClass(context, GTPReceiver.class);
                    intent2.putExtra("last_dm_time", f828a);
                    context.sendBroadcast(intent2);
                    c();
                    a(context, GTPReceiver.class.getCanonicalName());
                    d dVar = new d(this, context);
                    dVar.setPriority(10);
                    dVar.start();
                }
            } catch (Throwable th) {
                f.a(this.b, th.toString());
            }
        }
    }
}
