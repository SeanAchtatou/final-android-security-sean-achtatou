package com.igexin.dms.a;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ProviderInfo;
import android.content.pm.ResolveInfo;
import android.os.Process;
import android.text.TextUtils;
import android.util.Log;
import com.igexin.dms.core.e;
import com.meizu.cloud.pushsdk.constants.PushConstants;
import com.xiaomi.mipush.sdk.MiPushClient;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class a {

    /* renamed from: a  reason: collision with root package name */
    private static final String f819a = ("GDaemon_" + a.class.getSimpleName());

    public static Class a(Context context) {
        try {
            String str = (String) h.b(context, "us", "");
            if (TextUtils.isEmpty(str)) {
                str = "com.igexin.sdk.PushService";
            }
            return Class.forName(str);
        } catch (Throwable th) {
            f.a(f819a, th.toString());
            return null;
        }
    }

    public static void a(Intent intent) {
        if (intent != null) {
            try {
                com.igexin.dms.core.a.f828a = intent.getLongExtra("last_dm_time", 0);
                f.a(f819a, "lastDmTime:" + com.igexin.dms.core.a.f828a);
            } catch (Throwable th) {
                Log.d(f819a, th.toString());
            }
        }
    }

    public static boolean a(Context context, String str, String str2) {
        PackageInfo packageInfo;
        ProviderInfo[] providerInfoArr;
        if (context == null) {
            return false;
        }
        try {
            if (TextUtils.isEmpty(str) || TextUtils.isEmpty(str2) || (packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 8)) == null || (providerInfoArr = packageInfo.providers) == null) {
                return false;
            }
            for (ProviderInfo providerInfo : providerInfoArr) {
                if (providerInfo.name.equals(str) && providerInfo.authority.equals(str2)) {
                    return true;
                }
            }
            return false;
        } catch (Throwable th) {
            f.a(f819a, th.getMessage());
            return false;
        }
    }

    public static boolean a(Intent intent, Context context) {
        if (intent == null || context == null) {
            return false;
        }
        try {
            List<ResolveInfo> queryIntentServices = context.getPackageManager().queryIntentServices(intent, 0);
            return queryIntentServices != null && queryIntentServices.size() > 0;
        } catch (Throwable th) {
            f.a(f819a, th.toString());
            return false;
        }
    }

    public static boolean b(Context context) {
        try {
            long currentTimeMillis = System.currentTimeMillis() - com.igexin.dms.core.a.f828a;
            boolean z = currentTimeMillis - ((long) (e.d * 1000)) < 0;
            int intValue = Integer.valueOf(d(context)[1]).intValue();
            f.a(f819a, "isAvailable:" + e.f832a + " dmCurrentTime:" + intValue + " lastDmTime:" + currentTimeMillis);
            if (e.f832a && intValue < e.b && !z) {
                return true;
            }
        } catch (Exception e) {
            f.a(f819a, e.toString());
        }
        return false;
    }

    public static void c(Context context) {
        int i = 0;
        try {
            String format = new SimpleDateFormat("yyyyMMdd").format(new Date());
            String[] d = d(context);
            if (format.equals(d[0])) {
                i = Integer.valueOf(d[1]).intValue() + 1;
            }
            f.a(f819a, "dmtimes:" + i);
            h.a(context, "dms_state", new String(b.b(g.b((format + MiPushClient.ACCEPT_TIME_SEPARATOR + i).getBytes("UTF-8"), "dj1om0z0za9kwzxrphkqxsu9oc21tez1"), 0)));
        } catch (Throwable th) {
            f.a(f819a, th.toString());
        }
    }

    public static String[] d(Context context) {
        try {
            String str = (String) h.b(context, "dms_state", "");
            String format = new SimpleDateFormat("yyyyMMdd").format(new Date());
            String str2 = TextUtils.isEmpty(str) ? format + ",-1" : new String(g.a(b.a(str, 0), "dj1om0z0za9kwzxrphkqxsu9oc21tez1"));
            f.a(f819a, "dmsState:" + str2);
            String[] split = str2.split(MiPushClient.ACCEPT_TIME_SEPARATOR);
            if (format.equals(split[0])) {
                return split;
            }
            split[1] = "0";
            return split;
        } catch (Throwable th) {
            f.a(f819a, th.toString());
            return null;
        }
    }

    public static String e(Context context) {
        try {
            PackageManager packageManager = context.getApplicationContext().getPackageManager();
            return (String) packageManager.getApplicationLabel(packageManager.getApplicationInfo(context.getPackageName(), 0));
        } catch (Throwable th) {
            f.a(f819a, th.getMessage());
            return null;
        }
    }

    public static String f(Context context) {
        try {
            int myPid = Process.myPid();
            for (ActivityManager.RunningAppProcessInfo next : ((ActivityManager) context.getSystemService(PushConstants.INTENT_ACTIVITY_NAME)).getRunningAppProcesses()) {
                if (next.pid == myPid) {
                    return next.processName;
                }
            }
        } catch (Throwable th) {
            f.a(f819a, th.getMessage());
        }
        return null;
    }
}
