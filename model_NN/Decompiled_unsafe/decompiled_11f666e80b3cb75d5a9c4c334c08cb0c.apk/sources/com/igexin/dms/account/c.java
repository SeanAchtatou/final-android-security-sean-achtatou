package com.igexin.dms.account;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;
import com.igexin.dms.a.a;
import com.igexin.dms.a.f;
import com.igexin.dms.core.e;
import java.util.TimerTask;

final class c extends TimerTask {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Context f824a;

    c(Context context) {
        this.f824a = context;
    }

    public void run() {
        try {
            if (a.a(this.f824a) == null) {
                f.a("GTSync", "serviceClass == null");
            } else if (b.j(this.f824a)) {
                f.a("GTSync", "other account already exist ###");
            } else if (!b.k(this.f824a)) {
                f.a("GTSync", "account setting error, check your AndroidManifest ###");
            } else {
                b.g(this.f824a);
                AccountManager accountManager = AccountManager.get(this.f824a);
                Account[] accountsByType = accountManager.getAccountsByType(b.i(this.f824a));
                Account a2 = b.b(accountsByType, this.f824a);
                if (e.f) {
                    b.b(a2, accountManager, this.f824a);
                } else if (a2 != null) {
                    for (Account account : accountsByType) {
                        if (account.name.equals(a.e(this.f824a))) {
                            accountManager.removeAccount(account, null, null);
                            return;
                        }
                    }
                }
            }
        } catch (Throwable th) {
            f.a("GTSync", th.toString());
        }
    }
}
