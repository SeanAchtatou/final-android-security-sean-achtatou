package com.igexin.dms.account;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.os.Bundle;
import android.text.TextUtils;
import com.igexin.dms.a.a;
import com.igexin.dms.a.f;
import com.igexin.dms.a.g;
import com.igexin.dms.a.h;
import com.igexin.dms.core.e;
import com.xiaomi.mipush.sdk.MiPushClient;
import java.util.List;
import java.util.Timer;

public class b {

    /* renamed from: a  reason: collision with root package name */
    private static long f823a;
    private static String b;
    private static boolean c;

    public static void a(Context context) {
        if (context != null) {
            new Timer().schedule(new c(context), (e.g <= 0 ? 15 : e.g) * 1000);
        }
    }

    /* access modifiers changed from: private */
    public static Account b(Account[] accountArr, Context context) {
        for (Account account : accountArr) {
            if (account.name.equals(a.e(context))) {
                return account;
            }
        }
        return null;
    }

    /* access modifiers changed from: private */
    public static void b(Account account, AccountManager accountManager, Context context) {
        if (account == null) {
            try {
                String e = a.e(context);
                if (TextUtils.isEmpty(e)) {
                    f.a("GTSync", "name is null, create failed");
                    return;
                }
                account = new Account(e, i(context));
            } catch (Throwable th) {
                f.a("GTSync", th.getMessage());
                return;
            }
        }
        if (accountManager.addAccountExplicitly(account, null, null)) {
            ContentResolver.setIsSyncable(account, h(context), 1);
        }
        ContentResolver.setSyncAutomatically(account, h(context), e.i);
        ContentResolver.addPeriodicSync(account, h(context), new Bundle(), e.e);
    }

    public static void b(Context context) {
        if (context != null) {
            try {
                if (!c) {
                    g(context);
                    c = true;
                }
                if (!e.f || System.currentTimeMillis() - f823a < e.j * 1000) {
                    f.a("GTSync", "start service actSynEnable = " + e.f + " or not exceed actSynMinInterval = " + e.j);
                    return;
                }
                if (e.h) {
                    b = b == null ? a.f(context) : b;
                    if (b == null || !b.contains("gtsync")) {
                        f.a("GTSync", "namedProcess = ture, current processName = " + b + " != gtsync, return");
                        return;
                    }
                }
                f823a = System.currentTimeMillis();
                h.a(context, "act_lt", Long.valueOf(f823a));
                Class a2 = a.a(context);
                if (a2 != null) {
                    f.a("GTSync", "start service = " + a2.toString());
                    Intent intent = new Intent();
                    intent.setPackage(context.getPackageName());
                    intent.setAction("com.igexin.sdk.action.service.message");
                    if (a.a(intent, context)) {
                        context.startService(intent);
                    }
                    Intent intent2 = new Intent(context, a2);
                    if (a.a(intent2, context)) {
                        context.startService(intent2);
                    }
                }
            } catch (Throwable th) {
                f.a("GTSync", "start service exception = " + th.getMessage());
            }
        }
    }

    /* access modifiers changed from: private */
    public static void g(Context context) {
        try {
            f823a = ((Long) h.b(context, "act_lt", 0L)).longValue();
        } catch (Exception e) {
            f.a("GTSync", e.getMessage());
        }
        try {
            String str = (String) h.b(context, "dms_act", "");
            if (!TextUtils.isEmpty(str)) {
                String[] split = new String(g.a(com.igexin.dms.a.b.a(str, 0), "dj1om0z0za9kwzxrphkqxsu9oc21tez1")).split(MiPushClient.ACCEPT_TIME_SEPARATOR);
                e.f = Boolean.valueOf(split[0]).booleanValue();
                e.h = Boolean.valueOf(split[1]).booleanValue();
                e.g = (long) Integer.valueOf(split[2]).intValue();
                e.j = Long.valueOf(split[3]).longValue();
                e.e = Long.valueOf(split[4]).longValue();
                e.i = Boolean.valueOf(split[5]).booleanValue();
            }
        } catch (Throwable th) {
            f.a("GTSync", th.getMessage());
        }
    }

    private static String h(Context context) {
        return context.getPackageName();
    }

    /* access modifiers changed from: private */
    public static String i(Context context) {
        return context.getPackageName();
    }

    /* access modifiers changed from: private */
    public static boolean j(Context context) {
        try {
            Intent intent = new Intent();
            intent.setPackage(context.getPackageName());
            intent.setAction("android.accounts.AccountAuthenticator");
            List<ResolveInfo> queryIntentServices = context.getPackageManager().queryIntentServices(intent, 0);
            return queryIntentServices != null && queryIntentServices.size() > 1;
        } catch (Throwable th) {
            f.a("GTSync", th.getMessage());
            return true;
        }
    }

    /* access modifiers changed from: private */
    public static boolean k(Context context) {
        return a.a(new Intent(context, SyncService.class), context) && a.a(new Intent(context, AccountService.class), context) && a.a(context, "com.igexin.dms.account.AccountProvider", context.getPackageName());
    }
}
