package com.igexin.dms.account;

import android.accounts.Account;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ContentProviderClient;
import android.content.Context;
import android.content.SyncResult;
import android.os.Bundle;
import com.igexin.dms.a.f;

class d extends AbstractThreadedSyncAdapter {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SyncService f825a;
    private Context b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public d(SyncService syncService, Context context, boolean z) {
        super(context, z);
        this.f825a = syncService;
        this.b = context;
    }

    public void onPerformSync(Account account, Bundle bundle, String str, ContentProviderClient contentProviderClient, SyncResult syncResult) {
        f.a("GTSync", "onPerformSync");
        b.b(this.b);
    }
}
