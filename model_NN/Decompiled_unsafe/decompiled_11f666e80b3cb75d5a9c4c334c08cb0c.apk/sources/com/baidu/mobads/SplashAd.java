package com.baidu.mobads;

import android.content.Context;
import android.text.TextUtils;
import android.view.ViewGroup;
import com.baidu.mobads.j.m;
import com.baidu.mobads.openad.interfaces.event.IOAdEventListener;
import com.baidu.mobads.production.h.a;

public class SplashAd {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public a f438a;
    private volatile String b;
    /* access modifiers changed from: private */
    public SplashAdListener c;
    /* access modifiers changed from: private */
    public IOAdEventListener d;

    public SplashAd(Context context, ViewGroup viewGroup, SplashAdListener splashAdListener, String str) {
        this(context, viewGroup, splashAdListener, str, true);
    }

    public SplashAd(Context context, ViewGroup viewGroup, SplashAdListener splashAdListener, String str, boolean z) {
        this.b = "init";
        this.c = new ag(this);
        this.d = new ah(this);
        if (splashAdListener != null) {
            try {
                this.c = splashAdListener;
            } catch (Exception e) {
                m.a().f().d(e);
                com.baidu.mobads.c.a.a().a("splash ad create failed: " + e.toString());
                return;
            }
        }
        if (TextUtils.isEmpty(str)) {
            this.c.onAdFailed("请您输入正确的广告位ID");
            return;
        }
        ao aoVar = new ao(context);
        aoVar.a(new aj(this, context, aoVar, str, z));
        aoVar.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        viewGroup.addView(aoVar);
    }

    public static void setAppSid(Context context, String str) {
        m.a().m().setAppId(str);
    }

    @Deprecated
    public static void setAppSec(Context context, String str) {
    }

    public void destroy() {
        if (this.f438a != null) {
            this.f438a.l();
        }
    }
}
