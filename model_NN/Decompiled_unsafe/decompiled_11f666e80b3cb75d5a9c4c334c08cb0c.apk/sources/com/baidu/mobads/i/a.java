package com.baidu.mobads.i;

import android.content.Context;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.telephony.CellIdentityCdma;
import android.telephony.CellIdentityGsm;
import android.telephony.CellIdentityLte;
import android.telephony.CellIdentityWcdma;
import android.telephony.CellInfo;
import android.telephony.CellInfoCdma;
import android.telephony.CellInfoGsm;
import android.telephony.CellInfoLte;
import android.telephony.CellInfoWcdma;
import android.telephony.CellLocation;
import android.telephony.TelephonyManager;
import android.telephony.cdma.CdmaCellLocation;
import android.telephony.gsm.GsmCellLocation;
import com.baidu.mobads.interfaces.IXAdRequestInfo;
import com.baidu.mobads.interfaces.utils.IXAdSystemUtils;
import com.baidu.mobads.j.m;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import org.apache.mina.proxy.handlers.socks.SocksProxyConstants;

public class a {
    private static Method d = null;
    private static Method e = null;
    private static Method f = null;
    private static Class<?> g = null;
    private static char[] n = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_.".toCharArray();

    /* renamed from: a  reason: collision with root package name */
    private Context f504a = null;
    private TelephonyManager b = null;
    private C0013a c = new C0013a();
    private WifiManager h = null;
    private b i = null;
    private long j = 0;
    /* access modifiers changed from: private */
    public String k = null;
    /* access modifiers changed from: private */
    public int l = 0;
    private String m = null;

    public a(Context context) {
        String str;
        this.f504a = context.getApplicationContext();
        String packageName = this.f504a.getPackageName();
        try {
            this.b = (TelephonyManager) this.f504a.getSystemService("phone");
            str = (String) m.a().m().a(this.b, m.a().e().decodeStr("uvNYwANvpyP-iyfb"), new Object[0]);
        } catch (Exception e2) {
            str = null;
        }
        this.m = "&" + packageName + "&" + str;
        this.h = (WifiManager) this.f504a.getSystemService(IXAdSystemUtils.NT_WIFI);
    }

    public String a() {
        try {
            return a(10);
        } catch (Exception e2) {
            return null;
        }
    }

    private String a(int i2) {
        String str;
        String str2;
        if (i2 < 3) {
            i2 = 3;
        }
        try {
            C0013a b2 = b();
            if (b2 == null || !b2.b()) {
                a(this.b.getCellLocation());
            } else {
                this.c = b2;
            }
            str = this.c.a();
        } catch (Exception e2) {
            str = null;
        }
        if (str == null) {
            str = "Z";
        }
        try {
            if (this.i == null || this.i.c()) {
                this.i = new b(this.h.getScanResults());
            }
            str2 = this.i.a(i2);
        } catch (Exception e3) {
            str2 = null;
        }
        if (str2 != null) {
            str = str + str2;
        }
        if (str.equals("Z")) {
            return null;
        }
        return a(str + "t" + System.currentTimeMillis() + this.m);
    }

    private void a(CellLocation cellLocation) {
        int i2 = 0;
        if (cellLocation != null && this.b != null) {
            C0013a aVar = new C0013a();
            String networkOperator = this.b.getNetworkOperator();
            if (networkOperator != null && networkOperator.length() > 0) {
                try {
                    if (networkOperator.length() >= 3) {
                        int intValue = Integer.valueOf(networkOperator.substring(0, 3)).intValue();
                        if (intValue < 0) {
                            intValue = this.c.c;
                        }
                        aVar.c = intValue;
                    }
                    String substring = networkOperator.substring(3);
                    if (substring != null) {
                        char[] charArray = substring.toCharArray();
                        while (i2 < charArray.length && Character.isDigit(charArray[i2])) {
                            i2++;
                        }
                    }
                    int intValue2 = Integer.valueOf(substring.substring(0, i2)).intValue();
                    if (intValue2 < 0) {
                        intValue2 = this.c.d;
                    }
                    aVar.d = intValue2;
                } catch (Exception e2) {
                }
            }
            if (cellLocation instanceof GsmCellLocation) {
                aVar.f505a = ((GsmCellLocation) cellLocation).getLac();
                aVar.b = ((GsmCellLocation) cellLocation).getCid();
                aVar.e = 'g';
            } else if (cellLocation instanceof CdmaCellLocation) {
                aVar.e = 'w';
                if (g == null) {
                    try {
                        g = Class.forName("android.telephony.cdma.CdmaCellLocation");
                        d = g.getMethod("getBaseStationId", new Class[0]);
                        e = g.getMethod("getNetworkId", new Class[0]);
                        f = g.getMethod("getSystemId", new Class[0]);
                    } catch (Exception e3) {
                        g = null;
                        return;
                    }
                }
                if (g != null && g.isInstance(cellLocation)) {
                    try {
                        int intValue3 = ((Integer) f.invoke(cellLocation, new Object[0])).intValue();
                        if (intValue3 < 0) {
                            intValue3 = this.c.d;
                        }
                        aVar.d = intValue3;
                        aVar.b = ((Integer) d.invoke(cellLocation, new Object[0])).intValue();
                        aVar.f505a = ((Integer) e.invoke(cellLocation, new Object[0])).intValue();
                    } catch (Exception e4) {
                        return;
                    }
                }
            }
            if (aVar.b()) {
                this.c = aVar;
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:37:?, code lost:
        return r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:?, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:?, code lost:
        return null;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0046 A[ExcHandler: NoSuchMethodError (e java.lang.NoSuchMethodError), Splitter:B:2:0x0010] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private com.baidu.mobads.i.a.C0013a b() {
        /*
            r5 = this;
            r1 = 0
            int r0 = android.os.Build.VERSION.SDK_INT
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            int r0 = r0.intValue()
            r2 = 17
            if (r0 >= r2) goto L_0x0010
        L_0x000f:
            return r1
        L_0x0010:
            android.telephony.TelephonyManager r0 = r5.b     // Catch:{ Exception -> 0x0048, NoSuchMethodError -> 0x0046 }
            java.util.List r0 = r0.getAllCellInfo()     // Catch:{ Exception -> 0x0048, NoSuchMethodError -> 0x0046 }
            if (r0 == 0) goto L_0x000f
            int r2 = r0.size()     // Catch:{ Exception -> 0x0048, NoSuchMethodError -> 0x0046 }
            if (r2 <= 0) goto L_0x000f
            java.util.Iterator r3 = r0.iterator()     // Catch:{ Exception -> 0x0048, NoSuchMethodError -> 0x0046 }
            r2 = r1
        L_0x0023:
            boolean r0 = r3.hasNext()     // Catch:{ Exception -> 0x004a, NoSuchMethodError -> 0x0046 }
            if (r0 == 0) goto L_0x0050
            java.lang.Object r0 = r3.next()     // Catch:{ Exception -> 0x004a, NoSuchMethodError -> 0x0046 }
            android.telephony.CellInfo r0 = (android.telephony.CellInfo) r0     // Catch:{ Exception -> 0x004a, NoSuchMethodError -> 0x0046 }
            boolean r4 = r0.isRegistered()     // Catch:{ Exception -> 0x004a, NoSuchMethodError -> 0x0046 }
            if (r4 == 0) goto L_0x0023
            com.baidu.mobads.i.a$a r0 = r5.a(r0)     // Catch:{ Exception -> 0x004a, NoSuchMethodError -> 0x0046 }
            if (r0 != 0) goto L_0x003d
            r2 = r0
            goto L_0x0023
        L_0x003d:
            boolean r2 = r0.b()     // Catch:{ Exception -> 0x004d, NoSuchMethodError -> 0x0046 }
            if (r2 != 0) goto L_0x0044
            r0 = r1
        L_0x0044:
            r1 = r0
            goto L_0x000f
        L_0x0046:
            r0 = move-exception
            goto L_0x000f
        L_0x0048:
            r0 = move-exception
            goto L_0x000f
        L_0x004a:
            r0 = move-exception
            r1 = r2
            goto L_0x000f
        L_0x004d:
            r1 = move-exception
            r1 = r0
            goto L_0x000f
        L_0x0050:
            r1 = r2
            goto L_0x000f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.mobads.i.a.b():com.baidu.mobads.i.a$a");
    }

    private C0013a a(CellInfo cellInfo) {
        int intValue = Integer.valueOf(Build.VERSION.SDK_INT).intValue();
        if (intValue < 17) {
            return null;
        }
        C0013a aVar = new C0013a();
        boolean z = false;
        if (cellInfo instanceof CellInfoGsm) {
            CellIdentityGsm cellIdentity = ((CellInfoGsm) cellInfo).getCellIdentity();
            aVar.c = b(cellIdentity.getMcc());
            aVar.d = b(cellIdentity.getMnc());
            aVar.f505a = b(cellIdentity.getLac());
            aVar.b = b(cellIdentity.getCid());
            aVar.e = 'g';
            z = true;
        } else if (cellInfo instanceof CellInfoCdma) {
            CellIdentityCdma cellIdentity2 = ((CellInfoCdma) cellInfo).getCellIdentity();
            aVar.d = b(cellIdentity2.getSystemId());
            aVar.f505a = b(cellIdentity2.getNetworkId());
            aVar.b = b(cellIdentity2.getBasestationId());
            aVar.e = 'w';
            z = true;
        } else if (cellInfo instanceof CellInfoLte) {
            CellIdentityLte cellIdentity3 = ((CellInfoLte) cellInfo).getCellIdentity();
            aVar.c = b(cellIdentity3.getMcc());
            aVar.d = b(cellIdentity3.getMnc());
            aVar.f505a = b(cellIdentity3.getTac());
            aVar.b = b(cellIdentity3.getCi());
            aVar.e = 'g';
            z = true;
        }
        if (intValue >= 18 && !z) {
            try {
                if (cellInfo instanceof CellInfoWcdma) {
                    CellIdentityWcdma cellIdentity4 = ((CellInfoWcdma) cellInfo).getCellIdentity();
                    aVar.c = b(cellIdentity4.getMcc());
                    aVar.d = b(cellIdentity4.getMnc());
                    aVar.f505a = b(cellIdentity4.getLac());
                    aVar.b = b(cellIdentity4.getCid());
                    aVar.e = 'g';
                }
            } catch (Exception e2) {
            }
        }
        return aVar;
    }

    private int b(int i2) {
        if (i2 == Integer.MAX_VALUE) {
            return -1;
        }
        return i2;
    }

    /* renamed from: com.baidu.mobads.i.a$a  reason: collision with other inner class name */
    private class C0013a {

        /* renamed from: a  reason: collision with root package name */
        public int f505a;
        public int b;
        public int c;
        public int d;
        public char e;

        private C0013a() {
            this.f505a = -1;
            this.b = -1;
            this.c = -1;
            this.d = -1;
            this.e = 0;
        }

        /* access modifiers changed from: private */
        public boolean b() {
            return this.f505a > -1 && this.b > 0;
        }

        public String a() {
            if (!b()) {
                return null;
            }
            StringBuffer stringBuffer = new StringBuffer(128);
            stringBuffer.append(this.e);
            stringBuffer.append(IXAdRequestInfo.HEIGHT);
            if (this.c != 460) {
                stringBuffer.append(this.c);
            }
            stringBuffer.append(String.format(Locale.CHINA, "h%xh%xh%x", Integer.valueOf(this.d), Integer.valueOf(this.f505a), Integer.valueOf(this.b)));
            return stringBuffer.toString();
        }
    }

    protected class b {

        /* renamed from: a  reason: collision with root package name */
        public List<ScanResult> f506a = null;
        private long c = 0;

        public b(List<ScanResult> list) {
            this.f506a = list;
            this.c = System.currentTimeMillis();
            b();
        }

        public int a() {
            if (this.f506a == null) {
                return 0;
            }
            return this.f506a.size();
        }

        public String a(int i) {
            boolean z;
            boolean z2;
            String str;
            int i2;
            boolean z3;
            if (a() < 1) {
                return null;
            }
            boolean a2 = a.this.c();
            if (a2) {
                i--;
                z = false;
            } else {
                z = true;
            }
            StringBuffer stringBuffer = new StringBuffer(512);
            int size = this.f506a.size();
            int i3 = 0;
            int i4 = 0;
            boolean z4 = true;
            boolean z5 = z;
            while (true) {
                if (i3 >= size) {
                    z2 = z4;
                    break;
                }
                if (this.f506a.get(i3).level == 0) {
                    i2 = i4;
                    z2 = z4;
                    z3 = z5;
                } else {
                    String str2 = this.f506a.get(i3).BSSID;
                    int i5 = this.f506a.get(i3).level;
                    String replace = str2.replace(":", "");
                    if (a.this.k == null || !replace.equals(a.this.k)) {
                        if (i4 < i) {
                            stringBuffer.append(IXAdRequestInfo.HEIGHT);
                            stringBuffer.append(replace);
                            stringBuffer.append("m");
                            stringBuffer.append(StrictMath.abs(i5));
                            i2 = i4 + 1;
                            z2 = false;
                        } else {
                            i2 = i4;
                            z2 = z4;
                        }
                        if (i2 > i && z5) {
                            break;
                        }
                        z3 = z5;
                    } else {
                        int unused = a.this.l = StrictMath.abs(i5);
                        i2 = i4;
                        z2 = z4;
                        z3 = true;
                    }
                }
                i3++;
                z5 = z3;
                z4 = z2;
                i4 = i2;
            }
            if (a2) {
                str = IXAdRequestInfo.HEIGHT + a.this.k + "km" + a.this.l;
            } else {
                str = null;
            }
            if (!z2) {
                return str + stringBuffer.toString();
            }
            return str;
        }

        private void b() {
            boolean z;
            if (a() >= 1) {
                boolean z2 = true;
                for (int size = this.f506a.size() - 1; size >= 1 && z2; size--) {
                    int i = 0;
                    z2 = false;
                    while (i < size) {
                        if (this.f506a.get(i).level < this.f506a.get(i + 1).level) {
                            this.f506a.set(i + 1, this.f506a.get(i));
                            this.f506a.set(i, this.f506a.get(i + 1));
                            z = true;
                        } else {
                            z = z2;
                        }
                        i++;
                        z2 = z;
                    }
                }
            }
        }

        /* access modifiers changed from: private */
        public boolean c() {
            long currentTimeMillis = System.currentTimeMillis() - this.c;
            if (currentTimeMillis < 0 || currentTimeMillis > 500) {
                return true;
            }
            return false;
        }
    }

    /* access modifiers changed from: private */
    public boolean c() {
        String str = null;
        this.k = null;
        this.l = 0;
        WifiInfo connectionInfo = this.h.getConnectionInfo();
        if (connectionInfo == null) {
            return false;
        }
        try {
            String bssid = connectionInfo.getBSSID();
            if (bssid != null) {
                str = bssid.replace(":", "");
            }
            if (str.length() != 12) {
                return false;
            }
            this.k = new String(str);
            return true;
        } catch (Exception e2) {
            return false;
        }
    }

    private static String a(String str) {
        int i2 = 0;
        if (str == null) {
            return null;
        }
        byte[] bytes = str.getBytes();
        byte nextInt = (byte) new Random().nextInt(255);
        byte nextInt2 = (byte) new Random().nextInt(255);
        byte[] bArr = new byte[(bytes.length + 2)];
        int length = bytes.length;
        int i3 = 0;
        while (i2 < length) {
            bArr[i3] = (byte) (bytes[i2] ^ nextInt);
            i2++;
            i3++;
        }
        int i4 = i3 + 1;
        bArr[i3] = nextInt;
        int i5 = i4 + 1;
        bArr[i4] = nextInt2;
        return a(bArr);
    }

    private static String a(byte[] bArr) {
        boolean z;
        boolean z2;
        int i2;
        char[] cArr = new char[(((bArr.length + 2) / 3) * 4)];
        int i3 = 0;
        int i4 = 0;
        while (i4 < bArr.length) {
            int i5 = (bArr[i4] & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD) << 8;
            if (i4 + 1 < bArr.length) {
                i5 |= bArr[i4 + 1] & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD;
                z = true;
            } else {
                z = false;
            }
            int i6 = i5 << 8;
            if (i4 + 2 < bArr.length) {
                i6 |= bArr[i4 + 2] & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD;
                z2 = true;
            } else {
                z2 = false;
            }
            cArr[i3 + 3] = n[z2 ? 63 - (i6 & 63) : 64];
            int i7 = i6 >> 6;
            int i8 = i3 + 2;
            char[] cArr2 = n;
            if (z) {
                i2 = 63 - (i7 & 63);
            } else {
                i2 = 64;
            }
            cArr[i8] = cArr2[i2];
            int i9 = i7 >> 6;
            cArr[i3 + 1] = n[63 - (i9 & 63)];
            cArr[i3 + 0] = n[63 - ((i9 >> 6) & 63)];
            i4 += 3;
            i3 += 4;
        }
        return new String(cArr);
    }
}
