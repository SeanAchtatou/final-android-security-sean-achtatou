package com.baidu.mobads;

import android.view.ViewTreeObserver;

class r implements ViewTreeObserver.OnPreDrawListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppActivity f595a;

    r(AppActivity appActivity) {
        this.f595a = appActivity;
    }

    public boolean onPreDraw() {
        this.f595a.mBottomView.getViewTreeObserver().removeOnPreDrawListener(this);
        this.f595a.runBottomViewEnterAnimation(this.f595a.C, this.f595a.mBottomView);
        return true;
    }
}
