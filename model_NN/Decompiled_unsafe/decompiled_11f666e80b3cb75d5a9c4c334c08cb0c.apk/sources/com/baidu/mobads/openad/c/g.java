package com.baidu.mobads.openad.c;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.baidu.mobads.j.m;
import java.util.ArrayList;
import java.util.List;

public class g {

    /* renamed from: a  reason: collision with root package name */
    private i f548a;
    private String b;

    public g(Context context) {
        this.f548a = new i(context);
        this.b = m.a().n().getCurrentProcessName(context);
    }

    public boolean a(String str, String str2) {
        Cursor rawQuery = this.f548a.getReadableDatabase().rawQuery("select count(*)  from download_info where url=? and local_file=? and process_name=?", new String[]{str, str2, this.b});
        rawQuery.moveToFirst();
        int i = rawQuery.getInt(0);
        rawQuery.close();
        if (i > 0) {
            return true;
        }
        return false;
    }

    public void a(List<h> list) {
        SQLiteDatabase writableDatabase = this.f548a.getWritableDatabase();
        for (h next : list) {
            try {
                writableDatabase.execSQL("insert into download_info(thread_id,url,local_file,start_pos,end_pos,compelete_size,process_name) values (?,?,?,?,?,?,?)", new Object[]{Integer.valueOf(next.c()), next.b(), next.f(), Integer.valueOf(next.d()), Integer.valueOf(next.e()), Integer.valueOf(next.a()), this.b});
            } catch (Exception e) {
                m.a().f().e("OAdSqlLiteAccessObj", e.getMessage());
            }
        }
    }

    public List<h> b(String str, String str2) {
        ArrayList arrayList = new ArrayList();
        Cursor rawQuery = this.f548a.getReadableDatabase().rawQuery("select thread_id, url, local_file, start_pos, end_pos,compelete_size from download_info where url=? and local_file=? and process_name=?", new String[]{str, str2, this.b});
        while (rawQuery.moveToNext()) {
            arrayList.add(new h(rawQuery.getInt(0), rawQuery.getString(1), rawQuery.getString(2), rawQuery.getInt(3), rawQuery.getInt(4), rawQuery.getInt(5)));
        }
        rawQuery.close();
        return arrayList;
    }

    public void b(List<h> list) {
        SQLiteDatabase readableDatabase = this.f548a.getReadableDatabase();
        for (h next : list) {
            try {
                readableDatabase.execSQL("update download_info set compelete_size=? where thread_id=? and url=? and local_file=? and process_name=?", new Object[]{Integer.valueOf(next.a()), Integer.valueOf(next.c()), next.b(), next.f(), this.b});
            } catch (Exception e) {
                m.a().f().e("OAdSqlLiteAccessObj", e.getMessage());
            }
        }
    }
}
