package com.baidu.mobads.openad.f;

import java.util.TimerTask;

class b extends TimerTask {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ a f558a;

    b(a aVar) {
        this.f558a = aVar;
    }

    public void run() {
        if (this.f558a.h.get() == 0) {
            if (this.f558a.b != null) {
                int unused = this.f558a.f = this.f558a.d - this.f558a.e;
                this.f558a.b.onTimer(this.f558a.f);
            }
            if (this.f558a.e > 0) {
                a.f(this.f558a);
                return;
            }
            this.f558a.stop();
            if (this.f558a.b != null) {
                this.f558a.b.onTimerComplete();
            }
        }
    }
}
