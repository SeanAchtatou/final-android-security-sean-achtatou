package com.baidu.mobads.openad.c;

import com.baidu.mobads.j.m;
import com.baidu.mobads.openad.interfaces.download.IOAdDownloader;

class c implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ b f544a;

    c(b bVar) {
        this.f544a = bVar;
    }

    public void run() {
        try {
            if (this.f544a.c.g == IOAdDownloader.DownloadStatus.CANCELLED) {
                b.f543a.cancel(this.f544a.c.f);
                return;
            }
            b.f543a.notify(this.f544a.c.f, this.f544a.d());
            if (this.f544a.c.g == IOAdDownloader.DownloadStatus.ERROR) {
                m.a().f().d("OAdApkDownloaderObserver", "status >> error");
            } else if (this.f544a.c.g == IOAdDownloader.DownloadStatus.INITING && this.f544a.c.q == 1) {
                this.f544a.d("开始下载 " + this.f544a.c.f469a);
            }
        } catch (Exception e) {
            m.a().f().d("OAdApkDownloaderObserver", e);
        }
    }
}
