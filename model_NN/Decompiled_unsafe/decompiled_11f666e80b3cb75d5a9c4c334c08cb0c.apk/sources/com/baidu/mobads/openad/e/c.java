package com.baidu.mobads.openad.e;

import com.baidu.mobads.j.j;
import java.net.HttpURLConnection;

class c implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ a f555a;

    c(a aVar) {
        this.f555a = aVar;
    }

    public void run() {
        try {
            if (this.f555a.g != null && this.f555a.e.getAndSet(false)) {
                this.f555a.g.disconnect();
                HttpURLConnection unused = this.f555a.g = (HttpURLConnection) null;
            }
        } catch (Exception e) {
            j.a().e(e);
        }
    }
}
