package com.baidu.mobads.production;

import android.content.Context;
import com.baidu.mobads.interfaces.utils.IXAdPackageUtils;
import java.util.Timer;
import java.util.TimerTask;

class q extends TimerTask {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ IXAdPackageUtils f591a;
    final /* synthetic */ Context b;
    final /* synthetic */ String c;
    final /* synthetic */ Timer d;
    final /* synthetic */ o e;

    q(o oVar, IXAdPackageUtils iXAdPackageUtils, Context context, String str, Timer timer) {
        this.e = oVar;
        this.f591a = iXAdPackageUtils;
        this.b = context;
        this.c = str;
        this.d = timer;
    }

    public void run() {
        if (this.e.j >= this.e.k) {
            if (this.e.j >= this.e.l) {
                this.d.cancel();
                this.f591a.sendDialerIsSuccess(this.b, true, 0, this.c);
            } else if (!this.f591a.isForeground(this.b, this.c) && this.f591a.isForeground(this.b, this.b.getPackageName())) {
                this.d.cancel();
                this.f591a.sendDialerIsSuccess(this.b, false, this.e.j, this.c);
            }
        }
        o.h(this.e);
    }
}
