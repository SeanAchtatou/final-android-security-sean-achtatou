package com.baidu.mobads.production;

import com.baidu.mobads.c.a;
import com.baidu.mobads.j.m;
import com.baidu.mobads.openad.interfaces.event.IOAdEvent;
import com.baidu.mobads.openad.interfaces.event.IOAdEventListener;
import com.baidu.mobads.vo.c;
import org.json.JSONException;

class b implements IOAdEventListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ a f563a;

    b(a aVar) {
        this.f563a = aVar;
    }

    public void run(IOAdEvent iOAdEvent) {
        this.f563a.g();
        if ("URLLoader.Load.Complete".equals(iOAdEvent.getType())) {
            try {
                this.f563a.setAdResponseInfo(new c((String) iOAdEvent.getData().get("message")));
                if (this.f563a.getAdResponseInfo().getAdInstanceList().size() > 0) {
                    this.f563a.b = true;
                    this.f563a.a("XAdMouldeLoader ad-server requesting success");
                    return;
                }
                m.a().q().printErrorMessage(this.f563a.getAdResponseInfo().getErrorCode(), this.f563a.getAdResponseInfo().getErrorMessage(), "");
                this.f563a.b(this.f563a.getAdResponseInfo().getErrorMessage());
            } catch (JSONException e) {
                m.a().q().printErrorMessage("", "response json parsing error", "");
                this.f563a.b("response json parsing error");
                a.a().a("response json parsing error");
            }
        } else {
            m.a().q().printErrorMessage("", "request ad-server error, io_err/timeout", "");
            this.f563a.b("request ad-server error, io_err/timeout");
            a.a().a("request ad-server error, io_err/timeout");
        }
    }
}
