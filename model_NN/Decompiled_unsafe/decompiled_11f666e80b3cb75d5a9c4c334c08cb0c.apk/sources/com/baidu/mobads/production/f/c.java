package com.baidu.mobads.production.f;

import android.graphics.Color;

class c implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ b f572a;

    c(b bVar) {
        this.f572a = bVar;
    }

    public void run() {
        if (this.f572a.o()) {
            this.f572a.e.setBackgroundColor(Color.argb(51, 0, 0, 0));
        }
        if (this.f572a.h.getAdView() != null) {
            this.f572a.h.getAdView().setVisibility(0);
        }
        if (this.f572a.s()) {
            this.f572a.x.d("add countdown view");
            this.f572a.u();
            this.f572a.e.addView(this.f572a.t(), this.f572a.v());
        }
    }
}
