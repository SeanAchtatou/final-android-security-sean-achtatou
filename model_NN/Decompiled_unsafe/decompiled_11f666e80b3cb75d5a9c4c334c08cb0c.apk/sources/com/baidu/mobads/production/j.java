package com.baidu.mobads.production;

import com.baidu.mobads.c.a;
import com.baidu.mobads.h.g;
import com.baidu.mobads.interfaces.event.IXAdEvent;

class j implements g.c {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ a f584a;

    j(a aVar) {
        this.f584a = aVar;
    }

    public void a(boolean z) {
        if (z) {
            try {
                if (BaiduXAdSDKContext.mApkLoader != null) {
                    a.f561a = BaiduXAdSDKContext.mApkLoader.g();
                    BaiduXAdSDKContext.isRemoteLoadSuccess = true;
                    this.f584a.a("XAdMouldeLoader load success");
                    return;
                }
            } catch (Exception e) {
                com.baidu.mobads.j.j.a().e(e);
                a.a().a("async apk on load exception: " + e.toString());
                return;
            }
        }
        BaiduXAdSDKContext.mApkLoader = null;
        this.f584a.dispatchEvent(new com.baidu.mobads.f.a(IXAdEvent.AD_ERROR));
    }
}
