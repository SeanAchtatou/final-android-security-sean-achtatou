package com.baidu.mobads.production;

import com.baidu.mobads.interfaces.download.activate.IXMonitorActivation;

class g implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ f f576a;

    g(f fVar) {
        this.f576a = fVar;
    }

    public void run() {
        try {
            if (a.f561a != null) {
                IXMonitorActivation xMonitorActivation = a.f561a.getXMonitorActivation(this.f576a.f571a, this.f576a.b.s);
                xMonitorActivation.setIXActivateListener(new h(this));
                xMonitorActivation.startMonitor();
            }
        } catch (Exception e) {
            this.f576a.b.s.e(e);
        }
    }
}
