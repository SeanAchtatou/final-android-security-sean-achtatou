package com.baidu.mobads.production.f;

import android.view.ViewGroup;

class d implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ b f573a;

    d(b bVar) {
        this.f573a = bVar;
    }

    public void run() {
        this.f573a.x.d("remote Interstitial.removeAd");
        try {
            if (this.f573a.e.getParent() != null) {
                ((ViewGroup) this.f573a.e.getParent()).removeView(this.f573a.e);
            }
            this.f573a.e.removeAllViews();
        } catch (Exception e) {
            this.f573a.x.d("Interstitial.removeAd", e);
        }
    }
}
