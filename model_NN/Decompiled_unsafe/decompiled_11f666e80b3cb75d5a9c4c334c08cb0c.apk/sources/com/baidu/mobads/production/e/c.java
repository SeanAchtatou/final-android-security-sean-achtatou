package com.baidu.mobads.production.e;

import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.TranslateAnimation;
import android.widget.RelativeLayout;
import com.baidu.mobads.j.j;

class c implements View.OnTouchListener {

    /* renamed from: a  reason: collision with root package name */
    int f569a;
    int b;
    int c;
    int d;
    final /* synthetic */ int e;
    final /* synthetic */ int f;
    final /* synthetic */ View g;
    final /* synthetic */ int h;
    final /* synthetic */ int i;
    final /* synthetic */ b j;

    c(b bVar, int i2, int i3, View view, int i4, int i5) {
        this.j = bVar;
        this.e = i2;
        this.f = i3;
        this.g = view;
        this.h = i4;
        this.i = i5;
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        int width;
        RelativeLayout a2 = this.j.z;
        try {
            switch (motionEvent.getAction()) {
                case 0:
                    this.f569a = (int) motionEvent.getRawX();
                    this.b = (int) motionEvent.getRawY();
                    this.c = this.f569a;
                    this.d = this.b;
                    break;
                case 1:
                    if (Math.abs(this.c - this.f569a) >= 15 || Math.abs(this.d - this.b) < 15) {
                    }
                    boolean z = a2.getLeft() + (a2.getWidth() / 2) < this.e / 2;
                    if (z) {
                        width = 0;
                    } else {
                        width = this.e - a2.getWidth();
                    }
                    TranslateAnimation translateAnimation = new TranslateAnimation((float) a2.getLeft(), (float) width, 0.0f, 0.0f);
                    translateAnimation.setDuration(500);
                    this.g.startAnimation(translateAnimation);
                    RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(this.h, this.i);
                    if (!z) {
                        layoutParams.rightMargin = 0;
                        layoutParams.topMargin = a2.getTop();
                        this.g.setLayoutParams(layoutParams);
                        new Handler().postDelayed(new d(this, layoutParams, a2), 501);
                        break;
                    } else {
                        layoutParams.leftMargin = 0;
                        layoutParams.topMargin = a2.getTop();
                        this.g.setLayoutParams(layoutParams);
                        break;
                    }
                case 2:
                    int rawX = ((int) motionEvent.getRawX()) - this.f569a;
                    int rawY = ((int) motionEvent.getRawY()) - this.b;
                    int left = this.j.z.getLeft() + rawX;
                    int top = this.j.z.getTop() + rawY;
                    int right = this.j.z.getRight() + rawX;
                    int bottom = this.j.z.getBottom() + rawY;
                    if (left < 0) {
                        right = this.j.z.getWidth() + 0;
                        left = 0;
                    }
                    if (right > this.e) {
                        right = this.e;
                        left = right - this.j.z.getWidth();
                    }
                    if (top < this.j.E) {
                        top = this.j.E;
                        bottom = this.j.z.getHeight() + top;
                    }
                    if (bottom > this.f) {
                        bottom = this.f;
                        top = bottom - this.j.z.getHeight();
                    }
                    this.j.z.layout(left, top, right, bottom);
                    this.f569a = (int) motionEvent.getRawX();
                    this.b = (int) motionEvent.getRawY();
                    break;
            }
        } catch (Exception e2) {
            j.a().e(e2);
        }
        return false;
    }
}
