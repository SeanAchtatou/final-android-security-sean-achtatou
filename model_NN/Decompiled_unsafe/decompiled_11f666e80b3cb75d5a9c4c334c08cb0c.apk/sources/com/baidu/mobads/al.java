package com.baidu.mobads;

import com.baidu.mobads.interfaces.event.IXAdEvent;
import com.baidu.mobads.openad.interfaces.event.IOAdEvent;

class al implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ IOAdEvent f457a;
    final /* synthetic */ ak b;

    al(ak akVar, IOAdEvent iOAdEvent) {
        this.b = akVar;
        this.f457a = iOAdEvent;
    }

    public void run() {
        if (IXAdEvent.AD_LOADED.equals(this.f457a.getType())) {
            this.b.f456a.c.onVideoPrepared();
        }
        if (IXAdEvent.AD_STARTED.equals(this.f457a.getType())) {
            this.b.f456a.c.onVideoStart();
        }
        if (IXAdEvent.AD_CLICK_THRU.equals(this.f457a.getType())) {
            this.b.f456a.c.onVideoClickAd();
        }
        if (IXAdEvent.AD_STOPPED.equals(this.f457a.getType())) {
            this.b.f456a.c.onVideoFinish();
        }
        if (IXAdEvent.AD_ERROR.equals(this.f457a.getType())) {
            String str = (String) this.f457a.getData().get("message");
            this.b.f456a.c.onVideoError();
        }
    }
}
