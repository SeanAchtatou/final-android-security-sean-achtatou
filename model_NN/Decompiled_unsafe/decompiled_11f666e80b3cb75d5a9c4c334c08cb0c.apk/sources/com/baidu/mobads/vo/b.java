package com.baidu.mobads.vo;

import com.baidu.mobads.AdSettings;
import com.baidu.mobads.interfaces.IXAdConstants4PDK;
import com.baidu.mobads.interfaces.IXAdProdInfo;
import org.json.JSONObject;

public class b implements IXAdProdInfo {

    /* renamed from: a  reason: collision with root package name */
    private d f602a;
    private IXAdConstants4PDK.SlotType b;
    private JSONObject c;
    private boolean d = false;

    public boolean isAutoPlay() {
        return this.d;
    }

    public void a(boolean z) {
        this.d = z;
    }

    public int getApt() {
        return this.f602a.getApt();
    }

    public boolean isMsspTagAvailable() {
        return false;
    }

    public b(d dVar, IXAdConstants4PDK.SlotType slotType) {
        this.f602a = dVar;
        this.b = slotType;
    }

    public int getRequestAdWidth() {
        return this.f602a.getW();
    }

    public int getRequestAdHeight() {
        return this.f602a.getH();
    }

    public String getAdPlacementId() {
        return this.f602a.getApid();
    }

    public String getProdType() {
        return this.f602a.getProd();
    }

    public IXAdConstants4PDK.SlotType getType() {
        return this.b;
    }

    public JSONObject getAttribute() {
        return this.c != null ? this.c : AdSettings.getAttr();
    }

    public void a(JSONObject jSONObject) {
        this.c = jSONObject;
    }

    public int getInstanceCount() {
        return 0;
    }

    public String getAdRequestURL() {
        return this.f602a.b();
    }
}
