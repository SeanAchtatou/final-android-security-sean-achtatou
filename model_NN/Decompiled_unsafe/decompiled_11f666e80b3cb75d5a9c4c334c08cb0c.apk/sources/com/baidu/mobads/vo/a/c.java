package com.baidu.mobads.vo.a;

import com.baidu.mobads.command.XAdCommandExtraInfo;
import java.util.HashMap;
import java.util.concurrent.atomic.AtomicInteger;

public class c extends a {
    public long A = 0;
    public long B = 0;
    public int C = 0;
    public int D = 0;
    public int E = 0;
    public int F = 0;
    public int G = 0;
    public int H = 0;
    public int I = 0;
    public String o;
    public int p;
    public int q;
    public int r;
    public AtomicInteger s = new AtomicInteger(0);
    public int t;
    public int u;
    public long v;
    public int w;
    public int x;
    public int y;
    public int z;

    public c(XAdCommandExtraInfo xAdCommandExtraInfo) {
        super(xAdCommandExtraInfo);
    }

    /* access modifiers changed from: protected */
    public HashMap<String, String> b() {
        HashMap<String, String> hashMap = new HashMap<>();
        if (this.o.length() > 1024) {
            hashMap.put("obj", this.o.substring(0, 1023));
        } else {
            hashMap.put("obj", this.o);
        }
        hashMap.put("order", "" + this.p);
        hashMap.put("height", "" + this.q);
        hashMap.put("progress", "" + this.r);
        hashMap.put("moves", "" + this.s.get());
        hashMap.put("clicks", "" + this.t);
        hashMap.put("urlclicks", "" + this.t);
        hashMap.put("lploadtime", "" + this.u);
        hashMap.put("duration", "" + this.v);
        hashMap.put("_lpWebStartLoad", "" + this.A);
        hashMap.put("_lpWebFinishLoad", "" + this.B);
        hashMap.put("e75", "" + this.w);
        hashMap.put("e75_3", "" + this.x);
        hashMap.put("from", "" + this.y);
        hashMap.put("maxTabs", "" + this.z);
        hashMap.put("b_cancel", "" + this.G);
        hashMap.put("b_refresh", "" + this.E);
        hashMap.put("b_copy", "" + this.F);
        hashMap.put("b_goback", "" + this.C);
        hashMap.put("b_threeP", "" + this.D);
        hashMap.put("b_home", "" + this.I);
        hashMap.put("b_osgoback", "" + this.H);
        return hashMap;
    }
}
