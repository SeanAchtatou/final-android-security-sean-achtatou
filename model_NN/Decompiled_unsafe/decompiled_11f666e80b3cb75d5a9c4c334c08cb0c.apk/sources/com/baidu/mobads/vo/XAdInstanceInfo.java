package com.baidu.mobads.vo;

import android.annotation.SuppressLint;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import com.baidu.mobads.interfaces.IXAdInstanceInfo;
import com.baidu.mobads.interfaces.IXAdRequestInfo;
import com.baidu.mobads.j.m;
import com.meizu.cloud.pushsdk.notification.model.NotifyType;
import com.meizu.cloud.pushsdk.notification.model.TimeDisplaySetting;
import com.sina.weibo.sdk.constant.WBConstants;
import com.sina.weibo.sdk.exception.WeiboAuthException;
import com.tencent.open.SocialConstants;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONObject;

public class XAdInstanceInfo implements Parcelable, IXAdInstanceInfo, Cloneable {
    public static final Parcelable.Creator<XAdInstanceInfo> CREATOR = new a();
    public static final String TAG = "XAdInstanceInfo";
    private Set<String> A;
    private Set<String> B;
    private Set<String> C;
    private Set<String> D;
    private Set<String> E;
    private Set<String> F;
    private Set<String> G;
    private Set<String> H;
    private Set<String> I;
    private int J;
    private boolean K;
    private String L;
    private String M;
    private String N;
    private String O;
    private String P;
    private long Q;
    private int R;
    private String S;
    private int T;
    private boolean U;
    private long V;
    private IXAdInstanceInfo.CreativeType W;
    private String X;
    private int Y;
    private boolean Z;

    /* renamed from: a  reason: collision with root package name */
    private String f600a;
    private boolean aa;
    private boolean ab;
    private boolean ac;
    private boolean ad;
    private boolean ae;
    private boolean af;
    private boolean ag;
    private String ah;
    private String ai;
    private String aj;
    private JSONArray ak;
    private boolean al;
    private boolean am;
    private String an;
    private String b;
    private String c;
    private String d;
    private String e;
    private String f;
    private String g;
    private String h;
    private String i;
    private String j;
    private String k;
    private int l;
    private String m;
    private String n;
    private boolean o;
    private int p;
    @Deprecated
    private boolean q;
    @Deprecated
    private int r;
    private String s;
    private String t;
    private JSONObject u;
    private String v;
    private int w;
    private int x;
    @Deprecated
    private String y;
    private Set<String> z;

    /* synthetic */ XAdInstanceInfo(Parcel parcel, a aVar) {
        this(parcel);
    }

    public String getUrl() {
        return this.X;
    }

    public void setUrl(String str) {
        this.X = str;
    }

    public int getDlTunnel() {
        return this.Y;
    }

    public void setDlTunnel(int i2) {
        this.Y = i2;
    }

    public boolean isInapp() {
        return this.Z;
    }

    public void setInapp(boolean z2) {
        this.Z = z2;
    }

    public boolean isClose() {
        return this.aa;
    }

    public void setClose(boolean z2) {
        this.aa = z2;
    }

    public boolean isAutoOpen() {
        return this.ab;
    }

    public void setAutoOpen(boolean z2) {
        this.ab = z2;
    }

    public boolean isPopNotif() {
        return this.ac;
    }

    public void setPopNotif(boolean z2) {
        this.ac = z2;
    }

    public boolean isWifiTargeted() {
        return this.ad;
    }

    public void setWifiTargeted(boolean z2) {
        this.ad = z2;
    }

    public boolean isTooLarge() {
        return this.ae;
    }

    public void setTooLarge(boolean z2) {
        this.ae = z2;
    }

    public boolean isCanCancel() {
        return this.af;
    }

    public void setCanCancel(boolean z2) {
        this.af = z2;
    }

    public boolean isCanDelete() {
        return this.ag;
    }

    public void setCanDelete(boolean z2) {
        this.ag = z2;
    }

    public IXAdInstanceInfo.CreativeType getCreativeType() {
        return this.W;
    }

    public void setCreativeType(IXAdInstanceInfo.CreativeType creativeType) {
        this.W = creativeType;
    }

    public String getFwt() {
        return this.f600a;
    }

    public void setFwt(String str) {
        this.f600a = str;
    }

    public String getAdId() {
        return this.b;
    }

    public void setAdId(String str) {
        this.b = str;
    }

    public Boolean isValid() {
        return Boolean.valueOf(!WeiboAuthException.DEFAULT_AUTH_ERROR_CODE.equalsIgnoreCase(getAdId()));
    }

    public String getAdSource() {
        return this.c;
    }

    public void setAdSource(String str) {
        this.c = str;
    }

    public String getTitle() {
        return this.d;
    }

    public void setTitle(String str) {
        this.d = str;
    }

    public String getDescription() {
        return this.e;
    }

    public void setDescription(String str) {
        this.e = str;
    }

    public String getSponsorUrl() {
        return this.f;
    }

    public void setSponsorUrl(String str) {
        this.f = str;
    }

    public String getMaterialType() {
        return this.g;
    }

    public void setMaterialType(String str) {
        this.g = str;
    }

    public String getPhoneNumber() {
        return this.h;
    }

    public void setPhoneNumber(String str) {
        this.h = str;
    }

    public String getMainPictureUrl() {
        return this.i;
    }

    public void setMainPictureUrl(String str) {
        this.i = str;
    }

    public String getIconUrl() {
        return this.j;
    }

    public void setIconUrl(String str) {
        this.j = str;
    }

    public String getExp2ForSingleAd() {
        return this.k;
    }

    public void setExp2ForSingleAd(String str) {
        this.k = str;
    }

    public int getAntiTag() {
        return this.l;
    }

    public void setAntiTag(int i2) {
        this.l = i2;
    }

    public String getLocalCreativeURL() {
        return this.m;
    }

    public void setLocalCreativeURL(String str) {
        this.m = str;
    }

    public String getVideoUrl() {
        return this.n;
    }

    public void setVideoUrl(String str) {
        this.n = str;
    }

    public boolean isVideoMuted() {
        return this.o;
    }

    public void setVideoMuted(boolean z2) {
        this.o = z2;
    }

    public int getVideoDuration() {
        return this.p;
    }

    public void setVideoDuration(int i2) {
        this.p = i2;
    }

    public boolean isIconVisibleForImageType() {
        return this.q;
    }

    public void setIconVisibleForImageType(boolean z2) {
        this.q = z2;
    }

    public int getHoursInADayToShowAd() {
        return this.r;
    }

    public void setHoursInADayToShowAd(int i2) {
        this.r = i2;
    }

    public String getClickThroughUrl() {
        return this.s;
    }

    public void setClickThroughUrl(String str) {
        this.s = str;
    }

    public String getOriginClickUrl() {
        return this.t;
    }

    public void setOriginClickUrl(String str) {
        this.t = str;
    }

    public String getHtmlSnippet() {
        return this.v;
    }

    public void setHtmlSnippet(String str) {
        this.v = str;
    }

    public int getMainMaterialWidth() {
        return this.w;
    }

    public void setMainMaterialWidth(int i2) {
        this.w = i2;
    }

    public int getMainMaterialHeight() {
        return this.x;
    }

    public void setMainMaterialHeight(int i2) {
        this.x = i2;
    }

    public String getPhoneForLocalBranding() {
        return this.y;
    }

    public void setPhoneForLocalBranding(String str) {
        this.y = str;
    }

    public Set<String> getImpressionUrls() {
        return this.z;
    }

    public void setImpressionUrls(Set<String> set) {
        this.z = set;
    }

    public List<String> getThirdImpressionTrackingUrls() {
        return new ArrayList(this.A);
    }

    public void setThirdImpressionTrackingUrls(Set<String> set) {
        this.A = set;
    }

    public List<String> getThirdClickTrackingUrls() {
        return new ArrayList(this.B);
    }

    public void setThirdClickTrackingUrls(Set<String> set) {
        this.B = set;
    }

    public int getActionType() {
        return this.J;
    }

    public void setActionType(int i2) {
        this.J = i2;
    }

    public boolean isActionOnlyWifi() {
        return this.K;
    }

    public void setActionOnlyWifi(boolean z2) {
        this.K = z2;
    }

    public String getConfirmBorderPercent() {
        return this.L;
    }

    public void setConfirmBorderPercent(String str) {
        this.L = str;
    }

    public String getQueryKey() {
        return this.M;
    }

    public void setQueryKey(String str) {
        this.M = str;
    }

    public String getAppPackageName() {
        return this.O;
    }

    public void setAppPackageName(String str) {
        this.O = str;
    }

    public String getAppName() {
        return this.P;
    }

    public void setAppName(String str) {
        this.P = str;
    }

    public long getAppSize() {
        return this.Q;
    }

    public void setAppSize(long j2) {
        this.Q = j2;
    }

    public int getSwitchButton() {
        return this.R;
    }

    public void setSwitchButton(int i2) {
        this.R = i2;
    }

    public String getAppOpenStrs() {
        return this.S;
    }

    public void setAppOpenStrs(String str) {
        this.S = str;
    }

    public int getPointsForWall() {
        return this.T;
    }

    public void setPointsForWall(int i2) {
        this.T = i2;
    }

    public boolean isTaskDoneForWall() {
        return this.U;
    }

    public void setTaskDoneForWall(boolean z2) {
        this.U = z2;
    }

    public JSONObject getOriginJsonObject() {
        return this.u;
    }

    public String getVurl() {
        return this.ah;
    }

    public void setVurl(String str) {
        this.ah = str;
    }

    public String getClklogurl() {
        return this.ai;
    }

    public void setClklogurl(String str) {
        this.ai = str;
    }

    public String getWinurl() {
        return this.aj;
    }

    public void setWinurl(String str) {
        this.aj = str;
    }

    public JSONArray getNwinurl() {
        return this.ak;
    }

    public void setNwinurl(JSONArray jSONArray) {
        this.ak = jSONArray;
    }

    @SuppressLint({"DefaultLocale"})
    public XAdInstanceInfo(JSONObject jSONObject) {
        boolean z2;
        this.b = WeiboAuthException.DEFAULT_AUTH_ERROR_CODE;
        this.z = new HashSet();
        this.A = new HashSet();
        this.B = new HashSet();
        this.C = new HashSet();
        this.D = new HashSet();
        this.E = new HashSet();
        this.F = new HashSet();
        this.G = new HashSet();
        this.H = new HashSet();
        this.I = new HashSet();
        this.K = true;
        this.W = IXAdInstanceInfo.CreativeType.NONE;
        this.Z = true;
        this.ab = true;
        this.ac = true;
        this.al = false;
        this.am = false;
        this.an = null;
        this.u = jSONObject;
        try {
            this.V = System.currentTimeMillis();
            this.J = jSONObject.optInt("act");
            this.v = jSONObject.optString("html", null);
            this.b = jSONObject.optString("id", WeiboAuthException.DEFAULT_AUTH_ERROR_CODE);
            this.c = jSONObject.optString("src", "");
            this.d = jSONObject.optString("tit", "");
            this.e = jSONObject.optString(SocialConstants.PARAM_APP_DESC, "");
            this.f = jSONObject.optString("surl", "");
            this.h = jSONObject.optString("phone", "");
            this.i = jSONObject.optString("w_picurl", "");
            this.j = jSONObject.optString("icon", "");
            this.k = jSONObject.optString("exp2", "{}");
            this.l = jSONObject.optInt("anti_tag");
            this.n = jSONObject.optString("vurl", "");
            this.p = jSONObject.optInt("duration", 0);
            this.o = jSONObject.optInt("sound", 0) != 1;
            if (jSONObject.optInt("iv", 0) == 1) {
                z2 = true;
            } else {
                z2 = false;
            }
            this.q = z2;
            this.r = jSONObject.optInt("dur", 0);
            this.s = jSONObject.optString("curl", "");
            this.t = jSONObject.optString("ori_curl", "");
            this.g = jSONObject.optString("type");
            if (this.v != null && this.v.length() > 0 && (this.v.contains("html") || this.v.contains("HTML"))) {
                this.W = IXAdInstanceInfo.CreativeType.HTML;
            } else if (this.g != null) {
                if (this.g.equals("text")) {
                    this.W = IXAdInstanceInfo.CreativeType.TEXT;
                } else if (this.g.equals(WBConstants.GAME_PARAMS_GAME_IMAGE_URL)) {
                    if (this.i != null && !this.i.equals("")) {
                        int lastIndexOf = this.i.toLowerCase(Locale.getDefault()).lastIndexOf(46);
                        if ((lastIndexOf >= 0 ? this.i.toLowerCase(Locale.getDefault()).substring(lastIndexOf) : "").equals(".gif")) {
                            this.W = IXAdInstanceInfo.CreativeType.GIF;
                        } else {
                            this.W = IXAdInstanceInfo.CreativeType.STATIC_IMAGE;
                        }
                    }
                } else if (this.g.equals("rm")) {
                    this.W = IXAdInstanceInfo.CreativeType.RM;
                } else if (this.g.equals("video")) {
                    this.W = IXAdInstanceInfo.CreativeType.VIDEO;
                }
            }
            this.w = jSONObject.optInt(IXAdRequestInfo.WIDTH);
            this.x = jSONObject.optInt(IXAdRequestInfo.HEIGHT);
            this.y = jSONObject.optString("lb_phone", "");
            JSONArray optJSONArray = jSONObject.optJSONArray("nwinurl");
            if (optJSONArray == null || optJSONArray.length() <= 0) {
                String optString = jSONObject.optString("winurl", "");
                if (!optString.equals("")) {
                    this.z.add(optString);
                }
            } else {
                for (int i2 = 0; i2 < optJSONArray.length(); i2++) {
                    this.z.add(optJSONArray.getString(i2));
                }
            }
            String optString2 = jSONObject.optString("clklogurl", "");
            if (!optString2.equals("")) {
                this.z.add(optString2);
            }
            JSONArray optJSONArray2 = jSONObject.optJSONArray("mon");
            if (optJSONArray2 != null && optJSONArray2.length() > 0) {
                for (int i3 = 0; i3 < optJSONArray2.length(); i3++) {
                    JSONObject jSONObject2 = optJSONArray2.getJSONObject(i3);
                    String optString3 = jSONObject2.optString(NotifyType.SOUND, "");
                    String optString4 = jSONObject2.optString("c", "");
                    a(optString3);
                    b(optString4);
                }
            }
            JSONObject optJSONObject = jSONObject.optJSONObject("monitors");
            if (optJSONObject != null) {
                Iterator<String> keys = optJSONObject.keys();
                while (keys.hasNext()) {
                    String next = keys.next();
                    if (next.equals(NotifyType.SOUND)) {
                        JSONArray optJSONArray3 = optJSONObject.optJSONArray(next);
                        for (int i4 = 0; i4 < optJSONArray3.length(); i4++) {
                            a(optJSONArray3.optString(i4));
                        }
                    } else if (next.equals("vskip")) {
                        JSONArray optJSONArray4 = optJSONObject.optJSONArray(next);
                        for (int i5 = 0; i5 < optJSONArray4.length(); i5++) {
                            addSkipMonitorTrackers(optJSONArray4.optString(i5));
                        }
                    } else if (next.equals("scard")) {
                        JSONArray optJSONArray5 = optJSONObject.optJSONArray(next);
                        for (int i6 = 0; i6 < optJSONArray5.length(); i6++) {
                            addScardMonitorTrackers(optJSONArray5.optString(i6));
                        }
                    } else if (next.equals("ccard")) {
                        JSONArray optJSONArray6 = optJSONObject.optJSONArray(next);
                        for (int i7 = 0; i7 < optJSONArray6.length(); i7++) {
                            addCcardMonitorTrackers(optJSONArray6.optString(i7));
                        }
                    } else if (next.equals("vstart")) {
                        JSONArray optJSONArray7 = optJSONObject.optJSONArray(next);
                        for (int i8 = 0; i8 < optJSONArray7.length(); i8++) {
                            addStartMonitorTrackers(optJSONArray7.optString(i8));
                        }
                    } else if (next.equals("vfullscreen")) {
                        JSONArray optJSONArray8 = optJSONObject.optJSONArray(next);
                        for (int i9 = 0; i9 < optJSONArray8.length(); i9++) {
                            addFullScreenMonitorTrackers(optJSONArray8.optString(i9));
                        }
                    } else if (next.equals("vclose")) {
                        JSONArray optJSONArray9 = optJSONObject.optJSONArray(next);
                        for (int i10 = 0; i10 < optJSONArray9.length(); i10++) {
                            addCloseMonitorTrackers(optJSONArray9.optString(i10));
                        }
                    } else if (next.equals("cstartcard")) {
                        JSONArray optJSONArray10 = optJSONObject.optJSONArray(next);
                        for (int i11 = 0; i11 < optJSONArray10.length(); i11++) {
                            addCstartcardMonitorTrackers(optJSONArray10.optString(i11));
                        }
                    } else if (next.equals("c")) {
                        JSONArray optJSONArray11 = optJSONObject.optJSONArray(next);
                        for (int i12 = 0; i12 < optJSONArray11.length(); i12++) {
                            b(optJSONArray11.optString(i12));
                        }
                    }
                }
            }
            this.K = true;
            this.L = jSONObject.optString("cf", "");
            this.M = jSONObject.optString("qk", "");
            this.N = this.M + "_" + new Random().nextLong() + System.currentTimeMillis() + "|";
            this.P = jSONObject.optString("appname", "");
            this.O = jSONObject.optString("pk", "");
            this.Q = jSONObject.optLong("sz", 0);
            this.R = jSONObject.optInt("sb", 0);
            this.S = jSONObject.optString("apo", "");
            this.T = jSONObject.optInt("po", 0);
            this.U = jSONObject.optInt(TimeDisplaySetting.START_SHOW_TIME, 0) == 1;
        } catch (Exception e2) {
            m.a().f().e(TAG, e2.getMessage());
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(String str) {
        if (!TextUtils.isEmpty(str)) {
            this.A.add(str);
        }
    }

    /* access modifiers changed from: package-private */
    public final void b(String str) {
        if (!TextUtils.isEmpty(str)) {
            this.B.add(str);
        }
    }

    public void addStartMonitorTrackers(String str) {
        if (!TextUtils.isEmpty(str)) {
            this.C.add(str);
        }
    }

    public List<String> getStartTrackers() {
        return new ArrayList(this.C);
    }

    public void setStartTrackers(List<String> list) {
        this.C.clear();
        this.C.addAll(list);
    }

    public void addSkipMonitorTrackers(String str) {
        if (!TextUtils.isEmpty(str)) {
            this.D.add(str);
        }
    }

    public List<String> getSkipTrackers() {
        return new ArrayList(this.D);
    }

    public void setSkipTrackers(List<String> list) {
        this.D.addAll(list);
    }

    public void addScardMonitorTrackers(String str) {
        if (!TextUtils.isEmpty(str)) {
            this.E.add(str);
        }
    }

    public List<String> getScardTrackers() {
        return new ArrayList(this.E);
    }

    public void setScardTrackers(List<String> list) {
        this.E.addAll(list);
    }

    public void addCcardMonitorTrackers(String str) {
        if (!TextUtils.isEmpty(str)) {
            this.F.add(str);
        }
    }

    public List<String> getCcardTrackers() {
        return new ArrayList(this.F);
    }

    public void setCcardTrackers(List<String> list) {
        this.F.addAll(list);
    }

    public void addFullScreenMonitorTrackers(String str) {
        if (str != null && !str.equals("")) {
            this.G.add(str);
        }
    }

    public List<String> getFullScreenTrackers() {
        return new ArrayList(this.G);
    }

    public void setFullScreenTrackers(List<String> list) {
        this.G.addAll(list);
    }

    public void addCloseMonitorTrackers(String str) {
        if (str != null && !str.equals("")) {
            this.H.add(str);
        }
    }

    public List<String> getCloseTrackers() {
        return new ArrayList(this.H);
    }

    public void setCstartcardTrackers(List<String> list) {
        this.I.clear();
        this.I.addAll(list);
    }

    public void addCstartcardMonitorTrackers(String str) {
        if (str != null && !str.equals("")) {
            this.I.add(str);
        }
    }

    public List<String> getCstartcardTrackers() {
        return new ArrayList(this.I);
    }

    public void setCloseTrackers(List<String> list) {
        this.H.clear();
        this.H.addAll(list);
    }

    /* access modifiers changed from: protected */
    public Object clone() {
        return super.clone();
    }

    public long getCreateTime() {
        return this.V;
    }

    public void setCreateTime(long j2) {
        this.V = j2;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i2) {
        parcel.writeString(this.b);
        parcel.writeString(this.c);
        parcel.writeString(this.P);
        parcel.writeString(this.S);
        parcel.writeString(this.O);
        parcel.writeString(this.s);
        parcel.writeString(this.ai);
        parcel.writeString(this.L);
        parcel.writeString(this.e);
        parcel.writeString(this.k);
        parcel.writeString(this.f600a);
        parcel.writeString(this.v);
        parcel.writeString(this.j);
        parcel.writeString(this.i);
        parcel.writeString(this.g);
        parcel.writeInt(this.x);
        parcel.writeInt(this.w);
        parcel.writeString(this.t);
        parcel.writeString(this.y);
        parcel.writeString(this.h);
        parcel.writeString(this.M);
        parcel.writeString(this.f);
        parcel.writeString(this.d);
        parcel.writeString(this.X);
        parcel.writeString(this.n);
        parcel.writeInt(this.p);
        parcel.writeString(this.ah);
        parcel.writeString(this.aj);
    }

    private XAdInstanceInfo(Parcel parcel) {
        this.b = WeiboAuthException.DEFAULT_AUTH_ERROR_CODE;
        this.z = new HashSet();
        this.A = new HashSet();
        this.B = new HashSet();
        this.C = new HashSet();
        this.D = new HashSet();
        this.E = new HashSet();
        this.F = new HashSet();
        this.G = new HashSet();
        this.H = new HashSet();
        this.I = new HashSet();
        this.K = true;
        this.W = IXAdInstanceInfo.CreativeType.NONE;
        this.Z = true;
        this.ab = true;
        this.ac = true;
        this.al = false;
        this.am = false;
        this.an = null;
        this.b = parcel.readString();
        this.c = parcel.readString();
        this.P = parcel.readString();
        this.S = parcel.readString();
        this.O = parcel.readString();
        this.s = parcel.readString();
        this.ai = parcel.readString();
        this.L = parcel.readString();
        this.e = parcel.readString();
        this.k = parcel.readString();
        this.f600a = parcel.readString();
        this.v = parcel.readString();
        this.j = parcel.readString();
        this.i = parcel.readString();
        this.g = parcel.readString();
        this.x = parcel.readInt();
        this.w = parcel.readInt();
        this.t = parcel.readString();
        this.y = parcel.readString();
        this.h = parcel.readString();
        this.M = parcel.readString();
        this.f = parcel.readString();
        this.d = parcel.readString();
        this.X = parcel.readString();
        this.n = parcel.readString();
        this.p = parcel.readInt();
        this.ah = parcel.readString();
        this.aj = parcel.readString();
    }

    public String getUniqueId() {
        return this.N;
    }

    public boolean isSecondConfirmed() {
        return this.al;
    }

    public void setSecondConfirmed(boolean z2) {
        this.al = z2;
    }

    public boolean getAPOOpen() {
        return this.am;
    }

    public void setAPOOpen(boolean z2) {
        this.am = z2;
    }

    public String getPage() {
        return this.an;
    }

    public void setPage(String str) {
        this.an = str;
    }
}
