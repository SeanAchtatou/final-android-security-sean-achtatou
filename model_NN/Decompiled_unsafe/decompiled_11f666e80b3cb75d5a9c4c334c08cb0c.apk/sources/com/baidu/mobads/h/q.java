package com.baidu.mobads.h;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.util.Log;
import com.baidu.mobads.j.m;
import java.lang.Thread;

public class q implements Thread.UncaughtExceptionHandler {

    /* renamed from: a  reason: collision with root package name */
    private static Thread.UncaughtExceptionHandler f501a;
    private static volatile q b;
    private Context c;
    private a d;

    interface a {
        void a(String str);
    }

    public static q a(Context context) {
        if (b == null) {
            synchronized (q.class) {
                if (b == null) {
                    b = new q(context);
                }
            }
        }
        return b;
    }

    private q(Context context) {
        this.c = context.getApplicationContext();
        f501a = Thread.getDefaultUncaughtExceptionHandler();
        new Thread(new r(this)).start();
    }

    public void a() {
        if (!(Thread.getDefaultUncaughtExceptionHandler() instanceof q)) {
            Thread.setDefaultUncaughtExceptionHandler(this);
        }
    }

    public void uncaughtException(Thread thread, Throwable th) {
        try {
            String a2 = a(th);
            if (a2 != null) {
                a(a2, Log.getStackTraceString(th));
                th.printStackTrace();
                if (this.d != null) {
                    this.d.a(a2);
                }
            }
            if (f501a != null) {
                f501a.uncaughtException(thread, th);
            }
        } catch (Exception e) {
            m.a().f().e(e);
        }
    }

    public void a(a aVar) {
        this.d = aVar;
    }

    private String a(Throwable th) {
        th.printStackTrace();
        Throwable cause = th.getCause();
        if (cause != null) {
            th = cause;
        }
        StackTraceElement[] stackTrace = th.getStackTrace();
        if (stackTrace != null && stackTrace.length > 0) {
            for (StackTraceElement className : stackTrace) {
                String className2 = className.getClassName();
                if (className2.startsWith("junit.framework")) {
                    return null;
                }
                if (className2.startsWith("com.baidu.mobads.container")) {
                    return "remote";
                }
                if (className2.startsWith("com.baidu.mobads.loader")) {
                    return "loader";
                }
                if (className2.startsWith("com.baidu.mobads")) {
                    return "proxy";
                }
            }
        }
        return null;
    }

    private SharedPreferences b() {
        return this.c.getSharedPreferences("baidu_mobads_crash", 0);
    }

    private SharedPreferences.Editor c() {
        return b().edit();
    }

    private void a(String str, String str2) {
        SharedPreferences.Editor c2 = c();
        c2.putString("key_crash_source", str);
        c2.putString("key_crash_trace", str2);
        c2.putString("key_crash_ad", com.baidu.mobads.production.d.a.w);
        if (Build.VERSION.SDK_INT >= 9) {
            c2.apply();
        } else {
            c2.commit();
        }
    }

    /* access modifiers changed from: private */
    public String a(String str) {
        return b().getString(str, "");
    }

    /* access modifiers changed from: private */
    public void d() {
        SharedPreferences.Editor c2 = c();
        c2.clear();
        if (Build.VERSION.SDK_INT >= 9) {
            c2.apply();
        } else {
            c2.commit();
        }
    }
}
