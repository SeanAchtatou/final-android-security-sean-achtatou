package com.baidu.mobads.h;

import android.os.Build;
import com.baidu.mobads.interfaces.IXAdRequestInfo;
import com.baidu.mobads.interfaces.utils.IXAdURIUitls;
import com.baidu.mobads.j.m;
import com.baidu.mobads.openad.e.a;
import com.baidu.mobads.openad.e.d;
import java.util.HashMap;

class l implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ k f496a;

    l(k kVar) {
        this.f496a = kVar;
    }

    public void run() {
        try {
            IXAdURIUitls i = m.a().i();
            double d = this.f496a.f495a ? g.b.f486a : 0.0d;
            if (this.f496a.f495a) {
            }
            m mVar = new m(this, d);
            HashMap hashMap = new HashMap();
            hashMap.put("v", "" + d);
            hashMap.put(IXAdRequestInfo.OS, "android");
            hashMap.put(IXAdRequestInfo.PHONE_TYPE, m.a().m().getTextEncoder(Build.MODEL));
            hashMap.put(IXAdRequestInfo.BDR, m.a().m().getTextEncoder(Build.VERSION.SDK));
            d dVar = new d(i.addParameters(g.h, hashMap), "");
            dVar.e = 1;
            a unused = this.f496a.b.i = new a();
            this.f496a.b.i.addEventListener("URLLoader.Load.Complete", mVar);
            this.f496a.b.i.addEventListener("URLLoader.Load.Error", mVar);
            this.f496a.b.i.a(dVar);
        } catch (Exception e) {
        }
    }
}
