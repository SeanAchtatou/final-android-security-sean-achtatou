package com.baidu.mobads.h;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import com.baidu.mobads.h.g;

class i extends Handler {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ g f493a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    i(g gVar, Looper looper) {
        super(looper);
        this.f493a = gVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baidu.mobads.h.g.a(com.baidu.mobads.h.g, boolean):void
     arg types: [com.baidu.mobads.h.g, int]
     candidates:
      com.baidu.mobads.h.g.a(com.baidu.mobads.h.g, com.baidu.mobads.h.e):com.baidu.mobads.h.e
      com.baidu.mobads.h.g.a(com.baidu.mobads.h.g, com.baidu.mobads.openad.e.a):com.baidu.mobads.openad.e.a
      com.baidu.mobads.h.g.a(com.baidu.mobads.h.g, com.baidu.mobads.h.b):void
      com.baidu.mobads.h.g.a(boolean, java.lang.String):void
      com.baidu.mobads.h.g.a(com.baidu.mobads.h.g$c, android.os.Handler):void
      com.baidu.mobads.h.g.a(com.baidu.mobads.h.g, boolean):void */
    public void handleMessage(Message message) {
        String string = message.getData().getString("CODE");
        e eVar = (e) message.getData().getParcelable("APK_INFO");
        if ("OK".equals(string)) {
            b bVar = new b(eVar.e(), this.f493a.k, eVar);
            try {
                if (this.f493a.f == g.e) {
                    bVar.a();
                    bVar.a(this.f493a.e());
                    if (g.b != null) {
                        g.b.f486a = eVar.b();
                    }
                } else {
                    this.f493a.a(bVar);
                    bVar.a(this.f493a.e());
                    this.f493a.a(true);
                }
            } catch (g.a e) {
                this.f493a.a(false);
                this.f493a.l.e("XAdApkLoader", "download apk file failed: " + e.toString());
            } finally {
                bVar.delete();
            }
        } else {
            this.f493a.l.e("XAdApkLoader", "mOnApkDownloadCompleted: download failed, code: " + string);
            this.f493a.a(false);
        }
    }
}
