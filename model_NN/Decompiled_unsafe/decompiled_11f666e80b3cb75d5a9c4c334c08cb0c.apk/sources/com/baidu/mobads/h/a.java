package com.baidu.mobads.h;

import android.content.Context;
import com.baidu.mobads.h.g;
import com.baidu.mobads.interfaces.IXAdContainerFactory;
import com.baidu.mobads.interfaces.utils.IXAdLogger;
import com.baidu.mobads.j.m;

public class a {
    private static IXAdContainerFactory e;

    /* renamed from: a  reason: collision with root package name */
    public double f486a = 0.1d;
    private Context b;
    private Class<?> c = null;
    private double d;
    private Boolean f;
    private IXAdLogger g = m.a().f();

    public a(Class<?> cls, Context context, double d2, Boolean bool) {
        this.c = cls;
        this.b = context;
        this.d = d2;
        this.f = bool;
    }

    public IXAdContainerFactory a() {
        if (e == null) {
            try {
                e = (IXAdContainerFactory) this.c.getDeclaredConstructor(Context.class).newInstance(this.b);
                this.f486a = e.getRemoteVersion();
                e.setDebugMode(this.f);
                e.handleShakeVersion(this.d, "8.30");
            } catch (Throwable th) {
                this.g.w("XAdContainerFactoryBuilder", th.getMessage());
                throw new g.a("newXAdContainerFactory() failed, possibly API incompatible: " + th.getMessage());
            }
        }
        return e;
    }

    public void b() {
        e = null;
    }
}
