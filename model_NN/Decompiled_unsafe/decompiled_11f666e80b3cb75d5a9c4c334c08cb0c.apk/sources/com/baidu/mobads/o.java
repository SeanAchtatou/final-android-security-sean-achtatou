package com.baidu.mobads;

import android.content.DialogInterface;
import android.view.KeyEvent;
import android.webkit.SslErrorHandler;

class o implements DialogInterface.OnKeyListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SslErrorHandler f536a;
    final /* synthetic */ j b;

    o(j jVar, SslErrorHandler sslErrorHandler) {
        this.b = jVar;
        this.f536a = sslErrorHandler;
    }

    public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
        if (keyEvent.getAction() != 1 || i != 4) {
            return false;
        }
        this.f536a.cancel();
        dialogInterface.dismiss();
        return true;
    }
}
