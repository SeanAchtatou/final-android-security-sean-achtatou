package com.baidu.mobads.j;

import android.content.SharedPreferences;
import android.os.Build;

class o implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SharedPreferences f528a;
    final /* synthetic */ String b;
    final /* synthetic */ n c;

    o(n nVar, SharedPreferences sharedPreferences, String str) {
        this.c = nVar;
        this.f528a = sharedPreferences;
        this.b = str;
    }

    public void run() {
        if (Build.VERSION.SDK_INT >= 9) {
            this.f528a.edit().putString("imei", this.b).apply();
        } else {
            this.f528a.edit().putString("imei", this.b).commit();
        }
    }
}
