package com.baidu.mobads.j;

import java.io.File;

class h implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ String f524a;
    final /* synthetic */ g b;

    h(g gVar, String str) {
        this.b = gVar;
        this.f524a = str;
    }

    public void run() {
        String[] list = new File(this.f524a).list();
        if (list != null && list.length > 0) {
            for (String file : list) {
                File file2 = new File(file);
                long currentTimeMillis = System.currentTimeMillis() - file2.lastModified();
                if (file2.exists() && currentTimeMillis > 604800000) {
                    file2.delete();
                }
            }
        }
    }
}
