package com.baidu.mobads.j;

import android.util.Log;
import com.baidu.mobads.a.a;
import com.baidu.mobads.interfaces.utils.IXAdLogger;

public class j implements IXAdLogger {

    /* renamed from: a  reason: collision with root package name */
    private static volatile j f525a = null;

    public static j a() {
        if (f525a == null) {
            synchronized (j.class) {
                if (f525a == null) {
                    f525a = new j();
                }
            }
        }
        return f525a;
    }

    public boolean isLoggable(String str, int i) {
        return i >= a.b;
    }

    public boolean isLoggable(int i) {
        return isLoggable(IXAdLogger.TAG, i);
    }

    private String a(Object[] objArr) {
        StringBuilder sb = new StringBuilder();
        for (Object append : objArr) {
            sb.append(append).append(' ');
        }
        return sb.toString();
    }

    public int d(Object... objArr) {
        if (!isLoggable(3)) {
            return -1;
        }
        return d(a(objArr));
    }

    public int d(String str) {
        return d(IXAdLogger.TAG, str);
    }

    public int d(String str, String str2) {
        if (!isLoggable(3)) {
            return -1;
        }
        try {
            return Log.d(str, str2);
        } catch (Exception e) {
            return -1;
        }
    }

    public int d(Throwable th) {
        return d("", th);
    }

    public int d(String str, Throwable th) {
        if (!isLoggable(3)) {
            return -1;
        }
        try {
            return Log.d(IXAdLogger.TAG, str, th);
        } catch (Exception e) {
            return -1;
        }
    }

    public int w(String str) {
        if (!isLoggable(5)) {
            return -1;
        }
        try {
            return Log.w(IXAdLogger.TAG, str);
        } catch (Exception e) {
            return -1;
        }
    }

    public int w(Object... objArr) {
        if (!isLoggable(5)) {
            return -1;
        }
        return w(a(objArr));
    }

    public int w(String str, Throwable th) {
        if (!isLoggable(5)) {
            return -1;
        }
        try {
            return Log.w(IXAdLogger.TAG, str, th);
        } catch (Exception e) {
            return -1;
        }
    }

    public int w(Throwable th) {
        return w("", th);
    }

    public int e(Object... objArr) {
        if (!isLoggable(6)) {
            return -1;
        }
        return e(a(objArr));
    }

    public int e(String str) {
        if (!isLoggable(6)) {
            return -1;
        }
        try {
            return Log.e(IXAdLogger.TAG, str);
        } catch (Exception e) {
            return -1;
        }
    }

    public int e(Throwable th) {
        return e("", th);
    }

    public int e(String str, Throwable th) {
        if (!isLoggable(6)) {
            return -1;
        }
        try {
            return Log.e(IXAdLogger.TAG, str, th);
        } catch (Exception e) {
            return -1;
        }
    }

    public int i(String str) {
        return i(IXAdLogger.TAG, str);
    }

    public int i(String str, String str2) {
        if (!isLoggable(4)) {
            return -1;
        }
        try {
            return Log.i(str, str2);
        } catch (Exception e) {
            return -1;
        }
    }

    public int i(Object... objArr) {
        if (!isLoggable(4)) {
            return -1;
        }
        return i(a(objArr));
    }

    public int i(String str, Throwable th) {
        if (!isLoggable(4)) {
            return -1;
        }
        try {
            return Log.i(IXAdLogger.TAG, str, th);
        } catch (Exception e) {
            return -1;
        }
    }
}
