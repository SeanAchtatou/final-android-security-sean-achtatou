package com.baidu.mobads;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Build;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import com.baidu.mobads.j.j;
import com.xiaomi.mipush.sdk.MiPushClient;
import java.lang.reflect.Method;
import java.util.Locale;

public class ac extends af {

    /* renamed from: a  reason: collision with root package name */
    public String f449a = "";

    static class a {
        /* access modifiers changed from: private */

        /* renamed from: a  reason: collision with root package name */
        public static String[] f450a = "apk,zip,rar,7z,tar.gz,bz".split(MiPushClient.ACCEPT_TIME_SEPARATOR);
        /* access modifiers changed from: private */
        public static String[] b = "mp4,3gp,3g2,avi,rm,rmvb,wmv,flv,mkv,mov,asf,asx".split(MiPushClient.ACCEPT_TIME_SEPARATOR);
        /* access modifiers changed from: private */
        public static String[] c = "mp3,ra,wma,m4a,wav,aac,mmf,amr,ogg,adp".split(MiPushClient.ACCEPT_TIME_SEPARATOR);
        /* access modifiers changed from: private */
        public static String[] d = "sms,smsto,mms".split(MiPushClient.ACCEPT_TIME_SEPARATOR);
        /* access modifiers changed from: private */
        public static final String[] e = {"tel"};
        /* access modifiers changed from: private */
        public static final String[] f = {"mailto"};
        /* access modifiers changed from: private */
        public static String[] g = "http,https".split(MiPushClient.ACCEPT_TIME_SEPARATOR);
        /* access modifiers changed from: private */
        public static String[] h = "http,https,sms,smsto,mms,tel,fax,ftp,mailto,gopher,news,telnet,file".split(MiPushClient.ACCEPT_TIME_SEPARATOR);
    }

    @TargetApi(3)
    public ac(Context context, boolean z, boolean z2) {
        super(context);
        setClickable(true);
        if (z) {
            setBackgroundColor(0);
        }
        if (z2) {
            setFocusable(true);
        } else {
            setFocusable(false);
        }
        setScrollBarStyle(0);
        WebSettings settings = getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setJavaScriptCanOpenWindowsAutomatically(true);
        settings.setNeedInitialFocus(false);
        settings.setSupportZoom(true);
        settings.setBuiltInZoomControls(true);
        settings.setAllowFileAccess(true);
        setVerticalScrollBarEnabled(false);
        setHorizontalScrollBarEnabled(false);
        String path = context.getApplicationContext().getDir("database", 0).getPath();
        try {
            WebSettings.class.getMethod("setDatabasePath", String.class).invoke(settings, path);
            WebSettings.class.getMethod("setDomStorageEnabled", Boolean.TYPE).invoke(settings, true);
            WebSettings.class.getMethod("setDatabaseEnabled", Boolean.TYPE).invoke(settings, true);
        } catch (Exception e) {
        }
        Class<WebSettings> cls = WebSettings.class;
        try {
            cls.getMethod("setAppCacheEnabled", Boolean.TYPE).invoke(settings, true);
            WebSettings.class.getMethod("setAppCachePath", String.class).invoke(settings, path);
            WebSettings.class.getMethod("setAppCacheMaxSize", Long.TYPE).invoke(settings, 5242880L);
        } catch (Exception e2) {
        }
        Class<WebSettings> cls2 = WebSettings.class;
        try {
            cls2.getMethod("setGeolocationEnabled", Boolean.TYPE).invoke(settings, true);
            WebSettings.class.getMethod("setGeolocationDatabasePath", String.class).invoke(settings, path);
        } catch (Exception e3) {
        }
        setWebViewClient(new ad());
        setWebChromeClient(new WebChromeClient());
        try {
            if (Integer.parseInt(Build.VERSION.SDK) >= 14) {
                Method declaredMethod = Class.forName("android.view.View").getDeclaredMethod("setLayerType", Integer.TYPE, Paint.class);
                declaredMethod.setAccessible(true);
                declaredMethod.invoke(this, 1, null);
            }
        } catch (Exception e4) {
            j.a().e(e4);
        }
    }

    private static boolean a(String[] strArr, String str) {
        boolean z;
        if (str == null) {
            return false;
        }
        Uri parse = Uri.parse(str);
        String lowerCase = parse.getPath() == null ? "" : parse.getPath().toLowerCase(Locale.getDefault());
        int length = strArr.length;
        int i = 0;
        while (true) {
            if (i >= length) {
                z = false;
                break;
            }
            if (lowerCase.trim().endsWith("." + strArr[i])) {
                z = true;
                break;
            }
            i++;
        }
        return z;
    }

    private static boolean b(String[] strArr, String str) {
        if (str == null) {
            return false;
        }
        String lowerCase = str.toLowerCase(Locale.getDefault());
        int length = strArr.length;
        for (int i = 0; i < length; i++) {
            if (lowerCase.trim().startsWith(strArr[i] + ":")) {
                return true;
            }
        }
        return false;
    }

    public static boolean a(String str) {
        return !c(str) || g(str) || h(str) || i(str);
    }

    public static boolean b(String str) {
        return d(str) || e(str) || f(str) || (j(str) && (!c(str) || g(str) || h(str) || i(str)));
    }

    private static boolean j(String str) {
        return b(a.h, str);
    }

    public static boolean c(String str) {
        return b(a.g, str);
    }

    public static boolean d(String str) {
        return b(a.d, str);
    }

    public static boolean e(String str) {
        return b(a.e, str);
    }

    public static boolean f(String str) {
        return b(a.f, str);
    }

    public static boolean g(String str) {
        return a(a.f450a, str);
    }

    public static boolean h(String str) {
        return a(a.b, str);
    }

    public static boolean i(String str) {
        return a(a.c, str);
    }
}
