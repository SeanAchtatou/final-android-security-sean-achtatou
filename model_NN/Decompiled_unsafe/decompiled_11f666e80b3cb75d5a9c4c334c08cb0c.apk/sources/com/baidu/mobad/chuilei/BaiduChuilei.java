package com.baidu.mobad.chuilei;

import android.app.Activity;
import android.content.Context;
import com.baidu.mobad.feeds.BaiduNative;
import com.baidu.mobad.feeds.NativeErrorCode;
import com.baidu.mobad.feeds.NativeResponse;
import com.baidu.mobad.feeds.RequestParameters;
import com.baidu.mobads.production.b.b;
import java.util.ArrayList;
import java.util.List;

public class BaiduChuilei implements BaiduNative.BaiduNativeNetworkListener {

    /* renamed from: a  reason: collision with root package name */
    private b f396a;
    private BaiduChuileiNetworkListener b;
    private BaiduNative c;

    public interface BaiduChuileiNetworkListener {
        void onChuileiFail(BaiduChuileiErrorCode baiduChuileiErrorCode);

        void onChuileiLoad(List<BaiduChuileiResponse> list);
    }

    public BaiduChuilei(Context context, String str, BaiduChuileiNetworkListener baiduChuileiNetworkListener) {
        this.f396a = new b(context, str);
        this.b = baiduChuileiNetworkListener;
        this.c = new BaiduNative(context, str, this, this.f396a);
    }

    public void makeRequest(BaiduChuileiRequestParameters baiduChuileiRequestParameters) {
        RequestParameters requestParameters = baiduChuileiRequestParameters.getRequestParameters();
        if (requestParameters == null) {
            requestParameters = new RequestParameters.Builder().build();
        }
        this.c.makeRequest(requestParameters);
    }

    public void onNativeLoad(List<NativeResponse> list) {
        ArrayList arrayList = new ArrayList();
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < list.size()) {
                arrayList.add(new XAdChuileiResponse(list.get(i2)));
                i = i2 + 1;
            } else {
                this.b.onChuileiLoad(arrayList);
                return;
            }
        }
    }

    public void onNativeFail(NativeErrorCode nativeErrorCode) {
        this.b.onChuileiFail(BaiduChuileiErrorCode.LOAD_AD_FAILED);
    }

    public static void setAppSid(Activity activity, String str) {
        BaiduNative.setAppSid(activity, str);
    }
}
