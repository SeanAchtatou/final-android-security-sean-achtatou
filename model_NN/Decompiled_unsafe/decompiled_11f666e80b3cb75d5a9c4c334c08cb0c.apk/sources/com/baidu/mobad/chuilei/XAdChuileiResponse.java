package com.baidu.mobad.chuilei;

import android.view.View;
import com.baidu.mobad.feeds.BaiduNative;
import com.baidu.mobad.feeds.NativeResponse;
import com.baidu.mobad.feeds.XAdNativeResponse;
import com.baidu.mobads.interfaces.IXAdContainer;
import com.baidu.mobads.interfaces.IXAdInstanceInfo;
import com.baidu.mobads.interfaces.feeds.IXAdFeedsRequestParameters;

class XAdChuileiResponse implements BaiduChuileiResponse {

    /* renamed from: a  reason: collision with root package name */
    NativeResponse f399a;

    public XAdChuileiResponse(IXAdInstanceInfo iXAdInstanceInfo, BaiduNative baiduNative, IXAdFeedsRequestParameters iXAdFeedsRequestParameters, IXAdContainer iXAdContainer) {
        this.f399a = new XAdNativeResponse(iXAdInstanceInfo, baiduNative, iXAdFeedsRequestParameters, iXAdContainer);
    }

    public XAdChuileiResponse(NativeResponse nativeResponse) {
        this.f399a = nativeResponse;
    }

    public void recordImpression(View view) {
        this.f399a.recordImpression(view);
    }

    public void handleClick(View view) {
        this.f399a.handleClick(view);
    }

    public void handleClick(View view, int i) {
        this.f399a.handleClick(view, i);
    }

    public String getTitle() {
        return this.f399a.getTitle();
    }

    public String getImageUrl() {
        return this.f399a.getImageUrl();
    }
}
