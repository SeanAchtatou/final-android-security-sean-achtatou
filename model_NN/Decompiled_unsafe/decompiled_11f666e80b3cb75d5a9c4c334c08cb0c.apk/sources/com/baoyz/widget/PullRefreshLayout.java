package com.baoyz.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.os.Build;
import android.support.v4.view.MotionEventCompat;
import android.support.v4.view.ViewCompat;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.view.animation.Transformation;
import android.widget.AbsListView;
import android.widget.ImageView;
import com.baoyz.widget.c;
import com.ffcs.inapppaylib.bean.Constants;
import java.security.InvalidParameterException;
import org.apache.mina.proxy.handlers.http.ntlm.NTLMConstants;

public class PullRefreshLayout extends ViewGroup {

    /* renamed from: a  reason: collision with root package name */
    public int f609a;
    public int b;
    /* access modifiers changed from: private */
    public View c;
    /* access modifiers changed from: private */
    public ImageView d;
    private Interpolator e;
    private int f;
    /* access modifiers changed from: private */
    public int g;
    private int h;
    /* access modifiers changed from: private */
    public d i;
    /* access modifiers changed from: private */
    public int j;
    /* access modifiers changed from: private */
    public boolean k;
    private int l;
    private boolean m;
    private float n;
    /* access modifiers changed from: private */
    public int o;
    /* access modifiers changed from: private */
    public boolean p;
    /* access modifiers changed from: private */
    public a q;
    private int[] r;
    private int s;
    private boolean t;
    private float u;
    private final Animation v;
    private final Animation w;
    private Animation.AnimationListener x;
    private Animation.AnimationListener y;

    public interface a {
        void d_();
    }

    public PullRefreshLayout(Context context) {
        this(context, null);
    }

    public PullRefreshLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.v = new Animation() {
            public void applyTransformation(float f, Transformation transformation) {
                PullRefreshLayout.this.a(f);
            }
        };
        this.w = new Animation() {
            public void applyTransformation(float f, Transformation transformation) {
                int a2 = PullRefreshLayout.this.g;
                PullRefreshLayout.this.a((((int) (((float) (a2 - PullRefreshLayout.this.o)) * f)) + PullRefreshLayout.this.o) - PullRefreshLayout.this.c.getTop(), false);
            }
        };
        this.x = new Animation.AnimationListener() {
            public void onAnimationStart(Animation animation) {
                PullRefreshLayout.this.d.setVisibility(0);
            }

            public void onAnimationRepeat(Animation animation) {
            }

            public void onAnimationEnd(Animation animation) {
                if (PullRefreshLayout.this.k) {
                    PullRefreshLayout.this.i.start();
                    if (PullRefreshLayout.this.p && PullRefreshLayout.this.q != null) {
                        PullRefreshLayout.this.q.d_();
                    }
                } else {
                    PullRefreshLayout.this.i.stop();
                    PullRefreshLayout.this.d.setVisibility(8);
                    PullRefreshLayout.this.b();
                }
                int unused = PullRefreshLayout.this.j = PullRefreshLayout.this.c.getTop();
            }
        };
        this.y = new Animation.AnimationListener() {
            public void onAnimationStart(Animation animation) {
                PullRefreshLayout.this.i.stop();
            }

            public void onAnimationRepeat(Animation animation) {
            }

            public void onAnimationEnd(Animation animation) {
                PullRefreshLayout.this.d.setVisibility(8);
                int unused = PullRefreshLayout.this.j = PullRefreshLayout.this.c.getTop();
            }
        };
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, c.a.refresh_PullRefreshLayout);
        int integer = obtainStyledAttributes.getInteger(c.a.refresh_PullRefreshLayout_refreshType, 0);
        int resourceId = obtainStyledAttributes.getResourceId(c.a.refresh_PullRefreshLayout_refreshColors, 0);
        int resourceId2 = obtainStyledAttributes.getResourceId(c.a.refresh_PullRefreshLayout_refreshColor, 0);
        obtainStyledAttributes.recycle();
        this.e = new DecelerateInterpolator(2.0f);
        this.f = ViewConfiguration.get(context).getScaledTouchSlop();
        int integer2 = getResources().getInteger(17694721);
        this.f609a = integer2;
        this.b = integer2;
        int a2 = a(64);
        this.h = a2;
        this.g = a2;
        if (resourceId > 0) {
            this.r = context.getResources().getIntArray(resourceId);
        } else {
            this.r = new int[]{Color.rgb(201, 52, 55), Color.rgb(55, 91, 241), Color.rgb(247, (int) Constants.RESP_NO_PHONENUMBER, 62), Color.rgb(52, 163, 80)};
        }
        if (resourceId2 > 0) {
            this.r = new int[]{context.getResources().getColor(resourceId2)};
        }
        this.d = new ImageView(context);
        setRefreshStyle(integer);
        this.d.setVisibility(8);
        addView(this.d, 0);
        setWillNotDraw(false);
        ViewCompat.setChildrenDrawingOrderEnabled(this, true);
    }

    public void setColorSchemeColors(int... iArr) {
        this.r = iArr;
        this.i.a(iArr);
    }

    public void setColor(int i2) {
        setColorSchemeColors(i2);
    }

    public void setRefreshStyle(int i2) {
        setRefreshing(false);
        switch (i2) {
            case 0:
                this.i = new b(getContext(), this);
                break;
            case 1:
                this.i = new a(getContext(), this);
                break;
            case 2:
                this.i = new g(getContext(), this);
                break;
            case 3:
                this.i = new e(getContext(), this);
                break;
            case 4:
                this.i = new f(getContext(), this);
                break;
            default:
                throw new InvalidParameterException("Type does not exist");
        }
        this.i.a(this.r);
        this.d.setImageDrawable(this.i);
    }

    public void setRefreshDrawable(d dVar) {
        setRefreshing(false);
        this.i = dVar;
        this.i.a(this.r);
        this.d.setImageDrawable(this.i);
    }

    public int getFinalOffset() {
        return this.g;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        super.onMeasure(i2, i3);
        a();
        if (this.c != null) {
            int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec((getMeasuredWidth() - getPaddingRight()) - getPaddingLeft(), NTLMConstants.FLAG_NEGOTIATE_KEY_EXCHANGE);
            int makeMeasureSpec2 = View.MeasureSpec.makeMeasureSpec((getMeasuredHeight() - getPaddingTop()) - getPaddingBottom(), NTLMConstants.FLAG_NEGOTIATE_KEY_EXCHANGE);
            this.c.measure(makeMeasureSpec, makeMeasureSpec2);
            this.d.measure(makeMeasureSpec, makeMeasureSpec2);
        }
    }

    private void a() {
        if (this.c == null && getChildCount() > 0) {
            for (int i2 = 0; i2 < getChildCount(); i2++) {
                View childAt = getChildAt(i2);
                if (childAt != this.d) {
                    this.c = childAt;
                }
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baoyz.widget.PullRefreshLayout.a(int, boolean):void
     arg types: [int, int]
     candidates:
      com.baoyz.widget.PullRefreshLayout.a(android.view.MotionEvent, int):float
      com.baoyz.widget.PullRefreshLayout.a(com.baoyz.widget.PullRefreshLayout, int):int
      com.baoyz.widget.PullRefreshLayout.a(com.baoyz.widget.PullRefreshLayout, float):void
      com.baoyz.widget.PullRefreshLayout.a(boolean, boolean):void
      com.baoyz.widget.PullRefreshLayout.a(int, boolean):void */
    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        boolean z = false;
        if (!isEnabled()) {
            return false;
        }
        if (d() && !this.k) {
            return false;
        }
        switch (MotionEventCompat.getActionMasked(motionEvent)) {
            case 0:
                if (!this.k) {
                    a(0, true);
                }
                this.l = MotionEventCompat.getPointerId(motionEvent, 0);
                this.m = false;
                float a2 = a(motionEvent, this.l);
                if (a2 != -1.0f) {
                    this.n = a2;
                    this.s = this.j;
                    this.t = false;
                    this.u = 0.0f;
                    break;
                } else {
                    return false;
                }
            case 1:
            case 3:
                this.m = false;
                this.l = -1;
                break;
            case 2:
                if (this.l == -1) {
                    return false;
                }
                float a3 = a(motionEvent, this.l);
                if (a3 != -1.0f) {
                    float f2 = a3 - this.n;
                    if (!this.k) {
                        if (f2 > ((float) this.f) && !this.m) {
                            this.m = true;
                            break;
                        }
                    } else {
                        if (f2 >= 0.0f || this.j > 0) {
                            z = true;
                        }
                        this.m = z;
                        break;
                    }
                } else {
                    return false;
                }
            case 6:
                a(motionEvent);
                break;
        }
        return this.m;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baoyz.widget.PullRefreshLayout.a(int, boolean):void
     arg types: [int, int]
     candidates:
      com.baoyz.widget.PullRefreshLayout.a(android.view.MotionEvent, int):float
      com.baoyz.widget.PullRefreshLayout.a(com.baoyz.widget.PullRefreshLayout, int):int
      com.baoyz.widget.PullRefreshLayout.a(com.baoyz.widget.PullRefreshLayout, float):void
      com.baoyz.widget.PullRefreshLayout.a(boolean, boolean):void
      com.baoyz.widget.PullRefreshLayout.a(int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(long, long):long}
      ClspMth{java.lang.Math.max(float, float):float} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baoyz.widget.PullRefreshLayout.a(boolean, boolean):void
     arg types: [int, int]
     candidates:
      com.baoyz.widget.PullRefreshLayout.a(android.view.MotionEvent, int):float
      com.baoyz.widget.PullRefreshLayout.a(com.baoyz.widget.PullRefreshLayout, int):int
      com.baoyz.widget.PullRefreshLayout.a(int, boolean):void
      com.baoyz.widget.PullRefreshLayout.a(com.baoyz.widget.PullRefreshLayout, float):void
      com.baoyz.widget.PullRefreshLayout.a(boolean, boolean):void */
    public boolean onTouchEvent(MotionEvent motionEvent) {
        int i2 = -1;
        if (!this.m) {
            return super.onTouchEvent(motionEvent);
        }
        switch (MotionEventCompat.getActionMasked(motionEvent)) {
            case 1:
            case 3:
                if (this.l == -1) {
                    return false;
                }
                if (!this.k) {
                    this.m = false;
                    if ((MotionEventCompat.getY(motionEvent, MotionEventCompat.findPointerIndex(motionEvent, this.l)) - this.n) * 0.5f > ((float) this.h)) {
                        a(true, true);
                    } else {
                        this.k = false;
                        b();
                    }
                    this.l = -1;
                    return false;
                } else if (!this.t) {
                    return false;
                } else {
                    this.c.dispatchTouchEvent(motionEvent);
                    this.t = false;
                    return false;
                }
            case 2:
                int findPointerIndex = MotionEventCompat.findPointerIndex(motionEvent, this.l);
                if (findPointerIndex >= 0) {
                    float y2 = MotionEventCompat.getY(motionEvent, findPointerIndex);
                    float f2 = y2 - this.n;
                    if (this.k) {
                        int i3 = (int) (f2 + ((float) this.s));
                        if (d()) {
                            this.n = y2;
                            this.s = 0;
                            if (this.t) {
                                this.c.dispatchTouchEvent(motionEvent);
                            } else {
                                MotionEvent obtain = MotionEvent.obtain(motionEvent);
                                obtain.setAction(0);
                                this.t = true;
                                this.c.dispatchTouchEvent(obtain);
                            }
                        } else if (i3 < 0) {
                            if (this.t) {
                                this.c.dispatchTouchEvent(motionEvent);
                            } else {
                                MotionEvent obtain2 = MotionEvent.obtain(motionEvent);
                                obtain2.setAction(0);
                                this.t = true;
                                this.c.dispatchTouchEvent(obtain2);
                            }
                            i2 = 0;
                        } else if (i3 > this.h) {
                            i2 = this.h;
                        } else if (this.t) {
                            MotionEvent obtain3 = MotionEvent.obtain(motionEvent);
                            obtain3.setAction(3);
                            this.t = false;
                            this.c.dispatchTouchEvent(obtain3);
                            i2 = i3;
                        } else {
                            i2 = i3;
                        }
                    } else {
                        float f3 = f2 * 0.5f;
                        float f4 = f3 / ((float) this.h);
                        if (f4 < 0.0f) {
                            return false;
                        }
                        this.u = Math.min(1.0f, Math.abs(f4));
                        float abs = Math.abs(f3) - ((float) this.h);
                        float f5 = (float) this.g;
                        float max = Math.max(0.0f, Math.min(abs, f5 * 2.0f) / f5);
                        i2 = (int) ((((float) (((double) (max / 4.0f)) - Math.pow((double) (max / 4.0f), 2.0d))) * 2.0f * f5 * 2.0f) + (f5 * this.u));
                        if (this.d.getVisibility() != 0) {
                            this.d.setVisibility(0);
                        }
                        if (f3 < ((float) this.h)) {
                            this.i.a(this.u);
                        }
                    }
                    a(i2 - this.j, true);
                    break;
                } else {
                    return false;
                }
            case 5:
                this.l = MotionEventCompat.getPointerId(motionEvent, MotionEventCompat.getActionIndex(motionEvent));
                break;
            case 6:
                a(motionEvent);
                break;
        }
        return true;
    }

    /* access modifiers changed from: private */
    public void b() {
        this.o = this.j;
        this.v.reset();
        this.v.setDuration((long) this.f609a);
        this.v.setInterpolator(this.e);
        this.v.setAnimationListener(this.y);
        this.d.clearAnimation();
        this.d.startAnimation(this.v);
    }

    private void c() {
        this.o = this.j;
        this.w.reset();
        this.w.setDuration((long) this.b);
        this.w.setInterpolator(this.e);
        this.w.setAnimationListener(this.x);
        this.d.clearAnimation();
        this.d.startAnimation(this.w);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baoyz.widget.PullRefreshLayout.a(int, boolean):void
     arg types: [int, int]
     candidates:
      com.baoyz.widget.PullRefreshLayout.a(android.view.MotionEvent, int):float
      com.baoyz.widget.PullRefreshLayout.a(com.baoyz.widget.PullRefreshLayout, int):int
      com.baoyz.widget.PullRefreshLayout.a(com.baoyz.widget.PullRefreshLayout, float):void
      com.baoyz.widget.PullRefreshLayout.a(boolean, boolean):void
      com.baoyz.widget.PullRefreshLayout.a(int, boolean):void */
    /* access modifiers changed from: private */
    public void a(float f2) {
        a((this.o - ((int) (((float) this.o) * f2))) - this.c.getTop(), false);
        this.i.a(this.u * (1.0f - f2));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baoyz.widget.PullRefreshLayout.a(boolean, boolean):void
     arg types: [boolean, int]
     candidates:
      com.baoyz.widget.PullRefreshLayout.a(android.view.MotionEvent, int):float
      com.baoyz.widget.PullRefreshLayout.a(com.baoyz.widget.PullRefreshLayout, int):int
      com.baoyz.widget.PullRefreshLayout.a(int, boolean):void
      com.baoyz.widget.PullRefreshLayout.a(com.baoyz.widget.PullRefreshLayout, float):void
      com.baoyz.widget.PullRefreshLayout.a(boolean, boolean):void */
    public void setRefreshing(boolean z) {
        if (this.k != z) {
            a(z, false);
        }
    }

    private void a(boolean z, boolean z2) {
        if (this.k != z) {
            this.p = z2;
            a();
            this.k = z;
            if (this.k) {
                this.i.a(1.0f);
                c();
                return;
            }
            b();
        }
    }

    private void a(MotionEvent motionEvent) {
        int actionIndex = MotionEventCompat.getActionIndex(motionEvent);
        if (MotionEventCompat.getPointerId(motionEvent, actionIndex) == this.l) {
            this.l = MotionEventCompat.getPointerId(motionEvent, actionIndex == 0 ? 1 : 0);
        }
    }

    private float a(MotionEvent motionEvent, int i2) {
        int findPointerIndex = MotionEventCompat.findPointerIndex(motionEvent, i2);
        if (findPointerIndex < 0) {
            return -1.0f;
        }
        return MotionEventCompat.getY(motionEvent, findPointerIndex);
    }

    /* access modifiers changed from: private */
    public void a(int i2, boolean z) {
        this.c.offsetTopAndBottom(i2);
        this.j = this.c.getTop();
        this.i.a(i2);
        if (z && Build.VERSION.SDK_INT < 11) {
            invalidate();
        }
    }

    private boolean d() {
        boolean z = true;
        if (Build.VERSION.SDK_INT >= 14) {
            return ViewCompat.canScrollVertically(this.c, -1);
        }
        if (this.c instanceof AbsListView) {
            AbsListView absListView = (AbsListView) this.c;
            return absListView.getChildCount() > 0 && (absListView.getFirstVisiblePosition() > 0 || absListView.getChildAt(0).getTop() < absListView.getPaddingTop());
        }
        if (this.c.getScrollY() <= 0) {
            z = false;
        }
        return z;
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i2, int i3, int i4, int i5) {
        a();
        if (this.c != null) {
            int measuredHeight = getMeasuredHeight();
            int measuredWidth = getMeasuredWidth();
            int paddingLeft = getPaddingLeft();
            int paddingTop = getPaddingTop();
            int paddingRight = getPaddingRight();
            int paddingBottom = getPaddingBottom();
            this.c.layout(paddingLeft, this.c.getTop() + paddingTop, (paddingLeft + measuredWidth) - paddingRight, ((paddingTop + measuredHeight) - paddingBottom) + this.c.getTop());
            this.d.layout(paddingLeft, paddingTop, (measuredWidth + paddingLeft) - paddingRight, (measuredHeight + paddingTop) - paddingBottom);
        }
    }

    private int a(int i2) {
        return (int) TypedValue.applyDimension(1, (float) i2, getContext().getResources().getDisplayMetrics());
    }

    public void setOnRefreshListener(a aVar) {
        this.q = aVar;
    }
}
