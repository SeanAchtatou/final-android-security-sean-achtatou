package com.baoyz.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.Handler;
import android.util.TypedValue;

/* compiled from: RingDrawable */
class e extends d {

    /* renamed from: a  reason: collision with root package name */
    private boolean f626a;
    private RectF b;
    private int c;
    private int d;
    private Paint e = new Paint(1);
    private Path f;
    private float g;
    private int[] h;
    private Handler i = new Handler();
    private int j;
    private float k;

    e(Context context, PullRefreshLayout pullRefreshLayout) {
        super(context, pullRefreshLayout);
        this.e.setStyle(Paint.Style.STROKE);
        this.e.setStrokeWidth((float) c(3));
        this.e.setStrokeCap(Paint.Cap.ROUND);
        this.f = new Path();
    }

    public void a(float f2) {
        this.e.setColor(a(f2, this.h[0], this.h[1]));
        this.g = 340.0f * f2;
    }

    public void a(int[] iArr) {
        this.h = iArr;
    }

    public void a(int i2) {
        invalidateSelf();
    }

    public void start() {
        this.j = 50;
        this.f626a = true;
        invalidateSelf();
    }

    private void b(int i2) {
        int i3 = (i2 == 200 ? 0 : i2) / 50;
        float f2 = ((float) (i2 % 50)) / 50.0f;
        this.e.setColor(a(f2, this.h[i3], this.h[(i3 + 1) % this.h.length]));
        this.k = 360.0f * f2;
    }

    public void stop() {
        this.f626a = false;
        this.k = 0.0f;
    }

    public boolean isRunning() {
        return this.f626a;
    }

    /* access modifiers changed from: protected */
    public void onBoundsChange(Rect rect) {
        super.onBoundsChange(rect);
        this.c = d().getFinalOffset();
        this.d = this.c;
        this.b = new RectF((float) ((rect.width() / 2) - (this.c / 2)), (float) rect.top, (float) ((rect.width() / 2) + (this.c / 2)), (float) (rect.top + this.d));
        this.b.inset((float) c(15), (float) c(15));
    }

    public void draw(Canvas canvas) {
        canvas.save();
        canvas.rotate(this.k, this.b.centerX(), this.b.centerY());
        a(canvas);
        canvas.restore();
        if (this.f626a) {
            this.j = this.j >= 200 ? 0 : this.j + 1;
            b(this.j);
            invalidateSelf();
        }
    }

    private void a(Canvas canvas) {
        this.f.reset();
        this.f.arcTo(this.b, 270.0f, this.g, true);
        canvas.drawPath(this.f, this.e);
    }

    private int c(int i2) {
        return (int) TypedValue.applyDimension(1, (float) i2, c().getResources().getDisplayMetrics());
    }

    private int a(float f2, int i2, int i3) {
        int i4 = (i2 >> 24) & 255;
        int i5 = (i2 >> 16) & 255;
        int i6 = (i2 >> 8) & 255;
        int i7 = i2 & 255;
        return ((i4 + ((int) (((float) (((i3 >> 24) & 255) - i4)) * f2))) << 24) | ((i5 + ((int) (((float) (((i3 >> 16) & 255) - i5)) * f2))) << 16) | ((((int) (((float) (((i3 >> 8) & 255) - i6)) * f2)) + i6) << 8) | (((int) (((float) ((i3 & 255) - i7)) * f2)) + i7);
    }
}
