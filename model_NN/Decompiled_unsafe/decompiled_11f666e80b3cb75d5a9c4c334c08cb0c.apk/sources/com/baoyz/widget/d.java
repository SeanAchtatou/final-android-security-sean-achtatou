package com.baoyz.widget;

import android.content.Context;
import android.graphics.ColorFilter;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;

/* compiled from: RefreshDrawable */
public abstract class d extends Drawable implements Animatable, Drawable.Callback {

    /* renamed from: a  reason: collision with root package name */
    private PullRefreshLayout f625a;

    public abstract void a(float f);

    public abstract void a(int i);

    public abstract void a(int[] iArr);

    public d(Context context, PullRefreshLayout pullRefreshLayout) {
        this.f625a = pullRefreshLayout;
    }

    public Context c() {
        if (this.f625a != null) {
            return this.f625a.getContext();
        }
        return null;
    }

    public PullRefreshLayout d() {
        return this.f625a;
    }

    public void invalidateDrawable(Drawable drawable) {
        Drawable.Callback callback = getCallback();
        if (callback != null) {
            callback.invalidateDrawable(this);
        }
    }

    public void scheduleDrawable(Drawable drawable, Runnable runnable, long j) {
        Drawable.Callback callback = getCallback();
        if (callback != null) {
            callback.scheduleDrawable(this, runnable, j);
        }
    }

    public void unscheduleDrawable(Drawable drawable, Runnable runnable) {
        Drawable.Callback callback = getCallback();
        if (callback != null) {
            callback.unscheduleDrawable(this, runnable);
        }
    }

    public int getOpacity() {
        return -3;
    }

    public void setAlpha(int i) {
    }

    public void setColorFilter(ColorFilter colorFilter) {
    }
}
