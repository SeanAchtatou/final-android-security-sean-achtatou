package com.qq.e.ads.cfg;

public enum BannerRollAnimation {
    Default(0),
    NoAnimation(-1);
    

    /* renamed from: a  reason: collision with root package name */
    private final int f1205a;

    private BannerRollAnimation(int i) {
        this.f1205a = i;
    }

    public final int value() {
        return this.f1205a;
    }
}
