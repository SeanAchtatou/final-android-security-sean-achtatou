package com.qq.e.ads.cfg;

public enum DownAPPConfirmPolicy {
    Default(0),
    NOConfirm(2);
    

    /* renamed from: a  reason: collision with root package name */
    private final int f1207a;

    private DownAPPConfirmPolicy(int i) {
        this.f1207a = i;
    }

    public final int value() {
        return this.f1207a;
    }
}
