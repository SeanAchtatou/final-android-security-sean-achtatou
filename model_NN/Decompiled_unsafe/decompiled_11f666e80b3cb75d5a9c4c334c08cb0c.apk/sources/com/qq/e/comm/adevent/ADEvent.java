package com.qq.e.comm.adevent;

public class ADEvent {

    /* renamed from: a  reason: collision with root package name */
    private final int f1222a;
    private final Object[] b;

    public ADEvent(int i) {
        this(i, null);
    }

    public ADEvent(int i, Object[] objArr) {
        this.f1222a = i;
        this.b = objArr;
    }

    public Object[] getParas() {
        return this.b == null ? new Object[0] : this.b;
    }

    public int getType() {
        return this.f1222a;
    }
}
