package com.qq.e.comm.net.rr;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.util.Base64;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.util.Arrays;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import org.apache.mina.proxy.handlers.socks.SocksProxyConstants;

public final class a {

    /* renamed from: a  reason: collision with root package name */
    private static final byte[] f1246a = {SocksProxyConstants.V4_REPLY_REQUEST_REJECTED_OR_FAILED, -62};
    private static Cipher b = null;
    private static Cipher c = null;
    private static byte[] d = Base64.decode("4M3PpUC4Vu1uMp+Y0Mxd+vfc6v4ggJAINfgTlH74pis=", 0);

    /* renamed from: com.qq.e.comm.net.rr.a$a  reason: collision with other inner class name */
    static class C0035a extends Exception {
        public C0035a(String str, Throwable th) {
            super(str, th);
        }
    }

    public static class b extends Exception {
        public b(String str, Throwable th) {
            super(str, th);
        }
    }

    @SuppressLint({"TrulyRandom"})
    private static synchronized Cipher a() throws C0035a {
        Cipher cipher;
        synchronized (a.class) {
            if (b != null) {
                cipher = b;
            } else {
                try {
                    Cipher instance = Cipher.getInstance("AES/ECB/PKCS7Padding");
                    instance.init(1, new SecretKeySpec(d, "AES"));
                    b = instance;
                    cipher = b;
                } catch (Exception e) {
                    throw new C0035a("Fail To Init Cipher", e);
                }
            }
        }
        return cipher;
    }

    public static byte[] a(byte[] bArr) throws b {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        DataOutputStream dataOutputStream = new DataOutputStream(byteArrayOutputStream);
        try {
            dataOutputStream.write(f1246a);
            dataOutputStream.writeByte(1);
            dataOutputStream.writeByte(2);
            dataOutputStream.write(c(com.qq.e.comm.a.a(bArr)));
            dataOutputStream.close();
            return byteArrayOutputStream.toByteArray();
        } catch (Exception e) {
            throw new b("Exception while packaging byte array", e);
        }
    }

    private static synchronized Cipher b() throws C0035a {
        Cipher cipher;
        synchronized (a.class) {
            if (c != null) {
                cipher = c;
            } else {
                try {
                    Cipher instance = Cipher.getInstance("AES/ECB/PKCS7Padding");
                    instance.init(2, new SecretKeySpec(d, "AES"));
                    c = instance;
                    cipher = c;
                } catch (Exception e) {
                    throw new C0035a("Fail To Init Cipher", e);
                }
            }
        }
        return cipher;
    }

    @TargetApi(9)
    public static byte[] b(byte[] bArr) throws b {
        if (bArr == null || bArr.length < 4) {
            throw new b("S2SS Package FormatError", null);
        }
        try {
            byte[] bArr2 = new byte[4];
            new DataInputStream(new ByteArrayInputStream(bArr)).read(bArr2);
            if (f1246a[0] == bArr2[0] && f1246a[1] == bArr2[1] && 1 == bArr2[2] && 2 == bArr2[3]) {
                return com.qq.e.comm.a.b(d(Arrays.copyOfRange(bArr, 4, bArr.length)));
            }
            throw new b("S2SS Package Magic/Version FormatError", null);
        } catch (Exception e) {
            throw new b("Exception while packaging byte array", e);
        }
    }

    private static byte[] c(byte[] bArr) throws C0035a {
        try {
            return a().doFinal(bArr);
        } catch (Exception e) {
            throw new C0035a("Exception While encrypt byte array", e);
        }
    }

    private static byte[] d(byte[] bArr) throws C0035a {
        try {
            return b().doFinal(bArr);
        } catch (Exception e) {
            throw new C0035a("Exception While dencrypt byte array", e);
        }
    }
}
