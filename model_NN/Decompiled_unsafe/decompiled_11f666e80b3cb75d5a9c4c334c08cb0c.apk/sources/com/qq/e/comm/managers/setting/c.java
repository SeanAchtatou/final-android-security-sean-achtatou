package com.qq.e.comm.managers.setting;

import com.qq.e.comm.util.GDTLogger;
import com.qq.e.comm.util.StringUtil;
import org.json.JSONException;
import org.json.JSONObject;

public class c {

    /* renamed from: a  reason: collision with root package name */
    private JSONObject f1231a;

    public c() {
        this.f1231a = new JSONObject();
    }

    public c(String str) {
        this();
        GDTLogger.d("Initialize GDTSDKSetting,Json=" + str);
        if (!StringUtil.isEmpty(str)) {
            try {
                this.f1231a = new JSONObject(str);
            } catch (JSONException e) {
                GDTLogger.report("Exception while building GDTSDKSetting from json", e);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final Object a(String str) {
        return this.f1231a.opt(str);
    }

    /* access modifiers changed from: package-private */
    public final void a(String str, Object obj) {
        try {
            this.f1231a.putOpt(str, obj);
        } catch (JSONException e) {
            GDTLogger.e("Exception while update setting", e);
        }
    }

    public String toString() {
        return "GDTSDKSetting[" + this.f1231a.toString() + "]";
    }
}
