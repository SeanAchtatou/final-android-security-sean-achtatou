package com.tencent.wxop.stat.b;

import android.util.Base64;

public final class g {
    /* JADX WARN: Type inference failed for: r9v0, types: [byte[]] */
    /* JADX WARN: Type inference failed for: r5v13, types: [byte] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static byte[] a(byte[] r8, byte[] r9) {
        /*
            r7 = 256(0x100, float:3.59E-43)
            r0 = 0
            int[] r3 = new int[r7]
            int[] r4 = new int[r7]
            int r2 = r9.length
            if (r2 <= 0) goto L_0x000c
            if (r2 <= r7) goto L_0x0014
        L_0x000c:
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "key must be between 1 and 256 bytes"
            r0.<init>(r1)
            throw r0
        L_0x0014:
            r1 = r0
        L_0x0015:
            if (r1 >= r7) goto L_0x0022
            r3[r1] = r1
            int r5 = r1 % r2
            byte r5 = r9[r5]
            r4[r1] = r5
            int r1 = r1 + 1
            goto L_0x0015
        L_0x0022:
            r1 = r0
            r2 = r0
        L_0x0024:
            if (r2 >= r7) goto L_0x0039
            r5 = r3[r2]
            int r1 = r1 + r5
            r5 = r4[r2]
            int r1 = r1 + r5
            r1 = r1 & 255(0xff, float:3.57E-43)
            r5 = r3[r2]
            r6 = r3[r1]
            r3[r2] = r6
            r3[r1] = r5
            int r2 = r2 + 1
            goto L_0x0024
        L_0x0039:
            int r1 = r8.length
            byte[] r4 = new byte[r1]
            r1 = r0
            r2 = r0
        L_0x003e:
            int r5 = r8.length
            if (r0 >= r5) goto L_0x0064
            int r1 = r1 + 1
            r1 = r1 & 255(0xff, float:3.57E-43)
            r5 = r3[r1]
            int r2 = r2 + r5
            r2 = r2 & 255(0xff, float:3.57E-43)
            r5 = r3[r1]
            r6 = r3[r2]
            r3[r1] = r6
            r3[r2] = r5
            r5 = r3[r1]
            r6 = r3[r2]
            int r5 = r5 + r6
            r5 = r5 & 255(0xff, float:3.57E-43)
            r5 = r3[r5]
            byte r6 = r8[r0]
            r5 = r5 ^ r6
            byte r5 = (byte) r5
            r4[r0] = r5
            int r0 = r0 + 1
            goto L_0x003e
        L_0x0064:
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.wxop.stat.b.g.a(byte[], byte[]):byte[]");
    }

    public static byte[] b(byte[] bArr) {
        return a(bArr, Base64.decode("MDNhOTc2NTExZTJjYmUzYTdmMjY4MDhmYjdhZjNjMDU=", 0));
    }

    public static byte[] c(byte[] bArr) {
        return a(bArr, Base64.decode("MDNhOTc2NTExZTJjYmUzYTdmMjY4MDhmYjdhZjNjMDU=", 0));
    }
}
