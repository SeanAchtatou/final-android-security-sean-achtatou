package com.tencent.wxop.stat;

import android.content.ContentValues;
import android.content.Context;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import com.meizu.cloud.pushsdk.pushtracer.storage.EventStoreHelper;
import com.tencent.wxop.stat.a.d;
import com.tencent.wxop.stat.b.b;
import com.tencent.wxop.stat.b.c;
import com.tencent.wxop.stat.b.f;
import com.tencent.wxop.stat.b.l;
import com.xiaomi.mipush.sdk.MiPushClient;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

public class t {
    /* access modifiers changed from: private */
    public static b bZ = l.av();
    private static Context ca = null;
    private static t cb = null;
    volatile int aI = 0;
    private String ab = "";
    private ac bW = null;
    private ac bX = null;
    c bY = null;
    private f be = null;
    private String bq = "";
    private int cc = 0;
    private ConcurrentHashMap<d, String> cd = null;
    private boolean ce = false;
    private HashMap<String, String> cf = new HashMap<>();

    private t(Context context) {
        try {
            this.be = new f();
            ca = context.getApplicationContext();
            this.cd = new ConcurrentHashMap<>();
            this.ab = l.J(context);
            this.bq = "pri_" + l.J(context);
            this.bW = new ac(ca, this.ab);
            this.bX = new ac(ca, this.bq);
            b(true);
            b(false);
            aj();
            t(ca);
            I();
            an();
        } catch (Throwable th) {
            bZ.b(th);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x005b  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void I() {
        /*
            r9 = this;
            r8 = 0
            com.tencent.wxop.stat.ac r0 = r9.bW     // Catch:{ Throwable -> 0x0061, all -> 0x0057 }
            android.database.sqlite.SQLiteDatabase r0 = r0.getReadableDatabase()     // Catch:{ Throwable -> 0x0061, all -> 0x0057 }
            java.lang.String r1 = "config"
            r2 = 0
            r3 = 0
            r4 = 0
            r5 = 0
            r6 = 0
            r7 = 0
            android.database.Cursor r1 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ Throwable -> 0x0061, all -> 0x0057 }
        L_0x0013:
            boolean r0 = r1.moveToNext()     // Catch:{ Throwable -> 0x0045 }
            if (r0 == 0) goto L_0x0051
            r0 = 0
            int r0 = r1.getInt(r0)     // Catch:{ Throwable -> 0x0045 }
            r2 = 1
            java.lang.String r2 = r1.getString(r2)     // Catch:{ Throwable -> 0x0045 }
            r3 = 2
            java.lang.String r3 = r1.getString(r3)     // Catch:{ Throwable -> 0x0045 }
            r4 = 3
            int r4 = r1.getInt(r4)     // Catch:{ Throwable -> 0x0045 }
            com.tencent.wxop.stat.ah r5 = new com.tencent.wxop.stat.ah     // Catch:{ Throwable -> 0x0045 }
            r5.<init>(r0)     // Catch:{ Throwable -> 0x0045 }
            r5.aI = r0     // Catch:{ Throwable -> 0x0045 }
            org.json.JSONObject r0 = new org.json.JSONObject     // Catch:{ Throwable -> 0x0045 }
            r0.<init>(r2)     // Catch:{ Throwable -> 0x0045 }
            r5.df = r0     // Catch:{ Throwable -> 0x0045 }
            r5.c = r3     // Catch:{ Throwable -> 0x0045 }
            r5.L = r4     // Catch:{ Throwable -> 0x0045 }
            android.content.Context r0 = com.tencent.wxop.stat.t.ca     // Catch:{ Throwable -> 0x0045 }
            com.tencent.wxop.stat.c.a(r0, r5)     // Catch:{ Throwable -> 0x0045 }
            goto L_0x0013
        L_0x0045:
            r0 = move-exception
        L_0x0046:
            com.tencent.wxop.stat.b.b r2 = com.tencent.wxop.stat.t.bZ     // Catch:{ all -> 0x005f }
            r2.b(r0)     // Catch:{ all -> 0x005f }
            if (r1 == 0) goto L_0x0050
            r1.close()
        L_0x0050:
            return
        L_0x0051:
            if (r1 == 0) goto L_0x0050
            r1.close()
            goto L_0x0050
        L_0x0057:
            r0 = move-exception
            r1 = r8
        L_0x0059:
            if (r1 == 0) goto L_0x005e
            r1.close()
        L_0x005e:
            throw r0
        L_0x005f:
            r0 = move-exception
            goto L_0x0059
        L_0x0061:
            r0 = move-exception
            r1 = r8
            goto L_0x0046
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.wxop.stat.t.I():void");
    }

    private synchronized void a(int i, boolean z) {
        try {
            if (this.aI > 0 && i > 0 && !e.a()) {
                if (c.k()) {
                    bZ.b("Load " + this.aI + " unsent events");
                }
                ArrayList arrayList = new ArrayList(i);
                b(arrayList, i, z);
                if (arrayList.size() > 0) {
                    if (c.k()) {
                        bZ.b("Peek " + arrayList.size() + " unsent events.");
                    }
                    a(arrayList, 2, z);
                    ak.Z(ca).b(arrayList, new aa(this, arrayList, z));
                }
            }
        } catch (Throwable th) {
            bZ.b(th);
        }
        return;
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x00c7, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:?, code lost:
        com.tencent.wxop.stat.t.bZ.b(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x00cd, code lost:
        r0 = r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x00f0, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:?, code lost:
        com.tencent.wxop.stat.t.bZ.b(r1);
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [B:20:0x0092, B:51:0x00e9] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x009c A[SYNTHETIC, Splitter:B:25:0x009c] */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x00f7 A[Catch:{ Throwable -> 0x00f0, Throwable -> 0x00c7 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void a(com.tencent.wxop.stat.a.d r7, com.tencent.wxop.stat.aj r8, boolean r9, boolean r10) {
        /*
            r6 = this;
            r1 = 0
            monitor-enter(r6)
            int r0 = com.tencent.wxop.stat.c.s()     // Catch:{ all -> 0x00ed }
            if (r0 <= 0) goto L_0x00c5
            int r0 = com.tencent.wxop.stat.c.ay     // Catch:{ all -> 0x00ed }
            if (r0 <= 0) goto L_0x0010
            if (r9 != 0) goto L_0x0010
            if (r10 == 0) goto L_0x0110
        L_0x0010:
            android.database.sqlite.SQLiteDatabase r1 = r6.c(r9)     // Catch:{ Throwable -> 0x00cf }
            r1.beginTransaction()     // Catch:{ Throwable -> 0x00cf }
            if (r9 != 0) goto L_0x003c
            int r0 = r6.aI     // Catch:{ Throwable -> 0x00cf }
            int r2 = com.tencent.wxop.stat.c.s()     // Catch:{ Throwable -> 0x00cf }
            if (r0 <= r2) goto L_0x003c
            com.tencent.wxop.stat.b.b r0 = com.tencent.wxop.stat.t.bZ     // Catch:{ Throwable -> 0x00cf }
            java.lang.String r2 = "Too many events stored in db."
            r0.warn(r2)     // Catch:{ Throwable -> 0x00cf }
            int r0 = r6.aI     // Catch:{ Throwable -> 0x00cf }
            com.tencent.wxop.stat.ac r2 = r6.bW     // Catch:{ Throwable -> 0x00cf }
            android.database.sqlite.SQLiteDatabase r2 = r2.getWritableDatabase()     // Catch:{ Throwable -> 0x00cf }
            java.lang.String r3 = "events"
            java.lang.String r4 = "event_id in (select event_id from events where timestamp in (select min(timestamp) from events) limit 1)"
            r5 = 0
            int r2 = r2.delete(r3, r4, r5)     // Catch:{ Throwable -> 0x00cf }
            int r0 = r0 - r2
            r6.aI = r0     // Catch:{ Throwable -> 0x00cf }
        L_0x003c:
            android.content.ContentValues r0 = new android.content.ContentValues     // Catch:{ Throwable -> 0x00cf }
            r0.<init>()     // Catch:{ Throwable -> 0x00cf }
            java.lang.String r2 = r7.af()     // Catch:{ Throwable -> 0x00cf }
            boolean r3 = com.tencent.wxop.stat.c.k()     // Catch:{ Throwable -> 0x00cf }
            if (r3 == 0) goto L_0x005f
            com.tencent.wxop.stat.b.b r3 = com.tencent.wxop.stat.t.bZ     // Catch:{ Throwable -> 0x00cf }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x00cf }
            java.lang.String r5 = "insert 1 event, content:"
            r4.<init>(r5)     // Catch:{ Throwable -> 0x00cf }
            java.lang.StringBuilder r4 = r4.append(r2)     // Catch:{ Throwable -> 0x00cf }
            java.lang.String r4 = r4.toString()     // Catch:{ Throwable -> 0x00cf }
            r3.b(r4)     // Catch:{ Throwable -> 0x00cf }
        L_0x005f:
            java.lang.String r2 = com.tencent.wxop.stat.b.r.q(r2)     // Catch:{ Throwable -> 0x00cf }
            java.lang.String r3 = "content"
            r0.put(r3, r2)     // Catch:{ Throwable -> 0x00cf }
            java.lang.String r2 = "send_count"
            java.lang.String r3 = "0"
            r0.put(r2, r3)     // Catch:{ Throwable -> 0x00cf }
            java.lang.String r2 = "status"
            r3 = 1
            java.lang.String r3 = java.lang.Integer.toString(r3)     // Catch:{ Throwable -> 0x00cf }
            r0.put(r2, r3)     // Catch:{ Throwable -> 0x00cf }
            java.lang.String r2 = "timestamp"
            long r4 = r7.ad()     // Catch:{ Throwable -> 0x00cf }
            java.lang.Long r3 = java.lang.Long.valueOf(r4)     // Catch:{ Throwable -> 0x00cf }
            r0.put(r2, r3)     // Catch:{ Throwable -> 0x00cf }
            java.lang.String r2 = "events"
            r3 = 0
            long r2 = r1.insert(r2, r3, r0)     // Catch:{ Throwable -> 0x00cf }
            r1.setTransactionSuccessful()     // Catch:{ Throwable -> 0x00cf }
            if (r1 == 0) goto L_0x018a
            r1.endTransaction()     // Catch:{ Throwable -> 0x00c7 }
            r0 = r2
        L_0x0096:
            r2 = 0
            int r0 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r0 <= 0) goto L_0x00f7
            int r0 = r6.aI     // Catch:{ all -> 0x00ed }
            int r0 = r0 + 1
            r6.aI = r0     // Catch:{ all -> 0x00ed }
            boolean r0 = com.tencent.wxop.stat.c.k()     // Catch:{ all -> 0x00ed }
            if (r0 == 0) goto L_0x00c0
            com.tencent.wxop.stat.b.b r0 = com.tencent.wxop.stat.t.bZ     // Catch:{ all -> 0x00ed }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x00ed }
            java.lang.String r2 = "directStoreEvent insert event to db, event:"
            r1.<init>(r2)     // Catch:{ all -> 0x00ed }
            java.lang.String r2 = r7.af()     // Catch:{ all -> 0x00ed }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x00ed }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x00ed }
            r0.e(r1)     // Catch:{ all -> 0x00ed }
        L_0x00c0:
            if (r8 == 0) goto L_0x00c5
            r8.ah()     // Catch:{ all -> 0x00ed }
        L_0x00c5:
            monitor-exit(r6)
            return
        L_0x00c7:
            r0 = move-exception
            com.tencent.wxop.stat.b.b r1 = com.tencent.wxop.stat.t.bZ     // Catch:{ all -> 0x00ed }
            r1.b(r0)     // Catch:{ all -> 0x00ed }
            r0 = r2
            goto L_0x0096
        L_0x00cf:
            r0 = move-exception
            r2 = -1
            com.tencent.wxop.stat.b.b r4 = com.tencent.wxop.stat.t.bZ     // Catch:{ all -> 0x00e6 }
            r4.b(r0)     // Catch:{ all -> 0x00e6 }
            if (r1 == 0) goto L_0x018a
            r1.endTransaction()     // Catch:{ Throwable -> 0x00de }
            r0 = r2
            goto L_0x0096
        L_0x00de:
            r0 = move-exception
            com.tencent.wxop.stat.b.b r1 = com.tencent.wxop.stat.t.bZ     // Catch:{ all -> 0x00ed }
            r1.b(r0)     // Catch:{ all -> 0x00ed }
            r0 = r2
            goto L_0x0096
        L_0x00e6:
            r0 = move-exception
            if (r1 == 0) goto L_0x00ec
            r1.endTransaction()     // Catch:{ Throwable -> 0x00f0 }
        L_0x00ec:
            throw r0     // Catch:{ all -> 0x00ed }
        L_0x00ed:
            r0 = move-exception
            monitor-exit(r6)
            throw r0
        L_0x00f0:
            r1 = move-exception
            com.tencent.wxop.stat.b.b r2 = com.tencent.wxop.stat.t.bZ     // Catch:{ all -> 0x00ed }
            r2.b(r1)     // Catch:{ all -> 0x00ed }
            goto L_0x00ec
        L_0x00f7:
            com.tencent.wxop.stat.b.b r0 = com.tencent.wxop.stat.t.bZ     // Catch:{ all -> 0x00ed }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x00ed }
            java.lang.String r2 = "Failed to store event:"
            r1.<init>(r2)     // Catch:{ all -> 0x00ed }
            java.lang.String r2 = r7.af()     // Catch:{ all -> 0x00ed }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x00ed }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x00ed }
            r0.error(r1)     // Catch:{ all -> 0x00ed }
            goto L_0x00c5
        L_0x0110:
            int r0 = com.tencent.wxop.stat.c.ay     // Catch:{ all -> 0x00ed }
            if (r0 <= 0) goto L_0x00c5
            boolean r0 = com.tencent.wxop.stat.c.k()     // Catch:{ all -> 0x00ed }
            if (r0 == 0) goto L_0x0164
            com.tencent.wxop.stat.b.b r0 = com.tencent.wxop.stat.t.bZ     // Catch:{ all -> 0x00ed }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x00ed }
            java.lang.String r2 = "cacheEventsInMemory.size():"
            r1.<init>(r2)     // Catch:{ all -> 0x00ed }
            java.util.concurrent.ConcurrentHashMap<com.tencent.wxop.stat.a.d, java.lang.String> r2 = r6.cd     // Catch:{ all -> 0x00ed }
            int r2 = r2.size()     // Catch:{ all -> 0x00ed }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x00ed }
            java.lang.String r2 = ",numEventsCachedInMemory:"
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x00ed }
            int r2 = com.tencent.wxop.stat.c.ay     // Catch:{ all -> 0x00ed }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x00ed }
            java.lang.String r2 = ",numStoredEvents:"
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x00ed }
            int r2 = r6.aI     // Catch:{ all -> 0x00ed }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x00ed }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x00ed }
            r0.b(r1)     // Catch:{ all -> 0x00ed }
            com.tencent.wxop.stat.b.b r0 = com.tencent.wxop.stat.t.bZ     // Catch:{ all -> 0x00ed }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x00ed }
            java.lang.String r2 = "cache event:"
            r1.<init>(r2)     // Catch:{ all -> 0x00ed }
            java.lang.String r2 = r7.af()     // Catch:{ all -> 0x00ed }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x00ed }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x00ed }
            r0.b(r1)     // Catch:{ all -> 0x00ed }
        L_0x0164:
            java.util.concurrent.ConcurrentHashMap<com.tencent.wxop.stat.a.d, java.lang.String> r0 = r6.cd     // Catch:{ all -> 0x00ed }
            java.lang.String r1 = ""
            r0.put(r7, r1)     // Catch:{ all -> 0x00ed }
            java.util.concurrent.ConcurrentHashMap<com.tencent.wxop.stat.a.d, java.lang.String> r0 = r6.cd     // Catch:{ all -> 0x00ed }
            int r0 = r0.size()     // Catch:{ all -> 0x00ed }
            int r1 = com.tencent.wxop.stat.c.ay     // Catch:{ all -> 0x00ed }
            if (r0 < r1) goto L_0x0178
            r6.am()     // Catch:{ all -> 0x00ed }
        L_0x0178:
            if (r8 == 0) goto L_0x00c5
            java.util.concurrent.ConcurrentHashMap<com.tencent.wxop.stat.a.d, java.lang.String> r0 = r6.cd     // Catch:{ all -> 0x00ed }
            int r0 = r0.size()     // Catch:{ all -> 0x00ed }
            if (r0 <= 0) goto L_0x0185
            r6.am()     // Catch:{ all -> 0x00ed }
        L_0x0185:
            r8.ah()     // Catch:{ all -> 0x00ed }
            goto L_0x00c5
        L_0x018a:
            r0 = r2
            goto L_0x0096
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.wxop.stat.t.a(com.tencent.wxop.stat.a.d, com.tencent.wxop.stat.aj, boolean, boolean):void");
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x00f4 A[SYNTHETIC, Splitter:B:40:0x00f4] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void a(com.tencent.wxop.stat.ah r14) {
        /*
            r13 = this;
            r9 = 1
            r10 = 0
            r8 = 0
            monitor-enter(r13)
            org.json.JSONObject r0 = r14.df     // Catch:{ Throwable -> 0x0108, all -> 0x00f0 }
            java.lang.String r11 = r0.toString()     // Catch:{ Throwable -> 0x0108, all -> 0x00f0 }
            java.lang.String r0 = com.tencent.wxop.stat.b.l.t(r11)     // Catch:{ Throwable -> 0x0108, all -> 0x00f0 }
            android.content.ContentValues r12 = new android.content.ContentValues     // Catch:{ Throwable -> 0x0108, all -> 0x00f0 }
            r12.<init>()     // Catch:{ Throwable -> 0x0108, all -> 0x00f0 }
            java.lang.String r1 = "content"
            org.json.JSONObject r2 = r14.df     // Catch:{ Throwable -> 0x0108, all -> 0x00f0 }
            java.lang.String r2 = r2.toString()     // Catch:{ Throwable -> 0x0108, all -> 0x00f0 }
            r12.put(r1, r2)     // Catch:{ Throwable -> 0x0108, all -> 0x00f0 }
            java.lang.String r1 = "md5sum"
            r12.put(r1, r0)     // Catch:{ Throwable -> 0x0108, all -> 0x00f0 }
            r14.c = r0     // Catch:{ Throwable -> 0x0108, all -> 0x00f0 }
            java.lang.String r0 = "version"
            int r1 = r14.L     // Catch:{ Throwable -> 0x0108, all -> 0x00f0 }
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)     // Catch:{ Throwable -> 0x0108, all -> 0x00f0 }
            r12.put(r0, r1)     // Catch:{ Throwable -> 0x0108, all -> 0x00f0 }
            com.tencent.wxop.stat.ac r0 = r13.bW     // Catch:{ Throwable -> 0x0108, all -> 0x00f0 }
            android.database.sqlite.SQLiteDatabase r0 = r0.getReadableDatabase()     // Catch:{ Throwable -> 0x0108, all -> 0x00f0 }
            java.lang.String r1 = "config"
            r2 = 0
            r3 = 0
            r4 = 0
            r5 = 0
            r6 = 0
            r7 = 0
            android.database.Cursor r1 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ Throwable -> 0x0108, all -> 0x00f0 }
        L_0x0042:
            boolean r0 = r1.moveToNext()     // Catch:{ Throwable -> 0x00d9 }
            if (r0 == 0) goto L_0x010d
            r0 = 0
            int r0 = r1.getInt(r0)     // Catch:{ Throwable -> 0x00d9 }
            int r2 = r14.aI     // Catch:{ Throwable -> 0x00d9 }
            if (r0 != r2) goto L_0x0042
            r0 = r9
        L_0x0052:
            com.tencent.wxop.stat.ac r2 = r13.bW     // Catch:{ Throwable -> 0x00d9 }
            android.database.sqlite.SQLiteDatabase r2 = r2.getWritableDatabase()     // Catch:{ Throwable -> 0x00d9 }
            r2.beginTransaction()     // Catch:{ Throwable -> 0x00d9 }
            if (r9 != r0) goto L_0x00ab
            com.tencent.wxop.stat.ac r0 = r13.bW     // Catch:{ Throwable -> 0x00d9 }
            android.database.sqlite.SQLiteDatabase r0 = r0.getWritableDatabase()     // Catch:{ Throwable -> 0x00d9 }
            java.lang.String r2 = "config"
            java.lang.String r3 = "type=?"
            r4 = 1
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ Throwable -> 0x00d9 }
            r5 = 0
            int r6 = r14.aI     // Catch:{ Throwable -> 0x00d9 }
            java.lang.String r6 = java.lang.Integer.toString(r6)     // Catch:{ Throwable -> 0x00d9 }
            r4[r5] = r6     // Catch:{ Throwable -> 0x00d9 }
            int r0 = r0.update(r2, r12, r3, r4)     // Catch:{ Throwable -> 0x00d9 }
            long r2 = (long) r0     // Catch:{ Throwable -> 0x00d9 }
        L_0x0078:
            r4 = -1
            int r0 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r0 != 0) goto L_0x00c4
            com.tencent.wxop.stat.b.b r0 = com.tencent.wxop.stat.t.bZ     // Catch:{ Throwable -> 0x00d9 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x00d9 }
            java.lang.String r3 = "Failed to store cfg:"
            r2.<init>(r3)     // Catch:{ Throwable -> 0x00d9 }
            java.lang.StringBuilder r2 = r2.append(r11)     // Catch:{ Throwable -> 0x00d9 }
            java.lang.String r2 = r2.toString()     // Catch:{ Throwable -> 0x00d9 }
            r0.d(r2)     // Catch:{ Throwable -> 0x00d9 }
        L_0x0092:
            com.tencent.wxop.stat.ac r0 = r13.bW     // Catch:{ Throwable -> 0x00d9 }
            android.database.sqlite.SQLiteDatabase r0 = r0.getWritableDatabase()     // Catch:{ Throwable -> 0x00d9 }
            r0.setTransactionSuccessful()     // Catch:{ Throwable -> 0x00d9 }
            if (r1 == 0) goto L_0x00a0
            r1.close()     // Catch:{ all -> 0x0101 }
        L_0x00a0:
            com.tencent.wxop.stat.ac r0 = r13.bW     // Catch:{ Exception -> 0x010b }
            android.database.sqlite.SQLiteDatabase r0 = r0.getWritableDatabase()     // Catch:{ Exception -> 0x010b }
            r0.endTransaction()     // Catch:{ Exception -> 0x010b }
        L_0x00a9:
            monitor-exit(r13)
            return
        L_0x00ab:
            java.lang.String r0 = "type"
            int r2 = r14.aI     // Catch:{ Throwable -> 0x00d9 }
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)     // Catch:{ Throwable -> 0x00d9 }
            r12.put(r0, r2)     // Catch:{ Throwable -> 0x00d9 }
            com.tencent.wxop.stat.ac r0 = r13.bW     // Catch:{ Throwable -> 0x00d9 }
            android.database.sqlite.SQLiteDatabase r0 = r0.getWritableDatabase()     // Catch:{ Throwable -> 0x00d9 }
            java.lang.String r2 = "config"
            r3 = 0
            long r2 = r0.insert(r2, r3, r12)     // Catch:{ Throwable -> 0x00d9 }
            goto L_0x0078
        L_0x00c4:
            com.tencent.wxop.stat.b.b r0 = com.tencent.wxop.stat.t.bZ     // Catch:{ Throwable -> 0x00d9 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x00d9 }
            java.lang.String r3 = "Sucessed to store cfg:"
            r2.<init>(r3)     // Catch:{ Throwable -> 0x00d9 }
            java.lang.StringBuilder r2 = r2.append(r11)     // Catch:{ Throwable -> 0x00d9 }
            java.lang.String r2 = r2.toString()     // Catch:{ Throwable -> 0x00d9 }
            r0.e(r2)     // Catch:{ Throwable -> 0x00d9 }
            goto L_0x0092
        L_0x00d9:
            r0 = move-exception
        L_0x00da:
            com.tencent.wxop.stat.b.b r2 = com.tencent.wxop.stat.t.bZ     // Catch:{ all -> 0x0106 }
            r2.b(r0)     // Catch:{ all -> 0x0106 }
            if (r1 == 0) goto L_0x00e4
            r1.close()     // Catch:{ all -> 0x0101 }
        L_0x00e4:
            com.tencent.wxop.stat.ac r0 = r13.bW     // Catch:{ Exception -> 0x00ee }
            android.database.sqlite.SQLiteDatabase r0 = r0.getWritableDatabase()     // Catch:{ Exception -> 0x00ee }
            r0.endTransaction()     // Catch:{ Exception -> 0x00ee }
            goto L_0x00a9
        L_0x00ee:
            r0 = move-exception
            goto L_0x00a9
        L_0x00f0:
            r0 = move-exception
            r1 = r8
        L_0x00f2:
            if (r1 == 0) goto L_0x00f7
            r1.close()     // Catch:{ all -> 0x0101 }
        L_0x00f7:
            com.tencent.wxop.stat.ac r1 = r13.bW     // Catch:{ Exception -> 0x0104 }
            android.database.sqlite.SQLiteDatabase r1 = r1.getWritableDatabase()     // Catch:{ Exception -> 0x0104 }
            r1.endTransaction()     // Catch:{ Exception -> 0x0104 }
        L_0x0100:
            throw r0     // Catch:{ all -> 0x0101 }
        L_0x0101:
            r0 = move-exception
            monitor-exit(r13)
            throw r0
        L_0x0104:
            r1 = move-exception
            goto L_0x0100
        L_0x0106:
            r0 = move-exception
            goto L_0x00f2
        L_0x0108:
            r0 = move-exception
            r1 = r8
            goto L_0x00da
        L_0x010b:
            r0 = move-exception
            goto L_0x00a9
        L_0x010d:
            r0 = r10
            goto L_0x0052
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.wxop.stat.t.a(com.tencent.wxop.stat.ah):void");
    }

    static /* synthetic */ void a(t tVar, int i, boolean z) {
        int ak = i == -1 ? !z ? tVar.ak() : tVar.al() : i;
        if (ak > 0) {
            int u = c.u() * 60 * c.q();
            if (ak > u && u > 0) {
                ak = u;
            }
            int r = c.r();
            int i2 = ak / r;
            int i3 = ak % r;
            if (c.k()) {
                bZ.b("sentStoreEventsByDb sendNumbers=" + ak + ",important=" + z + ",maxSendNumPerFor1Period=" + u + ",maxCount=" + i2 + ",restNumbers=" + i3);
            }
            for (int i4 = 0; i4 < i2; i4++) {
                tVar.a(r, z);
            }
            if (i3 > 0) {
                tVar.a(i3, z);
            }
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x00cc A[SYNTHETIC, Splitter:B:45:0x00cc] */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x00dd A[SYNTHETIC, Splitter:B:53:0x00dd] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void a(java.util.List<com.tencent.wxop.stat.ad> r7, int r8, boolean r9) {
        /*
            r6 = this;
            r2 = 0
            monitor-enter(r6)
            int r0 = r7.size()     // Catch:{ all -> 0x0080 }
            if (r0 != 0) goto L_0x000a
        L_0x0008:
            monitor-exit(r6)
            return
        L_0x000a:
            if (r9 != 0) goto L_0x0083
            int r0 = com.tencent.wxop.stat.c.p()     // Catch:{ all -> 0x0080 }
        L_0x0010:
            android.database.sqlite.SQLiteDatabase r1 = r6.c(r9)     // Catch:{ Throwable -> 0x00c3, all -> 0x00d9 }
            r3 = 2
            if (r8 != r3) goto L_0x0088
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x00ea }
            java.lang.String r3 = "update events set status="
            r0.<init>(r3)     // Catch:{ Throwable -> 0x00ea }
            java.lang.StringBuilder r0 = r0.append(r8)     // Catch:{ Throwable -> 0x00ea }
            java.lang.String r3 = ", send_count=send_count+1  where "
            java.lang.StringBuilder r0 = r0.append(r3)     // Catch:{ Throwable -> 0x00ea }
            java.lang.String r3 = b(r7)     // Catch:{ Throwable -> 0x00ea }
            java.lang.StringBuilder r0 = r0.append(r3)     // Catch:{ Throwable -> 0x00ea }
            java.lang.String r0 = r0.toString()     // Catch:{ Throwable -> 0x00ea }
        L_0x0034:
            boolean r3 = com.tencent.wxop.stat.c.k()     // Catch:{ Throwable -> 0x00ea }
            if (r3 == 0) goto L_0x004e
            com.tencent.wxop.stat.b.b r3 = com.tencent.wxop.stat.t.bZ     // Catch:{ Throwable -> 0x00ea }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x00ea }
            java.lang.String r5 = "update sql:"
            r4.<init>(r5)     // Catch:{ Throwable -> 0x00ea }
            java.lang.StringBuilder r4 = r4.append(r0)     // Catch:{ Throwable -> 0x00ea }
            java.lang.String r4 = r4.toString()     // Catch:{ Throwable -> 0x00ea }
            r3.b(r4)     // Catch:{ Throwable -> 0x00ea }
        L_0x004e:
            r1.beginTransaction()     // Catch:{ Throwable -> 0x00ea }
            r1.execSQL(r0)     // Catch:{ Throwable -> 0x00ea }
            if (r2 == 0) goto L_0x0070
            com.tencent.wxop.stat.b.b r0 = com.tencent.wxop.stat.t.bZ     // Catch:{ Throwable -> 0x00ea }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x00ea }
            java.lang.String r4 = "update for delete sql:"
            r3.<init>(r4)     // Catch:{ Throwable -> 0x00ea }
            java.lang.StringBuilder r3 = r3.append(r2)     // Catch:{ Throwable -> 0x00ea }
            java.lang.String r3 = r3.toString()     // Catch:{ Throwable -> 0x00ea }
            r0.b(r3)     // Catch:{ Throwable -> 0x00ea }
            r1.execSQL(r2)     // Catch:{ Throwable -> 0x00ea }
            r6.aj()     // Catch:{ Throwable -> 0x00ea }
        L_0x0070:
            r1.setTransactionSuccessful()     // Catch:{ Throwable -> 0x00ea }
            if (r1 == 0) goto L_0x0008
            r1.endTransaction()     // Catch:{ Throwable -> 0x0079 }
            goto L_0x0008
        L_0x0079:
            r0 = move-exception
            com.tencent.wxop.stat.b.b r1 = com.tencent.wxop.stat.t.bZ     // Catch:{ all -> 0x0080 }
            r1.b(r0)     // Catch:{ all -> 0x0080 }
            goto L_0x0008
        L_0x0080:
            r0 = move-exception
            monitor-exit(r6)
            throw r0
        L_0x0083:
            int r0 = com.tencent.wxop.stat.c.n()     // Catch:{ all -> 0x0080 }
            goto L_0x0010
        L_0x0088:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x00ea }
            java.lang.String r4 = "update events set status="
            r3.<init>(r4)     // Catch:{ Throwable -> 0x00ea }
            java.lang.StringBuilder r3 = r3.append(r8)     // Catch:{ Throwable -> 0x00ea }
            java.lang.String r4 = " where "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Throwable -> 0x00ea }
            java.lang.String r4 = b(r7)     // Catch:{ Throwable -> 0x00ea }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Throwable -> 0x00ea }
            java.lang.String r3 = r3.toString()     // Catch:{ Throwable -> 0x00ea }
            int r4 = r6.cc     // Catch:{ Throwable -> 0x00ea }
            int r4 = r4 % 3
            if (r4 != 0) goto L_0x00ba
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x00ea }
            java.lang.String r4 = "delete from events where send_count>"
            r2.<init>(r4)     // Catch:{ Throwable -> 0x00ea }
            java.lang.StringBuilder r0 = r2.append(r0)     // Catch:{ Throwable -> 0x00ea }
            java.lang.String r2 = r0.toString()     // Catch:{ Throwable -> 0x00ea }
        L_0x00ba:
            int r0 = r6.cc     // Catch:{ Throwable -> 0x00ea }
            int r0 = r0 + 1
            r6.cc = r0     // Catch:{ Throwable -> 0x00ea }
            r0 = r3
            goto L_0x0034
        L_0x00c3:
            r0 = move-exception
            r1 = r2
        L_0x00c5:
            com.tencent.wxop.stat.b.b r2 = com.tencent.wxop.stat.t.bZ     // Catch:{ all -> 0x00e8 }
            r2.b(r0)     // Catch:{ all -> 0x00e8 }
            if (r1 == 0) goto L_0x0008
            r1.endTransaction()     // Catch:{ Throwable -> 0x00d1 }
            goto L_0x0008
        L_0x00d1:
            r0 = move-exception
            com.tencent.wxop.stat.b.b r1 = com.tencent.wxop.stat.t.bZ     // Catch:{ all -> 0x0080 }
            r1.b(r0)     // Catch:{ all -> 0x0080 }
            goto L_0x0008
        L_0x00d9:
            r0 = move-exception
            r1 = r2
        L_0x00db:
            if (r1 == 0) goto L_0x00e0
            r1.endTransaction()     // Catch:{ Throwable -> 0x00e1 }
        L_0x00e0:
            throw r0     // Catch:{ all -> 0x0080 }
        L_0x00e1:
            r1 = move-exception
            com.tencent.wxop.stat.b.b r2 = com.tencent.wxop.stat.t.bZ     // Catch:{ all -> 0x0080 }
            r2.b(r1)     // Catch:{ all -> 0x0080 }
            goto L_0x00e0
        L_0x00e8:
            r0 = move-exception
            goto L_0x00db
        L_0x00ea:
            r0 = move-exception
            goto L_0x00c5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.wxop.stat.t.a(java.util.List, int, boolean):void");
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x00c6, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:?, code lost:
        com.tencent.wxop.stat.t.bZ.b(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x00ed, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x00ee, code lost:
        com.tencent.wxop.stat.t.bZ.b(r1);
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [B:25:0x00c1, B:44:0x00e9] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void a(java.util.List<com.tencent.wxop.stat.ad> r9, boolean r10) {
        /*
            r8 = this;
            r1 = 0
            monitor-enter(r8)
            int r0 = r9.size()     // Catch:{ all -> 0x00ce }
            if (r0 != 0) goto L_0x000a
        L_0x0008:
            monitor-exit(r8)
            return
        L_0x000a:
            boolean r0 = com.tencent.wxop.stat.c.k()     // Catch:{ all -> 0x00ce }
            if (r0 == 0) goto L_0x0032
            com.tencent.wxop.stat.b.b r0 = com.tencent.wxop.stat.t.bZ     // Catch:{ all -> 0x00ce }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x00ce }
            java.lang.String r3 = "Delete "
            r2.<init>(r3)     // Catch:{ all -> 0x00ce }
            int r3 = r9.size()     // Catch:{ all -> 0x00ce }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x00ce }
            java.lang.String r3 = " events, important:"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x00ce }
            java.lang.StringBuilder r2 = r2.append(r10)     // Catch:{ all -> 0x00ce }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x00ce }
            r0.b(r2)     // Catch:{ all -> 0x00ce }
        L_0x0032:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x00ce }
            int r0 = r9.size()     // Catch:{ all -> 0x00ce }
            int r0 = r0 * 3
            r3.<init>(r0)     // Catch:{ all -> 0x00ce }
            java.lang.String r0 = "event_id in ("
            r3.append(r0)     // Catch:{ all -> 0x00ce }
            r0 = 0
            int r4 = r9.size()     // Catch:{ all -> 0x00ce }
            java.util.Iterator r5 = r9.iterator()     // Catch:{ all -> 0x00ce }
            r2 = r0
        L_0x004c:
            boolean r0 = r5.hasNext()     // Catch:{ all -> 0x00ce }
            if (r0 == 0) goto L_0x006a
            java.lang.Object r0 = r5.next()     // Catch:{ all -> 0x00ce }
            com.tencent.wxop.stat.ad r0 = (com.tencent.wxop.stat.ad) r0     // Catch:{ all -> 0x00ce }
            long r6 = r0.K     // Catch:{ all -> 0x00ce }
            r3.append(r6)     // Catch:{ all -> 0x00ce }
            int r0 = r4 + -1
            if (r2 == r0) goto L_0x0066
            java.lang.String r0 = ","
            r3.append(r0)     // Catch:{ all -> 0x00ce }
        L_0x0066:
            int r0 = r2 + 1
            r2 = r0
            goto L_0x004c
        L_0x006a:
            java.lang.String r0 = ")"
            r3.append(r0)     // Catch:{ all -> 0x00ce }
            android.database.sqlite.SQLiteDatabase r1 = r8.c(r10)     // Catch:{ Throwable -> 0x00d1 }
            r1.beginTransaction()     // Catch:{ Throwable -> 0x00d1 }
            java.lang.String r0 = "events"
            java.lang.String r2 = r3.toString()     // Catch:{ Throwable -> 0x00d1 }
            r5 = 0
            int r0 = r1.delete(r0, r2, r5)     // Catch:{ Throwable -> 0x00d1 }
            boolean r2 = com.tencent.wxop.stat.c.k()     // Catch:{ Throwable -> 0x00d1 }
            if (r2 == 0) goto L_0x00b3
            com.tencent.wxop.stat.b.b r2 = com.tencent.wxop.stat.t.bZ     // Catch:{ Throwable -> 0x00d1 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x00d1 }
            java.lang.String r6 = "delete "
            r5.<init>(r6)     // Catch:{ Throwable -> 0x00d1 }
            java.lang.StringBuilder r4 = r5.append(r4)     // Catch:{ Throwable -> 0x00d1 }
            java.lang.String r5 = " event "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Throwable -> 0x00d1 }
            java.lang.String r3 = r3.toString()     // Catch:{ Throwable -> 0x00d1 }
            java.lang.StringBuilder r3 = r4.append(r3)     // Catch:{ Throwable -> 0x00d1 }
            java.lang.String r4 = ", success delete:"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Throwable -> 0x00d1 }
            java.lang.StringBuilder r3 = r3.append(r0)     // Catch:{ Throwable -> 0x00d1 }
            java.lang.String r3 = r3.toString()     // Catch:{ Throwable -> 0x00d1 }
            r2.b(r3)     // Catch:{ Throwable -> 0x00d1 }
        L_0x00b3:
            int r2 = r8.aI     // Catch:{ Throwable -> 0x00d1 }
            int r0 = r2 - r0
            r8.aI = r0     // Catch:{ Throwable -> 0x00d1 }
            r1.setTransactionSuccessful()     // Catch:{ Throwable -> 0x00d1 }
            r8.aj()     // Catch:{ Throwable -> 0x00d1 }
            if (r1 == 0) goto L_0x0008
            r1.endTransaction()     // Catch:{ Throwable -> 0x00c6 }
            goto L_0x0008
        L_0x00c6:
            r0 = move-exception
            com.tencent.wxop.stat.b.b r1 = com.tencent.wxop.stat.t.bZ     // Catch:{ all -> 0x00ce }
            r1.b(r0)     // Catch:{ all -> 0x00ce }
            goto L_0x0008
        L_0x00ce:
            r0 = move-exception
            monitor-exit(r8)
            throw r0
        L_0x00d1:
            r0 = move-exception
            com.tencent.wxop.stat.b.b r2 = com.tencent.wxop.stat.t.bZ     // Catch:{ all -> 0x00e6 }
            r2.b(r0)     // Catch:{ all -> 0x00e6 }
            if (r1 == 0) goto L_0x0008
            r1.endTransaction()     // Catch:{ Throwable -> 0x00de }
            goto L_0x0008
        L_0x00de:
            r0 = move-exception
            com.tencent.wxop.stat.b.b r1 = com.tencent.wxop.stat.t.bZ     // Catch:{ all -> 0x00ce }
            r1.b(r0)     // Catch:{ all -> 0x00ce }
            goto L_0x0008
        L_0x00e6:
            r0 = move-exception
            if (r1 == 0) goto L_0x00ec
            r1.endTransaction()     // Catch:{ Throwable -> 0x00ed }
        L_0x00ec:
            throw r0     // Catch:{ all -> 0x00ce }
        L_0x00ed:
            r1 = move-exception
            com.tencent.wxop.stat.b.b r2 = com.tencent.wxop.stat.t.bZ     // Catch:{ all -> 0x00ce }
            r2.b(r1)     // Catch:{ all -> 0x00ce }
            goto L_0x00ec
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.wxop.stat.t.a(java.util.List, boolean):void");
    }

    public static t ai() {
        return cb;
    }

    private void aj() {
        this.aI = ak() + al();
    }

    private int ak() {
        return (int) DatabaseUtils.queryNumEntries(this.bW.getReadableDatabase(), EventStoreHelper.TABLE_EVENTS);
    }

    private int al() {
        return (int) DatabaseUtils.queryNumEntries(this.bX.getReadableDatabase(), EventStoreHelper.TABLE_EVENTS);
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x0122, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:?, code lost:
        com.tencent.wxop.stat.t.bZ.b(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x013a, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x013b, code lost:
        com.tencent.wxop.stat.t.bZ.b(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:?, code lost:
        return;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [B:39:0x011b, B:48:0x0133] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void am() {
        /*
            r9 = this;
            r1 = 0
            boolean r0 = r9.ce
            if (r0 == 0) goto L_0x0006
        L_0x0005:
            return
        L_0x0006:
            java.util.concurrent.ConcurrentHashMap<com.tencent.wxop.stat.a.d, java.lang.String> r2 = r9.cd
            monitor-enter(r2)
            java.util.concurrent.ConcurrentHashMap<com.tencent.wxop.stat.a.d, java.lang.String> r0 = r9.cd     // Catch:{ all -> 0x0013 }
            int r0 = r0.size()     // Catch:{ all -> 0x0013 }
            if (r0 != 0) goto L_0x0016
            monitor-exit(r2)     // Catch:{ all -> 0x0013 }
            goto L_0x0005
        L_0x0013:
            r0 = move-exception
            monitor-exit(r2)
            throw r0
        L_0x0016:
            r0 = 1
            r9.ce = r0     // Catch:{ all -> 0x0013 }
            boolean r0 = com.tencent.wxop.stat.c.k()     // Catch:{ all -> 0x0013 }
            if (r0 == 0) goto L_0x0051
            com.tencent.wxop.stat.b.b r0 = com.tencent.wxop.stat.t.bZ     // Catch:{ all -> 0x0013 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x0013 }
            java.lang.String r4 = "insert "
            r3.<init>(r4)     // Catch:{ all -> 0x0013 }
            java.util.concurrent.ConcurrentHashMap<com.tencent.wxop.stat.a.d, java.lang.String> r4 = r9.cd     // Catch:{ all -> 0x0013 }
            int r4 = r4.size()     // Catch:{ all -> 0x0013 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x0013 }
            java.lang.String r4 = " events ,numEventsCachedInMemory:"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x0013 }
            int r4 = com.tencent.wxop.stat.c.ay     // Catch:{ all -> 0x0013 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x0013 }
            java.lang.String r4 = ",numStoredEvents:"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x0013 }
            int r4 = r9.aI     // Catch:{ all -> 0x0013 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x0013 }
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x0013 }
            r0.b(r3)     // Catch:{ all -> 0x0013 }
        L_0x0051:
            com.tencent.wxop.stat.ac r0 = r9.bW     // Catch:{ Throwable -> 0x00ca }
            android.database.sqlite.SQLiteDatabase r1 = r0.getWritableDatabase()     // Catch:{ Throwable -> 0x00ca }
            r1.beginTransaction()     // Catch:{ Throwable -> 0x00ca }
            java.util.concurrent.ConcurrentHashMap<com.tencent.wxop.stat.a.d, java.lang.String> r0 = r9.cd     // Catch:{ Throwable -> 0x00ca }
            java.util.Set r0 = r0.entrySet()     // Catch:{ Throwable -> 0x00ca }
            java.util.Iterator r3 = r0.iterator()     // Catch:{ Throwable -> 0x00ca }
        L_0x0064:
            boolean r0 = r3.hasNext()     // Catch:{ Throwable -> 0x00ca }
            if (r0 == 0) goto L_0x0116
            java.lang.Object r0 = r3.next()     // Catch:{ Throwable -> 0x00ca }
            java.util.Map$Entry r0 = (java.util.Map.Entry) r0     // Catch:{ Throwable -> 0x00ca }
            java.lang.Object r0 = r0.getKey()     // Catch:{ Throwable -> 0x00ca }
            com.tencent.wxop.stat.a.d r0 = (com.tencent.wxop.stat.a.d) r0     // Catch:{ Throwable -> 0x00ca }
            android.content.ContentValues r4 = new android.content.ContentValues     // Catch:{ Throwable -> 0x00ca }
            r4.<init>()     // Catch:{ Throwable -> 0x00ca }
            java.lang.String r5 = r0.af()     // Catch:{ Throwable -> 0x00ca }
            boolean r6 = com.tencent.wxop.stat.c.k()     // Catch:{ Throwable -> 0x00ca }
            if (r6 == 0) goto L_0x0099
            com.tencent.wxop.stat.b.b r6 = com.tencent.wxop.stat.t.bZ     // Catch:{ Throwable -> 0x00ca }
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x00ca }
            java.lang.String r8 = "insert content:"
            r7.<init>(r8)     // Catch:{ Throwable -> 0x00ca }
            java.lang.StringBuilder r7 = r7.append(r5)     // Catch:{ Throwable -> 0x00ca }
            java.lang.String r7 = r7.toString()     // Catch:{ Throwable -> 0x00ca }
            r6.b(r7)     // Catch:{ Throwable -> 0x00ca }
        L_0x0099:
            java.lang.String r5 = com.tencent.wxop.stat.b.r.q(r5)     // Catch:{ Throwable -> 0x00ca }
            java.lang.String r6 = "content"
            r4.put(r6, r5)     // Catch:{ Throwable -> 0x00ca }
            java.lang.String r5 = "send_count"
            java.lang.String r6 = "0"
            r4.put(r5, r6)     // Catch:{ Throwable -> 0x00ca }
            java.lang.String r5 = "status"
            r6 = 1
            java.lang.String r6 = java.lang.Integer.toString(r6)     // Catch:{ Throwable -> 0x00ca }
            r4.put(r5, r6)     // Catch:{ Throwable -> 0x00ca }
            java.lang.String r5 = "timestamp"
            long r6 = r0.ad()     // Catch:{ Throwable -> 0x00ca }
            java.lang.Long r0 = java.lang.Long.valueOf(r6)     // Catch:{ Throwable -> 0x00ca }
            r4.put(r5, r0)     // Catch:{ Throwable -> 0x00ca }
            java.lang.String r0 = "events"
            r5 = 0
            r1.insert(r0, r5, r4)     // Catch:{ Throwable -> 0x00ca }
            r3.remove()     // Catch:{ Throwable -> 0x00ca }
            goto L_0x0064
        L_0x00ca:
            r0 = move-exception
            com.tencent.wxop.stat.b.b r3 = com.tencent.wxop.stat.t.bZ     // Catch:{ all -> 0x0130 }
            r3.b(r0)     // Catch:{ all -> 0x0130 }
            if (r1 == 0) goto L_0x00d8
            r1.endTransaction()     // Catch:{ Throwable -> 0x0129 }
            r9.aj()     // Catch:{ Throwable -> 0x0129 }
        L_0x00d8:
            r0 = 0
            r9.ce = r0     // Catch:{ all -> 0x0013 }
            boolean r0 = com.tencent.wxop.stat.c.k()     // Catch:{ all -> 0x0013 }
            if (r0 == 0) goto L_0x0113
            com.tencent.wxop.stat.b.b r0 = com.tencent.wxop.stat.t.bZ     // Catch:{ all -> 0x0013 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x0013 }
            java.lang.String r3 = "after insert, cacheEventsInMemory.size():"
            r1.<init>(r3)     // Catch:{ all -> 0x0013 }
            java.util.concurrent.ConcurrentHashMap<com.tencent.wxop.stat.a.d, java.lang.String> r3 = r9.cd     // Catch:{ all -> 0x0013 }
            int r3 = r3.size()     // Catch:{ all -> 0x0013 }
            java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ all -> 0x0013 }
            java.lang.String r3 = ",numEventsCachedInMemory:"
            java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ all -> 0x0013 }
            int r3 = com.tencent.wxop.stat.c.ay     // Catch:{ all -> 0x0013 }
            java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ all -> 0x0013 }
            java.lang.String r3 = ",numStoredEvents:"
            java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ all -> 0x0013 }
            int r3 = r9.aI     // Catch:{ all -> 0x0013 }
            java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ all -> 0x0013 }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x0013 }
            r0.b(r1)     // Catch:{ all -> 0x0013 }
        L_0x0113:
            monitor-exit(r2)     // Catch:{ all -> 0x0013 }
            goto L_0x0005
        L_0x0116:
            r1.setTransactionSuccessful()     // Catch:{ Throwable -> 0x00ca }
            if (r1 == 0) goto L_0x00d8
            r1.endTransaction()     // Catch:{ Throwable -> 0x0122 }
            r9.aj()     // Catch:{ Throwable -> 0x0122 }
            goto L_0x00d8
        L_0x0122:
            r0 = move-exception
            com.tencent.wxop.stat.b.b r1 = com.tencent.wxop.stat.t.bZ     // Catch:{ all -> 0x0013 }
            r1.b(r0)     // Catch:{ all -> 0x0013 }
            goto L_0x00d8
        L_0x0129:
            r0 = move-exception
            com.tencent.wxop.stat.b.b r1 = com.tencent.wxop.stat.t.bZ     // Catch:{ all -> 0x0013 }
            r1.b(r0)     // Catch:{ all -> 0x0013 }
            goto L_0x00d8
        L_0x0130:
            r0 = move-exception
            if (r1 == 0) goto L_0x0139
            r1.endTransaction()     // Catch:{ Throwable -> 0x013a }
            r9.aj()     // Catch:{ Throwable -> 0x013a }
        L_0x0139:
            throw r0     // Catch:{ all -> 0x0013 }
        L_0x013a:
            r1 = move-exception
            com.tencent.wxop.stat.b.b r3 = com.tencent.wxop.stat.t.bZ     // Catch:{ all -> 0x0013 }
            r3.b(r1)     // Catch:{ all -> 0x0013 }
            goto L_0x0139
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.wxop.stat.t.am():void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x003f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void an() {
        /*
            r9 = this;
            r8 = 0
            com.tencent.wxop.stat.ac r0 = r9.bW     // Catch:{ Throwable -> 0x0045, all -> 0x003b }
            android.database.sqlite.SQLiteDatabase r0 = r0.getReadableDatabase()     // Catch:{ Throwable -> 0x0045, all -> 0x003b }
            java.lang.String r1 = "keyvalues"
            r2 = 0
            r3 = 0
            r4 = 0
            r5 = 0
            r6 = 0
            r7 = 0
            android.database.Cursor r1 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ Throwable -> 0x0045, all -> 0x003b }
        L_0x0013:
            boolean r0 = r1.moveToNext()     // Catch:{ Throwable -> 0x0029 }
            if (r0 == 0) goto L_0x0035
            java.util.HashMap<java.lang.String, java.lang.String> r0 = r9.cf     // Catch:{ Throwable -> 0x0029 }
            r2 = 0
            java.lang.String r2 = r1.getString(r2)     // Catch:{ Throwable -> 0x0029 }
            r3 = 1
            java.lang.String r3 = r1.getString(r3)     // Catch:{ Throwable -> 0x0029 }
            r0.put(r2, r3)     // Catch:{ Throwable -> 0x0029 }
            goto L_0x0013
        L_0x0029:
            r0 = move-exception
        L_0x002a:
            com.tencent.wxop.stat.b.b r2 = com.tencent.wxop.stat.t.bZ     // Catch:{ all -> 0x0043 }
            r2.b(r0)     // Catch:{ all -> 0x0043 }
            if (r1 == 0) goto L_0x0034
            r1.close()
        L_0x0034:
            return
        L_0x0035:
            if (r1 == 0) goto L_0x0034
            r1.close()
            goto L_0x0034
        L_0x003b:
            r0 = move-exception
            r1 = r8
        L_0x003d:
            if (r1 == 0) goto L_0x0042
            r1.close()
        L_0x0042:
            throw r0
        L_0x0043:
            r0 = move-exception
            goto L_0x003d
        L_0x0045:
            r0 = move-exception
            r1 = r8
            goto L_0x002a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.wxop.stat.t.an():void");
    }

    private static String b(List<ad> list) {
        StringBuilder sb = new StringBuilder(list.size() * 3);
        sb.append("event_id in (");
        int i = 0;
        int size = list.size();
        Iterator<ad> it = list.iterator();
        while (true) {
            int i2 = i;
            if (it.hasNext()) {
                sb.append(it.next().K);
                if (i2 != size - 1) {
                    sb.append(MiPushClient.ACCEPT_TIME_SEPARATOR);
                }
                i = i2 + 1;
            } else {
                sb.append(")");
                return sb.toString();
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:28:0x00a1  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void b(java.util.List<com.tencent.wxop.stat.ad> r11, int r12, boolean r13) {
        /*
            r10 = this;
            r9 = 0
            if (r13 != 0) goto L_0x008f
            com.tencent.wxop.stat.ac r0 = r10.bW     // Catch:{ Throwable -> 0x00aa, all -> 0x009d }
            android.database.sqlite.SQLiteDatabase r0 = r0.getReadableDatabase()     // Catch:{ Throwable -> 0x00aa, all -> 0x009d }
        L_0x0009:
            java.lang.String r1 = "events"
            r2 = 0
            java.lang.String r3 = "status=?"
            r4 = 1
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ Throwable -> 0x00aa, all -> 0x009d }
            r5 = 0
            r6 = 1
            java.lang.String r6 = java.lang.Integer.toString(r6)     // Catch:{ Throwable -> 0x00aa, all -> 0x009d }
            r4[r5] = r6     // Catch:{ Throwable -> 0x00aa, all -> 0x009d }
            r5 = 0
            r6 = 0
            r7 = 0
            java.lang.String r8 = java.lang.Integer.toString(r12)     // Catch:{ Throwable -> 0x00aa, all -> 0x009d }
            android.database.Cursor r7 = r0.query(r1, r2, r3, r4, r5, r6, r7, r8)     // Catch:{ Throwable -> 0x00aa, all -> 0x009d }
        L_0x0024:
            boolean r0 = r7.moveToNext()     // Catch:{ Throwable -> 0x0082, all -> 0x00a5 }
            if (r0 == 0) goto L_0x0097
            r0 = 0
            long r2 = r7.getLong(r0)     // Catch:{ Throwable -> 0x0082, all -> 0x00a5 }
            r0 = 1
            java.lang.String r4 = r7.getString(r0)     // Catch:{ Throwable -> 0x0082, all -> 0x00a5 }
            boolean r0 = com.tencent.wxop.stat.c.ad     // Catch:{ Throwable -> 0x0082, all -> 0x00a5 }
            if (r0 != 0) goto L_0x003c
            java.lang.String r4 = com.tencent.wxop.stat.b.r.t(r4)     // Catch:{ Throwable -> 0x0082, all -> 0x00a5 }
        L_0x003c:
            r0 = 2
            int r5 = r7.getInt(r0)     // Catch:{ Throwable -> 0x0082, all -> 0x00a5 }
            r0 = 3
            int r6 = r7.getInt(r0)     // Catch:{ Throwable -> 0x0082, all -> 0x00a5 }
            com.tencent.wxop.stat.ad r1 = new com.tencent.wxop.stat.ad     // Catch:{ Throwable -> 0x0082, all -> 0x00a5 }
            r1.<init>(r2, r4, r5, r6)     // Catch:{ Throwable -> 0x0082, all -> 0x00a5 }
            boolean r0 = com.tencent.wxop.stat.c.k()     // Catch:{ Throwable -> 0x0082, all -> 0x00a5 }
            if (r0 == 0) goto L_0x007e
            com.tencent.wxop.stat.b.b r0 = com.tencent.wxop.stat.t.bZ     // Catch:{ Throwable -> 0x0082, all -> 0x00a5 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0082, all -> 0x00a5 }
            java.lang.String r5 = "peek event, id="
            r4.<init>(r5)     // Catch:{ Throwable -> 0x0082, all -> 0x00a5 }
            java.lang.StringBuilder r2 = r4.append(r2)     // Catch:{ Throwable -> 0x0082, all -> 0x00a5 }
            java.lang.String r3 = ",send_count="
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Throwable -> 0x0082, all -> 0x00a5 }
            java.lang.StringBuilder r2 = r2.append(r6)     // Catch:{ Throwable -> 0x0082, all -> 0x00a5 }
            java.lang.String r3 = ",timestamp="
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Throwable -> 0x0082, all -> 0x00a5 }
            r3 = 4
            long r4 = r7.getLong(r3)     // Catch:{ Throwable -> 0x0082, all -> 0x00a5 }
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ Throwable -> 0x0082, all -> 0x00a5 }
            java.lang.String r2 = r2.toString()     // Catch:{ Throwable -> 0x0082, all -> 0x00a5 }
            r0.b(r2)     // Catch:{ Throwable -> 0x0082, all -> 0x00a5 }
        L_0x007e:
            r11.add(r1)     // Catch:{ Throwable -> 0x0082, all -> 0x00a5 }
            goto L_0x0024
        L_0x0082:
            r0 = move-exception
            r1 = r7
        L_0x0084:
            com.tencent.wxop.stat.b.b r2 = com.tencent.wxop.stat.t.bZ     // Catch:{ all -> 0x00a7 }
            r2.b(r0)     // Catch:{ all -> 0x00a7 }
            if (r1 == 0) goto L_0x008e
            r1.close()
        L_0x008e:
            return
        L_0x008f:
            com.tencent.wxop.stat.ac r0 = r10.bX     // Catch:{ Throwable -> 0x00aa, all -> 0x009d }
            android.database.sqlite.SQLiteDatabase r0 = r0.getReadableDatabase()     // Catch:{ Throwable -> 0x00aa, all -> 0x009d }
            goto L_0x0009
        L_0x0097:
            if (r7 == 0) goto L_0x008e
            r7.close()
            goto L_0x008e
        L_0x009d:
            r0 = move-exception
            r7 = r9
        L_0x009f:
            if (r7 == 0) goto L_0x00a4
            r7.close()
        L_0x00a4:
            throw r0
        L_0x00a5:
            r0 = move-exception
            goto L_0x009f
        L_0x00a7:
            r0 = move-exception
            r7 = r1
            goto L_0x009f
        L_0x00aa:
            r0 = move-exception
            r1 = r9
            goto L_0x0084
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.wxop.stat.t.b(java.util.List, int, boolean):void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    private void b(boolean z) {
        SQLiteDatabase sQLiteDatabase = null;
        try {
            SQLiteDatabase c = c(z);
            c.beginTransaction();
            ContentValues contentValues = new ContentValues();
            contentValues.put("status", (Integer) 1);
            int update = c.update(EventStoreHelper.TABLE_EVENTS, contentValues, "status=?", new String[]{Long.toString(2)});
            if (c.k()) {
                bZ.b("update " + update + " unsent events.");
            }
            c.setTransactionSuccessful();
            if (c != null) {
                try {
                    c.endTransaction();
                } catch (Throwable th) {
                    bZ.b(th);
                }
            }
        } catch (Throwable th2) {
            bZ.b(th2);
        }
    }

    private SQLiteDatabase c(boolean z) {
        return !z ? this.bW.getWritableDatabase() : this.bX.getWritableDatabase();
    }

    public static t s(Context context) {
        if (cb == null) {
            synchronized (t.class) {
                if (cb == null) {
                    cb = new t(context);
                }
            }
        }
        return cb;
    }

    /* access modifiers changed from: package-private */
    public final void H() {
        if (c.l()) {
            try {
                this.be.a(new w(this));
            } catch (Throwable th) {
                bZ.b(th);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final void b(int i) {
        this.be.a(new ab(this, i));
    }

    /* access modifiers changed from: package-private */
    public final void b(d dVar, aj ajVar, boolean z, boolean z2) {
        if (this.be != null) {
            this.be.a(new x(this, dVar, ajVar, z, z2));
        }
    }

    /* access modifiers changed from: package-private */
    public final void b(ah ahVar) {
        if (ahVar != null) {
            this.be.a(new y(this, ahVar));
        }
    }

    /* access modifiers changed from: package-private */
    public final void b(List<ad> list, boolean z) {
        if (this.be != null) {
            this.be.a(new u(this, list, z));
        }
    }

    /* access modifiers changed from: package-private */
    public final void c(List<ad> list, boolean z) {
        if (this.be != null) {
            this.be.a(new v(this, list, z));
        }
    }

    public final int r() {
        return this.aI;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    /* JADX WARNING: Removed duplicated region for block: B:84:0x0201 A[SYNTHETIC, Splitter:B:84:0x0201] */
    /* JADX WARNING: Removed duplicated region for block: B:93:0x021b A[SYNTHETIC, Splitter:B:93:0x021b] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized com.tencent.wxop.stat.b.c t(android.content.Context r20) {
        /*
            r19 = this;
            monitor-enter(r19)
            r0 = r19
            com.tencent.wxop.stat.b.c r2 = r0.bY     // Catch:{ all -> 0x01f5 }
            if (r2 == 0) goto L_0x000d
            r0 = r19
            com.tencent.wxop.stat.b.c r2 = r0.bY     // Catch:{ all -> 0x01f5 }
        L_0x000b:
            monitor-exit(r19)
            return r2
        L_0x000d:
            r11 = 0
            r0 = r19
            com.tencent.wxop.stat.ac r2 = r0.bW     // Catch:{ Throwable -> 0x01f8, all -> 0x0217 }
            android.database.sqlite.SQLiteDatabase r2 = r2.getWritableDatabase()     // Catch:{ Throwable -> 0x01f8, all -> 0x0217 }
            r2.beginTransaction()     // Catch:{ Throwable -> 0x01f8, all -> 0x0217 }
            boolean r2 = com.tencent.wxop.stat.c.k()     // Catch:{ Throwable -> 0x01f8, all -> 0x0217 }
            if (r2 == 0) goto L_0x0026
            com.tencent.wxop.stat.b.b r2 = com.tencent.wxop.stat.t.bZ     // Catch:{ Throwable -> 0x01f8, all -> 0x0217 }
            java.lang.String r3 = "try to load user info from db."
            r2.b(r3)     // Catch:{ Throwable -> 0x01f8, all -> 0x0217 }
        L_0x0026:
            r0 = r19
            com.tencent.wxop.stat.ac r2 = r0.bW     // Catch:{ Throwable -> 0x01f8, all -> 0x0217 }
            android.database.sqlite.SQLiteDatabase r2 = r2.getReadableDatabase()     // Catch:{ Throwable -> 0x01f8, all -> 0x0217 }
            java.lang.String r3 = "user"
            r4 = 0
            r5 = 0
            r6 = 0
            r7 = 0
            r8 = 0
            r9 = 0
            r10 = 0
            android.database.Cursor r5 = r2.query(r3, r4, r5, r6, r7, r8, r9, r10)     // Catch:{ Throwable -> 0x01f8, all -> 0x0217 }
            r2 = 0
            boolean r3 = r5.moveToNext()     // Catch:{ Throwable -> 0x0236, all -> 0x0231 }
            if (r3 == 0) goto L_0x012d
            r2 = 0
            java.lang.String r10 = r5.getString(r2)     // Catch:{ Throwable -> 0x0236, all -> 0x0231 }
            java.lang.String r7 = com.tencent.wxop.stat.b.r.t(r10)     // Catch:{ Throwable -> 0x0236, all -> 0x0231 }
            r2 = 1
            int r9 = r5.getInt(r2)     // Catch:{ Throwable -> 0x0236, all -> 0x0231 }
            r2 = 2
            java.lang.String r3 = r5.getString(r2)     // Catch:{ Throwable -> 0x0236, all -> 0x0231 }
            r2 = 3
            long r12 = r5.getLong(r2)     // Catch:{ Throwable -> 0x0236, all -> 0x0231 }
            r6 = 1
            long r14 = java.lang.System.currentTimeMillis()     // Catch:{ Throwable -> 0x0236, all -> 0x0231 }
            r16 = 1000(0x3e8, double:4.94E-321)
            long r14 = r14 / r16
            r2 = 1
            if (r9 == r2) goto L_0x0249
            r16 = 1000(0x3e8, double:4.94E-321)
            long r12 = r12 * r16
            java.lang.String r2 = com.tencent.wxop.stat.b.l.d(r12)     // Catch:{ Throwable -> 0x0236, all -> 0x0231 }
            r12 = 1000(0x3e8, double:4.94E-321)
            long r12 = r12 * r14
            java.lang.String r4 = com.tencent.wxop.stat.b.l.d(r12)     // Catch:{ Throwable -> 0x0236, all -> 0x0231 }
            boolean r2 = r2.equals(r4)     // Catch:{ Throwable -> 0x0236, all -> 0x0231 }
            if (r2 != 0) goto L_0x0249
            r2 = 1
        L_0x007c:
            java.lang.String r4 = com.tencent.wxop.stat.b.l.G(r20)     // Catch:{ Throwable -> 0x0236, all -> 0x0231 }
            boolean r3 = r3.equals(r4)     // Catch:{ Throwable -> 0x0236, all -> 0x0231 }
            if (r3 != 0) goto L_0x0246
            r2 = r2 | 2
            r8 = r2
        L_0x0089:
            java.lang.String r2 = ","
            java.lang.String[] r11 = r7.split(r2)     // Catch:{ Throwable -> 0x0236, all -> 0x0231 }
            r2 = 0
            if (r11 == 0) goto L_0x01c0
            int r3 = r11.length     // Catch:{ Throwable -> 0x0236, all -> 0x0231 }
            if (r3 <= 0) goto L_0x01c0
            r3 = 0
            r4 = r11[r3]     // Catch:{ Throwable -> 0x0236, all -> 0x0231 }
            if (r4 == 0) goto L_0x00a2
            int r3 = r4.length()     // Catch:{ Throwable -> 0x0236, all -> 0x0231 }
            r12 = 11
            if (r3 >= r12) goto L_0x023f
        L_0x00a2:
            java.lang.String r3 = com.tencent.wxop.stat.b.r.b(r20)     // Catch:{ Throwable -> 0x0236, all -> 0x0231 }
            if (r3 == 0) goto L_0x023c
            int r12 = r3.length()     // Catch:{ Throwable -> 0x0236, all -> 0x0231 }
            r13 = 10
            if (r12 <= r13) goto L_0x023c
            r2 = 1
        L_0x00b1:
            r4 = r7
            r7 = r3
        L_0x00b3:
            if (r11 == 0) goto L_0x01c8
            int r3 = r11.length     // Catch:{ Throwable -> 0x0236, all -> 0x0231 }
            r12 = 2
            if (r3 < r12) goto L_0x01c8
            r3 = 1
            r3 = r11[r3]     // Catch:{ Throwable -> 0x0236, all -> 0x0231 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0236, all -> 0x0231 }
            r4.<init>()     // Catch:{ Throwable -> 0x0236, all -> 0x0231 }
            java.lang.StringBuilder r4 = r4.append(r7)     // Catch:{ Throwable -> 0x0236, all -> 0x0231 }
            java.lang.String r11 = ","
            java.lang.StringBuilder r4 = r4.append(r11)     // Catch:{ Throwable -> 0x0236, all -> 0x0231 }
            java.lang.StringBuilder r4 = r4.append(r3)     // Catch:{ Throwable -> 0x0236, all -> 0x0231 }
            java.lang.String r4 = r4.toString()     // Catch:{ Throwable -> 0x0236, all -> 0x0231 }
        L_0x00d3:
            com.tencent.wxop.stat.b.c r11 = new com.tencent.wxop.stat.b.c     // Catch:{ Throwable -> 0x0236, all -> 0x0231 }
            r11.<init>(r7, r3, r8)     // Catch:{ Throwable -> 0x0236, all -> 0x0231 }
            r0 = r19
            r0.bY = r11     // Catch:{ Throwable -> 0x0236, all -> 0x0231 }
            android.content.ContentValues r3 = new android.content.ContentValues     // Catch:{ Throwable -> 0x0236, all -> 0x0231 }
            r3.<init>()     // Catch:{ Throwable -> 0x0236, all -> 0x0231 }
            java.lang.String r4 = com.tencent.wxop.stat.b.r.q(r4)     // Catch:{ Throwable -> 0x0236, all -> 0x0231 }
            java.lang.String r7 = "uid"
            r3.put(r7, r4)     // Catch:{ Throwable -> 0x0236, all -> 0x0231 }
            java.lang.String r4 = "user_type"
            java.lang.Integer r7 = java.lang.Integer.valueOf(r8)     // Catch:{ Throwable -> 0x0236, all -> 0x0231 }
            r3.put(r4, r7)     // Catch:{ Throwable -> 0x0236, all -> 0x0231 }
            java.lang.String r4 = "app_ver"
            java.lang.String r7 = com.tencent.wxop.stat.b.l.G(r20)     // Catch:{ Throwable -> 0x0236, all -> 0x0231 }
            r3.put(r4, r7)     // Catch:{ Throwable -> 0x0236, all -> 0x0231 }
            java.lang.String r4 = "ts"
            java.lang.Long r7 = java.lang.Long.valueOf(r14)     // Catch:{ Throwable -> 0x0236, all -> 0x0231 }
            r3.put(r4, r7)     // Catch:{ Throwable -> 0x0236, all -> 0x0231 }
            if (r2 == 0) goto L_0x011c
            r0 = r19
            com.tencent.wxop.stat.ac r2 = r0.bW     // Catch:{ Throwable -> 0x0236, all -> 0x0231 }
            android.database.sqlite.SQLiteDatabase r2 = r2.getWritableDatabase()     // Catch:{ Throwable -> 0x0236, all -> 0x0231 }
            java.lang.String r4 = "user"
            java.lang.String r7 = "uid=?"
            r11 = 1
            java.lang.String[] r11 = new java.lang.String[r11]     // Catch:{ Throwable -> 0x0236, all -> 0x0231 }
            r12 = 0
            r11[r12] = r10     // Catch:{ Throwable -> 0x0236, all -> 0x0231 }
            r2.update(r4, r3, r7, r11)     // Catch:{ Throwable -> 0x0236, all -> 0x0231 }
        L_0x011c:
            if (r8 == r9) goto L_0x012c
            r0 = r19
            com.tencent.wxop.stat.ac r2 = r0.bW     // Catch:{ Throwable -> 0x0236, all -> 0x0231 }
            android.database.sqlite.SQLiteDatabase r2 = r2.getWritableDatabase()     // Catch:{ Throwable -> 0x0236, all -> 0x0231 }
            java.lang.String r4 = "user"
            r7 = 0
            r2.replace(r4, r7, r3)     // Catch:{ Throwable -> 0x0236, all -> 0x0231 }
        L_0x012c:
            r2 = r6
        L_0x012d:
            if (r2 != 0) goto L_0x019f
            java.lang.String r3 = com.tencent.wxop.stat.b.l.c(r20)     // Catch:{ Throwable -> 0x0236, all -> 0x0231 }
            java.lang.String r4 = com.tencent.wxop.stat.b.l.w(r20)     // Catch:{ Throwable -> 0x0236, all -> 0x0231 }
            if (r4 == 0) goto L_0x0239
            int r2 = r4.length()     // Catch:{ Throwable -> 0x0236, all -> 0x0231 }
            if (r2 <= 0) goto L_0x0239
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0236, all -> 0x0231 }
            r2.<init>()     // Catch:{ Throwable -> 0x0236, all -> 0x0231 }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Throwable -> 0x0236, all -> 0x0231 }
            java.lang.String r6 = ","
            java.lang.StringBuilder r2 = r2.append(r6)     // Catch:{ Throwable -> 0x0236, all -> 0x0231 }
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ Throwable -> 0x0236, all -> 0x0231 }
            java.lang.String r2 = r2.toString()     // Catch:{ Throwable -> 0x0236, all -> 0x0231 }
        L_0x0156:
            long r6 = java.lang.System.currentTimeMillis()     // Catch:{ Throwable -> 0x0236, all -> 0x0231 }
            r8 = 1000(0x3e8, double:4.94E-321)
            long r6 = r6 / r8
            java.lang.String r8 = com.tencent.wxop.stat.b.l.G(r20)     // Catch:{ Throwable -> 0x0236, all -> 0x0231 }
            android.content.ContentValues r9 = new android.content.ContentValues     // Catch:{ Throwable -> 0x0236, all -> 0x0231 }
            r9.<init>()     // Catch:{ Throwable -> 0x0236, all -> 0x0231 }
            java.lang.String r2 = com.tencent.wxop.stat.b.r.q(r2)     // Catch:{ Throwable -> 0x0236, all -> 0x0231 }
            java.lang.String r10 = "uid"
            r9.put(r10, r2)     // Catch:{ Throwable -> 0x0236, all -> 0x0231 }
            java.lang.String r2 = "user_type"
            r10 = 0
            java.lang.Integer r10 = java.lang.Integer.valueOf(r10)     // Catch:{ Throwable -> 0x0236, all -> 0x0231 }
            r9.put(r2, r10)     // Catch:{ Throwable -> 0x0236, all -> 0x0231 }
            java.lang.String r2 = "app_ver"
            r9.put(r2, r8)     // Catch:{ Throwable -> 0x0236, all -> 0x0231 }
            java.lang.String r2 = "ts"
            java.lang.Long r6 = java.lang.Long.valueOf(r6)     // Catch:{ Throwable -> 0x0236, all -> 0x0231 }
            r9.put(r2, r6)     // Catch:{ Throwable -> 0x0236, all -> 0x0231 }
            r0 = r19
            com.tencent.wxop.stat.ac r2 = r0.bW     // Catch:{ Throwable -> 0x0236, all -> 0x0231 }
            android.database.sqlite.SQLiteDatabase r2 = r2.getWritableDatabase()     // Catch:{ Throwable -> 0x0236, all -> 0x0231 }
            java.lang.String r6 = "user"
            r7 = 0
            r2.insert(r6, r7, r9)     // Catch:{ Throwable -> 0x0236, all -> 0x0231 }
            com.tencent.wxop.stat.b.c r2 = new com.tencent.wxop.stat.b.c     // Catch:{ Throwable -> 0x0236, all -> 0x0231 }
            r6 = 0
            r2.<init>(r3, r4, r6)     // Catch:{ Throwable -> 0x0236, all -> 0x0231 }
            r0 = r19
            r0.bY = r2     // Catch:{ Throwable -> 0x0236, all -> 0x0231 }
        L_0x019f:
            r0 = r19
            com.tencent.wxop.stat.ac r2 = r0.bW     // Catch:{ Throwable -> 0x0236, all -> 0x0231 }
            android.database.sqlite.SQLiteDatabase r2 = r2.getWritableDatabase()     // Catch:{ Throwable -> 0x0236, all -> 0x0231 }
            r2.setTransactionSuccessful()     // Catch:{ Throwable -> 0x0236, all -> 0x0231 }
            if (r5 == 0) goto L_0x01af
            r5.close()     // Catch:{ Throwable -> 0x01ee }
        L_0x01af:
            r0 = r19
            com.tencent.wxop.stat.ac r2 = r0.bW     // Catch:{ Throwable -> 0x01ee }
            android.database.sqlite.SQLiteDatabase r2 = r2.getWritableDatabase()     // Catch:{ Throwable -> 0x01ee }
            r2.endTransaction()     // Catch:{ Throwable -> 0x01ee }
        L_0x01ba:
            r0 = r19
            com.tencent.wxop.stat.b.c r2 = r0.bY     // Catch:{ all -> 0x01f5 }
            goto L_0x000b
        L_0x01c0:
            java.lang.String r4 = com.tencent.wxop.stat.b.l.c(r20)     // Catch:{ Throwable -> 0x0236, all -> 0x0231 }
            r2 = 1
            r7 = r4
            goto L_0x00b3
        L_0x01c8:
            java.lang.String r3 = com.tencent.wxop.stat.b.l.w(r20)     // Catch:{ Throwable -> 0x0236, all -> 0x0231 }
            if (r3 == 0) goto L_0x00d3
            int r11 = r3.length()     // Catch:{ Throwable -> 0x0236, all -> 0x0231 }
            if (r11 <= 0) goto L_0x00d3
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0236, all -> 0x0231 }
            r2.<init>()     // Catch:{ Throwable -> 0x0236, all -> 0x0231 }
            java.lang.StringBuilder r2 = r2.append(r7)     // Catch:{ Throwable -> 0x0236, all -> 0x0231 }
            java.lang.String r4 = ","
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ Throwable -> 0x0236, all -> 0x0231 }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Throwable -> 0x0236, all -> 0x0231 }
            java.lang.String r4 = r2.toString()     // Catch:{ Throwable -> 0x0236, all -> 0x0231 }
            r2 = 1
            goto L_0x00d3
        L_0x01ee:
            r2 = move-exception
            com.tencent.wxop.stat.b.b r3 = com.tencent.wxop.stat.t.bZ     // Catch:{ all -> 0x01f5 }
            r3.b(r2)     // Catch:{ all -> 0x01f5 }
            goto L_0x01ba
        L_0x01f5:
            r2 = move-exception
            monitor-exit(r19)
            throw r2
        L_0x01f8:
            r2 = move-exception
            r3 = r11
        L_0x01fa:
            com.tencent.wxop.stat.b.b r4 = com.tencent.wxop.stat.t.bZ     // Catch:{ all -> 0x0233 }
            r4.b(r2)     // Catch:{ all -> 0x0233 }
            if (r3 == 0) goto L_0x0204
            r3.close()     // Catch:{ Throwable -> 0x0210 }
        L_0x0204:
            r0 = r19
            com.tencent.wxop.stat.ac r2 = r0.bW     // Catch:{ Throwable -> 0x0210 }
            android.database.sqlite.SQLiteDatabase r2 = r2.getWritableDatabase()     // Catch:{ Throwable -> 0x0210 }
            r2.endTransaction()     // Catch:{ Throwable -> 0x0210 }
            goto L_0x01ba
        L_0x0210:
            r2 = move-exception
            com.tencent.wxop.stat.b.b r3 = com.tencent.wxop.stat.t.bZ     // Catch:{ all -> 0x01f5 }
            r3.b(r2)     // Catch:{ all -> 0x01f5 }
            goto L_0x01ba
        L_0x0217:
            r2 = move-exception
            r5 = r11
        L_0x0219:
            if (r5 == 0) goto L_0x021e
            r5.close()     // Catch:{ Throwable -> 0x022a }
        L_0x021e:
            r0 = r19
            com.tencent.wxop.stat.ac r3 = r0.bW     // Catch:{ Throwable -> 0x022a }
            android.database.sqlite.SQLiteDatabase r3 = r3.getWritableDatabase()     // Catch:{ Throwable -> 0x022a }
            r3.endTransaction()     // Catch:{ Throwable -> 0x022a }
        L_0x0229:
            throw r2     // Catch:{ all -> 0x01f5 }
        L_0x022a:
            r3 = move-exception
            com.tencent.wxop.stat.b.b r4 = com.tencent.wxop.stat.t.bZ     // Catch:{ all -> 0x01f5 }
            r4.b(r3)     // Catch:{ all -> 0x01f5 }
            goto L_0x0229
        L_0x0231:
            r2 = move-exception
            goto L_0x0219
        L_0x0233:
            r2 = move-exception
            r5 = r3
            goto L_0x0219
        L_0x0236:
            r2 = move-exception
            r3 = r5
            goto L_0x01fa
        L_0x0239:
            r2 = r3
            goto L_0x0156
        L_0x023c:
            r3 = r4
            goto L_0x00b1
        L_0x023f:
            r18 = r4
            r4 = r7
            r7 = r18
            goto L_0x00b3
        L_0x0246:
            r8 = r2
            goto L_0x0089
        L_0x0249:
            r2 = r9
            goto L_0x007c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.wxop.stat.t.t(android.content.Context):com.tencent.wxop.stat.b.c");
    }
}
