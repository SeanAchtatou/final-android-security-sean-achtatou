package com.tencent.wxop.stat;

import android.content.Context;
import com.tencent.wxop.stat.a.c;
import com.tencent.wxop.stat.a.f;

final class ap implements Runnable {
    final /* synthetic */ Throwable dn;
    final /* synthetic */ Context e;

    ap(Context context, Throwable th) {
        this.e = context;
        this.dn = th;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.wxop.stat.e.a(android.content.Context, boolean, com.tencent.wxop.stat.f):int
     arg types: [android.content.Context, int, ?[OBJECT, ARRAY]]
     candidates:
      com.tencent.wxop.stat.e.a(android.content.Context, java.lang.String, com.tencent.wxop.stat.f):void
      com.tencent.wxop.stat.e.a(android.content.Context, java.lang.String, java.lang.String):boolean
      com.tencent.wxop.stat.e.a(android.content.Context, boolean, com.tencent.wxop.stat.f):int */
    public final void run() {
        try {
            if (c.l()) {
                new p(new c(this.e, e.a(this.e, false, (f) null), this.dn, f.bw)).ah();
            }
        } catch (Throwable th) {
            e.aV.d("reportSdkSelfException error: " + th);
        }
    }
}
