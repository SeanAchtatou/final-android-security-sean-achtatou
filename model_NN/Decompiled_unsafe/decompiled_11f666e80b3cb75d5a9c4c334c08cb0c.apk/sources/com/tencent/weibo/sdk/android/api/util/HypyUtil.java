package com.tencent.weibo.sdk.android.api.util;

import org.apache.mina.proxy.handlers.socks.SocksProxyConstants;

public class HypyUtil {
    private static int BEGIN = 45217;
    private static int END = 63486;
    private static char[] chartable = {21834, 33453, 25830, 25645, 34558, 21457, 22134, 21704, 21704, 20987, 21888, 22403, 22920, 25343, 21734, 21866, 26399, 28982, 25746, 22604, 22604, 22604, 25366, 26132, 21387, 21277};
    private static char[] initialtable = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'h', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 't', 't', 'w', 'x', 'y', 'z'};
    private static int[] table = new int[27];

    static {
        for (int i = 0; i < 26; i++) {
            table[i] = gbValue(chartable[i]);
        }
        table[26] = END;
    }

    public static String cn2py(String str) {
        String str2 = "";
        int i = 0;
        while (i < str.length()) {
            try {
                String str3 = String.valueOf(str2) + Char2Initial(str.charAt(i));
                i++;
                str2 = str3;
            } catch (Exception e) {
                return "";
            }
        }
        return str2;
    }

    private static char Char2Initial(char c) {
        int gbValue;
        if (c >= 'a' && c <= 'z') {
            return (char) ((c - 'a') + 65);
        }
        if ((c >= 'A' && c <= 'Z') || (gbValue = gbValue(c)) < BEGIN || gbValue > END) {
            return c;
        }
        int i = 0;
        while (i < 26 && (gbValue < table[i] || gbValue >= table[i + 1])) {
            i++;
        }
        if (gbValue == END) {
            i = 25;
        }
        return initialtable[i];
    }

    private static int gbValue(char c) {
        try {
            byte[] bytes = (String.valueOf(new String()) + c).getBytes("GB2312");
            if (bytes.length < 2) {
                return 0;
            }
            return (bytes[1] & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD) + ((bytes[0] << 8) & 65280);
        } catch (Exception e) {
            return 0;
        }
    }
}
