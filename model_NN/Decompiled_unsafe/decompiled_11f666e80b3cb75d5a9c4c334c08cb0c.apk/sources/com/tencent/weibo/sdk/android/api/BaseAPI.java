package com.tencent.weibo.sdk.android.api;

import android.content.Context;
import android.util.Log;
import com.sina.weibo.sdk.auth.Oauth2AccessToken;
import com.sina.weibo.sdk.constant.WBConstants;
import com.tencent.weibo.sdk.android.api.util.Util;
import com.tencent.weibo.sdk.android.model.AccountModel;
import com.tencent.weibo.sdk.android.model.BaseVO;
import com.tencent.weibo.sdk.android.model.ModelResult;
import com.tencent.weibo.sdk.android.network.HttpCallback;
import com.tencent.weibo.sdk.android.network.HttpReqWeiBo;
import com.tencent.weibo.sdk.android.network.HttpService;
import com.tencent.weibo.sdk.android.network.ReqParam;

public abstract class BaseAPI {
    public static final String API_SERVER = "https://open.t.qq.com/api";
    public static final String HTTPMETHOD_GET = "GET";
    public static final String HTTPMETHOD_POST = "POST";
    public static final String REQUEST_METHOD_GET = "GET";
    public static final String REQUEST_METHOD_POST = "POST";
    private HttpCallback callback = new HttpCallback() {
        public void onResult(Object obj) {
            Log.d("sss", new StringBuilder().append(obj).toString());
            if (obj != null) {
                String[] split = ((ModelResult) obj).getObj().toString().split("&");
                String str = split[0].split("=")[1];
                BaseAPI.this.mAccessToken = str;
                String str2 = split[1].split("=")[1];
                String str3 = split[2].split("=")[1];
                String str4 = split[3].split("=")[1];
                String str5 = split[4].split("=")[1];
                String str6 = split[5].split("=")[1];
                Util.saveSharePersistent(BaseAPI.this.mContext, "ACCESS_TOKEN", str);
                Util.saveSharePersistent(BaseAPI.this.mContext, "EXPIRES_IN", str2);
                Util.saveSharePersistent(BaseAPI.this.mContext, "OPEN_ID", str4);
                Util.saveSharePersistent(BaseAPI.this.mContext, "REFRESH_TOKEN", str3);
                Util.saveSharePersistent(BaseAPI.this.mContext, "NAME", str5);
                Util.saveSharePersistent(BaseAPI.this.mContext, "NICK", str6);
                Util.saveSharePersistent(BaseAPI.this.mContext, "AUTHORIZETIME", String.valueOf(System.currentTimeMillis() / 1000));
                BaseAPI.this.weibo = new HttpReqWeiBo(BaseAPI.this.mContext, BaseAPI.this.mRequestUrl, BaseAPI.this.mmCallBack, BaseAPI.this.mmTargetClass, BaseAPI.this.mRequestMethod, Integer.valueOf(BaseAPI.this.mResultType));
                BaseAPI.this.mParams.addParam("access_token", BaseAPI.this.mAccessToken);
                BaseAPI.this.weibo.setParam(BaseAPI.this.mParams);
                HttpService.getInstance().addImmediateReq(BaseAPI.this.weibo);
            }
        }
    };
    /* access modifiers changed from: private */
    public String mAccessToken;
    private AccountModel mAccount;
    /* access modifiers changed from: private */
    public Context mContext;
    /* access modifiers changed from: private */
    public ReqParam mParams;
    /* access modifiers changed from: private */
    public String mRequestMethod;
    /* access modifiers changed from: private */
    public String mRequestUrl;
    /* access modifiers changed from: private */
    public int mResultType;
    /* access modifiers changed from: private */
    public HttpCallback mmCallBack;
    /* access modifiers changed from: private */
    public Class<? extends BaseVO> mmTargetClass;
    /* access modifiers changed from: private */
    public HttpReqWeiBo weibo;

    public BaseAPI(AccountModel accountModel) {
        this.mAccount = accountModel;
        if (this.mAccount != null) {
            this.mAccessToken = this.mAccount.getAccessToken();
        }
    }

    /* access modifiers changed from: protected */
    public void startRequest(Context context, String str, ReqParam reqParam, HttpCallback httpCallback, Class<? extends BaseVO> cls, String str2, int i) {
        if (isAuthorizeExpired(context)) {
            this.mContext = context;
            this.mRequestUrl = str;
            this.mParams = reqParam;
            this.mmCallBack = httpCallback;
            this.mmTargetClass = cls;
            this.mRequestMethod = str2;
            this.mResultType = i;
            this.weibo = new HttpReqWeiBo(context, "https://open.t.qq.com/cgi-bin/oauth2/access_token", this.callback, null, "GET", 4);
            this.weibo.setParam(refreshToken(context));
            HttpService.getInstance().addImmediateReq(this.weibo);
            return;
        }
        this.weibo = new HttpReqWeiBo(context, str, httpCallback, cls, str2, Integer.valueOf(i));
        reqParam.addParam("access_token", this.mAccessToken);
        this.weibo.setParam(reqParam);
        HttpService.getInstance().addImmediateReq(this.weibo);
    }

    private ReqParam refreshToken(Context context) {
        ReqParam reqParam = new ReqParam();
        String sharePersistent = Util.getSharePersistent(context, "CLIENT_ID");
        String sharePersistent2 = Util.getSharePersistent(context, "REFRESH_TOKEN");
        reqParam.addParam("client_id", sharePersistent);
        reqParam.addParam(WBConstants.AUTH_PARAMS_GRANT_TYPE, Oauth2AccessToken.KEY_REFRESH_TOKEN);
        reqParam.addParam(Oauth2AccessToken.KEY_REFRESH_TOKEN, sharePersistent2);
        reqParam.addParam("state", Integer.valueOf((((int) Math.random()) * 1000) + 111));
        return reqParam;
    }

    public boolean isAuthorizeExpired(Context context) {
        String sharePersistent = Util.getSharePersistent(context, "AUTHORIZETIME");
        System.out.println("===== : " + sharePersistent);
        String sharePersistent2 = Util.getSharePersistent(context, "EXPIRES_IN");
        System.out.println("====== : " + sharePersistent2);
        long currentTimeMillis = System.currentTimeMillis() / 1000;
        if (sharePersistent2 == null || sharePersistent == null) {
            return false;
        }
        if (Long.valueOf(sharePersistent2).longValue() + Long.valueOf(sharePersistent).longValue() < currentTimeMillis) {
            return true;
        }
        return false;
    }
}
