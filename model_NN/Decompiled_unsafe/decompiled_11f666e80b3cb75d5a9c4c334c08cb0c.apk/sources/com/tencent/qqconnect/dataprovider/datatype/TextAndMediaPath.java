package com.tencent.qqconnect.dataprovider.datatype;

import android.os.Parcel;
import android.os.Parcelable;

/* compiled from: ProGuard */
public class TextAndMediaPath implements Parcelable {
    public static final Parcelable.Creator<TextAndMediaPath> CREATOR = new Parcelable.Creator<TextAndMediaPath>() {
        public TextAndMediaPath createFromParcel(Parcel parcel) {
            return new TextAndMediaPath(parcel);
        }

        public TextAndMediaPath[] newArray(int i) {
            return new TextAndMediaPath[i];
        }
    };

    /* renamed from: a  reason: collision with root package name */
    private String f2547a;
    private String b;

    public TextAndMediaPath(String str, String str2) {
        this.f2547a = str;
        this.b = str2;
    }

    public String getText() {
        return this.f2547a;
    }

    public String getMediaPath() {
        return this.b;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.f2547a);
        parcel.writeString(this.b);
    }

    private TextAndMediaPath(Parcel parcel) {
        this.f2547a = parcel.readString();
        this.b = parcel.readString();
    }
}
