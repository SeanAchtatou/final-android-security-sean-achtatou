package com.tencent.stat.a;

import android.content.Context;
import com.qq.e.comm.constants.Constants;
import com.tencent.stat.common.k;
import org.json.JSONObject;

public class j extends e {

    /* renamed from: a  reason: collision with root package name */
    Long f2568a = null;
    String l;
    String m;

    public j(Context context, String str, String str2, int i, Long l2) {
        super(context, i);
        this.m = str;
        this.l = str2;
        this.f2568a = l2;
    }

    public f a() {
        return f.PAGE_VIEW;
    }

    public boolean a(JSONObject jSONObject) {
        k.a(jSONObject, "pi", this.l);
        k.a(jSONObject, Constants.KEYS.Banner_RF, this.m);
        if (this.f2568a == null) {
            return true;
        }
        jSONObject.put("du", this.f2568a);
        return true;
    }
}
