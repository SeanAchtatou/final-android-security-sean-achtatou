package com.tencent.stat.a;

import android.content.Context;
import com.tencent.stat.StatConfig;
import com.tencent.stat.common.k;
import java.util.Map;
import org.json.JSONObject;

public class a extends e {

    /* renamed from: a  reason: collision with root package name */
    Map<String, ?> f2559a = null;

    public a(Context context, int i, Map<String, ?> map) {
        super(context, i);
        this.f2559a = map;
    }

    public f a() {
        return f.ADDITION;
    }

    public boolean a(JSONObject jSONObject) {
        k.a(jSONObject, "qq", StatConfig.getQQ());
        if (this.f2559a == null || this.f2559a.size() <= 0) {
            return true;
        }
        for (Map.Entry next : this.f2559a.entrySet()) {
            jSONObject.put((String) next.getKey(), next.getValue());
        }
        return true;
    }
}
