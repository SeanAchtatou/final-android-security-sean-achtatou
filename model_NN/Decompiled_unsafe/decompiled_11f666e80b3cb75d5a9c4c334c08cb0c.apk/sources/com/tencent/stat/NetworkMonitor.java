package com.tencent.stat;

import com.baidu.mobads.interfaces.IXAdRequestInfo;
import com.meizu.cloud.pushsdk.notification.model.TimeDisplaySetting;
import org.json.JSONException;
import org.json.JSONObject;

public class NetworkMonitor {

    /* renamed from: a  reason: collision with root package name */
    private long f2550a = 0;
    private int b = 0;
    private String c = "";
    private int d = 0;
    private String e = "";

    public String getDomain() {
        return this.c;
    }

    public long getMillisecondsConsume() {
        return this.f2550a;
    }

    public int getPort() {
        return this.d;
    }

    public String getRemoteIp() {
        return this.e;
    }

    public int getStatusCode() {
        return this.b;
    }

    public void setDomain(String str) {
        this.c = str;
    }

    public void setMillisecondsConsume(long j) {
        this.f2550a = j;
    }

    public void setPort(int i) {
        this.d = i;
    }

    public void setRemoteIp(String str) {
        this.e = str;
    }

    public void setStatusCode(int i) {
        this.b = i;
    }

    public JSONObject toJSONObject() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put(IXAdRequestInfo.MAX_TITLE_LENGTH, this.f2550a);
            jSONObject.put(TimeDisplaySetting.START_SHOW_TIME, this.b);
            if (this.c != null) {
                jSONObject.put("dm", this.c);
            }
            jSONObject.put("pt", this.d);
            if (this.e != null) {
                jSONObject.put("rip", this.e);
            }
            jSONObject.put("ts", System.currentTimeMillis() / 1000);
        } catch (JSONException e2) {
        }
        return jSONObject;
    }
}
