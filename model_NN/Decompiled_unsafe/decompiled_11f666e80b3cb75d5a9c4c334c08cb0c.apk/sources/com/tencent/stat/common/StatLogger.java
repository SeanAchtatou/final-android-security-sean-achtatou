package com.tencent.stat.common;

import android.util.Log;
import org.apache.mina.proxy.handlers.http.HttpProxyConstants;

public final class StatLogger {

    /* renamed from: a  reason: collision with root package name */
    private String f2572a = "default";
    private boolean b = true;
    private int c = 2;

    public StatLogger() {
    }

    public StatLogger(String str) {
        this.f2572a = str;
    }

    private String a() {
        StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
        if (stackTrace == null) {
            return null;
        }
        for (StackTraceElement stackTraceElement : stackTrace) {
            if (!stackTraceElement.isNativeMethod() && !stackTraceElement.getClassName().equals(Thread.class.getName()) && !stackTraceElement.getClassName().equals(getClass().getName())) {
                return "[" + Thread.currentThread().getName() + "(" + Thread.currentThread().getId() + "): " + stackTraceElement.getFileName() + ":" + stackTraceElement.getLineNumber() + "]";
            }
        }
        return null;
    }

    public void d(Object obj) {
        if (isDebugEnable()) {
            debug(obj);
        }
    }

    public void debug(Object obj) {
        if (this.c <= 3) {
            String a2 = a();
            Log.d(this.f2572a, a2 == null ? obj.toString() : a2 + " - " + obj);
        }
    }

    public void e(Exception exc) {
        if (isDebugEnable()) {
            error(exc);
        }
    }

    public void e(Object obj) {
        if (isDebugEnable()) {
            error(obj);
        }
    }

    public void error(Exception exc) {
        if (this.c <= 6) {
            StringBuffer stringBuffer = new StringBuffer();
            String a2 = a();
            StackTraceElement[] stackTrace = exc.getStackTrace();
            if (a2 != null) {
                stringBuffer.append(a2 + " - " + exc + HttpProxyConstants.CRLF);
            } else {
                stringBuffer.append(exc + HttpProxyConstants.CRLF);
            }
            if (stackTrace != null && stackTrace.length > 0) {
                for (StackTraceElement stackTraceElement : stackTrace) {
                    if (stackTraceElement != null) {
                        stringBuffer.append("[ " + stackTraceElement.getFileName() + ":" + stackTraceElement.getLineNumber() + " ]\r\n");
                    }
                }
            }
            Log.e(this.f2572a, stringBuffer.toString());
        }
    }

    public void error(Object obj) {
        if (this.c <= 6) {
            String a2 = a();
            Log.e(this.f2572a, a2 == null ? obj.toString() : a2 + " - " + obj);
        }
    }

    public int getLogLevel() {
        return this.c;
    }

    public void i(Object obj) {
        if (isDebugEnable()) {
            info(obj);
        }
    }

    public void info(Object obj) {
        if (this.c <= 4) {
            String a2 = a();
            Log.i(this.f2572a, a2 == null ? obj.toString() : a2 + " - " + obj);
        }
    }

    public boolean isDebugEnable() {
        return this.b;
    }

    public void setDebugEnable(boolean z) {
        this.b = z;
    }

    public void setLogLevel(int i) {
        this.c = i;
    }

    public void setTag(String str) {
        this.f2572a = str;
    }

    public void v(Object obj) {
        if (isDebugEnable()) {
            verbose(obj);
        }
    }

    public void verbose(Object obj) {
        if (this.c <= 2) {
            String a2 = a();
            Log.v(this.f2572a, a2 == null ? obj.toString() : a2 + " - " + obj);
        }
    }

    public void w(Object obj) {
        if (isDebugEnable()) {
            warn(obj);
        }
    }

    public void warn(Object obj) {
        if (this.c <= 5) {
            String a2 = a();
            Log.w(this.f2572a, a2 == null ? obj.toString() : a2 + " - " + obj);
        }
    }
}
