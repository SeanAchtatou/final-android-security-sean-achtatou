package com.tencent.stat;

import android.content.ContentValues;
import android.content.Context;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteException;
import android.os.Handler;
import android.os.HandlerThread;
import com.meizu.cloud.pushsdk.pushtracer.constant.Parameters;
import com.meizu.cloud.pushsdk.pushtracer.storage.EventStoreHelper;
import com.tencent.stat.a.e;
import com.tencent.stat.common.StatLogger;
import com.tencent.stat.common.k;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class n {
    /* access modifiers changed from: private */
    public static StatLogger e = k.b();
    private static n f = null;

    /* renamed from: a  reason: collision with root package name */
    Handler f2592a = null;
    volatile int b = 0;
    DeviceInfo c = null;
    /* access modifiers changed from: private */
    public w d;
    private HashMap<String, String> g = new HashMap<>();

    private n(Context context) {
        try {
            HandlerThread handlerThread = new HandlerThread("StatStore");
            handlerThread.start();
            e.w("Launch store thread:" + handlerThread);
            this.f2592a = new Handler(handlerThread.getLooper());
            Context applicationContext = context.getApplicationContext();
            this.d = new w(applicationContext);
            this.d.getWritableDatabase();
            this.d.getReadableDatabase();
            b(applicationContext);
            c();
            f();
            this.f2592a.post(new o(this));
        } catch (Throwable th) {
            e.e(th);
        }
    }

    public static synchronized n a(Context context) {
        n nVar;
        synchronized (n.class) {
            if (f == null) {
                f = new n(context);
            }
            nVar = f;
        }
        return nVar;
    }

    public static n b() {
        return f;
    }

    /* access modifiers changed from: private */
    public synchronized void b(int i) {
        try {
            if (this.b > 0 && i > 0) {
                e.i("Load " + Integer.toString(this.b) + " unsent events");
                ArrayList arrayList = new ArrayList();
                ArrayList<x> arrayList2 = new ArrayList<>();
                if (i == -1 || i > StatConfig.a()) {
                    i = StatConfig.a();
                }
                this.b -= i;
                c(arrayList2, i);
                e.i("Peek " + Integer.toString(arrayList2.size()) + " unsent events.");
                if (!arrayList2.isEmpty()) {
                    b(arrayList2, 2);
                    for (x xVar : arrayList2) {
                        arrayList.add(xVar.b);
                    }
                    d.b().b(arrayList, new u(this, arrayList2, i));
                }
            }
        } catch (Throwable th) {
            e.e(th);
        }
        return;
    }

    /* access modifiers changed from: private */
    public synchronized void b(e eVar, c cVar) {
        if (StatConfig.getMaxStoreEventCount() > 0) {
            try {
                this.d.getWritableDatabase().beginTransaction();
                if (this.b > StatConfig.getMaxStoreEventCount()) {
                    e.warn("Too many events stored in db.");
                    this.b -= this.d.getWritableDatabase().delete(EventStoreHelper.TABLE_EVENTS, "event_id in (select event_id from events where timestamp in (select min(timestamp) from events) limit 1)", null);
                }
                ContentValues contentValues = new ContentValues();
                String c2 = k.c(eVar.d());
                contentValues.put("content", c2);
                contentValues.put("send_count", "0");
                contentValues.put("status", Integer.toString(1));
                contentValues.put(Parameters.TIMESTAMP, Long.valueOf(eVar.b()));
                if (this.d.getWritableDatabase().insert(EventStoreHelper.TABLE_EVENTS, null, contentValues) == -1) {
                    e.error("Failed to store event:" + c2);
                } else {
                    this.b++;
                    this.d.getWritableDatabase().setTransactionSuccessful();
                    if (cVar != null) {
                        cVar.a();
                    }
                }
                try {
                    this.d.getWritableDatabase().endTransaction();
                } catch (Throwable th) {
                }
            } catch (Throwable th2) {
            }
        }
        return;
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x00d7 A[SYNTHETIC, Splitter:B:36:0x00d7] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void b(com.tencent.stat.b r14) {
        /*
            r13 = this;
            r9 = 1
            r10 = 0
            r8 = 0
            monitor-enter(r13)
            java.lang.String r11 = r14.a()     // Catch:{ Throwable -> 0x00dd, all -> 0x00d3 }
            java.lang.String r0 = com.tencent.stat.common.k.a(r11)     // Catch:{ Throwable -> 0x00dd, all -> 0x00d3 }
            android.content.ContentValues r12 = new android.content.ContentValues     // Catch:{ Throwable -> 0x00dd, all -> 0x00d3 }
            r12.<init>()     // Catch:{ Throwable -> 0x00dd, all -> 0x00d3 }
            java.lang.String r1 = "content"
            org.json.JSONObject r2 = r14.b     // Catch:{ Throwable -> 0x00dd, all -> 0x00d3 }
            java.lang.String r2 = r2.toString()     // Catch:{ Throwable -> 0x00dd, all -> 0x00d3 }
            r12.put(r1, r2)     // Catch:{ Throwable -> 0x00dd, all -> 0x00d3 }
            java.lang.String r1 = "md5sum"
            r12.put(r1, r0)     // Catch:{ Throwable -> 0x00dd, all -> 0x00d3 }
            r14.c = r0     // Catch:{ Throwable -> 0x00dd, all -> 0x00d3 }
            java.lang.String r0 = "version"
            int r1 = r14.d     // Catch:{ Throwable -> 0x00dd, all -> 0x00d3 }
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)     // Catch:{ Throwable -> 0x00dd, all -> 0x00d3 }
            r12.put(r0, r1)     // Catch:{ Throwable -> 0x00dd, all -> 0x00d3 }
            com.tencent.stat.w r0 = r13.d     // Catch:{ Throwable -> 0x00dd, all -> 0x00d3 }
            android.database.sqlite.SQLiteDatabase r0 = r0.getReadableDatabase()     // Catch:{ Throwable -> 0x00dd, all -> 0x00d3 }
            java.lang.String r1 = "config"
            r2 = 0
            r3 = 0
            r4 = 0
            r5 = 0
            r6 = 0
            r7 = 0
            android.database.Cursor r1 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ Throwable -> 0x00dd, all -> 0x00d3 }
        L_0x0040:
            boolean r0 = r1.moveToNext()     // Catch:{ Throwable -> 0x00c4 }
            if (r0 == 0) goto L_0x00e0
            r0 = 0
            int r0 = r1.getInt(r0)     // Catch:{ Throwable -> 0x00c4 }
            int r2 = r14.f2570a     // Catch:{ Throwable -> 0x00c4 }
            if (r0 != r2) goto L_0x0040
            r0 = r9
        L_0x0050:
            if (r9 != r0) goto L_0x0092
            com.tencent.stat.w r0 = r13.d     // Catch:{ Throwable -> 0x00c4 }
            android.database.sqlite.SQLiteDatabase r0 = r0.getWritableDatabase()     // Catch:{ Throwable -> 0x00c4 }
            java.lang.String r2 = "config"
            java.lang.String r3 = "type=?"
            r4 = 1
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ Throwable -> 0x00c4 }
            r5 = 0
            int r6 = r14.f2570a     // Catch:{ Throwable -> 0x00c4 }
            java.lang.String r6 = java.lang.Integer.toString(r6)     // Catch:{ Throwable -> 0x00c4 }
            r4[r5] = r6     // Catch:{ Throwable -> 0x00c4 }
            int r0 = r0.update(r2, r12, r3, r4)     // Catch:{ Throwable -> 0x00c4 }
            long r2 = (long) r0     // Catch:{ Throwable -> 0x00c4 }
        L_0x006d:
            r4 = -1
            int r0 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r0 != 0) goto L_0x00ab
            com.tencent.stat.common.StatLogger r0 = com.tencent.stat.n.e     // Catch:{ Throwable -> 0x00c4 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x00c4 }
            r2.<init>()     // Catch:{ Throwable -> 0x00c4 }
            java.lang.String r3 = "Failed to store cfg:"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Throwable -> 0x00c4 }
            java.lang.StringBuilder r2 = r2.append(r11)     // Catch:{ Throwable -> 0x00c4 }
            java.lang.String r2 = r2.toString()     // Catch:{ Throwable -> 0x00c4 }
            r0.e(r2)     // Catch:{ Throwable -> 0x00c4 }
        L_0x008b:
            if (r1 == 0) goto L_0x0090
            r1.close()     // Catch:{ all -> 0x00d0 }
        L_0x0090:
            monitor-exit(r13)
            return
        L_0x0092:
            java.lang.String r0 = "type"
            int r2 = r14.f2570a     // Catch:{ Throwable -> 0x00c4 }
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)     // Catch:{ Throwable -> 0x00c4 }
            r12.put(r0, r2)     // Catch:{ Throwable -> 0x00c4 }
            com.tencent.stat.w r0 = r13.d     // Catch:{ Throwable -> 0x00c4 }
            android.database.sqlite.SQLiteDatabase r0 = r0.getWritableDatabase()     // Catch:{ Throwable -> 0x00c4 }
            java.lang.String r2 = "config"
            r3 = 0
            long r2 = r0.insert(r2, r3, r12)     // Catch:{ Throwable -> 0x00c4 }
            goto L_0x006d
        L_0x00ab:
            com.tencent.stat.common.StatLogger r0 = com.tencent.stat.n.e     // Catch:{ Throwable -> 0x00c4 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x00c4 }
            r2.<init>()     // Catch:{ Throwable -> 0x00c4 }
            java.lang.String r3 = "Sucessed to store cfg:"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Throwable -> 0x00c4 }
            java.lang.StringBuilder r2 = r2.append(r11)     // Catch:{ Throwable -> 0x00c4 }
            java.lang.String r2 = r2.toString()     // Catch:{ Throwable -> 0x00c4 }
            r0.d(r2)     // Catch:{ Throwable -> 0x00c4 }
            goto L_0x008b
        L_0x00c4:
            r0 = move-exception
        L_0x00c5:
            com.tencent.stat.common.StatLogger r2 = com.tencent.stat.n.e     // Catch:{ all -> 0x00db }
            r2.e(r0)     // Catch:{ all -> 0x00db }
            if (r1 == 0) goto L_0x0090
            r1.close()     // Catch:{ all -> 0x00d0 }
            goto L_0x0090
        L_0x00d0:
            r0 = move-exception
            monitor-exit(r13)
            throw r0
        L_0x00d3:
            r0 = move-exception
            r1 = r8
        L_0x00d5:
            if (r1 == 0) goto L_0x00da
            r1.close()     // Catch:{ all -> 0x00d0 }
        L_0x00da:
            throw r0     // Catch:{ all -> 0x00d0 }
        L_0x00db:
            r0 = move-exception
            goto L_0x00d5
        L_0x00dd:
            r0 = move-exception
            r1 = r8
            goto L_0x00c5
        L_0x00e0:
            r0 = r10
            goto L_0x0050
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.stat.n.b(com.tencent.stat.b):void");
    }

    /* access modifiers changed from: private */
    public synchronized void b(List<x> list) {
        e.i("Delete " + list.size() + " sent events in thread:" + Thread.currentThread());
        try {
            this.d.getWritableDatabase().beginTransaction();
            for (x xVar : list) {
                this.b -= this.d.getWritableDatabase().delete(EventStoreHelper.TABLE_EVENTS, "event_id = ?", new String[]{Long.toString(xVar.f2601a)});
            }
            this.d.getWritableDatabase().setTransactionSuccessful();
            this.b = (int) DatabaseUtils.queryNumEntries(this.d.getReadableDatabase(), EventStoreHelper.TABLE_EVENTS);
            try {
                this.d.getWritableDatabase().endTransaction();
            } catch (SQLiteException e2) {
                e.e((Exception) e2);
            }
        } catch (Throwable th) {
            try {
                this.d.getWritableDatabase().endTransaction();
            } catch (SQLiteException e3) {
                e.e((Exception) e3);
            }
            throw th;
        }
        return;
    }

    /* access modifiers changed from: private */
    public synchronized void b(List<x> list, int i) {
        e.i("Update " + list.size() + " sending events to status:" + i + " in thread:" + Thread.currentThread());
        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put("status", Integer.toString(i));
            this.d.getWritableDatabase().beginTransaction();
            for (x next : list) {
                if (next.d + 1 > StatConfig.getMaxSendRetryCount()) {
                    this.b -= this.d.getWritableDatabase().delete(EventStoreHelper.TABLE_EVENTS, "event_id=?", new String[]{Long.toString(next.f2601a)});
                } else {
                    contentValues.put("send_count", Integer.valueOf(next.d + 1));
                    e.i("Update event:" + next.f2601a + " for content:" + contentValues);
                    int update = this.d.getWritableDatabase().update(EventStoreHelper.TABLE_EVENTS, contentValues, "event_id=?", new String[]{Long.toString(next.f2601a)});
                    if (update <= 0) {
                        e.e("Failed to update db, error code:" + Integer.toString(update));
                    }
                }
            }
            this.d.getWritableDatabase().setTransactionSuccessful();
            this.b = (int) DatabaseUtils.queryNumEntries(this.d.getReadableDatabase(), EventStoreHelper.TABLE_EVENTS);
            try {
                this.d.getWritableDatabase().endTransaction();
            } catch (SQLiteException e2) {
                e.e((Exception) e2);
            }
        } catch (Throwable th) {
            try {
                this.d.getWritableDatabase().endTransaction();
            } catch (SQLiteException e3) {
                e.e((Exception) e3);
            }
            throw th;
        }
        return;
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x0060  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void c(java.util.List<com.tencent.stat.x> r11, int r12) {
        /*
            r10 = this;
            r9 = 0
            com.tencent.stat.w r0 = r10.d     // Catch:{ Throwable -> 0x006a, all -> 0x005d }
            android.database.sqlite.SQLiteDatabase r0 = r0.getReadableDatabase()     // Catch:{ Throwable -> 0x006a, all -> 0x005d }
            java.lang.String r1 = "events"
            r2 = 0
            java.lang.String r3 = "status=?"
            r4 = 1
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ Throwable -> 0x006a, all -> 0x005d }
            r5 = 0
            r6 = 1
            java.lang.String r6 = java.lang.Integer.toString(r6)     // Catch:{ Throwable -> 0x006a, all -> 0x005d }
            r4[r5] = r6     // Catch:{ Throwable -> 0x006a, all -> 0x005d }
            r5 = 0
            r6 = 0
            java.lang.String r7 = "event_id"
            java.lang.String r8 = java.lang.Integer.toString(r12)     // Catch:{ Throwable -> 0x006a, all -> 0x005d }
            android.database.Cursor r7 = r0.query(r1, r2, r3, r4, r5, r6, r7, r8)     // Catch:{ Throwable -> 0x006a, all -> 0x005d }
        L_0x0023:
            boolean r0 = r7.moveToNext()     // Catch:{ Throwable -> 0x004a, all -> 0x0064 }
            if (r0 == 0) goto L_0x0057
            r0 = 0
            long r2 = r7.getLong(r0)     // Catch:{ Throwable -> 0x004a, all -> 0x0064 }
            r0 = 1
            java.lang.String r0 = r7.getString(r0)     // Catch:{ Throwable -> 0x004a, all -> 0x0064 }
            java.lang.String r4 = com.tencent.stat.common.k.d(r0)     // Catch:{ Throwable -> 0x004a, all -> 0x0064 }
            r0 = 2
            int r5 = r7.getInt(r0)     // Catch:{ Throwable -> 0x004a, all -> 0x0064 }
            r0 = 3
            int r6 = r7.getInt(r0)     // Catch:{ Throwable -> 0x004a, all -> 0x0064 }
            com.tencent.stat.x r1 = new com.tencent.stat.x     // Catch:{ Throwable -> 0x004a, all -> 0x0064 }
            r1.<init>(r2, r4, r5, r6)     // Catch:{ Throwable -> 0x004a, all -> 0x0064 }
            r11.add(r1)     // Catch:{ Throwable -> 0x004a, all -> 0x0064 }
            goto L_0x0023
        L_0x004a:
            r0 = move-exception
            r1 = r7
        L_0x004c:
            com.tencent.stat.common.StatLogger r2 = com.tencent.stat.n.e     // Catch:{ all -> 0x0067 }
            r2.e(r0)     // Catch:{ all -> 0x0067 }
            if (r1 == 0) goto L_0x0056
            r1.close()
        L_0x0056:
            return
        L_0x0057:
            if (r7 == 0) goto L_0x0056
            r7.close()
            goto L_0x0056
        L_0x005d:
            r0 = move-exception
        L_0x005e:
            if (r9 == 0) goto L_0x0063
            r9.close()
        L_0x0063:
            throw r0
        L_0x0064:
            r0 = move-exception
            r9 = r7
            goto L_0x005e
        L_0x0067:
            r0 = move-exception
            r9 = r1
            goto L_0x005e
        L_0x006a:
            r0 = move-exception
            r1 = r9
            goto L_0x004c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.stat.n.c(java.util.List, int):void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    /* access modifiers changed from: private */
    public void e() {
        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put("status", (Integer) 1);
            this.d.getWritableDatabase().update(EventStoreHelper.TABLE_EVENTS, contentValues, "status=?", new String[]{Long.toString(2)});
            this.b = (int) DatabaseUtils.queryNumEntries(this.d.getReadableDatabase(), EventStoreHelper.TABLE_EVENTS);
            e.i("Total " + this.b + " unsent events.");
        } catch (Throwable th) {
            e.e(th);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x003f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void f() {
        /*
            r9 = this;
            r8 = 0
            com.tencent.stat.w r0 = r9.d     // Catch:{ Throwable -> 0x0045, all -> 0x003b }
            android.database.sqlite.SQLiteDatabase r0 = r0.getReadableDatabase()     // Catch:{ Throwable -> 0x0045, all -> 0x003b }
            java.lang.String r1 = "keyvalues"
            r2 = 0
            r3 = 0
            r4 = 0
            r5 = 0
            r6 = 0
            r7 = 0
            android.database.Cursor r1 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ Throwable -> 0x0045, all -> 0x003b }
        L_0x0013:
            boolean r0 = r1.moveToNext()     // Catch:{ Throwable -> 0x0029 }
            if (r0 == 0) goto L_0x0035
            java.util.HashMap<java.lang.String, java.lang.String> r0 = r9.g     // Catch:{ Throwable -> 0x0029 }
            r2 = 0
            java.lang.String r2 = r1.getString(r2)     // Catch:{ Throwable -> 0x0029 }
            r3 = 1
            java.lang.String r3 = r1.getString(r3)     // Catch:{ Throwable -> 0x0029 }
            r0.put(r2, r3)     // Catch:{ Throwable -> 0x0029 }
            goto L_0x0013
        L_0x0029:
            r0 = move-exception
        L_0x002a:
            com.tencent.stat.common.StatLogger r2 = com.tencent.stat.n.e     // Catch:{ all -> 0x0043 }
            r2.e(r0)     // Catch:{ all -> 0x0043 }
            if (r1 == 0) goto L_0x0034
            r1.close()
        L_0x0034:
            return
        L_0x0035:
            if (r1 == 0) goto L_0x0034
            r1.close()
            goto L_0x0034
        L_0x003b:
            r0 = move-exception
            r1 = r8
        L_0x003d:
            if (r1 == 0) goto L_0x0042
            r1.close()
        L_0x0042:
            throw r0
        L_0x0043:
            r0 = move-exception
            goto L_0x003d
        L_0x0045:
            r0 = move-exception
            r1 = r8
            goto L_0x002a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.stat.n.f():void");
    }

    public int a() {
        return this.b;
    }

    /* access modifiers changed from: package-private */
    public void a(int i) {
        this.f2592a.post(new v(this, i));
    }

    /* access modifiers changed from: package-private */
    public void a(e eVar, c cVar) {
        if (StatConfig.isEnableStatService()) {
            try {
                if (Thread.currentThread().getId() == this.f2592a.getLooper().getThread().getId()) {
                    b(eVar, cVar);
                } else {
                    this.f2592a.post(new r(this, eVar, cVar));
                }
            } catch (Throwable th) {
                e.e(th);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a(b bVar) {
        if (bVar != null) {
            this.f2592a.post(new s(this, bVar));
        }
    }

    /* access modifiers changed from: package-private */
    public void a(List<x> list) {
        try {
            if (Thread.currentThread().getId() == this.f2592a.getLooper().getThread().getId()) {
                b(list);
            } else {
                this.f2592a.post(new q(this, list));
            }
        } catch (SQLiteException e2) {
            e.e((Exception) e2);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(List<x> list, int i) {
        try {
            if (Thread.currentThread().getId() == this.f2592a.getLooper().getThread().getId()) {
                b(list, i);
            } else {
                this.f2592a.post(new p(this, list, i));
            }
        } catch (Throwable th) {
            e.e(th);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    /* JADX WARNING: Removed duplicated region for block: B:70:0x01cf A[SYNTHETIC, Splitter:B:70:0x01cf] */
    /* JADX WARNING: Removed duplicated region for block: B:78:0x01da A[SYNTHETIC, Splitter:B:78:0x01da] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized com.tencent.stat.DeviceInfo b(android.content.Context r20) {
        /*
            r19 = this;
            monitor-enter(r19)
            r0 = r19
            com.tencent.stat.DeviceInfo r2 = r0.c     // Catch:{ all -> 0x01d3 }
            if (r2 == 0) goto L_0x000d
            r0 = r19
            com.tencent.stat.DeviceInfo r2 = r0.c     // Catch:{ all -> 0x01d3 }
        L_0x000b:
            monitor-exit(r19)
            return r2
        L_0x000d:
            r11 = 0
            r0 = r19
            com.tencent.stat.w r2 = r0.d     // Catch:{ Throwable -> 0x01c6, all -> 0x01d6 }
            android.database.sqlite.SQLiteDatabase r2 = r2.getReadableDatabase()     // Catch:{ Throwable -> 0x01c6, all -> 0x01d6 }
            java.lang.String r3 = "user"
            r4 = 0
            r5 = 0
            r6 = 0
            r7 = 0
            r8 = 0
            r9 = 0
            r10 = 0
            android.database.Cursor r5 = r2.query(r3, r4, r5, r6, r7, r8, r9, r10)     // Catch:{ Throwable -> 0x01c6, all -> 0x01d6 }
            r2 = 0
            java.lang.String r3 = ""
            boolean r3 = r5.moveToNext()     // Catch:{ Throwable -> 0x01e3, all -> 0x01de }
            if (r3 == 0) goto L_0x0117
            r2 = 0
            java.lang.String r10 = r5.getString(r2)     // Catch:{ Throwable -> 0x01e3, all -> 0x01de }
            java.lang.String r7 = com.tencent.stat.common.k.d(r10)     // Catch:{ Throwable -> 0x01e3, all -> 0x01de }
            r2 = 1
            int r9 = r5.getInt(r2)     // Catch:{ Throwable -> 0x01e3, all -> 0x01de }
            r2 = 2
            java.lang.String r3 = r5.getString(r2)     // Catch:{ Throwable -> 0x01e3, all -> 0x01de }
            r2 = 3
            long r12 = r5.getLong(r2)     // Catch:{ Throwable -> 0x01e3, all -> 0x01de }
            r6 = 1
            long r14 = java.lang.System.currentTimeMillis()     // Catch:{ Throwable -> 0x01e3, all -> 0x01de }
            r16 = 1000(0x3e8, double:4.94E-321)
            long r14 = r14 / r16
            r2 = 1
            if (r9 == r2) goto L_0x01fb
            r16 = 1000(0x3e8, double:4.94E-321)
            long r12 = r12 * r16
            java.lang.String r2 = com.tencent.stat.common.k.a(r12)     // Catch:{ Throwable -> 0x01e3, all -> 0x01de }
            r12 = 1000(0x3e8, double:4.94E-321)
            long r12 = r12 * r14
            java.lang.String r4 = com.tencent.stat.common.k.a(r12)     // Catch:{ Throwable -> 0x01e3, all -> 0x01de }
            boolean r2 = r2.equals(r4)     // Catch:{ Throwable -> 0x01e3, all -> 0x01de }
            if (r2 != 0) goto L_0x01fb
            r2 = 1
        L_0x0066:
            java.lang.String r4 = com.tencent.stat.common.k.r(r20)     // Catch:{ Throwable -> 0x01e3, all -> 0x01de }
            boolean r3 = r3.equals(r4)     // Catch:{ Throwable -> 0x01e3, all -> 0x01de }
            if (r3 != 0) goto L_0x01f8
            r2 = r2 | 2
            r8 = r2
        L_0x0073:
            java.lang.String r2 = ","
            java.lang.String[] r11 = r7.split(r2)     // Catch:{ Throwable -> 0x01e3, all -> 0x01de }
            r4 = 0
            if (r11 == 0) goto L_0x0193
            int r2 = r11.length     // Catch:{ Throwable -> 0x01e3, all -> 0x01de }
            if (r2 <= 0) goto L_0x0193
            r2 = 0
            r3 = r11[r2]     // Catch:{ Throwable -> 0x01e3, all -> 0x01de }
            if (r3 == 0) goto L_0x008c
            int r2 = r3.length()     // Catch:{ Throwable -> 0x01e3, all -> 0x01de }
            r12 = 11
            if (r2 >= r12) goto L_0x01f0
        L_0x008c:
            java.lang.String r2 = com.tencent.stat.common.k.l(r20)     // Catch:{ Throwable -> 0x01e3, all -> 0x01de }
            if (r2 == 0) goto L_0x01ec
            int r12 = r2.length()     // Catch:{ Throwable -> 0x01e3, all -> 0x01de }
            r13 = 10
            if (r12 <= r13) goto L_0x01ec
            r3 = 1
        L_0x009b:
            r4 = r7
            r7 = r2
        L_0x009d:
            if (r11 == 0) goto L_0x01a0
            int r2 = r11.length     // Catch:{ Throwable -> 0x01e3, all -> 0x01de }
            r12 = 2
            if (r2 < r12) goto L_0x01a0
            r2 = 1
            r2 = r11[r2]     // Catch:{ Throwable -> 0x01e3, all -> 0x01de }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x01e3, all -> 0x01de }
            r4.<init>()     // Catch:{ Throwable -> 0x01e3, all -> 0x01de }
            java.lang.StringBuilder r4 = r4.append(r7)     // Catch:{ Throwable -> 0x01e3, all -> 0x01de }
            java.lang.String r11 = ","
            java.lang.StringBuilder r4 = r4.append(r11)     // Catch:{ Throwable -> 0x01e3, all -> 0x01de }
            java.lang.StringBuilder r4 = r4.append(r2)     // Catch:{ Throwable -> 0x01e3, all -> 0x01de }
            java.lang.String r4 = r4.toString()     // Catch:{ Throwable -> 0x01e3, all -> 0x01de }
        L_0x00bd:
            com.tencent.stat.DeviceInfo r11 = new com.tencent.stat.DeviceInfo     // Catch:{ Throwable -> 0x01e3, all -> 0x01de }
            r11.<init>(r7, r2, r8)     // Catch:{ Throwable -> 0x01e3, all -> 0x01de }
            r0 = r19
            r0.c = r11     // Catch:{ Throwable -> 0x01e3, all -> 0x01de }
            android.content.ContentValues r2 = new android.content.ContentValues     // Catch:{ Throwable -> 0x01e3, all -> 0x01de }
            r2.<init>()     // Catch:{ Throwable -> 0x01e3, all -> 0x01de }
            java.lang.String r4 = com.tencent.stat.common.k.c(r4)     // Catch:{ Throwable -> 0x01e3, all -> 0x01de }
            java.lang.String r7 = "uid"
            r2.put(r7, r4)     // Catch:{ Throwable -> 0x01e3, all -> 0x01de }
            java.lang.String r4 = "user_type"
            java.lang.Integer r7 = java.lang.Integer.valueOf(r8)     // Catch:{ Throwable -> 0x01e3, all -> 0x01de }
            r2.put(r4, r7)     // Catch:{ Throwable -> 0x01e3, all -> 0x01de }
            java.lang.String r4 = "app_ver"
            java.lang.String r7 = com.tencent.stat.common.k.r(r20)     // Catch:{ Throwable -> 0x01e3, all -> 0x01de }
            r2.put(r4, r7)     // Catch:{ Throwable -> 0x01e3, all -> 0x01de }
            java.lang.String r4 = "ts"
            java.lang.Long r7 = java.lang.Long.valueOf(r14)     // Catch:{ Throwable -> 0x01e3, all -> 0x01de }
            r2.put(r4, r7)     // Catch:{ Throwable -> 0x01e3, all -> 0x01de }
            if (r3 == 0) goto L_0x0106
            r0 = r19
            com.tencent.stat.w r3 = r0.d     // Catch:{ Throwable -> 0x01e3, all -> 0x01de }
            android.database.sqlite.SQLiteDatabase r3 = r3.getWritableDatabase()     // Catch:{ Throwable -> 0x01e3, all -> 0x01de }
            java.lang.String r4 = "user"
            java.lang.String r7 = "uid=?"
            r11 = 1
            java.lang.String[] r11 = new java.lang.String[r11]     // Catch:{ Throwable -> 0x01e3, all -> 0x01de }
            r12 = 0
            r11[r12] = r10     // Catch:{ Throwable -> 0x01e3, all -> 0x01de }
            r3.update(r4, r2, r7, r11)     // Catch:{ Throwable -> 0x01e3, all -> 0x01de }
        L_0x0106:
            if (r8 == r9) goto L_0x01e9
            r0 = r19
            com.tencent.stat.w r3 = r0.d     // Catch:{ Throwable -> 0x01e3, all -> 0x01de }
            android.database.sqlite.SQLiteDatabase r3 = r3.getWritableDatabase()     // Catch:{ Throwable -> 0x01e3, all -> 0x01de }
            java.lang.String r4 = "user"
            r7 = 0
            r3.replace(r4, r7, r2)     // Catch:{ Throwable -> 0x01e3, all -> 0x01de }
            r2 = r6
        L_0x0117:
            if (r2 != 0) goto L_0x0188
            java.lang.String r3 = com.tencent.stat.common.k.b(r20)     // Catch:{ Throwable -> 0x01e3, all -> 0x01de }
            java.lang.String r4 = com.tencent.stat.common.k.c(r20)     // Catch:{ Throwable -> 0x01e3, all -> 0x01de }
            if (r4 == 0) goto L_0x01e6
            int r2 = r4.length()     // Catch:{ Throwable -> 0x01e3, all -> 0x01de }
            if (r2 <= 0) goto L_0x01e6
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x01e3, all -> 0x01de }
            r2.<init>()     // Catch:{ Throwable -> 0x01e3, all -> 0x01de }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Throwable -> 0x01e3, all -> 0x01de }
            java.lang.String r6 = ","
            java.lang.StringBuilder r2 = r2.append(r6)     // Catch:{ Throwable -> 0x01e3, all -> 0x01de }
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ Throwable -> 0x01e3, all -> 0x01de }
            java.lang.String r2 = r2.toString()     // Catch:{ Throwable -> 0x01e3, all -> 0x01de }
        L_0x0140:
            r6 = 0
            long r8 = java.lang.System.currentTimeMillis()     // Catch:{ Throwable -> 0x01e3, all -> 0x01de }
            r10 = 1000(0x3e8, double:4.94E-321)
            long r8 = r8 / r10
            java.lang.String r7 = com.tencent.stat.common.k.r(r20)     // Catch:{ Throwable -> 0x01e3, all -> 0x01de }
            android.content.ContentValues r10 = new android.content.ContentValues     // Catch:{ Throwable -> 0x01e3, all -> 0x01de }
            r10.<init>()     // Catch:{ Throwable -> 0x01e3, all -> 0x01de }
            java.lang.String r2 = com.tencent.stat.common.k.c(r2)     // Catch:{ Throwable -> 0x01e3, all -> 0x01de }
            java.lang.String r11 = "uid"
            r10.put(r11, r2)     // Catch:{ Throwable -> 0x01e3, all -> 0x01de }
            java.lang.String r2 = "user_type"
            java.lang.Integer r11 = java.lang.Integer.valueOf(r6)     // Catch:{ Throwable -> 0x01e3, all -> 0x01de }
            r10.put(r2, r11)     // Catch:{ Throwable -> 0x01e3, all -> 0x01de }
            java.lang.String r2 = "app_ver"
            r10.put(r2, r7)     // Catch:{ Throwable -> 0x01e3, all -> 0x01de }
            java.lang.String r2 = "ts"
            java.lang.Long r7 = java.lang.Long.valueOf(r8)     // Catch:{ Throwable -> 0x01e3, all -> 0x01de }
            r10.put(r2, r7)     // Catch:{ Throwable -> 0x01e3, all -> 0x01de }
            r0 = r19
            com.tencent.stat.w r2 = r0.d     // Catch:{ Throwable -> 0x01e3, all -> 0x01de }
            android.database.sqlite.SQLiteDatabase r2 = r2.getWritableDatabase()     // Catch:{ Throwable -> 0x01e3, all -> 0x01de }
            java.lang.String r7 = "user"
            r8 = 0
            r2.insert(r7, r8, r10)     // Catch:{ Throwable -> 0x01e3, all -> 0x01de }
            com.tencent.stat.DeviceInfo r2 = new com.tencent.stat.DeviceInfo     // Catch:{ Throwable -> 0x01e3, all -> 0x01de }
            r2.<init>(r3, r4, r6)     // Catch:{ Throwable -> 0x01e3, all -> 0x01de }
            r0 = r19
            r0.c = r2     // Catch:{ Throwable -> 0x01e3, all -> 0x01de }
        L_0x0188:
            if (r5 == 0) goto L_0x018d
            r5.close()     // Catch:{ all -> 0x01d3 }
        L_0x018d:
            r0 = r19
            com.tencent.stat.DeviceInfo r2 = r0.c     // Catch:{ all -> 0x01d3 }
            goto L_0x000b
        L_0x0193:
            java.lang.String r3 = com.tencent.stat.common.k.b(r20)     // Catch:{ Throwable -> 0x01e3, all -> 0x01de }
            r4 = 1
            r7 = r3
            r18 = r4
            r4 = r3
            r3 = r18
            goto L_0x009d
        L_0x01a0:
            java.lang.String r2 = com.tencent.stat.common.k.c(r20)     // Catch:{ Throwable -> 0x01e3, all -> 0x01de }
            if (r2 == 0) goto L_0x00bd
            int r11 = r2.length()     // Catch:{ Throwable -> 0x01e3, all -> 0x01de }
            if (r11 <= 0) goto L_0x00bd
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x01e3, all -> 0x01de }
            r3.<init>()     // Catch:{ Throwable -> 0x01e3, all -> 0x01de }
            java.lang.StringBuilder r3 = r3.append(r7)     // Catch:{ Throwable -> 0x01e3, all -> 0x01de }
            java.lang.String r4 = ","
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Throwable -> 0x01e3, all -> 0x01de }
            java.lang.StringBuilder r3 = r3.append(r2)     // Catch:{ Throwable -> 0x01e3, all -> 0x01de }
            java.lang.String r4 = r3.toString()     // Catch:{ Throwable -> 0x01e3, all -> 0x01de }
            r3 = 1
            goto L_0x00bd
        L_0x01c6:
            r2 = move-exception
            r3 = r11
        L_0x01c8:
            com.tencent.stat.common.StatLogger r4 = com.tencent.stat.n.e     // Catch:{ all -> 0x01e0 }
            r4.e(r2)     // Catch:{ all -> 0x01e0 }
            if (r3 == 0) goto L_0x018d
            r3.close()     // Catch:{ all -> 0x01d3 }
            goto L_0x018d
        L_0x01d3:
            r2 = move-exception
            monitor-exit(r19)
            throw r2
        L_0x01d6:
            r2 = move-exception
            r5 = r11
        L_0x01d8:
            if (r5 == 0) goto L_0x01dd
            r5.close()     // Catch:{ all -> 0x01d3 }
        L_0x01dd:
            throw r2     // Catch:{ all -> 0x01d3 }
        L_0x01de:
            r2 = move-exception
            goto L_0x01d8
        L_0x01e0:
            r2 = move-exception
            r5 = r3
            goto L_0x01d8
        L_0x01e3:
            r2 = move-exception
            r3 = r5
            goto L_0x01c8
        L_0x01e6:
            r2 = r3
            goto L_0x0140
        L_0x01e9:
            r2 = r6
            goto L_0x0117
        L_0x01ec:
            r2 = r3
            r3 = r4
            goto L_0x009b
        L_0x01f0:
            r18 = r3
            r3 = r4
            r4 = r7
            r7 = r18
            goto L_0x009d
        L_0x01f8:
            r8 = r2
            goto L_0x0073
        L_0x01fb:
            r2 = r9
            goto L_0x0066
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.stat.n.b(android.content.Context):com.tencent.stat.DeviceInfo");
    }

    /* access modifiers changed from: package-private */
    public void c() {
        this.f2592a.post(new t(this));
    }
}
