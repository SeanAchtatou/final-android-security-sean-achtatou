package com.tencent.stat;

import android.content.Context;
import com.tencent.stat.a.d;
import java.lang.Thread;

final class g implements Thread.UncaughtExceptionHandler {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Context f2585a;

    g(Context context) {
        this.f2585a = context;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.stat.StatService.a(android.content.Context, boolean):int
     arg types: [android.content.Context, int]
     candidates:
      com.tencent.stat.StatService.a(android.content.Context, java.lang.Throwable):void
      com.tencent.stat.StatService.a(android.content.Context, java.util.Map<java.lang.String, ?>):void
      com.tencent.stat.StatService.a(android.content.Context, boolean):int */
    public void uncaughtException(Thread thread, Throwable th) {
        if (StatConfig.isEnableStatService()) {
            n.a(this.f2585a).a(new d(this.f2585a, StatService.a(this.f2585a, false), 2, th), (c) null);
            StatService.i.debug("MTA has caught the following uncaught exception:");
            StatService.i.error(th);
            if (StatService.j != null) {
                StatService.i.debug("Call the original uncaught exception handler.");
                StatService.j.uncaughtException(thread, th);
                return;
            }
            StatService.i.debug("Original uncaught exception handler not set.");
        }
    }
}
