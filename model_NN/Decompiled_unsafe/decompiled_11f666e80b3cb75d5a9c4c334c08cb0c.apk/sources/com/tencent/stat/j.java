package com.tencent.stat;

import android.content.Context;
import com.tencent.stat.a.i;
import com.tencent.stat.common.k;
import com.xiaomi.mipush.sdk.MiPushClient;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONArray;

class j implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private Context f2588a = null;
    private Map<String, Integer> b = null;

    public j(Context context, Map<String, Integer> map) {
        this.f2588a = context;
        if (map != null) {
            this.b = map;
        }
    }

    private NetworkMonitor a(String str, int i) {
        NetworkMonitor networkMonitor = new NetworkMonitor();
        Socket socket = new Socket();
        int i2 = 0;
        try {
            networkMonitor.setDomain(str);
            networkMonitor.setPort(i);
            long currentTimeMillis = System.currentTimeMillis();
            InetSocketAddress inetSocketAddress = new InetSocketAddress(str, i);
            socket.connect(inetSocketAddress, 30000);
            networkMonitor.setMillisecondsConsume(System.currentTimeMillis() - currentTimeMillis);
            networkMonitor.setRemoteIp(inetSocketAddress.getAddress().getHostAddress());
            if (socket != null) {
                socket.close();
            }
            if (socket != null) {
                try {
                    socket.close();
                } catch (Throwable th) {
                    StatService.i.e(th);
                }
            }
        } catch (IOException e) {
            IOException iOException = e;
            i2 = -1;
            StatService.i.e((Exception) iOException);
            if (socket != null) {
                socket.close();
            }
        } catch (Throwable th2) {
            StatService.i.e(th2);
        }
        networkMonitor.setStatusCode(i2);
        return networkMonitor;
    }

    private Map<String, Integer> a() {
        String str;
        HashMap hashMap = new HashMap();
        String a2 = StatConfig.a("__MTA_TEST_SPEED__", (String) null);
        if (!(a2 == null || a2.trim().length() == 0)) {
            for (String split : a2.split(";")) {
                String[] split2 = split.split(MiPushClient.ACCEPT_TIME_SEPARATOR);
                if (!(split2 == null || split2.length != 2 || (str = split2[0]) == null || str.trim().length() == 0)) {
                    try {
                        hashMap.put(str, Integer.valueOf(Integer.valueOf(split2[1]).intValue()));
                    } catch (NumberFormatException e) {
                        StatService.i.e((Exception) e);
                    }
                }
            }
        }
        return hashMap;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.stat.StatService.a(android.content.Context, boolean):int
     arg types: [android.content.Context, int]
     candidates:
      com.tencent.stat.StatService.a(android.content.Context, java.lang.Throwable):void
      com.tencent.stat.StatService.a(android.content.Context, java.util.Map<java.lang.String, ?>):void
      com.tencent.stat.StatService.a(android.content.Context, boolean):int */
    public void run() {
        try {
            if (k.h(this.f2588a)) {
                if (this.b == null) {
                    this.b = a();
                }
                if (this.b == null || this.b.size() == 0) {
                    StatService.i.w("empty domain list.");
                    return;
                }
                JSONArray jSONArray = new JSONArray();
                for (Map.Entry next : this.b.entrySet()) {
                    String str = (String) next.getKey();
                    if (str == null || str.length() == 0) {
                        StatService.i.w("empty domain name.");
                    } else if (((Integer) next.getValue()) == null) {
                        StatService.i.w("port is null for " + str);
                    } else {
                        jSONArray.put(a((String) next.getKey(), ((Integer) next.getValue()).intValue()).toJSONObject());
                    }
                }
                if (jSONArray.length() != 0) {
                    i iVar = new i(this.f2588a, StatService.a(this.f2588a, false));
                    iVar.a(jSONArray.toString());
                    if (StatService.c(this.f2588a) != null) {
                        StatService.c(this.f2588a).post(new k(iVar));
                    }
                }
            }
        } catch (Throwable th) {
            StatService.i.e(th);
        }
    }
}
