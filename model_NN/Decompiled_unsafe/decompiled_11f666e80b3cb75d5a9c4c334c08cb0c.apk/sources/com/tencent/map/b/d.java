package com.tencent.map.b;

import android.content.Context;
import android.telephony.NeighboringCellInfo;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import java.lang.reflect.Method;
import java.util.LinkedList;
import java.util.List;

public final class d {

    /* renamed from: a  reason: collision with root package name */
    private Context f2426a = null;
    /* access modifiers changed from: private */
    public TelephonyManager b = null;
    private a c = null;
    /* access modifiers changed from: private */
    public c d = null;
    /* access modifiers changed from: private */
    public b e = null;
    private boolean f = false;
    /* access modifiers changed from: private */
    public List<NeighboringCellInfo> g = new LinkedList();
    private byte[] h = new byte[0];
    /* access modifiers changed from: private */
    public byte[] i = new byte[0];
    /* access modifiers changed from: private */
    public boolean j = false;

    public class a extends PhoneStateListener {

        /* renamed from: a  reason: collision with root package name */
        private int f2428a = 0;
        private int b = 0;
        private int c = 0;
        private int d = 0;
        private int e = 0;
        private int f = -1;
        private int g = Integer.MAX_VALUE;
        private int h = Integer.MAX_VALUE;
        private Method i = null;
        private Method j = null;
        private Method k = null;
        private Method l = null;
        private Method m = null;

        public a(int i2, int i3) {
            this.b = i2;
            this.f2428a = i3;
        }

        /* JADX WARNING: Removed duplicated region for block: B:22:0x0076 A[SYNTHETIC, Splitter:B:22:0x0076] */
        /* JADX WARNING: Removed duplicated region for block: B:43:? A[RETURN, SYNTHETIC] */
        /* JADX WARNING: Removed duplicated region for block: B:6:0x0039  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final void onCellLocationChanged(android.telephony.CellLocation r12) {
            /*
                r11 = this;
                r5 = 2147483647(0x7fffffff, float:NaN)
                r4 = 3
                r3 = -1
                r1 = 0
                r11.f = r3
                r11.e = r3
                r11.d = r3
                r11.c = r3
                if (r12 == 0) goto L_0x0015
                int r0 = r11.f2428a
                switch(r0) {
                    case 1: goto L_0x0049;
                    case 2: goto L_0x00aa;
                    default: goto L_0x0015;
                }
            L_0x0015:
                com.tencent.map.b.d r10 = com.tencent.map.b.d.this
                com.tencent.map.b.d$b r0 = new com.tencent.map.b.d$b
                com.tencent.map.b.d r1 = com.tencent.map.b.d.this
                int r2 = r11.f2428a
                int r3 = r11.b
                int r4 = r11.c
                int r5 = r11.d
                int r6 = r11.e
                int r7 = r11.f
                int r8 = r11.g
                int r9 = r11.h
                r0.<init>(r1, r2, r3, r4, r5, r6, r7, r8, r9)
                com.tencent.map.b.d.b unused = r10.e = r0
                com.tencent.map.b.d r0 = com.tencent.map.b.d.this
                com.tencent.map.b.d$c r0 = r0.d
                if (r0 == 0) goto L_0x0048
                com.tencent.map.b.d r0 = com.tencent.map.b.d.this
                com.tencent.map.b.d$c r0 = r0.d
                com.tencent.map.b.d r1 = com.tencent.map.b.d.this
                com.tencent.map.b.d$b r1 = r1.e
                r0.a(r1)
            L_0x0048:
                return
            L_0x0049:
                r0 = 0
                android.telephony.gsm.GsmCellLocation r12 = (android.telephony.gsm.GsmCellLocation) r12     // Catch:{ Exception -> 0x009e }
                int r0 = r12.getLac()     // Catch:{ Exception -> 0x0169 }
                if (r0 > 0) goto L_0x0065
                int r0 = r12.getCid()     // Catch:{ Exception -> 0x0169 }
                if (r0 > 0) goto L_0x0065
                com.tencent.map.b.d r0 = com.tencent.map.b.d.this     // Catch:{ Exception -> 0x0169 }
                android.telephony.TelephonyManager r0 = r0.b     // Catch:{ Exception -> 0x0169 }
                android.telephony.CellLocation r0 = r0.getCellLocation()     // Catch:{ Exception -> 0x0169 }
                android.telephony.gsm.GsmCellLocation r0 = (android.telephony.gsm.GsmCellLocation) r0     // Catch:{ Exception -> 0x0169 }
                r12 = r0
            L_0x0065:
                r0 = 1
            L_0x0066:
                if (r0 == 0) goto L_0x0015
                if (r12 == 0) goto L_0x0015
                com.tencent.map.b.d r0 = com.tencent.map.b.d.this
                android.telephony.TelephonyManager r0 = r0.b
                java.lang.String r0 = r0.getNetworkOperator()
                if (r0 == 0) goto L_0x008b
                int r1 = r0.length()     // Catch:{ Exception -> 0x00a2 }
                if (r1 <= r4) goto L_0x008b
                r1 = 3
                java.lang.String r0 = r0.substring(r1)     // Catch:{ Exception -> 0x00a2 }
                java.lang.Integer r0 = java.lang.Integer.valueOf(r0)     // Catch:{ Exception -> 0x00a2 }
                int r0 = r0.intValue()     // Catch:{ Exception -> 0x00a2 }
                r11.c = r0     // Catch:{ Exception -> 0x00a2 }
            L_0x008b:
                int r0 = r12.getLac()     // Catch:{ Exception -> 0x00a2 }
                r11.d = r0     // Catch:{ Exception -> 0x00a2 }
                int r0 = r12.getCid()     // Catch:{ Exception -> 0x00a2 }
                r11.e = r0     // Catch:{ Exception -> 0x00a2 }
            L_0x0097:
                com.tencent.map.b.d r0 = com.tencent.map.b.d.this
                com.tencent.map.b.d.c(r0)
                goto L_0x0015
            L_0x009e:
                r2 = move-exception
                r12 = r0
            L_0x00a0:
                r0 = r1
                goto L_0x0066
            L_0x00a2:
                r0 = move-exception
                r11.e = r3
                r11.d = r3
                r11.c = r3
                goto L_0x0097
            L_0x00aa:
                if (r12 == 0) goto L_0x0015
                java.lang.reflect.Method r0 = r11.i     // Catch:{ Exception -> 0x015c }
                if (r0 != 0) goto L_0x0105
                java.lang.String r0 = "android.telephony.cdma.CdmaCellLocation"
                java.lang.Class r0 = java.lang.Class.forName(r0)     // Catch:{ Exception -> 0x015c }
                java.lang.String r1 = "getBaseStationId"
                r2 = 0
                java.lang.Class[] r2 = new java.lang.Class[r2]     // Catch:{ Exception -> 0x015c }
                java.lang.reflect.Method r0 = r0.getMethod(r1, r2)     // Catch:{ Exception -> 0x015c }
                r11.i = r0     // Catch:{ Exception -> 0x015c }
                java.lang.String r0 = "android.telephony.cdma.CdmaCellLocation"
                java.lang.Class r0 = java.lang.Class.forName(r0)     // Catch:{ Exception -> 0x015c }
                java.lang.String r1 = "getSystemId"
                r2 = 0
                java.lang.Class[] r2 = new java.lang.Class[r2]     // Catch:{ Exception -> 0x015c }
                java.lang.reflect.Method r0 = r0.getMethod(r1, r2)     // Catch:{ Exception -> 0x015c }
                r11.j = r0     // Catch:{ Exception -> 0x015c }
                java.lang.String r0 = "android.telephony.cdma.CdmaCellLocation"
                java.lang.Class r0 = java.lang.Class.forName(r0)     // Catch:{ Exception -> 0x015c }
                java.lang.String r1 = "getNetworkId"
                r2 = 0
                java.lang.Class[] r2 = new java.lang.Class[r2]     // Catch:{ Exception -> 0x015c }
                java.lang.reflect.Method r0 = r0.getMethod(r1, r2)     // Catch:{ Exception -> 0x015c }
                r11.k = r0     // Catch:{ Exception -> 0x015c }
                java.lang.String r0 = "android.telephony.cdma.CdmaCellLocation"
                java.lang.Class r0 = java.lang.Class.forName(r0)     // Catch:{ Exception -> 0x015c }
                java.lang.String r1 = "getBaseStationLatitude"
                r2 = 0
                java.lang.Class[] r2 = new java.lang.Class[r2]     // Catch:{ Exception -> 0x015c }
                java.lang.reflect.Method r0 = r0.getMethod(r1, r2)     // Catch:{ Exception -> 0x015c }
                r11.l = r0     // Catch:{ Exception -> 0x015c }
                java.lang.String r0 = "android.telephony.cdma.CdmaCellLocation"
                java.lang.Class r0 = java.lang.Class.forName(r0)     // Catch:{ Exception -> 0x015c }
                java.lang.String r1 = "getBaseStationLongitude"
                r2 = 0
                java.lang.Class[] r2 = new java.lang.Class[r2]     // Catch:{ Exception -> 0x015c }
                java.lang.reflect.Method r0 = r0.getMethod(r1, r2)     // Catch:{ Exception -> 0x015c }
                r11.m = r0     // Catch:{ Exception -> 0x015c }
            L_0x0105:
                java.lang.reflect.Method r0 = r11.j     // Catch:{ Exception -> 0x015c }
                r1 = 0
                java.lang.Object[] r1 = new java.lang.Object[r1]     // Catch:{ Exception -> 0x015c }
                java.lang.Object r0 = r0.invoke(r12, r1)     // Catch:{ Exception -> 0x015c }
                java.lang.Integer r0 = (java.lang.Integer) r0     // Catch:{ Exception -> 0x015c }
                int r0 = r0.intValue()     // Catch:{ Exception -> 0x015c }
                r11.c = r0     // Catch:{ Exception -> 0x015c }
                java.lang.reflect.Method r0 = r11.k     // Catch:{ Exception -> 0x015c }
                r1 = 0
                java.lang.Object[] r1 = new java.lang.Object[r1]     // Catch:{ Exception -> 0x015c }
                java.lang.Object r0 = r0.invoke(r12, r1)     // Catch:{ Exception -> 0x015c }
                java.lang.Integer r0 = (java.lang.Integer) r0     // Catch:{ Exception -> 0x015c }
                int r0 = r0.intValue()     // Catch:{ Exception -> 0x015c }
                r11.d = r0     // Catch:{ Exception -> 0x015c }
                java.lang.reflect.Method r0 = r11.i     // Catch:{ Exception -> 0x015c }
                r1 = 0
                java.lang.Object[] r1 = new java.lang.Object[r1]     // Catch:{ Exception -> 0x015c }
                java.lang.Object r0 = r0.invoke(r12, r1)     // Catch:{ Exception -> 0x015c }
                java.lang.Integer r0 = (java.lang.Integer) r0     // Catch:{ Exception -> 0x015c }
                int r0 = r0.intValue()     // Catch:{ Exception -> 0x015c }
                r11.e = r0     // Catch:{ Exception -> 0x015c }
                java.lang.reflect.Method r0 = r11.l     // Catch:{ Exception -> 0x015c }
                r1 = 0
                java.lang.Object[] r1 = new java.lang.Object[r1]     // Catch:{ Exception -> 0x015c }
                java.lang.Object r0 = r0.invoke(r12, r1)     // Catch:{ Exception -> 0x015c }
                java.lang.Integer r0 = (java.lang.Integer) r0     // Catch:{ Exception -> 0x015c }
                int r0 = r0.intValue()     // Catch:{ Exception -> 0x015c }
                r11.g = r0     // Catch:{ Exception -> 0x015c }
                java.lang.reflect.Method r0 = r11.m     // Catch:{ Exception -> 0x015c }
                r1 = 0
                java.lang.Object[] r1 = new java.lang.Object[r1]     // Catch:{ Exception -> 0x015c }
                java.lang.Object r0 = r0.invoke(r12, r1)     // Catch:{ Exception -> 0x015c }
                java.lang.Integer r0 = (java.lang.Integer) r0     // Catch:{ Exception -> 0x015c }
                int r0 = r0.intValue()     // Catch:{ Exception -> 0x015c }
                r11.h = r0     // Catch:{ Exception -> 0x015c }
                goto L_0x0015
            L_0x015c:
                r0 = move-exception
                r11.e = r3
                r11.d = r3
                r11.c = r3
                r11.g = r5
                r11.h = r5
                goto L_0x0015
            L_0x0169:
                r0 = move-exception
                goto L_0x00a0
            */
            throw new UnsupportedOperationException("Method not decompiled: com.tencent.map.b.d.a.onCellLocationChanged(android.telephony.CellLocation):void");
        }

        public final void onSignalStrengthChanged(int i2) {
            if (this.f2428a == 1) {
                d.c(d.this);
            }
            if (Math.abs(i2 - ((this.f + 113) / 2)) <= 3) {
                return;
            }
            if (this.f == -1) {
                this.f = (i2 << 1) - 113;
                return;
            }
            this.f = (i2 << 1) - 113;
            b unused = d.this.e = new b(d.this, this.f2428a, this.b, this.c, this.d, this.e, this.f, this.g, this.h);
            if (d.this.d != null) {
                d.this.d.a(d.this.e);
            }
        }
    }

    public class b implements Cloneable {

        /* renamed from: a  reason: collision with root package name */
        public int f2429a = 0;
        public int b = 0;
        public int c = 0;
        public int d = 0;
        public int e = 0;
        public int f = 0;
        public int g = Integer.MAX_VALUE;
        public int h = Integer.MAX_VALUE;

        public b(d dVar, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
            this.f2429a = i;
            this.b = i2;
            this.c = i3;
            this.d = i4;
            this.e = i5;
            this.f = i6;
            this.g = i7;
            this.h = i8;
        }

        public final Object clone() {
            try {
                return (b) super.clone();
            } catch (Exception e2) {
                return null;
            }
        }
    }

    public interface c {
        void a(b bVar);
    }

    private int a(int i2) {
        int i3;
        String networkOperator = this.b.getNetworkOperator();
        if (networkOperator != null && networkOperator.length() >= 3) {
            try {
                i3 = Integer.valueOf(networkOperator.substring(0, 3)).intValue();
            } catch (Exception e2) {
            }
            if (i2 == 2 || i3 != -1) {
                return i3;
            }
            return 0;
        }
        i3 = -1;
        if (i2 == 2) {
        }
        return i3;
    }

    static /* synthetic */ void c(d dVar) {
        if (!dVar.j) {
            dVar.j = true;
            new Thread() {
                /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                 method: com.tencent.map.b.d.a(com.tencent.map.b.d, boolean):boolean
                 arg types: [com.tencent.map.b.d, int]
                 candidates:
                  com.tencent.map.b.d.a(com.tencent.map.b.d, com.tencent.map.b.d$b):com.tencent.map.b.d$b
                  com.tencent.map.b.d.a(android.content.Context, com.tencent.map.b.d$c):boolean
                  com.tencent.map.b.d.a(com.tencent.map.b.d, boolean):boolean */
                public final void run() {
                    if (d.this.b != null) {
                        List neighboringCellInfo = d.this.b.getNeighboringCellInfo();
                        synchronized (d.this.i) {
                            if (neighboringCellInfo != null) {
                                d.this.g.clear();
                                d.this.g.addAll(neighboringCellInfo);
                            }
                        }
                    }
                    boolean unused = d.this.j = false;
                }
            }.start();
        }
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a() {
        /*
            r4 = this;
            byte[] r1 = r4.h
            monitor-enter(r1)
            boolean r0 = r4.f     // Catch:{ all -> 0x001e }
            if (r0 != 0) goto L_0x0009
            monitor-exit(r1)     // Catch:{ all -> 0x001e }
        L_0x0008:
            return
        L_0x0009:
            android.telephony.TelephonyManager r0 = r4.b     // Catch:{ all -> 0x001e }
            if (r0 == 0) goto L_0x0019
            com.tencent.map.b.d$a r0 = r4.c     // Catch:{ all -> 0x001e }
            if (r0 == 0) goto L_0x0019
            android.telephony.TelephonyManager r0 = r4.b     // Catch:{ Exception -> 0x0021 }
            com.tencent.map.b.d$a r2 = r4.c     // Catch:{ Exception -> 0x0021 }
            r3 = 0
            r0.listen(r2, r3)     // Catch:{ Exception -> 0x0021 }
        L_0x0019:
            r0 = 0
            r4.f = r0     // Catch:{ all -> 0x001e }
            monitor-exit(r1)     // Catch:{ all -> 0x001e }
            goto L_0x0008
        L_0x001e:
            r0 = move-exception
            monitor-exit(r1)
            throw r0
        L_0x0021:
            r0 = move-exception
            r0 = 0
            r4.f = r0     // Catch:{ all -> 0x001e }
            goto L_0x0019
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.map.b.d.a():void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:39:?, code lost:
        return false;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean a(android.content.Context r6, com.tencent.map.b.d.c r7) {
        /*
            r5 = this;
            r0 = 1
            r1 = 0
            byte[] r2 = r5.h
            monitor-enter(r2)
            boolean r3 = r5.f     // Catch:{ all -> 0x0055 }
            if (r3 == 0) goto L_0x000b
            monitor-exit(r2)     // Catch:{ all -> 0x0055 }
        L_0x000a:
            return r0
        L_0x000b:
            if (r6 == 0) goto L_0x000f
            if (r7 != 0) goto L_0x0012
        L_0x000f:
            monitor-exit(r2)
            r0 = r1
            goto L_0x000a
        L_0x0012:
            r5.f2426a = r6     // Catch:{ all -> 0x0055 }
            r5.d = r7     // Catch:{ all -> 0x0055 }
            android.content.Context r0 = r5.f2426a     // Catch:{ Exception -> 0x0051 }
            java.lang.String r3 = "phone"
            java.lang.Object r0 = r0.getSystemService(r3)     // Catch:{ Exception -> 0x0051 }
            android.telephony.TelephonyManager r0 = (android.telephony.TelephonyManager) r0     // Catch:{ Exception -> 0x0051 }
            r5.b = r0     // Catch:{ Exception -> 0x0051 }
            android.telephony.TelephonyManager r0 = r5.b     // Catch:{ Exception -> 0x0051 }
            if (r0 != 0) goto L_0x0029
            monitor-exit(r2)     // Catch:{ all -> 0x0055 }
            r0 = r1
            goto L_0x000a
        L_0x0029:
            android.telephony.TelephonyManager r0 = r5.b     // Catch:{ Exception -> 0x0051 }
            int r0 = r0.getPhoneType()     // Catch:{ Exception -> 0x0051 }
            int r3 = r5.a(r0)     // Catch:{ Exception -> 0x0051 }
            com.tencent.map.b.d$a r4 = new com.tencent.map.b.d$a     // Catch:{ Exception -> 0x0051 }
            r4.<init>(r3, r0)     // Catch:{ Exception -> 0x0051 }
            r5.c = r4     // Catch:{ Exception -> 0x0051 }
            com.tencent.map.b.d$a r0 = r5.c     // Catch:{ Exception -> 0x0051 }
            if (r0 != 0) goto L_0x0041
            monitor-exit(r2)     // Catch:{ all -> 0x0055 }
            r0 = r1
            goto L_0x000a
        L_0x0041:
            android.telephony.TelephonyManager r0 = r5.b     // Catch:{ Exception -> 0x0051 }
            com.tencent.map.b.d$a r3 = r5.c     // Catch:{ Exception -> 0x0051 }
            r4 = 18
            r0.listen(r3, r4)     // Catch:{ Exception -> 0x0051 }
            r0 = 1
            r5.f = r0     // Catch:{ all -> 0x0055 }
            monitor-exit(r2)     // Catch:{ all -> 0x0055 }
            boolean r0 = r5.f
            goto L_0x000a
        L_0x0051:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x0055 }
            r0 = r1
            goto L_0x000a
        L_0x0055:
            r0 = move-exception
            monitor-exit(r2)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.map.b.d.a(android.content.Context, com.tencent.map.b.d$c):boolean");
    }

    public final List<NeighboringCellInfo> b() {
        LinkedList linkedList = null;
        synchronized (this.i) {
            if (this.g != null) {
                linkedList = new LinkedList();
                linkedList.addAll(this.g);
            }
        }
        return linkedList;
    }
}
