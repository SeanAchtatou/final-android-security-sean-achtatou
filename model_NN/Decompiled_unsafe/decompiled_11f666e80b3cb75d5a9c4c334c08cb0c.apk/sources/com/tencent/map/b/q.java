package com.tencent.map.b;

import android.net.Proxy;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/* compiled from: ProGuard */
public final class q {

    /* renamed from: a  reason: collision with root package name */
    private static int f2450a = 0;
    private static boolean b;

    /* JADX WARNING: Removed duplicated region for block: B:40:0x009f A[Catch:{ IOException -> 0x00e6 }] */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x00dc  */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x00e0 A[FALL_THROUGH, SYNTHETIC, Splitter:B:59:0x00e0] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.net.HttpURLConnection a(java.lang.String r8, boolean r9) {
        /*
            r6 = -1
            r1 = 0
            r3 = 1
            r2 = 0
            java.net.URL r4 = new java.net.URL     // Catch:{ MalformedURLException -> 0x0019 }
            r4.<init>(r8)     // Catch:{ MalformedURLException -> 0x0019 }
            boolean r0 = com.tencent.map.b.l.c()
            if (r0 == 0) goto L_0x001f
            r0 = r2
        L_0x0010:
            if (r0 != 0) goto L_0x0036
            java.net.URLConnection r0 = r4.openConnection()     // Catch:{ IOException -> 0x0030 }
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ IOException -> 0x0030 }
        L_0x0018:
            return r0
        L_0x0019:
            r0 = move-exception
            r0.printStackTrace()
            r0 = r1
            goto L_0x0018
        L_0x001f:
            com.tencent.map.b.m.a()
            java.lang.String r0 = android.net.Proxy.getDefaultHost()
            boolean r0 = com.tencent.map.b.b.a(r0)
            if (r0 == 0) goto L_0x002e
            r0 = r2
            goto L_0x0010
        L_0x002e:
            r0 = r3
            goto L_0x0010
        L_0x0030:
            r0 = move-exception
            r0.printStackTrace()
            r0 = r1
            goto L_0x0018
        L_0x0036:
            int r0 = com.tencent.map.b.q.f2450a
            if (r0 != 0) goto L_0x009a
            boolean r0 = com.tencent.map.b.q.b
            if (r0 != 0) goto L_0x009a
            com.tencent.map.b.q.b = r3
            java.net.URL r3 = new java.net.URL     // Catch:{ MalformedURLException -> 0x00bf }
            java.lang.String r0 = "http://ls.map.soso.com/monitor/monitor.html"
            r3.<init>(r0)     // Catch:{ MalformedURLException -> 0x00bf }
            java.lang.String r2 = android.net.Proxy.getDefaultHost()
            int r0 = android.net.Proxy.getDefaultPort()
            if (r0 != r6) goto L_0x0053
            r0 = 80
        L_0x0053:
            java.net.InetSocketAddress r5 = new java.net.InetSocketAddress
            r5.<init>(r2, r0)
            java.net.Proxy r0 = new java.net.Proxy
            java.net.Proxy$Type r2 = java.net.Proxy.Type.HTTP
            r0.<init>(r2, r5)
            java.net.URLConnection r0 = r3.openConnection(r0)     // Catch:{ Exception -> 0x00f5, all -> 0x00d9 }
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ Exception -> 0x00f5, all -> 0x00d9 }
            java.lang.String r2 = "GET"
            r0.setRequestMethod(r2)     // Catch:{ Exception -> 0x00c8, all -> 0x00ed }
            r2 = 15000(0x3a98, float:2.102E-41)
            r0.setConnectTimeout(r2)     // Catch:{ Exception -> 0x00c8, all -> 0x00ed }
            r2 = 45000(0xafc8, float:6.3058E-41)
            r0.setReadTimeout(r2)     // Catch:{ Exception -> 0x00c8, all -> 0x00ed }
            java.lang.String r2 = "User-Agent"
            java.lang.String r3 = "QQ Map Mobile"
            r0.setRequestProperty(r2, r3)     // Catch:{ Exception -> 0x00c8, all -> 0x00ed }
            r2 = 1
            r0.setDoInput(r2)     // Catch:{ Exception -> 0x00c8, all -> 0x00ed }
            r2 = 0
            r0.setDoOutput(r2)     // Catch:{ Exception -> 0x00c8, all -> 0x00ed }
            r2 = 0
            r0.setUseCaches(r2)     // Catch:{ Exception -> 0x00c8, all -> 0x00ed }
            boolean r2 = a(r0)     // Catch:{ Exception -> 0x00c8, all -> 0x00ed }
            r0.connect()     // Catch:{ Exception -> 0x00c8, all -> 0x00ed }
            if (r2 == 0) goto L_0x00c3
            r2 = 1
            a(r2)     // Catch:{ Exception -> 0x00c8, all -> 0x00ed }
        L_0x0095:
            if (r0 == 0) goto L_0x009a
            r0.disconnect()
        L_0x009a:
            int r0 = com.tencent.map.b.q.f2450a     // Catch:{ IOException -> 0x00e6 }
            switch(r0) {
                case 2: goto L_0x00e0;
                default: goto L_0x009f;
            }     // Catch:{ IOException -> 0x00e6 }
        L_0x009f:
            java.lang.String r2 = android.net.Proxy.getDefaultHost()     // Catch:{ IOException -> 0x00e6 }
            int r0 = android.net.Proxy.getDefaultPort()     // Catch:{ IOException -> 0x00e6 }
            if (r0 != r6) goto L_0x00ab
            r0 = 80
        L_0x00ab:
            java.net.InetSocketAddress r3 = new java.net.InetSocketAddress     // Catch:{ IOException -> 0x00e6 }
            r3.<init>(r2, r0)     // Catch:{ IOException -> 0x00e6 }
            java.net.Proxy r0 = new java.net.Proxy     // Catch:{ IOException -> 0x00e6 }
            java.net.Proxy$Type r2 = java.net.Proxy.Type.HTTP     // Catch:{ IOException -> 0x00e6 }
            r0.<init>(r2, r3)     // Catch:{ IOException -> 0x00e6 }
            java.net.URLConnection r0 = r4.openConnection(r0)     // Catch:{ IOException -> 0x00e6 }
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ IOException -> 0x00e6 }
            goto L_0x0018
        L_0x00bf:
            r0 = move-exception
            com.tencent.map.b.q.b = r2
            goto L_0x009a
        L_0x00c3:
            r2 = 2
            a(r2)     // Catch:{ Exception -> 0x00c8, all -> 0x00ed }
            goto L_0x0095
        L_0x00c8:
            r2 = move-exception
            r7 = r2
            r2 = r0
            r0 = r7
        L_0x00cc:
            r0.printStackTrace()     // Catch:{ all -> 0x00f2 }
            r0 = 2
            a(r0)     // Catch:{ all -> 0x00f2 }
            if (r2 == 0) goto L_0x009a
            r2.disconnect()
            goto L_0x009a
        L_0x00d9:
            r0 = move-exception
        L_0x00da:
            if (r1 == 0) goto L_0x00df
            r1.disconnect()
        L_0x00df:
            throw r0
        L_0x00e0:
            java.net.HttpURLConnection r0 = a(r4, r8)     // Catch:{ IOException -> 0x00e6 }
            goto L_0x0018
        L_0x00e6:
            r0 = move-exception
            r0.printStackTrace()
            r0 = r1
            goto L_0x0018
        L_0x00ed:
            r1 = move-exception
            r7 = r1
            r1 = r0
            r0 = r7
            goto L_0x00da
        L_0x00f2:
            r0 = move-exception
            r1 = r2
            goto L_0x00da
        L_0x00f5:
            r0 = move-exception
            r2 = r1
            goto L_0x00cc
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.map.b.q.a(java.lang.String, boolean):java.net.HttpURLConnection");
    }

    /* JADX WARNING: Removed duplicated region for block: B:54:0x00b2  */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x00b7  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.tencent.map.b.n a(boolean r6, java.lang.String r7, java.lang.String r8, java.lang.String r9, byte[] r10, boolean r11, boolean r12) throws java.lang.Exception {
        /*
            r2 = 1
            r0 = 0
            r1 = 0
            boolean r3 = com.tencent.map.b.l.d()
            if (r3 != 0) goto L_0x000f
            com.tencent.map.b.r r0 = new com.tencent.map.b.r
            r0.<init>()
            throw r0
        L_0x000f:
            java.net.HttpURLConnection r3 = a(r7, r12)     // Catch:{ p -> 0x011b, Exception -> 0x0115, all -> 0x010f }
            r4 = 0
            boolean r4 = com.tencent.map.b.b.a(r4)     // Catch:{ p -> 0x00a7, Exception -> 0x00c2 }
            if (r4 == 0) goto L_0x00a0
            java.net.URL r4 = r3.getURL()     // Catch:{ p -> 0x00a7, Exception -> 0x00c2 }
            java.lang.String r4 = r4.getHost()     // Catch:{ p -> 0x00a7, Exception -> 0x00c2 }
            boolean r4 = com.tencent.map.b.b.a(r4)     // Catch:{ p -> 0x00a7, Exception -> 0x00c2 }
            if (r4 == 0) goto L_0x0028
        L_0x0028:
            if (r6 == 0) goto L_0x00bb
            java.lang.String r4 = "GET"
            r3.setRequestMethod(r4)     // Catch:{ p -> 0x00a7, Exception -> 0x00c2 }
        L_0x002f:
            int r4 = com.tencent.map.b.k.a()     // Catch:{ p -> 0x00a7, Exception -> 0x00c2 }
            r3.setConnectTimeout(r4)     // Catch:{ p -> 0x00a7, Exception -> 0x00c2 }
            int r4 = com.tencent.map.b.k.b()     // Catch:{ p -> 0x00a7, Exception -> 0x00c2 }
            r3.setReadTimeout(r4)     // Catch:{ p -> 0x00a7, Exception -> 0x00c2 }
            java.lang.String r4 = "User-Agent"
            r3.setRequestProperty(r4, r8)     // Catch:{ p -> 0x00a7, Exception -> 0x00c2 }
            r4 = 1
            r3.setDoInput(r4)     // Catch:{ p -> 0x00a7, Exception -> 0x00c2 }
            if (r6 == 0) goto L_0x0049
            r2 = r0
        L_0x0049:
            r3.setDoOutput(r2)     // Catch:{ p -> 0x00a7, Exception -> 0x00c2 }
            r2 = 0
            r3.setUseCaches(r2)     // Catch:{ p -> 0x00a7, Exception -> 0x00c2 }
            if (r11 == 0) goto L_0x0059
            java.lang.String r2 = "Connection"
            java.lang.String r4 = "Keep-Alive"
            r3.setRequestProperty(r2, r4)     // Catch:{ p -> 0x00a7, Exception -> 0x00c2 }
        L_0x0059:
            com.tencent.map.b.k.a(r3)     // Catch:{ p -> 0x00a7, Exception -> 0x00c2 }
            r3.connect()     // Catch:{ p -> 0x00a7, Exception -> 0x00c2 }
            com.tencent.map.b.k.c()     // Catch:{ p -> 0x00a7, Exception -> 0x00c2 }
            if (r6 != 0) goto L_0x007b
            if (r10 == 0) goto L_0x007b
            int r2 = r10.length     // Catch:{ p -> 0x00a7, Exception -> 0x00c2 }
            if (r2 == 0) goto L_0x007b
            java.io.DataOutputStream r2 = new java.io.DataOutputStream     // Catch:{ p -> 0x00a7, Exception -> 0x00c2 }
            java.io.OutputStream r4 = r3.getOutputStream()     // Catch:{ p -> 0x00a7, Exception -> 0x00c2 }
            r2.<init>(r4)     // Catch:{ p -> 0x00a7, Exception -> 0x00c2 }
            r2.write(r10)     // Catch:{ p -> 0x011e, Exception -> 0x0118, all -> 0x0112 }
            r2.flush()     // Catch:{ p -> 0x011e, Exception -> 0x0118, all -> 0x0112 }
            r2.close()     // Catch:{ p -> 0x011e, Exception -> 0x0118, all -> 0x0112 }
        L_0x007b:
            int r2 = r3.getResponseCode()     // Catch:{ p -> 0x00a7, Exception -> 0x00c2 }
            r4 = 200(0xc8, float:2.8E-43)
            if (r2 == r4) goto L_0x0087
            r4 = 206(0xce, float:2.89E-43)
            if (r2 != r4) goto L_0x00ca
        L_0x0087:
            com.tencent.map.b.k.d()     // Catch:{ p -> 0x00a7, Exception -> 0x00c2 }
            com.tencent.map.b.n r2 = a(r3, r6)     // Catch:{ p -> 0x00a7, Exception -> 0x00c2 }
            if (r2 == 0) goto L_0x0097
            byte[] r4 = r2.f2449a     // Catch:{ p -> 0x00a7, Exception -> 0x00c2 }
            if (r4 == 0) goto L_0x0097
            byte[] r0 = r2.f2449a     // Catch:{ p -> 0x00a7, Exception -> 0x00c2 }
            int r0 = r0.length     // Catch:{ p -> 0x00a7, Exception -> 0x00c2 }
        L_0x0097:
            com.tencent.map.b.k.a(r0)     // Catch:{ p -> 0x00a7, Exception -> 0x00c2 }
            if (r3 == 0) goto L_0x009f
            r3.disconnect()
        L_0x009f:
            return r2
        L_0x00a0:
            java.lang.String r4 = "Host"
            r5 = 0
            r3.setRequestProperty(r4, r5)     // Catch:{ p -> 0x00a7, Exception -> 0x00c2 }
            goto L_0x0028
        L_0x00a7:
            r0 = move-exception
            r2 = r3
        L_0x00a9:
            r3 = 1
            com.tencent.map.b.k.a(r3)     // Catch:{ all -> 0x00ae }
            throw r0     // Catch:{ all -> 0x00ae }
        L_0x00ae:
            r0 = move-exception
            r3 = r2
        L_0x00b0:
            if (r1 == 0) goto L_0x00b5
            r1.close()
        L_0x00b5:
            if (r3 == 0) goto L_0x00ba
            r3.disconnect()
        L_0x00ba:
            throw r0
        L_0x00bb:
            java.lang.String r4 = "POST"
            r3.setRequestMethod(r4)     // Catch:{ p -> 0x00a7, Exception -> 0x00c2 }
            goto L_0x002f
        L_0x00c2:
            r0 = move-exception
        L_0x00c3:
            r2 = 0
            com.tencent.map.b.k.a(r2)     // Catch:{ all -> 0x00c8 }
            throw r0     // Catch:{ all -> 0x00c8 }
        L_0x00c8:
            r0 = move-exception
            goto L_0x00b0
        L_0x00ca:
            r0 = 202(0xca, float:2.83E-43)
            if (r2 == r0) goto L_0x00f2
            r0 = 201(0xc9, float:2.82E-43)
            if (r2 == r0) goto L_0x00f2
            r0 = 204(0xcc, float:2.86E-43)
            if (r2 == r0) goto L_0x00f2
            r0 = 205(0xcd, float:2.87E-43)
            if (r2 == r0) goto L_0x00f2
            r0 = 304(0x130, float:4.26E-43)
            if (r2 == r0) goto L_0x00f2
            r0 = 305(0x131, float:4.27E-43)
            if (r2 == r0) goto L_0x00f2
            r0 = 408(0x198, float:5.72E-43)
            if (r2 == r0) goto L_0x00f2
            r0 = 502(0x1f6, float:7.03E-43)
            if (r2 == r0) goto L_0x00f2
            r0 = 504(0x1f8, float:7.06E-43)
            if (r2 == r0) goto L_0x00f2
            r0 = 503(0x1f7, float:7.05E-43)
            if (r2 != r0) goto L_0x00fa
        L_0x00f2:
            java.io.IOException r0 = new java.io.IOException     // Catch:{ p -> 0x00a7, Exception -> 0x00c2 }
            java.lang.String r2 = "doGetOrPost retry"
            r0.<init>(r2)     // Catch:{ p -> 0x00a7, Exception -> 0x00c2 }
            throw r0     // Catch:{ p -> 0x00a7, Exception -> 0x00c2 }
        L_0x00fa:
            com.tencent.map.b.p r0 = new com.tencent.map.b.p     // Catch:{ p -> 0x00a7, Exception -> 0x00c2 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ p -> 0x00a7, Exception -> 0x00c2 }
            java.lang.String r5 = "response code is "
            r4.<init>(r5)     // Catch:{ p -> 0x00a7, Exception -> 0x00c2 }
            java.lang.StringBuilder r2 = r4.append(r2)     // Catch:{ p -> 0x00a7, Exception -> 0x00c2 }
            java.lang.String r2 = r2.toString()     // Catch:{ p -> 0x00a7, Exception -> 0x00c2 }
            r0.<init>(r2)     // Catch:{ p -> 0x00a7, Exception -> 0x00c2 }
            throw r0     // Catch:{ p -> 0x00a7, Exception -> 0x00c2 }
        L_0x010f:
            r0 = move-exception
            r3 = r1
            goto L_0x00b0
        L_0x0112:
            r0 = move-exception
            r1 = r2
            goto L_0x00b0
        L_0x0115:
            r0 = move-exception
            r3 = r1
            goto L_0x00c3
        L_0x0118:
            r0 = move-exception
            r1 = r2
            goto L_0x00c3
        L_0x011b:
            r0 = move-exception
            r2 = r1
            goto L_0x00a9
        L_0x011e:
            r0 = move-exception
            r1 = r2
            r2 = r3
            goto L_0x00a9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.map.b.q.a(boolean, java.lang.String, java.lang.String, java.lang.String, byte[], boolean, boolean):com.tencent.map.b.n");
    }

    private static n a(HttpURLConnection httpURLConnection, boolean z) throws IOException {
        int read;
        boolean z2 = true;
        int i = 0;
        InputStream inputStream = null;
        try {
            n nVar = new n();
            String contentType = httpURLConnection.getContentType();
            String str = "GBK";
            if (contentType != null) {
                String[] split = contentType.split(";");
                int length = split.length;
                int i2 = 0;
                while (true) {
                    if (i2 >= length) {
                        break;
                    }
                    String str2 = split[i2];
                    if (str2.contains("charset")) {
                        String[] split2 = str2.split("=");
                        if (split2.length > 1) {
                            str = split2[1].trim();
                        }
                    } else {
                        i2++;
                    }
                }
            }
            nVar.b = str;
            if (z) {
                if (contentType == null || !contentType.contains("vnd.wap.wml")) {
                    z2 = false;
                }
                if (z2) {
                    httpURLConnection.disconnect();
                    httpURLConnection.connect();
                }
            }
            inputStream = httpURLConnection.getInputStream();
            if (inputStream != null) {
                nVar.f2449a = new byte[0];
                byte[] bArr = new byte[1024];
                do {
                    read = inputStream.read(bArr);
                    if (read > 0) {
                        i += read;
                        byte[] bArr2 = new byte[i];
                        System.arraycopy(nVar.f2449a, 0, bArr2, 0, nVar.f2449a.length);
                        System.arraycopy(bArr, 0, bArr2, nVar.f2449a.length, read);
                        nVar.f2449a = bArr2;
                        continue;
                    }
                } while (read > 0);
            }
            return nVar;
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                }
            }
        }
    }

    private static void a(int i) {
        if (f2450a != i) {
            f2450a = i;
        }
    }

    private static boolean a(HttpURLConnection httpURLConnection) throws IOException {
        InputStream inputStream = null;
        try {
            inputStream = httpURLConnection.getInputStream();
            if (!httpURLConnection.getContentType().equals("text/html")) {
                return false;
            }
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            while (inputStream.available() > 0) {
                byteArrayOutputStream.write(inputStream.read());
            }
            boolean equals = new String(byteArrayOutputStream.toByteArray()).trim().equals("1");
            if (inputStream == null) {
                return equals;
            }
            inputStream.close();
            return equals;
        } finally {
            if (inputStream != null) {
                inputStream.close();
            }
        }
    }

    private static HttpURLConnection a(URL url, String str) throws IOException {
        String replaceFirst;
        int i = 80;
        String defaultHost = Proxy.getDefaultHost();
        int defaultPort = Proxy.getDefaultPort();
        if (defaultPort == -1) {
            defaultPort = 80;
        }
        String host = url.getHost();
        int port = url.getPort();
        if (port != -1) {
            i = port;
        }
        if (str.indexOf(String.valueOf(host) + ":" + i) != -1) {
            replaceFirst = str.replaceFirst(String.valueOf(host) + ":" + i, String.valueOf(defaultHost) + ":" + defaultPort);
        } else {
            replaceFirst = str.replaceFirst(host, String.valueOf(defaultHost) + ":" + defaultPort);
        }
        try {
            HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(replaceFirst).openConnection();
            httpURLConnection.setRequestProperty("X-Online-Host", String.valueOf(host) + ":" + i);
            return httpURLConnection;
        } catch (MalformedURLException e) {
            return null;
        }
    }
}
