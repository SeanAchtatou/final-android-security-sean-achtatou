package com.tencent.open;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.NinePatch;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.NinePatchDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Interpolator;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import cn.banshenggua.aichang.room.message.SocketMessage;
import com.tencent.connect.auth.QQAuth;
import com.tencent.connect.auth.QQToken;
import com.tencent.connect.common.BaseApi;
import com.tencent.open.utils.HttpUtils;
import com.tencent.tauth.IRequestListener;
import com.tencent.tauth.IUiListener;
import com.tencent.tauth.UiError;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import org.apache.http.conn.ConnectTimeoutException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: ProGuard */
public class TaskGuide extends BaseApi {
    private static int L = 3000;
    static long b = 5000;
    private static Drawable k;
    private static Drawable l;
    private static Drawable m;
    private static int n = 75;
    private static int o = 284;
    /* access modifiers changed from: private */
    public static int p = 75;
    /* access modifiers changed from: private */
    public static int q = 30;
    private static int r = 29;
    private static int s = 5;
    private static int t = 74;
    private static int u = 0;
    private static int v = 6;
    private static int w = 153;
    private static int x = 30;
    private static int y = 6;
    private static int z = 3;
    private int A = 0;
    private int B = 0;
    private float C = 0.0f;
    /* access modifiers changed from: private */
    public Interpolator D = new AccelerateInterpolator();
    /* access modifiers changed from: private */
    public boolean E = false;
    /* access modifiers changed from: private */
    public Context F;
    private boolean G = false;
    private boolean H = false;
    private long I;
    /* access modifiers changed from: private */
    public int J;
    /* access modifiers changed from: private */
    public int K;
    /* access modifiers changed from: private */
    public Runnable M = null;
    private Runnable N = null;

    /* renamed from: a  reason: collision with root package name */
    boolean f2467a = false;
    IUiListener c;
    /* access modifiers changed from: private */
    public WindowManager.LayoutParams d = null;
    /* access modifiers changed from: private */
    public ViewGroup e = null;
    /* access modifiers changed from: private */
    public WindowManager f;
    /* access modifiers changed from: private */
    public Handler g = new Handler(Looper.getMainLooper());
    /* access modifiers changed from: private */
    public h h;
    /* access modifiers changed from: private */
    public k i = k.INIT;
    /* access modifiers changed from: private */
    public k j = k.INIT;

    public TaskGuide(Context context, QQToken qQToken) {
        super(qQToken);
        this.F = context;
        this.f = (WindowManager) context.getSystemService("window");
        c();
    }

    public TaskGuide(Context context, QQAuth qQAuth, QQToken qQToken) {
        super(qQAuth, qQToken);
        this.F = context;
        this.f = (WindowManager) context.getSystemService("window");
        c();
    }

    private void c() {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        this.f.getDefaultDisplay().getMetrics(displayMetrics);
        this.A = displayMetrics.widthPixels;
        this.B = displayMetrics.heightPixels;
        this.C = displayMetrics.density;
    }

    /* access modifiers changed from: private */
    public WindowManager.LayoutParams a(Context context) {
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        layoutParams.gravity = 49;
        this.f.getDefaultDisplay().getWidth();
        this.f.getDefaultDisplay().getHeight();
        layoutParams.width = a(o);
        layoutParams.height = a(n);
        layoutParams.windowAnimations = 16973826;
        layoutParams.format = 1;
        layoutParams.flags |= 520;
        layoutParams.type = 2;
        this.d = layoutParams;
        return layoutParams;
    }

    /* access modifiers changed from: private */
    public void d() {
        if (this.d != null) {
            this.d.y = -this.d.height;
        }
    }

    /* access modifiers changed from: private */
    public int a(int i2) {
        return (int) (((float) i2) * this.C);
    }

    /* access modifiers changed from: private */
    public ViewGroup b(Context context) {
        e eVar = new e(context);
        g[] gVarArr = this.h.c;
        if (gVarArr.length == 1) {
            i iVar = new i(context, gVarArr[0]);
            iVar.setId(1);
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -2);
            layoutParams.addRule(15);
            eVar.addView(iVar, layoutParams);
        } else {
            i iVar2 = new i(context, gVarArr[0]);
            iVar2.setId(1);
            i iVar3 = new i(context, gVarArr[1]);
            iVar3.setId(2);
            RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-1, -2);
            layoutParams2.addRule(14);
            layoutParams2.setMargins(0, a(6), 0, 0);
            RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(-1, -2);
            layoutParams3.addRule(14);
            layoutParams3.setMargins(0, a(4), 0, 0);
            layoutParams3.addRule(3, 1);
            layoutParams3.addRule(5, 1);
            eVar.addView(iVar2, layoutParams2);
            eVar.addView(iVar3, layoutParams3);
        }
        eVar.setBackgroundDrawable(e());
        return eVar;
    }

    private Drawable e() {
        if (k == null) {
            k = a("background.9.png", this.F);
        }
        return k;
    }

    /* access modifiers changed from: private */
    public Drawable f() {
        if (l == null) {
            l = a("button_green.9.png", this.F);
        }
        return l;
    }

    /* access modifiers changed from: private */
    public Drawable g() {
        if (m == null) {
            m = a("button_red.9.png", this.F);
        }
        return m;
    }

    /* access modifiers changed from: private */
    public void b(final int i2) {
        if (this.g != null) {
            this.g.post(new Runnable() {
                public void run() {
                    if (!TaskGuide.this.E) {
                        return;
                    }
                    if (i2 == 0) {
                        ((i) TaskGuide.this.e.findViewById(1)).a(TaskGuide.this.i);
                    } else if (i2 == 1) {
                        ((i) TaskGuide.this.e.findViewById(2)).a(TaskGuide.this.j);
                    } else if (i2 == 2) {
                        ((i) TaskGuide.this.e.findViewById(1)).a(TaskGuide.this.i);
                        if (TaskGuide.this.e.getChildCount() > 1) {
                            ((i) TaskGuide.this.e.findViewById(2)).a(TaskGuide.this.j);
                        }
                    }
                }
            });
        }
    }

    /* compiled from: ProGuard */
    private enum k {
        INIT,
        WAITTING_BACK_TASKINFO,
        WAITTING_BACK_REWARD,
        NORAML,
        REWARD_SUCCESS,
        REWARD_FAIL;

        public static k[] a() {
            return (k[]) g.clone();
        }
    }

    /* access modifiers changed from: private */
    public void a(int i2, k kVar) {
        if (i2 == 0) {
            this.i = kVar;
        } else if (i2 == 1) {
            this.j = kVar;
        } else {
            this.i = kVar;
            this.j = kVar;
        }
    }

    /* compiled from: ProGuard */
    class f implements View.OnClickListener {

        /* renamed from: a  reason: collision with root package name */
        int f2477a;

        public f(int i) {
            this.f2477a = i;
        }

        public void onClick(View view) {
            Button button = (Button) view;
            if (TaskGuide.this.c(this.f2477a) == k.NORAML) {
                TaskGuide.this.e(this.f2477a);
                TaskGuide.this.b(this.f2477a);
            }
            TaskGuide.this.h();
        }
    }

    /* access modifiers changed from: private */
    public k c(int i2) {
        if (i2 == 0) {
            return this.i;
        }
        if (i2 == 1) {
            return this.j;
        }
        return k.INIT;
    }

    @SuppressLint({"ResourceAsColor"})
    public void showWindow() {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.tencent.open.TaskGuide.a(com.tencent.open.TaskGuide, boolean):boolean
             arg types: [com.tencent.open.TaskGuide, int]
             candidates:
              com.tencent.open.TaskGuide.a(java.lang.String, android.content.Context):android.graphics.drawable.Drawable
              com.tencent.open.TaskGuide.a(com.tencent.open.TaskGuide, android.content.Context):android.view.ViewGroup
              com.tencent.open.TaskGuide.a(com.tencent.open.TaskGuide, android.view.ViewGroup):android.view.ViewGroup
              com.tencent.open.TaskGuide.a(com.tencent.open.TaskGuide, android.view.WindowManager$LayoutParams):android.view.WindowManager$LayoutParams
              com.tencent.open.TaskGuide.a(com.tencent.open.TaskGuide, com.tencent.open.TaskGuide$h):com.tencent.open.TaskGuide$h
              com.tencent.open.TaskGuide.a(com.tencent.open.TaskGuide, int):com.tencent.open.TaskGuide$k
              com.tencent.open.TaskGuide.a(int, com.tencent.open.TaskGuide$k):void
              com.tencent.open.TaskGuide.a(com.tencent.open.TaskGuide, java.lang.String):void
              com.tencent.open.TaskGuide.a(com.tencent.open.TaskGuide, boolean):boolean */
            public void run() {
                ViewGroup unused = TaskGuide.this.e = TaskGuide.this.b(TaskGuide.this.F);
                WindowManager.LayoutParams unused2 = TaskGuide.this.d = TaskGuide.this.a(TaskGuide.this.F);
                TaskGuide.this.d();
                WindowManager windowManager = (WindowManager) TaskGuide.this.F.getSystemService("window");
                if (!((Activity) TaskGuide.this.F).isFinishing()) {
                    if (!TaskGuide.this.E) {
                        windowManager.addView(TaskGuide.this.e, TaskGuide.this.d);
                    }
                    boolean unused3 = TaskGuide.this.E = true;
                    TaskGuide.this.b(2);
                    TaskGuide.this.k();
                }
            }
        });
        com.tencent.connect.a.a.a(this.F, this.mToken, "TaskApi", "showTaskWindow");
    }

    /* compiled from: ProGuard */
    private class i extends LinearLayout {
        private TextView b;
        private Button c;
        private g d;

        public i(Context context, g gVar) {
            super(context);
            this.d = gVar;
            setOrientation(0);
            a();
        }

        private void a() {
            this.b = new TextView(TaskGuide.this.F);
            this.b.setTextColor(Color.rgb(255, 255, 255));
            this.b.setTextSize(15.0f);
            this.b.setShadowLayer(1.0f, 1.0f, 1.0f, Color.rgb(242, 211, 199));
            this.b.setGravity(3);
            this.b.setEllipsize(TextUtils.TruncateAt.END);
            this.b.setIncludeFontPadding(false);
            this.b.setSingleLine(true);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(0, -2);
            layoutParams.weight = 1.0f;
            layoutParams.leftMargin = TaskGuide.this.a(4);
            addView(this.b, layoutParams);
            this.c = new Button(TaskGuide.this.F);
            this.c.setPadding(0, 0, 0, 0);
            this.c.setTextSize(16.0f);
            this.c.setTextColor(Color.rgb(255, 255, 255));
            this.c.setShadowLayer(1.0f, 1.0f, 1.0f, Color.rgb(242, 211, 199));
            this.c.setIncludeFontPadding(false);
            this.c.setOnClickListener(new f(this.d.f2478a));
            LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(TaskGuide.this.a(TaskGuide.p), TaskGuide.this.a(TaskGuide.q));
            layoutParams2.leftMargin = TaskGuide.this.a(2);
            layoutParams2.rightMargin = TaskGuide.this.a(8);
            addView(this.c, layoutParams2);
        }

        public void a(k kVar) {
            if (!TextUtils.isEmpty(this.d.b)) {
                this.b.setText(this.d.b);
            }
            switch (kVar) {
                case INIT:
                    this.c.setEnabled(false);
                    return;
                case NORAML:
                    if (this.d.e == 1) {
                        this.c.setText(this.d.c);
                        this.c.setBackgroundDrawable(null);
                        this.c.setTextColor(Color.rgb(255, 246, 0));
                        this.c.setEnabled(false);
                        return;
                    } else if (this.d.e == 2) {
                        this.c.setText("领取奖励");
                        this.c.setTextColor(Color.rgb(255, 255, 255));
                        this.c.setBackgroundDrawable(TaskGuide.this.f());
                        this.c.setEnabled(true);
                        return;
                    } else {
                        return;
                    }
                case WAITTING_BACK_REWARD:
                    this.c.setText("领取中...");
                    this.c.setEnabled(false);
                    return;
                case REWARD_SUCCESS:
                    this.c.setText("已领取");
                    this.c.setBackgroundDrawable(TaskGuide.this.g());
                    this.c.setEnabled(false);
                    return;
                default:
                    return;
            }
        }
    }

    /* compiled from: ProGuard */
    private class e extends RelativeLayout {

        /* renamed from: a  reason: collision with root package name */
        int f2476a = 0;

        public e(Context context) {
            super(context);
        }

        public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
            int y = (int) motionEvent.getY();
            com.tencent.open.a.f.a("openSDK_LOG.TaskGuide", "onInterceptTouchEvent-- action = " + motionEvent.getAction() + "currentY = " + y);
            TaskGuide.this.d(3000);
            switch (motionEvent.getAction()) {
                case 0:
                    this.f2476a = y;
                    return false;
                case 1:
                    if (this.f2476a - y > ViewConfiguration.getTouchSlop() * 2) {
                        TaskGuide.this.l();
                        return true;
                    }
                    break;
            }
            return super.onInterceptTouchEvent(motionEvent);
        }

        public boolean onTouchEvent(MotionEvent motionEvent) {
            super.onTouchEvent(motionEvent);
            int y = (int) motionEvent.getY();
            com.tencent.open.a.f.b("openSDK_LOG.TaskGuide", " onTouchEvent-----startY = " + this.f2476a + "currentY = " + y);
            switch (motionEvent.getAction()) {
                case 0:
                    this.f2476a = y;
                    return false;
                case 1:
                    if (this.f2476a - y <= ViewConfiguration.getTouchSlop() * 2) {
                        return false;
                    }
                    TaskGuide.this.l();
                    return false;
                case 2:
                default:
                    return false;
            }
        }
    }

    /* compiled from: ProGuard */
    class c implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        boolean f2474a = false;
        float b = 0.0f;

        public c(boolean z) {
            this.f2474a = z;
        }

        public void run() {
            boolean z = true;
            SystemClock.currentThreadTimeMillis();
            this.b = (float) (((double) this.b) + 0.1d);
            float f = this.b;
            if (f > 1.0f) {
                f = 1.0f;
            }
            boolean z2 = f >= 1.0f;
            int interpolation = (int) (TaskGuide.this.D.getInterpolation(f) * ((float) TaskGuide.this.J));
            if (this.f2474a) {
                TaskGuide.this.d.y = TaskGuide.this.K + interpolation;
            } else {
                TaskGuide.this.d.y = TaskGuide.this.K - interpolation;
            }
            com.tencent.open.a.f.b("openSDK_LOG.TaskGuide", "mWinParams.y = " + TaskGuide.this.d.y + "deltaDistence = " + interpolation);
            if (TaskGuide.this.E) {
                TaskGuide.this.f.updateViewLayout(TaskGuide.this.e, TaskGuide.this.d);
                z = z2;
            }
            if (z) {
                TaskGuide.this.i();
            } else {
                TaskGuide.this.g.postDelayed(TaskGuide.this.M, 5);
            }
        }
    }

    /* compiled from: ProGuard */
    private class b implements Runnable {
        private b() {
        }

        public void run() {
            TaskGuide.this.l();
        }
    }

    /* access modifiers changed from: private */
    public void d(int i2) {
        h();
        this.N = new b();
        this.g.postDelayed(this.N, (long) i2);
    }

    /* access modifiers changed from: private */
    public void h() {
        this.g.removeCallbacks(this.N);
        if (!j()) {
            this.g.removeCallbacks(this.M);
        }
    }

    /* access modifiers changed from: private */
    public void i() {
        if (this.G) {
            d(3000);
        } else {
            removeWindow();
        }
        if (this.G) {
            this.d.flags &= -17;
            this.f.updateViewLayout(this.e, this.d);
        }
        this.G = false;
        this.H = false;
    }

    private void a(boolean z2) {
        this.I = SystemClock.currentThreadTimeMillis();
        if (z2) {
            this.G = true;
        } else {
            this.H = true;
        }
        this.J = this.d.height;
        this.K = this.d.y;
        this.d.flags |= 16;
        this.f.updateViewLayout(this.e, this.d);
    }

    private boolean j() {
        return this.G || this.H;
    }

    /* access modifiers changed from: private */
    public void k() {
        if (!j()) {
            this.g.removeCallbacks(this.N);
            this.g.removeCallbacks(this.M);
            this.M = new c(true);
            a(true);
            this.g.post(this.M);
        }
    }

    /* access modifiers changed from: private */
    public void l() {
        if (!j()) {
            this.g.removeCallbacks(this.N);
            this.g.removeCallbacks(this.M);
            this.M = new c(false);
            a(false);
            this.g.post(this.M);
        }
    }

    public void removeWindow() {
        if (this.E) {
            this.f.removeView(this.e);
            this.E = false;
        }
    }

    private Drawable a(String str, Context context) {
        IOException e2;
        Drawable drawable;
        Bitmap bitmap;
        try {
            InputStream open = context.getApplicationContext().getAssets().open(str);
            if (open == null) {
                return null;
            }
            if (str.endsWith(".9.png")) {
                try {
                    bitmap = BitmapFactory.decodeStream(open);
                } catch (OutOfMemoryError e3) {
                    e3.printStackTrace();
                    bitmap = null;
                }
                if (bitmap == null) {
                    return null;
                }
                byte[] ninePatchChunk = bitmap.getNinePatchChunk();
                NinePatch.isNinePatchChunk(ninePatchChunk);
                return new NinePatchDrawable(bitmap, ninePatchChunk, new Rect(), null);
            }
            drawable = Drawable.createFromStream(open, str);
            try {
                open.close();
                return drawable;
            } catch (IOException e4) {
                e2 = e4;
            }
        } catch (IOException e5) {
            IOException iOException = e5;
            drawable = null;
            e2 = iOException;
        }
        e2.printStackTrace();
        return drawable;
    }

    /* compiled from: ProGuard */
    private static class h {

        /* renamed from: a  reason: collision with root package name */
        String f2479a;
        String b;
        g[] c;

        private h() {
        }

        public boolean a() {
            if (TextUtils.isEmpty(this.f2479a) || this.c == null || this.c.length <= 0) {
                return false;
            }
            return true;
        }

        static h a(JSONObject jSONObject) throws JSONException {
            if (jSONObject == null) {
                return null;
            }
            h hVar = new h();
            JSONObject jSONObject2 = jSONObject.getJSONObject("task_info");
            hVar.f2479a = jSONObject2.getString("task_id");
            hVar.b = jSONObject2.getString("task_desc");
            JSONArray jSONArray = jSONObject2.getJSONArray("step_info");
            int length = jSONArray.length();
            if (length > 0) {
                hVar.c = new g[length];
            }
            for (int i = 0; i < length; i++) {
                JSONObject jSONObject3 = jSONArray.getJSONObject(i);
                hVar.c[i] = new g(jSONObject3.getInt("step_no"), jSONObject3.getString("step_desc"), jSONObject3.getString("step_gift"), jSONObject3.getLong("end_time"), jSONObject3.getInt("status"));
            }
            return hVar;
        }
    }

    /* compiled from: ProGuard */
    private static class g {

        /* renamed from: a  reason: collision with root package name */
        int f2478a;
        String b;
        String c;
        long d;
        int e;

        public g(int i, String str, String str2, long j, int i2) {
            this.f2478a = i;
            this.b = str;
            this.c = str2;
            this.d = j;
            this.e = i2;
        }
    }

    /* compiled from: ProGuard */
    private class j extends a {
        private j() {
            super();
        }

        public void onComplete(JSONObject jSONObject) {
            try {
                h unused = TaskGuide.this.h = h.a(jSONObject);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            if (TaskGuide.this.h == null || !TaskGuide.this.h.a()) {
                a(null);
                return;
            }
            TaskGuide.this.showWindow();
            TaskGuide.this.a(2, k.NORAML);
            JSONObject jSONObject2 = new JSONObject();
            try {
                jSONObject2.put(SocketMessage.MSG_RESULE_KEY, "获取成功");
            } catch (JSONException e2) {
                e2.printStackTrace();
            }
            TaskGuide.this.c.onComplete(jSONObject2);
        }

        /* access modifiers changed from: protected */
        public void a(Exception exc) {
            if (exc != null) {
                exc.printStackTrace();
            }
            if (exc == null) {
                JSONObject jSONObject = new JSONObject();
                try {
                    jSONObject.put(SocketMessage.MSG_RESULE_KEY, "暂无任务");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                TaskGuide.this.c.onComplete(jSONObject);
            } else {
                TaskGuide.this.c.onError(new UiError(100, "error ", "获取任务失败"));
            }
            TaskGuide.this.g.post(new Runnable() {
                public void run() {
                    TaskGuide.this.a(2, k.INIT);
                }
            });
        }
    }

    /* compiled from: ProGuard */
    private abstract class a implements IRequestListener {
        /* access modifiers changed from: protected */
        public abstract void a(Exception exc);

        private a() {
        }

        public void onIOException(IOException iOException) {
            a(iOException);
        }

        public void onMalformedURLException(MalformedURLException malformedURLException) {
            a(malformedURLException);
        }

        public void onJSONException(JSONException jSONException) {
            a(jSONException);
        }

        public void onConnectTimeoutException(ConnectTimeoutException connectTimeoutException) {
            a(connectTimeoutException);
        }

        public void onSocketTimeoutException(SocketTimeoutException socketTimeoutException) {
            a(socketTimeoutException);
        }

        public void onNetworkUnavailableException(HttpUtils.NetworkUnavailableException networkUnavailableException) {
            a(networkUnavailableException);
        }

        public void onHttpStatusException(HttpUtils.HttpStatusException httpStatusException) {
            a(httpStatusException);
        }

        public void onUnknowException(Exception exc) {
            a(exc);
        }
    }

    /* access modifiers changed from: private */
    public void a(final String str) {
        this.g.post(new Runnable() {
            public void run() {
                Toast.makeText(TaskGuide.this.F, "失败：" + str, 1).show();
            }
        });
    }

    /* compiled from: ProGuard */
    private class d extends a {
        int b = -1;

        public d(int i) {
            super();
            this.b = i;
        }

        /* JADX WARNING: Unknown top exception splitter block from list: {B:8:0x0024=Splitter:B:8:0x0024, B:22:0x006c=Splitter:B:22:0x006c} */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void onComplete(org.json.JSONObject r6) {
            /*
                r5 = this;
                r1 = 0
                java.lang.String r0 = "code"
                int r0 = r6.getInt(r0)     // Catch:{ JSONException -> 0x003f }
                java.lang.String r2 = "message"
                java.lang.String r1 = r6.getString(r2)     // Catch:{ JSONException -> 0x003f }
                if (r0 != 0) goto L_0x0052
                com.tencent.open.TaskGuide r0 = com.tencent.open.TaskGuide.this     // Catch:{ JSONException -> 0x003f }
                int r2 = r5.b     // Catch:{ JSONException -> 0x003f }
                com.tencent.open.TaskGuide$k r3 = com.tencent.open.TaskGuide.k.REWARD_SUCCESS     // Catch:{ JSONException -> 0x003f }
                r0.a(r2, r3)     // Catch:{ JSONException -> 0x003f }
                org.json.JSONObject r2 = new org.json.JSONObject     // Catch:{ JSONException -> 0x003f }
                r2.<init>()     // Catch:{ JSONException -> 0x003f }
                java.lang.String r0 = "result"
                java.lang.String r3 = "金券领取成功"
                r2.put(r0, r3)     // Catch:{ JSONException -> 0x003a }
            L_0x0024:
                com.tencent.open.TaskGuide r0 = com.tencent.open.TaskGuide.this     // Catch:{ JSONException -> 0x003f }
                com.tencent.tauth.IUiListener r0 = r0.c     // Catch:{ JSONException -> 0x003f }
                r0.onComplete(r2)     // Catch:{ JSONException -> 0x003f }
            L_0x002b:
                com.tencent.open.TaskGuide r0 = com.tencent.open.TaskGuide.this
                int r1 = r5.b
                r0.b(r1)
                com.tencent.open.TaskGuide r0 = com.tencent.open.TaskGuide.this
                r1 = 2000(0x7d0, float:2.803E-42)
                r0.d(r1)
                return
            L_0x003a:
                r0 = move-exception
                r0.printStackTrace()     // Catch:{ JSONException -> 0x003f }
                goto L_0x0024
            L_0x003f:
                r0 = move-exception
                com.tencent.open.TaskGuide r2 = com.tencent.open.TaskGuide.this
                int r3 = r5.b
                com.tencent.open.TaskGuide$k r4 = com.tencent.open.TaskGuide.k.NORAML
                r2.a(r3, r4)
                com.tencent.open.TaskGuide r2 = com.tencent.open.TaskGuide.this
                r2.a(r1)
                r0.printStackTrace()
                goto L_0x002b
            L_0x0052:
                com.tencent.open.TaskGuide r0 = com.tencent.open.TaskGuide.this     // Catch:{ JSONException -> 0x003f }
                int r2 = r5.b     // Catch:{ JSONException -> 0x003f }
                com.tencent.open.TaskGuide$k r3 = com.tencent.open.TaskGuide.k.NORAML     // Catch:{ JSONException -> 0x003f }
                r0.a(r2, r3)     // Catch:{ JSONException -> 0x003f }
                com.tencent.open.TaskGuide r0 = com.tencent.open.TaskGuide.this     // Catch:{ JSONException -> 0x003f }
                r0.a(r1)     // Catch:{ JSONException -> 0x003f }
                org.json.JSONObject r2 = new org.json.JSONObject     // Catch:{ JSONException -> 0x003f }
                r2.<init>()     // Catch:{ JSONException -> 0x003f }
                java.lang.String r0 = "result"
                java.lang.String r3 = "金券领取失败"
                r2.put(r0, r3)     // Catch:{ JSONException -> 0x0074 }
            L_0x006c:
                com.tencent.open.TaskGuide r0 = com.tencent.open.TaskGuide.this     // Catch:{ JSONException -> 0x003f }
                com.tencent.tauth.IUiListener r0 = r0.c     // Catch:{ JSONException -> 0x003f }
                r0.onComplete(r2)     // Catch:{ JSONException -> 0x003f }
                goto L_0x002b
            L_0x0074:
                r0 = move-exception
                r0.printStackTrace()     // Catch:{ JSONException -> 0x003f }
                goto L_0x006c
            */
            throw new UnsupportedOperationException("Method not decompiled: com.tencent.open.TaskGuide.d.onComplete(org.json.JSONObject):void");
        }

        /* access modifiers changed from: protected */
        public void a(final Exception exc) {
            if (exc != null) {
                exc.printStackTrace();
            }
            TaskGuide.this.c.onError(new UiError(101, "error ", "金券领取时出现异常"));
            if (TaskGuide.this.g != null) {
                TaskGuide.this.g.post(new Runnable() {
                    public void run() {
                        k d;
                        k kVar = k.INIT;
                        if (d.this.b == 0) {
                            d = TaskGuide.this.i;
                        } else {
                            d = TaskGuide.this.j;
                        }
                        if (d == k.WAITTING_BACK_REWARD) {
                            TaskGuide.this.a(d.this.b, k.NORAML);
                            TaskGuide.this.a("领取失败 :" + exc.getClass().getName());
                        }
                        TaskGuide.this.b(d.this.b);
                        TaskGuide.this.d(2000);
                    }
                });
            }
        }
    }

    public void showTaskGuideWindow(Activity activity, Bundle bundle, IUiListener iUiListener) {
        Bundle composeCGIParams;
        this.F = activity;
        this.c = iUiListener;
        if (this.i == k.WAITTING_BACK_TASKINFO || this.j == k.WAITTING_BACK_TASKINFO || this.E) {
            com.tencent.open.a.f.c("openSDK_LOG.TaskGuide", "showTaskGuideWindow, mState1 ==" + this.i + ", mState2" + this.j);
            return;
        }
        this.h = null;
        if (bundle != null) {
            composeCGIParams = new Bundle(bundle);
            composeCGIParams.putAll(composeCGIParams());
        } else {
            composeCGIParams = composeCGIParams();
        }
        j jVar = new j();
        composeCGIParams.putString("action", "task_list");
        composeCGIParams.putString("auth", "mobile");
        composeCGIParams.putString("appid", this.mToken.getAppId());
        HttpUtils.requestAsync(this.mToken, this.F, "http://appact.qzone.qq.com/appstore_activity_task_pcpush_sdk", composeCGIParams, "GET", jVar);
        a(2, k.WAITTING_BACK_TASKINFO);
    }

    /* access modifiers changed from: private */
    public void e(int i2) {
        Bundle composeCGIParams = composeCGIParams();
        composeCGIParams.putString("action", "get_gift");
        composeCGIParams.putString("task_id", this.h.f2479a);
        composeCGIParams.putString("step_no", new Integer(i2).toString());
        composeCGIParams.putString("appid", this.mToken.getAppId());
        HttpUtils.requestAsync(this.mToken, this.F, "http://appact.qzone.qq.com/appstore_activity_task_pcpush_sdk", composeCGIParams, "GET", new d(i2));
        a(i2, k.WAITTING_BACK_REWARD);
        com.tencent.connect.a.a.a(this.F, this.mToken, "TaskApi", "getGift");
    }
}
