package com.tencent.open.yyb;

import android.os.Parcel;
import android.os.Parcelable;

/* compiled from: ProGuard */
public class ShareModel implements Parcelable {
    public static final Parcelable.Creator<ShareModel> CREATOR = new Parcelable.Creator<ShareModel>() {
        /* renamed from: a */
        public ShareModel createFromParcel(Parcel parcel) {
            ShareModel shareModel = new ShareModel();
            shareModel.f2545a = parcel.readString();
            shareModel.b = parcel.readString();
            shareModel.c = parcel.readString();
            shareModel.d = parcel.readString();
            return shareModel;
        }

        /* renamed from: a */
        public ShareModel[] newArray(int i) {
            return null;
        }
    };

    /* renamed from: a  reason: collision with root package name */
    public String f2545a;
    public String b;
    public String c;
    public String d;

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.f2545a);
        parcel.writeString(this.b);
        parcel.writeString(this.c);
        parcel.writeString(this.d);
    }
}
