package com.tencent.open;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Environment;
import android.text.TextUtils;
import com.tencent.open.a.f;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

/* compiled from: ProGuard */
public class e extends AsyncTask<Bitmap, Void, HashMap<String, Object>> {

    /* renamed from: a  reason: collision with root package name */
    private static final SimpleDateFormat f2511a = new SimpleDateFormat("yyyy-MM-dd-HHmmss", Locale.CHINA);
    private a b;

    /* compiled from: ProGuard */
    public interface a {
        void a(String str);

        void b(String str);
    }

    public e(a aVar) {
        this.b = aVar;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public HashMap<String, Object> doInBackground(Bitmap... bitmapArr) {
        String str;
        HashMap<String, Object> hashMap = new HashMap<>();
        try {
            Bitmap bitmap = bitmapArr[0];
            if (bitmap != null) {
                if (bitmap.getWidth() > 320 || bitmap.getHeight() > 320) {
                    Bitmap a2 = a(bitmap);
                    str = b(a2);
                    a2.recycle();
                } else {
                    str = b(bitmap);
                }
                bitmap.recycle();
                hashMap.put("ResultType", 1);
                hashMap.put("ResultValue", str);
            }
        } catch (Exception e) {
            hashMap.put("ResultType", 0);
            hashMap.put("ResultValue", e.getMessage());
        }
        return hashMap;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void onPostExecute(HashMap<String, Object> hashMap) {
        if (((Integer) hashMap.get("ResultType")).intValue() == 1) {
            this.b.a((String) hashMap.get("ResultValue"));
        } else {
            this.b.b((String) hashMap.get("ResultValue"));
        }
        super.onPostExecute(hashMap);
    }

    private Bitmap a(Bitmap bitmap) {
        int i = 1;
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
        if (byteArrayOutputStream.toByteArray().length / 1024 > 1024) {
            byteArrayOutputStream.reset();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 50, byteArrayOutputStream);
        }
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(byteArrayOutputStream.toByteArray());
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        try {
            BitmapFactory.decodeStream(byteArrayInputStream, null, options);
        } catch (OutOfMemoryError e) {
            f.c("openSDK_LOG.VoiceHelper", "VoiceHelper decodeStream has OutOfMemoryError!");
        }
        options.inJustDecodeBounds = false;
        int a2 = a(options, 320, 320);
        if (a2 > 0) {
            i = a2;
        }
        f.c("openSDK_LOG.VoiceHelper", "comp be=" + i);
        options.inSampleSize = i;
        try {
            return BitmapFactory.decodeStream(new ByteArrayInputStream(byteArrayOutputStream.toByteArray()), null, options);
        } catch (OutOfMemoryError e2) {
            f.c("openSDK_LOG.VoiceHelper", "VoiceHelper decodeStream has OutOfMemoryError!");
            return null;
        }
    }

    private int a(BitmapFactory.Options options, int i, int i2) {
        int i3 = options.outHeight;
        int i4 = options.outWidth;
        if (i3 <= i2 && i4 <= i) {
            return 1;
        }
        int round = Math.round(((float) i3) / ((float) i2));
        int round2 = Math.round(((float) i4) / ((float) i));
        return round < round2 ? round : round2;
    }

    public static void a(String str) {
        if (!TextUtils.isEmpty(str)) {
            try {
                File file = new File(str);
                if (file.exists()) {
                    file.delete();
                }
            } catch (Exception e) {
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:19:0x008e A[SYNTHETIC, Splitter:B:19:0x008e] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.lang.String b(android.graphics.Bitmap r6) {
        /*
            r5 = this;
            r1 = 0
            java.lang.String r0 = ""
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0089 }
            r0.<init>()     // Catch:{ Exception -> 0x0089 }
            long r2 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x0089 }
            java.lang.String r2 = r5.a(r2)     // Catch:{ Exception -> 0x0089 }
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ Exception -> 0x0089 }
            java.lang.String r2 = ".png"
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ Exception -> 0x0089 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x0089 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0089 }
            r2.<init>()     // Catch:{ Exception -> 0x0089 }
            java.lang.String r3 = r5.b()     // Catch:{ Exception -> 0x0089 }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x0089 }
            java.lang.String r3 = java.io.File.separator     // Catch:{ Exception -> 0x0089 }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x0089 }
            java.lang.String r3 = ".AppCenterWebBuffer"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x0089 }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x0089 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0089 }
            r3.<init>()     // Catch:{ Exception -> 0x0089 }
            java.lang.StringBuilder r3 = r3.append(r2)     // Catch:{ Exception -> 0x0089 }
            java.lang.String r4 = java.io.File.separator     // Catch:{ Exception -> 0x0089 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x0089 }
            java.lang.StringBuilder r0 = r3.append(r0)     // Catch:{ Exception -> 0x0089 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x0089 }
            java.io.File r3 = new java.io.File     // Catch:{ Exception -> 0x0089 }
            r3.<init>(r2)     // Catch:{ Exception -> 0x0089 }
            boolean r2 = r3.exists()     // Catch:{ Exception -> 0x0089 }
            if (r2 != 0) goto L_0x0063
            boolean r2 = r3.mkdirs()     // Catch:{ Exception -> 0x0089 }
            if (r2 != 0) goto L_0x0063
        L_0x0063:
            java.io.File r3 = new java.io.File     // Catch:{ Exception -> 0x0089 }
            r3.<init>(r0)     // Catch:{ Exception -> 0x0089 }
            boolean r2 = r3.exists()     // Catch:{ Exception -> 0x0089 }
            if (r2 == 0) goto L_0x0071
            r3.delete()     // Catch:{ Exception -> 0x0089 }
        L_0x0071:
            r3.createNewFile()     // Catch:{ Exception -> 0x0089 }
            java.io.FileOutputStream r2 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x0089 }
            r2.<init>(r3)     // Catch:{ Exception -> 0x0089 }
            android.graphics.Bitmap$CompressFormat r1 = android.graphics.Bitmap.CompressFormat.PNG     // Catch:{ Exception -> 0x00a2, all -> 0x009f }
            r3 = 100
            r6.compress(r1, r3, r2)     // Catch:{ Exception -> 0x00a2, all -> 0x009f }
            r2.flush()     // Catch:{ Exception -> 0x00a2, all -> 0x009f }
            if (r2 == 0) goto L_0x0088
            r2.close()     // Catch:{ IOException -> 0x009b }
        L_0x0088:
            return r0
        L_0x0089:
            r0 = move-exception
        L_0x008a:
            java.lang.String r0 = ""
            if (r1 == 0) goto L_0x0088
            r1.close()     // Catch:{ IOException -> 0x0092 }
            goto L_0x0088
        L_0x0092:
            r1 = move-exception
            goto L_0x0088
        L_0x0094:
            r0 = move-exception
        L_0x0095:
            if (r1 == 0) goto L_0x009a
            r1.close()     // Catch:{ IOException -> 0x009d }
        L_0x009a:
            throw r0
        L_0x009b:
            r1 = move-exception
            goto L_0x0088
        L_0x009d:
            r1 = move-exception
            goto L_0x009a
        L_0x009f:
            r0 = move-exception
            r1 = r2
            goto L_0x0095
        L_0x00a2:
            r0 = move-exception
            r1 = r2
            goto L_0x008a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.open.e.b(android.graphics.Bitmap):java.lang.String");
    }

    private String b() {
        if (Environment.getExternalStorageState().equals("mounted")) {
            return Environment.getExternalStorageDirectory().getAbsolutePath();
        }
        if (new File("/mnt/sdcard-ext").isDirectory()) {
            return "/mnt/sdcard-ext";
        }
        return ".";
    }

    private String a(long j) {
        return f2511a.format(new Date(j));
    }

    public static boolean a() {
        if (!Environment.getExternalStorageState().equals("mounted") && !new File("/mnt/sdcard-ext").isDirectory()) {
            return false;
        }
        return true;
    }
}
