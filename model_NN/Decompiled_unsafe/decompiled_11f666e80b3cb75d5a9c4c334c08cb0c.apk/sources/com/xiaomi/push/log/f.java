package com.xiaomi.push.log;

import android.content.Context;
import android.util.Pair;
import com.xiaomi.channel.commonutils.logger.LoggerInterface;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

public class f implements LoggerInterface {

    /* renamed from: a  reason: collision with root package name */
    private static final SimpleDateFormat f2925a = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss aaa");
    private static com.xiaomi.channel.commonutils.misc.f b = new com.xiaomi.channel.commonutils.misc.f(true);
    private static String c = "/MiPushLog";
    private static List<Pair<String, Throwable>> f = Collections.synchronizedList(new ArrayList());
    private String d;
    private Context e;

    public f(Context context) {
        this.e = context;
        if (context.getApplicationContext() != null) {
            this.e = context.getApplicationContext();
        }
        this.d = this.e.getPackageName();
    }

    public final void log(String str) {
        log(str, null);
    }

    public final void log(String str, Throwable th) {
        f.add(new Pair(String.format("%1$s %2$s %3$s ", f2925a.format(new Date()), this.d, str), th));
        b.a(new g(this));
    }
}
