package com.xiaomi.mipush.sdk;

import android.content.Context;

final class b implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Context f2913a;
    final /* synthetic */ String b;
    final /* synthetic */ String c;

    b(Context context, String str, String str2) {
        this.f2913a = context;
        this.b = str;
        this.c = str2;
    }

    public void run() {
        MiPushClient.initialize(this.f2913a, this.b, this.c, null);
    }
}
