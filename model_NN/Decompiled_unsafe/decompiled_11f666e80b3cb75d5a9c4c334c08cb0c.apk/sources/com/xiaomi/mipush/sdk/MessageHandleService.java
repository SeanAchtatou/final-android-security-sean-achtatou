package com.xiaomi.mipush.sdk;

import android.app.IntentService;
import android.content.Intent;
import android.text.TextUtils;
import com.xiaomi.channel.commonutils.logger.b;
import com.xiaomi.mipush.sdk.PushMessageHandler;
import java.util.concurrent.ConcurrentLinkedQueue;

public class MessageHandleService extends IntentService {

    /* renamed from: a  reason: collision with root package name */
    private static ConcurrentLinkedQueue<a> f2907a = new ConcurrentLinkedQueue<>();

    public class a {

        /* renamed from: a  reason: collision with root package name */
        private PushMessageReceiver f2908a;
        private Intent b;

        public a(Intent intent, PushMessageReceiver pushMessageReceiver) {
            this.f2908a = pushMessageReceiver;
            this.b = intent;
        }

        public PushMessageReceiver a() {
            return this.f2908a;
        }

        public Intent b() {
            return this.b;
        }
    }

    public MessageHandleService() {
        super("MessageHandleThread");
    }

    public static void addJob(a aVar) {
        if (aVar != null && XmSystemUtils.isBrandXiaoMi()) {
            f2907a.add(aVar);
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* access modifiers changed from: protected */
    public void onHandleIntent(Intent intent) {
        a poll;
        if (intent != null && XmSystemUtils.isBrandXiaoMi() && (poll = f2907a.poll()) != null) {
            try {
                PushMessageReceiver a2 = poll.a();
                Intent b = poll.b();
                switch (b.getIntExtra(PushMessageHelper.MESSAGE_TYPE, 1)) {
                    case 1:
                        PushMessageHandler.a a3 = i.a(this).a(b);
                        if (a3 == null) {
                            return;
                        }
                        if (a3 instanceof MiPushMessage) {
                            MiPushMessage miPushMessage = (MiPushMessage) a3;
                            if (!miPushMessage.isArrivedMessage()) {
                                a2.onReceiveMessage(this, miPushMessage);
                            }
                            if (miPushMessage.getPassThrough() == 1) {
                                a2.onReceivePassThroughMessage(this, miPushMessage);
                                return;
                            } else if (miPushMessage.isNotified()) {
                                a2.onNotificationMessageClicked(this, miPushMessage);
                                return;
                            } else {
                                a2.onNotificationMessageArrived(this, miPushMessage);
                                return;
                            }
                        } else if (a3 instanceof MiPushCommandMessage) {
                            MiPushCommandMessage miPushCommandMessage = (MiPushCommandMessage) a3;
                            a2.onCommandResult(this, miPushCommandMessage);
                            if (TextUtils.equals(miPushCommandMessage.getCommand(), MiPushClient.COMMAND_REGISTER)) {
                                a2.onReceiveRegisterResult(this, miPushCommandMessage);
                                return;
                            }
                            return;
                        } else {
                            return;
                        }
                    case 2:
                    default:
                        return;
                    case 3:
                        MiPushCommandMessage miPushCommandMessage2 = (MiPushCommandMessage) b.getSerializableExtra(PushMessageHelper.KEY_COMMAND);
                        a2.onCommandResult(this, miPushCommandMessage2);
                        if (TextUtils.equals(miPushCommandMessage2.getCommand(), MiPushClient.COMMAND_REGISTER)) {
                            a2.onReceiveRegisterResult(this, miPushCommandMessage2);
                            return;
                        }
                        return;
                    case 4:
                        return;
                }
            } catch (RuntimeException e) {
                b.a(e);
            }
            b.a(e);
        }
    }
}
