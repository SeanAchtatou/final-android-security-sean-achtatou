package com.xiaomi.mipush.sdk;

import android.content.Context;
import com.sina.weibo.sdk.exception.WeiboAuthException;
import com.xiaomi.channel.commonutils.misc.d;
import com.xiaomi.push.service.v;
import com.xiaomi.push.service.w;
import com.xiaomi.xmpush.thrift.a;
import com.xiaomi.xmpush.thrift.ad;
import com.xiaomi.xmpush.thrift.c;
import com.xiaomi.xmpush.thrift.f;
import com.xiaomi.xmpush.thrift.i;
import com.xiaomi.xmpush.thrift.l;
import com.xiaomi.xmpush.thrift.r;

public class g extends d.a {

    /* renamed from: a  reason: collision with root package name */
    private Context f2916a;

    public g(Context context) {
        this.f2916a = context;
    }

    public int a() {
        return 2;
    }

    public void run() {
        v a2 = v.a(this.f2916a);
        l lVar = new l();
        lVar.a(w.a(a2, c.MISC_CONFIG));
        lVar.b(w.a(a2, c.PLUGIN_CONFIG));
        r rVar = new r(WeiboAuthException.DEFAULT_AUTH_ERROR_CODE, false);
        rVar.c(f.DailyCheckClientConfig.p);
        rVar.a(ad.a(lVar));
        j.a(this.f2916a).a(rVar, a.Notification, (i) null);
    }
}
