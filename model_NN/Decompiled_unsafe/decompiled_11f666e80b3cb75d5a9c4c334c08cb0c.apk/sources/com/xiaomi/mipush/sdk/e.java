package com.xiaomi.mipush.sdk;

import android.content.Context;
import android.content.pm.PackageInfo;
import java.util.List;

final class e implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Context f2914a;

    e(Context context) {
        this.f2914a = context;
    }

    public void run() {
        try {
            List<PackageInfo> installedPackages = this.f2914a.getPackageManager().getInstalledPackages(4);
            if (installedPackages != null) {
                for (PackageInfo access$100 : installedPackages) {
                    MiPushClient.awakePushServiceByPackageInfo(this.f2914a, access$100);
                }
            }
        } catch (Throwable th) {
        }
    }
}
