package com.xiaomi.mipush.sdk;

import android.database.ContentObserver;
import android.os.Handler;
import com.xiaomi.channel.commonutils.network.d;
import com.xiaomi.push.service.ab;

class k extends ContentObserver {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ j f2923a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    k(j jVar, Handler handler) {
        super(handler);
        this.f2923a = jVar;
    }

    public void onChange(boolean z) {
        Integer unused = this.f2923a.g = Integer.valueOf(ab.a(this.f2923a.c).b());
        if (this.f2923a.g.intValue() != 0) {
            this.f2923a.c.getContentResolver().unregisterContentObserver(this);
            if (d.d(this.f2923a.c)) {
                this.f2923a.c();
            }
        }
    }
}
