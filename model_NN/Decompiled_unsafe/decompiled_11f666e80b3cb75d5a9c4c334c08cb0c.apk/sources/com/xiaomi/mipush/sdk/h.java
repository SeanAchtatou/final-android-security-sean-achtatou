package com.xiaomi.mipush.sdk;

import android.content.Context;
import android.text.TextUtils;
import com.xiaomi.xmpush.thrift.a;
import com.xiaomi.xmpush.thrift.aa;
import com.xiaomi.xmpush.thrift.ac;
import com.xiaomi.xmpush.thrift.ad;
import com.xiaomi.xmpush.thrift.j;
import com.xiaomi.xmpush.thrift.k;
import com.xiaomi.xmpush.thrift.n;
import com.xiaomi.xmpush.thrift.o;
import com.xiaomi.xmpush.thrift.r;
import com.xiaomi.xmpush.thrift.t;
import com.xiaomi.xmpush.thrift.v;
import com.xiaomi.xmpush.thrift.w;
import com.xiaomi.xmpush.thrift.y;
import java.nio.ByteBuffer;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import org.apache.thrift.b;
import org.apache.thrift.f;

public class h {

    /* renamed from: a  reason: collision with root package name */
    private static final byte[] f2917a = {100, 23, 84, 114, 72, 0, 4, 97, 73, 97, 2, 52, 84, 102, 18, 32};

    protected static <T extends b<T, ?>> o a(Context context, T t, a aVar) {
        return a(context, t, aVar, !aVar.equals(a.Registration), context.getPackageName(), a.a(context).c());
    }

    protected static <T extends b<T, ?>> o a(Context context, T t, a aVar, boolean z, String str, String str2) {
        return a(context, t, aVar, !aVar.equals(a.Registration), str, str2, false);
    }

    protected static <T extends b<T, ?>> o a(Context context, T t, a aVar, boolean z, String str, String str2, boolean z2) {
        byte[] a2 = ad.a(t);
        if (a2 == null) {
            com.xiaomi.channel.commonutils.logger.b.a("invoke convertThriftObjectToBytes method, return null.");
            return null;
        }
        o oVar = new o();
        if (z2) {
            a2 = com.xiaomi.channel.commonutils.file.a.a(a2);
        }
        if (z) {
            String f = a.a(context).f();
            if (TextUtils.isEmpty(f)) {
                com.xiaomi.channel.commonutils.logger.b.a("regSecret is empty, return null");
                return null;
            }
            try {
                a2 = b(com.xiaomi.channel.commonutils.string.a.a(f), a2);
            } catch (Exception e) {
                com.xiaomi.channel.commonutils.logger.b.d("encryption error. ");
            }
        }
        j jVar = new j();
        jVar.f2959a = 5;
        jVar.b = "fakeid";
        oVar.a(jVar);
        oVar.a(ByteBuffer.wrap(a2));
        oVar.a(aVar);
        oVar.c(true);
        oVar.b(str);
        oVar.a(z);
        oVar.a(str2);
        return oVar;
    }

    private static Cipher a(byte[] bArr, int i) {
        SecretKeySpec secretKeySpec = new SecretKeySpec(bArr, "AES");
        IvParameterSpec ivParameterSpec = new IvParameterSpec(f2917a);
        Cipher instance = Cipher.getInstance("AES/CBC/PKCS5Padding");
        instance.init(i, secretKeySpec, ivParameterSpec);
        return instance;
    }

    public static b a(Context context, o oVar) {
        byte[] f;
        if (oVar.c()) {
            try {
                f = a(com.xiaomi.channel.commonutils.string.a.a(a.a(context).f()), oVar.f());
            } catch (Exception e) {
                throw new f("the aes decrypt failed.", e);
            }
        } else {
            f = oVar.f();
        }
        b a2 = a(oVar.a());
        if (a2 != null) {
            ad.a(a2, f);
        }
        return a2;
    }

    private static b a(a aVar) {
        switch (aVar) {
            case Registration:
                return new t();
            case UnRegistration:
                return new aa();
            case Subscription:
                return new y();
            case UnSubscription:
                return new ac();
            case SendMessage:
                return new w();
            case AckMessage:
                return new k();
            case SetConfig:
                return new n();
            case ReportFeedback:
                return new v();
            case Notification:
                return new r();
            case Command:
                return new n();
            default:
                return null;
        }
    }

    public static byte[] a(byte[] bArr, byte[] bArr2) {
        return a(bArr, 2).doFinal(bArr2);
    }

    public static byte[] b(byte[] bArr, byte[] bArr2) {
        return a(bArr, 1).doFinal(bArr2);
    }
}
