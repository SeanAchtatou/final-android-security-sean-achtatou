package com.xiaomi.xmpush.thrift;

import com.sina.weibo.sdk.component.WidgetRequestParam;
import java.io.Serializable;
import java.util.Collections;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.apache.thrift.b;
import org.apache.thrift.meta_data.e;
import org.apache.thrift.meta_data.g;
import org.apache.thrift.protocol.c;
import org.apache.thrift.protocol.f;
import org.apache.thrift.protocol.i;
import org.apache.thrift.protocol.k;

public class u implements Serializable, Cloneable, b<u, a> {
    public static final Map<a, org.apache.thrift.meta_data.b> g;
    private static final k h = new k("XmPushActionSendFeedback");
    private static final c i = new c("debug", (byte) 11, 1);
    private static final c j = new c("target", (byte) 12, 2);
    private static final c k = new c("id", (byte) 11, 3);
    private static final c l = new c("appId", (byte) 11, 4);
    private static final c m = new c("feedbacks", (byte) 13, 5);
    private static final c n = new c(WidgetRequestParam.REQ_PARAM_COMMENT_CATEGORY, (byte) 11, 6);

    /* renamed from: a  reason: collision with root package name */
    public String f2981a;
    public j b;
    public String c;
    public String d;
    public Map<String, String> e;
    public String f;

    public enum a {
        DEBUG(1, "debug"),
        TARGET(2, "target"),
        ID(3, "id"),
        APP_ID(4, "appId"),
        FEEDBACKS(5, "feedbacks"),
        CATEGORY(6, WidgetRequestParam.REQ_PARAM_COMMENT_CATEGORY);
        
        private static final Map<String, a> g = new HashMap();
        private final short h;
        private final String i;

        static {
            Iterator it = EnumSet.allOf(a.class).iterator();
            while (it.hasNext()) {
                a aVar = (a) it.next();
                g.put(aVar.a(), aVar);
            }
        }

        private a(short s, String str) {
            this.h = s;
            this.i = str;
        }

        public String a() {
            return this.i;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [com.xiaomi.xmpush.thrift.u$a, org.apache.thrift.meta_data.b]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        EnumMap enumMap = new EnumMap(a.class);
        enumMap.put((Object) a.DEBUG, (Object) new org.apache.thrift.meta_data.b("debug", (byte) 2, new org.apache.thrift.meta_data.c((byte) 11)));
        enumMap.put((Object) a.TARGET, (Object) new org.apache.thrift.meta_data.b("target", (byte) 2, new g((byte) 12, j.class)));
        enumMap.put((Object) a.ID, (Object) new org.apache.thrift.meta_data.b("id", (byte) 1, new org.apache.thrift.meta_data.c((byte) 11)));
        enumMap.put((Object) a.APP_ID, (Object) new org.apache.thrift.meta_data.b("appId", (byte) 1, new org.apache.thrift.meta_data.c((byte) 11)));
        enumMap.put((Object) a.FEEDBACKS, (Object) new org.apache.thrift.meta_data.b("feedbacks", (byte) 2, new e((byte) 13, new org.apache.thrift.meta_data.c((byte) 11), new org.apache.thrift.meta_data.c((byte) 11))));
        enumMap.put((Object) a.CATEGORY, (Object) new org.apache.thrift.meta_data.b(WidgetRequestParam.REQ_PARAM_COMMENT_CATEGORY, (byte) 2, new org.apache.thrift.meta_data.c((byte) 11)));
        g = Collections.unmodifiableMap(enumMap);
        org.apache.thrift.meta_data.b.a(u.class, g);
    }

    public void a(f fVar) {
        fVar.g();
        while (true) {
            c i2 = fVar.i();
            if (i2.b == 0) {
                fVar.h();
                g();
                return;
            }
            switch (i2.c) {
                case 1:
                    if (i2.b != 11) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.f2981a = fVar.w();
                        break;
                    }
                case 2:
                    if (i2.b != 12) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.b = new j();
                        this.b.a(fVar);
                        break;
                    }
                case 3:
                    if (i2.b != 11) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.c = fVar.w();
                        break;
                    }
                case 4:
                    if (i2.b != 11) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.d = fVar.w();
                        break;
                    }
                case 5:
                    if (i2.b != 13) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        org.apache.thrift.protocol.e k2 = fVar.k();
                        this.e = new HashMap(k2.c * 2);
                        for (int i3 = 0; i3 < k2.c; i3++) {
                            this.e.put(fVar.w(), fVar.w());
                        }
                        fVar.l();
                        break;
                    }
                case 6:
                    if (i2.b != 11) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.f = fVar.w();
                        break;
                    }
                default:
                    i.a(fVar, i2.b);
                    break;
            }
            fVar.j();
        }
    }

    public boolean a() {
        return this.f2981a != null;
    }

    public void b(f fVar) {
        g();
        fVar.a(h);
        if (this.f2981a != null && a()) {
            fVar.a(i);
            fVar.a(this.f2981a);
            fVar.b();
        }
        if (this.b != null && b()) {
            fVar.a(j);
            this.b.b(fVar);
            fVar.b();
        }
        if (this.c != null) {
            fVar.a(k);
            fVar.a(this.c);
            fVar.b();
        }
        if (this.d != null) {
            fVar.a(l);
            fVar.a(this.d);
            fVar.b();
        }
        if (this.e != null && e()) {
            fVar.a(m);
            fVar.a(new org.apache.thrift.protocol.e((byte) 11, (byte) 11, this.e.size()));
            for (Map.Entry next : this.e.entrySet()) {
                fVar.a((String) next.getKey());
                fVar.a((String) next.getValue());
            }
            fVar.d();
            fVar.b();
        }
        if (this.f != null && f()) {
            fVar.a(n);
            fVar.a(this.f);
            fVar.b();
        }
        fVar.c();
        fVar.a();
    }

    public boolean b() {
        return this.b != null;
    }

    public boolean e() {
        return this.e != null;
    }

    public boolean f() {
        return this.f != null;
    }

    public void g() {
        if (this.c == null) {
            throw new org.apache.thrift.protocol.g("Required field 'id' was not present! Struct: " + toString());
        } else if (this.d == null) {
            throw new org.apache.thrift.protocol.g("Required field 'appId' was not present! Struct: " + toString());
        }
    }

    public String toString() {
        boolean z = false;
        StringBuilder sb = new StringBuilder("XmPushActionSendFeedback(");
        boolean z2 = true;
        if (a()) {
            sb.append("debug:");
            if (this.f2981a == null) {
                sb.append("null");
            } else {
                sb.append(this.f2981a);
            }
            z2 = false;
        }
        if (b()) {
            if (!z2) {
                sb.append(", ");
            }
            sb.append("target:");
            if (this.b == null) {
                sb.append("null");
            } else {
                sb.append(this.b);
            }
        } else {
            z = z2;
        }
        if (!z) {
            sb.append(", ");
        }
        sb.append("id:");
        if (this.c == null) {
            sb.append("null");
        } else {
            sb.append(this.c);
        }
        sb.append(", ");
        sb.append("appId:");
        if (this.d == null) {
            sb.append("null");
        } else {
            sb.append(this.d);
        }
        if (e()) {
            sb.append(", ");
            sb.append("feedbacks:");
            if (this.e == null) {
                sb.append("null");
            } else {
                sb.append(this.e);
            }
        }
        if (f()) {
            sb.append(", ");
            sb.append("category:");
            if (this.f == null) {
                sb.append("null");
            } else {
                sb.append(this.f);
            }
        }
        sb.append(")");
        return sb.toString();
    }
}
