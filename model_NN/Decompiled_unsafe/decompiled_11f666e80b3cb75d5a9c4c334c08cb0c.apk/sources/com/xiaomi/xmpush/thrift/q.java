package com.xiaomi.xmpush.thrift;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.Collections;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.apache.thrift.b;
import org.apache.thrift.meta_data.d;
import org.apache.thrift.meta_data.g;
import org.apache.thrift.protocol.c;
import org.apache.thrift.protocol.f;
import org.apache.thrift.protocol.i;
import org.apache.thrift.protocol.k;

public class q implements Serializable, Cloneable, b<q, a> {
    public static final Map<a, org.apache.thrift.meta_data.b> d;
    private static final k e = new k("XmPushActionNormalConfig");
    private static final c f = new c("normalConfigs", (byte) 15, 1);
    private static final c g = new c("appId", (byte) 10, 4);
    private static final c h = new c("packageName", (byte) 11, 5);

    /* renamed from: a  reason: collision with root package name */
    public List<e> f2973a;
    public long b;
    public String c;
    private BitSet i = new BitSet(1);

    public enum a {
        NORMAL_CONFIGS(1, "normalConfigs"),
        APP_ID(4, "appId"),
        PACKAGE_NAME(5, "packageName");
        
        private static final Map<String, a> d = new HashMap();
        private final short e;
        private final String f;

        static {
            Iterator it = EnumSet.allOf(a.class).iterator();
            while (it.hasNext()) {
                a aVar = (a) it.next();
                d.put(aVar.a(), aVar);
            }
        }

        private a(short s, String str) {
            this.e = s;
            this.f = str;
        }

        public String a() {
            return this.f;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [com.xiaomi.xmpush.thrift.q$a, org.apache.thrift.meta_data.b]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        EnumMap enumMap = new EnumMap(a.class);
        enumMap.put((Object) a.NORMAL_CONFIGS, (Object) new org.apache.thrift.meta_data.b("normalConfigs", (byte) 1, new d((byte) 15, new g((byte) 12, e.class))));
        enumMap.put((Object) a.APP_ID, (Object) new org.apache.thrift.meta_data.b("appId", (byte) 2, new org.apache.thrift.meta_data.c((byte) 10)));
        enumMap.put((Object) a.PACKAGE_NAME, (Object) new org.apache.thrift.meta_data.b("packageName", (byte) 2, new org.apache.thrift.meta_data.c((byte) 11)));
        d = Collections.unmodifiableMap(enumMap);
        org.apache.thrift.meta_data.b.a(q.class, d);
    }

    public List<e> a() {
        return this.f2973a;
    }

    public void a(f fVar) {
        fVar.g();
        while (true) {
            c i2 = fVar.i();
            if (i2.b == 0) {
                fVar.h();
                e();
                return;
            }
            switch (i2.c) {
                case 1:
                    if (i2.b != 15) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        org.apache.thrift.protocol.d m = fVar.m();
                        this.f2973a = new ArrayList(m.b);
                        for (int i3 = 0; i3 < m.b; i3++) {
                            e eVar = new e();
                            eVar.a(fVar);
                            this.f2973a.add(eVar);
                        }
                        fVar.n();
                        break;
                    }
                case 2:
                case 3:
                default:
                    i.a(fVar, i2.b);
                    break;
                case 4:
                    if (i2.b != 10) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.b = fVar.u();
                        a(true);
                        break;
                    }
                case 5:
                    if (i2.b != 11) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.c = fVar.w();
                        break;
                    }
            }
            fVar.j();
        }
    }

    public void a(boolean z) {
        this.i.set(0, z);
    }

    public void b(f fVar) {
        e();
        fVar.a(e);
        if (this.f2973a != null) {
            fVar.a(f);
            fVar.a(new org.apache.thrift.protocol.d((byte) 12, this.f2973a.size()));
            for (e b2 : this.f2973a) {
                b2.b(fVar);
            }
            fVar.e();
            fVar.b();
        }
        if (c()) {
            fVar.a(g);
            fVar.a(this.b);
            fVar.b();
        }
        if (this.c != null && d()) {
            fVar.a(h);
            fVar.a(this.c);
            fVar.b();
        }
        fVar.c();
        fVar.a();
    }

    public boolean c() {
        return this.i.get(0);
    }

    public boolean d() {
        return this.c != null;
    }

    public void e() {
        if (this.f2973a == null) {
            throw new org.apache.thrift.protocol.g("Required field 'normalConfigs' was not present! Struct: " + toString());
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("XmPushActionNormalConfig(");
        sb.append("normalConfigs:");
        if (this.f2973a == null) {
            sb.append("null");
        } else {
            sb.append(this.f2973a);
        }
        if (c()) {
            sb.append(", ");
            sb.append("appId:");
            sb.append(this.b);
        }
        if (d()) {
            sb.append(", ");
            sb.append("packageName:");
            if (this.c == null) {
                sb.append("null");
            } else {
                sb.append(this.c);
            }
        }
        sb.append(")");
        return sb.toString();
    }
}
