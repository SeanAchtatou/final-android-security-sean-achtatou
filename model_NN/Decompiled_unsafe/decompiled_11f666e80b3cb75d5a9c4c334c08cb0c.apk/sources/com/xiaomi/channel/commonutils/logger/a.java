package com.xiaomi.channel.commonutils.logger;

import android.util.Log;

public class a implements LoggerInterface {

    /* renamed from: a  reason: collision with root package name */
    private String f2895a = "xiaomi";

    public void log(String str) {
        Log.v(this.f2895a, str);
    }

    public void log(String str, Throwable th) {
        Log.v(this.f2895a, str, th);
    }
}
