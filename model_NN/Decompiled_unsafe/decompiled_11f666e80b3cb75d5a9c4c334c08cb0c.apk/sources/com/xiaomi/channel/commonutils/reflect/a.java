package com.xiaomi.channel.commonutils.reflect;

import android.util.Log;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

public class a {

    /* renamed from: a  reason: collision with root package name */
    private static final Map<Class<?>, Class<?>> f2904a = new HashMap();

    /* renamed from: com.xiaomi.channel.commonutils.reflect.a$a  reason: collision with other inner class name */
    public class C0073a<T> {

        /* renamed from: a  reason: collision with root package name */
        public final Class<? extends T> f2905a;
        public final T b;
    }

    static {
        f2904a.put(Boolean.class, Boolean.TYPE);
        f2904a.put(Byte.class, Byte.TYPE);
        f2904a.put(Character.class, Character.TYPE);
        f2904a.put(Short.class, Short.TYPE);
        f2904a.put(Integer.class, Integer.TYPE);
        f2904a.put(Float.class, Float.TYPE);
        f2904a.put(Long.class, Long.TYPE);
        f2904a.put(Double.class, Double.TYPE);
        f2904a.put(Boolean.TYPE, Boolean.TYPE);
        f2904a.put(Byte.TYPE, Byte.TYPE);
        f2904a.put(Character.TYPE, Character.TYPE);
        f2904a.put(Short.TYPE, Short.TYPE);
        f2904a.put(Integer.TYPE, Integer.TYPE);
        f2904a.put(Float.TYPE, Float.TYPE);
        f2904a.put(Long.TYPE, Long.TYPE);
        f2904a.put(Double.TYPE, Double.TYPE);
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:13:0x000c */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:14:0x000c */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:4:0x000c */
    public static <T> T a(Class<? extends Object> cls, Object obj, String str) {
        Field field = null;
        Class<? extends Object> cls2 = cls;
        while (field == null) {
            try {
                field = cls2.getDeclaredField(str);
                field.setAccessible(true);
                continue;
            } catch (NoSuchFieldException e) {
                cls2 = cls2.getSuperclass();
                continue;
            }
            if (cls2 == null) {
                throw new NoSuchFieldException();
            }
        }
        field.setAccessible(true);
        return field.get(obj);
    }

    public static <T> T a(Class<? extends Object> cls, String str) {
        try {
            return a(cls, (Object) null, str);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
            return null;
        } catch (IllegalAccessException e2) {
            e2.printStackTrace();
            return null;
        }
    }

    public static <T> T a(Class<?> cls, String str, Object... objArr) {
        return a(cls, str, a(objArr)).invoke(null, b(objArr));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.xiaomi.channel.commonutils.reflect.a.a(java.lang.Class<? extends java.lang.Object>, java.lang.Object, java.lang.String):T
     arg types: [java.lang.Class<?>, java.lang.Object, java.lang.String]
     candidates:
      com.xiaomi.channel.commonutils.reflect.a.a(java.lang.Class<?>, java.lang.String, java.lang.Object[]):T
      com.xiaomi.channel.commonutils.reflect.a.a(java.lang.Object, java.lang.String, java.lang.Object[]):T
      com.xiaomi.channel.commonutils.reflect.a.a(java.lang.String, java.lang.String, java.lang.Object[]):T
      com.xiaomi.channel.commonutils.reflect.a.a(java.lang.Class<?>, java.lang.String, java.lang.Class<?>[]):java.lang.reflect.Method
      com.xiaomi.channel.commonutils.reflect.a.a(java.lang.reflect.Method[], java.lang.String, java.lang.Class<?>[]):java.lang.reflect.Method
      com.xiaomi.channel.commonutils.reflect.a.a(java.lang.Class<? extends java.lang.Object>, java.lang.Object, java.lang.String):T */
    public static <T> T a(Object obj, String str) {
        try {
            return a((Class<? extends Object>) obj.getClass(), obj, str);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e2) {
            e2.printStackTrace();
        }
        return null;
    }

    public static <T> T a(Object obj, String str, Object... objArr) {
        try {
            return b(obj, str, objArr);
        } catch (Exception e) {
            Log.w("JavaCalls", "Meet exception when call Method '" + str + "' in " + obj, e);
            return null;
        }
    }

    public static <T> T a(String str, String str2, Object... objArr) {
        try {
            return a(Class.forName(str), str2, objArr);
        } catch (Exception e) {
            Log.w("JavaCalls", "Meet exception when call Method '" + str2 + "' in " + str, e);
            return null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.xiaomi.channel.commonutils.reflect.a.a(java.lang.Class<?>, java.lang.String, java.lang.Class<?>[]):java.lang.reflect.Method
     arg types: [java.lang.Class<? super ?>, java.lang.String, java.lang.Class<?>[]]
     candidates:
      com.xiaomi.channel.commonutils.reflect.a.a(java.lang.Class<? extends java.lang.Object>, java.lang.Object, java.lang.String):T
      com.xiaomi.channel.commonutils.reflect.a.a(java.lang.Class<?>, java.lang.String, java.lang.Object[]):T
      com.xiaomi.channel.commonutils.reflect.a.a(java.lang.Object, java.lang.String, java.lang.Object[]):T
      com.xiaomi.channel.commonutils.reflect.a.a(java.lang.String, java.lang.String, java.lang.Object[]):T
      com.xiaomi.channel.commonutils.reflect.a.a(java.lang.reflect.Method[], java.lang.String, java.lang.Class<?>[]):java.lang.reflect.Method
      com.xiaomi.channel.commonutils.reflect.a.a(java.lang.Class<?>, java.lang.String, java.lang.Class<?>[]):java.lang.reflect.Method */
    private static Method a(Class<?> cls, String str, Class<?>... clsArr) {
        Method a2 = a(cls.getDeclaredMethods(), str, clsArr);
        if (a2 != null) {
            a2.setAccessible(true);
            return a2;
        } else if (cls.getSuperclass() != null) {
            return a((Class<?>) cls.getSuperclass(), str, clsArr);
        } else {
            throw new NoSuchMethodException();
        }
    }

    private static Method a(Method[] methodArr, String str, Class<?>[] clsArr) {
        if (str == null) {
            throw new NullPointerException("Method name must not be null.");
        }
        for (Method method : methodArr) {
            if (method.getName().equals(str) && a(method.getParameterTypes(), clsArr)) {
                return method;
            }
        }
        return null;
    }

    private static boolean a(Class<?>[] clsArr, Class<?>[] clsArr2) {
        if (clsArr == null) {
            return clsArr2 == null || clsArr2.length == 0;
        } else if (clsArr2 == null) {
            return clsArr.length == 0;
        } else {
            if (clsArr.length != clsArr2.length) {
                return false;
            }
            for (int i = 0; i < clsArr.length; i++) {
                if (!clsArr[i].isAssignableFrom(clsArr2[i]) && (!f2904a.containsKey(clsArr[i]) || !f2904a.get(clsArr[i]).equals(f2904a.get(clsArr2[i])))) {
                    return false;
                }
            }
            return true;
        }
    }

    private static Class<?>[] a(Object... objArr) {
        if (objArr == null || objArr.length <= 0) {
            return null;
        }
        Class<?>[] clsArr = new Class[objArr.length];
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= objArr.length) {
                return clsArr;
            }
            Object obj = objArr[i2];
            if (obj == null || !(obj instanceof C0073a)) {
                clsArr[i2] = obj == null ? null : obj.getClass();
            } else {
                clsArr[i2] = ((C0073a) obj).f2905a;
            }
            i = i2 + 1;
        }
    }

    public static <T> T b(Object obj, String str, Object... objArr) {
        return a(obj.getClass(), str, a(objArr)).invoke(obj, b(objArr));
    }

    private static Object[] b(Object... objArr) {
        if (objArr == null || objArr.length <= 0) {
            return null;
        }
        Object[] objArr2 = new Object[objArr.length];
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= objArr.length) {
                return objArr2;
            }
            Object obj = objArr[i2];
            if (obj == null || !(obj instanceof C0073a)) {
                objArr2[i2] = obj;
            } else {
                objArr2[i2] = ((C0073a) obj).b;
            }
            i = i2 + 1;
        }
    }
}
