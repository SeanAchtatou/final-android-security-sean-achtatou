package com.huawei.hms.support.api.push;

import com.huawei.hms.support.api.push.PushReceiver;

/* synthetic */ class a {

    /* renamed from: a  reason: collision with root package name */
    static final /* synthetic */ int[] f759a = new int[PushReceiver.c.values().length];

    static {
        try {
            f759a[PushReceiver.c.ReceiveType_Token.ordinal()] = 1;
        } catch (NoSuchFieldError e) {
        }
        try {
            f759a[PushReceiver.c.ReceiveType_Msg.ordinal()] = 2;
        } catch (NoSuchFieldError e2) {
        }
        try {
            f759a[PushReceiver.c.ReceiveType_PushState.ordinal()] = 3;
        } catch (NoSuchFieldError e3) {
        }
        try {
            f759a[PushReceiver.c.ReceiveType_NotifyClick.ordinal()] = 4;
        } catch (NoSuchFieldError e4) {
        }
        try {
            f759a[PushReceiver.c.ReceiveType_ClickBtn.ordinal()] = 5;
        } catch (NoSuchFieldError e5) {
        }
    }
}
