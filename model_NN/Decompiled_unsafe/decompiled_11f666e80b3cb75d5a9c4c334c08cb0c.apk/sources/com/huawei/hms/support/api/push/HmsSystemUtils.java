package com.huawei.hms.support.api.push;

import android.os.Build;
import android.text.TextUtils;

public class HmsSystemUtils {
    public static final String HONOR = "honor".toLowerCase();
    public static final String HUAWEI = "Huawei".toLowerCase();
    private static final String phoneBrand = Build.BRAND;

    public static boolean isBrandHuaWei() {
        try {
            return TextUtils.equals(HUAWEI, phoneBrand.toLowerCase()) || TextUtils.equals(HONOR, phoneBrand.toLowerCase());
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }
}
