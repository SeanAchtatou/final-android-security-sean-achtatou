package com.huawei.hms.support.api.entity.core;

public interface CommonCode {
    public static final int OK = 0;

    public interface ErrorCode {
        public static final int ARGUMENTS_INVALID = 907135000;
        public static final int CLIENT_API_INVALID = 907135003;
        public static final int EXECUTE_TIMEOUT = 907135004;
        public static final int INTERNAL_ERROR = 907135001;
        public static final int NAMING_INVALID = 907135002;
    }

    public interface StatusCode {
        public static final int API_UNAVAILABLE = 1000;
    }
}
