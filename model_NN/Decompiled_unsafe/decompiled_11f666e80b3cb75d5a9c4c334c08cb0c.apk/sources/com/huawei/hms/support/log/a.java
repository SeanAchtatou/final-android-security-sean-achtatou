package com.huawei.hms.support.log;

import android.util.Log;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

public class a extends f {
    private static C0025a c = new C0025a();
    private String d;
    private boolean e = false;
    private boolean f = false;
    private boolean g = true;

    /* renamed from: com.huawei.hms.support.log.a$a  reason: collision with other inner class name */
    public class C0025a extends com.huawei.hms.support.log.b.a {

        /* renamed from: a  reason: collision with root package name */
        private BlockingQueue<b> f766a = new LinkedBlockingQueue();

        public C0025a() {
            super("logger");
        }

        public void a(b bVar) {
            this.f766a.offer(bVar);
        }

        /* access modifiers changed from: protected */
        public boolean a() {
            return true;
        }

        /* access modifiers changed from: protected */
        public boolean b() {
            try {
                b poll = this.f766a.poll(3, TimeUnit.SECONDS);
                if (poll != null) {
                    if (!e.a(poll.f768a)) {
                        poll.b.b(poll.f768a);
                    } else if (poll.f768a != null) {
                        return false;
                    }
                }
            } catch (InterruptedException e) {
            }
            return true;
        }

        /* access modifiers changed from: protected */
        public void c() {
        }
    }

    public class b {

        /* renamed from: a  reason: collision with root package name */
        e f768a;
        f b;

        public b(e eVar, f fVar) {
            if (eVar == null && fVar == null) {
                throw new NullPointerException("record and logger is null");
            }
            this.b = fVar;
            this.f768a = eVar;
        }
    }

    public a(String str, String str2, LogLevel logLevel) {
        super(str2, 409600);
        this.f775a = logLevel;
        this.d = str;
        try {
            a();
            c();
        } catch (Exception e2) {
            Log.e("AndroidLogger", "AndroidLogger init error", e2);
        }
    }

    public static void a() {
        if (!c.isAlive()) {
            c.start();
        }
    }

    private void b(String str, LogLevel logLevel, String str2) {
        if (c(logLevel)) {
            String str3 = this.d + "." + str;
            switch (logLevel.value()) {
                case 2:
                    Log.v(str3, str2);
                    return;
                case 3:
                    Log.d(str3, str2);
                    return;
                case 4:
                    Log.i(str3, str2);
                    return;
                case 5:
                    Log.w(str3, str2);
                    return;
                case 6:
                    Log.e(str3, str2);
                    return;
                default:
                    Log.w(str3, "[" + logLevel.toString() + "] " + str2);
                    return;
            }
        }
    }

    private void c() {
        boolean z = false;
        try {
            Class<?> cls = Class.forName("android.util.Log");
            boolean z2 = cls.getField("HWLog").getBoolean(null);
            boolean z3 = cls.getField("HWModuleLog").getBoolean(null);
            this.e = z2 || (z3 && Log.isLoggable(this.d, 3));
            if (cls.getField("HWINFO").getBoolean(null) || (z3 && Log.isLoggable(this.d, 4))) {
                z = true;
            }
            this.f = z;
        } catch (Exception e2) {
            Log.w("AndroidLogger", "reading configuration error:" + e2.getMessage());
        }
    }

    private boolean c(LogLevel logLevel) {
        switch (logLevel.value()) {
            case 3:
                return this.e;
            case 4:
                return this.f;
            case 5:
            case 6:
                return this.g;
            default:
                return false;
        }
    }

    public void a(e eVar) {
        c.a(new b(eVar, this));
    }

    public boolean a(LogLevel logLevel) {
        return this.f775a.value() <= logLevel.value();
    }

    public void b(e eVar) {
        try {
            String e2 = eVar.e();
            b(eVar.b, eVar.c, e2);
            a(eVar.b, eVar.c, eVar.d() + e2);
        } catch (OutOfMemoryError e3) {
            Log.e("AndroidLogger", "write error");
        }
    }
}
