package com.huawei.hms.support.api.push.a;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.text.TextUtils;
import java.io.File;
import java.util.List;

public abstract class a {

    /* renamed from: a  reason: collision with root package name */
    private static int f760a = -1;

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x002c, code lost:
        if (1 == com.huawei.hms.support.api.push.a.a.f760a) goto L_0x002e;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean a(android.content.Context r5) {
        /*
            r0 = 0
            r1 = 1
            java.lang.Class<com.huawei.hms.support.api.push.a.a> r2 = com.huawei.hms.support.api.push.a.a.class
            monitor-enter(r2)
            boolean r2 = com.huawei.hms.support.api.push.a.b.b()     // Catch:{ all -> 0x0045 }
            if (r2 == 0) goto L_0x0025
            java.lang.String r2 = "CommFun"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x0045 }
            r3.<init>()     // Catch:{ all -> 0x0045 }
            java.lang.String r4 = "existFrameworkPush:"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x0045 }
            int r4 = com.huawei.hms.support.api.push.a.a.f760a     // Catch:{ all -> 0x0045 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x0045 }
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x0045 }
            com.huawei.hms.support.api.push.a.b.a(r2, r3)     // Catch:{ all -> 0x0045 }
        L_0x0025:
            r2 = -1
            int r3 = com.huawei.hms.support.api.push.a.a.f760a     // Catch:{ all -> 0x0045 }
            if (r2 == r3) goto L_0x0033
            int r2 = com.huawei.hms.support.api.push.a.a.f760a     // Catch:{ all -> 0x0045 }
            if (r1 != r2) goto L_0x002f
        L_0x002e:
            r0 = r1
        L_0x002f:
            java.lang.Class<com.huawei.hms.support.api.push.a.a> r1 = com.huawei.hms.support.api.push.a.a.class
            monitor-exit(r1)
            return r0
        L_0x0033:
            boolean r2 = c(r5)     // Catch:{ all -> 0x0045 }
            if (r2 == 0) goto L_0x0041
            r2 = 1
            com.huawei.hms.support.api.push.a.a.f760a = r2     // Catch:{ all -> 0x0045 }
        L_0x003c:
            int r2 = com.huawei.hms.support.api.push.a.a.f760a     // Catch:{ all -> 0x0045 }
            if (r1 == r2) goto L_0x002e
            goto L_0x002f
        L_0x0041:
            r2 = 0
            com.huawei.hms.support.api.push.a.a.f760a = r2     // Catch:{ all -> 0x0045 }
            goto L_0x003c
        L_0x0045:
            r0 = move-exception
            java.lang.Class<com.huawei.hms.support.api.push.a.a> r1 = com.huawei.hms.support.api.push.a.a.class
            monitor-exit(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.huawei.hms.support.api.push.a.a.a(android.content.Context):boolean");
    }

    public static boolean a(Context context, String str) {
        if (context == null || str == null || TextUtils.isEmpty(str)) {
            return false;
        }
        try {
            if (context.getPackageManager().getApplicationInfo(str, 0) == null) {
                return false;
            }
            if (b.b()) {
                b.a("CommFun", str + " is installed");
            }
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    public static String b(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            if (!b.b()) {
                return "0.0";
            }
            b.a("CommFun", "package not exist");
            return "0.0";
        } catch (Exception e2) {
            if (!b.b()) {
                return "0.0";
            }
            b.a("CommFun", "getApkVersionName error", e2);
            return "0.0";
        }
    }

    private static boolean c(Context context) {
        if (b.b()) {
            b.a("CommFun", "existFrameworkPush:" + f760a);
        }
        try {
            if (!new File("/system/framework/" + "hwpush.jar").isFile()) {
                return false;
            }
            if (b.b()) {
                b.a("CommFun", "push jarFile is exist");
            }
            List<ResolveInfo> queryIntentServices = context.getPackageManager().queryIntentServices(new Intent().setClassName("android", "com.huawei.android.pushagentproxy.PushService"), 128);
            if (queryIntentServices != null && !queryIntentServices.isEmpty()) {
                if (b.c()) {
                    b.b("CommFun", "framework push exist, use framework push first");
                }
                return true;
            } else if (!b.c()) {
                return false;
            } else {
                b.b("CommFun", "framework push not exist, need vote apk or sdk to support pushservice");
                return false;
            }
        } catch (Exception e) {
            if (!b.b()) {
                return false;
            }
            b.d("CommFun", "get Apk version faild ,Exception e= " + e.toString());
            return false;
        }
    }
}
