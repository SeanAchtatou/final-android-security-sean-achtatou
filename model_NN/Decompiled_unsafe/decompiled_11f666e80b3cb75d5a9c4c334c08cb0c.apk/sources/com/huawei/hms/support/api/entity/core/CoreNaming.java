package com.huawei.hms.support.api.entity.core;

public interface CoreNaming {
    public static final String connect = "core.connect";
    public static final String disConnect = "core.disconnect";
}
