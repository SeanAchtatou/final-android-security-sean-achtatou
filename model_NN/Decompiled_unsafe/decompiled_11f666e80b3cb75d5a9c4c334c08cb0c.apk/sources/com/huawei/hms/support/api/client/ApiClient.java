package com.huawei.hms.support.api.client;

import android.content.Context;

public interface ApiClient {
    String getAppID();

    Context getContext();

    String getPackageName();

    String getTransportName();

    boolean isConnected();
}
