package com.huawei.hms.support.api.push.a.a.a;

import android.content.Context;
import android.text.TextUtils;

public abstract class d {
    public static String a(Context context, String str) {
        return TextUtils.isEmpty(str) ? "" : a.a(str);
    }

    public static String b(Context context, String str) {
        return TextUtils.isEmpty(str) ? "" : a.b(str);
    }
}
