package com.huawei.hms.support.api.push.a.a.a;

import android.text.TextUtils;
import com.huawei.hms.support.api.push.a.b;

public abstract class a {
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x006f, code lost:
        return null;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String a(java.lang.String r5) {
        /*
            boolean r0 = android.text.TextUtils.isEmpty(r5)
            if (r0 == 0) goto L_0x0009
            java.lang.String r0 = ""
        L_0x0008:
            return r0
        L_0x0009:
            byte[] r0 = a()     // Catch:{ IllegalArgumentException -> 0x004c, InvalidKeyException -> 0x0071, InvalidAlgorithmParameterException -> 0x0095, IllegalBlockSizeException -> 0x00b9, UnsupportedEncodingException -> 0x00dd, Exception -> 0x0102 }
            int r1 = r0.length     // Catch:{ IllegalArgumentException -> 0x004c, InvalidKeyException -> 0x0071, InvalidAlgorithmParameterException -> 0x0095, IllegalBlockSizeException -> 0x00b9, UnsupportedEncodingException -> 0x00dd, Exception -> 0x0102 }
            if (r1 != 0) goto L_0x0013
            java.lang.String r0 = ""
            goto L_0x0008
        L_0x0013:
            javax.crypto.spec.SecretKeySpec r1 = new javax.crypto.spec.SecretKeySpec     // Catch:{ IllegalArgumentException -> 0x004c, InvalidKeyException -> 0x0071, InvalidAlgorithmParameterException -> 0x0095, IllegalBlockSizeException -> 0x00b9, UnsupportedEncodingException -> 0x00dd, Exception -> 0x0102 }
            java.lang.String r2 = "AES"
            r1.<init>(r0, r2)     // Catch:{ IllegalArgumentException -> 0x004c, InvalidKeyException -> 0x0071, InvalidAlgorithmParameterException -> 0x0095, IllegalBlockSizeException -> 0x00b9, UnsupportedEncodingException -> 0x00dd, Exception -> 0x0102 }
            java.lang.String r0 = "AES/CBC/PKCS5Padding"
            javax.crypto.Cipher r0 = javax.crypto.Cipher.getInstance(r0)     // Catch:{ IllegalArgumentException -> 0x004c, InvalidKeyException -> 0x0071, InvalidAlgorithmParameterException -> 0x0095, IllegalBlockSizeException -> 0x00b9, UnsupportedEncodingException -> 0x00dd, Exception -> 0x0102 }
            java.security.SecureRandom r2 = new java.security.SecureRandom     // Catch:{ IllegalArgumentException -> 0x004c, InvalidKeyException -> 0x0071, InvalidAlgorithmParameterException -> 0x0095, IllegalBlockSizeException -> 0x00b9, UnsupportedEncodingException -> 0x00dd, Exception -> 0x0102 }
            r2.<init>()     // Catch:{ IllegalArgumentException -> 0x004c, InvalidKeyException -> 0x0071, InvalidAlgorithmParameterException -> 0x0095, IllegalBlockSizeException -> 0x00b9, UnsupportedEncodingException -> 0x00dd, Exception -> 0x0102 }
            r3 = 16
            byte[] r3 = new byte[r3]     // Catch:{ IllegalArgumentException -> 0x004c, InvalidKeyException -> 0x0071, InvalidAlgorithmParameterException -> 0x0095, IllegalBlockSizeException -> 0x00b9, UnsupportedEncodingException -> 0x00dd, Exception -> 0x0102 }
            r2.nextBytes(r3)     // Catch:{ IllegalArgumentException -> 0x004c, InvalidKeyException -> 0x0071, InvalidAlgorithmParameterException -> 0x0095, IllegalBlockSizeException -> 0x00b9, UnsupportedEncodingException -> 0x00dd, Exception -> 0x0102 }
            r2 = 1
            javax.crypto.spec.IvParameterSpec r4 = new javax.crypto.spec.IvParameterSpec     // Catch:{ IllegalArgumentException -> 0x004c, InvalidKeyException -> 0x0071, InvalidAlgorithmParameterException -> 0x0095, IllegalBlockSizeException -> 0x00b9, UnsupportedEncodingException -> 0x00dd, Exception -> 0x0102 }
            r4.<init>(r3)     // Catch:{ IllegalArgumentException -> 0x004c, InvalidKeyException -> 0x0071, InvalidAlgorithmParameterException -> 0x0095, IllegalBlockSizeException -> 0x00b9, UnsupportedEncodingException -> 0x00dd, Exception -> 0x0102 }
            r0.init(r2, r1, r4)     // Catch:{ IllegalArgumentException -> 0x004c, InvalidKeyException -> 0x0071, InvalidAlgorithmParameterException -> 0x0095, IllegalBlockSizeException -> 0x00b9, UnsupportedEncodingException -> 0x00dd, Exception -> 0x0102 }
            java.lang.String r1 = "UTF-8"
            byte[] r1 = r5.getBytes(r1)     // Catch:{ IllegalArgumentException -> 0x004c, InvalidKeyException -> 0x0071, InvalidAlgorithmParameterException -> 0x0095, IllegalBlockSizeException -> 0x00b9, UnsupportedEncodingException -> 0x00dd, Exception -> 0x0102 }
            byte[] r0 = r0.doFinal(r1)     // Catch:{ IllegalArgumentException -> 0x004c, InvalidKeyException -> 0x0071, InvalidAlgorithmParameterException -> 0x0095, IllegalBlockSizeException -> 0x00b9, UnsupportedEncodingException -> 0x00dd, Exception -> 0x0102 }
            java.lang.String r1 = com.huawei.hms.support.api.push.a.a.a.a(r3)     // Catch:{ IllegalArgumentException -> 0x004c, InvalidKeyException -> 0x0071, InvalidAlgorithmParameterException -> 0x0095, IllegalBlockSizeException -> 0x00b9, UnsupportedEncodingException -> 0x00dd, Exception -> 0x0102 }
            java.lang.String r0 = com.huawei.hms.support.api.push.a.a.a.a(r0)     // Catch:{ IllegalArgumentException -> 0x004c, InvalidKeyException -> 0x0071, InvalidAlgorithmParameterException -> 0x0095, IllegalBlockSizeException -> 0x00b9, UnsupportedEncodingException -> 0x00dd, Exception -> 0x0102 }
            java.lang.String r0 = a(r1, r0)     // Catch:{ IllegalArgumentException -> 0x004c, InvalidKeyException -> 0x0071, InvalidAlgorithmParameterException -> 0x0095, IllegalBlockSizeException -> 0x00b9, UnsupportedEncodingException -> 0x00dd, Exception -> 0x0102 }
            goto L_0x0008
        L_0x004c:
            r0 = move-exception
            boolean r1 = com.huawei.hms.support.api.push.a.b.e()
            if (r1 == 0) goto L_0x006f
            java.lang.String r1 = "AES128_CBC"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "IllegalArgumentException aes cbc encrypter data error"
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r0 = r0.getMessage()
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r0 = r0.toString()
            com.huawei.hms.support.api.push.a.b.d(r1, r0)
        L_0x006f:
            r0 = 0
            goto L_0x0008
        L_0x0071:
            r0 = move-exception
            boolean r1 = com.huawei.hms.support.api.push.a.b.e()
            if (r1 == 0) goto L_0x006f
            java.lang.String r1 = "AES128_CBC"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "InvalidKeyException aes cbc encrypter data error"
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r0 = r0.getMessage()
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r0 = r0.toString()
            com.huawei.hms.support.api.push.a.b.d(r1, r0)
            goto L_0x006f
        L_0x0095:
            r0 = move-exception
            boolean r1 = com.huawei.hms.support.api.push.a.b.e()
            if (r1 == 0) goto L_0x006f
            java.lang.String r1 = "AES128_CBC"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "InvalidAlgorithmParameterException aes cbc encrypter data error"
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r0 = r0.getMessage()
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r0 = r0.toString()
            com.huawei.hms.support.api.push.a.b.d(r1, r0)
            goto L_0x006f
        L_0x00b9:
            r0 = move-exception
            boolean r1 = com.huawei.hms.support.api.push.a.b.e()
            if (r1 == 0) goto L_0x006f
            java.lang.String r1 = "AES128_CBC"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "IllegalBlockSizeException aes cbc encrypter data error"
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r0 = r0.getMessage()
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r0 = r0.toString()
            com.huawei.hms.support.api.push.a.b.d(r1, r0)
            goto L_0x006f
        L_0x00dd:
            r0 = move-exception
            boolean r1 = com.huawei.hms.support.api.push.a.b.e()
            if (r1 == 0) goto L_0x006f
            java.lang.String r1 = "AES128_CBC"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "UnsupportedEncodingException aes cbc encrypter data error"
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r0 = r0.getMessage()
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r0 = r0.toString()
            com.huawei.hms.support.api.push.a.b.d(r1, r0)
            goto L_0x006f
        L_0x0102:
            r0 = move-exception
            boolean r1 = com.huawei.hms.support.api.push.a.b.e()
            if (r1 == 0) goto L_0x006f
            java.lang.String r1 = "AES128_CBC"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "aes cbc encrypter data error"
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r0 = r0.getMessage()
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r0 = r0.toString()
            com.huawei.hms.support.api.push.a.b.d(r1, r0)
            goto L_0x006f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.huawei.hms.support.api.push.a.a.a.a.a(java.lang.String):java.lang.String");
    }

    private static String a(String str, String str2) {
        if (TextUtils.isEmpty(str) || TextUtils.isEmpty(str2)) {
            return "";
        }
        try {
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append(str2.substring(0, 6));
            stringBuffer.append(str.substring(0, 6));
            stringBuffer.append(str2.substring(6, 10));
            stringBuffer.append(str.substring(6, 16));
            stringBuffer.append(str2.substring(10, 16));
            stringBuffer.append(str.substring(16));
            stringBuffer.append(str2.substring(16));
            return stringBuffer.toString();
        } catch (Exception e) {
            if (b.e()) {
                b.d("AES128_CBC", e.getMessage());
            }
            return "";
        }
    }

    private static byte[] a() {
        byte[] b = com.huawei.hms.support.api.push.a.a.a.b(com.huawei.hms.support.api.push.a.a.b.a());
        byte[] b2 = com.huawei.hms.support.api.push.a.a.a.b(b.a());
        return a(a(a(b, b2), com.huawei.hms.support.api.push.a.a.a.b("2A57086C86EF54970C1E6EB37BFC72B1")));
    }

    private static byte[] a(byte[] bArr) {
        if (bArr == null || bArr.length == 0) {
            return new byte[0];
        }
        for (int i = 0; i < bArr.length; i++) {
            bArr[i] = (byte) (bArr[i] >> 2);
        }
        return bArr;
    }

    private static byte[] a(byte[] bArr, byte[] bArr2) {
        if (bArr == null || bArr2 == null || bArr.length == 0 || bArr2.length == 0) {
            return new byte[0];
        }
        int length = bArr.length;
        if (length != bArr2.length) {
            return new byte[0];
        }
        byte[] bArr3 = new byte[length];
        for (int i = 0; i < length; i++) {
            bArr3[i] = (byte) (bArr[i] ^ bArr2[i]);
        }
        return bArr3;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:23:0x006f, code lost:
        return "";
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String b(java.lang.String r6) {
        /*
            boolean r0 = android.text.TextUtils.isEmpty(r6)
            if (r0 == 0) goto L_0x0009
            java.lang.String r0 = ""
        L_0x0008:
            return r0
        L_0x0009:
            byte[] r0 = a()     // Catch:{ IllegalArgumentException -> 0x0061, InvalidKeyException -> 0x0072, InvalidAlgorithmParameterException -> 0x0081, IllegalBlockSizeException -> 0x0090, BadPaddingException -> 0x009f, UnsupportedEncodingException -> 0x00ae, NoSuchAlgorithmException -> 0x00bd, NoSuchPaddingException -> 0x00cc, Exception -> 0x00db }
            int r1 = r0.length     // Catch:{ IllegalArgumentException -> 0x0061, InvalidKeyException -> 0x0072, InvalidAlgorithmParameterException -> 0x0081, IllegalBlockSizeException -> 0x0090, BadPaddingException -> 0x009f, UnsupportedEncodingException -> 0x00ae, NoSuchAlgorithmException -> 0x00bd, NoSuchPaddingException -> 0x00cc, Exception -> 0x00db }
            if (r1 != 0) goto L_0x0013
            java.lang.String r0 = ""
            goto L_0x0008
        L_0x0013:
            javax.crypto.spec.SecretKeySpec r1 = new javax.crypto.spec.SecretKeySpec     // Catch:{ IllegalArgumentException -> 0x0061, InvalidKeyException -> 0x0072, InvalidAlgorithmParameterException -> 0x0081, IllegalBlockSizeException -> 0x0090, BadPaddingException -> 0x009f, UnsupportedEncodingException -> 0x00ae, NoSuchAlgorithmException -> 0x00bd, NoSuchPaddingException -> 0x00cc, Exception -> 0x00db }
            java.lang.String r2 = "AES"
            r1.<init>(r0, r2)     // Catch:{ IllegalArgumentException -> 0x0061, InvalidKeyException -> 0x0072, InvalidAlgorithmParameterException -> 0x0081, IllegalBlockSizeException -> 0x0090, BadPaddingException -> 0x009f, UnsupportedEncodingException -> 0x00ae, NoSuchAlgorithmException -> 0x00bd, NoSuchPaddingException -> 0x00cc, Exception -> 0x00db }
            java.lang.String r0 = "AES/CBC/PKCS5Padding"
            javax.crypto.Cipher r2 = javax.crypto.Cipher.getInstance(r0)     // Catch:{ IllegalArgumentException -> 0x0061, InvalidKeyException -> 0x0072, InvalidAlgorithmParameterException -> 0x0081, IllegalBlockSizeException -> 0x0090, BadPaddingException -> 0x009f, UnsupportedEncodingException -> 0x00ae, NoSuchAlgorithmException -> 0x00bd, NoSuchPaddingException -> 0x00cc, Exception -> 0x00db }
            java.lang.String r0 = c(r6)     // Catch:{ IllegalArgumentException -> 0x0061, InvalidKeyException -> 0x0072, InvalidAlgorithmParameterException -> 0x0081, IllegalBlockSizeException -> 0x0090, BadPaddingException -> 0x009f, UnsupportedEncodingException -> 0x00ae, NoSuchAlgorithmException -> 0x00bd, NoSuchPaddingException -> 0x00cc, Exception -> 0x00db }
            java.lang.String r3 = d(r6)     // Catch:{ IllegalArgumentException -> 0x0061, InvalidKeyException -> 0x0072, InvalidAlgorithmParameterException -> 0x0081, IllegalBlockSizeException -> 0x0090, BadPaddingException -> 0x009f, UnsupportedEncodingException -> 0x00ae, NoSuchAlgorithmException -> 0x00bd, NoSuchPaddingException -> 0x00cc, Exception -> 0x00db }
            boolean r4 = android.text.TextUtils.isEmpty(r0)     // Catch:{ IllegalArgumentException -> 0x0061, InvalidKeyException -> 0x0072, InvalidAlgorithmParameterException -> 0x0081, IllegalBlockSizeException -> 0x0090, BadPaddingException -> 0x009f, UnsupportedEncodingException -> 0x00ae, NoSuchAlgorithmException -> 0x00bd, NoSuchPaddingException -> 0x00cc, Exception -> 0x00db }
            if (r4 != 0) goto L_0x0034
            boolean r4 = android.text.TextUtils.isEmpty(r3)     // Catch:{ IllegalArgumentException -> 0x0061, InvalidKeyException -> 0x0072, InvalidAlgorithmParameterException -> 0x0081, IllegalBlockSizeException -> 0x0090, BadPaddingException -> 0x009f, UnsupportedEncodingException -> 0x00ae, NoSuchAlgorithmException -> 0x00bd, NoSuchPaddingException -> 0x00cc, Exception -> 0x00db }
            if (r4 == 0) goto L_0x0044
        L_0x0034:
            boolean r0 = com.huawei.hms.support.api.push.a.b.c()     // Catch:{ IllegalArgumentException -> 0x0061, InvalidKeyException -> 0x0072, InvalidAlgorithmParameterException -> 0x0081, IllegalBlockSizeException -> 0x0090, BadPaddingException -> 0x009f, UnsupportedEncodingException -> 0x00ae, NoSuchAlgorithmException -> 0x00bd, NoSuchPaddingException -> 0x00cc, Exception -> 0x00db }
            if (r0 == 0) goto L_0x0041
            java.lang.String r0 = "AES128_CBC"
            java.lang.String r1 = "ivParameter or encrypedWord is null"
            com.huawei.hms.support.api.push.a.b.b(r0, r1)     // Catch:{ IllegalArgumentException -> 0x0061, InvalidKeyException -> 0x0072, InvalidAlgorithmParameterException -> 0x0081, IllegalBlockSizeException -> 0x0090, BadPaddingException -> 0x009f, UnsupportedEncodingException -> 0x00ae, NoSuchAlgorithmException -> 0x00bd, NoSuchPaddingException -> 0x00cc, Exception -> 0x00db }
        L_0x0041:
            java.lang.String r0 = ""
            goto L_0x0008
        L_0x0044:
            r4 = 2
            javax.crypto.spec.IvParameterSpec r5 = new javax.crypto.spec.IvParameterSpec     // Catch:{ IllegalArgumentException -> 0x0061, InvalidKeyException -> 0x0072, InvalidAlgorithmParameterException -> 0x0081, IllegalBlockSizeException -> 0x0090, BadPaddingException -> 0x009f, UnsupportedEncodingException -> 0x00ae, NoSuchAlgorithmException -> 0x00bd, NoSuchPaddingException -> 0x00cc, Exception -> 0x00db }
            byte[] r0 = com.huawei.hms.support.api.push.a.a.a.b(r0)     // Catch:{ IllegalArgumentException -> 0x0061, InvalidKeyException -> 0x0072, InvalidAlgorithmParameterException -> 0x0081, IllegalBlockSizeException -> 0x0090, BadPaddingException -> 0x009f, UnsupportedEncodingException -> 0x00ae, NoSuchAlgorithmException -> 0x00bd, NoSuchPaddingException -> 0x00cc, Exception -> 0x00db }
            r5.<init>(r0)     // Catch:{ IllegalArgumentException -> 0x0061, InvalidKeyException -> 0x0072, InvalidAlgorithmParameterException -> 0x0081, IllegalBlockSizeException -> 0x0090, BadPaddingException -> 0x009f, UnsupportedEncodingException -> 0x00ae, NoSuchAlgorithmException -> 0x00bd, NoSuchPaddingException -> 0x00cc, Exception -> 0x00db }
            r2.init(r4, r1, r5)     // Catch:{ IllegalArgumentException -> 0x0061, InvalidKeyException -> 0x0072, InvalidAlgorithmParameterException -> 0x0081, IllegalBlockSizeException -> 0x0090, BadPaddingException -> 0x009f, UnsupportedEncodingException -> 0x00ae, NoSuchAlgorithmException -> 0x00bd, NoSuchPaddingException -> 0x00cc, Exception -> 0x00db }
            java.lang.String r0 = new java.lang.String     // Catch:{ IllegalArgumentException -> 0x0061, InvalidKeyException -> 0x0072, InvalidAlgorithmParameterException -> 0x0081, IllegalBlockSizeException -> 0x0090, BadPaddingException -> 0x009f, UnsupportedEncodingException -> 0x00ae, NoSuchAlgorithmException -> 0x00bd, NoSuchPaddingException -> 0x00cc, Exception -> 0x00db }
            byte[] r1 = com.huawei.hms.support.api.push.a.a.a.b(r3)     // Catch:{ IllegalArgumentException -> 0x0061, InvalidKeyException -> 0x0072, InvalidAlgorithmParameterException -> 0x0081, IllegalBlockSizeException -> 0x0090, BadPaddingException -> 0x009f, UnsupportedEncodingException -> 0x00ae, NoSuchAlgorithmException -> 0x00bd, NoSuchPaddingException -> 0x00cc, Exception -> 0x00db }
            byte[] r1 = r2.doFinal(r1)     // Catch:{ IllegalArgumentException -> 0x0061, InvalidKeyException -> 0x0072, InvalidAlgorithmParameterException -> 0x0081, IllegalBlockSizeException -> 0x0090, BadPaddingException -> 0x009f, UnsupportedEncodingException -> 0x00ae, NoSuchAlgorithmException -> 0x00bd, NoSuchPaddingException -> 0x00cc, Exception -> 0x00db }
            java.lang.String r2 = "UTF-8"
            r0.<init>(r1, r2)     // Catch:{ IllegalArgumentException -> 0x0061, InvalidKeyException -> 0x0072, InvalidAlgorithmParameterException -> 0x0081, IllegalBlockSizeException -> 0x0090, BadPaddingException -> 0x009f, UnsupportedEncodingException -> 0x00ae, NoSuchAlgorithmException -> 0x00bd, NoSuchPaddingException -> 0x00cc, Exception -> 0x00db }
            goto L_0x0008
        L_0x0061:
            r0 = move-exception
            boolean r1 = com.huawei.hms.support.api.push.a.b.e()
            if (r1 == 0) goto L_0x006f
            java.lang.String r1 = "AES128_CBC"
            java.lang.String r2 = "aes cbc decrypter data error"
            com.huawei.hms.support.api.push.a.b.a(r1, r2, r0)
        L_0x006f:
            java.lang.String r0 = ""
            goto L_0x0008
        L_0x0072:
            r0 = move-exception
            boolean r1 = com.huawei.hms.support.api.push.a.b.e()
            if (r1 == 0) goto L_0x006f
            java.lang.String r1 = "AES128_CBC"
            java.lang.String r2 = "aes cbc decrypter data error"
            com.huawei.hms.support.api.push.a.b.a(r1, r2, r0)
            goto L_0x006f
        L_0x0081:
            r0 = move-exception
            boolean r1 = com.huawei.hms.support.api.push.a.b.e()
            if (r1 == 0) goto L_0x006f
            java.lang.String r1 = "AES128_CBC"
            java.lang.String r2 = "aes cbc decrypter data error"
            com.huawei.hms.support.api.push.a.b.a(r1, r2, r0)
            goto L_0x006f
        L_0x0090:
            r0 = move-exception
            boolean r1 = com.huawei.hms.support.api.push.a.b.e()
            if (r1 == 0) goto L_0x006f
            java.lang.String r1 = "AES128_CBC"
            java.lang.String r2 = "aes cbc decrypter data error"
            com.huawei.hms.support.api.push.a.b.a(r1, r2, r0)
            goto L_0x006f
        L_0x009f:
            r0 = move-exception
            boolean r1 = com.huawei.hms.support.api.push.a.b.e()
            if (r1 == 0) goto L_0x006f
            java.lang.String r1 = "AES128_CBC"
            java.lang.String r2 = "aes cbc decrypter data error"
            com.huawei.hms.support.api.push.a.b.a(r1, r2, r0)
            goto L_0x006f
        L_0x00ae:
            r0 = move-exception
            boolean r1 = com.huawei.hms.support.api.push.a.b.e()
            if (r1 == 0) goto L_0x006f
            java.lang.String r1 = "AES128_CBC"
            java.lang.String r2 = "aes cbc decrypter data error"
            com.huawei.hms.support.api.push.a.b.a(r1, r2, r0)
            goto L_0x006f
        L_0x00bd:
            r0 = move-exception
            boolean r1 = com.huawei.hms.support.api.push.a.b.e()
            if (r1 == 0) goto L_0x006f
            java.lang.String r1 = "AES128_CBC"
            java.lang.String r2 = "aes cbc decrypter data error"
            com.huawei.hms.support.api.push.a.b.a(r1, r2, r0)
            goto L_0x006f
        L_0x00cc:
            r0 = move-exception
            boolean r1 = com.huawei.hms.support.api.push.a.b.e()
            if (r1 == 0) goto L_0x006f
            java.lang.String r1 = "AES128_CBC"
            java.lang.String r2 = "aes cbc decrypter data error"
            com.huawei.hms.support.api.push.a.b.a(r1, r2, r0)
            goto L_0x006f
        L_0x00db:
            r0 = move-exception
            boolean r1 = com.huawei.hms.support.api.push.a.b.e()
            if (r1 == 0) goto L_0x006f
            java.lang.String r1 = "AES128_CBC"
            java.lang.String r2 = "aes cbc encrypter data error"
            com.huawei.hms.support.api.push.a.b.a(r1, r2, r0)
            goto L_0x006f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.huawei.hms.support.api.push.a.a.a.a.b(java.lang.String):java.lang.String");
    }

    private static String c(String str) {
        if (TextUtils.isEmpty(str)) {
            return "";
        }
        try {
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append(str.substring(6, 12));
            stringBuffer.append(str.substring(16, 26));
            stringBuffer.append(str.substring(32, 48));
            return stringBuffer.toString();
        } catch (Exception e) {
            if (b.e()) {
                b.d("AES128_CBC", "get iv error:" + e.getMessage());
            }
            return "";
        }
    }

    private static String d(String str) {
        if (TextUtils.isEmpty(str)) {
            return "";
        }
        try {
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append(str.substring(0, 6));
            stringBuffer.append(str.substring(12, 16));
            stringBuffer.append(str.substring(26, 32));
            stringBuffer.append(str.substring(48));
            return stringBuffer.toString();
        } catch (Exception e) {
            if (b.e()) {
                b.d("AES128_CBC", "get encrypt word error:" + e.getMessage());
            }
            return "";
        }
    }
}
