package com.huawei.hms.support.log;

import com.huawei.hms.support.log.d;

public abstract class c {

    /* renamed from: a  reason: collision with root package name */
    private static d f770a = null;

    public static void a(String str, LogLevel logLevel, String str2) {
        d.a(str + str2 + ".log");
        d.a(logLevel);
        f770a = new d.a(str2).a(true).a();
    }

    public static void a(String str, String str2) {
        if (e()) {
            f770a.a(str, str2);
        }
    }

    public static void a(String str, String str2, Throwable th) {
        if (e()) {
            f770a.a(str, str2, th);
        }
    }

    public static boolean a() {
        return e() && f770a.c(LogLevel.DEBUG);
    }

    public static void b(String str, String str2) {
        if (e()) {
            f770a.b(str, str2);
        }
    }

    public static boolean b() {
        return e() && f770a.c(LogLevel.INFO);
    }

    public static void c(String str, String str2) {
        if (e()) {
            f770a.c(str, str2);
        }
    }

    public static boolean c() {
        return e() && f770a.c(LogLevel.WARN);
    }

    public static void d(String str, String str2) {
        if (e()) {
            f770a.d(str, str2);
        }
    }

    public static boolean d() {
        return e() && f770a.c(LogLevel.ERROR);
    }

    private static boolean e() {
        return f770a != null;
    }
}
