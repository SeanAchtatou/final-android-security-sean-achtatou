package com.huawei.hms.api.internal;

import android.os.Handler;
import android.os.Looper;
import com.huawei.hms.support.api.ResolveResult;
import com.huawei.hms.support.api.client.ResultCallback;
import com.huawei.hms.support.api.entity.core.DisconnectResp;

class c implements ResultCallback<ResolveResult<DisconnectResp>> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ b f737a;

    c(b bVar) {
        this.f737a = bVar;
    }

    /* renamed from: a */
    public void onResult(ResolveResult<DisconnectResp> resolveResult) {
        new Handler(Looper.getMainLooper()).post(new d(this, resolveResult));
    }
}
