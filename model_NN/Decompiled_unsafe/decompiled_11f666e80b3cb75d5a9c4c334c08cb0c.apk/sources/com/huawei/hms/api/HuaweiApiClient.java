package com.huawei.hms.api;

import android.content.Context;
import android.support.v4.util.ArrayMap;
import com.huawei.hms.a.a;
import com.huawei.hms.api.Api;
import com.huawei.hms.api.internal.b;
import com.huawei.hms.support.api.client.ApiClient;
import com.huawei.hms.support.api.entity.auth.PermissionInfo;
import com.huawei.hms.support.api.entity.auth.Scope;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public abstract class HuaweiApiClient implements ApiClient {

    public final class Builder {

        /* renamed from: a  reason: collision with root package name */
        private final Context f733a;
        private final List<Scope> b = new ArrayList();
        private final List<PermissionInfo> c = new ArrayList();
        private final Map<Api<?>, Api.ApiOptions> d = new ArrayMap();
        private OnConnectionFailedListener e;
        private ConnectionCallbacks f;

        public Builder(Context context) {
            a.a(context, "context must not be null.");
            this.f733a = context.getApplicationContext();
        }

        public Builder addApi(Api<? extends Api.ApiOptions.NotRequiredOptions> api) {
            this.d.put(api, null);
            return this;
        }

        public <O extends Api.ApiOptions.HasOptions> Builder addApi(Api<O> api, O o) {
            a.a(api, "Api must not be null");
            a.a(o, "Null options are not permitted for this Api");
            this.d.put(api, o);
            this.b.addAll(api.getOptions().getScopeList(o));
            this.c.addAll(api.getOptions().getPermissionInfoList(o));
            return this;
        }

        public Builder addConnectionCallbacks(ConnectionCallbacks connectionCallbacks) {
            a.a(connectionCallbacks, "listener must not be null.");
            this.f = connectionCallbacks;
            return this;
        }

        public Builder addOnConnectionFailedListener(OnConnectionFailedListener onConnectionFailedListener) {
            a.a(onConnectionFailedListener, "listener must not be null.");
            this.e = onConnectionFailedListener;
            return this;
        }

        public Builder addScope(Scope scope) {
            a.a(scope, "scope must not be null.");
            this.b.add(scope);
            return this;
        }

        public HuaweiApiClient build() {
            b bVar = new b(this.f733a);
            bVar.a(this.b);
            bVar.b(this.c);
            bVar.a(this.d);
            bVar.setConnectionCallbacks(this.f);
            bVar.setConnectionFailedListener(this.e);
            return bVar;
        }
    }

    public interface ConnectionCallbacks {
        public static final int CAUSE_NETWORK_LOST = 2;
        public static final int CAUSE_SERVICE_DISCONNECTED = 1;

        void onConnected();

        void onConnectionSuspended(int i);
    }

    public interface OnConnectionFailedListener {
        void onConnectionFailed(ConnectionResult connectionResult);
    }

    public abstract void connect();

    public abstract void disconnect();

    public abstract boolean isConnected();

    public abstract boolean isConnecting();

    public abstract void setConnectionCallbacks(ConnectionCallbacks connectionCallbacks);

    public abstract void setConnectionFailedListener(OnConnectionFailedListener onConnectionFailedListener);
}
