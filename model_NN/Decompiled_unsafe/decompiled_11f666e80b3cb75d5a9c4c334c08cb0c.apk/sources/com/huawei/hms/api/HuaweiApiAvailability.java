package com.huawei.hms.api;

import android.app.Activity;
import android.content.Context;
import com.huawei.hms.api.internal.a;

public abstract class HuaweiApiAvailability {
    public static final String SERVICES_PACKAGE = "com.huawei.hwid";
    public static final String SERVICES_SIGNATURE = "B92825C2BD5D6D6D1E7F39EECD17843B7D9016F611136B75441BC6F4D3F00F05";
    public static final int SERVICES_VERSION_CODE = 20400300;

    public interface OnUpdateListener {
        void onUpdateFailed(ConnectionResult connectionResult);
    }

    public static HuaweiApiAvailability getInstance() {
        return a.a();
    }

    public abstract int isHuaweiMobileServicesAvailable(Context context);

    public abstract boolean isUserResolvableError(int i);

    public abstract void resolveError(Activity activity, int i, int i2, OnUpdateListener onUpdateListener);
}
