package com.huawei.hms.core.aidl;

import android.os.Parcel;
import android.os.Parcelable;

final class b implements Parcelable.Creator<a> {
    b() {
    }

    /* renamed from: a */
    public a createFromParcel(Parcel parcel) {
        return new a(parcel, null);
    }

    /* renamed from: a */
    public a[] newArray(int i) {
        return new a[i];
    }
}
