package com.huawei.hms.core.aidl;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;

public class a implements Parcelable {
    public static final Parcelable.Creator<a> CREATOR = new b();

    /* renamed from: a  reason: collision with root package name */
    public String f743a;
    public Bundle b;
    private int c;
    private Bundle d;

    public a() {
        this.c = 1;
        this.b = null;
        this.d = null;
    }

    private a(Parcel parcel) {
        this.c = 1;
        this.b = null;
        this.d = null;
        a(parcel);
    }

    /* synthetic */ a(Parcel parcel, b bVar) {
        this(parcel);
    }

    public a(String str) {
        this.c = 1;
        this.b = null;
        this.d = null;
        this.f743a = str;
    }

    private static ClassLoader a(Class cls) {
        return cls == null ? ClassLoader.getSystemClassLoader() : cls.getClassLoader();
    }

    private void a(Parcel parcel) {
        this.c = parcel.readInt();
        this.f743a = parcel.readString();
        this.b = parcel.readBundle(a(Bundle.class));
        this.d = parcel.readBundle(a(Bundle.class));
    }

    public Bundle a() {
        return this.d;
    }

    public a a(Bundle bundle) {
        this.d = bundle;
        return this;
    }

    public int b() {
        return this.d == null ? 0 : 1;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.c);
        parcel.writeString(this.f743a);
        parcel.writeBundle(this.b);
        parcel.writeBundle(this.d);
    }
}
