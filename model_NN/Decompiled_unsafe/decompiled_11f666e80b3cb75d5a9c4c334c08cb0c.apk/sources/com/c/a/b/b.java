package com.c.a.b;

import android.os.SystemClock;
import com.c.a.b.a.c;
import com.c.a.b.a.d;
import com.c.a.b.a.e;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.UnknownHostException;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.ProtocolException;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpRequestRetryHandler;
import org.apache.http.client.RedirectHandler;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.impl.client.AbstractHttpClient;
import org.apache.http.protocol.HttpContext;

/* compiled from: HttpHandler */
public class b<T> extends com.c.a.c.a.a<Object, Object, Void> implements d {
    private static final a u = new a(null);
    private final AbstractHttpClient c;
    private final HttpContext d;
    private final e e = new e();
    private com.c.a.b.a.b f;
    private String g;
    private String h;
    private HttpRequestBase i;
    private boolean j = true;
    private c<T> k;
    private int l = 0;
    private String m = null;
    private boolean n = false;
    private boolean o = false;
    private boolean p = false;
    private String q;
    private C0017b r = C0017b.WAITING;
    private long s = a.a();
    private long t;

    public void a(com.c.a.b.a.b bVar) {
        if (bVar != null) {
            this.f = bVar;
        }
    }

    public b(AbstractHttpClient abstractHttpClient, HttpContext httpContext, String str, c<T> cVar) {
        this.c = abstractHttpClient;
        this.d = httpContext;
        this.k = cVar;
        this.q = str;
        this.c.setRedirectHandler(u);
    }

    public void a(long j2) {
        this.s = j2;
    }

    private d<T> a(HttpRequestBase httpRequestBase) throws com.c.a.a.b {
        boolean retryRequest;
        IOException e2;
        String a2;
        long j2;
        HttpRequestRetryHandler httpRequestRetryHandler = this.c.getHttpRequestRetryHandler();
        do {
            if (this.o && this.n) {
                File file = new File(this.m);
                if (!file.isFile() || !file.exists()) {
                    j2 = 0;
                } else {
                    j2 = file.length();
                }
                if (j2 > 0) {
                    httpRequestBase.setHeader("RANGE", "bytes=" + j2 + "-");
                }
            }
            try {
                this.h = httpRequestBase.getMethod();
                if (com.c.a.a.f630a.b(this.h) && (a2 = com.c.a.a.f630a.a(this.g)) != null) {
                    return new d<>(null, a2, true);
                }
                if (!c()) {
                    return a(this.c.execute(httpRequestBase, this.d));
                }
                return null;
            } catch (UnknownHostException e3) {
                e2 = e3;
                int i2 = this.l + 1;
                this.l = i2;
                retryRequest = httpRequestRetryHandler.retryRequest(e2, i2, this.d);
                continue;
            } catch (IOException e4) {
                e2 = e4;
                int i3 = this.l + 1;
                this.l = i3;
                retryRequest = httpRequestRetryHandler.retryRequest(e2, i3, this.d);
                continue;
            } catch (NullPointerException e5) {
                IOException iOException = new IOException(e5.getMessage());
                iOException.initCause(e5);
                int i4 = this.l + 1;
                this.l = i4;
                IOException iOException2 = iOException;
                retryRequest = httpRequestRetryHandler.retryRequest(iOException, i4, this.d);
                e2 = iOException2;
                continue;
            } catch (com.c.a.a.b e6) {
                throw e6;
            } catch (Throwable th) {
                IOException iOException3 = new IOException(th.getMessage());
                iOException3.initCause(th);
                int i5 = this.l + 1;
                this.l = i5;
                IOException iOException4 = iOException3;
                retryRequest = httpRequestRetryHandler.retryRequest(iOException3, i5, this.d);
                e2 = iOException4;
                continue;
            }
        } while (retryRequest);
        throw new com.c.a.a.b(e2);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Void c(Object... objArr) {
        if (!(this.r == C0017b.STOPPED || objArr == null || objArr.length == 0)) {
            if (objArr.length > 3) {
                this.m = String.valueOf(objArr[1]);
                this.n = this.m != null;
                this.o = ((Boolean) objArr[2]).booleanValue();
                this.p = ((Boolean) objArr[3]).booleanValue();
            }
            try {
                if (this.r != C0017b.STOPPED) {
                    this.i = (HttpRequestBase) objArr[0];
                    this.g = this.i.getURI().toString();
                    if (this.k != null) {
                        this.k.setRequestUrl(this.g);
                    }
                    d(1);
                    this.t = SystemClock.uptimeMillis();
                    d a2 = a(this.i);
                    if (a2 != null) {
                        d(4, a2);
                    }
                }
            } catch (com.c.a.a.b e2) {
                d(3, e2, e2.getMessage());
            }
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public void b(Object... objArr) {
        if (this.r != C0017b.STOPPED && objArr != null && objArr.length != 0 && this.k != null) {
            switch (((Integer) objArr[0]).intValue()) {
                case 1:
                    this.r = C0017b.STARTED;
                    this.k.onStart();
                    return;
                case 2:
                    if (objArr.length == 3) {
                        this.r = C0017b.LOADING;
                        this.k.onLoading(Long.valueOf(String.valueOf(objArr[1])).longValue(), Long.valueOf(String.valueOf(objArr[2])).longValue(), this.j);
                        return;
                    }
                    return;
                case 3:
                    if (objArr.length == 3) {
                        this.r = C0017b.FAILURE;
                        this.k.onFailure((com.c.a.a.b) objArr[1], (String) objArr[2]);
                        return;
                    }
                    return;
                case 4:
                    if (objArr.length == 2) {
                        this.r = C0017b.SUCCESS;
                        this.k.onSuccess((d) objArr[1]);
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    }

    private d<T> a(HttpResponse httpResponse) throws com.c.a.a.b, IOException {
        String str = null;
        if (httpResponse == null) {
            throw new com.c.a.a.b("response is null");
        } else if (c()) {
            return null;
        } else {
            StatusLine statusLine = httpResponse.getStatusLine();
            int statusCode = statusLine.getStatusCode();
            if (statusCode < 300) {
                HttpEntity entity = httpResponse.getEntity();
                if (entity != null) {
                    this.j = false;
                    if (this.n) {
                        this.o = this.o && com.c.a.c.d.a(httpResponse);
                        if (this.p) {
                            com.c.a.c.d.b(httpResponse);
                        }
                    } else {
                        str = this.e.a(entity, this, this.q);
                        if (com.c.a.a.f630a.b(this.h)) {
                            com.c.a.a.f630a.a(this.g, str, this.s);
                        }
                    }
                }
                return new d<>(httpResponse, str, false);
            } else if (statusCode == 301 || statusCode == 302) {
                if (this.f == null) {
                    this.f = new com.c.a.b.a.a();
                }
                HttpRequestBase a2 = this.f.a(httpResponse);
                if (a2 != null) {
                    return a(a2);
                }
                return null;
            } else if (statusCode == 416) {
                throw new com.c.a.a.b(statusCode, "maybe the file has downloaded completely");
            } else {
                throw new com.c.a.a.b(statusCode, statusLine.getReasonPhrase());
            }
        }
    }

    public boolean a(long j2, long j3, boolean z) {
        if (!(this.k == null || this.r == C0017b.STOPPED)) {
            if (z) {
                d(2, Long.valueOf(j2), Long.valueOf(j3));
            } else {
                long uptimeMillis = SystemClock.uptimeMillis();
                if (uptimeMillis - this.t >= ((long) this.k.getRate())) {
                    this.t = uptimeMillis;
                    d(2, Long.valueOf(j2), Long.valueOf(j3));
                }
            }
        }
        if (this.r != C0017b.STOPPED) {
            return true;
        }
        return false;
    }

    /* renamed from: com.c.a.b.b$b  reason: collision with other inner class name */
    /* compiled from: HttpHandler */
    public enum C0017b {
        WAITING(0),
        STARTED(1),
        LOADING(2),
        FAILURE(3),
        STOPPED(4),
        SUCCESS(5);
        
        private int g = 0;

        private C0017b(int i) {
            this.g = i;
        }
    }

    /* compiled from: HttpHandler */
    private static final class a implements RedirectHandler {
        private a() {
        }

        /* synthetic */ a(a aVar) {
            this();
        }

        public boolean isRedirectRequested(HttpResponse httpResponse, HttpContext httpContext) {
            return false;
        }

        public URI getLocationURI(HttpResponse httpResponse, HttpContext httpContext) throws ProtocolException {
            return null;
        }
    }
}
