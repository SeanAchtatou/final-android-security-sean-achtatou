package com.c.a.b.b;

import com.c.a.b.a.d;
import com.c.a.b.c;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.util.List;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpEntityEnclosingRequest;
import org.apache.http.NameValuePair;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.utils.CloneUtils;
import org.apache.mina.proxy.handlers.http.HttpProxyConstants;

/* compiled from: HttpRequest */
public class b extends HttpRequestBase implements HttpEntityEnclosingRequest {

    /* renamed from: a  reason: collision with root package name */
    private HttpEntity f641a;
    private a b;
    private com.c.a.b.b.c.a c;
    private Charset d;

    public b(a aVar, String str) {
        this.b = aVar;
        a(str);
    }

    public b a(List<NameValuePair> list) {
        if (list != null) {
            for (NameValuePair next : list) {
                this.c.a(next.getName(), next.getValue());
            }
        }
        return this;
    }

    public void a(c cVar, d dVar) {
        if (cVar != null) {
            if (this.d == null) {
                this.d = Charset.forName(cVar.a());
            }
            List<c.a> d2 = cVar.d();
            if (d2 != null) {
                for (c.a next : d2) {
                    if (next.f656a) {
                        setHeader(next.b);
                    } else {
                        addHeader(next.b);
                    }
                }
            }
            a(cVar.c());
            com.c.a.b.b.a.b b2 = cVar.b();
            if (b2 != null) {
                if (b2 instanceof com.c.a.b.b.a.b) {
                    b2.a(dVar);
                }
                setEntity(b2);
            }
        }
    }

    public URI getURI() {
        try {
            if (this.d == null) {
                this.d = com.c.a.c.d.a((HttpRequestBase) this);
            }
            if (this.d == null) {
                this.d = Charset.forName("UTF-8");
            }
            return this.c.a(this.d);
        } catch (URISyntaxException e) {
            com.c.a.c.c.a(e.getMessage(), e);
            return null;
        }
    }

    public void setURI(URI uri) {
        this.c = new com.c.a.b.b.c.a(uri);
    }

    public void a(String str) {
        this.c = new com.c.a.b.b.c.a(str);
    }

    public String getMethod() {
        return this.b.toString();
    }

    public HttpEntity getEntity() {
        return this.f641a;
    }

    public void setEntity(HttpEntity httpEntity) {
        this.f641a = httpEntity;
    }

    public boolean expectContinue() {
        Header firstHeader = getFirstHeader("Expect");
        return firstHeader != null && "100-continue".equalsIgnoreCase(firstHeader.getValue());
    }

    public Object clone() throws CloneNotSupportedException {
        b bVar = (b) b.super.clone();
        if (this.f641a != null) {
            bVar.f641a = (HttpEntity) CloneUtils.clone(this.f641a);
        }
        return bVar;
    }

    /* compiled from: HttpRequest */
    public enum a {
        GET("GET"),
        POST("POST"),
        PUT(HttpProxyConstants.PUT),
        HEAD("HEAD"),
        MOVE("MOVE"),
        COPY("COPY"),
        DELETE("DELETE"),
        OPTIONS("OPTIONS"),
        TRACE("TRACE"),
        CONNECT(HttpProxyConstants.CONNECT);
        
        private final String k;

        private a(String str) {
            this.k = str;
        }

        public String toString() {
            return this.k;
        }
    }
}
