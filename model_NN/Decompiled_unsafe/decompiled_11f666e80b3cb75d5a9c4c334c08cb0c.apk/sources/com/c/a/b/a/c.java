package com.c.a.b.a;

import com.c.a.a.b;
import com.c.a.b.d;

/* compiled from: RequestCallBack */
public abstract class c<T> {
    private static final int DEFAULT_RATE = 1000;
    private static final int MIN_RATE = 200;
    private int rate;
    private String requestUrl;
    protected Object userTag;

    public abstract void onFailure(b bVar, String str);

    public abstract void onSuccess(d<T> dVar);

    public c() {
        this.rate = 1000;
    }

    public c(int i) {
        this.rate = i;
    }

    public c(Object obj) {
        this.rate = 1000;
        this.userTag = obj;
    }

    public c(int i, Object obj) {
        this.rate = i;
        this.userTag = obj;
    }

    public final int getRate() {
        if (this.rate < 200) {
            return 200;
        }
        return this.rate;
    }

    public final void setRate(int i) {
        this.rate = i;
    }

    public Object getUserTag() {
        return this.userTag;
    }

    public void setUserTag(Object obj) {
        this.userTag = obj;
    }

    public final String getRequestUrl() {
        return this.requestUrl;
    }

    public final void setRequestUrl(String str) {
        this.requestUrl = str;
    }

    public void onStart() {
    }

    public void onStopped() {
    }

    public void onLoading(long j, long j2, boolean z) {
    }
}
