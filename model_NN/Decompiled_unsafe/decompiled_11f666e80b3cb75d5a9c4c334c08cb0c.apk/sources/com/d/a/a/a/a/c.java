package com.d.a.a.a.a;

import com.d.a.b.a;
import java.io.File;

/* compiled from: TotalSizeLimitedDiscCache */
public class c extends com.d.a.a.a.c {
    public c(File file, int i) {
        this(file, a.a(), i);
    }

    public c(File file, com.d.a.a.a.b.a aVar, int i) {
        super(file, aVar, i);
        if (i < 2097152) {
            com.d.a.c.c.c("You set too small disc cache size (less than %1$d Mb)", 2);
        }
    }

    /* access modifiers changed from: protected */
    public int a(File file) {
        return (int) file.length();
    }
}
