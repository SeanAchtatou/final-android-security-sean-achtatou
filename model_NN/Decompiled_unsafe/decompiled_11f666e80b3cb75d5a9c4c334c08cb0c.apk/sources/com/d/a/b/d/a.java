package com.d.a.b.d;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import com.d.a.b.d.b;
import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/* compiled from: BaseImageDownloader */
public class a implements b {

    /* renamed from: a  reason: collision with root package name */
    protected final Context f703a;
    protected final int b = 5000;
    protected final int c = 20000;

    public a(Context context) {
        this.f703a = context.getApplicationContext();
    }

    public InputStream a(String str, Object obj) throws IOException {
        switch (b.a.a(str)) {
            case HTTP:
            case HTTPS:
                return b(str, obj);
            case FILE:
                return d(str, obj);
            case CONTENT:
                return e(str, obj);
            case ASSETS:
                return f(str, obj);
            case DRAWABLE:
                return g(str, obj);
            default:
                return h(str, obj);
        }
    }

    /* access modifiers changed from: protected */
    public InputStream b(String str, Object obj) throws IOException {
        HttpURLConnection c2 = c(str, obj);
        int i = 0;
        while (c2.getResponseCode() / 100 == 3 && i < 5) {
            c2 = c(c2.getHeaderField("Location"), obj);
            i++;
        }
        return new com.d.a.b.a.a(new BufferedInputStream(c2.getInputStream(), 32768), (long) c2.getContentLength());
    }

    /* access modifiers changed from: protected */
    public HttpURLConnection c(String str, Object obj) throws IOException {
        HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(Uri.encode(str, "@#&=*+-_.,:!?()/~'%")).openConnection();
        httpURLConnection.setConnectTimeout(this.b);
        httpURLConnection.setReadTimeout(this.c);
        return httpURLConnection;
    }

    /* access modifiers changed from: protected */
    public InputStream d(String str, Object obj) throws IOException {
        String c2 = b.a.FILE.c(str);
        return new com.d.a.b.a.a(new BufferedInputStream(new FileInputStream(c2), 32768), new File(c2).length());
    }

    /* access modifiers changed from: protected */
    public InputStream e(String str, Object obj) throws FileNotFoundException {
        return this.f703a.getContentResolver().openInputStream(Uri.parse(str));
    }

    /* access modifiers changed from: protected */
    public InputStream f(String str, Object obj) throws IOException {
        return this.f703a.getAssets().open(b.a.ASSETS.c(str));
    }

    /* access modifiers changed from: protected */
    public InputStream g(String str, Object obj) {
        Bitmap bitmap = ((BitmapDrawable) this.f703a.getResources().getDrawable(Integer.parseInt(b.a.DRAWABLE.c(str)))).getBitmap();
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 0, byteArrayOutputStream);
        return new ByteArrayInputStream(byteArrayOutputStream.toByteArray());
    }

    /* access modifiers changed from: protected */
    public InputStream h(String str, Object obj) throws IOException {
        throw new UnsupportedOperationException(String.format("UIL doesn't support scheme(protocol) by default [%s]. You should implement this support yourself (BaseImageDownloader.getStreamFromOtherSource(...))", str));
    }
}
