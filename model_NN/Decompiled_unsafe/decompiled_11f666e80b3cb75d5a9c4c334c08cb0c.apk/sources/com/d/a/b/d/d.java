package com.d.a.b.d;

import com.d.a.b.d.b;
import java.io.IOException;
import java.io.InputStream;

/* compiled from: SlowNetworkImageDownloader */
public class d implements b {

    /* renamed from: a  reason: collision with root package name */
    private final b f708a;

    public d(b bVar) {
        this.f708a = bVar;
    }

    public InputStream a(String str, Object obj) throws IOException {
        InputStream a2 = this.f708a.a(str, obj);
        switch (b.a.a(str)) {
            case HTTP:
            case HTTPS:
                return new com.d.a.b.a.d(a2);
            default:
                return a2;
        }
    }
}
