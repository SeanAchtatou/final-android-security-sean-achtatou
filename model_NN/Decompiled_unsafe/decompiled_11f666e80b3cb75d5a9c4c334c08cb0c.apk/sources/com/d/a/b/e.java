package com.d.a.b;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.util.DisplayMetrics;
import com.d.a.a.b.b;
import com.d.a.b.a.h;
import com.d.a.b.a.j;
import com.d.a.b.a.l;
import com.d.a.b.d.c;
import com.d.a.b.d.d;
import java.util.concurrent.Executor;

/* compiled from: ImageLoaderConfiguration */
public final class e {

    /* renamed from: a  reason: collision with root package name */
    final Resources f710a;
    final int b;
    final int c;
    final int d;
    final int e;
    final Bitmap.CompressFormat f;
    final int g;
    final com.d.a.b.f.a h;
    final Executor i;
    final Executor j;
    final boolean k;
    final boolean l;
    final int m;
    final int n;
    final l o;
    final b<String, Bitmap> p;
    final com.d.a.a.a.b q;
    final com.d.a.b.d.b r;
    final com.d.a.b.b.b s;
    final c t;
    final boolean u;
    final com.d.a.a.a.b v;
    final com.d.a.b.d.b w;
    final com.d.a.b.d.b x;

    private e(a aVar) {
        this.f710a = aVar.b.getResources();
        this.b = aVar.c;
        this.c = aVar.d;
        this.d = aVar.e;
        this.e = aVar.f;
        this.f = aVar.g;
        this.g = aVar.h;
        this.h = aVar.i;
        this.i = aVar.j;
        this.j = aVar.k;
        this.m = aVar.n;
        this.n = aVar.o;
        this.o = aVar.q;
        this.q = aVar.v;
        this.p = aVar.u;
        this.t = aVar.z;
        this.u = aVar.A;
        this.r = aVar.x;
        this.s = aVar.y;
        this.k = aVar.l;
        this.l = aVar.m;
        this.w = new c(this.r);
        this.x = new d(this.r);
        this.v = a.a(com.d.a.c.d.a(aVar.b, false));
    }

    /* access modifiers changed from: package-private */
    public h a() {
        DisplayMetrics displayMetrics = this.f710a.getDisplayMetrics();
        int i2 = this.b;
        if (i2 <= 0) {
            i2 = displayMetrics.widthPixels;
        }
        int i3 = this.c;
        if (i3 <= 0) {
            i3 = displayMetrics.heightPixels;
        }
        return new h(i2, i3);
    }

    /* compiled from: ImageLoaderConfiguration */
    public static class a {

        /* renamed from: a  reason: collision with root package name */
        public static final l f711a = l.FIFO;
        /* access modifiers changed from: private */
        public boolean A = false;
        /* access modifiers changed from: private */
        public Context b;
        /* access modifiers changed from: private */
        public int c = 0;
        /* access modifiers changed from: private */
        public int d = 0;
        /* access modifiers changed from: private */
        public int e = 0;
        /* access modifiers changed from: private */
        public int f = 0;
        /* access modifiers changed from: private */
        public Bitmap.CompressFormat g = null;
        /* access modifiers changed from: private */
        public int h = 0;
        /* access modifiers changed from: private */
        public com.d.a.b.f.a i = null;
        /* access modifiers changed from: private */
        public Executor j = null;
        /* access modifiers changed from: private */
        public Executor k = null;
        /* access modifiers changed from: private */
        public boolean l = false;
        /* access modifiers changed from: private */
        public boolean m = false;
        /* access modifiers changed from: private */
        public int n = 3;
        /* access modifiers changed from: private */
        public int o = 4;
        private boolean p = false;
        /* access modifiers changed from: private */
        public l q = f711a;
        private int r = 0;
        private int s = 0;
        private int t = 0;
        /* access modifiers changed from: private */
        public b<String, Bitmap> u = null;
        /* access modifiers changed from: private */
        public com.d.a.a.a.b v = null;
        private com.d.a.a.a.b.a w = null;
        /* access modifiers changed from: private */
        public com.d.a.b.d.b x = null;
        /* access modifiers changed from: private */
        public com.d.a.b.b.b y;
        /* access modifiers changed from: private */
        public c z = null;

        public a(Context context) {
            this.b = context.getApplicationContext();
        }

        public a a(int i2, int i3) {
            this.c = i2;
            this.d = i3;
            return this;
        }

        public a a(int i2, int i3, Bitmap.CompressFormat compressFormat, int i4, com.d.a.b.f.a aVar) {
            this.e = i2;
            this.f = i3;
            this.g = compressFormat;
            this.h = i4;
            this.i = aVar;
            return this;
        }

        public a a(int i2) {
            if (!(this.j == null && this.k == null)) {
                com.d.a.c.c.c("threadPoolSize(), threadPriority() and tasksProcessingOrder() calls can overlap taskExecutor() and taskExecutorForCachedImages() calls.", new Object[0]);
            }
            this.n = i2;
            return this;
        }

        public a b(int i2) {
            if (!(this.j == null && this.k == null)) {
                com.d.a.c.c.c("threadPoolSize(), threadPriority() and tasksProcessingOrder() calls can overlap taskExecutor() and taskExecutorForCachedImages() calls.", new Object[0]);
            }
            if (i2 < 1) {
                this.o = 1;
            } else if (i2 > 10) {
                this.o = 10;
            } else {
                this.o = i2;
            }
            return this;
        }

        public a a() {
            this.p = true;
            return this;
        }

        public a a(l lVar) {
            if (!(this.j == null && this.k == null)) {
                com.d.a.c.c.c("threadPoolSize(), threadPriority() and tasksProcessingOrder() calls can overlap taskExecutor() and taskExecutorForCachedImages() calls.", new Object[0]);
            }
            this.q = lVar;
            return this;
        }

        public a a(b<String, Bitmap> bVar) {
            if (this.r != 0) {
                com.d.a.c.c.c("memoryCache() and memoryCacheSize() calls overlap each other", new Object[0]);
            }
            this.u = bVar;
            return this;
        }

        public a a(com.d.a.a.a.b.a aVar) {
            if (this.v != null) {
                com.d.a.c.c.c("discCache() and discCacheFileNameGenerator() calls overlap each other", new Object[0]);
            }
            this.w = aVar;
            return this;
        }

        public a a(com.d.a.a.a.b bVar) {
            if (this.s > 0 || this.t > 0) {
                com.d.a.c.c.c("discCache(), discCacheSize() and discCacheFileCount calls overlap each other", new Object[0]);
            }
            if (this.w != null) {
                com.d.a.c.c.c("discCache() and discCacheFileNameGenerator() calls overlap each other", new Object[0]);
            }
            this.v = bVar;
            return this;
        }

        public a a(c cVar) {
            this.z = cVar;
            return this;
        }

        public a b() {
            this.A = true;
            return this;
        }

        public e c() {
            d();
            return new e(this);
        }

        private void d() {
            if (this.j == null) {
                this.j = a.a(this.n, this.o, this.q);
            } else {
                this.l = true;
            }
            if (this.k == null) {
                this.k = a.a(this.n, this.o, this.q);
            } else {
                this.m = true;
            }
            if (this.v == null) {
                if (this.w == null) {
                    this.w = a.a();
                }
                this.v = a.a(this.b, this.w, this.s, this.t);
            }
            if (this.u == null) {
                this.u = a.a(this.r);
            }
            if (this.p) {
                this.u = new com.d.a.a.b.a.a(this.u, j.a());
            }
            if (this.x == null) {
                this.x = a.a(this.b);
            }
            if (this.y == null) {
                this.y = a.a(this.A);
            }
            if (this.z == null) {
                this.z = c.t();
            }
        }
    }
}
