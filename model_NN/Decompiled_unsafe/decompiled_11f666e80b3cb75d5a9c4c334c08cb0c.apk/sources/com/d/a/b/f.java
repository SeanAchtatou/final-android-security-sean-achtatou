package com.d.a.b;

import com.d.a.b.e.a;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.ReentrantLock;

/* compiled from: ImageLoaderEngine */
class f {

    /* renamed from: a  reason: collision with root package name */
    final e f714a;
    /* access modifiers changed from: private */
    public Executor b;
    /* access modifiers changed from: private */
    public Executor c;
    private ExecutorService d;
    private final Map<Integer, String> e = Collections.synchronizedMap(new HashMap());
    private final Map<String, ReentrantLock> f = new WeakHashMap();
    private final AtomicBoolean g = new AtomicBoolean(false);
    private final AtomicBoolean h = new AtomicBoolean(false);
    private final AtomicBoolean i = new AtomicBoolean(false);
    private final Object j = new Object();

    f(e eVar) {
        this.f714a = eVar;
        this.b = eVar.i;
        this.c = eVar.j;
        this.d = Executors.newCachedThreadPool();
    }

    /* access modifiers changed from: package-private */
    public void a(final h hVar) {
        this.d.execute(new Runnable() {
            public void run() {
                boolean exists = f.this.f714a.q.a(hVar.a()).exists();
                f.this.g();
                if (exists) {
                    f.this.c.execute(hVar);
                } else {
                    f.this.b.execute(hVar);
                }
            }
        });
    }

    /* access modifiers changed from: package-private */
    public void a(i iVar) {
        g();
        this.c.execute(iVar);
    }

    /* access modifiers changed from: private */
    public void g() {
        if (!this.f714a.k && ((ExecutorService) this.b).isShutdown()) {
            this.b = h();
        }
        if (!this.f714a.l && ((ExecutorService) this.c).isShutdown()) {
            this.c = h();
        }
    }

    private Executor h() {
        return a.a(this.f714a.m, this.f714a.n, this.f714a.o);
    }

    /* access modifiers changed from: package-private */
    public String a(a aVar) {
        return this.e.get(Integer.valueOf(aVar.f()));
    }

    /* access modifiers changed from: package-private */
    public void a(a aVar, String str) {
        this.e.put(Integer.valueOf(aVar.f()), str);
    }

    /* access modifiers changed from: package-private */
    public void b(a aVar) {
        this.e.remove(Integer.valueOf(aVar.f()));
    }

    /* access modifiers changed from: package-private */
    public void a() {
        this.g.set(true);
    }

    /* access modifiers changed from: package-private */
    public void b() {
        this.g.set(false);
        synchronized (this.j) {
            this.j.notifyAll();
        }
    }

    /* access modifiers changed from: package-private */
    public ReentrantLock a(String str) {
        ReentrantLock reentrantLock = this.f.get(str);
        if (reentrantLock != null) {
            return reentrantLock;
        }
        ReentrantLock reentrantLock2 = new ReentrantLock();
        this.f.put(str, reentrantLock2);
        return reentrantLock2;
    }

    /* access modifiers changed from: package-private */
    public AtomicBoolean c() {
        return this.g;
    }

    /* access modifiers changed from: package-private */
    public Object d() {
        return this.j;
    }

    /* access modifiers changed from: package-private */
    public boolean e() {
        return this.h.get();
    }

    /* access modifiers changed from: package-private */
    public boolean f() {
        return this.i.get();
    }
}
