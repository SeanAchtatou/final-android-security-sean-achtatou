package com.d.a.b;

import android.graphics.Bitmap;
import android.os.Handler;
import com.d.a.c.c;

/* compiled from: ProcessAndDisplayImageTask */
class i implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final f f722a;
    private final Bitmap b;
    private final g c;
    private final Handler d;

    public i(f fVar, Bitmap bitmap, g gVar, Handler handler) {
        this.f722a = fVar;
        this.b = bitmap;
        this.c = gVar;
        this.d = handler;
    }

    public void run() {
        if (this.f722a.f714a.u) {
            c.a("PostProcess image before displaying [%s]", this.c.b);
        }
        b bVar = new b(this.c.e.p().a(this.b), this.c, this.f722a, com.d.a.b.a.i.MEMORY_CACHE);
        bVar.a(this.f722a.f714a.u);
        if (this.c.e.s()) {
            bVar.run();
        } else {
            this.d.post(bVar);
        }
    }
}
