package com.d.a.b.c;

import android.graphics.Bitmap;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.DecelerateInterpolator;
import com.d.a.b.a.i;
import com.d.a.b.e.a;

/* compiled from: FadeInBitmapDisplayer */
public class b implements a {

    /* renamed from: a  reason: collision with root package name */
    private final int f699a;
    private final boolean b;
    private final boolean c;
    private final boolean d;

    public b(int i) {
        this(i, true, true, true);
    }

    public b(int i, boolean z, boolean z2, boolean z3) {
        this.f699a = i;
        this.b = z;
        this.c = z2;
        this.d = z3;
    }

    public void display(Bitmap bitmap, a aVar, i iVar) {
        aVar.a(bitmap);
        if ((this.b && iVar == i.NETWORK) || ((this.c && iVar == i.DISC_CACHE) || (this.d && iVar == i.MEMORY_CACHE))) {
            a(aVar.d(), this.f699a);
        }
    }

    public static void a(View view, int i) {
        if (view != null) {
            AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
            alphaAnimation.setDuration((long) i);
            alphaAnimation.setInterpolator(new DecelerateInterpolator());
            view.startAnimation(alphaAnimation);
        }
    }
}
