package com.cmsc.cmmusic.init;

import com.cmsc.cmmusic.common.data.Result;

public class SmsLoginInfoRsp extends Result {
    private String mobile;

    public String getMobile() {
        return this.mobile;
    }

    public void setMobile(String str) {
        this.mobile = str;
    }

    public String toString() {
        return "SmsLoginInfoRsp [mobile=" + this.mobile + ", getResCode()=" + getResCode() + ", getResMsg()=" + getResMsg() + "]";
    }
}
