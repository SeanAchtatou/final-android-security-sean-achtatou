package com.cmsc.cmmusic.common;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.cmsc.cmmusic.common.data.BizInfo;
import com.cmsc.cmmusic.common.data.OrderPolicy;
import com.cmsc.cmmusic.common.data.OrderResult;

final class RingbackOpenView extends OrderView {
    private static /* synthetic */ int[] $SWITCH_TABLE$com$cmsc$cmmusic$common$data$OrderPolicy$OrderType = null;
    private static final String LOG_TAG = "RingbackOpenView";
    private TextView txtRingbackTip;

    static /* synthetic */ int[] $SWITCH_TABLE$com$cmsc$cmmusic$common$data$OrderPolicy$OrderType() {
        int[] iArr = $SWITCH_TABLE$com$cmsc$cmmusic$common$data$OrderPolicy$OrderType;
        if (iArr == null) {
            iArr = new int[OrderPolicy.OrderType.values().length];
            try {
                iArr[OrderPolicy.OrderType.net.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[OrderPolicy.OrderType.sms.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[OrderPolicy.OrderType.verifyCode.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            $SWITCH_TABLE$com$cmsc$cmmusic$common$data$OrderPolicy$OrderType = iArr;
        }
        return iArr;
    }

    public RingbackOpenView(Context context, Bundle bundle) {
        super(context, bundle);
    }

    /* access modifiers changed from: protected */
    public void initContentView(LinearLayout linearLayout) {
        linearLayout.setPadding(0, 50, 0, 0);
        this.txtRingbackTip = new TextView(this.mCurActivity);
        this.txtRingbackTip.setTextAppearance(this.mCurActivity, 16973892);
        linearLayout.addView(this.txtRingbackTip);
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* access modifiers changed from: protected */
    public void sureClicked() {
        try {
            switch ($SWITCH_TABLE$com$cmsc$cmmusic$common$data$OrderPolicy$OrderType()[this.orderType.ordinal()]) {
                case 1:
                    BizInfo bizInfo = this.policyObj.getBizInfos().get(0);
                    EnablerInterface.openRingback(this.mCurActivity, bizInfo.getBizCode(), bizInfo.getSalePrice(), new CMMusicCallback<OrderResult>() {
                        public void operationResult(OrderResult orderResult) {
                            RingbackOpenView.this.mCurActivity.closeActivity(orderResult);
                        }
                    });
                    return;
                case 2:
                default:
                    return;
                case 3:
                    Log.d(LOG_TAG, "Get download url by sign code");
                    return;
            }
        } catch (Exception e) {
            e.printStackTrace();
            this.mCurActivity.showToast("开通失败");
        }
        e.printStackTrace();
        this.mCurActivity.showToast("开通失败");
    }

    /* access modifiers changed from: protected */
    public void updateNetView() {
        String str = "";
        if (!(this.policyObj == null || this.policyObj.getBizInfos() == null)) {
            str = this.policyObj.getBizInfos().get(0).getSalePrice();
        }
        Logger.i("TAG", "price = " + str);
        this.baseView.setVisibility(8);
        setUserTip("点击”确认“将开通彩铃功能\n");
        this.txtRingbackTip.setText("开通彩铃功能价格： " + EnablerInterface.getPriceString(str));
    }
}
