package com.cmsc.cmmusic.common;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import com.cmsc.cmmusic.common.data.MVMonthPolicy;
import com.cmsc.cmmusic.common.data.MVOrderInfo;
import com.cmsc.cmmusic.common.data.MemberOpenPolicy;
import com.cmsc.cmmusic.common.data.OrderPolicy;
import com.cmsc.cmmusic.common.data.UserInfo;
import java.util.ArrayList;

final class OpenMvMonthView extends BaseView {
    private static final String LOG_TAG = "OpenMvMonthView";
    protected LinearLayout contentView;
    protected LinearLayout memInfoView;
    private TextView memLevelTxt;
    private Button openMemButton;
    private Button openSpecMemButton;
    /* access modifiers changed from: private */
    public MVMonthPolicy policy = null;
    private RadioGroup rdGroup;
    private TextView specMemInfoTxt;
    protected LinearLayout specMemInfoView;
    private TextView txtUserTip;

    public OpenMvMonthView(Context context, Bundle bundle, MVMonthPolicy mVMonthPolicy) {
        super(context, bundle);
        this.policy = mVMonthPolicy;
        initContentView(context);
        setVisibility(0);
        updateNetView();
    }

    public MVMonthPolicy getPolicy() {
        return this.policy;
    }

    public void setPolicy(MVMonthPolicy mVMonthPolicy) {
        this.policy = mVMonthPolicy;
    }

    /* access modifiers changed from: protected */
    public void sureClicked() {
        Log.d(LOG_TAG, "sure button clicked");
        this.mCurActivity.showProgressBar("请稍候...");
        MiguSdkUtil.buildOrderParam(this.mCurActivity, "http://218.200.227.123:95/sdkServer/1.0/mv/open", EnablerInterface.buildRequsetXml("<serviceId>" + ((String) this.rdGroup.getChildAt(this.rdGroup.getCheckedRadioButtonId()).getTag()) + "</serviceId>"));
    }

    public void updateNetView() {
        Log.i("TAG", "policy = " + this.policy);
        if (this.policy.isOpen()) {
            this.txtUserTip.setText("尊敬的手机用户\n" + this.policy.getMobile() + ", 您还有未开通的MV包月点播功能，请问您是否需要开通?");
        } else {
            this.txtUserTip.setText("尊敬的手机用户\n" + this.policy.getMobile() + ", 您尚未开通任何一个MV包月点播功能，请问您是否需要开通？");
        }
        updateMemInfoView();
        UserInfo userInfo = this.policy.getUserInfo();
        String str = null;
        if (userInfo != null) {
            str = userInfo.getMemLevel();
        }
        if (!"3".equals(str)) {
            this.contentView.addView(getSpecMemInfoView());
            updateSpecMemInfoView("推荐：开通咪咕特级会员专享MV包月开通7折优惠。");
        } else if (this.specMemInfoView != null) {
            this.specMemInfoView.setVisibility(8);
            this.contentView.removeView(this.specMemInfoView);
        }
    }

    private void initContentView(Context context) {
        this.contentView = new LinearLayout(context);
        this.contentView.setOrientation(1);
        this.contentView.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        this.txtUserTip = new TextView(context);
        this.txtUserTip.setTextAppearance(context, 16973892);
        this.txtUserTip.setPadding(10, 10, 10, 10);
        this.contentView.addView(this.txtUserTip);
        this.rdGroup = new RadioGroup(this.mCurActivity);
        ArrayList<MVOrderInfo> avalibleMvInfo = this.policy.getAvalibleMvInfo();
        if (avalibleMvInfo != null) {
            for (int i = 0; i < avalibleMvInfo.size(); i++) {
                MVOrderInfo mVOrderInfo = avalibleMvInfo.get(i);
                RadioButton radioButton = new RadioButton(this.mCurActivity);
                radioButton.setText(mVOrderInfo.getName());
                radioButton.setId(i);
                radioButton.setTag(mVOrderInfo.getServiceId());
                if (i == 0) {
                    radioButton.setChecked(true);
                }
                this.rdGroup.addView(radioButton);
            }
        }
        this.contentView.addView(this.rdGroup);
        this.contentView.addView(getMemInfoView());
        this.contentView.setVisibility(0);
        this.rootView.addView(this.contentView);
    }

    /* access modifiers changed from: protected */
    public LinearLayout getMemInfoView() {
        this.memInfoView = new LinearLayout(this.mCurActivity);
        this.memInfoView.setOrientation(0);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -2);
        layoutParams.setMargins(10, 10, 10, 10);
        this.memInfoView.setLayoutParams(layoutParams);
        this.memInfoView.setVisibility(8);
        this.openMemButton = new Button(this.mCurActivity);
        this.openMemButton.setHeight(30);
        this.openMemButton.setLayoutParams(new LinearLayout.LayoutParams(-2, -2));
        this.openMemButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                OpenMvMonthView.this.mCurActivity.showProgressBar("请稍候...");
                new Thread() {
                    public void run() {
                        try {
                            final MemberOpenPolicy queryMemberOpenPolicyByNet = EnablerInterface.queryMemberOpenPolicyByNet(OpenMvMonthView.this.mCurActivity);
                            if (queryMemberOpenPolicyByNet == null || !"000000".equals(queryMemberOpenPolicyByNet.getResCode())) {
                                OpenMvMonthView.this.mCurActivity.showToast("请求失败");
                                return;
                            }
                            OpenMvMonthView.this.mHandler.post(new Runnable() {
                                public void run() {
                                    OpenMemberView openMemberView = new OpenMemberView(OpenMvMonthView.this.mCurActivity, new Bundle());
                                    OpenMvMonthView.this.mCurActivity.setContentView(openMemberView);
                                    OpenMvMonthView.this.policyObj.setClubUserInfos(queryMemberOpenPolicyByNet.getClubUserInfos());
                                    openMemberView.updateView(OpenMvMonthView.this.policyObj);
                                }
                            });
                            OpenMvMonthView.this.mCurActivity.hideProgressBar();
                        } catch (Exception e) {
                            e.printStackTrace();
                            OpenMvMonthView.this.mCurActivity.showToast("请求失败");
                        } finally {
                            OpenMvMonthView.this.mCurActivity.hideProgressBar();
                        }
                    }
                }.start();
            }
        });
        this.openMemButton.setVisibility(8);
        this.memLevelTxt = new TextView(this.mCurActivity);
        this.memLevelTxt.setTextAppearance(this.mCurActivity, 16973892);
        this.memInfoView.addView(this.memLevelTxt);
        this.memInfoView.addView(this.openMemButton);
        return this.memInfoView;
    }

    /* access modifiers changed from: protected */
    public LinearLayout getSpecMemInfoView() {
        this.specMemInfoView = new LinearLayout(this.mCurActivity);
        this.specMemInfoView.setOrientation(1);
        this.specMemInfoView.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        this.specMemInfoView.setVisibility(8);
        this.openSpecMemButton = new Button(this.mCurActivity);
        this.openSpecMemButton.setLayoutParams(new LinearLayout.LayoutParams(-2, -2));
        this.openSpecMemButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                OpenMvMonthView.this.mCurActivity.showProgressBar("请稍候...");
                new Thread() {
                    public void run() {
                        try {
                            OpenMvMonthView.this.mHandler.post(new Runnable() {
                                public void run() {
                                    OpenMemberView openMemberView = new OpenMemberView(OpenMvMonthView.this.mCurActivity, new Bundle());
                                    OpenMvMonthView.this.mCurActivity.setContentView(openMemberView);
                                    OrderPolicy orderPolicy = new OrderPolicy();
                                    orderPolicy.setResCode(OpenMvMonthView.this.policy.getResCode());
                                    orderPolicy.setResMsg(OpenMvMonthView.this.policy.getResMsg());
                                    orderPolicy.setMobile(OpenMvMonthView.this.policy.getMobile());
                                    orderPolicy.setmVOrderInfos(OpenMvMonthView.this.policy.getmVOrderInfos());
                                    orderPolicy.setUserInfo(OpenMvMonthView.this.policy.getUserInfo());
                                    orderPolicy.setOrderType(OrderPolicy.OrderType.net);
                                    openMemberView.updateView(orderPolicy);
                                }
                            });
                        } catch (Exception e) {
                            e.printStackTrace();
                            OpenMvMonthView.this.mCurActivity.showToast("请求失败");
                        } finally {
                            OpenMvMonthView.this.mCurActivity.hideProgressBar();
                        }
                    }
                }.start();
            }
        });
        this.openSpecMemButton.setText("了解咪咕特级会员");
        this.openSpecMemButton.setVisibility(8);
        this.specMemInfoTxt = new TextView(this.mCurActivity);
        this.specMemInfoTxt.setTextAppearance(this.mCurActivity, 16973892);
        this.specMemInfoView.addView(this.specMemInfoTxt);
        this.specMemInfoView.addView(this.openSpecMemButton);
        return this.specMemInfoView;
    }

    /* access modifiers changed from: package-private */
    public void updateSpecMemInfoView(String str) {
        if (this.specMemInfoView != null) {
            this.openSpecMemButton.setVisibility(0);
            this.specMemInfoTxt.setText(str);
            this.specMemInfoView.setVisibility(0);
        }
    }

    /* access modifiers changed from: protected */
    public void updateMemInfoView() {
        String str;
        UserInfo userInfo = this.policy.getUserInfo();
        if (userInfo != null && this.memInfoView != null) {
            String memLevel = userInfo.getMemLevel();
            if ("1".equals(memLevel)) {
                str = "普通会员";
                this.openMemButton.setText("开通特级会员");
            } else if ("2".equals(memLevel)) {
                str = "高级会员";
                this.openMemButton.setText("开通特级会员");
            } else if ("3".equals(memLevel)) {
                str = "特级会员";
                this.openMemButton.setVisibility(8);
            } else {
                str = "非会员";
                this.openMemButton.setText("开通特级会员");
            }
            this.memLevelTxt.setText("会员级别:  " + str + "   ");
            this.memInfoView.setVisibility(0);
        }
    }

    /* access modifiers changed from: package-private */
    public void updateView(OrderPolicy orderPolicy) {
    }
}
