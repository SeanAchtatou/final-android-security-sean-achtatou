package com.cmsc.cmmusic.common.data;

public class DownloadRsp extends Result {
    private String downUrl;
    private String downloadKey;

    public String getDownUrl() {
        return this.downUrl;
    }

    public void setDownUrl(String str) {
        this.downUrl = str;
    }

    public String getDownloadKey() {
        return this.downloadKey;
    }

    public void setDownloadKey(String str) {
        this.downloadKey = str;
    }

    public String toString() {
        return "DownloadRsp [downUrl=" + this.downUrl + ", downloadKey=" + this.downloadKey + ", getResCode()=" + getResCode() + ", getResMsg()=" + getResMsg() + "]";
    }
}
