package com.cmsc.cmmusic.common.data;

import java.util.ArrayList;

public class PaymentResult extends Result {
    private ArrayList<PaymentInfo> paymentInfoList;

    public ArrayList<PaymentInfo> getPaymentInfoList() {
        return this.paymentInfoList;
    }

    public void setPaymentInfoList(ArrayList<PaymentInfo> arrayList) {
        this.paymentInfoList = arrayList;
    }

    public String toString() {
        return "PaymentResult [paymentInfoList=" + this.paymentInfoList + ", getResCode()=" + getResCode() + ", getResMsg()=" + getResMsg() + "]";
    }
}
