package com.cmsc.cmmusic.common.data;

public class OwnRingRsp extends Result {
    private String crbtId;
    private String listenUrl;

    public String getCrbtId() {
        return this.crbtId;
    }

    public void setCrbtId(String str) {
        this.crbtId = str;
    }

    public String getListenUrl() {
        return this.listenUrl;
    }

    public void setListenUrl(String str) {
        this.listenUrl = str;
    }

    public String toString() {
        return "OwnRingRsp [crbtId=" + this.crbtId + ", listenUrl=" + this.listenUrl + "]";
    }
}
