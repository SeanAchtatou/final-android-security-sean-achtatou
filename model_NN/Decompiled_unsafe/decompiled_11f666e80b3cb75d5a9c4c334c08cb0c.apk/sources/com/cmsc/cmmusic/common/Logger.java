package com.cmsc.cmmusic.common;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

public class Logger {
    public static int DEBUG = 4;
    public static int ERROR = 1;
    public static int INFO = 3;
    public static final String LOG = "LOG";
    public static final int LOG_LEVEL = 5;
    public static int VERBOSE = 5;
    public static int WARN = 2;

    public static void v(String str, String str2) {
        if (5 > VERBOSE) {
            Log.v(str, str2);
        }
    }

    public static void d(String str, String str2) {
        if (5 > DEBUG) {
            Log.d(str, str2);
        }
    }

    public static void i(String str, String str2) {
        if (5 > INFO) {
            Log.i(str, str2);
        }
    }

    public static void w(String str, String str2) {
        if (5 > WARN) {
            Log.w(str, str2);
        }
    }

    public static void e(String str, String str2) {
        if (5 > ERROR) {
            Log.e(str, str2);
        }
    }

    public static void log2Preferences(Context context, String str) {
        SharedPreferences.Editor edit = PreferenceUtil.getPreferences(context).edit();
        edit.putString(LOG, String.valueOf(getPreferencesLog(context)) + "                                    " + str);
        edit.commit();
    }

    public static String getPreferencesLog(Context context) {
        return PreferenceUtil.getPreferences(context).getString(LOG, "");
    }

    public static void cleanPreferencesLog(Context context) {
        SharedPreferences.Editor edit = PreferenceUtil.getPreferences(context).edit();
        edit.remove(LOG);
        edit.commit();
    }
}
