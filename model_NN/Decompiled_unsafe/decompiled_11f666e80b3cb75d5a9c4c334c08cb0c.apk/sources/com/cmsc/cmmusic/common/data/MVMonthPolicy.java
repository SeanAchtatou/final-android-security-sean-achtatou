package com.cmsc.cmmusic.common.data;

import java.util.ArrayList;
import java.util.Iterator;

public class MVMonthPolicy extends Result {
    private ArrayList<MVOrderInfo> mVOrderInfos;
    private String mobile;
    private OrderType orderType;
    private UserInfo userInfo;

    public enum OrderType {
        net,
        sms,
        verifyCode
    }

    public OrderType getOrderType() {
        return this.orderType;
    }

    public void setOrderType(OrderType orderType2) {
        this.orderType = orderType2;
    }

    public String getMobile() {
        return this.mobile;
    }

    public void setMobile(String str) {
        this.mobile = str;
    }

    public ArrayList<MVOrderInfo> getmVOrderInfos() {
        return this.mVOrderInfos;
    }

    public void setmVOrderInfos(ArrayList<MVOrderInfo> arrayList) {
        this.mVOrderInfos = arrayList;
    }

    public UserInfo getUserInfo() {
        return this.userInfo;
    }

    public void setUserInfo(UserInfo userInfo2) {
        this.userInfo = userInfo2;
    }

    public String toString() {
        return "MVMonthPolicy [mobile=" + this.mobile + ", mVOrderInfos=" + this.mVOrderInfos + ", userInfo=" + this.userInfo + ", getResCode()=" + getResCode() + ", getResMsg()=" + getResMsg() + "]";
    }

    public boolean isAllOpen() {
        if (this.mVOrderInfos == null) {
            return false;
        }
        Iterator<MVOrderInfo> it = this.mVOrderInfos.iterator();
        while (it.hasNext()) {
            if (it.next().getType().equals("2")) {
                return false;
            }
        }
        return true;
    }

    public boolean isOpen() {
        if (this.mVOrderInfos == null) {
            return false;
        }
        Iterator<MVOrderInfo> it = this.mVOrderInfos.iterator();
        while (it.hasNext()) {
            if (it.next().getType().equals("1")) {
                return true;
            }
        }
        return false;
    }

    public ArrayList<MVOrderInfo> getAvalibleMvInfo() {
        ArrayList<MVOrderInfo> arrayList = new ArrayList<>();
        if (this.mVOrderInfos != null) {
            Iterator<MVOrderInfo> it = this.mVOrderInfos.iterator();
            while (it.hasNext()) {
                MVOrderInfo next = it.next();
                if (next.getType().equals("2")) {
                    arrayList.add(next);
                }
            }
        }
        return arrayList;
    }
}
