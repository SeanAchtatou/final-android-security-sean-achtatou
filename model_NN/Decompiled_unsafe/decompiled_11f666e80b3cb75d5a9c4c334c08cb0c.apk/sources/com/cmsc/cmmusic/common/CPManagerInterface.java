package com.cmsc.cmmusic.common;

import android.content.Context;
import android.os.Bundle;
import com.cmsc.cmmusic.common.MiguSdkUtil;
import com.cmsc.cmmusic.common.data.OrderResult;
import com.cmsc.cmmusic.common.data.QueryResult;
import com.cmsc.cmmusic.common.data.Result;
import com.sina.weibo.sdk.register.mobile.SelectCountryActivity;
import java.io.IOException;
import java.io.InputStream;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

public class CPManagerInterface {
    public static void queryCPVibrateRingTimeDownloadUrl(Context context, String str, CMMusicCallback<OrderResult> cMMusicCallback) {
        Bundle bundle = new Bundle();
        bundle.putString("MusicId", str);
        CMMusicActivity.showActivityDefault(context, bundle, 15, cMMusicCallback);
    }

    public static void getCPVibrateRingTimeDownloadUrl(Context context, String str, String str2, String str3, CMMusicCallback<OrderResult> cMMusicCallback) {
        MiguSdkUtil.TL.init();
        EnablerInterface.getCPVibrateRingTimeDownloadUrl(context, str, str2, str3, cMMusicCallback);
    }

    public static void getCPFullSongTimeDownloadUrl(Context context, String str, CMMusicCallback<OrderResult> cMMusicCallback) {
        Bundle bundle = new Bundle();
        bundle.putString("MusicId", str);
        CMMusicActivity.showActivityDefault(context, bundle, 14, cMMusicCallback);
    }

    public static void getCpFullSongTimeDownloadUrl(Context context, String str, String str2, String str3, CMMusicCallback<OrderResult> cMMusicCallback) {
        MiguSdkUtil.TL.init();
        EnablerInterface.getCPFullSongTimeDownloadUrl(context, str, str2, str3, cMMusicCallback);
    }

    public static void openCPMonth(Context context, String str, String str2, CMMusicCallback<OrderResult> cMMusicCallback) {
        Bundle bundle = new Bundle();
        bundle.putString("serviceId", str);
        bundle.putString("definedseq", str2);
        CMMusicActivity.showActivityDefault(context, bundle, 10, cMMusicCallback);
    }

    public static void openCpMonth(Context context, String str, String str2, String str3, CMMusicCallback<OrderResult> cMMusicCallback) {
        MiguSdkUtil.TL.init();
        EnablerInterface.openCPMonth(context, str, str2, str3, cMMusicCallback);
    }

    public static Result cancelCPMonth(Context context, String str) {
        try {
            return EnablerInterface.getResult(HttpPostCore.httpConnection(context, "http://218.200.227.123:95/sdkServer/1.0/cp/cancel", EnablerInterface.buildRequsetXml("<serviceId>" + str + "</serviceId>")));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String queryCPMonthPolicy(Context context, String str) {
        try {
            return HttpPostCore.httpConnectionToString(context, "http://218.200.227.123:95/sdkServer/1.0/mon/bizInfo", EnablerInterface.buildRequsetXml("<serviceId>" + str + "</serviceId><type>5</type>"));
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static QueryResult queryCPMonth(Context context, String str) {
        try {
            return queryCPMonth(HttpPostCore.httpConnection(context, "http://218.200.227.123:95/sdkServer/1.0/cp/query", EnablerInterface.buildRequsetXml("<serviceId>" + str + "</serviceId>")));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void queryCPVibrateRingDownloadUrl(Context context, String str, String str2, String str3, CMMusicCallback<OrderResult> cMMusicCallback) {
        MiguSdkUtil.order(context, MiguSdkUtil.buildOrderParam(context, "http://218.200.227.123:95/sdkServer/1.0/cp/rdownlink", EnablerInterface.buildRequsetXml("<serviceId>" + str + "</serviceId><musicId>" + str2 + "</musicId><codeRate>" + str3 + "</codeRate>")), cMMusicCallback);
    }

    public static void queryCPFullSongDownloadUrl(Context context, String str, String str2, String str3, CMMusicCallback<OrderResult> cMMusicCallback) {
        MiguSdkUtil.order(context, MiguSdkUtil.buildOrderParam(context, "http://218.200.227.123:95/sdkServer/1.0/cp/sdownlink", EnablerInterface.buildRequsetXml("<serviceId>" + str + "</serviceId><musicId>" + str2 + "</musicId><codeRate>" + str3 + "</codeRate>")), cMMusicCallback);
    }

    private static QueryResult queryCPMonth(InputStream inputStream) throws XmlPullParserException, IOException {
        if (inputStream == null) {
            return null;
        }
        QueryResult queryResult = new QueryResult();
        try {
            XmlPullParser newPullParser = XmlPullParserFactory.newInstance().newPullParser();
            newPullParser.setInput(inputStream, "UTF-8");
            for (int eventType = newPullParser.getEventType(); 1 != eventType; eventType = newPullParser.next()) {
                String name = newPullParser.getName();
                switch (eventType) {
                    case 2:
                        if (!name.equalsIgnoreCase("resCode")) {
                            if (!name.equalsIgnoreCase("resMsg")) {
                                if (!name.equalsIgnoreCase("mobile")) {
                                    if (!name.equalsIgnoreCase(SelectCountryActivity.EXTRA_COUNTRY_NAME)) {
                                        break;
                                    } else {
                                        queryResult.setName(newPullParser.nextText());
                                        break;
                                    }
                                } else {
                                    queryResult.setMobile(newPullParser.nextText());
                                    break;
                                }
                            } else {
                                queryResult.setResMsg(newPullParser.nextText());
                                break;
                            }
                        } else {
                            queryResult.setResCode(newPullParser.nextText());
                            break;
                        }
                }
            }
            if (inputStream == null) {
                return queryResult;
            }
            try {
                return queryResult;
            } catch (IOException e) {
                return queryResult;
            }
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e2) {
                }
            }
        }
    }
}
