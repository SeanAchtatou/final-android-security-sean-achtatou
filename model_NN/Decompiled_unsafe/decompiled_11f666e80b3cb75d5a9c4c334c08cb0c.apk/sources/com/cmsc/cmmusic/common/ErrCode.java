package com.cmsc.cmmusic.common;

public class ErrCode {
    static final String FAIL_NO_NET_CODE = "-10000";
    static final String FAIL_NO_NET_MSG = "没有连接网络";
    static final String FAIL_NO_PHONE_CODE = "-20000";
    static final String FAIL_NO_PHONE_MSG = "无法获取用户手机号";
}
