package com.cmsc.cmmusic.common;

import android.content.Context;
import android.os.Bundle;
import com.cmsc.cmmusic.common.data.MVMonthPolicy;
import com.cmsc.cmmusic.common.data.OrderResult;
import com.cmsc.cmmusic.common.data.Result;

class MvManagerInterface {
    MvManagerInterface() {
    }

    public static void openMvMonth(Context context, String[] strArr, CMMusicCallback<Result> cMMusicCallback) {
        Bundle bundle = new Bundle();
        bundle.putStringArray("serviceIds", strArr);
        CMMusicActivity.showActivityDefault(context, bundle, 17, cMMusicCallback);
    }

    public static void mvDownload(Context context, String str, CMMusicCallback<OrderResult> cMMusicCallback) {
        Bundle bundle = new Bundle();
        bundle.putString("mvId", str);
        CMMusicActivity.showActivityDefault(context, bundle, 18, cMMusicCallback);
    }

    public static Result cancelMvMonth(Context context, String str) {
        try {
            return EnablerInterface.getResult(HttpPostCore.httpConnection(context, "http://218.200.227.123:95/sdkServer/1.0/mv/cancel", EnablerInterface.buildRequsetXml("<serviceId>" + str + "</serviceId>")));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static MVMonthPolicy getMvMonthQuery(Context context, String[] strArr) {
        if (strArr == null) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        int length = strArr.length;
        for (int i = 0; i < length; i++) {
            sb.append("<serviceId>" + strArr[i] + "</serviceId>");
        }
        try {
            return EnablerInterface.getMvMonthPolicy(HttpPostCore.httpConnection(context, "http://218.200.227.123:95/sdkServer/1.0/mv/query", EnablerInterface.buildRequsetXml(sb.toString())));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
