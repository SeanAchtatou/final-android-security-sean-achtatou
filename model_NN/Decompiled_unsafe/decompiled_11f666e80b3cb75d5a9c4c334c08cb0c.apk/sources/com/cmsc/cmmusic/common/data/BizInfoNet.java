package com.cmsc.cmmusic.common.data;

public class BizInfoNet {
    private String bizCode;
    private String bizType;
    private String desc;
    private String offReason;
    private String originalPrice;
    private String resource;
    private String salePrice;

    public String getBizCode() {
        return this.bizCode;
    }

    public void setBizCode(String str) {
        this.bizCode = str;
    }

    public String getBizType() {
        return this.bizType;
    }

    public void setBizType(String str) {
        this.bizType = str;
    }

    public String getOriginalPrice() {
        return this.originalPrice;
    }

    public void setOriginalPrice(String str) {
        this.originalPrice = str;
    }

    public String getSalePrice() {
        return this.salePrice;
    }

    public void setSalePrice(String str) {
        this.salePrice = str;
    }

    public String getDesc() {
        return this.desc;
    }

    public void setDesc(String str) {
        this.desc = str;
    }

    public String getOffReason() {
        return this.offReason;
    }

    public void setOffReason(String str) {
        this.offReason = str;
    }

    public String getResource() {
        return this.resource;
    }

    public void setResource(String str) {
        this.resource = str;
    }

    public String toString() {
        return "BizInfoNet [bizCode=" + this.bizCode + ", bizType=" + this.bizType + ", originalPrice=" + this.originalPrice + ", salePrice=" + this.salePrice + ", desc=" + this.desc + ", offReason=" + this.offReason + ", resource=" + this.resource + "]";
    }
}
