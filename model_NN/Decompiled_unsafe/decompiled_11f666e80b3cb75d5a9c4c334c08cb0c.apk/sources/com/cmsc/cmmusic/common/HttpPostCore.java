package com.cmsc.cmmusic.common;

import android.content.Context;
import android.util.Log;
import com.cmsc.cmmusic.init.GetAppInfo;
import com.cmsc.cmmusic.init.GetAppInfoInterface;
import com.cmsc.cmmusic.init.PreferenceUtil;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import org.apache.mina.proxy.handlers.http.HttpProxyConstants;

final class HttpPostCore {
    private static final String TAG = "HttpPostCore";

    HttpPostCore() {
    }

    static String httpConnectionToString(Context context, String str, String str2) throws IOException {
        InputStream httpConnection = httpConnection(context, str, str2);
        if (httpConnection == null) {
            return null;
        }
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        byte[] bArr = new byte[1024];
        while (true) {
            int read = httpConnection.read(bArr, 0, 1024);
            if (read == -1) {
                return new String(byteArrayOutputStream.toByteArray(), "UTF-8");
            }
            byteArrayOutputStream.write(bArr, 0, read);
        }
    }

    static InputStream httpConnection(Context context, String str, String str2) throws IOException {
        return httpConnection(context, str, str2, null);
    }

    static InputStream httpConnection(Context context, String str, String str2, String str3) throws IOException {
        byte[] bytes = str2.getBytes("UTF-8");
        HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(str).openConnection();
        httpURLConnection.setDoOutput(true);
        httpURLConnection.setDoInput(true);
        httpURLConnection.setConnectTimeout(30000);
        httpURLConnection.setReadTimeout(30000);
        httpURLConnection.setUseCaches(false);
        httpURLConnection.setRequestMethod("POST");
        if (str3 != null && str3.length() > 0) {
            httpURLConnection.addRequestProperty("Authorization", str3);
        } else if (GetAppInfoInterface.isTokenExist(context)) {
            setTokenAuthorization(context, httpURLConnection);
        } else if (GetAppInfoInterface.isImsiExist(context)) {
            setIMSIAuthorization(context, httpURLConnection);
        } else {
            setAuthorization(context, httpURLConnection);
        }
        httpURLConnection.setRequestProperty("Content-length", new StringBuilder().append(bytes.length).toString());
        httpURLConnection.setRequestProperty("Accept", "*/*");
        httpURLConnection.setRequestProperty("Content-Type", "*/*");
        httpURLConnection.setRequestProperty("Connection", "Keep-Alive");
        httpURLConnection.setRequestProperty("Charset", "UTF-8");
        OutputStream outputStream = httpURLConnection.getOutputStream();
        outputStream.write(bytes);
        outputStream.flush();
        outputStream.close();
        int responseCode = httpURLConnection.getResponseCode();
        Log.i("httpConnection", "responseCode-----" + responseCode);
        if (200 == responseCode) {
            return httpURLConnection.getInputStream();
        }
        return null;
    }

    static InputStream httpConnection(Context context, String str, File file) throws IOException {
        HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(str).openConnection();
        httpURLConnection.setDoOutput(true);
        httpURLConnection.setDoInput(true);
        httpURLConnection.setConnectTimeout(100000);
        httpURLConnection.setReadTimeout(100000);
        httpURLConnection.setUseCaches(false);
        httpURLConnection.setRequestMethod("POST");
        httpURLConnection.setRequestProperty("Connection", "Keep-Alive");
        if (GetAppInfoInterface.isTokenExist(context)) {
            setTokenAuthorization(context, httpURLConnection);
        } else if (GetAppInfoInterface.isImsiExist(context)) {
            setIMSIAuthorization(context, httpURLConnection);
        } else {
            setAuthorization(context, httpURLConnection);
        }
        httpURLConnection.setRequestProperty("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)");
        httpURLConnection.setRequestProperty("Charsert", "UTF-8");
        httpURLConnection.setRequestProperty("Content-Type", "multipart/form-data; boundary=" + "---------7d4a6d158c9");
        new DataOutputStream(httpURLConnection.getOutputStream());
        Log.d("Authorization", httpURLConnection.getRequestProperty("Authorization"));
        Log.i("httpConnection", "output before");
        DataOutputStream dataOutputStream = new DataOutputStream(httpURLConnection.getOutputStream());
        StringBuilder sb = new StringBuilder();
        sb.append("--");
        sb.append("---------7d4a6d158c9");
        sb.append(HttpProxyConstants.CRLF);
        sb.append("Content-Disposition: form-data;name=\"file\";filename=\"" + file.getName() + "\"\r\n");
        sb.append("Content-Type: application/octet-stream\r\n\r\n");
        dataOutputStream.write(sb.toString().getBytes());
        Log.d(TAG, new StringBuilder(String.valueOf(file.getName())).toString());
        FileInputStream fileInputStream = new FileInputStream(file);
        byte[] bArr = new byte[1024];
        while (true) {
            int read = fileInputStream.read(bArr);
            if (read == -1) {
                break;
            }
            dataOutputStream.write(bArr, 0, read);
            dataOutputStream.flush();
        }
        fileInputStream.close();
        dataOutputStream.write(("\r\n--" + "---------7d4a6d158c9" + "--\r\n").getBytes());
        dataOutputStream.flush();
        dataOutputStream.close();
        int responseCode = httpURLConnection.getResponseCode();
        Log.i("httpConnection", "responseCode-----" + responseCode);
        if (200 == responseCode) {
            return httpURLConnection.getInputStream();
        }
        return null;
    }

    private static void setIMSIAuthorization(Context context, HttpURLConnection httpURLConnection) {
        httpURLConnection.addRequestProperty("Authorization", MiguSdkUtil.getImsiAuthorization(context, ""));
    }

    private static void setAuthorization(Context context, HttpURLConnection httpURLConnection) {
        httpURLConnection.addRequestProperty("Authorization", MiguSdkUtil.getAuthorization(context, ""));
    }

    private static void setTokenAuthorization(Context context, HttpURLConnection httpURLConnection) {
        httpURLConnection.addRequestProperty("Authorization", MiguSdkUtil.getTokenAuthorization(context, ""));
    }

    public static InputStream httpConnectionGet(Context context, String str) throws IOException {
        HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(str).openConnection();
        httpURLConnection.setDoOutput(true);
        httpURLConnection.setDoInput(true);
        httpURLConnection.setConnectTimeout(100000);
        httpURLConnection.setReadTimeout(100000);
        httpURLConnection.setUseCaches(false);
        httpURLConnection.setRequestMethod("GET");
        if (GetAppInfoInterface.isTokenExist(context)) {
            setTokenAuthorization(context, httpURLConnection);
        } else if (GetAppInfoInterface.getIMSI(context).trim().length() != 0) {
            setIMSIAuthorization(context, httpURLConnection);
        } else {
            setAuthorization(context, httpURLConnection);
        }
        httpURLConnection.setRequestProperty("Accept", "*/*");
        httpURLConnection.setRequestProperty("Content-Type", "*/*");
        httpURLConnection.setRequestProperty("Connection", "Keep-Alive");
        httpURLConnection.setRequestProperty("Charset", "UTF-8");
        httpURLConnection.setRequestProperty("Token", PreferenceUtil.getToken(context));
        httpURLConnection.setRequestProperty("excode", GetAppInfo.getexCode(context));
        int responseCode = httpURLConnection.getResponseCode();
        Log.i("httpConnection", "responseCode-----" + responseCode);
        if (200 == responseCode) {
            return httpURLConnection.getInputStream();
        }
        return null;
    }
}
