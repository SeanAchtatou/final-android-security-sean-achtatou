package com.cmsc.cmmusic.common.data;

import java.util.List;

public class CrbtListRsp extends Result {
    private List<ToneInfo> toneInfos;

    public List<ToneInfo> getToneInfos() {
        return this.toneInfos;
    }

    public void setToneInfos(List<ToneInfo> list) {
        this.toneInfos = list;
    }

    public String toString() {
        return "CrbtListRsp [toneInfos=" + this.toneInfos + ", getResCode()=" + getResCode() + ", getResMsg()=" + getResMsg() + "]";
    }
}
