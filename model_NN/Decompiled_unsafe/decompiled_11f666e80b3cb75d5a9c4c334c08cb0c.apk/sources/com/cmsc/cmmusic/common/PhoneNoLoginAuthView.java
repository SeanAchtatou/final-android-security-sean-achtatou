package com.cmsc.cmmusic.common;

import android.content.Context;
import android.os.Bundle;
import android.text.Html;
import android.text.method.DigitsKeyListener;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.cmsc.cmmusic.common.data.OrderPolicy;
import com.cmsc.cmmusic.common.data.Result;
import com.cmsc.cmmusic.common.data.SmsLoginAuthResult;
import com.cmsc.cmmusic.init.PreferenceUtil;
import com.cmsc.cmmusic.init.Utils;

final class PhoneNoLoginAuthView extends BaseView {
    /* access modifiers changed from: private */
    public EditText edPhoneNum = new EditText(this.mCurActivity);
    private EditText edSignCode;

    public PhoneNoLoginAuthView(Context context, Bundle bundle) {
        super(context, bundle);
        setVisibility(0);
        TextView textView = new TextView(this.mCurActivity);
        textView.setTextAppearance(this.mCurActivity, 16973892);
        textView.setText("\n尊敬的用户，您正在使用中国移动提供的音乐服务，请使用短信验证码登录。");
        this.rootView.addView(textView);
        this.edPhoneNum.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        this.edPhoneNum.setHint("请输入您的手机号码");
        this.edPhoneNum.setInputType(3);
        this.edPhoneNum.setKeyListener(new DigitsKeyListener(false, false));
        this.rootView.addView(this.edPhoneNum);
        LinearLayout linearLayout = new LinearLayout(this.mCurActivity);
        linearLayout.setOrientation(0);
        linearLayout.setLayoutParams(new LinearLayout.LayoutParams(-1, -2, 1.0f));
        linearLayout.setGravity(16);
        this.edSignCode = new EditText(this.mCurActivity);
        this.edSignCode.setLayoutParams(new LinearLayout.LayoutParams(-1, -2, 0.5f));
        this.edSignCode.setHint("请输入短信验证码");
        this.edSignCode.setInputType(2);
        this.edSignCode.setKeyListener(new DigitsKeyListener(false, false));
        linearLayout.addView(this.edSignCode);
        Button button = new Button(this.mCurActivity);
        button.setLayoutParams(new LinearLayout.LayoutParams(-1, -2, 0.5f));
        button.setText("获取短信验证码");
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                final String editable = PhoneNoLoginAuthView.this.edPhoneNum.getText().toString();
                if (editable == null || !Utils.validatePhoneNumber(editable)) {
                    PhoneNoLoginAuthView.this.edPhoneNum.requestFocus();
                    PhoneNoLoginAuthView.this.edPhoneNum.setError(Html.fromHtml("<font color='red'>请正确输入手机号码</font>"));
                    return;
                }
                PhoneNoLoginAuthView.this.mCurActivity.showProgressBar("请稍候...");
                new Thread(new Runnable() {
                    public void run() {
                        try {
                            Result validateCode = EnablerInterface.getValidateCode(PhoneNoLoginAuthView.this.mCurActivity, editable);
                            if (validateCode == null || !"000000".equals(validateCode.getResCode())) {
                                PhoneNoLoginAuthView.this.mCurActivity.showToast("获取短信验证码失败，请重试");
                            } else {
                                PhoneNoLoginAuthView.this.mCurActivity.showToast("验证码短信已发出，请注意查收");
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            PhoneNoLoginAuthView.this.mCurActivity.showToast("获取短信验证码失败，请重试");
                        } finally {
                            PhoneNoLoginAuthView.this.mCurActivity.hideProgressBar();
                        }
                    }
                }).start();
            }
        });
        linearLayout.addView(button);
        this.rootView.addView(linearLayout);
    }

    /* access modifiers changed from: protected */
    public void sureClicked() {
        final String editable = this.edPhoneNum.getText().toString();
        final String editable2 = this.edSignCode.getText().toString();
        if (editable == null || !Utils.validatePhoneNumber(editable)) {
            this.edPhoneNum.requestFocus();
            this.edPhoneNum.setError(Html.fromHtml("<font color='red'>请正确输入手机号码</font>"));
        } else if (editable2 == null || editable2.length() == 0) {
            this.edSignCode.requestFocus();
            this.edSignCode.setError(Html.fromHtml("<font color='red'>请输入短信验证码</font>"));
        } else {
            this.mCurActivity.showProgressBar("请稍候...");
            new Thread(new Runnable() {
                public void run() {
                    try {
                        SmsLoginAuthResult smsLoginAuth = EnablerInterface.smsLoginAuth(PhoneNoLoginAuthView.this.mCurActivity, editable, editable2);
                        if (smsLoginAuth != null && "000000".equals(smsLoginAuth.getResCode())) {
                            PhoneNoLoginAuthView.this.mCurActivity.showToast("登录成功");
                            PreferenceUtil.saveToken(PhoneNoLoginAuthView.this.mCurActivity, smsLoginAuth.getToken());
                            if (PhoneNoLoginAuthView.this.mCurActivity.getViewStackSize() > 1) {
                                PhoneNoLoginAuthView.this.mHandler.post(new Runnable() {
                                    public void run() {
                                        PhoneNoLoginAuthView.this.mCurActivity.start();
                                    }
                                });
                            } else {
                                Result result = new Result();
                                result.setResCode(smsLoginAuth.getResCode());
                                result.setResMsg(smsLoginAuth.getResMsg());
                                PhoneNoLoginAuthView.this.mCurActivity.closeActivity(result);
                            }
                        } else if (smsLoginAuth != null) {
                            PhoneNoLoginAuthView.this.mCurActivity.showToast(smsLoginAuth.getResMsg());
                        } else {
                            PhoneNoLoginAuthView.this.mCurActivity.showToast("登录失败，请重试");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        PhoneNoLoginAuthView.this.mCurActivity.showToast("登录失败，请重试");
                    } finally {
                        PhoneNoLoginAuthView.this.mCurActivity.hideProgressBar();
                    }
                }
            }).start();
        }
    }

    /* access modifiers changed from: package-private */
    public void updateView(OrderPolicy orderPolicy) {
    }
}
