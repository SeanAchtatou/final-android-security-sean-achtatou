package com.cmsc.cmmusic.common.data;

import java.util.Map;

public class PaymentUserInfo {
    private String createTime;
    private Map<String, String> ext;
    private String identityID;
    private String updateTime;

    public String getIdentityID() {
        return this.identityID;
    }

    public void setIdentityID(String str) {
        this.identityID = str;
    }

    public String getCreateTime() {
        return this.createTime;
    }

    public void setCreateTime(String str) {
        this.createTime = str;
    }

    public String getUpdateTime() {
        return this.updateTime;
    }

    public void setUpdateTime(String str) {
        this.updateTime = str;
    }

    public Map<String, String> getExt() {
        return this.ext;
    }

    public void setExt(Map<String, String> map) {
        this.ext = map;
    }

    public String toString() {
        return "PaymentUserInfo [identityID=" + this.identityID + ", createTime=" + this.createTime + ", updateTime=" + this.updateTime + ", ext=" + this.ext + "]";
    }
}
