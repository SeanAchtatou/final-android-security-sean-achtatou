package com.cmsc.cmmusic.common;

import android.content.Context;
import com.cmsc.cmmusic.common.data.StreamRsp;
import java.io.IOException;
import java.io.InputStream;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

public class OnlineListenerMusicInterface {
    public static StreamRsp getStream(Context context, String str, String str2) {
        try {
            return getStream(HttpPostCore.httpConnection(context, "http://218.200.227.123:95/sdkServer/1.0/stream/query", EnablerInterface.buildRequsetXml("<musicId>" + str + "</musicId><codeRate>" + str2 + "</codeRate>")));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static StreamRsp getStream(InputStream inputStream) throws IOException, XmlPullParserException {
        if (inputStream == null) {
            return null;
        }
        StreamRsp streamRsp = new StreamRsp();
        try {
            XmlPullParser newPullParser = XmlPullParserFactory.newInstance().newPullParser();
            newPullParser.setInput(inputStream, "UTF-8");
            for (int eventType = newPullParser.getEventType(); 1 != eventType; eventType = newPullParser.next()) {
                String name = newPullParser.getName();
                switch (eventType) {
                    case 2:
                        if (!name.equalsIgnoreCase("resCode")) {
                            if (!name.equalsIgnoreCase("resMsg")) {
                                if (!name.equalsIgnoreCase("streamUrl")) {
                                    break;
                                } else {
                                    streamRsp.setStreamUrl(newPullParser.nextText());
                                    break;
                                }
                            } else {
                                streamRsp.setResMsg(newPullParser.nextText());
                                break;
                            }
                        } else {
                            streamRsp.setResCode(newPullParser.nextText());
                            break;
                        }
                }
            }
            if (inputStream == null) {
                return streamRsp;
            }
            try {
                return streamRsp;
            } catch (IOException e) {
                return streamRsp;
            }
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e2) {
                }
            }
        }
    }
}
