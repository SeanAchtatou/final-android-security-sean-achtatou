package com.cmsc.cmmusic.common;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.util.Xml;
import cn.banshenggua.aichang.utils.Constants;
import com.cmsc.cmmusic.common.data.AccountInfo;
import com.cmsc.cmmusic.common.data.AccountResult;
import com.cmsc.cmmusic.common.data.BizInfoNet;
import com.cmsc.cmmusic.common.data.DownloadRsp;
import com.cmsc.cmmusic.common.data.OrderResult;
import com.cmsc.cmmusic.common.data.PayPolicy;
import com.cmsc.cmmusic.common.data.PaymentInfo;
import com.cmsc.cmmusic.common.data.PaymentResult;
import com.cmsc.cmmusic.common.data.PaymentUserInfo;
import com.cmsc.cmmusic.common.data.RechargeResult;
import com.cmsc.cmmusic.common.data.RegistResult;
import com.cmsc.cmmusic.common.data.RegistRsp;
import com.cmsc.cmmusic.common.data.Result;
import com.cmsc.cmmusic.common.data.UserResult;
import com.tencent.open.SocialConstants;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;
import org.xmlpull.v1.XmlSerializer;

class PaymentManagerInterface {
    PaymentManagerInterface() {
    }

    public static void regist(Context context, boolean z, CMMusicCallback<RegistResult> cMMusicCallback) {
        Bundle bundle = new Bundle();
        bundle.putBoolean("PrioritySMS", z);
        CMMusicActivity.showActivityDefault(context, bundle, 11, cMMusicCallback);
    }

    public static void login(Context context, boolean z, CMMusicCallback<Result> cMMusicCallback) {
        Bundle bundle = new Bundle();
        bundle.putBoolean("PrioritySMS", z);
        CMMusicActivity.showActivityDefault(context, bundle, 12, cMMusicCallback);
    }

    public static void transfer(Context context, boolean z, String str, CMMusicCallback<Result> cMMusicCallback) {
        Bundle bundle = new Bundle();
        bundle.putString(Constants.UID, str);
        bundle.putBoolean("PrioritySMS", z);
        CMMusicActivity.showActivityDefault(context, bundle, 13, cMMusicCallback);
    }

    public static RegistRsp checkMember(Context context, String str) {
        try {
            return EnablerInterface.getRegistResult(HttpPostCore.httpConnection(context, "http://218.200.227.123:95/sdkServer/1.0/pay/member/check", EnablerInterface.buildRequsetXml("<accountName>" + str + "</accountName>")));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static UserResult queryMember(Context context, String str, String str2) {
        try {
            return getUserResult(HttpPostCore.httpConnection(context, "http://218.200.227.123:95/sdkServer/1.0/pay/member/query", EnablerInterface.buildRequsetXml("<UID>" + str + "</UID><accountName>" + str2 + "</accountName>")));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static UserResult updateMember(Context context, String str, Map<String, String> map) {
        try {
            return getUserResult(HttpPostCore.httpConnection(context, "http://218.200.227.123:95/sdkServer/1.0/pay/member/update", writeUpdateMemberRequest(str, map)));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static AccountResult queryAccount(Context context, String str, String str2) {
        try {
            return getAccountResult(HttpPostCore.httpConnection(context, "http://218.200.227.123:95/sdkServer/1.0/pay/account/query", EnablerInterface.buildRequsetXml("<UID>" + str + "</UID><type>" + str2 + "</type>")));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static PaymentResult getPaymentResult(Context context, String str, String str2, String str3, String str4, String str5) {
        try {
            return getPaymentResult(HttpPostCore.httpConnection(context, "http://218.200.227.123:95/sdkServer/1.0/pay/account/paymentInfo", EnablerInterface.buildRequsetXml("<UID>" + str + "</UID><type>" + str2 + "</type><tradeNum>" + str3 + "</tradeNum><startTime>" + str4 + "</startTime><endTime>" + str5 + "</endTime>")));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static PaymentResult getTransferResult(Context context, String str, String str2, String str3, String str4) {
        try {
            return getPaymentResult(HttpPostCore.httpConnection(context, "http://218.200.227.123:95/sdkServer/1.0/pay/account/transferInfo", EnablerInterface.buildRequsetXml("<UID>" + str + "</UID><type>" + str2 + "</type><startTime>" + str3 + "</startTime><endTime>" + str4 + "</endTime>")));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String getPayments(Context context) {
        try {
            return getPayments(HttpPostCore.httpConnection(context, "http://218.200.227.123:95/sdkServer/1.0/pay/account/bank", ""));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static RechargeResult getRechargeResult(Context context, String str, String str2, String str3) {
        try {
            return getRechargeResult(HttpPostCore.httpConnection(context, "http://218.200.227.123:95/sdkServer/1.0/pay/account/recharge", EnablerInterface.buildRequsetXml("<UID>" + str + "</UID><type>" + str2 + "</type><amount>" + str3 + "</amount>")));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static Result getOrderOpenResult(Context context, String str, String str2, String str3, CMMusicCallback<Result> cMMusicCallback) {
        String buildRequsetXml = EnablerInterface.buildRequsetXml("<UID>" + str + "</UID><type>" + str2 + "</type><amount>" + str3 + "</amount>");
        if (!"0".equals(str2)) {
            if (!"1".equals(str2)) {
                if ("2".equals(str2)) {
                }
            }
        }
        MiguSdkUtil.buildOrderParam(context, "http://218.200.227.123:95/sdkServer/1.0/pay/order/open", buildRequsetXml);
        return null;
    }

    public static PayPolicy getPayPolicy(Context context, String str, String str2) {
        try {
            return getPayPolicy(HttpPostCore.httpConnection(context, "http://218.200.227.123:95/sdkServer/1.0/pay/order/policy", EnablerInterface.buildRequsetXml("<UID>" + str + "</UID><musicId>" + str2 + "</musicId>")));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void getFullSongDownload(Context context, String str, String str2, String str3, CMMusicCallback<OrderResult> cMMusicCallback) {
        MiguSdkUtil.buildOrderParam(context, "http://218.200.227.123:95/sdkServer/1.0/pay/order/sdownlink", EnablerInterface.buildRequsetXml("<UID>" + str + "</UID><musicId>" + str2 + "</musicId><bizCode>" + str3 + "</bizCode>"));
    }

    public static void getVibrateDownload(Context context, String str, String str2, String str3, CMMusicCallback<OrderResult> cMMusicCallback) {
        MiguSdkUtil.buildOrderParam(context, "http://218.200.227.123:95/sdkServer/1.0/pay/order/rdownlink", EnablerInterface.buildRequsetXml("<UID>" + str + "</UID><musicId>" + str2 + "</musicId><bizCode>" + str3 + "</bizCode>"));
    }

    static UserResult getUserResult(InputStream inputStream) throws IOException, XmlPullParserException {
        String str;
        HashMap hashMap;
        AccountInfo accountInfo;
        PaymentUserInfo paymentUserInfo;
        ArrayList arrayList;
        ArrayList arrayList2 = null;
        if (inputStream == null) {
            return null;
        }
        UserResult userResult = new UserResult();
        try {
            XmlPullParser newPullParser = XmlPullParserFactory.newInstance().newPullParser();
            newPullParser.setInput(inputStream, "UTF-8");
            String str2 = null;
            HashMap hashMap2 = null;
            AccountInfo accountInfo2 = null;
            PaymentUserInfo paymentUserInfo2 = null;
            for (int eventType = newPullParser.getEventType(); 1 != eventType; eventType = newPullParser.next()) {
                String name = newPullParser.getName();
                switch (eventType) {
                    case 0:
                        str = str2;
                        hashMap = hashMap2;
                        accountInfo = accountInfo2;
                        paymentUserInfo = paymentUserInfo2;
                        arrayList = arrayList2;
                        continue;
                        arrayList2 = arrayList;
                        paymentUserInfo2 = paymentUserInfo;
                        accountInfo2 = accountInfo;
                        hashMap2 = hashMap;
                        str2 = str;
                    case 2:
                        if (name.equalsIgnoreCase("resCode")) {
                            userResult.setResCode(newPullParser.nextText());
                            str = str2;
                            hashMap = hashMap2;
                            accountInfo = accountInfo2;
                            paymentUserInfo = paymentUserInfo2;
                            arrayList = arrayList2;
                            continue;
                        } else if (name.equalsIgnoreCase("resMsg")) {
                            userResult.setResMsg(newPullParser.nextText());
                            str = str2;
                            hashMap = hashMap2;
                            accountInfo = accountInfo2;
                            paymentUserInfo = paymentUserInfo2;
                            arrayList = arrayList2;
                        } else if (name.equalsIgnoreCase("AccountInfo")) {
                            str = str2;
                            hashMap = hashMap2;
                            accountInfo = new AccountInfo();
                            paymentUserInfo = paymentUserInfo2;
                            arrayList = arrayList2;
                        } else if (name.equalsIgnoreCase("accountName")) {
                            accountInfo2.setAccountName(newPullParser.nextText());
                            str = str2;
                            hashMap = hashMap2;
                            accountInfo = accountInfo2;
                            paymentUserInfo = paymentUserInfo2;
                            arrayList = arrayList2;
                        } else if (name.equalsIgnoreCase("accountType")) {
                            accountInfo2.setAccountType(newPullParser.nextText());
                            str = str2;
                            hashMap = hashMap2;
                            accountInfo = accountInfo2;
                            paymentUserInfo = paymentUserInfo2;
                            arrayList = arrayList2;
                        } else if (name.equalsIgnoreCase("verified")) {
                            accountInfo2.setVerified(newPullParser.nextText());
                            str = str2;
                            hashMap = hashMap2;
                            accountInfo = accountInfo2;
                            paymentUserInfo = paymentUserInfo2;
                            arrayList = arrayList2;
                        } else if (name.equalsIgnoreCase("createTime")) {
                            if (accountInfo2 != null) {
                                accountInfo2.setCreateTime(newPullParser.nextText());
                                str = str2;
                                hashMap = hashMap2;
                                accountInfo = accountInfo2;
                                paymentUserInfo = paymentUserInfo2;
                                arrayList = arrayList2;
                            } else if (paymentUserInfo2 != null) {
                                paymentUserInfo2.setCreateTime(newPullParser.nextText());
                                str = str2;
                                hashMap = hashMap2;
                                accountInfo = accountInfo2;
                                paymentUserInfo = paymentUserInfo2;
                                arrayList = arrayList2;
                            }
                        } else if (name.equalsIgnoreCase("updateTime")) {
                            if (accountInfo2 != null) {
                                accountInfo2.setUpdateTime(newPullParser.nextText());
                                str = str2;
                                hashMap = hashMap2;
                                accountInfo = accountInfo2;
                                paymentUserInfo = paymentUserInfo2;
                                arrayList = arrayList2;
                            } else if (paymentUserInfo2 != null) {
                                paymentUserInfo2.setUpdateTime(newPullParser.nextText());
                                str = str2;
                                hashMap = hashMap2;
                                accountInfo = accountInfo2;
                                paymentUserInfo = paymentUserInfo2;
                                arrayList = arrayList2;
                            }
                        } else if (name.equalsIgnoreCase("UserInfo")) {
                            PaymentUserInfo paymentUserInfo3 = new PaymentUserInfo();
                            str = str2;
                            hashMap = new HashMap();
                            accountInfo = accountInfo2;
                            paymentUserInfo = paymentUserInfo3;
                            arrayList = arrayList2;
                        } else if (name.equalsIgnoreCase("identityID")) {
                            paymentUserInfo2.setIdentityID(newPullParser.nextText());
                            str = str2;
                            hashMap = hashMap2;
                            accountInfo = accountInfo2;
                            paymentUserInfo = paymentUserInfo2;
                            arrayList = arrayList2;
                        } else if (name.equalsIgnoreCase("userInfo")) {
                            str = str2;
                            hashMap = new HashMap();
                            accountInfo = accountInfo2;
                            paymentUserInfo = paymentUserInfo2;
                            arrayList = arrayList2;
                        } else if (name.equalsIgnoreCase("key")) {
                            str = newPullParser.nextText();
                            hashMap = hashMap2;
                            accountInfo = accountInfo2;
                            paymentUserInfo = paymentUserInfo2;
                            arrayList = arrayList2;
                        } else if (name.equalsIgnoreCase("value")) {
                            hashMap2.put(str2, newPullParser.nextText());
                            str = str2;
                            hashMap = hashMap2;
                            accountInfo = accountInfo2;
                            paymentUserInfo = paymentUserInfo2;
                            arrayList = arrayList2;
                        } else if (name.equalsIgnoreCase("registerMode")) {
                            userResult.setRegisterMode(newPullParser.nextText());
                            str = str2;
                            hashMap = hashMap2;
                            accountInfo = accountInfo2;
                            paymentUserInfo = paymentUserInfo2;
                            arrayList = arrayList2;
                        }
                        arrayList2 = arrayList;
                        paymentUserInfo2 = paymentUserInfo;
                        accountInfo2 = accountInfo;
                        hashMap2 = hashMap;
                        str2 = str;
                        break;
                    case 3:
                        if (!name.equalsIgnoreCase("AccountInfo")) {
                            if (name.equalsIgnoreCase("UserInfo")) {
                                if (paymentUserInfo2 != null) {
                                    paymentUserInfo2.setExt(hashMap2);
                                }
                                userResult.setUserInfo(paymentUserInfo2);
                                break;
                            }
                        } else {
                            if (arrayList2 == null) {
                                arrayList2 = new ArrayList();
                                userResult.setAccountInfoList(arrayList2);
                            }
                            arrayList2.add(accountInfo2);
                            str = str2;
                            hashMap = hashMap2;
                            accountInfo = accountInfo2;
                            paymentUserInfo = paymentUserInfo2;
                            arrayList = arrayList2;
                            continue;
                            arrayList2 = arrayList;
                            paymentUserInfo2 = paymentUserInfo;
                            accountInfo2 = accountInfo;
                            hashMap2 = hashMap;
                            str2 = str;
                        }
                        break;
                }
                str = str2;
                hashMap = hashMap2;
                accountInfo = accountInfo2;
                paymentUserInfo = paymentUserInfo2;
                arrayList = arrayList2;
                arrayList2 = arrayList;
                paymentUserInfo2 = paymentUserInfo;
                accountInfo2 = accountInfo;
                hashMap2 = hashMap;
                str2 = str;
            }
            return userResult;
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                }
            }
        }
    }

    static AccountResult getAccountResult(InputStream inputStream) throws IOException, XmlPullParserException {
        if (inputStream == null) {
            return null;
        }
        AccountResult accountResult = new AccountResult();
        try {
            XmlPullParser newPullParser = XmlPullParserFactory.newInstance().newPullParser();
            newPullParser.setInput(inputStream, "UTF-8");
            for (int eventType = newPullParser.getEventType(); 1 != eventType; eventType = newPullParser.next()) {
                String name = newPullParser.getName();
                switch (eventType) {
                    case 2:
                        if (!name.equalsIgnoreCase("resCode")) {
                            if (!name.equalsIgnoreCase("resMsg")) {
                                if (!name.equalsIgnoreCase("balance")) {
                                    if (!name.equalsIgnoreCase("accountStatus")) {
                                        if (!name.equalsIgnoreCase("passwordFlag")) {
                                            break;
                                        } else {
                                            accountResult.setPasswordFlag(newPullParser.nextText());
                                            break;
                                        }
                                    } else {
                                        accountResult.setAccountStatus(newPullParser.nextText());
                                        break;
                                    }
                                } else {
                                    accountResult.setBalance(newPullParser.nextText());
                                    break;
                                }
                            } else {
                                accountResult.setResMsg(newPullParser.nextText());
                                break;
                            }
                        } else {
                            accountResult.setResCode(newPullParser.nextText());
                            break;
                        }
                }
            }
            if (inputStream == null) {
                return accountResult;
            }
            try {
                return accountResult;
            } catch (IOException e) {
                return accountResult;
            }
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e2) {
                }
            }
        }
    }

    static DownloadRsp getDownloadRsp(InputStream inputStream) throws IOException, XmlPullParserException {
        if (inputStream == null) {
            return null;
        }
        DownloadRsp downloadRsp = new DownloadRsp();
        try {
            XmlPullParser newPullParser = XmlPullParserFactory.newInstance().newPullParser();
            newPullParser.setInput(inputStream, "UTF-8");
            for (int eventType = newPullParser.getEventType(); 1 != eventType; eventType = newPullParser.next()) {
                String name = newPullParser.getName();
                switch (eventType) {
                    case 2:
                        if (!name.equalsIgnoreCase("resCode")) {
                            if (!name.equalsIgnoreCase("resMsg")) {
                                if (!name.equalsIgnoreCase("downUrl")) {
                                    if (!name.equalsIgnoreCase("downloadKey")) {
                                        break;
                                    } else {
                                        downloadRsp.setDownloadKey(newPullParser.nextText());
                                        break;
                                    }
                                } else {
                                    downloadRsp.setDownUrl(newPullParser.nextText());
                                    break;
                                }
                            } else {
                                downloadRsp.setResMsg(newPullParser.nextText());
                                break;
                            }
                        } else {
                            downloadRsp.setResCode(newPullParser.nextText());
                            break;
                        }
                }
            }
            if (inputStream == null) {
                return downloadRsp;
            }
            try {
                return downloadRsp;
            } catch (IOException e) {
                return downloadRsp;
            }
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e2) {
                }
            }
        }
    }

    static PaymentResult getPaymentResult(InputStream inputStream) throws IOException, XmlPullParserException {
        PaymentInfo paymentInfo;
        ArrayList arrayList;
        ArrayList arrayList2 = null;
        if (inputStream == null) {
            return null;
        }
        PaymentResult paymentResult = new PaymentResult();
        try {
            XmlPullParser newPullParser = XmlPullParserFactory.newInstance().newPullParser();
            newPullParser.setInput(inputStream, "UTF-8");
            PaymentInfo paymentInfo2 = null;
            for (int eventType = newPullParser.getEventType(); 1 != eventType; eventType = newPullParser.next()) {
                String name = newPullParser.getName();
                switch (eventType) {
                    case 0:
                        paymentInfo = paymentInfo2;
                        arrayList = arrayList2;
                        continue;
                        arrayList2 = arrayList;
                        paymentInfo2 = paymentInfo;
                    case 2:
                        if (name.equalsIgnoreCase("resCode")) {
                            paymentResult.setResCode(newPullParser.nextText());
                            paymentInfo = paymentInfo2;
                            arrayList = arrayList2;
                            continue;
                        } else if (name.equalsIgnoreCase("resMsg")) {
                            paymentResult.setResMsg(newPullParser.nextText());
                            paymentInfo = paymentInfo2;
                            arrayList = arrayList2;
                        } else if (name.equalsIgnoreCase("PaymentInfo")) {
                            paymentInfo = new PaymentInfo();
                            arrayList = arrayList2;
                        } else if (name.equalsIgnoreCase("identityID")) {
                            paymentInfo2.setIdentityID(newPullParser.nextText());
                            paymentInfo = paymentInfo2;
                            arrayList = arrayList2;
                        } else if (name.equalsIgnoreCase("currencyType")) {
                            paymentInfo2.setCurrencyType(newPullParser.nextText());
                            paymentInfo = paymentInfo2;
                            arrayList = arrayList2;
                        } else if (name.equalsIgnoreCase("tradeNum")) {
                            paymentInfo2.setTradeNum(newPullParser.nextText());
                            paymentInfo = paymentInfo2;
                            arrayList = arrayList2;
                        } else if (name.equalsIgnoreCase("optType")) {
                            paymentInfo2.setOptType(newPullParser.nextText());
                            paymentInfo = paymentInfo2;
                            arrayList = arrayList2;
                        } else if (name.equalsIgnoreCase("amount")) {
                            paymentInfo2.setAmount(newPullParser.nextText());
                            paymentInfo = paymentInfo2;
                            arrayList = arrayList2;
                        } else if (name.equalsIgnoreCase("relationID")) {
                            paymentInfo2.setRelationID(newPullParser.nextText());
                            paymentInfo = paymentInfo2;
                            arrayList = arrayList2;
                        } else if (name.equalsIgnoreCase("orderNum")) {
                            paymentInfo2.setOrderNum(newPullParser.nextText());
                            paymentInfo = paymentInfo2;
                            arrayList = arrayList2;
                        } else if (name.equalsIgnoreCase("orderContent")) {
                            paymentInfo2.setOrderContent(newPullParser.nextText());
                            paymentInfo = paymentInfo2;
                            arrayList = arrayList2;
                        } else if (name.equalsIgnoreCase("orderDesc")) {
                            paymentInfo2.setOrderDesc(newPullParser.nextText());
                            paymentInfo = paymentInfo2;
                            arrayList = arrayList2;
                        } else if (name.equalsIgnoreCase("oldBalance")) {
                            paymentInfo2.setOldBalance(newPullParser.nextText());
                            paymentInfo = paymentInfo2;
                            arrayList = arrayList2;
                        } else if (name.equalsIgnoreCase("dealResult")) {
                            paymentInfo2.setDealResult(newPullParser.nextText());
                            paymentInfo = paymentInfo2;
                            arrayList = arrayList2;
                        } else if (name.equalsIgnoreCase("optTime")) {
                            paymentInfo2.setOptTime(newPullParser.nextText());
                            paymentInfo = paymentInfo2;
                            arrayList = arrayList2;
                        } else if (name.equalsIgnoreCase("accessPlatformID")) {
                            paymentInfo2.setAccessPlatformID(newPullParser.nextText());
                            paymentInfo = paymentInfo2;
                            arrayList = arrayList2;
                        } else if (name.equalsIgnoreCase("payChannel")) {
                            paymentInfo2.setPayChannel(newPullParser.nextText());
                            paymentInfo = paymentInfo2;
                            arrayList = arrayList2;
                        } else if (name.equalsIgnoreCase("description")) {
                            paymentInfo2.setDescription(newPullParser.nextText());
                            paymentInfo = paymentInfo2;
                            arrayList = arrayList2;
                        }
                        arrayList2 = arrayList;
                        paymentInfo2 = paymentInfo;
                        break;
                    case 3:
                        if (name.equalsIgnoreCase("PaymentInfo")) {
                            if (arrayList2 == null) {
                                arrayList2 = new ArrayList();
                                paymentResult.setPaymentInfoList(arrayList2);
                            }
                            arrayList2.add(paymentInfo2);
                            break;
                        }
                        break;
                }
                paymentInfo = paymentInfo2;
                arrayList = arrayList2;
                arrayList2 = arrayList;
                paymentInfo2 = paymentInfo;
            }
            return paymentResult;
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                }
            }
        }
    }

    static PayPolicy getPayPolicy(InputStream inputStream) throws IOException, XmlPullParserException {
        BizInfoNet bizInfoNet;
        ArrayList arrayList;
        ArrayList arrayList2 = null;
        if (inputStream == null) {
            return null;
        }
        PayPolicy payPolicy = new PayPolicy();
        try {
            XmlPullParser newPullParser = XmlPullParserFactory.newInstance().newPullParser();
            newPullParser.setInput(inputStream, "UTF-8");
            BizInfoNet bizInfoNet2 = null;
            for (int eventType = newPullParser.getEventType(); 1 != eventType; eventType = newPullParser.next()) {
                String name = newPullParser.getName();
                switch (eventType) {
                    case 0:
                        bizInfoNet = bizInfoNet2;
                        arrayList = arrayList2;
                        continue;
                        arrayList2 = arrayList;
                        bizInfoNet2 = bizInfoNet;
                    case 2:
                        if (name.equalsIgnoreCase("resCode")) {
                            payPolicy.setResCode(newPullParser.nextText());
                            bizInfoNet = bizInfoNet2;
                            arrayList = arrayList2;
                            continue;
                        } else if (name.equalsIgnoreCase("resMsg")) {
                            payPolicy.setResMsg(newPullParser.nextText());
                            bizInfoNet = bizInfoNet2;
                            arrayList = arrayList2;
                        } else if (name.equalsIgnoreCase("BizInfos")) {
                            bizInfoNet = new BizInfoNet();
                            arrayList = arrayList2;
                        } else if (name.equalsIgnoreCase("bizCode")) {
                            bizInfoNet2.setBizCode(newPullParser.nextText());
                            bizInfoNet = bizInfoNet2;
                            arrayList = arrayList2;
                        } else if (name.equalsIgnoreCase("bizType")) {
                            bizInfoNet2.setBizType(newPullParser.nextText());
                            bizInfoNet = bizInfoNet2;
                            arrayList = arrayList2;
                        } else if (name.equalsIgnoreCase("originalPrice")) {
                            bizInfoNet2.setOriginalPrice(newPullParser.nextText());
                            bizInfoNet = bizInfoNet2;
                            arrayList = arrayList2;
                        } else if (name.equalsIgnoreCase("salePrice")) {
                            bizInfoNet2.setSalePrice(newPullParser.nextText());
                            bizInfoNet = bizInfoNet2;
                            arrayList = arrayList2;
                        } else if (name.equalsIgnoreCase(SocialConstants.PARAM_APP_DESC)) {
                            bizInfoNet2.setDesc(newPullParser.nextText());
                            bizInfoNet = bizInfoNet2;
                            arrayList = arrayList2;
                        } else if (name.equalsIgnoreCase("offReason")) {
                            bizInfoNet2.setOffReason(newPullParser.nextText());
                            bizInfoNet = bizInfoNet2;
                            arrayList = arrayList2;
                        } else if (name.equalsIgnoreCase("resource")) {
                            bizInfoNet2.setResource(newPullParser.nextText());
                            bizInfoNet = bizInfoNet2;
                            arrayList = arrayList2;
                        } else if (name.equalsIgnoreCase("sequencesID")) {
                            payPolicy.setSequencesID(newPullParser.nextText());
                            bizInfoNet = bizInfoNet2;
                            arrayList = arrayList2;
                        } else if (name.equalsIgnoreCase("monLevel")) {
                            payPolicy.setMonLevel(newPullParser.nextText());
                            bizInfoNet = bizInfoNet2;
                            arrayList = arrayList2;
                        }
                        arrayList2 = arrayList;
                        bizInfoNet2 = bizInfoNet;
                        break;
                    case 3:
                        if (name.equalsIgnoreCase("BizInfos")) {
                            if (arrayList2 == null) {
                                arrayList2 = new ArrayList();
                                payPolicy.setBizInfos(arrayList2);
                            }
                            arrayList2.add(bizInfoNet2);
                            break;
                        }
                        break;
                }
                bizInfoNet = bizInfoNet2;
                arrayList = arrayList2;
                arrayList2 = arrayList;
                bizInfoNet2 = bizInfoNet;
            }
            return payPolicy;
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                }
            }
        }
    }

    static RechargeResult getRechargeResult(InputStream inputStream) throws IOException, XmlPullParserException {
        if (inputStream == null) {
            return null;
        }
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        byte[] bArr = new byte[1024];
        while (true) {
            int read = inputStream.read(bArr, 0, 1024);
            if (read == -1) {
                break;
            }
            byteArrayOutputStream.write(bArr, 0, read);
        }
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(new String(byteArrayOutputStream.toByteArray(), "UTF-8").replace("&", "&amp;").getBytes("UTF-8"));
        RechargeResult rechargeResult = new RechargeResult();
        try {
            XmlPullParser newPullParser = XmlPullParserFactory.newInstance().newPullParser();
            newPullParser.setInput(byteArrayInputStream, "UTF-8");
            for (int eventType = newPullParser.getEventType(); 1 != eventType; eventType = newPullParser.next()) {
                String name = newPullParser.getName();
                switch (eventType) {
                    case 2:
                        if (!name.equalsIgnoreCase("resCode")) {
                            if (!name.equalsIgnoreCase("resMsg")) {
                                if (!name.equalsIgnoreCase("redirectUrl")) {
                                    break;
                                } else {
                                    rechargeResult.setRedirectUrl(newPullParser.nextText());
                                    break;
                                }
                            } else {
                                rechargeResult.setResMsg(newPullParser.nextText());
                                break;
                            }
                        } else {
                            rechargeResult.setResCode(newPullParser.nextText());
                            break;
                        }
                }
            }
            if (byteArrayInputStream == null) {
                return rechargeResult;
            }
            try {
                return rechargeResult;
            } catch (IOException e) {
                return rechargeResult;
            }
        } finally {
            if (byteArrayInputStream != null) {
                try {
                    byteArrayInputStream.close();
                } catch (IOException e2) {
                }
            }
        }
    }

    private static String writeUpdateMemberRequest(String str, Map<String, String> map) throws Exception {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        XmlSerializer newSerializer = Xml.newSerializer();
        newSerializer.setOutput(byteArrayOutputStream, "UTF-8");
        newSerializer.startDocument("UTF-8", true);
        newSerializer.startTag(null, SocialConstants.TYPE_REQUEST);
        newSerializer.startTag(null, Constants.UID);
        newSerializer.text(str);
        newSerializer.endTag(null, Constants.UID);
        newSerializer.startTag(null, "userInfo");
        newSerializer.startTag(null, "ext");
        if (map != null) {
            for (Map.Entry next : map.entrySet()) {
                newSerializer.startTag(null, "entry");
                newSerializer.startTag(null, "key");
                newSerializer.text(next.getKey().toString());
                newSerializer.endTag(null, "key");
                newSerializer.startTag(null, "value");
                newSerializer.text(next.getValue().toString());
                newSerializer.endTag(null, "value");
                newSerializer.endTag(null, "entry");
            }
        }
        newSerializer.endTag(null, "ext");
        newSerializer.endTag(null, "userInfo");
        newSerializer.endTag(null, SocialConstants.TYPE_REQUEST);
        newSerializer.endDocument();
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        Log.d("TAG", "requestbody------------\r\n" + new String(byteArray, "UTF-8"));
        return new String(byteArray, "UTF-8");
    }

    private static String getPayments(InputStream inputStream) {
        String str;
        if (inputStream == null) {
            return null;
        }
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        byte[] bArr = new byte[1024];
        while (true) {
            try {
                int read = inputStream.read(bArr, 0, 1024);
                if (read == -1) {
                    break;
                }
                byteArrayOutputStream.write(bArr, 0, read);
            } catch (Exception e) {
                e.printStackTrace();
                str = null;
            }
        }
        str = new String(byteArrayOutputStream.toByteArray(), "GB2312");
        Log.i("TAG", "responseBody------------\r\n" + str);
        return str;
    }
}
