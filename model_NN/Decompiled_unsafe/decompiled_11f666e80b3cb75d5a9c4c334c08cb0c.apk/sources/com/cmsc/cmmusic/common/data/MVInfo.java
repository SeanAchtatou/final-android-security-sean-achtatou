package com.cmsc.cmmusic.common.data;

public class MVInfo {
    private String mvName;
    private String price;
    private String singerName;

    public String getPrice() {
        return this.price;
    }

    public void setPrice(String str) {
        this.price = str;
    }

    public String getMvName() {
        return this.mvName;
    }

    public void setMvName(String str) {
        this.mvName = str;
    }

    public String getSingerName() {
        return this.singerName;
    }

    public void setSingerName(String str) {
        this.singerName = str;
    }

    public String toString() {
        return "MVInfo [price=" + this.price + ", mvName=" + this.mvName + ", singerName=" + this.singerName + "]";
    }
}
