package com.cmsc.cmmusic.common;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import com.cmsc.cmmusic.common.data.BizInfo;
import com.cmsc.cmmusic.common.data.OrderPolicy;
import com.cmsc.cmmusic.common.data.OrderResult;
import com.cmsc.cmmusic.common.data.SongOpenPolicy;
import com.tencent.connect.common.Constants;
import java.util.ArrayList;
import java.util.Iterator;

final class CPFullSongView extends OrderView {
    private static /* synthetic */ int[] $SWITCH_TABLE$com$cmsc$cmmusic$common$data$OrderPolicy$OrderType = null;
    private static final String LOG_TAG = "CPFullSongView";
    private BizInfo binfo;
    private String dbPrice;
    private String hdPrice;
    private Button openMonButton;
    private LinearLayout priceView;
    private RadioGroup rdGroup;
    private String sdPrice;

    static /* synthetic */ int[] $SWITCH_TABLE$com$cmsc$cmmusic$common$data$OrderPolicy$OrderType() {
        int[] iArr = $SWITCH_TABLE$com$cmsc$cmmusic$common$data$OrderPolicy$OrderType;
        if (iArr == null) {
            iArr = new int[OrderPolicy.OrderType.values().length];
            try {
                iArr[OrderPolicy.OrderType.net.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[OrderPolicy.OrderType.sms.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[OrderPolicy.OrderType.verifyCode.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            $SWITCH_TABLE$com$cmsc$cmmusic$common$data$OrderPolicy$OrderType = iArr;
        }
        return iArr;
    }

    public CPFullSongView(Context context, Bundle bundle) {
        super(context, bundle);
    }

    /* access modifiers changed from: protected */
    public void updateNetView() {
        setUserTip("点击“确认”直接下载歌曲至手机");
        ArrayList<BizInfo> bizInfos = this.policyObj.getBizInfos();
        if (bizInfos != null) {
            Iterator<BizInfo> it = bizInfos.iterator();
            while (it.hasNext()) {
                BizInfo next = it.next();
                if (next != null) {
                    String bizType = next.getBizType();
                    Logger.i("TAG", "bizType = " + bizType);
                    if (Constants.VIA_REPORT_TYPE_SHARE_TO_QZONE.equalsIgnoreCase(bizType)) {
                        this.binfo = next;
                        String resource = next.getResource();
                        if ("000009".equals(resource)) {
                            this.sdPrice = String.valueOf(EnablerInterface.getPriceString(next.getOriginalPrice())) + "  ";
                        } else if ("020007".equals(resource)) {
                            this.hdPrice = String.valueOf(EnablerInterface.getPriceString(next.getOriginalPrice())) + "  ";
                        } else if ("020022".equals(resource)) {
                            this.dbPrice = String.valueOf(EnablerInterface.getPriceString(next.getOriginalPrice())) + "  ";
                        }
                    } else if ("30".equalsIgnoreCase(bizType)) {
                        this.binfo = next;
                        String originalPrice = next.getOriginalPrice();
                        if (originalPrice != null && originalPrice.trim().length() > 0) {
                            String resource2 = next.getResource();
                            if ("000009".equals(resource2)) {
                                this.sdPrice = String.valueOf(EnablerInterface.getPriceString(next.getOriginalPrice())) + "  ";
                            } else if ("020007".equals(resource2)) {
                                this.hdPrice = String.valueOf(EnablerInterface.getPriceString(next.getOriginalPrice())) + "  ";
                            } else if ("020022".equals(resource2)) {
                                this.dbPrice = String.valueOf(EnablerInterface.getPriceString(next.getOriginalPrice())) + "  ";
                            }
                        }
                    } else if ("00".equalsIgnoreCase(bizType)) {
                        this.binfo = next;
                        String resource3 = next.getResource();
                        if ("000009".equals(resource3)) {
                            this.sdPrice = String.valueOf(EnablerInterface.getPriceString(next.getOriginalPrice())) + "  ";
                        } else if ("020007".equals(resource3)) {
                            this.hdPrice = String.valueOf(EnablerInterface.getPriceString(next.getOriginalPrice())) + "  ";
                        } else if ("020022".equals(resource3)) {
                            this.dbPrice = String.valueOf(EnablerInterface.getPriceString(next.getOriginalPrice())) + "  ";
                        }
                    }
                }
            }
        }
        if (this.sdPrice == null || this.sdPrice.trim().length() <= 0) {
            ((RadioButton) this.rdGroup.findViewById(100)).setVisibility(8);
        } else {
            ((RadioButton) this.rdGroup.findViewById(100)).setText("标清版（40kbps）/2.0 元");
        }
        if (this.hdPrice == null || this.hdPrice.trim().length() <= 0) {
            ((RadioButton) this.rdGroup.findViewById(101)).setVisibility(8);
        } else {
            ((RadioButton) this.rdGroup.findViewById(101)).setText("高清版（128kbps）/2.0 元");
        }
        if (this.dbPrice == null || this.dbPrice.trim().length() <= 0) {
            ((RadioButton) this.rdGroup.findViewById(102)).setVisibility(8);
        } else {
            ((RadioButton) this.rdGroup.findViewById(102)).setText("杜比高清版 /2.0 元");
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* access modifiers changed from: protected */
    public void sureClicked() {
        Log.d(LOG_TAG, "sure button clicked");
        try {
            switch ($SWITCH_TABLE$com$cmsc$cmmusic$common$data$OrderPolicy$OrderType()[this.orderType.ordinal()]) {
                case 1:
                    Log.d(LOG_TAG, "Get download url by net");
                    String str = "";
                    switch (this.rdGroup.getCheckedRadioButtonId()) {
                        case 100:
                            str = "0";
                            break;
                        case 101:
                            str = "1";
                            break;
                        case 102:
                            str = "2";
                            break;
                    }
                    EnablerInterface.getCPFullSongTimeDownloadUrl(this.mCurActivity, this.curExtraInfo.getString("MusicId"), str, this.policyObj.getMonLevel(), new CMMusicCallback<OrderResult>() {
                        public void operationResult(OrderResult orderResult) {
                            CPFullSongView.this.mCurActivity.closeActivity(orderResult);
                        }
                    });
                    return;
                case 2:
                    Log.d(LOG_TAG, "Get download url by sms");
                    this.mCurActivity.closeActivity(null);
                    return;
                case 3:
                    Log.d(LOG_TAG, "Get download url by sign code");
                    return;
                default:
                    return;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        e.printStackTrace();
    }

    /* access modifiers changed from: protected */
    public void initContentView(LinearLayout linearLayout) {
        this.rdGroup = new RadioGroup(this.mCurActivity);
        RadioButton radioButton = new RadioButton(this.mCurActivity);
        radioButton.setText("标清版（40kbps）");
        radioButton.setId(100);
        radioButton.setChecked(true);
        RadioButton radioButton2 = new RadioButton(this.mCurActivity);
        radioButton2.setText("高清版（128kbps）");
        radioButton2.setId(101);
        RadioButton radioButton3 = new RadioButton(this.mCurActivity);
        radioButton3.setText("杜比高清版");
        radioButton3.setId(102);
        this.rdGroup.addView(radioButton);
        this.rdGroup.addView(radioButton2);
        this.rdGroup.addView(radioButton3);
        linearLayout.addView(this.rdGroup);
        linearLayout.addView(getPriceView());
    }

    /* access modifiers changed from: protected */
    public LinearLayout getPriceView() {
        this.priceView = new LinearLayout(this.mCurActivity);
        this.priceView.setOrientation(0);
        this.priceView.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        this.priceView.setVisibility(0);
        this.openMonButton = new Button(this.mCurActivity);
        this.openMonButton.setHeight(30);
        this.openMonButton.setText("开通包月");
        this.openMonButton.setGravity(17);
        this.openMonButton.setLayoutParams(new LinearLayout.LayoutParams(-2, -2));
        this.openMonButton.setVisibility(8);
        this.openMonButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                CPFullSongView.this.mCurActivity.showProgressBar("请稍候...");
                new Thread() {
                    public void run() {
                        try {
                            final SongOpenPolicy querySongOpenPolicyByNet = EnablerInterface.querySongOpenPolicyByNet(CPFullSongView.this.mCurActivity);
                            if (querySongOpenPolicyByNet == null || !"000000".equals(querySongOpenPolicyByNet.getResCode())) {
                                CPFullSongView.this.mCurActivity.showToast("请求失败");
                                return;
                            }
                            CPFullSongView.this.mHandler.post(new Runnable() {
                                public void run() {
                                    OpenSongMonthView openSongMonthView = new OpenSongMonthView(CPFullSongView.this.mCurActivity, new Bundle());
                                    CPFullSongView.this.mCurActivity.setContentView(openSongMonthView);
                                    CPFullSongView.this.policyObj.setSongMonthInfos(querySongOpenPolicyByNet.getSongMonthInfos());
                                    openSongMonthView.updateView(CPFullSongView.this.policyObj);
                                }
                            });
                            CPFullSongView.this.mCurActivity.hideProgressBar();
                        } catch (Exception e) {
                            e.printStackTrace();
                            CPFullSongView.this.mCurActivity.showToast("请求失败");
                        } finally {
                            CPFullSongView.this.mCurActivity.hideProgressBar();
                        }
                    }
                }.start();
            }
        });
        this.priceView.addView(this.openMonButton);
        return this.priceView;
    }
}
