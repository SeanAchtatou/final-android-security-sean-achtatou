package com.cmsc.cmmusic.common;

import android.content.Context;
import android.os.Build;
import android.util.Base64;
import android.util.Log;
import com.cmsc.cmmusic.common.data.OrderResult;
import com.cmsc.cmmusic.init.GetAppInfo;
import com.cmsc.cmmusic.init.GetAppInfoInterface;
import com.cmsc.cmmusic.init.PreferenceUtil;
import com.cmsc.cmmusic.init.Utils;
import com.migu.sdk.api.CommonInfo;
import com.migu.sdk.api.CommonPayInfo;
import com.migu.sdk.api.MiguSdk;
import com.migu.sdk.api.PayCallBack;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.json.JSONException;
import org.json.JSONObject;
import org.xmlpull.v1.XmlPullParserException;

final class MiguSdkUtil {
    static final String BILLING = "billing";

    MiguSdkUtil() {
    }

    static final class TL {
        private static Object obj = new Object();
        private static final ThreadLocal tl = new ThreadLocal();

        TL() {
        }

        static void init() {
            tl.set(obj);
        }

        static boolean is() {
            boolean z = obj == tl.get();
            tl.remove();
            return z;
        }
    }

    static String buildOrderParam(Context context, String str, String str2) {
        return buildOrderParam(context, str, str2, null);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{org.json.JSONObject.put(java.lang.String, double):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, long):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, int):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, java.lang.Object):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException} */
    static String buildOrderParam(Context context, String str, String str2, String str3) {
        String authorization;
        if (GetAppInfoInterface.isTokenExist(context)) {
            authorization = getTokenAuthorization(context, str3);
        } else if (GetAppInfoInterface.isImsiExist(context)) {
            authorization = getImsiAuthorization(context, str3);
        } else {
            authorization = getAuthorization(context, str3);
        }
        JSONObject jSONObject = new JSONObject();
        try {
            JSONObject put = jSONObject.put("header", authorization);
            if (str2 == null) {
                str2 = "";
            }
            put.put("body", str2).put("target", str);
            if (TL.is()) {
                jSONObject.put("noInterface", true);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jSONObject.toString();
    }

    static String buildCpparam(Context context, String str, String str2) {
        return buildCpparam(context, str, str2, null);
    }

    static String buildCpparam(Context context, String str, String str2, String str3) {
        return buildCpparam(context, str, str2, str3, false);
    }

    private static String buildMonCpparam(Context context) {
        return buildMonCpparam(context, null);
    }

    private static String buildMonCpparam(Context context, String str) {
        return buildCpparam(context, null, null, str, true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{org.json.JSONObject.put(java.lang.String, double):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, long):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, int):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, java.lang.Object):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException} */
    static String buildCpparam(Context context, String str, String str2, String str3, boolean z) {
        String str4 = "~";
        if (z) {
            str4 = "|";
            str = getMonCpparam(context);
        }
        JSONObject jSONObject = new JSONObject();
        try {
            String str5 = GetAppInfo.getexCode(context);
            if (str5 != null && str5.length() > 0) {
                jSONObject.put("excode", str5);
            }
            if (str2 != null && str2.length() > 0) {
                jSONObject.put("optCode", str2);
            }
            if (str3 != null && str3.length() > 0) {
                jSONObject.put("defSeq", str3);
            }
            if (TL.is()) {
                jSONObject.put("nointerface", true);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return String.valueOf(str) + str4 + encodeBase64(jSONObject.toString().getBytes());
    }

    static String getTokenAuthorization(Context context, String str) {
        return "OEPAUTH realm=\"OEP\",Token=\"" + PreferenceUtil.getToken(context) + buildAuthorization(context, str);
    }

    static String getImsiAuthorization(Context context, String str) {
        return "OEPAUTH realm=\"OEP\",IMSI=\"" + GetAppInfoInterface.getIMSI(context) + buildAuthorization(context, str);
    }

    static String getAuthorization(Context context, String str) {
        return "OEPAUTH realm=\"OEP\",IMSI=\"" + buildAuthorization(context, str);
    }

    private static String buildAuthorization(Context context, String str) {
        return "\",chCode=\"" + GetAppInfoInterface.getChannelCode(context) + "\",appID=\"" + GetAppInfoInterface.getAppid(context) + "\",pubKey=\"" + GetAppInfoInterface.getSign(context) + "\",netMode=\"" + GetAppInfoInterface.getNetMode(context) + "\",packageName=\"" + GetAppInfoInterface.getPackageName(context) + "\",version=\"" + "M3.3" + "\",excode=\"" + GetAppInfo.getexCode(context) + "\",definedseq=\"" + str + "\"" + ",imei=\"" + GetAppInfo.getIMEI(context) + "\"" + ",sim=\"" + GetAppInfo.getIMSI(context) + "\"" + ",os=\"" + Build.VERSION.RELEASE + "\"" + ",brand=\"" + Build.BRAND + "\"" + ",model=\"" + Build.MODEL + "\"" + ",mac=\"" + GetAppInfo.getMacAddress(context) + "\"" + ",payTime=\"" + getPayTime() + "\"";
    }

    private static String getPayTime() {
        return new SimpleDateFormat("yyyyMMddHHmmss").format(new Date(System.currentTimeMillis()));
    }

    /* access modifiers changed from: private */
    public static String appendOrderIdToAuthHeader(Context context, String str, String str2) {
        return String.valueOf(str) + ",orderId=\"" + str2 + "\"";
    }

    static void pay(Context context, CommonPayInfo commonPayInfo, String str, String str2, String str3, CMMusicCallback<OrderResult> cMMusicCallback) {
        pay(context, new CommonPayInfo[]{commonPayInfo}, str, str2, str3, cMMusicCallback);
    }

    static void payMon(Context context, CommonPayInfo commonPayInfo, CMMusicCallback<OrderResult> cMMusicCallback) {
        payMon(context, new CommonPayInfo[]{commonPayInfo}, buildMonCpparam(context), cMMusicCallback);
    }

    static void payMon(Context context, CommonPayInfo commonPayInfo, String str, CMMusicCallback<OrderResult> cMMusicCallback) {
        payMon(context, new CommonPayInfo[]{commonPayInfo}, buildMonCpparam(context, str), cMMusicCallback);
    }

    private static String getMonCpparam(Context context) {
        return "1128028||" + GetAppInfo.getIMEI(context) + "||" + GetAppInfo.getIMSI(context) + "|" + Build.VERSION.RELEASE + "|" + Build.BRAND + "|" + Build.MODEL + "|" + GetAppInfo.getMacAddress(context) + "|" + getPayTime();
    }

    static void payMon(Context context, CommonPayInfo[] commonPayInfoArr, String str, final CMMusicCallback<OrderResult> cMMusicCallback) {
        CommonInfo commonInfo = setupCommonInfo(null, setupCommonPayInfosAndTotalPrice(context, commonPayInfoArr, true));
        commonInfo.setMonthly(true);
        commonInfo.setOperType("0");
        commonPayInfoArr[0].setOrderId(commonInfo.getOrderId());
        final OrderResult orderResult = new OrderResult();
        orderResult.setOrderId(commonInfo.getOrderId());
        MiguSdk.pay(context, commonInfo, commonPayInfoArr, str, "", new PayCallBack.IPayCallback() {
            public void onResult(int i, String str, String str2) {
                OrderResult.this.setResultCode(i);
                OrderResult.this.setResCode(str);
                OrderResult.this.setResMsg(str2);
                cMMusicCallback.operationResult(OrderResult.this);
            }
        });
    }

    private static void pay(Context context, CommonPayInfo[] commonPayInfoArr, String str, String str2, String str3, CMMusicCallback<OrderResult> cMMusicCallback) {
        if (commonPayInfoArr == null || commonPayInfoArr.length == 0) {
            cMMusicCallback.operationResult(null);
            return;
        }
        OrderResult orderResult = new OrderResult();
        try {
            Integer num = setupCommonPayInfosAndTotalPrice(context, commonPayInfoArr, false);
            JSONObject jSONObject = new JSONObject(str3);
            String optString = jSONObject.optString("header");
            String optString2 = jSONObject.optString("body");
            String optString3 = jSONObject.optString("target");
            if (num.intValue() > 0) {
                payAndOrder(context, commonPayInfoArr, num, str, str2, orderResult, optString, optString2, optString3, cMMusicCallback);
            } else {
                order(context, orderResult, optString, optString2, optString3, cMMusicCallback);
            }
        } catch (Exception e) {
            e.printStackTrace();
            orderResult.setResCode("900");
            orderResult.setResMsg("请求不合法");
            cMMusicCallback.operationResult(orderResult);
        }
    }

    private static Integer setupCommonPayInfosAndTotalPrice(Context context, CommonPayInfo[] commonPayInfoArr, boolean z) {
        int i = 0;
        for (CommonPayInfo commonPayInfo : commonPayInfoArr) {
            String price = commonPayInfo.getPrice();
            if (price != null && price.trim().length() > 0) {
                i += Integer.valueOf(commonPayInfo.getPrice()).intValue();
            }
            if (!z) {
                String contentId = commonPayInfo.getContentId();
                if (contentId != null && contentId.length() > 0) {
                    commonPayInfo.setCpId(contentId.substring(0, 6));
                }
            } else {
                String productId = commonPayInfo.getProductId();
                if (productId != null && productId.length() > 0) {
                    String substring = productId.substring(0, 6);
                    commonPayInfo.setSpCode(substring);
                    commonPayInfo.setCpId(substring);
                }
            }
            commonPayInfo.setOrderId(getOrderId());
            commonPayInfo.setChannelId(GetAppInfoInterface.getChannelCode(context));
        }
        return Integer.valueOf(i);
    }

    private static void payAndOrder(Context context, CommonPayInfo[] commonPayInfoArr, Integer num, String str, String str2, OrderResult orderResult, String str3, String str4, String str5, CMMusicCallback<OrderResult> cMMusicCallback) {
        CommonInfo commonInfo = setupCommonInfo(str, num);
        orderResult.setOrderId(commonInfo.getOrderId());
        Log.i("MemberType", commonInfo.getMemberType());
        final OrderResult orderResult2 = orderResult;
        final Context context2 = context;
        final String str6 = str3;
        final String str7 = str4;
        final String str8 = str5;
        final CMMusicCallback<OrderResult> cMMusicCallback2 = cMMusicCallback;
        MiguSdk.pay(context, commonInfo, commonPayInfoArr, str2, "", new PayCallBack.IPayCallback() {
            public void onResult(int i, String str, String str2) {
                OrderResult.this.setResultCode(i);
                OrderResult.this.setResCode(str);
                OrderResult.this.setResMsg(str2);
                if ("0".equals(str) || "00000".equals(str) || "2001".equals(str) || "2002".equals(str) || "10003".equals(str) || "10004".equals(str)) {
                    MiguSdkUtil.order(context2, OrderResult.this, MiguSdkUtil.appendOrderIdToAuthHeader(context2, str6, OrderResult.this.getOrderId()), str7, str8, cMMusicCallback2);
                } else {
                    cMMusicCallback2.operationResult(OrderResult.this);
                }
            }
        });
    }

    private static CommonInfo setupCommonInfo(String str, Integer num) {
        CommonInfo commonInfo = new CommonInfo();
        commonInfo.setcType("1");
        commonInfo.setPrice(num.toString());
        commonInfo.setMemberType(str);
        commonInfo.setOrderId(getOrderId());
        return commonInfo;
    }

    private static String getOrderId() {
        return "01010" + new SimpleDateFormat("yyyyMMdd").format(new Date(System.currentTimeMillis())) + Utils.getRandomInt(9);
    }

    static void order(Context context, String str, CMMusicCallback<OrderResult> cMMusicCallback) {
        OrderResult orderResult = new OrderResult();
        try {
            JSONObject jSONObject = new JSONObject(str);
            order(context, orderResult, jSONObject.optString("header"), jSONObject.optString("body"), jSONObject.optString("target"), cMMusicCallback);
        } catch (JSONException e) {
            e.printStackTrace();
            cMMusicCallback.operationResult(null);
        }
    }

    /* access modifiers changed from: private */
    public static void order(Context context, OrderResult orderResult, String str, String str2, String str3, CMMusicCallback<OrderResult> cMMusicCallback) {
        if (context instanceof CMMusicActivity) {
            ((CMMusicActivity) context).showProgressBar("请稍候...");
        }
        final Context context2 = context;
        final CMMusicCallback<OrderResult> cMMusicCallback2 = cMMusicCallback;
        final OrderResult orderResult2 = orderResult;
        final String str4 = str3;
        final String str5 = str2;
        final String str6 = str;
        new Thread() {
            public void run() {
                try {
                    EnablerInterface.getOrderResult(HttpPostCore.httpConnection(context2, str4, str5, str6), orderResult2);
                    if (context2 instanceof CMMusicActivity) {
                        ((CMMusicActivity) context2).hideProgressBar();
                    }
                    cMMusicCallback2.operationResult(orderResult2);
                } catch (IOException e) {
                    e.printStackTrace();
                    orderResult2.setResCode("901");
                    orderResult2.setResMsg("网络异常");
                    if (context2 instanceof CMMusicActivity) {
                        ((CMMusicActivity) context2).hideProgressBar();
                    }
                    cMMusicCallback2.operationResult(orderResult2);
                } catch (XmlPullParserException e2) {
                    e2.printStackTrace();
                    orderResult2.setResCode("902");
                    orderResult2.setResMsg("返回参数异常");
                    if (context2 instanceof CMMusicActivity) {
                        ((CMMusicActivity) context2).hideProgressBar();
                    }
                    cMMusicCallback2.operationResult(orderResult2);
                } catch (Throwable th) {
                    Throwable th2 = th;
                    if (context2 instanceof CMMusicActivity) {
                        ((CMMusicActivity) context2).hideProgressBar();
                    }
                    cMMusicCallback2.operationResult(orderResult2);
                    throw th2;
                }
            }
        }.start();
    }

    static String encodeBase64(byte[] bArr) {
        return Base64.encodeToString(bArr, 10);
    }

    private static byte[] decodeBase64(String str) {
        if (str == null) {
            str = "";
        }
        return Base64.decode(str, 10);
    }

    private static InputStream getResultStream(Object obj) {
        return new ByteArrayInputStream(decodeBase64((String) obj));
    }
}
