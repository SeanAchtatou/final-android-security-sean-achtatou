package com.cmsc.cmmusic.common.data;

import java.util.ArrayList;

public class PromptRsp extends Result {
    private ArrayList<String> busNames;

    public ArrayList<String> getBusNames() {
        return this.busNames;
    }

    public void setBusNames(ArrayList<String> arrayList) {
        this.busNames = arrayList;
    }
}
