package com.cmsc.cmmusic.common.data;

public class SmsLoginAuthResult extends Result {
    private String token;

    public String getToken() {
        return this.token;
    }

    public void setToken(String str) {
        this.token = str;
    }

    public String toString() {
        return "SmsLoginAuthResult [token=" + this.token + ", getResCode()=" + getResCode() + ", getResMsg()=" + getResMsg() + "]";
    }
}
