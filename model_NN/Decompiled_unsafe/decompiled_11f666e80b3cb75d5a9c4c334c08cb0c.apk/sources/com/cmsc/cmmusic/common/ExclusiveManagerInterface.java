package com.cmsc.cmmusic.common;

import android.content.Context;
import android.os.Bundle;
import com.cmsc.cmmusic.common.MiguSdkUtil;
import com.cmsc.cmmusic.common.data.OrderResult;
import java.io.IOException;

public class ExclusiveManagerInterface {
    public static void exclusiveOnce(Context context, String str, String str2, CMMusicCallback<OrderResult> cMMusicCallback) {
        Bundle bundle = new Bundle();
        bundle.putString("serviceId", str);
        bundle.putString("orderType", "net");
        bundle.putString("definedseq", str2);
        CMMusicActivity.showActivityDefault(context, bundle, 19, cMMusicCallback);
    }

    public static void exclusiveTimesOrder(Context context, String str, String str2, String str3, String str4, String str5, CMMusicCallback<OrderResult> cMMusicCallback) {
        MiguSdkUtil.TL.init();
        EnablerInterface.exclusiveTimesOrder(context, str, str2, str3, str4, str5, cMMusicCallback);
    }

    public static String getServiceEx(Context context, String str) {
        try {
            return HttpPostCore.httpConnectionToString(context, "http://218.200.227.123:95/sdkServer/1.0/cp/serviceEx", EnablerInterface.buildRequsetXml("<contentId>" + str + "</contentId>"));
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
