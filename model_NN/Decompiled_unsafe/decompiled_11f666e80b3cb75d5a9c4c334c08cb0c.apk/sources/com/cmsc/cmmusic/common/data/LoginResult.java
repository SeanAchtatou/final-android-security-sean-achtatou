package com.cmsc.cmmusic.common.data;

import java.util.ArrayList;

public class LoginResult extends Result {
    private ArrayList<AccountInfo> accountInfoList;
    private PaymentUserInfo userInfo;

    public PaymentUserInfo getUserInfo() {
        return this.userInfo;
    }

    public void setUserInfo(PaymentUserInfo paymentUserInfo) {
        this.userInfo = paymentUserInfo;
    }

    public ArrayList<AccountInfo> getAccountInfoList() {
        return this.accountInfoList;
    }

    public void setAccountInfoList(ArrayList<AccountInfo> arrayList) {
        this.accountInfoList = arrayList;
    }

    public String toString() {
        return "LoginResult [userInfo=" + this.userInfo + ", accountInfoList=" + this.accountInfoList + ", getResCode()=" + getResCode() + ", getResMsg()=" + getResMsg() + "]";
    }
}
