package com.cmsc.cmmusic.common;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.cmsc.cmmusic.common.data.OrderResult;
import com.cmsc.cmmusic.common.data.ServiceExInfo;
import java.io.IOException;
import java.io.InputStream;

final class ExclusiveOrderView extends LinearLayout {
    private ImageView back;
    private Button btnSure;
    private String channelCode;
    protected CMMusicActivity cmMusicActivity;
    private Context context;
    /* access modifiers changed from: private */
    public String definedseq;
    private LinearLayout layoutAmount;
    private LinearLayout layoutAmountAndPhoneNumber;
    private LinearLayout layoutAppName;
    private LinearLayout layoutButton;
    private LinearLayout layoutIcon;
    private LinearLayout layoutNoticeContent;
    private LinearLayout layoutNoticeTitle;
    private LinearLayout layoutPhoneNumber;
    private LinearLayout layoutSendMode;
    private LinearLayout layoutServerName;
    private LinearLayout layoutTitle1;
    private LinearLayout layoutTitle2;
    private String noticeAfter;
    private int orderType;
    private TextView sendMode;
    /* access modifiers changed from: private */
    public ServiceExInfo serviceExInfo;

    public ExclusiveOrderView(Context context2) {
        super(context2);
    }

    public ExclusiveOrderView(Context context2, AttributeSet attributeSet) {
        super(context2, attributeSet);
    }

    public ExclusiveOrderView(Context context2, Bundle bundle) {
        super(context2);
        this.cmMusicActivity = (CMMusicActivity) context2;
        setOrientation(1);
        int outerHeight = getOuterHeight();
        int outerwidth = getOuterwidth();
        int density = getDensity();
        this.serviceExInfo = (ServiceExInfo) bundle.getSerializable("serviceExInfo");
        String appName = this.serviceExInfo.getAppName();
        this.serviceExInfo.getServiceName();
        this.serviceExInfo.getProdType();
        String valueOf = String.valueOf(((double) Integer.parseInt(this.serviceExInfo.getPrice())) / 100.0d);
        String noticeBefore = this.serviceExInfo.getNoticeBefore();
        this.noticeAfter = this.serviceExInfo.getNoticeAfter();
        this.serviceExInfo.getCpName();
        this.serviceExInfo.getCpId();
        this.serviceExInfo.getCopyright_id();
        String copyrightName = this.serviceExInfo.getCopyrightName();
        this.serviceExInfo.getChannelSubName();
        String phoneNum = this.serviceExInfo.getPhoneNum();
        String string = bundle.getString("orderType");
        if (string.equals("sms")) {
            this.orderType = 0;
        } else if (string.equals("net")) {
            this.orderType = 1;
        }
        this.channelCode = bundle.getString("Channel");
        this.definedseq = bundle.getString("definedseq");
        View view = new View(context2);
        view.setLayoutParams(new LinearLayout.LayoutParams(-1, density));
        view.setBackgroundColor(537594635);
        View view2 = new View(context2);
        view2.setLayoutParams(new LinearLayout.LayoutParams(-1, density));
        view2.setBackgroundColor(572662306);
        View view3 = new View(context2);
        view3.setLayoutParams(new LinearLayout.LayoutParams(-1, density));
        view3.setBackgroundColor(572662306);
        View view4 = new View(context2);
        view4.setLayoutParams(new LinearLayout.LayoutParams(-1, density));
        view4.setBackgroundColor(572662306);
        this.layoutIcon = new LinearLayout(context2);
        this.layoutIcon.setPadding(outerwidth / 5, density * 3, outerwidth / 5, density * 3);
        this.layoutIcon.setOrientation(0);
        this.layoutIcon.setBackgroundColor(572662306);
        this.layoutIcon.setLayoutParams(new LinearLayout.LayoutParams(-1, outerHeight / 14));
        this.layoutIcon.setGravity(17);
        try {
            ImageView imageView = new ImageView(context2);
            InputStream open = context2.getAssets().open("logo_and.png");
            BitmapDrawable bitmapDrawable = new BitmapDrawable(BitmapFactory.decodeStream(open));
            open.close();
            imageView.setBackgroundDrawable(bitmapDrawable);
            this.layoutIcon.addView(imageView);
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.layoutTitle1 = new LinearLayout(context2);
        this.layoutTitle1.setLayoutParams(new LinearLayout.LayoutParams(-1, outerHeight / 14));
        this.layoutTitle1.setOrientation(0);
        try {
            this.back = new ImageView(context2);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams((outerHeight / 14) - (density * 6), -2);
            layoutParams.setMargins(density * 10, density * 3, ((outerwidth / 2) - (outerwidth / 4)) - (density * 5), density * 3);
            this.back.setLayoutParams(layoutParams);
            InputStream open2 = context2.getAssets().open("back_green.png");
            BitmapDrawable bitmapDrawable2 = new BitmapDrawable(BitmapFactory.decodeStream(open2));
            open2.close();
            this.back.setBackgroundDrawable(bitmapDrawable2);
            initBackButton(context2);
            this.layoutTitle1.addView(this.back);
            TextView textView = new TextView(context2);
            textView.setTextSize(20.0f);
            textView.setGravity(17);
            textView.setText("话费支付");
            this.layoutTitle1.addView(textView);
        } catch (IOException e2) {
            e2.printStackTrace();
        }
        this.layoutTitle2 = new LinearLayout(context2);
        this.layoutTitle2.setBackgroundColor(572662306);
        this.layoutTitle2.setLayoutParams(new LinearLayout.LayoutParams(-1, outerHeight / 14));
        this.layoutTitle2.setGravity(16);
        TextView textView2 = new TextView(context2);
        textView2.setTextSize(16.0f);
        textView2.setText("购买产品");
        this.layoutTitle2.addView(textView2);
        this.layoutServerName = new LinearLayout(context2);
        this.layoutServerName.setLayoutParams(new LinearLayout.LayoutParams(-1, outerHeight / 14));
        this.layoutServerName.setOrientation(0);
        LinearLayout linearLayout = new LinearLayout(context2);
        linearLayout.setLayoutParams(new LinearLayout.LayoutParams(outerwidth / 2, outerHeight / 14));
        linearLayout.setOrientation(0);
        linearLayout.setGravity(16);
        LinearLayout linearLayout2 = new LinearLayout(context2);
        linearLayout2.setLayoutParams(new LinearLayout.LayoutParams(outerwidth / 2, outerHeight / 14));
        linearLayout2.setOrientation(0);
        linearLayout2.setGravity(16);
        TextView textView3 = new TextView(context2);
        textView3.setText("商品：");
        TextView textView4 = new TextView(context2);
        textView4.setTextColor(-570480896);
        textView4.setText(copyrightName);
        TextView textView5 = new TextView(context2);
        textView5.setText("价格：");
        TextView textView6 = new TextView(context2);
        textView6.setText(String.valueOf(valueOf) + "元");
        linearLayout.addView(textView3);
        linearLayout.addView(textView4);
        linearLayout2.addView(textView5);
        linearLayout2.addView(textView6);
        this.layoutServerName.addView(linearLayout);
        this.layoutServerName.addView(linearLayout2);
        this.layoutAppName = new LinearLayout(context2);
        this.layoutAppName.setLayoutParams(new LinearLayout.LayoutParams(-1, outerHeight / 14));
        TextView textView7 = new TextView(context2);
        TextView textView8 = new TextView(context2);
        textView7.setText("所属应用：");
        textView8.setText(appName);
        this.layoutAppName.addView(textView7);
        this.layoutAppName.addView(textView8);
        this.layoutAmountAndPhoneNumber = new LinearLayout(context2);
        this.layoutAmountAndPhoneNumber.setLayoutParams(new LinearLayout.LayoutParams(-1, outerHeight / 14));
        this.layoutAmount = new LinearLayout(context2);
        this.layoutAmount.setOrientation(0);
        this.layoutAmount.setLayoutParams(new LinearLayout.LayoutParams(outerwidth / 2, -1));
        this.layoutAmount.setGravity(16);
        this.layoutPhoneNumber = new LinearLayout(context2);
        this.layoutPhoneNumber.setOrientation(0);
        this.layoutPhoneNumber.setLayoutParams(new LinearLayout.LayoutParams(outerwidth / 2, -1));
        this.layoutPhoneNumber.setGravity(16);
        TextView textView9 = new TextView(context2);
        TextView textView10 = new TextView(context2);
        TextView textView11 = new TextView(context2);
        TextView textView12 = new TextView(context2);
        textView9.setText("话费支付金额：");
        textView10.setText(String.valueOf(valueOf) + "元");
        textView11.setText("支付手机：");
        textView11.setTextSize(10.0f);
        textView12.setText(phoneNum);
        textView12.setTextSize(10.0f);
        this.layoutAmount.addView(textView9);
        this.layoutAmount.addView(textView10);
        this.layoutPhoneNumber.addView(textView11);
        this.layoutPhoneNumber.addView(textView12);
        this.layoutAmountAndPhoneNumber.addView(this.layoutAmount);
        this.layoutAmountAndPhoneNumber.addView(this.layoutPhoneNumber);
        this.layoutButton = new LinearLayout(context2);
        this.layoutButton.setLayoutParams(new LinearLayout.LayoutParams(-1, outerHeight / 7));
        this.layoutButton.setPadding(density * 10, density * 5, density * 10, density * 5);
        this.layoutButton.setBackgroundColor(285410051);
        this.layoutButton.setGravity(17);
        this.btnSure = new Button(context2);
        try {
            InputStream open3 = context2.getAssets().open("button_green.png");
            BitmapDrawable bitmapDrawable3 = new BitmapDrawable(BitmapFactory.decodeStream(open3));
            open3.close();
            this.btnSure.setBackgroundDrawable(bitmapDrawable3);
        } catch (IOException e3) {
            e3.printStackTrace();
        }
        this.btnSure.setText("确认支付");
        this.btnSure.setTextColor(-1);
        this.btnSure.setTextSize(32.0f);
        this.layoutButton.addView(this.btnSure);
        initBtnSure(context2);
        this.layoutNoticeTitle = new LinearLayout(context2);
        this.layoutNoticeTitle.setLayoutParams(new LinearLayout.LayoutParams(-1, outerHeight / 14));
        this.layoutNoticeTitle.setBackgroundColor(572662306);
        this.layoutNoticeTitle.setGravity(16);
        TextView textView13 = new TextView(context2);
        textView13.setText("温馨提示");
        this.layoutNoticeTitle.addView(textView13);
        this.layoutNoticeContent = new LinearLayout(context2);
        this.layoutNoticeContent.setLayoutParams(new LinearLayout.LayoutParams(-1, outerHeight / 7));
        this.layoutNoticeContent.setGravity(16);
        TextView textView14 = new TextView(context2);
        textView14.setText(noticeBefore);
        this.layoutNoticeContent.addView(textView14);
        this.layoutSendMode = new LinearLayout(context2);
        this.layoutSendMode.setLayoutParams(new LinearLayout.LayoutParams(-1, outerHeight / 7));
        this.sendMode = new TextView(context2);
        addView(this.layoutIcon);
        addView(this.layoutTitle1);
        addView(this.layoutTitle2);
        addView(this.layoutServerName);
        addView(view);
        addView(this.layoutAppName);
        addView(view2);
        addView(this.layoutAmountAndPhoneNumber);
        addView(this.layoutButton);
        addView(this.layoutNoticeTitle);
        addView(this.layoutNoticeContent);
        addView(view3);
        addView(this.layoutSendMode);
    }

    private int getDensity() {
        return (int) this.cmMusicActivity.getResources().getDisplayMetrics().density;
    }

    private int getOuterwidth() {
        return this.cmMusicActivity.getResources().getDisplayMetrics().widthPixels;
    }

    private int getOuterHeight() {
        return this.cmMusicActivity.getResources().getDisplayMetrics().heightPixels - ((int) (25.0f * this.cmMusicActivity.getResources().getDisplayMetrics().density));
    }

    private void initBtnSure(Context context2) {
        this.btnSure.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                EnablerInterface.exclusiveTimesOrder(ExclusiveOrderView.this.cmMusicActivity, ExclusiveOrderView.this.serviceExInfo.getCopyright_id(), ExclusiveOrderView.this.serviceExInfo.getServiceId(), ExclusiveOrderView.this.serviceExInfo.getPrice(), ExclusiveOrderView.this.serviceExInfo.getMonLevel(), ExclusiveOrderView.this.definedseq, new CMMusicCallback<OrderResult>() {
                    public void operationResult(OrderResult orderResult) {
                        ExclusiveOrderView.this.cmMusicActivity.closeActivity(orderResult);
                    }
                });
            }
        });
    }

    private void initBackButton(Context context2) {
        this.back.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                ExclusiveOrderView.this.cmMusicActivity.closeActivity(null);
            }
        });
    }
}
