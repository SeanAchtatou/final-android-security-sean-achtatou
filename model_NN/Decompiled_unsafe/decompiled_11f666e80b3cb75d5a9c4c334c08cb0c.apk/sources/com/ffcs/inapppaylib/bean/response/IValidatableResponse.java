package com.ffcs.inapppaylib.bean.response;

public class IValidatableResponse extends BaseResponse {
    private String app_name;
    private String billing_type;
    private String billing_type_name;
    private boolean lowTariff;
    private String low_price;
    private String order_no;
    private String pay_code;
    private String pay_code_name;
    private String pay_type;
    private String phone;
    private String price;
    private String sms_num;
    private String sp_name;
    private String tel_phone;
    private String verify_code;
    private String verify_method;

    public String getApp_name() {
        return this.app_name;
    }

    public String getBilling_type() {
        return this.billing_type;
    }

    public String getBilling_type_name() {
        return this.billing_type_name;
    }

    public boolean getLowTariff() {
        return this.lowTariff;
    }

    public String getLow_price() {
        return this.low_price;
    }

    public String getOrder_no() {
        return this.order_no;
    }

    public String getPay_code() {
        return this.pay_code;
    }

    public String getPay_code_name() {
        return this.pay_code_name;
    }

    public String getPay_type() {
        return this.pay_type;
    }

    public String getPhone() {
        return this.phone;
    }

    public String getPrice() {
        return this.price;
    }

    public String getSms_num() {
        return this.sms_num;
    }

    public String getSp_name() {
        return this.sp_name;
    }

    public String getTel_phone() {
        return this.tel_phone;
    }

    public String getType() {
        return this.pay_type;
    }

    public String getVerify_code() {
        return this.verify_code;
    }

    public String getVerify_method() {
        return this.verify_method;
    }

    public void setApp_name(String str) {
        this.app_name = str;
    }

    public void setBilling_type(String str) {
        this.billing_type = str;
    }

    public void setBilling_type_name(String str) {
        this.billing_type_name = str;
    }

    public void setLowTariff(boolean z) {
        this.lowTariff = z;
    }

    public void setLow_price(String str) {
        this.low_price = str;
    }

    public void setOrder_no(String str) {
        this.order_no = str;
    }

    public void setPay_code(String str) {
        this.pay_code = str;
    }

    public void setPay_code_name(String str) {
        this.pay_code_name = str;
    }

    public void setPay_type(String str) {
        this.pay_type = str;
    }

    public void setPhone(String str) {
        this.phone = str;
    }

    public void setPrice(String str) {
        this.price = str;
    }

    public void setSms_num(String str) {
        this.sms_num = str;
    }

    public void setSp_name(String str) {
        this.sp_name = str;
    }

    public void setTel_phone(String str) {
        this.tel_phone = str;
    }

    public void setType(String str) {
        this.pay_type = str;
    }

    public void setVerify_code(String str) {
        this.verify_code = str;
    }

    public void setVerify_method(String str) {
        this.verify_method = str;
    }
}
