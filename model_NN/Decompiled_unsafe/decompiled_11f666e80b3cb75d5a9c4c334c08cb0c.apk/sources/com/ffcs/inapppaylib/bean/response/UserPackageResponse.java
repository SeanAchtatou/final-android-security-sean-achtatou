package com.ffcs.inapppaylib.bean.response;

import java.util.ArrayList;
import java.util.List;

public class UserPackageResponse extends EmpResponse {
    private List<UserPackage> user_package = new ArrayList();

    public List<UserPackage> getUser_package() {
        return this.user_package;
    }

    public void setUser_package(List<UserPackage> list) {
        this.user_package = list;
    }
}
