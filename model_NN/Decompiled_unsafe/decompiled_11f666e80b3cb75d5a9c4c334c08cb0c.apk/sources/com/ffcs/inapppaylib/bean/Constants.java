package com.ffcs.inapppaylib.bean;

public class Constants {
    public static final String IMAGE_DIR = "iapImageRes/";
    public static final String KEY_PAY_RESPONSE = "PayResponse";
    public static final int LOW_TARIFF = 311;
    public static final int LOW_TARIFF_PAY_SUCCESS = 312;
    public static final int OP_PAY = 3;
    public static final int OP_SUB = 0;
    public static final int OP_UNSUB = 1;
    public static final int RESPONSE_FAILURE = 1;
    public static final int RESPONSE_SUCCESS = 0;
    public static final int RESP_NO_PHONENUMBER = 210;
    public static final int RESP_VCODE_ERR = 209;
    public static final int RESULT_DLG_CLOSE = 296;
    public static final int RESULT_NO_CT = 87;
    public static final int RESULT_PAY_FAILURE = 293;
    public static final int RESULT_PAY_SUCCESS = 292;
    public static final int RESULT_VALIDATE_FAILURE = 294;
}
