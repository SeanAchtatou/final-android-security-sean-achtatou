package com.ffcs.inapppaylib.util;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import java.io.InputStream;

public class StreamPaser {
    public static Bitmap InputStream2Bitmap(InputStream inputStream) {
        return BitmapFactory.decodeStream(inputStream);
    }

    public static Drawable InputStream2Drawable(InputStream inputStream) {
        return bitmap2Drawable(InputStream2Bitmap(inputStream));
    }

    public static Drawable bitmap2Drawable(Bitmap bitmap) {
        return new BitmapDrawable(bitmap);
    }
}
