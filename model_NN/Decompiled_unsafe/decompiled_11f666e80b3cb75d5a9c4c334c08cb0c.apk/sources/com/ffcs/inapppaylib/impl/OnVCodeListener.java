package com.ffcs.inapppaylib.impl;

import com.ffcs.inapppaylib.bean.response.VerifyResponse;

public interface OnVCodeListener {
    void onRefreshVcoBtnFailure(VerifyResponse verifyResponse);

    void onRefreshVcoBtnSuccess(VerifyResponse verifyResponse);
}
