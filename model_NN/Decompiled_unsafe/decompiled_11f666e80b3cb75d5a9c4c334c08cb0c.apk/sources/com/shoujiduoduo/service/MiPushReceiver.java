package com.shoujiduoduo.service;

import android.content.Context;
import android.text.TextUtils;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.util.ab;
import com.xiaomi.mipush.sdk.MiPushClient;
import com.xiaomi.mipush.sdk.MiPushCommandMessage;
import com.xiaomi.mipush.sdk.MiPushMessage;
import com.xiaomi.mipush.sdk.PushMessageReceiver;
import java.util.List;

public class MiPushReceiver extends PushMessageReceiver {

    /* renamed from: a  reason: collision with root package name */
    private String f1451a;
    private long b = -1;
    private String c;
    private String d;
    private String e;
    private String f;
    private String g;

    public void onNotificationMessageClicked(Context context, MiPushMessage miPushMessage) {
        this.c = miPushMessage.getContent();
        a.a("MiPushReceiver", "content:" + this.c);
        a.a("MiPushReceiver", "title:" + miPushMessage.getTitle());
        if (!TextUtils.isEmpty(miPushMessage.getTopic())) {
            this.d = miPushMessage.getTopic();
        } else if (!TextUtils.isEmpty(miPushMessage.getAlias())) {
            this.e = miPushMessage.getAlias();
        }
        if (!TextUtils.isEmpty(this.c)) {
            try {
                new ab(context).a(this.c);
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }

    public void onNotificationMessageArrived(Context context, MiPushMessage miPushMessage) {
        super.onNotificationMessageArrived(context, miPushMessage);
    }

    public void onCommandResult(Context context, MiPushCommandMessage miPushCommandMessage) {
        String str;
        String command = miPushCommandMessage.getCommand();
        List<String> commandArguments = miPushCommandMessage.getCommandArguments();
        String str2 = (commandArguments == null || commandArguments.size() <= 0) ? null : commandArguments.get(0);
        if (commandArguments == null || commandArguments.size() <= 1) {
            str = null;
        } else {
            str = commandArguments.get(1);
        }
        if (MiPushClient.COMMAND_REGISTER.equals(command)) {
            if (miPushCommandMessage.getResultCode() == 0) {
                a.a("MiPushReceiver", "onCommandResult:register, success, regid:" + str2);
                MiPushClient.subscribe(context, "music_album_new", null);
                this.f1451a = str2;
            }
        } else if (MiPushClient.COMMAND_SET_ALIAS.equals(command)) {
            if (miPushCommandMessage.getResultCode() == 0) {
                this.e = str2;
            }
        } else if (MiPushClient.COMMAND_UNSET_ALIAS.equals(command)) {
            if (miPushCommandMessage.getResultCode() == 0) {
                this.e = str2;
            }
        } else if (MiPushClient.COMMAND_SUBSCRIBE_TOPIC.equals(command)) {
            if (miPushCommandMessage.getResultCode() == 0) {
                a.a("MiPushReceiver", "onCommandResult:subscribe topic, success, topic:" + str2);
                this.d = str2;
            }
        } else if (MiPushClient.COMMAND_UNSUBSCRIBE_TOPIC.equals(command)) {
            if (miPushCommandMessage.getResultCode() == 0) {
                this.d = str2;
            }
        } else if (MiPushClient.COMMAND_SET_ACCEPT_TIME.equals(command) && miPushCommandMessage.getResultCode() == 0) {
            this.f = str2;
            this.g = str;
        }
    }
}
