package com.shoujiduoduo.service;

import android.content.Context;
import com.igexin.sdk.GTIntentService;
import com.igexin.sdk.message.GTCmdMessage;
import com.igexin.sdk.message.GTTransmitMessage;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.util.ab;
import com.shoujiduoduo.util.ah;

public class DuoDuoIntentService extends GTIntentService {
    public void onReceiveServicePid(Context context, int i) {
        a.a("DuoDuoIntentService", "onReceiveServicePid, " + i);
    }

    public void onReceiveMessageData(Context context, GTTransmitMessage gTTransmitMessage) {
        String str = null;
        if (!(gTTransmitMessage == null || gTTransmitMessage.getPayload() == null)) {
            str = new String(gTTransmitMessage.getPayload());
        }
        a.a("DuoDuoIntentService", "onReceiveMessageData, " + str);
        if (!ah.c(str)) {
            try {
                new ab(context).a(str);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void onReceiveClientId(Context context, String str) {
        a.b("DuoDuoIntentService", "onReceiveClientId -> clientid = " + str);
    }

    public void onReceiveOnlineState(Context context, boolean z) {
    }

    public void onReceiveCommandResult(Context context, GTCmdMessage gTCmdMessage) {
    }
}
