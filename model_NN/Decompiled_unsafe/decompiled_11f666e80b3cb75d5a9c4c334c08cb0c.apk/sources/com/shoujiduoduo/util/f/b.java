package com.shoujiduoduo.util.f;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

/* compiled from: ID3V1 */
public class b {

    /* renamed from: a  reason: collision with root package name */
    private String f2296a;
    private String b;
    private String c;
    private String d;
    private String e;
    private byte f;
    private byte g;
    private byte h;
    private File i;
    private byte[] j;

    public b(File file) {
        this.i = file;
    }

    public byte[] a() {
        if (this.j != null) {
            return this.j;
        }
        return null;
    }

    public void b() throws d {
        try {
            RandomAccessFile randomAccessFile = new RandomAccessFile(this.i, "r");
            randomAccessFile.seek(randomAccessFile.length() - 128);
            byte[] bArr = new byte[3];
            randomAccessFile.read(bArr);
            if (!new String(bArr).equals("TAG")) {
                throw new d("No ID3V1 found");
            }
            byte[] bArr2 = new byte[125];
            randomAccessFile.read(bArr2);
            a(bArr2);
            if (this.j == null) {
                this.j = new byte[128];
                System.arraycopy(bArr, 0, this.j, 0, 3);
                System.arraycopy(bArr2, 0, this.j, 3, 125);
            }
        } catch (FileNotFoundException e2) {
            e2.printStackTrace();
        } catch (IOException e3) {
            e3.printStackTrace();
        }
    }

    private void a(byte[] bArr) {
        this.f2296a = new String(bArr, 0, 30).trim();
        this.b = new String(bArr, 30, 30).trim();
        this.c = new String(bArr, 60, 30).trim();
        this.e = new String(bArr, 90, 4);
        this.d = new String(bArr, 94, 28).trim();
        this.f = bArr[122];
        this.g = bArr[123];
        this.h = bArr[124];
    }

    public String toString() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("标题=" + this.f2296a + "\n");
        stringBuffer.append("歌手=" + this.b + "\n");
        stringBuffer.append("专辑=" + this.c + "\n");
        stringBuffer.append("年代=" + this.e + "\n");
        stringBuffer.append("注释=" + this.d + "\n");
        return stringBuffer.toString();
    }
}
