package com.shoujiduoduo.util.f;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/* compiled from: MP3File */
public class e {

    /* renamed from: a  reason: collision with root package name */
    private File f2298a;
    private long b;
    private c c;
    private b d;
    private a e;

    public e(String str) throws d, IOException {
        this.f2298a = new File(str);
        if (this.f2298a.exists()) {
            this.b = this.f2298a.length();
            d();
        }
    }

    private void d() throws IOException, d {
        if (this.c == null) {
            this.c = new c(this.f2298a);
            try {
                this.c.a();
            } catch (d e2) {
                this.c = null;
                e2.printStackTrace();
            }
        }
        if (this.e == null) {
            this.e = new a(this);
        }
        if (this.d == null) {
            this.d = new b(this.f2298a);
            try {
                this.d.b();
            } catch (d e3) {
                this.d = null;
                e3.printStackTrace();
            }
        }
    }

    public String a() {
        if (this.f2298a != null) {
            return this.f2298a.getPath();
        }
        return null;
    }

    public int b() {
        if (this.c != null) {
            return this.c.b() + 10;
        }
        return 0;
    }

    public long c() {
        return this.b;
    }

    public String a(long j, long j2, String str) throws FileNotFoundException, IOException {
        int read;
        FileInputStream fileInputStream = new FileInputStream(new File(this.f2298a.getPath()));
        FileOutputStream fileOutputStream = new FileOutputStream(new File(str));
        int b2 = b();
        if (b2 > 0) {
            byte[] bArr = new byte[b2];
            fileInputStream.read(bArr);
            fileOutputStream.write(bArr);
        }
        long a2 = this.e.a(j);
        long a3 = this.e.a(j2);
        fileInputStream.skip(a2 - ((long) b2));
        byte[] bArr2 = new byte[4096];
        for (int i = 0; ((long) i) < a3 - a2 && (read = fileInputStream.read(bArr2)) != -1; i += read) {
            fileOutputStream.write(bArr2, 0, read);
        }
        if (this.d != null) {
            fileOutputStream.write(this.d.a());
        }
        fileOutputStream.flush();
        fileInputStream.close();
        fileOutputStream.close();
        return str;
    }
}
