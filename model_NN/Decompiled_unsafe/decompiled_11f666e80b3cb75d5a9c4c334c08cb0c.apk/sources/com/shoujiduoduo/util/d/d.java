package com.shoujiduoduo.util.d;

import java.security.MessageDigest;
import org.apache.mina.proxy.handlers.socks.SocksProxyConstants;

/* compiled from: MD5 */
public class d {
    public static String a(String str) {
        try {
            return a(MessageDigest.getInstance("MD5").digest(str.getBytes()));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static String a(byte[] bArr) {
        StringBuffer stringBuffer = new StringBuffer();
        for (byte a2 : bArr) {
            stringBuffer.append(a(a2));
        }
        return stringBuffer.toString();
    }

    private static String a(byte b) {
        if (b < 0) {
            b &= SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD;
        }
        return (Integer.toHexString(b / 16) + Integer.toHexString(b % 16)).toUpperCase();
    }
}
