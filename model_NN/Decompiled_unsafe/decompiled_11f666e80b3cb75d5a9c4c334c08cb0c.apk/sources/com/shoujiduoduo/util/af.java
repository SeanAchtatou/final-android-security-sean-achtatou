package com.shoujiduoduo.util;

import android.content.Context;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;
import android.text.TextUtils;
import android.widget.EditText;
import com.shoujiduoduo.base.a.a;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/* compiled from: SmsContentUtil */
public class af extends ContentObserver {

    /* renamed from: a  reason: collision with root package name */
    private Context f2184a = null;
    private String b = "";
    private EditText c = null;
    private int d = 6;
    private String e;

    public af(Context context, Handler handler, EditText editText, String str) {
        super(handler);
        this.f2184a = context;
        this.c = editText;
        this.e = str;
        this.d = 6;
    }

    public af(Context context, Handler handler, EditText editText, String str, int i) {
        super(handler);
        this.f2184a = context;
        this.c = editText;
        this.e = str;
        this.d = i;
    }

    public void onChange(boolean z) {
        super.onChange(z);
        try {
            Cursor query = this.f2184a.getContentResolver().query(Uri.parse("content://sms/inbox"), new String[]{"_id", "address", "body", "read", "date"}, TextUtils.isEmpty(this.e) ? "read=?" : "address=? and read=?", TextUtils.isEmpty(this.e) ? new String[]{"0"} : new String[]{this.e, "0"}, "date desc");
            if (query != null) {
                query.moveToFirst();
                if (query.moveToFirst()) {
                    String string = query.getString(query.getColumnIndex("body"));
                    a.a("SmsContentUtil", "smsbody:" + string);
                    Matcher matcher = Pattern.compile("[0-9]{" + this.d + "}").matcher(string.toString());
                    if (matcher.find()) {
                        this.b = matcher.group(0);
                        this.c.setText(this.b);
                        this.c.setSelection(this.c.getText().toString().trim().length());
                    }
                }
            }
        } catch (Exception e2) {
            e2.printStackTrace();
            a.e("SmsContentUtil", "未获取读取短信权限, 部分系统限制了读取通知类短信权限，如MIUI");
        }
    }
}
