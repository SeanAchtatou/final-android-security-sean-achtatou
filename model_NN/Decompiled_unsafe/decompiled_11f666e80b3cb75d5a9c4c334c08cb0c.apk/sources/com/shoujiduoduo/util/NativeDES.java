package com.shoujiduoduo.util;

import com.shoujiduoduo.base.a.a;

public class NativeDES {

    /* renamed from: a  reason: collision with root package name */
    private static String f2160a = "NativeDES";
    private static boolean b = y.a("url_encode");

    public native String Encrypt(String str);

    static {
        a.a(f2160a, "load url_encode lib, res:" + b);
    }

    public static boolean a() {
        return b;
    }
}
