package com.shoujiduoduo.util;

import com.shoujiduoduo.player.PlayerService;

/* compiled from: PlayerServiceUtil */
public class aa {
    private static aa b = null;

    /* renamed from: a  reason: collision with root package name */
    private PlayerService f2171a;

    private aa() {
    }

    public static aa a() {
        if (b == null) {
            b = new aa();
        }
        return b;
    }

    public void a(PlayerService playerService) {
        this.f2171a = playerService;
    }

    public PlayerService b() {
        return this.f2171a;
    }

    public void c() {
        this.f2171a = null;
        b = null;
    }
}
