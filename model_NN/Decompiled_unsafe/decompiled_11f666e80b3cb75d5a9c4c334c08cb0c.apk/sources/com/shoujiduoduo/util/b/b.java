package com.shoujiduoduo.util.b;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import com.shoujiduoduo.util.b.c;

/* compiled from: RequestHandler */
public class b {

    /* renamed from: a  reason: collision with root package name */
    private Handler f2211a;

    public b() {
        if (Looper.myLooper() != null) {
            this.f2211a = new Handler() {
                public void handleMessage(Message message) {
                    b.this.a(message);
                }
            };
        }
    }

    public void a(c.b bVar) {
    }

    public void b(c.b bVar) {
    }

    public void a() {
    }

    public void b() {
    }

    /* access modifiers changed from: protected */
    public void c(c.b bVar) {
        b(a(0, bVar));
    }

    /* access modifiers changed from: protected */
    public void d(c.b bVar) {
        b(a(1, bVar));
    }

    /* access modifiers changed from: protected */
    public void e(c.b bVar) {
        a(bVar);
    }

    /* access modifiers changed from: protected */
    public void f(c.b bVar) {
        b(bVar);
    }

    /* access modifiers changed from: protected */
    public void a(Message message) {
        switch (message.what) {
            case 0:
                e((c.b) message.obj);
                return;
            case 1:
                f((c.b) message.obj);
                return;
            case 2:
                b();
                return;
            case 3:
                a();
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void b(Message message) {
        if (this.f2211a != null) {
            this.f2211a.sendMessage(message);
        } else {
            a(message);
        }
    }

    /* access modifiers changed from: protected */
    public Message a(int i, Object obj) {
        if (this.f2211a != null) {
            return this.f2211a.obtainMessage(i, obj);
        }
        Message obtain = Message.obtain();
        obtain.what = i;
        obtain.obj = obj;
        return obtain;
    }

    public void g(c.b bVar) {
        if (bVar != null) {
            String a2 = bVar.a();
            if (a2 == null || (!a2.equals("0000") && !a2.equals("000000") && !a2.equals("0"))) {
                d(bVar);
            } else {
                c(bVar);
            }
        }
    }
}
