package com.shoujiduoduo.util.a;

import com.shoujiduoduo.util.a.d;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import org.apache.mina.proxy.handlers.socks.SocksProxyConstants;

/* compiled from: CheapWAV */
public class e extends d {

    /* renamed from: a  reason: collision with root package name */
    private int f2170a;
    private int[] b;
    private int[] h;
    private int[] i;
    private int j;
    private int k;
    private int l;
    private int m;
    private int n;

    public static d.a a() {
        return new d.a() {
            public d a() {
                return new e();
            }

            public String[] b() {
                return new String[]{"wav"};
            }
        };
    }

    public int b() {
        return this.f2170a;
    }

    public int c() {
        return this.l / 50;
    }

    public int[] d() {
        return this.i;
    }

    public int e() {
        return this.l;
    }

    public int f() {
        return this.m;
    }

    public void a(File file) throws FileNotFoundException, IOException {
        super.a(file);
        this.k = (int) this.g.length();
        if (this.k < 128) {
            throw new IOException("File too small to parse");
        }
        FileInputStream fileInputStream = new FileInputStream(this.g);
        byte[] bArr = new byte[12];
        fileInputStream.read(bArr, 0, 12);
        this.n += 12;
        if (bArr[0] == 82 && bArr[1] == 73 && bArr[2] == 70 && bArr[3] == 70 && bArr[8] == 87 && bArr[9] == 65 && bArr[10] == 86 && bArr[11] == 69) {
            this.m = 0;
            this.l = 0;
            while (this.n + 8 <= this.k) {
                byte[] bArr2 = new byte[8];
                fileInputStream.read(bArr2, 0, 8);
                this.n += 8;
                int i2 = ((bArr2[7] & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD) << 24) | ((bArr2[6] & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD) << 16) | ((bArr2[5] & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD) << 8) | (bArr2[4] & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD);
                if (bArr2[0] != 102 || bArr2[1] != 109 || bArr2[2] != 116 || bArr2[3] != 32) {
                    if (bArr2[0] == 100 && bArr2[1] == 97 && bArr2[2] == 116 && bArr2[3] == 97) {
                        if (this.m != 0 && this.l != 0) {
                            this.j = ((this.l * this.m) / 50) * 2;
                            this.f2170a = ((this.j - 1) + i2) / this.j;
                            this.b = new int[this.f2170a];
                            this.h = new int[this.f2170a];
                            this.i = new int[this.f2170a];
                            byte[] bArr3 = new byte[this.j];
                            int i3 = 0;
                            int i4 = 0;
                            while (i4 < i2) {
                                int i5 = this.j;
                                if (i4 + i5 > i2) {
                                    i4 = i2 - i5;
                                }
                                fileInputStream.read(bArr3, 0, i5);
                                int i6 = 0;
                                int i7 = 1;
                                while (i7 < i5) {
                                    int abs = Math.abs((int) bArr3[i7]);
                                    if (abs <= i6) {
                                        abs = i6;
                                    }
                                    i7 = (this.m * 4) + i7;
                                    i6 = abs;
                                }
                                this.b[i3] = this.n;
                                this.h[i3] = i5;
                                this.i[i3] = i6;
                                int i8 = i3 + 1;
                                this.n += i5;
                                i4 += i5;
                                if (this.f != null && !this.f.a((((double) i4) * 1.0d) / ((double) i2))) {
                                    break;
                                }
                                i3 = i8;
                            }
                        } else {
                            throw new IOException("Bad WAV file: data chunk before fmt chunk");
                        }
                    } else {
                        fileInputStream.skip((long) i2);
                        this.n += i2;
                    }
                } else if (i2 < 16 || i2 > 1024) {
                    throw new IOException("WAV file has bad fmt chunk");
                } else {
                    byte[] bArr4 = new byte[i2];
                    fileInputStream.read(bArr4, 0, i2);
                    this.n += i2;
                    byte b2 = ((bArr4[1] & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD) << 8) | (bArr4[0] & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD);
                    this.m = ((bArr4[3] & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD) << 8) | (bArr4[2] & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD);
                    this.l = (bArr4[4] & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD) | ((bArr4[7] & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD) << 24) | ((bArr4[6] & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD) << 16) | ((bArr4[5] & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD) << 8);
                    if (b2 != 1) {
                        throw new IOException("Unsupported WAV file encoding");
                    }
                }
            }
            return;
        }
        throw new IOException("Not a WAV file");
    }

    public void a(File file, int i2, int i3) throws IOException {
        file.createNewFile();
        FileInputStream fileInputStream = new FileInputStream(this.g);
        FileOutputStream fileOutputStream = new FileOutputStream(file);
        long j2 = 0;
        for (int i4 = 0; i4 < i3; i4++) {
            j2 += (long) this.h[i2 + i4];
        }
        long j3 = 36 + j2;
        long j4 = (long) this.l;
        long j5 = (long) (this.l * 2 * this.m);
        fileOutputStream.write(new byte[]{82, 73, 70, 70, (byte) ((int) (255 & j3)), (byte) ((int) ((j3 >> 8) & 255)), (byte) ((int) ((j3 >> 16) & 255)), (byte) ((int) ((j3 >> 24) & 255)), 87, 65, 86, 69, 102, 109, 116, 32, 16, 0, 0, 0, 1, 0, (byte) this.m, 0, (byte) ((int) (255 & j4)), (byte) ((int) ((j4 >> 8) & 255)), (byte) ((int) ((j4 >> 16) & 255)), (byte) ((int) ((j4 >> 24) & 255)), (byte) ((int) (255 & j5)), (byte) ((int) ((j5 >> 8) & 255)), (byte) ((int) ((j5 >> 16) & 255)), (byte) ((int) ((j5 >> 24) & 255)), (byte) (this.m * 2), 0, 16, 0, 100, 97, 116, 97, (byte) ((int) (255 & j2)), (byte) ((int) ((j2 >> 8) & 255)), (byte) ((int) ((j2 >> 16) & 255)), (byte) ((int) ((j2 >> 24) & 255))}, 0, 44);
        byte[] bArr = new byte[this.j];
        int i5 = 0;
        for (int i6 = 0; i6 < i3; i6++) {
            int i7 = this.b[i2 + i6] - i5;
            int i8 = this.h[i2 + i6];
            if (i7 >= 0) {
                if (i7 > 0) {
                    fileInputStream.skip((long) i7);
                    i5 += i7;
                }
                fileInputStream.read(bArr, 0, i8);
                fileOutputStream.write(bArr, 0, i8);
                i5 += i8;
            }
        }
        fileInputStream.close();
        fileOutputStream.close();
    }
}
