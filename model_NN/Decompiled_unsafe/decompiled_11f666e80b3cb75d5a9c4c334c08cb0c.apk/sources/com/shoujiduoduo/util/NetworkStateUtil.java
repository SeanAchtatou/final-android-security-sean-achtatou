package com.shoujiduoduo.util;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import com.igexin.sdk.PushConsts;
import com.shoujiduoduo.util.v;

public class NetworkStateUtil extends BroadcastReceiver {

    /* renamed from: a  reason: collision with root package name */
    private static final String[] f2162a = {"UNKNOWN", "2G", "3G", "4G"};
    private static final int[][] b = {new int[]{0, 0}, new int[]{1, 2}, new int[]{1, 1}, new int[]{2, 2}, new int[]{1, 3}, new int[]{2, 3}, new int[]{2, 3}, new int[]{1, 0}, new int[]{2, 2}, new int[]{2, 0}, new int[]{2, 0}, new int[]{1, 0}, new int[]{2, 3}, new int[]{3, 0}, new int[]{2, 0}, new int[]{2, 0}};
    private static NetworkStateUtil c = new NetworkStateUtil();
    private static boolean d;
    /* access modifiers changed from: private */
    public static volatile boolean e;
    /* access modifiers changed from: private */
    public static volatile boolean f;
    private static volatile int g;
    private static volatile String h = "UNKNOWN";
    private static volatile int i;
    private static volatile String j = "None";

    public static void a(Context context) {
        if (!d) {
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(PushConsts.ACTION_BROADCAST_NETWORK_CHANGE);
            try {
                context.registerReceiver(c, intentFilter);
                d = true;
            } catch (Exception e2) {
            }
            c(context);
        }
    }

    public static void b(Context context) {
        if (d) {
            try {
                context.unregisterReceiver(c);
            } catch (Exception e2) {
            }
            d = false;
        }
    }

    public static boolean a() {
        return e;
    }

    public static boolean b() {
        return a() && c() == 1;
    }

    public static int c() {
        return g;
    }

    public static String d() {
        return h;
    }

    public final void onReceive(final Context context, Intent intent) {
        v.a(v.a.NORMAL, new Runnable() {
            public void run() {
                boolean unused = NetworkStateUtil.e;
                boolean unused2 = NetworkStateUtil.f;
                NetworkStateUtil.c(context);
            }
        });
    }

    public static void c(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
        if (connectivityManager != null) {
            NetworkInfo[] networkInfoArr = null;
            try {
                networkInfoArr = connectivityManager.getAllNetworkInfo();
            } catch (Exception e2) {
            }
            if (networkInfoArr != null) {
                e = false;
                f = false;
                int i2 = 0;
                while (i2 < networkInfoArr.length) {
                    if (networkInfoArr[i2].isConnected()) {
                        e = true;
                        j = networkInfoArr[i2].getTypeName();
                        if (networkInfoArr[i2].getType() == 1) {
                            f = true;
                            h = "WIFI";
                            return;
                        } else if (networkInfoArr[i2].getType() == 0) {
                            int subtype = networkInfoArr[i2].getSubtype();
                            if (subtype < b.length) {
                                j = networkInfoArr[i2].getExtraInfo();
                                g = b[subtype][0];
                                i = b[subtype][1];
                                h = f2162a[g];
                                return;
                            }
                            g = 2;
                            h = "3G";
                            return;
                        } else {
                            h = "UNKNOWN";
                            return;
                        }
                    } else {
                        i2++;
                    }
                }
            }
        }
    }
}
