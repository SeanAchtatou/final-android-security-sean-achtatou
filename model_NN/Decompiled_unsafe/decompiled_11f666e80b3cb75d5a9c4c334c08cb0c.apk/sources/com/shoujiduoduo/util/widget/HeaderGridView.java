package com.shoujiduoduo.util.widget;

import android.content.Context;
import android.database.DataSetObservable;
import android.database.DataSetObserver;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.GridView;
import android.widget.ListAdapter;
import android.widget.WrapperListAdapter;
import java.util.ArrayList;
import java.util.Iterator;

public class HeaderGridView extends GridView {

    /* renamed from: a  reason: collision with root package name */
    private ArrayList<a> f2337a = new ArrayList<>();

    private static class a {

        /* renamed from: a  reason: collision with root package name */
        public ViewGroup f2338a;
        public Object b;
        public boolean c;

        private a() {
        }
    }

    private void a() {
        super.setClipChildren(false);
    }

    public HeaderGridView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a();
    }

    public HeaderGridView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        a();
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
        ListAdapter adapter = getAdapter();
        if (adapter != null && (adapter instanceof b)) {
            ((b) adapter).a(getNumColumns());
        }
    }

    public void setClipChildren(boolean z) {
    }

    public int getHeaderViewCount() {
        return this.f2337a.size();
    }

    public void setAdapter(ListAdapter listAdapter) {
        if (this.f2337a.size() > 0) {
            b bVar = new b(this.f2337a, listAdapter);
            int numColumns = getNumColumns();
            if (numColumns > 1) {
                bVar.a(numColumns);
            }
            super.setAdapter((ListAdapter) bVar);
            return;
        }
        super.setAdapter(listAdapter);
    }

    private static class b implements Filterable, WrapperListAdapter {

        /* renamed from: a  reason: collision with root package name */
        ArrayList<a> f2339a;
        boolean b;
        private final DataSetObservable c = new DataSetObservable();
        private final ListAdapter d;
        private int e = 1;
        private final boolean f;

        public b(ArrayList<a> arrayList, ListAdapter listAdapter) {
            this.d = listAdapter;
            this.f = listAdapter instanceof Filterable;
            if (arrayList == null) {
                throw new IllegalArgumentException("headerViewInfos cannot be null");
            }
            this.f2339a = arrayList;
            this.b = a(this.f2339a);
        }

        public int a() {
            return this.f2339a.size();
        }

        public boolean isEmpty() {
            return (this.d == null || this.d.isEmpty()) && a() == 0;
        }

        public void a(int i) {
            if (i < 1) {
                throw new IllegalArgumentException("Number of columns must be 1 or more");
            } else if (this.e != i) {
                this.e = i;
                b();
            }
        }

        private boolean a(ArrayList<a> arrayList) {
            if (arrayList != null) {
                Iterator<a> it = arrayList.iterator();
                while (it.hasNext()) {
                    if (!it.next().c) {
                        return false;
                    }
                }
            }
            return true;
        }

        public int getCount() {
            if (this.d != null) {
                return (a() * this.e) + this.d.getCount();
            }
            return a() * this.e;
        }

        public boolean areAllItemsEnabled() {
            if (this.d == null) {
                return true;
            }
            if (!this.b || !this.d.areAllItemsEnabled()) {
                return false;
            }
            return true;
        }

        public boolean isEnabled(int i) {
            int a2 = a() * this.e;
            if (i < a2) {
                return i % this.e == 0 && this.f2339a.get(i / this.e).c;
            }
            int i2 = i - a2;
            if (this.d != null && i2 < this.d.getCount()) {
                return this.d.isEnabled(i2);
            }
            throw new ArrayIndexOutOfBoundsException(i);
        }

        public Object getItem(int i) {
            int a2 = a() * this.e;
            if (i >= a2) {
                int i2 = i - a2;
                if (this.d != null && i2 < this.d.getCount()) {
                    return this.d.getItem(i2);
                }
                throw new ArrayIndexOutOfBoundsException(i);
            } else if (i % this.e == 0) {
                return this.f2339a.get(i / this.e).b;
            } else {
                return null;
            }
        }

        public long getItemId(int i) {
            int i2;
            int a2 = a() * this.e;
            if (this.d == null || i < a2 || (i2 = i - a2) >= this.d.getCount()) {
                return -1;
            }
            return this.d.getItemId(i2);
        }

        public boolean hasStableIds() {
            if (this.d != null) {
                return this.d.hasStableIds();
            }
            return false;
        }

        public View getView(int i, View view, ViewGroup viewGroup) {
            int a2 = a() * this.e;
            if (i < a2) {
                ViewGroup viewGroup2 = this.f2339a.get(i / this.e).f2338a;
                if (i % this.e == 0) {
                    return viewGroup2;
                }
                if (view == null) {
                    view = new View(viewGroup.getContext());
                }
                view.setVisibility(4);
                view.setMinimumHeight(viewGroup2.getHeight());
                return view;
            }
            int i2 = i - a2;
            if (this.d != null && i2 < this.d.getCount()) {
                return this.d.getView(i2, view, viewGroup);
            }
            throw new ArrayIndexOutOfBoundsException(i);
        }

        public int getItemViewType(int i) {
            int i2;
            int a2 = a() * this.e;
            if (i >= a2 || i % this.e == 0) {
                if (this.d == null || i < a2 || (i2 = i - a2) >= this.d.getCount()) {
                    return -2;
                }
                return this.d.getItemViewType(i2);
            } else if (this.d != null) {
                return this.d.getViewTypeCount();
            } else {
                return 1;
            }
        }

        public int getViewTypeCount() {
            if (this.d != null) {
                return this.d.getViewTypeCount() + 1;
            }
            return 2;
        }

        public void registerDataSetObserver(DataSetObserver dataSetObserver) {
            this.c.registerObserver(dataSetObserver);
            if (this.d != null) {
                this.d.registerDataSetObserver(dataSetObserver);
            }
        }

        public void unregisterDataSetObserver(DataSetObserver dataSetObserver) {
            this.c.unregisterObserver(dataSetObserver);
            if (this.d != null) {
                this.d.unregisterDataSetObserver(dataSetObserver);
            }
        }

        public Filter getFilter() {
            if (this.f) {
                return ((Filterable) this.d).getFilter();
            }
            return null;
        }

        public ListAdapter getWrappedAdapter() {
            return this.d;
        }

        public void b() {
            this.c.notifyChanged();
        }
    }
}
