package com.shoujiduoduo.util.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.view.MotionEventCompat;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.widget.ImageView;
import com.shoujiduoduo.ringtone.R;
import java.lang.Number;
import java.math.BigDecimal;

/* compiled from: RangeSeekBar */
public class e<T extends Number> extends ImageView {

    /* renamed from: a  reason: collision with root package name */
    public static final int f2358a = Color.argb(255, 51, 181, 229);
    private final Paint b = new Paint(1);
    private final Bitmap c = BitmapFactory.decodeResource(getResources(), R.drawable.left_handle);
    private final Bitmap d = BitmapFactory.decodeResource(getResources(), R.drawable.left_handle_down);
    private final Bitmap e = BitmapFactory.decodeResource(getResources(), R.drawable.right_handle);
    private final Bitmap f = BitmapFactory.decodeResource(getResources(), R.drawable.right_handle_down);
    private final float g = ((float) this.c.getWidth());
    private final float h = (this.g * 0.5f);
    private final float i = (((float) this.c.getHeight()) * 0.5f);
    private final float j = (0.3f * this.i);
    private final float k = 0.0f;
    private final T l;
    private final T m;
    private final a n;
    private final double o;
    private final double p;
    private double q = 0.0d;
    private double r = 1.0d;
    private c s = null;
    private boolean t = false;
    private b<T> u;
    private float v;
    private int w = 255;
    private int x;
    private boolean y;

    /* compiled from: RangeSeekBar */
    public interface b<T> {
        void a(e<?> eVar, Object obj, Object obj2);
    }

    /* compiled from: RangeSeekBar */
    public enum c {
        MIN,
        MAX
    }

    public e(T t2, T t3, Context context) throws IllegalArgumentException {
        super(context);
        this.l = t2;
        this.m = t3;
        this.o = t2.doubleValue();
        this.p = t3.doubleValue();
        this.n = a.a(t2);
        setFocusable(true);
        setFocusableInTouchMode(true);
        c();
    }

    private final void c() {
        this.x = ViewConfiguration.get(getContext()).getScaledTouchSlop();
    }

    public void setNotifyWhileDragging(boolean z) {
        this.t = z;
    }

    public T getAbsoluteMinValue() {
        return this.l;
    }

    public T getAbsoluteMaxValue() {
        return this.m;
    }

    public T getSelectedMinValue() {
        return a(this.q);
    }

    public void setSelectedMinValue(T t2) {
        if (0.0d == this.p - this.o) {
            setNormalizedMinValue(0.0d);
        } else {
            setNormalizedMinValue(a((Number) t2));
        }
    }

    public T getSelectedMaxValue() {
        return a(this.r);
    }

    public void setSelectedMaxValue(T t2) {
        if (0.0d == this.p - this.o) {
            setNormalizedMaxValue(1.0d);
        } else {
            setNormalizedMaxValue(a((Number) t2));
        }
    }

    public void setOnRangeSeekBarChangeListener(b<T> bVar) {
        this.u = bVar;
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (!isEnabled()) {
            return false;
        }
        switch (motionEvent.getAction() & 255) {
            case 0:
                this.w = motionEvent.getPointerId(motionEvent.getPointerCount() - 1);
                this.v = motionEvent.getX(motionEvent.findPointerIndex(this.w));
                this.s = a(this.v);
                if (this.s != null) {
                    setPressed(true);
                    invalidate();
                    a();
                    b(motionEvent);
                    d();
                    break;
                } else {
                    return super.onTouchEvent(motionEvent);
                }
            case 1:
                if (this.y) {
                    b(motionEvent);
                    b();
                    setPressed(false);
                } else {
                    a();
                    b(motionEvent);
                    b();
                }
                invalidate();
                if (this.u != null) {
                    this.u.a(this, getSelectedMinValue(), getSelectedMaxValue());
                    break;
                }
                break;
            case 2:
                if (this.s != null) {
                    if (this.y) {
                        b(motionEvent);
                    } else if (Math.abs(motionEvent.getX(motionEvent.findPointerIndex(this.w)) - this.v) > ((float) this.x)) {
                        setPressed(true);
                        invalidate();
                        a();
                        b(motionEvent);
                        d();
                    }
                    if (this.t && this.u != null) {
                        this.u.a(this, getSelectedMinValue(), getSelectedMaxValue());
                        break;
                    }
                }
                break;
            case 3:
                if (this.y) {
                    b();
                    setPressed(false);
                }
                invalidate();
                break;
            case 5:
                int pointerCount = motionEvent.getPointerCount() - 1;
                this.v = motionEvent.getX(pointerCount);
                this.w = motionEvent.getPointerId(pointerCount);
                invalidate();
                break;
            case 6:
                a(motionEvent);
                invalidate();
                break;
        }
        return true;
    }

    private final void a(MotionEvent motionEvent) {
        int action = (motionEvent.getAction() & MotionEventCompat.ACTION_POINTER_INDEX_MASK) >> 8;
        if (motionEvent.getPointerId(action) == this.w) {
            int i2 = action == 0 ? 1 : 0;
            this.v = motionEvent.getX(i2);
            this.w = motionEvent.getPointerId(i2);
        }
    }

    private final void b(MotionEvent motionEvent) {
        float x2 = motionEvent.getX(motionEvent.findPointerIndex(this.w));
        if (c.MIN.equals(this.s)) {
            setNormalizedMinValue(b(x2 - this.h));
        } else if (c.MAX.equals(this.s)) {
            setNormalizedMaxValue(b(x2 + this.h));
        }
    }

    private void d() {
        if (getParent() != null) {
            getParent().requestDisallowInterceptTouchEvent(true);
        }
    }

    /* access modifiers changed from: package-private */
    public void a() {
        this.y = true;
    }

    /* access modifiers changed from: package-private */
    public void b() {
        this.y = false;
    }

    /* access modifiers changed from: protected */
    public synchronized void onMeasure(int i2, int i3) {
        int i4;
        if (View.MeasureSpec.getMode(i2) != 0) {
            i4 = View.MeasureSpec.getSize(i2);
        } else {
            i4 = 200;
        }
        int height = this.c.getHeight();
        if (View.MeasureSpec.getMode(i3) != 0) {
            height = Math.min(height, View.MeasureSpec.getSize(i3));
        }
        setMeasuredDimension(i4, height);
    }

    /* access modifiers changed from: protected */
    public synchronized void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        a(b(this.q), c.MIN.equals(this.s), canvas, true);
        a(b(this.r), c.MAX.equals(this.s), canvas, false);
    }

    public c getFocusThumb() {
        return c.MIN.equals(this.s) ? c.MIN : c.MAX;
    }

    /* access modifiers changed from: protected */
    public Parcelable onSaveInstanceState() {
        Bundle bundle = new Bundle();
        bundle.putParcelable("SUPER", super.onSaveInstanceState());
        bundle.putDouble("MIN", this.q);
        bundle.putDouble("MAX", this.r);
        return bundle;
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Parcelable parcelable) {
        Bundle bundle = (Bundle) parcelable;
        super.onRestoreInstanceState(bundle.getParcelable("SUPER"));
        this.q = bundle.getDouble("MIN");
        this.r = bundle.getDouble("MAX");
    }

    private void a(float f2, boolean z, Canvas canvas, boolean z2) {
        if (z2) {
            canvas.drawBitmap(z ? this.d : this.c, f2, (((float) getHeight()) * 0.5f) - this.i, this.b);
        } else {
            canvas.drawBitmap(z ? this.f : this.e, f2 - this.g, (((float) getHeight()) * 0.5f) - this.i, this.b);
        }
    }

    private c a(float f2) {
        boolean a2 = a(f2, this.q);
        boolean a3 = a(f2, this.r);
        if (a2 && a3) {
            return f2 / ((float) getWidth()) > 0.5f ? c.MIN : c.MAX;
        }
        if (a2) {
            return c.MIN;
        }
        if (a3) {
            return c.MAX;
        }
        return null;
    }

    private boolean a(float f2, double d2) {
        return Math.abs(f2 - b(d2)) <= this.g;
    }

    public void setNormalizedMinValue(double d2) {
        this.q = Math.max(0.0d, Math.min(1.0d, Math.min(d2, this.r)));
        this.s = c.MIN;
        invalidate();
    }

    public void setNormalizedMaxValue(double d2) {
        this.r = Math.max(0.0d, Math.min(1.0d, Math.max(d2, this.q)));
        this.s = c.MAX;
        invalidate();
    }

    private T a(double d2) {
        return this.n.a(this.o + ((this.p - this.o) * d2));
    }

    private double a(T t2) {
        if (0.0d == this.p - this.o) {
            return 0.0d;
        }
        return (t2.doubleValue() - this.o) / (this.p - this.o);
    }

    private float b(double d2) {
        return (float) (0.0d + (((double) (((float) getWidth()) - 0.0f)) * d2));
    }

    private double b(float f2) {
        int width = getWidth();
        if (((float) width) <= 0.0f) {
            return 0.0d;
        }
        return Math.min(1.0d, Math.max(0.0d, (double) ((f2 - 0.0f) / (((float) width) - 0.0f))));
    }

    /* compiled from: RangeSeekBar */
    private enum a {
        LONG,
        DOUBLE,
        INTEGER,
        FLOAT,
        SHORT,
        BYTE,
        BIG_DECIMAL;

        public static <E extends Number> a a(E e) throws IllegalArgumentException {
            if (e instanceof Long) {
                return LONG;
            }
            if (e instanceof Double) {
                return DOUBLE;
            }
            if (e instanceof Integer) {
                return INTEGER;
            }
            if (e instanceof Float) {
                return FLOAT;
            }
            if (e instanceof Short) {
                return SHORT;
            }
            if (e instanceof Byte) {
                return BYTE;
            }
            if (e instanceof BigDecimal) {
                return BIG_DECIMAL;
            }
            throw new IllegalArgumentException("Number class '" + e.getClass().getName() + "' is not supported");
        }

        public Number a(double d) {
            switch (this) {
                case LONG:
                    return Long.valueOf((long) d);
                case DOUBLE:
                    return Double.valueOf(d);
                case INTEGER:
                    return Integer.valueOf((int) d);
                case FLOAT:
                    return Float.valueOf((float) d);
                case SHORT:
                    return new Short((short) ((int) d));
                case BYTE:
                    return new Byte((byte) ((int) d));
                case BIG_DECIMAL:
                    return new BigDecimal(d);
                default:
                    throw new InstantiationError("can't convert " + this + " to a Number object");
            }
        }
    }
}
