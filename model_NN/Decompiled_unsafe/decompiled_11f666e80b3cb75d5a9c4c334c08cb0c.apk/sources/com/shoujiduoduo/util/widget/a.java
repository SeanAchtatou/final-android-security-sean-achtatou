package com.shoujiduoduo.util.widget;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.WindowManager;
import android.widget.TextView;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.util.ah;

/* compiled from: CustomProgressDialog */
public class a extends ProgressDialog {

    /* renamed from: a  reason: collision with root package name */
    private boolean f2351a;
    private boolean b;
    private String c;

    public a(Context context) {
        super(context, R.style.CustomProgressDialog);
    }

    public void a(boolean z) {
        this.f2351a = z;
    }

    public void b(boolean z) {
        this.b = z;
    }

    public void a(String str) {
        this.c = str;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        a(getContext());
    }

    private void a(Context context) {
        setCancelable(this.f2351a);
        setCanceledOnTouchOutside(this.b);
        setContentView((int) R.layout.load_dialog);
        if (!ah.c(this.c)) {
            ((TextView) findViewById(R.id.tv_load_dialog)).setText(this.c);
        }
        WindowManager.LayoutParams attributes = getWindow().getAttributes();
        attributes.width = -2;
        attributes.height = -2;
        getWindow().setAttributes(attributes);
    }
}
