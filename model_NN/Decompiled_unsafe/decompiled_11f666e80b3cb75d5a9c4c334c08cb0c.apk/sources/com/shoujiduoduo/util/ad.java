package com.shoujiduoduo.util;

import android.content.ContentValues;
import android.content.Context;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.widget.Toast;
import com.igexin.download.Downloads;
import com.shoujiduoduo.a.a.b;
import com.shoujiduoduo.a.a.c;
import com.shoujiduoduo.a.c.i;
import com.shoujiduoduo.a.c.p;
import com.shoujiduoduo.base.bean.MakeRingData;
import com.shoujiduoduo.base.bean.RingCacheData;
import com.shoujiduoduo.base.bean.RingData;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ringtone.RingDDApp;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.HashMap;

/* compiled from: SetRingTone */
public class ad implements i {
    private static ad i = null;
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public RingData f2181a;
    private String b;
    private String c;
    /* access modifiers changed from: private */
    public Context d;
    private HashMap<String, Integer> e = new HashMap<>();
    /* access modifiers changed from: private */
    public boolean f = false;
    /* access modifiers changed from: private */
    public int g;
    private a h = new a(this);
    private String j;

    private ad(Context context) {
        this.d = context;
    }

    public static synchronized ad a(Context context) {
        ad adVar;
        synchronized (ad.class) {
            if (i == null) {
                i = new ad(context);
                n.a(context).a(i);
                com.shoujiduoduo.base.a.a.a("SetRingTone", "getInstance: SetRingTone created!");
            }
            adVar = i;
        }
        return adVar;
    }

    public static ad a() {
        if (i != null) {
            return i;
        }
        return null;
    }

    /* compiled from: SetRingTone */
    private static class a extends Handler {

        /* renamed from: a  reason: collision with root package name */
        private WeakReference<ad> f2182a;

        public a(ad adVar) {
            this.f2182a = new WeakReference<>(adVar);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.shoujiduoduo.util.ad.a(com.shoujiduoduo.util.ad, boolean):boolean
         arg types: [com.shoujiduoduo.util.ad, int]
         candidates:
          com.shoujiduoduo.util.ad.a(com.shoujiduoduo.base.bean.RingData, int):void
          com.shoujiduoduo.util.ad.a(java.lang.String, android.net.Uri):boolean
          com.shoujiduoduo.util.ad.a(com.shoujiduoduo.base.bean.RingCacheData, int):void
          com.shoujiduoduo.a.c.i.a(com.shoujiduoduo.base.bean.RingCacheData, int):void
          com.shoujiduoduo.util.ad.a(com.shoujiduoduo.util.ad, boolean):boolean */
        public void handleMessage(Message message) {
            super.handleMessage(message);
            ad adVar = this.f2182a.get();
            if (adVar != null) {
                switch (message.what) {
                    case 1001:
                        Toast.makeText(adVar.d, adVar.d.getResources().getText(R.string.set_ring_error_message), 1).show();
                        return;
                    case 1002:
                        Toast.makeText(adVar.d, (String) message.obj, 1).show();
                        return;
                    case 2000:
                        RingCacheData ringCacheData = (RingCacheData) message.obj;
                        if (adVar.f2181a != null && adVar.f && ringCacheData.rid == adVar.f2181a.getRid()) {
                            adVar.a(adVar.g);
                            boolean unused = adVar.f = false;
                            return;
                        }
                        return;
                    default:
                        return;
                }
            }
        }
    }

    public void a(HashMap<String, Integer> hashMap, RingData ringData, String str, String str2) {
        this.e.clear();
        this.e.putAll(hashMap);
        this.f = false;
        this.f2181a = ringData;
        this.b = str;
        this.c = str2;
        RingCacheData c2 = c();
        if (c2 == null) {
            com.shoujiduoduo.base.a.a.c("SetRingTone", "不应该为空，找漏洞吧！");
            this.h.sendEmptyMessage(1001);
        } else if (c2.downSize < c2.totalSize || c2.totalSize < 0) {
            this.f = true;
            this.g = 0;
            Message message = new Message();
            message.what = 1002;
            message.obj = this.d.getResources().getString(R.string.set_ring_not_down_finish_hint) + this.d.getResources().getString(R.string.set_ring_contact);
            this.h.sendMessage(message);
        } else {
            a(8);
        }
    }

    public void a(int i2, RingData ringData, String str, String str2) {
        this.f = false;
        this.f2181a = ringData;
        this.b = str;
        this.c = str2;
        RingCacheData c2 = c();
        if (c2 == null) {
            com.shoujiduoduo.base.a.a.c("SetRingTone", "不应该为空，找漏洞吧！");
            this.h.sendEmptyMessage(1001);
            return;
        }
        com.shoujiduoduo.base.a.a.a("SetRingTone", "type = " + i2);
        if (c2.downSize < c2.totalSize || c2.totalSize < 0) {
            com.shoujiduoduo.base.a.a.a("SetRingTone", "铃声尚未下载完成，下载完之后再设置铃声");
            this.f = true;
            this.g = i2;
            Message message = new Message();
            message.what = 1002;
            String string = this.d.getResources().getString(R.string.set_ring_not_down_finish_hint);
            String str3 = "";
            if (!((i2 & 1) == 0 && (i2 & 32) == 0)) {
                str3 = str3 + this.d.getResources().getString(R.string.set_ring_incoming_call);
            }
            if (!((i2 & 2) == 0 && (i2 & 64) == 0 && (i2 & 128) == 0)) {
                str3 = str3 + this.d.getResources().getString(R.string.set_ring_message);
            }
            if ((i2 & 4) != 0) {
                str3 = str3 + this.d.getResources().getString(R.string.set_ring_alarm);
            }
            message.obj = string + str3;
            this.h.sendMessage(message);
            return;
        }
        a(i2);
    }

    private boolean a(Uri uri) {
        for (String next : this.e.keySet()) {
            if (this.e.get(next).intValue() == 0) {
                a(next);
            } else if (this.e.get(next).intValue() == 1) {
                a(next, uri);
            }
        }
        return true;
    }

    private boolean a(String str, Uri uri) {
        try {
            Uri withAppendedPath = Uri.withAppendedPath(ContactsContract.Contacts.CONTENT_URI, str);
            ContentValues contentValues = new ContentValues();
            contentValues.put("custom_ringtone", uri.toString());
            this.d.getContentResolver().update(withAppendedPath, contentValues, null, null);
            return true;
        } catch (Exception e2) {
            e2.printStackTrace();
            return false;
        }
    }

    private boolean a(String str) {
        try {
            Uri withAppendedPath = Uri.withAppendedPath(ContactsContract.Contacts.CONTENT_URI, str);
            ContentValues contentValues = new ContentValues();
            contentValues.put("custom_ringtone", RingtoneManager.getActualDefaultRingtoneUri(this.d, 1).toString());
            this.d.getContentResolver().update(withAppendedPath, contentValues, null, null);
            return true;
        } catch (Exception e2) {
            e2.printStackTrace();
            return false;
        }
    }

    private RingCacheData c() {
        if (!(this.f2181a instanceof MakeRingData)) {
            return n.a(this.d).b(this.f2181a.getRid());
        }
        MakeRingData makeRingData = (MakeRingData) this.f2181a;
        File file = new File(makeRingData.localPath);
        if (file.exists()) {
            RingCacheData ringCacheData = new RingCacheData(this.f2181a.name, this.f2181a.artist, 0, (int) file.length(), (int) file.length(), 128000, q.b(makeRingData.localPath), "");
            ringCacheData.setPath(makeRingData.localPath);
            return ringCacheData;
        } else if (!makeRingData.rid.equals("")) {
            return n.a(this.d).b(this.f2181a.getRid());
        } else {
            return null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void} */
    public static boolean a(int i2, String str, String str2, String str3, String str4) {
        File file = new File(str);
        try {
            FileInputStream fileInputStream = new FileInputStream(str);
            int available = fileInputStream.available();
            fileInputStream.close();
            com.shoujiduoduo.base.a.a.a("SetRingTone", "ringtone path: " + file.getAbsolutePath());
            String b2 = q.b(str);
            ContentValues contentValues = new ContentValues();
            if (b2.equals("mp3")) {
                contentValues.put("mime_type", "audio/mp3");
            } else if (b2.equals("aac")) {
                contentValues.put("mime_type", "audio/aac");
            } else {
                com.shoujiduoduo.base.a.a.c("SetRingTone", "format data not support");
                return false;
            }
            contentValues.put(Downloads._DATA, file.getAbsolutePath());
            contentValues.put("title", str2);
            contentValues.put("_size", Integer.valueOf(available));
            contentValues.put("artist", str3);
            contentValues.put("duration", str4);
            contentValues.put("is_ringtone", (Boolean) true);
            contentValues.put("is_notification", (Boolean) true);
            contentValues.put("is_alarm", (Boolean) true);
            contentValues.put("is_music", (Boolean) false);
            Uri contentUriForPath = MediaStore.Audio.Media.getContentUriForPath(file.getAbsolutePath());
            if (contentUriForPath == null) {
                com.shoujiduoduo.base.a.a.a("SetRingTone", "setringtone:uri is NULL!");
                return false;
            }
            com.shoujiduoduo.base.a.a.a("SetRingTone", "uri encoded = " + contentUriForPath.toString());
            com.shoujiduoduo.base.a.a.a("SetRingTone", "uri decoded = " + contentUriForPath.getPath());
            try {
                RingDDApp.c().getContentResolver().delete(contentUriForPath, "_data=\"" + file.getAbsolutePath() + "\"", null);
            } catch (Exception e2) {
            }
            try {
                Uri insert = RingDDApp.c().getContentResolver().insert(contentUriForPath, contentValues);
                if (insert == null) {
                    com.shoujiduoduo.base.a.a.a("SetRingTone", "setringtone: newURI is NULL!");
                    return false;
                }
                a(RingDDApp.c(), i2, insert);
                return true;
            } catch (Exception e3) {
                return false;
            }
        } catch (IOException e4) {
            e4.printStackTrace();
            com.shoujiduoduo.base.a.a.c("SetRingTone", "ringtone file missing!");
            return false;
        }
    }

    public boolean a(final int i2) {
        com.shoujiduoduo.base.a.a.a("SetRingTone", "in setRing");
        Uri d2 = d();
        if (d2 == null) {
            return false;
        }
        if ((i2 & 1) != 0) {
            a(this.f2181a, 1);
            ae.c(this.d, "user_ring_phone_select", this.f2181a.rid);
            ae.c(this.d, "user_ring_phone_select_name", this.f2181a.name);
            if (s.b()) {
                s.a(this.d, d2, i2);
            } else {
                ai.a(this.d, d2, this.j);
                a(this.d, 1, d2);
            }
        }
        if ((i2 & 2) != 0) {
            a(this.f2181a, 2);
            ae.c(this.d, "user_ring_notification_select", this.f2181a.rid);
            ae.c(this.d, "user_ring_notification_select_name", this.f2181a.name);
            if (s.b()) {
                s.a(this.d, d2, i2);
            } else {
                ai.b(this.d, d2, this.j);
                if (!Build.BRAND.equalsIgnoreCase("OPPO")) {
                    a(this.d, 2, d2);
                }
            }
        }
        if ((i2 & 4) != 0) {
            a(this.f2181a, 3);
            ae.c(this.d, "user_ring_alarm_select", this.f2181a.rid);
            ae.c(this.d, "user_ring_alarm_select_name", this.f2181a.name);
            if (s.b()) {
                s.a(this.d, d2, i2);
            } else {
                ai.c(this.d, d2, this.j);
                a(this.d, 4, d2);
            }
        }
        if ((i2 & 8) != 0) {
            a(this.f2181a, 4);
            a(d2);
        }
        c.a().b(b.OBSERVER_RING_CHANGE, new c.a<p>() {
            public void a() {
                ((p) this.f1284a).a(i2, ad.this.f2181a);
            }
        });
        Message message = new Message();
        message.what = 1002;
        String string = this.d.getResources().getString(R.string.set_ring_hint);
        if ((i2 & 1) != 0) {
            string = (string + this.d.getResources().getString(R.string.set_ring_incoming_call)) + " ";
        }
        if ((i2 & 2) != 0) {
            string = (string + this.d.getResources().getString(R.string.set_ring_message)) + " ";
        }
        if ((i2 & 4) != 0) {
            string = (string + this.d.getResources().getString(R.string.set_ring_alarm)) + " ";
        }
        if ((i2 & 8) != 0) {
            string = string + this.d.getResources().getString(R.string.set_ring_contact);
        }
        message.obj = string;
        this.h.sendMessage(message);
        return true;
    }

    private static void a(Context context, int i2, Uri uri) {
        try {
            RingtoneManager.setActualDefaultRingtoneUri(context, i2, uri);
        } catch (UnsupportedOperationException e2) {
            e2.printStackTrace();
            com.shoujiduoduo.base.a.a.a(e2);
        } catch (SecurityException e3) {
            e3.printStackTrace();
            com.shoujiduoduo.base.a.a.a(e3);
        } catch (Exception e4) {
            e4.printStackTrace();
            com.shoujiduoduo.base.a.a.a(e4);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void} */
    private Uri d() {
        RingCacheData c2 = c();
        if (c2 == null) {
            com.shoujiduoduo.base.a.a.c("SetRingTone", "data is null");
            this.h.sendEmptyMessage(1001);
            return null;
        }
        String songPath = c2.getSongPath();
        this.j = songPath;
        File file = new File(songPath);
        try {
            FileInputStream fileInputStream = new FileInputStream(songPath);
            int available = fileInputStream.available();
            fileInputStream.close();
            com.shoujiduoduo.base.a.a.a("SetRingTone", "ringtone path: " + file.getAbsolutePath());
            ContentValues contentValues = new ContentValues();
            if (c2.format == null || c2.format.length() == 0) {
                contentValues.put("mime_type", "audio/mp3");
            } else if (c2.format.equalsIgnoreCase("mp3")) {
                contentValues.put("mime_type", "audio/mp3");
            } else if (c2.format.equalsIgnoreCase("aac")) {
                contentValues.put("mime_type", "audio/aac");
            } else {
                com.shoujiduoduo.base.a.a.c("SetRingTone", "format data not support");
                this.h.sendEmptyMessage(1001);
                return null;
            }
            contentValues.put(Downloads._DATA, file.getAbsolutePath());
            contentValues.put("title", this.f2181a.name);
            contentValues.put("_size", Integer.valueOf(available));
            contentValues.put("artist", this.f2181a.artist);
            contentValues.put("duration", Double.valueOf(((double) this.f2181a.duration) * 1000.0d));
            contentValues.put("is_ringtone", (Boolean) true);
            contentValues.put("is_notification", (Boolean) true);
            contentValues.put("is_alarm", (Boolean) true);
            contentValues.put("is_music", (Boolean) false);
            Uri contentUriForPath = MediaStore.Audio.Media.getContentUriForPath(file.getAbsolutePath());
            if (contentUriForPath == null) {
                com.shoujiduoduo.base.a.a.a("SetRingTone", "setringtone:uri is NULL!");
                this.h.sendEmptyMessage(1001);
                return null;
            }
            com.shoujiduoduo.base.a.a.a("SetRingTone", "uri encoded = " + contentUriForPath.toString());
            com.shoujiduoduo.base.a.a.a("SetRingTone", "uri decoded = " + contentUriForPath.getPath());
            try {
                this.d.getContentResolver().delete(contentUriForPath, "_data=\"" + file.getAbsolutePath() + "\"", null);
            } catch (Exception e2) {
                g.c("delete media uri error! uri = " + contentUriForPath.toString() + "\n" + "path = " + file.getAbsolutePath() + "\n" + com.shoujiduoduo.base.a.b.a(e2));
            }
            try {
                Uri insert = this.d.getContentResolver().insert(contentUriForPath, contentValues);
                if (insert != null) {
                    return insert;
                }
                com.shoujiduoduo.base.a.a.a("SetRingTone", "setringtone: newURI is NULL!");
                this.h.sendEmptyMessage(1001);
                return null;
            } catch (Exception e3) {
                g.c("insert media uri error! uri = " + contentUriForPath.toString() + "\n" + "path = " + file.getAbsolutePath() + "\n" + com.shoujiduoduo.base.a.b.a(e3));
                this.h.sendEmptyMessage(1001);
                return null;
            }
        } catch (IOException e4) {
            e4.printStackTrace();
            com.shoujiduoduo.base.a.a.c("SetRingTone", "ringtone file missing!");
            this.h.sendEmptyMessage(1001);
            return null;
        }
    }

    private void a(RingData ringData, int i2) {
        al.a(ringData.rid, i2, "&from=" + this.b + "&listType=" + this.c + "&cucid=" + ringData.cucid);
    }

    public void a(RingCacheData ringCacheData) {
    }

    public void a(RingCacheData ringCacheData, int i2) {
    }

    public void b(RingCacheData ringCacheData) {
        this.h.sendMessage(this.h.obtainMessage(2000, ringCacheData));
    }

    public void c(RingCacheData ringCacheData) {
    }

    public void d(RingCacheData ringCacheData) {
    }

    public void b() {
        if (this.h != null) {
            this.h.removeCallbacksAndMessages(null);
        }
        i = null;
    }
}
