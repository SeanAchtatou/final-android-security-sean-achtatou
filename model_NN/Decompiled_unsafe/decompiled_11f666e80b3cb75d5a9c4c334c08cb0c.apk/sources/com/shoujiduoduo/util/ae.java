package com.shoujiduoduo.util;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;

/* compiled from: SharedPref */
public class ae {

    /* renamed from: a  reason: collision with root package name */
    private static SharedPreferences.Editor f2183a = null;
    private static SharedPreferences b = null;
    private static Context c = null;

    public static void a(Context context) {
        c = context;
    }

    public static String a(Context context, String str) {
        return a(context, str, (String) null);
    }

    public static String a(Context context, String str, String str2) {
        if (context == null) {
            context = c;
        }
        if (b == null) {
            b = context.getSharedPreferences("ring.shoujiduoduo.com", 0);
        }
        return b.getString(str, str2);
    }

    public static boolean b(Context context, String str, String str2) {
        if (context == null) {
            context = c;
        }
        if (f2183a == null) {
            f2183a = context.getSharedPreferences("ring.shoujiduoduo.com", 0).edit();
        }
        f2183a.putString(str, str2);
        return f2183a.commit();
    }

    public static boolean c(Context context, String str, String str2) {
        if (context == null) {
            context = c;
        }
        if (f2183a == null) {
            f2183a = context.getSharedPreferences("ring.shoujiduoduo.com", 0).edit();
        }
        f2183a.putString(str, str2);
        return a(f2183a);
    }

    public static int a(Context context, String str, int i) {
        if (context == null) {
            context = c;
        }
        if (b == null) {
            b = context.getSharedPreferences("ring.shoujiduoduo.com", 0);
        }
        return b.getInt(str, i);
    }

    public static boolean b(Context context, String str, int i) {
        if (context == null) {
            context = c;
        }
        if (f2183a == null) {
            f2183a = context.getSharedPreferences("ring.shoujiduoduo.com", 0).edit();
        }
        f2183a.putInt(str, i);
        return a(f2183a);
    }

    public static long a(Context context, String str, long j) {
        if (context == null) {
            context = c;
        }
        if (b == null) {
            b = context.getSharedPreferences("ring.shoujiduoduo.com", 0);
        }
        return b.getLong(str, j);
    }

    public static boolean b(Context context, String str, long j) {
        if (context == null) {
            context = c;
        }
        if (f2183a == null) {
            f2183a = context.getSharedPreferences("ring.shoujiduoduo.com", 0).edit();
        }
        f2183a.putLong(str, j);
        return a(f2183a);
    }

    @TargetApi(9)
    private static boolean a(SharedPreferences.Editor editor) {
        if (Build.VERSION.SDK_INT < 9) {
            return editor.commit();
        }
        editor.apply();
        return true;
    }
}
