package com.shoujiduoduo.util;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v4.app.NotificationCompat;
import android.widget.RemoteViews;
import com.shoujiduoduo.a.c.h;
import com.shoujiduoduo.ringtone.R;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import org.apache.mina.proxy.handlers.http.ntlm.NTLMConstants;

/* compiled from: DownloadSoftManager */
public class o {
    private static o b = null;
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public Context f2315a;
    private int c = 0;
    /* access modifiers changed from: private */
    public boolean d;
    /* access modifiers changed from: private */
    public HashMap<String, b> e = new HashMap<>();

    /* compiled from: DownloadSoftManager */
    public enum a {
        none,
        immediatelly,
        notifybar
    }

    private o(Context context) {
        this.f2315a = context;
    }

    public static synchronized o a(Context context) {
        o oVar;
        synchronized (o.class) {
            if (b == null) {
                b = new o(context);
            }
            oVar = b;
        }
        return oVar;
    }

    public void a(String str, String str2) {
        a(str, str2, a.none, false);
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(java.lang.String r8, java.lang.String r9, com.shoujiduoduo.util.o.a r10, boolean r11) {
        /*
            r7 = this;
            java.util.HashMap<java.lang.String, com.shoujiduoduo.util.o$b> r6 = r7.e
            monitor-enter(r6)
            java.net.URL r0 = new java.net.URL     // Catch:{ MalformedURLException -> 0x0030 }
            r0.<init>(r8)     // Catch:{ MalformedURLException -> 0x0030 }
            r7.d = r11     // Catch:{ all -> 0x0036 }
            java.util.HashMap<java.lang.String, com.shoujiduoduo.util.o$b> r0 = r7.e     // Catch:{ all -> 0x0036 }
            boolean r0 = r0.containsKey(r8)     // Catch:{ all -> 0x0036 }
            if (r0 != 0) goto L_0x002e
            com.shoujiduoduo.util.o$b r0 = new com.shoujiduoduo.util.o$b     // Catch:{ all -> 0x0036 }
            int r4 = r7.c     // Catch:{ all -> 0x0036 }
            r1 = r7
            r2 = r8
            r3 = r9
            r5 = r10
            r0.<init>(r2, r3, r4, r5)     // Catch:{ all -> 0x0036 }
            java.util.HashMap<java.lang.String, com.shoujiduoduo.util.o$b> r1 = r7.e     // Catch:{ all -> 0x0036 }
            r1.put(r8, r0)     // Catch:{ all -> 0x0036 }
            int r1 = r7.c     // Catch:{ all -> 0x0036 }
            int r1 = r1 + 1
            r7.c = r1     // Catch:{ all -> 0x0036 }
            r1 = 0
            java.lang.Void[] r1 = new java.lang.Void[r1]     // Catch:{ all -> 0x0036 }
            r0.execute(r1)     // Catch:{ all -> 0x0036 }
        L_0x002e:
            monitor-exit(r6)     // Catch:{ all -> 0x0036 }
        L_0x002f:
            return
        L_0x0030:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ all -> 0x0036 }
            monitor-exit(r6)     // Catch:{ all -> 0x0036 }
            goto L_0x002f
        L_0x0036:
            r0 = move-exception
            monitor-exit(r6)     // Catch:{ all -> 0x0036 }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.shoujiduoduo.util.o.a(java.lang.String, java.lang.String, com.shoujiduoduo.util.o$a, boolean):void");
    }

    /* compiled from: DownloadSoftManager */
    class b extends AsyncTask<Void, Integer, Boolean> implements h {

        /* renamed from: a  reason: collision with root package name */
        public long f2317a = -1;
        public long b = 0;
        public int c = 0;
        private final String e = "UpdateTask";
        private String f;
        private String g;
        private String h;
        private String i;
        private int j;
        private boolean k = false;
        private a l = a.none;
        private final int m = 1922;
        private final int n = 2922;
        private final int o = 3922;
        private NotificationManager p = null;
        private Notification q = null;
        private Notification r = null;
        private Notification s = null;

        public b(String str, String str2, int i2, a aVar) {
            com.shoujiduoduo.base.a.a.a("UpdateTask", "UpdateTask start!");
            this.f = str;
            this.g = str2;
            this.c = i2;
            this.l = aVar;
            this.p = (NotificationManager) o.this.f2315a.getSystemService("notification");
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.shoujiduoduo.util.ae.b(android.content.Context, java.lang.String, long):boolean
         arg types: [android.content.Context, java.lang.String, int]
         candidates:
          com.shoujiduoduo.util.ae.b(android.content.Context, java.lang.String, int):boolean
          com.shoujiduoduo.util.ae.b(android.content.Context, java.lang.String, java.lang.String):boolean
          com.shoujiduoduo.util.ae.b(android.content.Context, java.lang.String, long):boolean */
        private void c() {
            this.f2317a = -1;
            ae.b(o.this.f2315a, this.h + ":total", -1L);
            this.b = 0;
            ae.b(o.this.f2315a, this.h + ":current", 0L);
            k.a(this.i);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.shoujiduoduo.util.ae.a(android.content.Context, java.lang.String, long):long
         arg types: [android.content.Context, java.lang.String, int]
         candidates:
          com.shoujiduoduo.util.ae.a(android.content.Context, java.lang.String, int):int
          com.shoujiduoduo.util.ae.a(android.content.Context, java.lang.String, java.lang.String):java.lang.String
          com.shoujiduoduo.util.ae.a(android.content.Context, java.lang.String, long):long */
        private boolean d() {
            boolean z = false;
            this.h = this.f.substring(this.f.lastIndexOf("/") + 1);
            this.h = this.h.toLowerCase();
            if (!this.h.endsWith(".apk")) {
                com.shoujiduoduo.base.a.a.a("UpdateTask", "url not end with apk, user url hash to cache");
                this.h = this.f.hashCode() + ".apk";
            }
            com.shoujiduoduo.base.a.a.a("UpdateTask", "mCacheName = " + this.h);
            this.i = p.c() + this.h;
            com.shoujiduoduo.base.a.a.a("UpdateTask", "download soft: cachePath = " + this.i);
            this.f2317a = ae.a(o.this.f2315a, this.h + ":total", -1L);
            this.b = ae.a(o.this.f2315a, this.h + ":current", 0L);
            com.shoujiduoduo.base.a.a.a("UpdateTask", "download soft: totalLength = " + this.f2317a + "; currentLength = " + this.b);
            this.k = true;
            if (this.f2317a > 0 && this.b >= 0) {
                if (this.b <= this.f2317a) {
                    File file = new File(this.i);
                    if (!file.exists() || !file.canWrite() || !file.isFile()) {
                        this.b = 0;
                        this.f2317a = -1;
                        z = true;
                    } else if (this.b == this.f2317a) {
                        if (file.length() == this.b) {
                            this.k = false;
                        } else {
                            this.b = 0;
                            this.f2317a = -1;
                            z = true;
                        }
                    }
                } else {
                    this.b = 0;
                    this.f2317a = -1;
                    z = true;
                }
                if (z) {
                    c();
                }
            }
            return true;
        }

        private void b(int i2) {
            String str = this.g;
            Intent intent = new Intent(o.this.f2315a, o.this.f2315a.getClass());
            intent.addFlags(NTLMConstants.FLAG_NEGOTIATE_128_BIT_ENCRYPTION);
            this.s = new Notification(R.drawable.icon_download, str, System.currentTimeMillis());
            com.shoujiduoduo.base.a.a.a("UpdateTask", "package name: " + o.this.f2315a.getPackageName());
            this.s.contentIntent = PendingIntent.getActivity(o.this.f2315a, 0, intent, 0);
            this.s.contentView = new RemoteViews(o.this.f2315a.getPackageName(), (int) R.layout.download_notif);
            this.s.contentView.setProgressBar(R.id.down_progress_bar, 100, i2, false);
            this.s.contentView.setTextViewText(R.id.down_tv, "正在下载" + this.g);
            this.p.notify(this.c + 1922, this.s);
        }

        public boolean a(String str, String str2, long j2, h hVar) {
            com.shoujiduoduo.base.a.a.a("UpdateTask", "download soft: url = " + str);
            com.shoujiduoduo.base.a.a.a("UpdateTask", "download soft: path = " + str2);
            com.shoujiduoduo.base.a.a.a("UpdateTask", "start_pos = " + j2);
            try {
                URL url = new URL(str);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                com.shoujiduoduo.base.a.a.a("UpdateTask", "download soft: conn = " + httpURLConnection.toString());
                httpURLConnection.setRequestProperty("RANGE", "bytes=" + j2 + "-");
                httpURLConnection.connect();
                com.shoujiduoduo.base.a.a.a("UpdateTask", "download soft: connect finished!");
                int contentLength = httpURLConnection.getContentLength();
                if (contentLength <= 0) {
                    com.shoujiduoduo.base.a.a.a("UpdateTask", "download soft: filesize Error! filesize= " + contentLength);
                    if (hVar != null) {
                        hVar.a(0);
                    }
                    return false;
                }
                if (!(j2 == 0 || this.f2317a <= 0 || ((long) contentLength) + j2 == this.f2317a)) {
                    com.shoujiduoduo.base.a.a.a("UpdateTask", "Error! the URL filesize changed! filesize = " + contentLength + ", start = " + j2 + ", totalsize = " + this.f2317a);
                    c();
                    j2 = 0;
                    httpURLConnection.disconnect();
                    httpURLConnection = (HttpURLConnection) url.openConnection();
                    com.shoujiduoduo.base.a.a.a("UpdateTask", "download soft: conn = " + httpURLConnection.toString());
                    httpURLConnection.setRequestProperty("RANGE", "bytes=" + 0L + "-");
                    httpURLConnection.connect();
                    com.shoujiduoduo.base.a.a.a("UpdateTask", "download soft: connect finished!");
                    contentLength = httpURLConnection.getContentLength();
                    if (contentLength <= 0) {
                        com.shoujiduoduo.base.a.a.a("UpdateTask", "download soft: filesize Error! filesize= " + contentLength);
                        if (hVar != null) {
                            hVar.a(0);
                        }
                        return false;
                    }
                }
                int i2 = contentLength;
                HttpURLConnection httpURLConnection2 = httpURLConnection;
                long j3 = j2;
                if (hVar != null) {
                    hVar.b(((long) i2) + j3);
                }
                if (isCancelled()) {
                    if (hVar != null) {
                        hVar.b();
                    }
                    return true;
                }
                com.shoujiduoduo.base.a.a.a("UpdateTask", "download soft: filesize = " + i2);
                InputStream inputStream = httpURLConnection2.getInputStream();
                RandomAccessFile randomAccessFile = new RandomAccessFile(str2, "rw");
                randomAccessFile.seek(j3);
                byte[] bArr = new byte[10240];
                while (true) {
                    int read = inputStream.read(bArr, 0, 10240);
                    if (read <= 0) {
                        break;
                    }
                    randomAccessFile.write(bArr, 0, read);
                    j3 += (long) read;
                    if (hVar != null) {
                        hVar.a(j3);
                    }
                    if (isCancelled()) {
                        if (hVar != null) {
                            hVar.b();
                        }
                    }
                }
                randomAccessFile.close();
                httpURLConnection2.disconnect();
                if (hVar != null && !isCancelled()) {
                    hVar.a();
                }
                return true;
            } catch (MalformedURLException e2) {
                e2.printStackTrace();
                if (hVar != null) {
                    hVar.a(0);
                }
                return false;
            } catch (IOException e3) {
                e3.printStackTrace();
                if (hVar != null) {
                    hVar.a(0);
                }
                return false;
            }
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            com.shoujiduoduo.base.a.a.a("UpdateTask", "UPdateTask: onPreExecute");
            b(0);
        }

        /* access modifiers changed from: protected */
        public void onCancelled() {
            synchronized (o.this.e) {
                o.this.e.remove(this.f);
            }
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public Boolean doInBackground(Void... voidArr) {
            com.shoujiduoduo.base.a.a.a("UpdateTask", "UPdateTask: doInBackground");
            if (!d()) {
                com.shoujiduoduo.base.a.a.a("UpdateTask", "getCacheInfo failed");
                return Boolean.FALSE;
            } else if (!this.k) {
                publishProgress(100, 100);
                com.shoujiduoduo.base.a.a.a("UpdateTask", "已经下载完毕，不需要下载了");
                return Boolean.TRUE;
            } else {
                com.shoujiduoduo.base.a.a.a("UpdateTask", "准备开始下载");
                return Boolean.valueOf(a(this.f, this.i, this.b, this));
            }
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void onPostExecute(Boolean bool) {
            synchronized (o.this.e) {
                this.p.cancel(this.c + 1922);
                if (!bool.booleanValue()) {
                    Intent intent = new Intent(o.this.f2315a, o.this.f2315a.getClass());
                    intent.addFlags(NTLMConstants.FLAG_NEGOTIATE_128_BIT_ENCRYPTION);
                    intent.putExtra("update_fail", "yes");
                    PendingIntent activity = PendingIntent.getActivity(o.this.f2315a, 0, intent, 0);
                    NotificationCompat.Builder builder = new NotificationCompat.Builder(o.this.f2315a);
                    builder.setSmallIcon(R.drawable.duoduo_icon);
                    builder.setTicker("下载失败");
                    builder.setWhen(System.currentTimeMillis());
                    builder.setAutoCancel(true);
                    builder.setContentTitle(this.g);
                    builder.setContentText(this.g + "下载失败，请稍后再试");
                    builder.setContentIntent(activity);
                    this.q = builder.build();
                    this.p.notify(this.c + 2922, this.q);
                } else if (this.l == a.none) {
                    String a2 = ac.a().a("ad_install_immediately");
                    if (a2 == null) {
                        a2 = "true";
                    }
                    if (a2.equalsIgnoreCase("false")) {
                        e();
                    } else {
                        g.b(this.i);
                    }
                } else if (this.l == a.immediatelly) {
                    g.b(this.i);
                } else if (this.l == a.notifybar) {
                    if (o.this.d) {
                        Intent intent2 = new Intent();
                        intent2.setAction("install_apk_from_start_ad");
                        intent2.putExtra("PackagePath", this.i);
                        intent2.putExtra("PackageName", this.g);
                        if (o.this.f2315a != null) {
                            o.this.f2315a.sendBroadcast(intent2);
                            com.shoujiduoduo.base.a.a.a("UpdateTask", "send broadcast");
                        }
                    }
                    e();
                }
                o.this.e.remove(this.f);
            }
        }

        private void e() {
            Intent intent = new Intent("android.intent.action.VIEW");
            intent.setFlags(NTLMConstants.FLAG_UNIDENTIFIED_11);
            intent.setDataAndType(Uri.fromFile(new File(this.i)), "application/vnd.android.package-archive");
            intent.putExtra("down_finish", "yes");
            PendingIntent activity = PendingIntent.getActivity(o.this.f2315a, 0, intent, 0);
            NotificationCompat.Builder builder = new NotificationCompat.Builder(o.this.f2315a);
            builder.setSmallIcon(R.drawable.duoduo_icon);
            builder.setTicker("下载完毕");
            builder.setWhen(System.currentTimeMillis());
            builder.setAutoCancel(true);
            builder.setContentTitle(this.g);
            builder.setContentText(this.g + "下载完成，点击安装");
            builder.setContentIntent(activity);
            this.r = builder.build();
            this.p.notify(this.c + 3922, this.r);
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void onProgressUpdate(Integer... numArr) {
            if (!isCancelled()) {
                com.shoujiduoduo.base.a.a.a("UpdateTask", "onProgressUpdate: progress = " + numArr[1]);
                if (this.s != null) {
                    this.s.contentView.setProgressBar(R.id.down_progress_bar, 100, numArr[1].intValue(), false);
                    this.p.notify(this.c + 1922, this.s);
                }
            }
        }

        public void a(long j2) {
            this.b = j2;
            int i2 = (int) ((((float) this.b) * 100.0f) / ((float) this.f2317a));
            if (this.j != i2) {
                publishProgress(100, Integer.valueOf(i2));
                if (i2 > 0 && i2 % 5 == 0) {
                    ae.b(o.this.f2315a, this.h + ":current", j2);
                }
                this.j = i2;
            }
        }

        public void a(int i2) {
        }

        public void a() {
            ae.b(o.this.f2315a, this.h + ":current", this.f2317a);
            publishProgress(100, 100);
        }

        public void b(long j2) {
            ae.b(o.this.f2315a, this.h + ":total", j2);
            if (this.f2317a != j2) {
                this.f2317a = j2;
                this.b = 0;
            }
        }

        public void b() {
        }
    }
}
