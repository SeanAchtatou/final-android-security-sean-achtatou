package com.shoujiduoduo.util;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/* compiled from: DDThreadPool */
public class i {

    /* renamed from: a  reason: collision with root package name */
    private static ExecutorService f2304a = Executors.newCachedThreadPool();

    public static void a(Runnable runnable) {
        f2304a.execute(runnable);
    }

    public static void a() {
        if (f2304a != null) {
            f2304a.shutdown();
        }
    }
}
