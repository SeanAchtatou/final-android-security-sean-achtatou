package com.shoujiduoduo.util;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.provider.Settings;
import com.igexin.download.Downloads;
import com.shoujiduoduo.ringtone.R;
import java.util.ArrayList;
import java.util.List;

/* compiled from: SystemRingtoneUtils */
public class ai {

    /* renamed from: a  reason: collision with root package name */
    private Context f2186a;

    public ai(Context context) {
        this.f2186a = context;
    }

    /* compiled from: SystemRingtoneUtils */
    public class a {

        /* renamed from: a  reason: collision with root package name */
        public String f2187a;
        public String b;
        public int c;

        public a() {
        }

        public a(String str, String str2, int i) {
            this.f2187a = str;
            this.b = str2;
            this.c = i;
        }
    }

    /* compiled from: SystemRingtoneUtils */
    public class b {

        /* renamed from: a  reason: collision with root package name */
        public a f2188a = null;
        public a b = null;
        public a c = null;

        public b() {
        }
    }

    public b a() {
        b bVar = new b();
        Uri actualDefaultRingtoneUri = RingtoneManager.getActualDefaultRingtoneUri(this.f2186a, 1);
        String a2 = actualDefaultRingtoneUri == null ? "" : a(actualDefaultRingtoneUri);
        Uri uri = null;
        if (Build.BRAND.equalsIgnoreCase("OPPO")) {
            String string = Settings.System.getString(this.f2186a.getContentResolver(), "oppo_sms_notification_sound");
            if (!ah.c(string)) {
                uri = Uri.parse(string);
            }
        } else {
            uri = RingtoneManager.getActualDefaultRingtoneUri(this.f2186a, 2);
        }
        String a3 = uri == null ? "" : a(uri);
        Uri actualDefaultRingtoneUri2 = RingtoneManager.getActualDefaultRingtoneUri(this.f2186a, 4);
        String a4 = actualDefaultRingtoneUri2 == null ? "" : a(actualDefaultRingtoneUri2);
        com.shoujiduoduo.base.a.a.a("SystemRingtoneUtils", "def Ringtone: " + a2);
        com.shoujiduoduo.base.a.a.a("SystemRingtoneUtils", "def notification: " + a3);
        com.shoujiduoduo.base.a.a.a("SystemRingtoneUtils", "def alarm: " + a4);
        List<a> a5 = a(7, true);
        if (a5 != null) {
            for (a next : a5) {
                if (!ah.c(a2) && next.f2187a.equalsIgnoreCase(a2)) {
                    bVar.f2188a = next;
                }
                if (!ah.c(a3) && next.f2187a.equalsIgnoreCase(a3)) {
                    bVar.b = next;
                }
                if (!ah.c(a4) && next.f2187a.equalsIgnoreCase(a4)) {
                    bVar.c = next;
                }
            }
        }
        List<a> a6 = a(7, false);
        if (a6 != null) {
            for (a next2 : a6) {
                if (!ah.c(a2) && next2.f2187a.equalsIgnoreCase(a2)) {
                    bVar.f2188a = next2;
                }
                if (!ah.c(a3) && next2.f2187a.equalsIgnoreCase(a3)) {
                    bVar.b = next2;
                }
                if (!ah.c(a4) && next2.f2187a.equalsIgnoreCase(a4)) {
                    bVar.c = next2;
                }
            }
        }
        if (bVar.c == null) {
            bVar.c = new a("", this.f2186a.getResources().getString(R.string.unknown_alarm), 0);
        }
        if (bVar.b == null) {
            bVar.b = new a("", this.f2186a.getResources().getString(R.string.unknown_notification), 0);
        }
        if (bVar.f2188a == null) {
            bVar.f2188a = new a("", this.f2186a.getResources().getString(R.string.unknown_ringtone), 0);
        }
        return bVar;
    }

    private String a(Uri uri) {
        if (uri == null) {
            return "";
        }
        if (uri.toString().startsWith("file") && uri.toString().length() > 8) {
            return uri.toString().substring(7);
        }
        try {
            Cursor query = this.f2186a.getContentResolver().query(uri, new String[]{Downloads._DATA}, null, null, null);
            if (query == null) {
                return "";
            }
            if (!query.moveToFirst()) {
                return "";
            }
            int columnIndex = query.getColumnIndex(Downloads._DATA);
            if (columnIndex != -1) {
                return query.getString(columnIndex);
            }
            return "";
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public List<a> a(int i, boolean z) {
        Cursor query;
        Uri uri;
        ArrayList arrayList = new ArrayList();
        ContentResolver contentResolver = this.f2186a.getContentResolver();
        String str = "";
        switch (i) {
            case 1:
                str = "is_ringtone != ?";
                break;
            case 2:
                str = "is_notification != ?";
                break;
            case 4:
                str = "is_alarm != ?";
                break;
            case 7:
                str = "is_ringtone != ? or is_notification != ? or is_alarm != ?";
                break;
        }
        if (i != 7) {
            if (z) {
                try {
                    uri = MediaStore.Audio.Media.INTERNAL_CONTENT_URI;
                } catch (Exception e) {
                    e.printStackTrace();
                    query = null;
                }
            } else {
                uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
            }
            query = contentResolver.query(uri, new String[]{"_id", Downloads._DATA, "title", "duration"}, str, new String[]{"0"}, "_id asc");
        } else {
            query = contentResolver.query(z ? MediaStore.Audio.Media.INTERNAL_CONTENT_URI : MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, new String[]{"_id", Downloads._DATA, "title", "duration"}, str, new String[]{"0", "0", "0"}, "_id asc");
        }
        if (query == null) {
            return null;
        }
        while (query.moveToNext()) {
            a aVar = new a();
            aVar.f2187a = query.getString(query.getColumnIndex(Downloads._DATA));
            if (aVar.f2187a == null) {
                aVar.f2187a = "";
            }
            aVar.b = query.getString(query.getColumnIndex("title"));
            if (aVar.b == null) {
                aVar.b = "";
            }
            aVar.c = query.getInt(query.getColumnIndex("duration"));
            arrayList.add(aVar);
        }
        query.close();
        return arrayList;
    }

    public void a(int i, Uri uri, String str) {
        try {
            RingtoneManager.setActualDefaultRingtoneUri(this.f2186a, i, uri);
        } catch (Exception e) {
            e.printStackTrace();
            com.shoujiduoduo.base.a.a.a(e);
        }
        b(this.f2186a, uri, str);
    }

    public static boolean a(Context context, Uri uri, String str) {
        String str2;
        String str3;
        String str4;
        String str5 = null;
        try {
            if (Build.BRAND.equalsIgnoreCase("OPPO")) {
                Settings.System.putString(context.getContentResolver(), "ringtone_sim2", uri != null ? uri.toString() : null);
            }
            if (Build.BRAND.equalsIgnoreCase("vivo")) {
                ContentResolver contentResolver = context.getContentResolver();
                if (uri != null) {
                    str4 = uri.toString();
                } else {
                    str4 = null;
                }
                Settings.System.putString(contentResolver, "ringtone_sim2", str4);
            }
            if (Build.MANUFACTURER.equalsIgnoreCase("HUAWEI")) {
                ContentResolver contentResolver2 = context.getContentResolver();
                if (uri != null) {
                    str3 = uri.toString();
                } else {
                    str3 = null;
                }
                Settings.System.putString(contentResolver2, "ringtone2", str3);
                Settings.System.putString(context.getContentResolver(), "ringtone2_path", str);
            }
            if (Build.BRAND.equalsIgnoreCase("samsung")) {
                ContentResolver contentResolver3 = context.getContentResolver();
                if (uri != null) {
                    str2 = uri.toString();
                } else {
                    str2 = null;
                }
                Settings.System.putString(contentResolver3, "ringtone_2", str2);
            }
            if (!Build.BRAND.equalsIgnoreCase("TCT")) {
                return false;
            }
            ContentResolver contentResolver4 = context.getContentResolver();
            if (uri != null) {
                str5 = uri.toString();
            }
            Settings.System.putString(contentResolver4, "ringtone2", str5);
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static boolean b(Context context, Uri uri, String str) {
        String str2;
        String str3;
        String str4;
        String str5;
        String str6;
        String str7 = null;
        try {
            if (Build.BRAND.equalsIgnoreCase("vivo")) {
                Settings.System.putString(context.getContentResolver(), "message_sound", uri != null ? uri.toString() : null);
                ContentResolver contentResolver = context.getContentResolver();
                if (uri != null) {
                    str6 = uri.toString();
                } else {
                    str6 = null;
                }
                Settings.System.putString(contentResolver, "message_sound_sim2", str6);
            }
            if (Build.BRAND.equalsIgnoreCase("OPPO")) {
                ContentResolver contentResolver2 = context.getContentResolver();
                if (uri != null) {
                    str4 = uri.toString();
                } else {
                    str4 = null;
                }
                Settings.System.putString(contentResolver2, "oppo_sms_notification_sound", str4);
                ContentResolver contentResolver3 = context.getContentResolver();
                if (uri != null) {
                    str5 = uri.toString();
                } else {
                    str5 = null;
                }
                Settings.System.putString(contentResolver3, "notification_sim2", str5);
            }
            if (Build.BRAND.equalsIgnoreCase("Xiaomi")) {
                ContentResolver contentResolver4 = context.getContentResolver();
                if (uri != null) {
                    str3 = uri.toString();
                } else {
                    str3 = null;
                }
                Settings.System.putString(contentResolver4, "sms_received_sound", str3);
            }
            if (g.m()) {
                ContentResolver contentResolver5 = context.getContentResolver();
                if (uri != null) {
                    str2 = uri.toString();
                } else {
                    str2 = null;
                }
                Settings.System.putString(contentResolver5, "mms_sound", str2);
                Settings.System.putString(context.getContentResolver(), "mms_sound_file_path", str);
            }
            if (Build.BRAND.equalsIgnoreCase("samsung")) {
                ContentResolver contentResolver6 = context.getContentResolver();
                if (uri != null) {
                    str7 = uri.toString();
                }
                Settings.System.putString(contentResolver6, "notification_sound_2", str7);
            }
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public static boolean c(Context context, Uri uri, String str) {
        String str2 = null;
        try {
            if (Build.BRAND.equalsIgnoreCase("OPPO")) {
                Settings.System.putString(context.getContentResolver(), "oppo_default_alarm", uri != null ? uri.toString() : null);
            }
            if (Build.BRAND.equalsIgnoreCase("vivo")) {
                ContentResolver contentResolver = context.getContentResolver();
                if (uri != null) {
                    str2 = uri.toString();
                }
                Settings.System.putString(contentResolver, "bbk_alarm_alert", str2);
            }
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
