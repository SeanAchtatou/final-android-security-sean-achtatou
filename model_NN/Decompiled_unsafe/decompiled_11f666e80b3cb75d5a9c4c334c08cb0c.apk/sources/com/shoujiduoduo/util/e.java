package com.shoujiduoduo.util;

import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.drawable.Drawable;
import com.d.a.b.a.i;
import com.d.a.b.c.c;

/* compiled from: CircleBitmapDisplayer */
public class e extends c {
    public e(int i) {
        super(i);
    }

    public void display(Bitmap bitmap, com.d.a.b.e.a aVar, i iVar) {
        aVar.a(new a(bitmap, this.b));
    }

    /* compiled from: CircleBitmapDisplayer */
    public static class a extends Drawable {

        /* renamed from: a  reason: collision with root package name */
        private final int f2283a = 0;
        private final RectF b = new RectF();
        private final BitmapShader c;
        private final Paint d;
        private RectF e;

        public a(Bitmap bitmap, int i) {
            this.c = new BitmapShader(bitmap, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);
            this.e = new RectF((float) i, (float) i, (float) (bitmap.getWidth() - i), (float) (bitmap.getHeight() - i));
            this.d = new Paint();
            this.d.setAntiAlias(true);
            this.d.setShader(this.c);
        }

        /* access modifiers changed from: protected */
        public void onBoundsChange(Rect rect) {
            super.onBoundsChange(rect);
            this.b.set((float) this.f2283a, (float) this.f2283a, (float) (rect.width() - this.f2283a), (float) (rect.height() - this.f2283a));
            Matrix matrix = new Matrix();
            matrix.setRectToRect(this.e, this.b, Matrix.ScaleToFit.FILL);
            this.c.setLocalMatrix(matrix);
        }

        public void draw(Canvas canvas) {
            canvas.drawRoundRect(this.b, this.b.width() / 2.0f, this.b.height() / 2.0f, this.d);
        }

        public int getOpacity() {
            return -3;
        }

        public void setAlpha(int i) {
            this.d.setAlpha(i);
        }

        public void setColorFilter(ColorFilter colorFilter) {
            this.d.setColorFilter(colorFilter);
        }
    }
}
