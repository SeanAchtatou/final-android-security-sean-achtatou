package com.shoujiduoduo.util;

import android.os.Handler;
import cn.banshenggua.aichang.room.message.SocketMessage;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.umeng.a.b;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/* compiled from: DnsDetector */
public class m {
    /* access modifiers changed from: private */
    public static volatile String e = "cdnringhlt.shoujiduoduo.com";
    /* access modifiers changed from: private */
    public static volatile String g = "www.shoujiduoduo.com";
    private static final m m = new m();
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public String f2306a = "cdnringhlt.shoujiduoduo.com";
    /* access modifiers changed from: private */
    public String b = "";
    /* access modifiers changed from: private */
    public String c = "cdnringfw.shoujiduoduo.com";
    private String d = "";
    /* access modifiers changed from: private */
    public String f = "";
    /* access modifiers changed from: private */
    public int h = 8000;
    /* access modifiers changed from: private */
    public int i = 3;
    private final ReentrantReadWriteLock j = new ReentrantReadWriteLock();
    private final Lock k = this.j.readLock();
    /* access modifiers changed from: private */
    public final Lock l = this.j.writeLock();
    private Handler n;

    private m() {
    }

    public static m a() {
        return m;
    }

    public void a(Handler handler) {
        this.n = handler;
        this.f2306a = ac.a().a("dns_value1");
        this.b = ac.a().a("dns_check1");
        this.c = ac.a().a("dns_value2");
        this.d = ac.a().a("dns_check2");
        this.f = ac.a().a("dns_duoduo_check");
        this.h = r.a(ac.a().a("dns_timeout"), 8000);
        this.i = r.a(ac.a().a("dns_retry"), 3);
        com.shoujiduoduo.base.a.a.a("dnsdetector", "detect start");
        com.shoujiduoduo.base.a.a.a("dnsdetector", "online cdn1:" + this.f2306a);
        com.shoujiduoduo.base.a.a.a("dnsdetector", "online cdn2:" + this.c);
        com.shoujiduoduo.base.a.a.a("dnsdetector", "online timeout:" + this.h);
        com.shoujiduoduo.base.a.a.a("dnsdetector", "online retry:" + this.i);
        com.shoujiduoduo.base.a.a.a("dnsdetector", "switch condition:" + ac.a().a("dns_switchcondition"));
        if (!NetworkStateUtil.a()) {
            com.shoujiduoduo.base.a.a.e("dnsdetector", "network is not available");
            return;
        }
        new Thread(new Runnable() {
            /* JADX INFO: finally extract failed */
            public void run() {
                boolean z = false;
                int i = 0;
                while (true) {
                    if (i >= m.this.i) {
                        break;
                    }
                    a aVar = new a(m.this.f2306a);
                    aVar.start();
                    try {
                        aVar.join((long) m.this.h);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    if (aVar.a() == null || !m.this.a(m.this.b, "verify_cdn.dat", true)) {
                        com.shoujiduoduo.base.a.a.e("dnsdetector", "cdn dns not work:" + m.this.f2306a + ", retry times:" + (i + 1));
                        i++;
                    } else {
                        com.shoujiduoduo.base.a.a.a("dnsdetector", "cdn dns available:" + m.this.f2306a + ", ip:" + aVar.a());
                        m.this.l.lock();
                        try {
                            String unused = m.e = m.this.f2306a;
                            m.this.l.unlock();
                            z = true;
                            break;
                        } catch (Throwable th) {
                            m.this.l.unlock();
                            throw th;
                        }
                    }
                }
                HashMap hashMap = new HashMap();
                if (!z) {
                    HashMap hashMap2 = new HashMap();
                    a aVar2 = new a(m.this.c);
                    aVar2.start();
                    try {
                        aVar2.join((long) m.this.h);
                    } catch (InterruptedException e2) {
                        e2.printStackTrace();
                    }
                    if (aVar2.a() != null) {
                        hashMap2.put("cdn2 available", "yes");
                        hashMap.put(SocketMessage.MSG_RESULE_KEY, "cdn2 success");
                    } else {
                        hashMap2.put("cdn2 available", "no");
                        hashMap.put(SocketMessage.MSG_RESULE_KEY, "fail");
                    }
                    hashMap2.put("network type", NetworkStateUtil.d());
                    b.a(RingDDApp.c(), "cdn_dns_error_new", hashMap2);
                    m.this.l.lock();
                    try {
                        String unused2 = m.e = m.this.c;
                        m.this.l.unlock();
                        com.shoujiduoduo.base.a.a.a("dnsdetector", "availableCdn use cdn2:" + m.e);
                    } catch (Throwable th2) {
                        m.this.l.unlock();
                        throw th2;
                    }
                } else {
                    hashMap.put(SocketMessage.MSG_RESULE_KEY, "cdn1 success");
                }
                com.shoujiduoduo.base.a.a.a("dnsdetector", "*************duoduo cdn check, use cdn:" + m.e);
                b.a(RingDDApp.c(), "cdn_dns_check", hashMap);
                com.shoujiduoduo.base.a.a.a("dnsdetector", "detect cdn dns end");
            }
        }).start();
        new Thread(new Runnable() {
            public void run() {
                boolean z;
                boolean z2 = false;
                a aVar = new a("www.shoujiduoduo.com");
                aVar.start();
                try {
                    aVar.join((long) m.this.h);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                HashMap hashMap = new HashMap();
                if (aVar.a() == null || !m.this.a(m.this.f, "verify_duoduo1.dat", false)) {
                    com.shoujiduoduo.base.a.a.e("dnsdetector", "duoduo server dns lookup fail or verify file error");
                    com.shoujiduoduo.base.a.a.a("dnsdetector", "duoduo server dns lookup res:" + aVar.a());
                    HashMap hashMap2 = new HashMap();
                    hashMap2.put("network type", NetworkStateUtil.d());
                    b.a(RingDDApp.c(), "duoduo_server_dns_error_new", hashMap2);
                    String a2 = ac.a().a("dd_server_ip_1");
                    String a3 = ac.a().a("dd_server_ip_2");
                    String a4 = ac.a().a("dd_server_ip_3");
                    boolean a5 = m.this.a(m.this.f.replace("www.shoujiduoduo.com", a2), "verify_duoduo1.dat", false);
                    com.shoujiduoduo.base.a.a.a("dnsdetector", "duoduo server ip1:" + a2 + ", res:" + a5);
                    if (!a5) {
                        z = m.this.a(m.this.f.replace("www.shoujiduoduo.com", a3), "verify_duoduo2.dat", false);
                        com.shoujiduoduo.base.a.a.a("dnsdetector", "duoduo server ip2:" + a3 + ", res:" + z);
                        if (!z) {
                            z2 = m.this.a(m.this.f.replace("www.shoujiduoduo.com", a4), "verify_duoduo3.dat", false);
                            com.shoujiduoduo.base.a.a.a("dnsdetector", "duoduo server ip3:" + a4 + ", res:" + z2);
                        }
                    } else {
                        z = false;
                    }
                    m.this.l.lock();
                    if (a5) {
                        try {
                            String unused = m.g = a2;
                        } catch (Throwable th) {
                            m.this.l.unlock();
                            throw th;
                        }
                    } else if (z) {
                        String unused2 = m.g = a3;
                    } else if (z2) {
                        String unused3 = m.g = a4;
                    } else {
                        String unused4 = m.g = a2;
                    }
                    m.this.l.unlock();
                    com.shoujiduoduo.base.a.a.a("dnsdetector", "*************duoduo server check, use ip:" + m.g);
                    hashMap.put(SocketMessage.MSG_RESULE_KEY, "fail");
                } else {
                    hashMap.put(SocketMessage.MSG_RESULE_KEY, "success");
                    com.shoujiduoduo.base.a.a.a("dnsdetector", "duoduo server ip:" + aVar.a());
                    com.shoujiduoduo.base.a.a.a("dnsdetector", "*************duoduo server check, use host:www.shoujiduoduo.com");
                }
                b.a(RingDDApp.c(), "duoduo_server_dns_check", hashMap);
                com.shoujiduoduo.base.a.a.a("dnsdetector", "detect duoduo server end");
            }
        }).start();
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00df  */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x0153 A[SYNTHETIC, Splitter:B:65:0x0153] */
    /* JADX WARNING: Removed duplicated region for block: B:74:0x0168 A[SYNTHETIC, Splitter:B:74:0x0168] */
    /* JADX WARNING: Removed duplicated region for block: B:81:0x0177 A[SYNTHETIC, Splitter:B:81:0x0177] */
    /* JADX WARNING: Removed duplicated region for block: B:92:0x0193  */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:71:0x0163=Splitter:B:71:0x0163, B:62:0x014e=Splitter:B:62:0x014e} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean a(java.lang.String r13, java.lang.String r14, boolean r15) {
        /*
            r12 = this;
            r5 = 6000(0x1770, float:8.408E-42)
            r10 = 256(0x100, float:3.59E-43)
            r3 = 0
            r2 = 1
            com.shoujiduoduo.util.ac r0 = com.shoujiduoduo.util.ac.a()
            java.lang.String r1 = "dns_switchcondition"
            java.lang.String r6 = r0.a(r1)
            java.lang.String r0 = "none"
            boolean r0 = r0.equals(r6)
            if (r0 == 0) goto L_0x0019
        L_0x0018:
            return r2
        L_0x0019:
            java.lang.String r0 = "dnsdetector"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r4 = "verify file url:"
            java.lang.StringBuilder r1 = r1.append(r4)
            java.lang.StringBuilder r1 = r1.append(r13)
            java.lang.String r1 = r1.toString()
            com.shoujiduoduo.base.a.a.a(r0, r1)
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            r1 = 2
            java.lang.String r1 = com.shoujiduoduo.util.l.a(r1)
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.StringBuilder r0 = r0.append(r14)
            java.lang.String r7 = r0.toString()
            java.lang.String r1 = ""
            boolean r8 = com.shoujiduoduo.util.t.a(r13, r7, r2, r5, r5)
            java.lang.String r4 = "dnsdetector"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            if (r15 == 0) goto L_0x0139
            java.lang.String r0 = "CDN_check"
        L_0x0058:
            java.lang.StringBuilder r0 = r5.append(r0)
            java.lang.String r5 = "download file success:"
            java.lang.StringBuilder r0 = r0.append(r5)
            java.lang.StringBuilder r0 = r0.append(r8)
            java.lang.String r0 = r0.toString()
            com.shoujiduoduo.base.a.a.a(r4, r0)
            if (r8 == 0) goto L_0x0240
            boolean r0 = com.shoujiduoduo.util.q.f(r7)
            if (r0 == 0) goto L_0x0240
            r5 = 0
            java.io.FileInputStream r4 = new java.io.FileInputStream     // Catch:{ FileNotFoundException -> 0x0149, IOException -> 0x015e, all -> 0x0173 }
            r4.<init>(r7)     // Catch:{ FileNotFoundException -> 0x0149, IOException -> 0x015e, all -> 0x0173 }
            r0 = 256(0x100, float:3.59E-43)
            byte[] r0 = new byte[r0]     // Catch:{ FileNotFoundException -> 0x0231, IOException -> 0x0228 }
            int r5 = r4.read(r0)     // Catch:{ FileNotFoundException -> 0x0231, IOException -> 0x0228 }
            r7 = -1
            if (r5 == r7) goto L_0x023d
            java.lang.String r7 = new java.lang.String     // Catch:{ FileNotFoundException -> 0x0231, IOException -> 0x0228 }
            r9 = 0
            r7.<init>(r0, r9, r5)     // Catch:{ FileNotFoundException -> 0x0231, IOException -> 0x0228 }
            java.lang.String r0 = r7.trim()     // Catch:{ FileNotFoundException -> 0x0231, IOException -> 0x0228 }
        L_0x0090:
            java.lang.String r5 = "dnsdetector"
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x0237, IOException -> 0x022e }
            r7.<init>()     // Catch:{ FileNotFoundException -> 0x0237, IOException -> 0x022e }
            if (r15 == 0) goto L_0x013d
            java.lang.String r1 = "CDN_check"
        L_0x009b:
            java.lang.StringBuilder r1 = r7.append(r1)     // Catch:{ FileNotFoundException -> 0x0237, IOException -> 0x022e }
            java.lang.String r7 = " verify file content:"
            java.lang.StringBuilder r1 = r1.append(r7)     // Catch:{ FileNotFoundException -> 0x0237, IOException -> 0x022e }
            java.lang.StringBuilder r1 = r1.append(r0)     // Catch:{ FileNotFoundException -> 0x0237, IOException -> 0x022e }
            java.lang.String r1 = r1.toString()     // Catch:{ FileNotFoundException -> 0x0237, IOException -> 0x022e }
            com.shoujiduoduo.base.a.a.a(r5, r1)     // Catch:{ FileNotFoundException -> 0x0237, IOException -> 0x022e }
            java.lang.String r1 = "ok"
            boolean r1 = r1.equalsIgnoreCase(r0)     // Catch:{ FileNotFoundException -> 0x0237, IOException -> 0x022e }
            if (r1 != 0) goto L_0x00cd
            int r1 = r0.length()     // Catch:{ FileNotFoundException -> 0x0237, IOException -> 0x022e }
            if (r1 <= r10) goto L_0x023a
            r1 = 0
            r5 = 256(0x100, float:3.59E-43)
            java.lang.String r1 = r0.substring(r1, r5)     // Catch:{ FileNotFoundException -> 0x0237, IOException -> 0x022e }
            r5 = r1
        L_0x00c6:
            if (r15 == 0) goto L_0x0141
            java.lang.String r1 = "cdn_file_check_1"
        L_0x00ca:
            com.shoujiduoduo.util.t.b(r1, r5)     // Catch:{ FileNotFoundException -> 0x0237, IOException -> 0x022e }
        L_0x00cd:
            if (r4 == 0) goto L_0x00d2
            r4.close()     // Catch:{ IOException -> 0x0144 }
        L_0x00d2:
            java.util.HashMap r4 = new java.util.HashMap
            r4.<init>()
            java.lang.String r1 = "network"
            boolean r1 = r6.equals(r1)
            if (r1 == 0) goto L_0x0193
            if (r8 == 0) goto L_0x0180
            java.lang.String r1 = "ok"
            boolean r1 = r1.equalsIgnoreCase(r0)
            if (r1 == 0) goto L_0x0180
            r1 = r2
        L_0x00ea:
            java.lang.String r3 = "dnsdetector"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            if (r15 == 0) goto L_0x0183
            java.lang.String r2 = "CDN_check"
        L_0x00f5:
            java.lang.StringBuilder r2 = r5.append(r2)
            java.lang.String r5 = " [isVerifyFileOk]:"
            java.lang.StringBuilder r2 = r2.append(r5)
            java.lang.StringBuilder r2 = r2.append(r1)
            java.lang.String r2 = r2.toString()
            com.shoujiduoduo.base.a.a.a(r3, r2)
            java.lang.String r3 = "downSuc"
            if (r8 == 0) goto L_0x0187
            java.lang.String r2 = "true"
        L_0x0110:
            r4.put(r3, r2)
            java.lang.String r2 = "contentSuc"
            java.lang.String r3 = "ok"
            boolean r0 = r3.equalsIgnoreCase(r0)
            if (r0 == 0) goto L_0x018a
            java.lang.String r0 = "true"
        L_0x011f:
            r4.put(r2, r0)
            java.lang.String r2 = "res"
            if (r1 == 0) goto L_0x018d
            java.lang.String r0 = "true"
        L_0x0128:
            r4.put(r2, r0)
            android.content.Context r2 = com.shoujiduoduo.ringtone.RingDDApp.c()
            if (r15 == 0) goto L_0x0190
            java.lang.String r0 = "cdn_file_check_by_network"
        L_0x0133:
            com.umeng.a.b.a(r2, r0, r4)
            r2 = r1
            goto L_0x0018
        L_0x0139:
            java.lang.String r0 = "duoduo_check"
            goto L_0x0058
        L_0x013d:
            java.lang.String r1 = "duoduo_check"
            goto L_0x009b
        L_0x0141:
            java.lang.String r1 = "duoduo_file_check_1"
            goto L_0x00ca
        L_0x0144:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x00d2
        L_0x0149:
            r0 = move-exception
            r4 = r5
            r11 = r0
            r0 = r1
            r1 = r11
        L_0x014e:
            r1.printStackTrace()     // Catch:{ all -> 0x0225 }
            if (r4 == 0) goto L_0x00d2
            r4.close()     // Catch:{ IOException -> 0x0158 }
            goto L_0x00d2
        L_0x0158:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x00d2
        L_0x015e:
            r0 = move-exception
            r4 = r5
            r11 = r0
            r0 = r1
            r1 = r11
        L_0x0163:
            r1.printStackTrace()     // Catch:{ all -> 0x0225 }
            if (r4 == 0) goto L_0x00d2
            r4.close()     // Catch:{ IOException -> 0x016d }
            goto L_0x00d2
        L_0x016d:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x00d2
        L_0x0173:
            r0 = move-exception
            r4 = r5
        L_0x0175:
            if (r4 == 0) goto L_0x017a
            r4.close()     // Catch:{ IOException -> 0x017b }
        L_0x017a:
            throw r0
        L_0x017b:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x017a
        L_0x0180:
            r1 = r3
            goto L_0x00ea
        L_0x0183:
            java.lang.String r2 = "duoduo_check"
            goto L_0x00f5
        L_0x0187:
            java.lang.String r2 = "false"
            goto L_0x0110
        L_0x018a:
            java.lang.String r0 = "false"
            goto L_0x011f
        L_0x018d:
            java.lang.String r0 = "false"
            goto L_0x0128
        L_0x0190:
            java.lang.String r0 = "duoduo_file_check_by_network"
            goto L_0x0133
        L_0x0193:
            java.lang.String r1 = "content"
            boolean r1 = r6.equals(r1)
            if (r1 == 0) goto L_0x0204
            if (r8 == 0) goto L_0x01a5
            java.lang.String r1 = "ok"
            boolean r1 = r1.equalsIgnoreCase(r0)
            if (r1 == 0) goto L_0x01a6
        L_0x01a5:
            r3 = r2
        L_0x01a6:
            java.lang.String r2 = "dnsdetector"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            if (r15 == 0) goto L_0x01f5
            java.lang.String r1 = "CDN_check"
        L_0x01b1:
            java.lang.StringBuilder r1 = r5.append(r1)
            java.lang.String r5 = " [isVerifyFileOk]:"
            java.lang.StringBuilder r1 = r1.append(r5)
            java.lang.StringBuilder r1 = r1.append(r3)
            java.lang.String r1 = r1.toString()
            com.shoujiduoduo.base.a.a.a(r2, r1)
            java.lang.String r2 = "downSuc"
            if (r8 == 0) goto L_0x01f8
            java.lang.String r1 = "true"
        L_0x01cc:
            r4.put(r2, r1)
            java.lang.String r1 = "contentSuc"
            java.lang.String r2 = "ok"
            boolean r0 = r2.equalsIgnoreCase(r0)
            if (r0 == 0) goto L_0x01fb
            java.lang.String r0 = "true"
        L_0x01db:
            r4.put(r1, r0)
            java.lang.String r1 = "res"
            if (r3 == 0) goto L_0x01fe
            java.lang.String r0 = "true"
        L_0x01e4:
            r4.put(r1, r0)
            android.content.Context r1 = com.shoujiduoduo.ringtone.RingDDApp.c()
            if (r15 == 0) goto L_0x0201
            java.lang.String r0 = "cdn_file_check_by_content"
        L_0x01ef:
            com.umeng.a.b.a(r1, r0, r4)
            r2 = r3
            goto L_0x0018
        L_0x01f5:
            java.lang.String r1 = "duoduo_check"
            goto L_0x01b1
        L_0x01f8:
            java.lang.String r1 = "false"
            goto L_0x01cc
        L_0x01fb:
            java.lang.String r0 = "false"
            goto L_0x01db
        L_0x01fe:
            java.lang.String r0 = "false"
            goto L_0x01e4
        L_0x0201:
            java.lang.String r0 = "duoduo_file_check_by_content"
            goto L_0x01ef
        L_0x0204:
            java.lang.String r1 = "dnsdetector"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            if (r15 == 0) goto L_0x0222
            java.lang.String r0 = "CDN_check"
        L_0x020f:
            java.lang.StringBuilder r0 = r3.append(r0)
            java.lang.String r3 = " [isVerifyFileOk] , 默认返回true"
            java.lang.StringBuilder r0 = r0.append(r3)
            java.lang.String r0 = r0.toString()
            com.shoujiduoduo.base.a.a.a(r1, r0)
            goto L_0x0018
        L_0x0222:
            java.lang.String r0 = "duoduo_check"
            goto L_0x020f
        L_0x0225:
            r0 = move-exception
            goto L_0x0175
        L_0x0228:
            r0 = move-exception
            r11 = r0
            r0 = r1
            r1 = r11
            goto L_0x0163
        L_0x022e:
            r1 = move-exception
            goto L_0x0163
        L_0x0231:
            r0 = move-exception
            r11 = r0
            r0 = r1
            r1 = r11
            goto L_0x014e
        L_0x0237:
            r1 = move-exception
            goto L_0x014e
        L_0x023a:
            r5 = r0
            goto L_0x00c6
        L_0x023d:
            r0 = r1
            goto L_0x0090
        L_0x0240:
            r0 = r1
            goto L_0x00d2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.shoujiduoduo.util.m.a(java.lang.String, java.lang.String, boolean):boolean");
    }

    public String b() {
        this.k.lock();
        try {
            return e;
        } finally {
            this.k.unlock();
        }
    }

    public String c() {
        this.k.lock();
        try {
            return g;
        } finally {
            this.k.unlock();
        }
    }

    /* compiled from: DnsDetector */
    public class a extends Thread {
        private InetAddress b;
        private String c;

        public a(String str) {
            this.c = str;
        }

        public void run() {
            try {
                a(InetAddress.getByName(this.c));
            } catch (UnknownHostException e) {
            }
        }

        private synchronized void a(InetAddress inetAddress) {
            this.b = inetAddress;
        }

        public synchronized String a() {
            String str;
            if (this.b != null) {
                str = this.b.getHostAddress();
            } else {
                str = null;
            }
            return str;
        }
    }
}
