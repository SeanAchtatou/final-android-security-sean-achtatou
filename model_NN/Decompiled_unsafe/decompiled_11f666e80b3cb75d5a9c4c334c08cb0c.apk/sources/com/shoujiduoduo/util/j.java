package com.shoujiduoduo.util;

import android.util.Base64;
import com.a.a.e;
import com.a.a.p;
import com.baidu.mobads.interfaces.IXAdRequestInfo;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.base.bean.ArtistData;
import com.shoujiduoduo.base.bean.CollectData;
import com.shoujiduoduo.base.bean.CommentData;
import com.shoujiduoduo.base.bean.ListContent;
import com.shoujiduoduo.base.bean.MakeRingData;
import com.shoujiduoduo.base.bean.MessageData;
import com.shoujiduoduo.base.bean.RingData;
import com.shoujiduoduo.base.bean.UserData;
import com.sina.weibo.sdk.constant.WBConstants;
import com.sina.weibo.sdk.constant.WBPageConstants;
import com.sina.weibo.sdk.register.mobile.SelectCountryActivity;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/* compiled from: DataParse */
public class j {
    public static ListContent<CollectData> a(String str) {
        try {
            return e(new FileInputStream(str));
        } catch (ArrayIndexOutOfBoundsException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e2) {
            e2.printStackTrace();
        }
        return null;
    }

    public static UserData b(String str) {
        DocumentBuilder newDocumentBuilder;
        Document parse;
        Element documentElement;
        NodeList elementsByTagName;
        if (ah.c(str)) {
            return null;
        }
        try {
            DocumentBuilderFactory newInstance = DocumentBuilderFactory.newInstance();
            if (newInstance == null || (newDocumentBuilder = newInstance.newDocumentBuilder()) == null || (parse = newDocumentBuilder.parse(new InputSource(new StringReader(str)))) == null || (documentElement = parse.getDocumentElement()) == null || (elementsByTagName = documentElement.getElementsByTagName("user")) == null || elementsByTagName.getLength() == 0) {
                return null;
            }
            NamedNodeMap attributes = elementsByTagName.item(0).getAttributes();
            UserData userData = new UserData();
            a(attributes, userData);
            return userData;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        } catch (DOMException e2) {
            e2.printStackTrace();
            return null;
        } catch (SAXException e3) {
            e3.printStackTrace();
            return null;
        } catch (ParserConfigurationException e4) {
            e4.printStackTrace();
            return null;
        } catch (ArrayIndexOutOfBoundsException e5) {
            e5.printStackTrace();
            return null;
        }
    }

    public static ListContent<CommentData> a(InputStream inputStream, boolean z) {
        boolean z2;
        JSONArray optJSONArray;
        String a2 = u.a(inputStream);
        if (a2 == null) {
            return null;
        }
        try {
            JSONObject jSONObject = (JSONObject) new JSONTokener(a2).nextValue();
            ListContent<CommentData> listContent = new ListContent<>();
            ArrayList<T> arrayList = new ArrayList<>();
            JSONObject optJSONObject = jSONObject.optJSONObject("hot");
            if (!(optJSONObject == null || (optJSONArray = optJSONObject.optJSONArray("list")) == null)) {
                for (int i = 0; i < optJSONArray.length(); i++) {
                    JSONObject optJSONObject2 = optJSONArray.optJSONObject(i);
                    if (optJSONObject2 != null) {
                        CommentData commentData = new CommentData();
                        if (i == 0 && !z) {
                            commentData.catetoryHint = "最热评论";
                        }
                        a(optJSONObject2, commentData);
                        arrayList.add(commentData);
                    }
                }
            }
            listContent.hotCommentNum = arrayList.size();
            JSONObject optJSONObject3 = jSONObject.optJSONObject("new");
            if (optJSONObject3 != null) {
                if (optJSONObject3.optInt("hasmore") != 0) {
                    z2 = true;
                } else {
                    z2 = false;
                }
                JSONArray optJSONArray2 = optJSONObject3.optJSONArray("list");
                if (optJSONArray2 != null) {
                    for (int i2 = 0; i2 < optJSONArray2.length(); i2++) {
                        JSONObject optJSONObject4 = optJSONArray2.optJSONObject(i2);
                        if (optJSONObject4 != null) {
                            CommentData commentData2 = new CommentData();
                            if (i2 == 0 && !z) {
                                commentData2.catetoryHint = "最新评论";
                            }
                            a(optJSONObject4, commentData2);
                            arrayList.add(commentData2);
                        }
                    }
                }
            } else {
                z2 = false;
            }
            listContent.data = arrayList;
            listContent.hasMore = z2;
            return listContent;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static ListContent<RingData> a(InputStream inputStream) {
        boolean z;
        String a2 = u.a(inputStream);
        if (a2 == null) {
            return null;
        }
        try {
            JSONObject jSONObject = (JSONObject) new JSONTokener(a2).nextValue();
            ListContent<RingData> listContent = new ListContent<>();
            ArrayList<T> arrayList = new ArrayList<>();
            if (jSONObject.optInt("hasmore") != 0) {
                z = true;
            } else {
                z = false;
            }
            JSONArray optJSONArray = jSONObject.optJSONArray("list");
            if (optJSONArray != null) {
                for (int i = 0; i < optJSONArray.length(); i++) {
                    JSONObject optJSONObject = optJSONArray.optJSONObject(i);
                    if (optJSONObject != null) {
                        RingData ringData = new RingData();
                        a(optJSONObject, ringData);
                        arrayList.add(ringData);
                    }
                }
            }
            listContent.data = arrayList;
            listContent.hasMore = z;
            return listContent;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    private static void a(JSONObject jSONObject, RingData ringData) {
        ringData.name = jSONObject.optString(SelectCountryActivity.EXTRA_COUNTRY_NAME);
        ringData.artist = jSONObject.optString("artist");
        ringData.uid = jSONObject.optString("uid");
        ringData.duration = jSONObject.optInt("duration", 0);
        ringData.score = jSONObject.optInt(WBConstants.GAME_PARAMS_SCORE, 0);
        ringData.playcnt = jSONObject.optInt("playcnt", 0);
        ringData.rid = jSONObject.optString("rid");
        ringData.baiduURL = jSONObject.optString("bdurl");
        ringData.setHighAACBitrate(jSONObject.optInt("hbr", 0));
        ringData.setHighAACURL(jSONObject.optString("hurl"));
        ringData.setLowAACBitrate(jSONObject.optInt("lbr", 0));
        ringData.setLowAACURL(jSONObject.optString("lurl"));
        ringData.setMp3Bitrate(jSONObject.optInt("mp3br", 0));
        ringData.setMp3URL(jSONObject.optString("mp3url"));
        ringData.isNew = jSONObject.optInt("isnew", 0);
        ringData.cid = jSONObject.optString(IXAdRequestInfo.CELL_ID);
        ringData.valid = jSONObject.optString("valid");
        ringData.singerId = jSONObject.optString("singerId");
        ringData.price = r.a(jSONObject.optString("price"), 0);
        ringData.hasmedia = r.a(jSONObject.optString("hasmedia"), 0);
        ringData.ctcid = jSONObject.optString("ctcid");
        ringData.ctvalid = jSONObject.optString("ctvalid");
        ringData.ctprice = r.a(jSONObject.optString("ctprice"), 0);
        ringData.cthasmedia = r.a(jSONObject.optString("cthasmedia"), 0);
        ringData.ctVip = r.a(jSONObject.optString("ctvip"), 0);
        ringData.ctWavUrl = jSONObject.optString("wavurl");
        ringData.cuvip = r.a(jSONObject.optString("cuvip"), 0);
        ringData.cuftp = jSONObject.optString("cuftp");
        ringData.cucid = jSONObject.optString("cucid");
        ringData.cusid = jSONObject.optString("cusid");
        ringData.cuurl = jSONObject.optString("cuurl");
        ringData.cuvalid = jSONObject.optString("cuvalid");
        ringData.hasshow = r.a(jSONObject.optString("hasshow"), 0);
        ringData.isHot = r.a(jSONObject.optString("ishot"), 0);
        ringData.userHead = jSONObject.optString("head_url");
        ringData.commentNum = jSONObject.optInt("comment_num", 0);
        ringData.date = jSONObject.optString("date");
    }

    private static void a(JSONObject jSONObject, CommentData commentData) {
        commentData.rid = jSONObject.optString("rid");
        commentData.uid = jSONObject.optString("uid");
        commentData.tuid = jSONObject.optString("tuid");
        commentData.ddid = jSONObject.optString("ddid");
        commentData.tcid = jSONObject.optString("tcid");
        commentData.comment = jSONObject.optString("comment");
        commentData.tcomment = jSONObject.optString("tcomment");
        commentData.name = jSONObject.optString(SelectCountryActivity.EXTRA_COUNTRY_NAME);
        commentData.head_url = jSONObject.optString("head_url");
        commentData.tname = jSONObject.optString("tname");
        commentData.thead_url = jSONObject.optString("thead_url");
        commentData.cid = jSONObject.optString(IXAdRequestInfo.CELL_ID);
        commentData.createtime = jSONObject.optString("createtime");
        commentData.upvote = jSONObject.optInt("upvote");
    }

    public static ListContent<UserData> b(InputStream inputStream) {
        DocumentBuilder newDocumentBuilder;
        Document parse;
        Element documentElement;
        try {
            DocumentBuilderFactory newInstance = DocumentBuilderFactory.newInstance();
            if (newInstance == null || (newDocumentBuilder = newInstance.newDocumentBuilder()) == null || inputStream == null || (parse = newDocumentBuilder.parse(inputStream)) == null || (documentElement = parse.getDocumentElement()) == null) {
                return null;
            }
            boolean equalsIgnoreCase = documentElement.getAttribute("hasmore").equalsIgnoreCase("true");
            String attribute = documentElement.getAttribute("area");
            String attribute2 = documentElement.getAttribute("baseurl");
            String attribute3 = documentElement.getAttribute("sig");
            int a2 = r.a(documentElement.getAttribute(WBPageConstants.ParamKey.PAGE), -1);
            NodeList elementsByTagName = documentElement.getElementsByTagName("user");
            if (elementsByTagName == null) {
                return null;
            }
            ArrayList<T> arrayList = new ArrayList<>();
            for (int i = 0; i < elementsByTagName.getLength(); i++) {
                NamedNodeMap attributes = elementsByTagName.item(i).getAttributes();
                UserData userData = new UserData();
                a(attributes, userData);
                arrayList.add(userData);
            }
            ListContent<UserData> listContent = new ListContent<>();
            listContent.data = arrayList;
            listContent.hasMore = equalsIgnoreCase;
            listContent.area = attribute;
            listContent.baseURL = attribute2;
            listContent.sig = attribute3;
            listContent.page = a2;
            return listContent;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        } catch (DOMException e2) {
            e2.printStackTrace();
            return null;
        } catch (SAXException e3) {
            e3.printStackTrace();
            return null;
        } catch (ParserConfigurationException e4) {
            e4.printStackTrace();
            return null;
        } catch (ArrayIndexOutOfBoundsException e5) {
            e5.printStackTrace();
            return null;
        }
    }

    public static ListContent<MessageData> c(InputStream inputStream) {
        boolean z;
        String a2 = u.a(inputStream);
        if (a2 == null) {
            return null;
        }
        try {
            JSONObject jSONObject = (JSONObject) new JSONTokener(a2).nextValue();
            ListContent<MessageData> listContent = new ListContent<>();
            ArrayList<T> arrayList = new ArrayList<>();
            e eVar = new e();
            if (jSONObject.optInt("hasmore") != 0) {
                z = true;
            } else {
                z = false;
            }
            JSONArray optJSONArray = jSONObject.optJSONArray("list");
            if (optJSONArray != null) {
                for (int i = 0; i < optJSONArray.length(); i++) {
                    JSONObject optJSONObject = optJSONArray.optJSONObject(i);
                    if (optJSONObject != null) {
                        try {
                            String optString = optJSONObject.optString("feedtype");
                            if (optString.equals("ring") || optString.equals("comment")) {
                                arrayList.add((MessageData) eVar.a(optJSONObject.toString(), MessageData.class));
                            } else {
                                a.a("DataParse", "not support message type");
                            }
                        } catch (p e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            listContent.data = arrayList;
            listContent.hasMore = z;
            return listContent;
        } catch (JSONException e2) {
            e2.printStackTrace();
            return null;
        }
    }

    public static ListContent<ArtistData> d(InputStream inputStream) {
        DocumentBuilder newDocumentBuilder;
        Document parse;
        Element documentElement;
        try {
            DocumentBuilderFactory newInstance = DocumentBuilderFactory.newInstance();
            if (newInstance == null || (newDocumentBuilder = newInstance.newDocumentBuilder()) == null || inputStream == null || (parse = newDocumentBuilder.parse(inputStream)) == null || (documentElement = parse.getDocumentElement()) == null) {
                return null;
            }
            boolean equalsIgnoreCase = documentElement.getAttribute("hasmore").equalsIgnoreCase("true");
            String attribute = documentElement.getAttribute("area");
            String attribute2 = documentElement.getAttribute("baseurl");
            String attribute3 = documentElement.getAttribute("sig");
            int a2 = r.a(documentElement.getAttribute(WBPageConstants.ParamKey.PAGE), -1);
            NodeList elementsByTagName = documentElement.getElementsByTagName("artist");
            if (elementsByTagName == null) {
                return null;
            }
            ArrayList<T> arrayList = new ArrayList<>();
            for (int i = 0; i < elementsByTagName.getLength(); i++) {
                NamedNodeMap attributes = elementsByTagName.item(i).getAttributes();
                ArtistData artistData = new ArtistData();
                a(attributes, artistData);
                arrayList.add(artistData);
            }
            ListContent<ArtistData> listContent = new ListContent<>();
            listContent.data = arrayList;
            listContent.hasMore = equalsIgnoreCase;
            listContent.area = attribute;
            listContent.baseURL = attribute2;
            listContent.sig = attribute3;
            listContent.page = a2;
            return listContent;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        } catch (DOMException e2) {
            e2.printStackTrace();
            return null;
        } catch (SAXException e3) {
            e3.printStackTrace();
            return null;
        } catch (ParserConfigurationException e4) {
            e4.printStackTrace();
            return null;
        } catch (ArrayIndexOutOfBoundsException e5) {
            e5.printStackTrace();
            return null;
        }
    }

    public static ListContent<CollectData> e(InputStream inputStream) {
        DocumentBuilder newDocumentBuilder;
        Document parse;
        Element documentElement;
        try {
            DocumentBuilderFactory newInstance = DocumentBuilderFactory.newInstance();
            if (newInstance == null || (newDocumentBuilder = newInstance.newDocumentBuilder()) == null || inputStream == null || (parse = newDocumentBuilder.parse(inputStream)) == null || (documentElement = parse.getDocumentElement()) == null) {
                return null;
            }
            boolean equalsIgnoreCase = documentElement.getAttribute("hasmore").equalsIgnoreCase("true");
            String attribute = documentElement.getAttribute("area");
            String attribute2 = documentElement.getAttribute("baseurl");
            String attribute3 = documentElement.getAttribute("sig");
            int a2 = r.a(documentElement.getAttribute(WBPageConstants.ParamKey.PAGE), -1);
            NodeList elementsByTagName = documentElement.getElementsByTagName("collect");
            if (elementsByTagName == null) {
                return null;
            }
            ArrayList<T> arrayList = new ArrayList<>();
            for (int i = 0; i < elementsByTagName.getLength(); i++) {
                NamedNodeMap attributes = elementsByTagName.item(i).getAttributes();
                CollectData collectData = new CollectData();
                a(attributes, collectData);
                arrayList.add(collectData);
            }
            ListContent<CollectData> listContent = new ListContent<>();
            listContent.data = arrayList;
            listContent.hasMore = equalsIgnoreCase;
            listContent.area = attribute;
            listContent.baseURL = attribute2;
            listContent.sig = attribute3;
            listContent.page = a2;
            return listContent;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        } catch (DOMException e2) {
            e2.printStackTrace();
            return null;
        } catch (SAXException e3) {
            e3.printStackTrace();
            return null;
        } catch (ParserConfigurationException e4) {
            e4.printStackTrace();
            return null;
        } catch (ArrayIndexOutOfBoundsException e5) {
            e5.printStackTrace();
            return null;
        }
    }

    public static ListContent<RingData> f(InputStream inputStream) {
        DocumentBuilder newDocumentBuilder;
        Document parse;
        Element documentElement;
        try {
            DocumentBuilderFactory newInstance = DocumentBuilderFactory.newInstance();
            if (newInstance == null || (newDocumentBuilder = newInstance.newDocumentBuilder()) == null || inputStream == null || (parse = newDocumentBuilder.parse(inputStream)) == null || (documentElement = parse.getDocumentElement()) == null) {
                return null;
            }
            boolean equalsIgnoreCase = documentElement.getAttribute("hasmore").equalsIgnoreCase("true");
            String attribute = documentElement.getAttribute("area");
            String attribute2 = documentElement.getAttribute("baseurl");
            String attribute3 = documentElement.getAttribute("sig");
            int a2 = r.a(documentElement.getAttribute(WBPageConstants.ParamKey.PAGE), -1);
            NodeList elementsByTagName = documentElement.getElementsByTagName("ring");
            if (elementsByTagName == null) {
                return null;
            }
            ArrayList<T> arrayList = new ArrayList<>();
            for (int i = 0; i < elementsByTagName.getLength(); i++) {
                NamedNodeMap attributes = elementsByTagName.item(i).getAttributes();
                RingData ringData = new RingData();
                ringData.setBaseUrl(attribute2);
                a(attributes, ringData);
                arrayList.add(ringData);
            }
            ListContent<RingData> listContent = new ListContent<>();
            listContent.data = arrayList;
            listContent.hasMore = equalsIgnoreCase;
            listContent.area = attribute;
            listContent.baseURL = attribute2;
            listContent.sig = attribute3;
            listContent.page = a2;
            return listContent;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        } catch (DOMException e2) {
            e2.printStackTrace();
            return null;
        } catch (SAXException e3) {
            e3.printStackTrace();
            return null;
        } catch (ParserConfigurationException e4) {
            e4.printStackTrace();
            return null;
        } catch (ArrayIndexOutOfBoundsException e5) {
            e5.printStackTrace();
            return null;
        }
    }

    public static ListContent<RingData> g(InputStream inputStream) {
        DocumentBuilder newDocumentBuilder;
        Document parse;
        Element documentElement;
        try {
            DocumentBuilderFactory newInstance = DocumentBuilderFactory.newInstance();
            if (newInstance == null || (newDocumentBuilder = newInstance.newDocumentBuilder()) == null || inputStream == null || (parse = newDocumentBuilder.parse(inputStream)) == null || (documentElement = parse.getDocumentElement()) == null) {
                return null;
            }
            boolean equalsIgnoreCase = documentElement.getAttribute("hasmore").equalsIgnoreCase("true");
            String attribute = documentElement.getAttribute("area");
            String attribute2 = documentElement.getAttribute("baseurl");
            String attribute3 = documentElement.getAttribute("sig");
            int a2 = r.a(documentElement.getAttribute(WBPageConstants.ParamKey.PAGE), -1);
            NodeList elementsByTagName = documentElement.getElementsByTagName("ring");
            if (elementsByTagName == null) {
                return null;
            }
            ArrayList<T> arrayList = new ArrayList<>();
            HashMap<String, String> hashMap = new HashMap<>();
            for (int i = 0; i < elementsByTagName.getLength(); i++) {
                NamedNodeMap attributes = elementsByTagName.item(i).getAttributes();
                RingData ringData = new RingData();
                ringData.setBaseUrl(attribute2);
                a(attributes, ringData);
                String a3 = g.a(attributes, "frid", "");
                if (!ah.c(a3)) {
                    hashMap.put(a3, ringData.rid);
                }
                arrayList.add(ringData);
            }
            ListContent<RingData> listContent = new ListContent<>();
            listContent.data = arrayList;
            listContent.hasMore = equalsIgnoreCase;
            listContent.area = attribute;
            listContent.baseURL = attribute2;
            listContent.sig = attribute3;
            listContent.fadeRidMap = hashMap;
            listContent.page = a2;
            return listContent;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        } catch (DOMException e2) {
            e2.printStackTrace();
            return null;
        } catch (SAXException e3) {
            e3.printStackTrace();
            return null;
        } catch (ParserConfigurationException e4) {
            e4.printStackTrace();
            return null;
        } catch (ArrayIndexOutOfBoundsException e5) {
            e5.printStackTrace();
            return null;
        }
    }

    public static ListContent<MakeRingData> h(InputStream inputStream) {
        DocumentBuilder newDocumentBuilder;
        Document parse;
        Element documentElement;
        try {
            DocumentBuilderFactory newInstance = DocumentBuilderFactory.newInstance();
            if (newInstance == null || (newDocumentBuilder = newInstance.newDocumentBuilder()) == null || inputStream == null || (parse = newDocumentBuilder.parse(inputStream)) == null || (documentElement = parse.getDocumentElement()) == null) {
                return null;
            }
            boolean equalsIgnoreCase = documentElement.getAttribute("hasmore").equalsIgnoreCase("true");
            String attribute = documentElement.getAttribute("area");
            String attribute2 = documentElement.getAttribute("baseurl");
            String attribute3 = documentElement.getAttribute("sig");
            int a2 = r.a(documentElement.getAttribute(WBPageConstants.ParamKey.PAGE), -1);
            NodeList elementsByTagName = documentElement.getElementsByTagName("ring");
            if (elementsByTagName == null) {
                return null;
            }
            ArrayList<T> arrayList = new ArrayList<>();
            a.a("DataParse", "parseContent: listRing size = " + arrayList.size());
            for (int i = 0; i < elementsByTagName.getLength(); i++) {
                NamedNodeMap attributes = elementsByTagName.item(i).getAttributes();
                MakeRingData makeRingData = new MakeRingData();
                makeRingData.setBaseUrl(attribute2);
                a(attributes, makeRingData);
                makeRingData.makeDate = g.a(attributes, "date");
                makeRingData.upload = r.a(g.a(attributes, "upload"), 0);
                makeRingData.makeType = r.a(g.a(attributes, "makeType"), 0);
                makeRingData.localPath = g.a(attributes, "localPath");
                arrayList.add(makeRingData);
            }
            ListContent<MakeRingData> listContent = new ListContent<>();
            listContent.data = arrayList;
            listContent.hasMore = equalsIgnoreCase;
            listContent.area = attribute;
            listContent.baseURL = attribute2;
            listContent.sig = attribute3;
            listContent.page = a2;
            return listContent;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        } catch (DOMException e2) {
            e2.printStackTrace();
            return null;
        } catch (SAXException e3) {
            e3.printStackTrace();
            return null;
        } catch (ParserConfigurationException e4) {
            e4.printStackTrace();
            return null;
        } catch (ArrayIndexOutOfBoundsException e5) {
            e5.printStackTrace();
            return null;
        }
    }

    private static void a(NamedNodeMap namedNodeMap, UserData userData) {
        boolean z;
        userData.userName = g.a(namedNodeMap, SelectCountryActivity.EXTRA_COUNTRY_NAME);
        userData.userName = new String(Base64.decode(userData.userName, 0));
        userData.uid = g.a(namedNodeMap, "uid");
        userData.ringsNum = g.a(namedNodeMap, "rings_num", 0);
        userData.followerNum = g.a(namedNodeMap, "follower_num", 0);
        userData.followingNum = g.a(namedNodeMap, "following_num", 0);
        userData.headUrl = g.a(namedNodeMap, "head_url");
        userData.sex = g.a(namedNodeMap, "sex", "未知");
        userData.ddid = g.a(namedNodeMap, "ddid");
        userData.intro = r.c(g.a(namedNodeMap, "intro"));
        userData.bgurl = g.a(namedNodeMap, "bgurl");
        if (g.a(namedNodeMap, "followed", 0) != 0) {
            z = true;
        } else {
            z = false;
        }
        userData.followed = z;
        userData.isSuperUser = g.a(namedNodeMap, "superuser", 0);
        userData.followings = g.a(namedNodeMap, "followings");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.shoujiduoduo.util.g.a(org.w3c.dom.NamedNodeMap, java.lang.String, boolean):boolean
     arg types: [org.w3c.dom.NamedNodeMap, java.lang.String, int]
     candidates:
      com.shoujiduoduo.util.g.a(org.w3c.dom.NamedNodeMap, java.lang.String, int):int
      com.shoujiduoduo.util.g.a(org.w3c.dom.NamedNodeMap, java.lang.String, java.lang.String):java.lang.String
      com.shoujiduoduo.util.g.a(android.content.Context, java.lang.String, int):void
      com.shoujiduoduo.util.g.a(org.w3c.dom.NamedNodeMap, java.lang.String, boolean):boolean */
    private static void a(NamedNodeMap namedNodeMap, ArtistData artistData) {
        artistData.pic = g.a(namedNodeMap, "pic");
        artistData.isNew = g.a(namedNodeMap, "isNew", false);
        artistData.sale = r.a(g.a(namedNodeMap, "sale"), 100);
        artistData.work = g.a(namedNodeMap, "work");
        artistData.name = g.a(namedNodeMap, SelectCountryActivity.EXTRA_COUNTRY_NAME);
        artistData.id = g.a(namedNodeMap, "id");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.shoujiduoduo.util.g.a(org.w3c.dom.NamedNodeMap, java.lang.String, boolean):boolean
     arg types: [org.w3c.dom.NamedNodeMap, java.lang.String, int]
     candidates:
      com.shoujiduoduo.util.g.a(org.w3c.dom.NamedNodeMap, java.lang.String, int):int
      com.shoujiduoduo.util.g.a(org.w3c.dom.NamedNodeMap, java.lang.String, java.lang.String):java.lang.String
      com.shoujiduoduo.util.g.a(android.content.Context, java.lang.String, int):void
      com.shoujiduoduo.util.g.a(org.w3c.dom.NamedNodeMap, java.lang.String, boolean):boolean */
    private static void a(NamedNodeMap namedNodeMap, CollectData collectData) {
        collectData.pic = g.a(namedNodeMap, "pic");
        collectData.artist = g.a(namedNodeMap, "artist");
        collectData.title = g.a(namedNodeMap, "title");
        collectData.content = g.a(namedNodeMap, "content");
        collectData.time = g.a(namedNodeMap, "time");
        collectData.isNew = g.a(namedNodeMap, "isnew", false);
        collectData.favNum = g.a(namedNodeMap, "fav");
        collectData.id = g.a(namedNodeMap, "id");
    }

    private static void a(NamedNodeMap namedNodeMap, RingData ringData) {
        ringData.name = g.a(namedNodeMap, SelectCountryActivity.EXTRA_COUNTRY_NAME);
        ringData.artist = g.a(namedNodeMap, "artist");
        ringData.uid = g.a(namedNodeMap, "uid");
        ringData.duration = r.a(g.a(namedNodeMap, "duration"), 0);
        ringData.score = r.a(g.a(namedNodeMap, WBConstants.GAME_PARAMS_SCORE), 0);
        ringData.playcnt = r.a(g.a(namedNodeMap, "playcnt"), 0);
        ringData.rid = g.a(namedNodeMap, "rid");
        ringData.baiduURL = g.a(namedNodeMap, "bdurl");
        ringData.setHighAACBitrate(r.a(g.a(namedNodeMap, "hbr"), 0));
        ringData.setHighAACURL(g.a(namedNodeMap, "hurl"));
        ringData.setLowAACBitrate(r.a(g.a(namedNodeMap, "lbr"), 0));
        ringData.setLowAACURL(g.a(namedNodeMap, "lurl"));
        ringData.setMp3Bitrate(r.a(g.a(namedNodeMap, "mp3br"), 0));
        ringData.setMp3URL(g.a(namedNodeMap, "mp3url"));
        ringData.isNew = r.a(g.a(namedNodeMap, "isnew"), 0);
        ringData.cid = g.a(namedNodeMap, IXAdRequestInfo.CELL_ID);
        ringData.valid = g.a(namedNodeMap, "valid");
        ringData.singerId = g.a(namedNodeMap, "singerId");
        ringData.price = r.a(g.a(namedNodeMap, "price"), 0);
        ringData.hasmedia = r.a(g.a(namedNodeMap, "hasmedia"), 0);
        ringData.ctcid = g.a(namedNodeMap, "ctcid");
        ringData.ctvalid = g.a(namedNodeMap, "ctvalid");
        ringData.ctprice = r.a(g.a(namedNodeMap, "ctprice"), 0);
        ringData.cthasmedia = r.a(g.a(namedNodeMap, "cthasmedia"), 0);
        ringData.ctVip = r.a(g.a(namedNodeMap, "ctvip"), 0);
        ringData.ctWavUrl = g.a(namedNodeMap, "wavurl");
        ringData.cuvip = r.a(g.a(namedNodeMap, "cuvip"), 0);
        ringData.cuftp = g.a(namedNodeMap, "cuftp");
        ringData.cucid = g.a(namedNodeMap, "cucid");
        ringData.cusid = g.a(namedNodeMap, "cusid");
        ringData.cuurl = g.a(namedNodeMap, "cuurl");
        ringData.cuvalid = g.a(namedNodeMap, "cuvalid");
        ringData.hasshow = r.a(g.a(namedNodeMap, "hasshow"), 0);
        ringData.isHot = r.a(g.a(namedNodeMap, "ishot"), 0);
        ringData.userHead = g.a(namedNodeMap, "head_url");
        ringData.commentNum = g.a(namedNodeMap, "comment_num", 0);
        ringData.date = g.a(namedNodeMap, "date");
    }
}
