package com.shoujiduoduo.util.e;

import android.annotation.SuppressLint;
import android.text.TextUtils;
import cn.banshenggua.aichang.utils.StringUtil;
import com.meizu.cloud.pushsdk.pushtracer.constant.Parameters;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.util.d.d;
import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

/* compiled from: HttpUtils */
public class c {
    @SuppressLint({"SimpleDateFormat"})
    public static String a(List<NameValuePair> list, String str) {
        a.a("HttpUtils", "httpGetTool:methodName:" + str);
        if (list == null) {
            return null;
        }
        list.add(new BasicNameValuePair(LogBuilder.KEY_APPKEY, b.f2292a));
        list.add(new BasicNameValuePair(Parameters.TIMESTAMP, a()));
        for (NameValuePair next : list) {
            a.a("HttpUtils", "name:" + next.getName() + " , value:" + next.getValue());
        }
        DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
        StringBuilder sb = new StringBuilder();
        sb.append("http://api.10155.com" + str);
        sb.append("?");
        sb.append("digest=").append(a(list));
        for (int i = 0; i < list.size(); i++) {
            try {
                sb.append("&").append(list.get(i).getName()).append("=").append(URLEncoder.encode(list.get(i).getValue(), StringUtil.Encoding));
            } catch (UnsupportedEncodingException e) {
            }
        }
        if (sb.toString().endsWith("&")) {
            sb.deleteCharAt(sb.lastIndexOf("&"));
        }
        HttpGet httpGet = new HttpGet(sb.toString());
        a.a("HttpUtils", "url=" + sb.toString());
        try {
            HttpResponse execute = defaultHttpClient.execute(httpGet);
            a.a("HttpUtils", "method:" + str + ", status code:" + execute.getStatusLine().getStatusCode());
            if (execute.getStatusLine().getStatusCode() == 200) {
                HttpEntity entity = execute.getEntity();
                if (entity != null) {
                    InputStream content = entity.getContent();
                    if (content != null) {
                        byte[] bArr = new byte[2048];
                        BufferedInputStream bufferedInputStream = new BufferedInputStream(content);
                        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                        while (true) {
                            int read = bufferedInputStream.read(bArr);
                            if (read <= 0) {
                                break;
                            }
                            byteArrayOutputStream.write(bArr, 0, read);
                        }
                        bufferedInputStream.close();
                        content.close();
                        a.a("HttpUtils", "httpGet, method:" + str + ", content = " + byteArrayOutputStream.toString());
                        if (byteArrayOutputStream.toByteArray() != null) {
                            return byteArrayOutputStream.toString(StringUtil.Encoding);
                        }
                        return "OK";
                    }
                    a.c("HttpUtils", "http inputstream is null");
                } else {
                    a.c("HttpUtils", "entity is null");
                }
            } else {
                a.c("HttpUtils", "status is not 200");
            }
        } catch (ClientProtocolException e2) {
            e2.printStackTrace();
            a.c("http", "httpGet: ClientProtocolException catched!");
        } catch (IOException e3) {
            e3.printStackTrace();
            a.c("http", "httpGet: IOException catched!");
        }
        a.c("http", "method:" + str + ", return null");
        return null;
    }

    @SuppressLint({"SimpleDateFormat"})
    public static String a() {
        try {
            String a2 = com.umeng.b.a.a().a(RingDDApp.c(), "timestamp_url");
            if (TextUtils.isEmpty(a2)) {
                a2 = "http://www.shoujiduoduo.com";
            }
            URLConnection openConnection = new URL(a2).openConnection();
            openConnection.connect();
            String format = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date(openConnection.getDate()));
            a.a("HttpUtils", "online time:" + format);
            if (!format.startsWith("1970")) {
                return format;
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e2) {
            e2.printStackTrace();
        } catch (Exception e3) {
            e3.printStackTrace();
        }
        String format2 = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
        a.a("HttpUtils", "user local time:" + format2);
        return format2;
    }

    @SuppressLint({"SimpleDateFormat"})
    public static String a(List<NameValuePair> list, String str, String str2) {
        HttpEntity entity;
        InputStream content;
        a.a("HttpUtils", "httpPostTool:" + str2);
        if (list == null) {
            return null;
        }
        list.add(new BasicNameValuePair(LogBuilder.KEY_APPKEY, b.f2292a));
        list.add(new BasicNameValuePair(Parameters.TIMESTAMP, a()));
        for (NameValuePair next : list) {
            a.a("HttpUtils", "name:" + next.getName() + " , value:" + next.getValue());
        }
        StringBuilder sb = new StringBuilder();
        sb.append("http://api.10155.com" + str2);
        sb.append("?");
        sb.append("digest=").append(a(list));
        for (int i = 0; i < list.size(); i++) {
            try {
                sb.append("&").append(list.get(i).getName()).append("=").append(URLEncoder.encode(list.get(i).getValue(), StringUtil.Encoding));
            } catch (UnsupportedEncodingException e) {
            }
        }
        if (sb.toString().endsWith("&")) {
            sb.deleteCharAt(sb.lastIndexOf("&"));
        }
        HttpPost httpPost = new HttpPost(sb.toString());
        a.a("HttpUtils", "url=" + ((Object) sb));
        if (str2.equals("/v1/ringSetting/setTone")) {
            httpPost.setHeader("content-type", "application/x-www-form-urlencoded");
            try {
                httpPost.setEntity(new StringEntity(str, "UTF-8"));
            } catch (UnsupportedEncodingException e2) {
                e2.printStackTrace();
            }
        } else {
            try {
                httpPost.setEntity(new UrlEncodedFormEntity(list, "UTF-8"));
            } catch (UnsupportedEncodingException e3) {
                e3.printStackTrace();
            }
        }
        try {
            HttpResponse execute = new DefaultHttpClient().execute(httpPost);
            a.a("HttpUtils", "status code:" + execute.getStatusLine().getStatusCode());
            if (!(execute.getStatusLine().getStatusCode() != 200 || (entity = execute.getEntity()) == null || (content = entity.getContent()) == null)) {
                byte[] bArr = new byte[2048];
                BufferedInputStream bufferedInputStream = new BufferedInputStream(content);
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                while (true) {
                    int read = bufferedInputStream.read(bArr);
                    if (read <= 0) {
                        break;
                    }
                    byteArrayOutputStream.write(bArr, 0, read);
                }
                bufferedInputStream.close();
                content.close();
                a.a("HttpUtils", "httpPost: content = " + byteArrayOutputStream.toString());
                if (byteArrayOutputStream.toByteArray() != null) {
                    return byteArrayOutputStream.toString(StringUtil.Encoding);
                }
                return "OK";
            }
        } catch (ClientProtocolException e4) {
            e4.printStackTrace();
            a.c("http", "httpPost: ClientProtocolException catched!");
        } catch (IOException e5) {
            e5.printStackTrace();
            a.c("http", "httpPost: IOException catched!");
        }
        return null;
    }

    @SuppressLint({"SimpleDateFormat"})
    public static String b(List<NameValuePair> list, String str) {
        HttpEntity entity;
        InputStream content;
        int i = 0;
        a.a("HttpUtils", "httpPostTool:" + str);
        if (list == null) {
            return null;
        }
        list.add(new BasicNameValuePair(LogBuilder.KEY_APPKEY, b.f2292a));
        list.add(new BasicNameValuePair(Parameters.TIMESTAMP, a()));
        StringBuilder sb = new StringBuilder();
        sb.append("http://api.10155.com" + str);
        sb.append("?");
        sb.append("digest=").append(a(list));
        while (true) {
            int i2 = i;
            if (i2 >= list.size()) {
                break;
            }
            try {
                sb.append("&").append(list.get(i2).getName()).append("=").append(URLEncoder.encode(list.get(i2).getValue(), StringUtil.Encoding));
            } catch (UnsupportedEncodingException e) {
            }
            i = i2 + 1;
        }
        if (sb.toString().endsWith("&")) {
            sb.deleteCharAt(sb.lastIndexOf("&"));
        }
        HttpPost httpPost = new HttpPost(sb.toString());
        a.a("HttpUtils", "url=" + ((Object) sb));
        try {
            httpPost.setEntity(new UrlEncodedFormEntity(list, "UTF-8"));
        } catch (UnsupportedEncodingException e2) {
            e2.printStackTrace();
        }
        try {
            HttpResponse execute = new DefaultHttpClient().execute(httpPost);
            a.a("HttpUtils", "status code:" + execute.getStatusLine().getStatusCode());
            if (!(execute.getStatusLine().getStatusCode() != 200 || (entity = execute.getEntity()) == null || (content = entity.getContent()) == null)) {
                byte[] bArr = new byte[2048];
                BufferedInputStream bufferedInputStream = new BufferedInputStream(content);
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                while (true) {
                    int read = bufferedInputStream.read(bArr);
                    if (read <= 0) {
                        break;
                    }
                    byteArrayOutputStream.write(bArr, 0, read);
                }
                bufferedInputStream.close();
                content.close();
                a.a("HttpUtils", "httpPost: content = " + byteArrayOutputStream.toString());
                if (byteArrayOutputStream.toByteArray() != null) {
                    return byteArrayOutputStream.toString(StringUtil.Encoding);
                }
                return "OK";
            }
        } catch (ClientProtocolException e3) {
            e3.printStackTrace();
            a.c("http", "httpPost: ClientProtocolException catched!");
        } catch (IOException e4) {
            e4.printStackTrace();
            a.c("http", "httpPost: IOException catched!");
        }
        return null;
    }

    private static String a(List<NameValuePair> list) {
        Collections.sort(list, new Comparator<NameValuePair>() {
            /* renamed from: a */
            public int compare(NameValuePair nameValuePair, NameValuePair nameValuePair2) {
                return nameValuePair.toString().compareTo(nameValuePair2.toString());
            }
        });
        StringBuilder sb = new StringBuilder();
        for (NameValuePair next : list) {
            try {
                sb.append(next.getName()).append(URLEncoder.encode(next.getValue(), StringUtil.Encoding));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
        sb.append(b.b);
        return d.a(sb.toString());
    }
}
