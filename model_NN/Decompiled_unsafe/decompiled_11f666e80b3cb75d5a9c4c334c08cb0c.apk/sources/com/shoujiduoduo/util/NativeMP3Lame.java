package com.shoujiduoduo.util;

public class NativeMP3Lame {

    /* renamed from: a  reason: collision with root package name */
    private static boolean f2161a = y.a("mp3lame");
    private static NativeMP3Lame b = null;

    public native void destroyEncoder();

    public native int encodeSamples(short[] sArr, int i);

    public native int initEncoder(int i, int i2, int i3, int i4, int i5);

    public native int setTargetFile(String str);

    public static NativeMP3Lame a() {
        if (b == null) {
            b = new NativeMP3Lame();
        }
        return b;
    }
}
