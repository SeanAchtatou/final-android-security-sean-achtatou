package com.shoujiduoduo.util;

import cn.banshenggua.aichang.utils.Constants;
import com.shoujiduoduo.a.a.c;
import com.shoujiduoduo.a.c.f;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.sina.weibo.sdk.register.mobile.SelectCountryActivity;
import com.tencent.stat.DeviceInfo;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.DOMException;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/* compiled from: ServerConfig */
public class ac {
    /* access modifiers changed from: private */
    public static String c = l.b(0);
    private static final HashMap<String, String> e = new HashMap<>();

    /* renamed from: a  reason: collision with root package name */
    private ArrayList<b> f2173a;
    private ArrayList<c> b;
    /* access modifiers changed from: private */
    public boolean d;

    /* compiled from: ServerConfig */
    public enum a {
        DOMOB,
        GOOGLE,
        DUODUO,
        BAIDU,
        TENCENT,
        TAOBAO
    }

    /* compiled from: ServerConfig */
    private static class d {

        /* renamed from: a  reason: collision with root package name */
        public static ac f2180a = new ac();
    }

    private ac() {
        this.f2173a = new ArrayList<>();
        this.b = new ArrayList<>();
        this.d = false;
        this.f2173a = new ArrayList<>();
    }

    public static ac a() {
        return d.f2180a;
    }

    private void f() {
        synchronized (e) {
            e.put("update_version", "");
            e.put("update_url", "");
            e.put("update_type", "");
            e.put("ad_cancel_download", "false");
            e.put("ad_switch_time", "10000");
            e.put("ad_install_immediately", "true");
            e.put("ad_wall_default", "ebusiness");
            e.put("duoduo_ad_duration", "0");
            e.put("domob_ad_duration", "0");
            e.put("baidu_ad_duration", "3000");
            e.put("tencent_ad_duration", "30");
            e.put("taobao_ad_duration", "0");
            e.put("adturbo_enable", "1");
            e.put("taobao_wall_type", "sdk");
            e.put("share_url", "http://main.shoujiduoduo.com/share/index.php");
            e.put("bcs_domain_name", "bj.bcebos.com");
            e.put("mi_push_enable", "true");
            e.put("ctcc_enable", "true");
            e.put("dns_retry", "3");
            e.put("dns_timeout", "8000");
            e.put("dns_value1", "cdnringhlt.shoujiduoduo.com");
            e.put("dns_value2", "cdnringfw.shoujiduoduo.com");
            e.put("dns_check1", "http://cdnringhlt.shoujiduoduo.com/ringres/verify.dat");
            e.put("dns_check2", "http://cdnringfw.shoujiduoduo.com/ringres/verify.dat");
            e.put("dns_switchcondition", "network");
            e.put("dns_duoduo_check", "http://www.shoujiduoduo.com/ringv1/verify.dat");
            e.put("aichang_enable", "false");
            e.put("aichang_title", "K歌");
            e.put("ad_delay_time", "0");
            e.put("cm_sunshine_sdk_enable", "true");
            e.put("feed_ad_v1_type", "baidu");
            e.put("feed_ad_v1_enable", "true");
            e.put("feed_ad_v1_channel", "all");
            e.put("feed_ad_v1_show_limit", "3");
            e.put("splash_ad_enable", "true");
            e.put("splash_ad_type", "baidu");
            e.put("quit_ad_enable", "true");
            e.put("quit_ad_type", "baidu");
            e.put("dd_server_ip_1", "117.121.41.242");
            e.put("dd_server_ip_2", "116.213.204.28");
            e.put("dd_server_ip_3", "115.29.204.207");
            e.put("navi_ad_enable", "false");
            e.put("navi_ad_type", "shop");
            e.put("navi_ad_title", "必抢");
            e.put("app_install_mode", "self");
            e.put("cmcc_month_id", "600927020000006624");
            g();
            com.shoujiduoduo.base.a.a.a("ServerConfig", "load default config");
        }
    }

    private void g() {
        ArrayList<b> arrayList = new ArrayList<>();
        arrayList.add(new b(0, "3000", a.BAIDU));
        arrayList.add(new b(1, "60", a.TENCENT));
        this.f2173a = arrayList;
    }

    public void b() {
        boolean z = false;
        com.shoujiduoduo.base.a.a.a("ServerConfig", "begin loadServerConfig");
        this.d = false;
        f();
        if (!i()) {
            com.shoujiduoduo.base.a.a.e("ServerConfig", "server cache not exist or load failed");
        } else {
            z = true;
            com.shoujiduoduo.base.a.a.a("ServerConfig", "load server cache success");
        }
        if (h() || !z) {
            com.shoujiduoduo.base.a.a.a("ServerConfig", "cache is out of date or load failed, get server config");
            j();
            return;
        }
        com.shoujiduoduo.base.a.a.a("ServerConfig", "cache is available, user cache");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.shoujiduoduo.util.ae.a(android.content.Context, java.lang.String, long):long
     arg types: [android.content.Context, java.lang.String, int]
     candidates:
      com.shoujiduoduo.util.ae.a(android.content.Context, java.lang.String, int):int
      com.shoujiduoduo.util.ae.a(android.content.Context, java.lang.String, java.lang.String):java.lang.String
      com.shoujiduoduo.util.ae.a(android.content.Context, java.lang.String, long):long */
    private boolean h() {
        long a2 = ae.a(RingDDApp.c(), "update_config_time", 0L);
        if (a2 == 0) {
            return true;
        }
        long currentTimeMillis = System.currentTimeMillis() - a2;
        com.shoujiduoduo.base.a.a.a("ServerConfig", "cache time duration:" + currentTimeMillis);
        if (currentTimeMillis <= 600000) {
            return false;
        }
        com.shoujiduoduo.base.a.a.a("ServerConfig", "cache out of data, download new data");
        return true;
    }

    /* access modifiers changed from: private */
    public boolean i() {
        if (!q.f(c)) {
            return false;
        }
        com.shoujiduoduo.base.a.a.a("ServerConfig", "load cache config");
        synchronized (e) {
            this.f2173a = new ArrayList<>();
            try {
                Element documentElement = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new FileInputStream(c)).getDocumentElement();
                if (documentElement == null) {
                    com.shoujiduoduo.base.a.a.c("ServerConfig", "getDocumentElement return null");
                    return false;
                }
                NodeList elementsByTagName = documentElement.getElementsByTagName(Constants.ITEM);
                for (int i = 0; i < elementsByTagName.getLength(); i++) {
                    NamedNodeMap attributes = elementsByTagName.item(i).getAttributes();
                    String a2 = g.a(attributes, SelectCountryActivity.EXTRA_COUNTRY_NAME);
                    if (a2.equalsIgnoreCase("update")) {
                        e.put("update_version", g.a(attributes, DeviceInfo.TAG_VERSION));
                        e.put("update_url", g.a(attributes, "url"));
                        e.put("update_type", g.a(attributes, "type"));
                    } else if (a2.equalsIgnoreCase("ad")) {
                        String a3 = g.a(attributes, "cancel_enable");
                        HashMap<String, String> hashMap = e;
                        if (a3.equals("")) {
                            a3 = "false";
                        }
                        hashMap.put("ad_cancel_download", a3);
                        String a4 = g.a(attributes, "switch_time");
                        HashMap<String, String> hashMap2 = e;
                        if (a4.equals("")) {
                            a4 = "10000";
                        }
                        hashMap2.put("ad_switch_time", a4);
                        String a5 = g.a(attributes, "install_immediately");
                        HashMap<String, String> hashMap3 = e;
                        if (a5.equals("")) {
                            a5 = "true";
                        }
                        hashMap3.put("ad_install_immediately", a5);
                    } else if (a2.equalsIgnoreCase("duoduo_ad")) {
                        a(attributes, a.DUODUO);
                    } else if (a2.equalsIgnoreCase("domob_ad")) {
                        a(attributes, a.DOMOB);
                    } else if (a2.equalsIgnoreCase("baidu_ad")) {
                        a(attributes, a.BAIDU);
                    } else if (a2.equalsIgnoreCase("tencent_ad")) {
                        a(attributes, a.TENCENT);
                    } else if (a2.equalsIgnoreCase("taobao_ad")) {
                        a(attributes, a.TAOBAO);
                    } else if (a2.equalsIgnoreCase("ad_wall")) {
                        String a6 = g.a(attributes, "default");
                        HashMap<String, String> hashMap4 = e;
                        if (a6.equals("")) {
                            a6 = "ebusiness";
                        }
                        hashMap4.put("ad_wall_default", a6);
                    } else if (a2.equalsIgnoreCase("game_url1")) {
                        String a7 = g.a(attributes, "url");
                        HashMap<String, String> hashMap5 = e;
                        if (a7.equals("")) {
                            a7 = "";
                        }
                        hashMap5.put("game_url1", a7);
                    } else if (a2.equalsIgnoreCase("taobao_wall_v2")) {
                        String a8 = g.a(attributes, "type");
                        HashMap<String, String> hashMap6 = e;
                        if (a8.equals("")) {
                            a8 = "sdk";
                        }
                        hashMap6.put("taobao_wall_type", a8);
                    } else if (a2.equalsIgnoreCase("bcs_domain_name")) {
                        String a9 = g.a(attributes, "value");
                        HashMap<String, String> hashMap7 = e;
                        if (a9.equals("")) {
                            a9 = "bj.bcebos.com";
                        }
                        hashMap7.put("bcs_domain_name", a9);
                    } else if (a2.equalsIgnoreCase("mi_push")) {
                        String a10 = g.a(attributes, "enable");
                        HashMap<String, String> hashMap8 = e;
                        if (a10.equals("")) {
                            a10 = "true";
                        }
                        hashMap8.put("mi_push_enable", a10);
                    } else if (a2.equalsIgnoreCase("adturbo")) {
                        String a11 = g.a(attributes, "enable");
                        HashMap<String, String> hashMap9 = e;
                        if (a11.equals("")) {
                            a11 = "1";
                        }
                        hashMap9.put("adturbo_enable", a11);
                    } else if (a2.equalsIgnoreCase("ctcc")) {
                        String a12 = g.a(attributes, "enable");
                        HashMap<String, String> hashMap10 = e;
                        if (a12.equals("")) {
                            a12 = "true";
                        }
                        hashMap10.put("ctcc_enable", a12);
                    } else if (a2.equalsIgnoreCase("share")) {
                        String a13 = g.a(attributes, "url");
                        HashMap<String, String> hashMap11 = e;
                        if (a13.equals("")) {
                            a13 = "http://main.shoujiduoduo.com/share/index.php";
                        }
                        hashMap11.put("share_url", a13);
                    } else if (a2.equalsIgnoreCase("feed_ad_v1")) {
                        String a14 = g.a(attributes, "type");
                        HashMap<String, String> hashMap12 = e;
                        if (a14.equals("")) {
                            a14 = "baidu";
                        }
                        hashMap12.put("feed_ad_v1_type", a14);
                        String a15 = g.a(attributes, "enable");
                        HashMap<String, String> hashMap13 = e;
                        if (a15.equals("")) {
                            a15 = "true";
                        }
                        hashMap13.put("feed_ad_v1_enable", a15);
                        String a16 = g.a(attributes, LogBuilder.KEY_CHANNEL);
                        HashMap<String, String> hashMap14 = e;
                        if (a16.equals("")) {
                            a16 = "all";
                        }
                        hashMap14.put("feed_ad_v1_channel", a16);
                        String a17 = g.a(attributes, "show_limit");
                        HashMap<String, String> hashMap15 = e;
                        if (a17.equals("")) {
                            a17 = "3";
                        }
                        hashMap15.put("feed_ad_v1_show_limit", a17);
                        String a18 = g.a(attributes, "baidu");
                        HashMap<String, String> hashMap16 = e;
                        if (a18.equals("")) {
                            a18 = "1";
                        }
                        hashMap16.put("feed_ad_v1_percent_baidu", a18);
                        String a19 = g.a(attributes, "gdt");
                        HashMap<String, String> hashMap17 = e;
                        if (a19.equals("")) {
                            a19 = "1";
                        }
                        hashMap17.put("feed_ad_v1_percent_gdt", a19);
                    } else if (a2.equalsIgnoreCase("dns")) {
                        String a20 = g.a(attributes, "retry");
                        HashMap<String, String> hashMap18 = e;
                        if (a20.equals("")) {
                            a20 = "3";
                        }
                        hashMap18.put("dns_retry", a20);
                        String a21 = g.a(attributes, "timeout");
                        HashMap<String, String> hashMap19 = e;
                        if (a21.equals("")) {
                            a21 = "8000";
                        }
                        hashMap19.put("dns_timeout", a21);
                        String a22 = g.a(attributes, "value1");
                        HashMap<String, String> hashMap20 = e;
                        if (a22.equals("")) {
                            a22 = "cdnringhlt.shoujiduoduo.com";
                        }
                        hashMap20.put("dns_value1", a22);
                        String a23 = g.a(attributes, "value2");
                        HashMap<String, String> hashMap21 = e;
                        if (a23.equals("")) {
                            a23 = "cdnringfw.shoujiduoduo.com";
                        }
                        hashMap21.put("dns_value2", a23);
                        String a24 = g.a(attributes, "check1");
                        HashMap<String, String> hashMap22 = e;
                        if (a24.equals("")) {
                            a24 = "http://cdnringhlt.shoujiduoduo.com/ringres/verify.dat";
                        }
                        hashMap22.put("dns_check1", a24);
                        String a25 = g.a(attributes, "check2");
                        HashMap<String, String> hashMap23 = e;
                        if (a25.equals("")) {
                            a25 = "http://cdnringfw.shoujiduoduo.com/ringres/verify.dat";
                        }
                        hashMap23.put("dns_check2", a25);
                        String a26 = g.a(attributes, "duoduocheck");
                        HashMap<String, String> hashMap24 = e;
                        if (a26.equals("")) {
                            a26 = "http://www.shoujiduoduo.com/ringv1/verify.dat";
                        }
                        hashMap24.put("dns_duoduo_check", a26);
                        String a27 = g.a(attributes, "switchcondition");
                        HashMap<String, String> hashMap25 = e;
                        if (a27.equals("")) {
                            a27 = "network";
                        }
                        hashMap25.put("dns_switchcondition", a27);
                    } else if (a2.equalsIgnoreCase("aichang")) {
                        String a28 = g.a(attributes, "enable");
                        HashMap<String, String> hashMap26 = e;
                        if (a28.equals("")) {
                            a28 = "false";
                        }
                        hashMap26.put("aichang_enable", a28);
                        String a29 = g.a(attributes, "title");
                        HashMap<String, String> hashMap27 = e;
                        if (a29.equals("")) {
                            a29 = "K歌";
                        }
                        hashMap27.put("aichang_title", a29);
                    } else if (a2.equalsIgnoreCase("ad_delay")) {
                        String a30 = g.a(attributes, "time");
                        HashMap<String, String> hashMap28 = e;
                        if (a30.equals("")) {
                            a30 = "0";
                        }
                        hashMap28.put("ad_delay_time", a30);
                    } else if (a2.equalsIgnoreCase("cm_sunshine_sdk")) {
                        String a31 = g.a(attributes, "enable");
                        HashMap<String, String> hashMap29 = e;
                        if (a31.equals("")) {
                            a31 = "true";
                        }
                        hashMap29.put("cm_sunshine_sdk_enable", a31);
                    } else if (a2.equalsIgnoreCase("splash_ad")) {
                        String a32 = g.a(attributes, "enable");
                        HashMap<String, String> hashMap30 = e;
                        if (a32.equals("")) {
                            a32 = "true";
                        }
                        hashMap30.put("splash_ad_enable", a32);
                        String a33 = g.a(attributes, "type");
                        HashMap<String, String> hashMap31 = e;
                        if (a33.equals("")) {
                            a33 = "baidu";
                        }
                        hashMap31.put("splash_ad_type", a33);
                    } else if (a2.equalsIgnoreCase("quit_ad")) {
                        String a34 = g.a(attributes, "enable");
                        HashMap<String, String> hashMap32 = e;
                        if (a34.equals("")) {
                            a34 = "true";
                        }
                        hashMap32.put("quit_ad_enable", a34);
                        String a35 = g.a(attributes, "type");
                        HashMap<String, String> hashMap33 = e;
                        if (a35.equals("")) {
                            a35 = "baidu";
                        }
                        hashMap33.put("quit_ad_type", a35);
                    } else if (a2.equalsIgnoreCase("dd_server_ip")) {
                        String a36 = g.a(attributes, "ip1");
                        HashMap<String, String> hashMap34 = e;
                        if (a36.equals("")) {
                            a36 = "117.121.41.242";
                        }
                        hashMap34.put("dd_server_ip_1", a36);
                        String a37 = g.a(attributes, "ip2");
                        HashMap<String, String> hashMap35 = e;
                        if (a37.equals("")) {
                            a37 = "116.213.204.28";
                        }
                        hashMap35.put("dd_server_ip_2", a37);
                        String a38 = g.a(attributes, "ip3");
                        HashMap<String, String> hashMap36 = e;
                        if (a38.equals("")) {
                            a38 = "115.29.204.207";
                        }
                        hashMap36.put("dd_server_ip_3", a38);
                    } else if (a2.equalsIgnoreCase("navi_ad")) {
                        String a39 = g.a(attributes, "enable");
                        HashMap<String, String> hashMap37 = e;
                        if (a39.equals("")) {
                            a39 = "false";
                        }
                        hashMap37.put("navi_ad_enable", a39);
                        String a40 = g.a(attributes, "type");
                        HashMap<String, String> hashMap38 = e;
                        if (a40.equals("")) {
                            a40 = "shop";
                        }
                        hashMap38.put("navi_ad_type", a40);
                        String a41 = g.a(attributes, "title");
                        HashMap<String, String> hashMap39 = e;
                        if (a41.equals("")) {
                            a41 = "必抢";
                        }
                        hashMap39.put("navi_ad_title", a41);
                        this.b.clear();
                        NodeList childNodes = elementsByTagName.item(i).getChildNodes();
                        for (int i2 = 0; i2 < childNodes.getLength(); i2++) {
                            NamedNodeMap attributes2 = childNodes.item(i2).getAttributes();
                            if (attributes2 != null) {
                                c cVar = new c();
                                cVar.f2179a = g.a(attributes2, SelectCountryActivity.EXTRA_COUNTRY_NAME);
                                cVar.b = g.a(attributes2, "url");
                                this.b.add(cVar);
                            }
                        }
                    } else if (a2.equalsIgnoreCase("app_install")) {
                        String a42 = g.a(attributes, "mode");
                        HashMap<String, String> hashMap40 = e;
                        if (a42.equals("")) {
                            a42 = "self";
                        }
                        hashMap40.put("app_install_mode", a42);
                    } else if (a2.equalsIgnoreCase("cmcc")) {
                        String a43 = g.a(attributes, "month_id");
                        HashMap<String, String> hashMap41 = e;
                        if (a43.equals("")) {
                            a43 = "600927020000006624";
                        }
                        hashMap41.put("cmcc_month_id", a43);
                    }
                }
                Collections.sort(this.f2173a, new Comparator<b>() {
                    /* renamed from: a */
                    public int compare(b bVar, b bVar2) {
                        if (bVar.b > bVar2.b) {
                            return 1;
                        }
                        if (bVar.b < bVar2.b) {
                            return -1;
                        }
                        return 0;
                    }
                });
                com.shoujiduoduo.base.a.a.a("ServerConfig", "end load cache! return TRUE!");
                return true;
            } catch (IOException e2) {
                com.shoujiduoduo.base.a.a.a(e2);
                return false;
            } catch (SAXException e3) {
                com.shoujiduoduo.base.a.a.a(e3);
                return false;
            } catch (ParserConfigurationException e4) {
                com.shoujiduoduo.base.a.a.a(e4);
                return false;
            } catch (DOMException e5) {
                com.shoujiduoduo.base.a.a.a(e5);
                return false;
            } catch (Exception e6) {
                com.shoujiduoduo.base.a.a.a(e6);
                return false;
            }
        }
    }

    /* compiled from: ServerConfig */
    public static class b {

        /* renamed from: a  reason: collision with root package name */
        public String f2178a;
        public int b;
        public a c;

        b() {
            this.f2178a = "0";
            this.b = -1;
            this.c = a.BAIDU;
        }

        b(int i, String str, a aVar) {
            this.f2178a = str;
            this.b = i;
            this.c = aVar;
        }
    }

    private b a(NamedNodeMap namedNodeMap, a aVar) {
        b bVar = new b();
        bVar.f2178a = g.a(namedNodeMap, "duration");
        bVar.b = r.a(g.a(namedNodeMap, "neworder"), -1);
        bVar.c = aVar;
        if (bVar.b >= 0 && !bVar.f2178a.equalsIgnoreCase("0") && !bVar.f2178a.equalsIgnoreCase("")) {
            this.f2173a.add(bVar);
        }
        switch (aVar) {
            case DUODUO:
                e.put("duoduo_ad_duration", bVar.f2178a.equals("") ? "0" : bVar.f2178a);
                break;
            case DOMOB:
                e.put("domob_ad_duration", bVar.f2178a.equals("") ? "0" : bVar.f2178a);
                break;
            case BAIDU:
                e.put("baidu_ad_duration", bVar.f2178a.equals("") ? "3000" : bVar.f2178a);
                break;
            case TENCENT:
                e.put("tencent_ad_duration", bVar.f2178a.equals("") ? "30" : bVar.f2178a);
                break;
            case TAOBAO:
                e.put("taobao_ad_duration", bVar.f2178a.equals("") ? "0" : bVar.f2178a);
                break;
        }
        return bVar;
    }

    public ArrayList<b> c() {
        return this.f2173a;
    }

    /* compiled from: ServerConfig */
    public static class c {

        /* renamed from: a  reason: collision with root package name */
        public String f2179a;
        public String b;

        public c() {
        }

        public c(String str, String str2) {
            this.f2179a = str;
            this.b = str2;
        }
    }

    public ArrayList<c> d() {
        ArrayList<c> arrayList;
        synchronized (e) {
            arrayList = this.b;
        }
        return arrayList;
    }

    public String a(String str) {
        String str2;
        synchronized (e) {
            if (e.containsKey(str)) {
                str2 = e.get(str);
            } else {
                str2 = "";
            }
        }
        return str2;
    }

    public int a(String str, int i) {
        synchronized (e) {
            if (e.containsKey(str)) {
                i = r.a(e.get(str), i);
            }
        }
        return i;
    }

    public boolean b(String str) {
        boolean z;
        synchronized (e) {
            if (e.containsKey(str)) {
                z = "true".equalsIgnoreCase(e.get(str));
            } else {
                z = false;
            }
        }
        return z;
    }

    private void j() {
        i.a(new Runnable() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.shoujiduoduo.util.ac.a(com.shoujiduoduo.util.ac, boolean):boolean
             arg types: [com.shoujiduoduo.util.ac, int]
             candidates:
              com.shoujiduoduo.util.ac.a(org.w3c.dom.NamedNodeMap, com.shoujiduoduo.util.ac$a):com.shoujiduoduo.util.ac$b
              com.shoujiduoduo.util.ac.a(java.lang.String, int):int
              com.shoujiduoduo.util.ac.a(com.shoujiduoduo.util.ac, boolean):boolean */
            public void run() {
                com.shoujiduoduo.base.a.a.a("ServerConfig", "loadFromNetwork Thread, ThreadID = " + Thread.currentThread().getId());
                String b = t.b();
                if (!ah.c(b)) {
                    if (!q.b(ac.c, b)) {
                        com.shoujiduoduo.base.a.a.c("ServerConfig", "write config to sd card failed, 请检查SD卡存取权限");
                    }
                    long currentTimeMillis = System.currentTimeMillis();
                    ae.c(RingDDApp.c(), "pref_config_src", g.n());
                    if (ac.this.i()) {
                        boolean unused = ac.this.d = true;
                        ae.b(RingDDApp.c(), "update_config_time", currentTimeMillis);
                        com.shoujiduoduo.a.a.c.a().b(com.shoujiduoduo.a.a.b.OBSERVER_CONFIG, new c.a<f>() {
                            public void a() {
                                ((f) this.f1284a).a(true);
                            }
                        });
                        return;
                    }
                    com.shoujiduoduo.base.a.a.e("ServerConfig", "load cache failed");
                    return;
                }
                com.shoujiduoduo.base.a.a.c("ServerConfig", "get server config error!!");
            }
        });
    }
}
