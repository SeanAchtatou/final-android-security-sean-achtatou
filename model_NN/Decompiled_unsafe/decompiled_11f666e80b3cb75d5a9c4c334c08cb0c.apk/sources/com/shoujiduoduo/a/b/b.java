package com.shoujiduoduo.a.b;

import com.meizu.cloud.pushsdk.constants.PushConstants;
import com.shoujiduoduo.b.a.f;
import com.shoujiduoduo.b.c.g;
import com.shoujiduoduo.b.c.m;
import com.shoujiduoduo.b.d.d;
import com.shoujiduoduo.b.e.a;
import com.shoujiduoduo.b.f.c;
import com.shoujiduoduo.ringtone.RingDDApp;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;

/* compiled from: ModMgr */
public final class b {

    /* renamed from: a  reason: collision with root package name */
    private static LinkedList<a> f1287a = new LinkedList<>();
    private static boolean b;
    private static c c = null;
    private static f d = null;
    private static com.shoujiduoduo.b.b.c e = null;
    private static com.shoujiduoduo.b.d.c f = null;
    private static g g = null;
    private static a h = null;

    public static void a() {
        b = true;
        if (Thread.currentThread().getId() != RingDDApp.d()) {
            com.shoujiduoduo.base.a.a.c("ModMgr", "releaseAll");
            HashMap hashMap = new HashMap();
            hashMap.put("release", "true");
            hashMap.put(PushConstants.MZ_PUSH_MESSAGE_METHOD, "releaseAll");
            com.umeng.a.b.a(RingDDApp.c(), "MODMGR_ERROR", hashMap);
        }
        com.shoujiduoduo.base.a.a.a("ModMgr", "release module num:" + f1287a.size());
        Iterator<a> it = f1287a.iterator();
        while (it.hasNext()) {
            try {
                it.next().b();
            } catch (Throwable th) {
            }
        }
        f1287a.clear();
    }

    private static void a(a aVar) {
        aVar.a();
        f1287a.add(aVar);
    }

    public static synchronized c b() {
        c cVar;
        synchronized (b.class) {
            if (Thread.currentThread().getId() != RingDDApp.d()) {
                com.shoujiduoduo.base.a.a.c("ModMgr", "getUserListMgr");
                HashMap hashMap = new HashMap();
                hashMap.put("release", b ? "true" : "false");
                hashMap.put(PushConstants.MZ_PUSH_MESSAGE_METHOD, "getUserListMgr");
                com.umeng.a.b.a(RingDDApp.c(), "MODMGR_ERROR", hashMap);
            }
            if (c == null) {
                c = new com.shoujiduoduo.b.f.f();
                a(c);
            }
            cVar = c;
        }
        return cVar;
    }

    public static synchronized f c() {
        f fVar;
        synchronized (b.class) {
            if (Thread.currentThread().getId() != RingDDApp.d()) {
                com.shoujiduoduo.base.a.a.c("ModMgr", "getAdMgr");
                HashMap hashMap = new HashMap();
                hashMap.put("release", b ? "true" : "false");
                hashMap.put(PushConstants.MZ_PUSH_MESSAGE_METHOD, "getAdMgr");
                com.umeng.a.b.a(RingDDApp.c(), "MODMGR_ERROR", hashMap);
            }
            if (d == null) {
                d = new com.shoujiduoduo.b.a.a();
                a(d);
            }
            fVar = d;
        }
        return fVar;
    }

    public static synchronized com.shoujiduoduo.b.b.c d() {
        com.shoujiduoduo.b.b.c cVar;
        synchronized (b.class) {
            if (Thread.currentThread().getId() != RingDDApp.d()) {
                com.shoujiduoduo.base.a.a.c("ModMgr", "getCategoryMgr");
                HashMap hashMap = new HashMap();
                hashMap.put("release", b ? "true" : "false");
                hashMap.put(PushConstants.MZ_PUSH_MESSAGE_METHOD, "getCategoryMgr");
                com.umeng.a.b.a(RingDDApp.c(), "MODMGR_ERROR", hashMap);
            }
            if (e == null) {
                e = new com.shoujiduoduo.b.b.b();
                a(e);
            }
            cVar = e;
        }
        return cVar;
    }

    public static synchronized com.shoujiduoduo.b.d.c e() {
        com.shoujiduoduo.b.d.c cVar;
        synchronized (b.class) {
            if (Thread.currentThread().getId() != RingDDApp.d()) {
                com.shoujiduoduo.base.a.a.c("ModMgr", "getSearchMgr");
                HashMap hashMap = new HashMap();
                hashMap.put("release", b ? "true" : "false");
                hashMap.put(PushConstants.MZ_PUSH_MESSAGE_METHOD, "getSearchMgr");
                com.umeng.a.b.a(RingDDApp.c(), "MODMGR_ERROR", hashMap);
            }
            if (f == null) {
                f = new d();
                a(f);
            }
            cVar = f;
        }
        return cVar;
    }

    public static synchronized g f() {
        g gVar;
        synchronized (b.class) {
            if (Thread.currentThread().getId() != RingDDApp.d()) {
                com.shoujiduoduo.base.a.a.c("ModMgr", "getTopListMgr");
                HashMap hashMap = new HashMap();
                hashMap.put("release", b ? "true" : "false");
                hashMap.put(PushConstants.MZ_PUSH_MESSAGE_METHOD, "getTopListMgr");
                com.umeng.a.b.a(RingDDApp.c(), "MODMGR_ERROR", hashMap);
            }
            if (g == null) {
                g = new m();
                a(g);
            }
            gVar = g;
        }
        return gVar;
    }

    public static synchronized a g() {
        a aVar;
        synchronized (b.class) {
            if (Thread.currentThread().getId() != RingDDApp.d()) {
                HashMap hashMap = new HashMap();
                hashMap.put("release", b ? "true" : "false");
                hashMap.put(PushConstants.MZ_PUSH_MESSAGE_METHOD, "getUserInfoMgr");
                com.umeng.a.b.a(RingDDApp.c(), "MODMGR_ERROR", hashMap);
            }
            if (h == null) {
                h = new com.shoujiduoduo.b.e.b();
                a(h);
            }
            aVar = h;
        }
        return aVar;
    }
}
