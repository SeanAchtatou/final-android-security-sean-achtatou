package com.shoujiduoduo.player;

/* compiled from: BasePlayer */
public abstract class b {

    /* renamed from: a  reason: collision with root package name */
    protected boolean f1408a = true;
    protected e b = null;
    protected C0041b c = null;
    protected c d = null;
    protected f e = null;
    protected d f = null;
    protected a g = null;
    private final Object h = new Object();
    private int i = 0;

    /* compiled from: BasePlayer */
    public interface a {
        void a(b bVar, int i);
    }

    /* renamed from: com.shoujiduoduo.player.b$b  reason: collision with other inner class name */
    /* compiled from: BasePlayer */
    public interface C0041b {
        void a(b bVar);
    }

    /* compiled from: BasePlayer */
    public interface c {
        boolean a(b bVar, int i, int i2);
    }

    /* compiled from: BasePlayer */
    public interface d {
        void a(b bVar, int i, int i2);
    }

    /* compiled from: BasePlayer */
    public interface e {
        void a(b bVar);
    }

    /* compiled from: BasePlayer */
    public interface f {
        void b(b bVar);
    }

    public abstract int a(String str);

    public abstract void b();

    public abstract void c();

    public abstract void d();

    public abstract int e();

    public abstract int f();

    public abstract int h();

    public void a(boolean z) {
        this.f1408a = z;
    }

    public void a() {
        d();
        a(0);
    }

    public int g() {
        int i2;
        synchronized (this.h) {
            i2 = this.i;
        }
        return i2;
    }

    public void a(int i2) {
        int i3;
        synchronized (this.h) {
            i3 = this.i;
            this.i = i2;
        }
        if (i3 != i2 && this.e != null) {
            this.e.b(this);
        }
    }

    public boolean i() {
        return g() == 4;
    }

    public boolean j() {
        return g() == 3;
    }

    public boolean k() {
        return g() == 0 || g() == 5;
    }

    public void a(C0041b bVar) {
        this.c = bVar;
    }
}
