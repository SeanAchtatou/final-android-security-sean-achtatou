package com.shoujiduoduo.player;

/* compiled from: PlayerManager */
public class e {

    /* renamed from: a  reason: collision with root package name */
    private d f1411a;
    private h b;

    /* compiled from: PlayerManager */
    private static class a {

        /* renamed from: a  reason: collision with root package name */
        public static e f1412a = new e();
    }

    private e() {
        this.f1411a = null;
        this.b = null;
        this.b = new h();
        this.f1411a = new d();
    }

    public static e a() {
        return a.f1412a;
    }

    public void b() {
        if (this.b != null) {
            this.b.a();
            this.b.l();
        }
        if (this.f1411a != null) {
            this.f1411a.a();
            this.f1411a.m();
        }
    }

    public b c() {
        if (this.f1411a == null) {
            this.f1411a = new d();
        }
        return this.f1411a;
    }

    public b d() {
        if (this.b == null) {
            this.b = new h();
        }
        return this.b;
    }
}
