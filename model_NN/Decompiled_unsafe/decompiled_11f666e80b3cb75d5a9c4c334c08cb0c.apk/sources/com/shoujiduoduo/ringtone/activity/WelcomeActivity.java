package com.shoujiduoduo.ringtone.activity;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.baidu.mobads.SplashAd;
import com.baidu.mobads.SplashAdListener;
import com.d.a.b.d;
import com.meizu.cloud.pushsdk.pushtracer.constant.Parameters;
import com.qq.e.ads.nativ.NativeAD;
import com.qq.e.ads.nativ.NativeADDataRef;
import com.qq.e.ads.splash.SplashAD;
import com.qq.e.ads.splash.SplashADListener;
import com.shoujiduoduo.a.a.c;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.ui.utils.BaseActivity;
import com.shoujiduoduo.ui.utils.h;
import com.shoujiduoduo.util.ac;
import com.shoujiduoduo.util.ae;
import com.shoujiduoduo.util.ag;
import com.shoujiduoduo.util.ah;
import com.shoujiduoduo.util.g;
import com.shoujiduoduo.util.l;
import com.shoujiduoduo.util.o;
import com.shoujiduoduo.util.q;
import com.shoujiduoduo.util.r;
import com.shoujiduoduo.util.widget.WebViewActivity;
import com.shoujiduoduo.util.widget.b;
import com.umeng.a.b;
import java.util.HashMap;
import java.util.List;

public class WelcomeActivity extends BaseActivity {
    private static boolean h;

    /* renamed from: a  reason: collision with root package name */
    public boolean f1441a = false;
    /* access modifiers changed from: private */
    public ImageView b;
    private LinearLayout c;
    /* access modifiers changed from: private */
    public boolean d;
    private FrameLayout e;
    /* access modifiers changed from: private */
    public boolean f;
    /* access modifiers changed from: private */
    public boolean g;
    /* access modifiers changed from: private */
    public Activity i;
    /* access modifiers changed from: private */
    public boolean j;
    /* access modifiers changed from: private */
    public Handler k = new Handler() {
        public void handleMessage(Message message) {
            switch (message.what) {
                case 2:
                    com.shoujiduoduo.base.a.a.a("WelcomeActivity", "MSG_ENTER_MAIN_ACTIVITY Received!");
                    if (!WelcomeActivity.this.f) {
                        if (WelcomeActivity.this.d) {
                            b.b(WelcomeActivity.this, "START_AD_SHOW");
                        }
                        WelcomeActivity.this.g();
                        return;
                    }
                    return;
                case 3:
                    boolean unused = WelcomeActivity.this.f = true;
                    new b.a(WelcomeActivity.this).a((int) R.string.sdcard_not_found).a((int) R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialogInterface, int i) {
                            WelcomeActivity.this.finish();
                            RingDDApp.g();
                        }
                    }).a().show();
                    return;
                case 4:
                    boolean unused2 = WelcomeActivity.this.f = true;
                    new b.a(WelcomeActivity.this).a((int) R.string.sdcard_not_access).a((int) R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialogInterface, int i) {
                            WelcomeActivity.this.finish();
                            RingDDApp.g();
                        }
                    }).a().show();
                    return;
                case 5:
                    if (com.shoujiduoduo.util.a.a(true)) {
                        com.shoujiduoduo.base.a.a.a("WelcomeActivity", "1秒钟后检查，显示广告");
                        WelcomeActivity.this.b();
                        return;
                    }
                    com.shoujiduoduo.base.a.a.a("WelcomeActivity", "1秒钟后检查，show normal splash");
                    WelcomeActivity.this.k();
                    return;
                case 6:
                    com.shoujiduoduo.base.a.a.a("WelcomeActivity", "splash ad over time, enter main activity");
                    if (!WelcomeActivity.this.f && !WelcomeActivity.this.j) {
                        com.umeng.a.b.b(WelcomeActivity.this.getApplicationContext(), "splashad_over_time");
                        WelcomeActivity.this.g();
                        return;
                    }
                    return;
                case 7:
                    a aVar = (a) message.obj;
                    if (WelcomeActivity.this.g) {
                        return;
                    }
                    if (!"baidu".equals(aVar.f1450a) && !"gdt".equals(aVar.f1450a)) {
                        WelcomeActivity.this.k();
                        return;
                    } else if (!"false".equals(com.umeng.b.a.a().a(WelcomeActivity.this.getApplicationContext(), "gdt_splash_native_ad_switch"))) {
                        WelcomeActivity.this.e();
                        return;
                    } else {
                        return;
                    }
                case 8:
                case 9:
                    if (!WelcomeActivity.this.f && !WelcomeActivity.this.g) {
                        WelcomeActivity.this.h();
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    };

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.shoujiduoduo.util.ae.a(android.content.Context, java.lang.String, int):int
     arg types: [com.shoujiduoduo.ringtone.activity.WelcomeActivity, java.lang.String, int]
     candidates:
      com.shoujiduoduo.util.ae.a(android.content.Context, java.lang.String, long):long
      com.shoujiduoduo.util.ae.a(android.content.Context, java.lang.String, java.lang.String):java.lang.String
      com.shoujiduoduo.util.ae.a(android.content.Context, java.lang.String, int):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.shoujiduoduo.util.ae.b(android.content.Context, java.lang.String, int):boolean
     arg types: [com.shoujiduoduo.ringtone.activity.WelcomeActivity, java.lang.String, int]
     candidates:
      com.shoujiduoduo.util.ae.b(android.content.Context, java.lang.String, long):boolean
      com.shoujiduoduo.util.ae.b(android.content.Context, java.lang.String, java.lang.String):boolean
      com.shoujiduoduo.util.ae.b(android.content.Context, java.lang.String, int):boolean */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Intent intent = getIntent();
        String a2 = com.umeng.b.a.a().a(RingDDApp.c(), "start_ad_slient_gap");
        if (!(intent == null || (intent.getFlags() & 4194304) == 0)) {
            int intExtra = intent.getIntExtra(g.b, 0);
            if (h && intExtra != g.c) {
                if (RingDDApp.b().a("activity_on_stop_time") != null) {
                    long currentTimeMillis = System.currentTimeMillis() - ((Long) RingDDApp.b().a("activity_on_stop_time")).longValue();
                    com.shoujiduoduo.base.a.a.a("WelcomeActivity", "background time:" + currentTimeMillis);
                    com.shoujiduoduo.base.a.a.a("WelcomeActivity", "slient gap:" + r.a(a2, 30000));
                    if (a2.equals("0") || currentTimeMillis < ((long) r.a(a2, 30000)) || j()) {
                        com.shoujiduoduo.base.a.a.a("WelcomeActivity", "restart time gap is too small, or is vip, so just put task to front");
                        finish();
                        return;
                    }
                } else {
                    finish();
                    return;
                }
            }
        }
        this.g = false;
        if (!h) {
            setContentView((int) R.layout.activity_welcome);
            com.umeng.b.a.a().a(getApplicationContext());
            com.shoujiduoduo.util.c.b.a().a((com.shoujiduoduo.util.b.b) null);
            this.i = this;
            this.d = false;
            a();
            if (ae.a((Context) this, "preference_create_shortcut", 0) <= 0) {
                g.a(getApplicationContext(), getResources().getString(R.string.app_name), (int) R.drawable.duoduo_icon);
                ae.b((Context) this, "preference_create_shortcut", 1);
            }
            a(false);
            c.a().b(new c.b() {
                public void a() {
                    if (!l.b()) {
                        WelcomeActivity.this.k.sendEmptyMessage(3);
                    } else if (!l.a()) {
                        WelcomeActivity.this.k.sendEmptyMessage(4);
                    } else {
                        com.shoujiduoduo.a.b.b.g();
                        com.shoujiduoduo.a.b.b.b();
                        com.shoujiduoduo.a.b.b.c();
                        com.shoujiduoduo.a.b.b.e();
                        com.shoujiduoduo.a.b.b.d();
                        HashMap hashMap = new HashMap();
                        if (g.a("com.kingroot.master")) {
                            hashMap.put(Parameters.RESOLUTION, "install");
                        } else {
                            hashMap.put(Parameters.RESOLUTION, "uninstall");
                        }
                        com.umeng.a.b.a(RingDDApp.c(), "kingroot_install", hashMap);
                    }
                }
            });
        } else {
            setContentView((int) R.layout.activity_welcome);
            a();
            a(true);
        }
        com.shoujiduoduo.base.a.a.a("WelcomeActivity", "onCreate");
    }

    private void a(boolean z) {
        if (j()) {
            com.shoujiduoduo.base.a.a.a("WelcomeActivity", "current user is vip");
            if (z) {
                this.k.sendEmptyMessage(2);
            } else if (i()) {
                l();
            } else {
                k();
            }
        } else {
            com.shoujiduoduo.base.a.a.a("WelcomeActivity", "user is not vip");
            String a2 = com.umeng.b.a.a().a(getApplicationContext(), "splashad_over_time_duration");
            if (z) {
                this.k.sendEmptyMessageDelayed(6, 3000);
            } else {
                this.k.sendEmptyMessageDelayed(6, (long) r.a(a2, 7000));
            }
            if (com.shoujiduoduo.util.a.a(false)) {
                com.shoujiduoduo.base.a.a.a("WelcomeActivity", "启动时首次检查，显示广告");
                b();
                return;
            }
            com.shoujiduoduo.base.a.a.a("WelcomeActivity", "等待一秒钟，check server ad config");
            this.k.sendEmptyMessageDelayed(5, 1000);
        }
    }

    private void a() {
        this.b = (ImageView) findViewById(R.id.image_top);
        this.c = (LinearLayout) findViewById(R.id.bottom_layout);
        this.e = (FrameLayout) findViewById(R.id.ad_layout);
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 == 4) {
            return true;
        }
        return super.onKeyDown(i2, keyEvent);
    }

    private class a {

        /* renamed from: a  reason: collision with root package name */
        public String f1450a;
        public long b;

        public a(String str, long j) {
            this.b = j;
            this.f1450a = str;
        }
    }

    /* access modifiers changed from: private */
    public void b() {
        String a2 = ac.a().a("splash_ad_type");
        com.shoujiduoduo.base.a.a.a("WelcomeActivity", "server splash ad type:" + a2);
        if ("baidu".equals(a2)) {
            c();
        } else if ("gdt".equals(a2)) {
            d();
        } else if ("duoduo".equals(a2)) {
            f();
        } else {
            com.shoujiduoduo.base.a.a.a("WelcomeActivity", "not support splash ad type, 认为是百度吧，要不没有广告了");
            c();
        }
    }

    private void c() {
        this.j = false;
        this.e.setVisibility(0);
        com.shoujiduoduo.base.a.a.a("WelcomeActivity", "showBaiduSplashAd");
        com.umeng.a.b.b(RingDDApp.c(), "baidu_splash_show");
        final long currentTimeMillis = System.currentTimeMillis();
        new SplashAd(this, this.e, new SplashAdListener() {
            public void onAdDismissed() {
                com.shoujiduoduo.base.a.a.b("WelcomeActivity", "baidu ad onAdDismissed");
                HashMap hashMap = new HashMap();
                hashMap.put("status", "onAdDismissed");
                com.umeng.a.b.a(WelcomeActivity.this.getApplicationContext(), "BAIDU_SPLASH_AD", hashMap);
                com.umeng.a.b.a(WelcomeActivity.this.getApplicationContext(), "baidu_splashad_dismiss", hashMap, (int) (System.currentTimeMillis() - currentTimeMillis));
                if (!WelcomeActivity.this.f && !WelcomeActivity.this.g) {
                    WelcomeActivity.this.h();
                }
            }

            public void onAdFailed(String str) {
                com.shoujiduoduo.base.a.a.c("WelcomeActivity", "baidu ad onAdFailed:" + str);
                HashMap hashMap = new HashMap();
                hashMap.put("status", "onAdFailed");
                hashMap.put("errcode", str);
                com.umeng.a.b.a(WelcomeActivity.this.getApplicationContext(), "BAIDU_SPLASH_AD", hashMap);
                com.umeng.a.b.a(WelcomeActivity.this.getApplicationContext(), "baidu_splashad_fail", hashMap, (int) (System.currentTimeMillis() - currentTimeMillis));
                com.shoujiduoduo.base.a.a.a("WelcomeActivity", "failed time:" + (System.currentTimeMillis() - currentTimeMillis));
                Message obtainMessage = WelcomeActivity.this.k.obtainMessage();
                obtainMessage.what = 7;
                obtainMessage.obj = new a("baidu", currentTimeMillis);
                WelcomeActivity.this.k.sendMessage(obtainMessage);
            }

            public void onAdPresent() {
                com.shoujiduoduo.base.a.a.b("WelcomeActivity", "baidu ad onAdPresent");
                HashMap hashMap = new HashMap();
                hashMap.put("status", "onAdPresent");
                com.umeng.a.b.a(WelcomeActivity.this.getApplicationContext(), "BAIDU_SPLASH_AD", hashMap);
            }

            public void onAdClick() {
                boolean unused = WelcomeActivity.this.j = true;
                com.shoujiduoduo.base.a.a.b("WelcomeActivity", "baidu ad onAdClick");
            }
        }, "2385862", true);
    }

    private void d() {
        com.shoujiduoduo.base.a.a.a("WelcomeActivity", "showGDTSplashAd");
        this.j = false;
        this.e.setVisibility(0);
        com.umeng.a.b.b(getApplicationContext(), "gdt_splash_show");
        final long currentTimeMillis = System.currentTimeMillis();
        new SplashAD(this, this.e, "1105772024", "8010114633441949", new SplashADListener() {
            public void onADPresent() {
                com.shoujiduoduo.base.a.a.a("WelcomeActivity", "gdt ad onAdPresent");
                HashMap hashMap = new HashMap();
                hashMap.put("status", "onAdPresent");
                com.umeng.a.b.a(WelcomeActivity.this.getApplicationContext(), "GDT_SPLASH_AD", hashMap);
            }

            public void onADClicked() {
                boolean unused = WelcomeActivity.this.j = true;
                com.shoujiduoduo.base.a.a.a("WelcomeActivity", "gdt ad onADClicked");
            }

            public void onADDismissed() {
                com.shoujiduoduo.base.a.a.a("WelcomeActivity", "gdt ad onAdDismissed");
                HashMap hashMap = new HashMap();
                hashMap.put("status", "onAdDismissed");
                com.umeng.a.b.a(WelcomeActivity.this.getApplicationContext(), "GDT_SPLASH_AD", hashMap);
                if (!WelcomeActivity.this.f && !WelcomeActivity.this.g) {
                    WelcomeActivity.this.h();
                }
            }

            public void onNoAD(int i) {
                com.shoujiduoduo.base.a.a.a("WelcomeActivity", "gdt no ad or failed, errCode:" + i);
                HashMap hashMap = new HashMap();
                hashMap.put("status", "onNoAD");
                hashMap.put("errcode", "" + i);
                com.umeng.a.b.a(WelcomeActivity.this.getApplicationContext(), "GDT_SPLASH_AD", hashMap);
                Message obtainMessage = WelcomeActivity.this.k.obtainMessage();
                obtainMessage.what = 7;
                obtainMessage.obj = new a("gdt", currentTimeMillis);
                WelcomeActivity.this.k.sendMessage(obtainMessage);
            }
        });
    }

    /* access modifiers changed from: private */
    public void e() {
        com.shoujiduoduo.base.a.a.a("WelcomeActivity", "fetchGdtFeed");
        new NativeAD(this, "1105772024", "1020513693444900", new NativeAD.NativeAdListener() {
            public void onADLoaded(List<NativeADDataRef> list) {
                if (list.size() > 0) {
                    com.shoujiduoduo.base.a.a.a("WelcomeActivity", "fetchGdtFeed feed, onNativeLoad, ad size:" + list.size());
                    final NativeADDataRef nativeADDataRef = list.get(0);
                    d.a().a(nativeADDataRef.getImgUrl(), WelcomeActivity.this.b, h.a().c());
                    WelcomeActivity.this.b.setVisibility(0);
                    nativeADDataRef.onExposured(WelcomeActivity.this.b);
                    WelcomeActivity.this.b.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View view) {
                            nativeADDataRef.onClicked(view);
                        }
                    });
                    return;
                }
                com.shoujiduoduo.base.a.a.a("WelcomeActivity", "fetchGdtFeed feed, onNativeLoad, ad size is 0");
            }

            public void onNoAD(int i) {
                if (!WelcomeActivity.this.g) {
                    WelcomeActivity.this.k();
                }
            }

            public void onADStatusChanged(NativeADDataRef nativeADDataRef) {
            }

            public void onADError(NativeADDataRef nativeADDataRef, int i) {
            }
        }).loadAD(1);
        this.k.sendEmptyMessageDelayed(9, 3000);
    }

    private void f() {
        String a2 = com.umeng.b.a.a().a(getApplicationContext(), "start_ad_new");
        com.shoujiduoduo.base.a.a.a("WelcomeActivity", "umeng start ad config:" + a2);
        ag agVar = new ag(a2);
        this.j = false;
        if (agVar.a()) {
            com.shoujiduoduo.base.a.a.a("WelcomeActivity", "canShowStartAd is true");
            if (b(agVar)) {
                com.shoujiduoduo.base.a.a.a("WelcomeActivity", "hit start ad, but app is installed, show normal ad!");
                String a3 = com.umeng.b.a.a().a(RingDDApp.c(), "duoduo_splash_ad_substitute");
                if (ah.c(a3)) {
                    a3 = "baidu";
                }
                if ("baidu".equals(a3)) {
                    c();
                } else if ("gdt".equals(a3)) {
                    d();
                } else {
                    com.shoujiduoduo.base.a.a.a("WelcomeActivity", "show normal pic");
                    k();
                }
            } else {
                com.shoujiduoduo.base.a.a.a("WelcomeActivity", "show duoduo splash ad");
                a(agVar);
            }
        } else {
            com.shoujiduoduo.base.a.a.a("WelcomeActivity", "can not show duoduo splash, show normal pic");
            k();
        }
    }

    private void a(final ag agVar) {
        d.a().a(agVar.b(), this.b, h.a().c());
        this.b.setVisibility(0);
        com.umeng.a.b.b(getApplicationContext(), "DD_SPLASH_AD");
        this.b.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                com.shoujiduoduo.base.a.a.a("WelcomeActivity", "click start ad");
                String c = agVar.c();
                if (!ah.c(c)) {
                    com.shoujiduoduo.base.a.a.a("WelcomeActivity", "start ad is jump url ad, url:" + c);
                    boolean unused = WelcomeActivity.this.j = true;
                    HashMap hashMap = new HashMap();
                    hashMap.put("ad_type", "url");
                    com.umeng.a.b.a(WelcomeActivity.this.getApplicationContext(), "DUODUO_SPLASH_AD_CLICK", hashMap);
                    Intent intent = new Intent(WelcomeActivity.this.i, WebViewActivity.class);
                    intent.putExtra("url", c);
                    WelcomeActivity.this.i.startActivity(intent);
                    return;
                }
                String d = agVar.d();
                String e = agVar.e();
                com.shoujiduoduo.base.a.a.a("WelcomeActivity", "start ad is down apk ad, down url:" + d);
                if (!ah.c(d)) {
                    HashMap hashMap2 = new HashMap();
                    hashMap2.put(Parameters.PACKAGE_NAME, e);
                    if (!g.a(e)) {
                        com.shoujiduoduo.base.a.a.a("WelcomeActivity", "start ad is apk_down");
                        hashMap2.put("ad_type", "apk_down");
                        com.umeng.a.b.b(WelcomeActivity.this.getApplicationContext(), "CLICK_SPLASH_APK_DOWN");
                        o.a(WelcomeActivity.this.getApplicationContext()).a(d, agVar.f(), o.a.notifybar, true);
                    } else if ("true".equals(com.umeng.b.a.a().a(RingDDApp.c(), "splash_support_launch_app"))) {
                        g.a(RingDDApp.c(), e);
                        hashMap2.put("ad_type", "apk_launch");
                        com.shoujiduoduo.base.a.a.a("WelcomeActivity", "start ad is apk_launch");
                    } else {
                        hashMap2.put("ad_type", "apk_show");
                        com.shoujiduoduo.base.a.a.a("WelcomeActivity", "start ad is apk_show");
                    }
                    com.umeng.a.b.a(WelcomeActivity.this.getApplicationContext(), "DUODUO_SPLASH_AD_CLICK", hashMap2);
                }
            }
        });
        this.k.sendEmptyMessageDelayed(8, (long) (agVar.a() ? agVar.g() * 1000 : 1500));
    }

    /* access modifiers changed from: private */
    public void g() {
        com.shoujiduoduo.base.a.a.a("WelcomeActivity", "startMainActivity");
        if (this.k != null) {
            this.k.removeCallbacksAndMessages(null);
        }
        h = true;
        Intent intent = new Intent(this, RingToneDuoduoActivity.class);
        a(intent);
        startActivity(intent);
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        finish();
    }

    /* access modifiers changed from: private */
    public void h() {
        if (this.f1441a) {
            g();
        } else {
            this.f1441a = true;
        }
    }

    private void a(Intent intent) {
        intent.setData(getIntent().getData());
        intent.setAction(getIntent().getAction());
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            intent.putExtras(extras);
        }
    }

    private boolean i() {
        String a2 = ae.a(getApplicationContext(), "cur_splash_pic", "");
        return !TextUtils.isEmpty(a2) && q.f(a2);
    }

    private boolean j() {
        int a2 = ae.a(RingDDApp.c(), "user_loginStatus", 0);
        int a3 = ae.a(RingDDApp.c(), "user_vip_type", 0);
        if (a2 != 1 || a3 == 0) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (this.f1441a) {
            h();
        }
        this.f1441a = true;
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.f1441a = false;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.k != null) {
            this.k.removeCallbacksAndMessages(null);
        }
        this.g = true;
        com.shoujiduoduo.base.a.a.a("WelcomeActivity", "onDestroy");
    }

    private boolean b(ag agVar) {
        String d2 = agVar.d();
        String e2 = agVar.e();
        if (ah.c(d2) || ah.c(e2) || !g.a(e2) || !"true".equals(com.umeng.b.a.a().a(RingDDApp.c(), "show_ad_if_app_is_installed"))) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: private */
    public void k() {
        AlphaAnimation alphaAnimation = new AlphaAnimation(0.1f, 1.0f);
        alphaAnimation.setDuration(1000);
        this.b.setImageResource(R.drawable.splash8);
        this.b.startAnimation(alphaAnimation);
        this.b.setVisibility(0);
        this.e.setVisibility(4);
        this.k.sendEmptyMessageDelayed(2, 2000);
    }

    private void l() {
        this.b.setScaleType(ImageView.ScaleType.CENTER_CROP);
        this.b.setImageDrawable(Drawable.createFromPath(ae.a(getApplicationContext(), "cur_splash_pic", "")));
        this.b.setVisibility(0);
        this.c.setVisibility(8);
        this.k.sendEmptyMessageDelayed(2, 2000);
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        com.shoujiduoduo.base.a.a.a("WelcomeActivity", "onStart");
        super.onStart();
    }

    /* access modifiers changed from: protected */
    public void onRestart() {
        com.shoujiduoduo.base.a.a.a("WelcomeActivity", "onRestart");
        super.onRestart();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        com.shoujiduoduo.base.a.a.a("WelcomeActivity", "onStop");
        super.onStop();
    }
}
