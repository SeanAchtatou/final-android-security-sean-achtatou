package com.shoujiduoduo.ui.makering;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.text.format.Time;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.shoujiduoduo.a.c.v;
import com.shoujiduoduo.base.bean.ListType;
import com.shoujiduoduo.base.bean.MakeRingData;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ui.mine.d;
import com.shoujiduoduo.ui.user.UserLoginActivity;
import com.shoujiduoduo.util.an;
import com.shoujiduoduo.util.g;
import com.shoujiduoduo.util.l;
import com.shoujiduoduo.util.q;
import java.io.File;
import java.util.HashMap;

public class MakeRingSaveFragment extends Fragment {

    /* renamed from: a  reason: collision with root package name */
    public boolean f1730a;
    v b = new v() {
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.shoujiduoduo.ui.makering.MakeRingSaveFragment.a(com.shoujiduoduo.ui.makering.MakeRingSaveFragment, boolean):boolean
         arg types: [com.shoujiduoduo.ui.makering.MakeRingSaveFragment, int]
         candidates:
          com.shoujiduoduo.ui.makering.MakeRingSaveFragment.a(com.shoujiduoduo.ui.makering.MakeRingSaveFragment, android.app.ProgressDialog):android.app.ProgressDialog
          com.shoujiduoduo.ui.makering.MakeRingSaveFragment.a(com.shoujiduoduo.ui.makering.MakeRingSaveFragment, com.shoujiduoduo.base.bean.MakeRingData):com.shoujiduoduo.base.bean.MakeRingData
          com.shoujiduoduo.ui.makering.MakeRingSaveFragment.a(com.shoujiduoduo.ui.makering.MakeRingSaveFragment, java.lang.String):java.lang.String
          com.shoujiduoduo.ui.makering.MakeRingSaveFragment.a(com.shoujiduoduo.ui.makering.MakeRingSaveFragment, boolean):boolean */
        public void a(int i, boolean z, String str, String str2) {
            if (MakeRingSaveFragment.this.o) {
                boolean unused = MakeRingSaveFragment.this.o = false;
                if (MakeRingSaveFragment.this.m != null) {
                    new d(MakeRingSaveFragment.this.getActivity(), R.style.DuoDuoDialog, MakeRingSaveFragment.this.m).show();
                }
            }
        }

        public void a(int i) {
        }

        public void b(int i) {
        }

        public void a(String str, boolean z) {
        }

        public void a(String str) {
        }
    };
    /* access modifiers changed from: private */
    public EditText c;
    /* access modifiers changed from: private */
    public TextView d;
    /* access modifiers changed from: private */
    public Button e;
    /* access modifiers changed from: private */
    public Button f;
    /* access modifiers changed from: private */
    public Button g;
    /* access modifiers changed from: private */
    public Button h;
    /* access modifiers changed from: private */
    public Button i;
    /* access modifiers changed from: private */
    public c j;
    /* access modifiers changed from: private */
    public String k;
    /* access modifiers changed from: private */
    public String l;
    /* access modifiers changed from: private */
    public MakeRingData m;
    /* access modifiers changed from: private */
    public b n;
    /* access modifiers changed from: private */
    public boolean o;
    /* access modifiers changed from: private */
    public ProgressDialog p;

    public interface b {
        void a();

        void b();
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View inflate = layoutInflater.inflate((int) R.layout.fragment_makering_save, viewGroup, false);
        this.c = (EditText) inflate.findViewById(R.id.et_save_ring_name);
        if (an.a().b() != 0) {
            com.shoujiduoduo.base.a.a.b("MakeRingSaveFragment", "set ring name:" + an.a().f());
            this.c.setText(an.a().f());
        }
        this.d = (TextView) inflate.findViewById(R.id.tv_save_instraction);
        a aVar = new a();
        this.e = (Button) inflate.findViewById(R.id.btn_save);
        this.e.setOnClickListener(aVar);
        this.f = (Button) inflate.findViewById(R.id.btn_look);
        this.f.setOnClickListener(aVar);
        this.g = (Button) inflate.findViewById(R.id.btn_upload);
        this.g.setOnClickListener(aVar);
        this.h = (Button) inflate.findViewById(R.id.btn_remake);
        this.h.setOnClickListener(aVar);
        this.i = (Button) inflate.findViewById(R.id.btn_set_current);
        this.i.setOnClickListener(aVar);
        this.j = new c();
        this.f1730a = false;
        com.shoujiduoduo.a.a.c.a().a(com.shoujiduoduo.a.a.b.OBSERVER_USER_CENTER, this.b);
        return inflate;
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            this.n = (b) activity;
        } catch (ClassCastException e2) {
            e2.printStackTrace();
            throw new ClassCastException(activity.toString() + " must implement OnRingSaveListener");
        }
    }

    public boolean a() {
        return this.f1730a;
    }

    private class a implements View.OnClickListener {
        private a() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.shoujiduoduo.ui.makering.MakeRingSaveFragment.a(com.shoujiduoduo.ui.makering.MakeRingSaveFragment, boolean):boolean
         arg types: [com.shoujiduoduo.ui.makering.MakeRingSaveFragment, int]
         candidates:
          com.shoujiduoduo.ui.makering.MakeRingSaveFragment.a(com.shoujiduoduo.ui.makering.MakeRingSaveFragment, android.app.ProgressDialog):android.app.ProgressDialog
          com.shoujiduoduo.ui.makering.MakeRingSaveFragment.a(com.shoujiduoduo.ui.makering.MakeRingSaveFragment, com.shoujiduoduo.base.bean.MakeRingData):com.shoujiduoduo.base.bean.MakeRingData
          com.shoujiduoduo.ui.makering.MakeRingSaveFragment.a(com.shoujiduoduo.ui.makering.MakeRingSaveFragment, java.lang.String):java.lang.String
          com.shoujiduoduo.ui.makering.MakeRingSaveFragment.a(com.shoujiduoduo.ui.makering.MakeRingSaveFragment, boolean):boolean */
        public void onClick(View view) {
            if (view.getId() == R.id.btn_save) {
                ((InputMethodManager) MakeRingSaveFragment.this.getActivity().getSystemService("input_method")).hideSoftInputFromWindow(view.getWindowToken(), 0);
                String unused = MakeRingSaveFragment.this.k = MakeRingSaveFragment.this.c.getText().toString();
                if (MakeRingSaveFragment.this.k == null || MakeRingSaveFragment.this.k.equals("")) {
                    Toast.makeText(MakeRingSaveFragment.this.getActivity(), (int) R.string.input_ring_name, 1).show();
                    return;
                }
                if (an.a().b() == 0) {
                    String unused2 = MakeRingSaveFragment.this.l = l.a(3) + MakeRingSaveFragment.this.k + ".mp3";
                } else {
                    String unused3 = MakeRingSaveFragment.this.l = l.a(3) + MakeRingSaveFragment.this.k + "." + q.b(an.a().e());
                }
                if (new File(MakeRingSaveFragment.this.l).exists()) {
                    Toast.makeText(MakeRingSaveFragment.this.getActivity(), (int) R.string.change_ring_name, 1).show();
                } else {
                    an.a().b(MakeRingSaveFragment.this.l, MakeRingSaveFragment.this.j);
                }
            } else if (view.getId() == R.id.btn_look) {
                MakeRingSaveFragment.this.b();
                MakeRingSaveFragment.this.n.b();
            } else if (view.getId() == R.id.btn_upload) {
                MakeRingSaveFragment.this.b();
                com.umeng.a.b.b(MakeRingSaveFragment.this.getActivity(), "USER_CLICK_UPLOAD");
                if (!com.shoujiduoduo.a.b.b.g().g()) {
                    boolean unused4 = MakeRingSaveFragment.this.o = true;
                    MakeRingSaveFragment.this.getActivity().startActivity(new Intent(MakeRingSaveFragment.this.getActivity(), UserLoginActivity.class));
                } else if (MakeRingSaveFragment.this.m != null) {
                    new d(MakeRingSaveFragment.this.getActivity(), R.style.DuoDuoDialog, MakeRingSaveFragment.this.m).show();
                }
            } else if (view.getId() == R.id.btn_remake) {
                MakeRingSaveFragment.this.b();
                MakeRingSaveFragment.this.n.a();
            } else if (view.getId() == R.id.btn_set_current) {
                MakeRingSaveFragment.this.b();
                new com.shoujiduoduo.ui.settings.b(MakeRingSaveFragment.this.getActivity(), R.style.DuoDuoDialog, MakeRingSaveFragment.this.m, "user_make_ring", ListType.LIST_TYPE.list_user_make.toString()).show();
            }
        }
    }

    public void onStart() {
        if (an.a().b() == 1) {
            com.shoujiduoduo.base.a.a.b("MakeRingSaveFragment", "set ring name:" + an.a().f());
            this.c.setText(an.a().f());
        }
        super.onStart();
    }

    /* access modifiers changed from: private */
    public void b() {
        an.a().h();
        com.shoujiduoduo.player.a.b().j();
    }

    private class c extends Handler {
        private c() {
        }

        public void handleMessage(Message message) {
            switch (message.what) {
                case 1:
                    if (MakeRingSaveFragment.this.p == null) {
                        ProgressDialog unused = MakeRingSaveFragment.this.p = new ProgressDialog(MakeRingSaveFragment.this.getActivity());
                        MakeRingSaveFragment.this.p.setProgressStyle(0);
                        MakeRingSaveFragment.this.p.setMessage(MakeRingSaveFragment.this.getResources().getString(R.string.savering));
                        MakeRingSaveFragment.this.p.setIndeterminate(false);
                        MakeRingSaveFragment.this.p.setCancelable(false);
                        MakeRingSaveFragment.this.p.setButton(-2, MakeRingSaveFragment.this.getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogInterface, int i) {
                                an.a().g();
                                dialogInterface.cancel();
                            }
                        });
                        MakeRingSaveFragment.this.p.show();
                        break;
                    }
                    break;
                case 2:
                    if (MakeRingSaveFragment.this.p != null) {
                        MakeRingSaveFragment.this.p.setProgress(((Integer) message.obj).intValue());
                        break;
                    }
                    break;
                case 3:
                    if (MakeRingSaveFragment.this.p != null) {
                        MakeRingSaveFragment.this.p.cancel();
                    }
                    MakeRingSaveFragment.this.c.setVisibility(8);
                    MakeRingSaveFragment.this.d.setText((int) R.string.save_ring_success);
                    MakeRingSaveFragment.this.e.setVisibility(8);
                    MakeRingSaveFragment.this.h.setVisibility(0);
                    MakeRingSaveFragment.this.f.setVisibility(0);
                    MakeRingSaveFragment.this.g.setVisibility(0);
                    MakeRingSaveFragment.this.i.setVisibility(0);
                    MakeRingSaveFragment.this.f1730a = true;
                    MakeRingData unused2 = MakeRingSaveFragment.this.m = new MakeRingData();
                    MakeRingSaveFragment.this.m.artist = "";
                    MakeRingSaveFragment.this.m.name = MakeRingSaveFragment.this.k;
                    MakeRingSaveFragment.this.m.duration = an.a().d() / 1000;
                    MakeRingSaveFragment.this.m.localPath = MakeRingSaveFragment.this.l;
                    Time time = new Time();
                    time.setToNow();
                    MakeRingSaveFragment.this.m.makeDate = time.format("%Y-%m-%d %H:%M");
                    MakeRingSaveFragment.this.m.makeType = an.a().b();
                    com.shoujiduoduo.a.b.b.b().a(MakeRingSaveFragment.this.m, "make_ring_list");
                    HashMap hashMap = new HashMap();
                    if (an.a().b() == 0) {
                        g.a(MakeRingSaveFragment.this.getActivity(), "RECORD_RING_SAVE", hashMap, (long) MakeRingSaveFragment.this.m.duration);
                    } else {
                        g.a(MakeRingSaveFragment.this.getActivity(), "EDIT_RING_SAVE", hashMap, (long) MakeRingSaveFragment.this.m.duration);
                    }
                    MakeRingSaveFragment.this.b();
                    break;
                case 4:
                    if (MakeRingSaveFragment.this.p != null) {
                        MakeRingSaveFragment.this.p.cancel();
                    }
                    MakeRingSaveFragment.this.b();
                    Toast.makeText(MakeRingSaveFragment.this.getActivity(), (String) message.obj, 1).show();
                    break;
            }
            super.handleMessage(message);
        }
    }

    public void onPause() {
        super.onPause();
    }

    public void onDestroyView() {
        super.onDestroyView();
        com.shoujiduoduo.a.a.c.a().b(com.shoujiduoduo.a.a.b.OBSERVER_USER_CENTER, this.b);
    }

    public void onDestroy() {
        super.onDestroy();
        if (this.j != null) {
            this.j.removeCallbacksAndMessages(null);
        }
    }
}
