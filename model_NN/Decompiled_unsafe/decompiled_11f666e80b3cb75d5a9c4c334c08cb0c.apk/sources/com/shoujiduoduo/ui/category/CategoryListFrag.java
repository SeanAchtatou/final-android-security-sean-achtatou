package com.shoujiduoduo.ui.category;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.RelativeLayout;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import cn.banshenggua.aichang.utils.Constants;
import com.shoujiduoduo.a.a.c;
import com.shoujiduoduo.b.a.b;
import com.shoujiduoduo.b.b.a;
import com.shoujiduoduo.player.PlayerService;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.ui.search.AcActivity;
import com.shoujiduoduo.ui.user.RingListActivity;
import com.shoujiduoduo.ui.utils.c;
import com.shoujiduoduo.ui.utils.h;
import com.shoujiduoduo.util.aa;
import com.shoujiduoduo.util.g;
import com.shoujiduoduo.util.o;
import com.shoujiduoduo.util.widget.MyGallery;
import com.shoujiduoduo.util.widget.MyGridView;
import com.shoujiduoduo.util.widget.WebViewActivity;
import com.shoujiduoduo.util.widget.b;
import java.util.ArrayList;
import java.util.HashMap;

public class CategoryListFrag extends Fragment {

    /* renamed from: a  reason: collision with root package name */
    private MyGridView f1648a;
    /* access modifiers changed from: private */
    public c b;
    /* access modifiers changed from: private */
    public MyGallery c;
    private a d;
    private RelativeLayout e;
    /* access modifiers changed from: private */
    public LinearLayout f;
    /* access modifiers changed from: private */
    public ArrayList<a.C0038a> g;
    /* access modifiers changed from: private */
    public ArrayList<b.a> h;
    /* access modifiers changed from: private */
    public d i;
    private com.shoujiduoduo.a.c.b j = new com.shoujiduoduo.a.c.b() {
    };
    private com.shoujiduoduo.a.c.d k = new com.shoujiduoduo.a.c.d() {
        public void a(String str, String str2) {
        }

        public void a(String str) {
        }

        public void a() {
            ArrayList unused = CategoryListFrag.this.g = CategoryListFrag.this.b();
            CategoryListFrag.this.b.notifyDataSetChanged();
        }
    };
    private AdapterView.OnItemClickListener l = new AdapterView.OnItemClickListener() {
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
            a.C0038a aVar = (a.C0038a) CategoryListFrag.this.g.get(i);
            HashMap hashMap = new HashMap();
            hashMap.put(Constants.ITEM, aVar.d + "_" + aVar.f1304a);
            com.umeng.a.b.a(RingDDApp.c(), "category_click", hashMap);
            if (aVar.d.equals("aichang")) {
                com.shoujiduoduo.base.a.a.a("search_ad", "click ad, aichang");
                CategoryListFrag.this.getActivity().startActivity(new Intent(CategoryListFrag.this.getActivity(), AcActivity.class));
            } else {
                Intent intent = new Intent(RingDDApp.c(), RingListActivity.class);
                intent.putExtra("list_id", aVar.d);
                intent.putExtra("title", aVar.f1304a);
                CategoryListFrag.this.startActivity(intent);
            }
            PlayerService b = aa.a().b();
            if (b != null && b.l()) {
                b.m();
            }
        }
    };

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.i = new d();
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View inflate = layoutInflater.inflate((int) R.layout.category_entrance, viewGroup, false);
        this.f1648a = (MyGridView) inflate.findViewById(R.id.categoryGridView);
        this.e = (RelativeLayout) inflate.findViewById(R.id.recommend_adv_panel);
        this.c = (MyGallery) inflate.findViewById(R.id.recommend_adv);
        this.f = (LinearLayout) inflate.findViewById(R.id.recommend_hint_panel);
        this.b = new c();
        this.f1648a.setAdapter((ListAdapter) this.b);
        this.e.setVisibility(8);
        this.d = new a();
        this.c.setAdapter((SpinnerAdapter) this.d);
        this.c.setOnItemClickListener(new b());
        this.f1648a.setOnItemClickListener(this.l);
        com.shoujiduoduo.a.a.c.a().a(com.shoujiduoduo.a.a.b.OBSERVER_CATEGORY, this.k);
        com.shoujiduoduo.a.a.c.a().a(com.shoujiduoduo.a.a.b.OBSERVER_BANNER_AD, this.j);
        c();
        a();
        return inflate;
    }

    public void onDestroyView() {
        super.onDestroyView();
        com.shoujiduoduo.a.a.c.a().b(com.shoujiduoduo.a.a.b.OBSERVER_CATEGORY, this.k);
        com.shoujiduoduo.a.a.c.a().b(com.shoujiduoduo.a.a.b.OBSERVER_BANNER_AD, this.j);
    }

    public void onDetach() {
        super.onDetach();
    }

    private class d extends Handler {
        private d() {
        }

        public void handleMessage(Message message) {
            super.handleMessage(message);
            if (message.what == 1136) {
                CategoryListFrag.this.b.notifyDataSetChanged();
            } else if (message.what == 1) {
                if (CategoryListFrag.this.c.isShown()) {
                    CategoryListFrag.this.c.b();
                }
                CategoryListFrag.this.i.sendEmptyMessageDelayed(1, 3000);
            }
        }
    }

    private class c extends BaseAdapter {
        private LayoutInflater b = ((LayoutInflater) RingDDApp.c().getSystemService("layout_inflater"));

        public c() {
        }

        public int getCount() {
            if (CategoryListFrag.this.g == null) {
                return 0;
            }
            return CategoryListFrag.this.g.size();
        }

        public Object getItem(int i) {
            if (CategoryListFrag.this.g == null) {
                return null;
            }
            return CategoryListFrag.this.g.get(i);
        }

        public long getItemId(int i) {
            return (long) i;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [?, android.view.ViewGroup, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        public View getView(int i, View view, ViewGroup viewGroup) {
            if (view == null) {
                view = this.b.inflate((int) R.layout.listitem_category_item, viewGroup, false);
            }
            ImageView imageView = (ImageView) view.findViewById(R.id.category_item_pic);
            String str = ((a.C0038a) CategoryListFrag.this.g.get(i)).d;
            if (CategoryListFrag.this.a(str) != 0) {
                imageView.setImageResource(CategoryListFrag.this.a(str));
            } else {
                com.d.a.b.d.a().a(((a.C0038a) CategoryListFrag.this.g.get(i)).b, imageView, h.a().f());
            }
            ((TextView) view.findViewById(R.id.category_item_name)).setText(((a.C0038a) CategoryListFrag.this.g.get(i)).f1304a);
            return view;
        }
    }

    /* access modifiers changed from: private */
    public int a(String str) {
        char c2 = 65535;
        switch (str.hashCode()) {
            case -1005771731:
                if (str.equals("aichang")) {
                    c2 = 16;
                    break;
                }
                break;
            case 50:
                if (str.equals("2")) {
                    c2 = 0;
                    break;
                }
                break;
            case 51:
                if (str.equals("3")) {
                    c2 = 1;
                    break;
                }
                break;
            case 52:
                if (str.equals("4")) {
                    c2 = 2;
                    break;
                }
                break;
            case 53:
                if (str.equals("5")) {
                    c2 = 3;
                    break;
                }
                break;
            case 54:
                if (str.equals(com.tencent.connect.common.Constants.VIA_SHARE_TYPE_INFO)) {
                    c2 = 4;
                    break;
                }
                break;
            case 55:
                if (str.equals("7")) {
                    c2 = 5;
                    break;
                }
                break;
            case 56:
                if (str.equals("8")) {
                    c2 = 6;
                    break;
                }
                break;
            case 57:
                if (str.equals("9")) {
                    c2 = 7;
                    break;
                }
                break;
            case 1598:
                if (str.equals("20")) {
                    c2 = 14;
                    break;
                }
                break;
            case 1599:
                if (str.equals(com.tencent.connect.common.Constants.VIA_REPORT_TYPE_QQFAVORITES)) {
                    c2 = 12;
                    break;
                }
                break;
            case 1604:
                if (str.equals("26")) {
                    c2 = 13;
                    break;
                }
                break;
            case 48626:
                if (str.equals("101")) {
                    c2 = 8;
                    break;
                }
                break;
            case 48627:
                if (str.equals("102")) {
                    c2 = 9;
                    break;
                }
                break;
            case 48628:
                if (str.equals("103")) {
                    c2 = 10;
                    break;
                }
                break;
            case 48629:
                if (str.equals("104")) {
                    c2 = 15;
                    break;
                }
                break;
            case 48631:
                if (str.equals("106")) {
                    c2 = 11;
                    break;
                }
                break;
        }
        switch (c2) {
            case 0:
                return R.drawable.cate_2_normal;
            case 1:
                return R.drawable.cate_3_normal;
            case 2:
                return R.drawable.cate_4_normal;
            case 3:
                return R.drawable.cate_5_normal;
            case 4:
                return R.drawable.cate_6_normal;
            case 5:
                return R.drawable.cate_7_normal;
            case 6:
                return R.drawable.cate_8_normal;
            case 7:
                return R.drawable.cate_9_normal;
            case 8:
                return R.drawable.cate_101_normal;
            case 9:
                return R.drawable.cate_102_normal;
            case 10:
                return R.drawable.cate_103_normal;
            case 11:
            case 12:
            case 13:
            case 14:
                return R.drawable.cate_106_normal;
            case 15:
                return R.drawable.cate_104_normal;
            case 16:
                return R.drawable.aichang_tuiguang;
            default:
                return 0;
        }
    }

    private class a extends BaseAdapter {
        private LayoutInflater b = ((LayoutInflater) RingDDApp.c().getSystemService("layout_inflater"));

        a() {
        }

        public int getCount() {
            if (CategoryListFrag.this.h == null || CategoryListFrag.this.h.size() == 0) {
                return 0;
            }
            return Integer.MAX_VALUE;
        }

        public Object getItem(int i) {
            if (CategoryListFrag.this.h == null || CategoryListFrag.this.h.size() == 0) {
                return null;
            }
            return CategoryListFrag.this.h.get(i % CategoryListFrag.this.h.size());
        }

        public long getItemId(int i) {
            return (long) i;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [?, android.view.ViewGroup, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        public View getView(int i, View view, ViewGroup viewGroup) {
            Drawable a2;
            com.shoujiduoduo.base.a.a.a("CategoryListFrag", "Gallery adapter getview, pos:" + i);
            if (view == null) {
                view = this.b.inflate((int) R.layout.advertisement_item, viewGroup, false);
            }
            final ImageView imageView = (ImageView) view.findViewById(R.id.advertisement_image);
            if (!(CategoryListFrag.this.h == null || CategoryListFrag.this.h.size() <= 0 || CategoryListFrag.this.h.get(i % CategoryListFrag.this.h.size()) == null || (a2 = com.shoujiduoduo.ui.utils.c.a(((b.a) CategoryListFrag.this.h.get(i % CategoryListFrag.this.h.size())).e, new c.a() {
                public void a(Drawable drawable, String str) {
                    if (!CategoryListFrag.this.getActivity().isFinishing() && drawable != null) {
                        com.shoujiduoduo.base.a.a.a("CategoryListFrag", "set image drawable 1");
                        imageView.setImageDrawable(drawable);
                    }
                }
            })) == null)) {
                com.shoujiduoduo.base.a.a.a("CategoryListFrag", "set image drawable 2");
                imageView.setImageDrawable(a2);
            }
            return view;
        }
    }

    private class b implements AdapterView.OnItemClickListener {
        private b() {
        }

        public void onItemClick(AdapterView<?> adapterView, View view, final int i, long j) {
            String str = ((b.a) CategoryListFrag.this.h.get(i % CategoryListFrag.this.h.size())).f1290a;
            String str2 = ((b.a) CategoryListFrag.this.h.get(i % CategoryListFrag.this.h.size())).b;
            HashMap hashMap = new HashMap();
            hashMap.put("type", str + " - " + str2);
            com.umeng.a.b.a(RingDDApp.c(), "CLICK_BANNER_AD", hashMap);
            if (str.equals("down")) {
                if (!TextUtils.isEmpty(((b.a) CategoryListFrag.this.h.get(i % CategoryListFrag.this.h.size())).f)) {
                    new b.a(CategoryListFrag.this.getActivity()).b((int) R.string.hint).a(((b.a) CategoryListFrag.this.h.get(i % CategoryListFrag.this.h.size())).c).a((int) R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialogInterface, int i) {
                            o.a(CategoryListFrag.this.getActivity()).a(((b.a) CategoryListFrag.this.h.get(i % CategoryListFrag.this.h.size())).f, ((b.a) CategoryListFrag.this.h.get(i % CategoryListFrag.this.h.size())).b);
                            dialogInterface.dismiss();
                            HashMap hashMap = new HashMap();
                            hashMap.put("type", "ok");
                            com.umeng.a.b.a(RingDDApp.c(), "APK_DOWN_DIALOG_CLICK", hashMap);
                        }
                    }).b((int) R.string.cancel, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                            HashMap hashMap = new HashMap();
                            hashMap.put("type", "cancel");
                            com.umeng.a.b.a(RingDDApp.c(), "APK_DOWN_DIALOG_CLICK", hashMap);
                        }
                    }).a().show();
                }
            } else if (str.equals("web")) {
                Intent intent = new Intent(CategoryListFrag.this.getActivity(), WebViewActivity.class);
                intent.putExtra("url", ((b.a) CategoryListFrag.this.h.get(i % CategoryListFrag.this.h.size())).f);
                intent.putExtra("apkname", ((b.a) CategoryListFrag.this.h.get(i % CategoryListFrag.this.h.size())).b);
                CategoryListFrag.this.getActivity().startActivity(intent);
            } else if (str.equals("search")) {
                com.shoujiduoduo.a.a.c.a().a(com.shoujiduoduo.a.a.b.OBSERVER_CATEGORY, new c.a<com.shoujiduoduo.a.c.d>() {
                    public void a() {
                        ((com.shoujiduoduo.a.c.d) this.f1284a).a(((b.a) CategoryListFrag.this.h.get(i % CategoryListFrag.this.h.size())).d);
                    }
                });
            } else if (str.equals("list")) {
                com.shoujiduoduo.a.a.c.a().a(com.shoujiduoduo.a.a.b.OBSERVER_CATEGORY, new c.a<com.shoujiduoduo.a.c.d>() {
                    public void a() {
                        String str = ((b.a) CategoryListFrag.this.h.get(i % CategoryListFrag.this.h.size())).g;
                        ((com.shoujiduoduo.a.c.d) this.f1284a).a(((b.a) CategoryListFrag.this.h.get(i % CategoryListFrag.this.h.size())).b, str);
                    }
                });
                PlayerService b = aa.a().b();
                if (b != null && b.l()) {
                    b.m();
                }
            } else {
                com.shoujiduoduo.base.a.a.c("CategoryListFrag", "not support ad type:" + str);
            }
        }
    }

    private void a() {
        this.g = b();
        this.b.notifyDataSetChanged();
    }

    /* access modifiers changed from: private */
    public ArrayList<a.C0038a> b() {
        return com.shoujiduoduo.a.b.b.d().c();
    }

    private void c() {
        if (com.shoujiduoduo.a.b.b.c().c()) {
            com.shoujiduoduo.base.a.a.a("CategoryListFrag", "banner ad data is ready");
            this.h = com.shoujiduoduo.a.b.b.c().d();
            if (this.h.size() > 0) {
                this.e.setVisibility(0);
                this.d.notifyDataSetChanged();
                if (this.h.size() > 1) {
                    this.i.sendEmptyMessageDelayed(1, 3000);
                }
                this.e.setLayoutParams(new LinearLayout.LayoutParams(-1, g.a((float) com.shoujiduoduo.a.b.b.c().e())));
                final int size = this.h.size();
                for (int i2 = 0; i2 < size; i2++) {
                    ImageView imageView = new ImageView(getActivity());
                    imageView.setBackgroundResource(R.drawable.adv_hint_normal);
                    this.f.addView(imageView, new LinearLayout.LayoutParams(-2, -2));
                }
                this.c.setSelection(0);
                this.c.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long j) {
                        int i2 = 0;
                        while (true) {
                            int i3 = i2;
                            if (i3 < size) {
                                ImageView imageView = (ImageView) CategoryListFrag.this.f.getChildAt(i3 % size);
                                if (i3 == i % size) {
                                    imageView.setBackgroundResource(R.drawable.adv_hint_selected);
                                } else {
                                    imageView.setBackgroundResource(R.drawable.adv_hint_normal);
                                }
                                i2 = i3 + 1;
                            } else {
                                return;
                            }
                        }
                    }

                    public void onNothingSelected(AdapterView<?> adapterView) {
                    }
                });
                return;
            }
            this.e.setVisibility(8);
            return;
        }
        com.shoujiduoduo.base.a.a.a("CategoryListFrag", "banner ad data is not ready");
    }
}
