package com.shoujiduoduo.ui.utils;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.d.a.b.d;
import com.shoujiduoduo.b.c.c;
import com.shoujiduoduo.base.bean.CollectData;
import com.shoujiduoduo.base.bean.DDList;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.util.g;
import com.shoujiduoduo.util.r;
import java.text.DecimalFormat;

/* compiled from: CollectListAdapter */
public class e extends d {

    /* renamed from: a  reason: collision with root package name */
    private c f2069a;
    private Context b;

    public e(Context context) {
        this.b = context;
    }

    public int getCount() {
        if (this.f2069a != null) {
            return this.f2069a.size();
        }
        com.shoujiduoduo.base.a.a.a("CollectListAdapter", "count:0");
        return 0;
    }

    public Object getItem(int i) {
        if (this.f2069a != null) {
            return this.f2069a.get(i);
        }
        return null;
    }

    public long getItemId(int i) {
        return (long) i;
    }

    /* compiled from: CollectListAdapter */
    private static class a {

        /* renamed from: a  reason: collision with root package name */
        ImageView f2070a;
        TextView b;
        TextView c;
        TextView d;
        TextView e;
        TextView f;

        private a() {
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View getView(int i, View view, ViewGroup viewGroup) {
        a aVar;
        if (this.f2069a == null) {
            com.shoujiduoduo.base.a.a.a("CollectListAdapter", "return null");
            return null;
        }
        if (view == null) {
            view = LayoutInflater.from(this.b).inflate((int) R.layout.listitem_collect, viewGroup, false);
            a aVar2 = new a();
            aVar2.f2070a = (ImageView) view.findViewById(R.id.pic);
            aVar2.b = (TextView) view.findViewById(R.id.title);
            aVar2.c = (TextView) view.findViewById(R.id.content);
            if (g.l()) {
                aVar2.c.setLines(1);
            }
            aVar2.d = (TextView) view.findViewById(R.id.releate_time);
            aVar2.e = (TextView) view.findViewById(R.id.fav_num);
            aVar2.f = (TextView) view.findViewById(R.id.artist);
            view.setTag(aVar2);
            aVar = aVar2;
        } else {
            aVar = (a) view.getTag();
        }
        CollectData a2 = this.f2069a.get(i);
        com.shoujiduoduo.base.a.a.a("CollectListAdapter", "title:" + a2.title + ", content:" + a2.content);
        d.a().a(a2.pic, aVar.f2070a, h.a().h());
        aVar.b.setText(a2.title);
        aVar.c.setText(a2.content);
        aVar.d.setText(a2.time);
        aVar.f.setText(TextUtils.isEmpty(a2.artist) ? "多多网友" : a2.artist);
        int a3 = r.a(a2.favNum, 1000);
        StringBuilder sb = new StringBuilder();
        if (a3 > 10000) {
            sb.append(new DecimalFormat("#.00").format((double) (((float) a3) / 10000.0f)));
            sb.append("万");
        } else {
            sb.append(a3);
        }
        aVar.e.setText(sb.toString());
        return view;
    }

    public void a(boolean z) {
    }

    public void a(DDList dDList) {
        this.f2069a = (c) dDList;
    }

    public void a() {
    }

    public void b() {
    }
}
