package com.shoujiduoduo.ui.utils;

import android.content.Context;
import android.content.Intent;
import android.text.SpannableString;
import android.text.TextPaint;
import android.text.style.ClickableSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.d.a.b.d;
import com.shoujiduoduo.a.b.b;
import com.shoujiduoduo.b.c.e;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.base.bean.CommentData;
import com.shoujiduoduo.base.bean.DDList;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.ui.mine.UserMainPageActivity;
import com.shoujiduoduo.ui.utils.TextViewFixTouchConsume;
import com.shoujiduoduo.util.ae;
import com.shoujiduoduo.util.ah;
import com.shoujiduoduo.util.g;
import com.shoujiduoduo.util.t;
import java.text.ParseException;
import java.text.SimpleDateFormat;

/* compiled from: CommentListAdapter */
public class f extends d {

    /* renamed from: a  reason: collision with root package name */
    private e f2071a;
    /* access modifiers changed from: private */
    public Context b;

    public f(Context context) {
        this.b = context;
    }

    public void a(boolean z) {
    }

    public void a(DDList dDList) {
        this.f2071a = (e) dDList;
    }

    public void a() {
    }

    public void b() {
    }

    public int getCount() {
        if (this.f2071a != null) {
            return this.f2071a.size();
        }
        return 0;
    }

    public Object getItem(int i) {
        if (this.f2071a != null) {
            return this.f2071a.get(i);
        }
        return null;
    }

    public long getItemId(int i) {
        return (long) i;
    }

    /* access modifiers changed from: private */
    public boolean a(CommentData commentData) {
        return ae.a(RingDDApp.c(), "upvote_comment_list", "").contains(commentData.cid);
    }

    /* access modifiers changed from: private */
    public void b(CommentData commentData) {
        String str;
        String a2 = ae.a(RingDDApp.c(), "upvote_comment_list", "");
        if (a2.equals("")) {
            str = commentData.cid;
        } else {
            str = a2 + "|" + commentData.cid;
        }
        ae.c(RingDDApp.c(), "upvote_comment_list", str);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = LayoutInflater.from(this.b).inflate((int) R.layout.listitem_comment, viewGroup, false);
        }
        ImageView imageView = (ImageView) m.a(view, R.id.user_head);
        TextView textView = (TextView) m.a(view, R.id.user_name);
        TextView textView2 = (TextView) m.a(view, R.id.create_time);
        TextView textView3 = (TextView) m.a(view, R.id.vote_num);
        TextViewFixTouchConsume textViewFixTouchConsume = (TextViewFixTouchConsume) m.a(view, R.id.comment);
        TextViewFixTouchConsume textViewFixTouchConsume2 = (TextViewFixTouchConsume) m.a(view, R.id.tcomment);
        TextView textView4 = (TextView) m.a(view, R.id.comment_catetory);
        ImageView imageView2 = (ImageView) m.a(view, R.id.tv_upvote);
        final CommentData commentData = (CommentData) this.f2071a.get(i);
        if (!ah.c(commentData.head_url)) {
            d.a().a(commentData.head_url, imageView, h.a().e());
        }
        imageView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent intent = new Intent(RingDDApp.c(), UserMainPageActivity.class);
                intent.putExtra("tuid", commentData.uid);
                f.this.b.startActivity(intent);
            }
        });
        textView.setText(commentData.name);
        try {
            textView2.setText(g.a(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(commentData.createtime)));
        } catch (ParseException e) {
            e.printStackTrace();
            textView2.setText(commentData.createtime);
        }
        if (commentData.upvote > 0) {
            textView3.setText("" + commentData.upvote);
        } else {
            textView3.setText("");
        }
        if (a(commentData)) {
            imageView2.setImageResource(R.drawable.icon_upvote_pressed);
            textView3.setTextColor(RingDDApp.c().getResources().getColor(R.color.text_green));
        } else {
            imageView2.setImageResource(R.drawable.icon_upvote_normal);
            textView3.setTextColor(RingDDApp.c().getResources().getColor(R.color.text_gray));
        }
        m.a(view, R.id.vote_layout).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if (!f.this.a(commentData)) {
                    a.a("CommentListAdapter", "顶评论， comment：" + commentData.comment + ", cid:" + commentData.cid);
                    commentData.upvote++;
                    f.this.b(commentData);
                    f.this.notifyDataSetChanged();
                    t.a("upvote", "&rid=" + commentData.rid + "&uid=" + b.g().c().getUid() + "&cid=" + commentData.cid, new t.a() {
                        public void a(String str) {
                            a.a("CommentListAdapter", "vote comment success:" + str);
                        }

                        public void a(String str, String str2) {
                            a.a("CommentListAdapter", "vote comment error");
                        }
                    });
                }
            }
        });
        if (!ah.c(commentData.tcid)) {
            textViewFixTouchConsume.setMovementMethod(TextViewFixTouchConsume.a.a());
            textViewFixTouchConsume.setFocusable(false);
            textViewFixTouchConsume2.setMovementMethod(TextViewFixTouchConsume.a.a());
            textViewFixTouchConsume2.setFocusable(false);
            SpannableString spannableString = new SpannableString("回复@" + commentData.tname + ":" + commentData.comment);
            SpannableString spannableString2 = new SpannableString("@" + commentData.tname + ":" + commentData.tcomment);
            final int color = this.b.getResources().getColor(R.color.text_blue);
            AnonymousClass3 r3 = new ClickableSpan() {
                public void updateDrawState(TextPaint textPaint) {
                    super.updateDrawState(textPaint);
                    textPaint.setColor(color);
                    textPaint.setUnderlineText(false);
                }

                public void onClick(View view) {
                    Intent intent = new Intent(RingDDApp.c(), UserMainPageActivity.class);
                    intent.putExtra("tuid", commentData.tuid);
                    f.this.b.startActivity(intent);
                }
            };
            spannableString.setSpan(r3, 2, ("@" + commentData.tname).length() + 2, 17);
            spannableString2.setSpan(r3, 0, ("@" + commentData.tname).length(), 17);
            textViewFixTouchConsume2.setText(spannableString2);
            textViewFixTouchConsume2.setVisibility(0);
            textViewFixTouchConsume.setText(spannableString);
        } else {
            textViewFixTouchConsume.setText(commentData.comment);
            textViewFixTouchConsume2.setVisibility(8);
        }
        if (!ah.c(commentData.catetoryHint)) {
            textView4.setText(commentData.catetoryHint);
            textView4.setVisibility(0);
        } else {
            textView4.setVisibility(8);
        }
        return view;
    }
}
