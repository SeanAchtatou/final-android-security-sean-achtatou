package com.shoujiduoduo.ui.utils;

import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.baoyz.widget.PullRefreshLayout;
import com.shoujiduoduo.a.a.c;
import com.shoujiduoduo.a.c.m;
import com.shoujiduoduo.a.c.p;
import com.shoujiduoduo.a.c.v;
import com.shoujiduoduo.a.c.x;
import com.shoujiduoduo.b.c.j;
import com.shoujiduoduo.base.bean.CommentData;
import com.shoujiduoduo.base.bean.DDList;
import com.shoujiduoduo.base.bean.ListType;
import com.shoujiduoduo.base.bean.MessageData;
import com.shoujiduoduo.base.bean.RingData;
import com.shoujiduoduo.base.bean.UserData;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.ui.cailing.f;
import com.shoujiduoduo.ui.home.ArtistRingActivity;
import com.shoujiduoduo.ui.home.CollectRingActivity;
import com.shoujiduoduo.ui.mine.UserMainPageActivity;
import com.shoujiduoduo.ui.user.CommentActivity;
import com.shoujiduoduo.util.ah;
import com.shoujiduoduo.util.g;
import com.shoujiduoduo.util.r;
import com.sina.weibo.sdk.register.mobile.SelectCountryActivity;

public class DDListFragment extends LazyFragment implements PullRefreshLayout.a {
    /* access modifiers changed from: private */
    public boolean A;
    private View.OnClickListener B = new View.OnClickListener() {
        public void onClick(View view) {
            if (!DDListFragment.this.A) {
                new com.shoujiduoduo.ui.cailing.f(DDListFragment.this.getActivity(), R.style.DuoDuoDialog, g.b.f2302a, new f.a() {
                    public void a(f.a.C0045a aVar) {
                        if (aVar.equals(f.a.C0045a.open)) {
                        }
                    }
                }).show();
            } else if (DDListFragment.this.b != null) {
                com.shoujiduoduo.base.a.a.a("DDListFragment", "重新获取彩铃库");
                DDListFragment.this.a(g.LIST_LOADING);
                DDListFragment.this.b.retrieveData();
            }
        }
    };
    private x C = new x() {
        public void a(int i) {
            if (DDListFragment.this.b != null && DDListFragment.this.h()) {
                com.shoujiduoduo.base.a.a.a("DDListFragment", "vipType:" + i);
                if ((i == 1 && !DDListFragment.this.b.getListType().equals(ListType.LIST_TYPE.list_ring_cmcc)) || ((i == 3 && !DDListFragment.this.b.getListType().equals(ListType.LIST_TYPE.list_ring_cucc)) || (i == 2 && !DDListFragment.this.b.getListType().equals(ListType.LIST_TYPE.list_ring_ctcc)))) {
                    switch (i) {
                        case 1:
                            DDList unused = DDListFragment.this.b = new j(ListType.LIST_TYPE.list_ring_cmcc, "", false, "");
                            break;
                        case 2:
                            DDList unused2 = DDListFragment.this.b = new j(ListType.LIST_TYPE.list_ring_ctcc, "", false, "");
                            break;
                        case 3:
                            DDList unused3 = DDListFragment.this.b = new j(ListType.LIST_TYPE.list_ring_cucc, "", false, "");
                            break;
                    }
                    com.shoujiduoduo.base.a.a.a("DDListFragment", "vipType:" + i + ", cur list id:" + DDListFragment.this.b.getListId());
                    DDListFragment.this.a(g.LIST_LOADING);
                    DDListFragment.this.b.reloadData();
                    DDListFragment.this.f();
                }
            }
        }
    };
    private p D = new p() {
        public void a(int i, RingData ringData) {
            if (i == 16 && DDListFragment.this.b != null) {
                if (DDListFragment.this.b.getListId().equals("cmcc_cailing") || DDListFragment.this.b.getListId().equals("ctcc_cailing") || DDListFragment.this.b.getListId().equals("cucc_cailing")) {
                    com.shoujiduoduo.base.a.a.a("DDListFragment", "default cailing change, refresh list");
                    if (DDListFragment.this.h != null) {
                        DDListFragment.this.h.notifyDataSetChanged();
                    }
                }
            }
        }
    };
    private v E = new v() {
        public void a(int i, boolean z, String str, String str2) {
        }

        public void a(int i) {
        }

        public void b(int i) {
        }

        public void a(String str, boolean z) {
            if (DDListFragment.this.h != null && DDListFragment.this.i != null) {
                DDListFragment.this.h.notifyDataSetChanged();
                if (DDListFragment.this.b == null) {
                    return;
                }
                if (DDListFragment.this.b.getListId().startsWith("concern_ring") || DDListFragment.this.b.getListId().startsWith("concern_feeds")) {
                    com.shoujiduoduo.base.a.a.a("DDListFragment", "listid:" + DDListFragment.this.b.getListId() + ", onFollowingsChange, reloadData");
                    DDListFragment.this.b.reloadData();
                }
            }
        }

        public void a(String str) {
        }
    };
    private com.shoujiduoduo.a.c.e F = new com.shoujiduoduo.a.c.e() {
        public void a(CommentData commentData) {
        }

        public void a() {
            if (DDListFragment.this.b != null && DDListFragment.this.b.getListId().equals("comment_list")) {
                DDListFragment.this.a(g.LIST_LOADING);
                DDListFragment.this.b.reloadData();
            }
        }
    };
    private com.shoujiduoduo.a.c.c G = new com.shoujiduoduo.a.c.c() {
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.shoujiduoduo.ui.utils.DDListFragment.a(com.shoujiduoduo.ui.utils.DDListFragment, boolean):boolean
         arg types: [com.shoujiduoduo.ui.utils.DDListFragment, int]
         candidates:
          com.shoujiduoduo.ui.utils.DDListFragment.a(com.shoujiduoduo.ui.utils.DDListFragment, int):int
          com.shoujiduoduo.ui.utils.DDListFragment.a(com.shoujiduoduo.ui.utils.DDListFragment, com.shoujiduoduo.base.bean.DDList):com.shoujiduoduo.base.bean.DDList
          com.shoujiduoduo.ui.utils.DDListFragment.a(com.shoujiduoduo.ui.utils.DDListFragment, com.shoujiduoduo.ui.utils.DDListFragment$e):void
          com.shoujiduoduo.ui.utils.DDListFragment.a(com.shoujiduoduo.ui.utils.DDListFragment, boolean):boolean */
        public void a(boolean z, g.b bVar) {
            if (DDListFragment.this.b != null && DDListFragment.this.b.getListId().equals("cmcc_cailing")) {
                com.shoujiduoduo.base.a.a.a("DDListFragment", "on Cailing Status change:" + z);
                if (z) {
                    com.shoujiduoduo.base.a.a.a("DDListFragment", "cailing is wait to open");
                    boolean unused = DDListFragment.this.z = false;
                    boolean unused2 = DDListFragment.this.A = true;
                    DDListFragment.this.a(g.LIST_LOADING);
                    DDListFragment.this.b.retrieveData();
                    return;
                }
                boolean unused3 = DDListFragment.this.z = true;
                DDListFragment.this.a(g.LIST_FAILED);
            }
        }

        public void a(g.b bVar) {
            if (DDListFragment.this.b == null) {
                return;
            }
            if (DDListFragment.this.b.getListId().equals("cmcc_cailing") || DDListFragment.this.b.getListId().equals("ctcc_cailing") || DDListFragment.this.b.getListId().equals("cucc_cailing")) {
                com.shoujiduoduo.base.a.a.a("DDListFragment", "onOrderCailing");
                DDListFragment.this.a(g.LIST_LOADING);
                DDListFragment.this.b.reloadData();
            }
        }

        public void b(g.b bVar) {
            if (DDListFragment.this.b == null) {
                return;
            }
            if (DDListFragment.this.b.getListId().equals("cmcc_cailing") || DDListFragment.this.b.getListId().equals("ctcc_cailing") || DDListFragment.this.b.getListId().equals("cucc_cailing")) {
                com.shoujiduoduo.base.a.a.a("DDListFragment", "onDeleteCailing");
                DDListFragment.this.a(g.LIST_LOADING);
                DDListFragment.this.b.reloadData();
            }
        }
    };
    private com.shoujiduoduo.a.c.g H = new com.shoujiduoduo.a.c.g() {
        public void a(DDList dDList, int i) {
            if (DDListFragment.this.b != null && dDList.getListId().equals(DDListFragment.this.b.getListId())) {
                com.shoujiduoduo.base.a.a.a("DDListFragment", "onDataUpdate in, id:" + DDListFragment.this.b.getListId());
                if (DDListFragment.this.j) {
                    boolean unused = DDListFragment.this.j = false;
                    DDListFragment.this.g.setRefreshing(false);
                }
                switch (i) {
                    case 0:
                    case 5:
                    case 6:
                        if (DDListFragment.this.t == g.LIST_LOADING) {
                            com.shoujiduoduo.base.a.a.a("DDListFragment", "show content now! listid:" + dDList.getListId());
                            DDListFragment.this.a(g.LIST_CONTENT);
                        }
                        DDListFragment.this.h.notifyDataSetChanged();
                        return;
                    case 1:
                        com.shoujiduoduo.base.a.a.a("DDListFragment", "show failed now. listid:" + dDList.getListId());
                        DDListFragment.this.a(g.LIST_FAILED);
                        return;
                    case 2:
                        com.shoujiduoduo.base.a.a.a("DDListFragment", "more data ready. notify the adapter to update. listid:" + dDList.getListId());
                        com.shoujiduoduo.base.a.a.a("DDListFragment", "FooterState: set failed onDataUpdate.");
                        DDListFragment.this.a(e.RETRIEVE_FAILED);
                        DDListFragment.this.h.notifyDataSetChanged();
                        return;
                    case 3:
                        com.shoujiduoduo.util.widget.d.a("已经是最新啦");
                        return;
                    case 4:
                        com.shoujiduoduo.util.widget.d.a("获取失败");
                        return;
                    default:
                        return;
                }
            }
        }
    };
    private AbsListView.OnScrollListener I = new AbsListView.OnScrollListener() {

        /* renamed from: a  reason: collision with root package name */
        boolean f2036a = false;

        public void onScrollStateChanged(AbsListView absListView, int i) {
            if (this.f2036a && i == 0) {
                if (DDListFragment.this.b != null) {
                    if (DDListFragment.this.b.hasMoreData()) {
                        if (!DDListFragment.this.b.isRetrieving()) {
                            DDListFragment.this.b.retrieveData();
                            DDListFragment.this.a(e.RETRIEVE);
                        }
                    } else if (DDListFragment.this.b.size() > 1) {
                        DDListFragment.this.a(e.TOTAL);
                    } else {
                        DDListFragment.this.a(e.INVISIBLE);
                    }
                }
                this.f2036a = false;
            }
        }

        public void onScroll(AbsListView absListView, int i, int i2, int i3) {
            if (i + i2 == i3 && i3 > 0 && i2 < i3) {
                this.f2036a = true;
            }
        }
    };
    /* access modifiers changed from: private */
    public DDList b;
    /* access modifiers changed from: private */
    public RelativeLayout c;
    private RelativeLayout d;
    private RelativeLayout e;
    private RelativeLayout f;
    /* access modifiers changed from: private */
    public PullRefreshLayout g;
    /* access modifiers changed from: private */
    public d h;
    /* access modifiers changed from: private */
    public ListView i;
    /* access modifiers changed from: private */
    public boolean j;
    private Button k;
    private Button l;
    private View m;
    private View n;
    private boolean o;
    private boolean p;
    private boolean q;
    private boolean r;
    private boolean s;
    /* access modifiers changed from: private */
    public g t = g.LIST_FAILED;
    private boolean u;
    private boolean v;
    private int w;
    private int x;
    private String y;
    /* access modifiers changed from: private */
    public boolean z;

    enum e {
        RETRIEVE,
        TOTAL,
        RETRIEVE_FAILED,
        INVISIBLE
    }

    public enum g {
        LIST_CONTENT,
        LIST_LOADING,
        LIST_FAILED
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, ?[OBJECT, ARRAY], int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View inflate = layoutInflater.inflate((int) R.layout.ring_list_panel, viewGroup, false);
        this.c = (RelativeLayout) inflate.findViewById(R.id.content_view);
        this.d = (RelativeLayout) inflate.findViewById(R.id.failed_view);
        this.e = (RelativeLayout) inflate.findViewById(R.id.cailing_not_open_view);
        this.e.setVisibility(8);
        this.f = (RelativeLayout) inflate.findViewById(R.id.loading_view);
        this.f.setVisibility(0);
        ((AnimationDrawable) ((ImageView) this.f.findViewById(R.id.loading)).getBackground()).start();
        this.g = (PullRefreshLayout) inflate.findViewById(R.id.swipeRefreshLayout);
        this.g.setOnRefreshListener(this);
        this.e.findViewById(R.id.open).setOnClickListener(this.B);
        this.i = (ListView) this.c.findViewById(R.id.list_view);
        this.m = layoutInflater.inflate((int) R.layout.get_more_rings, (ViewGroup) null, false);
        if (this.m != null) {
            this.i.addFooterView(this.m);
            this.m.setVisibility(4);
        }
        this.i.setOnScrollListener(this.I);
        this.d.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if (DDListFragment.this.b != null) {
                    DDListFragment.this.a(g.LIST_LOADING);
                    DDListFragment.this.b.retrieveData();
                }
            }
        });
        Bundle arguments = getArguments();
        if (arguments != null) {
            this.o = arguments.getBoolean("support_area", false);
            if (this.o) {
                g();
            }
            this.p = arguments.getBoolean("support_batch", false);
            if (this.p) {
                c();
            }
            this.q = arguments.getBoolean("support_feed_ad", false);
            this.r = arguments.getBoolean("support_lazy_load", false);
            if (!this.r) {
                setUserVisibleHint(true);
            }
            this.s = arguments.getBoolean("support_pull_refresh", false);
            this.g.setEnabled(this.s);
            this.y = arguments.getString("userlist_tuid");
            if (this.y == null) {
                this.y = "";
            }
            String string = arguments.getString("adapter_type");
            if ("ring_list_adapter".equals(string)) {
                this.h = new k(getActivity());
                if (!ah.c(this.y)) {
                    ((k) this.h).a(this.y);
                }
                this.i.setOnItemClickListener(new f());
            } else if ("cailing_list_adapter".equals(string)) {
                this.h = new com.shoujiduoduo.ui.cailing.b(getActivity());
                this.i.setOnItemClickListener(new f());
            } else if ("system_ring_list_adapter".equals(string)) {
                this.h = new com.shoujiduoduo.ui.mine.changering.a(getActivity());
                this.i.setOnItemClickListener(new f());
            } else if ("collect_list_adapter".equals(string)) {
                this.h = new e(getActivity());
                this.i.setOnItemClickListener(new b());
            } else if ("artist_list_adapter".equals(string)) {
                this.h = new b(getActivity());
                this.i.setOnItemClickListener(new a());
            } else if ("user_list_adapter".equals(string)) {
                com.shoujiduoduo.base.a.a.a("DDListFragment", "user list adapter");
                this.h = new l(getActivity());
                if (!ah.c(this.y)) {
                    ((l) this.h).a(this.y);
                }
                this.i.setOnItemClickListener(new h());
            } else if ("comment_list_adapter".equals(string)) {
                this.h = new f(getActivity());
                this.i.setOnItemClickListener(new c());
            } else if ("concern_feeds_adapter".equals(string)) {
                this.h = new g(getActivity());
                this.i.setOnItemClickListener(new d());
            } else {
                com.shoujiduoduo.base.a.a.c("DDListFragment", "not support adapter type");
            }
        }
        if (this.h != null) {
            if (this.q) {
                this.h.a(true);
            }
            this.h.a();
        }
        this.w = r.a(com.umeng.b.a.a().a(RingDDApp.c(), "feed_ad_gap"), 10);
        this.x = r.a(com.umeng.b.a.a().a(RingDDApp.c(), "feed_ad_start_pos"), 6);
        this.u = true;
        this.v = false;
        if (this.n != null) {
            a(this.n);
        }
        a();
        com.shoujiduoduo.a.a.c.a().a(com.shoujiduoduo.a.a.b.OBSERVER_RING_CHANGE, this.D);
        com.shoujiduoduo.a.a.c.a().a(com.shoujiduoduo.a.a.b.OBSERVER_CAILING, this.G);
        com.shoujiduoduo.a.a.c.a().a(com.shoujiduoduo.a.a.b.OBSERVER_LIST_DATA, this.H);
        com.shoujiduoduo.a.a.c.a().a(com.shoujiduoduo.a.a.b.OBSERVER_VIP, this.C);
        com.shoujiduoduo.a.a.c.a().a(com.shoujiduoduo.a.a.b.OBSERVER_COMMENT, this.F);
        com.shoujiduoduo.a.a.c.a().a(com.shoujiduoduo.a.a.b.OBSERVER_USER_CENTER, this.E);
        return inflate;
    }

    public void d_() {
        if (!this.j) {
            com.shoujiduoduo.base.a.a.a("DDListFragment", "onRefresh, begin refresh data");
            this.j = true;
            this.b.refreshData();
            return;
        }
        com.shoujiduoduo.base.a.a.a("DDListFragment", "onRefresh, is refreshing, do nothing");
    }

    public void onDestroy() {
        com.shoujiduoduo.base.a.a.a("DDListFragment", "onDestroy, list id:" + (this.b == null ? "no id" : this.b.getListId()));
        super.onDestroy();
    }

    public void onDestroyView() {
        super.onDestroyView();
        com.shoujiduoduo.base.a.a.a("DDListFragment", "onDestroyView, id:" + (this.b == null ? "no id" : this.b.getListId()));
        if (this.h != null) {
            this.h.b();
        }
        this.u = false;
        com.shoujiduoduo.a.a.c.a().b(com.shoujiduoduo.a.a.b.OBSERVER_RING_CHANGE, this.D);
        com.shoujiduoduo.a.a.c.a().b(com.shoujiduoduo.a.a.b.OBSERVER_CAILING, this.G);
        com.shoujiduoduo.a.a.c.a().b(com.shoujiduoduo.a.a.b.OBSERVER_LIST_DATA, this.H);
        com.shoujiduoduo.a.a.c.a().b(com.shoujiduoduo.a.a.b.OBSERVER_VIP, this.C);
        com.shoujiduoduo.a.a.c.a().b(com.shoujiduoduo.a.a.b.OBSERVER_COMMENT, this.F);
        com.shoujiduoduo.a.a.c.a().b(com.shoujiduoduo.a.a.b.OBSERVER_USER_CENTER, this.E);
    }

    public void onDetach() {
        super.onDetach();
    }

    /* access modifiers changed from: protected */
    public void a() {
        if (this.u && this.f2052a && this.b != null && this.h != null && !this.v) {
            com.shoujiduoduo.base.a.a.a("DDListFragment", "lazyLoad, loadListData");
            f();
            this.v = true;
        }
    }

    public void a(View view) {
        if (!this.u) {
            this.n = view;
        } else if (this.n == null || this.i.getHeaderViewsCount() == 0) {
            this.n = view;
            this.i.addHeaderView(this.n);
        }
    }

    public class h implements AdapterView.OnItemClickListener {
        public h() {
        }

        public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
            com.shoujiduoduo.base.a.a.a("DDListFragment", "onItemClick, UserClickListener");
            if (DDListFragment.this.b != null && j >= 0) {
                Intent intent = new Intent(DDListFragment.this.getActivity(), UserMainPageActivity.class);
                intent.putExtra("tuid", ((UserData) DDListFragment.this.b.get(i)).uid);
                intent.putExtra("fansNum", ((UserData) DDListFragment.this.b.get(i)).followerNum);
                intent.putExtra("followNum", ((UserData) DDListFragment.this.b.get(i)).followingNum);
                DDListFragment.this.getActivity().startActivity(intent);
            }
        }
    }

    public class c implements AdapterView.OnItemClickListener {
        public c() {
        }

        public void onItemClick(AdapterView<?> adapterView, View view, final int i, long j) {
            com.shoujiduoduo.a.a.c.a().a(com.shoujiduoduo.a.a.b.OBSERVER_COMMENT, new c.a<com.shoujiduoduo.a.c.e>() {
                public void a() {
                    ((com.shoujiduoduo.a.c.e) this.f1284a).a((CommentData) DDListFragment.this.b.get(i));
                }
            });
        }
    }

    public class d implements AdapterView.OnItemClickListener {
        public d() {
        }

        public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
            MessageData messageData = (MessageData) DDListFragment.this.b.get(i);
            if (messageData.feedtype.equals("comment")) {
                Intent intent = new Intent(RingDDApp.c(), CommentActivity.class);
                intent.putExtra("tuid", messageData.ruid);
                intent.putExtra("rid", messageData.rid);
                RingData a2 = com.shoujiduoduo.a.b.b.b().a(messageData.rid);
                if (a2 != null) {
                    intent.putExtra(SelectCountryActivity.EXTRA_COUNTRY_NAME, a2.name);
                }
                intent.putExtra("from", "feeds_replay");
                intent.putExtra("current_comment", messageData);
                DDListFragment.this.getActivity().startActivity(intent);
            }
        }
    }

    public class a implements AdapterView.OnItemClickListener {
        public a() {
        }

        public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
            if (DDListFragment.this.b != null && j >= 0) {
                RingDDApp.b().a("artistdata", DDListFragment.this.b.get(i));
                Intent intent = new Intent(DDListFragment.this.getActivity(), ArtistRingActivity.class);
                intent.putExtra("parakey", "artistdata");
                DDListFragment.this.getActivity().startActivity(intent);
            }
        }
    }

    public class b implements AdapterView.OnItemClickListener {
        public b() {
        }

        public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
            if (DDListFragment.this.b != null && j >= 0) {
                RingDDApp.b().a("collectdata", DDListFragment.this.b.get(i));
                Intent intent = new Intent(DDListFragment.this.getActivity(), CollectRingActivity.class);
                intent.putExtra("parakey", "collectdata");
                DDListFragment.this.getActivity().startActivity(intent);
            }
        }
    }

    /* access modifiers changed from: private */
    public int a(int i2) {
        if (this.q && i2 + 1 >= this.x) {
            return (((i2 + 1) - this.x) / this.w) + 1;
        }
        return 0;
    }

    public class f implements AdapterView.OnItemClickListener {
        public f() {
        }

        /* JADX WARN: Type inference failed for: r1v0, types: [android.widget.Adapter] */
        /* JADX WARN: Type inference failed for: r1v2, types: [android.widget.Adapter] */
        /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
            jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
            	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
            	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
            	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
            	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
            	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
            	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
            */
        public void onItemClick(android.widget.AdapterView<?> r5, android.view.View r6, int r7, long r8) {
            /*
                r4 = this;
                com.shoujiduoduo.ui.utils.DDListFragment r0 = com.shoujiduoduo.ui.utils.DDListFragment.this
                com.shoujiduoduo.base.bean.DDList r0 = r0.b
                if (r0 == 0) goto L_0x000e
                r0 = 0
                int r0 = (r8 > r0 ? 1 : (r8 == r0 ? 0 : -1))
                if (r0 >= 0) goto L_0x000f
            L_0x000e:
                return
            L_0x000f:
                int r0 = (int) r8
                android.widget.Adapter r1 = r5.getAdapter()
                int r1 = r1.getItemViewType(r7)
                r2 = 1
                if (r1 == r2) goto L_0x000e
                android.widget.Adapter r1 = r5.getAdapter()
                int r1 = r1.getItemViewType(r7)
                if (r1 != 0) goto L_0x0040
                com.shoujiduoduo.util.aa r1 = com.shoujiduoduo.util.aa.a()
                com.shoujiduoduo.player.PlayerService r1 = r1.b()
                if (r1 == 0) goto L_0x000e
                com.shoujiduoduo.ui.utils.DDListFragment r2 = com.shoujiduoduo.ui.utils.DDListFragment.this
                com.shoujiduoduo.base.bean.DDList r2 = r2.b
                com.shoujiduoduo.ui.utils.DDListFragment r3 = com.shoujiduoduo.ui.utils.DDListFragment.this
                int r3 = r3.a(r0)
                int r0 = r0 - r3
                r1.a(r2, r0)
                goto L_0x000e
            L_0x0040:
                com.shoujiduoduo.util.aa r1 = com.shoujiduoduo.util.aa.a()
                com.shoujiduoduo.player.PlayerService r1 = r1.b()
                if (r1 == 0) goto L_0x000e
                com.shoujiduoduo.ui.utils.DDListFragment r2 = com.shoujiduoduo.ui.utils.DDListFragment.this
                com.shoujiduoduo.base.bean.DDList r2 = r2.b
                r1.a(r2, r0)
                goto L_0x000e
            */
            throw new UnsupportedOperationException("Method not decompiled: com.shoujiduoduo.ui.utils.DDListFragment.f.onItemClick(android.widget.AdapterView, android.view.View, int, long):void");
        }
    }

    public String b() {
        if (this.b != null) {
            return this.b.getListId();
        }
        return "";
    }

    public void a(DDList dDList) {
        if (dDList != this.b) {
            this.b = null;
            this.b = dDList;
            if (this.u) {
                this.v = false;
                a();
            }
        }
    }

    /* access modifiers changed from: private */
    public void f() {
        this.i.setAdapter((ListAdapter) this.h);
        if (this.b != null) {
            this.h.a(this.b);
            if (this.b.size() == 0) {
                com.shoujiduoduo.base.a.a.a("DDListFragment", "loadListData: show loading panel, id:" + this.b.getListId());
                a(g.LIST_LOADING);
                if (!this.b.isRetrieving()) {
                    this.b.retrieveData();
                    return;
                }
                return;
            }
            com.shoujiduoduo.base.a.a.a("DDListFragment", "setRingList: Show list content, id:" + this.b.getListId());
            a(g.LIST_CONTENT);
            return;
        }
        this.h.a((DDList) null);
        this.h.notifyDataSetChanged();
    }

    private void g() {
        this.l = (Button) this.c.findViewById(R.id.changeArea);
        this.l.setVisibility(0);
        this.l.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                new com.shoujiduoduo.ui.home.a(DDListFragment.this.getActivity(), R.style.DuoDuoDialog, DDListFragment.this.b.getListId()).show();
            }
        });
    }

    public void c() {
        this.k = (Button) this.c.findViewById(R.id.changeBatch);
        this.k.setVisibility(0);
        this.k.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                com.umeng.a.b.b(RingDDApp.c(), "HOT_LIST_CHANGE_BATCH");
                com.shoujiduoduo.a.a.c.a().a(com.shoujiduoduo.a.a.b.OBSERVER_CHANGE_BATCH, new c.a<m>() {
                    public void a() {
                        ((m) this.f1284a).a(DDListFragment.this.b.getListType(), DDListFragment.this.b.getListId());
                    }
                });
            }
        });
    }

    /* access modifiers changed from: private */
    public boolean h() {
        if (this.b == null) {
            return false;
        }
        if (this.b.getListType().equals(ListType.LIST_TYPE.list_ring_cmcc) || this.b.getListType().equals(ListType.LIST_TYPE.list_ring_cucc) || this.b.getListType().equals(ListType.LIST_TYPE.list_ring_ctcc)) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: private */
    public void a(e eVar) {
        String str;
        if (this.m != null) {
            ImageView imageView = (ImageView) this.m.findViewById(R.id.more_data_loading);
            TextView textView = (TextView) this.m.findViewById(R.id.get_more_text);
            switch (eVar) {
                case RETRIEVE:
                    imageView.setVisibility(0);
                    ((AnimationDrawable) imageView.getBackground()).start();
                    textView.setText((int) R.string.ringlist_retrieving);
                    this.m.setVisibility(0);
                    return;
                case TOTAL:
                    imageView.setVisibility(8);
                    if (this.i.getCount() > (this.i.getHeaderViewsCount() > 0 ? 2 : 1)) {
                        String string = RingDDApp.c().getResources().getString(R.string.total);
                        if (this.b.getListType().equals(ListType.LIST_TYPE.list_artist)) {
                            str = "个歌手";
                        } else if (this.b.getListType().equals(ListType.LIST_TYPE.list_collect)) {
                            str = "个精选集";
                        } else if (this.b.getListType().equals(ListType.LIST_TYPE.list_user)) {
                            str = "个用户";
                        } else if (this.b.getListType().equals(ListType.LIST_TYPE.list_comment)) {
                            str = "个评论";
                        } else {
                            str = "首铃声";
                        }
                        int count = this.i.getCount();
                        if (this.i.getHeaderViewsCount() > 0) {
                            count -= this.i.getHeaderViewsCount();
                        }
                        textView.setText(string + (count - 1) + str);
                    }
                    if (this.b.getListType().equals(ListType.LIST_TYPE.list_comment)) {
                        this.m.setVisibility(8);
                        return;
                    } else {
                        this.m.setVisibility(0);
                        return;
                    }
                case RETRIEVE_FAILED:
                    imageView.setVisibility(8);
                    textView.setText((int) R.string.ringlist_retrieve_error);
                    this.m.setVisibility(0);
                    return;
                case INVISIBLE:
                    this.m.setVisibility(8);
                    return;
                default:
                    return;
            }
        }
    }

    public void a(g gVar) {
        this.c.setVisibility(4);
        this.d.setVisibility(4);
        this.f.setVisibility(4);
        switch (gVar) {
            case LIST_CONTENT:
                this.i.post(new Runnable() {
                    public void run() {
                        DDListFragment.this.c.setVisibility(0);
                    }
                });
                break;
            case LIST_LOADING:
                this.f.setVisibility(0);
                break;
            case LIST_FAILED:
                if (!this.z) {
                    this.d.setVisibility(0);
                    break;
                } else {
                    this.e.setVisibility(0);
                    TextView textView = (TextView) this.e.findViewById(R.id.hint);
                    TextView textView2 = (TextView) this.e.findViewById(R.id.open_tips);
                    TextView textView3 = (TextView) this.e.findViewById(R.id.cost_hint);
                    if (!this.A) {
                        textView.setText("尊敬的移动用户,\n您目前尚未开通彩铃功能,无法使用彩铃。");
                        textView2.setText("立即开通");
                        textView3.setVisibility(0);
                        break;
                    } else {
                        textView.setText("正在为您开通彩铃业务，\n请稍候点击“查询彩铃”获取当前彩铃");
                        textView2.setText("查询彩铃");
                        textView3.setVisibility(4);
                        break;
                    }
                }
        }
        this.t = gVar;
    }
}
