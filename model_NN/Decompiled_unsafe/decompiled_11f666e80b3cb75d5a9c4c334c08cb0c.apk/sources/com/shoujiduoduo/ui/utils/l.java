package com.shoujiduoduo.ui.utils;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.a.a.e;
import com.a.a.p;
import com.d.a.b.d;
import com.shoujiduoduo.a.b.b;
import com.shoujiduoduo.b.c.n;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.base.bean.DDList;
import com.shoujiduoduo.base.bean.HttpJsonRes;
import com.shoujiduoduo.base.bean.UserData;
import com.shoujiduoduo.base.bean.UserInfo;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.util.ah;
import com.shoujiduoduo.util.t;

/* compiled from: UserListAdapter */
public class l extends d {

    /* renamed from: a  reason: collision with root package name */
    private n f2136a;
    private Context b;
    private String c;
    private boolean d;
    /* access modifiers changed from: private */
    public boolean e;
    private n.a f;

    public l(Context context) {
        this.b = context;
    }

    public void a(String str) {
        this.c = str;
        this.d = this.c != null && this.c.equals(b.g().f());
    }

    public void a(boolean z) {
    }

    public void a(DDList dDList) {
        this.f2136a = (n) dDList;
        this.f = this.f2136a.a();
    }

    public void a() {
    }

    public void b() {
    }

    public int getCount() {
        if (this.f2136a != null) {
            return this.f2136a.size();
        }
        a.a("UserListAdapter", "count:0");
        return 0;
    }

    public Object getItem(int i) {
        if (this.f2136a != null) {
            return this.f2136a.get(i);
        }
        return null;
    }

    public long getItemId(int i) {
        return (long) i;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = LayoutInflater.from(this.b).inflate((int) R.layout.listitem_fans_follow, viewGroup, false);
        }
        TextView textView = (TextView) m.a(view, R.id.user_des);
        Button button = (Button) m.a(view, R.id.btn_follow);
        UserData userData = (UserData) this.f2136a.get(i);
        d.a().a(userData.headUrl, (ImageView) m.a(view, R.id.user_head), h.a().e());
        ((TextView) m.a(view, R.id.user_name)).setText(userData.userName);
        textView.setText(userData.intro);
        if (ah.c(userData.intro)) {
            textView.setVisibility(8);
        }
        final String str = userData.uid;
        if (this.f == n.a.fans) {
            if (b.g().i().contains(userData.uid)) {
                button.setText("取消关注");
            } else {
                button.setText("关注");
            }
        } else if (this.d) {
            button.setText("取消关注");
        } else if (b.g().i().contains(userData.uid)) {
            button.setText("取消关注");
        } else {
            button.setText("关注");
        }
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(final View view) {
                String charSequence = ((Button) view).getText().toString();
                UserInfo c = b.g().c();
                if (!l.this.e) {
                    boolean unused = l.this.e = true;
                    if ("关注".equals(charSequence)) {
                        t.a("follow", "&uid=" + c.getUid() + "&tuid=" + str, new t.a() {
                            public void a(String str) {
                                try {
                                    HttpJsonRes httpJsonRes = (HttpJsonRes) new e().a(str, HttpJsonRes.class);
                                    if (httpJsonRes.getResult().equals("success")) {
                                        ((Button) view).setText("取消关注");
                                        com.shoujiduoduo.util.widget.d.a("关注成功");
                                        b.g().b(str);
                                    } else {
                                        com.shoujiduoduo.util.widget.d.a(httpJsonRes.getMsg());
                                    }
                                } catch (p e) {
                                    e.printStackTrace();
                                }
                                boolean unused = l.this.e = false;
                            }

                            public void a(String str, String str2) {
                                com.shoujiduoduo.util.widget.d.a("关注失败");
                                boolean unused = l.this.e = false;
                            }
                        });
                    } else {
                        t.a("unfollow", "&uid=" + c.getUid() + "&tuid=" + str, new t.a() {
                            public void a(String str) {
                                ((Button) view).setText("关注");
                                com.shoujiduoduo.util.widget.d.a("取消关注成功");
                                boolean unused = l.this.e = false;
                                b.g().a(str);
                            }

                            public void a(String str, String str2) {
                                com.shoujiduoduo.util.widget.d.a("取消失败");
                                boolean unused = l.this.e = false;
                            }
                        });
                    }
                }
            }
        });
        return view;
    }
}
