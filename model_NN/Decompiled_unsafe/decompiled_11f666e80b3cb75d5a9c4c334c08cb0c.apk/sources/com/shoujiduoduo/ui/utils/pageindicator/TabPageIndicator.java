package com.shoujiduoduo.ui.utils.pageindicator;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.shoujiduoduo.ringtone.R;
import org.apache.mina.proxy.handlers.http.ntlm.NTLMConstants;

public class TabPageIndicator extends HorizontalScrollView implements PageIndicator {

    /* renamed from: a  reason: collision with root package name */
    private static final CharSequence f2146a = "";
    /* access modifiers changed from: private */
    public Runnable b;
    private final View.OnClickListener c = new View.OnClickListener() {
        public void onClick(View view) {
            int currentItem = TabPageIndicator.this.e.getCurrentItem();
            int a2 = ((b) view).a();
            TabPageIndicator.this.e.setCurrentItem(a2);
            if (currentItem == a2 && TabPageIndicator.this.i != null) {
                TabPageIndicator.this.i.a(a2);
            }
        }
    };
    private final b d;
    /* access modifiers changed from: private */
    public ViewPager e;
    private ViewPager.OnPageChangeListener f;
    /* access modifiers changed from: private */
    public int g;
    private int h;
    /* access modifiers changed from: private */
    public a i;

    public interface a {
        void a(int i);
    }

    public TabPageIndicator(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        setHorizontalScrollBarEnabled(false);
        this.d = new b(context, R.attr.vpiTabPageIndicatorStyle);
        addView(this.d, new ViewGroup.LayoutParams(-2, -1));
    }

    public void setOnTabReselectedListener(a aVar) {
        this.i = aVar;
    }

    public void onMeasure(int i2, int i3) {
        int mode = View.MeasureSpec.getMode(i2);
        boolean z = mode == 1073741824;
        setFillViewport(z);
        int childCount = this.d.getChildCount();
        if (childCount <= 1 || !(mode == 1073741824 || mode == Integer.MIN_VALUE)) {
            this.g = -1;
        } else if (childCount > 2) {
            this.g = (int) (((float) View.MeasureSpec.getSize(i2)) * 0.4f);
        } else {
            this.g = View.MeasureSpec.getSize(i2) / 2;
        }
        int measuredWidth = getMeasuredWidth();
        super.onMeasure(i2, i3);
        int measuredWidth2 = getMeasuredWidth();
        if (z && measuredWidth != measuredWidth2) {
            setCurrentItem(this.h);
        }
    }

    private void a(int i2) {
        final View childAt = this.d.getChildAt(i2);
        if (this.b != null) {
            removeCallbacks(this.b);
        }
        this.b = new Runnable() {
            public void run() {
                TabPageIndicator.this.smoothScrollTo(childAt.getLeft() - ((TabPageIndicator.this.getWidth() - childAt.getWidth()) / 2), 0);
                Runnable unused = TabPageIndicator.this.b = null;
            }
        };
        post(this.b);
    }

    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (this.b != null) {
            post(this.b);
        }
    }

    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (this.b != null) {
            removeCallbacks(this.b);
        }
    }

    private void a(int i2, CharSequence charSequence, int i3) {
        b bVar = new b(getContext());
        int unused = bVar.b = i2;
        bVar.setFocusable(true);
        bVar.setOnClickListener(this.c);
        bVar.setText(charSequence);
        if (i3 != 0) {
            bVar.setCompoundDrawablesWithIntrinsicBounds(i3, 0, 0, 0);
        }
        this.d.addView(bVar, new LinearLayout.LayoutParams(-2, -1, 1.0f));
    }

    public void onPageScrollStateChanged(int i2) {
        if (this.f != null) {
            this.f.onPageScrollStateChanged(i2);
        }
    }

    public void onPageScrolled(int i2, float f2, int i3) {
        if (this.f != null) {
            this.f.onPageScrolled(i2, f2, i3);
        }
    }

    public void onPageSelected(int i2) {
        setCurrentItem(i2);
        if (this.f != null) {
            this.f.onPageSelected(i2);
        }
    }

    public void setViewPager(ViewPager viewPager) {
        if (this.e != viewPager) {
            if (this.e != null) {
                this.e.setOnPageChangeListener(null);
            }
            if (viewPager.getAdapter() == null) {
                throw new IllegalStateException("ViewPager does not have adapter instance.");
            }
            this.e = viewPager;
            viewPager.setOnPageChangeListener(this);
            a();
        }
    }

    public void a() {
        CharSequence charSequence;
        int i2;
        this.d.removeAllViews();
        PagerAdapter adapter = this.e.getAdapter();
        a aVar = null;
        if (adapter instanceof a) {
            aVar = (a) adapter;
        }
        int count = adapter.getCount();
        for (int i3 = 0; i3 < count; i3++) {
            CharSequence pageTitle = adapter.getPageTitle(i3);
            if (pageTitle == null) {
                charSequence = f2146a;
            } else {
                charSequence = pageTitle;
            }
            if (aVar != null) {
                i2 = aVar.a(i3);
            } else {
                i2 = 0;
            }
            a(i3, charSequence, i2);
        }
        if (this.h > count) {
            this.h = count - 1;
        }
        setCurrentItem(this.h);
        requestLayout();
    }

    public void setCurrentItem(int i2) {
        boolean z;
        if (this.e == null) {
            throw new IllegalStateException("ViewPager has not been bound.");
        }
        this.h = i2;
        this.e.setCurrentItem(i2);
        int childCount = this.d.getChildCount();
        for (int i3 = 0; i3 < childCount; i3++) {
            View childAt = this.d.getChildAt(i3);
            if (i3 == i2) {
                z = true;
            } else {
                z = false;
            }
            childAt.setSelected(z);
            if (z) {
                a(i2);
            }
        }
    }

    public void setOnPageChangeListener(ViewPager.OnPageChangeListener onPageChangeListener) {
        this.f = onPageChangeListener;
    }

    private class b extends TextView {
        /* access modifiers changed from: private */
        public int b;

        public b(Context context) {
            super(context, null, R.attr.vpiTabPageIndicatorStyle);
        }

        public void onMeasure(int i, int i2) {
            super.onMeasure(i, i2);
            if (TabPageIndicator.this.g > 0 && getMeasuredWidth() > TabPageIndicator.this.g) {
                super.onMeasure(View.MeasureSpec.makeMeasureSpec(TabPageIndicator.this.g, NTLMConstants.FLAG_NEGOTIATE_KEY_EXCHANGE), i2);
            }
        }

        public int a() {
            return this.b;
        }
    }
}
