package com.shoujiduoduo.ui.utils;

import android.util.SparseArray;
import android.view.View;

/* compiled from: ViewHolder */
public class m {
    public static <T extends View> T a(View view, int i) {
        SparseArray sparseArray;
        SparseArray sparseArray2 = (SparseArray) view.getTag();
        if (sparseArray2 == null) {
            SparseArray sparseArray3 = new SparseArray();
            view.setTag(sparseArray3);
            sparseArray = sparseArray3;
        } else {
            sparseArray = sparseArray2;
        }
        T t = (View) sparseArray.get(i);
        if (t != null) {
            return t;
        }
        T findViewById = view.findViewById(i);
        sparseArray.put(i, findViewById);
        return findViewById;
    }
}
