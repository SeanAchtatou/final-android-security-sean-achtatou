package com.shoujiduoduo.ui.utils;

import android.os.Bundle;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import com.shoujiduoduo.base.a.a;

public abstract class SlidingActivity extends BaseFragmentActivity {

    /* renamed from: a  reason: collision with root package name */
    private int f2056a;
    private float b;
    private float c;
    private float d;
    private float e;
    private VelocityTracker f;

    public abstract void a();

    public abstract void b();

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.f2056a = (int) (getResources().getDisplayMetrics().density * 400.0f);
    }

    public boolean dispatchTouchEvent(MotionEvent motionEvent) {
        a(motionEvent);
        switch (motionEvent.getAction()) {
            case 0:
                this.b = motionEvent.getRawX();
                this.c = motionEvent.getRawY();
                break;
            case 1:
                this.d = motionEvent.getRawX();
                this.e = motionEvent.getRawY();
                int i = (int) (this.d - this.b);
                int i2 = (int) (this.e - this.c);
                int d2 = d();
                a.a("slide", "distanceX:" + i + ", distanceY:" + i2);
                a.a("slide", "speedX:" + d2 + ", minSpeed:" + this.f2056a);
                if (Math.abs(i) > Math.abs(i2) && d2 > this.f2056a) {
                    if (i > 0) {
                        b();
                    } else {
                        a();
                    }
                }
                c();
                break;
        }
        return super.dispatchTouchEvent(motionEvent);
    }

    private void a(MotionEvent motionEvent) {
        if (this.f == null) {
            this.f = VelocityTracker.obtain();
        }
        this.f.addMovement(motionEvent);
    }

    private void c() {
        this.f.recycle();
        this.f = null;
    }

    private int d() {
        this.f.computeCurrentVelocity(1000);
        return Math.abs((int) this.f.getXVelocity());
    }
}
