package com.shoujiduoduo.ui.utils;

import android.content.Context;
import android.content.Intent;
import android.text.SpannableString;
import android.text.TextPaint;
import android.text.style.ClickableSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.d.a.b.d;
import com.shoujiduoduo.a.a.b;
import com.shoujiduoduo.a.a.c;
import com.shoujiduoduo.a.c.o;
import com.shoujiduoduo.b.c.h;
import com.shoujiduoduo.base.bean.DDList;
import com.shoujiduoduo.base.bean.MessageData;
import com.shoujiduoduo.base.bean.RingData;
import com.shoujiduoduo.base.bean.UserInfo;
import com.shoujiduoduo.player.PlayerService;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.ui.mine.UserMainPageActivity;
import com.shoujiduoduo.ui.user.CommentActivity;
import com.shoujiduoduo.ui.utils.TextViewFixTouchConsume;
import com.shoujiduoduo.util.aa;
import com.shoujiduoduo.util.ah;
import com.shoujiduoduo.util.r;
import com.sina.weibo.sdk.register.mobile.SelectCountryActivity;
import java.text.ParseException;
import java.text.SimpleDateFormat;

/* compiled from: ConcernFeedsAdapter */
public class g extends d {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public Context f2076a;
    /* access modifiers changed from: private */
    public h b;
    /* access modifiers changed from: private */
    public boolean c;
    /* access modifiers changed from: private */
    public int d = -1;
    private o e = new o() {
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.shoujiduoduo.ui.utils.g.a(com.shoujiduoduo.ui.utils.g, boolean):boolean
         arg types: [com.shoujiduoduo.ui.utils.g, int]
         candidates:
          com.shoujiduoduo.ui.utils.g.a(com.shoujiduoduo.ui.utils.g, int):int
          com.shoujiduoduo.ui.utils.g.a(com.shoujiduoduo.ui.utils.g, boolean):boolean */
        public void a(String str, int i) {
            if (g.this.b != null && g.this.b.getListId().equals(str)) {
                com.shoujiduoduo.base.a.a.a("ConcernFeedsAdapter", "onSetPlay, listid:" + str);
                boolean unused = g.this.c = true;
                g.this.notifyDataSetChanged();
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.shoujiduoduo.ui.utils.g.a(com.shoujiduoduo.ui.utils.g, boolean):boolean
         arg types: [com.shoujiduoduo.ui.utils.g, int]
         candidates:
          com.shoujiduoduo.ui.utils.g.a(com.shoujiduoduo.ui.utils.g, int):int
          com.shoujiduoduo.ui.utils.g.a(com.shoujiduoduo.ui.utils.g, boolean):boolean */
        public void b(String str, int i) {
            if (g.this.b != null && g.this.b.getListId().equals(str)) {
                com.shoujiduoduo.base.a.a.a("ConcernFeedsAdapter", "onCanclePlay, listId:" + str);
                boolean unused = g.this.c = false;
                g.this.notifyDataSetChanged();
            }
        }

        public void a(String str, int i, int i2) {
            if (g.this.b != null && g.this.b.getListId().equals(str)) {
                g.this.notifyDataSetChanged();
            }
        }
    };

    public g(Context context) {
        this.f2076a = context;
    }

    public void a(boolean z) {
    }

    public void a(DDList dDList) {
        this.b = (h) dDList;
    }

    public void a() {
        c.a().a(b.OBSERVER_PLAY_STATUS, this.e);
    }

    public void b() {
        c.a().b(b.OBSERVER_PLAY_STATUS, this.e);
    }

    public int getCount() {
        if (this.b != null) {
            return this.b.size();
        }
        return 0;
    }

    public Object getItem(int i) {
        if (this.b != null) {
            return this.b.get(i);
        }
        return null;
    }

    public long getItemId(int i) {
        return (long) i;
    }

    /* compiled from: ConcernFeedsAdapter */
    private class a implements View.OnClickListener {
        private int b;

        public a(int i) {
            this.b = i;
        }

        public void onClick(View view) {
            PlayerService b2 = aa.a().b();
            if (b2 == null) {
                return;
            }
            if (g.this.c && g.this.d == this.b && b2.a() == 3) {
                b2.n();
                return;
            }
            int unused = g.this.d = this.b;
            RingData ringData = new RingData();
            MessageData messageData = (MessageData) g.this.b.get(this.b);
            ringData.rid = messageData.rid;
            ringData.setHighAACBitrate(r.a(messageData.hbr, 0));
            ringData.setHighAACURL(messageData.hurl);
            b2.a(ringData, g.this.b.getListId());
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View getView(final int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = LayoutInflater.from(this.f2076a).inflate((int) R.layout.listitem_feeds_comment, viewGroup, false);
        }
        ImageView imageView = (ImageView) m.a(view, R.id.user_head);
        TextView textView = (TextView) m.a(view, R.id.user_name);
        TextView textView2 = (TextView) m.a(view, R.id.tv_desc);
        TextView textView3 = (TextView) m.a(view, R.id.create_time);
        TextView textView4 = (TextView) m.a(view, R.id.tv_songname);
        TextViewFixTouchConsume textViewFixTouchConsume = (TextViewFixTouchConsume) m.a(view, R.id.comment);
        TextViewFixTouchConsume textViewFixTouchConsume2 = (TextViewFixTouchConsume) m.a(view, R.id.tcomment);
        RelativeLayout relativeLayout = (RelativeLayout) m.a(view, R.id.ring_laytout);
        ImageButton imageButton = (ImageButton) m.a(view, R.id.btn_enter_ring);
        final MessageData messageData = (MessageData) this.b.get(i);
        if (!ah.c(messageData.head_url)) {
            d.a().a(messageData.head_url, imageView, h.a().e());
        } else {
            imageView.setImageResource(R.drawable.icon_fans_def_head);
        }
        imageButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent intent = new Intent(RingDDApp.c(), CommentActivity.class);
                if (messageData.feedtype.equals("comment")) {
                    intent.putExtra("tuid", messageData.ruid);
                    intent.putExtra("rid", messageData.rid);
                    RingData a2 = com.shoujiduoduo.a.b.b.b().a(messageData.rid);
                    if (a2 != null) {
                        intent.putExtra(SelectCountryActivity.EXTRA_COUNTRY_NAME, a2.name);
                    }
                    intent.putExtra("from", "feeds_replay");
                    intent.putExtra("current_comment", messageData);
                } else {
                    intent.putExtra("tuid", messageData.uid);
                    intent.putExtra("rid", messageData.rid);
                    intent.putExtra(SelectCountryActivity.EXTRA_COUNTRY_NAME, messageData.name);
                    intent.putExtra("from", "feeds_replay");
                    intent.putExtra("current_comment", messageData);
                }
                g.this.f2076a.startActivity(intent);
            }
        });
        imageView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent intent = new Intent(RingDDApp.c(), UserMainPageActivity.class);
                intent.putExtra("tuid", messageData.uid);
                g.this.f2076a.startActivity(intent);
            }
        });
        textView2.setText(messageData.desc);
        if (!ah.c(messageData.date)) {
            try {
                textView3.setText(com.shoujiduoduo.util.g.a(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(messageData.date)));
            } catch (ParseException e2) {
                e2.printStackTrace();
                textView3.setText("");
            } catch (Exception e3) {
                textView3.setText("");
            }
        } else {
            textView3.setText("");
        }
        if (messageData.feedtype.equals("ring")) {
            textView4.setText(messageData.name);
            textView.setText(messageData.artist);
            relativeLayout.setVisibility(0);
        } else {
            RingData a2 = com.shoujiduoduo.a.b.b.b().a(messageData.rid);
            if (a2 != null) {
                textView4.setText(a2.name);
                relativeLayout.setVisibility(0);
            } else {
                relativeLayout.setVisibility(8);
            }
        }
        relativeLayout.setOnClickListener(new a(i));
        ProgressBar progressBar = (ProgressBar) view.findViewById(R.id.ringitem_download_progress);
        progressBar.setVisibility(4);
        ImageButton imageButton2 = (ImageButton) m.a(view, R.id.ringitem_play);
        ImageButton imageButton3 = (ImageButton) m.a(view, R.id.ringitem_pause);
        imageButton2.setOnClickListener(new a(i));
        imageButton3.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                PlayerService b = aa.a().b();
                if (b != null) {
                    b.j();
                }
            }
        });
        ((ImageButton) m.a(view, R.id.ringitem_failed)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                PlayerService b2 = aa.a().b();
                int unused = g.this.d = i;
                if (b2 != null) {
                    RingData ringData = new RingData();
                    ringData.rid = messageData.rid;
                    ringData.setHighAACBitrate(r.a(messageData.hbr, 0));
                    ringData.setHighAACURL(messageData.hurl);
                    b2.a(ringData, g.this.b.getListId());
                }
            }
        });
        if (this.c && this.d == i) {
            int i2 = 5;
            PlayerService b2 = aa.a().b();
            if (b2 != null) {
                i2 = b2.a();
            }
            switch (i2) {
                case 1:
                    progressBar.setVisibility(0);
                    imageButton2.setVisibility(4);
                    break;
                case 2:
                    imageButton3.setVisibility(0);
                    imageButton2.setVisibility(4);
                    break;
                case 3:
                case 4:
                case 5:
                case 6:
                    imageButton2.setVisibility(0);
                    imageButton3.setVisibility(4);
                    break;
            }
        } else {
            imageButton2.setVisibility(0);
            imageButton3.setVisibility(4);
        }
        if (messageData.feedtype.equals("comment")) {
            textView.setText(messageData.name);
            if (!ah.c(messageData.tcomment)) {
                com.shoujiduoduo.base.a.a.a("ConcernFeedsAdapter", "tcid:" + messageData.tcid + ", 别人回复你的评论");
                textViewFixTouchConsume.setMovementMethod(TextViewFixTouchConsume.a.a());
                textViewFixTouchConsume.setFocusable(false);
                textViewFixTouchConsume2.setMovementMethod(TextViewFixTouchConsume.a.a());
                textViewFixTouchConsume2.setFocusable(false);
                final UserInfo c2 = com.shoujiduoduo.a.b.b.g().c();
                SpannableString spannableString = new SpannableString("回复@" + c2.getUserName() + ":" + messageData.comment);
                SpannableString spannableString2 = new SpannableString("@" + c2.getUserName() + ":" + messageData.tcomment);
                final int color = this.f2076a.getResources().getColor(R.color.text_blue);
                AnonymousClass6 r5 = new ClickableSpan() {
                    public void updateDrawState(TextPaint textPaint) {
                        super.updateDrawState(textPaint);
                        textPaint.setColor(color);
                        textPaint.setUnderlineText(false);
                    }

                    public void onClick(View view) {
                        Intent intent = new Intent(RingDDApp.c(), UserMainPageActivity.class);
                        intent.putExtra("tuid", c2.getUid());
                        g.this.f2076a.startActivity(intent);
                    }
                };
                spannableString.setSpan(r5, 2, ("@" + c2.getUserName()).length() + 2, 17);
                spannableString2.setSpan(r5, 0, ("@" + c2.getUserName()).length(), 17);
                textViewFixTouchConsume2.setText(spannableString2);
                textViewFixTouchConsume2.setVisibility(0);
                textViewFixTouchConsume.setText(spannableString);
                textViewFixTouchConsume.setVisibility(0);
            } else {
                com.shoujiduoduo.base.a.a.a("ConcernFeedsAdapter", "别人评论你的铃声");
                textViewFixTouchConsume.setText(messageData.comment);
                textViewFixTouchConsume.setVisibility(0);
                textViewFixTouchConsume2.setVisibility(8);
            }
        } else {
            textViewFixTouchConsume2.setVisibility(8);
            textViewFixTouchConsume.setVisibility(8);
        }
        return view;
    }
}
