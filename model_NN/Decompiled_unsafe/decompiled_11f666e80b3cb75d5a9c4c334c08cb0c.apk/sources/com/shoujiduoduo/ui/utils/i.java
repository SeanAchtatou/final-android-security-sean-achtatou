package com.shoujiduoduo.ui.utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;

/* compiled from: ProgressWaitDlg */
public class i {

    /* renamed from: a  reason: collision with root package name */
    private static ProgressDialog f2085a = null;
    private static ProgressDialog b = null;

    public static void a(Context context) {
        a(context, "请稍候...");
    }

    public static void a(Context context, String str) {
        if (f2085a == null) {
            f2085a = new ProgressDialog(context);
            f2085a.setMessage(str);
            f2085a.setIndeterminate(false);
            f2085a.setCancelable(true);
            f2085a.setCanceledOnTouchOutside(false);
            f2085a.show();
        }
    }

    public static void a() {
        if (f2085a != null) {
            f2085a.dismiss();
            f2085a = null;
        }
    }

    public static void b(Context context) {
        if (b == null) {
            b = new ProgressDialog(context);
            b.setMessage("中国移动提示您：首次使用彩铃功能需要发送一条免费短信，该过程自动完成，如弹出警告，请选择允许。");
            b.setIndeterminate(false);
            b.setCancelable(true);
            b.setCanceledOnTouchOutside(false);
            b.setButton(-1, "确定", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });
            b.show();
        }
    }

    public static void b() {
        if (b != null) {
            b.dismiss();
            b = null;
        }
    }
}
