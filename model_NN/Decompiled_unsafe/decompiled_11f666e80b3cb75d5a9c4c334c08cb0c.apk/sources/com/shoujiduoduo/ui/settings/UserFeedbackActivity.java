package com.shoujiduoduo.ui.settings;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ui.utils.SlidingActivity;
import com.shoujiduoduo.util.ah;
import com.shoujiduoduo.util.g;
import com.shoujiduoduo.util.widget.d;

public class UserFeedbackActivity extends SlidingActivity implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private EditText f1919a;
    private EditText b;
    private String c = "";
    private String d = "";

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.user_feedback_activity);
        this.f1919a = (EditText) findViewById(R.id.user_feedback_edit);
        this.b = (EditText) findViewById(R.id.contact_info_edit);
        findViewById(R.id.btn_submit_advice).setOnClickListener(this);
        findViewById(R.id.back).setOnClickListener(this);
    }

    public void a() {
    }

    public void b() {
        finish();
    }

    public void onClick(View view) {
        if (view.getId() == R.id.back) {
            finish();
        } else if (view.getId() == R.id.btn_submit_advice) {
            String obj = this.f1919a.getText().toString();
            String obj2 = this.b.getText().toString();
            if (!ah.c(obj)) {
                if (!this.c.equals(obj) || !this.d.equals(obj2)) {
                    this.d = obj2;
                    this.c = obj;
                    g.a(obj, obj2);
                    a.a("UserFeedbackActivity", "提交至服务器");
                } else {
                    a.a("UserFeedbackActivity", "反馈内容相同，不用重复提交");
                }
                d.a("反馈提交成功，多谢！");
                return;
            }
            d.a("请输入问题描述");
        }
    }
}
