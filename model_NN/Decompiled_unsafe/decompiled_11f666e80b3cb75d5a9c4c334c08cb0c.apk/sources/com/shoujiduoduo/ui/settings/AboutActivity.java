package com.shoujiduoduo.ui.settings;

import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import com.jaeger.library.a;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ui.utils.SlidingActivity;
import com.shoujiduoduo.util.w;

public class AboutActivity extends SlidingActivity {
    public void onCreate(Bundle bundle) {
        String str;
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_about);
        a.a(this, getResources().getColor(R.color.bkg_green), 0);
        String str2 = "";
        try {
            str2 = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
            str = str2.substring(0, str2.lastIndexOf(46));
        } catch (PackageManager.NameNotFoundException e) {
            PackageManager.NameNotFoundException nameNotFoundException = e;
            nameNotFoundException.printStackTrace();
            str = str2;
        }
        TextView textView = (TextView) findViewById(R.id.about_activity_app_name);
        if (textView != null) {
            textView.setText(((Object) getResources().getText(R.string.app_name)) + " " + str);
        }
        findViewById(R.id.migu_install).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                w.a("cmccwm.mobilemusic");
            }
        });
        ((TextView) findViewById(R.id.about_activity_release_date)).setText("2017-06-16");
        ((TextView) findViewById(R.id.about_activity_app_intro)).setText(getResources().getString(R.string.app_intro1) + "\n" + getResources().getString(R.string.app_intro2) + "\n" + getResources().getString(R.string.app_intro3) + "\n" + getResources().getString(R.string.app_intro4) + "\n" + getResources().getString(R.string.app_intro5));
        findViewById(R.id.about_activity_back).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                AboutActivity.this.finish();
            }
        });
    }

    public void a() {
    }

    public void b() {
        finish();
    }
}
