package com.shoujiduoduo.ui.settings;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.shoujiduoduo.a.a.c;
import com.shoujiduoduo.a.c.v;
import com.shoujiduoduo.base.bean.UserInfo;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.ui.utils.SlidingActivity;
import com.shoujiduoduo.util.aj;
import com.shoujiduoduo.util.g;
import com.shoujiduoduo.util.n;
import com.shoujiduoduo.util.widget.SwitchButton;
import com.shoujiduoduo.util.widget.b;
import com.shoujiduoduo.util.widget.d;

public class SettingActivity extends SlidingActivity implements AdapterView.OnItemClickListener {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public String[] f1913a = {"五星支持我们", "用户反馈", "清空播放缓存", "连续播放开关", "关于我们", "退出登录"};
    /* access modifiers changed from: private */
    public ProgressDialog b;
    /* access modifiers changed from: private */
    public boolean c;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_setting);
        com.jaeger.library.a.a(this, getResources().getColor(R.color.bkg_green), 0);
        ListView listView = (ListView) findViewById(R.id.lv_setting);
        listView.setAdapter((ListAdapter) new b());
        listView.setOnItemClickListener(this);
        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                SettingActivity.this.finish();
            }
        });
    }

    public void a() {
    }

    public void b() {
        finish();
    }

    public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
        String str;
        String str2 = this.f1913a[i];
        char c2 = 65535;
        switch (str2.hashCode()) {
            case -1428764885:
                if (str2.equals("清空播放缓存")) {
                    c2 = 2;
                    break;
                }
                break;
            case -1413641293:
                if (str2.equals("连续播放开关")) {
                    c2 = 3;
                    break;
                }
                break;
            case -450979144:
                if (str2.equals("五星支持我们")) {
                    c2 = 0;
                    break;
                }
                break;
            case 641296310:
                if (str2.equals("关于我们")) {
                    c2 = 4;
                    break;
                }
                break;
            case 918358442:
                if (str2.equals("用户反馈")) {
                    c2 = 1;
                    break;
                }
                break;
            case 1119347636:
                if (str2.equals("退出登录")) {
                    c2 = 5;
                    break;
                }
                break;
        }
        switch (c2) {
            case 0:
                try {
                    startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=com.shoujiduoduo.ringtone")));
                    return;
                } catch (Exception e) {
                    e.printStackTrace();
                    d.a((int) R.string.not_found_store);
                    return;
                }
            case 1:
                startActivity(new Intent(RingDDApp.c(), UserFeedbackActivity.class));
                return;
            case 2:
                if (!this.c) {
                    new a().execute(new Void[0]);
                    return;
                }
                return;
            case 3:
            default:
                return;
            case 4:
                startActivity(new Intent(this, AboutActivity.class));
                return;
            case 5:
                if (com.shoujiduoduo.a.b.b.g().h()) {
                    str = "退出登录后将不再享受VIP特权，确定退出登录吗？";
                } else {
                    str = "确定退出当前账号吗？";
                }
                new b.a(this).a(str).a("退出", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        SettingActivity.this.c();
                        dialogInterface.dismiss();
                        SettingActivity.this.finish();
                    }
                }).b("取消", (DialogInterface.OnClickListener) null).a().show();
                return;
        }
    }

    /* access modifiers changed from: private */
    public void c() {
        com.umeng.socialize.c.a aVar = null;
        switch (com.shoujiduoduo.a.b.b.g().d()) {
            case 2:
                aVar = com.umeng.socialize.c.a.QQ;
                break;
            case 3:
                aVar = com.umeng.socialize.c.a.SINA;
                break;
            case 5:
                aVar = com.umeng.socialize.c.a.WEIXIN;
                break;
        }
        if (aVar != null) {
            aj.a().a(this, aVar);
        }
        com.umeng.a.b.b(RingDDApp.c(), "USER_LOGOUT");
        UserInfo c2 = com.shoujiduoduo.a.b.b.g().c();
        c2.setLoginStatus(0);
        com.shoujiduoduo.a.b.b.g().a(c2);
        c.a().a(com.shoujiduoduo.a.a.b.OBSERVER_USER_CENTER, new c.a<v>() {
            public void a() {
                ((v) this.f1284a).a(com.shoujiduoduo.a.b.b.g().d());
            }
        });
    }

    private class a extends AsyncTask<Void, Void, Void> {
        private a() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.shoujiduoduo.ui.settings.SettingActivity.a(com.shoujiduoduo.ui.settings.SettingActivity, boolean):boolean
         arg types: [com.shoujiduoduo.ui.settings.SettingActivity, int]
         candidates:
          com.shoujiduoduo.ui.settings.SettingActivity.a(com.shoujiduoduo.ui.settings.SettingActivity, android.app.ProgressDialog):android.app.ProgressDialog
          com.shoujiduoduo.ui.settings.SettingActivity.a(com.shoujiduoduo.ui.settings.SettingActivity, boolean):boolean */
        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void onPostExecute(Void voidR) {
            super.onPostExecute(voidR);
            boolean unused = SettingActivity.this.c = false;
            if (SettingActivity.this.b != null) {
                SettingActivity.this.b.dismiss();
            }
            d.a("清空缓存已完成!");
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.shoujiduoduo.ui.settings.SettingActivity.a(com.shoujiduoduo.ui.settings.SettingActivity, boolean):boolean
         arg types: [com.shoujiduoduo.ui.settings.SettingActivity, int]
         candidates:
          com.shoujiduoduo.ui.settings.SettingActivity.a(com.shoujiduoduo.ui.settings.SettingActivity, android.app.ProgressDialog):android.app.ProgressDialog
          com.shoujiduoduo.ui.settings.SettingActivity.a(com.shoujiduoduo.ui.settings.SettingActivity, boolean):boolean */
        /* access modifiers changed from: protected */
        public void onPreExecute() {
            super.onPreExecute();
            ProgressDialog unused = SettingActivity.this.b = new ProgressDialog(SettingActivity.this);
            SettingActivity.this.b.setProgressStyle(0);
            SettingActivity.this.b.setIndeterminate(true);
            SettingActivity.this.b.setTitle("");
            SettingActivity.this.b.setMessage(SettingActivity.this.getResources().getString(R.string.cleaning_cache));
            SettingActivity.this.b.setCancelable(false);
            SettingActivity.this.b.show();
            boolean unused2 = SettingActivity.this.c = true;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public Void doInBackground(Void... voidArr) {
            n.a(RingDDApp.c()).a();
            return null;
        }
    }

    private class b extends BaseAdapter {
        private b() {
        }

        public int getCount() {
            return SettingActivity.this.f1913a.length;
        }

        public Object getItem(int i) {
            return SettingActivity.this.f1913a[i];
        }

        public long getItemId(int i) {
            return (long) i;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [?, android.view.ViewGroup, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        public View getView(int i, View view, ViewGroup viewGroup) {
            if (view == null) {
                view = SettingActivity.this.getLayoutInflater().inflate((int) R.layout.listitem_setting, viewGroup, false);
            }
            ImageView imageView = (ImageView) view.findViewById(R.id.setting_icon);
            TextView textView = (TextView) view.findViewById(R.id.settting_title);
            SwitchButton switchButton = (SwitchButton) view.findViewById(R.id.switch_btn);
            if (g.w()) {
                switchButton.setSwitchStatus(true);
            } else {
                switchButton.setSwitchStatus(false);
            }
            switchButton.setOnChangeListener(new SwitchButton.a() {
                public void a(SwitchButton switchButton, boolean z) {
                    com.shoujiduoduo.base.a.a.a("SettingActivity", "switchBtn:" + z);
                    g.a(z);
                    d.a(z ? "连续播放已打开" : "连续播放已关闭");
                }
            });
            textView.setText(SettingActivity.this.f1913a[i]);
            if (SettingActivity.this.f1913a[i].equals("连续播放开关")) {
                switchButton.setVisibility(0);
            } else {
                switchButton.setVisibility(8);
            }
            return view;
        }
    }
}
