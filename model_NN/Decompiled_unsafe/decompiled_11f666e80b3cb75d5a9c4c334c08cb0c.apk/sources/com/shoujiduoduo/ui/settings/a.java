package com.shoujiduoduo.ui.settings;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.baidu.mobad.feeds.BaiduNative;
import com.baidu.mobad.feeds.NativeErrorCode;
import com.baidu.mobad.feeds.NativeResponse;
import com.baidu.mobad.feeds.RequestParameters;
import com.d.a.b.d;
import com.qq.e.ads.nativ.NativeAD;
import com.qq.e.ads.nativ.NativeADDataRef;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.ui.utils.h;
import com.shoujiduoduo.util.ac;
import java.util.ArrayList;
import java.util.List;

/* compiled from: QuitDialog */
public class a extends Dialog {

    /* renamed from: a  reason: collision with root package name */
    private Button f1920a;
    private Button b;
    private Context c;
    private TextView d;
    private TextView e;
    /* access modifiers changed from: private */
    public String f;
    private boolean g;
    private ImageView h;
    private RelativeLayout i;
    private ImageView j;
    /* access modifiers changed from: private */
    public List<C0047a> k = new ArrayList();
    private int l;
    /* access modifiers changed from: private */
    public long m;

    public a(Context context, int i2) {
        super(context, i2);
        this.c = context;
        this.f = ac.a().a("quit_ad_type");
        if ("baidu".equals(this.f)) {
            this.f = "baidu";
        } else if ("gdt".equals(this.f)) {
            this.f = "gdt";
        } else {
            this.f = "baidu";
        }
        com.shoujiduoduo.base.a.a.a("QuitDialog", "quit ad type:" + this.f);
        this.g = ac.a().b("quit_ad_enable");
        com.shoujiduoduo.base.a.a.a("QuitDialog", "quit ad switch:" + this.g);
        if (!this.g || !com.shoujiduoduo.util.a.e()) {
            this.g = false;
        } else {
            a();
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        com.shoujiduoduo.base.a.a.a("QuitDialog", "onCreate");
        setContentView((int) R.layout.dialog_quit_app);
        this.f1920a = (Button) findViewById(R.id.ok);
        this.b = (Button) findViewById(R.id.cancel);
        this.d = (TextView) findViewById(R.id.tips);
        this.i = (RelativeLayout) findViewById(R.id.ad_view);
        this.j = (ImageView) findViewById(R.id.ad_icon);
        this.h = (ImageView) findViewById(R.id.ad_image);
        this.e = (TextView) findViewById(R.id.ad_hint);
        this.b.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                com.shoujiduoduo.base.a.a.a("QuitDialog", "quit dialog click cancel");
                a.this.dismiss();
            }
        });
        this.f1920a.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                RingDDApp.g();
                a.this.dismiss();
            }
        });
    }

    private void a() {
        if (this.f.equals("baidu")) {
            b();
        } else if (this.f.equals("gdt")) {
            c();
        } else {
            com.shoujiduoduo.base.a.a.e("QuitDialog", "not support");
        }
    }

    private void b() {
        com.shoujiduoduo.base.a.a.a("QuitDialog", "initBaiduFeed");
        new BaiduNative(this.c, "2010241", new BaiduNative.BaiduNativeNetworkListener() {
            public void onNativeFail(NativeErrorCode nativeErrorCode) {
                com.shoujiduoduo.base.a.a.e("QuitDialog", "baidu feed, onNativeFail reason:" + nativeErrorCode.name());
            }

            public void onNativeLoad(List<NativeResponse> list) {
                if (list.size() > 0) {
                    com.shoujiduoduo.base.a.a.a("QuitDialog", "baidu feed, onNativeLoad, ad size:" + list.size());
                    long unused = a.this.m = System.currentTimeMillis();
                    for (NativeResponse aVar : list) {
                        a.this.k.add(new C0047a(aVar));
                    }
                    return;
                }
                com.shoujiduoduo.base.a.a.a("QuitDialog", "baidu feed, onNativeLoad, ad size is 0");
            }
        }).makeRequest(new RequestParameters.Builder().confirmDownloading(true).build());
    }

    private void c() {
        com.shoujiduoduo.base.a.a.a("QuitDialog", "fetchGdtFeed");
        new NativeAD(this.c, "1105772024", "3090615613140947", new NativeAD.NativeAdListener() {
            public void onADLoaded(List<NativeADDataRef> list) {
                if (list.size() > 0) {
                    com.shoujiduoduo.base.a.a.a("QuitDialog", "fetchGdtFeed feed, onNativeLoad, ad size:" + list.size());
                    long unused = a.this.m = System.currentTimeMillis();
                    for (NativeADDataRef aVar : list) {
                        a.this.k.add(new C0047a(aVar));
                    }
                    return;
                }
                com.shoujiduoduo.base.a.a.a("QuitDialog", "fetchGdtFeed feed, onNativeLoad, ad size is 0");
            }

            public void onNoAD(int i) {
            }

            public void onADStatusChanged(NativeADDataRef nativeADDataRef) {
            }

            public void onADError(NativeADDataRef nativeADDataRef, int i) {
            }
        }).loadAD(10);
    }

    /* renamed from: com.shoujiduoduo.ui.settings.a$a  reason: collision with other inner class name */
    /* compiled from: QuitDialog */
    private class C0047a {
        private Object b;

        public C0047a(Object obj) {
            this.b = obj;
        }

        public String a() {
            if (a.this.f.equals("baidu")) {
                return ((NativeResponse) this.b).getTitle();
            }
            if (a.this.f.equals("gdt")) {
                return ((NativeADDataRef) this.b).getTitle();
            }
            return "";
        }

        public String b() {
            if (a.this.f.equals("baidu")) {
                return ((NativeResponse) this.b).getImageUrl();
            }
            if (a.this.f.equals("gdt")) {
                return ((NativeADDataRef) this.b).getImgUrl();
            }
            return "";
        }

        public void a(View view) {
            if (a.this.f.equals("baidu")) {
                ((NativeResponse) this.b).recordImpression(view);
            } else if (a.this.f.equals("gdt")) {
                ((NativeADDataRef) this.b).onExposured(view);
            }
        }

        public void b(View view) {
            if (a.this.f.equals("baidu")) {
                ((NativeResponse) this.b).handleClick(view);
            } else if (a.this.f.equals("gdt")) {
                ((NativeADDataRef) this.b).onClicked(view);
            }
        }
    }

    private void d() {
        if (!this.g || !com.shoujiduoduo.util.a.e()) {
            com.shoujiduoduo.base.a.a.a("QuitDialog", "can not showQuitAD");
            return;
        }
        com.shoujiduoduo.base.a.a.a("QuitDialog", "showQuitAD");
        if (System.currentTimeMillis() - this.m > 1800000) {
            com.shoujiduoduo.base.a.a.a("QuitDialog", "ad is out of time, fetch new ad data");
            a();
        } else if (this.k != null && this.k.size() > 0) {
            this.l++;
            this.l %= this.k.size();
            if (this.l < this.k.size()) {
                final C0047a aVar = this.k.get(this.l);
                com.shoujiduoduo.base.a.a.a("QuitDialog", "current ad index:" + this.l);
                com.shoujiduoduo.base.a.a.a("QuitDialog", "ad title:" + aVar.a());
                com.shoujiduoduo.base.a.a.a("QuitDialog", "ad image:" + aVar.b());
                this.i.setVisibility(0);
                if (this.f.equals("gdt")) {
                    this.j.setVisibility(0);
                } else {
                    this.j.setVisibility(4);
                }
                d.a().a(aVar.b(), this.h, h.a().b());
                this.d.setText(aVar.a());
                this.d.setVisibility(0);
                this.e.setVisibility(0);
                aVar.a(this.h);
                this.h.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {
                        aVar.b(view);
                    }
                });
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        com.shoujiduoduo.base.a.a.a("QuitDialog", "onStart");
        d();
        super.onStart();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        com.shoujiduoduo.base.a.a.a("QuitDialog", "onStop");
        if (this.g && com.shoujiduoduo.util.a.e()) {
            a();
        }
        super.onStop();
    }
}
