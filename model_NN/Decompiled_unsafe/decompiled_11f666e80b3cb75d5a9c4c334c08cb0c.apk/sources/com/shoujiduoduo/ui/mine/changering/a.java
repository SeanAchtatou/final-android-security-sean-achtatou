package com.shoujiduoduo.ui.mine.changering;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.shoujiduoduo.a.a.b;
import com.shoujiduoduo.a.a.c;
import com.shoujiduoduo.a.c.o;
import com.shoujiduoduo.a.c.p;
import com.shoujiduoduo.b.c.j;
import com.shoujiduoduo.base.bean.DDList;
import com.shoujiduoduo.base.bean.ListType;
import com.shoujiduoduo.base.bean.RingData;
import com.shoujiduoduo.player.PlayerService;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.ui.utils.d;
import com.shoujiduoduo.ui.utils.m;
import com.shoujiduoduo.util.aa;
import com.shoujiduoduo.util.ai;
import java.io.File;

/* compiled from: SystemRingListAdapter */
public class a extends d {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public j f1859a;
    /* access modifiers changed from: private */
    public int b = -1;
    /* access modifiers changed from: private */
    public boolean c;
    /* access modifiers changed from: private */
    public Context d;
    private o e = new o() {
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.shoujiduoduo.ui.mine.changering.a.a(com.shoujiduoduo.ui.mine.changering.a, boolean):boolean
         arg types: [com.shoujiduoduo.ui.mine.changering.a, int]
         candidates:
          com.shoujiduoduo.ui.mine.changering.a.a(com.shoujiduoduo.ui.mine.changering.a, int):int
          com.shoujiduoduo.ui.mine.changering.a.a(android.view.View, int):void
          com.shoujiduoduo.ui.mine.changering.a.a(com.shoujiduoduo.ui.mine.changering.a, boolean):boolean */
        public void a(String str, int i) {
            if (a.this.f1859a != null && a.this.f1859a.getListId().equals(str)) {
                com.shoujiduoduo.base.a.a.a("SystemRingListAdapter", "onSetPlay, listid:" + str);
                if (str.equals(a.this.f1859a.getListId())) {
                    boolean unused = a.this.c = true;
                    int unused2 = a.this.b = i;
                } else {
                    boolean unused3 = a.this.c = false;
                }
                a.this.notifyDataSetChanged();
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.shoujiduoduo.ui.mine.changering.a.a(com.shoujiduoduo.ui.mine.changering.a, boolean):boolean
         arg types: [com.shoujiduoduo.ui.mine.changering.a, int]
         candidates:
          com.shoujiduoduo.ui.mine.changering.a.a(com.shoujiduoduo.ui.mine.changering.a, int):int
          com.shoujiduoduo.ui.mine.changering.a.a(android.view.View, int):void
          com.shoujiduoduo.ui.mine.changering.a.a(com.shoujiduoduo.ui.mine.changering.a, boolean):boolean */
        public void b(String str, int i) {
            if (a.this.f1859a != null && a.this.f1859a.getListId().equals(str)) {
                com.shoujiduoduo.base.a.a.a("SystemRingListAdapter", "onCanclePlay, listId:" + str);
                boolean unused = a.this.c = false;
                int unused2 = a.this.b = i;
                a.this.notifyDataSetChanged();
            }
        }

        public void a(String str, int i, int i2) {
            if (a.this.f1859a != null && a.this.f1859a.getListId().equals(str)) {
                com.shoujiduoduo.base.a.a.a("SystemRingListAdapter", "onStatusChange, listid:" + str);
                a.this.notifyDataSetChanged();
            }
        }
    };
    private View.OnClickListener f = new View.OnClickListener() {
        public void onClick(View view) {
            PlayerService b = aa.a().b();
            if (b != null) {
                if (b.a() == 3) {
                    b.n();
                } else {
                    b.i();
                }
            }
        }
    };
    private View.OnClickListener g = new View.OnClickListener() {
        public void onClick(View view) {
            PlayerService b = aa.a().b();
            if (b != null) {
                b.j();
            }
        }
    };
    private View.OnClickListener h = new View.OnClickListener() {
        public void onClick(View view) {
            PlayerService b = aa.a().b();
            if (b != null) {
                b.a(a.this.f1859a, a.this.b);
            }
        }
    };
    private View.OnClickListener i = new View.OnClickListener() {
        public void onClick(View view) {
            if (a.this.f1859a.get(a.this.b) != null) {
                final int i = 0;
                switch (AnonymousClass6.f1865a[a.this.f1859a.a().ordinal()]) {
                    case 1:
                        i = 4;
                        break;
                    case 2:
                        i = 2;
                        break;
                    case 3:
                        i = 1;
                        break;
                }
                ai aiVar = new ai(RingDDApp.c());
                File file = new File(a.this.f1859a.get(a.this.b).localPath);
                if (file.exists()) {
                    aiVar.a(i, Uri.fromFile(file), a.this.f1859a.get(a.this.b).localPath);
                    String str = "";
                    String str2 = a.this.f1859a.get(a.this.b).name;
                    String string = a.this.d.getResources().getString(R.string.set_ring_hint2);
                    switch (i) {
                        case 1:
                            str = str2 + string + a.this.d.getResources().getString(R.string.set_ring_incoming_call);
                            break;
                        case 2:
                            str = str2 + string + a.this.d.getResources().getString(R.string.set_ring_message);
                            break;
                        case 4:
                            str = str2 + string + a.this.d.getResources().getString(R.string.set_ring_alarm);
                            break;
                    }
                    com.shoujiduoduo.util.widget.d.a(str);
                    c.a().b(b.OBSERVER_RING_CHANGE, new c.a<p>() {
                        public void a() {
                            ((p) this.f1284a).a(i, new RingData());
                        }
                    });
                }
            }
        }
    };

    public a(Context context) {
        this.d = context;
    }

    public void a() {
        c.a().a(b.OBSERVER_PLAY_STATUS, this.e);
    }

    public void b() {
        c.a().b(b.OBSERVER_PLAY_STATUS, this.e);
    }

    public int getCount() {
        if (this.f1859a == null) {
            return 0;
        }
        return this.f1859a.size();
    }

    public Object getItem(int i2) {
        if (this.f1859a != null && i2 >= 0 && i2 < this.f1859a.size()) {
            return this.f1859a.get(i2);
        }
        return null;
    }

    public long getItemId(int i2) {
        return (long) i2;
    }

    private void a(View view, int i2) {
        RingData b2 = this.f1859a.get(i2);
        TextView textView = (TextView) m.a(view, R.id.item_duration);
        ((TextView) m.a(view, R.id.item_song_name)).setText(b2.name);
        textView.setText(String.format("%02d:%02d", Integer.valueOf(b2.duration / 60), Integer.valueOf(b2.duration % 60)));
        if (b2.duration == 0) {
            textView.setVisibility(4);
        } else {
            textView.setVisibility(0);
        }
    }

    /* renamed from: com.shoujiduoduo.ui.mine.changering.a$6  reason: invalid class name */
    /* compiled from: SystemRingListAdapter */
    static /* synthetic */ class AnonymousClass6 {

        /* renamed from: a  reason: collision with root package name */
        static final /* synthetic */ int[] f1865a = new int[ListType.LIST_TYPE.values().length];

        static {
            try {
                f1865a[ListType.LIST_TYPE.sys_alarm.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                f1865a[ListType.LIST_TYPE.sys_notify.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                f1865a[ListType.LIST_TYPE.sys_ringtone.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View getView(int i2, View view, ViewGroup viewGroup) {
        if (this.f1859a == null) {
            return null;
        }
        if (i2 >= this.f1859a.size()) {
            return view;
        }
        if (view == null) {
            view = LayoutInflater.from(this.d).inflate((int) R.layout.system_ring_item, viewGroup, false);
        }
        a(view, i2);
        ProgressBar progressBar = (ProgressBar) m.a(view, R.id.ringitem_download_progress);
        TextView textView = (TextView) m.a(view, R.id.ringitem_serial_number);
        ImageButton imageButton = (ImageButton) m.a(view, R.id.ringitem_play);
        imageButton.setOnClickListener(this.f);
        ImageButton imageButton2 = (ImageButton) m.a(view, R.id.ringitem_pause);
        imageButton2.setOnClickListener(this.g);
        ImageButton imageButton3 = (ImageButton) m.a(view, R.id.ringitem_failed);
        imageButton3.setOnClickListener(this.h);
        PlayerService b2 = aa.a().b();
        if (i2 != this.b || !this.c) {
            ((Button) m.a(view, R.id.ring_item_set)).setVisibility(8);
            textView.setText(Integer.toString(i2 + 1));
            textView.setVisibility(0);
            progressBar.setVisibility(4);
            imageButton.setVisibility(4);
            imageButton2.setVisibility(4);
            imageButton3.setVisibility(4);
            return view;
        }
        Button button = (Button) m.a(view, R.id.ring_item_set);
        button.setVisibility(0);
        button.setOnClickListener(this.i);
        textView.setVisibility(4);
        imageButton.setVisibility(4);
        imageButton2.setVisibility(4);
        imageButton3.setVisibility(4);
        int i3 = 5;
        if (b2 != null) {
            i3 = b2.a();
        }
        switch (i3) {
            case 1:
                progressBar.setVisibility(0);
                return view;
            case 2:
                imageButton2.setVisibility(0);
                return view;
            case 3:
            case 4:
            case 5:
                imageButton.setVisibility(0);
                return view;
            case 6:
                imageButton3.setVisibility(0);
                return view;
            default:
                return view;
        }
    }

    public void a(boolean z) {
    }

    public void a(DDList dDList) {
        if (this.f1859a != dDList) {
            this.f1859a = null;
            this.f1859a = (j) dDList;
            this.c = false;
            notifyDataSetChanged();
        }
    }
}
