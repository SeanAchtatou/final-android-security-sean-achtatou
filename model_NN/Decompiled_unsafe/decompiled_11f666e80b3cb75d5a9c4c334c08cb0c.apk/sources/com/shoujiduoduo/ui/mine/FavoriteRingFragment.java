package com.shoujiduoduo.ui.mine;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.shoujiduoduo.a.a.c;
import com.shoujiduoduo.a.c.g;
import com.shoujiduoduo.a.c.p;
import com.shoujiduoduo.a.c.w;
import com.shoujiduoduo.base.bean.DDList;
import com.shoujiduoduo.base.bean.RingData;
import com.shoujiduoduo.player.PlayerService;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.util.aa;
import com.shoujiduoduo.util.ai;
import com.shoujiduoduo.util.i;
import com.shoujiduoduo.util.widget.b;
import com.shoujiduoduo.util.widget.d;
import java.util.List;

public class FavoriteRingFragment extends Fragment {

    /* renamed from: a  reason: collision with root package name */
    private ListView f1738a;
    /* access modifiers changed from: private */
    public b b;
    /* access modifiers changed from: private */
    public a c;
    /* access modifiers changed from: private */
    public b d;
    /* access modifiers changed from: private */
    public String[] e;
    private LinearLayout f;
    /* access modifiers changed from: private */
    public boolean g;
    private View h;
    private Button i;
    /* access modifiers changed from: private */
    public Button j;
    /* access modifiers changed from: private */
    public int k;
    private boolean l;
    private View.OnKeyListener m = new View.OnKeyListener() {
        public boolean onKey(View view, int i, KeyEvent keyEvent) {
            if (i != 4 || keyEvent.getAction() != 0 || !FavoriteRingFragment.this.g) {
                return false;
            }
            com.shoujiduoduo.base.a.a.a("FavoriteRingFrag", "edit mode, key back");
            FavoriteRingFragment.this.a(false);
            return true;
        }
    };
    private View.OnClickListener n = new View.OnClickListener() {
        public void onClick(View view) {
            FavoriteRingFragment.this.a(false);
            int unused = FavoriteRingFragment.this.k = 0;
            FavoriteRingFragment.this.j.setText("删除");
        }
    };
    private View.OnClickListener o = new View.OnClickListener() {
        public void onClick(View view) {
            final List<Integer> b = FavoriteRingFragment.this.c.b();
            if (b == null || b.size() == 0) {
                d.a("请选择要删除的铃声", 0);
            } else {
                new b.a(FavoriteRingFragment.this.getActivity()).b((int) R.string.hint).a("确定删除选中的铃声吗？").a((int) R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (com.shoujiduoduo.a.b.b.b().a("favorite_ring_list", b)) {
                            d.a("已删除" + b.size() + "首铃声", 0);
                            FavoriteRingFragment.this.a(false);
                        } else {
                            d.a("删除铃声失败", 0);
                        }
                        dialogInterface.dismiss();
                    }
                }).b((int) R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).a().show();
            }
        }
    };
    private p p = new p() {
        public void a(int i, RingData ringData) {
            FavoriteRingFragment.this.c();
            if (FavoriteRingFragment.this.b != null) {
                FavoriteRingFragment.this.b.notifyDataSetChanged();
            }
        }
    };
    private g q = new g() {
        public void a(DDList dDList, int i) {
            if (dDList != null && dDList.getListId().equals("user_favorite")) {
                com.shoujiduoduo.base.a.a.a("FavoriteRingFrag", "data update");
                FavoriteRingFragment.this.b();
                FavoriteRingFragment.this.a(false);
                FavoriteRingFragment.this.b.notifyDataSetChanged();
            }
        }
    };
    private w r = new w() {
        public void a() {
            com.shoujiduoduo.base.a.a.b("FavoriteRingFrag", "load start!");
        }

        public void a(int i, List<RingData> list, String str) {
            com.shoujiduoduo.base.a.a.b("FavoriteRingFrag", "load complete!");
            if (str.equals("user_favorite")) {
                FavoriteRingFragment.this.a();
            }
        }
    };

    static /* synthetic */ int j(FavoriteRingFragment favoriteRingFragment) {
        int i2 = favoriteRingFragment.k;
        favoriteRingFragment.k = i2 + 1;
        return i2;
    }

    static /* synthetic */ int k(FavoriteRingFragment favoriteRingFragment) {
        int i2 = favoriteRingFragment.k;
        favoriteRingFragment.k = i2 - 1;
        return i2;
    }

    public void onCreate(Bundle bundle) {
        this.b = new b(getActivity());
        this.c = new a(getActivity(), com.shoujiduoduo.a.b.b.b().b("favorite_ring_list"), "favorite_ring_list");
        this.d = new b();
        this.e = new String[3];
        com.shoujiduoduo.base.a.a.a("FavoriteRingFrag", "onCreate");
        super.onCreate(bundle);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View inflate = layoutInflater.inflate((int) R.layout.my_ringtone_favorite, viewGroup, false);
        this.f = (LinearLayout) inflate.findViewById(R.id.no_data);
        this.h = inflate.findViewById(R.id.del_confirm);
        this.i = (Button) this.h.findViewById(R.id.cancel);
        this.i.setOnClickListener(this.n);
        this.j = (Button) this.h.findViewById(R.id.delete);
        this.j.setOnClickListener(this.o);
        this.f1738a = (ListView) inflate.findViewById(R.id.my_ringtone_like_list);
        if (com.shoujiduoduo.a.b.b.b().c()) {
            com.shoujiduoduo.base.a.a.b("FavoriteRingFrag", "favorite ring data is loaded!");
            a();
            b();
        }
        this.f1738a.setChoiceMode(1);
        this.f1738a.setOnItemClickListener(new a());
        c();
        this.b.a();
        c.a().a(com.shoujiduoduo.a.a.b.OBSERVER_USER_RING, this.r);
        c.a().a(com.shoujiduoduo.a.a.b.OBSERVER_LIST_DATA, this.q);
        c.a().a(com.shoujiduoduo.a.a.b.OBSERVER_RING_CHANGE, this.p);
        com.shoujiduoduo.base.a.a.a("FavoriteRingFrag", "FavoriteRingFragment onCreateView");
        return inflate;
    }

    /* access modifiers changed from: private */
    public void a() {
        if (!this.l) {
            this.f1738a.setAdapter((ListAdapter) this.b);
            b();
            this.l = true;
        }
    }

    /* access modifiers changed from: private */
    public void b() {
        if (com.shoujiduoduo.a.b.b.b().b("favorite_ring_list").size() > 0) {
            this.f.setVisibility(8);
        } else {
            this.f.setVisibility(0);
        }
    }

    /* access modifiers changed from: private */
    public void c() {
        i.a(new Runnable() {
            public void run() {
                com.shoujiduoduo.base.a.a.a("FavoriteRingFrag", "read ring begin");
                ai.b a2 = new ai(FavoriteRingFragment.this.getActivity()).a();
                FavoriteRingFragment.this.e[0] = a2.f2188a.b;
                FavoriteRingFragment.this.e[1] = a2.b.b;
                FavoriteRingFragment.this.e[2] = a2.c.b;
                com.shoujiduoduo.base.a.a.a("FavoriteRingFrag", "read ring end");
                FavoriteRingFragment.this.d.sendEmptyMessage(0);
            }
        });
    }

    public void onResume() {
        com.shoujiduoduo.base.a.a.a("FavoriteRingFrag", "onResume");
        super.onResume();
        getView().setFocusable(true);
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(this.m);
    }

    public void onPause() {
        com.shoujiduoduo.base.a.a.a("FavoriteRingFrag", "onPause");
        super.onPause();
    }

    public void onStop() {
        com.shoujiduoduo.base.a.a.a("FavoriteRingFrag", "onStop");
        super.onStop();
    }

    public void a(boolean z) {
        if (this.g == z || !this.l) {
            com.shoujiduoduo.base.a.a.a("FavoriteRingFrag", "return, bEdit:" + z + ", mEditMode:" + this.g + ", mListviewInited:" + this.l);
            return;
        }
        this.g = z;
        this.h.setVisibility(z ? 0 : 8);
        if (z) {
            this.b.a(-1);
            this.c.a(com.shoujiduoduo.a.b.b.b().b("favorite_ring_list"));
            this.f1738a.setAdapter((ListAdapter) this.c);
            this.k = 0;
            this.j.setText("删除");
            return;
        }
        this.f1738a.setAdapter((ListAdapter) this.b);
    }

    public void onDestroy() {
        super.onDestroy();
        com.shoujiduoduo.base.a.a.a("FavoriteRingFrag", "onDestroy");
    }

    public void onDestroyView() {
        com.shoujiduoduo.base.a.a.a("FavoriteRingFrag", "FavoriteRingFragment ondestroyView");
        if (this.d != null) {
            this.d.removeCallbacksAndMessages(null);
        }
        this.b.b();
        c.a().b(com.shoujiduoduo.a.a.b.OBSERVER_USER_RING, this.r);
        c.a().b(com.shoujiduoduo.a.a.b.OBSERVER_LIST_DATA, this.q);
        c.a().b(com.shoujiduoduo.a.a.b.OBSERVER_RING_CHANGE, this.p);
        this.l = false;
        this.g = false;
        super.onDestroyView();
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    private class b extends Handler {
        private b() {
        }

        public void handleMessage(Message message) {
            super.handleMessage(message);
        }
    }

    private class a implements AdapterView.OnItemClickListener {
        private a() {
        }

        public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
            com.shoujiduoduo.base.a.a.a("FavoriteRingFrag", "click MyRingtone Item.");
            if (j >= 0) {
                int i2 = (int) j;
                if (FavoriteRingFragment.this.g) {
                    CheckBox checkBox = (CheckBox) view.findViewById(R.id.checkbox);
                    checkBox.toggle();
                    FavoriteRingFragment.this.c.a().set(i2, Boolean.valueOf(checkBox.isChecked()));
                    if (checkBox.isChecked()) {
                        FavoriteRingFragment.j(FavoriteRingFragment.this);
                    } else {
                        FavoriteRingFragment.k(FavoriteRingFragment.this);
                    }
                    if (FavoriteRingFragment.this.k > 0) {
                        FavoriteRingFragment.this.j.setText("删除(" + FavoriteRingFragment.this.k + ")");
                    } else {
                        FavoriteRingFragment.this.j.setText("删除");
                    }
                } else {
                    PlayerService b = aa.a().b();
                    if (b != null) {
                        b.a(com.shoujiduoduo.a.b.b.b().b("favorite_ring_list"), i2);
                    } else {
                        com.shoujiduoduo.base.a.a.c("FavoriteRingFrag", "PlayerService is unavailable!");
                    }
                }
            }
        }
    }
}
