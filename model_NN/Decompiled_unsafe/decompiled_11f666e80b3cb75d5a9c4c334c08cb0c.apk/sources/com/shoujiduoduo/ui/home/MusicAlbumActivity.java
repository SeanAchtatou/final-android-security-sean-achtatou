package com.shoujiduoduo.ui.home;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.media.TransportMediator;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;
import com.igexin.download.Downloads;
import com.shoujiduoduo.player.PlayerService;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.ui.utils.BaseActivity;
import com.shoujiduoduo.util.aa;
import com.shoujiduoduo.util.ah;
import com.shoujiduoduo.util.g;
import com.shoujiduoduo.util.h;
import com.shoujiduoduo.util.widget.b;
import com.tencent.open.SocialConstants;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;

public class MusicAlbumActivity extends BaseActivity implements View.OnClickListener {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public WebView f1688a;
    private TextView b;
    private String c;
    /* access modifiers changed from: private */
    public View d;
    /* access modifiers changed from: private */
    public ValueCallback<Uri> e;
    /* access modifiers changed from: private */
    public ValueCallback<Uri[]> f;
    /* access modifiers changed from: private */
    public boolean g;
    private a h;
    private String i;

    public enum a {
        my_album,
        create_album,
        ring_story,
        create_ring_story
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        String a2;
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_music_album);
        com.jaeger.library.a.a(this, getResources().getColor(R.color.bkg_green), 0);
        PlayerService b2 = aa.a().b();
        if (b2 != null && b2.l()) {
            b2.m();
        }
        findViewById(R.id.backButton).setOnClickListener(this);
        findViewById(R.id.btn_close_web_activity).setOnClickListener(this);
        this.b = (TextView) findViewById(R.id.header_text);
        this.d = findViewById(R.id.loading_view);
        this.d.setVisibility(0);
        this.f1688a = (WebView) findViewById(R.id.webview_window);
        this.f1688a.setVisibility(4);
        String str = "";
        this.h = a.create_album;
        Intent intent = getIntent();
        if (intent != null) {
            String stringExtra = intent.getStringExtra("musicid");
            if (!ah.c(stringExtra)) {
                str = stringExtra;
            }
            com.shoujiduoduo.base.a.a.a("MusicAlbumActivity", "musicId:" + str);
            a aVar = (a) intent.getSerializableExtra("type");
            if (aVar != null) {
                this.h = aVar;
                com.shoujiduoduo.base.a.a.a("MusicAlbumActivity", "web type:" + this.h);
                if (this.h == a.create_ring_story) {
                    this.c = intent.getStringExtra(SocialConstants.PARAM_APP_DESC);
                }
            }
            a2 = intent.getStringExtra("url");
            if (ah.c(a2)) {
                a2 = a(this.h);
            }
            String stringExtra2 = intent.getStringExtra("title");
            if (!ah.c(stringExtra2)) {
                this.b.setText(stringExtra2);
            }
        } else {
            a2 = a(a.my_album);
        }
        com.shoujiduoduo.base.a.a.a("MusicAlbumActivity", "base url:" + a2);
        this.i = a(a2, str);
        com.shoujiduoduo.base.a.a.a("MusicAlbumActivity", "url:" + this.i);
        WebSettings settings = this.f1688a.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setUseWideViewPort(true);
        settings.setLoadWithOverviewMode(true);
        settings.setBuiltInZoomControls(false);
        settings.setDomStorageEnabled(true);
        settings.setSupportZoom(false);
        if (Build.VERSION.SDK_INT >= 21) {
            settings.setMixedContentMode(0);
        }
        this.f1688a.requestFocus(TransportMediator.KEYCODE_MEDIA_RECORD);
        this.f1688a.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent motionEvent) {
                switch (motionEvent.getAction()) {
                    case 0:
                    case 1:
                        if (view.hasFocus()) {
                            return false;
                        }
                        view.requestFocus();
                        return false;
                    default:
                        return false;
                }
            }
        });
        this.f1688a.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView webView, String str) {
                com.shoujiduoduo.base.a.a.a("MusicAlbumActivity", "override url:" + str);
                if (h.a(str)) {
                    h.a(MusicAlbumActivity.this, str);
                    return true;
                }
                webView.loadUrl(str);
                return true;
            }

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.shoujiduoduo.ui.home.MusicAlbumActivity.a(com.shoujiduoduo.ui.home.MusicAlbumActivity, boolean):boolean
             arg types: [com.shoujiduoduo.ui.home.MusicAlbumActivity, int]
             candidates:
              com.shoujiduoduo.ui.home.MusicAlbumActivity.a(com.shoujiduoduo.ui.home.MusicAlbumActivity, android.webkit.ValueCallback):android.webkit.ValueCallback
              com.shoujiduoduo.ui.home.MusicAlbumActivity.a(java.lang.String, java.lang.String):java.lang.String
              com.shoujiduoduo.ui.home.MusicAlbumActivity.a(int, android.content.Intent):void
              com.shoujiduoduo.ui.home.MusicAlbumActivity.a(com.shoujiduoduo.ui.home.MusicAlbumActivity, boolean):boolean */
            public void onPageFinished(WebView webView, String str) {
                super.onPageFinished(webView, str);
                com.shoujiduoduo.base.a.a.a("MusicAlbumActivity", "onPageFinished, url:" + str);
                if (TextUtils.isEmpty(str) || !str.contains("/album.php")) {
                    com.shoujiduoduo.base.a.a.a("MusicAlbumActivity", "not in create album view");
                    boolean unused = MusicAlbumActivity.this.g = false;
                } else {
                    com.shoujiduoduo.base.a.a.a("MusicAlbumActivity", "in create album view");
                    boolean unused2 = MusicAlbumActivity.this.g = true;
                }
                MusicAlbumActivity.this.d.setVisibility(4);
                MusicAlbumActivity.this.f1688a.setVisibility(0);
                MusicAlbumActivity.this.runOnUiThread(new Runnable() {
                    public void run() {
                        MusicAlbumActivity.this.f1688a.loadUrl("javascript: jResume()");
                    }
                });
            }
        });
        this.f1688a.setWebChromeClient(new WebChromeClient() {
            public void openFileChooser(ValueCallback<Uri> valueCallback) {
                customOpenFileChooser(valueCallback);
            }

            /* access modifiers changed from: protected */
            public void openFileChooser(ValueCallback valueCallback, String str) {
                customOpenFileChooser(valueCallback);
            }

            /* access modifiers changed from: protected */
            public void openFileChooser(ValueCallback<Uri> valueCallback, String str, String str2) {
                customOpenFileChooser(valueCallback);
            }

            public boolean onShowFileChooser(WebView webView, ValueCallback<Uri[]> valueCallback, WebChromeClient.FileChooserParams fileChooserParams) {
                com.shoujiduoduo.base.a.a.a("musicalbum", "onShowFileChooser");
                ValueCallback unused = MusicAlbumActivity.this.f = valueCallback;
                Intent intent = new Intent("android.intent.action.GET_CONTENT");
                intent.addCategory("android.intent.category.OPENABLE");
                intent.setType("image/*");
                MusicAlbumActivity.this.startActivityForResult(Intent.createChooser(intent, "选择文件"), 102);
                return true;
            }

            /* access modifiers changed from: protected */
            public void customOpenFileChooser(ValueCallback<Uri> valueCallback) {
                com.shoujiduoduo.base.a.a.a("musicalbum", "customOpenFileChooser");
                ValueCallback unused = MusicAlbumActivity.this.e = valueCallback;
                Intent intent = new Intent("android.intent.action.GET_CONTENT");
                intent.addCategory("android.intent.category.OPENABLE");
                intent.setType("image/*");
                MusicAlbumActivity.this.startActivityForResult(Intent.createChooser(intent, "选择文件"), 102);
            }
        });
        if (Build.VERSION.SDK_INT >= 17) {
            this.f1688a.getSettings().setMediaPlaybackRequiresUserGesture(false);
        }
        this.f1688a.loadUrl(this.i);
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.backButton:
                if (this.f1688a == null) {
                    finish();
                    return;
                } else if (this.g && !a.ring_story.equals(this.h)) {
                    new b.a(this).a("是否将音乐相册分享给好友呢？").a("确定", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                            if (MusicAlbumActivity.this.f1688a != null) {
                                MusicAlbumActivity.this.f1688a.loadUrl("javascript:ddshare()");
                            }
                            HashMap hashMap = new HashMap();
                            hashMap.put("act", "ok");
                            com.umeng.a.b.a(RingDDApp.c(), "musicalbum_quit_dialog", hashMap);
                        }
                    }).b("取消", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                            HashMap hashMap = new HashMap();
                            hashMap.put("act", "cancel");
                            com.umeng.a.b.a(RingDDApp.c(), "musicalbum_quit_dialog", hashMap);
                            if (MusicAlbumActivity.this.f1688a.canGoBack()) {
                                MusicAlbumActivity.this.f1688a.goBack();
                            } else {
                                MusicAlbumActivity.this.finish();
                            }
                        }
                    }).a().show();
                    return;
                } else if (this.f1688a.canGoBack()) {
                    this.f1688a.goBack();
                    return;
                } else {
                    finish();
                    return;
                }
            case R.id.btn_close_web_activity:
                finish();
                return;
            default:
                return;
        }
    }

    private String a(String str, String str2) {
        StringBuilder sb = new StringBuilder();
        sb.append(h.b(str));
        sb.append("&needstory=1");
        if (!ah.c(str2)) {
            sb.append("&musicid=").append(str2);
        }
        if (!ah.c(this.c)) {
            try {
                sb.append("&desc=").append(URLEncoder.encode(this.c, "UTF-8"));
            } catch (UnsupportedEncodingException e2) {
                e2.printStackTrace();
            }
        }
        sb.append("&isrc=").append(g.n());
        return sb.toString();
    }

    private String a(a aVar) {
        switch (aVar) {
            case create_album:
                String a2 = com.umeng.b.a.a().a(RingDDApp.c(), "create_music_album_url_new");
                if (TextUtils.isEmpty(a2)) {
                    return "http://musicalbum.shoujiduoduo.com/malbum/serv/createalbum.php?ddsrc=ring_ar";
                }
                return a2;
            case my_album:
                String a3 = com.umeng.b.a.a().a(RingDDApp.c(), "music_album_url_new");
                if (TextUtils.isEmpty(a3)) {
                    return "https://musicalbum.shoujiduoduo.com/malbum/serv/myalbums.php?ddsrc=ring_ar";
                }
                return a3;
            case ring_story:
                String a4 = com.umeng.b.a.a().a(RingDDApp.c(), "ring_story_url_new");
                if (TextUtils.isEmpty(a4)) {
                    return "http://musicstory1.csoot.com/mstory/serv/hotstory.php?ddsrc=ring_ar";
                }
                return a4;
            case create_ring_story:
                String a5 = com.umeng.b.a.a().a(RingDDApp.c(), "create_ring_story_url_new");
                if (TextUtils.isEmpty(a5)) {
                    return "http://musicalbum.shoujiduoduo.com/mstory/serv/edit.php?local=1&ddsrc=upload_ring_ar";
                }
                return a5;
            default:
                return "";
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        super.onActivityResult(i2, i3, intent);
        if (i2 == 102) {
            a(i3, intent);
        }
    }

    private void a(int i2, Intent intent) {
        String str;
        if (Build.VERSION.SDK_INT < 21) {
            Uri data = (intent == null || i2 != -1) ? null : intent.getData();
            if ("smartisan".equals(Build.BRAND)) {
                try {
                    Cursor managedQuery = managedQuery(data, new String[]{Downloads._DATA}, null, null, null);
                    int columnIndexOrThrow = managedQuery.getColumnIndexOrThrow(Downloads._DATA);
                    managedQuery.moveToFirst();
                    str = managedQuery.getString(columnIndexOrThrow);
                } catch (Exception e2) {
                    str = null;
                }
                if (!TextUtils.isEmpty(str)) {
                    try {
                        data = Uri.fromFile(new File(str));
                    } catch (Exception e3) {
                    }
                }
            }
            if (this.e != null) {
                this.e.onReceiveValue(data);
            } else {
                com.shoujiduoduo.base.a.a.c("MusicAlbumActivity", "mUploadMsgOld is null");
            }
        } else if (this.f != null) {
            this.f.onReceiveValue(WebChromeClient.FileChooserParams.parseResult(i2, intent));
        } else {
            com.shoujiduoduo.base.a.a.c("MusicAlbumActivity", "mUploadMsg is null");
        }
        this.f = null;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        this.f1688a.loadUrl("about:blank");
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 != 4) {
            return super.onKeyDown(i2, keyEvent);
        }
        findViewById(R.id.backButton).performClick();
        return true;
    }
}
