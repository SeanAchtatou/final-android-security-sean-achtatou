package com.shoujiduoduo.ui.home;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.CheckedTextView;
import android.widget.ListAdapter;
import android.widget.SectionIndexer;
import android.widget.TextView;
import com.shoujiduoduo.a.a.c;
import com.shoujiduoduo.a.c.l;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.util.widget.IndexListView;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

/* compiled from: ChangeAreaDialog */
public class a extends Dialog {

    /* renamed from: a  reason: collision with root package name */
    Context f1697a;
    IndexListView b;
    C0046a c;
    String[] d;
    String[] e;
    int f = -1;
    String g;

    public a(Context context, int i, String str) {
        super(context, i);
        this.f1697a = context;
        this.g = str;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.dialog_change_area);
        this.d = this.f1697a.getResources().getStringArray(R.array.city_list);
        this.e = this.f1697a.getResources().getStringArray(R.array.city_list_alpha);
        this.c = new C0046a(this.f1697a, this.d, this.e);
        this.b = (IndexListView) findViewById(R.id.area_list);
        this.b.setFastScrollEnabled(true);
        setCanceledOnTouchOutside(true);
        this.b.setAdapter((ListAdapter) this.c);
        this.b.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
                a.this.f = i;
                a.this.c.notifyDataSetChanged();
                if (a.this.f > -1 && a.this.f < a.this.d.length) {
                    com.shoujiduoduo.base.a.a.a("ChangeAreaDialog", "选择地域：" + a.this.d[a.this.f]);
                    final String str = a.this.d[a.this.f];
                    final String str2 = a.this.g;
                    c.a().b(com.shoujiduoduo.a.a.b.OBSERVER_LIST_AREA, new c.a<l>() {
                        public void a() {
                            ((l) this.f1284a).a(str, str2);
                        }
                    });
                }
                a.this.f = -1;
                a.this.dismiss();
            }
        });
    }

    /* compiled from: ChangeAreaDialog */
    private static class b {

        /* renamed from: a  reason: collision with root package name */
        TextView f1700a;
        CheckedTextView b;

        private b() {
        }
    }

    /* renamed from: com.shoujiduoduo.ui.home.a$a  reason: collision with other inner class name */
    /* compiled from: ChangeAreaDialog */
    private class C0046a extends BaseAdapter implements SectionIndexer {
        private LayoutInflater b;
        private String[] c;
        private String[] d;
        private HashMap<String, Integer> e = new HashMap<>();
        private String[] f;

        public C0046a(Context context, String[] strArr, String[] strArr2) {
            this.b = LayoutInflater.from(context);
            this.c = strArr;
            this.d = strArr2;
            for (int i = 0; i < strArr2.length; i++) {
                if (!this.e.containsKey(strArr2[i])) {
                    this.e.put(strArr2[i], Integer.valueOf(i));
                }
            }
            ArrayList arrayList = new ArrayList(this.e.keySet());
            Collections.sort(arrayList);
            this.f = new String[arrayList.size()];
            arrayList.toArray(this.f);
        }

        public int getCount() {
            return this.c.length;
        }

        public Object getItem(int i) {
            if (i < this.c.length) {
                return this.c[i];
            }
            return null;
        }

        public long getItemId(int i) {
            return (long) i;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [?, android.view.ViewGroup, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        public View getView(int i, View view, ViewGroup viewGroup) {
            b bVar;
            if (view == null) {
                view = this.b.inflate((int) R.layout.listitem_area, viewGroup, false);
                b bVar2 = new b();
                bVar2.f1700a = (TextView) view.findViewById(R.id.area_list_alpha);
                bVar2.b = (CheckedTextView) view.findViewById(R.id.area_name);
                view.setTag(bVar2);
                bVar = bVar2;
            } else {
                bVar = (b) view.getTag();
            }
            if (i == a.this.f) {
                bVar.b.setChecked(true);
            } else {
                bVar.b.setChecked(false);
            }
            bVar.b.setText(this.c[i]);
            String str = this.d[i];
            if (!(i + -1 >= 0 ? this.d[i - 1] : " ").equals(str)) {
                bVar.f1700a.setVisibility(0);
                bVar.f1700a.setText(str);
            } else {
                bVar.f1700a.setVisibility(8);
            }
            return view;
        }

        public int getPositionForSection(int i) {
            return this.e.get(this.f[i]).intValue();
        }

        public int getSectionForPosition(int i) {
            String str = this.d[i];
            for (int i2 = 0; i2 < this.f.length; i2++) {
                if (this.f[i2].equals(str)) {
                    return i2;
                }
            }
            return 0;
        }

        public Object[] getSections() {
            return this.f;
        }
    }
}
