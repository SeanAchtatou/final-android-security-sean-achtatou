package com.shoujiduoduo.ui.cailing;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import com.shoujiduoduo.a.a.c;
import com.shoujiduoduo.a.c.o;
import com.shoujiduoduo.a.c.p;
import com.shoujiduoduo.b.c.j;
import com.shoujiduoduo.base.bean.DDList;
import com.shoujiduoduo.base.bean.ListType;
import com.shoujiduoduo.base.bean.RingData;
import com.shoujiduoduo.player.PlayerService;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.ui.cailing.e;
import com.shoujiduoduo.ui.utils.d;
import com.shoujiduoduo.ui.utils.m;
import com.shoujiduoduo.util.aa;
import com.shoujiduoduo.util.ae;
import com.shoujiduoduo.util.al;
import com.shoujiduoduo.util.b.c;
import com.shoujiduoduo.util.g;

/* compiled from: CailingListAdapter */
public class b extends d {

    /* renamed from: a  reason: collision with root package name */
    boolean f1568a = false;
    public com.shoujiduoduo.util.b.b b = new com.shoujiduoduo.util.b.b() {
        public void a(c.b bVar) {
            b.this.c();
            String unused = b.this.e = b.this.d;
            com.shoujiduoduo.util.widget.d.a("已设置为默认彩铃");
            ae.c(b.this.i, "DEFAULT_CAILING_ID", b.this.e);
            b.this.notifyDataSetChanged();
            com.shoujiduoduo.a.a.c.a().b(com.shoujiduoduo.a.a.b.OBSERVER_RING_CHANGE, new c.a<p>() {
                public void a() {
                    RingData b = b.this.c.get(b.this.f);
                    if (b != null) {
                        ((p) this.f1284a).a(16, b);
                    }
                }
            });
        }

        public void b(c.b bVar) {
            String str;
            b.this.c();
            if (bVar.a().equals("999018") || bVar.a().equals("999019")) {
                new e(b.this.i, R.style.DuoDuoDialog, ae.a(RingDDApp.c(), "user_phone_num", ""), g.b.f2302a, new e.a() {
                    public void a(String str) {
                        com.shoujiduoduo.util.c.b.a().f(b.this.c.get(b.this.f).cid, b.this.b);
                    }
                }).show();
                return;
            }
            if (bVar != null) {
                str = bVar.b();
            } else {
                str = "对不起，彩铃设置失败。";
            }
            Toast.makeText(b.this.i, str, 1).show();
        }
    };
    /* access modifiers changed from: private */
    public j c;
    /* access modifiers changed from: private */
    public String d = "";
    /* access modifiers changed from: private */
    public String e = "";
    /* access modifiers changed from: private */
    public int f = -1;
    private LayoutInflater g;
    /* access modifiers changed from: private */
    public g.b h;
    /* access modifiers changed from: private */
    public Context i;
    /* access modifiers changed from: private */
    public boolean j;
    /* access modifiers changed from: private */
    public String k;
    private o l = new o() {
        public void a(String str, int i) {
            if (b.this.c != null && b.this.c.getListId().equals(str)) {
                com.shoujiduoduo.base.a.a.a("CailingListAdapter", "onSetPlay, listid:" + str);
                if (str.equals(b.this.c.getListId())) {
                    b.this.f1568a = true;
                    int unused = b.this.f = i;
                } else {
                    b.this.f1568a = false;
                }
                b.this.notifyDataSetChanged();
            }
        }

        public void b(String str, int i) {
            if (b.this.c != null && b.this.c.getListId().equals(str)) {
                com.shoujiduoduo.base.a.a.a("CailingListAdapter", "onCanclePlay, listId:" + str);
                b.this.f1568a = false;
                int unused = b.this.f = i;
                b.this.notifyDataSetChanged();
            }
        }

        public void a(String str, int i, int i2) {
            if (b.this.c != null && b.this.c.getListId().equals(str)) {
                com.shoujiduoduo.base.a.a.a("CailingListAdapter", "onStatusChange, listid:" + str);
                b.this.notifyDataSetChanged();
            }
        }
    };
    private View.OnClickListener m = new View.OnClickListener() {
        public void onClick(View view) {
            PlayerService b = aa.a().b();
            if (b == null) {
                return;
            }
            if (b.a() == 3) {
                b.n();
            } else {
                b.i();
            }
        }
    };
    private View.OnClickListener n = new View.OnClickListener() {
        public void onClick(View view) {
            PlayerService b = aa.a().b();
            if (b != null) {
                b.j();
            }
        }
    };
    private View.OnClickListener o = new View.OnClickListener() {
        public void onClick(View view) {
            PlayerService b = aa.a().b();
            if (b != null) {
                b.a(b.this.c, b.this.f);
            }
        }
    };
    private View.OnClickListener p = new View.OnClickListener() {
        public void onClick(View view) {
            new AlertDialog.Builder(b.this.i).setTitle("提示").setMessage("删除后将无法使用该彩铃，确定删除吗？").setPositiveButton("确认", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    com.shoujiduoduo.base.a.a.a("CailingListAdapter", "delete cailing");
                    b.this.a("正在删除...");
                    RingData b = b.this.c.get(b.this.f);
                    PlayerService b2 = aa.a().b();
                    if (b2 != null) {
                        b2.e();
                    }
                    if (b.this.h == g.b.f2302a) {
                        com.shoujiduoduo.util.c.b.a().g(b.cid, b.this.q);
                    } else if (b.this.h == g.b.ct) {
                        com.shoujiduoduo.util.d.b.a().e(ae.a(RingDDApp.c(), "pref_phone_num", ""), b.ctcid, b.this.q);
                    } else if (b.this.h == g.b.cu) {
                        com.shoujiduoduo.util.e.a.a().f(b.cucid, b.this.q);
                    }
                }
            }).setNegativeButton("取消", (DialogInterface.OnClickListener) null).show();
        }
    };
    /* access modifiers changed from: private */
    public com.shoujiduoduo.util.b.b q = new com.shoujiduoduo.util.b.b() {
        public void a(c.b bVar) {
            super.a(bVar);
            com.shoujiduoduo.a.a.c.a().b(new c.b() {
                public void a() {
                    b.this.c();
                }
            });
            com.shoujiduoduo.base.a.a.a("CailingListAdapter", "删除铃声成功，设置需要获取铃音库标识true");
            int unused = b.this.f = -1;
            ae.b(RingDDApp.c(), "NeedUpdateCaiLingLib", 1);
            com.shoujiduoduo.a.a.c.a().b(com.shoujiduoduo.a.a.b.OBSERVER_CAILING, new c.a<com.shoujiduoduo.a.c.c>() {
                public void a() {
                    ((com.shoujiduoduo.a.c.c) this.f1284a).b(g.s());
                }
            });
        }

        public void b(c.b bVar) {
            super.b(bVar);
            com.shoujiduoduo.a.a.c.a().b(new c.b() {
                public void a() {
                    b.this.c();
                    new AlertDialog.Builder(b.this.i).setTitle("").setMessage("操作失败，请稍后再试").setPositiveButton("确认", (DialogInterface.OnClickListener) null).show();
                }
            });
        }
    };
    private View.OnClickListener r = new View.OnClickListener() {
        public void onClick(View view) {
            com.shoujiduoduo.base.a.a.a("CailingListAdapter", "set default cailing");
            b.this.a("正在设置...");
            RingData b = b.this.c.get(b.this.f);
            if (b != null) {
                if (b.this.h.equals(g.b.f2302a)) {
                    String unused = b.this.d = b.cid;
                    com.shoujiduoduo.util.c.b.a().f(b.cid, b.this.b);
                } else if (b.this.h.equals(g.b.ct)) {
                    String unused2 = b.this.d = b.ctcid;
                    com.shoujiduoduo.util.d.b.a().c(ae.a(RingDDApp.c(), "pref_phone_num"), b.ctcid, b.this.b);
                } else {
                    String unused3 = b.this.d = b.cucid;
                    com.shoujiduoduo.util.e.a.a().a(b.cucid, b.this.j, b.this.k, b.this.b);
                }
            }
        }
    };
    private View.OnClickListener s = new View.OnClickListener() {
        public void onClick(View view) {
            ListType.LIST_TYPE list_type;
            com.shoujiduoduo.base.a.a.a("CailingListAdapter", "give cailing");
            RingData b = b.this.c.get(b.this.f);
            if (b != null) {
                Bundle bundle = new Bundle();
                Intent intent = new Intent(b.this.i, GiveCailingActivity.class);
                bundle.putParcelable("ringdata", b);
                intent.putExtras(bundle);
                intent.putExtra("listid", "cailingmanage");
                if (g.t()) {
                    intent.putExtra("operator_type", 0);
                    list_type = ListType.LIST_TYPE.list_ring_cmcc;
                } else if (g.v()) {
                    intent.putExtra("operator_type", 1);
                    list_type = ListType.LIST_TYPE.list_ring_ctcc;
                } else {
                    list_type = ListType.LIST_TYPE.list_ring_cmcc;
                }
                intent.putExtra("listtype", list_type.toString());
                b.this.i.startActivity(intent);
                al.a(b.rid, 7, "&cucid=" + b.cucid);
            }
        }
    };
    private ProgressDialog t = null;

    public b(Context context) {
        this.i = context;
        this.g = LayoutInflater.from(context);
    }

    public void a() {
        com.shoujiduoduo.a.a.c.a().a(com.shoujiduoduo.a.a.b.OBSERVER_PLAY_STATUS, this.l);
    }

    public void b() {
        com.shoujiduoduo.a.a.c.a().b(com.shoujiduoduo.a.a.b.OBSERVER_PLAY_STATUS, this.l);
    }

    private void d() {
        com.shoujiduoduo.base.a.a.a("CailingListAdapter", "begin queryUserRingBox");
        if (this.h.equals(g.b.f2302a)) {
            com.shoujiduoduo.util.c.b.a().h("", new a());
        } else if (this.h.equals(g.b.ct)) {
            com.shoujiduoduo.util.d.b.a().f(ae.a(RingDDApp.c(), "pref_phone_num"), new a());
        } else if (this.h.equals(g.b.cu)) {
            com.shoujiduoduo.util.e.a.a().h(new a());
        }
    }

    /* compiled from: CailingListAdapter */
    private class a extends com.shoujiduoduo.util.b.b {
        private a() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.shoujiduoduo.ui.cailing.b.a(com.shoujiduoduo.ui.cailing.b, boolean):boolean
         arg types: [com.shoujiduoduo.ui.cailing.b, int]
         candidates:
          com.shoujiduoduo.ui.cailing.b.a(com.shoujiduoduo.ui.cailing.b, int):int
          com.shoujiduoduo.ui.cailing.b.a(com.shoujiduoduo.ui.cailing.b, java.lang.String):java.lang.String
          com.shoujiduoduo.ui.cailing.b.a(android.view.View, int):void
          com.shoujiduoduo.ui.cailing.b.a(com.shoujiduoduo.ui.cailing.b, boolean):boolean */
        public void a(c.b bVar) {
            if (b.this.h.equals(g.b.cu)) {
                if (bVar != null && (bVar instanceof c.s)) {
                    c.s sVar = (c.s) bVar;
                    if (sVar.d != null) {
                        for (int i = 0; i < sVar.d.length; i++) {
                            if (sVar.d[i].c.equals("0")) {
                                boolean unused = b.this.j = true;
                                String unused2 = b.this.k = sVar.d[i].f2224a;
                                String unused3 = b.this.e = sVar.d[i].d;
                                ae.c(b.this.i, "DEFAULT_CAILING_ID", b.this.e);
                                b.this.notifyDataSetChanged();
                                com.shoujiduoduo.base.a.a.a("CailingListAdapter", "default cucc cailing id:" + b.this.e);
                                return;
                            }
                        }
                    }
                }
            } else if (bVar != null && (bVar instanceof c.z)) {
                c.z zVar = (c.z) bVar;
                if (zVar.a() == null || zVar.d() == null || zVar.d().size() <= 0) {
                    com.shoujiduoduo.base.a.a.c("CailingListAdapter", "查询默认彩铃失败，code:" + bVar.a() + " msg:" + bVar.b());
                    return;
                }
                String unused4 = b.this.e = zVar.d().get(0).b();
                ae.c(b.this.i, "DEFAULT_CAILING_ID", b.this.e);
                b.this.notifyDataSetChanged();
                com.shoujiduoduo.base.a.a.a("CailingListAdapter", "default cailing id:" + b.this.e);
            }
        }

        public void b(c.b bVar) {
            com.shoujiduoduo.base.a.a.c("CailingListAdapter", "查询默认彩铃失败，code:" + bVar.a() + " msg:" + bVar.b());
        }
    }

    public int getCount() {
        if (this.c == null) {
            return 0;
        }
        return this.c.size();
    }

    public Object getItem(int i2) {
        if (this.c != null && i2 >= 0 && i2 < this.c.size()) {
            return this.c.get(i2);
        }
        return null;
    }

    public long getItemId(int i2) {
        return (long) i2;
    }

    private void a(View view, int i2) {
        RingData b2 = this.c.get(i2);
        TextView textView = (TextView) m.a(view, R.id.cailing_item_valid_date);
        TextView textView2 = (TextView) m.a(view, R.id.cailing_item_default_tip);
        ((TextView) m.a(view, R.id.cailing_item_song_name)).setText(b2.name);
        ((TextView) m.a(view, R.id.cailing_item_artist)).setText(b2.artist);
        String str = "";
        String str2 = "";
        if (this.h.equals(g.b.f2302a)) {
            str = b2.valid;
            str2 = b2.cid;
        } else if (this.h.equals(g.b.ct)) {
            str = b2.ctvalid;
            str2 = b2.ctcid;
        } else if (this.h.equals(g.b.cu)) {
            str = b2.cuvalid;
            str2 = b2.cucid;
        }
        textView.setText(String.format("有效期:" + str, new Object[0]));
        if (str.equals("")) {
            textView.setVisibility(4);
        } else {
            textView.setVisibility(0);
        }
        textView2.setText("当前彩铃");
        this.e = ae.a(RingDDApp.c(), "DEFAULT_CAILING_ID", "");
        if (str2.equals(this.e)) {
            textView2.setVisibility(0);
        } else {
            textView2.setVisibility(8);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View getView(int i2, View view, ViewGroup viewGroup) {
        if (this.c == null) {
            return null;
        }
        if (i2 >= this.c.size()) {
            return view;
        }
        if (view == null) {
            view = this.g.inflate((int) R.layout.listitem_cailing_manage, viewGroup, false);
        }
        a(view, i2);
        ProgressBar progressBar = (ProgressBar) m.a(view, R.id.cailing_item_download_progress);
        TextView textView = (TextView) m.a(view, R.id.cailing_item_serial_number);
        ImageButton imageButton = (ImageButton) m.a(view, R.id.cailing_item_play);
        ImageButton imageButton2 = (ImageButton) m.a(view, R.id.cailing_item_pause);
        ImageButton imageButton3 = (ImageButton) m.a(view, R.id.cailing_item_failed);
        imageButton3.setOnClickListener(this.o);
        imageButton.setOnClickListener(this.m);
        imageButton2.setOnClickListener(this.n);
        if (i2 != this.f || !this.f1568a) {
            ((Button) m.a(view, R.id.cailing_item_set_default)).setVisibility(8);
            ((Button) m.a(view, R.id.cailing_item_give)).setVisibility(8);
            ((Button) m.a(view, R.id.cailing_item_delete)).setVisibility(8);
            textView.setText(Integer.toString(i2 + 1));
            textView.setVisibility(0);
            progressBar.setVisibility(4);
            imageButton.setVisibility(4);
            imageButton2.setVisibility(4);
            imageButton3.setVisibility(4);
            return view;
        }
        Button button = (Button) m.a(view, R.id.cailing_item_set_default);
        Button button2 = (Button) m.a(view, R.id.cailing_item_give);
        Button button3 = (Button) m.a(view, R.id.cailing_item_delete);
        button.setVisibility(0);
        button2.setVisibility(8);
        button3.setVisibility(0);
        button.setOnClickListener(this.r);
        button2.setOnClickListener(this.s);
        button3.setOnClickListener(this.p);
        textView.setVisibility(4);
        progressBar.setVisibility(4);
        imageButton.setVisibility(4);
        imageButton2.setVisibility(4);
        imageButton3.setVisibility(4);
        PlayerService b2 = aa.a().b();
        if (b2 == null) {
            return view;
        }
        switch (b2.a()) {
            case 1:
                progressBar.setVisibility(0);
                return view;
            case 2:
                imageButton2.setVisibility(0);
                return view;
            case 3:
            case 4:
            case 5:
                imageButton.setVisibility(0);
                return view;
            case 6:
                imageButton3.setVisibility(0);
                return view;
            default:
                return view;
        }
    }

    /* access modifiers changed from: package-private */
    public void a(String str) {
        if (this.t == null) {
            this.t = new ProgressDialog(this.i);
            this.t.setMessage(str);
            this.t.setIndeterminate(false);
            this.t.setCancelable(false);
            this.t.show();
        }
    }

    /* access modifiers changed from: package-private */
    public void c() {
        if (this.t != null) {
            this.t.dismiss();
            this.t = null;
        }
    }

    public void a(boolean z) {
    }

    public void a(DDList dDList) {
        if (this.c != dDList) {
            this.c = (j) dDList;
            notifyDataSetChanged();
        }
        if (dDList.getListId().equals("cmcc_cailing")) {
            this.h = g.b.f2302a;
        } else if (dDList.getListId().equals("ctcc_cailing")) {
            this.h = g.b.ct;
        } else if (dDList.getListId().equals("cucc_cailing")) {
            this.h = g.b.cu;
        }
        d();
    }
}
