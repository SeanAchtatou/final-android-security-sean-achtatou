package com.shoujiduoduo.ui.cailing;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.database.ContentObserver;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import cn.banshenggua.aichang.utils.Constants;
import com.ffcs.inapppaylib.EMPHelper;
import com.shoujiduoduo.a.a.c;
import com.shoujiduoduo.a.c.v;
import com.shoujiduoduo.b.a.e;
import com.shoujiduoduo.base.bean.UserInfo;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ui.cailing.e;
import com.shoujiduoduo.ui.utils.BaseActivity;
import com.shoujiduoduo.util.af;
import com.shoujiduoduo.util.ah;
import com.shoujiduoduo.util.b.c;
import com.shoujiduoduo.util.d.b;
import com.shoujiduoduo.util.g;
import com.shoujiduoduo.util.i;
import com.shoujiduoduo.util.t;
import com.shoujiduoduo.util.widget.d;
import com.sina.weibo.sdk.exception.WeiboAuthException;
import com.umeng.b.a;
import java.util.List;

public class TestCtcc extends BaseActivity implements View.OnClickListener {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public String f1498a = "15313739485";
    private String b = "810027210086";
    private EditText c;
    /* access modifiers changed from: private */
    public EditText d;
    private ContentObserver e;
    /* access modifiers changed from: private */
    public CheckBox f;
    private e g = new e();
    private EMPHelper h;
    private String i = "";
    private boolean j = false;
    private Handler k = new Handler() {
        public void handleMessage(Message message) {
            switch (message.what) {
                case 910:
                    d.a("退订成功");
                    break;
                case 912:
                    d.a("退订失败");
                    break;
            }
            super.handleMessage(message);
        }
    };

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        String str;
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_test_ctcc);
        this.c = (EditText) findViewById(R.id.music_id);
        this.c.setText(this.b);
        this.d = (EditText) findViewById(R.id.phone_num);
        this.d.setText(this.f1498a);
        findViewById(R.id.query_vip_state).setOnClickListener(this);
        findViewById(R.id.open_vip).setOnClickListener(this);
        findViewById(R.id.emp_open_vip).setOnClickListener(this);
        findViewById(R.id.vip_order).setOnClickListener(this);
        findViewById(R.id.sms_random_key).setOnClickListener(this);
        findViewById(R.id.close_vip).setOnClickListener(this);
        findViewById(R.id.emp_close_vip).setOnClickListener(this);
        findViewById(R.id.emp_launch).setOnClickListener(this);
        findViewById(R.id.query3rd_phone).setOnClickListener(this);
        findViewById(R.id.query3rd_uid).setOnClickListener(this);
        findViewById(R.id.check_base_cailing_status).setOnClickListener(this);
        findViewById(R.id.find_mdn_by_imsi).setOnClickListener(this);
        findViewById(R.id.emp_one_key_open).setOnClickListener(this);
        findViewById(R.id.sms_random_key_common).setOnClickListener(this);
        findViewById(R.id.query_caililng_and_vip).setOnClickListener(this);
        findViewById(R.id.diy_clip_upload).setOnClickListener(this);
        findViewById(R.id.set_diy_ring).setOnClickListener(this);
        findViewById(R.id.get_diy_ring).setOnClickListener(this);
        findViewById(R.id.query_ring_info).setOnClickListener(this);
        findViewById(R.id.query_cailing_url).setOnClickListener(this);
        findViewById(R.id.query_zhenling_url).setOnClickListener(this);
        findViewById(R.id.query_diy_ring_status).setOnClickListener(this);
        findViewById(R.id.query_ring_box).setOnClickListener(this);
        findViewById(R.id.query_default_ring).setOnClickListener(this);
        this.f = (CheckBox) findViewById(R.id.checkbox);
        this.f.setChecked(true);
        if (!TextUtils.isEmpty(a.a().a(this, "ctcc_num_launch"))) {
            str = a.a().a(this, "ctcc_num_launch");
        } else {
            str = "1065987320001";
        }
        this.h = EMPHelper.getInstance(this);
        this.h.init("1000010404421", "5297", "x4lRWHcgRqfH", this.k, 15000);
        this.e = new af(this, new Handler(), (EditText) findViewById(R.id.random_key), str);
        getContentResolver().registerContentObserver(Uri.parse("content://sms/"), true, this.e);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        getContentResolver().unregisterContentObserver(this.e);
        super.onDestroy();
    }

    public void onClick(View view) {
        this.f1498a = this.d.getText().toString();
        if (ah.c(this.f1498a)) {
            d.a("请输入手机号");
            a(this.f1498a, g.b.ct);
            return;
        }
        switch (view.getId()) {
            case R.id.check_base_cailing_status:
                if (!TextUtils.isEmpty(this.d.getText().toString())) {
                    this.f1498a = this.d.getText().toString();
                }
                b.a().g(this.f1498a, new com.shoujiduoduo.util.b.b() {
                    public void a(c.b bVar) {
                        super.a(bVar);
                        com.shoujiduoduo.base.a.a.a("testctcc", "openCheck onSuccess:" + bVar.toString());
                        TestCtcc.this.a(bVar);
                    }

                    public void b(c.b bVar) {
                        super.b(bVar);
                        com.shoujiduoduo.base.a.a.a("testctcc", "openCheck onFailure:" + bVar.toString());
                        TestCtcc.this.a(bVar);
                    }
                });
                return;
            case R.id.query_vip_state:
                b.a().a(this.f1498a, this.f.isChecked(), "", new com.shoujiduoduo.util.b.b() {
                    public void a(c.b bVar) {
                        super.a(bVar);
                        com.shoujiduoduo.base.a.a.a("testctcc", new StringBuilder().append("query vip onSuccess:").append(bVar).toString() != null ? bVar.toString() : "result is null");
                        if (bVar != null && (bVar instanceof c.v)) {
                            c.v vVar = (c.v) bVar;
                            com.shoujiduoduo.base.a.a.a("testctcc", "code:" + vVar.a() + " msg:" + vVar.b());
                            UserInfo c = com.shoujiduoduo.a.b.b.g().c();
                            if (vVar.d) {
                                c.setVipType(2);
                                new AlertDialog.Builder(TestCtcc.this).setMessage("开通状态").setPositiveButton("确定", (DialogInterface.OnClickListener) null).show();
                                return;
                            }
                            c.setVipType(0);
                            new AlertDialog.Builder(TestCtcc.this).setMessage("未开通状态， status：" + WeiboAuthException.DEFAULT_AUTH_ERROR_CODE).setPositiveButton("确定", (DialogInterface.OnClickListener) null).show();
                        }
                    }

                    public void b(c.b bVar) {
                        com.shoujiduoduo.base.a.a.a("testctcc", "query vip onFailure:" + bVar.toString());
                        new AlertDialog.Builder(TestCtcc.this).setMessage(bVar.toString()).setPositiveButton("确定", (DialogInterface.OnClickListener) null).show();
                        super.b(bVar);
                    }
                });
                return;
            case R.id.query_caililng_and_vip:
                b.a().a(this.f1498a, new com.shoujiduoduo.util.b.b() {
                    public void a(c.b bVar) {
                        super.a(bVar);
                        com.shoujiduoduo.base.a.a.a("testctcc", new StringBuilder().append("checkCailingAndVip onSuccess:").append(bVar).toString() != null ? bVar.toString() : "result is null");
                        c.e eVar = (c.e) bVar;
                        new AlertDialog.Builder(TestCtcc.this).setMessage("彩铃状态：" + eVar.d() + " , vip状态:" + eVar.e() + ", diy状态：" + eVar.f()).setPositiveButton("确定", (DialogInterface.OnClickListener) null).show();
                    }

                    public void b(c.b bVar) {
                        super.b(bVar);
                        com.shoujiduoduo.base.a.a.a("testctcc", "checkCailingAndVip onFailure:" + bVar.toString());
                    }
                });
                return;
            case R.id.vip_order:
                if (!TextUtils.isEmpty(this.c.getText().toString())) {
                    this.b = this.c.getText().toString();
                }
                if (!TextUtils.isEmpty(this.d.getText().toString())) {
                    this.f1498a = this.d.getText().toString();
                }
                b.a().a(this.f1498a, this.b, "", new com.shoujiduoduo.util.b.b() {
                    public void a(c.b bVar) {
                        super.a(bVar);
                        com.shoujiduoduo.base.a.a.a("testctcc", "vipOrder onSuccess:" + bVar.toString());
                        new AlertDialog.Builder(TestCtcc.this).setMessage(bVar.toString()).setPositiveButton("确定", (DialogInterface.OnClickListener) null).show();
                    }

                    public void b(c.b bVar) {
                        com.shoujiduoduo.base.a.a.a("testctcc", "vipOrder onFailure:" + bVar.toString());
                        new AlertDialog.Builder(TestCtcc.this).setMessage(bVar.toString()).setPositiveButton("确定", (DialogInterface.OnClickListener) null).show();
                        super.b(bVar);
                    }
                });
                return;
            case R.id.open_vip:
                if (!TextUtils.isEmpty(this.d.getText().toString())) {
                    this.f1498a = this.d.getText().toString();
                }
                new AlertDialog.Builder(this).setMessage("确定要开通包月？").setNegativeButton("取消", (DialogInterface.OnClickListener) null).setPositiveButton("确定", new DialogInterface.OnClickListener() {
                    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                     method: com.shoujiduoduo.util.d.b.a(java.lang.String, boolean, java.lang.String, boolean, java.lang.String, com.shoujiduoduo.util.b.b):void
                     arg types: [java.lang.String, int, ?[OBJECT, ARRAY], boolean, java.lang.String, com.shoujiduoduo.ui.cailing.TestCtcc$24$1]
                     candidates:
                      com.shoujiduoduo.util.d.b.a(java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, com.shoujiduoduo.util.b.b):void
                      com.shoujiduoduo.util.d.b.a(java.lang.String, boolean, java.lang.String, boolean, java.lang.String, com.shoujiduoduo.util.b.b):void */
                    public void onClick(DialogInterface dialogInterface, int i) {
                        b.a().a(TestCtcc.this.f1498a, false, (String) null, TestCtcc.this.f.isChecked(), "", (com.shoujiduoduo.util.b.b) new com.shoujiduoduo.util.b.b() {
                            public void a(c.b bVar) {
                                super.a(bVar);
                                com.shoujiduoduo.base.a.a.a("testctcc", "openVip onSuccess:" + bVar.toString());
                                new AlertDialog.Builder(TestCtcc.this).setMessage(bVar.toString()).setPositiveButton("确定", (DialogInterface.OnClickListener) null).show();
                            }

                            public void b(c.b bVar) {
                                com.shoujiduoduo.base.a.a.a("testctcc", "openVip onFailure:" + bVar.toString());
                                new AlertDialog.Builder(TestCtcc.this).setMessage(bVar.toString()).setPositiveButton("确定", (DialogInterface.OnClickListener) null).show();
                                super.b(bVar);
                            }
                        });
                    }
                }).show();
                return;
            case R.id.close_vip:
                if (!TextUtils.isEmpty(this.d.getText().toString())) {
                    this.f1498a = this.d.getText().toString();
                }
                new AlertDialog.Builder(this).setMessage("确定关闭包月？").setNegativeButton("取消", (DialogInterface.OnClickListener) null).setPositiveButton("确定", new DialogInterface.OnClickListener() {
                    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                     method: com.shoujiduoduo.util.d.b.a(java.lang.String, boolean, boolean, com.shoujiduoduo.util.b.b):void
                     arg types: [java.lang.String, int, boolean, com.shoujiduoduo.ui.cailing.TestCtcc$6$1]
                     candidates:
                      com.shoujiduoduo.util.d.b.a(com.shoujiduoduo.util.d.b, java.lang.String, com.shoujiduoduo.util.b.c$b, java.lang.String):void
                      com.shoujiduoduo.util.d.b.a(java.util.List<org.apache.http.NameValuePair>, java.lang.String, com.shoujiduoduo.util.b.b, com.shoujiduoduo.util.d.b$a):void
                      com.shoujiduoduo.util.d.b.a(java.lang.String, java.lang.String, java.lang.String, com.shoujiduoduo.util.b.b):void
                      com.shoujiduoduo.util.d.b.a(java.lang.String, boolean, java.lang.String, com.shoujiduoduo.util.b.b):void
                      com.shoujiduoduo.util.d.b.a(java.lang.String, boolean, boolean, com.shoujiduoduo.util.b.b):void */
                    public void onClick(DialogInterface dialogInterface, int i) {
                        b.a().a(TestCtcc.this.f1498a, false, TestCtcc.this.f.isChecked(), (com.shoujiduoduo.util.b.b) new com.shoujiduoduo.util.b.b() {
                            public void a(c.b bVar) {
                                com.shoujiduoduo.base.a.a.a("testctcc", "closeVip onFailure:" + bVar.toString());
                                new AlertDialog.Builder(TestCtcc.this).setMessage(bVar.toString()).setPositiveButton("确定", (DialogInterface.OnClickListener) null).show();
                                super.a(bVar);
                            }

                            public void b(c.b bVar) {
                                com.shoujiduoduo.base.a.a.a("testctcc", "closeVip onFailure:" + bVar.toString());
                                new AlertDialog.Builder(TestCtcc.this).setMessage(bVar.toString()).setPositiveButton("确定", (DialogInterface.OnClickListener) null).show();
                                super.b(bVar);
                            }
                        });
                    }
                }).show();
                return;
            case R.id.get_sms_code:
            case R.id.query_ring_circle:
            case R.id.emp_operate_layout:
            default:
                return;
            case R.id.query_cailing_url:
                b.a().h("810027214935", new com.shoujiduoduo.util.b.b());
                return;
            case R.id.query_ring_box:
                b.a().e(this.f1498a, new com.shoujiduoduo.util.b.b() {
                    public void a(c.b bVar) {
                        super.a(bVar);
                        List<c.ae> d = ((c.z) bVar).d();
                        if (d != null) {
                            StringBuilder sb = new StringBuilder();
                            for (c.ae next : d) {
                                sb.append(next.d()).append("-").append(next.c());
                                sb.append(" | ");
                            }
                            TestCtcc.this.a(bVar, sb.toString());
                            return;
                        }
                        TestCtcc.this.a(bVar, "查询不到彩铃库");
                    }

                    public void b(c.b bVar) {
                        super.b(bVar);
                        TestCtcc.this.a(bVar);
                    }
                });
                return;
            case R.id.query_default_ring:
                b.a().f(this.f1498a, new com.shoujiduoduo.util.b.b() {
                    public void a(c.b bVar) {
                        super.a(bVar);
                        TestCtcc.this.a(bVar, "彩铃id：" + ((c.z) bVar).d().get(0).b());
                    }

                    public void b(c.b bVar) {
                        super.b(bVar);
                        TestCtcc.this.a(bVar);
                    }
                });
                return;
            case R.id.find_mdn_by_imsi:
                String h2 = g.h();
                if (!TextUtils.isEmpty(h2)) {
                    b.a().b(h2, new com.shoujiduoduo.util.b.b() {
                        public void a(c.b bVar) {
                            super.a(bVar);
                            com.shoujiduoduo.base.a.a.a("testctcc", "findMdnByImsi onSuccess:" + bVar.toString());
                            new AlertDialog.Builder(TestCtcc.this).setMessage(bVar.toString()).setPositiveButton("确定", (DialogInterface.OnClickListener) null).show();
                        }

                        public void b(c.b bVar) {
                            super.b(bVar);
                            com.shoujiduoduo.base.a.a.a("testctcc", "findMdnByImsi onFailure:" + bVar.toString());
                            new AlertDialog.Builder(TestCtcc.this).setMessage(bVar.toString()).setPositiveButton("确定", (DialogInterface.OnClickListener) null).show();
                        }
                    });
                    return;
                }
                return;
            case R.id.diy_clip_upload:
                b.a().a("剪辑铃声", this.f1498a, "http://www.shoujiduoduo.com/data/test.wav ", "", new com.shoujiduoduo.util.b.b() {
                    public void a(c.b bVar) {
                        super.a(bVar);
                        com.shoujiduoduo.base.a.a.a("testctcc", "diy_clip_upload onSuccess:" + bVar.toString());
                        new AlertDialog.Builder(TestCtcc.this).setMessage(bVar.toString()).setPositiveButton("确定", (DialogInterface.OnClickListener) null).show();
                    }

                    public void b(c.b bVar) {
                        super.b(bVar);
                        com.shoujiduoduo.base.a.a.a("testctcc", "diy_clip_upload onFailure:" + bVar.toString());
                        new AlertDialog.Builder(TestCtcc.this).setMessage(bVar.toString()).setPositiveButton("确定", (DialogInterface.OnClickListener) null).show();
                    }
                });
                return;
            case R.id.set_diy_ring:
                b.a().b("588909", this.f1498a, "", new com.shoujiduoduo.util.b.b() {
                    public void a(c.b bVar) {
                        super.a(bVar);
                        com.shoujiduoduo.base.a.a.a("testctcc", "setDiyRing onSuccess:" + bVar.toString());
                        new AlertDialog.Builder(TestCtcc.this).setMessage(bVar.toString()).setPositiveButton("确定", (DialogInterface.OnClickListener) null).show();
                    }

                    public void b(c.b bVar) {
                        super.b(bVar);
                        com.shoujiduoduo.base.a.a.a("testctcc", "setDiyRing onFailure:" + bVar.toString());
                        new AlertDialog.Builder(TestCtcc.this).setMessage(bVar.toString()).setPositiveButton("确定", (DialogInterface.OnClickListener) null).show();
                    }
                });
                return;
            case R.id.get_diy_ring:
                b.a().c(this.f1498a, new com.shoujiduoduo.util.b.b() {
                    public void a(c.b bVar) {
                        super.a(bVar);
                        com.shoujiduoduo.base.a.a.a("testctcc", "getDiyRing onSuccess:" + bVar.toString());
                        new AlertDialog.Builder(TestCtcc.this).setMessage(bVar.toString()).setPositiveButton("确定", (DialogInterface.OnClickListener) null).show();
                    }

                    public void b(c.b bVar) {
                        super.b(bVar);
                        com.shoujiduoduo.base.a.a.a("testctcc", "getDiyRing onFailure:" + bVar.toString());
                        new AlertDialog.Builder(TestCtcc.this).setMessage(bVar.toString()).setPositiveButton("确定", (DialogInterface.OnClickListener) null).show();
                    }
                });
                return;
            case R.id.query_diy_ring_status:
                b.a().b("810079618974", this.f1498a, new com.shoujiduoduo.util.b.b() {
                    public void a(c.b bVar) {
                        super.a(bVar);
                        com.shoujiduoduo.base.a.a.a("testctcc", "queryDiyStatus onSuccess:" + bVar.toString());
                        new AlertDialog.Builder(TestCtcc.this).setMessage(bVar.toString()).setPositiveButton("确定", (DialogInterface.OnClickListener) null).show();
                    }

                    public void b(c.b bVar) {
                        super.b(bVar);
                        com.shoujiduoduo.base.a.a.a("testctcc", "queryDiyStatus onFailure:" + bVar.toString());
                        new AlertDialog.Builder(TestCtcc.this).setMessage(bVar.toString()).setPositiveButton("确定", (DialogInterface.OnClickListener) null).show();
                    }
                });
                return;
            case R.id.sms_random_key:
                if (!TextUtils.isEmpty(this.d.getText().toString())) {
                    this.f1498a = this.d.getText().toString();
                }
                b.a().a(this.f1498a, "铃声多多自定义短信验证码：", new com.shoujiduoduo.util.b.b() {
                    public void a(c.b bVar) {
                        com.shoujiduoduo.base.a.a.a("testctcc", "sendAuthRandomKey onSuccess:" + bVar.toString());
                        new AlertDialog.Builder(TestCtcc.this).setMessage(bVar.toString()).setPositiveButton("确定", (DialogInterface.OnClickListener) null).show();
                        super.a(bVar);
                    }

                    public void b(c.b bVar) {
                        com.shoujiduoduo.base.a.a.a("testctcc", "sendAuthRandomKey onFailure:" + bVar.toString());
                        new AlertDialog.Builder(TestCtcc.this).setMessage(bVar.toString()).setPositiveButton("确定", (DialogInterface.OnClickListener) null).show();
                        super.b(bVar);
                    }
                });
                return;
            case R.id.sms_random_key_common:
                if (!TextUtils.isEmpty(this.d.getText().toString())) {
                    this.f1498a = this.d.getText().toString();
                }
                b.a().d(this.f1498a, new com.shoujiduoduo.util.b.b() {
                    public void a(c.b bVar) {
                        com.shoujiduoduo.base.a.a.a("testctcc", "getValidateCode onSuccess:" + bVar.toString());
                        new AlertDialog.Builder(TestCtcc.this).setMessage(bVar.toString()).setPositiveButton("确定", (DialogInterface.OnClickListener) null).show();
                        super.a(bVar);
                    }

                    public void b(c.b bVar) {
                        com.shoujiduoduo.base.a.a.a("testctcc", "getValidateCode onFailure:" + bVar.toString());
                        new AlertDialog.Builder(TestCtcc.this).setMessage(bVar.toString()).setPositiveButton("确定", (DialogInterface.OnClickListener) null).show();
                        super.b(bVar);
                    }
                });
                return;
            case R.id.query_ring_info:
                b.a().j("810027214935", new com.shoujiduoduo.util.b.b() {
                    public void a(c.b bVar) {
                        super.a(bVar);
                        new AlertDialog.Builder(TestCtcc.this).setMessage(bVar.toString()).setPositiveButton("确定", (DialogInterface.OnClickListener) null).show();
                    }

                    public void b(c.b bVar) {
                        super.b(bVar);
                        new AlertDialog.Builder(TestCtcc.this).setMessage(bVar.toString()).setPositiveButton("确定", (DialogInterface.OnClickListener) null).show();
                    }
                });
                return;
            case R.id.query_zhenling_url:
                b.a().i("810027214935", new com.shoujiduoduo.util.b.b());
                return;
            case R.id.emp_launch:
                b.a().a(this.f1498a, this.f.isChecked(), new com.shoujiduoduo.util.b.b() {
                    public void a(c.b bVar) {
                        super.a(bVar);
                        if (bVar != null && (bVar instanceof c.o)) {
                            com.shoujiduoduo.base.a.a.a("testctcc", "fee_type:" + ((c.o) bVar).d());
                            com.shoujiduoduo.base.a.a.a("testctcc", "launchVip onSuccess:" + bVar.toString());
                        }
                        TestCtcc.this.a(bVar);
                    }

                    public void b(c.b bVar) {
                        super.b(bVar);
                        com.shoujiduoduo.base.a.a.a("testctcc", "launchVip onFailure:" + bVar.toString());
                        TestCtcc.this.a(bVar);
                    }
                });
                return;
            case R.id.emp_open_vip:
                if (!TextUtils.isEmpty(this.d.getText().toString())) {
                    this.f1498a = this.d.getText().toString();
                }
                final String obj = ((EditText) findViewById(R.id.random_key)).getText().toString();
                if (TextUtils.isEmpty(obj)) {
                    d.a("验证码不正确");
                    return;
                } else {
                    new AlertDialog.Builder(this).setMessage("确定要开通包月？").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                         method: com.shoujiduoduo.util.d.b.a(java.lang.String, boolean, java.lang.String, boolean, java.lang.String, com.shoujiduoduo.util.b.b):void
                         arg types: [java.lang.String, int, java.lang.String, boolean, java.lang.String, com.shoujiduoduo.ui.cailing.TestCtcc$25$1]
                         candidates:
                          com.shoujiduoduo.util.d.b.a(java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, com.shoujiduoduo.util.b.b):void
                          com.shoujiduoduo.util.d.b.a(java.lang.String, boolean, java.lang.String, boolean, java.lang.String, com.shoujiduoduo.util.b.b):void */
                        public void onClick(DialogInterface dialogInterface, int i) {
                            b.a().a(TestCtcc.this.f1498a, true, obj, TestCtcc.this.f.isChecked(), "", (com.shoujiduoduo.util.b.b) new com.shoujiduoduo.util.b.b() {
                                public void a(c.b bVar) {
                                    super.a(bVar);
                                    com.shoujiduoduo.base.a.a.a("testctcc", "openVip onSuccess:" + bVar.toString());
                                    new AlertDialog.Builder(TestCtcc.this).setMessage(bVar.toString()).setPositiveButton("确定", (DialogInterface.OnClickListener) null).show();
                                    TestCtcc.this.a();
                                }

                                public void b(c.b bVar) {
                                    com.shoujiduoduo.base.a.a.a("testctcc", "openVip onFailure:" + bVar.toString());
                                    new AlertDialog.Builder(TestCtcc.this).setMessage(bVar.toString()).setPositiveButton("确定", (DialogInterface.OnClickListener) null).show();
                                    super.b(bVar);
                                }
                            });
                        }
                    }).setNegativeButton("取消", (DialogInterface.OnClickListener) null).show();
                    return;
                }
            case R.id.emp_close_vip:
                if (!TextUtils.isEmpty(this.d.getText().toString())) {
                    this.f1498a = this.d.getText().toString();
                }
                new AlertDialog.Builder(this).setMessage("确定关闭包月？").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                     method: com.shoujiduoduo.util.d.b.a(java.lang.String, boolean, boolean, com.shoujiduoduo.util.b.b):void
                     arg types: [java.lang.String, int, boolean, com.shoujiduoduo.ui.cailing.TestCtcc$7$1]
                     candidates:
                      com.shoujiduoduo.util.d.b.a(com.shoujiduoduo.util.d.b, java.lang.String, com.shoujiduoduo.util.b.c$b, java.lang.String):void
                      com.shoujiduoduo.util.d.b.a(java.util.List<org.apache.http.NameValuePair>, java.lang.String, com.shoujiduoduo.util.b.b, com.shoujiduoduo.util.d.b$a):void
                      com.shoujiduoduo.util.d.b.a(java.lang.String, java.lang.String, java.lang.String, com.shoujiduoduo.util.b.b):void
                      com.shoujiduoduo.util.d.b.a(java.lang.String, boolean, java.lang.String, com.shoujiduoduo.util.b.b):void
                      com.shoujiduoduo.util.d.b.a(java.lang.String, boolean, boolean, com.shoujiduoduo.util.b.b):void */
                    public void onClick(DialogInterface dialogInterface, int i) {
                        b.a().a(TestCtcc.this.f1498a, true, TestCtcc.this.f.isChecked(), (com.shoujiduoduo.util.b.b) new com.shoujiduoduo.util.b.b() {
                            public void a(c.b bVar) {
                                com.shoujiduoduo.base.a.a.a("testctcc", "closeVip onFailure:" + bVar.toString());
                                new AlertDialog.Builder(TestCtcc.this).setMessage(bVar.toString()).setPositiveButton("确定", (DialogInterface.OnClickListener) null).show();
                                super.a(bVar);
                            }

                            public void b(c.b bVar) {
                                com.shoujiduoduo.base.a.a.a("testctcc", "closeVip onFailure:" + bVar.toString());
                                new AlertDialog.Builder(TestCtcc.this).setMessage(bVar.toString()).setPositiveButton("确定", (DialogInterface.OnClickListener) null).show();
                                super.b(bVar);
                            }
                        });
                    }
                }).setNegativeButton("取消", (DialogInterface.OnClickListener) null).show();
                return;
            case R.id.emp_one_key_open:
                if (!TextUtils.isEmpty(this.d.getText().toString())) {
                    this.f1498a = this.d.getText().toString();
                }
                final String obj2 = ((EditText) findViewById(R.id.random_key)).getText().toString();
                if (TextUtils.isEmpty(obj2)) {
                    d.a("验证码不正确");
                    return;
                } else {
                    new AlertDialog.Builder(this).setMessage("确定一键开通彩铃和包月？").setNegativeButton("取消", (DialogInterface.OnClickListener) null).setPositiveButton("确定", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialogInterface, int i) {
                            b.a().a(TestCtcc.this.f1498a, obj2, TestCtcc.this.f.isChecked(), "", new com.shoujiduoduo.util.b.b() {
                                public void a(c.b bVar) {
                                    com.shoujiduoduo.base.a.a.a("testctcc", "openCailingAndVip onSuccess:" + bVar.toString());
                                    new AlertDialog.Builder(TestCtcc.this).setMessage(bVar.toString()).setPositiveButton("确定", (DialogInterface.OnClickListener) null).show();
                                    super.a(bVar);
                                }

                                public void b(c.b bVar) {
                                    com.shoujiduoduo.base.a.a.a("testctcc", "openCailingAndVip onFailure:" + bVar.toString());
                                    new AlertDialog.Builder(TestCtcc.this).setMessage(bVar.toString()).setPositiveButton("确定", (DialogInterface.OnClickListener) null).show();
                                    super.b(bVar);
                                }
                            });
                        }
                    }).show();
                    return;
                }
            case R.id.query3rd_phone:
                UserInfo c2 = com.shoujiduoduo.a.b.b.g().c();
                String h3 = g.h();
                final StringBuilder sb = new StringBuilder();
                sb.append("&newimsi=").append(h3).append("&phone=").append(c2.getPhoneNum());
                i.a(new Runnable() {
                    public void run() {
                        com.shoujiduoduo.base.a.a.a("testctcc", "query3rd by phone: " + t.a("query3rd", sb.toString()));
                    }
                });
                return;
            case R.id.query3rd_uid:
                i.a(new Runnable() {
                    public void run() {
                        com.shoujiduoduo.base.a.a.a("testctcc", "query3rd by uid: " + t.a("query3rd", "&uid=" + com.shoujiduoduo.a.b.b.g().f()));
                    }
                });
                return;
        }
    }

    /* access modifiers changed from: private */
    public void a() {
        UserInfo c2 = com.shoujiduoduo.a.b.b.g().c();
        String h2 = g.h();
        com.shoujiduoduo.base.a.a.a("testctcc", "getservice end");
        String bVar = g.s().toString();
        String str = "";
        switch (c2.getLoginType()) {
            case 1:
                str = "phone";
                break;
            case 2:
                str = "qq";
                break;
            case 3:
                str = Constants.WEIBO;
                break;
            case 5:
                str = "weixin";
                break;
        }
        final StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("&newimsi=").append(h2).append("&phone=").append(c2.getPhoneNum()).append("&st=").append(bVar).append("&uid=").append(c2.getUid()).append("&3rd=").append(str);
        i.a(new Runnable() {
            public void run() {
                com.shoujiduoduo.base.a.a.a("testctcc", "res:" + t.a("openvip", stringBuffer.toString()));
            }
        });
    }

    /* access modifiers changed from: private */
    public void a(c.b bVar) {
        new AlertDialog.Builder(this).setMessage(bVar.toString()).setPositiveButton("确定", (DialogInterface.OnClickListener) null).show();
    }

    /* access modifiers changed from: private */
    public void a(c.b bVar, String str) {
        new AlertDialog.Builder(this).setMessage(bVar.toString() + " , " + str).setPositiveButton("确定", (DialogInterface.OnClickListener) null).show();
    }

    private void a(String str, g.b bVar) {
        new e(this, R.style.DuoDuoDialog, str, bVar, new e.a() {
            public void a(String str) {
                UserInfo c = com.shoujiduoduo.a.b.b.g().c();
                if (!c.isLogin()) {
                    c.setUserName(str);
                    c.setUid("phone_" + str);
                }
                c.setPhoneNum(str);
                c.setLoginStatus(1);
                com.shoujiduoduo.a.b.b.g().a(c);
                TestCtcc.this.d.setText(str);
                com.shoujiduoduo.a.a.c.a().a(com.shoujiduoduo.a.a.b.OBSERVER_USER_CENTER, new c.a<v>() {
                    public void a() {
                        ((v) this.f1284a).a(1, true, "", "");
                    }
                });
                d.a("初始化成功，可以进行功能调用了。。。");
            }
        }).show();
    }
}
