package com.shoujiduoduo.ui.cailing;

import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.TextView;
import com.shoujiduoduo.a.b.b;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ui.utils.BaseActivity;

public class CuccMemInfoActivity extends BaseActivity {

    /* renamed from: a  reason: collision with root package name */
    private TextView f1467a;
    private TextView b;
    private String c = "<body><h2>一.炫铃包月用户权益介绍</h2>&nbsp;&nbsp;&nbsp;&nbsp;1.20万首彩铃免费换, 换彩铃不用花钱啦<br/>&nbsp;&nbsp;&nbsp;&nbsp;2.永久去除应用内广告,无广告，真干净<br/>&nbsp;&nbsp;&nbsp;&nbsp;3.私人定制炫酷启动画面<br/>&nbsp;&nbsp;&nbsp;&nbsp;4.客服MM一对一为您服务<br/><h2>二.炫铃包月服务须知</h2>&nbsp;&nbsp;&nbsp;&nbsp;1.<font color=#ff0000>“铃声多多”</font>炫铃包月为包月计费，资费为<font color=#ff0000>5元/月</font>，用户订购即扣费。<br/>&nbsp;&nbsp;&nbsp;&nbsp;2.订购包月业务后，如月末不退订，次月1日将自动扣除包月费用。<br/>&nbsp;&nbsp;&nbsp;&nbsp;3.若您取消炫铃功能，将不能正常使用炫铃包月业务<br/>&nbsp;&nbsp;&nbsp;&nbsp;4.联通沃3G预付费20元卡不支持开通此业务<br/>&nbsp;&nbsp;&nbsp;&nbsp;5.使用炫铃过程中产生流量费按照您手机套餐内资费标准收取<br/>&nbsp;&nbsp;&nbsp;&nbsp;6.发送短信“TDLSDD””到10655158按照短信提示操作即可退订<br/>&nbsp;&nbsp;&nbsp;&nbsp;7.暂不支持山东用户进行订购，敬请期待<br/><h2>三.其他问题</h2>&nbsp;&nbsp;&nbsp;&nbsp;请咨询客服QQ：3219210390<br/></body>";

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_cucc_mem_info);
        this.f1467a = (TextView) findViewById(R.id.info);
        this.f1467a.setText(Html.fromHtml(this.c));
        this.b = (TextView) findViewById(R.id.header_title);
        if (b.g().e() == 3) {
            this.b.setText("炫铃包月(已订购)");
        }
        findViewById(R.id.backButton).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                CuccMemInfoActivity.this.finish();
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }
}
