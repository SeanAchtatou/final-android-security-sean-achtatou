package com.shoujiduoduo.ui.user;

import android.app.AlertDialog;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import cn.banshenggua.aichang.room.message.SocketMessage;
import com.a.a.p;
import com.shoujiduoduo.a.a.c;
import com.shoujiduoduo.a.c.e;
import com.shoujiduoduo.a.c.g;
import com.shoujiduoduo.base.bean.CommentData;
import com.shoujiduoduo.base.bean.DDList;
import com.shoujiduoduo.base.bean.HttpJsonRes;
import com.shoujiduoduo.base.bean.MessageData;
import com.shoujiduoduo.base.bean.UserData;
import com.shoujiduoduo.base.bean.UserInfo;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.ui.mine.FollowAndFansActivity;
import com.shoujiduoduo.ui.mine.UserMainPageActivity;
import com.shoujiduoduo.ui.utils.DDListFragment;
import com.shoujiduoduo.ui.utils.SlidingActivity;
import com.shoujiduoduo.ui.utils.h;
import com.shoujiduoduo.util.ae;
import com.shoujiduoduo.util.ah;
import com.shoujiduoduo.util.j;
import com.shoujiduoduo.util.r;
import com.shoujiduoduo.util.t;
import com.shoujiduoduo.util.widget.b;
import com.shoujiduoduo.util.widget.d;
import com.sina.weibo.sdk.register.mobile.SelectCountryActivity;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

public class CommentActivity extends SlidingActivity implements View.OnClickListener {
    private g A = new g() {
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.shoujiduoduo.ui.user.CommentActivity.a(android.view.View, android.content.Context):void
         arg types: [android.widget.EditText, com.shoujiduoduo.ui.user.CommentActivity]
         candidates:
          com.shoujiduoduo.ui.user.CommentActivity.a(com.shoujiduoduo.ui.user.CommentActivity, com.shoujiduoduo.base.bean.CommentData):com.shoujiduoduo.base.bean.CommentData
          com.shoujiduoduo.ui.user.CommentActivity.a(com.shoujiduoduo.ui.user.CommentActivity, com.shoujiduoduo.base.bean.MessageData):com.shoujiduoduo.base.bean.MessageData
          com.shoujiduoduo.ui.user.CommentActivity.a(com.shoujiduoduo.ui.user.CommentActivity, com.shoujiduoduo.base.bean.UserData):com.shoujiduoduo.base.bean.UserData
          com.shoujiduoduo.ui.user.CommentActivity.a(com.shoujiduoduo.ui.user.CommentActivity, com.shoujiduoduo.util.widget.b):com.shoujiduoduo.util.widget.b
          com.shoujiduoduo.ui.user.CommentActivity.a(android.widget.EditText, android.content.Context):void
          com.shoujiduoduo.ui.user.CommentActivity.a(java.lang.String, boolean):void
          com.shoujiduoduo.ui.user.CommentActivity.a(com.shoujiduoduo.ui.user.CommentActivity, java.lang.String):boolean
          com.shoujiduoduo.ui.user.CommentActivity.a(com.shoujiduoduo.ui.user.CommentActivity, boolean):boolean
          com.shoujiduoduo.ui.user.CommentActivity.a(android.view.View, android.content.Context):void */
        public void a(DDList dDList, int i) {
            if (dDList.getListId().equals(CommentActivity.this.x.getListId())) {
                com.shoujiduoduo.base.a.a.a("CommentActivity", "onDataUpdate in, id:" + CommentActivity.this.x.getListId());
                switch (i) {
                    case 0:
                        if (dDList.size() == 0) {
                            CommentActivity.this.t.setVisibility(0);
                            CommentActivity.this.a((View) CommentActivity.this.m, (Context) CommentActivity.this);
                            return;
                        }
                        CommentActivity.this.t.setVisibility(8);
                        return;
                    default:
                        return;
                }
            }
        }
    };
    private TextWatcher B = new TextWatcher() {
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.shoujiduoduo.ui.user.CommentActivity.a(com.shoujiduoduo.ui.user.CommentActivity, boolean):boolean
         arg types: [com.shoujiduoduo.ui.user.CommentActivity, int]
         candidates:
          com.shoujiduoduo.ui.user.CommentActivity.a(com.shoujiduoduo.ui.user.CommentActivity, com.shoujiduoduo.base.bean.CommentData):com.shoujiduoduo.base.bean.CommentData
          com.shoujiduoduo.ui.user.CommentActivity.a(com.shoujiduoduo.ui.user.CommentActivity, com.shoujiduoduo.base.bean.MessageData):com.shoujiduoduo.base.bean.MessageData
          com.shoujiduoduo.ui.user.CommentActivity.a(com.shoujiduoduo.ui.user.CommentActivity, com.shoujiduoduo.base.bean.UserData):com.shoujiduoduo.base.bean.UserData
          com.shoujiduoduo.ui.user.CommentActivity.a(com.shoujiduoduo.ui.user.CommentActivity, com.shoujiduoduo.util.widget.b):com.shoujiduoduo.util.widget.b
          com.shoujiduoduo.ui.user.CommentActivity.a(android.widget.EditText, android.content.Context):void
          com.shoujiduoduo.ui.user.CommentActivity.a(java.lang.String, boolean):void
          com.shoujiduoduo.ui.user.CommentActivity.a(com.shoujiduoduo.ui.user.CommentActivity, java.lang.String):boolean
          com.shoujiduoduo.ui.user.CommentActivity.a(android.view.View, android.content.Context):void
          com.shoujiduoduo.ui.user.CommentActivity.a(com.shoujiduoduo.ui.user.CommentActivity, boolean):boolean */
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            if (charSequence == null || charSequence.length() <= 0) {
                CommentActivity.this.l.setTextColor(CommentActivity.this.getResources().getColor(R.color.text_gray));
                boolean unused = CommentActivity.this.n = false;
                return;
            }
            CommentActivity.this.l.setTextColor(CommentActivity.this.getResources().getColor(R.color.text_green));
            boolean unused2 = CommentActivity.this.n = true;
        }

        public void afterTextChanged(Editable editable) {
        }
    };
    private View.OnFocusChangeListener C = new View.OnFocusChangeListener() {
        public void onFocusChange(View view, boolean z) {
            com.shoujiduoduo.base.a.a.a("CommentActivity", "EditText has focus:" + z);
            if (!z) {
                CommentActivity.this.m.setHint("说点什么...");
            }
        }
    };
    /* access modifiers changed from: private */
    public b D;
    private e E = new e() {
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [?, ?[OBJECT, ARRAY], int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        public void a(final CommentData commentData) {
            MessageData unused = CommentActivity.this.w = (MessageData) null;
            CommentData unused2 = CommentActivity.this.y = commentData;
            final UserInfo c = com.shoujiduoduo.a.b.b.g().c();
            View inflate = LayoutInflater.from(CommentActivity.this).inflate((int) R.layout.comment_click_choice, (ViewGroup) null, false);
            inflate.findViewById(R.id.answer_comment).setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    if (CommentActivity.this.D != null) {
                        CommentActivity.this.D.dismiss();
                    }
                    CommentActivity.this.z.sendEmptyMessageDelayed(1, 20);
                }
            });
            inflate.findViewById(R.id.copy_comment).setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    if (CommentActivity.this.D != null) {
                        CommentActivity.this.D.dismiss();
                    }
                    if (Build.VERSION.SDK_INT >= 11) {
                        ((ClipboardManager) CommentActivity.this.getSystemService("clipboard")).setText(commentData.comment);
                    } else {
                        ((android.text.ClipboardManager) CommentActivity.this.getSystemService("clipboard")).setText(commentData.comment);
                    }
                    d.a("已复制到剪贴板");
                }
            });
            inflate.findViewById(R.id.complain).setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    if (CommentActivity.this.D != null) {
                        CommentActivity.this.D.dismiss();
                    }
                    if (!c.isLogin()) {
                        CommentActivity.this.startActivity(new Intent(RingDDApp.c(), UserLoginActivity.class));
                    } else if (CommentActivity.this.a(commentData.cid)) {
                        d.a("已经举报过啦");
                    } else {
                        t.a("complain", "&cid=" + commentData.cid + "&uid=" + c.getUid(), new t.a() {
                            public void a(String str) {
                                com.shoujiduoduo.base.a.a.a("CommentActivity", "complaint success, res:" + str);
                                d.a("举报成功");
                                CommentActivity.this.b(commentData.cid);
                            }

                            public void a(String str, String str2) {
                                com.shoujiduoduo.base.a.a.a("CommentActivity", "complaint errror");
                                d.a("举报失败");
                            }
                        });
                    }
                }
            });
            View findViewById = inflate.findViewById(R.id.del_comment);
            if (c.getUid().equals(commentData.uid) || CommentActivity.this.u || c.isSuperUser()) {
                findViewById.setVisibility(0);
                findViewById.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {
                        if (CommentActivity.this.D != null) {
                            CommentActivity.this.D.dismiss();
                        }
                        if (!c.isLogin()) {
                            CommentActivity.this.startActivity(new Intent(RingDDApp.c(), UserLoginActivity.class));
                        } else {
                            t.a("delcomment", "&rid=" + CommentActivity.this.c + "&uid=" + c.getUid() + "&cid=" + commentData.cid + (c.isSuperUser() ? "&superuser=1" : ""), new t.a() {
                                public void a(String str) {
                                    com.shoujiduoduo.base.a.a.a("CommentActivity", "del comment:" + str);
                                    d.a("删除成功");
                                }

                                public void a(String str, String str2) {
                                    com.shoujiduoduo.base.a.a.a("CommentActivity", "del comment error");
                                    d.a("删除失败");
                                }
                            });
                        }
                    }
                });
            } else {
                findViewById.setVisibility(8);
            }
            if (CommentActivity.this.v == null || !c.isSuperUser()) {
                inflate.findViewById(R.id.blacklist).setVisibility(8);
                inflate.findViewById(R.id.del_all_comment).setVisibility(8);
            } else {
                inflate.findViewById(R.id.blacklist).setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {
                        if (CommentActivity.this.D != null) {
                            CommentActivity.this.D.dismiss();
                        }
                        if (!c.isLogin()) {
                            CommentActivity.this.startActivity(new Intent(RingDDApp.c(), UserLoginActivity.class));
                        } else {
                            new AlertDialog.Builder(CommentActivity.this).setMessage("确定屏蔽该用户发言？").setNegativeButton("再想想", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.dismiss();
                                }
                            }).setPositiveButton("确定", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    t.a("blacklist", "&rid=" + CommentActivity.this.c + "&uid=" + c.getUid() + "&tuid=" + commentData.uid, new t.a() {
                                        public void a(String str) {
                                            com.shoujiduoduo.base.a.a.a("CommentActivity", "blacklist user:" + str);
                                            d.a("操作成功");
                                        }

                                        public void a(String str, String str2) {
                                            com.shoujiduoduo.base.a.a.a("CommentActivity", "blacklist user error");
                                            d.a("操作失败");
                                        }
                                    });
                                }
                            }).show();
                        }
                    }
                });
                inflate.findViewById(R.id.del_all_comment).setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {
                        if (CommentActivity.this.D != null) {
                            CommentActivity.this.D.dismiss();
                        }
                        if (!c.isLogin()) {
                            CommentActivity.this.startActivity(new Intent(RingDDApp.c(), UserLoginActivity.class));
                        } else {
                            new AlertDialog.Builder(CommentActivity.this).setMessage("确定删除该用户所有评论？").setNegativeButton("再想想", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.dismiss();
                                }
                            }).setPositiveButton("确定", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    t.a("admindelcomment", "&rid=" + CommentActivity.this.c + "&uid=" + c.getUid() + "&tuid=" + commentData.uid, new t.a() {
                                        public void a(String str) {
                                            com.shoujiduoduo.base.a.a.a("CommentActivity", "del user all comment:" + str);
                                            d.a("删除成功");
                                        }

                                        public void a(String str, String str2) {
                                            com.shoujiduoduo.base.a.a.a("CommentActivity", "del user all comment error");
                                            d.a("删除失败");
                                        }
                                    });
                                }
                            }).show();
                        }
                    }
                });
            }
            b unused3 = CommentActivity.this.D = new b.a(CommentActivity.this).a(inflate).a();
            CommentActivity.this.D.show();
        }

        public void a() {
        }
    };
    private String F;
    /* access modifiers changed from: private */
    public boolean G;
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public String f1940a;
    private String b;
    /* access modifiers changed from: private */
    public String c;
    private TextView d;
    private TextView e;
    private TextView f;
    private TextView g;
    private TextView h;
    private ImageView i;
    private ImageView j;
    /* access modifiers changed from: private */
    public Button k;
    /* access modifiers changed from: private */
    public Button l;
    /* access modifiers changed from: private */
    public EditText m;
    /* access modifiers changed from: private */
    public boolean n;
    private final String o = "关注TA";
    private final String p = "取消关注";
    private final String q = "已关注";
    private int r;
    private int s;
    /* access modifiers changed from: private */
    public TextView t;
    /* access modifiers changed from: private */
    public boolean u;
    /* access modifiers changed from: private */
    public UserData v;
    /* access modifiers changed from: private */
    public MessageData w;
    /* access modifiers changed from: private */
    public com.shoujiduoduo.b.c.e x;
    /* access modifiers changed from: private */
    public CommentData y;
    /* access modifiers changed from: private */
    public Handler z = new Handler() {
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.shoujiduoduo.ui.user.CommentActivity.a(android.view.View, android.content.Context):void
         arg types: [android.widget.EditText, com.shoujiduoduo.ui.user.CommentActivity]
         candidates:
          com.shoujiduoduo.ui.user.CommentActivity.a(com.shoujiduoduo.ui.user.CommentActivity, com.shoujiduoduo.base.bean.CommentData):com.shoujiduoduo.base.bean.CommentData
          com.shoujiduoduo.ui.user.CommentActivity.a(com.shoujiduoduo.ui.user.CommentActivity, com.shoujiduoduo.base.bean.MessageData):com.shoujiduoduo.base.bean.MessageData
          com.shoujiduoduo.ui.user.CommentActivity.a(com.shoujiduoduo.ui.user.CommentActivity, com.shoujiduoduo.base.bean.UserData):com.shoujiduoduo.base.bean.UserData
          com.shoujiduoduo.ui.user.CommentActivity.a(com.shoujiduoduo.ui.user.CommentActivity, com.shoujiduoduo.util.widget.b):com.shoujiduoduo.util.widget.b
          com.shoujiduoduo.ui.user.CommentActivity.a(android.widget.EditText, android.content.Context):void
          com.shoujiduoduo.ui.user.CommentActivity.a(java.lang.String, boolean):void
          com.shoujiduoduo.ui.user.CommentActivity.a(com.shoujiduoduo.ui.user.CommentActivity, java.lang.String):boolean
          com.shoujiduoduo.ui.user.CommentActivity.a(com.shoujiduoduo.ui.user.CommentActivity, boolean):boolean
          com.shoujiduoduo.ui.user.CommentActivity.a(android.view.View, android.content.Context):void */
        public void handleMessage(Message message) {
            super.handleMessage(message);
            if (message.what == 1) {
                if (CommentActivity.this.w != null) {
                    CommentActivity.this.m.setHint("回复给:" + ("ring".equals(CommentActivity.this.w.feedtype) ? CommentActivity.this.w.artist : CommentActivity.this.w.name));
                } else if (CommentActivity.this.y != null) {
                    CommentActivity.this.m.setHint("回复给:" + CommentActivity.this.y.name);
                }
                CommentActivity.this.m.requestFocus();
                CommentActivity.this.m.performClick();
                CommentActivity.this.a((View) CommentActivity.this.m, (Context) CommentActivity.this);
            }
        }
    };

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        c.a().b(com.shoujiduoduo.a.a.b.OBSERVER_COMMENT, this.E);
        c.a().b(com.shoujiduoduo.a.a.b.OBSERVER_LIST_DATA, this.A);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_comment);
        findViewById(R.id.back).setOnClickListener(this);
        findViewById(R.id.tv_fans_hint).setOnClickListener(this);
        findViewById(R.id.tv_follow_hint).setOnClickListener(this);
        findViewById(R.id.userinfo_layout).setOnClickListener(this);
        this.d = (TextView) findViewById(R.id.user_fans);
        this.d.setOnClickListener(this);
        this.e = (TextView) findViewById(R.id.user_follow);
        this.e.setOnClickListener(this);
        this.f = (TextView) findViewById(R.id.user_name);
        this.g = (TextView) findViewById(R.id.user_id);
        this.j = (ImageView) findViewById(R.id.user_head);
        this.j.setOnClickListener(this);
        this.k = (Button) findViewById(R.id.btn_follow);
        this.k.setOnClickListener(this);
        this.i = (ImageView) findViewById(R.id.iv_sex);
        this.h = (TextView) findViewById(R.id.tv_songname);
        this.m = (EditText) findViewById(R.id.et_write_comment);
        this.m.setOnFocusChangeListener(this.C);
        this.m.addTextChangedListener(this.B);
        this.l = (Button) findViewById(R.id.btn_commit);
        this.l.setOnClickListener(this);
        this.t = (TextView) findViewById(R.id.tv_nodata_hint);
        Intent intent = getIntent();
        if (intent != null) {
            this.f1940a = intent.getStringExtra("tuid");
            this.b = intent.getStringExtra(SelectCountryActivity.EXTRA_COUNTRY_NAME);
            this.c = intent.getStringExtra("rid");
            if ("feeds_replay".equals(intent.getStringExtra("from"))) {
                this.w = (MessageData) intent.getParcelableExtra("current_comment");
            }
            String f2 = com.shoujiduoduo.a.b.b.g().f();
            if (!ah.c(f2) && f2.equals(this.f1940a)) {
                this.u = true;
            }
            a(this.f1940a, this.u);
            this.h.setText(this.b);
        }
        DDListFragment dDListFragment = new DDListFragment();
        Bundle bundle2 = new Bundle();
        bundle2.putString("adapter_type", "comment_list_adapter");
        dDListFragment.setArguments(bundle2);
        this.x = new com.shoujiduoduo.b.c.e(this.c);
        dDListFragment.a(this.x);
        FragmentTransaction beginTransaction = getSupportFragmentManager().beginTransaction();
        beginTransaction.add((int) R.id.comment_layout, dDListFragment);
        beginTransaction.commit();
        c.a().a(com.shoujiduoduo.a.a.b.OBSERVER_COMMENT, this.E);
        c.a().a(com.shoujiduoduo.a.a.b.OBSERVER_LIST_DATA, this.A);
    }

    public void a() {
    }

    public void b() {
        finish();
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (!(motionEvent.getAction() != 0 || getCurrentFocus() == null || getCurrentFocus().getWindowToken() == null)) {
            ((InputMethodManager) getSystemService("input_method")).hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 2);
        }
        return super.onTouchEvent(motionEvent);
    }

    public void a(View view, Context context) {
        if (view.requestFocus()) {
            ((InputMethodManager) context.getSystemService("input_method")).showSoftInput(view, 1);
        }
    }

    public static void a(EditText editText, Context context) {
        ((InputMethodManager) context.getSystemService("input_method")).hideSoftInputFromWindow(editText.getWindowToken(), 0);
    }

    /* access modifiers changed from: private */
    public boolean a(String str) {
        return ae.a(RingDDApp.c(), "complain_comment_list", "").contains(str);
    }

    /* access modifiers changed from: private */
    public void b(String str) {
        String a2 = ae.a(RingDDApp.c(), "complain_comment_list", "");
        if (!a2.equals("")) {
            str = a2 + "|" + str;
        }
        ae.c(RingDDApp.c(), "complain_comment_list", str);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.shoujiduoduo.ui.user.CommentActivity.a(android.widget.EditText, android.content.Context):void
     arg types: [android.widget.EditText, com.shoujiduoduo.ui.user.CommentActivity]
     candidates:
      com.shoujiduoduo.ui.user.CommentActivity.a(com.shoujiduoduo.ui.user.CommentActivity, com.shoujiduoduo.base.bean.CommentData):com.shoujiduoduo.base.bean.CommentData
      com.shoujiduoduo.ui.user.CommentActivity.a(com.shoujiduoduo.ui.user.CommentActivity, com.shoujiduoduo.base.bean.MessageData):com.shoujiduoduo.base.bean.MessageData
      com.shoujiduoduo.ui.user.CommentActivity.a(com.shoujiduoduo.ui.user.CommentActivity, com.shoujiduoduo.base.bean.UserData):com.shoujiduoduo.base.bean.UserData
      com.shoujiduoduo.ui.user.CommentActivity.a(com.shoujiduoduo.ui.user.CommentActivity, com.shoujiduoduo.util.widget.b):com.shoujiduoduo.util.widget.b
      com.shoujiduoduo.ui.user.CommentActivity.a(java.lang.String, boolean):void
      com.shoujiduoduo.ui.user.CommentActivity.a(com.shoujiduoduo.ui.user.CommentActivity, java.lang.String):boolean
      com.shoujiduoduo.ui.user.CommentActivity.a(com.shoujiduoduo.ui.user.CommentActivity, boolean):boolean
      com.shoujiduoduo.ui.user.CommentActivity.a(android.view.View, android.content.Context):void
      com.shoujiduoduo.ui.user.CommentActivity.a(android.widget.EditText, android.content.Context):void */
    private void a(String str, String str2, String str3, String str4, String str5) {
        UserInfo c2 = com.shoujiduoduo.a.b.b.g().c();
        if (!c2.isLogin()) {
            startActivity(new Intent(this, UserLoginActivity.class));
            return;
        }
        this.F = str;
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("rid", this.c);
            jSONObject.put("ruid", str5);
            jSONObject.put("uid", c2.getUid());
            jSONObject.put("tuid", str4);
            jSONObject.put("ddid", this.v.ddid);
            jSONObject.put("tcid", str3);
            jSONObject.put("comment", str);
            jSONObject.put("tcomment", str2);
        } catch (JSONException e2) {
            e2.printStackTrace();
        }
        com.shoujiduoduo.base.a.a.a("CommentActivity", "post comment json:" + jSONObject.toString());
        a(this.m, (Context) this);
        this.m.clearFocus();
        this.m.setText("");
        t.a("comment", "&rid=" + this.c + "&uid=" + c2.getUid(), jSONObject, new t.a() {
            public void a(String str) {
                com.shoujiduoduo.base.a.a.a("CommentActivity", "commitcomment success, res:" + str);
                a c = CommentActivity.this.c(str);
                if (c.a()) {
                    d.a("发表成功");
                    c.a().b(com.shoujiduoduo.a.a.b.OBSERVER_COMMENT, new c.a<e>() {
                        public void a() {
                            ((e) this.f1284a).a();
                        }
                    });
                    return;
                }
                d.a("" + c.b);
            }

            public void a(String str, String str2) {
                com.shoujiduoduo.base.a.a.a("CommentActivity", "commitcomment error");
                d.a("发表失败");
            }
        });
    }

    /* access modifiers changed from: private */
    public a c(String str) {
        try {
            JSONObject jSONObject = (JSONObject) new JSONTokener(str).nextValue();
            if (jSONObject != null) {
                a aVar = new a();
                aVar.b = jSONObject.optString("msg");
                aVar.f1964a = jSONObject.optString(SocketMessage.MSG_RESULE_KEY);
                return aVar;
            }
        } catch (JSONException e2) {
            e2.printStackTrace();
        }
        return new a("failed", "发表失败!");
    }

    private class a {

        /* renamed from: a  reason: collision with root package name */
        String f1964a;
        String b;

        public a() {
            this.f1964a = "";
            this.b = "";
        }

        public a(String str, String str2) {
            this.b = str2;
            this.f1964a = str;
        }

        public boolean a() {
            return "success".equals(this.f1964a);
        }
    }

    private void a(String str, boolean z2) {
        UserInfo c2 = com.shoujiduoduo.a.b.b.g().c();
        StringBuilder sb = new StringBuilder();
        sb.append("&uid=").append(c2.getUid()).append("&tuid=").append(str);
        if (z2) {
            sb.append("&username=").append(t.i(c2.getUserName()));
            sb.append("&headurl=").append(t.i(c2.getHeadPic()));
        }
        t.a("getuserinfo", sb.toString(), new t.a() {
            public void a(String str) {
                com.shoujiduoduo.base.a.a.a("CommentActivity", "userinfo:" + str);
                UserData b = j.b(str);
                if (b != null) {
                    UserData unused = CommentActivity.this.v = b;
                    CommentActivity.this.a(b);
                } else {
                    com.shoujiduoduo.base.a.a.a("CommentActivity", "user 解析失败");
                }
                if (CommentActivity.this.w != null) {
                    CommentActivity.this.z.sendEmptyMessageDelayed(1, 20);
                }
            }

            public void a(String str, String str2) {
                com.shoujiduoduo.base.a.a.a("CommentActivity", "user 信息获取失败");
            }
        });
    }

    /* access modifiers changed from: private */
    public void a(UserData userData) {
        this.s = userData.followingNum;
        this.r = userData.followerNum;
        com.shoujiduoduo.base.a.a.a("CommentActivity", "fansNum:" + this.r + ", followNum:" + this.s);
        this.f.setText(userData.userName);
        if (userData.followerNum >= 0) {
            this.d.setText("" + userData.followerNum);
        }
        if (userData.followingNum >= 0) {
            this.e.setText("" + userData.followingNum);
        }
        if (!ah.c(userData.headUrl)) {
            com.d.a.b.d.a().a(userData.headUrl, this.j, h.a().d());
        }
        if (!ah.c(userData.ddid)) {
            this.g.setText("多多ID: " + userData.ddid);
        } else {
            this.g.setVisibility(4);
        }
        if (!ah.c(userData.sex)) {
            String str = userData.sex;
            char c2 = 65535;
            switch (str.hashCode()) {
                case 22899:
                    if (str.equals("女")) {
                        c2 = 1;
                        break;
                    }
                    break;
                case 30007:
                    if (str.equals("男")) {
                        c2 = 0;
                        break;
                    }
                    break;
                case 657289:
                    if (str.equals("保密")) {
                        c2 = 2;
                        break;
                    }
                    break;
            }
            switch (c2) {
                case 0:
                    this.i.setImageResource(R.drawable.icon_boy);
                    break;
                case 1:
                    this.i.setImageResource(R.drawable.icon_girl);
                    break;
                case 2:
                    this.i.setImageResource(R.drawable.icon_sex_secket);
                    break;
                default:
                    this.i.setVisibility(8);
                    break;
            }
        }
        if (this.u) {
            return;
        }
        if (userData.followed) {
            this.k.setText("已关注");
        } else {
            this.k.setText("关注TA");
        }
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.back:
                finish();
                return;
            case R.id.user_head:
            case R.id.userinfo_layout:
                Intent intent = new Intent(RingDDApp.c(), UserMainPageActivity.class);
                intent.putExtra("tuid", this.f1940a);
                startActivity(intent);
                return;
            case R.id.user_fans:
            case R.id.tv_fans_hint:
                Intent intent2 = new Intent(RingDDApp.c(), FollowAndFansActivity.class);
                intent2.putExtra("type", "fans");
                intent2.putExtra("tuid", this.f1940a);
                intent2.putExtra("fansNum", this.r);
                intent2.putExtra("followNum", this.s);
                startActivity(intent2);
                return;
            case R.id.user_follow:
            case R.id.tv_follow_hint:
                Intent intent3 = new Intent(RingDDApp.c(), FollowAndFansActivity.class);
                intent3.putExtra("type", "follow");
                intent3.putExtra("tuid", this.f1940a);
                intent3.putExtra("fansNum", this.r);
                intent3.putExtra("followNum", this.s);
                startActivity(intent3);
                return;
            case R.id.btn_follow:
                if (!this.u) {
                    c();
                    return;
                }
                return;
            case R.id.btn_commit:
                if (this.n) {
                    String obj = this.m.getText().toString();
                    if (ah.c(obj)) {
                        d.a("请留下您的评论再发表吧！");
                        return;
                    } else if (obj.equals(this.F)) {
                        com.shoujiduoduo.base.a.a.a("CommentActivity", "相同的评论不能多次提交");
                        return;
                    } else if (this.m.getHint().equals("说点什么...")) {
                        com.shoujiduoduo.base.a.a.a("CommentActivity", "发表新评论, msg:" + obj);
                        a(obj, "", "", "", this.f1940a);
                        return;
                    } else if (this.w != null) {
                        com.shoujiduoduo.base.a.a.a("CommentActivity", "回复评论, 来自我的关注， 回复：" + this.w.name + ", msg:" + obj);
                        a(obj, this.w.comment, this.w.cid, this.w.uid, this.w.ruid);
                        return;
                    } else if (this.y != null) {
                        com.shoujiduoduo.base.a.a.a("CommentActivity", "回复评论, 回复：" + this.y.name + ", msg:" + obj);
                        a(obj, this.y.comment, this.y.cid, this.y.uid, this.f1940a);
                        return;
                    } else {
                        return;
                    }
                } else {
                    return;
                }
            default:
                return;
        }
    }

    private void c() {
        UserInfo c2 = com.shoujiduoduo.a.b.b.g().c();
        if (c2.isLogin()) {
            String charSequence = this.k.getText().toString();
            UserInfo c3 = com.shoujiduoduo.a.b.b.g().c();
            String str = "&uid=" + c2.getUid() + "&tuid=" + this.f1940a + "&username=" + r.a(c3.getUserName()) + "&headurl=" + r.a(c3.getHeadPic());
            if (this.G) {
                com.shoujiduoduo.base.a.a.a("CommentActivity", "isRequesting, return");
                return;
            }
            this.G = true;
            if ("关注TA".equals(charSequence)) {
                t.a("follow", str, new t.a() {
                    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                     method: com.shoujiduoduo.ui.user.CommentActivity.b(com.shoujiduoduo.ui.user.CommentActivity, boolean):boolean
                     arg types: [com.shoujiduoduo.ui.user.CommentActivity, int]
                     candidates:
                      com.shoujiduoduo.ui.user.CommentActivity.b(com.shoujiduoduo.ui.user.CommentActivity, com.shoujiduoduo.base.bean.UserData):void
                      com.shoujiduoduo.ui.user.CommentActivity.b(com.shoujiduoduo.ui.user.CommentActivity, java.lang.String):void
                      com.shoujiduoduo.ui.user.CommentActivity.b(com.shoujiduoduo.ui.user.CommentActivity, boolean):boolean */
                    public void a(String str) {
                        try {
                            HttpJsonRes httpJsonRes = (HttpJsonRes) new com.a.a.e().a(str, HttpJsonRes.class);
                            if (httpJsonRes.getResult().equals("success")) {
                                CommentActivity.this.k.setText("取消关注");
                                d.a("关注成功");
                                com.shoujiduoduo.a.b.b.g().b(CommentActivity.this.f1940a);
                            } else {
                                d.a(httpJsonRes.getMsg());
                            }
                        } catch (p e) {
                            e.printStackTrace();
                        }
                        boolean unused = CommentActivity.this.G = false;
                    }

                    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                     method: com.shoujiduoduo.ui.user.CommentActivity.b(com.shoujiduoduo.ui.user.CommentActivity, boolean):boolean
                     arg types: [com.shoujiduoduo.ui.user.CommentActivity, int]
                     candidates:
                      com.shoujiduoduo.ui.user.CommentActivity.b(com.shoujiduoduo.ui.user.CommentActivity, com.shoujiduoduo.base.bean.UserData):void
                      com.shoujiduoduo.ui.user.CommentActivity.b(com.shoujiduoduo.ui.user.CommentActivity, java.lang.String):void
                      com.shoujiduoduo.ui.user.CommentActivity.b(com.shoujiduoduo.ui.user.CommentActivity, boolean):boolean */
                    public void a(String str, String str2) {
                        d.a("关注失败");
                        boolean unused = CommentActivity.this.G = false;
                    }
                });
            } else {
                t.a("unfollow", str, new t.a() {
                    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                     method: com.shoujiduoduo.ui.user.CommentActivity.b(com.shoujiduoduo.ui.user.CommentActivity, boolean):boolean
                     arg types: [com.shoujiduoduo.ui.user.CommentActivity, int]
                     candidates:
                      com.shoujiduoduo.ui.user.CommentActivity.b(com.shoujiduoduo.ui.user.CommentActivity, com.shoujiduoduo.base.bean.UserData):void
                      com.shoujiduoduo.ui.user.CommentActivity.b(com.shoujiduoduo.ui.user.CommentActivity, java.lang.String):void
                      com.shoujiduoduo.ui.user.CommentActivity.b(com.shoujiduoduo.ui.user.CommentActivity, boolean):boolean */
                    public void a(String str) {
                        CommentActivity.this.k.setText("关注TA");
                        d.a("取消关注成功");
                        boolean unused = CommentActivity.this.G = false;
                        com.shoujiduoduo.a.b.b.g().a(CommentActivity.this.f1940a);
                    }

                    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                     method: com.shoujiduoduo.ui.user.CommentActivity.b(com.shoujiduoduo.ui.user.CommentActivity, boolean):boolean
                     arg types: [com.shoujiduoduo.ui.user.CommentActivity, int]
                     candidates:
                      com.shoujiduoduo.ui.user.CommentActivity.b(com.shoujiduoduo.ui.user.CommentActivity, com.shoujiduoduo.base.bean.UserData):void
                      com.shoujiduoduo.ui.user.CommentActivity.b(com.shoujiduoduo.ui.user.CommentActivity, java.lang.String):void
                      com.shoujiduoduo.ui.user.CommentActivity.b(com.shoujiduoduo.ui.user.CommentActivity, boolean):boolean */
                    public void a(String str, String str2) {
                        d.a("取消失败");
                        boolean unused = CommentActivity.this.G = false;
                    }
                });
            }
        } else {
            startActivity(new Intent(this, UserLoginActivity.class));
        }
    }
}
