package com.shoujiduoduo.ui.user;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.d.a.b.d;
import com.shoujiduoduo.a.a.b;
import com.shoujiduoduo.a.a.c;
import com.shoujiduoduo.a.c.v;
import com.shoujiduoduo.a.c.x;
import com.shoujiduoduo.base.bean.UserInfo;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ui.cailing.e;
import com.shoujiduoduo.ui.utils.BaseActivity;
import com.shoujiduoduo.ui.utils.h;
import com.shoujiduoduo.util.b.c;
import com.shoujiduoduo.util.g;
import com.shoujiduoduo.util.i;
import com.shoujiduoduo.util.t;
import com.shoujiduoduo.util.widget.b;

public class UserInfoEditActivity2 extends BaseActivity implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private ImageView f1997a;
    /* access modifiers changed from: private */
    public Button b;
    /* access modifiers changed from: private */
    public TextView c;
    private TextView d;
    /* access modifiers changed from: private */
    public Handler e;
    private v f = new v() {
        public void b(int i) {
            UserInfoEditActivity2.this.b();
        }

        public void a(String str, boolean z) {
        }

        public void a(String str) {
        }

        public void a(int i) {
        }

        public void a(int i, boolean z, String str, String str2) {
        }
    };
    /* access modifiers changed from: private */
    public ProgressDialog g = null;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_edit_userinfo);
        this.d = (TextView) findViewById(R.id.user_nickname);
        this.c = (TextView) findViewById(R.id.phone);
        this.b = (Button) findViewById(R.id.btn_bind);
        this.f1997a = (ImageView) findViewById(R.id.user_head);
        findViewById(R.id.back).setOnClickListener(this);
        this.b.setOnClickListener(this);
        this.e = new a();
        b();
        c.a().a(b.OBSERVER_USER_CENTER, this.f);
    }

    /* access modifiers changed from: private */
    public void b() {
        UserInfo c2 = com.shoujiduoduo.a.b.b.g().c();
        d.a().a(c2.getHeadPic(), this.f1997a, h.a().d());
        this.d.setText(!TextUtils.isEmpty(c2.getNickName()) ? c2.getNickName() : c2.getUserName());
        if (!TextUtils.isEmpty(c2.getPhoneNum())) {
            this.c.setText(c2.getPhoneNum());
            this.b.setText("解除绑定");
            return;
        }
        this.c.setText("无");
        this.b.setText("绑定手机");
    }

    private class a extends Handler {
        private a() {
        }

        public void handleMessage(Message message) {
            String str;
            super.handleMessage(message);
            if (message.what == 1) {
                final UserInfo c = com.shoujiduoduo.a.b.b.g().c();
                final String str2 = (String) message.obj;
                switch (c.getLoginType()) {
                    case 1:
                        str = "手机账号";
                        break;
                    case 2:
                        str = "QQ号码";
                        break;
                    case 3:
                        str = "微博账号";
                        break;
                    case 4:
                    default:
                        str = "账号";
                        break;
                    case 5:
                        str = "微信账号";
                        break;
                }
                new b.a(UserInfoEditActivity2.this).a("当前输入的手机号已经与另一个" + str + "绑定, 是否要取消之前的绑定状态并与当前账号绑定？").a("确定", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        UserInfoEditActivity2.this.a("请稍候...");
                        dialogInterface.dismiss();
                        i.a(new Runnable() {
                            public void run() {
                                StringBuilder sb = new StringBuilder();
                                sb.append("&phone=").append(str2).append("&newimsi=").append(g.h()).append("&3rd=").append(c.getLoginTypeStr()).append("&uid=").append(c.getUid());
                                t.a("clear3rd", sb.toString());
                                UserInfoEditActivity2.this.a(str2, c.getLoginTypeStr(), c);
                            }
                        });
                    }
                }).b("取消", (DialogInterface.OnClickListener) null).a().show();
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.e != null) {
            this.e.removeCallbacksAndMessages(null);
        }
        c.a().b(com.shoujiduoduo.a.a.b.OBSERVER_USER_CENTER, this.f);
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.back:
                finish();
                return;
            case R.id.btn_bind:
                final UserInfo c2 = com.shoujiduoduo.a.b.b.g().c();
                if (!TextUtils.isEmpty(c2.getPhoneNum())) {
                    new b.a(this).a("确定要解除与当前手机号的绑定吗？").a("确定", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialogInterface, int i) {
                            UserInfoEditActivity2.this.b.setText("绑定手机");
                            UserInfoEditActivity2.this.c.setText("(无)");
                            dialogInterface.dismiss();
                            i.a(new Runnable() {
                                public void run() {
                                    UserInfoEditActivity2.this.b(c2.getPhoneNum(), c2.getLoginTypeStr(), c2);
                                }
                            });
                        }
                    }).b("取消", (DialogInterface.OnClickListener) null).a().show();
                    return;
                } else {
                    c();
                    return;
                }
            default:
                return;
        }
    }

    private void c() {
        new e(this, R.style.DuoDuoDialog, "", g.s(), new e.a() {
            public void a(final String str) {
                UserInfoEditActivity2.this.a("请稍候...");
                final UserInfo c = com.shoujiduoduo.a.b.b.g().c();
                final String loginTypeStr = c.getLoginTypeStr();
                i.a(new Runnable() {
                    public void run() {
                        StringBuilder sb = new StringBuilder();
                        sb.append("&newimsi=").append(g.h()).append("&phone=").append(str);
                        String a2 = t.a("query3rd", sb.toString());
                        if (a2 != null) {
                            com.shoujiduoduo.base.a.a.a("UserInfoEditActivity2", "curLoginType:" + loginTypeStr + ", bindedType:" + a2);
                            if (a2.contains(loginTypeStr)) {
                                com.shoujiduoduo.base.a.a.a("UserInfoEditActivity2", "已经与当前平台绑定");
                                UserInfoEditActivity2.this.a();
                                UserInfoEditActivity2.this.e.sendMessage(UserInfoEditActivity2.this.e.obtainMessage(1, str));
                                return;
                            }
                            com.shoujiduoduo.base.a.a.a("UserInfoEditActivity2", "没有与当前平台绑定");
                        }
                        UserInfoEditActivity2.this.a(str, loginTypeStr, c);
                    }
                });
            }
        }).show();
    }

    /* access modifiers changed from: private */
    public void a(final String str, String str2, final UserInfo userInfo) {
        StringBuilder sb = new StringBuilder();
        sb.append("&newimsi=").append(g.h()).append("&phone=").append(str).append("&3rd=").append(str2).append("&uid=").append(userInfo.getUid());
        t.a("connect3rd", sb.toString());
        c.a().a(new c.b() {
            public void a() {
                UserInfo c = com.shoujiduoduo.a.b.b.g().c();
                c.setPhoneNum(str);
                com.shoujiduoduo.a.b.b.g().a(c);
            }
        });
        "&from=user_info_edit&phone=" + userInfo.getPhoneNum();
        switch (g.g(str)) {
            case cu:
                com.shoujiduoduo.util.e.a.a().f(new com.shoujiduoduo.util.b.b() {
                    public void a(c.b bVar) {
                        boolean z = true;
                        super.a(bVar);
                        if (bVar instanceof c.f) {
                            int vipType = userInfo.getVipType();
                            if (((c.f) bVar).e()) {
                                userInfo.setVipType(3);
                                if (vipType == 3) {
                                    z = false;
                                }
                            } else {
                                userInfo.setVipType(0);
                                if (vipType == 0) {
                                    z = false;
                                }
                            }
                            if (z) {
                                com.shoujiduoduo.a.a.c.a().a(new c.b() {
                                    public void a() {
                                        com.shoujiduoduo.a.b.b.g().a(userInfo);
                                    }
                                });
                                com.shoujiduoduo.a.a.c.a().a(com.shoujiduoduo.a.a.b.OBSERVER_VIP, new c.a<x>() {
                                    public void a() {
                                        ((x) this.f1284a).a(3);
                                    }
                                });
                            }
                        }
                    }

                    public void b(c.b bVar) {
                        super.b(bVar);
                    }
                });
                break;
            case f2302a:
                com.shoujiduoduo.util.c.b.a().e(new com.shoujiduoduo.util.b.b() {
                    public void a(c.b bVar) {
                        boolean z = true;
                        super.a(bVar);
                        if (bVar != null && (bVar instanceof c.d)) {
                            int vipType = userInfo.getVipType();
                            if (((c.d) bVar).e()) {
                                userInfo.setVipType(1);
                                if (vipType == 1) {
                                    z = false;
                                }
                            } else {
                                userInfo.setVipType(0);
                                if (vipType == 0) {
                                    z = false;
                                }
                            }
                            if (z) {
                                com.shoujiduoduo.a.a.c.a().a(new c.b() {
                                    public void a() {
                                        com.shoujiduoduo.a.b.b.g().a(userInfo);
                                    }
                                });
                                com.shoujiduoduo.a.a.c.a().a(com.shoujiduoduo.a.a.b.OBSERVER_VIP, new c.a<x>() {
                                    public void a() {
                                        ((x) this.f1284a).a(1);
                                    }
                                });
                            }
                        }
                    }

                    public void b(c.b bVar) {
                        super.b(bVar);
                    }
                });
                break;
            case ct:
                com.shoujiduoduo.util.d.b.a().a(userInfo.getPhoneNum(), new com.shoujiduoduo.util.b.b() {
                    public void a(c.b bVar) {
                        boolean z = true;
                        super.a(bVar);
                        if (bVar != null && (bVar instanceof c.e)) {
                            c.e eVar = (c.e) bVar;
                            int vipType = userInfo.getVipType();
                            if (eVar.e() || eVar.f()) {
                                userInfo.setVipType(2);
                                if (vipType == 2) {
                                    z = false;
                                }
                            } else {
                                userInfo.setVipType(0);
                                if (vipType == 0) {
                                    z = false;
                                }
                            }
                            if (z) {
                                com.shoujiduoduo.a.a.c.a().a(new c.b() {
                                    public void a() {
                                        com.shoujiduoduo.a.b.b.g().a(userInfo);
                                    }
                                });
                                com.shoujiduoduo.a.a.c.a().a(com.shoujiduoduo.a.a.b.OBSERVER_VIP, new c.a<x>() {
                                    public void a() {
                                        ((x) this.f1284a).a(2);
                                    }
                                });
                            }
                        }
                    }

                    public void b(c.b bVar) {
                        super.b(bVar);
                    }
                });
                break;
        }
        a();
        com.shoujiduoduo.util.widget.d.a("手机号已经成功绑定");
    }

    /* access modifiers changed from: private */
    public void b(String str, String str2, final UserInfo userInfo) {
        StringBuilder sb = new StringBuilder();
        sb.append("&phone=").append(str).append("&newimsi=").append(g.h()).append("&3rd=").append(str2).append("&uid=").append(userInfo.getUid());
        t.a("clear3rd", sb.toString());
        com.shoujiduoduo.a.a.c.a().a(new c.b() {
            public void a() {
                userInfo.setPhoneNum("");
                userInfo.setVipType(0);
                com.shoujiduoduo.a.b.b.g().a(userInfo);
                com.shoujiduoduo.a.a.c.a().a(com.shoujiduoduo.a.a.b.OBSERVER_VIP, new c.a<x>() {
                    public void a() {
                        ((x) this.f1284a).a(0);
                    }
                });
            }
        });
        com.shoujiduoduo.util.widget.d.a("已为您解除与当前手机号的绑定");
    }

    /* access modifiers changed from: package-private */
    public void a(final String str) {
        this.e.post(new Runnable() {
            public void run() {
                if (UserInfoEditActivity2.this.g == null) {
                    ProgressDialog unused = UserInfoEditActivity2.this.g = new ProgressDialog(UserInfoEditActivity2.this);
                    UserInfoEditActivity2.this.g.setMessage(str);
                    UserInfoEditActivity2.this.g.setIndeterminate(false);
                    UserInfoEditActivity2.this.g.setCancelable(true);
                    UserInfoEditActivity2.this.g.setCanceledOnTouchOutside(false);
                    if (!UserInfoEditActivity2.this.isFinishing()) {
                        UserInfoEditActivity2.this.g.show();
                    }
                }
            }
        });
    }

    /* access modifiers changed from: package-private */
    public void a() {
        this.e.post(new Runnable() {
            public void run() {
                if (UserInfoEditActivity2.this.g != null) {
                    UserInfoEditActivity2.this.g.dismiss();
                    ProgressDialog unused = UserInfoEditActivity2.this.g = null;
                }
            }
        });
    }
}
