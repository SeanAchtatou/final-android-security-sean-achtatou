package com.shoujiduoduo.ui.user;

import android.content.ClipboardManager;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;
import com.d.a.b.d;
import com.d.a.b.d.b;
import com.shoujiduoduo.base.bean.UserData;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.ui.settings.CropImageActivity;
import com.shoujiduoduo.ui.utils.SlidingActivity;
import com.shoujiduoduo.ui.utils.h;
import com.shoujiduoduo.util.ah;
import com.shoujiduoduo.util.c;
import com.shoujiduoduo.util.g;
import com.shoujiduoduo.util.i;
import com.shoujiduoduo.util.l;
import com.shoujiduoduo.util.q;
import com.shoujiduoduo.util.widget.a;
import com.shoujiduoduo.util.widget.b;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class UserInfoEditActivity extends SlidingActivity implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private ImageView f1990a;
    private ImageView b;
    private EditText c;
    private TextView d;
    /* access modifiers changed from: private */
    public TextView e;
    private EditText f;
    private UserData g;
    private String h;
    /* access modifiers changed from: private */
    public String i;
    /* access modifiers changed from: private */
    public String j;
    /* access modifiers changed from: private */
    public String k;
    /* access modifiers changed from: private */
    public String l;
    /* access modifiers changed from: private */
    public b m;
    /* access modifiers changed from: private */
    public String n = null;
    private a o = null;
    private final int p = 0;
    private final int q = 1;
    /* access modifiers changed from: private */
    public Handler r = new Handler() {
        public void handleMessage(Message message) {
            super.handleMessage(message);
            if (message.what == 0) {
                UserInfoEditActivity.this.g();
            }
        }
    };

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_userinfo_edit);
        findViewById(R.id.btn_save).setOnClickListener(this);
        findViewById(R.id.btn_change_bkg).setOnClickListener(this);
        findViewById(R.id.btn_change_head).setOnClickListener(this);
        findViewById(R.id.btn_copy_ddid).setOnClickListener(this);
        findViewById(R.id.back).setOnClickListener(this);
        this.c = (EditText) findViewById(R.id.et_user_name);
        this.f = (EditText) findViewById(R.id.et_user_intro);
        this.d = (TextView) findViewById(R.id.tv_ddid);
        this.e = (TextView) findViewById(R.id.tv_sex);
        this.e.setOnClickListener(this);
        this.f1990a = (ImageView) findViewById(R.id.iv_bkg);
        this.b = (ImageView) findViewById(R.id.user_head);
        Intent intent = getIntent();
        if (intent != null) {
            UserData userData = (UserData) intent.getParcelableExtra("userdata");
            if (userData != null) {
                this.g = userData;
                a(userData);
                return;
            }
            com.shoujiduoduo.base.a.a.c("UserInfoEditActivity", "ringdata is null");
        }
    }

    public void a() {
    }

    public void b() {
        setResult(0);
        finish();
    }

    private void a(UserData userData) {
        this.c.setText(userData.userName);
        if (!ah.c(userData.headUrl)) {
            d.a().a(userData.headUrl, this.b, h.a().d());
        }
        if (!ah.c(userData.ddid)) {
            this.d.setText("" + userData.ddid);
        } else {
            this.d.setVisibility(4);
        }
        if (!ah.c(userData.bgurl)) {
            d.a().a(userData.bgurl, this.f1990a, h.a().k());
        }
        if (!ah.c(userData.intro)) {
            this.f.setText(userData.intro);
        }
        this.e.setText(userData.sex);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, ?[OBJECT, ARRAY], int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    private void d() {
        View inflate = LayoutInflater.from(this).inflate((int) R.layout.choice_set_sex, (ViewGroup) null, false);
        ((RadioGroup) inflate.findViewById(R.id.selector_group)).setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                switch (i) {
                    case R.id.male:
                        UserInfoEditActivity.this.e.setText("男");
                        break;
                    case R.id.female:
                        UserInfoEditActivity.this.e.setText("女");
                        break;
                    case R.id.secket:
                        UserInfoEditActivity.this.e.setText("保密");
                        break;
                }
                if (UserInfoEditActivity.this.m != null) {
                    UserInfoEditActivity.this.m.dismiss();
                }
            }
        });
        this.m = new b.a(this).a(inflate).a();
        this.m.show();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, ?[OBJECT, ARRAY], int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    private void e() {
        View inflate = LayoutInflater.from(this).inflate((int) R.layout.choise_choose_pic, (ViewGroup) null, false);
        inflate.findViewById(R.id.camera).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
                String unused = UserInfoEditActivity.this.n = String.valueOf(System.currentTimeMillis()) + ".jpg";
                intent.putExtra("output", Uri.fromFile(new File(l.a(6), UserInfoEditActivity.this.n)));
                if (RingDDApp.b().getApplicationContext().getPackageManager().resolveActivity(intent, 65536) == null) {
                    com.shoujiduoduo.util.widget.d.a("请先安装相机");
                    return;
                }
                UserInfoEditActivity.this.startActivityForResult(intent, 2);
                if (UserInfoEditActivity.this.m != null) {
                    UserInfoEditActivity.this.m.dismiss();
                }
            }
        });
        inflate.findViewById(R.id.album).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent intent = new Intent("android.intent.action.PICK");
                intent.setDataAndType(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, "image/*");
                if (RingDDApp.b().getApplicationContext().getPackageManager().resolveActivity(intent, 65536) == null) {
                    com.shoujiduoduo.util.widget.d.a("请先安装相册");
                    return;
                }
                UserInfoEditActivity.this.startActivityForResult(intent, 2);
                if (UserInfoEditActivity.this.m != null) {
                    UserInfoEditActivity.this.m.dismiss();
                }
            }
        });
        this.m = new b.a(this).a(inflate).a();
        this.m.show();
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        Bundle extras;
        Uri fromFile;
        super.onActivityResult(i2, i3, intent);
        if (i3 == -1) {
            switch (i2) {
                case 2:
                    if (intent != null) {
                        fromFile = intent.getData();
                    } else if (this.n != null) {
                        fromFile = Uri.fromFile(new File(l.a(6), this.n));
                    } else {
                        return;
                    }
                    DisplayMetrics displayMetrics = new DisplayMetrics();
                    getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                    if (fromFile == null) {
                        com.shoujiduoduo.util.widget.d.a("相机未提供图片,换个相机试试");
                        return;
                    }
                    com.shoujiduoduo.base.a.a.a("UserInfoEditActivity", "捕获图片结束，准备截取图片");
                    if (this.h.equals("head")) {
                        a(fromFile, (displayMetrics.widthPixels * 3) / 4, (displayMetrics.widthPixels * 3) / 4, 3);
                        return;
                    } else {
                        a(fromFile, (displayMetrics.widthPixels * 3) / 4, (((displayMetrics.widthPixels * 3) / 4) * 7) / 10, 3);
                        return;
                    }
                case 3:
                    Bitmap bitmap = null;
                    Uri data = intent.getData();
                    if (data != null) {
                        bitmap = BitmapFactory.decodeFile(data.getPath());
                    }
                    if (bitmap == null && (extras = intent.getExtras()) != null) {
                        bitmap = (Bitmap) extras.get("data");
                    }
                    if (bitmap != null) {
                        a(bitmap);
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    }

    private void a(Bitmap bitmap) {
        String str;
        String format = new SimpleDateFormat("yyyyMMddHHmmss", Locale.getDefault()).format(new Date());
        if (this.h.equals("head")) {
            str = l.a(2) + "user_head_" + format + ".jpg";
        } else {
            str = l.a(2) + "user_bkg_" + format + ".jpg";
        }
        com.shoujiduoduo.base.a.a.a("UserInfoEditActivity", " 压缩图片, 路径：" + str);
        if (g.a(bitmap, Bitmap.CompressFormat.JPEG, 90, str)) {
            com.shoujiduoduo.base.a.a.a("UserInfoEditActivity", "保存图片成功");
            String b2 = b.a.FILE.b(str);
            if (this.h.equals("head")) {
                this.i = str;
                d.a().a(b2, this.b, h.a().d());
                return;
            }
            this.k = str;
            d.a().a(b2, this.f1990a, h.a().k());
            return;
        }
        com.shoujiduoduo.base.a.a.e("UserInfoEditActivity", "保存图片出错");
        com.shoujiduoduo.util.widget.d.a("保存图片出错");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public void a(Uri uri, int i2, int i3, int i4) {
        Intent intent = new Intent(this, CropImageActivity.class);
        intent.setDataAndType(uri, "image/*");
        intent.putExtra("crop", "true");
        intent.putExtra("aspectX", i2);
        intent.putExtra("aspectY", i3);
        intent.putExtra("outputX", i2);
        intent.putExtra("outputY", i3);
        intent.putExtra("outputFormat", Bitmap.CompressFormat.JPEG.toString());
        intent.putExtra("noFaceDetection", true);
        intent.putExtra("return-data", false);
        intent.putExtra("save_path", l.a(6) + "skin_" + String.valueOf(System.currentTimeMillis()) + ".png");
        startActivityForResult(intent, i4);
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.back:
                setResult(0);
                finish();
                return;
            case R.id.btn_save:
                f();
                return;
            case R.id.btn_change_head:
                this.h = "head";
                e();
                return;
            case R.id.btn_change_bkg:
                this.h = "bkg";
                e();
                return;
            case R.id.btn_copy_ddid:
                if (this.g != null) {
                    if (Build.VERSION.SDK_INT >= 11) {
                        ((ClipboardManager) getSystemService("clipboard")).setText(this.g.ddid);
                    } else {
                        ((android.text.ClipboardManager) getSystemService("clipboard")).setText(this.g.ddid);
                    }
                    com.shoujiduoduo.util.widget.d.a("已复制到剪贴板");
                    return;
                }
                return;
            case R.id.tv_sex:
                d();
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: package-private */
    public void a(String str) {
        if (this.o == null) {
            this.o = new a(this);
            this.o.a(str);
            this.o.a(true);
            this.o.b(false);
            this.o.show();
        }
    }

    /* access modifiers changed from: package-private */
    public void c() {
        if (this.o != null) {
            this.o.dismiss();
            this.o = null;
        }
    }

    private void f() {
        a("请稍候...");
        i.a(new Runnable() {
            public void run() {
                if (!ah.c(UserInfoEditActivity.this.k)) {
                    if (c.a(UserInfoEditActivity.this.k, q.d(UserInfoEditActivity.this.k), c.a.userBkg)) {
                        String unused = UserInfoEditActivity.this.l = c.a(q.d(UserInfoEditActivity.this.k), c.a.userBkg);
                        com.shoujiduoduo.base.a.a.a("UserInfoEditActivity", "上传bkg成功， url:" + UserInfoEditActivity.this.l);
                    } else {
                        com.shoujiduoduo.base.a.a.a("UserInfoEditActivity", "上传bkg失败");
                    }
                }
                if (!ah.c(UserInfoEditActivity.this.i)) {
                    if (c.a(UserInfoEditActivity.this.i, q.d(UserInfoEditActivity.this.i), c.a.userHead)) {
                        String unused2 = UserInfoEditActivity.this.j = c.a(q.d(UserInfoEditActivity.this.i), c.a.userHead);
                        com.shoujiduoduo.base.a.a.a("UserInfoEditActivity", "上传bkg成功， url:" + UserInfoEditActivity.this.j);
                    } else {
                        com.shoujiduoduo.base.a.a.a("UserInfoEditActivity", "上传head失败");
                    }
                }
                Message message = new Message();
                message.what = 0;
                UserInfoEditActivity.this.r.sendMessage(message);
            }
        });
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x0099  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x00f3  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void g() {
        /*
            r9 = this;
            r2 = 0
            r0 = 1
            com.shoujiduoduo.b.e.a r1 = com.shoujiduoduo.a.b.b.g()
            com.shoujiduoduo.base.bean.UserInfo r3 = r1.c()
            org.json.JSONObject r4 = new org.json.JSONObject
            r4.<init>()
            com.shoujiduoduo.base.bean.UserData r5 = new com.shoujiduoduo.base.bean.UserData
            r5.<init>()
            android.widget.EditText r1 = r9.c     // Catch:{ JSONException -> 0x00ec }
            android.text.Editable r1 = r1.getText()     // Catch:{ JSONException -> 0x00ec }
            java.lang.String r1 = r1.toString()     // Catch:{ JSONException -> 0x00ec }
            com.shoujiduoduo.base.bean.UserData r6 = r9.g     // Catch:{ JSONException -> 0x00ec }
            java.lang.String r6 = r6.userName     // Catch:{ JSONException -> 0x00ec }
            boolean r6 = r6.equals(r1)     // Catch:{ JSONException -> 0x00ec }
            if (r6 != 0) goto L_0x0108
            java.lang.String r6 = "usernamev1"
            java.lang.String r7 = com.shoujiduoduo.util.r.b(r1)     // Catch:{ JSONException -> 0x0101 }
            r4.put(r6, r7)     // Catch:{ JSONException -> 0x0101 }
            r5.userName = r1     // Catch:{ JSONException -> 0x0101 }
            r1 = r0
        L_0x0034:
            java.lang.String r6 = r9.l     // Catch:{ JSONException -> 0x0106 }
            boolean r6 = com.shoujiduoduo.util.ah.c(r6)     // Catch:{ JSONException -> 0x0106 }
            if (r6 != 0) goto L_0x0048
            java.lang.String r1 = "bgurl"
            java.lang.String r6 = r9.l     // Catch:{ JSONException -> 0x0101 }
            r4.put(r1, r6)     // Catch:{ JSONException -> 0x0101 }
            java.lang.String r1 = r9.l     // Catch:{ JSONException -> 0x0101 }
            r5.bgurl = r1     // Catch:{ JSONException -> 0x0101 }
            r1 = r0
        L_0x0048:
            java.lang.String r6 = r9.j     // Catch:{ JSONException -> 0x0106 }
            boolean r6 = com.shoujiduoduo.util.ah.c(r6)     // Catch:{ JSONException -> 0x0106 }
            if (r6 != 0) goto L_0x005c
            java.lang.String r1 = "headurlv1"
            java.lang.String r6 = r9.j     // Catch:{ JSONException -> 0x0101 }
            r4.put(r1, r6)     // Catch:{ JSONException -> 0x0101 }
            java.lang.String r1 = r9.j     // Catch:{ JSONException -> 0x0101 }
            r5.headUrl = r1     // Catch:{ JSONException -> 0x0101 }
            r1 = r0
        L_0x005c:
            android.widget.TextView r6 = r9.e     // Catch:{ JSONException -> 0x0106 }
            java.lang.CharSequence r6 = r6.getText()     // Catch:{ JSONException -> 0x0106 }
            java.lang.String r6 = r6.toString()     // Catch:{ JSONException -> 0x0106 }
            com.shoujiduoduo.base.bean.UserData r7 = r9.g     // Catch:{ JSONException -> 0x0106 }
            java.lang.String r7 = r7.sex     // Catch:{ JSONException -> 0x0106 }
            boolean r7 = r7.equals(r6)     // Catch:{ JSONException -> 0x0106 }
            if (r7 != 0) goto L_0x0078
            java.lang.String r1 = "sex"
            r4.put(r1, r6)     // Catch:{ JSONException -> 0x0101 }
            r5.sex = r6     // Catch:{ JSONException -> 0x0101 }
            r1 = r0
        L_0x0078:
            android.widget.EditText r6 = r9.f     // Catch:{ JSONException -> 0x0106 }
            android.text.Editable r6 = r6.getText()     // Catch:{ JSONException -> 0x0106 }
            java.lang.String r6 = r6.toString()     // Catch:{ JSONException -> 0x0106 }
            com.shoujiduoduo.base.bean.UserData r7 = r9.g     // Catch:{ JSONException -> 0x0106 }
            java.lang.String r7 = r7.intro     // Catch:{ JSONException -> 0x0106 }
            boolean r7 = r7.equals(r6)     // Catch:{ JSONException -> 0x0106 }
            if (r7 != 0) goto L_0x00f1
            java.lang.String r1 = "intro"
            java.lang.String r7 = com.shoujiduoduo.util.r.b(r6)     // Catch:{ JSONException -> 0x0101 }
            r4.put(r1, r7)     // Catch:{ JSONException -> 0x0101 }
            r5.intro = r6     // Catch:{ JSONException -> 0x0101 }
        L_0x0097:
            if (r0 == 0) goto L_0x00f3
            java.lang.String r0 = "UserInfoEditActivity"
            java.lang.String r1 = "有变化，提交修改"
            com.shoujiduoduo.base.a.a.a(r0, r1)
            java.lang.String r0 = "UserInfoEditActivity"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "json:"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = r4.toString()
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            com.shoujiduoduo.base.a.a.a(r0, r1)
            java.lang.String r0 = "setuserinfo"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "&uid="
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = r3.getUid()
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = "&tuid="
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = r3.getUid()
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            com.shoujiduoduo.ui.user.UserInfoEditActivity$6 r2 = new com.shoujiduoduo.ui.user.UserInfoEditActivity$6
            r2.<init>(r5)
            com.shoujiduoduo.util.t.a(r0, r1, r4, r2)
        L_0x00eb:
            return
        L_0x00ec:
            r0 = move-exception
            r1 = r2
        L_0x00ee:
            r0.printStackTrace()
        L_0x00f1:
            r0 = r1
            goto L_0x0097
        L_0x00f3:
            java.lang.String r0 = "UserInfoEditActivity"
            java.lang.String r1 = "无改变，quit"
            com.shoujiduoduo.base.a.a.a(r0, r1)
            r9.setResult(r2)
            r9.finish()
            goto L_0x00eb
        L_0x0101:
            r1 = move-exception
            r8 = r1
            r1 = r0
            r0 = r8
            goto L_0x00ee
        L_0x0106:
            r0 = move-exception
            goto L_0x00ee
        L_0x0108:
            r1 = r2
            goto L_0x0034
        */
        throw new UnsupportedOperationException("Method not decompiled: com.shoujiduoduo.ui.user.UserInfoEditActivity.g():void");
    }
}
