package com.shoujiduoduo.ui.adwall;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.igexin.download.Downloads;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.ui.settings.AboutActivity;
import com.shoujiduoduo.ui.settings.UserFeedbackActivity;
import com.shoujiduoduo.util.g;
import com.shoujiduoduo.util.n;
import com.shoujiduoduo.util.o;
import com.shoujiduoduo.util.widget.d;

public class MoreOptionFrag extends Fragment implements AdapterView.OnItemClickListener {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public int f1455a;
    private ListView b;
    private BaseAdapter c;
    private LinearLayout d;
    private LinearLayout e;
    private RelativeLayout f;
    private View g;
    /* access modifiers changed from: private */
    public ProgressDialog h;
    /* access modifiers changed from: private */
    public Handler i = new Handler() {
        public void handleMessage(Message message) {
            switch (message.what) {
                case Downloads.STATUS_SUCCESS:
                    com.shoujiduoduo.base.a.a.a("MoreOptionFrag", "MESSAGE_OK_CLEAR_CACHE Received.");
                    MoreOptionFrag.this.a();
                    return;
                case 206:
                    com.shoujiduoduo.base.a.a.a("MoreOptionFrag", "MESSAGE_FINISH_CLEAR_CACHE");
                    if (MoreOptionFrag.this.h != null) {
                        MoreOptionFrag.this.h.cancel();
                    }
                    Toast.makeText(MoreOptionFrag.this.getActivity(), (int) R.string.clean_cache_suc, 0).show();
                    return;
                default:
                    return;
            }
        }
    };

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        String str;
        View inflate = layoutInflater.inflate((int) R.layout.more_options, viewGroup, false);
        this.d = (LinearLayout) inflate.findViewById(R.id.more_options_entrance_panel);
        this.e = (LinearLayout) inflate.findViewById(R.id.user_feedback_panel);
        this.f = (RelativeLayout) inflate.findViewById(R.id.about_info_panel);
        if (g.k().contains("anzhi")) {
            this.f1455a = 4;
        } else {
            this.f1455a = 3;
        }
        String str2 = "";
        try {
            str2 = getActivity().getPackageManager().getPackageInfo(getContext().getPackageName(), 0).versionName;
            str = str2.substring(0, str2.lastIndexOf(46));
        } catch (PackageManager.NameNotFoundException e2) {
            PackageManager.NameNotFoundException nameNotFoundException = e2;
            nameNotFoundException.printStackTrace();
            str = str2;
        }
        TextView textView = (TextView) inflate.findViewById(R.id.about_app_name);
        if (textView != null) {
            textView.setText(((Object) getResources().getText(R.string.app_name)) + " " + str);
        }
        this.g = this.d;
        this.b = (ListView) inflate.findViewById(R.id.more_options_list);
        this.c = new a(getContext());
        this.b.setAdapter((ListAdapter) this.c);
        this.b.setOnItemClickListener(this);
        ((TextView) inflate.findViewById(R.id.about_app_intro)).setText(getResources().getString(R.string.app_intro1) + "\n" + getResources().getString(R.string.app_intro2) + "\n" + getResources().getString(R.string.app_intro3) + "\n" + getResources().getString(R.string.app_intro4) + "\n" + getResources().getString(R.string.app_intro5));
        return inflate;
    }

    public void onDestroyView() {
        super.onDestroyView();
    }

    /* access modifiers changed from: private */
    public void a() {
        this.h = new ProgressDialog(getActivity());
        this.h.setProgressStyle(0);
        this.h.setIndeterminate(true);
        this.h.setTitle("");
        this.h.setMessage(getResources().getString(R.string.cleaning_cache));
        this.h.setCancelable(false);
        this.h.show();
        new Thread() {
            public void run() {
                n.a(MoreOptionFrag.this.getContext()).a();
                MoreOptionFrag.this.i.sendEmptyMessage(206);
            }
        }.start();
    }

    public void onItemClick(AdapterView<?> adapterView, View view, int i2, long j) {
        if (this.g == this.d) {
            switch (i2) {
                case 0:
                    com.shoujiduoduo.base.a.a.a("MoreOptionFrag", "enter User-Feedback panel.");
                    getActivity().startActivity(new Intent(RingDDApp.c(), UserFeedbackActivity.class));
                    return;
                case 1:
                    startActivity(new Intent(RingDDApp.c(), AboutActivity.class));
                    return;
                case 2:
                    new AlertDialog.Builder(getActivity()).setTitle((int) R.string.hint).setMessage((int) R.string.clean_cache_confirm).setIcon(17301543).setPositiveButton((int) R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialogInterface, int i) {
                            MoreOptionFrag.this.i.sendEmptyMessage(Downloads.STATUS_SUCCESS);
                        }
                    }).setNegativeButton((int) R.string.cancel, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialogInterface, int i) {
                        }
                    }).show();
                    return;
                case 3:
                    String a2 = com.umeng.b.a.a().a(getContext(), "anzhi_down_url");
                    if (TextUtils.isEmpty(a2)) {
                        a2 = "http://m.anzhi.com/redirect.php?do=dlapk&puid=1140";
                    }
                    o.a(getContext()).a(a2, "安智市场", o.a.immediatelly, false);
                    d.a("正在为您下载安智市场");
                    return;
                default:
                    return;
            }
        }
    }

    class a extends BaseAdapter {
        private Context b;
        private LayoutInflater c;

        private a(Context context) {
            this.b = context;
            this.c = LayoutInflater.from(context);
        }

        public int getCount() {
            return MoreOptionFrag.this.f1455a;
        }

        public Object getItem(int i) {
            return Integer.valueOf(i);
        }

        public long getItemId(int i) {
            return (long) i;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [?, android.view.ViewGroup, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        public View getView(int i, View view, ViewGroup viewGroup) {
            if (view == null) {
                view = this.c.inflate((int) R.layout.more_options_list_item, viewGroup, false);
            }
            TextView textView = (TextView) view.findViewById(R.id.option_title);
            TextView textView2 = (TextView) view.findViewById(R.id.option_des);
            ImageView imageView = (ImageView) view.findViewById(R.id.icon);
            switch (i) {
                case 0:
                    textView.setText((int) R.string.more_options_feedback);
                    break;
                case 1:
                    textView.setText((int) R.string.more_options_help_about);
                    break;
                case 2:
                    textView.setText((int) R.string.more_options_clear_cache);
                    break;
                case 3:
                    textView2.setVisibility(0);
                    imageView.setVisibility(0);
                    textView.setText("安智市场");
                    textView2.setText("中国最大的安卓手机应用市场");
                    break;
            }
            return view;
        }
    }
}
