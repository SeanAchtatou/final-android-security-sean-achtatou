package com.shoujiduoduo.base.a;

import android.os.Environment;
import android.util.Log;
import java.io.File;
import java.io.PrintWriter;
import java.io.StringWriter;

/* compiled from: Logger */
public class b {

    /* renamed from: a  reason: collision with root package name */
    public static boolean f1396a = true;
    private static final Object b = new Object();
    private static final String c = Environment.getExternalStorageDirectory().getAbsolutePath();
    private static final String d = (c + File.separator + "shoujiduoduo/Ring/ddlog.txt");

    public static int a(String str, String str2) {
        return f1396a ? Log.v(str, str2) : a(2, str, str2);
    }

    public static int b(String str, String str2) {
        return f1396a ? Log.d(str, str2) : a(3, str, str2);
    }

    public static int c(String str, String str2) {
        return f1396a ? Log.w(str, str2) : a(5, str, str2);
    }

    public static int d(String str, String str2) {
        return f1396a ? Log.i(str, str2) : a(4, str, str2);
    }

    public static int e(String str, String str2) {
        return f1396a ? Log.e(str, str2) : a(6, str, str2);
    }

    public static int a(String str, Throwable th) {
        return e(str, a(th));
    }

    public static String a(Throwable th) {
        if (th == null) {
            return "";
        }
        StringWriter stringWriter = new StringWriter();
        th.printStackTrace(new PrintWriter(stringWriter));
        return stringWriter.toString();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileWriter.<init>(java.io.File, boolean):void throws java.io.IOException}
     arg types: [java.io.File, int]
     candidates:
      ClspMth{java.io.FileWriter.<init>(java.lang.String, boolean):void throws java.io.IOException}
      ClspMth{java.io.FileWriter.<init>(java.io.File, boolean):void throws java.io.IOException} */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x009b A[SYNTHETIC, Splitter:B:18:0x009b] */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00ac A[SYNTHETIC, Splitter:B:31:0x00ac] */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x00b5 A[SYNTHETIC, Splitter:B:36:0x00b5] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:12:0x0095=Splitter:B:12:0x0095, B:38:0x00b8=Splitter:B:38:0x00b8, B:20:0x009e=Splitter:B:20:0x009e} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static int a(int r8, java.lang.String r9, java.lang.String r10) {
        /*
            r3 = 1
            r0 = 0
            r1 = 8
            java.lang.String[] r1 = new java.lang.String[r1]
            java.lang.String r2 = ""
            r1[r0] = r2
            java.lang.String r2 = ""
            r1[r3] = r2
            r2 = 2
            java.lang.String r3 = "V"
            r1[r2] = r3
            r2 = 3
            java.lang.String r3 = "D"
            r1[r2] = r3
            r2 = 4
            java.lang.String r3 = "I"
            r1[r2] = r3
            r2 = 5
            java.lang.String r3 = "W"
            r1[r2] = r3
            r2 = 6
            java.lang.String r3 = "E"
            r1[r2] = r3
            r2 = 7
            java.lang.String r3 = "A"
            r1[r2] = r3
            java.text.SimpleDateFormat r2 = new java.text.SimpleDateFormat
            java.lang.String r3 = "[MM-dd hh:mm:ss.SSS]"
            r2.<init>(r3)
            java.util.Date r3 = new java.util.Date
            r3.<init>()
            java.lang.String r2 = r2.format(r3)
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            r3.append(r2)
            java.lang.String r2 = "\t"
            r3.append(r2)
            r1 = r1[r8]
            r3.append(r1)
            java.lang.String r1 = "/"
            r3.append(r1)
            r3.append(r9)
            int r1 = android.os.Process.myPid()
            java.lang.String r2 = "("
            r3.append(r2)
            r3.append(r1)
            java.lang.String r1 = "):"
            r3.append(r1)
            r3.append(r10)
            java.lang.String r1 = "\n"
            r3.append(r1)
            r1 = 0
            java.lang.Object r4 = com.shoujiduoduo.base.a.b.b
            monitor-enter(r4)
            java.io.File r5 = new java.io.File     // Catch:{ FileNotFoundException -> 0x0097, IOException -> 0x00a3, all -> 0x00b2 }
            java.lang.String r2 = com.shoujiduoduo.base.a.b.d     // Catch:{ FileNotFoundException -> 0x0097, IOException -> 0x00a3, all -> 0x00b2 }
            r5.<init>(r2)     // Catch:{ FileNotFoundException -> 0x0097, IOException -> 0x00a3, all -> 0x00b2 }
            boolean r2 = r5.exists()     // Catch:{ FileNotFoundException -> 0x0097, IOException -> 0x00a3, all -> 0x00b2 }
            if (r2 != 0) goto L_0x0083
            r5.createNewFile()     // Catch:{ FileNotFoundException -> 0x0097, IOException -> 0x00a3, all -> 0x00b2 }
        L_0x0083:
            java.io.FileWriter r2 = new java.io.FileWriter     // Catch:{ FileNotFoundException -> 0x0097, IOException -> 0x00a3, all -> 0x00b2 }
            r6 = 1
            r2.<init>(r5, r6)     // Catch:{ FileNotFoundException -> 0x0097, IOException -> 0x00a3, all -> 0x00b2 }
            java.lang.String r1 = r3.toString()     // Catch:{ FileNotFoundException -> 0x00c4, IOException -> 0x00c2 }
            r2.write(r1)     // Catch:{ FileNotFoundException -> 0x00c4, IOException -> 0x00c2 }
            if (r2 == 0) goto L_0x0095
            r2.close()     // Catch:{ IOException -> 0x00b9 }
        L_0x0095:
            monitor-exit(r4)     // Catch:{ all -> 0x00a0 }
        L_0x0096:
            return r0
        L_0x0097:
            r0 = move-exception
        L_0x0098:
            r0 = -1
            if (r1 == 0) goto L_0x009e
            r1.close()     // Catch:{ IOException -> 0x00bb }
        L_0x009e:
            monitor-exit(r4)     // Catch:{ all -> 0x00a0 }
            goto L_0x0096
        L_0x00a0:
            r0 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x00a0 }
            throw r0
        L_0x00a3:
            r2 = move-exception
            r7 = r2
            r2 = r1
            r1 = r7
        L_0x00a7:
            r1.printStackTrace()     // Catch:{ all -> 0x00bf }
            if (r2 == 0) goto L_0x0095
            r2.close()     // Catch:{ IOException -> 0x00b0 }
            goto L_0x0095
        L_0x00b0:
            r1 = move-exception
            goto L_0x0095
        L_0x00b2:
            r0 = move-exception
        L_0x00b3:
            if (r1 == 0) goto L_0x00b8
            r1.close()     // Catch:{ IOException -> 0x00bd }
        L_0x00b8:
            throw r0     // Catch:{ all -> 0x00a0 }
        L_0x00b9:
            r1 = move-exception
            goto L_0x0095
        L_0x00bb:
            r1 = move-exception
            goto L_0x009e
        L_0x00bd:
            r1 = move-exception
            goto L_0x00b8
        L_0x00bf:
            r0 = move-exception
            r1 = r2
            goto L_0x00b3
        L_0x00c2:
            r1 = move-exception
            goto L_0x00a7
        L_0x00c4:
            r0 = move-exception
            r1 = r2
            goto L_0x0098
        */
        throw new UnsupportedOperationException("Method not decompiled: com.shoujiduoduo.base.a.b.a(int, java.lang.String, java.lang.String):int");
    }
}
