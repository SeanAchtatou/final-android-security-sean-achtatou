package com.shoujiduoduo.base.a;

import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.util.g;

/* compiled from: DDLog */
public final class a {

    /* renamed from: a  reason: collision with root package name */
    public static boolean f1395a = false;
    public static final g.b b = g.b.none;

    static {
        b.f1396a = true;
    }

    public static void a(String str, String str2) {
        if (f1395a) {
            b.b("DDLog: " + str, str2);
        }
    }

    public static void b(String str, String str2) {
        if (f1395a) {
            b.d("DDlog: " + str, str2);
        }
    }

    public static void c(String str, String str2) {
        if (f1395a) {
            b.e("DDlog: " + str, str2);
        }
    }

    public static void d(String str, String str2) {
        if (f1395a) {
            b.a("DDlog: " + str, str2);
        }
    }

    public static void e(String str, String str2) {
        if (f1395a) {
            b.c("DDlog: " + str, str2);
        }
    }

    public static void a(Exception exc) {
        if (f1395a) {
            b.a("DuoduoException", exc);
        }
    }

    public static void a() {
        if (Thread.currentThread().getId() != RingDDApp.d()) {
            c("DDLog", "必须在主线程中调用");
        }
    }

    public static void a(Class<?> cls, Object obj) {
        if (!cls.isInstance(obj)) {
            c("DDLog", obj.toString() + "必须是" + cls.toString() + "的子类");
        }
    }
}
