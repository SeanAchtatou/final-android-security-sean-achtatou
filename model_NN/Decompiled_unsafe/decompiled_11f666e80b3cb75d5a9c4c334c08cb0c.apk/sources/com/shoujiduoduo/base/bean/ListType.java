package com.shoujiduoduo.base.bean;

public final class ListType {

    public enum LIST_TYPE {
        list_artist,
        list_collect,
        list_user,
        list_comment,
        list_user_collect,
        list_user_favorite,
        list_user_make,
        list_ring_normal,
        list_ring_artist,
        list_ring_collect,
        list_ring_search,
        list_ring_user_upload,
        list_ring_concern,
        list_concern_feeds,
        list_ring_cmcc,
        list_ring_ctcc,
        list_ring_cucc,
        sys_ringtone,
        sys_notify,
        sys_alarm,
        list_simple
    }
}
