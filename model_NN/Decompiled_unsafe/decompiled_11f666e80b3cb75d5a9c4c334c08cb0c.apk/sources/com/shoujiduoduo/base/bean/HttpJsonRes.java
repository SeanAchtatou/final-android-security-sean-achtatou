package com.shoujiduoduo.base.bean;

public class HttpJsonRes {
    private String msg = "";
    private String result = "";

    public String getResult() {
        return this.result;
    }

    public void setResult(String str) {
        this.result = str;
    }

    public void setMsg(String str) {
        this.msg = str;
    }

    public String getMsg() {
        return this.msg;
    }
}
