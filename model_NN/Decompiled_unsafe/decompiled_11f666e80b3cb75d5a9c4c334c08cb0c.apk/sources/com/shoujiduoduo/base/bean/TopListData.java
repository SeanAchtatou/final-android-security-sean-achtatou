package com.shoujiduoduo.base.bean;

public class TopListData {
    public static String artist_type = "artist";
    public static String collect_type = "collect";
    public static String concern_type = "concern";
    public static String html_type = "html";
    public static String list_type = "list";
    public int id;
    public String name;
    public String type;
    public String url;
}
