package com.shoujiduoduo.base.bean;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import com.shoujiduoduo.util.m;

public class RingData implements Parcelable {
    public static final Parcelable.Creator<RingData> CREATOR = new Parcelable.Creator<RingData>() {
        public RingData createFromParcel(Parcel parcel) {
            RingData ringData = new RingData();
            ringData.name = parcel.readString();
            ringData.artist = parcel.readString();
            ringData.rid = parcel.readString();
            ringData.baiduURL = parcel.readString();
            ringData.score = parcel.readInt();
            ringData.duration = parcel.readInt();
            ringData.playcnt = parcel.readInt();
            String unused = ringData.lowAACURL = parcel.readString();
            int unused2 = ringData.lowAACBitrate = parcel.readInt();
            String unused3 = ringData.highAACURL = parcel.readString();
            int unused4 = ringData.highAACBitrate = parcel.readInt();
            ringData.cid = parcel.readString();
            ringData.valid = parcel.readString();
            ringData.price = parcel.readInt();
            ringData.hasmedia = parcel.readInt();
            ringData.singerId = parcel.readString();
            ringData.isNew = parcel.readInt();
            ringData.ctcid = parcel.readString();
            ringData.ctvalid = parcel.readString();
            ringData.ctprice = parcel.readInt();
            ringData.cthasmedia = parcel.readInt();
            ringData.ctVip = parcel.readInt();
            ringData.ctWavUrl = parcel.readString();
            ringData.cuvip = parcel.readInt();
            ringData.cuftp = parcel.readString();
            ringData.cucid = parcel.readString();
            ringData.cusid = parcel.readString();
            ringData.cuvalid = parcel.readString();
            ringData.cuurl = parcel.readString();
            ringData.hasshow = parcel.readInt();
            ringData.uid = parcel.readString();
            ringData.isHot = parcel.readInt();
            ringData.userHead = parcel.readString();
            ringData.commentNum = parcel.readInt();
            ringData.date = parcel.readString();
            return ringData;
        }

        public RingData[] newArray(int i) {
            return new RingData[i];
        }
    };
    public String artist = "";
    public String baiduURL = "";
    private String baseUrl = "";
    public String cid = "";
    public int commentNum = 0;
    public int ctVip;
    public String ctWavUrl = "";
    public String ctcid = "";
    public int cthasmedia = 0;
    public int ctprice;
    public String ctvalid = "";
    public String cucid = "";
    public String cuftp = "";
    public String cusid = "";
    public String cuurl = "";
    public String cuvalid = "";
    public int cuvip;
    public String date = "";
    public int duration;
    public int hasmedia = 1;
    public int hasshow = 0;
    /* access modifiers changed from: private */
    public int highAACBitrate;
    /* access modifiers changed from: private */
    public String highAACURL = "";
    public int isHot = 0;
    public int isNew;
    public String localPath = "";
    /* access modifiers changed from: private */
    public int lowAACBitrate;
    /* access modifiers changed from: private */
    public String lowAACURL = "";
    private int mp3Bitrate;
    private String mp3URL = "";
    public String name = "";
    public int playcnt;
    public int price;
    public String rid = "";
    public int score;
    public String singerId = "";
    public String uid = "";
    public String userHead = "";
    public String valid = "";

    public String getBaseUrl() {
        return "http://" + m.a().b();
    }

    public void setBaseUrl(String str) {
        this.baseUrl = str;
    }

    public String getPlayHighAACUrl() {
        return getBaseUrl() + this.highAACURL;
    }

    public String getHighAACURL() {
        return this.highAACURL;
    }

    public void setHighAACURL(String str) {
        this.highAACURL = str;
    }

    public void setHighAACBitrate(int i) {
        this.highAACBitrate = i;
    }

    public int getHighAACBitrate() {
        return this.highAACBitrate;
    }

    public String getPlayLowAACURL() {
        return getBaseUrl() + this.lowAACURL;
    }

    public String getLowAACURL() {
        return this.lowAACURL;
    }

    public void setLowAACURL(String str) {
        this.lowAACURL = str;
    }

    public void setLowAACBitrate(int i) {
        this.lowAACBitrate = i;
    }

    public int getLowAACBitrate() {
        return this.lowAACBitrate;
    }

    public String getPlayMp3Url() {
        return getBaseUrl() + this.mp3URL;
    }

    public String getMp3URL() {
        return this.mp3URL;
    }

    public void setMp3URL(String str) {
        this.mp3URL = str;
    }

    public void setMp3Bitrate(int i) {
        this.mp3Bitrate = i;
    }

    public int getMp3Bitrate() {
        return this.mp3Bitrate;
    }

    public String getUid() {
        return this.uid;
    }

    public void setUid(String str) {
        this.uid = str;
    }

    public boolean hasMP3Url() {
        if (TextUtils.isEmpty(this.mp3URL) || this.mp3Bitrate <= 0) {
            return false;
        }
        return true;
    }

    public boolean hasAACUrl() {
        if (!TextUtils.isEmpty(this.highAACURL) && this.highAACBitrate > 0) {
            return true;
        }
        if (TextUtils.isEmpty(this.lowAACURL) || this.lowAACBitrate <= 0) {
            return false;
        }
        return true;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("name: ").append(this.name).append(", artist: ").append(this.artist).append(", uid:").append(this.uid).append(", rid: ").append(this.rid).append(", duration: ").append(this.duration).append(", score: ").append(this.score).append(", playcnt: ").append(this.playcnt);
        return sb.toString();
    }

    public int getRid() {
        int i = 0;
        try {
            i = Integer.valueOf(this.rid.equals("") ? "0" : this.rid).intValue();
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        if (this.cid.equals("") || i > 900000000 || this.hasmedia == 1) {
            return i;
        }
        return i + 1000000000;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.name);
        parcel.writeString(this.artist);
        parcel.writeString(this.rid);
        parcel.writeString(this.baiduURL);
        parcel.writeInt(this.score);
        parcel.writeInt(this.duration);
        parcel.writeInt(this.playcnt);
        parcel.writeString(this.lowAACURL);
        parcel.writeInt(this.lowAACBitrate);
        parcel.writeString(this.highAACURL);
        parcel.writeInt(this.highAACBitrate);
        parcel.writeString(this.cid);
        parcel.writeString(this.valid);
        parcel.writeInt(this.price);
        parcel.writeInt(this.hasmedia);
        parcel.writeString(this.singerId);
        parcel.writeInt(this.isNew);
        parcel.writeString(this.ctcid);
        parcel.writeString(this.ctvalid);
        parcel.writeInt(this.ctprice);
        parcel.writeInt(this.cthasmedia);
        parcel.writeInt(this.ctVip);
        parcel.writeString(this.ctWavUrl);
        parcel.writeInt(this.cuvip);
        parcel.writeString(this.cuftp);
        parcel.writeString(this.cucid);
        parcel.writeString(this.cusid);
        parcel.writeString(this.cuvalid);
        parcel.writeString(this.cuurl);
        parcel.writeInt(this.hasshow);
        parcel.writeString(this.uid);
        parcel.writeInt(this.isHot);
        parcel.writeString(this.userHead);
        parcel.writeInt(this.commentNum);
        parcel.writeString(this.date);
    }
}
