package com.shoujiduoduo.base.bean;

public class MakeRingData extends RingData {
    public String makeDate = "";
    public int makeType;
    public int percent;
    public int upload;

    public MakeRingData copy(RingData ringData) {
        this.artist = ringData.artist;
        this.baiduURL = ringData.baiduURL;
        this.cid = ringData.cid;
        this.rid = ringData.rid;
        this.duration = ringData.duration;
        this.hasmedia = ringData.hasmedia;
        setBaseUrl(ringData.getBaseUrl());
        setHighAACBitrate(ringData.getHighAACBitrate());
        setHighAACURL(ringData.getHighAACURL());
        this.isNew = ringData.isNew;
        setLowAACBitrate(ringData.getLowAACBitrate());
        setLowAACURL(ringData.getLowAACURL());
        this.singerId = ringData.singerId;
        this.name = ringData.name;
        this.playcnt = ringData.playcnt;
        this.price = ringData.price;
        this.score = ringData.score;
        this.valid = ringData.valid;
        this.localPath = ringData.localPath;
        if (ringData instanceof MakeRingData) {
            this.percent = ((MakeRingData) ringData).percent;
            this.upload = ((MakeRingData) ringData).upload;
            this.makeDate = ((MakeRingData) ringData).makeDate;
            this.makeType = ((MakeRingData) ringData).makeType;
        }
        return this;
    }
}
