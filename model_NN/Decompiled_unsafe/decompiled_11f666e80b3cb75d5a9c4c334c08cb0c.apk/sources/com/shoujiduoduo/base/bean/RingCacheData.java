package com.shoujiduoduo.base.bean;

import android.text.TextUtils;
import com.shoujiduoduo.util.m;
import com.shoujiduoduo.util.p;

public class RingCacheData {
    public String artist;
    private String baseUrl = "";
    public int bitrate;
    public String cid = "";
    public String ctcid = "";
    public int cthasmedia = 0;
    public String cucid = "";
    public String cuurl = "";
    public int downSize;
    public String format;
    public int hasmedia = 0;
    private int highAACBitrate;
    private String highAACURL;
    private int lowAACBitrate;
    private String lowAACURL;
    private int mp3Bitrate;
    private String mp3URL;
    public String name;
    private String path = "";
    public int rid;
    public int totalSize;
    public String url;

    public RingCacheData(String str, String str2, int i, int i2, int i3, int i4, String str3, String str4) {
        this.name = str;
        this.artist = str2;
        this.rid = i;
        this.downSize = i2;
        this.totalSize = i3;
        this.bitrate = i4;
        this.format = str3;
        this.url = str4;
    }

    public String getBaseUrl() {
        return "http://" + m.a().b();
    }

    public void setBaseUrl(String str) {
        this.baseUrl = str;
    }

    public String getPlayHighAACURL() {
        return getBaseUrl() + this.highAACURL;
    }

    public String getHighAACURL() {
        return this.highAACURL;
    }

    public void setHighAACURL(String str) {
        this.highAACURL = str;
    }

    public void setHighAACBitrate(int i) {
        this.highAACBitrate = i;
    }

    public int getHighAACBitrate() {
        return this.highAACBitrate;
    }

    public String getPlayLowAACURL() {
        return getBaseUrl() + this.lowAACURL;
    }

    public String getLowAACURL() {
        return this.lowAACURL;
    }

    public void setLowAACURL(String str) {
        this.lowAACURL = str;
    }

    public void setLowAACBitrate(int i) {
        this.lowAACBitrate = i;
    }

    public int getLowAACBitrate() {
        return this.lowAACBitrate;
    }

    public String getPlayMp3Url() {
        return getBaseUrl() + this.mp3URL;
    }

    public String getMp3URL() {
        return this.mp3URL;
    }

    public void setMp3URL(String str) {
        this.mp3URL = str;
    }

    public void setMp3Bitrate(int i) {
        this.mp3Bitrate = i;
    }

    public int getMp3Bitrate() {
        return this.mp3Bitrate;
    }

    public boolean hasMP3Url() {
        if (TextUtils.isEmpty(this.mp3URL) || this.mp3Bitrate <= 0) {
            return false;
        }
        return true;
    }

    public boolean hasAACUrl() {
        if (!TextUtils.isEmpty(this.highAACURL) && this.highAACBitrate > 0) {
            return true;
        }
        if (TextUtils.isEmpty(this.lowAACURL) || this.lowAACBitrate <= 0) {
            return false;
        }
        return true;
    }

    public boolean hasHighAACUrl() {
        if (TextUtils.isEmpty(this.highAACURL) || this.highAACBitrate <= 0) {
            return false;
        }
        return true;
    }

    public boolean hasLowAACUrl() {
        if (TextUtils.isEmpty(this.lowAACURL) || this.lowAACBitrate <= 0) {
            return false;
        }
        return true;
    }

    public void setPath(String str) {
        this.path = str;
    }

    public String getSongPath() {
        return this.path;
    }

    public static String oldGetSongPath(int i, String str) {
        StringBuilder append = new StringBuilder().append(p.c()).append(i).append(".");
        if (str == null || str.length() == 0) {
            str = "mp3";
        }
        return append.append(str).toString();
    }
}
