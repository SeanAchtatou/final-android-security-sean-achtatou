package com.shoujiduoduo.b.a;

import com.shoujiduoduo.b.a.b;
import com.shoujiduoduo.b.a.e;
import com.shoujiduoduo.b.a.g;
import java.util.ArrayList;

/* compiled from: AdMgrImpl */
public class a implements f {

    /* renamed from: a  reason: collision with root package name */
    private b f1288a = new b();
    private g b = new g();
    private e c = new e();

    public void a() {
        this.f1288a.a();
        this.b.a();
        this.c.a();
    }

    public void b() {
        this.f1288a.c();
        this.b.b();
        this.c.c();
    }

    public boolean c() {
        return this.f1288a.b();
    }

    public ArrayList<b.a> d() {
        if (this.f1288a.b()) {
            return this.f1288a.d();
        }
        return null;
    }

    public int e() {
        if (this.f1288a.b()) {
            return this.f1288a.e();
        }
        return 0;
    }

    public boolean f() {
        return this.b.c();
    }

    public g.a g() {
        if (this.b.c()) {
            return this.b.d();
        }
        return null;
    }

    public void h() {
        this.c.b();
    }

    public e.a i() {
        return this.c.d();
    }
}
