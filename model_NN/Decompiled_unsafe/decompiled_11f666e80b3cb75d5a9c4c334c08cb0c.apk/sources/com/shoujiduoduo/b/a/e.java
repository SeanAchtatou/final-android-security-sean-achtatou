package com.shoujiduoduo.b.a;

import android.view.View;
import com.baidu.mobad.feeds.BaiduNative;
import com.baidu.mobad.feeds.NativeErrorCode;
import com.baidu.mobad.feeds.NativeResponse;
import com.baidu.mobad.feeds.RequestParameters;
import com.qhad.ads.sdk.adcore.Qhad;
import com.qhad.ads.sdk.interfaces.IQhNativeAd;
import com.qhad.ads.sdk.interfaces.IQhNativeAdListener;
import com.qhad.ads.sdk.interfaces.IQhNativeAdLoader;
import com.qq.e.ads.nativ.NativeAD;
import com.qq.e.ads.nativ.NativeADDataRef;
import com.shoujiduoduo.a.a.c;
import com.shoujiduoduo.a.c.k;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.ringtone.activity.RingToneDuoduoActivity;
import com.shoujiduoduo.util.ac;
import com.shoujiduoduo.util.ah;
import com.shoujiduoduo.util.r;
import com.tencent.open.SocialConstants;
import com.umeng.a.b;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Timer;

/* compiled from: FeedAdData */
public class e {
    private static int d = 1800000;
    private static int e = 1800;
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public int f1294a;
    private int b;
    private int c;
    /* access modifiers changed from: private */
    public int f;
    /* access modifiers changed from: private */
    public LinkedList<a> g = new LinkedList<>();
    /* access modifiers changed from: private */
    public LinkedList<a> h = new LinkedList<>();
    /* access modifiers changed from: private */
    public LinkedList<a> i = new LinkedList<>();
    /* access modifiers changed from: private */
    public final byte[] j = new byte[0];
    /* access modifiers changed from: private */
    public Object k;
    /* access modifiers changed from: private */
    public Object l;
    /* access modifiers changed from: private */
    public Object m;
    /* access modifiers changed from: private */
    public Object n;
    private Timer o;
    private int p;
    /* access modifiers changed from: private */
    public volatile boolean q;
    /* access modifiers changed from: private */
    public boolean r;
    private int s;
    private int t;
    private int u;

    /* compiled from: FeedAdData */
    public static class a {

        /* renamed from: a  reason: collision with root package name */
        private Object f1298a;
        private long b;
        private long c;
        private int d;
        private int e;
        private int f;

        public a() {
        }

        public a(Object obj, long j, long j2, int i) {
            this.f1298a = obj;
            this.b = j;
            this.c = j2;
            this.d = i;
        }

        public int a() {
            return this.d;
        }

        public boolean b() {
            boolean z;
            switch (this.d) {
                case 1:
                    try {
                        return ((NativeResponse) this.f1298a).isAdAvailable(RingDDApp.c());
                    } catch (Exception e2) {
                        b.a(RingDDApp.c(), e2);
                        e2.printStackTrace();
                        return false;
                    }
                case 2:
                case 4:
                    if (System.currentTimeMillis() - this.b < this.c) {
                        z = true;
                    } else {
                        z = false;
                    }
                    return z;
                case 3:
                default:
                    return false;
            }
        }

        public void c() {
            this.e++;
        }

        public void a(int i) {
            this.f = i;
        }

        public boolean d() {
            return this.e >= this.f;
        }

        public String e() {
            switch (this.d) {
                case 1:
                    return ((NativeResponse) this.f1298a).getTitle();
                case 2:
                    return ((IQhNativeAd) this.f1298a).getContent().optString("title", "");
                case 3:
                default:
                    return "";
                case 4:
                    return ((NativeADDataRef) this.f1298a).getTitle();
            }
        }

        public String f() {
            switch (this.d) {
                case 1:
                    return ((NativeResponse) this.f1298a).getDesc();
                case 2:
                    return ((IQhNativeAd) this.f1298a).getContent().optString(SocialConstants.PARAM_APP_DESC, "");
                case 3:
                default:
                    return "";
                case 4:
                    return ((NativeADDataRef) this.f1298a).getDesc();
            }
        }

        public String g() {
            switch (this.d) {
                case 1:
                    return ((NativeResponse) this.f1298a).getIconUrl();
                case 2:
                    return ((IQhNativeAd) this.f1298a).getContent().optString("contentimg", "");
                case 3:
                default:
                    return "";
                case 4:
                    return ((NativeADDataRef) this.f1298a).getIconUrl();
            }
        }

        public void a(View view) {
            switch (this.d) {
                case 1:
                    b.b(RingDDApp.c(), "baidu_feed_ad_show");
                    ((NativeResponse) this.f1298a).recordImpression(view);
                    return;
                case 2:
                    b.b(RingDDApp.c(), "qihu_feed_ad_show");
                    ((IQhNativeAd) this.f1298a).onAdShowed();
                    return;
                case 3:
                default:
                    return;
                case 4:
                    b.b(RingDDApp.c(), "gdt_feed_ad_show");
                    ((NativeADDataRef) this.f1298a).onExposured(view);
                    return;
            }
        }

        public void b(View view) {
            switch (this.d) {
                case 1:
                    b.b(RingDDApp.c(), "baidu_feed_ad_click");
                    ((NativeResponse) this.f1298a).handleClick(view);
                    return;
                case 2:
                    b.b(RingDDApp.c(), "qihu_feed_ad_click");
                    ((IQhNativeAd) this.f1298a).onAdClicked();
                    return;
                case 3:
                default:
                    return;
                case 4:
                    b.b(RingDDApp.c(), "gdt_feed_ad_click");
                    ((NativeADDataRef) this.f1298a).onClicked(view);
                    return;
            }
        }
    }

    public void a() {
        int i2 = 1;
        String a2 = ac.a().a("feed_ad_v1_type");
        if (a2.equalsIgnoreCase("baidu")) {
            this.p = 1;
        } else if (a2.equals("gdt")) {
            this.p = 4;
        } else if (a2.equals("mix")) {
            this.p = 99;
        } else {
            com.shoujiduoduo.base.a.a.c("FeedAdData", "not support ad type, use default value baidu");
            this.p = 1;
        }
        this.b = ac.a().a("feed_ad_v1_percent_baidu", 1);
        this.c = ac.a().a("feed_ad_v1_percent_gdt", 1);
        if (this.b < this.c) {
            i2 = 4;
        }
        this.u = i2;
        com.shoujiduoduo.base.a.a.a("FeedAdData", "ad type is :" + a2 + ", reuseLimit is: " + ac.a().a("feed_ad_v1_show_limit", 3));
        com.shoujiduoduo.base.a.a.a("FeedAdData", "baidu percent:" + this.b);
        com.shoujiduoduo.base.a.a.a("FeedAdData", "gdt percent:" + this.c);
    }

    public void b() {
        if (!com.shoujiduoduo.util.a.f()) {
            com.shoujiduoduo.base.a.a.a("FeedAdData", "not support feed ad");
        } else if (this.p == 2) {
            g();
        } else if (this.p == 1) {
            e();
        } else if (this.p == 4) {
            f();
        } else if (this.p == 99) {
            e();
            f();
        }
    }

    private void e() {
        com.shoujiduoduo.base.a.a.a("FeedAdData", "[BAIDU] init Baidu Feed ad");
        String a2 = com.umeng.b.a.a().a(RingDDApp.c(), "baidu_feed_ad_valid_time");
        if (ah.c(a2)) {
            this.f = d;
        } else {
            this.f = r.a(a2, e) * 1000;
        }
        String a3 = com.umeng.b.a.a().a(RingDDApp.c(), "baidu_feed_ad_list_capacity");
        if (ah.c(a3)) {
            this.f1294a = 20;
        } else {
            this.f1294a = r.a(a3, 20);
        }
        this.m = new BaiduNative(RingToneDuoduoActivity.a(), "2417956", new BaiduNative.BaiduNativeNetworkListener() {
            public void onNativeLoad(List<NativeResponse> list) {
                com.shoujiduoduo.base.a.a.a("FeedAdData", "[BAIDU] baidu feed ad load success");
                if (list == null || list.size() <= 0) {
                    boolean unused = e.this.r = false;
                    com.shoujiduoduo.base.a.a.a("FeedAdData", "[BAIDU] onNativeAdLoadSucceeded, size is 0");
                    return;
                }
                com.shoujiduoduo.base.a.a.a("FeedAdData", "[BAIDU] onNativeAdLoadSucceeded, size:" + list.size());
                HashMap hashMap = new HashMap();
                hashMap.put("status", "onNativeLoad");
                b.a(RingDDApp.c(), "baidu_feed_ad", hashMap);
                long currentTimeMillis = System.currentTimeMillis();
                synchronized (e.this.j) {
                    for (NativeResponse aVar : list) {
                        a aVar2 = new a(aVar, currentTimeMillis, (long) e.this.f, 1);
                        aVar2.a(e.this.i());
                        e.this.g.add(aVar2);
                    }
                }
                if (!e.this.q) {
                    c.a().b(com.shoujiduoduo.a.a.b.OBSERVER_FEED_AD, new c.a<k>() {
                        public void a() {
                            ((k) this.f1284a).a();
                        }
                    });
                    boolean unused2 = e.this.q = true;
                }
                com.shoujiduoduo.base.a.a.a("FeedAdData", "[BAIDU] ad list size:" + e.this.g.size());
                if (e.this.g.size() < e.this.f1294a) {
                    com.shoujiduoduo.base.a.a.a("FeedAdData", "[BAIDU] ad list size is < " + e.this.f1294a + ", so fetch more data");
                    boolean unused3 = e.this.r = true;
                    ((BaiduNative) e.this.m).makeRequest((RequestParameters) e.this.n);
                    return;
                }
                boolean unused4 = e.this.r = false;
            }

            public void onNativeFail(NativeErrorCode nativeErrorCode) {
                if (nativeErrorCode != null) {
                    HashMap hashMap = new HashMap();
                    hashMap.put("status", "onNativeFail");
                    hashMap.put("errCode", nativeErrorCode.toString());
                    b.a(RingDDApp.c(), "baidu_feed_ad", hashMap);
                    com.shoujiduoduo.base.a.a.a("FeedAdData", "[BAIDU] baidu feed ad load failed, errcode:" + nativeErrorCode.toString());
                }
            }
        });
        boolean equals = "true".equals(com.umeng.b.a.a().a(RingDDApp.c(), "feed_ad_confirm_down"));
        com.shoujiduoduo.base.a.a.a("FeedAdData", "[BAIDU] baidu feed ad confirmdown:" + equals);
        this.n = new RequestParameters.Builder().confirmDownloading(equals).build();
        this.r = true;
        ((BaiduNative) this.m).makeRequest((RequestParameters) this.n);
    }

    private void f() {
        com.shoujiduoduo.base.a.a.a("FeedAdData", "[GDT] init Gdt Feed ad");
        String a2 = com.umeng.b.a.a().a(RingDDApp.c(), "gdt_feed_ad_valid_time");
        if (ah.c(a2)) {
            this.f = d;
        } else {
            this.f = r.a(a2, e) * 1000;
        }
        String a3 = com.umeng.b.a.a().a(RingDDApp.c(), "gdt_feed_ad_list_capacity");
        if (ah.c(a3)) {
            this.f1294a = 20;
        } else {
            this.f1294a = r.a(a3, 20);
        }
        this.k = new NativeAD(RingToneDuoduoActivity.a(), "1105772024", "4090414688268566", new NativeAD.NativeAdListener() {
            public void onADLoaded(List<NativeADDataRef> list) {
                com.shoujiduoduo.base.a.a.a("FeedAdData", "[GDT] gdt feed ad load success");
                if (list == null || list.size() <= 0) {
                    boolean unused = e.this.r = false;
                    com.shoujiduoduo.base.a.a.a("FeedAdData", "[GDT] onADLoaded, size is 0");
                    return;
                }
                com.shoujiduoduo.base.a.a.a("FeedAdData", "[GDT] onADLoaded, size:" + list.size());
                HashMap hashMap = new HashMap();
                hashMap.put("status", "onNativeLoad");
                b.a(RingDDApp.c(), "gdt_feed_ad", hashMap);
                long currentTimeMillis = System.currentTimeMillis();
                synchronized (e.this.j) {
                    for (NativeADDataRef aVar : list) {
                        a aVar2 = new a(aVar, currentTimeMillis, (long) e.this.f, 4);
                        aVar2.a(e.this.i());
                        e.this.h.add(aVar2);
                    }
                }
                if (!e.this.q) {
                    c.a().b(com.shoujiduoduo.a.a.b.OBSERVER_FEED_AD, new c.a<k>() {
                        public void a() {
                            ((k) this.f1284a).a();
                        }
                    });
                    boolean unused2 = e.this.q = true;
                }
                com.shoujiduoduo.base.a.a.a("FeedAdData", "[GDT] ad list size:" + e.this.h.size());
                if (e.this.h.size() < e.this.f1294a) {
                    com.shoujiduoduo.base.a.a.a("FeedAdData", "[GDT] ad list size is < " + e.this.f1294a + ", so fetch more data");
                    boolean unused3 = e.this.r = true;
                    ((NativeAD) e.this.k).loadAD(10);
                    return;
                }
                boolean unused4 = e.this.r = false;
            }

            public void onNoAD(int i) {
                com.shoujiduoduo.base.a.a.a("FeedAdData", "[GDT] onNoAd");
                HashMap hashMap = new HashMap();
                hashMap.put("status", "onNoAD");
                b.a(RingDDApp.c(), "gdt_feed_ad", hashMap);
            }

            public void onADStatusChanged(NativeADDataRef nativeADDataRef) {
                com.shoujiduoduo.base.a.a.a("FeedAdData", "[GDT] onADStatusChanged");
            }

            public void onADError(NativeADDataRef nativeADDataRef, int i) {
                com.shoujiduoduo.base.a.a.a("FeedAdData", "[GDT] onADError:" + i);
                HashMap hashMap = new HashMap();
                hashMap.put("status", "onADError");
                hashMap.put("errCode", "" + i);
                b.a(RingDDApp.c(), "gdt_feed_ad", hashMap);
            }
        });
        this.r = true;
        ((NativeAD) this.k).loadAD(10);
    }

    private void g() {
        com.shoujiduoduo.base.a.a.a("FeedAdData", "init 360 Feed ad");
        String a2 = com.umeng.b.a.a().a(RingDDApp.c(), "360_feed_ad_valid_time");
        if (ah.c(a2)) {
            this.f = d;
        } else {
            this.f = r.a(a2, e) * 1000;
        }
        String a3 = com.umeng.b.a.a().a(RingDDApp.c(), "360_feed_ad_list_capacity");
        if (ah.c(a3)) {
            this.f1294a = 20;
        } else {
            this.f1294a = r.a(a3, 20);
        }
        this.l = Qhad.initNativeAdLoader(RingToneDuoduoActivity.a(), "uaaGFTRq7O", new IQhNativeAdListener() {
            public void onNativeAdLoadSucceeded(ArrayList<IQhNativeAd> arrayList) {
                if (arrayList == null || arrayList.size() <= 0) {
                    boolean unused = e.this.r = false;
                    com.shoujiduoduo.base.a.a.a("FeedAdData", "onNativeAdLoadSucceeded, size is 0");
                    return;
                }
                com.shoujiduoduo.base.a.a.a("FeedAdData", "onNativeAdLoadSucceeded, size:" + arrayList.size());
                long currentTimeMillis = System.currentTimeMillis();
                synchronized (e.this.j) {
                    Iterator<IQhNativeAd> it = arrayList.iterator();
                    while (it.hasNext()) {
                        a aVar = new a(it.next(), currentTimeMillis, (long) e.this.f, 2);
                        aVar.a(e.this.i());
                        e.this.i.add(aVar);
                    }
                }
                if (!e.this.q) {
                    c.a().b(com.shoujiduoduo.a.a.b.OBSERVER_FEED_AD, new c.a<k>() {
                        public void a() {
                            ((k) this.f1284a).a();
                        }
                    });
                    boolean unused2 = e.this.q = true;
                }
                com.shoujiduoduo.base.a.a.a("FeedAdData", "ad list size:" + e.this.i.size());
                if (e.this.i.size() < e.this.f1294a) {
                    com.shoujiduoduo.base.a.a.a("FeedAdData", "ad list size is < " + e.this.f1294a + ", so fetch more data");
                    boolean unused3 = e.this.r = true;
                    ((IQhNativeAdLoader) e.this.l).loadAds();
                    return;
                }
                boolean unused4 = e.this.r = false;
            }

            public void onNativeAdLoadFailed() {
                com.shoujiduoduo.base.a.a.a("FeedAdData", "onNativeAdLoadFailed");
            }
        }, false);
        if (this.l != null) {
            this.r = true;
            ((IQhNativeAdLoader) this.l).loadAds();
        }
    }

    public void c() {
        if (this.o != null) {
            this.o.cancel();
        }
    }

    private void h() {
        switch (this.p) {
            case 1:
                if (this.m != null && !this.r) {
                    ((BaiduNative) this.m).makeRequest((RequestParameters) this.n);
                    return;
                }
                return;
            case 2:
                if (this.l != null && !this.r) {
                    ((IQhNativeAdLoader) this.l).loadAds();
                    return;
                }
                return;
            case 4:
                if (this.k != null && !this.r) {
                    ((NativeAD) this.k).loadAD(10);
                    return;
                }
                return;
            case 99:
                if (this.k != null && !this.r) {
                    ((NativeAD) this.k).loadAD(10);
                }
                if (this.m != null && !this.r) {
                    ((BaiduNative) this.m).makeRequest((RequestParameters) this.n);
                    return;
                }
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: private */
    public int i() {
        return ac.a().a("feed_ad_v1_show_limit", 3);
    }

    public a d() {
        LinkedList<a> linkedList;
        com.shoujiduoduo.base.a.a.a("FeedAdData", "getNextAdItem");
        if (this.p != 99) {
            if (this.p == 1) {
                linkedList = this.g;
            } else if (this.p == 4) {
                linkedList = this.h;
            } else {
                com.shoujiduoduo.base.a.a.c("FeedAdData", "not supported ad type");
                return null;
            }
            return a(linkedList);
        } else if (this.u == 1) {
            com.shoujiduoduo.base.a.a.a("FeedAdData", "current is baidu");
            this.s++;
            if (this.b > 0 && this.c > 0 && this.s % this.b == 0) {
                com.shoujiduoduo.base.a.a.a("FeedAdData", "switch to gdt");
                this.u = 4;
            }
            return a(this.g);
        } else {
            com.shoujiduoduo.base.a.a.a("FeedAdData", "current is gdt");
            this.t++;
            if (this.c > 0 && this.b > 0 && this.t % this.c == 0) {
                com.shoujiduoduo.base.a.a.a("FeedAdData", "switch to baidu");
                this.u = 1;
            }
            return a(this.h);
        }
    }

    private a a(LinkedList<a> linkedList) {
        a aVar;
        synchronized (this.j) {
            while (true) {
                if (linkedList.size() <= 0) {
                    aVar = null;
                    break;
                }
                aVar = linkedList.poll();
                if (aVar.b()) {
                    break;
                }
            }
        }
        com.shoujiduoduo.base.a.a.a("FeedAdData", "ad list size is :" + linkedList.size());
        if (linkedList.size() < this.f1294a / 2) {
            com.shoujiduoduo.base.a.a.a("FeedAdData", "current list size < " + (this.f1294a / 2) + ", so fetch more data");
            h();
        }
        if (aVar == null) {
            com.shoujiduoduo.base.a.a.e("FeedAdData", "no valid ad, return null");
        }
        return aVar;
    }
}
