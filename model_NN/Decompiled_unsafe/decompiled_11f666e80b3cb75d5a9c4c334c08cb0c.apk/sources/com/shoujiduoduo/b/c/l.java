package com.shoujiduoduo.b.c;

import com.shoujiduoduo.base.bean.DDList;
import com.shoujiduoduo.base.bean.ListType;
import com.shoujiduoduo.base.bean.RingData;
import java.util.ArrayList;

/* compiled from: SimpleList */
public class l implements DDList {

    /* renamed from: a  reason: collision with root package name */
    private ArrayList<RingData> f1347a = new ArrayList<>();
    private String b;

    public l(String str) {
        this.b = str;
    }

    public void a(RingData ringData) {
        this.f1347a.add(ringData);
    }

    public Object get(int i) {
        if (i < 0 || i >= this.f1347a.size()) {
            return null;
        }
        return this.f1347a.get(i);
    }

    public String getBaseURL() {
        return "";
    }

    public String getListId() {
        return this.b;
    }

    public ListType.LIST_TYPE getListType() {
        return ListType.LIST_TYPE.list_simple;
    }

    public int size() {
        if (this.f1347a != null) {
            return this.f1347a.size();
        }
        return 0;
    }

    public boolean isRetrieving() {
        return false;
    }

    public void retrieveData() {
    }

    public void refreshData() {
    }

    public void reloadData() {
    }

    public boolean hasMoreData() {
        return false;
    }
}
