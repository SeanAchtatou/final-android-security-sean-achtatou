package com.shoujiduoduo.b.c;

import android.os.Handler;
import android.os.Message;
import com.shoujiduoduo.a.a.b;
import com.shoujiduoduo.a.a.c;
import com.shoujiduoduo.a.c.g;
import com.shoujiduoduo.base.bean.DDList;
import com.shoujiduoduo.base.bean.ListContent;
import com.shoujiduoduo.base.bean.ListType;
import com.shoujiduoduo.base.bean.UserData;
import com.shoujiduoduo.util.ah;
import com.shoujiduoduo.util.i;
import com.shoujiduoduo.util.j;
import com.shoujiduoduo.util.t;
import java.io.ByteArrayInputStream;
import java.util.ArrayList;

/* compiled from: UserList */
public class n implements DDList {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public boolean f1351a = true;
    /* access modifiers changed from: private */
    public boolean b = false;
    /* access modifiers changed from: private */
    public boolean c = false;
    /* access modifiers changed from: private */
    public int d = -1;
    /* access modifiers changed from: private */
    public boolean e = false;
    /* access modifiers changed from: private */
    public ArrayList<UserData> f = new ArrayList<>();
    private a g;
    private String h;
    private Handler i = new Handler() {
        public void handleMessage(Message message) {
            switch (message.what) {
                case 0:
                    ListContent listContent = (ListContent) message.obj;
                    if (listContent != null) {
                        com.shoujiduoduo.base.a.a.a("RingList", "new obtained list data size = " + listContent.data.size());
                        if (n.this.f == null) {
                            ArrayList unused = n.this.f = listContent.data;
                        } else {
                            n.this.f.addAll(listContent.data);
                        }
                        boolean unused2 = n.this.f1351a = listContent.hasMore;
                        listContent.data = n.this.f;
                        if (n.this.d >= 0) {
                            listContent.page = n.this.d;
                        }
                        if (n.this.e && n.this.f.size() > 0) {
                            boolean unused3 = n.this.e = false;
                        }
                    }
                    boolean unused4 = n.this.b = false;
                    boolean unused5 = n.this.c = false;
                    break;
                case 1:
                case 2:
                    boolean unused6 = n.this.b = false;
                    boolean unused7 = n.this.c = true;
                    break;
            }
            final int i = message.what;
            c.a().a(b.OBSERVER_LIST_DATA, new c.a<g>() {
                public void a() {
                    ((g) this.f1284a).a(n.this, i);
                }
            });
        }
    };

    /* compiled from: UserList */
    public enum a {
        fans,
        follow
    }

    public n(a aVar, String str) {
        this.g = aVar;
        this.h = str;
    }

    public Object get(int i2) {
        if (i2 >= this.f.size() || i2 < 0) {
            return null;
        }
        return this.f.get(i2);
    }

    public a a() {
        return this.g;
    }

    public String getBaseURL() {
        return null;
    }

    public String getListId() {
        return "userlist";
    }

    public ListType.LIST_TYPE getListType() {
        return ListType.LIST_TYPE.list_user;
    }

    public int size() {
        return this.f.size();
    }

    public boolean isRetrieving() {
        return this.b;
    }

    public void retrieveData() {
        if (this.f == null || this.f.size() == 0) {
            this.b = true;
            this.c = false;
            i.a(new Runnable() {
                public void run() {
                    n.this.b();
                }
            });
        } else if (this.f1351a) {
            this.b = true;
            this.c = false;
            i.a(new Runnable() {
                public void run() {
                    n.this.c();
                }
            });
        }
    }

    public void refreshData() {
    }

    /* access modifiers changed from: private */
    public void b() {
        com.shoujiduoduo.base.a.a.a("UserList", "getListData");
        String a2 = a(0);
        if (ah.c(a2)) {
            com.shoujiduoduo.base.a.a.a("UserList", "RingList: httpGetRingList Failed!");
            this.i.sendEmptyMessage(1);
            return;
        }
        ListContent<UserData> b2 = j.b(new ByteArrayInputStream(a2.getBytes()));
        if (b2 != null) {
            com.shoujiduoduo.base.a.a.a("UserList", "list data size = " + b2.data.size());
            com.shoujiduoduo.base.a.a.a("UserList", "UserList: Read from network Success! send MESSAGE_SUCCESS_RETRIEVE_DATA");
            this.e = true;
            this.d = 0;
            this.i.sendMessage(this.i.obtainMessage(0, b2));
            return;
        }
        com.shoujiduoduo.base.a.a.a("UserList", "RingList: but parse FAILED! send MESSAGE_FAIL_RETRIEVE_DATA");
        this.i.sendEmptyMessage(1);
    }

    /* access modifiers changed from: private */
    public void c() {
        int i2;
        ListContent<UserData> listContent;
        com.shoujiduoduo.base.a.a.a("UserList", "retrieving more data, list size = " + this.f.size());
        if (this.d < 0) {
            i2 = this.f.size() / 25;
            com.shoujiduoduo.base.a.a.a("UserList", "没有cache current page 记录，通过list size 计算页数， 下一页，page：" + i2);
        } else {
            i2 = this.d + 1;
            com.shoujiduoduo.base.a.a.a("UserList", "有 cache current page 记录，下一页，page：" + i2);
        }
        String a2 = a(i2);
        if (ah.c(a2)) {
            this.i.sendEmptyMessage(2);
            return;
        }
        try {
            listContent = j.b(new ByteArrayInputStream(a2.getBytes()));
        } catch (ArrayIndexOutOfBoundsException e2) {
            listContent = null;
        }
        if (listContent != null) {
            com.shoujiduoduo.base.a.a.a("RingList", "list data size = " + listContent.data.size());
            this.e = true;
            this.d = i2;
            this.i.sendMessage(this.i.obtainMessage(0, listContent));
            return;
        }
        this.i.sendEmptyMessage(2);
    }

    private String a(int i2) {
        return t.a(this.g == a.fans ? "myfollower" : "myfollowing", "&page=" + i2 + "&pagesize=" + 25 + "&uid=" + com.shoujiduoduo.a.b.b.g().c().getUid() + "&tuid=" + this.h + "&pagesupport=1");
    }

    public void reloadData() {
    }

    public boolean hasMoreData() {
        return this.f1351a;
    }
}
