package com.shoujiduoduo.b.c;

import android.os.Handler;
import android.os.Message;
import com.shoujiduoduo.a.a.b;
import com.shoujiduoduo.a.a.c;
import com.shoujiduoduo.a.c.g;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.base.bean.CollectData;
import com.shoujiduoduo.base.bean.DDList;
import com.shoujiduoduo.base.bean.ListContent;
import com.shoujiduoduo.base.bean.ListType;
import com.shoujiduoduo.util.i;
import com.shoujiduoduo.util.j;
import com.shoujiduoduo.util.t;
import java.io.ByteArrayInputStream;
import java.util.ArrayList;

/* compiled from: CollectList */
public class c implements DDList {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public d f1310a = null;
    private String b;
    /* access modifiers changed from: private */
    public boolean c = true;
    /* access modifiers changed from: private */
    public boolean d = false;
    /* access modifiers changed from: private */
    public boolean e = false;
    /* access modifiers changed from: private */
    public int f = -1;
    /* access modifiers changed from: private */
    public boolean g = false;
    /* access modifiers changed from: private */
    public ArrayList<CollectData> h = new ArrayList<>();
    private Handler i = new Handler() {
        public void handleMessage(Message message) {
            switch (message.what) {
                case 0:
                    ListContent listContent = (ListContent) message.obj;
                    if (listContent != null) {
                        a.a("RingList", "new obtained list data size = " + listContent.data.size());
                        if (c.this.h == null) {
                            ArrayList unused = c.this.h = listContent.data;
                        } else {
                            c.this.h.addAll(listContent.data);
                        }
                        boolean unused2 = c.this.c = listContent.hasMore;
                        listContent.data = c.this.h;
                        if (c.this.f >= 0) {
                            listContent.page = c.this.f;
                        }
                        if (c.this.g && c.this.h.size() > 0) {
                            c.this.f1310a.a(listContent);
                            boolean unused3 = c.this.g = false;
                        }
                    }
                    boolean unused4 = c.this.d = false;
                    boolean unused5 = c.this.e = false;
                    break;
                case 1:
                case 2:
                    boolean unused6 = c.this.d = false;
                    boolean unused7 = c.this.e = true;
                    break;
            }
            final int i = message.what;
            com.shoujiduoduo.a.a.c.a().a(b.OBSERVER_LIST_DATA, new c.a<g>() {
                public void a() {
                    ((g) this.f1284a).a(c.this, i);
                }
            });
        }
    };

    public c(String str) {
        this.f1310a = new d("collect_" + str + ".tmp");
        this.b = str;
    }

    public String getBaseURL() {
        return "";
    }

    public String getListId() {
        return "collect";
    }

    public void retrieveData() {
        if (this.h == null || this.h.size() == 0) {
            this.d = true;
            this.e = false;
            i.a(new Runnable() {
                public void run() {
                    c.this.a();
                }
            });
        } else if (this.c) {
            this.d = true;
            this.e = false;
            i.a(new Runnable() {
                public void run() {
                    c.this.b();
                }
            });
        }
    }

    public void refreshData() {
    }

    /* access modifiers changed from: private */
    public void a() {
        if (!this.f1310a.a(14400000)) {
            a.a("CollectList", "CollectList: cache is available! Use Cache!");
            ListContent<CollectData> a2 = this.f1310a.a();
            if (!(a2 == null || a2.data == null || a2.data.size() <= 0)) {
                a.a("CollectList", "RingList: Read RingList Cache Success!");
                this.f = a2.page;
                this.i.sendMessage(this.i.obtainMessage(0, a2));
                return;
            }
        }
        a.a("CollectList", "RingList: cache is out of date or read cache failed!");
        String b2 = b(0);
        if (b2 == null) {
            a.a("CollectList", "RingList: httpGetRingList Failed!");
            this.i.sendEmptyMessage(1);
            return;
        }
        ListContent<CollectData> e2 = j.e(new ByteArrayInputStream(b2.getBytes()));
        if (e2 != null) {
            a.a("CollectList", "list data size = " + e2.data.size());
            a.a("CollectList", "RingList: Read from network Success! send MESSAGE_SUCCESS_RETRIEVE_DATA");
            this.g = true;
            this.f = 0;
            this.i.sendMessage(this.i.obtainMessage(0, e2));
            return;
        }
        a.a("CollectList", "RingList: parse FAILED! send MESSAGE_FAIL_RETRIEVE_DATA");
        this.i.sendEmptyMessage(1);
    }

    /* access modifiers changed from: private */
    public void b() {
        int i2;
        ListContent<CollectData> listContent;
        a.a("CollectList", "retrieving more data, list size = " + this.h.size());
        if (this.f < 0) {
            i2 = this.h.size() / 25;
            a.a("CollectList", "没有cache current page 记录，通过list size 计算页数， 下一页，page：" + i2);
        } else {
            i2 = this.f + 1;
            a.a("CollectList", "有 cache current page 记录，下一页，page：" + i2);
        }
        String b2 = b(i2);
        if (b2 == null) {
            this.i.sendEmptyMessage(2);
            return;
        }
        try {
            listContent = j.e(new ByteArrayInputStream(b2.getBytes()));
        } catch (ArrayIndexOutOfBoundsException e2) {
            listContent = null;
        }
        if (listContent != null) {
            a.a("RingList", "list data size = " + listContent.data.size());
            this.g = true;
            this.f = i2;
            this.i.sendMessage(this.i.obtainMessage(0, listContent));
            return;
        }
        this.i.sendEmptyMessage(2);
    }

    private String b(int i2) {
        return t.a("getcollects", "&page=" + i2 + "&pagesize=" + 25 + "&listid=" + this.b);
    }

    public boolean hasMoreData() {
        return this.c;
    }

    public boolean isRetrieving() {
        return this.d;
    }

    /* renamed from: a */
    public CollectData get(int i2) {
        if (i2 < 0 || i2 >= this.h.size()) {
            return null;
        }
        return this.h.get(i2);
    }

    public int size() {
        return this.h.size();
    }

    public ListType.LIST_TYPE getListType() {
        return ListType.LIST_TYPE.list_collect;
    }

    public void reloadData() {
    }
}
