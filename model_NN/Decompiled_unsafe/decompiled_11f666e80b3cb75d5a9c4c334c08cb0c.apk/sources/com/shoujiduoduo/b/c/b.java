package com.shoujiduoduo.b.c;

import cn.banshenggua.aichang.utils.StringUtil;
import com.meizu.cloud.pushsdk.constants.PushConstants;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.base.bean.ArtistData;
import com.shoujiduoduo.base.bean.ListContent;
import com.shoujiduoduo.util.j;
import com.shoujiduoduo.util.p;
import com.sina.weibo.sdk.constant.WBPageConstants;
import com.sina.weibo.sdk.register.mobile.SelectCountryActivity;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/* compiled from: ArtistListCache */
public class b extends p<ListContent<ArtistData>> {
    public b() {
    }

    public b(String str) {
        super(str);
    }

    public void a(ListContent<ArtistData> listContent) {
        ArrayList<T> arrayList = listContent.data;
        try {
            Document newDocument = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
            Element createElement = newDocument.createElement("list");
            createElement.setAttribute("num", String.valueOf(arrayList.size()));
            createElement.setAttribute("hasmore", String.valueOf(listContent.hasMore));
            createElement.setAttribute("area", String.valueOf(listContent.area));
            createElement.setAttribute("baseurl", listContent.baseURL);
            createElement.setAttribute(WBPageConstants.ParamKey.PAGE, "" + listContent.page);
            newDocument.appendChild(createElement);
            for (int i = 0; i < arrayList.size(); i++) {
                ArtistData artistData = (ArtistData) arrayList.get(i);
                Element createElement2 = newDocument.createElement("artist");
                createElement2.setAttribute("id", artistData.id);
                createElement2.setAttribute(SelectCountryActivity.EXTRA_COUNTRY_NAME, artistData.name);
                createElement2.setAttribute("work", artistData.work);
                createElement2.setAttribute("isnew", artistData.isNew ? "1" : "0");
                createElement2.setAttribute("pic", artistData.pic);
                createElement2.setAttribute("sale", "" + artistData.sale);
                createElement.appendChild(createElement2);
            }
            Transformer newTransformer = TransformerFactory.newInstance().newTransformer();
            newTransformer.setOutputProperty("encoding", StringUtil.Encoding);
            newTransformer.setOutputProperty("indent", "yes");
            newTransformer.setOutputProperty("standalone", "yes");
            newTransformer.setOutputProperty(PushConstants.MZ_PUSH_MESSAGE_METHOD, "xml");
            newTransformer.transform(new DOMSource(newDocument.getDocumentElement()), new StreamResult(new FileOutputStream(new File(c + this.b))));
        } catch (DOMException e) {
            e.printStackTrace();
            a.a(e);
        } catch (ParserConfigurationException e2) {
            e2.printStackTrace();
            a.a(e2);
        } catch (TransformerConfigurationException e3) {
            e3.printStackTrace();
            a.a(e3);
        } catch (FileNotFoundException e4) {
            e4.printStackTrace();
            a.a(e4);
        } catch (TransformerException e5) {
            e5.printStackTrace();
            a.a(e5);
        } catch (Exception e6) {
            e6.printStackTrace();
            a.a(e6);
        }
    }

    public ListContent<ArtistData> a() {
        try {
            return j.d(new FileInputStream(c + this.b));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        } catch (ArrayIndexOutOfBoundsException e2) {
            e2.printStackTrace();
            return null;
        }
    }
}
