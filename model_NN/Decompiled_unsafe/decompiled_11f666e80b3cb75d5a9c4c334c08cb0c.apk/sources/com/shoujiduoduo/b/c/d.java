package com.shoujiduoduo.b.c;

import cn.banshenggua.aichang.utils.StringUtil;
import com.meizu.cloud.pushsdk.constants.PushConstants;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.base.bean.CollectData;
import com.shoujiduoduo.base.bean.ListContent;
import com.shoujiduoduo.util.j;
import com.shoujiduoduo.util.p;
import com.sina.weibo.sdk.constant.WBPageConstants;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/* compiled from: CollectListCache */
public class d extends p<ListContent<CollectData>> {
    public d() {
    }

    public d(String str) {
        super(str);
    }

    public void a(ListContent<CollectData> listContent) {
        ArrayList<T> arrayList = listContent.data;
        try {
            Document newDocument = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
            Element createElement = newDocument.createElement("list");
            createElement.setAttribute("num", String.valueOf(arrayList.size()));
            createElement.setAttribute("hasmore", String.valueOf(listContent.hasMore));
            createElement.setAttribute("area", String.valueOf(listContent.area));
            createElement.setAttribute("baseurl", listContent.baseURL);
            createElement.setAttribute(WBPageConstants.ParamKey.PAGE, "" + listContent.page);
            newDocument.appendChild(createElement);
            for (int i = 0; i < arrayList.size(); i++) {
                CollectData collectData = (CollectData) arrayList.get(i);
                Element createElement2 = newDocument.createElement("collect");
                createElement2.setAttribute("id", collectData.id);
                createElement2.setAttribute("title", collectData.title);
                createElement2.setAttribute("content", collectData.content);
                createElement2.setAttribute("time", collectData.time);
                createElement2.setAttribute("isnew", collectData.isNew ? "1" : "0");
                createElement2.setAttribute("fav", collectData.favNum);
                createElement2.setAttribute("pic", collectData.pic);
                createElement2.setAttribute("artist", collectData.artist);
                createElement.appendChild(createElement2);
            }
            Transformer newTransformer = TransformerFactory.newInstance().newTransformer();
            newTransformer.setOutputProperty("encoding", StringUtil.Encoding);
            newTransformer.setOutputProperty("indent", "yes");
            newTransformer.setOutputProperty("standalone", "yes");
            newTransformer.setOutputProperty(PushConstants.MZ_PUSH_MESSAGE_METHOD, "xml");
            newTransformer.transform(new DOMSource(newDocument.getDocumentElement()), new StreamResult(new FileOutputStream(new File(c + this.b))));
        } catch (DOMException e) {
            e.printStackTrace();
            a.a(e);
        } catch (ParserConfigurationException e2) {
            e2.printStackTrace();
            a.a(e2);
        } catch (TransformerConfigurationException e3) {
            e3.printStackTrace();
            a.a(e3);
        } catch (FileNotFoundException e4) {
            e4.printStackTrace();
            a.a(e4);
        } catch (TransformerException e5) {
            e5.printStackTrace();
            a.a(e5);
        } catch (Exception e6) {
            e6.printStackTrace();
            a.a(e6);
        }
    }

    public ListContent<CollectData> a() {
        try {
            return j.e(new FileInputStream(c + this.b));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        } catch (ArrayIndexOutOfBoundsException e2) {
            e2.printStackTrace();
            return null;
        }
    }
}
