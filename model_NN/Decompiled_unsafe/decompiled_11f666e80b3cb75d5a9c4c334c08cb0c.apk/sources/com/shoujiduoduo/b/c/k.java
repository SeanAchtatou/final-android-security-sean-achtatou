package com.shoujiduoduo.b.c;

import cn.banshenggua.aichang.utils.StringUtil;
import com.baidu.mobads.interfaces.IXAdRequestInfo;
import com.meizu.cloud.pushsdk.constants.PushConstants;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.base.bean.ListContent;
import com.shoujiduoduo.base.bean.RingData;
import com.shoujiduoduo.util.ah;
import com.shoujiduoduo.util.j;
import com.shoujiduoduo.util.p;
import com.shoujiduoduo.util.q;
import com.sina.weibo.sdk.constant.WBConstants;
import com.sina.weibo.sdk.constant.WBPageConstants;
import com.sina.weibo.sdk.register.mobile.SelectCountryActivity;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/* compiled from: RingListCache */
public class k extends p<ListContent<RingData>> {
    k(String str) {
        super(str);
    }

    public boolean a() {
        return q.g(c + this.b);
    }

    public ListContent<RingData> b() {
        try {
            return j.f(new FileInputStream(c + this.b));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        } catch (ArrayIndexOutOfBoundsException e2) {
            e2.printStackTrace();
            return null;
        }
    }

    public void a(ListContent<RingData> listContent) {
        ArrayList<T> arrayList = listContent.data;
        try {
            Document newDocument = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
            Element createElement = newDocument.createElement("list");
            createElement.setAttribute("num", String.valueOf(arrayList.size()));
            createElement.setAttribute("hasmore", String.valueOf(listContent.hasMore));
            createElement.setAttribute("area", String.valueOf(listContent.area));
            createElement.setAttribute("baseurl", listContent.baseURL);
            createElement.setAttribute(WBPageConstants.ParamKey.PAGE, "" + listContent.page);
            newDocument.appendChild(createElement);
            for (int i = 0; i < arrayList.size(); i++) {
                RingData ringData = (RingData) arrayList.get(i);
                Element createElement2 = newDocument.createElement("ring");
                createElement2.setAttribute(SelectCountryActivity.EXTRA_COUNTRY_NAME, ringData.name);
                createElement2.setAttribute("artist", ringData.artist);
                a(createElement2, "uid", ringData.uid);
                a(createElement2, "duration", ringData.duration);
                a(createElement2, WBConstants.GAME_PARAMS_SCORE, ringData.score);
                a(createElement2, "playcnt", ringData.playcnt);
                createElement2.setAttribute("rid", ringData.rid);
                a(createElement2, "bdurl", ringData.baiduURL);
                a(createElement2, "hbr", ringData.getHighAACBitrate());
                a(createElement2, "hurl", ringData.getHighAACURL());
                a(createElement2, "lbr", ringData.getLowAACBitrate());
                a(createElement2, "lurl", ringData.getLowAACURL());
                a(createElement2, "mp3br", ringData.getMp3Bitrate());
                a(createElement2, "mp3url", ringData.getMp3URL());
                a(createElement2, "isnew", ringData.isNew);
                a(createElement2, IXAdRequestInfo.CELL_ID, ringData.cid);
                a(createElement2, "valid", ringData.valid);
                a(createElement2, "hasmedia", ringData.hasmedia);
                a(createElement2, "singerId", ringData.singerId);
                a(createElement2, "price", ringData.price);
                a(createElement2, "ctcid", ringData.ctcid);
                a(createElement2, "ctvalid", ringData.ctvalid);
                a(createElement2, "cthasmedia", ringData.cthasmedia);
                a(createElement2, "ctprice", ringData.ctprice);
                a(createElement2, "ctvip", ringData.ctVip);
                a(createElement2, "wavurl", ringData.ctWavUrl);
                a(createElement2, "cuvip", ringData.cuvip);
                a(createElement2, "cuftp", ringData.cuftp);
                a(createElement2, "cucid", ringData.cucid);
                a(createElement2, "cusid", ringData.cusid);
                a(createElement2, "cuurl", ringData.cuurl);
                a(createElement2, "cuvalid", ringData.cuvalid);
                a(createElement2, "hasshow", ringData.hasshow);
                a(createElement2, "ishot", ringData.isHot);
                a(createElement2, "head_url", ringData.userHead);
                a(createElement2, "comment_num", ringData.commentNum);
                a(createElement2, "date", ringData.date);
                createElement.appendChild(createElement2);
            }
            Transformer newTransformer = TransformerFactory.newInstance().newTransformer();
            newTransformer.setOutputProperty("encoding", StringUtil.Encoding);
            newTransformer.setOutputProperty("indent", "yes");
            newTransformer.setOutputProperty("standalone", "yes");
            newTransformer.setOutputProperty(PushConstants.MZ_PUSH_MESSAGE_METHOD, "xml");
            newTransformer.transform(new DOMSource(newDocument.getDocumentElement()), new StreamResult(new FileOutputStream(new File(c + this.b))));
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
            a.a(e);
        } catch (TransformerException e2) {
            e2.printStackTrace();
            a.a(e2);
        } catch (FileNotFoundException e3) {
            e3.printStackTrace();
            a.a(e3);
        } catch (Exception e4) {
            e4.printStackTrace();
            a.a(e4);
        }
    }

    private void a(Element element, String str, String str2) {
        if (!ah.c(str2)) {
            element.setAttribute(str, str2);
        }
    }

    private void a(Element element, String str, int i) {
        if (i != 0) {
            element.setAttribute(str, "" + i);
        }
    }
}
