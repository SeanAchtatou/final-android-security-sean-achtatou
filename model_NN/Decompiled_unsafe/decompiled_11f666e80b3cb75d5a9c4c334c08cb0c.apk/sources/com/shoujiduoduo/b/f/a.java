package com.shoujiduoduo.b.f;

import com.shoujiduoduo.a.a.b;
import com.shoujiduoduo.a.a.c;
import com.shoujiduoduo.a.c.g;
import com.shoujiduoduo.a.c.w;
import com.shoujiduoduo.base.bean.CollectData;
import com.shoujiduoduo.base.bean.DDList;
import com.shoujiduoduo.base.bean.ListContent;
import com.shoujiduoduo.base.bean.ListType;
import com.shoujiduoduo.util.i;
import com.shoujiduoduo.util.j;
import com.shoujiduoduo.util.l;
import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

/* compiled from: CollectRingList */
public class a implements DDList {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public String f1372a = l.b(4);
    /* access modifiers changed from: private */
    public ArrayList<CollectData> b = new ArrayList<>();
    /* access modifiers changed from: private */
    public boolean c;
    private final byte[] d = new byte[0];

    public boolean a() {
        return this.c;
    }

    public String getBaseURL() {
        return "";
    }

    public String getListId() {
        return "user_collect";
    }

    public void b() {
        i.a(new Runnable() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.shoujiduoduo.b.f.a.a(com.shoujiduoduo.b.f.a, boolean):boolean
             arg types: [com.shoujiduoduo.b.f.a, int]
             candidates:
              com.shoujiduoduo.b.f.a.a(com.shoujiduoduo.b.f.a, java.util.ArrayList):java.util.ArrayList
              com.shoujiduoduo.b.f.a.a(com.shoujiduoduo.b.f.a, boolean):boolean */
            public void run() {
                if (new File(a.this.f1372a).exists()) {
                    ListContent<CollectData> a2 = j.a(a.this.f1372a);
                    if (a2 != null) {
                        ArrayList unused = a.this.b = a2.data;
                        com.shoujiduoduo.base.a.a.a("CollectRingList", "user collect size:" + a.this.b.size());
                    } else {
                        com.shoujiduoduo.base.a.a.c("CollectRingList", "read user collect file, return null");
                    }
                } else {
                    com.shoujiduoduo.base.a.a.a("CollectRingList", "Collect_ring.xml 不存在");
                }
                boolean unused2 = a.this.c = true;
                c.a().a(b.OBSERVER_USER_RING, new c.a<w>() {
                    public void a() {
                        ((w) this.f1284a).a(0, null, "user_collect");
                    }
                });
            }
        });
    }

    public boolean a(CollectData collectData) {
        synchronized (this.d) {
            Iterator<CollectData> it = this.b.iterator();
            while (true) {
                if (it.hasNext()) {
                    if (it.next().id.equals(collectData.id)) {
                        break;
                    }
                } else {
                    this.b.add(collectData);
                    c();
                    d();
                    break;
                }
            }
        }
        return true;
    }

    public boolean a(Collection<Integer> collection) {
        if (collection == null || collection.size() == 0) {
            return false;
        }
        synchronized (this.d) {
            ArrayList arrayList = new ArrayList();
            for (Integer intValue : collection) {
                arrayList.add(this.b.get(intValue.intValue()));
            }
            Iterator it = arrayList.iterator();
            while (it.hasNext()) {
                this.b.remove((CollectData) it.next());
            }
            c();
            d();
        }
        return true;
    }

    private void c() {
        c.a().a(b.OBSERVER_LIST_DATA, new c.a<g>() {
            public void a() {
                ((g) this.f1284a).a(a.this, 0);
            }
        });
    }

    private void d() {
        i.a(new Runnable() {
            public void run() {
                boolean unused = a.this.e();
            }
        });
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean e() {
        /*
            r9 = this;
            r1 = 0
            byte[] r4 = r9.d
            monitor-enter(r4)
            javax.xml.parsers.DocumentBuilderFactory r0 = javax.xml.parsers.DocumentBuilderFactory.newInstance()     // Catch:{ ParserConfigurationException -> 0x00d2, TransformerConfigurationException -> 0x00d9, FileNotFoundException -> 0x00e1, TransformerException -> 0x00e6, Exception -> 0x00eb }
            javax.xml.parsers.DocumentBuilder r0 = r0.newDocumentBuilder()     // Catch:{ ParserConfigurationException -> 0x00d2, TransformerConfigurationException -> 0x00d9, FileNotFoundException -> 0x00e1, TransformerException -> 0x00e6, Exception -> 0x00eb }
            org.w3c.dom.Document r5 = r0.newDocument()     // Catch:{ ParserConfigurationException -> 0x00d2, TransformerConfigurationException -> 0x00d9, FileNotFoundException -> 0x00e1, TransformerException -> 0x00e6, Exception -> 0x00eb }
            java.lang.String r0 = "list"
            org.w3c.dom.Element r6 = r5.createElement(r0)     // Catch:{ ParserConfigurationException -> 0x00d2, TransformerConfigurationException -> 0x00d9, FileNotFoundException -> 0x00e1, TransformerException -> 0x00e6, Exception -> 0x00eb }
            java.lang.String r0 = "num"
            java.util.ArrayList<com.shoujiduoduo.base.bean.CollectData> r2 = r9.b     // Catch:{ ParserConfigurationException -> 0x00d2, TransformerConfigurationException -> 0x00d9, FileNotFoundException -> 0x00e1, TransformerException -> 0x00e6, Exception -> 0x00eb }
            int r2 = r2.size()     // Catch:{ ParserConfigurationException -> 0x00d2, TransformerConfigurationException -> 0x00d9, FileNotFoundException -> 0x00e1, TransformerException -> 0x00e6, Exception -> 0x00eb }
            java.lang.String r2 = java.lang.String.valueOf(r2)     // Catch:{ ParserConfigurationException -> 0x00d2, TransformerConfigurationException -> 0x00d9, FileNotFoundException -> 0x00e1, TransformerException -> 0x00e6, Exception -> 0x00eb }
            r6.setAttribute(r0, r2)     // Catch:{ ParserConfigurationException -> 0x00d2, TransformerConfigurationException -> 0x00d9, FileNotFoundException -> 0x00e1, TransformerException -> 0x00e6, Exception -> 0x00eb }
            r5.appendChild(r6)     // Catch:{ ParserConfigurationException -> 0x00d2, TransformerConfigurationException -> 0x00d9, FileNotFoundException -> 0x00e1, TransformerException -> 0x00e6, Exception -> 0x00eb }
            r3 = r1
        L_0x0029:
            java.util.ArrayList<com.shoujiduoduo.base.bean.CollectData> r0 = r9.b     // Catch:{ ParserConfigurationException -> 0x00d2, TransformerConfigurationException -> 0x00d9, FileNotFoundException -> 0x00e1, TransformerException -> 0x00e6, Exception -> 0x00eb }
            int r0 = r0.size()     // Catch:{ ParserConfigurationException -> 0x00d2, TransformerConfigurationException -> 0x00d9, FileNotFoundException -> 0x00e1, TransformerException -> 0x00e6, Exception -> 0x00eb }
            if (r3 >= r0) goto L_0x0085
            java.util.ArrayList<com.shoujiduoduo.base.bean.CollectData> r0 = r9.b     // Catch:{ ParserConfigurationException -> 0x00d2, TransformerConfigurationException -> 0x00d9, FileNotFoundException -> 0x00e1, TransformerException -> 0x00e6, Exception -> 0x00eb }
            java.lang.Object r0 = r0.get(r3)     // Catch:{ ParserConfigurationException -> 0x00d2, TransformerConfigurationException -> 0x00d9, FileNotFoundException -> 0x00e1, TransformerException -> 0x00e6, Exception -> 0x00eb }
            com.shoujiduoduo.base.bean.CollectData r0 = (com.shoujiduoduo.base.bean.CollectData) r0     // Catch:{ ParserConfigurationException -> 0x00d2, TransformerConfigurationException -> 0x00d9, FileNotFoundException -> 0x00e1, TransformerException -> 0x00e6, Exception -> 0x00eb }
            java.lang.String r2 = "collect"
            org.w3c.dom.Element r7 = r5.createElement(r2)     // Catch:{ ParserConfigurationException -> 0x00d2, TransformerConfigurationException -> 0x00d9, FileNotFoundException -> 0x00e1, TransformerException -> 0x00e6, Exception -> 0x00eb }
            java.lang.String r2 = "title"
            java.lang.String r8 = r0.title     // Catch:{ ParserConfigurationException -> 0x00d2, TransformerConfigurationException -> 0x00d9, FileNotFoundException -> 0x00e1, TransformerException -> 0x00e6, Exception -> 0x00eb }
            r7.setAttribute(r2, r8)     // Catch:{ ParserConfigurationException -> 0x00d2, TransformerConfigurationException -> 0x00d9, FileNotFoundException -> 0x00e1, TransformerException -> 0x00e6, Exception -> 0x00eb }
            java.lang.String r2 = "content"
            java.lang.String r8 = r0.content     // Catch:{ ParserConfigurationException -> 0x00d2, TransformerConfigurationException -> 0x00d9, FileNotFoundException -> 0x00e1, TransformerException -> 0x00e6, Exception -> 0x00eb }
            r7.setAttribute(r2, r8)     // Catch:{ ParserConfigurationException -> 0x00d2, TransformerConfigurationException -> 0x00d9, FileNotFoundException -> 0x00e1, TransformerException -> 0x00e6, Exception -> 0x00eb }
            java.lang.String r2 = "artist"
            java.lang.String r8 = r0.artist     // Catch:{ ParserConfigurationException -> 0x00d2, TransformerConfigurationException -> 0x00d9, FileNotFoundException -> 0x00e1, TransformerException -> 0x00e6, Exception -> 0x00eb }
            r7.setAttribute(r2, r8)     // Catch:{ ParserConfigurationException -> 0x00d2, TransformerConfigurationException -> 0x00d9, FileNotFoundException -> 0x00e1, TransformerException -> 0x00e6, Exception -> 0x00eb }
            java.lang.String r2 = "time"
            java.lang.String r8 = r0.time     // Catch:{ ParserConfigurationException -> 0x00d2, TransformerConfigurationException -> 0x00d9, FileNotFoundException -> 0x00e1, TransformerException -> 0x00e6, Exception -> 0x00eb }
            r7.setAttribute(r2, r8)     // Catch:{ ParserConfigurationException -> 0x00d2, TransformerConfigurationException -> 0x00d9, FileNotFoundException -> 0x00e1, TransformerException -> 0x00e6, Exception -> 0x00eb }
            java.lang.String r2 = "pic"
            java.lang.String r8 = r0.pic     // Catch:{ ParserConfigurationException -> 0x00d2, TransformerConfigurationException -> 0x00d9, FileNotFoundException -> 0x00e1, TransformerException -> 0x00e6, Exception -> 0x00eb }
            r7.setAttribute(r2, r8)     // Catch:{ ParserConfigurationException -> 0x00d2, TransformerConfigurationException -> 0x00d9, FileNotFoundException -> 0x00e1, TransformerException -> 0x00e6, Exception -> 0x00eb }
            java.lang.String r2 = "id"
            java.lang.String r8 = r0.id     // Catch:{ ParserConfigurationException -> 0x00d2, TransformerConfigurationException -> 0x00d9, FileNotFoundException -> 0x00e1, TransformerException -> 0x00e6, Exception -> 0x00eb }
            r7.setAttribute(r2, r8)     // Catch:{ ParserConfigurationException -> 0x00d2, TransformerConfigurationException -> 0x00d9, FileNotFoundException -> 0x00e1, TransformerException -> 0x00e6, Exception -> 0x00eb }
            java.lang.String r8 = "isnew"
            boolean r2 = r0.isNew     // Catch:{ ParserConfigurationException -> 0x00d2, TransformerConfigurationException -> 0x00d9, FileNotFoundException -> 0x00e1, TransformerException -> 0x00e6, Exception -> 0x00eb }
            if (r2 == 0) goto L_0x0082
            java.lang.String r2 = "1"
        L_0x0071:
            r7.setAttribute(r8, r2)     // Catch:{ ParserConfigurationException -> 0x00d2, TransformerConfigurationException -> 0x00d9, FileNotFoundException -> 0x00e1, TransformerException -> 0x00e6, Exception -> 0x00eb }
            java.lang.String r2 = "fav"
            java.lang.String r0 = r0.favNum     // Catch:{ ParserConfigurationException -> 0x00d2, TransformerConfigurationException -> 0x00d9, FileNotFoundException -> 0x00e1, TransformerException -> 0x00e6, Exception -> 0x00eb }
            r7.setAttribute(r2, r0)     // Catch:{ ParserConfigurationException -> 0x00d2, TransformerConfigurationException -> 0x00d9, FileNotFoundException -> 0x00e1, TransformerException -> 0x00e6, Exception -> 0x00eb }
            r6.appendChild(r7)     // Catch:{ ParserConfigurationException -> 0x00d2, TransformerConfigurationException -> 0x00d9, FileNotFoundException -> 0x00e1, TransformerException -> 0x00e6, Exception -> 0x00eb }
            int r0 = r3 + 1
            r3 = r0
            goto L_0x0029
        L_0x0082:
            java.lang.String r2 = "0"
            goto L_0x0071
        L_0x0085:
            javax.xml.transform.TransformerFactory r0 = javax.xml.transform.TransformerFactory.newInstance()     // Catch:{ ParserConfigurationException -> 0x00d2, TransformerConfigurationException -> 0x00d9, FileNotFoundException -> 0x00e1, TransformerException -> 0x00e6, Exception -> 0x00eb }
            javax.xml.transform.Transformer r0 = r0.newTransformer()     // Catch:{ ParserConfigurationException -> 0x00d2, TransformerConfigurationException -> 0x00d9, FileNotFoundException -> 0x00e1, TransformerException -> 0x00e6, Exception -> 0x00eb }
            java.lang.String r2 = "encoding"
            java.lang.String r3 = "utf-8"
            r0.setOutputProperty(r2, r3)     // Catch:{ ParserConfigurationException -> 0x00d2, TransformerConfigurationException -> 0x00d9, FileNotFoundException -> 0x00e1, TransformerException -> 0x00e6, Exception -> 0x00eb }
            java.lang.String r2 = "indent"
            java.lang.String r3 = "yes"
            r0.setOutputProperty(r2, r3)     // Catch:{ ParserConfigurationException -> 0x00d2, TransformerConfigurationException -> 0x00d9, FileNotFoundException -> 0x00e1, TransformerException -> 0x00e6, Exception -> 0x00eb }
            java.lang.String r2 = "standalone"
            java.lang.String r3 = "yes"
            r0.setOutputProperty(r2, r3)     // Catch:{ ParserConfigurationException -> 0x00d2, TransformerConfigurationException -> 0x00d9, FileNotFoundException -> 0x00e1, TransformerException -> 0x00e6, Exception -> 0x00eb }
            java.lang.String r2 = "method"
            java.lang.String r3 = "xml"
            r0.setOutputProperty(r2, r3)     // Catch:{ ParserConfigurationException -> 0x00d2, TransformerConfigurationException -> 0x00d9, FileNotFoundException -> 0x00e1, TransformerException -> 0x00e6, Exception -> 0x00eb }
            javax.xml.transform.dom.DOMSource r2 = new javax.xml.transform.dom.DOMSource     // Catch:{ ParserConfigurationException -> 0x00d2, TransformerConfigurationException -> 0x00d9, FileNotFoundException -> 0x00e1, TransformerException -> 0x00e6, Exception -> 0x00eb }
            org.w3c.dom.Element r3 = r5.getDocumentElement()     // Catch:{ ParserConfigurationException -> 0x00d2, TransformerConfigurationException -> 0x00d9, FileNotFoundException -> 0x00e1, TransformerException -> 0x00e6, Exception -> 0x00eb }
            r2.<init>(r3)     // Catch:{ ParserConfigurationException -> 0x00d2, TransformerConfigurationException -> 0x00d9, FileNotFoundException -> 0x00e1, TransformerException -> 0x00e6, Exception -> 0x00eb }
            java.io.File r3 = new java.io.File     // Catch:{ ParserConfigurationException -> 0x00d2, TransformerConfigurationException -> 0x00d9, FileNotFoundException -> 0x00e1, TransformerException -> 0x00e6, Exception -> 0x00eb }
            java.lang.String r5 = r9.f1372a     // Catch:{ ParserConfigurationException -> 0x00d2, TransformerConfigurationException -> 0x00d9, FileNotFoundException -> 0x00e1, TransformerException -> 0x00e6, Exception -> 0x00eb }
            r3.<init>(r5)     // Catch:{ ParserConfigurationException -> 0x00d2, TransformerConfigurationException -> 0x00d9, FileNotFoundException -> 0x00e1, TransformerException -> 0x00e6, Exception -> 0x00eb }
            boolean r5 = r3.exists()     // Catch:{ ParserConfigurationException -> 0x00d2, TransformerConfigurationException -> 0x00d9, FileNotFoundException -> 0x00e1, TransformerException -> 0x00e6, Exception -> 0x00eb }
            if (r5 != 0) goto L_0x00c2
            r3.createNewFile()     // Catch:{ ParserConfigurationException -> 0x00d2, TransformerConfigurationException -> 0x00d9, FileNotFoundException -> 0x00e1, TransformerException -> 0x00e6, Exception -> 0x00eb }
        L_0x00c2:
            java.io.FileOutputStream r5 = new java.io.FileOutputStream     // Catch:{ ParserConfigurationException -> 0x00d2, TransformerConfigurationException -> 0x00d9, FileNotFoundException -> 0x00e1, TransformerException -> 0x00e6, Exception -> 0x00eb }
            r5.<init>(r3)     // Catch:{ ParserConfigurationException -> 0x00d2, TransformerConfigurationException -> 0x00d9, FileNotFoundException -> 0x00e1, TransformerException -> 0x00e6, Exception -> 0x00eb }
            javax.xml.transform.stream.StreamResult r3 = new javax.xml.transform.stream.StreamResult     // Catch:{ ParserConfigurationException -> 0x00d2, TransformerConfigurationException -> 0x00d9, FileNotFoundException -> 0x00e1, TransformerException -> 0x00e6, Exception -> 0x00eb }
            r3.<init>(r5)     // Catch:{ ParserConfigurationException -> 0x00d2, TransformerConfigurationException -> 0x00d9, FileNotFoundException -> 0x00e1, TransformerException -> 0x00e6, Exception -> 0x00eb }
            r0.transform(r2, r3)     // Catch:{ ParserConfigurationException -> 0x00d2, TransformerConfigurationException -> 0x00d9, FileNotFoundException -> 0x00e1, TransformerException -> 0x00e6, Exception -> 0x00eb }
            r0 = 1
            monitor-exit(r4)     // Catch:{ all -> 0x00de }
        L_0x00d1:
            return r0
        L_0x00d2:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ all -> 0x00de }
        L_0x00d6:
            monitor-exit(r4)     // Catch:{ all -> 0x00de }
            r0 = r1
            goto L_0x00d1
        L_0x00d9:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ all -> 0x00de }
            goto L_0x00d6
        L_0x00de:
            r0 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x00de }
            throw r0
        L_0x00e1:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ all -> 0x00de }
            goto L_0x00d6
        L_0x00e6:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ all -> 0x00de }
            goto L_0x00d6
        L_0x00eb:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ all -> 0x00de }
            goto L_0x00d6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.shoujiduoduo.b.f.a.e():boolean");
    }

    /* renamed from: a */
    public CollectData get(int i) {
        if (i < 0 || i >= this.b.size()) {
            return null;
        }
        return this.b.get(i);
    }

    public int size() {
        return this.b.size();
    }

    public boolean isRetrieving() {
        return false;
    }

    public void retrieveData() {
    }

    public void refreshData() {
    }

    public boolean hasMoreData() {
        return false;
    }

    public ListType.LIST_TYPE getListType() {
        return ListType.LIST_TYPE.list_user_collect;
    }

    public void reloadData() {
    }
}
