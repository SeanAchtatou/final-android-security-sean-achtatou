package com.shoujiduoduo.b.f;

import android.text.TextUtils;
import com.qq.e.comm.constants.Constants;
import com.shoujiduoduo.a.a.b;
import com.shoujiduoduo.a.a.c;
import com.shoujiduoduo.a.c.q;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.base.bean.MakeRingData;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.util.c;
import com.shoujiduoduo.util.i;
import com.shoujiduoduo.util.t;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

/* compiled from: RingUploader */
public class e {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public Timer f1391a;
    /* access modifiers changed from: private */
    public int b;

    static /* synthetic */ int a(e eVar) {
        int i = eVar.b;
        eVar.b = i + 1;
        return i;
    }

    public void a(final MakeRingData makeRingData, final int i) {
        c.a().b(b.OBSERVER_RING_UPLOAD, new c.a<q>() {
            public void a() {
                ((q) this.f1284a).a(makeRingData);
            }
        });
        i.a(new Runnable() {
            public void run() {
                int unused = e.this.b = 0;
                Timer unused2 = e.this.f1391a = new Timer();
                e.this.f1391a.schedule(new TimerTask() {
                    public void run() {
                        e.a(e.this);
                        if (e.this.b > 95) {
                            int unused = e.this.b = 95;
                            e.this.f1391a.cancel();
                        }
                        e.this.b(makeRingData, e.this.b);
                    }
                }, 0, 600);
                HashMap hashMap = new HashMap();
                String str = "";
                if (makeRingData.rid.equals("")) {
                    str = t.e();
                    if (TextUtils.isEmpty(str) || str.equals("0")) {
                        a.c("RingUploader", "genrid error");
                        e.this.a(makeRingData);
                        hashMap.put(Constants.KEYS.RET, "genrid error");
                        com.umeng.a.b.a(RingDDApp.c(), "USER_RING_UPLOAD", hashMap);
                        return;
                    }
                    a.a("RingUploader", "genrid,success. rid:" + str);
                }
                String str2 = makeRingData.localPath;
                String b2 = com.shoujiduoduo.util.q.b(str2);
                c.a aVar = makeRingData.makeType == 0 ? c.a.recordRing : c.a.editRing;
                a.a("RingUploader", "开始BCS 上传");
                if (!com.shoujiduoduo.util.c.a(str2, str + "." + b2, aVar)) {
                    a.c("RingUploader", "bcs 上传失败");
                    e.this.a(makeRingData);
                    hashMap.put(Constants.KEYS.RET, "bcs upload error");
                    com.umeng.a.b.a(RingDDApp.c(), "USER_RING_UPLOAD", hashMap);
                    return;
                }
                a.a("RingUploader", "bcs 上传成功");
                if (t.a(makeRingData, String.valueOf(i), str)) {
                    a.a("RingUploader", "上传后的铃声数据通知服务器。 成功");
                    e.this.a(makeRingData, str);
                    hashMap.put(Constants.KEYS.RET, "success");
                    com.umeng.a.b.a(RingDDApp.c(), "USER_RING_UPLOAD", hashMap);
                    return;
                }
                a.a("RingUploader", "上传后的铃声数据通知服务器。 失败");
                e.this.a(makeRingData);
                hashMap.put(Constants.KEYS.RET, "upload inform error");
                com.umeng.a.b.a(RingDDApp.c(), "USER_RING_UPLOAD", hashMap);
            }
        });
    }

    /* access modifiers changed from: private */
    public void a(final MakeRingData makeRingData, final String str) {
        this.f1391a.cancel();
        com.shoujiduoduo.a.a.c.a().b(b.OBSERVER_RING_UPLOAD, new c.a<q>() {
            public void a() {
                makeRingData.upload = 1;
                makeRingData.percent = 100;
                makeRingData.rid = str;
                ((q) this.f1284a).b(makeRingData);
            }
        });
    }

    /* access modifiers changed from: private */
    public void a(final MakeRingData makeRingData) {
        this.f1391a.cancel();
        com.shoujiduoduo.a.a.c.a().b(b.OBSERVER_RING_UPLOAD, new c.a<q>() {
            public void a() {
                makeRingData.upload = 0;
                makeRingData.percent = -1;
                makeRingData.rid = "";
                ((q) this.f1284a).c(makeRingData);
            }
        });
    }

    /* access modifiers changed from: private */
    public void b(final MakeRingData makeRingData, final int i) {
        com.shoujiduoduo.a.a.c.a().b(b.OBSERVER_RING_UPLOAD, new c.a<q>() {
            public void a() {
                makeRingData.upload = 0;
                makeRingData.percent = i;
                ((q) this.f1284a).a(makeRingData, i);
            }
        });
    }
}
