package com.shoujiduoduo.b.f;

import cn.banshenggua.aichang.utils.StringUtil;
import com.meizu.cloud.pushsdk.constants.PushConstants;
import com.shoujiduoduo.a.a.c;
import com.shoujiduoduo.a.b.b;
import com.shoujiduoduo.a.c.g;
import com.shoujiduoduo.a.c.q;
import com.shoujiduoduo.a.c.v;
import com.shoujiduoduo.a.c.w;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.base.bean.DDList;
import com.shoujiduoduo.base.bean.ListContent;
import com.shoujiduoduo.base.bean.ListType;
import com.shoujiduoduo.base.bean.MakeRingData;
import com.shoujiduoduo.base.bean.RingData;
import com.shoujiduoduo.util.i;
import com.shoujiduoduo.util.j;
import com.shoujiduoduo.util.l;
import com.shoujiduoduo.util.t;
import com.sina.weibo.sdk.constant.WBConstants;
import com.sina.weibo.sdk.register.mobile.SelectCountryActivity;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/* compiled from: MakeRingList */
public class d implements DDList {

    /* renamed from: a  reason: collision with root package name */
    private static final String f1384a = l.b(2);
    private int b = -1;
    private int c = -1;
    private int d = -1;
    /* access modifiers changed from: private */
    public ArrayList<RingData> e = new ArrayList<>();
    /* access modifiers changed from: private */
    public boolean f;
    private v g = new v() {
        public void a(int i, boolean z, String str, String str2) {
            if (z) {
                a.a("MakeRingList", "登录成功，准备更新录制铃声数据");
                if (b.g().g()) {
                    d.this.k();
                }
            }
        }

        public void a(int i) {
            a.a("MakeRingList", "退出登录，移除线上数据");
            d.this.j();
            d.this.g();
            c.a().a(com.shoujiduoduo.a.a.b.OBSERVER_LIST_DATA, new c.a<g>() {
                public void a() {
                    ((g) this.f1284a).a(d.this, 0);
                }
            });
        }

        public void b(int i) {
        }

        public void a(String str, boolean z) {
        }

        public void a(String str) {
        }
    };
    private q h = new q() {
        public void a(RingData ringData) {
        }

        public void a(RingData ringData, int i) {
            for (int i2 = 0; i2 < d.this.e.size(); i2++) {
                MakeRingData makeRingData = (MakeRingData) d.this.e.get(i2);
                if (makeRingData.localPath.equals(((MakeRingData) ringData).localPath)) {
                    makeRingData.percent = i;
                    makeRingData.upload = 0;
                    return;
                }
            }
        }

        public void c(RingData ringData) {
            for (int i = 0; i < d.this.e.size(); i++) {
                MakeRingData makeRingData = (MakeRingData) d.this.e.get(i);
                if (makeRingData.localPath.equals(((MakeRingData) ringData).localPath)) {
                    makeRingData.percent = -1;
                    makeRingData.upload = 0;
                    return;
                }
            }
        }

        public void b(RingData ringData) {
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 >= d.this.e.size()) {
                    break;
                }
                MakeRingData makeRingData = (MakeRingData) d.this.e.get(i2);
                if (makeRingData.localPath.equals(((MakeRingData) ringData).localPath)) {
                    makeRingData.percent = 100;
                    makeRingData.upload = 1;
                    makeRingData.rid = ringData.rid;
                    break;
                }
                i = i2 + 1;
            }
            d.this.i();
            d.this.g();
        }
    };

    public boolean a() {
        return this.f;
    }

    public void b() {
        a.b("MakeRingList", "begin init makering data");
        final boolean g2 = b.g().g();
        i.a(new Runnable() {
            public void run() {
                final ArrayList a2 = d.this.f();
                c.a().a(new c.b() {
                    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                     method: com.shoujiduoduo.b.f.d.a(com.shoujiduoduo.b.f.d, boolean):boolean
                     arg types: [com.shoujiduoduo.b.f.d, int]
                     candidates:
                      com.shoujiduoduo.b.f.d.a(com.shoujiduoduo.b.f.d, java.util.ArrayList):java.util.ArrayList
                      com.shoujiduoduo.b.f.d.a(com.shoujiduoduo.b.f.d, boolean):boolean */
                    public void a() {
                        if (a2 != null) {
                            ArrayList unused = d.this.e = a2;
                        }
                        boolean unused2 = d.this.f = true;
                        c.a().a(com.shoujiduoduo.a.a.b.OBSERVER_USER_RING, new c.a<w>() {
                            public void a() {
                                ((w) this.f1284a).a(0, null, "user_make");
                            }
                        });
                    }
                });
                if (g2) {
                    d.this.k();
                }
            }
        });
        a.b("MakeRingList", "end init makering data");
    }

    public void c() {
        c.a().a(com.shoujiduoduo.a.a.b.OBSERVER_USER_CENTER, this.g);
        c.a().a(com.shoujiduoduo.a.a.b.OBSERVER_RING_UPLOAD, this.h);
    }

    public void d() {
        c.a().b(com.shoujiduoduo.a.a.b.OBSERVER_USER_CENTER, this.g);
        c.a().b(com.shoujiduoduo.a.a.b.OBSERVER_RING_UPLOAD, this.h);
    }

    public String getListId() {
        return "user_make";
    }

    public int e() {
        return this.e.size();
    }

    public boolean a(String str) {
        synchronized ("MakeRingList") {
            for (int i = 0; i < this.e.size(); i++) {
                if (str.equalsIgnoreCase(this.e.get(i).rid)) {
                    return true;
                }
            }
            return false;
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x0166, code lost:
        r8.duration = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x016b, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:?, code lost:
        r1.printStackTrace();
        r1 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:?, code lost:
        r8.score = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x018c, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:?, code lost:
        r1.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:0x0190, code lost:
        r1 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:0x0198, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:72:?, code lost:
        r1.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:73:0x019c, code lost:
        r1 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:0x019e, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:77:?, code lost:
        r8.makeType = 0;
        r8.upload = 0;
        r1.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:78:0x01a9, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:80:?, code lost:
        r1.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:81:0x01ad, code lost:
        r1 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:85:0x01e6, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:87:?, code lost:
        r1.printStackTrace();
        r1 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:88:0x01ec, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:89:0x01ed, code lost:
        r1.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:90:0x01f0, code lost:
        r1 = true;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x016b A[ExcHandler: FileNotFoundException (r1v18 'e' java.io.FileNotFoundException A[CUSTOM_DECLARE]), Splitter:B:19:0x002a] */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x018c A[ExcHandler: ParserConfigurationException (r1v11 'e' javax.xml.parsers.ParserConfigurationException A[CUSTOM_DECLARE]), Splitter:B:19:0x002a] */
    /* JADX WARNING: Removed duplicated region for block: B:70:0x0198 A[ExcHandler: SAXException (r1v9 'e' org.xml.sax.SAXException A[CUSTOM_DECLARE]), Splitter:B:19:0x002a] */
    /* JADX WARNING: Removed duplicated region for block: B:78:0x01a9 A[ExcHandler: IOException (r1v7 'e' java.io.IOException A[CUSTOM_DECLARE]), Splitter:B:19:0x002a] */
    /* JADX WARNING: Removed duplicated region for block: B:88:0x01ec A[Catch:{ IOException -> 0x0016 }, ExcHandler: NullPointerException (r1v3 'e' java.lang.NullPointerException A[CUSTOM_DECLARE, Catch:{  }]), Splitter:B:19:0x002a] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized java.util.ArrayList<com.shoujiduoduo.base.bean.RingData> f() {
        /*
            r10 = this;
            r3 = 1
            r2 = 0
            monitor-enter(r10)
            java.io.File r0 = new java.io.File     // Catch:{ all -> 0x001b }
            java.lang.String r1 = com.shoujiduoduo.b.f.d.f1384a     // Catch:{ all -> 0x001b }
            r0.<init>(r1)     // Catch:{ all -> 0x001b }
            boolean r1 = r0.exists()     // Catch:{ all -> 0x001b }
            if (r1 != 0) goto L_0x001e
            r0.createNewFile()     // Catch:{ IOException -> 0x0016 }
        L_0x0013:
            r0 = 0
        L_0x0014:
            monitor-exit(r10)
            return r0
        L_0x0016:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ all -> 0x001b }
            goto L_0x0013
        L_0x001b:
            r0 = move-exception
            monitor-exit(r10)
            throw r0
        L_0x001e:
            java.lang.String r0 = "MakeRingList"
            java.lang.String r1 = "UserRingMake: read begin"
            com.shoujiduoduo.base.a.a.a(r0, r1)     // Catch:{ all -> 0x001b }
            java.util.ArrayList r0 = new java.util.ArrayList     // Catch:{ all -> 0x001b }
            r0.<init>()     // Catch:{ all -> 0x001b }
            java.io.FileInputStream r1 = new java.io.FileInputStream     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            java.lang.String r4 = com.shoujiduoduo.b.f.d.f1384a     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            r1.<init>(r4)     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            javax.xml.parsers.DocumentBuilderFactory r4 = javax.xml.parsers.DocumentBuilderFactory.newInstance()     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            javax.xml.parsers.DocumentBuilder r4 = r4.newDocumentBuilder()     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            org.w3c.dom.Document r1 = r4.parse(r1)     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            org.w3c.dom.Element r1 = r1.getDocumentElement()     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            java.lang.String r4 = "phone_sel"
            java.lang.String r4 = r1.getAttribute(r4)     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            int r4 = java.lang.Integer.parseInt(r4)     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            r10.b = r4     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            java.lang.String r4 = "alarm_sel"
            java.lang.String r4 = r1.getAttribute(r4)     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            int r4 = java.lang.Integer.parseInt(r4)     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            r10.c = r4     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            java.lang.String r4 = "notification_sel"
            java.lang.String r4 = r1.getAttribute(r4)     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            int r4 = java.lang.Integer.parseInt(r4)     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            r10.d = r4     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            java.lang.String r4 = "ring"
            org.w3c.dom.NodeList r6 = r1.getElementsByTagName(r4)     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            r5 = r2
        L_0x006c:
            int r1 = r6.getLength()     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            if (r5 >= r1) goto L_0x01af
            org.w3c.dom.Node r1 = r6.item(r5)     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            org.w3c.dom.NamedNodeMap r7 = r1.getAttributes()     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            com.shoujiduoduo.base.bean.MakeRingData r8 = new com.shoujiduoduo.base.bean.MakeRingData     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            r8.<init>()     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            java.lang.String r1 = "name"
            java.lang.String r1 = com.shoujiduoduo.util.g.a(r7, r1)     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            r8.name = r1     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            java.lang.String r1 = "artist"
            java.lang.String r1 = com.shoujiduoduo.util.g.a(r7, r1)     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            r8.artist = r1     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            java.lang.String r1 = "duration"
            java.lang.String r1 = com.shoujiduoduo.util.g.a(r7, r1)     // Catch:{ NumberFormatException -> 0x0165, FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NullPointerException -> 0x01ec }
            int r1 = java.lang.Integer.parseInt(r1)     // Catch:{ NumberFormatException -> 0x0165, FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NullPointerException -> 0x01ec }
            r8.duration = r1     // Catch:{ NumberFormatException -> 0x0165, FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NullPointerException -> 0x01ec }
        L_0x009b:
            java.lang.String r1 = "score"
            java.lang.String r1 = com.shoujiduoduo.util.g.a(r7, r1)     // Catch:{ NumberFormatException -> 0x0186, FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NullPointerException -> 0x01ec }
            int r1 = java.lang.Integer.parseInt(r1)     // Catch:{ NumberFormatException -> 0x0186, FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NullPointerException -> 0x01ec }
            r8.score = r1     // Catch:{ NumberFormatException -> 0x0186, FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NullPointerException -> 0x01ec }
        L_0x00a7:
            java.lang.String r1 = "playcnt"
            java.lang.String r1 = com.shoujiduoduo.util.g.a(r7, r1)     // Catch:{ NumberFormatException -> 0x0192, FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NullPointerException -> 0x01ec }
            int r1 = java.lang.Integer.parseInt(r1)     // Catch:{ NumberFormatException -> 0x0192, FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NullPointerException -> 0x01ec }
            r8.playcnt = r1     // Catch:{ NumberFormatException -> 0x0192, FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NullPointerException -> 0x01ec }
        L_0x00b3:
            java.lang.String r1 = "cid"
            java.lang.String r1 = com.shoujiduoduo.util.g.a(r7, r1)     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            r8.cid = r1     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            java.lang.String r1 = "valid"
            java.lang.String r1 = com.shoujiduoduo.util.g.a(r7, r1)     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            r8.valid = r1     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            java.lang.String r1 = "singerId"
            java.lang.String r1 = com.shoujiduoduo.util.g.a(r7, r1)     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            r8.singerId = r1     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            java.lang.String r1 = "price"
            java.lang.String r1 = com.shoujiduoduo.util.g.a(r7, r1)     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            r4 = 200(0xc8, float:2.8E-43)
            int r1 = com.shoujiduoduo.util.r.a(r1, r4)     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            r8.price = r1     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            java.lang.String r1 = "hasmedia"
            java.lang.String r1 = com.shoujiduoduo.util.g.a(r7, r1)     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            r4 = 0
            int r1 = com.shoujiduoduo.util.r.a(r1, r4)     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            r8.hasmedia = r1     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            java.lang.String r1 = "ctcid"
            java.lang.String r1 = com.shoujiduoduo.util.g.a(r7, r1)     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            r8.ctcid = r1     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            java.lang.String r1 = "ctvalid"
            java.lang.String r1 = com.shoujiduoduo.util.g.a(r7, r1)     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            r8.ctvalid = r1     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            java.lang.String r1 = "ctprice"
            java.lang.String r1 = com.shoujiduoduo.util.g.a(r7, r1)     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            r4 = 200(0xc8, float:2.8E-43)
            int r1 = com.shoujiduoduo.util.r.a(r1, r4)     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            r8.ctprice = r1     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            java.lang.String r1 = "cthasmedia"
            java.lang.String r1 = com.shoujiduoduo.util.g.a(r7, r1)     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            r4 = 0
            int r1 = com.shoujiduoduo.util.r.a(r1, r4)     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            r8.cthasmedia = r1     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            java.lang.String r1 = "localPath"
            java.lang.String r1 = com.shoujiduoduo.util.g.a(r7, r1)     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            r8.localPath = r1     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            java.lang.String r1 = "makeDate"
            java.lang.String r1 = com.shoujiduoduo.util.g.a(r7, r1)     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            r8.makeDate = r1     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            java.lang.String r1 = "makeType"
            java.lang.String r4 = com.shoujiduoduo.util.g.a(r7, r1)     // Catch:{ NumberFormatException -> 0x019e, FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NullPointerException -> 0x01ec }
            java.lang.String r1 = "upload"
            java.lang.String r1 = com.shoujiduoduo.util.g.a(r7, r1)     // Catch:{ NumberFormatException -> 0x019e, FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NullPointerException -> 0x01ec }
            java.lang.String r9 = ""
            boolean r9 = r4.equals(r9)     // Catch:{ NumberFormatException -> 0x019e, FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NullPointerException -> 0x01ec }
            if (r9 == 0) goto L_0x0137
            java.lang.String r4 = "0"
        L_0x0137:
            int r4 = java.lang.Integer.parseInt(r4)     // Catch:{ NumberFormatException -> 0x019e, FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NullPointerException -> 0x01ec }
            r8.makeType = r4     // Catch:{ NumberFormatException -> 0x019e, FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NullPointerException -> 0x01ec }
            java.lang.String r4 = ""
            boolean r4 = r1.equals(r4)     // Catch:{ NumberFormatException -> 0x019e, FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NullPointerException -> 0x01ec }
            if (r4 == 0) goto L_0x0147
            java.lang.String r1 = "0"
        L_0x0147:
            int r1 = java.lang.Integer.parseInt(r1)     // Catch:{ NumberFormatException -> 0x019e, FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NullPointerException -> 0x01ec }
            r8.upload = r1     // Catch:{ NumberFormatException -> 0x019e, FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NullPointerException -> 0x01ec }
        L_0x014d:
            java.lang.String r1 = "rid"
            java.lang.String r1 = com.shoujiduoduo.util.g.a(r7, r1)     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            r8.rid = r1     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            java.lang.String r1 = "bdurl"
            java.lang.String r1 = com.shoujiduoduo.util.g.a(r7, r1)     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            r8.baiduURL = r1     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            r0.add(r8)     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            int r1 = r5 + 1
            r5 = r1
            goto L_0x006c
        L_0x0165:
            r1 = move-exception
            r1 = 0
            r8.duration = r1     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            goto L_0x009b
        L_0x016b:
            r1 = move-exception
            r1.printStackTrace()     // Catch:{ all -> 0x001b }
            r1 = r3
        L_0x0170:
            if (r1 == 0) goto L_0x017b
            r1 = -1
            r10.b = r1     // Catch:{ all -> 0x001b }
            r1 = -1
            r10.c = r1     // Catch:{ all -> 0x001b }
            r1 = -1
            r10.d = r1     // Catch:{ all -> 0x001b }
        L_0x017b:
            int r1 = r0.size()     // Catch:{ all -> 0x001b }
            if (r1 <= r3) goto L_0x0014
            r10.b(r0)     // Catch:{ all -> 0x001b }
            goto L_0x0014
        L_0x0186:
            r1 = move-exception
            r1 = 0
            r8.score = r1     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            goto L_0x00a7
        L_0x018c:
            r1 = move-exception
            r1.printStackTrace()     // Catch:{ all -> 0x001b }
            r1 = r3
            goto L_0x0170
        L_0x0192:
            r1 = move-exception
            r1 = 0
            r8.playcnt = r1     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            goto L_0x00b3
        L_0x0198:
            r1 = move-exception
            r1.printStackTrace()     // Catch:{ all -> 0x001b }
            r1 = r3
            goto L_0x0170
        L_0x019e:
            r1 = move-exception
            r4 = 0
            r8.makeType = r4     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            r4 = 0
            r8.upload = r4     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            r1.printStackTrace()     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            goto L_0x014d
        L_0x01a9:
            r1 = move-exception
            r1.printStackTrace()     // Catch:{ all -> 0x001b }
            r1 = r3
            goto L_0x0170
        L_0x01af:
            java.util.HashMap r1 = new java.util.HashMap     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            r1.<init>()     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            android.content.Context r4 = com.shoujiduoduo.ringtone.RingDDApp.c()     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            java.lang.String r5 = "MAKE_RING_NUM"
            int r6 = r0.size()     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            long r6 = (long) r6     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            com.shoujiduoduo.util.g.a(r4, r5, r1, r6)     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            java.lang.String r1 = "MakeRingList"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            r4.<init>()     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            java.lang.String r5 = "read "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            int r5 = r10.e()     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            java.lang.String r5 = " rings."
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            java.lang.String r4 = r4.toString()     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            com.shoujiduoduo.base.a.a.a(r1, r4)     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            r1 = r2
            goto L_0x0170
        L_0x01e6:
            r1 = move-exception
            r1.printStackTrace()     // Catch:{ all -> 0x001b }
            r1 = r3
            goto L_0x0170
        L_0x01ec:
            r1 = move-exception
            r1.printStackTrace()     // Catch:{ all -> 0x001b }
            r1 = r3
            goto L_0x0170
        */
        throw new UnsupportedOperationException("Method not decompiled: com.shoujiduoduo.b.f.d.f():java.util.ArrayList");
    }

    /* access modifiers changed from: private */
    public void g() {
        i.a(new Runnable() {
            public void run() {
                boolean unused = d.this.h();
            }
        });
    }

    /* access modifiers changed from: private */
    public synchronized boolean h() {
        boolean z;
        try {
            Document newDocument = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
            Element createElement = newDocument.createElement("list");
            createElement.setAttribute("num", String.valueOf(this.e.size()));
            createElement.setAttribute("phone_sel", "" + this.b);
            createElement.setAttribute("alarm_sel", "" + this.c);
            createElement.setAttribute("notification_sel", "" + this.d);
            newDocument.appendChild(createElement);
            for (int i = 0; i < this.e.size(); i++) {
                MakeRingData makeRingData = (MakeRingData) this.e.get(i);
                Element createElement2 = newDocument.createElement("ring");
                createElement2.setAttribute(SelectCountryActivity.EXTRA_COUNTRY_NAME, makeRingData.name);
                createElement2.setAttribute("artist", makeRingData.artist);
                createElement2.setAttribute("duration", String.valueOf(makeRingData.duration));
                createElement2.setAttribute(WBConstants.GAME_PARAMS_SCORE, String.valueOf(makeRingData.score));
                createElement2.setAttribute("playcnt", String.valueOf(makeRingData.playcnt));
                createElement2.setAttribute("rid", makeRingData.rid);
                createElement2.setAttribute("bdurl", makeRingData.baiduURL);
                createElement2.setAttribute("localPath", makeRingData.localPath);
                createElement2.setAttribute("makeType", String.valueOf(makeRingData.makeType));
                createElement2.setAttribute("makeDate", makeRingData.makeDate);
                createElement2.setAttribute("upload", String.valueOf(makeRingData.upload));
                createElement.appendChild(createElement2);
            }
            Transformer newTransformer = TransformerFactory.newInstance().newTransformer();
            newTransformer.setOutputProperty("encoding", StringUtil.Encoding);
            newTransformer.setOutputProperty("indent", "yes");
            newTransformer.setOutputProperty("standalone", "yes");
            newTransformer.setOutputProperty(PushConstants.MZ_PUSH_MESSAGE_METHOD, "xml");
            newTransformer.transform(new DOMSource(newDocument.getDocumentElement()), new StreamResult(new FileOutputStream(new File(f1384a))));
            z = true;
        } catch (ParserConfigurationException e2) {
            e2.printStackTrace();
            z = false;
            return z;
        } catch (TransformerConfigurationException e3) {
            e3.printStackTrace();
            z = false;
            return z;
        } catch (FileNotFoundException e4) {
            e4.printStackTrace();
            z = false;
            return z;
        } catch (TransformerException e5) {
            e5.printStackTrace();
            z = false;
            return z;
        } catch (Exception e6) {
            e6.printStackTrace();
            z = false;
            return z;
        }
        return z;
    }

    public synchronized RingData b(String str) {
        RingData ringData;
        Iterator<RingData> it = this.e.iterator();
        while (true) {
            if (!it.hasNext()) {
                ringData = null;
                break;
            }
            ringData = it.next();
            if (ringData.rid.equals(str)) {
                break;
            }
        }
        return ringData;
    }

    public synchronized boolean a(Collection<Integer> collection) {
        ArrayList arrayList = new ArrayList();
        StringBuilder sb = new StringBuilder();
        for (Integer next : collection) {
            arrayList.add((MakeRingData) this.e.get(next.intValue()));
            if (!((MakeRingData) this.e.get(next.intValue())).rid.equals("")) {
                sb.append(this.e.get(next.intValue()).rid);
                sb.append("|");
            }
            try {
                File file = new File(((MakeRingData) this.e.get(next.intValue())).localPath);
                if (file.exists()) {
                    file.delete();
                }
            } catch (Exception e2) {
            }
        }
        if (sb.toString().endsWith("|")) {
            sb.deleteCharAt(sb.length() - 1);
        }
        t.h(sb.toString());
        this.e.removeAll(arrayList);
        i();
        g();
        return true;
    }

    public synchronized boolean a(int i) {
        boolean z;
        if (i >= this.e.size() || i < 0) {
            z = false;
        } else {
            try {
                File file = new File(((MakeRingData) this.e.get(i)).localPath);
                if (file.exists()) {
                    file.delete();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
            t.h(this.e.get(i).rid);
            this.e.remove(i);
            i();
            g();
            z = true;
        }
        return z;
    }

    /* access modifiers changed from: private */
    public synchronized void i() {
        c.a().a(com.shoujiduoduo.a.a.b.OBSERVER_LIST_DATA, new c.a<g>() {
            public void a() {
                ((g) this.f1284a).a(d.this, 0);
            }
        });
    }

    public synchronized boolean a(MakeRingData makeRingData) {
        boolean addAll;
        ArrayList arrayList = new ArrayList();
        arrayList.add(makeRingData);
        addAll = this.e.addAll(0, arrayList);
        i();
        g();
        return addAll;
    }

    public String getBaseURL() {
        return "";
    }

    /* access modifiers changed from: private */
    public synchronized void j() {
        Iterator<RingData> it = this.e.iterator();
        while (it.hasNext()) {
            if (((MakeRingData) it.next()).localPath.equals("")) {
                it.remove();
            }
        }
    }

    /* access modifiers changed from: private */
    public synchronized ArrayList<RingData> a(ArrayList<MakeRingData> arrayList) {
        ArrayList<RingData> arrayList2;
        boolean z;
        arrayList2 = new ArrayList<>();
        Iterator<RingData> it = this.e.iterator();
        while (it.hasNext()) {
            RingData next = it.next();
            if (!next.localPath.equals("")) {
                MakeRingData makeRingData = new MakeRingData();
                makeRingData.copy(next);
                arrayList2.add(makeRingData);
            }
        }
        Iterator<MakeRingData> it2 = arrayList.iterator();
        while (it2.hasNext()) {
            RingData next2 = it2.next();
            Iterator<RingData> it3 = this.e.iterator();
            while (true) {
                if (!it3.hasNext()) {
                    z = false;
                    break;
                }
                RingData next3 = it3.next();
                if (next2.rid.equals(next3.rid)) {
                    if (((MakeRingData) next3).localPath.equals("")) {
                        MakeRingData makeRingData2 = new MakeRingData();
                        makeRingData2.copy(next3);
                        arrayList2.add(makeRingData2);
                        z = true;
                    } else {
                        z = true;
                    }
                }
            }
            if (!z) {
                MakeRingData makeRingData3 = new MakeRingData();
                makeRingData3.copy(next2);
                arrayList2.add(makeRingData3);
            }
        }
        b(arrayList2);
        return arrayList2;
    }

    private void b(ArrayList<RingData> arrayList) {
        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        Collections.sort(arrayList, new Comparator<RingData>() {
            /* renamed from: a */
            public int compare(RingData ringData, RingData ringData2) {
                try {
                    int compareTo = simpleDateFormat.parse(((MakeRingData) ringData).makeDate).compareTo(simpleDateFormat.parse(((MakeRingData) ringData2).makeDate));
                    if (compareTo > 0) {
                        return -1;
                    }
                    if (compareTo != 0) {
                        return 1;
                    }
                    return 0;
                } catch (ParseException e) {
                    e.printStackTrace();
                    return 0;
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public void k() {
        i.a(new Runnable() {
            public void run() {
                ListContent<MakeRingData> listContent;
                String f = t.f();
                if (f == null) {
                    a.c("MakeRingList", "get user makering failed");
                    return;
                }
                a.a("MakeRingList", "获取线上数据成功");
                try {
                    listContent = j.h(new ByteArrayInputStream(f.getBytes()));
                } catch (ArrayIndexOutOfBoundsException e) {
                    a.c("MakeRingList", "parse user makering exception");
                    e.printStackTrace();
                    listContent = null;
                }
                if (listContent != null) {
                    a.a("MakeRingList", "解析线上数据成功，线上数据个数：" + listContent.data.size());
                    a.a("MakeRingList", "准备merge数据");
                    final ArrayList b = d.this.a((ArrayList<MakeRingData>) listContent.data);
                    c.a().a(new c.b() {
                        public void a() {
                            if (b != null) {
                                ArrayList unused = d.this.e = b;
                            }
                            c.a().a(com.shoujiduoduo.a.a.b.OBSERVER_LIST_DATA, new c.a<g>() {
                                public void a() {
                                    ((g) this.f1284a).a(d.this, 0);
                                }
                            });
                            d.this.g();
                        }
                    });
                    a.a("MakeRingList", "merge 完毕， 总共个数：" + d.this.e.size());
                    return;
                }
                a.a("MakeRingList", "解析线上数据失败");
            }
        });
    }

    /* renamed from: b */
    public RingData get(int i) {
        if (i < 0 || i >= this.e.size()) {
            return null;
        }
        return this.e.get(i);
    }

    public int size() {
        return this.e.size();
    }

    public boolean isRetrieving() {
        return false;
    }

    public void retrieveData() {
    }

    public void refreshData() {
    }

    public boolean hasMoreData() {
        return false;
    }

    public ListType.LIST_TYPE getListType() {
        return ListType.LIST_TYPE.list_user_make;
    }

    public void reloadData() {
    }
}
