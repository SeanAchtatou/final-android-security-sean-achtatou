package com.jaeger.library;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.support.v4.view.ViewCompat;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import org.apache.mina.proxy.handlers.http.ntlm.NTLMConstants;

/* compiled from: StatusBarUtil */
public class a {
    public static void a(Activity activity, int i, int i2) {
        if (Build.VERSION.SDK_INT >= 21) {
            activity.getWindow().addFlags(Integer.MIN_VALUE);
            activity.getWindow().clearFlags(NTLMConstants.FLAG_UNIDENTIFIED_9);
            activity.getWindow().setStatusBarColor(a(i, i2));
        } else if (Build.VERSION.SDK_INT >= 19) {
            activity.getWindow().addFlags(NTLMConstants.FLAG_UNIDENTIFIED_9);
            ViewGroup viewGroup = (ViewGroup) activity.getWindow().getDecorView();
            int childCount = viewGroup.getChildCount();
            if (childCount <= 0 || !(viewGroup.getChildAt(childCount - 1) instanceof StatusBarView)) {
                viewGroup.addView(b(activity, i, i2));
            } else {
                viewGroup.getChildAt(childCount - 1).setBackgroundColor(a(i, i2));
            }
            a(activity);
        }
    }

    private static StatusBarView b(Activity activity, int i, int i2) {
        StatusBarView statusBarView = new StatusBarView(activity);
        statusBarView.setLayoutParams(new LinearLayout.LayoutParams(-1, a((Context) activity)));
        statusBarView.setBackgroundColor(a(i, i2));
        return statusBarView;
    }

    private static void a(Activity activity) {
        ViewGroup viewGroup = (ViewGroup) activity.findViewById(16908290);
        int childCount = viewGroup.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = viewGroup.getChildAt(i);
            if (childAt instanceof ViewGroup) {
                childAt.setFitsSystemWindows(true);
                ((ViewGroup) childAt).setClipToPadding(true);
            }
        }
    }

    private static int a(Context context) {
        return context.getResources().getDimensionPixelSize(context.getResources().getIdentifier("status_bar_height", "dimen", "android"));
    }

    private static int a(int i, int i2) {
        float f = 1.0f - (((float) i2) / 255.0f);
        return ((int) (((double) (f * ((float) (i & 255)))) + 0.5d)) | (((int) (((double) (((float) ((i >> 16) & 255)) * f)) + 0.5d)) << 16) | ViewCompat.MEASURED_STATE_MASK | (((int) (((double) (((float) ((i >> 8) & 255)) * f)) + 0.5d)) << 8);
    }
}
