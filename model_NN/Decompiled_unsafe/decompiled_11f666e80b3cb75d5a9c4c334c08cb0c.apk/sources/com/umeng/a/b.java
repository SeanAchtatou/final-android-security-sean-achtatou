package com.umeng.a;

import android.content.Context;
import com.umeng.a.a.aw;
import java.util.HashMap;
import java.util.Map;

/* compiled from: MobclickAgent */
public class b {

    /* renamed from: a  reason: collision with root package name */
    private static final d f2741a = new d();

    public static void a(Context context) {
        f2741a.b(context);
    }

    public static void b(Context context) {
        if (context == null) {
            aw.c("unexpected null context in onResume");
        } else {
            f2741a.a(context);
        }
    }

    public static void a(Context context, String str) {
        f2741a.a(context, str);
    }

    public static void a(Context context, Throwable th) {
        f2741a.a(context, th);
    }

    public static void b(Context context, String str) {
        f2741a.a(context, str, null, -1, 1);
    }

    public static void a(Context context, String str, Map<String, String> map) {
        if (map == null) {
            aw.c("input map is null");
            return;
        }
        f2741a.a(context, str, new HashMap(map), -1);
    }

    public static void a(Context context, String str, Map<String, String> map, int i) {
        HashMap hashMap;
        if (map == null) {
            hashMap = new HashMap();
        } else {
            hashMap = new HashMap(map);
        }
        hashMap.put("__ct__", Integer.valueOf(i));
        f2741a.a(context, str, hashMap, -1);
    }

    public static void c(Context context) {
        f2741a.c(context);
    }

    /* compiled from: MobclickAgent */
    public enum a {
        E_UM_NORMAL(0),
        E_UM_GAME(1),
        E_UM_ANALYTICS_OEM(224),
        E_UM_GAME_OEM(225);
        
        private int e;

        private a(int i) {
            this.e = i;
        }

        public int a() {
            return this.e;
        }
    }
}
