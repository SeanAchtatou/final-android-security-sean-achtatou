package com.umeng.a;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.text.TextUtils;
import com.umeng.a.a.aa;
import com.umeng.a.a.ad;
import com.umeng.a.a.af;
import com.umeng.a.a.ar;
import com.umeng.a.a.av;
import com.umeng.a.a.aw;
import com.umeng.a.a.ax;
import com.umeng.a.a.ba;
import com.umeng.a.a.ck;
import com.umeng.a.a.co;
import com.umeng.a.a.cx;
import com.umeng.a.a.p;
import com.umeng.a.a.r;
import com.umeng.a.a.s;
import com.umeng.a.a.t;
import com.umeng.a.a.x;
import java.util.Map;
import org.json.JSONObject;

/* compiled from: InternalAgent */
public class d implements x {

    /* renamed from: a  reason: collision with root package name */
    private Context f2744a = null;
    private av b;
    private s c = new s();
    private af d = new af();
    private ad e = new ad();
    private t f = null;
    private r g = null;
    private p h = null;
    /* access modifiers changed from: private */
    public co i = null;
    private boolean j = false;
    /* access modifiers changed from: private */
    public boolean k = false;
    private JSONObject l = null;
    private boolean m = false;

    d() {
        this.c.a(this);
    }

    private void d(Context context) {
        if (context != null) {
            try {
                if (Build.VERSION.SDK_INT > 13 && !this.m && (context instanceof Activity)) {
                    this.h = new p((Activity) context);
                    this.m = true;
                }
                if (!this.j) {
                    this.f2744a = context.getApplicationContext();
                    this.f = new t(this.f2744a);
                    this.g = r.b(this.f2744a);
                    this.j = true;
                    if (this.i == null) {
                        this.i = co.a(this.f2744a);
                    }
                    if (!this.k) {
                        ax.b(new ba() {
                            public void a() {
                                d.this.i.a(new ck() {
                                    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                                     method: com.umeng.a.d.a(com.umeng.a.d, boolean):boolean
                                     arg types: [com.umeng.a.d, int]
                                     candidates:
                                      com.umeng.a.d.a(com.umeng.a.d, android.content.Context):void
                                      com.umeng.a.d.a(android.content.Context, java.lang.String):void
                                      com.umeng.a.d.a(android.content.Context, java.lang.Throwable):void
                                      com.umeng.a.d.a(com.umeng.a.d, boolean):boolean */
                                    public void a(Object obj, boolean z) {
                                        boolean unused = d.this.k = true;
                                    }
                                });
                            }
                        });
                    }
                }
            } catch (Throwable th) {
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a(final Context context) {
        if (context == null) {
            aw.c("unexpected null context in onResume");
            return;
        }
        if (a.e) {
            this.d.a(context.getClass().getName());
        }
        try {
            if (!this.j || !this.m) {
                d(context);
            }
            ax.a(new ba() {
                public void a() {
                    d.this.e(context.getApplicationContext());
                }
            });
        } catch (Exception e2) {
            aw.a("Exception occurred in Mobclick.onResume(). ", e2);
        }
    }

    /* access modifiers changed from: package-private */
    public void b(final Context context) {
        if (context == null) {
            aw.c("unexpected null context in onPause");
            return;
        }
        if (a.e) {
            this.d.b(context.getClass().getName());
        }
        try {
            if (!this.j || !this.m) {
                d(context);
            }
            ax.a(new ba() {
                public void a() {
                    d.this.f(context.getApplicationContext());
                    d.this.i.e();
                }
            });
        } catch (Exception e2) {
            if (aw.f2645a) {
                aw.a("Exception occurred in Mobclick.onRause(). ", e2);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a(Context context, String str) {
        if (!TextUtils.isEmpty(str)) {
            if (context == null) {
                aw.c("unexpected null context in reportError");
                return;
            }
            try {
                if (!this.j || !this.m) {
                    d(context);
                }
                JSONObject jSONObject = new JSONObject();
                jSONObject.put("ts", System.currentTimeMillis());
                jSONObject.put("error_source", 2);
                jSONObject.put("context", str);
                cx.a(this.f2744a).a(ad.a(), jSONObject.toString(), 2);
            } catch (Exception e2) {
                if (aw.f2645a) {
                    aw.a(e2);
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a(Context context, Throwable th) {
        if (context != null && th != null) {
            try {
                a(context, ar.a(th));
            } catch (Exception e2) {
                if (aw.f2645a) {
                    aw.a(e2);
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void e(Context context) {
        this.e.c(context);
        if (this.b != null) {
            this.b.a();
        }
    }

    /* access modifiers changed from: private */
    public void f(Context context) {
        this.e.d(context);
        af.a(context);
        p.b(context);
        this.g.a(this.f2744a).a(context);
        if (this.b != null) {
            this.b.b();
        }
    }

    public void a(Context context, String str, String str2, long j2, int i2) {
        try {
            if (!this.j || !this.m) {
                d(context);
            }
            this.f.a(str, str2, j2, i2);
        } catch (Exception e2) {
            if (aw.f2645a) {
                aw.a(e2);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a(Context context, String str, Map<String, Object> map, long j2) {
        try {
            if (!this.j || !this.m) {
                d(context);
            }
            this.f.a(str, map, j2);
        } catch (Exception e2) {
            if (aw.f2645a) {
                aw.a(e2);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void c(Context context) {
        try {
            this.h.a(context);
            this.d.a();
            f(context);
            aa.a(context).edit().commit();
            this.i.c();
            ax.a();
        } catch (Exception e2) {
            if (aw.f2645a) {
                e2.printStackTrace();
            }
        }
    }

    public void a(Throwable th) {
        try {
            this.d.a();
            if (this.f2744a != null) {
                if (!(th == null || this.g == null)) {
                    JSONObject jSONObject = new JSONObject();
                    jSONObject.put("ts", System.currentTimeMillis());
                    jSONObject.put("error_source", 1);
                    jSONObject.put("context", ar.a(th));
                    cx.a(this.f2744a).a(ad.a(), jSONObject.toString(), 1);
                }
                this.i.d();
                this.h.a(this.f2744a);
                f(this.f2744a);
                aa.a(this.f2744a).edit().commit();
            }
            ax.a();
        } catch (Exception e2) {
            if (aw.f2645a) {
                aw.a("Exception in onAppCrash", e2);
            }
        }
    }
}
