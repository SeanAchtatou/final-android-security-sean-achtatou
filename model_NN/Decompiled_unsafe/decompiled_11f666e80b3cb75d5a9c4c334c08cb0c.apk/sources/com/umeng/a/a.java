package com.umeng.a;

import android.content.Context;
import android.text.TextUtils;
import com.umeng.a.a.at;
import com.umeng.a.a.bb;

/* compiled from: AnalyticsConfig */
public class a {

    /* renamed from: a  reason: collision with root package name */
    public static String f2614a = null;
    public static String b = null;
    public static String c = "";
    public static String d = "";
    public static boolean e = true;
    public static boolean f = true;
    public static long g = StatisticConfig.MIN_UPLOAD_INTERVAL;
    public static boolean h = false;
    public static int i;
    static double[] j = null;
    private static String k = null;
    private static String l = null;
    private static String m = null;
    private static int n = 0;

    public static String a(Context context) {
        if (TextUtils.isEmpty(k)) {
            k = at.m(context);
            if (TextUtils.isEmpty(k)) {
                k = bb.a(context).b();
            }
        }
        return k;
    }

    public static String b(Context context) {
        if (TextUtils.isEmpty(l)) {
            l = at.p(context);
        }
        return l;
    }

    public static double[] a() {
        return j;
    }

    public static String c(Context context) {
        if (TextUtils.isEmpty(m)) {
            m = bb.a(context).c();
        }
        return m;
    }

    public static int d(Context context) {
        if (n == 0) {
            n = bb.a(context).d();
        }
        return n;
    }

    public static String e(Context context) {
        return "6.1.1";
    }
}
