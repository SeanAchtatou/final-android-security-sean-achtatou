package com.umeng.a.a;

import java.io.Serializable;
import java.util.BitSet;
import java.util.Collections;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/* compiled from: IdJournal */
public class aj implements be<aj, e>, Serializable, Cloneable {
    public static final Map<e, bj> e;
    /* access modifiers changed from: private */
    public static final bz f = new bz("IdJournal");
    /* access modifiers changed from: private */
    public static final br g = new br("domain", (byte) 11, 1);
    /* access modifiers changed from: private */
    public static final br h = new br("old_id", (byte) 11, 2);
    /* access modifiers changed from: private */
    public static final br i = new br("new_id", (byte) 11, 3);
    /* access modifiers changed from: private */
    public static final br j = new br("ts", (byte) 10, 4);
    private static final Map<Class<? extends cb>, cc> k = new HashMap();

    /* renamed from: a  reason: collision with root package name */
    public String f2624a;
    public String b;
    public String c;
    public long d;
    private byte l = 0;
    private e[] m = {e.OLD_ID};

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [com.umeng.a.a.aj$e, com.umeng.a.a.bj]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        k.put(cd.class, new b());
        k.put(ce.class, new d());
        EnumMap enumMap = new EnumMap(e.class);
        enumMap.put((Object) e.DOMAIN, (Object) new bj("domain", (byte) 1, new bk((byte) 11)));
        enumMap.put((Object) e.OLD_ID, (Object) new bj("old_id", (byte) 2, new bk((byte) 11)));
        enumMap.put((Object) e.NEW_ID, (Object) new bj("new_id", (byte) 1, new bk((byte) 11)));
        enumMap.put((Object) e.TS, (Object) new bj("ts", (byte) 1, new bk((byte) 10)));
        e = Collections.unmodifiableMap(enumMap);
        bj.a(aj.class, e);
    }

    /* compiled from: IdJournal */
    public enum e {
        DOMAIN(1, "domain"),
        OLD_ID(2, "old_id"),
        NEW_ID(3, "new_id"),
        TS(4, "ts");
        
        private static final Map<String, e> e = new HashMap();
        private final short f;
        private final String g;

        static {
            Iterator it = EnumSet.allOf(e.class).iterator();
            while (it.hasNext()) {
                e eVar = (e) it.next();
                e.put(eVar.a(), eVar);
            }
        }

        private e(short s, String str) {
            this.f = s;
            this.g = str;
        }

        public String a() {
            return this.g;
        }
    }

    public aj a(String str) {
        this.f2624a = str;
        return this;
    }

    public void a(boolean z) {
        if (!z) {
            this.f2624a = null;
        }
    }

    public aj b(String str) {
        this.b = str;
        return this;
    }

    public boolean a() {
        return this.b != null;
    }

    public void b(boolean z) {
        if (!z) {
            this.b = null;
        }
    }

    public aj c(String str) {
        this.c = str;
        return this;
    }

    public void c(boolean z) {
        if (!z) {
            this.c = null;
        }
    }

    public aj a(long j2) {
        this.d = j2;
        d(true);
        return this;
    }

    public boolean b() {
        return bc.a(this.l, 0);
    }

    public void d(boolean z) {
        this.l = bc.a(this.l, 0, z);
    }

    public void a(bu buVar) throws bh {
        k.get(buVar.y()).b().b(buVar, this);
    }

    public void b(bu buVar) throws bh {
        k.get(buVar.y()).b().a(buVar, this);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("IdJournal(");
        sb.append("domain:");
        if (this.f2624a == null) {
            sb.append("null");
        } else {
            sb.append(this.f2624a);
        }
        if (a()) {
            sb.append(", ");
            sb.append("old_id:");
            if (this.b == null) {
                sb.append("null");
            } else {
                sb.append(this.b);
            }
        }
        sb.append(", ");
        sb.append("new_id:");
        if (this.c == null) {
            sb.append("null");
        } else {
            sb.append(this.c);
        }
        sb.append(", ");
        sb.append("ts:");
        sb.append(this.d);
        sb.append(")");
        return sb.toString();
    }

    public void c() throws bh {
        if (this.f2624a == null) {
            throw new bv("Required field 'domain' was not present! Struct: " + toString());
        } else if (this.c == null) {
            throw new bv("Required field 'new_id' was not present! Struct: " + toString());
        }
    }

    /* compiled from: IdJournal */
    private static class b implements cc {
        private b() {
        }

        /* renamed from: a */
        public a b() {
            return new a();
        }
    }

    /* compiled from: IdJournal */
    private static class a extends cd<aj> {
        private a() {
        }

        /* renamed from: a */
        public void b(bu buVar, aj ajVar) throws bh {
            buVar.f();
            while (true) {
                br h = buVar.h();
                if (h.b == 0) {
                    buVar.g();
                    if (!ajVar.b()) {
                        throw new bv("Required field 'ts' was not found in serialized data! Struct: " + toString());
                    }
                    ajVar.c();
                    return;
                }
                switch (h.c) {
                    case 1:
                        if (h.b != 11) {
                            bx.a(buVar, h.b);
                            break;
                        } else {
                            ajVar.f2624a = buVar.v();
                            ajVar.a(true);
                            break;
                        }
                    case 2:
                        if (h.b != 11) {
                            bx.a(buVar, h.b);
                            break;
                        } else {
                            ajVar.b = buVar.v();
                            ajVar.b(true);
                            break;
                        }
                    case 3:
                        if (h.b != 11) {
                            bx.a(buVar, h.b);
                            break;
                        } else {
                            ajVar.c = buVar.v();
                            ajVar.c(true);
                            break;
                        }
                    case 4:
                        if (h.b != 10) {
                            bx.a(buVar, h.b);
                            break;
                        } else {
                            ajVar.d = buVar.t();
                            ajVar.d(true);
                            break;
                        }
                    default:
                        bx.a(buVar, h.b);
                        break;
                }
                buVar.i();
            }
        }

        /* renamed from: b */
        public void a(bu buVar, aj ajVar) throws bh {
            ajVar.c();
            buVar.a(aj.f);
            if (ajVar.f2624a != null) {
                buVar.a(aj.g);
                buVar.a(ajVar.f2624a);
                buVar.b();
            }
            if (ajVar.b != null && ajVar.a()) {
                buVar.a(aj.h);
                buVar.a(ajVar.b);
                buVar.b();
            }
            if (ajVar.c != null) {
                buVar.a(aj.i);
                buVar.a(ajVar.c);
                buVar.b();
            }
            buVar.a(aj.j);
            buVar.a(ajVar.d);
            buVar.b();
            buVar.c();
            buVar.a();
        }
    }

    /* compiled from: IdJournal */
    private static class d implements cc {
        private d() {
        }

        /* renamed from: a */
        public c b() {
            return new c();
        }
    }

    /* compiled from: IdJournal */
    private static class c extends ce<aj> {
        private c() {
        }

        public void a(bu buVar, aj ajVar) throws bh {
            ca caVar = (ca) buVar;
            caVar.a(ajVar.f2624a);
            caVar.a(ajVar.c);
            caVar.a(ajVar.d);
            BitSet bitSet = new BitSet();
            if (ajVar.a()) {
                bitSet.set(0);
            }
            caVar.a(bitSet, 1);
            if (ajVar.a()) {
                caVar.a(ajVar.b);
            }
        }

        public void b(bu buVar, aj ajVar) throws bh {
            ca caVar = (ca) buVar;
            ajVar.f2624a = caVar.v();
            ajVar.a(true);
            ajVar.c = caVar.v();
            ajVar.c(true);
            ajVar.d = caVar.t();
            ajVar.d(true);
            if (caVar.b(1).get(0)) {
                ajVar.b = caVar.v();
                ajVar.b(true);
            }
        }
    }
}
