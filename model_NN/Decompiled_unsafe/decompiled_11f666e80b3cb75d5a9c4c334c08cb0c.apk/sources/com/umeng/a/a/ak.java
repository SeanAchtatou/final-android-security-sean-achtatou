package com.umeng.a.a;

import java.io.Serializable;
import java.util.Collections;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/* compiled from: IdSnapshot */
public class ak implements be<ak, e>, Serializable, Cloneable {
    public static final Map<e, bj> d;
    /* access modifiers changed from: private */
    public static final bz e = new bz("IdSnapshot");
    /* access modifiers changed from: private */
    public static final br f = new br("identity", (byte) 11, 1);
    /* access modifiers changed from: private */
    public static final br g = new br("ts", (byte) 10, 2);
    /* access modifiers changed from: private */
    public static final br h = new br("version", (byte) 8, 3);
    private static final Map<Class<? extends cb>, cc> i = new HashMap();

    /* renamed from: a  reason: collision with root package name */
    public String f2626a;
    public long b;
    public int c;
    private byte j = 0;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [com.umeng.a.a.ak$e, com.umeng.a.a.bj]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        i.put(cd.class, new b());
        i.put(ce.class, new d());
        EnumMap enumMap = new EnumMap(e.class);
        enumMap.put((Object) e.IDENTITY, (Object) new bj("identity", (byte) 1, new bk((byte) 11)));
        enumMap.put((Object) e.TS, (Object) new bj("ts", (byte) 1, new bk((byte) 10)));
        enumMap.put((Object) e.VERSION, (Object) new bj("version", (byte) 1, new bk((byte) 8)));
        d = Collections.unmodifiableMap(enumMap);
        bj.a(ak.class, d);
    }

    /* compiled from: IdSnapshot */
    public enum e {
        IDENTITY(1, "identity"),
        TS(2, "ts"),
        VERSION(3, "version");
        
        private static final Map<String, e> d = new HashMap();
        private final short e;
        private final String f;

        static {
            Iterator it = EnumSet.allOf(e.class).iterator();
            while (it.hasNext()) {
                e eVar = (e) it.next();
                d.put(eVar.a(), eVar);
            }
        }

        private e(short s, String str) {
            this.e = s;
            this.f = str;
        }

        public String a() {
            return this.f;
        }
    }

    public String a() {
        return this.f2626a;
    }

    public ak a(String str) {
        this.f2626a = str;
        return this;
    }

    public void a(boolean z) {
        if (!z) {
            this.f2626a = null;
        }
    }

    public long b() {
        return this.b;
    }

    public ak a(long j2) {
        this.b = j2;
        b(true);
        return this;
    }

    public boolean c() {
        return bc.a(this.j, 0);
    }

    public void b(boolean z) {
        this.j = bc.a(this.j, 0, z);
    }

    public int d() {
        return this.c;
    }

    public ak a(int i2) {
        this.c = i2;
        c(true);
        return this;
    }

    public boolean e() {
        return bc.a(this.j, 1);
    }

    public void c(boolean z) {
        this.j = bc.a(this.j, 1, z);
    }

    public void a(bu buVar) throws bh {
        i.get(buVar.y()).b().b(buVar, this);
    }

    public void b(bu buVar) throws bh {
        i.get(buVar.y()).b().a(buVar, this);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("IdSnapshot(");
        sb.append("identity:");
        if (this.f2626a == null) {
            sb.append("null");
        } else {
            sb.append(this.f2626a);
        }
        sb.append(", ");
        sb.append("ts:");
        sb.append(this.b);
        sb.append(", ");
        sb.append("version:");
        sb.append(this.c);
        sb.append(")");
        return sb.toString();
    }

    public void f() throws bh {
        if (this.f2626a == null) {
            throw new bv("Required field 'identity' was not present! Struct: " + toString());
        }
    }

    /* compiled from: IdSnapshot */
    private static class b implements cc {
        private b() {
        }

        /* renamed from: a */
        public a b() {
            return new a();
        }
    }

    /* compiled from: IdSnapshot */
    private static class a extends cd<ak> {
        private a() {
        }

        /* renamed from: a */
        public void b(bu buVar, ak akVar) throws bh {
            buVar.f();
            while (true) {
                br h = buVar.h();
                if (h.b == 0) {
                    buVar.g();
                    if (!akVar.c()) {
                        throw new bv("Required field 'ts' was not found in serialized data! Struct: " + toString());
                    } else if (!akVar.e()) {
                        throw new bv("Required field 'version' was not found in serialized data! Struct: " + toString());
                    } else {
                        akVar.f();
                        return;
                    }
                } else {
                    switch (h.c) {
                        case 1:
                            if (h.b != 11) {
                                bx.a(buVar, h.b);
                                break;
                            } else {
                                akVar.f2626a = buVar.v();
                                akVar.a(true);
                                break;
                            }
                        case 2:
                            if (h.b != 10) {
                                bx.a(buVar, h.b);
                                break;
                            } else {
                                akVar.b = buVar.t();
                                akVar.b(true);
                                break;
                            }
                        case 3:
                            if (h.b != 8) {
                                bx.a(buVar, h.b);
                                break;
                            } else {
                                akVar.c = buVar.s();
                                akVar.c(true);
                                break;
                            }
                        default:
                            bx.a(buVar, h.b);
                            break;
                    }
                    buVar.i();
                }
            }
        }

        /* renamed from: b */
        public void a(bu buVar, ak akVar) throws bh {
            akVar.f();
            buVar.a(ak.e);
            if (akVar.f2626a != null) {
                buVar.a(ak.f);
                buVar.a(akVar.f2626a);
                buVar.b();
            }
            buVar.a(ak.g);
            buVar.a(akVar.b);
            buVar.b();
            buVar.a(ak.h);
            buVar.a(akVar.c);
            buVar.b();
            buVar.c();
            buVar.a();
        }
    }

    /* compiled from: IdSnapshot */
    private static class d implements cc {
        private d() {
        }

        /* renamed from: a */
        public c b() {
            return new c();
        }
    }

    /* compiled from: IdSnapshot */
    private static class c extends ce<ak> {
        private c() {
        }

        public void a(bu buVar, ak akVar) throws bh {
            ca caVar = (ca) buVar;
            caVar.a(akVar.f2626a);
            caVar.a(akVar.b);
            caVar.a(akVar.c);
        }

        public void b(bu buVar, ak akVar) throws bh {
            ca caVar = (ca) buVar;
            akVar.f2626a = caVar.v();
            akVar.a(true);
            akVar.b = caVar.t();
            akVar.b(true);
            akVar.c = caVar.s();
            akVar.c(true);
        }
    }
}
