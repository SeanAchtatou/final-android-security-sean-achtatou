package com.umeng.a.a;

import android.content.Context;
import android.text.TextUtils;
import java.io.File;

/* compiled from: OldUMIDTracker */
public class j extends cy {

    /* renamed from: a  reason: collision with root package name */
    private Context f2722a;
    private String b = null;
    private String c = null;

    public j(Context context) {
        super("oldumid");
        this.f2722a = context;
    }

    public String a() {
        return this.b;
    }

    public boolean b() {
        return c();
    }

    public boolean c() {
        this.c = g.a(this.f2722a).b().e(null);
        if (!TextUtils.isEmpty(this.c)) {
            this.c = ar.c(this.c);
            String a2 = au.a(new File("/sdcard/Android/data/.um/sysid.dat"));
            String a3 = au.a(new File("/sdcard/Android/obj/.um/sysid.dat"));
            String a4 = au.a(new File("/data/local/tmp/.um/sysid.dat"));
            if (TextUtils.isEmpty(a2)) {
                l();
            } else if (!this.c.equals(a2)) {
                this.b = a2;
                return true;
            }
            if (TextUtils.isEmpty(a3)) {
                k();
            } else if (!this.c.equals(a3)) {
                this.b = a3;
                return true;
            }
            if (TextUtils.isEmpty(a4)) {
                j();
            } else if (!this.c.equals(a4)) {
                this.b = a4;
                return true;
            }
        }
        return false;
    }

    public void d() {
        try {
            l();
            k();
            j();
        } catch (Exception e) {
        }
    }

    private void j() {
        try {
            b("/data/local/tmp/.um");
            au.a(new File("/data/local/tmp/.um/sysid.dat"), this.c);
        } catch (Throwable th) {
        }
    }

    private void k() {
        try {
            b("/sdcard/Android/obj/.um");
            au.a(new File("/sdcard/Android/obj/.um/sysid.dat"), this.c);
        } catch (Throwable th) {
        }
    }

    private void l() {
        try {
            b("/sdcard/Android/data/.um");
            au.a(new File("/sdcard/Android/data/.um/sysid.dat"), this.c);
        } catch (Throwable th) {
        }
    }

    private void b(String str) {
        File file = new File(str);
        if (!file.exists()) {
            file.mkdirs();
        }
    }
}
