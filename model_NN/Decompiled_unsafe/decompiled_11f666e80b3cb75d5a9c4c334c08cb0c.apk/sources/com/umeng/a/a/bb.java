package com.umeng.a.a;

import android.content.Context;
import android.content.SharedPreferences;
import com.meizu.cloud.pushsdk.notification.model.TimeDisplaySetting;
import java.io.File;
import java.io.FilenameFilter;
import java.util.Locale;

/* compiled from: StoreHelper */
public final class bb {

    /* renamed from: a  reason: collision with root package name */
    private static bb f2657a = null;
    /* access modifiers changed from: private */
    public static Context b;
    private static String c;
    private a d;

    /* compiled from: StoreHelper */
    public interface b {
        void a(File file);

        boolean b(File file);

        void c(File file);
    }

    public bb(Context context) {
        this.d = new a(context);
    }

    public static synchronized bb a(Context context) {
        bb bbVar;
        synchronized (bb.class) {
            b = context.getApplicationContext();
            c = context.getPackageName();
            if (f2657a == null) {
                f2657a = new bb(context);
            }
            bbVar = f2657a;
        }
        return bbVar;
    }

    public String[] a() {
        SharedPreferences i = i();
        String string = i.getString("au_p", null);
        String string2 = i.getString("au_u", null);
        if (string == null || string2 == null) {
            return null;
        }
        return new String[]{string, string2};
    }

    public String b() {
        SharedPreferences a2 = aa.a(b);
        if (a2 != null) {
            return a2.getString(LogBuilder.KEY_APPKEY, null);
        }
        return null;
    }

    public String c() {
        SharedPreferences a2 = aa.a(b);
        if (a2 != null) {
            return a2.getString(TimeDisplaySetting.START_SHOW_TIME, null);
        }
        return null;
    }

    public int d() {
        SharedPreferences a2 = aa.a(b);
        if (a2 != null) {
            return a2.getInt("vt", 0);
        }
        return 0;
    }

    public void e() {
        b.deleteFile(j());
        b.deleteFile(k());
        cx.a(b).a(true, false);
        co.a(b).b(new ck() {
            public void a(Object obj, boolean z) {
                if (obj.equals("success")) {
                }
            }
        });
    }

    public void a(byte[] bArr) {
        this.d.a(bArr);
    }

    public boolean f() {
        return this.d.a();
    }

    public a g() {
        return this.d;
    }

    private SharedPreferences i() {
        return b.getSharedPreferences("mobclick_agent_user_" + c, 0);
    }

    private String j() {
        return "mobclick_agent_header_" + c;
    }

    private String k() {
        SharedPreferences a2 = aa.a(b);
        if (a2 == null) {
            return "mobclick_agent_cached_" + c + at.a(b);
        }
        int i = a2.getInt("versioncode", 0);
        int parseInt = Integer.parseInt(at.a(b));
        if (i == 0 || parseInt == i) {
            return "mobclick_agent_cached_" + c + at.a(b);
        }
        return "mobclick_agent_cached_" + c + i;
    }

    /* compiled from: StoreHelper */
    public static class a {

        /* renamed from: a  reason: collision with root package name */
        private final int f2659a;
        private File b;
        private FilenameFilter c;

        public a(Context context) {
            this(context, ".um");
        }

        public a(Context context, String str) {
            this.f2659a = 10;
            this.c = new FilenameFilter() {
                public boolean accept(File file, String str) {
                    return str.startsWith("um");
                }
            };
            this.b = new File(context.getFilesDir(), str);
            if (!this.b.exists() || !this.b.isDirectory()) {
                this.b.mkdir();
            }
        }

        public boolean a() {
            File[] listFiles = this.b.listFiles();
            if (listFiles == null || listFiles.length <= 0) {
                return false;
            }
            return true;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:18:0x0047, code lost:
            r2[r0].delete();
         */
        /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void a(com.umeng.a.a.bb.b r6) {
            /*
                r5 = this;
                r0 = 0
                java.io.File r1 = r5.b
                java.io.FilenameFilter r2 = r5.c
                java.io.File[] r2 = r1.listFiles(r2)
                if (r2 == 0) goto L_0x0029
                int r1 = r2.length
                r3 = 10
                if (r1 < r3) goto L_0x0029
                java.util.Arrays.sort(r2)
                int r1 = r2.length
                int r3 = r1 + -10
                com.umeng.a.a.bb$a$1 r1 = new com.umeng.a.a.bb$a$1
                r1.<init>(r3)
                com.umeng.a.a.ax.b(r1)
                r1 = r0
            L_0x001f:
                if (r1 >= r3) goto L_0x0029
                r4 = r2[r1]
                r4.delete()
                int r1 = r1 + 1
                goto L_0x001f
            L_0x0029:
                if (r2 == 0) goto L_0x0054
                int r1 = r2.length
                if (r1 <= 0) goto L_0x0054
                java.io.File r1 = r5.b
                r6.a(r1)
                int r1 = r2.length
            L_0x0034:
                if (r0 >= r1) goto L_0x004f
                r3 = r2[r0]     // Catch:{ Throwable -> 0x0046, all -> 0x004d }
                boolean r3 = r6.b(r3)     // Catch:{ Throwable -> 0x0046, all -> 0x004d }
                if (r3 == 0) goto L_0x0043
                r3 = r2[r0]
                r3.delete()
            L_0x0043:
                int r0 = r0 + 1
                goto L_0x0034
            L_0x0046:
                r3 = move-exception
                r3 = r2[r0]
                r3.delete()
                goto L_0x0043
            L_0x004d:
                r0 = move-exception
                throw r0
            L_0x004f:
                java.io.File r0 = r5.b
                r6.c(r0)
            L_0x0054:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.umeng.a.a.bb.a.a(com.umeng.a.a.bb$b):void");
        }

        public void a(byte[] bArr) {
            if (bArr != null && bArr.length != 0) {
                try {
                    au.a(new File(this.b, String.format(Locale.US, "um_cache_%d.env", Long.valueOf(System.currentTimeMillis()))), bArr);
                } catch (Exception e) {
                }
            }
        }
    }
}
