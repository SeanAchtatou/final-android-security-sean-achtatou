package com.umeng.a.a;

import android.content.Context;
import android.text.TextUtils;
import cn.banshenggua.aichang.room.message.SocketMessage;
import com.umeng.a.a.g;
import com.xiaomi.mipush.sdk.MiPushClient;

/* compiled from: ABTest */
public class ag implements y {
    private static ag i = null;

    /* renamed from: a  reason: collision with root package name */
    private boolean f2621a = false;
    private int b = -1;
    private int c = -1;
    private int d = -1;
    private float e = 0.0f;
    private float f = 0.0f;
    private String g = null;
    private Context h = null;

    public static synchronized ag a(Context context) {
        ag agVar;
        synchronized (ag.class) {
            if (i == null) {
                g.a b2 = g.a(context).b();
                i = new ag(context, b2.d((String) null), b2.d(0));
            }
            agVar = i;
        }
        return agVar;
    }

    private ag(Context context, String str, int i2) {
        this.h = context;
        a(str, i2);
    }

    private float b(String str, int i2) {
        int i3 = i2 * 2;
        if (str == null) {
            return 0.0f;
        }
        return ((float) Integer.valueOf(str.substring(i3, i3 + 5), 16).intValue()) / 1048576.0f;
    }

    public void a(String str, int i2) {
        this.c = i2;
        String a2 = b.a(this.h);
        if (TextUtils.isEmpty(a2) || TextUtils.isEmpty(str)) {
            this.f2621a = false;
            return;
        }
        try {
            this.e = b(a2, 12);
            this.f = b(a2, 6);
            if (str.startsWith("SIG7")) {
                b(str);
            } else if (str.startsWith("FIXED")) {
                c(str);
            }
        } catch (Exception e2) {
            this.f2621a = false;
            aw.a("v:" + str, e2);
        }
    }

    public static boolean a(String str) {
        int parseInt;
        if (TextUtils.isEmpty(str)) {
            return false;
        }
        String[] split = str.split("\\|");
        if (split.length != 6) {
            return false;
        }
        if (split[0].startsWith("SIG7") && split[1].split(MiPushClient.ACCEPT_TIME_SEPARATOR).length == split[5].split(MiPushClient.ACCEPT_TIME_SEPARATOR).length) {
            return true;
        }
        if (!split[0].startsWith("FIXED") || split[5].split(MiPushClient.ACCEPT_TIME_SEPARATOR).length < (parseInt = Integer.parseInt(split[1])) || parseInt < 1) {
            return false;
        }
        return true;
    }

    private void b(String str) {
        float f2;
        if (str != null) {
            String[] split = str.split("\\|");
            if (split[2].equals("SIG13")) {
                f2 = Float.valueOf(split[3]).floatValue();
            } else {
                f2 = 0.0f;
            }
            if (this.e > f2) {
                this.f2621a = false;
                return;
            }
            float[] fArr = null;
            if (split[0].equals("SIG7")) {
                String[] split2 = split[1].split(MiPushClient.ACCEPT_TIME_SEPARATOR);
                float[] fArr2 = new float[split2.length];
                for (int i2 = 0; i2 < split2.length; i2++) {
                    fArr2[i2] = Float.valueOf(split2[i2]).floatValue();
                }
                fArr = fArr2;
            }
            int[] iArr = null;
            if (split[4].equals("RPT")) {
                this.g = "RPT";
                String[] split3 = split[5].split(MiPushClient.ACCEPT_TIME_SEPARATOR);
                int[] iArr2 = new int[split3.length];
                for (int i3 = 0; i3 < split3.length; i3++) {
                    iArr2[i3] = Integer.valueOf(split3[i3]).intValue();
                }
                iArr = iArr2;
            } else if (split[4].equals("DOM")) {
                this.f2621a = true;
                this.g = "DOM";
                try {
                    String[] split4 = split[5].split(MiPushClient.ACCEPT_TIME_SEPARATOR);
                    iArr = new int[split4.length];
                    for (int i4 = 0; i4 < split4.length; i4++) {
                        iArr[i4] = Integer.valueOf(split4[i4]).intValue();
                    }
                } catch (Exception e2) {
                }
            }
            float f3 = 0.0f;
            int i5 = 0;
            while (true) {
                if (i5 >= fArr.length) {
                    i5 = -1;
                    break;
                }
                f3 += fArr[i5];
                if (this.f < f3) {
                    break;
                }
                i5++;
            }
            if (i5 != -1) {
                this.f2621a = true;
                this.d = i5 + 1;
                if (iArr != null) {
                    this.b = iArr[i5];
                    return;
                }
                return;
            }
            this.f2621a = false;
        }
    }

    private void c(String str) {
        int i2;
        if (str != null) {
            String[] split = str.split("\\|");
            float f2 = 0.0f;
            if (split[2].equals("SIG13")) {
                f2 = Float.valueOf(split[3]).floatValue();
            }
            if (this.e > f2) {
                this.f2621a = false;
                return;
            }
            if (split[0].equals("FIXED")) {
                i2 = Integer.valueOf(split[1]).intValue();
            } else {
                i2 = -1;
            }
            int[] iArr = null;
            if (split[4].equals("RPT")) {
                this.g = "RPT";
                String[] split2 = split[5].split(MiPushClient.ACCEPT_TIME_SEPARATOR);
                int[] iArr2 = new int[split2.length];
                for (int i3 = 0; i3 < split2.length; i3++) {
                    iArr2[i3] = Integer.valueOf(split2[i3]).intValue();
                }
                iArr = iArr2;
            } else if (split[4].equals("DOM")) {
                this.g = "DOM";
                this.f2621a = true;
                try {
                    String[] split3 = split[5].split(MiPushClient.ACCEPT_TIME_SEPARATOR);
                    iArr = new int[split3.length];
                    for (int i4 = 0; i4 < split3.length; i4++) {
                        iArr[i4] = Integer.valueOf(split3[i4]).intValue();
                    }
                } catch (Exception e2) {
                }
            }
            if (i2 != -1) {
                this.f2621a = true;
                this.d = i2;
                if (iArr != null) {
                    this.b = iArr[i2 - 1];
                    return;
                }
                return;
            }
            this.f2621a = false;
        }
    }

    public boolean a() {
        return this.f2621a;
    }

    public int b() {
        return this.b;
    }

    public String c() {
        if (!this.f2621a) {
            return SocketMessage.MSG_ERROR_KEY;
        }
        return String.valueOf(this.d);
    }

    public String d() {
        return this.g;
    }

    public void a(g.a aVar) {
        a(aVar.d((String) null), aVar.d(0));
    }

    public String toString() {
        return " p13:" + this.e + " p07:" + this.f + " policy:" + this.b + " interval:" + this.c;
    }
}
