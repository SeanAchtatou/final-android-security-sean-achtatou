package com.umeng.a.a;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/* compiled from: UMCCAggregatedObject */
public class cm implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    private List<String> f2686a = new ArrayList();
    private List<String> b = new ArrayList();
    private long c = 0;
    private long d = 0;
    private long e = 0;
    private String f = null;

    public void a(String str) {
        try {
            if (this.b.size() < cp.a().b()) {
                this.b.add(str);
            } else {
                this.b.remove(this.b.get(0));
                this.b.add(str);
            }
            if (this.b.size() > cp.a().b()) {
                for (int i = 0; i < this.b.size() - cp.a().b(); i++) {
                    this.b.remove(this.b.get(0));
                }
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public String toString() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("[key: ").append(this.f2686a).append("] [label: ").append(this.b).append("][ totalTimeStamp").append(this.f).append("][ value").append(this.d).append("][ count").append(this.e).append("][ timeWindowNum").append(this.f).append("]");
        return stringBuffer.toString();
    }

    public String a() {
        return bq.a(this.f2686a);
    }

    public String b() {
        return bq.a(this.b);
    }

    public List<String> c() {
        return this.b;
    }

    public long d() {
        return this.c;
    }

    public long e() {
        return this.d;
    }

    public long f() {
        return this.e;
    }

    public String g() {
        return this.f;
    }

    public void a(List<String> list) {
        this.f2686a = list;
    }

    public void b(List<String> list) {
        this.b = list;
    }

    public void a(long j) {
        this.c = j;
    }

    public void b(long j) {
        this.d = j;
    }

    public void c(long j) {
        this.e = j;
    }

    public void b(String str) {
        this.f = str;
    }
}
