package com.umeng.a.a;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/* compiled from: UMCCDBHelper */
class ay extends SQLiteOpenHelper {
    /* access modifiers changed from: private */
    public static Context b;

    /* renamed from: a  reason: collision with root package name */
    private String f2647a;

    /* compiled from: UMCCDBHelper */
    private static class a {
        /* access modifiers changed from: private */

        /* renamed from: a  reason: collision with root package name */
        public static final ay f2648a = new ay(ay.b, bq.a(ay.b), "cc.db", null, 1);
    }

    public static synchronized ay a(Context context) {
        ay a2;
        synchronized (ay.class) {
            b = context;
            a2 = a.f2648a;
        }
        return a2;
    }

    private ay(Context context, String str, String str2, SQLiteDatabase.CursorFactory cursorFactory, int i) {
        this(new cj(context, str), str2, cursorFactory, i);
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private ay(android.content.Context r2, java.lang.String r3, android.database.sqlite.SQLiteDatabase.CursorFactory r4, int r5) {
        /*
            r1 = this;
            if (r3 == 0) goto L_0x000a
            java.lang.String r0 = ""
            boolean r0 = r3.equals(r0)
            if (r0 == 0) goto L_0x000c
        L_0x000a:
            java.lang.String r3 = "cc.db"
        L_0x000c:
            r1.<init>(r2, r3, r4, r5)
            r1.b()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.umeng.a.a.ay.<init>(android.content.Context, java.lang.String, android.database.sqlite.SQLiteDatabase$CursorFactory, int):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:?, code lost:
        return;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void b() {
        /*
            r2 = this;
            android.database.sqlite.SQLiteDatabase r0 = r2.getWritableDatabase()     // Catch:{ Exception -> 0x0030, all -> 0x002e }
            java.lang.String r1 = "aggregated"
            boolean r1 = r2.a(r1, r0)     // Catch:{ Exception -> 0x0030, all -> 0x002e }
            if (r1 == 0) goto L_0x0014
            java.lang.String r1 = "aggregated_cache"
            boolean r1 = r2.a(r1, r0)     // Catch:{ Exception -> 0x0030, all -> 0x002e }
            if (r1 != 0) goto L_0x0017
        L_0x0014:
            r2.c(r0)     // Catch:{ Exception -> 0x0030, all -> 0x002e }
        L_0x0017:
            java.lang.String r1 = "system"
            boolean r1 = r2.a(r1, r0)     // Catch:{ Exception -> 0x0030, all -> 0x002e }
            if (r1 != 0) goto L_0x0022
            r2.b(r0)     // Catch:{ Exception -> 0x0030, all -> 0x002e }
        L_0x0022:
            java.lang.String r1 = "limitedck"
            boolean r1 = r2.a(r1, r0)     // Catch:{ Exception -> 0x0030, all -> 0x002e }
            if (r1 != 0) goto L_0x002d
            r2.a(r0)     // Catch:{ Exception -> 0x0030, all -> 0x002e }
        L_0x002d:
            return
        L_0x002e:
            r0 = move-exception
            throw r0
        L_0x0030:
            r0 = move-exception
            goto L_0x002d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.umeng.a.a.ay.b():void");
    }

    public boolean a(String str, SQLiteDatabase sQLiteDatabase) {
        Cursor cursor = null;
        boolean z = false;
        if (str != null) {
            try {
                Cursor rawQuery = sQLiteDatabase.rawQuery("select count(*) as c from sqlite_master where type ='table' and name ='" + str.trim() + "' ", null);
                if (rawQuery.moveToNext() && rawQuery.getInt(0) > 0) {
                    z = true;
                }
                if (rawQuery != null) {
                    rawQuery.close();
                }
            } catch (Exception e) {
                if (cursor != null) {
                    cursor.close();
                }
            } catch (Throwable th) {
                if (cursor != null) {
                    cursor.close();
                }
                throw th;
            }
        }
        return z;
    }

    public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
    }

    public void onCreate(SQLiteDatabase sQLiteDatabase) {
        try {
            sQLiteDatabase.beginTransaction();
            c(sQLiteDatabase);
            b(sQLiteDatabase);
            a(sQLiteDatabase);
            sQLiteDatabase.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            sQLiteDatabase.endTransaction();
        }
    }

    private boolean a(SQLiteDatabase sQLiteDatabase) {
        try {
            this.f2647a = "create table if not exists limitedck(Id INTEGER primary key autoincrement, ck TEXT unique)";
            sQLiteDatabase.execSQL(this.f2647a);
            return true;
        } catch (SQLException e) {
            aw.c("create reference table error!");
            return false;
        }
    }

    private boolean b(SQLiteDatabase sQLiteDatabase) {
        try {
            this.f2647a = "create table if not exists system(Id INTEGER primary key autoincrement, key TEXT, timeStamp INTEGER, count INTEGER)";
            sQLiteDatabase.execSQL(this.f2647a);
            return true;
        } catch (SQLException e) {
            aw.c("create system table error!");
            return false;
        }
    }

    private boolean c(SQLiteDatabase sQLiteDatabase) {
        try {
            this.f2647a = "create table if not exists aggregated_cache(Id INTEGER primary key autoincrement, key TEXT, totalTimestamp TEXT, value INTEGER, count INTEGER, label TEXT, timeWindowNum TEXT)";
            sQLiteDatabase.execSQL(this.f2647a);
            this.f2647a = "create table if not exists aggregated(Id INTEGER primary key autoincrement, key TEXT, totalTimestamp TEXT, value INTEGER, count INTEGER, label TEXT, timeWindowNum TEXT)";
            sQLiteDatabase.execSQL(this.f2647a);
            return true;
        } catch (SQLException e) {
            aw.c("create aggregated table error!");
            return false;
        }
    }
}
