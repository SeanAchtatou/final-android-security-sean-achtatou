package com.umeng.a.a;

/* compiled from: SafeRunnable */
public abstract class ba implements Runnable {
    public abstract void a();

    public void run() {
        try {
            a();
        } catch (Throwable th) {
            if (th != null) {
                th.printStackTrace();
            }
        }
    }
}
