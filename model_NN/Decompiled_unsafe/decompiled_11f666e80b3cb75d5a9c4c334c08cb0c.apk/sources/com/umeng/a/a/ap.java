package com.umeng.a.a;

import com.igexin.download.Downloads;
import com.tencent.open.GameAppOperation;
import java.io.Serializable;
import java.nio.ByteBuffer;
import java.util.BitSet;
import java.util.Collections;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/* compiled from: UMEnvelope */
public class ap implements be<ap, e>, Serializable, Cloneable {
    public static final Map<e, bj> k;
    /* access modifiers changed from: private */
    public static final bz l = new bz("UMEnvelope");
    /* access modifiers changed from: private */
    public static final br m = new br("version", (byte) 11, 1);
    /* access modifiers changed from: private */
    public static final br n = new br("address", (byte) 11, 2);
    /* access modifiers changed from: private */
    public static final br o = new br(GameAppOperation.GAME_SIGNATURE, (byte) 11, 3);
    /* access modifiers changed from: private */
    public static final br p = new br("serial_num", (byte) 8, 4);
    /* access modifiers changed from: private */
    public static final br q = new br("ts_secs", (byte) 8, 5);
    /* access modifiers changed from: private */
    public static final br r = new br("length", (byte) 8, 6);
    /* access modifiers changed from: private */
    public static final br s = new br(Downloads.COLUMN_APP_DATA, (byte) 11, 7);
    /* access modifiers changed from: private */
    public static final br t = new br("guid", (byte) 11, 8);
    /* access modifiers changed from: private */
    public static final br u = new br("checksum", (byte) 11, 9);
    /* access modifiers changed from: private */
    public static final br v = new br("codex", (byte) 8, 10);
    private static final Map<Class<? extends cb>, cc> w = new HashMap();

    /* renamed from: a  reason: collision with root package name */
    public String f2636a;
    public String b;
    public String c;
    public int d;
    public int e;
    public int f;
    public ByteBuffer g;
    public String h;
    public String i;
    public int j;
    private byte x = 0;
    private e[] y = {e.CODEX};

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [com.umeng.a.a.ap$e, com.umeng.a.a.bj]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        w.put(cd.class, new b());
        w.put(ce.class, new d());
        EnumMap enumMap = new EnumMap(e.class);
        enumMap.put((Object) e.VERSION, (Object) new bj("version", (byte) 1, new bk((byte) 11)));
        enumMap.put((Object) e.ADDRESS, (Object) new bj("address", (byte) 1, new bk((byte) 11)));
        enumMap.put((Object) e.SIGNATURE, (Object) new bj(GameAppOperation.GAME_SIGNATURE, (byte) 1, new bk((byte) 11)));
        enumMap.put((Object) e.SERIAL_NUM, (Object) new bj("serial_num", (byte) 1, new bk((byte) 8)));
        enumMap.put((Object) e.TS_SECS, (Object) new bj("ts_secs", (byte) 1, new bk((byte) 8)));
        enumMap.put((Object) e.LENGTH, (Object) new bj("length", (byte) 1, new bk((byte) 8)));
        enumMap.put((Object) e.ENTITY, (Object) new bj(Downloads.COLUMN_APP_DATA, (byte) 1, new bk((byte) 11, true)));
        enumMap.put((Object) e.h, (Object) new bj("guid", (byte) 1, new bk((byte) 11)));
        enumMap.put((Object) e.CHECKSUM, (Object) new bj("checksum", (byte) 1, new bk((byte) 11)));
        enumMap.put((Object) e.CODEX, (Object) new bj("codex", (byte) 2, new bk((byte) 8)));
        k = Collections.unmodifiableMap(enumMap);
        bj.a(ap.class, k);
    }

    /* compiled from: UMEnvelope */
    public enum e {
        VERSION(1, "version"),
        ADDRESS(2, "address"),
        SIGNATURE(3, GameAppOperation.GAME_SIGNATURE),
        SERIAL_NUM(4, "serial_num"),
        TS_SECS(5, "ts_secs"),
        LENGTH(6, "length"),
        ENTITY(7, Downloads.COLUMN_APP_DATA),
        h(8, "guid"),
        CHECKSUM(9, "checksum"),
        CODEX(10, "codex");
        
        private static final Map<String, e> k = new HashMap();
        private final short l;
        private final String m;

        static {
            Iterator it = EnumSet.allOf(e.class).iterator();
            while (it.hasNext()) {
                e eVar = (e) it.next();
                k.put(eVar.a(), eVar);
            }
        }

        private e(short s, String str) {
            this.l = s;
            this.m = str;
        }

        public String a() {
            return this.m;
        }
    }

    public ap a(String str) {
        this.f2636a = str;
        return this;
    }

    public void a(boolean z) {
        if (!z) {
            this.f2636a = null;
        }
    }

    public ap b(String str) {
        this.b = str;
        return this;
    }

    public void b(boolean z) {
        if (!z) {
            this.b = null;
        }
    }

    public ap c(String str) {
        this.c = str;
        return this;
    }

    public void c(boolean z) {
        if (!z) {
            this.c = null;
        }
    }

    public ap a(int i2) {
        this.d = i2;
        d(true);
        return this;
    }

    public boolean a() {
        return bc.a(this.x, 0);
    }

    public void d(boolean z) {
        this.x = bc.a(this.x, 0, z);
    }

    public ap b(int i2) {
        this.e = i2;
        e(true);
        return this;
    }

    public boolean b() {
        return bc.a(this.x, 1);
    }

    public void e(boolean z) {
        this.x = bc.a(this.x, 1, z);
    }

    public ap c(int i2) {
        this.f = i2;
        f(true);
        return this;
    }

    public boolean c() {
        return bc.a(this.x, 2);
    }

    public void f(boolean z) {
        this.x = bc.a(this.x, 2, z);
    }

    public ap a(byte[] bArr) {
        a(bArr == null ? null : ByteBuffer.wrap(bArr));
        return this;
    }

    public ap a(ByteBuffer byteBuffer) {
        this.g = byteBuffer;
        return this;
    }

    public void g(boolean z) {
        if (!z) {
            this.g = null;
        }
    }

    public ap d(String str) {
        this.h = str;
        return this;
    }

    public void h(boolean z) {
        if (!z) {
            this.h = null;
        }
    }

    public ap e(String str) {
        this.i = str;
        return this;
    }

    public void i(boolean z) {
        if (!z) {
            this.i = null;
        }
    }

    public ap d(int i2) {
        this.j = i2;
        j(true);
        return this;
    }

    public boolean d() {
        return bc.a(this.x, 3);
    }

    public void j(boolean z) {
        this.x = bc.a(this.x, 3, z);
    }

    public void a(bu buVar) throws bh {
        w.get(buVar.y()).b().b(buVar, this);
    }

    public void b(bu buVar) throws bh {
        w.get(buVar.y()).b().a(buVar, this);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("UMEnvelope(");
        sb.append("version:");
        if (this.f2636a == null) {
            sb.append("null");
        } else {
            sb.append(this.f2636a);
        }
        sb.append(", ");
        sb.append("address:");
        if (this.b == null) {
            sb.append("null");
        } else {
            sb.append(this.b);
        }
        sb.append(", ");
        sb.append("signature:");
        if (this.c == null) {
            sb.append("null");
        } else {
            sb.append(this.c);
        }
        sb.append(", ");
        sb.append("serial_num:");
        sb.append(this.d);
        sb.append(", ");
        sb.append("ts_secs:");
        sb.append(this.e);
        sb.append(", ");
        sb.append("length:");
        sb.append(this.f);
        sb.append(", ");
        sb.append("entity:");
        if (this.g == null) {
            sb.append("null");
        } else {
            bf.a(this.g, sb);
        }
        sb.append(", ");
        sb.append("guid:");
        if (this.h == null) {
            sb.append("null");
        } else {
            sb.append(this.h);
        }
        sb.append(", ");
        sb.append("checksum:");
        if (this.i == null) {
            sb.append("null");
        } else {
            sb.append(this.i);
        }
        if (d()) {
            sb.append(", ");
            sb.append("codex:");
            sb.append(this.j);
        }
        sb.append(")");
        return sb.toString();
    }

    public void e() throws bh {
        if (this.f2636a == null) {
            throw new bv("Required field 'version' was not present! Struct: " + toString());
        } else if (this.b == null) {
            throw new bv("Required field 'address' was not present! Struct: " + toString());
        } else if (this.c == null) {
            throw new bv("Required field 'signature' was not present! Struct: " + toString());
        } else if (this.g == null) {
            throw new bv("Required field 'entity' was not present! Struct: " + toString());
        } else if (this.h == null) {
            throw new bv("Required field 'guid' was not present! Struct: " + toString());
        } else if (this.i == null) {
            throw new bv("Required field 'checksum' was not present! Struct: " + toString());
        }
    }

    /* compiled from: UMEnvelope */
    private static class b implements cc {
        private b() {
        }

        /* renamed from: a */
        public a b() {
            return new a();
        }
    }

    /* compiled from: UMEnvelope */
    private static class a extends cd<ap> {
        private a() {
        }

        /* renamed from: a */
        public void b(bu buVar, ap apVar) throws bh {
            buVar.f();
            while (true) {
                br h = buVar.h();
                if (h.b == 0) {
                    buVar.g();
                    if (!apVar.a()) {
                        throw new bv("Required field 'serial_num' was not found in serialized data! Struct: " + toString());
                    } else if (!apVar.b()) {
                        throw new bv("Required field 'ts_secs' was not found in serialized data! Struct: " + toString());
                    } else if (!apVar.c()) {
                        throw new bv("Required field 'length' was not found in serialized data! Struct: " + toString());
                    } else {
                        apVar.e();
                        return;
                    }
                } else {
                    switch (h.c) {
                        case 1:
                            if (h.b != 11) {
                                bx.a(buVar, h.b);
                                break;
                            } else {
                                apVar.f2636a = buVar.v();
                                apVar.a(true);
                                break;
                            }
                        case 2:
                            if (h.b != 11) {
                                bx.a(buVar, h.b);
                                break;
                            } else {
                                apVar.b = buVar.v();
                                apVar.b(true);
                                break;
                            }
                        case 3:
                            if (h.b != 11) {
                                bx.a(buVar, h.b);
                                break;
                            } else {
                                apVar.c = buVar.v();
                                apVar.c(true);
                                break;
                            }
                        case 4:
                            if (h.b != 8) {
                                bx.a(buVar, h.b);
                                break;
                            } else {
                                apVar.d = buVar.s();
                                apVar.d(true);
                                break;
                            }
                        case 5:
                            if (h.b != 8) {
                                bx.a(buVar, h.b);
                                break;
                            } else {
                                apVar.e = buVar.s();
                                apVar.e(true);
                                break;
                            }
                        case 6:
                            if (h.b != 8) {
                                bx.a(buVar, h.b);
                                break;
                            } else {
                                apVar.f = buVar.s();
                                apVar.f(true);
                                break;
                            }
                        case 7:
                            if (h.b != 11) {
                                bx.a(buVar, h.b);
                                break;
                            } else {
                                apVar.g = buVar.w();
                                apVar.g(true);
                                break;
                            }
                        case 8:
                            if (h.b != 11) {
                                bx.a(buVar, h.b);
                                break;
                            } else {
                                apVar.h = buVar.v();
                                apVar.h(true);
                                break;
                            }
                        case 9:
                            if (h.b != 11) {
                                bx.a(buVar, h.b);
                                break;
                            } else {
                                apVar.i = buVar.v();
                                apVar.i(true);
                                break;
                            }
                        case 10:
                            if (h.b != 8) {
                                bx.a(buVar, h.b);
                                break;
                            } else {
                                apVar.j = buVar.s();
                                apVar.j(true);
                                break;
                            }
                        default:
                            bx.a(buVar, h.b);
                            break;
                    }
                    buVar.i();
                }
            }
        }

        /* renamed from: b */
        public void a(bu buVar, ap apVar) throws bh {
            apVar.e();
            buVar.a(ap.l);
            if (apVar.f2636a != null) {
                buVar.a(ap.m);
                buVar.a(apVar.f2636a);
                buVar.b();
            }
            if (apVar.b != null) {
                buVar.a(ap.n);
                buVar.a(apVar.b);
                buVar.b();
            }
            if (apVar.c != null) {
                buVar.a(ap.o);
                buVar.a(apVar.c);
                buVar.b();
            }
            buVar.a(ap.p);
            buVar.a(apVar.d);
            buVar.b();
            buVar.a(ap.q);
            buVar.a(apVar.e);
            buVar.b();
            buVar.a(ap.r);
            buVar.a(apVar.f);
            buVar.b();
            if (apVar.g != null) {
                buVar.a(ap.s);
                buVar.a(apVar.g);
                buVar.b();
            }
            if (apVar.h != null) {
                buVar.a(ap.t);
                buVar.a(apVar.h);
                buVar.b();
            }
            if (apVar.i != null) {
                buVar.a(ap.u);
                buVar.a(apVar.i);
                buVar.b();
            }
            if (apVar.d()) {
                buVar.a(ap.v);
                buVar.a(apVar.j);
                buVar.b();
            }
            buVar.c();
            buVar.a();
        }
    }

    /* compiled from: UMEnvelope */
    private static class d implements cc {
        private d() {
        }

        /* renamed from: a */
        public c b() {
            return new c();
        }
    }

    /* compiled from: UMEnvelope */
    private static class c extends ce<ap> {
        private c() {
        }

        public void a(bu buVar, ap apVar) throws bh {
            ca caVar = (ca) buVar;
            caVar.a(apVar.f2636a);
            caVar.a(apVar.b);
            caVar.a(apVar.c);
            caVar.a(apVar.d);
            caVar.a(apVar.e);
            caVar.a(apVar.f);
            caVar.a(apVar.g);
            caVar.a(apVar.h);
            caVar.a(apVar.i);
            BitSet bitSet = new BitSet();
            if (apVar.d()) {
                bitSet.set(0);
            }
            caVar.a(bitSet, 1);
            if (apVar.d()) {
                caVar.a(apVar.j);
            }
        }

        public void b(bu buVar, ap apVar) throws bh {
            ca caVar = (ca) buVar;
            apVar.f2636a = caVar.v();
            apVar.a(true);
            apVar.b = caVar.v();
            apVar.b(true);
            apVar.c = caVar.v();
            apVar.c(true);
            apVar.d = caVar.s();
            apVar.d(true);
            apVar.e = caVar.s();
            apVar.e(true);
            apVar.f = caVar.s();
            apVar.f(true);
            apVar.g = caVar.w();
            apVar.g(true);
            apVar.h = caVar.v();
            apVar.h(true);
            apVar.i = caVar.v();
            apVar.i(true);
            if (caVar.b(1).get(0)) {
                apVar.j = caVar.s();
                apVar.j(true);
            }
        }
    }
}
