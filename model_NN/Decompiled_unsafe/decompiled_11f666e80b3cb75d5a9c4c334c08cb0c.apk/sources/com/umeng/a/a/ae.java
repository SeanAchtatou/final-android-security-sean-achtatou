package com.umeng.a.a;

import android.content.Context;
import android.content.SharedPreferences;

/* compiled from: StatTracer */
public class ae implements v {

    /* renamed from: a  reason: collision with root package name */
    public int f2619a;
    public int b;
    public long c;
    private final int d = 3600000;
    private int e;
    private long f = 0;
    private long g = 0;
    private Context h;

    public ae(Context context) {
        a(context);
    }

    private void a(Context context) {
        this.h = context.getApplicationContext();
        SharedPreferences a2 = aa.a(context);
        this.f2619a = a2.getInt("successful_request", 0);
        this.b = a2.getInt("failed_requests ", 0);
        this.e = a2.getInt("last_request_spent_ms", 0);
        this.c = a2.getLong("last_request_time", 0);
        this.f = a2.getLong("last_req", 0);
    }

    public boolean e() {
        boolean z;
        boolean z2;
        if (this.c == 0) {
            z = true;
        } else {
            z = false;
        }
        if (!bb.a(this.h).f()) {
            z2 = true;
        } else {
            z2 = false;
        }
        if (!z || !z2) {
            return false;
        }
        return true;
    }

    public void f() {
        this.f2619a++;
        this.c = this.f;
    }

    public void g() {
        this.b++;
    }

    public void h() {
        this.f = System.currentTimeMillis();
    }

    public void i() {
        this.e = (int) (System.currentTimeMillis() - this.f);
    }

    public void j() {
        aa.a(this.h).edit().putInt("successful_request", this.f2619a).putInt("failed_requests ", this.b).putInt("last_request_spent_ms", this.e).putLong("last_request_time", this.c).putLong("last_req", this.f).commit();
    }

    public long k() {
        SharedPreferences a2 = aa.a(this.h);
        this.g = aa.a(this.h).getLong("first_activate_time", 0);
        if (this.g == 0) {
            this.g = System.currentTimeMillis();
            a2.edit().putLong("first_activate_time", this.g).commit();
        }
        return this.g;
    }

    public long l() {
        return this.f;
    }

    public void a() {
        h();
    }

    public void b() {
        i();
    }

    public void c() {
        f();
    }

    public void d() {
        g();
    }
}
