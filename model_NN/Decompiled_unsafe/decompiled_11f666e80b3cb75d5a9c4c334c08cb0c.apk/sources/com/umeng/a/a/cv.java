package com.umeng.a.a;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import java.util.concurrent.atomic.AtomicInteger;

/* compiled from: UMDBManager */
class cv {
    private static cv c;
    private static SQLiteOpenHelper d;

    /* renamed from: a  reason: collision with root package name */
    private AtomicInteger f2708a = new AtomicInteger();
    private AtomicInteger b = new AtomicInteger();
    private SQLiteDatabase e;

    cv() {
    }

    private static synchronized void b(Context context) {
        synchronized (cv.class) {
            if (c == null) {
                c = new cv();
                d = cu.a(context);
            }
        }
    }

    public static synchronized cv a(Context context) {
        cv cvVar;
        synchronized (cv.class) {
            if (c == null) {
                b(context);
            }
            cvVar = c;
        }
        return cvVar;
    }

    public synchronized SQLiteDatabase a() {
        if (this.f2708a.incrementAndGet() == 1) {
            this.e = d.getWritableDatabase();
        }
        return this.e;
    }

    public synchronized void b() {
        if (this.f2708a.decrementAndGet() == 0) {
            this.e.close();
        }
        if (this.b.decrementAndGet() == 0) {
            this.e.close();
        }
    }
}
