package com.umeng.a.a;

import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import org.apache.http.conn.ssl.SSLSocketFactory;

/* compiled from: NetworkHelper */
class ab extends SSLSocketFactory {

    /* renamed from: a  reason: collision with root package name */
    SSLContext f2615a = SSLContext.getInstance("TLS");

    public ab(KeyStore keyStore) throws NoSuchAlgorithmException, KeyManagementException, KeyStoreException, UnrecoverableKeyException {
        super(keyStore);
        try {
            AnonymousClass1 r0 = new o() {
            };
            this.f2615a.init(null, new TrustManager[]{r0}, null);
        } catch (Throwable th) {
        }
    }

    public Socket createSocket(Socket socket, String str, int i, boolean z) throws IOException, UnknownHostException {
        return this.f2615a.getSocketFactory().createSocket(socket, str, i, z);
    }

    public Socket createSocket() throws IOException {
        return this.f2615a.getSocketFactory().createSocket();
    }
}
