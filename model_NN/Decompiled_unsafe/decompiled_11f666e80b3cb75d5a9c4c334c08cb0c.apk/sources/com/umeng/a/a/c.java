package com.umeng.a.a;

import android.content.Context;

/* compiled from: IDFATracker */
public class c extends cy {

    /* renamed from: a  reason: collision with root package name */
    private Context f2679a;

    public c(Context context) {
        super("idfa");
        this.f2679a = context;
    }

    public String a() {
        String a2 = aq.a(this.f2679a);
        return a2 == null ? "" : a2;
    }
}
