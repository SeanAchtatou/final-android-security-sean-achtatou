package com.umeng.a.a;

import android.content.Context;
import android.text.TextUtils;
import com.xiaomi.mipush.sdk.MiPushClient;
import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/* compiled from: IdTracker */
public class e {

    /* renamed from: a  reason: collision with root package name */
    public static e f2715a;
    private final String b = "umeng_it.cache";
    private File c;
    private al d = null;
    private long e;
    private long f;
    private Set<cy> g = new HashSet();
    private a h = null;

    e(Context context) {
        this.c = new File(context.getFilesDir(), "umeng_it.cache");
        this.f = LogBuilder.MAX_INTERVAL;
        this.h = new a(context);
        this.h.b();
    }

    public static synchronized e a(Context context) {
        e eVar;
        synchronized (e.class) {
            if (f2715a == null) {
                f2715a = new e(context);
                f2715a.a(new f(context));
                f2715a.a(new cz(context));
                f2715a.a(new m(context));
                f2715a.a(new d(context));
                f2715a.a(new c(context));
                f2715a.a(new h(context));
                f2715a.a(new k());
                f2715a.a(new n(context));
                l lVar = new l(context);
                if (!TextUtils.isEmpty(lVar.a())) {
                    f2715a.a(lVar);
                }
                j jVar = new j(context);
                if (jVar.b()) {
                    f2715a.a(jVar);
                    f2715a.a(new i(context));
                    jVar.d();
                }
                f2715a.d();
            }
            eVar = f2715a;
        }
        return eVar;
    }

    public boolean a(cy cyVar) {
        if (this.h.a(cyVar.f())) {
            return this.g.add(cyVar);
        }
        return false;
    }

    public void a() {
        long currentTimeMillis = System.currentTimeMillis();
        if (currentTimeMillis - this.e >= this.f) {
            boolean z = false;
            for (cy next : this.g) {
                if (next.g()) {
                    if (next.e()) {
                        z = true;
                        if (!next.g()) {
                            this.h.b(next.f());
                        }
                    }
                    z = z;
                }
            }
            if (z) {
                f();
                this.h.a();
                e();
            }
            this.e = currentTimeMillis;
        }
    }

    public al b() {
        return this.d;
    }

    private void f() {
        al alVar = new al();
        HashMap hashMap = new HashMap();
        ArrayList arrayList = new ArrayList();
        for (cy next : this.g) {
            if (next.g()) {
                if (next.h() != null) {
                    hashMap.put(next.f(), next.h());
                }
                if (next.i() != null && !next.i().isEmpty()) {
                    arrayList.addAll(next.i());
                }
            }
        }
        alVar.a(arrayList);
        alVar.a(hashMap);
        synchronized (this) {
            this.d = alVar;
        }
    }

    public void c() {
        boolean z = false;
        for (cy next : this.g) {
            if (next.g()) {
                if (next.i() != null && !next.i().isEmpty()) {
                    next.a((List<aj>) null);
                    z = true;
                }
                z = z;
            }
        }
        if (z) {
            this.d.b(false);
            e();
        }
    }

    public void d() {
        al g2 = g();
        if (g2 != null) {
            ArrayList<cy> arrayList = new ArrayList<>(this.g.size());
            synchronized (this) {
                this.d = g2;
                for (cy next : this.g) {
                    next.a(this.d);
                    if (!next.g()) {
                        arrayList.add(next);
                    }
                }
                for (cy remove : arrayList) {
                    this.g.remove(remove);
                }
            }
            f();
        }
    }

    public void e() {
        if (this.d != null) {
            a(this.d);
        }
    }

    private al g() {
        FileInputStream fileInputStream;
        Throwable th;
        if (!this.c.exists()) {
            return null;
        }
        try {
            fileInputStream = new FileInputStream(this.c);
            try {
                byte[] b2 = au.b(fileInputStream);
                al alVar = new al();
                new bg().a(alVar, b2);
                au.c(fileInputStream);
                return alVar;
            } catch (Exception e2) {
                e = e2;
                try {
                    e.printStackTrace();
                    au.c(fileInputStream);
                    return null;
                } catch (Throwable th2) {
                    th = th2;
                    au.c(fileInputStream);
                    throw th;
                }
            }
        } catch (Exception e3) {
            e = e3;
            fileInputStream = null;
            e.printStackTrace();
            au.c(fileInputStream);
            return null;
        } catch (Throwable th3) {
            fileInputStream = null;
            th = th3;
            au.c(fileInputStream);
            throw th;
        }
    }

    private void a(al alVar) {
        byte[] a2;
        if (alVar != null) {
            try {
                synchronized (this) {
                    a2 = new bi().a(alVar);
                }
                if (a2 != null) {
                    au.a(this.c, a2);
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }

    /* compiled from: IdTracker */
    public static class a {

        /* renamed from: a  reason: collision with root package name */
        private Context f2716a;
        private Set<String> b = new HashSet();

        public a(Context context) {
            this.f2716a = context;
        }

        public boolean a(String str) {
            return !this.b.contains(str);
        }

        public void b(String str) {
            this.b.add(str);
        }

        public void a() {
            if (!this.b.isEmpty()) {
                StringBuilder sb = new StringBuilder();
                for (String append : this.b) {
                    sb.append(append);
                    sb.append(',');
                }
                sb.deleteCharAt(sb.length() - 1);
                aa.a(this.f2716a).edit().putString("invld_id", sb.toString()).commit();
            }
        }

        public void b() {
            String[] split;
            String string = aa.a(this.f2716a).getString("invld_id", null);
            if (!TextUtils.isEmpty(string) && (split = string.split(MiPushClient.ACCEPT_TIME_SEPARATOR)) != null) {
                for (String str : split) {
                    if (!TextUtils.isEmpty(str)) {
                        this.b.add(str);
                    }
                }
            }
        }
    }
}
