package com.umeng.a.a;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import java.util.concurrent.atomic.AtomicInteger;

/* compiled from: DatabaseManager */
public class z {
    private static z c;
    private static SQLiteOpenHelper d;

    /* renamed from: a  reason: collision with root package name */
    private AtomicInteger f2740a = new AtomicInteger();
    private AtomicInteger b = new AtomicInteger();
    private SQLiteDatabase e;

    private static synchronized void b(Context context) {
        synchronized (z.class) {
            if (c == null) {
                c = new z();
                d = ay.a(context);
            }
        }
    }

    public static synchronized z a(Context context) {
        z zVar;
        synchronized (z.class) {
            if (c == null) {
                b(context);
            }
            zVar = c;
        }
        return zVar;
    }

    public synchronized SQLiteDatabase a() {
        if (this.f2740a.incrementAndGet() == 1) {
            this.e = d.getReadableDatabase();
        }
        return this.e;
    }

    public synchronized SQLiteDatabase b() {
        if (this.f2740a.incrementAndGet() == 1) {
            this.e = d.getWritableDatabase();
        }
        return this.e;
    }

    public synchronized void c() {
        if (this.f2740a.decrementAndGet() == 0) {
            this.e.close();
        }
        if (this.b.decrementAndGet() == 0) {
            this.e.close();
        }
    }
}
