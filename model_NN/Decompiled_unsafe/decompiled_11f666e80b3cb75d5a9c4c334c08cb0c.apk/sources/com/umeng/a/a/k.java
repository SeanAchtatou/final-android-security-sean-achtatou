package com.umeng.a.a;

import android.os.Build;

/* compiled from: SerialTracker */
public class k extends cy {
    public k() {
        super("serial");
    }

    public String a() {
        if (Build.VERSION.SDK_INT >= 9) {
            return Build.SERIAL;
        }
        return null;
    }
}
