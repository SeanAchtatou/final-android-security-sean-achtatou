package com.umeng.a.a;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/* compiled from: FieldMetaData */
public class bj implements Serializable {
    private static Map<Class<? extends be>, Map<? extends Object, bj>> d = new HashMap();

    /* renamed from: a  reason: collision with root package name */
    public final String f2666a;
    public final byte b;
    public final bk c;

    public bj(String str, byte b2, bk bkVar) {
        this.f2666a = str;
        this.b = b2;
        this.c = bkVar;
    }

    public static void a(Class<? extends be> cls, Map<? extends Object, bj> map) {
        d.put(cls, map);
    }
}
