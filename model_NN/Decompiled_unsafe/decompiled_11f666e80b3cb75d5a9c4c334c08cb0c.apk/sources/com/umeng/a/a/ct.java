package com.umeng.a.a;

import android.content.Context;
import android.content.ContextWrapper;
import android.database.sqlite.SQLiteDatabase;
import java.io.File;

/* compiled from: UMDBCreater */
class ct extends ContextWrapper {

    /* renamed from: a  reason: collision with root package name */
    private String f2705a;

    public ct(Context context, String str) {
        super(context);
        this.f2705a = str;
    }

    public SQLiteDatabase openOrCreateDatabase(String str, int i, SQLiteDatabase.CursorFactory cursorFactory) {
        return SQLiteDatabase.openDatabase(getDatabasePath(str).getAbsolutePath(), cursorFactory, 268435472);
    }

    public File getDatabasePath(String str) {
        File file = new File(this.f2705a + str);
        if (!file.getParentFile().exists() && !file.getParentFile().isDirectory()) {
            file.getParentFile().mkdirs();
        }
        return file;
    }
}
