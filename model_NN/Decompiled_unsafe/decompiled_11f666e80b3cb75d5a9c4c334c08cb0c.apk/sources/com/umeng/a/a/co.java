package com.umeng.a.a;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.json.JSONObject;

/* compiled from: UMCCAggregatedManager */
public class co {
    /* access modifiers changed from: private */
    public static Context i;
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public cl f2688a;
    /* access modifiers changed from: private */
    public cq b;
    /* access modifiers changed from: private */
    public cr c;
    /* access modifiers changed from: private */
    public boolean d;
    private boolean e;
    private long f;
    private final String g;
    private final String h;
    private List<String> j;
    /* access modifiers changed from: private */
    public a k;
    private final Thread l;

    /* compiled from: UMCCAggregatedManager */
    private static class b {
        /* access modifiers changed from: private */

        /* renamed from: a  reason: collision with root package name */
        public static final co f2700a = new co();
    }

    /* compiled from: UMCCAggregatedManager */
    private static class a extends Handler {

        /* renamed from: a  reason: collision with root package name */
        private final WeakReference<co> f2699a;

        public a(co coVar) {
            this.f2699a = new WeakReference<>(coVar);
        }

        public void handleMessage(Message message) {
            if (this.f2699a != null) {
                switch (message.what) {
                    case 48:
                        sendEmptyMessageDelayed(48, cs.c(System.currentTimeMillis()));
                        co.a(co.i).l();
                        return;
                    case 49:
                        sendEmptyMessageDelayed(49, cs.d(System.currentTimeMillis()));
                        co.a(co.i).k();
                        return;
                    default:
                        return;
                }
            }
        }
    }

    private co() {
        this.f2688a = null;
        this.b = null;
        this.c = null;
        this.d = false;
        this.e = false;
        this.f = 0;
        this.g = "main_fest_mode";
        this.h = "main_fest_timestamp";
        this.j = new ArrayList();
        this.k = null;
        this.l = new Thread(new Runnable() {
            public void run() {
                Looper.prepare();
                if (co.this.k == null) {
                    a unused = co.this.k = new a(co.this);
                }
                co.this.g();
            }
        });
        if (i != null) {
            if (this.f2688a == null) {
                this.f2688a = new cl();
            }
            if (this.b == null) {
                this.b = cq.a(i);
            }
            if (this.c == null) {
                this.c = new cr();
            }
        }
        this.l.start();
    }

    /* access modifiers changed from: private */
    public void g() {
        long currentTimeMillis = System.currentTimeMillis();
        this.k.sendEmptyMessageDelayed(48, cs.c(currentTimeMillis));
        this.k.sendEmptyMessageDelayed(49, cs.d(currentTimeMillis));
    }

    public static final co a(Context context) {
        i = context;
        return b.f2700a;
    }

    public void a(final ck ckVar) {
        if (!this.d) {
            ax.b(new ba() {
                public void a() {
                    try {
                        co.this.b.a(new ck() {
                            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                             method: com.umeng.a.a.co.a(com.umeng.a.a.co, boolean):boolean
                             arg types: [com.umeng.a.a.co, int]
                             candidates:
                              com.umeng.a.a.co.a(com.umeng.a.a.co, com.umeng.a.a.co$a):com.umeng.a.a.co$a
                              com.umeng.a.a.co.a(com.umeng.a.a.co, boolean):boolean */
                            public void a(Object obj, boolean z) {
                                if (obj instanceof Map) {
                                    co.this.f2688a.a((Map) obj);
                                } else if ((obj instanceof String) || (obj instanceof Boolean)) {
                                }
                                boolean unused = co.this.d = true;
                            }
                        });
                        co.this.i();
                        co.this.m();
                        ckVar.a("success", false);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }

    private void h() {
        SharedPreferences.Editor edit = aa.a(i).edit();
        edit.putBoolean("main_fest_mode", false);
        edit.putLong("main_fest_timestamp", 0);
        edit.commit();
        this.e = false;
    }

    /* access modifiers changed from: private */
    public void i() {
        SharedPreferences a2 = aa.a(i);
        this.e = a2.getBoolean("main_fest_mode", false);
        this.f = a2.getLong("main_fest_timestamp", 0);
    }

    public JSONObject a() {
        JSONObject a2 = this.b.a();
        JSONObject jSONObject = new JSONObject();
        if (a2 == null || a2.length() <= 0) {
            return null;
        }
        for (String next : this.j) {
            if (a2.has(next)) {
                try {
                    jSONObject.put(next, a2.opt(next));
                } catch (Exception e2) {
                }
            }
        }
        return jSONObject;
    }

    public JSONObject b() {
        if (this.c.a().size() > 0) {
            this.b.b(new ck() {
                public void a(Object obj, boolean z) {
                    if (obj instanceof String) {
                        co.this.c.b();
                    }
                }
            }, this.c.a());
        }
        return this.b.b(new ck());
    }

    public void b(ck ckVar) {
        boolean z = false;
        if (this.e) {
            if (this.f == 0) {
                i();
            }
            z = cs.a(System.currentTimeMillis(), this.f);
        }
        if (!z) {
            h();
            this.j.clear();
        }
        this.c.b();
        this.b.a(new ck() {
            public void a(Object obj, boolean z) {
                if (obj.equals("success")) {
                    co.this.j();
                }
            }
        }, z);
    }

    /* access modifiers changed from: private */
    public void j() {
        for (Map.Entry<List<String>, cm> key : this.f2688a.a().entrySet()) {
            List list = (List) key.getKey();
            if (!this.j.contains(list)) {
                this.j.add(bq.a(list));
            }
        }
        if (this.j.size() > 0) {
            this.b.a(new ck(), this.j);
        }
    }

    public void a(long j2, long j3, String str) {
        this.b.a(new ck() {
            public void a(Object obj, boolean z) {
                if (obj.equals("success")) {
                }
            }
        }, str, j2, j3);
    }

    /* access modifiers changed from: private */
    public void k() {
        try {
            if (this.f2688a.a().size() > 0) {
                this.b.c(new ck() {
                    public void a(Object obj, boolean z) {
                        if (obj instanceof String) {
                            co.this.f2688a.b();
                        }
                    }
                }, this.f2688a.a());
            }
            if (this.c.a().size() > 0) {
                this.b.b(new ck() {
                    public void a(Object obj, boolean z) {
                        if (obj instanceof String) {
                            co.this.c.b();
                        }
                    }
                }, this.c.a());
            }
            if (this.j.size() > 0) {
                this.b.a(new ck(), this.j);
            }
        } catch (Throwable th) {
            aw.a("converyMemoryToDataTable happen error: " + th.toString());
        }
    }

    /* access modifiers changed from: private */
    public void l() {
        try {
            if (this.f2688a.a().size() > 0) {
                this.b.a(new ck() {
                    public void a(Object obj, boolean z) {
                    }
                }, this.f2688a.a());
            }
            if (this.c.a().size() > 0) {
                this.b.b(new ck() {
                    public void a(Object obj, boolean z) {
                        if (obj instanceof String) {
                            co.this.c.b();
                        }
                    }
                }, this.c.a());
            }
            if (this.j.size() > 0) {
                this.b.a(new ck(), this.j);
            }
        } catch (Throwable th) {
            aw.a("convertMemoryToCacheTable happen error: " + th.toString());
        }
    }

    /* access modifiers changed from: private */
    public void m() {
        List<String> b2 = this.b.b();
        if (b2 != null) {
            this.j = b2;
        }
    }

    public void c() {
        l();
    }

    public void d() {
        l();
    }

    public void e() {
        l();
    }
}
