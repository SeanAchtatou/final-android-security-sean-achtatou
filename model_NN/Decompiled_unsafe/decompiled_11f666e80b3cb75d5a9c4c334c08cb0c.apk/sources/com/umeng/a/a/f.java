package com.umeng.a.a;

import android.content.Context;
import android.telephony.TelephonyManager;

/* compiled from: ImeiTracker */
public class f extends cy {

    /* renamed from: a  reason: collision with root package name */
    private Context f2717a;

    public f(Context context) {
        super("imei");
        this.f2717a = context;
    }

    public String a() {
        TelephonyManager telephonyManager = (TelephonyManager) this.f2717a.getSystemService("phone");
        if (telephonyManager == null) {
        }
        try {
            if (at.a(this.f2717a, "android.permission.READ_PHONE_STATE")) {
                return telephonyManager.getDeviceId();
            }
            return null;
        } catch (Exception e) {
            return null;
        }
    }
}
