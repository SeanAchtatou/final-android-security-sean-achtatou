package com.umeng.a.a;

import android.content.Context;
import android.os.Environment;
import java.io.File;
import java.io.FileInputStream;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/* compiled from: UTDIdTracker */
public class m extends cy {

    /* renamed from: a  reason: collision with root package name */
    private static final Pattern f2724a = Pattern.compile("UTDID\">([^<]+)");
    private Context b;

    public m(Context context) {
        super("utdid");
        this.b = context;
    }

    public String a() {
        try {
            return (String) Class.forName("com.e.a.a").getMethod("getUtdid", Context.class).invoke(null, this.b);
        } catch (Exception e) {
            return b();
        }
    }

    private String b() {
        FileInputStream fileInputStream;
        File c = c();
        if (c == null || !c.exists()) {
            return null;
        }
        try {
            fileInputStream = new FileInputStream(c);
            String b2 = b(au.a(fileInputStream));
            au.c(fileInputStream);
            return b2;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } catch (Throwable th) {
            au.c(fileInputStream);
            throw th;
        }
    }

    private String b(String str) {
        if (str == null) {
            return null;
        }
        Matcher matcher = f2724a.matcher(str);
        if (matcher.find()) {
            return matcher.group(1);
        }
        return null;
    }

    private File c() {
        if (!at.a(this.b, "android.permission.WRITE_EXTERNAL_STORAGE") || !Environment.getExternalStorageState().equals("mounted")) {
            return null;
        }
        try {
            return new File(Environment.getExternalStorageDirectory().getCanonicalPath(), ".UTSystemConfig/Global/Alvin2.xml");
        } catch (Exception e) {
            return null;
        }
    }
}
