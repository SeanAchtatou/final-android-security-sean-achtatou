package com.umeng.a.a;

import java.io.Serializable;
import java.util.BitSet;
import java.util.Collections;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/* compiled from: ImprintValue */
public class an implements be<an, e>, Serializable, Cloneable {
    public static final Map<e, bj> d;
    /* access modifiers changed from: private */
    public static final bz e = new bz("ImprintValue");
    /* access modifiers changed from: private */
    public static final br f = new br("value", (byte) 11, 1);
    /* access modifiers changed from: private */
    public static final br g = new br("ts", (byte) 10, 2);
    /* access modifiers changed from: private */
    public static final br h = new br("guid", (byte) 11, 3);
    private static final Map<Class<? extends cb>, cc> i = new HashMap();

    /* renamed from: a  reason: collision with root package name */
    public String f2632a;
    public long b;
    public String c;
    private byte j = 0;
    private e[] k = {e.VALUE};

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [com.umeng.a.a.an$e, com.umeng.a.a.bj]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        i.put(cd.class, new b());
        i.put(ce.class, new d());
        EnumMap enumMap = new EnumMap(e.class);
        enumMap.put((Object) e.VALUE, (Object) new bj("value", (byte) 2, new bk((byte) 11)));
        enumMap.put((Object) e.TS, (Object) new bj("ts", (byte) 1, new bk((byte) 10)));
        enumMap.put((Object) e.c, (Object) new bj("guid", (byte) 1, new bk((byte) 11)));
        d = Collections.unmodifiableMap(enumMap);
        bj.a(an.class, d);
    }

    /* compiled from: ImprintValue */
    public enum e {
        VALUE(1, "value"),
        TS(2, "ts"),
        c(3, "guid");
        
        private static final Map<String, e> d = new HashMap();
        private final short e;
        private final String f;

        static {
            Iterator it = EnumSet.allOf(e.class).iterator();
            while (it.hasNext()) {
                e eVar = (e) it.next();
                d.put(eVar.a(), eVar);
            }
        }

        private e(short s, String str) {
            this.e = s;
            this.f = str;
        }

        public String a() {
            return this.f;
        }
    }

    public String a() {
        return this.f2632a;
    }

    public boolean b() {
        return this.f2632a != null;
    }

    public void a(boolean z) {
        if (!z) {
            this.f2632a = null;
        }
    }

    public long c() {
        return this.b;
    }

    public boolean d() {
        return bc.a(this.j, 0);
    }

    public void b(boolean z) {
        this.j = bc.a(this.j, 0, z);
    }

    public String e() {
        return this.c;
    }

    public void c(boolean z) {
        if (!z) {
            this.c = null;
        }
    }

    public void a(bu buVar) throws bh {
        i.get(buVar.y()).b().b(buVar, this);
    }

    public void b(bu buVar) throws bh {
        i.get(buVar.y()).b().a(buVar, this);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("ImprintValue(");
        boolean z = true;
        if (b()) {
            sb.append("value:");
            if (this.f2632a == null) {
                sb.append("null");
            } else {
                sb.append(this.f2632a);
            }
            z = false;
        }
        if (!z) {
            sb.append(", ");
        }
        sb.append("ts:");
        sb.append(this.b);
        sb.append(", ");
        sb.append("guid:");
        if (this.c == null) {
            sb.append("null");
        } else {
            sb.append(this.c);
        }
        sb.append(")");
        return sb.toString();
    }

    public void f() throws bh {
        if (this.c == null) {
            throw new bv("Required field 'guid' was not present! Struct: " + toString());
        }
    }

    /* compiled from: ImprintValue */
    private static class b implements cc {
        private b() {
        }

        /* renamed from: a */
        public a b() {
            return new a();
        }
    }

    /* compiled from: ImprintValue */
    private static class a extends cd<an> {
        private a() {
        }

        /* renamed from: a */
        public void b(bu buVar, an anVar) throws bh {
            buVar.f();
            while (true) {
                br h = buVar.h();
                if (h.b == 0) {
                    buVar.g();
                    if (!anVar.d()) {
                        throw new bv("Required field 'ts' was not found in serialized data! Struct: " + toString());
                    }
                    anVar.f();
                    return;
                }
                switch (h.c) {
                    case 1:
                        if (h.b != 11) {
                            bx.a(buVar, h.b);
                            break;
                        } else {
                            anVar.f2632a = buVar.v();
                            anVar.a(true);
                            break;
                        }
                    case 2:
                        if (h.b != 10) {
                            bx.a(buVar, h.b);
                            break;
                        } else {
                            anVar.b = buVar.t();
                            anVar.b(true);
                            break;
                        }
                    case 3:
                        if (h.b != 11) {
                            bx.a(buVar, h.b);
                            break;
                        } else {
                            anVar.c = buVar.v();
                            anVar.c(true);
                            break;
                        }
                    default:
                        bx.a(buVar, h.b);
                        break;
                }
                buVar.i();
            }
        }

        /* renamed from: b */
        public void a(bu buVar, an anVar) throws bh {
            anVar.f();
            buVar.a(an.e);
            if (anVar.f2632a != null && anVar.b()) {
                buVar.a(an.f);
                buVar.a(anVar.f2632a);
                buVar.b();
            }
            buVar.a(an.g);
            buVar.a(anVar.b);
            buVar.b();
            if (anVar.c != null) {
                buVar.a(an.h);
                buVar.a(anVar.c);
                buVar.b();
            }
            buVar.c();
            buVar.a();
        }
    }

    /* compiled from: ImprintValue */
    private static class d implements cc {
        private d() {
        }

        /* renamed from: a */
        public c b() {
            return new c();
        }
    }

    /* compiled from: ImprintValue */
    private static class c extends ce<an> {
        private c() {
        }

        public void a(bu buVar, an anVar) throws bh {
            ca caVar = (ca) buVar;
            caVar.a(anVar.b);
            caVar.a(anVar.c);
            BitSet bitSet = new BitSet();
            if (anVar.b()) {
                bitSet.set(0);
            }
            caVar.a(bitSet, 1);
            if (anVar.b()) {
                caVar.a(anVar.f2632a);
            }
        }

        public void b(bu buVar, an anVar) throws bh {
            ca caVar = (ca) buVar;
            anVar.b = caVar.t();
            anVar.b(true);
            anVar.c = caVar.v();
            anVar.c(true);
            if (caVar.b(1).get(0)) {
                anVar.f2632a = caVar.v();
                anVar.a(true);
            }
        }
    }
}
