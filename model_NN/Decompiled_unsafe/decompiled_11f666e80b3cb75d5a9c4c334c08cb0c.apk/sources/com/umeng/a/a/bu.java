package com.umeng.a.a;

import java.nio.ByteBuffer;

/* compiled from: TProtocol */
public abstract class bu {
    protected ch e;

    public abstract void a() throws bh;

    public abstract void a(int i) throws bh;

    public abstract void a(long j) throws bh;

    public abstract void a(br brVar) throws bh;

    public abstract void a(bs bsVar) throws bh;

    public abstract void a(bt btVar) throws bh;

    public abstract void a(bz bzVar) throws bh;

    public abstract void a(String str) throws bh;

    public abstract void a(ByteBuffer byteBuffer) throws bh;

    public abstract void b() throws bh;

    public abstract void c() throws bh;

    public abstract void d() throws bh;

    public abstract void e() throws bh;

    public abstract bz f() throws bh;

    public abstract void g() throws bh;

    public abstract br h() throws bh;

    public abstract void i() throws bh;

    public abstract bt j() throws bh;

    public abstract void k() throws bh;

    public abstract bs l() throws bh;

    public abstract void m() throws bh;

    public abstract by n() throws bh;

    public abstract void o() throws bh;

    public abstract boolean p() throws bh;

    public abstract byte q() throws bh;

    public abstract short r() throws bh;

    public abstract int s() throws bh;

    public abstract long t() throws bh;

    public abstract double u() throws bh;

    public abstract String v() throws bh;

    public abstract ByteBuffer w() throws bh;

    protected bu(ch chVar) {
        this.e = chVar;
    }

    public void x() {
    }

    public Class<? extends cb> y() {
        return cd.class;
    }
}
