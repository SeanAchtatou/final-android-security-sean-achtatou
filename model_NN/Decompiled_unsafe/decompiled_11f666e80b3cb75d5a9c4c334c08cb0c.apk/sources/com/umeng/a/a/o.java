package com.umeng.a.a;

import java.security.KeyStore;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

/* compiled from: NetworkHelper */
class o implements X509TrustManager {

    /* renamed from: a  reason: collision with root package name */
    X509TrustManager f2727a;

    public o() {
        try {
            TrustManagerFactory instance = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
            instance.init((KeyStore) null);
            TrustManager[] trustManagers = instance.getTrustManagers();
            for (int i = 0; i < trustManagers.length; i++) {
                if (trustManagers[i] instanceof X509TrustManager) {
                    this.f2727a = (X509TrustManager) trustManagers[i];
                    return;
                }
            }
        } catch (Throwable th) {
        }
    }

    public void checkClientTrusted(X509Certificate[] x509CertificateArr, String str) {
    }

    public void checkServerTrusted(X509Certificate[] x509CertificateArr, String str) {
        try {
            this.f2727a.checkServerTrusted(x509CertificateArr, str);
        } catch (CertificateException e) {
        }
    }

    public X509Certificate[] getAcceptedIssuers() {
        return this.f2727a.getAcceptedIssuers();
    }
}
