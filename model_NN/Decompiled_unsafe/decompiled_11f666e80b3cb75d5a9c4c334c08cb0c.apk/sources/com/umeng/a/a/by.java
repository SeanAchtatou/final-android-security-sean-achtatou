package com.umeng.a.a;

/* compiled from: TSet */
public final class by {

    /* renamed from: a  reason: collision with root package name */
    public final byte f2677a;
    public final int b;

    public by() {
        this((byte) 0, 0);
    }

    public by(byte b2, int i) {
        this.f2677a = b2;
        this.b = i;
    }

    public by(bs bsVar) {
        this(bsVar.f2673a, bsVar.b);
    }
}
