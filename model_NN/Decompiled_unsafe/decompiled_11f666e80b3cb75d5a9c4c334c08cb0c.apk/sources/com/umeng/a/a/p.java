package com.umeng.a.a;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.Bundle;
import com.umeng.a.a.cx;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONObject;

/* compiled from: AutoViewPageTracker */
public class p {

    /* renamed from: a  reason: collision with root package name */
    public static String f2728a = null;
    private static JSONObject d = new JSONObject();
    Application.ActivityLifecycleCallbacks b = new Application.ActivityLifecycleCallbacks() {
        public void onActivityStopped(Activity activity) {
        }

        public void onActivityStarted(Activity activity) {
        }

        public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
        }

        public void onActivityResumed(Activity activity) {
            p.this.b(activity);
        }

        public void onActivityPaused(Activity activity) {
            p.this.c(activity);
        }

        public void onActivityDestroyed(Activity activity) {
        }

        public void onActivityCreated(Activity activity, Bundle bundle) {
        }
    };
    private final Map<String, Long> c = new HashMap();
    private Application e = null;

    public p(Activity activity) {
        if (activity != null) {
            this.e = activity.getApplication();
            a(activity);
        }
    }

    private void a(Activity activity) {
        this.e.registerActivityLifecycleCallbacks(this.b);
        if (f2728a == null) {
            b(activity);
        }
    }

    public void a() {
        if (this.e != null) {
            this.e.unregisterActivityLifecycleCallbacks(this.b);
        }
    }

    public void a(Context context) {
        c(null);
        a();
    }

    public static void b(Context context) {
        try {
            synchronized (d) {
                if (d.length() > 0) {
                    cx.a(context).a(ad.a(), d, cx.a.AUTOPAGE);
                    d = new JSONObject();
                }
            }
        } catch (Throwable th) {
        }
    }

    /* access modifiers changed from: private */
    public void b(Activity activity) {
        f2728a = activity.getPackageName() + "." + activity.getLocalClassName();
        synchronized (this.c) {
            this.c.put(f2728a, Long.valueOf(System.currentTimeMillis()));
        }
    }

    /* access modifiers changed from: private */
    public void c(Activity activity) {
        long j = 0;
        try {
            synchronized (this.c) {
                if (this.c.containsKey(f2728a)) {
                    j = System.currentTimeMillis() - this.c.get(f2728a).longValue();
                    this.c.remove(f2728a);
                }
            }
            synchronized (d) {
                try {
                    d = new JSONObject();
                    d.put("page_name", f2728a);
                    d.put("duration", j);
                } catch (Throwable th) {
                }
            }
        } catch (Throwable th2) {
        }
    }
}
