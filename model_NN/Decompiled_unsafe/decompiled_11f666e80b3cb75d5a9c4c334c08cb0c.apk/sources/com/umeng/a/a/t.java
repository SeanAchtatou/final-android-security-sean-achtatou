package com.umeng.a.a;

import android.content.Context;
import android.text.TextUtils;
import java.util.Iterator;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: EventTracker */
public class t {

    /* renamed from: a  reason: collision with root package name */
    private final int f2738a = 128;
    private final int b = 256;
    private final int c = 10;
    private Context d;
    private r e = null;
    private q f = null;
    private JSONObject g = null;
    private r h;

    public t(Context context) {
        if (context == null) {
            try {
                aw.c("Context is null, can't track event");
            } catch (Exception e2) {
                e2.printStackTrace();
                return;
            }
        }
        this.h = r.b(context);
        this.d = context;
        this.e = r.b(this.d);
        this.f = this.e.a(this.d);
        if (this.g == null) {
            a(context);
        }
    }

    public void a(String str, Map<String, Object> map, long j) {
        try {
            if (a(str) && a(map)) {
                JSONObject jSONObject = new JSONObject();
                jSONObject.put("id", str);
                jSONObject.put("ts", System.currentTimeMillis());
                if (j > 0) {
                    jSONObject.put("du", j);
                }
                jSONObject.put("__t", 2049);
                Iterator<Map.Entry<String, Object>> it = map.entrySet().iterator();
                for (int i = 0; i < 10 && it.hasNext(); i++) {
                    Map.Entry next = it.next();
                    if (!"$st_fl".equals(next.getKey()) && !"dplus_st".equals(next.getKey()) && !"du".equals(next.getKey()) && !"id".equals(next.getKey()) && !"ts".equals(next.getKey())) {
                        Object value = next.getValue();
                        if ((value instanceof String) || (value instanceof Integer) || (value instanceof Long)) {
                            jSONObject.put((String) next.getKey(), value);
                        }
                    }
                }
                jSONObject.put("__i", ad.g(this.d));
                jSONObject.put("_umpname", p.f2728a);
                this.h.a(jSONObject);
            }
        } catch (Throwable th) {
        }
    }

    public void a(String str, String str2, long j, int i) {
        try {
            if (a(str) && b(str2)) {
                JSONObject jSONObject = new JSONObject();
                jSONObject.put("id", str);
                jSONObject.put("ts", System.currentTimeMillis());
                if (j > 0) {
                    jSONObject.put("du", j);
                }
                jSONObject.put("__t", 2049);
                if (str2 == null) {
                    str2 = "";
                }
                jSONObject.put(str, str2);
                jSONObject.put("__i", ad.g(this.d));
                jSONObject.put("_umpname", p.f2728a);
                this.h.a(jSONObject);
            }
        } catch (Throwable th) {
        }
    }

    private void a(Context context) {
        try {
            String string = aa.a(context).getString("fs_lc_tl", null);
            if (!TextUtils.isEmpty(string)) {
                this.g = new JSONObject(string);
            }
            a();
        } catch (Exception e2) {
        }
    }

    private void a() {
        int i = 0;
        try {
            if (!TextUtils.isEmpty(this.f.f2730a)) {
                String[] split = this.f.f2730a.split("!");
                JSONObject jSONObject = new JSONObject();
                if (this.g != null) {
                    for (String a2 : split) {
                        String a3 = au.a(a2, 128);
                        if (this.g.has(a3)) {
                            jSONObject.put(a3, this.g.get(a3));
                        }
                    }
                }
                this.g = new JSONObject();
                if (split.length >= 10) {
                    while (i < 10) {
                        a(split[i], jSONObject);
                        i++;
                    }
                } else {
                    while (i < split.length) {
                        a(split[i], jSONObject);
                        i++;
                    }
                }
                b(this.d);
                this.f.f2730a = null;
            }
        } catch (Exception e2) {
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.umeng.a.a.t.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.umeng.a.a.t.a(java.lang.String, org.json.JSONObject):void
      com.umeng.a.a.t.a(java.lang.String, boolean):void */
    private void a(String str, JSONObject jSONObject) throws JSONException {
        String a2 = au.a(str, 128);
        if (jSONObject.has(a2)) {
            a(a2, ((Boolean) jSONObject.get(a2)).booleanValue());
        } else {
            a(a2, false);
        }
    }

    private void a(String str, boolean z) {
        try {
            if (!"$st_fl".equals(str) && !"dplus_st".equals(str) && !"du".equals(str) && !"id".equals(str) && !"ts".equals(str) && !this.g.has(str)) {
                this.g.put(str, z);
            }
        } catch (Exception e2) {
        }
    }

    private void b(Context context) {
        try {
            if (this.g != null) {
                aa.a(this.d).edit().putString("fs_lc_tl", this.g.toString()).commit();
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    private boolean a(String str) {
        if (str != null) {
            try {
                int length = str.trim().getBytes().length;
                if (length > 0 && length <= 128) {
                    return true;
                }
            } catch (Exception e2) {
            }
        }
        aw.c("Event id is empty or too long in tracking Event");
        return false;
    }

    private boolean b(String str) {
        if (str == null) {
            return true;
        }
        try {
            if (str.trim().getBytes().length <= 256) {
                return true;
            }
            aw.c("Event label or value is empty or too long in tracking Event");
            return false;
        } catch (Exception e2) {
        }
    }

    private boolean a(Map<String, Object> map) {
        if (map != null) {
            try {
                if (!map.isEmpty()) {
                    for (Map.Entry next : map.entrySet()) {
                        if (!a((String) next.getKey())) {
                            return false;
                        }
                        if (next.getValue() == null) {
                            return false;
                        }
                        if ((next.getValue() instanceof String) && !b(next.getValue().toString())) {
                            return false;
                        }
                    }
                    return true;
                }
            } catch (Exception e2) {
            }
        }
        aw.c("map is null or empty in onEvent");
        return false;
    }
}
