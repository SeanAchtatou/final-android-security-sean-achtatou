package com.umeng.a.a;

import android.content.Context;

/* compiled from: ReportPolicy */
public class az {
    public static boolean a(int i2) {
        switch (i2) {
            case 0:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 8:
                return true;
            case 7:
            default:
                return false;
        }
    }

    /* compiled from: ReportPolicy */
    public static class h {
        public boolean a(boolean z) {
            return true;
        }

        public boolean a() {
            return true;
        }
    }

    /* compiled from: ReportPolicy */
    public static class g extends h {
        public boolean a(boolean z) {
            return true;
        }
    }

    /* compiled from: ReportPolicy */
    public static class d extends h {
        public boolean a(boolean z) {
            return z;
        }
    }

    /* compiled from: ReportPolicy */
    public static class e extends h {

        /* renamed from: a  reason: collision with root package name */
        private static long f2652a = 90000;
        private static long b = LogBuilder.MAX_INTERVAL;
        private long c;
        private ae d;

        public e(ae aeVar, long j) {
            this.d = aeVar;
            a(j);
        }

        public boolean a(boolean z) {
            if (System.currentTimeMillis() - this.d.c >= this.c) {
                return true;
            }
            return false;
        }

        public void a(long j) {
            if (j < f2652a || j > b) {
                this.c = f2652a;
            } else {
                this.c = j;
            }
        }
    }

    /* compiled from: ReportPolicy */
    public static class f extends h {

        /* renamed from: a  reason: collision with root package name */
        private long f2653a = LogBuilder.MAX_INTERVAL;
        private ae b;

        public f(ae aeVar) {
            this.b = aeVar;
        }

        public boolean a(boolean z) {
            if (System.currentTimeMillis() - this.b.c >= this.f2653a) {
                return true;
            }
            return false;
        }
    }

    /* compiled from: ReportPolicy */
    public static class i extends h {

        /* renamed from: a  reason: collision with root package name */
        private Context f2654a = null;

        public i(Context context) {
            this.f2654a = context;
        }

        public boolean a(boolean z) {
            return at.i(this.f2654a);
        }
    }

    /* compiled from: ReportPolicy */
    public static class b extends h {

        /* renamed from: a  reason: collision with root package name */
        private ah f2650a;
        private ae b;

        public b(ae aeVar, ah ahVar) {
            this.b = aeVar;
            this.f2650a = ahVar;
        }

        public boolean a(boolean z) {
            long currentTimeMillis = System.currentTimeMillis();
            if (currentTimeMillis - this.b.c >= this.f2650a.b()) {
                return true;
            }
            return false;
        }

        public boolean a() {
            return this.f2650a.c();
        }
    }

    /* compiled from: ReportPolicy */
    public static class c extends h {

        /* renamed from: a  reason: collision with root package name */
        private long f2651a;
        private long b = 0;

        public c(int i) {
            this.f2651a = (long) i;
            this.b = System.currentTimeMillis();
        }

        public boolean a(boolean z) {
            if (System.currentTimeMillis() - this.b >= this.f2651a) {
                return true;
            }
            return false;
        }

        public boolean a() {
            return System.currentTimeMillis() - this.b < this.f2651a;
        }
    }

    /* compiled from: ReportPolicy */
    public static class j extends h {

        /* renamed from: a  reason: collision with root package name */
        private final long f2655a = 10800000;
        private ae b;

        public j(ae aeVar) {
            this.b = aeVar;
        }

        public boolean a(boolean z) {
            if (System.currentTimeMillis() - this.b.c >= 10800000) {
                return true;
            }
            return false;
        }
    }

    /* compiled from: ReportPolicy */
    public static class a extends h {

        /* renamed from: a  reason: collision with root package name */
        private final long f2649a = 15000;
        private ae b;

        public a(ae aeVar) {
            this.b = aeVar;
        }

        public boolean a(boolean z) {
            if (System.currentTimeMillis() - this.b.c >= 15000) {
                return true;
            }
            return false;
        }
    }
}
