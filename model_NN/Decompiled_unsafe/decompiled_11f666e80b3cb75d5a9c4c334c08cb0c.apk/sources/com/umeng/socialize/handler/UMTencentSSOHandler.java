package com.umeng.socialize.handler;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;
import com.qq.e.comm.constants.Constants;
import com.tencent.connect.common.Constants;
import com.tencent.tauth.Tencent;
import com.umeng.socialize.Config;
import com.umeng.socialize.PlatformConfig;
import com.umeng.socialize.UMAuthListener;
import com.umeng.socialize.UMShareListener;
import com.umeng.socialize.c.a;
import com.umeng.socialize.d.m;
import com.umeng.socialize.media.UMediaObject;
import com.umeng.socialize.utils.g;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public abstract class UMTencentSSOHandler extends UMSSOHandler {
    protected static Map<String, String> c = new HashMap();
    protected ProgressDialog b = null;
    protected String d = null;
    public PlatformConfig.QQZone e = null;
    protected UMAuthListener f;
    protected Tencent g;
    protected UMShareListener h;
    private Context i;

    protected interface ObtainAppIdListener {
    }

    public interface ObtainImageUrlListener {
        void a(String str);
    }

    public void a(Context context, PlatformConfig.Platform platform) {
        super.a(context, platform);
        this.i = context;
        this.e = (PlatformConfig.QQZone) platform;
        g.c("appid", "appid qq:" + this.e.appId);
        this.g = Tencent.createInstance(this.e.appId, context);
        Log.e("xxxx", "tencent=" + this.g.getOpenId());
        if (this.g == null) {
            g.b("UMTencentSSOHandler", "Tencent变量初始化失败，请检查你的app id跟AndroidManifest.xml文件中AuthActivity的scheme是否填写正确");
        }
    }

    /* access modifiers changed from: protected */
    public boolean a(String str, int i2) {
        if (TextUtils.isEmpty(str)) {
            return false;
        }
        a name = this.e.getName();
        boolean d2 = d(this.i);
        boolean z = !str.startsWith("http://") && !str.startsWith("https://");
        if (d2 || !z) {
            return false;
        }
        if (name == a.QQ && (i2 == 2 || i2 == 1)) {
            return true;
        }
        if (name != a.QZONE) {
            return false;
        }
        if (i2 == 1 || i2 == 2) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public Bundle a(Object obj) {
        JSONObject jSONObject;
        Bundle bundle = new Bundle();
        if (obj != null) {
            String trim = obj.toString().trim();
            if (!TextUtils.isEmpty(trim)) {
                try {
                    jSONObject = new JSONObject(trim);
                } catch (JSONException e2) {
                    e2.printStackTrace();
                    jSONObject = null;
                }
                if (jSONObject != null) {
                    bundle.putString("auth_time", jSONObject.optString("auth_time", ""));
                    bundle.putString("pay_token", jSONObject.optString("pay_token", ""));
                    bundle.putString(Constants.PARAM_PLATFORM_ID, jSONObject.optString(Constants.PARAM_PLATFORM_ID, ""));
                    bundle.putString(Constants.KEYS.RET, String.valueOf(jSONObject.optInt(Constants.KEYS.RET, -1)));
                    bundle.putString("sendinstall", jSONObject.optString("sendinstall", ""));
                    bundle.putString("page_type", jSONObject.optString("page_type", ""));
                    bundle.putString("appid", jSONObject.optString("appid", ""));
                    bundle.putString("openid", jSONObject.optString("openid", ""));
                    bundle.putString("uid", jSONObject.optString("openid", ""));
                    bundle.putString("expires_in", jSONObject.optString("expires_in", ""));
                    bundle.putString("pfkey", jSONObject.optString("pfkey", ""));
                    bundle.putString("access_token", jSONObject.optString("access_token", ""));
                }
            }
        }
        return bundle;
    }

    public boolean d(Context context) {
        return this.g.isSupportSSOLogin((Activity) context);
    }

    /* access modifiers changed from: protected */
    public String c() {
        if (!TextUtils.isEmpty(Config.QQAPPNAME)) {
            return Config.QQAPPNAME;
        }
        if (this.i == null) {
            return "";
        }
        CharSequence loadLabel = this.i.getApplicationInfo().loadLabel(this.i.getPackageManager());
        if (!TextUtils.isEmpty(loadLabel)) {
            return loadLabel.toString();
        }
        return "";
    }

    public void a(UMediaObject uMediaObject, String str, ObtainImageUrlListener obtainImageUrlListener) {
        com.umeng.socialize.media.g gVar;
        System.currentTimeMillis();
        if (uMediaObject instanceof com.umeng.socialize.media.g) {
            gVar = (com.umeng.socialize.media.g) uMediaObject;
        } else {
            gVar = null;
        }
        if (gVar != null) {
            String file = gVar.j().toString();
            String str2 = c.get(file);
            if (!TextUtils.isEmpty(str2)) {
                this.d = str2;
                g.a("UMTencentSSOHandler", "obtain image url form cache..." + this.d);
            } else {
                g.a("UMTencentSSOHandler", "obtain image url form server...");
                String f2 = new m(this.i, gVar, str).f();
                a(file, f2);
                if (this.i != null && TextUtils.isEmpty(f2)) {
                    Toast.makeText(this.i, "上传图片失败", 0).show();
                }
                g.a("UMTencentSSOHandler", "obtain image url form server..." + this.d);
            }
        }
        g.a("UMTencentSSOHandler", "doInBackground end...");
        obtainImageUrlListener.a(this.d);
    }

    private void a(String str, String str2) {
        if (!TextUtils.isEmpty(str2)) {
            c.put(str, str2);
            this.d = str2;
        }
    }

    /* access modifiers changed from: protected */
    public boolean d() {
        return this.g != null && this.g.getAppId().equals(this.e.appId);
    }
}
