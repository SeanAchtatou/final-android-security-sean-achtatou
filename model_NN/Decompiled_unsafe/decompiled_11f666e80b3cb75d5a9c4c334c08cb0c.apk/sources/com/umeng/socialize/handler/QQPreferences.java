package com.umeng.socialize.handler;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import com.umeng.socialize.utils.g;

public class QQPreferences {
    private static String b = null;

    /* renamed from: a  reason: collision with root package name */
    private String f2823a = null;
    private String c = null;
    private SharedPreferences d = null;

    public QQPreferences(Context context, String str) {
        this.d = context.getSharedPreferences(str, 0);
        this.f2823a = this.d.getString("access_token", null);
        this.c = this.d.getString("uid", null);
        b = this.d.getString("expires_in", null);
    }

    public String a() {
        return this.f2823a;
    }

    public static String b() {
        return b;
    }

    public String c() {
        return this.c;
    }

    public QQPreferences a(Bundle bundle) {
        this.f2823a = bundle.getString("access_token");
        b = bundle.getString("expires_in");
        this.c = bundle.getString("uid");
        return this;
    }

    public String d() {
        return this.c;
    }

    public boolean e() {
        return this.f2823a != null;
    }

    public void f() {
        this.d.edit().putString("access_token", this.f2823a).putString("expires_in", b).putString("uid", this.c).commit();
        g.a("save auth succeed");
    }

    public void g() {
        this.d.edit().clear().commit();
    }
}
