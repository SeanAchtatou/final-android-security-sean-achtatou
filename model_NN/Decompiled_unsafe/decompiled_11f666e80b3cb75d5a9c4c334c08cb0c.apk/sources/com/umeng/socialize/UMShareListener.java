package com.umeng.socialize;

import com.umeng.socialize.c.a;

public interface UMShareListener {
    void onCancel(a aVar);

    void onError(a aVar, Throwable th);

    void onResult(a aVar);
}
