package com.umeng.socialize.editorpage;

import android.location.Location;
import android.widget.Toast;
import com.umeng.socialize.c.c;
import com.umeng.socialize.editorpage.location.a;
import com.umeng.socialize.editorpage.location.b;
import com.umeng.socialize.utils.g;

/* compiled from: ShareActivity */
class f extends b {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ShareActivity f2816a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    f(ShareActivity shareActivity, a aVar) {
        super(aVar);
        this.f2816a = shareActivity;
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
        super.onPreExecute();
        this.f2816a.a(true);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void onPostExecute(Location location) {
        super.onPostExecute(location);
        g.b("xxxxx", "result = " + location);
        c unused = this.f2816a.y = c.a(location);
        this.f2816a.a(false);
        if (location == null && !this.f2816a.isFinishing()) {
            Toast.makeText(this.f2816a.u, "获取地理位置失败，请稍候重试.", 0).show();
        }
    }

    /* access modifiers changed from: protected */
    public void onCancelled() {
        super.onCancelled();
        this.f2816a.a(false);
    }
}
