package com.umeng.socialize.editorpage.location;

import android.app.Activity;
import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;

/* compiled from: SocializeLocationManager */
public class d {

    /* renamed from: a  reason: collision with root package name */
    LocationManager f2821a = null;

    public void a(Context context) {
        if (com.umeng.socialize.utils.d.a(context, "android.permission.ACCESS_FINE_LOCATION") || com.umeng.socialize.utils.d.a(context, "android.permission.ACCESS_COARSE_LOCATION")) {
            this.f2821a = (LocationManager) context.getApplicationContext().getSystemService("location");
        }
    }

    public String a(Criteria criteria, boolean z) {
        if (this.f2821a == null) {
            return null;
        }
        return this.f2821a.getBestProvider(criteria, z);
    }

    public Location a(String str) {
        if (this.f2821a == null) {
            return null;
        }
        return this.f2821a.getLastKnownLocation(str);
    }

    public boolean b(String str) {
        if (this.f2821a == null) {
            return false;
        }
        return this.f2821a.isProviderEnabled(str);
    }

    public void a(Activity activity, String str, long j, float f, LocationListener locationListener) {
        if (this.f2821a != null) {
            activity.runOnUiThread(new e(this, str, j, f, locationListener));
        }
    }

    public void a(LocationListener locationListener) {
        if (this.f2821a != null) {
            this.f2821a.removeUpdates(locationListener);
        }
    }
}
