package com.umeng.socialize.view;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.WebView;
import com.tencent.connect.common.Constants;
import com.umeng.socialize.Config;
import com.umeng.socialize.SocializeException;
import com.umeng.socialize.UMAuthListener;
import com.umeng.socialize.utils.g;
import com.umeng.socialize.utils.h;
import com.umeng.socialize.utils.i;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/* compiled from: OauthDialog */
public class a extends Dialog {

    /* renamed from: a  reason: collision with root package name */
    private WebView f2888a;
    /* access modifiers changed from: private */
    public View b;
    private Bundle c;
    private Context d;
    private com.umeng.socialize.c.a e;
    private C0072a f;
    /* access modifiers changed from: private */
    public Handler g;

    private String a(com.umeng.socialize.c.a aVar) {
        i iVar = new i(this.d);
        iVar.a("http://log.umsns.com/").b("share/auth/").c(h.a(this.d)).d(Config.EntityKey).a(aVar).e(Constants.VIA_REPORT_TYPE_SHARE_TO_QQ).f(Config.SessionId).g(Config.UID);
        return iVar.a();
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        return super.onKeyDown(i, keyEvent);
    }

    public void show() {
        super.show();
        this.c = null;
        this.f2888a.loadUrl(a(this.e));
    }

    public void dismiss() {
        if (this.c == null) {
            this.f.a();
        } else if (TextUtils.isEmpty(this.c.getString("uid"))) {
            this.f.a(new SocializeException("unfetch usid..."));
        } else {
            g.c("OauthDialog", "### dismiss ");
            this.f.a(this.c);
        }
        super.dismiss();
    }

    /* renamed from: com.umeng.socialize.view.a$a  reason: collision with other inner class name */
    /* compiled from: OauthDialog */
    static class C0072a {

        /* renamed from: a  reason: collision with root package name */
        private UMAuthListener f2889a;
        private com.umeng.socialize.c.a b;
        private int c;

        public void a(Exception exc) {
            if (this.f2889a != null) {
                this.f2889a.onError(this.b, this.c, exc);
            }
        }

        public void a(Bundle bundle) {
            if (this.f2889a != null) {
                this.f2889a.onComplete(this.b, this.c, b(bundle));
            }
        }

        public void a() {
            if (this.f2889a != null) {
                this.f2889a.onCancel(this.b, this.c);
            }
        }

        private Map<String, String> b(Bundle bundle) {
            if (bundle == null || bundle.isEmpty()) {
                return null;
            }
            Set<String> keySet = bundle.keySet();
            HashMap hashMap = new HashMap();
            for (String next : keySet) {
                hashMap.put(next, bundle.getString(next));
            }
            return hashMap;
        }
    }
}
