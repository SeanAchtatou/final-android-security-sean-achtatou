package com.umeng.socialize;

import android.content.Context;
import android.text.TextUtils;
import com.umeng.socialize.c.a;
import com.umeng.socialize.d.c;
import com.umeng.socialize.d.d;
import com.umeng.socialize.d.g;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONObject;

public class PlatformConfig {
    public static Map<a, Platform> configs = new HashMap();

    public interface Platform {
        a getName();

        boolean isAuthrized();

        boolean isConfigured();

        void parse(JSONObject jSONObject);
    }

    static {
        configs.put(a.QQ, new QQZone(a.QQ));
        configs.put(a.QZONE, new QQZone(a.QZONE));
        configs.put(a.WEIXIN, new Weixin(a.WEIXIN));
        configs.put(a.WEIXIN_CIRCLE, new Weixin(a.WEIXIN_CIRCLE));
        configs.put(a.WEIXIN_FAVORITE, new Weixin(a.WEIXIN_FAVORITE));
        configs.put(a.DOUBAN, new Douban());
        configs.put(a.LAIWANG, new Laiwang(a.LAIWANG));
        configs.put(a.LAIWANG_DYNAMIC, new Laiwang(a.LAIWANG_DYNAMIC));
        configs.put(a.YIXIN, new Yixin(a.YIXIN));
        configs.put(a.YIXIN_CIRCLE, new Yixin(a.YIXIN_CIRCLE));
        configs.put(a.SINA, new SinaWeibo());
        configs.put(a.TENCENT, new QQZone(a.TENCENT));
        configs.put(a.ALIPAY, new Alipay());
        configs.put(a.RENREN, new Renren());
        configs.put(a.GOOGLEPLUS, new GooglePlus());
        configs.put(a.FACEBOOK, new CustomPlatform(a.FACEBOOK));
        configs.put(a.TWITTER, new Twitter(a.TWITTER));
        configs.put(a.TUMBLR, new CustomPlatform(a.TUMBLR));
        configs.put(a.PINTEREST, new Pinterest());
        configs.put(a.POCKET, new CustomPlatform(a.POCKET));
        configs.put(a.WHATSAPP, new CustomPlatform(a.WHATSAPP));
        configs.put(a.EMAIL, new CustomPlatform(a.EMAIL));
        configs.put(a.SMS, new CustomPlatform(a.SMS));
        configs.put(a.LINKEDIN, new CustomPlatform(a.LINKEDIN));
        configs.put(a.LINE, new CustomPlatform(a.LINE));
        configs.put(a.FLICKR, new CustomPlatform(a.FLICKR));
        configs.put(a.EVERNOTE, new CustomPlatform(a.EVERNOTE));
        configs.put(a.FOURSQUARE, new CustomPlatform(a.FOURSQUARE));
        configs.put(a.YNOTE, new CustomPlatform(a.YNOTE));
        configs.put(a.KAKAO, new CustomPlatform(a.KAKAO));
        configs.put(a.INSTAGRAM, new CustomPlatform(a.INSTAGRAM));
    }

    public static void setQQZone(String str, String str2) {
        QQZone qQZone = (QQZone) configs.get(a.QZONE);
        qQZone.appId = str;
        qQZone.appKey = str2;
        QQZone qQZone2 = (QQZone) configs.get(a.QQ);
        qQZone2.appId = str;
        qQZone2.appKey = str2;
        QQZone qQZone3 = (QQZone) configs.get(a.TENCENT);
        qQZone3.appId = str;
        qQZone3.appKey = str2;
    }

    public static void setTwitter(String str, String str2) {
        Twitter twitter = (Twitter) configs.get(a.TWITTER);
        twitter.appKey = str;
        twitter.appSecret = str2;
    }

    public static void setAlipay(String str) {
        ((Alipay) configs.get(a.ALIPAY)).id = str;
    }

    public static void setTencentWB(String str, String str2) {
        TencentWeibo tencentWeibo = (TencentWeibo) configs.get(a.TENCENT);
        tencentWeibo.appKey = str;
        tencentWeibo.appSecret = str2;
    }

    public static void setSinaWeibo(String str, String str2) {
        SinaWeibo sinaWeibo = (SinaWeibo) configs.get(a.SINA);
        sinaWeibo.appKey = str;
        sinaWeibo.appSecret = str2;
    }

    public static void setTencentWeibo(String str, String str2) {
        TencentWeibo tencentWeibo = (TencentWeibo) configs.get(a.TENCENT);
        tencentWeibo.appKey = str;
        tencentWeibo.appSecret = str2;
    }

    private void a(String str, String str2, String str3) {
        Renren renren = (Renren) configs.get(a.RENREN);
        renren.appId = str;
        renren.appkey = str2;
        renren.appSecret = str3;
    }

    private static void a(String str, String str2) {
        Douban douban = (Douban) configs.get(a.DOUBAN);
        douban.appKey = str;
        douban.appSecret = str2;
    }

    public static void setWeixin(String str, String str2) {
        Weixin weixin = (Weixin) configs.get(a.WEIXIN);
        weixin.appId = str;
        weixin.appSecret = str2;
        Weixin weixin2 = (Weixin) configs.get(a.WEIXIN_CIRCLE);
        weixin2.appId = str;
        weixin2.appSecret = str2;
        Weixin weixin3 = (Weixin) configs.get(a.WEIXIN_FAVORITE);
        weixin3.appId = str;
        weixin3.appSecret = str2;
    }

    public static void setLaiwang(String str, String str2) {
        Laiwang laiwang = (Laiwang) configs.get(a.LAIWANG);
        laiwang.appToken = str;
        laiwang.appSecret = str2;
        Laiwang laiwang2 = (Laiwang) configs.get(a.LAIWANG_DYNAMIC);
        laiwang2.appToken = str;
        laiwang2.appSecret = str2;
    }

    public static void setYixin(String str) {
        ((Yixin) configs.get(a.YIXIN)).yixinId = str;
        ((Yixin) configs.get(a.YIXIN_CIRCLE)).yixinId = str;
    }

    public static void setPinterest(String str) {
        ((Pinterest) configs.get(a.PINTEREST)).appId = str;
    }

    public static Platform getPlatform(a aVar) {
        return configs.get(aVar);
    }

    /* JADX WARNING: Removed duplicated region for block: B:3:0x0010  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void updateConfig(android.content.Context r2) {
        /*
            java.util.Map<com.umeng.socialize.c.a, com.umeng.socialize.PlatformConfig$Platform> r0 = com.umeng.socialize.PlatformConfig.configs
            java.util.Collection r0 = r0.values()
            java.util.Iterator r1 = r0.iterator()
        L_0x000a:
            boolean r0 = r1.hasNext()
            if (r0 == 0) goto L_0x001c
            java.lang.Object r0 = r1.next()
            com.umeng.socialize.PlatformConfig$Platform r0 = (com.umeng.socialize.PlatformConfig.Platform) r0
            boolean r0 = r0.isConfigured()
            if (r0 != 0) goto L_0x000a
        L_0x001c:
            boolean r0 = a(r2)
            if (r0 == 0) goto L_0x0023
        L_0x0022:
            return
        L_0x0023:
            boolean r0 = b(r2)
            if (r0 == 0) goto L_0x0022
            goto L_0x0022
        */
        throw new UnsupportedOperationException("Method not decompiled: com.umeng.socialize.PlatformConfig.updateConfig(android.content.Context):void");
    }

    private static boolean a(Context context) {
        return false;
    }

    private static boolean b(Context context) {
        d a2 = g.a(new c(context));
        if (a2 == null || !a2.b()) {
            return false;
        }
        JSONObject c = a2.c();
        try {
            for (Map.Entry next : configs.entrySet()) {
                ((Platform) next.getValue()).parse(c.getJSONObject(((a) next.getKey()).toString()));
            }
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public static class QQZone implements Platform {
        public String appId = null;
        public String appKey = null;
        private final a media;

        public QQZone(a aVar) {
            this.media = aVar;
        }

        public a getName() {
            return this.media;
        }

        public void parse(JSONObject jSONObject) {
            this.appId = jSONObject.optString("key");
            this.appKey = jSONObject.optString("secret");
        }

        public boolean isConfigured() {
            return !TextUtils.isEmpty(this.appId) && !TextUtils.isEmpty(this.appKey);
        }

        public boolean isAuthrized() {
            return false;
        }
    }

    public static class Twitter implements Platform {
        public String appKey = null;
        public String appSecret = null;
        private final a media;

        public Twitter(a aVar) {
            this.media = aVar;
        }

        public a getName() {
            return this.media;
        }

        public void parse(JSONObject jSONObject) {
            this.appKey = jSONObject.optString("key");
            this.appSecret = jSONObject.optString("secret");
        }

        public boolean isConfigured() {
            return !TextUtils.isEmpty(this.appSecret) && !TextUtils.isEmpty(this.appKey);
        }

        public boolean isAuthrized() {
            return false;
        }
    }

    public static class SinaWeibo implements Platform {
        public String appKey = null;
        public String appSecret = null;

        public a getName() {
            return a.SINA;
        }

        public void parse(JSONObject jSONObject) {
            this.appKey = jSONObject.optString("key");
            this.appSecret = jSONObject.optString("secret");
        }

        public boolean isConfigured() {
            return !TextUtils.isEmpty(this.appKey) && !TextUtils.isEmpty(this.appSecret);
        }

        public boolean isAuthrized() {
            return false;
        }
    }

    public static class TencentWeibo implements Platform {
        public static final String Name = "tencent";
        public String appKey = null;
        public String appSecret = null;

        public a getName() {
            return a.TENCENT;
        }

        public void parse(JSONObject jSONObject) {
            this.appKey = jSONObject.optString("key");
            this.appSecret = jSONObject.optString("secret");
        }

        public boolean isConfigured() {
            return true;
        }

        public boolean isAuthrized() {
            return false;
        }
    }

    public static class Alipay implements Platform {
        public static final String Name = "alipay";
        public String id = null;

        public a getName() {
            return a.ALIPAY;
        }

        public void parse(JSONObject jSONObject) {
            this.id = jSONObject.optString("id");
        }

        public boolean isConfigured() {
            return !TextUtils.isEmpty(this.id);
        }

        public boolean isAuthrized() {
            return false;
        }
    }

    public static class Renren implements Platform {
        public static final String Name = "renren";
        public String appId = "201874";
        public String appSecret = "3bf66e42db1e4fa9829b955cc300b737";
        public String appkey = "28401c0964f04a72a14c812d6132fcef";

        public a getName() {
            return a.RENREN;
        }

        public void parse(JSONObject jSONObject) {
            this.appId = jSONObject.optString("id");
            this.appkey = jSONObject.optString("key");
            this.appSecret = jSONObject.optString("secret");
        }

        public boolean isConfigured() {
            boolean z;
            boolean z2;
            boolean z3;
            if (!TextUtils.isEmpty(this.appkey)) {
                z = true;
            } else {
                z = false;
            }
            if (!TextUtils.isEmpty(this.appSecret)) {
                z2 = true;
            } else {
                z2 = false;
            }
            if (!TextUtils.isEmpty(this.appId)) {
                z3 = true;
            } else {
                z3 = false;
            }
            if (!z || !z2 || !z3) {
                return false;
            }
            return true;
        }

        public boolean isAuthrized() {
            return false;
        }
    }

    public static class Facebook implements Platform {
        public a getName() {
            return a.FACEBOOK;
        }

        public void parse(JSONObject jSONObject) {
        }

        public boolean isConfigured() {
            return false;
        }

        public boolean isAuthrized() {
            return false;
        }
    }

    public static class Douban implements Platform {
        public String appKey = null;
        public String appSecret = null;

        public a getName() {
            return a.DOUBAN;
        }

        public void parse(JSONObject jSONObject) {
            this.appKey = jSONObject.optString("key");
            this.appSecret = jSONObject.optString("secret");
        }

        public boolean isConfigured() {
            return true;
        }

        public boolean isAuthrized() {
            return false;
        }
    }

    public static class Weixin implements Platform {
        public String appId = null;
        public String appSecret = null;
        private final a media;

        public a getName() {
            return this.media;
        }

        public Weixin(a aVar) {
            this.media = aVar;
        }

        public void parse(JSONObject jSONObject) {
        }

        public boolean isConfigured() {
            return !TextUtils.isEmpty(this.appId) && !TextUtils.isEmpty(this.appSecret);
        }

        public boolean isAuthrized() {
            return false;
        }
    }

    public static class Pinterest implements Platform {
        public String appId = null;

        public a getName() {
            return a.PINTEREST;
        }

        public void parse(JSONObject jSONObject) {
        }

        public boolean isConfigured() {
            return !TextUtils.isEmpty(this.appId);
        }

        public boolean isAuthrized() {
            return false;
        }
    }

    public static class Laiwang implements Platform {
        public String appSecret = null;
        public String appToken = null;
        private final a media;

        public Laiwang(a aVar) {
            this.media = aVar;
        }

        public a getName() {
            return this.media;
        }

        public void parse(JSONObject jSONObject) {
        }

        public boolean isConfigured() {
            return !TextUtils.isEmpty(this.appToken) || !TextUtils.isEmpty(this.appSecret);
        }

        public boolean isAuthrized() {
            return false;
        }
    }

    public static class Yixin implements Platform {
        private final a media;
        public String yixinId = null;

        public Yixin(a aVar) {
            this.media = aVar;
        }

        public a getName() {
            return this.media;
        }

        public void parse(JSONObject jSONObject) {
        }

        public boolean isConfigured() {
            return !TextUtils.isEmpty(this.yixinId);
        }

        public boolean isAuthrized() {
            return false;
        }
    }

    public static class GooglePlus implements Platform {
        public static final String Name = "g+";
        public String appId = null;
        public String appSecret = null;
        public String appkey = null;

        public a getName() {
            return a.GOOGLEPLUS;
        }

        public void parse(JSONObject jSONObject) {
        }

        public boolean isConfigured() {
            return true;
        }

        public boolean isAuthrized() {
            return false;
        }
    }

    public static class CustomPlatform implements Platform {
        public static final String Name = "g+";
        public String appId = null;
        public String appSecret = null;
        public String appkey = null;
        private a p;

        public CustomPlatform(a aVar) {
            this.p = aVar;
        }

        public a getName() {
            return this.p;
        }

        public void parse(JSONObject jSONObject) {
        }

        public boolean isConfigured() {
            return true;
        }

        public boolean isAuthrized() {
            return false;
        }
    }
}
