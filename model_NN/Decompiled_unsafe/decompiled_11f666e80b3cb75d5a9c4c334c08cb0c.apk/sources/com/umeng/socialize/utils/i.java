package com.umeng.socialize.utils;

import android.content.Context;
import android.os.Build;
import com.umeng.socialize.d.b.a;

/* compiled from: URLBuilder */
public class i {

    /* renamed from: a  reason: collision with root package name */
    private String f2887a = "0";
    private String b = null;
    private String c = null;
    private String d = null;
    private String e = null;
    private String f = null;
    private String g = null;
    private String h = null;
    private String i = null;
    private String j = null;
    private String k = null;
    private String l = null;
    private String m = null;
    private String n = null;
    private String o = null;
    private String p = null;
    private String q = null;
    private String r = null;

    public i(Context context) {
        this.j = d.a(context);
        if (this.j != null) {
            this.k = a.c(this.j);
        }
        this.l = d.f(context);
        this.m = d.b(context)[0];
        this.n = Build.MODEL;
        this.o = "5.1.4";
        this.p = "Android";
        this.q = String.valueOf(System.currentTimeMillis());
        this.r = "2.0";
    }

    public i a(String str) {
        this.b = str;
        return this;
    }

    public i b(String str) {
        this.c = str;
        return this;
    }

    public i c(String str) {
        this.d = str;
        return this;
    }

    public i d(String str) {
        this.e = str;
        return this;
    }

    public i a(com.umeng.socialize.c.a aVar) {
        this.i = aVar.toString();
        return this;
    }

    public i e(String str) {
        this.f = str;
        return this;
    }

    public i f(String str) {
        this.h = str;
        return this;
    }

    public i g(String str) {
        this.g = str;
        return this;
    }

    public String a() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.b);
        sb.append(this.c);
        sb.append(this.d);
        sb.append("/");
        sb.append(this.e);
        sb.append("/?");
        String b2 = b();
        g.a("base url: " + sb.toString());
        g.a("params: " + b2);
        a.a(this.d);
        try {
            String a2 = a.a(b2, "UTF-8");
            sb.append("ud_get=");
            sb.append(a2);
        } catch (Exception e2) {
            g.e("fail to encrypt query string");
            sb.append(b2);
        }
        return sb.toString();
    }

    private String b() {
        StringBuilder sb = new StringBuilder();
        sb.append("via=").append(this.i.toLowerCase());
        sb.append("&opid=").append(this.f);
        sb.append("&ak=").append(this.d);
        sb.append("&pcv=").append(this.r);
        sb.append("&tp=").append(this.f2887a);
        if (this.j != null) {
            sb.append("&imei=").append(this.j);
        }
        if (this.k != null) {
            sb.append("&md5imei=").append(this.k);
        }
        if (this.l != null) {
            sb.append("&mac=").append(this.l);
        }
        if (this.m != null) {
            sb.append("&en=").append(this.m);
        }
        if (this.n != null) {
            sb.append("&de=").append(this.n);
        }
        if (this.o != null) {
            sb.append("&sdkv=").append(this.o);
        }
        if (this.p != null) {
            sb.append("&os=").append(this.p);
        }
        if (this.q != null) {
            sb.append("&dt=").append(this.q);
        }
        if (this.g != null) {
            sb.append("&uid=").append(this.g);
        }
        if (this.e != null) {
            sb.append("&ek=").append(this.e);
        }
        if (this.h != null) {
            sb.append("&sid=").append(this.h);
        }
        return sb.toString();
    }
}
