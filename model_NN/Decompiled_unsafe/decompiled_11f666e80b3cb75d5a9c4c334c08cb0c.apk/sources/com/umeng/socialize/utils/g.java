package com.umeng.socialize.utils;

import android.util.Log;

/* compiled from: Log */
public class g {

    /* renamed from: a  reason: collision with root package name */
    public static boolean f2885a = true;

    public static void a(String str, String str2) {
        if (f2885a) {
            Log.i(str, str2);
        }
    }

    public static void a(String str, String str2, Exception exc) {
        if (f2885a) {
            Log.i(str, exc.toString() + ":  [" + str2 + "]");
        }
    }

    public static void b(String str, String str2) {
        if (f2885a) {
            Log.e(str, str2);
        }
    }

    public static void b(String str, String str2, Exception exc) {
        Log.e(str, exc.toString() + ":  [" + str2 + "]");
        StackTraceElement[] stackTrace = exc.getStackTrace();
        int length = stackTrace.length;
        for (int i = 0; i < length; i++) {
            Log.e(str, "        at\t " + stackTrace[i].toString());
        }
    }

    public static void c(String str, String str2) {
        if (f2885a) {
            Log.d(str, str2);
        }
    }

    public static void d(String str, String str2) {
        if (f2885a) {
            Log.v(str, str2);
        }
    }

    public static void e(String str, String str2) {
        if (f2885a) {
            Log.w(str, str2);
        }
    }

    public static void c(String str, String str2, Exception exc) {
        if (f2885a) {
            Log.w(str, exc.toString() + ":  [" + str2 + "]");
            StackTraceElement[] stackTrace = exc.getStackTrace();
            int length = stackTrace.length;
            for (int i = 0; i < length; i++) {
                Log.w(str, "        at\t " + stackTrace[i].toString());
            }
        }
    }

    public static void a(String str) {
        if (f2885a) {
            Log.i("umengsocial", str);
        }
    }

    public static void b(String str) {
        if (f2885a) {
            Log.e("umengsocial", str);
        }
    }

    public static void c(String str) {
        if (f2885a) {
            Log.d("umengsocial", str);
        }
    }

    public static void d(String str) {
        if (f2885a) {
            Log.v("umengsocial", str);
        }
    }

    public static void e(String str) {
        if (f2885a) {
            Log.w("umengsocial", str);
        }
    }
}
