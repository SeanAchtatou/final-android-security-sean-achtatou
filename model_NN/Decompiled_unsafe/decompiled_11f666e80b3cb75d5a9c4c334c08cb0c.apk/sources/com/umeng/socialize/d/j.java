package com.umeng.socialize.d;

import android.content.Context;
import com.umeng.socialize.d.a.b;
import com.umeng.socialize.utils.h;

/* compiled from: ShareMultiFollowRequest */
public class j extends b {
    private String g;
    private String h;
    private String i;

    public j(Context context, String str, String str2, String str3) {
        super(context, "", k.class, 18, b.C0069b.POST);
        this.b = context;
        this.g = str;
        this.h = str2;
        this.i = str3;
    }

    public void a() {
        super.a();
        a("to", this.g);
        a("fusid", this.i);
    }

    /* access modifiers changed from: protected */
    public String b() {
        StringBuilder sb = new StringBuilder();
        sb.append("/share/follow/").append(h.a(this.b)).append("/").append(this.h).append("/");
        return sb.toString();
    }
}
