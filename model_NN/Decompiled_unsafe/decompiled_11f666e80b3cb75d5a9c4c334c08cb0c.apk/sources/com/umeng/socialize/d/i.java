package com.umeng.socialize.d;

import android.text.TextUtils;
import com.sina.weibo.sdk.register.mobile.SelectCountryActivity;
import com.umeng.socialize.c.b;
import com.umeng.socialize.d.a.f;
import com.umeng.socialize.utils.g;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.json.JSONObject;

/* compiled from: ShareFriendsResponse */
public class i extends f {

    /* renamed from: a  reason: collision with root package name */
    public List<b> f2805a;

    public i(JSONObject jSONObject) {
        super(jSONObject);
    }

    public void a() {
        JSONObject jSONObject = this.j;
        if (jSONObject == null) {
            g.b("SocializeReseponse", "data json is null....");
            return;
        }
        this.f2805a = new ArrayList();
        Iterator<String> keys = jSONObject.keys();
        while (keys.hasNext()) {
            try {
                String obj = keys.next().toString();
                JSONObject jSONObject2 = (JSONObject) this.j.get(obj);
                if (jSONObject2.has(SelectCountryActivity.EXTRA_COUNTRY_NAME)) {
                    String string = jSONObject2.getString(SelectCountryActivity.EXTRA_COUNTRY_NAME);
                    if (!TextUtils.isEmpty(obj) && !TextUtils.isEmpty(string)) {
                        b bVar = new b();
                        bVar.b(obj);
                        bVar.c(string);
                        String optString = jSONObject2.optString("link_name", "");
                        if (!TextUtils.isEmpty(optString)) {
                            string = optString;
                        }
                        bVar.a(string);
                        String optString2 = jSONObject2.optString("pinyin", "");
                        if (!TextUtils.isEmpty(optString2)) {
                            b.a aVar = new b.a();
                            aVar.b = String.valueOf(a(optString2.charAt(0)));
                            aVar.f2780a = optString2;
                            bVar.a(aVar);
                        }
                        if (jSONObject2.has("profile_image_url")) {
                            bVar.d(jSONObject2.getString("profile_image_url"));
                        }
                        this.f2805a.add(bVar);
                    }
                }
            } catch (Exception e) {
                g.b("SocializeReseponse", "Parse friend data error", e);
            }
        }
    }

    public static char a(char c) {
        if (c < 'a' || c > 'z') {
            return c;
        }
        return (char) (c - ' ');
    }
}
