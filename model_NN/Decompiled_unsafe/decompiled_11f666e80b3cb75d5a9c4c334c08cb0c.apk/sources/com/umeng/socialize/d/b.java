package com.umeng.socialize.d;

import com.baidu.mobads.interfaces.IXAdRequestInfo;
import com.umeng.socialize.d.a.f;
import com.umeng.socialize.utils.g;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: ActionBarResponse */
public class b extends f {

    /* renamed from: a  reason: collision with root package name */
    public int f2796a;
    public int b;
    public int c;
    public String d;
    public String e;
    public int f;
    public int g;
    public String h;
    public int i;

    public b(JSONObject jSONObject) {
        super(jSONObject);
    }

    public void a() {
        JSONObject jSONObject = this.j;
        if (jSONObject == null) {
            g.b("SocializeReseponse", "data json is null....");
            return;
        }
        try {
            if (jSONObject.has(IXAdRequestInfo.MAX_CONTENT_LENGTH)) {
                this.b = jSONObject.getInt(IXAdRequestInfo.MAX_CONTENT_LENGTH);
            }
            if (jSONObject.has("ek")) {
                this.e = jSONObject.getString("ek");
            }
            if (jSONObject.has("ft")) {
                this.f = jSONObject.getInt("ft");
            }
            if (jSONObject.has("fr")) {
                this.g = jSONObject.optInt("fr", 0);
            }
            if (jSONObject.has("lk")) {
                this.c = jSONObject.getInt("lk");
            }
            if (jSONObject.has("pv")) {
                this.f2796a = jSONObject.getInt("pv");
            }
            if (jSONObject.has("sid")) {
                this.d = jSONObject.getString("sid");
            }
            if (jSONObject.has("uid")) {
                this.h = jSONObject.getString("uid");
            }
            if (jSONObject.has(IXAdRequestInfo.SN)) {
                this.i = jSONObject.getInt(IXAdRequestInfo.SN);
            }
        } catch (JSONException e2) {
            g.b("SocializeReseponse", "Parse json error[ " + jSONObject.toString() + " ]", e2);
        }
    }
}
