package com.umeng.socialize.d.b;

/* compiled from: BaseNCodec */
public abstract class c {

    /* renamed from: a  reason: collision with root package name */
    private final int f2799a;
    protected final int b;
    protected byte[] c;
    protected int d;
    protected boolean e;
    protected int f;
    protected int g;
    private final int h;
    private final int i;
    private int j;

    /* access modifiers changed from: package-private */
    public abstract void a(byte[] bArr, int i2, int i3);

    /* access modifiers changed from: protected */
    public abstract boolean a(byte b2);

    /* access modifiers changed from: package-private */
    public abstract void b(byte[] bArr, int i2, int i3);

    protected c(int i2, int i3, int i4, int i5) {
        this.f2799a = i2;
        this.h = i3;
        this.b = (i4 <= 0 || i5 <= 0) ? 0 : (i4 / i3) * i3;
        this.i = i5;
    }

    /* access modifiers changed from: package-private */
    public int a() {
        if (this.c != null) {
            return this.d - this.j;
        }
        return 0;
    }

    /* access modifiers changed from: protected */
    public int b() {
        return 8192;
    }

    private void c() {
        if (this.c == null) {
            this.c = new byte[b()];
            this.d = 0;
            this.j = 0;
            return;
        }
        byte[] bArr = new byte[(this.c.length * 2)];
        System.arraycopy(this.c, 0, bArr, 0, this.c.length);
        this.c = bArr;
    }

    /* access modifiers changed from: protected */
    public void a(int i2) {
        if (this.c == null || this.c.length < this.d + i2) {
            c();
        }
    }

    /* access modifiers changed from: package-private */
    public int c(byte[] bArr, int i2, int i3) {
        if (this.c == null) {
            return this.e ? -1 : 0;
        }
        int min = Math.min(a(), i3);
        System.arraycopy(this.c, this.j, bArr, i2, min);
        this.j += min;
        if (this.j < this.d) {
            return min;
        }
        this.c = null;
        return min;
    }

    private void d() {
        this.c = null;
        this.d = 0;
        this.j = 0;
        this.f = 0;
        this.g = 0;
        this.e = false;
    }

    public byte[] b(String str) {
        return b(a.b(str));
    }

    public byte[] b(byte[] bArr) {
        d();
        if (bArr == null || bArr.length == 0) {
            return bArr;
        }
        b(bArr, 0, bArr.length);
        b(bArr, 0, -1);
        byte[] bArr2 = new byte[this.d];
        c(bArr2, 0, bArr2.length);
        return bArr2;
    }

    public byte[] c(byte[] bArr) {
        d();
        if (bArr == null || bArr.length == 0) {
            return bArr;
        }
        a(bArr, 0, bArr.length);
        a(bArr, 0, -1);
        byte[] bArr2 = new byte[(this.d - this.j)];
        c(bArr2, 0, bArr2.length);
        return bArr2;
    }

    /* access modifiers changed from: protected */
    public boolean d(byte[] bArr) {
        if (bArr == null) {
            return false;
        }
        for (int i2 = 0; i2 < bArr.length; i2++) {
            if (61 == bArr[i2] || a(bArr[i2])) {
                return true;
            }
        }
        return false;
    }

    public long e(byte[] bArr) {
        long length = ((long) (((bArr.length + this.f2799a) - 1) / this.f2799a)) * ((long) this.h);
        if (this.b > 0) {
            return length + ((((((long) this.b) + length) - 1) / ((long) this.b)) * ((long) this.i));
        }
        return length;
    }
}
