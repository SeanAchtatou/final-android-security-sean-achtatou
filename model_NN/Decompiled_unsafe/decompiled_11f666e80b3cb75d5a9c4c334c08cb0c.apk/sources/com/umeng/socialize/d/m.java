package com.umeng.socialize.d;

import android.content.Context;
import com.umeng.socialize.d.a.b;
import com.umeng.socialize.media.UMediaObject;
import com.umeng.socialize.utils.h;

/* compiled from: UploadImageRequest */
public class m extends b {
    private Context g;
    private String h;
    private UMediaObject i;

    public m(Context context, UMediaObject uMediaObject, String str) {
        super(context, "", n.class, 23, b.C0069b.POST);
        this.g = context;
        this.h = str;
        this.i = uMediaObject;
    }

    /* access modifiers changed from: protected */
    public String b() {
        return "/api/upload_pic/" + h.a(this.g) + "/";
    }

    public void a() {
        a("usid", this.h);
        b(this.i);
    }
}
