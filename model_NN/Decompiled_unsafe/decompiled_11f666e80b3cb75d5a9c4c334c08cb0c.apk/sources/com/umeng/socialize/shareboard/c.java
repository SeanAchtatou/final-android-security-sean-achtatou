package com.umeng.socialize.shareboard;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.view.ViewCompat;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.RelativeLayout;
import com.umeng.socialize.Config;
import com.umeng.socialize.shareboard.b.a;
import com.umeng.socialize.shareboard.b.b;
import com.umeng.socialize.utils.g;

/* compiled from: UMActionBoard */
public class c extends RelativeLayout {

    /* renamed from: a  reason: collision with root package name */
    private Context f2880a;
    private b b;
    private Animation c;
    private View d;

    public c(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    public c(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public c(Context context) {
        super(context);
        this.f2880a = context;
        a();
    }

    private void a() {
        int i = 12;
        this.b = new b(this.f2880a);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -1);
        layoutParams.addRule(Config.showShareBoardOnTop ? 10 : 12);
        this.b.setLayoutParams(layoutParams);
        this.c = new TranslateAnimation(0.0f, 0.0f, 80.0f, 0.0f);
        this.c.setDuration(150);
        this.d = new View(this.f2880a);
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-1, -1);
        if (!Config.showShareBoardOnTop) {
            i = 10;
        }
        layoutParams2.addRule(i);
        this.d.setLayoutParams(layoutParams2);
        this.d.setBackgroundColor(Color.argb(50, 0, 0, 0));
        AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
        alphaAnimation.setDuration(1500);
        this.d.setAnimation(alphaAnimation);
        addView(this.d);
        addView(this.b);
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
        int size = View.MeasureSpec.getSize(i);
        int size2 = View.MeasureSpec.getSize(i2);
        g.c("onMeasure", "ActionBoard, width = " + size + ", height = " + size2);
        ViewGroup.LayoutParams layoutParams = this.b.getLayoutParams();
        layoutParams.height = this.b.b(size);
        layoutParams.width = size;
        ViewGroup.LayoutParams layoutParams2 = this.d.getLayoutParams();
        layoutParams2.height = size2 - layoutParams.height;
        layoutParams2.width = size;
    }

    public void a(a aVar) {
        this.b.a(aVar);
        this.b.setBackgroundColor(ViewCompat.MEASURED_STATE_MASK);
    }

    public void a(View.OnClickListener onClickListener) {
        this.d.setOnClickListener(onClickListener);
    }
}
