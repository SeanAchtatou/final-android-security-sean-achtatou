package com.umeng.socialize.shareboard.a;

import android.view.View;
import android.widget.Toast;
import com.umeng.socialize.shareboard.a;
import com.umeng.socialize.utils.d;

/* compiled from: SNSPlatformAdapter */
class b implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ a f2876a;
    final /* synthetic */ a b;

    b(a aVar, a aVar2) {
        this.b = aVar;
        this.f2876a = aVar2;
    }

    public void onClick(View view) {
        this.b.c.dismiss();
        com.umeng.socialize.c.a aVar = this.f2876a.f;
        if (d.d(this.b.b) || aVar == com.umeng.socialize.c.a.SMS) {
            this.b.a(this.f2876a, aVar);
        } else {
            Toast.makeText(this.b.b, "您的网络不可用,请检查网络连接...", 0).show();
        }
    }
}
