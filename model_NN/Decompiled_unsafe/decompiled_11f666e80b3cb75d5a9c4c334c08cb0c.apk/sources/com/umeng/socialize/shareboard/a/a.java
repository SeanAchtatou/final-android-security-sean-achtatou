package com.umeng.socialize.shareboard.a;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.umeng.socialize.common.g;
import com.umeng.socialize.shareboard.b;
import java.util.ArrayList;
import java.util.List;

/* compiled from: SNSPlatformAdapter */
public class a extends com.umeng.socialize.shareboard.b.a {

    /* renamed from: a  reason: collision with root package name */
    private List<com.umeng.socialize.shareboard.a> f2875a = new ArrayList();
    /* access modifiers changed from: private */
    public Context b;
    /* access modifiers changed from: private */
    public b c;

    public a(Context context, List<com.umeng.socialize.shareboard.a> list, b bVar) {
        this.f2875a = list;
        this.b = context;
        this.c = bVar;
    }

    private void a(View view, com.umeng.socialize.shareboard.a aVar) {
        ((ImageView) view.findViewById(g.a(this.b, "id", "umeng_socialize_shareboard_image"))).setImageResource(g.a(this.b, "drawable", aVar.c));
        ((TextView) view.findViewById(g.a(this.b, "id", "umeng_socialize_shareboard_pltform_name"))).setText(g.a(this.b, aVar.b));
    }

    /* access modifiers changed from: private */
    public void a(com.umeng.socialize.shareboard.a aVar, com.umeng.socialize.c.a aVar2) {
        if (aVar != null && this.c.a() != null) {
            this.c.a().a(aVar, aVar2);
        }
    }

    public int a() {
        if (this.f2875a == null) {
            return 0;
        }
        return this.f2875a.size();
    }

    public View a(int i, ViewGroup viewGroup) {
        com.umeng.socialize.shareboard.a aVar = this.f2875a.get(i);
        View inflate = View.inflate(this.b, g.a(this.b, "layout", "umeng_socialize_shareboard_item"), null);
        a(inflate, aVar);
        inflate.setOnClickListener(new b(this, aVar));
        inflate.setOnTouchListener(new c(this, inflate));
        inflate.setFocusable(true);
        return inflate;
    }
}
