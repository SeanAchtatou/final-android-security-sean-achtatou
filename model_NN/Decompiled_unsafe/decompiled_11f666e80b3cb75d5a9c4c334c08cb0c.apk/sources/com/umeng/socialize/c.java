package com.umeng.socialize;

import com.umeng.socialize.shareboard.a;
import com.umeng.socialize.utils.ShareBoardlistener;

/* compiled from: ShareAction */
class c implements ShareBoardlistener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ShareAction f2777a;

    c(ShareAction shareAction) {
        this.f2777a = shareAction;
    }

    public void a(a aVar, com.umeng.socialize.c.a aVar2) {
        ShareContent shareContent;
        int indexOf = this.f2777a.g.indexOf(aVar2);
        int size = this.f2777a.i.size();
        if (size != 0) {
            if (indexOf < size) {
                shareContent = (ShareContent) this.f2777a.i.get(indexOf);
            } else {
                shareContent = (ShareContent) this.f2777a.i.get(size - 1);
            }
            ShareContent unused = this.f2777a.f2763a = shareContent;
        }
        int size2 = this.f2777a.j.size();
        if (size2 != 0) {
            if (indexOf < size2) {
                UMShareListener unused2 = this.f2777a.d = (UMShareListener) this.f2777a.j.get(indexOf);
            } else {
                UMShareListener unused3 = this.f2777a.d = (UMShareListener) this.f2777a.j.get(size2 - 1);
            }
        }
        this.f2777a.setPlatform(aVar2);
        this.f2777a.share();
    }
}
