package com.umeng.socialize.c;

import com.tencent.connect.common.Constants;
import com.tencent.mm.sdk.constants.ConstantsAPI;
import com.umeng.socialize.PlatformConfig;

/* compiled from: SHARE_MEDIA */
public enum a {
    GOOGLEPLUS,
    GENERIC,
    SMS,
    EMAIL,
    SINA,
    QZONE,
    QQ,
    RENREN,
    WEIXIN,
    WEIXIN_CIRCLE,
    WEIXIN_FAVORITE,
    TENCENT,
    DOUBAN,
    FACEBOOK,
    TWITTER,
    LAIWANG,
    LAIWANG_DYNAMIC,
    YIXIN,
    YIXIN_CIRCLE,
    INSTAGRAM,
    PINTEREST,
    EVERNOTE,
    POCKET,
    LINKEDIN,
    FOURSQUARE,
    YNOTE,
    WHATSAPP,
    LINE,
    FLICKR,
    TUMBLR,
    ALIPAY,
    KAKAO;

    public static com.umeng.socialize.shareboard.a a(String str, String str2, String str3, String str4, int i) {
        com.umeng.socialize.shareboard.a aVar = new com.umeng.socialize.shareboard.a();
        aVar.b = str;
        aVar.c = str3;
        aVar.d = str4;
        aVar.e = i;
        aVar.f2874a = str2;
        return aVar;
    }

    public com.umeng.socialize.shareboard.a a() {
        com.umeng.socialize.shareboard.a aVar = new com.umeng.socialize.shareboard.a();
        if (toString().equals(Constants.SOURCE_QQ)) {
            aVar.b = "umeng_socialize_text_qq_key";
            aVar.c = "umeng_socialize_qq_on";
            aVar.d = "umeng_socialize_qq_off";
            aVar.e = 0;
            aVar.f2874a = "qq";
        } else if (toString().equals("SMS")) {
            aVar.b = "umeng_socialize_sms";
            aVar.c = "umeng_socialize_sms_on";
            aVar.d = "umeng_socialize_sms_off";
            aVar.e = 1;
            aVar.f2874a = "sms";
        } else if (toString().equals("GOOGLEPLUS")) {
            aVar.b = "umeng_socialize_text_googleplus_key";
            aVar.c = "umeng_socialize_google";
            aVar.d = "umeng_socialize_google";
            aVar.e = 0;
            aVar.f2874a = "gooleplus";
        } else if (!toString().equals("GENERIC")) {
            if (toString().equals("EMAIL")) {
                aVar.b = "umeng_socialize_mail";
                aVar.c = "umeng_socialize_gmail_on";
                aVar.d = "umeng_socialize_gmail_off";
                aVar.e = 2;
                aVar.f2874a = "email";
            } else if (toString().equals("SINA")) {
                aVar.b = "umeng_socialize_sina";
                aVar.c = "umeng_socialize_sina_on";
                aVar.d = "umeng_socialize_sina_off";
                aVar.e = 0;
                aVar.f2874a = "sina";
            } else if (toString().equals("QZONE")) {
                aVar.b = "umeng_socialize_text_qq_zone_key";
                aVar.c = "umeng_socialize_qzone_on";
                aVar.d = "umeng_socialize_qzone_off";
                aVar.e = 0;
                aVar.f2874a = Constants.SOURCE_QZONE;
            } else if (toString().equals("RENREN")) {
                aVar.b = "umeng_socialize_text_renren_key";
                aVar.c = "umeng_socialize_renren_on";
                aVar.d = "umeng_socialize_renren_off";
                aVar.e = 0;
                aVar.f2874a = PlatformConfig.Renren.Name;
            } else if (toString().equals("WEIXIN")) {
                aVar.b = "umeng_socialize_text_weixin_key";
                aVar.c = "umeng_socialize_wechat";
                aVar.d = "umeng_socialize_weichat_gray";
                aVar.e = 0;
                aVar.f2874a = ConstantsAPI.Token.WX_TOKEN_PLATFORMID_VALUE;
            } else if (toString().equals("WEIXIN_CIRCLE")) {
                aVar.b = "umeng_socialize_text_weixin_circle_key";
                aVar.c = "umeng_socialize_wxcircle";
                aVar.d = "umeng_socialize_wxcircle_gray";
                aVar.e = 0;
                aVar.f2874a = "wxcircle";
            } else if (toString().equals("WEIXIN_FAVORITE")) {
                aVar.b = "umeng_socialize_text_weixin_fav_key";
                aVar.c = "wechat_fav";
                aVar.d = "wechat_fav";
                aVar.e = 0;
                aVar.f2874a = "wechatfavorite";
            } else if (toString().equals("TENCENT")) {
                aVar.b = "umeng_socialize_text_tencent_key";
                aVar.c = "umeng_socialize_tx_on";
                aVar.d = "umeng_socialize_tx_off";
                aVar.e = 0;
                aVar.f2874a = PlatformConfig.TencentWeibo.Name;
            } else if (toString().equals("FACEBOOK")) {
                aVar.b = "umeng_socialize_text_facebook_key";
                aVar.c = "umeng_socialize_facebook";
                aVar.d = "umeng_socialize_facebook";
                aVar.e = 0;
                aVar.f2874a = "facebook";
            } else if (toString().equals("YIXIN")) {
                aVar.b = "umeng_socialize_text_yixin_key";
                aVar.c = "umeng_socialize_yixin";
                aVar.d = "umeng_socialize_yixin_gray";
                aVar.e = 0;
                aVar.f2874a = "yinxin";
            } else if (toString().equals("TWITTER")) {
                aVar.b = "umeng_socialize_text_twitter_key";
                aVar.c = "umeng_socialize_twitter";
                aVar.d = "umeng_socialize_twitter";
                aVar.e = 0;
                aVar.f2874a = "twitter";
            } else if (toString().equals("LAIWANG")) {
                aVar.b = "umeng_socialize_text_laiwang_key";
                aVar.c = "umeng_socialize_laiwang";
                aVar.d = "umeng_socialize_laiwang_gray";
                aVar.e = 0;
                aVar.f2874a = "laiwang";
            } else if (toString().equals("LAIWANG_DYNAMIC")) {
                aVar.b = "umeng_socialize_text_laiwangdynamic_key";
                aVar.c = "umeng_socialize_laiwang_dynamic";
                aVar.d = "umeng_socialize_laiwang_dynamic_gray";
                aVar.e = 0;
                aVar.f2874a = "laiwang_dynamic";
            } else if (toString().equals("INSTAGRAM")) {
                aVar.b = "umeng_socialize_text_instagram_key";
                aVar.c = "umeng_socialize_instagram_on";
                aVar.d = "umeng_socialize_instagram_off";
                aVar.e = 0;
                aVar.f2874a = "qq";
            } else if (toString().equals("YIXIN_CIRCLE")) {
                aVar.b = "umeng_socialize_text_yixincircle_key";
                aVar.c = "umeng_socialize_yixin_circle";
                aVar.d = "umeng_socialize_yixin_circle_gray";
                aVar.e = 0;
                aVar.f2874a = "yinxincircle";
            } else if (toString().equals("PINTEREST")) {
                aVar.b = "umeng_socialize_text_pinterest_key";
                aVar.c = "umeng_socialize_pinterest";
                aVar.d = "umeng_socialize_pinterest_gray";
                aVar.e = 0;
                aVar.f2874a = "pinterest";
            } else if (toString().equals("EVERNOTE")) {
                aVar.b = "umeng_socialize_text_evernote_key";
                aVar.c = "umeng_socialize_evernote";
                aVar.d = "umeng_socialize_evernote_gray";
                aVar.e = 0;
                aVar.f2874a = "evernote";
            } else if (toString().equals("POCKET")) {
                aVar.b = "umeng_socialize_text_pocket_key";
                aVar.c = "umeng_socialize_pocket";
                aVar.d = "umeng_socialize_pocket_gray";
                aVar.e = 0;
                aVar.f2874a = "pocket";
            } else if (toString().equals("LINKEDIN")) {
                aVar.b = "umeng_socialize_text_linkedin_key";
                aVar.c = "umeng_socialize_linkedin";
                aVar.d = "umeng_socialize_linkedin_gray";
                aVar.e = 0;
                aVar.f2874a = "linkedin";
            } else if (toString().equals("FOURSQUARE")) {
                aVar.b = "umeng_socialize_text_foursquare_key";
                aVar.c = "umeng_socialize_foursquare";
                aVar.d = "umeng_socialize_foursquare_gray";
                aVar.e = 0;
                aVar.f2874a = "foursquare";
            } else if (toString().equals("YNOTE")) {
                aVar.b = "umeng_socialize_text_ydnote_key";
                aVar.c = "umeng_socialize_ynote";
                aVar.d = "umeng_socialize_ynote_gray";
                aVar.e = 0;
                aVar.f2874a = "ynote";
            } else if (toString().equals("WHATSAPP")) {
                aVar.b = "umeng_socialize_text_whatsapp_key";
                aVar.c = "umeng_socialize_whatsapp";
                aVar.d = "umeng_socialize_whatsapp_gray";
                aVar.e = 0;
                aVar.f2874a = "whatsapp";
            } else if (toString().equals("LINE")) {
                aVar.b = "umeng_socialize_text_line_key";
                aVar.c = "umeng_socialize_line";
                aVar.d = "umeng_socialize_line_gray";
                aVar.e = 0;
                aVar.f2874a = "line";
            } else if (toString().equals("FLICKR")) {
                aVar.b = "umeng_socialize_text_flickr_key";
                aVar.c = "umeng_socialize_flickr";
                aVar.d = "umeng_socialize_flickr_gray";
                aVar.e = 0;
                aVar.f2874a = "flickr";
            } else if (toString().equals("TUMBLR")) {
                aVar.b = "umeng_socialize_text_tumblr_key";
                aVar.c = "umeng_socialize_tumblr";
                aVar.d = "umeng_socialize_tumblr_gray";
                aVar.e = 0;
                aVar.f2874a = "tumblr";
            } else if (toString().equals("KAKAO")) {
                aVar.b = "umeng_socialize_text_kakao_key";
                aVar.c = "umeng_socialize_kakao";
                aVar.d = "umeng_socialize_kakao_gray";
                aVar.e = 0;
                aVar.f2874a = "kakao";
            } else if (toString().equals("DOUBAN")) {
                aVar.b = "umeng_socialize_text_douban_key";
                aVar.c = "umeng_socialize_douban_on";
                aVar.d = "umeng_socialize_douban_off";
                aVar.e = 0;
                aVar.f2874a = "douban";
            } else if (toString().equals("ALIPAY")) {
                aVar.b = "umeng_socialize_text_alipay_key";
                aVar.c = PlatformConfig.Alipay.Name;
                aVar.d = PlatformConfig.Alipay.Name;
                aVar.e = 0;
                aVar.f2874a = PlatformConfig.Alipay.Name;
            }
        }
        aVar.f = this;
        return aVar;
    }

    public String toString() {
        return super.toString();
    }
}
