package com.umeng.socialize;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import cn.banshenggua.aichang.room.message.SocketMessage;
import com.umeng.socialize.common.b;
import com.umeng.socialize.handler.UMSSOHandler;
import com.umeng.socialize.utils.c;
import com.umeng.socialize.utils.g;
import com.umeng.socialize.view.UMFriendListener;

public class UMShareAPI {
    private static UMShareAPI b = null;

    /* renamed from: a  reason: collision with root package name */
    com.umeng.socialize.b.a f2765a;

    private UMShareAPI(Context context) {
        c.a(context);
        if (Build.VERSION.SDK_INT >= 23) {
            g.c("check", "check permission");
            for (String str : new String[]{"android.permission.ACCESS_FINE_LOCATION", "android.permission.CALL_PHONE", "android.permission.READ_LOGS", "android.permission.READ_PHONE_STATE", "android.permission.WRITE_EXTERNAL_STORAGE", "android.permission.SET_DEBUG_APP", "android.permission.SYSTEM_ALERT_WINDOW", "android.permission.GET_ACCOUNTS"}) {
                try {
                    if (((Integer) Class.forName("android.content.Context").getMethod("checkSelfPermission", String.class).invoke(context, str)).intValue() == 0) {
                        g.c("Umeng", "PERMISSION_GRANTED:" + str);
                    } else {
                        g.b("Umeng Error", "PERMISSION_MISSING:" + str);
                    }
                } catch (Exception e) {
                    g.b("Umeng Error", SocketMessage.MSG_ERROR_KEY + e.getMessage());
                }
            }
        }
        this.f2765a = new com.umeng.socialize.b.a(context);
        new a(context).d();
    }

    public static UMShareAPI get(Context context) {
        if (b == null || b.f2765a == null) {
            b = new UMShareAPI(context);
        }
        return b;
    }

    public void doOauthVerify(Activity activity, com.umeng.socialize.c.a aVar, UMAuthListener uMAuthListener) {
        if (activity != null) {
            new e(this, activity, activity, aVar, uMAuthListener).d();
        } else {
            g.c("UMerror", "doOauthVerify activity is null");
        }
    }

    public void deleteOauth(Activity activity, com.umeng.socialize.c.a aVar, UMAuthListener uMAuthListener) {
        if (activity != null) {
            new f(this, activity, activity, aVar, uMAuthListener).d();
        } else {
            g.c("UMerror", "deleteOauth activity is null");
        }
    }

    public void getPlatformInfo(Activity activity, com.umeng.socialize.c.a aVar, UMAuthListener uMAuthListener) {
        if (activity != null) {
            new g(this, activity, activity, aVar, uMAuthListener).d();
        } else {
            g.c("UMerror", "getPlatformInfo activity argument is null");
        }
    }

    public boolean isInstall(Activity activity, com.umeng.socialize.c.a aVar) {
        if (this.f2765a != null) {
            return this.f2765a.a(activity, aVar);
        }
        this.f2765a = new com.umeng.socialize.b.a(activity);
        return this.f2765a.a(activity, aVar);
    }

    public boolean isAuthorize(Activity activity, com.umeng.socialize.c.a aVar) {
        if (this.f2765a != null) {
            return this.f2765a.b(activity, aVar);
        }
        this.f2765a = new com.umeng.socialize.b.a(activity);
        return this.f2765a.b(activity, aVar);
    }

    public void getFriend(Activity activity, com.umeng.socialize.c.a aVar, UMFriendListener uMFriendListener) {
        if (activity != null) {
            new h(this, activity, activity, aVar, uMFriendListener).d();
        } else {
            g.c("UMerror", "getFriend activity is null");
        }
    }

    public void doShare(Activity activity, ShareAction shareAction, UMShareListener uMShareListener) {
        if (activity != null) {
            new i(this, activity, activity, shareAction, uMShareListener).d();
        } else {
            g.c("UMerror", "Share activity is null");
        }
    }

    public void openShare(ShareAction shareAction, UMShareListener uMShareListener) {
    }

    public void onActivityResult(int i, int i2, Intent intent) {
        if (this.f2765a != null) {
            this.f2765a.a(i, i2, intent);
        } else {
            g.d("auth fail", "router=null");
        }
    }

    public UMSSOHandler getHandler(com.umeng.socialize.c.a aVar) {
        if (this.f2765a != null) {
            return this.f2765a.a(aVar);
        }
        return null;
    }

    static class a extends b.C0068b<Void> {

        /* renamed from: a  reason: collision with root package name */
        private Context f2766a;

        public a(Context context) {
            this.f2766a = context;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public Void c() {
            com.umeng.socialize.d.b a2 = com.umeng.socialize.d.g.a(new com.umeng.socialize.d.a(this.f2766a, e()));
            if (a2 != null && a2.b()) {
                b();
                g.a("response: " + a2.k);
                Config.EntityKey = a2.e;
                Config.SessionId = a2.d;
                Config.UID = a2.h;
            }
            g.a("response has error: " + (a2 == null ? "null" : a2.k));
            return null;
        }

        private boolean e() {
            return this.f2766a.getSharedPreferences("umeng_socialize", 0).getBoolean("newinstall", false);
        }

        public void b() {
            SharedPreferences.Editor edit = this.f2766a.getSharedPreferences("umeng_socialize", 0).edit();
            edit.putBoolean("newinstall", true);
            edit.commit();
        }
    }
}
