package com.umeng.socialize;

import android.app.Activity;
import android.content.Context;
import com.umeng.socialize.c.a;
import com.umeng.socialize.common.b;

/* compiled from: UMShareAPI */
class g extends b.a<Void> {
    final /* synthetic */ Activity c;
    final /* synthetic */ a d;
    final /* synthetic */ UMAuthListener e;
    final /* synthetic */ UMShareAPI f;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    g(UMShareAPI uMShareAPI, Context context, Activity activity, a aVar, UMAuthListener uMAuthListener) {
        super(context);
        this.f = uMShareAPI;
        this.c = activity;
        this.d = aVar;
        this.e = uMAuthListener;
    }

    /* access modifiers changed from: protected */
    public Object c() {
        if (this.f.f2765a == null) {
            return null;
        }
        this.f.f2765a.b(this.c, this.d, this.e);
        return null;
    }
}
