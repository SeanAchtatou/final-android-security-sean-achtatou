package com.umeng.socialize.media;

import com.umeng.socialize.ShareContent;

/* compiled from: SimpleShareContent */
public class d {

    /* renamed from: a  reason: collision with root package name */
    private g f2869a;
    private String b;
    private String c;
    private String d;
    private h e;
    private p f;

    public d(ShareContent shareContent) {
        this.b = shareContent.mText;
        this.c = shareContent.mTitle;
        this.d = shareContent.mTargetUrl;
        if (shareContent.mMedia != null && (shareContent.mMedia instanceof g)) {
            this.f2869a = (g) shareContent.mMedia;
        }
    }

    public void a(String str) {
        this.c = str;
    }

    public String f() {
        return this.c;
    }

    public String g() {
        return this.b;
    }

    public g h() {
        return this.f2869a;
    }

    public void b(String str) {
        this.d = str;
    }

    public String i() {
        return this.d;
    }

    public void a(p pVar) {
        this.f = pVar;
    }

    public p j() {
        return this.f;
    }

    public void a(h hVar) {
        this.e = hVar;
    }

    public h k() {
        return this.e;
    }
}
