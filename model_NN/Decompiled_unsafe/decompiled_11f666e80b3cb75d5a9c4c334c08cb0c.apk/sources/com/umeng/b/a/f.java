package com.umeng.b.a;

import com.umeng.b.b;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import org.apache.mina.proxy.handlers.socks.SocksProxyConstants;

/* compiled from: OnlineConfigHelper */
public class f {

    /* renamed from: a  reason: collision with root package name */
    public static final String f2758a = System.getProperty("line.separator");
    private static final String b = f.class.getName();

    public static boolean a(String str) {
        return str == null || str.length() == 0;
    }

    public static String b(String str) {
        try {
            MessageDigest instance = MessageDigest.getInstance("MD5");
            instance.update(str.getBytes());
            byte[] digest = instance.digest();
            StringBuffer stringBuffer = new StringBuffer();
            for (byte b2 : digest) {
                stringBuffer.append(Integer.toHexString(b2 & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD));
            }
            return stringBuffer.toString();
        } catch (NoSuchAlgorithmException e) {
            b.a(b, "getMD5 error", e);
            return "";
        }
    }
}
