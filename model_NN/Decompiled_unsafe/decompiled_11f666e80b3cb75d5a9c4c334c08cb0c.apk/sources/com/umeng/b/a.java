package com.umeng.b;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import com.umeng.b.a.b;
import com.umeng.b.a.c;
import com.umeng.b.a.e;
import com.umeng.b.a.f;
import java.util.Iterator;
import org.json.JSONObject;

/* compiled from: OnlineConfigAgent */
public class a {
    private static a b = null;

    /* renamed from: a  reason: collision with root package name */
    private c f2750a = null;
    /* access modifiers changed from: private */
    public String c;
    /* access modifiers changed from: private */
    public String d;

    private a() {
    }

    public static synchronized a a() {
        a aVar;
        synchronized (a.class) {
            if (b == null) {
                b = new a();
            }
            aVar = b;
        }
        return aVar;
    }

    public void a(Context context) {
        if (context == null) {
            try {
                b.b("OnlineConfigAgent", "unexpected null context in updateOnlineConfig");
            } catch (Exception e) {
                b.b("OnlineConfigAgent", "exception in updateOnlineConfig");
            }
        } else {
            new Thread(new b(context.getApplicationContext())).start();
        }
    }

    /* access modifiers changed from: private */
    public void a(JSONObject jSONObject) {
        if (this.f2750a != null) {
            this.f2750a.a(jSONObject);
        }
    }

    /* access modifiers changed from: private */
    public long b(Context context) {
        return f.a(context).a().getLong("oc_mdf_told", 0);
    }

    /* access modifiers changed from: private */
    public void a(Context context, e eVar) {
        if (eVar.f2760a != null && eVar.f2760a.length() != 0) {
            SharedPreferences.Editor edit = f.a(context).a().edit();
            try {
                JSONObject jSONObject = eVar.f2760a;
                Iterator<String> keys = jSONObject.keys();
                while (keys.hasNext()) {
                    String next = keys.next();
                    edit.putString(next, jSONObject.getString(next));
                }
                edit.commit();
                b.a("OnlineConfigAgent", "get online setting params: " + jSONObject);
            } catch (Exception e) {
                b.c("OnlineConfigAgent", "save online config params", e);
            }
        }
    }

    public String a(Context context, String str) {
        return f.a(context).a().getString(str, "");
    }

    /* compiled from: OnlineConfigAgent */
    public class b extends com.umeng.b.a.a implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        Context f2753a;

        public b(Context context) {
            this.f2753a = context.getApplicationContext();
        }

        public void run() {
            try {
                if (c()) {
                    b();
                }
            } catch (Exception e) {
                a.this.a((JSONObject) null);
                b.c("OnlineConfigAgent", "request online config error", e);
            }
        }

        public boolean a() {
            return true;
        }

        private void b() {
            e eVar = (e) a(new C0066a(this.f2753a), e.class);
            if (eVar == null) {
                a.this.a((JSONObject) null);
                return;
            }
            if (b.f2759a) {
                b.a("OnlineConfigAgent", "response : " + eVar.b);
            }
            if (eVar.b) {
                a.this.a(this.f2753a, eVar);
                a.this.a(eVar.f2760a);
                return;
            }
            a.this.a((JSONObject) null);
        }

        /* JADX WARNING: Removed duplicated region for block: B:25:0x00c4  */
        /* JADX WARNING: Removed duplicated region for block: B:28:0x00d1  */
        /* JADX WARNING: Removed duplicated region for block: B:31:0x00ec  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        private boolean c() {
            /*
                r14 = this;
                r1 = 1
                r12 = 0
                r2 = 0
                com.umeng.b.a r0 = com.umeng.b.a.this
                java.lang.String r0 = r0.c
                boolean r0 = android.text.TextUtils.isEmpty(r0)
                if (r0 == 0) goto L_0x0024
                android.content.Context r0 = r14.f2753a
                java.lang.String r0 = com.umeng.b.a.e.a(r0)
            L_0x0016:
                boolean r0 = android.text.TextUtils.isEmpty(r0)
                if (r0 == 0) goto L_0x002b
                java.lang.String r0 = "OnlineConfigAgent"
                java.lang.String r1 = "Appkey is missing ,Please check AndroidManifest.xml or set appKey"
                com.umeng.b.b.b(r0, r1)
            L_0x0023:
                return r2
            L_0x0024:
                com.umeng.b.a r0 = com.umeng.b.a.this
                java.lang.String r0 = r0.c
                goto L_0x0016
            L_0x002b:
                boolean r0 = com.umeng.b.b.f2759a
                if (r0 == 0) goto L_0x00e9
                android.content.Context r0 = r14.f2753a
                boolean r0 = com.umeng.b.a.e.g(r0)
                if (r0 == 0) goto L_0x00e9
                r3 = r1
            L_0x0038:
                if (r3 != 0) goto L_0x00ee
                android.content.Context r0 = r14.f2753a
                com.umeng.b.f r0 = com.umeng.b.f.a(r0)
                android.content.SharedPreferences r0 = r0.a()
                java.lang.String r4 = "last_test_t"
                long r4 = r0.getLong(r4, r12)
                long r6 = java.lang.System.currentTimeMillis()
                long r4 = r6 - r4
                java.lang.String r8 = "oc_req_i"
                r10 = 600000(0x927c0, double:2.964394E-318)
                long r8 = r0.getLong(r8, r10)
                int r4 = (r4 > r8 ? 1 : (r4 == r8 ? 0 : -1))
                if (r4 <= 0) goto L_0x00ee
                android.content.SharedPreferences$Editor r0 = r0.edit()
                java.lang.String r4 = "last_test_t"
                android.content.SharedPreferences$Editor r0 = r0.putLong(r4, r6)
                r0.commit()
                r0 = r1
            L_0x006b:
                java.lang.String r4 = "OnlineConfigAgent"
                java.lang.StringBuilder r5 = new java.lang.StringBuilder
                r5.<init>()
                java.lang.String r6 = "isDebug="
                java.lang.StringBuilder r5 = r5.append(r6)
                java.lang.StringBuilder r5 = r5.append(r3)
                java.lang.String r6 = ",isReqTimeout="
                java.lang.StringBuilder r5 = r5.append(r6)
                java.lang.StringBuilder r5 = r5.append(r0)
                java.lang.String r5 = r5.toString()
                com.umeng.b.b.b(r4, r5)
                if (r3 != 0) goto L_0x0091
                if (r0 == 0) goto L_0x0023
            L_0x0091:
                com.umeng.b.a$c r0 = new com.umeng.b.a$c
                com.umeng.b.a r3 = com.umeng.b.a.this
                android.content.Context r4 = r14.f2753a
                r0.<init>(r4)
                java.lang.Class<com.umeng.b.a$d> r3 = com.umeng.b.a.d.class
                com.umeng.b.a.c r0 = r14.a(r0, r3)
                com.umeng.b.a$d r0 = (com.umeng.b.a.d) r0
                if (r0 == 0) goto L_0x0023
                android.content.Context r3 = r14.f2753a
                com.umeng.b.f r3 = com.umeng.b.f.a(r3)
                android.content.SharedPreferences r3 = r3.a()
                long r4 = r0.f2755a
                java.lang.String r6 = "oc_mdf_t"
                long r6 = r3.getLong(r6, r12)
                int r4 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
                if (r4 <= 0) goto L_0x00ec
            L_0x00ba:
                android.content.SharedPreferences$Editor r2 = r3.edit()
                long r4 = r0.b
                int r4 = (r4 > r12 ? 1 : (r4 == r12 ? 0 : -1))
                if (r4 < 0) goto L_0x00cb
                java.lang.String r4 = "oc_req_i"
                long r6 = r0.b
                r2.putLong(r4, r6)
            L_0x00cb:
                long r4 = r0.f2755a
                int r4 = (r4 > r12 ? 1 : (r4 == r12 ? 0 : -1))
                if (r4 < 0) goto L_0x00e3
                java.lang.String r4 = "oc_mdf_told"
                java.lang.String r5 = "oc_mdf_t"
                long r6 = r3.getLong(r5, r12)
                r2.putLong(r4, r6)
                java.lang.String r3 = "oc_mdf_t"
                long r4 = r0.f2755a
                r2.putLong(r3, r4)
            L_0x00e3:
                r2.commit()
                r2 = r1
                goto L_0x0023
            L_0x00e9:
                r3 = r2
                goto L_0x0038
            L_0x00ec:
                r1 = r2
                goto L_0x00ba
            L_0x00ee:
                r0 = r2
                goto L_0x006b
            */
            throw new UnsupportedOperationException("Method not decompiled: com.umeng.b.a.b.c():boolean");
        }
    }

    /* renamed from: com.umeng.b.a$a  reason: collision with other inner class name */
    /* compiled from: OnlineConfigAgent */
    class C0066a extends b {
        private final String e = "http://oc.umeng.com/v2/check_config_update";
        private JSONObject f;

        public C0066a(Context context) {
            super(null);
            this.d = "http://oc.umeng.com/v2/check_config_update";
            this.f = a(context);
        }

        public JSONObject a() {
            return this.f;
        }

        public String b() {
            return this.d;
        }

        private JSONObject a(Context context) {
            JSONObject jSONObject = new JSONObject();
            try {
                jSONObject.put("type", "online_config");
                jSONObject.put(LogBuilder.KEY_APPKEY, TextUtils.isEmpty(a.this.c) ? e.a(context) : a.this.c);
                jSONObject.put("version_code", e.b(context));
                jSONObject.put("package", e.f(context));
                jSONObject.put("sdk_version", e.a());
                jSONObject.put("idmd5", f.b(e.d(context)));
                jSONObject.put(LogBuilder.KEY_CHANNEL, TextUtils.isEmpty(a.this.d) ? e.c(context) : a.this.d);
                jSONObject.put("last_config_time", a.this.b(context));
                return jSONObject;
            } catch (Exception e2) {
                b.b("OnlineConfigAgent", "exception in onlineConfigInternal");
                return null;
            }
        }
    }

    /* compiled from: OnlineConfigAgent */
    class c extends b {
        private final String e = "http://oc.umeng.com/v2/get_update_time";
        private JSONObject f;

        public c(Context context) {
            super(null);
            this.d = "http://oc.umeng.com/v2/get_update_time";
            this.f = a(context);
        }

        public JSONObject a() {
            return this.f;
        }

        private JSONObject a(Context context) {
            JSONObject jSONObject = new JSONObject();
            try {
                jSONObject.put(LogBuilder.KEY_APPKEY, TextUtils.isEmpty(a.this.c) ? e.a(context) : a.this.c);
                jSONObject.put("version_code", e.b(context));
                return jSONObject;
            } catch (Exception e2) {
                b.b("OnlineConfigAgent", "exception in onlineConfigInternal");
                return null;
            }
        }

        public String b() {
            return this.d;
        }
    }

    /* compiled from: OnlineConfigAgent */
    public static class d extends c {

        /* renamed from: a  reason: collision with root package name */
        public long f2755a = -1;
        public long b = -1;

        public d(JSONObject jSONObject) {
            super(jSONObject);
            a(jSONObject);
        }

        private void a(JSONObject jSONObject) {
            if (jSONObject != null) {
                try {
                    this.f2755a = jSONObject.optLong("last_config_time", -1);
                    this.b = jSONObject.optLong("oc_interval", -1) * 60 * 1000;
                } catch (Exception e) {
                    b.d("OnlineConfigAgent", "fail to parce online config response", e);
                }
            }
        }
    }
}
