package com.umeng.b;

import android.content.Context;
import android.content.SharedPreferences;

/* compiled from: OnlineConfigStoreHelper */
public class f {

    /* renamed from: a  reason: collision with root package name */
    private static f f2761a = null;
    private static Context b;
    private static String c;

    public f(Context context) {
        b = context.getApplicationContext();
        c = context.getPackageName();
    }

    public static synchronized f a(Context context) {
        f fVar;
        synchronized (f.class) {
            if (f2761a == null) {
                f2761a = new f(context);
            }
            fVar = f2761a;
        }
        return fVar;
    }

    public SharedPreferences a() {
        return b.getSharedPreferences("onlineconfig_agent_online_setting_" + c, 0);
    }
}
