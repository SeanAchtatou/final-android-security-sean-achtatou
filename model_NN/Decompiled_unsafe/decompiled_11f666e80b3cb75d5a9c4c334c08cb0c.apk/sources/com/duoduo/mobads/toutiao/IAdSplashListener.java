package com.duoduo.mobads.toutiao;

public interface IAdSplashListener {
    void afterAdExposure();

    void onAdClicked();

    void onAdExposure();

    void onAdSkiped();

    void onError();
}
