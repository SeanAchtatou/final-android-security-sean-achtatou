package com.duoduo.mobads.gdt;

public interface IGdtSplashAdListener {
    void onADClicked();

    void onADDismissed();

    void onADPresent();

    void onNoAD(int i);
}
