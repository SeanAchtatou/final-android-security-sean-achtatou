package com.duoduo.mobads;

import java.util.List;

public interface IBaiduNativeNetworkListener {
    void onNativeFail(INativeErrorCode iNativeErrorCode);

    void onNativeLoad(List<INativeResponse> list);
}
