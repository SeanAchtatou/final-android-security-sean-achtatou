package com.duoduo.dynamicdex;

import android.os.Handler;

public interface IDuoContext {
    Handler getMainThreadHandler();

    long getMainThreadID();
}
