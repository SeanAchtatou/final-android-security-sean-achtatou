package com.duoduo.dynamicdex.utils;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import com.duoduo.dynamicdex.DuoMobApp;

public class AppSPUtils {
    private static final String PREFS = "_dexmob";
    private static Context mContext = DuoMobApp.Ins.getApp();

    public static String loadPrefString(String str) {
        return loadPrefString(str, null);
    }

    @TargetApi(11)
    private static SharedPreferences getSharedPreferences() {
        if (Build.VERSION.SDK_INT >= 11) {
            return mContext.getSharedPreferences(PREFS, 4);
        }
        return mContext.getSharedPreferences(PREFS, 0);
    }

    public static String loadPrefString(String str, String str2) {
        return getSharedPreferences().getString(str, str2);
    }

    public static boolean savePrefString(String str, String str2) {
        SharedPreferences.Editor edit = getSharedPreferences().edit();
        edit.putString(str, str2);
        return edit.commit();
    }

    public static int loadPrefInt(String str, int i) {
        return getSharedPreferences().getInt(str, i);
    }

    public static boolean savePrefInt(String str, int i) {
        SharedPreferences.Editor edit = getSharedPreferences().edit();
        edit.putInt(str, i);
        return edit.commit();
    }

    public static long loadPrefLong(String str, long j) {
        return getSharedPreferences().getLong(str, j);
    }

    public static boolean savePrefLong(String str, long j) {
        SharedPreferences.Editor edit = getSharedPreferences().edit();
        edit.putLong(str, j);
        return edit.commit();
    }

    public static boolean loadPrefBoolean(String str, boolean z) {
        return getSharedPreferences().getBoolean(str, z);
    }

    public static boolean savePrefBoolean(String str, boolean z) {
        SharedPreferences.Editor edit = getSharedPreferences().edit();
        edit.putBoolean(str, z);
        return edit.commit();
    }

    public static boolean deletePref(String str) {
        SharedPreferences.Editor edit = getSharedPreferences().edit();
        edit.remove(str);
        return edit.commit();
    }
}
