package com.duoduo.dynamicdex.data;

import android.app.Activity;
import android.view.ViewGroup;
import com.duoduo.mobads.toutiao.IAdSplash;
import java.lang.reflect.Constructor;

public class DToutiaoUtils implements IAdUtils {
    private Class<?> mSplashClazz = null;

    public boolean isEmpty() {
        return this.mSplashClazz == null;
    }

    public void load(String str, ClassLoader classLoader) {
        if (classLoader != null) {
            try {
                this.mSplashClazz = classLoader.loadClass("com.duoduo.mobads.wrapper.toutiao.AdSplashWrapper");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public IAdSplash getAdSplash(String str, Activity activity, ViewGroup viewGroup) {
        if (this.mSplashClazz == null) {
            return null;
        }
        try {
            Constructor<?> constructor = this.mSplashClazz.getConstructor(String.class, Activity.class, ViewGroup.class);
            if (constructor != null) {
                return (IAdSplash) constructor.newInstance(str, activity, viewGroup);
            }
        } catch (Exception e) {
        }
        return null;
    }
}
