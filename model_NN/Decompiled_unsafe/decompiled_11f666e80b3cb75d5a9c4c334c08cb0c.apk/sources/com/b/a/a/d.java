package com.b.a.a;

import com.b.a.a.a.ac;
import com.b.a.a.a.ad;
import com.b.a.a.r;
import java.io.IOException;
import java.io.Writer;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

public class d extends i {

    /* renamed from: a  reason: collision with root package name */
    static final Enumeration f383a = new g();
    private static final Integer b = new Integer(1);
    private f c;
    private String d;
    private r.a e;
    private Vector f;
    private final Hashtable g;

    public interface a {
        void a(d dVar);
    }

    public d() {
        this.c = null;
        this.e = r.a();
        this.f = new Vector();
        this.g = null;
        this.d = "MEMORY";
    }

    d(String str) {
        this.c = null;
        this.e = r.a();
        this.f = new Vector();
        this.g = null;
        this.d = str;
    }

    public f a() {
        return this.c;
    }

    /* access modifiers changed from: package-private */
    public v a(ac acVar, boolean z) throws ad {
        if (acVar.b() == z) {
            return new v(this, acVar);
        }
        throw new ad(acVar, new StringBuffer().append("\"").append(acVar).append("\" evaluates to ").append(z ? "evaluates to element not string" : "evaluates to string not element").toString());
    }

    /* access modifiers changed from: package-private */
    public void a(ac acVar) throws ad {
    }

    public void a(f fVar) {
        this.c = fVar;
        this.c.a(this);
        b();
    }

    public void a(Writer writer) throws IOException {
        this.c.a(writer);
    }

    public void a(String str) {
        this.d = str;
        b();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.b.a.a.d.a(com.b.a.a.a.ac, boolean):com.b.a.a.v
     arg types: [com.b.a.a.a.ac, int]
     candidates:
      com.b.a.a.i.a(java.io.Writer, java.lang.String):void
      com.b.a.a.d.a(com.b.a.a.a.ac, boolean):com.b.a.a.v */
    public f b(String str) throws m {
        try {
            if (str.charAt(0) != '/') {
                str = new StringBuffer().append("/").append(str).toString();
            }
            ac a2 = ac.a(str);
            a(a2);
            return a(a2, false).a();
        } catch (ad e2) {
            throw new m("XPath problem", e2);
        }
    }

    /* access modifiers changed from: package-private */
    public void b() {
        Enumeration elements = this.f.elements();
        while (elements.hasMoreElements()) {
            ((a) elements.nextElement()).a(this);
        }
    }

    public void b(Writer writer) throws IOException {
        writer.write("<?xml version=\"1.0\" ?>\n");
        this.c.b(writer);
    }

    /* access modifiers changed from: protected */
    public int c() {
        return this.c.hashCode();
    }

    public Object clone() {
        d dVar = new d(this.d);
        dVar.c = (f) this.c.clone();
        return dVar;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof d)) {
            return false;
        }
        return this.c.equals(((d) obj).c);
    }

    public String toString() {
        return this.d;
    }
}
