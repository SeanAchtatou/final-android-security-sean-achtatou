package com.b.a.a;

import com.b.a.a.a.aa;
import com.b.a.a.a.ab;
import com.b.a.a.a.ac;
import com.b.a.a.a.ad;
import com.b.a.a.a.c;
import com.b.a.a.a.d;
import com.b.a.a.a.f;
import com.b.a.a.a.g;
import com.b.a.a.a.h;
import com.b.a.a.a.j;
import com.b.a.a.a.k;
import com.b.a.a.a.m;
import com.b.a.a.a.q;
import com.b.a.a.a.r;
import com.b.a.a.a.t;
import com.b.a.a.a.w;
import com.b.a.a.a.x;
import com.b.a.a.a.y;
import com.b.a.a.a.z;
import java.util.Enumeration;
import java.util.Vector;

class v implements ab {

    /* renamed from: a  reason: collision with root package name */
    private static final Boolean f393a = new Boolean(true);
    private static final Boolean b = new Boolean(false);
    private final j c;
    private Vector d;
    private Enumeration e;
    private Object f;
    private final a g;
    private i h;
    private boolean i;
    private ac j;

    /* renamed from: com.b.a.a.v$1  reason: invalid class name */
    class AnonymousClass1 {
    }

    private static class a {

        /* renamed from: a  reason: collision with root package name */
        private C0011a f394a;

        /* renamed from: com.b.a.a.v$a$a  reason: collision with other inner class name */
        private static class C0011a {

            /* renamed from: a  reason: collision with root package name */
            final Boolean f395a;
            final C0011a b;

            C0011a(Boolean bool, C0011a aVar) {
                this.f395a = bool;
                this.b = aVar;
            }
        }

        private a() {
            this.f394a = null;
        }

        a(AnonymousClass1 r1) {
            this();
        }

        /* access modifiers changed from: package-private */
        public Boolean a() {
            Boolean bool = this.f394a.f395a;
            this.f394a = this.f394a.b;
            return bool;
        }

        /* access modifiers changed from: package-private */
        public void a(Boolean bool) {
            this.f394a = new C0011a(bool, this.f394a);
        }
    }

    private v(ac acVar, i iVar) throws ad {
        this.c = new j();
        this.d = new Vector();
        this.e = null;
        this.f = null;
        this.g = new a(null);
        this.j = acVar;
        this.h = iVar;
        this.d = new Vector(1);
        this.d.addElement(this.h);
        Enumeration c2 = acVar.c();
        while (c2.hasMoreElements()) {
            t tVar = (t) c2.nextElement();
            this.i = tVar.a();
            this.e = null;
            tVar.c().a(this);
            this.e = this.c.a();
            this.d.removeAllElements();
            k d2 = tVar.d();
            while (this.e.hasMoreElements()) {
                this.f = this.e.nextElement();
                d2.a(this);
                if (this.g.a().booleanValue()) {
                    this.d.addElement(this.f);
                }
            }
        }
    }

    public v(d dVar, ac acVar) throws ad {
        this(acVar, dVar);
    }

    public v(f fVar, ac acVar) throws ad {
        this(acVar, fVar);
        if (acVar.a()) {
            throw new ad(acVar, "Cannot use element as context node for absolute xpath");
        }
    }

    private void a(d dVar) {
        f a2 = dVar.a();
        this.c.a(a2, 1);
        if (this.i) {
            a(a2);
        }
    }

    private void a(d dVar, String str) {
        f a2 = dVar.a();
        if (a2 != null) {
            if (a2.a() == str) {
                this.c.a(a2, 1);
            }
            if (this.i) {
                a(a2, str);
            }
        }
    }

    private void a(f fVar) {
        int i2 = 0;
        for (i d2 = fVar.d(); d2 != null; d2 = d2.i()) {
            if (d2 instanceof f) {
                int i3 = i2 + 1;
                this.c.a(d2, i3);
                if (this.i) {
                    a((f) d2);
                }
                i2 = i3;
            }
        }
    }

    private void a(f fVar, String str) {
        int i2 = 0;
        for (i d2 = fVar.d(); d2 != null; d2 = d2.i()) {
            if (d2 instanceof f) {
                f fVar2 = (f) d2;
                if (fVar2.a() == str) {
                    i2++;
                    this.c.a(fVar2, i2);
                }
                if (this.i) {
                    a(fVar2, str);
                }
            }
        }
    }

    public f a() {
        if (this.d.size() == 0) {
            return null;
        }
        return (f) this.d.elementAt(0);
    }

    public void a(com.b.a.a.a.a aVar) {
        Vector vector = this.d;
        this.c.b();
        Enumeration elements = vector.elements();
        while (elements.hasMoreElements()) {
            Object nextElement = elements.nextElement();
            if (nextElement instanceof f) {
                a((f) nextElement);
            } else if (nextElement instanceof d) {
                a((d) nextElement);
            }
        }
    }

    public void a(aa aaVar) {
        this.g.a(f393a);
    }

    public void a(c cVar) throws ad {
        if (!(this.f instanceof f)) {
            throw new ad(this.j, "Cannot test attribute of document");
        }
        this.g.a(cVar.a().equals(((f) this.f).b(cVar.b())) ? f393a : b);
    }

    public void a(d dVar) throws ad {
        if (!(this.f instanceof f)) {
            throw new ad(this.j, "Cannot test attribute of document");
        }
        String b2 = ((f) this.f).b(dVar.b());
        this.g.a(b2 != null && b2.length() > 0 ? f393a : b);
    }

    public void a(f fVar) throws ad {
        if (!(this.f instanceof f)) {
            throw new ad(this.j, "Cannot test attribute of document");
        }
        this.g.a((((double) Long.parseLong(((f) this.f).b(fVar.b()))) > fVar.a() ? 1 : (((double) Long.parseLong(((f) this.f).b(fVar.b()))) == fVar.a() ? 0 : -1)) > 0 ? f393a : b);
    }

    public void a(g gVar) throws ad {
        if (!(this.f instanceof f)) {
            throw new ad(this.j, "Cannot test attribute of document");
        }
        this.g.a((((double) Long.parseLong(((f) this.f).b(gVar.b()))) > gVar.a() ? 1 : (((double) Long.parseLong(((f) this.f).b(gVar.b()))) == gVar.a() ? 0 : -1)) < 0 ? f393a : b);
    }

    public void a(h hVar) throws ad {
        if (!(this.f instanceof f)) {
            throw new ad(this.j, "Cannot test attribute of document");
        }
        this.g.a(!hVar.a().equals(((f) this.f).b(hVar.b())) ? f393a : b);
    }

    public void a(j jVar) {
        String b2;
        Vector vector = this.d;
        this.c.b();
        Enumeration elements = vector.elements();
        while (elements.hasMoreElements()) {
            i iVar = (i) elements.nextElement();
            if ((iVar instanceof f) && (b2 = ((f) iVar).b(jVar.b())) != null) {
                this.c.a(b2);
            }
        }
    }

    public void a(m mVar) {
        String b2 = mVar.b();
        Vector vector = this.d;
        int size = vector.size();
        this.c.b();
        for (int i2 = 0; i2 < size; i2++) {
            Object elementAt = vector.elementAt(i2);
            if (elementAt instanceof f) {
                a((f) elementAt, b2);
            } else if (elementAt instanceof d) {
                a((d) elementAt, b2);
            }
        }
    }

    public void a(q qVar) throws ad {
        this.c.b();
        f g2 = this.h.g();
        if (g2 == null) {
            throw new ad(this.j, "Illegal attempt to apply \"..\" to node with no parent.");
        }
        this.c.a(g2, 1);
    }

    public void a(r rVar) throws ad {
        if (!(this.f instanceof f)) {
            throw new ad(this.j, "Cannot test position of document");
        }
        this.g.a(this.c.a((f) this.f) == rVar.a() ? f393a : b);
    }

    public void a(com.b.a.a.a.v vVar) throws ad {
        if (!(this.f instanceof f)) {
            throw new ad(this.j, "Cannot test attribute of document");
        }
        i d2 = ((f) this.f).d();
        while (d2 != null) {
            if (!(d2 instanceof u) || !((u) d2).a().equals(vVar.a())) {
                d2 = d2.i();
            } else {
                this.g.a(f393a);
                return;
            }
        }
        this.g.a(b);
    }

    public void a(w wVar) throws ad {
        if (!(this.f instanceof f)) {
            throw new ad(this.j, "Cannot test attribute of document");
        }
        for (i d2 = ((f) this.f).d(); d2 != null; d2 = d2.i()) {
            if (d2 instanceof u) {
                this.g.a(f393a);
                return;
            }
        }
        this.g.a(b);
    }

    public void a(x xVar) throws ad {
        if (!(this.f instanceof f)) {
            throw new ad(this.j, "Cannot test attribute of document");
        }
        i d2 = ((f) this.f).d();
        while (d2 != null) {
            if (!(d2 instanceof u) || ((u) d2).a().equals(xVar.a())) {
                d2 = d2.i();
            } else {
                this.g.a(f393a);
                return;
            }
        }
        this.g.a(b);
    }

    public void a(y yVar) {
        Vector vector = this.d;
        this.c.b();
        Enumeration elements = vector.elements();
        while (elements.hasMoreElements()) {
            Object nextElement = elements.nextElement();
            if (nextElement instanceof f) {
                for (i d2 = ((f) nextElement).d(); d2 != null; d2 = d2.i()) {
                    if (d2 instanceof u) {
                        this.c.a(((u) d2).a());
                    }
                }
            }
        }
    }

    public void a(z zVar) {
        this.c.b();
        this.c.a(this.h, 1);
    }

    public String b() {
        if (this.d.size() == 0) {
            return null;
        }
        return this.d.elementAt(0).toString();
    }
}
