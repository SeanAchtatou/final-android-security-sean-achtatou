package com.b.a.a;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;

public abstract class i {

    /* renamed from: a  reason: collision with root package name */
    private d f386a = null;
    private f b = null;
    private i c = null;
    private i d = null;
    private Object e = null;
    private int f = 0;

    protected static void a(Writer writer, String str) throws IOException {
        String str2;
        int i = 0;
        int length = str.length();
        for (int i2 = 0; i2 < length; i2++) {
            char charAt = str.charAt(i2);
            if (charAt < 128) {
                switch (charAt) {
                    case '\"':
                        str2 = "&quot;";
                        break;
                    case '&':
                        str2 = "&amp;";
                        break;
                    case '\'':
                        str2 = "&#39;";
                        break;
                    case '<':
                        str2 = "&lt;";
                        break;
                    case '>':
                        str2 = "&gt;";
                        break;
                    default:
                        str2 = null;
                        break;
                }
            } else {
                str2 = new StringBuffer().append("&#").append((int) charAt).append(";").toString();
            }
            if (str2 != null) {
                writer.write(str, i, i2 - i);
                writer.write(str2);
                i = i2 + 1;
            }
        }
        if (i < length) {
            writer.write(str, i, length - i);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(d dVar) {
        this.f386a = dVar;
    }

    /* access modifiers changed from: package-private */
    public abstract void a(Writer writer) throws IOException;

    /* access modifiers changed from: package-private */
    public void b() {
        this.f = 0;
        if (this.f386a != null) {
            this.f386a.b();
        }
    }

    /* access modifiers changed from: package-private */
    public void b(f fVar) {
        this.b = fVar;
    }

    /* access modifiers changed from: package-private */
    public abstract void b(Writer writer) throws IOException;

    /* access modifiers changed from: protected */
    public abstract int c();

    public abstract Object clone();

    /* access modifiers changed from: package-private */
    public void d(i iVar) {
        this.c = iVar;
        if (iVar != null) {
            iVar.d = this;
        }
    }

    public d f() {
        return this.f386a;
    }

    public f g() {
        return this.b;
    }

    public i h() {
        return this.c;
    }

    public int hashCode() {
        if (this.f == 0) {
            this.f = c();
        }
        return this.f;
    }

    public i i() {
        return this.d;
    }

    /* access modifiers changed from: package-private */
    public void j() {
        if (this.c != null) {
            this.c.d = this.d;
        }
        if (this.d != null) {
            this.d.c = this.c;
        }
        this.d = null;
        this.c = null;
    }

    public String k() throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(byteArrayOutputStream);
        b(outputStreamWriter);
        outputStreamWriter.flush();
        return new String(byteArrayOutputStream.toByteArray());
    }

    public String toString() {
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(byteArrayOutputStream);
            a(outputStreamWriter);
            outputStreamWriter.flush();
            return new String(byteArrayOutputStream.toByteArray());
        } catch (IOException e2) {
            return super.toString();
        }
    }
}
