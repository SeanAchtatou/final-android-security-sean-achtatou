package com.a.a;

import com.a.a.b.g;
import java.util.Map;
import java.util.Set;

/* compiled from: JsonObject */
public final class l extends i {

    /* renamed from: a  reason: collision with root package name */
    private final g<String, i> f323a = new g<>();

    public void a(String str, i iVar) {
        if (iVar == null) {
            iVar = k.f322a;
        }
        this.f323a.put(str, iVar);
    }

    public Set<Map.Entry<String, i>> o() {
        return this.f323a.entrySet();
    }

    public boolean equals(Object obj) {
        return obj == this || ((obj instanceof l) && ((l) obj).f323a.equals(this.f323a));
    }

    public int hashCode() {
        return this.f323a.hashCode();
    }
}
