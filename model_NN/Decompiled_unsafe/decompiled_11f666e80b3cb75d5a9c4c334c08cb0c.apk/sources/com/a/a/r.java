package com.a.a;

import com.a.a.b.a.f;
import com.a.a.d.a;
import com.a.a.d.b;
import com.a.a.d.c;
import java.io.IOException;

/* compiled from: TypeAdapter */
public abstract class r<T> {
    public abstract void a(c cVar, Object obj) throws IOException;

    public abstract T b(a aVar) throws IOException;

    public final r<T> a() {
        return new r<T>() {
            public void a(c cVar, T t) throws IOException {
                if (t == null) {
                    cVar.f();
                } else {
                    r.this.a(cVar, t);
                }
            }

            public T b(a aVar) throws IOException {
                if (aVar.f() != b.NULL) {
                    return r.this.b(aVar);
                }
                aVar.j();
                return null;
            }
        };
    }

    public final i a(Object obj) {
        try {
            f fVar = new f();
            a(fVar, obj);
            return fVar.a();
        } catch (IOException e) {
            throw new j(e);
        }
    }
}
