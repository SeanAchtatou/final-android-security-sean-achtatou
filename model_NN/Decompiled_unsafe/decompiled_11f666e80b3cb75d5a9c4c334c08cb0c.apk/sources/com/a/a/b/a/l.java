package com.a.a.b.a;

import com.a.a.b.a.i;
import com.a.a.d.a;
import com.a.a.d.c;
import com.a.a.e;
import com.a.a.r;
import java.io.IOException;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;

/* compiled from: TypeAdapterRuntimeTypeWrapper */
final class l<T> extends r<T> {

    /* renamed from: a  reason: collision with root package name */
    private final e f263a;
    private final r<T> b;
    private final Type c;

    l(e eVar, r<T> rVar, Type type) {
        this.f263a = eVar;
        this.b = rVar;
        this.c = type;
    }

    public T b(a aVar) throws IOException {
        return this.b.b(aVar);
    }

    public void a(c cVar, T t) throws IOException {
        r<T> rVar = this.b;
        Type a2 = a(this.c, t);
        if (a2 != this.c) {
            rVar = this.f263a.a((com.a.a.c.a) com.a.a.c.a.a(a2));
            if ((rVar instanceof i.a) && !(this.b instanceof i.a)) {
                rVar = this.b;
            }
        }
        rVar.a(cVar, t);
    }

    private Type a(Type type, Object obj) {
        if (obj == null) {
            return type;
        }
        if (type == Object.class || (type instanceof TypeVariable) || (type instanceof Class)) {
            return obj.getClass();
        }
        return type;
    }
}
