package com.a.a.b;

import com.a.a.b.a.m;
import com.a.a.d.c;
import com.a.a.i;
import java.io.IOException;
import java.io.Writer;

/* compiled from: Streams */
public final class j {
    public static void a(i iVar, c cVar) throws IOException {
        m.X.a(cVar, iVar);
    }

    public static Writer a(Appendable appendable) {
        return appendable instanceof Writer ? (Writer) appendable : new a(appendable);
    }

    /* compiled from: Streams */
    private static final class a extends Writer {

        /* renamed from: a  reason: collision with root package name */
        private final Appendable f303a;
        private final C0009a b = new C0009a();

        a(Appendable appendable) {
            this.f303a = appendable;
        }

        public void write(char[] cArr, int i, int i2) throws IOException {
            this.b.f304a = cArr;
            this.f303a.append(this.b, i, i + i2);
        }

        public void write(int i) throws IOException {
            this.f303a.append((char) i);
        }

        public void flush() {
        }

        public void close() {
        }

        /* renamed from: com.a.a.b.j$a$a  reason: collision with other inner class name */
        /* compiled from: Streams */
        static class C0009a implements CharSequence {

            /* renamed from: a  reason: collision with root package name */
            char[] f304a;

            C0009a() {
            }

            public int length() {
                return this.f304a.length;
            }

            public char charAt(int i) {
                return this.f304a[i];
            }

            public CharSequence subSequence(int i, int i2) {
                return new String(this.f304a, i, i2 - i);
            }
        }
    }
}
