package com.a.a.b.a;

import com.a.a.b.c;
import com.a.a.b.h;
import com.a.a.e;
import com.a.a.r;
import com.a.a.s;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.Collection;

/* compiled from: CollectionTypeAdapterFactory */
public final class b implements s {

    /* renamed from: a  reason: collision with root package name */
    private final c f249a;

    public b(c cVar) {
        this.f249a = cVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.a.a.b.b.a(java.lang.reflect.Type, java.lang.Class<?>):java.lang.reflect.Type
     arg types: [java.lang.reflect.Type, java.lang.Class<? super T>]
     candidates:
      com.a.a.b.b.a(java.lang.Object[], java.lang.Object):int
      com.a.a.b.b.a(java.lang.Object, java.lang.Object):boolean
      com.a.a.b.b.a(java.lang.reflect.Type, java.lang.reflect.Type):boolean
      com.a.a.b.b.a(java.lang.reflect.Type, java.lang.Class<?>):java.lang.reflect.Type */
    public <T> r<T> a(e eVar, com.a.a.c.a<T> aVar) {
        Type b = aVar.b();
        Class<? super T> a2 = aVar.a();
        if (!Collection.class.isAssignableFrom(a2)) {
            return null;
        }
        Type a3 = com.a.a.b.b.a(b, (Class<?>) a2);
        return new a(eVar, a3, eVar.a((com.a.a.c.a) com.a.a.c.a.a(a3)), this.f249a.a(aVar));
    }

    /* compiled from: CollectionTypeAdapterFactory */
    private static final class a<E> extends r<Collection<E>> {

        /* renamed from: a  reason: collision with root package name */
        private final r<E> f250a;
        private final h<? extends Collection<E>> b;

        public a(e eVar, Type type, r<E> rVar, h<? extends Collection<E>> hVar) {
            this.f250a = new l(eVar, rVar, type);
            this.b = hVar;
        }

        /* renamed from: a */
        public Collection<E> b(com.a.a.d.a aVar) throws IOException {
            if (aVar.f() == com.a.a.d.b.NULL) {
                aVar.j();
                return null;
            }
            Collection<E> collection = (Collection) this.b.a();
            aVar.a();
            while (aVar.e()) {
                collection.add(this.f250a.b(aVar));
            }
            aVar.b();
            return collection;
        }

        public void a(com.a.a.d.c cVar, Collection<E> collection) throws IOException {
            if (collection == null) {
                cVar.f();
                return;
            }
            cVar.b();
            for (E a2 : collection) {
                this.f250a.a(cVar, a2);
            }
            cVar.c();
        }
    }
}
