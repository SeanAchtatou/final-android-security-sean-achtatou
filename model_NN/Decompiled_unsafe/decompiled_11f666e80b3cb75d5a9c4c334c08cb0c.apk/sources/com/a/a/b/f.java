package com.a.a.b;

import java.math.BigDecimal;

/* compiled from: LazilyParsedNumber */
public final class f extends Number {

    /* renamed from: a  reason: collision with root package name */
    private final String f295a;

    public f(String str) {
        this.f295a = str;
    }

    public int intValue() {
        try {
            return Integer.parseInt(this.f295a);
        } catch (NumberFormatException e) {
            try {
                return (int) Long.parseLong(this.f295a);
            } catch (NumberFormatException e2) {
                return new BigDecimal(this.f295a).intValue();
            }
        }
    }

    public long longValue() {
        try {
            return Long.parseLong(this.f295a);
        } catch (NumberFormatException e) {
            return new BigDecimal(this.f295a).longValue();
        }
    }

    public float floatValue() {
        return Float.parseFloat(this.f295a);
    }

    public double doubleValue() {
        return Double.parseDouble(this.f295a);
    }

    public String toString() {
        return this.f295a;
    }

    public int hashCode() {
        return this.f295a.hashCode();
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof f)) {
            return false;
        }
        f fVar = (f) obj;
        if (this.f295a == fVar.f295a || this.f295a.equals(fVar.f295a)) {
            return true;
        }
        return false;
    }
}
