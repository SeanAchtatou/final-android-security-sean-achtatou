package com.a.a.b.a;

import com.a.a.d.c;
import com.a.a.g;
import com.a.a.i;
import com.a.a.k;
import com.a.a.l;
import com.a.a.n;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

/* compiled from: JsonTreeWriter */
public final class f extends c {

    /* renamed from: a  reason: collision with root package name */
    private static final Writer f253a = new Writer() {
        public void write(char[] cArr, int i, int i2) {
            throw new AssertionError();
        }

        public void flush() throws IOException {
            throw new AssertionError();
        }

        public void close() throws IOException {
            throw new AssertionError();
        }
    };
    private static final n b = new n("closed");
    private final List<i> c = new ArrayList();
    private String d;
    private i e = k.f322a;

    public f() {
        super(f253a);
    }

    public i a() {
        if (this.c.isEmpty()) {
            return this.e;
        }
        throw new IllegalStateException("Expected one JSON element but was " + this.c);
    }

    private i j() {
        return this.c.get(this.c.size() - 1);
    }

    private void a(i iVar) {
        if (this.d != null) {
            if (!iVar.j() || i()) {
                ((l) j()).a(this.d, iVar);
            }
            this.d = null;
        } else if (this.c.isEmpty()) {
            this.e = iVar;
        } else {
            i j = j();
            if (j instanceof g) {
                ((g) j).a(iVar);
                return;
            }
            throw new IllegalStateException();
        }
    }

    public c b() throws IOException {
        g gVar = new g();
        a(gVar);
        this.c.add(gVar);
        return this;
    }

    public c c() throws IOException {
        if (this.c.isEmpty() || this.d != null) {
            throw new IllegalStateException();
        } else if (j() instanceof g) {
            this.c.remove(this.c.size() - 1);
            return this;
        } else {
            throw new IllegalStateException();
        }
    }

    public c d() throws IOException {
        l lVar = new l();
        a(lVar);
        this.c.add(lVar);
        return this;
    }

    public c e() throws IOException {
        if (this.c.isEmpty() || this.d != null) {
            throw new IllegalStateException();
        } else if (j() instanceof l) {
            this.c.remove(this.c.size() - 1);
            return this;
        } else {
            throw new IllegalStateException();
        }
    }

    public c a(String str) throws IOException {
        if (this.c.isEmpty() || this.d != null) {
            throw new IllegalStateException();
        } else if (j() instanceof l) {
            this.d = str;
            return this;
        } else {
            throw new IllegalStateException();
        }
    }

    public c b(String str) throws IOException {
        if (str == null) {
            return f();
        }
        a(new n(str));
        return this;
    }

    public c f() throws IOException {
        a(k.f322a);
        return this;
    }

    public c a(boolean z) throws IOException {
        a(new n(Boolean.valueOf(z)));
        return this;
    }

    public c a(long j) throws IOException {
        a(new n(Long.valueOf(j)));
        return this;
    }

    public c a(Number number) throws IOException {
        if (number == null) {
            return f();
        }
        if (!g()) {
            double doubleValue = number.doubleValue();
            if (Double.isNaN(doubleValue) || Double.isInfinite(doubleValue)) {
                throw new IllegalArgumentException("JSON forbids NaN and infinities: " + number);
            }
        }
        a(new n(number));
        return this;
    }

    public void flush() throws IOException {
    }

    public void close() throws IOException {
        if (!this.c.isEmpty()) {
            throw new IOException("Incomplete document");
        }
        this.c.add(b);
    }
}
