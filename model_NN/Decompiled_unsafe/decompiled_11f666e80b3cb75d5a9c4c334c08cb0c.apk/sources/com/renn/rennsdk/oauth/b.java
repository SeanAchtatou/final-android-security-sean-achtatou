package com.renn.rennsdk.oauth;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import com.renn.rennsdk.b;

/* compiled from: LoginLogic */
public class b {

    /* renamed from: a  reason: collision with root package name */
    private Context f1265a;
    private ProgressDialog b;
    private h c;
    private b.a d;

    public void a(Activity activity) {
        this.f1265a = activity;
        this.b = new ProgressDialog(activity);
        this.b.setCanceledOnTouchOutside(false);
        this.b.setMessage("renren_login_logging");
        this.c = new h(this.f1265a, this);
        this.d = e.a(activity.getApplicationContext()).a();
        this.c.a(this.d);
    }

    public void a(Context context) {
        this.f1265a = context;
    }

    public boolean a(int i, String str, String str2, String str3, String str4) {
        if (this.c == null) {
            this.c = new h(this.f1265a, this);
        }
        return this.c.a(i, str, str2, str3, str4);
    }

    public boolean a(int i, int i2, Intent intent) {
        if (this.c != null) {
            return this.c.a(i, i2, intent);
        }
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public void a(int i, String str, String str2, String str3) {
        this.c.a(i);
        if (this.f1265a instanceof Activity) {
            Intent intent = new Intent();
            intent.setClass(this.f1265a, OAuthActivity.class);
            intent.putExtra("apikey", str);
            intent.putExtra("scope", str2);
            intent.putExtra("token_type", str3);
            intent.putExtra("fromoauth", false);
            ((Activity) this.f1265a).startActivityForResult(intent, i);
        }
    }
}
