package com.renn.rennsdk.oauth;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import cn.banshenggua.aichang.utils.Constants;
import com.renn.rennsdk.b;

/* compiled from: RenRenOAuth */
public class e {
    private static e e;

    /* renamed from: a  reason: collision with root package name */
    public String f1270a;
    public String b;
    public String c;
    public String d;
    private b f = new b();
    private Activity g;
    private b.a h;

    public static e a(Context context) {
        if (e == null) {
            e = new e(context);
        }
        return e;
    }

    private e(Context context) {
        this.f.a(context);
    }

    public b.a a() {
        return this.h;
    }

    public boolean a(int i, int i2, Intent intent) {
        String action;
        if (i != 32766) {
            return this.f.a(i, i2, intent);
        }
        switch (i2) {
            case -1:
                if (intent == null || (action = intent.getAction()) == null) {
                    return true;
                }
                if (action.equals("ACTION_CHOOSE_ACCOUNT_USE_ANOTHER")) {
                    a(this.g, false);
                    return true;
                } else if (!action.equals("ACTION_CHOOSE_ACCOUNT_USE_SSO")) {
                    return true;
                } else {
                    intent.getStringExtra(Constants.ACCOUNT);
                    a(this.g, true);
                    return true;
                }
            case 0:
                if (this.h == null) {
                    return true;
                }
                this.h.b();
                return true;
            default:
                return true;
        }
    }

    private void a(Activity activity, boolean z) {
        this.g = activity;
        this.f.a(activity);
        if (TextUtils.isEmpty(this.d)) {
            this.d = "bearer";
        }
        if (!z || !this.f.a(1, this.f1270a, this.b, this.c, this.d)) {
            a(1, this.f1270a, this.c, this.d);
        }
    }

    public void a(int i, String str, String str2, String str3) {
        if (this.f != null && (this.f instanceof b)) {
            this.f.a(i, str, str2, str3);
        }
    }
}
