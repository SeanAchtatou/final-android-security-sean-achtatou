package com.renn.rennsdk.oauth;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import com.renn.rennsdk.a;
import com.renn.rennsdk.b;
import com.renn.rennsdk.oauth.f;

/* compiled from: SSO */
public class h {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public Context f1276a;
    private b b;
    private int c;
    /* access modifiers changed from: private */
    public b.a d;

    public h(Context context, b bVar) {
        this.f1276a = context;
        this.b = bVar;
    }

    public void a(int i) {
        this.c = i;
    }

    public void a(b.a aVar) {
        this.d = aVar;
    }

    public boolean a(int i, int i2, Intent intent) {
        if (i == this.c) {
            switch (i2) {
                case -1:
                    if (intent != null) {
                        a(intent.getStringExtra("access_token"), intent.getStringExtra("scope"), intent.getLongExtra("expired_in", 0), intent.getStringExtra("token_type"), intent.getStringExtra("mac_key"), intent.getStringExtra("mac_algorithm"));
                        return true;
                    }
                    break;
                case 0:
                    if (this.d != null) {
                        this.d.b();
                        break;
                    }
                    break;
            }
        }
        return false;
    }

    public boolean a(int i, String str, String str2, String str3, String str4) {
        this.c = i;
        if (this.f1276a instanceof Activity) {
            final f fVar = new f((Activity) this.f1276a, str, str3, a.a(this.f1276a).e(), str4);
            if (Boolean.valueOf(fVar.a(new f.d() {
                public void a(boolean z) {
                    h.this.a(fVar.a(), fVar.b(), fVar.c(), fVar.d(), fVar.e(), fVar.f());
                }

                public void a(f.e eVar) {
                    if (h.this.d != null) {
                        new Handler(h.this.f1276a.getMainLooper()).post(new Runnable() {
                            public void run() {
                                h.this.d.b();
                            }
                        });
                    }
                }
            })).booleanValue()) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public void a(String str, String str2, long j, String str3, String str4, String str5) {
        a.C0037a aVar = a.C0037a.Bearer;
        String str6 = null;
        if (str3.equals("mac")) {
            aVar = a.C0037a.MAC;
            String substring = str.substring(0, str.lastIndexOf("."));
            str6 = substring.substring(substring.lastIndexOf(".") + 1);
        } else if (str3.equals("bearer")) {
            aVar = a.C0037a.Bearer;
            str6 = str.substring(str.lastIndexOf("-") + 1);
        }
        a aVar2 = new a();
        aVar2.f1256a = aVar;
        aVar2.b = str;
        aVar2.c = "";
        aVar2.d = str4;
        aVar2.e = str5;
        aVar2.f = str2;
        aVar2.g = j;
        aVar2.h = System.currentTimeMillis();
        i a2 = i.a(this.f1276a);
        a2.a("rr_renn_tokenType", aVar2.f1256a);
        a2.a("rr_renn_accessToken", aVar2.b);
        a2.a("rr_renn_refreshToken", aVar2.c);
        a2.a("rr_renn_macKey", aVar2.d);
        a2.a("rr_renn_macAlgorithm", aVar2.e);
        a2.a("rr_renn_accessScope", aVar2.f);
        a2.a("rr_renn_expiresIn", Long.valueOf(aVar2.g));
        a2.a("rr_renn_requestTime", Long.valueOf(aVar2.h));
        a2.a("rr_renn_uid", str6);
        b.a(this.f1276a).a(aVar2);
        b.a(this.f1276a).a(str6);
        new Thread() {
            public void run() {
                new Handler(h.this.f1276a.getMainLooper()).post(new Runnable() {
                    public void run() {
                        if (h.this.d != null) {
                            h.this.d.a();
                        }
                    }
                });
            }
        }.start();
    }
}
