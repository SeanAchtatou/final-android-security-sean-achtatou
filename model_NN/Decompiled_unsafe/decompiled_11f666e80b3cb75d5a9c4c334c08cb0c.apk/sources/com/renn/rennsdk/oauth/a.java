package com.renn.rennsdk.oauth;

import android.content.Context;
import android.content.res.Configuration;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import com.baidu.mobads.interfaces.utils.IXAdSystemUtils;
import com.xiaomi.mipush.sdk.MiPushClient;

/* compiled from: EnvironmentUtil */
public class a {

    /* renamed from: a  reason: collision with root package name */
    private static a f1264a;
    private Context b;
    private String c;
    private String d;
    private String e;
    private String f;
    private String g = "01";
    private String h = "023000";
    private String i;
    private String j;
    private String k;
    private int l;
    private String m = "2.0";

    public static final a a(Context context) {
        if (f1264a == null) {
            f1264a = new a(context);
        }
        return f1264a;
    }

    private a(Context context) {
        this.b = context;
        this.c = d();
        this.l = 9600201;
        this.d = b(this.b);
        this.e = "android_" + Build.VERSION.RELEASE;
        this.f = Build.MODEL;
        this.i = a();
        this.j = b();
        this.k = c();
        if ("000000000000000".equals(this.c)) {
            this.c = this.i;
        }
    }

    public String a() {
        String str = "";
        if (this.b.checkCallingOrSelfPermission("android.permission.ACCESS_WIFI_STATE") == 0) {
            str = ((WifiManager) this.b.getSystemService(IXAdSystemUtils.NT_WIFI)).getConnectionInfo().getMacAddress();
        }
        return str == null ? "" : str;
    }

    public String b() {
        String str = "";
        if (this.b.checkCallingOrSelfPermission("android.permission.READ_PHONE_STATE") == 0) {
            str = ((TelephonyManager) this.b.getSystemService("phone")).getNetworkOperator();
        }
        return str == null ? "" : str;
    }

    public String c() {
        String packageName = this.b.getPackageName();
        return packageName == null ? "" : packageName;
    }

    public String d() {
        String str = null;
        if (this.b.checkCallingOrSelfPermission("android.permission.READ_PHONE_STATE") == 0) {
            str = ((TelephonyManager) this.b.getSystemService("phone")).getDeviceId();
        }
        if (str == null) {
            return "000000000000000";
        }
        return str;
    }

    public static String b(Context context) {
        Configuration configuration = context.getResources().getConfiguration();
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        if (configuration.orientation == 2) {
            return String.valueOf(Integer.toString(displayMetrics.heightPixels)) + "x" + Integer.toString(displayMetrics.widthPixels);
        }
        return String.valueOf(Integer.toString(displayMetrics.widthPixels)) + "x" + Integer.toString(displayMetrics.heightPixels);
    }

    public String e() {
        StringBuilder sb = new StringBuilder();
        sb.append("{\"model\":").append("\"").append(this.f).append("\",").append("\"uniqid\":").append("\"").append(this.c).append("\",").append("\"os\":").append("\"").append(this.e).append("\",").append("\"screen\":").append("\"").append(this.d).append("\",").append("\"from\":").append(this.l).append(MiPushClient.ACCEPT_TIME_SEPARATOR).append("\"sdkkey\":").append("\"").append(this.g).append(this.h).append("\",").append("\"mac\":").append("\"").append(this.i).append("\",").append("\"other\":").append("\"").append(this.j).append(MiPushClient.ACCEPT_TIME_SEPARATOR).append(this.k).append("\",").append("\"version\":").append("\"").append(this.m).append("\"}");
        return sb.toString();
    }
}
