package com.pocketmusic.kshare.requestobjs;

import android.support.v4.media.TransportMediator;
import android.text.TextUtils;
import cn.banshenggua.aichang.api.APIKey;
import com.ffcs.inapppaylib.bean.Constants;
import com.igexin.download.Downloads;
import java.util.HashMap;

/* compiled from: UrlConfig */
public class s {

    /* renamed from: a  reason: collision with root package name */
    public static HashMap<String, String> f1187a = new HashMap<>();
    private static String b = "";
    private static /* synthetic */ int[] c;

    static /* synthetic */ int[] b() {
        int[] iArr = c;
        if (iArr == null) {
            iArr = new int[APIKey.values().length];
            try {
                iArr[APIKey.APIKEY_CrashLog.ordinal()] = 138;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[APIKey.APIKEY_GetUserAlbums.ordinal()] = 74;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[APIKey.APIKEY_LikeFanchang.ordinal()] = 60;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[APIKey.APIKEY_Modify_UserZoneHomePic.ordinal()] = 43;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[APIKey.APIKEY_SendLog.ordinal()] = 137;
            } catch (NoSuchFieldError e5) {
            }
            try {
                iArr[APIKey.APIKEY_UploadUserAlbums.ordinal()] = 75;
            } catch (NoSuchFieldError e6) {
            }
            try {
                iArr[APIKey.APIKey_ACTIVATE.ordinal()] = 155;
            } catch (NoSuchFieldError e7) {
            }
            try {
                iArr[APIKey.APIKey_AccountUpdateNotify.ordinal()] = 148;
            } catch (NoSuchFieldError e8) {
            }
            try {
                iArr[APIKey.APIKey_AccountUpdateSnsNotify.ordinal()] = 149;
            } catch (NoSuchFieldError e9) {
            }
            try {
                iArr[APIKey.APIKey_AddTo_Club.ordinal()] = 220;
            } catch (NoSuchFieldError e10) {
            }
            try {
                iArr[APIKey.APIKey_Add_Vice_Admin.ordinal()] = 227;
            } catch (NoSuchFieldError e11) {
            }
            try {
                iArr[APIKey.APIKey_BanZouUrl.ordinal()] = 139;
            } catch (NoSuchFieldError e12) {
            }
            try {
                iArr[APIKey.APIKey_BindSNS.ordinal()] = 144;
            } catch (NoSuchFieldError e13) {
            }
            try {
                iArr[APIKey.APIKey_Bind_Phone.ordinal()] = 181;
            } catch (NoSuchFieldError e14) {
            }
            try {
                iArr[APIKey.APIKey_BuyGift.ordinal()] = 198;
            } catch (NoSuchFieldError e15) {
            }
            try {
                iArr[APIKey.APIKey_COMMON_DISCOVERY.ordinal()] = 238;
            } catch (NoSuchFieldError e16) {
            }
            try {
                iArr[APIKey.APIKey_Category_AOD.ordinal()] = 66;
            } catch (NoSuchFieldError e17) {
            }
            try {
                iArr[APIKey.APIKey_Category_AOD_LIVE.ordinal()] = 67;
            } catch (NoSuchFieldError e18) {
            }
            try {
                iArr[APIKey.APIKey_Category_AOD_YQ.ordinal()] = 68;
            } catch (NoSuchFieldError e19) {
            }
            try {
                iArr[APIKey.APIKey_Category_TopList.ordinal()] = 72;
            } catch (NoSuchFieldError e20) {
            }
            try {
                iArr[APIKey.APIKey_Change_Phone.ordinal()] = 180;
            } catch (NoSuchFieldError e21) {
            }
            try {
                iArr[APIKey.APIKey_Channel.ordinal()] = 115;
            } catch (NoSuchFieldError e22) {
            }
            try {
                iArr[APIKey.APIKey_Channel_ByAll.ordinal()] = 118;
            } catch (NoSuchFieldError e23) {
            }
            try {
                iArr[APIKey.APIKey_Channel_ByPinyin.ordinal()] = 119;
            } catch (NoSuchFieldError e24) {
            }
            try {
                iArr[APIKey.APIKey_Channel_BySinger.ordinal()] = 116;
            } catch (NoSuchFieldError e25) {
            }
            try {
                iArr[APIKey.APIKey_Channel_BySongName.ordinal()] = 117;
            } catch (NoSuchFieldError e26) {
            }
            try {
                iArr[APIKey.APIKey_Channel_ByTopbanzou.ordinal()] = 120;
            } catch (NoSuchFieldError e27) {
            }
            try {
                iArr[APIKey.APIKey_Channel_Rank.ordinal()] = 153;
            } catch (NoSuchFieldError e28) {
            }
            try {
                iArr[APIKey.APIKey_CheckNickName.ordinal()] = 3;
            } catch (NoSuchFieldError e29) {
            }
            try {
                iArr[APIKey.APIKey_CheckUserName.ordinal()] = 2;
            } catch (NoSuchFieldError e30) {
            }
            try {
                iArr[APIKey.APIKey_Club_Add.ordinal()] = 210;
            } catch (NoSuchFieldError e31) {
            }
            try {
                iArr[APIKey.APIKey_Club_Apply_Add.ordinal()] = 213;
            } catch (NoSuchFieldError e32) {
            }
            try {
                iArr[APIKey.APIKey_Club_Apply_Del.ordinal()] = 214;
            } catch (NoSuchFieldError e33) {
            }
            try {
                iArr[APIKey.APIKey_Club_Apply_List.ordinal()] = 215;
            } catch (NoSuchFieldError e34) {
            }
            try {
                iArr[APIKey.APIKey_Club_Del.ordinal()] = 211;
            } catch (NoSuchFieldError e35) {
            }
            try {
                iArr[APIKey.APIKey_Club_Edit.ordinal()] = 212;
            } catch (NoSuchFieldError e36) {
            }
            try {
                iArr[APIKey.APIKey_Club_Relation.ordinal()] = 222;
            } catch (NoSuchFieldError e37) {
            }
            try {
                iArr[APIKey.APIKey_Club_User_List.ordinal()] = 216;
            } catch (NoSuchFieldError e38) {
            }
            try {
                iArr[APIKey.APIKey_Common_Property.ordinal()] = 88;
            } catch (NoSuchFieldError e39) {
            }
            try {
                iArr[APIKey.APIKey_DAY_EVENT.ordinal()] = 158;
            } catch (NoSuchFieldError e40) {
            }
            try {
                iArr[APIKey.APIKey_Default.ordinal()] = 1;
            } catch (NoSuchFieldError e41) {
            }
            try {
                iArr[APIKey.APIKey_DelTo_Club.ordinal()] = 221;
            } catch (NoSuchFieldError e42) {
            }
            try {
                iArr[APIKey.APIKey_Del_Vice_Admin.ordinal()] = 228;
            } catch (NoSuchFieldError e43) {
            }
            try {
                iArr[APIKey.APIKey_DeleteUserPics.ordinal()] = 76;
            } catch (NoSuchFieldError e44) {
            }
            try {
                iArr[APIKey.APIKey_DeleteUserWeibo.ordinal()] = 49;
            } catch (NoSuchFieldError e45) {
            }
            try {
                iArr[APIKey.APIKey_EVENT_SEND.ordinal()] = 154;
            } catch (NoSuchFieldError e46) {
            }
            try {
                iArr[APIKey.APIKey_Exchange.ordinal()] = 140;
            } catch (NoSuchFieldError e47) {
            }
            try {
                iArr[APIKey.APIKey_FanChang_Add_Cover.ordinal()] = 124;
            } catch (NoSuchFieldError e48) {
            }
            try {
                iArr[APIKey.APIKey_FanChang_Cover.ordinal()] = 122;
            } catch (NoSuchFieldError e49) {
            }
            try {
                iArr[APIKey.APIKey_FanChang_Cover_Data.ordinal()] = 123;
            } catch (NoSuchFieldError e50) {
            }
            try {
                iArr[APIKey.APIKey_FanChang_Info.ordinal()] = 121;
            } catch (NoSuchFieldError e51) {
            }
            try {
                iArr[APIKey.APIKey_FollowRoomList.ordinal()] = 80;
            } catch (NoSuchFieldError e52) {
            }
            try {
                iArr[APIKey.APIKey_Friends_In_ROOM.ordinal()] = 208;
            } catch (NoSuchFieldError e53) {
            }
            try {
                iArr[APIKey.APIKey_Friends_ROOM.ordinal()] = 206;
            } catch (NoSuchFieldError e54) {
            }
            try {
                iArr[APIKey.APIKey_Friends_Recommend_ROOM.ordinal()] = 207;
            } catch (NoSuchFieldError e55) {
            }
            try {
                iArr[APIKey.APIKey_GET_BALANCE.ordinal()] = 159;
            } catch (NoSuchFieldError e56) {
            }
            try {
                iArr[APIKey.APIKey_GET_FACE_SYSTEM.ordinal()] = 182;
            } catch (NoSuchFieldError e57) {
            }
            try {
                iArr[APIKey.APIKey_GUACHANG.ordinal()] = 63;
            } catch (NoSuchFieldError e58) {
            }
            try {
                iArr[APIKey.APIKey_GUACHANGList.ordinal()] = 150;
            } catch (NoSuchFieldError e59) {
            }
            try {
                iArr[APIKey.APIKey_GUACHANG_ITEM.ordinal()] = 64;
            } catch (NoSuchFieldError e60) {
            }
            try {
                iArr[APIKey.APIKey_GetAPK.ordinal()] = 95;
            } catch (NoSuchFieldError e61) {
            }
            try {
                iArr[APIKey.APIKey_GetAlbums.ordinal()] = 142;
            } catch (NoSuchFieldError e62) {
            }
            try {
                iArr[APIKey.APIKey_GetAllOrders.ordinal()] = 203;
            } catch (NoSuchFieldError e63) {
            }
            try {
                iArr[APIKey.APIKey_GetConsumeList.ordinal()] = 205;
            } catch (NoSuchFieldError e64) {
            }
            try {
                iArr[APIKey.APIKey_GetDialogUserInfo.ordinal()] = 7;
            } catch (NoSuchFieldError e65) {
            }
            try {
                iArr[APIKey.APIKey_GetFanChangURL.ordinal()] = 136;
            } catch (NoSuchFieldError e66) {
            }
            try {
                iArr[APIKey.APIKey_GetFixedRoomRank.ordinal()] = 82;
            } catch (NoSuchFieldError e67) {
            }
            try {
                iArr[APIKey.APIKey_GetLevelImgUrl.ordinal()] = 12;
            } catch (NoSuchFieldError e68) {
            }
            try {
                iArr[APIKey.APIKey_GetLiveDown.ordinal()] = 174;
            } catch (NoSuchFieldError e69) {
            }
            try {
                iArr[APIKey.APIKey_GetLiveUp.ordinal()] = 173;
            } catch (NoSuchFieldError e70) {
            }
            try {
                iArr[APIKey.APIKey_GetLyrcURL.ordinal()] = 129;
            } catch (NoSuchFieldError e71) {
            }
            try {
                iArr[APIKey.APIKey_GetMainCategory.ordinal()] = 65;
            } catch (NoSuchFieldError e72) {
            }
            try {
                iArr[APIKey.APIKey_GetMic_RoomList.ordinal()] = 229;
            } catch (NoSuchFieldError e73) {
            }
            try {
                iArr[APIKey.APIKey_GetNetLyrcURL.ordinal()] = 135;
            } catch (NoSuchFieldError e74) {
            }
            try {
                iArr[APIKey.APIKey_GetOrderInfo.ordinal()] = 200;
            } catch (NoSuchFieldError e75) {
            }
            try {
                iArr[APIKey.APIKey_GetOrderStatus.ordinal()] = 201;
            } catch (NoSuchFieldError e76) {
            }
            try {
                iArr[APIKey.APIKey_GetPayEventNotice.ordinal()] = 202;
            } catch (NoSuchFieldError e77) {
            }
            try {
                iArr[APIKey.APIKey_GetRoomList.ordinal()] = 77;
            } catch (NoSuchFieldError e78) {
            }
            try {
                iArr[APIKey.APIKey_GetRoomSongScore.ordinal()] = 83;
            } catch (NoSuchFieldError e79) {
            }
            try {
                iArr[APIKey.APIKey_GetSingerList.ordinal()] = 134;
            } catch (NoSuchFieldError e80) {
            }
            try {
                iArr[APIKey.APIKey_GetSingerPicURL.ordinal()] = 133;
            } catch (NoSuchFieldError e81) {
            }
            try {
                iArr[APIKey.APIKey_GetSmsCode.ordinal()] = 177;
            } catch (NoSuchFieldError e82) {
            }
            try {
                iArr[APIKey.APIKey_GetSongPicURL.ordinal()] = 132;
            } catch (NoSuchFieldError e83) {
            }
            try {
                iArr[APIKey.APIKey_GetSongURL.ordinal()] = 131;
            } catch (NoSuchFieldError e84) {
            }
            try {
                iArr[APIKey.APIKey_GetTradePriceList.ordinal()] = 204;
            } catch (NoSuchFieldError e85) {
            }
            try {
                iArr[APIKey.APIKey_GetUpdateInfo.ordinal()] = 141;
            } catch (NoSuchFieldError e86) {
            }
            try {
                iArr[APIKey.APIKey_GetUserFace.ordinal()] = 11;
            } catch (NoSuchFieldError e87) {
            }
            try {
                iArr[APIKey.APIKey_GetUserInfo.ordinal()] = 5;
            } catch (NoSuchFieldError e88) {
            }
            try {
                iArr[APIKey.APIKey_GetUserInfoItem.ordinal()] = 6;
            } catch (NoSuchFieldError e89) {
            }
            try {
                iArr[APIKey.APIKey_GetUserList.ordinal()] = 44;
            } catch (NoSuchFieldError e90) {
            }
            try {
                iArr[APIKey.APIKey_GetValidCode.ordinal()] = 175;
            } catch (NoSuchFieldError e91) {
            }
            try {
                iArr[APIKey.APIKey_GetVipList.ordinal()] = 230;
            } catch (NoSuchFieldError e92) {
            }
            try {
                iArr[APIKey.APIKey_Get_Animation_Config.ordinal()] = 164;
            } catch (NoSuchFieldError e93) {
            }
            try {
                iArr[APIKey.APIKey_Get_Club_Info.ordinal()] = 219;
            } catch (NoSuchFieldError e94) {
            }
            try {
                iArr[APIKey.APIKey_Get_Config.ordinal()] = 161;
            } catch (NoSuchFieldError e95) {
            }
            try {
                iArr[APIKey.APIKey_Get_My_Club.ordinal()] = 217;
            } catch (NoSuchFieldError e96) {
            }
            try {
                iArr[APIKey.APIKey_Get_My_Club_Tuijian.ordinal()] = 218;
            } catch (NoSuchFieldError e97) {
            }
            try {
                iArr[APIKey.APIKey_Get_Start_Pic.ordinal()] = 165;
            } catch (NoSuchFieldError e98) {
            }
            try {
                iArr[APIKey.APIKey_Get_Super_Gift_Fans.ordinal()] = 209;
            } catch (NoSuchFieldError e99) {
            }
            try {
                iArr[APIKey.APIKey_Get_Third_Config.ordinal()] = 163;
            } catch (NoSuchFieldError e100) {
            }
            try {
                iArr[APIKey.APIKey_GiftList.ordinal()] = 199;
            } catch (NoSuchFieldError e101) {
            }
            try {
                iArr[APIKey.APIKey_HallOfflineMessage.ordinal()] = 92;
            } catch (NoSuchFieldError e102) {
            }
            try {
                iArr[APIKey.APIKey_Home_Page.ordinal()] = 162;
            } catch (NoSuchFieldError e103) {
            }
            try {
                iArr[APIKey.APIKey_HotHMWeiBoSelect.ordinal()] = 97;
            } catch (NoSuchFieldError e104) {
            }
            try {
                iArr[APIKey.APIKey_HotRoomList.ordinal()] = 78;
            } catch (NoSuchFieldError e105) {
            }
            try {
                iArr[APIKey.APIKey_HotWeiBoSelect.ordinal()] = 96;
            } catch (NoSuchFieldError e106) {
            }
            try {
                iArr[APIKey.APIKey_Hot_Clubs.ordinal()] = 225;
            } catch (NoSuchFieldError e107) {
            }
            try {
                iArr[APIKey.APIKey_Hot_Program.ordinal()] = 70;
            } catch (NoSuchFieldError e108) {
            }
            try {
                iArr[APIKey.APIKey_Hot_Program_Thumb.ordinal()] = 71;
            } catch (NoSuchFieldError e109) {
            }
            try {
                iArr[APIKey.APIKey_Hot_Room_Page_AD.ordinal()] = 69;
            } catch (NoSuchFieldError e110) {
            }
            try {
                iArr[APIKey.APIKey_Hot_Today_Selected.ordinal()] = 231;
            } catch (NoSuchFieldError e111) {
            }
            try {
                iArr[APIKey.APIKey_Kick_User.ordinal()] = 226;
            } catch (NoSuchFieldError e112) {
            }
            try {
                iArr[APIKey.APIKey_Level_Img.ordinal()] = 189;
            } catch (NoSuchFieldError e113) {
            }
            try {
                iArr[APIKey.APIKey_Level_RoomDetail.ordinal()] = 187;
            } catch (NoSuchFieldError e114) {
            }
            try {
                iArr[APIKey.APIKey_Level_UserDetail.ordinal()] = 188;
            } catch (NoSuchFieldError e115) {
            }
            try {
                iArr[APIKey.APIKey_LibCheck.ordinal()] = 94;
            } catch (NoSuchFieldError e116) {
            }
            try {
                iArr[APIKey.APIKey_ListPeopleHeChang.ordinal()] = 147;
            } catch (NoSuchFieldError e117) {
            }
            try {
                iArr[APIKey.APIKey_LiveServer.ordinal()] = 90;
            } catch (NoSuchFieldError e118) {
            }
            try {
                iArr[APIKey.APIKey_LiveServerHall.ordinal()] = 91;
            } catch (NoSuchFieldError e119) {
            }
            try {
                iArr[APIKey.APIKey_Login.ordinal()] = 38;
            } catch (NoSuchFieldError e120) {
            }
            try {
                iArr[APIKey.APIKey_Logout.ordinal()] = 39;
            } catch (NoSuchFieldError e121) {
            }
            try {
                iArr[APIKey.APIKey_LyrcPath.ordinal()] = 130;
            } catch (NoSuchFieldError e122) {
            }
            try {
                iArr[APIKey.APIKey_Machine_Config.ordinal()] = 160;
            } catch (NoSuchFieldError e123) {
            }
            try {
                iArr[APIKey.APIKey_Modify_MyFace.ordinal()] = 42;
            } catch (NoSuchFieldError e124) {
            }
            try {
                iArr[APIKey.APIKey_Modify_PassWord.ordinal()] = 41;
            } catch (NoSuchFieldError e125) {
            }
            try {
                iArr[APIKey.APIKey_MyGiftByUser.ordinal()] = 197;
            } catch (NoSuchFieldError e126) {
            }
            try {
                iArr[APIKey.APIKey_MyGiftGroup.ordinal()] = 196;
            } catch (NoSuchFieldError e127) {
            }
            try {
                iArr[APIKey.APIKey_NEW_GUANGCHANG.ordinal()] = 62;
            } catch (NoSuchFieldError e128) {
            }
            try {
                iArr[APIKey.APIKey_New_PeopleSong.ordinal()] = 100;
            } catch (NoSuchFieldError e129) {
            }
            try {
                iArr[APIKey.APIKey_Notify_Config.ordinal()] = 99;
            } catch (NoSuchFieldError e130) {
            }
            try {
                iArr[APIKey.APIKey_Quit_Club.ordinal()] = 223;
            } catch (NoSuchFieldError e131) {
            }
            try {
                iArr[APIKey.APIKey_ReGetSmsCode.ordinal()] = 179;
            } catch (NoSuchFieldError e132) {
            }
            try {
                iArr[APIKey.APIKey_RecentLike.ordinal()] = 146;
            } catch (NoSuchFieldError e133) {
            }
            try {
                iArr[APIKey.APIKey_Register.ordinal()] = 45;
            } catch (NoSuchFieldError e134) {
            }
            try {
                iArr[APIKey.APIKey_Report.ordinal()] = 169;
            } catch (NoSuchFieldError e135) {
            }
            try {
                iArr[APIKey.APIKey_Report_Banzou_Lowquality.ordinal()] = 172;
            } catch (NoSuchFieldError e136) {
            }
            try {
                iArr[APIKey.APIKey_Report_Lyrc_Error.ordinal()] = 170;
            } catch (NoSuchFieldError e137) {
            }
            try {
                iArr[APIKey.APIKey_Report_Lyrc_Unalign.ordinal()] = 171;
            } catch (NoSuchFieldError e138) {
            }
            try {
                iArr[APIKey.APIKey_Rondom_Room_Info.ordinal()] = 85;
            } catch (NoSuchFieldError e139) {
            }
            try {
                iArr[APIKey.APIKey_RoomBuyVip.ordinal()] = 193;
            } catch (NoSuchFieldError e140) {
            }
            try {
                iArr[APIKey.APIKey_RoomGiftSpendTop.ordinal()] = 192;
            } catch (NoSuchFieldError e141) {
            }
            try {
                iArr[APIKey.APIKey_RoomHanHua.ordinal()] = 194;
            } catch (NoSuchFieldError e142) {
            }
            try {
                iArr[APIKey.APIKey_RoomParam.ordinal()] = 93;
            } catch (NoSuchFieldError e143) {
            }
            try {
                iArr[APIKey.APIKey_RoomRank.ordinal()] = 81;
            } catch (NoSuchFieldError e144) {
            }
            try {
                iArr[APIKey.APIKey_RoomVipList.ordinal()] = 191;
            } catch (NoSuchFieldError e145) {
            }
            try {
                iArr[APIKey.APIKey_Room_Create.ordinal()] = 87;
            } catch (NoSuchFieldError e146) {
            }
            try {
                iArr[APIKey.APIKey_Room_Edit.ordinal()] = 89;
            } catch (NoSuchFieldError e147) {
            }
            try {
                iArr[APIKey.APIKey_Room_GetMices.ordinal()] = 184;
            } catch (NoSuchFieldError e148) {
            }
            try {
                iArr[APIKey.APIKey_Room_GetMicesWhiteList.ordinal()] = 185;
            } catch (NoSuchFieldError e149) {
            }
            try {
                iArr[APIKey.APIKey_Room_GetUsers.ordinal()] = 183;
            } catch (NoSuchFieldError e150) {
            }
            try {
                iArr[APIKey.APIKey_Room_Info.ordinal()] = 84;
            } catch (NoSuchFieldError e151) {
            }
            try {
                iArr[APIKey.APIKey_Room_Info_Item.ordinal()] = 86;
            } catch (NoSuchFieldError e152) {
            }
            try {
                iArr[APIKey.APIKey_Room_Multi_GetMices.ordinal()] = 186;
            } catch (NoSuchFieldError e153) {
            }
            try {
                iArr[APIKey.APIKey_SDK_Config.ordinal()] = 98;
            } catch (NoSuchFieldError e154) {
            }
            try {
                iArr[APIKey.APIKey_SHARE_LOG.ordinal()] = 156;
            } catch (NoSuchFieldError e155) {
            }
            try {
                iArr[APIKey.APIKey_SHARE_SUCCESS_LOG.ordinal()] = 157;
            } catch (NoSuchFieldError e156) {
            }
            try {
                iArr[APIKey.APIKey_SMS_ADD.ordinal()] = 104;
            } catch (NoSuchFieldError e157) {
            }
            try {
                iArr[APIKey.APIKey_SMS_ALL.ordinal()] = 105;
            } catch (NoSuchFieldError e158) {
            }
            try {
                iArr[APIKey.APIKey_SMS_DEL_LIST.ordinal()] = 108;
            } catch (NoSuchFieldError e159) {
            }
            try {
                iArr[APIKey.APIKey_SMS_DEL_ONE.ordinal()] = 107;
            } catch (NoSuchFieldError e160) {
            }
            try {
                iArr[APIKey.APIKey_SMS_GET_NEW.ordinal()] = 109;
            } catch (NoSuchFieldError e161) {
            }
            try {
                iArr[APIKey.APIKey_SMS_PEOPLE.ordinal()] = 106;
            } catch (NoSuchFieldError e162) {
            }
            try {
                iArr[APIKey.APIKey_SearchRoom.ordinal()] = 79;
            } catch (NoSuchFieldError e163) {
            }
            try {
                iArr[APIKey.APIKey_SearchUser.ordinal()] = 4;
            } catch (NoSuchFieldError e164) {
            }
            try {
                iArr[APIKey.APIKey_Search_Club.ordinal()] = 224;
            } catch (NoSuchFieldError e165) {
            }
            try {
                iArr[APIKey.APIKey_Search_From_QQ_Weibo.ordinal()] = 168;
            } catch (NoSuchFieldError e166) {
            }
            try {
                iArr[APIKey.APIKey_Search_From_Sina.ordinal()] = 167;
            } catch (NoSuchFieldError e167) {
            }
            try {
                iArr[APIKey.APIKey_Search_From_Third.ordinal()] = 166;
            } catch (NoSuchFieldError e168) {
            }
            try {
                iArr[APIKey.APIKey_SetAuth.ordinal()] = 46;
            } catch (NoSuchFieldError e169) {
            }
            try {
                iArr[APIKey.APIKey_Song_AddImage.ordinal()] = 102;
            } catch (NoSuchFieldError e170) {
            }
            try {
                iArr[APIKey.APIKey_Song_Albums.ordinal()] = 101;
            } catch (NoSuchFieldError e171) {
            }
            try {
                iArr[APIKey.APIKey_Song_DelImage.ordinal()] = 103;
            } catch (NoSuchFieldError e172) {
            }
            try {
                iArr[APIKey.APIKey_Song_Download.ordinal()] = 126;
            } catch (NoSuchFieldError e173) {
            }
            try {
                iArr[APIKey.APIKey_Song_Info.ordinal()] = 125;
            } catch (NoSuchFieldError e174) {
            }
            try {
                iArr[APIKey.APIKey_Song_Upload.ordinal()] = 127;
            } catch (NoSuchFieldError e175) {
            }
            try {
                iArr[APIKey.APIKey_Song_Upload_Slice.ordinal()] = 128;
            } catch (NoSuchFieldError e176) {
            }
            try {
                iArr[APIKey.APIKey_SystemUserList_Short.ordinal()] = 35;
            } catch (NoSuchFieldError e177) {
            }
            try {
                iArr[APIKey.APIKey_UNBindSNS.ordinal()] = 145;
            } catch (NoSuchFieldError e178) {
            }
            try {
                iArr[APIKey.APIKey_USER_DISABLE_RECEIVE_SUBSCRIPTION.ordinal()] = 236;
            } catch (NoSuchFieldError e179) {
            }
            try {
                iArr[APIKey.APIKey_USER_ENABLE_RECEIVE_SUBSCRIPTION.ordinal()] = 235;
            } catch (NoSuchFieldError e180) {
            }
            try {
                iArr[APIKey.APIKey_USER_FRIENDS.ordinal()] = 110;
            } catch (NoSuchFieldError e181) {
            }
            try {
                iArr[APIKey.APIKey_USER_SUBSCRIBE.ordinal()] = 232;
            } catch (NoSuchFieldError e182) {
            }
            try {
                iArr[APIKey.APIKey_USER_SUBSCRIPTION.ordinal()] = 237;
            } catch (NoSuchFieldError e183) {
            }
            try {
                iArr[APIKey.APIKey_USER_THIRDCHANGE.ordinal()] = 240;
            } catch (NoSuchFieldError e184) {
            }
            try {
                iArr[APIKey.APIKey_USER_TUISONG_SETTING.ordinal()] = 239;
            } catch (NoSuchFieldError e185) {
            }
            try {
                iArr[APIKey.APIKey_USER_UNSUBSCRIBE.ordinal()] = 233;
            } catch (NoSuchFieldError e186) {
            }
            try {
                iArr[APIKey.APIKey_USER_UPDATE_GETUI_ID.ordinal()] = 234;
            } catch (NoSuchFieldError e187) {
            }
            try {
                iArr[APIKey.APIKey_USE_DEAL_WITH.ordinal()] = 190;
            } catch (NoSuchFieldError e188) {
            }
            try {
                iArr[APIKey.APIKey_UserBlack_Add.ordinal()] = 17;
            } catch (NoSuchFieldError e189) {
            }
            try {
                iArr[APIKey.APIKey_UserBlack_Check.ordinal()] = 19;
            } catch (NoSuchFieldError e190) {
            }
            try {
                iArr[APIKey.APIKey_UserBlack_Del.ordinal()] = 18;
            } catch (NoSuchFieldError e191) {
            }
            try {
                iArr[APIKey.APIKey_UserBlack_List.ordinal()] = 23;
            } catch (NoSuchFieldError e192) {
            }
            try {
                iArr[APIKey.APIKey_UserBlack_ShortList.ordinal()] = 24;
            } catch (NoSuchFieldError e193) {
            }
            try {
                iArr[APIKey.APIKey_UserFans.ordinal()] = 8;
            } catch (NoSuchFieldError e194) {
            }
            try {
                iArr[APIKey.APIKey_UserFollowList_Short.ordinal()] = 34;
            } catch (NoSuchFieldError e195) {
            }
            try {
                iArr[APIKey.APIKey_UserFollowers.ordinal()] = 9;
            } catch (NoSuchFieldError e196) {
            }
            try {
                iArr[APIKey.APIKey_UserFriends.ordinal()] = 10;
            } catch (NoSuchFieldError e197) {
            }
            try {
                iArr[APIKey.APIKey_UserGift.ordinal()] = 195;
            } catch (NoSuchFieldError e198) {
            }
            try {
                iArr[APIKey.APIKey_UserList_FavoriteMe.ordinal()] = 33;
            } catch (NoSuchFieldError e199) {
            }
            try {
                iArr[APIKey.APIKey_UserModify_Profile.ordinal()] = 40;
            } catch (NoSuchFieldError e200) {
            }
            try {
                iArr[APIKey.APIKey_UserRank.ordinal()] = 151;
            } catch (NoSuchFieldError e201) {
            }
            try {
                iArr[APIKey.APIKey_UserRelationship.ordinal()] = 13;
            } catch (NoSuchFieldError e202) {
            }
            try {
                iArr[APIKey.APIKey_UserRelationship_GetRelation.ordinal()] = 14;
            } catch (NoSuchFieldError e203) {
            }
            try {
                iArr[APIKey.APIKey_UserRelationship_SetFollow.ordinal()] = 15;
            } catch (NoSuchFieldError e204) {
            }
            try {
                iArr[APIKey.APIKey_UserRelationship_SetUnfollow.ordinal()] = 16;
            } catch (NoSuchFieldError e205) {
            }
            try {
                iArr[APIKey.APIKey_UserTop.ordinal()] = 47;
            } catch (NoSuchFieldError e206) {
            }
            try {
                iArr[APIKey.APIKey_UserWhite_Add.ordinal()] = 20;
            } catch (NoSuchFieldError e207) {
            }
            try {
                iArr[APIKey.APIKey_UserWhite_Check.ordinal()] = 22;
            } catch (NoSuchFieldError e208) {
            }
            try {
                iArr[APIKey.APIKey_UserWhite_Del.ordinal()] = 21;
            } catch (NoSuchFieldError e209) {
            }
            try {
                iArr[APIKey.APIKey_ValidDate.ordinal()] = 176;
            } catch (NoSuchFieldError e210) {
            }
            try {
                iArr[APIKey.APIKey_VerifySmsCode.ordinal()] = 178;
            } catch (NoSuchFieldError e211) {
            }
            try {
                iArr[APIKey.APIKey_WeiBoRank.ordinal()] = 152;
            } catch (NoSuchFieldError e212) {
            }
            try {
                iArr[APIKey.APIKey_WeiboList_AtMe.ordinal()] = 28;
            } catch (NoSuchFieldError e213) {
            }
            try {
                iArr[APIKey.APIKey_WeiboList_CommentMe.ordinal()] = 27;
            } catch (NoSuchFieldError e214) {
            }
            try {
                iArr[APIKey.APIKey_WeiboList_Favorite.ordinal()] = 31;
            } catch (NoSuchFieldError e215) {
            }
            try {
                iArr[APIKey.APIKey_WeiboList_Favorite_Short.ordinal()] = 32;
            } catch (NoSuchFieldError e216) {
            }
            try {
                iArr[APIKey.APIKey_WeiboList_Forward.ordinal()] = 37;
            } catch (NoSuchFieldError e217) {
            }
            try {
                iArr[APIKey.APIKey_WeiboList_Friends.ordinal()] = 29;
            } catch (NoSuchFieldError e218) {
            }
            try {
                iArr[APIKey.APIKey_WeiboList_Friends_Original.ordinal()] = 30;
            } catch (NoSuchFieldError e219) {
            }
            try {
                iArr[APIKey.APIKey_WeiboList_HotRecordHistory.ordinal()] = 114;
            } catch (NoSuchFieldError e220) {
            }
            try {
                iArr[APIKey.APIKey_WeiboList_HotRecordToday.ordinal()] = 113;
            } catch (NoSuchFieldError e221) {
            }
            try {
                iArr[APIKey.APIKey_WeiboList_MyComment.ordinal()] = 26;
            } catch (NoSuchFieldError e222) {
            }
            try {
                iArr[APIKey.APIKey_WeiboList_Post.ordinal()] = 25;
            } catch (NoSuchFieldError e223) {
            }
            try {
                iArr[APIKey.APIKey_WeiboList_Public.ordinal()] = 111;
            } catch (NoSuchFieldError e224) {
            }
            try {
                iArr[APIKey.APIKey_WeiboList_Reply.ordinal()] = 36;
            } catch (NoSuchFieldError e225) {
            }
            try {
                iArr[APIKey.APIKey_WeiboList_Top.ordinal()] = 112;
            } catch (NoSuchFieldError e226) {
            }
            try {
                iArr[APIKey.APIKey_Weibo_Checkfavorite.ordinal()] = 58;
            } catch (NoSuchFieldError e227) {
            }
            try {
                iArr[APIKey.APIKey_Weibo_Favorite.ordinal()] = 56;
            } catch (NoSuchFieldError e228) {
            }
            try {
                iArr[APIKey.APIKey_Weibo_Forward.ordinal()] = 54;
            } catch (NoSuchFieldError e229) {
            }
            try {
                iArr[APIKey.APIKey_Weibo_Like.ordinal()] = 59;
            } catch (NoSuchFieldError e230) {
            }
            try {
                iArr[APIKey.APIKey_Weibo_Listen.ordinal()] = 61;
            } catch (NoSuchFieldError e231) {
            }
            try {
                iArr[APIKey.APIKey_Weibo_Post.ordinal()] = 48;
            } catch (NoSuchFieldError e232) {
            }
            try {
                iArr[APIKey.APIKey_Weibo_Refresh.ordinal()] = 50;
            } catch (NoSuchFieldError e233) {
            }
            try {
                iArr[APIKey.APIKey_Weibo_Reply.ordinal()] = 53;
            } catch (NoSuchFieldError e234) {
            }
            try {
                iArr[APIKey.APIKey_Weibo_ReplyForward.ordinal()] = 55;
            } catch (NoSuchFieldError e235) {
            }
            try {
                iArr[APIKey.APIKey_Weibo_Show.ordinal()] = 51;
            } catch (NoSuchFieldError e236) {
            }
            try {
                iArr[APIKey.APIKey_Weibo_Show_Item.ordinal()] = 52;
            } catch (NoSuchFieldError e237) {
            }
            try {
                iArr[APIKey.APIKey_Weibo_Unfavorite.ordinal()] = 57;
            } catch (NoSuchFieldError e238) {
            }
            try {
                iArr[APIKey.APIkey_ListHeChang.ordinal()] = 143;
            } catch (NoSuchFieldError e239) {
            }
            try {
                iArr[APIKey.APKKey_Category_Match.ordinal()] = 73;
            } catch (NoSuchFieldError e240) {
            }
            c = iArr;
        }
        return iArr;
    }

    public static void a() {
    }

    public static String a(APIKey aPIKey) {
        switch (b()[aPIKey.ordinal()]) {
            case 3:
                return a("user_checknickname");
            case 4:
                return a("user_search");
            case 5:
                return a("user_info");
            case 6:
            case 10:
            case 26:
            case 33:
            case 37:
            case 44:
            case 46:
            case 52:
            case 63:
            case 64:
            case 77:
            case 80:
            case 81:
            case 86:
            case 111:
            case 112:
            case 115:
            case 117:
            case TransportMediator.KEYCODE_MEDIA_PLAY:
            case TransportMediator.KEYCODE_MEDIA_RECORD:
            case 134:
            case 137:
            case 138:
            case 139:
            case 141:
            case 146:
            case 150:
            case 151:
            case 152:
            case 153:
            case 154:
            case 158:
            case 160:
            case 164:
            case 166:
            case 167:
            case 168:
            case 225:
            default:
                return null;
            case 7:
                return a("user_simple_user_info");
            case 8:
                return a("user_fans");
            case 9:
                return a("user_follow");
            case 11:
                return a("user_getface");
            case 12:
            case 189:
                return a("level_levelimg");
            case 13:
            case 14:
                return a("user_relationship");
            case 15:
                return a("user_follownew");
            case 16:
                return a("user_followdel");
            case 17:
                return a("user_blacklistadd");
            case 18:
                return a("user_blacklistdel");
            case 19:
                return a("user_blacklistcheck");
            case 20:
                return a("live_addtowhitelist");
            case 21:
                return a("live_delfromwhitelist");
            case 22:
                return a("live_checkinwhitelist");
            case 23:
                return a("user_blacklist");
            case 24:
                return a("user_blacklistshort");
            case 25:
                return a("user_recent_topics");
            case 27:
                return a("user_commentme_topics");
            case 28:
                return a("user_atme_topics");
            case 29:
                return a("user_myfriend_topics");
            case 30:
                return a("user_myfriend_topics_filter");
            case 31:
                return a("user_myfavorite_topics");
            case 32:
                return a("user_myfavorite_topics_short");
            case 34:
                return a("user_focusfriend");
            case 35:
                return a("user_sysuser");
            case 36:
                return a("weibo_topic_comments");
            case 38:
                return a("user_login");
            case 39:
                return a("user_logout");
            case 40:
                return a("user_modify");
            case 41:
                return a("user_changepassword");
            case 42:
                return a("user_modifyface");
            case 43:
                return a("user_homeimage");
            case 45:
                return a("common_register");
            case 47:
                return a("hot_people");
            case 48:
            case 53:
            case 54:
            case 55:
                return a("weibo_new");
            case 49:
                return a("weibo_del");
            case 50:
            case 51:
                return a("weibo_show");
            case 56:
                return a("weibo_favorite");
            case 57:
                return a("weibo_unfavorite");
            case 58:
                return a("weibo_checkfavorite");
            case 59:
            case 60:
                return a("weibo_like");
            case 61:
                return a("common_listen_end");
            case 62:
                return a("category_board");
            case 65:
                return a("category_hot");
            case 66:
                return a("category_aod");
            case 67:
                return a("category_aod_live_new");
            case 68:
                return a("category_aod_yq_new");
            case 69:
                return a("common_banner");
            case 70:
                return a("common_stir");
            case 71:
                return a("common_stir_thumb");
            case 72:
                return a("category_toplist");
            case 73:
                return a("category_match");
            case 74:
                return a("album_user_album");
            case 75:
                return a("album_user_addimage");
            case 76:
                return a("album_user_delimage");
            case 78:
                return a("live_hotroom");
            case 79:
                return a("live_searchroom");
            case 82:
                return a("live_roomrank");
            case 83:
                return a("live_songscore");
            case 84:
                return a("live_getroominfo");
            case 85:
                return a("live_randomliveroom");
            case 87:
                return a("live_getrid");
            case 88:
                return a("common_property");
            case 89:
                return a("live_edit");
            case 90:
                return a("live_sigserver");
            case 91:
                return a("hall_hallserver");
            case 92:
                return a("hall_offlinemsg");
            case 93:
                return a("live_roomparam");
            case 94:
                return "http://android.aichang.cn/config/libcheck.php";
            case 95:
                return "http://api.aichang.cn/apiv5/url/getapk.php";
            case 96:
                return "http://api.aichang.cn/apiv5/hot/hotvideo.php";
            case 97:
                return "http://api.aichang.cn/apiv5/hot/hotmc.php";
            case 98:
                return "http://api.aichang.cn/apiv5/url/getsdkconfigv2.php";
            case 99:
                return "http://api.aichang.cn/apiv5/url/notify.php";
            case 100:
                return "http://api.aichang.cn/apiv5/hot/junior.php";
            case 101:
                return a("album_fc_album");
            case 102:
                return a("album_fc_addimage");
            case 103:
                return a("album_fc_delimage");
            case 104:
                return a("pm_add");
            case 105:
                return a("pm_getmessagelist");
            case 106:
                return a("pm_getmessage");
            case 107:
            case 108:
                return a("pm_delete");
            case 109:
                return a("pm_unreadinfo");
            case 110:
                return a("user_user_friends");
            case 113:
                return a("hot_today_topics");
            case 114:
                return a("hot_history_topics");
            case 116:
                return a("search_singer");
            case 118:
                return a("search_search_all");
            case 119:
                return a("search_song");
            case 120:
                return a("hot_banzou");
            case 121:
            case 125:
                return a("common_banzou_detail");
            case 122:
                return a("album_fc_cover");
            case 123:
                return a("album_fc_cover_data");
            case 124:
                return a("album_fc_addcover");
            case TransportMediator.KEYCODE_MEDIA_PAUSE:
            case 128:
                return a("upload_upload");
            case 129:
                return a("url_lyric");
            case 131:
                return a("url_song");
            case 132:
                return a("url_songpic");
            case 133:
                return a("url_singerpic");
            case 135:
                return a("url_netlyric");
            case 136:
                return a("url_song");
            case 140:
                return a("common_exchange");
            case 142:
                return a("album_user_album");
            case 143:
                return a("weibo_combined_topics");
            case 144:
                return a("user_bind");
            case 145:
                return a("user_unbind");
            case 147:
                return a("user_hechang_topics");
            case 148:
                return a("notify_notify2");
            case 149:
                return a("user_snsnotification");
            case 155:
                return a("log_activelog");
            case 156:
            case 157:
                return a("log_sharelog");
            case 159:
                return a("live_getbalance");
            case 161:
                return a("common_config");
            case 162:
                a("common_home");
                return a("common_homepage");
            case 163:
                return a("common_third");
            case 165:
                return a("common_flashscreen");
            case 169:
                return a("feedback_report");
            case 170:
                return a("feedback_lyrc_error");
            case 171:
                return a("feedback_lyrc_unalign");
            case 172:
                return a("feedback_banzou_lowquality");
            case 173:
                return a("live_upurl");
            case 174:
                return a("live_downurl");
            case 175:
                return a("common_validcode");
            case 176:
                return a("common_validate");
            case 177:
                return a("common_smscode");
            case 178:
                return a("common_validatesms");
            case 179:
                return a("common_smscode_resend");
            case 180:
                return a("common_exchangephone");
            case 181:
                return a("user_bindphone");
            case 182:
                return a("user_getface_sys");
            case 183:
                return a("live_getusers");
            case 184:
                return a("live_getmics");
            case 185:
                return a("live_whitelist");
            case 186:
                return a("live_multi_getmics");
            case 187:
                return a("level_roomdetail");
            case 188:
                return a("level_userdetail");
            case Downloads.STATUS_PENDING:
                return a("common_touchhelp");
            case Downloads.STATUS_PENDING_PAUSED:
                return a("live_vip_order");
            case Downloads.STATUS_RUNNING:
                return a("live_roomgift");
            case Downloads.STATUS_RUNNING_PAUSED:
                return a("live_buy_vip");
            case 194:
                return a("live_shout");
            case 195:
                return a("live_recentgift");
            case 196:
                return a("live_getmygiftgroup");
            case 197:
                return a("live_getgiftbyuser");
            case 198:
                return a("live_buygift");
            case 199:
                return a("live_giftlist");
            case Downloads.STATUS_SUCCESS:
                return a("live_trade_pay");
            case 201:
                return a("live_trade_get_order_status");
            case 202:
                return a("live_activity");
            case 203:
                return a("live_trade_get_user_order");
            case 204:
                return a("live_trade_get_trade_money");
            case 205:
                return a("live_trade_get_user_present");
            case 206:
                return a("live_followplay");
            case 207:
                return a("live_interest");
            case 208:
                return a("live_getusers_myfriend");
            case Constants.RESP_VCODE_ERR:
                return a("live_gifttopsbyfcid");
            case Constants.RESP_NO_PHONENUMBER:
                return a("family_add_family");
            case 211:
                return a("family_del_family");
            case 212:
                return a("family_edit_family");
            case 213:
                return a("family_add_apply");
            case 214:
                return a("family_del_apply");
            case 215:
                return a("family_apply_list");
            case 216:
                return a("family_list_member");
            case 217:
            case 218:
                return a("family_my_family");
            case 219:
                return a("family_get_family");
            case 220:
                return a("family_add_member");
            case 221:
                return a("family_del_member");
            case 222:
                return a("family_relation_family");
            case 223:
                return a("family_quit_family");
            case 224:
                return a("family_search_family");
            case 226:
                return a("live_kickuser");
            case 227:
                return a("room_admin_add_admin");
            case 228:
                return a("room_admin_del_admin");
            case 229:
                return a("live_liveroomlist");
            case 230:
                return a("live_viplist");
            case 231:
                return a("hot_today_selected");
            case 232:
                return a("user_subscribe");
            case 233:
                return a("user_unsubscribe");
            case 234:
                return a("user_update_getui_id");
            case 235:
                return a("user_enable_receive_subscription");
            case 236:
                return a("user_disable_receive_subscription");
            case 237:
                return a("user_subscription");
            case 238:
                return a("common_discovery");
            case 239:
                return a("user_user_setting");
            case 240:
                return "http://reg.aichang.cn/apiv5/public/register_third.php?cmd=register";
        }
    }

    public static String a(String str) {
        String str2 = f1187a.get(str);
        if (TextUtils.isEmpty(str2)) {
            return t.f1188a.get(str);
        }
        return str2;
    }
}
