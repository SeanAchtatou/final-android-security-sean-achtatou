package com.pocketmusic.kshare.requestobjs;

import cn.banshenggua.aichang.api.APIKey;
import cn.banshenggua.aichang.api.KURL;
import cn.banshenggua.aichang.app.KShareApplication;
import cn.banshenggua.aichang.room.message.SocketMessage;
import cn.banshenggua.aichang.sdk.ACException;
import cn.banshenggua.aichang.utils.PreferencesUtils;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

/* compiled from: SDKConfig */
public class q extends n {

    /* renamed from: a  reason: collision with root package name */
    public List<a> f1184a = new ArrayList();

    /* compiled from: SDKConfig */
    public enum b {
        Rooms,
        WeiBos,
        __NO_SUPPORT;

        public static b a(int i) {
            switch (i) {
                case 0:
                    return Rooms;
                case 1:
                    return WeiBos;
                default:
                    return __NO_SUPPORT;
            }
        }
    }

    /* compiled from: SDKConfig */
    public class a {

        /* renamed from: a  reason: collision with root package name */
        public b f1185a;
        public String b;
        public String c;

        public a(b bVar, String str, String str2) {
            this.b = str;
            this.f1185a = bVar;
            this.c = str2;
        }
    }

    public void a() {
        KURL kurl = new KURL();
        kurl.baseURL = s.a(APIKey.APIKey_SDK_Config);
        a(kurl, this.ar, APIKey.APIKey_SDK_Config, true);
    }

    public void a(JSONObject jSONObject) {
        JSONObject optJSONObject;
        JSONArray optJSONArray;
        b a2;
        if (jSONObject != null && !b(jSONObject) && (optJSONObject = jSONObject.optJSONObject(SocketMessage.MSG_RESULE_KEY)) != null && optJSONObject.length() != 0 && (optJSONArray = optJSONObject.optJSONArray("entrys")) != null && optJSONArray.length() != 0) {
            this.f1184a.clear();
            for (int i = 0; i < optJSONArray.length(); i++) {
                JSONObject optJSONObject2 = optJSONArray.optJSONObject(i);
                if (!(optJSONObject2 == null || (a2 = b.a(optJSONObject2.optInt("type"))) == b.__NO_SUPPORT)) {
                    this.f1184a.add(new a(a2, optJSONObject2.optString("title"), optJSONObject2.optString("url")));
                }
            }
            try {
                PreferencesUtils.savePrefBoolean(KShareApplication.getInstance().getApp(), PreferencesUtils.Entry_Market_Flag, optJSONObject.optBoolean("entrymarket", false));
            } catch (ACException e) {
                e.printStackTrace();
            }
        }
    }
}
