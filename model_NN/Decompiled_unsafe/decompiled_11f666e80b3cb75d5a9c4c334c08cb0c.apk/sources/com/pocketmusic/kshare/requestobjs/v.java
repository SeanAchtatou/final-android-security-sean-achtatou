package com.pocketmusic.kshare.requestobjs;

import android.text.TextUtils;
import cn.banshenggua.aichang.api.APIKey;
import cn.banshenggua.aichang.api.KURL;
import cn.banshenggua.aichang.room.message.SocketMessage;
import cn.banshenggua.aichang.utils.PreferencesUtils;
import cn.banshenggua.aichang.utils.ULog;
import com.pocketmusic.kshare.requestobjs.g;
import com.sina.weibo.sdk.constant.WBPageConstants;
import com.sina.weibo.sdk.exception.WeiboAuthException;
import com.sina.weibo.sdk.register.mobile.SelectCountryActivity;
import com.tencent.open.GameAppOperation;
import java.io.Serializable;
import java.net.URLEncoder;
import java.util.HashMap;
import org.json.JSONObject;

/* compiled from: WeiBo */
public class v extends n implements Serializable, Cloneable {
    private static /* synthetic */ int[] ab;
    private static /* synthetic */ int[] ac;
    public String A = "";
    public String B = "";
    public v C = null;
    public boolean D = false;
    public String E = "";
    public String F = "";
    public String G = "";
    public String H = "";
    public String I = "0";
    public String J = "0";
    public String K = "";
    public String L = "";
    public String M = "";
    public v N = null;
    public int O = 0;
    public boolean P = false;
    public String Q = "";
    public String R = "0";
    public a S = new a();
    public c T = c.Default;
    public Song U;
    public g.a V = null;
    public String W = "";
    public String X = "a";
    public String Y = "";
    public String Z = "";

    /* renamed from: a  reason: collision with root package name */
    public String f1190a = "";
    private String aa = "";
    public String b = "";
    public String c = "";
    public String d = "";
    public String e = "";
    public String f = "0";
    public String g = "0";
    public String h = WeiboAuthException.DEFAULT_AUTH_ERROR_CODE;
    public String i = "";
    public String j = "";
    public String k = "";
    public String l = "";
    public String m = "";
    public String n = "";
    public String o = "";
    public String p = "0";
    public String q = "";
    public String r = "";
    public String s = "";
    public String t = "";
    public String u = "";
    public String v = "";
    public int w = 0;
    public String x = "";
    public String y = "";
    public String z = "";

    /* compiled from: WeiBo */
    public enum a {
        NORMARE,
        INVITE,
        HECHANG,
        NONE
    }

    /* compiled from: WeiBo */
    public enum b {
        Audio,
        Video,
        Both
    }

    /* compiled from: WeiBo */
    public enum c {
        Default,
        writeWeibo,
        WriteComment,
        WriteForward,
        WriteBoth,
        InfoItem
    }

    static /* synthetic */ int[] n() {
        int[] iArr = ab;
        if (iArr == null) {
            iArr = new int[c.values().length];
            try {
                iArr[c.Default.ordinal()] = 1;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[c.InfoItem.ordinal()] = 6;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[c.WriteBoth.ordinal()] = 5;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[c.WriteComment.ordinal()] = 3;
            } catch (NoSuchFieldError e5) {
            }
            try {
                iArr[c.WriteForward.ordinal()] = 4;
            } catch (NoSuchFieldError e6) {
            }
            try {
                iArr[c.writeWeibo.ordinal()] = 2;
            } catch (NoSuchFieldError e7) {
            }
            ab = iArr;
        }
        return iArr;
    }

    static /* synthetic */ int[] o() {
        int[] iArr = ac;
        if (iArr == null) {
            iArr = new int[APIKey.values().length];
            try {
                iArr[APIKey.APIKEY_CrashLog.ordinal()] = 138;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[APIKey.APIKEY_GetUserAlbums.ordinal()] = 74;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[APIKey.APIKEY_LikeFanchang.ordinal()] = 60;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[APIKey.APIKEY_Modify_UserZoneHomePic.ordinal()] = 43;
            } catch (NoSuchFieldError e5) {
            }
            try {
                iArr[APIKey.APIKEY_SendLog.ordinal()] = 137;
            } catch (NoSuchFieldError e6) {
            }
            try {
                iArr[APIKey.APIKEY_UploadUserAlbums.ordinal()] = 75;
            } catch (NoSuchFieldError e7) {
            }
            try {
                iArr[APIKey.APIKey_ACTIVATE.ordinal()] = 155;
            } catch (NoSuchFieldError e8) {
            }
            try {
                iArr[APIKey.APIKey_AccountUpdateNotify.ordinal()] = 148;
            } catch (NoSuchFieldError e9) {
            }
            try {
                iArr[APIKey.APIKey_AccountUpdateSnsNotify.ordinal()] = 149;
            } catch (NoSuchFieldError e10) {
            }
            try {
                iArr[APIKey.APIKey_AddTo_Club.ordinal()] = 220;
            } catch (NoSuchFieldError e11) {
            }
            try {
                iArr[APIKey.APIKey_Add_Vice_Admin.ordinal()] = 227;
            } catch (NoSuchFieldError e12) {
            }
            try {
                iArr[APIKey.APIKey_BanZouUrl.ordinal()] = 139;
            } catch (NoSuchFieldError e13) {
            }
            try {
                iArr[APIKey.APIKey_BindSNS.ordinal()] = 144;
            } catch (NoSuchFieldError e14) {
            }
            try {
                iArr[APIKey.APIKey_Bind_Phone.ordinal()] = 181;
            } catch (NoSuchFieldError e15) {
            }
            try {
                iArr[APIKey.APIKey_BuyGift.ordinal()] = 198;
            } catch (NoSuchFieldError e16) {
            }
            try {
                iArr[APIKey.APIKey_COMMON_DISCOVERY.ordinal()] = 238;
            } catch (NoSuchFieldError e17) {
            }
            try {
                iArr[APIKey.APIKey_Category_AOD.ordinal()] = 66;
            } catch (NoSuchFieldError e18) {
            }
            try {
                iArr[APIKey.APIKey_Category_AOD_LIVE.ordinal()] = 67;
            } catch (NoSuchFieldError e19) {
            }
            try {
                iArr[APIKey.APIKey_Category_AOD_YQ.ordinal()] = 68;
            } catch (NoSuchFieldError e20) {
            }
            try {
                iArr[APIKey.APIKey_Category_TopList.ordinal()] = 72;
            } catch (NoSuchFieldError e21) {
            }
            try {
                iArr[APIKey.APIKey_Change_Phone.ordinal()] = 180;
            } catch (NoSuchFieldError e22) {
            }
            try {
                iArr[APIKey.APIKey_Channel.ordinal()] = 115;
            } catch (NoSuchFieldError e23) {
            }
            try {
                iArr[APIKey.APIKey_Channel_ByAll.ordinal()] = 118;
            } catch (NoSuchFieldError e24) {
            }
            try {
                iArr[APIKey.APIKey_Channel_ByPinyin.ordinal()] = 119;
            } catch (NoSuchFieldError e25) {
            }
            try {
                iArr[APIKey.APIKey_Channel_BySinger.ordinal()] = 116;
            } catch (NoSuchFieldError e26) {
            }
            try {
                iArr[APIKey.APIKey_Channel_BySongName.ordinal()] = 117;
            } catch (NoSuchFieldError e27) {
            }
            try {
                iArr[APIKey.APIKey_Channel_ByTopbanzou.ordinal()] = 120;
            } catch (NoSuchFieldError e28) {
            }
            try {
                iArr[APIKey.APIKey_Channel_Rank.ordinal()] = 153;
            } catch (NoSuchFieldError e29) {
            }
            try {
                iArr[APIKey.APIKey_CheckNickName.ordinal()] = 3;
            } catch (NoSuchFieldError e30) {
            }
            try {
                iArr[APIKey.APIKey_CheckUserName.ordinal()] = 2;
            } catch (NoSuchFieldError e31) {
            }
            try {
                iArr[APIKey.APIKey_Club_Add.ordinal()] = 210;
            } catch (NoSuchFieldError e32) {
            }
            try {
                iArr[APIKey.APIKey_Club_Apply_Add.ordinal()] = 213;
            } catch (NoSuchFieldError e33) {
            }
            try {
                iArr[APIKey.APIKey_Club_Apply_Del.ordinal()] = 214;
            } catch (NoSuchFieldError e34) {
            }
            try {
                iArr[APIKey.APIKey_Club_Apply_List.ordinal()] = 215;
            } catch (NoSuchFieldError e35) {
            }
            try {
                iArr[APIKey.APIKey_Club_Del.ordinal()] = 211;
            } catch (NoSuchFieldError e36) {
            }
            try {
                iArr[APIKey.APIKey_Club_Edit.ordinal()] = 212;
            } catch (NoSuchFieldError e37) {
            }
            try {
                iArr[APIKey.APIKey_Club_Relation.ordinal()] = 222;
            } catch (NoSuchFieldError e38) {
            }
            try {
                iArr[APIKey.APIKey_Club_User_List.ordinal()] = 216;
            } catch (NoSuchFieldError e39) {
            }
            try {
                iArr[APIKey.APIKey_Common_Property.ordinal()] = 88;
            } catch (NoSuchFieldError e40) {
            }
            try {
                iArr[APIKey.APIKey_DAY_EVENT.ordinal()] = 158;
            } catch (NoSuchFieldError e41) {
            }
            try {
                iArr[APIKey.APIKey_Default.ordinal()] = 1;
            } catch (NoSuchFieldError e42) {
            }
            try {
                iArr[APIKey.APIKey_DelTo_Club.ordinal()] = 221;
            } catch (NoSuchFieldError e43) {
            }
            try {
                iArr[APIKey.APIKey_Del_Vice_Admin.ordinal()] = 228;
            } catch (NoSuchFieldError e44) {
            }
            try {
                iArr[APIKey.APIKey_DeleteUserPics.ordinal()] = 76;
            } catch (NoSuchFieldError e45) {
            }
            try {
                iArr[APIKey.APIKey_DeleteUserWeibo.ordinal()] = 49;
            } catch (NoSuchFieldError e46) {
            }
            try {
                iArr[APIKey.APIKey_EVENT_SEND.ordinal()] = 154;
            } catch (NoSuchFieldError e47) {
            }
            try {
                iArr[APIKey.APIKey_Exchange.ordinal()] = 140;
            } catch (NoSuchFieldError e48) {
            }
            try {
                iArr[APIKey.APIKey_FanChang_Add_Cover.ordinal()] = 124;
            } catch (NoSuchFieldError e49) {
            }
            try {
                iArr[APIKey.APIKey_FanChang_Cover.ordinal()] = 122;
            } catch (NoSuchFieldError e50) {
            }
            try {
                iArr[APIKey.APIKey_FanChang_Cover_Data.ordinal()] = 123;
            } catch (NoSuchFieldError e51) {
            }
            try {
                iArr[APIKey.APIKey_FanChang_Info.ordinal()] = 121;
            } catch (NoSuchFieldError e52) {
            }
            try {
                iArr[APIKey.APIKey_FollowRoomList.ordinal()] = 80;
            } catch (NoSuchFieldError e53) {
            }
            try {
                iArr[APIKey.APIKey_Friends_In_ROOM.ordinal()] = 208;
            } catch (NoSuchFieldError e54) {
            }
            try {
                iArr[APIKey.APIKey_Friends_ROOM.ordinal()] = 206;
            } catch (NoSuchFieldError e55) {
            }
            try {
                iArr[APIKey.APIKey_Friends_Recommend_ROOM.ordinal()] = 207;
            } catch (NoSuchFieldError e56) {
            }
            try {
                iArr[APIKey.APIKey_GET_BALANCE.ordinal()] = 159;
            } catch (NoSuchFieldError e57) {
            }
            try {
                iArr[APIKey.APIKey_GET_FACE_SYSTEM.ordinal()] = 182;
            } catch (NoSuchFieldError e58) {
            }
            try {
                iArr[APIKey.APIKey_GUACHANG.ordinal()] = 63;
            } catch (NoSuchFieldError e59) {
            }
            try {
                iArr[APIKey.APIKey_GUACHANGList.ordinal()] = 150;
            } catch (NoSuchFieldError e60) {
            }
            try {
                iArr[APIKey.APIKey_GUACHANG_ITEM.ordinal()] = 64;
            } catch (NoSuchFieldError e61) {
            }
            try {
                iArr[APIKey.APIKey_GetAPK.ordinal()] = 95;
            } catch (NoSuchFieldError e62) {
            }
            try {
                iArr[APIKey.APIKey_GetAlbums.ordinal()] = 142;
            } catch (NoSuchFieldError e63) {
            }
            try {
                iArr[APIKey.APIKey_GetAllOrders.ordinal()] = 203;
            } catch (NoSuchFieldError e64) {
            }
            try {
                iArr[APIKey.APIKey_GetConsumeList.ordinal()] = 205;
            } catch (NoSuchFieldError e65) {
            }
            try {
                iArr[APIKey.APIKey_GetDialogUserInfo.ordinal()] = 7;
            } catch (NoSuchFieldError e66) {
            }
            try {
                iArr[APIKey.APIKey_GetFanChangURL.ordinal()] = 136;
            } catch (NoSuchFieldError e67) {
            }
            try {
                iArr[APIKey.APIKey_GetFixedRoomRank.ordinal()] = 82;
            } catch (NoSuchFieldError e68) {
            }
            try {
                iArr[APIKey.APIKey_GetLevelImgUrl.ordinal()] = 12;
            } catch (NoSuchFieldError e69) {
            }
            try {
                iArr[APIKey.APIKey_GetLiveDown.ordinal()] = 174;
            } catch (NoSuchFieldError e70) {
            }
            try {
                iArr[APIKey.APIKey_GetLiveUp.ordinal()] = 173;
            } catch (NoSuchFieldError e71) {
            }
            try {
                iArr[APIKey.APIKey_GetLyrcURL.ordinal()] = 129;
            } catch (NoSuchFieldError e72) {
            }
            try {
                iArr[APIKey.APIKey_GetMainCategory.ordinal()] = 65;
            } catch (NoSuchFieldError e73) {
            }
            try {
                iArr[APIKey.APIKey_GetMic_RoomList.ordinal()] = 229;
            } catch (NoSuchFieldError e74) {
            }
            try {
                iArr[APIKey.APIKey_GetNetLyrcURL.ordinal()] = 135;
            } catch (NoSuchFieldError e75) {
            }
            try {
                iArr[APIKey.APIKey_GetOrderInfo.ordinal()] = 200;
            } catch (NoSuchFieldError e76) {
            }
            try {
                iArr[APIKey.APIKey_GetOrderStatus.ordinal()] = 201;
            } catch (NoSuchFieldError e77) {
            }
            try {
                iArr[APIKey.APIKey_GetPayEventNotice.ordinal()] = 202;
            } catch (NoSuchFieldError e78) {
            }
            try {
                iArr[APIKey.APIKey_GetRoomList.ordinal()] = 77;
            } catch (NoSuchFieldError e79) {
            }
            try {
                iArr[APIKey.APIKey_GetRoomSongScore.ordinal()] = 83;
            } catch (NoSuchFieldError e80) {
            }
            try {
                iArr[APIKey.APIKey_GetSingerList.ordinal()] = 134;
            } catch (NoSuchFieldError e81) {
            }
            try {
                iArr[APIKey.APIKey_GetSingerPicURL.ordinal()] = 133;
            } catch (NoSuchFieldError e82) {
            }
            try {
                iArr[APIKey.APIKey_GetSmsCode.ordinal()] = 177;
            } catch (NoSuchFieldError e83) {
            }
            try {
                iArr[APIKey.APIKey_GetSongPicURL.ordinal()] = 132;
            } catch (NoSuchFieldError e84) {
            }
            try {
                iArr[APIKey.APIKey_GetSongURL.ordinal()] = 131;
            } catch (NoSuchFieldError e85) {
            }
            try {
                iArr[APIKey.APIKey_GetTradePriceList.ordinal()] = 204;
            } catch (NoSuchFieldError e86) {
            }
            try {
                iArr[APIKey.APIKey_GetUpdateInfo.ordinal()] = 141;
            } catch (NoSuchFieldError e87) {
            }
            try {
                iArr[APIKey.APIKey_GetUserFace.ordinal()] = 11;
            } catch (NoSuchFieldError e88) {
            }
            try {
                iArr[APIKey.APIKey_GetUserInfo.ordinal()] = 5;
            } catch (NoSuchFieldError e89) {
            }
            try {
                iArr[APIKey.APIKey_GetUserInfoItem.ordinal()] = 6;
            } catch (NoSuchFieldError e90) {
            }
            try {
                iArr[APIKey.APIKey_GetUserList.ordinal()] = 44;
            } catch (NoSuchFieldError e91) {
            }
            try {
                iArr[APIKey.APIKey_GetValidCode.ordinal()] = 175;
            } catch (NoSuchFieldError e92) {
            }
            try {
                iArr[APIKey.APIKey_GetVipList.ordinal()] = 230;
            } catch (NoSuchFieldError e93) {
            }
            try {
                iArr[APIKey.APIKey_Get_Animation_Config.ordinal()] = 164;
            } catch (NoSuchFieldError e94) {
            }
            try {
                iArr[APIKey.APIKey_Get_Club_Info.ordinal()] = 219;
            } catch (NoSuchFieldError e95) {
            }
            try {
                iArr[APIKey.APIKey_Get_Config.ordinal()] = 161;
            } catch (NoSuchFieldError e96) {
            }
            try {
                iArr[APIKey.APIKey_Get_My_Club.ordinal()] = 217;
            } catch (NoSuchFieldError e97) {
            }
            try {
                iArr[APIKey.APIKey_Get_My_Club_Tuijian.ordinal()] = 218;
            } catch (NoSuchFieldError e98) {
            }
            try {
                iArr[APIKey.APIKey_Get_Start_Pic.ordinal()] = 165;
            } catch (NoSuchFieldError e99) {
            }
            try {
                iArr[APIKey.APIKey_Get_Super_Gift_Fans.ordinal()] = 209;
            } catch (NoSuchFieldError e100) {
            }
            try {
                iArr[APIKey.APIKey_Get_Third_Config.ordinal()] = 163;
            } catch (NoSuchFieldError e101) {
            }
            try {
                iArr[APIKey.APIKey_GiftList.ordinal()] = 199;
            } catch (NoSuchFieldError e102) {
            }
            try {
                iArr[APIKey.APIKey_HallOfflineMessage.ordinal()] = 92;
            } catch (NoSuchFieldError e103) {
            }
            try {
                iArr[APIKey.APIKey_Home_Page.ordinal()] = 162;
            } catch (NoSuchFieldError e104) {
            }
            try {
                iArr[APIKey.APIKey_HotHMWeiBoSelect.ordinal()] = 97;
            } catch (NoSuchFieldError e105) {
            }
            try {
                iArr[APIKey.APIKey_HotRoomList.ordinal()] = 78;
            } catch (NoSuchFieldError e106) {
            }
            try {
                iArr[APIKey.APIKey_HotWeiBoSelect.ordinal()] = 96;
            } catch (NoSuchFieldError e107) {
            }
            try {
                iArr[APIKey.APIKey_Hot_Clubs.ordinal()] = 225;
            } catch (NoSuchFieldError e108) {
            }
            try {
                iArr[APIKey.APIKey_Hot_Program.ordinal()] = 70;
            } catch (NoSuchFieldError e109) {
            }
            try {
                iArr[APIKey.APIKey_Hot_Program_Thumb.ordinal()] = 71;
            } catch (NoSuchFieldError e110) {
            }
            try {
                iArr[APIKey.APIKey_Hot_Room_Page_AD.ordinal()] = 69;
            } catch (NoSuchFieldError e111) {
            }
            try {
                iArr[APIKey.APIKey_Hot_Today_Selected.ordinal()] = 231;
            } catch (NoSuchFieldError e112) {
            }
            try {
                iArr[APIKey.APIKey_Kick_User.ordinal()] = 226;
            } catch (NoSuchFieldError e113) {
            }
            try {
                iArr[APIKey.APIKey_Level_Img.ordinal()] = 189;
            } catch (NoSuchFieldError e114) {
            }
            try {
                iArr[APIKey.APIKey_Level_RoomDetail.ordinal()] = 187;
            } catch (NoSuchFieldError e115) {
            }
            try {
                iArr[APIKey.APIKey_Level_UserDetail.ordinal()] = 188;
            } catch (NoSuchFieldError e116) {
            }
            try {
                iArr[APIKey.APIKey_LibCheck.ordinal()] = 94;
            } catch (NoSuchFieldError e117) {
            }
            try {
                iArr[APIKey.APIKey_ListPeopleHeChang.ordinal()] = 147;
            } catch (NoSuchFieldError e118) {
            }
            try {
                iArr[APIKey.APIKey_LiveServer.ordinal()] = 90;
            } catch (NoSuchFieldError e119) {
            }
            try {
                iArr[APIKey.APIKey_LiveServerHall.ordinal()] = 91;
            } catch (NoSuchFieldError e120) {
            }
            try {
                iArr[APIKey.APIKey_Login.ordinal()] = 38;
            } catch (NoSuchFieldError e121) {
            }
            try {
                iArr[APIKey.APIKey_Logout.ordinal()] = 39;
            } catch (NoSuchFieldError e122) {
            }
            try {
                iArr[APIKey.APIKey_LyrcPath.ordinal()] = 130;
            } catch (NoSuchFieldError e123) {
            }
            try {
                iArr[APIKey.APIKey_Machine_Config.ordinal()] = 160;
            } catch (NoSuchFieldError e124) {
            }
            try {
                iArr[APIKey.APIKey_Modify_MyFace.ordinal()] = 42;
            } catch (NoSuchFieldError e125) {
            }
            try {
                iArr[APIKey.APIKey_Modify_PassWord.ordinal()] = 41;
            } catch (NoSuchFieldError e126) {
            }
            try {
                iArr[APIKey.APIKey_MyGiftByUser.ordinal()] = 197;
            } catch (NoSuchFieldError e127) {
            }
            try {
                iArr[APIKey.APIKey_MyGiftGroup.ordinal()] = 196;
            } catch (NoSuchFieldError e128) {
            }
            try {
                iArr[APIKey.APIKey_NEW_GUANGCHANG.ordinal()] = 62;
            } catch (NoSuchFieldError e129) {
            }
            try {
                iArr[APIKey.APIKey_New_PeopleSong.ordinal()] = 100;
            } catch (NoSuchFieldError e130) {
            }
            try {
                iArr[APIKey.APIKey_Notify_Config.ordinal()] = 99;
            } catch (NoSuchFieldError e131) {
            }
            try {
                iArr[APIKey.APIKey_Quit_Club.ordinal()] = 223;
            } catch (NoSuchFieldError e132) {
            }
            try {
                iArr[APIKey.APIKey_ReGetSmsCode.ordinal()] = 179;
            } catch (NoSuchFieldError e133) {
            }
            try {
                iArr[APIKey.APIKey_RecentLike.ordinal()] = 146;
            } catch (NoSuchFieldError e134) {
            }
            try {
                iArr[APIKey.APIKey_Register.ordinal()] = 45;
            } catch (NoSuchFieldError e135) {
            }
            try {
                iArr[APIKey.APIKey_Report.ordinal()] = 169;
            } catch (NoSuchFieldError e136) {
            }
            try {
                iArr[APIKey.APIKey_Report_Banzou_Lowquality.ordinal()] = 172;
            } catch (NoSuchFieldError e137) {
            }
            try {
                iArr[APIKey.APIKey_Report_Lyrc_Error.ordinal()] = 170;
            } catch (NoSuchFieldError e138) {
            }
            try {
                iArr[APIKey.APIKey_Report_Lyrc_Unalign.ordinal()] = 171;
            } catch (NoSuchFieldError e139) {
            }
            try {
                iArr[APIKey.APIKey_Rondom_Room_Info.ordinal()] = 85;
            } catch (NoSuchFieldError e140) {
            }
            try {
                iArr[APIKey.APIKey_RoomBuyVip.ordinal()] = 193;
            } catch (NoSuchFieldError e141) {
            }
            try {
                iArr[APIKey.APIKey_RoomGiftSpendTop.ordinal()] = 192;
            } catch (NoSuchFieldError e142) {
            }
            try {
                iArr[APIKey.APIKey_RoomHanHua.ordinal()] = 194;
            } catch (NoSuchFieldError e143) {
            }
            try {
                iArr[APIKey.APIKey_RoomParam.ordinal()] = 93;
            } catch (NoSuchFieldError e144) {
            }
            try {
                iArr[APIKey.APIKey_RoomRank.ordinal()] = 81;
            } catch (NoSuchFieldError e145) {
            }
            try {
                iArr[APIKey.APIKey_RoomVipList.ordinal()] = 191;
            } catch (NoSuchFieldError e146) {
            }
            try {
                iArr[APIKey.APIKey_Room_Create.ordinal()] = 87;
            } catch (NoSuchFieldError e147) {
            }
            try {
                iArr[APIKey.APIKey_Room_Edit.ordinal()] = 89;
            } catch (NoSuchFieldError e148) {
            }
            try {
                iArr[APIKey.APIKey_Room_GetMices.ordinal()] = 184;
            } catch (NoSuchFieldError e149) {
            }
            try {
                iArr[APIKey.APIKey_Room_GetMicesWhiteList.ordinal()] = 185;
            } catch (NoSuchFieldError e150) {
            }
            try {
                iArr[APIKey.APIKey_Room_GetUsers.ordinal()] = 183;
            } catch (NoSuchFieldError e151) {
            }
            try {
                iArr[APIKey.APIKey_Room_Info.ordinal()] = 84;
            } catch (NoSuchFieldError e152) {
            }
            try {
                iArr[APIKey.APIKey_Room_Info_Item.ordinal()] = 86;
            } catch (NoSuchFieldError e153) {
            }
            try {
                iArr[APIKey.APIKey_Room_Multi_GetMices.ordinal()] = 186;
            } catch (NoSuchFieldError e154) {
            }
            try {
                iArr[APIKey.APIKey_SDK_Config.ordinal()] = 98;
            } catch (NoSuchFieldError e155) {
            }
            try {
                iArr[APIKey.APIKey_SHARE_LOG.ordinal()] = 156;
            } catch (NoSuchFieldError e156) {
            }
            try {
                iArr[APIKey.APIKey_SHARE_SUCCESS_LOG.ordinal()] = 157;
            } catch (NoSuchFieldError e157) {
            }
            try {
                iArr[APIKey.APIKey_SMS_ADD.ordinal()] = 104;
            } catch (NoSuchFieldError e158) {
            }
            try {
                iArr[APIKey.APIKey_SMS_ALL.ordinal()] = 105;
            } catch (NoSuchFieldError e159) {
            }
            try {
                iArr[APIKey.APIKey_SMS_DEL_LIST.ordinal()] = 108;
            } catch (NoSuchFieldError e160) {
            }
            try {
                iArr[APIKey.APIKey_SMS_DEL_ONE.ordinal()] = 107;
            } catch (NoSuchFieldError e161) {
            }
            try {
                iArr[APIKey.APIKey_SMS_GET_NEW.ordinal()] = 109;
            } catch (NoSuchFieldError e162) {
            }
            try {
                iArr[APIKey.APIKey_SMS_PEOPLE.ordinal()] = 106;
            } catch (NoSuchFieldError e163) {
            }
            try {
                iArr[APIKey.APIKey_SearchRoom.ordinal()] = 79;
            } catch (NoSuchFieldError e164) {
            }
            try {
                iArr[APIKey.APIKey_SearchUser.ordinal()] = 4;
            } catch (NoSuchFieldError e165) {
            }
            try {
                iArr[APIKey.APIKey_Search_Club.ordinal()] = 224;
            } catch (NoSuchFieldError e166) {
            }
            try {
                iArr[APIKey.APIKey_Search_From_QQ_Weibo.ordinal()] = 168;
            } catch (NoSuchFieldError e167) {
            }
            try {
                iArr[APIKey.APIKey_Search_From_Sina.ordinal()] = 167;
            } catch (NoSuchFieldError e168) {
            }
            try {
                iArr[APIKey.APIKey_Search_From_Third.ordinal()] = 166;
            } catch (NoSuchFieldError e169) {
            }
            try {
                iArr[APIKey.APIKey_SetAuth.ordinal()] = 46;
            } catch (NoSuchFieldError e170) {
            }
            try {
                iArr[APIKey.APIKey_Song_AddImage.ordinal()] = 102;
            } catch (NoSuchFieldError e171) {
            }
            try {
                iArr[APIKey.APIKey_Song_Albums.ordinal()] = 101;
            } catch (NoSuchFieldError e172) {
            }
            try {
                iArr[APIKey.APIKey_Song_DelImage.ordinal()] = 103;
            } catch (NoSuchFieldError e173) {
            }
            try {
                iArr[APIKey.APIKey_Song_Download.ordinal()] = 126;
            } catch (NoSuchFieldError e174) {
            }
            try {
                iArr[APIKey.APIKey_Song_Info.ordinal()] = 125;
            } catch (NoSuchFieldError e175) {
            }
            try {
                iArr[APIKey.APIKey_Song_Upload.ordinal()] = 127;
            } catch (NoSuchFieldError e176) {
            }
            try {
                iArr[APIKey.APIKey_Song_Upload_Slice.ordinal()] = 128;
            } catch (NoSuchFieldError e177) {
            }
            try {
                iArr[APIKey.APIKey_SystemUserList_Short.ordinal()] = 35;
            } catch (NoSuchFieldError e178) {
            }
            try {
                iArr[APIKey.APIKey_UNBindSNS.ordinal()] = 145;
            } catch (NoSuchFieldError e179) {
            }
            try {
                iArr[APIKey.APIKey_USER_DISABLE_RECEIVE_SUBSCRIPTION.ordinal()] = 236;
            } catch (NoSuchFieldError e180) {
            }
            try {
                iArr[APIKey.APIKey_USER_ENABLE_RECEIVE_SUBSCRIPTION.ordinal()] = 235;
            } catch (NoSuchFieldError e181) {
            }
            try {
                iArr[APIKey.APIKey_USER_FRIENDS.ordinal()] = 110;
            } catch (NoSuchFieldError e182) {
            }
            try {
                iArr[APIKey.APIKey_USER_SUBSCRIBE.ordinal()] = 232;
            } catch (NoSuchFieldError e183) {
            }
            try {
                iArr[APIKey.APIKey_USER_SUBSCRIPTION.ordinal()] = 237;
            } catch (NoSuchFieldError e184) {
            }
            try {
                iArr[APIKey.APIKey_USER_THIRDCHANGE.ordinal()] = 240;
            } catch (NoSuchFieldError e185) {
            }
            try {
                iArr[APIKey.APIKey_USER_TUISONG_SETTING.ordinal()] = 239;
            } catch (NoSuchFieldError e186) {
            }
            try {
                iArr[APIKey.APIKey_USER_UNSUBSCRIBE.ordinal()] = 233;
            } catch (NoSuchFieldError e187) {
            }
            try {
                iArr[APIKey.APIKey_USER_UPDATE_GETUI_ID.ordinal()] = 234;
            } catch (NoSuchFieldError e188) {
            }
            try {
                iArr[APIKey.APIKey_USE_DEAL_WITH.ordinal()] = 190;
            } catch (NoSuchFieldError e189) {
            }
            try {
                iArr[APIKey.APIKey_UserBlack_Add.ordinal()] = 17;
            } catch (NoSuchFieldError e190) {
            }
            try {
                iArr[APIKey.APIKey_UserBlack_Check.ordinal()] = 19;
            } catch (NoSuchFieldError e191) {
            }
            try {
                iArr[APIKey.APIKey_UserBlack_Del.ordinal()] = 18;
            } catch (NoSuchFieldError e192) {
            }
            try {
                iArr[APIKey.APIKey_UserBlack_List.ordinal()] = 23;
            } catch (NoSuchFieldError e193) {
            }
            try {
                iArr[APIKey.APIKey_UserBlack_ShortList.ordinal()] = 24;
            } catch (NoSuchFieldError e194) {
            }
            try {
                iArr[APIKey.APIKey_UserFans.ordinal()] = 8;
            } catch (NoSuchFieldError e195) {
            }
            try {
                iArr[APIKey.APIKey_UserFollowList_Short.ordinal()] = 34;
            } catch (NoSuchFieldError e196) {
            }
            try {
                iArr[APIKey.APIKey_UserFollowers.ordinal()] = 9;
            } catch (NoSuchFieldError e197) {
            }
            try {
                iArr[APIKey.APIKey_UserFriends.ordinal()] = 10;
            } catch (NoSuchFieldError e198) {
            }
            try {
                iArr[APIKey.APIKey_UserGift.ordinal()] = 195;
            } catch (NoSuchFieldError e199) {
            }
            try {
                iArr[APIKey.APIKey_UserList_FavoriteMe.ordinal()] = 33;
            } catch (NoSuchFieldError e200) {
            }
            try {
                iArr[APIKey.APIKey_UserModify_Profile.ordinal()] = 40;
            } catch (NoSuchFieldError e201) {
            }
            try {
                iArr[APIKey.APIKey_UserRank.ordinal()] = 151;
            } catch (NoSuchFieldError e202) {
            }
            try {
                iArr[APIKey.APIKey_UserRelationship.ordinal()] = 13;
            } catch (NoSuchFieldError e203) {
            }
            try {
                iArr[APIKey.APIKey_UserRelationship_GetRelation.ordinal()] = 14;
            } catch (NoSuchFieldError e204) {
            }
            try {
                iArr[APIKey.APIKey_UserRelationship_SetFollow.ordinal()] = 15;
            } catch (NoSuchFieldError e205) {
            }
            try {
                iArr[APIKey.APIKey_UserRelationship_SetUnfollow.ordinal()] = 16;
            } catch (NoSuchFieldError e206) {
            }
            try {
                iArr[APIKey.APIKey_UserTop.ordinal()] = 47;
            } catch (NoSuchFieldError e207) {
            }
            try {
                iArr[APIKey.APIKey_UserWhite_Add.ordinal()] = 20;
            } catch (NoSuchFieldError e208) {
            }
            try {
                iArr[APIKey.APIKey_UserWhite_Check.ordinal()] = 22;
            } catch (NoSuchFieldError e209) {
            }
            try {
                iArr[APIKey.APIKey_UserWhite_Del.ordinal()] = 21;
            } catch (NoSuchFieldError e210) {
            }
            try {
                iArr[APIKey.APIKey_ValidDate.ordinal()] = 176;
            } catch (NoSuchFieldError e211) {
            }
            try {
                iArr[APIKey.APIKey_VerifySmsCode.ordinal()] = 178;
            } catch (NoSuchFieldError e212) {
            }
            try {
                iArr[APIKey.APIKey_WeiBoRank.ordinal()] = 152;
            } catch (NoSuchFieldError e213) {
            }
            try {
                iArr[APIKey.APIKey_WeiboList_AtMe.ordinal()] = 28;
            } catch (NoSuchFieldError e214) {
            }
            try {
                iArr[APIKey.APIKey_WeiboList_CommentMe.ordinal()] = 27;
            } catch (NoSuchFieldError e215) {
            }
            try {
                iArr[APIKey.APIKey_WeiboList_Favorite.ordinal()] = 31;
            } catch (NoSuchFieldError e216) {
            }
            try {
                iArr[APIKey.APIKey_WeiboList_Favorite_Short.ordinal()] = 32;
            } catch (NoSuchFieldError e217) {
            }
            try {
                iArr[APIKey.APIKey_WeiboList_Forward.ordinal()] = 37;
            } catch (NoSuchFieldError e218) {
            }
            try {
                iArr[APIKey.APIKey_WeiboList_Friends.ordinal()] = 29;
            } catch (NoSuchFieldError e219) {
            }
            try {
                iArr[APIKey.APIKey_WeiboList_Friends_Original.ordinal()] = 30;
            } catch (NoSuchFieldError e220) {
            }
            try {
                iArr[APIKey.APIKey_WeiboList_HotRecordHistory.ordinal()] = 114;
            } catch (NoSuchFieldError e221) {
            }
            try {
                iArr[APIKey.APIKey_WeiboList_HotRecordToday.ordinal()] = 113;
            } catch (NoSuchFieldError e222) {
            }
            try {
                iArr[APIKey.APIKey_WeiboList_MyComment.ordinal()] = 26;
            } catch (NoSuchFieldError e223) {
            }
            try {
                iArr[APIKey.APIKey_WeiboList_Post.ordinal()] = 25;
            } catch (NoSuchFieldError e224) {
            }
            try {
                iArr[APIKey.APIKey_WeiboList_Public.ordinal()] = 111;
            } catch (NoSuchFieldError e225) {
            }
            try {
                iArr[APIKey.APIKey_WeiboList_Reply.ordinal()] = 36;
            } catch (NoSuchFieldError e226) {
            }
            try {
                iArr[APIKey.APIKey_WeiboList_Top.ordinal()] = 112;
            } catch (NoSuchFieldError e227) {
            }
            try {
                iArr[APIKey.APIKey_Weibo_Checkfavorite.ordinal()] = 58;
            } catch (NoSuchFieldError e228) {
            }
            try {
                iArr[APIKey.APIKey_Weibo_Favorite.ordinal()] = 56;
            } catch (NoSuchFieldError e229) {
            }
            try {
                iArr[APIKey.APIKey_Weibo_Forward.ordinal()] = 54;
            } catch (NoSuchFieldError e230) {
            }
            try {
                iArr[APIKey.APIKey_Weibo_Like.ordinal()] = 59;
            } catch (NoSuchFieldError e231) {
            }
            try {
                iArr[APIKey.APIKey_Weibo_Listen.ordinal()] = 61;
            } catch (NoSuchFieldError e232) {
            }
            try {
                iArr[APIKey.APIKey_Weibo_Post.ordinal()] = 48;
            } catch (NoSuchFieldError e233) {
            }
            try {
                iArr[APIKey.APIKey_Weibo_Refresh.ordinal()] = 50;
            } catch (NoSuchFieldError e234) {
            }
            try {
                iArr[APIKey.APIKey_Weibo_Reply.ordinal()] = 53;
            } catch (NoSuchFieldError e235) {
            }
            try {
                iArr[APIKey.APIKey_Weibo_ReplyForward.ordinal()] = 55;
            } catch (NoSuchFieldError e236) {
            }
            try {
                iArr[APIKey.APIKey_Weibo_Show.ordinal()] = 51;
            } catch (NoSuchFieldError e237) {
            }
            try {
                iArr[APIKey.APIKey_Weibo_Show_Item.ordinal()] = 52;
            } catch (NoSuchFieldError e238) {
            }
            try {
                iArr[APIKey.APIKey_Weibo_Unfavorite.ordinal()] = 57;
            } catch (NoSuchFieldError e239) {
            }
            try {
                iArr[APIKey.APIkey_ListHeChang.ordinal()] = 143;
            } catch (NoSuchFieldError e240) {
            }
            try {
                iArr[APIKey.APKKey_Category_Match.ordinal()] = 73;
            } catch (NoSuchFieldError e241) {
            }
            ac = iArr;
        }
        return iArr;
    }

    public b a() {
        if (TextUtils.isEmpty(this.X) || this.X.equalsIgnoreCase("a")) {
            return b.Audio;
        }
        if (this.X.equalsIgnoreCase("v")) {
            return b.Video;
        }
        if (this.X.equalsIgnoreCase("b")) {
            return b.Both;
        }
        return b.Audio;
    }

    public String b() {
        if (TextUtils.isEmpty(this.aa)) {
            return this.i;
        }
        return this.aa;
    }

    public void a(String str) {
        this.aa = str;
    }

    public Object clone() throws CloneNotSupportedException {
        return (v) super.clone();
    }

    public boolean c() {
        try {
            if (Integer.parseInt(this.f1190a) > 0) {
                return false;
            }
            return true;
        } catch (Exception e2) {
            return true;
        }
    }

    public a d() {
        switch (e()) {
            case 0:
            case 3:
            case 6:
                return a.NORMARE;
            case 1:
            case 4:
            case 7:
                return a.INVITE;
            case 2:
            case 5:
            case 8:
                return a.HECHANG;
            default:
                return a.NONE;
        }
    }

    public int e() {
        if (this.e.equalsIgnoreCase("first")) {
            if (this.K.equalsIgnoreCase("1")) {
                return 1;
            }
            if (this.N != null) {
                return 2;
            }
            return 0;
        } else if (this.e.equalsIgnoreCase(PreferencesUtils.FORWARD) || this.e.equalsIgnoreCase("both")) {
            if (this.C != null && this.C.K.equalsIgnoreCase("1")) {
                return 4;
            }
            if (this.N != null) {
                return 5;
            }
            return 3;
        } else if (!this.e.equalsIgnoreCase("reply")) {
            return 0;
        } else {
            if (this.C != null && (this.C.e.equalsIgnoreCase("reply") || this.C.e.equalsIgnoreCase("both"))) {
                return 9;
            }
            if (this.C != null && this.C.K.equalsIgnoreCase("1")) {
                return 7;
            }
            if (this.N != null) {
                return 8;
            }
            return 6;
        }
    }

    public v f() {
        switch (e()) {
            case 0:
            case 1:
            case 2:
                return this;
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
                return this.C;
            default:
                return null;
        }
    }

    public v g() {
        switch (e()) {
            case 1:
                return this;
            case 2:
            case 5:
            case 8:
                return this.N;
            case 3:
            case 6:
            default:
                return null;
            case 4:
            case 7:
                return this.C;
        }
    }

    public Song h() {
        if (this.U == null) {
            this.U = k();
        }
        return this.U;
    }

    public Song k() {
        v vVar;
        Song song = new Song();
        if (this.C != null) {
            vVar = this.C;
        } else {
            vVar = this;
        }
        song.q = vVar.z;
        song.o = vVar.G;
        song.n = vVar.E;
        song.l = vVar.A;
        song.g = vVar.x;
        if (g() != null) {
            song.p = true;
        } else {
            song.p = false;
        }
        song.r = vVar.y;
        song.d = song.r;
        song.f = song.d;
        if (TextUtils.isEmpty(this.E)) {
            song.c = vVar.l();
        } else {
            song.c = this.E;
        }
        return song;
    }

    public String l() {
        return this.E;
    }

    public void m() {
        KURL kurl = new KURL();
        HashMap hashMap = new HashMap();
        int i2 = n()[this.T.ordinal()];
        kurl.baseURL = s.a(APIKey.APIKey_Weibo_Show);
        hashMap.put("id", this.f1190a);
        kurl.getParameter.putAll(hashMap);
        a(kurl, this.ar, APIKey.APIKey_Weibo_Show);
    }

    public String toString() {
        String str = "tid: " + this.f1190a + "; type: " + this.e + "; totid: " + this.h + "; content: " + this.d;
        if (this.C != null) {
            return String.valueOf(str) + "reply_tid: " + this.C.f1190a + "; reply_type: " + this.C.e;
        }
        return str;
    }

    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof v) || !this.f1190a.equals(((v) obj).f1190a)) {
            return false;
        }
        ULog.d("WeiBo", "add repeat weibo");
        return true;
    }

    private void p() {
        if (!this.e.equalsIgnoreCase("first")) {
            this.B = this.d;
            return;
        }
        String[] split = this.d.split("&");
        if (split.length > 4) {
            HashMap hashMap = new HashMap();
            for (String split2 : split) {
                String[] split3 = split2.split("=");
                if (split3.length > 1) {
                    hashMap.put(split3[0], split3[1]);
                }
            }
            if (hashMap.size() > 0) {
                this.A = (String) hashMap.get(SelectCountryActivity.EXTRA_COUNTRY_NAME);
                this.x = (String) hashMap.get("artist");
                this.z = (String) hashMap.get("bzid");
                this.y = (String) hashMap.get("fcid");
                this.B = (String) hashMap.get("real");
                this.X = (String) hashMap.get("media");
                if (this.B == null) {
                    this.B = "";
                }
                if (!(this.A == null || this.x == null || this.z == null || this.y == null)) {
                    HashMap hashMap2 = new HashMap();
                    hashMap2.clear();
                    hashMap2.put("type", "fcurl");
                    hashMap2.put("fcid", this.y);
                    this.E = KURL.urlEncode(s.a(APIKey.APIKey_GetSongURL), hashMap2, true);
                    hashMap2.clear();
                    hashMap2.put("size", "m");
                    hashMap2.put("artist", URLEncoder.encode(this.x));
                    this.F = KURL.urlEncode(s.a(APIKey.APIKey_GetSongPicURL), hashMap2, true);
                    hashMap2.clear();
                    hashMap2.put("bzid", this.z);
                    this.G = KURL.urlEncode(s.a(APIKey.APIKey_GetLyrcURL), hashMap2, true);
                    hashMap2.clear();
                    hashMap2.put("type", "fcurl");
                    hashMap2.put("fcid", this.y);
                    this.H = KURL.urlEncode(s.a(APIKey.APIKey_GetFanChangURL), hashMap2, true);
                    String str = "uid=" + this.b + "&fcid=" + this.y + "&artist=" + URLEncoder.encode(this.x) + "&show_user=" + (this.C == null ? "0" : "1");
                    this.t = KURL.urlEncode(s.a(APIKey.APIKey_FanChang_Cover), str);
                    this.u = KURL.urlEncode(s.a(APIKey.APIKey_FanChang_Cover_Data), str);
                    return;
                }
            }
        }
        this.B = this.d;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public void a(JSONObject jSONObject) {
        if (jSONObject != null && !b(jSONObject)) {
            JSONObject optJSONObject = jSONObject.optJSONObject(SocketMessage.MSG_RESULE_KEY);
            if (jSONObject.has(SocketMessage.MSG_RESULE_KEY)) {
                jSONObject = optJSONObject;
            }
            switch (o()[this.aq.ordinal()]) {
                case 25:
                case 27:
                case 28:
                case 29:
                case 30:
                case 31:
                case 36:
                case 37:
                case 96:
                case 97:
                case 100:
                case 111:
                case 112:
                case 113:
                case 114:
                case 143:
                case 147:
                case 152:
                case 231:
                    break;
                case 50:
                case 52:
                case 128:
                    if (jSONObject != null) {
                        this.f = jSONObject.optString("replys", this.f);
                        this.g = jSONObject.optString("forwards", this.g);
                        this.J = jSONObject.optString("liked_count", this.J);
                        this.I = jSONObject.optString("listened_count", this.I);
                        this.W = jSONObject.optString("auth", this.W);
                        this.p = jSONObject.optString("level", this.p);
                        this.q = jSONObject.optString("levelname", this.q);
                        break;
                    } else {
                        this.y = WeiboAuthException.DEFAULT_AUTH_ERROR_CODE;
                        return;
                    }
                case 51:
                    if (jSONObject != null) {
                        this.f = jSONObject.optString("replys", this.f);
                        this.g = jSONObject.optString("forwards", this.g);
                        this.J = jSONObject.optString("liked_count", this.J);
                        this.I = jSONObject.optString("listened_count", this.I);
                        this.W = jSONObject.optString("auth", this.W);
                        this.p = jSONObject.optString("level", this.p);
                        this.q = jSONObject.optString("levelname", this.q);
                        this.Z = jSONObject.optString("vip", this.Z);
                        return;
                    }
                    this.y = WeiboAuthException.DEFAULT_AUTH_ERROR_CODE;
                    return;
                case 60:
                    this.J = jSONObject.optString(WBPageConstants.ParamKey.COUNT, this.J);
                    return;
                default:
                    return;
            }
            this.f1190a = jSONObject.optString("tid");
            this.b = jSONObject.optString("uid", WeiboAuthException.DEFAULT_AUTH_ERROR_CODE);
            this.c = jSONObject.optString("username");
            this.d = jSONObject.optString("content");
            this.e = jSONObject.optString("type");
            this.f = jSONObject.optString("replys", "0");
            this.g = jSONObject.optString("forwards", "0");
            this.J = jSONObject.optString("liked_count", this.J);
            this.I = jSONObject.optString("listened_count", this.I);
            this.h = jSONObject.optString("totid");
            this.i = jSONObject.optString("nickname");
            a(jSONObject.optString("full", this.i));
            this.j = jSONObject.optString("addtime");
            this.k = jSONObject.optString("from_string");
            this.l = jSONObject.optString("is_vote");
            this.m = jSONObject.optString("face_url");
            this.n = jSONObject.optString("face");
            this.n = jSONObject.optString("cover_url", this.n);
            this.o = jSONObject.optString("validate");
            this.p = jSONObject.optString("level", "0");
            this.q = jSONObject.optString("levelname");
            this.r = jSONObject.optString("face_original");
            this.s = jSONObject.optString("face_small");
            this.v = jSONObject.optString("gender_ta");
            this.w = jSONObject.optInt("gender");
            this.Q = jSONObject.optString(GameAppOperation.GAME_SIGNATURE);
            this.R = jSONObject.optString("fans_count");
            if (this.h.equals("0")) {
                this.C = null;
            } else {
                JSONObject optJSONObject2 = jSONObject.optJSONObject("to_topics");
                if (optJSONObject2 != null && (optJSONObject2 instanceof JSONObject)) {
                    this.C = new v();
                    this.C.aq = this.aq;
                    this.C.a(optJSONObject2);
                }
            }
            p();
            if (jSONObject.has("is_invite")) {
                this.K = jSONObject.optString("is_invite");
            }
            if (jSONObject.has("hc_count")) {
                this.L = jSONObject.optString("hc_count");
            }
            if (jSONObject.has("yqid")) {
                this.M = jSONObject.optString("yqid");
            }
            if (jSONObject.has("combined_topics")) {
                JSONObject optJSONObject3 = jSONObject.optJSONObject("combined_topics");
                if (optJSONObject3 != null && (optJSONObject3 instanceof JSONObject)) {
                    this.N = new v();
                    this.N.aq = this.aq;
                    this.N.a(optJSONObject3);
                }
            } else {
                this.N = null;
            }
            this.W = jSONObject.optString("auth", "");
            this.Y = jSONObject.optString("icon", this.Y);
        }
    }
}
