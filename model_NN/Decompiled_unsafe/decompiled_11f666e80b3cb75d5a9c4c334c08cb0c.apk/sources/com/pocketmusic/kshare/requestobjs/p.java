package com.pocketmusic.kshare.requestobjs;

import cn.banshenggua.aichang.api.APIKey;
import cn.banshenggua.aichang.api.KURL;
import cn.banshenggua.aichang.room.message.SocketMessage;
import com.pocketmusic.kshare.requestobjs.g;
import com.sina.weibo.sdk.constant.WBPageConstants;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: RoomList */
public class p extends n implements Serializable {
    private static /* synthetic */ int[] p;

    /* renamed from: a  reason: collision with root package name */
    public int f1182a = 0;
    public int b = 20;
    public int c = 1;
    public int d = 0;
    public int e = 1;
    public int f = 0;
    public int g = 0;
    public int h = 20;
    public g.a i = null;
    public g j = null;
    public String k = "";
    public a l;
    public String m = null;
    public boolean n = true;
    private List<o> o = new ArrayList();

    /* compiled from: RoomList */
    public enum a {
        LIST,
        Rank,
        HOT,
        Search,
        Follow,
        GetMicRoomList,
        HotUrl
    }

    static /* synthetic */ int[] f() {
        int[] iArr = p;
        if (iArr == null) {
            iArr = new int[a.values().length];
            try {
                iArr[a.Follow.ordinal()] = 5;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[a.GetMicRoomList.ordinal()] = 6;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[a.HOT.ordinal()] = 3;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[a.HotUrl.ordinal()] = 7;
            } catch (NoSuchFieldError e5) {
            }
            try {
                iArr[a.LIST.ordinal()] = 1;
            } catch (NoSuchFieldError e6) {
            }
            try {
                iArr[a.Rank.ordinal()] = 2;
            } catch (NoSuchFieldError e7) {
            }
            try {
                iArr[a.Search.ordinal()] = 4;
            } catch (NoSuchFieldError e8) {
            }
            p = iArr;
        }
        return iArr;
    }

    public p(a aVar) {
        this.l = aVar;
    }

    public p(a aVar, String str) {
        this.l = aVar;
        this.m = str;
    }

    public void a() {
        this.e = 1;
        this.n = true;
        d();
        c();
    }

    public boolean b() {
        if (this.c > this.d) {
            return true;
        }
        return false;
    }

    public void c() {
        d();
        KURL kurl = new KURL();
        HashMap hashMap = new HashMap();
        hashMap.put(WBPageConstants.ParamKey.COUNT, String.valueOf(this.h));
        hashMap.put(WBPageConstants.ParamKey.PAGE, String.valueOf(this.e));
        switch (f()[this.l.ordinal()]) {
            case 3:
                kurl.baseURL = s.a(APIKey.APIKey_HotRoomList);
                hashMap.put("cmd", "hotroom");
                kurl.getParameter.putAll(hashMap);
                a(kurl, this.ar, APIKey.APIKey_HotRoomList);
                return;
            case 7:
                kurl.baseURL = this.m;
                kurl.getParameter.putAll(hashMap);
                a(kurl, this.ar, APIKey.APIKey_RoomRank);
                return;
            default:
                return;
        }
    }

    public void a(o oVar) {
        this.o.add(oVar);
    }

    public void d() {
        this.o.clear();
    }

    public List<o> e() {
        return this.o;
    }

    public void a(JSONObject jSONObject) {
        JSONObject optJSONObject;
        if (jSONObject != null && !b(jSONObject) && (optJSONObject = jSONObject.optJSONObject(SocketMessage.MSG_RESULE_KEY)) != null && optJSONObject.length() != 0) {
            this.f1182a = optJSONObject.optInt("total");
            this.b = optJSONObject.optInt(WBPageConstants.ParamKey.COUNT);
            this.d = optJSONObject.optInt(WBPageConstants.ParamKey.PAGE);
            this.c = optJSONObject.optInt("page_count");
            this.e = optJSONObject.optInt("page_next");
            this.f = optJSONObject.optInt("page_previous");
            this.g = optJSONObject.optInt(WBPageConstants.ParamKey.OFFSET);
            this.h = optJSONObject.optInt("limit", this.h);
            JSONArray optJSONArray = optJSONObject.optJSONArray("rooms");
            if (optJSONArray != null && optJSONArray.length() != 0) {
                int i2 = 0;
                while (i2 < optJSONArray.length()) {
                    try {
                        if (!(optJSONArray.get(i2) == null || optJSONArray.get(i2) == JSONObject.NULL)) {
                            o oVar = new o();
                            oVar.c((JSONObject) optJSONArray.get(i2));
                            a(oVar);
                        }
                        i2++;
                    } catch (JSONException e2) {
                        return;
                    }
                }
            }
        }
    }
}
