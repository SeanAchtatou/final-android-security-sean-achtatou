package net.lucode.hackware.magicindicator;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import net.lucode.hackware.magicindicator.a.a;

public class MagicIndicator extends FrameLayout {

    /* renamed from: a  reason: collision with root package name */
    private a f2999a;

    public MagicIndicator(Context context) {
        super(context);
    }

    public MagicIndicator(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public void a(int i, float f, int i2) {
        if (this.f2999a != null) {
            this.f2999a.a(i, f, i2);
        }
    }

    public void a(int i) {
        if (this.f2999a != null) {
            this.f2999a.a(i);
        }
    }

    public void b(int i) {
        if (this.f2999a != null) {
            this.f2999a.b(i);
        }
    }

    public a getNavigator() {
        return this.f2999a;
    }

    public void setNavigator(a aVar) {
        if (this.f2999a != aVar) {
            if (this.f2999a != null) {
                this.f2999a.b();
            }
            this.f2999a = aVar;
            removeAllViews();
            if (this.f2999a instanceof View) {
                addView((View) this.f2999a, new FrameLayout.LayoutParams(-1, -1));
                this.f2999a.a();
            }
        }
    }
}
