package net.lucode.hackware.magicindicator;

import android.annotation.TargetApi;
import java.util.List;

@TargetApi(11)
/* compiled from: FragmentContainerHelper */
public class a {
    public static net.lucode.hackware.magicindicator.b.a.c.a a(List<net.lucode.hackware.magicindicator.b.a.c.a> list, int i) {
        net.lucode.hackware.magicindicator.b.a.c.a aVar;
        if (i >= 0 && i <= list.size() - 1) {
            return list.get(i);
        }
        net.lucode.hackware.magicindicator.b.a.c.a aVar2 = new net.lucode.hackware.magicindicator.b.a.c.a();
        if (i < 0) {
            aVar = list.get(0);
        } else {
            i = (i - list.size()) + 1;
            aVar = list.get(list.size() - 1);
        }
        aVar2.f3005a = aVar.f3005a + (aVar.a() * i);
        aVar2.b = aVar.b;
        aVar2.c = aVar.c + (aVar.a() * i);
        aVar2.d = aVar.d;
        aVar2.e = aVar.e + (aVar.a() * i);
        aVar2.f = aVar.f;
        aVar2.g = aVar.g + (aVar.a() * i);
        aVar2.h = aVar.h;
        return aVar2;
    }
}
