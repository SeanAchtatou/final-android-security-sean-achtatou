package net.lucode.hackware.magicindicator;

import android.util.SparseArray;
import android.util.SparseBooleanArray;

/* compiled from: NavigatorHelper */
public class b {

    /* renamed from: a  reason: collision with root package name */
    private SparseBooleanArray f3000a = new SparseBooleanArray();
    private SparseArray<Float> b = new SparseArray<>();
    private int c;
    private int d;
    private int e;
    private float f;
    private int g;
    private boolean h;
    private a i;

    /* compiled from: NavigatorHelper */
    public interface a {
        void a(int i, int i2);

        void a(int i, int i2, float f, boolean z);

        void b(int i, int i2);

        void b(int i, int i2, float f, boolean z);
    }

    public void a(int i2, float f2, int i3) {
        boolean z;
        boolean z2;
        int i4;
        float f3 = ((float) i2) + f2;
        if (this.f <= f3) {
            z = true;
        } else {
            z = false;
        }
        if (this.g == 0) {
            for (int i5 = 0; i5 < this.c; i5++) {
                if (i5 != this.d) {
                    if (!this.f3000a.get(i5)) {
                        e(i5);
                    }
                    if (this.b.get(i5, Float.valueOf(0.0f)).floatValue() != 1.0f) {
                        b(i5, 1.0f, false, true);
                    }
                }
            }
            a(this.d, 1.0f, false, true);
            d(this.d);
        } else if (f3 != this.f) {
            int i6 = i2 + 1;
            if (f2 != 0.0f || !z) {
                z2 = true;
                i4 = i6;
            } else {
                z2 = false;
                i4 = i2 - 1;
            }
            for (int i7 = 0; i7 < this.c; i7++) {
                if (!(i7 == i2 || i7 == i4 || this.b.get(i7, Float.valueOf(0.0f)).floatValue() == 1.0f)) {
                    b(i7, 1.0f, z, true);
                }
            }
            if (!z2) {
                b(i4, 1.0f - f2, true, false);
                a(i2, 1.0f - f2, true, false);
            } else if (z) {
                b(i2, f2, true, false);
                a(i4, f2, true, false);
            } else {
                b(i4, 1.0f - f2, false, false);
                a(i2, 1.0f - f2, false, false);
            }
        } else {
            return;
        }
        this.f = f3;
    }

    private void a(int i2, float f2, boolean z, boolean z2) {
        if (this.h || i2 == this.d || this.g == 1 || z2) {
            if (this.i != null) {
                this.i.a(i2, this.c, f2, z);
            }
            this.b.put(i2, Float.valueOf(1.0f - f2));
        }
    }

    private void b(int i2, float f2, boolean z, boolean z2) {
        if (this.h || i2 == this.e || this.g == 1 || (((i2 == this.d - 1 || i2 == this.d + 1) && this.b.get(i2, Float.valueOf(0.0f)).floatValue() != 1.0f) || z2)) {
            if (this.i != null) {
                this.i.b(i2, this.c, f2, z);
            }
            this.b.put(i2, Float.valueOf(f2));
        }
    }

    private void d(int i2) {
        if (this.i != null) {
            this.i.a(i2, this.c);
        }
        this.f3000a.put(i2, false);
    }

    private void e(int i2) {
        if (this.i != null) {
            this.i.b(i2, this.c);
        }
        this.f3000a.put(i2, true);
    }

    public void a(int i2) {
        this.e = this.d;
        this.d = i2;
        d(this.d);
        for (int i3 = 0; i3 < this.c; i3++) {
            if (i3 != this.d && !this.f3000a.get(i3)) {
                e(i3);
            }
        }
    }

    public void b(int i2) {
        this.g = i2;
    }

    public void a(a aVar) {
        this.i = aVar;
    }

    public void a(boolean z) {
        this.h = z;
    }

    public int a() {
        return this.c;
    }

    public void c(int i2) {
        this.c = i2;
        this.f3000a.clear();
        this.b.clear();
    }

    public int b() {
        return this.d;
    }

    public int c() {
        return this.g;
    }
}
