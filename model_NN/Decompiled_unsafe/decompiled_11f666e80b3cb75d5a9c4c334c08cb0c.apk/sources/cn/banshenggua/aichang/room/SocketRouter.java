package cn.banshenggua.aichang.room;

import android.text.TextUtils;
import cn.banshenggua.aichang.room.message.ContextError;
import cn.banshenggua.aichang.room.message.SocketMessage;
import cn.banshenggua.aichang.utils.ULog;
import java.net.InetSocketAddress;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;
import org.apache.mina.core.future.ConnectFuture;
import org.apache.mina.core.future.WriteFuture;
import org.apache.mina.core.service.IoConnector;
import org.apache.mina.core.service.IoHandlerAdapter;
import org.apache.mina.core.session.IdleStatus;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.core.session.IoSessionConfig;
import org.apache.mina.filter.codec.ProtocolCodecFilter;
import org.apache.mina.filter.codec.textline.LineDelimiter;
import org.apache.mina.filter.codec.textline.TextLineCodecFactory;
import org.apache.mina.transport.socket.nio.NioSocketConnector;

public class SocketRouter extends IoHandlerAdapter {
    private static int Read_TIME_OUT = 5000;
    public static final String TAG = "SocketRouter";
    private static short TIME_OUT = 5000;
    IoConnector connector = null;
    private long currentMessageId = 0;
    private ConnectFuture mFuture = null;
    private String mHost = null;
    private OnSocketRouterListener mListener = null;
    private int mPort = 0;
    private IoSession mSession = null;
    private Map<String, SocketMessage> messageList = new HashMap();

    public interface OnSocketRouterListener {
        void OnMessageReceived(SocketMessage socketMessage);

        void OnSocketError(SocketMessage socketMessage);
    }

    public SocketRouter(String str, int i) {
        this.mHost = str;
        this.mPort = i;
        initSocketRouter();
    }

    private void initSocketRouter() {
        this.connector = null;
        this.connector = new NioSocketConnector();
        this.connector.setConnectTimeoutMillis((long) TIME_OUT);
        IoSessionConfig sessionConfig = this.connector.getSessionConfig();
        ULog.d(TAG, "config readeridletime: " + sessionConfig.getReaderIdleTime());
        sessionConfig.setReaderIdleTime(Read_TIME_OUT);
        ULog.d(TAG, "config readeridletime: " + sessionConfig.getReaderIdleTime());
        TextLineCodecFactory textLineCodecFactory = new TextLineCodecFactory(Charset.forName("UTF-8"), LineDelimiter.UNIX.getValue(), LineDelimiter.UNIX.getValue());
        textLineCodecFactory.setDecoderMaxLineLength(1048576);
        textLineCodecFactory.setEncoderMaxLineLength(1048576);
        this.connector.getFilterChain().addLast("codec", new ProtocolCodecFilter(textLineCodecFactory));
        this.connector.setHandler(this);
        ULog.d(TAG, "config readeridletime: " + this.connector.getSessionConfig().getReaderIdleTime());
    }

    public boolean reConnect() {
        initSocketRouter();
        return connect();
    }

    public boolean connect() {
        if (!TextUtils.isEmpty(this.mHost) && this.mPort <= 0) {
            return false;
        }
        try {
            this.mFuture = this.connector.connect(new InetSocketAddress(this.mHost, this.mPort));
            this.mFuture.awaitUninterruptibly();
            ULog.d(TAG, "getSession");
            this.mSession = this.mFuture.getSession();
            for (Object next : this.mSession.getAttributeKeys()) {
                ULog.d(TAG, "key: " + next + ": " + this.mSession.getAttribute(next));
            }
            return true;
        } catch (Exception e) {
            SocketMessage socketMessage = new SocketMessage();
            socketMessage.mError = new SocketMessage.MessageError();
            socketMessage.mError.setError(ContextError.SocketException);
            socketMessage.mError.setErrorString("网络异常");
            if (this.mListener == null) {
                return false;
            }
            this.mListener.OnSocketError(socketMessage);
            return false;
        }
    }

    public boolean disconnect() {
        this.messageList.clear();
        this.mListener = null;
        if (this.mSession != null) {
            this.mSession.close(true);
            this.mSession = null;
        }
        if (this.connector == null) {
            return false;
        }
        this.connector.dispose(true);
        this.connector = null;
        return false;
    }

    public boolean isActive() {
        if (this.connector != null && this.connector.isActive()) {
            return true;
        }
        ULog.d(TAG, "isActive false");
        if (this.mListener != null) {
            SocketMessage socketMessage = new SocketMessage();
            socketMessage.mError = new SocketMessage.MessageError();
            socketMessage.mError.setError(ContextError.SocketException);
            socketMessage.mError.setErrorString("isActive false");
            this.mListener.OnSocketError(socketMessage);
        }
        return false;
    }

    public synchronized void sendMessage(SocketMessage socketMessage, boolean z) {
        if (this.mSession != null) {
            ULog.d(TAG, "mSession = " + this.mSession + "; mSession.isConnected = " + this.mSession.isConnected());
        }
        if (isActive() && this.mSession != null && this.mSession.isConnected()) {
            processMessage(socketMessage, true, z);
            try {
                ULog.d(TAG, "sendMessage: write");
                WriteFuture write = this.mSession.write(socketMessage.mCommend.messageEncode());
                write.await();
                if (write.isWritten()) {
                    ULog.d(TAG, "sendMessage: write OK");
                } else {
                    ULog.d(TAG, "sendMessage: write error ", write.getException());
                }
            } catch (Exception e) {
                e.printStackTrace();
                ULog.d(TAG, "sendMessage: ", e);
            }
        }
        return;
    }

    private void processMessage(SocketMessage socketMessage, boolean z, boolean z2) {
        if (!z) {
            ULog.d(TAG, "message.mFinger: " + socketMessage.mFinger);
            if (!TextUtils.isEmpty(socketMessage.mFinger) && this.messageList.containsKey(socketMessage.mFinger)) {
                SocketMessage socketMessage2 = this.messageList.get(socketMessage.mFinger);
                if (socketMessage2 != null) {
                    socketMessage.mCommend = socketMessage2.mCommend;
                }
                ULog.d(TAG, "remove: " + socketMessage.mFinger);
                this.messageList.remove(socketMessage.mFinger);
            }
        } else if (z2) {
            this.currentMessageId++;
            socketMessage.mFinger = new StringBuilder().append(this.currentMessageId).toString();
            socketMessage.pushCommend(SocketMessage.MSG_FINGER_KEY, socketMessage.mFinger);
            this.messageList.put(socketMessage.mFinger, socketMessage);
        }
    }

    public void exceptionCaught(IoSession ioSession, Throwable th) throws Exception {
        super.exceptionCaught(ioSession, th);
        ULog.d(TAG, "exceptionCaught: " + th.getMessage());
        if (this.mListener != null) {
            SocketMessage socketMessage = new SocketMessage();
            socketMessage.mError = new SocketMessage.MessageError();
            socketMessage.mError.setError(ContextError.SocketException);
            socketMessage.mError.setErrorString(th.getMessage());
            this.mListener.OnSocketError(socketMessage);
        }
    }

    public void messageReceived(IoSession ioSession, Object obj) throws Exception {
        super.messageReceived(ioSession, obj);
        ULog.d(TAG, "messageReceived: " + obj);
        long currentTimeMillis = System.currentTimeMillis();
        SocketMessage createMessage = SocketMessage.createMessage((String) obj, null);
        createMessage.mBeginTime = currentTimeMillis;
        ULog.d(TAG, "msg.mFinger: " + createMessage.mFinger);
        if (!TextUtils.isEmpty(createMessage.mFinger)) {
            processMessage(createMessage, false, false);
        }
        if (this.mListener != null) {
            this.mListener.OnMessageReceived(createMessage);
        }
    }

    public void messageSent(IoSession ioSession, Object obj) throws Exception {
        super.messageSent(ioSession, obj);
        ULog.d(TAG, "messageSent: " + obj);
    }

    public void sessionClosed(IoSession ioSession) throws Exception {
        super.sessionClosed(ioSession);
        ULog.d(TAG, "sessionClosed");
        if (this.mListener != null) {
            SocketMessage socketMessage = new SocketMessage();
            socketMessage.mError = new SocketMessage.MessageError();
            socketMessage.mError.setError(ContextError.SocketClosed);
            this.mListener.OnSocketError(socketMessage);
        }
    }

    public void sessionCreated(IoSession ioSession) throws Exception {
        super.sessionCreated(ioSession);
        ULog.d(TAG, "sessionCreated: " + ioSession.getServiceAddress().toString() + "; " + ioSession.getLocalAddress().toString());
    }

    public void sessionIdle(IoSession ioSession, IdleStatus idleStatus) throws Exception {
        super.sessionIdle(ioSession, idleStatus);
        ULog.d(TAG, "sessionIdle: " + idleStatus);
    }

    public void sessionOpened(IoSession ioSession) throws Exception {
        super.sessionOpened(ioSession);
        ULog.d(TAG, "sessionOpened");
    }

    public OnSocketRouterListener getListener() {
        return this.mListener;
    }

    public void setListener(OnSocketRouterListener onSocketRouterListener) {
        this.mListener = onSocketRouterListener;
    }
}
