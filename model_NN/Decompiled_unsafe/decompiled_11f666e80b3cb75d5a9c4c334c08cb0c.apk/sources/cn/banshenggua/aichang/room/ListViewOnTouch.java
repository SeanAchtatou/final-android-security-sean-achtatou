package cn.banshenggua.aichang.room;

public interface ListViewOnTouch {
    boolean OnDrag();

    boolean OnDragDownMove();

    boolean OnDragUpMove();

    boolean OnDragXMove();
}
