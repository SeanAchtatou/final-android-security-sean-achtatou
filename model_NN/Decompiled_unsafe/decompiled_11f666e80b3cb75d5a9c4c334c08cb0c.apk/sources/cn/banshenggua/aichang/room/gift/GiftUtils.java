package cn.banshenggua.aichang.room.gift;

import cn.banshenggua.aichang.app.KShareApplication;
import cn.banshenggua.aichang.sdk.ACException;
import cn.banshenggua.aichang.utils.SharedPreferencesUtil;
import com.pocketmusic.kshare.requestobjs.e;
import com.pocketmusic.kshare.requestobjs.f;
import java.util.HashMap;
import java.util.List;

public class GiftUtils {
    public static String fileName = SharedPreferencesUtil.GiftCacheFile;
    public static HashMap<String, e> mExtendGifts = null;
    public static f mGifts = null;
    public static HashMap<String, e> mHashGifts = null;

    private static void InitGifts() {
        if (mHashGifts == null) {
            mHashGifts = new HashMap<>();
        }
        if (mHashGifts.size() == 0) {
            f fVar = null;
            try {
                fVar = (f) SharedPreferencesUtil.getObjectFromFile(KShareApplication.getInstance().getApp(), fileName);
            } catch (ACException e) {
                e.printStackTrace();
            }
            if (fVar != null) {
                for (int i = 0; i < fVar.g(); i++) {
                    e a2 = fVar.a(i);
                    if (a2 != null) {
                        mHashGifts.put(a2.b, a2);
                    }
                }
            }
        }
    }

    public static void resetHash() {
        if (mHashGifts != null) {
            mHashGifts.clear();
        }
    }

    public static e getGift(String str) {
        InitGifts();
        e eVar = mHashGifts.get(str);
        if (eVar != null || mExtendGifts == null) {
            return eVar;
        }
        return mExtendGifts.get(str);
    }

    public static void putGift(e eVar) {
        if (eVar != null) {
            if (mExtendGifts == null) {
                mExtendGifts = new HashMap<>();
            }
            mExtendGifts.put(eVar.b, eVar);
        }
    }

    public static void putGifts(List<e> list) {
        if (list != null && list.size() != 0) {
            if (mExtendGifts == null) {
                mExtendGifts = new HashMap<>();
            }
            for (e next : list) {
                mExtendGifts.put(next.b, next);
            }
        }
    }
}
