package cn.banshenggua.aichang.room.message;

import android.text.TextUtils;
import cn.banshenggua.aichang.room.message.ChatMessage;
import cn.banshenggua.aichang.room.message.GiftMessage;
import cn.banshenggua.aichang.room.message.MicMessage;
import cn.banshenggua.aichang.room.message.SimpleMessage;
import cn.banshenggua.aichang.room.message.UserMessage;
import com.pocketmusic.kshare.requestobjs.o;
import org.json.JSONObject;

public class LiveMessage {
    private static /* synthetic */ int[] $SWITCH_TABLE$cn$banshenggua$aichang$room$message$MessageKey;
    public long mBegin = 0;
    public long mEnd = 0;
    public Message_Flag mFlag = Message_Flag.Message_Normal;
    public MessageKey mKey;
    public String mMsg = null;
    public PanelType mPanel = PanelType.Public;
    public int mRid = -1;
    public long mTestMessageId = 0;
    public String mUid = "";
    public User mUser;

    public enum Message_Flag {
        Message_Normal,
        Message_ACK,
        Message_Broadcast,
        Message_Server,
        Message_Vehicle
    }

    static /* synthetic */ int[] $SWITCH_TABLE$cn$banshenggua$aichang$room$message$MessageKey() {
        int[] iArr = $SWITCH_TABLE$cn$banshenggua$aichang$room$message$MessageKey;
        if (iArr == null) {
            iArr = new int[MessageKey.values().length];
            try {
                iArr[MessageKey.MessageCenter_Private.ordinal()] = 32;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[MessageKey.Message_ACK.ordinal()] = 33;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[MessageKey.Message_ALL.ordinal()] = 2;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[MessageKey.Message_AdjustMic.ordinal()] = 24;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[MessageKey.Message_CancelMic.ordinal()] = 14;
            } catch (NoSuchFieldError e5) {
            }
            try {
                iArr[MessageKey.Message_ChangeBanzou.ordinal()] = 25;
            } catch (NoSuchFieldError e6) {
            }
            try {
                iArr[MessageKey.Message_ChangeMic.ordinal()] = 19;
            } catch (NoSuchFieldError e7) {
            }
            try {
                iArr[MessageKey.Message_CloseMic.ordinal()] = 18;
            } catch (NoSuchFieldError e8) {
            }
            try {
                iArr[MessageKey.Message_FAMILY_DISSOLVE.ordinal()] = 28;
            } catch (NoSuchFieldError e9) {
            }
            try {
                iArr[MessageKey.Message_Family.ordinal()] = 27;
            } catch (NoSuchFieldError e10) {
            }
            try {
                iArr[MessageKey.Message_GetMics.ordinal()] = 11;
            } catch (NoSuchFieldError e11) {
            }
            try {
                iArr[MessageKey.Message_GetUsers.ordinal()] = 10;
            } catch (NoSuchFieldError e12) {
            }
            try {
                iArr[MessageKey.Message_Gift.ordinal()] = 9;
            } catch (NoSuchFieldError e13) {
            }
            try {
                iArr[MessageKey.Message_GlobalGift.ordinal()] = 44;
            } catch (NoSuchFieldError e14) {
            }
            try {
                iArr[MessageKey.Message_HeartBeat.ordinal()] = 12;
            } catch (NoSuchFieldError e15) {
            }
            try {
                iArr[MessageKey.Message_Join.ordinal()] = 5;
            } catch (NoSuchFieldError e16) {
            }
            try {
                iArr[MessageKey.Message_KickUser.ordinal()] = 15;
            } catch (NoSuchFieldError e17) {
            }
            try {
                iArr[MessageKey.Message_Leave.ordinal()] = 6;
            } catch (NoSuchFieldError e18) {
            }
            try {
                iArr[MessageKey.Message_Login.ordinal()] = 3;
            } catch (NoSuchFieldError e19) {
            }
            try {
                iArr[MessageKey.Message_Logout.ordinal()] = 4;
            } catch (NoSuchFieldError e20) {
            }
            try {
                iArr[MessageKey.Message_Media.ordinal()] = 20;
            } catch (NoSuchFieldError e21) {
            }
            try {
                iArr[MessageKey.Message_MultiCancelMic.ordinal()] = 43;
            } catch (NoSuchFieldError e22) {
            }
            try {
                iArr[MessageKey.Message_MultiChangeMic.ordinal()] = 40;
            } catch (NoSuchFieldError e23) {
            }
            try {
                iArr[MessageKey.Message_MultiCloseMic.ordinal()] = 37;
            } catch (NoSuchFieldError e24) {
            }
            try {
                iArr[MessageKey.Message_MultiConfirmMic.ordinal()] = 38;
            } catch (NoSuchFieldError e25) {
            }
            try {
                iArr[MessageKey.Message_MultiDelMic.ordinal()] = 35;
            } catch (NoSuchFieldError e26) {
            }
            try {
                iArr[MessageKey.Message_MultiInviteMic.ordinal()] = 36;
            } catch (NoSuchFieldError e27) {
            }
            try {
                iArr[MessageKey.Message_MultiMedia.ordinal()] = 39;
            } catch (NoSuchFieldError e28) {
            }
            try {
                iArr[MessageKey.Message_MultiOpenMic.ordinal()] = 41;
            } catch (NoSuchFieldError e29) {
            }
            try {
                iArr[MessageKey.Message_MultiReqMic.ordinal()] = 34;
            } catch (NoSuchFieldError e30) {
            }
            try {
                iArr[MessageKey.Message_MuteRoom.ordinal()] = 21;
            } catch (NoSuchFieldError e31) {
            }
            try {
                iArr[MessageKey.Message_MuteUser.ordinal()] = 16;
            } catch (NoSuchFieldError e32) {
            }
            try {
                iArr[MessageKey.Message_Null.ordinal()] = 1;
            } catch (NoSuchFieldError e33) {
            }
            try {
                iArr[MessageKey.Message_OpenMic.ordinal()] = 17;
            } catch (NoSuchFieldError e34) {
            }
            try {
                iArr[MessageKey.Message_ReqMic.ordinal()] = 13;
            } catch (NoSuchFieldError e35) {
            }
            try {
                iArr[MessageKey.Message_RoomMod.ordinal()] = 29;
            } catch (NoSuchFieldError e36) {
            }
            try {
                iArr[MessageKey.Message_RoomParam.ordinal()] = 31;
            } catch (NoSuchFieldError e37) {
            }
            try {
                iArr[MessageKey.Message_RoomUpdate.ordinal()] = 26;
            } catch (NoSuchFieldError e38) {
            }
            try {
                iArr[MessageKey.Message_RtmpProp.ordinal()] = 42;
            } catch (NoSuchFieldError e39) {
            }
            try {
                iArr[MessageKey.Message_STalk.ordinal()] = 8;
            } catch (NoSuchFieldError e40) {
            }
            try {
                iArr[MessageKey.Message_ServerError.ordinal()] = 22;
            } catch (NoSuchFieldError e41) {
            }
            try {
                iArr[MessageKey.Message_ServerSys.ordinal()] = 23;
            } catch (NoSuchFieldError e42) {
            }
            try {
                iArr[MessageKey.Message_Talk.ordinal()] = 7;
            } catch (NoSuchFieldError e43) {
            }
            try {
                iArr[MessageKey.Message_Toast.ordinal()] = 30;
            } catch (NoSuchFieldError e44) {
            }
            $SWITCH_TABLE$cn$banshenggua$aichang$room$message$MessageKey = iArr;
        }
        return iArr;
    }

    public enum PanelType {
        Public("public"),
        TalkTo("talkto"),
        Gift("gift");
        
        private String value = "";

        private PanelType(String str) {
            this.value = str;
        }

        public String getValue() {
            return this.value;
        }

        public static PanelType getType(String str) {
            if (TextUtils.isEmpty(str)) {
                return Public;
            }
            if (str.equalsIgnoreCase(Public.value)) {
                return Public;
            }
            if (str.equalsIgnoreCase(TalkTo.value)) {
                return TalkTo;
            }
            if (str.equalsIgnoreCase(Gift.value)) {
                return Gift;
            }
            return Public;
        }
    }

    public LiveMessage(o oVar) {
        this.mUser = User.getCurrentUser(oVar);
    }

    public SocketMessage getSocketMessage() {
        SocketMessage socketMessage = new SocketMessage();
        if (!TextUtils.isEmpty(this.mKey.getKey())) {
            socketMessage.pushCommend("cmd", this.mKey.getKey());
        }
        if (this.mUser != null) {
            socketMessage.pushCommend("uid", this.mUser.mUid);
            if (this.mUser.mRoom != null) {
                socketMessage.pushCommend("room", this.mUser.mRoom.f1178a);
            }
        }
        return socketMessage;
    }

    public void parseOut(JSONObject jSONObject) {
        if (jSONObject != null) {
            this.mMsg = jSONObject.toString();
            String optString = jSONObject.optString("flag", "");
            if (optString.equalsIgnoreCase("ack")) {
                this.mFlag = Message_Flag.Message_ACK;
            }
            if (optString.equalsIgnoreCase("broadcast")) {
                this.mFlag = Message_Flag.Message_Broadcast;
            }
            if (optString.equalsIgnoreCase("vehicle")) {
                this.mFlag = Message_Flag.Message_Vehicle;
            } else if (jSONObject.optString("cmd", "").equalsIgnoreCase("server")) {
                this.mFlag = Message_Flag.Message_Server;
            }
            this.mRid = jSONObject.optInt("rid", -1);
            this.mUid = jSONObject.optString("uid", "");
            this.mPanel = PanelType.getType(jSONObject.optString("panel", ""));
        }
    }

    public static LiveMessage parseResult(String str) {
        return null;
    }

    public static LiveMessage parseResult(JSONObject jSONObject) {
        LiveMessage giftMessage;
        if (jSONObject == null) {
            return null;
        }
        switch ($SWITCH_TABLE$cn$banshenggua$aichang$room$message$MessageKey()[getMessageKey(jSONObject.optString("type", "")).ordinal()]) {
            case 3:
                giftMessage = new SimpleMessage(SimpleMessage.SimpleType.Message_Login, null);
                giftMessage.parseOut(jSONObject);
                break;
            case 4:
                giftMessage = new SimpleMessage(SimpleMessage.SimpleType.Message_Logout, null);
                giftMessage.parseOut(jSONObject);
                break;
            case 5:
                giftMessage = new SimpleMessage(SimpleMessage.SimpleType.Message_Join, null);
                giftMessage.parseOut(jSONObject);
                break;
            case 6:
                giftMessage = new SimpleMessage(SimpleMessage.SimpleType.Message_Leave, null);
                giftMessage.parseOut(jSONObject);
                break;
            case 7:
                giftMessage = new ChatMessage(ChatMessage.ChatType.Chat_Public, null);
                giftMessage.parseOut(jSONObject);
                break;
            case 8:
                giftMessage = new ChatMessage(ChatMessage.ChatType.Chat_Secret, null);
                giftMessage.parseOut(jSONObject);
                break;
            case 9:
                giftMessage = new GiftMessage(GiftMessage.GiftMessageType.Message_Gift, null);
                giftMessage.parseOut(jSONObject);
                break;
            case 10:
                giftMessage = new UserMessage(UserMessage.UserMessageType.User_List, null);
                giftMessage.parseOut(jSONObject);
                break;
            case 11:
                giftMessage = new MicMessage(MicMessage.MicType.GetMicList, null);
                giftMessage.parseOut(jSONObject);
                break;
            case 12:
            case 16:
            case 33:
            case 42:
            default:
                giftMessage = null;
                break;
            case 13:
                giftMessage = new MicMessage(MicMessage.MicType.ReqMic, null);
                giftMessage.parseOut(jSONObject);
                break;
            case 14:
                giftMessage = new MicMessage(MicMessage.MicType.CancelMic, null);
                giftMessage.parseOut(jSONObject);
                break;
            case 15:
                giftMessage = new SimpleMessage(SimpleMessage.SimpleType.Message_Kick, null);
                giftMessage.parseOut(jSONObject);
                break;
            case 17:
                giftMessage = new MicMessage(MicMessage.MicType.OpenMic, null);
                giftMessage.parseOut(jSONObject);
                break;
            case 18:
                giftMessage = new MicMessage(MicMessage.MicType.CloseMic, null);
                giftMessage.parseOut(jSONObject);
                break;
            case 19:
                giftMessage = new MicMessage(MicMessage.MicType.ChangeMic, null);
                giftMessage.parseOut(jSONObject);
                break;
            case 20:
                giftMessage = new SimpleMessage(SimpleMessage.SimpleType.Message_Media, null);
                giftMessage.parseOut(jSONObject);
                break;
            case 21:
                giftMessage = new SimpleMessage(SimpleMessage.SimpleType.Message_MuteRoom, null);
                giftMessage.parseOut(jSONObject);
                break;
            case 22:
                giftMessage = new SimpleMessage(SimpleMessage.SimpleType.Message_Error, null);
                giftMessage.parseOut(jSONObject);
                break;
            case 23:
                giftMessage = new SimpleMessage(SimpleMessage.SimpleType.Message_Sys, null);
                giftMessage.parseOut(jSONObject);
                break;
            case 24:
                giftMessage = new MicMessage(MicMessage.MicType.AdjustMic, null);
                giftMessage.parseOut(jSONObject);
                break;
            case 25:
                giftMessage = new MicMessage(MicMessage.MicType.ChangeBanzou, null);
                giftMessage.parseOut(jSONObject);
                break;
            case 26:
                giftMessage = new SimpleMessage(SimpleMessage.SimpleType.Message_RoomUpdate, null);
                giftMessage.parseOut(jSONObject);
                break;
            case 27:
                giftMessage = new ClubMessage(null);
                giftMessage.parseOut(jSONObject);
                break;
            case 28:
                giftMessage = new SimpleMessage(SimpleMessage.SimpleType.Message_FAMILY_DISSOLVE, null);
                giftMessage.parseOut(jSONObject);
                break;
            case 29:
                giftMessage = new SimpleMessage(SimpleMessage.SimpleType.Message_RoomMod, null);
                giftMessage.parseOut(jSONObject);
                break;
            case 30:
                giftMessage = new SimpleMessage(SimpleMessage.SimpleType.Message_Toast, null);
                giftMessage.parseOut(jSONObject);
                break;
            case 31:
                giftMessage = new SimpleMessage(SimpleMessage.SimpleType.Message_RoomParam, null);
                giftMessage.parseOut(jSONObject);
                break;
            case 32:
                giftMessage = new PrivateMessage(null);
                giftMessage.parseOut(jSONObject);
                break;
            case 34:
                giftMessage = new MicMessage(MicMessage.MicType.MultiReqMic, null);
                giftMessage.parseOut(jSONObject);
                break;
            case 35:
                giftMessage = new MicMessage(MicMessage.MicType.MultiDelMic, null);
                giftMessage.parseOut(jSONObject);
                break;
            case 36:
                giftMessage = new MicMessage(MicMessage.MicType.MultiInviteMic, null);
                giftMessage.parseOut(jSONObject);
                break;
            case 37:
                giftMessage = new MicMessage(MicMessage.MicType.MultiCloseMic, null);
                giftMessage.parseOut(jSONObject);
                break;
            case 38:
                giftMessage = new MicMessage(MicMessage.MicType.MultiConfirmMic, null);
                giftMessage.parseOut(jSONObject);
                break;
            case 39:
                giftMessage = new SimpleMessage(SimpleMessage.SimpleType.Message_MultiMedia, null);
                giftMessage.parseOut(jSONObject);
                break;
            case 40:
                giftMessage = new MicMessage(MicMessage.MicType.MultiChangeMic, null);
                giftMessage.parseOut(jSONObject);
                break;
            case 41:
                giftMessage = new MicMessage(MicMessage.MicType.MultiOpenMic, null);
                giftMessage.parseOut(jSONObject);
                break;
            case 43:
                giftMessage = new MicMessage(MicMessage.MicType.MultiCancelMic, null);
                giftMessage.parseOut(jSONObject);
                break;
            case 44:
                giftMessage = new GiftMessage(GiftMessage.GiftMessageType.Message_GlobalGift, null);
                giftMessage.parseOut(jSONObject);
                break;
        }
        return giftMessage;
    }

    public static MessageKey getMessageKey(String str) {
        if (!TextUtils.isEmpty(str)) {
            for (MessageKey messageKey : MessageKey.values()) {
                if (str.equalsIgnoreCase(messageKey.getKey())) {
                    return messageKey;
                }
            }
        }
        return MessageKey.Message_Null;
    }
}
