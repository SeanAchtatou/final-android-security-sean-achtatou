package cn.banshenggua.aichang.room;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.PowerManager;
import android.support.v4.view.ViewPager;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import cn.a.a.a;
import cn.banshenggua.aichang.app.DownloadService;
import cn.banshenggua.aichang.app.KShareApplication;
import cn.banshenggua.aichang.app.Session;
import cn.banshenggua.aichang.entry.ArrayListAdapter;
import cn.banshenggua.aichang.room.EventMessage;
import cn.banshenggua.aichang.room.LiveController;
import cn.banshenggua.aichang.room.LiveObject;
import cn.banshenggua.aichang.room.LiveObjectController;
import cn.banshenggua.aichang.room.SocketRouter;
import cn.banshenggua.aichang.room.gift.GiftUtils;
import cn.banshenggua.aichang.room.message.ClubMessage;
import cn.banshenggua.aichang.room.message.ContextError;
import cn.banshenggua.aichang.room.message.GiftMessage;
import cn.banshenggua.aichang.room.message.LiveMessage;
import cn.banshenggua.aichang.room.message.MessageKey;
import cn.banshenggua.aichang.room.message.MicMessage;
import cn.banshenggua.aichang.room.message.Rtmp;
import cn.banshenggua.aichang.room.message.SimpleMessage;
import cn.banshenggua.aichang.room.message.SocketMessage;
import cn.banshenggua.aichang.room.message.User;
import cn.banshenggua.aichang.sdk.ACException;
import cn.banshenggua.aichang.ui.BaseFragmentActivity;
import cn.banshenggua.aichang.ui.DownloadFragment;
import cn.banshenggua.aichang.utils.CommonUtil;
import cn.banshenggua.aichang.utils.KShareUtil;
import cn.banshenggua.aichang.utils.PreferencesUtils;
import cn.banshenggua.aichang.utils.SharedPreferencesUtil;
import cn.banshenggua.aichang.utils.Toaster;
import cn.banshenggua.aichang.utils.UIUtil;
import cn.banshenggua.aichang.utils.ULog;
import com.pocketmusic.kshare.dialog.MyDialogFragment;
import com.pocketmusic.kshare.requestobjs.e;
import com.pocketmusic.kshare.requestobjs.f;
import com.pocketmusic.kshare.requestobjs.j;
import com.pocketmusic.kshare.requestobjs.k;
import com.pocketmusic.kshare.requestobjs.o;
import com.pocketmusic.kshare.requestobjs.r;
import eu.inmite.android.lib.dialogs.b;
import eu.inmite.android.lib.dialogs.c;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.json.JSONObject;

@SuppressLint({"CutPasteId", "InflateParams"})
public class SimpleLiveRoomActivity extends BaseFragmentActivity implements SocketRouter.OnSocketRouterListener, c {
    private static /* synthetic */ int[] $SWITCH_TABLE$cn$banshenggua$aichang$room$EventMessage$EventMessageType = null;
    private static /* synthetic */ int[] $SWITCH_TABLE$cn$banshenggua$aichang$room$message$LiveMessage$Message_Flag = null;
    private static /* synthetic */ int[] $SWITCH_TABLE$cn$banshenggua$aichang$room$message$MessageKey = null;
    protected static final int DIALOG_EXIT_CODE = 101;
    protected static final int DIALOG_MUTED_CODE = 102;
    protected static final int DIALOG_NET_ERROR_CODE = 103;
    public static final int DONWLOAD_START = 3000;
    public static final int DOWNLOAD_COMP = 3002;
    public static final int DOWNLOAD_PROCESS = 3001;
    private static final int ERROR = 2;
    public static final int Event = 4;
    public static final String FINISH_ROOM_ACTION = "com.banshenggua.sdk.finishroom";
    public static final String GET_MIC_NOTIFY_BROADCAST = "get_mic_notify_broadcast";
    private static final int MESSAGE = 1;
    public static final String PRE_KEY_SHOWFREEGIFT = "isShowFreeGift";
    public static final String PRE_KEY_SHOW_VOICE_GIFT = "isShowVoiceGift";
    public static final String PROGRESS = "progress";
    public static final String ROOM = "room";
    public static final String UPDATE_CURRENT_USER = "com.banshenggua.sdk.updateuser";
    public static int mClubUserApply = 0;
    public static j mConfig = null;
    /* access modifiers changed from: private */
    public static LiveController mController = null;
    private static o mHanHuaRoomInfo = null;
    public static SimpleLiveRoomActivity mInstance = null;
    /* access modifiers changed from: private */
    public static LiveObjectController mLiveObjectController = null;
    public static User mMicUser;
    public static o mRoom;
    MyDialogFragment dialog = null;
    private r getGiftListener = new r() {
        public void onResponse(JSONObject jSONObject) {
            super.onResponse(jSONObject);
            f fVar = new f(f.a.GiftList);
            fVar.a(jSONObject);
            if (fVar.i() == -1000) {
                try {
                    if (KShareApplication.getInstance() == null) {
                        return;
                    }
                } catch (ACException e) {
                    e.printStackTrace();
                }
                try {
                    SharedPreferencesUtil.saveObjectToFile(KShareApplication.getInstance().getApp(), fVar, GiftUtils.fileName);
                } catch (ACException e2) {
                    e2.printStackTrace();
                }
                GiftUtils.resetHash();
            }
        }
    };
    /* access modifiers changed from: private */
    public RadioGroup groupHead;
    boolean isCallOnSave = false;
    boolean isRoomPause = false;
    protected boolean isRunningBg = false;
    /* access modifiers changed from: private */
    public List<ArrayListAdapter<LiveMessage>> liveMessageAdapters = new ArrayList();
    private SessionFragmentAdapter mAdapter;
    /* access modifiers changed from: private */
    public Rtmp.RtmpUrls mAudRtmpUrl = null;
    /* access modifiers changed from: private */
    public LiveCenterGiftView mCenterGiftView = null;
    DownloadFragment mDownloadFragment = null;
    private DownloadReceiver mDownloadReceiver = null;
    private FinishRoomReceiver mFinishRoomReceiver = null;
    /* access modifiers changed from: private */
    public LiveGiftView mGiftView = null;
    public List<e> mHanHua = new ArrayList();
    /* access modifiers changed from: private */
    @SuppressLint({"HandlerLeak"})
    public Handler mHandler = new Handler() {
        public void handleMessage(Message message) {
            super.handleMessage(message);
            switch (message.what) {
                case 1:
                    if (message.obj instanceof SocketMessage) {
                        SimpleLiveRoomActivity.this.processSocketMessage((SocketMessage) message.obj);
                        return;
                    }
                    return;
                case 2:
                    if (message.obj instanceof SocketMessage) {
                        SocketMessage socketMessage = (SocketMessage) message.obj;
                        if (socketMessage.mError != null) {
                            SimpleLiveRoomActivity.this.processErrorSocketMessage(socketMessage);
                            return;
                        }
                        return;
                    }
                    return;
                case 4:
                    if (message.obj != null && (message.obj instanceof EventMessage)) {
                        SimpleLiveRoomActivity.this.processEventMessage((EventMessage) message.obj);
                        return;
                    }
                    return;
                case 3000:
                    SimpleLiveRoomActivity.this.createDownloadProcessDialog();
                    return;
                case 3001:
                    SimpleLiveRoomActivity.this.updateDownloadProcess(message.arg1);
                    return;
                case 3002:
                    SimpleLiveRoomActivity.this.closeDownloadProcess();
                    return;
                default:
                    return;
            }
        }
    };
    private FrameLayout mHeadVideoView = null;
    private ListViewOnTouch mListViewOnTouch = new ListViewOnTouch() {
        public boolean OnDragXMove() {
            return false;
        }

        public boolean OnDragUpMove() {
            return false;
        }

        public boolean OnDragDownMove() {
            return false;
        }

        public boolean OnDrag() {
            return false;
        }
    };
    /* access modifiers changed from: private */
    public r mLiveConfigListener = new r() {
        public void onResponse(JSONObject jSONObject) {
            super.onResponse(jSONObject);
            SimpleLiveRoomActivity.mConfig.a(jSONObject);
            if (SimpleLiveRoomActivity.mConfig.ao != -1000) {
                switch (SimpleLiveRoomActivity.mConfig.i()) {
                    case 503:
                    case ContextError.Room_MUTED /*533*/:
                        SimpleLiveRoomActivity.this.showRoomMuted(ContextError.getErrorString(SimpleLiveRoomActivity.mConfig.i()));
                        return;
                    default:
                        KShareUtil.showToastJsonStatus(SimpleLiveRoomActivity.this, SimpleLiveRoomActivity.mConfig);
                        return;
                }
            } else {
                new ConnectRoomTask(SimpleLiveRoomActivity.this, null).execute(new Void[0]);
            }
        }
    };
    private ViewGroup mLiveControlLayout = null;
    private ViewGroup mLiveGiftLayout = null;
    public String mMedia;
    private HashMap<MessageKey, LiveMessageProcess> mMessageProcessMap = new HashMap<MessageKey, LiveMessageProcess>() {
        private static final long serialVersionUID = 1;

        {
            put(MessageKey.Message_AdjustMic, new AdjustMicMessage(SimpleLiveRoomActivity.this, null));
            put(MessageKey.Message_CancelMic, new CancelMicMessage(SimpleLiveRoomActivity.this, null));
            put(MessageKey.Message_ChangeBanzou, new ChangeBanzouMessage(SimpleLiveRoomActivity.this, null));
            put(MessageKey.Message_ChangeMic, new ChangeMicMessage(SimpleLiveRoomActivity.this, null));
            put(MessageKey.Message_CloseMic, new CloseMicMessage(SimpleLiveRoomActivity.this, null));
            put(MessageKey.Message_Family, new FamilyMessage(SimpleLiveRoomActivity.this, null));
            put(MessageKey.Message_FAMILY_DISSOLVE, new FamilyDisMessage(SimpleLiveRoomActivity.this, null));
            put(MessageKey.Message_Gift, new GiftMessageProcess(SimpleLiveRoomActivity.this, null));
            put(MessageKey.Message_Join, new JoinMessage(SimpleLiveRoomActivity.this, null));
            put(MessageKey.Message_KickUser, new KickUserMessage(SimpleLiveRoomActivity.this, null));
            put(MessageKey.Message_Leave, new LeaveMessage(SimpleLiveRoomActivity.this, null));
            put(MessageKey.Message_Media, new MediaMessage(SimpleLiveRoomActivity.this, null));
            put(MessageKey.Message_MuteRoom, new MuteRoomMessage(SimpleLiveRoomActivity.this, null));
            put(MessageKey.Message_OpenMic, new OpenMicMessage(SimpleLiveRoomActivity.this, null));
            put(MessageKey.Message_ReqMic, new ReqMicMessage(SimpleLiveRoomActivity.this, null));
            put(MessageKey.Message_RoomMod, new RoomModMessage(SimpleLiveRoomActivity.this, null));
            put(MessageKey.Message_RoomUpdate, new RoomUpdateMessage(SimpleLiveRoomActivity.this, null));
            put(MessageKey.Message_STalk, new STalkMessage(SimpleLiveRoomActivity.this, null));
            put(MessageKey.Message_Talk, new TalkMessage(SimpleLiveRoomActivity.this, null));
            put(MessageKey.Message_Toast, new ToastMessage(SimpleLiveRoomActivity.this, null));
            put(MessageKey.Message_ServerSys, new ServerSysMessage(SimpleLiveRoomActivity.this, null));
            put(MessageKey.Message_MultiReqMic, new MultiReqMicMessage(SimpleLiveRoomActivity.this, null));
            put(MessageKey.Message_MultiInviteMic, new MultiInviteMicMessage(SimpleLiveRoomActivity.this, null));
            put(MessageKey.Message_MultiChangeMic, new MultiChangeMicMessage(SimpleLiveRoomActivity.this, null));
            put(MessageKey.Message_MultiCloseMic, new MultiColseMicMessage(SimpleLiveRoomActivity.this, null));
            put(MessageKey.Message_MultiConfirmMic, new MultiConfirmMicMessage(SimpleLiveRoomActivity.this, null));
            put(MessageKey.Message_MultiMedia, new MultiMediaMessage(SimpleLiveRoomActivity.this, null));
            put(MessageKey.Message_MultiCancelMic, new MultiCancelMicMessage(SimpleLiveRoomActivity.this, null));
            put(MessageKey.Message_GlobalGift, new GlobalGiftMessageProcess(SimpleLiveRoomActivity.this, null));
        }
    };
    /* access modifiers changed from: private */
    public User mMicRightUser = null;
    /* access modifiers changed from: private */
    public int mMidNum = 0;
    private View.OnClickListener mOnClickListener = new View.OnClickListener() {
        public void onClick(View view) {
            int id = view.getId();
            if (id == a.f.head_rb_0 || id == a.f.btn_room_group_chat) {
                ULog.d(SimpleLiveRoomActivity.this.TAG, "radio 0 status: " + ((RadioButton) SimpleLiveRoomActivity.this.groupHead.getChildAt(0)).isChecked());
                KShareUtil.setNumUpIcon(-1, (Button) SimpleLiveRoomActivity.this.findViewById(a.f.notify_count_left));
                SimpleLiveRoomActivity.this.setGroupChecked(0);
            } else if (id == a.f.head_rb_1 || id == a.f.btn_room_private_chat) {
                ULog.d(SimpleLiveRoomActivity.this.TAG, "radio 1 status: " + ((RadioButton) SimpleLiveRoomActivity.this.groupHead.getChildAt(1)).isChecked());
                SimpleLiveRoomActivity.this.mMidNum = 0;
                KShareUtil.setNumUpIcon(-1, (Button) SimpleLiveRoomActivity.this.findViewById(a.f.notify_count_middle));
                SimpleLiveRoomActivity.this.setGroupChecked(1);
            } else if (id == a.f.head_rb_2 || id == a.f.btn_room_spend_top) {
                ULog.d(SimpleLiveRoomActivity.this.TAG, "radio 2 status: " + ((RadioButton) SimpleLiveRoomActivity.this.groupHead.getChildAt(2)).isChecked());
                SimpleLiveRoomActivity.this.setGroupChecked(2);
            } else if (id == a.f.head_rb_3 || id == a.f.btn_room_gift) {
                ULog.d(SimpleLiveRoomActivity.this.TAG, "radio 3 status: " + ((RadioButton) SimpleLiveRoomActivity.this.groupHead.getChildAt(3)).isChecked());
                SimpleLiveRoomActivity.this.mRightNum = 0;
                KShareUtil.setNumUpIcon(-1, (Button) SimpleLiveRoomActivity.this.findViewById(a.f.notify_count_right));
                SimpleLiveRoomActivity.this.setGroupChecked(3);
            } else {
                if (id == a.f.room_sendmessage_btn) {
                    KShareUtil.mCurrentNotifyKey = k.b.ROOM_CHAT;
                } else if (id == a.f.room_gift_btn) {
                    KShareUtil.mCurrentNotifyKey = k.b.ROOM_GIFT;
                } else if (id == a.f.room_getmic_btn) {
                    KShareUtil.mCurrentNotifyKey = k.b.ROOM_SING;
                } else if (id == a.f.room_viewers_btn) {
                    KShareUtil.mCurrentNotifyKey = k.b.ROOM_MICS;
                } else if (id == a.f.room_share_btn) {
                    KShareUtil.mCurrentNotifyKey = k.b.ROOM_INVITE;
                } else {
                    KShareUtil.mCurrentNotifyKey = k.b.DEFAULT;
                }
                if (KShareUtil.processAnonymous(SimpleLiveRoomActivity.this, null, SimpleLiveRoomActivity.mRoom.f1178a)) {
                }
            }
        }
    };
    private ViewPager.OnPageChangeListener mOnPageChangeListener = new ViewPager.OnPageChangeListener() {
        public void onPageSelected(int i) {
            int i2;
            for (int i3 = 0; i3 < SimpleLiveRoomActivity.this.groupHead.getChildCount(); i3++) {
                switch (i3) {
                    case 0:
                        i2 = a.f.notify_count_left;
                        break;
                    case 1:
                        i2 = a.f.notify_count_middle;
                        SimpleLiveRoomActivity.this.mMidNum = 0;
                        break;
                    case 2:
                        i2 = a.f.notify_count_right;
                        SimpleLiveRoomActivity.this.mRightNum = 0;
                        break;
                    default:
                        i2 = 0;
                        break;
                }
                if (i == i3) {
                    if (i2 > 0) {
                        KShareUtil.setNumUpIcon(-1, (Button) SimpleLiveRoomActivity.this.findViewById(i2));
                    }
                    ((RadioButton) SimpleLiveRoomActivity.this.groupHead.getChildAt(i)).setChecked(true);
                } else {
                    ((RadioButton) SimpleLiveRoomActivity.this.groupHead.getChildAt(i3)).setChecked(false);
                }
            }
        }

        public void onPageScrolled(int i, float f, int i2) {
        }

        public void onPageScrollStateChanged(int i) {
        }
    };
    /* access modifiers changed from: private */
    public ViewPager mPager;
    public PhoneStatReceiver mPhoneStatReceiver;
    private int mProgress = -1;
    private Rtmp mPubRtmp = null;
    private User mRecorderUser = null;
    /* access modifiers changed from: private */
    public Rtmp.RtmpUrls mRightAudRtmpUrl = null;
    /* access modifiers changed from: private */
    public int mRightNum = 0;
    private Rtmp mRightPubRtmp = null;
    private FrameLayout mRightVideoView = null;
    private SocketRouter mRouter = null;
    private com.pocketmusic.kshare.requestobjs.a mUser = null;
    private PowerManager.WakeLock mWakeLock;
    int multiReqNum = 0;
    MyDialogFragment mutedDialog = null;
    MyDialogFragment netErrorDialog = null;
    private int passwordShowNum = 0;
    private OnRoomMessageClickListener roomMessageClickListener = new OnRoomMessageClickListener() {
        public void OnItemIconClick(LiveMessage liveMessage, int i) {
            KShareUtil.mCurrentNotifyKey = k.b.ROOM_HEAD;
            if (KShareUtil.processAnonymous(SimpleLiveRoomActivity.this, null, SimpleLiveRoomActivity.mRoom.f1178a)) {
            }
        }

        public void OnItemClick(LiveMessage liveMessage, int i) {
            KShareUtil.mCurrentNotifyKey = k.b.ROOM_CHAT;
            if (KShareUtil.processAnonymous(SimpleLiveRoomActivity.this, null, SimpleLiveRoomActivity.mRoom.f1178a)) {
            }
        }
    };

    public /* bridge */ /* synthetic */ View onCreateView(View view, String str, Context context, AttributeSet attributeSet) {
        return super.onCreateView(view, str, context, attributeSet);
    }

    public /* bridge */ /* synthetic */ View onCreateView(String str, Context context, AttributeSet attributeSet) {
        return super.onCreateView(str, context, attributeSet);
    }

    static /* synthetic */ int[] $SWITCH_TABLE$cn$banshenggua$aichang$room$EventMessage$EventMessageType() {
        int[] iArr = $SWITCH_TABLE$cn$banshenggua$aichang$room$EventMessage$EventMessageType;
        if (iArr == null) {
            iArr = new int[EventMessage.EventMessageType.values().length];
            try {
                iArr[EventMessage.EventMessageType.ObjectMessage.ordinal()] = 2;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[EventMessage.EventMessageType.ViewMessage.ordinal()] = 1;
            } catch (NoSuchFieldError e2) {
            }
            $SWITCH_TABLE$cn$banshenggua$aichang$room$EventMessage$EventMessageType = iArr;
        }
        return iArr;
    }

    static /* synthetic */ int[] $SWITCH_TABLE$cn$banshenggua$aichang$room$message$LiveMessage$Message_Flag() {
        int[] iArr = $SWITCH_TABLE$cn$banshenggua$aichang$room$message$LiveMessage$Message_Flag;
        if (iArr == null) {
            iArr = new int[LiveMessage.Message_Flag.values().length];
            try {
                iArr[LiveMessage.Message_Flag.Message_ACK.ordinal()] = 2;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[LiveMessage.Message_Flag.Message_Broadcast.ordinal()] = 3;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[LiveMessage.Message_Flag.Message_Normal.ordinal()] = 1;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[LiveMessage.Message_Flag.Message_Server.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[LiveMessage.Message_Flag.Message_Vehicle.ordinal()] = 5;
            } catch (NoSuchFieldError e5) {
            }
            $SWITCH_TABLE$cn$banshenggua$aichang$room$message$LiveMessage$Message_Flag = iArr;
        }
        return iArr;
    }

    static /* synthetic */ int[] $SWITCH_TABLE$cn$banshenggua$aichang$room$message$MessageKey() {
        int[] iArr = $SWITCH_TABLE$cn$banshenggua$aichang$room$message$MessageKey;
        if (iArr == null) {
            iArr = new int[MessageKey.values().length];
            try {
                iArr[MessageKey.MessageCenter_Private.ordinal()] = 32;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[MessageKey.Message_ACK.ordinal()] = 33;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[MessageKey.Message_ALL.ordinal()] = 2;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[MessageKey.Message_AdjustMic.ordinal()] = 24;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[MessageKey.Message_CancelMic.ordinal()] = 14;
            } catch (NoSuchFieldError e5) {
            }
            try {
                iArr[MessageKey.Message_ChangeBanzou.ordinal()] = 25;
            } catch (NoSuchFieldError e6) {
            }
            try {
                iArr[MessageKey.Message_ChangeMic.ordinal()] = 19;
            } catch (NoSuchFieldError e7) {
            }
            try {
                iArr[MessageKey.Message_CloseMic.ordinal()] = 18;
            } catch (NoSuchFieldError e8) {
            }
            try {
                iArr[MessageKey.Message_FAMILY_DISSOLVE.ordinal()] = 28;
            } catch (NoSuchFieldError e9) {
            }
            try {
                iArr[MessageKey.Message_Family.ordinal()] = 27;
            } catch (NoSuchFieldError e10) {
            }
            try {
                iArr[MessageKey.Message_GetMics.ordinal()] = 11;
            } catch (NoSuchFieldError e11) {
            }
            try {
                iArr[MessageKey.Message_GetUsers.ordinal()] = 10;
            } catch (NoSuchFieldError e12) {
            }
            try {
                iArr[MessageKey.Message_Gift.ordinal()] = 9;
            } catch (NoSuchFieldError e13) {
            }
            try {
                iArr[MessageKey.Message_GlobalGift.ordinal()] = 44;
            } catch (NoSuchFieldError e14) {
            }
            try {
                iArr[MessageKey.Message_HeartBeat.ordinal()] = 12;
            } catch (NoSuchFieldError e15) {
            }
            try {
                iArr[MessageKey.Message_Join.ordinal()] = 5;
            } catch (NoSuchFieldError e16) {
            }
            try {
                iArr[MessageKey.Message_KickUser.ordinal()] = 15;
            } catch (NoSuchFieldError e17) {
            }
            try {
                iArr[MessageKey.Message_Leave.ordinal()] = 6;
            } catch (NoSuchFieldError e18) {
            }
            try {
                iArr[MessageKey.Message_Login.ordinal()] = 3;
            } catch (NoSuchFieldError e19) {
            }
            try {
                iArr[MessageKey.Message_Logout.ordinal()] = 4;
            } catch (NoSuchFieldError e20) {
            }
            try {
                iArr[MessageKey.Message_Media.ordinal()] = 20;
            } catch (NoSuchFieldError e21) {
            }
            try {
                iArr[MessageKey.Message_MultiCancelMic.ordinal()] = 43;
            } catch (NoSuchFieldError e22) {
            }
            try {
                iArr[MessageKey.Message_MultiChangeMic.ordinal()] = 40;
            } catch (NoSuchFieldError e23) {
            }
            try {
                iArr[MessageKey.Message_MultiCloseMic.ordinal()] = 37;
            } catch (NoSuchFieldError e24) {
            }
            try {
                iArr[MessageKey.Message_MultiConfirmMic.ordinal()] = 38;
            } catch (NoSuchFieldError e25) {
            }
            try {
                iArr[MessageKey.Message_MultiDelMic.ordinal()] = 35;
            } catch (NoSuchFieldError e26) {
            }
            try {
                iArr[MessageKey.Message_MultiInviteMic.ordinal()] = 36;
            } catch (NoSuchFieldError e27) {
            }
            try {
                iArr[MessageKey.Message_MultiMedia.ordinal()] = 39;
            } catch (NoSuchFieldError e28) {
            }
            try {
                iArr[MessageKey.Message_MultiOpenMic.ordinal()] = 41;
            } catch (NoSuchFieldError e29) {
            }
            try {
                iArr[MessageKey.Message_MultiReqMic.ordinal()] = 34;
            } catch (NoSuchFieldError e30) {
            }
            try {
                iArr[MessageKey.Message_MuteRoom.ordinal()] = 21;
            } catch (NoSuchFieldError e31) {
            }
            try {
                iArr[MessageKey.Message_MuteUser.ordinal()] = 16;
            } catch (NoSuchFieldError e32) {
            }
            try {
                iArr[MessageKey.Message_Null.ordinal()] = 1;
            } catch (NoSuchFieldError e33) {
            }
            try {
                iArr[MessageKey.Message_OpenMic.ordinal()] = 17;
            } catch (NoSuchFieldError e34) {
            }
            try {
                iArr[MessageKey.Message_ReqMic.ordinal()] = 13;
            } catch (NoSuchFieldError e35) {
            }
            try {
                iArr[MessageKey.Message_RoomMod.ordinal()] = 29;
            } catch (NoSuchFieldError e36) {
            }
            try {
                iArr[MessageKey.Message_RoomParam.ordinal()] = 31;
            } catch (NoSuchFieldError e37) {
            }
            try {
                iArr[MessageKey.Message_RoomUpdate.ordinal()] = 26;
            } catch (NoSuchFieldError e38) {
            }
            try {
                iArr[MessageKey.Message_RtmpProp.ordinal()] = 42;
            } catch (NoSuchFieldError e39) {
            }
            try {
                iArr[MessageKey.Message_STalk.ordinal()] = 8;
            } catch (NoSuchFieldError e40) {
            }
            try {
                iArr[MessageKey.Message_ServerError.ordinal()] = 22;
            } catch (NoSuchFieldError e41) {
            }
            try {
                iArr[MessageKey.Message_ServerSys.ordinal()] = 23;
            } catch (NoSuchFieldError e42) {
            }
            try {
                iArr[MessageKey.Message_Talk.ordinal()] = 7;
            } catch (NoSuchFieldError e43) {
            }
            try {
                iArr[MessageKey.Message_Toast.ordinal()] = 30;
            } catch (NoSuchFieldError e44) {
            }
            $SWITCH_TABLE$cn$banshenggua$aichang$room$message$MessageKey = iArr;
        }
        return iArr;
    }

    public static void launch(Context context, o oVar, int i) {
        context.sendBroadcast(new Intent(FINISH_ROOM_ACTION));
        Intent intent = new Intent(context, SimpleLiveRoomActivity.class);
        if (oVar != null) {
            intent.putExtra("room", oVar);
        }
        intent.putExtra("progress", i);
        context.startActivity(intent);
    }

    public static boolean isVideoOpen() {
        if (mController != null) {
            return mController.isVideoOpen();
        }
        return false;
    }

    public static boolean canDoConnectMic() {
        if (mRoom != null && mMicUser != null && mRoom.R == o.a.Show && mMicUser.mUid.equalsIgnoreCase(Session.getCurrentAccount().b)) {
            return true;
        }
        return false;
    }

    public static boolean isAdminUser(String str, boolean z) {
        boolean isAdminUser = isAdminUser(str);
        if (isAdminUser || z) {
            return isAdminUser;
        }
        if (mRoom != null) {
            return mRoom.a(str);
        }
        return false;
    }

    public static boolean isVipUser(String str) {
        if (mRoom != null) {
            return mRoom.b(str);
        }
        return false;
    }

    public static boolean isViceUser(String str) {
        if (mRoom != null) {
            return mRoom.a(str);
        }
        return false;
    }

    public static boolean isAdminUser(String str) {
        if (mRoom == null || str == null || mRoom.m == null || TextUtils.isEmpty(mRoom.m.mUid) || !mRoom.m.mUid.equalsIgnoreCase(str)) {
            return false;
        }
        return true;
    }

    public void onCreate(Bundle bundle) {
        try {
            if (!KShareApplication.getInstance().isInitData) {
                KShareApplication.getInstance().initData(false);
            }
        } catch (ACException e) {
            e.printStackTrace();
        }
        super.onCreate(bundle);
        mInstance = this;
        setContentView(a.g.activity_room_sample);
        this.mProgress = getIntent().getIntExtra("progress", -1);
        initRoom();
        initUI();
        initData();
        registerDownloadReceiver(this);
        registerPhoneStatReceiver();
        registerFinishRoomReceiver();
        DownloadService.mFrom = DownloadService.F_ZHIBO;
        if (this.mProgress >= 0) {
            Message message = new Message();
            message.what = 3001;
            message.arg1 = this.mProgress;
            this.mHandler.sendMessage(message);
        }
    }

    private void registerDownloadReceiver(Context context) {
        if (this.mDownloadReceiver != null) {
            unregisterDownloadReceiver(context);
        }
        this.mDownloadReceiver = new DownloadReceiver(this, null);
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(DownloadService.ACTION_START_DOWNLOAD);
        intentFilter.addAction(DownloadService.ACTION_DWNLOAD_PROGRESS);
        intentFilter.addAction(DownloadService.ACTOIN_DWNLOAD_COMP);
        context.registerReceiver(this.mDownloadReceiver, intentFilter);
    }

    public void unregisterDownloadReceiver(Context context) {
        if (this.mDownloadReceiver != null) {
            context.unregisterReceiver(this.mDownloadReceiver);
            this.mDownloadReceiver = null;
        }
    }

    private class DownloadReceiver extends BroadcastReceiver {
        private DownloadReceiver() {
        }

        /* synthetic */ DownloadReceiver(SimpleLiveRoomActivity simpleLiveRoomActivity, DownloadReceiver downloadReceiver) {
            this();
        }

        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (!TextUtils.isEmpty(action)) {
                if (action.equals(DownloadService.ACTION_START_DOWNLOAD)) {
                    Message message = new Message();
                    message.what = 3000;
                    SimpleLiveRoomActivity.this.mHandler.sendMessage(message);
                } else if (action.equals(DownloadService.ACTION_DWNLOAD_PROGRESS)) {
                    int intExtra = intent.getIntExtra(DownloadService.ACTION_DWNLOAD_PROGRESS, 0);
                    ULog.d("ACDownload", "receive download process: " + intExtra);
                    Message message2 = new Message();
                    message2.what = 3001;
                    message2.arg1 = intExtra;
                    SimpleLiveRoomActivity.this.mHandler.sendMessage(message2);
                } else if (action.equals(DownloadService.ACTOIN_DWNLOAD_COMP)) {
                    Message message3 = new Message();
                    message3.what = 3002;
                    SimpleLiveRoomActivity.this.mHandler.sendMessage(message3);
                }
            }
        }
    }

    private void registerFinishRoomReceiver() {
        if (this.mFinishRoomReceiver == null) {
            this.mFinishRoomReceiver = new FinishRoomReceiver(this, null);
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(FINISH_ROOM_ACTION);
            try {
                registerReceiver(this.mFinishRoomReceiver, intentFilter);
            } catch (Exception e) {
            }
        }
    }

    private void unregisterFinishRoomReceiver() {
        try {
            if (this.mFinishRoomReceiver != null) {
                unregisterReceiver(this.mFinishRoomReceiver);
            }
            this.mFinishRoomReceiver = null;
        } catch (Exception e) {
        }
    }

    private class FinishRoomReceiver extends BroadcastReceiver {
        private FinishRoomReceiver() {
        }

        /* synthetic */ FinishRoomReceiver(SimpleLiveRoomActivity simpleLiveRoomActivity, FinishRoomReceiver finishRoomReceiver) {
            this();
        }

        public void onReceive(Context context, Intent intent) {
            if (SimpleLiveRoomActivity.this.isRunningBg) {
                SimpleLiveRoomActivity.this.finish();
            }
        }
    }

    public void registerPhoneStatReceiver() {
        this.mPhoneStatReceiver = new PhoneStatReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.intent.action.PHONE_STATE");
        intentFilter.setPriority(Integer.MAX_VALUE);
        registerReceiver(this.mPhoneStatReceiver, intentFilter);
    }

    public void unregisterPhoneStatReceiver() {
        if (this.mPhoneStatReceiver != null) {
            unregisterReceiver(this.mPhoneStatReceiver);
            this.mPhoneStatReceiver = null;
        }
    }

    public class PhoneStatReceiver extends BroadcastReceiver {
        public PhoneStatReceiver() {
        }

        public void onReceive(Context context, Intent intent) {
            TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
            ULog.d(SimpleLiveRoomActivity.this.TAG, "tm.getCallState() = " + telephonyManager.getCallState());
            switch (telephonyManager.getCallState()) {
                case 1:
                    if (SimpleLiveRoomActivity.this != null) {
                        SimpleLiveRoomActivity.this.finish();
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    }

    public void onDestroy() {
        ULog.d(this.TAG, "onDestroy");
        super.onDestroy();
        downMic();
        mLiveObjectController.removeAll();
        if (this.mRouter != null) {
            this.mRouter.disconnect();
            this.mRouter = null;
        }
        unregisterDownloadReceiver(this);
        unregisterPhoneStatReceiver();
        unregisterFinishRoomReceiver();
        try {
            KShareApplication.getInstance().onDestroy();
        } catch (ACException e) {
            e.printStackTrace();
        }
        CommonUtil.killProcess(this);
    }

    public void OnMessageReceived(SocketMessage socketMessage) {
        if (socketMessage != null) {
            socketMessage.mBeginTime = System.currentTimeMillis();
            if (socketMessage.mError == null || socketMessage.mError.getError() == 0) {
                Message.obtain(this.mHandler, 1, socketMessage).sendToTarget();
            } else {
                Message.obtain(this.mHandler, 2, socketMessage).sendToTarget();
            }
        }
    }

    public void OnSocketError(SocketMessage socketMessage) {
        if (socketMessage != null) {
            Message.obtain(this.mHandler, 2, socketMessage).sendToTarget();
        }
    }

    public void SendMessageToHandle(int i, Object obj) {
        ULog.d(SocketRouter.TAG, "sendmessage to handle what: " + i + "; obj: " + obj);
        if (obj != null && i == 4) {
            Message.obtain(this.mHandler, i, obj).sendToTarget();
        }
    }

    private void setRoomPause() {
        if (mLiveObjectController == null) {
        }
    }

    private void setRoomStart() {
        if (mLiveObjectController != null && this.isRoomPause && mConfig != null) {
            mConfig.a();
        }
    }

    public void onPause() {
        super.onPause();
        if (this.mWakeLock != null) {
            this.mWakeLock.release();
            this.mWakeLock = null;
        }
        setRoomPause();
        if (this.mDownloadFragment != null && this.mDownloadFragment.isVisible()) {
            KShareUtil.pop(this.mDownloadFragment);
        }
    }

    public void onResume() {
        super.onResume();
        if (this.mWakeLock == null) {
            this.mWakeLock = ((PowerManager) getSystemService("power")).newWakeLock(10, this.TAG);
            this.mWakeLock.acquire();
        }
        if (Session.getCurrentAccount().d()) {
            Session.getSharedSession().updateAccount();
        }
        com.pocketmusic.kshare.requestobjs.a currentAccount = Session.getCurrentAccount();
        if (this.mUser == null) {
            this.mUser = currentAccount;
        }
        this.mUser = currentAccount;
        ULog.d(this.TAG, "onResume");
        this.isRunningBg = true;
        this.isCallOnSave = false;
    }

    public void finish() {
        if (mController != null) {
            showPlayerInfo(null, false);
        }
        closeMediaPlayer(-1);
        super.finish();
    }

    public void onPositiveButtonClicked(int i) {
        switch (i) {
            case 101:
            case 102:
                finish();
                return;
            case 103:
                if (mConfig != null) {
                    mConfig.a();
                }
                this.netErrorDialog = null;
                return;
            default:
                return;
        }
    }

    public void onNegativeButtonClicked(int i) {
        switch (i) {
            case 102:
            case 103:
                finish();
                return;
            default:
                return;
        }
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        ULog.d(this.TAG, "onKeyDown: " + i + "; home: " + 3);
        if (i != 4) {
            return super.onKeyDown(i, keyEvent);
        }
        backAffirm();
        return true;
    }

    public void sendSocketMessage(LiveMessage liveMessage) {
        if (this.mRouter != null && this.mRouter.isActive()) {
            this.mRouter.sendMessage(liveMessage.getSocketMessage(), false);
        }
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        this.isCallOnSave = true;
    }

    /* access modifiers changed from: private */
    public void createDownloadProcessDialog() {
        if (this.mDownloadFragment == null || !this.mDownloadFragment.isVisible()) {
            this.mDownloadFragment = new DownloadFragment();
            KShareUtil.pushFromBottom(this, this.mDownloadFragment, a.f.container);
        }
    }

    /* access modifiers changed from: private */
    public void updateDownloadProcess(int i) {
        if (this.mDownloadFragment != null) {
            this.mDownloadFragment.updateProgress(i);
            return;
        }
        createDownloadProcessDialog();
        updateDownloadProcess(i);
    }

    /* access modifiers changed from: private */
    public void closeDownloadProcess() {
        if (this.mDownloadFragment != null) {
            KShareUtil.pop(this.mDownloadFragment);
            this.mDownloadFragment = null;
        }
    }

    private void backAffirm() {
        ((MyDialogFragment.a) MyDialogFragment.a(this, getSupportFragmentManager()).a(k.a(this, k.b.DEFAULT).f1174a).b(a.h.room_leave_dialog_tip).e(a.h.cancel).f(101)).d(a.h.ok).d();
    }

    private void initMessageAdapters() {
        for (int i = 0; i < this.mAdapter.getCount(); i++) {
            RoomMessgeAdapter roomMessgeAdapter = new RoomMessgeAdapter(this, i, mRoom);
            if (i == 0 || i == 1) {
                roomMessgeAdapter.setLimit(100, false);
                roomMessgeAdapter.setUseBufferForScorll(true);
            } else {
                roomMessgeAdapter.setLimit(500, false);
            }
            roomMessgeAdapter.setListener(this.roomMessageClickListener);
            this.liveMessageAdapters.add(roomMessgeAdapter);
        }
    }

    private void initUI() {
        this.mLiveControlLayout = (ViewGroup) findViewById(a.f.room_live_control_layout);
        this.mLiveGiftLayout = (ViewGroup) findViewById(a.f.room_live_gift_layout);
        this.mHeadVideoView = (FrameLayout) findViewById(a.f.room_video_show_layout);
        this.mRightVideoView = (FrameLayout) findViewById(a.f.room_video_show_layout_right);
        this.mRightVideoView.setVisibility(8);
        this.mGiftView = new LiveGiftView(this, this.mLiveGiftLayout);
        this.mCenterGiftView = new LiveCenterGiftView(this);
        mLiveObjectController = new LiveObjectController(this, this.mHeadVideoView, this.mRightVideoView);
        mController = new LiveController(this, this.mLiveControlLayout);
        mController.setControllerType(LiveController.LiveControllerType.None);
        this.mPager = (ViewPager) findViewById(a.f.pager);
        this.groupHead = (RadioGroup) findViewById(a.f.head_rb_group);
        View findViewById = findViewById(a.f.room_video_layout);
        ViewGroup.LayoutParams layoutParams = findViewById.getLayoutParams();
        int i = (UIUtil.getInstance().getmScreenWidth() * 3) / 4;
        layoutParams.height = i;
        findViewById.setLayoutParams(layoutParams);
        View findViewById2 = findViewById(a.f.room_live_control_layout);
        ViewGroup.LayoutParams layoutParams2 = findViewById2.getLayoutParams();
        layoutParams2.height = i;
        findViewById2.setLayoutParams(layoutParams2);
        View findViewById3 = findViewById(a.f.room_live_gift_layout);
        ViewGroup.LayoutParams layoutParams3 = findViewById3.getLayoutParams();
        layoutParams3.height = i;
        findViewById3.setLayoutParams(layoutParams3);
        findViewById(a.f.btn_room_group_chat).setOnClickListener(this.mOnClickListener);
        findViewById(a.f.btn_room_private_chat).setOnClickListener(this.mOnClickListener);
        findViewById(a.f.btn_room_gift).setOnClickListener(this.mOnClickListener);
        findViewById(a.f.btn_room_spend_top).setOnClickListener(this.mOnClickListener);
        findViewById(a.f.head_rb_0).setOnClickListener(this.mOnClickListener);
        findViewById(a.f.head_rb_1).setOnClickListener(this.mOnClickListener);
        findViewById(a.f.head_rb_2).setOnClickListener(this.mOnClickListener);
        findViewById(a.f.head_rb_3).setOnClickListener(this.mOnClickListener);
        findViewById(a.f.room_sendmessage_btn).setOnClickListener(this.mOnClickListener);
        findViewById(a.f.room_gift_btn).setOnClickListener(this.mOnClickListener);
        findViewById(a.f.room_getmic_btn).setOnClickListener(this.mOnClickListener);
        findViewById(a.f.room_viewers_btn).setOnClickListener(this.mOnClickListener);
        findViewById(a.f.room_share_btn).setOnClickListener(this.mOnClickListener);
        setMultiReqNum(0);
    }

    /* access modifiers changed from: private */
    public void setGroupChecked(int i) {
        switch (i) {
            case 0:
                ((RadioButton) this.groupHead.getChildAt(0)).setChecked(true);
                ((RadioButton) this.groupHead.getChildAt(1)).setChecked(false);
                ((RadioButton) this.groupHead.getChildAt(2)).setChecked(false);
                ((RadioButton) this.groupHead.getChildAt(3)).setChecked(false);
                break;
            case 1:
                ((RadioButton) this.groupHead.getChildAt(1)).setChecked(true);
                ((RadioButton) this.groupHead.getChildAt(0)).setChecked(false);
                ((RadioButton) this.groupHead.getChildAt(2)).setChecked(false);
                ((RadioButton) this.groupHead.getChildAt(3)).setChecked(false);
                break;
            case 2:
                ((RadioButton) this.groupHead.getChildAt(2)).setChecked(true);
                ((RadioButton) this.groupHead.getChildAt(1)).setChecked(false);
                ((RadioButton) this.groupHead.getChildAt(0)).setChecked(false);
                ((RadioButton) this.groupHead.getChildAt(3)).setChecked(false);
                break;
            case 3:
                ((RadioButton) this.groupHead.getChildAt(3)).setChecked(true);
                ((RadioButton) this.groupHead.getChildAt(1)).setChecked(false);
                ((RadioButton) this.groupHead.getChildAt(2)).setChecked(false);
                ((RadioButton) this.groupHead.getChildAt(0)).setChecked(false);
                break;
        }
        if (i > -1 && i < 4) {
            this.mPager.setCurrentItem(i);
        }
    }

    private void initRoom() {
        this.mMedia = getIntent().getStringExtra("mMedia");
        mRoom = (o) getIntent().getSerializableExtra("room");
        if (mRoom == null) {
            mRoom = new o();
            mRoom.f1178a = "2";
        }
        try {
            if (KShareApplication.getInstance().hasLiveLib()) {
                mConfig = new j(mRoom.f1178a);
                mRoom.a(new RoomInfoListener(this, null));
                mRoom.a();
                return;
            }
            KShareApplication.getInstance().checkLiveLib(new KShareApplication.DownloadListener() {
                MyDialogFragment dialog = null;

                public void onStart() {
                    if (this.dialog != null) {
                        this.dialog = null;
                    }
                    this.dialog = (MyDialogFragment) MyDialogFragment.a(SimpleLiveRoomActivity.this, SimpleLiveRoomActivity.this.getSupportFragmentManager()).a(a.h.title_for_install_lib).d(a.h.hide_dialog).c(a.g.progressview).d();
                }

                public void onLoading(int i) {
                    if (this.dialog != null && this.dialog.getDialog() != null) {
                        ((ProgressBar) this.dialog.getDialog().findViewById(a.f.progressbar_updown)).setProgress(i);
                    }
                }

                public void onError(String str) {
                    Toaster.showLongToast("直播模块下载失败");
                }

                public void onDownloaded(String str) {
                    if (!SimpleLiveRoomActivity.this.isFinishing()) {
                        SimpleLiveRoomActivity.mConfig = new j(SimpleLiveRoomActivity.mRoom.f1178a);
                        SimpleLiveRoomActivity.mRoom.a(new RoomInfoListener(SimpleLiveRoomActivity.this, null));
                        SimpleLiveRoomActivity.mRoom.a();
                        if (!SimpleLiveRoomActivity.this.isCallOnSave && this.dialog != null) {
                            this.dialog.dismiss();
                            this.dialog = null;
                        }
                    }
                }
            });
        } catch (ACException e) {
            e.printStackTrace();
        }
    }

    private void initData() {
        this.mAdapter = new SessionFragmentAdapter(getSupportFragmentManager(), mRoom);
        initGiftList();
        initMessageAdapters();
        this.mAdapter.setCenterGiftView(this.mCenterGiftView);
        this.mAdapter.setOnTouch(this.mListViewOnTouch);
        this.mAdapter.setAdapters(this.liveMessageAdapters);
        this.mPager.setAdapter(this.mAdapter);
        this.mPager.setOnPageChangeListener(this.mOnPageChangeListener);
    }

    public void initGiftList() {
        f fVar = new f(f.a.GiftList, mRoom.f1178a);
        fVar.a(this.getGiftListener);
        fVar.a();
    }

    private void initGiftSound(f fVar) {
    }

    /* access modifiers changed from: private */
    public void addMessageToAdapter(int i, LiveMessage liveMessage, boolean z) {
        if (this.liveMessageAdapters != null && i >= 0 && i < this.liveMessageAdapters.size() && this.liveMessageAdapters.get(i) != null && liveMessage != null) {
            this.liveMessageAdapters.get(i).addItem(liveMessage, z);
        }
    }

    /* access modifiers changed from: private */
    public void downMic() {
        downMic(false, null, null);
    }

    private void downMic(String str) {
        downMic(false, null, str);
    }

    /* access modifiers changed from: private */
    public void setMultiReqNum(int i) {
        this.multiReqNum = i;
        Button button = (Button) findViewById(a.f.req_mic_otify_count);
        if (button != null) {
            KShareUtil.setNumUpIcon(this.multiReqNum, button);
        }
    }

    private void downMic(boolean z, MicMessage.CannelMicAction cannelMicAction) {
        downMic(z, cannelMicAction, null);
    }

    private void downMic(boolean z, MicMessage.CannelMicAction cannelMicAction, String str) {
        downMic(z, cannelMicAction, str, false);
    }

    private void downMic(boolean z, MicMessage.CannelMicAction cannelMicAction, String str, boolean z2) {
        setMultiReqNum(0);
        LiveObject liveObject = mLiveObjectController.getLiveObject(LiveObjectController.LiveObjectIndex.Primary);
        if (liveObject == null || !liveObject.isRunning()) {
            mController.setControllerType(LiveController.LiveControllerType.None);
            showPlayerInfo(null, false);
        } else if (liveObject.isVideo()) {
            mController.setControllerType(LiveController.LiveControllerType.VideoPlay);
        } else {
            mController.setControllerType(LiveController.LiveControllerType.AudioPlay);
        }
        mController.showRoomInfo(mRoom);
    }

    /* access modifiers changed from: private */
    public void backToCurrentActivity() {
    }

    private void showRoomPassword() {
        this.passwordShowNum++;
        if (this.passwordShowNum > 1) {
            Toaster.showLongAtCenter(this, "密码错误");
        }
        final MyDialogFragment myDialogFragment = (MyDialogFragment) MyDialogFragment.a(this, getSupportFragmentManager()).a("请输入房间密码").c(a.g.inputdialog).e(a.h.cancel).d(a.h.ok).d();
        myDialogFragment.a(new c() {
            public void onPositiveButtonClicked(int i) {
                SimpleLiveRoomActivity.this.joinRoom(((EditText) myDialogFragment.getDialog().findViewById(a.f.dialog_input)).getText().toString(), false);
            }

            public void onNegativeButtonClicked(int i) {
                SimpleLiveRoomActivity.this.finish();
            }
        });
        myDialogFragment.a(new b() {
            public void onCancelled(int i) {
                SimpleLiveRoomActivity.this.finish();
            }
        });
    }

    private void showNetError(String str) {
        if (this.netErrorDialog == null && this.mutedDialog == null) {
            downMic(true, null, null);
            if (mController != null) {
                showPlayerInfo(null, false);
            }
            closeMediaPlayer(-1);
            this.netErrorDialog = (MyDialogFragment) ((MyDialogFragment.a) ((MyDialogFragment.a) MyDialogFragment.a(this, getSupportFragmentManager()).a("错误").a((CharSequence) "网络异常，是否重新连接! ").e(a.h.cancel).a(false)).f(103)).b("重连").d();
            if (this.mutedDialog == null || this.mutedDialog.getDialog() == null || !this.mutedDialog.getDialog().isShowing()) {
                backToCurrentActivity();
            }
        }
    }

    /* access modifiers changed from: private */
    public void showRoomMuted(String str) {
        if (this.mutedDialog == null) {
            if (mController != null) {
                showPlayerInfo(null, false);
            }
            closeMediaPlayer(-1);
            backToCurrentActivity();
            this.mutedDialog = (MyDialogFragment) ((MyDialogFragment.a) ((MyDialogFragment.a) MyDialogFragment.a(this, getSupportFragmentManager()).a("通知").a((CharSequence) str).a(false)).f(102)).d(a.h.ok).d();
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0067 A[FALL_THROUGH] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void processErrorSocketMessage(cn.banshenggua.aichang.room.message.SocketMessage r5) {
        /*
            r4 = this;
            r0 = 0
            if (r5 == 0) goto L_0x0021
            cn.banshenggua.aichang.room.message.SocketMessage$MessageError r1 = r5.mError
            if (r1 == 0) goto L_0x0021
            java.lang.String r1 = r4.TAG
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r3 = "message Error: "
            r2.<init>(r3)
            cn.banshenggua.aichang.room.message.SocketMessage$MessageError r3 = r5.mError
            java.lang.String r3 = r3.getErrorString()
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            cn.banshenggua.aichang.utils.ULog.d(r1, r2)
        L_0x0021:
            r1 = 1
            if (r5 == 0) goto L_0x002e
            cn.banshenggua.aichang.room.message.SocketMessage$MessageResult r2 = r5.mResult
            if (r2 == 0) goto L_0x002e
            cn.banshenggua.aichang.room.message.SocketMessage$MessageResult r2 = r5.mResult
            cn.banshenggua.aichang.room.message.LiveMessage r2 = r2.mParseResult
            if (r2 != 0) goto L_0x0054
        L_0x002e:
            if (r5 == 0) goto L_0x003d
            cn.banshenggua.aichang.room.message.SocketMessage$MessageError r2 = r5.mError
            if (r2 == 0) goto L_0x003d
            cn.banshenggua.aichang.room.message.SocketMessage$MessageError r2 = r5.mError
            int r2 = r2.getError()
            switch(r2) {
                case 100404: goto L_0x0049;
                case 100405: goto L_0x0049;
                default: goto L_0x003d;
            }
        L_0x003d:
            if (r1 == 0) goto L_0x0048
            cn.banshenggua.aichang.room.message.SocketMessage$MessageError r0 = r5.mError
            java.lang.String r0 = r0.getErrorString()
            cn.banshenggua.aichang.utils.Toaster.showLongAtCenter(r4, r0)
        L_0x0048:
            return
        L_0x0049:
            cn.banshenggua.aichang.room.message.SocketMessage$MessageError r1 = r5.mError
            java.lang.String r1 = r1.getErrorString()
            r4.showNetError(r1)
            r1 = r0
            goto L_0x003d
        L_0x0054:
            int[] r2 = $SWITCH_TABLE$cn$banshenggua$aichang$room$message$MessageKey()
            cn.banshenggua.aichang.room.message.SocketMessage$MessageResult r3 = r5.mResult
            cn.banshenggua.aichang.room.message.LiveMessage r3 = r3.mParseResult
            cn.banshenggua.aichang.room.message.MessageKey r3 = r3.mKey
            int r3 = r3.ordinal()
            r2 = r2[r3]
            switch(r2) {
                case 5: goto L_0x0090;
                case 13: goto L_0x0074;
                case 22: goto L_0x00a8;
                default: goto L_0x0067;
            }
        L_0x0067:
            r0 = r1
        L_0x0068:
            if (r0 == 0) goto L_0x0048
            cn.banshenggua.aichang.room.message.SocketMessage$MessageError r0 = r5.mError
            java.lang.String r0 = r0.getErrorString()
            cn.banshenggua.aichang.utils.Toaster.showLongAtCenter(r4, r0)
            goto L_0x0048
        L_0x0074:
            cn.banshenggua.aichang.room.message.SocketMessage$MessageError r2 = r5.mError
            int r2 = r2.getError()
            switch(r2) {
                case 529: goto L_0x007e;
                default: goto L_0x007d;
            }
        L_0x007d:
            goto L_0x0067
        L_0x007e:
            com.pocketmusic.kshare.requestobjs.o r2 = cn.banshenggua.aichang.room.SimpleLiveRoomActivity.mRoom
            if (r2 == 0) goto L_0x0067
            com.pocketmusic.kshare.requestobjs.o r2 = cn.banshenggua.aichang.room.SimpleLiveRoomActivity.mRoom
            com.pocketmusic.kshare.requestobjs.o$a r2 = r2.R
            com.pocketmusic.kshare.requestobjs.o$a r3 = com.pocketmusic.kshare.requestobjs.o.a.Show
            if (r2 != r3) goto L_0x0067
            java.lang.String r1 = "不能重复排麦"
            cn.banshenggua.aichang.utils.Toaster.showLongAtCenter(r4, r1)
            goto L_0x0068
        L_0x0090:
            cn.banshenggua.aichang.room.message.SocketMessage$MessageError r2 = r5.mError
            int r2 = r2.getError()
            switch(r2) {
                case 503: goto L_0x009a;
                case 509: goto L_0x00a4;
                case 533: goto L_0x009a;
                case 534: goto L_0x009a;
                case 535: goto L_0x009a;
                default: goto L_0x0099;
            }
        L_0x0099:
            goto L_0x0067
        L_0x009a:
            cn.banshenggua.aichang.room.message.SocketMessage$MessageError r1 = r5.mError
            java.lang.String r1 = r1.getErrorString()
            r4.showRoomMuted(r1)
            goto L_0x0068
        L_0x00a4:
            r4.showRoomPassword()
            goto L_0x0068
        L_0x00a8:
            cn.banshenggua.aichang.room.message.SocketMessage$MessageError r2 = r5.mError
            int r2 = r2.getError()
            switch(r2) {
                case 534: goto L_0x00b2;
                case 535: goto L_0x00b2;
                default: goto L_0x00b1;
            }
        L_0x00b1:
            goto L_0x0067
        L_0x00b2:
            cn.banshenggua.aichang.room.message.SocketMessage$MessageError r1 = r5.mError
            java.lang.String r1 = r1.getErrorString()
            r4.showRoomMuted(r1)
            goto L_0x0068
        */
        throw new UnsupportedOperationException("Method not decompiled: cn.banshenggua.aichang.room.SimpleLiveRoomActivity.processErrorSocketMessage(cn.banshenggua.aichang.room.message.SocketMessage):void");
    }

    /* access modifiers changed from: private */
    public void processSocketMessage(SocketMessage socketMessage) {
        if (socketMessage == null || socketMessage.mResult == null || socketMessage.mResult.mParseResult == null || socketMessage.mResult.mParseResult.mKey == null) {
            ULog.d(this.TAG, "message type error");
        } else if (socketMessage.mError != null && socketMessage.mError.getError() != 0) {
            processErrorSocketMessage(socketMessage);
        } else if (!checkMessage(socketMessage.mResult.mParseResult)) {
            socketMessage.mResult.mParseResult.mBegin = socketMessage.mBeginTime;
            choseMessageProcess(socketMessage.mResult.mParseResult);
        }
    }

    private void processEventViewMessage(EventMessage eventMessage) {
        if (eventMessage.mViewId == a.f.room_video_button) {
            if (eventMessage.mObject != null && (eventMessage.mObject instanceof Boolean)) {
                mLiveObjectController.CloseOrOpenVideo(((Boolean) eventMessage.mObject).booleanValue());
            }
        } else if (eventMessage.mViewId == a.f.head_back) {
            backAffirm();
        } else if (eventMessage.mViewId == a.f.room_more_btn) {
            if (KShareUtil.processAnonymous(this, null, mRoom.f1178a)) {
            }
        } else if (eventMessage.mViewId == a.f.req_connect_mic_btn) {
            KShareUtil.mCurrentNotifyKey = k.b.DEFAULT;
            if (KShareUtil.processAnonymous(this, null, mRoom.f1178a)) {
            }
        }
    }

    /* access modifiers changed from: private */
    public void processEventMessage(EventMessage eventMessage) {
        if (eventMessage != null && eventMessage.mType != null) {
            switch ($SWITCH_TABLE$cn$banshenggua$aichang$room$EventMessage$EventMessageType()[eventMessage.mType.ordinal()]) {
                case 1:
                    processEventViewMessage(eventMessage);
                    return;
                default:
                    return;
            }
        }
    }

    private boolean checkMessage(LiveMessage liveMessage) {
        if (liveMessage == null) {
            return false;
        }
        switch ($SWITCH_TABLE$cn$banshenggua$aichang$room$message$MessageKey()[liveMessage.mKey.ordinal()]) {
            case 44:
                return false;
            default:
                String str = liveMessage.mUid;
                if (TextUtils.isEmpty(str)) {
                    return false;
                }
                if (PreferencesUtils.loadPrefBoolean(this, PreferencesUtils.HEAD_MASK + str, false)) {
                    switch ($SWITCH_TABLE$cn$banshenggua$aichang$room$message$MessageKey()[liveMessage.mKey.ordinal()]) {
                        case 7:
                        case 8:
                            return true;
                    }
                }
                try {
                    if (Long.parseLong(str) > 0) {
                        return false;
                    }
                    switch ($SWITCH_TABLE$cn$banshenggua$aichang$room$message$MessageKey()[liveMessage.mKey.ordinal()]) {
                        case 5:
                        case 6:
                            if (liveMessage.mFlag == LiveMessage.Message_Flag.Message_ACK) {
                                choseMessageProcess(liveMessage);
                                break;
                            }
                            break;
                    }
                    return true;
                } catch (NumberFormatException e) {
                    return false;
                }
        }
    }

    private void choseMessageProcess(LiveMessage liveMessage) {
        if (liveMessage != null) {
            ULog.d(SocketRouter.TAG, "choseMessageProcess begin " + liveMessage.mKey + "; flag: " + liveMessage.mFlag);
            LiveMessageProcess liveMessageProcess = this.mMessageProcessMap.get(liveMessage.mKey);
            if (liveMessageProcess != null) {
                switch ($SWITCH_TABLE$cn$banshenggua$aichang$room$message$LiveMessage$Message_Flag()[liveMessage.mFlag.ordinal()]) {
                    case 1:
                        liveMessageProcess.normalMessage(liveMessage);
                        break;
                    case 2:
                        liveMessageProcess.ackMessage(liveMessage);
                        break;
                    case 3:
                        liveMessageProcess.broadcastMesssage(liveMessage);
                        break;
                    case 4:
                        liveMessageProcess.serverMessage(liveMessage);
                        break;
                    case 5:
                        liveMessageProcess.serverMessage(liveMessage);
                        break;
                }
            }
            ULog.d(SocketRouter.TAG, "choseMessageProcess end " + liveMessage.mKey + "; flag: " + liveMessage.mFlag);
        }
    }

    /* access modifiers changed from: private */
    public void connectRoom(String str, int i) {
        if (this.mRouter != null) {
            this.mRouter.disconnect();
            this.mRouter = null;
        }
        if (!TextUtils.isEmpty(str) && i > 0) {
            this.mRouter = new SocketRouter(str, i);
            this.mRouter.setListener(this);
            if (this.mRouter.connect()) {
                joinRoom(PreferencesUtils.loadPrefString(this, "rid" + mRoom.f1178a, ""), true);
            }
        }
    }

    /* access modifiers changed from: private */
    public void joinRoom(String str, boolean z) {
        if (z && mConfig != null) {
            this.mRouter.sendMessage(new SimpleMessage(SimpleMessage.SimpleType.Message_Login, mRoom, mConfig.d).getSocketMessage(), false);
        }
        if (mConfig != null) {
            SimpleMessage simpleMessage = new SimpleMessage(SimpleMessage.SimpleType.Message_Join, mRoom, mConfig.d);
            mRoom.d = str.trim();
            simpleMessage.roomPass = mRoom.d;
            this.mRouter.sendMessage(simpleMessage.getSocketMessage(), false);
        }
    }

    /* access modifiers changed from: private */
    public void openMediaPlayer(Rtmp.RtmpUrls rtmpUrls, int i) {
        LiveObjectController.LiveObjectIndex liveObjectIndex;
        if (rtmpUrls != null && !TextUtils.isEmpty(rtmpUrls.uid)) {
            String str = null;
            boolean equalsIgnoreCase = rtmpUrls.media.equalsIgnoreCase("video");
            String str2 = rtmpUrls.urlAudio;
            if (equalsIgnoreCase) {
                str = rtmpUrls.urlVideo;
            }
            String str3 = rtmpUrls.uid;
            ULog.d(SocketRouter.TAG, "audiourl: " + str2);
            ULog.d(SocketRouter.TAG, "videourl: " + str);
            LiveObjectController.LiveObjectIndex liveObjectIndex2 = LiveObjectController.LiveObjectIndex.Primary;
            if (i == 1) {
                liveObjectIndex = LiveObjectController.LiveObjectIndex.Secondary;
            } else {
                liveObjectIndex = liveObjectIndex2;
            }
            LiveObject.LiveObjectType liveObjectType = equalsIgnoreCase ? LiveObject.LiveObjectType.VideoPlayer : LiveObject.LiveObjectType.AudioPlayer;
            User user = new User();
            user.mUid = str3;
            if (mController.isPlayController()) {
                if (!mController.isPlayVideo() && equalsIgnoreCase) {
                    mController.setControllerType(LiveController.LiveControllerType.VideoPlay);
                }
                if (liveObjectIndex == LiveObjectController.LiveObjectIndex.Secondary) {
                    mController.ClosePlayLyric();
                }
            }
            mLiveObjectController.addLiveObject(liveObjectIndex, liveObjectType, rtmpUrls, user);
            mLiveObjectController.startLiveObject(liveObjectIndex);
        }
    }

    /* access modifiers changed from: private */
    public void closeMediaPlayer(int i) {
        switch (i) {
            case -1:
                mLiveObjectController.removeAll();
                return;
            case 0:
                mLiveObjectController.removeLiveObject(LiveObjectController.LiveObjectIndex.Primary);
                return;
            case 1:
                mLiveObjectController.removeLiveObject(LiveObjectController.LiveObjectIndex.Secondary);
                return;
            default:
                return;
        }
    }

    private void updateMenuNotify(int i) {
        KShareUtil.setNumUpIcon(i, (TextView) findViewById(a.f.room_notify_count));
    }

    /* access modifiers changed from: private */
    public void showRoomInfo(o oVar) {
        showRoomInfo(oVar, false, false);
    }

    /* access modifiers changed from: private */
    public void showPlayerInfo(User user, boolean z) {
        mController.showPlayInfo(user, z);
        this.mGiftView.cleanGifts();
        if (this.mCenterGiftView != null) {
            this.mCenterGiftView.cleanGifts();
        }
    }

    /* access modifiers changed from: private */
    public void showRoomInfo(o oVar, boolean z, boolean z2) {
        if (oVar != null) {
            mRoom = oVar;
            mRoom.b();
            if (this.liveMessageAdapters != null) {
                for (int i = 0; i < this.liveMessageAdapters.size(); i++) {
                    RoomMessgeAdapter roomMessgeAdapter = (RoomMessgeAdapter) this.liveMessageAdapters.get(i);
                    if (roomMessgeAdapter != null) {
                        roomMessgeAdapter.setRoomClass(mRoom.R);
                    }
                }
            }
            mController.showRoomInfo(mRoom);
            if (!z2) {
                return;
            }
            if (oVar.n != null) {
                showPlayerInfo(oVar.n, false);
                if (oVar.n.mBanzou != null && oVar.n.mBanzou.bztime > 0) {
                    if (mMicUser == null) {
                        mMicUser = oVar.n;
                    }
                    if (!mMicUser.mUid.equalsIgnoreCase(oVar.n.mUid)) {
                        mMicUser = oVar.n;
                    }
                    processRecorderUser(mMicUser);
                    if (z) {
                        openMediaPlayer(this.mAudRtmpUrl, 0);
                        if (this.mRightAudRtmpUrl != null) {
                            openMediaPlayer(this.mRightAudRtmpUrl, 1);
                        }
                        mLiveObjectController.setUser(LiveObjectController.LiveObjectIndex.Primary, mMicUser);
                    }
                    if (oVar.n.mBanzou != null) {
                        mController.stopTimer();
                        mController.beginTimer(oVar.n.mBanzou.bztime, oVar.t, oVar.u);
                        return;
                    }
                    return;
                }
                return;
            }
            showPlayerInfo(null, false);
            closeMediaPlayer(-1);
        }
    }

    private void sendBroadcast(String str, MessageKey messageKey) {
        Intent intent = new Intent(str);
        intent.putExtra("message_key", messageKey.getKey());
        sendBroadcast(intent);
    }

    private class ConnectRoomTask extends AsyncTask<Void, Void, Void> {
        private ConnectRoomTask() {
        }

        /* synthetic */ ConnectRoomTask(SimpleLiveRoomActivity simpleLiveRoomActivity, ConnectRoomTask connectRoomTask) {
            this();
        }

        /* access modifiers changed from: protected */
        public Void doInBackground(Void... voidArr) {
            if (SimpleLiveRoomActivity.mConfig == null) {
                return null;
            }
            SimpleLiveRoomActivity.this.connectRoom(SimpleLiveRoomActivity.mConfig.f1172a, SimpleLiveRoomActivity.mConfig.b);
            return null;
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Void voidR) {
            super.onPostExecute((Object) voidR);
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            super.onPreExecute();
        }
    }

    private class RoomInfoListener extends r {
        private RoomInfoListener() {
        }

        /* synthetic */ RoomInfoListener(SimpleLiveRoomActivity simpleLiveRoomActivity, RoomInfoListener roomInfoListener) {
            this();
        }

        public void onResponse(JSONObject jSONObject) {
            super.onResponse(jSONObject);
            ULog.d("JSGAPI", "RoomInfoListener response: " + jSONObject);
            ULog.d("JSGAPI", "RoomInfoListener response: " + jSONObject.optJSONObject(SocketMessage.MSG_RESULE_KEY));
            ULog.d("JSGAPI", "parseError: " + SimpleLiveRoomActivity.mRoom.i() + "; noerror: " + 0);
            SimpleLiveRoomActivity.mRoom.a(jSONObject);
            if (SimpleLiveRoomActivity.mRoom.i() != -1000) {
                KShareUtil.showToastJsonStatus(SimpleLiveRoomActivity.this, SimpleLiveRoomActivity.mRoom);
                return;
            }
            if (SimpleLiveRoomActivity.mRoom.P != null && SimpleLiveRoomActivity.mRoom.P.size() > 0) {
                if (SimpleLiveRoomActivity.this.mHanHua == null) {
                    SimpleLiveRoomActivity.this.mHanHua = new ArrayList();
                }
                SimpleLiveRoomActivity.this.mHanHua.clear();
                SimpleLiveRoomActivity.this.mHanHua.addAll(SimpleLiveRoomActivity.mRoom.P);
                GiftUtils.putGifts(SimpleLiveRoomActivity.this.mHanHua);
            }
            SimpleLiveRoomActivity.mConfig.c = SimpleLiveRoomActivity.mRoom.f1178a;
            ULog.d("JSGAPI", "roominfo id: " + SimpleLiveRoomActivity.mRoom.f1178a + "; config.mRid: " + SimpleLiveRoomActivity.mConfig.c);
            SimpleLiveRoomActivity.mConfig.a(SimpleLiveRoomActivity.this.mLiveConfigListener);
            SimpleLiveRoomActivity.mConfig.a();
        }
    }

    private class LiveMessageProcess {
        private LiveMessageProcess() {
        }

        /* synthetic */ LiveMessageProcess(SimpleLiveRoomActivity simpleLiveRoomActivity, LiveMessageProcess liveMessageProcess) {
            this();
        }

        public void ackMessage(LiveMessage liveMessage) {
            comProcess(liveMessage);
        }

        public void broadcastMesssage(LiveMessage liveMessage) {
            comProcess(liveMessage);
        }

        public void serverMessage(LiveMessage liveMessage) {
            comProcess(liveMessage);
        }

        public void normalMessage(LiveMessage liveMessage) {
            comProcess(liveMessage);
        }

        public void comProcess(LiveMessage liveMessage) {
        }
    }

    private class JoinMessage extends LiveMessageProcess {
        private JoinMessage() {
            super(SimpleLiveRoomActivity.this, null);
        }

        /* synthetic */ JoinMessage(SimpleLiveRoomActivity simpleLiveRoomActivity, JoinMessage joinMessage) {
            this();
        }

        public void ackMessage(LiveMessage liveMessage) {
            if ((SimpleLiveRoomActivity.mRoom != null) && (!TextUtils.isEmpty(SimpleLiveRoomActivity.mRoom.d))) {
                PreferencesUtils.savePrefString(SimpleLiveRoomActivity.this, "rid" + SimpleLiveRoomActivity.mRoom.f1178a, SimpleLiveRoomActivity.mRoom.d);
            }
            List<Rtmp.RtmpUrls> list = ((SimpleMessage) liveMessage).mRtmp.urlsList;
            if (list != null && list.size() > 0) {
                SimpleLiveRoomActivity.this.mAudRtmpUrl = list.get(0);
            }
            if (list == null || list.size() <= 1) {
                SimpleLiveRoomActivity.this.mRightAudRtmpUrl = null;
            } else {
                SimpleLiveRoomActivity.this.mRightAudRtmpUrl = list.get(1);
            }
            SimpleLiveRoomActivity.this.showRoomInfo(((SimpleMessage) liveMessage).retRoom, true, true);
        }

        public void broadcastMesssage(LiveMessage liveMessage) {
            super.broadcastMesssage(liveMessage);
        }

        public void comProcess(LiveMessage liveMessage) {
            if (SimpleLiveRoomActivity.mRoom == null) {
                SimpleLiveRoomActivity.mRoom = ((SimpleMessage) liveMessage).retRoom;
            }
            SimpleLiveRoomActivity.mRoom.a(((SimpleMessage) liveMessage).retRoom);
            SimpleLiveRoomActivity.this.showRoomInfo(SimpleLiveRoomActivity.mRoom);
            SimpleLiveRoomActivity.this.addMessageToAdapter(0, liveMessage, true);
        }
    }

    private class TalkMessage extends LiveMessageProcess {
        private TalkMessage() {
            super(SimpleLiveRoomActivity.this, null);
        }

        /* synthetic */ TalkMessage(SimpleLiveRoomActivity simpleLiveRoomActivity, TalkMessage talkMessage) {
            this();
        }

        public void ackMessage(LiveMessage liveMessage) {
            SimpleLiveRoomActivity.this.addMessageToAdapter(0, liveMessage, true);
        }

        public void comProcess(LiveMessage liveMessage) {
            if (TextUtils.isEmpty(liveMessage.mUid) || !liveMessage.mUid.equalsIgnoreCase(Session.getCurrentAccount().b)) {
                SimpleLiveRoomActivity.this.addMessageToAdapter(0, liveMessage, true);
            }
        }
    }

    private class STalkMessage extends LiveMessageProcess {
        private STalkMessage() {
            super(SimpleLiveRoomActivity.this, null);
        }

        /* synthetic */ STalkMessage(SimpleLiveRoomActivity simpleLiveRoomActivity, STalkMessage sTalkMessage) {
            this();
        }

        public void comProcess(LiveMessage liveMessage) {
            if (SimpleLiveRoomActivity.this.mPager.getCurrentItem() != 1) {
                SimpleLiveRoomActivity simpleLiveRoomActivity = SimpleLiveRoomActivity.this;
                simpleLiveRoomActivity.mMidNum = simpleLiveRoomActivity.mMidNum + 1;
                KShareUtil.setNumUpIcon(SimpleLiveRoomActivity.this.mMidNum, (Button) SimpleLiveRoomActivity.this.findViewById(a.f.notify_count_middle));
            }
            SimpleLiveRoomActivity.this.addMessageToAdapter(1, liveMessage, true);
        }
    }

    /* access modifiers changed from: private */
    public void processRecorderUser(User user) {
        if (this.mRecorderUser == null || !this.mRecorderUser.mUid.equalsIgnoreCase(Session.getCurrentAccount().b)) {
            this.mRecorderUser = user;
        }
    }

    private class ChangeMicMessage extends LiveMessageProcess {
        private static /* synthetic */ int[] $SWITCH_TABLE$com$pocketmusic$kshare$requestobjs$Room$RoomClass;

        static /* synthetic */ int[] $SWITCH_TABLE$com$pocketmusic$kshare$requestobjs$Room$RoomClass() {
            int[] iArr = $SWITCH_TABLE$com$pocketmusic$kshare$requestobjs$Room$RoomClass;
            if (iArr == null) {
                iArr = new int[o.a.values().length];
                try {
                    iArr[o.a.Match.ordinal()] = 2;
                } catch (NoSuchFieldError e) {
                }
                try {
                    iArr[o.a.Normal.ordinal()] = 1;
                } catch (NoSuchFieldError e2) {
                }
                try {
                    iArr[o.a.Show.ordinal()] = 3;
                } catch (NoSuchFieldError e3) {
                }
                $SWITCH_TABLE$com$pocketmusic$kshare$requestobjs$Room$RoomClass = iArr;
            }
            return iArr;
        }

        private ChangeMicMessage() {
            super(SimpleLiveRoomActivity.this, null);
        }

        /* synthetic */ ChangeMicMessage(SimpleLiveRoomActivity simpleLiveRoomActivity, ChangeMicMessage changeMicMessage) {
            this();
        }

        public void comProcess(LiveMessage liveMessage) {
            if (liveMessage instanceof MicMessage) {
                MicMessage micMessage = (MicMessage) liveMessage;
                if (!(micMessage.mRtmp == null || micMessage.mRtmp.urlsList == null || micMessage.mRtmp.urlsList.size() <= 0)) {
                    SimpleLiveRoomActivity.this.mAudRtmpUrl = micMessage.mRtmp.urlsList.get(0);
                }
                SimpleLiveRoomActivity.this.showPlayerInfo(null, false);
                if (micMessage.mUpUser != null) {
                    micMessage.mUpUser.mBanzou = micMessage.mBanzou;
                }
                SimpleLiveRoomActivity.mMicUser = micMessage.mUpUser;
                SimpleLiveRoomActivity.this.processRecorderUser(SimpleLiveRoomActivity.mMicUser);
                SimpleLiveRoomActivity.this.closeMediaPlayer(-1);
                SimpleLiveRoomActivity.this.downMic();
                if (micMessage.mDownUser != null && !micMessage.mDownUser.isAnonymous()) {
                    SimpleLiveRoomActivity.this.addMessageToAdapter(0, liveMessage, true);
                    if (micMessage.mDownUser.mUid.equalsIgnoreCase(Session.getCurrentAccount().b)) {
                        switch ($SWITCH_TABLE$com$pocketmusic$kshare$requestobjs$Room$RoomClass()[SimpleLiveRoomActivity.mRoom.R.ordinal()]) {
                            case 2:
                            default:
                                return;
                        }
                    }
                }
            }
        }
    }

    private class GiftMessageProcess extends LiveMessageProcess {
        private static /* synthetic */ int[] $SWITCH_TABLE$cn$banshenggua$aichang$room$message$LiveMessage$PanelType;
        private static /* synthetic */ int[] $SWITCH_TABLE$com$pocketmusic$kshare$requestobjs$Gift$GiftType;

        static /* synthetic */ int[] $SWITCH_TABLE$cn$banshenggua$aichang$room$message$LiveMessage$PanelType() {
            int[] iArr = $SWITCH_TABLE$cn$banshenggua$aichang$room$message$LiveMessage$PanelType;
            if (iArr == null) {
                iArr = new int[LiveMessage.PanelType.values().length];
                try {
                    iArr[LiveMessage.PanelType.Gift.ordinal()] = 3;
                } catch (NoSuchFieldError e) {
                }
                try {
                    iArr[LiveMessage.PanelType.Public.ordinal()] = 1;
                } catch (NoSuchFieldError e2) {
                }
                try {
                    iArr[LiveMessage.PanelType.TalkTo.ordinal()] = 2;
                } catch (NoSuchFieldError e3) {
                }
                $SWITCH_TABLE$cn$banshenggua$aichang$room$message$LiveMessage$PanelType = iArr;
            }
            return iArr;
        }

        static /* synthetic */ int[] $SWITCH_TABLE$com$pocketmusic$kshare$requestobjs$Gift$GiftType() {
            int[] iArr = $SWITCH_TABLE$com$pocketmusic$kshare$requestobjs$Gift$GiftType;
            if (iArr == null) {
                iArr = new int[e.c.values().length];
                try {
                    iArr[e.c.GuiBin.ordinal()] = 4;
                } catch (NoSuchFieldError e) {
                }
                try {
                    iArr[e.c.HanHua.ordinal()] = 3;
                } catch (NoSuchFieldError e2) {
                }
                try {
                    iArr[e.c.Normal.ordinal()] = 1;
                } catch (NoSuchFieldError e3) {
                }
                try {
                    iArr[e.c.SaQian.ordinal()] = 2;
                } catch (NoSuchFieldError e4) {
                }
                $SWITCH_TABLE$com$pocketmusic$kshare$requestobjs$Gift$GiftType = iArr;
            }
            return iArr;
        }

        private GiftMessageProcess() {
            super(SimpleLiveRoomActivity.this, null);
        }

        /* synthetic */ GiftMessageProcess(SimpleLiveRoomActivity simpleLiveRoomActivity, GiftMessageProcess giftMessageProcess) {
            this();
        }

        public void ackMessage(LiveMessage liveMessage) {
        }

        public void comProcess(LiveMessage liveMessage) {
            boolean z;
            if (liveMessage instanceof GiftMessage) {
                GiftMessage giftMessage = (GiftMessage) liveMessage;
                e gift = giftMessage.getGift();
                if (gift == null || !"0".equalsIgnoreCase(gift.c) || PreferencesUtils.loadPrefBoolean(SimpleLiveRoomActivity.this, SimpleLiveRoomActivity.PRE_KEY_SHOWFREEGIFT, true)) {
                    if (!(giftMessage.mTo == null || !giftMessage.mTo.mUid.equalsIgnoreCase(Session.getCurrentAccount().b) || SimpleLiveRoomActivity.this.mPager.getCurrentItem() == 3)) {
                        SimpleLiveRoomActivity simpleLiveRoomActivity = SimpleLiveRoomActivity.this;
                        simpleLiveRoomActivity.mRightNum = simpleLiveRoomActivity.mRightNum + 1;
                        KShareUtil.setNumUpIcon(SimpleLiveRoomActivity.this.mRightNum, (Button) SimpleLiveRoomActivity.this.findViewById(a.f.notify_count_right));
                    }
                    if (gift != null) {
                        switch ($SWITCH_TABLE$com$pocketmusic$kshare$requestobjs$Gift$GiftType()[gift.O.ordinal()]) {
                            case 2:
                                switch ($SWITCH_TABLE$cn$banshenggua$aichang$room$message$LiveMessage$PanelType()[liveMessage.mPanel.ordinal()]) {
                                    case 3:
                                        SimpleLiveRoomActivity.this.addMessageToAdapter(2, liveMessage, true);
                                        z = false;
                                        break;
                                    default:
                                        SimpleLiveRoomActivity.this.addMessageToAdapter(0, liveMessage, true);
                                        z = true;
                                        break;
                                }
                            case 3:
                                SimpleLiveRoomActivity.this.addMessageToAdapter(0, liveMessage, true);
                                z = true;
                                break;
                            case 4:
                                SimpleLiveRoomActivity.this.addMessageToAdapter(0, liveMessage, true);
                                z = false;
                                break;
                            default:
                                SimpleLiveRoomActivity.this.addMessageToAdapter(0, liveMessage, true);
                                SimpleLiveRoomActivity.this.addMessageToAdapter(2, liveMessage, true);
                                if (SimpleLiveRoomActivity.mMicUser != null && (giftMessage.mTo == null || SimpleLiveRoomActivity.mMicUser.mUid.equals(giftMessage.mTo.mUid))) {
                                    z = true;
                                    break;
                                } else {
                                    z = false;
                                    break;
                                }
                                break;
                        }
                        ULog.d(LiveGiftView.TAG, "showAnim: " + z + "gift.id: " + gift.b);
                        if (!z) {
                            return;
                        }
                        if (!"1".equalsIgnoreCase(SimpleLiveRoomActivity.mConfig.e) && gift.O != e.c.HanHua) {
                            SimpleLiveRoomActivity.this.mGiftView.playGiftAnim(SimpleLiveRoomActivity.mConfig, gift, giftMessage);
                        } else if (!"1".equalsIgnoreCase(giftMessage.mGlobal)) {
                            SimpleLiveRoomActivity.this.mCenterGiftView.playGiftAnim(SimpleLiveRoomActivity.mConfig, gift, giftMessage);
                        }
                    }
                }
            }
        }
    }

    private class AdjustMicMessage extends LiveMessageProcess {
        private AdjustMicMessage() {
            super(SimpleLiveRoomActivity.this, null);
        }

        /* synthetic */ AdjustMicMessage(SimpleLiveRoomActivity simpleLiveRoomActivity, AdjustMicMessage adjustMicMessage) {
            this();
        }

        public void ackMessage(LiveMessage liveMessage) {
            Toaster.showLongAtCenter(SimpleLiveRoomActivity.this, "调整排麦顺序成功");
        }

        public void broadcastMesssage(LiveMessage liveMessage) {
            SimpleLiveRoomActivity.this.addMessageToAdapter(0, liveMessage, true);
        }
    }

    private class ReqMicMessage extends LiveMessageProcess {
        private ReqMicMessage() {
            super(SimpleLiveRoomActivity.this, null);
        }

        /* synthetic */ ReqMicMessage(SimpleLiveRoomActivity simpleLiveRoomActivity, ReqMicMessage reqMicMessage) {
            this();
        }

        public void ackMessage(LiveMessage liveMessage) {
            if (SimpleLiveRoomActivity.this.mMedia != null) {
                Toaster.showShortAtCenter(SimpleLiveRoomActivity.this, "您已经排麦，请耐心等待");
            }
        }

        public void comProcess(LiveMessage liveMessage) {
            SimpleLiveRoomActivity.this.addMessageToAdapter(0, liveMessage, true);
        }
    }

    private class CancelMicMessage extends LiveMessageProcess {
        private static /* synthetic */ int[] $SWITCH_TABLE$cn$banshenggua$aichang$room$message$MicMessage$SwitchMicReason;

        static /* synthetic */ int[] $SWITCH_TABLE$cn$banshenggua$aichang$room$message$MicMessage$SwitchMicReason() {
            int[] iArr = $SWITCH_TABLE$cn$banshenggua$aichang$room$message$MicMessage$SwitchMicReason;
            if (iArr == null) {
                iArr = new int[MicMessage.SwitchMicReason.values().length];
                try {
                    iArr[MicMessage.SwitchMicReason.ADMIN_CANCEL_MIC_QUEUE.ordinal()] = 10;
                } catch (NoSuchFieldError e) {
                }
                try {
                    iArr[MicMessage.SwitchMicReason.ADMIN_CANEL_MIC.ordinal()] = 4;
                } catch (NoSuchFieldError e2) {
                }
                try {
                    iArr[MicMessage.SwitchMicReason.NO_USER_ON_MIC.ordinal()] = 1;
                } catch (NoSuchFieldError e3) {
                }
                try {
                    iArr[MicMessage.SwitchMicReason.SERVER_CANCEL_HB_DIE.ordinal()] = 6;
                } catch (NoSuchFieldError e4) {
                }
                try {
                    iArr[MicMessage.SwitchMicReason.SERVER_CANCEL_NO_HB.ordinal()] = 5;
                } catch (NoSuchFieldError e5) {
                }
                try {
                    iArr[MicMessage.SwitchMicReason.SERVER_CANCEL_USERLEAVE.ordinal()] = 7;
                } catch (NoSuchFieldError e6) {
                }
                try {
                    iArr[MicMessage.SwitchMicReason.SERVER_TIME_END.ordinal()] = 8;
                } catch (NoSuchFieldError e7) {
                }
                try {
                    iArr[MicMessage.SwitchMicReason.USER_CANCEL_MIC.ordinal()] = 2;
                } catch (NoSuchFieldError e8) {
                }
                try {
                    iArr[MicMessage.SwitchMicReason.USER_CANCEL_MIC_QUEUE.ordinal()] = 9;
                } catch (NoSuchFieldError e9) {
                }
                try {
                    iArr[MicMessage.SwitchMicReason.USER_CANCEL_WEAK_NET.ordinal()] = 3;
                } catch (NoSuchFieldError e10) {
                }
                $SWITCH_TABLE$cn$banshenggua$aichang$room$message$MicMessage$SwitchMicReason = iArr;
            }
            return iArr;
        }

        private CancelMicMessage() {
            super(SimpleLiveRoomActivity.this, null);
        }

        /* synthetic */ CancelMicMessage(SimpleLiveRoomActivity simpleLiveRoomActivity, CancelMicMessage cancelMicMessage) {
            this();
        }

        public void ackMessage(LiveMessage liveMessage) {
            Toaster.showLongAtCenter(SimpleLiveRoomActivity.this, "操作成功");
        }

        public void comProcess(LiveMessage liveMessage) {
            switch ($SWITCH_TABLE$cn$banshenggua$aichang$room$message$MicMessage$SwitchMicReason()[((MicMessage) liveMessage).mReason.ordinal()]) {
                case 9:
                case 10:
                    SimpleLiveRoomActivity.this.addMessageToAdapter(0, liveMessage, true);
                    return;
                default:
                    return;
            }
        }
    }

    private class CloseMicMessage extends LiveMessageProcess {
        private CloseMicMessage() {
            super(SimpleLiveRoomActivity.this, null);
        }

        /* synthetic */ CloseMicMessage(SimpleLiveRoomActivity simpleLiveRoomActivity, CloseMicMessage closeMicMessage) {
            this();
        }

        public void ackMessage(LiveMessage liveMessage) {
            SimpleLiveRoomActivity.this.downMic();
        }

        public void serverMessage(LiveMessage liveMessage) {
            SimpleLiveRoomActivity.this.downMic();
        }

        public void comProcess(LiveMessage liveMessage) {
            SimpleLiveRoomActivity.this.addMessageToAdapter(0, liveMessage, true);
        }
    }

    private class OpenMicMessage extends LiveMessageProcess {
        private OpenMicMessage() {
            super(SimpleLiveRoomActivity.this, null);
        }

        /* synthetic */ OpenMicMessage(SimpleLiveRoomActivity simpleLiveRoomActivity, OpenMicMessage openMicMessage) {
            this();
        }

        public void ackMessage(LiveMessage liveMessage) {
            processOpenMic(liveMessage);
        }

        public void serverMessage(LiveMessage liveMessage) {
            processOpenMic(liveMessage);
        }

        private void processOpenMic(LiveMessage liveMessage) {
        }
    }

    private class KickUserMessage extends LiveMessageProcess {
        private KickUserMessage() {
            super(SimpleLiveRoomActivity.this, null);
        }

        /* synthetic */ KickUserMessage(SimpleLiveRoomActivity simpleLiveRoomActivity, KickUserMessage kickUserMessage) {
            this();
        }

        public void ackMessage(LiveMessage liveMessage) {
            Toaster.showLongAtCenter(SimpleLiveRoomActivity.this, "踢人成功");
        }

        public void serverMessage(LiveMessage liveMessage) {
            if (((SimpleMessage) liveMessage).mUser.mUid.equalsIgnoreCase(Session.getCurrentAccount().b)) {
                SimpleLiveRoomActivity.this.showRoomMuted("你被管理员踢出了房间");
            }
        }

        public void comProcess(LiveMessage liveMessage) {
            SimpleLiveRoomActivity.this.addMessageToAdapter(0, liveMessage, true);
        }
    }

    private class LeaveMessage extends LiveMessageProcess {
        private LeaveMessage() {
            super(SimpleLiveRoomActivity.this, null);
        }

        /* synthetic */ LeaveMessage(SimpleLiveRoomActivity simpleLiveRoomActivity, LeaveMessage leaveMessage) {
            this();
        }

        public void ackMessage(LiveMessage liveMessage) {
        }

        public void serverMessage(LiveMessage liveMessage) {
        }

        public void comProcess(LiveMessage liveMessage) {
            SimpleMessage simpleMessage = (SimpleMessage) liveMessage;
            if (SimpleLiveRoomActivity.mRoom == null && simpleMessage.retRoom != null) {
                SimpleLiveRoomActivity.mRoom = simpleMessage.retRoom;
            }
            SimpleLiveRoomActivity.mRoom.a(simpleMessage.retRoom);
            SimpleLiveRoomActivity.this.showRoomInfo(SimpleLiveRoomActivity.mRoom);
            if (simpleMessage.mUser.mUid.equalsIgnoreCase(SimpleLiveRoomActivity.mRoom.m.mUid)) {
                SimpleLiveRoomActivity.this.addMessageToAdapter(0, liveMessage, true);
            }
        }
    }

    private class RoomUpdateMessage extends LiveMessageProcess {
        private static /* synthetic */ int[] $SWITCH_TABLE$cn$banshenggua$aichang$room$message$SimpleMessage$PropertyType;

        static /* synthetic */ int[] $SWITCH_TABLE$cn$banshenggua$aichang$room$message$SimpleMessage$PropertyType() {
            int[] iArr = $SWITCH_TABLE$cn$banshenggua$aichang$room$message$SimpleMessage$PropertyType;
            if (iArr == null) {
                iArr = new int[SimpleMessage.PropertyType.values().length];
                try {
                    iArr[SimpleMessage.PropertyType.NoSurport.ordinal()] = 3;
                } catch (NoSuchFieldError e) {
                }
                try {
                    iArr[SimpleMessage.PropertyType.RoomName.ordinal()] = 2;
                } catch (NoSuchFieldError e2) {
                }
                try {
                    iArr[SimpleMessage.PropertyType.Type.ordinal()] = 1;
                } catch (NoSuchFieldError e3) {
                }
                $SWITCH_TABLE$cn$banshenggua$aichang$room$message$SimpleMessage$PropertyType = iArr;
            }
            return iArr;
        }

        private RoomUpdateMessage() {
            super(SimpleLiveRoomActivity.this, null);
        }

        /* synthetic */ RoomUpdateMessage(SimpleLiveRoomActivity simpleLiveRoomActivity, RoomUpdateMessage roomUpdateMessage) {
            this();
        }

        public void comProcess(LiveMessage liveMessage) {
            SimpleMessage simpleMessage = (SimpleMessage) liveMessage;
            if (simpleMessage.mPropertys != null && simpleMessage.mPropertys.size() > 0) {
                int i = 0;
                while (true) {
                    int i2 = i;
                    if (i2 < simpleMessage.mPropertys.size()) {
                        SimpleMessage.Property property = simpleMessage.mPropertys.get(i2);
                        switch ($SWITCH_TABLE$cn$banshenggua$aichang$room$message$SimpleMessage$PropertyType()[property.getType().ordinal()]) {
                            case 1:
                                SimpleLiveRoomActivity.mRoom.R = o.a.a(property.getNewValue());
                                break;
                            case 2:
                                SimpleLiveRoomActivity.mRoom.b = property.getNewValue();
                                break;
                        }
                        i = i2 + 1;
                    }
                }
            }
            SimpleLiveRoomActivity.this.showRoomInfo(SimpleLiveRoomActivity.mRoom);
        }
    }

    private class RoomModMessage extends LiveMessageProcess {
        private RoomModMessage() {
            super(SimpleLiveRoomActivity.this, null);
        }

        /* synthetic */ RoomModMessage(SimpleLiveRoomActivity simpleLiveRoomActivity, RoomModMessage roomModMessage) {
            this();
        }

        public void comProcess(LiveMessage liveMessage) {
            SimpleMessage simpleMessage = (SimpleMessage) liveMessage;
            if (simpleMessage.retRoom != null) {
                SimpleLiveRoomActivity.this.showRoomInfo(simpleMessage.retRoom);
            }
        }
    }

    private class MediaMessage extends LiveMessageProcess {
        private MediaMessage() {
            super(SimpleLiveRoomActivity.this, null);
        }

        /* synthetic */ MediaMessage(SimpleLiveRoomActivity simpleLiveRoomActivity, MediaMessage mediaMessage) {
            this();
        }

        public void ackMessage(LiveMessage liveMessage) {
        }

        public void comProcess(LiveMessage liveMessage) {
            if (SimpleLiveRoomActivity.mController.isRecordController()) {
                SimpleLiveRoomActivity.mLiveObjectController.setUser(LiveObjectController.LiveObjectIndex.Primary, SimpleLiveRoomActivity.mMicUser);
            } else if (liveMessage instanceof SimpleMessage) {
                SimpleMessage simpleMessage = (SimpleMessage) liveMessage;
                if (simpleMessage.mAnchor != null) {
                    SimpleLiveRoomActivity.mMicUser = simpleMessage.mAnchor;
                    SimpleLiveRoomActivity.this.processRecorderUser(SimpleLiveRoomActivity.mMicUser);
                    ULog.d(SimpleLiveRoomActivity.this.TAG, "Message_Media - micUser: " + SimpleLiveRoomActivity.mMicUser.mUid);
                    SimpleLiveRoomActivity.this.closeMediaPlayer(-1);
                }
                if (SimpleLiveRoomActivity.this.mAudRtmpUrl != null && SimpleLiveRoomActivity.mMicUser != null) {
                    SimpleLiveRoomActivity.this.mAudRtmpUrl.isMixStream = SimpleLiveRoomActivity.mMicUser.isMixStreams;
                    SimpleLiveRoomActivity.this.showPlayerInfo(SimpleLiveRoomActivity.mMicUser, true);
                    SimpleLiveRoomActivity.this.openMediaPlayer(SimpleLiveRoomActivity.this.mAudRtmpUrl, 0);
                    SimpleLiveRoomActivity.mController.stopTimer();
                    SimpleLiveRoomActivity.mController.beginTimer(-1, 0, 0);
                    SimpleLiveRoomActivity.mLiveObjectController.setUser(LiveObjectController.LiveObjectIndex.Primary, SimpleLiveRoomActivity.mMicUser);
                }
            }
        }
    }

    private class MuteRoomMessage extends LiveMessageProcess {
        private MuteRoomMessage() {
            super(SimpleLiveRoomActivity.this, null);
        }

        /* synthetic */ MuteRoomMessage(SimpleLiveRoomActivity simpleLiveRoomActivity, MuteRoomMessage muteRoomMessage) {
            this();
        }

        public void comProcess(LiveMessage liveMessage) {
            SimpleLiveRoomActivity.this.showRoomMuted("房间被封");
        }
    }

    private class FamilyDisMessage extends LiveMessageProcess {
        private FamilyDisMessage() {
            super(SimpleLiveRoomActivity.this, null);
        }

        /* synthetic */ FamilyDisMessage(SimpleLiveRoomActivity simpleLiveRoomActivity, FamilyDisMessage familyDisMessage) {
            this();
        }

        public void comProcess(LiveMessage liveMessage) {
            SimpleLiveRoomActivity.this.showRoomMuted("家族已解散");
        }
    }

    private class ServerSysMessage extends LiveMessageProcess {
        private static /* synthetic */ int[] $SWITCH_TABLE$cn$banshenggua$aichang$room$message$LiveMessage$PanelType;

        static /* synthetic */ int[] $SWITCH_TABLE$cn$banshenggua$aichang$room$message$LiveMessage$PanelType() {
            int[] iArr = $SWITCH_TABLE$cn$banshenggua$aichang$room$message$LiveMessage$PanelType;
            if (iArr == null) {
                iArr = new int[LiveMessage.PanelType.values().length];
                try {
                    iArr[LiveMessage.PanelType.Gift.ordinal()] = 3;
                } catch (NoSuchFieldError e) {
                }
                try {
                    iArr[LiveMessage.PanelType.Public.ordinal()] = 1;
                } catch (NoSuchFieldError e2) {
                }
                try {
                    iArr[LiveMessage.PanelType.TalkTo.ordinal()] = 2;
                } catch (NoSuchFieldError e3) {
                }
                $SWITCH_TABLE$cn$banshenggua$aichang$room$message$LiveMessage$PanelType = iArr;
            }
            return iArr;
        }

        private ServerSysMessage() {
            super(SimpleLiveRoomActivity.this, null);
        }

        /* synthetic */ ServerSysMessage(SimpleLiveRoomActivity simpleLiveRoomActivity, ServerSysMessage serverSysMessage) {
            this();
        }

        public void comProcess(LiveMessage liveMessage) {
            if (liveMessage instanceof SimpleMessage) {
                switch ($SWITCH_TABLE$cn$banshenggua$aichang$room$message$LiveMessage$PanelType()[((SimpleMessage) liveMessage).mPanel.ordinal()]) {
                    case 2:
                        if (SimpleLiveRoomActivity.this.mPager.getCurrentItem() != 1) {
                            SimpleLiveRoomActivity simpleLiveRoomActivity = SimpleLiveRoomActivity.this;
                            simpleLiveRoomActivity.mMidNum = simpleLiveRoomActivity.mMidNum + 1;
                            KShareUtil.setNumUpIcon(SimpleLiveRoomActivity.this.mMidNum, (Button) SimpleLiveRoomActivity.this.findViewById(a.f.notify_count_middle));
                        }
                        SimpleLiveRoomActivity.this.addMessageToAdapter(1, liveMessage, true);
                        return;
                    default:
                        SimpleLiveRoomActivity.this.addMessageToAdapter(0, liveMessage, true);
                        return;
                }
            }
        }
    }

    private class ChangeBanzouMessage extends LiveMessageProcess {
        private ChangeBanzouMessage() {
            super(SimpleLiveRoomActivity.this, null);
        }

        /* synthetic */ ChangeBanzouMessage(SimpleLiveRoomActivity simpleLiveRoomActivity, ChangeBanzouMessage changeBanzouMessage) {
            this();
        }

        public void ackMessage(LiveMessage liveMessage) {
        }

        public void comProcess(LiveMessage liveMessage) {
            if (liveMessage instanceof MicMessage) {
                MicMessage micMessage = (MicMessage) liveMessage;
                if (SimpleLiveRoomActivity.mController != null && micMessage.mBanzou != null) {
                    SimpleLiveRoomActivity.mController.changeBanzou(micMessage.mBanzou);
                }
            }
        }
    }

    private class FamilyMessage extends LiveMessageProcess {
        private static /* synthetic */ int[] $SWITCH_TABLE$cn$banshenggua$aichang$room$message$ClubMessage$ClubMessageType;
        private static /* synthetic */ int[] $SWITCH_TABLE$cn$banshenggua$aichang$room$message$LiveMessage$PanelType;

        static /* synthetic */ int[] $SWITCH_TABLE$cn$banshenggua$aichang$room$message$ClubMessage$ClubMessageType() {
            int[] iArr = $SWITCH_TABLE$cn$banshenggua$aichang$room$message$ClubMessage$ClubMessageType;
            if (iArr == null) {
                iArr = new int[ClubMessage.ClubMessageType.values().length];
                try {
                    iArr[ClubMessage.ClubMessageType.APPLY.ordinal()] = 4;
                } catch (NoSuchFieldError e) {
                }
                try {
                    iArr[ClubMessage.ClubMessageType.APPLY_AGREE.ordinal()] = 7;
                } catch (NoSuchFieldError e2) {
                }
                try {
                    iArr[ClubMessage.ClubMessageType.APPLY_DISAGREE.ordinal()] = 6;
                } catch (NoSuchFieldError e3) {
                }
                try {
                    iArr[ClubMessage.ClubMessageType.APPLY_RESULT.ordinal()] = 5;
                } catch (NoSuchFieldError e4) {
                }
                try {
                    iArr[ClubMessage.ClubMessageType.DISSOLVE.ordinal()] = 3;
                } catch (NoSuchFieldError e5) {
                }
                try {
                    iArr[ClubMessage.ClubMessageType.JOIN.ordinal()] = 1;
                } catch (NoSuchFieldError e6) {
                }
                try {
                    iArr[ClubMessage.ClubMessageType.LEAVE.ordinal()] = 2;
                } catch (NoSuchFieldError e7) {
                }
                try {
                    iArr[ClubMessage.ClubMessageType.LEAVE_By_Admin.ordinal()] = 9;
                } catch (NoSuchFieldError e8) {
                }
                try {
                    iArr[ClubMessage.ClubMessageType.LEAVE_By_User.ordinal()] = 8;
                } catch (NoSuchFieldError e9) {
                }
                try {
                    iArr[ClubMessage.ClubMessageType.NO_SURPORT.ordinal()] = 10;
                } catch (NoSuchFieldError e10) {
                }
                $SWITCH_TABLE$cn$banshenggua$aichang$room$message$ClubMessage$ClubMessageType = iArr;
            }
            return iArr;
        }

        static /* synthetic */ int[] $SWITCH_TABLE$cn$banshenggua$aichang$room$message$LiveMessage$PanelType() {
            int[] iArr = $SWITCH_TABLE$cn$banshenggua$aichang$room$message$LiveMessage$PanelType;
            if (iArr == null) {
                iArr = new int[LiveMessage.PanelType.values().length];
                try {
                    iArr[LiveMessage.PanelType.Gift.ordinal()] = 3;
                } catch (NoSuchFieldError e) {
                }
                try {
                    iArr[LiveMessage.PanelType.Public.ordinal()] = 1;
                } catch (NoSuchFieldError e2) {
                }
                try {
                    iArr[LiveMessage.PanelType.TalkTo.ordinal()] = 2;
                } catch (NoSuchFieldError e3) {
                }
                $SWITCH_TABLE$cn$banshenggua$aichang$room$message$LiveMessage$PanelType = iArr;
            }
            return iArr;
        }

        private FamilyMessage() {
            super(SimpleLiveRoomActivity.this, null);
        }

        /* synthetic */ FamilyMessage(SimpleLiveRoomActivity simpleLiveRoomActivity, FamilyMessage familyMessage) {
            this();
        }

        public void ackMessage(LiveMessage liveMessage) {
        }

        public void comProcess(LiveMessage liveMessage) {
            if (liveMessage instanceof ClubMessage) {
                ClubMessage clubMessage = (ClubMessage) liveMessage;
                switch ($SWITCH_TABLE$cn$banshenggua$aichang$room$message$ClubMessage$ClubMessageType()[clubMessage.mType.ordinal()]) {
                    case 1:
                    case 2:
                    case 3:
                    case 4:
                    case 5:
                    case 6:
                    case 7:
                    case 8:
                    case 9:
                    default:
                        if (1 != 0) {
                            switch ($SWITCH_TABLE$cn$banshenggua$aichang$room$message$LiveMessage$PanelType()[clubMessage.mPanel.ordinal()]) {
                                case 2:
                                    if (SimpleLiveRoomActivity.this.liveMessageAdapters.size() >= 1) {
                                        SimpleLiveRoomActivity.this.mPager.getCurrentItem();
                                        SimpleLiveRoomActivity.this.addMessageToAdapter(1, liveMessage, true);
                                        return;
                                    }
                                    return;
                                default:
                                    SimpleLiveRoomActivity.this.addMessageToAdapter(0, liveMessage, true);
                                    return;
                            }
                        } else {
                            return;
                        }
                }
            }
        }
    }

    private class ToastMessage extends LiveMessageProcess {
        private ToastMessage() {
            super(SimpleLiveRoomActivity.this, null);
        }

        /* synthetic */ ToastMessage(SimpleLiveRoomActivity simpleLiveRoomActivity, ToastMessage toastMessage) {
            this();
        }

        public void comProcess(LiveMessage liveMessage) {
            if (liveMessage instanceof SimpleMessage) {
                SimpleMessage simpleMessage = (SimpleMessage) liveMessage;
                if (!TextUtils.isEmpty(simpleMessage.mMsg)) {
                    Toaster.showShortAtCenter(SimpleLiveRoomActivity.this, simpleMessage.mMsg);
                }
            }
        }
    }

    private class MultiReqMicMessage extends LiveMessageProcess {
        private MultiReqMicMessage() {
            super(SimpleLiveRoomActivity.this, null);
        }

        /* synthetic */ MultiReqMicMessage(SimpleLiveRoomActivity simpleLiveRoomActivity, MultiReqMicMessage multiReqMicMessage) {
            this();
        }

        public void ackMessage(LiveMessage liveMessage) {
            Toaster.showShort(SimpleLiveRoomActivity.this, "请求连麦成功");
        }

        public void comProcess(LiveMessage liveMessage) {
            if (SimpleLiveRoomActivity.canDoConnectMic()) {
                SimpleLiveRoomActivity.this.setMultiReqNum(SimpleLiveRoomActivity.this.multiReqNum + 1);
            }
        }
    }

    private class MultiInviteMicMessage extends LiveMessageProcess {
        private MultiInviteMicMessage() {
            super(SimpleLiveRoomActivity.this, null);
        }

        /* synthetic */ MultiInviteMicMessage(SimpleLiveRoomActivity simpleLiveRoomActivity, MultiInviteMicMessage multiInviteMicMessage) {
            this();
        }

        public void ackMessage(LiveMessage liveMessage) {
            Toaster.showLongAtCenter(SimpleLiveRoomActivity.this, "操作成功");
            SimpleLiveRoomActivity.this.backToCurrentActivity();
        }

        public void comProcess(LiveMessage liveMessage) {
            SimpleLiveRoomActivity.this.addMessageToAdapter(0, liveMessage, true);
        }
    }

    private class MultiChangeMicMessage extends LiveMessageProcess {
        private MultiChangeMicMessage() {
            super(SimpleLiveRoomActivity.this, null);
        }

        /* synthetic */ MultiChangeMicMessage(SimpleLiveRoomActivity simpleLiveRoomActivity, MultiChangeMicMessage multiChangeMicMessage) {
            this();
        }

        public void comProcess(LiveMessage liveMessage) {
            if (liveMessage instanceof MicMessage) {
                MicMessage micMessage = (MicMessage) liveMessage;
                if (!(micMessage.mRtmp == null || micMessage.mRtmp.urlsList == null || micMessage.mRtmp.urlsList.size() <= 0)) {
                    SimpleLiveRoomActivity.this.mRightAudRtmpUrl = micMessage.mRtmp.urlsList.get(0);
                }
                SimpleLiveRoomActivity.this.mMicRightUser = micMessage.mUpUser;
                SimpleLiveRoomActivity.this.processRecorderUser(SimpleLiveRoomActivity.this.mMicRightUser);
                if (micMessage.mDownUser != null && !micMessage.mDownUser.isAnonymous()) {
                    SimpleLiveRoomActivity.this.addMessageToAdapter(0, liveMessage, true);
                    SimpleLiveRoomActivity.this.closeMediaPlayer(1);
                }
            }
        }
    }

    private class MultiColseMicMessage extends LiveMessageProcess {
        private MultiColseMicMessage() {
            super(SimpleLiveRoomActivity.this, null);
        }

        /* synthetic */ MultiColseMicMessage(SimpleLiveRoomActivity simpleLiveRoomActivity, MultiColseMicMessage multiColseMicMessage) {
            this();
        }

        public void ackMessage(LiveMessage liveMessage) {
            SimpleLiveRoomActivity.this.downMic();
        }

        public void serverMessage(LiveMessage liveMessage) {
            SimpleLiveRoomActivity.this.downMic();
        }

        public void comProcess(LiveMessage liveMessage) {
            SimpleLiveRoomActivity.this.addMessageToAdapter(0, liveMessage, true);
        }
    }

    private class MultiConfirmMicMessage extends LiveMessageProcess {
        private MultiConfirmMicMessage() {
            super(SimpleLiveRoomActivity.this, null);
        }

        /* synthetic */ MultiConfirmMicMessage(SimpleLiveRoomActivity simpleLiveRoomActivity, MultiConfirmMicMessage multiConfirmMicMessage) {
            this();
        }
    }

    private class MultiMediaMessage extends LiveMessageProcess {
        private MultiMediaMessage() {
            super(SimpleLiveRoomActivity.this, null);
        }

        /* synthetic */ MultiMediaMessage(SimpleLiveRoomActivity simpleLiveRoomActivity, MultiMediaMessage multiMediaMessage) {
            this();
        }

        public void ackMessage(LiveMessage liveMessage) {
        }

        public void comProcess(LiveMessage liveMessage) {
            if (SimpleLiveRoomActivity.this.mMicRightUser != null && SimpleLiveRoomActivity.this.mMicRightUser.mUid.equalsIgnoreCase(Session.getCurrentAccount().b)) {
                SimpleLiveRoomActivity.mLiveObjectController.setUser(LiveObjectController.LiveObjectIndex.Secondary, SimpleLiveRoomActivity.this.mMicRightUser);
            } else if (liveMessage instanceof SimpleMessage) {
                if (((SimpleMessage) liveMessage).mAckType != SimpleMessage.ACKType.Accept || SimpleLiveRoomActivity.this.mRightAudRtmpUrl == null || SimpleLiveRoomActivity.this.mMicRightUser == null) {
                    SimpleLiveRoomActivity.this.mRightAudRtmpUrl = null;
                    SimpleLiveRoomActivity.this.mMicRightUser = null;
                    SimpleLiveRoomActivity.this.closeMediaPlayer(1);
                } else {
                    SimpleLiveRoomActivity.this.mRightAudRtmpUrl.isMixStream = SimpleLiveRoomActivity.this.mMicRightUser.isMixStreams;
                    SimpleLiveRoomActivity.this.closeMediaPlayer(1);
                    SimpleLiveRoomActivity.this.openMediaPlayer(SimpleLiveRoomActivity.this.mRightAudRtmpUrl, 1);
                    SimpleLiveRoomActivity.mLiveObjectController.setUser(LiveObjectController.LiveObjectIndex.Secondary, SimpleLiveRoomActivity.this.mMicRightUser);
                }
                SimpleLiveRoomActivity.this.addMessageToAdapter(0, liveMessage, true);
            }
        }
    }

    private class MultiCancelMicMessage extends LiveMessageProcess {
        private MultiCancelMicMessage() {
            super(SimpleLiveRoomActivity.this, null);
        }

        /* synthetic */ MultiCancelMicMessage(SimpleLiveRoomActivity simpleLiveRoomActivity, MultiCancelMicMessage multiCancelMicMessage) {
            this();
        }

        public void ackMessage(LiveMessage liveMessage) {
            Toaster.showShort(SimpleLiveRoomActivity.this, "操作成功");
        }

        public void comProcess(LiveMessage liveMessage) {
            super.comProcess(liveMessage);
            SimpleLiveRoomActivity.this.addMessageToAdapter(0, liveMessage, true);
            SimpleLiveRoomActivity.this.closeMediaPlayer(1);
        }
    }

    private class GlobalGiftMessageProcess extends LiveMessageProcess {
        private GlobalGiftMessageProcess() {
            super(SimpleLiveRoomActivity.this, null);
        }

        /* synthetic */ GlobalGiftMessageProcess(SimpleLiveRoomActivity simpleLiveRoomActivity, GlobalGiftMessageProcess globalGiftMessageProcess) {
            this();
        }

        public void ackMessage(LiveMessage liveMessage) {
        }

        public void comProcess(LiveMessage liveMessage) {
            if (liveMessage instanceof GiftMessage) {
                GiftMessage giftMessage = (GiftMessage) liveMessage;
                e gift = giftMessage.getGift();
                if (gift == null || !"0".equalsIgnoreCase(gift.c) || PreferencesUtils.loadPrefBoolean(SimpleLiveRoomActivity.this, SimpleLiveRoomActivity.PRE_KEY_SHOWFREEGIFT, true)) {
                    ULog.d(SocketRouter.TAG, "Global Gift: " + gift);
                    if (gift != null) {
                        gift.I = "1";
                        if (!SimpleLiveRoomActivity.mRoom.f1178a.equalsIgnoreCase(new StringBuilder(String.valueOf(giftMessage.mRid)).toString())) {
                            gift.K = false;
                        }
                        SimpleLiveRoomActivity.this.mCenterGiftView.playGiftAnim(SimpleLiveRoomActivity.mConfig, gift, giftMessage);
                    }
                }
            }
        }
    }
}
