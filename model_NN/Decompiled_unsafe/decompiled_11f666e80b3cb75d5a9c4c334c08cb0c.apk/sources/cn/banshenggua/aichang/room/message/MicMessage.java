package cn.banshenggua.aichang.room.message;

import android.text.TextUtils;
import com.pocketmusic.kshare.requestobjs.o;
import com.sina.weibo.sdk.register.mobile.SelectCountryActivity;
import com.tencent.stat.DeviceInfo;
import org.json.JSONObject;

public class MicMessage extends LiveMessage {
    private static /* synthetic */ int[] $SWITCH_TABLE$cn$banshenggua$aichang$room$message$MicMessage$CannelMicAction;
    private static /* synthetic */ int[] $SWITCH_TABLE$cn$banshenggua$aichang$room$message$MicMessage$MicType;
    public CannelMicAction mAction = CannelMicAction.No;
    public Banzou mBanzou = null;
    public String mBzid = null;
    public User mDownUser = null;
    public String mMedia = "audio";
    public String mMicId = null;
    public String mMsg = "";
    public int mOrder = 1;
    public SwitchMicReason mReason = SwitchMicReason.NO_USER_ON_MIC;
    public Rtmp mRtmp = null;
    public String mSongArtist = "";
    public String mSongDuration = "";
    public String mSongId = "";
    public String mSongName = "";
    public MicType mType = MicType.GetMicList;
    public String mUid = "";
    public User mUpUser = null;
    public User mUser = null;
    public UserList mUsers = null;

    public enum CannelMicAction {
        No,
        OnLine,
        OnList,
        OnLineByAdmin,
        OnListByAdmin,
        NetError,
        TIME_END
    }

    public enum MicType {
        ReqMic,
        CancelMic,
        GetMicList,
        OpenMic,
        CloseMic,
        ChangeMic,
        AdjustMic,
        ChangeBanzou,
        MultiReqMic,
        MultiDelMic,
        MultiInviteMic,
        MultiCloseMic,
        MultiConfirmMic,
        MultiChangeMic,
        MultiOpenMic,
        MultiCancelMic
    }

    public enum SwitchMicReason {
        NO_USER_ON_MIC,
        USER_CANCEL_MIC,
        USER_CANCEL_WEAK_NET,
        ADMIN_CANEL_MIC,
        SERVER_CANCEL_NO_HB,
        SERVER_CANCEL_HB_DIE,
        SERVER_CANCEL_USERLEAVE,
        SERVER_TIME_END,
        USER_CANCEL_MIC_QUEUE,
        ADMIN_CANCEL_MIC_QUEUE
    }

    static /* synthetic */ int[] $SWITCH_TABLE$cn$banshenggua$aichang$room$message$MicMessage$CannelMicAction() {
        int[] iArr = $SWITCH_TABLE$cn$banshenggua$aichang$room$message$MicMessage$CannelMicAction;
        if (iArr == null) {
            iArr = new int[CannelMicAction.values().length];
            try {
                iArr[CannelMicAction.NetError.ordinal()] = 6;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[CannelMicAction.No.ordinal()] = 1;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[CannelMicAction.OnLine.ordinal()] = 2;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[CannelMicAction.OnLineByAdmin.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[CannelMicAction.OnList.ordinal()] = 3;
            } catch (NoSuchFieldError e5) {
            }
            try {
                iArr[CannelMicAction.OnListByAdmin.ordinal()] = 5;
            } catch (NoSuchFieldError e6) {
            }
            try {
                iArr[CannelMicAction.TIME_END.ordinal()] = 7;
            } catch (NoSuchFieldError e7) {
            }
            $SWITCH_TABLE$cn$banshenggua$aichang$room$message$MicMessage$CannelMicAction = iArr;
        }
        return iArr;
    }

    static /* synthetic */ int[] $SWITCH_TABLE$cn$banshenggua$aichang$room$message$MicMessage$MicType() {
        int[] iArr = $SWITCH_TABLE$cn$banshenggua$aichang$room$message$MicMessage$MicType;
        if (iArr == null) {
            iArr = new int[MicType.values().length];
            try {
                iArr[MicType.AdjustMic.ordinal()] = 7;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[MicType.CancelMic.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[MicType.ChangeBanzou.ordinal()] = 8;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[MicType.ChangeMic.ordinal()] = 6;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[MicType.CloseMic.ordinal()] = 5;
            } catch (NoSuchFieldError e5) {
            }
            try {
                iArr[MicType.GetMicList.ordinal()] = 3;
            } catch (NoSuchFieldError e6) {
            }
            try {
                iArr[MicType.MultiCancelMic.ordinal()] = 16;
            } catch (NoSuchFieldError e7) {
            }
            try {
                iArr[MicType.MultiChangeMic.ordinal()] = 14;
            } catch (NoSuchFieldError e8) {
            }
            try {
                iArr[MicType.MultiCloseMic.ordinal()] = 12;
            } catch (NoSuchFieldError e9) {
            }
            try {
                iArr[MicType.MultiConfirmMic.ordinal()] = 13;
            } catch (NoSuchFieldError e10) {
            }
            try {
                iArr[MicType.MultiDelMic.ordinal()] = 10;
            } catch (NoSuchFieldError e11) {
            }
            try {
                iArr[MicType.MultiInviteMic.ordinal()] = 11;
            } catch (NoSuchFieldError e12) {
            }
            try {
                iArr[MicType.MultiOpenMic.ordinal()] = 15;
            } catch (NoSuchFieldError e13) {
            }
            try {
                iArr[MicType.MultiReqMic.ordinal()] = 9;
            } catch (NoSuchFieldError e14) {
            }
            try {
                iArr[MicType.OpenMic.ordinal()] = 4;
            } catch (NoSuchFieldError e15) {
            }
            try {
                iArr[MicType.ReqMic.ordinal()] = 1;
            } catch (NoSuchFieldError e16) {
            }
            $SWITCH_TABLE$cn$banshenggua$aichang$room$message$MicMessage$MicType = iArr;
        }
        return iArr;
    }

    public MicMessage(MicType micType, o oVar) {
        super(oVar);
        this.mType = micType;
        switch ($SWITCH_TABLE$cn$banshenggua$aichang$room$message$MicMessage$MicType()[micType.ordinal()]) {
            case 1:
                this.mKey = MessageKey.Message_ReqMic;
                return;
            case 2:
                this.mKey = MessageKey.Message_CancelMic;
                return;
            case 3:
                this.mKey = MessageKey.Message_GetMics;
                return;
            case 4:
                this.mKey = MessageKey.Message_OpenMic;
                return;
            case 5:
                this.mKey = MessageKey.Message_CloseMic;
                return;
            case 6:
                this.mKey = MessageKey.Message_ChangeMic;
                return;
            case 7:
                this.mKey = MessageKey.Message_AdjustMic;
                return;
            case 8:
                this.mKey = MessageKey.Message_ChangeBanzou;
                return;
            case 9:
                this.mKey = MessageKey.Message_MultiReqMic;
                return;
            case 10:
                this.mKey = MessageKey.Message_MultiDelMic;
                return;
            case 11:
                this.mKey = MessageKey.Message_MultiInviteMic;
                return;
            case 12:
                this.mKey = MessageKey.Message_MultiCloseMic;
                return;
            case 13:
                this.mKey = MessageKey.Message_MultiConfirmMic;
                return;
            case 14:
                this.mKey = MessageKey.Message_MultiChangeMic;
                return;
            case 15:
                this.mKey = MessageKey.Message_MultiOpenMic;
                return;
            case 16:
                this.mKey = MessageKey.Message_MultiCancelMic;
                return;
            default:
                return;
        }
    }

    public SocketMessage getSocketMessage() {
        SocketMessage socketMessage = super.getSocketMessage();
        switch ($SWITCH_TABLE$cn$banshenggua$aichang$room$message$MicMessage$MicType()[this.mType.ordinal()]) {
            case 1:
                if (!TextUtils.isEmpty(this.mMedia)) {
                    socketMessage.pushCommend("media", this.mMedia);
                }
                if (!TextUtils.isEmpty(this.mBzid)) {
                    socketMessage.pushCommend("bzid", this.mBzid);
                    socketMessage.pushCommend("cdata", this.mSongId);
                    if (!TextUtils.isEmpty(this.mSongName)) {
                        socketMessage.pushCommend(SelectCountryActivity.EXTRA_COUNTRY_NAME, this.mSongName);
                        socketMessage.pushCommend("singer", this.mSongArtist);
                        socketMessage.pushCommend("bztime", this.mSongDuration);
                        break;
                    }
                }
                break;
            case 2:
            case 16:
                if (!TextUtils.isEmpty(this.mUid)) {
                    socketMessage.pushCommend("touid", this.mUid);
                }
                if (!TextUtils.isEmpty(this.mMicId)) {
                    socketMessage.pushCommend(DeviceInfo.TAG_MID, this.mMicId);
                }
                if (this.mAction != null) {
                    switch ($SWITCH_TABLE$cn$banshenggua$aichang$room$message$MicMessage$CannelMicAction()[this.mAction.ordinal()]) {
                        case 2:
                            socketMessage.pushCommend("action", 1);
                            break;
                        case 3:
                            socketMessage.pushCommend("action", 8);
                            break;
                        case 4:
                            socketMessage.pushCommend("action", 3);
                            break;
                        case 5:
                            socketMessage.pushCommend("action", 9);
                            break;
                        case 6:
                            socketMessage.pushCommend("action", 2);
                            break;
                        case 7:
                            socketMessage.pushCommend("action", 7);
                            break;
                    }
                }
                break;
            case 7:
                if (!TextUtils.isEmpty(this.mUid)) {
                    socketMessage.pushCommend("touid", this.mUid);
                }
                if (!TextUtils.isEmpty(this.mMicId)) {
                    socketMessage.pushCommend(DeviceInfo.TAG_MID, this.mMicId);
                }
                if (this.mOrder <= 0) {
                    this.mOrder = 1;
                }
                socketMessage.pushCommend("order", this.mOrder);
                break;
            case 8:
                socketMessage.pushCommend("bzid", this.mBzid);
                if (!TextUtils.isEmpty(this.mMicId)) {
                    socketMessage.pushCommend(DeviceInfo.TAG_MID, this.mMicId);
                }
                if (!TextUtils.isEmpty(this.mSongName)) {
                    socketMessage.pushCommend(SelectCountryActivity.EXTRA_COUNTRY_NAME, this.mSongName);
                    socketMessage.pushCommend("singer", this.mSongArtist);
                    socketMessage.pushCommend("bztime", this.mSongDuration);
                    break;
                }
                break;
            case 10:
                if (!TextUtils.isEmpty(this.mUid)) {
                    socketMessage.pushCommend("destuid", this.mUid);
                }
                if (!TextUtils.isEmpty(this.mMicId)) {
                    socketMessage.pushCommend(DeviceInfo.TAG_MID, this.mMicId);
                    break;
                }
                break;
            case 11:
                if (!TextUtils.isEmpty(this.mMedia)) {
                    socketMessage.pushCommend("media", this.mMedia);
                }
                if (!TextUtils.isEmpty(this.mUid)) {
                    socketMessage.pushCommend("destuid", this.mUid);
                    break;
                }
                break;
            case 13:
                if (!TextUtils.isEmpty(this.mMedia)) {
                    socketMessage.pushCommend("media", this.mMedia);
                    break;
                }
                break;
        }
        return socketMessage;
    }

    public void parseOut(JSONObject jSONObject) {
        super.parseOut(jSONObject);
        if (this.mUsers != null) {
            this.mUsers = null;
        }
        if (jSONObject != null) {
            if (jSONObject.has("users")) {
                this.mUsers = new UserList();
                this.mUsers.parseUsers(jSONObject);
            }
            switch (jSONObject.optInt("action", 0)) {
                case 1:
                    this.mReason = SwitchMicReason.USER_CANCEL_MIC;
                    break;
                case 2:
                    this.mReason = SwitchMicReason.USER_CANCEL_WEAK_NET;
                    break;
                case 3:
                    this.mReason = SwitchMicReason.ADMIN_CANEL_MIC;
                    break;
                case 4:
                    this.mReason = SwitchMicReason.SERVER_CANCEL_NO_HB;
                    break;
                case 5:
                    this.mReason = SwitchMicReason.SERVER_CANCEL_HB_DIE;
                    break;
                case 6:
                    this.mReason = SwitchMicReason.SERVER_CANCEL_USERLEAVE;
                    break;
                case 7:
                    this.mReason = SwitchMicReason.SERVER_TIME_END;
                    break;
                case 8:
                    this.mReason = SwitchMicReason.USER_CANCEL_MIC_QUEUE;
                    break;
                case 9:
                    this.mReason = SwitchMicReason.ADMIN_CANCEL_MIC_QUEUE;
                    break;
                default:
                    this.mReason = SwitchMicReason.NO_USER_ON_MIC;
                    break;
            }
            if (jSONObject.has("user")) {
                this.mUser = new User();
                this.mUser.parseUser(jSONObject.optJSONObject("user"));
            }
            if (jSONObject.has("banzou")) {
                this.mBanzou = new Banzou();
                this.mBanzou.parseBanzou(jSONObject.optJSONObject("banzou"));
            }
            if (jSONObject.has("up")) {
                JSONObject optJSONObject = jSONObject.optJSONObject("up");
                if (optJSONObject.length() > 0) {
                    this.mUpUser = new User();
                    this.mUpUser.parseUser(optJSONObject);
                }
            }
            if (jSONObject.has("down")) {
                JSONObject optJSONObject2 = jSONObject.optJSONObject("down");
                if (optJSONObject2.length() > 0) {
                    this.mDownUser = new User();
                    this.mDownUser.parseUser(optJSONObject2);
                }
            }
            this.mMedia = jSONObject.optString("media", "");
            if (this.mMedia.equalsIgnoreCase("V")) {
                this.mMedia = "video";
            }
            if (jSONObject.has("mt")) {
                this.mMedia = jSONObject.optString("mt", "");
            }
            if (jSONObject.has("rtmp")) {
                this.mRtmp = new Rtmp();
                this.mRtmp.parseRtmp(jSONObject.optJSONObject("rtmp"));
            }
            this.mSongId = jSONObject.optString("cdata", "");
            this.mMsg = jSONObject.optString("message");
            this.mMicId = jSONObject.optString(DeviceInfo.TAG_MID, "");
        }
    }

    public String toString() {
        return "MicMessage type: " + this.mKey;
    }
}
