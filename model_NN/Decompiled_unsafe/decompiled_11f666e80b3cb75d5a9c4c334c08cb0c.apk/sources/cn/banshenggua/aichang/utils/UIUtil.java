package cn.banshenggua.aichang.utils;

import android.content.res.Resources;
import android.util.DisplayMetrics;
import android.view.WindowManager;
import cn.a.a.a;
import cn.banshenggua.aichang.app.KShareApplication;
import cn.banshenggua.aichang.sdk.ACException;

public class UIUtil {
    private static UIUtil instance = null;
    private boolean inited = false;
    private final DisplayMetrics mDisplayMetrics = new DisplayMetrics();

    private UIUtil() {
    }

    public static UIUtil getInstance() {
        if (instance == null) {
            instance = new UIUtil();
            instance.init();
        }
        return instance;
    }

    private void init() {
        WindowManager windowManager;
        if (!this.inited) {
            try {
                windowManager = (WindowManager) KShareApplication.getInstance().getApp().getSystemService("window");
            } catch (ACException e) {
                e.printStackTrace();
                windowManager = null;
            }
            if (windowManager != null) {
                windowManager.getDefaultDisplay().getMetrics(this.mDisplayMetrics);
                this.inited = true;
            }
        }
    }

    public DisplayMetrics getDisplayMetrics() {
        return this.mDisplayMetrics;
    }

    public String makeTimeString(long j) {
        int i = ((int) j) / 1000;
        System.out.println(i);
        return makeTimeString(i);
    }

    public String makeTimeString(int i) {
        StringBuffer stringBuffer = new StringBuffer();
        int i2 = i / 60;
        stringBuffer.append(i2 < 10 ? "0" : "").append(i2);
        stringBuffer.append(":");
        int i3 = i % 60;
        stringBuffer.append(i3 < 10 ? "0" : "").append(i3);
        return stringBuffer.toString();
    }

    public String toTime(long j) {
        long j2 = j / 1000;
        long j3 = j2 / 60;
        long j4 = j2 % 60;
        long j5 = j3 / 60;
        long j6 = j3 % 60;
        if (j5 > 0) {
            return String.format("%02d:%02d:%02d", Long.valueOf(j5), Long.valueOf(j6), Long.valueOf(j4));
        }
        return String.format("%02d:%02d", Long.valueOf(j6), Long.valueOf(j4));
    }

    public float getTextSizeAjustDensity(float f) {
        if (f <= 0.0f) {
            f = 15.0f;
        }
        return (float) (((double) f) * (((double) getDensity()) - 0.1d));
    }

    public float getDensity() {
        return this.mDisplayMetrics.density;
    }

    public int getmScreenWidth() {
        return this.mDisplayMetrics.widthPixels;
    }

    public int getmScreenHeight() {
        return this.mDisplayMetrics.heightPixels;
    }

    public int getDefaultImageWidth() {
        try {
            return KShareApplication.getInstance().getApp().getResources().getDimensionPixelSize(a.d.hot_item_width_image);
        } catch (Resources.NotFoundException e) {
            e.printStackTrace();
        } catch (ACException e2) {
            e2.printStackTrace();
        }
        return 160;
    }

    public int getDefaultImageHeight() {
        try {
            return KShareApplication.getInstance().getApp().getResources().getDimensionPixelSize(a.d.hot_item_height_image);
        } catch (Resources.NotFoundException e) {
            e.printStackTrace();
        } catch (ACException e2) {
            e2.printStackTrace();
        }
        return 160;
    }

    public int getDefaultUserHeight() {
        try {
            return KShareApplication.getInstance().getApp().getResources().getDimensionPixelSize(a.d.zone_user_height_image);
        } catch (Resources.NotFoundException e) {
            e.printStackTrace();
        } catch (ACException e2) {
            e2.printStackTrace();
        }
        return 160;
    }

    public int getDefaultUserWidth() {
        try {
            return KShareApplication.getInstance().getApp().getResources().getDimensionPixelSize(a.d.zone_user_width_image);
        } catch (Resources.NotFoundException e) {
            e.printStackTrace();
        } catch (ACException e2) {
            e2.printStackTrace();
        }
        return 160;
    }
}
