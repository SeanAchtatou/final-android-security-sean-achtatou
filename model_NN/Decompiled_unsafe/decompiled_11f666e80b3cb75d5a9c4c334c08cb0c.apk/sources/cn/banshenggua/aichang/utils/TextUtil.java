package cn.banshenggua.aichang.utils;

import java.io.UnsupportedEncodingException;

public class TextUtil {
    public static String editAbleString = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789~!@#%'&*?()-_:;/<>+=÷^'[]\\|\"$￥{},.";

    public static String getUTF8String(String str) {
        try {
            return new String(str.getBytes("UTF-8"), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return "";
        }
    }
}
