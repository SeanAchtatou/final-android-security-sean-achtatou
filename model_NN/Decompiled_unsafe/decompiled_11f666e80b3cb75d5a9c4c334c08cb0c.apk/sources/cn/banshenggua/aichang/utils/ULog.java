package cn.banshenggua.aichang.utils;

import android.util.Log;

public class ULog {
    public static boolean debug = false;
    public static boolean showdebug = false;

    public static void ds(String str, String str2) {
        if (debug) {
            Log.e(str, str2);
            Log.e(str, Log.getStackTraceString(new Throwable()));
        }
    }

    public static void d(String str, String str2) {
        if (debug) {
            Log.e(str, str2);
            processLog(str, str2);
        }
    }

    public static void e(String str, String str2) {
        if (debug) {
            Log.e(str, str2);
            processLog(str, str2);
        }
    }

    public static void e(String str, String str2, Throwable th) {
        Log.e(str, str2, th);
    }

    public static void i(String str, String str2) {
        if (debug) {
            Log.i(str, str2);
            processLog(str, str2);
        }
    }

    public static void d(String str, String str2, Throwable th) {
        if (debug) {
            Log.d(str, str2, th);
            processLog(str, str2);
        }
    }

    public static void processLog(String str, String str2) {
    }
}
