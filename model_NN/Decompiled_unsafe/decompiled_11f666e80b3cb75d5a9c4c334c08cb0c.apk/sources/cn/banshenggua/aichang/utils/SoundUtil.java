package cn.banshenggua.aichang.utils;

import android.content.Context;
import android.media.SoundPool;
import android.text.TextUtils;
import java.io.File;
import java.util.HashMap;

public class SoundUtil {
    private static final String TAG = "SoundUtil";
    /* access modifiers changed from: private */
    public HashMap<String, String> mSoundHitPerMap;
    /* access modifiers changed from: private */
    public HashMap<String, SoundHitObject> mSoundHitsMap;
    /* access modifiers changed from: private */
    public SoundPool mSoundPool = null;
    /* access modifiers changed from: private */
    public HashMap<String, String> mWillPlaySound;

    private class SoundHitObject {
        private boolean loaded = false;
        private int soundid;

        public SoundHitObject(int i) {
            this.soundid = i;
        }

        public int getSoundId() {
            return this.soundid;
        }

        public boolean isloadedOk() {
            return this.loaded;
        }

        public void setloaded(boolean z) {
            this.loaded = z;
        }
    }

    public SoundUtil(Context context) {
        initSound(context);
    }

    private void initSound(Context context) {
        if (this.mSoundPool != null) {
            this.mSoundPool.release();
            this.mSoundPool = null;
            this.mSoundHitsMap.clear();
            this.mSoundHitsMap = null;
            this.mSoundHitPerMap.clear();
            this.mSoundHitPerMap = null;
        }
        this.mSoundHitPerMap = new HashMap<>();
        this.mSoundHitsMap = new HashMap<>();
        this.mSoundPool = new SoundPool(5, 3, 0);
        if (this.mSoundPool != null) {
            this.mSoundPool.setOnLoadCompleteListener(new SoundPool.OnLoadCompleteListener() {
                public void onLoadComplete(SoundPool soundPool, int i, int i2) {
                    if (SoundUtil.this.mSoundHitPerMap != null) {
                        String str = (String) SoundUtil.this.mSoundHitPerMap.get(SoundUtil.this.getSoundIdStr(i));
                        ULog.d(SoundUtil.TAG, "onLoadComplete: " + str + "; id: " + i + "; " + i2);
                        if (!TextUtils.isEmpty(str) && SoundUtil.this.mSoundHitsMap != null) {
                            if (i2 == 0) {
                                SoundHitObject soundHitObject = (SoundHitObject) SoundUtil.this.mSoundHitsMap.get(str);
                                if (soundHitObject != null) {
                                    soundHitObject.setloaded(true);
                                    SoundUtil.this.mSoundHitsMap.put(str, soundHitObject);
                                    if (SoundUtil.this.mWillPlaySound != null && SoundUtil.this.mWillPlaySound.containsKey(SoundUtil.this.getSoundIdStr(i))) {
                                        SoundUtil.this.mSoundPool.play(i, 0.5f, 0.5f, 0, 0, 1.0f);
                                        SoundUtil.this.mWillPlaySound.remove(SoundUtil.this.getSoundIdStr(i));
                                        return;
                                    }
                                    return;
                                }
                                return;
                            }
                            SoundUtil.this.mSoundHitsMap.remove(str);
                        }
                    }
                }
            });
        }
    }

    public String getSoundIdStr(int i) {
        return new StringBuilder().append(i).toString();
    }

    public void playSound(Context context, String str) {
        File file;
        if (this.mSoundPool != null && this.mSoundHitsMap != null && this.mSoundHitPerMap != null && (file = new File(str)) != null && file.exists()) {
            String md5sum = MD5.md5sum(str);
            SoundHitObject soundHitObject = this.mSoundHitsMap.get(md5sum);
            ULog.d(TAG, "file: " + file);
            if (soundHitObject != null && soundHitObject.isloadedOk()) {
                this.mSoundPool.play(soundHitObject.getSoundId(), 0.5f, 0.5f, 0, 0, 1.0f);
            } else if (soundHitObject == null) {
                if (this.mWillPlaySound == null) {
                    this.mWillPlaySound = new HashMap<>();
                }
                int load = this.mSoundPool.load(str, 1);
                this.mSoundHitsMap.put(md5sum, new SoundHitObject(load));
                this.mSoundHitPerMap.put(getSoundIdStr(load), md5sum);
                this.mWillPlaySound.put(getSoundIdStr(load), getSoundIdStr(load));
            }
        }
    }

    public void clean() {
        if (this.mSoundPool != null) {
            this.mSoundPool.release();
            this.mSoundPool = null;
        }
    }
}
