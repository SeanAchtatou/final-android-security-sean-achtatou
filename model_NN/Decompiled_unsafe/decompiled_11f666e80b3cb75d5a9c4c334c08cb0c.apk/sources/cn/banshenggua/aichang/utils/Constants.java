package cn.banshenggua.aichang.utils;

import android.support.v4.view.PointerIconCompat;

public class Constants {
    public static final String ACCOUNT = "account";
    public static final String ACTION_LOGIN = "LOGIN";
    public static final int ADD_BEFORE = 0;
    public static final int ADD_END = -1;
    public static String BALANCE = "balance";
    public static int CHARGE_REQUES_CODE = PointerIconCompat.TYPE_ALIAS;
    public static final int CLEARIMGED = 1000;
    public static final int CLEARIMGING = 999;
    public static final int CLEAR_ALL = -2;
    public static String CLUB = "club";
    public static final String CurrentAccount = "currentAccount";
    public static final String INPUT_WEIBO = "input_weibo";
    public static final String ITEM = "item";
    public static final String ImageDetail = "image_detail";
    public static final String ImageIndex = "image_index";
    public static final int LOGIN = 100;
    public static final int LOGOUT = 7;
    public static final String PLAYMUSIC = "play_music";
    public static final int REGISTER_OK = 102;
    public static final int RESPONSE_ERROR = 4;
    public static final int RESPONSE_LOCAL = 6;
    public static final int RESPONSE_NODATA = 3;
    public static final int RESPONSE_OK = 1;
    public static final int RESPONSE_UPDATE = 2;
    public static final int RESULT_OK = 101;
    public static final int RETURN_ERROR = 5;
    public static final String ROOM = "room";
    public static final String ROOM_LIST = "room_list";
    public static final String SINGER_LIST = "singer_list";
    public static final String SMS_NEW_COUNT = "new_count";
    public static final String ServiceNotification = "play_music_service";
    public static final String ServiceNotificationBufferTime = "play_music_buffertime";
    public static final String ServiceNotificationTime = "play_music_time";
    public static final String ServiceNotificationTotalTime = "play_music_totaltime";
    public static final String SongObject = "song";
    public static final String TALK = "talk";
    public static final int TYPE_FARMILY_APPLY_FOR = 3;
    public static final int TYPE_FARMILY_APPLY_FOR_ING = 5;
    public static final int TYPE_FARMILY_DONOT_KNOW = 6;
    public static final int TYPE_FARMILY_EXIT = 4;
    public static final int TYPE_FARMILY_INFO = 8;
    public static final int TYPE_FARMILY_MANAGER = 1;
    public static final int TYPE_FARMILY_MEMBER = 7;
    public static final int TYPE_FARMILY_MEMBER_APPLY_FOR = 2;
    public static final int TYPE_FREE_GIFT_NO_SHOW = 10;
    public static final int TYPE_FREE_GIFT_SHOW = 9;
    public static final int TYPE_REPORT = 0;
    public static final int TYPE_ROOM_EDIT = 14;
    public static final int TYPE_ROOM_INFO = 13;
    public static final int TYPE_VIDEO_APPLY_FOR = 15;
    public static final int TYPE_VOICE_GIFT_NO_SHOW = 12;
    public static final int TYPE_VOICE_GIFT_SHOW = 11;
    public static final String ThirdName = "thirdname";
    public static final String Title = "title";
    public static final String UID = "UID";
    public static final String USER_LIST = "user_list";
    public static final String WEIBO = "weibo";
    public static final String WEIBO_LIST = "weibo_list";
    public static final String account_key = "ACCOUNT_MAIN";
    public static final String albums = "albums";
    public static final String callBackAccount = "callBackAccount";
    public static final String lookAround = "lookAround";
    public static final String result = "RESULT_OK";
    public static final int textWatch_result = 500;
    public static final int textWatch_update = 501;
}
