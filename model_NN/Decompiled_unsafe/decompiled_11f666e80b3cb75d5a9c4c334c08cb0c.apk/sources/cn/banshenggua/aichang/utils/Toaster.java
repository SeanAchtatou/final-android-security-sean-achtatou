package cn.banshenggua.aichang.utils;

import android.app.Activity;
import android.app.Application;
import android.text.TextUtils;
import android.view.View;
import android.widget.Toast;
import cn.banshenggua.aichang.app.KShareApplication;
import cn.banshenggua.aichang.sdk.ACException;
import java.text.MessageFormat;

public class Toaster {
    private static final int LONG_DELAY = 3500;
    private static final int SHORT_DELAY = 2000;
    private static final int SHORT_SHORT_DELAY = 1000;
    static Toast centerTotast = null;
    static Toast normalToast = null;

    private static void show(Activity activity, final int i, final int i2) {
        if (activity != null) {
            final Application application = activity.getApplication();
            activity.runOnUiThread(new Runnable() {
                public void run() {
                    Toast.makeText(application, i, i2).show();
                }
            });
        }
    }

    public static void showShortToast(int i) {
        try {
            Toast.makeText(KShareApplication.getInstance().getApp(), i, 1000).show();
        } catch (ACException e) {
            e.printStackTrace();
        }
    }

    public static void showShortToast(String str) {
        try {
            Toast.makeText(KShareApplication.getInstance().getApp(), str, 1000).show();
        } catch (ACException e) {
            e.printStackTrace();
        }
    }

    public static void showLongToast(String str) {
        try {
            Toast.makeText(KShareApplication.getInstance().getApp(), str, (int) LONG_DELAY).show();
        } catch (ACException e) {
            e.printStackTrace();
        }
    }

    public static void showLongToast(int i) {
        try {
            Toast.makeText(KShareApplication.getInstance().getApp(), i, (int) LONG_DELAY).show();
        } catch (ACException e) {
            e.printStackTrace();
        }
    }

    private static void showLayout(Activity activity, final int i, final View view) {
        if (activity != null) {
            final Application application = activity.getApplication();
            activity.runOnUiThread(new Runnable() {
                public void run() {
                    Toast toast = new Toast(application);
                    toast.setGravity(53, 12, 40);
                    toast.setDuration(i);
                    toast.setView(view);
                    toast.show();
                }
            });
        }
    }

    private static void show(Activity activity, final String str, final int i) {
        if (activity != null && !TextUtils.isEmpty(str)) {
            final Application application = activity.getApplication();
            activity.runOnUiThread(new Runnable() {
                public void run() {
                    if (Toaster.normalToast != null) {
                        Toaster.normalToast.cancel();
                    }
                    Toaster.normalToast = Toast.makeText(application, str, i);
                    Toaster.normalToast.show();
                }
            });
        }
    }

    public static void cancelToast() {
        if (centerTotast != null) {
            centerTotast.cancel();
        }
        if (normalToast != null) {
            normalToast.cancel();
        }
    }

    private static void showAtCenter(Activity activity, final String str, final int i) {
        if (activity != null && !TextUtils.isEmpty(str)) {
            final Application application = activity.getApplication();
            activity.runOnUiThread(new Runnable() {
                public void run() {
                    if (Toaster.centerTotast != null) {
                        Toaster.centerTotast.cancel();
                    }
                    Toaster.centerTotast = Toast.makeText(application, str, i);
                    Toaster.centerTotast.setGravity(17, 0, 0);
                    Toaster.centerTotast.show();
                }
            });
        }
    }

    public static void showLong(Activity activity, int i) {
        show(activity, i, 1);
    }

    public static void showLongAtCenter(Activity activity, String str) {
        showAtCenter(activity, str, 1);
    }

    public static void showShortAtCenter(Activity activity, String str) {
        showAtCenter(activity, str, 0);
    }

    public static void showLayoutLong(Activity activity, View view) {
        showLayout(activity, 1, view);
    }

    public static void showLayoutShort(Activity activity, View view) {
        showLayout(activity, 0, view);
    }

    public static void showShort(Activity activity, int i) {
        show(activity, i, 0);
    }

    public static void showLong(Activity activity, String str) {
        show(activity, str, 1);
    }

    public static void showShort(Activity activity, String str) {
        show(activity, str, 0);
    }

    public static void showLong(Activity activity, String str, Object... objArr) {
        show(activity, MessageFormat.format(str, objArr), 1);
    }

    public static void showShort(Activity activity, String str, Object... objArr) {
        show(activity, MessageFormat.format(str, objArr), 0);
    }

    public static void showLong(Activity activity, int i, Object... objArr) {
        if (activity != null) {
            showLong(activity, activity.getString(i), objArr);
        }
    }

    public static void showShort(Activity activity, int i, Object... objArr) {
        if (activity != null) {
            showShort(activity, activity.getString(i), objArr);
        }
    }
}
