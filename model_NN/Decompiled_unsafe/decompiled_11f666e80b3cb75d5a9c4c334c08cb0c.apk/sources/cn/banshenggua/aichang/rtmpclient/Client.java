package cn.banshenggua.aichang.rtmpclient;

import cn.banshenggua.aichang.utils.ULog;
import java.util.ArrayList;
import java.util.List;

public class Client {
    public int cid;
    List<Channel> mChannels = new ArrayList();
    public String[] mParams = null;
    List<NetStream> mStreams = new ArrayList();
    public String mUrl = "";
    private RtmpClientManager manager;
    private RtmpClientManager2 manager2;

    public void setCid(int i) {
        this.cid = i;
    }

    public NetStream findStream(int i) {
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 >= this.mStreams.size()) {
                return null;
            }
            NetStream netStream = this.mStreams.get(i3);
            if (netStream != null && netStream.sid == i) {
                return netStream;
            }
            i2 = i3 + 1;
        }
    }

    public void removeStream(int i) {
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 < this.mStreams.size()) {
                NetStream netStream = this.mStreams.get(i3);
                if (netStream == null || netStream.sid != i) {
                    i2 = i3 + 1;
                } else {
                    this.mStreams.remove(i3);
                    return;
                }
            } else {
                return;
            }
        }
    }

    public void stop() {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= this.mStreams.size()) {
                this.mStreams.clear();
                return;
            }
            NetStream netStream = this.mStreams.get(i2);
            ULog.d("luoleixxxxxxxxx", "stop stream: " + i2);
            if (netStream != null) {
                netStream.stopChannel();
            }
            i = i2 + 1;
        }
    }

    public Client(String str, String[] strArr) {
        this.mUrl = str;
        this.mParams = strArr;
    }

    public Client(String str, String[] strArr, int i) {
        this.mUrl = str;
        this.mParams = strArr;
        this.cid = i;
    }

    public void checkChannels() {
        if (this.mChannels == null) {
            this.mChannels = new ArrayList();
        }
    }

    public boolean checkChannels(Channel channel) {
        checkChannels();
        if (channel == null) {
            return false;
        }
        for (int i = 0; i < this.mChannels.size(); i++) {
            if (channel.equal(this.mChannels.get(i))) {
                return false;
            }
        }
        return true;
    }

    public void checkNetStreams() {
        if (this.mStreams == null) {
            this.mStreams = new ArrayList();
        }
    }

    public boolean checkNetStreams(NetStream netStream) {
        checkNetStreams();
        if (netStream == null) {
            return false;
        }
        for (int i = 0; i < this.mStreams.size(); i++) {
            if (netStream.equals(this.mStreams.get(i))) {
                return false;
            }
        }
        return true;
    }

    public void addChannel(Channel channel) {
        if (checkChannels(channel)) {
            this.mChannels.add(channel);
        }
    }

    public void addStream(NetStream netStream) {
        if (checkNetStreams(netStream)) {
            this.mStreams.add(netStream);
        }
    }

    public void updateManager(RtmpClientManager rtmpClientManager) {
        checkChannels();
        this.manager = rtmpClientManager;
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < this.mChannels.size()) {
                this.mChannels.get(i2).updateManager(rtmpClientManager);
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    public void updateManager(RtmpClientManager2 rtmpClientManager2) {
        checkNetStreams();
        this.manager2 = rtmpClientManager2;
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < this.mStreams.size()) {
                this.mStreams.get(i2).updateManager(rtmpClientManager2);
                i = i2 + 1;
            } else {
                return;
            }
        }
    }
}
