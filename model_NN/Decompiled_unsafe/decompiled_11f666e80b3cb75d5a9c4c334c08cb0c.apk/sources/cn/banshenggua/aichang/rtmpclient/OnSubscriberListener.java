package cn.banshenggua.aichang.rtmpclient;

public interface OnSubscriberListener {
    void OnError(int i);

    void OnFinish();

    void OnLoading();

    void OnPlaying();
}
