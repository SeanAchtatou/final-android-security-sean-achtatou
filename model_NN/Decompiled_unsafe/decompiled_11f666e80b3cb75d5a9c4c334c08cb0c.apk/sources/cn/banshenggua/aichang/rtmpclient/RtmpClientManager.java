package cn.banshenggua.aichang.rtmpclient;

import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class RtmpClientManager implements Runnable {
    List<Client> mClients = new ArrayList();
    private Handler mHandler = new Handler() {
        private static /* synthetic */ int[] $SWITCH_TABLE$cn$banshenggua$aichang$rtmpclient$RtmpClientManager$MessageType;

        static /* synthetic */ int[] $SWITCH_TABLE$cn$banshenggua$aichang$rtmpclient$RtmpClientManager$MessageType() {
            int[] iArr = $SWITCH_TABLE$cn$banshenggua$aichang$rtmpclient$RtmpClientManager$MessageType;
            if (iArr == null) {
                iArr = new int[MessageType.values().length];
                try {
                    iArr[MessageType.NoSurpport.ordinal()] = 2;
                } catch (NoSuchFieldError e) {
                }
                try {
                    iArr[MessageType.OnStreamCreate.ordinal()] = 1;
                } catch (NoSuchFieldError e2) {
                }
                $SWITCH_TABLE$cn$banshenggua$aichang$rtmpclient$RtmpClientManager$MessageType = iArr;
            }
            return iArr;
        }

        public void handleMessage(Message message) {
            super.handleMessage(message);
            if (message.obj != null && (message.obj instanceof RtmpClientMessage)) {
                switch ($SWITCH_TABLE$cn$banshenggua$aichang$rtmpclient$RtmpClientManager$MessageType()[MessageType.getMessageType(((RtmpClientMessage) message.obj).msg).ordinal()]) {
                    case 1:
                        for (int i = 0; i < RtmpClientManager.this.mClients.size(); i++) {
                        }
                        return;
                    default:
                        return;
                }
            }
        }
    };
    private boolean mStop = false;
    Thread mThread = null;

    private native void getmessage();

    private native void init(String str);

    private native void start(String str);

    public native byte[] getdata(int i);

    public native int pulldata(int i, byte[] bArr, int i2);

    static {
        System.loadLibrary("rtmpclient_shared");
    }

    public void addClient(Client client) {
        if (client != null) {
            client.updateManager(this);
            this.mClients.add(client);
        }
    }

    public enum MessageType {
        OnStreamCreate("stream_create"),
        NoSurpport("_____no_surpport");
        
        private String key = "";

        private MessageType(String str) {
            this.key = str;
        }

        public String getKey() {
            return this.key;
        }

        public static MessageType getMessageType(String str) {
            if (!TextUtils.isEmpty(str)) {
                for (MessageType messageType : values()) {
                    if (str.equalsIgnoreCase(messageType.getKey())) {
                        return messageType;
                    }
                }
            }
            return NoSurpport;
        }
    }

    private class RtmpClientMessage {
        public HashMap<String, String> extendObj;
        public String msg;

        private RtmpClientMessage() {
        }
    }

    public void initManager() {
        init("");
    }

    public void startManager() {
        this.mStop = false;
        this.mThread = new Thread(this);
        this.mThread.setPriority(10);
        this.mThread.start();
    }

    public void run() {
        Log.d("luolei", "run start");
        init("");
        Log.d("luolei", "init end");
        while (!this.mStop) {
            try {
                Thread.sleep(50);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
