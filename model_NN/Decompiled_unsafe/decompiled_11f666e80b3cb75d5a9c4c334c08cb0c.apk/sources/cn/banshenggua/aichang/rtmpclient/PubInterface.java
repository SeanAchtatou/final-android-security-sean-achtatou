package cn.banshenggua.aichang.rtmpclient;

public interface PubInterface {
    boolean isConnected();

    void pushdata(byte[] bArr, int i);

    void pushdata(byte[] bArr, int i, long j, boolean z);

    void start();

    void stop();

    void updateVideoSize(VideoSize videoSize, VideoSize videoSize2, int i, int i2);
}
