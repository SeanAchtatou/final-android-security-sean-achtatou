package cn.banshenggua.aichang.rtmpclient;

public class VideoSize {
    public int height;
    public int width;

    public VideoSize() {
        this.width = 0;
        this.height = 0;
    }

    public VideoSize(int i, int i2) {
        this.width = i;
        this.height = i2;
    }

    public String toString() {
        return "size: " + this.width + "x" + this.height;
    }

    public int getWidth() {
        return this.width;
    }

    public void setWidth(int i) {
        this.width = i;
    }

    public int getHeight() {
        return this.height;
    }

    public void setHeight(int i) {
        this.height = i;
    }
}
