package cn.banshenggua.aichang.ui;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.ProgressBar;
import cn.a.a.a;
import cn.banshenggua.aichang.entry.BaseFragment;
import cn.banshenggua.aichang.utils.UIUtil;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

public class DownloadFragment extends BaseFragment {
    public static final int DONWLOAD_START = 3000;
    public static final int DOWNLOAD_COMP = 3002;
    public static final int DOWNLOAD_PROCESS = 3001;
    private static final float IMAGE_HEIGHT = 653.0f;
    private static final float IMAGE_WIDTH = 1242.0f;
    /* access modifiers changed from: private */
    public static final int[] PHOTOS = {a.e.des_1, a.e.des_2, a.e.des_3, a.e.des_4};
    public static final int PLAY_PHOTO = 4001;
    private int[] aniIDs = {a.C0007a.playmusic_alpha_in, a.C0007a.playmusic_left_in, a.C0007a.push_bottom_in, a.C0007a.playmusic_slide_left_in};
    /* access modifiers changed from: private */
    public Handler mHandler = new Handler() {
        public void handleMessage(Message message) {
            switch (message.what) {
                case DownloadFragment.PLAY_PHOTO /*4001*/:
                    DownloadFragment.this.playPhoto(message.arg1);
                    return;
                default:
                    return;
            }
        }
    };
    ImageView mImageView = null;
    /* access modifiers changed from: private */
    public int mPhotoIndex = 0;
    ProgressBar mProgress = null;
    private Timer mTimer = null;
    private Random random = new Random();

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ViewGroup viewGroup2 = (ViewGroup) layoutInflater.inflate(a.g.download_fragment, (ViewGroup) null);
        this.mProgress = (ProgressBar) viewGroup2.findViewById(a.f.progressbar_updown);
        this.mImageView = (ImageView) viewGroup2.findViewById(a.f.download_des_img);
        ViewGroup.LayoutParams layoutParams = this.mImageView.getLayoutParams();
        layoutParams.width = UIUtil.getInstance().getmScreenWidth();
        layoutParams.height = (int) (((float) UIUtil.getInstance().getmScreenWidth()) * 0.5257649f);
        this.mImageView.setLayoutParams(layoutParams);
        return viewGroup2;
    }

    public void onDestroy() {
        super.onDestroy();
    }

    public void onStart() {
        super.onStart();
        startTimer();
    }

    public void onStop() {
        cancelTimer();
        super.onStop();
    }

    public void updateProgress(int i) {
        if (this.mProgress != null) {
            this.mProgress.setProgress(i);
        }
    }

    /* access modifiers changed from: private */
    public void playPhoto(int i) {
        if (i >= 0 && i < PHOTOS.length) {
            int i2 = PHOTOS[i];
            if (this.mImageView != null) {
                this.mImageView.setImageResource(i2);
                this.mImageView.startAnimation(AnimationUtils.loadAnimation(this.mImageView.getContext(), a.C0007a.playmusic_alpha_in));
            }
        }
    }

    private void startTimer() {
        if (this.mTimer == null) {
            this.mTimer = new Timer();
            this.mTimer.schedule(new TimerTask() {
                public void run() {
                    try {
                        if (DownloadFragment.this.mPhotoIndex >= DownloadFragment.PHOTOS.length) {
                            DownloadFragment.this.mPhotoIndex = 0;
                        }
                        Message obtainMessage = DownloadFragment.this.mHandler.obtainMessage();
                        obtainMessage.what = DownloadFragment.PLAY_PHOTO;
                        obtainMessage.arg1 = DownloadFragment.this.mPhotoIndex;
                        DownloadFragment.this.mHandler.sendMessage(obtainMessage);
                        DownloadFragment downloadFragment = DownloadFragment.this;
                        downloadFragment.mPhotoIndex = downloadFragment.mPhotoIndex + 1;
                    } catch (Exception e) {
                    }
                }
            }, 0, 5000);
        }
    }

    public void cancelTimer() {
        if (this.mTimer != null) {
            this.mTimer.cancel();
            this.mTimer = null;
        }
    }
}
