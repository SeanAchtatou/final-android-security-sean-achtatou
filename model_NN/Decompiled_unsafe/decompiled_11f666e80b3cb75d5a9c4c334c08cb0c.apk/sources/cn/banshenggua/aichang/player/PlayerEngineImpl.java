package cn.banshenggua.aichang.player;

import android.annotation.SuppressLint;
import android.media.MediaPlayer;
import android.os.Handler;
import android.text.TextUtils;
import android.view.SurfaceHolder;
import cn.banshenggua.aichang.player.Playlist;
import cn.banshenggua.aichang.utils.ULog;
import com.pocketmusic.kshare.requestobjs.Song;
import com.pocketmusic.kshare.requestobjs.v;
import java.io.File;
import java.io.IOException;

public class PlayerEngineImpl implements PlayerEngine {
    private static final int ACCEPTABLE_FAIL_NUMBER = 2;
    private static final long FAIL_TIME_FRAME = 1000;
    protected static final String TAG = "PlayerEngineImpl";
    private static final long TIME_UPDATE = 500;
    /* access modifiers changed from: private */
    public int duration;
    /* access modifiers changed from: private */
    public InternalMediaPlayer mCurrentMediaPlayer;
    /* access modifiers changed from: private */
    public Handler mHandler = new Handler();
    /* access modifiers changed from: private */
    public long mLastFailTime = 0;
    /* access modifiers changed from: private */
    public PlayerEngineListener mPlayerEngineListener;
    /* access modifiers changed from: private */
    public Playlist mPlaylist = null;
    private SurfaceHolder mSurfaceHolder;
    /* access modifiers changed from: private */
    public long mTimesFailed = 0;
    /* access modifiers changed from: private */
    public Runnable mUpdateTimeTask = new Runnable() {
        public void run() {
            try {
                if (PlayerEngineImpl.this.mPlayerEngineListener != null) {
                    if (PlayerEngineImpl.this.mCurrentMediaPlayer != null) {
                        PlayerEngineImpl.this.mPlayerEngineListener.onTrackProgress(PlayerEngineImpl.this.mCurrentMediaPlayer.getCurrentPosition());
                    }
                    PlayerEngineImpl.this.mHandler.postDelayed(this, PlayerEngineImpl.TIME_UPDATE);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };
    private Playlist prevPlaylist = null;

    private class InternalMediaPlayer extends MediaPlayer {
        public boolean playAfterPrepare;
        public String playPath;
        public boolean preparing;

        private InternalMediaPlayer() {
            this.preparing = false;
            this.playAfterPrepare = false;
        }

        /* synthetic */ InternalMediaPlayer(PlayerEngineImpl playerEngineImpl, InternalMediaPlayer internalMediaPlayer) {
            this();
        }
    }

    public void next() {
        if (this.mPlaylist != null) {
            this.mPlaylist.selectNext();
            play();
            ULog.d(TAG, "play next");
        }
    }

    public void openPlaylist(Playlist playlist) {
        if (!playlist.isEmpty()) {
            this.prevPlaylist = this.mPlaylist;
            this.mPlaylist = playlist;
            return;
        }
        this.mPlaylist = null;
    }

    public void pause() {
        try {
            this.mHandler.postDelayed(new Runnable() {
                public void run() {
                    try {
                        if (PlayerEngineImpl.this.mCurrentMediaPlayer == null) {
                            return;
                        }
                        if (PlayerEngineImpl.this.mCurrentMediaPlayer.preparing) {
                            PlayerEngineImpl.this.mCurrentMediaPlayer.playAfterPrepare = false;
                        } else if (PlayerEngineImpl.this.mCurrentMediaPlayer.isPlaying()) {
                            PlayerEngineImpl.this.mCurrentMediaPlayer.pause();
                            if (PlayerEngineImpl.this.mPlayerEngineListener != null) {
                                PlayerEngineImpl.this.mPlayerEngineListener.onTrackPause();
                            }
                        }
                    } catch (Exception e) {
                    }
                }
            }, 100);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean isPausing() {
        String playPath;
        try {
            if (this.mCurrentMediaPlayer != null && !this.mCurrentMediaPlayer.preparing && (playPath = getPlayPath(this.mPlaylist.getSelectedTrack())) != null && playPath.equals(this.mCurrentMediaPlayer.playPath) && !this.mCurrentMediaPlayer.isPlaying()) {
                return true;
            }
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @SuppressLint({"NewApi", "NewApi", "NewApi", "NewApi", "NewApi", "NewApi", "NewApi"})
    public void play() {
        try {
            this.mHandler.postDelayed(new Runnable() {
                public void run() {
                    ULog.d(PlayerEngineImpl.TAG, "call play start");
                    if (PlayerEngineImpl.this.mPlaylist != null) {
                        String access$4 = PlayerEngineImpl.this.getPlayPath(PlayerEngineImpl.this.mPlaylist.getSelectedTrack());
                        ULog.d(PlayerEngineImpl.TAG, "player path: " + access$4);
                        if (PlayerEngineImpl.this.mPlayerEngineListener.onTrackStart() && !TextUtils.isEmpty(access$4) && PlayerEngineImpl.this.mPlaylist != null) {
                            if (PlayerEngineImpl.this.mCurrentMediaPlayer == null) {
                                PlayerEngineImpl.this.mCurrentMediaPlayer = PlayerEngineImpl.this.build(access$4);
                            }
                            if (!(PlayerEngineImpl.this.mCurrentMediaPlayer == null || access$4 == null || access$4.equals(PlayerEngineImpl.this.mCurrentMediaPlayer.playPath))) {
                                PlayerEngineImpl.this.cleanUp();
                                PlayerEngineImpl.this.mCurrentMediaPlayer = PlayerEngineImpl.this.build(access$4);
                            }
                            if (PlayerEngineImpl.this.mCurrentMediaPlayer == null) {
                                return;
                            }
                            if (PlayerEngineImpl.this.mCurrentMediaPlayer.preparing) {
                                PlayerEngineImpl.this.mCurrentMediaPlayer.playAfterPrepare = true;
                            } else if (!PlayerEngineImpl.this.mCurrentMediaPlayer.isPlaying()) {
                                PlayerEngineImpl.this.mHandler.removeCallbacks(PlayerEngineImpl.this.mUpdateTimeTask);
                                PlayerEngineImpl.this.mHandler.postDelayed(PlayerEngineImpl.this.mUpdateTimeTask, PlayerEngineImpl.TIME_UPDATE);
                                PlayerEngineImpl.this.mCurrentMediaPlayer.start();
                            }
                        }
                    }
                }
            }, 100);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void prev() {
        if (this.mPlaylist != null) {
            this.mPlaylist.selectPrev();
            play();
        }
    }

    public void skipTo(int i) {
        this.mPlaylist.select(i);
        play();
    }

    public void stop() {
        cleanUp();
        if (this.mPlayerEngineListener != null) {
            this.mPlayerEngineListener.onTrackStop();
        }
        ULog.d(TAG, "play stop");
    }

    /* access modifiers changed from: private */
    @SuppressLint({"NewApi", "NewApi"})
    public void cleanUp() {
        if (this.mCurrentMediaPlayer != null) {
            try {
                this.mCurrentMediaPlayer.stop();
            } catch (IllegalStateException e) {
            } finally {
                this.mCurrentMediaPlayer.reset();
                this.mCurrentMediaPlayer.release();
                this.mCurrentMediaPlayer = null;
            }
        }
    }

    /* access modifiers changed from: private */
    public String getPlayPath(v vVar) {
        if (vVar == null) {
            return null;
        }
        Song h = vVar.h();
        if (TextUtils.isEmpty(null) || !new File((String) null).exists()) {
            return h.c;
        }
        return null;
    }

    /* access modifiers changed from: private */
    public InternalMediaPlayer build(String str) {
        this.duration = 0;
        final InternalMediaPlayer internalMediaPlayer = new InternalMediaPlayer(this, null);
        if (str == null || str.length() == 0) {
            if (this.mPlayerEngineListener != null) {
                this.mPlayerEngineListener.onTrackStreamError();
            }
            stop();
            return null;
        }
        try {
            internalMediaPlayer.setDataSource(str);
            internalMediaPlayer.playPath = str;
            internalMediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                public void onCompletion(MediaPlayer mediaPlayer) {
                    PlayerEngineImpl.this.mPlayerEngineListener.onTrackStop();
                }
            });
            internalMediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                public void onPrepared(MediaPlayer mediaPlayer) {
                    internalMediaPlayer.preparing = false;
                    ULog.d(PlayerEngineImpl.TAG, "onPrepared");
                    if (internalMediaPlayer.playAfterPrepare) {
                        internalMediaPlayer.playAfterPrepare = false;
                        PlayerEngineImpl.this.duration = mediaPlayer.getDuration();
                        PlayerEngineImpl.this.mPlayerEngineListener.onTrackPlay();
                        PlayerEngineImpl.this.play();
                    }
                }
            });
            internalMediaPlayer.setOnBufferingUpdateListener(new MediaPlayer.OnBufferingUpdateListener() {
                public void onBufferingUpdate(MediaPlayer mediaPlayer, int i) {
                    ULog.d(PlayerEngineImpl.TAG, "onBuffering: " + i);
                    if (PlayerEngineImpl.this.mPlayerEngineListener != null) {
                        PlayerEngineImpl.this.mPlayerEngineListener.onTrackBuffering(i);
                    }
                }
            });
            internalMediaPlayer.setOnVideoSizeChangedListener(new MediaPlayer.OnVideoSizeChangedListener() {
                public void onVideoSizeChanged(MediaPlayer mediaPlayer, int i, int i2) {
                    if (PlayerEngineImpl.this.mPlayerEngineListener != null) {
                        PlayerEngineImpl.this.mPlayerEngineListener.onVideoSizeChange(i, i2);
                    }
                }
            });
            internalMediaPlayer.setOnErrorListener(new MediaPlayer.OnErrorListener() {
                public boolean onError(MediaPlayer mediaPlayer, int i, int i2) {
                    ULog.d(PlayerEngineImpl.TAG, "PlayerEngineImpl fail, what (" + i + ") extra (" + i2 + ")");
                    if (i == 1) {
                        if (PlayerEngineImpl.this.mPlayerEngineListener != null) {
                            PlayerEngineImpl.this.mPlayerEngineListener.onTrackStreamError();
                        }
                        PlayerEngineImpl.this.stop();
                        return true;
                    }
                    if (i == -1) {
                        long currentTimeMillis = System.currentTimeMillis();
                        if (currentTimeMillis - PlayerEngineImpl.this.mLastFailTime > PlayerEngineImpl.FAIL_TIME_FRAME) {
                            PlayerEngineImpl.this.mTimesFailed = 1;
                            PlayerEngineImpl.this.mLastFailTime = currentTimeMillis;
                            ULog.d(PlayerEngineImpl.TAG, "PlayerEngineImpl " + PlayerEngineImpl.this.mTimesFailed + " fail within FAIL_TIME_FRAME");
                        } else {
                            PlayerEngineImpl playerEngineImpl = PlayerEngineImpl.this;
                            playerEngineImpl.mTimesFailed = playerEngineImpl.mTimesFailed + 1;
                            if (PlayerEngineImpl.this.mTimesFailed > 2) {
                                ULog.d(PlayerEngineImpl.TAG, "PlayerEngineImpl too many fails, aborting playback");
                                if (PlayerEngineImpl.this.mPlayerEngineListener != null) {
                                    PlayerEngineImpl.this.mPlayerEngineListener.onTrackStreamError();
                                }
                                PlayerEngineImpl.this.stop();
                                return true;
                            }
                        }
                    }
                    return false;
                }
            });
            ULog.d(TAG, "Player [buffering] " + internalMediaPlayer.playPath);
            internalMediaPlayer.preparing = true;
            internalMediaPlayer.prepareAsync();
            internalMediaPlayer.setAudioStreamType(3);
            if (this.mSurfaceHolder != null) {
                internalMediaPlayer.setDisplay(this.mSurfaceHolder);
            }
            if (this.mPlayerEngineListener == null) {
                return internalMediaPlayer;
            }
            this.mPlayerEngineListener.onTrackChanged(this.mPlaylist.getSelectedTrack());
            return internalMediaPlayer;
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            return null;
        } catch (IllegalStateException e2) {
            e2.printStackTrace();
            return null;
        } catch (IOException e3) {
            e3.printStackTrace();
            return null;
        } catch (Exception e4) {
            e4.printStackTrace();
            return null;
        }
    }

    public Playlist getPlaylist() {
        return this.mPlaylist;
    }

    public boolean isPlaying() {
        try {
            if (this.mCurrentMediaPlayer != null && !this.mCurrentMediaPlayer.preparing) {
                return this.mCurrentMediaPlayer.isPlaying();
            }
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public void setListener(PlayerEngineListener playerEngineListener) {
        this.mPlayerEngineListener = playerEngineListener;
    }

    public void setPlaybackMode(Playlist.PlaylistPlaybackMode playlistPlaybackMode) {
        this.mPlaylist.setPlaylistPlaybackMode(playlistPlaybackMode);
    }

    public Playlist.PlaylistPlaybackMode getPlaybackMode() {
        return this.mPlaylist.getPlaylistPlaybackMode();
    }

    public void forward(int i) {
        try {
            this.mCurrentMediaPlayer.seekTo(this.mCurrentMediaPlayer.getCurrentPosition() + i);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void rewind(int i) {
        try {
            this.mCurrentMediaPlayer.seekTo(this.mCurrentMediaPlayer.getCurrentPosition() - i);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void prevList() {
        if (this.prevPlaylist != null) {
            openPlaylist(this.prevPlaylist);
            play();
        }
    }

    public void seekToPosition(int i) {
        try {
            if (this.mCurrentMediaPlayer != null && !this.mCurrentMediaPlayer.preparing) {
                this.mCurrentMediaPlayer.seekTo(i);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public int getDuration() {
        if (this.duration == 0 && this.mCurrentMediaPlayer != null) {
            this.duration = this.mCurrentMediaPlayer.getDuration();
        }
        return this.duration;
    }

    public void setPlayerSurfaceHolder(SurfaceHolder surfaceHolder) {
        this.mSurfaceHolder = surfaceHolder;
    }

    public MediaPlayer getMyCurrentMedia() {
        return this.mCurrentMediaPlayer;
    }
}
