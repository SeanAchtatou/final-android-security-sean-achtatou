package cn.banshenggua.aichang.sdk;

public class ACException extends Exception {
    public static final String NOT_INIT_EXCEPTION = "not init context";
    private static final long serialVersionUID = 2552178674218182505L;

    public ACException(String str) {
        super(str);
    }
}
