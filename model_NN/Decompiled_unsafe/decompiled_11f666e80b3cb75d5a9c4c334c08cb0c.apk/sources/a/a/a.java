package a.a;

/* compiled from: Assert */
public class a {
    public static void a(String str) {
        throw new b(str);
    }

    public static void a(String str, Object obj, Object obj2) {
        if (obj != null || obj2 != null) {
            if (obj == null || !obj.equals(obj2)) {
                b(str, obj, obj2);
            }
        }
    }

    public static void a(String str, boolean z, boolean z2) {
        a(str, Boolean.valueOf(z), Boolean.valueOf(z2));
    }

    public static void a(boolean z, boolean z2) {
        a((String) null, z, z2);
    }

    public static void b(String str, Object obj, Object obj2) {
        a(c(str, obj, obj2));
    }

    public static String c(String str, Object obj, Object obj2) {
        String str2 = "";
        if (str != null) {
            str2 = str + " ";
        }
        return str2 + "expected:<" + obj + "> but was:<" + obj2 + ">";
    }
}
