package android.support.v7.widget;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.drawable.Drawable;

/* compiled from: ActionBarBackgroundDrawable */
class a extends Drawable {

    /* renamed from: a  reason: collision with root package name */
    final ActionBarContainer f205a;

    public a(ActionBarContainer actionBarContainer) {
        this.f205a = actionBarContainer;
    }

    public void draw(Canvas canvas) {
        if (!this.f205a.d) {
            if (this.f205a.f116a != null) {
                this.f205a.f116a.draw(canvas);
            }
            if (this.f205a.b != null && this.f205a.e) {
                this.f205a.b.draw(canvas);
            }
        } else if (this.f205a.c != null) {
            this.f205a.c.draw(canvas);
        }
    }

    public void setAlpha(int i) {
    }

    public void setColorFilter(ColorFilter colorFilter) {
    }

    public int getOpacity() {
        return 0;
    }
}
