package android.support.v7.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v7.a.a;
import android.text.method.PasswordTransformationMethod;
import android.util.AttributeSet;
import android.widget.TextView;

/* compiled from: AppCompatTextHelper */
class k {

    /* renamed from: a  reason: collision with root package name */
    final TextView f232a;
    private aa b;
    private aa c;
    private aa d;
    private aa e;

    static k a(TextView textView) {
        if (Build.VERSION.SDK_INT >= 17) {
            return new l(textView);
        }
        return new k(textView);
    }

    k(TextView textView) {
        this.f232a = textView;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.ac.a(int, boolean):boolean
     arg types: [int, int]
     candidates:
      android.support.v7.widget.ac.a(int, float):float
      android.support.v7.widget.ac.a(int, int):int
      android.support.v7.widget.ac.a(int, boolean):boolean */
    /* access modifiers changed from: package-private */
    public void a(AttributeSet attributeSet, int i) {
        ColorStateList colorStateList;
        boolean z;
        boolean z2;
        ColorStateList colorStateList2 = null;
        Context context = this.f232a.getContext();
        g a2 = g.a();
        ac a3 = ac.a(context, attributeSet, a.k.AppCompatTextHelper, i, 0);
        int g = a3.g(a.k.AppCompatTextHelper_android_textAppearance, -1);
        if (a3.g(a.k.AppCompatTextHelper_android_drawableLeft)) {
            this.b = a(context, a2, a3.g(a.k.AppCompatTextHelper_android_drawableLeft, 0));
        }
        if (a3.g(a.k.AppCompatTextHelper_android_drawableTop)) {
            this.c = a(context, a2, a3.g(a.k.AppCompatTextHelper_android_drawableTop, 0));
        }
        if (a3.g(a.k.AppCompatTextHelper_android_drawableRight)) {
            this.d = a(context, a2, a3.g(a.k.AppCompatTextHelper_android_drawableRight, 0));
        }
        if (a3.g(a.k.AppCompatTextHelper_android_drawableBottom)) {
            this.e = a(context, a2, a3.g(a.k.AppCompatTextHelper_android_drawableBottom, 0));
        }
        a3.a();
        boolean z3 = this.f232a.getTransformationMethod() instanceof PasswordTransformationMethod;
        if (g != -1) {
            ac a4 = ac.a(context, g, a.k.TextAppearance);
            if (z3 || !a4.g(a.k.TextAppearance_textAllCaps)) {
                z = false;
                z2 = false;
            } else {
                z2 = a4.a(a.k.TextAppearance_textAllCaps, false);
                z = true;
            }
            if (Build.VERSION.SDK_INT < 23) {
                if (a4.g(a.k.TextAppearance_android_textColor)) {
                    colorStateList = a4.e(a.k.TextAppearance_android_textColor);
                } else {
                    colorStateList = null;
                }
                if (a4.g(a.k.TextAppearance_android_textColorHint)) {
                    colorStateList2 = a4.e(a.k.TextAppearance_android_textColorHint);
                }
            } else {
                colorStateList = null;
            }
            a4.a();
        } else {
            colorStateList = null;
            z = false;
            z2 = false;
        }
        ac a5 = ac.a(context, attributeSet, a.k.TextAppearance, i, 0);
        if (!z3 && a5.g(a.k.TextAppearance_textAllCaps)) {
            z2 = a5.a(a.k.TextAppearance_textAllCaps, false);
            z = true;
        }
        if (Build.VERSION.SDK_INT < 23) {
            if (a5.g(a.k.TextAppearance_android_textColor)) {
                colorStateList = a5.e(a.k.TextAppearance_android_textColor);
            }
            if (a5.g(a.k.TextAppearance_android_textColorHint)) {
                colorStateList2 = a5.e(a.k.TextAppearance_android_textColorHint);
            }
        }
        a5.a();
        if (colorStateList != null) {
            this.f232a.setTextColor(colorStateList);
        }
        if (colorStateList2 != null) {
            this.f232a.setHintTextColor(colorStateList2);
        }
        if (!z3 && z) {
            a(z2);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.ac.a(int, boolean):boolean
     arg types: [int, int]
     candidates:
      android.support.v7.widget.ac.a(int, float):float
      android.support.v7.widget.ac.a(int, int):int
      android.support.v7.widget.ac.a(int, boolean):boolean */
    /* access modifiers changed from: package-private */
    public void a(Context context, int i) {
        ColorStateList e2;
        ac a2 = ac.a(context, i, a.k.TextAppearance);
        if (a2.g(a.k.TextAppearance_textAllCaps)) {
            a(a2.a(a.k.TextAppearance_textAllCaps, false));
        }
        if (Build.VERSION.SDK_INT < 23 && a2.g(a.k.TextAppearance_android_textColor) && (e2 = a2.e(a.k.TextAppearance_android_textColor)) != null) {
            this.f232a.setTextColor(e2);
        }
        a2.a();
    }

    /* access modifiers changed from: package-private */
    public void a(boolean z) {
        this.f232a.setTransformationMethod(z ? new android.support.v7.d.a(this.f232a.getContext()) : null);
    }

    /* access modifiers changed from: package-private */
    public void a() {
        if (this.b != null || this.c != null || this.d != null || this.e != null) {
            Drawable[] compoundDrawables = this.f232a.getCompoundDrawables();
            a(compoundDrawables[0], this.b);
            a(compoundDrawables[1], this.c);
            a(compoundDrawables[2], this.d);
            a(compoundDrawables[3], this.e);
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(Drawable drawable, aa aaVar) {
        if (drawable != null && aaVar != null) {
            g.a(drawable, aaVar, this.f232a.getDrawableState());
        }
    }

    protected static aa a(Context context, g gVar, int i) {
        ColorStateList b2 = gVar.b(context, i);
        if (b2 == null) {
            return null;
        }
        aa aaVar = new aa();
        aaVar.e = true;
        aaVar.b = b2;
        return aaVar;
    }
}
