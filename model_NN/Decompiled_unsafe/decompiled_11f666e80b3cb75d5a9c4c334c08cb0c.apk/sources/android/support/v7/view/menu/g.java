package android.support.v7.view.menu;

import android.content.DialogInterface;
import android.os.IBinder;
import android.support.v7.a.a;
import android.support.v7.app.b;
import android.support.v7.view.menu.l;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

/* compiled from: MenuDialogHelper */
class g implements DialogInterface.OnClickListener, DialogInterface.OnDismissListener, DialogInterface.OnKeyListener, l.a {

    /* renamed from: a  reason: collision with root package name */
    e f104a;
    private MenuBuilder b;
    private b c;
    private l.a d;

    public g(MenuBuilder menuBuilder) {
        this.b = menuBuilder;
    }

    public void a(IBinder iBinder) {
        MenuBuilder menuBuilder = this.b;
        b.a aVar = new b.a(menuBuilder.e());
        this.f104a = new e(aVar.a(), a.h.abc_list_menu_item_layout);
        this.f104a.a(this);
        this.b.a(this.f104a);
        aVar.a(this.f104a.a(), this);
        View o = menuBuilder.o();
        if (o != null) {
            aVar.a(o);
        } else {
            aVar.a(menuBuilder.n()).a(menuBuilder.m());
        }
        aVar.a(this);
        this.c = aVar.b();
        this.c.setOnDismissListener(this);
        WindowManager.LayoutParams attributes = this.c.getWindow().getAttributes();
        attributes.type = 1003;
        if (iBinder != null) {
            attributes.token = iBinder;
        }
        attributes.flags |= 131072;
        this.c.show();
    }

    public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
        Window window;
        View decorView;
        KeyEvent.DispatcherState keyDispatcherState;
        View decorView2;
        KeyEvent.DispatcherState keyDispatcherState2;
        if (i == 82 || i == 4) {
            if (keyEvent.getAction() == 0 && keyEvent.getRepeatCount() == 0) {
                Window window2 = this.c.getWindow();
                if (!(window2 == null || (decorView2 = window2.getDecorView()) == null || (keyDispatcherState2 = decorView2.getKeyDispatcherState()) == null)) {
                    keyDispatcherState2.startTracking(keyEvent, this);
                    return true;
                }
            } else if (keyEvent.getAction() == 1 && !keyEvent.isCanceled() && (window = this.c.getWindow()) != null && (decorView = window.getDecorView()) != null && (keyDispatcherState = decorView.getKeyDispatcherState()) != null && keyDispatcherState.isTracking(keyEvent)) {
                this.b.a(true);
                dialogInterface.dismiss();
                return true;
            }
        }
        return this.b.performShortcut(i, keyEvent, 0);
    }

    public void a() {
        if (this.c != null) {
            this.c.dismiss();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.view.menu.e.a(android.support.v7.view.menu.MenuBuilder, boolean):void
     arg types: [android.support.v7.view.menu.MenuBuilder, int]
     candidates:
      android.support.v7.view.menu.e.a(android.content.Context, android.support.v7.view.menu.MenuBuilder):void
      android.support.v7.view.menu.e.a(android.support.v7.view.menu.MenuBuilder, android.support.v7.view.menu.MenuItemImpl):boolean
      android.support.v7.view.menu.l.a(android.content.Context, android.support.v7.view.menu.MenuBuilder):void
      android.support.v7.view.menu.l.a(android.support.v7.view.menu.MenuBuilder, android.support.v7.view.menu.MenuItemImpl):boolean
      android.support.v7.view.menu.e.a(android.support.v7.view.menu.MenuBuilder, boolean):void */
    public void onDismiss(DialogInterface dialogInterface) {
        this.f104a.a(this.b, true);
    }

    public void a(MenuBuilder menuBuilder, boolean z) {
        if (z || menuBuilder == this.b) {
            a();
        }
        if (this.d != null) {
            this.d.a(menuBuilder, z);
        }
    }

    public boolean a(MenuBuilder menuBuilder) {
        if (this.d != null) {
            return this.d.a(menuBuilder);
        }
        return false;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        this.b.a((MenuItemImpl) this.f104a.a().getItem(i), 0);
    }
}
