package android.support.v7.view.menu;

import android.content.Context;
import android.os.Build;
import android.support.v4.internal.view.SupportMenu;
import android.support.v4.internal.view.SupportMenuItem;
import android.support.v4.internal.view.SupportSubMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;

/* compiled from: MenuWrapperFactory */
public final class n {
    public static Menu a(Context context, SupportMenu supportMenu) {
        if (Build.VERSION.SDK_INT >= 14) {
            return new o(context, supportMenu);
        }
        throw new UnsupportedOperationException();
    }

    public static MenuItem a(Context context, SupportMenuItem supportMenuItem) {
        if (Build.VERSION.SDK_INT >= 16) {
            return new i(context, supportMenuItem);
        }
        if (Build.VERSION.SDK_INT >= 14) {
            return new h(context, supportMenuItem);
        }
        throw new UnsupportedOperationException();
    }

    public static SubMenu a(Context context, SupportSubMenu supportSubMenu) {
        if (Build.VERSION.SDK_INT >= 14) {
            return new r(context, supportSubMenu);
        }
        throw new UnsupportedOperationException();
    }
}
