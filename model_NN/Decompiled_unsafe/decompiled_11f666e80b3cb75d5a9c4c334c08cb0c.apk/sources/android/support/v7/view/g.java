package android.support.v7.view;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.res.TypedArray;
import android.content.res.XmlResourceParser;
import android.support.v4.internal.view.SupportMenu;
import android.support.v4.view.ActionProvider;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.a.a;
import android.support.v7.view.menu.MenuItemImpl;
import android.support.v7.view.menu.h;
import android.util.AttributeSet;
import android.util.Log;
import android.util.Xml;
import android.view.InflateException;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import cn.banshenggua.aichang.utils.Constants;
import com.tencent.open.wpa.WPA;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

/* compiled from: SupportMenuInflater */
public class g extends MenuInflater {

    /* renamed from: a  reason: collision with root package name */
    static final Class<?>[] f81a = {Context.class};
    static final Class<?>[] b = f81a;
    final Object[] c;
    final Object[] d = this.c;
    Context e;
    private Object f;

    public g(Context context) {
        super(context);
        this.e = context;
        this.c = new Object[]{context};
    }

    public void inflate(int i, Menu menu) {
        if (!(menu instanceof SupportMenu)) {
            super.inflate(i, menu);
            return;
        }
        XmlResourceParser xmlResourceParser = null;
        try {
            xmlResourceParser = this.e.getResources().getLayout(i);
            a(xmlResourceParser, Xml.asAttributeSet(xmlResourceParser), menu);
            if (xmlResourceParser != null) {
                xmlResourceParser.close();
            }
        } catch (XmlPullParserException e2) {
            throw new InflateException("Error inflating menu XML", e2);
        } catch (IOException e3) {
            throw new InflateException("Error inflating menu XML", e3);
        } catch (Throwable th) {
            if (xmlResourceParser != null) {
                xmlResourceParser.close();
            }
            throw th;
        }
    }

    private void a(XmlPullParser xmlPullParser, AttributeSet attributeSet, Menu menu) throws XmlPullParserException, IOException {
        boolean z;
        b bVar = new b(menu);
        int eventType = xmlPullParser.getEventType();
        while (true) {
            if (eventType != 2) {
                eventType = xmlPullParser.next();
                if (eventType == 1) {
                    break;
                }
            } else {
                String name = xmlPullParser.getName();
                if (name.equals("menu")) {
                    eventType = xmlPullParser.next();
                } else {
                    throw new RuntimeException("Expecting menu, got " + name);
                }
            }
        }
        String str = null;
        boolean z2 = false;
        int i = eventType;
        boolean z3 = false;
        while (!z3) {
            switch (i) {
                case 1:
                    throw new RuntimeException("Unexpected end of document");
                case 2:
                    if (z2) {
                        z = z2;
                        continue;
                    } else {
                        String name2 = xmlPullParser.getName();
                        if (name2.equals(WPA.CHAT_TYPE_GROUP)) {
                            bVar.a(attributeSet);
                            z = z2;
                        } else if (name2.equals(Constants.ITEM)) {
                            bVar.b(attributeSet);
                            z = z2;
                        } else if (name2.equals("menu")) {
                            a(xmlPullParser, attributeSet, bVar.c());
                            z = z2;
                        } else {
                            str = name2;
                            z = true;
                        }
                    }
                    boolean z4 = z;
                    i = xmlPullParser.next();
                    z2 = z4;
                case 3:
                    String name3 = xmlPullParser.getName();
                    if (!z2 || !name3.equals(str)) {
                        if (name3.equals(WPA.CHAT_TYPE_GROUP)) {
                            bVar.a();
                            z = z2;
                        } else if (name3.equals(Constants.ITEM)) {
                            if (!bVar.d()) {
                                if (bVar.f83a == null || !bVar.f83a.hasSubMenu()) {
                                    bVar.b();
                                    z = z2;
                                } else {
                                    bVar.c();
                                    z = z2;
                                }
                            }
                        } else if (name3.equals("menu")) {
                            z3 = true;
                            z = z2;
                        }
                        boolean z42 = z;
                        i = xmlPullParser.next();
                        z2 = z42;
                    } else {
                        str = null;
                        z = false;
                        continue;
                        boolean z422 = z;
                        i = xmlPullParser.next();
                        z2 = z422;
                    }
                    break;
            }
            z = z2;
            boolean z4222 = z;
            i = xmlPullParser.next();
            z2 = z4222;
        }
    }

    /* access modifiers changed from: package-private */
    public Object a() {
        if (this.f == null) {
            this.f = a(this.e);
        }
        return this.f;
    }

    private Object a(Object obj) {
        if (!(obj instanceof Activity) && (obj instanceof ContextWrapper)) {
            return a(((ContextWrapper) obj).getBaseContext());
        }
        return obj;
    }

    /* compiled from: SupportMenuInflater */
    private static class a implements MenuItem.OnMenuItemClickListener {

        /* renamed from: a  reason: collision with root package name */
        private static final Class<?>[] f82a = {MenuItem.class};
        private Object b;
        private Method c;

        public a(Object obj, String str) {
            this.b = obj;
            Class<?> cls = obj.getClass();
            try {
                this.c = cls.getMethod(str, f82a);
            } catch (Exception e) {
                InflateException inflateException = new InflateException("Couldn't resolve menu item onClick handler " + str + " in class " + cls.getName());
                inflateException.initCause(e);
                throw inflateException;
            }
        }

        public boolean onMenuItemClick(MenuItem menuItem) {
            try {
                if (this.c.getReturnType() == Boolean.TYPE) {
                    return ((Boolean) this.c.invoke(this.b, menuItem)).booleanValue();
                }
                this.c.invoke(this.b, menuItem);
                return true;
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }

    /* compiled from: SupportMenuInflater */
    private class b {

        /* renamed from: a  reason: collision with root package name */
        ActionProvider f83a;
        private Menu c;
        private int d;
        private int e;
        private int f;
        private int g;
        private boolean h;
        private boolean i;
        private boolean j;
        private int k;
        private int l;
        private CharSequence m;
        private CharSequence n;
        private int o;
        private char p;
        private char q;
        private int r;
        private boolean s;
        private boolean t;
        private boolean u;
        private int v;
        private int w;
        private String x;
        private String y;
        private String z;

        public b(Menu menu) {
            this.c = menu;
            a();
        }

        public void a() {
            this.d = 0;
            this.e = 0;
            this.f = 0;
            this.g = 0;
            this.h = true;
            this.i = true;
        }

        public void a(AttributeSet attributeSet) {
            TypedArray obtainStyledAttributes = g.this.e.obtainStyledAttributes(attributeSet, a.k.MenuGroup);
            this.d = obtainStyledAttributes.getResourceId(a.k.MenuGroup_android_id, 0);
            this.e = obtainStyledAttributes.getInt(a.k.MenuGroup_android_menuCategory, 0);
            this.f = obtainStyledAttributes.getInt(a.k.MenuGroup_android_orderInCategory, 0);
            this.g = obtainStyledAttributes.getInt(a.k.MenuGroup_android_checkableBehavior, 0);
            this.h = obtainStyledAttributes.getBoolean(a.k.MenuGroup_android_visible, true);
            this.i = obtainStyledAttributes.getBoolean(a.k.MenuGroup_android_enabled, true);
            obtainStyledAttributes.recycle();
        }

        public void b(AttributeSet attributeSet) {
            boolean z2 = true;
            TypedArray obtainStyledAttributes = g.this.e.obtainStyledAttributes(attributeSet, a.k.MenuItem);
            this.k = obtainStyledAttributes.getResourceId(a.k.MenuItem_android_id, 0);
            this.l = (obtainStyledAttributes.getInt(a.k.MenuItem_android_menuCategory, this.e) & SupportMenu.CATEGORY_MASK) | (obtainStyledAttributes.getInt(a.k.MenuItem_android_orderInCategory, this.f) & SupportMenu.USER_MASK);
            this.m = obtainStyledAttributes.getText(a.k.MenuItem_android_title);
            this.n = obtainStyledAttributes.getText(a.k.MenuItem_android_titleCondensed);
            this.o = obtainStyledAttributes.getResourceId(a.k.MenuItem_android_icon, 0);
            this.p = a(obtainStyledAttributes.getString(a.k.MenuItem_android_alphabeticShortcut));
            this.q = a(obtainStyledAttributes.getString(a.k.MenuItem_android_numericShortcut));
            if (obtainStyledAttributes.hasValue(a.k.MenuItem_android_checkable)) {
                this.r = obtainStyledAttributes.getBoolean(a.k.MenuItem_android_checkable, false) ? 1 : 0;
            } else {
                this.r = this.g;
            }
            this.s = obtainStyledAttributes.getBoolean(a.k.MenuItem_android_checked, false);
            this.t = obtainStyledAttributes.getBoolean(a.k.MenuItem_android_visible, this.h);
            this.u = obtainStyledAttributes.getBoolean(a.k.MenuItem_android_enabled, this.i);
            this.v = obtainStyledAttributes.getInt(a.k.MenuItem_showAsAction, -1);
            this.z = obtainStyledAttributes.getString(a.k.MenuItem_android_onClick);
            this.w = obtainStyledAttributes.getResourceId(a.k.MenuItem_actionLayout, 0);
            this.x = obtainStyledAttributes.getString(a.k.MenuItem_actionViewClass);
            this.y = obtainStyledAttributes.getString(a.k.MenuItem_actionProviderClass);
            if (this.y == null) {
                z2 = false;
            }
            if (z2 && this.w == 0 && this.x == null) {
                this.f83a = (ActionProvider) a(this.y, g.b, g.this.d);
            } else {
                if (z2) {
                    Log.w("SupportMenuInflater", "Ignoring attribute 'actionProviderClass'. Action view already specified.");
                }
                this.f83a = null;
            }
            obtainStyledAttributes.recycle();
            this.j = false;
        }

        private char a(String str) {
            if (str == null) {
                return 0;
            }
            return str.charAt(0);
        }

        private void a(MenuItem menuItem) {
            boolean z2 = true;
            menuItem.setChecked(this.s).setVisible(this.t).setEnabled(this.u).setCheckable(this.r >= 1).setTitleCondensed(this.n).setIcon(this.o).setAlphabeticShortcut(this.p).setNumericShortcut(this.q);
            if (this.v >= 0) {
                MenuItemCompat.setShowAsAction(menuItem, this.v);
            }
            if (this.z != null) {
                if (g.this.e.isRestricted()) {
                    throw new IllegalStateException("The android:onClick attribute cannot be used within a restricted context");
                }
                menuItem.setOnMenuItemClickListener(new a(g.this.a(), this.z));
            }
            if (menuItem instanceof MenuItemImpl) {
                MenuItemImpl menuItemImpl = (MenuItemImpl) menuItem;
            }
            if (this.r >= 2) {
                if (menuItem instanceof MenuItemImpl) {
                    ((MenuItemImpl) menuItem).a(true);
                } else if (menuItem instanceof h) {
                    ((h) menuItem).a(true);
                }
            }
            if (this.x != null) {
                MenuItemCompat.setActionView(menuItem, (View) a(this.x, g.f81a, g.this.c));
            } else {
                z2 = false;
            }
            if (this.w > 0) {
                if (!z2) {
                    MenuItemCompat.setActionView(menuItem, this.w);
                } else {
                    Log.w("SupportMenuInflater", "Ignoring attribute 'itemActionViewLayout'. Action view already specified.");
                }
            }
            if (this.f83a != null) {
                MenuItemCompat.setActionProvider(menuItem, this.f83a);
            }
        }

        public void b() {
            this.j = true;
            a(this.c.add(this.d, this.k, this.l, this.m));
        }

        public SubMenu c() {
            this.j = true;
            SubMenu addSubMenu = this.c.addSubMenu(this.d, this.k, this.l, this.m);
            a(addSubMenu.getItem());
            return addSubMenu;
        }

        public boolean d() {
            return this.j;
        }

        private <T> T a(String str, Class<?>[] clsArr, Object[] objArr) {
            try {
                Constructor<?> constructor = g.this.e.getClassLoader().loadClass(str).getConstructor(clsArr);
                constructor.setAccessible(true);
                return constructor.newInstance(objArr);
            } catch (Exception e2) {
                Log.w("SupportMenuInflater", "Cannot instantiate class: " + str, e2);
                return null;
            }
        }
    }
}
