package android.support.v7.view.menu;

import android.support.v7.a.a;
import android.support.v7.view.menu.m;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import java.util.ArrayList;

/* compiled from: MenuAdapter */
public class f extends BaseAdapter {

    /* renamed from: a  reason: collision with root package name */
    static final int f103a = a.h.abc_popup_menu_item_layout;
    MenuBuilder b;
    private int c = -1;
    private boolean d;
    private final boolean e;
    private final LayoutInflater f;

    public f(MenuBuilder menuBuilder, LayoutInflater layoutInflater, boolean z) {
        this.e = z;
        this.f = layoutInflater;
        this.b = menuBuilder;
        b();
    }

    public void a(boolean z) {
        this.d = z;
    }

    public int getCount() {
        ArrayList<MenuItemImpl> l = this.e ? this.b.l() : this.b.i();
        if (this.c < 0) {
            return l.size();
        }
        return l.size() - 1;
    }

    public MenuBuilder a() {
        return this.b;
    }

    /* renamed from: a */
    public MenuItemImpl getItem(int i) {
        ArrayList<MenuItemImpl> l = this.e ? this.b.l() : this.b.i();
        if (this.c >= 0 && i >= this.c) {
            i++;
        }
        return l.get(i);
    }

    public long getItemId(int i) {
        return (long) i;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View getView(int i, View view, ViewGroup viewGroup) {
        View view2;
        if (view == null) {
            view2 = this.f.inflate(f103a, viewGroup, false);
        } else {
            view2 = view;
        }
        m.a aVar = (m.a) view2;
        if (this.d) {
            ((ListMenuItemView) view2).setForceShowIcon(true);
        }
        aVar.a(getItem(i), 0);
        return view2;
    }

    /* access modifiers changed from: package-private */
    public void b() {
        MenuItemImpl r = this.b.r();
        if (r != null) {
            ArrayList<MenuItemImpl> l = this.b.l();
            int size = l.size();
            for (int i = 0; i < size; i++) {
                if (l.get(i) == r) {
                    this.c = i;
                    return;
                }
            }
        }
        this.c = -1;
    }

    public void notifyDataSetChanged() {
        b();
        super.notifyDataSetChanged();
    }
}
