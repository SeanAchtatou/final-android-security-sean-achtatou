package android.support.v7.view.menu;

import android.annotation.TargetApi;
import android.content.Context;
import android.support.v4.internal.view.SupportMenuItem;
import android.support.v4.view.ActionProvider;
import android.support.v7.view.menu.h;
import android.view.ActionProvider;
import android.view.MenuItem;
import android.view.View;

@TargetApi(16)
/* compiled from: MenuItemWrapperJB */
class i extends h {
    i(Context context, SupportMenuItem supportMenuItem) {
        super(context, supportMenuItem);
    }

    /* access modifiers changed from: package-private */
    public h.a a(ActionProvider actionProvider) {
        return new a(this.f95a, actionProvider);
    }

    /* compiled from: MenuItemWrapperJB */
    class a extends h.a implements ActionProvider.VisibilityListener {
        ActionProvider.VisibilityListener c;

        public a(Context context, android.view.ActionProvider actionProvider) {
            super(context, actionProvider);
        }

        public View onCreateActionView(MenuItem menuItem) {
            return this.f105a.onCreateActionView(menuItem);
        }

        public boolean overridesItemVisibility() {
            return this.f105a.overridesItemVisibility();
        }

        public boolean isVisible() {
            return this.f105a.isVisible();
        }

        public void refreshVisibility() {
            this.f105a.refreshVisibility();
        }

        public void setVisibilityListener(ActionProvider.VisibilityListener visibilityListener) {
            this.c = visibilityListener;
            android.view.ActionProvider actionProvider = this.f105a;
            if (visibilityListener == null) {
                this = null;
            }
            actionProvider.setVisibilityListener(this);
        }

        public void onActionProviderVisibilityChanged(boolean z) {
            if (this.c != null) {
                this.c.onActionProviderVisibilityChanged(z);
            }
        }
    }
}
