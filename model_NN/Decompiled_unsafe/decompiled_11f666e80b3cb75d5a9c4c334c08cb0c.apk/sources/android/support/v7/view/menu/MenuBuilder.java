package android.support.v7.view.menu;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.internal.view.SupportMenu;
import android.support.v4.view.ActionProvider;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.a.a;
import android.util.SparseArray;
import android.view.ContextMenu;
import android.view.KeyCharacterMap;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class MenuBuilder implements SupportMenu {
    private static final int[] d = {1, 4, 5, 3, 2, 0};

    /* renamed from: a  reason: collision with root package name */
    CharSequence f91a;
    Drawable b;
    View c;
    private final Context e;
    private final Resources f;
    private boolean g;
    private boolean h;
    private a i;
    private ArrayList<MenuItemImpl> j;
    private ArrayList<MenuItemImpl> k;
    private boolean l;
    private ArrayList<MenuItemImpl> m;
    private ArrayList<MenuItemImpl> n;
    private boolean o;
    private int p = 0;
    private ContextMenu.ContextMenuInfo q;
    private boolean r = false;
    private boolean s = false;
    private boolean t = false;
    private boolean u = false;
    private ArrayList<MenuItemImpl> v = new ArrayList<>();
    private CopyOnWriteArrayList<WeakReference<l>> w = new CopyOnWriteArrayList<>();
    private MenuItemImpl x;
    private boolean y;

    public interface a {
        void a(MenuBuilder menuBuilder);

        boolean a(MenuBuilder menuBuilder, MenuItem menuItem);
    }

    public interface b {
        boolean a(MenuItemImpl menuItemImpl);
    }

    public MenuBuilder(Context context) {
        this.e = context;
        this.f = context.getResources();
        this.j = new ArrayList<>();
        this.k = new ArrayList<>();
        this.l = true;
        this.m = new ArrayList<>();
        this.n = new ArrayList<>();
        this.o = true;
        e(true);
    }

    public MenuBuilder a(int i2) {
        this.p = i2;
        return this;
    }

    public void a(l lVar) {
        a(lVar, this.e);
    }

    public void a(l lVar, Context context) {
        this.w.add(new WeakReference(lVar));
        lVar.a(context, this);
        this.o = true;
    }

    public void b(l lVar) {
        Iterator<WeakReference<l>> it = this.w.iterator();
        while (it.hasNext()) {
            WeakReference next = it.next();
            l lVar2 = (l) next.get();
            if (lVar2 == null || lVar2 == lVar) {
                this.w.remove(next);
            }
        }
    }

    private void d(boolean z) {
        if (!this.w.isEmpty()) {
            g();
            Iterator<WeakReference<l>> it = this.w.iterator();
            while (it.hasNext()) {
                WeakReference next = it.next();
                l lVar = (l) next.get();
                if (lVar == null) {
                    this.w.remove(next);
                } else {
                    lVar.a(z);
                }
            }
            h();
        }
    }

    private boolean a(SubMenuBuilder subMenuBuilder, l lVar) {
        boolean z = false;
        if (this.w.isEmpty()) {
            return false;
        }
        if (lVar != null) {
            z = lVar.a(subMenuBuilder);
        }
        Iterator<WeakReference<l>> it = this.w.iterator();
        while (true) {
            boolean z2 = z;
            if (!it.hasNext()) {
                return z2;
            }
            WeakReference next = it.next();
            l lVar2 = (l) next.get();
            if (lVar2 == null) {
                this.w.remove(next);
            } else if (!z2) {
                z2 = lVar2.a(subMenuBuilder);
            }
            z = z2;
        }
    }

    public void a(Bundle bundle) {
        int size = size();
        int i2 = 0;
        SparseArray sparseArray = null;
        while (i2 < size) {
            MenuItem item = getItem(i2);
            View actionView = MenuItemCompat.getActionView(item);
            if (!(actionView == null || actionView.getId() == -1)) {
                if (sparseArray == null) {
                    sparseArray = new SparseArray();
                }
                actionView.saveHierarchyState(sparseArray);
                if (MenuItemCompat.isActionViewExpanded(item)) {
                    bundle.putInt("android:menu:expandedactionview", item.getItemId());
                }
            }
            SparseArray sparseArray2 = sparseArray;
            if (item.hasSubMenu()) {
                ((SubMenuBuilder) item.getSubMenu()).a(bundle);
            }
            i2++;
            sparseArray = sparseArray2;
        }
        if (sparseArray != null) {
            bundle.putSparseParcelableArray(a(), sparseArray);
        }
    }

    public void b(Bundle bundle) {
        MenuItem findItem;
        if (bundle != null) {
            SparseArray sparseParcelableArray = bundle.getSparseParcelableArray(a());
            int size = size();
            for (int i2 = 0; i2 < size; i2++) {
                MenuItem item = getItem(i2);
                View actionView = MenuItemCompat.getActionView(item);
                if (!(actionView == null || actionView.getId() == -1)) {
                    actionView.restoreHierarchyState(sparseParcelableArray);
                }
                if (item.hasSubMenu()) {
                    ((SubMenuBuilder) item.getSubMenu()).b(bundle);
                }
            }
            int i3 = bundle.getInt("android:menu:expandedactionview");
            if (i3 > 0 && (findItem = findItem(i3)) != null) {
                MenuItemCompat.expandActionView(findItem);
            }
        }
    }

    /* access modifiers changed from: protected */
    public String a() {
        return "android:menu:actionviewstates";
    }

    public void a(a aVar) {
        this.i = aVar;
    }

    /* access modifiers changed from: protected */
    public MenuItem a(int i2, int i3, int i4, CharSequence charSequence) {
        int f2 = f(i4);
        MenuItemImpl a2 = a(i2, i3, i4, f2, charSequence, this.p);
        if (this.q != null) {
            a2.a(this.q);
        }
        this.j.add(a(this.j, f2), a2);
        b(true);
        return a2;
    }

    private MenuItemImpl a(int i2, int i3, int i4, int i5, CharSequence charSequence, int i6) {
        return new MenuItemImpl(this, i2, i3, i4, i5, charSequence, i6);
    }

    public MenuItem add(CharSequence charSequence) {
        return a(0, 0, 0, charSequence);
    }

    public MenuItem add(int i2) {
        return a(0, 0, 0, this.f.getString(i2));
    }

    public MenuItem add(int i2, int i3, int i4, CharSequence charSequence) {
        return a(i2, i3, i4, charSequence);
    }

    public MenuItem add(int i2, int i3, int i4, int i5) {
        return a(i2, i3, i4, this.f.getString(i5));
    }

    public SubMenu addSubMenu(CharSequence charSequence) {
        return addSubMenu(0, 0, 0, charSequence);
    }

    public SubMenu addSubMenu(int i2) {
        return addSubMenu(0, 0, 0, this.f.getString(i2));
    }

    public SubMenu addSubMenu(int i2, int i3, int i4, CharSequence charSequence) {
        MenuItemImpl menuItemImpl = (MenuItemImpl) a(i2, i3, i4, charSequence);
        SubMenuBuilder subMenuBuilder = new SubMenuBuilder(this.e, this, menuItemImpl);
        menuItemImpl.a(subMenuBuilder);
        return subMenuBuilder;
    }

    public SubMenu addSubMenu(int i2, int i3, int i4, int i5) {
        return addSubMenu(i2, i3, i4, this.f.getString(i5));
    }

    public int addIntentOptions(int i2, int i3, int i4, ComponentName componentName, Intent[] intentArr, Intent intent, int i5, MenuItem[] menuItemArr) {
        Intent intent2;
        PackageManager packageManager = this.e.getPackageManager();
        List<ResolveInfo> queryIntentActivityOptions = packageManager.queryIntentActivityOptions(componentName, intentArr, intent, 0);
        int size = queryIntentActivityOptions != null ? queryIntentActivityOptions.size() : 0;
        if ((i5 & 1) == 0) {
            removeGroup(i2);
        }
        for (int i6 = 0; i6 < size; i6++) {
            ResolveInfo resolveInfo = queryIntentActivityOptions.get(i6);
            if (resolveInfo.specificIndex < 0) {
                intent2 = intent;
            } else {
                intent2 = intentArr[resolveInfo.specificIndex];
            }
            Intent intent3 = new Intent(intent2);
            intent3.setComponent(new ComponentName(resolveInfo.activityInfo.applicationInfo.packageName, resolveInfo.activityInfo.name));
            MenuItem intent4 = add(i2, i3, i4, resolveInfo.loadLabel(packageManager)).setIcon(resolveInfo.loadIcon(packageManager)).setIntent(intent3);
            if (menuItemArr != null && resolveInfo.specificIndex >= 0) {
                menuItemArr[resolveInfo.specificIndex] = intent4;
            }
        }
        return size;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.view.menu.MenuBuilder.a(int, boolean):void
     arg types: [int, int]
     candidates:
      android.support.v7.view.menu.MenuBuilder.a(java.util.ArrayList<android.support.v7.view.menu.MenuItemImpl>, int):int
      android.support.v7.view.menu.MenuBuilder.a(android.support.v7.view.menu.SubMenuBuilder, android.support.v7.view.menu.l):boolean
      android.support.v7.view.menu.MenuBuilder.a(int, int):int
      android.support.v7.view.menu.MenuBuilder.a(int, android.view.KeyEvent):android.support.v7.view.menu.MenuItemImpl
      android.support.v7.view.menu.MenuBuilder.a(android.support.v7.view.menu.l, android.content.Context):void
      android.support.v7.view.menu.MenuBuilder.a(android.support.v7.view.menu.MenuBuilder, android.view.MenuItem):boolean
      android.support.v7.view.menu.MenuBuilder.a(android.view.MenuItem, int):boolean
      android.support.v7.view.menu.MenuBuilder.a(int, boolean):void */
    public void removeItem(int i2) {
        a(b(i2), true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.view.menu.MenuBuilder.a(int, boolean):void
     arg types: [int, int]
     candidates:
      android.support.v7.view.menu.MenuBuilder.a(java.util.ArrayList<android.support.v7.view.menu.MenuItemImpl>, int):int
      android.support.v7.view.menu.MenuBuilder.a(android.support.v7.view.menu.SubMenuBuilder, android.support.v7.view.menu.l):boolean
      android.support.v7.view.menu.MenuBuilder.a(int, int):int
      android.support.v7.view.menu.MenuBuilder.a(int, android.view.KeyEvent):android.support.v7.view.menu.MenuItemImpl
      android.support.v7.view.menu.MenuBuilder.a(android.support.v7.view.menu.l, android.content.Context):void
      android.support.v7.view.menu.MenuBuilder.a(android.support.v7.view.menu.MenuBuilder, android.view.MenuItem):boolean
      android.support.v7.view.menu.MenuBuilder.a(android.view.MenuItem, int):boolean
      android.support.v7.view.menu.MenuBuilder.a(int, boolean):void */
    public void removeGroup(int i2) {
        int c2 = c(i2);
        if (c2 >= 0) {
            int size = this.j.size() - c2;
            int i3 = 0;
            while (true) {
                int i4 = i3 + 1;
                if (i3 >= size || this.j.get(c2).getGroupId() != i2) {
                    b(true);
                } else {
                    a(c2, false);
                    i3 = i4;
                }
            }
            b(true);
        }
    }

    private void a(int i2, boolean z) {
        if (i2 >= 0 && i2 < this.j.size()) {
            this.j.remove(i2);
            if (z) {
                b(true);
            }
        }
    }

    public void clear() {
        if (this.x != null) {
            d(this.x);
        }
        this.j.clear();
        b(true);
    }

    /* access modifiers changed from: package-private */
    public void a(MenuItem menuItem) {
        int groupId = menuItem.getGroupId();
        int size = this.j.size();
        for (int i2 = 0; i2 < size; i2++) {
            MenuItemImpl menuItemImpl = this.j.get(i2);
            if (menuItemImpl.getGroupId() == groupId && menuItemImpl.f() && menuItemImpl.isCheckable()) {
                menuItemImpl.b(menuItemImpl == menuItem);
            }
        }
    }

    public void setGroupCheckable(int i2, boolean z, boolean z2) {
        int size = this.j.size();
        for (int i3 = 0; i3 < size; i3++) {
            MenuItemImpl menuItemImpl = this.j.get(i3);
            if (menuItemImpl.getGroupId() == i2) {
                menuItemImpl.a(z2);
                menuItemImpl.setCheckable(z);
            }
        }
    }

    public void setGroupVisible(int i2, boolean z) {
        boolean z2;
        int size = this.j.size();
        int i3 = 0;
        boolean z3 = false;
        while (i3 < size) {
            MenuItemImpl menuItemImpl = this.j.get(i3);
            if (menuItemImpl.getGroupId() != i2 || !menuItemImpl.c(z)) {
                z2 = z3;
            } else {
                z2 = true;
            }
            i3++;
            z3 = z2;
        }
        if (z3) {
            b(true);
        }
    }

    public void setGroupEnabled(int i2, boolean z) {
        int size = this.j.size();
        for (int i3 = 0; i3 < size; i3++) {
            MenuItemImpl menuItemImpl = this.j.get(i3);
            if (menuItemImpl.getGroupId() == i2) {
                menuItemImpl.setEnabled(z);
            }
        }
    }

    public boolean hasVisibleItems() {
        if (this.y) {
            return true;
        }
        int size = size();
        for (int i2 = 0; i2 < size; i2++) {
            if (this.j.get(i2).isVisible()) {
                return true;
            }
        }
        return false;
    }

    public MenuItem findItem(int i2) {
        MenuItem findItem;
        int size = size();
        for (int i3 = 0; i3 < size; i3++) {
            MenuItemImpl menuItemImpl = this.j.get(i3);
            if (menuItemImpl.getItemId() == i2) {
                return menuItemImpl;
            }
            if (menuItemImpl.hasSubMenu() && (findItem = menuItemImpl.getSubMenu().findItem(i2)) != null) {
                return findItem;
            }
        }
        return null;
    }

    public int b(int i2) {
        int size = size();
        for (int i3 = 0; i3 < size; i3++) {
            if (this.j.get(i3).getItemId() == i2) {
                return i3;
            }
        }
        return -1;
    }

    public int c(int i2) {
        return a(i2, 0);
    }

    public int a(int i2, int i3) {
        int size = size();
        if (i3 < 0) {
            i3 = 0;
        }
        for (int i4 = i3; i4 < size; i4++) {
            if (this.j.get(i4).getGroupId() == i2) {
                return i4;
            }
        }
        return -1;
    }

    public int size() {
        return this.j.size();
    }

    public MenuItem getItem(int i2) {
        return this.j.get(i2);
    }

    public boolean isShortcutKey(int i2, KeyEvent keyEvent) {
        return a(i2, keyEvent) != null;
    }

    public void setQwertyMode(boolean z) {
        this.g = z;
        b(false);
    }

    private static int f(int i2) {
        int i3 = (-65536 & i2) >> 16;
        if (i3 >= 0 && i3 < d.length) {
            return (d[i3] << 16) | (65535 & i2);
        }
        throw new IllegalArgumentException("order does not contain a valid category.");
    }

    /* access modifiers changed from: package-private */
    public boolean b() {
        return this.g;
    }

    private void e(boolean z) {
        boolean z2 = true;
        if (!z || this.f.getConfiguration().keyboard == 1 || !this.f.getBoolean(a.b.abc_config_showMenuShortcutsWhenKeyboardPresent)) {
            z2 = false;
        }
        this.h = z2;
    }

    public boolean c() {
        return this.h;
    }

    /* access modifiers changed from: package-private */
    public Resources d() {
        return this.f;
    }

    public Context e() {
        return this.e;
    }

    /* access modifiers changed from: package-private */
    public boolean a(MenuBuilder menuBuilder, MenuItem menuItem) {
        return this.i != null && this.i.a(menuBuilder, menuItem);
    }

    public void f() {
        if (this.i != null) {
            this.i.a(this);
        }
    }

    private static int a(ArrayList<MenuItemImpl> arrayList, int i2) {
        for (int size = arrayList.size() - 1; size >= 0; size--) {
            if (arrayList.get(size).b() <= i2) {
                return size + 1;
            }
        }
        return 0;
    }

    public boolean performShortcut(int i2, KeyEvent keyEvent, int i3) {
        MenuItemImpl a2 = a(i2, keyEvent);
        boolean z = false;
        if (a2 != null) {
            z = a(a2, i3);
        }
        if ((i3 & 2) != 0) {
            a(true);
        }
        return z;
    }

    /* access modifiers changed from: package-private */
    public void a(List<MenuItemImpl> list, int i2, KeyEvent keyEvent) {
        boolean b2 = b();
        int metaState = keyEvent.getMetaState();
        KeyCharacterMap.KeyData keyData = new KeyCharacterMap.KeyData();
        if (keyEvent.getKeyData(keyData) || i2 == 67) {
            int size = this.j.size();
            for (int i3 = 0; i3 < size; i3++) {
                MenuItemImpl menuItemImpl = this.j.get(i3);
                if (menuItemImpl.hasSubMenu()) {
                    ((MenuBuilder) menuItemImpl.getSubMenu()).a(list, i2, keyEvent);
                }
                char alphabeticShortcut = b2 ? menuItemImpl.getAlphabeticShortcut() : menuItemImpl.getNumericShortcut();
                if ((metaState & 5) == 0 && alphabeticShortcut != 0 && ((alphabeticShortcut == keyData.meta[0] || alphabeticShortcut == keyData.meta[2] || (b2 && alphabeticShortcut == 8 && i2 == 67)) && menuItemImpl.isEnabled())) {
                    list.add(menuItemImpl);
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public MenuItemImpl a(int i2, KeyEvent keyEvent) {
        char numericShortcut;
        ArrayList<MenuItemImpl> arrayList = this.v;
        arrayList.clear();
        a(arrayList, i2, keyEvent);
        if (arrayList.isEmpty()) {
            return null;
        }
        int metaState = keyEvent.getMetaState();
        KeyCharacterMap.KeyData keyData = new KeyCharacterMap.KeyData();
        keyEvent.getKeyData(keyData);
        int size = arrayList.size();
        if (size == 1) {
            return arrayList.get(0);
        }
        boolean b2 = b();
        for (int i3 = 0; i3 < size; i3++) {
            MenuItemImpl menuItemImpl = arrayList.get(i3);
            if (b2) {
                numericShortcut = menuItemImpl.getAlphabeticShortcut();
            } else {
                numericShortcut = menuItemImpl.getNumericShortcut();
            }
            if (numericShortcut == keyData.meta[0] && (metaState & 2) == 0) {
                return menuItemImpl;
            }
            if (numericShortcut == keyData.meta[2] && (metaState & 2) != 0) {
                return menuItemImpl;
            }
            if (b2 && numericShortcut == 8 && i2 == 67) {
                return menuItemImpl;
            }
        }
        return null;
    }

    public boolean performIdentifierAction(int i2, int i3) {
        return a(findItem(i2), i3);
    }

    public boolean a(MenuItem menuItem, int i2) {
        return a(menuItem, (l) null, i2);
    }

    public boolean a(MenuItem menuItem, l lVar, int i2) {
        boolean z;
        MenuItemImpl menuItemImpl = (MenuItemImpl) menuItem;
        if (menuItemImpl == null || !menuItemImpl.isEnabled()) {
            return false;
        }
        boolean a2 = menuItemImpl.a();
        ActionProvider supportActionProvider = menuItemImpl.getSupportActionProvider();
        if (supportActionProvider == null || !supportActionProvider.hasSubMenu()) {
            z = false;
        } else {
            z = true;
        }
        if (menuItemImpl.m()) {
            boolean expandActionView = menuItemImpl.expandActionView() | a2;
            if (!expandActionView) {
                return expandActionView;
            }
            a(true);
            return expandActionView;
        } else if (menuItemImpl.hasSubMenu() || z) {
            if ((i2 & 4) == 0) {
                a(false);
            }
            if (!menuItemImpl.hasSubMenu()) {
                menuItemImpl.a(new SubMenuBuilder(e(), this, menuItemImpl));
            }
            SubMenuBuilder subMenuBuilder = (SubMenuBuilder) menuItemImpl.getSubMenu();
            if (z) {
                supportActionProvider.onPrepareSubMenu(subMenuBuilder);
            }
            boolean a3 = a(subMenuBuilder, lVar) | a2;
            if (a3) {
                return a3;
            }
            a(true);
            return a3;
        } else {
            if ((i2 & 1) == 0) {
                a(true);
            }
            return a2;
        }
    }

    public final void a(boolean z) {
        if (!this.u) {
            this.u = true;
            Iterator<WeakReference<l>> it = this.w.iterator();
            while (it.hasNext()) {
                WeakReference next = it.next();
                l lVar = (l) next.get();
                if (lVar == null) {
                    this.w.remove(next);
                } else {
                    lVar.a(this, z);
                }
            }
            this.u = false;
        }
    }

    public void close() {
        a(true);
    }

    public void b(boolean z) {
        if (!this.r) {
            if (z) {
                this.l = true;
                this.o = true;
            }
            d(z);
            return;
        }
        this.s = true;
    }

    public void g() {
        if (!this.r) {
            this.r = true;
            this.s = false;
        }
    }

    public void h() {
        this.r = false;
        if (this.s) {
            this.s = false;
            b(true);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(MenuItemImpl menuItemImpl) {
        this.l = true;
        b(true);
    }

    /* access modifiers changed from: package-private */
    public void b(MenuItemImpl menuItemImpl) {
        this.o = true;
        b(true);
    }

    public ArrayList<MenuItemImpl> i() {
        if (!this.l) {
            return this.k;
        }
        this.k.clear();
        int size = this.j.size();
        for (int i2 = 0; i2 < size; i2++) {
            MenuItemImpl menuItemImpl = this.j.get(i2);
            if (menuItemImpl.isVisible()) {
                this.k.add(menuItemImpl);
            }
        }
        this.l = false;
        this.o = true;
        return this.k;
    }

    public void j() {
        boolean b2;
        ArrayList<MenuItemImpl> i2 = i();
        if (this.o) {
            Iterator<WeakReference<l>> it = this.w.iterator();
            boolean z = false;
            while (it.hasNext()) {
                WeakReference next = it.next();
                l lVar = (l) next.get();
                if (lVar == null) {
                    this.w.remove(next);
                    b2 = z;
                } else {
                    b2 = lVar.b() | z;
                }
                z = b2;
            }
            if (z) {
                this.m.clear();
                this.n.clear();
                int size = i2.size();
                for (int i3 = 0; i3 < size; i3++) {
                    MenuItemImpl menuItemImpl = i2.get(i3);
                    if (menuItemImpl.i()) {
                        this.m.add(menuItemImpl);
                    } else {
                        this.n.add(menuItemImpl);
                    }
                }
            } else {
                this.m.clear();
                this.n.clear();
                this.n.addAll(i());
            }
            this.o = false;
        }
    }

    public ArrayList<MenuItemImpl> k() {
        j();
        return this.m;
    }

    public ArrayList<MenuItemImpl> l() {
        j();
        return this.n;
    }

    public void clearHeader() {
        this.b = null;
        this.f91a = null;
        this.c = null;
        b(false);
    }

    private void a(int i2, CharSequence charSequence, int i3, Drawable drawable, View view) {
        Resources d2 = d();
        if (view != null) {
            this.c = view;
            this.f91a = null;
            this.b = null;
        } else {
            if (i2 > 0) {
                this.f91a = d2.getText(i2);
            } else if (charSequence != null) {
                this.f91a = charSequence;
            }
            if (i3 > 0) {
                this.b = ContextCompat.getDrawable(e(), i3);
            } else if (drawable != null) {
                this.b = drawable;
            }
            this.c = null;
        }
        b(false);
    }

    /* access modifiers changed from: protected */
    public MenuBuilder a(CharSequence charSequence) {
        a(0, charSequence, 0, null, null);
        return this;
    }

    /* access modifiers changed from: protected */
    public MenuBuilder d(int i2) {
        a(i2, null, 0, null, null);
        return this;
    }

    /* access modifiers changed from: protected */
    public MenuBuilder a(Drawable drawable) {
        a(0, null, 0, drawable, null);
        return this;
    }

    /* access modifiers changed from: protected */
    public MenuBuilder e(int i2) {
        a(0, null, i2, null, null);
        return this;
    }

    /* access modifiers changed from: protected */
    public MenuBuilder a(View view) {
        a(0, null, 0, null, view);
        return this;
    }

    public CharSequence m() {
        return this.f91a;
    }

    public Drawable n() {
        return this.b;
    }

    public View o() {
        return this.c;
    }

    public MenuBuilder p() {
        return this;
    }

    /* access modifiers changed from: package-private */
    public boolean q() {
        return this.t;
    }

    public boolean c(MenuItemImpl menuItemImpl) {
        boolean z = false;
        if (!this.w.isEmpty()) {
            g();
            Iterator<WeakReference<l>> it = this.w.iterator();
            while (true) {
                boolean z2 = z;
                if (!it.hasNext()) {
                    z = z2;
                    break;
                }
                WeakReference next = it.next();
                l lVar = (l) next.get();
                if (lVar == null) {
                    this.w.remove(next);
                    z = z2;
                } else {
                    z = lVar.a(this, menuItemImpl);
                    if (z) {
                        break;
                    }
                }
            }
            h();
            if (z) {
                this.x = menuItemImpl;
            }
        }
        return z;
    }

    public boolean d(MenuItemImpl menuItemImpl) {
        boolean z = false;
        if (!this.w.isEmpty() && this.x == menuItemImpl) {
            g();
            Iterator<WeakReference<l>> it = this.w.iterator();
            while (true) {
                boolean z2 = z;
                if (!it.hasNext()) {
                    z = z2;
                    break;
                }
                WeakReference next = it.next();
                l lVar = (l) next.get();
                if (lVar == null) {
                    this.w.remove(next);
                    z = z2;
                } else {
                    z = lVar.b(this, menuItemImpl);
                    if (z) {
                        break;
                    }
                }
            }
            h();
            if (z) {
                this.x = null;
            }
        }
        return z;
    }

    public MenuItemImpl r() {
        return this.x;
    }

    public void c(boolean z) {
        this.y = z;
    }
}
