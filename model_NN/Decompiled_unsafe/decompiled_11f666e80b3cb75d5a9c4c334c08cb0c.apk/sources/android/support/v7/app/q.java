package android.support.v7.app;

import android.content.Context;
import android.content.res.Configuration;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.view.menu.MenuBuilder;
import android.support.v7.view.menu.l;
import android.support.v7.widget.n;
import android.view.KeyCharacterMap;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.view.Window;
import java.util.ArrayList;

/* compiled from: ToolbarActionBar */
class q extends ActionBar {

    /* renamed from: a  reason: collision with root package name */
    n f58a;
    Window.Callback b;
    private boolean c;
    private boolean d;
    private ArrayList<ActionBar.a> e;
    private final Runnable f;

    public void a(boolean z) {
    }

    public void a(float f2) {
        ViewCompat.setElevation(this.f58a.a(), f2);
    }

    public Context c() {
        return this.f58a.b();
    }

    public void c(boolean z) {
    }

    public void d(boolean z) {
    }

    public void a(Configuration configuration) {
        super.a(configuration);
    }

    public void a(CharSequence charSequence) {
        this.f58a.a(charSequence);
    }

    public boolean g() {
        ViewGroup a2 = this.f58a.a();
        if (a2 == null || a2.hasFocus()) {
            return false;
        }
        a2.requestFocus();
        return true;
    }

    public int a() {
        return this.f58a.o();
    }

    public boolean b() {
        return this.f58a.q() == 0;
    }

    public boolean e() {
        this.f58a.a().removeCallbacks(this.f);
        ViewCompat.postOnAnimation(this.f58a.a(), this.f);
        return true;
    }

    public boolean f() {
        if (!this.f58a.c()) {
            return false;
        }
        this.f58a.d();
        return true;
    }

    public boolean a(int i, KeyEvent keyEvent) {
        boolean z;
        Menu i2 = i();
        if (i2 != null) {
            if (KeyCharacterMap.load(keyEvent != null ? keyEvent.getDeviceId() : -1).getKeyboardType() != 1) {
                z = true;
            } else {
                z = false;
            }
            i2.setQwertyMode(z);
            i2.performShortcut(i, keyEvent, 0);
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    public void h() {
        this.f58a.a().removeCallbacks(this.f);
    }

    public void e(boolean z) {
        if (z != this.d) {
            this.d = z;
            int size = this.e.size();
            for (int i = 0; i < size; i++) {
                this.e.get(i).a(z);
            }
        }
    }

    private Menu i() {
        if (!this.c) {
            this.f58a.a(new a(), new b());
            this.c = true;
        }
        return this.f58a.r();
    }

    /* compiled from: ToolbarActionBar */
    private final class a implements l.a {
        private boolean b;

        a() {
        }

        public boolean a(MenuBuilder menuBuilder) {
            if (q.this.b == null) {
                return false;
            }
            q.this.b.onMenuOpened(108, menuBuilder);
            return true;
        }

        public void a(MenuBuilder menuBuilder, boolean z) {
            if (!this.b) {
                this.b = true;
                q.this.f58a.n();
                if (q.this.b != null) {
                    q.this.b.onPanelClosed(108, menuBuilder);
                }
                this.b = false;
            }
        }
    }

    /* compiled from: ToolbarActionBar */
    private final class b implements MenuBuilder.a {
        b() {
        }

        public boolean a(MenuBuilder menuBuilder, MenuItem menuItem) {
            return false;
        }

        public void a(MenuBuilder menuBuilder) {
            if (q.this.b == null) {
                return;
            }
            if (q.this.f58a.i()) {
                q.this.b.onPanelClosed(108, menuBuilder);
            } else if (q.this.b.onPreparePanel(0, null, menuBuilder)) {
                q.this.b.onMenuOpened(108, menuBuilder);
            }
        }
    }
}
