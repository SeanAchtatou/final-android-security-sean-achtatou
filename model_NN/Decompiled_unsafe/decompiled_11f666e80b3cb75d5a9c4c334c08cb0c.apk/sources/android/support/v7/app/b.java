package android.support.v7.app;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.a.a;
import android.support.v7.app.a;
import android.util.TypedValue;
import android.view.ContextThemeWrapper;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ListAdapter;

/* compiled from: AlertDialog */
public class b extends k implements DialogInterface {

    /* renamed from: a  reason: collision with root package name */
    final a f34a = new a(getContext(), this, getWindow());

    protected b(Context context, int i) {
        super(context, a(context, i));
    }

    static int a(Context context, int i) {
        if (i >= 16777216) {
            return i;
        }
        TypedValue typedValue = new TypedValue();
        context.getTheme().resolveAttribute(a.C0002a.alertDialogTheme, typedValue, true);
        return typedValue.resourceId;
    }

    public void setTitle(CharSequence charSequence) {
        super.setTitle(charSequence);
        this.f34a.a(charSequence);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.f34a.a();
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (this.f34a.a(i, keyEvent)) {
            return true;
        }
        return super.onKeyDown(i, keyEvent);
    }

    public boolean onKeyUp(int i, KeyEvent keyEvent) {
        if (this.f34a.b(i, keyEvent)) {
            return true;
        }
        return super.onKeyUp(i, keyEvent);
    }

    /* compiled from: AlertDialog */
    public static class a {

        /* renamed from: a  reason: collision with root package name */
        private final a.C0003a f35a;
        private final int b;

        public a(Context context) {
            this(context, b.a(context, 0));
        }

        public a(Context context, int i) {
            this.f35a = new a.C0003a(new ContextThemeWrapper(context, b.a(context, i)));
            this.b = i;
        }

        public Context a() {
            return this.f35a.f28a;
        }

        public a a(CharSequence charSequence) {
            this.f35a.f = charSequence;
            return this;
        }

        public a a(View view) {
            this.f35a.g = view;
            return this;
        }

        public a a(Drawable drawable) {
            this.f35a.d = drawable;
            return this;
        }

        public a a(DialogInterface.OnKeyListener onKeyListener) {
            this.f35a.r = onKeyListener;
            return this;
        }

        public a a(ListAdapter listAdapter, DialogInterface.OnClickListener onClickListener) {
            this.f35a.t = listAdapter;
            this.f35a.u = onClickListener;
            return this;
        }

        public b b() {
            b bVar = new b(this.f35a.f28a, this.b);
            this.f35a.a(bVar.f34a);
            bVar.setCancelable(this.f35a.o);
            if (this.f35a.o) {
                bVar.setCanceledOnTouchOutside(true);
            }
            bVar.setOnCancelListener(this.f35a.p);
            bVar.setOnDismissListener(this.f35a.q);
            if (this.f35a.r != null) {
                bVar.setOnKeyListener(this.f35a.r);
            }
            return bVar;
        }
    }
}
