package android.support.v7.app;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.view.b;
import android.support.v7.view.g;
import android.support.v7.view.i;
import android.support.v7.view.menu.MenuBuilder;
import android.support.v7.widget.ac;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.Window;
import java.lang.Thread;

/* compiled from: AppCompatDelegateImplBase */
abstract class e extends d {
    private static boolean m = true;
    private static final boolean n = (Build.VERSION.SDK_INT < 21);
    private static final int[] o = {16842836};

    /* renamed from: a  reason: collision with root package name */
    final Context f37a;
    final Window b;
    final Window.Callback c = this.b.getCallback();
    final Window.Callback d;
    final c e;
    ActionBar f;
    MenuInflater g;
    boolean h;
    boolean i;
    boolean j;
    boolean k;
    boolean l;
    private CharSequence p;
    private boolean q;
    private boolean r;

    /* access modifiers changed from: package-private */
    public abstract b a(b.a aVar);

    /* access modifiers changed from: package-private */
    public abstract void a(int i2, Menu menu);

    /* access modifiers changed from: package-private */
    public abstract boolean a(int i2, KeyEvent keyEvent);

    /* access modifiers changed from: package-private */
    public abstract boolean a(KeyEvent keyEvent);

    /* access modifiers changed from: package-private */
    public abstract void b(CharSequence charSequence);

    /* access modifiers changed from: package-private */
    public abstract boolean b(int i2, Menu menu);

    /* access modifiers changed from: package-private */
    public abstract void l();

    static {
        if (n && !m) {
            final Thread.UncaughtExceptionHandler defaultUncaughtExceptionHandler = Thread.getDefaultUncaughtExceptionHandler();
            Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
                public void uncaughtException(Thread thread, Throwable th) {
                    if (a(th)) {
                        Resources.NotFoundException notFoundException = new Resources.NotFoundException(th.getMessage() + ". If the resource you are trying to use is a vector resource, you may be referencing it in an unsupported way. See AppCompatDelegate.setCompatVectorFromResourcesEnabled() for more info.");
                        notFoundException.initCause(th.getCause());
                        notFoundException.setStackTrace(th.getStackTrace());
                        defaultUncaughtExceptionHandler.uncaughtException(thread, notFoundException);
                        return;
                    }
                    defaultUncaughtExceptionHandler.uncaughtException(thread, th);
                }

                private boolean a(Throwable th) {
                    String message;
                    if (!(th instanceof Resources.NotFoundException) || (message = th.getMessage()) == null) {
                        return false;
                    }
                    if (message.contains("drawable") || message.contains("Drawable")) {
                        return true;
                    }
                    return false;
                }
            });
        }
    }

    e(Context context, Window window, c cVar) {
        this.f37a = context;
        this.b = window;
        this.e = cVar;
        if (this.c instanceof a) {
            throw new IllegalStateException("AppCompat has already installed itself into the Window");
        }
        this.d = a(this.c);
        this.b.setCallback(this.d);
        ac a2 = ac.a(context, (AttributeSet) null, o);
        Drawable b2 = a2.b(0);
        if (b2 != null) {
            this.b.setBackgroundDrawable(b2);
        }
        a2.a();
    }

    /* access modifiers changed from: package-private */
    public Window.Callback a(Window.Callback callback) {
        return new a(callback);
    }

    public ActionBar a() {
        l();
        return this.f;
    }

    /* access modifiers changed from: package-private */
    public final ActionBar m() {
        return this.f;
    }

    public MenuInflater b() {
        if (this.g == null) {
            l();
            this.g = new g(this.f != null ? this.f.c() : this.f37a);
        }
        return this.g;
    }

    /* access modifiers changed from: package-private */
    public final Context n() {
        Context context = null;
        ActionBar a2 = a();
        if (a2 != null) {
            context = a2.c();
        }
        if (context == null) {
            return this.f37a;
        }
        return context;
    }

    public void c() {
        this.q = true;
    }

    public void d() {
        this.q = false;
    }

    public void g() {
        this.r = true;
    }

    public boolean o() {
        return false;
    }

    public boolean i() {
        return false;
    }

    /* access modifiers changed from: package-private */
    public final boolean p() {
        return this.r;
    }

    /* access modifiers changed from: package-private */
    public final Window.Callback q() {
        return this.b.getCallback();
    }

    public final void a(CharSequence charSequence) {
        this.p = charSequence;
        b(charSequence);
    }

    public void c(Bundle bundle) {
    }

    /* access modifiers changed from: package-private */
    public final CharSequence r() {
        if (this.c instanceof Activity) {
            return ((Activity) this.c).getTitle();
        }
        return this.p;
    }

    /* compiled from: AppCompatDelegateImplBase */
    class a extends i {
        a(Window.Callback callback) {
            super(callback);
        }

        public boolean dispatchKeyEvent(KeyEvent keyEvent) {
            return e.this.a(keyEvent) || super.dispatchKeyEvent(keyEvent);
        }

        public boolean dispatchKeyShortcutEvent(KeyEvent keyEvent) {
            return super.dispatchKeyShortcutEvent(keyEvent) || e.this.a(keyEvent.getKeyCode(), keyEvent);
        }

        public boolean onCreatePanelMenu(int i, Menu menu) {
            if (i != 0 || (menu instanceof MenuBuilder)) {
                return super.onCreatePanelMenu(i, menu);
            }
            return false;
        }

        public void onContentChanged() {
        }

        public boolean onPreparePanel(int i, View view, Menu menu) {
            MenuBuilder menuBuilder;
            if (menu instanceof MenuBuilder) {
                menuBuilder = (MenuBuilder) menu;
            } else {
                menuBuilder = null;
            }
            if (i == 0 && menuBuilder == null) {
                return false;
            }
            if (menuBuilder != null) {
                menuBuilder.c(true);
            }
            boolean onPreparePanel = super.onPreparePanel(i, view, menu);
            if (menuBuilder == null) {
                return onPreparePanel;
            }
            menuBuilder.c(false);
            return onPreparePanel;
        }

        public boolean onMenuOpened(int i, Menu menu) {
            super.onMenuOpened(i, menu);
            e.this.b(i, menu);
            return true;
        }

        public void onPanelClosed(int i, Menu menu) {
            super.onPanelClosed(i, menu);
            e.this.a(i, menu);
        }
    }
}
