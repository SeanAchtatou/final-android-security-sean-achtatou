package android.support.v7.app;

import android.content.Context;
import android.content.DialogInterface;
import android.content.res.TypedArray;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.a.a;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.ViewStub;
import android.view.Window;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckedTextView;
import android.widget.CursorAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import java.lang.ref.WeakReference;

/* compiled from: AlertController */
class a {
    private boolean A = false;
    private CharSequence B;
    private CharSequence C;
    private CharSequence D;
    private int E = 0;
    private Drawable F;
    private ImageView G;
    private TextView H;
    private TextView I;
    private View J;
    private int K;
    private int L;
    private int M = 0;
    private final View.OnClickListener N = new View.OnClickListener() {
        public void onClick(View view) {
            Message message;
            if (view == a.this.c && a.this.d != null) {
                message = Message.obtain(a.this.d);
            } else if (view == a.this.e && a.this.f != null) {
                message = Message.obtain(a.this.f);
            } else if (view != a.this.g || a.this.h == null) {
                message = null;
            } else {
                message = Message.obtain(a.this.h);
            }
            if (message != null) {
                message.sendToTarget();
            }
            a.this.p.obtainMessage(1, a.this.f22a).sendToTarget();
        }
    };

    /* renamed from: a  reason: collision with root package name */
    final k f22a;
    ListView b;
    Button c;
    Message d;
    Button e;
    Message f;
    Button g;
    Message h;
    NestedScrollView i;
    ListAdapter j;
    int k = -1;
    int l;
    int m;
    int n;
    int o;
    Handler p;
    private final Context q;
    private final Window r;
    private CharSequence s;
    private CharSequence t;
    private View u;
    private int v;
    private int w;
    private int x;
    private int y;
    private int z;

    /* compiled from: AlertController */
    private static final class b extends Handler {

        /* renamed from: a  reason: collision with root package name */
        private WeakReference<DialogInterface> f33a;

        public b(DialogInterface dialogInterface) {
            this.f33a = new WeakReference<>(dialogInterface);
        }

        public void handleMessage(Message message) {
            switch (message.what) {
                case -3:
                case -2:
                case -1:
                    ((DialogInterface.OnClickListener) message.obj).onClick(this.f33a.get(), message.what);
                    return;
                case 0:
                default:
                    return;
                case 1:
                    ((DialogInterface) message.obj).dismiss();
                    return;
            }
        }
    }

    public a(Context context, k kVar, Window window) {
        this.q = context;
        this.f22a = kVar;
        this.r = window;
        this.p = new b(kVar);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(null, a.k.AlertDialog, a.C0002a.alertDialogStyle, 0);
        this.K = obtainStyledAttributes.getResourceId(a.k.AlertDialog_android_layout, 0);
        this.L = obtainStyledAttributes.getResourceId(a.k.AlertDialog_buttonPanelSideLayout, 0);
        this.l = obtainStyledAttributes.getResourceId(a.k.AlertDialog_listLayout, 0);
        this.m = obtainStyledAttributes.getResourceId(a.k.AlertDialog_multiChoiceItemLayout, 0);
        this.n = obtainStyledAttributes.getResourceId(a.k.AlertDialog_singleChoiceItemLayout, 0);
        this.o = obtainStyledAttributes.getResourceId(a.k.AlertDialog_listItemLayout, 0);
        obtainStyledAttributes.recycle();
        kVar.a(1);
    }

    static boolean a(View view) {
        if (view.onCheckIsTextEditor()) {
            return true;
        }
        if (!(view instanceof ViewGroup)) {
            return false;
        }
        ViewGroup viewGroup = (ViewGroup) view;
        int childCount = viewGroup.getChildCount();
        while (childCount > 0) {
            childCount--;
            if (a(viewGroup.getChildAt(childCount))) {
                return true;
            }
        }
        return false;
    }

    public void a() {
        this.f22a.setContentView(b());
        c();
    }

    private int b() {
        if (this.L == 0) {
            return this.K;
        }
        if (this.M == 1) {
            return this.L;
        }
        return this.K;
    }

    public void a(CharSequence charSequence) {
        this.s = charSequence;
        if (this.H != null) {
            this.H.setText(charSequence);
        }
    }

    public void b(View view) {
        this.J = view;
    }

    public void b(CharSequence charSequence) {
        this.t = charSequence;
        if (this.I != null) {
            this.I.setText(charSequence);
        }
    }

    public void a(int i2) {
        this.u = null;
        this.v = i2;
        this.A = false;
    }

    public void c(View view) {
        this.u = view;
        this.v = 0;
        this.A = false;
    }

    public void a(View view, int i2, int i3, int i4, int i5) {
        this.u = view;
        this.v = 0;
        this.A = true;
        this.w = i2;
        this.x = i3;
        this.y = i4;
        this.z = i5;
    }

    public void a(int i2, CharSequence charSequence, DialogInterface.OnClickListener onClickListener, Message message) {
        if (message == null && onClickListener != null) {
            message = this.p.obtainMessage(i2, onClickListener);
        }
        switch (i2) {
            case -3:
                this.D = charSequence;
                this.h = message;
                return;
            case -2:
                this.C = charSequence;
                this.f = message;
                return;
            case -1:
                this.B = charSequence;
                this.d = message;
                return;
            default:
                throw new IllegalArgumentException("Button does not exist");
        }
    }

    public void b(int i2) {
        this.F = null;
        this.E = i2;
        if (this.G == null) {
            return;
        }
        if (i2 != 0) {
            this.G.setVisibility(0);
            this.G.setImageResource(this.E);
            return;
        }
        this.G.setVisibility(8);
    }

    public void a(Drawable drawable) {
        this.F = drawable;
        this.E = 0;
        if (this.G == null) {
            return;
        }
        if (drawable != null) {
            this.G.setVisibility(0);
            this.G.setImageDrawable(drawable);
            return;
        }
        this.G.setVisibility(8);
    }

    public int c(int i2) {
        TypedValue typedValue = new TypedValue();
        this.q.getTheme().resolveAttribute(i2, typedValue, true);
        return typedValue.resourceId;
    }

    public boolean a(int i2, KeyEvent keyEvent) {
        return this.i != null && this.i.executeKeyEvent(keyEvent);
    }

    public boolean b(int i2, KeyEvent keyEvent) {
        return this.i != null && this.i.executeKeyEvent(keyEvent);
    }

    private ViewGroup a(View view, View view2) {
        View view3;
        View view4;
        if (view == null) {
            if (view2 instanceof ViewStub) {
                view4 = ((ViewStub) view2).inflate();
            } else {
                view4 = view2;
            }
            return (ViewGroup) view4;
        }
        if (view2 != null) {
            ViewParent parent = view2.getParent();
            if (parent instanceof ViewGroup) {
                ((ViewGroup) parent).removeView(view2);
            }
        }
        if (view instanceof ViewStub) {
            view3 = ((ViewStub) view).inflate();
        } else {
            view3 = view;
        }
        return (ViewGroup) view3;
    }

    private void c() {
        boolean z2;
        boolean z3;
        int i2;
        View findViewById;
        View findViewById2 = this.r.findViewById(a.f.parentPanel);
        View findViewById3 = findViewById2.findViewById(a.f.topPanel);
        View findViewById4 = findViewById2.findViewById(a.f.contentPanel);
        View findViewById5 = findViewById2.findViewById(a.f.buttonPanel);
        ViewGroup viewGroup = (ViewGroup) findViewById2.findViewById(a.f.customPanel);
        a(viewGroup);
        View findViewById6 = viewGroup.findViewById(a.f.topPanel);
        View findViewById7 = viewGroup.findViewById(a.f.contentPanel);
        View findViewById8 = viewGroup.findViewById(a.f.buttonPanel);
        ViewGroup a2 = a(findViewById6, findViewById3);
        ViewGroup a3 = a(findViewById7, findViewById4);
        ViewGroup a4 = a(findViewById8, findViewById5);
        c(a3);
        d(a4);
        b(a2);
        boolean z4 = (viewGroup == null || viewGroup.getVisibility() == 8) ? false : true;
        if (a2 == null || a2.getVisibility() == 8) {
            z2 = false;
        } else {
            z2 = true;
        }
        if (a4 == null || a4.getVisibility() == 8) {
            z3 = false;
        } else {
            z3 = true;
        }
        if (!(z3 || a3 == null || (findViewById = a3.findViewById(a.f.textSpacerNoButtons)) == null)) {
            findViewById.setVisibility(0);
        }
        if (z2 && this.i != null) {
            this.i.setClipToPadding(true);
        }
        if (!z4) {
            ViewGroup viewGroup2 = this.b != null ? this.b : this.i;
            if (viewGroup2 != null) {
                if (z2) {
                    i2 = 1;
                } else {
                    i2 = 0;
                }
                a(a3, viewGroup2, (z3 ? 2 : 0) | i2, 3);
            }
        }
        ListView listView = this.b;
        if (listView != null && this.j != null) {
            listView.setAdapter(this.j);
            int i3 = this.k;
            if (i3 > -1) {
                listView.setItemChecked(i3, true);
                listView.setSelection(i3);
            }
        }
    }

    private void a(ViewGroup viewGroup, View view, int i2, int i3) {
        final View view2 = null;
        final View findViewById = this.r.findViewById(a.f.scrollIndicatorUp);
        View findViewById2 = this.r.findViewById(a.f.scrollIndicatorDown);
        if (Build.VERSION.SDK_INT >= 23) {
            ViewCompat.setScrollIndicators(view, i2, i3);
            if (findViewById != null) {
                viewGroup.removeView(findViewById);
            }
            if (findViewById2 != null) {
                viewGroup.removeView(findViewById2);
                return;
            }
            return;
        }
        if (findViewById != null && (i2 & 1) == 0) {
            viewGroup.removeView(findViewById);
            findViewById = null;
        }
        if (findViewById2 == null || (i2 & 2) != 0) {
            view2 = findViewById2;
        } else {
            viewGroup.removeView(findViewById2);
        }
        if (findViewById != null || view2 != null) {
            if (this.t != null) {
                this.i.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
                    public void onScrollChange(NestedScrollView nestedScrollView, int i, int i2, int i3, int i4) {
                        a.a(nestedScrollView, findViewById, view2);
                    }
                });
                this.i.post(new Runnable() {
                    public void run() {
                        a.a(a.this.i, findViewById, view2);
                    }
                });
            } else if (this.b != null) {
                this.b.setOnScrollListener(new AbsListView.OnScrollListener() {
                    public void onScrollStateChanged(AbsListView absListView, int i) {
                    }

                    public void onScroll(AbsListView absListView, int i, int i2, int i3) {
                        a.a(absListView, findViewById, view2);
                    }
                });
                this.b.post(new Runnable() {
                    public void run() {
                        a.a(a.this.b, findViewById, view2);
                    }
                });
            } else {
                if (findViewById != null) {
                    viewGroup.removeView(findViewById);
                }
                if (view2 != null) {
                    viewGroup.removeView(view2);
                }
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    private void a(ViewGroup viewGroup) {
        View view;
        boolean z2 = false;
        if (this.u != null) {
            view = this.u;
        } else if (this.v != 0) {
            view = LayoutInflater.from(this.q).inflate(this.v, viewGroup, false);
        } else {
            view = null;
        }
        if (view != null) {
            z2 = true;
        }
        if (!z2 || !a(view)) {
            this.r.setFlags(131072, 131072);
        }
        if (z2) {
            FrameLayout frameLayout = (FrameLayout) this.r.findViewById(a.f.custom);
            frameLayout.addView(view, new ViewGroup.LayoutParams(-1, -1));
            if (this.A) {
                frameLayout.setPadding(this.w, this.x, this.y, this.z);
            }
            if (this.b != null) {
                ((LinearLayout.LayoutParams) viewGroup.getLayoutParams()).weight = 0.0f;
                return;
            }
            return;
        }
        viewGroup.setVisibility(8);
    }

    private void b(ViewGroup viewGroup) {
        if (this.J != null) {
            viewGroup.addView(this.J, 0, new ViewGroup.LayoutParams(-1, -2));
            this.r.findViewById(a.f.title_template).setVisibility(8);
            return;
        }
        this.G = (ImageView) this.r.findViewById(16908294);
        if (!TextUtils.isEmpty(this.s)) {
            this.H = (TextView) this.r.findViewById(a.f.alertTitle);
            this.H.setText(this.s);
            if (this.E != 0) {
                this.G.setImageResource(this.E);
            } else if (this.F != null) {
                this.G.setImageDrawable(this.F);
            } else {
                this.H.setPadding(this.G.getPaddingLeft(), this.G.getPaddingTop(), this.G.getPaddingRight(), this.G.getPaddingBottom());
                this.G.setVisibility(8);
            }
        } else {
            this.r.findViewById(a.f.title_template).setVisibility(8);
            this.G.setVisibility(8);
            viewGroup.setVisibility(8);
        }
    }

    private void c(ViewGroup viewGroup) {
        this.i = (NestedScrollView) this.r.findViewById(a.f.scrollView);
        this.i.setFocusable(false);
        this.i.setNestedScrollingEnabled(false);
        this.I = (TextView) viewGroup.findViewById(16908299);
        if (this.I != null) {
            if (this.t != null) {
                this.I.setText(this.t);
                return;
            }
            this.I.setVisibility(8);
            this.i.removeView(this.I);
            if (this.b != null) {
                ViewGroup viewGroup2 = (ViewGroup) this.i.getParent();
                int indexOfChild = viewGroup2.indexOfChild(this.i);
                viewGroup2.removeViewAt(indexOfChild);
                viewGroup2.addView(this.b, indexOfChild, new ViewGroup.LayoutParams(-1, -1));
                return;
            }
            viewGroup.setVisibility(8);
        }
    }

    static void a(View view, View view2, View view3) {
        int i2 = 0;
        if (view2 != null) {
            view2.setVisibility(ViewCompat.canScrollVertically(view, -1) ? 0 : 4);
        }
        if (view3 != null) {
            if (!ViewCompat.canScrollVertically(view, 1)) {
                i2 = 4;
            }
            view3.setVisibility(i2);
        }
    }

    private void d(ViewGroup viewGroup) {
        boolean z2;
        boolean z3 = true;
        this.c = (Button) viewGroup.findViewById(16908313);
        this.c.setOnClickListener(this.N);
        if (TextUtils.isEmpty(this.B)) {
            this.c.setVisibility(8);
            z2 = false;
        } else {
            this.c.setText(this.B);
            this.c.setVisibility(0);
            z2 = true;
        }
        this.e = (Button) viewGroup.findViewById(16908314);
        this.e.setOnClickListener(this.N);
        if (TextUtils.isEmpty(this.C)) {
            this.e.setVisibility(8);
        } else {
            this.e.setText(this.C);
            this.e.setVisibility(0);
            z2 |= true;
        }
        this.g = (Button) viewGroup.findViewById(16908315);
        this.g.setOnClickListener(this.N);
        if (TextUtils.isEmpty(this.D)) {
            this.g.setVisibility(8);
        } else {
            this.g.setText(this.D);
            this.g.setVisibility(0);
            z2 |= true;
        }
        if (!z2) {
            z3 = false;
        }
        if (!z3) {
            viewGroup.setVisibility(8);
        }
    }

    /* renamed from: android.support.v7.app.a$a  reason: collision with other inner class name */
    /* compiled from: AlertController */
    public static class C0003a {
        public int A;
        public boolean B = false;
        public boolean[] C;
        public boolean D;
        public boolean E;
        public int F = -1;
        public DialogInterface.OnMultiChoiceClickListener G;
        public Cursor H;
        public String I;
        public String J;
        public AdapterView.OnItemSelectedListener K;
        public C0004a L;
        public boolean M = true;

        /* renamed from: a  reason: collision with root package name */
        public final Context f28a;
        public final LayoutInflater b;
        public int c = 0;
        public Drawable d;
        public int e = 0;
        public CharSequence f;
        public View g;
        public CharSequence h;
        public CharSequence i;
        public DialogInterface.OnClickListener j;
        public CharSequence k;
        public DialogInterface.OnClickListener l;
        public CharSequence m;
        public DialogInterface.OnClickListener n;
        public boolean o;
        public DialogInterface.OnCancelListener p;
        public DialogInterface.OnDismissListener q;
        public DialogInterface.OnKeyListener r;
        public CharSequence[] s;
        public ListAdapter t;
        public DialogInterface.OnClickListener u;
        public int v;
        public View w;
        public int x;
        public int y;
        public int z;

        /* renamed from: android.support.v7.app.a$a$a  reason: collision with other inner class name */
        /* compiled from: AlertController */
        public interface C0004a {
            void a(ListView listView);
        }

        public C0003a(Context context) {
            this.f28a = context;
            this.o = true;
            this.b = (LayoutInflater) context.getSystemService("layout_inflater");
        }

        public void a(a aVar) {
            if (this.g != null) {
                aVar.b(this.g);
            } else {
                if (this.f != null) {
                    aVar.a(this.f);
                }
                if (this.d != null) {
                    aVar.a(this.d);
                }
                if (this.c != 0) {
                    aVar.b(this.c);
                }
                if (this.e != 0) {
                    aVar.b(aVar.c(this.e));
                }
            }
            if (this.h != null) {
                aVar.b(this.h);
            }
            if (this.i != null) {
                aVar.a(-1, this.i, this.j, (Message) null);
            }
            if (this.k != null) {
                aVar.a(-2, this.k, this.l, (Message) null);
            }
            if (this.m != null) {
                aVar.a(-3, this.m, this.n, (Message) null);
            }
            if (!(this.s == null && this.H == null && this.t == null)) {
                b(aVar);
            }
            if (this.w != null) {
                if (this.B) {
                    aVar.a(this.w, this.x, this.y, this.z, this.A);
                    return;
                }
                aVar.c(this.w);
            } else if (this.v != 0) {
                aVar.a(this.v);
            }
        }

        private void b(final a aVar) {
            int i2;
            ListAdapter cVar;
            final ListView listView = (ListView) this.b.inflate(aVar.l, (ViewGroup) null);
            if (!this.D) {
                if (this.E) {
                    i2 = aVar.n;
                } else {
                    i2 = aVar.o;
                }
                if (this.H != null) {
                    cVar = new SimpleCursorAdapter(this.f28a, i2, this.H, new String[]{this.I}, new int[]{16908308});
                } else if (this.t != null) {
                    cVar = this.t;
                } else {
                    cVar = new c(this.f28a, i2, 16908308, this.s);
                }
            } else if (this.H == null) {
                cVar = new ArrayAdapter<CharSequence>(this.f28a, aVar.m, 16908308, this.s) {
                    public View getView(int i, View view, ViewGroup viewGroup) {
                        View view2 = super.getView(i, view, viewGroup);
                        if (C0003a.this.C != null && C0003a.this.C[i]) {
                            listView.setItemChecked(i, true);
                        }
                        return view2;
                    }
                };
            } else {
                final a aVar2 = aVar;
                cVar = new CursorAdapter(this.f28a, this.H, false) {
                    private final int d;
                    private final int e;

                    {
                        Cursor cursor = getCursor();
                        this.d = cursor.getColumnIndexOrThrow(C0003a.this.I);
                        this.e = cursor.getColumnIndexOrThrow(C0003a.this.J);
                    }

                    public void bindView(View view, Context context, Cursor cursor) {
                        ((CheckedTextView) view.findViewById(16908308)).setText(cursor.getString(this.d));
                        listView.setItemChecked(cursor.getPosition(), cursor.getInt(this.e) == 1);
                    }

                    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
                     arg types: [int, android.view.ViewGroup, int]
                     candidates:
                      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
                      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
                    public View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
                        return C0003a.this.b.inflate(aVar2.m, viewGroup, false);
                    }
                };
            }
            if (this.L != null) {
                this.L.a(listView);
            }
            aVar.j = cVar;
            aVar.k = this.F;
            if (this.u != null) {
                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
                        C0003a.this.u.onClick(aVar.f22a, i);
                        if (!C0003a.this.E) {
                            aVar.f22a.dismiss();
                        }
                    }
                });
            } else if (this.G != null) {
                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
                        if (C0003a.this.C != null) {
                            C0003a.this.C[i] = listView.isItemChecked(i);
                        }
                        C0003a.this.G.onClick(aVar.f22a, i, listView.isItemChecked(i));
                    }
                });
            }
            if (this.K != null) {
                listView.setOnItemSelectedListener(this.K);
            }
            if (this.E) {
                listView.setChoiceMode(1);
            } else if (this.D) {
                listView.setChoiceMode(2);
            }
            aVar.b = listView;
        }
    }

    /* compiled from: AlertController */
    private static class c extends ArrayAdapter<CharSequence> {
        public c(Context context, int i, int i2, CharSequence[] charSequenceArr) {
            super(context, i, i2, charSequenceArr);
        }

        public boolean hasStableIds() {
            return true;
        }

        public long getItemId(int i) {
            return (long) i;
        }
    }
}
