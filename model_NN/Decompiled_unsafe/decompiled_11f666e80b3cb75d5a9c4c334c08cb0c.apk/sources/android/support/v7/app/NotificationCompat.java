package android.support.v7.app;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.res.ColorStateList;
import android.os.Build;
import android.support.v4.app.NotificationBuilderWithBuilderAccessor;
import android.support.v4.app.NotificationCompat;
import android.support.v4.media.session.MediaSessionCompat;
import android.support.v4.text.BidiFormatter;
import android.support.v4.view.ViewCompat;
import android.support.v7.a.a;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.TextAppearanceSpan;
import android.widget.RemoteViews;
import java.util.ArrayList;
import java.util.List;

public class NotificationCompat extends android.support.v4.app.NotificationCompat {

    public static class DecoratedCustomViewStyle extends NotificationCompat.Style {
    }

    public static class DecoratedMediaCustomViewStyle extends MediaStyle {
    }

    public static class MediaStyle extends NotificationCompat.Style {

        /* renamed from: a  reason: collision with root package name */
        int[] f21a = null;
        MediaSessionCompat.Token b;
        boolean c;
        PendingIntent d;
    }

    /* access modifiers changed from: private */
    public static void e(NotificationBuilderWithBuilderAccessor notificationBuilderWithBuilderAccessor, NotificationCompat.Builder builder) {
        if (builder.mStyle instanceof DecoratedCustomViewStyle) {
            n.a(notificationBuilderWithBuilderAccessor);
        } else if (builder.mStyle instanceof DecoratedMediaCustomViewStyle) {
            n.b(notificationBuilderWithBuilderAccessor);
        } else if (!(builder.mStyle instanceof NotificationCompat.MessagingStyle)) {
            f(notificationBuilderWithBuilderAccessor, builder);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.app.o.a(android.support.v4.app.NotificationBuilderWithBuilderAccessor, android.content.Context, java.lang.CharSequence, java.lang.CharSequence, java.lang.CharSequence, int, android.graphics.Bitmap, java.lang.CharSequence, boolean, long, int, java.util.List, int[], boolean, android.app.PendingIntent, boolean):android.widget.RemoteViews
     arg types: [android.support.v4.app.NotificationBuilderWithBuilderAccessor, android.content.Context, java.lang.CharSequence, java.lang.CharSequence, java.lang.CharSequence, int, android.graphics.Bitmap, java.lang.CharSequence, boolean, long, int, java.util.ArrayList<android.support.v4.app.NotificationCompat$Action>, int[], int, ?[OBJECT, ARRAY], boolean]
     candidates:
      android.support.v7.app.o.a(android.app.Notification, android.content.Context, java.lang.CharSequence, java.lang.CharSequence, java.lang.CharSequence, int, android.graphics.Bitmap, java.lang.CharSequence, boolean, long, int, int, java.util.List, boolean, android.app.PendingIntent, boolean):void
      android.support.v7.app.o.a(android.support.v4.app.NotificationBuilderWithBuilderAccessor, android.content.Context, java.lang.CharSequence, java.lang.CharSequence, java.lang.CharSequence, int, android.graphics.Bitmap, java.lang.CharSequence, boolean, long, int, java.util.List, int[], boolean, android.app.PendingIntent, boolean):android.widget.RemoteViews */
    /* access modifiers changed from: private */
    public static RemoteViews f(NotificationBuilderWithBuilderAccessor notificationBuilderWithBuilderAccessor, NotificationCompat.Builder builder) {
        if (builder.mStyle instanceof MediaStyle) {
            MediaStyle mediaStyle = (MediaStyle) builder.mStyle;
            m.a(notificationBuilderWithBuilderAccessor, mediaStyle.f21a, mediaStyle.b != null ? mediaStyle.b.getToken() : null);
            boolean z = builder.getContentView() != null;
            boolean z2 = z || ((Build.VERSION.SDK_INT >= 21 && Build.VERSION.SDK_INT <= 23) && builder.getBigContentView() != null);
            if (!(builder.mStyle instanceof DecoratedMediaCustomViewStyle) || !z2) {
                return null;
            }
            RemoteViews a2 = o.a(notificationBuilderWithBuilderAccessor, builder.mContext, builder.mContentTitle, builder.mContentText, builder.mContentInfo, builder.mNumber, builder.mLargeIcon, builder.mSubText, builder.mUseChronometer, builder.getWhenIfShowing(), builder.getPriority(), (List) builder.mActions, mediaStyle.f21a, false, (PendingIntent) null, z);
            if (z) {
                o.a(builder.mContext, a2, builder.getContentView());
            }
            a(builder.mContext, a2, builder.getColor());
            return a2;
        } else if (builder.mStyle instanceof DecoratedCustomViewStyle) {
            return a(builder);
        } else {
            return g(notificationBuilderWithBuilderAccessor, builder);
        }
    }

    /* access modifiers changed from: private */
    public static RemoteViews g(NotificationBuilderWithBuilderAccessor notificationBuilderWithBuilderAccessor, NotificationCompat.Builder builder) {
        if (builder.mStyle instanceof NotificationCompat.MessagingStyle) {
            a((NotificationCompat.MessagingStyle) builder.mStyle, notificationBuilderWithBuilderAccessor, builder);
        }
        return h(notificationBuilderWithBuilderAccessor, builder);
    }

    /* access modifiers changed from: private */
    public static NotificationCompat.MessagingStyle.Message b(NotificationCompat.MessagingStyle messagingStyle) {
        List<NotificationCompat.MessagingStyle.Message> messages = messagingStyle.getMessages();
        for (int size = messages.size() - 1; size >= 0; size--) {
            NotificationCompat.MessagingStyle.Message message = messages.get(size);
            if (!TextUtils.isEmpty(message.getSender())) {
                return message;
            }
        }
        if (!messages.isEmpty()) {
            return messages.get(messages.size() - 1);
        }
        return null;
    }

    /* access modifiers changed from: private */
    public static CharSequence b(NotificationCompat.Builder builder, NotificationCompat.MessagingStyle messagingStyle, NotificationCompat.MessagingStyle.Message message) {
        int i;
        CharSequence charSequence;
        CharSequence userDisplayName;
        BidiFormatter instance = BidiFormatter.getInstance();
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder();
        boolean z = Build.VERSION.SDK_INT >= 21;
        int i2 = (z || Build.VERSION.SDK_INT <= 10) ? ViewCompat.MEASURED_STATE_MASK : -1;
        CharSequence sender = message.getSender();
        if (TextUtils.isEmpty(message.getSender())) {
            if (messagingStyle.getUserDisplayName() == null) {
                userDisplayName = "";
            } else {
                userDisplayName = messagingStyle.getUserDisplayName();
            }
            if (z && builder.getColor() != 0) {
                i2 = builder.getColor();
            }
            CharSequence charSequence2 = userDisplayName;
            i = i2;
            charSequence = charSequence2;
        } else {
            CharSequence charSequence3 = sender;
            i = i2;
            charSequence = charSequence3;
        }
        CharSequence unicodeWrap = instance.unicodeWrap(charSequence);
        spannableStringBuilder.append(unicodeWrap);
        spannableStringBuilder.setSpan(a(i), spannableStringBuilder.length() - unicodeWrap.length(), spannableStringBuilder.length(), 33);
        spannableStringBuilder.append((CharSequence) "  ").append(instance.unicodeWrap(message.getText() == null ? "" : message.getText()));
        return spannableStringBuilder;
    }

    private static TextAppearanceSpan a(int i) {
        return new TextAppearanceSpan(null, 0, 0, ColorStateList.valueOf(i), null);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.text.SpannableStringBuilder.insert(int, java.lang.CharSequence):android.text.SpannableStringBuilder}
     arg types: [int, java.lang.String]
     candidates:
      ClspMth{android.text.SpannableStringBuilder.insert(int, java.lang.CharSequence):android.text.Editable}
      ClspMth{android.text.SpannableStringBuilder.insert(int, java.lang.CharSequence):android.text.SpannableStringBuilder} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.text.SpannableStringBuilder.insert(int, java.lang.CharSequence):android.text.SpannableStringBuilder}
     arg types: [int, java.lang.CharSequence]
     candidates:
      ClspMth{android.text.SpannableStringBuilder.insert(int, java.lang.CharSequence):android.text.Editable}
      ClspMth{android.text.SpannableStringBuilder.insert(int, java.lang.CharSequence):android.text.SpannableStringBuilder} */
    private static void a(NotificationCompat.MessagingStyle messagingStyle, NotificationBuilderWithBuilderAccessor notificationBuilderWithBuilderAccessor, NotificationCompat.Builder builder) {
        boolean z;
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder();
        List<NotificationCompat.MessagingStyle.Message> messages = messagingStyle.getMessages();
        if (messagingStyle.getConversationTitle() != null || a(messagingStyle.getMessages())) {
            z = true;
        } else {
            z = false;
        }
        for (int size = messages.size() - 1; size >= 0; size--) {
            NotificationCompat.MessagingStyle.Message message = messages.get(size);
            CharSequence b2 = z ? b(builder, messagingStyle, message) : message.getText();
            if (size != messages.size() - 1) {
                spannableStringBuilder.insert(0, (CharSequence) "\n");
            }
            spannableStringBuilder.insert(0, b2);
        }
        p.a(notificationBuilderWithBuilderAccessor, spannableStringBuilder);
    }

    private static boolean a(List<NotificationCompat.MessagingStyle.Message> list) {
        for (int size = list.size() - 1; size >= 0; size--) {
            if (list.get(size).getSender() == null) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: private */
    public static RemoteViews h(NotificationBuilderWithBuilderAccessor notificationBuilderWithBuilderAccessor, NotificationCompat.Builder builder) {
        if (builder.mStyle instanceof MediaStyle) {
            MediaStyle mediaStyle = (MediaStyle) builder.mStyle;
            boolean z = (builder.mStyle instanceof DecoratedMediaCustomViewStyle) && builder.getContentView() != null;
            RemoteViews a2 = o.a(notificationBuilderWithBuilderAccessor, builder.mContext, builder.mContentTitle, builder.mContentText, builder.mContentInfo, builder.mNumber, builder.mLargeIcon, builder.mSubText, builder.mUseChronometer, builder.getWhenIfShowing(), builder.getPriority(), builder.mActions, mediaStyle.f21a, mediaStyle.c, mediaStyle.d, z);
            if (z) {
                o.a(builder.mContext, a2, builder.getContentView());
                return a2;
            }
        } else if (builder.mStyle instanceof DecoratedCustomViewStyle) {
            return a(builder);
        }
        return null;
    }

    /* access modifiers changed from: private */
    public static void d(Notification notification, NotificationCompat.Builder builder) {
        RemoteViews contentView;
        if (builder.mStyle instanceof MediaStyle) {
            MediaStyle mediaStyle = (MediaStyle) builder.mStyle;
            if (builder.getBigContentView() != null) {
                contentView = builder.getBigContentView();
            } else {
                contentView = builder.getContentView();
            }
            boolean z = (builder.mStyle instanceof DecoratedMediaCustomViewStyle) && contentView != null;
            o.a(notification, builder.mContext, builder.mContentTitle, builder.mContentText, builder.mContentInfo, builder.mNumber, builder.mLargeIcon, builder.mSubText, builder.mUseChronometer, builder.getWhenIfShowing(), builder.getPriority(), 0, builder.mActions, mediaStyle.c, mediaStyle.d, z);
            if (z) {
                o.a(builder.mContext, notification.bigContentView, contentView);
            }
        } else if (builder.mStyle instanceof DecoratedCustomViewStyle) {
            e(notification, builder);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.app.o.a(android.content.Context, java.lang.CharSequence, java.lang.CharSequence, java.lang.CharSequence, int, int, android.graphics.Bitmap, java.lang.CharSequence, boolean, long, int, int, int, boolean, java.util.ArrayList<android.support.v4.app.NotificationCompat$Action>):android.widget.RemoteViews
     arg types: [android.content.Context, java.lang.CharSequence, java.lang.CharSequence, java.lang.CharSequence, int, int, android.graphics.Bitmap, java.lang.CharSequence, boolean, long, int, int, int, int, ?[OBJECT, ARRAY]]
     candidates:
      android.support.v7.app.o.a(android.content.Context, java.lang.CharSequence, java.lang.CharSequence, java.lang.CharSequence, int, android.graphics.Bitmap, java.lang.CharSequence, boolean, long, int, int, java.util.List, boolean, android.app.PendingIntent, boolean):android.widget.RemoteViews
      android.support.v7.app.o.a(android.content.Context, java.lang.CharSequence, java.lang.CharSequence, java.lang.CharSequence, int, android.graphics.Bitmap, java.lang.CharSequence, boolean, long, int, java.util.List, int[], boolean, android.app.PendingIntent, boolean):android.widget.RemoteViews
      android.support.v7.app.o.a(android.content.Context, java.lang.CharSequence, java.lang.CharSequence, java.lang.CharSequence, int, int, android.graphics.Bitmap, java.lang.CharSequence, boolean, long, int, int, int, boolean, java.util.ArrayList<android.support.v4.app.NotificationCompat$Action>):android.widget.RemoteViews */
    private static RemoteViews a(NotificationCompat.Builder builder) {
        if (builder.getContentView() == null) {
            return null;
        }
        RemoteViews a2 = o.a(builder.mContext, builder.mContentTitle, builder.mContentText, builder.mContentInfo, builder.mNumber, builder.mNotification.icon, builder.mLargeIcon, builder.mSubText, builder.mUseChronometer, builder.getWhenIfShowing(), builder.getPriority(), builder.getColor(), a.h.notification_template_custom_big, false, (ArrayList<NotificationCompat.Action>) null);
        o.a(builder.mContext, a2, builder.getContentView());
        return a2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.app.o.a(android.content.Context, java.lang.CharSequence, java.lang.CharSequence, java.lang.CharSequence, int, int, android.graphics.Bitmap, java.lang.CharSequence, boolean, long, int, int, int, boolean, java.util.ArrayList<android.support.v4.app.NotificationCompat$Action>):android.widget.RemoteViews
     arg types: [android.content.Context, java.lang.CharSequence, java.lang.CharSequence, java.lang.CharSequence, int, int, android.graphics.Bitmap, java.lang.CharSequence, boolean, long, int, int, int, int, java.util.ArrayList<android.support.v4.app.NotificationCompat$Action>]
     candidates:
      android.support.v7.app.o.a(android.content.Context, java.lang.CharSequence, java.lang.CharSequence, java.lang.CharSequence, int, android.graphics.Bitmap, java.lang.CharSequence, boolean, long, int, int, java.util.List, boolean, android.app.PendingIntent, boolean):android.widget.RemoteViews
      android.support.v7.app.o.a(android.content.Context, java.lang.CharSequence, java.lang.CharSequence, java.lang.CharSequence, int, android.graphics.Bitmap, java.lang.CharSequence, boolean, long, int, java.util.List, int[], boolean, android.app.PendingIntent, boolean):android.widget.RemoteViews
      android.support.v7.app.o.a(android.content.Context, java.lang.CharSequence, java.lang.CharSequence, java.lang.CharSequence, int, int, android.graphics.Bitmap, java.lang.CharSequence, boolean, long, int, int, int, boolean, java.util.ArrayList<android.support.v4.app.NotificationCompat$Action>):android.widget.RemoteViews */
    private static void e(Notification notification, NotificationCompat.Builder builder) {
        RemoteViews bigContentView = builder.getBigContentView();
        if (bigContentView == null) {
            bigContentView = builder.getContentView();
        }
        if (bigContentView != null) {
            RemoteViews a2 = o.a(builder.mContext, builder.mContentTitle, builder.mContentText, builder.mContentInfo, builder.mNumber, notification.icon, builder.mLargeIcon, builder.mSubText, builder.mUseChronometer, builder.getWhenIfShowing(), builder.getPriority(), builder.getColor(), a.h.notification_template_custom_big, false, builder.mActions);
            o.a(builder.mContext, a2, bigContentView);
            notification.bigContentView = a2;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.app.o.a(android.content.Context, java.lang.CharSequence, java.lang.CharSequence, java.lang.CharSequence, int, int, android.graphics.Bitmap, java.lang.CharSequence, boolean, long, int, int, int, boolean, java.util.ArrayList<android.support.v4.app.NotificationCompat$Action>):android.widget.RemoteViews
     arg types: [android.content.Context, java.lang.CharSequence, java.lang.CharSequence, java.lang.CharSequence, int, int, android.graphics.Bitmap, java.lang.CharSequence, boolean, long, int, int, int, int, java.util.ArrayList<android.support.v4.app.NotificationCompat$Action>]
     candidates:
      android.support.v7.app.o.a(android.content.Context, java.lang.CharSequence, java.lang.CharSequence, java.lang.CharSequence, int, android.graphics.Bitmap, java.lang.CharSequence, boolean, long, int, int, java.util.List, boolean, android.app.PendingIntent, boolean):android.widget.RemoteViews
      android.support.v7.app.o.a(android.content.Context, java.lang.CharSequence, java.lang.CharSequence, java.lang.CharSequence, int, android.graphics.Bitmap, java.lang.CharSequence, boolean, long, int, java.util.List, int[], boolean, android.app.PendingIntent, boolean):android.widget.RemoteViews
      android.support.v7.app.o.a(android.content.Context, java.lang.CharSequence, java.lang.CharSequence, java.lang.CharSequence, int, int, android.graphics.Bitmap, java.lang.CharSequence, boolean, long, int, int, int, boolean, java.util.ArrayList<android.support.v4.app.NotificationCompat$Action>):android.widget.RemoteViews */
    private static void f(Notification notification, NotificationCompat.Builder builder) {
        RemoteViews headsUpContentView = builder.getHeadsUpContentView();
        RemoteViews contentView = headsUpContentView != null ? headsUpContentView : builder.getContentView();
        if (headsUpContentView != null) {
            RemoteViews a2 = o.a(builder.mContext, builder.mContentTitle, builder.mContentText, builder.mContentInfo, builder.mNumber, notification.icon, builder.mLargeIcon, builder.mSubText, builder.mUseChronometer, builder.getWhenIfShowing(), builder.getPriority(), builder.getColor(), a.h.notification_template_custom_big, false, builder.mActions);
            o.a(builder.mContext, a2, contentView);
            notification.headsUpContentView = a2;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.app.o.a(android.app.Notification, android.content.Context, java.lang.CharSequence, java.lang.CharSequence, java.lang.CharSequence, int, android.graphics.Bitmap, java.lang.CharSequence, boolean, long, int, int, java.util.List, boolean, android.app.PendingIntent, boolean):void
     arg types: [android.app.Notification, android.content.Context, java.lang.CharSequence, java.lang.CharSequence, java.lang.CharSequence, int, android.graphics.Bitmap, java.lang.CharSequence, boolean, long, int, int, java.util.ArrayList<android.support.v4.app.NotificationCompat$Action>, int, ?[OBJECT, ARRAY], int]
     candidates:
      android.support.v7.app.o.a(android.support.v4.app.NotificationBuilderWithBuilderAccessor, android.content.Context, java.lang.CharSequence, java.lang.CharSequence, java.lang.CharSequence, int, android.graphics.Bitmap, java.lang.CharSequence, boolean, long, int, java.util.List, int[], boolean, android.app.PendingIntent, boolean):android.widget.RemoteViews
      android.support.v7.app.o.a(android.app.Notification, android.content.Context, java.lang.CharSequence, java.lang.CharSequence, java.lang.CharSequence, int, android.graphics.Bitmap, java.lang.CharSequence, boolean, long, int, int, java.util.List, boolean, android.app.PendingIntent, boolean):void */
    /* access modifiers changed from: private */
    public static void g(Notification notification, NotificationCompat.Builder builder) {
        RemoteViews contentView;
        if (builder.getBigContentView() != null) {
            contentView = builder.getBigContentView();
        } else {
            contentView = builder.getContentView();
        }
        if ((builder.mStyle instanceof DecoratedMediaCustomViewStyle) && contentView != null) {
            o.a(notification, builder.mContext, builder.mContentTitle, builder.mContentText, builder.mContentInfo, builder.mNumber, builder.mLargeIcon, builder.mSubText, builder.mUseChronometer, builder.getWhenIfShowing(), builder.getPriority(), 0, (List) builder.mActions, false, (PendingIntent) null, true);
            o.a(builder.mContext, notification.bigContentView, contentView);
            a(builder.mContext, notification.bigContentView, builder.getColor());
        } else if (builder.mStyle instanceof DecoratedCustomViewStyle) {
            e(notification, builder);
        }
    }

    private static void a(Context context, RemoteViews remoteViews, int i) {
        if (i == 0) {
            i = context.getResources().getColor(a.c.notification_material_background_media_default_color);
        }
        remoteViews.setInt(a.f.status_bar_latest_event_content, "setBackgroundColor", i);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.app.o.a(android.content.Context, java.lang.CharSequence, java.lang.CharSequence, java.lang.CharSequence, int, android.graphics.Bitmap, java.lang.CharSequence, boolean, long, int, int, java.util.List, boolean, android.app.PendingIntent, boolean):android.widget.RemoteViews
     arg types: [android.content.Context, java.lang.CharSequence, java.lang.CharSequence, java.lang.CharSequence, int, android.graphics.Bitmap, java.lang.CharSequence, boolean, long, int, int, java.util.ArrayList<android.support.v4.app.NotificationCompat$Action>, int, ?[OBJECT, ARRAY], int]
     candidates:
      android.support.v7.app.o.a(android.content.Context, java.lang.CharSequence, java.lang.CharSequence, java.lang.CharSequence, int, int, android.graphics.Bitmap, java.lang.CharSequence, boolean, long, int, int, int, boolean, java.util.ArrayList<android.support.v4.app.NotificationCompat$Action>):android.widget.RemoteViews
      android.support.v7.app.o.a(android.content.Context, java.lang.CharSequence, java.lang.CharSequence, java.lang.CharSequence, int, android.graphics.Bitmap, java.lang.CharSequence, boolean, long, int, java.util.List, int[], boolean, android.app.PendingIntent, boolean):android.widget.RemoteViews
      android.support.v7.app.o.a(android.content.Context, java.lang.CharSequence, java.lang.CharSequence, java.lang.CharSequence, int, android.graphics.Bitmap, java.lang.CharSequence, boolean, long, int, int, java.util.List, boolean, android.app.PendingIntent, boolean):android.widget.RemoteViews */
    /* access modifiers changed from: private */
    public static void h(Notification notification, NotificationCompat.Builder builder) {
        RemoteViews contentView;
        if (builder.getHeadsUpContentView() != null) {
            contentView = builder.getHeadsUpContentView();
        } else {
            contentView = builder.getContentView();
        }
        if ((builder.mStyle instanceof DecoratedMediaCustomViewStyle) && contentView != null) {
            notification.headsUpContentView = o.a(builder.mContext, builder.mContentTitle, builder.mContentText, builder.mContentInfo, builder.mNumber, builder.mLargeIcon, builder.mSubText, builder.mUseChronometer, builder.getWhenIfShowing(), builder.getPriority(), 0, (List) builder.mActions, false, (PendingIntent) null, true);
            o.a(builder.mContext, notification.headsUpContentView, contentView);
            a(builder.mContext, notification.headsUpContentView, builder.getColor());
        } else if (builder.mStyle instanceof DecoratedCustomViewStyle) {
            f(notification, builder);
        }
    }

    public static class Builder extends NotificationCompat.Builder {
        /* access modifiers changed from: protected */
        public CharSequence resolveText() {
            if (this.mStyle instanceof NotificationCompat.MessagingStyle) {
                NotificationCompat.MessagingStyle messagingStyle = (NotificationCompat.MessagingStyle) this.mStyle;
                NotificationCompat.MessagingStyle.Message a2 = NotificationCompat.b(messagingStyle);
                CharSequence conversationTitle = messagingStyle.getConversationTitle();
                if (a2 != null) {
                    if (conversationTitle != null) {
                        return NotificationCompat.b(this, messagingStyle, a2);
                    }
                    return a2.getText();
                }
            }
            return super.resolveText();
        }

        /* access modifiers changed from: protected */
        public CharSequence resolveTitle() {
            if (this.mStyle instanceof NotificationCompat.MessagingStyle) {
                NotificationCompat.MessagingStyle messagingStyle = (NotificationCompat.MessagingStyle) this.mStyle;
                NotificationCompat.MessagingStyle.Message a2 = NotificationCompat.b(messagingStyle);
                CharSequence conversationTitle = messagingStyle.getConversationTitle();
                if (!(conversationTitle == null && a2 == null)) {
                    if (conversationTitle != null) {
                        return conversationTitle;
                    }
                    return a2.getSender();
                }
            }
            return super.resolveTitle();
        }

        /* access modifiers changed from: protected */
        public NotificationCompat.BuilderExtender getExtender() {
            if (Build.VERSION.SDK_INT >= 24) {
                return new a();
            }
            if (Build.VERSION.SDK_INT >= 21) {
                return new d();
            }
            if (Build.VERSION.SDK_INT >= 16) {
                return new c();
            }
            if (Build.VERSION.SDK_INT >= 14) {
                return new b();
            }
            return super.getExtender();
        }
    }

    private static class b extends NotificationCompat.BuilderExtender {
        b() {
        }

        public Notification build(NotificationCompat.Builder builder, NotificationBuilderWithBuilderAccessor notificationBuilderWithBuilderAccessor) {
            RemoteViews a2 = NotificationCompat.h(notificationBuilderWithBuilderAccessor, builder);
            Notification build = notificationBuilderWithBuilderAccessor.build();
            if (a2 != null) {
                build.contentView = a2;
            } else if (builder.getContentView() != null) {
                build.contentView = builder.getContentView();
            }
            return build;
        }
    }

    private static class c extends NotificationCompat.BuilderExtender {
        c() {
        }

        public Notification build(NotificationCompat.Builder builder, NotificationBuilderWithBuilderAccessor notificationBuilderWithBuilderAccessor) {
            RemoteViews b = NotificationCompat.g(notificationBuilderWithBuilderAccessor, builder);
            Notification build = notificationBuilderWithBuilderAccessor.build();
            if (b != null) {
                build.contentView = b;
            }
            NotificationCompat.d(build, builder);
            return build;
        }
    }

    private static class d extends NotificationCompat.BuilderExtender {
        d() {
        }

        public Notification build(NotificationCompat.Builder builder, NotificationBuilderWithBuilderAccessor notificationBuilderWithBuilderAccessor) {
            RemoteViews c = NotificationCompat.f(notificationBuilderWithBuilderAccessor, builder);
            Notification build = notificationBuilderWithBuilderAccessor.build();
            if (c != null) {
                build.contentView = c;
            }
            NotificationCompat.g(build, builder);
            NotificationCompat.h(build, builder);
            return build;
        }
    }

    private static class a extends NotificationCompat.BuilderExtender {
        private a() {
        }

        public Notification build(NotificationCompat.Builder builder, NotificationBuilderWithBuilderAccessor notificationBuilderWithBuilderAccessor) {
            NotificationCompat.e(notificationBuilderWithBuilderAccessor, builder);
            return notificationBuilderWithBuilderAccessor.build();
        }
    }
}
