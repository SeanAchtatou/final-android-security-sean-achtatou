package com.waps;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

public class r extends BroadcastReceiver {
    protected static boolean c = false;
    private static String d = "";
    int a;
    q b;
    private String e;
    private SharedPreferences f;
    private SharedPreferences.Editor g;

    public r(q qVar, int i, String str) {
        this.a = i;
        this.b = qVar;
        this.e = str;
    }

    public r(String str) {
        this.e = str;
    }

    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals("android.intent.action.PACKAGE_ADDED")) {
            d = intent.getDataString().substring(8);
            AppConnect.getInstanceNoConnect(context).package_receiver(d, 0);
            try {
                if (this.b != null) {
                    this.b.a(this.a, this.e);
                }
                context.startActivity(context.getPackageManager().getLaunchIntentForPackage(d));
                if (p.g) {
                    this.f = context.getSharedPreferences("DownLoadSave", 3);
                    this.g = this.f.edit();
                    this.g.putString(this.f.getAll().size() + "", d);
                    this.g.commit();
                    c = true;
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        if (intent.getAction().equals("android.intent.action.PACKAGE_REMOVED")) {
            AppConnect.getInstanceNoConnect(context).package_receiver(intent.getDataString().substring(8), 3);
        }
        context.unregisterReceiver(this);
    }
}
