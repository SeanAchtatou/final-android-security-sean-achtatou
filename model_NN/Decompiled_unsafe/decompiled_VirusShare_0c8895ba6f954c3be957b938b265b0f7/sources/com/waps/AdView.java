package com.waps;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.TranslateAnimation;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

public class AdView implements DisplayAdNotifier {
    static long e = 0;
    static int f = 30;
    LinearLayout a;
    boolean b = false;
    View c;
    Context d;
    String g;
    RelativeLayout h;
    WebView i;
    Button j;
    int k;
    RelativeLayout.LayoutParams l;
    RelativeLayout.LayoutParams m;
    final Handler mHandler = new Handler();
    final Runnable mUpdateResults = new b(this);
    RelativeLayout.LayoutParams n;
    boolean o = true;
    private int[] p;
    private int q;
    private AnimationType r;
    private int s = -1;
    /* access modifiers changed from: private */
    public String t;
    /* access modifiers changed from: private */
    public String u;
    /* access modifiers changed from: private */
    public String v;

    public AdView(Context context, LinearLayout linearLayout) {
        this.d = context;
        this.a = linearLayout;
        this.p = new int[]{0};
        this.q = 0;
        this.s = 0;
    }

    private void click() {
        getLayout();
        this.a.setOnTouchListener(new d(this));
    }

    private void getLayout() {
        this.k = ((Activity) this.d).getWindowManager().getDefaultDisplay().getHeight();
        this.l = new RelativeLayout.LayoutParams(-1, this.k - (this.k / 4));
        this.m = new RelativeLayout.LayoutParams(-1, this.k - (this.k / 4));
        this.n = new RelativeLayout.LayoutParams(-1, -2);
        this.h = new RelativeLayout(this.d);
        this.i = new WebView(this.d);
        this.i.setLayoutParams(this.l);
        this.i.loadUrl("http://www.baidu.com");
        this.j = new Button(this.d);
        this.j.setText("返回");
        this.j.setLayoutParams(this.n);
        this.j.setId(1);
        this.m.addRule(2, 1);
        this.n.addRule(12);
        this.h.addView(this.j, this.n);
        this.h.addView(this.i, this.m);
        RelativeLayout relativeLayout = this.h;
        RelativeLayout relativeLayout2 = this.h;
        relativeLayout.setVisibility(4);
        ((Activity) this.d).addContentView(this.h, this.l);
        this.j.setOnClickListener(new c(this));
    }

    /* access modifiers changed from: private */
    public void showAdsInfo() {
        this.h.clearAnimation();
        TranslateAnimation translateAnimation = new TranslateAnimation(0.0f, 0.0f, (float) this.k, (float) (this.k / 4));
        translateAnimation.setDuration(500);
        translateAnimation.setFillAfter(true);
        translateAnimation.setInterpolator(new DecelerateInterpolator());
        translateAnimation.start();
        this.h.setBackgroundColor(-1);
        this.h.setAnimation(translateAnimation);
    }

    /* access modifiers changed from: private */
    public void updateResultsInUi() {
        if (this.b) {
            this.a.removeAllViews();
            this.a.addView(this.c);
            if (this.a.getAnimation() != null) {
                this.a.clearAnimation();
                this.r = null;
            }
            if (this.s == 0) {
                this.r = new AnimationType(this.p);
            } else if (this.s == 1) {
                this.r = new AnimationType(this.q);
            } else if (this.s == 2) {
                this.r = new AnimationType(this.p);
            }
            this.r.startAnimation(this.a);
            this.b = false;
        }
    }

    public void DisplayAd() {
        DisplayAd(f);
    }

    public void DisplayAd(int i2) {
        showADS();
        e = System.currentTimeMillis();
        if (f < 20) {
            f = 20;
        }
        new a(this).start();
    }

    public void getDisplayAdResponse(View view) {
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        int width = ((Activity) this.d).getResources().getConfiguration().orientation == 1 ? ((Activity) this.d).getWindowManager().getDefaultDisplay().getWidth() : ((Activity) this.d).getResources().getConfiguration().orientation == 2 ? ((Activity) this.d).getWindowManager().getDefaultDisplay().getHeight() : 0;
        this.c = view;
        this.c.setLayoutParams(new ViewGroup.LayoutParams(width, (int) (((double) width) / (((double) layoutParams.width) / ((double) layoutParams.height)))));
        this.b = true;
        this.mHandler.post(this.mUpdateResults);
    }

    public void getDisplayAdResponseFailed(String str) {
        this.b = false;
        this.mHandler.post(this.mUpdateResults);
    }

    public AdView setAnimationType(int i2) {
        this.q = i2;
        this.s = 1;
        return this;
    }

    public AdView setAnimationType(int[] iArr) {
        this.p = iArr;
        this.s = 2;
        return this;
    }

    public void showADS() {
        AppConnect.getInstance(this.d).getDisplayAd(this);
    }
}
