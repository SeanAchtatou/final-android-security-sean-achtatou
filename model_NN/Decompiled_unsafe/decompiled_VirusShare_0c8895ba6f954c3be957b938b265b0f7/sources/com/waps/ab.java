package com.waps;

import android.content.DialogInterface;
import android.content.Intent;

class ab implements DialogInterface.OnClickListener {
    final /* synthetic */ OffersWebView a;

    ab(OffersWebView offersWebView) {
        this.a = offersWebView;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        Intent intent = new Intent();
        intent.setClass(this.a, OffersWebView.class);
        intent.putExtra("UrlPath", this.a.w);
        intent.putExtra("isFinshClose", "true");
        intent.putExtra("ACTIVITY_FLAG", "notify");
        intent.putExtra("offers_webview_tag", "OffersWebView");
        if (this.a.w.contains("down_type")) {
            intent.putExtra("Notify_Url_Params", this.a.z);
        }
        this.a.startActivity(intent);
        this.a.B.a(2);
        this.a.F.clear();
        this.a.F.commit();
        dialogInterface.cancel();
        this.a.finish();
    }
}
