package com.waps;

import android.os.AsyncTask;

class u extends AsyncTask {
    final /* synthetic */ DisplayAd a;

    private u(DisplayAd displayAd) {
        this.a = displayAd;
    }

    /* synthetic */ u(DisplayAd displayAd, t tVar) {
        this(displayAd);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Boolean doInBackground(Void... voidArr) {
        boolean z = false;
        String a2 = DisplayAd.f.a(this.a.h, this.a.j);
        if (a2 == null || a2.length() == 0) {
            DisplayAd.e.getDisplayAdResponseFailed("网络错误.");
        } else {
            z = this.a.buildResponse(a2);
            if (!z) {
                DisplayAd.e.getDisplayAdResponseFailed("无法显示广告内容.");
            }
        }
        return Boolean.valueOf(z);
    }
}
