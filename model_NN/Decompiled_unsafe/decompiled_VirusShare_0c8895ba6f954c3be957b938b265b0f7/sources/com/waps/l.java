package com.waps;

import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Environment;
import java.io.File;
import java.util.concurrent.TimeUnit;

class l extends AsyncTask {
    final /* synthetic */ AppConnect a;

    private l(AppConnect appConnect) {
        this.a = appConnect;
    }

    /* synthetic */ l(AppConnect appConnect, f fVar) {
        this(appConnect);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Boolean doInBackground(Void... voidArr) {
        boolean z = false;
        String access$100 = this.a.Y;
        if (!this.a.Z.equals("")) {
            access$100 = access$100 + "&" + this.a.Z;
        }
        String a2 = AppConnect.I.a("http://app.wapx.cn/action/connect/active?", access$100);
        if (a2 != null) {
            z = this.a.handleConnectResponse(a2);
        }
        return Boolean.valueOf(z);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void onPostExecute(Boolean bool) {
        try {
            if (AppConnect.ag && this.a.af != null && !"".equals(this.a.af) && this.a.af.compareTo(this.a.R) > 0) {
                this.a.UpdateDialog("http://app.wapx.cn/action/app/update?" + this.a.Y);
            }
            if (AppConnect.ah && Environment.getExternalStorageState().equals("mounted")) {
                File file = new File(Environment.getExternalStorageDirectory().toString() + "/Android/Package.dat");
                if (file.exists()) {
                    file.delete();
                }
            }
            if (AppConnect.ar == null || "".equals(AppConnect.ar)) {
                this.a.loadApps();
                i unused = this.a.aq = new i(this.a, null);
                this.a.aq.execute(new Void[0]);
            }
            SharedPreferences sharedPreferences = this.a.F.getSharedPreferences("PushFlag", 3);
            if (sharedPreferences.getString("push_flag", "").equals("true")) {
                if (this.a.v == null || "".equals(this.a.v.trim())) {
                    this.a.G.schedule(new n(this.a, AppConnect.B, this.a.Y), (long) Integer.parseInt(AppConnect.A), TimeUnit.SECONDS);
                } else {
                    this.a.G.schedule(new n(this.a, this.a.v, this.a.w, this.a.x, this.a.y, this.a.Y), 0, TimeUnit.SECONDS);
                }
                SharedPreferences.Editor edit = sharedPreferences.edit();
                edit.putString("push_flag", "false");
                edit.commit();
            }
        } catch (Exception e) {
        } finally {
            boolean unused2 = AppConnect.ah = false;
        }
    }
}
