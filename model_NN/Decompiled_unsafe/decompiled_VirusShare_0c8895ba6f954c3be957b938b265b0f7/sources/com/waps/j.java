package com.waps;

import android.os.AsyncTask;

class j extends AsyncTask {
    final /* synthetic */ AppConnect a;
    private String b = "";

    public j(AppConnect appConnect, String str) {
        this.a = appConnect;
        this.b = str;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Boolean doInBackground(Void... voidArr) {
        boolean z = false;
        String a2 = AppConnect.I.a("http://app.wapx.cn/action/" + AppConnect.as, (this.a.Y + "&" + AppConnect.at + "=" + this.b) + "&x=" + this.a.toMD5(("kingxiaoguang@gmail.com" + this.a.K + this.a.Q + this.b).toLowerCase().getBytes()).toLowerCase());
        if (a2 != null) {
            z = this.a.handleConnectResponse(a2);
        }
        return Boolean.valueOf(z);
    }
}
