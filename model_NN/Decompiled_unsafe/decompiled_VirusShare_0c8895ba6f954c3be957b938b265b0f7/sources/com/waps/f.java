package com.waps;

import android.content.DialogInterface;
import android.content.Intent;

class f implements DialogInterface.OnClickListener {
    final /* synthetic */ String a;
    final /* synthetic */ AppConnect b;

    f(AppConnect appConnect, String str) {
        this.b = appConnect;
        this.a = str;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        Intent intent = new Intent(this.b.F, OffersWebView.class);
        intent.putExtra("URL", this.a);
        intent.putExtra("isFinshClose", "true");
        intent.putExtra("CLIENT_PACKAGE", this.b.X);
        this.b.F.startActivity(intent);
        boolean unused = AppConnect.ag = false;
    }
}
