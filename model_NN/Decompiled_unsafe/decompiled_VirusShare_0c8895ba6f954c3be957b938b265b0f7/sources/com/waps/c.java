package com.waps;

import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.TranslateAnimation;

class c implements View.OnClickListener {
    final /* synthetic */ AdView a;

    c(AdView adView) {
        this.a = adView;
    }

    public void onClick(View view) {
        this.a.h.clearAnimation();
        TranslateAnimation translateAnimation = new TranslateAnimation(0.0f, 0.0f, (float) (this.a.k / 4), (float) this.a.k);
        translateAnimation.setDuration(500);
        translateAnimation.setFillAfter(true);
        translateAnimation.setInterpolator(new DecelerateInterpolator());
        translateAnimation.start();
        this.a.h.setBackgroundColor(-1);
        this.a.h.setAnimation(translateAnimation);
    }
}
