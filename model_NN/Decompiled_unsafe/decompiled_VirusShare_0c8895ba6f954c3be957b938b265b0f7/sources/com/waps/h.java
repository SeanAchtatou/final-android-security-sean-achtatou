package com.waps;

import android.os.AsyncTask;

class h extends AsyncTask {
    final /* synthetic */ AppConnect a;

    private h(AppConnect appConnect) {
        this.a = appConnect;
    }

    /* synthetic */ h(AppConnect appConnect, f fVar) {
        this(appConnect);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Boolean doInBackground(Void... voidArr) {
        boolean z = false;
        String a2 = AppConnect.I.a("http://app.wapx.cn/action/account/getinfo?", this.a.Y);
        if (a2 != null) {
            z = this.a.handleGetPointsResponse(a2);
        }
        if (!z) {
            AppConnect.am.getUpdatePointsFailed("无法更新积分");
        }
        return Boolean.valueOf(z);
    }
}
