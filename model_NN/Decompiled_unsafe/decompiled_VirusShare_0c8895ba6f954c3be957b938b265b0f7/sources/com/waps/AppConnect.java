package com.waps;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.Looper;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.WindowManager;
import com.anderfans.girlfart.R;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.security.MessageDigest;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public final class AppConnect {
    protected static String A = "";
    protected static String B = "";
    public static boolean D = false;
    private static AppConnect H = null;
    /* access modifiers changed from: private */
    public static s I = null;
    private static DisplayAd J = null;
    public static final String LIBRARY_VERSION_NUMBER = "1.5";
    private static final byte[] aD = {-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 62, -1, 62, -1, 63, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, -1, -1, -1, -1, -1, -1, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, -1, -1, -1, -1, 63, -1, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51};
    /* access modifiers changed from: private */
    public static boolean ag = true;
    /* access modifiers changed from: private */
    public static boolean ah = false;
    private static String ai = null;
    /* access modifiers changed from: private */
    public static UpdatePointsNotifier am;
    /* access modifiers changed from: private */
    public static String ar = "";
    /* access modifiers changed from: private */
    public static String as = "receiver/install?";
    /* access modifiers changed from: private */
    public static String at = "install";
    private static boolean ax = true;
    public static ComponentName z;
    o C;
    private l E = null;
    /* access modifiers changed from: private */
    public Context F = null;
    /* access modifiers changed from: private */
    public final ScheduledExecutorService G = Executors.newScheduledThreadPool(1);
    /* access modifiers changed from: private */
    public String K = "";
    private String L = "";
    private String M = "";
    private String N = "";
    private String O = "";
    private String P = "";
    /* access modifiers changed from: private */
    public String Q = "";
    /* access modifiers changed from: private */
    public String R = "";
    private String S = "";
    private String T = "";
    /* access modifiers changed from: private */
    public String U = "";
    private String V = "http://app.wapx.cn/action/account/offerlist?";
    private String W = "http://app.wapx.cn/action/account/ownslist?";
    /* access modifiers changed from: private */
    public String X = "";
    /* access modifiers changed from: private */
    public String Y = "";
    /* access modifiers changed from: private */
    public String Z = "";
    final String a = "x";
    private String aA = "";
    private String aB = "";
    private Bitmap aC;
    private byte[] aE;
    private int aF;
    private int aG;
    private boolean aH;
    private int aI;
    private String aa = "";
    private String ab = "";
    private String ac = "";
    private int ad = 0;
    private int ae = 0;
    /* access modifiers changed from: private */
    public String af = "";
    private String aj = "";
    private String ak = "";
    private String al = "";
    private h an = null;
    private k ao = null;
    private j ap = null;
    /* access modifiers changed from: private */
    public i aq = null;
    private boolean au = true;
    private int av;
    private String aw = "";
    private int ay = 0;
    private String az = "";
    final String b = "y";
    final String c = "net";
    final String d = "imsi";
    final String e = "udid";
    final String f = "device_name";
    final String g = "device_type";
    final String h = "os_version";
    final String i = "country_code";
    final String j = "language";
    final String k = "app_version";
    final String l = "sdk_version";
    final String m = "act";
    final String n = "userid";
    final String o = "channel";
    final String p = "points";
    final String q = "install";
    final String r = "uninstall";
    final String s = "load";
    final String t = "device_width";
    final String u = "device_height";
    protected String v = "";
    protected String w = "";
    protected String x = "";
    protected String y = "";

    public AppConnect() {
    }

    private AppConnect(Context context) {
        this.Y = getParams(context);
        if (Looper.myLooper() == null) {
            Looper.prepare();
        }
        this.E = new l(this, null);
        this.E.execute(new Void[0]);
    }

    public AppConnect(Context context, int i2) {
        this.Y = getParams(context);
    }

    private AppConnect(Context context, String str) {
        this.F = context;
        this.Y = getParams(context);
        this.Y += "&userid=" + str;
        if (Looper.myLooper() == null) {
            Looper.prepare();
        }
        this.E = new l(this, null);
        this.E.execute(new Void[0]);
    }

    private void FinishApplicationRunnable() {
        if (((Activity) this.F).isFinishing() && r.c) {
            new q(this.F).a();
            D = true;
        }
        if (((Activity) this.F).isFinishing() && !p.g) {
            SharedPreferences.Editor edit = this.F.getSharedPreferences("Package_Name", 3).edit();
            edit.clear();
            edit.commit();
            r.c = false;
        }
    }

    /* access modifiers changed from: private */
    public void UpdateDialog(String str) {
        try {
            AlertDialog.Builder builder = new AlertDialog.Builder(this.F);
            builder.setTitle("新版提示");
            builder.setMessage("有新版本(" + this.af + "),是否下载?");
            builder.setPositiveButton("下载", new f(this, str));
            builder.setNegativeButton("下次再说", new g(this));
            builder.show();
        } catch (Exception e2) {
        }
    }

    private Document buildDocument(String str) {
        try {
            DocumentBuilderFactory newInstance = DocumentBuilderFactory.newInstance();
            return newInstance.newDocumentBuilder().parse(new ByteArrayInputStream(str.getBytes("UTF-8")));
        } catch (Exception e2) {
            return null;
        }
    }

    /* access modifiers changed from: private */
    public boolean buildResponse(String str) {
        DocumentBuilderFactory newInstance = DocumentBuilderFactory.newInstance();
        try {
            Document parse = newInstance.newDocumentBuilder().parse(new ByteArrayInputStream(str.getBytes("UTF-8")));
            this.az = getNodeTrimValue(parse.getElementsByTagName("Title"));
            this.aA = getNodeTrimValue(parse.getElementsByTagName("Content"));
            this.aB = getNodeTrimValue(parse.getElementsByTagName("ClickUrl"));
            String nodeTrimValue = getNodeTrimValue(parse.getElementsByTagName("Image"));
            decodeBase64(nodeTrimValue.getBytes(), 0, nodeTrimValue.getBytes().length);
            this.aC = BitmapFactory.decodeByteArray(this.aE, 0, this.aF);
            new q(this.F).a(this.F.getSharedPreferences("Push_Icon", 3).getInt("push_icon", 0), this.aC, this.az, this.aA, this.aB);
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        return false;
    }

    private void getAlreadyInstalledPackages(Context context) {
        List<PackageInfo> installedPackages = context.getPackageManager().getInstalledPackages(0);
        String str = "";
        for (int i2 = 0; i2 < installedPackages.size(); i2++) {
            str = str + installedPackages.get(i2).packageName;
        }
        SharedPreferences.Editor edit = context.getSharedPreferences("Package_Name", 3).edit();
        edit.putString("Package_Names", str);
        edit.commit();
    }

    public static AppConnect getInstance(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("PushFlag", 3);
        SharedPreferences.Editor edit = sharedPreferences.edit();
        if (sharedPreferences.getString("push_flag", "").equals("")) {
            edit.putString("push_flag", "true");
        }
        edit.commit();
        if (I == null) {
            I = new s();
            NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
            if (!(activeNetworkInfo == null || activeNetworkInfo.getExtraInfo() == null || (!activeNetworkInfo.getExtraInfo().toLowerCase().equals("3gwap") && !activeNetworkInfo.getExtraInfo().toLowerCase().equals("cmwap")))) {
                I.a(true);
            }
        }
        if (H == null) {
            H = new AppConnect(context);
        }
        if (J == null) {
            J = new DisplayAd(context);
        }
        return H;
    }

    public static AppConnect getInstance(Context context, int i2) {
        if (I == null) {
            I = new s();
            NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
            if (!(activeNetworkInfo == null || activeNetworkInfo.getExtraInfo() == null || (!activeNetworkInfo.getExtraInfo().toLowerCase().equals("3gwap") && !activeNetworkInfo.getExtraInfo().toLowerCase().equals("cmwap")))) {
                I.a(true);
            }
        }
        if (H == null) {
            H = new AppConnect(context, i2);
        }
        if (J == null) {
            J = new DisplayAd(context);
        }
        return H;
    }

    public static AppConnect getInstance(Context context, String str) {
        if (I == null) {
            I = new s();
            NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
            if (!(activeNetworkInfo == null || activeNetworkInfo.getExtraInfo() == null || (!activeNetworkInfo.getExtraInfo().toLowerCase().equals("3gwap") && !activeNetworkInfo.getExtraInfo().toLowerCase().equals("cmwap")))) {
                I.a(true);
            }
        }
        if (H == null) {
            H = new AppConnect(context, str);
        }
        if (J == null) {
            J = new DisplayAd(context);
        }
        return H;
    }

    public static AppConnect getInstance(String str, Context context) {
        SharedPreferences.Editor edit = context.getSharedPreferences("WAPS_ID", 3).edit();
        edit.putString("app_id", str.trim());
        edit.commit();
        if (ax) {
            List<ActivityManager.RunningTaskInfo> runningTasks = ((ActivityManager) context.getSystemService("activity")).getRunningTasks(2);
            if (runningTasks != null && !runningTasks.isEmpty()) {
                z = runningTasks.get(0).baseActivity;
            }
            ax = false;
        }
        if (I == null) {
            I = new s();
            NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
            if (!(activeNetworkInfo == null || activeNetworkInfo.getExtraInfo() == null || (!activeNetworkInfo.getExtraInfo().toLowerCase().equals("3gwap") && !activeNetworkInfo.getExtraInfo().toLowerCase().equals("cmwap")))) {
                I.a(true);
            }
        }
        if (H == null) {
            H = new AppConnect(context);
        }
        if (J == null) {
            J = new DisplayAd(context);
        }
        return H;
    }

    public static AppConnect getInstanceNoConnect(Context context) {
        if (I == null) {
            I = new s();
            NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
            if (!(activeNetworkInfo == null || activeNetworkInfo.getExtraInfo() == null || (!activeNetworkInfo.getExtraInfo().toLowerCase().equals("3gwap") && !activeNetworkInfo.getExtraInfo().toLowerCase().equals("cmwap")))) {
                I.a(true);
            }
        }
        if (H == null) {
            H = new AppConnect(context, 0);
        }
        if (J == null) {
            J = new DisplayAd(context);
        }
        return H;
    }

    private String getNodeTrimValue(NodeList nodeList) {
        Element element = (Element) nodeList.item(0);
        if (element == null) {
            return null;
        }
        NodeList childNodes = element.getChildNodes();
        int length = childNodes.getLength();
        String str = "";
        for (int i2 = 0; i2 < length; i2++) {
            Node item = childNodes.item(i2);
            if (item != null) {
                str = str + item.getNodeValue();
            }
        }
        if (str == null || str.equals("")) {
            return null;
        }
        return str.trim();
    }

    private void getPointsHelper() {
        this.an = new h(this, null);
        this.an.execute(new Void[0]);
    }

    /* access modifiers changed from: private */
    public void getPushAd(String str, String str2) {
        getPushAdDateFromServer("http://app.wapx.cn/action/", str2);
    }

    private void getPushAdDateFromServer(String str, String str2) {
        this.C = new o(this, str + "push/ad?", str2);
        this.C.execute(new Void[0]);
    }

    /* access modifiers changed from: private */
    public boolean handleConnectResponse(String str) {
        String str2;
        String str3;
        String str4;
        String str5;
        String str6;
        try {
            Document buildDocument = buildDocument(str);
            if (!(buildDocument == null || buildDocument.getElementsByTagName("Version") == null)) {
                String nodeTrimValue = getNodeTrimValue(buildDocument.getElementsByTagName("Success"));
                String nodeTrimValue2 = getNodeTrimValue(buildDocument.getElementsByTagName("Version"));
                String nodeTrimValue3 = getNodeTrimValue(buildDocument.getElementsByTagName("Clear"));
                if (buildDocument.getElementsByTagName("Notify") == null || buildDocument.getElementsByTagName("Notify").getLength() <= 0) {
                    str2 = "";
                    str3 = "";
                    str4 = "";
                    str5 = "";
                } else {
                    String nodeTrimValue4 = getNodeTrimValue(buildDocument.getElementsByTagName("Id"));
                    String nodeTrimValue5 = getNodeTrimValue(buildDocument.getElementsByTagName("title"));
                    String nodeTrimValue6 = getNodeTrimValue(buildDocument.getElementsByTagName("content"));
                    str2 = nodeTrimValue4;
                    str3 = getNodeTrimValue(buildDocument.getElementsByTagName("url"));
                    String str7 = nodeTrimValue5;
                    str4 = nodeTrimValue6;
                    str5 = str7;
                }
                String nodeTrimValue7 = getNodeTrimValue(buildDocument.getElementsByTagName("AppList"));
                String str8 = "";
                if (buildDocument.getElementsByTagName("Push") == null || buildDocument.getElementsByTagName("Push").getLength() <= 0) {
                    str6 = "";
                } else {
                    str8 = getNodeTrimValue(buildDocument.getElementsByTagName("Wait"));
                    str6 = getNodeTrimValue(buildDocument.getElementsByTagName("Loop"));
                }
                if (nodeTrimValue != null && nodeTrimValue.equals("true")) {
                    if (nodeTrimValue2 != null && !"".equals(nodeTrimValue2)) {
                        this.af = nodeTrimValue2;
                    }
                    if (nodeTrimValue3 != null && !"".equals(nodeTrimValue3.trim())) {
                        ah = true;
                    }
                    if (str2 != null && !"".equals(str2.trim())) {
                        this.v = str2;
                    }
                    if (str5 != null && !"".equals(str5.trim())) {
                        this.w = str5;
                    }
                    if (str4 != null && !"".equals(str4.trim())) {
                        this.x = str4;
                    }
                    if (str3 != null && !"".equals(str3.trim())) {
                        this.y = str3;
                    }
                    if (nodeTrimValue7 != null && !"".equals(nodeTrimValue7.trim())) {
                        SharedPreferences.Editor edit = this.F.getSharedPreferences("Finalize_Flag", 3).edit();
                        edit.putString("appList", nodeTrimValue7);
                        edit.commit();
                    }
                    if (str8 != null && !"".equals(str8.trim())) {
                        A = str8;
                    }
                    if (str6 != null && !"".equals(str6.trim())) {
                        B = str6;
                    }
                    return true;
                }
            }
        } catch (Exception e2) {
        }
        return false;
    }

    /* access modifiers changed from: private */
    public boolean handleGetPointsResponse(String str) {
        String nodeTrimValue;
        Document buildDocument = buildDocument(str);
        if (!(buildDocument == null || (nodeTrimValue = getNodeTrimValue(buildDocument.getElementsByTagName("Success"))) == null || !nodeTrimValue.equals("true"))) {
            this.F.getSharedPreferences("Points", 0);
            String nodeTrimValue2 = getNodeTrimValue(buildDocument.getElementsByTagName("Points"));
            String nodeTrimValue3 = getNodeTrimValue(buildDocument.getElementsByTagName("CurrencyName"));
            if (!(nodeTrimValue2 == null || nodeTrimValue3 == null)) {
                am.getUpdatePoints(nodeTrimValue3, Integer.parseInt(nodeTrimValue2));
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: private */
    public boolean handleSpendPointsResponse(String str) {
        Document buildDocument = buildDocument(str);
        if (buildDocument != null) {
            String nodeTrimValue = getNodeTrimValue(buildDocument.getElementsByTagName("Success"));
            if (nodeTrimValue != null && nodeTrimValue.equals("true")) {
                String nodeTrimValue2 = getNodeTrimValue(buildDocument.getElementsByTagName("Points"));
                String nodeTrimValue3 = getNodeTrimValue(buildDocument.getElementsByTagName("CurrencyName"));
                if (!(nodeTrimValue2 == null || nodeTrimValue3 == null)) {
                    am.getUpdatePoints(nodeTrimValue3, Integer.parseInt(nodeTrimValue2));
                    return true;
                }
            } else if (nodeTrimValue != null && nodeTrimValue.endsWith("false")) {
                am.getUpdatePointsFailed(getNodeTrimValue(buildDocument.getElementsByTagName("Message")));
                return true;
            }
        }
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException}
     arg types: [java.io.File, int]
     candidates:
      ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException}
      ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException} */
    /* access modifiers changed from: private */
    public void loadApps() {
        FileInputStream fileInputStream;
        FileOutputStream fileOutputStream;
        BufferedReader bufferedReader;
        FileInputStream fileInputStream2;
        File file;
        FileOutputStream fileOutputStream2;
        int i2 = 0;
        new Intent("android.intent.action.MAIN", (Uri) null).addCategory("android.intent.category.LAUNCHER");
        String str = "";
        try {
            if (Environment.getExternalStorageState().equals("mounted")) {
                File file2 = new File(Environment.getExternalStorageDirectory().toString() + "/Android");
                File file3 = new File(Environment.getExternalStorageDirectory().toString() + "/Android/Package.dat");
                if (!file2.exists()) {
                    file2.mkdir();
                }
                if (!file3.exists()) {
                    file3.createNewFile();
                }
                FileInputStream fileInputStream3 = new FileInputStream(file3);
                try {
                    BufferedReader bufferedReader2 = new BufferedReader(new InputStreamReader(fileInputStream3));
                    if (bufferedReader2 != null) {
                        while (true) {
                            try {
                                String readLine = bufferedReader2.readLine();
                                if (readLine == null) {
                                    break;
                                }
                                str = str + readLine;
                            } catch (Exception e2) {
                                e = e2;
                                fileOutputStream = null;
                                BufferedReader bufferedReader3 = bufferedReader2;
                                fileInputStream = fileInputStream3;
                                bufferedReader = bufferedReader3;
                                try {
                                    e.printStackTrace();
                                    try {
                                        fileOutputStream.close();
                                        fileInputStream.close();
                                        bufferedReader.close();
                                    } catch (Exception e3) {
                                        e3.printStackTrace();
                                        return;
                                    }
                                } catch (Throwable th) {
                                    th = th;
                                    try {
                                        fileOutputStream.close();
                                        fileInputStream.close();
                                        bufferedReader.close();
                                    } catch (Exception e4) {
                                        e4.printStackTrace();
                                    }
                                    throw th;
                                }
                            } catch (Throwable th2) {
                                th = th2;
                                fileOutputStream = null;
                                BufferedReader bufferedReader4 = bufferedReader2;
                                fileInputStream = fileInputStream3;
                                bufferedReader = bufferedReader4;
                                fileOutputStream.close();
                                fileInputStream.close();
                                bufferedReader.close();
                                throw th;
                            }
                        }
                    }
                    BufferedReader bufferedReader5 = bufferedReader2;
                    file = file3;
                    fileInputStream2 = fileInputStream3;
                    bufferedReader = bufferedReader5;
                } catch (Exception e5) {
                    e = e5;
                    fileOutputStream = null;
                    fileInputStream = fileInputStream3;
                    bufferedReader = null;
                    e.printStackTrace();
                    fileOutputStream.close();
                    fileInputStream.close();
                    bufferedReader.close();
                } catch (Throwable th3) {
                    th = th3;
                    fileOutputStream = null;
                    fileInputStream = fileInputStream3;
                    bufferedReader = null;
                    fileOutputStream.close();
                    fileInputStream.close();
                    bufferedReader.close();
                    throw th;
                }
            } else {
                bufferedReader = null;
                fileInputStream2 = null;
                file = null;
            }
            try {
                List<PackageInfo> installedPackages = this.F.getPackageManager().getInstalledPackages(0);
                for (int i3 = 0; i3 < installedPackages.size(); i3++) {
                    PackageInfo packageInfo = installedPackages.get(i3);
                    int i4 = packageInfo.applicationInfo.flags;
                    ApplicationInfo applicationInfo = packageInfo.applicationInfo;
                    if ((i4 & 1) <= 0) {
                        i2++;
                        String str2 = packageInfo.packageName;
                        if (str2.startsWith("com.")) {
                            String substring = str2.substring(3, str2.length());
                            if (!str.contains(substring)) {
                                ar += substring + ";";
                            }
                        }
                    }
                }
                byte[] bytes = ar.getBytes("UTF-8");
                if (file != null) {
                    FileOutputStream fileOutputStream3 = new FileOutputStream(file, true);
                    try {
                        fileOutputStream3.write(bytes);
                        fileOutputStream2 = fileOutputStream3;
                    } catch (Exception e6) {
                        e = e6;
                        fileInputStream = fileInputStream2;
                        fileOutputStream = fileOutputStream3;
                        e.printStackTrace();
                        fileOutputStream.close();
                        fileInputStream.close();
                        bufferedReader.close();
                    } catch (Throwable th4) {
                        th = th4;
                        fileInputStream = fileInputStream2;
                        fileOutputStream = fileOutputStream3;
                        fileOutputStream.close();
                        fileInputStream.close();
                        bufferedReader.close();
                        throw th;
                    }
                } else {
                    fileOutputStream2 = null;
                }
                try {
                    fileOutputStream2.close();
                    fileInputStream2.close();
                    bufferedReader.close();
                } catch (Exception e7) {
                    e7.printStackTrace();
                }
            } catch (Exception e8) {
                e = e8;
                fileInputStream = fileInputStream2;
                fileOutputStream = null;
                e.printStackTrace();
                fileOutputStream.close();
                fileInputStream.close();
                bufferedReader.close();
            } catch (Throwable th5) {
                th = th5;
                fileInputStream = fileInputStream2;
                fileOutputStream = null;
                fileOutputStream.close();
                fileInputStream.close();
                bufferedReader.close();
                throw th;
            }
        } catch (Exception e9) {
            e = e9;
            bufferedReader = null;
            fileOutputStream = null;
            fileInputStream = null;
            e.printStackTrace();
            fileOutputStream.close();
            fileInputStream.close();
            bufferedReader.close();
        } catch (Throwable th6) {
            th = th6;
            bufferedReader = null;
            fileOutputStream = null;
            fileInputStream = null;
            fileOutputStream.close();
            fileInputStream.close();
            bufferedReader.close();
            throw th;
        }
    }

    private void notifyReceiverHelper() {
        this.ap = new j(this, this.ab);
        this.ap.execute(new Void[0]);
    }

    private void packageReceiverHelper() {
        this.ap = new j(this, this.aa);
        this.ap.execute(new Void[0]);
    }

    /* access modifiers changed from: private */
    public void sendNotify(String str, String str2, String str3, String str4, String str5) {
        new q(this.F).a(str, str2, str3, str4, str5, true);
    }

    private void spendPointsHelper() {
        this.ao = new k(this, null);
        this.ao.execute(new Void[0]);
    }

    private String toHexString(byte[] bArr, String str) {
        StringBuilder sb = new StringBuilder();
        for (byte b2 : bArr) {
            String hexString = Integer.toHexString(b2 & 255);
            if (hexString.length() == 1) {
                sb.append("0");
            }
            sb.append(hexString).append(str);
        }
        return sb.toString();
    }

    /* access modifiers changed from: private */
    public String toMD5(byte[] bArr) {
        try {
            MessageDigest instance = MessageDigest.getInstance("MD5");
            instance.reset();
            instance.update(bArr);
            return toHexString(instance.digest(), "");
        } catch (Exception e2) {
            e2.printStackTrace();
            return "";
        }
    }

    /* access modifiers changed from: package-private */
    public void decodeBase64(byte[] bArr, int i2, int i3) {
        byte b2;
        this.aE = new byte[bArr.length];
        this.aF = 0;
        this.aH = false;
        this.aG = 0;
        if (i3 < 0) {
            this.aH = true;
        }
        int i4 = 0;
        int i5 = i2;
        while (true) {
            if (i4 >= i3) {
                break;
            }
            int i6 = i5 + 1;
            byte b3 = bArr[i5];
            if (b3 == 61) {
                this.aH = true;
                break;
            }
            if (b3 >= 0 && b3 < aD.length && (b2 = aD[b3]) >= 0) {
                int i7 = this.aG + 1;
                this.aG = i7;
                this.aG = i7 % 4;
                this.aI = b2 + (this.aI << 6);
                if (this.aG == 0) {
                    byte[] bArr2 = this.aE;
                    int i8 = this.aF;
                    this.aF = i8 + 1;
                    bArr2[i8] = (byte) ((this.aI >> 16) & 255);
                    byte[] bArr3 = this.aE;
                    int i9 = this.aF;
                    this.aF = i9 + 1;
                    bArr3[i9] = (byte) ((this.aI >> 8) & 255);
                    byte[] bArr4 = this.aE;
                    int i10 = this.aF;
                    this.aF = i10 + 1;
                    bArr4[i10] = (byte) (this.aI & 255);
                }
            }
            i4++;
            i5 = i6;
        }
        if (this.aH && this.aG != 0) {
            this.aI <<= 6;
            switch (this.aG) {
                case R.styleable.com_adwo_adsdk_AdwoAdView_secondaryTextColor:
                    this.aI <<= 6;
                    byte[] bArr5 = this.aE;
                    int i11 = this.aF;
                    this.aF = i11 + 1;
                    bArr5[i11] = (byte) ((this.aI >> 16) & 255);
                    return;
                case R.styleable.com_adwo_adsdk_AdwoAdView_refreshInterval:
                    byte[] bArr6 = this.aE;
                    int i12 = this.aF;
                    this.aF = i12 + 1;
                    bArr6[i12] = (byte) ((this.aI >> 16) & 255);
                    byte[] bArr7 = this.aE;
                    int i13 = this.aF;
                    this.aF = i13 + 1;
                    bArr7[i13] = (byte) ((this.aI >> 8) & 255);
                    return;
                default:
                    return;
            }
        }
    }

    public void finalize() {
        H = null;
        this.G.schedule(new m(this, null), 0, TimeUnit.SECONDS);
        if (((Activity) this.F).isFinishing() && this.au) {
            SharedPreferences.Editor edit = this.F.getSharedPreferences("PushFlag", 3).edit();
            edit.clear();
            edit.commit();
            this.au = false;
        }
    }

    public void getDisplayAd(DisplayAdNotifier displayAdNotifier) {
        J.getDisplayAdDataFromServer("http://ads.wapx.cn/action/", this.Y, displayAdNotifier);
    }

    public String getParams(Context context) {
        this.F = context;
        initMetaData();
        this.Y += "app_id=" + this.Q + "&";
        this.Y += "udid=" + this.K + "&";
        this.Y += "imsi=" + this.aj + "&";
        this.Y += "net=" + this.ak + "&";
        this.Y += "app_version=" + this.R + "&";
        this.Y += "sdk_version=" + this.S + "&";
        this.Y += "device_name=" + this.L + "&";
        this.Y += "y=" + this.al + "&";
        this.Y += "device_type=" + this.M + "&";
        this.Y += "os_version=" + this.N + "&";
        this.Y += "country_code=" + this.O + "&";
        this.Y += "language=" + this.P + "&";
        this.Y += "act=" + context.getPackageName() + "." + context.getClass().getSimpleName();
        if (this.T != null && !"".equals(this.T)) {
            this.Y += "&";
            this.Y += "channel=" + this.T;
        }
        if (this.ad > 0 && this.ae > 0) {
            this.Y += "&";
            this.Y += "device_width=" + this.ad + "&";
            this.Y += "device_height=" + this.ae;
        }
        return this.Y.replaceAll(" ", "%20");
    }

    public void getPoints(UpdatePointsNotifier updatePointsNotifier) {
        if (H != null) {
            am = updatePointsNotifier;
            H.getPointsHelper();
        }
    }

    public void getPushAd() {
        getPushAd("", this.Y);
    }

    public String getWapsId() {
        return this.F.getSharedPreferences("WAPS_ID", 3).getString("app_id", "");
    }

    public void initMetaData() {
        String obj;
        PackageManager packageManager = this.F.getPackageManager();
        try {
            ApplicationInfo applicationInfo = packageManager.getApplicationInfo(this.F.getPackageName(), 128);
            if (applicationInfo != null && applicationInfo.metaData != null) {
                if (getWapsId() == null || "".equals(getWapsId())) {
                    String string = applicationInfo.metaData.getString("WAPS_ID");
                    if (string == null || "".equals(string)) {
                        string = applicationInfo.metaData.getString("APP_ID");
                    }
                    if (string == null || string.equals("")) {
                        Log.w("WAPS_SDK", "WapsId is not setted！ Please chick it!");
                        return;
                    } else {
                        this.Q = string.trim();
                        Log.d("WAPS_SDK", "WapsId is setted by manifest, the value is: " + this.Q);
                    }
                } else {
                    this.Q = getWapsId();
                    Log.d("WAPS_SDK", "WapsId is setted by code,the value is: " + this.Q);
                    String string2 = applicationInfo.metaData.getString("WAPS_ID");
                    if (string2 == null || "".equals(string2)) {
                        string2 = applicationInfo.metaData.getString("APP_ID");
                    }
                    if (string2 != null && !string2.equals("")) {
                        if (!this.Q.equals(string2.trim())) {
                            Log.w("WAPS_SDK", "WapsId is setted by code is not equals the value be setted by manifest! Please chick it!");
                        } else {
                            Log.d("WAPS_SDK", "The WapsId in manifest is: " + string2.trim());
                        }
                    } else {
                        return;
                    }
                }
                this.X = this.F.getPackageName();
                String string3 = applicationInfo.metaData.getString("CLIENT_PACKAGE");
                if (string3 != null && !string3.equals("")) {
                    this.X = string3;
                }
                Object obj2 = applicationInfo.metaData.get("WAPS_PID");
                if (!(obj2 == null || (obj = obj2.toString()) == null || obj.equals(""))) {
                    this.T = obj;
                }
                this.R = packageManager.getPackageInfo(this.F.getPackageName(), 0).versionName;
                this.M = "android";
                this.L = Build.MODEL;
                this.N = Build.VERSION.RELEASE;
                this.O = Locale.getDefault().getCountry();
                this.P = Locale.getDefault().getLanguage();
                this.aj = ((TelephonyManager) this.F.getSystemService("phone")).getSubscriberId();
                try {
                    Context context = this.F;
                    Context context2 = this.F;
                    NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
                    if (activeNetworkInfo == null || activeNetworkInfo.getTypeName().toLowerCase().equals("mobile")) {
                        this.ak = activeNetworkInfo.getExtraInfo().toLowerCase();
                    } else {
                        this.ak = activeNetworkInfo.getTypeName().toLowerCase();
                    }
                    Log.d("WAPS_SDK", "The net is: " + this.ak);
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
                this.S = LIBRARY_VERSION_NUMBER;
                SharedPreferences sharedPreferences = this.F.getSharedPreferences("appPrefrences", 0);
                String string4 = applicationInfo.metaData.getString("DEVICE_ID");
                if (string4 == null || string4.equals("")) {
                    TelephonyManager telephonyManager = (TelephonyManager) this.F.getSystemService("phone");
                    if (telephonyManager != null) {
                        this.K = telephonyManager.getDeviceId();
                        if (this.K == null || this.K.length() == 0) {
                            this.K = "0";
                        }
                        try {
                            this.K = this.K.toLowerCase();
                            if (Integer.valueOf(Integer.parseInt(this.K)).intValue() == 0) {
                                StringBuffer stringBuffer = new StringBuffer();
                                stringBuffer.append("EMULATOR");
                                String string5 = sharedPreferences.getString("emulatorDeviceId", null);
                                if (string5 == null || string5.equals("")) {
                                    for (int i2 = 0; i2 < 32; i2++) {
                                        stringBuffer.append("1234567890abcdefghijklmnopqrstuvw".charAt(((int) (Math.random() * 100.0d)) % 30));
                                    }
                                    this.K = stringBuffer.toString().toLowerCase();
                                    SharedPreferences.Editor edit = sharedPreferences.edit();
                                    edit.putString("emulatorDeviceId", this.K);
                                    edit.commit();
                                } else {
                                    this.K = string5;
                                }
                            }
                        } catch (NumberFormatException e3) {
                        }
                    } else {
                        this.K = null;
                    }
                } else {
                    this.K = string4;
                }
                this.al = toMD5(("kingxiaoguang@gmail.com" + this.K + this.Q).toLowerCase().getBytes()).toLowerCase();
                try {
                    DisplayMetrics displayMetrics = new DisplayMetrics();
                    ((WindowManager) this.F.getSystemService("window")).getDefaultDisplay().getMetrics(displayMetrics);
                    this.ad = displayMetrics.widthPixels;
                    this.ae = displayMetrics.heightPixels;
                } catch (Exception e4) {
                }
                this.av = sharedPreferences.getInt("PrimaryColor", 0);
                String string6 = sharedPreferences.getString("InstallReferral", null);
                if (string6 != null && !string6.equals("")) {
                    this.Z = string6;
                }
            }
        } catch (Exception e5) {
        }
    }

    public void notify_receiver(String str, int i2) {
        switch (i2) {
            case 0:
                as = "notify/show?";
                at = "nyid";
                break;
            case 1:
                as = "notify/click?";
                at = "nyid";
                break;
        }
        this.ab = str;
        if (H != null) {
            H.notifyReceiverHelper();
        }
    }

    public void package_receiver(String str, int i2) {
        switch (i2) {
            case 0:
                as = "receiver/install?";
                at = "install";
                break;
            case 1:
                as = "receiver/load_offer?";
                at = "load";
                break;
            case R.styleable.com_adwo_adsdk_AdwoAdView_secondaryTextColor:
                as = "receiver/load_ad?";
                at = "load";
                break;
            case R.styleable.com_adwo_adsdk_AdwoAdView_refreshInterval:
                as = "receiver/uninstall?";
                as = "uninstall";
                break;
            default:
                as = "receiver/install?";
                at = "install";
                break;
        }
        this.aa = str;
        if (H != null) {
            H.packageReceiverHelper();
        }
    }

    public void setPushIcon(int i2) {
        this.ay = i2;
        SharedPreferences.Editor edit = this.F.getSharedPreferences("Push_Icon", 3).edit();
        edit.putInt("push_icon", this.ay);
        edit.commit();
    }

    public void setWapsId(Context context, String str) {
        SharedPreferences.Editor edit = context.getSharedPreferences("WAPS_ID", 3).edit();
        edit.putString("app_id", str.trim());
        edit.commit();
    }

    public void showFeedback() {
        this.F.startActivity(showFeedback_forTab());
    }

    public Intent showFeedback_forTab() {
        Intent intent = new Intent();
        intent.setClass(this.F, OffersWebView.class);
        intent.putExtra("UrlPath", "http://app.wapx.cn/action/feedback/form");
        intent.putExtra("ACTIVITY_FLAG", "feedback");
        intent.putExtra("URL_PARAMS", this.Y);
        intent.putExtra("offers_webview_tag", "OffersWebView");
        return intent;
    }

    public void showMore(Context context) {
        showMore(context, this.K);
    }

    public void showMore(Context context, String str) {
        Intent intent = new Intent(context, OffersWebView.class);
        intent.putExtra("Offers_URL", this.W);
        intent.putExtra("USER_ID", str);
        intent.putExtra("URL_PARAMS", this.Y);
        intent.putExtra("CLIENT_PACKAGE", this.X);
        intent.putExtra("offers_webview_tag", "OffersWebView");
        context.startActivity(intent);
    }

    public Intent showMore_forTab(Context context) {
        return showMore_forTab(context, this.K);
    }

    public Intent showMore_forTab(Context context, String str) {
        Intent intent = new Intent(context, OffersWebView.class);
        intent.putExtra("Offers_URL", this.W);
        intent.putExtra("USER_ID", str);
        intent.putExtra("URL_PARAMS", this.Y);
        intent.putExtra("CLIENT_PACKAGE", this.X);
        intent.putExtra("offers_webview_tag", "OffersWebView");
        return intent;
    }

    public void showOffers(Context context) {
        showOffers(context, this.K);
    }

    public void showOffers(Context context, String str) {
        Intent intent = new Intent(context, OffersWebView.class);
        intent.putExtra("Offers_URL", this.V);
        intent.putExtra("USER_ID", str);
        intent.putExtra("URL_PARAMS", this.Y);
        intent.putExtra("CLIENT_PACKAGE", this.X);
        intent.putExtra("offers_webview_tag", "OffersWebView");
        context.startActivity(intent);
    }

    public Intent showOffers_forTab(Context context) {
        return showOffers_forTab(context, this.K);
    }

    public Intent showOffers_forTab(Context context, String str) {
        Intent intent = new Intent(context, OffersWebView.class);
        intent.putExtra("Offers_URL", this.V);
        intent.putExtra("USER_ID", str);
        intent.putExtra("URL_PARAMS", this.Y);
        intent.putExtra("CLIENT_PACKAGE", this.X);
        intent.putExtra("offers_webview_tag", "OffersWebView");
        return intent;
    }

    public void spendPoints(int i2, UpdatePointsNotifier updatePointsNotifier) {
        if (i2 >= 0) {
            this.U = "" + i2;
            if (H != null) {
                am = updatePointsNotifier;
                H.spendPointsHelper();
            }
        }
    }
}
