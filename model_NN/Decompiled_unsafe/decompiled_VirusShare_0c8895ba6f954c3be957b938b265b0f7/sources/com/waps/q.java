package com.waps;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.view.View;
import android.widget.RemoteViews;
import android.widget.Toast;
import java.io.File;

public class q {
    protected static boolean b = false;
    protected static boolean c = true;
    public static String d = "";
    public Notification a;
    private NotificationManager e;
    private Context f;
    private String g;
    private String h = "";
    private String i = "";
    private String j = "";
    private String k = "";
    private String l = "";
    private boolean m = true;

    public q(Context context) {
        this.f = context;
    }

    public void a() {
        this.a = new Notification();
        this.a.icon = 17301545;
        this.a.tickerText = "发现新应用";
        this.a.when = System.currentTimeMillis();
        this.a.defaults = 1;
        this.a.flags = 32;
        Intent intent = new Intent(this.f, OffersWebView.class);
        SharedPreferences sharedPreferences = this.f.getSharedPreferences("DownLoadSave", 3);
        sharedPreferences.edit();
        PendingIntent activity = PendingIntent.getActivity(this.f, 100, intent, 0);
        this.a.contentIntent = activity;
        this.a.setLatestEventInfo(this.f, "新安装应用", "您有新应用安装，点击查看！", activity);
        this.e = (NotificationManager) this.f.getSystemService("notification");
        if (c) {
            this.e.notify(2, this.a);
            c = false;
        }
        SharedPreferences.Editor edit = this.f.getSharedPreferences("Start_Tag", 3).edit();
        edit.putString("notify_start_tag", "Notify");
        edit.commit();
        this.f.getSharedPreferences("Notify", 3);
        SharedPreferences.Editor edit2 = sharedPreferences.edit();
        edit2.clear();
        edit2.commit();
    }

    public void a(int i2) {
        this.e = (NotificationManager) this.f.getSystemService("notification");
        this.e.cancel(i2);
    }

    public void a(int i2, Bitmap bitmap, String str, String str2, String str3) {
        this.k = str3;
        this.i = str;
        this.j = str2;
        String str4 = this.i;
        this.a = new Notification();
        if (i2 == 0) {
            this.a.icon = 17301651;
        } else {
            this.a.icon = i2;
        }
        this.a.tickerText = this.j;
        this.a.when = System.currentTimeMillis();
        this.a.defaults = 1;
        this.a.flags = 16;
        Intent intent = new Intent();
        if (this.k != null && !"".equals(this.k.trim())) {
            intent.setClass(this.f, OffersWebView.class);
            SharedPreferences.Editor edit = this.f.getSharedPreferences("Notify", 3).edit();
            edit.putString("Notity_Title", this.i);
            edit.putString("Notity_Content", this.j);
            edit.putString("Notity_UrlPath", this.k);
            edit.putString("offers_webview_tag", "OffersWebView");
            edit.putString("NotifyAd_Tag", "true");
            edit.commit();
        }
        PendingIntent activity = PendingIntent.getActivity(this.f, 100, intent, 0);
        this.a.contentIntent = activity;
        if (this.f.getResources().getIdentifier("push_layout", "layout", this.f.getPackageName()) != 0) {
            RemoteViews remoteViews = new RemoteViews(this.f.getPackageName(), this.f.getResources().getIdentifier("push_layout", "layout", this.f.getPackageName()));
            remoteViews.setImageViewBitmap(this.f.getResources().getIdentifier("notify_image", "id", this.f.getPackageName()), bitmap);
            remoteViews.setTextViewText(this.f.getResources().getIdentifier("notify_text", "id", this.f.getPackageName()), str4);
            remoteViews.setTextViewText(this.f.getResources().getIdentifier("content_text", "id", this.f.getPackageName()), this.j);
            this.a.contentView = remoteViews;
        } else {
            this.a.setLatestEventInfo(this.f, str4, this.j, activity);
        }
        this.e = (NotificationManager) this.f.getSystemService("notification");
        this.e.notify(2, this.a);
        AppConnect.getInstanceNoConnect(this.f).notify_receiver(this.h, 0);
        b = true;
        SharedPreferences.Editor edit2 = this.f.getSharedPreferences("Start_Tag", 3).edit();
        edit2.putString("notify_start_tag", "Notify");
        edit2.commit();
    }

    public void a(int i2, String str) {
        File fileStreamPath;
        this.e.cancel(i2);
        if (!Environment.getExternalStorageState().equals("mounted") && (fileStreamPath = this.f.getFileStreamPath(str)) != null) {
            fileStreamPath.delete();
            Toast.makeText(this.f, str + "已经被删除", 1).show();
        }
    }

    public void a(View view, String str, int i2, String str2) {
        this.a = new Notification();
        this.a.icon = 17301633;
        this.a.tickerText = "正在下载";
        this.a.when = System.currentTimeMillis();
        this.a.flags = 16;
        this.a.setLatestEventInfo(this.f, str, "正在下载，已完成  " + str2 + "", PendingIntent.getActivity(this.f, 100, new Intent(), 0));
        this.e = (NotificationManager) this.f.getSystemService("notification");
        this.e.notify(i2, this.a);
    }

    public void a(View view, String str, int i2, String str2, String str3) {
        this.g = str2;
        this.a = new Notification();
        this.a.icon = 17301634;
        this.a.tickerText = "";
        this.a.when = System.currentTimeMillis();
        this.a.defaults = 1;
        this.a.flags = 16;
        Intent intent = new Intent();
        intent.setAction("android.intent.action.VIEW");
        if (Environment.getExternalStorageState().equals("mounted")) {
            intent.setDataAndType(Uri.fromFile(new File("/sdcard/download/" + str)), "application/vnd.android.package-archive");
        } else {
            intent.setDataAndType(Uri.fromFile(this.f.getFileStreamPath(str)), "application/vnd.android.package-archive");
        }
        this.a.setLatestEventInfo(this.f, str, str3, PendingIntent.getActivity(this.f, 100, intent, 0));
        this.e = (NotificationManager) this.f.getSystemService("notification");
        this.e.notify(i2, this.a);
    }

    public void a(String str, String str2, String str3, String str4, String str5, boolean z) {
        this.h = str;
        this.i = str2;
        this.j = str3;
        this.k = str4;
        this.l = str5;
        String str6 = this.i;
        this.a = new Notification();
        int identifier = this.f.getResources().getIdentifier("icon", "drawable", this.f.getPackageName());
        if (identifier != 0) {
            this.a.icon = identifier;
        } else {
            this.a.icon = 17301618;
        }
        this.a.tickerText = this.j;
        this.a.when = System.currentTimeMillis();
        if (z) {
            this.a.defaults = 1;
        }
        this.a.flags = 16;
        Intent intent = new Intent();
        if (this.k != null && !"".equals(this.k.trim())) {
            intent.setClass(this.f, OffersWebView.class);
            SharedPreferences.Editor edit = this.f.getSharedPreferences("Notify", 3).edit();
            edit.putString("Notity_Id", this.h);
            edit.putString("Notity_Title", this.i);
            edit.putString("Notity_Content", this.j);
            edit.putString("Notity_UrlPath", this.k);
            edit.putString("offers_webview_tag", "OffersWebView");
            edit.putString("NotifyAd_Tag", "false");
            if (this.k.contains("down_type")) {
                edit.putString("Notity_UrlParams", str5);
            }
            edit.commit();
        }
        PendingIntent activity = PendingIntent.getActivity(this.f, 100, intent, 0);
        this.a.contentIntent = activity;
        this.a.setLatestEventInfo(this.f, str6, this.j, activity);
        this.e = (NotificationManager) this.f.getSystemService("notification");
        if (this.m) {
            this.e.notify(2, this.a);
            AppConnect.getInstanceNoConnect(this.f).notify_receiver(this.h, 0);
            this.m = false;
        }
        b = true;
        SharedPreferences.Editor edit2 = this.f.getSharedPreferences("Start_Tag", 3).edit();
        edit2.putString("notify_start_tag", "Notify");
        edit2.commit();
    }
}
