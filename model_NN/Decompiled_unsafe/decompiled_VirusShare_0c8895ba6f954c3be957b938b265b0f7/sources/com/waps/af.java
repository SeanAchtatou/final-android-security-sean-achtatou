package com.waps;

import android.content.Intent;
import android.graphics.Bitmap;
import android.webkit.WebView;
import android.webkit.WebViewClient;

class af extends WebViewClient {
    final /* synthetic */ OffersWebView a;

    private af(OffersWebView offersWebView) {
        this.a = offersWebView;
    }

    /* synthetic */ af(OffersWebView offersWebView, v vVar) {
        this(offersWebView);
    }

    public void onPageFinished(WebView webView, String str) {
        this.a.h.setVisibility(8);
        super.onPageFinished(webView, str);
    }

    public void onPageStarted(WebView webView, String str, Bitmap bitmap) {
        this.a.h.setVisibility(0);
        super.onPageStarted(webView, str, bitmap);
    }

    public void onReceivedError(WebView webView, int i, String str, String str2) {
        webView.setScrollBarStyle(0);
        webView.loadDataWithBaseURL("", "<html><body bgcolor=\"000000\" align=\"center\"><br/><font color=\"ffffff\">网络链接失败，请检查网络。</font><br/></body></html>", "text/html", "utf-8", "");
        boolean unused = this.a.p = false;
        super.onReceivedError(webView, i, str, str2);
    }

    public boolean shouldOverrideUrlLoading(WebView webView, String str) {
        String substring;
        Intent intent;
        String unused = this.a.o = str;
        if (str.startsWith("load://")) {
            if (str.contains(";http://")) {
                substring = str.substring("load://".length(), str.indexOf(";"));
                this.a.a = str.substring(str.indexOf(";") + 1);
            } else {
                substring = str.substring("load://".length());
            }
            if (substring != "") {
                try {
                    intent = this.a.getPackageManager().getLaunchIntentForPackage(substring);
                } catch (Exception e) {
                    intent = null;
                }
                if (intent != null) {
                    this.a.startActivity(intent);
                    AppConnect.getInstanceNoConnect(this.a).package_receiver(substring, 1);
                    return true;
                }
                webView.loadUrl(this.a.a);
                return true;
            }
        }
        return false;
    }
}
