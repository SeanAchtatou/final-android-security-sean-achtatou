package com.waps;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.view.View;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;

public class p extends AsyncTask {
    public static boolean g = false;
    private static Context r;
    q a;
    r b;
    int c;
    s d;
    String e = "";
    String f = "";
    float h = 0.0f;
    float i = 0.0f;
    NumberFormat j = new DecimalFormat("#0");
    float k;
    InputStream l = null;
    FileOutputStream m = null;
    String n = "";
    String o = "";
    private String p;
    private View q;

    public p(Context context, View view, String str) {
        r = context;
        this.q = view;
        this.p = str;
        this.e = str.substring(str.indexOf("http://") + 7, str.indexOf("/", str.indexOf("http://") + 8));
        this.f = str.substring(0, str.indexOf("/", str.indexOf("http://") + 8));
        this.a = new q(r);
        this.d = new s();
        Context context2 = r;
        Context context3 = r;
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) context2.getSystemService("connectivity")).getActiveNetworkInfo();
        if (activeNetworkInfo != null && activeNetworkInfo.getExtraInfo() != null) {
            if (activeNetworkInfo.getExtraInfo().toLowerCase().equals("3gwap") || activeNetworkInfo.getExtraInfo().toLowerCase().equals("cmwap")) {
                this.d.a(true);
            }
        }
    }

    /* access modifiers changed from: protected */
    public String a(String str) {
        return str.substring(str.lastIndexOf("/") + 1);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public String doInBackground(String... strArr) {
        HttpResponse execute;
        this.c = (int) System.currentTimeMillis();
        this.a.a(this.q, "正在获取文件名...", this.c, "0 %");
        try {
            this.n = a(this.p);
            this.o = "/sdcard/download/";
            this.h = (float) b(this.p);
            if (!this.d.a()) {
                execute = new DefaultHttpClient().execute(new HttpGet(strArr[0].replaceAll(" ", "%20")));
            } else {
                String str = strArr[0];
                HttpHost httpHost = new HttpHost("10.0.0.172", 80, "http");
                HttpHost httpHost2 = new HttpHost(this.e, 80, "http");
                HttpGet httpGet = new HttpGet(str.replaceAll(" ", "%20").replaceFirst(this.f, ""));
                BasicHttpParams basicHttpParams = new BasicHttpParams();
                HttpConnectionParams.setConnectionTimeout(basicHttpParams, 15000);
                HttpConnectionParams.setSoTimeout(basicHttpParams, 30000);
                DefaultHttpClient defaultHttpClient = new DefaultHttpClient(basicHttpParams);
                defaultHttpClient.getParams().setParameter("http.route.default-proxy", httpHost);
                execute = defaultHttpClient.execute(httpHost2, httpGet);
            }
            this.l = execute.getEntity().getContent();
            if (Environment.getExternalStorageState().equals("mounted")) {
                File file = new File(this.o);
                File file2 = new File(this.o, this.n);
                if (!file.exists()) {
                    file.mkdir();
                }
                if (!file2.exists()) {
                    file2.createNewFile();
                }
                this.m = new FileOutputStream(file2);
            } else {
                this.m = r.openFileOutput(this.n, 3);
            }
            if (this.l != null) {
                byte[] bArr = new byte[51200];
                while (true) {
                    int read = this.l.read(bArr);
                    if (read == -1) {
                        break;
                    } else if (Integer.parseInt(this.j.format((double) this.k)) > 100) {
                        this.a.a(this.q, this.n, this.c, this.o + this.n, "下载失败，请重新下载");
                        break;
                    } else {
                        this.m.write(bArr, 0, read);
                        this.i = ((float) read) + this.i;
                        this.k = (this.i / this.h) * 100.0f;
                        publishProgress(Integer.valueOf(((int) (this.i / this.h)) * 100));
                        this.j.format((double) this.k);
                        this.a.a(this.q, this.n, this.c, this.j.format((double) this.k) + " %");
                    }
                }
                int read2 = this.l.read(bArr);
                Thread.sleep(1000);
                if (read2 == -1) {
                    String str2 = this.o + this.n;
                    File file3 = Environment.getExternalStorageState().equals("mounted") ? new File(str2) : r.getFileStreamPath(this.n);
                    if (this.n.endsWith(".apk")) {
                        this.a.a(this.q, this.n, this.c, str2, "下载完成,点击安装");
                    } else {
                        this.a.a(this.q, this.n, this.c, str2, "下载完成！");
                    }
                    g = true;
                    Intent intent = new Intent();
                    intent.setAction("android.intent.action.VIEW");
                    intent.setDataAndType(Uri.fromFile(file3), "application/vnd.android.package-archive");
                    r.startActivity(intent);
                    this.b = new r(this.a, this.c, this.n);
                    a(this.b);
                }
            }
            try {
                if (this.l != null) {
                    this.l.close();
                }
                if (this.m == null) {
                    return "";
                }
                this.m.close();
                return "";
            } catch (Exception e2) {
                return "";
            }
        } catch (Exception e3) {
            e3.printStackTrace();
            try {
                if (this.l != null) {
                    this.l.close();
                }
                if (this.m == null) {
                    return "";
                }
                this.m.close();
                return "";
            } catch (Exception e4) {
                return "";
            }
        } catch (Throwable th) {
            try {
                if (this.l != null) {
                    this.l.close();
                }
                if (this.m != null) {
                    this.m.close();
                }
            } catch (Exception e5) {
            }
            throw th;
        }
    }

    /* access modifiers changed from: protected */
    public void a(r rVar) {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.intent.action.PACKAGE_ADDED");
        intentFilter.addDataScheme("package");
        r.registerReceiver(rVar, intentFilter);
    }

    /* access modifiers changed from: protected */
    public long b(String str) {
        if (!this.d.a()) {
            HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(str).openConnection();
            httpURLConnection.setConnectTimeout(5000);
            httpURLConnection.setRequestMethod("GET");
            return (long) httpURLConnection.getContentLength();
        }
        HttpHost httpHost = new HttpHost("10.0.0.172", 80, "http");
        HttpHost httpHost2 = new HttpHost(this.e, 80, "http");
        HttpGet httpGet = new HttpGet(str.replaceAll(" ", "%20").replaceFirst(this.f, ""));
        BasicHttpParams basicHttpParams = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(basicHttpParams, 15000);
        HttpConnectionParams.setSoTimeout(basicHttpParams, 30000);
        DefaultHttpClient defaultHttpClient = new DefaultHttpClient(basicHttpParams);
        defaultHttpClient.getParams().setParameter("http.route.default-proxy", httpHost);
        return defaultHttpClient.execute(httpHost2, httpGet).getEntity().getContentLength();
    }
}
