package com.waps;

import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.view.MotionEvent;
import android.view.View;

class d implements View.OnTouchListener {
    final /* synthetic */ AdView a;

    d(AdView adView) {
        this.a = adView;
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        boolean z;
        Intent intent;
        try {
            ApplicationInfo applicationInfo = this.a.d.getPackageManager().getApplicationInfo(this.a.d.getPackageName(), 128);
            this.a.d.getPackageName();
            String string = applicationInfo.metaData.getString("CLIENT_PACKAGE");
            if (string == null || !string.equals("")) {
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        if (this.a.u == null || "".equals(this.a.u)) {
            z = true;
        } else {
            try {
                intent = this.a.d.getPackageManager().getLaunchIntentForPackage(this.a.u);
            } catch (Exception e2) {
                e2.printStackTrace();
                intent = null;
            }
            if (intent != null) {
                this.a.d.startActivity(intent);
                AppConnect.getInstanceNoConnect(this.a.d).package_receiver(this.a.u, 2);
                z = false;
            } else {
                z = true;
            }
        }
        if (!z) {
            return false;
        }
        if (this.a.v == null || "".equals(this.a.v)) {
            this.a.showAdsInfo();
        } else {
            Intent intent2 = new Intent("android.intent.action.VIEW", Uri.parse(this.a.t));
            intent2.setFlags(268435456);
            this.a.d.startActivity(intent2);
        }
        return true;
    }
}
