package com.waps;

import android.app.Activity;
import android.content.SharedPreferences;

class m implements Runnable {
    final /* synthetic */ AppConnect a;

    private m(AppConnect appConnect) {
        this.a = appConnect;
    }

    /* synthetic */ m(AppConnect appConnect, f fVar) {
        this(appConnect);
    }

    public void run() {
        SharedPreferences sharedPreferences = this.a.F.getSharedPreferences("Finalize_Flag", 3);
        SharedPreferences.Editor edit = sharedPreferences.edit();
        String string = sharedPreferences.getString("appList", "");
        if (string != null && !"".equals(string.trim()) && string.equals("2")) {
            if (((Activity) this.a.F).isFinishing() && r.c) {
                new q(this.a.F).a();
                AppConnect.D = true;
                q.c = false;
            }
            if (((Activity) this.a.F).isFinishing() && !p.g) {
                SharedPreferences.Editor edit2 = this.a.F.getSharedPreferences("Package_Name", 3).edit();
                edit2.clear();
                edit2.commit();
                r.c = false;
                edit.clear();
                edit.commit();
            }
        }
    }
}
