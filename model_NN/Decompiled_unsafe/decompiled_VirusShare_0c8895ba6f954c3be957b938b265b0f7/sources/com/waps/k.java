package com.waps;

import android.os.AsyncTask;

class k extends AsyncTask {
    final /* synthetic */ AppConnect a;

    private k(AppConnect appConnect) {
        this.a = appConnect;
    }

    /* synthetic */ k(AppConnect appConnect, f fVar) {
        this(appConnect);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Boolean doInBackground(Void... voidArr) {
        boolean z = false;
        String a2 = AppConnect.I.a("http://app.wapx.cn/action/account/spend?", this.a.Y + "&points=" + this.a.U);
        if (a2 != null) {
            z = this.a.handleSpendPointsResponse(a2);
        }
        if (!z) {
            AppConnect.am.getUpdatePointsFailed("消费积分失败.");
        }
        return Boolean.valueOf(z);
    }
}
