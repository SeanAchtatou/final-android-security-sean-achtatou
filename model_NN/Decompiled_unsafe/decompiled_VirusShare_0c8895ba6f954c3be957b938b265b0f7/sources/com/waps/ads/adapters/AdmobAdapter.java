package com.waps.ads.adapters;

import android.app.Activity;
import android.util.Log;
import android.view.ViewGroup;
import com.anderfans.girlfart.R;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;
import com.waps.ads.AdGroupLayout;
import com.waps.ads.AdGroupTargeting;
import com.waps.ads.a.a;
import com.waps.ads.b.c;
import com.waps.ads.g;
import java.text.SimpleDateFormat;

public class AdmobAdapter extends a {
    boolean a = false;

    public AdmobAdapter(AdGroupLayout adGroupLayout, c cVar) {
        super(adGroupLayout, cVar);
    }

    /* access modifiers changed from: protected */
    public String birthdayForAdGroupTargeting() {
        if (AdGroupTargeting.getBirthDate() != null) {
            return new SimpleDateFormat("yyyyMMdd").format(AdGroupTargeting.getBirthDate().getTime());
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public AdRequest.Gender genderForAdGroupTargeting() {
        switch (a.a[AdGroupTargeting.getGender().ordinal()]) {
            case 1:
                return AdRequest.Gender.MALE;
            case R.styleable.com_adwo_adsdk_AdwoAdView_secondaryTextColor:
                return AdRequest.Gender.FEMALE;
            default:
                return null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.waps.ads.g.<init>(com.waps.ads.AdGroupLayout, android.view.ViewGroup):void
     arg types: [com.waps.ads.AdGroupLayout, com.google.ads.AdView]
     candidates:
      com.waps.ads.g.<init>(com.waps.ads.AdGroupLayout, android.view.View):void
      com.waps.ads.g.<init>(com.waps.ads.AdGroupLayout, android.view.ViewGroup):void */
    public void handle() {
        Activity activity;
        AdGroupLayout adGroupLayout = (AdGroupLayout) this.c.get();
        if (adGroupLayout != null && (activity = (Activity) adGroupLayout.a.get()) != null) {
            AdView adView = new AdView(activity, AdSize.BANNER, this.d.e);
            adView.loadAd(requestForAdGroupLayout(adGroupLayout));
            adGroupLayout.j.resetRollover();
            adGroupLayout.b.post(new g(adGroupLayout, (ViewGroup) adView));
            adGroupLayout.rotateThreadedDelayed();
        }
    }

    /* access modifiers changed from: protected */
    public void log(String str) {
        Log.d("AdGroup_SDK", "GoogleAdapter " + str);
    }

    /* access modifiers changed from: protected */
    public AdRequest requestForAdGroupLayout(AdGroupLayout adGroupLayout) {
        AdRequest adRequest = new AdRequest();
        adRequest.setTesting(AdGroupTargeting.getTestMode());
        adRequest.setGender(genderForAdGroupTargeting());
        adRequest.setBirthday(birthdayForAdGroupTargeting());
        if (adGroupLayout.d.j == 1) {
            adRequest.setLocation(adGroupLayout.j.e);
        }
        adRequest.setKeywords(AdGroupTargeting.getKeywordSet());
        return adRequest;
    }
}
