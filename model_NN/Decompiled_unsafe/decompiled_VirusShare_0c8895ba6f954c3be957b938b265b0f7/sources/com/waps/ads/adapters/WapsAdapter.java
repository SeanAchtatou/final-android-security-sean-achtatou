package com.waps.ads.adapters;

import android.view.View;
import com.waps.AppConnect;
import com.waps.DisplayAdNotifier;
import com.waps.ads.AdGroupLayout;
import com.waps.ads.a.a;
import com.waps.ads.b.b;
import com.waps.ads.b.c;
import com.waps.ads.g;

public class WapsAdapter extends a implements DisplayAdNotifier {
    static AdGroupLayout b;
    boolean a = false;

    public WapsAdapter(AdGroupLayout adGroupLayout, c cVar) {
        super(adGroupLayout, cVar);
    }

    private void updateResultsInUi() {
        if (this.a) {
            b.removeAllViews();
            if (b != null) {
            }
            this.a = false;
        }
    }

    public void getDisplayAdResponse(View view) {
        b.b.post(new g(b, view));
    }

    public void getDisplayAdResponseFailed(String str) {
    }

    public void handle() {
        b = (AdGroupLayout) this.c.get();
        if (b != null) {
            b bVar = b.d;
            AppConnect.getInstanceNoConnect(b.getContext()).getDisplayAd(this);
            b.j.resetRollover();
            b.rotateThreadedDelayed();
        }
    }
}
