package com.waps.ads.c;

import android.app.Activity;
import android.util.DisplayMetrics;

public class a {
    public static String a = "http://ads.wapx.cn/action/adgroup/config?";
    private static double b = -1.0d;

    public static double a(double d, double d2) {
        return d2 > 0.0d ? d * d2 : d;
    }

    public static double a(Activity activity) {
        if (b == -1.0d) {
            DisplayMetrics displayMetrics = new DisplayMetrics();
            activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            b = (double) displayMetrics.density;
        }
        return b;
    }

    public static int a(int i, double d) {
        return (int) a((double) i, d);
    }

    public static String a(byte[] bArr) {
        StringBuffer stringBuffer = new StringBuffer();
        for (byte b2 : bArr) {
            byte b3 = (b2 >>> 4) & 15;
            int i = 0;
            while (true) {
                if (b3 < 0 || b3 > 9) {
                    stringBuffer.append((char) ((b3 - 10) + 97));
                } else {
                    stringBuffer.append((char) (b3 + 48));
                }
                b3 = b2 & 15;
                int i2 = i + 1;
                if (i >= 1) {
                    break;
                }
                i = i2;
            }
        }
        return stringBuffer.toString();
    }
}
