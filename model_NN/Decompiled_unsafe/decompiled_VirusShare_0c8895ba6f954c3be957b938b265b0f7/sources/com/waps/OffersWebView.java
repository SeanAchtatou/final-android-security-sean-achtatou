package com.waps;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class OffersWebView extends Activity {
    private String A = "";
    /* access modifiers changed from: private */
    public q B;
    private SharedPreferences C;
    private SharedPreferences D;
    private SharedPreferences E;
    /* access modifiers changed from: private */
    public SharedPreferences.Editor F;
    /* access modifiers changed from: private */
    public SharedPreferences.Editor G;
    /* access modifiers changed from: private */
    public SharedPreferences.Editor H;
    private String I = "";
    /* access modifiers changed from: private */
    public PackageManager J;
    private List K;
    /* access modifiers changed from: private */
    public List L;
    private ListView M;
    private LinearLayout N;
    /* access modifiers changed from: private */
    public boolean O = false;
    private String P = "";
    String a = "";
    p b;
    String c = "";
    String d = "";
    r e;
    /* access modifiers changed from: private */
    public WebView f = null;
    private String g = null;
    /* access modifiers changed from: private */
    public ProgressBar h;
    private String i = "";
    private String j = "";
    private String k = "";
    private String l = "";
    private String m = "";
    /* access modifiers changed from: private */
    public String n = "false";
    /* access modifiers changed from: private */
    public String o;
    /* access modifiers changed from: private */
    public boolean p = true;
    private String q = "";
    private String r = "";
    private String s = "";
    private String t = "";
    private String u = "";
    private String v = "";
    /* access modifiers changed from: private */
    public String w = "";
    private String x = "";
    private String y = "";
    /* access modifiers changed from: private */
    public String z = "";

    private void bindNewApp(PackageManager packageManager) {
        Intent intent = new Intent("android.intent.action.MAIN", (Uri) null);
        intent.addCategory("android.intent.category.LAUNCHER");
        this.K = packageManager.queryIntentActivities(intent, 0);
        this.L = getNewAppInfo(this.K, this.I);
        Collections.sort(this.L, new ResolveInfo.DisplayNameComparator(packageManager));
    }

    private void getAlreadyInstalledPackages(Context context) {
        List<PackageInfo> installedPackages = context.getPackageManager().getInstalledPackages(0);
        String str = "";
        for (int i2 = 0; i2 < installedPackages.size(); i2++) {
            str = str + installedPackages.get(i2).packageName;
        }
        SharedPreferences.Editor edit = context.getSharedPreferences("Package_Name", 3).edit();
        edit.putString("Package_Names", str);
        edit.commit();
    }

    /* access modifiers changed from: private */
    public void getDownload() {
        try {
            this.c = this.b.a(this.o);
            this.d = "/sdcard/download/";
            int b2 = (int) this.b.b(this.o);
            if (Environment.getExternalStorageState().equals("mounted")) {
                File file = new File(this.d, this.c);
                if (file != null && file.length() == ((long) b2)) {
                    new AlertDialog.Builder(this).setTitle("提示").setIcon(17301618).setMessage("该安装文件已存在于/sdcard/download/目录下，您可以直接安装或重新下载！").setPositiveButton("安装", new aa(this)).setNeutralButton("重新下载", new z(this)).setNegativeButton("取消", new y(this)).create().show();
                } else if (file.length() < ((long) b2) && file.length() != 0) {
                    Toast.makeText(this, "正在下载,请稍候...", 0).show();
                } else if (file.length() == 0) {
                    Toast.makeText(this, "正在准备下载,请稍候...", 0).show();
                    this.b.execute(this.o);
                    if (this.n != null && "true".equals(this.n)) {
                        finish();
                    }
                }
            } else {
                File fileStreamPath = getFileStreamPath(this.c);
                if (fileStreamPath != null && fileStreamPath.length() == ((long) b2)) {
                    fileStreamPath.delete();
                    Toast.makeText(this, "正在准备下载,请稍候...", 0).show();
                    this.b.execute(this.o);
                    if (this.n != null && "true".equals(this.n)) {
                        finish();
                    }
                } else if (fileStreamPath != null && fileStreamPath.length() != 0) {
                    Toast.makeText(this, "正在下载,请稍候...", 0).show();
                } else if (fileStreamPath.length() == 0) {
                    Toast.makeText(this, "正在准备下载,请稍候...", 0).show();
                    this.b.execute(this.o);
                    if (this.n != null && "true".equals(this.n)) {
                        finish();
                    }
                }
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    private void initMetaData(Bundle bundle) {
        if (bundle != null) {
            if (bundle.getString("Offers_URL") != null) {
                this.k = bundle.getString("Offers_URL");
            }
            if (bundle.getString("URL") != null) {
                this.k = bundle.getString("URL");
            }
            if (bundle.getString("UrlPath") != null) {
                this.q = bundle.getString("Notify_Id");
                this.r = bundle.getString("UrlPath");
                this.s = bundle.getString("ACTIVITY_FLAG");
                this.t = bundle.getString("SHWO_FLAG");
                if (this.r.contains("down_type")) {
                    this.u = bundle.getString("Notify_Url_Params");
                }
            }
            this.l = bundle.getString("URL_PARAMS");
            this.j = bundle.getString("CLIENT_PACKAGE");
            this.m = bundle.getString("USER_ID");
            this.n = bundle.getString("isFinshClose");
            this.l += "&publisher_user_id=" + this.m;
            this.i = bundle.getString("offers_webview_tag");
        }
    }

    private void initNotityData(SharedPreferences sharedPreferences) {
        if (sharedPreferences != null) {
            this.v = sharedPreferences.getString("Notity_Id", "");
            this.x = sharedPreferences.getString("Notity_Title", "");
            this.y = sharedPreferences.getString("Notity_Content", "");
            this.w = sharedPreferences.getString("Notity_UrlPath", "");
            this.A = sharedPreferences.getString("offers_webview_tag", "");
            this.P = sharedPreferences.getString("NotifyAd_Tag", "");
            if (this.w.contains("down_type")) {
                this.z = sharedPreferences.getString("Notity_UrlParams", "");
            }
        }
    }

    private void showNewApp() {
        try {
            this.D = getSharedPreferences("DownLoadSave", 3);
            this.E = getSharedPreferences("Package_Name", 3);
            this.G = this.D.edit();
            this.H = this.E.edit();
            this.I = this.E.getString("Package_Names", "");
            this.J = getPackageManager();
            bindNewApp(this.J);
            this.N = new LinearLayout(this);
            this.N.setOrientation(1);
            this.N.setGravity(17);
            this.N.setBackgroundColor(-16777216);
            RelativeLayout relativeLayout = new RelativeLayout(this);
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -2);
            TextView textView = new TextView(this);
            textView.setText("新安装应用列表");
            textView.setTextSize(18.0f);
            textView.setTextColor(-1);
            textView.setPadding(0, 10, 0, 10);
            ImageView imageView = new ImageView(this);
            imageView.setImageResource(17301545);
            imageView.setId(2);
            imageView.setPadding(10, 0, 10, 0);
            layoutParams.addRule(1, imageView.getId());
            relativeLayout.addView(imageView);
            relativeLayout.addView(textView, layoutParams);
            LinearLayout linearLayout = new LinearLayout(this);
            linearLayout.setOrientation(0);
            linearLayout.setLayoutParams(new LinearLayout.LayoutParams(-1, -2, 1.0f));
            this.M = new ListView(this);
            Button button = new Button(this);
            button.setGravity(1);
            button.setText("确　定");
            button.setTextSize(15.0f);
            button.setLayoutParams(new LinearLayout.LayoutParams(-1, -2, 1.0f));
            Button button2 = new Button(this);
            button2.setGravity(1);
            button2.setText("关　闭");
            button2.setTextSize(15.0f);
            button2.setLayoutParams(new LinearLayout.LayoutParams(-1, -2, 1.0f));
            linearLayout.addView(button);
            linearLayout.addView(button2);
            this.M.setAdapter((ListAdapter) new ag(this, this, this.L));
            this.M.setOnItemClickListener(new ad(this));
            this.M.setBackgroundColor(-1);
            this.N.addView(relativeLayout);
            this.N.addView(this.M);
            this.N.addView(linearLayout);
            setContentView(this.N);
            button.setOnClickListener(new ae(this));
            button2.setOnClickListener(new w(this));
        } catch (Exception e2) {
        }
    }

    private void showNotifyList(SharedPreferences sharedPreferences) {
        this.D = getSharedPreferences("DownLoadSave", 3);
        this.E = getSharedPreferences("Package_Name", 3);
        this.G = this.D.edit();
        this.H = this.E.edit();
        this.I = this.E.getString("Package_Names", "");
        this.J = getPackageManager();
        bindNewApp(this.J);
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 < this.L.size()) {
                String str = ((ResolveInfo) this.L.get(i3)).activityInfo.packageName;
                String string = sharedPreferences.getString("package_tag" + i3, "");
                if (string != null && !"".equals(string.trim()) && str.equals(string)) {
                    startActivity(getPackageManager().getLaunchIntentForPackage(str));
                }
                i2 = i3 + 1;
            } else {
                return;
            }
        }
    }

    private void showPushDialog() {
        try {
            this.C = getSharedPreferences("Notify", 3);
            initNotityData(this.C);
            this.F = this.C.edit();
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(this.x);
            builder.setMessage(this.y);
            if (this.w != null && !"".equals(this.w.trim())) {
                builder.setPositiveButton("确定", new ab(this));
            }
            builder.setNegativeButton("关闭", new ac(this));
            builder.show();
            AppConnect.getInstanceNoConnect(this).notify_receiver(this.v, 1);
        } catch (Exception e2) {
        }
    }

    public List getNewAppInfo(List list, String str) {
        ArrayList arrayList = new ArrayList();
        for (int i2 = 0; i2 < list.size(); i2++) {
            ResolveInfo resolveInfo = (ResolveInfo) list.get(i2);
            String str2 = resolveInfo.activityInfo.packageName;
            if (str != null && !"".equals(str) && !str.contains(str2)) {
                arrayList.add(resolveInfo);
            }
        }
        return arrayList;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        RelativeLayout.LayoutParams layoutParams;
        RelativeLayout.LayoutParams layoutParams2;
        initMetaData(getIntent().getExtras());
        initNotityData(getSharedPreferences("Notify", 3));
        if ("feedback".equals(this.s)) {
            this.g = this.r + "?" + this.l;
        } else if (this.i == null || "".equals(this.i)) {
            if (!this.w.contains("down_type")) {
                AppConnect.getInstanceNoConnect(this).notify_receiver(this.v, 1);
                this.i = this.A;
                this.g = this.w + "?" + "nyid" + "=" + this.q + this.z;
            } else if (this.P.equals("true")) {
                this.i = this.A;
                this.g = this.w + "&publisher_user_id=" + this.m;
                this.n = "true";
                AppConnect.getInstanceNoConnect(this).notify_receiver(this.v, 1);
            } else if (this.P.equals("false")) {
                AppConnect.getInstanceNoConnect(this).notify_receiver(this.v, 1);
                this.i = this.A;
                this.g = this.w + "&" + this.z;
                this.n = "true";
            }
        } else if (this.k.indexOf("?") > -1) {
            this.g = this.k + this.l;
        } else {
            this.g = this.k + "?a=1" + this.l;
        }
        this.g = this.g.replaceAll(" ", "%20");
        if (this.i == null || "".equals(this.i.trim()) || !this.i.equals("OffersWebView")) {
            SharedPreferences sharedPreferences = getSharedPreferences("Start_Tag", 3);
            SharedPreferences.Editor edit = sharedPreferences.edit();
            String string = sharedPreferences.getString("notify_start_tag", "");
            if (string != null && !"".equals(string)) {
                setTheme(16973835);
                super.onCreate(bundle);
                requestWindowFeature(1);
                this.B = new q(this);
                if (r.c && AppConnect.D) {
                    this.D = getSharedPreferences("DownLoadSave", 3);
                    this.E = getSharedPreferences("Package_Name", 3);
                    this.G = this.D.edit();
                    this.H = this.E.edit();
                    if (this.D.getAll().size() == 1) {
                        startActivity(getPackageManager().getLaunchIntentForPackage(this.D.getString("0", "")));
                        this.B.a(2);
                        edit.clear();
                        edit.commit();
                        finish();
                        return;
                    }
                    showNewApp();
                    edit.clear();
                    edit.commit();
                    return;
                }
                return;
            }
            return;
        }
        super.onCreate(bundle);
        requestWindowFeature(1);
        getWindowManager().getDefaultDisplay().getWidth();
        getWindowManager().getDefaultDisplay().getHeight();
        RelativeLayout relativeLayout = new RelativeLayout(this);
        this.f = new WebView(this);
        this.h = new ProgressBar(this);
        this.h.setEnabled(true);
        this.h.setVisibility(0);
        WebSettings settings = this.f.getSettings();
        if (this.s == null || "".equals(this.s.trim())) {
            RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(-1, -1);
            LinearLayout linearLayout = new LinearLayout(this);
            linearLayout.setOrientation(1);
            linearLayout.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
            relativeLayout.setGravity(17);
            RelativeLayout.LayoutParams layoutParams4 = new RelativeLayout.LayoutParams(-2, -2);
            layoutParams4.addRule(13);
            relativeLayout.addView(this.f, layoutParams3);
            relativeLayout.addView(this.h, layoutParams4);
            linearLayout.addView(relativeLayout, new ViewGroup.LayoutParams(-1, -1));
            setContentView(linearLayout);
        } else {
            if ("notify".equals(this.s)) {
                RelativeLayout.LayoutParams layoutParams5 = new RelativeLayout.LayoutParams(-1, -2);
                RelativeLayout.LayoutParams layoutParams6 = new RelativeLayout.LayoutParams(-1, -1);
                if ("show".equals(this.t)) {
                    AppConnect.getInstanceNoConnect(this).notify_receiver(this.q, 0);
                }
                RelativeLayout.LayoutParams layoutParams7 = layoutParams6;
                layoutParams = layoutParams5;
                layoutParams2 = layoutParams7;
            } else {
                layoutParams = null;
                layoutParams2 = null;
            }
            if ("feedback".equals(this.s)) {
                RelativeLayout.LayoutParams layoutParams8 = new RelativeLayout.LayoutParams(-1, -2);
                RelativeLayout.LayoutParams layoutParams9 = new RelativeLayout.LayoutParams(-1, -1);
                relativeLayout.setBackgroundColor(-1);
                setRequestedOrientation(1);
                RelativeLayout.LayoutParams layoutParams10 = layoutParams9;
                layoutParams = layoutParams8;
                layoutParams2 = layoutParams10;
            }
            RelativeLayout.LayoutParams layoutParams11 = new RelativeLayout.LayoutParams(-2, -2);
            this.f.setId(2);
            this.f.setLayoutParams(layoutParams2);
            Button button = new Button(this);
            button.setLayoutParams(layoutParams);
            button.setId(1);
            button.setText("关　闭");
            button.getBackground().setAlpha(100);
            this.h.setLayoutParams(layoutParams11);
            layoutParams.addRule(12);
            layoutParams2.addRule(2, 1);
            layoutParams11.addRule(13);
            relativeLayout.addView(this.f, layoutParams2);
            relativeLayout.addView(this.h, layoutParams11);
            setContentView(relativeLayout);
            button.setOnClickListener(new v(this));
        }
        this.f.setWebViewClient(new af(this, null));
        this.f.addJavascriptInterface(new SDKUtils(this), "SDKUtils");
        settings.setJavaScriptEnabled(true);
        WebView webView = this.f;
        WebView.enablePlatformNotifications();
        this.f.setHttpAuthUsernamePassword("10.0.0.172", "", "", "");
        this.f.setScrollBarStyle(0);
        if (this.n != null && "true".equals(this.n)) {
            Toast.makeText(this, "加载中,请稍候...", 0).show();
        }
        this.f.loadUrl(this.g);
        this.f.setDownloadListener(new x(this));
        if (getSharedPreferences("Package_Name", 3).getString("Package_Names", "") == "") {
            getAlreadyInstalledPackages(this);
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        if (this.f != null) {
            this.f.destroy();
        }
        finish();
        super.onDestroy();
    }

    public void onItemClick(AdapterView adapterView, View view, int i2, long j2) {
        ResolveInfo resolveInfo = (ResolveInfo) this.L.get(i2);
        ComponentName componentName = new ComponentName(resolveInfo.activityInfo.packageName, resolveInfo.activityInfo.name);
        Intent intent = new Intent();
        intent.setComponent(componentName);
        startActivity(intent);
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (this.f == null || i2 != 4 || !this.f.canGoBack()) {
            return super.onKeyDown(i2, keyEvent);
        }
        if (!this.p) {
            finish();
            this.p = true;
        }
        this.f.goBack();
        q.c = true;
        finish();
        return true;
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        if (!(this.g == null || this.f == null)) {
            this.f.loadUrl(this.g);
        }
        super.onResume();
    }
}
