package com.anderfans.girlfart;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.media.SoundPool;
import com.anderfans.girlfart.critical.Critical;
import com.waps.UpdatePointsNotifier;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class Data {
    private static ArrayList<HashMap<String, Object>> dataMap;
    private static HashMap<String, Integer> soundMap;
    private static SoundPool soundPool;

    public static void init(Context ctx, UpdatePointsNotifier notifier) {
        soundPool = new SoundPool(4, 3, 100);
        dataMap = new ArrayList<>();
        soundMap = new HashMap<>();
        setData("放荡的屁", R.drawable.gf1, R.raw.a, ctx, true);
        setData("腼腆的屁", R.drawable.gf2, R.raw.a1, ctx, true);
        setData("清纯的屁", R.drawable.gf3, R.raw.a2, ctx, true);
        setData("喧哗的屁", R.drawable.gf4, R.raw.a3, ctx, true);
        setData("张扬的屁", R.drawable.gf5, R.raw.a4, ctx, true);
        setData("暗恋的屁1", R.drawable.gf6, R.raw.x, ctx, true);
        setData("暗恋的屁2", R.drawable.gf7, R.raw.y, ctx, true);
        setData("害羞的屁", R.drawable.gf8, R.raw.b, ctx, true);
        setData("成熟的屁1", R.drawable.gf9, R.raw.c, ctx, true);
        setData("成熟的屁2", R.drawable.gf10, R.raw.d, ctx, true);
        setData("诱惑的屁", R.drawable.gf11, R.raw.e, ctx, true);
        setData("高潮的屁", R.drawable.gf12, R.raw.f, ctx, true);
        setData("狂野的屁", R.drawable.gf13, R.raw.g, ctx, true);
        setData("幽默的屁", R.drawable.gf14, R.raw.h, ctx, true);
        setData("恶心的屁1", R.drawable.gf15, R.raw.i, ctx, true);
        setData("恶心的屁2", R.drawable.gf16, R.raw.j, ctx, true);
        setData("恶心的屁3", R.drawable.gf17, R.raw.t, ctx, true);
        setData("直白的屁", R.drawable.gf18, R.raw.z, ctx, true);
        setData("有韵味的屁", R.drawable.gf19, R.raw.k, ctx, true);
        setData("放大屁", R.drawable.gf20, R.raw.m, ctx, true);
        setData("调情的屁1", R.drawable.gf21, R.raw.o, ctx, true);
        setData("调情的屁2", R.drawable.gf22, R.raw.p, ctx, true);
        setData("作呕的屁", R.drawable.gf23, R.raw.q, ctx, true);
        setData("急不可待的屁", R.drawable.gf24, R.raw.r, ctx, true);
        setData("饥渴的屁", R.drawable.gf25, R.raw.s, ctx, true);
        setData("打枪屁", R.drawable.gf26, R.raw.v, ctx, true);
        setData("低潮的屁", R.drawable.gf27, R.raw.u, ctx, true);
        setData("温柔的屁", R.drawable.gf28, R.raw.w, ctx, true);
        if (Critical.isSoundLockedAndAlert(ctx, "放屁接力", notifier)) {
            setData("放屁接力", R.drawable.question, R.raw.n, ctx);
        } else {
            setData("放屁接力", R.drawable.gf29, R.raw.n, ctx);
        }
        if (Critical.isSoundLockedAndAlert(ctx, "无限的屁", notifier)) {
            setData("无限的屁", R.drawable.question, R.raw.l, ctx);
        } else {
            setData("无限的屁", R.drawable.gf30, R.raw.l, ctx);
        }
        if (Critical.isSoundLockedAndAlert(ctx, "放屁交响曲", notifier)) {
            setData("放屁接力", R.drawable.question, R.raw.n, ctx);
        } else {
            setData("放屁交响曲", R.drawable.gf31, R.raw.jinglebells, ctx);
        }
    }

    public static ArrayList<HashMap<String, Object>> getData() {
        return dataMap;
    }

    public static void playSound(String key) {
        soundPool.play(soundMap.get(key).intValue(), 1.0f, 1.0f, 0, 0, 1.0f);
    }

    public static void playSound(String key, int nCounts) {
        soundPool.play(soundMap.get(key).intValue(), 1.0f, 1.0f, 0, nCounts - 1, 1.0f);
    }

    private static void setData(String tip, int imgId, int soundResId, Context ctx) {
        HashMap<String, Object> tmp = new HashMap<>();
        tmp.put("fartText", tip);
        tmp.put("fartImage", Integer.valueOf(imgId));
        dataMap.add(tmp);
        soundMap.put(tip, Integer.valueOf(soundPool.load(ctx, soundResId, 1)));
    }

    private static void setData(String tip, int imgId, int soundResId, Context ctx, boolean isUnlock) {
        setData(tip, imgId, soundResId, ctx);
        if (isUnlock) {
            Critical.unlockSound(ctx, tip);
        }
    }

    public static List<String> avaliableSounds(Context ctx) {
        Set<String> keys = soundMap.keySet();
        ArrayList<String> ret = new ArrayList<>();
        for (String itemKey : keys) {
            if (!Critical.isSoundLocked(ctx, itemKey)) {
                ret.add(itemKey);
            }
        }
        return ret;
    }

    public static Drawable getDrawable(Context ctx, String itemKey) {
        Iterator<HashMap<String, Object>> it = dataMap.iterator();
        while (it.hasNext()) {
            HashMap<String, Object> item = it.next();
            if (item.get("fartText").equals(itemKey)) {
                return ctx.getResources().getDrawable(((Integer) item.get("fartImage")).intValue());
            }
        }
        return null;
    }
}
