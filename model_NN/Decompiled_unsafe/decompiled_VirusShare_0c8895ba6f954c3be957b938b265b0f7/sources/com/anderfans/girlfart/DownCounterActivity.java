package com.anderfans.girlfart;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class DownCounterActivity extends Activity {
    /* access modifiers changed from: private */
    public int delay;
    /* access modifiers changed from: private */
    public boolean isThreadAvaliable = false;
    /* access modifiers changed from: private */
    public String itemKey;
    /* access modifiers changed from: private */
    public int nPlayCounts;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.downcounterview);
        this.itemKey = super.getIntent().getStringExtra("itemKey");
        this.delay = (int) super.getIntent().getLongExtra("delay", 15);
        this.nPlayCounts = super.getIntent().getIntExtra("nPlayCounts", 1);
        if (Data.getDrawable(this, this.itemKey) == null) {
            Log.e("", "itemKey is not exist! " + this.itemKey);
            finish();
            return;
        }
        findViewById(R.id.btnCounterBoard).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                DownCounterActivity.this.isThreadAvaliable = false;
                Data.playSound(DownCounterActivity.this.itemKey, DownCounterActivity.this.nPlayCounts);
            }
        });
        findViewById(R.id.btnReturn).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                DownCounterActivity.this.finish();
            }
        });
        findViewById(R.id.btnHidden).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                DownCounterActivity.this.backToHome();
            }
        });
        this.isThreadAvaliable = true;
        new Thread(new Runnable() {
            public void run() {
                int nDownCounts = DownCounterActivity.this.delay;
                final Button btnBoard = (Button) DownCounterActivity.this.findViewById(R.id.btnCounterBoard);
                while (nDownCounts > 0) {
                    final int n = nDownCounts;
                    DownCounterActivity.this.runOnUiThread(new Runnable() {
                        public void run() {
                            btnBoard.setText(new StringBuilder().append(n).toString());
                        }
                    });
                    if (DownCounterActivity.this.isThreadAvaliable) {
                        nDownCounts--;
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    } else {
                        return;
                    }
                }
                DownCounterActivity.this.runOnUiThread(new Runnable() {
                    public void run() {
                        btnBoard.setText("0");
                    }
                });
                Data.playSound(DownCounterActivity.this.itemKey, DownCounterActivity.this.nPlayCounts);
                DownCounterActivity.this.finish();
            }
        }).start();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.isThreadAvaliable = false;
        super.onDestroy();
    }

    /* access modifiers changed from: private */
    public void backToHome() {
        Intent i = new Intent();
        i.setAction("android.intent.action.MAIN");
        i.addCategory("android.intent.category.HOME");
        startActivity(i);
    }
}
