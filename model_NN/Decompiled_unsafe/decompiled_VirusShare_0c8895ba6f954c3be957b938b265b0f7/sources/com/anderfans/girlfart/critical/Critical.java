package com.anderfans.girlfart.critical;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import com.anderfans.girlfart.DialogUtil;
import com.anderfans.girlfart.R;
import com.anderfans.girlfart.critical.waps.WapsService;
import com.waps.AppConnect;
import com.waps.UpdatePointsNotifier;

public class Critical {
    public static final int CostBadMode = 200;
    public static final int CostPerSound = 100;
    public static final String conftoken = "girlfart";
    private static int points = -1;

    public static void setPoints(int point) {
        points = point;
    }

    public static int getPoints() {
        return points;
    }

    public static String getPointsStr() {
        if (points >= 0) {
            return String.valueOf(points) + "分";
        }
        return "{无法获取积分，请检查网络}";
    }

    public static boolean isSoundLockedAndAlert(Context ctx, String itemKey, UpdatePointsNotifier notifier) {
        boolean ret = isSoundLocked(ctx, itemKey);
        if (!ret) {
            return ret;
        }
        if (points < 100) {
            return true;
        }
        AppConnect.getInstance(ctx).spendPoints(100, notifier);
        unlockSound(ctx, itemKey);
        return false;
    }

    public static boolean isSoundLocked(Context ctx, String itemKey) {
        return ctx.getSharedPreferences(conftoken, 0).getBoolean(itemKey, true);
    }

    public static void unlockSound(Context ctx, String itemKey) {
        SharedPreferences.Editor edi = ctx.getSharedPreferences(conftoken, 0).edit();
        edi.putBoolean(itemKey, false);
        edi.commit();
    }

    public static boolean isBadModeFreeAndAlert(Context ctx, UpdatePointsNotifier notifier) {
        if (ctx.getSharedPreferences(conftoken, 0).getBoolean("badmode_sign", false)) {
            return true;
        }
        if (points < 200) {
            return false;
        }
        AppConnect.getInstance(ctx).spendPoints(CostBadMode, notifier);
        freeBadMode(ctx);
        return true;
    }

    private static void freeBadMode(Context ctx) {
        SharedPreferences.Editor edi = ctx.getSharedPreferences(conftoken, 0).edit();
        edi.putBoolean("badmode_sign", true);
        edi.commit();
    }

    public static void suggestUnlockSound(String itemKey, final Activity ctx) {
        DialogUtil.createTipDialog(ctx, "获取积分以解锁", String.valueOf(ctx.getResources().getString(R.string.criticaltip)) + "\n    您当前有积分：" + getPointsStr() + ",此功能需要积分：" + 100 + "分", new Runnable() {
            public void run() {
                WapsService.showMoreOffers(ctx);
            }
        }).show();
    }

    public static void suggestUnlockBadMode(final Activity ctx) {
        DialogUtil.createTipDialog(ctx, "获取积分以解锁", String.valueOf(ctx.getResources().getString(R.string.badmodecriticaltip)) + "\n    您当前有积分：" + getPointsStr() + ",此功能需要积分：" + ((int) CostBadMode) + "分", new Runnable() {
            public void run() {
                WapsService.showMoreOffers(ctx);
            }
        }).show();
    }
}
