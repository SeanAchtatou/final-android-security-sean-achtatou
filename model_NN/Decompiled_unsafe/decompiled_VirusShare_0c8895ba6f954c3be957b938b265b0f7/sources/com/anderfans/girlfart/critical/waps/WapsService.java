package com.anderfans.girlfart.critical.waps;

import android.app.Activity;
import com.waps.AppConnect;

public class WapsService {
    public static void showMoreOffers(Activity sender) {
        AppConnect.getInstance(sender).showOffers(sender);
    }

    public static void showAnderSoftwore(Activity sender) {
        AppConnect.getInstance(sender).showMore(sender);
    }
}
