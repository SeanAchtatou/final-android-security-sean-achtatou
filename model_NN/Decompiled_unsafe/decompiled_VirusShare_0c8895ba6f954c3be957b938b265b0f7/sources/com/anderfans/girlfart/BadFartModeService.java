package com.anderfans.girlfart;

import android.content.Context;
import java.util.List;
import java.util.Random;

public class BadFartModeService {
    public static final BadFartModeService Instance = new BadFartModeService();
    private boolean isBading = false;
    private Random r = new Random();
    private List<String> sounds;

    private BadFartModeService() {
        new Thread(new Runnable() {
            public void run() {
                while (true) {
                    try {
                        BadFartModeService.this.internalBadProc();
                        Thread.sleep(100);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();
    }

    public void beginBadFartMode(Context ctx, int delay) {
        this.sounds = Data.avaliableSounds(ctx);
        try {
            Thread.sleep((long) delay);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        this.isBading = true;
    }

    public boolean isBadingRun() {
        return this.isBading;
    }

    public void disableBadding() {
        this.isBading = false;
    }

    public void stopBadFartMode() {
        this.isBading = false;
    }

    /* access modifiers changed from: private */
    public void internalBadProc() throws InterruptedException {
        if (this.isBading) {
            Data.playSound(this.sounds.get(this.r.nextInt(this.sounds.size())));
            Thread.sleep(500);
        }
    }
}
