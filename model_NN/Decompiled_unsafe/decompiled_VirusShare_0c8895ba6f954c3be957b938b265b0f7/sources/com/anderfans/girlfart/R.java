package com.anderfans.girlfart;

public final class R {

    public static final class attr {
        public static final int backgroundColor = 2130771968;
        public static final int isTesting = 2130771972;
        public static final int primaryTextColor = 2130771969;
        public static final int refreshInterval = 2130771971;
        public static final int secondaryTextColor = 2130771970;
    }

    public static final class color {
        public static final int darkReadColor = 2131099649;
        public static final int normalReadColor = 2131099648;
    }

    public static final class drawable {
        public static final int ap_icon = 2130837504;
        public static final int bkapp = 2130837505;
        public static final int button_custom = 2130837506;
        public static final int buttondisable = 2130837507;
        public static final int buttonnormal = 2130837508;
        public static final int buttonpressed = 2130837509;
        public static final int dialogbox = 2130837510;
        public static final int drop_list = 2130837511;
        public static final int droplist = 2130837512;
        public static final int droplist_down = 2130837513;
        public static final int gf1 = 2130837514;
        public static final int gf10 = 2130837515;
        public static final int gf11 = 2130837516;
        public static final int gf12 = 2130837517;
        public static final int gf13 = 2130837518;
        public static final int gf14 = 2130837519;
        public static final int gf15 = 2130837520;
        public static final int gf16 = 2130837521;
        public static final int gf17 = 2130837522;
        public static final int gf18 = 2130837523;
        public static final int gf19 = 2130837524;
        public static final int gf2 = 2130837525;
        public static final int gf20 = 2130837526;
        public static final int gf21 = 2130837527;
        public static final int gf22 = 2130837528;
        public static final int gf23 = 2130837529;
        public static final int gf24 = 2130837530;
        public static final int gf25 = 2130837531;
        public static final int gf26 = 2130837532;
        public static final int gf27 = 2130837533;
        public static final int gf28 = 2130837534;
        public static final int gf29 = 2130837535;
        public static final int gf3 = 2130837536;
        public static final int gf30 = 2130837537;
        public static final int gf31 = 2130837538;
        public static final int gf4 = 2130837539;
        public static final int gf5 = 2130837540;
        public static final int gf6 = 2130837541;
        public static final int gf7 = 2130837542;
        public static final int gf8 = 2130837543;
        public static final int gf9 = 2130837544;
        public static final int icon = 2130837545;
        public static final int question = 2130837546;
        public static final int separator = 2130837547;
        public static final int topbar = 2130837548;
    }

    public static final class id {
        public static final int ad = 2131230732;
        public static final int adArea = 2131230731;
        public static final int bottomArea = 2131230727;
        public static final int btnAbout = 2131230733;
        public static final int btnAboutReturn = 2131230742;
        public static final int btnBadMode = 2131230730;
        public static final int btnCounterBoard = 2131230725;
        public static final int btnFeedback = 2131230736;
        public static final int btnHidden = 2131230724;
        public static final int btnOthersOffer = 2131230735;
        public static final int btnReturn = 2131230723;
        public static final int btnSelfOffer = 2131230734;
        public static final int btnTimedFart = 2131230729;
        public static final int dcBottomBar = 2131230722;
        public static final int etBadTimedDelay = 2131230720;
        public static final int etFartCount = 2131230739;
        public static final int etTimedDelay = 2131230738;
        public static final int fartChooser = 2131230728;
        public static final int fartGridView = 2131230737;
        public static final int ivFart = 2131230726;
        public static final int tvDlgContent = 2131230741;
        public static final int tvDlgTitle = 2131230740;
        public static final int tvFart = 2131230721;
    }

    public static final class layout {
        public static final int badmodesettingdialog = 2130903040;
        public static final int chooseitem = 2130903041;
        public static final int downcounterview = 2130903042;
        public static final int fartitem = 2130903043;
        public static final int main = 2130903044;
        public static final int timedfartsettingdialog = 2130903045;
        public static final int unidialogview = 2130903046;
    }

    public static final class raw {
        public static final int a = 2130968576;
        public static final int a1 = 2130968577;
        public static final int a2 = 2130968578;
        public static final int a3 = 2130968579;
        public static final int a4 = 2130968580;
        public static final int b = 2130968581;
        public static final int c = 2130968582;
        public static final int d = 2130968583;
        public static final int e = 2130968584;
        public static final int f = 2130968585;
        public static final int g = 2130968586;
        public static final int h = 2130968587;
        public static final int i = 2130968588;
        public static final int j = 2130968589;
        public static final int jinglebells = 2130968590;
        public static final int k = 2130968591;
        public static final int l = 2130968592;
        public static final int m = 2130968593;
        public static final int n = 2130968594;
        public static final int o = 2130968595;
        public static final int p = 2130968596;
        public static final int q = 2130968597;
        public static final int r = 2130968598;
        public static final int s = 2130968599;
        public static final int t = 2130968600;
        public static final int u = 2130968601;
        public static final int v = 2130968602;
        public static final int w = 2130968603;
        public static final int x = 2130968604;
        public static final int y = 2130968605;
        public static final int z = 2130968606;
    }

    public static final class string {
        public static final int about = 2131034115;
        public static final int app_name = 2131034112;
        public static final int badModeTip = 2131034116;
        public static final int badmodecriticaltip = 2131034114;
        public static final int criticaltip = 2131034113;
    }

    public static final class style {
        public static final int BasicButtonStyle = 2131165186;
        public static final int NoTitle = 2131165184;
        public static final int dialog = 2131165185;
        public static final int faceMenubtnStyle = 2131165190;
        public static final int largeNormalBtnStyle = 2131165189;
        public static final int middleFaceMenubtnStyle = 2131165191;
        public static final int middleSideBarBtnStyle = 2131165193;
        public static final int normalBtnStyle = 2131165188;
        public static final int sideBarBtnStyle = 2131165192;
        public static final int spinerStyle = 2131165187;
    }

    public static final class styleable {
        public static final int[] com_adwo_adsdk_AdwoAdView = {R.attr.backgroundColor, R.attr.primaryTextColor, R.attr.secondaryTextColor, R.attr.refreshInterval, R.attr.isTesting};
        public static final int com_adwo_adsdk_AdwoAdView_backgroundColor = 0;
        public static final int com_adwo_adsdk_AdwoAdView_isTesting = 4;
        public static final int com_adwo_adsdk_AdwoAdView_primaryTextColor = 1;
        public static final int com_adwo_adsdk_AdwoAdView_refreshInterval = 3;
        public static final int com_adwo_adsdk_AdwoAdView_secondaryTextColor = 2;
    }
}
