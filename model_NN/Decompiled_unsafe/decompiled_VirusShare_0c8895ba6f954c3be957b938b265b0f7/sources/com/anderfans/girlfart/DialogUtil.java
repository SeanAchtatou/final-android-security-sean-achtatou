package com.anderfans.girlfart;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.widget.TextView;

public class DialogUtil {
    public static Dialog createTipDialog(Context ctx, String title, String content) {
        return createTipDialog(ctx, title, content, null);
    }

    public static Dialog createTipDialog(Context ctx, String title, String content, final Runnable onClosed) {
        final Dialog dialog = new Dialog(ctx, R.style.dialog);
        dialog.setContentView((int) R.layout.unidialogview);
        ((TextView) dialog.findViewById(R.id.tvDlgTitle)).setText(title);
        ((TextView) dialog.findViewById(R.id.tvDlgContent)).setText(content);
        dialog.findViewById(R.id.btnAboutReturn).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                dialog.dismiss();
                if (onClosed != null) {
                    onClosed.run();
                }
            }
        });
        return dialog;
    }
}
