package com.anderfans.girlfart;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ListAdapter;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import com.anderfans.girlfart.critical.Critical;
import com.anderfans.girlfart.critical.waps.WapsService;
import com.waps.AppConnect;
import com.waps.UpdatePointsNotifier;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FartActivity extends Activity implements UpdatePointsNotifier {
    /* access modifiers changed from: private */
    public boolean isPusherAlive = true;
    protected String itemKey;
    private Thread pusher;
    /* access modifiers changed from: private */
    public int pusherDelay = 0;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.main);
        internalOnCreated();
        this.pusher = new Thread(new Runnable() {
            public void run() {
                while (FartActivity.this.isPusherAlive) {
                    try {
                        FartActivity fartActivity = FartActivity.this;
                        int access$1 = fartActivity.pusherDelay;
                        fartActivity.pusherDelay = access$1 + 1;
                        Thread.sleep((long) (120000 + (access$1 * 1000 * 360)));
                        AppConnect.getInstance(FartActivity.this).getPushAd();
                    } catch (Exception e) {
                        return;
                    }
                }
            }
        });
        this.pusher.start();
    }

    private void internalOnCreated() {
        initWaps();
        GridView gridview = (GridView) findViewById(R.id.fartGridView);
        Data.init(this, this);
        List<? extends Map<String, ?>> lstImageItem = Data.getData();
        gridview.setAdapter((ListAdapter) new SimpleAdapter(this, lstImageItem, R.layout.fartitem, new String[]{"fartImage", "fartText"}, new int[]{R.id.ivFart, R.id.tvFart}));
        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                String itemKey = (String) ((HashMap) arg0.getItemAtPosition(arg2)).get("fartText");
                if (Critical.isSoundLockedAndAlert(FartActivity.this, itemKey, FartActivity.this)) {
                    Critical.suggestUnlockSound(itemKey, FartActivity.this);
                } else {
                    Data.playSound(itemKey);
                }
            }
        });
        List<? extends Map<String, ?>> list = lstImageItem;
        SimpleAdapter chooserAdapter = new SimpleAdapter(this, list, R.layout.chooseitem, new String[]{"fartImage", "fartText"}, new int[]{R.id.ivFart, R.id.tvFart});
        Spinner fartChooser = (Spinner) findViewById(R.id.fartChooser);
        fartChooser.setAdapter((SpinnerAdapter) chooserAdapter);
        fartChooser.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                FartActivity.this.itemKey = (String) ((Map) arg0.getItemAtPosition(arg2)).get("fartText");
            }

            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
        findViewById(R.id.btnTimedFart).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                View DialogView = LayoutInflater.from(FartActivity.this).inflate((int) R.layout.timedfartsettingdialog, (ViewGroup) null);
                final EditText etFartCount = (EditText) DialogView.findViewById(R.id.etFartCount);
                final EditText etTimedDelay = (EditText) DialogView.findViewById(R.id.etTimedDelay);
                new AlertDialog.Builder(FartActivity.this).setTitle("延时放屁参数设定").setView(DialogView).setPositiveButton("确定", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        int delay = 15;
                        try {
                            delay = (int) Long.parseLong(etTimedDelay.getText().toString());
                            if (delay <= 0) {
                                delay = 1;
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        int ni = 1;
                        try {
                            ni = (int) Long.parseLong(etFartCount.getText().toString());
                            if (ni <= 0) {
                                ni = 1;
                            }
                        } catch (Exception e2) {
                            e2.printStackTrace();
                        }
                        FartActivity.this.navigateToDownCounterView((long) delay, ni);
                    }
                }).create().show();
            }
        });
        findViewById(R.id.btnFeedback).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                AppConnect.getInstance(FartActivity.this).showFeedback();
            }
        });
        findViewById(R.id.btnSelfOffer).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                WapsService.showAnderSoftwore(FartActivity.this);
            }
        });
        findViewById(R.id.btnOthersOffer).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                WapsService.showMoreOffers(FartActivity.this);
            }
        });
        findViewById(R.id.btnAbout).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                DialogUtil.createTipDialog(FartActivity.this, "提示", FartActivity.this.getResources().getString(R.string.about)).show();
            }
        });
        Button btn = (Button) findViewById(R.id.btnBadMode);
        if (BadFartModeService.Instance.isBadingRun()) {
            btn.setText("停止恶搞");
        } else {
            btn.setText("恶搞模式");
        }
        btn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                if (BadFartModeService.Instance.isBadingRun()) {
                    BadFartModeService.Instance.disableBadding();
                    FartActivity.this.runOnUiThread(new Runnable() {
                        public void run() {
                            ((Button) FartActivity.this.findViewById(R.id.btnBadMode)).setText("恶搞模式");
                        }
                    });
                    return;
                }
                DialogUtil.createTipDialog(FartActivity.this, "恶搞模式说明", FartActivity.this.getResources().getString(R.string.badModeTip), new Runnable() {
                    public void run() {
                        if (!Critical.isBadModeFreeAndAlert(FartActivity.this, FartActivity.this)) {
                            Critical.suggestUnlockBadMode(FartActivity.this);
                            return;
                        }
                        View DialogView = LayoutInflater.from(FartActivity.this).inflate((int) R.layout.badmodesettingdialog, (ViewGroup) null);
                        final EditText etBadTimedDelay = (EditText) DialogView.findViewById(R.id.etBadTimedDelay);
                        new AlertDialog.Builder(FartActivity.this).setTitle("邪搞d设定").setView(DialogView).setPositiveButton("确定", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                int delay = 0;
                                try {
                                    delay = (int) Long.parseLong(etBadTimedDelay.getText().toString());
                                    if (delay <= 0) {
                                        delay = 0;
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                FartActivity.this.backToHome();
                                BadFartModeService.Instance.beginBadFartMode(FartActivity.this, delay * 1000);
                            }
                        }).create().show();
                    }
                }).show();
            }
        });
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4 || event.getRepeatCount() != 0) {
            return true;
        }
        backToHome();
        return true;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.isPusherAlive = false;
        AppConnect.getInstance(this).finalize();
        this.pusher.interrupt();
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void navigateToDownCounterView(long delay, int nPlayCounts) {
        if (Critical.isSoundLockedAndAlert(this, this.itemKey, this)) {
            Critical.suggestUnlockSound(this.itemKey, this);
            return;
        }
        Intent intent = new Intent(this, DownCounterActivity.class);
        intent.putExtra("itemKey", this.itemKey);
        intent.putExtra("delay", delay);
        intent.putExtra("nPlayCounts", nPlayCounts);
        startActivity(intent);
    }

    /* access modifiers changed from: private */
    public void backToHome() {
        Intent i = new Intent();
        i.setAction("android.intent.action.MAIN");
        i.addCategory("android.intent.category.HOME");
        startActivity(i);
    }

    private void initWaps() {
        AppConnect.getInstance("e51be6b99daa21711e94e6eeb06dabd0", this);
        AppConnect.getInstance(this).getPoints(this);
        AppConnect.getInstance(this).setPushIcon(R.drawable.ap_icon);
    }

    public void getUpdatePoints(String arg0, int arg1) {
        Critical.setPoints(arg1);
    }

    public void getUpdatePointsFailed(String arg0) {
        Critical.setPoints(-1);
    }
}
