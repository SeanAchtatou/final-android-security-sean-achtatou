package com.adwo.adsdk;

import android.util.Log;

final class I implements Runnable {
    private /* synthetic */ H a;
    private final /* synthetic */ C0001b b;
    private final /* synthetic */ int c;
    private final /* synthetic */ boolean d;

    I(H h, C0001b bVar, int i, boolean z) {
        this.a = h;
        this.b = bVar;
        this.c = i;
        this.d = z;
    }

    public final void run() {
        try {
            this.a.a.addView(this.b);
            if (this.c == 0) {
                this.b.b();
                if (this.d) {
                    AdwoAdView.b(this.a.a, this.b);
                } else {
                    AdwoAdView.c(this.a.a, this.b);
                }
            } else {
                this.a.a.c = this.b;
            }
        } catch (Exception e) {
            Log.e("Adwo SDK", e.toString());
        } finally {
            this.a.a.a = false;
        }
    }
}
