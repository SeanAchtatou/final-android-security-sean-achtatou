package com.adwo.adsdk;

import android.view.MotionEvent;
import android.view.View;

/* renamed from: com.adwo.adsdk.s  reason: case insensitive filesystem */
final class C0018s implements View.OnTouchListener {
    private /* synthetic */ C0012m a;

    C0018s(C0012m mVar) {
        this.a = mVar;
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        return this.a.p.onTouchEvent(motionEvent);
    }
}
