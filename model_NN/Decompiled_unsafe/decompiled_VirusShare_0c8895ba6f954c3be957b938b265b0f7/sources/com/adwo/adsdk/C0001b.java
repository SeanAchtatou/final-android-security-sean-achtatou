package com.adwo.adsdk;

import android.app.Activity;
import android.app.AlertDialog;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.CornerPathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/* renamed from: com.adwo.adsdk.b  reason: case insensitive filesystem */
final class C0001b extends RelativeLayout implements Animation.AnimationListener, C0010k {
    private static final Typeface a = Typeface.create(Typeface.SANS_SERIF, 1);
    private static final Typeface b = Typeface.create(Typeface.SANS_SERIF, 0);
    private int c = -16777216;
    private int d = -1;
    private Drawable e;
    private Drawable f;
    private Drawable g;
    private Drawable h;
    /* access modifiers changed from: private */
    public C0008i i;
    private TextView j;
    private TextView k;
    /* access modifiers changed from: private */
    public W l = null;
    private ImageView m = null;
    /* access modifiers changed from: private */
    public ProgressBar n;
    /* access modifiers changed from: private */
    public boolean o;
    private C0000a p;
    private Activity q;

    private synchronized byte[] a(String str) {
        byte[] bArr;
        String lowerCase = str.substring(str.lastIndexOf("/") + 1, str.lastIndexOf(".")).toLowerCase();
        try {
            URL url = new URL(str);
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            try {
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestProperty("User-Agent", C0011l.h);
                if (C0011l.a != null) {
                    httpURLConnection.setRequestProperty("Cookie", C0011l.a);
                }
                httpURLConnection.setDoInput(true);
                httpURLConnection.connect();
                InputStream inputStream = httpURLConnection.getInputStream();
                while (true) {
                    int read = inputStream.read();
                    if (read == -1) {
                        break;
                    }
                    byteArrayOutputStream.write(read);
                }
                inputStream.close();
                bArr = byteArrayOutputStream.toByteArray();
                if (bArr == null || bArr.length <= 0) {
                    bArr = null;
                } else {
                    try {
                        this.p.a(lowerCase, bArr);
                    } catch (Exception e2) {
                    }
                }
            } catch (IOException e3) {
                bArr = null;
            }
        } catch (MalformedURLException e4) {
            bArr = null;
        }
        return bArr;
    }

    public final void a() {
        g();
        if (!(this.l == null || this.l.getBackground() == null)) {
            this.l.getBackground().setCallback(null);
            this.l.setBackgroundDrawable(null);
        }
        if (this.m != null && this.m.getBackground() != null) {
            this.m.getBackground().setCallback(null);
            this.m.setBackgroundDrawable(null);
        }
    }

    private void g() {
        if (this.e != null) {
            ((BitmapDrawable) this.e).getBitmap().recycle();
        }
        if (this.f != null) {
            ((BitmapDrawable) this.f).getBitmap().recycle();
        }
        if (this.g != null) {
            ((BitmapDrawable) this.g).getBitmap().recycle();
        }
        this.e = null;
        this.g = null;
        this.f = null;
        if (this.h != null) {
            ((BitmapDrawable) this.h).getBitmap().recycle();
        }
        this.h = null;
    }

    public final void b() {
        if (this.l != null) {
            this.l.a();
        }
    }

    public final void c() {
        if (this.l != null) {
            this.l.b();
            if (this.l.getBackground() != null) {
                this.l.getBackground().setCallback(null);
                this.l.setBackgroundDrawable(null);
            }
            this.l = null;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:104:0x0349, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:105:0x034a, code lost:
        r9 = r2;
        r2 = r1;
        r1 = r9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:75:0x02b7, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:76:0x02b8, code lost:
        r9 = r2;
        r2 = r1;
        r1 = r9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:93:0x032d, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:94:0x032e, code lost:
        r9 = r2;
        r2 = r1;
        r1 = r9;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:104:0x0349 A[ExcHandler: all (r2v70 'th' java.lang.Throwable A[CUSTOM_DECLARE]), Splitter:B:32:0x00e1] */
    /* JADX WARNING: Removed duplicated region for block: B:107:0x034f A[SYNTHETIC, Splitter:B:107:0x034f] */
    /* JADX WARNING: Removed duplicated region for block: B:126:0x0413  */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x0127  */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x01cd  */
    /* JADX WARNING: Removed duplicated region for block: B:93:0x032d A[ExcHandler: Error (r2v72 'e' java.lang.Error A[CUSTOM_DECLARE]), Splitter:B:32:0x00e1] */
    /* JADX WARNING: Removed duplicated region for block: B:98:0x0336 A[SYNTHETIC, Splitter:B:98:0x0336] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:95:0x0331=Splitter:B:95:0x0331, B:77:0x02bb=Splitter:B:77:0x02bb} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public C0001b(com.adwo.adsdk.C0008i r11, android.content.Context r12, int r13, int r14, double r15) {
        /*
            r10 = this;
            r10.<init>(r12)
            r1 = -16777216(0xffffffffff000000, float:-1.7014118E38)
            r10.c = r1
            r1 = -1
            r10.d = r1
            r1 = 0
            r10.l = r1
            r1 = 0
            r10.m = r1
            r0 = r12
            android.app.Activity r0 = (android.app.Activity) r0
            r1 = r0
            r10.q = r1
            com.adwo.adsdk.a r1 = r10.p
            if (r1 != 0) goto L_0x0020
            com.adwo.adsdk.a r1 = com.adwo.adsdk.C0000a.a(r12)
            r10.p = r1
        L_0x0020:
            r10.i = r11
            r11.j = r10
            r1 = 0
            r10.e = r1
            r1 = 0
            r10.g = r1
            r1 = 0
            r10.f = r1
            r1 = 0
            r10.n = r1
            r1 = 0
            r10.o = r1
            if (r11 == 0) goto L_0x027b
            r1 = 1
            r10.setFocusable(r1)
            r1 = 1
            r10.setClickable(r1)
            r2 = 0
            r1 = 0
            java.lang.String r3 = r11.b()
            if (r3 == 0) goto L_0x0494
            java.lang.String r1 = r11.b()
            int r1 = r1.length()
            r3 = r1
        L_0x004e:
            java.lang.String r4 = r11.c()     // Catch:{ Exception -> 0x048e, Error -> 0x048b }
            if (r4 == 0) goto L_0x03bc
            com.adwo.adsdk.W r1 = r10.l     // Catch:{ Exception -> 0x048e, Error -> 0x048b }
            if (r1 == 0) goto L_0x007f
            com.adwo.adsdk.W r1 = r10.l     // Catch:{ Exception -> 0x048e, Error -> 0x048b }
            android.graphics.drawable.Drawable r1 = r1.getBackground()     // Catch:{ Exception -> 0x048e, Error -> 0x048b }
            if (r1 == 0) goto L_0x007f
            com.adwo.adsdk.W r1 = r10.l     // Catch:{ Exception -> 0x048e, Error -> 0x048b }
            android.graphics.drawable.Drawable r1 = r1.getBackground()     // Catch:{ Exception -> 0x048e, Error -> 0x048b }
            android.graphics.drawable.BitmapDrawable r1 = (android.graphics.drawable.BitmapDrawable) r1     // Catch:{ Exception -> 0x048e, Error -> 0x048b }
            android.graphics.Bitmap r1 = r1.getBitmap()     // Catch:{ Exception -> 0x048e, Error -> 0x048b }
            r1.recycle()     // Catch:{ Exception -> 0x048e, Error -> 0x048b }
            com.adwo.adsdk.W r1 = r10.l     // Catch:{ Exception -> 0x048e, Error -> 0x048b }
            android.graphics.drawable.Drawable r1 = r1.getBackground()     // Catch:{ Exception -> 0x048e, Error -> 0x048b }
            r5 = 0
            r1.setCallback(r5)     // Catch:{ Exception -> 0x048e, Error -> 0x048b }
            com.adwo.adsdk.W r1 = r10.l     // Catch:{ Exception -> 0x048e, Error -> 0x048b }
            r5 = 0
            r1.setBackgroundDrawable(r5)     // Catch:{ Exception -> 0x048e, Error -> 0x048b }
        L_0x007f:
            android.widget.ImageView r1 = r10.m     // Catch:{ Exception -> 0x048e, Error -> 0x048b }
            if (r1 == 0) goto L_0x00aa
            android.widget.ImageView r1 = r10.m     // Catch:{ Exception -> 0x048e, Error -> 0x048b }
            android.graphics.drawable.Drawable r1 = r1.getBackground()     // Catch:{ Exception -> 0x048e, Error -> 0x048b }
            if (r1 == 0) goto L_0x00aa
            android.widget.ImageView r1 = r10.m     // Catch:{ Exception -> 0x048e, Error -> 0x048b }
            android.graphics.drawable.Drawable r1 = r1.getBackground()     // Catch:{ Exception -> 0x048e, Error -> 0x048b }
            android.graphics.drawable.BitmapDrawable r1 = (android.graphics.drawable.BitmapDrawable) r1     // Catch:{ Exception -> 0x048e, Error -> 0x048b }
            android.graphics.Bitmap r1 = r1.getBitmap()     // Catch:{ Exception -> 0x048e, Error -> 0x048b }
            r1.recycle()     // Catch:{ Exception -> 0x048e, Error -> 0x048b }
            android.widget.ImageView r1 = r10.m     // Catch:{ Exception -> 0x048e, Error -> 0x048b }
            android.graphics.drawable.Drawable r1 = r1.getBackground()     // Catch:{ Exception -> 0x048e, Error -> 0x048b }
            r5 = 0
            r1.setCallback(r5)     // Catch:{ Exception -> 0x048e, Error -> 0x048b }
            android.widget.ImageView r1 = r10.m     // Catch:{ Exception -> 0x048e, Error -> 0x048b }
            r5 = 0
            r1.setBackgroundDrawable(r5)     // Catch:{ Exception -> 0x048e, Error -> 0x048b }
        L_0x00aa:
            int r1 = r4.length()     // Catch:{ Exception -> 0x048e, Error -> 0x048b }
            if (r1 == 0) goto L_0x0296
            java.lang.String r1 = "/"
            int r1 = r4.lastIndexOf(r1)     // Catch:{ Exception -> 0x048e, Error -> 0x048b }
            int r1 = r1 + 1
            java.lang.String r5 = "."
            int r5 = r4.lastIndexOf(r5)     // Catch:{ Exception -> 0x048e, Error -> 0x048b }
            java.lang.String r1 = r4.substring(r1, r5)     // Catch:{ Exception -> 0x048e, Error -> 0x048b }
            java.lang.String r1 = r1.toLowerCase()     // Catch:{ Exception -> 0x048e, Error -> 0x048b }
            com.adwo.adsdk.a r5 = r10.p     // Catch:{ Exception -> 0x048e, Error -> 0x048b }
            boolean r5 = r5.a(r1)     // Catch:{ Exception -> 0x048e, Error -> 0x048b }
            if (r5 == 0) goto L_0x0285
            com.adwo.adsdk.a r5 = r10.p     // Catch:{ Exception -> 0x048e, Error -> 0x048b }
            byte[] r1 = r5.b(r1)     // Catch:{ Exception -> 0x048e, Error -> 0x048b }
            if (r1 == 0) goto L_0x02a7
            int r5 = r1.length     // Catch:{ Exception -> 0x048e, Error -> 0x048b }
            if (r5 <= 0) goto L_0x02a7
            java.io.ByteArrayInputStream r5 = new java.io.ByteArrayInputStream     // Catch:{ Exception -> 0x048e, Error -> 0x048b }
            r5.<init>(r1)     // Catch:{ Exception -> 0x048e, Error -> 0x048b }
            r1 = r5
        L_0x00df:
            java.lang.String r2 = ".gif"
            boolean r2 = r4.contains(r2)     // Catch:{ Exception -> 0x02b7, Error -> 0x032d, all -> 0x0349 }
            if (r2 != 0) goto L_0x00ef
            java.lang.String r2 = ".GIF"
            boolean r2 = r4.contains(r2)     // Catch:{ Exception -> 0x02b7, Error -> 0x032d, all -> 0x0349 }
            if (r2 == 0) goto L_0x0353
        L_0x00ef:
            android.widget.RelativeLayout$LayoutParams r2 = new android.widget.RelativeLayout$LayoutParams     // Catch:{ Exception -> 0x02b7, Error -> 0x032d, all -> 0x0349 }
            r2.<init>(r13, r14)     // Catch:{ Exception -> 0x02b7, Error -> 0x032d, all -> 0x0349 }
            if (r3 > 0) goto L_0x02aa
            r4 = 0
            r5 = 0
            r6 = 0
            r7 = 0
            r2.setMargins(r4, r5, r6, r7)     // Catch:{ Exception -> 0x02b7, Error -> 0x032d, all -> 0x0349 }
        L_0x00fd:
            r4 = 9
            r2.addRule(r4)     // Catch:{ Exception -> 0x02b7, Error -> 0x032d, all -> 0x0349 }
            com.adwo.adsdk.W r4 = new com.adwo.adsdk.W     // Catch:{ Exception -> 0x02b7, Error -> 0x032d, all -> 0x0349 }
            r4.<init>(r12)     // Catch:{ Exception -> 0x02b7, Error -> 0x032d, all -> 0x0349 }
            r10.l = r4     // Catch:{ Exception -> 0x02b7, Error -> 0x032d, all -> 0x0349 }
            com.adwo.adsdk.W r4 = r10.l     // Catch:{ Exception -> 0x02c8, Error -> 0x032d, all -> 0x0349 }
            r4.a(r1)     // Catch:{ Exception -> 0x02c8, Error -> 0x032d, all -> 0x0349 }
            com.adwo.adsdk.W r4 = r10.l     // Catch:{ Exception -> 0x02c8, Error -> 0x032d, all -> 0x0349 }
            r4.setLayoutParams(r2)     // Catch:{ Exception -> 0x02c8, Error -> 0x032d, all -> 0x0349 }
            com.adwo.adsdk.W r2 = r10.l     // Catch:{ Exception -> 0x02c8, Error -> 0x032d, all -> 0x0349 }
            r4 = 1
            r2.setId(r4)     // Catch:{ Exception -> 0x02c8, Error -> 0x032d, all -> 0x0349 }
            com.adwo.adsdk.W r2 = r10.l     // Catch:{ Exception -> 0x02c8, Error -> 0x032d, all -> 0x0349 }
            r10.addView(r2)     // Catch:{ Exception -> 0x02c8, Error -> 0x032d, all -> 0x0349 }
        L_0x011e:
            if (r1 == 0) goto L_0x0123
            r1.close()     // Catch:{ IOException -> 0x0485 }
        L_0x0123:
            r1 = 15
            if (r3 <= r1) goto L_0x0413
            android.widget.TextView r1 = new android.widget.TextView
            r1.<init>(r12)
            r10.j = r1
            android.widget.TextView r1 = r10.j
            java.lang.String r2 = r11.b()
            r1.setText(r2)
            android.widget.TextView r1 = r10.j
            android.graphics.Typeface r2 = com.adwo.adsdk.C0001b.a
            r1.setTypeface(r2)
            android.widget.TextView r1 = r10.j
            int r2 = r10.d
            r1.setTextColor(r2)
            r1 = 1096810496(0x41600000, float:14.0)
            int r2 = r14 / 5
            r4 = 10
            int r2 = r2 - r4
            float r2 = (float) r2
            float r1 = r1 + r2
            r2 = 60
            if (r14 <= r2) goto L_0x015b
            r1 = 1098907648(0x41800000, float:16.0)
            int r2 = r14 / 5
            r4 = 10
            int r2 = r2 - r4
            float r2 = (float) r2
            float r1 = r1 + r2
        L_0x015b:
            android.widget.TextView r2 = r10.j
            r2.setTextSize(r1)
            android.widget.TextView r1 = r10.j
            android.text.TextUtils$TruncateAt r2 = android.text.TextUtils.TruncateAt.END
            r1.setEllipsize(r2)
            android.widget.TextView r1 = r10.j
            r2 = 1
            r1.setSingleLine(r2)
            android.widget.RelativeLayout$LayoutParams r1 = new android.widget.RelativeLayout$LayoutParams
            r2 = -1
            r4 = -1
            r1.<init>(r2, r4)
            android.widget.ImageView r2 = r10.m
            if (r2 == 0) goto L_0x017d
            r2 = 1
            r4 = 1
            r1.addRule(r2, r4)
        L_0x017d:
            double r4 = (double) r14
            r6 = 4613937818241073152(0x4008000000000000, double:3.0)
            double r4 = r4 / r6
            int r2 = (int) r4
            r4 = 10
            int r2 = r2 - r4
            r4 = 6
            r5 = 0
            r6 = 0
            r1.setMargins(r4, r2, r5, r6)
            android.widget.TextView r2 = r10.j
            r2.setLayoutParams(r1)
            android.widget.TextView r1 = r10.j
            r2 = 2
            r1.setId(r2)
            android.widget.TextView r1 = r10.j
            r10.addView(r1)
        L_0x019b:
            android.widget.RelativeLayout$LayoutParams r1 = new android.widget.RelativeLayout$LayoutParams
            r2 = 44
            r4 = 44
            r1.<init>(r2, r4)
            r2 = 44
            int r2 = r14 - r2
            int r2 = r2 / 2
            r4 = 0
            r1.setMargins(r2, r2, r4, r2)
            android.widget.ProgressBar r2 = new android.widget.ProgressBar
            r2.<init>(r12)
            r10.n = r2
            android.widget.ProgressBar r2 = r10.n
            r4 = 1
            r2.setIndeterminate(r4)
            android.widget.ProgressBar r2 = r10.n
            r2.setLayoutParams(r1)
            android.widget.ProgressBar r1 = r10.n
            r2 = 4
            r1.setVisibility(r2)
            android.widget.ProgressBar r1 = r10.n
            r10.addView(r1)
            if (r3 <= 0) goto L_0x027b
            android.widget.TextView r1 = new android.widget.TextView
            r1.<init>(r12)
            r10.k = r1
            android.widget.TextView r1 = r10.k
            r2 = 5
            r1.setGravity(r2)
            android.widget.TextView r1 = r10.k
            android.graphics.Typeface r2 = com.adwo.adsdk.C0001b.b
            r1.setTypeface(r2)
            android.widget.TextView r1 = r10.k
            int r2 = r10.d
            r1.setTextColor(r2)
            android.widget.TextView r1 = r10.k
            r2 = 1092616192(0x41200000, float:10.0)
            r1.setTextSize(r2)
            android.widget.TextView r1 = r10.k
            java.lang.String r2 = "安沃传媒"
            r1.setText(r2)
            android.widget.TextView r1 = r10.k
            r2 = 3
            r1.setId(r2)
            android.widget.RelativeLayout$LayoutParams r1 = new android.widget.RelativeLayout$LayoutParams
            r2 = -2
            r3 = -2
            r1.<init>(r2, r3)
            r2 = 4611686018427387904(0x4000000000000000, double:2.0)
            double r2 = r2 * r15
            int r2 = (int) r2
            r3 = 0
            int r2 = r2 * 6
            int r2 = r14 - r2
            r4 = 6
            r5 = 0
            r1.setMargins(r3, r2, r4, r5)
            r2 = 11
            r1.addRule(r2)
            android.widget.TextView r2 = r10.k
            r2.setLayoutParams(r1)
            android.widget.TextView r1 = r10.k
            r10.addView(r1)
            android.widget.ImageView r1 = new android.widget.ImageView
            r1.<init>(r12)
            r2 = 0
            android.content.Context r3 = r10.getContext()     // Catch:{ IOException -> 0x047c }
            android.content.res.AssetManager r3 = r3.getAssets()     // Catch:{ IOException -> 0x047c }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x047c }
            java.lang.String r5 = "t"
            r4.<init>(r5)     // Catch:{ IOException -> 0x047c }
            byte r5 = r11.f     // Catch:{ IOException -> 0x047c }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ IOException -> 0x047c }
            java.lang.String r5 = ".png"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ IOException -> 0x047c }
            java.lang.String r4 = r4.toString()     // Catch:{ IOException -> 0x047c }
            java.io.InputStream r3 = r3.open(r4)     // Catch:{ IOException -> 0x047c }
            android.graphics.Bitmap r2 = android.graphics.BitmapFactory.decodeStream(r3)     // Catch:{ IOException -> 0x047c }
        L_0x024d:
            if (r2 == 0) goto L_0x027b
            int r3 = r2.getHeight()
            int r3 = r14 - r3
            int r3 = r3 / 2
            android.widget.RelativeLayout$LayoutParams r4 = new android.widget.RelativeLayout$LayoutParams
            r5 = 4629418941960159232(0x403f000000000000, double:31.0)
            double r5 = r5 * r15
            int r5 = (int) r5
            r6 = 4629418941960159232(0x403f000000000000, double:31.0)
            double r6 = r6 * r15
            int r6 = (int) r6
            r4.<init>(r5, r6)
            r5 = 0
            r4.setMargins(r3, r3, r5, r3)
            r3 = 11
            r4.addRule(r3)
            r1.setLayoutParams(r4)
            android.graphics.drawable.BitmapDrawable r3 = new android.graphics.drawable.BitmapDrawable
            r3.<init>(r2)
            r1.setBackgroundDrawable(r3)
            r10.addView(r1)
        L_0x027b:
            r1 = -1
            r10.a(r1)
            r1 = -16777216(0xffffffffff000000, float:-1.7014118E38)
            r10.setBackgroundColor(r1)
            return
        L_0x0285:
            byte[] r1 = r10.a(r4)     // Catch:{ Exception -> 0x048e, Error -> 0x048b }
            if (r1 == 0) goto L_0x02a7
            int r5 = r1.length     // Catch:{ Exception -> 0x048e, Error -> 0x048b }
            if (r5 <= 0) goto L_0x02a7
            java.io.ByteArrayInputStream r5 = new java.io.ByteArrayInputStream     // Catch:{ Exception -> 0x048e, Error -> 0x048b }
            r5.<init>(r1)     // Catch:{ Exception -> 0x048e, Error -> 0x048b }
            r1 = r5
            goto L_0x00df
        L_0x0296:
            android.content.Context r1 = r10.getContext()     // Catch:{ Exception -> 0x048e, Error -> 0x048b }
            android.content.res.AssetManager r1 = r1.getAssets()     // Catch:{ Exception -> 0x048e, Error -> 0x048b }
            java.lang.String r5 = "adwo_logo.png"
            java.io.InputStream r1 = r1.open(r5)     // Catch:{ Exception -> 0x048e, Error -> 0x048b }
            android.graphics.BitmapFactory.decodeStream(r1)     // Catch:{ Exception -> 0x048e, Error -> 0x048b }
        L_0x02a7:
            r1 = r2
            goto L_0x00df
        L_0x02aa:
            r4 = 0
            int r4 = r14 - r4
            int r4 = r4 / 2
            int r5 = r4 / 2
            r6 = 0
            r2.setMargins(r5, r4, r6, r4)     // Catch:{ Exception -> 0x02b7, Error -> 0x032d, all -> 0x0349 }
            goto L_0x00fd
        L_0x02b7:
            r2 = move-exception
            r9 = r2
            r2 = r1
            r1 = r9
        L_0x02bb:
            r1.printStackTrace()     // Catch:{ all -> 0x0488 }
            if (r2 == 0) goto L_0x0123
            r2.close()     // Catch:{ IOException -> 0x02c5 }
            goto L_0x0123
        L_0x02c5:
            r1 = move-exception
            goto L_0x0123
        L_0x02c8:
            r2 = move-exception
            r2.printStackTrace()     // Catch:{ Exception -> 0x02b7, Error -> 0x032d, all -> 0x0349 }
            r2 = 0
            r10.l = r2     // Catch:{ Exception -> 0x02b7, Error -> 0x032d, all -> 0x0349 }
            android.graphics.Bitmap r2 = android.graphics.BitmapFactory.decodeStream(r1)     // Catch:{ Exception -> 0x02b7, Error -> 0x032d, all -> 0x0349 }
            if (r2 != 0) goto L_0x02e7
            android.content.Context r2 = r10.getContext()     // Catch:{ Exception -> 0x02b7, Error -> 0x032d, all -> 0x0349 }
            android.content.res.AssetManager r2 = r2.getAssets()     // Catch:{ Exception -> 0x02b7, Error -> 0x032d, all -> 0x0349 }
            java.lang.String r4 = "adwo_logo.png"
            java.io.InputStream r2 = r2.open(r4)     // Catch:{ Exception -> 0x02b7, Error -> 0x032d, all -> 0x0349 }
            android.graphics.Bitmap r2 = android.graphics.BitmapFactory.decodeStream(r2)     // Catch:{ Exception -> 0x02b7, Error -> 0x032d, all -> 0x0349 }
        L_0x02e7:
            if (r2 == 0) goto L_0x011e
            int r4 = r2.getWidth()     // Catch:{ Exception -> 0x02b7, Error -> 0x032d, all -> 0x0349 }
            int r5 = r2.getHeight()     // Catch:{ Exception -> 0x02b7, Error -> 0x032d, all -> 0x0349 }
            double r6 = (double) r4     // Catch:{ Exception -> 0x02b7, Error -> 0x032d, all -> 0x0349 }
            double r6 = r6 * r15
            int r4 = (int) r6     // Catch:{ Exception -> 0x02b7, Error -> 0x032d, all -> 0x0349 }
            double r5 = (double) r5     // Catch:{ Exception -> 0x02b7, Error -> 0x032d, all -> 0x0349 }
            double r5 = r5 * r15
            int r5 = (int) r5     // Catch:{ Exception -> 0x02b7, Error -> 0x032d, all -> 0x0349 }
            android.widget.RelativeLayout$LayoutParams r6 = new android.widget.RelativeLayout$LayoutParams     // Catch:{ Exception -> 0x02b7, Error -> 0x032d, all -> 0x0349 }
            r6.<init>(r4, r5)     // Catch:{ Exception -> 0x02b7, Error -> 0x032d, all -> 0x0349 }
            if (r3 > 0) goto L_0x033e
            r4 = 0
            r5 = 0
            r7 = 0
            r8 = 0
            r6.setMargins(r4, r5, r7, r8)     // Catch:{ Exception -> 0x02b7, Error -> 0x032d, all -> 0x0349 }
        L_0x0305:
            r4 = 9
            r6.addRule(r4)     // Catch:{ Exception -> 0x02b7, Error -> 0x032d, all -> 0x0349 }
            android.widget.ImageView r4 = new android.widget.ImageView     // Catch:{ Exception -> 0x02b7, Error -> 0x032d, all -> 0x0349 }
            r4.<init>(r12)     // Catch:{ Exception -> 0x02b7, Error -> 0x032d, all -> 0x0349 }
            r10.m = r4     // Catch:{ Exception -> 0x02b7, Error -> 0x032d, all -> 0x0349 }
            android.widget.ImageView r4 = r10.m     // Catch:{ Exception -> 0x02b7, Error -> 0x032d, all -> 0x0349 }
            r4.setLayoutParams(r6)     // Catch:{ Exception -> 0x02b7, Error -> 0x032d, all -> 0x0349 }
            android.widget.ImageView r4 = r10.m     // Catch:{ Exception -> 0x02b7, Error -> 0x032d, all -> 0x0349 }
            android.graphics.drawable.BitmapDrawable r5 = new android.graphics.drawable.BitmapDrawable     // Catch:{ Exception -> 0x02b7, Error -> 0x032d, all -> 0x0349 }
            r5.<init>(r2)     // Catch:{ Exception -> 0x02b7, Error -> 0x032d, all -> 0x0349 }
            r4.setBackgroundDrawable(r5)     // Catch:{ Exception -> 0x02b7, Error -> 0x032d, all -> 0x0349 }
            android.widget.ImageView r2 = r10.m     // Catch:{ Exception -> 0x02b7, Error -> 0x032d, all -> 0x0349 }
            r4 = 1
            r2.setId(r4)     // Catch:{ Exception -> 0x02b7, Error -> 0x032d, all -> 0x0349 }
            android.widget.ImageView r2 = r10.m     // Catch:{ Exception -> 0x02b7, Error -> 0x032d, all -> 0x0349 }
            r10.addView(r2)     // Catch:{ Exception -> 0x02b7, Error -> 0x032d, all -> 0x0349 }
            goto L_0x011e
        L_0x032d:
            r2 = move-exception
            r9 = r2
            r2 = r1
            r1 = r9
        L_0x0331:
            r1.printStackTrace()     // Catch:{ all -> 0x0488 }
            if (r2 == 0) goto L_0x0123
            r2.close()     // Catch:{ IOException -> 0x033b }
            goto L_0x0123
        L_0x033b:
            r1 = move-exception
            goto L_0x0123
        L_0x033e:
            int r4 = r14 - r5
            int r4 = r4 / 2
            int r5 = r4 / 2
            r7 = 0
            r6.setMargins(r5, r4, r7, r4)     // Catch:{ Exception -> 0x02b7, Error -> 0x032d, all -> 0x0349 }
            goto L_0x0305
        L_0x0349:
            r2 = move-exception
            r9 = r2
            r2 = r1
            r1 = r9
        L_0x034d:
            if (r2 == 0) goto L_0x0352
            r2.close()     // Catch:{ IOException -> 0x0482 }
        L_0x0352:
            throw r1
        L_0x0353:
            android.graphics.Bitmap r2 = android.graphics.BitmapFactory.decodeStream(r1)     // Catch:{ Exception -> 0x02b7, Error -> 0x032d, all -> 0x0349 }
            if (r2 != 0) goto L_0x036b
            android.content.Context r2 = r10.getContext()     // Catch:{ Exception -> 0x02b7, Error -> 0x032d, all -> 0x0349 }
            android.content.res.AssetManager r2 = r2.getAssets()     // Catch:{ Exception -> 0x02b7, Error -> 0x032d, all -> 0x0349 }
            java.lang.String r4 = "adwo_logo.png"
            java.io.InputStream r2 = r2.open(r4)     // Catch:{ Exception -> 0x02b7, Error -> 0x032d, all -> 0x0349 }
            android.graphics.Bitmap r2 = android.graphics.BitmapFactory.decodeStream(r2)     // Catch:{ Exception -> 0x02b7, Error -> 0x032d, all -> 0x0349 }
        L_0x036b:
            if (r2 == 0) goto L_0x011e
            int r4 = r2.getWidth()     // Catch:{ Exception -> 0x02b7, Error -> 0x032d, all -> 0x0349 }
            int r5 = r2.getHeight()     // Catch:{ Exception -> 0x02b7, Error -> 0x032d, all -> 0x0349 }
            double r6 = (double) r4     // Catch:{ Exception -> 0x02b7, Error -> 0x032d, all -> 0x0349 }
            double r6 = r6 * r15
            int r4 = (int) r6     // Catch:{ Exception -> 0x02b7, Error -> 0x032d, all -> 0x0349 }
            double r5 = (double) r5     // Catch:{ Exception -> 0x02b7, Error -> 0x032d, all -> 0x0349 }
            double r5 = r5 * r15
            int r5 = (int) r5     // Catch:{ Exception -> 0x02b7, Error -> 0x032d, all -> 0x0349 }
            android.widget.RelativeLayout$LayoutParams r6 = new android.widget.RelativeLayout$LayoutParams     // Catch:{ Exception -> 0x02b7, Error -> 0x032d, all -> 0x0349 }
            r6.<init>(r4, r5)     // Catch:{ Exception -> 0x02b7, Error -> 0x032d, all -> 0x0349 }
            if (r3 > 0) goto L_0x03b1
            r4 = 0
            r5 = 0
            r7 = 0
            r8 = 0
            r6.setMargins(r4, r5, r7, r8)     // Catch:{ Exception -> 0x02b7, Error -> 0x032d, all -> 0x0349 }
        L_0x0389:
            r4 = 9
            r6.addRule(r4)     // Catch:{ Exception -> 0x02b7, Error -> 0x032d, all -> 0x0349 }
            android.widget.ImageView r4 = new android.widget.ImageView     // Catch:{ Exception -> 0x02b7, Error -> 0x032d, all -> 0x0349 }
            r4.<init>(r12)     // Catch:{ Exception -> 0x02b7, Error -> 0x032d, all -> 0x0349 }
            r10.m = r4     // Catch:{ Exception -> 0x02b7, Error -> 0x032d, all -> 0x0349 }
            android.widget.ImageView r4 = r10.m     // Catch:{ Exception -> 0x02b7, Error -> 0x032d, all -> 0x0349 }
            r4.setLayoutParams(r6)     // Catch:{ Exception -> 0x02b7, Error -> 0x032d, all -> 0x0349 }
            android.widget.ImageView r4 = r10.m     // Catch:{ Exception -> 0x02b7, Error -> 0x032d, all -> 0x0349 }
            android.graphics.drawable.BitmapDrawable r5 = new android.graphics.drawable.BitmapDrawable     // Catch:{ Exception -> 0x02b7, Error -> 0x032d, all -> 0x0349 }
            r5.<init>(r2)     // Catch:{ Exception -> 0x02b7, Error -> 0x032d, all -> 0x0349 }
            r4.setBackgroundDrawable(r5)     // Catch:{ Exception -> 0x02b7, Error -> 0x032d, all -> 0x0349 }
            android.widget.ImageView r2 = r10.m     // Catch:{ Exception -> 0x02b7, Error -> 0x032d, all -> 0x0349 }
            r4 = 1
            r2.setId(r4)     // Catch:{ Exception -> 0x02b7, Error -> 0x032d, all -> 0x0349 }
            android.widget.ImageView r2 = r10.m     // Catch:{ Exception -> 0x02b7, Error -> 0x032d, all -> 0x0349 }
            r10.addView(r2)     // Catch:{ Exception -> 0x02b7, Error -> 0x032d, all -> 0x0349 }
            goto L_0x011e
        L_0x03b1:
            int r4 = r14 - r5
            int r4 = r4 / 2
            int r5 = r4 / 2
            r7 = 0
            r6.setMargins(r5, r4, r7, r4)     // Catch:{ Exception -> 0x02b7, Error -> 0x032d, all -> 0x0349 }
            goto L_0x0389
        L_0x03bc:
            if (r3 <= 0) goto L_0x0491
            android.content.Context r1 = r10.getContext()     // Catch:{ Exception -> 0x048e, Error -> 0x048b }
            android.content.res.AssetManager r1 = r1.getAssets()     // Catch:{ Exception -> 0x048e, Error -> 0x048b }
            java.lang.String r4 = "adwo_logo.png"
            java.io.InputStream r1 = r1.open(r4)     // Catch:{ Exception -> 0x048e, Error -> 0x048b }
            android.graphics.Bitmap r1 = android.graphics.BitmapFactory.decodeStream(r1)     // Catch:{ Exception -> 0x048e, Error -> 0x048b }
            if (r1 == 0) goto L_0x0491
            int r4 = r1.getWidth()     // Catch:{ Exception -> 0x048e, Error -> 0x048b }
            int r5 = r1.getHeight()     // Catch:{ Exception -> 0x048e, Error -> 0x048b }
            double r6 = (double) r4     // Catch:{ Exception -> 0x048e, Error -> 0x048b }
            double r6 = r6 * r15
            int r4 = (int) r6     // Catch:{ Exception -> 0x048e, Error -> 0x048b }
            double r5 = (double) r5     // Catch:{ Exception -> 0x048e, Error -> 0x048b }
            double r5 = r5 * r15
            int r5 = (int) r5     // Catch:{ Exception -> 0x048e, Error -> 0x048b }
            android.widget.RelativeLayout$LayoutParams r6 = new android.widget.RelativeLayout$LayoutParams     // Catch:{ Exception -> 0x048e, Error -> 0x048b }
            r6.<init>(r4, r5)     // Catch:{ Exception -> 0x048e, Error -> 0x048b }
            int r4 = r14 - r5
            int r4 = r4 / 2
            int r5 = r4 / 2
            r7 = 0
            r6.setMargins(r5, r4, r7, r4)     // Catch:{ Exception -> 0x048e, Error -> 0x048b }
            android.widget.ImageView r4 = new android.widget.ImageView     // Catch:{ Exception -> 0x048e, Error -> 0x048b }
            r4.<init>(r12)     // Catch:{ Exception -> 0x048e, Error -> 0x048b }
            r10.m = r4     // Catch:{ Exception -> 0x048e, Error -> 0x048b }
            android.widget.ImageView r4 = r10.m     // Catch:{ Exception -> 0x048e, Error -> 0x048b }
            r4.setLayoutParams(r6)     // Catch:{ Exception -> 0x048e, Error -> 0x048b }
            android.widget.ImageView r4 = r10.m     // Catch:{ Exception -> 0x048e, Error -> 0x048b }
            android.graphics.drawable.BitmapDrawable r5 = new android.graphics.drawable.BitmapDrawable     // Catch:{ Exception -> 0x048e, Error -> 0x048b }
            r5.<init>(r1)     // Catch:{ Exception -> 0x048e, Error -> 0x048b }
            r4.setBackgroundDrawable(r5)     // Catch:{ Exception -> 0x048e, Error -> 0x048b }
            android.widget.ImageView r1 = r10.m     // Catch:{ Exception -> 0x048e, Error -> 0x048b }
            r4 = 1
            r1.setId(r4)     // Catch:{ Exception -> 0x048e, Error -> 0x048b }
            android.widget.ImageView r1 = r10.m     // Catch:{ Exception -> 0x048e, Error -> 0x048b }
            r10.addView(r1)     // Catch:{ Exception -> 0x048e, Error -> 0x048b }
            r1 = r2
            goto L_0x011e
        L_0x0413:
            android.widget.TextView r1 = new android.widget.TextView
            r1.<init>(r12)
            r10.j = r1
            android.widget.TextView r1 = r10.j
            java.lang.String r2 = r11.b()
            r1.setText(r2)
            android.widget.TextView r1 = r10.j
            android.graphics.Typeface r2 = com.adwo.adsdk.C0001b.a
            r1.setTypeface(r2)
            android.widget.TextView r1 = r10.j
            int r2 = r10.d
            r1.setTextColor(r2)
            r1 = 1098907648(0x41800000, float:16.0)
            int r2 = r14 / 5
            r4 = 10
            int r2 = r2 - r4
            float r2 = (float) r2
            float r1 = r1 + r2
            r2 = 60
            if (r14 <= r2) goto L_0x0441
            r2 = 1073741824(0x40000000, float:2.0)
            float r1 = r1 + r2
        L_0x0441:
            android.widget.TextView r2 = r10.j
            r2.setTextSize(r1)
            android.widget.TextView r1 = r10.j
            r2 = 1
            r1.setSingleLine(r2)
            android.widget.RelativeLayout$LayoutParams r1 = new android.widget.RelativeLayout$LayoutParams
            r2 = -2
            r4 = -2
            r1.<init>(r2, r4)
            android.widget.ImageView r2 = r10.m
            if (r2 == 0) goto L_0x045c
            r2 = 1
            r4 = 1
            r1.addRule(r2, r4)
        L_0x045c:
            double r4 = (double) r14
            r6 = 4613937818241073152(0x4008000000000000, double:3.0)
            double r4 = r4 / r6
            int r2 = (int) r4
            r4 = 10
            int r2 = r2 - r4
            r4 = 6
            r5 = 0
            r6 = 0
            r1.setMargins(r4, r2, r5, r6)
            android.widget.TextView r2 = r10.j
            r2.setLayoutParams(r1)
            android.widget.TextView r1 = r10.j
            r2 = 2
            r1.setId(r2)
            android.widget.TextView r1 = r10.j
            r10.addView(r1)
            goto L_0x019b
        L_0x047c:
            r3 = move-exception
            r3.printStackTrace()
            goto L_0x024d
        L_0x0482:
            r2 = move-exception
            goto L_0x0352
        L_0x0485:
            r1 = move-exception
            goto L_0x0123
        L_0x0488:
            r1 = move-exception
            goto L_0x034d
        L_0x048b:
            r1 = move-exception
            goto L_0x0331
        L_0x048e:
            r1 = move-exception
            goto L_0x02bb
        L_0x0491:
            r1 = r2
            goto L_0x011e
        L_0x0494:
            r3 = r1
            goto L_0x004e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adwo.adsdk.C0001b.<init>(com.adwo.adsdk.i, android.content.Context, int, int, double):void");
    }

    public final void a(int i2) {
        this.d = -16777216 | i2;
        this.j.setTextColor(this.d);
        postInvalidate();
    }

    public final void setBackgroundColor(int i2) {
        this.c = -16777216 | i2;
    }

    /* access modifiers changed from: protected */
    public final C0008i d() {
        return this.i;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.adwo.adsdk.b.a(android.graphics.Rect, boolean):android.graphics.drawable.Drawable
     arg types: [android.graphics.Rect, int]
     candidates:
      com.adwo.adsdk.b.a(com.adwo.adsdk.b, boolean):void
      com.adwo.adsdk.b.a(android.graphics.Rect, boolean):android.graphics.drawable.Drawable */
    /* access modifiers changed from: protected */
    public final void onSizeChanged(int i2, int i3, int i4, int i5) {
        super.onSizeChanged(i2, i3, i4, i5);
        Typeface typeface = this.j.getTypeface();
        String b2 = this.i.b();
        if (b2 != null) {
            Paint paint = new Paint();
            paint.setTypeface(typeface);
            paint.setTextSize(this.j.getTextSize());
            paint.measureText(b2);
        }
        if (this.k != null) {
            this.k.setVisibility(0);
        }
        if (i2 != 0 && i3 != 0) {
            try {
                Rect rect = new Rect(0, 0, i2, i3);
                g();
                int i6 = this.c;
                this.e = a(rect, false);
                this.g = a(rect, false);
                int i7 = this.c;
                this.f = a(rect, true);
                setBackgroundDrawable(this.e);
            } catch (Error e2) {
            }
        }
    }

    private static Drawable a(Rect rect, boolean z) {
        Bitmap createBitmap = Bitmap.createBitmap(rect.width(), rect.height(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(createBitmap);
        if (z) {
            Paint paint = new Paint();
            paint.setAntiAlias(true);
            paint.setColor(-1147097);
            paint.setStyle(Paint.Style.STROKE);
            paint.setStrokeWidth(3.0f);
            paint.setPathEffect(new CornerPathEffect(3.0f));
            Path path = new Path();
            path.addRoundRect(new RectF(rect), 3.0f, 3.0f, Path.Direction.CW);
            canvas.drawPath(path, paint);
        }
        return new BitmapDrawable(createBitmap);
    }

    public final void e() {
        post(new C0002c(this));
    }

    public final void f() {
        post(new C0003d(this));
    }

    public final boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 == 66 || i2 == 23) {
            setPressed(true);
        }
        return super.onKeyDown(i2, keyEvent);
    }

    public final boolean onKeyUp(int i2, KeyEvent keyEvent) {
        if (i2 == 66 || i2 == 23) {
            h();
        }
        setPressed(false);
        return super.onKeyUp(i2, keyEvent);
    }

    public final boolean dispatchTouchEvent(MotionEvent motionEvent) {
        int action = motionEvent.getAction();
        if (action == 0) {
            setPressed(true);
        } else if (action == 2) {
            float x = motionEvent.getX();
            float y = motionEvent.getY();
            int left = getLeft();
            int top = getTop();
            int right = getRight();
            int bottom = getBottom();
            if (x < ((float) left) || x > ((float) right) || y < ((float) top) || y > ((float) bottom)) {
                setPressed(false);
            } else {
                setPressed(true);
            }
        } else if (action == 1) {
            if (isPressed()) {
                h();
            }
            setPressed(false);
        } else if (action == 3) {
            setPressed(false);
        }
        return super.dispatchTouchEvent(motionEvent);
    }

    public final boolean dispatchTrackballEvent(MotionEvent motionEvent) {
        if (motionEvent.getAction() == 0) {
            setPressed(true);
        } else if (motionEvent.getAction() == 1) {
            if (hasFocus()) {
                h();
            }
            setPressed(false);
        }
        return super.onTrackballEvent(motionEvent);
    }

    /* access modifiers changed from: protected */
    public final void onFocusChanged(boolean z, int i2, Rect rect) {
        super.onFocusChanged(z, i2, rect);
        if (z) {
            setBackgroundDrawable(this.f);
        } else {
            setBackgroundDrawable(this.e);
        }
    }

    public final void setPressed(boolean z) {
        Drawable drawable;
        if ((!z || !this.o) && isPressed() != z) {
            Drawable drawable2 = this.e;
            int i2 = this.d;
            if (z) {
                this.h = getBackground();
                drawable = this.g;
                i2 = -16777216;
            } else {
                drawable = this.h;
            }
            setBackgroundDrawable(drawable);
            if (this.j != null) {
                this.j.setTextColor(i2);
            }
            if (this.k != null) {
                this.k.setTextColor(i2);
            }
            super.setPressed(z);
            invalidate();
        }
    }

    private void h() {
        float f2;
        float f3;
        if (this.i != null && isPressed()) {
            setPressed(false);
            if (!this.o) {
                this.o = true;
                if (this.l == null && this.m == null) {
                    this.i.a();
                    this.o = false;
                    return;
                }
                AnimationSet animationSet = new AnimationSet(true);
                float f4 = 20.0f;
                float f5 = 20.0f;
                if (this.l != null) {
                    f4 = ((float) this.l.getWidth()) / 2.0f;
                    f5 = ((float) this.l.getHeight()) / 2.0f;
                    this.l.b();
                }
                float f6 = f5;
                float f7 = f4;
                float f8 = f6;
                if (this.m != null) {
                    f2 = ((float) this.m.getHeight()) / 2.0f;
                    f3 = ((float) this.m.getWidth()) / 2.0f;
                } else {
                    f2 = f8;
                    f3 = f7;
                }
                ScaleAnimation scaleAnimation = new ScaleAnimation(1.0f, 1.2f, 1.0f, 1.2f, f3, f2);
                scaleAnimation.setDuration(200);
                animationSet.addAnimation(scaleAnimation);
                ScaleAnimation scaleAnimation2 = new ScaleAnimation(1.2f, 0.001f, 1.2f, 0.001f, f3, f2);
                scaleAnimation2.setDuration(299);
                scaleAnimation2.setStartOffset(200);
                scaleAnimation2.setAnimationListener(this);
                animationSet.addAnimation(scaleAnimation2);
                if (this.l != null) {
                    this.l.startAnimation(animationSet);
                }
                if (this.m != null) {
                    this.m.startAnimation(animationSet);
                }
                if (this.i.f != 3 || !C0011l.g) {
                    postDelayed(new C0007h(this), 500);
                } else {
                    new AlertDialog.Builder(this.q).setTitle("安沃程序下载提示").setCancelable(false).setMessage("是否下载该程序？").setPositiveButton("下载", new C0004e(this)).setNegativeButton("取消", new C0006g(this)).show();
                }
            }
        }
    }

    public final void onAnimationRepeat(Animation animation) {
    }

    public final void onAnimationStart(Animation animation) {
    }

    public final void onAnimationEnd(Animation animation) {
    }
}
