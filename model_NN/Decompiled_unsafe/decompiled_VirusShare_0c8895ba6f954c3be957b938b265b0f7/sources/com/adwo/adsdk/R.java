package com.adwo.adsdk;

import java.io.UnsupportedEncodingException;

public final class R {
    protected static int a = 10000;
    protected static int b = 10000;
    protected static byte[] c;
    protected static byte[] d;
    protected static byte[] e = {104, 116, 116, 112, 58, 47, 47, 97, 100, 116, 101, 115, 116, 46, 97, 100, 119, 111, 46, 99, 111, 109, 47, 97, 100, 116, 101, 115, 116};
    protected static byte[] f = {104, 116, 116, 112, 58, 47, 47, 114, 50, 46, 97, 100, 119, 111, 46, 99, 111, 109};
    protected static final byte[] g = {104, 116, 116, 112, 58, 47, 47, 49, 48, 46, 48, 46, 48, 46, 49, 55, 50, 47, 116, 47, 116, 101, 115, 116};
    protected static final byte[] h = {104, 116, 116, 112, 58, 47, 47, 49, 48, 46, 48, 46, 48, 46, 49, 55, 50, 47, 97, 47, 112, 49};
    protected static final byte[] i = {49, 48, 46, 48, 46, 48, 46, 50, 48, 48};
    protected static final byte[] j = {49, 48, 46, 48, 46, 48, 46, 49, 55, 50};
    protected static final int[] k = {216, 320, 480, 640, 720};
    protected static final int[] l = {36, 50, 60, 80, 90};
    private static final String[][] m = {new String[]{".3gp", "video/3gpp"}, new String[]{".apk", "application/vnd.android.package-archive"}, new String[]{".asf", "video/x-ms-asf"}, new String[]{".avi", "video/x-msvideo"}, new String[]{".bmp", "image/bmp"}, new String[]{".gif", "image/gif"}, new String[]{".htm", "text/html"}, new String[]{".html", "text/html"}, new String[]{".jpeg", "image/jpeg"}, new String[]{".jpg", "image/jpeg"}, new String[]{".m4v", "video/x-m4v"}, new String[]{".mov", "video/quicktime"}, new String[]{".mp2", "audio/x-mpeg"}, new String[]{".mp3", "audio/x-mpeg"}, new String[]{".mp4", "video/mp4"}, new String[]{".mpe", "video/mpeg"}, new String[]{".mpeg", "video/mpeg"}, new String[]{".mpg", "video/mpeg"}, new String[]{".mpg4", "video/mp4"}, new String[]{".mpga", "audio/mpeg"}, new String[]{".ogg", "audio/ogg"}, new String[]{".png", "image/png"}, new String[]{".rmvb", "audio/x-pn-realaudio"}, new String[]{".wav", "audio/x-wav"}, new String[]{".wma", "audio/x-ms-wma"}, new String[]{".wmv", "audio/x-ms-wmv"}, new String[]{"", "*/*"}};

    static {
        c = null;
        try {
            c = "http://r2.adwo.com/ad".getBytes("UTF-8");
        } catch (UnsupportedEncodingException e2) {
        }
        d = null;
        try {
            d = "http://r2.adwo.com/adfs".getBytes("UTF-8");
        } catch (UnsupportedEncodingException e3) {
        }
        byte[] bArr = {104, 116, 116, 112, 58, 47, 47, 119, 119, 119, 46, 97, 100, 119, 111, 46, 99, 111, 109};
    }

    protected static String a(String str) {
        String lowerCase;
        int lastIndexOf = str.lastIndexOf(".");
        if (lastIndexOf < 0 || (lowerCase = str.substring(lastIndexOf, str.length()).toLowerCase()) == "") {
            return "*/*";
        }
        String str2 = "*/*";
        for (int i2 = 0; i2 < m.length; i2++) {
            if (lowerCase.equals(m[i2][0])) {
                str2 = m[i2][1];
            }
        }
        return str2;
    }
}
