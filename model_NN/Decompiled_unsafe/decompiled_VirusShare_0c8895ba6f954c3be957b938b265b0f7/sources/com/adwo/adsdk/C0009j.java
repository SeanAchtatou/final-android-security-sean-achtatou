package com.adwo.adsdk;

/* renamed from: com.adwo.adsdk.j  reason: case insensitive filesystem */
final class C0009j extends Thread {
    private /* synthetic */ C0008i a;

    C0009j(C0008i iVar) {
        this.a = iVar;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x006f A[SYNTHETIC, Splitter:B:15:0x006f] */
    /* JADX WARNING: Removed duplicated region for block: B:184:? A[ADDED_TO_REGION, ORIG_RETURN, RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0079  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0082  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x00a8  */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x0161 A[SYNTHETIC, Splitter:B:54:0x0161] */
    /* JADX WARNING: Removed duplicated region for block: B:83:0x01b5 A[SYNTHETIC, Splitter:B:83:0x01b5] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void run() {
        /*
            r13 = this;
            r11 = 268435456(0x10000000, float:2.5243549E-29)
            r10 = 1
            r9 = 0
            r8 = 0
            com.adwo.adsdk.i r0 = r13.a
            byte r0 = r0.f
            com.adwo.adsdk.i r1 = r13.a     // Catch:{ MalformedURLException -> 0x0409, IOException -> 0x0405 }
            com.adwo.adsdk.k r1 = r1.j     // Catch:{ MalformedURLException -> 0x0409, IOException -> 0x0405 }
            if (r1 == 0) goto L_0x0016
            com.adwo.adsdk.i r1 = r13.a     // Catch:{ MalformedURLException -> 0x0409, IOException -> 0x0405 }
            com.adwo.adsdk.k r1 = r1.j     // Catch:{ MalformedURLException -> 0x0409, IOException -> 0x0405 }
            r1.f()     // Catch:{ MalformedURLException -> 0x0409, IOException -> 0x0405 }
        L_0x0016:
            com.adwo.adsdk.i r1 = r13.a     // Catch:{ MalformedURLException -> 0x0409, IOException -> 0x0405 }
            java.lang.String r1 = r1.d     // Catch:{ MalformedURLException -> 0x0409, IOException -> 0x0405 }
            com.adwo.adsdk.i r2 = r13.a     // Catch:{ MalformedURLException -> 0x0409, IOException -> 0x0405 }
            java.lang.String r2 = r2.d     // Catch:{ MalformedURLException -> 0x0409, IOException -> 0x0405 }
            java.lang.String r3 = "clk?p0"
            int r2 = r2.indexOf(r3)     // Catch:{ MalformedURLException -> 0x0409, IOException -> 0x0405 }
            if (r2 <= 0) goto L_0x0051
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ MalformedURLException -> 0x0409, IOException -> 0x0405 }
            com.adwo.adsdk.i r2 = r13.a     // Catch:{ MalformedURLException -> 0x0409, IOException -> 0x0405 }
            java.lang.String r2 = r2.d     // Catch:{ MalformedURLException -> 0x0409, IOException -> 0x0405 }
            java.lang.String r2 = java.lang.String.valueOf(r2)     // Catch:{ MalformedURLException -> 0x0409, IOException -> 0x0405 }
            r1.<init>(r2)     // Catch:{ MalformedURLException -> 0x0409, IOException -> 0x0405 }
            java.lang.String r2 = "&p1="
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ MalformedURLException -> 0x0409, IOException -> 0x0405 }
            java.text.SimpleDateFormat r2 = new java.text.SimpleDateFormat     // Catch:{ MalformedURLException -> 0x0409, IOException -> 0x0405 }
            java.lang.String r3 = "yyyyMMddHHmmss"
            r2.<init>(r3)     // Catch:{ MalformedURLException -> 0x0409, IOException -> 0x0405 }
            java.util.Date r3 = new java.util.Date     // Catch:{ MalformedURLException -> 0x0409, IOException -> 0x0405 }
            r3.<init>()     // Catch:{ MalformedURLException -> 0x0409, IOException -> 0x0405 }
            java.lang.String r2 = r2.format(r3)     // Catch:{ MalformedURLException -> 0x0409, IOException -> 0x0405 }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ MalformedURLException -> 0x0409, IOException -> 0x0405 }
            java.lang.String r1 = r1.toString()     // Catch:{ MalformedURLException -> 0x0409, IOException -> 0x0405 }
        L_0x0051:
            java.net.URL r2 = new java.net.URL     // Catch:{ MalformedURLException -> 0x0409, IOException -> 0x0405 }
            r2.<init>(r1)     // Catch:{ MalformedURLException -> 0x0409, IOException -> 0x0405 }
            int r1 = com.adwo.adsdk.C0011l.f     // Catch:{ MalformedURLException -> 0x0409, IOException -> 0x0405 }
            int r3 = com.adwo.adsdk.C0011l.b     // Catch:{ MalformedURLException -> 0x0409, IOException -> 0x0405 }
            if (r1 != r3) goto L_0x00be
            java.net.URLConnection r1 = r2.openConnection()     // Catch:{ MalformedURLException -> 0x0409, IOException -> 0x0405 }
            int r3 = com.adwo.adsdk.R.b     // Catch:{ MalformedURLException -> 0x00f1, IOException -> 0x013a }
            r1.setConnectTimeout(r3)     // Catch:{ MalformedURLException -> 0x00f1, IOException -> 0x013a }
            int r3 = com.adwo.adsdk.R.b     // Catch:{ MalformedURLException -> 0x00f1, IOException -> 0x013a }
            r1.setReadTimeout(r3)     // Catch:{ MalformedURLException -> 0x00f1, IOException -> 0x013a }
        L_0x006a:
            r1.connect()     // Catch:{ MalformedURLException -> 0x00f1, IOException -> 0x013a }
        L_0x006d:
            if (r0 != r10) goto L_0x0161
            java.lang.String r1 = r2.toString()     // Catch:{ Exception -> 0x0156 }
        L_0x0073:
            com.adwo.adsdk.i r2 = r13.a
            com.adwo.adsdk.k r2 = r2.j
            if (r2 == 0) goto L_0x0080
            com.adwo.adsdk.i r2 = r13.a
            com.adwo.adsdk.k r2 = r2.j
            r2.e()
        L_0x0080:
            if (r1 == 0) goto L_0x00a2
            android.content.Intent r2 = new android.content.Intent
            r2.<init>()
            switch(r0) {
                case 1: goto L_0x01b9;
                case 2: goto L_0x01b9;
                case 3: goto L_0x0221;
                case 4: goto L_0x0221;
                case 5: goto L_0x0221;
                case 6: goto L_0x0221;
                case 7: goto L_0x0221;
                case 8: goto L_0x023b;
                case 9: goto L_0x0260;
                case 10: goto L_0x02b8;
                case 11: goto L_0x008a;
                case 12: goto L_0x02d0;
                case 13: goto L_0x0312;
                case 14: goto L_0x0384;
                case 15: goto L_0x03a4;
                default: goto L_0x008a;
            }
        L_0x008a:
            r0 = r2
        L_0x008b:
            r0.addFlags(r11)
            com.adwo.adsdk.i r2 = r13.a     // Catch:{ ActivityNotFoundException -> 0x03ce }
            android.content.Context r2 = r2.a     // Catch:{ ActivityNotFoundException -> 0x03ce }
            r2.startActivity(r0)     // Catch:{ ActivityNotFoundException -> 0x03ce }
            java.util.Set r0 = com.adwo.adsdk.C0011l.i     // Catch:{ ActivityNotFoundException -> 0x03ce }
            com.adwo.adsdk.i r2 = r13.a     // Catch:{ ActivityNotFoundException -> 0x03ce }
            int r2 = r2.b     // Catch:{ ActivityNotFoundException -> 0x03ce }
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)     // Catch:{ ActivityNotFoundException -> 0x03ce }
            r0.add(r2)     // Catch:{ ActivityNotFoundException -> 0x03ce }
        L_0x00a2:
            com.adwo.adsdk.i r0 = r13.a
            java.util.List r0 = r0.g
            if (r0 == 0) goto L_0x00bd
            com.adwo.adsdk.i r0 = r13.a
            java.util.List r0 = r0.g
            int r0 = r0.size()
            if (r0 == 0) goto L_0x00bd
            r1 = r9
        L_0x00b3:
            com.adwo.adsdk.i r0 = r13.a
            java.util.List r0 = r0.g
            int r0 = r0.size()
            if (r1 < r0) goto L_0x03e5
        L_0x00bd:
            return
        L_0x00be:
            int r1 = com.adwo.adsdk.C0011l.f     // Catch:{ MalformedURLException -> 0x0409, IOException -> 0x0405 }
            int r3 = com.adwo.adsdk.C0011l.c     // Catch:{ MalformedURLException -> 0x0409, IOException -> 0x0405 }
            if (r1 == r3) goto L_0x00ca
            int r1 = com.adwo.adsdk.C0011l.f     // Catch:{ MalformedURLException -> 0x0409, IOException -> 0x0405 }
            int r3 = com.adwo.adsdk.C0011l.d     // Catch:{ MalformedURLException -> 0x0409, IOException -> 0x0405 }
            if (r1 != r3) goto L_0x010d
        L_0x00ca:
            java.net.Proxy r1 = new java.net.Proxy     // Catch:{ MalformedURLException -> 0x0409, IOException -> 0x0405 }
            java.net.Proxy$Type r3 = java.net.Proxy.Type.HTTP     // Catch:{ MalformedURLException -> 0x0409, IOException -> 0x0405 }
            java.net.InetSocketAddress r4 = new java.net.InetSocketAddress     // Catch:{ MalformedURLException -> 0x0409, IOException -> 0x0405 }
            java.lang.String r5 = new java.lang.String     // Catch:{ MalformedURLException -> 0x0409, IOException -> 0x0405 }
            byte[] r6 = com.adwo.adsdk.R.j     // Catch:{ MalformedURLException -> 0x0409, IOException -> 0x0405 }
            java.lang.String r7 = "utf-8"
            r5.<init>(r6, r7)     // Catch:{ MalformedURLException -> 0x0409, IOException -> 0x0405 }
            r6 = 80
            r4.<init>(r5, r6)     // Catch:{ MalformedURLException -> 0x0409, IOException -> 0x0405 }
            r1.<init>(r3, r4)     // Catch:{ MalformedURLException -> 0x0409, IOException -> 0x0405 }
            java.net.URLConnection r1 = r2.openConnection(r1)     // Catch:{ MalformedURLException -> 0x0409, IOException -> 0x0405 }
            int r3 = com.adwo.adsdk.R.b     // Catch:{ MalformedURLException -> 0x00f1, IOException -> 0x013a }
            r1.setConnectTimeout(r3)     // Catch:{ MalformedURLException -> 0x00f1, IOException -> 0x013a }
            int r3 = com.adwo.adsdk.R.b     // Catch:{ MalformedURLException -> 0x00f1, IOException -> 0x013a }
            r1.setReadTimeout(r3)     // Catch:{ MalformedURLException -> 0x00f1, IOException -> 0x013a }
            goto L_0x006a
        L_0x00f1:
            r2 = move-exception
        L_0x00f2:
            java.lang.String r2 = "Adwo SDK"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r4 = "Malformed click URL.Will try to follow anyway."
            r3.<init>(r4)
            com.adwo.adsdk.i r4 = r13.a
            java.lang.String r4 = r4.d
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r3 = r3.toString()
            android.util.Log.w(r2, r3)
            r2 = r8
            goto L_0x006d
        L_0x010d:
            int r1 = com.adwo.adsdk.C0011l.f     // Catch:{ MalformedURLException -> 0x0409, IOException -> 0x0405 }
            int r3 = com.adwo.adsdk.C0011l.e     // Catch:{ MalformedURLException -> 0x0409, IOException -> 0x0405 }
            if (r1 != r3) goto L_0x041f
            java.net.Proxy r1 = new java.net.Proxy     // Catch:{ MalformedURLException -> 0x0409, IOException -> 0x0405 }
            java.net.Proxy$Type r3 = java.net.Proxy.Type.HTTP     // Catch:{ MalformedURLException -> 0x0409, IOException -> 0x0405 }
            java.net.InetSocketAddress r4 = new java.net.InetSocketAddress     // Catch:{ MalformedURLException -> 0x0409, IOException -> 0x0405 }
            java.lang.String r5 = new java.lang.String     // Catch:{ MalformedURLException -> 0x0409, IOException -> 0x0405 }
            byte[] r6 = com.adwo.adsdk.R.i     // Catch:{ MalformedURLException -> 0x0409, IOException -> 0x0405 }
            java.lang.String r7 = "utf-8"
            r5.<init>(r6, r7)     // Catch:{ MalformedURLException -> 0x0409, IOException -> 0x0405 }
            r6 = 80
            r4.<init>(r5, r6)     // Catch:{ MalformedURLException -> 0x0409, IOException -> 0x0405 }
            r1.<init>(r3, r4)     // Catch:{ MalformedURLException -> 0x0409, IOException -> 0x0405 }
            java.net.URLConnection r1 = r2.openConnection(r1)     // Catch:{ MalformedURLException -> 0x0409, IOException -> 0x0405 }
            int r3 = com.adwo.adsdk.R.b     // Catch:{ MalformedURLException -> 0x00f1, IOException -> 0x013a }
            r1.setConnectTimeout(r3)     // Catch:{ MalformedURLException -> 0x00f1, IOException -> 0x013a }
            int r3 = com.adwo.adsdk.R.b     // Catch:{ MalformedURLException -> 0x00f1, IOException -> 0x013a }
            r1.setReadTimeout(r3)     // Catch:{ MalformedURLException -> 0x00f1, IOException -> 0x013a }
            goto L_0x006a
        L_0x013a:
            r2 = move-exception
        L_0x013b:
            java.lang.String r2 = "Adwo SDK"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r4 = "Could not determine final click destination URL.  Will try to follow anyway.  "
            r3.<init>(r4)
            com.adwo.adsdk.i r4 = r13.a
            java.lang.String r4 = r4.d
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r3 = r3.toString()
            android.util.Log.w(r2, r3)
            r2 = r8
            goto L_0x006d
        L_0x0156:
            r1 = move-exception
            java.lang.String r1 = "Adwo SDK"
            java.lang.String r2 = "Could not get ad click url from  server."
            android.util.Log.e(r1, r2)
            r1 = r8
            goto L_0x0073
        L_0x0161:
            java.io.InputStream r2 = r1.getInputStream()     // Catch:{ Exception -> 0x0401, all -> 0x01b1 }
            if (r2 == 0) goto L_0x041c
            java.io.ByteArrayOutputStream r3 = new java.io.ByteArrayOutputStream     // Catch:{ Exception -> 0x018d }
            r3.<init>()     // Catch:{ Exception -> 0x018d }
        L_0x016c:
            int r4 = r2.read()     // Catch:{ Exception -> 0x018d }
            r5 = -1
            if (r4 != r5) goto L_0x0189
            java.lang.String r4 = new java.lang.String     // Catch:{ Exception -> 0x018d }
            byte[] r3 = r3.toByteArray()     // Catch:{ Exception -> 0x018d }
            java.lang.String r5 = "UTF-8"
            r4.<init>(r3, r5)     // Catch:{ Exception -> 0x018d }
            r1 = r4
        L_0x017f:
            if (r2 == 0) goto L_0x0073
            r2.close()     // Catch:{ Exception -> 0x0186 }
            goto L_0x0073
        L_0x0186:
            r2 = move-exception
            goto L_0x0073
        L_0x0189:
            r3.write(r4)     // Catch:{ Exception -> 0x018d }
            goto L_0x016c
        L_0x018d:
            r3 = move-exception
        L_0x018e:
            java.lang.String r3 = "Adwo SDK"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x03fd }
            java.lang.String r5 = "Connection off "
            r4.<init>(r5)     // Catch:{ all -> 0x03fd }
            r5 = 0
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x03fd }
            java.lang.String r4 = r4.toString()     // Catch:{ all -> 0x03fd }
            android.util.Log.e(r3, r4)     // Catch:{ all -> 0x03fd }
            if (r2 == 0) goto L_0x01a8
            r2.close()     // Catch:{ Exception -> 0x01ad }
        L_0x01a8:
            if (r1 == 0) goto L_0x0419
            r1 = r8
            goto L_0x0073
        L_0x01ad:
            r1 = move-exception
            r1 = r8
            goto L_0x0073
        L_0x01b1:
            r0 = move-exception
            r1 = r8
        L_0x01b3:
            if (r1 == 0) goto L_0x01b8
            r1.close()     // Catch:{ Exception -> 0x03f7 }
        L_0x01b8:
            throw r0
        L_0x01b9:
            com.adwo.adsdk.i r0 = r13.a     // Catch:{ ActivityNotFoundException -> 0x01ef }
            android.content.Context r0 = r0.a     // Catch:{ ActivityNotFoundException -> 0x01ef }
            java.lang.Class<com.adwo.adsdk.AdwoAdBrowserActivity> r3 = com.adwo.adsdk.AdwoAdBrowserActivity.class
            r2.setClass(r0, r3)     // Catch:{ ActivityNotFoundException -> 0x01ef }
            java.lang.String r0 = "android.intent.action.VIEW"
            r2.setAction(r0)     // Catch:{ ActivityNotFoundException -> 0x01ef }
            r0 = 268435456(0x10000000, float:2.5243549E-29)
            r2.addFlags(r0)     // Catch:{ ActivityNotFoundException -> 0x01ef }
            java.lang.String r0 = "url"
            r2.putExtra(r0, r1)     // Catch:{ ActivityNotFoundException -> 0x01ef }
        L_0x01d1:
            com.adwo.adsdk.i r0 = r13.a
            java.util.List r0 = r0.g
            if (r0 == 0) goto L_0x0416
            com.adwo.adsdk.i r0 = r13.a
            java.util.List r0 = r0.g
            int r0 = r0.size()
            if (r0 == 0) goto L_0x0416
            r3 = r9
        L_0x01e2:
            com.adwo.adsdk.i r0 = r13.a
            java.util.List r0 = r0.g
            int r0 = r0.size()
            if (r3 < r0) goto L_0x0210
            r0 = r2
            goto L_0x008b
        L_0x01ef:
            r0 = move-exception
            android.content.Intent r0 = new android.content.Intent
            r0.<init>()
            java.lang.String r2 = "android.intent.action.VIEW"
            r0.setAction(r2)     // Catch:{ Exception -> 0x020d }
            android.net.Uri r2 = android.net.Uri.parse(r1)     // Catch:{ Exception -> 0x020d }
            r0.setData(r2)     // Catch:{ Exception -> 0x020d }
            r2 = 268435456(0x10000000, float:2.5243549E-29)
            r0.addFlags(r2)     // Catch:{ Exception -> 0x020d }
            java.lang.String r2 = "CONFIGURATION ERROR:  com.adwo.adsdk.AdwoAdBrowserActivity must be registered in your AndroidManifest.xml file. "
            com.adwo.adsdk.C0011l.a(r2)     // Catch:{ Exception -> 0x020d }
            r2 = r0
            goto L_0x01d1
        L_0x020d:
            r2 = move-exception
            r2 = r0
            goto L_0x01d1
        L_0x0210:
            com.adwo.adsdk.i r0 = r13.a
            java.util.List r0 = r0.g
            java.lang.Object r0 = r0.get(r3)
            java.lang.String r0 = (java.lang.String) r0
            com.adwo.adsdk.C0011l.c(r0)
            int r0 = r3 + 1
            r3 = r0
            goto L_0x01e2
        L_0x0221:
            java.lang.String r0 = r1.trim()     // Catch:{ NullPointerException -> 0x0234 }
            java.lang.String r3 = "android.intent.action.VIEW"
            r2.setAction(r3)     // Catch:{ NullPointerException -> 0x0234 }
            android.net.Uri r0 = android.net.Uri.parse(r0)     // Catch:{ NullPointerException -> 0x0234 }
            r2.setData(r0)     // Catch:{ NullPointerException -> 0x0234 }
            r0 = r2
            goto L_0x008b
        L_0x0234:
            r0 = move-exception
            r0.printStackTrace()
            r0 = r2
            goto L_0x008b
        L_0x023b:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0259 }
            java.lang.String r3 = "tel:"
            r0.<init>(r3)     // Catch:{ Exception -> 0x0259 }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Exception -> 0x0259 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x0259 }
            java.lang.String r3 = "android.intent.action.DIAL"
            r2.setAction(r3)     // Catch:{ Exception -> 0x0259 }
            android.net.Uri r0 = android.net.Uri.parse(r0)     // Catch:{ Exception -> 0x0259 }
            r2.setData(r0)     // Catch:{ Exception -> 0x0259 }
            r0 = r2
            goto L_0x008b
        L_0x0259:
            r0 = move-exception
            r0.printStackTrace()
            r0 = r2
            goto L_0x008b
        L_0x0260:
            java.lang.String r0 = "|"
            boolean r0 = r1.contains(r0)
            if (r0 == 0) goto L_0x02ad
            java.lang.String r0 = "|"
            int r0 = r1.indexOf(r0)
            java.lang.String r3 = r1.substring(r9, r0)
            int r0 = r0 + 1
            java.lang.String r0 = r1.substring(r0)
        L_0x0278:
            java.lang.String r4 = "android.intent.action.VIEW"
            r2.setAction(r4)     // Catch:{ Exception -> 0x02b1 }
            java.lang.String r4 = "com.google.android.apps.maps"
            java.lang.String r5 = "com.google.android.maps.MapsActivity"
            r2.setClassName(r4, r5)     // Catch:{ Exception -> 0x02b1 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x02b1 }
            java.lang.String r5 = "http://maps.google.com/maps?q="
            r4.<init>(r5)     // Catch:{ Exception -> 0x02b1 }
            java.lang.StringBuilder r3 = r4.append(r3)     // Catch:{ Exception -> 0x02b1 }
            java.lang.String r4 = "("
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x02b1 }
            java.lang.StringBuilder r0 = r3.append(r0)     // Catch:{ Exception -> 0x02b1 }
            java.lang.String r3 = ")&z=22"
            java.lang.StringBuilder r0 = r0.append(r3)     // Catch:{ Exception -> 0x02b1 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x02b1 }
            android.net.Uri r0 = android.net.Uri.parse(r0)     // Catch:{ Exception -> 0x02b1 }
            r2.setData(r0)     // Catch:{ Exception -> 0x02b1 }
            r0 = r2
            goto L_0x008b
        L_0x02ad:
            java.lang.String r0 = ""
            r3 = r1
            goto L_0x0278
        L_0x02b1:
            r0 = move-exception
            r0.printStackTrace()
            r0 = r2
            goto L_0x008b
        L_0x02b8:
            android.content.Intent r0 = new android.content.Intent     // Catch:{ Exception -> 0x03fa }
            java.lang.String r3 = "android.intent.action.WEB_SEARCH"
            r0.<init>(r3)     // Catch:{ Exception -> 0x03fa }
            java.lang.String r2 = "query"
            r0.putExtra(r2, r1)     // Catch:{ Exception -> 0x02c6 }
            goto L_0x008b
        L_0x02c6:
            r2 = move-exception
            r12 = r2
            r2 = r0
            r0 = r12
        L_0x02ca:
            r0.printStackTrace()
            r0 = r2
            goto L_0x008b
        L_0x02d0:
            java.lang.String r0 = "|"
            boolean r0 = r1.contains(r0)
            if (r0 == 0) goto L_0x0412
            java.lang.String r0 = "|"
            int r0 = r1.indexOf(r0)
            java.lang.String r3 = r1.substring(r9, r0)
            int r0 = r0 + 1
            java.lang.String r0 = r1.substring(r0)
        L_0x02e8:
            java.lang.String r4 = "android.intent.action.SENDTO"
            r2.setAction(r4)     // Catch:{ Exception -> 0x030b }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x030b }
            java.lang.String r5 = "smsto:"
            r4.<init>(r5)     // Catch:{ Exception -> 0x030b }
            java.lang.StringBuilder r3 = r4.append(r3)     // Catch:{ Exception -> 0x030b }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x030b }
            android.net.Uri r3 = android.net.Uri.parse(r3)     // Catch:{ Exception -> 0x030b }
            r2.setData(r3)     // Catch:{ Exception -> 0x030b }
            java.lang.String r3 = "sms_body"
            r2.putExtra(r3, r0)     // Catch:{ Exception -> 0x030b }
            r0 = r2
            goto L_0x008b
        L_0x030b:
            r0 = move-exception
            r0.printStackTrace()
            r0 = r2
            goto L_0x008b
        L_0x0312:
            java.lang.String r0 = "android.intent.action.SEND"
            r2.setAction(r0)     // Catch:{ Exception -> 0x037d }
            java.lang.String r0 = "|"
            boolean r0 = r1.contains(r0)     // Catch:{ Exception -> 0x037d }
            if (r0 == 0) goto L_0x040d
            r0 = 3
            java.lang.String[] r0 = new java.lang.String[r0]     // Catch:{ Exception -> 0x037d }
            java.lang.String r3 = "|"
            int r3 = r1.indexOf(r3)     // Catch:{ Exception -> 0x037d }
            r4 = 0
            r5 = 0
            java.lang.String r5 = r1.substring(r5, r3)     // Catch:{ Exception -> 0x037d }
            r0[r4] = r5     // Catch:{ Exception -> 0x037d }
            int r3 = r3 + 1
            java.lang.String r3 = r1.substring(r3)     // Catch:{ Exception -> 0x037d }
            java.lang.String r4 = "|"
            int r4 = r3.indexOf(r4)     // Catch:{ Exception -> 0x037d }
            r5 = 1
            r6 = 0
            java.lang.String r6 = r3.substring(r6, r4)     // Catch:{ Exception -> 0x037d }
            r0[r5] = r6     // Catch:{ Exception -> 0x037d }
            r5 = 2
            int r4 = r4 + 1
            java.lang.String r3 = r3.substring(r4)     // Catch:{ Exception -> 0x037d }
            r0[r5] = r3     // Catch:{ Exception -> 0x037d }
            r3 = 0
            r3 = r0[r3]     // Catch:{ Exception -> 0x037d }
            r4 = 1
            r4 = r0[r4]     // Catch:{ Exception -> 0x037d }
            r5 = 2
            r0 = r0[r5]     // Catch:{ Exception -> 0x037d }
            r12 = r4
            r4 = r0
            r0 = r12
        L_0x0359:
            android.net.Uri r5 = android.net.Uri.parse(r3)     // Catch:{ Exception -> 0x037d }
            r2.setData(r5)     // Catch:{ Exception -> 0x037d }
            r5 = 1
            java.lang.String[] r5 = new java.lang.String[r5]     // Catch:{ Exception -> 0x037d }
            r6 = 0
            r5[r6] = r3     // Catch:{ Exception -> 0x037d }
            java.lang.String r3 = "android.intent.extra.EMAIL"
            r2.putExtra(r3, r5)     // Catch:{ Exception -> 0x037d }
            java.lang.String r3 = "android.intent.extra.TEXT"
            r2.putExtra(r3, r4)     // Catch:{ Exception -> 0x037d }
            java.lang.String r3 = "android.intent.extra.SUBJECT"
            r2.putExtra(r3, r0)     // Catch:{ Exception -> 0x037d }
            java.lang.String r0 = "message/rfc882"
            r2.setType(r0)     // Catch:{ Exception -> 0x037d }
            r0 = r2
            goto L_0x008b
        L_0x037d:
            r0 = move-exception
            r0.printStackTrace()
            r0 = r2
            goto L_0x008b
        L_0x0384:
            java.lang.String r0 = ".mp3"
            boolean r0 = r1.endsWith(r0)
            if (r0 != 0) goto L_0x0394
            java.lang.String r0 = ".wav"
            boolean r0 = r1.endsWith(r0)
            if (r0 == 0) goto L_0x03a4
        L_0x0394:
            java.lang.String r0 = "android.intent.action.VIEW"
            r2.setAction(r0)
            java.lang.String r0 = com.adwo.adsdk.R.a(r1)
            android.net.Uri r3 = android.net.Uri.parse(r1)
            r2.setDataAndType(r3, r0)
        L_0x03a4:
            java.lang.String r0 = ".3gp"
            boolean r0 = r1.endsWith(r0)
            if (r0 != 0) goto L_0x03bc
            java.lang.String r0 = ".mp4"
            boolean r0 = r1.endsWith(r0)
            if (r0 != 0) goto L_0x03bc
            java.lang.String r0 = ".mpeg"
            boolean r0 = r1.endsWith(r0)
            if (r0 == 0) goto L_0x008a
        L_0x03bc:
            java.lang.String r0 = "android.intent.action.VIEW"
            r2.setAction(r0)
            java.lang.String r0 = com.adwo.adsdk.R.a(r1)
            android.net.Uri r3 = android.net.Uri.parse(r1)
            r2.setDataAndType(r3, r0)
            goto L_0x008a
        L_0x03ce:
            r0 = move-exception
            java.lang.String r2 = "Adwo SDK"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r4 = "Could not intent to "
            r3.<init>(r4)
            java.lang.StringBuilder r1 = r3.append(r1)
            java.lang.String r1 = r1.toString()
            android.util.Log.e(r2, r1, r0)
            goto L_0x00a2
        L_0x03e5:
            com.adwo.adsdk.i r0 = r13.a
            java.util.List r0 = r0.g
            java.lang.Object r0 = r0.get(r1)
            java.lang.String r0 = (java.lang.String) r0
            com.adwo.adsdk.C0011l.c(r0)
            int r0 = r1 + 1
            r1 = r0
            goto L_0x00b3
        L_0x03f7:
            r1 = move-exception
            goto L_0x01b8
        L_0x03fa:
            r0 = move-exception
            goto L_0x02ca
        L_0x03fd:
            r0 = move-exception
            r1 = r2
            goto L_0x01b3
        L_0x0401:
            r2 = move-exception
            r2 = r8
            goto L_0x018e
        L_0x0405:
            r1 = move-exception
            r1 = r8
            goto L_0x013b
        L_0x0409:
            r1 = move-exception
            r1 = r8
            goto L_0x00f2
        L_0x040d:
            r0 = r8
            r3 = r8
            r4 = r8
            goto L_0x0359
        L_0x0412:
            r0 = r8
            r3 = r8
            goto L_0x02e8
        L_0x0416:
            r0 = r2
            goto L_0x008b
        L_0x0419:
            r1 = r8
            goto L_0x0073
        L_0x041c:
            r1 = r8
            goto L_0x017f
        L_0x041f:
            r1 = r8
            goto L_0x006a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adwo.adsdk.C0009j.run():void");
    }
}
