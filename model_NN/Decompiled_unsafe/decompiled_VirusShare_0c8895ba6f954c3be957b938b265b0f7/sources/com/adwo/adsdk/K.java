package com.adwo.adsdk;

import android.view.animation.Animation;

final class K implements Animation.AnimationListener {
    private /* synthetic */ AdwoAdView a;
    private final /* synthetic */ C0001b b;
    private final /* synthetic */ P c;

    K(AdwoAdView adwoAdView, C0001b bVar, P p) {
        this.a = adwoAdView;
        this.b = bVar;
        this.c = p;
    }

    public final void onAnimationStart(Animation animation) {
    }

    public final void onAnimationEnd(Animation animation) {
        this.a.post(new M(this.a, this.b, this.c));
    }

    public final void onAnimationRepeat(Animation animation) {
    }
}
