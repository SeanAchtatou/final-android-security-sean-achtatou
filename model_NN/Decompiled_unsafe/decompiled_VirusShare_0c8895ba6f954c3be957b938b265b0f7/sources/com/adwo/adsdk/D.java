package com.adwo.adsdk;

import android.webkit.WebView;
import android.webkit.WebViewClient;

final class D extends WebViewClient {
    /* access modifiers changed from: private */
    public /* synthetic */ C0012m a;

    D(C0012m mVar) {
        this.a = mVar;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:125:?, code lost:
        return false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:82:0x01d2, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:83:0x01d3, code lost:
        r0.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:84:0x01d9, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:85:0x01da, code lost:
        r8 = r2;
        r2 = null;
        r1 = r8;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:72:0x01a2  */
    /* JADX WARNING: Removed duplicated region for block: B:82:0x01d2 A[ExcHandler: MalformedURLException (r0v3 'e' java.net.MalformedURLException A[CUSTOM_DECLARE]), Splitter:B:5:0x000f] */
    /* JADX WARNING: Removed duplicated region for block: B:87:0x01e2  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean shouldOverrideUrlLoading(android.webkit.WebView r10, java.lang.String r11) {
        /*
            r9 = this;
            r7 = 0
            r6 = 1
            if (r11 == 0) goto L_0x01cf
            r1 = 0
            android.content.Context r0 = r10.getContext()
            android.app.Activity r0 = (android.app.Activity) r0
            if (r0 != 0) goto L_0x000f
            r0 = r7
        L_0x000e:
            return r0
        L_0x000f:
            android.net.Uri r2 = android.net.Uri.parse(r11)     // Catch:{ MalformedURLException -> 0x01d2, IOException -> 0x01d9 }
            java.lang.String r3 = r2.getScheme()     // Catch:{ MalformedURLException -> 0x01d2, IOException -> 0x01d9 }
            java.lang.String r4 = "tel"
            boolean r3 = r3.equalsIgnoreCase(r4)     // Catch:{ MalformedURLException -> 0x01d2, IOException -> 0x01d9 }
            if (r3 == 0) goto L_0x002d
            android.content.Intent r3 = new android.content.Intent     // Catch:{ MalformedURLException -> 0x01d2, IOException -> 0x01d9 }
            java.lang.String r4 = "android.intent.action.DIAL"
            r3.<init>(r4, r2)     // Catch:{ MalformedURLException -> 0x01d2, IOException -> 0x01d9 }
            if (r0 == 0) goto L_0x002b
            r0.startActivity(r3)     // Catch:{ ActivityNotFoundException -> 0x0275 }
        L_0x002b:
            r0 = r6
            goto L_0x000e
        L_0x002d:
            java.lang.String r3 = r2.getScheme()     // Catch:{ MalformedURLException -> 0x01d2, IOException -> 0x01d9 }
            java.lang.String r4 = "sms"
            boolean r3 = r3.equalsIgnoreCase(r4)     // Catch:{ MalformedURLException -> 0x01d2, IOException -> 0x01d9 }
            if (r3 == 0) goto L_0x0047
            android.content.Intent r3 = new android.content.Intent     // Catch:{ MalformedURLException -> 0x01d2, IOException -> 0x01d9 }
            java.lang.String r4 = "android.intent.action.VIEW"
            r3.<init>(r4, r2)     // Catch:{ MalformedURLException -> 0x01d2, IOException -> 0x01d9 }
            if (r0 == 0) goto L_0x0045
            r0.startActivity(r3)     // Catch:{ ActivityNotFoundException -> 0x0278 }
        L_0x0045:
            r0 = r6
            goto L_0x000e
        L_0x0047:
            java.lang.String r3 = r2.getScheme()     // Catch:{ MalformedURLException -> 0x01d2, IOException -> 0x01d9 }
            java.lang.String r4 = "market"
            boolean r3 = r3.equalsIgnoreCase(r4)     // Catch:{ MalformedURLException -> 0x01d2, IOException -> 0x01d9 }
            if (r3 != 0) goto L_0x006b
            java.lang.String r3 = r11.toLowerCase()     // Catch:{ MalformedURLException -> 0x01d2, IOException -> 0x01d9 }
            java.lang.String r4 = "http://market.android.com"
            boolean r3 = r3.startsWith(r4)     // Catch:{ MalformedURLException -> 0x01d2, IOException -> 0x01d9 }
            if (r3 != 0) goto L_0x006b
            java.lang.String r3 = r11.toLowerCase()     // Catch:{ MalformedURLException -> 0x01d2, IOException -> 0x01d9 }
            java.lang.String r4 = "https://market.android.com"
            boolean r3 = r3.startsWith(r4)     // Catch:{ MalformedURLException -> 0x01d2, IOException -> 0x01d9 }
            if (r3 == 0) goto L_0x0079
        L_0x006b:
            android.content.Intent r3 = new android.content.Intent     // Catch:{ MalformedURLException -> 0x01d2, IOException -> 0x01d9 }
            java.lang.String r4 = "android.intent.action.VIEW"
            r3.<init>(r4, r2)     // Catch:{ MalformedURLException -> 0x01d2, IOException -> 0x01d9 }
            if (r0 == 0) goto L_0x0077
            r0.startActivity(r3)     // Catch:{ ActivityNotFoundException -> 0x027b }
        L_0x0077:
            r0 = r6
            goto L_0x000e
        L_0x0079:
            java.lang.String r3 = r2.getScheme()     // Catch:{ MalformedURLException -> 0x01d2, IOException -> 0x01d9 }
            java.lang.String r4 = "http"
            boolean r3 = r3.equalsIgnoreCase(r4)     // Catch:{ MalformedURLException -> 0x01d2, IOException -> 0x01d9 }
            if (r3 == 0) goto L_0x00a1
            java.lang.String r3 = r11.toLowerCase()     // Catch:{ MalformedURLException -> 0x01d2, IOException -> 0x01d9 }
            java.lang.String r4 = ".apk"
            boolean r3 = r3.endsWith(r4)     // Catch:{ MalformedURLException -> 0x01d2, IOException -> 0x01d9 }
            if (r3 == 0) goto L_0x00a1
            java.lang.Thread r2 = new java.lang.Thread     // Catch:{ MalformedURLException -> 0x01d2, IOException -> 0x01d9 }
            com.adwo.adsdk.E r3 = new com.adwo.adsdk.E     // Catch:{ MalformedURLException -> 0x01d2, IOException -> 0x01d9 }
            r3.<init>(r9, r11, r0)     // Catch:{ MalformedURLException -> 0x01d2, IOException -> 0x01d9 }
            r2.<init>(r3)     // Catch:{ MalformedURLException -> 0x01d2, IOException -> 0x01d9 }
            r2.start()     // Catch:{ MalformedURLException -> 0x01d2, IOException -> 0x01d9 }
            r0 = r6
            goto L_0x000e
        L_0x00a1:
            java.lang.String r2 = r2.getScheme()     // Catch:{ MalformedURLException -> 0x01d2, IOException -> 0x01d9 }
            java.lang.String r3 = "http"
            boolean r2 = r2.equalsIgnoreCase(r3)     // Catch:{ MalformedURLException -> 0x01d2, IOException -> 0x01d9 }
            if (r2 == 0) goto L_0x00b9
            java.lang.String r2 = r11.toLowerCase()     // Catch:{ MalformedURLException -> 0x01d2, IOException -> 0x01d9 }
            java.lang.String r3 = ".jpeg"
            boolean r2 = r2.endsWith(r3)     // Catch:{ MalformedURLException -> 0x01d2, IOException -> 0x01d9 }
            if (r2 != 0) goto L_0x00e9
        L_0x00b9:
            java.lang.String r2 = r11.toLowerCase()     // Catch:{ MalformedURLException -> 0x01d2, IOException -> 0x01d9 }
            java.lang.String r3 = ".gif"
            boolean r2 = r2.endsWith(r3)     // Catch:{ MalformedURLException -> 0x01d2, IOException -> 0x01d9 }
            if (r2 != 0) goto L_0x00e9
            java.lang.String r2 = r11.toLowerCase()     // Catch:{ MalformedURLException -> 0x01d2, IOException -> 0x01d9 }
            java.lang.String r3 = ".png"
            boolean r2 = r2.endsWith(r3)     // Catch:{ MalformedURLException -> 0x01d2, IOException -> 0x01d9 }
            if (r2 != 0) goto L_0x00e9
            java.lang.String r2 = r11.toLowerCase()     // Catch:{ MalformedURLException -> 0x01d2, IOException -> 0x01d9 }
            java.lang.String r3 = ".jpg"
            boolean r2 = r2.endsWith(r3)     // Catch:{ MalformedURLException -> 0x01d2, IOException -> 0x01d9 }
            if (r2 != 0) goto L_0x00e9
            java.lang.String r2 = r11.toLowerCase()     // Catch:{ MalformedURLException -> 0x01d2, IOException -> 0x01d9 }
            java.lang.String r3 = ".bmp"
            boolean r2 = r2.endsWith(r3)     // Catch:{ MalformedURLException -> 0x01d2, IOException -> 0x01d9 }
            if (r2 == 0) goto L_0x00f9
        L_0x00e9:
            java.lang.Thread r2 = new java.lang.Thread     // Catch:{ MalformedURLException -> 0x01d2, IOException -> 0x01d9 }
            com.adwo.adsdk.F r3 = new com.adwo.adsdk.F     // Catch:{ MalformedURLException -> 0x01d2, IOException -> 0x01d9 }
            r3.<init>(r9, r11, r0)     // Catch:{ MalformedURLException -> 0x01d2, IOException -> 0x01d9 }
            r2.<init>(r3)     // Catch:{ MalformedURLException -> 0x01d2, IOException -> 0x01d9 }
            r2.start()     // Catch:{ MalformedURLException -> 0x01d2, IOException -> 0x01d9 }
            r0 = r6
            goto L_0x000e
        L_0x00f9:
            java.lang.String r2 = ".3gp"
            boolean r2 = r11.endsWith(r2)     // Catch:{ MalformedURLException -> 0x01d2, IOException -> 0x01d9 }
            if (r2 != 0) goto L_0x0121
            java.lang.String r2 = ".mp3"
            boolean r2 = r11.endsWith(r2)     // Catch:{ MalformedURLException -> 0x01d2, IOException -> 0x01d9 }
            if (r2 != 0) goto L_0x0121
            java.lang.String r2 = ".mp4"
            boolean r2 = r11.endsWith(r2)     // Catch:{ MalformedURLException -> 0x01d2, IOException -> 0x01d9 }
            if (r2 != 0) goto L_0x0121
            java.lang.String r2 = ".mpeg"
            boolean r2 = r11.endsWith(r2)     // Catch:{ MalformedURLException -> 0x01d2, IOException -> 0x01d9 }
            if (r2 != 0) goto L_0x0121
            java.lang.String r2 = ".wav"
            boolean r2 = r11.endsWith(r2)     // Catch:{ MalformedURLException -> 0x01d2, IOException -> 0x01d9 }
            if (r2 == 0) goto L_0x0140
        L_0x0121:
            android.content.Intent r2 = new android.content.Intent     // Catch:{ MalformedURLException -> 0x01d2, IOException -> 0x01d9 }
            java.lang.String r3 = "android.intent.action.VIEW"
            r2.<init>(r3)     // Catch:{ MalformedURLException -> 0x01d2, IOException -> 0x01d9 }
            r3 = 268435456(0x10000000, float:2.5243549E-29)
            r2.addFlags(r3)     // Catch:{ MalformedURLException -> 0x01d2, IOException -> 0x01d9 }
            java.lang.String r3 = com.adwo.adsdk.R.a(r11)     // Catch:{ MalformedURLException -> 0x01d2, IOException -> 0x01d9 }
            android.net.Uri r4 = android.net.Uri.parse(r11)     // Catch:{ MalformedURLException -> 0x01d2, IOException -> 0x01d9 }
            r2.setDataAndType(r4, r3)     // Catch:{ MalformedURLException -> 0x01d2, IOException -> 0x01d9 }
            if (r0 == 0) goto L_0x013d
            r0.startActivity(r2)     // Catch:{ ActivityNotFoundException -> 0x027e }
        L_0x013d:
            r0 = r6
            goto L_0x000e
        L_0x0140:
            java.net.URL r2 = new java.net.URL     // Catch:{ MalformedURLException -> 0x01d2, IOException -> 0x01d9 }
            r2.<init>(r11)     // Catch:{ MalformedURLException -> 0x01d2, IOException -> 0x01d9 }
            r3 = 0
            java.net.HttpURLConnection.setFollowRedirects(r3)     // Catch:{ MalformedURLException -> 0x01d2, IOException -> 0x01d9 }
            java.net.URLConnection r9 = r2.openConnection()     // Catch:{ MalformedURLException -> 0x01d2, IOException -> 0x01d9 }
            java.net.HttpURLConnection r9 = (java.net.HttpURLConnection) r9     // Catch:{ MalformedURLException -> 0x01d2, IOException -> 0x01d9 }
            java.lang.String r2 = "GET"
            r9.setRequestMethod(r2)     // Catch:{ MalformedURLException -> 0x01d2, IOException -> 0x01d9 }
            r9.connect()     // Catch:{ MalformedURLException -> 0x01d2, IOException -> 0x01d9 }
            java.lang.String r2 = "Location"
            java.lang.String r2 = r9.getHeaderField(r2)     // Catch:{ MalformedURLException -> 0x01d2, IOException -> 0x01d9 }
            java.lang.String r3 = "Content-Type"
            java.lang.String r1 = r9.getHeaderField(r3)     // Catch:{ MalformedURLException -> 0x01d2, IOException -> 0x01d9 }
            r9.getResponseCode()     // Catch:{ MalformedURLException -> 0x01d2, IOException -> 0x0281 }
            java.lang.String r3 = "Adwo AdSDK"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ MalformedURLException -> 0x01d2, IOException -> 0x0281 }
            java.lang.String r5 = "Response Code:"
            r4.<init>(r5)     // Catch:{ MalformedURLException -> 0x01d2, IOException -> 0x0281 }
            int r5 = r9.getResponseCode()     // Catch:{ MalformedURLException -> 0x01d2, IOException -> 0x0281 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ MalformedURLException -> 0x01d2, IOException -> 0x0281 }
            java.lang.String r5 = " Response Message:"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ MalformedURLException -> 0x01d2, IOException -> 0x0281 }
            java.lang.String r5 = r9.getResponseMessage()     // Catch:{ MalformedURLException -> 0x01d2, IOException -> 0x0281 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ MalformedURLException -> 0x01d2, IOException -> 0x0281 }
            java.lang.String r4 = r4.toString()     // Catch:{ MalformedURLException -> 0x01d2, IOException -> 0x0281 }
            android.util.Log.d(r3, r4)     // Catch:{ MalformedURLException -> 0x01d2, IOException -> 0x0281 }
            java.lang.String r3 = "Adwo AdSDK"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ MalformedURLException -> 0x01d2, IOException -> 0x0281 }
            java.lang.String r5 = "locurlString: "
            r4.<init>(r5)     // Catch:{ MalformedURLException -> 0x01d2, IOException -> 0x0281 }
            java.lang.StringBuilder r2 = r4.append(r2)     // Catch:{ MalformedURLException -> 0x01d2, IOException -> 0x0281 }
            java.lang.String r2 = r2.toString()     // Catch:{ MalformedURLException -> 0x01d2, IOException -> 0x0281 }
            android.util.Log.i(r3, r2)     // Catch:{ MalformedURLException -> 0x01d2, IOException -> 0x0281 }
        L_0x01a0:
            if (r11 == 0) goto L_0x01e2
            android.net.Uri r2 = android.net.Uri.parse(r11)
            if (r1 != 0) goto L_0x01aa
            java.lang.String r1 = ""
        L_0x01aa:
            if (r2 == 0) goto L_0x01cf
            java.lang.String r3 = r2.getScheme()
            if (r3 == 0) goto L_0x01cf
            java.lang.String r3 = r2.getScheme()
            java.lang.String r4 = "market"
            boolean r3 = r3.equalsIgnoreCase(r4)
            if (r3 == 0) goto L_0x01e5
            java.lang.String r1 = "Adwo AdSDK"
            java.lang.String r3 = "Android Market URL, launch the Market Application"
            android.util.Log.i(r1, r3)
            android.content.Intent r1 = new android.content.Intent
            java.lang.String r3 = "android.intent.action.VIEW"
            r1.<init>(r3, r2)
            r0.startActivity(r1)
        L_0x01cf:
            r0 = r6
            goto L_0x000e
        L_0x01d2:
            r0 = move-exception
            r0.printStackTrace()
            r0 = r7
            goto L_0x000e
        L_0x01d9:
            r2 = move-exception
            r8 = r2
            r2 = r1
            r1 = r8
        L_0x01dd:
            r1.printStackTrace()
            r1 = r2
            goto L_0x01a0
        L_0x01e2:
            r0 = r7
            goto L_0x000e
        L_0x01e5:
            java.lang.String r3 = r2.getScheme()
            java.lang.String r4 = "rtsp"
            boolean r3 = r3.equalsIgnoreCase(r4)
            if (r3 != 0) goto L_0x01cf
            java.lang.String r3 = r2.getScheme()
            java.lang.String r4 = "http"
            boolean r3 = r3.equalsIgnoreCase(r4)
            if (r3 == 0) goto L_0x020d
            java.lang.String r3 = "video/mp4"
            boolean r3 = r1.equalsIgnoreCase(r3)
            if (r3 != 0) goto L_0x01cf
            java.lang.String r3 = "video/3gpp"
            boolean r1 = r1.equalsIgnoreCase(r3)
            if (r1 != 0) goto L_0x01cf
        L_0x020d:
            java.lang.String r1 = r2.getScheme()
            java.lang.String r3 = "tel"
            boolean r1 = r1.equalsIgnoreCase(r3)
            if (r1 == 0) goto L_0x022b
            java.lang.String r1 = "Adwo AdSDK"
            java.lang.String r3 = "Telephone Number, launch the phone"
            android.util.Log.i(r1, r3)
            android.content.Intent r1 = new android.content.Intent
            java.lang.String r3 = "android.intent.action.DIAL"
            r1.<init>(r3, r2)
            r0.startActivity(r1)
            goto L_0x01cf
        L_0x022b:
            java.lang.String r1 = r2.getScheme()
            java.lang.String r3 = "sms"
            boolean r1 = r1.equalsIgnoreCase(r3)
            if (r1 == 0) goto L_0x0242
            android.content.Intent r1 = new android.content.Intent
            java.lang.String r3 = "android.intent.action.VIEW"
            r1.<init>(r3, r2)
            r0.startActivity(r1)
            goto L_0x01cf
        L_0x0242:
            java.lang.String r0 = r2.getScheme()
            java.lang.String r1 = "http"
            boolean r0 = r0.equalsIgnoreCase(r1)
            if (r0 == 0) goto L_0x026c
            java.lang.String r0 = r2.getLastPathSegment()
            if (r0 == 0) goto L_0x026c
            java.lang.String r0 = r2.getLastPathSegment()
            java.lang.String r1 = ".mp4"
            boolean r0 = r0.endsWith(r1)
            if (r0 != 0) goto L_0x01cf
            java.lang.String r0 = r2.getLastPathSegment()
            java.lang.String r1 = ".3gp"
            boolean r0 = r0.endsWith(r1)
            if (r0 != 0) goto L_0x01cf
        L_0x026c:
            java.lang.String r0 = r2.toString()
            r10.loadUrl(r0)
            goto L_0x01cf
        L_0x0275:
            r0 = move-exception
            goto L_0x002b
        L_0x0278:
            r0 = move-exception
            goto L_0x0045
        L_0x027b:
            r0 = move-exception
            goto L_0x0077
        L_0x027e:
            r0 = move-exception
            goto L_0x013d
        L_0x0281:
            r2 = move-exception
            r8 = r2
            r2 = r1
            r1 = r8
            goto L_0x01dd
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adwo.adsdk.D.shouldOverrideUrlLoading(android.webkit.WebView, java.lang.String):boolean");
    }

    public final void onPageFinished(WebView webView, String str) {
        if (this.a.a.canGoBack()) {
            if (this.a.f != null) {
                this.a.f.setVisibility(0);
            }
        } else if (this.a.f != null) {
            this.a.f.setVisibility(4);
        }
        if (this.a.a.canGoForward()) {
            if (this.a.e != null) {
                this.a.e.setVisibility(0);
            }
        } else if (this.a.e != null) {
            this.a.e.setVisibility(4);
        }
        C0012m mVar = this.a;
        C0012m.b(this.a.a());
    }
}
