package com.adwo.adsdk;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;
import java.util.ArrayList;
import java.util.List;

public final class FSAd implements Parcelable {
    public static final Parcelable.Creator CREATOR = new S();
    protected int a = -1;
    protected String b = null;
    protected String c = null;
    protected String d = null;
    protected byte e;
    protected List f = new ArrayList();
    protected String g = null;
    protected List h = new ArrayList();
    protected String i = null;

    protected static FSAd a(byte[] bArr) {
        FSAd a2 = Q.a(bArr);
        if (a2 == null) {
            return null;
        }
        if (a2.b == null && a2.d == null) {
            return null;
        }
        if (a2.b != null && a2.b.length() == 0) {
            return null;
        }
        if (a2.d != null && a2.d.length() == 0) {
            return null;
        }
        Log.d("Adwo SDK", "Get an ad from Adwo servers.");
        return a2;
    }

    protected FSAd() {
    }

    public final boolean equals(Object obj) {
        if (!(obj instanceof C0008i)) {
            return false;
        }
        C0008i iVar = (C0008i) obj;
        if (this.b != null && iVar.c != null && this.b.equals(iVar.c)) {
            return true;
        }
        if (this.d != null && iVar.e != null && this.d.equals(iVar.e)) {
            return true;
        }
        if (this.g == null || iVar.i == null || !this.g.equals(iVar.i)) {
            return false;
        }
        return true;
    }

    public final int hashCode() {
        return toString().hashCode();
    }

    public final int describeContents() {
        return 0;
    }

    public final void writeToParcel(Parcel parcel, int i2) {
        parcel.writeInt(this.a);
        parcel.writeString(this.b);
        parcel.writeString(this.c);
        parcel.writeString(this.d);
        parcel.writeByte(this.e);
        parcel.writeString(this.g);
        parcel.writeString(this.i);
    }
}
