package com.adwo.adsdk;

import android.content.Context;
import android.util.Log;
import java.util.ArrayList;
import java.util.List;

/* renamed from: com.adwo.adsdk.i  reason: case insensitive filesystem */
final class C0008i {
    Context a;
    protected int b = -1;
    protected String c = null;
    protected String d = null;
    protected String e = null;
    protected byte f;
    protected List g = new ArrayList();
    protected List h = new ArrayList();
    protected String i = null;
    C0010k j;

    public static C0008i a(Context context, byte[] bArr) {
        C0008i b2 = Q.b(bArr);
        if (b2 == null) {
            return null;
        }
        b2.a = context;
        if (b2.c == null && b2.e == null) {
            return null;
        }
        if (b2.c != null && b2.c.length() == 0) {
            return null;
        }
        if (b2.e != null && b2.e.length() == 0) {
            return null;
        }
        Log.d("Adwo SDK", "Get an ad from Adwo servers.");
        return b2;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        if (this.d != null) {
            new C0009j(this).start();
        }
    }

    public final String b() {
        return this.c;
    }

    public final String c() {
        return this.e;
    }

    public final String toString() {
        return this.c;
    }

    public final boolean equals(Object obj) {
        if (!(obj instanceof C0008i)) {
            return false;
        }
        C0008i iVar = (C0008i) obj;
        if (this.c != null && iVar.c != null && this.c.equals(iVar.c)) {
            return true;
        }
        if (this.e != null && iVar.e != null && this.e.equals(iVar.e)) {
            return true;
        }
        if (this.i == null || iVar.i == null || !this.i.equals(iVar.i)) {
            return false;
        }
        return true;
    }

    public final int hashCode() {
        return toString().hashCode();
    }
}
