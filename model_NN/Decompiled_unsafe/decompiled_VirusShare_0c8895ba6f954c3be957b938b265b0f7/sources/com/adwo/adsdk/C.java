package com.adwo.adsdk;

import android.graphics.Bitmap;
import android.os.Message;
import android.view.View;
import android.webkit.GeolocationPermissions;
import android.webkit.JsPromptResult;
import android.webkit.JsResult;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebStorage;
import android.webkit.WebView;

final class C extends WebChromeClient {
    private /* synthetic */ C0012m a;

    C(C0012m mVar) {
        this.a = mVar;
    }

    public final Bitmap getDefaultVideoPoster() {
        return super.getDefaultVideoPoster();
    }

    public final View getVideoLoadingProgressView() {
        return super.getVideoLoadingProgressView();
    }

    public final void getVisitedHistory(ValueCallback valueCallback) {
        super.getVisitedHistory(valueCallback);
    }

    public final void onCloseWindow(WebView webView) {
        super.onCloseWindow(webView);
    }

    public final void onConsoleMessage(String str, int i, String str2) {
        super.onConsoleMessage(str, i, str2);
    }

    public final boolean onCreateWindow(WebView webView, boolean z, boolean z2, Message message) {
        return super.onCreateWindow(webView, z, z2, message);
    }

    public final void onExceededDatabaseQuota(String str, String str2, long j, long j2, long j3, WebStorage.QuotaUpdater quotaUpdater) {
        super.onExceededDatabaseQuota(str, str2, j, j2, j3, quotaUpdater);
    }

    public final void onHideCustomView() {
        super.onHideCustomView();
    }

    public final boolean onJsAlert(WebView webView, String str, String str2, JsResult jsResult) {
        return super.onJsAlert(webView, str, str2, jsResult);
    }

    public final boolean onJsBeforeUnload(WebView webView, String str, String str2, JsResult jsResult) {
        return super.onJsBeforeUnload(webView, str, str2, jsResult);
    }

    public final boolean onJsConfirm(WebView webView, String str, String str2, JsResult jsResult) {
        return super.onJsConfirm(webView, str, str2, jsResult);
    }

    public final boolean onJsPrompt(WebView webView, String str, String str2, String str3, JsPromptResult jsPromptResult) {
        return super.onJsPrompt(webView, str, str2, str3, jsPromptResult);
    }

    public final boolean onJsTimeout() {
        return super.onJsTimeout();
    }

    public final void onReachedMaxAppCacheSize(long j, long j2, WebStorage.QuotaUpdater quotaUpdater) {
        super.onReachedMaxAppCacheSize(j, j2, quotaUpdater);
    }

    public final void onReceivedIcon(WebView webView, Bitmap bitmap) {
        super.onReceivedIcon(webView, bitmap);
    }

    public final void onReceivedTouchIconUrl(WebView webView, String str, boolean z) {
        super.onReceivedTouchIconUrl(webView, str, z);
    }

    public final void onRequestFocus(WebView webView) {
        super.onRequestFocus(webView);
    }

    public final void onShowCustomView(View view, WebChromeClient.CustomViewCallback customViewCallback) {
        super.onShowCustomView(view, customViewCallback);
    }

    public final void onReceivedTitle(WebView webView, String str) {
        this.a.b().setTitle(str);
        super.onReceivedTitle(webView, str);
    }

    public final void onProgressChanged(WebView webView, int i) {
        this.a.b().setProgress(i * 100);
        super.onProgressChanged(webView, i);
    }

    public final void onGeolocationPermissionsHidePrompt() {
        super.onGeolocationPermissionsHidePrompt();
    }

    public final void onGeolocationPermissionsShowPrompt(String str, GeolocationPermissions.Callback callback) {
        super.onGeolocationPermissionsShowPrompt(str, callback);
        callback.invoke(str, true, false);
    }
}
