package com.adwo.adsdk;

import android.os.Handler;
import android.os.Message;
import com.anderfans.girlfart.R;

/* renamed from: com.adwo.adsdk.n  reason: case insensitive filesystem */
final class C0013n extends Handler {
    private /* synthetic */ C0012m a;

    C0013n(C0012m mVar) {
        this.a = mVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.adwo.adsdk.m.a(com.adwo.adsdk.m, boolean):void
     arg types: [com.adwo.adsdk.m, int]
     candidates:
      com.adwo.adsdk.m.a(java.lang.String, android.app.Activity):void
      com.adwo.adsdk.m.a(com.adwo.adsdk.m, boolean):void */
    public final void handleMessage(Message message) {
        switch (message.what) {
            case R.styleable.com_adwo_adsdk_AdwoAdView_secondaryTextColor /*2*/:
                C0012m.a(this.a, true);
                return;
            default:
                return;
        }
    }
}
