package com.adwo.adsdk;

public interface SplashAdListener {
    void onFailedToReceiveAd();

    void onReceiveAd(FSAd fSAd);
}
