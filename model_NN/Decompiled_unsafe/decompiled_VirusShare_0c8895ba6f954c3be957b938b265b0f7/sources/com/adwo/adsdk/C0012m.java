package com.adwo.adsdk;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Environment;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.animation.AlphaAnimation;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.webkit.CookieManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import com.anderfans.girlfart.critical.Critical;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

/* renamed from: com.adwo.adsdk.m  reason: case insensitive filesystem */
final class C0012m extends FrameLayout implements GestureDetector.OnGestureListener {
    private static String o;
    protected WebView a;
    private NotificationManager b;
    private Notification c = null;
    private volatile boolean d = false;
    /* access modifiers changed from: private */
    public Button e;
    /* access modifiers changed from: private */
    public Button f;
    private RelativeLayout g;
    private RelativeLayout h;
    private Drawable i;
    private Drawable j;
    private ImageView k;
    private WeakReference l;
    /* access modifiers changed from: private */
    public A m;
    private String n;
    /* access modifiers changed from: private */
    public GestureDetector p;

    public final void a(A a2) {
        this.m = a2;
    }

    public final String a() {
        return this.n;
    }

    public final void a(String str) {
        this.n = str;
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:0x002c  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0039  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0040  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean b(java.lang.String r4) {
        /*
            r0 = 0
            r1 = 0
            java.net.URL r2 = new java.net.URL     // Catch:{ MalformedURLException -> 0x0029, IOException -> 0x0030, all -> 0x003d }
            r2.<init>(r4)     // Catch:{ MalformedURLException -> 0x0029, IOException -> 0x0030, all -> 0x003d }
            java.net.URLConnection r4 = r2.openConnection()     // Catch:{ MalformedURLException -> 0x0029, IOException -> 0x0030, all -> 0x003d }
            java.net.HttpURLConnection r4 = (java.net.HttpURLConnection) r4     // Catch:{ MalformedURLException -> 0x0029, IOException -> 0x0030, all -> 0x003d }
            int r1 = com.adwo.adsdk.R.b     // Catch:{ MalformedURLException -> 0x004d, IOException -> 0x004a, all -> 0x0044 }
            r4.setConnectTimeout(r1)     // Catch:{ MalformedURLException -> 0x004d, IOException -> 0x004a, all -> 0x0044 }
            int r1 = com.adwo.adsdk.R.b     // Catch:{ MalformedURLException -> 0x004d, IOException -> 0x004a, all -> 0x0044 }
            r4.setReadTimeout(r1)     // Catch:{ MalformedURLException -> 0x004d, IOException -> 0x004a, all -> 0x0044 }
            r4.connect()     // Catch:{ MalformedURLException -> 0x004d, IOException -> 0x004a, all -> 0x0044 }
            int r1 = r4.getResponseCode()     // Catch:{ MalformedURLException -> 0x004d, IOException -> 0x004a, all -> 0x0044 }
            r2 = 200(0xc8, float:2.8E-43)
            if (r1 != r2) goto L_0x0023
            r0 = 1
        L_0x0023:
            if (r4 == 0) goto L_0x0028
            r4.disconnect()
        L_0x0028:
            return r0
        L_0x0029:
            r2 = move-exception
        L_0x002a:
            if (r1 == 0) goto L_0x0028
            r1.disconnect()
            goto L_0x0028
        L_0x0030:
            r2 = move-exception
            r3 = r2
            r2 = r1
            r1 = r3
        L_0x0034:
            r1.printStackTrace()     // Catch:{ all -> 0x0047 }
            if (r2 == 0) goto L_0x0028
            r2.disconnect()
            goto L_0x0028
        L_0x003d:
            r0 = move-exception
        L_0x003e:
            if (r1 == 0) goto L_0x0043
            r1.disconnect()
        L_0x0043:
            throw r0
        L_0x0044:
            r0 = move-exception
            r1 = r4
            goto L_0x003e
        L_0x0047:
            r0 = move-exception
            r1 = r2
            goto L_0x003e
        L_0x004a:
            r1 = move-exception
            r2 = r4
            goto L_0x0034
        L_0x004d:
            r1 = move-exception
            r1 = r4
            goto L_0x002a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adwo.adsdk.C0012m.b(java.lang.String):boolean");
    }

    C0012m(Activity activity, int i2, long j2, String str, boolean z, boolean z2, boolean z3) {
        super(activity);
        new C0013n(this);
        this.n = null;
        this.p = null;
        this.l = new WeakReference(activity);
        setId(15062);
        Activity activity2 = (Activity) this.l.get();
        if (activity2 != null) {
            this.b = (NotificationManager) activity2.getSystemService("notification");
            this.c = new Notification(17301598, "程序下载完成", System.currentTimeMillis());
            this.c.defaults = 1;
            activity2.setTheme(16973840);
            setWillNotDraw(false);
            float f2 = activity2.getResources().getDisplayMetrics().density;
            Integer valueOf = Integer.valueOf((int) (0.0625f * f2 * ((float) i2)));
            setPadding(valueOf.intValue(), valueOf.intValue(), valueOf.intValue(), valueOf.intValue());
            this.h = new RelativeLayout(activity2);
            this.h.setLayoutParams(new FrameLayout.LayoutParams(-1, -1));
            Activity activity3 = (Activity) this.l.get();
            if (activity3 != null) {
                this.a = new WebView(activity3);
                this.a.setId(Critical.CostBadMode);
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -1);
                this.a.setLayoutParams(layoutParams);
                this.a.setWebViewClient(new D(this));
                this.a.setWebChromeClient(new C(this));
                this.a.addJavascriptInterface(new B(this, activity3), "interface");
                this.a.requestFocusFromTouch();
                this.a.requestFocusFromTouch();
                this.p = new GestureDetector(this);
                this.a.setOnTouchListener(new C0018s(this));
                CookieManager.getInstance().setAcceptCookie(true);
                WebSettings settings = this.a.getSettings();
                settings.setJavaScriptEnabled(true);
                settings.setDefaultTextEncodingName("utf-8");
                settings.setJavaScriptCanOpenWindowsAutomatically(true);
                settings.setGeolocationEnabled(true);
                settings.setBuiltInZoomControls(true);
                settings.setAllowFileAccess(true);
                settings.setSupportZoom(true);
                settings.setBuiltInZoomControls(true);
                settings.setPluginsEnabled(true);
                settings.setSaveFormData(true);
                settings.setLightTouchEnabled(true);
                o = settings.getUserAgentString();
                if (z) {
                    layoutParams.addRule(3, 100);
                }
                this.h.addView(this.a);
                if (z3) {
                    this.a.setBackgroundColor(0);
                    this.h.setBackgroundColor(0);
                } else {
                    this.a.setBackgroundColor(-1);
                    this.h.setBackgroundColor(-1);
                }
                this.g = new RelativeLayout(activity3);
                RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-1, (int) (40.0f * f2));
                layoutParams2.addRule(12);
                this.g.setBackgroundColor(-3355444);
                this.g.setId(300);
                AssetManager assets = activity3.getAssets();
                this.k = new ImageView(activity3);
                try {
                    this.k.setBackgroundDrawable(Drawable.createFromStream(assets.open("adwo_close.png"), "adwo_close.png"));
                } catch (IOException e2) {
                    e2.printStackTrace();
                }
                RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams((int) (31.0f * f2), (int) (31.0f * f2));
                layoutParams3.addRule(11);
                layoutParams3.addRule(10);
                layoutParams3.setMargins(0, (int) (10.0f * f2), (int) (10.0f * f2), 0);
                this.k.setVisibility(8);
                this.h.addView(this.k, layoutParams3);
                this.e = new Button(activity3);
                try {
                    this.i = Drawable.createFromStream(assets.open("adwo_right_arrow.png"), "adwo_right_arrow.png");
                } catch (IOException e3) {
                    this.e.setBackgroundColor(-3355444);
                    this.e.setText(">>");
                    this.e.setTextColor(-16777216);
                    e3.printStackTrace();
                }
                b(z2);
                this.f = new Button(activity3);
                try {
                    this.j = Drawable.createFromStream(assets.open("adwo_left_arrow.png"), "adwo_left_arrow.png");
                } catch (IOException e4) {
                    this.f.setBackgroundColor(-3355444);
                    this.f.setText("<<");
                    this.f.setTextColor(-16777216);
                    e4.printStackTrace();
                }
                this.f.setId(301);
                c(z2);
                this.f.setId(302);
                RelativeLayout.LayoutParams layoutParams4 = new RelativeLayout.LayoutParams((int) (48.0f * f2), (int) (31.0f * f2));
                layoutParams4.addRule(15);
                this.g.addView(this.f, layoutParams4);
                this.e.setId(303);
                RelativeLayout.LayoutParams layoutParams5 = new RelativeLayout.LayoutParams((int) (48.0f * f2), (int) (f2 * 31.0f));
                layoutParams5.addRule(1, this.f.getId());
                layoutParams5.addRule(15);
                this.g.addView(this.e, layoutParams5);
                this.f.setVisibility(4);
                this.e.setVisibility(4);
                this.h.addView(this.g, layoutParams2);
                addView(this.h);
                String str2 = str == null ? "toptobottom" : str;
                if (str2.equals("toptobottom")) {
                    TranslateAnimation translateAnimation = new TranslateAnimation(1, 0.0f, 1, 0.0f, 1, -1.0f, 1, 0.0f);
                    translateAnimation.setDuration(j2);
                    translateAnimation.setAnimationListener(new C0019t(this));
                    startAnimation(translateAnimation);
                } else if (str2.equals("explode")) {
                    ScaleAnimation scaleAnimation = new ScaleAnimation(1.1f, 0.9f, 0.1f, 0.9f, 1, 0.5f, 1, 0.5f);
                    scaleAnimation.setDuration(j2);
                    scaleAnimation.setAnimationListener(new C0020u(this));
                    startAnimation(scaleAnimation);
                } else {
                    TranslateAnimation translateAnimation2 = new TranslateAnimation(1, 0.0f, 1, 0.0f, 1, 1.0f, 1, 0.0f);
                    translateAnimation2.setDuration(j2);
                    translateAnimation2.setAnimationListener(new C0021v(this));
                    startAnimation(translateAnimation2);
                }
                this.g.setVisibility(8);
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        new Rect(new Rect(canvas.getClipBounds())).inset(0, 0);
        Paint paint = new Paint();
        paint.setStyle(Paint.Style.FILL);
        paint.setAntiAlias(true);
    }

    /* access modifiers changed from: package-private */
    public final void c(String str) {
        new Thread(new C0022w(this, str)).start();
    }

    /* access modifiers changed from: package-private */
    public final void d(String str) {
        new Thread(new C0023x(this, str)).start();
    }

    private void b(boolean z) {
        if (this.e != null && z) {
            this.e.setBackgroundDrawable(this.i);
            this.e.setOnClickListener(new y(this));
            this.e.setEnabled(true);
        }
    }

    private void c(boolean z) {
        if (this.f != null && z) {
            this.f.setBackgroundDrawable(this.j);
            this.f.setOnClickListener(new z(this));
            this.f.setEnabled(true);
        }
    }

    /* access modifiers changed from: protected */
    public final void a(boolean z) {
        if (this.k != null) {
            this.k.setVisibility(0);
            this.k.setOnClickListener(new C0014o(this));
            this.k.setEnabled(true);
        }
    }

    static /* synthetic */ void a(C0012m mVar, boolean z) {
        Activity activity = (Activity) mVar.l.get();
        if (activity == null) {
            return;
        }
        if (1 != 0) {
            AlphaAnimation alphaAnimation = new AlphaAnimation(1.0f, 0.0f);
            alphaAnimation.setDuration(200);
            activity.finish();
            mVar.startAnimation(alphaAnimation);
            return;
        }
        activity.finish();
    }

    /* access modifiers changed from: package-private */
    public final Activity b() {
        return (Activity) this.l.get();
    }

    /* access modifiers changed from: protected */
    public final void c() {
        if (this.g != null) {
            this.g.setVisibility(0);
            TranslateAnimation translateAnimation = new TranslateAnimation(1, 0.0f, 1, 0.0f, 1, 1.0f, 1, 0.0f);
            translateAnimation.setDuration(300);
            translateAnimation.setAnimationListener(new C0015p(this));
            this.g.startAnimation(translateAnimation);
            a(true);
            b(true);
            c(true);
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(String str, Activity activity) {
        String str2;
        CharSequence charSequence;
        String substring = str.substring(str.lastIndexOf("/") + 1);
        String externalStorageState = Environment.getExternalStorageState();
        if (!externalStorageState.equals("mounted")) {
            if (externalStorageState.equals("shared")) {
                str2 = "SD 卡正忙。要允许下载，请在通知中选择\"关闭USB 存储\"";
                charSequence = "SD 卡不可用";
            } else {
                str2 = "需要有 SD 卡才能下载" + substring;
                charSequence = "无 SD 卡";
            }
            new AlertDialog.Builder(b()).setTitle(charSequence).setIcon(17301543).setMessage(str2).setPositiveButton("OK", (DialogInterface.OnClickListener) null).show();
            return;
        }
        activity.runOnUiThread(new C0016q(this, activity, substring));
        if (!this.d) {
            this.d = true;
            DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
            HttpGet httpGet = new HttpGet(str);
            httpGet.setHeader("User-Agent", o);
            try {
                HttpResponse execute = defaultHttpClient.execute(httpGet);
                if (execute.getStatusLine().getStatusCode() == 200) {
                    InputStream content = execute.getEntity().getContent();
                    File file = new File(Environment.getExternalStorageDirectory() + "/adwo/");
                    if (!file.exists()) {
                        file.mkdir();
                    }
                    File file2 = new File(Environment.getExternalStorageDirectory() + "/adwo/", substring);
                    if (file2.exists() || file2.createNewFile()) {
                        FileOutputStream fileOutputStream = new FileOutputStream(file2);
                        while (true) {
                            int read = content.read();
                            if (read == -1) {
                                break;
                            }
                            fileOutputStream.write(read);
                        }
                        content.close();
                        fileOutputStream.close();
                        Uri fromFile = Uri.fromFile(file2);
                        Intent intent = new Intent("android.intent.action.VIEW");
                        intent.setFlags(268435456);
                        intent.setDataAndType(fromFile, "application/vnd.android.package-archive");
                        PendingIntent activity2 = PendingIntent.getActivity(activity, 0, intent, 268435456);
                        if (!(this.c == null || activity2 == null)) {
                            this.c.setLatestEventInfo(activity, String.valueOf(substring) + "下载完成", String.valueOf(substring) + "下载完成,请安装使用", activity2);
                        }
                        this.b.notify(0, this.c);
                    } else {
                        return;
                    }
                }
                this.d = false;
            } catch (IOException e2) {
                e2.printStackTrace();
            } catch (Exception e3) {
            } finally {
                this.d = false;
            }
        }
    }

    static /* synthetic */ void a(C0012m mVar, String str, Activity activity) {
        String str2;
        CharSequence charSequence;
        String substring = str.substring(str.lastIndexOf("/") + 1);
        String externalStorageState = Environment.getExternalStorageState();
        if (!externalStorageState.equals("mounted")) {
            if (externalStorageState.equals("shared")) {
                str2 = "SD 卡正忙。要允许下载，请在通知中选择\"关闭USB 存储\"";
                charSequence = "SD 卡不可用";
            } else {
                str2 = "需要有 SD 卡才能下载" + substring;
                charSequence = "无 SD 卡";
            }
            new AlertDialog.Builder(mVar.b()).setTitle(charSequence).setIcon(17301543).setMessage(str2).setPositiveButton("OK", (DialogInterface.OnClickListener) null).show();
            return;
        }
        activity.runOnUiThread(new C0017r(mVar, activity, substring));
        if (!mVar.d) {
            mVar.d = true;
            DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
            HttpGet httpGet = new HttpGet(str);
            httpGet.setHeader("User-Agent", o);
            try {
                HttpResponse execute = defaultHttpClient.execute(httpGet);
                if (execute.getStatusLine().getStatusCode() == 200) {
                    InputStream content = execute.getEntity().getContent();
                    File file = new File(Environment.getExternalStorageDirectory() + "/adwo/");
                    if (!file.exists()) {
                        file.mkdir();
                    }
                    File file2 = new File(Environment.getExternalStorageDirectory() + "/adwo/", substring);
                    if (file2.exists() || file2.createNewFile()) {
                        FileOutputStream fileOutputStream = new FileOutputStream(file2);
                        while (true) {
                            int read = content.read();
                            if (read == -1) {
                                break;
                            }
                            fileOutputStream.write(read);
                        }
                        content.close();
                        fileOutputStream.close();
                        Uri fromFile = Uri.fromFile(file2);
                        Intent intent = new Intent("android.intent.action.VIEW");
                        intent.setFlags(268435456);
                        intent.setDataAndType(fromFile, "image/bmp");
                        intent.setDataAndType(fromFile, "image/gif");
                        intent.setDataAndType(fromFile, "image/jpeg");
                        PendingIntent activity2 = PendingIntent.getActivity(activity, 0, intent, 268435456);
                        if (!(mVar.c == null || activity2 == null)) {
                            mVar.c.setLatestEventInfo(activity, String.valueOf(substring) + "下载完成", String.valueOf(substring) + "下载完成,请安装使用", activity2);
                        }
                        mVar.b.notify(0, mVar.c);
                    } else {
                        return;
                    }
                }
                mVar.d = false;
            } catch (IOException e2) {
                e2.printStackTrace();
            } catch (Exception e3) {
            } finally {
                mVar.d = false;
            }
        }
    }

    public final boolean onDown(MotionEvent motionEvent) {
        return false;
    }

    public final boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent2, float f2, float f3) {
        if (motionEvent.getX() - motionEvent2.getX() > 120.0f && Math.abs(f2) > 200.0f) {
            this.a.goBack();
            return false;
        } else if (motionEvent2.getX() - motionEvent.getX() <= 120.0f || Math.abs(f2) <= 200.0f) {
            return false;
        } else {
            this.a.goForward();
            return false;
        }
    }

    public final void onLongPress(MotionEvent motionEvent) {
    }

    public final boolean onScroll(MotionEvent motionEvent, MotionEvent motionEvent2, float f2, float f3) {
        return false;
    }

    public final void onShowPress(MotionEvent motionEvent) {
    }

    public final boolean onSingleTapUp(MotionEvent motionEvent) {
        return false;
    }
}
