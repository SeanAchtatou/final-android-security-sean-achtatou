package com.adwo.adsdk;

import android.view.animation.DecelerateInterpolator;

final class M implements Runnable {
    /* access modifiers changed from: private */
    public C0001b a;
    /* access modifiers changed from: private */
    public C0001b b;
    private P c;
    /* access modifiers changed from: private */
    public /* synthetic */ AdwoAdView d;

    public M(AdwoAdView adwoAdView, C0001b bVar, P p) {
        this.d = adwoAdView;
        this.a = bVar;
        this.c = p;
    }

    public final void run() {
        this.b = this.d.c;
        if (this.b != null) {
            this.b.setVisibility(8);
            this.b.c();
        }
        this.a.setVisibility(0);
        X x = new X(90.0f, 0.0f, ((float) this.d.getWidth()) / 2.0f, ((float) this.d.getHeight()) / 2.0f, 0.0f, 0.0f, -0.4f * ((float) this.d.getWidth()), false, this.c);
        x.setDuration(500);
        x.setFillAfter(true);
        x.setInterpolator(new DecelerateInterpolator());
        x.setAnimationListener(new N(this));
        this.d.startAnimation(x);
    }
}
