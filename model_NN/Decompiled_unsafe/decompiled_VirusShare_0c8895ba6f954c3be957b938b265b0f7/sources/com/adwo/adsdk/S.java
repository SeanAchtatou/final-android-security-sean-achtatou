package com.adwo.adsdk;

import android.os.Parcel;
import android.os.Parcelable;

final class S implements Parcelable.Creator {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        FSAd fSAd = new FSAd();
        fSAd.a = parcel.readInt();
        fSAd.b = parcel.readString();
        fSAd.c = parcel.readString();
        fSAd.d = parcel.readString();
        fSAd.e = parcel.readByte();
        fSAd.g = parcel.readString();
        fSAd.i = parcel.readString();
        return fSAd;
    }

    public final /* bridge */ /* synthetic */ Object[] newArray(int i) {
        return new FSAd[i];
    }

    S() {
    }
}
