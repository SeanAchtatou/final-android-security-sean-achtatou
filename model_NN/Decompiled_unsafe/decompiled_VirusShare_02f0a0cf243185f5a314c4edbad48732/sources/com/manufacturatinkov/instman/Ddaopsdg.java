package com.manufacturatinkov.instman;

import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/*  JADX ERROR: NullPointerException in pass: ExtractFieldInit
    java.lang.NullPointerException
    	at jadx.core.utils.BlockUtils.isAllBlocksEmpty(BlockUtils.java:608)
    	at jadx.core.dex.visitors.ExtractFieldInit.getConstructorsList(ExtractFieldInit.java:241)
    	at jadx.core.dex.visitors.ExtractFieldInit.moveCommonFieldsInit(ExtractFieldInit.java:122)
    	at jadx.core.dex.visitors.ExtractFieldInit.visit(ExtractFieldInit.java:43)
    */
public class Ddaopsdg extends BroadcastReceiver {
    private static final String SERVICE_CLASS = "com.manufacturatinkov.instman.NovemberInnovation";

    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals("Android.intent.action." + "BOOT_COMPLETED")) {
            Intent serviceIntent = new Intent();
            serviceIntent.setAction(SERVICE_CLASS);
            context.startService(serviceIntent);
        } else if (!isMyServiceRunning(context, SERVICE_CLASS)) {
            Intent serviceIntent2 = new Intent();
            serviceIntent2.setAction(SERVICE_CLASS);
            context.startService(serviceIntent2);
        }
    }

    private boolean isMyServiceRunning(Context context, String fullName) {
        for (ActivityManager.RunningServiceInfo service : ((ActivityManager) context.getSystemService("activity")).getRunningServices(Integer.MAX_VALUE)) {
            if (fullName.equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }
}
