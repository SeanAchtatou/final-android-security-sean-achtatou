package com.google.analytics.tracking.android;

import android.content.Context;
import com.google.analytics.tracking.android.g;
import com.google.analytics.tracking.android.r;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/* compiled from: GoogleAnalytics */
public class s implements ah {
    private static s j;
    private boolean a;
    private g b;
    private Context c;
    private ag d;
    private a e;
    /* access modifiers changed from: private */
    public volatile String f;
    /* access modifiers changed from: private */
    public volatile Boolean g;
    private final Map<String, ag> h;
    private String i;

    /* compiled from: GoogleAnalytics */
    public interface a {
        void a(boolean z);
    }

    s() {
        this.h = new HashMap();
    }

    private s(Context context) {
        this(context, q.a(context));
    }

    private s(Context context, g gVar) {
        this.h = new HashMap();
        if (context == null) {
            throw new IllegalArgumentException("context cannot be null");
        }
        this.c = context.getApplicationContext();
        this.b = gVar;
        this.e = new a();
        this.b.a(new a() {
            public void a(boolean z) {
                Boolean unused = s.this.g = Boolean.valueOf(z);
            }
        });
        this.b.a(new g.a() {
            public void a(String str) {
                String unused = s.this.f = str;
            }
        });
    }

    public static s a(Context context) {
        s sVar;
        synchronized (s.class) {
            if (j == null) {
                j = new s(context);
            }
            sVar = j;
        }
        return sVar;
    }

    public void a(boolean z) {
        r.a().a(r.a.SET_DEBUG);
        this.a = z;
        w.a(z);
    }

    public ag a(String str) {
        ag agVar;
        synchronized (this) {
            if (str == null) {
                throw new IllegalArgumentException("trackingId cannot be null");
            }
            agVar = this.h.get(str);
            if (agVar == null) {
                agVar = new ag(str, this);
                this.h.put(str, agVar);
                if (this.d == null) {
                    this.d = agVar;
                }
            }
            r.a().a(r.a.GET_TRACKER);
        }
        return agVar;
    }

    public void a(Map<String, String> map) {
        synchronized (this) {
            if (map == null) {
                throw new IllegalArgumentException("hit cannot be null");
            }
            map.put("language", ai.a(Locale.getDefault()));
            map.put("adSenseAdMobHitId", Integer.toString(this.e.a()));
            map.put("screenResolution", this.c.getResources().getDisplayMetrics().widthPixels + "x" + this.c.getResources().getDisplayMetrics().heightPixels);
            map.put("usage", r.a().c());
            r.a().b();
            this.b.a(map);
            this.i = map.get("trackingId");
        }
    }
}
