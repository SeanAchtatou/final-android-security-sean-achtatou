package com.google.analytics.tracking.android;

import android.text.TextUtils;
import com.google.analytics.tracking.android.r;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/* compiled from: Tracker */
public class ag {
    private static final DecimalFormat a = new DecimalFormat("0.######", new DecimalFormatSymbols(Locale.US));
    private final ah b;
    private final a c;
    private volatile boolean d;
    private volatile boolean e;
    private long f;
    private long g;
    private boolean h;

    ag() {
        this.d = false;
        this.e = false;
        this.f = 120000;
        this.h = true;
        this.b = null;
        this.c = null;
    }

    ag(String str, ah ahVar) {
        this.d = false;
        this.e = false;
        this.f = 120000;
        this.h = true;
        if (str == null) {
            throw new IllegalArgumentException("trackingId cannot be null");
        }
        this.b = ahVar;
        this.c = new a();
        this.c.b("trackingId", str);
        this.c.b("sampleRate", "100");
        this.c.a("sessionControl", "start");
        this.c.b("useSecure", Boolean.toString(true));
    }

    private void b() {
        if (this.d) {
            throw new IllegalStateException("Tracker closed");
        }
    }

    public void a(boolean z) {
        b();
        r.a().a(r.a.SET_START_SESSION);
        this.c.a("sessionControl", z ? "start" : null);
    }

    public void a(String str) {
        if (this.e) {
            w.i("Tracking already started, setAppName call ignored");
        } else if (TextUtils.isEmpty(str)) {
            w.i("setting appName to empty value not allowed, call ignored");
        } else {
            r.a().a(r.a.SET_APP_NAME);
            this.c.b("appName", str);
        }
    }

    public void b(String str) {
        if (this.e) {
            w.i("Tracking already started, setAppVersion call ignored");
            return;
        }
        r.a().a(r.a.SET_APP_VERSION);
        this.c.b("appVersion", str);
    }

    public void c(String str) {
        b();
        if (TextUtils.isEmpty(str)) {
            throw new IllegalStateException("trackView requires a appScreen to be set");
        }
        r.a().a(r.a.TRACK_VIEW_WITH_APPSCREEN);
        this.c.b("description", str);
        a("appview", (Map<String, String>) null);
    }

    public void a(String str, boolean z) {
        b();
        r.a().a(r.a.TRACK_EXCEPTION_WITH_DESCRIPTION);
        r.a().a(true);
        a("exception", b(str, z));
        r.a().a(false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.analytics.tracking.android.ag.a.a(java.util.Map<java.lang.String, java.lang.String>, java.lang.Boolean):void
     arg types: [java.util.Map<java.lang.String, java.lang.String>, int]
     candidates:
      com.google.analytics.tracking.android.ag.a.a(java.lang.String, java.lang.String):void
      com.google.analytics.tracking.android.ag.a.a(java.util.Map<java.lang.String, java.lang.String>, java.lang.Boolean):void */
    private void a(String str, Map<String, String> map) {
        this.e = true;
        if (map == null) {
            map = new HashMap<>();
        }
        map.put("hitType", str);
        this.c.a(map, (Boolean) true);
        if (!a()) {
            w.i("Too many hits sent too quickly, throttling invoked.");
        } else {
            this.b.a(this.c.b());
        }
        this.c.a();
    }

    public void b(boolean z) {
        r.a().a(r.a.SET_ANONYMIZE_IP);
        this.c.b("anonymizeIp", Boolean.toString(z));
    }

    public void a(double d2) {
        r.a().a(r.a.SET_SAMPLE_RATE);
        this.c.b("sampleRate", Double.toString(d2));
    }

    public Map<String, String> b(String str, boolean z) {
        HashMap hashMap = new HashMap();
        hashMap.put("exDescription", str);
        hashMap.put("exFatal", Boolean.toString(z));
        r.a().a(r.a.CONSTRUCT_EXCEPTION);
        return hashMap;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(long, long):long}
     arg types: [int, long]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(float, float):float}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(long, long):long} */
    /* access modifiers changed from: package-private */
    public synchronized boolean a() {
        boolean z = true;
        synchronized (this) {
            if (this.h) {
                long currentTimeMillis = System.currentTimeMillis();
                if (this.f < 120000) {
                    long j = currentTimeMillis - this.g;
                    if (j > 0) {
                        this.f = Math.min(120000L, j + this.f);
                    }
                }
                this.g = currentTimeMillis;
                if (this.f >= 2000) {
                    this.f -= 2000;
                } else {
                    w.i("Excessive tracking detected.  Tracking call ignored.");
                    z = false;
                }
            }
        }
        return z;
    }

    /* compiled from: Tracker */
    private static class a {
        private Map<String, String> a;
        private Map<String, String> b;

        private a() {
            this.a = new HashMap();
            this.b = new HashMap();
        }

        public synchronized void a(String str, String str2) {
            this.a.put(str, str2);
        }

        public synchronized void b(String str, String str2) {
            this.b.put(str, str2);
        }

        public synchronized void a() {
            this.a.clear();
        }

        public synchronized void a(Map<String, String> map, Boolean bool) {
            if (bool.booleanValue()) {
                this.a.putAll(map);
            } else {
                this.b.putAll(map);
            }
        }

        public synchronized Map<String, String> b() {
            HashMap hashMap;
            hashMap = new HashMap(this.b);
            hashMap.putAll(this.a);
            return hashMap;
        }
    }
}
