package com.google.analytics.tracking.android;

import com.google.analytics.tracking.android.x;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

/* compiled from: HitBuilder */
class u {
    static Map<String, String> a(x xVar, Map<String, String> map) {
        String a;
        HashMap hashMap = new HashMap();
        for (Map.Entry next : map.entrySet()) {
            x.b a2 = xVar.a((String) next.getKey());
            if (!(a2 == null || (a = a2.a((String) next.getKey())) == null)) {
                String str = (String) next.getValue();
                if (a2.b() != null) {
                    str = a2.b().a(str);
                }
                if (str != null && !str.equals(a2.a())) {
                    hashMap.put(a, str);
                }
            }
        }
        return hashMap;
    }

    static String a(t tVar, long j) {
        StringBuilder sb = new StringBuilder();
        sb.append(tVar.a());
        if (tVar.c() > 0) {
            long c = j - tVar.c();
            if (c >= 0) {
                sb.append("&").append("qt").append("=").append(c);
            }
        }
        sb.append("&").append("z").append("=").append(tVar.b());
        return sb.toString();
    }

    static String a(String str) {
        try {
            return URLEncoder.encode(str, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new AssertionError("URL encoding failed for: " + str);
        }
    }
}
