package com.google.analytics.tracking.android;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import java.lang.Thread;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

/* compiled from: EasyTracker */
public class j {
    private static j a;
    private boolean b = false;
    private String c;
    private String d;
    private String e;
    private int f = 1800;
    private boolean g;
    private Double h;
    private boolean i;
    private boolean j;
    private Thread.UncaughtExceptionHandler k;
    private boolean l = false;
    private int m = 0;
    private long n;
    private long o;
    private Context p;
    private final Map<String, String> q = new HashMap();
    private ag r = null;
    private z s;
    private s t;
    private ac u;
    private h v = new h() {
        public long a() {
            return System.currentTimeMillis();
        }
    };
    private Timer w;
    private TimerTask x;
    /* access modifiers changed from: private */
    public boolean y = false;

    private j() {
    }

    public static j a() {
        if (a == null) {
            a = new j();
        }
        return a;
    }

    /* access modifiers changed from: package-private */
    public boolean b() {
        return this.n == 0 || (this.n > 0 && this.v.a() > this.o + this.n);
    }

    private void c() {
        boolean z = true;
        this.c = this.s.a("ga_trackingId");
        if (TextUtils.isEmpty(this.c)) {
            this.c = this.s.a("ga_api_key");
            if (TextUtils.isEmpty(this.c)) {
                w.c("EasyTracker requested, but missing required ga_trackingId");
                this.r = new a();
                return;
            }
        }
        this.b = true;
        this.d = this.s.a("ga_appName");
        this.e = this.s.a("ga_appVersion");
        this.g = this.s.c("ga_debug");
        this.h = this.s.b("ga_sampleFrequency");
        if (this.h == null) {
            this.h = new Double((double) this.s.a("ga_sampleRate", 100));
        }
        this.f = this.s.a("ga_dispatchPeriod", 1800);
        this.n = (long) (this.s.a("ga_sessionTimeout", 30) * 1000);
        if (!this.s.c("ga_autoActivityTracking") && !this.s.c("ga_auto_activity_tracking")) {
            z = false;
        }
        this.l = z;
        this.i = this.s.c("ga_anonymizeIp");
        this.j = this.s.c("ga_reportUncaughtExceptions");
        this.r = this.t.a(this.c);
        if (!TextUtils.isEmpty(this.d)) {
            w.d("setting appName to " + this.d);
            this.r.a(this.d);
        }
        if (this.e != null) {
            this.r.b(this.e);
        }
        this.r.b(this.i);
        this.r.a(this.h.doubleValue());
        this.t.a(this.g);
        this.u.a(this.f);
        if (this.j) {
            Thread.UncaughtExceptionHandler uncaughtExceptionHandler = this.k;
            if (uncaughtExceptionHandler == null) {
                uncaughtExceptionHandler = new l(this.r, this.u, Thread.getDefaultUncaughtExceptionHandler(), this.p);
            }
            Thread.setDefaultUncaughtExceptionHandler(uncaughtExceptionHandler);
        }
    }

    public void a(Context context) {
        if (context == null) {
            w.c("Context cannot be null");
            return;
        }
        a(context, new aa(context.getApplicationContext()), s.a(context.getApplicationContext()), o.a());
    }

    /* access modifiers changed from: package-private */
    public void a(Context context, z zVar, s sVar, ac acVar) {
        if (context == null) {
            w.c("Context cannot be null");
        }
        if (this.p == null) {
            this.p = context.getApplicationContext();
            this.t = sVar;
            this.u = acVar;
            this.s = zVar;
            c();
        }
    }

    public void a(Activity activity) {
        a((Context) activity);
        if (this.b) {
            d();
            if (!this.y && this.m == 0 && b()) {
                this.r.a(true);
                if (!this.l) {
                }
            }
            this.y = true;
            this.m++;
            if (this.l) {
                this.r.c(c(activity));
            }
        }
    }

    public void b(Activity activity) {
        a((Context) activity);
        if (this.b) {
            this.m--;
            this.m = Math.max(0, this.m);
            this.o = this.v.a();
            if (this.m == 0) {
                d();
                this.x = new b();
                this.w = new Timer("waitForActivityStart");
                this.w.schedule(this.x, 1000);
            }
        }
    }

    private synchronized void d() {
        if (this.w != null) {
            this.w.cancel();
            this.w = null;
        }
    }

    private String c(Activity activity) {
        String canonicalName = activity.getClass().getCanonicalName();
        if (this.q.containsKey(canonicalName)) {
            return this.q.get(canonicalName);
        }
        String a2 = this.s.a(canonicalName);
        if (a2 == null) {
            a2 = canonicalName;
        }
        this.q.put(canonicalName, a2);
        return a2;
    }

    /* compiled from: EasyTracker */
    class a extends ag {
        private double b = 100.0d;
        private boolean c;

        a() {
        }

        public void a(boolean z) {
        }

        public void a(String str) {
        }

        public void b(String str) {
        }

        public void c(String str) {
        }

        public void a(String str, boolean z) {
        }

        public void b(boolean z) {
            this.c = z;
        }

        public void a(double d) {
            this.b = d;
        }

        public Map<String, String> b(String str, boolean z) {
            return new HashMap();
        }
    }

    /* compiled from: EasyTracker */
    private class b extends TimerTask {
        private b() {
        }

        public void run() {
            boolean unused = j.this.y = false;
        }
    }
}
