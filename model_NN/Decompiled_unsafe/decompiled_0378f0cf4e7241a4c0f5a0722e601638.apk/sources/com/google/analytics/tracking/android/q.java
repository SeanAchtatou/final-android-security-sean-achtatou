package com.google.analytics.tracking.android;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.text.TextUtils;
import com.google.analytics.tracking.android.g;
import com.google.analytics.tracking.android.s;
import com.google.android.gms.analytics.internal.Command;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.LinkedBlockingQueue;

/* compiled from: GAThread */
class q extends Thread implements g {
    private static q i;
    private final LinkedBlockingQueue<Runnable> a = new LinkedBlockingQueue<>();
    private volatile boolean b = false;
    private volatile boolean c = false;
    /* access modifiers changed from: private */
    public volatile boolean d;
    /* access modifiers changed from: private */
    public volatile List<Command> e;
    /* access modifiers changed from: private */
    public volatile x f;
    /* access modifiers changed from: private */
    public volatile String g;
    /* access modifiers changed from: private */
    public volatile String h;
    /* access modifiers changed from: private */
    public volatile ad j;
    private final Context k;

    static q a(Context context) {
        if (i == null) {
            i = new q(context);
        }
        return i;
    }

    private q(Context context) {
        super("GAThread");
        if (context != null) {
            this.k = context.getApplicationContext();
        } else {
            this.k = context;
        }
        start();
    }

    private void e() {
        this.j.e();
        this.e = new ArrayList();
        this.e.add(new Command("appendVersion", "_v", "ma1b5"));
        this.e.add(new Command("appendQueueTime", "qt", null));
        this.e.add(new Command("appendCacheBuster", "z", null));
        this.f = new x();
        y.a(this.f);
    }

    public void a(Map<String, String> map) {
        final HashMap hashMap = new HashMap(map);
        final long currentTimeMillis = System.currentTimeMillis();
        hashMap.put("hitTime", Long.toString(currentTimeMillis));
        a(new Runnable() {
            public void run() {
                hashMap.put("clientId", q.this.h);
                if (!q.this.d && !q.this.d(hashMap)) {
                    if (!TextUtils.isEmpty(q.this.g)) {
                        hashMap.put("campaign", q.this.g);
                        String unused = q.this.g = (String) null;
                    }
                    q.this.e(hashMap);
                    q.this.f(hashMap);
                    q.this.c(hashMap);
                    q.this.j.a(u.a(q.this.f, hashMap), currentTimeMillis, q.this.b(hashMap), q.this.e);
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public String b(Map<String, String> map) {
        String str = map.get("internalHitUrl");
        if (str == null) {
            return (!map.containsKey("useSecure") || ai.c(map.get("useSecure"))) ? "https://ssl.google-analytics.com/collect" : "http://www.google-analytics.com/collect";
        }
        return str;
    }

    /* access modifiers changed from: private */
    public void c(Map<String, String> map) {
        String str = map.get("rawException");
        if (str != null) {
            map.remove("rawException");
            try {
                ObjectInputStream objectInputStream = new ObjectInputStream(new ByteArrayInputStream(ai.e(str)));
                Object readObject = objectInputStream.readObject();
                objectInputStream.close();
                if (readObject instanceof Throwable) {
                    Throwable th = (Throwable) readObject;
                    map.put("exDescription", new af(this.k, new ArrayList()).a(map.get("exceptionThreadName"), th));
                }
            } catch (IOException e2) {
                w.h("IOException reading exception");
            } catch (ClassNotFoundException e3) {
                w.h("ClassNotFoundException reading exception");
            }
        }
    }

    /* access modifiers changed from: private */
    public boolean d(Map<String, String> map) {
        String str;
        if (map.get("sampleRate") != null) {
            double b2 = ai.b(map.get("sampleRate"));
            if (b2 <= 0.0d) {
                return true;
            }
            if (b2 < 100.0d && (str = map.get("clientId")) != null && ((double) (Math.abs(str.hashCode()) % 10000)) >= b2 * 100.0d) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: private */
    public void e(Map<String, String> map) {
        String str;
        PackageManager packageManager = this.k.getPackageManager();
        String packageName = this.k.getPackageName();
        String installerPackageName = packageManager.getInstallerPackageName(packageName);
        String str2 = null;
        try {
            PackageInfo packageInfo = packageManager.getPackageInfo(this.k.getPackageName(), 0);
            if (packageInfo != null) {
                str = packageManager.getApplicationLabel(packageInfo.applicationInfo).toString();
                try {
                    str2 = packageInfo.versionName;
                } catch (PackageManager.NameNotFoundException e2) {
                    w.c("Error retrieving package info: appName set to " + str);
                    a(map, "appName", str);
                    a(map, "appVersion", str2);
                    a(map, "appId", packageName);
                    a(map, "appInstallerId", installerPackageName);
                    map.put("apiVersion", "1");
                }
            } else {
                str = packageName;
            }
        } catch (PackageManager.NameNotFoundException e3) {
            str = packageName;
            w.c("Error retrieving package info: appName set to " + str);
            a(map, "appName", str);
            a(map, "appVersion", str2);
            a(map, "appId", packageName);
            a(map, "appInstallerId", installerPackageName);
            map.put("apiVersion", "1");
        }
        a(map, "appName", str);
        a(map, "appVersion", str2);
        a(map, "appId", packageName);
        a(map, "appInstallerId", installerPackageName);
        map.put("apiVersion", "1");
    }

    private void a(Map<String, String> map, String str, String str2) {
        if (!map.containsKey(str)) {
            map.put(str, str2);
        }
    }

    /* access modifiers changed from: private */
    public void f(Map<String, String> map) {
        String d2 = ai.d(map.get("campaign"));
        if (!TextUtils.isEmpty(d2)) {
            Map<String, String> a2 = ai.a(d2);
            map.put("campaignContent", a2.get("utm_content"));
            map.put("campaignMedium", a2.get("utm_medium"));
            map.put("campaignName", a2.get("utm_campaign"));
            map.put("campaignSource", a2.get("utm_source"));
            map.put("campaignKeyword", a2.get("utm_term"));
            map.put("campaignId", a2.get("utm_id"));
            map.put("gclid", a2.get("gclid"));
            map.put("dclid", a2.get("dclid"));
            map.put("gmob_t", a2.get("gmob_t"));
        }
    }

    public void a() {
        a(new Runnable() {
            public void run() {
                q.this.j.c();
            }
        });
    }

    public void a(final s.a aVar) {
        a(new Runnable() {
            public void run() {
                aVar.a(q.this.d);
            }
        });
    }

    public void a(final g.a aVar) {
        a(new Runnable() {
            public void run() {
                aVar.a(q.this.h);
            }
        });
    }

    private void a(Runnable runnable) {
        this.a.add(runnable);
    }

    private boolean f() {
        return this.k.getFileStreamPath("gaOptOut").exists();
    }

    private boolean a(String str) {
        try {
            FileOutputStream openFileOutput = this.k.openFileOutput("gaClientId", 0);
            openFileOutput.write(str.getBytes());
            openFileOutput.close();
            return true;
        } catch (FileNotFoundException e2) {
            w.c("Error creating clientId file.");
            return false;
        } catch (IOException e3) {
            w.c("Error writing to clientId file.");
            return false;
        }
    }

    private String g() {
        String lowerCase = UUID.randomUUID().toString().toLowerCase();
        if (!a(lowerCase)) {
            return "0";
        }
        return lowerCase;
    }

    /* access modifiers changed from: package-private */
    public String d() {
        String str = null;
        try {
            FileInputStream openFileInput = this.k.openFileInput("gaClientId");
            byte[] bArr = new byte[128];
            int read = openFileInput.read(bArr, 0, 128);
            if (openFileInput.available() > 0) {
                w.c("clientId file seems corrupted, deleting it.");
                openFileInput.close();
                this.k.deleteFile("gaInstallData");
            }
            if (read <= 0) {
                w.c("clientId file seems empty, deleting it.");
                openFileInput.close();
                this.k.deleteFile("gaInstallData");
            } else {
                String str2 = new String(bArr, 0, read);
                try {
                    openFileInput.close();
                    str = str2;
                } catch (FileNotFoundException e2) {
                    str = str2;
                } catch (IOException e3) {
                    str = str2;
                    w.c("Error reading clientId file, deleting it.");
                    this.k.deleteFile("gaInstallData");
                } catch (NumberFormatException e4) {
                    str = str2;
                    w.c("cliendId file doesn't have long value, deleting it.");
                    this.k.deleteFile("gaInstallData");
                }
            }
        } catch (FileNotFoundException e5) {
        } catch (IOException e6) {
            w.c("Error reading clientId file, deleting it.");
            this.k.deleteFile("gaInstallData");
        } catch (NumberFormatException e7) {
            w.c("cliendId file doesn't have long value, deleting it.");
            this.k.deleteFile("gaInstallData");
        }
        if (str == null) {
            return g();
        }
        return str;
    }

    static String b(Context context) {
        try {
            FileInputStream openFileInput = context.openFileInput("gaInstallData");
            byte[] bArr = new byte[8192];
            int read = openFileInput.read(bArr, 0, 8192);
            if (openFileInput.available() > 0) {
                w.c("Too much campaign data, ignoring it.");
                openFileInput.close();
                context.deleteFile("gaInstallData");
                return null;
            }
            openFileInput.close();
            context.deleteFile("gaInstallData");
            if (read <= 0) {
                w.h("Campaign file is empty.");
                return null;
            }
            String str = new String(bArr, 0, read);
            w.d("Campaign found: " + str);
            return str;
        } catch (FileNotFoundException e2) {
            w.d("No campaign data found.");
            return null;
        } catch (IOException e3) {
            w.c("Error reading campaign data.");
            context.deleteFile("gaInstallData");
            return null;
        }
    }

    private String a(Throwable th) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        PrintStream printStream = new PrintStream(byteArrayOutputStream);
        th.printStackTrace(printStream);
        printStream.flush();
        return new String(byteArrayOutputStream.toByteArray());
    }

    public void run() {
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e2) {
            w.h("sleep interrupted in GAThread initialize");
        }
        if (this.j == null) {
            this.j = new p(this.k, this);
        }
        e();
        try {
            this.d = f();
            this.h = d();
            this.g = b(this.k);
        } catch (Throwable th) {
            w.c("Error initializing the GAThread: " + a(th));
            w.c("Google Analytics will not start up.");
            this.b = true;
        }
        while (!this.c) {
            try {
                Runnable take = this.a.take();
                if (!this.b) {
                    take.run();
                }
            } catch (InterruptedException e3) {
                w.d(e3.toString());
            } catch (Throwable th2) {
                w.c("Error on GAThread: " + a(th2));
                w.c("Google Analytics is shutting down.");
                this.b = true;
            }
        }
    }

    public LinkedBlockingQueue<Runnable> b() {
        return this.a;
    }

    public Thread c() {
        return this;
    }
}
