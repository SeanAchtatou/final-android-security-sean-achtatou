package com.google.analytics.tracking.android;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.text.TextUtils;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Locale;
import org.apache.http.Header;
import org.apache.http.HttpEntityEnclosingRequest;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.entity.StringEntity;
import org.apache.http.message.BasicHttpEntityEnclosingRequest;

/* compiled from: SimpleNetworkDispatcher */
class ae implements i {
    private final String a;
    private final v b;
    private final Context c;

    ae(e eVar, v vVar, Context context) {
        this(vVar, context);
    }

    ae(v vVar, Context context) {
        this.c = context.getApplicationContext();
        this.a = a("GoogleAnalytics", "2.0", Build.VERSION.RELEASE, ai.a(Locale.getDefault()), Build.MODEL, Build.ID);
        this.b = vVar;
    }

    public boolean a() {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) this.c.getSystemService("connectivity")).getActiveNetworkInfo();
        if (activeNetworkInfo != null && activeNetworkInfo.isConnected()) {
            return true;
        }
        w.g("...no network connectivity");
        return false;
    }

    public int a(List<t> list) {
        int min = Math.min(list.size(), 40);
        int i = 0;
        int i2 = 0;
        while (i < min) {
            HttpClient a2 = this.b.a();
            t tVar = list.get(i);
            URL a3 = a(tVar);
            if (a3 != null) {
                HttpHost httpHost = new HttpHost(a3.getHost(), a3.getPort(), a3.getProtocol());
                String path = a3.getPath();
                String a4 = TextUtils.isEmpty(tVar.a()) ? "" : u.a(tVar, System.currentTimeMillis());
                HttpEntityEnclosingRequest a5 = a(a4, path);
                if (a5 == null) {
                    continue;
                } else {
                    a5.addHeader("Host", httpHost.toHostString());
                    a(w.a(), a5);
                    if (a4.length() > 8192) {
                        w.h("Hit too long (> 8192 bytes)--not sent");
                    } else {
                        try {
                            HttpResponse execute = a2.execute(httpHost, a5);
                            if (execute.getStatusLine().getStatusCode() != 200) {
                                w.h("Bad response: " + execute.getStatusLine().getStatusCode());
                                return i2;
                            }
                        } catch (ClientProtocolException e) {
                            w.h("ClientProtocolException sending hit; discarding hit...");
                        } catch (IOException e2) {
                            w.h("Exception sending hit: " + e2.getClass().getSimpleName());
                            w.h(e2.getMessage());
                            return i2;
                        }
                    }
                }
            } else if (w.a()) {
                w.h("No destination: discarding hit: " + tVar.a());
            } else {
                w.h("No destination: discarding hit.");
            }
            i++;
            i2++;
        }
        return i2;
    }

    private HttpEntityEnclosingRequest a(String str, String str2) {
        BasicHttpEntityEnclosingRequest basicHttpEntityEnclosingRequest;
        if (TextUtils.isEmpty(str)) {
            w.h("Empty hit, discarding.");
            return null;
        }
        String str3 = str2 + "?" + str;
        if (str3.length() < 2036) {
            basicHttpEntityEnclosingRequest = new BasicHttpEntityEnclosingRequest("GET", str3);
        } else {
            basicHttpEntityEnclosingRequest = new BasicHttpEntityEnclosingRequest("POST", str2);
            try {
                basicHttpEntityEnclosingRequest.setEntity(new StringEntity(str));
            } catch (UnsupportedEncodingException e) {
                w.h("Encoding error, discarding hit");
                return null;
            }
        }
        basicHttpEntityEnclosingRequest.addHeader("User-Agent", this.a);
        return basicHttpEntityEnclosingRequest;
    }

    private void a(boolean z, HttpEntityEnclosingRequest httpEntityEnclosingRequest) {
        int available;
        if (z) {
            StringBuffer stringBuffer = new StringBuffer();
            for (Header obj : httpEntityEnclosingRequest.getAllHeaders()) {
                stringBuffer.append(obj.toString()).append("\n");
            }
            stringBuffer.append(httpEntityEnclosingRequest.getRequestLine().toString()).append("\n");
            if (httpEntityEnclosingRequest.getEntity() != null) {
                try {
                    InputStream content = httpEntityEnclosingRequest.getEntity().getContent();
                    if (content != null && (available = content.available()) > 0) {
                        byte[] bArr = new byte[available];
                        content.read(bArr);
                        stringBuffer.append("POST:\n");
                        stringBuffer.append(new String(bArr)).append("\n");
                    }
                } catch (IOException e) {
                    w.h("Error Writing hit to log...");
                }
            }
            w.d(stringBuffer.toString());
        }
    }

    /* access modifiers changed from: package-private */
    public String a(String str, String str2, String str3, String str4, String str5, String str6) {
        return String.format("%s/%s (Linux; U; Android %s; %s; %s Build/%s)", str, str2, str3, str4, str5, str6);
    }

    private URL a(t tVar) {
        if (TextUtils.isEmpty(tVar.d())) {
            return null;
        }
        try {
            return new URL(tVar.d());
        } catch (MalformedURLException e) {
            try {
                return new URL("http://www.google-analytics.com/collect");
            } catch (MalformedURLException e2) {
                return null;
            }
        }
    }
}
