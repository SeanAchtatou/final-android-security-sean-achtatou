package com.google.analytics.tracking.android;

import java.util.HashMap;
import java.util.Map;

/* compiled from: MetaModel */
class x {
    private Map<String, b> a = new HashMap();

    /* compiled from: MetaModel */
    public interface a {
        String a(String str);
    }

    x() {
    }

    /* access modifiers changed from: package-private */
    public b a(String str) {
        if (str.startsWith("&")) {
            return new b(str.substring(1), null, null);
        }
        if (str.contains("*")) {
            str = str.substring(0, str.indexOf("*"));
        }
        return this.a.get(str);
    }

    public void a(String str, String str2, String str3, a aVar) {
        this.a.put(str, new b(str2, str3, aVar));
    }

    /* compiled from: MetaModel */
    public static class b {
        private final String a;
        private final String b;
        private final a c;

        public b(String str, String str2, a aVar) {
            this.a = str;
            this.b = str2;
            this.c = aVar;
        }

        public String a(String str) {
            if (!str.contains("*")) {
                return this.a;
            }
            String str2 = this.a;
            String[] split = str.split("\\*");
            if (split.length <= 1) {
                return null;
            }
            try {
                return str2 + Integer.parseInt(split[1]);
            } catch (NumberFormatException e) {
                w.h("Unable to parse slot for url parameter " + str2);
                return null;
            }
        }

        public String a() {
            return this.b;
        }

        public a b() {
            return this.c;
        }
    }
}
