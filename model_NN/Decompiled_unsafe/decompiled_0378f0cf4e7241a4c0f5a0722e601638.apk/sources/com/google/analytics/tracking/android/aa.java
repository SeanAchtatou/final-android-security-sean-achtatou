package com.google.analytics.tracking.android;

import android.content.Context;
import android.text.TextUtils;

/* compiled from: ParameterLoaderImpl */
class aa implements z {
    private final Context a;

    public aa(Context context) {
        if (context == null) {
            throw new NullPointerException("Context cannot be null");
        }
        this.a = context.getApplicationContext();
    }

    private int a(String str, String str2) {
        if (this.a == null) {
            return 0;
        }
        return this.a.getResources().getIdentifier(str, str2, this.a.getPackageName());
    }

    public String a(String str) {
        int a2 = a(str, "string");
        if (a2 == 0) {
            return null;
        }
        return this.a.getString(a2);
    }

    public boolean c(String str) {
        int a2 = a(str, "bool");
        if (a2 == 0) {
            return false;
        }
        return "true".equalsIgnoreCase(this.a.getString(a2));
    }

    public int a(String str, int i) {
        int a2 = a(str, "integer");
        if (a2 == 0) {
            return i;
        }
        try {
            return Integer.parseInt(this.a.getString(a2));
        } catch (NumberFormatException e) {
            w.h("NumberFormatException parsing " + this.a.getString(a2));
            return i;
        }
    }

    public Double b(String str) {
        String a2 = a(str);
        if (TextUtils.isEmpty(a2)) {
            return null;
        }
        try {
            return Double.valueOf(Double.parseDouble(a2));
        } catch (NumberFormatException e) {
            w.h("NumberFormatException parsing " + a2);
            return null;
        }
    }
}
