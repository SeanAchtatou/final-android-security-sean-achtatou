package com.google.analytics.tracking.android;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.RemoteException;
import com.google.android.gms.analytics.internal.Command;
import com.google.android.gms.analytics.internal.a;
import java.util.List;
import java.util.Map;

/* compiled from: AnalyticsGmsCoreClient */
class d implements c {
    /* access modifiers changed from: private */
    public ServiceConnection a;
    /* access modifiers changed from: private */
    public b b;
    /* access modifiers changed from: private */
    public c c;
    /* access modifiers changed from: private */
    public Context d;
    /* access modifiers changed from: private */
    public com.google.android.gms.analytics.internal.a e;

    /* compiled from: AnalyticsGmsCoreClient */
    public interface b {
        void a();

        void b();
    }

    /* compiled from: AnalyticsGmsCoreClient */
    public interface c {
        void a(int i, Intent intent);
    }

    public d(Context context, b bVar, c cVar) {
        this.d = context;
        if (bVar == null) {
            throw new IllegalArgumentException("onConnectedListener cannot be null");
        }
        this.b = bVar;
        if (cVar == null) {
            throw new IllegalArgumentException("onConnectionFailedListener cannot be null");
        }
        this.c = cVar;
    }

    public void b() {
        Intent intent = new Intent("com.google.android.gms.analytics.service.START");
        intent.putExtra("app_package_name", this.d.getPackageName());
        if (this.a != null) {
            w.c("Calling connect() while still connected, missing disconnect().");
            return;
        }
        this.a = new a();
        boolean bindService = this.d.bindService(intent, this.a, 129);
        w.e("connect: bindService returned " + bindService + " for " + intent);
        if (!bindService) {
            this.a = null;
            this.c.a(1, null);
        }
    }

    public void c() {
        this.e = null;
        if (this.a != null) {
            try {
                this.d.unbindService(this.a);
            } catch (IllegalArgumentException | IllegalStateException e2) {
            }
            this.a = null;
            this.b.b();
        }
    }

    public void a(Map<String, String> map, long j, String str, List<Command> list) {
        try {
            f().a(map, j, str, list);
        } catch (RemoteException e2) {
            w.c("sendHit failed: " + e2);
        }
    }

    public void a() {
        try {
            f().a();
        } catch (RemoteException e2) {
            w.c("clear hits failed: " + e2);
        }
    }

    private com.google.android.gms.analytics.internal.a f() {
        d();
        return this.e;
    }

    /* access modifiers changed from: protected */
    public void d() {
        if (!e()) {
            throw new IllegalStateException("Not connected. Call connect() and wait for onConnected() to be called.");
        }
    }

    public boolean e() {
        return this.e != null;
    }

    /* compiled from: AnalyticsGmsCoreClient */
    final class a implements ServiceConnection {
        a() {
        }

        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            w.b("service connected, binder: " + iBinder);
            try {
                if ("com.google.android.gms.analytics.internal.IAnalyticsService".equals(iBinder.getInterfaceDescriptor())) {
                    w.b("bound to service");
                    com.google.android.gms.analytics.internal.a unused = d.this.e = a.C0018a.a(iBinder);
                    d.this.g();
                    return;
                }
            } catch (RemoteException e) {
            }
            d.this.d.unbindService(this);
            ServiceConnection unused2 = d.this.a = (ServiceConnection) null;
            d.this.c.a(2, null);
        }

        public void onServiceDisconnected(ComponentName componentName) {
            w.b("service disconnected: " + componentName);
            ServiceConnection unused = d.this.a = (ServiceConnection) null;
            d.this.b.b();
        }
    }

    /* access modifiers changed from: private */
    public void g() {
        h();
    }

    private void h() {
        this.b.a();
    }
}
