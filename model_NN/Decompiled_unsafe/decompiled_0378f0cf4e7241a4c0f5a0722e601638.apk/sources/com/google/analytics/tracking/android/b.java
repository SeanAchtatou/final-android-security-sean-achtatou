package com.google.analytics.tracking.android;

import java.util.Random;

/* compiled from: AdMobInfo */
class b {
    private static final b a = new b();
    private int b;
    private Random c = new Random();

    private b() {
    }

    static b a() {
        return a;
    }

    /* access modifiers changed from: package-private */
    public int b() {
        this.b = this.c.nextInt(2147483646) + 1;
        return this.b;
    }
}
