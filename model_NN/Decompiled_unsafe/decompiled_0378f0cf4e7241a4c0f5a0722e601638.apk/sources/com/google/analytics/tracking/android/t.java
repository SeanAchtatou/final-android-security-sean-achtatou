package com.google.analytics.tracking.android;

/* compiled from: Hit */
class t {
    private String a;
    private final long b;
    private final long c;
    private String d;

    /* access modifiers changed from: package-private */
    public String a() {
        return this.a;
    }

    /* access modifiers changed from: package-private */
    public void a(String str) {
        this.a = str;
    }

    /* access modifiers changed from: package-private */
    public long b() {
        return this.b;
    }

    /* access modifiers changed from: package-private */
    public long c() {
        return this.c;
    }

    t(String str, long j, long j2) {
        this.a = str;
        this.b = j;
        this.c = j2;
    }

    /* access modifiers changed from: package-private */
    public String d() {
        return this.d;
    }

    /* access modifiers changed from: package-private */
    public void b(String str) {
        this.d = str;
    }
}
