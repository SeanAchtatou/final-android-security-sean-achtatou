package com.google.analytics.tracking.android;

import com.google.analytics.tracking.android.x;
import java.text.DecimalFormat;

/* compiled from: MetaModelInitializer */
class y {
    private static final x.a a = new x.a() {
        public String a(String str) {
            return ai.c(str) ? "1" : "0";
        }
    };
    private static final x.a b = new x.a() {
        private final DecimalFormat a = new DecimalFormat("0.##");

        public String a(String str) {
            return this.a.format(ai.b(str));
        }
    };

    public static void a(x xVar) {
        xVar.a("apiVersion", "v", null, null);
        xVar.a("libraryVersion", "_v", null, null);
        xVar.a("anonymizeIp", "aip", "0", a);
        xVar.a("trackingId", "tid", null, null);
        xVar.a("hitType", "t", null, null);
        xVar.a("sessionControl", "sc", null, null);
        xVar.a("adSenseAdMobHitId", "a", null, null);
        xVar.a("usage", "_u", null, null);
        xVar.a("title", "dt", null, null);
        xVar.a("referrer", "dr", null, null);
        xVar.a("language", "ul", null, null);
        xVar.a("encoding", "de", null, null);
        xVar.a("page", "dp", null, null);
        xVar.a("screenColors", "sd", null, null);
        xVar.a("screenResolution", "sr", null, null);
        xVar.a("viewportSize", "vp", null, null);
        xVar.a("javaEnabled", "je", "1", a);
        xVar.a("flashVersion", "fl", null, null);
        xVar.a("clientId", "cid", null, null);
        xVar.a("campaignName", "cn", null, null);
        xVar.a("campaignSource", "cs", null, null);
        xVar.a("campaignMedium", "cm", null, null);
        xVar.a("campaignKeyword", "ck", null, null);
        xVar.a("campaignContent", "cc", null, null);
        xVar.a("campaignId", "ci", null, null);
        xVar.a("gclid", "gclid", null, null);
        xVar.a("dclid", "dclid", null, null);
        xVar.a("gmob_t", "gmob_t", null, null);
        xVar.a("eventCategory", "ec", null, null);
        xVar.a("eventAction", "ea", null, null);
        xVar.a("eventLabel", "el", null, null);
        xVar.a("eventValue", "ev", null, null);
        xVar.a("nonInteraction", "ni", "0", a);
        xVar.a("socialNetwork", "sn", null, null);
        xVar.a("socialAction", "sa", null, null);
        xVar.a("socialTarget", "st", null, null);
        xVar.a("appName", "an", null, null);
        xVar.a("appVersion", "av", null, null);
        xVar.a("description", "cd", null, null);
        xVar.a("appId", "aid", null, null);
        xVar.a("appInstallerId", "aiid", null, null);
        xVar.a("transactionId", "ti", null, null);
        xVar.a("transactionAffiliation", "ta", null, null);
        xVar.a("transactionShipping", "ts", null, null);
        xVar.a("transactionTotal", "tr", null, null);
        xVar.a("transactionTax", "tt", null, null);
        xVar.a("currencyCode", "cu", null, null);
        xVar.a("itemPrice", "ip", null, null);
        xVar.a("itemCode", "ic", null, null);
        xVar.a("itemName", "in", null, null);
        xVar.a("itemCategory", "iv", null, null);
        xVar.a("itemQuantity", "iq", null, null);
        xVar.a("exDescription", "exd", null, null);
        xVar.a("exFatal", "exf", "1", a);
        xVar.a("timingVar", "utv", null, null);
        xVar.a("timingValue", "utt", null, null);
        xVar.a("timingCategory", "utc", null, null);
        xVar.a("timingLabel", "utl", null, null);
        xVar.a("sampleRate", "sf", "100", b);
        xVar.a("hitTime", "ht", null, null);
        xVar.a("customDimension", "cd", null, null);
        xVar.a("customMetric", "cm", null, null);
        xVar.a("contentGrouping", "cg", null, null);
    }
}
