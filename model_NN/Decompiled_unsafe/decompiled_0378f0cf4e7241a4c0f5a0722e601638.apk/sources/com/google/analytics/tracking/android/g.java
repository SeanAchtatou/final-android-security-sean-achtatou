package com.google.analytics.tracking.android;

import com.google.analytics.tracking.android.s;
import java.util.Map;
import java.util.concurrent.LinkedBlockingQueue;

/* compiled from: AnalyticsThread */
interface g {

    /* compiled from: AnalyticsThread */
    public interface a {
        void a(String str);
    }

    void a();

    void a(a aVar);

    void a(s.a aVar);

    void a(Map<String, String> map);

    LinkedBlockingQueue<Runnable> b();

    Thread c();
}
