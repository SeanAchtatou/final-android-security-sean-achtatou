package com.google.analytics.tracking.android;

import android.util.Log;

/* compiled from: Log */
public class w {
    private static boolean a;

    public static void a(boolean z) {
        a = z;
    }

    public static boolean a() {
        return a;
    }

    public static int a(String str) {
        return Log.d("GAV2", j(str));
    }

    public static int b(String str) {
        if (a) {
            return a(str);
        }
        return 0;
    }

    public static int c(String str) {
        return Log.e("GAV2", j(str));
    }

    public static int d(String str) {
        return Log.i("GAV2", j(str));
    }

    public static int e(String str) {
        if (a) {
            return d(str);
        }
        return 0;
    }

    public static int f(String str) {
        return Log.v("GAV2", j(str));
    }

    public static int g(String str) {
        if (a) {
            return f(str);
        }
        return 0;
    }

    public static int h(String str) {
        return Log.w("GAV2", j(str));
    }

    public static int i(String str) {
        if (a) {
            return h(str);
        }
        return 0;
    }

    private static String j(String str) {
        return Thread.currentThread().toString() + ": " + str;
    }
}
