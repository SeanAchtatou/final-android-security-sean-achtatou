package com.google.analytics.tracking.android;

import android.content.Context;
import java.lang.Thread;
import java.util.ArrayList;

/* compiled from: ExceptionReporter */
public class l implements Thread.UncaughtExceptionHandler {
    private final Thread.UncaughtExceptionHandler a;
    private final ag b;
    private final ac c;
    private k d;

    public l(ag agVar, ac acVar, Thread.UncaughtExceptionHandler uncaughtExceptionHandler, Context context) {
        if (agVar == null) {
            throw new NullPointerException("tracker cannot be null");
        } else if (acVar == null) {
            throw new NullPointerException("serviceManager cannot be null");
        } else {
            this.a = uncaughtExceptionHandler;
            this.b = agVar;
            this.c = acVar;
            this.d = new af(context, new ArrayList());
            w.e("ExceptionReporter created, original handler is " + (uncaughtExceptionHandler == null ? "null" : uncaughtExceptionHandler.getClass().getName()));
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.analytics.tracking.android.ag.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.google.analytics.tracking.android.ag.a(java.lang.String, java.util.Map<java.lang.String, java.lang.String>):void
      com.google.analytics.tracking.android.ag.a(java.lang.String, boolean):void */
    public void uncaughtException(Thread thread, Throwable th) {
        String str = "UncaughtException";
        if (this.d != null) {
            str = this.d.a(thread != null ? thread.getName() : null, th);
        }
        w.e("Tracking Exception: " + str);
        this.b.a(str, true);
        this.c.c();
        if (this.a != null) {
            w.e("Passing exception to original handler.");
            this.a.uncaughtException(thread, th);
        }
    }
}
