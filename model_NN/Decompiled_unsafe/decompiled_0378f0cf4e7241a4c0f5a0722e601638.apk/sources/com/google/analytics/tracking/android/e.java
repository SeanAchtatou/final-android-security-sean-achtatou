package com.google.analytics.tracking.android;

import com.google.android.gms.analytics.internal.Command;
import java.util.Collection;
import java.util.Map;

/* compiled from: AnalyticsStore */
interface e {
    void a();

    void a(long j);

    void a(Map<String, String> map, long j, String str, Collection<Command> collection);
}
