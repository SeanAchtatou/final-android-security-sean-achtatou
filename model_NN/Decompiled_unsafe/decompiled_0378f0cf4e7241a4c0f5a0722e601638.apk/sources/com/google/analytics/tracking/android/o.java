package com.google.analytics.tracking.android;

import android.content.Context;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.Message;
import com.google.analytics.tracking.android.r;

/* compiled from: GAServiceManager */
public class o implements ac {
    /* access modifiers changed from: private */
    public static final Object a = new Object();
    private static o m;
    private Context b;
    private e c;
    private volatile g d;
    /* access modifiers changed from: private */
    public int e = 1800;
    private boolean f = true;
    /* access modifiers changed from: private */
    public boolean g = true;
    private boolean h = true;
    private f i = new f() {
        public void a(boolean z) {
            o.this.a(z, o.this.g);
        }
    };
    /* access modifiers changed from: private */
    public Handler j;
    private n k;
    /* access modifiers changed from: private */
    public boolean l = false;

    public static o a() {
        if (m == null) {
            m = new o();
        }
        return m;
    }

    private o() {
    }

    private void e() {
        this.k = new n(this);
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
        this.b.registerReceiver(this.k, intentFilter);
    }

    private void f() {
        this.j = new Handler(this.b.getMainLooper(), new Handler.Callback() {
            public boolean handleMessage(Message message) {
                if (1 == message.what && o.a.equals(message.obj)) {
                    r.a().a(true);
                    o.this.c();
                    r.a().a(false);
                    if (o.this.e > 0 && !o.this.l) {
                        o.this.j.sendMessageDelayed(o.this.j.obtainMessage(1, o.a), (long) (o.this.e * 1000));
                    }
                }
                return true;
            }
        });
        if (this.e > 0) {
            this.j.sendMessageDelayed(this.j.obtainMessage(1, a), (long) (this.e * 1000));
        }
    }

    /* access modifiers changed from: package-private */
    public synchronized void a(Context context, g gVar) {
        if (this.b == null) {
            this.b = context.getApplicationContext();
            if (this.d == null) {
                this.d = gVar;
                if (this.f) {
                    gVar.a();
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public synchronized e b() {
        if (this.c == null) {
            if (this.b == null) {
                throw new IllegalStateException("Cant get a store unless we have a context");
            }
            this.c = new ab(this.i, this.b);
        }
        if (this.j == null) {
            f();
        }
        if (this.k == null && this.h) {
            e();
        }
        return this.c;
    }

    public synchronized void c() {
        if (this.d == null) {
            w.h("dispatch call queued.  Need to call GAServiceManager.getInstance().initialize().");
            this.f = true;
        } else {
            r.a().a(r.a.DISPATCH);
            this.d.a();
        }
    }

    public synchronized void a(int i2) {
        if (this.j == null) {
            w.h("Need to call initialize() and be in fallback mode to start dispatch.");
            this.e = i2;
        } else {
            r.a().a(r.a.SET_DISPATCH_PERIOD);
            if (!this.l && this.g && this.e > 0) {
                this.j.removeMessages(1, a);
            }
            this.e = i2;
            if (i2 > 0 && !this.l && this.g) {
                this.j.sendMessageDelayed(this.j.obtainMessage(1, a), (long) (i2 * 1000));
            }
        }
    }

    /* access modifiers changed from: package-private */
    public synchronized void a(boolean z, boolean z2) {
        if (!(this.l == z && this.g == z2)) {
            if (z || !z2) {
                if (this.e > 0) {
                    this.j.removeMessages(1, a);
                }
            }
            if (!z && z2 && this.e > 0) {
                this.j.sendMessageDelayed(this.j.obtainMessage(1, a), (long) (this.e * 1000));
            }
            w.e("PowerSaveMode " + ((z || !z2) ? "initiated." : "terminated."));
            this.l = z;
            this.g = z2;
        }
    }

    public synchronized void a(boolean z) {
        a(this.l, z);
    }
}
