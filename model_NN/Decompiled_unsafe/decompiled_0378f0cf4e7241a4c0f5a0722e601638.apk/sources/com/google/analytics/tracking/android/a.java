package com.google.analytics.tracking.android;

/* compiled from: AdHitIdGenerator */
class a {
    private boolean a;

    a() {
        boolean z;
        try {
            if (Class.forName("com.google.ads.AdRequest") != null) {
                z = true;
            } else {
                z = false;
            }
            this.a = z;
        } catch (ClassNotFoundException e) {
            this.a = false;
        }
    }

    /* access modifiers changed from: package-private */
    public int a() {
        if (!this.a) {
            return 0;
        }
        return b.a().b();
    }
}
