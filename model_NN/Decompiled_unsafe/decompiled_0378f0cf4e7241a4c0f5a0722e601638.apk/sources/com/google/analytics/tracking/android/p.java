package com.google.analytics.tracking.android;

import android.content.Context;
import android.content.Intent;
import com.google.analytics.tracking.android.d;
import com.google.android.gms.analytics.internal.Command;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentLinkedQueue;

/* compiled from: GAServiceProxy */
class p implements ad, d.b, d.c {
    /* access modifiers changed from: private */
    public volatile long a;
    /* access modifiers changed from: private */
    public volatile a b;
    private volatile c c;
    private e d;
    private e e;
    private final g f;
    private final Context g;
    /* access modifiers changed from: private */
    public final Queue<d> h;
    private volatile int i;
    private volatile Timer j;
    private volatile Timer k;
    /* access modifiers changed from: private */
    public volatile Timer l;
    private boolean m;
    private boolean n;
    /* access modifiers changed from: private */
    public h o;
    /* access modifiers changed from: private */
    public long p;

    /* compiled from: GAServiceProxy */
    private enum a {
        CONNECTING,
        CONNECTED_SERVICE,
        CONNECTED_LOCAL,
        BLOCKED,
        PENDING_CONNECTION,
        PENDING_DISCONNECT,
        DISCONNECTED
    }

    p(Context context, g gVar, e eVar) {
        this.h = new ConcurrentLinkedQueue();
        this.p = 300000;
        this.e = eVar;
        this.g = context;
        this.f = gVar;
        this.o = new h() {
            public long a() {
                return System.currentTimeMillis();
            }
        };
        this.i = 0;
        this.b = a.DISCONNECTED;
    }

    p(Context context, g gVar) {
        this(context, gVar, null);
    }

    public void a(Map<String, String> map, long j2, String str, List<Command> list) {
        w.e("putHit called");
        this.h.add(new d(map, j2, str, list));
        g();
    }

    public void c() {
        switch (this.b) {
            case CONNECTED_LOCAL:
                h();
                return;
            case CONNECTED_SERVICE:
                return;
            default:
                this.m = true;
                return;
        }
    }

    public void d() {
        w.e("clearHits called");
        this.h.clear();
        switch (this.b) {
            case CONNECTED_LOCAL:
                this.d.a(0);
                this.n = false;
                return;
            case CONNECTED_SERVICE:
                this.c.a();
                this.n = false;
                return;
            default:
                this.n = true;
                return;
        }
    }

    private Timer a(Timer timer) {
        if (timer == null) {
            return null;
        }
        timer.cancel();
        return null;
    }

    private void f() {
        this.j = a(this.j);
        this.k = a(this.k);
        this.l = a(this.l);
    }

    public void e() {
        if (this.c == null) {
            this.c = new d(this.g, this, this);
            j();
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* access modifiers changed from: private */
    public synchronized void g() {
        if (Thread.currentThread().equals(this.f.c())) {
            if (this.n) {
                d();
            }
            switch (this.b) {
                case CONNECTED_LOCAL:
                    while (!this.h.isEmpty()) {
                        d poll = this.h.poll();
                        w.e("Sending hit to store");
                        this.d.a(poll.a(), poll.b(), poll.c(), poll.d());
                    }
                    if (this.m) {
                        h();
                        break;
                    }
                    break;
                case CONNECTED_SERVICE:
                    while (!this.h.isEmpty()) {
                        d peek = this.h.peek();
                        w.e("Sending hit to service");
                        this.c.a(peek.a(), peek.b(), peek.c(), peek.d());
                        this.h.poll();
                    }
                    this.a = this.o.a();
                    break;
                case DISCONNECTED:
                    w.e("Need to reconnect");
                    if (!this.h.isEmpty()) {
                        j();
                        break;
                    }
                    break;
            }
        } else {
            this.f.b().add(new Runnable() {
                public void run() {
                    p.this.g();
                }
            });
        }
    }

    private void h() {
        this.d.a();
        this.m = false;
    }

    /* access modifiers changed from: private */
    public synchronized void i() {
        if (this.b != a.CONNECTED_LOCAL) {
            f();
            w.e("falling back to local store");
            if (this.e != null) {
                this.d = this.e;
            } else {
                o a2 = o.a();
                a2.a(this.g, this.f);
                this.d = a2.b();
            }
            this.b = a.CONNECTED_LOCAL;
            g();
        }
    }

    /* access modifiers changed from: private */
    public synchronized void j() {
        if (this.c == null || this.b == a.CONNECTED_LOCAL) {
            w.h("client not initialized.");
            i();
        } else {
            try {
                this.i++;
                a(this.k);
                this.b = a.CONNECTING;
                this.k = new Timer("Failed Connect");
                this.k.schedule(new c(), 3000);
                w.e("connecting to Analytics service");
                this.c.b();
            } catch (SecurityException e2) {
                w.h("security exception on connectToService");
                i();
            }
        }
        return;
    }

    /* access modifiers changed from: private */
    public synchronized void k() {
        if (this.c != null && this.b == a.CONNECTED_SERVICE) {
            this.b = a.PENDING_DISCONNECT;
            this.c.c();
        }
    }

    public synchronized void a() {
        this.k = a(this.k);
        this.i = 0;
        w.e("Connected to service");
        this.b = a.CONNECTED_SERVICE;
        g();
        this.l = a(this.l);
        this.l = new Timer("disconnect check");
        this.l.schedule(new b(), this.p);
    }

    public synchronized void b() {
        if (this.b == a.PENDING_DISCONNECT) {
            w.e("Disconnected from service");
            f();
            this.b = a.DISCONNECTED;
        } else {
            w.e("Unexpected disconnect.");
            this.b = a.PENDING_CONNECTION;
            if (this.i < 2) {
                l();
            } else {
                i();
            }
        }
    }

    public synchronized void a(int i2, Intent intent) {
        this.b = a.PENDING_CONNECTION;
        if (this.i < 2) {
            w.h("Service unavailable (code=" + i2 + "), will retry.");
            l();
        } else {
            w.h("Service unavailable (code=" + i2 + "), using local store.");
            i();
        }
    }

    private void l() {
        this.j = a(this.j);
        this.j = new Timer("Service Reconnect");
        this.j.schedule(new e(), 5000);
    }

    /* compiled from: GAServiceProxy */
    private class c extends TimerTask {
        private c() {
        }

        public void run() {
            if (p.this.b == a.CONNECTING) {
                p.this.i();
            }
        }
    }

    /* compiled from: GAServiceProxy */
    private class e extends TimerTask {
        private e() {
        }

        public void run() {
            p.this.j();
        }
    }

    /* compiled from: GAServiceProxy */
    private class b extends TimerTask {
        private b() {
        }

        public void run() {
            if (p.this.b != a.CONNECTED_SERVICE || !p.this.h.isEmpty() || p.this.a + p.this.p >= p.this.o.a()) {
                p.this.l.schedule(new b(), p.this.p);
                return;
            }
            w.e("Disconnecting due to inactivity");
            p.this.k();
        }
    }

    /* compiled from: GAServiceProxy */
    private static class d {
        private final Map<String, String> a;
        private final long b;
        private final String c;
        private final List<Command> d;

        public d(Map<String, String> map, long j, String str, List<Command> list) {
            this.a = map;
            this.b = j;
            this.c = str;
            this.d = list;
        }

        public Map<String, String> a() {
            return this.a;
        }

        public long b() {
            return this.b;
        }

        public String c() {
            return this.c;
        }

        public List<Command> d() {
            return this.d;
        }
    }
}
