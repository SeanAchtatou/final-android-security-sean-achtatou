package com.google.mygson.internal;

public interface ObjectConstructor<T> {
    T construct();
}
