package com.google.android.gms.location;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.internal.b;
import com.google.android.gms.internal.c;

public class b implements Parcelable.Creator<DetectedActivity> {
    static void a(DetectedActivity detectedActivity, Parcel parcel, int i) {
        int a = c.a(parcel);
        c.a(parcel, 1, detectedActivity.c);
        c.a(parcel, 1000, detectedActivity.b);
        c.a(parcel, 2, detectedActivity.d);
        c.a(parcel, a);
    }

    /* renamed from: a */
    public DetectedActivity createFromParcel(Parcel parcel) {
        DetectedActivity detectedActivity = new DetectedActivity();
        int b = com.google.android.gms.internal.b.b(parcel);
        while (parcel.dataPosition() < b) {
            int a = com.google.android.gms.internal.b.a(parcel);
            switch (com.google.android.gms.internal.b.a(a)) {
                case 1:
                    detectedActivity.c = com.google.android.gms.internal.b.f(parcel, a);
                    break;
                case 2:
                    detectedActivity.d = com.google.android.gms.internal.b.f(parcel, a);
                    break;
                case 1000:
                    detectedActivity.b = com.google.android.gms.internal.b.f(parcel, a);
                    break;
                default:
                    com.google.android.gms.internal.b.b(parcel, a);
                    break;
            }
        }
        if (parcel.dataPosition() == b) {
            return detectedActivity;
        }
        throw new b.a("Overread allowed size end=" + b, parcel);
    }

    /* renamed from: a */
    public DetectedActivity[] newArray(int i) {
        return new DetectedActivity[i];
    }
}
