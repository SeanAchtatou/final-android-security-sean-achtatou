package com.google.android.gms.maps.model;

import android.os.IBinder;
import android.os.Parcel;
import com.google.android.gms.internal.ae;
import com.google.android.gms.internal.aj;
import com.google.android.gms.internal.am;
import com.google.android.gms.internal.w;

public final class TileOverlayOptions implements ae {
    public static final k a = new k();
    private final int b;
    /* access modifiers changed from: private */
    public am c;
    private l d;
    private boolean e;
    private float f;

    public TileOverlayOptions() {
        this.e = true;
        this.b = 1;
    }

    TileOverlayOptions(int i, IBinder iBinder, boolean z, float f2) {
        this.e = true;
        this.b = i;
        this.c = am.a.a(iBinder);
        this.d = this.c == null ? null : new l() {
            private final am c = TileOverlayOptions.this.c;
        };
        this.e = z;
        this.f = f2;
    }

    public int a() {
        return this.b;
    }

    public IBinder b() {
        return this.c.asBinder();
    }

    public float c() {
        return this.f;
    }

    public boolean d() {
        return this.e;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        if (w.a()) {
            aj.a(this, parcel, i);
        } else {
            k.a(this, parcel, i);
        }
    }
}
