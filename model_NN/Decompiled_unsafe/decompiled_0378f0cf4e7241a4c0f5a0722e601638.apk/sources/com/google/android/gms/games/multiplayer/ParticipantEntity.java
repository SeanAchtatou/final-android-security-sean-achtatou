package com.google.android.gms.games.multiplayer;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.games.Player;
import com.google.android.gms.games.PlayerEntity;
import com.google.android.gms.internal.bu;

public final class ParticipantEntity implements Parcelable, Participant {
    public static final Parcelable.Creator<ParticipantEntity> CREATOR = new Parcelable.Creator<ParticipantEntity>() {
        /* renamed from: a */
        public ParticipantEntity createFromParcel(Parcel parcel) {
            boolean z = true;
            String readString = parcel.readString();
            String readString2 = parcel.readString();
            String readString3 = parcel.readString();
            Uri parse = readString3 == null ? null : Uri.parse(readString3);
            String readString4 = parcel.readString();
            Uri parse2 = readString4 == null ? null : Uri.parse(readString4);
            int readInt = parcel.readInt();
            String readString5 = parcel.readString();
            boolean z2 = parcel.readInt() > 0;
            if (parcel.readInt() <= 0) {
                z = false;
            }
            return new ParticipantEntity(readString, readString2, parse, parse2, readInt, readString5, z2, z ? PlayerEntity.CREATOR.createFromParcel(parcel) : null);
        }

        /* renamed from: a */
        public ParticipantEntity[] newArray(int i) {
            return new ParticipantEntity[i];
        }
    };
    private final PlayerEntity a;
    private final String b;
    private final String c;
    private final Uri d;
    private final Uri e;
    private final int f;
    private final String g;
    private final boolean h;

    public ParticipantEntity(Participant participant) {
        Player i = participant.i();
        this.a = i == null ? null : new PlayerEntity(i);
        this.b = participant.h();
        this.c = participant.e();
        this.d = participant.f();
        this.e = participant.g();
        this.f = participant.b();
        this.g = participant.c();
        this.h = participant.d();
    }

    private ParticipantEntity(String str, String str2, Uri uri, Uri uri2, int i, String str3, boolean z, PlayerEntity playerEntity) {
        this.b = str;
        this.c = str2;
        this.d = uri;
        this.e = uri2;
        this.f = i;
        this.g = str3;
        this.h = z;
        this.a = playerEntity;
    }

    public static int a(Participant participant) {
        return bu.a(participant.i(), Integer.valueOf(participant.b()), participant.c(), Boolean.valueOf(participant.d()), participant.e(), participant.f(), participant.g());
    }

    public static boolean a(Participant participant, Object obj) {
        if (!(obj instanceof Participant)) {
            return false;
        }
        if (participant == obj) {
            return true;
        }
        Participant participant2 = (Participant) obj;
        return bu.a(participant2.i(), participant.i()) && bu.a(Integer.valueOf(participant2.b()), Integer.valueOf(participant.b())) && bu.a(participant2.c(), participant.c()) && bu.a(Boolean.valueOf(participant2.d()), Boolean.valueOf(participant.d())) && bu.a(participant2.e(), participant.e()) && bu.a(participant2.f(), participant.f()) && bu.a(participant2.g(), participant.g());
    }

    public static String b(Participant participant) {
        return bu.a(participant).a("Player", participant.i()).a("Status", Integer.valueOf(participant.b())).a("ClientAddress", participant.c()).a("ConnectedToRoom", Boolean.valueOf(participant.d())).a("DisplayName", participant.e()).a("IconImage", participant.f()).a("HiResImage", participant.g()).toString();
    }

    public int b() {
        return this.f;
    }

    public String c() {
        return this.g;
    }

    public boolean d() {
        return this.h;
    }

    public int describeContents() {
        return 0;
    }

    public String e() {
        return this.a == null ? this.c : this.a.c();
    }

    public boolean equals(Object obj) {
        return a(this, obj);
    }

    public Uri f() {
        return this.a == null ? this.d : this.a.d();
    }

    public Uri g() {
        return this.a == null ? this.e : this.a.e();
    }

    public String h() {
        return this.b;
    }

    public int hashCode() {
        return a(this);
    }

    public Player i() {
        return this.a;
    }

    /* renamed from: j */
    public Participant a() {
        return this;
    }

    public String toString() {
        return b(this);
    }

    public void writeToParcel(Parcel parcel, int i) {
        String str = null;
        int i2 = 0;
        parcel.writeString(this.b);
        parcel.writeString(this.c);
        parcel.writeString(this.d == null ? null : this.d.toString());
        if (this.e != null) {
            str = this.e.toString();
        }
        parcel.writeString(str);
        parcel.writeInt(this.f);
        parcel.writeString(this.g);
        parcel.writeInt(this.h ? 1 : 0);
        if (this.a != null) {
            i2 = 1;
        }
        parcel.writeInt(i2);
        if (this.a != null) {
            this.a.writeToParcel(parcel, i);
        }
    }
}
