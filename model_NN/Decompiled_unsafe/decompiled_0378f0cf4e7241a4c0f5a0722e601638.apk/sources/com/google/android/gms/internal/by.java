package com.google.android.gms.internal;

import android.net.Uri;

public final class by {
    private static final Uri a = new Uri.Builder().scheme("android.resource").authority("com.google.android.gms").appendPath("drawable").build();

    public static Uri a(String str) {
        bv.a(str, "Resource name must not be null.");
        return a.buildUpon().appendPath(str).build();
    }
}
