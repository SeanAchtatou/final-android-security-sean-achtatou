package com.google.android.gms.internal;

import android.os.Parcel;
import com.google.android.gms.games.Game;
import com.google.android.gms.games.multiplayer.Invitation;
import com.google.android.gms.games.multiplayer.InvitationEntity;
import com.google.android.gms.games.multiplayer.Participant;
import java.util.ArrayList;

public final class bw extends bn implements Invitation {
    private final Game c;
    private final bx d;
    private final ArrayList<Participant> e;

    public Game b() {
        return this.c;
    }

    public String c() {
        return d("external_invitation_id");
    }

    public Participant d() {
        return this.d;
    }

    public int describeContents() {
        return 0;
    }

    public long e() {
        return a("creation_timestamp");
    }

    public boolean equals(Object obj) {
        return InvitationEntity.a(this, obj);
    }

    public int f() {
        return b("type");
    }

    public ArrayList<Participant> g() {
        return this.e;
    }

    /* renamed from: h */
    public Invitation a() {
        return new InvitationEntity(this);
    }

    public int hashCode() {
        return InvitationEntity.a(this);
    }

    public String toString() {
        return InvitationEntity.b(this);
    }

    public void writeToParcel(Parcel parcel, int i) {
        ((InvitationEntity) a()).writeToParcel(parcel, i);
    }
}
