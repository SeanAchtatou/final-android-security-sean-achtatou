package com.google.android.gms.maps.model;

import android.os.Parcel;
import com.google.android.gms.internal.aa;
import com.google.android.gms.internal.ae;
import com.google.android.gms.internal.bu;
import com.google.android.gms.internal.bv;
import com.google.android.gms.internal.w;

public final class LatLngBounds implements ae {
    public static final e a = new e();
    public final LatLng b;
    public final LatLng c;
    private final int d;

    LatLngBounds(int i, LatLng latLng, LatLng latLng2) {
        bv.a(latLng, "null southwest");
        bv.a(latLng2, "null northeast");
        bv.a(latLng2.b >= latLng.b, "southern latitude exceeds northern latitude (%s > %s)", Double.valueOf(latLng.b), Double.valueOf(latLng2.b));
        this.d = i;
        this.b = latLng;
        this.c = latLng2;
    }

    public int a() {
        return this.d;
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof LatLngBounds)) {
            return false;
        }
        LatLngBounds latLngBounds = (LatLngBounds) obj;
        return this.b.equals(latLngBounds.b) && this.c.equals(latLngBounds.c);
    }

    public int hashCode() {
        return bu.a(this.b, this.c);
    }

    public String toString() {
        return bu.a(this).a("southwest", this.b).a("northeast", this.c).toString();
    }

    public void writeToParcel(Parcel parcel, int i) {
        if (w.a()) {
            aa.a(this, parcel, i);
        } else {
            e.a(this, parcel, i);
        }
    }
}
