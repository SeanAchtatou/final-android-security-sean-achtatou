package com.google.android.gms.maps.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.a.e;
import com.google.android.gms.internal.b;
import com.google.android.gms.internal.c;

public class m implements Parcelable.Creator<VisibleRegion> {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.c.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.maps.model.LatLng, int, int]
     candidates:
      com.google.android.gms.internal.c.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.internal.c.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.c.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.maps.model.LatLngBounds, int, int]
     candidates:
      com.google.android.gms.internal.c.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.internal.c.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    static void a(VisibleRegion visibleRegion, Parcel parcel, int i) {
        int a = c.a(parcel);
        c.a(parcel, 1, visibleRegion.a());
        c.a(parcel, 2, (Parcelable) visibleRegion.b, i, false);
        c.a(parcel, 3, (Parcelable) visibleRegion.c, i, false);
        c.a(parcel, 4, (Parcelable) visibleRegion.d, i, false);
        c.a(parcel, 5, (Parcelable) visibleRegion.e, i, false);
        c.a(parcel, 6, (Parcelable) visibleRegion.f, i, false);
        c.a(parcel, a);
    }

    /* renamed from: a */
    public VisibleRegion createFromParcel(Parcel parcel) {
        LatLngBounds latLngBounds = null;
        int b = b.b(parcel);
        int i = 0;
        LatLng latLng = null;
        LatLng latLng2 = null;
        LatLng latLng3 = null;
        LatLng latLng4 = null;
        while (parcel.dataPosition() < b) {
            int a = b.a(parcel);
            switch (b.a(a)) {
                case 1:
                    i = b.f(parcel, a);
                    break;
                case 2:
                    latLng4 = (LatLng) b.a(parcel, a, LatLng.a);
                    break;
                case 3:
                    latLng3 = (LatLng) b.a(parcel, a, LatLng.a);
                    break;
                case e.g.com_facebook_picker_fragment_done_button_text:
                    latLng2 = (LatLng) b.a(parcel, a, LatLng.a);
                    break;
                case e.g.com_facebook_picker_fragment_title_bar_background:
                    latLng = (LatLng) b.a(parcel, a, LatLng.a);
                    break;
                case e.g.com_facebook_picker_fragment_done_button_background:
                    latLngBounds = (LatLngBounds) b.a(parcel, a, LatLngBounds.a);
                    break;
                default:
                    b.b(parcel, a);
                    break;
            }
        }
        if (parcel.dataPosition() == b) {
            return new VisibleRegion(i, latLng4, latLng3, latLng2, latLng, latLngBounds);
        }
        throw new b.a("Overread allowed size end=" + b, parcel);
    }

    /* renamed from: a */
    public VisibleRegion[] newArray(int i) {
        return new VisibleRegion[i];
    }
}
