package com.google.android.gms.internal;

import android.database.CursorIndexOutOfBoundsException;
import android.database.CursorWindow;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcel;
import java.util.ArrayList;
import java.util.HashMap;

public final class k implements ae {
    public static final bo a = new bo();
    private static final a k = new a(new String[0], null) {
    };
    int b;
    String[] c;
    Bundle d;
    CursorWindow[] e;
    int f;
    Bundle g;
    int[] h;
    int i;
    boolean j = false;

    public static class a {
        private final String[] a;
        private final ArrayList<HashMap<String, Object>> b;
        private final String c;
        private final HashMap<Object, Integer> d;
        private boolean e;
        private String f;

        private a(String[] strArr, String str) {
            this.a = (String[]) bv.a(strArr);
            this.b = new ArrayList<>();
            this.c = str;
            this.d = new HashMap<>();
            this.e = false;
            this.f = null;
        }
    }

    k() {
    }

    private void a(String str, int i2) {
        if (this.d == null || !this.d.containsKey(str)) {
            throw new IllegalArgumentException("No such column: " + str);
        } else if (c()) {
            throw new IllegalArgumentException("Buffer is closed.");
        } else if (i2 < 0 || i2 >= this.i) {
            throw new CursorIndexOutOfBoundsException(i2, this.i);
        }
    }

    public int a(int i2) {
        int i3 = 0;
        bv.a(i2 >= 0 && i2 < this.i);
        while (true) {
            if (i3 >= this.h.length) {
                break;
            } else if (i2 < this.h[i3]) {
                i3--;
                break;
            } else {
                i3++;
            }
        }
        return i3 == this.h.length ? i3 - 1 : i3;
    }

    public long a(String str, int i2, int i3) {
        a(str, i2);
        return this.e[i3].getLong(i2 - this.h[i3], this.d.getInt(str));
    }

    public void a() {
        this.d = new Bundle();
        for (int i2 = 0; i2 < this.c.length; i2++) {
            this.d.putInt(this.c[i2], i2);
        }
        this.h = new int[this.e.length];
        int i3 = 0;
        for (int i4 = 0; i4 < this.e.length; i4++) {
            this.h[i4] = i3;
            i3 += this.e[i4].getNumRows();
        }
        this.i = i3;
    }

    public int b() {
        return this.i;
    }

    public int b(String str, int i2, int i3) {
        a(str, i2);
        return this.e[i3].getInt(i2 - this.h[i3], this.d.getInt(str));
    }

    public String c(String str, int i2, int i3) {
        a(str, i2);
        return this.e[i3].getString(i2 - this.h[i3], this.d.getInt(str));
    }

    public boolean c() {
        boolean z;
        synchronized (this) {
            z = this.j;
        }
        return z;
    }

    public boolean d(String str, int i2, int i3) {
        a(str, i2);
        return Long.valueOf(this.e[i3].getLong(i2 - this.h[i3], this.d.getInt(str))).longValue() == 1;
    }

    public int describeContents() {
        bo boVar = a;
        return 0;
    }

    public Uri e(String str, int i2, int i3) {
        String c2 = c(str, i2, i3);
        if (c2 == null) {
            return null;
        }
        return Uri.parse(c2);
    }

    public boolean f(String str, int i2, int i3) {
        a(str, i2);
        return this.e[i3].isNull(i2 - this.h[i3], this.d.getInt(str));
    }

    public void writeToParcel(Parcel parcel, int i2) {
        bo boVar = a;
        bo.a(this, parcel, i2);
    }
}
