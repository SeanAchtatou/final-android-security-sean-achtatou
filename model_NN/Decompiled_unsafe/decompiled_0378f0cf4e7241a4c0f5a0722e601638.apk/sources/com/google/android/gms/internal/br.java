package com.google.android.gms.internal;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import com.google.android.gms.internal.bq;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;

public final class br implements Handler.Callback {
    private static br a;
    private final Context b;
    /* access modifiers changed from: private */
    public final HashMap<String, a> c = new HashMap<>();
    private final Handler d;

    final class a {
        private final String b;
        private final C0024a c = new C0024a();
        /* access modifiers changed from: private */
        public final HashSet<bq<?>.d> d = new HashSet<>();
        /* access modifiers changed from: private */
        public int e = 0;
        private boolean f;
        /* access modifiers changed from: private */
        public IBinder g;
        /* access modifiers changed from: private */
        public ComponentName h;

        /* renamed from: com.google.android.gms.internal.br$a$a  reason: collision with other inner class name */
        public class C0024a implements ServiceConnection {
            public C0024a() {
            }

            public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
                synchronized (br.this.c) {
                    IBinder unused = a.this.g = iBinder;
                    ComponentName unused2 = a.this.h = componentName;
                    Iterator it = a.this.d.iterator();
                    while (it.hasNext()) {
                        ((bq.d) it.next()).onServiceConnected(componentName, iBinder);
                    }
                    int unused3 = a.this.e = 1;
                }
            }

            public void onServiceDisconnected(ComponentName componentName) {
                synchronized (br.this.c) {
                    IBinder unused = a.this.g = (IBinder) null;
                    ComponentName unused2 = a.this.h = componentName;
                    Iterator it = a.this.d.iterator();
                    while (it.hasNext()) {
                        ((bq.d) it.next()).onServiceDisconnected(componentName);
                    }
                    int unused3 = a.this.e = 2;
                }
            }
        }

        public a(String str) {
            this.b = str;
        }

        public C0024a a() {
            return this.c;
        }

        public void a(bq<?>.d dVar) {
            this.d.add(dVar);
        }

        public void a(boolean z) {
            this.f = z;
        }

        public String b() {
            return this.b;
        }

        public void b(bq<?>.d dVar) {
            this.d.remove(dVar);
        }

        public boolean c() {
            return this.f;
        }

        public boolean c(bq<?>.d dVar) {
            return this.d.contains(dVar);
        }

        public int d() {
            return this.e;
        }

        public boolean e() {
            return this.d.isEmpty();
        }

        public IBinder f() {
            return this.g;
        }

        public ComponentName g() {
            return this.h;
        }
    }

    private br(Context context) {
        this.d = new Handler(context.getMainLooper(), this);
        this.b = context.getApplicationContext();
    }

    public static br a(Context context) {
        if (a == null) {
            a = new br(context.getApplicationContext());
        }
        return a;
    }

    public boolean a(String str, bq<?>.d dVar) {
        boolean c2;
        synchronized (this.c) {
            a aVar = this.c.get(str);
            if (aVar != null) {
                this.d.removeMessages(0, aVar);
                if (!aVar.c(dVar)) {
                    aVar.a(dVar);
                    switch (aVar.d()) {
                        case 1:
                            dVar.onServiceConnected(aVar.g(), aVar.f());
                            break;
                        case 2:
                            aVar.a(this.b.bindService(new Intent(str), aVar.a(), 129));
                            break;
                    }
                } else {
                    throw new IllegalStateException("Trying to bind a GmsServiceConnection that was already connected before.  startServiceAction=" + str);
                }
            } else {
                aVar = new a(str);
                aVar.a(dVar);
                aVar.a(this.b.bindService(new Intent(str), aVar.a(), 129));
                this.c.put(str, aVar);
            }
            c2 = aVar.c();
        }
        return c2;
    }

    public void b(String str, bq<?>.d dVar) {
        synchronized (this.c) {
            a aVar = this.c.get(str);
            if (aVar == null) {
                throw new IllegalStateException("Nonexistent connection status for service action: " + str);
            } else if (!aVar.c(dVar)) {
                throw new IllegalStateException("Trying to unbind a GmsServiceConnection  that was not bound before.  startServiceAction=" + str);
            } else {
                aVar.b(dVar);
                if (aVar.e()) {
                    this.d.sendMessageDelayed(this.d.obtainMessage(0, aVar), 5000);
                }
            }
        }
    }

    public boolean handleMessage(Message message) {
        switch (message.what) {
            case 0:
                a aVar = (a) message.obj;
                synchronized (this.c) {
                    if (aVar.e()) {
                        this.b.unbindService(aVar.a());
                        this.c.remove(aVar.b());
                    }
                }
                return true;
            default:
                return false;
        }
    }
}
