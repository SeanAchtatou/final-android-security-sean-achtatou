package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.a.e;
import com.google.android.gms.internal.an;
import com.google.android.gms.internal.b;

public class i implements Parcelable.Creator<an.a> {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.c.a(android.os.Parcel, int, java.lang.String, boolean):void
     arg types: [android.os.Parcel, int, java.lang.String, int]
     candidates:
      com.google.android.gms.internal.c.a(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.internal.c.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.internal.c.a(android.os.Parcel, int, android.os.Parcel, boolean):void
      com.google.android.gms.internal.c.a(android.os.Parcel, int, java.util.List<java.lang.String>, boolean):void
      com.google.android.gms.internal.c.a(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.internal.c.a(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.internal.c.a(android.os.Parcel, int, java.lang.String, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.c.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.internal.ai, int, int]
     candidates:
      com.google.android.gms.internal.c.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.internal.c.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    static void a(an.a aVar, Parcel parcel, int i) {
        int a = c.a(parcel);
        c.a(parcel, 1, aVar.a());
        c.a(parcel, 2, aVar.b());
        c.a(parcel, 3, aVar.c());
        c.a(parcel, 4, aVar.d());
        c.a(parcel, 5, aVar.e());
        c.a(parcel, 6, aVar.f(), false);
        c.a(parcel, 7, aVar.g());
        c.a(parcel, 8, aVar.i(), false);
        c.a(parcel, 9, (Parcelable) aVar.k(), i, false);
        c.a(parcel, a);
    }

    /* renamed from: a */
    public an.a createFromParcel(Parcel parcel) {
        ai aiVar = null;
        int i = 0;
        int b = b.b(parcel);
        String str = null;
        String str2 = null;
        boolean z = false;
        int i2 = 0;
        boolean z2 = false;
        int i3 = 0;
        int i4 = 0;
        while (parcel.dataPosition() < b) {
            int a = b.a(parcel);
            switch (b.a(a)) {
                case 1:
                    i4 = b.f(parcel, a);
                    break;
                case 2:
                    i3 = b.f(parcel, a);
                    break;
                case 3:
                    z2 = b.c(parcel, a);
                    break;
                case e.g.com_facebook_picker_fragment_done_button_text:
                    i2 = b.f(parcel, a);
                    break;
                case e.g.com_facebook_picker_fragment_title_bar_background:
                    z = b.c(parcel, a);
                    break;
                case e.g.com_facebook_picker_fragment_done_button_background:
                    str2 = b.l(parcel, a);
                    break;
                case 7:
                    i = b.f(parcel, a);
                    break;
                case 8:
                    str = b.l(parcel, a);
                    break;
                case 9:
                    aiVar = (ai) b.a(parcel, a, ai.a);
                    break;
                default:
                    b.b(parcel, a);
                    break;
            }
        }
        if (parcel.dataPosition() == b) {
            return new an.a(i4, i3, z2, i2, z, str2, i, str, aiVar);
        }
        throw new b.a("Overread allowed size end=" + b, parcel);
    }

    /* renamed from: a */
    public an.a[] newArray(int i) {
        return new an.a[i];
    }
}
