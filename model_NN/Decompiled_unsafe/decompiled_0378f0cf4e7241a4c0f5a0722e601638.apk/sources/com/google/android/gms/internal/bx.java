package com.google.android.gms.internal;

import android.net.Uri;
import android.os.Parcel;
import com.google.android.gms.games.Player;
import com.google.android.gms.games.multiplayer.Participant;
import com.google.android.gms.games.multiplayer.ParticipantEntity;

public final class bx extends bn implements Participant {
    private final bg c;

    public bx(k kVar, int i) {
        super(kVar, i);
        this.c = new bg(kVar, i);
    }

    public int b() {
        return b("player_status");
    }

    public String c() {
        return d("client_address");
    }

    public boolean d() {
        return b("connected") > 0;
    }

    public int describeContents() {
        return 0;
    }

    public String e() {
        return f("external_player_id") ? d("default_display_name") : this.c.c();
    }

    public boolean equals(Object obj) {
        return ParticipantEntity.a(this, obj);
    }

    public Uri f() {
        return f("external_player_id") ? e("default_display_image_uri") : this.c.d();
    }

    public Uri g() {
        if (f("external_player_id")) {
            return null;
        }
        return this.c.e();
    }

    public String h() {
        return d("external_participant_id");
    }

    public int hashCode() {
        return ParticipantEntity.a(this);
    }

    public Player i() {
        if (f("external_player_id")) {
            return null;
        }
        return this.c;
    }

    /* renamed from: j */
    public Participant a() {
        return new ParticipantEntity(this);
    }

    public String toString() {
        return ParticipantEntity.b(this);
    }

    public void writeToParcel(Parcel parcel, int i) {
        ((ParticipantEntity) a()).writeToParcel(parcel, i);
    }
}
