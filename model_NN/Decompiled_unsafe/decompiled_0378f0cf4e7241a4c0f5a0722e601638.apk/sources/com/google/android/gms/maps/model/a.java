package com.google.android.gms.maps.model;

import com.google.android.gms.internal.bv;
import com.google.android.gms.internal.s;

public final class a {
    private final s a;

    public a(s sVar) {
        this.a = (s) bv.a(sVar);
    }

    public s a() {
        return this.a;
    }
}
