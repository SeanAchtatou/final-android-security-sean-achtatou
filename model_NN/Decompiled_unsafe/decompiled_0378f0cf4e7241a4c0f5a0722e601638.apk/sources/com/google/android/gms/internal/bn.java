package com.google.android.gms.internal;

import android.net.Uri;

public abstract class bn {
    protected final k a;
    protected final int b;
    private final int c;

    public bn(k kVar, int i) {
        this.a = (k) bv.a(kVar);
        bv.a(i >= 0 && i < kVar.b());
        this.b = i;
        this.c = kVar.a(this.b);
    }

    /* access modifiers changed from: protected */
    public long a(String str) {
        return this.a.a(str, this.b, this.c);
    }

    /* access modifiers changed from: protected */
    public int b(String str) {
        return this.a.b(str, this.b, this.c);
    }

    /* access modifiers changed from: protected */
    public boolean c(String str) {
        return this.a.d(str, this.b, this.c);
    }

    /* access modifiers changed from: protected */
    public String d(String str) {
        return this.a.c(str, this.b, this.c);
    }

    /* access modifiers changed from: protected */
    public Uri e(String str) {
        return this.a.e(str, this.b, this.c);
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof bn)) {
            return false;
        }
        bn bnVar = (bn) obj;
        return bu.a(Integer.valueOf(bnVar.b), Integer.valueOf(this.b)) && bu.a(Integer.valueOf(bnVar.c), Integer.valueOf(this.c)) && bnVar.a == this.a;
    }

    /* access modifiers changed from: protected */
    public boolean f(String str) {
        return this.a.f(str, this.b, this.c);
    }

    public int hashCode() {
        return bu.a(Integer.valueOf(this.b), Integer.valueOf(this.c), this.a);
    }
}
