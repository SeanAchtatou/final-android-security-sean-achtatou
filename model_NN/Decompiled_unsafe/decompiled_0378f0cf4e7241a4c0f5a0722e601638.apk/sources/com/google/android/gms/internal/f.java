package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.internal.b;

public class f implements Parcelable.Creator<ai> {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.c.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.internal.ak, int, int]
     candidates:
      com.google.android.gms.internal.c.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.internal.c.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    static void a(ai aiVar, Parcel parcel, int i) {
        int a = c.a(parcel);
        c.a(parcel, 1, aiVar.a());
        c.a(parcel, 2, (Parcelable) aiVar.b(), i, false);
        c.a(parcel, a);
    }

    /* renamed from: a */
    public ai createFromParcel(Parcel parcel) {
        int b = b.b(parcel);
        int i = 0;
        ak akVar = null;
        while (parcel.dataPosition() < b) {
            int a = b.a(parcel);
            switch (b.a(a)) {
                case 1:
                    i = b.f(parcel, a);
                    break;
                case 2:
                    akVar = (ak) b.a(parcel, a, ak.a);
                    break;
                default:
                    b.b(parcel, a);
                    break;
            }
        }
        if (parcel.dataPosition() == b) {
            return new ai(i, akVar);
        }
        throw new b.a("Overread allowed size end=" + b, parcel);
    }

    /* renamed from: a */
    public ai[] newArray(int i) {
        return new ai[i];
    }
}
