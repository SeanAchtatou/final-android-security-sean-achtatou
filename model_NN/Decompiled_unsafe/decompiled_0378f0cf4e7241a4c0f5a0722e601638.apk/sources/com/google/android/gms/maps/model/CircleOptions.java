package com.google.android.gms.maps.model;

import android.os.Parcel;
import com.google.android.gms.internal.ae;
import com.google.android.gms.internal.w;
import com.google.android.gms.internal.y;

public final class CircleOptions implements ae {
    public static final c a = new c();
    private final int b;
    private LatLng c;
    private double d;
    private float e;
    private int f;
    private int g;
    private float h;
    private boolean i;

    public CircleOptions() {
        this.c = null;
        this.d = 0.0d;
        this.e = 10.0f;
        this.f = -16777216;
        this.g = 0;
        this.h = 0.0f;
        this.i = true;
        this.b = 1;
    }

    CircleOptions(int i2, LatLng latLng, double d2, float f2, int i3, int i4, float f3, boolean z) {
        this.c = null;
        this.d = 0.0d;
        this.e = 10.0f;
        this.f = -16777216;
        this.g = 0;
        this.h = 0.0f;
        this.i = true;
        this.b = i2;
        this.c = latLng;
        this.d = d2;
        this.e = f2;
        this.f = i3;
        this.g = i4;
        this.h = f3;
        this.i = z;
    }

    public int a() {
        return this.b;
    }

    public LatLng b() {
        return this.c;
    }

    public double c() {
        return this.d;
    }

    public float d() {
        return this.e;
    }

    public int describeContents() {
        return 0;
    }

    public int e() {
        return this.f;
    }

    public int f() {
        return this.g;
    }

    public float g() {
        return this.h;
    }

    public boolean h() {
        return this.i;
    }

    public void writeToParcel(Parcel parcel, int i2) {
        if (w.a()) {
            y.a(this, parcel, i2);
        } else {
            c.a(this, parcel, i2);
        }
    }
}
