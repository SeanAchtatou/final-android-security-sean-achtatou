package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.a.e;
import com.google.android.gms.internal.b;

public class t implements Parcelable.Creator<cf> {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.c.a(android.os.Parcel, int, java.lang.String, boolean):void
     arg types: [android.os.Parcel, int, java.lang.String, int]
     candidates:
      com.google.android.gms.internal.c.a(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.internal.c.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.internal.c.a(android.os.Parcel, int, android.os.Parcel, boolean):void
      com.google.android.gms.internal.c.a(android.os.Parcel, int, java.util.List<java.lang.String>, boolean):void
      com.google.android.gms.internal.c.a(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.internal.c.a(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.internal.c.a(android.os.Parcel, int, java.lang.String, boolean):void */
    static void a(cf cfVar, Parcel parcel, int i) {
        int a = c.a(parcel);
        c.a(parcel, 1, cfVar.f(), false);
        c.a(parcel, 1000, cfVar.a());
        c.a(parcel, 2, cfVar.g());
        c.a(parcel, 3, cfVar.b());
        c.a(parcel, 4, cfVar.c());
        c.a(parcel, 5, cfVar.d());
        c.a(parcel, 6, cfVar.e());
        c.a(parcel, 7, cfVar.h());
        c.a(parcel, a);
    }

    /* renamed from: a */
    public cf createFromParcel(Parcel parcel) {
        double d = 0.0d;
        short s = 0;
        int b = b.b(parcel);
        String str = null;
        float f = 0.0f;
        long j = 0;
        double d2 = 0.0d;
        int i = 0;
        int i2 = 0;
        while (parcel.dataPosition() < b) {
            int a = b.a(parcel);
            switch (b.a(a)) {
                case 1:
                    str = b.l(parcel, a);
                    break;
                case 2:
                    j = b.g(parcel, a);
                    break;
                case 3:
                    s = b.e(parcel, a);
                    break;
                case e.g.com_facebook_picker_fragment_done_button_text:
                    d2 = b.j(parcel, a);
                    break;
                case e.g.com_facebook_picker_fragment_title_bar_background:
                    d = b.j(parcel, a);
                    break;
                case e.g.com_facebook_picker_fragment_done_button_background:
                    f = b.i(parcel, a);
                    break;
                case 7:
                    i = b.f(parcel, a);
                    break;
                case 1000:
                    i2 = b.f(parcel, a);
                    break;
                default:
                    b.b(parcel, a);
                    break;
            }
        }
        if (parcel.dataPosition() == b) {
            return new cf(i2, str, i, s, d2, d, f, j);
        }
        throw new b.a("Overread allowed size end=" + b, parcel);
    }

    /* renamed from: a */
    public cf[] newArray(int i) {
        return new cf[i];
    }
}
