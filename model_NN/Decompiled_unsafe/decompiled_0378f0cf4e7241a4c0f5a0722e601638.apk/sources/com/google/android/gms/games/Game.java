package com.google.android.gms.games;

import android.net.Uri;
import android.os.Parcelable;
import com.google.android.gms.common.a.a;

public interface Game extends Parcelable, a<Game> {
    String b();

    String c();

    String d();

    String e();

    String f();

    String g();

    Uri h();

    Uri i();

    Uri j();

    boolean k();

    boolean l();

    String m();

    int n();

    int o();

    int p();
}
