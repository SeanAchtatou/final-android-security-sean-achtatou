package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.maps.model.GroundOverlayOptions;

public class z {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.c.a(android.os.Parcel, int, android.os.IBinder, boolean):void
     arg types: [android.os.Parcel, int, android.os.IBinder, int]
     candidates:
      com.google.android.gms.internal.c.a(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.internal.c.a(android.os.Parcel, int, android.os.Parcel, boolean):void
      com.google.android.gms.internal.c.a(android.os.Parcel, int, java.lang.String, boolean):void
      com.google.android.gms.internal.c.a(android.os.Parcel, int, java.util.List<java.lang.String>, boolean):void
      com.google.android.gms.internal.c.a(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.internal.c.a(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.internal.c.a(android.os.Parcel, int, android.os.IBinder, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.c.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.maps.model.LatLng, int, int]
     candidates:
      com.google.android.gms.internal.c.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.internal.c.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.c.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.maps.model.LatLngBounds, int, int]
     candidates:
      com.google.android.gms.internal.c.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.internal.c.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    public static void a(GroundOverlayOptions groundOverlayOptions, Parcel parcel, int i) {
        int a = c.a(parcel);
        c.a(parcel, 1, groundOverlayOptions.b());
        c.a(parcel, 2, groundOverlayOptions.a(), false);
        c.a(parcel, 3, (Parcelable) groundOverlayOptions.c(), i, false);
        c.a(parcel, 4, groundOverlayOptions.d());
        c.a(parcel, 5, groundOverlayOptions.e());
        c.a(parcel, 6, (Parcelable) groundOverlayOptions.f(), i, false);
        c.a(parcel, 7, groundOverlayOptions.g());
        c.a(parcel, 8, groundOverlayOptions.h());
        c.a(parcel, 9, groundOverlayOptions.l());
        c.a(parcel, 10, groundOverlayOptions.i());
        c.a(parcel, 11, groundOverlayOptions.j());
        c.a(parcel, 12, groundOverlayOptions.k());
        c.a(parcel, a);
    }
}
