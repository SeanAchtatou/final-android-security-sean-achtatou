package com.google.android.gms.internal;

import android.os.Bundle;
import android.os.Parcel;
import com.facebook.a.e;
import com.google.android.gms.internal.an;
import com.google.android.gms.internal.b;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class at extends an implements ae {
    public static final n a = new n();
    private final int b;
    private final Parcel c;
    private final int d = 2;
    private final aq e;
    private final String f;
    private int g;
    private int h;

    at(int i, Parcel parcel, aq aqVar) {
        this.b = i;
        this.c = (Parcel) bv.a(parcel);
        this.e = aqVar;
        if (this.e == null) {
            this.f = null;
        } else {
            this.f = this.e.d();
        }
        this.g = 2;
    }

    public static HashMap<String, String> a(Bundle bundle) {
        HashMap<String, String> hashMap = new HashMap<>();
        for (String next : bundle.keySet()) {
            hashMap.put(next, bundle.getString(next));
        }
        return hashMap;
    }

    private static HashMap<Integer, Map.Entry<String, an.a<?, ?>>> a(HashMap<String, an.a<?, ?>> hashMap) {
        HashMap<Integer, Map.Entry<String, an.a<?, ?>>> hashMap2 = new HashMap<>();
        for (Map.Entry next : hashMap.entrySet()) {
            hashMap2.put(Integer.valueOf(((an.a) next.getValue()).g()), next);
        }
        return hashMap2;
    }

    private void a(StringBuilder sb, int i, Object obj) {
        switch (i) {
            case 0:
            case 1:
            case 2:
            case 3:
            case e.g.com_facebook_picker_fragment_done_button_text:
            case e.g.com_facebook_picker_fragment_title_bar_background:
            case e.g.com_facebook_picker_fragment_done_button_background:
                sb.append(obj);
                return;
            case 7:
                sb.append("\"").append(q.a(obj.toString())).append("\"");
                return;
            case 8:
                sb.append("\"").append(p.a((byte[]) obj)).append("\"");
                return;
            case 9:
                sb.append("\"").append(p.b((byte[]) obj));
                sb.append("\"");
                return;
            case 10:
                r.a(sb, (HashMap) obj);
                return;
            case 11:
                throw new IllegalArgumentException("Method does not accept concrete type.");
            default:
                throw new IllegalArgumentException("Unknown type = " + i);
        }
    }

    private void a(StringBuilder sb, an.a<?, ?> aVar, Parcel parcel, int i) {
        switch (aVar.d()) {
            case 0:
                a(sb, aVar, a(aVar, Integer.valueOf(b.f(parcel, i))));
                return;
            case 1:
                a(sb, aVar, a(aVar, b.h(parcel, i)));
                return;
            case 2:
                a(sb, aVar, a(aVar, Long.valueOf(b.g(parcel, i))));
                return;
            case 3:
                a(sb, aVar, a(aVar, Float.valueOf(b.i(parcel, i))));
                return;
            case e.g.com_facebook_picker_fragment_done_button_text:
                a(sb, aVar, a(aVar, Double.valueOf(b.j(parcel, i))));
                return;
            case e.g.com_facebook_picker_fragment_title_bar_background:
                a(sb, aVar, a(aVar, b.k(parcel, i)));
                return;
            case e.g.com_facebook_picker_fragment_done_button_background:
                a(sb, aVar, a(aVar, Boolean.valueOf(b.c(parcel, i))));
                return;
            case 7:
                a(sb, aVar, a(aVar, b.l(parcel, i)));
                return;
            case 8:
            case 9:
                a(sb, aVar, a(aVar, b.o(parcel, i)));
                return;
            case 10:
                a(sb, aVar, a(aVar, a(b.n(parcel, i))));
                return;
            case 11:
                throw new IllegalArgumentException("Method does not accept concrete type.");
            default:
                throw new IllegalArgumentException("Unknown field out type = " + aVar.d());
        }
    }

    private void a(StringBuilder sb, an.a<?, ?> aVar, Object obj) {
        if (aVar.c()) {
            a(sb, aVar, (ArrayList<?>) ((ArrayList) obj));
        } else {
            a(sb, aVar.b(), obj);
        }
    }

    private void a(StringBuilder sb, an.a<?, ?> aVar, ArrayList<?> arrayList) {
        sb.append("[");
        int size = arrayList.size();
        for (int i = 0; i < size; i++) {
            if (i != 0) {
                sb.append(",");
            }
            a(sb, aVar.b(), arrayList.get(i));
        }
        sb.append("]");
    }

    private void a(StringBuilder sb, String str, an.a<?, ?> aVar, Parcel parcel, int i) {
        sb.append("\"").append(str).append("\":");
        if (aVar.j()) {
            a(sb, aVar, parcel, i);
        } else {
            b(sb, aVar, parcel, i);
        }
    }

    private void a(StringBuilder sb, HashMap<String, an.a<?, ?>> hashMap, Parcel parcel) {
        HashMap<Integer, Map.Entry<String, an.a<?, ?>>> a2 = a(hashMap);
        sb.append('{');
        int b2 = b.b(parcel);
        boolean z = false;
        while (parcel.dataPosition() < b2) {
            int a3 = b.a(parcel);
            Map.Entry entry = a2.get(Integer.valueOf(b.a(a3)));
            if (entry != null) {
                if (z) {
                    sb.append(",");
                }
                a(sb, (String) entry.getKey(), (an.a) entry.getValue(), parcel, a3);
                z = true;
            }
        }
        if (parcel.dataPosition() != b2) {
            throw new b.a("Overread allowed size end=" + b2, parcel);
        }
        sb.append('}');
    }

    private void b(StringBuilder sb, an.a<?, ?> aVar, Parcel parcel, int i) {
        if (aVar.e()) {
            sb.append("[");
            switch (aVar.d()) {
                case 0:
                    o.a(sb, b.q(parcel, i));
                    break;
                case 1:
                    o.a(sb, b.s(parcel, i));
                    break;
                case 2:
                    o.a(sb, b.r(parcel, i));
                    break;
                case 3:
                    o.a(sb, b.t(parcel, i));
                    break;
                case e.g.com_facebook_picker_fragment_done_button_text:
                    o.a(sb, b.u(parcel, i));
                    break;
                case e.g.com_facebook_picker_fragment_title_bar_background:
                    o.a(sb, b.v(parcel, i));
                    break;
                case e.g.com_facebook_picker_fragment_done_button_background:
                    o.a(sb, b.p(parcel, i));
                    break;
                case 7:
                    o.a(sb, b.w(parcel, i));
                    break;
                case 8:
                case 9:
                case 10:
                    throw new UnsupportedOperationException("List of type BASE64, BASE64_URL_SAFE, or STRING_MAP is not supported");
                case 11:
                    Parcel[] z = b.z(parcel, i);
                    int length = z.length;
                    for (int i2 = 0; i2 < length; i2++) {
                        if (i2 > 0) {
                            sb.append(",");
                        }
                        z[i2].setDataPosition(0);
                        a(sb, aVar.l(), z[i2]);
                    }
                    break;
                default:
                    throw new IllegalStateException("Unknown field type out.");
            }
            sb.append("]");
            return;
        }
        switch (aVar.d()) {
            case 0:
                sb.append(b.f(parcel, i));
                return;
            case 1:
                sb.append(b.h(parcel, i));
                return;
            case 2:
                sb.append(b.g(parcel, i));
                return;
            case 3:
                sb.append(b.i(parcel, i));
                return;
            case e.g.com_facebook_picker_fragment_done_button_text:
                sb.append(b.j(parcel, i));
                return;
            case e.g.com_facebook_picker_fragment_title_bar_background:
                sb.append(b.k(parcel, i));
                return;
            case e.g.com_facebook_picker_fragment_done_button_background:
                sb.append(b.c(parcel, i));
                return;
            case 7:
                sb.append("\"").append(q.a(b.l(parcel, i))).append("\"");
                return;
            case 8:
                sb.append("\"").append(p.a(b.o(parcel, i))).append("\"");
                return;
            case 9:
                sb.append("\"").append(p.b(b.o(parcel, i)));
                sb.append("\"");
                return;
            case 10:
                Bundle n = b.n(parcel, i);
                Set<String> keySet = n.keySet();
                keySet.size();
                sb.append("{");
                boolean z2 = true;
                for (String next : keySet) {
                    if (!z2) {
                        sb.append(",");
                    }
                    sb.append("\"").append(next).append("\"");
                    sb.append(":");
                    sb.append("\"").append(q.a(n.getString(next))).append("\"");
                    z2 = false;
                }
                sb.append("}");
                return;
            case 11:
                Parcel y = b.y(parcel, i);
                y.setDataPosition(0);
                a(sb, aVar.l(), y);
                return;
            default:
                throw new IllegalStateException("Unknown field type out");
        }
    }

    public int a() {
        return this.b;
    }

    /* access modifiers changed from: protected */
    public Object a(String str) {
        throw new UnsupportedOperationException("Converting to JSON does not require this method.");
    }

    public HashMap<String, an.a<?, ?>> b() {
        if (this.e == null) {
            return null;
        }
        return this.e.a(this.f);
    }

    /* access modifiers changed from: protected */
    public boolean b(String str) {
        throw new UnsupportedOperationException("Converting to JSON does not require this method.");
    }

    public int describeContents() {
        n nVar = a;
        return 0;
    }

    public Parcel e() {
        switch (this.g) {
            case 0:
                this.h = c.a(this.c);
                c.a(this.c, this.h);
                this.g = 2;
                break;
            case 1:
                c.a(this.c, this.h);
                this.g = 2;
                break;
        }
        return this.c;
    }

    /* access modifiers changed from: package-private */
    public aq f() {
        switch (this.d) {
            case 0:
                return null;
            case 1:
                return this.e;
            case 2:
                return this.e;
            default:
                throw new IllegalStateException("Invalid creation type: " + this.d);
        }
    }

    public String toString() {
        bv.a(this.e, "Cannot convert to JSON on client side.");
        Parcel e2 = e();
        e2.setDataPosition(0);
        StringBuilder sb = new StringBuilder(100);
        a(sb, this.e.a(this.f), e2);
        return sb.toString();
    }

    public void writeToParcel(Parcel parcel, int i) {
        n nVar = a;
        n.a(this, parcel, i);
    }
}
