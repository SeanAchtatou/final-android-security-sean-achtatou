package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.a.e;
import com.google.android.gms.internal.b;
import com.google.android.gms.internal.eq;
import java.util.HashSet;
import java.util.Set;

public class bb implements Parcelable.Creator<eq.b> {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.c.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.internal.eq$b$a, int, int]
     candidates:
      com.google.android.gms.internal.c.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.internal.c.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.c.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.internal.eq$b$b, int, int]
     candidates:
      com.google.android.gms.internal.c.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.internal.c.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    static void a(eq.b bVar, Parcel parcel, int i) {
        int a = c.a(parcel);
        Set<Integer> e = bVar.e();
        if (e.contains(1)) {
            c.a(parcel, 1, bVar.f());
        }
        if (e.contains(2)) {
            c.a(parcel, 2, (Parcelable) bVar.g(), i, true);
        }
        if (e.contains(3)) {
            c.a(parcel, 3, (Parcelable) bVar.h(), i, true);
        }
        if (e.contains(4)) {
            c.a(parcel, 4, bVar.i());
        }
        c.a(parcel, a);
    }

    /* renamed from: a */
    public eq.b createFromParcel(Parcel parcel) {
        eq.b.C0027b bVar = null;
        int i = 0;
        int b = b.b(parcel);
        HashSet hashSet = new HashSet();
        eq.b.a aVar = null;
        int i2 = 0;
        while (parcel.dataPosition() < b) {
            int a = b.a(parcel);
            switch (b.a(a)) {
                case 1:
                    i2 = b.f(parcel, a);
                    hashSet.add(1);
                    break;
                case 2:
                    hashSet.add(2);
                    aVar = (eq.b.a) b.a(parcel, a, eq.b.a.a);
                    break;
                case 3:
                    hashSet.add(3);
                    bVar = (eq.b.C0027b) b.a(parcel, a, eq.b.C0027b.a);
                    break;
                case e.g.com_facebook_picker_fragment_done_button_text:
                    i = b.f(parcel, a);
                    hashSet.add(4);
                    break;
                default:
                    b.b(parcel, a);
                    break;
            }
        }
        if (parcel.dataPosition() == b) {
            return new eq.b(hashSet, i2, aVar, bVar, i);
        }
        throw new b.a("Overread allowed size end=" + b, parcel);
    }

    /* renamed from: a */
    public eq.b[] newArray(int i) {
        return new eq.b[i];
    }
}
