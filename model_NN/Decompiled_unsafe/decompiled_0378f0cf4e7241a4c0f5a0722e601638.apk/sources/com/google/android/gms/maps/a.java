package com.google.android.gms.maps;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.a.e;
import com.google.android.gms.internal.b;
import com.google.android.gms.internal.c;
import com.google.android.gms.maps.model.CameraPosition;

public class a implements Parcelable.Creator<GoogleMapOptions> {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.c.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.maps.model.CameraPosition, int, int]
     candidates:
      com.google.android.gms.internal.c.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.internal.c.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    static void a(GoogleMapOptions googleMapOptions, Parcel parcel, int i) {
        int a = c.a(parcel);
        c.a(parcel, 1, googleMapOptions.a());
        c.a(parcel, 2, googleMapOptions.b());
        c.a(parcel, 3, googleMapOptions.c());
        c.a(parcel, 4, googleMapOptions.j());
        c.a(parcel, 5, (Parcelable) googleMapOptions.k(), i, false);
        c.a(parcel, 6, googleMapOptions.d());
        c.a(parcel, 7, googleMapOptions.e());
        c.a(parcel, 8, googleMapOptions.f());
        c.a(parcel, 9, googleMapOptions.g());
        c.a(parcel, 10, googleMapOptions.h());
        c.a(parcel, 11, googleMapOptions.i());
        c.a(parcel, a);
    }

    /* renamed from: a */
    public GoogleMapOptions createFromParcel(Parcel parcel) {
        byte b = 0;
        int b2 = b.b(parcel);
        CameraPosition cameraPosition = null;
        byte b3 = 0;
        byte b4 = 0;
        byte b5 = 0;
        byte b6 = 0;
        byte b7 = 0;
        int i = 0;
        byte b8 = 0;
        byte b9 = 0;
        int i2 = 0;
        while (parcel.dataPosition() < b2) {
            int a = b.a(parcel);
            switch (b.a(a)) {
                case 1:
                    i2 = b.f(parcel, a);
                    break;
                case 2:
                    b9 = b.d(parcel, a);
                    break;
                case 3:
                    b8 = b.d(parcel, a);
                    break;
                case e.g.com_facebook_picker_fragment_done_button_text:
                    i = b.f(parcel, a);
                    break;
                case e.g.com_facebook_picker_fragment_title_bar_background:
                    cameraPosition = (CameraPosition) b.a(parcel, a, CameraPosition.a);
                    break;
                case e.g.com_facebook_picker_fragment_done_button_background:
                    b7 = b.d(parcel, a);
                    break;
                case 7:
                    b6 = b.d(parcel, a);
                    break;
                case 8:
                    b5 = b.d(parcel, a);
                    break;
                case 9:
                    b4 = b.d(parcel, a);
                    break;
                case 10:
                    b3 = b.d(parcel, a);
                    break;
                case 11:
                    b = b.d(parcel, a);
                    break;
                default:
                    b.b(parcel, a);
                    break;
            }
        }
        if (parcel.dataPosition() == b2) {
            return new GoogleMapOptions(i2, b9, b8, i, cameraPosition, b7, b6, b5, b4, b3, b);
        }
        throw new b.a("Overread allowed size end=" + b2, parcel);
    }

    /* renamed from: a */
    public GoogleMapOptions[] newArray(int i) {
        return new GoogleMapOptions[i];
    }
}
