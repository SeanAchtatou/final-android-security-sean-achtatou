package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.a.e;
import com.google.android.gms.internal.b;
import com.google.android.gms.internal.eq;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class bk implements Parcelable.Creator<eq> {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.c.a(android.os.Parcel, int, java.lang.String, boolean):void
     arg types: [android.os.Parcel, int, java.lang.String, int]
     candidates:
      com.google.android.gms.internal.c.a(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.internal.c.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.internal.c.a(android.os.Parcel, int, android.os.Parcel, boolean):void
      com.google.android.gms.internal.c.a(android.os.Parcel, int, java.util.List<java.lang.String>, boolean):void
      com.google.android.gms.internal.c.a(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.internal.c.a(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.internal.c.a(android.os.Parcel, int, java.lang.String, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.c.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.internal.eq$a, int, int]
     candidates:
      com.google.android.gms.internal.c.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.internal.c.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.c.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.internal.eq$b, int, int]
     candidates:
      com.google.android.gms.internal.c.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.internal.c.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.c.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.internal.eq$d, int, int]
     candidates:
      com.google.android.gms.internal.c.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.internal.c.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.c.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.internal.eq$e, int, int]
     candidates:
      com.google.android.gms.internal.c.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.internal.c.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    static void a(eq eqVar, Parcel parcel, int i) {
        int a = c.a(parcel);
        Set<Integer> e = eqVar.e();
        if (e.contains(1)) {
            c.a(parcel, 1, eqVar.f());
        }
        if (e.contains(2)) {
            c.a(parcel, 2, eqVar.g(), true);
        }
        if (e.contains(3)) {
            c.a(parcel, 3, (Parcelable) eqVar.h(), i, true);
        }
        if (e.contains(4)) {
            c.a(parcel, 4, eqVar.i(), true);
        }
        if (e.contains(5)) {
            c.a(parcel, 5, eqVar.j(), true);
        }
        if (e.contains(6)) {
            c.a(parcel, 6, eqVar.k());
        }
        if (e.contains(7)) {
            c.a(parcel, 7, (Parcelable) eqVar.l(), i, true);
        }
        if (e.contains(8)) {
            c.a(parcel, 8, eqVar.m(), true);
        }
        if (e.contains(9)) {
            c.a(parcel, 9, eqVar.n(), true);
        }
        if (e.contains(10)) {
            c.b(parcel, 10, eqVar.o(), true);
        }
        if (e.contains(11)) {
            c.a(parcel, 11, eqVar.p(), true);
        }
        if (e.contains(12)) {
            c.a(parcel, 12, eqVar.q());
        }
        if (e.contains(13)) {
            c.a(parcel, 13, eqVar.r());
        }
        if (e.contains(14)) {
            c.a(parcel, 14, eqVar.s(), true);
        }
        if (e.contains(15)) {
            c.a(parcel, 15, (Parcelable) eqVar.t(), i, true);
        }
        if (e.contains(16)) {
            c.a(parcel, 16, eqVar.u());
        }
        if (e.contains(19)) {
            c.a(parcel, 19, (Parcelable) eqVar.w(), i, true);
        }
        if (e.contains(18)) {
            c.a(parcel, 18, eqVar.v(), true);
        }
        if (e.contains(21)) {
            c.a(parcel, 21, eqVar.y());
        }
        if (e.contains(20)) {
            c.a(parcel, 20, eqVar.x(), true);
        }
        if (e.contains(23)) {
            c.b(parcel, 23, eqVar.A(), true);
        }
        if (e.contains(22)) {
            c.b(parcel, 22, eqVar.z(), true);
        }
        if (e.contains(25)) {
            c.a(parcel, 25, eqVar.C());
        }
        if (e.contains(24)) {
            c.a(parcel, 24, eqVar.B());
        }
        if (e.contains(27)) {
            c.a(parcel, 27, eqVar.E(), true);
        }
        if (e.contains(26)) {
            c.a(parcel, 26, eqVar.D(), true);
        }
        if (e.contains(29)) {
            c.a(parcel, 29, eqVar.G());
        }
        if (e.contains(28)) {
            c.b(parcel, 28, eqVar.F(), true);
        }
        c.a(parcel, a);
    }

    /* renamed from: a */
    public eq createFromParcel(Parcel parcel) {
        int b = b.b(parcel);
        HashSet hashSet = new HashSet();
        int i = 0;
        String str = null;
        eq.a aVar = null;
        String str2 = null;
        String str3 = null;
        int i2 = 0;
        eq.b bVar = null;
        String str4 = null;
        String str5 = null;
        ArrayList arrayList = null;
        String str6 = null;
        int i3 = 0;
        boolean z = false;
        String str7 = null;
        eq.d dVar = null;
        boolean z2 = false;
        String str8 = null;
        eq.e eVar = null;
        String str9 = null;
        int i4 = 0;
        ArrayList arrayList2 = null;
        ArrayList arrayList3 = null;
        int i5 = 0;
        int i6 = 0;
        String str10 = null;
        String str11 = null;
        ArrayList arrayList4 = null;
        boolean z3 = false;
        while (parcel.dataPosition() < b) {
            int a = b.a(parcel);
            switch (b.a(a)) {
                case 1:
                    i = b.f(parcel, a);
                    hashSet.add(1);
                    break;
                case 2:
                    str = b.l(parcel, a);
                    hashSet.add(2);
                    break;
                case 3:
                    hashSet.add(3);
                    aVar = (eq.a) b.a(parcel, a, eq.a.a);
                    break;
                case e.g.com_facebook_picker_fragment_done_button_text:
                    str2 = b.l(parcel, a);
                    hashSet.add(4);
                    break;
                case e.g.com_facebook_picker_fragment_title_bar_background:
                    str3 = b.l(parcel, a);
                    hashSet.add(5);
                    break;
                case e.g.com_facebook_picker_fragment_done_button_background:
                    i2 = b.f(parcel, a);
                    hashSet.add(6);
                    break;
                case 7:
                    hashSet.add(7);
                    bVar = (eq.b) b.a(parcel, a, eq.b.a);
                    break;
                case 8:
                    str4 = b.l(parcel, a);
                    hashSet.add(8);
                    break;
                case 9:
                    str5 = b.l(parcel, a);
                    hashSet.add(9);
                    break;
                case 10:
                    arrayList = b.c(parcel, a, eq.c.a);
                    hashSet.add(10);
                    break;
                case 11:
                    str6 = b.l(parcel, a);
                    hashSet.add(11);
                    break;
                case 12:
                    i3 = b.f(parcel, a);
                    hashSet.add(12);
                    break;
                case 13:
                    z = b.c(parcel, a);
                    hashSet.add(13);
                    break;
                case 14:
                    str7 = b.l(parcel, a);
                    hashSet.add(14);
                    break;
                case 15:
                    hashSet.add(15);
                    dVar = (eq.d) b.a(parcel, a, eq.d.a);
                    break;
                case 16:
                    z2 = b.c(parcel, a);
                    hashSet.add(16);
                    break;
                case 17:
                default:
                    b.b(parcel, a);
                    break;
                case 18:
                    str8 = b.l(parcel, a);
                    hashSet.add(18);
                    break;
                case 19:
                    hashSet.add(19);
                    eVar = (eq.e) b.a(parcel, a, eq.e.a);
                    break;
                case 20:
                    str9 = b.l(parcel, a);
                    hashSet.add(20);
                    break;
                case 21:
                    i4 = b.f(parcel, a);
                    hashSet.add(21);
                    break;
                case 22:
                    arrayList2 = b.c(parcel, a, eq.g.a);
                    hashSet.add(22);
                    break;
                case 23:
                    arrayList3 = b.c(parcel, a, eq.h.a);
                    hashSet.add(23);
                    break;
                case 24:
                    i5 = b.f(parcel, a);
                    hashSet.add(24);
                    break;
                case 25:
                    i6 = b.f(parcel, a);
                    hashSet.add(25);
                    break;
                case 26:
                    str10 = b.l(parcel, a);
                    hashSet.add(26);
                    break;
                case 27:
                    str11 = b.l(parcel, a);
                    hashSet.add(27);
                    break;
                case 28:
                    arrayList4 = b.c(parcel, a, eq.i.a);
                    hashSet.add(28);
                    break;
                case 29:
                    z3 = b.c(parcel, a);
                    hashSet.add(29);
                    break;
            }
        }
        if (parcel.dataPosition() == b) {
            return new eq(hashSet, i, str, aVar, str2, str3, i2, bVar, str4, str5, arrayList, str6, i3, z, str7, dVar, z2, str8, eVar, str9, i4, arrayList2, arrayList3, i5, i6, str10, str11, arrayList4, z3);
        }
        throw new b.a("Overread allowed size end=" + b, parcel);
    }

    /* renamed from: a */
    public eq[] newArray(int i) {
        return new eq[i];
    }
}
