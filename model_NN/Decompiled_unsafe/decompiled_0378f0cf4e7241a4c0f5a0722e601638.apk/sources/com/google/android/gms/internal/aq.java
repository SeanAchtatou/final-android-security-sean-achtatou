package com.google.android.gms.internal;

import android.os.Parcel;
import com.google.android.gms.internal.an;
import java.util.ArrayList;
import java.util.HashMap;

public class aq implements ae {
    public static final l a = new l();
    private final int b;
    private final HashMap<String, HashMap<String, an.a<?, ?>>> c;
    private final ArrayList<a> d = null;
    private final String e;

    public static class a implements ae {
        public static final m a = new m();
        final int b;
        final String c;
        final ArrayList<b> d;

        a(int i, String str, ArrayList<b> arrayList) {
            this.b = i;
            this.c = str;
            this.d = arrayList;
        }

        a(String str, HashMap<String, an.a<?, ?>> hashMap) {
            this.b = 1;
            this.c = str;
            this.d = a(hashMap);
        }

        private static ArrayList<b> a(HashMap<String, an.a<?, ?>> hashMap) {
            if (hashMap == null) {
                return null;
            }
            ArrayList<b> arrayList = new ArrayList<>();
            for (String next : hashMap.keySet()) {
                arrayList.add(new b(next, hashMap.get(next)));
            }
            return arrayList;
        }

        /* access modifiers changed from: package-private */
        public HashMap<String, an.a<?, ?>> a() {
            HashMap<String, an.a<?, ?>> hashMap = new HashMap<>();
            int size = this.d.size();
            for (int i = 0; i < size; i++) {
                b bVar = this.d.get(i);
                hashMap.put(bVar.c, bVar.d);
            }
            return hashMap;
        }

        public int describeContents() {
            m mVar = a;
            return 0;
        }

        public void writeToParcel(Parcel parcel, int i) {
            m mVar = a;
            m.a(this, parcel, i);
        }
    }

    public static class b implements ae {
        public static final j a = new j();
        final int b;
        final String c;
        final an.a<?, ?> d;

        b(int i, String str, an.a<?, ?> aVar) {
            this.b = i;
            this.c = str;
            this.d = aVar;
        }

        b(String str, an.a<?, ?> aVar) {
            this.b = 1;
            this.c = str;
            this.d = aVar;
        }

        public int describeContents() {
            j jVar = a;
            return 0;
        }

        public void writeToParcel(Parcel parcel, int i) {
            j jVar = a;
            j.a(this, parcel, i);
        }
    }

    aq(int i, ArrayList<a> arrayList, String str) {
        this.b = i;
        this.c = a(arrayList);
        this.e = (String) bv.a(str);
        a();
    }

    private static HashMap<String, HashMap<String, an.a<?, ?>>> a(ArrayList<a> arrayList) {
        HashMap<String, HashMap<String, an.a<?, ?>>> hashMap = new HashMap<>();
        int size = arrayList.size();
        for (int i = 0; i < size; i++) {
            a aVar = arrayList.get(i);
            hashMap.put(aVar.c, aVar.a());
        }
        return hashMap;
    }

    public HashMap<String, an.a<?, ?>> a(String str) {
        return this.c.get(str);
    }

    public void a() {
        for (String str : this.c.keySet()) {
            HashMap hashMap = this.c.get(str);
            for (String str2 : hashMap.keySet()) {
                ((an.a) hashMap.get(str2)).a(this);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public int b() {
        return this.b;
    }

    /* access modifiers changed from: package-private */
    public ArrayList<a> c() {
        ArrayList<a> arrayList = new ArrayList<>();
        for (String next : this.c.keySet()) {
            arrayList.add(new a(next, this.c.get(next)));
        }
        return arrayList;
    }

    public String d() {
        return this.e;
    }

    public int describeContents() {
        l lVar = a;
        return 0;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (String next : this.c.keySet()) {
            sb.append(next).append(":\n");
            HashMap hashMap = this.c.get(next);
            for (String str : hashMap.keySet()) {
                sb.append("  ").append(str).append(": ");
                sb.append(hashMap.get(str));
            }
        }
        return sb.toString();
    }

    public void writeToParcel(Parcel parcel, int i) {
        l lVar = a;
        l.a(this, parcel, i);
    }
}
