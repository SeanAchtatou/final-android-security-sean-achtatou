package com.google.android.gms.maps.model;

import android.os.Parcel;
import com.google.android.gms.internal.ae;
import com.google.android.gms.internal.ah;
import com.google.android.gms.internal.w;

public final class Tile implements ae {
    public static final j a = new j();
    public final int b;
    public final int c;
    public final byte[] d;
    private final int e;

    Tile(int i, int i2, int i3, byte[] bArr) {
        this.e = i;
        this.b = i2;
        this.c = i3;
        this.d = bArr;
    }

    public Tile(int i, int i2, byte[] bArr) {
        this(1, i, i2, bArr);
    }

    public int a() {
        return this.e;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        if (w.a()) {
            ah.a(this, parcel, i);
        } else {
            j.a(this, parcel, i);
        }
    }
}
