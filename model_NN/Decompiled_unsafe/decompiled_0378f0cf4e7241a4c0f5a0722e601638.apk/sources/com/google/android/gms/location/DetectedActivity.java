package com.google.android.gms.location;

import android.os.Parcel;
import com.google.android.gms.internal.ae;

public class DetectedActivity implements ae {
    public static final b a = new b();
    int b = 1;
    int c;
    int d;

    private int a(int i) {
        if (i > 5) {
            return 4;
        }
        return i;
    }

    public int a() {
        return a(this.c);
    }

    public int describeContents() {
        return 0;
    }

    public String toString() {
        return "DetectedActivity [type=" + a() + ", confidence=" + this.d + "]";
    }

    public void writeToParcel(Parcel parcel, int i) {
        b.a(this, parcel, i);
    }
}
