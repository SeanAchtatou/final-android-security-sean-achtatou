package com.google.android.gms.games.multiplayer;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.games.Game;
import com.google.android.gms.games.GameEntity;
import com.google.android.gms.internal.bu;
import com.google.android.gms.internal.bv;
import java.util.ArrayList;

public final class InvitationEntity implements Invitation {
    public static final Parcelable.Creator<InvitationEntity> CREATOR = new Parcelable.Creator<InvitationEntity>() {
        /* renamed from: a */
        public InvitationEntity createFromParcel(Parcel parcel) {
            GameEntity createFromParcel = GameEntity.CREATOR.createFromParcel(parcel);
            String readString = parcel.readString();
            long readLong = parcel.readLong();
            int readInt = parcel.readInt();
            Participant createFromParcel2 = ParticipantEntity.CREATOR.createFromParcel(parcel);
            int readInt2 = parcel.readInt();
            ArrayList arrayList = new ArrayList(readInt2);
            for (int i = 0; i < readInt2; i++) {
                arrayList.add(ParticipantEntity.CREATOR.createFromParcel(parcel));
            }
            return new InvitationEntity(createFromParcel, readString, readLong, readInt, createFromParcel2, arrayList);
        }

        /* renamed from: a */
        public InvitationEntity[] newArray(int i) {
            return new InvitationEntity[i];
        }
    };
    private final GameEntity a;
    private final String b;
    private final long c;
    private final int d;
    private final Participant e;
    private final ArrayList<Participant> f;

    private InvitationEntity(GameEntity gameEntity, String str, long j, int i, Participant participant, ArrayList<Participant> arrayList) {
        this.a = gameEntity;
        this.b = str;
        this.c = j;
        this.d = i;
        this.e = participant;
        this.f = arrayList;
    }

    public InvitationEntity(Invitation invitation) {
        this.a = new GameEntity(invitation.b());
        this.b = invitation.c();
        this.c = invitation.e();
        this.d = invitation.f();
        String h = invitation.d().h();
        Participant participant = null;
        ArrayList<Participant> g = invitation.g();
        int size = g.size();
        this.f = new ArrayList<>(size);
        for (int i = 0; i < size; i++) {
            Participant participant2 = g.get(i);
            if (participant2.h().equals(h)) {
                participant = participant2;
            }
            this.f.add(participant2.a());
        }
        bv.a(participant, "Must have a valid inviter!");
        this.e = (Participant) participant.a();
    }

    public static int a(Invitation invitation) {
        return bu.a(invitation.b(), invitation.c(), Long.valueOf(invitation.e()), Integer.valueOf(invitation.f()), invitation.d(), invitation.g());
    }

    public static boolean a(Invitation invitation, Object obj) {
        if (!(obj instanceof Invitation)) {
            return false;
        }
        if (invitation == obj) {
            return true;
        }
        Invitation invitation2 = (Invitation) obj;
        return bu.a(invitation2.b(), invitation.b()) && bu.a(invitation2.c(), invitation.c()) && bu.a(Long.valueOf(invitation2.e()), Long.valueOf(invitation.e())) && bu.a(Integer.valueOf(invitation2.f()), Integer.valueOf(invitation.f())) && bu.a(invitation2.d(), invitation.d()) && bu.a(invitation2.g(), invitation.g());
    }

    public static String b(Invitation invitation) {
        return bu.a(invitation).a("Game", invitation.b()).a("InvitationId", invitation.c()).a("CreationTimestamp", Long.valueOf(invitation.e())).a("InvitationType", Integer.valueOf(invitation.f())).a("Inviter", invitation.d()).a("Participants", invitation.g()).toString();
    }

    public Game b() {
        return this.a;
    }

    public String c() {
        return this.b;
    }

    public Participant d() {
        return this.e;
    }

    public int describeContents() {
        return 0;
    }

    public long e() {
        return this.c;
    }

    public boolean equals(Object obj) {
        return a(this, obj);
    }

    public int f() {
        return this.d;
    }

    public ArrayList<Participant> g() {
        return this.f;
    }

    /* renamed from: h */
    public Invitation a() {
        return this;
    }

    public int hashCode() {
        return a(this);
    }

    public String toString() {
        return b(this);
    }

    public void writeToParcel(Parcel parcel, int i) {
        this.a.writeToParcel(parcel, i);
        parcel.writeString(this.b);
        parcel.writeLong(this.c);
        parcel.writeInt(this.d);
        this.e.writeToParcel(parcel, i);
        int size = this.f.size();
        parcel.writeInt(size);
        for (int i2 = 0; i2 < size; i2++) {
            this.f.get(i2).writeToParcel(parcel, i);
        }
    }
}
