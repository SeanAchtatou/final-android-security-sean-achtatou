package com.google.android.gms.maps.model;

import android.os.IBinder;
import android.os.Parcel;
import com.google.android.gms.internal.ae;
import com.google.android.gms.internal.s;
import com.google.android.gms.internal.w;
import com.google.android.gms.internal.z;

public final class GroundOverlayOptions implements ae {
    public static final d a = new d();
    private final int b;
    private a c;
    private LatLng d;
    private float e;
    private float f;
    private LatLngBounds g;
    private float h;
    private float i;
    private boolean j;
    private float k;
    private float l;
    private float m;

    public GroundOverlayOptions() {
        this.j = true;
        this.k = 0.0f;
        this.l = 0.5f;
        this.m = 0.5f;
        this.b = 1;
    }

    GroundOverlayOptions(int i2, IBinder iBinder, LatLng latLng, float f2, float f3, LatLngBounds latLngBounds, float f4, float f5, boolean z, float f6, float f7, float f8) {
        this.j = true;
        this.k = 0.0f;
        this.l = 0.5f;
        this.m = 0.5f;
        this.b = i2;
        this.c = new a(s.a.a(iBinder));
        this.d = latLng;
        this.e = f2;
        this.f = f3;
        this.g = latLngBounds;
        this.h = f4;
        this.i = f5;
        this.j = z;
        this.k = f6;
        this.l = f7;
        this.m = f8;
    }

    public IBinder a() {
        return this.c.a().asBinder();
    }

    public int b() {
        return this.b;
    }

    public LatLng c() {
        return this.d;
    }

    public float d() {
        return this.e;
    }

    public int describeContents() {
        return 0;
    }

    public float e() {
        return this.f;
    }

    public LatLngBounds f() {
        return this.g;
    }

    public float g() {
        return this.h;
    }

    public float h() {
        return this.i;
    }

    public float i() {
        return this.k;
    }

    public float j() {
        return this.l;
    }

    public float k() {
        return this.m;
    }

    public boolean l() {
        return this.j;
    }

    public void writeToParcel(Parcel parcel, int i2) {
        if (w.a()) {
            z.a(this, parcel, i2);
        } else {
            d.a(this, parcel, i2);
        }
    }
}
