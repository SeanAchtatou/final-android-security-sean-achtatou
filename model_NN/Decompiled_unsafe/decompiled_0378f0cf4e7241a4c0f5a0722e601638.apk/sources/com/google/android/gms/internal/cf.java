package com.google.android.gms.internal;

import android.os.Parcel;

public class cf implements ae {
    public static final t a = new t();
    private final int b;
    private final String c;
    private final long d;
    private final short e;
    private final double f;
    private final double g;
    private final float h;
    private final int i;

    public cf(int i2, String str, int i3, short s, double d2, double d3, float f2, long j) {
        a(str);
        a(f2);
        a(d2, d3);
        int a2 = a(i3);
        this.b = i2;
        this.e = s;
        this.c = str;
        this.f = d2;
        this.g = d3;
        this.h = f2;
        this.d = j;
        this.i = a2;
    }

    private static int a(int i2) {
        int i3 = i2 & 3;
        if (i3 != 0) {
            return i3;
        }
        throw new IllegalArgumentException("No supported transition specified: " + i2);
    }

    private static void a(double d2, double d3) {
        if (d2 > 90.0d || d2 < -90.0d) {
            throw new IllegalArgumentException("invalid latitude: " + d2);
        } else if (d3 > 180.0d || d3 < -180.0d) {
            throw new IllegalArgumentException("invalid longitude: " + d3);
        }
    }

    private static void a(float f2) {
        if (f2 <= 0.0f) {
            throw new IllegalArgumentException("invalid radius: " + f2);
        }
    }

    private static void a(String str) {
        if (str == null || str.length() > 100) {
            throw new IllegalArgumentException("requestId is null or too long: " + str);
        }
    }

    private static String b(int i2) {
        switch (i2) {
            case 1:
                return "CIRCLE";
            default:
                return null;
        }
    }

    public int a() {
        return this.b;
    }

    public short b() {
        return this.e;
    }

    public double c() {
        return this.f;
    }

    public double d() {
        return this.g;
    }

    public int describeContents() {
        t tVar = a;
        return 0;
    }

    public float e() {
        return this.h;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof cf)) {
            return false;
        }
        cf cfVar = (cf) obj;
        if (this.h != cfVar.h) {
            return false;
        }
        if (this.f != cfVar.f) {
            return false;
        }
        if (this.g != cfVar.g) {
            return false;
        }
        return this.e == cfVar.e;
    }

    public String f() {
        return this.c;
    }

    public long g() {
        return this.d;
    }

    public int h() {
        return this.i;
    }

    public int hashCode() {
        long doubleToLongBits = Double.doubleToLongBits(this.f);
        long doubleToLongBits2 = Double.doubleToLongBits(this.g);
        return ((((((((((int) (doubleToLongBits ^ (doubleToLongBits >>> 32))) + 31) * 31) + ((int) (doubleToLongBits2 ^ (doubleToLongBits2 >>> 32)))) * 31) + Float.floatToIntBits(this.h)) * 31) + this.e) * 31) + this.i;
    }

    public String toString() {
        return String.format("Geofence[%s id:%s transitions:%d %.6f, %.6f %.0fm, @%d]", b(this.e), this.c, Integer.valueOf(this.i), Double.valueOf(this.f), Double.valueOf(this.g), Float.valueOf(this.h), Long.valueOf(this.d));
    }

    public void writeToParcel(Parcel parcel, int i2) {
        t tVar = a;
        t.a(this, parcel, i2);
    }
}
