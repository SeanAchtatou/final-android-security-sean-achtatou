package com.google.android.gms.common.images;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.ParcelFileDescriptor;
import android.os.ResultReceiver;
import android.util.Log;
import com.google.android.gms.internal.d;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Map;

public final class ImageManager {
    /* access modifiers changed from: private */
    public final Context a;
    /* access modifiers changed from: private */
    public final d<Uri, WeakReference<Drawable.ConstantState>> b;
    /* access modifiers changed from: private */
    public final Map<Uri, ImageReceiver> c;

    public final class ImageReceiver extends ResultReceiver {
        final /* synthetic */ ImageManager a;
        private final Uri b;
        private final ArrayList<a> c;

        public void onReceiveResult(int i, Bundle bundle) {
            BitmapDrawable bitmapDrawable = null;
            ParcelFileDescriptor parcelFileDescriptor = (ParcelFileDescriptor) bundle.getParcelable("com.google.android.gms.extra.fileDescriptor");
            if (parcelFileDescriptor != null) {
                Bitmap decodeFileDescriptor = BitmapFactory.decodeFileDescriptor(parcelFileDescriptor.getFileDescriptor());
                try {
                    parcelFileDescriptor.close();
                } catch (IOException e) {
                    Log.e("ImageManager", "closed failed", e);
                }
                BitmapDrawable bitmapDrawable2 = new BitmapDrawable(this.a.a.getResources(), decodeFileDescriptor);
                this.a.b.a(this.b, new WeakReference(bitmapDrawable2.getConstantState()));
                bitmapDrawable = bitmapDrawable2;
            }
            this.a.c.remove(this.b);
            int size = this.c.size();
            for (int i2 = 0; i2 < size; i2++) {
                this.c.get(i2).a(this.b, bitmapDrawable);
            }
        }
    }

    abstract class a {
        protected final int a;

        public abstract void a(Uri uri, Drawable drawable);

        public int hashCode() {
            return this.a;
        }
    }
}
