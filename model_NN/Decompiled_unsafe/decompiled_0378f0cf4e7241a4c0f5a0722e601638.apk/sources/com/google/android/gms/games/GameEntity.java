package com.google.android.gms.games;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.internal.bu;

public final class GameEntity implements Game {
    public static final Parcelable.Creator<GameEntity> CREATOR = new Parcelable.Creator<GameEntity>() {
        /* renamed from: a */
        public GameEntity createFromParcel(Parcel parcel) {
            String readString = parcel.readString();
            String readString2 = parcel.readString();
            String readString3 = parcel.readString();
            String readString4 = parcel.readString();
            String readString5 = parcel.readString();
            String readString6 = parcel.readString();
            String readString7 = parcel.readString();
            Uri parse = readString7 == null ? null : Uri.parse(readString7);
            String readString8 = parcel.readString();
            Uri parse2 = readString8 == null ? null : Uri.parse(readString8);
            String readString9 = parcel.readString();
            return new GameEntity(readString, readString2, readString3, readString4, readString5, readString6, parse, parse2, readString9 == null ? null : Uri.parse(readString9), parcel.readInt() > 0, parcel.readInt() > 0, parcel.readString(), parcel.readInt(), parcel.readInt(), parcel.readInt());
        }

        /* renamed from: a */
        public GameEntity[] newArray(int i) {
            return new GameEntity[i];
        }
    };
    private final String a;
    private final String b;
    private final String c;
    private final String d;
    private final String e;
    private final String f;
    private final Uri g;
    private final Uri h;
    private final Uri i;
    private final boolean j;
    private final boolean k;
    private final String l;
    private final int m;
    private final int n;
    private final int o;

    public GameEntity(Game game) {
        this.a = game.b();
        this.c = game.d();
        this.d = game.e();
        this.e = game.f();
        this.f = game.g();
        this.b = game.c();
        this.g = game.h();
        this.h = game.i();
        this.i = game.j();
        this.j = game.k();
        this.k = game.l();
        this.l = game.m();
        this.m = game.n();
        this.n = game.o();
        this.o = game.p();
    }

    private GameEntity(String str, String str2, String str3, String str4, String str5, String str6, Uri uri, Uri uri2, Uri uri3, boolean z, boolean z2, String str7, int i2, int i3, int i4) {
        this.a = str;
        this.b = str2;
        this.c = str3;
        this.d = str4;
        this.e = str5;
        this.f = str6;
        this.g = uri;
        this.h = uri2;
        this.i = uri3;
        this.j = z;
        this.k = z2;
        this.l = str7;
        this.m = i2;
        this.n = i3;
        this.o = i4;
    }

    public static int a(Game game) {
        return bu.a(game.b(), game.c(), game.d(), game.e(), game.f(), game.g(), game.h(), game.i(), game.j(), Boolean.valueOf(game.k()), Boolean.valueOf(game.l()), game.m(), Integer.valueOf(game.n()), Integer.valueOf(game.o()), Integer.valueOf(game.p()));
    }

    public static boolean a(Game game, Object obj) {
        if (!(obj instanceof Game)) {
            return false;
        }
        if (game == obj) {
            return true;
        }
        Game game2 = (Game) obj;
        return bu.a(game2.b(), game.b()) && bu.a(game2.c(), game.c()) && bu.a(game2.d(), game.d()) && bu.a(game2.e(), game.e()) && bu.a(game2.f(), game.f()) && bu.a(game2.g(), game.g()) && bu.a(game2.h(), game.h()) && bu.a(game2.i(), game.i()) && bu.a(game2.j(), game.j()) && bu.a(Boolean.valueOf(game2.k()), Boolean.valueOf(game.k())) && bu.a(Boolean.valueOf(game2.l()), Boolean.valueOf(game.l())) && bu.a(game2.m(), game.m()) && bu.a(Integer.valueOf(game2.n()), Integer.valueOf(game.n())) && bu.a(Integer.valueOf(game2.o()), Integer.valueOf(game.o())) && bu.a(Integer.valueOf(game2.p()), Integer.valueOf(game.p()));
    }

    public static String b(Game game) {
        return bu.a(game).a("ApplicationId", game.b()).a("DisplayName", game.c()).a("PrimaryCategory", game.d()).a("SecondaryCategory", game.e()).a("Description", game.f()).a("DeveloperName", game.g()).a("IconImageUri", game.h()).a("HiResImageUri", game.i()).a("FeaturedImageUri", game.j()).a("PlayEnabledGame", Boolean.valueOf(game.k())).a("InstanceInstalled", Boolean.valueOf(game.l())).a("InstancePackageName", game.m()).a("GameplayAclStatus", Integer.valueOf(game.n())).a("AchievementTotalCount", Integer.valueOf(game.o())).a("LeaderboardCount", Integer.valueOf(game.p())).toString();
    }

    public String b() {
        return this.a;
    }

    public String c() {
        return this.b;
    }

    public String d() {
        return this.c;
    }

    public int describeContents() {
        return 0;
    }

    public String e() {
        return this.d;
    }

    public boolean equals(Object obj) {
        return a(this, obj);
    }

    public String f() {
        return this.e;
    }

    public String g() {
        return this.f;
    }

    public Uri h() {
        return this.g;
    }

    public int hashCode() {
        return a(this);
    }

    public Uri i() {
        return this.h;
    }

    public Uri j() {
        return this.i;
    }

    public boolean k() {
        return this.j;
    }

    public boolean l() {
        return this.k;
    }

    public String m() {
        return this.l;
    }

    public int n() {
        return this.m;
    }

    public int o() {
        return this.n;
    }

    public int p() {
        return this.o;
    }

    /* renamed from: q */
    public Game a() {
        return this;
    }

    public String toString() {
        return b(this);
    }

    public void writeToParcel(Parcel parcel, int i2) {
        int i3 = 1;
        String str = null;
        parcel.writeString(this.a);
        parcel.writeString(this.b);
        parcel.writeString(this.c);
        parcel.writeString(this.d);
        parcel.writeString(this.e);
        parcel.writeString(this.f);
        parcel.writeString(this.g == null ? null : this.g.toString());
        parcel.writeString(this.h == null ? null : this.h.toString());
        if (this.i != null) {
            str = this.i.toString();
        }
        parcel.writeString(str);
        parcel.writeInt(this.j ? 1 : 0);
        if (!this.k) {
            i3 = 0;
        }
        parcel.writeInt(i3);
        parcel.writeString(this.l);
        parcel.writeInt(this.m);
        parcel.writeInt(this.n);
        parcel.writeInt(this.o);
    }
}
