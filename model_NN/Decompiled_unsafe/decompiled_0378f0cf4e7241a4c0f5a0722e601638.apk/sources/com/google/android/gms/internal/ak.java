package com.google.android.gms.internal;

import android.os.Parcel;
import com.google.android.gms.internal.an;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

public final class ak implements ae, an.b<String, Integer> {
    public static final g a = new g();
    private final int b;
    private final HashMap<String, Integer> c;
    private final HashMap<Integer, String> d;
    private final ArrayList<a> e;

    public static final class a implements ae {
        public static final h a = new h();
        final int b;
        final String c;
        final int d;

        a(int i, String str, int i2) {
            this.b = i;
            this.c = str;
            this.d = i2;
        }

        a(String str, int i) {
            this.b = 1;
            this.c = str;
            this.d = i;
        }

        public int describeContents() {
            h hVar = a;
            return 0;
        }

        public void writeToParcel(Parcel parcel, int i) {
            h hVar = a;
            h.a(this, parcel, i);
        }
    }

    public ak() {
        this.b = 1;
        this.c = new HashMap<>();
        this.d = new HashMap<>();
        this.e = null;
    }

    ak(int i, ArrayList<a> arrayList) {
        this.b = i;
        this.c = new HashMap<>();
        this.d = new HashMap<>();
        this.e = null;
        a(arrayList);
    }

    private void a(ArrayList<a> arrayList) {
        Iterator<a> it = arrayList.iterator();
        while (it.hasNext()) {
            a next = it.next();
            a(next.c, next.d);
        }
    }

    /* access modifiers changed from: package-private */
    public int a() {
        return this.b;
    }

    public ak a(String str, int i) {
        this.c.put(str, Integer.valueOf(i));
        this.d.put(Integer.valueOf(i), str);
        return this;
    }

    public String a(Integer num) {
        return this.d.get(num);
    }

    /* access modifiers changed from: package-private */
    public ArrayList<a> b() {
        ArrayList<a> arrayList = new ArrayList<>();
        for (String next : this.c.keySet()) {
            arrayList.add(new a(next, this.c.get(next).intValue()));
        }
        return arrayList;
    }

    public int c() {
        return 7;
    }

    public int d() {
        return 0;
    }

    public int describeContents() {
        g gVar = a;
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        g gVar = a;
        g.a(this, parcel, i);
    }
}
