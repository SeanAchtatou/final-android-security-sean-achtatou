package com.google.android.gms.maps.model;

import android.os.Parcel;
import com.google.android.gms.internal.ab;
import com.google.android.gms.internal.ae;
import com.google.android.gms.internal.w;

public final class LatLng implements ae {
    public static final f a = new f();
    public final double b;
    public final double c;
    private final int d;

    LatLng(int i, double d2, double d3) {
        this.d = i;
        if (-180.0d > d3 || d3 >= 180.0d) {
            this.c = ((((d3 - 180.0d) % 360.0d) + 360.0d) % 360.0d) - 180.0d;
        } else {
            this.c = d3;
        }
        this.b = Math.max(-90.0d, Math.min(90.0d, d2));
    }

    public int a() {
        return this.d;
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof LatLng)) {
            return false;
        }
        LatLng latLng = (LatLng) obj;
        return Double.doubleToLongBits(this.b) == Double.doubleToLongBits(latLng.b) && Double.doubleToLongBits(this.c) == Double.doubleToLongBits(latLng.c);
    }

    public int hashCode() {
        long doubleToLongBits = Double.doubleToLongBits(this.b);
        long doubleToLongBits2 = Double.doubleToLongBits(this.c);
        return ((((int) (doubleToLongBits ^ (doubleToLongBits >>> 32))) + 31) * 31) + ((int) (doubleToLongBits2 ^ (doubleToLongBits2 >>> 32)));
    }

    public String toString() {
        return "lat/lng: (" + this.b + "," + this.c + ")";
    }

    public void writeToParcel(Parcel parcel, int i) {
        if (w.a()) {
            ab.a(this, parcel, i);
        } else {
            f.a(this, parcel, i);
        }
    }
}
