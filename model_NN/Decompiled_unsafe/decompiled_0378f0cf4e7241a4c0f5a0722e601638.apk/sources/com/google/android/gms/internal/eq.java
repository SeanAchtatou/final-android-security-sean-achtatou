package com.google.android.gms.internal;

import android.os.Parcel;
import com.facebook.a.e;
import com.google.android.gms.internal.an;
import com.google.android.gms.plus.a.b.a;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public final class eq extends an implements ae, com.google.android.gms.plus.a.b.a {
    public static final bk a = new bk();
    private static final HashMap<String, an.a<?, ?>> b = new HashMap<>();
    private int A;
    private String B;
    private String C;
    private List<i> D;
    private boolean E;
    private final Set<Integer> c;
    private final int d;
    private String e;
    private a f;
    private String g;
    private String h;
    private int i;
    private b j;
    private String k;
    private String l;
    private List<c> m;
    private String n;
    private int o;
    private boolean p;
    private String q;
    private d r;
    private boolean s;
    private String t;
    private e u;
    private String v;
    private int w;
    private List<g> x;
    private List<h> y;
    private int z;

    public static final class a extends an implements ae, a.C0030a {
        public static final ba a = new ba();
        private static final HashMap<String, an.a<?, ?>> b = new HashMap<>();
        private final Set<Integer> c;
        private final int d;
        private int e;
        private int f;

        static {
            b.put("max", an.a.a("max", 2));
            b.put("min", an.a.a("min", 3));
        }

        public a() {
            this.d = 1;
            this.c = new HashSet();
        }

        a(Set<Integer> set, int i, int i2, int i3) {
            this.c = set;
            this.d = i;
            this.e = i2;
            this.f = i3;
        }

        /* access modifiers changed from: protected */
        public Object a(String str) {
            return null;
        }

        /* access modifiers changed from: protected */
        public boolean a(an.a aVar) {
            return this.c.contains(Integer.valueOf(aVar.g()));
        }

        /* access modifiers changed from: protected */
        public Object b(an.a aVar) {
            switch (aVar.g()) {
                case 2:
                    return Integer.valueOf(this.e);
                case 3:
                    return Integer.valueOf(this.f);
                default:
                    throw new IllegalStateException("Unknown safe parcelable id=" + aVar.g());
            }
        }

        public HashMap<String, an.a<?, ?>> b() {
            return b;
        }

        /* access modifiers changed from: protected */
        public boolean b(String str) {
            return false;
        }

        public int describeContents() {
            ba baVar = a;
            return 0;
        }

        /* access modifiers changed from: package-private */
        public Set<Integer> e() {
            return this.c;
        }

        /* access modifiers changed from: package-private */
        public int f() {
            return this.d;
        }

        public int g() {
            return this.e;
        }

        public int h() {
            return this.f;
        }

        /* renamed from: i */
        public a a() {
            return this;
        }

        public void writeToParcel(Parcel parcel, int i) {
            ba baVar = a;
            ba.a(this, parcel, i);
        }
    }

    public static final class b extends an implements ae, a.b {
        public static final bb a = new bb();
        private static final HashMap<String, an.a<?, ?>> b = new HashMap<>();
        private final Set<Integer> c;
        private final int d;
        private a e;
        private C0027b f;
        private int g;

        public static final class a extends an implements ae, a.b.C0031a {
            public static final bc a = new bc();
            private static final HashMap<String, an.a<?, ?>> b = new HashMap<>();
            private final Set<Integer> c;
            private final int d;
            private int e;
            private int f;

            static {
                b.put("leftImageOffset", an.a.a("leftImageOffset", 2));
                b.put("topImageOffset", an.a.a("topImageOffset", 3));
            }

            public a() {
                this.d = 1;
                this.c = new HashSet();
            }

            a(Set<Integer> set, int i, int i2, int i3) {
                this.c = set;
                this.d = i;
                this.e = i2;
                this.f = i3;
            }

            /* access modifiers changed from: protected */
            public Object a(String str) {
                return null;
            }

            /* access modifiers changed from: protected */
            public boolean a(an.a aVar) {
                return this.c.contains(Integer.valueOf(aVar.g()));
            }

            /* access modifiers changed from: protected */
            public Object b(an.a aVar) {
                switch (aVar.g()) {
                    case 2:
                        return Integer.valueOf(this.e);
                    case 3:
                        return Integer.valueOf(this.f);
                    default:
                        throw new IllegalStateException("Unknown safe parcelable id=" + aVar.g());
                }
            }

            public HashMap<String, an.a<?, ?>> b() {
                return b;
            }

            /* access modifiers changed from: protected */
            public boolean b(String str) {
                return false;
            }

            public int describeContents() {
                bc bcVar = a;
                return 0;
            }

            /* access modifiers changed from: package-private */
            public Set<Integer> e() {
                return this.c;
            }

            public boolean equals(Object obj) {
                if (!(obj instanceof a)) {
                    return false;
                }
                if (this == obj) {
                    return true;
                }
                a aVar = (a) obj;
                for (an.a next : b.values()) {
                    if (a(next)) {
                        if (!aVar.a(next)) {
                            return false;
                        }
                        if (!b(next).equals(aVar.b(next))) {
                            return false;
                        }
                    } else if (aVar.a(next)) {
                        return false;
                    }
                }
                return true;
            }

            /* access modifiers changed from: package-private */
            public int f() {
                return this.d;
            }

            public int g() {
                return this.e;
            }

            public int h() {
                return this.f;
            }

            public int hashCode() {
                int i = 0;
                Iterator<an.a<?, ?>> it = b.values().iterator();
                while (true) {
                    int i2 = i;
                    if (!it.hasNext()) {
                        return i2;
                    }
                    an.a next = it.next();
                    if (a(next)) {
                        i = b(next).hashCode() + i2 + next.g();
                    } else {
                        i = i2;
                    }
                }
            }

            /* renamed from: i */
            public a a() {
                return this;
            }

            public void writeToParcel(Parcel parcel, int i) {
                bc bcVar = a;
                bc.a(this, parcel, i);
            }
        }

        /* renamed from: com.google.android.gms.internal.eq$b$b  reason: collision with other inner class name */
        public static final class C0027b extends an implements ae, a.b.C0032b {
            public static final bd a = new bd();
            private static final HashMap<String, an.a<?, ?>> b = new HashMap<>();
            private final Set<Integer> c;
            private final int d;
            private int e;
            private String f;
            private int g;

            static {
                b.put("height", an.a.a("height", 2));
                b.put("url", an.a.d("url", 3));
                b.put("width", an.a.a("width", 4));
            }

            public C0027b() {
                this.d = 1;
                this.c = new HashSet();
            }

            C0027b(Set<Integer> set, int i, int i2, String str, int i3) {
                this.c = set;
                this.d = i;
                this.e = i2;
                this.f = str;
                this.g = i3;
            }

            /* access modifiers changed from: protected */
            public Object a(String str) {
                return null;
            }

            /* access modifiers changed from: protected */
            public boolean a(an.a aVar) {
                return this.c.contains(Integer.valueOf(aVar.g()));
            }

            /* access modifiers changed from: protected */
            public Object b(an.a aVar) {
                switch (aVar.g()) {
                    case 2:
                        return Integer.valueOf(this.e);
                    case 3:
                        return this.f;
                    case e.g.com_facebook_picker_fragment_done_button_text:
                        return Integer.valueOf(this.g);
                    default:
                        throw new IllegalStateException("Unknown safe parcelable id=" + aVar.g());
                }
            }

            public HashMap<String, an.a<?, ?>> b() {
                return b;
            }

            /* access modifiers changed from: protected */
            public boolean b(String str) {
                return false;
            }

            public int describeContents() {
                bd bdVar = a;
                return 0;
            }

            /* access modifiers changed from: package-private */
            public Set<Integer> e() {
                return this.c;
            }

            public boolean equals(Object obj) {
                if (!(obj instanceof C0027b)) {
                    return false;
                }
                if (this == obj) {
                    return true;
                }
                C0027b bVar = (C0027b) obj;
                for (an.a next : b.values()) {
                    if (a(next)) {
                        if (!bVar.a(next)) {
                            return false;
                        }
                        if (!b(next).equals(bVar.b(next))) {
                            return false;
                        }
                    } else if (bVar.a(next)) {
                        return false;
                    }
                }
                return true;
            }

            /* access modifiers changed from: package-private */
            public int f() {
                return this.d;
            }

            public int g() {
                return this.e;
            }

            public String h() {
                return this.f;
            }

            public int hashCode() {
                int i = 0;
                Iterator<an.a<?, ?>> it = b.values().iterator();
                while (true) {
                    int i2 = i;
                    if (!it.hasNext()) {
                        return i2;
                    }
                    an.a next = it.next();
                    if (a(next)) {
                        i = b(next).hashCode() + i2 + next.g();
                    } else {
                        i = i2;
                    }
                }
            }

            public int i() {
                return this.g;
            }

            /* renamed from: j */
            public C0027b a() {
                return this;
            }

            public void writeToParcel(Parcel parcel, int i) {
                bd bdVar = a;
                bd.a(this, parcel, i);
            }
        }

        static {
            b.put("coverInfo", an.a.a("coverInfo", 2, a.class));
            b.put("coverPhoto", an.a.a("coverPhoto", 3, C0027b.class));
            b.put("layout", an.a.a("layout", 4, new ak().a("banner", 0), false));
        }

        public b() {
            this.d = 1;
            this.c = new HashSet();
        }

        b(Set<Integer> set, int i, a aVar, C0027b bVar, int i2) {
            this.c = set;
            this.d = i;
            this.e = aVar;
            this.f = bVar;
            this.g = i2;
        }

        /* access modifiers changed from: protected */
        public Object a(String str) {
            return null;
        }

        /* access modifiers changed from: protected */
        public boolean a(an.a aVar) {
            return this.c.contains(Integer.valueOf(aVar.g()));
        }

        /* access modifiers changed from: protected */
        public Object b(an.a aVar) {
            switch (aVar.g()) {
                case 2:
                    return this.e;
                case 3:
                    return this.f;
                case e.g.com_facebook_picker_fragment_done_button_text:
                    return Integer.valueOf(this.g);
                default:
                    throw new IllegalStateException("Unknown safe parcelable id=" + aVar.g());
            }
        }

        public HashMap<String, an.a<?, ?>> b() {
            return b;
        }

        /* access modifiers changed from: protected */
        public boolean b(String str) {
            return false;
        }

        public int describeContents() {
            bb bbVar = a;
            return 0;
        }

        /* access modifiers changed from: package-private */
        public Set<Integer> e() {
            return this.c;
        }

        public boolean equals(Object obj) {
            if (!(obj instanceof b)) {
                return false;
            }
            if (this == obj) {
                return true;
            }
            b bVar = (b) obj;
            for (an.a next : b.values()) {
                if (a(next)) {
                    if (!bVar.a(next)) {
                        return false;
                    }
                    if (!b(next).equals(bVar.b(next))) {
                        return false;
                    }
                } else if (bVar.a(next)) {
                    return false;
                }
            }
            return true;
        }

        /* access modifiers changed from: package-private */
        public int f() {
            return this.d;
        }

        /* access modifiers changed from: package-private */
        public a g() {
            return this.e;
        }

        /* access modifiers changed from: package-private */
        public C0027b h() {
            return this.f;
        }

        public int hashCode() {
            int i = 0;
            Iterator<an.a<?, ?>> it = b.values().iterator();
            while (true) {
                int i2 = i;
                if (!it.hasNext()) {
                    return i2;
                }
                an.a next = it.next();
                if (a(next)) {
                    i = b(next).hashCode() + i2 + next.g();
                } else {
                    i = i2;
                }
            }
        }

        public int i() {
            return this.g;
        }

        /* renamed from: j */
        public b a() {
            return this;
        }

        public void writeToParcel(Parcel parcel, int i) {
            bb bbVar = a;
            bb.a(this, parcel, i);
        }
    }

    public static final class c extends an implements ae, a.c {
        public static final be a = new be();
        private static final HashMap<String, an.a<?, ?>> b = new HashMap<>();
        private final Set<Integer> c;
        private final int d;
        private boolean e;
        private int f;
        private String g;

        static {
            b.put("primary", an.a.c("primary", 2));
            b.put("type", an.a.a("type", 3, new ak().a("home", 0).a("work", 1).a("other", 2), false));
            b.put("value", an.a.d("value", 4));
        }

        public c() {
            this.d = 1;
            this.c = new HashSet();
        }

        c(Set<Integer> set, int i, boolean z, int i2, String str) {
            this.c = set;
            this.d = i;
            this.e = z;
            this.f = i2;
            this.g = str;
        }

        /* access modifiers changed from: protected */
        public Object a(String str) {
            return null;
        }

        /* access modifiers changed from: protected */
        public boolean a(an.a aVar) {
            return this.c.contains(Integer.valueOf(aVar.g()));
        }

        /* access modifiers changed from: protected */
        public Object b(an.a aVar) {
            switch (aVar.g()) {
                case 2:
                    return Boolean.valueOf(this.e);
                case 3:
                    return Integer.valueOf(this.f);
                case e.g.com_facebook_picker_fragment_done_button_text:
                    return this.g;
                default:
                    throw new IllegalStateException("Unknown safe parcelable id=" + aVar.g());
            }
        }

        public HashMap<String, an.a<?, ?>> b() {
            return b;
        }

        /* access modifiers changed from: protected */
        public boolean b(String str) {
            return false;
        }

        public int describeContents() {
            be beVar = a;
            return 0;
        }

        /* access modifiers changed from: package-private */
        public Set<Integer> e() {
            return this.c;
        }

        public boolean equals(Object obj) {
            if (!(obj instanceof c)) {
                return false;
            }
            if (this == obj) {
                return true;
            }
            c cVar = (c) obj;
            for (an.a next : b.values()) {
                if (a(next)) {
                    if (!cVar.a(next)) {
                        return false;
                    }
                    if (!b(next).equals(cVar.b(next))) {
                        return false;
                    }
                } else if (cVar.a(next)) {
                    return false;
                }
            }
            return true;
        }

        /* access modifiers changed from: package-private */
        public int f() {
            return this.d;
        }

        public boolean g() {
            return this.e;
        }

        public int h() {
            return this.f;
        }

        public int hashCode() {
            int i = 0;
            Iterator<an.a<?, ?>> it = b.values().iterator();
            while (true) {
                int i2 = i;
                if (!it.hasNext()) {
                    return i2;
                }
                an.a next = it.next();
                if (a(next)) {
                    i = b(next).hashCode() + i2 + next.g();
                } else {
                    i = i2;
                }
            }
        }

        public String i() {
            return this.g;
        }

        /* renamed from: j */
        public c a() {
            return this;
        }

        public void writeToParcel(Parcel parcel, int i) {
            be beVar = a;
            be.a(this, parcel, i);
        }
    }

    public static final class d extends an implements ae, a.d {
        public static final bh a = new bh();
        private static final HashMap<String, an.a<?, ?>> b = new HashMap<>();
        private final Set<Integer> c;
        private final int d;
        private String e;

        static {
            b.put("url", an.a.d("url", 2));
        }

        public d() {
            this.d = 1;
            this.c = new HashSet();
        }

        d(Set<Integer> set, int i, String str) {
            this.c = set;
            this.d = i;
            this.e = str;
        }

        /* access modifiers changed from: protected */
        public Object a(String str) {
            return null;
        }

        /* access modifiers changed from: protected */
        public boolean a(an.a aVar) {
            return this.c.contains(Integer.valueOf(aVar.g()));
        }

        /* access modifiers changed from: protected */
        public Object b(an.a aVar) {
            switch (aVar.g()) {
                case 2:
                    break;
                default:
                    throw new IllegalStateException("Unknown safe parcelable id=" + aVar.g());
            }
            return this.e;
        }

        public HashMap<String, an.a<?, ?>> b() {
            return b;
        }

        /* access modifiers changed from: protected */
        public boolean b(String str) {
            return false;
        }

        public int describeContents() {
            bh bhVar = a;
            return 0;
        }

        /* access modifiers changed from: package-private */
        public Set<Integer> e() {
            return this.c;
        }

        public boolean equals(Object obj) {
            if (!(obj instanceof d)) {
                return false;
            }
            if (this == obj) {
                return true;
            }
            d dVar = (d) obj;
            for (an.a next : b.values()) {
                if (a(next)) {
                    if (!dVar.a(next)) {
                        return false;
                    }
                    if (!b(next).equals(dVar.b(next))) {
                        return false;
                    }
                } else if (dVar.a(next)) {
                    return false;
                }
            }
            return true;
        }

        /* access modifiers changed from: package-private */
        public int f() {
            return this.d;
        }

        public String g() {
            return this.e;
        }

        /* renamed from: h */
        public d a() {
            return this;
        }

        public int hashCode() {
            int i = 0;
            Iterator<an.a<?, ?>> it = b.values().iterator();
            while (true) {
                int i2 = i;
                if (!it.hasNext()) {
                    return i2;
                }
                an.a next = it.next();
                if (a(next)) {
                    i = b(next).hashCode() + i2 + next.g();
                } else {
                    i = i2;
                }
            }
        }

        public void writeToParcel(Parcel parcel, int i) {
            bh bhVar = a;
            bh.a(this, parcel, i);
        }
    }

    public static final class e extends an implements ae, a.e {
        public static final bi a = new bi();
        private static final HashMap<String, an.a<?, ?>> b = new HashMap<>();
        private final Set<Integer> c;
        private final int d;
        private String e;
        private String f;
        private String g;
        private String h;
        private String i;
        private String j;

        static {
            b.put("familyName", an.a.d("familyName", 2));
            b.put("formatted", an.a.d("formatted", 3));
            b.put("givenName", an.a.d("givenName", 4));
            b.put("honorificPrefix", an.a.d("honorificPrefix", 5));
            b.put("honorificSuffix", an.a.d("honorificSuffix", 6));
            b.put("middleName", an.a.d("middleName", 7));
        }

        public e() {
            this.d = 1;
            this.c = new HashSet();
        }

        e(Set<Integer> set, int i2, String str, String str2, String str3, String str4, String str5, String str6) {
            this.c = set;
            this.d = i2;
            this.e = str;
            this.f = str2;
            this.g = str3;
            this.h = str4;
            this.i = str5;
            this.j = str6;
        }

        /* access modifiers changed from: protected */
        public Object a(String str) {
            return null;
        }

        /* access modifiers changed from: protected */
        public boolean a(an.a aVar) {
            return this.c.contains(Integer.valueOf(aVar.g()));
        }

        /* access modifiers changed from: protected */
        public Object b(an.a aVar) {
            switch (aVar.g()) {
                case 2:
                    return this.e;
                case 3:
                    return this.f;
                case e.g.com_facebook_picker_fragment_done_button_text:
                    return this.g;
                case e.g.com_facebook_picker_fragment_title_bar_background:
                    return this.h;
                case e.g.com_facebook_picker_fragment_done_button_background:
                    return this.i;
                case 7:
                    return this.j;
                default:
                    throw new IllegalStateException("Unknown safe parcelable id=" + aVar.g());
            }
        }

        public HashMap<String, an.a<?, ?>> b() {
            return b;
        }

        /* access modifiers changed from: protected */
        public boolean b(String str) {
            return false;
        }

        public int describeContents() {
            bi biVar = a;
            return 0;
        }

        /* access modifiers changed from: package-private */
        public Set<Integer> e() {
            return this.c;
        }

        public boolean equals(Object obj) {
            if (!(obj instanceof e)) {
                return false;
            }
            if (this == obj) {
                return true;
            }
            e eVar = (e) obj;
            for (an.a next : b.values()) {
                if (a(next)) {
                    if (!eVar.a(next)) {
                        return false;
                    }
                    if (!b(next).equals(eVar.b(next))) {
                        return false;
                    }
                } else if (eVar.a(next)) {
                    return false;
                }
            }
            return true;
        }

        /* access modifiers changed from: package-private */
        public int f() {
            return this.d;
        }

        public String g() {
            return this.e;
        }

        public String h() {
            return this.f;
        }

        public int hashCode() {
            int i2 = 0;
            Iterator<an.a<?, ?>> it = b.values().iterator();
            while (true) {
                int i3 = i2;
                if (!it.hasNext()) {
                    return i3;
                }
                an.a next = it.next();
                if (a(next)) {
                    i2 = b(next).hashCode() + i3 + next.g();
                } else {
                    i2 = i3;
                }
            }
        }

        public String i() {
            return this.g;
        }

        public String j() {
            return this.h;
        }

        public String k() {
            return this.i;
        }

        public String l() {
            return this.j;
        }

        /* renamed from: m */
        public e a() {
            return this;
        }

        public void writeToParcel(Parcel parcel, int i2) {
            bi biVar = a;
            bi.a(this, parcel, i2);
        }
    }

    public static final class g extends an implements ae, a.f {
        public static final bj a = new bj();
        private static final HashMap<String, an.a<?, ?>> b = new HashMap<>();
        private final Set<Integer> c;
        private final int d;
        private String e;
        private String f;
        private String g;
        private String h;
        private String i;
        private boolean j;
        private String k;
        private String l;
        private int m;

        static {
            b.put("department", an.a.d("department", 2));
            b.put("description", an.a.d("description", 3));
            b.put("endDate", an.a.d("endDate", 4));
            b.put("location", an.a.d("location", 5));
            b.put("name", an.a.d("name", 6));
            b.put("primary", an.a.c("primary", 7));
            b.put("startDate", an.a.d("startDate", 8));
            b.put("title", an.a.d("title", 9));
            b.put("type", an.a.a("type", 10, new ak().a("work", 0).a("school", 1), false));
        }

        public g() {
            this.d = 1;
            this.c = new HashSet();
        }

        g(Set<Integer> set, int i2, String str, String str2, String str3, String str4, String str5, boolean z, String str6, String str7, int i3) {
            this.c = set;
            this.d = i2;
            this.e = str;
            this.f = str2;
            this.g = str3;
            this.h = str4;
            this.i = str5;
            this.j = z;
            this.k = str6;
            this.l = str7;
            this.m = i3;
        }

        /* access modifiers changed from: protected */
        public Object a(String str) {
            return null;
        }

        /* access modifiers changed from: protected */
        public boolean a(an.a aVar) {
            return this.c.contains(Integer.valueOf(aVar.g()));
        }

        /* access modifiers changed from: protected */
        public Object b(an.a aVar) {
            switch (aVar.g()) {
                case 2:
                    return this.e;
                case 3:
                    return this.f;
                case e.g.com_facebook_picker_fragment_done_button_text:
                    return this.g;
                case e.g.com_facebook_picker_fragment_title_bar_background:
                    return this.h;
                case e.g.com_facebook_picker_fragment_done_button_background:
                    return this.i;
                case 7:
                    return Boolean.valueOf(this.j);
                case 8:
                    return this.k;
                case 9:
                    return this.l;
                case 10:
                    return Integer.valueOf(this.m);
                default:
                    throw new IllegalStateException("Unknown safe parcelable id=" + aVar.g());
            }
        }

        public HashMap<String, an.a<?, ?>> b() {
            return b;
        }

        /* access modifiers changed from: protected */
        public boolean b(String str) {
            return false;
        }

        public int describeContents() {
            bj bjVar = a;
            return 0;
        }

        /* access modifiers changed from: package-private */
        public Set<Integer> e() {
            return this.c;
        }

        public boolean equals(Object obj) {
            if (!(obj instanceof g)) {
                return false;
            }
            if (this == obj) {
                return true;
            }
            g gVar = (g) obj;
            for (an.a next : b.values()) {
                if (a(next)) {
                    if (!gVar.a(next)) {
                        return false;
                    }
                    if (!b(next).equals(gVar.b(next))) {
                        return false;
                    }
                } else if (gVar.a(next)) {
                    return false;
                }
            }
            return true;
        }

        /* access modifiers changed from: package-private */
        public int f() {
            return this.d;
        }

        public String g() {
            return this.e;
        }

        public String h() {
            return this.f;
        }

        public int hashCode() {
            int i2 = 0;
            Iterator<an.a<?, ?>> it = b.values().iterator();
            while (true) {
                int i3 = i2;
                if (!it.hasNext()) {
                    return i3;
                }
                an.a next = it.next();
                if (a(next)) {
                    i2 = b(next).hashCode() + i3 + next.g();
                } else {
                    i2 = i3;
                }
            }
        }

        public String i() {
            return this.g;
        }

        public String j() {
            return this.h;
        }

        public String k() {
            return this.i;
        }

        public boolean l() {
            return this.j;
        }

        public String m() {
            return this.k;
        }

        public String n() {
            return this.l;
        }

        public int o() {
            return this.m;
        }

        /* renamed from: p */
        public g a() {
            return this;
        }

        public void writeToParcel(Parcel parcel, int i2) {
            bj bjVar = a;
            bj.a(this, parcel, i2);
        }
    }

    public static final class h extends an implements ae, a.g {
        public static final bl a = new bl();
        private static final HashMap<String, an.a<?, ?>> b = new HashMap<>();
        private final Set<Integer> c;
        private final int d;
        private boolean e;
        private String f;

        static {
            b.put("primary", an.a.c("primary", 2));
            b.put("value", an.a.d("value", 3));
        }

        public h() {
            this.d = 1;
            this.c = new HashSet();
        }

        h(Set<Integer> set, int i, boolean z, String str) {
            this.c = set;
            this.d = i;
            this.e = z;
            this.f = str;
        }

        /* access modifiers changed from: protected */
        public Object a(String str) {
            return null;
        }

        /* access modifiers changed from: protected */
        public boolean a(an.a aVar) {
            return this.c.contains(Integer.valueOf(aVar.g()));
        }

        /* access modifiers changed from: protected */
        public Object b(an.a aVar) {
            switch (aVar.g()) {
                case 2:
                    return Boolean.valueOf(this.e);
                case 3:
                    return this.f;
                default:
                    throw new IllegalStateException("Unknown safe parcelable id=" + aVar.g());
            }
        }

        public HashMap<String, an.a<?, ?>> b() {
            return b;
        }

        /* access modifiers changed from: protected */
        public boolean b(String str) {
            return false;
        }

        public int describeContents() {
            bl blVar = a;
            return 0;
        }

        /* access modifiers changed from: package-private */
        public Set<Integer> e() {
            return this.c;
        }

        public boolean equals(Object obj) {
            if (!(obj instanceof h)) {
                return false;
            }
            if (this == obj) {
                return true;
            }
            h hVar = (h) obj;
            for (an.a next : b.values()) {
                if (a(next)) {
                    if (!hVar.a(next)) {
                        return false;
                    }
                    if (!b(next).equals(hVar.b(next))) {
                        return false;
                    }
                } else if (hVar.a(next)) {
                    return false;
                }
            }
            return true;
        }

        /* access modifiers changed from: package-private */
        public int f() {
            return this.d;
        }

        public boolean g() {
            return this.e;
        }

        public String h() {
            return this.f;
        }

        public int hashCode() {
            int i = 0;
            Iterator<an.a<?, ?>> it = b.values().iterator();
            while (true) {
                int i2 = i;
                if (!it.hasNext()) {
                    return i2;
                }
                an.a next = it.next();
                if (a(next)) {
                    i = b(next).hashCode() + i2 + next.g();
                } else {
                    i = i2;
                }
            }
        }

        /* renamed from: i */
        public h a() {
            return this;
        }

        public void writeToParcel(Parcel parcel, int i) {
            bl blVar = a;
            bl.a(this, parcel, i);
        }
    }

    public static final class i extends an implements ae, a.h {
        public static final bm a = new bm();
        private static final HashMap<String, an.a<?, ?>> b = new HashMap<>();
        private final Set<Integer> c;
        private final int d;
        private boolean e;
        private int f;
        private String g;

        static {
            b.put("primary", an.a.c("primary", 2));
            b.put("type", an.a.a("type", 3, new ak().a("home", 0).a("work", 1).a("blog", 2).a("profile", 3).a("other", 4), false));
            b.put("value", an.a.d("value", 4));
        }

        public i() {
            this.d = 1;
            this.c = new HashSet();
        }

        i(Set<Integer> set, int i, boolean z, int i2, String str) {
            this.c = set;
            this.d = i;
            this.e = z;
            this.f = i2;
            this.g = str;
        }

        /* access modifiers changed from: protected */
        public Object a(String str) {
            return null;
        }

        /* access modifiers changed from: protected */
        public boolean a(an.a aVar) {
            return this.c.contains(Integer.valueOf(aVar.g()));
        }

        /* access modifiers changed from: protected */
        public Object b(an.a aVar) {
            switch (aVar.g()) {
                case 2:
                    return Boolean.valueOf(this.e);
                case 3:
                    return Integer.valueOf(this.f);
                case e.g.com_facebook_picker_fragment_done_button_text:
                    return this.g;
                default:
                    throw new IllegalStateException("Unknown safe parcelable id=" + aVar.g());
            }
        }

        public HashMap<String, an.a<?, ?>> b() {
            return b;
        }

        /* access modifiers changed from: protected */
        public boolean b(String str) {
            return false;
        }

        public int describeContents() {
            bm bmVar = a;
            return 0;
        }

        /* access modifiers changed from: package-private */
        public Set<Integer> e() {
            return this.c;
        }

        public boolean equals(Object obj) {
            if (!(obj instanceof i)) {
                return false;
            }
            if (this == obj) {
                return true;
            }
            i iVar = (i) obj;
            for (an.a next : b.values()) {
                if (a(next)) {
                    if (!iVar.a(next)) {
                        return false;
                    }
                    if (!b(next).equals(iVar.b(next))) {
                        return false;
                    }
                } else if (iVar.a(next)) {
                    return false;
                }
            }
            return true;
        }

        /* access modifiers changed from: package-private */
        public int f() {
            return this.d;
        }

        public boolean g() {
            return this.e;
        }

        public int h() {
            return this.f;
        }

        public int hashCode() {
            int i = 0;
            Iterator<an.a<?, ?>> it = b.values().iterator();
            while (true) {
                int i2 = i;
                if (!it.hasNext()) {
                    return i2;
                }
                an.a next = it.next();
                if (a(next)) {
                    i = b(next).hashCode() + i2 + next.g();
                } else {
                    i = i2;
                }
            }
        }

        public String i() {
            return this.g;
        }

        /* renamed from: j */
        public i a() {
            return this;
        }

        public void writeToParcel(Parcel parcel, int i) {
            bm bmVar = a;
            bm.a(this, parcel, i);
        }
    }

    static {
        b.put("aboutMe", an.a.d("aboutMe", 2));
        b.put("ageRange", an.a.a("ageRange", 3, a.class));
        b.put("birthday", an.a.d("birthday", 4));
        b.put("braggingRights", an.a.d("braggingRights", 5));
        b.put("circledByCount", an.a.a("circledByCount", 6));
        b.put("cover", an.a.a("cover", 7, b.class));
        b.put("currentLocation", an.a.d("currentLocation", 8));
        b.put("displayName", an.a.d("displayName", 9));
        b.put("emails", an.a.b("emails", 10, c.class));
        b.put("etag", an.a.d("etag", 11));
        b.put("gender", an.a.a("gender", 12, new ak().a("male", 0).a("female", 1).a("other", 2), false));
        b.put("hasApp", an.a.c("hasApp", 13));
        b.put("id", an.a.d("id", 14));
        b.put("image", an.a.a("image", 15, d.class));
        b.put("isPlusUser", an.a.c("isPlusUser", 16));
        b.put("language", an.a.d("language", 18));
        b.put("name", an.a.a("name", 19, e.class));
        b.put("nickname", an.a.d("nickname", 20));
        b.put("objectType", an.a.a("objectType", 21, new ak().a("person", 0).a("page", 1), false));
        b.put("organizations", an.a.b("organizations", 22, g.class));
        b.put("placesLived", an.a.b("placesLived", 23, h.class));
        b.put("plusOneCount", an.a.a("plusOneCount", 24));
        b.put("relationshipStatus", an.a.a("relationshipStatus", 25, new ak().a("single", 0).a("in_a_relationship", 1).a("engaged", 2).a("married", 3).a("its_complicated", 4).a("open_relationship", 5).a("widowed", 6).a("in_domestic_partnership", 7).a("in_civil_union", 8), false));
        b.put("tagline", an.a.d("tagline", 26));
        b.put("url", an.a.d("url", 27));
        b.put("urls", an.a.b("urls", 28, i.class));
        b.put("verified", an.a.c("verified", 29));
    }

    public eq() {
        this.d = 1;
        this.c = new HashSet();
    }

    eq(Set<Integer> set, int i2, String str, a aVar, String str2, String str3, int i3, b bVar, String str4, String str5, List<c> list, String str6, int i4, boolean z2, String str7, d dVar, boolean z3, String str8, e eVar, String str9, int i5, List<g> list2, List<h> list3, int i6, int i7, String str10, String str11, List<i> list4, boolean z4) {
        this.c = set;
        this.d = i2;
        this.e = str;
        this.f = aVar;
        this.g = str2;
        this.h = str3;
        this.i = i3;
        this.j = bVar;
        this.k = str4;
        this.l = str5;
        this.m = list;
        this.n = str6;
        this.o = i4;
        this.p = z2;
        this.q = str7;
        this.r = dVar;
        this.s = z3;
        this.t = str8;
        this.u = eVar;
        this.v = str9;
        this.w = i5;
        this.x = list2;
        this.y = list3;
        this.z = i6;
        this.A = i7;
        this.B = str10;
        this.C = str11;
        this.D = list4;
        this.E = z4;
    }

    public static eq a(byte[] bArr) {
        Parcel obtain = Parcel.obtain();
        obtain.unmarshall(bArr, 0, bArr.length);
        obtain.setDataPosition(0);
        eq a2 = a.createFromParcel(obtain);
        obtain.recycle();
        return a2;
    }

    /* access modifiers changed from: package-private */
    public List<h> A() {
        return this.y;
    }

    public int B() {
        return this.z;
    }

    public int C() {
        return this.A;
    }

    public String D() {
        return this.B;
    }

    public String E() {
        return this.C;
    }

    /* access modifiers changed from: package-private */
    public List<i> F() {
        return this.D;
    }

    public boolean G() {
        return this.E;
    }

    /* renamed from: H */
    public eq a() {
        return this;
    }

    /* access modifiers changed from: protected */
    public Object a(String str) {
        return null;
    }

    /* access modifiers changed from: protected */
    public boolean a(an.a aVar) {
        return this.c.contains(Integer.valueOf(aVar.g()));
    }

    /* access modifiers changed from: protected */
    public Object b(an.a aVar) {
        switch (aVar.g()) {
            case 2:
                return this.e;
            case 3:
                return this.f;
            case e.g.com_facebook_picker_fragment_done_button_text:
                return this.g;
            case e.g.com_facebook_picker_fragment_title_bar_background:
                return this.h;
            case e.g.com_facebook_picker_fragment_done_button_background:
                return Integer.valueOf(this.i);
            case 7:
                return this.j;
            case 8:
                return this.k;
            case 9:
                return this.l;
            case 10:
                return this.m;
            case 11:
                return this.n;
            case 12:
                return Integer.valueOf(this.o);
            case 13:
                return Boolean.valueOf(this.p);
            case 14:
                return this.q;
            case 15:
                return this.r;
            case 16:
                return Boolean.valueOf(this.s);
            case 17:
            default:
                throw new IllegalStateException("Unknown safe parcelable id=" + aVar.g());
            case 18:
                return this.t;
            case 19:
                return this.u;
            case 20:
                return this.v;
            case 21:
                return Integer.valueOf(this.w);
            case 22:
                return this.x;
            case 23:
                return this.y;
            case 24:
                return Integer.valueOf(this.z);
            case 25:
                return Integer.valueOf(this.A);
            case 26:
                return this.B;
            case 27:
                return this.C;
            case 28:
                return this.D;
            case 29:
                return Boolean.valueOf(this.E);
        }
    }

    public HashMap<String, an.a<?, ?>> b() {
        return b;
    }

    /* access modifiers changed from: protected */
    public boolean b(String str) {
        return false;
    }

    public int describeContents() {
        bk bkVar = a;
        return 0;
    }

    /* access modifiers changed from: package-private */
    public Set<Integer> e() {
        return this.c;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof eq)) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        eq eqVar = (eq) obj;
        for (an.a next : b.values()) {
            if (a(next)) {
                if (!eqVar.a(next)) {
                    return false;
                }
                if (!b(next).equals(eqVar.b(next))) {
                    return false;
                }
            } else if (eqVar.a(next)) {
                return false;
            }
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    public int f() {
        return this.d;
    }

    public String g() {
        return this.e;
    }

    /* access modifiers changed from: package-private */
    public a h() {
        return this.f;
    }

    public int hashCode() {
        int i2 = 0;
        Iterator<an.a<?, ?>> it = b.values().iterator();
        while (true) {
            int i3 = i2;
            if (!it.hasNext()) {
                return i3;
            }
            an.a next = it.next();
            if (a(next)) {
                i2 = b(next).hashCode() + i3 + next.g();
            } else {
                i2 = i3;
            }
        }
    }

    public String i() {
        return this.g;
    }

    public String j() {
        return this.h;
    }

    public int k() {
        return this.i;
    }

    /* access modifiers changed from: package-private */
    public b l() {
        return this.j;
    }

    public String m() {
        return this.k;
    }

    public String n() {
        return this.l;
    }

    /* access modifiers changed from: package-private */
    public List<c> o() {
        return this.m;
    }

    public String p() {
        return this.n;
    }

    public int q() {
        return this.o;
    }

    public boolean r() {
        return this.p;
    }

    public String s() {
        return this.q;
    }

    /* access modifiers changed from: package-private */
    public d t() {
        return this.r;
    }

    public boolean u() {
        return this.s;
    }

    public String v() {
        return this.t;
    }

    /* access modifiers changed from: package-private */
    public e w() {
        return this.u;
    }

    public void writeToParcel(Parcel parcel, int i2) {
        bk bkVar = a;
        bk.a(this, parcel, i2);
    }

    public String x() {
        return this.v;
    }

    public int y() {
        return this.w;
    }

    /* access modifiers changed from: package-private */
    public List<g> z() {
        return this.x;
    }
}
