package com.google.android.gms.games.multiplayer.realtime;

import android.os.Bundle;
import android.os.Parcelable;
import com.google.android.gms.common.a.a;

public interface Room extends Parcelable, a<Room>, com.google.android.gms.games.multiplayer.a {
    String b();

    String c();

    long d();

    int e();

    String f();

    int h();

    Bundle i();
}
