package com.google.android.gms.maps;

import android.os.Parcel;
import com.google.android.gms.internal.ae;
import com.google.android.gms.internal.u;
import com.google.android.gms.internal.v;
import com.google.android.gms.internal.w;
import com.google.android.gms.maps.model.CameraPosition;

public final class GoogleMapOptions implements ae {
    public static final a a = new a();
    private final int b;
    private Boolean c;
    private Boolean d;
    private int e;
    private CameraPosition f;
    private Boolean g;
    private Boolean h;
    private Boolean i;
    private Boolean j;
    private Boolean k;
    private Boolean l;

    public GoogleMapOptions() {
        this.e = -1;
        this.b = 1;
    }

    GoogleMapOptions(int i2, byte b2, byte b3, int i3, CameraPosition cameraPosition, byte b4, byte b5, byte b6, byte b7, byte b8, byte b9) {
        this.e = -1;
        this.b = i2;
        this.c = v.a(b2);
        this.d = v.a(b3);
        this.e = i3;
        this.f = cameraPosition;
        this.g = v.a(b4);
        this.h = v.a(b5);
        this.i = v.a(b6);
        this.j = v.a(b7);
        this.k = v.a(b8);
        this.l = v.a(b9);
    }

    public int a() {
        return this.b;
    }

    public byte b() {
        return v.a(this.c);
    }

    public byte c() {
        return v.a(this.d);
    }

    public byte d() {
        return v.a(this.g);
    }

    public int describeContents() {
        return 0;
    }

    public byte e() {
        return v.a(this.h);
    }

    public byte f() {
        return v.a(this.i);
    }

    public byte g() {
        return v.a(this.j);
    }

    public byte h() {
        return v.a(this.k);
    }

    public byte i() {
        return v.a(this.l);
    }

    public int j() {
        return this.e;
    }

    public CameraPosition k() {
        return this.f;
    }

    public void writeToParcel(Parcel parcel, int i2) {
        if (w.a()) {
            u.a(this, parcel, i2);
        } else {
            a.a(this, parcel, i2);
        }
    }
}
