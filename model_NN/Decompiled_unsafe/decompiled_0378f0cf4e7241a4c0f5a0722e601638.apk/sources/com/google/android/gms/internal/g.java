package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.internal.ak;
import com.google.android.gms.internal.b;
import java.util.ArrayList;

public class g implements Parcelable.Creator<ak> {
    static void a(ak akVar, Parcel parcel, int i) {
        int a = c.a(parcel);
        c.a(parcel, 1, akVar.a());
        c.b(parcel, 2, akVar.b(), false);
        c.a(parcel, a);
    }

    /* renamed from: a */
    public ak createFromParcel(Parcel parcel) {
        int b = b.b(parcel);
        int i = 0;
        ArrayList arrayList = null;
        while (parcel.dataPosition() < b) {
            int a = b.a(parcel);
            switch (b.a(a)) {
                case 1:
                    i = b.f(parcel, a);
                    break;
                case 2:
                    arrayList = b.c(parcel, a, ak.a.a);
                    break;
                default:
                    b.b(parcel, a);
                    break;
            }
        }
        if (parcel.dataPosition() == b) {
            return new ak(i, arrayList);
        }
        throw new b.a("Overread allowed size end=" + b, parcel);
    }

    /* renamed from: a */
    public ak[] newArray(int i) {
        return new ak[i];
    }
}
