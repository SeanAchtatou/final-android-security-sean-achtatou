package com.google.android.gms.games.multiplayer;

import android.net.Uri;
import android.os.Parcelable;
import com.google.android.gms.common.a.a;
import com.google.android.gms.games.Player;

public interface Participant extends Parcelable, a<Participant> {
    int b();

    String c();

    boolean d();

    String e();

    Uri f();

    Uri g();

    String h();

    Player i();
}
