package com.google.android.gms.internal;

import android.os.Parcel;
import com.google.android.gms.maps.model.LatLng;

public class ab {
    public static void a(LatLng latLng, Parcel parcel, int i) {
        int a = c.a(parcel);
        c.a(parcel, 1, latLng.a());
        c.a(parcel, 2, latLng.b);
        c.a(parcel, 3, latLng.c);
        c.a(parcel, a);
    }
}
