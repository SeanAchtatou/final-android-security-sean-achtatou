package com.google.android.gms.internal;

import android.os.Parcel;
import java.util.ArrayList;

public class eb implements ae {
    public static final ax a = new ax();
    private final int b;
    private final String c;
    private final ArrayList<ag> d;
    private final ArrayList<ag> e;
    private final boolean f;

    public eb(int i, String str, ArrayList<ag> arrayList, ArrayList<ag> arrayList2, boolean z) {
        this.b = i;
        this.c = str;
        this.d = arrayList;
        this.e = arrayList2;
        this.f = z;
    }

    public int a() {
        return this.b;
    }

    public String b() {
        return this.c;
    }

    public ArrayList<ag> c() {
        return this.d;
    }

    public ArrayList<ag> d() {
        return this.e;
    }

    public int describeContents() {
        return 0;
    }

    public boolean e() {
        return this.f;
    }

    public void writeToParcel(Parcel parcel, int i) {
        ax.a(this, parcel, i);
    }
}
