package com.google.android.gms.internal;

import android.content.Context;
import android.graphics.Paint;
import android.net.Uri;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

public class aw extends FrameLayout {
    private String[] a = null;
    private final ImageView b;
    private final TextView c;

    public aw(Context context) {
        super(context);
        this.b = new ImageView(context);
        addView(this.b, new FrameLayout.LayoutParams(-2, -2, 17));
        this.c = new TextView(context);
        addView(this.c, new FrameLayout.LayoutParams(-2, -2, 17));
        bringChildToFront(this.c);
    }

    public void a() {
        this.c.setSingleLine();
    }

    public void a(int i, float f) {
        this.c.setTextSize(i, f);
    }

    public void a(Uri uri) {
        this.b.setImageURI(uri);
    }

    public void a(String[] strArr) {
        this.a = strArr;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        int measureText;
        int size = View.MeasureSpec.getSize(i);
        Paint paint = new Paint();
        paint.setTextSize(this.c.getTextSize());
        paint.setTypeface(this.c.getTypeface());
        int length = this.a != null ? this.a.length : 0;
        int i3 = 0;
        String str = null;
        for (int i4 = 0; i4 < length; i4++) {
            if (this.a[i4] != null && (measureText = (int) paint.measureText(this.a[i4])) <= size && measureText >= i3) {
                str = this.a[i4];
                i3 = measureText;
            }
        }
        if (str == null || !str.equals(this.c.getText())) {
            this.c.setText(str);
        }
        super.onMeasure(i, i2);
    }

    public void setGravity(int i) {
        this.c.setGravity(i);
    }

    public void setTextColor(int i) {
        this.c.setTextColor(i);
    }
}
