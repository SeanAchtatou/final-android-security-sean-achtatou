package com.google.android.gms.internal;

import android.os.Parcel;

public final class ag implements ae {
    public static final e a = new e();
    private final int b;
    private final int c;
    private final int d;
    private final String e;
    private final String f;
    private final String g;
    private final String h;

    public ag(int i, int i2, int i3, String str, String str2, String str3, String str4) {
        this.b = i;
        this.c = i2;
        this.d = i3;
        this.e = str;
        this.f = str2;
        this.g = str3;
        this.h = str4;
    }

    public int a() {
        return this.b;
    }

    public int b() {
        return this.c;
    }

    public int c() {
        return this.d;
    }

    public String d() {
        return this.e;
    }

    public int describeContents() {
        return 0;
    }

    public String e() {
        return this.f;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof ag)) {
            return false;
        }
        ag agVar = (ag) obj;
        return this.b == agVar.b && this.c == agVar.c && this.d == agVar.d && bu.a(this.e, agVar.e) && bu.a(this.f, agVar.f);
    }

    public String f() {
        return this.g;
    }

    public String g() {
        return this.h;
    }

    public boolean h() {
        return this.c == 1 && this.d == -1;
    }

    public int hashCode() {
        return bu.a(Integer.valueOf(this.b), Integer.valueOf(this.c), Integer.valueOf(this.d), this.e, this.f);
    }

    public boolean i() {
        return this.c == 2;
    }

    public String toString() {
        if (i()) {
            return String.format("Person [%s] %s", e(), f());
        } else if (h()) {
            return String.format("Circle [%s] %s", d(), f());
        } else {
            return String.format("Group [%s] %s", d(), f());
        }
    }

    public void writeToParcel(Parcel parcel, int i) {
        e.a(this, parcel, i);
    }
}
