package com.google.android.gms.location;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.a.e;
import com.google.android.gms.internal.b;

public class c implements Parcelable.Creator<LocationRequest> {
    static void a(LocationRequest locationRequest, Parcel parcel, int i) {
        int a = com.google.android.gms.internal.c.a(parcel);
        com.google.android.gms.internal.c.a(parcel, 1, locationRequest.b);
        com.google.android.gms.internal.c.a(parcel, 1000, locationRequest.a);
        com.google.android.gms.internal.c.a(parcel, 2, locationRequest.c);
        com.google.android.gms.internal.c.a(parcel, 3, locationRequest.d);
        com.google.android.gms.internal.c.a(parcel, 4, locationRequest.e);
        com.google.android.gms.internal.c.a(parcel, 5, locationRequest.f);
        com.google.android.gms.internal.c.a(parcel, 6, locationRequest.g);
        com.google.android.gms.internal.c.a(parcel, 7, locationRequest.h);
        com.google.android.gms.internal.c.a(parcel, a);
    }

    /* renamed from: a */
    public LocationRequest createFromParcel(Parcel parcel) {
        LocationRequest locationRequest = new LocationRequest();
        int b = b.b(parcel);
        while (parcel.dataPosition() < b) {
            int a = b.a(parcel);
            switch (b.a(a)) {
                case 1:
                    locationRequest.b = b.f(parcel, a);
                    break;
                case 2:
                    locationRequest.c = b.g(parcel, a);
                    break;
                case 3:
                    locationRequest.d = b.g(parcel, a);
                    break;
                case e.g.com_facebook_picker_fragment_done_button_text:
                    locationRequest.e = b.c(parcel, a);
                    break;
                case e.g.com_facebook_picker_fragment_title_bar_background:
                    locationRequest.f = b.g(parcel, a);
                    break;
                case e.g.com_facebook_picker_fragment_done_button_background:
                    locationRequest.g = b.f(parcel, a);
                    break;
                case 7:
                    locationRequest.h = b.i(parcel, a);
                    break;
                case 1000:
                    locationRequest.a = b.f(parcel, a);
                    break;
                default:
                    b.b(parcel, a);
                    break;
            }
        }
        if (parcel.dataPosition() == b) {
            return locationRequest;
        }
        throw new b.a("Overread allowed size end=" + b, parcel);
    }

    /* renamed from: a */
    public LocationRequest[] newArray(int i) {
        return new LocationRequest[i];
    }
}
