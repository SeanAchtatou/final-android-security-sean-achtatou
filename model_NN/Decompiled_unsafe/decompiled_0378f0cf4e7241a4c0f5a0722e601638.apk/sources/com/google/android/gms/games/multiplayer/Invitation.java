package com.google.android.gms.games.multiplayer;

import android.os.Parcelable;
import com.google.android.gms.common.a.a;
import com.google.android.gms.games.Game;

public interface Invitation extends Parcelable, a<Invitation>, a {
    Game b();

    String c();

    Participant d();

    long e();

    int f();
}
