package com.google.android.gms.maps.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.a.e;
import com.google.android.gms.internal.b;

public class c implements Parcelable.Creator<CircleOptions> {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.c.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.maps.model.LatLng, int, int]
     candidates:
      com.google.android.gms.internal.c.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.internal.c.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    static void a(CircleOptions circleOptions, Parcel parcel, int i) {
        int a = com.google.android.gms.internal.c.a(parcel);
        com.google.android.gms.internal.c.a(parcel, 1, circleOptions.a());
        com.google.android.gms.internal.c.a(parcel, 2, (Parcelable) circleOptions.b(), i, false);
        com.google.android.gms.internal.c.a(parcel, 3, circleOptions.c());
        com.google.android.gms.internal.c.a(parcel, 4, circleOptions.d());
        com.google.android.gms.internal.c.a(parcel, 5, circleOptions.e());
        com.google.android.gms.internal.c.a(parcel, 6, circleOptions.f());
        com.google.android.gms.internal.c.a(parcel, 7, circleOptions.g());
        com.google.android.gms.internal.c.a(parcel, 8, circleOptions.h());
        com.google.android.gms.internal.c.a(parcel, a);
    }

    /* renamed from: a */
    public CircleOptions createFromParcel(Parcel parcel) {
        float f = 0.0f;
        boolean z = false;
        int b = b.b(parcel);
        LatLng latLng = null;
        double d = 0.0d;
        int i = 0;
        int i2 = 0;
        float f2 = 0.0f;
        int i3 = 0;
        while (parcel.dataPosition() < b) {
            int a = b.a(parcel);
            switch (b.a(a)) {
                case 1:
                    i3 = b.f(parcel, a);
                    break;
                case 2:
                    latLng = (LatLng) b.a(parcel, a, LatLng.a);
                    break;
                case 3:
                    d = b.j(parcel, a);
                    break;
                case e.g.com_facebook_picker_fragment_done_button_text:
                    f2 = b.i(parcel, a);
                    break;
                case e.g.com_facebook_picker_fragment_title_bar_background:
                    i2 = b.f(parcel, a);
                    break;
                case e.g.com_facebook_picker_fragment_done_button_background:
                    i = b.f(parcel, a);
                    break;
                case 7:
                    f = b.i(parcel, a);
                    break;
                case 8:
                    z = b.c(parcel, a);
                    break;
                default:
                    b.b(parcel, a);
                    break;
            }
        }
        if (parcel.dataPosition() == b) {
            return new CircleOptions(i3, latLng, d, f2, i2, i, f, z);
        }
        throw new b.a("Overread allowed size end=" + b, parcel);
    }

    /* renamed from: a */
    public CircleOptions[] newArray(int i) {
        return new CircleOptions[i];
    }
}
