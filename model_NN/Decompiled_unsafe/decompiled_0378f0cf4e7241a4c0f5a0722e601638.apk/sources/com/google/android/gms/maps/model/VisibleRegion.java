package com.google.android.gms.maps.model;

import android.os.Parcel;
import com.google.android.gms.internal.ae;
import com.google.android.gms.internal.al;
import com.google.android.gms.internal.bu;
import com.google.android.gms.internal.w;

public final class VisibleRegion implements ae {
    public static final m a = new m();
    public final LatLng b;
    public final LatLng c;
    public final LatLng d;
    public final LatLng e;
    public final LatLngBounds f;
    private final int g;

    VisibleRegion(int i, LatLng latLng, LatLng latLng2, LatLng latLng3, LatLng latLng4, LatLngBounds latLngBounds) {
        this.g = i;
        this.b = latLng;
        this.c = latLng2;
        this.d = latLng3;
        this.e = latLng4;
        this.f = latLngBounds;
    }

    public int a() {
        return this.g;
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof VisibleRegion)) {
            return false;
        }
        VisibleRegion visibleRegion = (VisibleRegion) obj;
        return this.b.equals(visibleRegion.b) && this.c.equals(visibleRegion.c) && this.d.equals(visibleRegion.d) && this.e.equals(visibleRegion.e) && this.f.equals(visibleRegion.f);
    }

    public int hashCode() {
        return bu.a(this.b, this.c, this.d, this.e, this.f);
    }

    public String toString() {
        return bu.a(this).a("nearLeft", this.b).a("nearRight", this.c).a("farLeft", this.d).a("farRight", this.e).a("latLngBounds", this.f).toString();
    }

    public void writeToParcel(Parcel parcel, int i) {
        if (w.a()) {
            al.a(this, parcel, i);
        } else {
            m.a(this, parcel, i);
        }
    }
}
