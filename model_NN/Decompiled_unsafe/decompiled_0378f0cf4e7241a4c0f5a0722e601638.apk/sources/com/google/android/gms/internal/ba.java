package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.internal.b;
import com.google.android.gms.internal.eq;
import java.util.HashSet;
import java.util.Set;

public class ba implements Parcelable.Creator<eq.a> {
    static void a(eq.a aVar, Parcel parcel, int i) {
        int a = c.a(parcel);
        Set<Integer> e = aVar.e();
        if (e.contains(1)) {
            c.a(parcel, 1, aVar.f());
        }
        if (e.contains(2)) {
            c.a(parcel, 2, aVar.g());
        }
        if (e.contains(3)) {
            c.a(parcel, 3, aVar.h());
        }
        c.a(parcel, a);
    }

    /* renamed from: a */
    public eq.a createFromParcel(Parcel parcel) {
        int i = 0;
        int b = b.b(parcel);
        HashSet hashSet = new HashSet();
        int i2 = 0;
        int i3 = 0;
        while (parcel.dataPosition() < b) {
            int a = b.a(parcel);
            switch (b.a(a)) {
                case 1:
                    i3 = b.f(parcel, a);
                    hashSet.add(1);
                    break;
                case 2:
                    i2 = b.f(parcel, a);
                    hashSet.add(2);
                    break;
                case 3:
                    i = b.f(parcel, a);
                    hashSet.add(3);
                    break;
                default:
                    b.b(parcel, a);
                    break;
            }
        }
        if (parcel.dataPosition() == b) {
            return new eq.a(hashSet, i3, i2, i);
        }
        throw new b.a("Overread allowed size end=" + b, parcel);
    }

    /* renamed from: a */
    public eq.a[] newArray(int i) {
        return new eq.a[i];
    }
}
