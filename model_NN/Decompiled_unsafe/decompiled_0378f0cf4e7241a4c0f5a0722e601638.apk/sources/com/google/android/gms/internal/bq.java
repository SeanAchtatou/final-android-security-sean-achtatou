package com.google.android.gms.internal;

import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.util.Log;
import com.google.android.gms.common.b;
import com.google.android.gms.internal.bs;
import com.google.android.gms.internal.bt;
import java.util.ArrayList;

public abstract class bq<T extends IInterface> implements com.google.android.gms.common.b {
    public static final String[] e = {"service_esmobile", "service_googleme"};
    final Handler a;
    final ArrayList<b.a> b = new ArrayList<>();
    boolean c = false;
    boolean d = false;
    /* access modifiers changed from: private */
    public final Context f;
    /* access modifiers changed from: private */
    public T g;
    /* access modifiers changed from: private */
    public ArrayList<b.a> h;
    private boolean i = false;
    private ArrayList<b.C0020b> j;
    private boolean k = false;
    /* access modifiers changed from: private */
    public final ArrayList<bq<T>.b<?>> l = new ArrayList<>();
    /* access modifiers changed from: private */
    public bq<T>.d m;
    private final String[] n;
    /* access modifiers changed from: private */
    public final Object o = new Object();

    final class a extends Handler {
        public a(Looper looper) {
            super(looper);
        }

        public void handleMessage(Message message) {
            synchronized (bq.this.o) {
                bq.this.d = false;
            }
            if (message.what == 3) {
                bq.this.a(new com.google.android.gms.common.a(((Integer) message.obj).intValue(), null));
            } else if (message.what == 4) {
                synchronized (bq.this.h) {
                    if (bq.this.c && bq.this.d() && bq.this.h.contains(message.obj)) {
                        ((b.a) message.obj).a(bq.this.h());
                    }
                }
            } else if (message.what == 2 && !bq.this.d()) {
            } else {
                if (message.what == 2 || message.what == 1) {
                    ((b) message.obj).b();
                }
            }
        }
    }

    public abstract class b<TListener> {
        private TListener a;

        public b(TListener tlistener) {
            this.a = tlistener;
            synchronized (bq.this.l) {
                bq.this.l.add(this);
            }
        }

        public void a() {
            synchronized (this) {
                this.a = null;
            }
        }

        /* access modifiers changed from: protected */
        public abstract void a(Object obj);

        public void b() {
            TListener tlistener;
            synchronized (this) {
                tlistener = this.a;
            }
            a(tlistener);
        }
    }

    public final class c extends bs.a {
        protected c() {
        }

        public void a(int i, IBinder iBinder, Bundle bundle) {
            bq.this.a.sendMessage(bq.this.a.obtainMessage(1, new e(i, iBinder, bundle)));
        }
    }

    final class d implements ServiceConnection {
        d() {
        }

        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            bq.this.c(iBinder);
        }

        public void onServiceDisconnected(ComponentName componentName) {
            IInterface unused = bq.this.g = (IInterface) null;
            bq.this.i();
        }
    }

    public final class e extends bq<T>.b<Boolean> {
        public final int a;
        public final Bundle b;
        public final IBinder c;

        public e(int i, IBinder iBinder, Bundle bundle) {
            super(true);
            this.a = i;
            this.c = iBinder;
            this.b = bundle;
        }

        /* access modifiers changed from: protected */
        public void a(Boolean bool) {
            PendingIntent pendingIntent = null;
            if (bool != null) {
                switch (this.a) {
                    case 0:
                        try {
                            if (bq.this.a().equals(this.c.getInterfaceDescriptor())) {
                                IInterface unused = bq.this.g = bq.this.b(this.c);
                                if (bq.this.g != null) {
                                    bq.this.g();
                                    return;
                                }
                            }
                        } catch (RemoteException e2) {
                        }
                        br.a(bq.this.f).b(bq.this.b(), bq.this.m);
                        d unused2 = bq.this.m = (d) null;
                        IInterface unused3 = bq.this.g = (IInterface) null;
                        bq.this.a(new com.google.android.gms.common.a(8, null));
                        return;
                    case 10:
                        throw new IllegalStateException("A fatal developer error has occurred. Check the logs for further information.");
                    default:
                        if (this.b != null) {
                            pendingIntent = (PendingIntent) this.b.getParcelable("pendingIntent");
                        }
                        bq.this.a(new com.google.android.gms.common.a(this.a, pendingIntent));
                        return;
                }
            }
        }
    }

    protected bq(Context context, b.a aVar, b.C0020b bVar, String... strArr) {
        this.f = (Context) bv.a(context);
        this.h = new ArrayList<>();
        this.h.add(bv.a(aVar));
        this.j = new ArrayList<>();
        this.j.add(bv.a(bVar));
        this.a = new a(context.getMainLooper());
        a(strArr);
        this.n = strArr;
    }

    /* access modifiers changed from: protected */
    public abstract String a();

    /* access modifiers changed from: protected */
    public void a(com.google.android.gms.common.a aVar) {
        this.a.removeMessages(4);
        synchronized (this.j) {
            this.k = true;
            ArrayList<b.C0020b> arrayList = this.j;
            int size = arrayList.size();
            int i2 = 0;
            while (i2 < size) {
                if (this.c) {
                    if (this.j.contains(arrayList.get(i2))) {
                        arrayList.get(i2).a(aVar);
                    }
                    i2++;
                } else {
                    return;
                }
            }
            this.k = false;
        }
    }

    public void a(b.a aVar) {
        bv.a(aVar);
        synchronized (this.h) {
            if (this.h.contains(aVar)) {
                Log.w("GmsClient", "registerConnectionCallbacks(): listener " + aVar + " is already registered");
            } else {
                if (this.i) {
                    this.h = new ArrayList<>(this.h);
                }
                this.h.add(aVar);
            }
        }
        if (d()) {
            this.a.sendMessage(this.a.obtainMessage(4, aVar));
        }
    }

    public void a(b.C0020b bVar) {
        bv.a(bVar);
        synchronized (this.j) {
            if (this.j.contains(bVar)) {
                Log.w("GmsClient", "registerConnectionFailedListener(): listener " + bVar + " is already registered");
            } else {
                if (this.k) {
                    this.j = new ArrayList<>(this.j);
                }
                this.j.add(bVar);
            }
        }
    }

    public final void a(bq<T>.b<?> bVar) {
        this.a.sendMessage(this.a.obtainMessage(2, bVar));
    }

    /* access modifiers changed from: protected */
    public abstract void a(bt btVar, bq<T>.c cVar) throws RemoteException;

    /* access modifiers changed from: protected */
    public void a(String... strArr) {
    }

    /* access modifiers changed from: protected */
    public abstract T b(IBinder iBinder);

    /* access modifiers changed from: protected */
    public abstract String b();

    public boolean b(b.a aVar) {
        boolean contains;
        bv.a(aVar);
        synchronized (this.h) {
            contains = this.h.contains(aVar);
        }
        return contains;
    }

    public boolean b(b.C0020b bVar) {
        boolean contains;
        bv.a(bVar);
        synchronized (this.j) {
            contains = this.j.contains(bVar);
        }
        return contains;
    }

    public void c() {
        this.c = true;
        synchronized (this.o) {
            this.d = true;
        }
        int a2 = com.google.android.gms.common.c.a(this.f);
        if (a2 != 0) {
            this.a.sendMessage(this.a.obtainMessage(3, Integer.valueOf(a2)));
            return;
        }
        if (this.m != null) {
            Log.e("GmsClient", "Calling connect() while still connected, missing disconnect().");
            this.g = null;
            br.a(this.f).b(b(), this.m);
        }
        this.m = new d();
        if (!br.a(this.f).a(b(), this.m)) {
            Log.e("GmsClient", "unable to connect to service: " + b());
            this.a.sendMessage(this.a.obtainMessage(3, 9));
        }
    }

    /* access modifiers changed from: protected */
    public final void c(IBinder iBinder) {
        try {
            a(bt.a.a(iBinder), new c());
        } catch (RemoteException e2) {
            Log.w("GmsClient", "service died");
        }
    }

    public void c(b.a aVar) {
        bv.a(aVar);
        synchronized (this.h) {
            if (this.h != null) {
                if (this.i) {
                    this.h = new ArrayList<>(this.h);
                }
                if (!this.h.remove(aVar)) {
                    Log.w("GmsClient", "unregisterConnectionCallbacks(): listener " + aVar + " not found");
                } else if (this.i && !this.b.contains(aVar)) {
                    this.b.add(aVar);
                }
            }
        }
    }

    public void c(b.C0020b bVar) {
        bv.a(bVar);
        synchronized (this.j) {
            if (this.j != null) {
                if (this.k) {
                    this.j = new ArrayList<>(this.j);
                }
                if (!this.j.remove(bVar)) {
                    Log.w("GmsClient", "unregisterConnectionFailedListener(): listener " + bVar + " not found");
                }
            }
        }
    }

    public boolean d() {
        return this.g != null;
    }

    public void e() {
        this.c = false;
        synchronized (this.o) {
            this.d = false;
        }
        synchronized (this.l) {
            int size = this.l.size();
            for (int i2 = 0; i2 < size; i2++) {
                this.l.get(i2).a();
            }
            this.l.clear();
        }
        this.g = null;
        if (this.m != null) {
            br.a(this.f).b(b(), this.m);
            this.m = null;
        }
    }

    public final String[] f() {
        return this.n;
    }

    /* access modifiers changed from: protected */
    public void g() {
        boolean z = true;
        synchronized (this.h) {
            bv.a(!this.i);
            this.a.removeMessages(4);
            this.i = true;
            if (this.b.size() != 0) {
                z = false;
            }
            bv.a(z);
            Bundle h2 = h();
            ArrayList<b.a> arrayList = this.h;
            int size = arrayList.size();
            for (int i2 = 0; i2 < size && this.c && d(); i2++) {
                this.b.size();
                if (!this.b.contains(arrayList.get(i2))) {
                    arrayList.get(i2).a(h2);
                }
            }
            this.b.clear();
            this.i = false;
        }
    }

    /* access modifiers changed from: protected */
    public Bundle h() {
        return null;
    }

    /* access modifiers changed from: protected */
    public final void i() {
        this.a.removeMessages(4);
        synchronized (this.h) {
            this.i = true;
            ArrayList<b.a> arrayList = this.h;
            int size = arrayList.size();
            for (int i2 = 0; i2 < size && this.c; i2++) {
                if (this.h.contains(arrayList.get(i2))) {
                    arrayList.get(i2).b();
                }
            }
            this.i = false;
        }
    }

    /* access modifiers changed from: protected */
    public final void j() {
        if (!d()) {
            throw new IllegalStateException("Not connected. Call connect() and wait for onConnected() to be called.");
        }
    }

    /* access modifiers changed from: protected */
    public final T k() {
        j();
        return this.g;
    }
}
