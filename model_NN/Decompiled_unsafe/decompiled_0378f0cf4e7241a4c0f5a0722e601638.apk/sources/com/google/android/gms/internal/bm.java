package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.a.e;
import com.google.android.gms.internal.b;
import com.google.android.gms.internal.eq;
import java.util.HashSet;
import java.util.Set;

public class bm implements Parcelable.Creator<eq.i> {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.c.a(android.os.Parcel, int, java.lang.String, boolean):void
     arg types: [android.os.Parcel, int, java.lang.String, int]
     candidates:
      com.google.android.gms.internal.c.a(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.internal.c.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.internal.c.a(android.os.Parcel, int, android.os.Parcel, boolean):void
      com.google.android.gms.internal.c.a(android.os.Parcel, int, java.util.List<java.lang.String>, boolean):void
      com.google.android.gms.internal.c.a(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.internal.c.a(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.internal.c.a(android.os.Parcel, int, java.lang.String, boolean):void */
    static void a(eq.i iVar, Parcel parcel, int i) {
        int a = c.a(parcel);
        Set<Integer> e = iVar.e();
        if (e.contains(1)) {
            c.a(parcel, 1, iVar.f());
        }
        if (e.contains(2)) {
            c.a(parcel, 2, iVar.g());
        }
        if (e.contains(3)) {
            c.a(parcel, 3, iVar.h());
        }
        if (e.contains(4)) {
            c.a(parcel, 4, iVar.i(), true);
        }
        c.a(parcel, a);
    }

    /* renamed from: a */
    public eq.i createFromParcel(Parcel parcel) {
        int i = 0;
        int b = b.b(parcel);
        HashSet hashSet = new HashSet();
        String str = null;
        boolean z = false;
        int i2 = 0;
        while (parcel.dataPosition() < b) {
            int a = b.a(parcel);
            switch (b.a(a)) {
                case 1:
                    i2 = b.f(parcel, a);
                    hashSet.add(1);
                    break;
                case 2:
                    z = b.c(parcel, a);
                    hashSet.add(2);
                    break;
                case 3:
                    i = b.f(parcel, a);
                    hashSet.add(3);
                    break;
                case e.g.com_facebook_picker_fragment_done_button_text:
                    str = b.l(parcel, a);
                    hashSet.add(4);
                    break;
                default:
                    b.b(parcel, a);
                    break;
            }
        }
        if (parcel.dataPosition() == b) {
            return new eq.i(hashSet, i2, z, i, str);
        }
        throw new b.a("Overread allowed size end=" + b, parcel);
    }

    /* renamed from: a */
    public eq.i[] newArray(int i) {
        return new eq.i[i];
    }
}
