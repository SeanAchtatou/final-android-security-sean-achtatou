package com.google.android.gms.internal;

import android.app.PendingIntent;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.os.ParcelFileDescriptor;
import android.os.RemoteException;
import android.util.Log;
import com.google.android.gms.common.b;
import com.google.android.gms.internal.au;
import com.google.android.gms.internal.bq;
import com.google.android.gms.internal.bs;
import com.google.android.gms.plus.a;
import java.io.IOException;

public class av extends bq<au> {
    private final String f;
    private final String g;
    private final String h;
    /* access modifiers changed from: private */
    public com.google.android.gms.plus.a.b.a i;
    private final String[] j;
    private final String[] k;

    final class a extends ar {
        private final a.b b;

        public a(a.b bVar) {
            this.b = bVar;
        }

        public void a(int i, Bundle bundle, ParcelFileDescriptor parcelFileDescriptor) {
            PendingIntent pendingIntent = null;
            if (bundle != null) {
                pendingIntent = (PendingIntent) bundle.getParcelable("pendingIntent");
            }
            av.this.a(new b(this.b, new com.google.android.gms.common.a(i, pendingIntent), parcelFileDescriptor));
        }
    }

    final class b extends bq<au>.b<a.b> {
        private final com.google.android.gms.common.a b;
        private final ParcelFileDescriptor c;

        public b(a.b bVar, com.google.android.gms.common.a aVar, ParcelFileDescriptor parcelFileDescriptor) {
            super(bVar);
            this.b = aVar;
            this.c = parcelFileDescriptor;
        }

        public void a() {
            super.a();
        }

        public void a(a.b bVar) {
            if (bVar != null) {
                bVar.a(this.b, this.c);
                return;
            }
            try {
                this.c.close();
            } catch (IOException e) {
                Log.e("PlusClientImpl", "failed close", e);
            }
        }
    }

    public final class c extends bs.a {
        private bq.c b;

        public c(bq.c cVar) {
            this.b = cVar;
        }

        public void a(int i, IBinder iBinder, Bundle bundle) {
            if (i == 0 && bundle != null && bundle.containsKey("loaded_person")) {
                com.google.android.gms.plus.a.b.a unused = av.this.i = eq.a(bundle.getByteArray("loaded_person"));
            }
            this.b.a(i, iBinder, bundle);
        }
    }

    final class d extends ar {
        private final a.c b;

        public d(a.c cVar) {
            this.b = cVar;
        }

        public void a(int i, Bundle bundle, Bundle bundle2) {
            ap apVar = null;
            com.google.android.gms.common.a aVar = new com.google.android.gms.common.a(i, bundle != null ? (PendingIntent) bundle.getParcelable("pendingIntent") : null);
            if (bundle2 != null) {
                apVar = new ap(bundle2);
            }
            av.this.a(new e(this.b, aVar, apVar));
        }
    }

    final class e extends bq<au>.b<a.c> {
        public final com.google.android.gms.common.a a;
        public final ap b;

        public e(a.c cVar, com.google.android.gms.common.a aVar, ap apVar) {
            super(cVar);
            this.a = aVar;
            this.b = apVar;
        }

        /* access modifiers changed from: protected */
        public void a(a.c cVar) {
            if (cVar != null) {
                cVar.a(this.a, this.b);
            }
        }
    }

    public av(Context context, String str, String str2, String str3, b.a aVar, b.C0020b bVar, String[] strArr, String[] strArr2, String[] strArr3) {
        super(context, aVar, bVar, strArr3);
        this.f = str;
        this.g = str2;
        this.h = str3;
        this.j = strArr;
        this.k = strArr2;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public au b(IBinder iBinder) {
        return au.a.a(iBinder);
    }

    /* access modifiers changed from: protected */
    public String a() {
        return "com.google.android.gms.plus.internal.IPlusService";
    }

    /* access modifiers changed from: protected */
    public void a(bt btVar, bq<au>.c cVar) throws RemoteException {
        Bundle bundle = new Bundle();
        bundle.putBoolean("skip_oob", false);
        bundle.putStringArray("request_visible_actions", this.j);
        if (this.k != null) {
            bundle.putStringArray("required_features", this.k);
        }
        btVar.a(new c(cVar), 3136100, this.f, this.g, f(), this.h, bundle);
    }

    public void a(a.b bVar, Uri uri, int i2) {
        j();
        Bundle bundle = new Bundle();
        bundle.putInt("bounding_box", i2);
        a aVar = new a(bVar);
        try {
            ((au) k()).a(aVar, uri, bundle);
        } catch (RemoteException e2) {
            aVar.a(8, (Bundle) null, (ParcelFileDescriptor) null);
        }
    }

    public void a(a.c cVar, String str) {
        j();
        d dVar = new d(cVar);
        try {
            ((au) k()).a(dVar, str);
        } catch (RemoteException e2) {
            dVar.a(8, (Bundle) null, (Bundle) null);
        }
    }

    /* access modifiers changed from: protected */
    public String b() {
        return "com.google.android.gms.plus.service.START";
    }
}
