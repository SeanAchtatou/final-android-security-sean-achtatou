package com.google.android.gms.common;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.IntentSender;
import com.facebook.a.e;
import com.google.android.gms.internal.bu;

public final class a {
    public static final a a = new a(0, null);
    private final PendingIntent b;
    private final int c;

    public a(int i, PendingIntent pendingIntent) {
        this.c = i;
        this.b = pendingIntent;
    }

    private String c() {
        switch (this.c) {
            case 0:
                return "SUCCESS";
            case 1:
                return "SERVICE_MISSING";
            case 2:
                return "SERVICE_VERSION_UPDATE_REQUIRED";
            case 3:
                return "SERVICE_DISABLED";
            case e.g.com_facebook_picker_fragment_done_button_text:
                return "SIGN_IN_REQUIRED";
            case e.g.com_facebook_picker_fragment_title_bar_background:
                return "INVALID_ACCOUNT";
            case e.g.com_facebook_picker_fragment_done_button_background:
                return "RESOLUTION_REQUIRED";
            case 7:
                return "NETWORK_ERROR";
            case 8:
                return "INTERNAL_ERROR";
            case 9:
                return "SERVICE_INVALID";
            case 10:
                return "DEVELOPER_ERROR";
            default:
                return "unknown status code " + this.c;
        }
    }

    public void a(Activity activity, int i) throws IntentSender.SendIntentException {
        if (a()) {
            activity.startIntentSenderForResult(this.b.getIntentSender(), i, null, 0, 0, 0);
        }
    }

    public boolean a() {
        return (this.c == 0 || this.b == null) ? false : true;
    }

    public boolean b() {
        return this.c == 0;
    }

    public String toString() {
        return bu.a(this).a("statusCode", c()).a("resolution", this.b).toString();
    }
}
