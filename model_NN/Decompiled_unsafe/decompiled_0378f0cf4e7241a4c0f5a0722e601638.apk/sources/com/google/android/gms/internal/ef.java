package com.google.android.gms.internal;

import android.os.Parcel;
import com.facebook.a.e;
import com.google.android.gms.internal.an;
import com.google.android.gms.plus.a.a.b;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public final class ef extends an implements ae, b {
    public static final az a = new az();
    private static final HashMap<String, an.a<?, ?>> b = new HashMap<>();
    private final Set<Integer> c;
    private final int d;
    private String e;
    private ed f;
    private String g;
    private ed h;
    private String i;

    static {
        b.put("id", an.a.d("id", 2));
        b.put("result", an.a.a("result", 4, ed.class));
        b.put("startDate", an.a.d("startDate", 5));
        b.put("target", an.a.a("target", 6, ed.class));
        b.put("type", an.a.d("type", 7));
    }

    public ef() {
        this.d = 1;
        this.c = new HashSet();
    }

    ef(Set<Integer> set, int i2, String str, ed edVar, String str2, ed edVar2, String str3) {
        this.c = set;
        this.d = i2;
        this.e = str;
        this.f = edVar;
        this.g = str2;
        this.h = edVar2;
        this.i = str3;
    }

    /* access modifiers changed from: protected */
    public Object a(String str) {
        return null;
    }

    /* access modifiers changed from: protected */
    public boolean a(an.a aVar) {
        return this.c.contains(Integer.valueOf(aVar.g()));
    }

    /* access modifiers changed from: protected */
    public Object b(an.a aVar) {
        switch (aVar.g()) {
            case 2:
                return this.e;
            case 3:
            default:
                throw new IllegalStateException("Unknown safe parcelable id=" + aVar.g());
            case e.g.com_facebook_picker_fragment_done_button_text:
                return this.f;
            case e.g.com_facebook_picker_fragment_title_bar_background:
                return this.g;
            case e.g.com_facebook_picker_fragment_done_button_background:
                return this.h;
            case 7:
                return this.i;
        }
    }

    public HashMap<String, an.a<?, ?>> b() {
        return b;
    }

    /* access modifiers changed from: protected */
    public boolean b(String str) {
        return false;
    }

    public int describeContents() {
        az azVar = a;
        return 0;
    }

    /* access modifiers changed from: package-private */
    public Set<Integer> e() {
        return this.c;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof ef)) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        ef efVar = (ef) obj;
        for (an.a next : b.values()) {
            if (a(next)) {
                if (!efVar.a(next)) {
                    return false;
                }
                if (!b(next).equals(efVar.b(next))) {
                    return false;
                }
            } else if (efVar.a(next)) {
                return false;
            }
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    public int f() {
        return this.d;
    }

    public String g() {
        return this.e;
    }

    /* access modifiers changed from: package-private */
    public ed h() {
        return this.f;
    }

    public int hashCode() {
        int i2 = 0;
        Iterator<an.a<?, ?>> it = b.values().iterator();
        while (true) {
            int i3 = i2;
            if (!it.hasNext()) {
                return i3;
            }
            an.a next = it.next();
            if (a(next)) {
                i2 = b(next).hashCode() + i3 + next.g();
            } else {
                i2 = i3;
            }
        }
    }

    public String i() {
        return this.g;
    }

    /* access modifiers changed from: package-private */
    public ed j() {
        return this.h;
    }

    public String k() {
        return this.i;
    }

    /* renamed from: l */
    public ef a() {
        return this;
    }

    public void writeToParcel(Parcel parcel, int i2) {
        az azVar = a;
        az.a(this, parcel, i2);
    }
}
