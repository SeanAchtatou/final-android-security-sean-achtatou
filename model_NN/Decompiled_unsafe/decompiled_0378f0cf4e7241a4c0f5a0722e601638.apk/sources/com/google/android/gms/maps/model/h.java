package com.google.android.gms.maps.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.a.e;
import com.google.android.gms.internal.b;
import com.google.android.gms.internal.c;
import java.util.ArrayList;

public class h implements Parcelable.Creator<PolygonOptions> {
    static void a(PolygonOptions polygonOptions, Parcel parcel, int i) {
        int a = c.a(parcel);
        c.a(parcel, 1, polygonOptions.a());
        c.b(parcel, 2, polygonOptions.c(), false);
        c.c(parcel, 3, polygonOptions.b(), false);
        c.a(parcel, 4, polygonOptions.d());
        c.a(parcel, 5, polygonOptions.e());
        c.a(parcel, 6, polygonOptions.f());
        c.a(parcel, 7, polygonOptions.g());
        c.a(parcel, 8, polygonOptions.h());
        c.a(parcel, 9, polygonOptions.i());
        c.a(parcel, a);
    }

    /* renamed from: a */
    public PolygonOptions createFromParcel(Parcel parcel) {
        float f = 0.0f;
        boolean z = false;
        int b = b.b(parcel);
        ArrayList arrayList = null;
        ArrayList arrayList2 = new ArrayList();
        boolean z2 = false;
        int i = 0;
        int i2 = 0;
        float f2 = 0.0f;
        int i3 = 0;
        while (parcel.dataPosition() < b) {
            int a = b.a(parcel);
            switch (b.a(a)) {
                case 1:
                    i3 = b.f(parcel, a);
                    break;
                case 2:
                    arrayList = b.c(parcel, a, LatLng.a);
                    break;
                case 3:
                    b.a(parcel, a, arrayList2, getClass().getClassLoader());
                    break;
                case e.g.com_facebook_picker_fragment_done_button_text:
                    f2 = b.i(parcel, a);
                    break;
                case e.g.com_facebook_picker_fragment_title_bar_background:
                    i2 = b.f(parcel, a);
                    break;
                case e.g.com_facebook_picker_fragment_done_button_background:
                    i = b.f(parcel, a);
                    break;
                case 7:
                    f = b.i(parcel, a);
                    break;
                case 8:
                    z2 = b.c(parcel, a);
                    break;
                case 9:
                    z = b.c(parcel, a);
                    break;
                default:
                    b.b(parcel, a);
                    break;
            }
        }
        if (parcel.dataPosition() == b) {
            return new PolygonOptions(i3, arrayList, arrayList2, f2, i2, i, f, z2, z);
        }
        throw new b.a("Overread allowed size end=" + b, parcel);
    }

    /* renamed from: a */
    public PolygonOptions[] newArray(int i) {
        return new PolygonOptions[i];
    }
}
