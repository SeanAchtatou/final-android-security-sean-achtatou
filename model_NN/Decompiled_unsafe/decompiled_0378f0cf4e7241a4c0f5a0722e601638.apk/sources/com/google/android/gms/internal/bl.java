package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.internal.b;
import com.google.android.gms.internal.eq;
import java.util.HashSet;
import java.util.Set;

public class bl implements Parcelable.Creator<eq.h> {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.c.a(android.os.Parcel, int, java.lang.String, boolean):void
     arg types: [android.os.Parcel, int, java.lang.String, int]
     candidates:
      com.google.android.gms.internal.c.a(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.internal.c.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.internal.c.a(android.os.Parcel, int, android.os.Parcel, boolean):void
      com.google.android.gms.internal.c.a(android.os.Parcel, int, java.util.List<java.lang.String>, boolean):void
      com.google.android.gms.internal.c.a(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.internal.c.a(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.internal.c.a(android.os.Parcel, int, java.lang.String, boolean):void */
    static void a(eq.h hVar, Parcel parcel, int i) {
        int a = c.a(parcel);
        Set<Integer> e = hVar.e();
        if (e.contains(1)) {
            c.a(parcel, 1, hVar.f());
        }
        if (e.contains(2)) {
            c.a(parcel, 2, hVar.g());
        }
        if (e.contains(3)) {
            c.a(parcel, 3, hVar.h(), true);
        }
        c.a(parcel, a);
    }

    /* renamed from: a */
    public eq.h createFromParcel(Parcel parcel) {
        boolean z = false;
        int b = b.b(parcel);
        HashSet hashSet = new HashSet();
        String str = null;
        int i = 0;
        while (parcel.dataPosition() < b) {
            int a = b.a(parcel);
            switch (b.a(a)) {
                case 1:
                    i = b.f(parcel, a);
                    hashSet.add(1);
                    break;
                case 2:
                    z = b.c(parcel, a);
                    hashSet.add(2);
                    break;
                case 3:
                    str = b.l(parcel, a);
                    hashSet.add(3);
                    break;
                default:
                    b.b(parcel, a);
                    break;
            }
        }
        if (parcel.dataPosition() == b) {
            return new eq.h(hashSet, i, z, str);
        }
        throw new b.a("Overread allowed size end=" + b, parcel);
    }

    /* renamed from: a */
    public eq.h[] newArray(int i) {
        return new eq.h[i];
    }
}
