package com.google.android.gms.location;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.internal.b;
import com.google.android.gms.internal.c;

public class a implements Parcelable.Creator<ActivityRecognitionResult> {
    static void a(ActivityRecognitionResult activityRecognitionResult, Parcel parcel, int i) {
        int a = c.a(parcel);
        c.b(parcel, 1, activityRecognitionResult.c, false);
        c.a(parcel, 1000, activityRecognitionResult.b);
        c.a(parcel, 2, activityRecognitionResult.d);
        c.a(parcel, 3, activityRecognitionResult.e);
        c.a(parcel, a);
    }

    /* renamed from: a */
    public ActivityRecognitionResult createFromParcel(Parcel parcel) {
        ActivityRecognitionResult activityRecognitionResult = new ActivityRecognitionResult();
        int b = b.b(parcel);
        while (parcel.dataPosition() < b) {
            int a = b.a(parcel);
            switch (b.a(a)) {
                case 1:
                    activityRecognitionResult.c = b.c(parcel, a, DetectedActivity.a);
                    break;
                case 2:
                    activityRecognitionResult.d = b.g(parcel, a);
                    break;
                case 3:
                    activityRecognitionResult.e = b.g(parcel, a);
                    break;
                case 1000:
                    activityRecognitionResult.b = b.f(parcel, a);
                    break;
                default:
                    b.b(parcel, a);
                    break;
            }
        }
        if (parcel.dataPosition() == b) {
            return activityRecognitionResult;
        }
        throw new b.a("Overread allowed size end=" + b, parcel);
    }

    /* renamed from: a */
    public ActivityRecognitionResult[] newArray(int i) {
        return new ActivityRecognitionResult[i];
    }
}
