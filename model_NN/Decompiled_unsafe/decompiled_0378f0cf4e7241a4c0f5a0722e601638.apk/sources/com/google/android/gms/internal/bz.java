package com.google.android.gms.internal;

import android.os.Bundle;
import android.os.Parcel;
import com.google.android.gms.games.multiplayer.Participant;
import com.google.android.gms.games.multiplayer.realtime.Room;
import com.google.android.gms.games.multiplayer.realtime.RoomEntity;
import com.google.android.gms.games.multiplayer.realtime.a;
import java.util.ArrayList;

public final class bz extends bn implements Room {
    private final int c;

    public String b() {
        return d("external_match_id");
    }

    public String c() {
        return d("creator_external");
    }

    public long d() {
        return a("creation_timestamp");
    }

    public int describeContents() {
        return 0;
    }

    public int e() {
        return b("status");
    }

    public boolean equals(Object obj) {
        return RoomEntity.a(this, obj);
    }

    public String f() {
        return d("description");
    }

    public ArrayList<Participant> g() {
        ArrayList<Participant> arrayList = new ArrayList<>(this.c);
        for (int i = 0; i < this.c; i++) {
            arrayList.add(new bx(this.a, this.b + i));
        }
        return arrayList;
    }

    public int h() {
        return b("variant");
    }

    public int hashCode() {
        return RoomEntity.a(this);
    }

    public Bundle i() {
        if (!c("has_automatch_criteria")) {
            return null;
        }
        return a.a(b("automatch_min_players"), b("automatch_max_players"), a("automatch_bit_mask"));
    }

    /* renamed from: j */
    public Room a() {
        return new RoomEntity(this);
    }

    public String toString() {
        return RoomEntity.b(this);
    }

    public void writeToParcel(Parcel parcel, int i) {
        ((RoomEntity) a()).writeToParcel(parcel, i);
    }
}
