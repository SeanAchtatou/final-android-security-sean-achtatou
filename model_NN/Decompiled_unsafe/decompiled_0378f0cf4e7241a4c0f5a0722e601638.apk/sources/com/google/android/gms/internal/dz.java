package com.google.android.gms.internal;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.google.android.gms.common.b;
import com.google.android.gms.plus.PlusOneButton;
import com.google.android.gms.plus.a;

public class dz extends LinearLayout implements b.a, b.C0020b {
    private static final int m = Color.parseColor("#666666");
    protected boolean a;
    protected int b = 0;
    protected final LinearLayout c;
    protected final FrameLayout d;
    protected final CompoundButton e;
    protected final aw f;
    protected int g = 1;
    protected String h;
    protected com.google.android.gms.plus.a i;
    protected ap j;
    protected final Resources k;
    protected final LayoutInflater l;
    private final ProgressBar n;
    private final ao[] o = new ao[4];
    private int p = 2;
    private int q = 3;
    private Uri[] r;
    private String[] s;
    private String[] t;
    private b u = new b();

    class a implements View.OnClickListener, PlusOneButton.a {
        private final PlusOneButton.a b;

        public a(PlusOneButton.a aVar) {
            this.b = aVar;
        }

        public void a(Intent intent) {
            Context context = dz.this.getContext();
            if ((context instanceof Activity) && intent != null) {
                ((Activity) context).startActivityForResult(intent, dz.this.b);
            }
        }

        public void onClick(View view) {
            if (view == dz.this.e || view == dz.this.f) {
                Intent e = dz.this.j == null ? null : dz.this.j.e();
                if (this.b != null) {
                    this.b.a(e);
                } else {
                    a(e);
                }
            }
        }
    }

    public class b implements a.c {
        protected b() {
        }

        public void a(com.google.android.gms.common.a aVar, ap apVar) {
            if (dz.this.a) {
                dz.this.a = false;
                dz.this.e.refreshDrawableState();
            }
            if (!aVar.b() || apVar == null) {
                dz.this.d();
                return;
            }
            dz.this.j = apVar;
            dz.this.g();
            dz.this.a();
        }
    }

    class c extends CompoundButton {
        public c(Context context) {
            super(context);
        }

        public void toggle() {
            if (dz.this.a) {
                super.toggle();
                return;
            }
            dz.this.a = true;
            dz.this.c();
        }
    }

    public dz(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        bv.a(context, "Context must not be null.");
        if (com.google.android.gms.common.c.a(context) != 0) {
            this.k = null;
            this.l = null;
        } else {
            Context a2 = a(context);
            this.k = a2.getResources();
            this.l = (LayoutInflater) a2.getSystemService("layout_inflater");
        }
        this.q = a(context, attributeSet);
        this.p = b(context, attributeSet);
        Point point = new Point();
        a(point);
        if (isInEditMode()) {
            TextView textView = new TextView(context);
            textView.setGravity(17);
            textView.setText("[ +1 ]");
            addView(textView, new LinearLayout.LayoutParams(point.x, point.y));
            this.f = null;
            this.n = null;
            this.e = null;
            this.d = null;
            this.c = null;
            return;
        }
        setFocusable(true);
        this.c = new LinearLayout(context);
        this.c.setGravity(17);
        this.c.setOrientation(0);
        addView(this.c);
        this.e = new c(context);
        this.e.setBackgroundDrawable(null);
        this.f = d(context);
        this.d = b(context);
        this.d.addView(this.e, new FrameLayout.LayoutParams(point.x, point.y, 17));
        b(point);
        this.n = c(context);
        this.n.setVisibility(4);
        this.d.addView(this.n, new FrameLayout.LayoutParams(point.x, point.y, 17));
        int length = this.o.length;
        for (int i2 = 0; i2 < length; i2++) {
            this.o[i2] = e(getContext());
        }
        e();
    }

    private int a(Context context, AttributeSet attributeSet) {
        String a2 = a.a("http://schemas.android.com/apk/lib/com.google.android.gms.plus", "size", context, attributeSet, true, false, "PlusOneButton");
        if ("SMALL".equalsIgnoreCase(a2)) {
            return 0;
        }
        if ("MEDIUM".equalsIgnoreCase(a2)) {
            return 1;
        }
        if ("TALL".equalsIgnoreCase(a2)) {
            return 2;
        }
        return "STANDARD".equalsIgnoreCase(a2) ? 3 : 3;
    }

    private Context a(Context context) {
        try {
            return getContext().createPackageContext("com.google.android.gms", 4);
        } catch (PackageManager.NameNotFoundException e2) {
            if (Log.isLoggable("PlusOneButton", 5)) {
                Log.w("PlusOneButton", "Google Play services is not installed");
            }
            return null;
        }
    }

    private void a(int i2, int i3) {
        this.g = i3;
        this.q = i2;
        a();
    }

    private void a(Point point) {
        int i2 = 24;
        int i3 = 20;
        switch (this.q) {
            case 0:
                i3 = 14;
                break;
            case 1:
                i2 = 32;
                break;
            case 2:
                i2 = 50;
                break;
            default:
                i2 = 38;
                i3 = 24;
                break;
        }
        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        float applyDimension = TypedValue.applyDimension(1, (float) i2, displayMetrics);
        float applyDimension2 = TypedValue.applyDimension(1, (float) i3, displayMetrics);
        point.x = (int) (((double) applyDimension) + 0.5d);
        point.y = (int) (((double) applyDimension2) + 0.5d);
    }

    private void a(View view) {
        int applyDimension = (int) TypedValue.applyDimension(1, 3.0f, getContext().getResources().getDisplayMetrics());
        int applyDimension2 = (int) TypedValue.applyDimension(1, 5.0f, getContext().getResources().getDisplayMetrics());
        if (this.p != 2) {
            applyDimension2 = 0;
        }
        if (!(this.q == 2 && this.p == 1)) {
            applyDimension = 0;
        }
        view.setPadding(applyDimension2, 0, 0, applyDimension);
    }

    private void a(Uri[] uriArr) {
        this.r = uriArr;
        l();
    }

    private void a(String[] strArr) {
        this.s = strArr;
        k();
    }

    private static int b(int i2, int i3) {
        switch (i2) {
            case 0:
                return 11;
            case 1:
            default:
                return 13;
            case 2:
                return i3 != 2 ? 15 : 13;
        }
    }

    private int b(Context context, AttributeSet attributeSet) {
        String a2 = a.a("http://schemas.android.com/apk/lib/com.google.android.gms.plus", "annotation", context, attributeSet, true, false, "PlusOneButton");
        if ("INLINE".equalsIgnoreCase(a2)) {
            return 2;
        }
        if ("NONE".equalsIgnoreCase(a2)) {
            return 0;
        }
        if ("BUBBLE".equalsIgnoreCase(a2)) {
        }
        return 1;
    }

    private FrameLayout b(Context context) {
        FrameLayout frameLayout = new FrameLayout(context);
        frameLayout.setFocusable(false);
        return frameLayout;
    }

    private void b(Point point) {
        point.y = (int) (((float) point.y) - TypedValue.applyDimension(1, 6.0f, getResources().getDisplayMetrics()));
        point.x = point.y;
    }

    private void b(String[] strArr) {
        this.t = strArr;
        k();
    }

    private ProgressBar c(Context context) {
        ProgressBar progressBar = new ProgressBar(context, null, 16843400);
        progressBar.setFocusable(false);
        progressBar.setIndeterminate(true);
        return progressBar;
    }

    private aw d(Context context) {
        aw awVar = new aw(context);
        awVar.setFocusable(false);
        awVar.setGravity(17);
        awVar.a();
        awVar.a(0, TypedValue.applyDimension(2, (float) b(this.q, this.p), context.getResources().getDisplayMetrics()));
        awVar.setTextColor(m);
        awVar.setVisibility(0);
        return awVar;
    }

    private ao e(Context context) {
        ao aoVar = new ao(context);
        aoVar.setVisibility(8);
        return aoVar;
    }

    private void i() {
        boolean z = true;
        int applyDimension = (int) TypedValue.applyDimension(1, 5.0f, getContext().getResources().getDisplayMetrics());
        int applyDimension2 = (int) TypedValue.applyDimension(1, 1.0f, getContext().getResources().getDisplayMetrics());
        int length = this.o.length;
        for (int i2 = 0; i2 < length; i2++) {
            if (this.o[i2].getVisibility() == 0) {
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(this.o[i2].getLayoutParams());
                if (z) {
                    layoutParams.setMargins(applyDimension, 0, applyDimension2, 0);
                    z = false;
                } else {
                    layoutParams.setMargins(applyDimension2, 0, applyDimension2, 0);
                }
                this.o[i2].setLayoutParams(layoutParams);
            }
        }
    }

    private LinearLayout.LayoutParams j() {
        LinearLayout.LayoutParams layoutParams;
        int i2 = 0;
        switch (this.p) {
            case 1:
                layoutParams = new LinearLayout.LayoutParams(-2, -2);
                break;
            case 2:
                layoutParams = new LinearLayout.LayoutParams(-2, -1);
                break;
            default:
                layoutParams = new LinearLayout.LayoutParams(-2, -2);
                break;
        }
        layoutParams.bottomMargin = this.q == 2 ? 1 : 0;
        if (this.q != 2) {
            i2 = 1;
        }
        layoutParams.leftMargin = i2;
        return layoutParams;
    }

    private void k() {
        switch (this.p) {
            case 1:
                this.f.a(this.t);
                this.f.setVisibility(0);
                return;
            case 2:
                this.f.a(this.s);
                this.f.setVisibility(0);
                return;
            default:
                this.f.a((String[]) null);
                this.f.setVisibility(8);
                return;
        }
    }

    private void l() {
        if (this.r == null || this.p != 2) {
            for (ao visibility : this.o) {
                visibility.setVisibility(8);
            }
        } else {
            Point point = new Point();
            a(point);
            point.x = point.y;
            int length = this.o.length;
            int length2 = this.r.length;
            int i2 = 0;
            while (i2 < length) {
                Uri uri = i2 < length2 ? this.r[i2] : null;
                if (uri == null) {
                    this.o[i2].setVisibility(8);
                } else {
                    this.o[i2].setLayoutParams(new LinearLayout.LayoutParams(point.x, point.y));
                    this.o[i2].a(uri, point.y);
                    this.o[i2].setVisibility(0);
                }
                i2++;
            }
        }
        i();
    }

    private Drawable m() {
        if (this.k == null) {
            return null;
        }
        return this.k.getDrawable(this.k.getIdentifier(n(), "drawable", "com.google.android.gms"));
    }

    private String n() {
        switch (this.q) {
            case 0:
                return "ic_plusone_small";
            case 1:
                return "ic_plusone_medium";
            case 2:
                return "ic_plusone_tall";
            default:
                return "ic_plusone_standard";
        }
    }

    private Uri o() {
        return by.a(p());
    }

    private String p() {
        switch (this.q) {
            case 0:
                return "global_count_bubble_small";
            case 1:
                return "global_count_bubble_medium";
            case 2:
                return "global_count_bubble_tall";
            default:
                return "global_count_bubble_standard";
        }
    }

    /* access modifiers changed from: protected */
    public void a() {
        if (!isInEditMode()) {
            this.c.removeAllViews();
            Point point = new Point();
            a(point);
            this.e.setLayoutParams(new FrameLayout.LayoutParams(point.x, point.y, 17));
            b(point);
            this.n.setLayoutParams(new FrameLayout.LayoutParams(point.x, point.y, 17));
            if (this.p == 1) {
                this.f.a(o());
            } else {
                this.f.a((Uri) null);
            }
            l();
            this.f.setLayoutParams(j());
            this.f.a(0, TypedValue.applyDimension(2, (float) b(this.q, this.p), getContext().getResources().getDisplayMetrics()));
            a(this.f);
            if (this.q == 2 && this.p == 1) {
                this.c.setOrientation(1);
                this.c.addView(this.f);
                this.c.addView(this.d);
            } else {
                this.c.setOrientation(0);
                this.c.addView(this.d);
                for (ao addView : this.o) {
                    this.c.addView(addView);
                }
                this.c.addView(this.f);
            }
            requestLayout();
        }
    }

    public void a(Bundle bundle) {
        if (this.h != null) {
            this.i.a(this.u, this.h);
        }
    }

    public void a(com.google.android.gms.common.a aVar) {
        d();
    }

    public void a(com.google.android.gms.plus.a aVar, String str, int i2) {
        bv.a(aVar, "Plus client must not be null.");
        bv.a(str, "URL must not be null.");
        bv.a(i2 >= 0 && i2 <= 65535, "activityRequestCode must be an unsigned 16 bit integer.");
        this.b = i2;
        this.h = str;
        if (aVar != this.i) {
            if (this.i != null) {
                this.i.c((b.a) this);
                this.i.c((b.C0020b) this);
            }
            this.i = aVar;
            this.i.a((b.a) this);
            this.i.a((b.C0020b) this);
            for (ao a2 : this.o) {
                a2.a(aVar);
            }
        } else if (this.i.b()) {
            a((Bundle) null);
        }
        a();
    }

    public void b() {
    }

    public void c() {
        setType(2);
        this.n.setVisibility(0);
        h();
    }

    public void d() {
        setType(3);
        this.n.setVisibility(4);
        h();
    }

    /* access modifiers changed from: protected */
    public void e() {
        setType(1);
        this.n.setVisibility(4);
        h();
    }

    /* access modifiers changed from: protected */
    public void f() {
        setType(0);
        this.n.setVisibility(4);
        h();
    }

    /* access modifiers changed from: protected */
    public void g() {
        if (this.j != null) {
            a(this.j.c());
            b(new String[]{this.j.b()});
            a(this.j.d());
            if (this.j.a()) {
                f();
            } else {
                e();
            }
        }
    }

    /* access modifiers changed from: protected */
    public void h() {
        this.e.setButtonDrawable(m());
        switch (this.g) {
            case 0:
                this.e.setEnabled(true);
                this.e.setChecked(true);
                return;
            case 1:
                this.e.setEnabled(true);
                this.e.setChecked(false);
                return;
            case 2:
                this.e.setEnabled(false);
                this.e.setChecked(true);
                return;
            default:
                this.e.setEnabled(false);
                this.e.setChecked(false);
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (this.i != null) {
            if (!this.i.b((b.a) this)) {
                this.i.a((b.a) this);
            }
            if (!this.i.b((b.C0020b) this)) {
                this.i.a((b.C0020b) this);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (this.i != null) {
            if (this.i.b((b.a) this)) {
                this.i.c((b.a) this);
            }
            if (this.i.b((b.C0020b) this)) {
                this.i.c((b.C0020b) this);
            }
        }
    }

    public boolean performClick() {
        return this.e.performClick();
    }

    public void setAnnotation(int i2) {
        bv.a(Integer.valueOf(i2), "Annotation must not be null.");
        this.p = i2;
        k();
        a();
    }

    public void setOnClickListener(View.OnClickListener onClickListener) {
        this.e.setOnClickListener(onClickListener);
        this.f.setOnClickListener(onClickListener);
    }

    public void setOnPlusOneClickListener(PlusOneButton.a aVar) {
        setOnClickListener(new a(aVar));
    }

    public void setSize(int i2) {
        a(i2, this.g);
    }

    public void setType(int i2) {
        a(this.q, i2);
    }
}
