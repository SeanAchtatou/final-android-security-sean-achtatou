package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.a.e;
import com.google.android.gms.internal.b;

public class e implements Parcelable.Creator<ag> {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.c.a(android.os.Parcel, int, java.lang.String, boolean):void
     arg types: [android.os.Parcel, int, java.lang.String, int]
     candidates:
      com.google.android.gms.internal.c.a(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.internal.c.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.internal.c.a(android.os.Parcel, int, android.os.Parcel, boolean):void
      com.google.android.gms.internal.c.a(android.os.Parcel, int, java.util.List<java.lang.String>, boolean):void
      com.google.android.gms.internal.c.a(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.internal.c.a(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.internal.c.a(android.os.Parcel, int, java.lang.String, boolean):void */
    static void a(ag agVar, Parcel parcel, int i) {
        int a = c.a(parcel);
        c.a(parcel, 1, agVar.b());
        c.a(parcel, 1000, agVar.a());
        c.a(parcel, 2, agVar.c());
        c.a(parcel, 3, agVar.d(), false);
        c.a(parcel, 4, agVar.e(), false);
        c.a(parcel, 5, agVar.f(), false);
        c.a(parcel, 6, agVar.g(), false);
        c.a(parcel, a);
    }

    /* renamed from: a */
    public ag createFromParcel(Parcel parcel) {
        int i = 0;
        String str = null;
        int b = b.b(parcel);
        String str2 = null;
        String str3 = null;
        String str4 = null;
        int i2 = 0;
        int i3 = 0;
        while (parcel.dataPosition() < b) {
            int a = b.a(parcel);
            switch (b.a(a)) {
                case 1:
                    i2 = b.f(parcel, a);
                    break;
                case 2:
                    i = b.f(parcel, a);
                    break;
                case 3:
                    str4 = b.l(parcel, a);
                    break;
                case e.g.com_facebook_picker_fragment_done_button_text:
                    str3 = b.l(parcel, a);
                    break;
                case e.g.com_facebook_picker_fragment_title_bar_background:
                    str2 = b.l(parcel, a);
                    break;
                case e.g.com_facebook_picker_fragment_done_button_background:
                    str = b.l(parcel, a);
                    break;
                case 1000:
                    i3 = b.f(parcel, a);
                    break;
                default:
                    b.b(parcel, a);
                    break;
            }
        }
        if (parcel.dataPosition() == b) {
            return new ag(i3, i2, i, str4, str3, str2, str);
        }
        throw new b.a("Overread allowed size end=" + b, parcel);
    }

    /* renamed from: a */
    public ag[] newArray(int i) {
        return new ag[i];
    }
}
