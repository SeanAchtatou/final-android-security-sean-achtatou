package com.google.android.gms.internal;

import android.os.Parcel;
import com.google.android.gms.internal.an;

public class ai implements ae {
    public static final f a = new f();
    private final int b;
    private final ak c;

    ai(int i, ak akVar) {
        this.b = i;
        this.c = akVar;
    }

    private ai(ak akVar) {
        this.b = 1;
        this.c = akVar;
    }

    public static ai a(an.b<?, ?> bVar) {
        if (bVar instanceof ak) {
            return new ai((ak) bVar);
        }
        throw new IllegalArgumentException("Unsupported safe parcelable field converter class.");
    }

    /* access modifiers changed from: package-private */
    public int a() {
        return this.b;
    }

    /* access modifiers changed from: package-private */
    public ak b() {
        return this.c;
    }

    public an.b<?, ?> c() {
        if (this.c != null) {
            return this.c;
        }
        throw new IllegalStateException("There was no converter wrapped in this ConverterWrapper.");
    }

    public int describeContents() {
        f fVar = a;
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        f fVar = a;
        f.a(this, parcel, i);
    }
}
