package com.google.android.gms.internal;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.ParcelFileDescriptor;
import android.util.Log;
import android.widget.ImageView;
import com.google.android.gms.common.b;
import com.google.android.gms.plus.a;
import java.io.IOException;

public class ao extends ImageView implements b.a, a.b {
    private int a;
    private Uri b;
    /* access modifiers changed from: private */
    public boolean c;
    private boolean d;
    /* access modifiers changed from: private */
    public Bitmap e;
    private com.google.android.gms.plus.a f;

    class a extends AsyncTask<ParcelFileDescriptor, Void, Bitmap> {
        private final int b;

        a(int i) {
            this.b = i;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public Bitmap doInBackground(ParcelFileDescriptor... parcelFileDescriptorArr) {
            ParcelFileDescriptor parcelFileDescriptor = parcelFileDescriptorArr[0];
            try {
                Bitmap decodeFileDescriptor = BitmapFactory.decodeFileDescriptor(parcelFileDescriptor.getFileDescriptor());
                if (this.b > 0) {
                    decodeFileDescriptor = ao.b(decodeFileDescriptor, this.b);
                    try {
                    } catch (IOException e) {
                        Log.e("PlusImageView", "closed failed", e);
                    }
                } else {
                    try {
                        parcelFileDescriptor.close();
                    } catch (IOException e2) {
                        Log.e("PlusImageView", "closed failed", e2);
                    }
                }
                return decodeFileDescriptor;
            } finally {
                try {
                    parcelFileDescriptor.close();
                } catch (IOException e3) {
                    Log.e("PlusImageView", "closed failed", e3);
                }
            }
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void onPostExecute(Bitmap bitmap) {
            Bitmap unused = ao.this.e = bitmap;
            if (ao.this.c) {
                ao.this.setImageBitmap(ao.this.e);
            }
        }
    }

    public ao(Context context) {
        super(context);
    }

    private void a() {
        boolean z = this.b != null && "android.resource".equals(this.b.getScheme());
        if (this.d) {
            if (this.b == null) {
                setImageBitmap(null);
            } else if (z || (this.f != null && this.f.b())) {
                if (z) {
                    setImageURI(this.b);
                } else {
                    this.f.a(this, this.b, this.a);
                }
                this.d = false;
            }
        }
    }

    /* access modifiers changed from: private */
    public static Bitmap b(Bitmap bitmap, int i) {
        double width = (double) bitmap.getWidth();
        double height = (double) bitmap.getHeight();
        double d2 = width > height ? ((double) i) / width : ((double) i) / height;
        return Bitmap.createScaledBitmap(bitmap, (int) ((width * d2) + 0.5d), (int) ((d2 * height) + 0.5d), true);
    }

    public void a(Uri uri, int i) {
        boolean z = false;
        boolean equals = this.b == null ? uri == null : this.b.equals(uri);
        if (this.a == i) {
            z = true;
        }
        if (!equals || !z) {
            this.b = uri;
            this.a = i;
            this.d = true;
            a();
        }
    }

    public void a(Bundle bundle) {
        a();
    }

    public void a(com.google.android.gms.common.a aVar, ParcelFileDescriptor parcelFileDescriptor) {
        if (aVar.b()) {
            this.d = false;
            if (parcelFileDescriptor != null) {
                new a(this.a).execute(parcelFileDescriptor);
            }
        }
    }

    public void a(com.google.android.gms.plus.a aVar) {
        if (aVar != this.f) {
            if (this.f != null && this.f.b(this)) {
                this.f.c(this);
            }
            this.f = aVar;
            this.f.a(this);
        }
    }

    public void b() {
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.c = true;
        if (this.f != null && !this.f.b(this)) {
            this.f.a(this);
        }
        if (this.e != null) {
            setImageBitmap(this.e);
        }
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.c = false;
        if (this.f != null && this.f.b(this)) {
            this.f.c(this);
        }
    }
}
