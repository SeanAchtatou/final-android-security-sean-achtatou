package com.google.android.gms.maps.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.a.e;
import com.google.android.gms.internal.b;
import com.google.android.gms.internal.c;

public class b implements Parcelable.Creator<CameraPosition> {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.c.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.maps.model.LatLng, int, int]
     candidates:
      com.google.android.gms.internal.c.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.internal.c.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    static void a(CameraPosition cameraPosition, Parcel parcel, int i) {
        int a = c.a(parcel);
        c.a(parcel, 1, cameraPosition.a());
        c.a(parcel, 2, (Parcelable) cameraPosition.b, i, false);
        c.a(parcel, 3, cameraPosition.c);
        c.a(parcel, 4, cameraPosition.d);
        c.a(parcel, 5, cameraPosition.e);
        c.a(parcel, a);
    }

    /* renamed from: a */
    public CameraPosition createFromParcel(Parcel parcel) {
        float f = 0.0f;
        int b = com.google.android.gms.internal.b.b(parcel);
        int i = 0;
        LatLng latLng = null;
        float f2 = 0.0f;
        float f3 = 0.0f;
        while (parcel.dataPosition() < b) {
            int a = com.google.android.gms.internal.b.a(parcel);
            switch (com.google.android.gms.internal.b.a(a)) {
                case 1:
                    i = com.google.android.gms.internal.b.f(parcel, a);
                    break;
                case 2:
                    latLng = (LatLng) com.google.android.gms.internal.b.a(parcel, a, LatLng.a);
                    break;
                case 3:
                    f3 = com.google.android.gms.internal.b.i(parcel, a);
                    break;
                case e.g.com_facebook_picker_fragment_done_button_text:
                    f2 = com.google.android.gms.internal.b.i(parcel, a);
                    break;
                case e.g.com_facebook_picker_fragment_title_bar_background:
                    f = com.google.android.gms.internal.b.i(parcel, a);
                    break;
                default:
                    com.google.android.gms.internal.b.b(parcel, a);
                    break;
            }
        }
        if (parcel.dataPosition() == b) {
            return new CameraPosition(i, latLng, f3, f2, f);
        }
        throw new b.a("Overread allowed size end=" + b, parcel);
    }

    /* renamed from: a */
    public CameraPosition[] newArray(int i) {
        return new CameraPosition[i];
    }
}
