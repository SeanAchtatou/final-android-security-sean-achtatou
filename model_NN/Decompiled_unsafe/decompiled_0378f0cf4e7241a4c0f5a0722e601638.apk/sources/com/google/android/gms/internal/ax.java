package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.a.e;
import com.google.android.gms.internal.b;
import java.util.ArrayList;

public class ax implements Parcelable.Creator<eb> {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.c.a(android.os.Parcel, int, java.lang.String, boolean):void
     arg types: [android.os.Parcel, int, java.lang.String, int]
     candidates:
      com.google.android.gms.internal.c.a(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.internal.c.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.internal.c.a(android.os.Parcel, int, android.os.Parcel, boolean):void
      com.google.android.gms.internal.c.a(android.os.Parcel, int, java.util.List<java.lang.String>, boolean):void
      com.google.android.gms.internal.c.a(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.internal.c.a(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.internal.c.a(android.os.Parcel, int, java.lang.String, boolean):void */
    static void a(eb ebVar, Parcel parcel, int i) {
        int a = c.a(parcel);
        c.a(parcel, 1, ebVar.b(), false);
        c.a(parcel, 1000, ebVar.a());
        c.b(parcel, 2, ebVar.c(), false);
        c.b(parcel, 3, ebVar.d(), false);
        c.a(parcel, 4, ebVar.e());
        c.a(parcel, a);
    }

    /* renamed from: a */
    public eb createFromParcel(Parcel parcel) {
        boolean z = false;
        ArrayList arrayList = null;
        int b = b.b(parcel);
        ArrayList arrayList2 = null;
        String str = null;
        int i = 0;
        while (parcel.dataPosition() < b) {
            int a = b.a(parcel);
            switch (b.a(a)) {
                case 1:
                    str = b.l(parcel, a);
                    break;
                case 2:
                    arrayList2 = b.c(parcel, a, ag.a);
                    break;
                case 3:
                    arrayList = b.c(parcel, a, ag.a);
                    break;
                case e.g.com_facebook_picker_fragment_done_button_text:
                    z = b.c(parcel, a);
                    break;
                case 1000:
                    i = b.f(parcel, a);
                    break;
                default:
                    b.b(parcel, a);
                    break;
            }
        }
        if (parcel.dataPosition() == b) {
            return new eb(i, str, arrayList2, arrayList, z);
        }
        throw new b.a("Overread allowed size end=" + b, parcel);
    }

    /* renamed from: a */
    public eb[] newArray(int i) {
        return new eb[i];
    }
}
