package com.google.android.gms.games;

import android.net.Uri;
import android.os.Parcelable;
import com.google.android.gms.common.a.a;

public interface Player extends Parcelable, a<Player> {
    String b();

    String c();

    Uri d();

    Uri e();

    long f();
}
