package com.google.android.gms.location;

import android.os.Parcel;
import android.os.SystemClock;
import com.google.android.gms.internal.ae;

public final class LocationRequest implements ae {
    public static final c i = new c();
    int a;
    int b = 102;
    long c = 3600000;
    long d = ((long) (((double) this.c) / 6.0d));
    boolean e = false;
    long f = Long.MAX_VALUE;
    int g = Integer.MAX_VALUE;
    float h = 0.0f;

    public static String a(int i2) {
        switch (i2) {
            case 100:
                return "PRIORITY_HIGH_ACCURACY";
            case 101:
            case 103:
            default:
                return "???";
            case 102:
                return "PRIORITY_BALANCED_POWER_ACCURACY";
            case 104:
                return "PRIORITY_LOW_POWER";
        }
    }

    public int describeContents() {
        return 0;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Request[").append(a(this.b));
        if (this.b != 105) {
            sb.append(" requested=");
            sb.append(this.c + "ms");
        }
        sb.append(" fastest=");
        sb.append(this.d + "ms");
        if (this.f != Long.MAX_VALUE) {
            long elapsedRealtime = this.f - SystemClock.elapsedRealtime();
            sb.append(" expireIn=");
            sb.append(elapsedRealtime + "ms");
        }
        if (this.g != Integer.MAX_VALUE) {
            sb.append(" num=").append(this.g);
        }
        sb.append(']');
        return sb.toString();
    }

    public void writeToParcel(Parcel parcel, int i2) {
        c cVar = i;
        c.a(this, parcel, i2);
    }
}
