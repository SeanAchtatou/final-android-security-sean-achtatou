package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.a.e;
import com.google.android.gms.internal.b;
import com.google.android.gms.internal.eq;
import java.util.HashSet;
import java.util.Set;

public class bj implements Parcelable.Creator<eq.g> {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.c.a(android.os.Parcel, int, java.lang.String, boolean):void
     arg types: [android.os.Parcel, int, java.lang.String, int]
     candidates:
      com.google.android.gms.internal.c.a(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.internal.c.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.internal.c.a(android.os.Parcel, int, android.os.Parcel, boolean):void
      com.google.android.gms.internal.c.a(android.os.Parcel, int, java.util.List<java.lang.String>, boolean):void
      com.google.android.gms.internal.c.a(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.internal.c.a(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.internal.c.a(android.os.Parcel, int, java.lang.String, boolean):void */
    static void a(eq.g gVar, Parcel parcel, int i) {
        int a = c.a(parcel);
        Set<Integer> e = gVar.e();
        if (e.contains(1)) {
            c.a(parcel, 1, gVar.f());
        }
        if (e.contains(2)) {
            c.a(parcel, 2, gVar.g(), true);
        }
        if (e.contains(3)) {
            c.a(parcel, 3, gVar.h(), true);
        }
        if (e.contains(4)) {
            c.a(parcel, 4, gVar.i(), true);
        }
        if (e.contains(5)) {
            c.a(parcel, 5, gVar.j(), true);
        }
        if (e.contains(6)) {
            c.a(parcel, 6, gVar.k(), true);
        }
        if (e.contains(7)) {
            c.a(parcel, 7, gVar.l());
        }
        if (e.contains(8)) {
            c.a(parcel, 8, gVar.m(), true);
        }
        if (e.contains(9)) {
            c.a(parcel, 9, gVar.n(), true);
        }
        if (e.contains(10)) {
            c.a(parcel, 10, gVar.o());
        }
        c.a(parcel, a);
    }

    /* renamed from: a */
    public eq.g createFromParcel(Parcel parcel) {
        int i = 0;
        String str = null;
        int b = b.b(parcel);
        HashSet hashSet = new HashSet();
        String str2 = null;
        boolean z = false;
        String str3 = null;
        String str4 = null;
        String str5 = null;
        String str6 = null;
        String str7 = null;
        int i2 = 0;
        while (parcel.dataPosition() < b) {
            int a = b.a(parcel);
            switch (b.a(a)) {
                case 1:
                    i2 = b.f(parcel, a);
                    hashSet.add(1);
                    break;
                case 2:
                    str7 = b.l(parcel, a);
                    hashSet.add(2);
                    break;
                case 3:
                    str6 = b.l(parcel, a);
                    hashSet.add(3);
                    break;
                case e.g.com_facebook_picker_fragment_done_button_text:
                    str5 = b.l(parcel, a);
                    hashSet.add(4);
                    break;
                case e.g.com_facebook_picker_fragment_title_bar_background:
                    str4 = b.l(parcel, a);
                    hashSet.add(5);
                    break;
                case e.g.com_facebook_picker_fragment_done_button_background:
                    str3 = b.l(parcel, a);
                    hashSet.add(6);
                    break;
                case 7:
                    z = b.c(parcel, a);
                    hashSet.add(7);
                    break;
                case 8:
                    str2 = b.l(parcel, a);
                    hashSet.add(8);
                    break;
                case 9:
                    str = b.l(parcel, a);
                    hashSet.add(9);
                    break;
                case 10:
                    i = b.f(parcel, a);
                    hashSet.add(10);
                    break;
                default:
                    b.b(parcel, a);
                    break;
            }
        }
        if (parcel.dataPosition() == b) {
            return new eq.g(hashSet, i2, str7, str6, str5, str4, str3, z, str2, str, i);
        }
        throw new b.a("Overread allowed size end=" + b, parcel);
    }

    /* renamed from: a */
    public eq.g[] newArray(int i) {
        return new eq.g[i];
    }
}
