package com.google.android.gms.games;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.internal.bp;
import com.google.android.gms.internal.bu;

public final class PlayerEntity implements Player {
    public static final Parcelable.Creator<PlayerEntity> CREATOR = new Parcelable.Creator<PlayerEntity>() {
        /* renamed from: a */
        public PlayerEntity createFromParcel(Parcel parcel) {
            String readString = parcel.readString();
            String readString2 = parcel.readString();
            String readString3 = parcel.readString();
            String readString4 = parcel.readString();
            return new PlayerEntity(readString, readString2, readString3 == null ? null : Uri.parse(readString3), readString4 == null ? null : Uri.parse(readString4), parcel.readLong());
        }

        /* renamed from: a */
        public PlayerEntity[] newArray(int i) {
            return new PlayerEntity[i];
        }
    };
    private final String a;
    private final String b;
    private final Uri c;
    private final Uri d;
    private final long e;

    public PlayerEntity(Player player) {
        this.a = player.b();
        this.b = player.c();
        this.c = player.d();
        this.d = player.e();
        this.e = player.f();
        bp.a(this.a);
        bp.a(this.b);
        bp.a(this.e > 0);
    }

    private PlayerEntity(String str, String str2, Uri uri, Uri uri2, long j) {
        this.a = str;
        this.b = str2;
        this.c = uri;
        this.d = uri2;
        this.e = j;
    }

    public static int a(Player player) {
        return bu.a(player.b(), player.c(), player.d(), player.e(), Long.valueOf(player.f()));
    }

    public static boolean a(Player player, Object obj) {
        if (!(obj instanceof Player)) {
            return false;
        }
        if (player == obj) {
            return true;
        }
        Player player2 = (Player) obj;
        return bu.a(player2.b(), player.b()) && bu.a(player2.c(), player.c()) && bu.a(player2.d(), player.d()) && bu.a(player2.e(), player.e()) && bu.a(Long.valueOf(player2.f()), Long.valueOf(player.f()));
    }

    public static String b(Player player) {
        return bu.a(player).a("PlayerId", player.b()).a("DisplayName", player.c()).a("IconImageUri", player.d()).a("HiResImageUri", player.e()).a("RetrievedTimestamp", Long.valueOf(player.f())).toString();
    }

    public String b() {
        return this.a;
    }

    public String c() {
        return this.b;
    }

    public Uri d() {
        return this.c;
    }

    public int describeContents() {
        return 0;
    }

    public Uri e() {
        return this.d;
    }

    public boolean equals(Object obj) {
        return a(this, obj);
    }

    public long f() {
        return this.e;
    }

    /* renamed from: g */
    public Player a() {
        return this;
    }

    public int hashCode() {
        return a(this);
    }

    public String toString() {
        return b(this);
    }

    public void writeToParcel(Parcel parcel, int i) {
        String str = null;
        parcel.writeString(this.a);
        parcel.writeString(this.b);
        parcel.writeString(this.c == null ? null : this.c.toString());
        if (this.d != null) {
            str = this.d.toString();
        }
        parcel.writeString(str);
        parcel.writeLong(this.e);
    }
}
