package com.google.android.gms.games.multiplayer.realtime;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.games.multiplayer.Participant;
import com.google.android.gms.games.multiplayer.ParticipantEntity;
import com.google.android.gms.internal.bu;
import java.util.ArrayList;

public final class RoomEntity implements Room {
    public static final Parcelable.Creator<RoomEntity> CREATOR = new Parcelable.Creator<RoomEntity>() {
        /* renamed from: a */
        public RoomEntity createFromParcel(Parcel parcel) {
            String readString = parcel.readString();
            String readString2 = parcel.readString();
            long readLong = parcel.readLong();
            int readInt = parcel.readInt();
            String readString3 = parcel.readString();
            int readInt2 = parcel.readInt();
            Bundle readBundle = parcel.readBundle();
            int readInt3 = parcel.readInt();
            ArrayList arrayList = new ArrayList(readInt3);
            for (int i = 0; i < readInt3; i++) {
                arrayList.add(ParticipantEntity.CREATOR.createFromParcel(parcel));
            }
            return new RoomEntity(readString, readString2, readLong, readInt, readString3, readInt2, readBundle, arrayList);
        }

        /* renamed from: a */
        public RoomEntity[] newArray(int i) {
            return new RoomEntity[i];
        }
    };
    private final String a;
    private final String b;
    private final long c;
    private final int d;
    private final String e;
    private final int f;
    private final Bundle g;
    private final ArrayList<Participant> h;

    public RoomEntity(Room room) {
        this.a = room.b();
        this.b = room.c();
        this.c = room.d();
        this.d = room.e();
        this.e = room.f();
        this.f = room.h();
        this.g = room.i();
        ArrayList<Participant> g2 = room.g();
        int size = g2.size();
        this.h = new ArrayList<>(size);
        for (int i = 0; i < size; i++) {
            this.h.add(g2.get(i).a());
        }
    }

    private RoomEntity(String str, String str2, long j, int i, String str3, int i2, Bundle bundle, ArrayList<Participant> arrayList) {
        this.a = str;
        this.b = str2;
        this.c = j;
        this.d = i;
        this.e = str3;
        this.f = i2;
        this.g = bundle;
        this.h = arrayList;
    }

    public static int a(Room room) {
        return bu.a(room.b(), room.c(), Long.valueOf(room.d()), Integer.valueOf(room.e()), room.f(), Integer.valueOf(room.h()), room.i(), room.g());
    }

    public static boolean a(Room room, Object obj) {
        if (!(obj instanceof Room)) {
            return false;
        }
        if (room == obj) {
            return true;
        }
        Room room2 = (Room) obj;
        return bu.a(room2.b(), room.b()) && bu.a(room2.c(), room.c()) && bu.a(Long.valueOf(room2.d()), Long.valueOf(room.d())) && bu.a(Integer.valueOf(room2.e()), Integer.valueOf(room.e())) && bu.a(room2.f(), room.f()) && bu.a(Integer.valueOf(room2.h()), Integer.valueOf(room.h())) && bu.a(room2.i(), room.i()) && bu.a(room2.g(), room.g());
    }

    public static String b(Room room) {
        return bu.a(room).a("RoomId", room.b()).a("CreatorId", room.c()).a("CreationTimestamp", Long.valueOf(room.d())).a("RoomStatus", Integer.valueOf(room.e())).a("Description", room.f()).a("Variant", Integer.valueOf(room.h())).a("AutoMatchCriteria", room.i()).a("Participants", room.g()).toString();
    }

    public String b() {
        return this.a;
    }

    public String c() {
        return this.b;
    }

    public long d() {
        return this.c;
    }

    public int describeContents() {
        return 0;
    }

    public int e() {
        return this.d;
    }

    public boolean equals(Object obj) {
        return a(this, obj);
    }

    public String f() {
        return this.e;
    }

    public ArrayList<Participant> g() {
        return this.h;
    }

    public int h() {
        return this.f;
    }

    public int hashCode() {
        return a(this);
    }

    public Bundle i() {
        return this.g;
    }

    /* renamed from: j */
    public Room a() {
        return this;
    }

    public String toString() {
        return b(this);
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.a);
        parcel.writeString(this.b);
        parcel.writeLong(this.c);
        parcel.writeInt(this.d);
        parcel.writeString(this.e);
        parcel.writeInt(this.f);
        parcel.writeBundle(this.g);
        int size = this.h.size();
        parcel.writeInt(size);
        for (int i2 = 0; i2 < size; i2++) {
            this.h.get(i2).writeToParcel(parcel, i);
        }
    }
}
