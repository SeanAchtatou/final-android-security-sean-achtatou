package com.google.android.gms.plus;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.AttributeSet;
import android.view.ViewGroup;
import com.google.android.gms.internal.bv;
import com.google.android.gms.internal.dz;

public final class PlusOneButton extends ViewGroup {
    private final dz a;

    public interface a {
        void a(Intent intent);
    }

    public PlusOneButton(Context context) {
        this(context, null);
    }

    public PlusOneButton(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.a = new dz(context, attributeSet);
        addView(this.a);
        if (!isInEditMode()) {
            setOnPlusOneClickListener(null);
        }
    }

    public void a(a aVar, String str, int i) {
        bv.a(getContext() instanceof Activity, "To use this method, the PlusOneButton must be placed in an Activity. Use initialize(PlusClient, String, OnPlusOneClickListener).");
        this.a.a(aVar, str, i);
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        this.a.layout(0, 0, i3 - i, i4 - i2);
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        dz dzVar = this.a;
        measureChild(dzVar, i, i2);
        setMeasuredDimension(dzVar.getMeasuredWidth(), dzVar.getMeasuredHeight());
    }

    public void setAnnotation(int i) {
        this.a.setAnnotation(i);
    }

    public void setOnPlusOneClickListener(a aVar) {
        this.a.setOnPlusOneClickListener(aVar);
    }

    public void setSize(int i) {
        this.a.setSize(i);
    }
}
