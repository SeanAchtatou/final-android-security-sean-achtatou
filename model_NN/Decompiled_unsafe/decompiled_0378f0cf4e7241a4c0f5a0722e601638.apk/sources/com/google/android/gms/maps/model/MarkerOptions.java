package com.google.android.gms.maps.model;

import android.os.IBinder;
import android.os.Parcel;
import com.google.android.gms.internal.ac;
import com.google.android.gms.internal.ae;
import com.google.android.gms.internal.s;
import com.google.android.gms.internal.w;

public final class MarkerOptions implements ae {
    public static final g a = new g();
    private final int b;
    private LatLng c;
    private String d;
    private String e;
    private a f;
    private float g;
    private float h;
    private boolean i;
    private boolean j;

    public MarkerOptions() {
        this.g = 0.5f;
        this.h = 1.0f;
        this.j = true;
        this.b = 1;
    }

    MarkerOptions(int i2, LatLng latLng, String str, String str2, IBinder iBinder, float f2, float f3, boolean z, boolean z2) {
        this.g = 0.5f;
        this.h = 1.0f;
        this.j = true;
        this.b = i2;
        this.c = latLng;
        this.d = str;
        this.e = str2;
        this.f = iBinder == null ? null : new a(s.a.a(iBinder));
        this.g = f2;
        this.h = f3;
        this.i = z;
        this.j = z2;
    }

    public int a() {
        return this.b;
    }

    public IBinder b() {
        if (this.f == null) {
            return null;
        }
        return this.f.a().asBinder();
    }

    public LatLng c() {
        return this.c;
    }

    public String d() {
        return this.d;
    }

    public int describeContents() {
        return 0;
    }

    public String e() {
        return this.e;
    }

    public float f() {
        return this.g;
    }

    public float g() {
        return this.h;
    }

    public boolean h() {
        return this.i;
    }

    public boolean i() {
        return this.j;
    }

    public void writeToParcel(Parcel parcel, int i2) {
        if (w.a()) {
            ac.a(this, parcel, i2);
        } else {
            g.a(this, parcel, i2);
        }
    }
}
