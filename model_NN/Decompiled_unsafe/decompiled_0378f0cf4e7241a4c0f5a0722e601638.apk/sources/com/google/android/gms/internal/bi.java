package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.a.e;
import com.google.android.gms.internal.b;
import com.google.android.gms.internal.eq;
import java.util.HashSet;
import java.util.Set;

public class bi implements Parcelable.Creator<eq.e> {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.c.a(android.os.Parcel, int, java.lang.String, boolean):void
     arg types: [android.os.Parcel, int, java.lang.String, int]
     candidates:
      com.google.android.gms.internal.c.a(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.internal.c.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.internal.c.a(android.os.Parcel, int, android.os.Parcel, boolean):void
      com.google.android.gms.internal.c.a(android.os.Parcel, int, java.util.List<java.lang.String>, boolean):void
      com.google.android.gms.internal.c.a(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.internal.c.a(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.internal.c.a(android.os.Parcel, int, java.lang.String, boolean):void */
    static void a(eq.e eVar, Parcel parcel, int i) {
        int a = c.a(parcel);
        Set<Integer> e = eVar.e();
        if (e.contains(1)) {
            c.a(parcel, 1, eVar.f());
        }
        if (e.contains(2)) {
            c.a(parcel, 2, eVar.g(), true);
        }
        if (e.contains(3)) {
            c.a(parcel, 3, eVar.h(), true);
        }
        if (e.contains(4)) {
            c.a(parcel, 4, eVar.i(), true);
        }
        if (e.contains(5)) {
            c.a(parcel, 5, eVar.j(), true);
        }
        if (e.contains(6)) {
            c.a(parcel, 6, eVar.k(), true);
        }
        if (e.contains(7)) {
            c.a(parcel, 7, eVar.l(), true);
        }
        c.a(parcel, a);
    }

    /* renamed from: a */
    public eq.e createFromParcel(Parcel parcel) {
        String str = null;
        int b = b.b(parcel);
        HashSet hashSet = new HashSet();
        int i = 0;
        String str2 = null;
        String str3 = null;
        String str4 = null;
        String str5 = null;
        String str6 = null;
        while (parcel.dataPosition() < b) {
            int a = b.a(parcel);
            switch (b.a(a)) {
                case 1:
                    i = b.f(parcel, a);
                    hashSet.add(1);
                    break;
                case 2:
                    str6 = b.l(parcel, a);
                    hashSet.add(2);
                    break;
                case 3:
                    str5 = b.l(parcel, a);
                    hashSet.add(3);
                    break;
                case e.g.com_facebook_picker_fragment_done_button_text:
                    str4 = b.l(parcel, a);
                    hashSet.add(4);
                    break;
                case e.g.com_facebook_picker_fragment_title_bar_background:
                    str3 = b.l(parcel, a);
                    hashSet.add(5);
                    break;
                case e.g.com_facebook_picker_fragment_done_button_background:
                    str2 = b.l(parcel, a);
                    hashSet.add(6);
                    break;
                case 7:
                    str = b.l(parcel, a);
                    hashSet.add(7);
                    break;
                default:
                    b.b(parcel, a);
                    break;
            }
        }
        if (parcel.dataPosition() == b) {
            return new eq.e(hashSet, i, str6, str5, str4, str3, str2, str);
        }
        throw new b.a("Overread allowed size end=" + b, parcel);
    }

    /* renamed from: a */
    public eq.e[] newArray(int i) {
        return new eq.e[i];
    }
}
