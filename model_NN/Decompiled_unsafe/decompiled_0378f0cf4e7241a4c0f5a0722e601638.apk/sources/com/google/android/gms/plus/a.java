package com.google.android.gms.plus;

import android.content.Context;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import com.google.android.gms.common.b;
import com.google.android.gms.internal.ap;
import com.google.android.gms.internal.av;
import java.util.ArrayList;

public class a implements com.google.android.gms.common.b {
    final av a;

    /* renamed from: com.google.android.gms.plus.a$a  reason: collision with other inner class name */
    public static class C0029a {
        private Context a;
        private String b;
        private b.a c;
        private b.C0020b d;
        private ArrayList<String> e = new ArrayList<>();
        private String[] f;
        private String[] g;
        private String h = this.a.getPackageName();
        private String i = this.a.getPackageName();

        public C0029a(Context context, b.a aVar, b.C0020b bVar) {
            this.a = context;
            this.c = aVar;
            this.d = bVar;
            this.e.add("https://www.googleapis.com/auth/plus.login");
        }

        public C0029a a() {
            this.e.clear();
            return this;
        }

        public a b() {
            if (this.b == null) {
                this.b = "<<default account>>";
            }
            return new a(this.a, this.i, this.h, this.b, this.c, this.d, this.f, this.g, (String[]) this.e.toArray(new String[this.e.size()]));
        }
    }

    public interface b {
        void a(com.google.android.gms.common.a aVar, ParcelFileDescriptor parcelFileDescriptor);
    }

    public interface c {
        void a(com.google.android.gms.common.a aVar, ap apVar);
    }

    a(Context context, String str, String str2, String str3, b.a aVar, b.C0020b bVar, String[] strArr, String[] strArr2, String[] strArr3) {
        this.a = new av(context, str, str2, str3, aVar, bVar, strArr, strArr2, strArr3);
    }

    public void a() {
        this.a.c();
    }

    public void a(b.a aVar) {
        this.a.a(aVar);
    }

    public void a(b.C0020b bVar) {
        this.a.a(bVar);
    }

    public void a(b bVar, Uri uri, int i) {
        this.a.a(bVar, uri, i);
    }

    public void a(c cVar, String str) {
        this.a.a(cVar, str);
    }

    public boolean b() {
        return this.a.d();
    }

    public boolean b(b.a aVar) {
        return this.a.b(aVar);
    }

    public boolean b(b.C0020b bVar) {
        return this.a.b(bVar);
    }

    public void c() {
        this.a.e();
    }

    public void c(b.a aVar) {
        this.a.c(aVar);
    }

    public void c(b.C0020b bVar) {
        this.a.c(bVar);
    }
}
