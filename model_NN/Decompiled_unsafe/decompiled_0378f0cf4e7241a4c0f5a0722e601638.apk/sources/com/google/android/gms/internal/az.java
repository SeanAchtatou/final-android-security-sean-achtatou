package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.a.e;
import com.google.android.gms.internal.b;
import java.util.HashSet;
import java.util.Set;

public class az implements Parcelable.Creator<ef> {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.c.a(android.os.Parcel, int, java.lang.String, boolean):void
     arg types: [android.os.Parcel, int, java.lang.String, int]
     candidates:
      com.google.android.gms.internal.c.a(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.internal.c.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.internal.c.a(android.os.Parcel, int, android.os.Parcel, boolean):void
      com.google.android.gms.internal.c.a(android.os.Parcel, int, java.util.List<java.lang.String>, boolean):void
      com.google.android.gms.internal.c.a(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.internal.c.a(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.internal.c.a(android.os.Parcel, int, java.lang.String, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.c.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.internal.ed, int, int]
     candidates:
      com.google.android.gms.internal.c.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.internal.c.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    static void a(ef efVar, Parcel parcel, int i) {
        int a = c.a(parcel);
        Set<Integer> e = efVar.e();
        if (e.contains(1)) {
            c.a(parcel, 1, efVar.f());
        }
        if (e.contains(2)) {
            c.a(parcel, 2, efVar.g(), true);
        }
        if (e.contains(4)) {
            c.a(parcel, 4, (Parcelable) efVar.h(), i, true);
        }
        if (e.contains(5)) {
            c.a(parcel, 5, efVar.i(), true);
        }
        if (e.contains(6)) {
            c.a(parcel, 6, (Parcelable) efVar.j(), i, true);
        }
        if (e.contains(7)) {
            c.a(parcel, 7, efVar.k(), true);
        }
        c.a(parcel, a);
    }

    /* renamed from: a */
    public ef createFromParcel(Parcel parcel) {
        String str = null;
        int b = b.b(parcel);
        HashSet hashSet = new HashSet();
        int i = 0;
        ed edVar = null;
        String str2 = null;
        ed edVar2 = null;
        String str3 = null;
        while (parcel.dataPosition() < b) {
            int a = b.a(parcel);
            switch (b.a(a)) {
                case 1:
                    i = b.f(parcel, a);
                    hashSet.add(1);
                    break;
                case 2:
                    str3 = b.l(parcel, a);
                    hashSet.add(2);
                    break;
                case 3:
                default:
                    b.b(parcel, a);
                    break;
                case e.g.com_facebook_picker_fragment_done_button_text:
                    hashSet.add(4);
                    edVar2 = (ed) b.a(parcel, a, ed.a);
                    break;
                case e.g.com_facebook_picker_fragment_title_bar_background:
                    str2 = b.l(parcel, a);
                    hashSet.add(5);
                    break;
                case e.g.com_facebook_picker_fragment_done_button_background:
                    hashSet.add(6);
                    edVar = (ed) b.a(parcel, a, ed.a);
                    break;
                case 7:
                    str = b.l(parcel, a);
                    hashSet.add(7);
                    break;
            }
        }
        if (parcel.dataPosition() == b) {
            return new ef(hashSet, i, str3, edVar2, str2, edVar, str);
        }
        throw new b.a("Overread allowed size end=" + b, parcel);
    }

    /* renamed from: a */
    public ef[] newArray(int i) {
        return new ef[i];
    }
}
