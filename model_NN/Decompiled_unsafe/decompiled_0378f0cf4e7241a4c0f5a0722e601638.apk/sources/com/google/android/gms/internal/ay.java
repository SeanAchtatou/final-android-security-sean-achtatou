package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.a.e;
import com.google.android.gms.internal.b;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class ay implements Parcelable.Creator<ed> {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.c.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.internal.ed, int, int]
     candidates:
      com.google.android.gms.internal.c.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.internal.c.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.c.a(android.os.Parcel, int, java.util.List<java.lang.String>, boolean):void
     arg types: [android.os.Parcel, int, java.util.List<java.lang.String>, int]
     candidates:
      com.google.android.gms.internal.c.a(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.internal.c.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.internal.c.a(android.os.Parcel, int, android.os.Parcel, boolean):void
      com.google.android.gms.internal.c.a(android.os.Parcel, int, java.lang.String, boolean):void
      com.google.android.gms.internal.c.a(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.internal.c.a(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.internal.c.a(android.os.Parcel, int, java.util.List<java.lang.String>, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.c.a(android.os.Parcel, int, java.lang.String, boolean):void
     arg types: [android.os.Parcel, int, java.lang.String, int]
     candidates:
      com.google.android.gms.internal.c.a(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.internal.c.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.internal.c.a(android.os.Parcel, int, android.os.Parcel, boolean):void
      com.google.android.gms.internal.c.a(android.os.Parcel, int, java.util.List<java.lang.String>, boolean):void
      com.google.android.gms.internal.c.a(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.internal.c.a(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.internal.c.a(android.os.Parcel, int, java.lang.String, boolean):void */
    static void a(ed edVar, Parcel parcel, int i) {
        int a = c.a(parcel);
        Set<Integer> e = edVar.e();
        if (e.contains(1)) {
            c.a(parcel, 1, edVar.f());
        }
        if (e.contains(2)) {
            c.a(parcel, 2, (Parcelable) edVar.g(), i, true);
        }
        if (e.contains(3)) {
            c.a(parcel, 3, edVar.h(), true);
        }
        if (e.contains(4)) {
            c.a(parcel, 4, (Parcelable) edVar.i(), i, true);
        }
        if (e.contains(5)) {
            c.a(parcel, 5, edVar.j(), true);
        }
        if (e.contains(6)) {
            c.a(parcel, 6, edVar.k(), true);
        }
        if (e.contains(7)) {
            c.a(parcel, 7, edVar.l(), true);
        }
        if (e.contains(8)) {
            c.b(parcel, 8, edVar.m(), true);
        }
        if (e.contains(9)) {
            c.a(parcel, 9, edVar.n());
        }
        if (e.contains(10)) {
            c.b(parcel, 10, edVar.o(), true);
        }
        if (e.contains(11)) {
            c.a(parcel, 11, (Parcelable) edVar.p(), i, true);
        }
        if (e.contains(12)) {
            c.b(parcel, 12, edVar.q(), true);
        }
        if (e.contains(13)) {
            c.a(parcel, 13, edVar.r(), true);
        }
        if (e.contains(14)) {
            c.a(parcel, 14, edVar.s(), true);
        }
        if (e.contains(15)) {
            c.a(parcel, 15, (Parcelable) edVar.t(), i, true);
        }
        if (e.contains(17)) {
            c.a(parcel, 17, edVar.v(), true);
        }
        if (e.contains(16)) {
            c.a(parcel, 16, edVar.u(), true);
        }
        if (e.contains(19)) {
            c.b(parcel, 19, edVar.x(), true);
        }
        if (e.contains(18)) {
            c.a(parcel, 18, edVar.w(), true);
        }
        if (e.contains(21)) {
            c.a(parcel, 21, edVar.z(), true);
        }
        if (e.contains(20)) {
            c.a(parcel, 20, edVar.y(), true);
        }
        if (e.contains(23)) {
            c.a(parcel, 23, edVar.B(), true);
        }
        if (e.contains(22)) {
            c.a(parcel, 22, edVar.A(), true);
        }
        if (e.contains(25)) {
            c.a(parcel, 25, edVar.D(), true);
        }
        if (e.contains(24)) {
            c.a(parcel, 24, edVar.C(), true);
        }
        if (e.contains(27)) {
            c.a(parcel, 27, edVar.F(), true);
        }
        if (e.contains(26)) {
            c.a(parcel, 26, edVar.E(), true);
        }
        if (e.contains(29)) {
            c.a(parcel, 29, (Parcelable) edVar.H(), i, true);
        }
        if (e.contains(28)) {
            c.a(parcel, 28, edVar.G(), true);
        }
        if (e.contains(31)) {
            c.a(parcel, 31, edVar.J(), true);
        }
        if (e.contains(30)) {
            c.a(parcel, 30, edVar.I(), true);
        }
        if (e.contains(34)) {
            c.a(parcel, 34, (Parcelable) edVar.M(), i, true);
        }
        if (e.contains(32)) {
            c.a(parcel, 32, edVar.K(), true);
        }
        if (e.contains(33)) {
            c.a(parcel, 33, edVar.L(), true);
        }
        if (e.contains(38)) {
            c.a(parcel, 38, edVar.P());
        }
        if (e.contains(39)) {
            c.a(parcel, 39, edVar.Q(), true);
        }
        if (e.contains(36)) {
            c.a(parcel, 36, edVar.N());
        }
        if (e.contains(37)) {
            c.a(parcel, 37, (Parcelable) edVar.O(), i, true);
        }
        if (e.contains(42)) {
            c.a(parcel, 42, edVar.T(), true);
        }
        if (e.contains(43)) {
            c.a(parcel, 43, edVar.U(), true);
        }
        if (e.contains(40)) {
            c.a(parcel, 40, (Parcelable) edVar.R(), i, true);
        }
        if (e.contains(41)) {
            c.b(parcel, 41, edVar.S(), true);
        }
        if (e.contains(46)) {
            c.a(parcel, 46, (Parcelable) edVar.X(), i, true);
        }
        if (e.contains(47)) {
            c.a(parcel, 47, edVar.Y(), true);
        }
        if (e.contains(44)) {
            c.a(parcel, 44, edVar.V(), true);
        }
        if (e.contains(45)) {
            c.a(parcel, 45, edVar.W(), true);
        }
        if (e.contains(51)) {
            c.a(parcel, 51, edVar.ac(), true);
        }
        if (e.contains(50)) {
            c.a(parcel, 50, (Parcelable) edVar.ab(), i, true);
        }
        if (e.contains(49)) {
            c.a(parcel, 49, edVar.aa(), true);
        }
        if (e.contains(48)) {
            c.a(parcel, 48, edVar.Z(), true);
        }
        if (e.contains(55)) {
            c.a(parcel, 55, edVar.ag(), true);
        }
        if (e.contains(54)) {
            c.a(parcel, 54, edVar.af(), true);
        }
        if (e.contains(53)) {
            c.a(parcel, 53, edVar.ae(), true);
        }
        if (e.contains(52)) {
            c.a(parcel, 52, edVar.ad(), true);
        }
        if (e.contains(56)) {
            c.a(parcel, 56, edVar.ah(), true);
        }
        c.a(parcel, a);
    }

    /* renamed from: a */
    public ed createFromParcel(Parcel parcel) {
        int b = b.b(parcel);
        HashSet hashSet = new HashSet();
        int i = 0;
        ed edVar = null;
        ArrayList<String> arrayList = null;
        ed edVar2 = null;
        String str = null;
        String str2 = null;
        String str3 = null;
        ArrayList arrayList2 = null;
        int i2 = 0;
        ArrayList arrayList3 = null;
        ed edVar3 = null;
        ArrayList arrayList4 = null;
        String str4 = null;
        String str5 = null;
        ed edVar4 = null;
        String str6 = null;
        String str7 = null;
        String str8 = null;
        ArrayList arrayList5 = null;
        String str9 = null;
        String str10 = null;
        String str11 = null;
        String str12 = null;
        String str13 = null;
        String str14 = null;
        String str15 = null;
        String str16 = null;
        String str17 = null;
        ed edVar5 = null;
        String str18 = null;
        String str19 = null;
        String str20 = null;
        String str21 = null;
        ed edVar6 = null;
        double d = 0.0d;
        ed edVar7 = null;
        double d2 = 0.0d;
        String str22 = null;
        ed edVar8 = null;
        ArrayList arrayList6 = null;
        String str23 = null;
        String str24 = null;
        String str25 = null;
        String str26 = null;
        ed edVar9 = null;
        String str27 = null;
        String str28 = null;
        String str29 = null;
        ed edVar10 = null;
        String str30 = null;
        String str31 = null;
        String str32 = null;
        String str33 = null;
        String str34 = null;
        String str35 = null;
        while (parcel.dataPosition() < b) {
            int a = b.a(parcel);
            switch (b.a(a)) {
                case 1:
                    i = b.f(parcel, a);
                    hashSet.add(1);
                    break;
                case 2:
                    hashSet.add(2);
                    edVar = (ed) b.a(parcel, a, ed.a);
                    break;
                case 3:
                    arrayList = b.x(parcel, a);
                    hashSet.add(3);
                    break;
                case e.g.com_facebook_picker_fragment_done_button_text:
                    hashSet.add(4);
                    edVar2 = (ed) b.a(parcel, a, ed.a);
                    break;
                case e.g.com_facebook_picker_fragment_title_bar_background:
                    str = b.l(parcel, a);
                    hashSet.add(5);
                    break;
                case e.g.com_facebook_picker_fragment_done_button_background:
                    str2 = b.l(parcel, a);
                    hashSet.add(6);
                    break;
                case 7:
                    str3 = b.l(parcel, a);
                    hashSet.add(7);
                    break;
                case 8:
                    arrayList2 = b.c(parcel, a, ed.a);
                    hashSet.add(8);
                    break;
                case 9:
                    i2 = b.f(parcel, a);
                    hashSet.add(9);
                    break;
                case 10:
                    arrayList3 = b.c(parcel, a, ed.a);
                    hashSet.add(10);
                    break;
                case 11:
                    hashSet.add(11);
                    edVar3 = (ed) b.a(parcel, a, ed.a);
                    break;
                case 12:
                    arrayList4 = b.c(parcel, a, ed.a);
                    hashSet.add(12);
                    break;
                case 13:
                    str4 = b.l(parcel, a);
                    hashSet.add(13);
                    break;
                case 14:
                    str5 = b.l(parcel, a);
                    hashSet.add(14);
                    break;
                case 15:
                    hashSet.add(15);
                    edVar4 = (ed) b.a(parcel, a, ed.a);
                    break;
                case 16:
                    str6 = b.l(parcel, a);
                    hashSet.add(16);
                    break;
                case 17:
                    str7 = b.l(parcel, a);
                    hashSet.add(17);
                    break;
                case 18:
                    str8 = b.l(parcel, a);
                    hashSet.add(18);
                    break;
                case 19:
                    arrayList5 = b.c(parcel, a, ed.a);
                    hashSet.add(19);
                    break;
                case 20:
                    str9 = b.l(parcel, a);
                    hashSet.add(20);
                    break;
                case 21:
                    str10 = b.l(parcel, a);
                    hashSet.add(21);
                    break;
                case 22:
                    str11 = b.l(parcel, a);
                    hashSet.add(22);
                    break;
                case 23:
                    str12 = b.l(parcel, a);
                    hashSet.add(23);
                    break;
                case 24:
                    str13 = b.l(parcel, a);
                    hashSet.add(24);
                    break;
                case 25:
                    str14 = b.l(parcel, a);
                    hashSet.add(25);
                    break;
                case 26:
                    str15 = b.l(parcel, a);
                    hashSet.add(26);
                    break;
                case 27:
                    str16 = b.l(parcel, a);
                    hashSet.add(27);
                    break;
                case 28:
                    str17 = b.l(parcel, a);
                    hashSet.add(28);
                    break;
                case 29:
                    hashSet.add(29);
                    edVar5 = (ed) b.a(parcel, a, ed.a);
                    break;
                case 30:
                    str18 = b.l(parcel, a);
                    hashSet.add(30);
                    break;
                case 31:
                    str19 = b.l(parcel, a);
                    hashSet.add(31);
                    break;
                case 32:
                    str20 = b.l(parcel, a);
                    hashSet.add(32);
                    break;
                case 33:
                    str21 = b.l(parcel, a);
                    hashSet.add(33);
                    break;
                case 34:
                    hashSet.add(34);
                    edVar6 = (ed) b.a(parcel, a, ed.a);
                    break;
                case 35:
                default:
                    b.b(parcel, a);
                    break;
                case 36:
                    d = b.j(parcel, a);
                    hashSet.add(36);
                    break;
                case 37:
                    hashSet.add(37);
                    edVar7 = (ed) b.a(parcel, a, ed.a);
                    break;
                case 38:
                    d2 = b.j(parcel, a);
                    hashSet.add(38);
                    break;
                case 39:
                    str22 = b.l(parcel, a);
                    hashSet.add(39);
                    break;
                case 40:
                    hashSet.add(40);
                    edVar8 = (ed) b.a(parcel, a, ed.a);
                    break;
                case 41:
                    arrayList6 = b.c(parcel, a, ed.a);
                    hashSet.add(41);
                    break;
                case 42:
                    str23 = b.l(parcel, a);
                    hashSet.add(42);
                    break;
                case 43:
                    str24 = b.l(parcel, a);
                    hashSet.add(43);
                    break;
                case 44:
                    str25 = b.l(parcel, a);
                    hashSet.add(44);
                    break;
                case 45:
                    str26 = b.l(parcel, a);
                    hashSet.add(45);
                    break;
                case 46:
                    hashSet.add(46);
                    edVar9 = (ed) b.a(parcel, a, ed.a);
                    break;
                case 47:
                    str27 = b.l(parcel, a);
                    hashSet.add(47);
                    break;
                case 48:
                    str28 = b.l(parcel, a);
                    hashSet.add(48);
                    break;
                case 49:
                    str29 = b.l(parcel, a);
                    hashSet.add(49);
                    break;
                case 50:
                    hashSet.add(50);
                    edVar10 = (ed) b.a(parcel, a, ed.a);
                    break;
                case 51:
                    str30 = b.l(parcel, a);
                    hashSet.add(51);
                    break;
                case 52:
                    str31 = b.l(parcel, a);
                    hashSet.add(52);
                    break;
                case 53:
                    str32 = b.l(parcel, a);
                    hashSet.add(53);
                    break;
                case 54:
                    str33 = b.l(parcel, a);
                    hashSet.add(54);
                    break;
                case 55:
                    str34 = b.l(parcel, a);
                    hashSet.add(55);
                    break;
                case 56:
                    str35 = b.l(parcel, a);
                    hashSet.add(56);
                    break;
            }
        }
        if (parcel.dataPosition() == b) {
            return new ed(hashSet, i, edVar, arrayList, edVar2, str, str2, str3, arrayList2, i2, arrayList3, edVar3, arrayList4, str4, str5, edVar4, str6, str7, str8, arrayList5, str9, str10, str11, str12, str13, str14, str15, str16, str17, edVar5, str18, str19, str20, str21, edVar6, d, edVar7, d2, str22, edVar8, arrayList6, str23, str24, str25, str26, edVar9, str27, str28, str29, edVar10, str30, str31, str32, str33, str34, str35);
        }
        throw new b.a("Overread allowed size end=" + b, parcel);
    }

    /* renamed from: a */
    public ed[] newArray(int i) {
        return new ed[i];
    }
}
