package com.google.android.gms.maps.model;

import android.os.Parcel;
import com.google.android.gms.internal.ae;
import com.google.android.gms.internal.bu;
import com.google.android.gms.internal.bv;
import com.google.android.gms.internal.w;
import com.google.android.gms.internal.x;

public final class CameraPosition implements ae {
    public static final b a = new b();
    public final LatLng b;
    public final float c;
    public final float d;
    public final float e;
    private final int f;

    CameraPosition(int i, LatLng latLng, float f2, float f3, float f4) {
        bv.a(latLng, "null camera target");
        bv.b(0.0f <= f3 && f3 <= 90.0f, "Tilt needs to be between 0 and 90 inclusive");
        this.f = i;
        this.b = latLng;
        this.c = f2;
        this.d = f3 + 0.0f;
        this.e = (((double) f4) <= 0.0d ? (f4 % 360.0f) + 360.0f : f4) % 360.0f;
    }

    public int a() {
        return this.f;
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof CameraPosition)) {
            return false;
        }
        CameraPosition cameraPosition = (CameraPosition) obj;
        return this.b.equals(cameraPosition.b) && Float.floatToIntBits(this.c) == Float.floatToIntBits(cameraPosition.c) && Float.floatToIntBits(this.d) == Float.floatToIntBits(cameraPosition.d) && Float.floatToIntBits(this.e) == Float.floatToIntBits(cameraPosition.e);
    }

    public int hashCode() {
        return bu.a(this.b, Float.valueOf(this.c), Float.valueOf(this.d), Float.valueOf(this.e));
    }

    public String toString() {
        return bu.a(this).a("target", this.b).a("zoom", Float.valueOf(this.c)).a("tilt", Float.valueOf(this.d)).a("bearing", Float.valueOf(this.e)).toString();
    }

    public void writeToParcel(Parcel parcel, int i) {
        if (w.a()) {
            x.a(this, parcel, i);
        } else {
            b.a(this, parcel, i);
        }
    }
}
