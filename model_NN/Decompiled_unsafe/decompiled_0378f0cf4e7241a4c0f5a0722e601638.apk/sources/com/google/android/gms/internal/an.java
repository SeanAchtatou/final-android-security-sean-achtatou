package com.google.android.gms.internal;

import android.os.Parcel;
import java.util.ArrayList;
import java.util.HashMap;

public abstract class an {

    public static class a<I, O> implements ae {
        public static final i i = new i();
        protected final int a;
        protected final boolean b;
        protected final int c;
        protected final boolean d;
        protected final String e;
        protected final int f;
        protected final Class<? extends an> g;
        protected final String h;
        private final int j;
        private aq k;
        /* access modifiers changed from: private */
        public b<I, O> l;

        a(int i2, int i3, boolean z, int i4, boolean z2, String str, int i5, String str2, ai aiVar) {
            this.j = i2;
            this.a = i3;
            this.b = z;
            this.c = i4;
            this.d = z2;
            this.e = str;
            this.f = i5;
            if (str2 == null) {
                this.g = null;
                this.h = null;
            } else {
                this.g = at.class;
                this.h = str2;
            }
            if (aiVar == null) {
                this.l = null;
            } else {
                this.l = aiVar.c();
            }
        }

        protected a(int i2, boolean z, int i3, boolean z2, String str, int i4, Class<? extends an> cls, b<I, O> bVar) {
            this.j = 1;
            this.a = i2;
            this.b = z;
            this.c = i3;
            this.d = z2;
            this.e = str;
            this.f = i4;
            this.g = cls;
            if (cls == null) {
                this.h = null;
            } else {
                this.h = cls.getCanonicalName();
            }
            this.l = bVar;
        }

        public static a<Integer, Integer> a(String str, int i2) {
            return new a<>(0, false, 0, false, str, i2, null, null);
        }

        public static a a(String str, int i2, b<?, ?> bVar, boolean z) {
            return new a(bVar.c(), z, bVar.d(), false, str, i2, null, bVar);
        }

        public static <T extends an> a<T, T> a(String str, int i2, Class<T> cls) {
            return new a<>(11, false, 11, false, str, i2, cls, null);
        }

        public static a<Double, Double> b(String str, int i2) {
            return new a<>(4, false, 4, false, str, i2, null, null);
        }

        public static <T extends an> a<ArrayList<T>, ArrayList<T>> b(String str, int i2, Class<T> cls) {
            return new a<>(11, true, 11, true, str, i2, cls, null);
        }

        public static a<Boolean, Boolean> c(String str, int i2) {
            return new a<>(6, false, 6, false, str, i2, null, null);
        }

        public static a<String, String> d(String str, int i2) {
            return new a<>(7, false, 7, false, str, i2, null, null);
        }

        public static a<ArrayList<String>, ArrayList<String>> e(String str, int i2) {
            return new a<>(7, true, 7, true, str, i2, null, null);
        }

        public int a() {
            return this.j;
        }

        public I a(O o) {
            return this.l.a(o);
        }

        public void a(aq aqVar) {
            this.k = aqVar;
        }

        public int b() {
            return this.a;
        }

        public boolean c() {
            return this.b;
        }

        public int d() {
            return this.c;
        }

        public int describeContents() {
            i iVar = i;
            return 0;
        }

        public boolean e() {
            return this.d;
        }

        public String f() {
            return this.e;
        }

        public int g() {
            return this.f;
        }

        public Class<? extends an> h() {
            return this.g;
        }

        /* access modifiers changed from: package-private */
        public String i() {
            if (this.h == null) {
                return null;
            }
            return this.h;
        }

        public boolean j() {
            return this.l != null;
        }

        /* access modifiers changed from: package-private */
        public ai k() {
            if (this.l == null) {
                return null;
            }
            return ai.a(this.l);
        }

        public HashMap<String, a<?, ?>> l() {
            bv.a(this.h);
            bv.a(this.k);
            return this.k.a(this.h);
        }

        public String toString() {
            StringBuilder sb = new StringBuilder();
            sb.append("Field\n");
            sb.append("            versionCode=").append(this.j).append(10);
            sb.append("                 typeIn=").append(this.a).append(10);
            sb.append("            typeInArray=").append(this.b).append(10);
            sb.append("                typeOut=").append(this.c).append(10);
            sb.append("           typeOutArray=").append(this.d).append(10);
            sb.append("        outputFieldName=").append(this.e).append(10);
            sb.append("      safeParcelFieldId=").append(this.f).append(10);
            sb.append("       concreteTypeName=").append(i()).append(10);
            if (h() != null) {
                sb.append("     concreteType.class=").append(h().getCanonicalName()).append(10);
            }
            sb.append("          converterName=").append(this.l == null ? "null" : this.l.getClass().getCanonicalName()).append(10);
            return sb.toString();
        }

        public void writeToParcel(Parcel parcel, int i2) {
            i iVar = i;
            i.a(this, parcel, i2);
        }
    }

    public interface b<I, O> {
        I a(Object obj);

        int c();

        int d();
    }

    private void a(StringBuilder sb, a aVar, Object obj) {
        if (aVar.b() == 11) {
            sb.append(((an) aVar.h().cast(obj)).toString());
        } else if (aVar.b() == 7) {
            sb.append("\"");
            sb.append(q.a((String) obj));
            sb.append("\"");
        } else {
            sb.append(obj);
        }
    }

    private void a(StringBuilder sb, a aVar, ArrayList<Object> arrayList) {
        sb.append("[");
        int size = arrayList.size();
        for (int i = 0; i < size; i++) {
            if (i > 0) {
                sb.append(",");
            }
            Object obj = arrayList.get(i);
            if (obj != null) {
                a(sb, aVar, obj);
            }
        }
        sb.append("]");
    }

    /* access modifiers changed from: protected */
    public <O, I> I a(a<I, O> aVar, Object obj) {
        return aVar.l != null ? aVar.a(obj) : obj;
    }

    /* access modifiers changed from: protected */
    public abstract Object a(String str);

    /* access modifiers changed from: protected */
    public boolean a(a aVar) {
        return aVar.d() == 11 ? aVar.e() ? d(aVar.f()) : c(aVar.f()) : b(aVar.f());
    }

    /* access modifiers changed from: protected */
    public Object b(a aVar) {
        boolean z = true;
        String f = aVar.f();
        if (aVar.h() == null) {
            return a(aVar.f());
        }
        if (a(aVar.f()) != null) {
            z = false;
        }
        bv.a(z, "Concrete field shouldn't be value object: " + aVar.f());
        HashMap<String, Object> d = aVar.e() ? d() : c();
        if (d != null) {
            return d.get(f);
        }
        try {
            return getClass().getMethod("get" + Character.toUpperCase(f.charAt(0)) + f.substring(1), new Class[0]).invoke(this, new Object[0]);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public abstract HashMap<String, a<?, ?>> b();

    /* access modifiers changed from: protected */
    public abstract boolean b(String str);

    public HashMap<String, Object> c() {
        return null;
    }

    /* access modifiers changed from: protected */
    public boolean c(String str) {
        throw new UnsupportedOperationException("Concrete types not supported");
    }

    public HashMap<String, Object> d() {
        return null;
    }

    /* access modifiers changed from: protected */
    public boolean d(String str) {
        throw new UnsupportedOperationException("Concrete type arrays not supported");
    }

    public String toString() {
        HashMap<String, a<?, ?>> b2 = b();
        StringBuilder sb = new StringBuilder(100);
        for (String next : b2.keySet()) {
            a aVar = b2.get(next);
            if (a(aVar)) {
                Object a2 = a(aVar, b(aVar));
                if (sb.length() == 0) {
                    sb.append("{");
                } else {
                    sb.append(",");
                }
                sb.append("\"").append(next).append("\":");
                if (a2 != null) {
                    switch (aVar.d()) {
                        case 8:
                            sb.append("\"").append(p.a((byte[]) a2)).append("\"");
                            continue;
                        case 9:
                            sb.append("\"").append(p.b((byte[]) a2)).append("\"");
                            continue;
                        case 10:
                            r.a(sb, (HashMap) a2);
                            continue;
                        default:
                            if (!aVar.c()) {
                                a(sb, aVar, a2);
                                break;
                            } else {
                                a(sb, aVar, (ArrayList<Object>) ((ArrayList) a2));
                                continue;
                            }
                    }
                } else {
                    sb.append("null");
                }
            }
        }
        if (sb.length() > 0) {
            sb.append("}");
        } else {
            sb.append("{}");
        }
        return sb.toString();
    }
}
