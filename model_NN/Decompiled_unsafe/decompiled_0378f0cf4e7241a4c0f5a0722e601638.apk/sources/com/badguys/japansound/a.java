package com.badguys.japansound;

import android.content.Context;
import java.util.ArrayList;

/* compiled from: Sound */
public class a {
    public static Context a;
    private static ArrayList<a> d;
    public String b;
    public String c;

    public a(String str) {
        this.b = str;
        String[] split = str.split("_");
        if (split.length > 1) {
            this.c = String.valueOf(split[0].substring(0, 1).toUpperCase()) + split[0].substring(1) + " " + split[1].substring(0, 1).toUpperCase() + split[1].substring(1);
        } else {
            this.c = String.valueOf(str.substring(0, 1).toUpperCase()) + str.substring(1);
        }
    }

    public static a a(int i) {
        b();
        return d.get(i);
    }

    public static ArrayList<a> a() {
        b();
        return d;
    }

    private static void b() {
        if (d == null) {
            d = new ArrayList<>();
            d.add(new a("akari_hosoda"));
            d.add(new a("anri_sugihara"));
            d.add(new a("fukasawa"));
            d.add(new a("kana_tsugihara"));
            d.add(new a("may_iikubo"));
            d.add(new a("morishita"));
            d.add(new a("nozomi_kawasaki"));
            d.add(new a("rina_nagasaki"));
            d.add(new a("saori_yamamoto"));
            d.add(new a("saya_hikita"));
            d.add(new a("sayaka_numajiri"));
            d.add(new a("sayaka_uchida"));
            d.add(new a("shizuka_miyazawa"));
            d.add(new a("takahashi"));
            d.add(new a("takaou_ayatsuki"));
            d.add(new a("takayo_oyama"));
            d.add(new a("tamiko_hasunuma"));
            d.add(new a("toyomi_suzuki"));
            d.add(new a("yoko_matsugane"));
            d.add(new a("yoshie_fujihara"));
            d.add(new a("yu_misaki"));
            d.add(new a("yui_minami"));
            d.add(new a("yuika_hotta"));
            d.add(new a("yuka_hirose"));
            d.add(new a("yuka_kawamoto"));
            d.add(new a("yuka_mizusawa"));
            d.add(new a("yukiko_nanase"));
            d.add(new a("yuko_nakazawa"));
            d.add(new a("yuko_ogura"));
            d.add(new a("yumi_ishikawa"));
            d.add(new a("yuri_himegami"));
            d.add(new a("yuri_kimura"));
            d.add(new a("yurina_inoue"));
            d.add(new a("yuu_tejima"));
            d.add(new a("yuuna_kawai"));
            d.add(new a("yuuri_morishita"));
        }
    }
}
