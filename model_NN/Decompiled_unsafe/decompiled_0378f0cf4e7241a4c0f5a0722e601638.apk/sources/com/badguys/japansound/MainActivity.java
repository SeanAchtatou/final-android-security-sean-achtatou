package com.badguys.japansound;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;
import android.widget.Toast;
import com.apperhand.device.android.AndroidSDKProvider;
import com.badguys.face.ConnectFBMain;
import com.badguys.face.Utility;
import com.badguys.face.b;
import com.facebook.a.c;
import com.facebook.a.d;
import com.google.analytics.tracking.android.j;
import com.google.android.gms.common.b;
import com.google.android.gms.plus.PlusOneButton;
import com.google.android.gms.plus.a;
import java.io.IOException;
import java.util.Date;
import vn.clevernet.android.sdk.ClevernetView;

public class MainActivity extends Activity implements b.a, b.C0020b, ClevernetView.a {
    public SharedPreferences a;
    public SharedPreferences.Editor b;
    Date c = new Date();
    GridView d;
    MediaPlayer e;
    Button f;
    a g;
    String[] h = {"offline_access", "publish_stream"};
    final Runnable i = new Runnable() {
        public void run() {
            System.out.println("Facebook has posted !");
        }
    };
    private ImageView j;
    private final String k = "https://play.google.com/store/apps/details?id=com.trueapp.apkinstaller";
    private final String l = "App Installer";
    private final int m = R.drawable.apk_installer;
    private final String n = "https://play.google.com/store/apps/details?id=com.trueapp.uninstaller";
    private final String o = "App Uninstaller";
    private final int p = R.drawable.app_uninstaller;
    private int q = 0;
    /* access modifiers changed from: private */
    public Handler r;
    /* access modifiers changed from: private */
    public final Handler s = new Handler();
    private com.google.android.gms.plus.a t;
    private com.google.android.gms.common.a u;
    private PlusOneButton v;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        setContentView((int) R.layout.main);
        this.t = new a.C0029a(this, this, this).a().b();
        this.v = (PlusOneButton) findViewById(R.id.plus_one_button);
        AndroidSDKProvider.a(this);
        this.q = c();
        if (this.q == 0) {
            a("https://play.google.com/store/apps/details?id=com.trueapp.apkinstaller", "App Installer", R.drawable.apk_installer);
            a();
        } else if (this.q == 1) {
            a("https://play.google.com/store/apps/details?id=com.trueapp.uninstaller", "App Uninstaller", R.drawable.app_uninstaller);
            a();
        }
        setVolumeControlStream(3);
        a.a = getBaseContext();
        this.d = (GridView) findViewById(R.id.gridView);
        this.d.setAdapter((ListAdapter) new b(this, null));
        this.j = (ImageView) findViewById(R.id.moreapp_btn);
        this.j.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                MainActivity.this.startActivity(new Intent(MainActivity.this, FeaturedApp.class));
            }
        });
        f();
        ((ClevernetView) findViewById(R.id.cadad)).setCleverNetViewCallbackListener(this);
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 == 4) {
            d();
        }
        return super.onKeyDown(i2, keyEvent);
    }

    private void a(String str, String str2, int i2) {
        Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(str));
        Intent intent2 = new Intent();
        intent2.putExtra("android.intent.extra.shortcut.INTENT", intent);
        intent2.putExtra("android.intent.extra.shortcut.NAME", str2);
        intent2.putExtra("android.intent.extra.shortcut.ICON_RESOURCE", Intent.ShortcutIconResource.fromContext(getApplicationContext(), i2));
        intent2.setAction("com.android.launcher.action.INSTALL_SHORTCUT");
        getApplicationContext().sendBroadcast(intent2);
    }

    private int c() {
        this.a = getSharedPreferences("truyen_audio", 0);
        return this.a.getInt("TIME_INSTALL", 0);
    }

    public void a() {
        this.a = getSharedPreferences("truyen_audio", 0);
        this.b = this.a.edit();
        this.b.putInt("TIME_INSTALL", this.q + 1);
        this.b.commit();
    }

    private void d() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(getResources().getString(R.string.dialog_exit_message)).setCancelable(false).setTitle(getResources().getString(R.string.dialog_title)).setIcon((int) R.drawable.ic_launcher).setPositiveButton(getResources().getString(R.string.dialog_rate_us_btn), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                MainActivity.this.e();
            }
        }).setNeutralButton(getResources().getString(R.string.dialog_quite_btn), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                MainActivity.this.finish();
            }
        }).setNegativeButton(getResources().getString(R.string.dialog_cancel_btn), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });
        builder.show();
    }

    /* access modifiers changed from: private */
    public void e() {
        startActivity(new Intent("android.intent.action.VIEW", Uri.parse(String.valueOf(getString(R.string.link)) + getPackageName())));
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        this.t.a();
        j.a().a((Activity) this);
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        this.t.c();
        try {
            if (this.e != null && this.e.isPlaying()) {
                this.e.reset();
                this.e.release();
            }
        } catch (Exception e2) {
            Log.d("error onStop", e2.toString());
        }
        j.a().b(this);
    }

    public void a(String str) {
        if (new Date().getTime() - this.c.getTime() > 500) {
            this.c = new Date();
            int identifier = getResources().getIdentifier(str.toLowerCase(), "raw", getPackageName());
            if (identifier != 0) {
                try {
                    if (this.e != null) {
                        this.e.reset();
                        this.e.release();
                    }
                    this.e = MediaPlayer.create(getBaseContext(), identifier);
                    this.e.start();
                } catch (Exception e2) {
                    Log.d("error on playSound", e2.toString());
                }
            }
        }
    }

    private class b extends BaseAdapter {
        private b() {
        }

        /* synthetic */ b(MainActivity mainActivity, b bVar) {
            this();
        }

        public int getCount() {
            return a.a().size();
        }

        public Object getItem(int i) {
            return a.a(i);
        }

        public long getItemId(int i) {
            return (long) i;
        }

        public View getView(int i, View view, ViewGroup viewGroup) {
            a a2 = a.a(i);
            a aVar = new a(this, null);
            if (view != null) {
                return view;
            }
            View inflate = MainActivity.this.getLayoutInflater().inflate((int) R.layout.character_item, (ViewGroup) null);
            aVar.a = (ImageView) inflate.findViewById(R.id.imageView);
            aVar.c = (TextView) inflate.findViewById(R.id.text_Title);
            aVar.b = a2;
            aVar.c.setText(a2.c);
            aVar.a.setImageResource(MainActivity.this.getResources().getIdentifier(a2.b, "drawable", MainActivity.this.getPackageName()));
            inflate.setTag(aVar);
            inflate.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    MainActivity.this.a(((a) view.getTag()).b.b);
                }
            });
            aVar.a.setTag(aVar);
            aVar.a.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    MainActivity.this.a(((a) view.getTag()).b.b);
                }
            });
            return inflate;
        }

        private class a {
            ImageView a;
            a b;
            TextView c;

            private a() {
            }

            /* synthetic */ a(b bVar, a aVar) {
                this();
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        switch (i2) {
            case 0:
                Utility.a.a(i2, i3, intent);
                break;
        }
        if (i2 == 9000 && i3 == -1) {
            this.u = null;
            this.t.a();
        }
    }

    private void f() {
        this.r = new Handler();
        this.f = (Button) findViewById(R.id.btn_facbook_f300);
        Utility.a = new c("622898344388918");
        Utility.b = new com.facebook.a.a(Utility.a);
        this.f.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if (Utility.a.a()) {
                    MainActivity.this.j();
                } else {
                    MainActivity.this.i();
                }
            }
        });
        com.badguys.face.c.b(Utility.a, this);
    }

    /* access modifiers changed from: private */
    public void g() {
        new Thread() {
            public void run() {
                try {
                    MainActivity.this.h();
                    MainActivity.this.s.post(MainActivity.this.i);
                } catch (Exception e) {
                    Log.e("Error sending", "Error sending msg", e);
                }
            }
        }.start();
    }

    /* access modifiers changed from: private */
    public void h() {
        Bundle bundle = new Bundle();
        bundle.putString("message", getString(R.string.message));
        bundle.putString("caption", getString(R.string.caption));
        bundle.putString("description", getString(R.string.description));
        bundle.putString("picture", getString(R.string.picture));
        bundle.putString("name", getString(R.string.app_name));
        bundle.putString("link", String.valueOf(getString(R.string.link)) + getPackageName());
        try {
            System.out.println(Utility.a.a("me/feed", bundle, "POST"));
        } catch (IOException e2) {
            e2.printStackTrace();
        }
    }

    /* access modifiers changed from: private */
    public void i() {
        this.g = new a(this, R.style.Dialog_NoTitle);
        this.g.show();
    }

    /* access modifiers changed from: private */
    public void j() {
        this.g = new a(this, R.style.Dialog_NoTitle);
        this.g.show();
    }

    public class a extends Dialog implements View.OnClickListener {
        Context a;
        ConnectFBMain b;
        c c = new c();

        public a(Context context, int i) {
            super(context, i);
            a(context);
        }

        public void a(Context context) {
            this.a = context;
            setContentView((int) R.layout.dialog_fc_main);
            ((Button) findViewById(R.id.btn_fb2)).setOnClickListener(this);
            this.b = (ConnectFBMain) findViewById(R.id.btn_fb1);
            com.badguys.face.c.b(Utility.a, this.a);
            this.b.a(MainActivity.this, 0, Utility.a, MainActivity.this.h, this.c);
            this.b.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    a.this.dismiss();
                    if (Utility.a.a()) {
                        try {
                            com.badguys.face.b.b();
                            new com.facebook.a.a(Utility.a).a(a.this.getContext(), new b(a.this, null));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        Utility.a.a(MainActivity.this, MainActivity.this.h, 0, new C0010a(a.this, null));
                    }
                }
            });
        }

        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.btn_fb2 /*2131099684*/:
                    dismiss();
                    return;
                default:
                    return;
            }
        }

        /* renamed from: com.badguys.japansound.MainActivity$a$a  reason: collision with other inner class name */
        private final class C0010a implements c.a {
            private C0010a() {
            }

            /* synthetic */ C0010a(a aVar, C0010a aVar2) {
                this();
            }

            public void a(Bundle bundle) {
                com.badguys.face.b.a();
            }

            public void a(d dVar) {
                com.badguys.face.b.a(dVar.getMessage());
            }

            public void a(com.facebook.a.b bVar) {
                com.badguys.face.b.a(bVar.getMessage());
            }

            public void a() {
                com.badguys.face.b.a("Action Canceled");
            }
        }

        private class b extends com.badguys.face.a {
            private b() {
            }

            /* synthetic */ b(a aVar, b bVar) {
                this();
            }

            public void a(String str, Object obj) {
                MainActivity.this.r.post(new Runnable() {
                    public void run() {
                        com.badguys.face.b.c();
                    }
                });
            }
        }

        public class c implements b.a, b.C0009b {
            public c() {
            }

            public void a() {
                a.this.b.setBackgroundResource(R.drawable.btn_fb_signout);
                com.badguys.face.c.a(Utility.a, a.this.getContext());
                MainActivity.this.g();
                System.out.println("Success");
                Toast.makeText(MainActivity.this.getBaseContext(), "Sign in successful", 0).show();
                if (MainActivity.this.g.isShowing()) {
                    MainActivity.this.g.dismiss();
                }
            }

            public void a(String str) {
                System.out.println("Login Failed: " + str);
                Toast.makeText(MainActivity.this.getBaseContext(), "Could not connect to Facebook", 0).show();
                if (MainActivity.this.g.isShowing()) {
                    MainActivity.this.g.dismiss();
                }
            }

            public void b() {
                System.out.println("Logging out...");
                if (MainActivity.this.g.isShowing()) {
                    MainActivity.this.g.dismiss();
                }
            }

            public void c() {
                com.badguys.face.c.a(a.this.getContext());
                a.this.b.setBackgroundResource(R.drawable.btn_fb_connect);
                System.out.println("You have logged out");
                Toast.makeText(MainActivity.this.getBaseContext(), "Signed out ", 0).show();
            }
        }
    }

    public void onDestroy() {
        super.onDestroy();
        System.gc();
    }

    public void a(Exception exc) {
    }

    public void a(int i2, String str) {
    }

    public void a(boolean z, ClevernetView clevernetView) {
    }

    public void a(com.google.android.gms.common.a aVar) {
        if (aVar.a()) {
            try {
                aVar.a(this, 9000);
            } catch (IntentSender.SendIntentException e2) {
                this.t.a();
            }
        }
        this.u = aVar;
    }

    public void a(Bundle bundle) {
    }

    public void b() {
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.v.a(this.t, String.valueOf(getString(R.string.link)) + getPackageName(), 0);
    }
}
