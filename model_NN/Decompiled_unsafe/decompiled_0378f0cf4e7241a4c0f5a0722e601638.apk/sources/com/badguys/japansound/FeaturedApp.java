package com.badguys.japansound;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;

public class FeaturedApp extends Activity {
    private WebView a;
    private ImageView b;
    /* access modifiers changed from: private */
    public ProgressDialog c;
    private String d = "http://www.androidbestapp.net/featuredapp/index.html";

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        setContentView((int) R.layout.featuredapp_layout);
        this.b = (ImageView) findViewById(R.id.btn_back);
        this.b.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                FeaturedApp.this.finish();
            }
        });
        this.a = (WebView) findViewById(R.id.webView1);
        this.a.getSettings().setJavaScriptEnabled(true);
        this.a.setScrollBarStyle(33554432);
        final AlertDialog create = new AlertDialog.Builder(this).create();
        this.c = ProgressDialog.show(this, "Featured Apps", "Data loading...");
        this.a.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView webView, String str) {
                if (str.startsWith("https://play.google.com")) {
                    FeaturedApp.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(str)));
                    return true;
                }
                webView.loadUrl(str);
                return true;
            }

            public void onPageFinished(WebView webView, String str) {
                if (FeaturedApp.this.c.isShowing()) {
                    FeaturedApp.this.c.dismiss();
                }
            }

            public void onReceivedError(WebView webView, int i, String str, String str2) {
                create.setTitle("Error");
                create.setMessage(str);
                create.setButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                    }
                });
                create.show();
            }
        });
        this.a.loadUrl(this.d);
    }
}
