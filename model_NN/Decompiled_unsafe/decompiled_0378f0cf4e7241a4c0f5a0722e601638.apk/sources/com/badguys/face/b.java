package com.badguys.face;

import java.util.Iterator;
import java.util.LinkedList;

/* compiled from: SessionEvents */
public class b {
    private static LinkedList<a> a = new LinkedList<>();
    private static LinkedList<C0009b> b = new LinkedList<>();

    /* compiled from: SessionEvents */
    public interface a {
        void a();

        void a(String str);
    }

    /* renamed from: com.badguys.face.b$b  reason: collision with other inner class name */
    /* compiled from: SessionEvents */
    public interface C0009b {
        void b();

        void c();
    }

    public static void a(a aVar) {
        a.clear();
        a.add(aVar);
    }

    public static void a(C0009b bVar) {
        b.clear();
        b.add(bVar);
    }

    public static void a() {
        Iterator<a> it = a.iterator();
        while (it.hasNext()) {
            it.next().a();
        }
    }

    public static void a(String str) {
        Iterator<a> it = a.iterator();
        while (it.hasNext()) {
            it.next().a(str);
        }
    }

    public static void b() {
        Iterator<C0009b> it = b.iterator();
        while (it.hasNext()) {
            it.next().b();
        }
    }

    public static void c() {
        Iterator<C0009b> it = b.iterator();
        while (it.hasNext()) {
            it.next().c();
        }
    }
}
