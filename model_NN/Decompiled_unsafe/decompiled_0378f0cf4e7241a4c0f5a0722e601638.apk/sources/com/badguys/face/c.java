package com.badguys.face;

import android.content.Context;
import android.content.SharedPreferences;

/* compiled from: SessionStore */
public class c {
    public static boolean a(com.facebook.a.c cVar, Context context) {
        SharedPreferences.Editor edit = context.getSharedPreferences("facebook-session", 0).edit();
        edit.putString("access_token", cVar.c());
        edit.putLong("expires_in", cVar.d());
        return edit.commit();
    }

    public static boolean b(com.facebook.a.c cVar, Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("facebook-session", 0);
        cVar.a(sharedPreferences.getString("access_token", null));
        cVar.a(sharedPreferences.getLong("expires_in", 0));
        return cVar.a();
    }

    public static void a(Context context) {
        SharedPreferences.Editor edit = context.getSharedPreferences("facebook-session", 0).edit();
        edit.clear();
        edit.commit();
    }
}
