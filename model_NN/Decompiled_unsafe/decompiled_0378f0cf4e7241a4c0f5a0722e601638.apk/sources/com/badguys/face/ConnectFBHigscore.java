package com.badguys.face;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageButton;

public class ConnectFBHigscore extends ImageButton {
    public ConnectFBHigscore(Context context) {
        super(context);
    }

    public ConnectFBHigscore(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public ConnectFBHigscore(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }
}
