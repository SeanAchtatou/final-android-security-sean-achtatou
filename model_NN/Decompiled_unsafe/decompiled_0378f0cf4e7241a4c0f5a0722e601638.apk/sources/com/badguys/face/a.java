package com.badguys.face;

import android.util.Log;
import com.facebook.a.a;
import com.facebook.a.d;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;

/* compiled from: BaseRequestListener */
public abstract class a implements a.C0011a {
    public void a(d dVar, Object obj) {
        Log.e("Facebook", dVar.getMessage());
        dVar.printStackTrace();
    }

    public void a(FileNotFoundException fileNotFoundException, Object obj) {
        Log.e("Facebook", fileNotFoundException.getMessage());
        fileNotFoundException.printStackTrace();
    }

    public void a(IOException iOException, Object obj) {
        Log.e("Facebook", iOException.getMessage());
        iOException.printStackTrace();
    }

    public void a(MalformedURLException malformedURLException, Object obj) {
        Log.e("Facebook", malformedURLException.getMessage());
        malformedURLException.printStackTrace();
    }
}
