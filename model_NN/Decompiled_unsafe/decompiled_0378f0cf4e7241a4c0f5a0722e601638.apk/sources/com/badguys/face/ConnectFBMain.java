package com.badguys.face;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.widget.ImageButton;
import com.badguys.face.b;
import com.badguys.japansound.MainActivity;
import com.badguys.japansound.R;
import com.facebook.a.c;

public class ConnectFBMain extends ImageButton {
    c a;
    Handler b;
    MainActivity.a.c c;
    String[] d;
    Activity e;
    int f;

    public ConnectFBMain(Context context) {
        super(context);
    }

    public ConnectFBMain(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public ConnectFBMain(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    public void a(Activity activity, int i, c cVar, String[] strArr, MainActivity.a.c cVar2) {
        this.e = activity;
        this.f = i;
        this.a = cVar;
        this.c = cVar2;
        this.d = strArr;
        this.b = new Handler();
        setBackgroundResource(cVar.a() ? R.drawable.btn_fb_signout : R.drawable.btn_fb_connect);
        drawableStateChanged();
        b.a((b.a) this.c);
        b.a((b.C0009b) this.c);
    }
}
