package com.badguys.face;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageButton;
import com.badguys.face.b;
import com.badguys.japansound.R;
import com.facebook.a.c;

public class ConnectFBButton extends ImageButton {
    /* access modifiers changed from: private */
    public c a;
    private a b = new a(this, null);

    public ConnectFBButton(Context context) {
        super(context);
    }

    public ConnectFBButton(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public ConnectFBButton(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    private class a implements b.a, b.C0009b {
        private a() {
        }

        /* synthetic */ a(ConnectFBButton connectFBButton, a aVar) {
            this();
        }

        public void a() {
            ConnectFBButton.this.setBackgroundResource(R.drawable.btn_fb_connect);
            c.a(ConnectFBButton.this.a, ConnectFBButton.this.getContext());
        }

        public void a(String str) {
        }

        public void b() {
        }

        public void c() {
            c.a(ConnectFBButton.this.getContext());
            ConnectFBButton.this.setBackgroundResource(R.drawable.btn_fb_connect);
        }
    }
}
