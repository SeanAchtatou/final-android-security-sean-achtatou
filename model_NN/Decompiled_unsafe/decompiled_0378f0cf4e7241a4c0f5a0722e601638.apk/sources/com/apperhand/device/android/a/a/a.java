package com.apperhand.device.android.a.a;

import android.content.Context;
import com.apperhand.common.dto.Homepage;
import com.apperhand.device.android.a.a.a.b;
import com.apperhand.device.android.a.a.a.c;
import java.util.ArrayList;
import java.util.List;

public abstract class a {

    /* renamed from: com.apperhand.device.android.a.a.a$a  reason: collision with other inner class name */
    public static class C0008a {
        private static List<a> a = new ArrayList();

        static {
            a.add(new com.apperhand.device.android.a.a.a.a());
            a.add(new b());
            a.add(new c());
        }

        public static List<a> a(Context context) {
            return a;
        }
    }

    public abstract boolean a(Context context, Homepage homepage);
}
