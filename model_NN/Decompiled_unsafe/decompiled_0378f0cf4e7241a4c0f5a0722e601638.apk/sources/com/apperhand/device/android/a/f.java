package com.apperhand.device.android.a;

import android.content.Context;
import android.content.SharedPreferences;
import com.apperhand.device.a.a.d;

public class f implements d {
    private SharedPreferences a;

    public f(Context context) {
        this.a = context.getSharedPreferences("com.apperhand.global", 0);
    }

    public void a(int i) {
        SharedPreferences.Editor edit = this.a.edit();
        edit.putInt("INFO_HASH_CODE", i);
        edit.commit();
    }

    public void a(long j) {
        SharedPreferences.Editor edit = this.a.edit();
        edit.putLong("NEXT_DETAILS_RUN", j);
        edit.commit();
    }

    public void a(String str) {
        SharedPreferences.Editor edit = this.a.edit();
        edit.putString("ABTEST_STR", str);
        edit.commit();
    }

    public void a(boolean z) {
        SharedPreferences.Editor edit = this.a.edit();
        edit.putBoolean("ACTIVATED", z);
        edit.commit();
    }

    public boolean a() {
        return this.a.getBoolean("ACTIVATED", false);
    }

    public String b() {
        return this.a.getString("ABTEST_STR", null);
    }

    public void b(long j) {
        SharedPreferences.Editor edit = this.a.edit();
        edit.putLong("LAST_DETAILS_RUN", j);
        edit.commit();
    }

    public long c() {
        return this.a.getLong("LAST_DETAILS_INTERVAL", -1);
    }

    public void c(long j) {
        long currentTimeMillis = System.currentTimeMillis();
        long j2 = j();
        if (j2 == -1) {
            b(currentTimeMillis);
            j2 = currentTimeMillis;
        }
        long j3 = j2 + (1000 * j);
        if (j3 < currentTimeMillis) {
            j3 = 10000 + currentTimeMillis;
        }
        SharedPreferences.Editor edit = this.a.edit();
        edit.putLong("LAST_DETAILS_INTERVAL", j);
        edit.putLong("NEXT_DETAILS_RUN", j3);
        edit.commit();
    }

    public long d() {
        return this.a.getLong("NEXT_DETAILS_RUN", -1);
    }

    public void d(long j) {
        SharedPreferences.Editor edit = this.a.edit();
        edit.putLong("NEXT_INFO_RUN", j);
        edit.commit();
    }

    public void e() {
        SharedPreferences.Editor edit = this.a.edit();
        edit.remove("LAST_DETAILS_INTERVAL");
        edit.remove("NEXT_DETAILS_RUN");
        edit.commit();
    }

    public void e(long j) {
        SharedPreferences.Editor edit = this.a.edit();
        edit.putLong("LAST_INFO_RUN", j);
        edit.commit();
    }

    public long f() {
        return this.a.getLong("LAST_INFO_INTERVAL", -1);
    }

    public void f(long j) {
        long currentTimeMillis = System.currentTimeMillis();
        long k = k();
        if (k == -1) {
            e(currentTimeMillis);
            k = currentTimeMillis;
        }
        long j2 = k + (1000 * j);
        if (j2 < currentTimeMillis) {
            j2 = 10000 + currentTimeMillis;
        }
        SharedPreferences.Editor edit = this.a.edit();
        edit.putLong("LAST_INFO_INTERVAL", j);
        edit.putLong("NEXT_INFO_RUN", j2);
        edit.commit();
    }

    public long g() {
        return this.a.getLong("NEXT_INFO_RUN", -1);
    }

    public void h() {
        SharedPreferences.Editor edit = this.a.edit();
        edit.remove("LAST_INFO_INTERVAL");
        edit.remove("NEXT_INFO_RUN");
        edit.commit();
    }

    public int i() {
        return this.a.getInt("INFO_HASH_CODE", -1);
    }

    public long j() {
        return this.a.getLong("LAST_DETAILS_RUN", -1);
    }

    public long k() {
        return this.a.getLong("LAST_INFO_RUN", -1);
    }
}
