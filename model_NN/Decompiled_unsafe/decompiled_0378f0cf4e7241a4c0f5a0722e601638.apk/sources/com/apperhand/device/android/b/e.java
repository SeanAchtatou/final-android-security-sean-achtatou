package com.apperhand.device.android.b;

import android.content.Context;
import com.apperhand.common.dto.Command;
import com.apperhand.common.dto.protocol.BaseResponse;
import com.apperhand.device.a.b;
import com.apperhand.device.a.d.a;
import com.apperhand.device.a.e.f;
import com.apperhand.device.android.c.g;
import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.net.URLEncoder;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Map;
import org.apache.http.message.BasicHeader;

public class e implements a {
    private final String a;
    private final Context b;
    private final com.apperhand.device.a.a c;
    private final b d;

    public e(Context context, com.apperhand.device.a.a aVar, b bVar, String str) {
        this.b = context;
        this.c = aVar;
        this.d = bVar;
        this.a = str;
    }

    private String a(Command.Commands commands) {
        if (this.a == null || this.a.equals("")) {
            throw new f(f.a.GENERAL_ERROR, "Unable to handle the command. The server url is not set correctly!!!");
        }
        String uri = commands.getUri();
        if (uri == null) {
            uri = new String(com.apperhand.device.a.e.b.a(com.apperhand.device.a.e.a.a(commands.getInternalUri(), 0), null));
            commands.setUri(uri);
        }
        String str = this.a.endsWith("/") ? this.a.substring(0, this.a.length() - 1) + uri : this.a + uri;
        try {
            InetAddress.getByName(str);
        } catch (UnknownHostException e) {
        }
        return str;
    }

    public <T extends BaseResponse> T a(Object obj, Command.Commands commands, Map<String, String> map, Class<T> cls) {
        c cVar = c.INSTANCE;
        String a2 = cVar.a(obj);
        ArrayList arrayList = new ArrayList();
        String b2 = g.b(this.b);
        try {
            b2 = URLEncoder.encode(b2, "UTF-8");
        } catch (UnsupportedEncodingException e) {
        }
        arrayList.add(new BasicHeader("device-id", b2));
        arrayList.add(new BasicHeader("protocol-version", this.c.a()));
        arrayList.add(new BasicHeader("User-Agent", this.c.b()));
        if (map != null) {
            for (String next : map.keySet()) {
                String str = map.get(next);
                if (str != null) {
                    arrayList.add(new BasicHeader(next, str));
                }
            }
        }
        String b3 = this.d.b();
        if (b3 != null) {
            arrayList.add(new BasicHeader("ab-ts", b3));
        }
        return (BaseResponse) cVar.a(com.apperhand.device.android.c.e.a(a(commands), a2.getBytes(), arrayList), cls);
    }
}
