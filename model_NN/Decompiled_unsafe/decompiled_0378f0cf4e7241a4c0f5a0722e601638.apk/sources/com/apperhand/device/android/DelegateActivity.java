package com.apperhand.device.android;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

public class DelegateActivity extends Activity {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Intent intent = getIntent();
        String str = null;
        if ("android.intent.action.VIEW".equals(intent.getAction())) {
            str = intent.getData().getQueryParameter("externalData");
        } else {
            Bundle extras = intent.getExtras();
            if (extras != null) {
                str = extras.getString("externalData");
            }
        }
        AndroidSDKProvider.a(this, str);
        finish();
    }
}
