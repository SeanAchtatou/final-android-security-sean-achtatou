package com.apperhand.device.android.a;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import com.apperhand.common.dto.Bookmark;

public abstract class i {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    private ContentValues a(Bookmark bookmark, String str) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("title", bookmark.getTitle());
        contentValues.put("bookmark", str);
        contentValues.put("url", bookmark.getUrl());
        contentValues.put("date", Long.valueOf(System.currentTimeMillis()));
        contentValues.put("visits", (Integer) 100);
        contentValues.put("created", Long.valueOf(System.currentTimeMillis()));
        return contentValues;
    }

    /* access modifiers changed from: protected */
    public long a(ContentResolver contentResolver, Bookmark bookmark, Uri uri, String str) {
        String uri2 = contentResolver.insert(uri, a(bookmark, str)).toString();
        return Long.parseLong(uri2.substring(uri2.lastIndexOf("/") + 1));
    }

    /* access modifiers changed from: protected */
    public Cursor a(ContentResolver contentResolver, Uri uri, String str) {
        return contentResolver.query(uri, new String[]{"_id", "title", "url", "visits", "date", "created"}, str, null, null);
    }

    /* access modifiers changed from: protected */
    public void a(Cursor cursor, Bookmark bookmark) {
        bookmark.setId(cursor.getLong(cursor.getColumnIndex("_id")));
        bookmark.setTitle(cursor.getString(cursor.getColumnIndex("title")));
        bookmark.setIdentifier(null);
        bookmark.setUrl(cursor.getString(cursor.getColumnIndex("url")));
    }

    /* access modifiers changed from: protected */
    public boolean a(ContentResolver contentResolver, Uri uri, long j, Bookmark bookmark, String str) {
        return contentResolver.update(uri, a(bookmark, str), "_id=?", new String[]{String.valueOf(j)}) > 0;
    }
}
