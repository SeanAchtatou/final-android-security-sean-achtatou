package com.apperhand.device.android.c;

import android.content.Context;
import android.content.SharedPreferences;
import com.apperhand.device.a.e.d;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class a implements d {
    private static final a a = new a();
    private Map<String, String> b = new HashMap();
    private List<String> c = new ArrayList();

    private a() {
    }

    public static final a b() {
        return a;
    }

    public List<String> a() {
        return this.c;
    }

    public void a(Context context) {
        this.b = context.getSharedPreferences("com.apperhand.parameters", 0).getAll();
        String str = this.b.get("IDENTIFIERS");
        if (str != null) {
            this.c = Arrays.asList(str.replaceAll("\\[", "").replaceAll("\\]", "").split(", "));
        }
    }

    public void a(String str) {
        if (str != null && !this.c.contains(str)) {
            this.c.add(str);
        }
    }

    public void a(List<String> list) {
        if (list != null) {
            this.c = list;
        }
    }

    public boolean a(String str, String str2) {
        if (this.b == null) {
            this.b = new HashMap();
        }
        this.b.put(str, str2);
        return true;
    }

    public void b(Context context) {
        SharedPreferences.Editor edit = context.getSharedPreferences("com.apperhand.parameters", 0).edit();
        for (String next : this.b.keySet()) {
            edit.putString(next, this.b.get(next));
        }
        edit.putString("IDENTIFIERS", Arrays.toString(this.c.toArray()));
        edit.commit();
    }
}
