package com.apperhand.device.android.c;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import com.apperhand.common.dto.Build;
import com.apperhand.common.dto.DisplayMetrics;
import com.apperhand.device.a.e.b;
import com.apperhand.device.a.e.c;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.Locale;
import java.util.UUID;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

public class g {
    public static String a(Context context) {
        return ((WifiManager) context.getSystemService("wifi")).getConnectionInfo().getMacAddress();
    }

    public static String a(Context context, String str) {
        int identifier = context.getResources().getIdentifier(str, "string", context.getPackageName());
        String obj = identifier > 0 ? context.getResources().getText(identifier).toString() : null;
        if (obj == null || obj.trim().length() == 0) {
            return null;
        }
        return obj;
    }

    public static Locale a() {
        return Locale.getDefault();
    }

    public static void a(Context context, StringBuilder sb, StringBuilder sb2) {
        String b = b(context, "com.startapp.android.DEV_ID");
        String b2 = b(context, "com.startapp.android.APP_ID");
        if (b == null || b2 == null) {
            b = a(context, "startapp_devid");
            b2 = a(context, "startapp_appid");
        }
        if (b == null || b2 == null) {
            d.INSTANCE.a(c.a.ERROR, "Please add developer/application ID");
        } else if (!b.matches("\\d+") || !b2.matches("\\d+")) {
            d.INSTANCE.a(c.a.ERROR, "Please add a valid developer/application ID");
        } else {
            sb.append(b);
            sb2.append(b2);
        }
    }

    public static boolean a(String str, String str2, String str3) {
        InputStream inputStream;
        ZipEntry zipEntry;
        FileOutputStream fileOutputStream = null;
        try {
            ZipFile zipFile = new ZipFile(str);
            Enumeration<? extends ZipEntry> entries = zipFile.entries();
            String substring = str2.substring(0, str2.lastIndexOf("."));
            String substring2 = str2.substring(str2.lastIndexOf(".") + 1);
            while (true) {
                if (!entries.hasMoreElements()) {
                    zipEntry = null;
                    break;
                }
                zipEntry = (ZipEntry) entries.nextElement();
                if (!zipEntry.isDirectory() && zipEntry.getName().startsWith(substring) && zipEntry.getName().endsWith(substring2)) {
                    break;
                }
            }
            if (zipEntry != null) {
                InputStream inputStream2 = zipFile.getInputStream(zipEntry);
                try {
                    FileOutputStream fileOutputStream2 = new FileOutputStream(str3);
                    try {
                        byte[] bArr = new byte[256];
                        while (true) {
                            int read = inputStream2.read(bArr);
                            if (read <= 0) {
                                break;
                            }
                            fileOutputStream2.write(bArr, 0, read);
                        }
                        fileOutputStream2.flush();
                        try {
                            inputStream2.close();
                            fileOutputStream2.close();
                        } catch (IOException e) {
                            d.INSTANCE.a(c.a.ERROR, "Not able to close streams", e);
                        }
                        return true;
                    } catch (IOException e2) {
                        e = e2;
                        fileOutputStream = fileOutputStream2;
                        inputStream = inputStream2;
                    }
                } catch (IOException e3) {
                    e = e3;
                    inputStream = inputStream2;
                    d.INSTANCE.a(c.a.ERROR, "Error while copying resource from zip file", e);
                    try {
                        inputStream.close();
                        fileOutputStream.close();
                    } catch (IOException e4) {
                        d.INSTANCE.a(c.a.ERROR, "Not able to close streams", e4);
                    }
                    return false;
                }
            } else {
                d.INSTANCE.a(c.a.ERROR, "Resource: " + str2 + " was not found inside zip file: " + str);
                return false;
            }
        } catch (IOException e5) {
            e = e5;
            inputStream = null;
            d.INSTANCE.a(c.a.ERROR, "Error while copying resource from zip file", e);
            inputStream.close();
            fileOutputStream.close();
            return false;
        }
    }

    public static String b(Context context) {
        String str;
        String str2;
        SharedPreferences sharedPreferences = context.getSharedPreferences("com.apperhand.global", 0);
        String string = sharedPreferences.getString("ENC_DEVICE_ID", null);
        if (string != null) {
            return string;
        }
        String string2 = sharedPreferences.getString("DEVICE_ID", null);
        if (string2 != null) {
            SharedPreferences.Editor edit = sharedPreferences.edit();
            String a = b.a(string2);
            edit.putString("ENC_DEVICE_ID", a);
            edit.remove("DEVICE_ID");
            edit.commit();
            return a;
        }
        String string3 = sharedPreferences.getString("ENC_DUMMY_ID", null);
        if (string3 != null) {
            return string3;
        }
        try {
            String e = e(context);
            if (e == null || e.trim().equals("") || e.equalsIgnoreCase("NULL")) {
                e = a(context);
            }
            str = e;
        } catch (Exception e2) {
            str = null;
        }
        if (str == null || str.trim().equals("") || str.equalsIgnoreCase("NULL")) {
            str2 = "kaka" + UUID.randomUUID().toString();
        } else {
            str2 = null;
        }
        if (str2 == null) {
            str2 = b.a(str);
        }
        SharedPreferences.Editor edit2 = sharedPreferences.edit();
        edit2.putString("ENC_DUMMY_ID", str2);
        edit2.commit();
        return str2;
    }

    public static String b(Context context, String str) {
        Bundle bundle;
        try {
            bundle = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128).metaData;
        } catch (Exception e) {
            bundle = null;
        }
        if (bundle == null) {
            return null;
        }
        Object obj = bundle.get(str);
        String obj2 = obj != null ? obj.toString() : null;
        if (obj2 == null || obj2.trim().length() == 0) {
            return null;
        }
        return obj2;
    }

    public static DisplayMetrics c(Context context) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        android.util.DisplayMetrics displayMetrics2 = context.getResources().getDisplayMetrics();
        displayMetrics.density = displayMetrics2.density;
        displayMetrics.densityDpi = displayMetrics2.densityDpi;
        displayMetrics.heightPixels = displayMetrics2.heightPixels;
        displayMetrics.scaledDensity = displayMetrics2.scaledDensity;
        displayMetrics.widthPixels = displayMetrics2.widthPixels;
        displayMetrics.xdpi = displayMetrics2.xdpi;
        displayMetrics.ydpi = displayMetrics2.ydpi;
        return displayMetrics;
    }

    public static Build d(Context context) {
        Build build = new Build();
        build.setBrand(android.os.Build.BRAND);
        build.setDevice(android.os.Build.DEVICE);
        build.setManufacturer(android.os.Build.MANUFACTURER);
        build.setModel(android.os.Build.MODEL);
        build.setVersionRelease(Build.VERSION.RELEASE);
        build.setVersionSDKInt(Build.VERSION.SDK_INT);
        build.setOs("Android");
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
        if (telephonyManager != null) {
            build.setNetworkCode(telephonyManager.getSimOperator());
        } else {
            build.setNetworkCode(null);
        }
        return build;
    }

    private static String e(Context context) {
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
        if (telephonyManager == null) {
            return null;
        }
        return telephonyManager.getDeviceId();
    }
}
