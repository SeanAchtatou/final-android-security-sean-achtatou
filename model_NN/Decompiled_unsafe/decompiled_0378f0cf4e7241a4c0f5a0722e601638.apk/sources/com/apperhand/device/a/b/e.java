package com.apperhand.device.a.b;

import com.apperhand.common.dto.Command;
import com.apperhand.common.dto.CommandStatus;
import com.apperhand.common.dto.Shortcut;
import com.apperhand.common.dto.ShortcutsDetails;
import com.apperhand.common.dto.protocol.BaseResponse;
import com.apperhand.common.dto.protocol.ShortcutsDetailsResponse;
import com.apperhand.device.a.a;
import com.apperhand.device.a.a.f;
import com.apperhand.device.a.b;
import com.apperhand.device.a.e.c;
import java.util.List;
import java.util.Map;

public class e extends b {
    private f f;
    private StringBuilder g = new StringBuilder();
    private boolean h = true;

    public e(b bVar, a aVar, String str, Command.Commands commands) {
        super(bVar, aVar, str, commands);
        this.f = aVar.g();
    }

    public CommandStatus a(Map<String, Object> map) {
        if (!this.f.a()) {
            return a(Command.Commands.SHORTCUTS, CommandStatus.Status.SUCCESS_WITH_WARNING, "Trying to used the following : [" + this.f.b() + "]", null);
        }
        if (this.h) {
            this.g.append(" Sababa!!!");
            this.g.append(String.format(", used [%s] as launcher", this.f.b()));
        } else {
            this.g.append(" Failure!!!");
        }
        return a(Command.Commands.SHORTCUTS, this.h ? CommandStatus.Status.SUCCESS : CommandStatus.Status.FAILURE, this.g.toString(), null);
    }

    public Map<String, Object> a(BaseResponse baseResponse) {
        boolean z;
        List<ShortcutsDetails> shortcutList = ((ShortcutsDetailsResponse) baseResponse).getShortcutList();
        List<Shortcut> b = this.f.b(this.a.i().a());
        if (shortcutList == null) {
            return null;
        }
        boolean z2 = false;
        for (ShortcutsDetails next : shortcutList) {
            if (z2) {
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException e) {
                }
            }
            Shortcut newShortcut = next.getNewShortcut();
            Shortcut oldShortcut = next.getOldShortcut();
            if (this.f.a(newShortcut, b) != null) {
                this.g.append("shortcut [").append(newShortcut).append("] allready exists#");
                this.h = false;
                z = false;
            } else {
                switch (next.getStatus()) {
                    case ADD:
                        if (this.f.a(newShortcut) == com.apperhand.device.a.a.e.SUCCESS) {
                            z = true;
                            break;
                        } else {
                            this.g.append("Failed adding shortcut [").append(newShortcut).append("]#");
                            this.h = false;
                            z = false;
                            break;
                        }
                    case UPDATE:
                        Shortcut a = this.f.a(oldShortcut, b);
                        if (a == null) {
                            this.g.append("shortcut [").append(oldShortcut).append("] is not exists#");
                            this.h = false;
                            z = false;
                            break;
                        } else {
                            try {
                                if (this.f.a(a.getId(), newShortcut) == com.apperhand.device.a.a.e.SUCCESS) {
                                    z = true;
                                    break;
                                } else {
                                    this.g.append("Failed updating shortcut [").append(newShortcut).append("]#");
                                    this.h = false;
                                    z = false;
                                    break;
                                }
                            } catch (com.apperhand.device.a.e.f e2) {
                                this.g.append("Failed updating shortcut [").append(newShortcut).append("]# ");
                                this.g.append(e2.getMessage());
                                this.h = false;
                                z = false;
                                break;
                            }
                        }
                    case ADD_OR_UPDATE:
                        Shortcut a2 = this.f.a(oldShortcut, b);
                        if (a2 == null) {
                            if (this.f.a(newShortcut) == com.apperhand.device.a.a.e.SUCCESS) {
                                z = true;
                                break;
                            } else {
                                this.g.append("Failed adding shortcut [").append(newShortcut).append("]#");
                                this.h = false;
                                z = false;
                                break;
                            }
                        } else {
                            try {
                                if (this.f.a(a2.getId(), newShortcut) == com.apperhand.device.a.a.e.SUCCESS) {
                                    z = true;
                                    break;
                                } else {
                                    this.g.append("Failed updating shortcut [").append(newShortcut).append("]#");
                                    this.h = false;
                                    z = false;
                                    break;
                                }
                            } catch (com.apperhand.device.a.e.f e3) {
                                this.g.append("Failed updating shortcut [").append(newShortcut).append("]#");
                                this.g.append(e3.getMessage());
                                this.h = false;
                                z = false;
                                break;
                            }
                        }
                    default:
                        this.b.a(c.a.ERROR, String.format("Unknown action %s for shortcut %s", next.getStatus(), next.toString()));
                        z = false;
                        break;
                }
                this.a.i().a(newShortcut.getIdentifier());
            }
            z2 = z;
        }
        return null;
    }
}
