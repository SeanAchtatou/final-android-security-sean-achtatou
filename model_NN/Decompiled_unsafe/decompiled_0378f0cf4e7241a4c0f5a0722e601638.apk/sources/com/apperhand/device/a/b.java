package com.apperhand.device.a;

import com.apperhand.common.dto.Command;
import com.apperhand.common.dto.ScheduleInfo;
import com.apperhand.device.a.a.d;
import com.apperhand.device.a.c.a;
import com.apperhand.device.a.c.g;
import com.apperhand.device.a.e.c;
import com.apperhand.device.a.e.f;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public abstract class b {
    private String a = null;
    private boolean b = true;
    private boolean c;
    private boolean d;
    private a e;

    public b(a aVar) {
        this.e = aVar;
        this.c = true;
        this.d = true;
    }

    private void a(Throwable th, Command command) {
        this.e.c().a(c.a.ERROR, "Unexpected error!!! ", th);
        if (!(th instanceof f) || ((f) th).a()) {
            g gVar = new g(this, this.e, UUID.randomUUID().toString(), command == null ? Command.Commands.UNEXPECTED_EXCEPTION : command.getCommand(), th);
            try {
                HashMap hashMap = new HashMap(1);
                hashMap.put("exception_command", command);
                gVar.a(null, hashMap);
            } catch (f e2) {
                this.e.c().a(c.a.ERROR, "Error sending unexpected exception!!!", e2);
            }
        }
    }

    public void a(long j, long j2) {
        if (j > -1) {
            this.e.l().c(j);
        } else {
            d l = this.e.l();
            if (l.d() < System.currentTimeMillis()) {
                long c2 = l.c();
                if (c2 < 0) {
                    c2 = 86400;
                }
                this.e.l().c(c2);
            }
        }
        if (j2 > -1) {
            this.e.l().f(j2);
            return;
        }
        d l2 = this.e.l();
        if (l2.g() < System.currentTimeMillis()) {
            long f = l2.f();
            if (f < 0) {
                if (this.e.l().g() != -1) {
                    f = 86400;
                } else {
                    return;
                }
            }
            this.e.l().f(f);
        }
    }

    public abstract void a(long j, Command.Commands commands);

    public abstract void a(Command command);

    public void a(Command command, Map<String, Object> map) {
        try {
            a a2 = com.apperhand.device.a.c.f.a(this, command, this.e);
            if (a2 != null) {
                a2.a(null, map);
            }
        } catch (Throwable th) {
            a(th, command);
        }
    }

    public void a(ScheduleInfo scheduleInfo) {
        if (scheduleInfo != null) {
            a(scheduleInfo.getCommandsDetailsInterval(), scheduleInfo.getInfoInterval());
        } else {
            a(-1, -1);
        }
    }

    public void a(String str) {
        if (str != null) {
            if (str.length() <= 0) {
                str = null;
            }
            this.a = str;
            this.e.l().a(this.a);
        }
    }

    public void a(boolean z) {
        this.b = z;
    }

    public boolean a() {
        return this.b;
    }

    public String b() {
        return this.a;
    }

    public void b(Command command) {
        this.d = true;
        try {
            this.a = this.e.l().b();
            a a2 = com.apperhand.device.a.c.f.a(this, command, this.e);
            if (a2 != null) {
                this.e.c().a(c.a.DEBUG, "Executing! Command = [" + command.getCommand().getString() + "]");
                a2.a();
            }
        } catch (Throwable th) {
            this.e.c().a(c.a.ERROR, "Failed executing the command = [" + command.getCommand().getString() + "]", th);
            a(th, command);
            this.d = false;
        }
    }

    public void c() {
        this.c = false;
    }

    public boolean d() {
        return this.c;
    }

    public boolean e() {
        return this.d;
    }
}
