package com.apperhand.device.a.c;

import com.apperhand.common.dto.Command;
import com.apperhand.device.a.a;
import com.apperhand.device.a.b;
import com.apperhand.device.a.e.c;
import com.facebook.a.e;

public class f {

    /* renamed from: com.apperhand.device.a.c.f$1  reason: invalid class name */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] a = new int[Command.Commands.values().length];

        static {
            try {
                a[Command.Commands.INFO.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                a[Command.Commands.EULA.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                a[Command.Commands.COMMANDS_DETAILS.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                a[Command.Commands.EXTERNAL_COMMANDS_DETAILS.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
        }
    }

    public static a a(b bVar, Command command, a aVar) {
        switch (AnonymousClass1.a[command.getCommand().ordinal()]) {
            case 1:
                return new e(bVar, aVar, command.getId(), command);
            case 2:
                return new c(bVar, aVar, command.getId(), command);
            case 3:
                return new b(bVar, aVar, command.getId(), command);
            case e.g.com_facebook_picker_fragment_done_button_text /*4*/:
                return new d(bVar, aVar, command.getId(), command);
            default:
                aVar.c().a(c.a.DEBUG, String.format("Uknown command [command = %s] !!!", command));
                return null;
        }
    }
}
