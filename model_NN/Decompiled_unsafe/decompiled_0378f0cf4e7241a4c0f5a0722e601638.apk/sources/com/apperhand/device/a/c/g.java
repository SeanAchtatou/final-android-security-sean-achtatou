package com.apperhand.device.a.c;

import com.apperhand.common.dto.Command;
import com.apperhand.common.dto.CommandStatus;
import com.apperhand.common.dto.protocol.BaseResponse;
import com.apperhand.common.dto.protocol.CommandStatusDetailsRequest;
import com.apperhand.device.a.a;
import com.apperhand.device.a.b;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.List;
import java.util.Map;

public class g extends a {
    private Throwable f;

    public g(b bVar, a aVar, String str, Command.Commands commands, Throwable th) {
        super(bVar, aVar, str, commands);
        this.f = th;
    }

    private List<CommandStatus> b(Command command) {
        StringWriter stringWriter = new StringWriter();
        this.f.printStackTrace(new PrintWriter(stringWriter));
        return a(command.getCommand(), CommandStatus.Status.EXCEPTION, this.f.getMessage() + "#NL#" + stringWriter.toString(), null);
    }

    /* access modifiers changed from: protected */
    public CommandStatusDetailsRequest a(Command command) {
        CommandStatusDetailsRequest b = super.b();
        b.setStatuses(b(command));
        b.setApplicationDetails(this.d.e());
        b.setAbTestId(this.e.b());
        return b;
    }

    /* access modifiers changed from: protected */
    public Map<String, Object> a(BaseResponse baseResponse) {
        return null;
    }

    public void a(BaseResponse baseResponse, Map<String, Object> map) {
        Command command = null;
        if (map != null) {
            command = (Command) map.get("exception_command");
        }
        a(a(command));
    }

    /* access modifiers changed from: protected */
    public BaseResponse d() {
        return null;
    }
}
