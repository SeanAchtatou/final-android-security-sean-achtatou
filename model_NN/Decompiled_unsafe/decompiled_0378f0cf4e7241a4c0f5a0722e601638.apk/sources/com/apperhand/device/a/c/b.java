package com.apperhand.device.a.c;

import com.apperhand.common.dto.Command;
import com.apperhand.common.dto.CommandStatus;
import com.apperhand.common.dto.ScheduleInfo;
import com.apperhand.common.dto.protocol.BaseResponse;
import com.apperhand.common.dto.protocol.CommandStatusDetailsRequest;
import com.apperhand.common.dto.protocol.CommandsDetailsRequest;
import com.apperhand.common.dto.protocol.CommandsDetailsResponse;
import com.apperhand.device.a.a;
import com.apperhand.device.a.a.d;
import com.apperhand.device.a.e.c;
import com.apperhand.device.a.e.e;
import com.apperhand.device.a.e.f;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class b extends a {
    private boolean f = false;
    private d g;
    private List<com.apperhand.device.a.b.b> h = null;

    public b(com.apperhand.device.a.b bVar, a aVar, String str, Command command) {
        super(bVar, aVar, str, command.getCommand());
        this.g = aVar.l();
        this.h = new ArrayList();
    }

    private CommandsDetailsResponse a(CommandsDetailsRequest commandsDetailsRequest) {
        Command.Commands commands = Command.Commands.COMMANDS_DETAILS;
        HashMap hashMap = null;
        if (this.e.a()) {
            hashMap = new HashMap(1);
            hashMap.put("first-time", Boolean.TRUE.toString());
        }
        try {
            return (CommandsDetailsResponse) this.d.d().a(commandsDetailsRequest, commands, hashMap, CommandsDetailsResponse.class);
        } catch (f e) {
            this.d.c().a(c.a.DEBUG, "Unable to handle get commands details command!!!!", e);
            throw e;
        }
    }

    private void a(Map<String, String> map) {
        if (map != null && map.size() > 0) {
            for (String next : map.keySet()) {
                this.d.i().a(next, map.get(next));
            }
        }
    }

    private List<CommandStatus> b(Map<String, Object> map) {
        ArrayList arrayList = new ArrayList();
        if (this.h != null) {
            Iterator<com.apperhand.device.a.b.b> it = this.h.iterator();
            while (it.hasNext()) {
                com.apperhand.device.a.b.b next = it.next();
                CommandStatus b = next != null ? next.b(map) : null;
                if (b != null) {
                    arrayList.add(b);
                }
            }
        }
        return arrayList;
    }

    private CommandsDetailsRequest e() {
        CommandsDetailsRequest commandsDetailsRequest = new CommandsDetailsRequest();
        commandsDetailsRequest.setApplicationDetails(this.d.e());
        commandsDetailsRequest.setAbTestId(this.e.b());
        ScheduleInfo scheduleInfo = new ScheduleInfo();
        scheduleInfo.setCommandsDetailsInterval(this.d.l().c());
        scheduleInfo.setInfoInterval(this.d.l().f());
        commandsDetailsRequest.setScheduleInfo(scheduleInfo);
        commandsDetailsRequest.setSupportLauncher(this.d.g().a());
        commandsDetailsRequest.setInitiationType(this.e.a() ? "first time" : "schedule");
        this.f = this.d.l().a();
        commandsDetailsRequest.setFirstTimeActivation(!this.f);
        return commandsDetailsRequest;
    }

    /* access modifiers changed from: protected */
    public Map<String, Object> a(BaseResponse baseResponse) {
        com.apperhand.device.a.b.b a;
        Map<String, Object> map;
        HashMap hashMap = new HashMap();
        CommandsDetailsResponse commandsDetailsResponse = (CommandsDetailsResponse) baseResponse;
        this.e.a(commandsDetailsResponse.getScheduleInfo());
        this.e.a(e.a(commandsDetailsResponse));
        a(commandsDetailsResponse.getGeneralParameters());
        this.d.i().a(commandsDetailsResponse.getIdentifiers());
        if (!this.f) {
            this.f = true;
            this.d.l().a(true);
        }
        List<BaseResponse> responses = commandsDetailsResponse.getResponses();
        if (responses == null || responses.size() == 0) {
            return hashMap;
        }
        this.h.clear();
        for (BaseResponse next : responses) {
            if (!(next == null || !next.isValidResponse() || (a = com.apperhand.device.a.b.c.a(next, this.e, this.d)) == null)) {
                this.h.add(a);
                try {
                    map = a.a(next);
                } catch (Throwable th) {
                    a.a(th);
                    map = null;
                }
                if (map != null) {
                    hashMap.putAll(map);
                }
            }
        }
        return hashMap;
    }

    public void a(BaseResponse baseResponse, Map<String, Object> map) {
        CommandStatusDetailsRequest b = b();
        List<CommandStatus> b2 = b(map);
        if (b2 != null && b2.size() != 0) {
            b.setStatuses(b2);
            a(b);
        }
    }

    /* access modifiers changed from: protected */
    public CommandStatusDetailsRequest b() {
        return super.b();
    }

    /* access modifiers changed from: protected */
    public void c() {
        this.e.a(86400, -1);
    }

    /* access modifiers changed from: protected */
    public BaseResponse d() {
        long currentTimeMillis = System.currentTimeMillis();
        this.g.a(currentTimeMillis);
        this.g.b(currentTimeMillis);
        return a(e());
    }
}
