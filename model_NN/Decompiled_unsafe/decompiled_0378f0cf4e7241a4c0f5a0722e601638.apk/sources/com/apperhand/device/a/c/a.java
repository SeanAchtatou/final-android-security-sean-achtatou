package com.apperhand.device.a.c;

import com.apperhand.common.dto.Command;
import com.apperhand.common.dto.CommandStatus;
import com.apperhand.common.dto.protocol.BaseResponse;
import com.apperhand.common.dto.protocol.CommandStatusDetailsRequest;
import com.apperhand.common.dto.protocol.CommandStatusDetailsResponse;
import com.apperhand.device.a.b;
import com.apperhand.device.a.e.c;
import com.apperhand.device.a.e.e;
import com.apperhand.device.a.e.f;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public abstract class a {
    protected final c a;
    protected final String b;
    protected final Command.Commands c;
    protected com.apperhand.device.a.a d;
    protected b e;

    public a(b bVar, com.apperhand.device.a.a aVar, String str, Command.Commands commands) {
        this.e = bVar;
        this.d = aVar;
        this.a = aVar.c();
        this.b = str;
        this.c = commands;
    }

    private void e() {
        CommandStatusDetailsRequest b2 = b();
        b2.setStatuses(a(this.c, CommandStatus.Status.FAILURE, "Got server error", null));
        a(b2);
    }

    /* access modifiers changed from: protected */
    public List<CommandStatus> a(Command.Commands commands, CommandStatus.Status status, String str, Map<String, Object> map) {
        return a(commands, new CommandStatus(), status, str, map);
    }

    /* access modifiers changed from: protected */
    public List<CommandStatus> a(Command.Commands commands, CommandStatus commandStatus, CommandStatus.Status status, String str, Map<String, Object> map) {
        ArrayList arrayList = new ArrayList(1);
        commandStatus.setCommand(commands);
        commandStatus.setId(this.b);
        commandStatus.setMessage(str);
        commandStatus.setStatus(status);
        commandStatus.setParameters(map);
        arrayList.add(commandStatus);
        return arrayList;
    }

    /* access modifiers changed from: protected */
    public abstract Map<String, Object> a(BaseResponse baseResponse);

    public void a() {
        this.a.a(c.a.DEBUG, "Entering execute()");
        BaseResponse d2 = d();
        if (d2 == null || !d2.isValidResponse()) {
            this.a.a(c.a.INFO, "Server Error in " + this.c.name());
            c();
            e();
            return;
        }
        this.e.a(e.a(d2));
        Map<String, Object> a2 = a(d2);
        if (a2 == null || a2.get("skip_status") != Boolean.TRUE) {
            a(d2, a2);
        }
    }

    public abstract void a(BaseResponse baseResponse, Map<String, Object> map);

    /* access modifiers changed from: protected */
    public void a(CommandStatusDetailsRequest commandStatusDetailsRequest) {
        try {
            CommandStatusDetailsResponse commandStatusDetailsResponse = (CommandStatusDetailsResponse) this.d.d().a(commandStatusDetailsRequest, Command.Commands.COMMANDS_STATUS, null, CommandStatusDetailsResponse.class);
            if (commandStatusDetailsResponse.isValidResponse()) {
                this.e.a(commandStatusDetailsResponse.getScheduleInfo());
                this.e.a(e.a(commandStatusDetailsResponse));
            }
        } catch (f e2) {
            this.d.c().a(c.a.DEBUG, String.format("Unable to send command status for command [%s]!!!!", this.c.getString()), e2);
        }
    }

    /* access modifiers changed from: protected */
    public CommandStatusDetailsRequest b() {
        CommandStatusDetailsRequest commandStatusDetailsRequest = new CommandStatusDetailsRequest();
        commandStatusDetailsRequest.setApplicationDetails(this.d.e());
        commandStatusDetailsRequest.setAbTestId(this.e.b());
        return commandStatusDetailsRequest;
    }

    /* access modifiers changed from: protected */
    public void c() {
    }

    /* access modifiers changed from: protected */
    public abstract BaseResponse d();
}
