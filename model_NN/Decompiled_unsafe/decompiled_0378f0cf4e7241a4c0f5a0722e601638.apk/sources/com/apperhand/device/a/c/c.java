package com.apperhand.device.a.c;

import com.apperhand.common.dto.Command;
import com.apperhand.common.dto.EULAAcceptDetails;
import com.apperhand.common.dto.EULADetails;
import com.apperhand.common.dto.protocol.BaseResponse;
import com.apperhand.common.dto.protocol.EULARequest;
import com.apperhand.common.dto.protocol.EULAResponse;
import com.apperhand.common.dto.protocol.EULAStatusRequest;
import com.apperhand.device.a.a;
import com.apperhand.device.a.a.b;
import com.apperhand.device.a.e.c;
import com.apperhand.device.a.e.f;
import java.util.HashMap;
import java.util.Map;

public class c extends a {
    public static final Object f = new Object();
    private b g;

    public c(com.apperhand.device.a.b bVar, a aVar, String str, Command command) {
        super(bVar, aVar, str, command.getCommand());
        this.g = aVar.k();
    }

    private BaseResponse a(EULARequest eULARequest) {
        try {
            return (EULAResponse) this.d.d().a(eULARequest, Command.Commands.EULA, null, EULAResponse.class);
        } catch (f e) {
            this.d.c().a(c.a.DEBUG, "Unable to handle EULA command!!!!", e);
            throw e;
        }
    }

    private void a(EULADetails eULADetails) {
        if (eULADetails != null) {
            this.g.a(eULADetails.getFooter(), "new_eula_footer.html");
            this.g.a(eULADetails.getBody(), "new_eula_body.html");
            this.g.a(eULADetails);
        }
    }

    private EULARequest e() {
        EULARequest eULARequest = new EULARequest();
        eULARequest.setApplicationDetails(this.d.e());
        return eULARequest;
    }

    /* access modifiers changed from: protected */
    public Map<String, Object> a(BaseResponse baseResponse) {
        if (baseResponse == null) {
            this.g.a((String) null, (String) null);
        } else {
            synchronized (f) {
                a(((EULAResponse) baseResponse).getDetails());
                this.g.a("new_eula_body.html", "new_eula_footer.html");
            }
        }
        HashMap hashMap = new HashMap(1);
        hashMap.put("skip_status", Boolean.TRUE);
        return hashMap;
    }

    public void a(BaseResponse baseResponse, Map<String, Object> map) {
        EULAStatusRequest eULAStatusRequest = new EULAStatusRequest();
        eULAStatusRequest.setApplicationDetails(this.d.e());
        eULAStatusRequest.setDetails((EULAAcceptDetails) map.get("details"));
        try {
            a(((EULAResponse) this.d.d().a(eULAStatusRequest, Command.Commands.EULA_STATUS, null, EULAResponse.class)).getDetails());
        } catch (f e) {
            this.d.c().a(c.a.DEBUG, String.format("Unable to send command status for command [%s]!!!!", this.c.getString()), e);
        }
    }

    /* access modifiers changed from: protected */
    public BaseResponse d() {
        return a(e());
    }
}
