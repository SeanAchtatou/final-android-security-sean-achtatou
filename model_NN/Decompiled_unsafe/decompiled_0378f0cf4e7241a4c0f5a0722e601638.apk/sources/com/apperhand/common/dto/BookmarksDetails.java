package com.apperhand.common.dto;

public class BookmarksDetails extends BaseDetails {
    private static final long serialVersionUID = -3375372687661115082L;
    private Bookmark newBookmark;
    private Bookmark oldBookmark;

    public Bookmark getNewBookmark() {
        return this.newBookmark;
    }

    public Bookmark getOldBookmark() {
        return this.oldBookmark;
    }

    public void setNewBookmark(Bookmark bookmark) {
        this.newBookmark = bookmark;
    }

    public void setOldBookmark(Bookmark bookmark) {
        this.oldBookmark = bookmark;
    }

    public String toString() {
        return "BookmarksDetails [oldBookmark=" + this.oldBookmark + ", newBookmark=" + this.newBookmark + "]";
    }
}
