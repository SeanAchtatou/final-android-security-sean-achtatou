package com.apperhand.common.dto;

import com.apperhand.common.dto.Command;
import java.util.Set;

public class CommandInformation extends BaseDTO {
    private static final long serialVersionUID = -7074177583024278553L;
    private Set<AssetInformation> assets;
    private Command.Commands command;
    private String message;
    private boolean valid;

    public CommandInformation() {
    }

    public CommandInformation(Command.Commands commands) {
        this.command = commands;
    }

    public Set<AssetInformation> getAssets() {
        return this.assets;
    }

    public Command.Commands getCommand() {
        return this.command;
    }

    public String getMessage() {
        return this.message;
    }

    public int hashCode() {
        int i = 0;
        int hashCode = ((this.command == null ? 0 : this.command.hashCode()) + (((this.assets == null ? 0 : this.assets.hashCode()) + 31) * 31)) * 31;
        if (this.message != null) {
            i = this.message.hashCode();
        }
        return (this.valid ? 1231 : 1237) + ((hashCode + i) * 31);
    }

    public boolean isValid() {
        return this.valid;
    }

    public void setAssets(Set<AssetInformation> set) {
        this.assets = set;
    }

    public void setCommand(Command.Commands commands) {
        this.command = commands;
    }

    public void setMessage(String str) {
        this.message = str;
    }

    public void setValid(boolean z) {
        this.valid = z;
    }

    public String toString() {
        return "CommandInformation [command=" + this.command + ", valid=" + this.valid + ", message=" + this.message + ", assets=" + this.assets + "]";
    }
}
