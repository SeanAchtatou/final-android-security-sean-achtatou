package com.apperhand.common.dto.protocol;

import com.apperhand.common.dto.ScheduleInfo;

public class InfoDetailsResponse extends BaseResponse {
    private static final long serialVersionUID = -6549210717995475041L;
    private ScheduleInfo scheduleInfo = null;

    public ScheduleInfo getScheduleInfo() {
        return this.scheduleInfo;
    }

    public void setScheduleInfo(ScheduleInfo scheduleInfo2) {
        this.scheduleInfo = scheduleInfo2;
    }

    public String toString() {
        return "InfoResponse [scheduleInfo=" + this.scheduleInfo + "]";
    }
}
