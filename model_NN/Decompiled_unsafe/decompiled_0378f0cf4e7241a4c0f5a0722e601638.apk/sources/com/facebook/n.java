package com.facebook;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.pm.Signature;
import android.text.TextUtils;
import com.facebook.b.g;
import java.util.ArrayList;
import java.util.List;

/* compiled from: NativeProtocol */
final class n {
    static final boolean a(Context context, String str) {
        try {
            for (Signature charsString : context.getPackageManager().getPackageInfo(str, 64).signatures) {
                if (charsString.toCharsString().equals("30820268308201d102044a9c4610300d06092a864886f70d0101040500307a310b3009060355040613025553310b3009060355040813024341311230100603550407130950616c6f20416c746f31183016060355040a130f46616365626f6f6b204d6f62696c653111300f060355040b130846616365626f6f6b311d301b0603550403131446616365626f6f6b20436f72706f726174696f6e3020170d3039303833313231353231365a180f32303530303932353231353231365a307a310b3009060355040613025553310b3009060355040813024341311230100603550407130950616c6f20416c746f31183016060355040a130f46616365626f6f6b204d6f62696c653111300f060355040b130846616365626f6f6b311d301b0603550403131446616365626f6f6b20436f72706f726174696f6e30819f300d06092a864886f70d010101050003818d0030818902818100c207d51df8eb8c97d93ba0c8c1002c928fab00dc1b42fca5e66e99cc3023ed2d214d822bc59e8e35ddcf5f44c7ae8ade50d7e0c434f500e6c131f4a2834f987fc46406115de2018ebbb0d5a3c261bd97581ccfef76afc7135a6d59e8855ecd7eacc8f8737e794c60a761c536b72b11fac8e603f5da1a2d54aa103b8a13c0dbc10203010001300d06092a864886f70d0101040500038181005ee9be8bcbb250648d3b741290a82a1c9dc2e76a0af2f2228f1d9f9c4007529c446a70175c5a900d5141812866db46be6559e2141616483998211f4a673149fb2232a10d247663b26a9031e15f84bc1c74d141ff98a02d76f85b2c8ab2571b6469b232d8e768a7f7ca04f7abe4a775615916c07940656b58717457b42bd928a2")) {
                    return true;
                }
            }
            return false;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    static Intent a(Context context, Intent intent) {
        if (intent == null) {
            return null;
        }
        ResolveInfo resolveActivity = context.getPackageManager().resolveActivity(intent, 0);
        if (resolveActivity == null) {
            return null;
        }
        if (!a(context, resolveActivity.activityInfo.packageName)) {
            return null;
        }
        return intent;
    }

    static Intent b(Context context, Intent intent) {
        if (intent == null) {
            return null;
        }
        ResolveInfo resolveService = context.getPackageManager().resolveService(intent, 0);
        if (resolveService == null) {
            return null;
        }
        if (!a(context, resolveService.serviceInfo.packageName)) {
            return null;
        }
        return intent;
    }

    static Intent a(Context context, String str, List<String> list) {
        Intent putExtra = new Intent().setClassName("com.facebook.katana", "com.facebook.katana.ProxyAuth").putExtra("client_id", str);
        if (!g.a(list)) {
            putExtra.putExtra("scope", TextUtils.join(",", list));
        }
        return a(context, putExtra);
    }

    static Intent a(Context context) {
        Intent intent = new Intent();
        intent.setClassName("com.facebook.katana", "com.facebook.katana.platform.TokenRefreshService");
        return b(context, intent);
    }

    static Intent a(Context context, String str, ArrayList<String> arrayList, String str2) {
        return a(context, new Intent().setAction("com.facebook.platform.PLATFORM_ACTIVITY").addCategory("android.intent.category.DEFAULT").putExtra("com.facebook.platform.protocol.PROTOCOL_VERSION", 20121101).putExtra("com.facebook.platform.protocol.PROTOCOL_ACTION", "com.facebook.platform.action.request.LOGIN_DIALOG").putExtra("com.facebook.platform.extra.APPLICATION_ID", str).putStringArrayListExtra("com.facebook.platform.extra.PERMISSIONS", a(arrayList)).putExtra("com.facebook.platform.extra.WRITE_PRIVACY", a(str2)));
    }

    private static String a(String str) {
        if (g.a(str)) {
            return "SELF";
        }
        return str;
    }

    /* JADX WARNING: Removed duplicated region for block: B:8:0x0022  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.util.ArrayList<java.lang.String> a(java.util.ArrayList<java.lang.String> r3) {
        /*
            boolean r0 = com.facebook.b.g.a(r3)
            if (r0 == 0) goto L_0x0012
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
        L_0x000b:
            java.lang.String r1 = "basic_info"
            r0.add(r1)
            r3 = r0
        L_0x0011:
            return r3
        L_0x0012:
            java.util.Iterator r1 = r3.iterator()
        L_0x0016:
            boolean r0 = r1.hasNext()
            if (r0 != 0) goto L_0x0022
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>(r3)
            goto L_0x000b
        L_0x0022:
            java.lang.Object r0 = r1.next()
            java.lang.String r0 = (java.lang.String) r0
            boolean r2 = com.facebook.s.a(r0)
            if (r2 != 0) goto L_0x0011
            java.lang.String r2 = "basic_info"
            boolean r0 = r2.equals(r0)
            if (r0 == 0) goto L_0x0016
            goto L_0x0011
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.n.a(java.util.ArrayList):java.util.ArrayList");
    }

    static boolean a(Intent intent) {
        int intExtra = intent.getIntExtra("com.facebook.platform.protocol.PROTOCOL_VERSION", 0);
        String stringExtra = intent.getStringExtra("com.facebook.platform.status.ERROR_TYPE");
        if (20121101 != intExtra || !"ServiceDisabled".equals(stringExtra)) {
            return false;
        }
        return true;
    }
}
