package com.facebook;

/* compiled from: FacebookServiceException */
public class j extends f {
    private final i a;

    public j(i iVar, String str) {
        super(str);
        this.a = iVar;
    }

    public final String toString() {
        return "{FacebookServiceException: " + "httpResponseCode: " + this.a.a() + ", facebookErrorCode: " + this.a.b() + ", facebookErrorType: " + this.a.c() + ", message: " + this.a.d() + "}";
    }
}
