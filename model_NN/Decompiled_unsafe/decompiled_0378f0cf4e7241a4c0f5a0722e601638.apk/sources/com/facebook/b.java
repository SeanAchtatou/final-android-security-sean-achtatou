package com.facebook;

/* compiled from: AccessTokenSource */
public enum b {
    NONE(false),
    FACEBOOK_APPLICATION_WEB(true),
    FACEBOOK_APPLICATION_NATIVE(true),
    FACEBOOK_APPLICATION_SERVICE(true),
    WEB_VIEW(false),
    TEST_USER(true);
    
    private final boolean g;

    private b(boolean z) {
        this.g = z;
    }

    /* access modifiers changed from: package-private */
    public boolean a() {
        return this.g;
    }
}
