package com.facebook;

import android.os.Bundle;
import com.facebook.b.h;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/* compiled from: TokenCachingStrategy */
public abstract class y {
    public abstract Bundle a();

    public abstract void a(Bundle bundle);

    public abstract void b();

    public static boolean b(Bundle bundle) {
        String string;
        if (bundle == null || (string = bundle.getString("com.facebook.TokenCachingStrategy.Token")) == null || string.length() == 0 || bundle.getLong("com.facebook.TokenCachingStrategy.ExpirationDate", 0) == 0) {
            return false;
        }
        return true;
    }

    public static String c(Bundle bundle) {
        h.a(bundle, "bundle");
        return bundle.getString("com.facebook.TokenCachingStrategy.Token");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.facebook.b.h.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      com.facebook.b.h.a(java.lang.String, java.lang.String):void
      com.facebook.b.h.a(java.util.Collection, java.lang.String):void
      com.facebook.b.h.a(java.lang.Object, java.lang.String):void */
    public static void a(Bundle bundle, String str) {
        h.a(bundle, "bundle");
        h.a((Object) str, "value");
        bundle.putString("com.facebook.TokenCachingStrategy.Token", str);
    }

    public static long d(Bundle bundle) {
        h.a(bundle, "bundle");
        return bundle.getLong("com.facebook.TokenCachingStrategy.ExpirationDate");
    }

    public static void a(Bundle bundle, long j) {
        h.a(bundle, "bundle");
        bundle.putLong("com.facebook.TokenCachingStrategy.ExpirationDate", j);
    }

    public static List<String> e(Bundle bundle) {
        h.a(bundle, "bundle");
        return bundle.getStringArrayList("com.facebook.TokenCachingStrategy.Permissions");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.facebook.b.h.a(java.lang.Object, java.lang.String):void
     arg types: [java.util.List<java.lang.String>, java.lang.String]
     candidates:
      com.facebook.b.h.a(java.lang.String, java.lang.String):void
      com.facebook.b.h.a(java.util.Collection, java.lang.String):void
      com.facebook.b.h.a(java.lang.Object, java.lang.String):void */
    public static void a(Bundle bundle, List<String> list) {
        ArrayList arrayList;
        h.a(bundle, "bundle");
        h.a((Object) list, "value");
        if (list instanceof ArrayList) {
            arrayList = (ArrayList) list;
        } else {
            arrayList = new ArrayList(list);
        }
        bundle.putStringArrayList("com.facebook.TokenCachingStrategy.Permissions", arrayList);
    }

    public static b f(Bundle bundle) {
        h.a(bundle, "bundle");
        if (bundle.containsKey("com.facebook.TokenCachingStrategy.AccessTokenSource")) {
            return (b) bundle.getSerializable("com.facebook.TokenCachingStrategy.AccessTokenSource");
        }
        return bundle.getBoolean("com.facebook.TokenCachingStrategy.IsSSO") ? b.FACEBOOK_APPLICATION_WEB : b.WEB_VIEW;
    }

    public static void a(Bundle bundle, b bVar) {
        h.a(bundle, "bundle");
        bundle.putSerializable("com.facebook.TokenCachingStrategy.AccessTokenSource", bVar);
    }

    public static long g(Bundle bundle) {
        h.a(bundle, "bundle");
        return bundle.getLong("com.facebook.TokenCachingStrategy.LastRefreshDate");
    }

    public static void b(Bundle bundle, long j) {
        h.a(bundle, "bundle");
        bundle.putLong("com.facebook.TokenCachingStrategy.LastRefreshDate", j);
    }

    static Date b(Bundle bundle, String str) {
        if (bundle == null) {
            return null;
        }
        long j = bundle.getLong(str, Long.MIN_VALUE);
        if (j != Long.MIN_VALUE) {
            return new Date(j);
        }
        return null;
    }

    static void a(Bundle bundle, String str, Date date) {
        bundle.putLong(str, date.getTime());
    }
}
