package com.facebook;

/* compiled from: SessionState */
public enum v {
    CREATED(a.CREATED_CATEGORY),
    CREATED_TOKEN_LOADED(a.CREATED_CATEGORY),
    OPENING(a.CREATED_CATEGORY),
    OPENED(a.OPENED_CATEGORY),
    OPENED_TOKEN_UPDATED(a.OPENED_CATEGORY),
    CLOSED_LOGIN_FAILED(a.CLOSED_CATEGORY),
    CLOSED(a.CLOSED_CATEGORY);
    
    private final a h;

    /* compiled from: SessionState */
    private enum a {
        CREATED_CATEGORY,
        OPENED_CATEGORY,
        CLOSED_CATEGORY
    }

    private v(a aVar) {
        this.h = aVar;
    }

    public boolean a() {
        return this.h == a.OPENED_CATEGORY;
    }

    public boolean b() {
        return this.h == a.CLOSED_CATEGORY;
    }
}
