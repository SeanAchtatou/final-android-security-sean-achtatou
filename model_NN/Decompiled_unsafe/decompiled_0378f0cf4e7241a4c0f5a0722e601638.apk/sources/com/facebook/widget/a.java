package com.facebook.widget;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.SslErrorHandler;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.facebook.a.e;
import com.facebook.b.g;
import com.facebook.f;
import com.facebook.h;
import com.facebook.i;
import com.facebook.j;
import com.facebook.s;

/* compiled from: WebDialog */
public class a extends Dialog {
    private String a;
    private d b;
    /* access modifiers changed from: private */
    public WebView c;
    /* access modifiers changed from: private */
    public ProgressDialog d;
    /* access modifiers changed from: private */
    public ImageView e;
    /* access modifiers changed from: private */
    public FrameLayout f;
    private boolean g = false;
    /* access modifiers changed from: private */
    public boolean h = false;

    /* compiled from: WebDialog */
    public interface d {
        void a(Bundle bundle, f fVar);
    }

    public a(Context context, String str, Bundle bundle, int i, d dVar) {
        super(context, i);
        bundle = bundle == null ? new Bundle() : bundle;
        bundle.putString("display", "touch");
        bundle.putString("type", "user_agent");
        this.a = g.a("m.facebook.com", "dialog/" + str, bundle).toString();
        this.b = dVar;
    }

    public void dismiss() {
        if (this.c != null) {
            this.c.stopLoading();
        }
        if (!this.h) {
            if (this.d.isShowing()) {
                this.d.dismiss();
            }
            super.dismiss();
        }
    }

    public void onDetachedFromWindow() {
        this.h = true;
        super.onDetachedFromWindow();
    }

    public void onAttachedToWindow() {
        this.h = false;
        super.onAttachedToWindow();
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setOnCancelListener(new DialogInterface.OnCancelListener() {
            public void onCancel(DialogInterface dialogInterface) {
                a.this.a();
            }
        });
        this.d = new ProgressDialog(getContext());
        this.d.requestWindowFeature(1);
        this.d.setMessage(getContext().getString(e.f.com_facebook_loading));
        this.d.setOnCancelListener(new DialogInterface.OnCancelListener() {
            public void onCancel(DialogInterface dialogInterface) {
                a.this.a();
                a.this.dismiss();
            }
        });
        requestWindowFeature(1);
        this.f = new FrameLayout(getContext());
        b();
        a(this.e.getDrawable().getIntrinsicWidth() / 2);
        this.f.addView(this.e, new ViewGroup.LayoutParams(-2, -2));
        addContentView(this.f, new ViewGroup.LayoutParams(-1, -1));
    }

    /* access modifiers changed from: private */
    public void a(Bundle bundle) {
        if (this.b != null && !this.g) {
            this.g = true;
            this.b.a(bundle, null);
        }
    }

    /* access modifiers changed from: private */
    public void a(Throwable th) {
        f fVar;
        if (this.b != null && !this.g) {
            this.g = true;
            if (th instanceof f) {
                fVar = (f) th;
            } else {
                fVar = new f(th);
            }
            this.b.a(null, fVar);
        }
    }

    /* access modifiers changed from: private */
    public void a() {
        a(new h());
    }

    private void b() {
        this.e = new ImageView(getContext());
        this.e.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                a.this.a();
                a.this.dismiss();
            }
        });
        this.e.setImageDrawable(getContext().getResources().getDrawable(e.c.com_facebook_close));
        this.e.setVisibility(4);
    }

    @SuppressLint({"SetJavaScriptEnabled"})
    private void a(int i) {
        LinearLayout linearLayout = new LinearLayout(getContext());
        this.c = new WebView(getContext());
        this.c.setVerticalScrollBarEnabled(false);
        this.c.setHorizontalScrollBarEnabled(false);
        this.c.setWebViewClient(new c(this, null));
        this.c.getSettings().setJavaScriptEnabled(true);
        this.c.loadUrl(this.a);
        this.c.setLayoutParams(new FrameLayout.LayoutParams(-1, -1));
        this.c.setVisibility(4);
        this.c.getSettings().setSavePassword(false);
        linearLayout.setPadding(i, i, i, i);
        linearLayout.addView(this.c);
        this.f.addView(linearLayout);
    }

    /* compiled from: WebDialog */
    private class c extends WebViewClient {
        private c() {
        }

        /* synthetic */ c(a aVar, c cVar) {
            this();
        }

        public boolean shouldOverrideUrlLoading(WebView webView, String str) {
            int i;
            g.a("FacebookSDK.WebDialog", "Redirect URL: " + str);
            if (str.startsWith("fbconnect://success")) {
                Bundle b = com.facebook.a.f.b(str);
                String string = b.getString("error");
                if (string == null) {
                    string = b.getString("error_type");
                }
                String string2 = b.getString("error_msg");
                if (string2 == null) {
                    string2 = b.getString("error_description");
                }
                String string3 = b.getString("error_code");
                if (!g.a(string3)) {
                    try {
                        i = Integer.parseInt(string3);
                    } catch (NumberFormatException e) {
                        i = -1;
                    }
                } else {
                    i = -1;
                }
                if (g.a(string) && g.a(string2) && i == -1) {
                    a.this.a(b);
                } else if (string == null || (!string.equals("access_denied") && !string.equals("OAuthAccessDeniedException"))) {
                    a.this.a(new j(new i(i, string, string2), string2));
                } else {
                    a.this.a();
                }
                a.this.dismiss();
                return true;
            } else if (str.startsWith("fbconnect://cancel")) {
                a.this.a();
                a.this.dismiss();
                return true;
            } else if (str.contains("touch")) {
                return false;
            } else {
                a.this.getContext().startActivity(new Intent("android.intent.action.VIEW", Uri.parse(str)));
                return true;
            }
        }

        public void onReceivedError(WebView webView, int i, String str, String str2) {
            super.onReceivedError(webView, i, str, str2);
            a.this.a(new com.facebook.e(str, i, str2));
            a.this.dismiss();
        }

        public void onReceivedSslError(WebView webView, SslErrorHandler sslErrorHandler, SslError sslError) {
            super.onReceivedSslError(webView, sslErrorHandler, sslError);
            a.this.a(new com.facebook.e(null, -11, null));
            sslErrorHandler.cancel();
            a.this.dismiss();
        }

        public void onPageStarted(WebView webView, String str, Bitmap bitmap) {
            g.a("FacebookSDK.WebDialog", "Webview loading URL: " + str);
            super.onPageStarted(webView, str, bitmap);
            if (!a.this.h) {
                a.this.d.show();
            }
        }

        public void onPageFinished(WebView webView, String str) {
            super.onPageFinished(webView, str);
            if (!a.this.h) {
                a.this.d.dismiss();
            }
            a.this.f.setBackgroundColor(0);
            a.this.c.setVisibility(0);
            a.this.e.setVisibility(0);
        }
    }

    /* compiled from: WebDialog */
    private static class b<CONCRETE extends b<?>> {
        private Context a;
        private s b;
        private String c;
        private String d;
        private int e = 16973840;
        private d f;
        private Bundle g;

        protected b(Context context, String str, String str2, Bundle bundle) {
            com.facebook.b.h.a(str, "applicationId");
            this.c = str;
            a(context, str2, bundle);
        }

        public CONCRETE a(d dVar) {
            this.f = dVar;
            return this;
        }

        public a a() {
            if (this.b == null || !this.b.b()) {
                this.g.putString("app_id", this.c);
            } else {
                this.g.putString("app_id", this.b.d());
                this.g.putString("access_token", this.b.e());
            }
            if (!this.g.containsKey("redirect_uri")) {
                this.g.putString("redirect_uri", "fbconnect://success");
            }
            return new a(this.a, this.d, this.g, this.e, this.f);
        }

        /* access modifiers changed from: protected */
        public String b() {
            return this.c;
        }

        /* access modifiers changed from: protected */
        public Context c() {
            return this.a;
        }

        /* access modifiers changed from: protected */
        public int d() {
            return this.e;
        }

        /* access modifiers changed from: protected */
        public Bundle e() {
            return this.g;
        }

        /* access modifiers changed from: protected */
        public d f() {
            return this.f;
        }

        private void a(Context context, String str, Bundle bundle) {
            this.a = context;
            this.d = str;
            if (bundle != null) {
                this.g = bundle;
            } else {
                this.g = new Bundle();
            }
        }
    }

    /* renamed from: com.facebook.widget.a$a  reason: collision with other inner class name */
    /* compiled from: WebDialog */
    public static class C0017a extends b<C0017a> {
        public /* bridge */ /* synthetic */ b a(d dVar) {
            return super.a(dVar);
        }

        public /* bridge */ /* synthetic */ a a() {
            return super.a();
        }

        public C0017a(Context context, String str, String str2, Bundle bundle) {
            super(context, str, str2, bundle);
        }
    }
}
