package com.facebook.widget;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.TypedArray;
import android.support.v4.app.Fragment;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import com.facebook.a.e;
import com.facebook.b.f;
import com.facebook.b.g;
import com.facebook.o;
import com.facebook.r;
import com.facebook.s;
import com.facebook.t;
import com.facebook.u;
import com.facebook.v;
import java.util.Collections;
import java.util.List;

public class LoginButton extends Button {
    /* access modifiers changed from: private */
    public static final String a = LoginButton.class.getName();
    /* access modifiers changed from: private */
    public String b = null;
    /* access modifiers changed from: private */
    public f c;
    /* access modifiers changed from: private */
    public com.facebook.c.d d = null;
    private s e = null;
    /* access modifiers changed from: private */
    public boolean f;
    private boolean g;
    private String h;
    private String i;
    /* access modifiers changed from: private */
    public e j;
    /* access modifiers changed from: private */
    public Fragment k;
    /* access modifiers changed from: private */
    public b l = new b();

    public interface d {
        void a(com.facebook.f fVar);
    }

    public interface e {
        void a(com.facebook.c.d dVar);
    }

    static class b {
        /* access modifiers changed from: private */
        public t a = t.FRIENDS;
        /* access modifiers changed from: private */
        public List<String> b = Collections.emptyList();
        /* access modifiers changed from: private */
        public com.facebook.b.e c = null;
        /* access modifiers changed from: private */
        public d d;
        /* access modifiers changed from: private */
        public u e = u.SSO_WITH_FALLBACK;
        /* access modifiers changed from: private */
        public s.f f;

        b() {
        }

        public void a(d dVar) {
            this.d = dVar;
        }

        public d a() {
            return this.d;
        }

        public void a(t tVar) {
            this.a = tVar;
        }

        public t b() {
            return this.a;
        }

        public void a(List<String> list, s sVar) {
            if (com.facebook.b.e.PUBLISH.equals(this.c)) {
                throw new UnsupportedOperationException("Cannot call setReadPermissions after setPublishPermissions has been called.");
            } else if (a(list, com.facebook.b.e.READ, sVar)) {
                this.b = list;
                this.c = com.facebook.b.e.READ;
            }
        }

        public void b(List<String> list, s sVar) {
            if (com.facebook.b.e.READ.equals(this.c)) {
                throw new UnsupportedOperationException("Cannot call setPublishPermissions after setReadPermissions has been called.");
            } else if (a(list, com.facebook.b.e.PUBLISH, sVar)) {
                this.b = list;
                this.c = com.facebook.b.e.PUBLISH;
            }
        }

        private boolean a(List<String> list, com.facebook.b.e eVar, s sVar) {
            if (com.facebook.b.e.PUBLISH.equals(eVar) && g.a(list)) {
                throw new IllegalArgumentException("Permissions for publish actions cannot be null or empty.");
            } else if (sVar == null || !sVar.b() || g.a(list, sVar.g())) {
                return true;
            } else {
                Log.e(LoginButton.a, "Cannot set additional permissions when session is already open.");
                return false;
            }
        }

        /* access modifiers changed from: package-private */
        public List<String> c() {
            return this.b;
        }

        public void a(u uVar) {
            this.e = uVar;
        }

        public u d() {
            return this.e;
        }

        public void a(s.f fVar) {
            this.f = fVar;
        }

        public s.f e() {
            return this.f;
        }
    }

    public LoginButton(Context context) {
        super(context);
        a(context);
        b();
    }

    public LoginButton(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        if (attributeSet.getStyleAttribute() == 0) {
            setTextColor(getResources().getColor(e.a.com_facebook_loginview_text_color));
            setTextSize(0, getResources().getDimension(e.b.com_facebook_loginview_text_size));
            setPadding(getResources().getDimensionPixelSize(e.b.com_facebook_loginview_padding_left), getResources().getDimensionPixelSize(e.b.com_facebook_loginview_padding_top), getResources().getDimensionPixelSize(e.b.com_facebook_loginview_padding_right), getResources().getDimensionPixelSize(e.b.com_facebook_loginview_padding_bottom));
            setWidth(getResources().getDimensionPixelSize(e.b.com_facebook_loginview_width));
            setHeight(getResources().getDimensionPixelSize(e.b.com_facebook_loginview_height));
            setGravity(17);
            a(attributeSet);
            if (isInEditMode()) {
                setBackgroundColor(getResources().getColor(e.a.com_facebook_blue));
                this.h = "Log in";
                return;
            }
            setBackgroundResource(e.c.com_facebook_loginbutton_blue);
            a(context);
        }
    }

    public LoginButton(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        a(attributeSet);
        a(context);
    }

    public void setOnErrorListener(d dVar) {
        this.l.a(dVar);
    }

    public d getOnErrorListener() {
        return this.l.a();
    }

    public void setDefaultAudience(t tVar) {
        this.l.a(tVar);
    }

    public t getDefaultAudience() {
        return this.l.b();
    }

    public void setReadPermissions(List<String> list) {
        this.l.a(list, this.c.a());
    }

    public void setPublishPermissions(List<String> list) {
        this.l.b(list, this.c.a());
    }

    public void setLoginBehavior(u uVar) {
        this.l.a(uVar);
    }

    public u getLoginBehavior() {
        return this.l.d();
    }

    public void setApplicationId(String str) {
        this.b = str;
    }

    public e getUserInfoChangedCallback() {
        return this.j;
    }

    public void setUserInfoChangedCallback(e eVar) {
        this.j = eVar;
    }

    public void setSessionStatusCallback(s.f fVar) {
        this.l.a(fVar);
    }

    public s.f getSessionStatusCallback() {
        return this.l.e();
    }

    public void setSession(s sVar) {
        this.c.a(sVar);
        d();
        c();
    }

    public void onFinishInflate() {
        super.onFinishInflate();
        b();
    }

    private void b() {
        setOnClickListener(new c(this, null));
        c();
        if (!isInEditMode()) {
            this.c = new f(getContext(), new a(this, null), null, false);
            d();
        }
    }

    public void setFragment(Fragment fragment) {
        this.k = fragment;
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (this.c != null && !this.c.e()) {
            this.c.c();
            d();
            c();
        }
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (this.c != null) {
            this.c.d();
        }
    }

    /* access modifiers changed from: package-private */
    public List<String> getPermissions() {
        return this.l.c();
    }

    /* access modifiers changed from: package-private */
    public void setProperties(b bVar) {
        this.l = bVar;
    }

    private void a(AttributeSet attributeSet) {
        TypedArray obtainStyledAttributes = getContext().obtainStyledAttributes(attributeSet, e.g.com_facebook_login_view);
        this.f = obtainStyledAttributes.getBoolean(0, true);
        this.g = obtainStyledAttributes.getBoolean(1, true);
        this.h = obtainStyledAttributes.getString(2);
        this.i = obtainStyledAttributes.getString(3);
        obtainStyledAttributes.recycle();
    }

    /* access modifiers changed from: private */
    public void c() {
        String string;
        String string2;
        if (this.c == null || this.c.b() == null) {
            if (this.h != null) {
                string = this.h;
            } else {
                string = getResources().getString(e.f.com_facebook_loginview_log_in_button);
            }
            setText(string);
            return;
        }
        if (this.i != null) {
            string2 = this.i;
        } else {
            string2 = getResources().getString(e.f.com_facebook_loginview_log_out_button);
        }
        setText(string2);
    }

    private boolean a(Context context) {
        if (context == null) {
            return false;
        }
        s j2 = s.j();
        if (j2 != null) {
            return j2.b();
        }
        if (g.a(context) == null || s.a(context) == null) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: private */
    public void d() {
        if (this.g) {
            final s b2 = this.c.b();
            if (b2 == null) {
                this.d = null;
                if (this.j != null) {
                    this.j.a(this.d);
                }
            } else if (b2 != this.e) {
                o.b(o.a(b2, new o.b() {
                    public void a(com.facebook.c.d dVar, r rVar) {
                        if (b2 == LoginButton.this.c.b()) {
                            LoginButton.this.d = dVar;
                            if (LoginButton.this.j != null) {
                                LoginButton.this.j.a(LoginButton.this.d);
                            }
                        }
                        if (rVar.a() != null) {
                            LoginButton.this.a(rVar.a().e());
                        }
                    }
                }));
                this.e = b2;
            }
        }
    }

    private class c implements View.OnClickListener {
        private c() {
        }

        /* synthetic */ c(LoginButton loginButton, c cVar) {
            this();
        }

        public void onClick(View view) {
            s.d dVar;
            String string;
            Context context = LoginButton.this.getContext();
            final s b = LoginButton.this.c.b();
            if (b == null) {
                s a2 = LoginButton.this.c.a();
                if (a2 == null || a2.c().b()) {
                    LoginButton.this.c.a((s) null);
                    a2 = new s.c(context).a(LoginButton.this.b).a();
                    s.a(a2);
                }
                if (!a2.b()) {
                    if (LoginButton.this.k != null) {
                        dVar = new s.d(LoginButton.this.k);
                    } else {
                        dVar = context instanceof Activity ? new s.d((Activity) context) : null;
                    }
                    if (dVar != null) {
                        dVar.a(LoginButton.this.l.a);
                        dVar.a(LoginButton.this.l.b);
                        dVar.a(LoginButton.this.l.e);
                        if (com.facebook.b.e.PUBLISH.equals(LoginButton.this.l.c)) {
                            a2.b(dVar);
                        } else {
                            a2.a(dVar);
                        }
                    }
                }
            } else if (LoginButton.this.f) {
                String string2 = LoginButton.this.getResources().getString(e.f.com_facebook_loginview_log_out_action);
                String string3 = LoginButton.this.getResources().getString(e.f.com_facebook_loginview_cancel_action);
                if (LoginButton.this.d == null || LoginButton.this.d.d() == null) {
                    string = LoginButton.this.getResources().getString(e.f.com_facebook_loginview_logged_in_using_facebook);
                } else {
                    string = String.format(LoginButton.this.getResources().getString(e.f.com_facebook_loginview_logged_in_as), LoginButton.this.d.d());
                }
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setMessage(string).setCancelable(true).setPositiveButton(string2, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        b.i();
                    }
                }).setNegativeButton(string3, (DialogInterface.OnClickListener) null);
                builder.create().show();
            } else {
                b.i();
            }
        }
    }

    private class a implements s.f {
        private a() {
        }

        /* synthetic */ a(LoginButton loginButton, a aVar) {
            this();
        }

        public void a(s sVar, v vVar, Exception exc) {
            LoginButton.this.d();
            LoginButton.this.c();
            if (exc != null) {
                LoginButton.this.a(exc);
            }
            if (LoginButton.this.l.f != null) {
                LoginButton.this.l.f.a(sVar, vVar, exc);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a(Exception exc) {
        if (this.l.d == null) {
            return;
        }
        if (exc instanceof com.facebook.f) {
            this.l.d.a((com.facebook.f) exc);
        } else {
            this.l.d.a(new com.facebook.f(exc));
        }
    }
}
