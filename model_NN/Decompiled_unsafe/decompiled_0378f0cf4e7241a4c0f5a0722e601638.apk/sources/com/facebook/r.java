package com.facebook;

import android.content.Context;
import com.facebook.b.b;
import com.facebook.b.g;
import com.facebook.c.b;
import com.facebook.c.c;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

/* compiled from: Response */
public class r {
    static final /* synthetic */ boolean a;
    private static b h;
    private final HttpURLConnection b;
    private final com.facebook.c.b c;
    private final c<com.facebook.c.b> d;
    private final boolean e;
    private final i f;
    private final o g;

    static {
        boolean z;
        if (!r.class.desiredAssertionStatus()) {
            z = true;
        } else {
            z = false;
        }
        a = z;
    }

    r(o oVar, HttpURLConnection httpURLConnection, com.facebook.c.b bVar, boolean z) {
        this.g = oVar;
        this.b = httpURLConnection;
        this.c = bVar;
        this.d = null;
        this.e = z;
        this.f = null;
    }

    r(o oVar, HttpURLConnection httpURLConnection, c<com.facebook.c.b> cVar, boolean z) {
        this.g = oVar;
        this.b = httpURLConnection;
        this.c = null;
        this.d = cVar;
        this.e = z;
        this.f = null;
    }

    r(o oVar, HttpURLConnection httpURLConnection, i iVar) {
        this.g = oVar;
        this.b = httpURLConnection;
        this.c = null;
        this.d = null;
        this.e = false;
        this.f = iVar;
    }

    public final i a() {
        return this.f;
    }

    public final com.facebook.c.b b() {
        return this.c;
    }

    public final <T extends com.facebook.c.b> T a(Class<T> cls) {
        if (this.c == null) {
            return null;
        }
        if (cls != null) {
            return this.c.a(cls);
        }
        throw new NullPointerException("Must pass in a valid interface that extends GraphObject");
    }

    public String toString() {
        String str;
        try {
            Object[] objArr = new Object[1];
            objArr[0] = Integer.valueOf(this.b != null ? this.b.getResponseCode() : 200);
            str = String.format("%d", objArr);
        } catch (IOException e2) {
            str = "unknown";
        }
        return "{Response: " + " responseCode: " + str + ", graphObject: " + this.c + ", error: " + this.f + ", isFromCache:" + this.e + "}";
    }

    static b c() {
        Context k;
        if (h == null && (k = s.k()) != null) {
            h = new b(k, "ResponseCache", new b.d());
        }
        return h;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.facebook.r.a(java.io.InputStream, java.net.HttpURLConnection, com.facebook.q, boolean):java.util.List<com.facebook.r>
     arg types: [java.io.InputStream, ?[OBJECT, ARRAY], com.facebook.q, int]
     candidates:
      com.facebook.r.a(java.net.HttpURLConnection, java.util.List<com.facebook.o>, java.lang.Object, boolean):java.util.List<com.facebook.r>
      com.facebook.r.a(java.io.InputStream, java.net.HttpURLConnection, com.facebook.q, boolean):java.util.List<com.facebook.r> */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.facebook.r.a(java.io.InputStream, java.net.HttpURLConnection, com.facebook.q, boolean):java.util.List<com.facebook.r>
     arg types: [java.io.InputStream, java.net.HttpURLConnection, com.facebook.q, int]
     candidates:
      com.facebook.r.a(java.net.HttpURLConnection, java.util.List<com.facebook.o>, java.lang.Object, boolean):java.util.List<com.facebook.r>
      com.facebook.r.a(java.io.InputStream, java.net.HttpURLConnection, com.facebook.q, boolean):java.util.List<com.facebook.r> */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x006c, code lost:
        com.facebook.b.g.a((java.io.Closeable) null);
        r0 = r3;
        r7 = r1;
        r1 = null;
        r2 = r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0075, code lost:
        com.facebook.b.g.a((java.io.Closeable) null);
        r0 = r3;
        r7 = r1;
        r1 = null;
        r2 = r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x007d, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x007e, code lost:
        com.facebook.b.g.a((java.io.Closeable) null);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x0081, code lost:
        throw r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x00f6, code lost:
        r0 = r2;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x005e A[Catch:{ f -> 0x009c, JSONException -> 0x00b4, IOException -> 0x00d2, all -> 0x00f0 }] */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x006b A[ExcHandler: JSONException (e org.json.JSONException), Splitter:B:12:0x0034] */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0074 A[ExcHandler: IOException (e java.io.IOException), Splitter:B:12:0x0034] */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x007d A[ExcHandler: all (r0v19 'th' java.lang.Throwable A[CUSTOM_DECLARE]), Splitter:B:12:0x0034] */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x008a A[SYNTHETIC, Splitter:B:38:0x008a] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static java.util.List<com.facebook.r> a(java.net.HttpURLConnection r8, com.facebook.q r9) {
        /*
            r2 = 0
            r6 = 1
            r5 = 0
            boolean r0 = r9 instanceof com.facebook.b.a
            if (r0 == 0) goto L_0x00ff
            r0 = r9
            com.facebook.b.a r0 = (com.facebook.b.a) r0
            com.facebook.b.b r3 = c()
            java.lang.String r1 = r0.k()
            boolean r4 = com.facebook.b.g.a(r1)
            if (r4 == 0) goto L_0x0026
            int r4 = r9.size()
            if (r4 != r6) goto L_0x0044
            com.facebook.o r1 = r9.get(r5)
            java.lang.String r1 = r1.d()
        L_0x0026:
            boolean r0 = r0.l()
            if (r0 != 0) goto L_0x00f9
            if (r3 == 0) goto L_0x00f9
            boolean r0 = com.facebook.b.g.a(r1)
            if (r0 != 0) goto L_0x00f9
            java.io.InputStream r2 = r3.a(r1)     // Catch:{ f -> 0x004e, JSONException -> 0x006b, IOException -> 0x0074, all -> 0x007d }
            if (r2 == 0) goto L_0x0082
            r0 = 0
            r4 = 1
            java.util.List r0 = a(r2, r0, r9, r4)     // Catch:{ f -> 0x00f5, JSONException -> 0x006b, IOException -> 0x0074, all -> 0x007d }
            com.facebook.b.g.a(r2)
        L_0x0043:
            return r0
        L_0x0044:
            com.facebook.m r4 = com.facebook.m.REQUESTS
            java.lang.String r5 = "ResponseCache"
            java.lang.String r6 = "Not using cache for cacheable request because no key was specified"
            com.facebook.b.c.a(r4, r5, r6)
            goto L_0x0026
        L_0x004e:
            r0 = move-exception
            r0 = r2
        L_0x0050:
            com.facebook.b.g.a(r0)
            r2 = r1
            r1 = r0
            r0 = r3
        L_0x0056:
            int r3 = r8.getResponseCode()     // Catch:{ f -> 0x009c, JSONException -> 0x00b4, IOException -> 0x00d2 }
            r4 = 400(0x190, float:5.6E-43)
            if (r3 < r4) goto L_0x008a
            java.io.InputStream r1 = r8.getErrorStream()     // Catch:{ f -> 0x009c, JSONException -> 0x00b4, IOException -> 0x00d2 }
        L_0x0062:
            r0 = 0
            java.util.List r0 = a(r1, r8, r9, r0)     // Catch:{ f -> 0x009c, JSONException -> 0x00b4, IOException -> 0x00d2 }
            com.facebook.b.g.a(r1)
            goto L_0x0043
        L_0x006b:
            r0 = move-exception
            com.facebook.b.g.a(r2)
            r0 = r3
            r7 = r1
            r1 = r2
            r2 = r7
            goto L_0x0056
        L_0x0074:
            r0 = move-exception
            com.facebook.b.g.a(r2)
            r0 = r3
            r7 = r1
            r1 = r2
            r2 = r7
            goto L_0x0056
        L_0x007d:
            r0 = move-exception
            com.facebook.b.g.a(r2)
            throw r0
        L_0x0082:
            com.facebook.b.g.a(r2)
            r0 = r3
            r7 = r1
            r1 = r2
            r2 = r7
            goto L_0x0056
        L_0x008a:
            java.io.InputStream r1 = r8.getInputStream()     // Catch:{ f -> 0x009c, JSONException -> 0x00b4, IOException -> 0x00d2 }
            if (r0 == 0) goto L_0x0062
            if (r2 == 0) goto L_0x0062
            if (r1 == 0) goto L_0x0062
            java.io.InputStream r0 = r0.a(r2, r1)     // Catch:{ f -> 0x009c, JSONException -> 0x00b4, IOException -> 0x00d2 }
            if (r0 == 0) goto L_0x0062
            r1 = r0
            goto L_0x0062
        L_0x009c:
            r0 = move-exception
            com.facebook.m r2 = com.facebook.m.REQUESTS     // Catch:{ all -> 0x00f0 }
            java.lang.String r3 = "Response"
            java.lang.String r4 = "Response <Error>: %s"
            r5 = 1
            java.lang.Object[] r5 = new java.lang.Object[r5]     // Catch:{ all -> 0x00f0 }
            r6 = 0
            r5[r6] = r0     // Catch:{ all -> 0x00f0 }
            com.facebook.b.c.a(r2, r3, r4, r5)     // Catch:{ all -> 0x00f0 }
            java.util.List r0 = a(r9, r8, r0)     // Catch:{ all -> 0x00f0 }
            com.facebook.b.g.a(r1)
            goto L_0x0043
        L_0x00b4:
            r0 = move-exception
            com.facebook.m r2 = com.facebook.m.REQUESTS     // Catch:{ all -> 0x00f0 }
            java.lang.String r3 = "Response"
            java.lang.String r4 = "Response <Error>: %s"
            r5 = 1
            java.lang.Object[] r5 = new java.lang.Object[r5]     // Catch:{ all -> 0x00f0 }
            r6 = 0
            r5[r6] = r0     // Catch:{ all -> 0x00f0 }
            com.facebook.b.c.a(r2, r3, r4, r5)     // Catch:{ all -> 0x00f0 }
            com.facebook.f r2 = new com.facebook.f     // Catch:{ all -> 0x00f0 }
            r2.<init>(r0)     // Catch:{ all -> 0x00f0 }
            java.util.List r0 = a(r9, r8, r2)     // Catch:{ all -> 0x00f0 }
            com.facebook.b.g.a(r1)
            goto L_0x0043
        L_0x00d2:
            r0 = move-exception
            com.facebook.m r2 = com.facebook.m.REQUESTS     // Catch:{ all -> 0x00f0 }
            java.lang.String r3 = "Response"
            java.lang.String r4 = "Response <Error>: %s"
            r5 = 1
            java.lang.Object[] r5 = new java.lang.Object[r5]     // Catch:{ all -> 0x00f0 }
            r6 = 0
            r5[r6] = r0     // Catch:{ all -> 0x00f0 }
            com.facebook.b.c.a(r2, r3, r4, r5)     // Catch:{ all -> 0x00f0 }
            com.facebook.f r2 = new com.facebook.f     // Catch:{ all -> 0x00f0 }
            r2.<init>(r0)     // Catch:{ all -> 0x00f0 }
            java.util.List r0 = a(r9, r8, r2)     // Catch:{ all -> 0x00f0 }
            com.facebook.b.g.a(r1)
            goto L_0x0043
        L_0x00f0:
            r0 = move-exception
            com.facebook.b.g.a(r1)
            throw r0
        L_0x00f5:
            r0 = move-exception
            r0 = r2
            goto L_0x0050
        L_0x00f9:
            r0 = r3
            r7 = r1
            r1 = r2
            r2 = r7
            goto L_0x0056
        L_0x00ff:
            r0 = r2
            r1 = r2
            goto L_0x0056
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.r.a(java.net.HttpURLConnection, com.facebook.q):java.util.List");
    }

    static List<r> a(InputStream inputStream, HttpURLConnection httpURLConnection, q qVar, boolean z) throws f, JSONException, IOException {
        String a2 = g.a(inputStream);
        com.facebook.b.c.a(m.INCLUDE_RAW_RESPONSES, "Response", "Response (raw)\n  Size: %d\n  Response:\n%s\n", Integer.valueOf(a2.length()), a2);
        List<r> a3 = a(httpURLConnection, qVar, new JSONTokener(a2).nextValue(), z);
        com.facebook.b.c.a(m.REQUESTS, "Response", "Response\n  Id: %s\n  Size: %d\n  Responses:\n%s\n", qVar.b(), Integer.valueOf(a2.length()), a3);
        return a3;
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    private static List<r> a(HttpURLConnection httpURLConnection, List<o> list, Object obj, boolean z) throws f, JSONException {
        JSONArray jSONArray;
        if (a || httpURLConnection != null || z) {
            int size = list.size();
            ArrayList arrayList = new ArrayList(size);
            if (size == 1) {
                o oVar = list.get(0);
                try {
                    JSONObject jSONObject = new JSONObject();
                    jSONObject.put("body", obj);
                    jSONObject.put("code", httpURLConnection != null ? httpURLConnection.getResponseCode() : 200);
                    JSONArray jSONArray2 = new JSONArray();
                    jSONArray2.put(jSONObject);
                    jSONArray = jSONArray2;
                } catch (JSONException e2) {
                    arrayList.add(new r(oVar, httpURLConnection, new i(httpURLConnection, e2)));
                    jSONArray = obj;
                } catch (IOException e3) {
                    arrayList.add(new r(oVar, httpURLConnection, new i(httpURLConnection, e3)));
                }
                if ((jSONArray instanceof JSONArray) || ((JSONArray) jSONArray).length() != size) {
                    throw new f("Unexpected number of results");
                }
                JSONArray jSONArray3 = (JSONArray) jSONArray;
                for (int i = 0; i < jSONArray3.length(); i++) {
                    o oVar2 = list.get(i);
                    try {
                        arrayList.add(a(oVar2, httpURLConnection, jSONArray3.get(i), z, obj));
                    } catch (JSONException e4) {
                        arrayList.add(new r(oVar2, httpURLConnection, new i(httpURLConnection, e4)));
                    } catch (f e5) {
                        arrayList.add(new r(oVar2, httpURLConnection, new i(httpURLConnection, e5)));
                    }
                }
                return arrayList;
            }
            jSONArray = obj;
            if (jSONArray instanceof JSONArray) {
            }
            throw new f("Unexpected number of results");
        }
        throw new AssertionError();
    }

    private static r a(o oVar, HttpURLConnection httpURLConnection, Object obj, boolean z, Object obj2) throws JSONException {
        s a2;
        if (obj instanceof JSONObject) {
            JSONObject jSONObject = (JSONObject) obj;
            i a3 = i.a(jSONObject, obj2, httpURLConnection);
            if (a3 != null) {
                if (a3.b() == 190 && (a2 = oVar.a()) != null) {
                    a2.i();
                }
                return new r(oVar, httpURLConnection, a3);
            }
            Object a4 = g.a(jSONObject, "body", "FACEBOOK_NON_JSON_RESULT");
            if (a4 instanceof JSONObject) {
                return new r(oVar, httpURLConnection, b.a.a((JSONObject) a4), z);
            }
            if (a4 instanceof JSONArray) {
                return new r(oVar, httpURLConnection, b.a.a((JSONArray) a4, com.facebook.c.b.class), z);
            }
            obj = JSONObject.NULL;
        }
        if (obj == JSONObject.NULL) {
            return new r(oVar, httpURLConnection, (com.facebook.c.b) null, z);
        }
        throw new f("Got unexpected object type in response, class: " + obj.getClass().getSimpleName());
    }

    static List<r> a(List<o> list, HttpURLConnection httpURLConnection, f fVar) {
        int size = list.size();
        ArrayList arrayList = new ArrayList(size);
        for (int i = 0; i < size; i++) {
            arrayList.add(new r(list.get(i), httpURLConnection, new i(httpURLConnection, fVar)));
        }
        return arrayList;
    }
}
