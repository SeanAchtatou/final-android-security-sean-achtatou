package com.facebook;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.v4.app.Fragment;
import android.util.Log;
import com.facebook.a.e;
import com.facebook.c;
import java.io.Serializable;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/* compiled from: Session */
public class s implements Serializable {
    public static final String a = s.class.getCanonicalName();
    private static final Object b = new Object();
    private static s c;
    /* access modifiers changed from: private */
    public static volatile Context d;
    private static final Set<String> e = new HashSet<String>() {
        {
            add("ads_management");
            add("create_event");
            add("rsvp_event");
        }
    };
    private static /* synthetic */ int[] s;
    private String f;
    private v g;
    private a h;
    private Date i;
    private a j;
    private c k;
    private volatile Bundle l;
    /* access modifiers changed from: private */
    public final List<f> m;
    /* access modifiers changed from: private */
    public Handler n;
    /* access modifiers changed from: private */
    public b o;
    private final Object p;
    private y q;
    /* access modifiers changed from: private */
    public volatile g r;

    /* compiled from: Session */
    interface e {
        Activity a();

        void a(Intent intent, int i);
    }

    /* compiled from: Session */
    public interface f {
        void a(s sVar, v vVar, Exception exc);
    }

    static /* synthetic */ int[] q() {
        int[] iArr = s;
        if (iArr == null) {
            iArr = new int[v.values().length];
            try {
                iArr[v.CLOSED.ordinal()] = 7;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[v.CLOSED_LOGIN_FAILED.ordinal()] = 6;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[v.CREATED.ordinal()] = 1;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[v.CREATED_TOKEN_LOADED.ordinal()] = 2;
            } catch (NoSuchFieldError e5) {
            }
            try {
                iArr[v.OPENED.ordinal()] = 4;
            } catch (NoSuchFieldError e6) {
            }
            try {
                iArr[v.OPENED_TOKEN_UPDATED.ordinal()] = 5;
            } catch (NoSuchFieldError e7) {
            }
            try {
                iArr[v.OPENING.ordinal()] = 3;
            } catch (NoSuchFieldError e8) {
            }
            s = iArr;
        }
        return iArr;
    }

    s(Context context, String str, y yVar) {
        this(context, str, yVar, true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.facebook.b.h.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      com.facebook.b.h.a(java.lang.String, java.lang.String):void
      com.facebook.b.h.a(java.util.Collection, java.lang.String):void
      com.facebook.b.h.a(java.lang.Object, java.lang.String):void */
    s(Context context, String str, y yVar, boolean z) {
        Bundle bundle = null;
        this.i = new Date(0);
        this.p = new Object();
        if (context != null && str == null) {
            str = com.facebook.b.g.a(context);
        }
        com.facebook.b.h.a((Object) str, "applicationId");
        b(context);
        yVar = yVar == null ? new x(d) : yVar;
        this.f = str;
        this.q = yVar;
        this.g = v.CREATED;
        this.j = null;
        this.m = new ArrayList();
        this.n = new Handler(Looper.getMainLooper());
        bundle = z ? yVar.a() : bundle;
        if (y.b(bundle)) {
            Date b2 = y.b(bundle, "com.facebook.TokenCachingStrategy.ExpirationDate");
            Date date = new Date();
            if (b2 == null || b2.before(date)) {
                yVar.b();
                this.h = a.a(Collections.emptyList());
                return;
            }
            this.h = a.a(bundle);
            this.g = v.CREATED_TOKEN_LOADED;
            return;
        }
        this.h = a.a(Collections.emptyList());
    }

    public final Bundle a() {
        Bundle bundle;
        synchronized (this.p) {
            bundle = this.l;
        }
        return bundle;
    }

    public final boolean b() {
        boolean a2;
        synchronized (this.p) {
            a2 = this.g.a();
        }
        return a2;
    }

    public final v c() {
        v vVar;
        synchronized (this.p) {
            vVar = this.g;
        }
        return vVar;
    }

    public final String d() {
        return this.f;
    }

    public final String e() {
        String a2;
        synchronized (this.p) {
            a2 = this.h == null ? null : this.h.a();
        }
        return a2;
    }

    public final Date f() {
        Date b2;
        synchronized (this.p) {
            b2 = this.h == null ? null : this.h.b();
        }
        return b2;
    }

    public final List<String> g() {
        List<String> c2;
        synchronized (this.p) {
            c2 = this.h == null ? null : this.h.c();
        }
        return c2;
    }

    public final void a(d dVar) {
        a(dVar, com.facebook.b.e.READ);
    }

    public final void b(d dVar) {
        a(dVar, com.facebook.b.e.PUBLISH);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.facebook.s.a(com.facebook.a, java.lang.Exception):void
     arg types: [?[OBJECT, ARRAY], com.facebook.h]
     candidates:
      com.facebook.s.a(int, com.facebook.c$j):void
      com.facebook.s.a(android.os.Handler, java.lang.Runnable):void
      com.facebook.s.a(com.facebook.s$a, com.facebook.b.e):void
      com.facebook.s.a(com.facebook.s$d, com.facebook.b.e):void
      com.facebook.s.a(com.facebook.s, com.facebook.s$b):void
      com.facebook.s.a(com.facebook.s, com.facebook.s$g):void
      com.facebook.s.a(java.lang.Object, java.lang.Object):boolean
      com.facebook.s.a(com.facebook.a, java.lang.Exception):void */
    /* JADX WARNING: Code restructure failed: missing block: B:10:0x001d, code lost:
        if (r8 == null) goto L_0x003c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x001f, code lost:
        r0 = (com.facebook.c.j) r8.getSerializableExtra("com.facebook.LoginActivity:Result");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0027, code lost:
        if (r0 == null) goto L_0x0031;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0029, code lost:
        a(r7, r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0033, code lost:
        if (r4.k == null) goto L_0x004a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0035, code lost:
        r4.k.a(r6, r7, r8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x003c, code lost:
        if (r7 != 0) goto L_0x004a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x003e, code lost:
        r0 = new com.facebook.h("User canceled operation.");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0045, code lost:
        a((com.facebook.a) null, (java.lang.Exception) r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x004a, code lost:
        r0 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:?, code lost:
        return true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:?, code lost:
        return true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:?, code lost:
        return true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x001a, code lost:
        return false;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean a(android.app.Activity r5, int r6, int r7, android.content.Intent r8) {
        /*
            r4 = this;
            r1 = 0
            r2 = 1
            java.lang.String r0 = "currentActivity"
            com.facebook.b.h.a(r5, r0)
            b(r5)
            java.lang.Object r3 = r4.p
            monitor-enter(r3)
            com.facebook.s$a r0 = r4.j     // Catch:{ all -> 0x002e }
            if (r0 == 0) goto L_0x0019
            com.facebook.s$a r0 = r4.j     // Catch:{ all -> 0x002e }
            int r0 = r0.c()     // Catch:{ all -> 0x002e }
            if (r6 == r0) goto L_0x001c
        L_0x0019:
            monitor-exit(r3)     // Catch:{ all -> 0x002e }
            r0 = 0
        L_0x001b:
            return r0
        L_0x001c:
            monitor-exit(r3)     // Catch:{ all -> 0x002e }
            if (r8 == 0) goto L_0x003c
            java.lang.String r0 = "com.facebook.LoginActivity:Result"
            java.io.Serializable r0 = r8.getSerializableExtra(r0)
            com.facebook.c$j r0 = (com.facebook.c.j) r0
            if (r0 == 0) goto L_0x0031
            r4.a(r7, r0)
            r0 = r2
            goto L_0x001b
        L_0x002e:
            r0 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x002e }
            throw r0
        L_0x0031:
            com.facebook.c r0 = r4.k
            if (r0 == 0) goto L_0x004a
            com.facebook.c r0 = r4.k
            r0.a(r6, r7, r8)
            r0 = r2
            goto L_0x001b
        L_0x003c:
            if (r7 != 0) goto L_0x004a
            com.facebook.h r0 = new com.facebook.h
            java.lang.String r3 = "User canceled operation."
            r0.<init>(r3)
        L_0x0045:
            r4.a(r1, r0)
            r0 = r2
            goto L_0x001b
        L_0x004a:
            r0 = r1
            goto L_0x0045
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.s.a(android.app.Activity, int, int, android.content.Intent):boolean");
    }

    public final void h() {
        synchronized (this.p) {
            v vVar = this.g;
            switch (q()[this.g.ordinal()]) {
                case 1:
                case 3:
                    this.g = v.CLOSED_LOGIN_FAILED;
                    a(vVar, this.g, new f("Log in attempt aborted."));
                    break;
                case 2:
                case e.g.com_facebook_picker_fragment_done_button_text:
                case e.g.com_facebook_picker_fragment_title_bar_background:
                    this.g = v.CLOSED;
                    a(vVar, this.g, (Exception) null);
                    break;
            }
        }
    }

    public final void i() {
        if (this.q != null) {
            this.q.b();
        }
        com.facebook.b.g.b(d);
        h();
    }

    public final void a(f fVar) {
        synchronized (this.m) {
            if (fVar != null) {
                if (!this.m.contains(fVar)) {
                    this.m.add(fVar);
                }
            }
        }
    }

    public final void b(f fVar) {
        synchronized (this.m) {
            this.m.remove(fVar);
        }
    }

    public String toString() {
        return "{Session" + " state:" + this.g + ", token:" + (this.h == null ? "null" : this.h) + ", appId:" + (this.f == null ? "null" : this.f) + "}";
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:17:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(android.os.Bundle r5) {
        /*
            r4 = this;
            java.lang.Object r1 = r4.p
            monitor-enter(r1)
            com.facebook.v r0 = r4.g     // Catch:{ all -> 0x004f }
            int[] r2 = q()     // Catch:{ all -> 0x004f }
            com.facebook.v r3 = r4.g     // Catch:{ all -> 0x004f }
            int r3 = r3.ordinal()     // Catch:{ all -> 0x004f }
            r2 = r2[r3]     // Catch:{ all -> 0x004f }
            switch(r2) {
                case 4: goto L_0x002c;
                case 5: goto L_0x0036;
                default: goto L_0x0014;
            }     // Catch:{ all -> 0x004f }
        L_0x0014:
            java.lang.String r0 = com.facebook.s.a     // Catch:{ all -> 0x004f }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x004f }
            java.lang.String r3 = "refreshToken ignored in state "
            r2.<init>(r3)     // Catch:{ all -> 0x004f }
            com.facebook.v r3 = r4.g     // Catch:{ all -> 0x004f }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x004f }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x004f }
            android.util.Log.d(r0, r2)     // Catch:{ all -> 0x004f }
            monitor-exit(r1)     // Catch:{ all -> 0x004f }
        L_0x002b:
            return
        L_0x002c:
            com.facebook.v r2 = com.facebook.v.OPENED_TOKEN_UPDATED     // Catch:{ all -> 0x004f }
            r4.g = r2     // Catch:{ all -> 0x004f }
            com.facebook.v r2 = r4.g     // Catch:{ all -> 0x004f }
            r3 = 0
            r4.a(r0, r2, r3)     // Catch:{ all -> 0x004f }
        L_0x0036:
            com.facebook.a r0 = r4.h     // Catch:{ all -> 0x004f }
            com.facebook.a r0 = com.facebook.a.a(r0, r5)     // Catch:{ all -> 0x004f }
            r4.h = r0     // Catch:{ all -> 0x004f }
            com.facebook.y r0 = r4.q     // Catch:{ all -> 0x004f }
            if (r0 == 0) goto L_0x004d
            com.facebook.y r0 = r4.q     // Catch:{ all -> 0x004f }
            com.facebook.a r2 = r4.h     // Catch:{ all -> 0x004f }
            android.os.Bundle r2 = r2.f()     // Catch:{ all -> 0x004f }
            r0.a(r2)     // Catch:{ all -> 0x004f }
        L_0x004d:
            monitor-exit(r1)     // Catch:{ all -> 0x004f }
            goto L_0x002b
        L_0x004f:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x004f }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.s.a(android.os.Bundle):void");
    }

    public static final s j() {
        s sVar;
        synchronized (b) {
            sVar = c;
        }
        return sVar;
    }

    public static final void a(s sVar) {
        synchronized (b) {
            if (sVar != c) {
                s sVar2 = c;
                if (sVar2 != null) {
                    sVar2.h();
                }
                c = sVar;
                if (sVar2 != null) {
                    b("com.facebook.sdk.ACTIVE_SESSION_UNSET");
                }
                if (sVar != null) {
                    b("com.facebook.sdk.ACTIVE_SESSION_SET");
                    if (sVar.b()) {
                        b("com.facebook.sdk.ACTIVE_SESSION_OPENED");
                    }
                }
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.facebook.s.a(android.content.Context, boolean, com.facebook.s$d):com.facebook.s
     arg types: [android.content.Context, int, ?[OBJECT, ARRAY]]
     candidates:
      com.facebook.s.a(com.facebook.s, int, com.facebook.c$j):void
      com.facebook.s.a(com.facebook.v, com.facebook.v, java.lang.Exception):void
      com.facebook.s.a(android.content.Context, boolean, com.facebook.s$d):com.facebook.s */
    public static s a(Context context) {
        return a(context, false, (d) null);
    }

    private static s a(Context context, boolean z, d dVar) {
        s a2 = new c(context).a();
        if (!v.CREATED_TOKEN_LOADED.equals(a2.c()) && !z) {
            return null;
        }
        a(a2);
        a2.a(dVar);
        return a2;
    }

    static Context k() {
        return d;
    }

    static void b(Context context) {
        if (context != null && d == null) {
            Context applicationContext = context.getApplicationContext();
            if (applicationContext != null) {
                context = applicationContext;
            }
            d = context;
        }
    }

    /* access modifiers changed from: package-private */
    public void a(a aVar) {
        aVar.a(this.f);
        r();
        boolean c2 = c(aVar);
        if (!c2 && aVar.e) {
            c2 = e(aVar);
        }
        if (!c2) {
            synchronized (this.p) {
                v vVar = this.g;
                switch (q()[this.g.ordinal()]) {
                    case e.g.com_facebook_picker_fragment_done_button_background:
                    case 7:
                        return;
                    default:
                        this.g = v.CLOSED_LOGIN_FAILED;
                        a(vVar, this.g, new f("Log in attempt failed."));
                        return;
                }
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.facebook.s.a(com.facebook.s$a, com.facebook.b.e):void
     arg types: [com.facebook.s$d, com.facebook.b.e]
     candidates:
      com.facebook.s.a(int, com.facebook.c$j):void
      com.facebook.s.a(android.os.Handler, java.lang.Runnable):void
      com.facebook.s.a(com.facebook.s$d, com.facebook.b.e):void
      com.facebook.s.a(com.facebook.s, com.facebook.s$b):void
      com.facebook.s.a(com.facebook.s, com.facebook.s$g):void
      com.facebook.s.a(java.lang.Object, java.lang.Object):boolean
      com.facebook.s.a(com.facebook.a, java.lang.Exception):void
      com.facebook.s.a(com.facebook.s$a, com.facebook.b.e):void */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0059, code lost:
        if (r0 != com.facebook.v.c) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x005b, code lost:
        a((com.facebook.s.a) r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(com.facebook.s.d r6, com.facebook.b.e r7) {
        /*
            r5 = this;
            r5.a(r6, r7)
            r5.b(r6)
            java.lang.Object r1 = r5.p
            monitor-enter(r1)
            com.facebook.s$a r0 = r5.j     // Catch:{ all -> 0x0036 }
            if (r0 == 0) goto L_0x001d
            com.facebook.v r0 = r5.g     // Catch:{ all -> 0x0036 }
            com.facebook.v r2 = r5.g     // Catch:{ all -> 0x0036 }
            java.lang.UnsupportedOperationException r3 = new java.lang.UnsupportedOperationException     // Catch:{ all -> 0x0036 }
            java.lang.String r4 = "Session: an attempt was made to open a session that has a pending request."
            r3.<init>(r4)     // Catch:{ all -> 0x0036 }
            r5.a(r0, r2, r3)     // Catch:{ all -> 0x0036 }
            monitor-exit(r1)     // Catch:{ all -> 0x0036 }
        L_0x001c:
            return
        L_0x001d:
            com.facebook.v r2 = r5.g     // Catch:{ all -> 0x0036 }
            int[] r0 = q()     // Catch:{ all -> 0x0036 }
            com.facebook.v r3 = r5.g     // Catch:{ all -> 0x0036 }
            int r3 = r3.ordinal()     // Catch:{ all -> 0x0036 }
            r0 = r0[r3]     // Catch:{ all -> 0x0036 }
            switch(r0) {
                case 1: goto L_0x0039;
                case 2: goto L_0x005f;
                default: goto L_0x002e;
            }     // Catch:{ all -> 0x0036 }
        L_0x002e:
            java.lang.UnsupportedOperationException r0 = new java.lang.UnsupportedOperationException     // Catch:{ all -> 0x0036 }
            java.lang.String r2 = "Session: an attempt was made to open an already opened session."
            r0.<init>(r2)     // Catch:{ all -> 0x0036 }
            throw r0     // Catch:{ all -> 0x0036 }
        L_0x0036:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0036 }
            throw r0
        L_0x0039:
            com.facebook.v r0 = com.facebook.v.OPENING     // Catch:{ all -> 0x0036 }
            r5.g = r0     // Catch:{ all -> 0x0036 }
            if (r6 != 0) goto L_0x0047
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException     // Catch:{ all -> 0x0036 }
            java.lang.String r2 = "openRequest cannot be null when opening a new Session"
            r0.<init>(r2)     // Catch:{ all -> 0x0036 }
            throw r0     // Catch:{ all -> 0x0036 }
        L_0x0047:
            r5.j = r6     // Catch:{ all -> 0x0036 }
        L_0x0049:
            if (r6 == 0) goto L_0x0052
            com.facebook.s$f r3 = r6.a()     // Catch:{ all -> 0x0036 }
            r5.a(r3)     // Catch:{ all -> 0x0036 }
        L_0x0052:
            r3 = 0
            r5.a(r2, r0, r3)     // Catch:{ all -> 0x0036 }
            monitor-exit(r1)     // Catch:{ all -> 0x0036 }
            com.facebook.v r1 = com.facebook.v.OPENING
            if (r0 != r1) goto L_0x001c
            r5.a(r6)
            goto L_0x001c
        L_0x005f:
            if (r6 == 0) goto L_0x007b
            java.util.List r0 = r6.d()     // Catch:{ all -> 0x0036 }
            boolean r0 = com.facebook.b.g.a(r0)     // Catch:{ all -> 0x0036 }
            if (r0 != 0) goto L_0x007b
            java.util.List r0 = r6.d()     // Catch:{ all -> 0x0036 }
            java.util.List r3 = r5.g()     // Catch:{ all -> 0x0036 }
            boolean r0 = com.facebook.b.g.a(r0, r3)     // Catch:{ all -> 0x0036 }
            if (r0 != 0) goto L_0x007b
            r5.j = r6     // Catch:{ all -> 0x0036 }
        L_0x007b:
            com.facebook.s$a r0 = r5.j     // Catch:{ all -> 0x0036 }
            if (r0 != 0) goto L_0x0084
            com.facebook.v r0 = com.facebook.v.OPENED     // Catch:{ all -> 0x0036 }
            r5.g = r0     // Catch:{ all -> 0x0036 }
            goto L_0x0049
        L_0x0084:
            com.facebook.v r0 = com.facebook.v.OPENING     // Catch:{ all -> 0x0036 }
            r5.g = r0     // Catch:{ all -> 0x0036 }
            goto L_0x0049
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.s.a(com.facebook.s$d, com.facebook.b.e):void");
    }

    private void b(a aVar) {
        if (aVar != null && !aVar.e) {
            Intent intent = new Intent();
            intent.setClass(k(), LoginActivity.class);
            if (!a(intent)) {
                throw new f(String.format("Cannot use SessionLoginBehavior %s when %s is not declared as an activity in AndroidManifest.xml", aVar.b(), LoginActivity.class.getName()));
            }
        }
    }

    private void a(a aVar, com.facebook.b.e eVar) {
        if (aVar != null && !com.facebook.b.g.a(aVar.d())) {
            for (String next : aVar.d()) {
                if (a(next)) {
                    if (com.facebook.b.e.READ.equals(eVar)) {
                        throw new f(String.format("Cannot pass a publish or manage permission (%s) to a request for read authorization", next));
                    }
                } else if (com.facebook.b.e.PUBLISH.equals(eVar)) {
                    Log.w(a, String.format("Should not pass a read permission (%s) to a request for publish or manage authorization", next));
                }
            }
        } else if (com.facebook.b.e.PUBLISH.equals(eVar)) {
            throw new f("Cannot request publish or manage authorization with no permissions.");
        }
    }

    static boolean a(String str) {
        return str != null && (str.startsWith("publish") || str.startsWith("manage") || e.contains(str));
    }

    /* access modifiers changed from: private */
    public void a(int i2, c.j jVar) {
        Exception exc;
        a aVar;
        if (i2 == -1) {
            if (jVar.a == c.j.a.SUCCESS) {
                aVar = jVar.b;
                exc = null;
            } else {
                exc = new d(jVar.c);
                aVar = null;
            }
        } else if (i2 == 0) {
            exc = new h(jVar.c);
            aVar = null;
        } else {
            exc = null;
            aVar = null;
        }
        this.k = null;
        a(aVar, exc);
    }

    private boolean c(a aVar) {
        Intent d2 = d(aVar);
        if (!a(d2)) {
            return false;
        }
        try {
            aVar.e().a(d2, aVar.c());
            return true;
        } catch (ActivityNotFoundException e2) {
            return false;
        }
    }

    private boolean a(Intent intent) {
        if (k().getPackageManager().resolveActivity(intent, 0) == null) {
            return false;
        }
        return true;
    }

    private Intent d(a aVar) {
        Intent intent = new Intent();
        intent.setClass(k(), LoginActivity.class);
        intent.setAction(aVar.b().toString());
        intent.putExtras(LoginActivity.a(aVar.f()));
        return intent;
    }

    private boolean e(a aVar) {
        this.k = new c();
        this.k.a(new c.i() {
            public void a(c.j jVar) {
                s.this.a(-1, jVar);
            }
        });
        this.k.a(k());
        this.k.a(aVar.f());
        return true;
    }

    /* access modifiers changed from: package-private */
    public void a(a aVar, Exception exc) {
        if (aVar != null && aVar.g()) {
            aVar = null;
            exc = new f("Invalid access token.");
        }
        synchronized (this.p) {
            switch (q()[this.g.ordinal()]) {
                case 3:
                    b(aVar, exc);
                    break;
                case e.g.com_facebook_picker_fragment_done_button_text:
                case e.g.com_facebook_picker_fragment_title_bar_background:
                    c(aVar, exc);
                    break;
            }
        }
    }

    private void b(a aVar, Exception exc) {
        v vVar = this.g;
        if (aVar != null) {
            this.h = aVar;
            a(aVar);
            this.g = v.OPENED;
        } else if (exc != null) {
            this.g = v.CLOSED_LOGIN_FAILED;
        }
        this.j = null;
        a(vVar, this.g, exc);
    }

    private void c(a aVar, Exception exc) {
        v vVar = this.g;
        if (aVar != null) {
            this.h = aVar;
            a(aVar);
            this.g = v.OPENED_TOKEN_UPDATED;
        }
        this.j = null;
        a(vVar, this.g, exc);
    }

    private void a(a aVar) {
        if (aVar != null && this.q != null) {
            this.q.a(aVar.f());
        }
    }

    /* access modifiers changed from: package-private */
    public void a(v vVar, final v vVar2, final Exception exc) {
        if (vVar != vVar2 || exc != null) {
            if (vVar2.b()) {
                this.h = a.a(Collections.emptyList());
            }
            synchronized (this.m) {
                b(this.n, new Runnable() {
                    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                     method: com.facebook.s.a(android.os.Handler, java.lang.Runnable):void
                     arg types: [android.os.Handler, com.facebook.s$3$1]
                     candidates:
                      com.facebook.s.a(int, com.facebook.c$j):void
                      com.facebook.s.a(com.facebook.s$a, com.facebook.b.e):void
                      com.facebook.s.a(com.facebook.s$d, com.facebook.b.e):void
                      com.facebook.s.a(com.facebook.s, com.facebook.s$b):void
                      com.facebook.s.a(com.facebook.s, com.facebook.s$g):void
                      com.facebook.s.a(java.lang.Object, java.lang.Object):boolean
                      com.facebook.s.a(com.facebook.a, java.lang.Exception):void
                      com.facebook.s.a(android.os.Handler, java.lang.Runnable):void */
                    public void run() {
                        for (final f fVar : s.this.m) {
                            final v vVar = vVar2;
                            final Exception exc = exc;
                            s.b(s.this.n, (Runnable) new Runnable() {
                                public void run() {
                                    fVar.a(s.this, vVar, exc);
                                }
                            });
                        }
                    }
                });
            }
            if (this == c && vVar.a() != vVar2.a()) {
                if (vVar2.a()) {
                    b("com.facebook.sdk.ACTIVE_SESSION_OPENED");
                } else {
                    b("com.facebook.sdk.ACTIVE_SESSION_CLOSED");
                }
            }
        }
    }

    static void b(String str) {
        android.support.v4.a.b.a(k()).a(new Intent(str));
    }

    /* access modifiers changed from: private */
    public static void b(Handler handler, Runnable runnable) {
        if (handler != null) {
            handler.post(runnable);
        } else {
            w.a().execute(runnable);
        }
    }

    /* access modifiers changed from: package-private */
    public void l() {
        if (n()) {
            m();
        }
    }

    /* access modifiers changed from: package-private */
    public void m() {
        g gVar = null;
        synchronized (this.p) {
            if (this.r == null) {
                gVar = new g();
                this.r = gVar;
            }
        }
        if (gVar != null) {
            gVar.a();
        }
    }

    /* access modifiers changed from: package-private */
    public boolean n() {
        if (this.r != null) {
            return false;
        }
        Date date = new Date();
        if (!this.g.a() || !this.h.d().a() || date.getTime() - this.i.getTime() <= 3600000 || date.getTime() - this.h.e().getTime() <= 86400000) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    public a o() {
        return this.h;
    }

    /* access modifiers changed from: package-private */
    public void a(Date date) {
        this.i = date;
    }

    /* compiled from: Session */
    class g implements ServiceConnection {
        final Messenger a;
        Messenger b = null;

        g() {
            this.a = new Messenger(new h(s.this, this));
        }

        public void a() {
            Intent a2 = n.a(s.k());
            if (a2 == null || !s.d.bindService(a2, new g(), 1)) {
                b();
            } else {
                s.this.a(new Date());
            }
        }

        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            this.b = new Messenger(iBinder);
            c();
        }

        public void onServiceDisconnected(ComponentName componentName) {
            b();
            s.d.unbindService(this);
        }

        /* access modifiers changed from: private */
        public void b() {
            if (s.this.r == this) {
                s.this.r = (g) null;
            }
        }

        private void c() {
            Bundle bundle = new Bundle();
            bundle.putString("access_token", s.this.o().a());
            Message obtain = Message.obtain();
            obtain.setData(bundle);
            obtain.replyTo = this.a;
            try {
                this.b.send(obtain);
            } catch (RemoteException e) {
                b();
            }
        }
    }

    /* compiled from: Session */
    static class h extends Handler {
        private WeakReference<s> a;
        private WeakReference<g> b;

        h(s sVar, g gVar) {
            super(Looper.getMainLooper());
            this.a = new WeakReference<>(sVar);
            this.b = new WeakReference<>(gVar);
        }

        public void handleMessage(Message message) {
            String string = message.getData().getString("access_token");
            s sVar = this.a.get();
            if (!(sVar == null || string == null)) {
                sVar.a(message.getData());
            }
            g gVar = this.b.get();
            if (gVar != null) {
                s.d.unbindService(gVar);
                gVar.b();
            }
        }
    }

    public int hashCode() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof s)) {
            return false;
        }
        s sVar = (s) obj;
        if (!a(sVar.f, this.f) || !a(sVar.l, this.l) || !a(sVar.g, this.g) || !a(sVar.f(), f())) {
            return false;
        }
        return true;
    }

    private static boolean a(Object obj, Object obj2) {
        if (obj == null) {
            return obj2 == null;
        }
        return obj.equals(obj2);
    }

    /* compiled from: Session */
    public static final class c {
        private final Context a;
        private String b;
        private y c;

        public c(Context context) {
            this.a = context;
        }

        public c a(String str) {
            this.b = str;
            return this;
        }

        public c a(y yVar) {
            this.c = yVar;
            return this;
        }

        public s a() {
            return new s(this.a, this.b, this.c);
        }
    }

    private void r() {
        String str;
        b bVar = null;
        synchronized (this) {
            if (this.o == null && w.b() && (str = this.f) != null) {
                bVar = new b(str, d);
                this.o = bVar;
            }
        }
        if (bVar != null) {
            bVar.execute(new Void[0]);
        }
    }

    /* compiled from: Session */
    private class b extends AsyncTask<Void, Void, Void> {
        private final String b;
        private final Context c;

        public b(String str, Context context) {
            this.b = str;
            this.c = context.getApplicationContext();
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public Void doInBackground(Void... voidArr) {
            try {
                w.a(this.c, this.b);
                return null;
            } catch (Exception e) {
                com.facebook.b.g.a("Facebook-publish", e.getMessage());
                return null;
            }
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void onPostExecute(Void voidR) {
            synchronized (s.this) {
                s.this.o = (b) null;
            }
        }
    }

    /* compiled from: Session */
    public static class a implements Serializable {
        /* access modifiers changed from: private */
        public final e a;
        private u b = u.SSO_WITH_FALLBACK;
        private int c = 64206;
        private f d;
        /* access modifiers changed from: private */
        public boolean e = false;
        private List<String> f = Collections.emptyList();
        private t g = t.FRIENDS;
        private String h;
        private String i;

        a(final Activity activity) {
            this.a = new e() {
                public void a(Intent intent, int i) {
                    activity.startActivityForResult(intent, i);
                }

                public Activity a() {
                    return activity;
                }
            };
        }

        a(final Fragment fragment) {
            this.a = new e() {
                public void a(Intent intent, int i) {
                    fragment.a(intent, i);
                }

                public Activity a() {
                    return fragment.b();
                }
            };
        }

        public void a(boolean z) {
            this.e = z;
        }

        /* access modifiers changed from: package-private */
        public a a(f fVar) {
            this.d = fVar;
            return this;
        }

        /* access modifiers changed from: package-private */
        public f a() {
            return this.d;
        }

        /* access modifiers changed from: package-private */
        public a a(u uVar) {
            if (uVar != null) {
                this.b = uVar;
            }
            return this;
        }

        /* access modifiers changed from: package-private */
        public u b() {
            return this.b;
        }

        /* access modifiers changed from: package-private */
        public a a(int i2) {
            if (i2 >= 0) {
                this.c = i2;
            }
            return this;
        }

        /* access modifiers changed from: package-private */
        public int c() {
            return this.c;
        }

        /* access modifiers changed from: package-private */
        public a a(List<String> list) {
            if (list != null) {
                this.f = list;
            }
            return this;
        }

        /* access modifiers changed from: package-private */
        public List<String> d() {
            return this.f;
        }

        /* access modifiers changed from: package-private */
        public a a(t tVar) {
            if (tVar != null) {
                this.g = tVar;
            }
            return this;
        }

        /* access modifiers changed from: package-private */
        public e e() {
            return this.a;
        }

        /* access modifiers changed from: package-private */
        public void a(String str) {
            this.h = str;
        }

        /* access modifiers changed from: package-private */
        public c.C0016c f() {
            return new c.C0016c(this.b, this.c, this.e, this.f, this.g, this.h, this.i, new c.k() {
                public void a(Intent intent, int i) {
                    a.this.a.a(intent, i);
                }

                public Activity a() {
                    return a.this.a.a();
                }
            });
        }
    }

    /* compiled from: Session */
    public static final class d extends a {
        public d(Activity activity) {
            super(activity);
        }

        public d(Fragment fragment) {
            super(fragment);
        }

        /* renamed from: b */
        public final d a(f fVar) {
            super.a(fVar);
            return this;
        }

        /* renamed from: b */
        public final d a(u uVar) {
            super.a(uVar);
            return this;
        }

        /* renamed from: b */
        public final d a(int i) {
            super.a(i);
            return this;
        }

        /* renamed from: b */
        public final d a(List<String> list) {
            super.a(list);
            return this;
        }

        /* renamed from: b */
        public final d a(t tVar) {
            super.a(tVar);
            return this;
        }
    }
}
