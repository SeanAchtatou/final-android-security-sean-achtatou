package com.facebook;

import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.ParcelFileDescriptor;
import android.text.TextUtils;
import android.util.Pair;
import com.facebook.b.g;
import com.facebook.b.h;
import com.facebook.q;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: Request */
public class o {
    private static String a;
    private static volatile String m;
    private s b;
    private l c;
    private String d;
    private com.facebook.c.b e;
    private String f;
    private String g;
    private String h;
    private boolean i;
    private Bundle j;
    private a k;
    private String l;

    /* compiled from: Request */
    public interface a {
        void a(r rVar);
    }

    /* compiled from: Request */
    public interface b {
        void a(com.facebook.c.d dVar, r rVar);
    }

    /* compiled from: Request */
    private interface c {
        void a(String str, String str2) throws IOException;
    }

    public o() {
        this(null, null, null, null, null);
    }

    public o(s sVar, String str, Bundle bundle, l lVar, a aVar) {
        this.i = true;
        this.b = sVar;
        this.d = str;
        this.k = aVar;
        a(lVar);
        if (bundle != null) {
            this.j = new Bundle(bundle);
        } else {
            this.j = new Bundle();
        }
        if (!this.j.containsKey("migration_bundle")) {
            this.j.putString("migration_bundle", "fbsdk:20121026");
        }
    }

    public static o a(s sVar, String str, com.facebook.c.b bVar, a aVar) {
        o oVar = new o(sVar, str, null, l.POST, aVar);
        oVar.a(bVar);
        return oVar;
    }

    public static o a(s sVar, final b bVar) {
        return new o(sVar, "me", null, null, new a() {
            public void a(r rVar) {
                if (b.this != null) {
                    b.this.a((com.facebook.c.d) rVar.a(com.facebook.c.d.class), rVar);
                }
            }
        });
    }

    public static o a(s sVar, String str, a aVar) {
        return new o(sVar, str, null, null, aVar);
    }

    public final void a(com.facebook.c.b bVar) {
        this.e = bVar;
    }

    public final void a(l lVar) {
        if (this.l == null || lVar == l.GET) {
            if (lVar == null) {
                lVar = l.GET;
            }
            this.c = lVar;
            return;
        }
        throw new f("Can't change HTTP method on request with overridden URL.");
    }

    public final void a(Bundle bundle) {
        this.j = bundle;
    }

    public final s a() {
        return this.b;
    }

    public final void a(a aVar) {
        this.k = aVar;
    }

    public final r b() {
        return a(this);
    }

    public static HttpURLConnection a(q qVar) {
        URL url;
        Iterator it = qVar.iterator();
        while (it.hasNext()) {
            ((o) it.next()).f();
        }
        try {
            if (qVar.size() == 1) {
                url = new URL(qVar.get(0).d());
            } else {
                url = new URL("https://graph.facebook.com");
            }
            try {
                HttpURLConnection a2 = a(url);
                a(qVar, a2);
                return a2;
            } catch (IOException e2) {
                throw new f("could not construct request body", e2);
            } catch (JSONException e3) {
                throw new f("could not construct request body", e3);
            }
        } catch (MalformedURLException e4) {
            throw new f("could not construct URL for request", e4);
        }
    }

    public static r a(o oVar) {
        List<r> a2 = a(oVar);
        if (a2 != null && a2.size() == 1) {
            return a2.get(0);
        }
        throw new f("invalid state: expected a single response");
    }

    public static List<r> a(o... oVarArr) {
        h.a(oVarArr, "requests");
        return a((Collection<o>) Arrays.asList(oVarArr));
    }

    public static List<r> a(Collection<o> collection) {
        return b(new q(collection));
    }

    public static List<r> b(q qVar) {
        h.c(qVar, "requests");
        try {
            return a(a(qVar), qVar);
        } catch (Exception e2) {
            List<r> a2 = r.a(qVar.d(), null, new f(e2));
            a(qVar, a2);
            return a2;
        }
    }

    public static p b(o... oVarArr) {
        h.a(oVarArr, "requests");
        return b((Collection<o>) Arrays.asList(oVarArr));
    }

    public static p b(Collection<o> collection) {
        return c(new q(collection));
    }

    public static p c(q qVar) {
        h.c(qVar, "requests");
        p pVar = new p(qVar);
        pVar.a();
        return pVar;
    }

    public static List<r> a(HttpURLConnection httpURLConnection, q qVar) {
        List<r> a2 = r.a(httpURLConnection, qVar);
        g.a(httpURLConnection);
        int size = qVar.size();
        if (size != a2.size()) {
            throw new f(String.format("Received %d responses while expecting %d", Integer.valueOf(a2.size()), Integer.valueOf(size)));
        }
        a(qVar, a2);
        HashSet hashSet = new HashSet();
        Iterator it = qVar.iterator();
        while (it.hasNext()) {
            o oVar = (o) it.next();
            if (oVar.b != null) {
                hashSet.add(oVar.b);
            }
        }
        Iterator it2 = hashSet.iterator();
        while (it2.hasNext()) {
            ((s) it2.next()).l();
        }
        return a2;
    }

    public String toString() {
        return "{Request: " + " session: " + this.b + ", graphPath: " + this.d + ", graphObject: " + this.e + ", restMethod: " + this.f + ", httpMethod: " + this.c + ", parameters: " + this.j + "}";
    }

    static void a(final q qVar, List<r> list) {
        int size = qVar.size();
        final ArrayList arrayList = new ArrayList();
        for (int i2 = 0; i2 < size; i2++) {
            o a2 = qVar.get(i2);
            if (a2.k != null) {
                arrayList.add(new Pair(a2.k, list.get(i2)));
            }
        }
        if (arrayList.size() > 0) {
            AnonymousClass2 r0 = new Runnable() {
                public void run() {
                    Iterator it = arrayList.iterator();
                    while (it.hasNext()) {
                        Pair pair = (Pair) it.next();
                        ((a) pair.first).a((r) pair.second);
                    }
                    for (q.a a2 : qVar.e()) {
                        a2.a(qVar);
                    }
                }
            };
            Handler c2 = qVar.c();
            if (c2 == null) {
                r0.run();
            } else {
                c2.post(r0);
            }
        }
    }

    static HttpURLConnection a(URL url) throws IOException {
        HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
        httpURLConnection.setRequestProperty("User-Agent", h());
        httpURLConnection.setRequestProperty("Content-Type", g());
        httpURLConnection.setChunkedStreamingMode(0);
        return httpURLConnection;
    }

    private void e() {
        if (this.b != null) {
            if (!this.b.b()) {
                throw new f("Session provided to a Request in un-opened state.");
            } else if (!this.j.containsKey("access_token")) {
                String e2 = this.b.e();
                com.facebook.b.c.a(e2);
                this.j.putString("access_token", e2);
            }
        }
        this.j.putString("sdk", "android");
        this.j.putString("format", "json");
    }

    private String a(String str) {
        Uri.Builder encodedPath = new Uri.Builder().encodedPath(str);
        for (String next : this.j.keySet()) {
            Object obj = this.j.get(next);
            if (obj == null) {
                obj = "";
            }
            if (d(obj)) {
                encodedPath.appendQueryParameter(next, e(obj).toString());
            } else if (this.c == l.GET) {
                throw new IllegalArgumentException(String.format("Unsupported parameter type for GET request: %s", obj.getClass().getSimpleName()));
            }
        }
        return encodedPath.toString();
    }

    /* access modifiers changed from: package-private */
    public final String c() {
        String str;
        if (this.l != null) {
            throw new f("Can't override URL for a batch request");
        }
        if (this.f != null) {
            str = "method/" + this.f;
        } else {
            str = this.d;
        }
        e();
        return a(str);
    }

    /* access modifiers changed from: package-private */
    public final String d() {
        String str;
        if (this.l != null) {
            return this.l.toString();
        }
        if (this.f != null) {
            str = "https://api.facebook.com/method/" + this.f;
        } else {
            str = "https://graph.facebook.com/" + this.d;
        }
        e();
        return a(str);
    }

    private void a(JSONArray jSONArray, Bundle bundle) throws JSONException, IOException {
        JSONObject jSONObject = new JSONObject();
        if (this.g != null) {
            jSONObject.put("name", this.g);
            jSONObject.put("omit_response_on_success", this.i);
        }
        if (this.h != null) {
            jSONObject.put("depends_on", this.h);
        }
        String c2 = c();
        jSONObject.put("relative_url", c2);
        jSONObject.put("method", this.c);
        if (this.b != null) {
            com.facebook.b.c.a(this.b.e());
        }
        ArrayList arrayList = new ArrayList();
        for (String str : this.j.keySet()) {
            Object obj = this.j.get(str);
            if (c(obj)) {
                String format = String.format("%s%d", "file", Integer.valueOf(bundle.size()));
                arrayList.add(format);
                g.a(bundle, format, obj);
            }
        }
        if (!arrayList.isEmpty()) {
            jSONObject.put("attached_files", TextUtils.join(",", arrayList));
        }
        if (this.e != null) {
            final ArrayList arrayList2 = new ArrayList();
            a(this.e, c2, new c() {
                public void a(String str, String str2) throws IOException {
                    arrayList2.add(String.format("%s=%s", str, URLEncoder.encode(str2, "UTF-8")));
                }
            });
            jSONObject.put("body", TextUtils.join("&", arrayList2));
        }
        jSONArray.put(jSONObject);
    }

    private void f() {
        if (this.d != null && this.f != null) {
            throw new IllegalArgumentException("Only one of a graph path or REST method may be specified per request.");
        }
    }

    /* JADX INFO: finally extract failed */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.facebook.b.c.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      com.facebook.b.c.a(java.lang.String, java.lang.String):void
      com.facebook.b.c.a(java.lang.String, java.lang.Object[]):void
      com.facebook.b.c.a(java.lang.String, java.lang.Object):void */
    static final void a(q qVar, HttpURLConnection httpURLConnection) throws IOException, JSONException {
        com.facebook.b.c cVar = new com.facebook.b.c(m.REQUESTS, "Request");
        int size = qVar.size();
        l lVar = size == 1 ? qVar.get(0).c : l.POST;
        httpURLConnection.setRequestMethod(lVar.name());
        URL url = httpURLConnection.getURL();
        cVar.c("Request:\n");
        cVar.a("Id", (Object) qVar.b());
        cVar.a("URL", url);
        cVar.a("Method", (Object) httpURLConnection.getRequestMethod());
        cVar.a("User-Agent", (Object) httpURLConnection.getRequestProperty("User-Agent"));
        cVar.a("Content-Type", (Object) httpURLConnection.getRequestProperty("Content-Type"));
        httpURLConnection.setConnectTimeout(qVar.a());
        httpURLConnection.setReadTimeout(qVar.a());
        if (!(lVar == l.POST)) {
            cVar.a();
            return;
        }
        httpURLConnection.setDoOutput(true);
        BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(httpURLConnection.getOutputStream());
        try {
            d dVar = new d(bufferedOutputStream, cVar);
            if (size == 1) {
                o a2 = qVar.get(0);
                cVar.c("  Parameters:\n");
                a(a2.j, dVar);
                cVar.c("  Attachments:\n");
                b(a2.j, dVar);
                if (a2.e != null) {
                    a(a2.e, url.getPath(), dVar);
                }
            } else {
                String d2 = d(qVar);
                if (g.a(d2)) {
                    throw new f("At least one request in a batch must have an open Session, or a default app ID must be specified.");
                }
                dVar.a("batch_app_id", d2);
                Bundle bundle = new Bundle();
                a(dVar, qVar, bundle);
                cVar.c("  Attachments:\n");
                b(bundle, dVar);
            }
            bufferedOutputStream.close();
            cVar.a();
        } catch (Throwable th) {
            bufferedOutputStream.close();
            throw th;
        }
    }

    private static void a(com.facebook.c.b bVar, String str, c cVar) throws IOException {
        boolean z;
        boolean z2;
        if (str.startsWith("me/") || str.startsWith("/me/")) {
            int indexOf = str.indexOf(":");
            int indexOf2 = str.indexOf("?");
            z = indexOf > 3 && (indexOf2 == -1 || indexOf < indexOf2);
        } else {
            z = false;
        }
        for (Map.Entry next : bVar.b().entrySet()) {
            if (!z || !((String) next.getKey()).equalsIgnoreCase("image")) {
                z2 = false;
            } else {
                z2 = true;
            }
            a((String) next.getKey(), next.getValue(), cVar, z2);
        }
    }

    private static void a(String str, Object obj, c cVar, boolean z) throws IOException {
        Class<?> cls;
        JSONObject jSONObject;
        Class<?> cls2 = obj.getClass();
        if (com.facebook.c.b.class.isAssignableFrom(cls2)) {
            JSONObject c2 = ((com.facebook.c.b) obj).c();
            cls = c2.getClass();
            jSONObject = c2;
        } else if (com.facebook.c.c.class.isAssignableFrom(cls2)) {
            JSONArray a2 = ((com.facebook.c.c) obj).a();
            cls = a2.getClass();
            jSONObject = a2;
        } else {
            cls = cls2;
            jSONObject = obj;
        }
        if (JSONObject.class.isAssignableFrom(cls)) {
            JSONObject jSONObject2 = (JSONObject) jSONObject;
            if (z) {
                Iterator<String> keys = jSONObject2.keys();
                while (keys.hasNext()) {
                    String next = keys.next();
                    a(String.format("%s[%s]", str, next), jSONObject2.opt(next), cVar, z);
                }
            } else if (jSONObject2.has("id")) {
                a(str, jSONObject2.optString("id"), cVar, z);
            } else if (jSONObject2.has("url")) {
                a(str, jSONObject2.optString("url"), cVar, z);
            }
        } else if (JSONArray.class.isAssignableFrom(cls)) {
            JSONArray jSONArray = (JSONArray) jSONObject;
            int length = jSONArray.length();
            for (int i2 = 0; i2 < length; i2++) {
                a(String.format("%s[%d]", str, Integer.valueOf(i2)), jSONArray.opt(i2), cVar, z);
            }
        } else if (String.class.isAssignableFrom(cls) || Number.class.isAssignableFrom(cls) || Boolean.class.isAssignableFrom(cls)) {
            cVar.a(str, jSONObject.toString());
        } else if (Date.class.isAssignableFrom(cls)) {
            cVar.a(str, new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", Locale.US).format((Date) jSONObject));
        }
    }

    private static void a(Bundle bundle, d dVar) throws IOException {
        for (String next : bundle.keySet()) {
            Object obj = bundle.get(next);
            if (d(obj)) {
                dVar.a(next, obj);
            }
        }
    }

    private static void b(Bundle bundle, d dVar) throws IOException {
        for (String next : bundle.keySet()) {
            Object obj = bundle.get(next);
            if (c(obj)) {
                dVar.a(next, obj);
            }
        }
    }

    private static void a(d dVar, Collection<o> collection, Bundle bundle) throws JSONException, IOException {
        JSONArray jSONArray = new JSONArray();
        for (o a2 : collection) {
            a2.a(jSONArray, bundle);
        }
        dVar.a("batch", jSONArray.toString());
    }

    private static String g() {
        return String.format("multipart/form-data; boundary=%s", "3i2ndDfv2rTHiSisAbouNdArYfORhtTPEefj3q2f");
    }

    private static String h() {
        if (m == null) {
            m = String.format("%s.%s", "FBAndroidSDK", "3.0.0");
        }
        return m;
    }

    private static String d(q qVar) {
        if (!g.a(qVar.f())) {
            return qVar.f();
        }
        Iterator it = qVar.iterator();
        while (it.hasNext()) {
            s sVar = ((o) it.next()).b;
            if (sVar != null) {
                return sVar.d();
            }
        }
        return a;
    }

    private static boolean c(Object obj) {
        return (obj instanceof Bitmap) || (obj instanceof byte[]) || (obj instanceof ParcelFileDescriptor);
    }

    /* access modifiers changed from: private */
    public static boolean d(Object obj) {
        return (obj instanceof String) || (obj instanceof Boolean) || (obj instanceof Number) || (obj instanceof Date);
    }

    /* access modifiers changed from: private */
    public static String e(Object obj) {
        if (obj instanceof String) {
            return (String) obj;
        }
        if ((obj instanceof Boolean) || (obj instanceof Number)) {
            return obj.toString();
        }
        if (obj instanceof Date) {
            return new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", Locale.US).format(obj);
        }
        throw new IllegalArgumentException("Unsupported parameter type.");
    }

    /* compiled from: Request */
    private static class d implements c {
        private final BufferedOutputStream a;
        private final com.facebook.b.c b;
        private boolean c = true;

        public d(BufferedOutputStream bufferedOutputStream, com.facebook.b.c cVar) {
            this.a = bufferedOutputStream;
            this.b = cVar;
        }

        public void a(String str, Object obj) throws IOException {
            if (o.d(obj)) {
                a(str, o.e(obj));
            } else if (obj instanceof Bitmap) {
                a(str, (Bitmap) obj);
            } else if (obj instanceof byte[]) {
                a(str, (byte[]) obj);
            } else if (obj instanceof ParcelFileDescriptor) {
                a(str, (ParcelFileDescriptor) obj);
            } else {
                throw new IllegalArgumentException("value is not a supported type: String, Bitmap, byte[]");
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.facebook.b.c.a(java.lang.String, java.lang.Object):void
         arg types: [java.lang.String, java.lang.String]
         candidates:
          com.facebook.b.c.a(java.lang.String, java.lang.String):void
          com.facebook.b.c.a(java.lang.String, java.lang.Object[]):void
          com.facebook.b.c.a(java.lang.String, java.lang.Object):void */
        public void a(String str, String str2) throws IOException {
            a(str, null, null);
            b("%s", str2);
            a();
            if (this.b != null) {
                this.b.a("    " + str, (Object) str2);
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.facebook.b.c.a(java.lang.String, java.lang.Object):void
         arg types: [java.lang.String, java.lang.String]
         candidates:
          com.facebook.b.c.a(java.lang.String, java.lang.String):void
          com.facebook.b.c.a(java.lang.String, java.lang.Object[]):void
          com.facebook.b.c.a(java.lang.String, java.lang.Object):void */
        public void a(String str, Bitmap bitmap) throws IOException {
            a(str, str, "image/png");
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, this.a);
            b("", new Object[0]);
            a();
            this.b.a("    " + str, (Object) "<Image>");
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.facebook.b.c.a(java.lang.String, java.lang.Object):void
         arg types: [java.lang.String, java.lang.String]
         candidates:
          com.facebook.b.c.a(java.lang.String, java.lang.String):void
          com.facebook.b.c.a(java.lang.String, java.lang.Object[]):void
          com.facebook.b.c.a(java.lang.String, java.lang.Object):void */
        public void a(String str, byte[] bArr) throws IOException {
            a(str, str, "content/unknown");
            this.a.write(bArr);
            b("", new Object[0]);
            a();
            this.b.a("    " + str, (Object) String.format("<Data: %d>", Integer.valueOf(bArr.length)));
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.facebook.b.c.a(java.lang.String, java.lang.Object):void
         arg types: [java.lang.String, java.lang.String]
         candidates:
          com.facebook.b.c.a(java.lang.String, java.lang.String):void
          com.facebook.b.c.a(java.lang.String, java.lang.Object[]):void
          com.facebook.b.c.a(java.lang.String, java.lang.Object):void */
        /* JADX WARNING: Removed duplicated region for block: B:23:0x0061  */
        /* JADX WARNING: Removed duplicated region for block: B:25:0x0066  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void a(java.lang.String r9, android.os.ParcelFileDescriptor r10) throws java.io.IOException {
            /*
                r8 = this;
                r2 = 0
                r4 = 0
                java.lang.String r0 = "content/unknown"
                r8.a(r9, r9, r0)
                android.os.ParcelFileDescriptor$AutoCloseInputStream r3 = new android.os.ParcelFileDescriptor$AutoCloseInputStream     // Catch:{ all -> 0x005d }
                r3.<init>(r10)     // Catch:{ all -> 0x005d }
                java.io.BufferedInputStream r1 = new java.io.BufferedInputStream     // Catch:{ all -> 0x006a }
                r1.<init>(r3)     // Catch:{ all -> 0x006a }
                r0 = 8192(0x2000, float:1.14794E-41)
                byte[] r2 = new byte[r0]     // Catch:{ all -> 0x006e }
                r0 = r4
            L_0x0016:
                int r5 = r1.read(r2)     // Catch:{ all -> 0x006e }
                r6 = -1
                if (r5 != r6) goto L_0x0055
                if (r1 == 0) goto L_0x0022
                r1.close()
            L_0x0022:
                if (r3 == 0) goto L_0x0027
                r3.close()
            L_0x0027:
                java.lang.String r1 = ""
                java.lang.Object[] r2 = new java.lang.Object[r4]
                r8.b(r1, r2)
                r8.a()
                com.facebook.b.c r1 = r8.b
                java.lang.StringBuilder r2 = new java.lang.StringBuilder
                java.lang.String r3 = "    "
                r2.<init>(r3)
                java.lang.StringBuilder r2 = r2.append(r9)
                java.lang.String r2 = r2.toString()
                java.lang.String r3 = "<Data: %d>"
                r5 = 1
                java.lang.Object[] r5 = new java.lang.Object[r5]
                java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
                r5[r4] = r0
                java.lang.String r0 = java.lang.String.format(r3, r5)
                r1.a(r2, r0)
                return
            L_0x0055:
                java.io.BufferedOutputStream r6 = r8.a     // Catch:{ all -> 0x006e }
                r7 = 0
                r6.write(r2, r7, r5)     // Catch:{ all -> 0x006e }
                int r0 = r0 + r5
                goto L_0x0016
            L_0x005d:
                r0 = move-exception
                r1 = r2
            L_0x005f:
                if (r1 == 0) goto L_0x0064
                r1.close()
            L_0x0064:
                if (r2 == 0) goto L_0x0069
                r2.close()
            L_0x0069:
                throw r0
            L_0x006a:
                r0 = move-exception
                r1 = r2
                r2 = r3
                goto L_0x005f
            L_0x006e:
                r0 = move-exception
                r2 = r3
                goto L_0x005f
            */
            throw new UnsupportedOperationException("Method not decompiled: com.facebook.o.d.a(java.lang.String, android.os.ParcelFileDescriptor):void");
        }

        public void a() throws IOException {
            b("--%s", "3i2ndDfv2rTHiSisAbouNdArYfORhtTPEefj3q2f");
        }

        public void a(String str, String str2, String str3) throws IOException {
            a("Content-Disposition: form-data; name=\"%s\"", str);
            if (str2 != null) {
                a("; filename=\"%s\"", str2);
            }
            b("", new Object[0]);
            if (str3 != null) {
                b("%s: %s", "Content-Type", str3);
            }
            b("", new Object[0]);
        }

        public void a(String str, Object... objArr) throws IOException {
            if (this.c) {
                this.a.write("--".getBytes());
                this.a.write("3i2ndDfv2rTHiSisAbouNdArYfORhtTPEefj3q2f".getBytes());
                this.a.write("\r\n".getBytes());
                this.c = false;
            }
            this.a.write(String.format(str, objArr).getBytes());
        }

        public void b(String str, Object... objArr) throws IOException {
            a(str, objArr);
            a("\r\n", new Object[0]);
        }
    }
}
