package com.facebook;

/* compiled from: FacebookException */
public class f extends RuntimeException {
    public f() {
    }

    public f(String str) {
        super(str);
    }

    public f(String str, Throwable th) {
        super(str, th);
    }

    public f(Throwable th) {
        super(th);
    }
}
