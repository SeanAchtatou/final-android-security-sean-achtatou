package com.facebook.c;

import com.facebook.b.h;
import com.facebook.g;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Proxy;
import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.AbstractList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: GraphObject */
public interface b {
    <T extends b> T a(Class cls);

    Object a(String str);

    void a(String str, Object obj);

    Map<String, Object> b();

    JSONObject c();

    /* compiled from: GraphObject */
    public static final class a {
        private static final HashSet<Class<?>> a = new HashSet<>();
        private static final SimpleDateFormat[] b = {new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", Locale.US), new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US), new SimpleDateFormat("yyyy-MM-dd", Locale.US)};

        public static b a(JSONObject jSONObject) {
            return a(jSONObject, b.class);
        }

        public static <T extends b> T a(JSONObject jSONObject, Class<T> cls) {
            return b(cls, jSONObject);
        }

        public static b a() {
            return a(b.class);
        }

        public static <T extends b> T a(Class<T> cls) {
            return b(cls, new JSONObject());
        }

        public static <T> c<T> a(JSONArray jSONArray, Class<T> cls) {
            return new C0014a(jSONArray, cls);
        }

        /* access modifiers changed from: private */
        public static <T extends b> T b(Class<T> cls, JSONObject jSONObject) {
            d(cls);
            return (b) Proxy.newProxyInstance(b.class.getClassLoader(), new Class[]{cls}, new C0015b(jSONObject, cls));
        }

        /* access modifiers changed from: private */
        public static Map<String, Object> c(JSONObject jSONObject) {
            return (Map) Proxy.newProxyInstance(b.class.getClassLoader(), new Class[]{Map.class}, new C0015b(jSONObject, Map.class));
        }

        private static synchronized <T extends b> boolean b(Class<T> cls) {
            boolean contains;
            synchronized (a.class) {
                contains = a.contains(cls);
            }
            return contains;
        }

        private static synchronized <T extends b> void c(Class<T> cls) {
            synchronized (a.class) {
                a.add(cls);
            }
        }

        private static <T extends b> void d(Class<T> cls) {
            if (!b((Class) cls)) {
                if (!cls.isInterface()) {
                    throw new g("Factory can only wrap interfaces, not class: " + cls.getName());
                }
                for (Method method : cls.getMethods()) {
                    String name = method.getName();
                    int length = method.getParameterTypes().length;
                    Class<?> returnType = method.getReturnType();
                    boolean isAnnotationPresent = method.isAnnotationPresent(f.class);
                    if (!method.getDeclaringClass().isAssignableFrom(b.class)) {
                        if (length == 1 && returnType == Void.TYPE) {
                            if (isAnnotationPresent) {
                                if (com.facebook.b.g.a(((f) method.getAnnotation(f.class)).a())) {
                                }
                            } else if (name.startsWith("set") && name.length() > 3) {
                            }
                        } else if (length == 0 && returnType != Void.TYPE) {
                            if (isAnnotationPresent) {
                                if (!com.facebook.b.g.a(((f) method.getAnnotation(f.class)).a())) {
                                }
                            } else if (name.startsWith("get") && name.length() > 3) {
                            }
                        }
                        throw new g("Factory can't proxy method: " + method.toString());
                    }
                }
                c(cls);
            }
        }

        static <U> U a(Object obj, Class<U> cls, ParameterizedType parameterizedType) {
            if (obj == null) {
                return null;
            }
            Class<?> cls2 = obj.getClass();
            if (cls.isAssignableFrom(cls2) || cls.isPrimitive()) {
                return obj;
            }
            if (b.class.isAssignableFrom(cls)) {
                if (JSONObject.class.isAssignableFrom(cls2)) {
                    return b(cls, (JSONObject) obj);
                }
                if (b.class.isAssignableFrom(cls2)) {
                    return ((b) obj).a(cls);
                }
                throw new g("Can't create GraphObject from " + cls2.getName());
            } else if (!Iterable.class.equals(cls) && !Collection.class.equals(cls) && !List.class.equals(cls) && !c.class.equals(cls)) {
                if (String.class.equals(cls)) {
                    if (Double.class.isAssignableFrom(cls2) || Float.class.isAssignableFrom(cls2)) {
                        return String.format("%f", obj);
                    } else if (Number.class.isAssignableFrom(cls2)) {
                        return String.format("%d", obj);
                    }
                } else if (Date.class.equals(cls) && String.class.isAssignableFrom(cls2)) {
                    SimpleDateFormat[] simpleDateFormatArr = b;
                    int length = simpleDateFormatArr.length;
                    int i = 0;
                    while (i < length) {
                        try {
                            U parse = simpleDateFormatArr[i].parse((String) obj);
                            if (parse != null) {
                                return parse;
                            }
                            i++;
                        } catch (ParseException e) {
                        }
                    }
                }
                throw new g("Can't convert type" + cls2.getName() + " to " + cls.getName());
            } else if (parameterizedType == null) {
                throw new g("can't infer generic type of: " + cls.toString());
            } else {
                Type[] actualTypeArguments = parameterizedType.getActualTypeArguments();
                if (actualTypeArguments == null || actualTypeArguments.length != 1 || !(actualTypeArguments[0] instanceof Class)) {
                    throw new g("Expect collection properties to be of a type with exactly one generic parameter.");
                }
                Class cls3 = (Class) actualTypeArguments[0];
                if (JSONArray.class.isAssignableFrom(cls2)) {
                    return a((JSONArray) obj, cls3);
                }
                throw new g("Can't create Collection from " + cls2.getName());
            }
        }

        static String a(String str) {
            return str.replaceAll("([a-z])([A-Z])", "$1_$2").toLowerCase(Locale.US);
        }

        /* access modifiers changed from: private */
        public static Object b(Object obj) {
            Class<?> cls = obj.getClass();
            if (b.class.isAssignableFrom(cls)) {
                return ((b) obj).c();
            }
            if (c.class.isAssignableFrom(cls)) {
                return ((c) obj).a();
            }
            return obj;
        }

        /* compiled from: GraphObject */
        private static abstract class c<STATE> implements InvocationHandler {
            protected final STATE a;

            protected c(STATE state) {
                this.a = state;
            }

            /* access modifiers changed from: protected */
            public final Object a(Method method) {
                throw new g(String.valueOf(getClass().getName()) + " got an unexpected method signature: " + method.toString());
            }

            /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
                jadx.core.utils.exceptions.JadxRuntimeException: Not class type: STATE
                	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
                	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
                	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
                	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
                	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
                	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
                */
            protected final java.lang.Object a(java.lang.Object r4, java.lang.reflect.Method r5, java.lang.Object[] r6) throws java.lang.Throwable {
                /*
                    r3 = this;
                    r2 = 0
                    java.lang.String r0 = r5.getName()
                    java.lang.String r1 = "equals"
                    boolean r1 = r0.equals(r1)
                    if (r1 == 0) goto L_0x0032
                    r0 = r6[r2]
                    if (r0 != 0) goto L_0x0016
                    java.lang.Boolean r0 = java.lang.Boolean.valueOf(r2)
                L_0x0015:
                    return r0
                L_0x0016:
                    java.lang.reflect.InvocationHandler r0 = java.lang.reflect.Proxy.getInvocationHandler(r0)
                    boolean r1 = r0 instanceof com.facebook.c.b.a.C0015b
                    if (r1 != 0) goto L_0x0023
                    java.lang.Boolean r0 = java.lang.Boolean.valueOf(r2)
                    goto L_0x0015
                L_0x0023:
                    com.facebook.c.b$a$b r0 = (com.facebook.c.b.a.C0015b) r0
                    STATE r1 = r3.a
                    java.lang.Object r0 = r0.a
                    boolean r0 = r1.equals(r0)
                    java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)
                    goto L_0x0015
                L_0x0032:
                    java.lang.String r1 = "toString"
                    boolean r0 = r0.equals(r1)
                    if (r0 == 0) goto L_0x003f
                    java.lang.String r0 = r3.toString()
                    goto L_0x0015
                L_0x003f:
                    STATE r0 = r3.a
                    java.lang.Object r0 = r5.invoke(r0, r6)
                    goto L_0x0015
                */
                throw new UnsupportedOperationException("Method not decompiled: com.facebook.c.b.a.c.a(java.lang.Object, java.lang.reflect.Method, java.lang.Object[]):java.lang.Object");
            }
        }

        /* renamed from: com.facebook.c.b$a$b  reason: collision with other inner class name */
        /* compiled from: GraphObject */
        private static final class C0015b extends c<JSONObject> {
            private final Class<?> b;

            public C0015b(JSONObject jSONObject, Class<?> cls) {
                super(jSONObject);
                this.b = cls;
            }

            public String toString() {
                return String.format("GraphObject{graphObjectClass=%s, state=%s}", this.b.getSimpleName(), this.a);
            }

            public final Object invoke(Object obj, Method method, Object[] objArr) throws Throwable {
                Class<?> declaringClass = method.getDeclaringClass();
                if (declaringClass == Object.class) {
                    return a(obj, method, objArr);
                }
                if (declaringClass == Map.class) {
                    return a(method, objArr);
                }
                if (declaringClass == b.class) {
                    return b(obj, method, objArr);
                }
                if (b.class.isAssignableFrom(declaringClass)) {
                    return b(method, objArr);
                }
                return a(method);
            }

            private final Object a(Method method, Object[] objArr) {
                Map<String, Object> map;
                String name = method.getName();
                if (name.equals("clear")) {
                    e.a((JSONObject) this.a);
                    return null;
                } else if (name.equals("containsKey")) {
                    return Boolean.valueOf(((JSONObject) this.a).has((String) objArr[0]));
                } else {
                    if (name.equals("containsValue")) {
                        return Boolean.valueOf(e.a((JSONObject) this.a, objArr[0]));
                    }
                    if (name.equals("entrySet")) {
                        return e.b((JSONObject) this.a);
                    }
                    if (name.equals("get")) {
                        return ((JSONObject) this.a).opt((String) objArr[0]);
                    }
                    if (name.equals("isEmpty")) {
                        return ((JSONObject) this.a).length() == 0;
                    }
                    if (name.equals("keySet")) {
                        return e.c((JSONObject) this.a);
                    }
                    if (name.equals("put")) {
                        return a(objArr);
                    }
                    if (name.equals("putAll")) {
                        if (objArr[0] instanceof Map) {
                            map = (Map) objArr[0];
                        } else if (objArr[0] instanceof b) {
                            map = ((b) objArr[0]).b();
                        } else {
                            map = null;
                        }
                        e.a((JSONObject) this.a, map);
                        return null;
                    } else if (name.equals("remove")) {
                        ((JSONObject) this.a).remove((String) objArr[0]);
                        return null;
                    } else if (name.equals("size")) {
                        return Integer.valueOf(((JSONObject) this.a).length());
                    } else {
                        if (name.equals("values")) {
                            return e.d((JSONObject) this.a);
                        }
                        return a(method);
                    }
                }
            }

            private final Object b(Object obj, Method method, Object[] objArr) {
                String name = method.getName();
                if (name.equals("cast")) {
                    Class cls = (Class) objArr[0];
                    if (cls == null || !cls.isAssignableFrom(this.b)) {
                        return a.b(cls, (JSONObject) this.a);
                    }
                    return obj;
                } else if (name.equals("getInnerJSONObject")) {
                    return ((C0015b) Proxy.getInvocationHandler(obj)).a;
                } else {
                    if (name.equals("asMap")) {
                        return a.c((JSONObject) this.a);
                    }
                    if (name.equals("getProperty")) {
                        return ((JSONObject) this.a).opt((String) objArr[0]);
                    }
                    if (name.equals("setProperty")) {
                        return a(objArr);
                    }
                    if (!name.equals("removeProperty")) {
                        return a(method);
                    }
                    ((JSONObject) this.a).remove((String) objArr[0]);
                    return null;
                }
            }

            /* JADX WARN: Failed to insert an additional move for type inference into block B:31:0x005a */
            /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v9, resolved type: java.lang.Object} */
            /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v7, resolved type: org.json.JSONArray} */
            /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v8, resolved type: org.json.JSONArray} */
            /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v9, resolved type: org.json.JSONArray} */
            /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v10, resolved type: java.lang.Object} */
            /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v20, resolved type: org.json.JSONObject} */
            /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v11, resolved type: org.json.JSONArray} */
            /* JADX WARNING: Multi-variable type inference failed */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            private final java.lang.Object b(java.lang.reflect.Method r8, java.lang.Object[] r9) throws org.json.JSONException {
                /*
                    r7 = this;
                    r1 = 0
                    java.lang.String r2 = r8.getName()
                    java.lang.Class[] r0 = r8.getParameterTypes()
                    int r4 = r0.length
                    java.lang.Class<com.facebook.c.f> r0 = com.facebook.c.f.class
                    java.lang.annotation.Annotation r0 = r8.getAnnotation(r0)
                    com.facebook.c.f r0 = (com.facebook.c.f) r0
                    if (r0 == 0) goto L_0x0036
                    java.lang.String r0 = r0.a()
                    r3 = r0
                L_0x0019:
                    if (r4 != 0) goto L_0x0041
                    java.lang.Object r0 = r7.a
                    org.json.JSONObject r0 = (org.json.JSONObject) r0
                    java.lang.Object r2 = r0.opt(r3)
                    java.lang.Class r3 = r8.getReturnType()
                    java.lang.reflect.Type r0 = r8.getGenericReturnType()
                    boolean r4 = r0 instanceof java.lang.reflect.ParameterizedType
                    if (r4 == 0) goto L_0x00b8
                    java.lang.reflect.ParameterizedType r0 = (java.lang.reflect.ParameterizedType) r0
                L_0x0031:
                    java.lang.Object r1 = com.facebook.c.b.a.a(r2, r3, r0)
                L_0x0035:
                    return r1
                L_0x0036:
                    r0 = 3
                    java.lang.String r0 = r2.substring(r0)
                    java.lang.String r0 = com.facebook.c.b.a.a(r0)
                    r3 = r0
                    goto L_0x0019
                L_0x0041:
                    r0 = 1
                    if (r4 != r0) goto L_0x00b1
                    r0 = 0
                    r0 = r9[r0]
                    java.lang.Class<com.facebook.c.b> r2 = com.facebook.c.b.class
                    java.lang.Class r4 = r0.getClass()
                    boolean r2 = r2.isAssignableFrom(r4)
                    if (r2 == 0) goto L_0x0062
                    com.facebook.c.b r0 = (com.facebook.c.b) r0
                    org.json.JSONObject r0 = r0.c()
                    r2 = r0
                L_0x005a:
                    java.lang.Object r0 = r7.a
                    org.json.JSONObject r0 = (org.json.JSONObject) r0
                    r0.putOpt(r3, r2)
                    goto L_0x0035
                L_0x0062:
                    java.lang.Class<com.facebook.c.c> r2 = com.facebook.c.c.class
                    java.lang.Class r4 = r0.getClass()
                    boolean r2 = r2.isAssignableFrom(r4)
                    if (r2 == 0) goto L_0x0076
                    com.facebook.c.c r0 = (com.facebook.c.c) r0
                    org.json.JSONArray r0 = r0.a()
                    r2 = r0
                    goto L_0x005a
                L_0x0076:
                    java.lang.Class<java.lang.Iterable> r2 = java.lang.Iterable.class
                    java.lang.Class r4 = r0.getClass()
                    boolean r2 = r2.isAssignableFrom(r4)
                    if (r2 == 0) goto L_0x00b6
                    org.json.JSONArray r2 = new org.json.JSONArray
                    r2.<init>()
                    java.lang.Iterable r0 = (java.lang.Iterable) r0
                    java.util.Iterator r4 = r0.iterator()
                L_0x008d:
                    boolean r0 = r4.hasNext()
                    if (r0 == 0) goto L_0x005a
                    java.lang.Object r0 = r4.next()
                    java.lang.Class<com.facebook.c.b> r5 = com.facebook.c.b.class
                    java.lang.Class r6 = r0.getClass()
                    boolean r5 = r5.isAssignableFrom(r6)
                    if (r5 == 0) goto L_0x00ad
                    com.facebook.c.b r0 = (com.facebook.c.b) r0
                    org.json.JSONObject r0 = r0.c()
                    r2.put(r0)
                    goto L_0x008d
                L_0x00ad:
                    r2.put(r0)
                    goto L_0x008d
                L_0x00b1:
                    java.lang.Object r1 = r7.a(r8)
                    goto L_0x0035
                L_0x00b6:
                    r2 = r0
                    goto L_0x005a
                L_0x00b8:
                    r0 = r1
                    goto L_0x0031
                */
                throw new UnsupportedOperationException("Method not decompiled: com.facebook.c.b.a.C0015b.b(java.lang.reflect.Method, java.lang.Object[]):java.lang.Object");
            }

            private Object a(Object[] objArr) {
                try {
                    ((JSONObject) this.a).putOpt((String) objArr[0], a.b(objArr[1]));
                    return null;
                } catch (JSONException e) {
                    throw new IllegalArgumentException(e);
                }
            }
        }

        /* renamed from: com.facebook.c.b$a$a  reason: collision with other inner class name */
        /* compiled from: GraphObject */
        private static final class C0014a<T> extends AbstractList<T> implements c<T> {
            private final JSONArray a;
            private final Class<?> b;

            public C0014a(JSONArray jSONArray, Class<?> cls) {
                h.a(jSONArray, "state");
                h.a(cls, "itemType");
                this.a = jSONArray;
                this.b = cls;
            }

            public String toString() {
                return String.format("GraphObjectList{itemType=%s, state=%s}", this.b.getSimpleName(), this.a);
            }

            public void add(int i, T t) {
                if (i < 0) {
                    throw new IndexOutOfBoundsException();
                } else if (i < size()) {
                    throw new UnsupportedOperationException("Only adding items at the end of the list is supported.");
                } else {
                    a(i, t);
                }
            }

            public T set(int i, T t) {
                a(i);
                T t2 = get(i);
                a(i, t);
                return t2;
            }

            public int hashCode() {
                return this.a.hashCode();
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (getClass() != obj.getClass()) {
                    return false;
                }
                return this.a.equals(((C0014a) obj).a);
            }

            public T get(int i) {
                a(i);
                return a.a(this.a.opt(i), this.b, null);
            }

            public int size() {
                return this.a.length();
            }

            public final JSONArray a() {
                return this.a;
            }

            public void clear() {
                throw new UnsupportedOperationException();
            }

            public boolean remove(Object obj) {
                throw new UnsupportedOperationException();
            }

            public boolean removeAll(Collection<?> collection) {
                throw new UnsupportedOperationException();
            }

            public boolean retainAll(Collection<?> collection) {
                throw new UnsupportedOperationException();
            }

            private void a(int i) {
                if (i < 0 || i >= this.a.length()) {
                    throw new IndexOutOfBoundsException();
                }
            }

            private void a(int i, T t) {
                try {
                    this.a.put(i, a.b((Object) t));
                } catch (JSONException e) {
                    throw new IllegalArgumentException(e);
                }
            }
        }
    }
}
