package com.facebook;

import android.annotation.TargetApi;
import android.os.AsyncTask;
import android.os.Handler;
import android.util.Log;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.HttpURLConnection;
import java.util.List;
import java.util.concurrent.Executor;

@TargetApi(3)
/* compiled from: RequestAsyncTask */
public class p extends AsyncTask<Void, Void, List<r>> {
    private static final String a = p.class.getCanonicalName();
    private static Method b;
    private final HttpURLConnection c;
    private final q d;
    private Exception e;

    static {
        for (Method method : AsyncTask.class.getMethods()) {
            if ("executeOnExecutor".equals(method.getName())) {
                Class<?>[] parameterTypes = method.getParameterTypes();
                if (parameterTypes.length == 2 && parameterTypes[0] == Executor.class && parameterTypes[1].isArray()) {
                    b = method;
                    return;
                }
            }
        }
    }

    public p(q qVar) {
        this(null, qVar);
    }

    public p(HttpURLConnection httpURLConnection, q qVar) {
        this.d = qVar;
        this.c = httpURLConnection;
    }

    public String toString() {
        return "{RequestAsyncTask: " + " connection: " + this.c + ", requests: " + this.d + "}";
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
        super.onPreExecute();
        if (this.d.c() == null) {
            this.d.a(new Handler());
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void onPostExecute(List<r> list) {
        super.onPostExecute(list);
        if (this.e != null) {
            Log.d(a, String.format("onPostExecute: exception encountered during request: %s", this.e.getMessage()));
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public List<r> doInBackground(Void... voidArr) {
        try {
            if (this.c == null) {
                return this.d.g();
            }
            return o.a(this.c, this.d);
        } catch (Exception e2) {
            this.e = e2;
            return null;
        }
    }

    /* access modifiers changed from: package-private */
    public p a() {
        try {
            if (b != null) {
                b.invoke(this, w.a(), null);
                return this;
            }
        } catch (IllegalAccessException | InvocationTargetException e2) {
        }
        execute(new Void[0]);
        return this;
    }
}
