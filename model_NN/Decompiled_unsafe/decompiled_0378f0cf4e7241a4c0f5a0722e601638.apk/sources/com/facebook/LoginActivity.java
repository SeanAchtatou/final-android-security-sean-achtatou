package com.facebook;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import com.facebook.a.e;
import com.facebook.c;

public class LoginActivity extends Activity {
    private String a;
    private c b;
    private c.C0016c c;

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(e.C0012e.com_facebook_login_activity_layout);
        if (bundle != null) {
            this.a = bundle.getString("callingPackage");
            this.b = (c) bundle.getSerializable("authorizationClient");
        } else {
            this.a = getCallingPackage();
            this.b = new c();
            this.c = (c.C0016c) getIntent().getSerializableExtra("request");
        }
        this.b.a((Activity) this);
        this.b.a(new c.i() {
            public void a(c.j jVar) {
                LoginActivity.this.a(jVar);
            }
        });
        this.b.a(new c.d() {
            public void a() {
                LoginActivity.this.findViewById(e.d.com_facebook_login_activity_progress_bar).setVisibility(0);
            }

            public void b() {
                LoginActivity.this.findViewById(e.d.com_facebook_login_activity_progress_bar).setVisibility(8);
            }
        });
    }

    /* access modifiers changed from: private */
    public void a(c.j jVar) {
        this.c = null;
        int i = jVar.a == c.j.a.CANCEL ? 0 : -1;
        Bundle bundle = new Bundle();
        bundle.putSerializable("com.facebook.LoginActivity:Result", jVar);
        Intent intent = new Intent();
        intent.putExtras(bundle);
        setResult(i, intent);
        finish();
    }

    public void onResume() {
        super.onResume();
        if (this.a == null) {
            throw new f("Cannot call LoginActivity with a null calling package. This can occur if the launchMode of the caller is singleInstance.");
        }
        this.b.a(this.c);
    }

    public void onPause() {
        super.onPause();
        this.b.c();
        findViewById(e.d.com_facebook_login_activity_progress_bar).setVisibility(8);
    }

    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putString("callingPackage", this.a);
        bundle.putSerializable("authorizationClient", this.b);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        this.b.a(i, i2, intent);
    }

    static Bundle a(c.C0016c cVar) {
        Bundle bundle = new Bundle();
        bundle.putSerializable("request", cVar);
        return bundle;
    }
}
