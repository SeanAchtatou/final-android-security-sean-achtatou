package com.facebook.a;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import com.facebook.d;
import com.facebook.h;
import com.facebook.s;
import com.facebook.u;
import com.facebook.v;
import com.facebook.y;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/* compiled from: Facebook */
public class c {
    @Deprecated
    public static final Uri a = Uri.parse("content://com.facebook.katana.provider.AttributionIdProvider");
    @Deprecated
    protected static String b = "https://m.facebook.com/dialog/";
    @Deprecated
    protected static String c = "https://graph.facebook.com/";
    @Deprecated
    protected static String d = "https://api.facebook.com/restserver.php";
    private final Object e = new Object();
    /* access modifiers changed from: private */
    public String f = null;
    /* access modifiers changed from: private */
    public long g = 0;
    /* access modifiers changed from: private */
    public long h = 0;
    private String i;
    private Activity j;
    /* access modifiers changed from: private */
    public String[] k;
    private s l;
    private volatile s m;
    private boolean n;
    private b o;
    private volatile s p;
    private final long q = 86400000;

    /* compiled from: Facebook */
    public interface a {
        void a();

        void a(Bundle bundle);

        void a(b bVar);

        void a(d dVar);
    }

    @Deprecated
    public c(String str) {
        if (str == null) {
            throw new IllegalArgumentException("You must specify your application ID when instantiating a Facebook object. See README for details.");
        }
        this.i = str;
    }

    @Deprecated
    public void a(Activity activity, String[] strArr, int i2, a aVar) {
        u uVar;
        if (i2 >= 0) {
            uVar = u.SSO_WITH_FALLBACK;
        } else {
            uVar = u.SUPPRESS_SSO;
        }
        a(activity, strArr, i2, uVar, aVar);
    }

    private void a(Activity activity, String[] strArr, int i2, u uVar, final a aVar) {
        b("authorize");
        this.l = new s.c(activity).a(this.i).a(e()).a();
        this.j = activity;
        this.k = strArr != null ? strArr : new String[0];
        a(this.l, new s.d(activity).a(new s.f() {
            public void a(s sVar, v vVar, Exception exc) {
                c.this.a(sVar, vVar, exc, aVar);
            }
        }).a(uVar).a(i2).a(Arrays.asList(strArr)), this.k.length > 0);
    }

    private void a(s sVar, s.d dVar, boolean z) {
        dVar.a(true);
        if (z) {
            sVar.b(dVar);
        } else {
            sVar.a(dVar);
        }
    }

    /* access modifiers changed from: private */
    public void a(s sVar, v vVar, Exception exc, a aVar) {
        Bundle a2 = sVar.a();
        if (vVar == v.OPENED) {
            s sVar2 = null;
            synchronized (this.e) {
                if (sVar != this.m) {
                    sVar2 = this.m;
                    this.m = sVar;
                    this.n = false;
                }
            }
            if (sVar2 != null) {
                sVar2.h();
            }
            aVar.a(a2);
        } else if (exc == null) {
        } else {
            if (exc instanceof h) {
                aVar.a();
            } else if (!(exc instanceof d) || a2 == null || !a2.containsKey("com.facebook.sdk.WebViewErrorCode") || !a2.containsKey("com.facebook.sdk.FailingUrl")) {
                aVar.a(new d(exc.getMessage()));
            } else {
                aVar.a(new b(exc.getMessage(), a2.getInt("com.facebook.sdk.WebViewErrorCode"), a2.getString("com.facebook.sdk.FailingUrl")));
            }
        }
    }

    @Deprecated
    public void a(int i2, int i3, Intent intent) {
        b("authorizeCallback");
        s sVar = this.l;
        if (sVar != null && sVar.a(this.j, i2, i3, intent)) {
            this.l = null;
            this.j = null;
            this.k = null;
        }
    }

    /* access modifiers changed from: package-private */
    public String a(Context context) throws MalformedURLException, IOException {
        s sVar;
        b("logout");
        Bundle bundle = new Bundle();
        bundle.putString("method", "auth.expireSession");
        String a2 = a(bundle);
        long currentTimeMillis = System.currentTimeMillis();
        synchronized (this.e) {
            sVar = this.m;
            this.m = null;
            this.f = null;
            this.g = 0;
            this.h = currentTimeMillis;
            this.n = false;
        }
        if (sVar != null) {
            sVar.i();
        }
        return a2;
    }

    @Deprecated
    public String a(Bundle bundle) throws MalformedURLException, IOException {
        if (bundle.containsKey("method")) {
            return b(null, bundle, "GET");
        }
        throw new IllegalArgumentException("API method must be specified. (parameters must contain key \"method\" and value). See http://developers.facebook.com/docs/reference/rest/");
    }

    @Deprecated
    public String a(String str, Bundle bundle, String str2) throws FileNotFoundException, MalformedURLException, IOException {
        return b(str, bundle, str2);
    }

    /* access modifiers changed from: package-private */
    public String b(String str, Bundle bundle, String str2) throws FileNotFoundException, MalformedURLException, IOException {
        bundle.putString("format", "json");
        if (a()) {
            bundle.putString("access_token", c());
        }
        return f.a(str != null ? String.valueOf(c) + str : d, str2, bundle);
    }

    @Deprecated
    public boolean a() {
        return c() != null && (d() == 0 || System.currentTimeMillis() < d());
    }

    private void b(String str) {
        if (this.p != null) {
            throw new UnsupportedOperationException(String.format("Cannot call %s after setSession has been called.", str));
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0021, code lost:
        if (r0 != null) goto L_0x0025;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0025, code lost:
        if (r4 == null) goto L_0x004e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0027, code lost:
        r0 = r4.g();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x002b, code lost:
        r3 = new com.facebook.s.c(r7.j).a(r7.i).a(e()).a();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x004a, code lost:
        if (r3.c() == com.facebook.v.b) goto L_0x005e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0050, code lost:
        if (r7.k == null) goto L_0x0059;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0052, code lost:
        r0 = java.util.Arrays.asList(r7.k);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0059, code lost:
        r0 = java.util.Collections.emptyList();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x005e, code lost:
        r4 = new com.facebook.s.d(r7.j).b(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x006d, code lost:
        if (r0.isEmpty() == false) goto L_0x0092;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x006f, code lost:
        r0 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x0070, code lost:
        a(r3, r4, r0);
        r4 = r7.e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x0075, code lost:
        monitor-enter(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x0078, code lost:
        if (r7.n != false) goto L_0x007e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x007c, code lost:
        if (r7.m != null) goto L_0x0097;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x007e, code lost:
        r0 = r7.m;
        r7.m = r3;
        r7.n = false;
        r6 = r3;
        r3 = r0;
        r0 = r6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x0088, code lost:
        monitor-exit(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x0089, code lost:
        if (r3 == null) goto L_0x008e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x008b, code lost:
        r3.h();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x0092, code lost:
        r0 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x0097, code lost:
        r0 = null;
        r3 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:0x008e, code lost:
        continue;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:?, code lost:
        return null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:67:?, code lost:
        return null;
     */
    @java.lang.Deprecated
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.facebook.s b() {
        /*
            r7 = this;
            r1 = 0
            r2 = 0
        L_0x0002:
            java.lang.Object r3 = r7.e
            monitor-enter(r3)
            com.facebook.s r0 = r7.p     // Catch:{ all -> 0x0019 }
            if (r0 == 0) goto L_0x000d
            com.facebook.s r0 = r7.p     // Catch:{ all -> 0x0019 }
            monitor-exit(r3)     // Catch:{ all -> 0x0019 }
        L_0x000c:
            return r0
        L_0x000d:
            com.facebook.s r0 = r7.m     // Catch:{ all -> 0x0019 }
            if (r0 != 0) goto L_0x0015
            boolean r0 = r7.n     // Catch:{ all -> 0x0019 }
            if (r0 != 0) goto L_0x001c
        L_0x0015:
            com.facebook.s r0 = r7.m     // Catch:{ all -> 0x0019 }
            monitor-exit(r3)     // Catch:{ all -> 0x0019 }
            goto L_0x000c
        L_0x0019:
            r0 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x0019 }
            throw r0
        L_0x001c:
            java.lang.String r0 = r7.f     // Catch:{ all -> 0x0019 }
            com.facebook.s r4 = r7.m     // Catch:{ all -> 0x0019 }
            monitor-exit(r3)     // Catch:{ all -> 0x0019 }
            if (r0 != 0) goto L_0x0025
            r0 = r2
            goto L_0x000c
        L_0x0025:
            if (r4 == 0) goto L_0x004e
            java.util.List r0 = r4.g()
        L_0x002b:
            com.facebook.s$c r3 = new com.facebook.s$c
            android.app.Activity r4 = r7.j
            r3.<init>(r4)
            java.lang.String r4 = r7.i
            com.facebook.s$c r3 = r3.a(r4)
            com.facebook.y r4 = r7.e()
            com.facebook.s$c r3 = r3.a(r4)
            com.facebook.s r3 = r3.a()
            com.facebook.v r4 = r3.c()
            com.facebook.v r5 = com.facebook.v.CREATED_TOKEN_LOADED
            if (r4 == r5) goto L_0x005e
            r0 = r2
            goto L_0x000c
        L_0x004e:
            java.lang.String[] r0 = r7.k
            if (r0 == 0) goto L_0x0059
            java.lang.String[] r0 = r7.k
            java.util.List r0 = java.util.Arrays.asList(r0)
            goto L_0x002b
        L_0x0059:
            java.util.List r0 = java.util.Collections.emptyList()
            goto L_0x002b
        L_0x005e:
            com.facebook.s$d r4 = new com.facebook.s$d
            android.app.Activity r5 = r7.j
            r4.<init>(r5)
            com.facebook.s$d r4 = r4.a(r0)
            boolean r0 = r0.isEmpty()
            if (r0 == 0) goto L_0x0092
            r0 = r1
        L_0x0070:
            r7.a(r3, r4, r0)
            java.lang.Object r4 = r7.e
            monitor-enter(r4)
            boolean r0 = r7.n     // Catch:{ all -> 0x0094 }
            if (r0 != 0) goto L_0x007e
            com.facebook.s r0 = r7.m     // Catch:{ all -> 0x0094 }
            if (r0 != 0) goto L_0x0097
        L_0x007e:
            com.facebook.s r0 = r7.m     // Catch:{ all -> 0x0094 }
            r7.m = r3     // Catch:{ all -> 0x0094 }
            r5 = 0
            r7.n = r5     // Catch:{ all -> 0x0094 }
            r6 = r3
            r3 = r0
            r0 = r6
        L_0x0088:
            monitor-exit(r4)     // Catch:{ all -> 0x0094 }
            if (r3 == 0) goto L_0x008e
            r3.h()
        L_0x008e:
            if (r0 == 0) goto L_0x0002
            goto L_0x000c
        L_0x0092:
            r0 = 1
            goto L_0x0070
        L_0x0094:
            r0 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x0094 }
            throw r0
        L_0x0097:
            r0 = r2
            r3 = r2
            goto L_0x0088
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.a.c.b():com.facebook.s");
    }

    @Deprecated
    public String c() {
        s b2 = b();
        if (b2 != null) {
            return b2.e();
        }
        return null;
    }

    @Deprecated
    public long d() {
        s b2 = b();
        if (b2 != null) {
            return b2.f().getTime();
        }
        return this.g;
    }

    @Deprecated
    public void a(String str) {
        b("setAccessToken");
        synchronized (this.e) {
            this.f = str;
            this.h = System.currentTimeMillis();
            this.n = true;
        }
    }

    @Deprecated
    public void a(long j2) {
        b("setAccessExpires");
        synchronized (this.e) {
            this.g = j2;
            this.h = System.currentTimeMillis();
            this.n = true;
        }
    }

    private y e() {
        if (this.o == null) {
            this.o = new b(this, null);
        }
        return this.o;
    }

    /* access modifiers changed from: private */
    public static String[] b(List<String> list) {
        String[] strArr = new String[list.size()];
        if (list != null) {
            int i2 = 0;
            while (true) {
                int i3 = i2;
                if (i3 >= strArr.length) {
                    break;
                }
                strArr[i3] = list.get(i3);
                i2 = i3 + 1;
            }
        }
        return strArr;
    }

    /* access modifiers changed from: private */
    public static List<String> b(String[] strArr) {
        if (strArr != null) {
            return Arrays.asList(strArr);
        }
        return Collections.emptyList();
    }

    /* compiled from: Facebook */
    private class b extends y {
        private b() {
        }

        /* synthetic */ b(c cVar, b bVar) {
            this();
        }

        public Bundle a() {
            Bundle bundle = new Bundle();
            if (c.this.f != null) {
                y.a(bundle, c.this.f);
                y.a(bundle, c.this.g);
                y.a(bundle, c.b(c.this.k));
                y.a(bundle, com.facebook.b.WEB_VIEW);
                y.b(bundle, c.this.h);
            }
            return bundle;
        }

        public void a(Bundle bundle) {
            c.this.f = y.c(bundle);
            c.this.g = y.d(bundle);
            c.this.k = c.b(y.e(bundle));
            c.this.h = y.g(bundle);
        }

        public void b() {
            c.this.f = (String) null;
        }
    }
}
