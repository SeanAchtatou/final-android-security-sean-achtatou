package com.facebook.a;

import android.content.Context;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;

@Deprecated
/* compiled from: AsyncFacebookRunner */
public class a {
    c a;

    @Deprecated
    /* renamed from: com.facebook.a.a$a  reason: collision with other inner class name */
    /* compiled from: AsyncFacebookRunner */
    public interface C0011a {
        void a(d dVar, Object obj);

        void a(FileNotFoundException fileNotFoundException, Object obj);

        void a(IOException iOException, Object obj);

        void a(String str, Object obj);

        void a(MalformedURLException malformedURLException, Object obj);
    }

    public a(c cVar) {
        this.a = cVar;
    }

    @Deprecated
    public void a(final Context context, final C0011a aVar, final Object obj) {
        new Thread() {
            public void run() {
                try {
                    String a2 = a.this.a.a(context);
                    if (a2.length() == 0 || a2.equals("false")) {
                        aVar.a(new d("auth.expireSession failed"), obj);
                    } else {
                        aVar.a(a2, obj);
                    }
                } catch (FileNotFoundException e) {
                    aVar.a(e, obj);
                } catch (MalformedURLException e2) {
                    aVar.a(e2, obj);
                } catch (IOException e3) {
                    aVar.a(e3, obj);
                }
            }
        }.start();
    }

    @Deprecated
    public void a(Context context, C0011a aVar) {
        a(context, aVar, null);
    }
}
