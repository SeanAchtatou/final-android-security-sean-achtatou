package com.facebook;

import com.facebook.a.e;
import com.facebook.b.g;
import java.net.HttpURLConnection;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: FacebookRequestError */
public final class i {
    private static final b a = new b(200, 299, null);
    private static final b b = new b(200, 299, null);
    private static final b c = new b(400, 499, null);
    private static final b d = new b(500, 599, null);
    private final int e;
    private final boolean f;
    private final a g;
    private final int h;
    private final int i;
    private final int j;
    private final String k;
    private final String l;
    private final JSONObject m;
    private final JSONObject n;
    private final Object o;
    private final HttpURLConnection p;
    private final f q;

    /* compiled from: FacebookRequestError */
    public enum a {
        AUTHENTICATION_RETRY,
        AUTHENTICATION_REOPEN_SESSION,
        PERMISSION,
        SERVER,
        THROTTLING,
        OTHER,
        BAD_REQUEST,
        CLIENT
    }

    /* compiled from: FacebookRequestError */
    private static class b {
        private final int a;
        private final int b;

        private b(int i, int i2) {
            this.a = i;
            this.b = i2;
        }

        /* synthetic */ b(int i, int i2, b bVar) {
            this(i, i2);
        }

        /* access modifiers changed from: package-private */
        public boolean a(int i) {
            return this.a <= i && i <= this.b;
        }
    }

    private i(int i2, int i3, int i4, String str, String str2, JSONObject jSONObject, JSONObject jSONObject2, Object obj, HttpURLConnection httpURLConnection, f fVar) {
        boolean z;
        int i5;
        boolean z2 = false;
        this.h = i2;
        this.i = i3;
        this.j = i4;
        this.k = str;
        this.l = str2;
        this.n = jSONObject;
        this.m = jSONObject2;
        this.o = obj;
        this.p = httpURLConnection;
        if (fVar != null) {
            this.q = fVar;
            z = true;
        } else {
            this.q = new j(this, str2);
            z = false;
        }
        a aVar = null;
        if (z) {
            aVar = a.CLIENT;
            i5 = 0;
        } else {
            if (i3 == 1 || i3 == 2) {
                aVar = a.SERVER;
                i5 = 0;
            } else if (i3 == 4 || i3 == 17) {
                aVar = a.THROTTLING;
                i5 = 0;
            } else if (i3 == 10 || a.a(i3)) {
                aVar = a.PERMISSION;
                i5 = e.f.com_facebook_requesterror_permissions;
            } else if (i3 != 102 && i3 != 190) {
                i5 = 0;
            } else if (i4 == 459 || i4 == 464) {
                aVar = a.AUTHENTICATION_RETRY;
                i5 = e.f.com_facebook_requesterror_web_login;
                z2 = true;
            } else {
                aVar = a.AUTHENTICATION_REOPEN_SESSION;
                if (i4 == 458) {
                    i5 = e.f.com_facebook_requesterror_relogin;
                } else if (i4 == 460) {
                    i5 = e.f.com_facebook_requesterror_password_changed;
                } else {
                    i5 = e.f.com_facebook_requesterror_reconnect;
                }
            }
            if (aVar == null) {
                if (c.a(i2)) {
                    aVar = a.BAD_REQUEST;
                } else if (d.a(i2)) {
                    aVar = a.SERVER;
                } else {
                    aVar = a.OTHER;
                }
            }
        }
        this.g = aVar;
        this.e = i5;
        this.f = z2;
    }

    private i(int i2, int i3, int i4, String str, String str2, JSONObject jSONObject, JSONObject jSONObject2, Object obj, HttpURLConnection httpURLConnection) {
        this(i2, i3, i4, str, str2, jSONObject, jSONObject2, obj, httpURLConnection, null);
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    i(HttpURLConnection httpURLConnection, Exception exc) {
        this(-1, -1, -1, null, null, null, null, null, httpURLConnection, exc instanceof f ? (f) exc : new f(exc));
    }

    public i(int i2, String str, String str2) {
        this(-1, i2, -1, str, str2, null, null, null, null, null);
    }

    public int a() {
        return this.h;
    }

    public int b() {
        return this.i;
    }

    public String c() {
        return this.k;
    }

    public String d() {
        if (this.l != null) {
            return this.l;
        }
        return this.q.getLocalizedMessage();
    }

    public f e() {
        return this.q;
    }

    public String toString() {
        return "{HttpStatus: " + this.h + ", errorCode: " + this.i + ", errorType: " + this.k + ", errorMessage: " + this.l + "}";
    }

    static i a(JSONObject jSONObject, Object obj, HttpURLConnection httpURLConnection) {
        JSONObject jSONObject2;
        String optString;
        String optString2;
        int i2;
        int i3 = -1;
        try {
            if (jSONObject.has("code")) {
                int i4 = jSONObject.getInt("code");
                Object a2 = g.a(jSONObject, "body", "FACEBOOK_NON_JSON_RESULT");
                if (a2 != null && (a2 instanceof JSONObject)) {
                    JSONObject jSONObject3 = (JSONObject) a2;
                    boolean z = false;
                    if (jSONObject3.has("error")) {
                        JSONObject jSONObject4 = (JSONObject) g.a(jSONObject3, "error", (String) null);
                        String optString3 = jSONObject4.optString("type", null);
                        optString2 = jSONObject4.optString("message", null);
                        int optInt = jSONObject4.optInt("code", -1);
                        i3 = jSONObject4.optInt("error_subcode", -1);
                        z = true;
                        i2 = optInt;
                        optString = optString3;
                    } else if (jSONObject3.has("error_code") || jSONObject3.has("error_msg") || jSONObject3.has("error_reason")) {
                        optString = jSONObject3.optString("error_reason", null);
                        optString2 = jSONObject3.optString("error_msg", null);
                        int optInt2 = jSONObject3.optInt("error_code", -1);
                        i3 = jSONObject3.optInt("error_subcode", -1);
                        i2 = optInt2;
                        z = true;
                    } else {
                        i2 = -1;
                        optString2 = null;
                        optString = null;
                    }
                    if (z) {
                        return new i(i4, i2, i3, optString, optString2, jSONObject3, jSONObject, obj, httpURLConnection);
                    }
                }
                if (!b.a(i4)) {
                    if (jSONObject.has("body")) {
                        jSONObject2 = (JSONObject) g.a(jSONObject, "body", "FACEBOOK_NON_JSON_RESULT");
                    } else {
                        jSONObject2 = null;
                    }
                    return new i(i4, -1, -1, null, null, jSONObject2, jSONObject, obj, httpURLConnection);
                }
            }
        } catch (JSONException e2) {
        }
        return null;
    }
}
