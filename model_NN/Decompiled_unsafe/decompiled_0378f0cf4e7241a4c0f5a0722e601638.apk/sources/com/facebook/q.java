package com.facebook;

import android.os.Handler;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/* compiled from: RequestBatch */
public class q extends AbstractList<o> {
    private static AtomicInteger a = new AtomicInteger();
    private Handler b;
    private List<o> c;
    private int d;
    private final String e;
    private List<a> f;
    private String g;

    /* compiled from: RequestBatch */
    public interface a {
        void a(q qVar);
    }

    public q() {
        this.c = new ArrayList();
        this.d = 0;
        this.e = Integer.valueOf(a.incrementAndGet()).toString();
        this.f = new ArrayList();
        this.c = new ArrayList();
    }

    public q(Collection<o> collection) {
        this.c = new ArrayList();
        this.d = 0;
        this.e = Integer.valueOf(a.incrementAndGet()).toString();
        this.f = new ArrayList();
        this.c = new ArrayList(collection);
    }

    public q(o... oVarArr) {
        this.c = new ArrayList();
        this.d = 0;
        this.e = Integer.valueOf(a.incrementAndGet()).toString();
        this.f = new ArrayList();
        this.c = Arrays.asList(oVarArr);
    }

    public int a() {
        return this.d;
    }

    public void a(a aVar) {
        if (!this.f.contains(aVar)) {
            this.f.add(aVar);
        }
    }

    /* renamed from: a */
    public final boolean add(o oVar) {
        return this.c.add(oVar);
    }

    /* renamed from: a */
    public final void add(int i, o oVar) {
        this.c.add(i, oVar);
    }

    public final void clear() {
        this.c.clear();
    }

    /* renamed from: a */
    public final o get(int i) {
        return this.c.get(i);
    }

    /* renamed from: b */
    public final o remove(int i) {
        return this.c.remove(i);
    }

    /* renamed from: b */
    public final o set(int i, o oVar) {
        return this.c.set(i, oVar);
    }

    public final int size() {
        return this.c.size();
    }

    /* access modifiers changed from: package-private */
    public final String b() {
        return this.e;
    }

    /* access modifiers changed from: package-private */
    public final Handler c() {
        return this.b;
    }

    /* access modifiers changed from: package-private */
    public final void a(Handler handler) {
        this.b = handler;
    }

    /* access modifiers changed from: package-private */
    public final List<o> d() {
        return this.c;
    }

    /* access modifiers changed from: package-private */
    public final List<a> e() {
        return this.f;
    }

    /* access modifiers changed from: package-private */
    public final String f() {
        return this.g;
    }

    /* access modifiers changed from: package-private */
    public final void a(String str) {
        this.g = str;
    }

    public final List<r> g() {
        return i();
    }

    public final p h() {
        return j();
    }

    /* access modifiers changed from: package-private */
    public List<r> i() {
        return o.b(this);
    }

    /* access modifiers changed from: package-private */
    public p j() {
        return o.c(this);
    }
}
