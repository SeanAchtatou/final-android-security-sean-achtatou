package com.facebook;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;

/* compiled from: GetTokenClient */
final class k implements ServiceConnection {
    final Context a;
    final String b;
    final Handler c;
    a d;
    boolean e;
    Messenger f;

    /* compiled from: GetTokenClient */
    interface a {
        void a(Bundle bundle);
    }

    k(Context context, String str) {
        Context applicationContext = context.getApplicationContext();
        this.a = applicationContext != null ? applicationContext : context;
        this.b = str;
        this.c = new Handler() {
            public void handleMessage(Message message) {
                k.this.a(message);
            }
        };
    }

    /* access modifiers changed from: package-private */
    public void a(a aVar) {
        this.d = aVar;
    }

    /* access modifiers changed from: package-private */
    public boolean a() {
        Intent intent = new Intent("com.facebook.platform.PLATFORM_SERVICE");
        intent.addCategory("android.intent.category.DEFAULT");
        Intent b2 = n.b(this.a, intent);
        if (b2 == null) {
            a((Bundle) null);
            return false;
        }
        this.e = true;
        this.a.bindService(b2, this, 1);
        return true;
    }

    /* access modifiers changed from: package-private */
    public void b() {
        this.e = false;
    }

    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        this.f = new Messenger(iBinder);
        c();
    }

    public void onServiceDisconnected(ComponentName componentName) {
        this.f = null;
        this.a.unbindService(this);
        a((Bundle) null);
    }

    private void c() {
        Bundle bundle = new Bundle();
        bundle.putString("com.facebook.platform.extra.APPLICATION_ID", this.b);
        Message obtain = Message.obtain((Handler) null, 65536);
        obtain.arg1 = 20121101;
        obtain.setData(bundle);
        obtain.replyTo = new Messenger(this.c);
        try {
            this.f.send(obtain);
        } catch (RemoteException e2) {
            a((Bundle) null);
        }
    }

    /* access modifiers changed from: private */
    public void a(Message message) {
        if (message.what == 65537) {
            Bundle data = message.getData();
            if (data.getString("com.facebook.platform.status.ERROR_TYPE") != null) {
                a((Bundle) null);
            } else {
                a(data);
            }
            this.a.unbindService(this);
        }
    }

    private void a(Bundle bundle) {
        if (this.e) {
            this.e = false;
            a aVar = this.d;
            if (aVar != null) {
                aVar.a(bundle);
            }
        }
    }
}
