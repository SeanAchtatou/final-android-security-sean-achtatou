package com.facebook;

import android.content.ContentResolver;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import com.facebook.b.g;
import com.facebook.c.b;
import com.facebook.o;
import java.lang.reflect.Field;
import java.util.HashSet;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Executor;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import org.json.JSONException;

/* compiled from: Settings */
public final class w {
    private static final HashSet<m> a = new HashSet<>();
    private static volatile Executor b;
    private static volatile boolean c;
    private static final Object d = new Object();
    private static final Uri e = Uri.parse("content://com.facebook.katana.provider.AttributionIdProvider");
    private static final BlockingQueue<Runnable> f = new LinkedBlockingQueue(10);
    private static final ThreadFactory g = new ThreadFactory() {
        private final AtomicInteger a = new AtomicInteger(0);

        public Thread newThread(Runnable runnable) {
            return new Thread(runnable, "FacebookSdk #" + this.a.incrementAndGet());
        }
    };

    public static final boolean a(m mVar) {
        boolean z;
        synchronized (a) {
            z = a.contains(mVar);
        }
        return z;
    }

    public static Executor a() {
        synchronized (d) {
            if (b == null) {
                Executor c2 = c();
                if (c2 == null) {
                    c2 = new ThreadPoolExecutor(5, 128, 1, TimeUnit.SECONDS, f, g);
                }
                b = c2;
            }
        }
        return b;
    }

    private static Executor c() {
        try {
            Field field = AsyncTask.class.getField("THREAD_POOL_EXECUTOR");
            if (field == null) {
                return null;
            }
            try {
                Object obj = field.get(null);
                if (obj == null) {
                    return null;
                }
                if (!(obj instanceof Executor)) {
                    return null;
                }
                return (Executor) obj;
            } catch (IllegalAccessException e2) {
                return null;
            }
        } catch (NoSuchFieldException e3) {
            return null;
        }
    }

    public static boolean b() {
        return c;
    }

    public static boolean a(Context context, String str) {
        if (str == null) {
            return false;
        }
        try {
            String a2 = a(context.getContentResolver());
            SharedPreferences sharedPreferences = context.getSharedPreferences("com.facebook.sdk.attributionTracking", 0);
            String str2 = String.valueOf(str) + "ping";
            long j = sharedPreferences.getLong(str2, 0);
            if (j == 0 && a2 != null) {
                Bundle bundle = new Bundle();
                bundle.putString("fields", "supports_attribution");
                o a3 = o.a((s) null, str, (o.a) null);
                a3.a(bundle);
                Object a4 = a3.b().b().a("supports_attribution");
                if (!(a4 instanceof Boolean)) {
                    throw new JSONException(String.format("%s contains %s instead of a Boolean", "supports_attribution", a4));
                } else if (((Boolean) a4).booleanValue()) {
                    b a5 = b.a.a();
                    a5.a("event", "MOBILE_APP_INSTALL");
                    a5.a("attribution", a2);
                    o.a((s) null, String.format("%s/activities", str), a5, (o.a) null).b();
                    SharedPreferences.Editor edit = sharedPreferences.edit();
                    j = System.currentTimeMillis();
                    edit.putLong(str2, j);
                    edit.commit();
                }
            }
            return j != 0;
        } catch (Exception e2) {
            g.a("Facebook-publish", e2.getMessage());
            return false;
        }
    }

    public static String a(ContentResolver contentResolver) {
        ContentResolver contentResolver2 = contentResolver;
        Cursor query = contentResolver2.query(e, new String[]{"aid"}, null, null, null);
        if (query == null || !query.moveToFirst()) {
            return null;
        }
        String string = query.getString(query.getColumnIndex("aid"));
        query.close();
        return string;
    }
}
