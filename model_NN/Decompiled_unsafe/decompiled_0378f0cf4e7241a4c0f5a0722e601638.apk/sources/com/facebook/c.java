package com.facebook;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.webkit.CookieSyncManager;
import com.facebook.a.e;
import com.facebook.c.a;
import com.facebook.c.b;
import com.facebook.c.d;
import com.facebook.k;
import com.facebook.o;
import com.facebook.q;
import com.facebook.widget.a;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/* compiled from: AuthorizationClient */
class c implements Serializable {
    List<b> a;
    b b;
    transient Context c;
    transient k d;
    transient i e;
    transient d f;
    transient boolean g;
    C0016c h;

    /* compiled from: AuthorizationClient */
    interface d {
        void a();

        void b();
    }

    /* compiled from: AuthorizationClient */
    interface i {
        void a(j jVar);
    }

    /* compiled from: AuthorizationClient */
    interface k {
        Activity a();

        void a(Intent intent, int i);
    }

    c() {
    }

    /* access modifiers changed from: package-private */
    public void a(Context context) {
        this.c = context;
        this.d = null;
    }

    /* access modifiers changed from: package-private */
    public void a(final Activity activity) {
        this.c = activity;
        this.d = new k() {
            public void a(Intent intent, int i) {
                activity.startActivityForResult(intent, i);
            }

            public Activity a() {
                return activity;
            }
        };
    }

    /* access modifiers changed from: package-private */
    public void a(C0016c cVar) {
        if (b()) {
            a();
        } else {
            b(cVar);
        }
    }

    /* access modifiers changed from: package-private */
    public void b(C0016c cVar) {
        if (cVar != null) {
            if (this.h != null) {
                throw new f("Attempted to authorize while a request is pending.");
            } else if (!cVar.i() || d()) {
                this.h = cVar;
                this.a = c(cVar);
                e();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a() {
        if (this.h == null || this.b == null) {
            throw new f("Attempted to continue authorization without a pending request.");
        } else if (this.b.a()) {
            this.b.c();
            f();
        }
    }

    /* access modifiers changed from: package-private */
    public boolean b() {
        return (this.h == null || this.b == null) ? false : true;
    }

    /* access modifiers changed from: package-private */
    public void c() {
        if (this.b != null) {
            this.b.c();
        }
    }

    /* access modifiers changed from: package-private */
    public boolean a(int i2, int i3, Intent intent) {
        if (i2 == this.h.d()) {
            return this.b.a(i2, i3, intent);
        }
        return false;
    }

    private List<b> c(C0016c cVar) {
        ArrayList arrayList = new ArrayList();
        u c2 = cVar.c();
        if (c2.a()) {
            if (!cVar.g()) {
                arrayList.add(new e());
                arrayList.add(new g());
            }
            arrayList.add(new h());
        }
        if (c2.b()) {
            arrayList.add(new l());
        }
        return arrayList;
    }

    /* access modifiers changed from: package-private */
    public boolean d() {
        if (this.g) {
            return true;
        }
        if (a("android.permission.INTERNET") != 0) {
            b(j.a(this.c.getString(e.f.com_facebook_internet_permission_error_title), this.c.getString(e.f.com_facebook_internet_permission_error_message)));
            return false;
        }
        this.g = true;
        return true;
    }

    /* access modifiers changed from: package-private */
    public void e() {
        while (this.a != null && !this.a.isEmpty()) {
            this.b = this.a.remove(0);
            if (f()) {
                return;
            }
        }
        if (this.h != null) {
            h();
        }
    }

    private void h() {
        b(j.a("Login attempt failed.", null));
    }

    /* access modifiers changed from: package-private */
    public boolean f() {
        if (!this.b.b() || d()) {
            return this.b.a(this.h);
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public void a(j jVar) {
        if (jVar.b == null || !this.h.i()) {
            b(jVar);
        } else {
            c(jVar);
        }
    }

    /* access modifiers changed from: package-private */
    public void b(j jVar) {
        this.a = null;
        this.b = null;
        this.h = null;
        e(jVar);
    }

    /* access modifiers changed from: package-private */
    public void a(i iVar) {
        this.e = iVar;
    }

    /* access modifiers changed from: package-private */
    public void a(d dVar) {
        this.f = dVar;
    }

    /* access modifiers changed from: package-private */
    public k g() {
        if (this.d != null) {
            return this.d;
        }
        if (this.h != null) {
            return new k() {
                public void a(Intent intent, int i) {
                    c.this.h.a().a(intent, i);
                }

                public Activity a() {
                    return c.this.h.a().a();
                }
            };
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public int a(String str) {
        return this.c.checkCallingOrSelfPermission(str);
    }

    /* access modifiers changed from: package-private */
    public void c(j jVar) {
        if (jVar.b == null) {
            throw new f("Can't validate without a token");
        }
        q d2 = d(jVar);
        i();
        d2.h();
    }

    /* access modifiers changed from: package-private */
    public q d(final j jVar) {
        final ArrayList arrayList = new ArrayList();
        final ArrayList arrayList2 = new ArrayList();
        String a2 = jVar.b.a();
        AnonymousClass3 r3 = new o.a() {
            public void a(r rVar) {
                try {
                    d dVar = (d) rVar.a(d.class);
                    if (dVar != null) {
                        arrayList.add(dVar.a());
                    }
                } catch (Exception e) {
                }
            }
        };
        String h2 = this.h.h();
        o c2 = c(h2);
        c2.a((o.a) r3);
        o c3 = c(a2);
        c3.a((o.a) r3);
        o b2 = b(h2);
        b2.a((o.a) new o.a() {
            public void a(r rVar) {
                com.facebook.c.c<b> a2;
                try {
                    a aVar = (a) rVar.a(a.class);
                    if (aVar != null && (a2 = aVar.a()) != null && a2.size() == 1) {
                        arrayList2.addAll(((b) a2.get(0)).b().keySet());
                    }
                } catch (Exception e) {
                }
            }
        });
        q qVar = new q(c2, c3, b2);
        qVar.a(this.h.f());
        qVar.a(new q.a() {
            public void a(q qVar) {
                j a2;
                try {
                    if (arrayList.size() != 2 || arrayList.get(0) == null || arrayList.get(1) == null || !((String) arrayList.get(0)).equals(arrayList.get(1))) {
                        a2 = j.a("User logged in as different Facebook user.", null);
                    } else {
                        a2 = j.a(a.a(jVar.b, arrayList2));
                    }
                    c.this.b(a2);
                } catch (Exception e) {
                    c.this.b(j.a("Caught exception", e.getMessage()));
                } finally {
                    c.this.j();
                }
            }
        });
        return qVar;
    }

    /* access modifiers changed from: package-private */
    public o b(String str) {
        Bundle bundle = new Bundle();
        bundle.putString("fields", "id");
        bundle.putString("access_token", str);
        return new o(null, "me/permissions", bundle, l.GET, null);
    }

    /* access modifiers changed from: package-private */
    public o c(String str) {
        Bundle bundle = new Bundle();
        bundle.putString("fields", "id");
        bundle.putString("access_token", str);
        return new o(null, "me", bundle, l.GET, null);
    }

    private void e(j jVar) {
        if (this.e != null) {
            this.e.a(jVar);
        }
    }

    /* access modifiers changed from: private */
    public void i() {
        if (this.f != null) {
            this.f.a();
        }
    }

    /* access modifiers changed from: private */
    public void j() {
        if (this.f != null) {
            this.f.b();
        }
    }

    /* compiled from: AuthorizationClient */
    abstract class b implements Serializable {
        /* access modifiers changed from: package-private */
        public abstract boolean a(C0016c cVar);

        b() {
        }

        /* access modifiers changed from: package-private */
        public boolean a(int i, int i2, Intent intent) {
            return false;
        }

        /* access modifiers changed from: package-private */
        public boolean a() {
            return false;
        }

        /* access modifiers changed from: package-private */
        public boolean b() {
            return false;
        }

        /* access modifiers changed from: package-private */
        public void c() {
        }
    }

    /* compiled from: AuthorizationClient */
    class l extends b {
        private transient com.facebook.widget.a c;

        l() {
            super();
        }

        /* access modifiers changed from: package-private */
        public boolean a() {
            return true;
        }

        /* access modifiers changed from: package-private */
        public boolean b() {
            return true;
        }

        /* access modifiers changed from: package-private */
        public void c() {
            if (this.c != null) {
                this.c.dismiss();
                this.c = null;
            }
        }

        /* access modifiers changed from: package-private */
        public boolean a(final C0016c cVar) {
            String f = cVar.f();
            Bundle bundle = new Bundle();
            if (!com.facebook.b.g.a(cVar.b())) {
                bundle.putString("scope", TextUtils.join(",", cVar.b()));
            }
            com.facebook.b.g.b(c.this.c);
            this.c = ((a.C0017a) new a(c.this.g().a(), f, bundle).a(new a.d() {
                public void a(Bundle bundle, f fVar) {
                    l.this.a(cVar, bundle, fVar);
                }
            })).a();
            this.c.show();
            return true;
        }

        /* access modifiers changed from: package-private */
        public void a(C0016c cVar, Bundle bundle, f fVar) {
            j a;
            if (bundle != null) {
                CookieSyncManager.createInstance(c.this.c).sync();
                a = j.a(a.a(cVar.b(), bundle, b.WEB_VIEW));
            } else if (fVar instanceof h) {
                a = j.a("User canceled log in.");
            } else {
                a = j.a(fVar.getMessage(), null);
            }
            c.this.a(a);
        }
    }

    /* compiled from: AuthorizationClient */
    class e extends b {
        private transient k c;

        e() {
            super();
        }

        /* access modifiers changed from: package-private */
        public void c() {
            if (this.c != null) {
                this.c.b();
                this.c = null;
            }
        }

        /* access modifiers changed from: package-private */
        public boolean a(final C0016c cVar) {
            this.c = new k(c.this.c, cVar.f());
            if (!this.c.a()) {
                return false;
            }
            c.this.i();
            this.c.a(new k.a() {
                public void a(Bundle bundle) {
                    e.this.a(cVar, bundle);
                }
            });
            return true;
        }

        /* access modifiers changed from: package-private */
        public void a(C0016c cVar, Bundle bundle) {
            this.c = null;
            c.this.j();
            if (bundle != null) {
                ArrayList<String> stringArrayList = bundle.getStringArrayList("com.facebook.platform.extra.PERMISSIONS");
                List<String> b2 = cVar.b();
                if (stringArrayList == null || (b2 != null && !stringArrayList.containsAll(b2))) {
                    ArrayList arrayList = new ArrayList();
                    for (String next : b2) {
                        if (!stringArrayList.contains(next)) {
                            arrayList.add(next);
                        }
                    }
                    cVar.a(arrayList);
                } else {
                    c.this.a(j.a(a.a(bundle, b.FACEBOOK_APPLICATION_SERVICE)));
                    return;
                }
            }
            c.this.e();
        }
    }

    /* compiled from: AuthorizationClient */
    abstract class f extends b {
        f() {
            super();
        }

        /* access modifiers changed from: protected */
        public boolean a(Intent intent, int i) {
            if (intent == null) {
                return false;
            }
            try {
                c.this.g().a(intent, i);
                return true;
            } catch (ActivityNotFoundException e) {
                return false;
            }
        }
    }

    /* compiled from: AuthorizationClient */
    class g extends f {
        g() {
            super();
        }

        /* access modifiers changed from: package-private */
        public boolean a(C0016c cVar) {
            return a(n.a(c.this.c, cVar.f(), new ArrayList(cVar.b()), cVar.e().a()), cVar.d());
        }

        /* access modifiers changed from: package-private */
        public boolean a(int i, int i2, Intent intent) {
            j a;
            if (n.a(intent)) {
                c.this.e();
                return true;
            }
            if (i2 == 0) {
                a = j.a(intent.getStringExtra("com.facebook.platform.status.ERROR_DESCRIPTION"));
            } else if (i2 != -1) {
                a = j.a("Unexpected resultCode from authorization.", null);
            } else {
                a = a(intent);
            }
            if (a != null) {
                c.this.a(a);
                return true;
            }
            c.this.e();
            return true;
        }

        private j a(Intent intent) {
            Bundle extras = intent.getExtras();
            String string = extras.getString("com.facebook.platform.status.ERROR_TYPE");
            if (string == null) {
                return j.a(a.a(extras, b.FACEBOOK_APPLICATION_NATIVE));
            }
            if ("ServiceDisabled".equals(string)) {
                return null;
            }
            if ("UserCanceled".equals(string)) {
                return j.a((String) null);
            }
            return j.a(string, extras.getString("error_description"));
        }
    }

    /* compiled from: AuthorizationClient */
    class h extends f {
        h() {
            super();
        }

        /* access modifiers changed from: package-private */
        public boolean a(C0016c cVar) {
            return a(n.a(c.this.c, cVar.f(), cVar.b()), cVar.d());
        }

        /* access modifiers changed from: package-private */
        public boolean a(int i, int i2, Intent intent) {
            j a;
            if (i2 == 0) {
                a = j.a(intent.getStringExtra("error"));
            } else if (i2 != -1) {
                a = j.a("Unexpected resultCode from authorization.", null);
            } else {
                a = a(intent);
            }
            if (a != null) {
                c.this.a(a);
                return true;
            }
            c.this.e();
            return true;
        }

        private j a(Intent intent) {
            Bundle extras = intent.getExtras();
            String string = extras.getString("error");
            if (string == null) {
                string = extras.getString("error_type");
            }
            if (string == null) {
                return j.a(a.a(c.this.h.b(), extras, b.FACEBOOK_APPLICATION_WEB));
            }
            if (com.facebook.b.d.a.contains(string)) {
                return null;
            }
            if (com.facebook.b.d.b.contains(string)) {
                return j.a((String) null);
            }
            return j.a(string, extras.getString("error_description"));
        }
    }

    /* compiled from: AuthorizationClient */
    static class a extends a.C0017a {
        public a(Context context, String str, Bundle bundle) {
            super(context, str, "oauth", bundle);
        }

        public com.facebook.widget.a a() {
            Bundle e = e();
            e.putString("redirect_uri", "fbconnect://success");
            e.putString("client_id", b());
            return new com.facebook.widget.a(c(), "oauth", e, d(), f());
        }
    }

    /* renamed from: com.facebook.c$c  reason: collision with other inner class name */
    /* compiled from: AuthorizationClient */
    static class C0016c implements Serializable {
        private final transient k a;
        private u b;
        private int c;
        private boolean d = false;
        private List<String> e;
        private t f;
        private String g;
        private String h;

        C0016c(u uVar, int i, boolean z, List<String> list, t tVar, String str, String str2, k kVar) {
            this.b = uVar;
            this.c = i;
            this.d = z;
            this.e = list;
            this.f = tVar;
            this.g = str;
            this.h = str2;
            this.a = kVar;
        }

        /* access modifiers changed from: package-private */
        public k a() {
            return this.a;
        }

        /* access modifiers changed from: package-private */
        public List<String> b() {
            return this.e;
        }

        /* access modifiers changed from: package-private */
        public void a(List<String> list) {
            this.e = list;
        }

        /* access modifiers changed from: package-private */
        public u c() {
            return this.b;
        }

        /* access modifiers changed from: package-private */
        public int d() {
            return this.c;
        }

        /* access modifiers changed from: package-private */
        public t e() {
            return this.f;
        }

        /* access modifiers changed from: package-private */
        public String f() {
            return this.g;
        }

        /* access modifiers changed from: package-private */
        public boolean g() {
            return this.d;
        }

        /* access modifiers changed from: package-private */
        public String h() {
            return this.h;
        }

        /* access modifiers changed from: package-private */
        public boolean i() {
            return this.h != null && !this.d;
        }
    }

    /* compiled from: AuthorizationClient */
    static class j implements Serializable {
        final a a;
        final a b;
        final String c;

        /* compiled from: AuthorizationClient */
        enum a {
            SUCCESS,
            CANCEL,
            ERROR
        }

        private j(a aVar, a aVar2, String str) {
            this.b = aVar2;
            this.c = str;
            this.a = aVar;
        }

        static j a(a aVar) {
            return new j(a.SUCCESS, aVar, null);
        }

        static j a(String str) {
            return new j(a.CANCEL, null, str);
        }

        static j a(String str, String str2) {
            if (str2 != null) {
                str = String.valueOf(str) + ": " + str2;
            }
            return new j(a.ERROR, null, str);
        }
    }
}
