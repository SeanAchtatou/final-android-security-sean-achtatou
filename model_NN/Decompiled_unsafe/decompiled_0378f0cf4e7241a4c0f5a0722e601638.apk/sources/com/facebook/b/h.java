package com.facebook.b;

import java.util.Collection;

/* compiled from: Validate */
public final class h {
    public static void a(Object obj, String str) {
        if (obj == null) {
            throw new NullPointerException("Argument " + str + " cannot be null");
        }
    }

    public static <T> void a(Collection collection, String str) {
        if (collection.isEmpty()) {
            throw new IllegalArgumentException("Container '" + str + "' cannot be empty");
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.facebook.b.h.a(java.lang.Object, java.lang.String):void
     arg types: [java.util.Collection<T>, java.lang.String]
     candidates:
      com.facebook.b.h.a(java.lang.String, java.lang.String):void
      com.facebook.b.h.a(java.util.Collection, java.lang.String):void
      com.facebook.b.h.a(java.lang.Object, java.lang.String):void */
    public static <T> void b(Collection<T> collection, String str) {
        a((Object) collection, str);
        for (T t : collection) {
            if (t == null) {
                throw new NullPointerException("Container '" + str + "' cannot contain null values");
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.facebook.b.h.a(java.util.Collection, java.lang.String):void
     arg types: [java.util.Collection<T>, java.lang.String]
     candidates:
      com.facebook.b.h.a(java.lang.Object, java.lang.String):void
      com.facebook.b.h.a(java.lang.String, java.lang.String):void
      com.facebook.b.h.a(java.util.Collection, java.lang.String):void */
    public static <T> void c(Collection<T> collection, String str) {
        b(collection, str);
        a((Collection) collection, str);
    }

    public static void a(String str, String str2) {
        if (g.a(str)) {
            throw new IllegalArgumentException("Argument " + str2 + " cannot be null or empty");
        }
    }
}
