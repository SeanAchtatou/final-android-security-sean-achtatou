package com.facebook.b;

import android.content.Context;
import com.facebook.m;
import com.facebook.w;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Date;
import java.util.PriorityQueue;
import java.util.concurrent.atomic.AtomicLong;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

/* compiled from: FileLruCache */
public final class b {
    static final String a = b.class.getSimpleName();
    /* access modifiers changed from: private */
    public static final AtomicLong b = new AtomicLong();
    private final String c;
    private final d d;
    private final File e;
    private boolean f;
    private final Object g = new Object();

    /* compiled from: FileLruCache */
    private interface f {
        void a();
    }

    public b(Context context, String str, d dVar) {
        this.c = str;
        this.d = dVar;
        this.e = new File(context.getCacheDir(), str);
        this.e.mkdirs();
        a.a(this.e);
    }

    public InputStream a(String str) throws IOException {
        return a(str, (String) null);
    }

    public InputStream a(String str, String str2) throws IOException {
        File file = new File(this.e, g.b(str));
        try {
            BufferedInputStream bufferedInputStream = new BufferedInputStream(new FileInputStream(file), 8192);
            try {
                JSONObject a2 = g.a(bufferedInputStream);
                if (a2 == null) {
                    return null;
                }
                String optString = a2.optString("key");
                if (optString == null || !optString.equals(str)) {
                    bufferedInputStream.close();
                    return null;
                }
                String optString2 = a2.optString("tag", null);
                if ((str2 != null || optString2 == null) && (str2 == null || str2.equals(optString2))) {
                    long time = new Date().getTime();
                    c.a(m.CACHE, a, "Setting lastModified to " + Long.valueOf(time) + " for " + file.getName());
                    file.setLastModified(time);
                    return bufferedInputStream;
                }
                bufferedInputStream.close();
                return null;
            } finally {
                bufferedInputStream.close();
            }
        } catch (IOException e2) {
            return null;
        }
    }

    /* access modifiers changed from: package-private */
    public OutputStream b(String str) throws IOException {
        return b(str, null);
    }

    public OutputStream b(final String str, String str2) throws IOException {
        final File b2 = a.b(this.e);
        b2.delete();
        if (!b2.createNewFile()) {
            throw new IOException("Could not create file at " + b2.getAbsolutePath());
        }
        try {
            BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(new C0013b(new FileOutputStream(b2), new f() {
                public void a() {
                    b.this.a(str, b2);
                }
            }), 8192);
            try {
                JSONObject jSONObject = new JSONObject();
                jSONObject.put("key", str);
                if (!g.a(str2)) {
                    jSONObject.put("tag", str2);
                }
                g.a(bufferedOutputStream, jSONObject);
                return bufferedOutputStream;
            } catch (JSONException e2) {
                c.a(m.CACHE, 5, a, "Error creating JSON header for cache file: " + e2);
                throw new IOException(e2.getMessage());
            } catch (Throwable th) {
                bufferedOutputStream.close();
                throw th;
            }
        } catch (FileNotFoundException e3) {
            c.a(m.CACHE, 5, a, "Error creating buffer output stream: " + e3);
            throw new IOException(e3.getMessage());
        }
    }

    /* access modifiers changed from: private */
    public void a(String str, File file) {
        if (!file.renameTo(new File(this.e, g.b(str)))) {
            file.delete();
        }
        b();
    }

    public InputStream a(String str, InputStream inputStream) throws IOException {
        return new c(inputStream, b(str));
    }

    public String toString() {
        return "{FileLruCache: tag:" + this.c + " file:" + this.e.getName() + "}";
    }

    private void b() {
        synchronized (this.g) {
            if (!this.f) {
                this.f = true;
                w.a().execute(new Runnable() {
                    public void run() {
                        b.this.c();
                    }
                });
            }
        }
    }

    /* access modifiers changed from: private */
    public void c() {
        try {
            c.a(m.CACHE, a, "trim started");
            PriorityQueue priorityQueue = new PriorityQueue();
            File[] listFiles = this.e.listFiles(a.a());
            int length = listFiles.length;
            long j = 0;
            long j2 = 0;
            int i = 0;
            while (i < length) {
                File file = listFiles[i];
                e eVar = new e(file);
                priorityQueue.add(eVar);
                c.a(m.CACHE, a, "  trim considering time=" + Long.valueOf(eVar.b()) + " name=" + eVar.a().getName());
                i++;
                j++;
                j2 = file.length() + j2;
            }
            long j3 = j2;
            long j4 = j;
            while (true) {
                if (j3 > ((long) this.d.a()) || j4 > ((long) this.d.b())) {
                    File a2 = ((e) priorityQueue.remove()).a();
                    c.a(m.CACHE, a, "  trim removing " + a2.getName());
                    j3 -= a2.length();
                    long j5 = j4 - 1;
                    a2.delete();
                    j4 = j5;
                } else {
                    synchronized (this.g) {
                        this.f = false;
                        this.g.notifyAll();
                    }
                    return;
                }
            }
        } catch (Throwable th) {
            synchronized (this.g) {
                this.f = false;
                this.g.notifyAll();
                throw th;
            }
        }
    }

    /* compiled from: FileLruCache */
    private static class a {
        private static final FilenameFilter a = new FilenameFilter() {
            public boolean accept(File file, String str) {
                return !str.startsWith("buffer");
            }
        };
        private static final FilenameFilter b = new FilenameFilter() {
            public boolean accept(File file, String str) {
                return str.startsWith("buffer");
            }
        };

        static void a(File file) {
            for (File delete : file.listFiles(b())) {
                delete.delete();
            }
        }

        static FilenameFilter a() {
            return a;
        }

        static FilenameFilter b() {
            return b;
        }

        static File b(File file) {
            return new File(file, "buffer" + Long.valueOf(b.b.incrementAndGet()).toString());
        }
    }

    /* compiled from: FileLruCache */
    private static final class g {
        static void a(OutputStream outputStream, JSONObject jSONObject) throws IOException {
            byte[] bytes = jSONObject.toString().getBytes();
            outputStream.write(0);
            outputStream.write((bytes.length >> 16) & 255);
            outputStream.write((bytes.length >> 8) & 255);
            outputStream.write((bytes.length >> 0) & 255);
            outputStream.write(bytes);
        }

        static JSONObject a(InputStream inputStream) throws IOException {
            int i = 0;
            if (inputStream.read() != 0) {
                return null;
            }
            int i2 = 0;
            for (int i3 = 0; i3 < 3; i3++) {
                int read = inputStream.read();
                if (read == -1) {
                    c.a(m.CACHE, b.a, "readHeader: stream.read returned -1 while reading header size");
                    return null;
                }
                i2 = (i2 << 8) + (read & 255);
            }
            byte[] bArr = new byte[i2];
            while (i < bArr.length) {
                int read2 = inputStream.read(bArr, i, bArr.length - i);
                if (read2 < 1) {
                    c.a(m.CACHE, b.a, "readHeader: stream.read stopped at " + Integer.valueOf(i) + " when expected " + bArr.length);
                    return null;
                }
                i += read2;
            }
            try {
                Object nextValue = new JSONTokener(new String(bArr)).nextValue();
                if (nextValue instanceof JSONObject) {
                    return (JSONObject) nextValue;
                }
                c.a(m.CACHE, b.a, "readHeader: expected JSONObject, got " + nextValue.getClass().getCanonicalName());
                return null;
            } catch (JSONException e) {
                throw new IOException(e.getMessage());
            }
        }
    }

    /* renamed from: com.facebook.b.b$b  reason: collision with other inner class name */
    /* compiled from: FileLruCache */
    private static class C0013b extends OutputStream {
        final OutputStream a;
        final f b;

        C0013b(OutputStream outputStream, f fVar) {
            this.a = outputStream;
            this.b = fVar;
        }

        public void close() throws IOException {
            try {
                this.a.close();
            } finally {
                this.b.a();
            }
        }

        public void flush() throws IOException {
            this.a.flush();
        }

        public void write(byte[] bArr, int i, int i2) throws IOException {
            this.a.write(bArr, i, i2);
        }

        public void write(byte[] bArr) throws IOException {
            this.a.write(bArr);
        }

        public void write(int i) throws IOException {
            this.a.write(i);
        }
    }

    /* compiled from: FileLruCache */
    private static final class c extends InputStream {
        final InputStream a;
        final OutputStream b;

        c(InputStream inputStream, OutputStream outputStream) {
            this.a = inputStream;
            this.b = outputStream;
        }

        public int available() throws IOException {
            return this.a.available();
        }

        public void close() throws IOException {
            try {
                this.a.close();
            } finally {
                this.b.close();
            }
        }

        public void mark(int i) {
            throw new UnsupportedOperationException();
        }

        public boolean markSupported() {
            return false;
        }

        public int read(byte[] bArr) throws IOException {
            int read = this.a.read(bArr);
            if (read > 0) {
                this.b.write(bArr, 0, read);
            }
            return read;
        }

        public int read() throws IOException {
            int read = this.a.read();
            if (read >= 0) {
                this.b.write(read);
            }
            return read;
        }

        public int read(byte[] bArr, int i, int i2) throws IOException {
            int read = this.a.read(bArr, i, i2);
            if (read > 0) {
                this.b.write(bArr, i, read);
            }
            return read;
        }

        public synchronized void reset() {
            throw new UnsupportedOperationException();
        }

        public long skip(long j) throws IOException {
            int read;
            byte[] bArr = new byte[1024];
            long j2 = 0;
            while (j2 < j && (read = read(bArr, 0, (int) Math.min(j - j2, (long) bArr.length))) >= 0) {
                j2 += (long) read;
            }
            return j2;
        }
    }

    /* compiled from: FileLruCache */
    public static final class d {
        private int a = 1048576;
        private int b = 1024;

        /* access modifiers changed from: package-private */
        public int a() {
            return this.a;
        }

        /* access modifiers changed from: package-private */
        public int b() {
            return this.b;
        }
    }

    /* compiled from: FileLruCache */
    private static final class e implements Comparable<e> {
        private final File a;
        private final long b;

        e(File file) {
            this.a = file;
            this.b = file.lastModified();
        }

        /* access modifiers changed from: package-private */
        public File a() {
            return this.a;
        }

        /* access modifiers changed from: package-private */
        public long b() {
            return this.b;
        }

        /* renamed from: a */
        public int compareTo(e eVar) {
            if (b() < eVar.b()) {
                return -1;
            }
            if (b() > eVar.b()) {
                return 1;
            }
            return a().compareTo(eVar.a());
        }

        public boolean equals(Object obj) {
            return (obj instanceof e) && compareTo((e) obj) == 0;
        }
    }
}
