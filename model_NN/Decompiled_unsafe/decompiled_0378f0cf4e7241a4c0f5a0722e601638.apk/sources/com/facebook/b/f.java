package com.facebook.b;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import com.facebook.s;
import com.facebook.v;

/* compiled from: SessionTracker */
public class f {
    /* access modifiers changed from: private */
    public s a;
    /* access modifiers changed from: private */
    public final s.f b;
    private final BroadcastReceiver c;
    private final android.support.v4.a.b d;
    private boolean e = false;

    public f(Context context, s.f fVar, s sVar, boolean z) {
        this.b = new b(fVar);
        this.a = sVar;
        this.c = new a(this, null);
        this.d = android.support.v4.a.b.a(context);
        if (z) {
            c();
        }
    }

    public s a() {
        return this.a == null ? s.j() : this.a;
    }

    public s b() {
        s a2 = a();
        if (a2 == null || !a2.b()) {
            return null;
        }
        return a2;
    }

    public void a(s sVar) {
        if (sVar != null) {
            if (this.a == null) {
                s j = s.j();
                if (j != null) {
                    j.b(this.b);
                }
                this.d.a(this.c);
            } else {
                this.a.b(this.b);
            }
            this.a = sVar;
            this.a.a(this.b);
        } else if (this.a != null) {
            this.a.b(this.b);
            this.a = null;
            f();
            if (a() != null) {
                a().a(this.b);
            }
        }
    }

    public void c() {
        if (!this.e) {
            if (this.a == null) {
                f();
            }
            if (a() != null) {
                a().a(this.b);
            }
            this.e = true;
        }
    }

    public void d() {
        if (this.e) {
            s a2 = a();
            if (a2 != null) {
                a2.b(this.b);
            }
            this.d.a(this.c);
            this.e = false;
        }
    }

    public boolean e() {
        return this.e;
    }

    private void f() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.facebook.sdk.ACTIVE_SESSION_SET");
        intentFilter.addAction("com.facebook.sdk.ACTIVE_SESSION_UNSET");
        this.d.a(this.c, intentFilter);
    }

    /* compiled from: SessionTracker */
    private class a extends BroadcastReceiver {
        private a() {
        }

        /* synthetic */ a(f fVar, a aVar) {
            this();
        }

        public void onReceive(Context context, Intent intent) {
            s j;
            if ("com.facebook.sdk.ACTIVE_SESSION_SET".equals(intent.getAction()) && (j = s.j()) != null) {
                j.a(f.this.b);
            }
        }
    }

    /* compiled from: SessionTracker */
    private class b implements s.f {
        private final s.f b;

        public b(s.f fVar) {
            this.b = fVar;
        }

        public void a(s sVar, v vVar, Exception exc) {
            if (this.b != null && f.this.e()) {
                this.b.a(sVar, vVar, exc);
            }
            if (sVar == f.this.a && vVar.b()) {
                f.this.a((s) null);
            }
        }
    }
}
