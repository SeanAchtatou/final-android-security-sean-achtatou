package com.facebook.b;

import android.util.Log;
import com.facebook.m;
import com.facebook.w;
import java.util.HashMap;
import java.util.Map;

/* compiled from: Logger */
public class c {
    private static final HashMap<String, String> a = new HashMap<>();
    private final m b;
    private final String c;
    private StringBuilder d;
    private int e = 3;

    public static synchronized void a(String str, String str2) {
        synchronized (c.class) {
            a.put(str, str2);
        }
    }

    public static synchronized void a(String str) {
        synchronized (c.class) {
            if (!w.a(m.INCLUDE_ACCESS_TOKENS)) {
                a(str, "ACCESS_TOKEN_REMOVED");
            }
        }
    }

    public static void a(m mVar, String str, String str2) {
        a(mVar, 3, str, str2);
    }

    public static void a(m mVar, String str, String str2, Object... objArr) {
        if (w.a(mVar)) {
            a(mVar, 3, str, String.format(str2, objArr));
        }
    }

    public static void a(m mVar, int i, String str, String str2) {
        if (w.a(mVar)) {
            String d2 = d(str2);
            if (!str.startsWith("FacebookSDK.")) {
                str = "FacebookSDK." + str;
            }
            Log.println(i, str, d2);
        }
    }

    private static synchronized String d(String str) {
        synchronized (c.class) {
            for (Map.Entry next : a.entrySet()) {
                str = str.replace((CharSequence) next.getKey(), (CharSequence) next.getValue());
            }
        }
        return str;
    }

    public c(m mVar, String str) {
        h.a(str, "tag");
        this.b = mVar;
        this.c = "FacebookSDK." + str;
        this.d = new StringBuilder();
    }

    public void a() {
        b(this.d.toString());
        this.d = new StringBuilder();
    }

    public void b(String str) {
        a(this.b, this.e, this.c, str);
    }

    public void c(String str) {
        if (b()) {
            this.d.append(str);
        }
    }

    public void a(String str, Object... objArr) {
        if (b()) {
            this.d.append(String.format(str, objArr));
        }
    }

    public void a(String str, Object obj) {
        a("  %s:\t%s\n", str, obj);
    }

    private boolean b() {
        return w.a(this.b);
    }
}
