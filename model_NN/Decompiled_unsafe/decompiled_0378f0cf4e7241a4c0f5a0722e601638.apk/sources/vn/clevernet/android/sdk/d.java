package vn.clevernet.android.sdk;

import android.content.Context;
import android.view.MotionEvent;
import android.webkit.WebView;
import vn.clevernet.android.sdk.ClevernetView;

final class d extends WebView {
    private b a;
    private ClevernetView.b b;

    interface a {
        void a();
    }

    public d(Context context, b bVar, a aVar, ClevernetView.b bVar2) {
        super(context);
        this.b = bVar2;
        this.a = bVar;
        setVerticalScrollBarEnabled(false);
        setHorizontalScrollBarEnabled(false);
        setScrollBarStyle(33554432);
        setBackgroundColor(0);
        setWebViewClient(new e(this, aVar));
        loadDataWithBaseURL(null, this.a.d(), "text/html", "UTF-8", null);
    }

    public d(Context context, int i, int i2, b bVar, a aVar, ClevernetView.b bVar2) {
        super(context);
        this.b = bVar2;
        this.a = bVar;
        setVerticalScrollBarEnabled(false);
        setHorizontalScrollBarEnabled(false);
        setScrollBarStyle(33554432);
        setBackgroundColor(0);
        setWebViewClient(new f(this, aVar));
        StringBuilder sb = new StringBuilder();
        sb.append("<html><head><style>* {margin:0;padding:0;}</style></head><body>").append("<img src=\"" + this.a.b() + "\" height=\"" + i2 + "\" width=\"" + i + "\"/>").append("<img src=\"" + this.a.h() + "\" height=\"0\" width=\"0" + "\"/>").append("</html></head>");
        loadDataWithBaseURL(null, sb.toString(), "text/html", "UTF-8", null);
    }

    public final boolean dispatchTouchEvent(MotionEvent motionEvent) {
        if (motionEvent.getAction() != 0) {
            return super.dispatchTouchEvent(motionEvent);
        }
        this.a.a();
        return true;
    }

    /* access modifiers changed from: protected */
    public final void onAnimationEnd() {
        super.onAnimationEnd();
        if (this.b != null) {
            this.b.a();
        }
    }
}
