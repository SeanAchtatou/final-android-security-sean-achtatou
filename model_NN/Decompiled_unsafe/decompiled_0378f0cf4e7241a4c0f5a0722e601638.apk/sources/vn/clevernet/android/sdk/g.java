package vn.clevernet.android.sdk;

import android.content.Context;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.webkit.WebView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import vn.clevernet.android.sdk.ClevernetView;

final class g extends RelativeLayout {
    private ClevernetView.b a;
    /* access modifiers changed from: private */
    public int b;
    /* access modifiers changed from: private */
    public int c;
    private float d = getContext().getApplicationContext().getApplicationContext().getResources().getDisplayMetrics().density;
    private String e;

    public g(Context context, b bVar, int i, int i2, ClevernetView.b bVar2) {
        super(context);
        this.e = bVar.d();
        this.a = bVar2;
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
        TextView textView = new TextView(getContext().getApplicationContext());
        textView.setGravity(17);
        textView.setText(this.e);
        textView.setTextSize((float) i);
        textView.setTextColor(i2);
        textView.setTypeface(Typeface.DEFAULT_BOLD);
        textView.setLayoutParams(layoutParams);
        textView.setId(12345);
        textView.setMaxLines(2);
        addView(textView);
        WebView webView = new WebView(context);
        webView.setVerticalScrollBarEnabled(false);
        webView.setHorizontalScrollBarEnabled(false);
        webView.setScrollBarStyle(33554432);
        webView.setBackgroundColor(0);
        StringBuilder sb = new StringBuilder();
        sb.append("<html><head><style>* {margin:0;padding:0;}</style></head><body>").append("<img src=\"" + bVar.h() + "\" height=\"0\" width=\"0" + "\"/>").append("</html></head>");
        webView.loadDataWithBaseURL(null, sb.toString(), "text/html", "UTF-8", null);
        addView(webView);
        int i3 = (int) (320.0f * this.d);
        int i4 = (int) (53.0f * this.d);
        Paint paint = new Paint();
        paint.setTextSize((float) i);
        this.c = 1;
        if (this.e.indexOf("\n") > 0) {
            this.c = (this.e.length() - this.e.replaceAll("\\\n", "").length()) + this.c;
        } else if (paint.measureText(this.e) * this.d > ((float) i3)) {
            this.c++;
        }
        getViewTreeObserver().addOnGlobalLayoutListener(new h(this, textView, i4, i, i3));
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams2.leftMargin = (int) (235.0f * this.d);
        layoutParams2.topMargin = (int) (39.0f * this.d);
        TextView textView2 = new TextView(getContext().getApplicationContext());
        textView2.setText("Ads by CleverAds");
        textView2.setTextSize(10.0f);
        textView2.setLayoutParams(layoutParams2);
        addView(textView2);
    }

    /* access modifiers changed from: protected */
    public final void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
        setMeasuredDimension((int) (this.d * 320.0f), (int) (this.d * 320.0f));
    }

    /* access modifiers changed from: protected */
    public final void onAnimationEnd() {
        super.onAnimationEnd();
        if (this.a != null) {
            this.a.a();
        }
    }
}
