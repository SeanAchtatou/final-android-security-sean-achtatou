package vn.clevernet.android.sdk;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.FrameLayout;
import java.lang.ref.SoftReference;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import org.json.JSONObject;

public class ClevernetView extends FrameLayout {
    private static List<SoftReference<b>> F = new ArrayList();
    private static int G = -1;
    private static boolean H = false;
    /* access modifiers changed from: private */
    public static boolean a = false;
    /* access modifiers changed from: private */
    public static BitmapDrawable c;
    /* access modifiers changed from: private */
    public static int e = 0;
    /* access modifiers changed from: private */
    public static int g = 0;
    /* access modifiers changed from: private */
    public static int j = 0;
    private static int q;
    private static int r;
    private Drawable A;
    private Thread B;
    private float C;
    private List<View> D;
    private b E;
    /* access modifiers changed from: private */
    public Rect I;
    /* access modifiers changed from: private */
    public final Runnable J;
    /* access modifiers changed from: private */
    public b b;
    /* access modifiers changed from: private */
    public int d;
    /* access modifiers changed from: private */
    public int f;
    /* access modifiers changed from: private */
    public int h;
    /* access modifiers changed from: private */
    public int i;
    /* access modifiers changed from: private */
    public String k;
    private String l;
    private String m;
    private boolean n;
    private int o;
    private int p;
    private int s;
    private int t;
    private int u;
    /* access modifiers changed from: private */
    public a v;
    /* access modifiers changed from: private */
    public Timer w;
    /* access modifiers changed from: private */
    public String x;
    /* access modifiers changed from: private */
    public final Handler y;
    private boolean z;

    public interface a {
        void a(int i, String str);

        void a(Exception exc);

        void a(boolean z, ClevernetView clevernetView);
    }

    interface b {
        final /* synthetic */ ClevernetView a;

        default b(ClevernetView clevernetView) {
            this.a = clevernetView;
        }

        default void a() {
            this.a.y.sendEmptyMessage(2);
        }
    }

    public ClevernetView(Context context) {
        this(context, null);
    }

    public ClevernetView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.d = -1;
        this.f = 0;
        this.h = 10;
        this.i = 0;
        this.k = "mma";
        this.l = "mma";
        this.m = "fade";
        this.n = false;
        this.o = 18;
        this.p = 0;
        this.s = 0;
        this.t = 0;
        this.u = 0;
        this.v = null;
        this.w = null;
        this.x = "";
        this.y = new j(this);
        this.z = false;
        this.A = null;
        this.D = new ArrayList();
        this.E = new b(this);
        this.J = new k(this);
        setVisibility(8);
        if (context.checkCallingOrSelfPermission("android.permission.INTERNET") == -1) {
            throw new IllegalArgumentException();
        }
        this.C = getContext().getApplicationContext().getApplicationContext().getResources().getDisplayMetrics().density;
        if (attributeSet != null) {
            String str = "http://schemas.android.com/apk/res/" + getContext().getApplicationContext().getPackageName();
            this.f = attributeSet.getAttributeIntValue(str, "backgroundColor", 0);
            if (attributeSet.getAttributeValue(str, "bannerType") != null) {
                this.k = attributeSet.getAttributeValue(str, "bannerType");
            }
            if (attributeSet.getAttributeValue(str, "animation") != null) {
                this.m = attributeSet.getAttributeValue(str, "animation");
            }
            this.n = attributeSet.getAttributeBooleanValue(str, "deliverOnlyText", false);
            if (!this.k.equals("mma") && this.n) {
                this.n = false;
            }
            this.o = attributeSet.getAttributeIntValue(str, "textSize", 18);
            if (this.o > 20) {
                this.o = 20;
            } else if (this.o < 10) {
                this.o = 10;
            }
        }
        if (this.h < 12 || this.h > 60) {
            this.h = 10;
        }
        DisplayMetrics displayMetrics = context.getApplicationContext().getResources().getDisplayMetrics();
        this.A = getBackground();
        this.I = new Rect(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
        if (c == null) {
            c = a(this.I, this.f, 16777215);
        }
        setClickable(true);
        setFocusable(true);
        setDescendantFocusability(131072);
        if (this.B == null || !this.B.isAlive()) {
            c(false);
        }
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (this.b == null) {
            return true;
        }
        this.b.a();
        return true;
    }

    private void g() {
        Context applicationContext = getContext().getApplicationContext();
        int i2 = this.u;
        int i3 = this.t;
        d dVar = new d(applicationContext, this.b, new l(this), this.E);
        j();
        addView(dVar);
        Animation a2 = a(false);
        if (a2 != null) {
            dVar.startAnimation(a2);
        }
    }

    static /* synthetic */ void c(ClevernetView clevernetView) {
        clevernetView.setBackgroundDrawable(clevernetView.A);
        if (clevernetView.b != null) {
            String lowerCase = clevernetView.b.c().trim().toLowerCase();
            if (lowerCase.equals("call") || lowerCase.equals("sms") || lowerCase.equals("demand") || lowerCase.equals("action")) {
                if (clevernetView.l.trim().toLowerCase().equals("mma")) {
                    clevernetView.h();
                }
                if (clevernetView.l.trim().toLowerCase().equals("mix")) {
                    clevernetView.g();
                }
                if (clevernetView.l.trim().toLowerCase().equals("txt")) {
                    clevernetView.i();
                    clevernetView.setVisibility(0);
                }
                clevernetView.b(true);
            } else if (clevernetView.b.c().trim().toLowerCase().equals("mix")) {
                clevernetView.g();
                clevernetView.b(true);
            } else {
                if (!clevernetView.b.e() || clevernetView.n) {
                    clevernetView.i();
                    clevernetView.setVisibility(0);
                } else {
                    clevernetView.h();
                }
                clevernetView.b(true);
            }
        } else {
            clevernetView.removeAllViews();
            clevernetView.b(false);
            clevernetView.setVisibility(8);
        }
    }

    private void h() {
        d dVar = new d(getContext().getApplicationContext(), this.u, this.t, this.b, new m(this), this.E);
        j();
        addView(dVar);
        Animation a2 = a(false);
        if (a2 != null) {
            dVar.startAnimation(a2);
        }
    }

    private void i() {
        setBackgroundDrawable(c);
        g gVar = new g(getContext().getApplicationContext(), this.b, this.o, this.d, this.E);
        j();
        addView(gVar);
        Animation a2 = a(false);
        if (a2 != null) {
            gVar.startAnimation(a2);
        }
    }

    static /* synthetic */ void a(ClevernetView clevernetView) {
        for (View removeView : clevernetView.D) {
            clevernetView.removeView(removeView);
        }
    }

    private void j() {
        int childCount = getChildCount();
        if (childCount > 0) {
            Animation a2 = a(true);
            for (int i2 = 0; i2 < childCount; i2++) {
                if (!(a2 == null || getChildAt(i2) == null)) {
                    getChildAt(i2).setAnimation(a2);
                    this.D.add(getChildAt(i2));
                }
            }
        }
    }

    private Animation a(boolean z2) {
        if (z2) {
            if (this.m != null && this.m.equals("fade")) {
                AlphaAnimation alphaAnimation = new AlphaAnimation(1.0f, 0.0f);
                alphaAnimation.setDuration(700);
                return alphaAnimation;
            } else if (this.m != null && this.m.equals("left_to_right")) {
                TranslateAnimation translateAnimation = new TranslateAnimation(2, 0.0f, 2, 1.0f, 2, 0.0f, 2, 0.0f);
                translateAnimation.setDuration(900);
                translateAnimation.setInterpolator(new AccelerateInterpolator());
                return translateAnimation;
            } else if (this.m == null || !this.m.equals("top_down")) {
                return null;
            } else {
                TranslateAnimation translateAnimation2 = new TranslateAnimation(2, 0.0f, 2, 0.0f, 2, 0.0f, 2, 1.0f);
                translateAnimation2.setDuration(900);
                translateAnimation2.setInterpolator(new AccelerateInterpolator());
                return translateAnimation2;
            }
        } else if (this.m != null && this.m.equals("fade")) {
            AlphaAnimation alphaAnimation2 = new AlphaAnimation(0.0f, 1.0f);
            alphaAnimation2.setDuration(1200);
            return alphaAnimation2;
        } else if (this.m != null && this.m.equals("left_to_right")) {
            TranslateAnimation translateAnimation3 = new TranslateAnimation(2, -1.0f, 2, 0.0f, 2, 0.0f, 2, 0.0f);
            translateAnimation3.setDuration(900);
            translateAnimation3.setInterpolator(new AccelerateInterpolator());
            return translateAnimation3;
        } else if (this.m == null || !this.m.equals("top_down")) {
            return null;
        } else {
            TranslateAnimation translateAnimation4 = new TranslateAnimation(2, 0.0f, 2, 0.0f, 2, -1.0f, 2, 0.0f);
            translateAnimation4.setDuration(900);
            translateAnimation4.setInterpolator(new AccelerateInterpolator());
            return translateAnimation4;
        }
    }

    private void b(boolean z2) {
        if (this.v != null) {
            this.v.a(z2, this);
        }
    }

    public void setVisibility(int i2) {
        if (super.getVisibility() != i2) {
            synchronized (this) {
                int childCount = getChildCount();
                for (int i3 = 0; i3 < childCount; i3++) {
                    getChildAt(i3).setVisibility(i2);
                }
                super.setVisibility(i2);
            }
        }
    }

    /* access modifiers changed from: private */
    public void c(boolean z2) {
        b bVar;
        if (!z2) {
            int i2 = 0;
            if (this.k == null || !this.k.equals("all") || F.size() <= 0) {
                while (true) {
                    int i3 = i2;
                    if (i3 < F.size()) {
                        l();
                        SoftReference softReference = F.get(G);
                        if (softReference != null && softReference.get() != null && this.k != null && this.k.contains(((b) softReference.get()).g())) {
                            this.k = ((b) softReference.get()).g();
                            bVar = (b) softReference.get();
                            break;
                        }
                        i2 = i3 + 1;
                    } else {
                        break;
                    }
                }
            } else {
                l();
                SoftReference softReference2 = F.get(G);
                if (softReference2 != null) {
                    this.k = ((b) softReference2.get()).g();
                    bVar = (b) softReference2.get();
                }
                bVar = null;
            }
            this.b = bVar;
        }
        if (this.b == null || z2) {
            this.B = new Thread(new n(this), "CleverNetRequestThread");
            this.B.start();
            return;
        }
        k();
        this.y.post(this.J);
    }

    static /* synthetic */ void a(ClevernetView clevernetView, JSONObject jSONObject) {
        try {
            if (jSONObject.has("banner_type")) {
                clevernetView.k = jSONObject.getString("banner_type");
            }
            if (jSONObject.has("zone_type")) {
                clevernetView.l = jSONObject.getString("zone_type");
            }
        } catch (Exception e2) {
        }
    }

    /* access modifiers changed from: private */
    public void k() {
        if (H) {
            this.p = (int) ((this.C * ((float) q)) + 0.5f);
            this.s = (int) ((this.C * ((float) r)) + 0.5f);
            this.t = q;
            this.u = r;
        } else if (this.b != null) {
            if (!this.b.e() || this.n) {
                this.p = (int) ((this.C * 53.0f) + 0.5f);
                this.s = (int) ((this.C * 320.0f) + 0.5f);
                this.t = 53;
                this.u = 320;
            } else {
                this.p = (int) ((this.C * ((float) this.b.l())) + 0.5f);
                this.s = (int) ((this.C * ((float) this.b.m())) + 0.5f);
                this.t = this.b.l();
                this.u = this.b.m();
            }
            H = false;
        }
        this.u = 856;
        DisplayMetrics displayMetrics = getContext().getApplicationContext().getApplicationContext().getResources().getDisplayMetrics();
        int i2 = this.p;
        int i3 = this.s;
        if (displayMetrics.heightPixels < this.p) {
            i2 = displayMetrics.heightPixels;
        }
        if (displayMetrics.widthPixels < this.s) {
            i3 = displayMetrics.widthPixels;
        }
        if (this.k == null || !this.k.equals("fullscreen")) {
            float f2 = ((float) this.p) / ((float) i2);
            float f3 = ((float) this.s) / ((float) i3);
            if (f2 > f3) {
                this.s = (int) (((float) this.s) / f2);
                this.p = i2;
                this.u = (int) (((float) this.s) / this.C);
                this.t = (int) (((float) i2) / this.C);
                return;
            }
            this.s = i3;
            this.p = (int) (((float) this.p) / f3);
            this.u = (int) (((float) i3) / this.C);
            this.t = (int) (((float) this.p) / this.C);
        } else if (i2 < i3) {
            this.s = i2;
            this.p = i2;
            this.u = (int) (((float) i2) / this.C);
            this.t = (int) (((float) i2) / this.C);
        } else {
            this.s = i3;
            this.p = i3;
            this.u = (int) (((float) i3) / this.C);
            this.t = (int) (((float) i3) / this.C);
        }
    }

    public void onWindowFocusChanged(boolean z2) {
        d(z2);
        super.onWindowFocusChanged(z2);
        getParent();
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        d(true);
        super.onAttachedToWindow();
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        d(false);
        super.onDetachedFromWindow();
    }

    /* access modifiers changed from: private */
    public BitmapDrawable a(Rect rect, int i2, int i3) {
        try {
            Bitmap createBitmap = Bitmap.createBitmap(rect.width(), rect.height(), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(createBitmap);
            Paint paint = new Paint();
            paint.setColor(i2);
            paint.setAntiAlias(true);
            canvas.drawRect(rect, paint);
            GradientDrawable gradientDrawable = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[]{Color.argb(127, Color.red(i3), Color.green(i3), Color.blue(i3)), i3});
            int height = ((int) (((double) rect.height()) * 0.7375d)) + rect.top;
            gradientDrawable.setBounds(rect.left, rect.top, rect.right, height);
            gradientDrawable.draw(canvas);
            Rect rect2 = new Rect(rect.left, height, rect.right, rect.bottom);
            Paint paint2 = new Paint();
            paint2.setColor(i3);
            canvas.drawRect(rect2, paint2);
            return new BitmapDrawable(createBitmap);
        } catch (Throwable th) {
            return null;
        }
    }

    public void a() {
        if (this.B != null && this.B.isAlive()) {
            this.B.interrupt();
        }
    }

    private void d(boolean z2) {
        synchronized (this) {
            if (z2) {
                try {
                    if (this.w == null && !a) {
                        this.w = new Timer();
                        this.w.schedule(new p(this), ((long) this.h) * 1000, ((long) this.h) * 1000);
                    }
                } catch (Exception e2) {
                }
            } else if (this.w != null) {
                this.w.cancel();
                this.w = null;
                a();
                a(this.b);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        super.onMeasure(i2, i3);
        View.MeasureSpec.getSize(i2);
        View.MeasureSpec.getSize(i3);
        setMeasuredDimension(this.s, this.p);
    }

    /* access modifiers changed from: private */
    public static void a(b bVar) {
        if (bVar != null) {
            if (F.size() >= 4) {
                F.remove(0);
            }
            F.add(new SoftReference(bVar));
        }
    }

    private static void l() {
        int i2 = G + 1;
        G = i2;
        if (i2 == F.size()) {
            G = 0;
        }
    }

    public static void setWidth(int i2) {
        H = true;
        r = i2;
    }

    public static void setHeight(int i2) {
        H = true;
        q = i2;
    }

    public static void setRefreshTime(int i2) {
        if (i2 < 12 || i2 > 60) {
            j = 10;
        } else {
            j = i2;
        }
    }

    public static void setTextColor(String str) {
        e = Color.parseColor(str);
    }

    public void setZoneAppID(String str) {
        this.x = str;
    }

    public static void setTextBackgroundColor(String str) {
        g = Color.parseColor(str);
    }

    public static void setGender(String str) {
        if (!str.equals("F")) {
            str.equals("M");
        }
    }

    public static void setAge(String str) {
    }

    public void setCleverNetViewCallbackListener(a aVar) {
        this.v = aVar;
    }
}
