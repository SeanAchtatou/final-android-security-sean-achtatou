package vn.clevernet.android.sdk;

final class c implements Runnable {
    private /* synthetic */ b a;
    private final /* synthetic */ String b;

    c(b bVar, String str) {
        this.a = bVar;
        this.b = str;
    }

    /* JADX WARNING: Unknown top exception splitter block from list: {B:17:0x0055=Splitter:B:17:0x0055, B:32:0x006b=Splitter:B:32:0x006b} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void run() {
        /*
            r4 = this;
            org.apache.http.client.methods.HttpPost r2 = new org.apache.http.client.methods.HttpPost     // Catch:{ Exception -> 0x006f }
            java.lang.String r0 = r4.b     // Catch:{ Exception -> 0x006f }
            r2.<init>(r0)     // Catch:{ Exception -> 0x006f }
            java.lang.String r0 = "Content-Type"
            java.lang.String r1 = "application/x-www-form-urlencoded; charset=utf-8"
            r2.setHeader(r0, r1)     // Catch:{ Exception -> 0x006f }
            java.util.ArrayList r3 = new java.util.ArrayList     // Catch:{ Exception -> 0x006f }
            r3.<init>()     // Catch:{ Exception -> 0x006f }
            vn.clevernet.android.sdk.b r0 = r4.a     // Catch:{ Exception -> 0x006f }
            android.content.Context r0 = r0.p     // Catch:{ Exception -> 0x006f }
            vn.clevernet.android.sdk.a.a(r0, r3)     // Catch:{ Exception -> 0x006f }
            r1 = 0
            org.apache.http.client.entity.UrlEncodedFormEntity r0 = new org.apache.http.client.entity.UrlEncodedFormEntity     // Catch:{ UnsupportedEncodingException -> 0x0057 }
            r0.<init>(r3)     // Catch:{ UnsupportedEncodingException -> 0x0057 }
        L_0x0022:
            r2.setEntity(r0)     // Catch:{ Exception -> 0x006f }
            monitor-enter(r4)     // Catch:{ Exception -> 0x006f }
            org.apache.http.impl.client.DefaultHttpClient r0 = new org.apache.http.impl.client.DefaultHttpClient     // Catch:{ all -> 0x006c }
            r0.<init>()     // Catch:{ all -> 0x006c }
            org.apache.http.params.HttpParams r1 = r0.getParams()     // Catch:{ Exception -> 0x005d }
            r3 = 10000(0x2710, float:1.4013E-41)
            org.apache.http.params.HttpConnectionParams.setConnectionTimeout(r1, r3)     // Catch:{ Exception -> 0x005d }
            r3 = 10000(0x2710, float:1.4013E-41)
            org.apache.http.params.HttpConnectionParams.setSoTimeout(r1, r3)     // Catch:{ Exception -> 0x005d }
            org.apache.http.HttpResponse r0 = r0.execute(r2)     // Catch:{ Exception -> 0x005d }
            org.apache.http.StatusLine r1 = r0.getStatusLine()     // Catch:{ Exception -> 0x005d }
            int r1 = r1.getStatusCode()     // Catch:{ Exception -> 0x005d }
            org.apache.http.HttpEntity r0 = r0.getEntity()     // Catch:{ Exception -> 0x005d }
            r3 = 200(0xc8, float:2.8E-43)
            if (r1 != r3) goto L_0x0052
            if (r0 == 0) goto L_0x0052
            r0.getContent()     // Catch:{ Exception -> 0x005d }
        L_0x0052:
            r2.abort()     // Catch:{ Exception -> 0x0076 }
        L_0x0055:
            monitor-exit(r4)     // Catch:{ all -> 0x006c }
        L_0x0056:
            return
        L_0x0057:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ Exception -> 0x006f }
            r0 = r1
            goto L_0x0022
        L_0x005d:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ all -> 0x0067 }
            r2.abort()     // Catch:{ Exception -> 0x0065 }
            goto L_0x0055
        L_0x0065:
            r0 = move-exception
            goto L_0x0055
        L_0x0067:
            r0 = move-exception
            r2.abort()     // Catch:{ Exception -> 0x0074 }
        L_0x006b:
            throw r0     // Catch:{ all -> 0x006c }
        L_0x006c:
            r0 = move-exception
            monitor-exit(r4)     // Catch:{ Exception -> 0x006f }
            throw r0     // Catch:{ Exception -> 0x006f }
        L_0x006f:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0056
        L_0x0074:
            r1 = move-exception
            goto L_0x006b
        L_0x0076:
            r0 = move-exception
            goto L_0x0055
        */
        throw new UnsupportedOperationException("Method not decompiled: vn.clevernet.android.sdk.c.run():void");
    }
}
