package vn.clevernet.android.sdk;

import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;

final class i implements LocationListener {
    private final /* synthetic */ LocationManager a;

    i(LocationManager locationManager) {
        this.a = locationManager;
    }

    public final void onLocationChanged(Location location) {
        Log.d("CAD_LOG", "Refreshing location");
        a.c = location;
        a.b = System.currentTimeMillis();
        this.a.removeUpdates(this);
    }

    public final void onProviderDisabled(String str) {
    }

    public final void onProviderEnabled(String str) {
    }

    public final void onStatusChanged(String str, int i, Bundle bundle) {
    }
}
