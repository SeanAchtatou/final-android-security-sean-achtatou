package vn.clevernet.android.sdk;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.telephony.PhoneStateListener;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;
import org.json.JSONException;
import org.json.JSONObject;
import vn.clevernet.android.sdk.ClevernetView;

final class b {
    private boolean a = false;
    private String b = "";
    private String c = "";
    private String d;
    private String e;
    private String f;
    private String g;
    private String h;
    private String i;
    private boolean j;
    private boolean k;
    private int l;
    private int m;
    private int n;
    private int o;
    /* access modifiers changed from: private */
    public Context p;
    private ClevernetView.a q;
    private Thread r;

    protected b(Context context, JSONObject jSONObject, ClevernetView.a aVar, String str) {
        String string;
        String string2;
        String string3;
        boolean z = false;
        this.p = context;
        this.q = aVar;
        this.i = str;
        try {
            this.e = jSONObject.isNull("click_url") ? "" : jSONObject.getString("click_url");
            this.f = jSONObject.isNull("banner_url") ? "" : jSONObject.getString("banner_url");
            this.g = jSONObject.isNull("text") ? "" : jSONObject.getString("text");
            this.h = jSONObject.isNull("beacon_url") ? "" : jSONObject.getString("beacon_url");
            this.d = jSONObject.isNull("refresh_rate") ? "" : jSONObject.getString("refresh_rate");
            if (jSONObject.isNull("has_banner")) {
                string = "true";
            } else {
                string = jSONObject.getString("has_banner");
            }
            this.j = Boolean.parseBoolean(string);
            if (jSONObject.isNull("has_ads")) {
                string2 = "false";
            } else {
                string2 = jSONObject.getString("has_ads");
            }
            this.a = Boolean.parseBoolean(string2);
            this.b = jSONObject.isNull("action_type") ? "" : jSONObject.getString("action_type");
            this.c = jSONObject.isNull("tel") ? "" : jSONObject.getString("tel");
            if (this.j) {
                if (this.f != null && !this.f.equals("")) {
                    z = true;
                }
                this.k = z;
                this.o = Integer.parseInt(jSONObject.isNull("banner_width") ? "0" : jSONObject.getString("banner_width"));
                if (jSONObject.isNull("banner_height")) {
                    string3 = "0";
                } else {
                    string3 = jSONObject.getString("banner_height");
                }
                this.n = Integer.parseInt(string3);
                return;
            }
            String string4 = jSONObject.isNull("text_color") ? "" : jSONObject.getString("text_color");
            if (!string4.equals("#-1")) {
                this.l = Color.parseColor(string4);
            } else {
                this.l = 0;
            }
            String string5 = jSONObject.isNull("background_color") ? "" : jSONObject.getString("background_color");
            if (!string5.equals("#-1")) {
                this.m = Color.parseColor(string5);
            } else {
                this.m = 0;
            }
            this.k = false;
        } catch (JSONException e2) {
            if (this.q != null) {
                this.q.a(e2);
            }
            e2.printStackTrace();
        }
    }

    private void a(String str) {
        this.r = new Thread(new c(this, str), "CleverNetRequestThread");
        this.r.start();
        try {
            this.r.join();
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    public final void a() {
        int i2 = 0;
        boolean z = true;
        if (this.b.trim().toLowerCase().equals("call")) {
            try {
                if (this.p.checkCallingOrSelfPermission("android.permission.CALL_PHONE") != 0) {
                    z = false;
                }
                if (z) {
                    a aVar = new a(this, (byte) 0);
                    aVar.a(this.p);
                    ((TelephonyManager) this.p.getSystemService("phone")).listen(aVar, 32);
                    Intent intent = new Intent("android.intent.action.CALL");
                    intent.setFlags(268435456);
                    intent.setData(Uri.parse("tel:" + this.c));
                    this.p.startActivity(intent);
                    a(this.e);
                }
            } catch (Exception e2) {
            }
        } else if (this.b.trim().toLowerCase().equals("sms")) {
            try {
                if (this.p.checkCallingOrSelfPermission("android.permission.SEND_SMS") == 0) {
                    i2 = 1;
                }
                if (i2 != 0) {
                    SmsManager.getDefault().sendTextMessage(this.c, null, this.g, null, null);
                    Toast.makeText(this.p.getApplicationContext(), "SMS Sent!", 1).show();
                    a(this.e);
                }
            } catch (Exception e3) {
                Toast.makeText(this.p.getApplicationContext(), "SMS faild, please try again later!", 1).show();
            }
        } else if (this.b.trim().toLowerCase().equals("demand")) {
            a(this.e);
        } else {
            String str = "";
            String[] split = this.e.trim().split("__");
            while (true) {
                if (i2 >= split.length) {
                    break;
                } else if (split[i2].trim().toLowerCase().startsWith("oadest")) {
                    String[] split2 = split[i2].trim().split("=");
                    if (split2.length >= 2) {
                        for (int i3 = 1; i3 < split2.length - 1; i3++) {
                            str = String.valueOf(str) + split2[i3] + "=";
                        }
                        str = String.valueOf(str) + split2[split2.length - 1];
                    }
                } else {
                    i2++;
                }
            }
            a(String.valueOf(this.e) + "&trackonly=1");
            if (str.trim().length() > 0) {
                Intent intent2 = new Intent("android.intent.action.VIEW", Uri.parse(str));
                intent2.addFlags(268435456);
                try {
                    this.p.startActivity(intent2);
                } catch (Exception e4) {
                    e4.printStackTrace();
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public final String b() {
        return this.f;
    }

    /* access modifiers changed from: protected */
    public final String c() {
        return this.b;
    }

    /* access modifiers changed from: protected */
    public final String d() {
        return this.g;
    }

    /* access modifiers changed from: protected */
    public final boolean e() {
        return this.k;
    }

    /* access modifiers changed from: protected */
    public final boolean f() {
        return this.a;
    }

    /* access modifiers changed from: protected */
    public final String g() {
        return this.i;
    }

    /* access modifiers changed from: protected */
    public final String h() {
        return this.h;
    }

    public final String i() {
        return this.d;
    }

    public final int j() {
        return this.l;
    }

    public final int k() {
        return this.m;
    }

    public final int l() {
        return this.n;
    }

    public final int m() {
        return this.o;
    }

    class a extends PhoneStateListener {
        private boolean a;
        private Context b;
        private String c;

        private a(b bVar) {
            this.a = false;
            this.c = "LOGGING PHONE CALL";
        }

        /* synthetic */ a(b bVar, byte b2) {
            this(bVar);
        }

        public final void a(Context context) {
            this.b = context;
        }

        public final void onCallStateChanged(int i, String str) {
            if (1 == i) {
                Log.i(this.c, "RINGING, number: " + str);
            }
            if (2 == i) {
                Log.i(this.c, "OFFHOOK");
                this.a = true;
            }
            if (i == 0) {
                Log.i(this.c, "IDLE");
                if (this.a) {
                    Log.i(this.c, "restart app");
                    Intent launchIntentForPackage = this.b.getPackageManager().getLaunchIntentForPackage(this.b.getPackageName());
                    launchIntentForPackage.addFlags(67108864);
                    this.b.startActivity(launchIntentForPackage);
                    this.a = false;
                }
            }
        }
    }
}
