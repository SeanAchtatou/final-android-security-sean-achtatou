package vn.clevernet.android.sdk;

import android.view.ViewTreeObserver;
import android.widget.RelativeLayout;
import android.widget.TextView;

final class h implements ViewTreeObserver.OnGlobalLayoutListener {
    private /* synthetic */ g a;
    private final /* synthetic */ TextView b;
    private final /* synthetic */ int c;
    private final /* synthetic */ int d;
    private final /* synthetic */ int e;

    h(g gVar, TextView textView, int i, int i2, int i3) {
        this.a = gVar;
        this.b = textView;
        this.c = i;
        this.d = i2;
        this.e = i3;
    }

    public final void onGlobalLayout() {
        this.a.b = this.b.getWidth();
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams.topMargin = (this.c / 2) - (this.a.c * this.d);
        layoutParams.leftMargin = (this.e / 2) - (this.a.b / 2);
        this.b.setLayoutParams(layoutParams);
    }
}
