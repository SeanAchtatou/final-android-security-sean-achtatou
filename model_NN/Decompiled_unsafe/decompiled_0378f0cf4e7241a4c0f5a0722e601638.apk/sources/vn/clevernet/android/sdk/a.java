package vn.clevernet.android.sdk;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Build;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Log;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import vn.clevernet.android.sdk.ClevernetView;

public class a {
    private static String a;
    /* access modifiers changed from: private */
    public static long b = 0;
    /* access modifiers changed from: private */
    public static Location c = null;
    private static Address d = null;
    private static String e = "";

    static String a(Context context, ClevernetView.a aVar) {
        String str;
        try {
            str = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128).metaData.getString("clevernet_site_token");
        } catch (Exception e2) {
            e2.printStackTrace();
            str = null;
        }
        if (str == null) {
            a(null, 3, "Could not fetch \"clevernet_site_token\" from AndroidManifest.xml");
            if (aVar != null) {
                aVar.a(new IllegalArgumentException("Could not fetch \"clevernet_site_token\" from AndroidManifest.xml"));
            }
        }
        return str;
    }

    private static String a() {
        try {
            Enumeration<NetworkInterface> networkInterfaces = NetworkInterface.getNetworkInterfaces();
            while (networkInterfaces.hasMoreElements()) {
                Enumeration<InetAddress> inetAddresses = networkInterfaces.nextElement().getInetAddresses();
                while (true) {
                    if (inetAddresses.hasMoreElements()) {
                        InetAddress nextElement = inetAddresses.nextElement();
                        if (!nextElement.isLoopbackAddress() && (nextElement instanceof Inet4Address)) {
                            return nextElement.getHostAddress().toString();
                        }
                    }
                }
            }
        } catch (Exception e2) {
        }
        return "";
    }

    private static synchronized String a(String str) {
        String str2;
        synchronized (a.class) {
            try {
                MessageDigest instance = MessageDigest.getInstance("MD5");
                try {
                    instance.update(str.getBytes());
                    byte[] digest = instance.digest();
                    StringBuffer stringBuffer = new StringBuffer();
                    for (byte b2 : digest) {
                        String hexString = Integer.toHexString(b2 & 255);
                        if (hexString.length() < 2) {
                            hexString = "0" + hexString;
                        }
                        stringBuffer.append(hexString);
                    }
                    str2 = stringBuffer.toString();
                } catch (Exception e2) {
                    str2 = "";
                }
            } catch (NoSuchAlgorithmException e3) {
                e3.printStackTrace();
                a(null, 3, "Could not create hash value");
                str2 = "";
            }
        }
        return str2;
    }

    static String a(InputStream inputStream) {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder sb = new StringBuilder();
        while (true) {
            try {
                String readLine = bufferedReader.readLine();
                if (readLine == null) {
                    try {
                        break;
                    } catch (IOException e2) {
                        e2.printStackTrace();
                    }
                } else {
                    sb.append(String.valueOf(readLine) + "\n");
                }
            } catch (IOException e3) {
                e3.printStackTrace();
                try {
                    inputStream.close();
                } catch (IOException e4) {
                    e4.printStackTrace();
                }
            } catch (Throwable th) {
                try {
                    inputStream.close();
                } catch (IOException e5) {
                    e5.printStackTrace();
                }
                throw th;
            }
        }
        inputStream.close();
        return sb.toString();
    }

    private static Location a(Context context) {
        if (c == null) {
            return null;
        }
        try {
            Location location = c;
            Geocoder geocoder = new Geocoder(context);
            new ArrayList();
            List<Address> fromLocation = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
            if (fromLocation.size() > 0) {
                d = fromLocation.get(0);
                e = fromLocation.get(0).toString();
            }
            return c;
        } catch (Exception e2) {
            return null;
        }
    }

    private static void a(String str, int i, String str2) {
        if (str2 == null) {
            str2 = "";
        }
        String className = Thread.currentThread().getStackTrace()[3].getClassName();
        String substring = className.substring(className.lastIndexOf(".") + 1);
        String methodName = Thread.currentThread().getStackTrace()[3].getMethodName();
        Log.println(3, "CAD_LOG", String.valueOf(str2) + "\n at " + className + "." + methodName + " (" + substring + ":" + Thread.currentThread().getStackTrace()[3].getLineNumber() + ")");
    }

    private static String b() {
        try {
            return Build.VERSION.RELEASE;
        } catch (Exception e2) {
            return "";
        }
    }

    private static String c() {
        try {
            return String.valueOf(Build.MODEL) + "-" + Build.MANUFACTURER;
        } catch (Exception e2) {
            return "";
        }
    }

    private static String b(Context context) {
        try {
            TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
            if (telephonyManager.getSimState() == 0) {
                return "";
            }
            return telephonyManager.getSimOperatorName();
        } catch (Exception e2) {
            return "";
        }
    }

    private static String c(Context context) {
        try {
            String string = Settings.Secure.getString(context.getContentResolver(), "android_id");
            if (string != null) {
                return a(string);
            }
        } catch (Exception e2) {
        }
        return "";
    }

    private static String d(Context context) {
        try {
            int i = context.getApplicationInfo().labelRes;
            if (i != 0) {
                return context.getApplicationContext().getString(i);
            }
        } catch (Exception e2) {
        }
        return "";
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    /* JADX WARNING: Removed duplicated region for block: B:122:0x041c A[SYNTHETIC, Splitter:B:122:0x041c] */
    /* JADX WARNING: Removed duplicated region for block: B:156:0x04bb  */
    /* JADX WARNING: Removed duplicated region for block: B:157:0x04be  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x00f6 A[Catch:{ Exception -> 0x0354 }] */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x0103 A[Catch:{ Exception -> 0x0361 }] */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x014a A[Catch:{ Exception -> 0x0484 }] */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x0276 A[Catch:{ Exception -> 0x0484 }] */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x033d A[SYNTHETIC, Splitter:B:68:0x033d] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static void a(android.content.Context r31, java.util.List<org.apache.http.NameValuePair> r32) {
        /*
            java.lang.String r1 = "phone"
            r0 = r31
            java.lang.Object r1 = r0.getSystemService(r1)     // Catch:{ Exception -> 0x0484 }
            android.telephony.TelephonyManager r1 = (android.telephony.TelephonyManager) r1     // Catch:{ Exception -> 0x0484 }
            java.lang.String r2 = "android.permission.READ_PHONE_STATE"
            r0 = r31
            int r2 = r0.checkCallingOrSelfPermission(r2)     // Catch:{ Exception -> 0x0484 }
            if (r2 != 0) goto L_0x0316
            r2 = 1
            r11 = r2
        L_0x0016:
            java.lang.String r14 = ""
            java.lang.String r10 = ""
            java.lang.String r9 = ""
            java.lang.String r8 = ""
            java.lang.String r7 = ""
            java.lang.String r6 = ""
            java.lang.String r5 = ""
            java.lang.String r4 = ""
            java.lang.String r3 = ""
            java.lang.String r2 = ""
            if (r11 == 0) goto L_0x04c5
            java.lang.String r14 = r1.getDeviceId()     // Catch:{ Exception -> 0x0484 }
            java.lang.String r9 = r1.getLine1Number()     // Catch:{ Exception -> 0x0484 }
            java.lang.String r8 = r1.getDeviceSoftwareVersion()     // Catch:{ Exception -> 0x0484 }
            java.lang.String r7 = r1.getNetworkOperatorName()     // Catch:{ Exception -> 0x0484 }
            java.lang.String r6 = r1.getSimCountryIso()     // Catch:{ Exception -> 0x0484 }
            java.lang.String r5 = r1.getSimOperatorName()     // Catch:{ Exception -> 0x0484 }
            java.lang.String r4 = r1.getSimSerialNumber()     // Catch:{ Exception -> 0x0484 }
            java.lang.String r3 = r1.getSubscriberId()     // Catch:{ Exception -> 0x0484 }
            int r2 = r1.getNetworkType()     // Catch:{ Exception -> 0x0484 }
            switch(r2) {
                case 1: goto L_0x031e;
                case 2: goto L_0x031a;
                case 3: goto L_0x0322;
                default: goto L_0x0053;
            }     // Catch:{ Exception -> 0x0484 }
        L_0x0053:
            java.lang.String r2 = "UNKNOWN"
        L_0x0055:
            int r1 = r1.getPhoneType()     // Catch:{ Exception -> 0x0484 }
            switch(r1) {
                case 0: goto L_0x032a;
                case 1: goto L_0x0326;
                default: goto L_0x005c;
            }     // Catch:{ Exception -> 0x0484 }
        L_0x005c:
            java.lang.String r1 = "UNKNOWN"
        L_0x005e:
            r19 = r1
            r20 = r2
            r21 = r3
            r22 = r4
            r23 = r5
            r24 = r6
            r25 = r7
            r26 = r8
            r27 = r9
        L_0x0070:
            java.lang.String r28 = a()     // Catch:{ Exception -> 0x0484 }
            java.text.SimpleDateFormat r1 = new java.text.SimpleDateFormat     // Catch:{ Exception -> 0x0484 }
            java.lang.String r2 = "yyyy-MM-dd HH:mm:ss"
            r1.<init>(r2)     // Catch:{ Exception -> 0x0484 }
            java.util.Date r2 = new java.util.Date     // Catch:{ Exception -> 0x0484 }
            r2.<init>()     // Catch:{ Exception -> 0x0484 }
            java.lang.String r29 = r1.format(r2)     // Catch:{ Exception -> 0x0484 }
            java.lang.String r1 = r14.trim()     // Catch:{ Exception -> 0x0484 }
            int r1 = r1.length()     // Catch:{ Exception -> 0x0484 }
            if (r1 != 0) goto L_0x0092
            java.lang.String r14 = c(r31)     // Catch:{ Exception -> 0x0484 }
        L_0x0092:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0484 }
            java.lang.String r2 = java.lang.String.valueOf(r28)     // Catch:{ Exception -> 0x0484 }
            r1.<init>(r2)     // Catch:{ Exception -> 0x0484 }
            java.lang.StringBuilder r1 = r1.append(r14)     // Catch:{ Exception -> 0x0484 }
            r0 = r29
            java.lang.StringBuilder r1 = r1.append(r0)     // Catch:{ Exception -> 0x0484 }
            java.lang.String r2 = "believe myself"
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x0484 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x0484 }
            java.lang.String r30 = a(r1)     // Catch:{ Exception -> 0x0484 }
            r3 = 0
            r4 = 0
            java.lang.String r2 = ""
            java.lang.String r1 = "window"
            r0 = r31
            java.lang.Object r1 = r0.getSystemService(r1)     // Catch:{ Exception -> 0x0332 }
            android.view.WindowManager r1 = (android.view.WindowManager) r1     // Catch:{ Exception -> 0x0332 }
            android.view.Display r5 = r1.getDefaultDisplay()     // Catch:{ Exception -> 0x0332 }
            int r1 = r5.getWidth()     // Catch:{ Exception -> 0x0332 }
            int r6 = r5.getHeight()     // Catch:{ Exception -> 0x0332 }
            if (r1 <= r6) goto L_0x032e
            java.lang.String r1 = "landscape"
        L_0x00d1:
            int r2 = r5.getWidth()     // Catch:{ Exception -> 0x04b4 }
            int r3 = r5.getHeight()     // Catch:{ Exception -> 0x04b8 }
            r16 = r1
            r17 = r3
            r18 = r2
        L_0x00df:
            java.lang.String r13 = ""
            java.lang.String r12 = ""
            java.lang.String r11 = ""
            java.lang.String r10 = ""
            java.lang.String r9 = ""
            java.lang.String r8 = ""
            java.lang.String r15 = ""
            java.lang.String r1 = "CAD_LOG"
            java.lang.String r2 = "Trying to refresh location"
            android.util.Log.d(r1, r2)     // Catch:{ Exception -> 0x0354 }
            if (r31 != 0) goto L_0x033d
            java.lang.String r1 = "CAD_LOG"
            java.lang.String r2 = "Context not set - quit location refresh"
            android.util.Log.d(r1, r2)     // Catch:{ Exception -> 0x0354 }
        L_0x00fd:
            android.location.Location r1 = a(r31)     // Catch:{ Exception -> 0x0361 }
            if (r1 == 0) goto L_0x04be
            android.location.Location r1 = a(r31)     // Catch:{ Exception -> 0x0361 }
            double r1 = r1.getLatitude()     // Catch:{ Exception -> 0x0361 }
            java.lang.String r6 = java.lang.Double.toString(r1)     // Catch:{ Exception -> 0x0361 }
            android.location.Location r1 = a(r31)     // Catch:{ Exception -> 0x0493 }
            double r1 = r1.getLongitude()     // Catch:{ Exception -> 0x0493 }
            java.lang.String r5 = java.lang.Double.toString(r1)     // Catch:{ Exception -> 0x0493 }
            java.lang.String r1 = vn.clevernet.android.sdk.a.e     // Catch:{ Exception -> 0x049b }
            r2 = 61
            r3 = 58
            java.lang.String r4 = r1.replace(r2, r3)     // Catch:{ Exception -> 0x049b }
            android.location.Address r1 = vn.clevernet.android.sdk.a.d     // Catch:{ Exception -> 0x04a2 }
            java.lang.String r3 = r1.getThoroughfare()     // Catch:{ Exception -> 0x04a2 }
            android.location.Address r1 = vn.clevernet.android.sdk.a.d     // Catch:{ Exception -> 0x04a8 }
            java.lang.String r2 = r1.getSubAdminArea()     // Catch:{ Exception -> 0x04a8 }
            android.location.Address r1 = vn.clevernet.android.sdk.a.d     // Catch:{ Exception -> 0x04ad }
            java.lang.String r1 = r1.getAdminArea()     // Catch:{ Exception -> 0x04ad }
            android.location.Address r7 = vn.clevernet.android.sdk.a.d     // Catch:{ Exception -> 0x04b1 }
            java.lang.String r7 = r7.getCountryName()     // Catch:{ Exception -> 0x04b1 }
            r8 = r1
            r9 = r2
            r2 = r7
        L_0x0140:
            java.lang.String r1 = r14.trim()     // Catch:{ Exception -> 0x0484 }
            int r1 = r1.length()     // Catch:{ Exception -> 0x0484 }
            if (r1 != 0) goto L_0x04bb
            java.lang.String r1 = c(r31)     // Catch:{ Exception -> 0x0484 }
        L_0x014e:
            org.apache.http.message.BasicNameValuePair r7 = new org.apache.http.message.BasicNameValuePair     // Catch:{ Exception -> 0x0484 }
            java.lang.String r10 = "imeinumber"
            r7.<init>(r10, r1)     // Catch:{ Exception -> 0x0484 }
            r0 = r32
            r0.add(r7)     // Catch:{ Exception -> 0x0484 }
            org.apache.http.message.BasicNameValuePair r1 = new org.apache.http.message.BasicNameValuePair     // Catch:{ Exception -> 0x0484 }
            java.lang.String r7 = "phonenumber"
            r0 = r27
            r1.<init>(r7, r0)     // Catch:{ Exception -> 0x0484 }
            r0 = r32
            r0.add(r1)     // Catch:{ Exception -> 0x0484 }
            org.apache.http.message.BasicNameValuePair r1 = new org.apache.http.message.BasicNameValuePair     // Catch:{ Exception -> 0x0484 }
            java.lang.String r7 = "softwareversion"
            r0 = r26
            r1.<init>(r7, r0)     // Catch:{ Exception -> 0x0484 }
            r0 = r32
            r0.add(r1)     // Catch:{ Exception -> 0x0484 }
            org.apache.http.message.BasicNameValuePair r1 = new org.apache.http.message.BasicNameValuePair     // Catch:{ Exception -> 0x0484 }
            java.lang.String r7 = "operatorname"
            r0 = r25
            r1.<init>(r7, r0)     // Catch:{ Exception -> 0x0484 }
            r0 = r32
            r0.add(r1)     // Catch:{ Exception -> 0x0484 }
            org.apache.http.message.BasicNameValuePair r1 = new org.apache.http.message.BasicNameValuePair     // Catch:{ Exception -> 0x0484 }
            java.lang.String r7 = "simcountrycode"
            r0 = r24
            r1.<init>(r7, r0)     // Catch:{ Exception -> 0x0484 }
            r0 = r32
            r0.add(r1)     // Catch:{ Exception -> 0x0484 }
            org.apache.http.message.BasicNameValuePair r1 = new org.apache.http.message.BasicNameValuePair     // Catch:{ Exception -> 0x0484 }
            java.lang.String r7 = "simoperator"
            r0 = r23
            r1.<init>(r7, r0)     // Catch:{ Exception -> 0x0484 }
            r0 = r32
            r0.add(r1)     // Catch:{ Exception -> 0x0484 }
            org.apache.http.message.BasicNameValuePair r1 = new org.apache.http.message.BasicNameValuePair     // Catch:{ Exception -> 0x0484 }
            java.lang.String r7 = "simserialno"
            r0 = r22
            r1.<init>(r7, r0)     // Catch:{ Exception -> 0x0484 }
            r0 = r32
            r0.add(r1)     // Catch:{ Exception -> 0x0484 }
            org.apache.http.message.BasicNameValuePair r1 = new org.apache.http.message.BasicNameValuePair     // Catch:{ Exception -> 0x0484 }
            java.lang.String r7 = "subscriberid"
            r0 = r21
            r1.<init>(r7, r0)     // Catch:{ Exception -> 0x0484 }
            r0 = r32
            r0.add(r1)     // Catch:{ Exception -> 0x0484 }
            org.apache.http.message.BasicNameValuePair r1 = new org.apache.http.message.BasicNameValuePair     // Catch:{ Exception -> 0x0484 }
            java.lang.String r7 = "networktype"
            r0 = r20
            r1.<init>(r7, r0)     // Catch:{ Exception -> 0x0484 }
            r0 = r32
            r0.add(r1)     // Catch:{ Exception -> 0x0484 }
            org.apache.http.message.BasicNameValuePair r1 = new org.apache.http.message.BasicNameValuePair     // Catch:{ Exception -> 0x0484 }
            java.lang.String r7 = "phonetype"
            r0 = r19
            r1.<init>(r7, r0)     // Catch:{ Exception -> 0x0484 }
            r0 = r32
            r0.add(r1)     // Catch:{ Exception -> 0x0484 }
            org.apache.http.message.BasicNameValuePair r1 = new org.apache.http.message.BasicNameValuePair     // Catch:{ Exception -> 0x0484 }
            java.lang.String r7 = "ip"
            r0 = r28
            r1.<init>(r7, r0)     // Catch:{ Exception -> 0x0484 }
            r0 = r32
            r0.add(r1)     // Catch:{ Exception -> 0x0484 }
            org.apache.http.message.BasicNameValuePair r1 = new org.apache.http.message.BasicNameValuePair     // Catch:{ Exception -> 0x0484 }
            java.lang.String r7 = "time"
            r0 = r29
            r1.<init>(r7, r0)     // Catch:{ Exception -> 0x0484 }
            r0 = r32
            r0.add(r1)     // Catch:{ Exception -> 0x0484 }
            org.apache.http.message.BasicNameValuePair r1 = new org.apache.http.message.BasicNameValuePair     // Catch:{ Exception -> 0x0484 }
            java.lang.String r7 = "md5"
            r0 = r30
            r1.<init>(r7, r0)     // Catch:{ Exception -> 0x0484 }
            r0 = r32
            r0.add(r1)     // Catch:{ Exception -> 0x0484 }
            org.apache.http.message.BasicNameValuePair r1 = new org.apache.http.message.BasicNameValuePair     // Catch:{ Exception -> 0x0484 }
            java.lang.String r7 = "osversion"
            java.lang.String r10 = b()     // Catch:{ Exception -> 0x0484 }
            r1.<init>(r7, r10)     // Catch:{ Exception -> 0x0484 }
            r0 = r32
            r0.add(r1)     // Catch:{ Exception -> 0x0484 }
            org.apache.http.message.BasicNameValuePair r1 = new org.apache.http.message.BasicNameValuePair     // Catch:{ Exception -> 0x0484 }
            java.lang.String r7 = "devicename"
            java.lang.String r10 = c()     // Catch:{ Exception -> 0x0484 }
            r1.<init>(r7, r10)     // Catch:{ Exception -> 0x0484 }
            r0 = r32
            r0.add(r1)     // Catch:{ Exception -> 0x0484 }
            org.apache.http.message.BasicNameValuePair r1 = new org.apache.http.message.BasicNameValuePair     // Catch:{ Exception -> 0x0484 }
            java.lang.String r7 = "duongpho"
            r1.<init>(r7, r3)     // Catch:{ Exception -> 0x0484 }
            r0 = r32
            r0.add(r1)     // Catch:{ Exception -> 0x0484 }
            org.apache.http.message.BasicNameValuePair r1 = new org.apache.http.message.BasicNameValuePair     // Catch:{ Exception -> 0x0484 }
            java.lang.String r3 = "quanhuyen"
            r1.<init>(r3, r9)     // Catch:{ Exception -> 0x0484 }
            r0 = r32
            r0.add(r1)     // Catch:{ Exception -> 0x0484 }
            org.apache.http.message.BasicNameValuePair r1 = new org.apache.http.message.BasicNameValuePair     // Catch:{ Exception -> 0x0484 }
            java.lang.String r3 = "tinhthanh"
            r1.<init>(r3, r8)     // Catch:{ Exception -> 0x0484 }
            r0 = r32
            r0.add(r1)     // Catch:{ Exception -> 0x0484 }
            org.apache.http.message.BasicNameValuePair r1 = new org.apache.http.message.BasicNameValuePair     // Catch:{ Exception -> 0x0484 }
            java.lang.String r3 = "quocgia"
            r1.<init>(r3, r2)     // Catch:{ Exception -> 0x0484 }
            r0 = r32
            r0.add(r1)     // Catch:{ Exception -> 0x0484 }
            org.apache.http.message.BasicNameValuePair r1 = new org.apache.http.message.BasicNameValuePair     // Catch:{ Exception -> 0x0484 }
            java.lang.String r2 = "locationname"
            r1.<init>(r2, r4)     // Catch:{ Exception -> 0x0484 }
            r0 = r32
            r0.add(r1)     // Catch:{ Exception -> 0x0484 }
            org.apache.http.message.BasicNameValuePair r1 = new org.apache.http.message.BasicNameValuePair     // Catch:{ Exception -> 0x0484 }
            java.lang.String r2 = "networkname"
            java.lang.String r3 = b(r31)     // Catch:{ Exception -> 0x0484 }
            r1.<init>(r2, r3)     // Catch:{ Exception -> 0x0484 }
            r0 = r32
            r0.add(r1)     // Catch:{ Exception -> 0x0484 }
            org.apache.http.message.BasicNameValuePair r2 = new org.apache.http.message.BasicNameValuePair     // Catch:{ Exception -> 0x0484 }
            java.lang.String r3 = "ua"
            java.lang.String r1 = vn.clevernet.android.sdk.a.a     // Catch:{ Exception -> 0x0484 }
            if (r1 == 0) goto L_0x041c
            java.lang.String r1 = vn.clevernet.android.sdk.a.a     // Catch:{ Exception -> 0x0484 }
        L_0x0278:
            r2.<init>(r3, r1)     // Catch:{ Exception -> 0x0484 }
            r0 = r32
            r0.add(r2)     // Catch:{ Exception -> 0x0484 }
            org.apache.http.message.BasicNameValuePair r1 = new org.apache.http.message.BasicNameValuePair     // Catch:{ Exception -> 0x0484 }
            java.lang.String r2 = "requester"
            java.lang.String r3 = "android_sdk"
            r1.<init>(r2, r3)     // Catch:{ Exception -> 0x0484 }
            r0 = r32
            r0.add(r1)     // Catch:{ Exception -> 0x0484 }
            org.apache.http.message.BasicNameValuePair r1 = new org.apache.http.message.BasicNameValuePair     // Catch:{ Exception -> 0x0484 }
            java.lang.String r2 = "app"
            java.lang.String r3 = "true"
            r1.<init>(r2, r3)     // Catch:{ Exception -> 0x0484 }
            r0 = r32
            r0.add(r1)     // Catch:{ Exception -> 0x0484 }
            org.apache.http.message.BasicNameValuePair r1 = new org.apache.http.message.BasicNameValuePair     // Catch:{ Exception -> 0x0484 }
            java.lang.String r2 = "uid"
            java.lang.String r3 = c(r31)     // Catch:{ Exception -> 0x0484 }
            r1.<init>(r2, r3)     // Catch:{ Exception -> 0x0484 }
            r0 = r32
            r0.add(r1)     // Catch:{ Exception -> 0x0484 }
            org.apache.http.message.BasicNameValuePair r1 = new org.apache.http.message.BasicNameValuePair     // Catch:{ Exception -> 0x0484 }
            java.lang.String r2 = "version"
            java.lang.String r3 = "2.0"
            r1.<init>(r2, r3)     // Catch:{ Exception -> 0x0484 }
            r0 = r32
            r0.add(r1)     // Catch:{ Exception -> 0x0484 }
            org.apache.http.message.BasicNameValuePair r1 = new org.apache.http.message.BasicNameValuePair     // Catch:{ Exception -> 0x0484 }
            java.lang.String r2 = "app_name"
            java.lang.String r3 = d(r31)     // Catch:{ Exception -> 0x0484 }
            r1.<init>(r2, r3)     // Catch:{ Exception -> 0x0484 }
            r0 = r32
            r0.add(r1)     // Catch:{ Exception -> 0x0484 }
            org.apache.http.message.BasicNameValuePair r1 = new org.apache.http.message.BasicNameValuePair     // Catch:{ Exception -> 0x0484 }
            java.lang.String r2 = "device_orientation"
            r0 = r16
            r1.<init>(r2, r0)     // Catch:{ Exception -> 0x0484 }
            r0 = r32
            r0.add(r1)     // Catch:{ Exception -> 0x0484 }
            org.apache.http.message.BasicNameValuePair r1 = new org.apache.http.message.BasicNameValuePair     // Catch:{ Exception -> 0x0484 }
            java.lang.String r2 = "screen_size"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0484 }
            java.lang.String r4 = java.lang.String.valueOf(r18)     // Catch:{ Exception -> 0x0484 }
            r3.<init>(r4)     // Catch:{ Exception -> 0x0484 }
            java.lang.String r4 = "x"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x0484 }
            r0 = r17
            java.lang.StringBuilder r3 = r3.append(r0)     // Catch:{ Exception -> 0x0484 }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x0484 }
            r1.<init>(r2, r3)     // Catch:{ Exception -> 0x0484 }
            r0 = r32
            r0.add(r1)     // Catch:{ Exception -> 0x0484 }
            org.apache.http.message.BasicNameValuePair r1 = new org.apache.http.message.BasicNameValuePair     // Catch:{ Exception -> 0x0484 }
            java.lang.String r2 = "lat"
            r1.<init>(r2, r6)     // Catch:{ Exception -> 0x0484 }
            r0 = r32
            r0.add(r1)     // Catch:{ Exception -> 0x0484 }
            org.apache.http.message.BasicNameValuePair r1 = new org.apache.http.message.BasicNameValuePair     // Catch:{ Exception -> 0x0484 }
            java.lang.String r2 = "lng"
            r1.<init>(r2, r5)     // Catch:{ Exception -> 0x0484 }
            r0 = r32
            r0.add(r1)     // Catch:{ Exception -> 0x0484 }
        L_0x0315:
            return
        L_0x0316:
            r2 = 0
            r11 = r2
            goto L_0x0016
        L_0x031a:
            java.lang.String r2 = "EDGE"
            goto L_0x0055
        L_0x031e:
            java.lang.String r2 = "GPRS"
            goto L_0x0055
        L_0x0322:
            java.lang.String r2 = "UMTS"
            goto L_0x0055
        L_0x0326:
            java.lang.String r1 = "GSM"
            goto L_0x005e
        L_0x032a:
            java.lang.String r1 = "UNKNOWN"
            goto L_0x005e
        L_0x032e:
            java.lang.String r1 = "portrait"
            goto L_0x00d1
        L_0x0332:
            r1 = move-exception
            r1 = r2
            r2 = r3
        L_0x0335:
            r16 = r1
            r17 = r4
            r18 = r2
            goto L_0x00df
        L_0x033d:
            long r1 = vn.clevernet.android.sdk.a.b     // Catch:{ Exception -> 0x0354 }
            r3 = 900000(0xdbba0, double:4.44659E-318)
            long r1 = r1 + r3
            long r3 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x0354 }
            int r1 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r1 <= 0) goto L_0x036d
            java.lang.String r1 = "CAD_LOG"
            java.lang.String r2 = "It's not time yet for refreshing the location"
            android.util.Log.d(r1, r2)     // Catch:{ Exception -> 0x0354 }
            goto L_0x00fd
        L_0x0354:
            r1 = move-exception
            r1 = 0
            vn.clevernet.android.sdk.a.c = r1     // Catch:{ Exception -> 0x0361 }
            r1 = 0
            vn.clevernet.android.sdk.a.d = r1     // Catch:{ Exception -> 0x0361 }
            java.lang.String r1 = ""
            vn.clevernet.android.sdk.a.e = r1     // Catch:{ Exception -> 0x0361 }
            goto L_0x00fd
        L_0x0361:
            r1 = move-exception
            r1 = r8
            r2 = r9
            r3 = r10
            r4 = r11
            r5 = r12
            r6 = r13
        L_0x0368:
            r8 = r1
            r9 = r2
            r2 = r15
            goto L_0x0140
        L_0x036d:
            monitor-enter(r31)     // Catch:{ Exception -> 0x0354 }
            long r1 = vn.clevernet.android.sdk.a.b     // Catch:{ all -> 0x0386 }
            r3 = 900000(0xdbba0, double:4.44659E-318)
            long r1 = r1 + r3
            long r3 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x0386 }
            int r1 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r1 <= 0) goto L_0x0389
            java.lang.String r1 = "CAD_LOG"
            java.lang.String r2 = "Another thread updated the loation already"
            android.util.Log.d(r1, r2)     // Catch:{ all -> 0x0386 }
            monitor-exit(r31)     // Catch:{ all -> 0x0386 }
            goto L_0x00fd
        L_0x0386:
            r1 = move-exception
            monitor-exit(r31)     // Catch:{ Exception -> 0x0354 }
            throw r1     // Catch:{ Exception -> 0x0354 }
        L_0x0389:
            java.lang.String r1 = "android.permission.ACCESS_COARSE_LOCATION"
            r0 = r31
            int r1 = r0.checkCallingOrSelfPermission(r1)     // Catch:{ all -> 0x0386 }
            if (r1 != 0) goto L_0x03af
            r1 = 1
            r4 = r1
        L_0x0395:
            java.lang.String r1 = "android.permission.ACCESS_FINE_LOCATION"
            r0 = r31
            int r1 = r0.checkCallingOrSelfPermission(r1)     // Catch:{ all -> 0x0386 }
            if (r1 != 0) goto L_0x03b2
            r1 = 1
            r3 = r1
        L_0x03a1:
            if (r4 != 0) goto L_0x03b5
            if (r3 != 0) goto L_0x03b5
            java.lang.String r1 = "CAD_LOG"
            java.lang.String r2 = "No permissions for requesting the location"
            android.util.Log.d(r1, r2)     // Catch:{ all -> 0x0386 }
            monitor-exit(r31)     // Catch:{ all -> 0x0386 }
            goto L_0x00fd
        L_0x03af:
            r1 = 0
            r4 = r1
            goto L_0x0395
        L_0x03b2:
            r1 = 0
            r3 = r1
            goto L_0x03a1
        L_0x03b5:
            java.lang.String r1 = "location"
            r0 = r31
            java.lang.Object r1 = r0.getSystemService(r1)     // Catch:{ all -> 0x0386 }
            android.location.LocationManager r1 = (android.location.LocationManager) r1     // Catch:{ all -> 0x0386 }
            if (r1 != 0) goto L_0x03cb
            java.lang.String r1 = "CAD_LOG"
            java.lang.String r2 = "Unable to fetch a location manger"
            android.util.Log.d(r1, r2)     // Catch:{ all -> 0x0386 }
            monitor-exit(r31)     // Catch:{ all -> 0x0386 }
            goto L_0x00fd
        L_0x03cb:
            r2 = 0
            android.location.Criteria r5 = new android.location.Criteria     // Catch:{ all -> 0x0386 }
            r5.<init>()     // Catch:{ all -> 0x0386 }
            r6 = 0
            r5.setCostAllowed(r6)     // Catch:{ all -> 0x0386 }
            if (r4 == 0) goto L_0x03e0
            r2 = 2
            r5.setAccuracy(r2)     // Catch:{ all -> 0x0386 }
            r2 = 1
            java.lang.String r2 = r1.getBestProvider(r5, r2)     // Catch:{ all -> 0x0386 }
        L_0x03e0:
            if (r2 != 0) goto L_0x03ed
            if (r3 == 0) goto L_0x03ed
            r2 = 1
            r5.setAccuracy(r2)     // Catch:{ all -> 0x0386 }
            r2 = 1
            java.lang.String r2 = r1.getBestProvider(r5, r2)     // Catch:{ all -> 0x0386 }
        L_0x03ed:
            if (r2 != 0) goto L_0x03f9
            java.lang.String r1 = "CAD_LOG"
            java.lang.String r2 = "Unable to fetch a location provider"
            android.util.Log.d(r1, r2)     // Catch:{ all -> 0x0386 }
            monitor-exit(r31)     // Catch:{ all -> 0x0386 }
            goto L_0x00fd
        L_0x03f9:
            r3 = 1
            java.lang.String r3 = r1.getBestProvider(r5, r3)     // Catch:{ all -> 0x0386 }
            android.location.Location r3 = r1.getLastKnownLocation(r3)     // Catch:{ all -> 0x0386 }
            vn.clevernet.android.sdk.a.c = r3     // Catch:{ all -> 0x0386 }
            long r3 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x0386 }
            vn.clevernet.android.sdk.a.b = r3     // Catch:{ all -> 0x0386 }
            r3 = 0
            r5 = 0
            vn.clevernet.android.sdk.i r6 = new vn.clevernet.android.sdk.i     // Catch:{ all -> 0x0386 }
            r6.<init>(r1)     // Catch:{ all -> 0x0386 }
            android.os.Looper r7 = r31.getMainLooper()     // Catch:{ all -> 0x0386 }
            r1.requestLocationUpdates(r2, r3, r5, r6, r7)     // Catch:{ all -> 0x0386 }
            monitor-exit(r31)     // Catch:{ all -> 0x0386 }
            goto L_0x00fd
        L_0x041c:
            java.lang.StringBuffer r1 = new java.lang.StringBuffer     // Catch:{ Exception -> 0x0484 }
            r1.<init>()     // Catch:{ Exception -> 0x0484 }
            java.lang.String r4 = android.os.Build.VERSION.RELEASE     // Catch:{ Exception -> 0x0484 }
            int r7 = r4.length()     // Catch:{ Exception -> 0x0484 }
            if (r7 <= 0) goto L_0x0487
            r1.append(r4)     // Catch:{ Exception -> 0x0484 }
        L_0x042c:
            java.lang.String r4 = "; "
            r1.append(r4)     // Catch:{ Exception -> 0x0484 }
            java.util.Locale r4 = java.util.Locale.getDefault()     // Catch:{ Exception -> 0x0484 }
            java.lang.String r7 = r4.getLanguage()     // Catch:{ Exception -> 0x0484 }
            if (r7 == 0) goto L_0x048d
            java.lang.String r7 = r7.toLowerCase()     // Catch:{ Exception -> 0x0484 }
            r1.append(r7)     // Catch:{ Exception -> 0x0484 }
            java.lang.String r4 = r4.getCountry()     // Catch:{ Exception -> 0x0484 }
            if (r4 == 0) goto L_0x0454
            java.lang.String r7 = "-"
            r1.append(r7)     // Catch:{ Exception -> 0x0484 }
            java.lang.String r4 = r4.toLowerCase()     // Catch:{ Exception -> 0x0484 }
            r1.append(r4)     // Catch:{ Exception -> 0x0484 }
        L_0x0454:
            java.lang.String r4 = android.os.Build.MODEL     // Catch:{ Exception -> 0x0484 }
            int r7 = r4.length()     // Catch:{ Exception -> 0x0484 }
            if (r7 <= 0) goto L_0x0464
            java.lang.String r7 = "; "
            r1.append(r7)     // Catch:{ Exception -> 0x0484 }
            r1.append(r4)     // Catch:{ Exception -> 0x0484 }
        L_0x0464:
            java.lang.String r4 = android.os.Build.ID     // Catch:{ Exception -> 0x0484 }
            int r7 = r4.length()     // Catch:{ Exception -> 0x0484 }
            if (r7 <= 0) goto L_0x0474
            java.lang.String r7 = " Build/"
            r1.append(r7)     // Catch:{ Exception -> 0x0484 }
            r1.append(r4)     // Catch:{ Exception -> 0x0484 }
        L_0x0474:
            java.lang.String r4 = "Mozilla/5.0 (Linux; U; Android %s) AppleWebKit/525.10+ (KHTML, like Gecko) Version/3.0.4 Mobile Safari/523.12.2"
            r7 = 1
            java.lang.Object[] r7 = new java.lang.Object[r7]     // Catch:{ Exception -> 0x0484 }
            r8 = 0
            r7[r8] = r1     // Catch:{ Exception -> 0x0484 }
            java.lang.String r1 = java.lang.String.format(r4, r7)     // Catch:{ Exception -> 0x0484 }
            vn.clevernet.android.sdk.a.a = r1     // Catch:{ Exception -> 0x0484 }
            goto L_0x0278
        L_0x0484:
            r1 = move-exception
            goto L_0x0315
        L_0x0487:
            java.lang.String r4 = "1.0"
            r1.append(r4)     // Catch:{ Exception -> 0x0484 }
            goto L_0x042c
        L_0x048d:
            java.lang.String r4 = "de"
            r1.append(r4)     // Catch:{ Exception -> 0x0484 }
            goto L_0x0454
        L_0x0493:
            r1 = move-exception
            r1 = r8
            r2 = r9
            r3 = r10
            r4 = r11
            r5 = r12
            goto L_0x0368
        L_0x049b:
            r1 = move-exception
            r1 = r8
            r2 = r9
            r3 = r10
            r4 = r11
            goto L_0x0368
        L_0x04a2:
            r1 = move-exception
            r1 = r8
            r2 = r9
            r3 = r10
            goto L_0x0368
        L_0x04a8:
            r1 = move-exception
            r1 = r8
            r2 = r9
            goto L_0x0368
        L_0x04ad:
            r1 = move-exception
            r1 = r8
            goto L_0x0368
        L_0x04b1:
            r7 = move-exception
            goto L_0x0368
        L_0x04b4:
            r2 = move-exception
            r2 = r3
            goto L_0x0335
        L_0x04b8:
            r3 = move-exception
            goto L_0x0335
        L_0x04bb:
            r1 = r14
            goto L_0x014e
        L_0x04be:
            r2 = r15
            r3 = r10
            r4 = r11
            r5 = r12
            r6 = r13
            goto L_0x0140
        L_0x04c5:
            r19 = r2
            r20 = r3
            r21 = r4
            r22 = r5
            r23 = r6
            r24 = r7
            r25 = r8
            r26 = r9
            r27 = r10
            goto L_0x0070
        */
        throw new UnsupportedOperationException("Method not decompiled: vn.clevernet.android.sdk.a.a(android.content.Context, java.util.List):void");
    }
}
