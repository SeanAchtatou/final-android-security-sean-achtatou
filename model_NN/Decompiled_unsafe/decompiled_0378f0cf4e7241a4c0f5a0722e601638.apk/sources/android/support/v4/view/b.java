package android.support.v4.view;

import android.view.KeyEvent;

/* compiled from: KeyEventCompatHoneycomb */
class b {
    public static boolean a(int i, int i2) {
        return KeyEvent.metaStateHasModifiers(i, i2);
    }

    public static boolean a(int i) {
        return KeyEvent.metaStateHasNoModifiers(i);
    }
}
