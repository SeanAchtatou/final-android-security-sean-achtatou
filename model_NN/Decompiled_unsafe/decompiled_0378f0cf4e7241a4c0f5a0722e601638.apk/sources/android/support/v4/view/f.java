package android.support.v4.view;

import android.os.Build;
import android.view.VelocityTracker;

/* compiled from: VelocityTrackerCompat */
public class f {
    static final c a;

    /* compiled from: VelocityTrackerCompat */
    interface c {
        float a(VelocityTracker velocityTracker, int i);
    }

    /* compiled from: VelocityTrackerCompat */
    static class a implements c {
        a() {
        }

        public float a(VelocityTracker velocityTracker, int i) {
            return velocityTracker.getXVelocity();
        }
    }

    /* compiled from: VelocityTrackerCompat */
    static class b implements c {
        b() {
        }

        public float a(VelocityTracker velocityTracker, int i) {
            return g.a(velocityTracker, i);
        }
    }

    static {
        if (Build.VERSION.SDK_INT >= 11) {
            a = new b();
        } else {
            a = new a();
        }
    }

    public static float a(VelocityTracker velocityTracker, int i) {
        return a.a(velocityTracker, i);
    }
}
