package android.support.v4.view;

import android.graphics.Paint;
import android.os.Build;
import android.view.View;

/* compiled from: ViewCompat */
public class h {
    static final g a;

    /* compiled from: ViewCompat */
    interface g {
        int a(View view);

        void a(View view, int i, Paint paint);

        void a(View view, Runnable runnable);

        boolean a(View view, int i);

        void b(View view);
    }

    /* compiled from: ViewCompat */
    static class a implements g {
        a() {
        }

        public boolean a(View view, int i) {
            return false;
        }

        public int a(View view) {
            return 2;
        }

        public void b(View view) {
            view.postInvalidateDelayed(a());
        }

        public void a(View view, Runnable runnable) {
            view.postDelayed(runnable, a());
        }

        /* access modifiers changed from: package-private */
        public long a() {
            return 10;
        }

        public void a(View view, int i, Paint paint) {
        }
    }

    /* compiled from: ViewCompat */
    static class b extends a {
        b() {
        }

        public int a(View view) {
            return i.a(view);
        }
    }

    /* compiled from: ViewCompat */
    static class c extends b {
        c() {
        }

        /* access modifiers changed from: package-private */
        public long a() {
            return j.a();
        }

        public void a(View view, int i, Paint paint) {
            j.a(view, i, paint);
        }
    }

    /* compiled from: ViewCompat */
    static class d extends c {
        d() {
        }

        public boolean a(View view, int i) {
            return k.a(view, i);
        }
    }

    /* compiled from: ViewCompat */
    static class e extends d {
        e() {
        }

        public void b(View view) {
            l.a(view);
        }

        public void a(View view, Runnable runnable) {
            l.a(view, runnable);
        }
    }

    /* compiled from: ViewCompat */
    static class f extends e {
        f() {
        }
    }

    static {
        int i = Build.VERSION.SDK_INT;
        if (i >= 17) {
            a = new f();
        } else if (i >= 16) {
            a = new e();
        } else if (i >= 14) {
            a = new d();
        } else if (i >= 11) {
            a = new c();
        } else if (i >= 9) {
            a = new b();
        } else {
            a = new a();
        }
    }

    public static boolean a(View view, int i) {
        return a.a(view, i);
    }

    public static int a(View view) {
        return a.a(view);
    }

    public static void b(View view) {
        a.b(view);
    }

    public static void a(View view, Runnable runnable) {
        a.a(view, runnable);
    }

    public static void a(View view, int i, Paint paint) {
        a.a(view, i, paint);
    }
}
