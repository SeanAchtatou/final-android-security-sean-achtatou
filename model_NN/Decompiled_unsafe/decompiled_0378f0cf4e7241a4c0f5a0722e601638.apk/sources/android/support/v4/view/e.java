package android.support.v4.view;

import android.database.DataSetObservable;
import android.database.DataSetObserver;
import android.os.Parcelable;
import android.view.View;
import android.view.ViewGroup;

/* compiled from: PagerAdapter */
public abstract class e {
    private DataSetObservable a;

    public abstract int a();

    public abstract boolean a(View view, Object obj);

    public void a(ViewGroup viewGroup) {
        a((View) viewGroup);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.e.a(android.view.View, int):java.lang.Object
     arg types: [android.view.ViewGroup, int]
     candidates:
      android.support.v4.view.e.a(android.view.ViewGroup, int):java.lang.Object
      android.support.v4.view.e.a(android.os.Parcelable, java.lang.ClassLoader):void
      android.support.v4.view.e.a(android.view.View, java.lang.Object):boolean
      android.support.v4.view.e.a(android.view.View, int):java.lang.Object */
    public Object a(ViewGroup viewGroup, int i) {
        return a((View) viewGroup, i);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.e.a(android.view.View, int, java.lang.Object):void
     arg types: [android.view.ViewGroup, int, java.lang.Object]
     candidates:
      android.support.v4.view.e.a(android.view.ViewGroup, int, java.lang.Object):void
      android.support.v4.view.e.a(android.view.View, int, java.lang.Object):void */
    public void a(ViewGroup viewGroup, int i, Object obj) {
        a((View) viewGroup, i, obj);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.e.b(android.view.View, int, java.lang.Object):void
     arg types: [android.view.ViewGroup, int, java.lang.Object]
     candidates:
      android.support.v4.view.e.b(android.view.ViewGroup, int, java.lang.Object):void
      android.support.v4.view.e.b(android.view.View, int, java.lang.Object):void */
    public void b(ViewGroup viewGroup, int i, Object obj) {
        b((View) viewGroup, i, obj);
    }

    public void b(ViewGroup viewGroup) {
        b((View) viewGroup);
    }

    public void a(View view) {
    }

    public Object a(View view, int i) {
        throw new UnsupportedOperationException("Required method instantiateItem was not overridden");
    }

    public void a(View view, int i, Object obj) {
        throw new UnsupportedOperationException("Required method destroyItem was not overridden");
    }

    public void b(View view, int i, Object obj) {
    }

    public void b(View view) {
    }

    public Parcelable b() {
        return null;
    }

    public void a(Parcelable parcelable, ClassLoader classLoader) {
    }

    public int a(Object obj) {
        return -1;
    }

    /* access modifiers changed from: package-private */
    public void a(DataSetObserver dataSetObserver) {
        this.a.registerObserver(dataSetObserver);
    }

    /* access modifiers changed from: package-private */
    public void b(DataSetObserver dataSetObserver) {
        this.a.unregisterObserver(dataSetObserver);
    }

    public float a(int i) {
        return 1.0f;
    }
}
