package android.support.v4.view;

import android.os.Build;
import android.view.KeyEvent;

/* compiled from: KeyEventCompat */
public class a {
    static final c a;

    /* compiled from: KeyEventCompat */
    interface c {
        boolean a(int i, int i2);

        boolean b(int i);
    }

    /* renamed from: android.support.v4.view.a$a  reason: collision with other inner class name */
    /* compiled from: KeyEventCompat */
    static class C0004a implements c {
        C0004a() {
        }

        private static int a(int i, int i2, int i3, int i4, int i5) {
            boolean z = true;
            boolean z2 = (i2 & i3) != 0;
            int i6 = i4 | i5;
            if ((i2 & i6) == 0) {
                z = false;
            }
            if (z2) {
                if (!z) {
                    return i & (i6 ^ -1);
                }
                throw new IllegalArgumentException("bad arguments");
            } else if (z) {
                return i & (i3 ^ -1);
            } else {
                return i;
            }
        }

        public int a(int i) {
            int i2;
            if ((i & 192) != 0) {
                i2 = i | 1;
            } else {
                i2 = i;
            }
            if ((i2 & 48) != 0) {
                i2 |= 2;
            }
            return i2 & 247;
        }

        public boolean a(int i, int i2) {
            if (a(a(a(i) & 247, i2, 1, 64, 128), i2, 2, 16, 32) == i2) {
                return true;
            }
            return false;
        }

        public boolean b(int i) {
            return (a(i) & 247) == 0;
        }
    }

    /* compiled from: KeyEventCompat */
    static class b implements c {
        b() {
        }

        public boolean a(int i, int i2) {
            return b.a(i, i2);
        }

        public boolean b(int i) {
            return b.a(i);
        }
    }

    static {
        if (Build.VERSION.SDK_INT >= 11) {
            a = new b();
        } else {
            a = new C0004a();
        }
    }

    public static boolean a(KeyEvent keyEvent, int i) {
        return a.a(keyEvent.getMetaState(), i);
    }

    public static boolean a(KeyEvent keyEvent) {
        return a.b(keyEvent.getMetaState());
    }
}
