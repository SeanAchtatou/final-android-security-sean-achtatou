package android.support.v4.view;

import android.view.View;

/* compiled from: ViewCompatJB */
public class l {
    public static void a(View view) {
        view.postInvalidateOnAnimation();
    }

    public static void a(View view, Runnable runnable) {
        view.postOnAnimation(runnable);
    }
}
