package android.support.v4.view;

import android.os.Build;
import android.view.MotionEvent;

/* compiled from: MotionEventCompat */
public class c {
    static final C0005c a;

    /* renamed from: android.support.v4.view.c$c  reason: collision with other inner class name */
    /* compiled from: MotionEventCompat */
    interface C0005c {
        int a(MotionEvent motionEvent, int i);

        int b(MotionEvent motionEvent, int i);

        float c(MotionEvent motionEvent, int i);

        float d(MotionEvent motionEvent, int i);
    }

    /* compiled from: MotionEventCompat */
    static class a implements C0005c {
        a() {
        }

        public int a(MotionEvent motionEvent, int i) {
            if (i == 0) {
                return 0;
            }
            return -1;
        }

        public int b(MotionEvent motionEvent, int i) {
            if (i == 0) {
                return 0;
            }
            throw new IndexOutOfBoundsException("Pre-Eclair does not support multiple pointers");
        }

        public float c(MotionEvent motionEvent, int i) {
            if (i == 0) {
                return motionEvent.getX();
            }
            throw new IndexOutOfBoundsException("Pre-Eclair does not support multiple pointers");
        }

        public float d(MotionEvent motionEvent, int i) {
            if (i == 0) {
                return motionEvent.getY();
            }
            throw new IndexOutOfBoundsException("Pre-Eclair does not support multiple pointers");
        }
    }

    /* compiled from: MotionEventCompat */
    static class b implements C0005c {
        b() {
        }

        public int a(MotionEvent motionEvent, int i) {
            return d.a(motionEvent, i);
        }

        public int b(MotionEvent motionEvent, int i) {
            return d.b(motionEvent, i);
        }

        public float c(MotionEvent motionEvent, int i) {
            return d.c(motionEvent, i);
        }

        public float d(MotionEvent motionEvent, int i) {
            return d.d(motionEvent, i);
        }
    }

    static {
        if (Build.VERSION.SDK_INT >= 5) {
            a = new b();
        } else {
            a = new a();
        }
    }

    public static int a(MotionEvent motionEvent) {
        return (motionEvent.getAction() & 65280) >> 8;
    }

    public static int a(MotionEvent motionEvent, int i) {
        return a.a(motionEvent, i);
    }

    public static int b(MotionEvent motionEvent, int i) {
        return a.b(motionEvent, i);
    }

    public static float c(MotionEvent motionEvent, int i) {
        return a.c(motionEvent, i);
    }

    public static float d(MotionEvent motionEvent, int i) {
        return a.d(motionEvent, i);
    }
}
