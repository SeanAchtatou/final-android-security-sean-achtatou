package android.support.v4.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.database.DataSetObserver;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.util.FloatMath;
import android.util.Log;
import android.view.FocusFinder;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.SoundEffectConstants;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.accessibility.AccessibilityEvent;
import android.view.animation.Interpolator;
import android.widget.Scroller;
import com.facebook.a.e;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class ViewPager extends ViewGroup {
    /* access modifiers changed from: private */
    public static final int[] a = {16842931};
    private static final g af = new g();
    private static final Comparator<b> b = new Comparator<b>() {
        /* renamed from: a */
        public int compare(b bVar, b bVar2) {
            return bVar.b - bVar2.b;
        }
    };
    private static final Interpolator c = new Interpolator() {
        public float getInterpolation(float f) {
            float f2 = f - 1.0f;
            return (f2 * f2 * f2 * f2 * f2) + 1.0f;
        }
    };
    private boolean A;
    private int B;
    private int C;
    private int D;
    private float E;
    private float F;
    private float G;
    private int H;
    private VelocityTracker I;
    private int J;
    private int K;
    private int L;
    private int M;
    private int N;
    private int O;
    private boolean P;
    private android.support.v4.d.a Q;
    private android.support.v4.d.a R;
    private boolean S;
    private boolean T;
    private boolean U;
    private int V;
    private d W;
    private d Z;
    private c aa;
    private e ab;
    private Method ac;
    private int ad;
    private ArrayList<View> ae;
    private final Runnable ag;
    private int ah;
    private final ArrayList<b> d;
    private final b e;
    private final Rect f;
    private e g;
    private int h;
    private int i;
    private Parcelable j;
    private ClassLoader k;
    private Scroller l;
    private f m;
    private int n;
    private Drawable o;
    private int p;
    private int q;
    private float r;
    private float s;
    private int t;
    private int u;
    private boolean v;
    private boolean w;
    private boolean x;
    private int y;
    private boolean z;

    interface a {
    }

    interface c {
        void a(e eVar, e eVar2);
    }

    public interface d {
        void a(int i);

        void a(int i, float f, int i2);

        void b(int i);
    }

    public interface e {
        void a(View view, float f);
    }

    static class b {
        Object a;
        int b;
        boolean c;
        float d;
        float e;

        b() {
        }
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        removeCallbacks(this.ag);
        super.onDetachedFromWindow();
    }

    private void setScrollState(int i2) {
        boolean z2 = true;
        if (this.ah != i2) {
            this.ah = i2;
            if (i2 == 1) {
                this.O = -1;
                this.N = -1;
            }
            if (this.ab != null) {
                if (i2 == 0) {
                    z2 = false;
                }
                b(z2);
            }
            if (this.W != null) {
                this.W.b(i2);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.e.a(android.view.ViewGroup, int, java.lang.Object):void
     arg types: [android.support.v4.view.ViewPager, int, java.lang.Object]
     candidates:
      android.support.v4.view.e.a(android.view.View, int, java.lang.Object):void
      android.support.v4.view.e.a(android.view.ViewGroup, int, java.lang.Object):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.ViewPager.a(int, boolean, boolean):void
     arg types: [int, int, int]
     candidates:
      android.support.v4.view.ViewPager.a(android.support.v4.view.ViewPager$b, int, android.support.v4.view.ViewPager$b):void
      android.support.v4.view.ViewPager.a(int, float, int):void
      android.support.v4.view.ViewPager.a(int, int, int):void
      android.support.v4.view.ViewPager.a(int, boolean, boolean):void */
    public void setAdapter(e eVar) {
        if (this.g != null) {
            this.g.b(this.m);
            this.g.a((ViewGroup) this);
            for (int i2 = 0; i2 < this.d.size(); i2++) {
                b bVar = this.d.get(i2);
                this.g.a((ViewGroup) this, bVar.b, bVar.a);
            }
            this.g.b((ViewGroup) this);
            this.d.clear();
            f();
            this.h = 0;
            scrollTo(0, 0);
        }
        e eVar2 = this.g;
        this.g = eVar;
        if (this.g != null) {
            if (this.m == null) {
                this.m = new f();
            }
            this.g.a((DataSetObserver) this.m);
            this.x = false;
            this.S = true;
            if (this.i >= 0) {
                this.g.a(this.j, this.k);
                a(this.i, false, true);
                this.i = -1;
                this.j = null;
                this.k = null;
            } else {
                b();
            }
        }
        if (this.aa != null && eVar2 != eVar) {
            this.aa.a(eVar2, eVar);
        }
    }

    private void f() {
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 < getChildCount()) {
                if (!((LayoutParams) getChildAt(i3).getLayoutParams()).a) {
                    removeViewAt(i3);
                    i3--;
                }
                i2 = i3 + 1;
            } else {
                return;
            }
        }
    }

    public e getAdapter() {
        return this.g;
    }

    /* access modifiers changed from: package-private */
    public void setOnAdapterChangeListener(c cVar) {
        this.aa = cVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.ViewPager.a(int, boolean, boolean):void
     arg types: [int, boolean, int]
     candidates:
      android.support.v4.view.ViewPager.a(android.support.v4.view.ViewPager$b, int, android.support.v4.view.ViewPager$b):void
      android.support.v4.view.ViewPager.a(int, float, int):void
      android.support.v4.view.ViewPager.a(int, int, int):void
      android.support.v4.view.ViewPager.a(int, boolean, boolean):void */
    public void setCurrentItem(int i2) {
        boolean z2;
        this.x = false;
        if (!this.S) {
            z2 = true;
        } else {
            z2 = false;
        }
        a(i2, z2, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.ViewPager.a(int, boolean, boolean):void
     arg types: [int, boolean, int]
     candidates:
      android.support.v4.view.ViewPager.a(android.support.v4.view.ViewPager$b, int, android.support.v4.view.ViewPager$b):void
      android.support.v4.view.ViewPager.a(int, float, int):void
      android.support.v4.view.ViewPager.a(int, int, int):void
      android.support.v4.view.ViewPager.a(int, boolean, boolean):void */
    public void a(int i2, boolean z2) {
        this.x = false;
        a(i2, z2, false);
    }

    public int getCurrentItem() {
        return this.h;
    }

    /* access modifiers changed from: package-private */
    public void a(int i2, boolean z2, boolean z3) {
        a(i2, z2, z3, 0);
    }

    /* access modifiers changed from: package-private */
    public void a(int i2, boolean z2, boolean z3, int i3) {
        boolean z4 = false;
        if (this.g == null || this.g.a() <= 0) {
            setScrollingCacheEnabled(false);
        } else if (z3 || this.h != i2 || this.d.size() == 0) {
            if (i2 < 0) {
                i2 = 0;
            } else if (i2 >= this.g.a()) {
                i2 = this.g.a() - 1;
            }
            int i4 = this.y;
            if (i2 > this.h + i4 || i2 < this.h - i4) {
                for (int i5 = 0; i5 < this.d.size(); i5++) {
                    this.d.get(i5).c = true;
                }
            }
            if (this.h != i2) {
                z4 = true;
            }
            a(i2);
            a(i2, z2, i3, z4);
        } else {
            setScrollingCacheEnabled(false);
        }
    }

    private void a(int i2, boolean z2, int i3, boolean z3) {
        int i4;
        b b2 = b(i2);
        if (b2 != null) {
            i4 = (int) (Math.max(this.r, Math.min(b2.e, this.s)) * ((float) getWidth()));
        } else {
            i4 = 0;
        }
        if (z2) {
            a(i4, 0, i3);
            if (z3 && this.W != null) {
                this.W.a(i2);
            }
            if (z3 && this.Z != null) {
                this.Z.a(i2);
                return;
            }
            return;
        }
        if (z3 && this.W != null) {
            this.W.a(i2);
        }
        if (z3 && this.Z != null) {
            this.Z.a(i2);
        }
        a(false);
        scrollTo(i4, 0);
    }

    public void setOnPageChangeListener(d dVar) {
        this.W = dVar;
    }

    /* access modifiers changed from: package-private */
    public void setChildrenDrawingOrderEnabledCompat(boolean z2) {
        if (this.ac == null) {
            Class<ViewGroup> cls = ViewGroup.class;
            try {
                this.ac = cls.getDeclaredMethod("setChildrenDrawingOrderEnabled", Boolean.TYPE);
            } catch (NoSuchMethodException e2) {
                Log.e("ViewPager", "Can't find setChildrenDrawingOrderEnabled", e2);
            }
        }
        try {
            this.ac.invoke(this, Boolean.valueOf(z2));
        } catch (Exception e3) {
            Log.e("ViewPager", "Error changing children drawing order", e3);
        }
    }

    /* access modifiers changed from: protected */
    public int getChildDrawingOrder(int i2, int i3) {
        if (this.ad == 2) {
            i3 = (i2 - 1) - i3;
        }
        return ((LayoutParams) this.ae.get(i3).getLayoutParams()).f;
    }

    public int getOffscreenPageLimit() {
        return this.y;
    }

    public void setOffscreenPageLimit(int i2) {
        if (i2 < 1) {
            Log.w("ViewPager", "Requested offscreen page limit " + i2 + " too small; defaulting to " + 1);
            i2 = 1;
        }
        if (i2 != this.y) {
            this.y = i2;
            b();
        }
    }

    public void setPageMargin(int i2) {
        int i3 = this.n;
        this.n = i2;
        int width = getWidth();
        a(width, width, i2, i3);
        requestLayout();
    }

    public int getPageMargin() {
        return this.n;
    }

    public void setPageMarginDrawable(Drawable drawable) {
        this.o = drawable;
        if (drawable != null) {
            refreshDrawableState();
        }
        setWillNotDraw(drawable == null);
        invalidate();
    }

    public void setPageMarginDrawable(int i2) {
        setPageMarginDrawable(getContext().getResources().getDrawable(i2));
    }

    /* access modifiers changed from: protected */
    public boolean verifyDrawable(Drawable drawable) {
        return super.verifyDrawable(drawable) || drawable == this.o;
    }

    /* access modifiers changed from: protected */
    public void drawableStateChanged() {
        super.drawableStateChanged();
        Drawable drawable = this.o;
        if (drawable != null && drawable.isStateful()) {
            drawable.setState(getDrawableState());
        }
    }

    /* access modifiers changed from: package-private */
    public float a(float f2) {
        return (float) Math.sin((double) ((float) (((double) (f2 - 0.5f)) * 0.4712389167638204d)));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    /* access modifiers changed from: package-private */
    public void a(int i2, int i3, int i4) {
        int abs;
        if (getChildCount() == 0) {
            setScrollingCacheEnabled(false);
            return;
        }
        int scrollX = getScrollX();
        int scrollY = getScrollY();
        int i5 = i2 - scrollX;
        int i6 = i3 - scrollY;
        if (i5 == 0 && i6 == 0) {
            a(false);
            b();
            setScrollState(0);
            return;
        }
        setScrollingCacheEnabled(true);
        setScrollState(2);
        int width = getWidth();
        int i7 = width / 2;
        float a2 = (((float) i7) * a(Math.min(1.0f, (((float) Math.abs(i5)) * 1.0f) / ((float) width)))) + ((float) i7);
        int abs2 = Math.abs(i4);
        if (abs2 > 0) {
            abs = Math.round(1000.0f * Math.abs(a2 / ((float) abs2))) * 4;
        } else {
            abs = (int) (((((float) Math.abs(i5)) / ((((float) width) * this.g.a(this.h)) + ((float) this.n))) + 1.0f) * 100.0f);
        }
        this.l.startScroll(scrollX, scrollY, i5, i6, Math.min(abs, 600));
        h.b(this);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.e.a(android.view.ViewGroup, int):java.lang.Object
     arg types: [android.support.v4.view.ViewPager, int]
     candidates:
      android.support.v4.view.e.a(android.view.View, int):java.lang.Object
      android.support.v4.view.e.a(android.os.Parcelable, java.lang.ClassLoader):void
      android.support.v4.view.e.a(android.view.View, java.lang.Object):boolean
      android.support.v4.view.e.a(android.view.ViewGroup, int):java.lang.Object */
    /* access modifiers changed from: package-private */
    public b a(int i2, int i3) {
        b bVar = new b();
        bVar.b = i2;
        bVar.a = this.g.a((ViewGroup) this, i2);
        bVar.d = this.g.a(i2);
        if (i3 < 0 || i3 >= this.d.size()) {
            this.d.add(bVar);
        } else {
            this.d.add(i3, bVar);
        }
        return bVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.e.a(android.view.ViewGroup, int, java.lang.Object):void
     arg types: [android.support.v4.view.ViewPager, int, java.lang.Object]
     candidates:
      android.support.v4.view.e.a(android.view.View, int, java.lang.Object):void
      android.support.v4.view.e.a(android.view.ViewGroup, int, java.lang.Object):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.ViewPager.a(int, boolean, boolean):void
     arg types: [int, int, int]
     candidates:
      android.support.v4.view.ViewPager.a(android.support.v4.view.ViewPager$b, int, android.support.v4.view.ViewPager$b):void
      android.support.v4.view.ViewPager.a(int, float, int):void
      android.support.v4.view.ViewPager.a(int, int, int):void
      android.support.v4.view.ViewPager.a(int, boolean, boolean):void */
    /* access modifiers changed from: package-private */
    public void a() {
        int i2;
        boolean z2;
        int i3;
        boolean z3;
        boolean z4 = this.d.size() < (this.y * 2) + 1 && this.d.size() < this.g.a();
        boolean z5 = false;
        int i4 = this.h;
        boolean z6 = z4;
        int i5 = 0;
        while (i5 < this.d.size()) {
            b bVar = this.d.get(i5);
            int a2 = this.g.a(bVar.a);
            if (a2 == -1) {
                i2 = i5;
                z2 = z5;
                i3 = i4;
                z3 = z6;
            } else if (a2 == -2) {
                this.d.remove(i5);
                int i6 = i5 - 1;
                if (!z5) {
                    this.g.a((ViewGroup) this);
                    z5 = true;
                }
                this.g.a((ViewGroup) this, bVar.b, bVar.a);
                if (this.h == bVar.b) {
                    i2 = i6;
                    z2 = z5;
                    i3 = Math.max(0, Math.min(this.h, this.g.a() - 1));
                    z3 = true;
                } else {
                    i2 = i6;
                    z2 = z5;
                    i3 = i4;
                    z3 = true;
                }
            } else if (bVar.b != a2) {
                if (bVar.b == this.h) {
                    i4 = a2;
                }
                bVar.b = a2;
                i2 = i5;
                z2 = z5;
                i3 = i4;
                z3 = true;
            } else {
                i2 = i5;
                z2 = z5;
                i3 = i4;
                z3 = z6;
            }
            z6 = z3;
            i4 = i3;
            z5 = z2;
            i5 = i2 + 1;
        }
        if (z5) {
            this.g.b((ViewGroup) this);
        }
        Collections.sort(this.d, b);
        if (z6) {
            int childCount = getChildCount();
            for (int i7 = 0; i7 < childCount; i7++) {
                LayoutParams layoutParams = (LayoutParams) getChildAt(i7).getLayoutParams();
                if (!layoutParams.a) {
                    layoutParams.c = 0.0f;
                }
            }
            a(i4, false, true);
            requestLayout();
        }
    }

    /* access modifiers changed from: package-private */
    public void b() {
        a(this.h);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.e.b(android.view.ViewGroup, int, java.lang.Object):void
     arg types: [android.support.v4.view.ViewPager, int, java.lang.Object]
     candidates:
      android.support.v4.view.e.b(android.view.View, int, java.lang.Object):void
      android.support.v4.view.e.b(android.view.ViewGroup, int, java.lang.Object):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.e.a(android.view.ViewGroup, int, java.lang.Object):void
     arg types: [android.support.v4.view.ViewPager, int, java.lang.Object]
     candidates:
      android.support.v4.view.e.a(android.view.View, int, java.lang.Object):void
      android.support.v4.view.e.a(android.view.ViewGroup, int, java.lang.Object):void */
    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0058, code lost:
        if (r0.b == r14.h) goto L_0x005a;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(int r15) {
        /*
            r14 = this;
            r0 = 0
            int r1 = r14.h
            if (r1 == r15) goto L_0x024a
            int r0 = r14.h
            android.support.v4.view.ViewPager$b r0 = r14.b(r0)
            r14.h = r15
            r1 = r0
        L_0x000e:
            android.support.v4.view.e r0 = r14.g
            if (r0 != 0) goto L_0x0013
        L_0x0012:
            return
        L_0x0013:
            boolean r0 = r14.x
            if (r0 != 0) goto L_0x0012
            android.os.IBinder r0 = r14.getWindowToken()
            if (r0 == 0) goto L_0x0012
            android.support.v4.view.e r0 = r14.g
            r0.a(r14)
            int r0 = r14.y
            r2 = 0
            int r3 = r14.h
            int r3 = r3 - r0
            int r7 = java.lang.Math.max(r2, r3)
            android.support.v4.view.e r2 = r14.g
            int r8 = r2.a()
            int r2 = r8 + -1
            int r3 = r14.h
            int r0 = r0 + r3
            int r9 = java.lang.Math.min(r2, r0)
            r3 = 0
            r0 = 0
            r2 = r0
        L_0x003e:
            java.util.ArrayList<android.support.v4.view.ViewPager$b> r0 = r14.d
            int r0 = r0.size()
            if (r2 >= r0) goto L_0x0247
            java.util.ArrayList<android.support.v4.view.ViewPager$b> r0 = r14.d
            java.lang.Object r0 = r0.get(r2)
            android.support.v4.view.ViewPager$b r0 = (android.support.v4.view.ViewPager.b) r0
            int r4 = r0.b
            int r5 = r14.h
            if (r4 < r5) goto L_0x011a
            int r4 = r0.b
            int r5 = r14.h
            if (r4 != r5) goto L_0x0247
        L_0x005a:
            if (r0 != 0) goto L_0x0244
            if (r8 <= 0) goto L_0x0244
            int r0 = r14.h
            android.support.v4.view.ViewPager$b r0 = r14.a(r0, r2)
            r6 = r0
        L_0x0065:
            if (r6 == 0) goto L_0x00bf
            r5 = 0
            int r4 = r2 + -1
            if (r4 < 0) goto L_0x011f
            java.util.ArrayList<android.support.v4.view.ViewPager$b> r0 = r14.d
            java.lang.Object r0 = r0.get(r4)
            android.support.v4.view.ViewPager$b r0 = (android.support.v4.view.ViewPager.b) r0
        L_0x0074:
            r3 = 1073741824(0x40000000, float:2.0)
            float r10 = r6.d
            float r10 = r3 - r10
            int r3 = r14.h
            int r3 = r3 + -1
            r12 = r3
            r3 = r5
            r5 = r12
            r13 = r4
            r4 = r2
            r2 = r13
        L_0x0084:
            if (r5 < 0) goto L_0x008e
            int r11 = (r3 > r10 ? 1 : (r3 == r10 ? 0 : -1))
            if (r11 < 0) goto L_0x014a
            if (r5 >= r7) goto L_0x014a
            if (r0 != 0) goto L_0x0122
        L_0x008e:
            float r3 = r6.d
            int r5 = r4 + 1
            r0 = 1073741824(0x40000000, float:2.0)
            int r0 = (r3 > r0 ? 1 : (r3 == r0 ? 0 : -1))
            if (r0 >= 0) goto L_0x00bc
            java.util.ArrayList<android.support.v4.view.ViewPager$b> r0 = r14.d
            int r0 = r0.size()
            if (r5 >= r0) goto L_0x017a
            java.util.ArrayList<android.support.v4.view.ViewPager$b> r0 = r14.d
            java.lang.Object r0 = r0.get(r5)
            android.support.v4.view.ViewPager$b r0 = (android.support.v4.view.ViewPager.b) r0
        L_0x00a8:
            int r2 = r14.h
            int r2 = r2 + 1
            r12 = r2
            r2 = r3
            r3 = r5
            r5 = r12
        L_0x00b0:
            if (r5 >= r8) goto L_0x00bc
            r7 = 1073741824(0x40000000, float:2.0)
            int r7 = (r2 > r7 ? 1 : (r2 == r7 ? 0 : -1))
            if (r7 < 0) goto L_0x01ad
            if (r5 <= r9) goto L_0x01ad
            if (r0 != 0) goto L_0x017d
        L_0x00bc:
            r14.a(r6, r4, r1)
        L_0x00bf:
            android.support.v4.view.e r1 = r14.g
            int r2 = r14.h
            if (r6 == 0) goto L_0x01ed
            java.lang.Object r0 = r6.a
        L_0x00c7:
            r1.b(r14, r2, r0)
            android.support.v4.view.e r0 = r14.g
            r0.b(r14)
            int r0 = r14.ad
            if (r0 == 0) goto L_0x01f0
            r0 = 1
            r2 = r0
        L_0x00d5:
            if (r2 == 0) goto L_0x00e2
            java.util.ArrayList<android.view.View> r0 = r14.ae
            if (r0 != 0) goto L_0x01f4
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r14.ae = r0
        L_0x00e2:
            int r3 = r14.getChildCount()
            r0 = 0
            r1 = r0
        L_0x00e8:
            if (r1 >= r3) goto L_0x01fb
            android.view.View r4 = r14.getChildAt(r1)
            android.view.ViewGroup$LayoutParams r0 = r4.getLayoutParams()
            android.support.v4.view.ViewPager$LayoutParams r0 = (android.support.v4.view.ViewPager.LayoutParams) r0
            r0.f = r1
            boolean r5 = r0.a
            if (r5 != 0) goto L_0x010f
            float r5 = r0.c
            r6 = 0
            int r5 = (r5 > r6 ? 1 : (r5 == r6 ? 0 : -1))
            if (r5 != 0) goto L_0x010f
            android.support.v4.view.ViewPager$b r5 = r14.a(r4)
            if (r5 == 0) goto L_0x010f
            float r6 = r5.d
            r0.c = r6
            int r5 = r5.b
            r0.e = r5
        L_0x010f:
            if (r2 == 0) goto L_0x0116
            java.util.ArrayList<android.view.View> r0 = r14.ae
            r0.add(r4)
        L_0x0116:
            int r0 = r1 + 1
            r1 = r0
            goto L_0x00e8
        L_0x011a:
            int r0 = r2 + 1
            r2 = r0
            goto L_0x003e
        L_0x011f:
            r0 = 0
            goto L_0x0074
        L_0x0122:
            int r11 = r0.b
            if (r5 != r11) goto L_0x0144
            boolean r11 = r0.c
            if (r11 != 0) goto L_0x0144
            java.util.ArrayList<android.support.v4.view.ViewPager$b> r11 = r14.d
            r11.remove(r2)
            android.support.v4.view.e r11 = r14.g
            java.lang.Object r0 = r0.a
            r11.a(r14, r5, r0)
            int r2 = r2 + -1
            int r4 = r4 + -1
            if (r2 < 0) goto L_0x0148
            java.util.ArrayList<android.support.v4.view.ViewPager$b> r0 = r14.d
            java.lang.Object r0 = r0.get(r2)
            android.support.v4.view.ViewPager$b r0 = (android.support.v4.view.ViewPager.b) r0
        L_0x0144:
            int r5 = r5 + -1
            goto L_0x0084
        L_0x0148:
            r0 = 0
            goto L_0x0144
        L_0x014a:
            if (r0 == 0) goto L_0x0162
            int r11 = r0.b
            if (r5 != r11) goto L_0x0162
            float r0 = r0.d
            float r3 = r3 + r0
            int r2 = r2 + -1
            if (r2 < 0) goto L_0x0160
            java.util.ArrayList<android.support.v4.view.ViewPager$b> r0 = r14.d
            java.lang.Object r0 = r0.get(r2)
            android.support.v4.view.ViewPager$b r0 = (android.support.v4.view.ViewPager.b) r0
            goto L_0x0144
        L_0x0160:
            r0 = 0
            goto L_0x0144
        L_0x0162:
            int r0 = r2 + 1
            android.support.v4.view.ViewPager$b r0 = r14.a(r5, r0)
            float r0 = r0.d
            float r3 = r3 + r0
            int r4 = r4 + 1
            if (r2 < 0) goto L_0x0178
            java.util.ArrayList<android.support.v4.view.ViewPager$b> r0 = r14.d
            java.lang.Object r0 = r0.get(r2)
            android.support.v4.view.ViewPager$b r0 = (android.support.v4.view.ViewPager.b) r0
            goto L_0x0144
        L_0x0178:
            r0 = 0
            goto L_0x0144
        L_0x017a:
            r0 = 0
            goto L_0x00a8
        L_0x017d:
            int r7 = r0.b
            if (r5 != r7) goto L_0x023f
            boolean r7 = r0.c
            if (r7 != 0) goto L_0x023f
            java.util.ArrayList<android.support.v4.view.ViewPager$b> r7 = r14.d
            r7.remove(r3)
            android.support.v4.view.e r7 = r14.g
            java.lang.Object r0 = r0.a
            r7.a(r14, r5, r0)
            java.util.ArrayList<android.support.v4.view.ViewPager$b> r0 = r14.d
            int r0 = r0.size()
            if (r3 >= r0) goto L_0x01ab
            java.util.ArrayList<android.support.v4.view.ViewPager$b> r0 = r14.d
            java.lang.Object r0 = r0.get(r3)
            android.support.v4.view.ViewPager$b r0 = (android.support.v4.view.ViewPager.b) r0
        L_0x01a1:
            r12 = r2
            r2 = r0
            r0 = r12
        L_0x01a4:
            int r5 = r5 + 1
            r12 = r0
            r0 = r2
            r2 = r12
            goto L_0x00b0
        L_0x01ab:
            r0 = 0
            goto L_0x01a1
        L_0x01ad:
            if (r0 == 0) goto L_0x01ce
            int r7 = r0.b
            if (r5 != r7) goto L_0x01ce
            float r0 = r0.d
            float r2 = r2 + r0
            int r3 = r3 + 1
            java.util.ArrayList<android.support.v4.view.ViewPager$b> r0 = r14.d
            int r0 = r0.size()
            if (r3 >= r0) goto L_0x01cc
            java.util.ArrayList<android.support.v4.view.ViewPager$b> r0 = r14.d
            java.lang.Object r0 = r0.get(r3)
            android.support.v4.view.ViewPager$b r0 = (android.support.v4.view.ViewPager.b) r0
        L_0x01c8:
            r12 = r2
            r2 = r0
            r0 = r12
            goto L_0x01a4
        L_0x01cc:
            r0 = 0
            goto L_0x01c8
        L_0x01ce:
            android.support.v4.view.ViewPager$b r0 = r14.a(r5, r3)
            int r3 = r3 + 1
            float r0 = r0.d
            float r2 = r2 + r0
            java.util.ArrayList<android.support.v4.view.ViewPager$b> r0 = r14.d
            int r0 = r0.size()
            if (r3 >= r0) goto L_0x01eb
            java.util.ArrayList<android.support.v4.view.ViewPager$b> r0 = r14.d
            java.lang.Object r0 = r0.get(r3)
            android.support.v4.view.ViewPager$b r0 = (android.support.v4.view.ViewPager.b) r0
        L_0x01e7:
            r12 = r2
            r2 = r0
            r0 = r12
            goto L_0x01a4
        L_0x01eb:
            r0 = 0
            goto L_0x01e7
        L_0x01ed:
            r0 = 0
            goto L_0x00c7
        L_0x01f0:
            r0 = 0
            r2 = r0
            goto L_0x00d5
        L_0x01f4:
            java.util.ArrayList<android.view.View> r0 = r14.ae
            r0.clear()
            goto L_0x00e2
        L_0x01fb:
            if (r2 == 0) goto L_0x0204
            java.util.ArrayList<android.view.View> r0 = r14.ae
            android.support.v4.view.ViewPager$g r1 = android.support.v4.view.ViewPager.af
            java.util.Collections.sort(r0, r1)
        L_0x0204:
            boolean r0 = r14.hasFocus()
            if (r0 == 0) goto L_0x0012
            android.view.View r0 = r14.findFocus()
            if (r0 == 0) goto L_0x023d
            android.support.v4.view.ViewPager$b r0 = r14.b(r0)
        L_0x0214:
            if (r0 == 0) goto L_0x021c
            int r0 = r0.b
            int r1 = r14.h
            if (r0 == r1) goto L_0x0012
        L_0x021c:
            r0 = 0
        L_0x021d:
            int r1 = r14.getChildCount()
            if (r0 >= r1) goto L_0x0012
            android.view.View r1 = r14.getChildAt(r0)
            android.support.v4.view.ViewPager$b r2 = r14.a(r1)
            if (r2 == 0) goto L_0x023a
            int r2 = r2.b
            int r3 = r14.h
            if (r2 != r3) goto L_0x023a
            r2 = 2
            boolean r1 = r1.requestFocus(r2)
            if (r1 != 0) goto L_0x0012
        L_0x023a:
            int r0 = r0 + 1
            goto L_0x021d
        L_0x023d:
            r0 = 0
            goto L_0x0214
        L_0x023f:
            r12 = r2
            r2 = r0
            r0 = r12
            goto L_0x01a4
        L_0x0244:
            r6 = r0
            goto L_0x0065
        L_0x0247:
            r0 = r3
            goto L_0x005a
        L_0x024a:
            r1 = r0
            goto L_0x000e
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v4.view.ViewPager.a(int):void");
    }

    private void a(b bVar, int i2, b bVar2) {
        float f2;
        b bVar3;
        b bVar4;
        int a2 = this.g.a();
        int width = getWidth();
        if (width > 0) {
            f2 = ((float) this.n) / ((float) width);
        } else {
            f2 = 0.0f;
        }
        if (bVar2 != null) {
            int i3 = bVar2.b;
            if (i3 < bVar.b) {
                float f3 = bVar2.e + bVar2.d + f2;
                int i4 = i3 + 1;
                int i5 = 0;
                while (i4 <= bVar.b && i5 < this.d.size()) {
                    b bVar5 = this.d.get(i5);
                    while (true) {
                        bVar4 = bVar5;
                        if (i4 > bVar4.b && i5 < this.d.size() - 1) {
                            i5++;
                            bVar5 = this.d.get(i5);
                        }
                    }
                    while (i4 < bVar4.b) {
                        f3 += this.g.a(i4) + f2;
                        i4++;
                    }
                    bVar4.e = f3;
                    f3 += bVar4.d + f2;
                    i4++;
                }
            } else if (i3 > bVar.b) {
                int size = this.d.size() - 1;
                float f4 = bVar2.e;
                int i6 = i3 - 1;
                while (i6 >= bVar.b && size >= 0) {
                    b bVar6 = this.d.get(size);
                    while (true) {
                        bVar3 = bVar6;
                        if (i6 < bVar3.b && size > 0) {
                            size--;
                            bVar6 = this.d.get(size);
                        }
                    }
                    while (i6 > bVar3.b) {
                        f4 -= this.g.a(i6) + f2;
                        i6--;
                    }
                    f4 -= bVar3.d + f2;
                    bVar3.e = f4;
                    i6--;
                }
            }
        }
        int size2 = this.d.size();
        float f5 = bVar.e;
        int i7 = bVar.b - 1;
        this.r = bVar.b == 0 ? bVar.e : -3.4028235E38f;
        this.s = bVar.b == a2 + -1 ? (bVar.e + bVar.d) - 1.0f : Float.MAX_VALUE;
        for (int i8 = i2 - 1; i8 >= 0; i8--) {
            b bVar7 = this.d.get(i8);
            float f6 = f5;
            while (i7 > bVar7.b) {
                f6 -= this.g.a(i7) + f2;
                i7--;
            }
            f5 = f6 - (bVar7.d + f2);
            bVar7.e = f5;
            if (bVar7.b == 0) {
                this.r = f5;
            }
            i7--;
        }
        float f7 = bVar.e + bVar.d + f2;
        int i9 = bVar.b + 1;
        for (int i10 = i2 + 1; i10 < size2; i10++) {
            b bVar8 = this.d.get(i10);
            float f8 = f7;
            while (i9 < bVar8.b) {
                f8 = this.g.a(i9) + f2 + f8;
                i9++;
            }
            if (bVar8.b == a2 - 1) {
                this.s = (bVar8.d + f8) - 1.0f;
            }
            bVar8.e = f8;
            f7 = f8 + bVar8.d + f2;
            i9++;
        }
        this.T = false;
    }

    public static class SavedState extends View.BaseSavedState {
        public static final Parcelable.Creator<SavedState> CREATOR = android.support.v4.b.a.a(new android.support.v4.b.b<SavedState>() {
            /* renamed from: b */
            public SavedState a(Parcel parcel, ClassLoader classLoader) {
                return new SavedState(parcel, classLoader);
            }

            /* renamed from: b */
            public SavedState[] a(int i) {
                return new SavedState[i];
            }
        });
        int a;
        Parcelable b;
        ClassLoader c;

        public SavedState(Parcelable parcelable) {
            super(parcelable);
        }

        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeInt(this.a);
            parcel.writeParcelable(this.b, i);
        }

        public String toString() {
            return "FragmentPager.SavedState{" + Integer.toHexString(System.identityHashCode(this)) + " position=" + this.a + "}";
        }

        SavedState(Parcel parcel, ClassLoader classLoader) {
            super(parcel);
            classLoader = classLoader == null ? getClass().getClassLoader() : classLoader;
            this.a = parcel.readInt();
            this.b = parcel.readParcelable(classLoader);
            this.c = classLoader;
        }
    }

    public Parcelable onSaveInstanceState() {
        SavedState savedState = new SavedState(super.onSaveInstanceState());
        savedState.a = this.h;
        if (this.g != null) {
            savedState.b = this.g.b();
        }
        return savedState;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.ViewPager.a(int, boolean, boolean):void
     arg types: [int, int, int]
     candidates:
      android.support.v4.view.ViewPager.a(android.support.v4.view.ViewPager$b, int, android.support.v4.view.ViewPager$b):void
      android.support.v4.view.ViewPager.a(int, float, int):void
      android.support.v4.view.ViewPager.a(int, int, int):void
      android.support.v4.view.ViewPager.a(int, boolean, boolean):void */
    public void onRestoreInstanceState(Parcelable parcelable) {
        if (!(parcelable instanceof SavedState)) {
            super.onRestoreInstanceState(parcelable);
            return;
        }
        SavedState savedState = (SavedState) parcelable;
        super.onRestoreInstanceState(savedState.getSuperState());
        if (this.g != null) {
            this.g.a(savedState.b, savedState.c);
            a(savedState.a, false, true);
            return;
        }
        this.i = savedState.a;
        this.j = savedState.b;
        this.k = savedState.c;
    }

    public void addView(View view, int i2, ViewGroup.LayoutParams layoutParams) {
        ViewGroup.LayoutParams layoutParams2;
        if (!checkLayoutParams(layoutParams)) {
            layoutParams2 = generateLayoutParams(layoutParams);
        } else {
            layoutParams2 = layoutParams;
        }
        LayoutParams layoutParams3 = (LayoutParams) layoutParams2;
        layoutParams3.a |= view instanceof a;
        if (!this.v) {
            super.addView(view, i2, layoutParams2);
        } else if (layoutParams3 == null || !layoutParams3.a) {
            layoutParams3.d = true;
            addViewInLayout(view, i2, layoutParams2);
        } else {
            throw new IllegalStateException("Cannot add pager decor view during layout");
        }
    }

    /* access modifiers changed from: package-private */
    public b a(View view) {
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 >= this.d.size()) {
                return null;
            }
            b bVar = this.d.get(i3);
            if (this.g.a(view, bVar.a)) {
                return bVar;
            }
            i2 = i3 + 1;
        }
    }

    /* access modifiers changed from: package-private */
    public b b(View view) {
        while (true) {
            ViewParent parent = view.getParent();
            if (parent == this) {
                return a(view);
            }
            if (parent == null || !(parent instanceof View)) {
                return null;
            }
            view = (View) parent;
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public b b(int i2) {
        int i3 = 0;
        while (true) {
            int i4 = i3;
            if (i4 >= this.d.size()) {
                return null;
            }
            b bVar = this.d.get(i4);
            if (bVar.b == i2) {
                return bVar;
            }
            i3 = i4 + 1;
        }
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.S = true;
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x00a0  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00b4  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onMeasure(int r14, int r15) {
        /*
            r13 = this;
            r0 = 0
            int r0 = getDefaultSize(r0, r14)
            r1 = 0
            int r1 = getDefaultSize(r1, r15)
            r13.setMeasuredDimension(r0, r1)
            int r0 = r13.getMeasuredWidth()
            int r1 = r0 / 10
            int r2 = r13.B
            int r1 = java.lang.Math.min(r1, r2)
            r13.C = r1
            int r1 = r13.getPaddingLeft()
            int r0 = r0 - r1
            int r1 = r13.getPaddingRight()
            int r3 = r0 - r1
            int r0 = r13.getMeasuredHeight()
            int r1 = r13.getPaddingTop()
            int r0 = r0 - r1
            int r1 = r13.getPaddingBottom()
            int r5 = r0 - r1
            int r9 = r13.getChildCount()
            r0 = 0
            r8 = r0
        L_0x003b:
            if (r8 >= r9) goto L_0x00bc
            android.view.View r10 = r13.getChildAt(r8)
            int r0 = r10.getVisibility()
            r1 = 8
            if (r0 == r1) goto L_0x00a5
            android.view.ViewGroup$LayoutParams r0 = r10.getLayoutParams()
            android.support.v4.view.ViewPager$LayoutParams r0 = (android.support.v4.view.ViewPager.LayoutParams) r0
            if (r0 == 0) goto L_0x00a5
            boolean r1 = r0.a
            if (r1 == 0) goto L_0x00a5
            int r1 = r0.b
            r6 = r1 & 7
            int r1 = r0.b
            r4 = r1 & 112(0x70, float:1.57E-43)
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r1 = -2147483648(0xffffffff80000000, float:-0.0)
            r7 = 48
            if (r4 == r7) goto L_0x0069
            r7 = 80
            if (r4 != r7) goto L_0x00a9
        L_0x0069:
            r4 = 1
            r7 = r4
        L_0x006b:
            r4 = 3
            if (r6 == r4) goto L_0x0071
            r4 = 5
            if (r6 != r4) goto L_0x00ac
        L_0x0071:
            r4 = 1
            r6 = r4
        L_0x0073:
            if (r7 == 0) goto L_0x00af
            r2 = 1073741824(0x40000000, float:2.0)
        L_0x0077:
            int r4 = r0.width
            r11 = -2
            if (r4 == r11) goto L_0x010f
            r4 = 1073741824(0x40000000, float:2.0)
            int r2 = r0.width
            r11 = -1
            if (r2 == r11) goto L_0x010c
            int r2 = r0.width
        L_0x0085:
            int r11 = r0.height
            r12 = -2
            if (r11 == r12) goto L_0x010a
            r1 = 1073741824(0x40000000, float:2.0)
            int r11 = r0.height
            r12 = -1
            if (r11 == r12) goto L_0x010a
            int r0 = r0.height
        L_0x0093:
            int r2 = android.view.View.MeasureSpec.makeMeasureSpec(r2, r4)
            int r0 = android.view.View.MeasureSpec.makeMeasureSpec(r0, r1)
            r10.measure(r2, r0)
            if (r7 == 0) goto L_0x00b4
            int r0 = r10.getMeasuredHeight()
            int r5 = r5 - r0
        L_0x00a5:
            int r0 = r8 + 1
            r8 = r0
            goto L_0x003b
        L_0x00a9:
            r4 = 0
            r7 = r4
            goto L_0x006b
        L_0x00ac:
            r4 = 0
            r6 = r4
            goto L_0x0073
        L_0x00af:
            if (r6 == 0) goto L_0x0077
            r1 = 1073741824(0x40000000, float:2.0)
            goto L_0x0077
        L_0x00b4:
            if (r6 == 0) goto L_0x00a5
            int r0 = r10.getMeasuredWidth()
            int r3 = r3 - r0
            goto L_0x00a5
        L_0x00bc:
            r0 = 1073741824(0x40000000, float:2.0)
            int r0 = android.view.View.MeasureSpec.makeMeasureSpec(r3, r0)
            r13.t = r0
            r0 = 1073741824(0x40000000, float:2.0)
            int r0 = android.view.View.MeasureSpec.makeMeasureSpec(r5, r0)
            r13.u = r0
            r0 = 1
            r13.v = r0
            r13.b()
            r0 = 0
            r13.v = r0
            int r2 = r13.getChildCount()
            r0 = 0
            r1 = r0
        L_0x00db:
            if (r1 >= r2) goto L_0x0109
            android.view.View r4 = r13.getChildAt(r1)
            int r0 = r4.getVisibility()
            r5 = 8
            if (r0 == r5) goto L_0x0105
            android.view.ViewGroup$LayoutParams r0 = r4.getLayoutParams()
            android.support.v4.view.ViewPager$LayoutParams r0 = (android.support.v4.view.ViewPager.LayoutParams) r0
            if (r0 == 0) goto L_0x00f5
            boolean r5 = r0.a
            if (r5 != 0) goto L_0x0105
        L_0x00f5:
            float r5 = (float) r3
            float r0 = r0.c
            float r0 = r0 * r5
            int r0 = (int) r0
            r5 = 1073741824(0x40000000, float:2.0)
            int r0 = android.view.View.MeasureSpec.makeMeasureSpec(r0, r5)
            int r5 = r13.u
            r4.measure(r0, r5)
        L_0x0105:
            int r0 = r1 + 1
            r1 = r0
            goto L_0x00db
        L_0x0109:
            return
        L_0x010a:
            r0 = r5
            goto L_0x0093
        L_0x010c:
            r2 = r3
            goto L_0x0085
        L_0x010f:
            r4 = r2
            r2 = r3
            goto L_0x0085
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v4.view.ViewPager.onMeasure(int, int):void");
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int i2, int i3, int i4, int i5) {
        super.onSizeChanged(i2, i3, i4, i5);
        if (i2 != i4) {
            a(i2, i4, this.n, this.n);
        }
    }

    private void a(int i2, int i3, int i4, int i5) {
        if (i3 <= 0 || this.d.isEmpty()) {
            b b2 = b(this.h);
            int min = (int) ((b2 != null ? Math.min(b2.e, this.s) : 0.0f) * ((float) i2));
            if (min != getScrollX()) {
                a(false);
                scrollTo(min, getScrollY());
                return;
            }
            return;
        }
        int scrollX = (int) (((float) (i2 + i4)) * (((float) getScrollX()) / ((float) (i3 + i5))));
        scrollTo(scrollX, getScrollY());
        if (!this.l.isFinished()) {
            this.l.startScroll(scrollX, 0, (int) (b(this.h).e * ((float) i2)), 0, this.l.getDuration() - this.l.timePassed());
        }
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z2, int i2, int i3, int i4, int i5) {
        b a2;
        int i6;
        int i7;
        int i8;
        int measuredHeight;
        int i9;
        int i10;
        this.v = true;
        b();
        this.v = false;
        int childCount = getChildCount();
        int i11 = i4 - i2;
        int i12 = i5 - i3;
        int paddingLeft = getPaddingLeft();
        int paddingTop = getPaddingTop();
        int paddingRight = getPaddingRight();
        int paddingBottom = getPaddingBottom();
        int scrollX = getScrollX();
        int i13 = 0;
        int i14 = 0;
        while (i14 < childCount) {
            View childAt = getChildAt(i14);
            if (childAt.getVisibility() != 8) {
                LayoutParams layoutParams = (LayoutParams) childAt.getLayoutParams();
                if (layoutParams.a) {
                    int i15 = layoutParams.b & 7;
                    int i16 = layoutParams.b & 112;
                    switch (i15) {
                        case 1:
                            i8 = Math.max((i11 - childAt.getMeasuredWidth()) / 2, paddingLeft);
                            break;
                        case 2:
                        case e.g.com_facebook_picker_fragment_done_button_text /*4*/:
                        default:
                            i8 = paddingLeft;
                            break;
                        case 3:
                            i8 = paddingLeft;
                            paddingLeft = childAt.getMeasuredWidth() + paddingLeft;
                            break;
                        case e.g.com_facebook_picker_fragment_title_bar_background /*5*/:
                            int measuredWidth = (i11 - paddingRight) - childAt.getMeasuredWidth();
                            paddingRight += childAt.getMeasuredWidth();
                            i8 = measuredWidth;
                            break;
                    }
                    switch (i16) {
                        case 16:
                            measuredHeight = Math.max((i12 - childAt.getMeasuredHeight()) / 2, paddingTop);
                            int i17 = paddingBottom;
                            i9 = paddingTop;
                            i10 = i17;
                            break;
                        case 48:
                            int measuredHeight2 = childAt.getMeasuredHeight() + paddingTop;
                            int i18 = paddingTop;
                            i10 = paddingBottom;
                            i9 = measuredHeight2;
                            measuredHeight = i18;
                            break;
                        case 80:
                            measuredHeight = (i12 - paddingBottom) - childAt.getMeasuredHeight();
                            int measuredHeight3 = paddingBottom + childAt.getMeasuredHeight();
                            i9 = paddingTop;
                            i10 = measuredHeight3;
                            break;
                        default:
                            measuredHeight = paddingTop;
                            int i19 = paddingBottom;
                            i9 = paddingTop;
                            i10 = i19;
                            break;
                    }
                    int i20 = i8 + scrollX;
                    childAt.layout(i20, measuredHeight, childAt.getMeasuredWidth() + i20, childAt.getMeasuredHeight() + measuredHeight);
                    i6 = i13 + 1;
                    i7 = i9;
                    paddingBottom = i10;
                    i14++;
                    paddingLeft = paddingLeft;
                    paddingRight = paddingRight;
                    paddingTop = i7;
                    i13 = i6;
                }
            }
            i6 = i13;
            i7 = paddingTop;
            i14++;
            paddingLeft = paddingLeft;
            paddingRight = paddingRight;
            paddingTop = i7;
            i13 = i6;
        }
        for (int i21 = 0; i21 < childCount; i21++) {
            View childAt2 = getChildAt(i21);
            if (childAt2.getVisibility() != 8) {
                LayoutParams layoutParams2 = (LayoutParams) childAt2.getLayoutParams();
                if (!layoutParams2.a && (a2 = a(childAt2)) != null) {
                    int i22 = ((int) (a2.e * ((float) i11))) + paddingLeft;
                    if (layoutParams2.d) {
                        layoutParams2.d = false;
                        childAt2.measure(View.MeasureSpec.makeMeasureSpec((int) (layoutParams2.c * ((float) ((i11 - paddingLeft) - paddingRight))), 1073741824), View.MeasureSpec.makeMeasureSpec((i12 - paddingTop) - paddingBottom, 1073741824));
                    }
                    childAt2.layout(i22, paddingTop, childAt2.getMeasuredWidth() + i22, childAt2.getMeasuredHeight() + paddingTop);
                }
            }
        }
        this.p = paddingTop;
        this.q = i12 - paddingBottom;
        this.V = i13;
        this.S = false;
    }

    public void computeScroll() {
        if (this.l.isFinished() || !this.l.computeScrollOffset()) {
            a(true);
            return;
        }
        int scrollX = getScrollX();
        int scrollY = getScrollY();
        int currX = this.l.getCurrX();
        int currY = this.l.getCurrY();
        if (!(scrollX == currX && scrollY == currY)) {
            scrollTo(currX, currY);
            if (!d(currX)) {
                this.l.abortAnimation();
                scrollTo(0, currY);
            }
        }
        h.b(this);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.ViewPager.a(int, float, int):void
     arg types: [int, int, int]
     candidates:
      android.support.v4.view.ViewPager.a(android.support.v4.view.ViewPager$b, int, android.support.v4.view.ViewPager$b):void
      android.support.v4.view.ViewPager.a(int, int, int):void
      android.support.v4.view.ViewPager.a(int, boolean, boolean):void
      android.support.v4.view.ViewPager.a(int, float, int):void */
    private boolean d(int i2) {
        if (this.d.size() == 0) {
            this.U = false;
            a(0, 0.0f, 0);
            if (this.U) {
                return false;
            }
            throw new IllegalStateException("onPageScrolled did not call superclass implementation");
        }
        b g2 = g();
        int width = getWidth();
        int i3 = this.n + width;
        float f2 = ((float) this.n) / ((float) width);
        int i4 = g2.b;
        float f3 = ((((float) i2) / ((float) width)) - g2.e) / (g2.d + f2);
        this.U = false;
        a(i4, f3, (int) (((float) i3) * f3));
        if (this.U) {
            return true;
        }
        throw new IllegalStateException("onPageScrolled did not call superclass implementation");
    }

    /* access modifiers changed from: protected */
    public void a(int i2, float f2, int i3) {
        int measuredWidth;
        int i4;
        int i5;
        if (this.V > 0) {
            int scrollX = getScrollX();
            int paddingLeft = getPaddingLeft();
            int paddingRight = getPaddingRight();
            int width = getWidth();
            int childCount = getChildCount();
            int i6 = 0;
            while (i6 < childCount) {
                View childAt = getChildAt(i6);
                LayoutParams layoutParams = (LayoutParams) childAt.getLayoutParams();
                if (!layoutParams.a) {
                    int i7 = paddingRight;
                    i4 = paddingLeft;
                    i5 = i7;
                } else {
                    switch (layoutParams.b & 7) {
                        case 1:
                            measuredWidth = Math.max((width - childAt.getMeasuredWidth()) / 2, paddingLeft);
                            int i8 = paddingRight;
                            i4 = paddingLeft;
                            i5 = i8;
                            break;
                        case 2:
                        case e.g.com_facebook_picker_fragment_done_button_text /*4*/:
                        default:
                            measuredWidth = paddingLeft;
                            int i9 = paddingRight;
                            i4 = paddingLeft;
                            i5 = i9;
                            break;
                        case 3:
                            int width2 = childAt.getWidth() + paddingLeft;
                            int i10 = paddingLeft;
                            i5 = paddingRight;
                            i4 = width2;
                            measuredWidth = i10;
                            break;
                        case e.g.com_facebook_picker_fragment_title_bar_background /*5*/:
                            measuredWidth = (width - paddingRight) - childAt.getMeasuredWidth();
                            int measuredWidth2 = paddingRight + childAt.getMeasuredWidth();
                            i4 = paddingLeft;
                            i5 = measuredWidth2;
                            break;
                    }
                    int left = (measuredWidth + scrollX) - childAt.getLeft();
                    if (left != 0) {
                        childAt.offsetLeftAndRight(left);
                    }
                }
                i6++;
                int i11 = i5;
                paddingLeft = i4;
                paddingRight = i11;
            }
        }
        if (this.N < 0 || i2 < this.N) {
            this.N = i2;
        }
        if (this.O < 0 || FloatMath.ceil(((float) i2) + f2) > ((float) this.O)) {
            this.O = i2 + 1;
        }
        if (this.W != null) {
            this.W.a(i2, f2, i3);
        }
        if (this.Z != null) {
            this.Z.a(i2, f2, i3);
        }
        if (this.ab != null) {
            int scrollX2 = getScrollX();
            int childCount2 = getChildCount();
            for (int i12 = 0; i12 < childCount2; i12++) {
                View childAt2 = getChildAt(i12);
                if (!((LayoutParams) childAt2.getLayoutParams()).a) {
                    this.ab.a(childAt2, ((float) (childAt2.getLeft() - scrollX2)) / ((float) getWidth()));
                }
            }
        }
        this.U = true;
    }

    private void a(boolean z2) {
        boolean z3 = this.ah == 2;
        if (z3) {
            setScrollingCacheEnabled(false);
            this.l.abortAnimation();
            int scrollX = getScrollX();
            int scrollY = getScrollY();
            int currX = this.l.getCurrX();
            int currY = this.l.getCurrY();
            if (!(scrollX == currX && scrollY == currY)) {
                scrollTo(currX, currY);
            }
        }
        this.x = false;
        boolean z4 = z3;
        for (int i2 = 0; i2 < this.d.size(); i2++) {
            b bVar = this.d.get(i2);
            if (bVar.c) {
                bVar.c = false;
                z4 = true;
            }
        }
        if (!z4) {
            return;
        }
        if (z2) {
            h.a(this, this.ag);
        } else {
            this.ag.run();
        }
    }

    private boolean a(float f2, float f3) {
        return (f2 < ((float) this.C) && f3 > 0.0f) || (f2 > ((float) (getWidth() - this.C)) && f3 < 0.0f);
    }

    private void b(boolean z2) {
        int i2;
        int childCount = getChildCount();
        for (int i3 = 0; i3 < childCount; i3++) {
            if (z2) {
                i2 = 2;
            } else {
                i2 = 0;
            }
            h.a(getChildAt(i3), i2, null);
        }
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        int action = motionEvent.getAction() & 255;
        if (action == 3 || action == 1) {
            this.z = false;
            this.A = false;
            this.H = -1;
            if (this.I == null) {
                return false;
            }
            this.I.recycle();
            this.I = null;
            return false;
        }
        if (action != 0) {
            if (this.z) {
                return true;
            }
            if (this.A) {
                return false;
            }
        }
        switch (action) {
            case 0:
                float x2 = motionEvent.getX();
                this.E = x2;
                this.F = x2;
                this.G = motionEvent.getY();
                this.H = c.b(motionEvent, 0);
                this.A = false;
                this.l.computeScrollOffset();
                if (this.ah == 2 && Math.abs(this.l.getFinalX() - this.l.getCurrX()) > this.M) {
                    this.l.abortAnimation();
                    this.x = false;
                    b();
                    this.z = true;
                    setScrollState(1);
                    break;
                } else {
                    a(false);
                    this.z = false;
                    break;
                }
            case 2:
                int i2 = this.H;
                if (i2 != -1) {
                    int a2 = c.a(motionEvent, i2);
                    float c2 = c.c(motionEvent, a2);
                    float f2 = c2 - this.F;
                    float abs = Math.abs(f2);
                    float d2 = c.d(motionEvent, a2);
                    float abs2 = Math.abs(d2 - this.G);
                    if (f2 == 0.0f || a(this.F, f2) || !a(this, false, (int) f2, (int) c2, (int) d2)) {
                        if (abs > ((float) this.D) && abs > abs2) {
                            this.z = true;
                            setScrollState(1);
                            this.F = f2 > 0.0f ? this.E + ((float) this.D) : this.E - ((float) this.D);
                            setScrollingCacheEnabled(true);
                        } else if (abs2 > ((float) this.D)) {
                            this.A = true;
                        }
                        if (this.z && b(c2)) {
                            h.b(this);
                            break;
                        }
                    } else {
                        this.F = c2;
                        this.E = c2;
                        this.G = d2;
                        this.A = true;
                        return false;
                    }
                }
                break;
            case e.g.com_facebook_picker_fragment_done_button_background /*6*/:
                a(motionEvent);
                break;
        }
        if (this.I == null) {
            this.I = VelocityTracker.obtain();
        }
        this.I.addMovement(motionEvent);
        return this.z;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.ViewPager.a(int, boolean, boolean, int):void
     arg types: [int, int, int, int]
     candidates:
      android.support.v4.view.ViewPager.a(int, float, int, int):int
      android.support.v4.view.ViewPager.a(int, int, int, int):void
      android.support.v4.view.ViewPager.a(int, boolean, int, boolean):void
      android.support.v4.view.ViewPager.a(int, boolean, boolean, int):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.ViewPager.a(int, boolean, int, boolean):void
     arg types: [int, int, int, int]
     candidates:
      android.support.v4.view.ViewPager.a(int, float, int, int):int
      android.support.v4.view.ViewPager.a(int, int, int, int):void
      android.support.v4.view.ViewPager.a(int, boolean, boolean, int):void
      android.support.v4.view.ViewPager.a(int, boolean, int, boolean):void */
    public boolean onTouchEvent(MotionEvent motionEvent) {
        float f2;
        boolean z2 = false;
        if (this.P) {
            return true;
        }
        if (motionEvent.getAction() == 0 && motionEvent.getEdgeFlags() != 0) {
            return false;
        }
        if (this.g == null || this.g.a() == 0) {
            return false;
        }
        if (this.I == null) {
            this.I = VelocityTracker.obtain();
        }
        this.I.addMovement(motionEvent);
        switch (motionEvent.getAction() & 255) {
            case 0:
                this.l.abortAnimation();
                this.x = false;
                b();
                this.z = true;
                setScrollState(1);
                float x2 = motionEvent.getX();
                this.E = x2;
                this.F = x2;
                this.H = c.b(motionEvent, 0);
                break;
            case 1:
                if (this.z) {
                    VelocityTracker velocityTracker = this.I;
                    velocityTracker.computeCurrentVelocity(1000, (float) this.K);
                    int a2 = (int) f.a(velocityTracker, this.H);
                    this.x = true;
                    int width = getWidth();
                    int scrollX = getScrollX();
                    b g2 = g();
                    a(a(g2.b, ((((float) scrollX) / ((float) width)) - g2.e) / g2.d, a2, (int) (c.c(motionEvent, c.a(motionEvent, this.H)) - this.E)), true, true, a2);
                    this.H = -1;
                    h();
                    z2 = this.R.c() | this.Q.c();
                    break;
                }
                break;
            case 2:
                if (!this.z) {
                    int a3 = c.a(motionEvent, this.H);
                    float c2 = c.c(motionEvent, a3);
                    float abs = Math.abs(c2 - this.F);
                    float abs2 = Math.abs(c.d(motionEvent, a3) - this.G);
                    if (abs > ((float) this.D) && abs > abs2) {
                        this.z = true;
                        if (c2 - this.E > 0.0f) {
                            f2 = this.E + ((float) this.D);
                        } else {
                            f2 = this.E - ((float) this.D);
                        }
                        this.F = f2;
                        setScrollState(1);
                        setScrollingCacheEnabled(true);
                    }
                }
                if (this.z) {
                    z2 = false | b(c.c(motionEvent, c.a(motionEvent, this.H)));
                    break;
                }
                break;
            case 3:
                if (this.z) {
                    a(this.h, true, 0, false);
                    this.H = -1;
                    h();
                    z2 = this.R.c() | this.Q.c();
                    break;
                }
                break;
            case e.g.com_facebook_picker_fragment_title_bar_background /*5*/:
                int a4 = c.a(motionEvent);
                this.F = c.c(motionEvent, a4);
                this.H = c.b(motionEvent, a4);
                break;
            case e.g.com_facebook_picker_fragment_done_button_background /*6*/:
                a(motionEvent);
                this.F = c.c(motionEvent, c.a(motionEvent, this.H));
                break;
        }
        if (z2) {
            h.b(this);
        }
        return true;
    }

    private boolean b(float f2) {
        boolean z2;
        float f3;
        boolean z3 = true;
        boolean z4 = false;
        this.F = f2;
        float scrollX = ((float) getScrollX()) + (this.F - f2);
        int width = getWidth();
        float f4 = ((float) width) * this.r;
        float f5 = ((float) width) * this.s;
        b bVar = this.d.get(0);
        b bVar2 = this.d.get(this.d.size() - 1);
        if (bVar.b != 0) {
            f4 = bVar.e * ((float) width);
            z2 = false;
        } else {
            z2 = true;
        }
        if (bVar2.b != this.g.a() - 1) {
            f3 = bVar2.e * ((float) width);
            z3 = false;
        } else {
            f3 = f5;
        }
        if (scrollX < f4) {
            if (z2) {
                z4 = this.Q.a(Math.abs(f4 - scrollX) / ((float) width));
            }
        } else if (scrollX > f3) {
            if (z3) {
                z4 = this.R.a(Math.abs(scrollX - f3) / ((float) width));
            }
            f4 = f3;
        } else {
            f4 = scrollX;
        }
        this.F += f4 - ((float) ((int) f4));
        scrollTo((int) f4, getScrollY());
        d((int) f4);
        return z4;
    }

    private b g() {
        float f2;
        int i2;
        b bVar;
        int width = getWidth();
        float scrollX = width > 0 ? ((float) getScrollX()) / ((float) width) : 0.0f;
        if (width > 0) {
            f2 = ((float) this.n) / ((float) width);
        } else {
            f2 = 0.0f;
        }
        float f3 = 0.0f;
        float f4 = 0.0f;
        int i3 = -1;
        int i4 = 0;
        boolean z2 = true;
        b bVar2 = null;
        while (i4 < this.d.size()) {
            b bVar3 = this.d.get(i4);
            if (z2 || bVar3.b == i3 + 1) {
                b bVar4 = bVar3;
                i2 = i4;
                bVar = bVar4;
            } else {
                b bVar5 = this.e;
                bVar5.e = f3 + f4 + f2;
                bVar5.b = i3 + 1;
                bVar5.d = this.g.a(bVar5.b);
                b bVar6 = bVar5;
                i2 = i4 - 1;
                bVar = bVar6;
            }
            float f5 = bVar.e;
            float f6 = bVar.d + f5 + f2;
            if (!z2 && scrollX < f5) {
                return bVar2;
            }
            if (scrollX < f6 || i2 == this.d.size() - 1) {
                return bVar;
            }
            f4 = f5;
            i3 = bVar.b;
            z2 = false;
            f3 = bVar.d;
            bVar2 = bVar;
            i4 = i2 + 1;
        }
        return bVar2;
    }

    private int a(int i2, float f2, int i3, int i4) {
        if (Math.abs(i4) <= this.L || Math.abs(i3) <= this.J) {
            if (this.N >= 0 && this.N < i2 && f2 < 0.5f) {
                i2++;
            } else if (this.O < 0 || this.O <= i2 + 1 || f2 < 0.5f) {
                i2 = (int) (((float) i2) + f2 + 0.5f);
            } else {
                i2--;
            }
        } else if (i3 <= 0) {
            i2++;
        }
        if (this.d.size() > 0) {
            return Math.max(this.d.get(0).b, Math.min(i2, this.d.get(this.d.size() - 1).b));
        }
        return i2;
    }

    public void draw(Canvas canvas) {
        super.draw(canvas);
        boolean z2 = false;
        int a2 = h.a(this);
        if (a2 == 0 || (a2 == 1 && this.g != null && this.g.a() > 1)) {
            if (!this.Q.a()) {
                int save = canvas.save();
                int height = (getHeight() - getPaddingTop()) - getPaddingBottom();
                int width = getWidth();
                canvas.rotate(270.0f);
                canvas.translate((float) ((-height) + getPaddingTop()), this.r * ((float) width));
                this.Q.a(height, width);
                z2 = false | this.Q.a(canvas);
                canvas.restoreToCount(save);
            }
            if (!this.R.a()) {
                int save2 = canvas.save();
                int width2 = getWidth();
                int height2 = (getHeight() - getPaddingTop()) - getPaddingBottom();
                canvas.rotate(90.0f);
                canvas.translate((float) (-getPaddingTop()), (-(this.s + 1.0f)) * ((float) width2));
                this.R.a(height2, width2);
                z2 |= this.R.a(canvas);
                canvas.restoreToCount(save2);
            }
        } else {
            this.Q.b();
            this.R.b();
        }
        if (z2) {
            h.b(this);
        }
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        float f2;
        super.onDraw(canvas);
        if (this.n > 0 && this.o != null && this.d.size() > 0 && this.g != null) {
            int scrollX = getScrollX();
            int width = getWidth();
            float f3 = ((float) this.n) / ((float) width);
            b bVar = this.d.get(0);
            float f4 = bVar.e;
            int size = this.d.size();
            int i2 = bVar.b;
            int i3 = this.d.get(size - 1).b;
            int i4 = 0;
            int i5 = i2;
            while (i5 < i3) {
                while (i5 > bVar.b && i4 < size) {
                    i4++;
                    bVar = this.d.get(i4);
                }
                if (i5 == bVar.b) {
                    f2 = (bVar.e + bVar.d) * ((float) width);
                    f4 = bVar.e + bVar.d + f3;
                } else {
                    float a2 = this.g.a(i5);
                    f2 = (f4 + a2) * ((float) width);
                    f4 += a2 + f3;
                }
                if (((float) this.n) + f2 > ((float) scrollX)) {
                    this.o.setBounds((int) f2, this.p, (int) (((float) this.n) + f2 + 0.5f), this.q);
                    this.o.draw(canvas);
                }
                if (f2 <= ((float) (scrollX + width))) {
                    i5++;
                } else {
                    return;
                }
            }
        }
    }

    private void a(MotionEvent motionEvent) {
        int a2 = c.a(motionEvent);
        if (c.b(motionEvent, a2) == this.H) {
            int i2 = a2 == 0 ? 1 : 0;
            this.F = c.c(motionEvent, i2);
            this.H = c.b(motionEvent, i2);
            if (this.I != null) {
                this.I.clear();
            }
        }
    }

    private void h() {
        this.z = false;
        this.A = false;
        if (this.I != null) {
            this.I.recycle();
            this.I = null;
        }
    }

    private void setScrollingCacheEnabled(boolean z2) {
        if (this.w != z2) {
            this.w = z2;
        }
    }

    /* access modifiers changed from: protected */
    public boolean a(View view, boolean z2, int i2, int i3, int i4) {
        if (view instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) view;
            int scrollX = view.getScrollX();
            int scrollY = view.getScrollY();
            for (int childCount = viewGroup.getChildCount() - 1; childCount >= 0; childCount--) {
                View childAt = viewGroup.getChildAt(childCount);
                if (i3 + scrollX >= childAt.getLeft() && i3 + scrollX < childAt.getRight() && i4 + scrollY >= childAt.getTop() && i4 + scrollY < childAt.getBottom()) {
                    if (a(childAt, true, i2, (i3 + scrollX) - childAt.getLeft(), (i4 + scrollY) - childAt.getTop())) {
                        return true;
                    }
                }
            }
        }
        if (!z2 || !h.a(view, -i2)) {
            return false;
        }
        return true;
    }

    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        return super.dispatchKeyEvent(keyEvent) || a(keyEvent);
    }

    public boolean a(KeyEvent keyEvent) {
        if (keyEvent.getAction() != 0) {
            return false;
        }
        switch (keyEvent.getKeyCode()) {
            case 21:
                return c(17);
            case 22:
                return c(66);
            case 61:
                if (Build.VERSION.SDK_INT < 11) {
                    return false;
                }
                if (a.a(keyEvent)) {
                    return c(2);
                }
                if (a.a(keyEvent, 1)) {
                    return c(1);
                }
                return false;
            default:
                return false;
        }
    }

    public boolean c(int i2) {
        boolean z2;
        View findFocus = findFocus();
        if (findFocus == this) {
            findFocus = null;
        }
        View findNextFocus = FocusFinder.getInstance().findNextFocus(this, findFocus, i2);
        if (findNextFocus == null || findNextFocus == findFocus) {
            if (i2 == 17 || i2 == 1) {
                z2 = c();
            } else {
                if (i2 == 66 || i2 == 2) {
                    z2 = d();
                }
                z2 = false;
            }
        } else if (i2 == 17) {
            z2 = (findFocus == null || a(this.f, findNextFocus).left < a(this.f, findFocus).left) ? findNextFocus.requestFocus() : c();
        } else {
            if (i2 == 66) {
                z2 = (findFocus == null || a(this.f, findNextFocus).left > a(this.f, findFocus).left) ? findNextFocus.requestFocus() : d();
            }
            z2 = false;
        }
        if (z2) {
            playSoundEffect(SoundEffectConstants.getContantForFocusDirection(i2));
        }
        return z2;
    }

    private Rect a(Rect rect, View view) {
        Rect rect2;
        if (rect == null) {
            rect2 = new Rect();
        } else {
            rect2 = rect;
        }
        if (view == null) {
            rect2.set(0, 0, 0, 0);
            return rect2;
        }
        rect2.left = view.getLeft();
        rect2.right = view.getRight();
        rect2.top = view.getTop();
        rect2.bottom = view.getBottom();
        ViewParent parent = view.getParent();
        while ((parent instanceof ViewGroup) && parent != this) {
            ViewGroup viewGroup = (ViewGroup) parent;
            rect2.left += viewGroup.getLeft();
            rect2.right += viewGroup.getRight();
            rect2.top += viewGroup.getTop();
            rect2.bottom += viewGroup.getBottom();
            parent = viewGroup.getParent();
        }
        return rect2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.ViewPager.a(int, boolean):void
     arg types: [int, int]
     candidates:
      android.support.v4.view.ViewPager.a(android.graphics.Rect, android.view.View):android.graphics.Rect
      android.support.v4.view.ViewPager.a(float, float):boolean
      android.support.v4.view.ViewPager.a(int, int):android.support.v4.view.ViewPager$b
      android.support.v4.view.ViewPager.a(int, boolean):void */
    /* access modifiers changed from: package-private */
    public boolean c() {
        if (this.h <= 0) {
            return false;
        }
        a(this.h - 1, true);
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.ViewPager.a(int, boolean):void
     arg types: [int, int]
     candidates:
      android.support.v4.view.ViewPager.a(android.graphics.Rect, android.view.View):android.graphics.Rect
      android.support.v4.view.ViewPager.a(float, float):boolean
      android.support.v4.view.ViewPager.a(int, int):android.support.v4.view.ViewPager$b
      android.support.v4.view.ViewPager.a(int, boolean):void */
    /* access modifiers changed from: package-private */
    public boolean d() {
        if (this.g == null || this.h >= this.g.a() - 1) {
            return false;
        }
        a(this.h + 1, true);
        return true;
    }

    public void addFocusables(ArrayList<View> arrayList, int i2, int i3) {
        b a2;
        int size = arrayList.size();
        int descendantFocusability = getDescendantFocusability();
        if (descendantFocusability != 393216) {
            for (int i4 = 0; i4 < getChildCount(); i4++) {
                View childAt = getChildAt(i4);
                if (childAt.getVisibility() == 0 && (a2 = a(childAt)) != null && a2.b == this.h) {
                    childAt.addFocusables(arrayList, i2, i3);
                }
            }
        }
        if ((descendantFocusability == 262144 && size != arrayList.size()) || !isFocusable()) {
            return;
        }
        if (((i3 & 1) != 1 || !isInTouchMode() || isFocusableInTouchMode()) && arrayList != null) {
            arrayList.add(this);
        }
    }

    public void addTouchables(ArrayList<View> arrayList) {
        b a2;
        for (int i2 = 0; i2 < getChildCount(); i2++) {
            View childAt = getChildAt(i2);
            if (childAt.getVisibility() == 0 && (a2 = a(childAt)) != null && a2.b == this.h) {
                childAt.addTouchables(arrayList);
            }
        }
    }

    /* access modifiers changed from: protected */
    public boolean onRequestFocusInDescendants(int i2, Rect rect) {
        int i3;
        b a2;
        int i4 = -1;
        int childCount = getChildCount();
        if ((i2 & 2) != 0) {
            i4 = 1;
            i3 = 0;
        } else {
            i3 = childCount - 1;
            childCount = -1;
        }
        while (i3 != childCount) {
            View childAt = getChildAt(i3);
            if (childAt.getVisibility() == 0 && (a2 = a(childAt)) != null && a2.b == this.h && childAt.requestFocus(i2, rect)) {
                return true;
            }
            i3 += i4;
        }
        return false;
    }

    public boolean dispatchPopulateAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
        b a2;
        int childCount = getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            View childAt = getChildAt(i2);
            if (childAt.getVisibility() == 0 && (a2 = a(childAt)) != null && a2.b == this.h && childAt.dispatchPopulateAccessibilityEvent(accessibilityEvent)) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public ViewGroup.LayoutParams generateDefaultLayoutParams() {
        return new LayoutParams();
    }

    /* access modifiers changed from: protected */
    public ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return generateDefaultLayoutParams();
    }

    /* access modifiers changed from: protected */
    public boolean checkLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return (layoutParams instanceof LayoutParams) && super.checkLayoutParams(layoutParams);
    }

    public ViewGroup.LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        return new LayoutParams(getContext(), attributeSet);
    }

    private class f extends DataSetObserver {
        private f() {
        }

        public void onChanged() {
            ViewPager.this.a();
        }

        public void onInvalidated() {
            ViewPager.this.a();
        }
    }

    public static class LayoutParams extends ViewGroup.LayoutParams {
        public boolean a;
        public int b;
        float c = 0.0f;
        boolean d;
        int e;
        int f;

        public LayoutParams() {
            super(-1, -1);
        }

        public LayoutParams(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, ViewPager.a);
            this.b = obtainStyledAttributes.getInteger(0, 48);
            obtainStyledAttributes.recycle();
        }
    }

    static class g implements Comparator<View> {
        g() {
        }

        /* renamed from: a */
        public int compare(View view, View view2) {
            LayoutParams layoutParams = (LayoutParams) view.getLayoutParams();
            LayoutParams layoutParams2 = (LayoutParams) view2.getLayoutParams();
            if (layoutParams.a != layoutParams2.a) {
                return layoutParams.a ? 1 : -1;
            }
            return layoutParams.e - layoutParams2.e;
        }
    }
}
