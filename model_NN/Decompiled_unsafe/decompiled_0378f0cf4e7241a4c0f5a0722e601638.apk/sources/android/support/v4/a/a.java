package android.support.v4.a;

import java.io.FileDescriptor;
import java.io.PrintWriter;

/* compiled from: Loader */
public class a<D> {
    int a;
    C0000a<D> b;
    boolean c;
    boolean d;
    boolean e;
    boolean f;

    /* renamed from: android.support.v4.a.a$a  reason: collision with other inner class name */
    /* compiled from: Loader */
    public interface C0000a<D> {
    }

    public void a(int i, C0000a<D> aVar) {
        if (this.b != null) {
            throw new IllegalStateException("There is already a listener registered");
        }
        this.b = aVar;
        this.a = i;
    }

    public void a(C0000a<D> aVar) {
        if (this.b == null) {
            throw new IllegalStateException("No listener register");
        } else if (this.b != aVar) {
            throw new IllegalArgumentException("Attempting to unregister the wrong listener");
        } else {
            this.b = null;
        }
    }

    public final void a() {
        this.c = true;
        this.e = false;
        this.d = false;
        b();
    }

    /* access modifiers changed from: protected */
    public void b() {
    }

    public void c() {
        this.c = false;
        d();
    }

    /* access modifiers changed from: protected */
    public void d() {
    }

    public void e() {
        f();
        this.e = true;
        this.c = false;
        this.d = false;
        this.f = false;
    }

    /* access modifiers changed from: protected */
    public void f() {
    }

    public String a(D d2) {
        StringBuilder sb = new StringBuilder(64);
        android.support.v4.c.a.a(d2, sb);
        sb.append("}");
        return sb.toString();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder(64);
        android.support.v4.c.a.a(this, sb);
        sb.append(" id=");
        sb.append(this.a);
        sb.append("}");
        return sb.toString();
    }

    public void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        printWriter.print(str);
        printWriter.print("mId=");
        printWriter.print(this.a);
        printWriter.print(" mListener=");
        printWriter.println(this.b);
        printWriter.print(str);
        printWriter.print("mStarted=");
        printWriter.print(this.c);
        printWriter.print(" mContentChanged=");
        printWriter.print(this.f);
        printWriter.print(" mAbandoned=");
        printWriter.print(this.d);
        printWriter.print(" mReset=");
        printWriter.println(this.e);
    }
}
