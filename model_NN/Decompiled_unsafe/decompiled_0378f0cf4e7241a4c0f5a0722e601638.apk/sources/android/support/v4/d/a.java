package android.support.v4.d;

import android.graphics.Canvas;
import android.os.Build;

/* compiled from: EdgeEffectCompat */
public class a {
    private static final c b;
    private Object a;

    /* compiled from: EdgeEffectCompat */
    interface c {
        void a(Object obj, int i, int i2);

        boolean a(Object obj);

        boolean a(Object obj, float f);

        boolean a(Object obj, Canvas canvas);

        void b(Object obj);

        boolean c(Object obj);
    }

    static {
        if (Build.VERSION.SDK_INT >= 14) {
            b = new b();
        } else {
            b = new C0003a();
        }
    }

    /* renamed from: android.support.v4.d.a$a  reason: collision with other inner class name */
    /* compiled from: EdgeEffectCompat */
    static class C0003a implements c {
        C0003a() {
        }

        public void a(Object obj, int i, int i2) {
        }

        public boolean a(Object obj) {
            return true;
        }

        public void b(Object obj) {
        }

        public boolean a(Object obj, float f) {
            return false;
        }

        public boolean c(Object obj) {
            return false;
        }

        public boolean a(Object obj, Canvas canvas) {
            return false;
        }
    }

    /* compiled from: EdgeEffectCompat */
    static class b implements c {
        b() {
        }

        public void a(Object obj, int i, int i2) {
            b.a(obj, i, i2);
        }

        public boolean a(Object obj) {
            return b.a(obj);
        }

        public void b(Object obj) {
            b.b(obj);
        }

        public boolean a(Object obj, float f) {
            return b.a(obj, f);
        }

        public boolean c(Object obj) {
            return b.c(obj);
        }

        public boolean a(Object obj, Canvas canvas) {
            return b.a(obj, canvas);
        }
    }

    public void a(int i, int i2) {
        b.a(this.a, i, i2);
    }

    public boolean a() {
        return b.a(this.a);
    }

    public void b() {
        b.b(this.a);
    }

    public boolean a(float f) {
        return b.a(this.a, f);
    }

    public boolean c() {
        return b.c(this.a);
    }

    public boolean a(Canvas canvas) {
        return b.a(this.a, canvas);
    }
}
