package android.support.v4.b;

import android.os.Parcel;

/* compiled from: ParcelableCompatCreatorCallbacks */
public interface b<T> {
    T a(Parcel parcel, ClassLoader classLoader);

    T[] a(int i);
}
