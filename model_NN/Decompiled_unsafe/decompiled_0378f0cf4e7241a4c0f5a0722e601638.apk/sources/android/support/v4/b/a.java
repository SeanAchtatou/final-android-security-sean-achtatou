package android.support.v4.b;

import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;

/* compiled from: ParcelableCompat */
public class a {
    public static <T> Parcelable.Creator<T> a(b<T> bVar) {
        if (Build.VERSION.SDK_INT >= 13) {
            d.a(bVar);
        }
        return new C0002a(bVar);
    }

    /* renamed from: android.support.v4.b.a$a  reason: collision with other inner class name */
    /* compiled from: ParcelableCompat */
    static class C0002a<T> implements Parcelable.Creator<T> {
        final b<T> a;

        public C0002a(b<T> bVar) {
            this.a = bVar;
        }

        public T createFromParcel(Parcel parcel) {
            return this.a.a(parcel, null);
        }

        public T[] newArray(int i) {
            return this.a.a(i);
        }
    }
}
