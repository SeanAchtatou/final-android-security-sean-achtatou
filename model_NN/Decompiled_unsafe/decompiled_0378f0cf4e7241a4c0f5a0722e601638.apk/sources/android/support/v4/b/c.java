package android.support.v4.b;

import android.os.Parcel;
import android.os.Parcelable;

/* compiled from: ParcelableCompatHoneycombMR2 */
class c<T> implements Parcelable.ClassLoaderCreator<T> {
    private final b<T> a;

    public c(b<T> bVar) {
        this.a = bVar;
    }

    public T createFromParcel(Parcel parcel) {
        return this.a.a(parcel, null);
    }

    public T createFromParcel(Parcel parcel, ClassLoader classLoader) {
        return this.a.a(parcel, classLoader);
    }

    public T[] newArray(int i) {
        return this.a.a(i);
    }
}
