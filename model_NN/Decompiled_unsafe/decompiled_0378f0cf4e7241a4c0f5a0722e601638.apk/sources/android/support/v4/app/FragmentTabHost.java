package android.support.v4.app;

import android.content.Context;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.view.View;
import android.widget.TabHost;
import java.util.ArrayList;

public class FragmentTabHost extends TabHost implements TabHost.OnTabChangeListener {
    private final ArrayList<a> a;
    private Context b;
    private d c;
    private int d;
    private TabHost.OnTabChangeListener e;
    private a f;
    private boolean g;

    static final class a {
        /* access modifiers changed from: private */
        public final String a;
        /* access modifiers changed from: private */
        public final Class<?> b;
        /* access modifiers changed from: private */
        public final Bundle c;
        /* access modifiers changed from: private */
        public Fragment d;
    }

    static class SavedState extends View.BaseSavedState {
        public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator<SavedState>() {
            /* renamed from: a */
            public SavedState createFromParcel(Parcel parcel) {
                return new SavedState(parcel);
            }

            /* renamed from: a */
            public SavedState[] newArray(int i) {
                return new SavedState[i];
            }
        };
        String a;

        SavedState(Parcelable parcelable) {
            super(parcelable);
        }

        private SavedState(Parcel parcel) {
            super(parcel);
            this.a = parcel.readString();
        }

        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeString(this.a);
        }

        public String toString() {
            return "FragmentTabHost.SavedState{" + Integer.toHexString(System.identityHashCode(this)) + " curTab=" + this.a + "}";
        }
    }

    @Deprecated
    public void setup() {
        throw new IllegalStateException("Must call setup() that takes a Context and FragmentManager");
    }

    public void setOnTabChangedListener(TabHost.OnTabChangeListener onTabChangeListener) {
        this.e = onTabChangeListener;
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        String currentTabTag = getCurrentTabTag();
        f fVar = null;
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= this.a.size()) {
                break;
            }
            a aVar = this.a.get(i2);
            Fragment unused = aVar.d = this.c.a(aVar.a);
            if (aVar.d != null && !aVar.d.d()) {
                if (aVar.a.equals(currentTabTag)) {
                    this.f = aVar;
                } else {
                    if (fVar == null) {
                        fVar = this.c.a();
                    }
                    fVar.a(aVar.d);
                }
            }
            i = i2 + 1;
        }
        this.g = true;
        f a2 = a(currentTabTag, fVar);
        if (a2 != null) {
            a2.a();
            this.c.b();
        }
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.g = false;
    }

    /* access modifiers changed from: protected */
    public Parcelable onSaveInstanceState() {
        SavedState savedState = new SavedState(super.onSaveInstanceState());
        savedState.a = getCurrentTabTag();
        return savedState;
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Parcelable parcelable) {
        SavedState savedState = (SavedState) parcelable;
        super.onRestoreInstanceState(savedState.getSuperState());
        setCurrentTabByTag(savedState.a);
    }

    public void onTabChanged(String str) {
        f a2;
        if (this.g && (a2 = a(str, null)) != null) {
            a2.a();
        }
        if (this.e != null) {
            this.e.onTabChanged(str);
        }
    }

    private f a(String str, f fVar) {
        a aVar = null;
        int i = 0;
        while (i < this.a.size()) {
            a aVar2 = this.a.get(i);
            if (!aVar2.a.equals(str)) {
                aVar2 = aVar;
            }
            i++;
            aVar = aVar2;
        }
        if (aVar == null) {
            throw new IllegalStateException("No tab known for tag " + str);
        }
        if (this.f != aVar) {
            if (fVar == null) {
                fVar = this.c.a();
            }
            if (!(this.f == null || this.f.d == null)) {
                fVar.a(this.f.d);
            }
            if (aVar != null) {
                if (aVar.d == null) {
                    Fragment unused = aVar.d = Fragment.a(this.b, aVar.b.getName(), aVar.c);
                    fVar.a(this.d, aVar.d, aVar.a);
                } else {
                    fVar.b(aVar.d);
                }
            }
            this.f = aVar;
        }
        return fVar;
    }
}
