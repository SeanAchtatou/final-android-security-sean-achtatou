package android.support.v4.app;

import android.content.Context;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.support.v4.app.d;
import android.support.v4.c.a;
import android.support.v4.c.b;
import android.util.Log;
import android.util.SparseArray;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.view.animation.ScaleAnimation;
import com.facebook.a.e;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;

/* compiled from: FragmentManager */
final class e extends d {
    static final Interpolator A = new DecelerateInterpolator(1.5f);
    static final Interpolator B = new AccelerateInterpolator(2.5f);
    static final Interpolator C = new AccelerateInterpolator(1.5f);
    static boolean a = false;
    static final boolean b;
    static final Interpolator z = new DecelerateInterpolator(2.5f);
    ArrayList<Runnable> c;
    Runnable[] d;
    boolean e;
    ArrayList<Fragment> f;
    ArrayList<Fragment> g;
    ArrayList<Integer> h;
    ArrayList<b> i;
    ArrayList<Fragment> j;
    ArrayList<b> k;
    ArrayList<Integer> l;
    ArrayList<d.a> m;
    int n = 0;
    FragmentActivity o;
    c p;
    Fragment q;
    boolean r;
    boolean s;
    boolean t;
    String u;
    boolean v;
    Bundle w = null;
    SparseArray<Parcelable> x = null;
    Runnable y = new Runnable() {
        public void run() {
            e.this.e();
        }
    };

    e() {
    }

    static {
        boolean z2 = false;
        if (Build.VERSION.SDK_INT >= 11) {
            z2 = true;
        }
        b = z2;
    }

    private void a(RuntimeException runtimeException) {
        Log.e("FragmentManager", runtimeException.getMessage());
        Log.e("FragmentManager", "Activity state:");
        PrintWriter printWriter = new PrintWriter(new b("FragmentManager"));
        if (this.o != null) {
            try {
                this.o.dump("  ", null, printWriter, new String[0]);
            } catch (Exception e2) {
                Log.e("FragmentManager", "Failed dumping state", e2);
            }
        } else {
            try {
                a("  ", (FileDescriptor) null, printWriter, new String[0]);
            } catch (Exception e3) {
                Log.e("FragmentManager", "Failed dumping state", e3);
            }
        }
        throw runtimeException;
    }

    public f a() {
        return new b(this);
    }

    public boolean b() {
        return e();
    }

    public boolean c() {
        t();
        b();
        return a(this.o.a, (String) null, -1, 0);
    }

    public void a(Bundle bundle, String str, Fragment fragment) {
        if (fragment.f < 0) {
            a(new IllegalStateException("Fragment " + fragment + " is not currently in the FragmentManager"));
        }
        bundle.putInt(str, fragment.f);
    }

    public Fragment a(Bundle bundle, String str) {
        int i2 = bundle.getInt(str, -1);
        if (i2 == -1) {
            return null;
        }
        if (i2 >= this.f.size()) {
            a(new IllegalStateException("Fragement no longer exists for key " + str + ": index " + i2));
        }
        Fragment fragment = this.f.get(i2);
        if (fragment != null) {
            return fragment;
        }
        a(new IllegalStateException("Fragement no longer exists for key " + str + ": index " + i2));
        return fragment;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder(128);
        sb.append("FragmentManager{");
        sb.append(Integer.toHexString(System.identityHashCode(this)));
        sb.append(" in ");
        if (this.q != null) {
            a.a(this.q, sb);
        } else {
            a.a(this.o, sb);
        }
        sb.append("}}");
        return sb.toString();
    }

    public void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        int size;
        int size2;
        int size3;
        int size4;
        int size5;
        int size6;
        String str2 = str + "    ";
        if (this.f != null && (size6 = this.f.size()) > 0) {
            printWriter.print(str);
            printWriter.print("Active Fragments in ");
            printWriter.print(Integer.toHexString(System.identityHashCode(this)));
            printWriter.println(":");
            for (int i2 = 0; i2 < size6; i2++) {
                Fragment fragment = this.f.get(i2);
                printWriter.print(str);
                printWriter.print("  #");
                printWriter.print(i2);
                printWriter.print(": ");
                printWriter.println(fragment);
                if (fragment != null) {
                    fragment.a(str2, fileDescriptor, printWriter, strArr);
                }
            }
        }
        if (this.g != null && (size5 = this.g.size()) > 0) {
            printWriter.print(str);
            printWriter.println("Added Fragments:");
            for (int i3 = 0; i3 < size5; i3++) {
                printWriter.print(str);
                printWriter.print("  #");
                printWriter.print(i3);
                printWriter.print(": ");
                printWriter.println(this.g.get(i3).toString());
            }
        }
        if (this.j != null && (size4 = this.j.size()) > 0) {
            printWriter.print(str);
            printWriter.println("Fragments Created Menus:");
            for (int i4 = 0; i4 < size4; i4++) {
                printWriter.print(str);
                printWriter.print("  #");
                printWriter.print(i4);
                printWriter.print(": ");
                printWriter.println(this.j.get(i4).toString());
            }
        }
        if (this.i != null && (size3 = this.i.size()) > 0) {
            printWriter.print(str);
            printWriter.println("Back Stack:");
            for (int i5 = 0; i5 < size3; i5++) {
                b bVar = this.i.get(i5);
                printWriter.print(str);
                printWriter.print("  #");
                printWriter.print(i5);
                printWriter.print(": ");
                printWriter.println(bVar.toString());
                bVar.a(str2, fileDescriptor, printWriter, strArr);
            }
        }
        synchronized (this) {
            if (this.k != null && (size2 = this.k.size()) > 0) {
                printWriter.print(str);
                printWriter.println("Back Stack Indices:");
                for (int i6 = 0; i6 < size2; i6++) {
                    printWriter.print(str);
                    printWriter.print("  #");
                    printWriter.print(i6);
                    printWriter.print(": ");
                    printWriter.println(this.k.get(i6));
                }
            }
            if (this.l != null && this.l.size() > 0) {
                printWriter.print(str);
                printWriter.print("mAvailBackStackIndices: ");
                printWriter.println(Arrays.toString(this.l.toArray()));
            }
        }
        if (this.c != null && (size = this.c.size()) > 0) {
            printWriter.print(str);
            printWriter.println("Pending Actions:");
            for (int i7 = 0; i7 < size; i7++) {
                printWriter.print(str);
                printWriter.print("  #");
                printWriter.print(i7);
                printWriter.print(": ");
                printWriter.println(this.c.get(i7));
            }
        }
        printWriter.print(str);
        printWriter.println("FragmentManager misc state:");
        printWriter.print(str);
        printWriter.print("  mActivity=");
        printWriter.println(this.o);
        printWriter.print(str);
        printWriter.print("  mContainer=");
        printWriter.println(this.p);
        if (this.q != null) {
            printWriter.print(str);
            printWriter.print("  mParent=");
            printWriter.println(this.q);
        }
        printWriter.print(str);
        printWriter.print("  mCurState=");
        printWriter.print(this.n);
        printWriter.print(" mStateSaved=");
        printWriter.print(this.s);
        printWriter.print(" mDestroyed=");
        printWriter.println(this.t);
        if (this.r) {
            printWriter.print(str);
            printWriter.print("  mNeedMenuInvalidate=");
            printWriter.println(this.r);
        }
        if (this.u != null) {
            printWriter.print(str);
            printWriter.print("  mNoTransactionsBecause=");
            printWriter.println(this.u);
        }
        if (this.h != null && this.h.size() > 0) {
            printWriter.print(str);
            printWriter.print("  mAvailIndices: ");
            printWriter.println(Arrays.toString(this.h.toArray()));
        }
    }

    static Animation a(Context context, float f2, float f3, float f4, float f5) {
        AnimationSet animationSet = new AnimationSet(false);
        ScaleAnimation scaleAnimation = new ScaleAnimation(f2, f3, f2, f3, 1, 0.5f, 1, 0.5f);
        scaleAnimation.setInterpolator(z);
        scaleAnimation.setDuration(220);
        animationSet.addAnimation(scaleAnimation);
        AlphaAnimation alphaAnimation = new AlphaAnimation(f4, f5);
        alphaAnimation.setInterpolator(A);
        alphaAnimation.setDuration(220);
        animationSet.addAnimation(alphaAnimation);
        return animationSet;
    }

    static Animation a(Context context, float f2, float f3) {
        AlphaAnimation alphaAnimation = new AlphaAnimation(f2, f3);
        alphaAnimation.setInterpolator(A);
        alphaAnimation.setDuration(220);
        return alphaAnimation;
    }

    /* access modifiers changed from: package-private */
    public Animation a(Fragment fragment, int i2, boolean z2, int i3) {
        Animation loadAnimation;
        Animation a2 = fragment.a(i2, z2, fragment.G);
        if (a2 != null) {
            return a2;
        }
        if (fragment.G != 0 && (loadAnimation = AnimationUtils.loadAnimation(this.o, fragment.G)) != null) {
            return loadAnimation;
        }
        if (i2 == 0) {
            return null;
        }
        int b2 = b(i2, z2);
        if (b2 < 0) {
            return null;
        }
        switch (b2) {
            case 1:
                return a(this.o, 1.125f, 1.0f, 0.0f, 1.0f);
            case 2:
                return a(this.o, 1.0f, 0.975f, 1.0f, 0.0f);
            case 3:
                return a(this.o, 0.975f, 1.0f, 0.0f, 1.0f);
            case e.g.com_facebook_picker_fragment_done_button_text /*4*/:
                return a(this.o, 1.0f, 1.075f, 1.0f, 0.0f);
            case e.g.com_facebook_picker_fragment_title_bar_background /*5*/:
                return a(this.o, 0.0f, 1.0f);
            case e.g.com_facebook_picker_fragment_done_button_background /*6*/:
                return a(this.o, 1.0f, 0.0f);
            default:
                if (i3 == 0 && this.o.getWindow() != null) {
                    i3 = this.o.getWindow().getAttributes().windowAnimations;
                }
                if (i3 == 0) {
                    return null;
                }
                return null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.e.a(android.support.v4.app.Fragment, int, int, int, boolean):void
     arg types: [android.support.v4.app.Fragment, int, int, int, int]
     candidates:
      android.support.v4.app.e.a(android.content.Context, float, float, float, float):android.view.animation.Animation
      android.support.v4.app.e.a(android.support.v4.app.Fragment, int, int, int, boolean):void */
    public void a(Fragment fragment) {
        if (!fragment.K) {
            return;
        }
        if (this.e) {
            this.v = true;
            return;
        }
        fragment.K = false;
        a(fragment, this.n, 0, 0, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.e.a(android.support.v4.app.Fragment, int, int, int, boolean):void
     arg types: [android.support.v4.app.Fragment, int, int, int, int]
     candidates:
      android.support.v4.app.e.a(android.content.Context, float, float, float, float):android.view.animation.Animation
      android.support.v4.app.e.a(android.support.v4.app.Fragment, int, int, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.e.a(android.support.v4.app.Fragment, int, boolean, int):android.view.animation.Animation
     arg types: [android.support.v4.app.Fragment, int, int, int]
     candidates:
      android.support.v4.app.e.a(int, int, int, boolean):void
      android.support.v4.app.e.a(java.lang.String, java.io.FileDescriptor, java.io.PrintWriter, java.lang.String[]):void
      android.support.v4.app.e.a(android.os.Handler, java.lang.String, int, int):boolean
      android.support.v4.app.e.a(android.support.v4.app.Fragment, int, boolean, int):android.view.animation.Animation */
    /* access modifiers changed from: package-private */
    public void a(final Fragment fragment, int i2, int i3, int i4, boolean z2) {
        Animation animation;
        ViewGroup viewGroup;
        if ((!fragment.l || fragment.A) && i2 > 1) {
            i2 = 1;
        }
        if (fragment.m && i2 > fragment.a) {
            i2 = fragment.a;
        }
        if (fragment.K && fragment.a < 4 && i2 > 3) {
            i2 = 3;
        }
        if (fragment.a >= i2) {
            if (fragment.a > i2) {
                switch (fragment.a) {
                    case e.g.com_facebook_picker_fragment_title_bar_background /*5*/:
                        if (i2 < 5) {
                            if (a) {
                                Log.v("FragmentManager", "movefrom RESUMED: " + fragment);
                            }
                            fragment.r();
                            fragment.n = false;
                        }
                    case e.g.com_facebook_picker_fragment_done_button_text /*4*/:
                        if (i2 < 4) {
                            if (a) {
                                Log.v("FragmentManager", "movefrom STARTED: " + fragment);
                            }
                            fragment.s();
                        }
                    case 3:
                        if (i2 < 3) {
                            if (a) {
                                Log.v("FragmentManager", "movefrom STOPPED: " + fragment);
                            }
                            fragment.t();
                        }
                    case 2:
                        if (i2 < 2) {
                            if (a) {
                                Log.v("FragmentManager", "movefrom ACTIVITY_CREATED: " + fragment);
                            }
                            if (fragment.I != null && !this.o.isFinishing() && fragment.e == null) {
                                e(fragment);
                            }
                            fragment.u();
                            if (!(fragment.I == null || fragment.H == null)) {
                                if (this.n <= 0 || this.t) {
                                    animation = null;
                                } else {
                                    animation = a(fragment, i3, false, i4);
                                }
                                if (animation != null) {
                                    fragment.b = fragment.I;
                                    fragment.c = i2;
                                    animation.setAnimationListener(new Animation.AnimationListener() {
                                        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                                         method: android.support.v4.app.e.a(android.support.v4.app.Fragment, int, int, int, boolean):void
                                         arg types: [android.support.v4.app.Fragment, int, int, int, int]
                                         candidates:
                                          android.support.v4.app.e.a(android.content.Context, float, float, float, float):android.view.animation.Animation
                                          android.support.v4.app.e.a(android.support.v4.app.Fragment, int, int, int, boolean):void */
                                        public void onAnimationEnd(Animation animation) {
                                            if (fragment.b != null) {
                                                fragment.b = null;
                                                e.this.a(fragment, fragment.c, 0, 0, false);
                                            }
                                        }

                                        public void onAnimationRepeat(Animation animation) {
                                        }

                                        public void onAnimationStart(Animation animation) {
                                        }
                                    });
                                    fragment.I.startAnimation(animation);
                                }
                                fragment.H.removeView(fragment.I);
                            }
                            fragment.H = null;
                            fragment.I = null;
                            fragment.J = null;
                        }
                        break;
                    case 1:
                        if (i2 < 1) {
                            if (this.t && fragment.b != null) {
                                View view = fragment.b;
                                fragment.b = null;
                                view.clearAnimation();
                            }
                            if (fragment.b == null) {
                                if (a) {
                                    Log.v("FragmentManager", "movefrom CREATED: " + fragment);
                                }
                                if (!fragment.C) {
                                    fragment.v();
                                }
                                fragment.F = false;
                                fragment.l();
                                if (fragment.F) {
                                    if (!z2) {
                                        if (fragment.C) {
                                            fragment.t = null;
                                            fragment.s = null;
                                            break;
                                        } else {
                                            d(fragment);
                                            break;
                                        }
                                    }
                                } else {
                                    throw new j("Fragment " + fragment + " did not call through to super.onDetach()");
                                }
                            } else {
                                fragment.c = i2;
                                i2 = 1;
                                break;
                            }
                        }
                        break;
                }
            }
        } else if (!fragment.o || fragment.p) {
            if (fragment.b != null) {
                fragment.b = null;
                a(fragment, fragment.c, 0, 0, true);
            }
            switch (fragment.a) {
                case 0:
                    if (a) {
                        Log.v("FragmentManager", "moveto CREATED: " + fragment);
                    }
                    if (fragment.d != null) {
                        fragment.e = fragment.d.getSparseParcelableArray("android:view_state");
                        fragment.i = a(fragment.d, "android:target_state");
                        if (fragment.i != null) {
                            fragment.k = fragment.d.getInt("android:target_req_state", 0);
                        }
                        fragment.L = fragment.d.getBoolean("android:user_visible_hint", true);
                        if (!fragment.L) {
                            fragment.K = true;
                            if (i2 > 3) {
                                i2 = 3;
                            }
                        }
                    }
                    fragment.t = this.o;
                    fragment.v = this.q;
                    fragment.s = this.q != null ? this.q.u : this.o.b;
                    fragment.F = false;
                    fragment.a(this.o);
                    if (!fragment.F) {
                        throw new j("Fragment " + fragment + " did not call through to super.onAttach()");
                    }
                    if (fragment.v == null) {
                        this.o.a(fragment);
                    }
                    if (!fragment.C) {
                        fragment.g(fragment.d);
                    }
                    fragment.C = false;
                    if (fragment.o) {
                        fragment.I = fragment.b(fragment.b(fragment.d), null, fragment.d);
                        if (fragment.I != null) {
                            fragment.J = fragment.I;
                            fragment.I = i.a(fragment.I);
                            if (fragment.z) {
                                fragment.I.setVisibility(8);
                            }
                            fragment.a(fragment.I, fragment.d);
                        } else {
                            fragment.J = null;
                        }
                    }
                case 1:
                    if (i2 > 1) {
                        if (a) {
                            Log.v("FragmentManager", "moveto ACTIVITY_CREATED: " + fragment);
                        }
                        if (!fragment.o) {
                            if (fragment.x != 0) {
                                viewGroup = (ViewGroup) this.p.a(fragment.x);
                                if (viewGroup == null && !fragment.q) {
                                    a(new IllegalArgumentException("No view found for id 0x" + Integer.toHexString(fragment.x) + " (" + fragment.c().getResourceName(fragment.x) + ") for fragment " + fragment));
                                }
                            } else {
                                viewGroup = null;
                            }
                            fragment.H = viewGroup;
                            fragment.I = fragment.b(fragment.b(fragment.d), viewGroup, fragment.d);
                            if (fragment.I != null) {
                                fragment.J = fragment.I;
                                fragment.I = i.a(fragment.I);
                                if (viewGroup != null) {
                                    Animation a2 = a(fragment, i3, true, i4);
                                    if (a2 != null) {
                                        fragment.I.startAnimation(a2);
                                    }
                                    viewGroup.addView(fragment.I);
                                }
                                if (fragment.z) {
                                    fragment.I.setVisibility(8);
                                }
                                fragment.a(fragment.I, fragment.d);
                            } else {
                                fragment.J = null;
                            }
                        }
                        fragment.h(fragment.d);
                        if (fragment.I != null) {
                            fragment.a(fragment.d);
                        }
                        fragment.d = null;
                    }
                case 2:
                case 3:
                    if (i2 > 3) {
                        if (a) {
                            Log.v("FragmentManager", "moveto STARTED: " + fragment);
                        }
                        fragment.o();
                    }
                case e.g.com_facebook_picker_fragment_done_button_text /*4*/:
                    if (i2 > 4) {
                        if (a) {
                            Log.v("FragmentManager", "moveto RESUMED: " + fragment);
                        }
                        fragment.n = true;
                        fragment.p();
                        fragment.d = null;
                        fragment.e = null;
                        break;
                    }
                    break;
            }
        } else {
            return;
        }
        fragment.a = i2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.e.a(android.support.v4.app.Fragment, int, int, int, boolean):void
     arg types: [android.support.v4.app.Fragment, int, int, int, int]
     candidates:
      android.support.v4.app.e.a(android.content.Context, float, float, float, float):android.view.animation.Animation
      android.support.v4.app.e.a(android.support.v4.app.Fragment, int, int, int, boolean):void */
    /* access modifiers changed from: package-private */
    public void b(Fragment fragment) {
        a(fragment, this.n, 0, 0, false);
    }

    /* access modifiers changed from: package-private */
    public void a(int i2, boolean z2) {
        a(i2, 0, 0, z2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.e.a(android.support.v4.app.Fragment, int, int, int, boolean):void
     arg types: [android.support.v4.app.Fragment, int, int, int, int]
     candidates:
      android.support.v4.app.e.a(android.content.Context, float, float, float, float):android.view.animation.Animation
      android.support.v4.app.e.a(android.support.v4.app.Fragment, int, int, int, boolean):void */
    /* access modifiers changed from: package-private */
    public void a(int i2, int i3, int i4, boolean z2) {
        if (this.o == null && i2 != 0) {
            throw new IllegalStateException("No activity");
        } else if (z2 || this.n != i2) {
            this.n = i2;
            if (this.f != null) {
                int i5 = 0;
                boolean z3 = false;
                while (i5 < this.f.size()) {
                    Fragment fragment = this.f.get(i5);
                    if (fragment != null) {
                        a(fragment, i2, i3, i4, false);
                        if (fragment.M != null) {
                            z3 |= fragment.M.a();
                        }
                    }
                    i5++;
                    z3 = z3;
                }
                if (!z3) {
                    d();
                }
                if (this.r && this.o != null && this.n == 5) {
                    this.o.c();
                    this.r = false;
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void d() {
        if (this.f != null) {
            int i2 = 0;
            while (true) {
                int i3 = i2;
                if (i3 < this.f.size()) {
                    Fragment fragment = this.f.get(i3);
                    if (fragment != null) {
                        a(fragment);
                    }
                    i2 = i3 + 1;
                } else {
                    return;
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void c(Fragment fragment) {
        if (fragment.f < 0) {
            if (this.h == null || this.h.size() <= 0) {
                if (this.f == null) {
                    this.f = new ArrayList<>();
                }
                fragment.a(this.f.size(), this.q);
                this.f.add(fragment);
            } else {
                fragment.a(this.h.remove(this.h.size() - 1).intValue(), this.q);
                this.f.set(fragment.f, fragment);
            }
            if (a) {
                Log.v("FragmentManager", "Allocated fragment index " + fragment);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void d(Fragment fragment) {
        if (fragment.f >= 0) {
            if (a) {
                Log.v("FragmentManager", "Freeing fragment index " + fragment);
            }
            this.f.set(fragment.f, null);
            if (this.h == null) {
                this.h = new ArrayList<>();
            }
            this.h.add(Integer.valueOf(fragment.f));
            this.o.a(fragment.g);
            fragment.k();
        }
    }

    public void a(Fragment fragment, boolean z2) {
        if (this.g == null) {
            this.g = new ArrayList<>();
        }
        if (a) {
            Log.v("FragmentManager", "add: " + fragment);
        }
        c(fragment);
        if (fragment.A) {
            return;
        }
        if (this.g.contains(fragment)) {
            throw new IllegalStateException("Fragment already added: " + fragment);
        }
        this.g.add(fragment);
        fragment.l = true;
        fragment.m = false;
        if (fragment.D && fragment.E) {
            this.r = true;
        }
        if (z2) {
            b(fragment);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.e.a(android.support.v4.app.Fragment, int, int, int, boolean):void
     arg types: [android.support.v4.app.Fragment, int, int, int, int]
     candidates:
      android.support.v4.app.e.a(android.content.Context, float, float, float, float):android.view.animation.Animation
      android.support.v4.app.e.a(android.support.v4.app.Fragment, int, int, int, boolean):void */
    public void a(Fragment fragment, int i2, int i3) {
        int i4;
        if (a) {
            Log.v("FragmentManager", "remove: " + fragment + " nesting=" + fragment.r);
        }
        boolean z2 = !fragment.a();
        if (!fragment.A || z2) {
            if (this.g != null) {
                this.g.remove(fragment);
            }
            if (fragment.D && fragment.E) {
                this.r = true;
            }
            fragment.l = false;
            fragment.m = true;
            if (z2) {
                i4 = 0;
            } else {
                i4 = 1;
            }
            a(fragment, i4, i2, i3, false);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.e.a(android.support.v4.app.Fragment, int, boolean, int):android.view.animation.Animation
     arg types: [android.support.v4.app.Fragment, int, int, int]
     candidates:
      android.support.v4.app.e.a(int, int, int, boolean):void
      android.support.v4.app.e.a(java.lang.String, java.io.FileDescriptor, java.io.PrintWriter, java.lang.String[]):void
      android.support.v4.app.e.a(android.os.Handler, java.lang.String, int, int):boolean
      android.support.v4.app.e.a(android.support.v4.app.Fragment, int, boolean, int):android.view.animation.Animation */
    public void b(Fragment fragment, int i2, int i3) {
        if (a) {
            Log.v("FragmentManager", "hide: " + fragment);
        }
        if (!fragment.z) {
            fragment.z = true;
            if (fragment.I != null) {
                Animation a2 = a(fragment, i2, true, i3);
                if (a2 != null) {
                    fragment.I.startAnimation(a2);
                }
                fragment.I.setVisibility(8);
            }
            if (fragment.l && fragment.D && fragment.E) {
                this.r = true;
            }
            fragment.a(true);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.e.a(android.support.v4.app.Fragment, int, boolean, int):android.view.animation.Animation
     arg types: [android.support.v4.app.Fragment, int, int, int]
     candidates:
      android.support.v4.app.e.a(int, int, int, boolean):void
      android.support.v4.app.e.a(java.lang.String, java.io.FileDescriptor, java.io.PrintWriter, java.lang.String[]):void
      android.support.v4.app.e.a(android.os.Handler, java.lang.String, int, int):boolean
      android.support.v4.app.e.a(android.support.v4.app.Fragment, int, boolean, int):android.view.animation.Animation */
    public void c(Fragment fragment, int i2, int i3) {
        if (a) {
            Log.v("FragmentManager", "show: " + fragment);
        }
        if (fragment.z) {
            fragment.z = false;
            if (fragment.I != null) {
                Animation a2 = a(fragment, i2, true, i3);
                if (a2 != null) {
                    fragment.I.startAnimation(a2);
                }
                fragment.I.setVisibility(0);
            }
            if (fragment.l && fragment.D && fragment.E) {
                this.r = true;
            }
            fragment.a(false);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.e.a(android.support.v4.app.Fragment, int, int, int, boolean):void
     arg types: [android.support.v4.app.Fragment, int, int, int, int]
     candidates:
      android.support.v4.app.e.a(android.content.Context, float, float, float, float):android.view.animation.Animation
      android.support.v4.app.e.a(android.support.v4.app.Fragment, int, int, int, boolean):void */
    public void d(Fragment fragment, int i2, int i3) {
        if (a) {
            Log.v("FragmentManager", "detach: " + fragment);
        }
        if (!fragment.A) {
            fragment.A = true;
            if (fragment.l) {
                if (this.g != null) {
                    if (a) {
                        Log.v("FragmentManager", "remove from detach: " + fragment);
                    }
                    this.g.remove(fragment);
                }
                if (fragment.D && fragment.E) {
                    this.r = true;
                }
                fragment.l = false;
                a(fragment, 1, i2, i3, false);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.e.a(android.support.v4.app.Fragment, int, int, int, boolean):void
     arg types: [android.support.v4.app.Fragment, int, int, int, int]
     candidates:
      android.support.v4.app.e.a(android.content.Context, float, float, float, float):android.view.animation.Animation
      android.support.v4.app.e.a(android.support.v4.app.Fragment, int, int, int, boolean):void */
    public void e(Fragment fragment, int i2, int i3) {
        if (a) {
            Log.v("FragmentManager", "attach: " + fragment);
        }
        if (fragment.A) {
            fragment.A = false;
            if (!fragment.l) {
                if (this.g == null) {
                    this.g = new ArrayList<>();
                }
                if (this.g.contains(fragment)) {
                    throw new IllegalStateException("Fragment already added: " + fragment);
                }
                if (a) {
                    Log.v("FragmentManager", "add from attach: " + fragment);
                }
                this.g.add(fragment);
                fragment.l = true;
                if (fragment.D && fragment.E) {
                    this.r = true;
                }
                a(fragment, this.n, i2, i3, false);
            }
        }
    }

    public Fragment a(int i2) {
        if (this.g != null) {
            for (int size = this.g.size() - 1; size >= 0; size--) {
                Fragment fragment = this.g.get(size);
                if (fragment != null && fragment.w == i2) {
                    return fragment;
                }
            }
        }
        if (this.f != null) {
            for (int size2 = this.f.size() - 1; size2 >= 0; size2--) {
                Fragment fragment2 = this.f.get(size2);
                if (fragment2 != null && fragment2.w == i2) {
                    return fragment2;
                }
            }
        }
        return null;
    }

    public Fragment a(String str) {
        if (!(this.g == null || str == null)) {
            for (int size = this.g.size() - 1; size >= 0; size--) {
                Fragment fragment = this.g.get(size);
                if (fragment != null && str.equals(fragment.y)) {
                    return fragment;
                }
            }
        }
        if (!(this.f == null || str == null)) {
            for (int size2 = this.f.size() - 1; size2 >= 0; size2--) {
                Fragment fragment2 = this.f.get(size2);
                if (fragment2 != null && str.equals(fragment2.y)) {
                    return fragment2;
                }
            }
        }
        return null;
    }

    private void t() {
        if (this.s) {
            throw new IllegalStateException("Can not perform this action after onSaveInstanceState");
        } else if (this.u != null) {
            throw new IllegalStateException("Can not perform this action inside of " + this.u);
        }
    }

    public void a(Runnable runnable, boolean z2) {
        if (!z2) {
            t();
        }
        synchronized (this) {
            if (this.o == null) {
                throw new IllegalStateException("Activity has been destroyed");
            }
            if (this.c == null) {
                this.c = new ArrayList<>();
            }
            this.c.add(runnable);
            if (this.c.size() == 1) {
                this.o.a.removeCallbacks(this.y);
                this.o.a.post(this.y);
            }
        }
    }

    public int a(b bVar) {
        int i2;
        synchronized (this) {
            if (this.l == null || this.l.size() <= 0) {
                if (this.k == null) {
                    this.k = new ArrayList<>();
                }
                i2 = this.k.size();
                if (a) {
                    Log.v("FragmentManager", "Setting back stack index " + i2 + " to " + bVar);
                }
                this.k.add(bVar);
            } else {
                i2 = this.l.remove(this.l.size() - 1).intValue();
                if (a) {
                    Log.v("FragmentManager", "Adding back stack index " + i2 + " with " + bVar);
                }
                this.k.set(i2, bVar);
            }
        }
        return i2;
    }

    public void a(int i2, b bVar) {
        synchronized (this) {
            if (this.k == null) {
                this.k = new ArrayList<>();
            }
            int size = this.k.size();
            if (i2 < size) {
                if (a) {
                    Log.v("FragmentManager", "Setting back stack index " + i2 + " to " + bVar);
                }
                this.k.set(i2, bVar);
            } else {
                while (size < i2) {
                    this.k.add(null);
                    if (this.l == null) {
                        this.l = new ArrayList<>();
                    }
                    if (a) {
                        Log.v("FragmentManager", "Adding available back stack index " + size);
                    }
                    this.l.add(Integer.valueOf(size));
                    size++;
                }
                if (a) {
                    Log.v("FragmentManager", "Adding back stack index " + i2 + " with " + bVar);
                }
                this.k.add(bVar);
            }
        }
    }

    public void b(int i2) {
        synchronized (this) {
            this.k.set(i2, null);
            if (this.l == null) {
                this.l = new ArrayList<>();
            }
            if (a) {
                Log.v("FragmentManager", "Freeing back stack index " + i2);
            }
            this.l.add(Integer.valueOf(i2));
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:35:0x0083, code lost:
        r6.e = true;
        r1 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x0086, code lost:
        if (r1 >= r3) goto L_0x009a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x0088, code lost:
        r6.d[r1].run();
        r6.d[r1] = null;
        r1 = r1 + 1;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean e() {
        /*
            r6 = this;
            r0 = 1
            r2 = 0
            boolean r1 = r6.e
            if (r1 == 0) goto L_0x000e
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "Recursive entry to executePendingTransactions"
            r0.<init>(r1)
            throw r0
        L_0x000e:
            android.os.Looper r1 = android.os.Looper.myLooper()
            android.support.v4.app.FragmentActivity r3 = r6.o
            android.os.Handler r3 = r3.a
            android.os.Looper r3 = r3.getLooper()
            if (r1 == r3) goto L_0x0024
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "Must be called from main thread of process"
            r0.<init>(r1)
            throw r0
        L_0x0024:
            r1 = r2
        L_0x0025:
            monitor-enter(r6)
            java.util.ArrayList<java.lang.Runnable> r3 = r6.c     // Catch:{ all -> 0x0097 }
            if (r3 == 0) goto L_0x0032
            java.util.ArrayList<java.lang.Runnable> r3 = r6.c     // Catch:{ all -> 0x0097 }
            int r3 = r3.size()     // Catch:{ all -> 0x0097 }
            if (r3 != 0) goto L_0x005a
        L_0x0032:
            monitor-exit(r6)     // Catch:{ all -> 0x0097 }
            boolean r0 = r6.v
            if (r0 == 0) goto L_0x00a5
            r3 = r2
            r4 = r2
        L_0x0039:
            java.util.ArrayList<android.support.v4.app.Fragment> r0 = r6.f
            int r0 = r0.size()
            if (r3 >= r0) goto L_0x009e
            java.util.ArrayList<android.support.v4.app.Fragment> r0 = r6.f
            java.lang.Object r0 = r0.get(r3)
            android.support.v4.app.Fragment r0 = (android.support.v4.app.Fragment) r0
            if (r0 == 0) goto L_0x0056
            android.support.v4.app.h r5 = r0.M
            if (r5 == 0) goto L_0x0056
            android.support.v4.app.h r0 = r0.M
            boolean r0 = r0.a()
            r4 = r4 | r0
        L_0x0056:
            int r0 = r3 + 1
            r3 = r0
            goto L_0x0039
        L_0x005a:
            java.util.ArrayList<java.lang.Runnable> r1 = r6.c     // Catch:{ all -> 0x0097 }
            int r3 = r1.size()     // Catch:{ all -> 0x0097 }
            java.lang.Runnable[] r1 = r6.d     // Catch:{ all -> 0x0097 }
            if (r1 == 0) goto L_0x0069
            java.lang.Runnable[] r1 = r6.d     // Catch:{ all -> 0x0097 }
            int r1 = r1.length     // Catch:{ all -> 0x0097 }
            if (r1 >= r3) goto L_0x006d
        L_0x0069:
            java.lang.Runnable[] r1 = new java.lang.Runnable[r3]     // Catch:{ all -> 0x0097 }
            r6.d = r1     // Catch:{ all -> 0x0097 }
        L_0x006d:
            java.util.ArrayList<java.lang.Runnable> r1 = r6.c     // Catch:{ all -> 0x0097 }
            java.lang.Runnable[] r4 = r6.d     // Catch:{ all -> 0x0097 }
            r1.toArray(r4)     // Catch:{ all -> 0x0097 }
            java.util.ArrayList<java.lang.Runnable> r1 = r6.c     // Catch:{ all -> 0x0097 }
            r1.clear()     // Catch:{ all -> 0x0097 }
            android.support.v4.app.FragmentActivity r1 = r6.o     // Catch:{ all -> 0x0097 }
            android.os.Handler r1 = r1.a     // Catch:{ all -> 0x0097 }
            java.lang.Runnable r4 = r6.y     // Catch:{ all -> 0x0097 }
            r1.removeCallbacks(r4)     // Catch:{ all -> 0x0097 }
            monitor-exit(r6)     // Catch:{ all -> 0x0097 }
            r6.e = r0
            r1 = r2
        L_0x0086:
            if (r1 >= r3) goto L_0x009a
            java.lang.Runnable[] r4 = r6.d
            r4 = r4[r1]
            r4.run()
            java.lang.Runnable[] r4 = r6.d
            r5 = 0
            r4[r1] = r5
            int r1 = r1 + 1
            goto L_0x0086
        L_0x0097:
            r0 = move-exception
            monitor-exit(r6)     // Catch:{ all -> 0x0097 }
            throw r0
        L_0x009a:
            r6.e = r2
            r1 = r0
            goto L_0x0025
        L_0x009e:
            if (r4 != 0) goto L_0x00a5
            r6.v = r2
            r6.d()
        L_0x00a5:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v4.app.e.e():boolean");
    }

    /* access modifiers changed from: package-private */
    public void f() {
        if (this.m != null) {
            int i2 = 0;
            while (true) {
                int i3 = i2;
                if (i3 < this.m.size()) {
                    this.m.get(i3).a();
                    i2 = i3 + 1;
                } else {
                    return;
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void b(b bVar) {
        if (this.i == null) {
            this.i = new ArrayList<>();
        }
        this.i.add(bVar);
        f();
    }

    /* access modifiers changed from: package-private */
    public boolean a(Handler handler, String str, int i2, int i3) {
        boolean z2;
        int i4;
        if (this.i == null) {
            return false;
        }
        if (str == null && i2 < 0 && (i3 & 1) == 0) {
            int size = this.i.size() - 1;
            if (size < 0) {
                return false;
            }
            this.i.remove(size).b(true);
            f();
        } else {
            int i5 = -1;
            if (str != null || i2 >= 0) {
                int size2 = this.i.size() - 1;
                while (i4 >= 0) {
                    b bVar = this.i.get(i4);
                    if ((str != null && str.equals(bVar.b())) || (i2 >= 0 && i2 == bVar.o)) {
                        break;
                    }
                    size2 = i4 - 1;
                }
                if (i4 < 0) {
                    return false;
                }
                if ((i3 & 1) != 0) {
                    i4--;
                    while (i4 >= 0) {
                        b bVar2 = this.i.get(i4);
                        if ((str == null || !str.equals(bVar2.b())) && (i2 < 0 || i2 != bVar2.o)) {
                            break;
                        }
                        i4--;
                    }
                }
                i5 = i4;
            }
            if (i5 == this.i.size() - 1) {
                return false;
            }
            ArrayList arrayList = new ArrayList();
            for (int size3 = this.i.size() - 1; size3 > i5; size3--) {
                arrayList.add(this.i.remove(size3));
            }
            int size4 = arrayList.size() - 1;
            for (int i6 = 0; i6 <= size4; i6++) {
                if (a) {
                    Log.v("FragmentManager", "Popping back stack state: " + arrayList.get(i6));
                }
                b bVar3 = (b) arrayList.get(i6);
                if (i6 == size4) {
                    z2 = true;
                } else {
                    z2 = false;
                }
                bVar3.b(z2);
            }
            f();
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    public ArrayList<Fragment> g() {
        ArrayList<Fragment> arrayList = null;
        if (this.f != null) {
            int i2 = 0;
            while (true) {
                int i3 = i2;
                if (i3 >= this.f.size()) {
                    break;
                }
                Fragment fragment = this.f.get(i3);
                if (fragment != null && fragment.B) {
                    if (arrayList == null) {
                        arrayList = new ArrayList<>();
                    }
                    arrayList.add(fragment);
                    fragment.C = true;
                    fragment.j = fragment.i != null ? fragment.i.f : -1;
                    if (a) {
                        Log.v("FragmentManager", "retainNonConfig: keeping retained " + fragment);
                    }
                }
                i2 = i3 + 1;
            }
        }
        return arrayList;
    }

    /* access modifiers changed from: package-private */
    public void e(Fragment fragment) {
        if (fragment.J != null) {
            if (this.x == null) {
                this.x = new SparseArray<>();
            } else {
                this.x.clear();
            }
            fragment.J.saveHierarchyState(this.x);
            if (this.x.size() > 0) {
                fragment.e = this.x;
                this.x = null;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public Bundle f(Fragment fragment) {
        Bundle bundle;
        if (this.w == null) {
            this.w = new Bundle();
        }
        fragment.i(this.w);
        if (!this.w.isEmpty()) {
            bundle = this.w;
            this.w = null;
        } else {
            bundle = null;
        }
        if (fragment.I != null) {
            e(fragment);
        }
        if (fragment.e != null) {
            if (bundle == null) {
                bundle = new Bundle();
            }
            bundle.putSparseParcelableArray("android:view_state", fragment.e);
        }
        if (!fragment.L) {
            if (bundle == null) {
                bundle = new Bundle();
            }
            bundle.putBoolean("android:user_visible_hint", fragment.L);
        }
        return bundle;
    }

    /* access modifiers changed from: package-private */
    public Parcelable h() {
        int[] iArr;
        int size;
        int size2;
        boolean z2;
        BackStackState[] backStackStateArr = null;
        e();
        if (b) {
            this.s = true;
        }
        if (this.f == null || this.f.size() <= 0) {
            return null;
        }
        int size3 = this.f.size();
        FragmentState[] fragmentStateArr = new FragmentState[size3];
        int i2 = 0;
        boolean z3 = false;
        while (i2 < size3) {
            Fragment fragment = this.f.get(i2);
            if (fragment != null) {
                if (fragment.f < 0) {
                    a(new IllegalStateException("Failure saving state: active " + fragment + " has cleared index: " + fragment.f));
                }
                FragmentState fragmentState = new FragmentState(fragment);
                fragmentStateArr[i2] = fragmentState;
                if (fragment.a <= 0 || fragmentState.j != null) {
                    fragmentState.j = fragment.d;
                } else {
                    fragmentState.j = f(fragment);
                    if (fragment.i != null) {
                        if (fragment.i.f < 0) {
                            a(new IllegalStateException("Failure saving state: " + fragment + " has target not in fragment manager: " + fragment.i));
                        }
                        if (fragmentState.j == null) {
                            fragmentState.j = new Bundle();
                        }
                        a(fragmentState.j, "android:target_state", fragment.i);
                        if (fragment.k != 0) {
                            fragmentState.j.putInt("android:target_req_state", fragment.k);
                        }
                    }
                }
                if (a) {
                    Log.v("FragmentManager", "Saved state of " + fragment + ": " + fragmentState.j);
                }
                z2 = true;
            } else {
                z2 = z3;
            }
            i2++;
            z3 = z2;
        }
        if (z3) {
            if (this.g == null || (size2 = this.g.size()) <= 0) {
                iArr = null;
            } else {
                iArr = new int[size2];
                for (int i3 = 0; i3 < size2; i3++) {
                    iArr[i3] = this.g.get(i3).f;
                    if (iArr[i3] < 0) {
                        a(new IllegalStateException("Failure saving state: active " + this.g.get(i3) + " has cleared index: " + iArr[i3]));
                    }
                    if (a) {
                        Log.v("FragmentManager", "saveAllState: adding fragment #" + i3 + ": " + this.g.get(i3));
                    }
                }
            }
            if (this.i != null && (size = this.i.size()) > 0) {
                backStackStateArr = new BackStackState[size];
                for (int i4 = 0; i4 < size; i4++) {
                    backStackStateArr[i4] = new BackStackState(this, this.i.get(i4));
                    if (a) {
                        Log.v("FragmentManager", "saveAllState: adding back stack #" + i4 + ": " + this.i.get(i4));
                    }
                }
            }
            FragmentManagerState fragmentManagerState = new FragmentManagerState();
            fragmentManagerState.a = fragmentStateArr;
            fragmentManagerState.b = iArr;
            fragmentManagerState.c = backStackStateArr;
            return fragmentManagerState;
        } else if (!a) {
            return null;
        } else {
            Log.v("FragmentManager", "saveAllState: no fragments!");
            return null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.b.a(java.lang.String, java.io.PrintWriter, boolean):void
     arg types: [java.lang.String, java.io.PrintWriter, int]
     candidates:
      android.support.v4.app.b.a(int, android.support.v4.app.Fragment, java.lang.String):android.support.v4.app.f
      android.support.v4.app.f.a(int, android.support.v4.app.Fragment, java.lang.String):android.support.v4.app.f
      android.support.v4.app.b.a(java.lang.String, java.io.PrintWriter, boolean):void */
    /* access modifiers changed from: package-private */
    public void a(Parcelable parcelable, ArrayList<Fragment> arrayList) {
        if (parcelable != null) {
            FragmentManagerState fragmentManagerState = (FragmentManagerState) parcelable;
            if (fragmentManagerState.a != null) {
                if (arrayList != null) {
                    for (int i2 = 0; i2 < arrayList.size(); i2++) {
                        Fragment fragment = arrayList.get(i2);
                        if (a) {
                            Log.v("FragmentManager", "restoreAllState: re-attaching retained " + fragment);
                        }
                        FragmentState fragmentState = fragmentManagerState.a[fragment.f];
                        fragmentState.k = fragment;
                        fragment.e = null;
                        fragment.r = 0;
                        fragment.p = false;
                        fragment.l = false;
                        fragment.i = null;
                        if (fragmentState.j != null) {
                            fragmentState.j.setClassLoader(this.o.getClassLoader());
                            fragment.e = fragmentState.j.getSparseParcelableArray("android:view_state");
                        }
                    }
                }
                this.f = new ArrayList<>(fragmentManagerState.a.length);
                if (this.h != null) {
                    this.h.clear();
                }
                for (int i3 = 0; i3 < fragmentManagerState.a.length; i3++) {
                    FragmentState fragmentState2 = fragmentManagerState.a[i3];
                    if (fragmentState2 != null) {
                        Fragment a2 = fragmentState2.a(this.o, this.q);
                        if (a) {
                            Log.v("FragmentManager", "restoreAllState: active #" + i3 + ": " + a2);
                        }
                        this.f.add(a2);
                        fragmentState2.k = null;
                    } else {
                        this.f.add(null);
                        if (this.h == null) {
                            this.h = new ArrayList<>();
                        }
                        if (a) {
                            Log.v("FragmentManager", "restoreAllState: avail #" + i3);
                        }
                        this.h.add(Integer.valueOf(i3));
                    }
                }
                if (arrayList != null) {
                    for (int i4 = 0; i4 < arrayList.size(); i4++) {
                        Fragment fragment2 = arrayList.get(i4);
                        if (fragment2.j >= 0) {
                            if (fragment2.j < this.f.size()) {
                                fragment2.i = this.f.get(fragment2.j);
                            } else {
                                Log.w("FragmentManager", "Re-attaching retained fragment " + fragment2 + " target no longer exists: " + fragment2.j);
                                fragment2.i = null;
                            }
                        }
                    }
                }
                if (fragmentManagerState.b != null) {
                    this.g = new ArrayList<>(fragmentManagerState.b.length);
                    for (int i5 = 0; i5 < fragmentManagerState.b.length; i5++) {
                        Fragment fragment3 = this.f.get(fragmentManagerState.b[i5]);
                        if (fragment3 == null) {
                            a(new IllegalStateException("No instantiated fragment for index #" + fragmentManagerState.b[i5]));
                        }
                        fragment3.l = true;
                        if (a) {
                            Log.v("FragmentManager", "restoreAllState: added #" + i5 + ": " + fragment3);
                        }
                        if (this.g.contains(fragment3)) {
                            throw new IllegalStateException("Already added!");
                        }
                        this.g.add(fragment3);
                    }
                } else {
                    this.g = null;
                }
                if (fragmentManagerState.c != null) {
                    this.i = new ArrayList<>(fragmentManagerState.c.length);
                    for (int i6 = 0; i6 < fragmentManagerState.c.length; i6++) {
                        b a3 = fragmentManagerState.c[i6].a(this);
                        if (a) {
                            Log.v("FragmentManager", "restoreAllState: back stack #" + i6 + " (index " + a3.o + "): " + a3);
                            a3.a("  ", new PrintWriter(new b("FragmentManager")), false);
                        }
                        this.i.add(a3);
                        if (a3.o >= 0) {
                            a(a3.o, a3);
                        }
                    }
                    return;
                }
                this.i = null;
            }
        }
    }

    public void a(FragmentActivity fragmentActivity, c cVar, Fragment fragment) {
        if (this.o != null) {
            throw new IllegalStateException("Already attached");
        }
        this.o = fragmentActivity;
        this.p = cVar;
        this.q = fragment;
    }

    public void i() {
        this.s = false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.e.a(int, boolean):void
     arg types: [int, int]
     candidates:
      android.support.v4.app.e.a(android.os.Bundle, java.lang.String):android.support.v4.app.Fragment
      android.support.v4.app.e.a(int, android.support.v4.app.b):void
      android.support.v4.app.e.a(android.os.Parcelable, java.util.ArrayList<android.support.v4.app.Fragment>):void
      android.support.v4.app.e.a(android.support.v4.app.Fragment, boolean):void
      android.support.v4.app.e.a(java.lang.Runnable, boolean):void
      android.support.v4.app.e.a(android.view.Menu, android.view.MenuInflater):boolean
      android.support.v4.app.e.a(int, boolean):void */
    public void j() {
        this.s = false;
        a(1, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.e.a(int, boolean):void
     arg types: [int, int]
     candidates:
      android.support.v4.app.e.a(android.os.Bundle, java.lang.String):android.support.v4.app.Fragment
      android.support.v4.app.e.a(int, android.support.v4.app.b):void
      android.support.v4.app.e.a(android.os.Parcelable, java.util.ArrayList<android.support.v4.app.Fragment>):void
      android.support.v4.app.e.a(android.support.v4.app.Fragment, boolean):void
      android.support.v4.app.e.a(java.lang.Runnable, boolean):void
      android.support.v4.app.e.a(android.view.Menu, android.view.MenuInflater):boolean
      android.support.v4.app.e.a(int, boolean):void */
    public void k() {
        this.s = false;
        a(2, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.e.a(int, boolean):void
     arg types: [int, int]
     candidates:
      android.support.v4.app.e.a(android.os.Bundle, java.lang.String):android.support.v4.app.Fragment
      android.support.v4.app.e.a(int, android.support.v4.app.b):void
      android.support.v4.app.e.a(android.os.Parcelable, java.util.ArrayList<android.support.v4.app.Fragment>):void
      android.support.v4.app.e.a(android.support.v4.app.Fragment, boolean):void
      android.support.v4.app.e.a(java.lang.Runnable, boolean):void
      android.support.v4.app.e.a(android.view.Menu, android.view.MenuInflater):boolean
      android.support.v4.app.e.a(int, boolean):void */
    public void l() {
        this.s = false;
        a(4, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.e.a(int, boolean):void
     arg types: [int, int]
     candidates:
      android.support.v4.app.e.a(android.os.Bundle, java.lang.String):android.support.v4.app.Fragment
      android.support.v4.app.e.a(int, android.support.v4.app.b):void
      android.support.v4.app.e.a(android.os.Parcelable, java.util.ArrayList<android.support.v4.app.Fragment>):void
      android.support.v4.app.e.a(android.support.v4.app.Fragment, boolean):void
      android.support.v4.app.e.a(java.lang.Runnable, boolean):void
      android.support.v4.app.e.a(android.view.Menu, android.view.MenuInflater):boolean
      android.support.v4.app.e.a(int, boolean):void */
    public void m() {
        this.s = false;
        a(5, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.e.a(int, boolean):void
     arg types: [int, int]
     candidates:
      android.support.v4.app.e.a(android.os.Bundle, java.lang.String):android.support.v4.app.Fragment
      android.support.v4.app.e.a(int, android.support.v4.app.b):void
      android.support.v4.app.e.a(android.os.Parcelable, java.util.ArrayList<android.support.v4.app.Fragment>):void
      android.support.v4.app.e.a(android.support.v4.app.Fragment, boolean):void
      android.support.v4.app.e.a(java.lang.Runnable, boolean):void
      android.support.v4.app.e.a(android.view.Menu, android.view.MenuInflater):boolean
      android.support.v4.app.e.a(int, boolean):void */
    public void n() {
        a(4, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.e.a(int, boolean):void
     arg types: [int, int]
     candidates:
      android.support.v4.app.e.a(android.os.Bundle, java.lang.String):android.support.v4.app.Fragment
      android.support.v4.app.e.a(int, android.support.v4.app.b):void
      android.support.v4.app.e.a(android.os.Parcelable, java.util.ArrayList<android.support.v4.app.Fragment>):void
      android.support.v4.app.e.a(android.support.v4.app.Fragment, boolean):void
      android.support.v4.app.e.a(java.lang.Runnable, boolean):void
      android.support.v4.app.e.a(android.view.Menu, android.view.MenuInflater):boolean
      android.support.v4.app.e.a(int, boolean):void */
    public void o() {
        this.s = true;
        a(3, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.e.a(int, boolean):void
     arg types: [int, int]
     candidates:
      android.support.v4.app.e.a(android.os.Bundle, java.lang.String):android.support.v4.app.Fragment
      android.support.v4.app.e.a(int, android.support.v4.app.b):void
      android.support.v4.app.e.a(android.os.Parcelable, java.util.ArrayList<android.support.v4.app.Fragment>):void
      android.support.v4.app.e.a(android.support.v4.app.Fragment, boolean):void
      android.support.v4.app.e.a(java.lang.Runnable, boolean):void
      android.support.v4.app.e.a(android.view.Menu, android.view.MenuInflater):boolean
      android.support.v4.app.e.a(int, boolean):void */
    public void p() {
        a(2, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.e.a(int, boolean):void
     arg types: [int, int]
     candidates:
      android.support.v4.app.e.a(android.os.Bundle, java.lang.String):android.support.v4.app.Fragment
      android.support.v4.app.e.a(int, android.support.v4.app.b):void
      android.support.v4.app.e.a(android.os.Parcelable, java.util.ArrayList<android.support.v4.app.Fragment>):void
      android.support.v4.app.e.a(android.support.v4.app.Fragment, boolean):void
      android.support.v4.app.e.a(java.lang.Runnable, boolean):void
      android.support.v4.app.e.a(android.view.Menu, android.view.MenuInflater):boolean
      android.support.v4.app.e.a(int, boolean):void */
    public void q() {
        a(1, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.e.a(int, boolean):void
     arg types: [int, int]
     candidates:
      android.support.v4.app.e.a(android.os.Bundle, java.lang.String):android.support.v4.app.Fragment
      android.support.v4.app.e.a(int, android.support.v4.app.b):void
      android.support.v4.app.e.a(android.os.Parcelable, java.util.ArrayList<android.support.v4.app.Fragment>):void
      android.support.v4.app.e.a(android.support.v4.app.Fragment, boolean):void
      android.support.v4.app.e.a(java.lang.Runnable, boolean):void
      android.support.v4.app.e.a(android.view.Menu, android.view.MenuInflater):boolean
      android.support.v4.app.e.a(int, boolean):void */
    public void r() {
        this.t = true;
        e();
        a(0, false);
        this.o = null;
        this.p = null;
        this.q = null;
    }

    public void a(Configuration configuration) {
        if (this.g != null) {
            int i2 = 0;
            while (true) {
                int i3 = i2;
                if (i3 < this.g.size()) {
                    Fragment fragment = this.g.get(i3);
                    if (fragment != null) {
                        fragment.a(configuration);
                    }
                    i2 = i3 + 1;
                } else {
                    return;
                }
            }
        }
    }

    public void s() {
        if (this.g != null) {
            int i2 = 0;
            while (true) {
                int i3 = i2;
                if (i3 < this.g.size()) {
                    Fragment fragment = this.g.get(i3);
                    if (fragment != null) {
                        fragment.q();
                    }
                    i2 = i3 + 1;
                } else {
                    return;
                }
            }
        }
    }

    public boolean a(Menu menu, MenuInflater menuInflater) {
        boolean z2;
        ArrayList<Fragment> arrayList = null;
        if (this.g != null) {
            int i2 = 0;
            z2 = false;
            while (i2 < this.g.size()) {
                Fragment fragment = this.g.get(i2);
                if (fragment != null && fragment.b(menu, menuInflater)) {
                    z2 = true;
                    if (arrayList == null) {
                        arrayList = new ArrayList<>();
                    }
                    arrayList.add(fragment);
                }
                i2++;
                z2 = z2;
            }
        } else {
            z2 = false;
        }
        if (this.j != null) {
            for (int i3 = 0; i3 < this.j.size(); i3++) {
                Fragment fragment2 = this.j.get(i3);
                if (arrayList == null || !arrayList.contains(fragment2)) {
                    fragment2.m();
                }
            }
        }
        this.j = arrayList;
        return z2;
    }

    public boolean a(Menu menu) {
        if (this.g == null) {
            return false;
        }
        boolean z2 = false;
        for (int i2 = 0; i2 < this.g.size(); i2++) {
            Fragment fragment = this.g.get(i2);
            if (fragment != null && fragment.c(menu)) {
                z2 = true;
            }
        }
        return z2;
    }

    public boolean a(MenuItem menuItem) {
        if (this.g == null) {
            return false;
        }
        for (int i2 = 0; i2 < this.g.size(); i2++) {
            Fragment fragment = this.g.get(i2);
            if (fragment != null && fragment.c(menuItem)) {
                return true;
            }
        }
        return false;
    }

    public boolean b(MenuItem menuItem) {
        if (this.g == null) {
            return false;
        }
        for (int i2 = 0; i2 < this.g.size(); i2++) {
            Fragment fragment = this.g.get(i2);
            if (fragment != null && fragment.d(menuItem)) {
                return true;
            }
        }
        return false;
    }

    public void b(Menu menu) {
        if (this.g != null) {
            int i2 = 0;
            while (true) {
                int i3 = i2;
                if (i3 < this.g.size()) {
                    Fragment fragment = this.g.get(i3);
                    if (fragment != null) {
                        fragment.d(menu);
                    }
                    i2 = i3 + 1;
                } else {
                    return;
                }
            }
        }
    }

    public static int c(int i2) {
        switch (i2) {
            case 4097:
                return 8194;
            case 4099:
                return 4099;
            case 8194:
                return 4097;
            default:
                return 0;
        }
    }

    public static int b(int i2, boolean z2) {
        switch (i2) {
            case 4097:
                return z2 ? 1 : 2;
            case 4099:
                return z2 ? 5 : 6;
            case 8194:
                return z2 ? 3 : 4;
            default:
                return -1;
        }
    }
}
