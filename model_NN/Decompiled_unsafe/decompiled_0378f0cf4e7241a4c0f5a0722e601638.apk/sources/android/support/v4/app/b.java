package android.support.v4.app;

import android.util.Log;
import com.facebook.a.e;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;

/* compiled from: BackStackRecord */
final class b extends f implements Runnable {
    final e a;
    a b;
    a c;
    int d;
    int e;
    int f;
    int g;
    int h;
    int i;
    int j;
    boolean k;
    boolean l = true;
    String m;
    boolean n;
    int o = -1;
    int p;
    CharSequence q;
    int r;
    CharSequence s;

    /* compiled from: BackStackRecord */
    static final class a {
        a a;
        a b;
        int c;
        Fragment d;
        int e;
        int f;
        int g;
        int h;
        ArrayList<Fragment> i;

        a() {
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder(128);
        sb.append("BackStackEntry{");
        sb.append(Integer.toHexString(System.identityHashCode(this)));
        if (this.o >= 0) {
            sb.append(" #");
            sb.append(this.o);
        }
        if (this.m != null) {
            sb.append(" ");
            sb.append(this.m);
        }
        sb.append("}");
        return sb.toString();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.b.a(java.lang.String, java.io.PrintWriter, boolean):void
     arg types: [java.lang.String, java.io.PrintWriter, int]
     candidates:
      android.support.v4.app.b.a(int, android.support.v4.app.Fragment, java.lang.String):android.support.v4.app.f
      android.support.v4.app.f.a(int, android.support.v4.app.Fragment, java.lang.String):android.support.v4.app.f
      android.support.v4.app.b.a(java.lang.String, java.io.PrintWriter, boolean):void */
    public void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        a(str, printWriter, true);
    }

    public void a(String str, PrintWriter printWriter, boolean z) {
        String str2;
        if (z) {
            printWriter.print(str);
            printWriter.print("mName=");
            printWriter.print(this.m);
            printWriter.print(" mIndex=");
            printWriter.print(this.o);
            printWriter.print(" mCommitted=");
            printWriter.println(this.n);
            if (this.i != 0) {
                printWriter.print(str);
                printWriter.print("mTransition=#");
                printWriter.print(Integer.toHexString(this.i));
                printWriter.print(" mTransitionStyle=#");
                printWriter.println(Integer.toHexString(this.j));
            }
            if (!(this.e == 0 && this.f == 0)) {
                printWriter.print(str);
                printWriter.print("mEnterAnim=#");
                printWriter.print(Integer.toHexString(this.e));
                printWriter.print(" mExitAnim=#");
                printWriter.println(Integer.toHexString(this.f));
            }
            if (!(this.g == 0 && this.h == 0)) {
                printWriter.print(str);
                printWriter.print("mPopEnterAnim=#");
                printWriter.print(Integer.toHexString(this.g));
                printWriter.print(" mPopExitAnim=#");
                printWriter.println(Integer.toHexString(this.h));
            }
            if (!(this.p == 0 && this.q == null)) {
                printWriter.print(str);
                printWriter.print("mBreadCrumbTitleRes=#");
                printWriter.print(Integer.toHexString(this.p));
                printWriter.print(" mBreadCrumbTitleText=");
                printWriter.println(this.q);
            }
            if (!(this.r == 0 && this.s == null)) {
                printWriter.print(str);
                printWriter.print("mBreadCrumbShortTitleRes=#");
                printWriter.print(Integer.toHexString(this.r));
                printWriter.print(" mBreadCrumbShortTitleText=");
                printWriter.println(this.s);
            }
        }
        if (this.b != null) {
            printWriter.print(str);
            printWriter.println("Operations:");
            String str3 = str + "    ";
            int i2 = 0;
            a aVar = this.b;
            while (aVar != null) {
                switch (aVar.c) {
                    case 0:
                        str2 = "NULL";
                        break;
                    case 1:
                        str2 = "ADD";
                        break;
                    case 2:
                        str2 = "REPLACE";
                        break;
                    case 3:
                        str2 = "REMOVE";
                        break;
                    case e.g.com_facebook_picker_fragment_done_button_text /*4*/:
                        str2 = "HIDE";
                        break;
                    case e.g.com_facebook_picker_fragment_title_bar_background /*5*/:
                        str2 = "SHOW";
                        break;
                    case e.g.com_facebook_picker_fragment_done_button_background /*6*/:
                        str2 = "DETACH";
                        break;
                    case 7:
                        str2 = "ATTACH";
                        break;
                    default:
                        str2 = "cmd=" + aVar.c;
                        break;
                }
                printWriter.print(str);
                printWriter.print("  Op #");
                printWriter.print(i2);
                printWriter.print(": ");
                printWriter.print(str2);
                printWriter.print(" ");
                printWriter.println(aVar.d);
                if (z) {
                    if (!(aVar.e == 0 && aVar.f == 0)) {
                        printWriter.print(str);
                        printWriter.print("enterAnim=#");
                        printWriter.print(Integer.toHexString(aVar.e));
                        printWriter.print(" exitAnim=#");
                        printWriter.println(Integer.toHexString(aVar.f));
                    }
                    if (!(aVar.g == 0 && aVar.h == 0)) {
                        printWriter.print(str);
                        printWriter.print("popEnterAnim=#");
                        printWriter.print(Integer.toHexString(aVar.g));
                        printWriter.print(" popExitAnim=#");
                        printWriter.println(Integer.toHexString(aVar.h));
                    }
                }
                if (aVar.i != null && aVar.i.size() > 0) {
                    for (int i3 = 0; i3 < aVar.i.size(); i3++) {
                        printWriter.print(str3);
                        if (aVar.i.size() == 1) {
                            printWriter.print("Removed: ");
                        } else {
                            if (i3 == 0) {
                                printWriter.println("Removed:");
                            }
                            printWriter.print(str3);
                            printWriter.print("  #");
                            printWriter.print(i3);
                            printWriter.print(": ");
                        }
                        printWriter.println(aVar.i.get(i3));
                    }
                }
                aVar = aVar.a;
                i2++;
            }
        }
    }

    public b(e eVar) {
        this.a = eVar;
    }

    /* access modifiers changed from: package-private */
    public void a(a aVar) {
        if (this.b == null) {
            this.c = aVar;
            this.b = aVar;
        } else {
            aVar.b = this.c;
            this.c.a = aVar;
            this.c = aVar;
        }
        aVar.e = this.e;
        aVar.f = this.f;
        aVar.g = this.g;
        aVar.h = this.h;
        this.d++;
    }

    public f a(int i2, Fragment fragment, String str) {
        a(i2, fragment, str, 1);
        return this;
    }

    private void a(int i2, Fragment fragment, String str, int i3) {
        fragment.s = this.a;
        if (str != null) {
            if (fragment.y == null || str.equals(fragment.y)) {
                fragment.y = str;
            } else {
                throw new IllegalStateException("Can't change tag of fragment " + fragment + ": was " + fragment.y + " now " + str);
            }
        }
        if (i2 != 0) {
            if (fragment.w == 0 || fragment.w == i2) {
                fragment.w = i2;
                fragment.x = i2;
            } else {
                throw new IllegalStateException("Can't change container ID of fragment " + fragment + ": was " + fragment.w + " now " + i2);
            }
        }
        a aVar = new a();
        aVar.c = i3;
        aVar.d = fragment;
        a(aVar);
    }

    public f a(Fragment fragment) {
        a aVar = new a();
        aVar.c = 6;
        aVar.d = fragment;
        a(aVar);
        return this;
    }

    public f b(Fragment fragment) {
        a aVar = new a();
        aVar.c = 7;
        aVar.d = fragment;
        a(aVar);
        return this;
    }

    /* access modifiers changed from: package-private */
    public void a(int i2) {
        if (this.k) {
            if (e.a) {
                Log.v("FragmentManager", "Bump nesting in " + this + " by " + i2);
            }
            for (a aVar = this.b; aVar != null; aVar = aVar.a) {
                if (aVar.d != null) {
                    aVar.d.r += i2;
                    if (e.a) {
                        Log.v("FragmentManager", "Bump nesting of " + aVar.d + " to " + aVar.d.r);
                    }
                }
                if (aVar.i != null) {
                    for (int size = aVar.i.size() - 1; size >= 0; size--) {
                        Fragment fragment = aVar.i.get(size);
                        fragment.r += i2;
                        if (e.a) {
                            Log.v("FragmentManager", "Bump nesting of " + fragment + " to " + fragment.r);
                        }
                    }
                }
            }
        }
    }

    public int a() {
        return a(false);
    }

    /* access modifiers changed from: package-private */
    public int a(boolean z) {
        if (this.n) {
            throw new IllegalStateException("commit already called");
        }
        if (e.a) {
            Log.v("FragmentManager", "Commit: " + this);
            a("  ", (FileDescriptor) null, new PrintWriter(new android.support.v4.c.b("FragmentManager")), (String[]) null);
        }
        this.n = true;
        if (this.k) {
            this.o = this.a.a(this);
        } else {
            this.o = -1;
        }
        this.a.a(this, z);
        return this.o;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.e.a(android.support.v4.app.Fragment, boolean):void
     arg types: [android.support.v4.app.Fragment, int]
     candidates:
      android.support.v4.app.e.a(android.os.Bundle, java.lang.String):android.support.v4.app.Fragment
      android.support.v4.app.e.a(int, android.support.v4.app.b):void
      android.support.v4.app.e.a(int, boolean):void
      android.support.v4.app.e.a(android.os.Parcelable, java.util.ArrayList<android.support.v4.app.Fragment>):void
      android.support.v4.app.e.a(java.lang.Runnable, boolean):void
      android.support.v4.app.e.a(android.view.Menu, android.view.MenuInflater):boolean
      android.support.v4.app.e.a(android.support.v4.app.Fragment, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.e.a(int, int, int, boolean):void
     arg types: [int, int, int, int]
     candidates:
      android.support.v4.app.e.a(android.support.v4.app.Fragment, int, boolean, int):android.view.animation.Animation
      android.support.v4.app.e.a(java.lang.String, java.io.FileDescriptor, java.io.PrintWriter, java.lang.String[]):void
      android.support.v4.app.e.a(android.os.Handler, java.lang.String, int, int):boolean
      android.support.v4.app.e.a(int, int, int, boolean):void */
    public void run() {
        Fragment fragment;
        if (e.a) {
            Log.v("FragmentManager", "Run: " + this);
        }
        if (!this.k || this.o >= 0) {
            a(1);
            for (a aVar = this.b; aVar != null; aVar = aVar.a) {
                switch (aVar.c) {
                    case 1:
                        Fragment fragment2 = aVar.d;
                        fragment2.G = aVar.e;
                        this.a.a(fragment2, false);
                        break;
                    case 2:
                        Fragment fragment3 = aVar.d;
                        if (this.a.g != null) {
                            fragment = fragment3;
                            for (int i2 = 0; i2 < this.a.g.size(); i2++) {
                                Fragment fragment4 = this.a.g.get(i2);
                                if (e.a) {
                                    Log.v("FragmentManager", "OP_REPLACE: adding=" + fragment + " old=" + fragment4);
                                }
                                if (fragment == null || fragment4.x == fragment.x) {
                                    if (fragment4 == fragment) {
                                        fragment = null;
                                        aVar.d = null;
                                    } else {
                                        if (aVar.i == null) {
                                            aVar.i = new ArrayList<>();
                                        }
                                        aVar.i.add(fragment4);
                                        fragment4.G = aVar.f;
                                        if (this.k) {
                                            fragment4.r++;
                                            if (e.a) {
                                                Log.v("FragmentManager", "Bump nesting of " + fragment4 + " to " + fragment4.r);
                                            }
                                        }
                                        this.a.a(fragment4, this.i, this.j);
                                    }
                                }
                            }
                        } else {
                            fragment = fragment3;
                        }
                        if (fragment == null) {
                            break;
                        } else {
                            fragment.G = aVar.e;
                            this.a.a(fragment, false);
                            break;
                        }
                    case 3:
                        Fragment fragment5 = aVar.d;
                        fragment5.G = aVar.f;
                        this.a.a(fragment5, this.i, this.j);
                        break;
                    case e.g.com_facebook_picker_fragment_done_button_text /*4*/:
                        Fragment fragment6 = aVar.d;
                        fragment6.G = aVar.f;
                        this.a.b(fragment6, this.i, this.j);
                        break;
                    case e.g.com_facebook_picker_fragment_title_bar_background /*5*/:
                        Fragment fragment7 = aVar.d;
                        fragment7.G = aVar.e;
                        this.a.c(fragment7, this.i, this.j);
                        break;
                    case e.g.com_facebook_picker_fragment_done_button_background /*6*/:
                        Fragment fragment8 = aVar.d;
                        fragment8.G = aVar.f;
                        this.a.d(fragment8, this.i, this.j);
                        break;
                    case 7:
                        Fragment fragment9 = aVar.d;
                        fragment9.G = aVar.e;
                        this.a.e(fragment9, this.i, this.j);
                        break;
                    default:
                        throw new IllegalArgumentException("Unknown cmd: " + aVar.c);
                }
            }
            this.a.a(this.a.n, this.i, this.j, true);
            if (this.k) {
                this.a.b(this);
                return;
            }
            return;
        }
        throw new IllegalStateException("addToBackStack() called after commit()");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.e.a(android.support.v4.app.Fragment, boolean):void
     arg types: [android.support.v4.app.Fragment, int]
     candidates:
      android.support.v4.app.e.a(android.os.Bundle, java.lang.String):android.support.v4.app.Fragment
      android.support.v4.app.e.a(int, android.support.v4.app.b):void
      android.support.v4.app.e.a(int, boolean):void
      android.support.v4.app.e.a(android.os.Parcelable, java.util.ArrayList<android.support.v4.app.Fragment>):void
      android.support.v4.app.e.a(java.lang.Runnable, boolean):void
      android.support.v4.app.e.a(android.view.Menu, android.view.MenuInflater):boolean
      android.support.v4.app.e.a(android.support.v4.app.Fragment, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.e.a(int, int, int, boolean):void
     arg types: [int, int, int, int]
     candidates:
      android.support.v4.app.e.a(android.support.v4.app.Fragment, int, boolean, int):android.view.animation.Animation
      android.support.v4.app.e.a(java.lang.String, java.io.FileDescriptor, java.io.PrintWriter, java.lang.String[]):void
      android.support.v4.app.e.a(android.os.Handler, java.lang.String, int, int):boolean
      android.support.v4.app.e.a(int, int, int, boolean):void */
    public void b(boolean z) {
        if (e.a) {
            Log.v("FragmentManager", "popFromBackStack: " + this);
            a("  ", (FileDescriptor) null, new PrintWriter(new android.support.v4.c.b("FragmentManager")), (String[]) null);
        }
        a(-1);
        for (a aVar = this.c; aVar != null; aVar = aVar.b) {
            switch (aVar.c) {
                case 1:
                    Fragment fragment = aVar.d;
                    fragment.G = aVar.h;
                    this.a.a(fragment, e.c(this.i), this.j);
                    break;
                case 2:
                    Fragment fragment2 = aVar.d;
                    if (fragment2 != null) {
                        fragment2.G = aVar.h;
                        this.a.a(fragment2, e.c(this.i), this.j);
                    }
                    if (aVar.i == null) {
                        break;
                    } else {
                        for (int i2 = 0; i2 < aVar.i.size(); i2++) {
                            Fragment fragment3 = aVar.i.get(i2);
                            fragment3.G = aVar.g;
                            this.a.a(fragment3, false);
                        }
                        break;
                    }
                case 3:
                    Fragment fragment4 = aVar.d;
                    fragment4.G = aVar.g;
                    this.a.a(fragment4, false);
                    break;
                case e.g.com_facebook_picker_fragment_done_button_text /*4*/:
                    Fragment fragment5 = aVar.d;
                    fragment5.G = aVar.g;
                    this.a.c(fragment5, e.c(this.i), this.j);
                    break;
                case e.g.com_facebook_picker_fragment_title_bar_background /*5*/:
                    Fragment fragment6 = aVar.d;
                    fragment6.G = aVar.h;
                    this.a.b(fragment6, e.c(this.i), this.j);
                    break;
                case e.g.com_facebook_picker_fragment_done_button_background /*6*/:
                    Fragment fragment7 = aVar.d;
                    fragment7.G = aVar.g;
                    this.a.e(fragment7, e.c(this.i), this.j);
                    break;
                case 7:
                    Fragment fragment8 = aVar.d;
                    fragment8.G = aVar.g;
                    this.a.d(fragment8, e.c(this.i), this.j);
                    break;
                default:
                    throw new IllegalArgumentException("Unknown cmd: " + aVar.c);
            }
        }
        if (z) {
            this.a.a(this.a.n, e.c(this.i), this.j, true);
        }
        if (this.o >= 0) {
            this.a.b(this.o);
            this.o = -1;
        }
    }

    public String b() {
        return this.m;
    }
}
