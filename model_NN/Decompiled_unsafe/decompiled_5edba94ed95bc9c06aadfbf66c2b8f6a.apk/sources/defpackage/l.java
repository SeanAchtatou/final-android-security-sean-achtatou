package defpackage;

import android.content.Context;
import java.io.InputStream;

/* renamed from: l  reason: default package */
public class l {
    public static d a(Context context, String str, String str2) {
        d dVar = new d();
        if (str == null || str.length() <= 0) {
            return dVar;
        }
        context.getSharedPreferences("preferences_data", 0).edit().putString(str2, str).commit();
        return a(str);
    }

    public static d a(String str) {
        d dVar = new d();
        if (str != null && str.length() > 0) {
            dVar.d(a(str.toString(), "OPERATE"));
            dVar.e(a(str.toString(), "SERVICE"));
            dVar.f(a(str.toString(), "FEECODE"));
            dVar.g(a(str.toString(), "MSG1"));
            dVar.h(a(str.toString(), "MSG2"));
            dVar.i(a(str.toString(), "MSG3"));
            dVar.j(a(str.toString(), "MSG4"));
            dVar.k(a(str.toString(), "MSG5"));
            dVar.b(a(str.toString(), "MSG6"));
            dVar.c(a(str.toString(), "PASSWAYID"));
            dVar.l(a(str.toString(), "SPNUMBERS"));
            dVar.m(a(str.toString(), "CONTENTS"));
            dVar.n(a(str.toString(), "SECONDS"));
            dVar.o(a(str.toString(), "DYNAMICS"));
            dVar.p(a(str.toString(), "ANSWERS"));
            dVar.a(a(str.toString(), "QUESTION"));
        }
        return dVar;
    }

    public static String a(InputStream inputStream) {
        byte[] bArr = new byte[1024];
        StringBuffer stringBuffer = new StringBuffer();
        while (true) {
            int read = inputStream.read(bArr);
            if (read == -1) {
                return stringBuffer.toString();
            }
            stringBuffer.append(new String(bArr, 0, read));
            bArr = new byte[1024];
        }
    }

    public static String a(String str, String str2) {
        return (str == null || str.equals("") || str.indexOf(str2) == -1) ? "" : str.substring(str.indexOf("<" + str2 + ">") + str2.length() + 2, str.indexOf("</" + str2 + ">"));
    }
}
