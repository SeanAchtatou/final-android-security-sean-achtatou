package com.admob.android.ads;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.text.Html;
import android.util.Log;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

class Ad {
    /* access modifiers changed from: private */
    public static int CLICK_REQUEST_TIMEOUT = 5000;
    /* access modifiers changed from: private */
    public String clickURL;
    /* access modifiers changed from: private */
    public Context context;
    private String html;
    private Bitmap icon;
    private String iconURL;
    private Bitmap image;
    private int imageHeight;
    private String imageURL;
    private int imageWidth;
    /* access modifiers changed from: private */
    public NetworkListener networkListener;
    private String text;

    interface NetworkListener {
        void onNetworkActivityEnd();

        void onNetworkActivityStart();
    }

    /* JADX INFO: Multiple debug info for r2v5 int: [D('context' android.content.Context), D('i' int)] */
    /* JADX INFO: Multiple debug info for r1v2 int: [D('start' int), D('startHeight' int)] */
    /* JADX INFO: Multiple debug info for r2v30 java.lang.String: [D('endHeight' int), D('height' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r4v4 int: [D('i' int), D('startWidth' int)] */
    /* JADX INFO: Multiple debug info for r4v5 java.lang.String: [D('width' java.lang.String), D('startWidth' int)] */
    /* JADX INFO: Multiple debug info for r2v38 int: [D('endWidth' int), D('i' int)] */
    /* JADX INFO: Multiple debug info for r2v45 'i'  int: [D('end' int), D('i' int)] */
    public static Ad createAd(Context context2, String html2, String iconURL2) {
        if (html2 == null || html2.equals("")) {
            return null;
        }
        Ad ad = new Ad();
        ad.context = context2;
        ad.html = html2;
        ad.iconURL = iconURL2;
        try {
            int i = html2.indexOf("<a ");
            if (i >= 0) {
                int start = html2.indexOf(" href=\"", i) + 7;
                int end = html2.indexOf("\"", start);
                ad.clickURL = html2.substring(start, end);
                i = skipToNext(html2, end + 1);
                if (i < 0) {
                    return null;
                }
            }
            int i2 = i;
            if (i2 >= 0 && html2.indexOf("<img", i2) == i2) {
                int start2 = html2.indexOf(" src=\"", i2) + 6;
                ad.imageURL = html2.substring(start2, html2.indexOf("\"", start2));
                int startHeight = html2.indexOf(" height=\"", i2) + 9;
                ad.imageHeight = Integer.valueOf(html2.substring(startHeight, html2.indexOf("\"", startHeight))).intValue();
                int startWidth = html2.indexOf(" width=\"", i2) + 8;
                int endWidth = html2.indexOf("\"", startWidth);
                ad.imageWidth = Integer.valueOf(html2.substring(startWidth, endWidth)).intValue();
                int i3 = html2.indexOf("<a", endWidth + 1);
                i2 = i3 >= 0 ? skipToNext(html2, i3 + 2) : i3;
            }
            if (i2 >= 0) {
                ad.text = html2.substring(i2, html2.indexOf("<", i2)).trim();
                ad.text = Html.fromHtml(ad.text).toString();
            }
            if (ad.hasImage() && ad.getImage() == null) {
                return null;
            }
            if (ad.iconURL != null) {
                ad.getIcon();
            }
            return ad;
        } catch (Exception e) {
            Log.e("AdMob SDK", "Failed to parse ad response:  " + html2, e);
            return null;
        }
    }

    private Ad() {
    }

    public NetworkListener getNetworkListener() {
        return this.networkListener;
    }

    public void setNetworkListener(NetworkListener networkListener2) {
        this.networkListener = networkListener2;
    }

    private static int skipToNext(String html2, int pos) {
        int end = html2.length();
        if (pos < 0 || pos >= end) {
            return -1;
        }
        char c = html2.charAt(pos);
        while (c != '>' && c != '<') {
            pos = pos + 1;
            if (pos >= end) {
                return -1;
            }
            c = html2.charAt(pos);
        }
        if (c == '>') {
            pos++;
            char c2 = html2.charAt(pos);
            while (Character.isWhitespace(c2)) {
                pos++;
                if (pos >= end) {
                    return -1;
                }
                c2 = html2.charAt(c2);
            }
        }
        return pos;
    }

    public String getHTML() {
        return this.html;
    }

    public String getText() {
        return this.text;
    }

    public boolean hasImage() {
        return this.imageURL != null;
    }

    public String getImageURL() {
        return this.imageURL;
    }

    public Bitmap getImage() {
        if (this.image == null && this.imageURL != null) {
            this.image = fetchImage(this.imageURL, false);
        }
        return this.image;
    }

    private static Bitmap fetchImage(String imageURL2, boolean useCaches) {
        Bitmap image2 = null;
        if (imageURL2 != null) {
            InputStream is = null;
            try {
                URLConnection conn = new URL(imageURL2).openConnection();
                conn.setConnectTimeout(0);
                conn.setReadTimeout(0);
                conn.setUseCaches(useCaches);
                conn.connect();
                is = conn.getInputStream();
                image2 = BitmapFactory.decodeStream(is);
                if (is != null) {
                    try {
                        is.close();
                    } catch (IOException e) {
                    }
                }
            } catch (Throwable th) {
                if (is != null) {
                    try {
                        is.close();
                    } catch (IOException e2) {
                    }
                }
                throw th;
            }
        }
        return image2;
    }

    public int getImageWidth() {
        if (this.image != null) {
            return this.image.getWidth();
        }
        return this.imageWidth;
    }

    public int getImageHeight() {
        if (this.image != null) {
            return this.image.getHeight();
        }
        return this.imageHeight;
    }

    public Bitmap getIcon() {
        if (this.icon == null) {
            this.icon = fetchImage(this.iconURL, true);
            if (this.icon == null) {
                Log.w("AdMob SDK", "Could not get icon for ad from " + this.iconURL);
            }
        }
        return this.icon;
    }

    public String getClickURL() {
        return this.clickURL;
    }

    public void clicked() {
        Log.i("AdMob SDK", "Ad clicked.");
        if (this.clickURL != null) {
            new Thread() {
                public void run() {
                    URL destinationURL = null;
                    try {
                        if (Ad.this.networkListener != null) {
                            Ad.this.networkListener.onNetworkActivityStart();
                        }
                        URL url = new URL(Ad.this.clickURL);
                        URL destinationURL2 = url;
                        HttpURLConnection.setFollowRedirects(true);
                        HttpURLConnection redirectConnection = (HttpURLConnection) url.openConnection();
                        redirectConnection.setConnectTimeout(Ad.CLICK_REQUEST_TIMEOUT);
                        redirectConnection.setReadTimeout(Ad.CLICK_REQUEST_TIMEOUT);
                        redirectConnection.setRequestProperty("User-Agent", AdManager.getUserAgent());
                        redirectConnection.setRequestProperty("X-ADMOB-ISU", AdManager.getUserId(Ad.this.context));
                        redirectConnection.connect();
                        redirectConnection.getResponseCode();
                        destinationURL = redirectConnection.getURL();
                        if (Log.isLoggable("AdMob SDK", 3)) {
                            Log.d("AdMob SDK", "Final click destination URL:  " + destinationURL);
                        }
                    } catch (MalformedURLException e) {
                        Log.w("AdMob SDK", "Malformed click URL.  Will try to follow anyway.  " + Ad.this.clickURL, e);
                    } catch (IOException e2) {
                        Log.w("AdMob SDK", "Could not determine final click destination URL.  Will try to follow anyway.  " + Ad.this.clickURL, e2);
                    }
                    if (destinationURL != null) {
                        if (Log.isLoggable("AdMob SDK", 3)) {
                            Log.d("AdMob SDK", "Opening " + destinationURL);
                        }
                        Intent i = new Intent("android.intent.action.VIEW", Uri.parse(destinationURL.toString()));
                        i.addFlags(268435456);
                        try {
                            Ad.this.context.startActivity(i);
                        } catch (Exception e3) {
                            Log.e("AdMob SDK", "Could not open browser on ad click to " + destinationURL, e3);
                        }
                    }
                    if (Ad.this.networkListener != null) {
                        Ad.this.networkListener.onNetworkActivityEnd();
                    }
                }
            }.start();
        }
    }

    public String toString() {
        String s = getText();
        if (s == null) {
            return "";
        }
        return s;
    }

    public boolean equals(Object o) {
        if (o instanceof Ad) {
            return toString().equals(((Ad) o).toString());
        }
        return false;
    }

    public int hashCode() {
        return toString().hashCode();
    }
}
