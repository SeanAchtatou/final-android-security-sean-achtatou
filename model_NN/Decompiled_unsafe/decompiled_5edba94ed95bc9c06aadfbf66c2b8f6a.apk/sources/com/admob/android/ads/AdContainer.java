package com.admob.android.ads;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.CornerPathEffect;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.admob.android.ads.Ad;

class AdContainer extends RelativeLayout implements Animation.AnimationListener, Ad.NetworkListener {
    private static final int ADMOB_TEXT_ID = 3;
    private static final String ADS_BY_ADMOB = "Ads by AdMob";
    private static final Typeface ADS_BY_ADMOB_FONT = Typeface.create(Typeface.SANS_SERIF, 0);
    private static final float ADS_BY_ADMOB_FONT_SIZE = 9.5f;
    private static final Typeface AD_FONT = Typeface.create(Typeface.SANS_SERIF, 1);
    private static final float AD_FONT_SIZE = 13.0f;
    private static final int AD_TEXT_ID = 2;
    public static final int DEFAULT_BACKGROUND_COLOR = -16777216;
    public static final int DEFAULT_TEXT_COLOR = -1;
    private static final int FOCUS_COLOR = -1147097;
    private static final float FOCUS_CORNER_ROUNDING = 3.0f;
    private static final float FOCUS_WIDTH = 3.0f;
    private static final int GRADIENT_BACKGROUND_COLOR = -1;
    private static final double GRADIENT_STOP = 0.4375d;
    private static final int GRADIENT_TOP_ALPHA = 127;
    private static final int HIGHLIGHT_BACKGROUND_COLOR = -1147097;
    private static final int HIGHLIGHT_COLOR = -19456;
    private static final int HIGHLIGHT_TEXT_COLOR = -16777216;
    private static final int ICON_ID = 1;
    public static final int MAX_WIDTH = 320;
    private static final int NUM_MILLIS_IN_SECS = 1000;
    private static final int PADDING_DEFAULT = 8;
    private static final float PULSE_ANIMATION_DURATION = 0.5f;
    private static final float PULSE_GROWN_SCALE = 1.2f;
    private static final float PULSE_GROW_KEY_TIME = 0.4f;
    private static final float PULSE_INITIAL_SCALE = 1.0f;
    private static final float PULSE_SHRUNKEN_SCALE = 0.001f;
    /* access modifiers changed from: private */
    public ProgressBar activityIndicator;
    /* access modifiers changed from: private */
    public Ad ad;
    private TextView adMobBrandingTextView;
    private TextView adTextView;
    private int backgroundColor;
    /* access modifiers changed from: private */
    public boolean clickInProgress;
    private BitmapDrawable defaultBackground;
    private BitmapDrawable focusedBackground;
    /* access modifiers changed from: private */
    public ImageView iconView;
    private Drawable lastBackground;
    private int padding;
    private BitmapDrawable pressedBackground;
    private int textColor;

    public AdContainer(Ad ad2, Context context) {
        super(context);
        this.backgroundColor = -16777216;
        this.textColor = -1;
        init(ad2, context, null, 0);
    }

    public AdContainer(Ad ad2, Context context, AttributeSet attrs) {
        this(ad2, context, attrs, 0);
    }

    public AdContainer(Ad ad2, Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.backgroundColor = -16777216;
        this.textColor = -1;
        init(ad2, context, attrs, defStyle);
    }

    private void init(Ad ad2, Context context, AttributeSet attrs, int defStyle) {
        this.ad = ad2;
        ad2.setNetworkListener(this);
        this.defaultBackground = null;
        this.pressedBackground = null;
        this.focusedBackground = null;
        this.activityIndicator = null;
        this.clickInProgress = false;
        if (ad2 != null) {
            setFocusable(true);
            setClickable(true);
            Bitmap icon = ad2.getIcon();
            this.iconView = null;
            this.padding = PADDING_DEFAULT;
            if (icon != null) {
                this.padding = (48 - icon.getHeight()) / 2;
                this.iconView = new ImageView(context);
                this.iconView.setImageBitmap(icon);
                RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(icon.getWidth(), icon.getHeight());
                params.setMargins(this.padding, this.padding, 0, this.padding);
                this.iconView.setLayoutParams(params);
                this.iconView.setId(1);
                addView(this.iconView);
                this.activityIndicator = new ProgressBar(context);
                this.activityIndicator.setIndeterminate(true);
                this.activityIndicator.setId(1);
                this.activityIndicator.setLayoutParams(params);
                this.activityIndicator.setVisibility(4);
                addView(this.activityIndicator);
            }
            this.adTextView = new TextView(context);
            this.adTextView.setText(ad2.getText());
            this.adTextView.setTypeface(AD_FONT);
            this.adTextView.setTextColor(this.textColor);
            this.adTextView.setTextSize(AD_FONT_SIZE);
            this.adTextView.setId(2);
            RelativeLayout.LayoutParams adTextViewParams = new RelativeLayout.LayoutParams(-1, -1);
            if (icon != null) {
                adTextViewParams.addRule(1, 1);
            }
            adTextViewParams.setMargins(this.padding, this.padding, this.padding, this.padding);
            adTextViewParams.addRule(11);
            adTextViewParams.addRule(10);
            this.adTextView.setLayoutParams(adTextViewParams);
            addView(this.adTextView);
            this.adMobBrandingTextView = new TextView(context);
            this.adMobBrandingTextView.setGravity(5);
            this.adMobBrandingTextView.setText(AdMobLocalizer.localize(ADS_BY_ADMOB));
            this.adMobBrandingTextView.setTypeface(ADS_BY_ADMOB_FONT);
            this.adMobBrandingTextView.setTextColor(this.textColor);
            this.adMobBrandingTextView.setTextSize(ADS_BY_ADMOB_FONT_SIZE);
            this.adMobBrandingTextView.setId(3);
            RelativeLayout.LayoutParams adMobBrandingTextViewParams = new RelativeLayout.LayoutParams(-2, -2);
            adMobBrandingTextViewParams.setMargins(0, 0, this.padding, this.padding);
            adMobBrandingTextViewParams.addRule(11);
            adMobBrandingTextViewParams.addRule(12);
            this.adMobBrandingTextView.setLayoutParams(adMobBrandingTextViewParams);
            addView(this.adMobBrandingTextView);
        }
        int tc = -1;
        int bc = -16777216;
        if (attrs != null) {
            String namespace = "http://schemas.android.com/apk/res/" + context.getPackageName();
            tc = attrs.getAttributeUnsignedIntValue(namespace, "textColor", -1);
            bc = attrs.getAttributeUnsignedIntValue(namespace, "backgroundColor", -16777216);
        }
        setTextColor(tc);
        setBackgroundColor(bc);
    }

    public void setTextColor(int color) {
        this.textColor = -16777216 | color;
        if (this.adTextView != null) {
            this.adTextView.setTextColor(this.textColor);
        }
        if (this.adMobBrandingTextView != null) {
            this.adMobBrandingTextView.setTextColor(this.textColor);
        }
        postInvalidate();
    }

    public int getTextColor() {
        return this.textColor;
    }

    public void setBackgroundColor(int color) {
        this.backgroundColor = -16777216 | color;
    }

    public int getBackgroundColor() {
        return this.backgroundColor;
    }

    /* access modifiers changed from: protected */
    public Ad getAd() {
        return this.ad;
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        if (!(this.adMobBrandingTextView == null || this.adTextView == null)) {
            int brandingVisibility = this.adMobBrandingTextView.getVisibility();
            if (w > 0) {
                if (w <= 128) {
                    this.adTextView.setTextSize(9.099999f);
                    brandingVisibility = PADDING_DEFAULT;
                } else if (w <= 176) {
                    this.adTextView.setTextSize(10.400001f);
                    brandingVisibility = 0;
                    this.adMobBrandingTextView.setTextSize(7.6f);
                } else {
                    this.adTextView.setTextSize(AD_FONT_SIZE);
                    brandingVisibility = 0;
                    this.adMobBrandingTextView.setTextSize(ADS_BY_ADMOB_FONT_SIZE);
                }
            }
            if (brandingVisibility == 0) {
                Typeface tf = this.adTextView.getTypeface();
                String text = this.ad.getText();
                if (text != null) {
                    Paint paint = new Paint();
                    paint.setTypeface(tf);
                    paint.setTextSize(this.adTextView.getTextSize());
                    float textViewWidth = paint.measureText(text);
                    float adTextViewWidth = (float) (w - (this.padding * 2));
                    if (this.iconView != null) {
                        adTextViewWidth -= (float) (this.iconView.getWidth() + this.padding);
                    }
                    if (textViewWidth > adTextViewWidth) {
                        brandingVisibility = PADDING_DEFAULT;
                    }
                }
            }
            this.adMobBrandingTextView.setVisibility(brandingVisibility);
        }
        if (w != 0 && h != 0) {
            Rect r = new Rect(0, 0, w, h);
            this.defaultBackground = generateBackgroundDrawable(r, -1, this.backgroundColor);
            this.pressedBackground = generateBackgroundDrawable(r, -1147097, HIGHLIGHT_COLOR);
            this.focusedBackground = generateBackgroundDrawable(r, -1, this.backgroundColor, true);
            setBackgroundDrawable(this.defaultBackground);
        }
    }

    /* access modifiers changed from: package-private */
    public void recycleBitmaps() {
        if (this.defaultBackground != null) {
            BitmapDrawable temp = this.defaultBackground;
            this.defaultBackground = null;
            recycleBitmapDrawable(temp);
        }
        if (this.pressedBackground != null) {
            BitmapDrawable temp2 = this.pressedBackground;
            this.pressedBackground = null;
            recycleBitmapDrawable(temp2);
        }
        if (this.focusedBackground != null) {
            BitmapDrawable temp3 = this.focusedBackground;
            this.focusedBackground = null;
            recycleBitmapDrawable(temp3);
        }
    }

    private void recycleBitmapDrawable(BitmapDrawable bitmapDrawable) {
        Bitmap b;
        if (bitmapDrawable != null && (b = bitmapDrawable.getBitmap()) != null) {
            b.recycle();
        }
    }

    private static void drawBackground(Canvas c, Rect r, int backgroundColor2, int color) {
        Paint paint = new Paint();
        paint.setColor(backgroundColor2);
        paint.setAntiAlias(true);
        c.drawRect(r, paint);
        GradientDrawable shine = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[]{Color.argb((int) GRADIENT_TOP_ALPHA, Color.red(color), Color.green(color), Color.blue(color)), color});
        int stop = ((int) (((double) r.height()) * GRADIENT_STOP)) + r.top;
        shine.setBounds(r.left, r.top, r.right, stop);
        shine.draw(c);
        Rect shadowRect = new Rect(r.left, stop, r.right, r.bottom);
        Paint shadowPaint = new Paint();
        shadowPaint.setColor(color);
        c.drawRect(shadowRect, shadowPaint);
    }

    private BitmapDrawable generateBackgroundDrawable(Rect r, int backgroundColor2, int color) {
        return generateBackgroundDrawable(r, backgroundColor2, color, false);
    }

    private BitmapDrawable generateBackgroundDrawable(Rect r, int backgroundColor2, int color, boolean addFocusRing) {
        try {
            Bitmap bitmap = Bitmap.createBitmap(r.width(), r.height(), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(bitmap);
            drawBackground(canvas, r, backgroundColor2, color);
            if (addFocusRing) {
                drawFocusRing(canvas, r);
            }
            return new BitmapDrawable(bitmap);
        } catch (Throwable th) {
            return null;
        }
    }

    public void onNetworkActivityEnd() {
        post(new Thread() {
            public void run() {
                if (AdContainer.this.activityIndicator != null) {
                    AdContainer.this.activityIndicator.setVisibility(4);
                }
                if (AdContainer.this.iconView != null) {
                    AdContainer.this.iconView.setImageMatrix(new Matrix());
                    AdContainer.this.iconView.setVisibility(0);
                }
                boolean unused = AdContainer.this.clickInProgress = false;
            }
        });
    }

    public void onNetworkActivityStart() {
        post(new Thread() {
            public void run() {
                if (AdContainer.this.iconView != null) {
                    AdContainer.this.iconView.setVisibility(4);
                }
                if (AdContainer.this.activityIndicator != null) {
                    AdContainer.this.activityIndicator.setVisibility(0);
                }
            }
        });
    }

    private static void drawFocusRing(Canvas c, Rect r) {
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setColor(-1147097);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(3.0f);
        paint.setPathEffect(new CornerPathEffect(3.0f));
        Path path = new Path();
        path.addRoundRect(new RectF(r), 3.0f, 3.0f, Path.Direction.CW);
        c.drawPath(path, paint);
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (Log.isLoggable("AdMob SDK", 2)) {
            Log.v("AdMob SDK", "onKeyDown: keyCode=" + keyCode);
        }
        if (keyCode == 66 || keyCode == 23) {
            setPressed(true);
        }
        return super.onKeyDown(keyCode, event);
    }

    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (Log.isLoggable("AdMob SDK", 2)) {
            Log.v("AdMob SDK", "onKeyUp: keyCode=" + keyCode);
        }
        if (keyCode == 66 || keyCode == 23) {
            click();
        }
        setPressed(false);
        return super.onKeyUp(keyCode, event);
    }

    public boolean dispatchTouchEvent(MotionEvent e) {
        int action = e.getAction();
        if (Log.isLoggable("AdMob SDK", 2)) {
            Log.v("AdMob SDK", "dispatchTouchEvent: action=" + action + " x=" + e.getX() + " y=" + e.getY());
        }
        if (action == 0) {
            setPressed(true);
        } else if (action == 2) {
            float x = e.getX();
            float y = e.getY();
            int left = getLeft();
            int top = getTop();
            int right = getRight();
            int bottom = getBottom();
            if (x < ((float) left) || x > ((float) right) || y < ((float) top) || y > ((float) bottom)) {
                setPressed(false);
            } else {
                setPressed(true);
            }
        } else if (action == 1) {
            if (isPressed()) {
                click();
            }
            setPressed(false);
        } else if (action == 3) {
            setPressed(false);
        }
        return super.dispatchTouchEvent(e);
    }

    public boolean dispatchTrackballEvent(MotionEvent event) {
        if (Log.isLoggable("AdMob SDK", 2)) {
            Log.v("AdMob SDK", "dispatchTrackballEvent: action=" + event.getAction());
        }
        if (event.getAction() == 0) {
            setPressed(true);
        } else if (event.getAction() == 1) {
            if (hasFocus()) {
                click();
            }
            setPressed(false);
        }
        return super.onTrackballEvent(event);
    }

    /* access modifiers changed from: protected */
    public void onFocusChanged(boolean gainFocus, int direction, Rect previouslyFocusedRect) {
        super.onFocusChanged(gainFocus, direction, previouslyFocusedRect);
        if (gainFocus) {
            setBackgroundDrawable(this.focusedBackground);
        } else {
            setBackgroundDrawable(this.defaultBackground);
        }
    }

    public void setPressed(boolean pressed) {
        Drawable newBackground;
        if ((!pressed || !this.clickInProgress) && isPressed() != pressed) {
            BitmapDrawable bitmapDrawable = this.defaultBackground;
            int color = this.textColor;
            if (pressed) {
                this.lastBackground = getBackground();
                newBackground = this.pressedBackground;
                color = -16777216;
            } else {
                newBackground = this.lastBackground;
            }
            setBackgroundDrawable(newBackground);
            if (this.adTextView != null) {
                this.adTextView.setTextColor(color);
            }
            if (this.adMobBrandingTextView != null) {
                this.adMobBrandingTextView.setTextColor(color);
            }
            super.setPressed(pressed);
            invalidate();
        }
    }

    private void click() {
        if (this.ad != null && isPressed()) {
            setPressed(false);
            if (!this.clickInProgress) {
                this.clickInProgress = true;
                if (this.iconView != null) {
                    AnimationSet animationSet = new AnimationSet(true);
                    float pivotX = ((float) this.iconView.getWidth()) / 2.0f;
                    float pivotY = ((float) this.iconView.getHeight()) / 2.0f;
                    ScaleAnimation growAnim = new ScaleAnimation(PULSE_INITIAL_SCALE, PULSE_GROWN_SCALE, PULSE_INITIAL_SCALE, PULSE_GROWN_SCALE, pivotX, pivotY);
                    growAnim.setDuration(200);
                    animationSet.addAnimation(growAnim);
                    ScaleAnimation shrinkAnim = new ScaleAnimation(PULSE_GROWN_SCALE, PULSE_SHRUNKEN_SCALE, PULSE_GROWN_SCALE, PULSE_SHRUNKEN_SCALE, pivotX, pivotY);
                    shrinkAnim.setDuration(299);
                    shrinkAnim.setStartOffset(200);
                    shrinkAnim.setAnimationListener(this);
                    animationSet.addAnimation(shrinkAnim);
                    postDelayed(new Thread() {
                        public void run() {
                            AdContainer.this.ad.clicked();
                        }
                    }, 500);
                    this.iconView.startAnimation(animationSet);
                    return;
                }
                this.ad.clicked();
            }
        }
    }

    public void onAnimationRepeat(Animation animation) {
    }

    public void onAnimationStart(Animation animation) {
    }

    public void onAnimationEnd(Animation animation) {
    }
}
