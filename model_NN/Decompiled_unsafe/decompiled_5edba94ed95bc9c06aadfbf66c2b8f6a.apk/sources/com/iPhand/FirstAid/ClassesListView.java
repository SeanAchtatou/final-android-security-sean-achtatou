package com.iPhand.FirstAid;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import java.util.ArrayList;
import java.util.HashMap;

public class ClassesListView extends Activity {
    int[] Icons = {R.drawable.icon_01, R.drawable.icon_02, R.drawable.icon_03, R.drawable.icon_04, R.drawable.icon_05, R.drawable.icon_06, R.drawable.icon_07, R.drawable.icon_08, R.drawable.icon_09, R.drawable.icon_10};
    int Num = 0;
    String Title = null;
    private SQLiteDatabase database;
    ListView mListView;
    String[] mQ = {"common", "necessaryknowhow", "outdoor", "internal", "external", "face", "gynaecological", "paediatrics", "skin", "psychological"};

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.dialogueclass_list);
        this.database = DBUtil.openDatabase(this);
        this.mListView = (ListView) findViewById(R.id.DialogueClassList);
        this.Num = getIntent().getExtras().getInt("Num");
        this.Title = getIntent().getExtras().getString("Title");
        setTitle(this.Title);
        setListAdapter();
        this.mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View arg1, int arg2, long arg3) {
                Intent intent = new Intent(ClassesListView.this, DetailView.class);
                Bundle b = new Bundle();
                b.putInt("Num", ClassesListView.this.getIntent().getExtras().getInt("Num"));
                b.putInt("ItemNum", arg2);
                intent.putExtras(b);
                ClassesListView.this.startActivity(intent);
            }
        });
    }

    public void setListAdapter() {
        Cursor cursor = this.database.rawQuery("select topic from firstaid where category =?", new String[]{this.mQ[this.Num]});
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
        }
        try {
            ArrayList<HashMap<String, Object>> lstImageItem = new ArrayList<>();
            for (int i = 0; i < cursor.getCount(); i++) {
                HashMap<String, Object> map = new HashMap<>();
                map.put("Image", Integer.valueOf(this.Icons[this.Num]));
                map.put("ItemTitle", cursor.getString(cursor.getColumnIndex("topic")));
                lstImageItem.add(map);
                cursor.moveToNext();
            }
            this.mListView.setAdapter((ListAdapter) new SimpleAdapter(this, lstImageItem, R.layout.dialogue_details_item, new String[]{"Image", "ItemTitle"}, new int[]{R.id.DialogueclassImage, R.id.DialogueclassTitle}));
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.database.close();
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onRestart() {
        this.database = DBUtil.openDatabase(this);
        super.onRestart();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        this.database.close();
        super.onStop();
    }
}
