package com.iPhand.FirstAid;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class DetailView extends Activity implements View.OnClickListener {
    private static final int MENU_SendWithEmail = 1;
    String[] C = {"common", "necessaryknowhow", "outdoor", "internal", "external", "face", "gynaecological", "paediatrics", "skin", "psychological"};
    Cursor Detail;
    int ItemNum = 0;
    int Num = 0;
    Cursor Summary;
    Cursor Topic;
    private SQLiteDatabase database;
    /* access modifiers changed from: private */
    public boolean isPre = false;
    /* access modifiers changed from: private */
    public ViewGroup mContainer;
    /* access modifiers changed from: private */
    public ImageView mImageView;
    /* access modifiers changed from: private */
    public TextView mTextView;
    ImageButton nextButton;
    ImageButton preButton;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.detail_view);
        this.database = DBUtil.openDatabase(this);
        this.mImageView = (ImageView) findViewById(R.id.picture);
        this.mContainer = (ViewGroup) findViewById(R.id.DetailContainer);
        this.mTextView = (TextView) findViewById(R.id.myTextView);
        this.preButton = (ImageButton) findViewById(R.id.preButton);
        this.nextButton = (ImageButton) findViewById(R.id.nextButton);
        this.Num = getIntent().getExtras().getInt("Num");
        this.Topic = this.database.rawQuery("select topic from firstaid where category =?", new String[]{this.C[this.Num]});
        this.Summary = this.database.rawQuery("select summary from firstaid where category =?", new String[]{this.C[this.Num]});
        this.Detail = this.database.rawQuery("select detail from firstaid where category =?", new String[]{this.C[this.Num]});
        if (this.Topic.getCount() > 0) {
            this.Topic.moveToFirst();
        }
        if (this.Summary.getCount() > 0) {
            this.Summary.moveToFirst();
        }
        if (this.Detail.getCount() > 0) {
            this.Detail.moveToFirst();
        }
        this.ItemNum = getIntent().getExtras().getInt("ItemNum");
        this.nextButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (DetailView.this.ItemNum == DetailView.this.Summary.getCount() - 1) {
                    Toast.makeText(DetailView.this, "已经是本篇最后一条了！休息一下^_^", 1).show();
                    return;
                }
                DetailView.this.isPre = false;
                DetailView.this.applyRotation(-1, 0.0f, 180.0f);
            }
        });
        this.preButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (DetailView.this.ItemNum == 0) {
                    Toast.makeText(DetailView.this, "已经是本篇第一条了！→_→", 1).show();
                    return;
                }
                DetailView.this.isPre = true;
                DetailView.this.applyRotation(-1, 0.0f, -180.0f);
            }
        });
        this.mTextView.setFocusable(true);
        this.Topic.moveToPosition(this.ItemNum);
        setTitle(this.Topic.getString(this.Topic.getColumnIndex("topic")));
        this.Detail.moveToPosition(this.ItemNum);
        this.Summary.moveToPosition(this.ItemNum);
        this.mTextView.setText(String.valueOf(this.Summary.getString(this.Summary.getColumnIndex("summary"))) + "\n\n" + this.Detail.getString(this.Detail.getColumnIndex("detail")));
        this.mContainer.setPersistentDrawingCache(1);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        menu.add(0, 1, 1, "邮件分享").setIcon((int) R.drawable.sendemail);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        switch (item.getItemId()) {
            case 1:
                share();
                return true;
            default:
                return false;
        }
    }

    private void share() {
        Intent intent = new Intent("android.intent.action.VIEW", Uri.parse("mailto:"));
        this.Topic.moveToPosition(this.ItemNum);
        this.Detail.moveToPosition(this.ItemNum);
        this.Summary.moveToPosition(this.ItemNum);
        intent.putExtra("android.intent.extra.SUBJECT", "急救手册——" + this.Topic.getString(this.Topic.getColumnIndex("topic")));
        intent.putExtra("android.intent.extra.TEXT", String.valueOf(this.Summary.getString(this.Summary.getColumnIndex("summary"))) + "\n\n" + this.Detail.getString(this.Detail.getColumnIndex("detail")));
        startActivity(intent);
    }

    /* access modifiers changed from: private */
    public void applyRotation(int position, float start, float end) {
        Rotate3dAnimation rotation = new Rotate3dAnimation(start, end, ((float) this.mContainer.getWidth()) / 2.0f, ((float) this.mContainer.getHeight()) / 2.0f, 310.0f, true);
        rotation.setDuration(500);
        rotation.setFillAfter(true);
        rotation.setInterpolator(new AccelerateInterpolator());
        rotation.setAnimationListener(new DisplayNextView(this, null));
        this.mContainer.startAnimation(rotation);
    }

    public void onClick(View v) {
    }

    private final class DisplayNextView implements Animation.AnimationListener {
        private DisplayNextView() {
        }

        /* synthetic */ DisplayNextView(DetailView detailView, DisplayNextView displayNextView) {
            this();
        }

        public void onAnimationStart(Animation animation) {
        }

        public void onAnimationEnd(Animation animation) {
            DetailView.this.mContainer.post(new SwapViews());
        }

        public void onAnimationRepeat(Animation animation) {
        }
    }

    private final class SwapViews implements Runnable {
        public SwapViews() {
        }

        public void run() {
            float centerX = ((float) DetailView.this.mContainer.getWidth()) / 2.0f;
            float centerY = ((float) DetailView.this.mContainer.getHeight()) / 2.0f;
            if (!DetailView.this.isPre) {
                DetailView.this.ItemNum++;
            } else {
                DetailView.this.ItemNum--;
            }
            DetailView.this.mImageView.setBackgroundResource(R.drawable.q);
            DetailView.this.Topic.moveToPosition(DetailView.this.ItemNum);
            DetailView.this.setTitle(DetailView.this.Topic.getString(DetailView.this.Topic.getColumnIndex("topic")));
            DetailView.this.Detail.moveToPosition(DetailView.this.ItemNum);
            DetailView.this.Summary.moveToPosition(DetailView.this.ItemNum);
            DetailView.this.mTextView.setText(String.valueOf(DetailView.this.Summary.getString(DetailView.this.Summary.getColumnIndex("summary"))) + "\n\n" + DetailView.this.Detail.getString(DetailView.this.Detail.getColumnIndex("detail")));
            Rotate3dAnimation rotation = new Rotate3dAnimation(90.0f, 0.0f, centerX, centerY, 310.0f, false);
            rotation.setDuration(500);
            rotation.setFillAfter(true);
            rotation.setInterpolator(new DecelerateInterpolator());
            DetailView.this.mContainer.startAnimation(rotation);
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.Summary.close();
        this.Detail.close();
        this.database.close();
        super.onDestroy();
    }
}
