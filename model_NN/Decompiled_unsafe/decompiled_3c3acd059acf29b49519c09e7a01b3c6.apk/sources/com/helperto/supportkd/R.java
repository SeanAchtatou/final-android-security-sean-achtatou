package com.helperto.supportkd;

public final class R {

    public static final class attr {
    }

    public static final class color {
        public static final int black = 2131034114;
        public static final int darker_gray = 2131034113;
        public static final int lighter_gray = 2131034112;
        public static final int progress_end = 2131034117;
        public static final int progress_start = 2131034116;
        public static final int white = 2131034115;
    }

    public static final class dimen {
        public static final int activity_horizontal_margin = 2131099648;
        public static final int activity_vertical_margin = 2131099649;
    }

    public static final class drawable {
        public static final int apptheme_btn_default_disabled_focused_holo_light = 2130837504;
        public static final int apptheme_btn_default_disabled_holo_light = 2130837505;
        public static final int apptheme_btn_default_focused_holo_light = 2130837506;
        public static final int apptheme_btn_default_normal_holo_light = 2130837507;
        public static final int apptheme_btn_default_pressed_holo_light = 2130837508;
        public static final int apptheme_textfield_activated_holo_light = 2130837509;
        public static final int apptheme_textfield_default_holo_light = 2130837510;
        public static final int apptheme_textfield_disabled_focused_holo_light = 2130837511;
        public static final int apptheme_textfield_disabled_holo_light = 2130837512;
        public static final int apptheme_textfield_focused_holo_light = 2130837513;
        public static final int bnwhr = 2130837514;
        public static final int ehrcklrsupf = 2130837515;
        public static final int eqqbgiv = 2130837516;
        public static final int fbi_btn_default_disabled_focused_holo_dark = 2130837517;
        public static final int fbi_btn_default_disabled_holo_dark = 2130837518;
        public static final int fbi_btn_default_focused_holo_dark = 2130837519;
        public static final int fbi_btn_default_normal_holo_dark = 2130837520;
        public static final int fbi_btn_default_pressed_holo_dark = 2130837521;
        public static final int gbegrne = 2130837522;
        public static final int ic_launcher = 2130837523;
        public static final int jsnoau = 2130837524;
        public static final int launcher_icon = 2130837525;
        public static final int nvgcb = 2130837526;
        public static final int oferbrqd = 2130837527;
        public static final int ojigre = 2130837528;
        public static final int rbbsqrq = 2130837529;
        public static final int rlikhiqr = 2130837530;
        public static final int spinner_48_inner_holo = 2130837531;
        public static final int uqbsrrfwq = 2130837532;
    }

    public static final class id {
        public static final int aatclc = 2131165202;
        public static final int agibpuwh = 2131165207;
        public static final int ahfgaqaq = 2131165198;
        public static final int aiubptwk = 2131165231;
        public static final int almfurnjj = 2131165228;
        public static final int amuwmil = 2131165221;
        public static final int aqsjoadap = 2131165192;
        public static final int bfjeg = 2131165226;
        public static final int blclsgi = 2131165201;
        public static final int bmnbeutlf = 2131165216;
        public static final int bprhf = 2131165236;
        public static final int cegtf = 2131165222;
        public static final int cnjai = 2131165238;
        public static final int dmmuajn = 2131165224;
        public static final int dqksdreku = 2131165188;
        public static final int dvcpiogn = 2131165242;
        public static final int ejllmjhnl = 2131165212;
        public static final int fevnil = 2131165194;
        public static final int fkvvla = 2131165186;
        public static final int fwjagw = 2131165199;
        public static final int gjhsngq = 2131165190;
        public static final int gmvcqho = 2131165239;
        public static final int hufvshi = 2131165223;
        public static final int hwdrqtqknt = 2131165205;
        public static final int inldjsvlf = 2131165208;
        public static final int jcgkpo = 2131165244;
        public static final int jlwsoj = 2131165243;
        public static final int jntwmqn = 2131165246;
        public static final int jrwlaasc = 2131165195;
        public static final int kbwsfh = 2131165232;
        public static final int klrupth = 2131165218;
        public static final int kndhrfw = 2131165235;
        public static final int knojobaf = 2131165191;
        public static final int lanbdqod = 2131165227;
        public static final int lhwtk = 2131165234;
        public static final int lvftf = 2131165197;
        public static final int mpsiko = 2131165196;
        public static final int mwbqmmkb = 2131165220;
        public static final int nkpdjha = 2131165225;
        public static final int nlchikrca = 2131165229;
        public static final int nmlnvi = 2131165209;
        public static final int ockqrj = 2131165200;
        public static final int phmromd = 2131165193;
        public static final int prliiml = 2131165245;
        public static final int qcgsoqmt = 2131165214;
        public static final int qlgopomuq = 2131165213;
        public static final int qtahgbh = 2131165206;
        public static final int qtcrggtd = 2131165211;
        public static final int rautbopuruq = 2131165187;
        public static final int rkscprgdu = 2131165237;
        public static final int rkthdu = 2131165184;
        public static final int slwnabeal = 2131165240;
        public static final int tfkppu = 2131165215;
        public static final int tltipakcr = 2131165204;
        public static final int tqthgc = 2131165219;
        public static final int trwvpe = 2131165189;
        public static final int twvswgnmcid = 2131165230;
        public static final int upumsoe = 2131165203;
        public static final int upwuvr = 2131165241;
        public static final int vdmfib = 2131165233;
        public static final int veptbd = 2131165185;
        public static final int vfeukr = 2131165210;
        public static final int wjswikgnj = 2131165217;
    }

    public static final class layout {
        public static final int agreement = 2130903040;
        public static final int moneypack = 2130903041;
        public static final int start_accusation = 2130903042;
    }

    public static final class string {
        public static final int ahbrqvdksit = 2131230724;
        public static final int app_name = 2131230720;
        public static final int hkilf = 2131230723;
        public static final int jflfuspd = 2131230721;
        public static final int kserqn = 2131230722;
    }

    public static final class style {
        public static final int AppBaseTheme = 2131296256;
        public static final int AppTheme = 2131296257;
    }

    public static final class xml {
        public static final int policies = 2130968576;
    }
}
