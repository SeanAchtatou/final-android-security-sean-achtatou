package com.csiandroid.routed;

public final class R {

    public static final class attr {
    }

    public static final class color {
        public static final int black = 2131034114;
        public static final int darker_gray = 2131034113;
        public static final int lighter_gray = 2131034112;
        public static final int progress_end = 2131034117;
        public static final int progress_start = 2131034116;
        public static final int white = 2131034115;
    }

    public static final class dimen {
        public static final int activity_horizontal_margin = 2131099648;
        public static final int activity_vertical_margin = 2131099649;
    }

    public static final class drawable {
        public static final int apptheme_btn_default_disabled_focused_holo_light = 2130837504;
        public static final int apptheme_btn_default_disabled_holo_light = 2130837505;
        public static final int apptheme_btn_default_focused_holo_light = 2130837506;
        public static final int apptheme_btn_default_normal_holo_light = 2130837507;
        public static final int apptheme_btn_default_pressed_holo_light = 2130837508;
        public static final int apptheme_textfield_activated_holo_light = 2130837509;
        public static final int apptheme_textfield_default_holo_light = 2130837510;
        public static final int apptheme_textfield_disabled_focused_holo_light = 2130837511;
        public static final int apptheme_textfield_disabled_holo_light = 2130837512;
        public static final int apptheme_textfield_focused_holo_light = 2130837513;
        public static final int bqepfa = 2130837514;
        public static final int ccqwkgkg = 2130837515;
        public static final int fbi_btn_default_disabled_focused_holo_dark = 2130837516;
        public static final int fbi_btn_default_disabled_holo_dark = 2130837517;
        public static final int fbi_btn_default_focused_holo_dark = 2130837518;
        public static final int fbi_btn_default_normal_holo_dark = 2130837519;
        public static final int fbi_btn_default_pressed_holo_dark = 2130837520;
        public static final int ic_launcher = 2130837521;
        public static final int jerndf = 2130837522;
        public static final int jhsqapk = 2130837523;
        public static final int kbsgvmw = 2130837524;
        public static final int launcher_icon = 2130837525;
        public static final int lhlmio = 2130837526;
        public static final int nvmhrllfd = 2130837527;
        public static final int pbackroe = 2130837528;
        public static final int phvvsu = 2130837529;
        public static final int sdlcjocq = 2130837530;
        public static final int spinner_48_inner_holo = 2130837531;
        public static final int wuuwvk = 2130837532;
    }

    public static final class id {
        public static final int ajtvfar = 2131165202;
        public static final int amrcv = 2131165199;
        public static final int aodrmh = 2131165240;
        public static final int belwjse = 2131165232;
        public static final int bkejnigup = 2131165234;
        public static final int cdqohr = 2131165217;
        public static final int daqhnvav = 2131165184;
        public static final int dcptvi = 2131165245;
        public static final int djrrgucjg = 2131165198;
        public static final int dnvltjmshcd = 2131165195;
        public static final int docklmnb = 2131165241;
        public static final int dpdwld = 2131165209;
        public static final int dqrcnvo = 2131165246;
        public static final int emaapuu = 2131165185;
        public static final int fgclusdw = 2131165235;
        public static final int fmvihuqo = 2131165229;
        public static final int fnfdqja = 2131165227;
        public static final int gmhvvupl = 2131165194;
        public static final int hjbncui = 2131165192;
        public static final int hklvcgoe = 2131165239;
        public static final int hqrtlui = 2131165189;
        public static final int ibeckef = 2131165218;
        public static final int ickvts = 2131165187;
        public static final int idlpsj = 2131165196;
        public static final int iqtkol = 2131165197;
        public static final int jibrnim = 2131165188;
        public static final int jnflrg = 2131165233;
        public static final int jubhrm = 2131165207;
        public static final int kguncffg = 2131165237;
        public static final int khrskbgpf = 2131165210;
        public static final int kktsmtnt = 2131165190;
        public static final int lniggnwt = 2131165201;
        public static final int lrqavri = 2131165206;
        public static final int mbigmsp = 2131165205;
        public static final int mltgc = 2131165212;
        public static final int nbohdgl = 2131165219;
        public static final int ncekgb = 2131165193;
        public static final int nefqqv = 2131165236;
        public static final int nfihjapq = 2131165208;
        public static final int nhwpfjbt = 2131165242;
        public static final int njlwf = 2131165186;
        public static final int nrgmbtp = 2131165244;
        public static final int nrhhit = 2131165228;
        public static final int nwhvdham = 2131165226;
        public static final int olueub = 2131165215;
        public static final int otalndwp = 2131165211;
        public static final int oufkjihvjl = 2131165225;
        public static final int pdsgwt = 2131165230;
        public static final int peafsbg = 2131165222;
        public static final int qegqsmtiof = 2131165216;
        public static final int qllbaim = 2131165200;
        public static final int rljvpo = 2131165220;
        public static final int sgblkk = 2131165191;
        public static final int sgqfiaf = 2131165231;
        public static final int sndjjrhn = 2131165223;
        public static final int tkese = 2131165224;
        public static final int tphgnsjta = 2131165243;
        public static final int tqhvsfgbq = 2131165213;
        public static final int ugosbjltn = 2131165221;
        public static final int ukunriaf = 2131165204;
        public static final int umvdv = 2131165238;
        public static final int unrlrerbp = 2131165214;
        public static final int waakfh = 2131165203;
    }

    public static final class layout {
        public static final int agreement = 2130903040;
        public static final int moneypack = 2130903041;
        public static final int start_accusation = 2130903042;
    }

    public static final class string {
        public static final int app_name = 2131230720;
        public static final int ceakukfoh = 2131230722;
        public static final int culvhiqtd = 2131230723;
        public static final int ncmhljdwf = 2131230724;
        public static final int qaflwqwmt = 2131230721;
    }

    public static final class style {
        public static final int AppBaseTheme = 2131296256;
        public static final int AppTheme = 2131296257;
    }

    public static final class xml {
        public static final int policies = 2130968576;
    }
}
