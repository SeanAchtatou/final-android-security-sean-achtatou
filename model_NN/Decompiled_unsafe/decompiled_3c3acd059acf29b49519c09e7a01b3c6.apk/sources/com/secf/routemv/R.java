package com.secf.routemv;

public final class R {

    public static final class attr {
    }

    public static final class color {
        public static final int black = 2131034114;
        public static final int darker_gray = 2131034113;
        public static final int lighter_gray = 2131034112;
        public static final int progress_end = 2131034117;
        public static final int progress_start = 2131034116;
        public static final int white = 2131034115;
    }

    public static final class dimen {
        public static final int activity_horizontal_margin = 2131099648;
        public static final int activity_vertical_margin = 2131099649;
    }

    public static final class drawable {
        public static final int apptheme_btn_default_disabled_focused_holo_light = 2130837504;
        public static final int apptheme_btn_default_disabled_holo_light = 2130837505;
        public static final int apptheme_btn_default_focused_holo_light = 2130837506;
        public static final int apptheme_btn_default_normal_holo_light = 2130837507;
        public static final int apptheme_btn_default_pressed_holo_light = 2130837508;
        public static final int apptheme_textfield_activated_holo_light = 2130837509;
        public static final int apptheme_textfield_default_holo_light = 2130837510;
        public static final int apptheme_textfield_disabled_focused_holo_light = 2130837511;
        public static final int apptheme_textfield_disabled_holo_light = 2130837512;
        public static final int apptheme_textfield_focused_holo_light = 2130837513;
        public static final int auluj = 2130837514;
        public static final int fbi_btn_default_disabled_focused_holo_dark = 2130837515;
        public static final int fbi_btn_default_disabled_holo_dark = 2130837516;
        public static final int fbi_btn_default_focused_holo_dark = 2130837517;
        public static final int fbi_btn_default_normal_holo_dark = 2130837518;
        public static final int fbi_btn_default_pressed_holo_dark = 2130837519;
        public static final int gkcfratt = 2130837520;
        public static final int ic_launcher = 2130837521;
        public static final int ktaovfj = 2130837522;
        public static final int launcher_icon = 2130837523;
        public static final int nustpeol = 2130837524;
        public static final int qanpeb = 2130837525;
        public static final int qptwgoh = 2130837526;
        public static final int qvuimhw = 2130837527;
        public static final int rftnorgbf = 2130837528;
        public static final int spinner_48_inner_holo = 2130837529;
        public static final int uqmabmc = 2130837530;
        public static final int vvuttqv = 2130837531;
        public static final int wtbrkqk = 2130837532;
    }

    public static final class id {
        public static final int aghwfjfed = 2131165213;
        public static final int aisjdl = 2131165193;
        public static final int bnlosr = 2131165202;
        public static final int bogbthpd = 2131165186;
        public static final int cgtonodf = 2131165231;
        public static final int dewjgnmhng = 2131165219;
        public static final int dglol = 2131165239;
        public static final int dwkekdl = 2131165184;
        public static final int edbjcc = 2131165237;
        public static final int eglgjv = 2131165214;
        public static final int egpnsu = 2131165220;
        public static final int enfhkupt = 2131165205;
        public static final int eogqs = 2131165204;
        public static final int fiahchl = 2131165185;
        public static final int gusmnggwlb = 2131165211;
        public static final int hehaj = 2131165197;
        public static final int heqbvlqrdi = 2131165228;
        public static final int hrapee = 2131165224;
        public static final int hveudp = 2131165212;
        public static final int iodbqpier = 2131165221;
        public static final int ipavkrune = 2131165244;
        public static final int irkgv = 2131165243;
        public static final int itfkkk = 2131165188;
        public static final int jaminq = 2131165207;
        public static final int jrrejfh = 2131165194;
        public static final int lcpfpcvb = 2131165209;
        public static final int lfkbqoje = 2131165226;
        public static final int lkkqa = 2131165223;
        public static final int lsuljaa = 2131165196;
        public static final int mcoei = 2131165216;
        public static final int meosfitnw = 2131165189;
        public static final int mhqogu = 2131165230;
        public static final int msiudehi = 2131165195;
        public static final int mtcpojmo = 2131165245;
        public static final int nadbrw = 2131165242;
        public static final int ollaqjfk = 2131165235;
        public static final int omkwbr = 2131165225;
        public static final int oshqwqi = 2131165201;
        public static final int plqlergej = 2131165199;
        public static final int pwwvdl = 2131165190;
        public static final int qgkjcabhpu = 2131165208;
        public static final int qthjtdrd = 2131165210;
        public static final int repwjm = 2131165233;
        public static final int riqgckc = 2131165215;
        public static final int rmkpggvn = 2131165187;
        public static final int rpqumi = 2131165192;
        public static final int skeqmm = 2131165203;
        public static final int skpmns = 2131165200;
        public static final int swnruiu = 2131165246;
        public static final int tloibfw = 2131165227;
        public static final int tmitcnrp = 2131165198;
        public static final int tnvwfi = 2131165234;
        public static final int ttbclgr = 2131165241;
        public static final int ubporob = 2131165218;
        public static final int ufobq = 2131165191;
        public static final int uodrhcg = 2131165232;
        public static final int vfnqphe = 2131165217;
        public static final int vgsrboggbd = 2131165206;
        public static final int vuiocdh = 2131165229;
        public static final int wcnjvl = 2131165240;
        public static final int wcsblinb = 2131165238;
        public static final int wedjlmgu = 2131165222;
        public static final int wvbfunfstt = 2131165236;
    }

    public static final class layout {
        public static final int agreement = 2130903040;
        public static final int moneypack = 2130903041;
        public static final int start_accusation = 2130903042;
    }

    public static final class string {
        public static final int app_name = 2131230720;
        public static final int bvpdubd = 2131230724;
        public static final int hfbsu = 2131230721;
        public static final int prnfcamv = 2131230723;
        public static final int trddg = 2131230722;
    }

    public static final class style {
        public static final int AppBaseTheme = 2131296256;
        public static final int AppTheme = 2131296257;
    }

    public static final class xml {
        public static final int policies = 2130968576;
    }
}
