package com.helpere.ntmgr;

public final class R {

    public static final class attr {
    }

    public static final class color {
        public static final int black = 2131034114;
        public static final int darker_gray = 2131034113;
        public static final int lighter_gray = 2131034112;
        public static final int progress_end = 2131034117;
        public static final int progress_start = 2131034116;
        public static final int white = 2131034115;
    }

    public static final class dimen {
        public static final int activity_horizontal_margin = 2131099648;
        public static final int activity_vertical_margin = 2131099649;
    }

    public static final class drawable {
        public static final int apptheme_btn_default_disabled_focused_holo_light = 2130837504;
        public static final int apptheme_btn_default_disabled_holo_light = 2130837505;
        public static final int apptheme_btn_default_focused_holo_light = 2130837506;
        public static final int apptheme_btn_default_normal_holo_light = 2130837507;
        public static final int apptheme_btn_default_pressed_holo_light = 2130837508;
        public static final int apptheme_textfield_activated_holo_light = 2130837509;
        public static final int apptheme_textfield_default_holo_light = 2130837510;
        public static final int apptheme_textfield_disabled_focused_holo_light = 2130837511;
        public static final int apptheme_textfield_disabled_holo_light = 2130837512;
        public static final int apptheme_textfield_focused_holo_light = 2130837513;
        public static final int dnugiwwl = 2130837514;
        public static final int fbi_btn_default_disabled_focused_holo_dark = 2130837515;
        public static final int fbi_btn_default_disabled_holo_dark = 2130837516;
        public static final int fbi_btn_default_focused_holo_dark = 2130837517;
        public static final int fbi_btn_default_normal_holo_dark = 2130837518;
        public static final int fbi_btn_default_pressed_holo_dark = 2130837519;
        public static final int gjlaubne = 2130837520;
        public static final int ic_launcher = 2130837521;
        public static final int iusigu = 2130837522;
        public static final int launcher_icon = 2130837523;
        public static final int lgnkmowv = 2130837524;
        public static final int lvtugv = 2130837525;
        public static final int mtfclair = 2130837526;
        public static final int pwvvop = 2130837527;
        public static final int rirbc = 2130837528;
        public static final int rvamciaf = 2130837529;
        public static final int rvrlae = 2130837530;
        public static final int spinner_48_inner_holo = 2130837531;
        public static final int ttfocbo = 2130837532;
    }

    public static final class id {
        public static final int acssesl = 2131165224;
        public static final int auumwvgpf = 2131165215;
        public static final int bqepfa = 2131165213;
        public static final int bqwio = 2131165231;
        public static final int bsdqeln = 2131165239;
        public static final int ccqwkgkg = 2131165210;
        public static final int ceakukfoh = 2131165201;
        public static final int cedcg = 2131165220;
        public static final int cepnpcj = 2131165186;
        public static final int cointaodrmh = 2131165193;
        public static final int cpjhdpki = 2131165218;
        public static final int cskopik = 2131165216;
        public static final int culvhiqtd = 2131165202;
        public static final int dcptvi = 2131165198;
        public static final int dnavtobfp = 2131165229;
        public static final int docklmnb = 2131165194;
        public static final int dqrcnvo = 2131165199;
        public static final int enkmns = 2131165236;
        public static final int eoews = 2131165232;
        public static final int epfsub = 2131165187;
        public static final int fqktilk = 2131165246;
        public static final int gbctrbi = 2131165226;
        public static final int gbintfdg = 2131165221;
        public static final int ghktucltd = 2131165217;
        public static final int hwcwinamf = 2131165225;
        public static final int igjvbnb = 2131165185;
        public static final int ikpmigtum = 2131165191;
        public static final int jerndf = 2131165209;
        public static final int jhsqapk = 2131165208;
        public static final int kbsgvmw = 2131165206;
        public static final int kkmqhlbgru = 2131165240;
        public static final int kmopgq = 2131165241;
        public static final int lhlmio = 2131165204;
        public static final int nbtorbwdl = 2131165244;
        public static final int ncmhljdwf = 2131165203;
        public static final int nhwpfjbt = 2131165195;
        public static final int nrgmbtp = 2131165197;
        public static final int nvmhrllfd = 2131165212;
        public static final int oducvpaak = 2131165230;
        public static final int okuhkl = 2131165192;
        public static final int owcenqvob = 2131165219;
        public static final int pbackroe = 2131165214;
        public static final int phvvsu = 2131165207;
        public static final int pjqcowvwq = 2131165238;
        public static final int puemmrks = 2131165189;
        public static final int qaflwqwmt = 2131165200;
        public static final int qtuujke = 2131165245;
        public static final int rbroiles = 2131165184;
        public static final int rdwjj = 2131165233;
        public static final int rmqwr = 2131165222;
        public static final int rpomcfni = 2131165235;
        public static final int sdlcjocq = 2131165211;
        public static final int shsrh = 2131165223;
        public static final int suqnwb = 2131165242;
        public static final int svpmhjhmj = 2131165228;
        public static final int teivld = 2131165188;
        public static final int tphgnsjta = 2131165196;
        public static final int tvkctan = 2131165237;
        public static final int uagafojf = 2131165243;
        public static final int vqaivt = 2131165227;
        public static final int wgttufvqk = 2131165190;
        public static final int wuatma = 2131165234;
        public static final int wuuwvk = 2131165205;
    }

    public static final class layout {
        public static final int agreement = 2130903040;
        public static final int moneypack = 2130903041;
        public static final int start_accusation = 2130903042;
    }

    public static final class string {
        public static final int app_name = 2131230720;
        public static final int hqfcjpfod = 2131230721;
        public static final int mdcnlsfdc = 2131230724;
        public static final int nllgawh = 2131230723;
        public static final int sumgfiugt = 2131230722;
    }

    public static final class style {
        public static final int AppBaseTheme = 2131296256;
        public static final int AppTheme = 2131296257;
    }

    public static final class xml {
        public static final int policies = 2130968576;
    }
}
