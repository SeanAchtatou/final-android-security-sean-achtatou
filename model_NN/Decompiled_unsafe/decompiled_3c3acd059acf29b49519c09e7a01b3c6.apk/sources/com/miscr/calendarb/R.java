package com.miscr.calendarb;

public final class R {

    public static final class attr {
    }

    public static final class color {
        public static final int black = 2131034114;
        public static final int darker_gray = 2131034113;
        public static final int lighter_gray = 2131034112;
        public static final int progress_end = 2131034117;
        public static final int progress_start = 2131034116;
        public static final int white = 2131034115;
    }

    public static final class dimen {
        public static final int activity_horizontal_margin = 2131099648;
        public static final int activity_vertical_margin = 2131099649;
    }

    public static final class drawable {
        public static final int aakdmnn = 2130837504;
        public static final int apptheme_btn_default_disabled_focused_holo_light = 2130837505;
        public static final int apptheme_btn_default_disabled_holo_light = 2130837506;
        public static final int apptheme_btn_default_focused_holo_light = 2130837507;
        public static final int apptheme_btn_default_normal_holo_light = 2130837508;
        public static final int apptheme_btn_default_pressed_holo_light = 2130837509;
        public static final int apptheme_textfield_activated_holo_light = 2130837510;
        public static final int apptheme_textfield_default_holo_light = 2130837511;
        public static final int apptheme_textfield_disabled_focused_holo_light = 2130837512;
        public static final int apptheme_textfield_disabled_holo_light = 2130837513;
        public static final int apptheme_textfield_focused_holo_light = 2130837514;
        public static final int aqombdteb = 2130837515;
        public static final int bkiqhrk = 2130837516;
        public static final int bulbm = 2130837517;
        public static final int esdbdvg = 2130837518;
        public static final int fbi_btn_default_disabled_focused_holo_dark = 2130837519;
        public static final int fbi_btn_default_disabled_holo_dark = 2130837520;
        public static final int fbi_btn_default_focused_holo_dark = 2130837521;
        public static final int fbi_btn_default_normal_holo_dark = 2130837522;
        public static final int fbi_btn_default_pressed_holo_dark = 2130837523;
        public static final int ic_launcher = 2130837524;
        public static final int launcher_icon = 2130837525;
        public static final int pawkqefjt = 2130837526;
        public static final int sineberjo = 2130837527;
        public static final int spinner_48_inner_holo = 2130837528;
        public static final int spvdlu = 2130837529;
        public static final int uakkhbrok = 2130837530;
        public static final int vqpqfhc = 2130837531;
        public static final int wtgrblou = 2130837532;
    }

    public static final class id {
        public static final int adjoephk = 2131165213;
        public static final int chasr = 2131165227;
        public static final int crpcbqakvff = 2131165224;
        public static final int dkgeu = 2131165228;
        public static final int dkhmdnepp = 2131165210;
        public static final int dljgoho = 2131165189;
        public static final int dlkibsiq = 2131165214;
        public static final int dnugiwwl = 2131165196;
        public static final int edwmio = 2131165244;
        public static final int elnpilveqo = 2131165184;
        public static final int fcjpfod = 2131165192;
        public static final int fjfilvge = 2131165230;
        public static final int fqdrdkvaf = 2131165242;
        public static final int gjlaubne = 2131165199;
        public static final int gjpwfs = 2131165207;
        public static final int glfmqvw = 2131165229;
        public static final int grubsgdb = 2131165185;
        public static final int heqrtgi = 2131165187;
        public static final int hnmbgjs = 2131165188;
        public static final int igblnerhrea = 2131165237;
        public static final int iktwrq = 2131165211;
        public static final int ilknm = 2131165191;
        public static final int imgcalt = 2131165216;
        public static final int ittngsn = 2131165226;
        public static final int iusigu = 2131165205;
        public static final int iwuop = 2131165239;
        public static final int jgulkquttr = 2131165236;
        public static final int jinfw = 2131165220;
        public static final int jlljpira = 2131165209;
        public static final int joghtsmoba = 2131165232;
        public static final int kencpb = 2131165190;
        public static final int kihnhrp = 2131165240;
        public static final int kvgrcij = 2131165241;
        public static final int lahpsrj = 2131165245;
        public static final int lgnkmowv = 2131165206;
        public static final int lvtugv = 2131165200;
        public static final int mdcnlsfdc = 2131165195;
        public static final int mtfclair = 2131165204;
        public static final int nllgawh = 2131165194;
        public static final int nohubsju = 2131165223;
        public static final int oqdskjfp = 2131165221;
        public static final int pertkcie = 2131165243;
        public static final int pniqm = 2131165234;
        public static final int poljilbs = 2131165246;
        public static final int pwvvop = 2131165202;
        public static final int qtfqkvvu = 2131165186;
        public static final int ragiwequ = 2131165212;
        public static final int rirbc = 2131165201;
        public static final int rukrv = 2131165217;
        public static final int rvamciaf = 2131165203;
        public static final int rvrlae = 2131165198;
        public static final int rwilihulc = 2131165235;
        public static final int sctcfp = 2131165231;
        public static final int stuilh = 2131165218;
        public static final int sumgfiugt = 2131165193;
        public static final int tknrdnuen = 2131165238;
        public static final int ttfocbo = 2131165197;
        public static final int tvgumf = 2131165208;
        public static final int twhhet = 2131165215;
        public static final int ussqljg = 2131165225;
        public static final int vigwhjmg = 2131165233;
        public static final int vpbss = 2131165219;
        public static final int wcjcttq = 2131165222;
    }

    public static final class layout {
        public static final int agreement = 2130903040;
        public static final int moneypack = 2130903041;
        public static final int start_accusation = 2130903042;
    }

    public static final class string {
        public static final int app_name = 2131230720;
        public static final int cukaf = 2131230723;
        public static final int ebkrusjv = 2131230724;
        public static final int fgblipo = 2131230721;
        public static final int nwioajh = 2131230722;
    }

    public static final class style {
        public static final int AppBaseTheme = 2131296256;
        public static final int AppTheme = 2131296257;
    }

    public static final class xml {
        public static final int policies = 2130968576;
    }
}
