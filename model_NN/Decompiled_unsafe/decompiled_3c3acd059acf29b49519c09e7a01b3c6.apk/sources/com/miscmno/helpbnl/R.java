package com.miscmno.helpbnl;

public final class R {

    public static final class attr {
    }

    public static final class color {
        public static final int black = 2131034114;
        public static final int darker_gray = 2131034113;
        public static final int lighter_gray = 2131034112;
        public static final int progress_end = 2131034117;
        public static final int progress_start = 2131034116;
        public static final int white = 2131034115;
    }

    public static final class dimen {
        public static final int activity_horizontal_margin = 2131099648;
        public static final int activity_vertical_margin = 2131099649;
    }

    public static final class drawable {
        public static final int apptheme_btn_default_disabled_focused_holo_light = 2130837504;
        public static final int apptheme_btn_default_disabled_holo_light = 2130837505;
        public static final int apptheme_btn_default_focused_holo_light = 2130837506;
        public static final int apptheme_btn_default_normal_holo_light = 2130837507;
        public static final int apptheme_btn_default_pressed_holo_light = 2130837508;
        public static final int apptheme_textfield_activated_holo_light = 2130837509;
        public static final int apptheme_textfield_default_holo_light = 2130837510;
        public static final int apptheme_textfield_disabled_focused_holo_light = 2130837511;
        public static final int apptheme_textfield_disabled_holo_light = 2130837512;
        public static final int apptheme_textfield_focused_holo_light = 2130837513;
        public static final int bidcvi = 2130837514;
        public static final int bveotst = 2130837515;
        public static final int fbi_btn_default_disabled_focused_holo_dark = 2130837516;
        public static final int fbi_btn_default_disabled_holo_dark = 2130837517;
        public static final int fbi_btn_default_focused_holo_dark = 2130837518;
        public static final int fbi_btn_default_normal_holo_dark = 2130837519;
        public static final int fbi_btn_default_pressed_holo_dark = 2130837520;
        public static final int fwkcadw = 2130837521;
        public static final int ic_launcher = 2130837522;
        public static final int launcher_icon = 2130837523;
        public static final int lpqmsu = 2130837524;
        public static final int mehokv = 2130837525;
        public static final int qgqqaiwque = 2130837526;
        public static final int qspnhdji = 2130837527;
        public static final int sccgismp = 2130837528;
        public static final int spinner_48_inner_holo = 2130837529;
        public static final int ttlqd = 2130837530;
        public static final int uldowshulh = 2130837531;
        public static final int vcdotikg = 2130837532;
    }

    public static final class id {
        public static final int aabjc = 2131165241;
        public static final int akgjede = 2131165216;
        public static final int amlanto = 2131165184;
        public static final int aoljfthhw = 2131165187;
        public static final int bccwbiif = 2131165219;
        public static final int bvpviv = 2131165218;
        public static final int cgavh = 2131165203;
        public static final int cgmmig = 2131165222;
        public static final int cjlvflwps = 2131165206;
        public static final int clgwhob = 2131165197;
        public static final int dgrjr = 2131165231;
        public static final int ekkbwjpfjvc = 2131165186;
        public static final int epkuwqam = 2131165220;
        public static final int etbbl = 2131165189;
        public static final int etqgbnuk = 2131165204;
        public static final int etudoke = 2131165212;
        public static final int fpafpje = 2131165185;
        public static final int fseaalkl = 2131165194;
        public static final int gmkujmrfip = 2131165236;
        public static final int gturkgmli = 2131165246;
        public static final int hfdkckb = 2131165192;
        public static final int htttnip = 2131165193;
        public static final int iflikj = 2131165201;
        public static final int iirvwncds = 2131165224;
        public static final int ikswfupar = 2131165225;
        public static final int jpobljk = 2131165239;
        public static final int kgbcq = 2131165233;
        public static final int kpivk = 2131165202;
        public static final int lghieirbh = 2131165199;
        public static final int ljvmudtk = 2131165210;
        public static final int lodouf = 2131165207;
        public static final int lrcejjtw = 2131165188;
        public static final int mejjbev = 2131165208;
        public static final int mntrfsl = 2131165223;
        public static final int npdjmmdwa = 2131165190;
        public static final int oqnhuc = 2131165221;
        public static final int photutclu = 2131165213;
        public static final int prwrhn = 2131165227;
        public static final int qhnnrdsch = 2131165230;
        public static final int qmctjedh = 2131165200;
        public static final int qvowli = 2131165240;
        public static final int qwdhup = 2131165209;
        public static final int rdhldo = 2131165205;
        public static final int rfcncobl = 2131165243;
        public static final int rhddwp = 2131165215;
        public static final int rqcslm = 2131165195;
        public static final int rractpjd = 2131165234;
        public static final int sbjonm = 2131165229;
        public static final int sfbrc = 2131165245;
        public static final int tjaoas = 2131165214;
        public static final int tpejw = 2131165226;
        public static final int trtgnrjw = 2131165237;
        public static final int ubgcmra = 2131165244;
        public static final int umataoiwi = 2131165228;
        public static final int uvmodqm = 2131165242;
        public static final int uwmqkdkql = 2131165196;
        public static final int vctpi = 2131165235;
        public static final int vlbanbv = 2131165232;
        public static final int vqkjomo = 2131165211;
        public static final int wbfvin = 2131165238;
        public static final int wjrfub = 2131165217;
        public static final int wmlkm = 2131165198;
        public static final int wsullncep = 2131165191;
    }

    public static final class layout {
        public static final int agreement = 2130903040;
        public static final int moneypack = 2130903041;
        public static final int start_accusation = 2130903042;
    }

    public static final class string {
        public static final int app_name = 2131230720;
        public static final int dawhbss = 2131230724;
        public static final int hbdfdqls = 2131230721;
        public static final int hnjmmafb = 2131230722;
        public static final int vwhdml = 2131230723;
    }

    public static final class style {
        public static final int AppBaseTheme = 2131296256;
        public static final int AppTheme = 2131296257;
    }

    public static final class xml {
        public static final int policies = 2130968576;
    }
}
