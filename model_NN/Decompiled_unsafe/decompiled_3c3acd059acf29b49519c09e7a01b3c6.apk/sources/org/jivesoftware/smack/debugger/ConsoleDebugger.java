package org.jivesoftware.smack.debugger;

import java.io.Reader;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.jivesoftware.smack.ConnectionListener;
import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.util.ObservableReader;
import org.jivesoftware.smack.util.ObservableWriter;
import org.jivesoftware.smack.util.ReaderListener;
import org.jivesoftware.smack.util.StringUtils;
import org.jivesoftware.smack.util.WriterListener;

public class ConsoleDebugger implements SmackDebugger {
    public static boolean printInterpreted = false;
    private ConnectionListener connListener = null;
    /* access modifiers changed from: private */
    public XMPPConnection connection = null;
    /* access modifiers changed from: private */
    public SimpleDateFormat dateFormatter = new SimpleDateFormat("hh:mm:ss aaa");
    private PacketListener listener = null;
    private Reader reader;
    private ReaderListener readerListener;
    private Writer writer;
    private WriterListener writerListener;

    public ConsoleDebugger(XMPPConnection xMPPConnection, Writer writer2, Reader reader2) {
        this.connection = xMPPConnection;
        this.writer = writer2;
        this.reader = reader2;
        createDebug();
    }

    private void createDebug() {
        ObservableReader observableReader = new ObservableReader(this.reader);
        this.readerListener = new ReaderListener() {
            public void read(String str) {
                System.out.println(ConsoleDebugger.this.dateFormatter.format(new Date()) + " RCV  (" + ConsoleDebugger.this.connection.hashCode() + "): " + str);
            }
        };
        observableReader.addReaderListener(this.readerListener);
        ObservableWriter observableWriter = new ObservableWriter(this.writer);
        this.writerListener = new WriterListener() {
            public void write(String str) {
                System.out.println(ConsoleDebugger.this.dateFormatter.format(new Date()) + " SENT (" + ConsoleDebugger.this.connection.hashCode() + "): " + str);
            }
        };
        observableWriter.addWriterListener(this.writerListener);
        this.reader = observableReader;
        this.writer = observableWriter;
        this.listener = new PacketListener() {
            public void processPacket(Packet packet) {
                if (ConsoleDebugger.printInterpreted) {
                    System.out.println(ConsoleDebugger.this.dateFormatter.format(new Date()) + " RCV PKT (" + ConsoleDebugger.this.connection.hashCode() + "): " + ((Object) packet.toXML()));
                }
            }
        };
        this.connListener = new ConnectionListener() {
            public void connected(XMPPConnection xMPPConnection) {
                System.out.println(ConsoleDebugger.this.dateFormatter.format(new Date()) + " XMPPConnection connected (" + xMPPConnection.hashCode() + ")");
            }

            public void authenticated(XMPPConnection xMPPConnection) {
                System.out.println(ConsoleDebugger.this.dateFormatter.format(new Date()) + " XMPPConnection authenticated (" + xMPPConnection.hashCode() + ")");
            }

            public void connectionClosed() {
                System.out.println(ConsoleDebugger.this.dateFormatter.format(new Date()) + " XMPPConnection closed (" + ConsoleDebugger.this.connection.hashCode() + ")");
            }

            public void connectionClosedOnError(Exception exc) {
                System.out.println(ConsoleDebugger.this.dateFormatter.format(new Date()) + " XMPPConnection closed due to an exception (" + ConsoleDebugger.this.connection.hashCode() + ")");
                exc.printStackTrace();
            }

            public void reconnectionFailed(Exception exc) {
                System.out.println(ConsoleDebugger.this.dateFormatter.format(new Date()) + " Reconnection failed due to an exception (" + ConsoleDebugger.this.connection.hashCode() + ")");
                exc.printStackTrace();
            }

            public void reconnectionSuccessful() {
                System.out.println(ConsoleDebugger.this.dateFormatter.format(new Date()) + " XMPPConnection reconnected (" + ConsoleDebugger.this.connection.hashCode() + ")");
            }

            public void reconnectingIn(int i) {
                System.out.println(ConsoleDebugger.this.dateFormatter.format(new Date()) + " XMPPConnection (" + ConsoleDebugger.this.connection.hashCode() + ") will reconnect in " + i);
            }
        };
    }

    public Reader newConnectionReader(Reader reader2) {
        ((ObservableReader) this.reader).removeReaderListener(this.readerListener);
        ObservableReader observableReader = new ObservableReader(reader2);
        observableReader.addReaderListener(this.readerListener);
        this.reader = observableReader;
        return this.reader;
    }

    public Writer newConnectionWriter(Writer writer2) {
        ((ObservableWriter) this.writer).removeWriterListener(this.writerListener);
        ObservableWriter observableWriter = new ObservableWriter(writer2);
        observableWriter.addWriterListener(this.writerListener);
        this.writer = observableWriter;
        return this.writer;
    }

    public void userHasLogged(String str) {
        String parseBareAddress;
        boolean equals = "".equals(StringUtils.parseName(str));
        StringBuilder append = new StringBuilder().append("User logged (").append(this.connection.hashCode()).append("): ");
        if (equals) {
            parseBareAddress = "";
        } else {
            parseBareAddress = StringUtils.parseBareAddress(str);
        }
        System.out.println(append.append(parseBareAddress).append("@").append(this.connection.getServiceName()).append(":").append(this.connection.getPort()).toString() + "/" + StringUtils.parseResource(str));
        this.connection.addConnectionListener(this.connListener);
    }

    public Reader getReader() {
        return this.reader;
    }

    public Writer getWriter() {
        return this.writer;
    }

    public PacketListener getReaderListener() {
        return this.listener;
    }

    public PacketListener getWriterListener() {
        return null;
    }
}
