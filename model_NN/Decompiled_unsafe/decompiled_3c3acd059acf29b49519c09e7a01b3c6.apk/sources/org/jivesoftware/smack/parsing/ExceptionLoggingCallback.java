package org.jivesoftware.smack.parsing;

import java.util.logging.Level;
import java.util.logging.Logger;

public class ExceptionLoggingCallback extends ParsingExceptionCallback {
    private static final Logger LOGGER = Logger.getLogger(ExceptionLoggingCallback.class.getName());

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
     arg types: [java.util.logging.Level, java.lang.String, java.lang.Exception]
     candidates:
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
    public void handleUnparsablePacket(UnparsablePacket unparsablePacket) throws Exception {
        LOGGER.log(Level.SEVERE, "Smack message parsing exception: ", (Throwable) unparsablePacket.getParsingException());
        LOGGER.severe("Unparsed content: " + unparsablePacket.getContent());
    }
}
