package org.jivesoftware.smack.parsing;

public class UnparsablePacket {
    private final String content;
    private final Exception e;

    public UnparsablePacket(String str, Exception exc) {
        this.content = str;
        this.e = exc;
    }

    public Exception getParsingException() {
        return this.e;
    }

    public String getContent() {
        return this.content;
    }
}
