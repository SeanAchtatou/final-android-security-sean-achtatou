package org.jivesoftware.smack.provider;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.packet.PacketExtension;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

public class ProviderFileLoader implements ProviderLoader {
    private static final Logger LOGGER = Logger.getLogger(ProviderFileLoader.class.getName());
    private List<Exception> exceptions;
    private Collection<ExtensionProviderInfo> extProviders;
    private Collection<IQProviderInfo> iqProviders;

    public ProviderFileLoader(InputStream inputStream) {
        this(inputStream, ProviderFileLoader.class.getClassLoader());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.jivesoftware.smack.provider.IQProviderInfo.<init>(java.lang.String, java.lang.String, java.lang.Class<? extends org.jivesoftware.smack.packet.IQ>):void
     arg types: [java.lang.String, java.lang.String, java.lang.Class<?>]
     candidates:
      org.jivesoftware.smack.provider.IQProviderInfo.<init>(java.lang.String, java.lang.String, org.jivesoftware.smack.provider.IQProvider):void
      org.jivesoftware.smack.provider.IQProviderInfo.<init>(java.lang.String, java.lang.String, java.lang.Class<? extends org.jivesoftware.smack.packet.IQ>):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
     arg types: [java.util.logging.Level, java.lang.String, java.lang.ClassNotFoundException]
     candidates:
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
     arg types: [java.util.logging.Level, java.lang.String, java.lang.IllegalArgumentException]
     candidates:
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
     arg types: [java.util.logging.Level, java.lang.String, java.lang.Exception]
     candidates:
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
    public ProviderFileLoader(InputStream inputStream, ClassLoader classLoader) {
        this.exceptions = new LinkedList();
        this.iqProviders = new ArrayList();
        this.extProviders = new ArrayList();
        try {
            XmlPullParser newPullParser = XmlPullParserFactory.newInstance().newPullParser();
            newPullParser.setFeature("http://xmlpull.org/v1/doc/features.html#process-namespaces", true);
            newPullParser.setInput(inputStream, "UTF-8");
            int eventType = newPullParser.getEventType();
            do {
                if (eventType == 2) {
                    String name = newPullParser.getName();
                    try {
                        if (!"smackProviders".equals(name)) {
                            newPullParser.next();
                            newPullParser.next();
                            String nextText = newPullParser.nextText();
                            newPullParser.next();
                            newPullParser.next();
                            String nextText2 = newPullParser.nextText();
                            newPullParser.next();
                            newPullParser.next();
                            try {
                                Class<?> loadClass = classLoader.loadClass(newPullParser.nextText());
                                if ("iqProvider".equals(name)) {
                                    if (IQProvider.class.isAssignableFrom(loadClass)) {
                                        this.iqProviders.add(new IQProviderInfo(nextText, nextText2, (IQProvider) loadClass.newInstance()));
                                    } else if (IQ.class.isAssignableFrom(loadClass)) {
                                        this.iqProviders.add(new IQProviderInfo(nextText, nextText2, (Class<? extends IQ>) loadClass));
                                    }
                                } else if (PacketExtensionProvider.class.isAssignableFrom(loadClass)) {
                                    this.extProviders.add(new ExtensionProviderInfo(nextText, nextText2, (PacketExtensionProvider) loadClass.newInstance()));
                                } else if (PacketExtension.class.isAssignableFrom(loadClass)) {
                                    this.extProviders.add(new ExtensionProviderInfo(nextText, nextText2, loadClass));
                                }
                            } catch (ClassNotFoundException e) {
                                LOGGER.log(Level.SEVERE, "Could not find provider class", (Throwable) e);
                                this.exceptions.add(e);
                            }
                        }
                    } catch (IllegalArgumentException e2) {
                        LOGGER.log(Level.SEVERE, "Invalid provider type found [" + name + "] when expecting iqProvider or extensionProvider", (Throwable) e2);
                        this.exceptions.add(e2);
                    }
                }
                eventType = newPullParser.next();
            } while (eventType != 1);
            try {
                inputStream.close();
            } catch (Exception e3) {
            }
        } catch (Exception e4) {
            LOGGER.log(Level.SEVERE, "Unknown error occurred while parsing provider file", (Throwable) e4);
            this.exceptions.add(e4);
            try {
                inputStream.close();
            } catch (Exception e5) {
            }
        } catch (Throwable th) {
            try {
                inputStream.close();
            } catch (Exception e6) {
            }
            throw th;
        }
    }

    public Collection<IQProviderInfo> getIQProviderInfo() {
        return this.iqProviders;
    }

    public Collection<ExtensionProviderInfo> getExtensionProviderInfo() {
        return this.extProviders;
    }

    public List<Exception> getLoadingExceptions() {
        return Collections.unmodifiableList(this.exceptions);
    }
}
