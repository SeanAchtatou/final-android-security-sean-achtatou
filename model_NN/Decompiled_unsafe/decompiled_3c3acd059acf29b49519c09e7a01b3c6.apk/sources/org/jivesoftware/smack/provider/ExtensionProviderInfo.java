package org.jivesoftware.smack.provider;

public final class ExtensionProviderInfo extends AbstractProviderInfo {
    public /* bridge */ /* synthetic */ String getElementName() {
        return super.getElementName();
    }

    public /* bridge */ /* synthetic */ String getNamespace() {
        return super.getNamespace();
    }

    public ExtensionProviderInfo(String str, String str2, PacketExtensionProvider packetExtensionProvider) {
        super(str, str2, packetExtensionProvider);
    }

    public ExtensionProviderInfo(String str, String str2, Class<?> cls) {
        super(str, str2, cls);
    }
}
