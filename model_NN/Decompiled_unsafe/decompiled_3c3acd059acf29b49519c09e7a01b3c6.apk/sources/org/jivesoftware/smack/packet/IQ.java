package org.jivesoftware.smack.packet;

import java.util.Locale;
import org.jivesoftware.smack.util.XmlStringBuilder;
import org.jivesoftware.smackx.xdata.Form;

public abstract class IQ extends Packet {
    private Type type = Type.GET;

    public abstract CharSequence getChildElementXML();

    public IQ() {
    }

    public IQ(IQ iq) {
        super(iq);
        this.type = iq.getType();
    }

    public Type getType() {
        return this.type;
    }

    public void setType(Type type2) {
        if (type2 == null) {
            this.type = Type.GET;
        } else {
            this.type = type2;
        }
    }

    public CharSequence toXML() {
        XmlStringBuilder xmlStringBuilder = new XmlStringBuilder();
        xmlStringBuilder.halfOpenElement("iq");
        addCommonAttributes(xmlStringBuilder);
        if (this.type == null) {
            xmlStringBuilder.attribute("type", "get");
        } else {
            xmlStringBuilder.attribute("type", this.type.toString());
        }
        xmlStringBuilder.rightAngelBracket();
        xmlStringBuilder.optAppend(getChildElementXML());
        XMPPError error = getError();
        if (error != null) {
            xmlStringBuilder.append(error.toXML());
        }
        xmlStringBuilder.closeElement("iq");
        return xmlStringBuilder;
    }

    public static IQ createResultIQ(IQ iq) {
        if (iq.getType() == Type.GET || iq.getType() == Type.SET) {
            AnonymousClass1 r0 = new IQ() {
                public String getChildElementXML() {
                    return null;
                }
            };
            r0.setType(Type.RESULT);
            r0.setPacketID(iq.getPacketID());
            r0.setFrom(iq.getTo());
            r0.setTo(iq.getFrom());
            return r0;
        }
        throw new IllegalArgumentException("IQ must be of type 'set' or 'get'. Original IQ: " + ((Object) iq.toXML()));
    }

    public static IQ createErrorResponse(IQ iq, XMPPError xMPPError) {
        if (iq.getType() == Type.GET || iq.getType() == Type.SET) {
            AnonymousClass2 r0 = new IQ(iq) {
                final /* synthetic */ IQ val$request;

                {
                    this.val$request = r1;
                }

                public CharSequence getChildElementXML() {
                    return this.val$request.getChildElementXML();
                }
            };
            r0.setType(Type.ERROR);
            r0.setPacketID(iq.getPacketID());
            r0.setFrom(iq.getTo());
            r0.setTo(iq.getFrom());
            r0.setError(xMPPError);
            return r0;
        }
        throw new IllegalArgumentException("IQ must be of type 'set' or 'get'. Original IQ: " + ((Object) iq.toXML()));
    }

    public static class Type {
        public static final Type ERROR = new Type("error");
        public static final Type GET = new Type("get");
        public static final Type RESULT = new Type(Form.TYPE_RESULT);
        public static final Type SET = new Type("set");
        private String value;

        public static Type fromString(String str) {
            if (str == null) {
                return null;
            }
            String lowerCase = str.toLowerCase(Locale.US);
            if (GET.toString().equals(lowerCase)) {
                return GET;
            }
            if (SET.toString().equals(lowerCase)) {
                return SET;
            }
            if (ERROR.toString().equals(lowerCase)) {
                return ERROR;
            }
            if (RESULT.toString().equals(lowerCase)) {
                return RESULT;
            }
            return null;
        }

        private Type(String str) {
            this.value = str;
        }

        public String toString() {
            return this.value;
        }
    }
}
