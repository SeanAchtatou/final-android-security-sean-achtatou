package org.jivesoftware.smack.packet;

import java.util.Map;
import org.jivesoftware.smack.util.XmlStringBuilder;

public class Registration extends IQ {
    private Map<String, String> attributes = null;
    private String instructions = null;

    public String getInstructions() {
        return this.instructions;
    }

    public void setInstructions(String str) {
        this.instructions = str;
    }

    public Map<String, String> getAttributes() {
        return this.attributes;
    }

    public void setAttributes(Map<String, String> map) {
        this.attributes = map;
    }

    public XmlStringBuilder getChildElementXML() {
        XmlStringBuilder xmlStringBuilder = new XmlStringBuilder();
        xmlStringBuilder.halfOpenElement("query");
        xmlStringBuilder.xmlnsAttribute("jabber:iq:register");
        xmlStringBuilder.rightAngelBracket();
        xmlStringBuilder.optElement("instructions", this.instructions);
        if (this.attributes != null && this.attributes.size() > 0) {
            for (String next : this.attributes.keySet()) {
                xmlStringBuilder.element(next, this.attributes.get(next));
            }
        }
        xmlStringBuilder.append(getExtensionsXML());
        xmlStringBuilder.closeElement("query");
        return xmlStringBuilder;
    }
}
