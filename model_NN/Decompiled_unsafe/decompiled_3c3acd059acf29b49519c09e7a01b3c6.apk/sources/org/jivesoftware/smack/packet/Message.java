package org.jivesoftware.smack.packet;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import org.jivesoftware.smack.util.XmlStringBuilder;
import org.jivesoftware.smackx.xhtmlim.provider.XHTMLExtensionProvider;

public class Message extends Packet {
    private final Set<Body> bodies = new HashSet();
    private String language;
    private final Set<Subject> subjects = new HashSet();
    private String thread = null;
    private Type type = Type.normal;

    public Message() {
    }

    public Message(String str) {
        setTo(str);
    }

    public Message(String str, Type type2) {
        setTo(str);
        if (type2 != null) {
            this.type = type2;
        }
    }

    public Type getType() {
        return this.type;
    }

    public void setType(Type type2) {
        if (type2 == null) {
            throw new IllegalArgumentException("Type cannot be null.");
        }
        this.type = type2;
    }

    public String getSubject() {
        return getSubject(null);
    }

    public String getSubject(String str) {
        Subject messageSubject = getMessageSubject(str);
        if (messageSubject == null) {
            return null;
        }
        return messageSubject.subject;
    }

    private Subject getMessageSubject(String str) {
        String determineLanguage = determineLanguage(str);
        for (Subject next : this.subjects) {
            if (determineLanguage.equals(next.language)) {
                return next;
            }
        }
        return null;
    }

    public Collection<Subject> getSubjects() {
        return Collections.unmodifiableCollection(this.subjects);
    }

    public void setSubject(String str) {
        if (str == null) {
            removeSubject("");
        } else {
            addSubject(null, str);
        }
    }

    public Subject addSubject(String str, String str2) {
        Subject subject = new Subject(determineLanguage(str), str2);
        this.subjects.add(subject);
        return subject;
    }

    public boolean removeSubject(String str) {
        String determineLanguage = determineLanguage(str);
        for (Subject next : this.subjects) {
            if (determineLanguage.equals(next.language)) {
                return this.subjects.remove(next);
            }
        }
        return false;
    }

    public boolean removeSubject(Subject subject) {
        return this.subjects.remove(subject);
    }

    public Collection<String> getSubjectLanguages() {
        Subject messageSubject = getMessageSubject(null);
        ArrayList arrayList = new ArrayList();
        for (Subject next : this.subjects) {
            if (!next.equals(messageSubject)) {
                arrayList.add(next.language);
            }
        }
        return Collections.unmodifiableCollection(arrayList);
    }

    public String getBody() {
        return getBody(null);
    }

    public String getBody(String str) {
        Body messageBody = getMessageBody(str);
        if (messageBody == null) {
            return null;
        }
        return messageBody.message;
    }

    private Body getMessageBody(String str) {
        String determineLanguage = determineLanguage(str);
        for (Body next : this.bodies) {
            if (determineLanguage.equals(next.language)) {
                return next;
            }
        }
        return null;
    }

    public Collection<Body> getBodies() {
        return Collections.unmodifiableCollection(this.bodies);
    }

    public void setBody(String str) {
        if (str == null) {
            removeBody("");
        } else {
            addBody(null, str);
        }
    }

    public Body addBody(String str, String str2) {
        Body body = new Body(determineLanguage(str), str2);
        this.bodies.add(body);
        return body;
    }

    public boolean removeBody(String str) {
        String determineLanguage = determineLanguage(str);
        for (Body next : this.bodies) {
            if (determineLanguage.equals(next.language)) {
                return this.bodies.remove(next);
            }
        }
        return false;
    }

    public boolean removeBody(Body body) {
        return this.bodies.remove(body);
    }

    public Collection<String> getBodyLanguages() {
        Body messageBody = getMessageBody(null);
        ArrayList arrayList = new ArrayList();
        for (Body next : this.bodies) {
            if (!next.equals(messageBody)) {
                arrayList.add(next.language);
            }
        }
        return Collections.unmodifiableCollection(arrayList);
    }

    public String getThread() {
        return this.thread;
    }

    public void setThread(String str) {
        this.thread = str;
    }

    public String getLanguage() {
        return this.language;
    }

    public void setLanguage(String str) {
        this.language = str;
    }

    private String determineLanguage(String str) {
        String str2 = "".equals(str) ? null : str;
        if (str2 == null && this.language != null) {
            return this.language;
        }
        if (str2 == null) {
            return getDefaultLanguage();
        }
        return str2;
    }

    public XmlStringBuilder toXML() {
        XMPPError error;
        XmlStringBuilder xmlStringBuilder = new XmlStringBuilder();
        xmlStringBuilder.halfOpenElement("message");
        xmlStringBuilder.xmlnsAttribute(getXmlns());
        xmlStringBuilder.xmllangAttribute(getLanguage());
        addCommonAttributes(xmlStringBuilder);
        if (this.type != Type.normal) {
            xmlStringBuilder.attribute("type", this.type);
        }
        xmlStringBuilder.rightAngelBracket();
        Subject messageSubject = getMessageSubject(null);
        if (messageSubject != null) {
            xmlStringBuilder.element("subject", messageSubject.subject);
        }
        for (Subject next : getSubjects()) {
            if (!next.equals(messageSubject)) {
                xmlStringBuilder.halfOpenElement("subject").xmllangAttribute(next.language).rightAngelBracket();
                xmlStringBuilder.escape(next.subject);
                xmlStringBuilder.closeElement("subject");
            }
        }
        Body messageBody = getMessageBody(null);
        if (messageBody != null) {
            xmlStringBuilder.element(XHTMLExtensionProvider.BODY_ELEMENT, messageBody.message);
        }
        for (Body next2 : getBodies()) {
            if (!next2.equals(messageBody)) {
                xmlStringBuilder.halfOpenElement(XHTMLExtensionProvider.BODY_ELEMENT).xmllangAttribute(next2.getLanguage()).rightAngelBracket();
                xmlStringBuilder.escape(next2.getMessage());
                xmlStringBuilder.closeElement(XHTMLExtensionProvider.BODY_ELEMENT);
            }
        }
        xmlStringBuilder.optElement("thread", this.thread);
        if (this.type == Type.error && (error = getError()) != null) {
            xmlStringBuilder.append(error.toXML());
        }
        xmlStringBuilder.append(getExtensionsXML());
        xmlStringBuilder.closeElement("message");
        return xmlStringBuilder;
    }

    public boolean equals(Object obj) {
        boolean z = true;
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        Message message = (Message) obj;
        if (!super.equals(message) || this.bodies.size() != message.bodies.size() || !this.bodies.containsAll(message.bodies)) {
            return false;
        }
        if (this.language != null) {
            if (!this.language.equals(message.language)) {
                return false;
            }
        } else if (message.language != null) {
            return false;
        }
        if (this.subjects.size() != message.subjects.size() || !this.subjects.containsAll(message.subjects)) {
            return false;
        }
        if (this.thread != null) {
            if (!this.thread.equals(message.thread)) {
                return false;
            }
        } else if (message.thread != null) {
            return false;
        }
        if (this.type != message.type) {
            z = false;
        }
        return z;
    }

    public int hashCode() {
        int i;
        int i2 = 0;
        int hashCode = (((this.type != null ? this.type.hashCode() : 0) * 31) + this.subjects.hashCode()) * 31;
        if (this.thread != null) {
            i = this.thread.hashCode();
        } else {
            i = 0;
        }
        int i3 = (i + hashCode) * 31;
        if (this.language != null) {
            i2 = this.language.hashCode();
        }
        return ((i3 + i2) * 31) + this.bodies.hashCode();
    }

    public static class Subject {
        /* access modifiers changed from: private */
        public String language;
        /* access modifiers changed from: private */
        public String subject;

        private Subject(String str, String str2) {
            if (str == null) {
                throw new NullPointerException("Language cannot be null.");
            } else if (str2 == null) {
                throw new NullPointerException("Subject cannot be null.");
            } else {
                this.language = str;
                this.subject = str2;
            }
        }

        public String getLanguage() {
            return this.language;
        }

        public String getSubject() {
            return this.subject;
        }

        public int hashCode() {
            return ((this.language.hashCode() + 31) * 31) + this.subject.hashCode();
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            Subject subject2 = (Subject) obj;
            if (!this.language.equals(subject2.language) || !this.subject.equals(subject2.subject)) {
                return false;
            }
            return true;
        }
    }

    public static class Body {
        /* access modifiers changed from: private */
        public String language;
        /* access modifiers changed from: private */
        public String message;

        private Body(String str, String str2) {
            if (str == null) {
                throw new NullPointerException("Language cannot be null.");
            } else if (str2 == null) {
                throw new NullPointerException("Message cannot be null.");
            } else {
                this.language = str;
                this.message = str2;
            }
        }

        public String getLanguage() {
            return this.language;
        }

        public String getMessage() {
            return this.message;
        }

        public int hashCode() {
            return ((this.language.hashCode() + 31) * 31) + this.message.hashCode();
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            Body body = (Body) obj;
            if (!this.language.equals(body.language) || !this.message.equals(body.message)) {
                return false;
            }
            return true;
        }
    }

    public enum Type {
        normal,
        chat,
        groupchat,
        headline,
        error;

        public static Type fromString(String str) {
            try {
                return valueOf(str);
            } catch (Exception e) {
                return normal;
            }
        }
    }
}
