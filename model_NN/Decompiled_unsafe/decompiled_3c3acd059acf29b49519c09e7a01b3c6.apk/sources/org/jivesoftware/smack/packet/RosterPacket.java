package org.jivesoftware.smack.packet;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;
import org.jivesoftware.smack.util.StringUtils;
import org.jivesoftware.smack.util.XmlStringBuilder;

public class RosterPacket extends IQ {
    private final List<Item> rosterItems = new ArrayList();
    private String rosterVersion;

    public enum ItemType {
        none,
        to,
        from,
        both,
        remove
    }

    public void addRosterItem(Item item) {
        synchronized (this.rosterItems) {
            this.rosterItems.add(item);
        }
    }

    public int getRosterItemCount() {
        int size;
        synchronized (this.rosterItems) {
            size = this.rosterItems.size();
        }
        return size;
    }

    public Collection<Item> getRosterItems() {
        List unmodifiableList;
        synchronized (this.rosterItems) {
            unmodifiableList = Collections.unmodifiableList(new ArrayList(this.rosterItems));
        }
        return unmodifiableList;
    }

    public CharSequence getChildElementXML() {
        XmlStringBuilder xmlStringBuilder = new XmlStringBuilder();
        xmlStringBuilder.halfOpenElement("query");
        xmlStringBuilder.xmlnsAttribute("jabber:iq:roster");
        xmlStringBuilder.optAttribute("ver", this.rosterVersion);
        xmlStringBuilder.rightAngelBracket();
        synchronized (this.rosterItems) {
            for (Item xml : this.rosterItems) {
                xmlStringBuilder.append((CharSequence) xml.toXML());
            }
        }
        xmlStringBuilder.closeElement("query");
        return xmlStringBuilder;
    }

    public String getVersion() {
        return this.rosterVersion;
    }

    public void setVersion(String str) {
        this.rosterVersion = str;
    }

    public static class Item {
        private final Set<String> groupNames = new CopyOnWriteArraySet();
        private ItemStatus itemStatus = null;
        private ItemType itemType = null;
        private String name;
        private String user;

        public Item(String str, String str2) {
            this.user = str.toLowerCase(Locale.US);
            this.name = str2;
        }

        public String getUser() {
            return this.user;
        }

        public String getName() {
            return this.name;
        }

        public void setName(String str) {
            this.name = str;
        }

        public ItemType getItemType() {
            return this.itemType;
        }

        public void setItemType(ItemType itemType2) {
            this.itemType = itemType2;
        }

        public ItemStatus getItemStatus() {
            return this.itemStatus;
        }

        public void setItemStatus(ItemStatus itemStatus2) {
            this.itemStatus = itemStatus2;
        }

        public Set<String> getGroupNames() {
            return Collections.unmodifiableSet(this.groupNames);
        }

        public void addGroupName(String str) {
            this.groupNames.add(str);
        }

        public void removeGroupName(String str) {
            this.groupNames.remove(str);
        }

        public String toXML() {
            StringBuilder sb = new StringBuilder();
            sb.append("<item jid=\"").append(StringUtils.escapeForXML(this.user)).append("\"");
            if (this.name != null) {
                sb.append(" name=\"").append(StringUtils.escapeForXML(this.name)).append("\"");
            }
            if (this.itemType != null) {
                sb.append(" subscription=\"").append(this.itemType).append("\"");
            }
            if (this.itemStatus != null) {
                sb.append(" ask=\"").append(this.itemStatus).append("\"");
            }
            sb.append(">");
            for (String escapeForXML : this.groupNames) {
                sb.append("<group>").append(StringUtils.escapeForXML(escapeForXML)).append("</group>");
            }
            sb.append("</item>");
            return sb.toString();
        }

        public int hashCode() {
            int i = 0;
            int hashCode = ((this.name == null ? 0 : this.name.hashCode()) + (((this.itemType == null ? 0 : this.itemType.hashCode()) + (((this.itemStatus == null ? 0 : this.itemStatus.hashCode()) + (((this.groupNames == null ? 0 : this.groupNames.hashCode()) + 31) * 31)) * 31)) * 31)) * 31;
            if (this.user != null) {
                i = this.user.hashCode();
            }
            return hashCode + i;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            Item item = (Item) obj;
            if (this.groupNames == null) {
                if (item.groupNames != null) {
                    return false;
                }
            } else if (!this.groupNames.equals(item.groupNames)) {
                return false;
            }
            if (this.itemStatus != item.itemStatus) {
                return false;
            }
            if (this.itemType != item.itemType) {
                return false;
            }
            if (this.name == null) {
                if (item.name != null) {
                    return false;
                }
            } else if (!this.name.equals(item.name)) {
                return false;
            }
            if (this.user == null) {
                if (item.user != null) {
                    return false;
                }
                return true;
            } else if (!this.user.equals(item.user)) {
                return false;
            } else {
                return true;
            }
        }
    }

    public enum ItemStatus {
        subscribe,
        unsubscribe;
        
        public static final ItemStatus SUBSCRIPTION_PENDING = subscribe;
        public static final ItemStatus UNSUBSCRIPTION_PENDING = unsubscribe;

        public static ItemStatus fromString(String str) {
            if (str == null) {
                return null;
            }
            try {
                return valueOf(str);
            } catch (IllegalArgumentException e) {
                return null;
            }
        }
    }
}
