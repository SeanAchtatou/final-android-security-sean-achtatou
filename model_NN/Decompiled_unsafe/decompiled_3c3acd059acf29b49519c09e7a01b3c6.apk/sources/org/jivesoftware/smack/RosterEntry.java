package org.jivesoftware.smack;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.packet.RosterPacket;

public class RosterEntry {
    private final XMPPConnection connection;
    private String name;
    private final Roster roster;
    private RosterPacket.ItemStatus status;
    private RosterPacket.ItemType type;
    private String user;

    RosterEntry(String str, String str2, RosterPacket.ItemType itemType, RosterPacket.ItemStatus itemStatus, Roster roster2, XMPPConnection xMPPConnection) {
        this.user = str;
        this.name = str2;
        this.type = itemType;
        this.status = itemStatus;
        this.roster = roster2;
        this.connection = xMPPConnection;
    }

    public String getUser() {
        return this.user;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String str) throws SmackException.NotConnectedException {
        if (str == null || !str.equals(this.name)) {
            this.name = str;
            RosterPacket rosterPacket = new RosterPacket();
            rosterPacket.setType(IQ.Type.SET);
            rosterPacket.addRosterItem(toRosterItem(this));
            this.connection.sendPacket(rosterPacket);
        }
    }

    /* access modifiers changed from: package-private */
    public void updateState(String str, RosterPacket.ItemType itemType, RosterPacket.ItemStatus itemStatus) {
        this.name = str;
        this.type = itemType;
        this.status = itemStatus;
    }

    public Collection<RosterGroup> getGroups() {
        ArrayList arrayList = new ArrayList();
        for (RosterGroup next : this.roster.getGroups()) {
            if (next.contains(this)) {
                arrayList.add(next);
            }
        }
        return Collections.unmodifiableCollection(arrayList);
    }

    public RosterPacket.ItemType getType() {
        return this.type;
    }

    public RosterPacket.ItemStatus getStatus() {
        return this.status;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        if (this.name != null) {
            sb.append(this.name).append(": ");
        }
        sb.append(this.user);
        Collection<RosterGroup> groups = getGroups();
        if (!groups.isEmpty()) {
            sb.append(" [");
            Iterator<RosterGroup> it = groups.iterator();
            sb.append(it.next().getName());
            while (it.hasNext()) {
                sb.append(", ");
                sb.append(it.next().getName());
            }
            sb.append("]");
        }
        return sb.toString();
    }

    public int hashCode() {
        if (this.user == null) {
            return 0;
        }
        return this.user.hashCode();
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || !(obj instanceof RosterEntry)) {
            return false;
        }
        return this.user.equals(((RosterEntry) obj).getUser());
    }

    public boolean equalsDeep(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        RosterEntry rosterEntry = (RosterEntry) obj;
        if (this.name == null) {
            if (rosterEntry.name != null) {
                return false;
            }
        } else if (!this.name.equals(rosterEntry.name)) {
            return false;
        }
        if (this.status == null) {
            if (rosterEntry.status != null) {
                return false;
            }
        } else if (!this.status.equals(rosterEntry.status)) {
            return false;
        }
        if (this.type == null) {
            if (rosterEntry.type != null) {
                return false;
            }
        } else if (!this.type.equals(rosterEntry.type)) {
            return false;
        }
        if (this.user == null) {
            if (rosterEntry.user != null) {
                return false;
            }
            return true;
        } else if (!this.user.equals(rosterEntry.user)) {
            return false;
        } else {
            return true;
        }
    }

    static RosterPacket.Item toRosterItem(RosterEntry rosterEntry) {
        RosterPacket.Item item = new RosterPacket.Item(rosterEntry.getUser(), rosterEntry.getName());
        item.setItemType(rosterEntry.getType());
        item.setItemStatus(rosterEntry.getStatus());
        for (RosterGroup name2 : rosterEntry.getGroups()) {
            item.addGroupName(name2.getName());
        }
        return item;
    }
}
