package org.jivesoftware.smack;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.filter.AndFilter;
import org.jivesoftware.smack.filter.IQReplyFilter;
import org.jivesoftware.smack.filter.IQTypeFilter;
import org.jivesoftware.smack.filter.PacketFilter;
import org.jivesoftware.smack.filter.PacketTypeFilter;
import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smack.packet.RosterPacket;
import org.jivesoftware.smack.util.StringUtils;

public class Roster {
    /* access modifiers changed from: private */
    public static final Logger LOGGER = Logger.getLogger(Roster.class.getName());
    private static final PacketFilter PRESENCE_PACKET_FILTER = new PacketTypeFilter(Presence.class);
    private static final PacketFilter ROSTER_PUSH_FILTER = new AndFilter(new PacketTypeFilter(RosterPacket.class), new IQTypeFilter(IQ.Type.SET));
    private static SubscriptionMode defaultSubscriptionMode = SubscriptionMode.accept_all;
    /* access modifiers changed from: private */
    public final XMPPConnection connection;
    /* access modifiers changed from: private */
    public final Map<String, RosterEntry> entries = new ConcurrentHashMap();
    private final Map<String, RosterGroup> groups = new ConcurrentHashMap();
    /* access modifiers changed from: private */
    public final Map<String, Map<String, Presence>> presenceMap = new ConcurrentHashMap();
    private final PresencePacketListener presencePacketListener = new PresencePacketListener();
    boolean rosterInitialized = false;
    private final List<RosterListener> rosterListeners = new CopyOnWriteArrayList();
    /* access modifiers changed from: private */
    public final RosterStore rosterStore;
    /* access modifiers changed from: private */
    public SubscriptionMode subscriptionMode = getDefaultSubscriptionMode();
    private final List<RosterEntry> unfiledEntries = new CopyOnWriteArrayList();

    public enum SubscriptionMode {
        accept_all,
        reject_all,
        manual
    }

    public static SubscriptionMode getDefaultSubscriptionMode() {
        return defaultSubscriptionMode;
    }

    public static void setDefaultSubscriptionMode(SubscriptionMode subscriptionMode2) {
        defaultSubscriptionMode = subscriptionMode2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
     arg types: [java.util.logging.Level, java.lang.String, org.jivesoftware.smack.SmackException]
     candidates:
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
    Roster(XMPPConnection xMPPConnection) {
        this.connection = xMPPConnection;
        this.rosterStore = xMPPConnection.getConfiguration().getRosterStore();
        xMPPConnection.addPacketListener(new RosterPushListener(), ROSTER_PUSH_FILTER);
        xMPPConnection.addPacketListener(this.presencePacketListener, PRESENCE_PACKET_FILTER);
        xMPPConnection.addConnectionListener(new AbstractConnectionListener() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
             arg types: [java.util.logging.Level, java.lang.String, org.jivesoftware.smack.SmackException$NotConnectedException]
             candidates:
              ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
              ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
              ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
              ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
            public void connectionClosed() {
                try {
                    Roster.this.setOfflinePresences();
                } catch (SmackException.NotConnectedException e) {
                    Roster.LOGGER.log(Level.SEVERE, "Not connected exception", (Throwable) e);
                }
            }

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
             arg types: [java.util.logging.Level, java.lang.String, java.lang.Exception]
             candidates:
              ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
              ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
              ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
              ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
            public void connectionClosedOnError(Exception exc) {
                try {
                    Roster.this.setOfflinePresences();
                } catch (SmackException.NotConnectedException e) {
                    Roster.LOGGER.log(Level.SEVERE, "Not connected exception", (Throwable) exc);
                }
            }
        });
        if (xMPPConnection.isAuthenticated()) {
            try {
                reload();
            } catch (SmackException e) {
                LOGGER.log(Level.SEVERE, "Could not reload Roster", (Throwable) e);
            }
        }
        xMPPConnection.addConnectionListener(new AbstractConnectionListener() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
             arg types: [java.util.logging.Level, java.lang.String, org.jivesoftware.smack.SmackException]
             candidates:
              ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
              ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
              ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
              ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
            public void authenticated(XMPPConnection xMPPConnection) {
                if (!xMPPConnection.isAnonymous() && xMPPConnection.getConfiguration().isRosterLoadedAtLogin()) {
                    try {
                        Roster.this.reload();
                    } catch (SmackException e) {
                        Roster.LOGGER.log(Level.SEVERE, "Could not reload Roster", (Throwable) e);
                    }
                }
            }
        });
    }

    public SubscriptionMode getSubscriptionMode() {
        return this.subscriptionMode;
    }

    public void setSubscriptionMode(SubscriptionMode subscriptionMode2) {
        this.subscriptionMode = subscriptionMode2;
    }

    public void reload() throws SmackException.NotLoggedInException, SmackException.NotConnectedException {
        if (!this.connection.isAuthenticated()) {
            throw new SmackException.NotLoggedInException();
        } else if (this.connection.isAnonymous()) {
            throw new IllegalStateException("Anonymous users can't have a roster.");
        } else {
            RosterPacket rosterPacket = new RosterPacket();
            if (this.rosterStore != null && this.connection.isRosterVersioningSupported()) {
                rosterPacket.setVersion(this.rosterStore.getRosterVersion());
            }
            this.connection.addPacketListener(new RosterResultListener(), new IQReplyFilter(rosterPacket, this.connection));
            this.connection.sendPacket(rosterPacket);
        }
    }

    public void addRosterListener(RosterListener rosterListener) {
        if (!this.rosterListeners.contains(rosterListener)) {
            this.rosterListeners.add(rosterListener);
        }
    }

    public void removeRosterListener(RosterListener rosterListener) {
        this.rosterListeners.remove(rosterListener);
    }

    public RosterGroup createGroup(String str) {
        if (this.connection.isAnonymous()) {
            throw new IllegalStateException("Anonymous users can't have a roster.");
        } else if (this.groups.containsKey(str)) {
            return this.groups.get(str);
        } else {
            RosterGroup rosterGroup = new RosterGroup(str, this.connection);
            this.groups.put(str, rosterGroup);
            return rosterGroup;
        }
    }

    public void createEntry(String str, String str2, String[] strArr) throws SmackException.NotLoggedInException, SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        if (!this.connection.isAuthenticated()) {
            throw new SmackException.NotLoggedInException();
        } else if (this.connection.isAnonymous()) {
            throw new IllegalStateException("Anonymous users can't have a roster.");
        } else {
            RosterPacket rosterPacket = new RosterPacket();
            rosterPacket.setType(IQ.Type.SET);
            RosterPacket.Item item = new RosterPacket.Item(str, str2);
            if (strArr != null) {
                for (String str3 : strArr) {
                    if (str3 != null && str3.trim().length() > 0) {
                        item.addGroupName(str3);
                    }
                }
            }
            rosterPacket.addRosterItem(item);
            this.connection.createPacketCollectorAndSend(rosterPacket).nextResultOrThrow();
            Presence presence = new Presence(Presence.Type.subscribe);
            presence.setTo(str);
            this.connection.sendPacket(presence);
        }
    }

    public void removeEntry(RosterEntry rosterEntry) throws SmackException.NotLoggedInException, SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        if (!this.connection.isAuthenticated()) {
            throw new SmackException.NotLoggedInException();
        } else if (this.connection.isAnonymous()) {
            throw new IllegalStateException("Anonymous users can't have a roster.");
        } else if (this.entries.containsKey(rosterEntry.getUser())) {
            RosterPacket rosterPacket = new RosterPacket();
            rosterPacket.setType(IQ.Type.SET);
            RosterPacket.Item rosterItem = RosterEntry.toRosterItem(rosterEntry);
            rosterItem.setItemType(RosterPacket.ItemType.remove);
            rosterPacket.addRosterItem(rosterItem);
            this.connection.createPacketCollectorAndSend(rosterPacket).nextResultOrThrow();
        }
    }

    public int getEntryCount() {
        return getEntries().size();
    }

    public Collection<RosterEntry> getEntries() {
        HashSet hashSet = new HashSet();
        for (RosterGroup entries2 : getGroups()) {
            hashSet.addAll(entries2.getEntries());
        }
        hashSet.addAll(this.unfiledEntries);
        return Collections.unmodifiableCollection(hashSet);
    }

    public int getUnfiledEntryCount() {
        return this.unfiledEntries.size();
    }

    public Collection<RosterEntry> getUnfiledEntries() {
        return Collections.unmodifiableList(this.unfiledEntries);
    }

    public RosterEntry getEntry(String str) {
        if (str == null) {
            return null;
        }
        return this.entries.get(str.toLowerCase(Locale.US));
    }

    public boolean contains(String str) {
        return getEntry(str) != null;
    }

    public RosterGroup getGroup(String str) {
        return this.groups.get(str);
    }

    public int getGroupCount() {
        return this.groups.size();
    }

    public Collection<RosterGroup> getGroups() {
        return Collections.unmodifiableCollection(this.groups.values());
    }

    /* JADX WARNING: Code restructure failed: missing block: B:21:0x006a, code lost:
        if (r3.compareTo((java.lang.Enum) r4) >= 0) goto L_0x006c;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public org.jivesoftware.smack.packet.Presence getPresence(java.lang.String r7) {
        /*
            r6 = this;
            java.lang.String r0 = org.jivesoftware.smack.util.StringUtils.parseBareAddress(r7)
            java.lang.String r0 = r6.getPresenceMapKey(r0)
            java.util.Map<java.lang.String, java.util.Map<java.lang.String, org.jivesoftware.smack.packet.Presence>> r1 = r6.presenceMap
            java.lang.Object r0 = r1.get(r0)
            java.util.Map r0 = (java.util.Map) r0
            if (r0 != 0) goto L_0x001d
            org.jivesoftware.smack.packet.Presence r2 = new org.jivesoftware.smack.packet.Presence
            org.jivesoftware.smack.packet.Presence$Type r0 = org.jivesoftware.smack.packet.Presence.Type.unavailable
            r2.<init>(r0)
            r2.setFrom(r7)
        L_0x001c:
            return r2
        L_0x001d:
            r2 = 0
            java.util.Set r1 = r0.keySet()
            java.util.Iterator r5 = r1.iterator()
        L_0x0026:
            boolean r1 = r5.hasNext()
            if (r1 == 0) goto L_0x006e
            java.lang.Object r1 = r5.next()
            java.lang.String r1 = (java.lang.String) r1
            java.lang.Object r1 = r0.get(r1)
            org.jivesoftware.smack.packet.Presence r1 = (org.jivesoftware.smack.packet.Presence) r1
            boolean r3 = r1.isAvailable()
            if (r3 == 0) goto L_0x0026
            if (r2 == 0) goto L_0x004a
            int r3 = r1.getPriority()
            int r4 = r2.getPriority()
            if (r3 <= r4) goto L_0x004c
        L_0x004a:
            r2 = r1
            goto L_0x0026
        L_0x004c:
            int r3 = r1.getPriority()
            int r4 = r2.getPriority()
            if (r3 != r4) goto L_0x006c
            org.jivesoftware.smack.packet.Presence$Mode r3 = r1.getMode()
            if (r3 != 0) goto L_0x005e
            org.jivesoftware.smack.packet.Presence$Mode r3 = org.jivesoftware.smack.packet.Presence.Mode.available
        L_0x005e:
            org.jivesoftware.smack.packet.Presence$Mode r4 = r2.getMode()
            if (r4 != 0) goto L_0x0066
            org.jivesoftware.smack.packet.Presence$Mode r4 = org.jivesoftware.smack.packet.Presence.Mode.available
        L_0x0066:
            int r3 = r3.compareTo(r4)
            if (r3 < 0) goto L_0x004a
        L_0x006c:
            r1 = r2
            goto L_0x004a
        L_0x006e:
            if (r2 != 0) goto L_0x001c
            org.jivesoftware.smack.packet.Presence r2 = new org.jivesoftware.smack.packet.Presence
            org.jivesoftware.smack.packet.Presence$Type r0 = org.jivesoftware.smack.packet.Presence.Type.unavailable
            r2.<init>(r0)
            r2.setFrom(r7)
            goto L_0x001c
        */
        throw new UnsupportedOperationException("Method not decompiled: org.jivesoftware.smack.Roster.getPresence(java.lang.String):org.jivesoftware.smack.packet.Presence");
    }

    public Presence getPresenceResource(String str) {
        String presenceMapKey = getPresenceMapKey(str);
        String parseResource = StringUtils.parseResource(str);
        Map map = this.presenceMap.get(presenceMapKey);
        if (map == null) {
            Presence presence = new Presence(Presence.Type.unavailable);
            presence.setFrom(str);
            return presence;
        }
        Presence presence2 = (Presence) map.get(parseResource);
        if (presence2 != null) {
            return presence2;
        }
        Presence presence3 = new Presence(Presence.Type.unavailable);
        presence3.setFrom(str);
        return presence3;
    }

    public List<Presence> getPresences(String str) {
        List asList;
        Map map = this.presenceMap.get(getPresenceMapKey(str));
        if (map == null) {
            Presence presence = new Presence(Presence.Type.unavailable);
            presence.setFrom(str);
            asList = Arrays.asList(presence);
        } else {
            ArrayList arrayList = new ArrayList();
            for (Presence presence2 : map.values()) {
                if (presence2.isAvailable()) {
                    arrayList.add(presence2);
                }
            }
            if (!arrayList.isEmpty()) {
                asList = arrayList;
            } else {
                Presence presence3 = new Presence(Presence.Type.unavailable);
                presence3.setFrom(str);
                asList = Arrays.asList(presence3);
            }
        }
        return Collections.unmodifiableList(asList);
    }

    /* access modifiers changed from: private */
    public String getPresenceMapKey(String str) {
        if (str == null) {
            return null;
        }
        if (!contains(str)) {
            str = StringUtils.parseBareAddress(str);
        }
        return str.toLowerCase(Locale.US);
    }

    /* access modifiers changed from: private */
    public void setOfflinePresences() throws SmackException.NotConnectedException {
        for (String next : this.presenceMap.keySet()) {
            Map map = this.presenceMap.get(next);
            if (map != null) {
                for (String str : map.keySet()) {
                    Presence presence = new Presence(Presence.Type.unavailable);
                    presence.setFrom(next + "/" + str);
                    this.presencePacketListener.processPacket(presence);
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void fireRosterChangedEvent(Collection<String> collection, Collection<String> collection2, Collection<String> collection3) {
        for (RosterListener next : this.rosterListeners) {
            if (!collection.isEmpty()) {
                next.entriesAdded(collection);
            }
            if (!collection2.isEmpty()) {
                next.entriesUpdated(collection2);
            }
            if (!collection3.isEmpty()) {
                next.entriesDeleted(collection3);
            }
        }
    }

    /* access modifiers changed from: private */
    public void fireRosterPresenceEvent(Presence presence) {
        for (RosterListener presenceChanged : this.rosterListeners) {
            presenceChanged.presenceChanged(presence);
        }
    }

    /* access modifiers changed from: private */
    public void addUpdateEntry(Collection<String> collection, Collection<String> collection2, Collection<String> collection3, RosterPacket.Item item, RosterEntry rosterEntry) {
        RosterEntry put = this.entries.put(item.getUser(), rosterEntry);
        if (put == null) {
            collection.add(item.getUser());
        } else {
            RosterPacket.Item rosterItem = RosterEntry.toRosterItem(put);
            if (!put.equalsDeep(rosterEntry) || !item.getGroupNames().equals(rosterItem.getGroupNames())) {
                collection2.add(item.getUser());
            } else {
                collection3.add(item.getUser());
            }
        }
        if (item.getGroupNames().isEmpty()) {
            this.unfiledEntries.remove(rosterEntry);
            this.unfiledEntries.add(rosterEntry);
        } else {
            this.unfiledEntries.remove(rosterEntry);
        }
        ArrayList arrayList = new ArrayList();
        for (String next : item.getGroupNames()) {
            arrayList.add(next);
            RosterGroup group = getGroup(next);
            if (group == null) {
                group = createGroup(next);
                this.groups.put(next, group);
            }
            group.addEntryLocal(rosterEntry);
        }
        ArrayList<String> arrayList2 = new ArrayList<>();
        for (RosterGroup name : getGroups()) {
            arrayList2.add(name.getName());
        }
        arrayList2.removeAll(arrayList);
        for (String str : arrayList2) {
            RosterGroup group2 = getGroup(str);
            group2.removeEntryLocal(rosterEntry);
            if (group2.getEntryCount() == 0) {
                this.groups.remove(str);
            }
        }
    }

    /* access modifiers changed from: private */
    public void deleteEntry(Collection<String> collection, RosterEntry rosterEntry) {
        String user = rosterEntry.getUser();
        this.entries.remove(user);
        this.unfiledEntries.remove(rosterEntry);
        this.presenceMap.remove(StringUtils.parseBareAddress(user));
        collection.add(user);
        for (Map.Entry next : this.groups.entrySet()) {
            RosterGroup rosterGroup = (RosterGroup) next.getValue();
            rosterGroup.removeEntryLocal(rosterEntry);
            if (rosterGroup.getEntryCount() == 0) {
                this.groups.remove(next.getKey());
            }
        }
    }

    /* access modifiers changed from: private */
    public void removeEmptyGroups() {
        for (RosterGroup next : getGroups()) {
            if (next.getEntryCount() == 0) {
                this.groups.remove(next.getName());
            }
        }
    }

    /* access modifiers changed from: private */
    public static boolean hasValidSubscriptionType(RosterPacket.Item item) {
        return item.getItemType().equals(RosterPacket.ItemType.none) || item.getItemType().equals(RosterPacket.ItemType.from) || item.getItemType().equals(RosterPacket.ItemType.to) || item.getItemType().equals(RosterPacket.ItemType.both);
    }

    private class PresencePacketListener implements PacketListener {
        private PresencePacketListener() {
        }

        public void processPacket(Packet packet) throws SmackException.NotConnectedException {
            Map map;
            Object obj;
            Map map2;
            Presence presence = (Presence) packet;
            String from = presence.getFrom();
            String access$500 = Roster.this.getPresenceMapKey(from);
            if (presence.getType() == Presence.Type.available) {
                if (Roster.this.presenceMap.get(access$500) == null) {
                    map2 = new ConcurrentHashMap();
                    Roster.this.presenceMap.put(access$500, map2);
                } else {
                    map2 = (Map) Roster.this.presenceMap.get(access$500);
                }
                map2.remove("");
                map2.put(StringUtils.parseResource(from), presence);
                if (((RosterEntry) Roster.this.entries.get(access$500)) != null) {
                    Roster.this.fireRosterPresenceEvent(presence);
                }
            } else if (presence.getType() == Presence.Type.unavailable) {
                if ("".equals(StringUtils.parseResource(from))) {
                    if (Roster.this.presenceMap.get(access$500) == null) {
                        obj = new ConcurrentHashMap();
                        Roster.this.presenceMap.put(access$500, obj);
                    } else {
                        obj = (Map) Roster.this.presenceMap.get(access$500);
                    }
                    obj.put("", presence);
                } else if (Roster.this.presenceMap.get(access$500) != null) {
                    ((Map) Roster.this.presenceMap.get(access$500)).put(StringUtils.parseResource(from), presence);
                }
                if (((RosterEntry) Roster.this.entries.get(access$500)) != null) {
                    Roster.this.fireRosterPresenceEvent(presence);
                }
            } else if (presence.getType() == Presence.Type.subscribe) {
                Presence presence2 = null;
                switch (Roster.this.subscriptionMode) {
                    case accept_all:
                        presence2 = new Presence(Presence.Type.subscribed);
                        break;
                    case reject_all:
                        presence2 = new Presence(Presence.Type.unsubscribed);
                        break;
                }
                if (presence2 != null) {
                    presence2.setTo(presence.getFrom());
                    Roster.this.connection.sendPacket(presence2);
                }
            } else if (presence.getType() == Presence.Type.unsubscribe) {
                if (Roster.this.subscriptionMode != SubscriptionMode.manual) {
                    Presence presence3 = new Presence(Presence.Type.unsubscribed);
                    presence3.setTo(presence.getFrom());
                    Roster.this.connection.sendPacket(presence3);
                }
            } else if (presence.getType() == Presence.Type.error && "".equals(StringUtils.parseResource(from))) {
                if (!Roster.this.presenceMap.containsKey(access$500)) {
                    map = new ConcurrentHashMap();
                    Roster.this.presenceMap.put(access$500, map);
                } else {
                    map = (Map) Roster.this.presenceMap.get(access$500);
                    map.clear();
                }
                map.put("", presence);
                if (((RosterEntry) Roster.this.entries.get(access$500)) != null) {
                    Roster.this.fireRosterPresenceEvent(presence);
                }
            }
        }
    }

    private class RosterResultListener implements PacketListener {
        private RosterResultListener() {
        }

        public void processPacket(Packet packet) {
            Roster.this.connection.removePacketListener(this);
            IQ iq = (IQ) packet;
            if (!iq.getType().equals(IQ.Type.RESULT)) {
                Roster.LOGGER.severe("Roster result IQ not of type result. Packet: " + ((Object) iq.toXML()));
                return;
            }
            ArrayList arrayList = new ArrayList();
            ArrayList arrayList2 = new ArrayList();
            ArrayList arrayList3 = new ArrayList();
            ArrayList arrayList4 = new ArrayList();
            if (packet instanceof RosterPacket) {
                RosterPacket rosterPacket = (RosterPacket) packet;
                String version = rosterPacket.getVersion();
                ArrayList arrayList5 = new ArrayList();
                for (RosterPacket.Item next : rosterPacket.getRosterItems()) {
                    if (Roster.hasValidSubscriptionType(next)) {
                        arrayList5.add(next);
                    }
                }
                Iterator it = arrayList5.iterator();
                while (it.hasNext()) {
                    RosterPacket.Item item = (RosterPacket.Item) it.next();
                    Roster.this.addUpdateEntry(arrayList, arrayList2, arrayList4, item, new RosterEntry(item.getUser(), item.getName(), item.getItemType(), item.getItemStatus(), Roster.this, Roster.this.connection));
                }
                HashSet<String> hashSet = new HashSet<>();
                for (RosterEntry user : Roster.this.entries.values()) {
                    hashSet.add(user.getUser());
                }
                hashSet.removeAll(arrayList);
                hashSet.removeAll(arrayList2);
                hashSet.removeAll(arrayList4);
                for (String str : hashSet) {
                    Roster.this.deleteEntry(arrayList3, (RosterEntry) Roster.this.entries.get(str));
                }
                if (Roster.this.rosterStore != null) {
                    Roster.this.rosterStore.resetEntries(arrayList5, version);
                }
                Roster.this.removeEmptyGroups();
            } else {
                for (RosterPacket.Item next2 : Roster.this.rosterStore.getEntries()) {
                    Roster.this.addUpdateEntry(arrayList, arrayList2, arrayList4, next2, new RosterEntry(next2.getUser(), next2.getName(), next2.getItemType(), next2.getItemStatus(), Roster.this, Roster.this.connection));
                }
            }
            Roster.this.rosterInitialized = true;
            synchronized (Roster.this) {
                Roster.this.notifyAll();
            }
            Roster.this.fireRosterChangedEvent(arrayList, arrayList2, arrayList3);
        }
    }

    private class RosterPushListener implements PacketListener {
        private RosterPushListener() {
        }

        public void processPacket(Packet packet) throws SmackException.NotConnectedException {
            RosterPacket rosterPacket = (RosterPacket) packet;
            String version = rosterPacket.getVersion();
            String parseBareAddress = StringUtils.parseBareAddress(Roster.this.connection.getUser());
            if (rosterPacket.getFrom() == null || rosterPacket.getFrom().equals(parseBareAddress)) {
                Collection<RosterPacket.Item> rosterItems = rosterPacket.getRosterItems();
                if (rosterItems.size() != 1) {
                    Roster.LOGGER.warning("Ignoring roster push with not exaclty one entry. size=" + rosterItems.size());
                    return;
                }
                ArrayList arrayList = new ArrayList();
                ArrayList arrayList2 = new ArrayList();
                ArrayList arrayList3 = new ArrayList();
                ArrayList arrayList4 = new ArrayList();
                RosterPacket.Item next = rosterItems.iterator().next();
                RosterEntry rosterEntry = new RosterEntry(next.getUser(), next.getName(), next.getItemType(), next.getItemStatus(), Roster.this, Roster.this.connection);
                if (next.getItemType().equals(RosterPacket.ItemType.remove)) {
                    Roster.this.deleteEntry(arrayList3, rosterEntry);
                    if (Roster.this.rosterStore != null) {
                        Roster.this.rosterStore.removeEntry(rosterEntry.getUser(), version);
                    }
                } else if (Roster.hasValidSubscriptionType(next)) {
                    Roster.this.addUpdateEntry(arrayList, arrayList2, arrayList4, next, rosterEntry);
                    if (Roster.this.rosterStore != null) {
                        Roster.this.rosterStore.addEntry(next, version);
                    }
                }
                Roster.this.connection.sendPacket(IQ.createResultIQ(rosterPacket));
                Roster.this.removeEmptyGroups();
                Roster.this.fireRosterChangedEvent(arrayList, arrayList2, arrayList3);
                return;
            }
            Roster.LOGGER.warning("Ignoring roster push with a non matching 'from' ourJid=" + parseBareAddress + " from=" + rosterPacket.getFrom());
        }
    }
}
