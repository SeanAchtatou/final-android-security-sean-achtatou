package org.jivesoftware.smack;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.apache.harmony.javax.security.auth.callback.CallbackHandler;
import org.apache.harmony.javax.security.sasl.SaslException;
import org.apache.qpid.management.common.sasl.Constants;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.sasl.SASLAnonymous;
import org.jivesoftware.smack.sasl.SASLDigestMD5Mechanism;
import org.jivesoftware.smack.sasl.SASLErrorException;
import org.jivesoftware.smack.sasl.SASLExternalMechanism;
import org.jivesoftware.smack.sasl.SASLMechanism;
import org.jivesoftware.smack.sasl.SASLPlainMechanism;

public class SASLAuthentication {
    private static Map<String, Class<? extends SASLMechanism>> implementedMechanisms = new HashMap();
    private static List<String> mechanismsPreferences = new ArrayList();
    private XMPPConnection connection;
    private SASLMechanism currentMechanism = null;
    private SASLMechanism.SASLFailure saslFailure;
    private boolean saslNegotiated;
    private Collection<String> serverMechanisms = new ArrayList();

    static {
        registerSASLMechanism("EXTERNAL", SASLExternalMechanism.class);
        registerSASLMechanism("DIGEST-MD5", SASLDigestMD5Mechanism.class);
        registerSASLMechanism(Constants.MECH_PLAIN, SASLPlainMechanism.class);
        registerSASLMechanism("ANONYMOUS", SASLAnonymous.class);
        supportSASLMechanism("DIGEST-MD5", 0);
        supportSASLMechanism(Constants.MECH_PLAIN, 1);
        supportSASLMechanism("ANONYMOUS", 2);
    }

    public static void registerSASLMechanism(String str, Class<? extends SASLMechanism> cls) {
        implementedMechanisms.put(str, cls);
    }

    public static void unregisterSASLMechanism(String str) {
        implementedMechanisms.remove(str);
        mechanismsPreferences.remove(str);
    }

    public static void supportSASLMechanism(String str) {
        mechanismsPreferences.add(0, str);
    }

    public static void supportSASLMechanism(String str, int i) {
        mechanismsPreferences.add(i, str);
    }

    public static void unsupportSASLMechanism(String str) {
        mechanismsPreferences.remove(str);
    }

    public static List<Class<? extends SASLMechanism>> getRegisterSASLMechanisms() {
        ArrayList arrayList = new ArrayList();
        for (String str : mechanismsPreferences) {
            arrayList.add(implementedMechanisms.get(str));
        }
        return arrayList;
    }

    SASLAuthentication(XMPPConnection xMPPConnection) {
        this.connection = xMPPConnection;
        init();
    }

    public boolean hasAnonymousAuthentication() {
        return this.serverMechanisms.contains("ANONYMOUS");
    }

    public boolean hasNonAnonymousAuthentication() {
        return !this.serverMechanisms.isEmpty() && (this.serverMechanisms.size() != 1 || !hasAnonymousAuthentication());
    }

    public void authenticate(String str, CallbackHandler callbackHandler) throws IOException, SmackException.NoResponseException, XMPPException.XMPPErrorException, SASLErrorException, SmackException.ResourceBindingNotOfferedException, SmackException.NotConnectedException {
        String str2 = null;
        Iterator<String> it = mechanismsPreferences.iterator();
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            String next = it.next();
            if (implementedMechanisms.containsKey(next) && this.serverMechanisms.contains(next)) {
                str2 = next;
                break;
            }
        }
        if (str2 != null) {
            try {
                this.currentMechanism = (SASLMechanism) implementedMechanisms.get(str2).getConstructor(SASLAuthentication.class).newInstance(this);
                synchronized (this) {
                    this.currentMechanism.authenticate(this.connection.getHost(), callbackHandler);
                    try {
                        wait(this.connection.getPacketReplyTimeout());
                    } catch (InterruptedException e) {
                    }
                }
                if (this.saslFailure != null) {
                    throw new SASLErrorException(str2, this.saslFailure);
                } else if (!this.saslNegotiated) {
                    throw new SmackException.NoResponseException();
                }
            } catch (Exception e2) {
                throw new SaslException("Exception when creating the SASLAuthentication instance", e2);
            }
        } else {
            throw new SaslException("SASL Authentication failed. No known authentication mechanisims.");
        }
    }

    public void authenticate(String str, String str2, String str3) throws XMPPException.XMPPErrorException, SASLErrorException, SaslException, IOException, SmackException {
        String str4 = null;
        Iterator<String> it = mechanismsPreferences.iterator();
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            String next = it.next();
            if (implementedMechanisms.containsKey(next) && this.serverMechanisms.contains(next)) {
                str4 = next;
                break;
            }
        }
        if (str4 != null) {
            try {
                this.currentMechanism = (SASLMechanism) implementedMechanisms.get(str4).getConstructor(SASLAuthentication.class).newInstance(this);
                synchronized (this) {
                    this.currentMechanism.authenticate(str, this.connection.getHost(), this.connection.getServiceName(), str2);
                    try {
                        wait(this.connection.getPacketReplyTimeout());
                    } catch (InterruptedException e) {
                    }
                }
                if (this.saslFailure != null) {
                    throw new SASLErrorException(str4, this.saslFailure);
                } else if (!this.saslNegotiated) {
                    throw new SmackException.NoResponseException();
                }
            } catch (Exception e2) {
                throw new SaslException("Exception when creating the SASLAuthentication instance", e2);
            }
        } else {
            throw new SaslException("SASL Authentication failed. No known authentication mechanisims.");
        }
    }

    public void authenticateAnonymously() throws SASLErrorException, SaslException, IOException, SmackException, XMPPException.XMPPErrorException {
        this.currentMechanism = new SASLAnonymous(this);
        synchronized (this) {
            this.currentMechanism.authenticate(null, null, null, "");
            try {
                wait(this.connection.getPacketReplyTimeout());
            } catch (InterruptedException e) {
            }
        }
        if (this.saslFailure != null) {
            throw new SASLErrorException(this.currentMechanism.toString(), this.saslFailure);
        } else if (!this.saslNegotiated) {
            throw new SmackException.NoResponseException();
        }
    }

    public void setAvailableSASLMethods(Collection<String> collection) {
        this.serverMechanisms = collection;
    }

    public boolean isAuthenticated() {
        return this.saslNegotiated;
    }

    public void challengeReceived(String str) throws IOException, SmackException.NotConnectedException {
        this.currentMechanism.challengeReceived(str);
    }

    public void authenticated() {
        this.saslNegotiated = true;
        synchronized (this) {
            notify();
        }
    }

    public void authenticationFailed(SASLMechanism.SASLFailure sASLFailure) {
        this.saslFailure = sASLFailure;
        synchronized (this) {
            notify();
        }
    }

    public void send(Packet packet) throws SmackException.NotConnectedException {
        this.connection.sendPacket(packet);
    }

    /* access modifiers changed from: protected */
    public void init() {
        this.saslNegotiated = false;
        this.saslFailure = null;
    }
}
