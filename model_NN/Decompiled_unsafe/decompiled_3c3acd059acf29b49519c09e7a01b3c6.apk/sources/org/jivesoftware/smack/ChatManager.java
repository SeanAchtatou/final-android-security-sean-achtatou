package org.jivesoftware.smack;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.WeakHashMap;
import java.util.concurrent.CopyOnWriteArraySet;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.filter.AndFilter;
import org.jivesoftware.smack.filter.FromMatchesFilter;
import org.jivesoftware.smack.filter.PacketFilter;
import org.jivesoftware.smack.filter.ThreadFilter;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.util.StringUtils;

public class ChatManager extends Manager {
    private static final Map<XMPPConnection, ChatManager> INSTANCES = new WeakHashMap();
    private static boolean defaultIsNormalInclude = true;
    private static MatchMode defaultMatchMode = MatchMode.BARE_JID;
    private Map<String, Chat> baseJidChats = Collections.synchronizedMap(new HashMap());
    private Set<ChatManagerListener> chatManagerListeners = new CopyOnWriteArraySet();
    private Map<PacketInterceptor, PacketFilter> interceptors = new WeakHashMap();
    private Map<String, Chat> jidChats = Collections.synchronizedMap(new HashMap());
    private MatchMode matchMode = defaultMatchMode;
    /* access modifiers changed from: private */
    public boolean normalIncluded = defaultIsNormalInclude;
    private Map<String, Chat> threadChats = Collections.synchronizedMap(new HashMap());

    public enum MatchMode {
        NONE,
        SUPPLIED_JID,
        BARE_JID
    }

    public static synchronized ChatManager getInstanceFor(XMPPConnection xMPPConnection) {
        ChatManager chatManager;
        synchronized (ChatManager.class) {
            chatManager = INSTANCES.get(xMPPConnection);
            if (chatManager == null) {
                chatManager = new ChatManager(xMPPConnection);
            }
        }
        return chatManager;
    }

    private ChatManager(XMPPConnection xMPPConnection) {
        super(xMPPConnection);
        xMPPConnection.addPacketListener(new PacketListener() {
            public void processPacket(Packet packet) {
                Chat threadChat;
                Message message = (Message) packet;
                if (message.getThread() == null) {
                    threadChat = ChatManager.this.getUserChat(message.getFrom());
                } else {
                    threadChat = ChatManager.this.getThreadChat(message.getThread());
                }
                if (threadChat == null) {
                    threadChat = ChatManager.this.createChat(message);
                }
                if (threadChat != null) {
                    ChatManager.this.deliverMessage(threadChat, message);
                }
            }
        }, new PacketFilter() {
            public boolean accept(Packet packet) {
                if (!(packet instanceof Message)) {
                    return false;
                }
                Message.Type type = ((Message) packet).getType();
                if (type == Message.Type.chat || (ChatManager.this.normalIncluded && type == Message.Type.normal)) {
                    return true;
                }
                return false;
            }
        });
        INSTANCES.put(xMPPConnection, this);
    }

    public boolean isNormalIncluded() {
        return this.normalIncluded;
    }

    public void setNormalIncluded(boolean z) {
        this.normalIncluded = z;
    }

    public MatchMode getMatchMode() {
        return this.matchMode;
    }

    public void setMatchMode(MatchMode matchMode2) {
        this.matchMode = matchMode2;
    }

    public Chat createChat(String str, MessageListener messageListener) {
        return createChat(str, (String) null, messageListener);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.jivesoftware.smack.ChatManager.createChat(java.lang.String, java.lang.String, boolean):org.jivesoftware.smack.Chat
     arg types: [java.lang.String, java.lang.String, int]
     candidates:
      org.jivesoftware.smack.ChatManager.createChat(java.lang.String, java.lang.String, org.jivesoftware.smack.MessageListener):org.jivesoftware.smack.Chat
      org.jivesoftware.smack.ChatManager.createChat(java.lang.String, java.lang.String, boolean):org.jivesoftware.smack.Chat */
    public Chat createChat(String str, String str2, MessageListener messageListener) {
        if (str2 == null) {
            str2 = nextID();
        }
        if (this.threadChats.get(str2) != null) {
            throw new IllegalArgumentException("ThreadID is already used");
        }
        Chat createChat = createChat(str, str2, true);
        createChat.addMessageListener(messageListener);
        return createChat;
    }

    private Chat createChat(String str, String str2, boolean z) {
        Chat chat = new Chat(this, str, str2);
        this.threadChats.put(str2, chat);
        this.jidChats.put(str, chat);
        this.baseJidChats.put(StringUtils.parseBareAddress(str), chat);
        for (ChatManagerListener chatCreated : this.chatManagerListeners) {
            chatCreated.chatCreated(chat, z);
        }
        return chat;
    }

    /* access modifiers changed from: package-private */
    public void closeChat(Chat chat) {
        this.threadChats.remove(chat.getThreadID());
        String participant = chat.getParticipant();
        this.jidChats.remove(participant);
        this.baseJidChats.remove(StringUtils.parseBareAddress(participant));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.jivesoftware.smack.ChatManager.createChat(java.lang.String, java.lang.String, boolean):org.jivesoftware.smack.Chat
     arg types: [java.lang.String, java.lang.String, int]
     candidates:
      org.jivesoftware.smack.ChatManager.createChat(java.lang.String, java.lang.String, org.jivesoftware.smack.MessageListener):org.jivesoftware.smack.Chat
      org.jivesoftware.smack.ChatManager.createChat(java.lang.String, java.lang.String, boolean):org.jivesoftware.smack.Chat */
    /* access modifiers changed from: private */
    public Chat createChat(Message message) {
        String from = message.getFrom();
        if (from == null) {
            return null;
        }
        String thread = message.getThread();
        if (thread == null) {
            thread = nextID();
        }
        return createChat(from, thread, false);
    }

    /* access modifiers changed from: private */
    public Chat getUserChat(String str) {
        if (this.matchMode == MatchMode.NONE || str == null) {
            return null;
        }
        Chat chat = this.jidChats.get(str);
        if (chat == null && this.matchMode == MatchMode.BARE_JID) {
            return this.baseJidChats.get(StringUtils.parseBareAddress(str));
        }
        return chat;
    }

    public Chat getThreadChat(String str) {
        return this.threadChats.get(str);
    }

    public void addChatListener(ChatManagerListener chatManagerListener) {
        this.chatManagerListeners.add(chatManagerListener);
    }

    public void removeChatListener(ChatManagerListener chatManagerListener) {
        this.chatManagerListeners.remove(chatManagerListener);
    }

    public Collection<ChatManagerListener> getChatListeners() {
        return Collections.unmodifiableCollection(this.chatManagerListeners);
    }

    /* access modifiers changed from: private */
    public void deliverMessage(Chat chat, Message message) {
        chat.deliver(message);
    }

    /* access modifiers changed from: package-private */
    public void sendMessage(Chat chat, Message message) throws SmackException.NotConnectedException {
        for (Map.Entry next : this.interceptors.entrySet()) {
            PacketFilter packetFilter = (PacketFilter) next.getValue();
            if (packetFilter != null && packetFilter.accept(message)) {
                ((PacketInterceptor) next.getKey()).interceptPacket(message);
            }
        }
        if (message.getFrom() == null) {
            message.setFrom(connection().getUser());
        }
        connection().sendPacket(message);
    }

    /* access modifiers changed from: package-private */
    public PacketCollector createPacketCollector(Chat chat) {
        return connection().createPacketCollector(new AndFilter(new ThreadFilter(chat.getThreadID()), FromMatchesFilter.create(chat.getParticipant())));
    }

    public void addOutgoingMessageInterceptor(PacketInterceptor packetInterceptor) {
        addOutgoingMessageInterceptor(packetInterceptor, null);
    }

    public void addOutgoingMessageInterceptor(PacketInterceptor packetInterceptor, PacketFilter packetFilter) {
        if (packetInterceptor != null) {
            this.interceptors.put(packetInterceptor, packetFilter);
        }
    }

    private static String nextID() {
        return UUID.randomUUID().toString();
    }

    public static void setDefaultMatchMode(MatchMode matchMode2) {
        defaultMatchMode = matchMode2;
    }

    public static void setDefaultIsNormalIncluded(boolean z) {
        defaultIsNormalInclude = z;
    }
}
