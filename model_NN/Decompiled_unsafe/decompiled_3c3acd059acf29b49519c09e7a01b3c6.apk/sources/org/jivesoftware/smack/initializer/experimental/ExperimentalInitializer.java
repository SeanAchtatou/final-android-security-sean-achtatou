package org.jivesoftware.smack.initializer.experimental;

import org.jivesoftware.smack.initializer.UrlInitializer;

public class ExperimentalInitializer extends UrlInitializer {
    /* access modifiers changed from: protected */
    public String getProvidersUrl() {
        return "classpath:org.jivesoftware.smackx/experimental.providers";
    }

    /* access modifiers changed from: protected */
    public String getConfigUrl() {
        return "classpath:org.jivesoftware.smackx/experimental.xml";
    }
}
