package org.jivesoftware.smack.initializer;

import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jivesoftware.smack.SmackConfiguration;
import org.jivesoftware.smack.provider.ProviderFileLoader;
import org.jivesoftware.smack.provider.ProviderManager;
import org.jivesoftware.smack.util.FileUtils;

public abstract class UrlInitializer extends SmackAndOsgiInitializer {
    private static final Logger LOGGER = Logger.getLogger(UrlInitializer.class.getName());

    public List<Exception> initialize() {
        return initialize(getClass().getClassLoader());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
     arg types: [java.util.logging.Level, java.lang.String, java.lang.Exception]
     candidates:
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
    public List<Exception> initialize(ClassLoader classLoader) {
        LinkedList linkedList = new LinkedList();
        String providersUrl = getProvidersUrl();
        if (providersUrl != null) {
            try {
                InputStream streamForUrl = FileUtils.getStreamForUrl(providersUrl, classLoader);
                if (streamForUrl != null) {
                    LOGGER.log(Level.FINE, "Loading providers for providerUrl [" + providersUrl + "]");
                    ProviderFileLoader providerFileLoader = new ProviderFileLoader(streamForUrl, classLoader);
                    ProviderManager.addLoader(providerFileLoader);
                    linkedList.addAll(providerFileLoader.getLoadingExceptions());
                } else {
                    LOGGER.log(Level.WARNING, "No input stream created for " + providersUrl);
                    linkedList.add(new IOException("No input stream created for " + providersUrl));
                }
            } catch (Exception e) {
                LOGGER.log(Level.SEVERE, "Error trying to load provider file " + providersUrl, (Throwable) e);
                linkedList.add(e);
            }
        }
        String configUrl = getConfigUrl();
        if (configUrl != null) {
            try {
                SmackConfiguration.processConfigFile(FileUtils.getStreamForUrl(configUrl, classLoader), linkedList, classLoader);
            } catch (Exception e2) {
                linkedList.add(e2);
            }
        }
        return linkedList;
    }

    /* access modifiers changed from: protected */
    public String getProvidersUrl() {
        return null;
    }

    /* access modifiers changed from: protected */
    public String getConfigUrl() {
        return null;
    }
}
