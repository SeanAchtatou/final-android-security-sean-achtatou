package org.jivesoftware.smack.initializer;

public abstract class SmackAndOsgiInitializer implements SmackInitializer {
    public final void activate() {
        initialize();
    }
}
