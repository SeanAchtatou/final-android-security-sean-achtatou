package org.jivesoftware.smack.initializer.extensions;

import org.jivesoftware.smack.initializer.UrlInitializer;

public class ExtensionsInitializer extends UrlInitializer {
    /* access modifiers changed from: protected */
    public String getProvidersUrl() {
        return "classpath:org.jivesoftware.smackx/extensions.providers";
    }

    /* access modifiers changed from: protected */
    public String getConfigUrl() {
        return "classpath:org.jivesoftware.smackx/extensions.xml";
    }
}
