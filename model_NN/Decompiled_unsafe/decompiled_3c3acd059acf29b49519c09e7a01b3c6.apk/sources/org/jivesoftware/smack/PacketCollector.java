package org.jivesoftware.smack;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.filter.PacketFilter;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.packet.XMPPError;

public class PacketCollector {
    private static final Logger LOGGER = Logger.getLogger(PacketCollector.class.getName());
    private boolean cancelled;
    private XMPPConnection connection;
    private PacketFilter packetFilter;
    private ArrayBlockingQueue<Packet> resultQueue;

    protected PacketCollector(XMPPConnection xMPPConnection, PacketFilter packetFilter2) {
        this(xMPPConnection, packetFilter2, SmackConfiguration.getPacketCollectorSize());
    }

    protected PacketCollector(XMPPConnection xMPPConnection, PacketFilter packetFilter2, int i) {
        this.cancelled = false;
        this.connection = xMPPConnection;
        this.packetFilter = packetFilter2;
        this.resultQueue = new ArrayBlockingQueue<>(i);
    }

    public void cancel() {
        if (!this.cancelled) {
            this.cancelled = true;
            this.connection.removePacketCollector(this);
        }
    }

    public PacketFilter getPacketFilter() {
        return this.packetFilter;
    }

    public Packet pollResult() {
        return this.resultQueue.poll();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
     arg types: [java.util.logging.Level, java.lang.String, java.lang.InterruptedException]
     candidates:
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
    public Packet nextResultBlockForever() {
        Packet packet = null;
        while (packet == null) {
            try {
                packet = this.resultQueue.take();
            } catch (InterruptedException e) {
                LOGGER.log(Level.FINE, "nextResultBlockForever was interrupted", (Throwable) e);
            }
        }
        return packet;
    }

    public Packet nextResult() {
        return nextResult(this.connection.getPacketReplyTimeout());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
     arg types: [java.util.logging.Level, java.lang.String, java.lang.InterruptedException]
     candidates:
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
    public Packet nextResult(long j) {
        Packet packet = null;
        long currentTimeMillis = System.currentTimeMillis();
        long j2 = j;
        while (packet == null && j2 > 0) {
            try {
                Packet poll = this.resultQueue.poll(j2, TimeUnit.MILLISECONDS);
                try {
                    j2 = j - (System.currentTimeMillis() - currentTimeMillis);
                    packet = poll;
                } catch (InterruptedException e) {
                    InterruptedException interruptedException = e;
                    packet = poll;
                    e = interruptedException;
                    LOGGER.log(Level.FINE, "nextResult was interrupted", (Throwable) e);
                }
            } catch (InterruptedException e2) {
                e = e2;
                LOGGER.log(Level.FINE, "nextResult was interrupted", (Throwable) e);
            }
        }
        return packet;
    }

    public Packet nextResultOrThrow() throws SmackException.NoResponseException, XMPPException.XMPPErrorException {
        return nextResultOrThrow(this.connection.getPacketReplyTimeout());
    }

    public Packet nextResultOrThrow(long j) throws SmackException.NoResponseException, XMPPException.XMPPErrorException {
        Packet nextResult = nextResult(j);
        cancel();
        if (nextResult == null) {
            throw new SmackException.NoResponseException();
        }
        XMPPError error = nextResult.getError();
        if (error == null) {
            return nextResult;
        }
        throw new XMPPException.XMPPErrorException(error);
    }

    /* access modifiers changed from: protected */
    public void processPacket(Packet packet) {
        if (packet != null) {
            if (this.packetFilter == null || this.packetFilter.accept(packet)) {
                while (!this.resultQueue.offer(packet)) {
                    this.resultQueue.poll();
                }
            }
        }
    }
}
