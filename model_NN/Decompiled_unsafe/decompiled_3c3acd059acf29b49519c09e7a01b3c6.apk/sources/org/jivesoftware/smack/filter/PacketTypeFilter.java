package org.jivesoftware.smack.filter;

import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.packet.Presence;

public class PacketTypeFilter implements PacketFilter {
    public static final PacketTypeFilter MESSAGE = new PacketTypeFilter(Message.class);
    public static final PacketTypeFilter PRESENCE = new PacketTypeFilter(Presence.class);
    Class<? extends Packet> packetType;

    public PacketTypeFilter(Class<? extends Packet> cls) {
        if (!Packet.class.isAssignableFrom(cls)) {
            throw new IllegalArgumentException("Packet type must be a sub-class of Packet.");
        }
        this.packetType = cls;
    }

    public boolean accept(Packet packet) {
        return this.packetType.isInstance(packet);
    }

    public String toString() {
        return "PacketTypeFilter: " + this.packetType.getName();
    }
}
