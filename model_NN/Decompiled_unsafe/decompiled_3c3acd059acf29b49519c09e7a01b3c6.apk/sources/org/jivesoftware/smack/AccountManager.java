package org.jivesoftware.smack;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.WeakHashMap;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.packet.Registration;
import org.jivesoftware.smack.util.StringUtils;
import org.jivesoftware.smackx.disco.packet.DiscoverItems;

public class AccountManager extends Manager {
    private static final Map<XMPPConnection, AccountManager> INSTANCES = new WeakHashMap();
    private boolean accountCreationSupported = false;
    private Registration info = null;

    public static synchronized AccountManager getInstance(XMPPConnection xMPPConnection) {
        AccountManager accountManager;
        synchronized (AccountManager.class) {
            accountManager = INSTANCES.get(xMPPConnection);
            if (accountManager == null) {
                accountManager = new AccountManager(xMPPConnection);
            }
        }
        return accountManager;
    }

    private AccountManager(XMPPConnection xMPPConnection) {
        super(xMPPConnection);
        INSTANCES.put(xMPPConnection, this);
    }

    /* access modifiers changed from: package-private */
    public void setSupportsAccountCreation(boolean z) {
        this.accountCreationSupported = z;
    }

    public boolean supportsAccountCreation() throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        boolean z = true;
        if (this.accountCreationSupported) {
            return true;
        }
        if (this.info == null) {
            getRegistrationInfo();
            if (this.info.getType() == IQ.Type.ERROR) {
                z = false;
            }
            this.accountCreationSupported = z;
        }
        return this.accountCreationSupported;
    }

    public Collection<String> getAccountAttributes() throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        if (this.info == null) {
            getRegistrationInfo();
        }
        Map<String, String> attributes = this.info.getAttributes();
        if (attributes != null) {
            return Collections.unmodifiableSet(attributes.keySet());
        }
        return Collections.emptySet();
    }

    public String getAccountAttribute(String str) throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        if (this.info == null) {
            getRegistrationInfo();
        }
        return this.info.getAttributes().get(str);
    }

    public String getAccountInstructions() throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        if (this.info == null) {
            getRegistrationInfo();
        }
        return this.info.getInstructions();
    }

    public void createAccount(String str, String str2) throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        HashMap hashMap = new HashMap();
        for (String put : getAccountAttributes()) {
            hashMap.put(put, "");
        }
        createAccount(str, str2, hashMap);
    }

    public void createAccount(String str, String str2, Map<String, String> map) throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        Registration registration = new Registration();
        registration.setType(IQ.Type.SET);
        registration.setTo(connection().getServiceName());
        map.put("username", str);
        map.put("password", str2);
        registration.setAttributes(map);
        connection().createPacketCollectorAndSend(registration).nextResultOrThrow();
    }

    public void changePassword(String str) throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        Registration registration = new Registration();
        registration.setType(IQ.Type.SET);
        registration.setTo(connection().getServiceName());
        HashMap hashMap = new HashMap();
        hashMap.put("username", StringUtils.parseName(connection().getUser()));
        hashMap.put("password", str);
        registration.setAttributes(hashMap);
        connection().createPacketCollectorAndSend(registration).nextResultOrThrow();
    }

    public void deleteAccount() throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        Registration registration = new Registration();
        registration.setType(IQ.Type.SET);
        registration.setTo(connection().getServiceName());
        HashMap hashMap = new HashMap();
        hashMap.put(DiscoverItems.Item.REMOVE_ACTION, "");
        registration.setAttributes(hashMap);
        connection().createPacketCollectorAndSend(registration).nextResultOrThrow();
    }

    private synchronized void getRegistrationInfo() throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        Registration registration = new Registration();
        registration.setTo(connection().getServiceName());
        this.info = (Registration) connection().createPacketCollectorAndSend(registration).nextResultOrThrow();
    }
}
