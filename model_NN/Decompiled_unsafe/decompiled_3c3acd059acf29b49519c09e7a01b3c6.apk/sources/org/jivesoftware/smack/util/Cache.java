package org.jivesoftware.smack.util;

import java.util.AbstractCollection;
import java.util.AbstractSet;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

public class Cache<K, V> implements Map<K, V> {
    private static final Logger LOGGER = Logger.getLogger(Cache.class.getName());
    protected LinkedList ageList;
    protected long cacheHits;
    protected long cacheMisses = 0;
    protected LinkedList lastAccessedList;
    protected Map<K, CacheObject<V>> map;
    protected int maxCacheSize;
    protected long maxLifetime;

    public Cache(int i, long j) {
        if (i == 0) {
            throw new IllegalArgumentException("Max cache size cannot be 0.");
        }
        this.maxCacheSize = i;
        this.maxLifetime = j;
        this.map = new HashMap(103);
        this.lastAccessedList = new LinkedList();
        this.ageList = new LinkedList();
    }

    public synchronized V put(K k, V v) {
        V v2;
        v2 = null;
        if (this.map.containsKey(k)) {
            v2 = remove(k, true);
        }
        CacheObject cacheObject = new CacheObject(v);
        this.map.put(k, cacheObject);
        cacheObject.lastAccessedListNode = this.lastAccessedList.addFirst((Object) k);
        LinkedListNode addFirst = this.ageList.addFirst((Object) k);
        addFirst.timestamp = System.currentTimeMillis();
        cacheObject.ageListNode = addFirst;
        cullCache();
        return v2;
    }

    public synchronized V get(Object obj) {
        V v;
        deleteExpiredEntries();
        CacheObject cacheObject = this.map.get(obj);
        if (cacheObject == null) {
            this.cacheMisses++;
            v = null;
        } else {
            cacheObject.lastAccessedListNode.remove();
            this.lastAccessedList.addFirst(cacheObject.lastAccessedListNode);
            this.cacheHits++;
            cacheObject.readCount++;
            v = cacheObject.object;
        }
        return v;
    }

    public synchronized V remove(Object obj) {
        return remove(obj, false);
    }

    public synchronized V remove(Object obj, boolean z) {
        V v;
        CacheObject remove = this.map.remove(obj);
        if (remove == null) {
            v = null;
        } else {
            remove.lastAccessedListNode.remove();
            remove.ageListNode.remove();
            remove.ageListNode = null;
            remove.lastAccessedListNode = null;
            v = remove.object;
        }
        return v;
    }

    public synchronized void clear() {
        for (Object remove : this.map.keySet().toArray()) {
            remove(remove);
        }
        this.map.clear();
        this.lastAccessedList.clear();
        this.ageList.clear();
        this.cacheHits = 0;
        this.cacheMisses = 0;
    }

    public synchronized int size() {
        deleteExpiredEntries();
        return this.map.size();
    }

    public synchronized boolean isEmpty() {
        deleteExpiredEntries();
        return this.map.isEmpty();
    }

    public synchronized Collection<V> values() {
        deleteExpiredEntries();
        return Collections.unmodifiableCollection(new AbstractCollection<V>() {
            Collection<CacheObject<V>> values = Cache.this.map.values();

            public Iterator<V> iterator() {
                return new Iterator<V>() {
                    Iterator<CacheObject<V>> it = AnonymousClass1.this.values.iterator();

                    public boolean hasNext() {
                        return this.it.hasNext();
                    }

                    public V next() {
                        return this.it.next().object;
                    }

                    public void remove() {
                        this.it.remove();
                    }
                };
            }

            public int size() {
                return this.values.size();
            }
        });
    }

    public synchronized boolean containsKey(Object obj) {
        deleteExpiredEntries();
        return this.map.containsKey(obj);
    }

    public void putAll(Map<? extends K, ? extends V> map2) {
        for (Map.Entry next : map2.entrySet()) {
            V value = next.getValue();
            if (value instanceof CacheObject) {
                value = ((CacheObject) value).object;
            }
            put(next.getKey(), value);
        }
    }

    public synchronized boolean containsValue(Object obj) {
        deleteExpiredEntries();
        return this.map.containsValue(new CacheObject(obj));
    }

    public synchronized Set<Map.Entry<K, V>> entrySet() {
        deleteExpiredEntries();
        return new AbstractSet<Map.Entry<K, V>>() {
            /* access modifiers changed from: private */
            public final Set<Map.Entry<K, CacheObject<V>>> set = Cache.this.map.entrySet();

            public Iterator<Map.Entry<K, V>> iterator() {
                return new Iterator<Map.Entry<K, V>>() {
                    private final Iterator<Map.Entry<K, CacheObject<V>>> it = AnonymousClass2.this.set.iterator();

                    public boolean hasNext() {
                        return this.it.hasNext();
                    }

                    public Map.Entry<K, V> next() {
                        Map.Entry next = this.it.next();
                        return new AbstractMapEntry<K, V>(next.getKey(), ((CacheObject) next.getValue()).object) {
                            public V setValue(V v) {
                                throw new UnsupportedOperationException("Cannot set");
                            }
                        };
                    }

                    public void remove() {
                        this.it.remove();
                    }
                };
            }

            public int size() {
                return this.set.size();
            }
        };
    }

    public synchronized Set<K> keySet() {
        deleteExpiredEntries();
        return Collections.unmodifiableSet(this.map.keySet());
    }

    public long getCacheHits() {
        return this.cacheHits;
    }

    public long getCacheMisses() {
        return this.cacheMisses;
    }

    public int getMaxCacheSize() {
        return this.maxCacheSize;
    }

    public synchronized void setMaxCacheSize(int i) {
        this.maxCacheSize = i;
        cullCache();
    }

    public long getMaxLifetime() {
        return this.maxLifetime;
    }

    public void setMaxLifetime(long j) {
        this.maxLifetime = j;
    }

    /* access modifiers changed from: protected */
    public synchronized void deleteExpiredEntries() {
        if (this.maxLifetime > 0) {
            LinkedListNode last = this.ageList.getLast();
            if (last != null) {
                long currentTimeMillis = System.currentTimeMillis() - this.maxLifetime;
                while (currentTimeMillis > last.timestamp) {
                    if (remove(last.object, true) == null) {
                        LOGGER.warning("Error attempting to remove(" + last.object.toString() + ") - cacheObject not found in cache!");
                        last.remove();
                    }
                    last = this.ageList.getLast();
                    if (last == null) {
                        break;
                    }
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public synchronized void cullCache() {
        if (this.maxCacheSize >= 0) {
            if (this.map.size() > this.maxCacheSize) {
                deleteExpiredEntries();
                int i = (int) (((double) this.maxCacheSize) * 0.9d);
                for (int size = this.map.size(); size > i; size--) {
                    if (remove(this.lastAccessedList.getLast().object, true) == null) {
                        LOGGER.warning("Error attempting to cullCache with remove(" + this.lastAccessedList.getLast().object.toString() + ") - cacheObject not found in cache!");
                        this.lastAccessedList.getLast().remove();
                    }
                }
            }
        }
    }

    private static class CacheObject<V> {
        public LinkedListNode ageListNode;
        public LinkedListNode lastAccessedListNode;
        public V object;
        public int readCount = 0;

        public CacheObject(V v) {
            this.object = v;
        }

        /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
            jadx.core.utils.exceptions.JadxRuntimeException: Not class type: V
            	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
            	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
            	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
            	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
            	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
            	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
            */
        public boolean equals(java.lang.Object r3) {
            /*
                r2 = this;
                if (r2 != r3) goto L_0x0004
                r0 = 1
            L_0x0003:
                return r0
            L_0x0004:
                boolean r0 = r3 instanceof org.jivesoftware.smack.util.Cache.CacheObject
                if (r0 != 0) goto L_0x000a
                r0 = 0
                goto L_0x0003
            L_0x000a:
                org.jivesoftware.smack.util.Cache$CacheObject r3 = (org.jivesoftware.smack.util.Cache.CacheObject) r3
                V r0 = r2.object
                V r1 = r3.object
                boolean r0 = r0.equals(r1)
                goto L_0x0003
            */
            throw new UnsupportedOperationException("Method not decompiled: org.jivesoftware.smack.util.Cache.CacheObject.equals(java.lang.Object):boolean");
        }

        public int hashCode() {
            return this.object.hashCode();
        }
    }

    private static class LinkedList {
        private LinkedListNode head = new LinkedListNode("head", null, null);

        public LinkedList() {
            LinkedListNode linkedListNode = this.head;
            LinkedListNode linkedListNode2 = this.head;
            LinkedListNode linkedListNode3 = this.head;
            linkedListNode2.previous = linkedListNode3;
            linkedListNode.next = linkedListNode3;
        }

        public LinkedListNode getFirst() {
            LinkedListNode linkedListNode = this.head.next;
            if (linkedListNode == this.head) {
                return null;
            }
            return linkedListNode;
        }

        public LinkedListNode getLast() {
            LinkedListNode linkedListNode = this.head.previous;
            if (linkedListNode == this.head) {
                return null;
            }
            return linkedListNode;
        }

        public LinkedListNode addFirst(LinkedListNode linkedListNode) {
            linkedListNode.next = this.head.next;
            linkedListNode.previous = this.head;
            linkedListNode.previous.next = linkedListNode;
            linkedListNode.next.previous = linkedListNode;
            return linkedListNode;
        }

        public LinkedListNode addFirst(Object obj) {
            LinkedListNode linkedListNode = new LinkedListNode(obj, this.head.next, this.head);
            linkedListNode.previous.next = linkedListNode;
            linkedListNode.next.previous = linkedListNode;
            return linkedListNode;
        }

        public LinkedListNode addLast(Object obj) {
            LinkedListNode linkedListNode = new LinkedListNode(obj, this.head, this.head.previous);
            linkedListNode.previous.next = linkedListNode;
            linkedListNode.next.previous = linkedListNode;
            return linkedListNode;
        }

        public void clear() {
            LinkedListNode last = getLast();
            while (last != null) {
                last.remove();
                last = getLast();
            }
            LinkedListNode linkedListNode = this.head;
            LinkedListNode linkedListNode2 = this.head;
            LinkedListNode linkedListNode3 = this.head;
            linkedListNode2.previous = linkedListNode3;
            linkedListNode.next = linkedListNode3;
        }

        public String toString() {
            StringBuilder sb = new StringBuilder();
            for (LinkedListNode linkedListNode = this.head.next; linkedListNode != this.head; linkedListNode = linkedListNode.next) {
                sb.append(linkedListNode.toString()).append(", ");
            }
            return sb.toString();
        }
    }

    private static class LinkedListNode {
        public LinkedListNode next;
        public Object object;
        public LinkedListNode previous;
        public long timestamp;

        public LinkedListNode(Object obj, LinkedListNode linkedListNode, LinkedListNode linkedListNode2) {
            this.object = obj;
            this.next = linkedListNode;
            this.previous = linkedListNode2;
        }

        public void remove() {
            this.previous.next = this.next;
            this.next.previous = this.previous;
        }

        public String toString() {
            return this.object.toString();
        }
    }

    static class AbstractMapEntry<K, V> implements Map.Entry<K, V> {
        final K key;
        V value;

        AbstractMapEntry(K k, V v) {
            this.key = k;
            this.value = v;
        }

        public K getKey() {
            return this.key;
        }

        public V getValue() {
            return this.value;
        }

        public V setValue(V v) {
            V v2 = this.value;
            this.value = v;
            return v2;
        }

        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof Map.Entry)) {
                return false;
            }
            Map.Entry entry = (Map.Entry) obj;
            if (getKey() != null ? getKey().equals(entry.getKey()) : entry.getKey() == null) {
                if (getValue() == null) {
                    if (entry.getValue() == null) {
                        return true;
                    }
                } else if (getValue().equals(entry.getValue())) {
                    return true;
                }
            }
            return false;
        }

        public int hashCode() {
            int i = 0;
            int hashCode = getKey() == null ? 0 : getKey().hashCode();
            if (getValue() != null) {
                i = getValue().hashCode();
            }
            return hashCode ^ i;
        }
    }
}
