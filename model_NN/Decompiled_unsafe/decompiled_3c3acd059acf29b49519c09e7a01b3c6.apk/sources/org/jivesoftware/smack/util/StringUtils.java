package org.jivesoftware.smack.util;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

public class StringUtils {
    public static final String AMP_ENCODE = "&amp;";
    public static final String APOS_ENCODE = "&apos;";
    public static final String GT_ENCODE = "&gt;";
    private static final Logger LOGGER = Logger.getLogger(StringUtils.class.getName());
    public static final String LT_ENCODE = "&lt;";
    public static final String QUOTE_ENCODE = "&quot;";
    private static MessageDigest digest = null;
    private static char[] numbersAndLetters = "0123456789abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();
    private static Random randGen = new Random();

    public static String parseName(String str) {
        if (str == null) {
            return null;
        }
        int lastIndexOf = str.lastIndexOf("@");
        if (lastIndexOf <= 0) {
            return "";
        }
        return str.substring(0, lastIndexOf);
    }

    public static String parseServer(String str) {
        if (str == null) {
            return null;
        }
        int lastIndexOf = str.lastIndexOf("@");
        if (lastIndexOf + 1 > str.length()) {
            return "";
        }
        int indexOf = str.indexOf("/");
        if (indexOf <= 0 || indexOf <= lastIndexOf) {
            return str.substring(lastIndexOf + 1);
        }
        return str.substring(lastIndexOf + 1, indexOf);
    }

    public static String parseResource(String str) {
        if (str == null) {
            return null;
        }
        int indexOf = str.indexOf("/");
        if (indexOf + 1 > str.length() || indexOf < 0) {
            return "";
        }
        return str.substring(indexOf + 1);
    }

    public static String parseBareAddress(String str) {
        if (str == null) {
            return null;
        }
        int indexOf = str.indexOf("/");
        if (indexOf < 0) {
            return str;
        }
        if (indexOf == 0) {
            return "";
        }
        return str.substring(0, indexOf);
    }

    public static boolean isFullJID(String str) {
        if (parseName(str).length() <= 0 || parseServer(str).length() <= 0 || parseResource(str).length() <= 0) {
            return false;
        }
        return true;
    }

    public static String escapeNode(String str) {
        if (str == null) {
            return null;
        }
        StringBuilder sb = new StringBuilder(str.length() + 8);
        int length = str.length();
        for (int i = 0; i < length; i++) {
            char charAt = str.charAt(i);
            switch (charAt) {
                case '\"':
                    sb.append("\\22");
                    break;
                case '&':
                    sb.append("\\26");
                    break;
                case '\'':
                    sb.append("\\27");
                    break;
                case '/':
                    sb.append("\\2f");
                    break;
                case ':':
                    sb.append("\\3a");
                    break;
                case '<':
                    sb.append("\\3c");
                    break;
                case '>':
                    sb.append("\\3e");
                    break;
                case '@':
                    sb.append("\\40");
                    break;
                case '\\':
                    sb.append("\\5c");
                    break;
                default:
                    if (!Character.isWhitespace(charAt)) {
                        sb.append(charAt);
                        break;
                    } else {
                        sb.append("\\20");
                        break;
                    }
            }
        }
        return sb.toString();
    }

    public static String unescapeNode(String str) {
        if (str == null) {
            return null;
        }
        char[] charArray = str.toCharArray();
        StringBuilder sb = new StringBuilder(charArray.length);
        int i = 0;
        int length = charArray.length;
        while (i < length) {
            char charAt = str.charAt(i);
            if (charAt == '\\' && i + 2 < length) {
                char c = charArray[i + 1];
                char c2 = charArray[i + 2];
                if (c == '2') {
                    switch (c2) {
                        case '0':
                            sb.append(' ');
                            i += 2;
                            break;
                        case '2':
                            sb.append('\"');
                            i += 2;
                            break;
                        case '6':
                            sb.append('&');
                            i += 2;
                            break;
                        case '7':
                            sb.append('\'');
                            i += 2;
                            break;
                        case 'f':
                            sb.append('/');
                            i += 2;
                            break;
                    }
                    i++;
                } else if (c == '3') {
                    switch (c2) {
                        case 'a':
                            sb.append(':');
                            i += 2;
                            break;
                        case 'c':
                            sb.append('<');
                            i += 2;
                            break;
                        case 'e':
                            sb.append('>');
                            i += 2;
                            break;
                    }
                    i++;
                } else if (c == '4') {
                    if (c2 == '0') {
                        sb.append("@");
                        i += 2;
                        i++;
                    }
                } else if (c == '5' && c2 == 'c') {
                    sb.append("\\");
                    i += 2;
                    i++;
                }
            }
            sb.append(charAt);
            i++;
        }
        return sb.toString();
    }

    public static CharSequence escapeForXML(String str) {
        String str2;
        if (str == null) {
            return null;
        }
        char[] charArray = str.toCharArray();
        int length = charArray.length;
        StringBuilder sb = new StringBuilder((int) (((double) length) * 1.3d));
        int i = 0;
        int i2 = 0;
        while (i < length) {
            switch (charArray[i]) {
                case '\"':
                    str2 = QUOTE_ENCODE;
                    break;
                case '&':
                    str2 = AMP_ENCODE;
                    break;
                case '\'':
                    str2 = APOS_ENCODE;
                    break;
                case '<':
                    str2 = LT_ENCODE;
                    break;
                case '>':
                    str2 = GT_ENCODE;
                    break;
                default:
                    str2 = null;
                    break;
            }
            if (str2 != null) {
                if (i > i2) {
                    sb.append(charArray, i2, i - i2);
                }
                sb.append((CharSequence) str2);
                int i3 = i + 1;
                i = i3;
                i2 = i3;
            } else {
                i++;
            }
        }
        if (i2 == 0) {
            return str;
        }
        if (i > i2) {
            sb.append(charArray, i2, i - i2);
        }
        return sb;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
     arg types: [java.util.logging.Level, java.lang.String, java.security.NoSuchAlgorithmException]
     candidates:
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
     arg types: [java.util.logging.Level, java.lang.String, java.io.UnsupportedEncodingException]
     candidates:
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
    public static synchronized String hash(String str) {
        String encodeHex;
        synchronized (StringUtils.class) {
            if (digest == null) {
                try {
                    digest = MessageDigest.getInstance("SHA-1");
                } catch (NoSuchAlgorithmException e) {
                    LOGGER.log(Level.SEVERE, "Failed to load the SHA-1 MessageDigest. Smack will be unable to function normally.", (Throwable) e);
                }
            }
            try {
                digest.update(str.getBytes("UTF-8"));
            } catch (UnsupportedEncodingException e2) {
                LOGGER.log(Level.SEVERE, "Error computing hash", (Throwable) e2);
            }
            encodeHex = encodeHex(digest.digest());
        }
        return encodeHex;
    }

    public static String encodeHex(byte[] bArr) {
        StringBuilder sb = new StringBuilder(bArr.length * 2);
        for (byte b : bArr) {
            if ((b & 255) < 16) {
                sb.append("0");
            }
            sb.append(Integer.toString(b & 255, 16));
        }
        return sb.toString();
    }

    public static String encodeBase64(String str) {
        try {
            return encodeBase64(str.getBytes("ISO-8859-1"));
        } catch (UnsupportedEncodingException e) {
            throw new IllegalStateException(e);
        }
    }

    public static String encodeBase64(byte[] bArr) {
        return encodeBase64(bArr, false);
    }

    public static String encodeBase64(byte[] bArr, boolean z) {
        return encodeBase64(bArr, 0, bArr.length, z);
    }

    public static String encodeBase64(byte[] bArr, int i, int i2, boolean z) {
        return Base64.encodeBytes(bArr, i, i2, z ? 0 : 8);
    }

    public static byte[] decodeBase64(String str) {
        byte[] bytes;
        try {
            bytes = str.getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            bytes = str.getBytes();
        }
        return Base64.decode(bytes, 0, bytes.length, 0);
    }

    public static String randomString(int i) {
        if (i < 1) {
            return null;
        }
        char[] cArr = new char[i];
        for (int i2 = 0; i2 < cArr.length; i2++) {
            cArr[i2] = numbersAndLetters[randGen.nextInt(71)];
        }
        return new String(cArr);
    }

    public static boolean isNotEmpty(CharSequence charSequence) {
        return !isNullOrEmpty(charSequence);
    }

    public static boolean isNullOrEmpty(CharSequence charSequence) {
        return charSequence == null || isEmpty(charSequence);
    }

    public static boolean isEmpty(CharSequence charSequence) {
        return charSequence.length() == 0;
    }

    public static boolean nullSafeCharSequenceEquals(CharSequence charSequence, CharSequence charSequence2) {
        return nullSafeCharSequenceComperator(charSequence, charSequence2) == 0;
    }

    public static int nullSafeCharSequenceComperator(CharSequence charSequence, CharSequence charSequence2) {
        if ((charSequence2 == null) ^ (charSequence == null)) {
            if (charSequence == null) {
                return -1;
            }
            return 1;
        } else if (charSequence == null && charSequence2 == null) {
            return 0;
        } else {
            return charSequence.toString().compareTo(charSequence2.toString());
        }
    }
}
