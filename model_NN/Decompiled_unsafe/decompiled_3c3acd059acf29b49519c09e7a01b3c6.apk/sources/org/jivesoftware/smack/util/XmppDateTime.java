package org.jivesoftware.smack.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.regex.Pattern;

public class XmppDateTime {
    private static final List<PatternCouplings> couplings = new ArrayList();
    private static final DateFormatType dateFormatter = DateFormatType.XEP_0082_DATE_PROFILE;
    private static final Pattern datePattern = Pattern.compile("^\\d+-\\d+-\\d+$");
    private static final DateFormatType dateTimeFormatter = DateFormatType.XEP_0082_DATETIME_MILLIS_PROFILE;
    private static final DateFormatType dateTimeNoMillisFormatter = DateFormatType.XEP_0082_DATETIME_PROFILE;
    private static final Pattern dateTimeNoMillisPattern = Pattern.compile("^\\d+(-\\d+){2}+T(\\d+:){2}\\d+(Z|([+-](\\d+:\\d+)))?$");
    private static final Pattern dateTimePattern = Pattern.compile("^\\d+(-\\d+){2}+T(\\d+:){2}\\d+.\\d+(Z|([+-](\\d+:\\d+)))?$");
    private static final DateFormatType timeFormatter = DateFormatType.XEP_0082_TIME_MILLIS_ZONE_PROFILE;
    private static final DateFormatType timeNoMillisFormatter = DateFormatType.XEP_0082_TIME_ZONE_PROFILE;
    private static final DateFormatType timeNoMillisNoZoneFormatter = DateFormatType.XEP_0082_TIME_PROFILE;
    private static final Pattern timeNoMillisNoZonePattern = Pattern.compile("^(\\d+:){2}\\d+$");
    private static final Pattern timeNoMillisPattern = Pattern.compile("^(\\d+:){2}\\d+(Z|([+-](\\d+:\\d+)))$");
    private static final DateFormatType timeNoZoneFormatter = DateFormatType.XEP_0082_TIME_MILLIS_PROFILE;
    private static final Pattern timeNoZonePattern = Pattern.compile("^(\\d+:){2}\\d+.\\d+$");
    private static final Pattern timePattern = Pattern.compile("^(\\d+:){2}\\d+.\\d+(Z|([+-](\\d+:\\d+)))$");
    private static final DateFormat xep0091Date6DigitFormatter = new SimpleDateFormat("yyyyMd'T'HH:mm:ss");
    private static final DateFormat xep0091Date7Digit1MonthFormatter = new SimpleDateFormat("yyyyMdd'T'HH:mm:ss");
    private static final DateFormat xep0091Date7Digit2MonthFormatter = new SimpleDateFormat("yyyyMMd'T'HH:mm:ss");
    private static final DateFormat xep0091Formatter = new SimpleDateFormat("yyyyMMdd'T'HH:mm:ss");
    private static final Pattern xep0091Pattern = Pattern.compile("^\\d+T\\d+:\\d+:\\d+$");

    static {
        TimeZone timeZone = TimeZone.getTimeZone("UTC");
        xep0091Formatter.setTimeZone(timeZone);
        xep0091Date6DigitFormatter.setTimeZone(timeZone);
        xep0091Date7Digit1MonthFormatter.setTimeZone(timeZone);
        xep0091Date7Digit1MonthFormatter.setLenient(false);
        xep0091Date7Digit2MonthFormatter.setTimeZone(timeZone);
        xep0091Date7Digit2MonthFormatter.setLenient(false);
        couplings.add(new PatternCouplings(datePattern, dateFormatter));
        couplings.add(new PatternCouplings(dateTimePattern, dateTimeFormatter));
        couplings.add(new PatternCouplings(dateTimeNoMillisPattern, dateTimeNoMillisFormatter));
        couplings.add(new PatternCouplings(timePattern, timeFormatter));
        couplings.add(new PatternCouplings(timeNoZonePattern, timeNoZoneFormatter));
        couplings.add(new PatternCouplings(timeNoMillisPattern, timeNoMillisFormatter));
        couplings.add(new PatternCouplings(timeNoMillisNoZonePattern, timeNoMillisNoZoneFormatter));
    }

    public enum DateFormatType {
        XEP_0082_DATE_PROFILE("yyyy-MM-dd"),
        XEP_0082_DATETIME_PROFILE("yyyy-MM-dd'T'HH:mm:ssZ"),
        XEP_0082_DATETIME_MILLIS_PROFILE("yyyy-MM-dd'T'HH:mm:ss.SSSZ"),
        XEP_0082_TIME_PROFILE("hh:mm:ss"),
        XEP_0082_TIME_ZONE_PROFILE("hh:mm:ssZ"),
        XEP_0082_TIME_MILLIS_PROFILE("hh:mm:ss.SSS"),
        XEP_0082_TIME_MILLIS_ZONE_PROFILE("hh:mm:ss.SSSZ"),
        XEP_0091_DATETIME("yyyyMMdd'T'HH:mm:ss");
        
        private final boolean CONVERT_TIMEZONE;
        private final DateFormat FORMATTER = new SimpleDateFormat(this.FORMAT_STRING);
        private final String FORMAT_STRING;

        private DateFormatType(String str) {
            this.FORMAT_STRING = str;
            this.FORMATTER.setTimeZone(TimeZone.getTimeZone("UTC"));
            this.CONVERT_TIMEZONE = str.charAt(str.length() + -1) == 'Z';
        }

        public String format(Date date) {
            String format;
            synchronized (this.FORMATTER) {
                format = this.FORMATTER.format(date);
            }
            if (this.CONVERT_TIMEZONE) {
                return XmppDateTime.convertRfc822TimezoneToXep82(format);
            }
            return format;
        }

        public Date parse(String str) throws ParseException {
            Date parse;
            if (this.CONVERT_TIMEZONE) {
                str = XmppDateTime.convertXep82TimezoneToRfc822(str);
            }
            synchronized (this.FORMATTER) {
                parse = this.FORMATTER.parse(str);
            }
            return parse;
        }
    }

    public static Date parseXEP0082Date(String str) throws ParseException {
        return parseDate(str);
    }

    public static Date parseDate(String str) throws ParseException {
        Date parse;
        Date parse2;
        if (xep0091Pattern.matcher(str).matches()) {
            int length = str.split("T")[0].length();
            if (length < 8) {
                Date handleDateWithMissingLeadingZeros = handleDateWithMissingLeadingZeros(str, length);
                if (handleDateWithMissingLeadingZeros != null) {
                    return handleDateWithMissingLeadingZeros;
                }
            } else {
                synchronized (xep0091Formatter) {
                    parse2 = xep0091Formatter.parse(str);
                }
                return parse2;
            }
        } else {
            for (PatternCouplings next : couplings) {
                if (next.pattern.matcher(str).matches()) {
                    return next.formatter.parse(str);
                }
            }
        }
        synchronized (dateTimeNoMillisFormatter) {
            parse = dateTimeNoMillisFormatter.parse(str);
        }
        return parse;
    }

    public static String formatXEP0082Date(Date date) {
        String format;
        synchronized (dateTimeFormatter) {
            format = dateTimeFormatter.format(date);
        }
        return format;
    }

    public static String convertXep82TimezoneToRfc822(String str) {
        if (str.charAt(str.length() - 1) == 'Z') {
            return str.replace("Z", "+0000");
        }
        return str.replaceAll("([\\+\\-]\\d\\d):(\\d\\d)", "$1$2");
    }

    public static String convertRfc822TimezoneToXep82(String str) {
        int length = str.length();
        return (str.substring(0, length - 2) + ':') + str.substring(length - 2, length);
    }

    public static String asString(TimeZone timeZone) {
        int rawOffset = timeZone.getRawOffset();
        int i = rawOffset / 3600000;
        return String.format("%+d:%02d", Integer.valueOf(i), Integer.valueOf(Math.abs((rawOffset / 60000) - (i * 60))));
    }

    private static Date handleDateWithMissingLeadingZeros(String str, int i) throws ParseException {
        Date parse;
        if (i == 6) {
            synchronized (xep0091Date6DigitFormatter) {
                parse = xep0091Date6DigitFormatter.parse(str);
            }
            return parse;
        }
        Calendar instance = Calendar.getInstance();
        List<Calendar> filterDatesBefore = filterDatesBefore(instance, parseXEP91Date(str, xep0091Date7Digit1MonthFormatter), parseXEP91Date(str, xep0091Date7Digit2MonthFormatter));
        if (!filterDatesBefore.isEmpty()) {
            return determineNearestDate(instance, filterDatesBefore).getTime();
        }
        return null;
    }

    private static Calendar parseXEP91Date(String str, DateFormat dateFormat) {
        Calendar calendar;
        try {
            synchronized (dateFormat) {
                dateFormat.parse(str);
                calendar = dateFormat.getCalendar();
            }
            return calendar;
        } catch (ParseException e) {
            return null;
        }
    }

    private static List<Calendar> filterDatesBefore(Calendar calendar, Calendar... calendarArr) {
        ArrayList arrayList = new ArrayList();
        for (Calendar calendar2 : calendarArr) {
            if (calendar2 != null && calendar2.before(calendar)) {
                arrayList.add(calendar2);
            }
        }
        return arrayList;
    }

    private static Calendar determineNearestDate(final Calendar calendar, List<Calendar> list) {
        Collections.sort(list, new Comparator<Calendar>() {
            public int compare(Calendar calendar, Calendar calendar2) {
                return new Long(calendar.getTimeInMillis() - calendar.getTimeInMillis()).compareTo(new Long(calendar.getTimeInMillis() - calendar2.getTimeInMillis()));
            }
        });
        return list.get(0);
    }

    private static class PatternCouplings {
        final DateFormatType formatter;
        final Pattern pattern;

        public PatternCouplings(Pattern pattern2, DateFormatType dateFormatType) {
            this.pattern = pattern2;
            this.formatter = dateFormatType;
        }
    }
}
