package org.jivesoftware.smack.util;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class LazyStringBuilder implements Appendable, CharSequence {
    static final /* synthetic */ boolean $assertionsDisabled = (!LazyStringBuilder.class.desiredAssertionStatus());
    private String cache;
    private final List<CharSequence> list = new ArrayList(20);

    private void invalidateCache() {
        this.cache = null;
    }

    public LazyStringBuilder append(LazyStringBuilder lazyStringBuilder) {
        this.list.addAll(lazyStringBuilder.list);
        invalidateCache();
        return this;
    }

    public LazyStringBuilder append(CharSequence charSequence) {
        if ($assertionsDisabled || charSequence != null) {
            this.list.add(charSequence);
            invalidateCache();
            return this;
        }
        throw new AssertionError();
    }

    public LazyStringBuilder append(CharSequence charSequence, int i, int i2) {
        this.list.add(charSequence.subSequence(i, i2));
        invalidateCache();
        return this;
    }

    public LazyStringBuilder append(char c) {
        this.list.add(Character.toString(c));
        invalidateCache();
        return this;
    }

    public int length() {
        if (this.cache != null) {
            return this.cache.length();
        }
        int i = 0;
        Iterator<CharSequence> it = this.list.iterator();
        while (true) {
            int i2 = i;
            if (!it.hasNext()) {
                return i2;
            }
            i = it.next().length() + i2;
        }
    }

    public char charAt(int i) {
        if (this.cache != null) {
            return this.cache.charAt(i);
        }
        for (CharSequence next : this.list) {
            if (i < next.length()) {
                return next.charAt(i);
            }
            i -= next.length();
        }
        throw new IndexOutOfBoundsException();
    }

    public CharSequence subSequence(int i, int i2) {
        return toString().subSequence(i, i2);
    }

    public String toString() {
        if (this.cache == null) {
            StringBuilder sb = new StringBuilder(length());
            for (CharSequence append : this.list) {
                sb.append(append);
            }
            this.cache = sb.toString();
        }
        return this.cache;
    }
}
