package org.jivesoftware.smack.util.dns.dnsjava;

import java.util.ArrayList;
import java.util.List;
import org.jivesoftware.smack.util.dns.DNSResolver;
import org.jivesoftware.smack.util.dns.SRVRecord;
import org.xbill.DNS.Lookup;
import org.xbill.DNS.TextParseException;

public class DNSJavaResolver implements DNSResolver {
    private static DNSJavaResolver instance = new DNSJavaResolver();

    private DNSJavaResolver() {
    }

    public static DNSResolver getInstance() {
        return instance;
    }

    public List<SRVRecord> lookupSRVRecords(String str) throws TextParseException {
        ArrayList arrayList = new ArrayList();
        org.xbill.DNS.SRVRecord[] run = new Lookup(str, 33).run();
        if (run == null) {
            return arrayList;
        }
        for (org.xbill.DNS.SRVRecord sRVRecord : run) {
            if (!(sRVRecord == null || sRVRecord.getTarget() == null)) {
                arrayList.add(new SRVRecord(sRVRecord.getTarget().toString(), sRVRecord.getPort(), sRVRecord.getPriority(), sRVRecord.getWeight()));
            }
        }
        return arrayList;
    }
}
