package org.jivesoftware.smack.util;

import android.support.v4.media.TransportMediator;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilterInputStream;
import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

public class Base64 {
    public static final int DECODE = 0;
    public static final int DONT_BREAK_LINES = 8;
    public static final int ENCODE = 1;
    private static final byte EQUALS_SIGN = 61;
    private static final byte EQUALS_SIGN_ENC = -1;
    public static final int GZIP = 2;
    private static final Logger LOGGER = Logger.getLogger(Base64.class.getName());
    private static final int MAX_LINE_LENGTH = 76;
    private static final byte NEW_LINE = 10;
    public static final int NO_OPTIONS = 0;
    public static final int ORDERED = 32;
    private static final String PREFERRED_ENCODING = "UTF-8";
    public static final int URL_SAFE = 16;
    private static final byte WHITE_SPACE_ENC = -5;
    private static final byte[] _ORDERED_ALPHABET = {45, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 95, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122};
    private static final byte[] _ORDERED_DECODABET;
    private static final byte[] _STANDARD_ALPHABET = {65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 43, 47};
    private static final byte[] _STANDARD_DECODABET;
    private static final byte[] _URL_SAFE_ALPHABET = {65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 45, 95};
    private static final byte[] _URL_SAFE_DECODABET;

    static {
        byte[] bArr = new byte[TransportMediator.KEYCODE_MEDIA_PAUSE];
        // fill-array-data instruction
        bArr[0] = -9;
        bArr[1] = -9;
        bArr[2] = -9;
        bArr[3] = -9;
        bArr[4] = -9;
        bArr[5] = -9;
        bArr[6] = -9;
        bArr[7] = -9;
        bArr[8] = -9;
        bArr[9] = -5;
        bArr[10] = -5;
        bArr[11] = -9;
        bArr[12] = -9;
        bArr[13] = -5;
        bArr[14] = -9;
        bArr[15] = -9;
        bArr[16] = -9;
        bArr[17] = -9;
        bArr[18] = -9;
        bArr[19] = -9;
        bArr[20] = -9;
        bArr[21] = -9;
        bArr[22] = -9;
        bArr[23] = -9;
        bArr[24] = -9;
        bArr[25] = -9;
        bArr[26] = -9;
        bArr[27] = -9;
        bArr[28] = -9;
        bArr[29] = -9;
        bArr[30] = -9;
        bArr[31] = -9;
        bArr[32] = -5;
        bArr[33] = -9;
        bArr[34] = -9;
        bArr[35] = -9;
        bArr[36] = -9;
        bArr[37] = -9;
        bArr[38] = -9;
        bArr[39] = -9;
        bArr[40] = -9;
        bArr[41] = -9;
        bArr[42] = -9;
        bArr[43] = 62;
        bArr[44] = -9;
        bArr[45] = -9;
        bArr[46] = -9;
        bArr[47] = 63;
        bArr[48] = 52;
        bArr[49] = 53;
        bArr[50] = 54;
        bArr[51] = 55;
        bArr[52] = 56;
        bArr[53] = 57;
        bArr[54] = 58;
        bArr[55] = 59;
        bArr[56] = 60;
        bArr[57] = 61;
        bArr[58] = -9;
        bArr[59] = -9;
        bArr[60] = -9;
        bArr[61] = -1;
        bArr[62] = -9;
        bArr[63] = -9;
        bArr[64] = -9;
        bArr[65] = 0;
        bArr[66] = 1;
        bArr[67] = 2;
        bArr[68] = 3;
        bArr[69] = 4;
        bArr[70] = 5;
        bArr[71] = 6;
        bArr[72] = 7;
        bArr[73] = 8;
        bArr[74] = 9;
        bArr[75] = 10;
        bArr[76] = 11;
        bArr[77] = 12;
        bArr[78] = 13;
        bArr[79] = 14;
        bArr[80] = 15;
        bArr[81] = 16;
        bArr[82] = 17;
        bArr[83] = 18;
        bArr[84] = 19;
        bArr[85] = 20;
        bArr[86] = 21;
        bArr[87] = 22;
        bArr[88] = 23;
        bArr[89] = 24;
        bArr[90] = 25;
        bArr[91] = -9;
        bArr[92] = -9;
        bArr[93] = -9;
        bArr[94] = -9;
        bArr[95] = -9;
        bArr[96] = -9;
        bArr[97] = 26;
        bArr[98] = 27;
        bArr[99] = 28;
        bArr[100] = 29;
        bArr[101] = 30;
        bArr[102] = 31;
        bArr[103] = 32;
        bArr[104] = 33;
        bArr[105] = 34;
        bArr[106] = 35;
        bArr[107] = 36;
        bArr[108] = 37;
        bArr[109] = 38;
        bArr[110] = 39;
        bArr[111] = 40;
        bArr[112] = 41;
        bArr[113] = 42;
        bArr[114] = 43;
        bArr[115] = 44;
        bArr[116] = 45;
        bArr[117] = 46;
        bArr[118] = 47;
        bArr[119] = 48;
        bArr[120] = 49;
        bArr[121] = 50;
        bArr[122] = 51;
        bArr[123] = -9;
        bArr[124] = -9;
        bArr[125] = -9;
        bArr[126] = -9;
        _STANDARD_DECODABET = bArr;
        byte[] bArr2 = new byte[TransportMediator.KEYCODE_MEDIA_PAUSE];
        // fill-array-data instruction
        bArr2[0] = -9;
        bArr2[1] = -9;
        bArr2[2] = -9;
        bArr2[3] = -9;
        bArr2[4] = -9;
        bArr2[5] = -9;
        bArr2[6] = -9;
        bArr2[7] = -9;
        bArr2[8] = -9;
        bArr2[9] = -5;
        bArr2[10] = -5;
        bArr2[11] = -9;
        bArr2[12] = -9;
        bArr2[13] = -5;
        bArr2[14] = -9;
        bArr2[15] = -9;
        bArr2[16] = -9;
        bArr2[17] = -9;
        bArr2[18] = -9;
        bArr2[19] = -9;
        bArr2[20] = -9;
        bArr2[21] = -9;
        bArr2[22] = -9;
        bArr2[23] = -9;
        bArr2[24] = -9;
        bArr2[25] = -9;
        bArr2[26] = -9;
        bArr2[27] = -9;
        bArr2[28] = -9;
        bArr2[29] = -9;
        bArr2[30] = -9;
        bArr2[31] = -9;
        bArr2[32] = -5;
        bArr2[33] = -9;
        bArr2[34] = -9;
        bArr2[35] = -9;
        bArr2[36] = -9;
        bArr2[37] = -9;
        bArr2[38] = -9;
        bArr2[39] = -9;
        bArr2[40] = -9;
        bArr2[41] = -9;
        bArr2[42] = -9;
        bArr2[43] = -9;
        bArr2[44] = -9;
        bArr2[45] = 62;
        bArr2[46] = -9;
        bArr2[47] = -9;
        bArr2[48] = 52;
        bArr2[49] = 53;
        bArr2[50] = 54;
        bArr2[51] = 55;
        bArr2[52] = 56;
        bArr2[53] = 57;
        bArr2[54] = 58;
        bArr2[55] = 59;
        bArr2[56] = 60;
        bArr2[57] = 61;
        bArr2[58] = -9;
        bArr2[59] = -9;
        bArr2[60] = -9;
        bArr2[61] = -1;
        bArr2[62] = -9;
        bArr2[63] = -9;
        bArr2[64] = -9;
        bArr2[65] = 0;
        bArr2[66] = 1;
        bArr2[67] = 2;
        bArr2[68] = 3;
        bArr2[69] = 4;
        bArr2[70] = 5;
        bArr2[71] = 6;
        bArr2[72] = 7;
        bArr2[73] = 8;
        bArr2[74] = 9;
        bArr2[75] = 10;
        bArr2[76] = 11;
        bArr2[77] = 12;
        bArr2[78] = 13;
        bArr2[79] = 14;
        bArr2[80] = 15;
        bArr2[81] = 16;
        bArr2[82] = 17;
        bArr2[83] = 18;
        bArr2[84] = 19;
        bArr2[85] = 20;
        bArr2[86] = 21;
        bArr2[87] = 22;
        bArr2[88] = 23;
        bArr2[89] = 24;
        bArr2[90] = 25;
        bArr2[91] = -9;
        bArr2[92] = -9;
        bArr2[93] = -9;
        bArr2[94] = -9;
        bArr2[95] = 63;
        bArr2[96] = -9;
        bArr2[97] = 26;
        bArr2[98] = 27;
        bArr2[99] = 28;
        bArr2[100] = 29;
        bArr2[101] = 30;
        bArr2[102] = 31;
        bArr2[103] = 32;
        bArr2[104] = 33;
        bArr2[105] = 34;
        bArr2[106] = 35;
        bArr2[107] = 36;
        bArr2[108] = 37;
        bArr2[109] = 38;
        bArr2[110] = 39;
        bArr2[111] = 40;
        bArr2[112] = 41;
        bArr2[113] = 42;
        bArr2[114] = 43;
        bArr2[115] = 44;
        bArr2[116] = 45;
        bArr2[117] = 46;
        bArr2[118] = 47;
        bArr2[119] = 48;
        bArr2[120] = 49;
        bArr2[121] = 50;
        bArr2[122] = 51;
        bArr2[123] = -9;
        bArr2[124] = -9;
        bArr2[125] = -9;
        bArr2[126] = -9;
        _URL_SAFE_DECODABET = bArr2;
        byte[] bArr3 = new byte[TransportMediator.KEYCODE_MEDIA_PAUSE];
        // fill-array-data instruction
        bArr3[0] = -9;
        bArr3[1] = -9;
        bArr3[2] = -9;
        bArr3[3] = -9;
        bArr3[4] = -9;
        bArr3[5] = -9;
        bArr3[6] = -9;
        bArr3[7] = -9;
        bArr3[8] = -9;
        bArr3[9] = -5;
        bArr3[10] = -5;
        bArr3[11] = -9;
        bArr3[12] = -9;
        bArr3[13] = -5;
        bArr3[14] = -9;
        bArr3[15] = -9;
        bArr3[16] = -9;
        bArr3[17] = -9;
        bArr3[18] = -9;
        bArr3[19] = -9;
        bArr3[20] = -9;
        bArr3[21] = -9;
        bArr3[22] = -9;
        bArr3[23] = -9;
        bArr3[24] = -9;
        bArr3[25] = -9;
        bArr3[26] = -9;
        bArr3[27] = -9;
        bArr3[28] = -9;
        bArr3[29] = -9;
        bArr3[30] = -9;
        bArr3[31] = -9;
        bArr3[32] = -5;
        bArr3[33] = -9;
        bArr3[34] = -9;
        bArr3[35] = -9;
        bArr3[36] = -9;
        bArr3[37] = -9;
        bArr3[38] = -9;
        bArr3[39] = -9;
        bArr3[40] = -9;
        bArr3[41] = -9;
        bArr3[42] = -9;
        bArr3[43] = -9;
        bArr3[44] = -9;
        bArr3[45] = 0;
        bArr3[46] = -9;
        bArr3[47] = -9;
        bArr3[48] = 1;
        bArr3[49] = 2;
        bArr3[50] = 3;
        bArr3[51] = 4;
        bArr3[52] = 5;
        bArr3[53] = 6;
        bArr3[54] = 7;
        bArr3[55] = 8;
        bArr3[56] = 9;
        bArr3[57] = 10;
        bArr3[58] = -9;
        bArr3[59] = -9;
        bArr3[60] = -9;
        bArr3[61] = -1;
        bArr3[62] = -9;
        bArr3[63] = -9;
        bArr3[64] = -9;
        bArr3[65] = 11;
        bArr3[66] = 12;
        bArr3[67] = 13;
        bArr3[68] = 14;
        bArr3[69] = 15;
        bArr3[70] = 16;
        bArr3[71] = 17;
        bArr3[72] = 18;
        bArr3[73] = 19;
        bArr3[74] = 20;
        bArr3[75] = 21;
        bArr3[76] = 22;
        bArr3[77] = 23;
        bArr3[78] = 24;
        bArr3[79] = 25;
        bArr3[80] = 26;
        bArr3[81] = 27;
        bArr3[82] = 28;
        bArr3[83] = 29;
        bArr3[84] = 30;
        bArr3[85] = 31;
        bArr3[86] = 32;
        bArr3[87] = 33;
        bArr3[88] = 34;
        bArr3[89] = 35;
        bArr3[90] = 36;
        bArr3[91] = -9;
        bArr3[92] = -9;
        bArr3[93] = -9;
        bArr3[94] = -9;
        bArr3[95] = 37;
        bArr3[96] = -9;
        bArr3[97] = 38;
        bArr3[98] = 39;
        bArr3[99] = 40;
        bArr3[100] = 41;
        bArr3[101] = 42;
        bArr3[102] = 43;
        bArr3[103] = 44;
        bArr3[104] = 45;
        bArr3[105] = 46;
        bArr3[106] = 47;
        bArr3[107] = 48;
        bArr3[108] = 49;
        bArr3[109] = 50;
        bArr3[110] = 51;
        bArr3[111] = 52;
        bArr3[112] = 53;
        bArr3[113] = 54;
        bArr3[114] = 55;
        bArr3[115] = 56;
        bArr3[116] = 57;
        bArr3[117] = 58;
        bArr3[118] = 59;
        bArr3[119] = 60;
        bArr3[120] = 61;
        bArr3[121] = 62;
        bArr3[122] = 63;
        bArr3[123] = -9;
        bArr3[124] = -9;
        bArr3[125] = -9;
        bArr3[126] = -9;
        _ORDERED_DECODABET = bArr3;
    }

    private static final byte[] getAlphabet(int i) {
        if ((i & 16) == 16) {
            return _URL_SAFE_ALPHABET;
        }
        if ((i & 32) == 32) {
            return _ORDERED_ALPHABET;
        }
        return _STANDARD_ALPHABET;
    }

    /* access modifiers changed from: private */
    public static final byte[] getDecodabet(int i) {
        if ((i & 16) == 16) {
            return _URL_SAFE_DECODABET;
        }
        if ((i & 32) == 32) {
            return _ORDERED_DECODABET;
        }
        return _STANDARD_DECODABET;
    }

    private Base64() {
    }

    /* access modifiers changed from: private */
    public static byte[] encode3to4(byte[] bArr, byte[] bArr2, int i, int i2) {
        encode3to4(bArr2, 0, i, bArr, 0, i2);
        return bArr;
    }

    /* access modifiers changed from: private */
    public static byte[] encode3to4(byte[] bArr, int i, int i2, byte[] bArr2, int i3, int i4) {
        int i5 = 0;
        byte[] alphabet = getAlphabet(i4);
        int i6 = (i2 > 1 ? (bArr[i + 1] << 24) >>> 16 : 0) | (i2 > 0 ? (bArr[i] << 24) >>> 8 : 0);
        if (i2 > 2) {
            i5 = (bArr[i + 2] << 24) >>> 24;
        }
        int i7 = i5 | i6;
        switch (i2) {
            case 1:
                bArr2[i3] = alphabet[i7 >>> 18];
                bArr2[i3 + 1] = alphabet[(i7 >>> 12) & 63];
                bArr2[i3 + 2] = EQUALS_SIGN;
                bArr2[i3 + 3] = EQUALS_SIGN;
                break;
            case 2:
                bArr2[i3] = alphabet[i7 >>> 18];
                bArr2[i3 + 1] = alphabet[(i7 >>> 12) & 63];
                bArr2[i3 + 2] = alphabet[(i7 >>> 6) & 63];
                bArr2[i3 + 3] = EQUALS_SIGN;
                break;
            case 3:
                bArr2[i3] = alphabet[i7 >>> 18];
                bArr2[i3 + 1] = alphabet[(i7 >>> 12) & 63];
                bArr2[i3 + 2] = alphabet[(i7 >>> 6) & 63];
                bArr2[i3 + 3] = alphabet[i7 & 63];
                break;
        }
        return bArr2;
    }

    public static String encodeObject(Serializable serializable) {
        return encodeObject(serializable, 0);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
     arg types: [java.util.logging.Level, java.lang.String, java.io.IOException]
     candidates:
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
    public static String encodeObject(Serializable serializable, int i) {
        ByteArrayOutputStream byteArrayOutputStream;
        OutputStream outputStream;
        ObjectOutputStream objectOutputStream;
        GZIPOutputStream gZIPOutputStream;
        Throwable th;
        int i2 = i & 2;
        try {
            byteArrayOutputStream = new ByteArrayOutputStream();
            try {
                outputStream = new OutputStream(byteArrayOutputStream, i | 1);
                if (i2 == 2) {
                    try {
                        gZIPOutputStream = new GZIPOutputStream(outputStream);
                        try {
                            objectOutputStream = new ObjectOutputStream(gZIPOutputStream);
                        } catch (IOException e) {
                            e = e;
                            objectOutputStream = null;
                            try {
                                LOGGER.log(Level.SEVERE, "Error encoding object", (Throwable) e);
                                try {
                                    objectOutputStream.close();
                                } catch (Exception e2) {
                                }
                                try {
                                    gZIPOutputStream.close();
                                } catch (Exception e3) {
                                }
                                try {
                                    outputStream.close();
                                } catch (Exception e4) {
                                }
                                try {
                                    byteArrayOutputStream.close();
                                    return null;
                                } catch (Exception e5) {
                                    return null;
                                }
                            } catch (Throwable th2) {
                                th = th2;
                                try {
                                    objectOutputStream.close();
                                } catch (Exception e6) {
                                }
                                try {
                                    gZIPOutputStream.close();
                                } catch (Exception e7) {
                                }
                                try {
                                    outputStream.close();
                                } catch (Exception e8) {
                                }
                                try {
                                    byteArrayOutputStream.close();
                                } catch (Exception e9) {
                                }
                                throw th;
                            }
                        } catch (Throwable th3) {
                            objectOutputStream = null;
                            th = th3;
                            objectOutputStream.close();
                            gZIPOutputStream.close();
                            outputStream.close();
                            byteArrayOutputStream.close();
                            throw th;
                        }
                    } catch (IOException e10) {
                        e = e10;
                        gZIPOutputStream = null;
                        objectOutputStream = null;
                        LOGGER.log(Level.SEVERE, "Error encoding object", (Throwable) e);
                        objectOutputStream.close();
                        gZIPOutputStream.close();
                        outputStream.close();
                        byteArrayOutputStream.close();
                        return null;
                    } catch (Throwable th4) {
                        gZIPOutputStream = null;
                        objectOutputStream = null;
                        th = th4;
                        objectOutputStream.close();
                        gZIPOutputStream.close();
                        outputStream.close();
                        byteArrayOutputStream.close();
                        throw th;
                    }
                } else {
                    objectOutputStream = new ObjectOutputStream(outputStream);
                    gZIPOutputStream = null;
                }
                try {
                    objectOutputStream.writeObject(serializable);
                    try {
                        objectOutputStream.close();
                    } catch (Exception e11) {
                    }
                    try {
                        gZIPOutputStream.close();
                    } catch (Exception e12) {
                    }
                    try {
                        outputStream.close();
                    } catch (Exception e13) {
                    }
                    try {
                        byteArrayOutputStream.close();
                    } catch (Exception e14) {
                    }
                    try {
                        return new String(byteArrayOutputStream.toByteArray(), PREFERRED_ENCODING);
                    } catch (UnsupportedEncodingException e15) {
                        return new String(byteArrayOutputStream.toByteArray());
                    }
                } catch (IOException e16) {
                    e = e16;
                    LOGGER.log(Level.SEVERE, "Error encoding object", (Throwable) e);
                    objectOutputStream.close();
                    gZIPOutputStream.close();
                    outputStream.close();
                    byteArrayOutputStream.close();
                    return null;
                }
            } catch (IOException e17) {
                e = e17;
                gZIPOutputStream = null;
                objectOutputStream = null;
                outputStream = null;
                LOGGER.log(Level.SEVERE, "Error encoding object", (Throwable) e);
                objectOutputStream.close();
                gZIPOutputStream.close();
                outputStream.close();
                byteArrayOutputStream.close();
                return null;
            } catch (Throwable th5) {
                gZIPOutputStream = null;
                objectOutputStream = null;
                outputStream = null;
                th = th5;
                objectOutputStream.close();
                gZIPOutputStream.close();
                outputStream.close();
                byteArrayOutputStream.close();
                throw th;
            }
        } catch (IOException e18) {
            e = e18;
            gZIPOutputStream = null;
            objectOutputStream = null;
            outputStream = null;
            byteArrayOutputStream = null;
            LOGGER.log(Level.SEVERE, "Error encoding object", (Throwable) e);
            objectOutputStream.close();
            gZIPOutputStream.close();
            outputStream.close();
            byteArrayOutputStream.close();
            return null;
        } catch (Throwable th6) {
            gZIPOutputStream = null;
            objectOutputStream = null;
            outputStream = null;
            byteArrayOutputStream = null;
            th = th6;
            objectOutputStream.close();
            gZIPOutputStream.close();
            outputStream.close();
            byteArrayOutputStream.close();
            throw th;
        }
    }

    public static String encodeBytes(byte[] bArr) {
        return encodeBytes(bArr, 0, bArr.length, 0);
    }

    public static String encodeBytes(byte[] bArr, int i) {
        return encodeBytes(bArr, 0, bArr.length, i);
    }

    public static String encodeBytes(byte[] bArr, int i, int i2) {
        return encodeBytes(bArr, i, i2, 0);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
     arg types: [java.util.logging.Level, java.lang.String, java.io.IOException]
     candidates:
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
    public static String encodeBytes(byte[] bArr, int i, int i2, int i3) {
        int i4;
        int i5;
        ByteArrayOutputStream byteArrayOutputStream;
        GZIPOutputStream gZIPOutputStream;
        OutputStream outputStream;
        Throwable th;
        int i6 = i3 & 8;
        if ((i3 & 2) == 2) {
            try {
                byteArrayOutputStream = new ByteArrayOutputStream();
                try {
                    outputStream = new OutputStream(byteArrayOutputStream, i3 | 1);
                    try {
                        gZIPOutputStream = new GZIPOutputStream(outputStream);
                        try {
                            gZIPOutputStream.write(bArr, i, i2);
                            gZIPOutputStream.close();
                            try {
                                gZIPOutputStream.close();
                            } catch (Exception e) {
                            }
                            try {
                                outputStream.close();
                            } catch (Exception e2) {
                            }
                            try {
                                byteArrayOutputStream.close();
                            } catch (Exception e3) {
                            }
                            try {
                                return new String(byteArrayOutputStream.toByteArray(), PREFERRED_ENCODING);
                            } catch (UnsupportedEncodingException e4) {
                                return new String(byteArrayOutputStream.toByteArray());
                            }
                        } catch (IOException e5) {
                            e = e5;
                            try {
                                LOGGER.log(Level.SEVERE, "Error encoding bytes", (Throwable) e);
                                try {
                                    gZIPOutputStream.close();
                                } catch (Exception e6) {
                                }
                                try {
                                    outputStream.close();
                                } catch (Exception e7) {
                                }
                                try {
                                    byteArrayOutputStream.close();
                                    return null;
                                } catch (Exception e8) {
                                    return null;
                                }
                            } catch (Throwable th2) {
                                th = th2;
                                try {
                                    gZIPOutputStream.close();
                                } catch (Exception e9) {
                                }
                                try {
                                    outputStream.close();
                                } catch (Exception e10) {
                                }
                                try {
                                    byteArrayOutputStream.close();
                                } catch (Exception e11) {
                                }
                                throw th;
                            }
                        }
                    } catch (IOException e12) {
                        e = e12;
                        gZIPOutputStream = null;
                        LOGGER.log(Level.SEVERE, "Error encoding bytes", (Throwable) e);
                        gZIPOutputStream.close();
                        outputStream.close();
                        byteArrayOutputStream.close();
                        return null;
                    } catch (Throwable th3) {
                        gZIPOutputStream = null;
                        th = th3;
                        gZIPOutputStream.close();
                        outputStream.close();
                        byteArrayOutputStream.close();
                        throw th;
                    }
                } catch (IOException e13) {
                    e = e13;
                    outputStream = null;
                    gZIPOutputStream = null;
                    LOGGER.log(Level.SEVERE, "Error encoding bytes", (Throwable) e);
                    gZIPOutputStream.close();
                    outputStream.close();
                    byteArrayOutputStream.close();
                    return null;
                } catch (Throwable th4) {
                    outputStream = null;
                    gZIPOutputStream = null;
                    th = th4;
                    gZIPOutputStream.close();
                    outputStream.close();
                    byteArrayOutputStream.close();
                    throw th;
                }
            } catch (IOException e14) {
                e = e14;
                outputStream = null;
                gZIPOutputStream = null;
                byteArrayOutputStream = null;
                LOGGER.log(Level.SEVERE, "Error encoding bytes", (Throwable) e);
                gZIPOutputStream.close();
                outputStream.close();
                byteArrayOutputStream.close();
                return null;
            } catch (Throwable th5) {
                outputStream = null;
                gZIPOutputStream = null;
                byteArrayOutputStream = null;
                th = th5;
                gZIPOutputStream.close();
                outputStream.close();
                byteArrayOutputStream.close();
                throw th;
            }
        } else {
            boolean z = i6 == 0;
            int i7 = (i2 * 4) / 3;
            if (i2 % 3 > 0) {
                i4 = 4;
            } else {
                i4 = 0;
            }
            int i8 = i7 + i4;
            if (z) {
                i5 = i7 / MAX_LINE_LENGTH;
            } else {
                i5 = 0;
            }
            byte[] bArr2 = new byte[(i5 + i8)];
            int i9 = i2 - 2;
            int i10 = 0;
            int i11 = 0;
            int i12 = 0;
            while (i12 < i9) {
                encode3to4(bArr, i12 + i, 3, bArr2, i11, i3);
                int i13 = i10 + 4;
                if (z && i13 == MAX_LINE_LENGTH) {
                    bArr2[i11 + 4] = NEW_LINE;
                    i11++;
                    i13 = 0;
                }
                i11 += 4;
                i10 = i13;
                i12 += 3;
            }
            if (i12 < i2) {
                encode3to4(bArr, i12 + i, i2 - i12, bArr2, i11, i3);
                i11 += 4;
            }
            try {
                return new String(bArr2, 0, i11, PREFERRED_ENCODING);
            } catch (UnsupportedEncodingException e15) {
                return new String(bArr2, 0, i11);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
     arg types: [java.util.logging.Level, java.lang.String, java.lang.Exception]
     candidates:
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
    /* access modifiers changed from: private */
    public static int decode4to3(byte[] bArr, int i, byte[] bArr2, int i2, int i3) {
        byte[] decodabet = getDecodabet(i3);
        if (bArr[i + 2] == 61) {
            bArr2[i2] = (byte) ((((decodabet[bArr[i]] & EQUALS_SIGN_ENC) << 18) | ((decodabet[bArr[i + 1]] & EQUALS_SIGN_ENC) << 12)) >>> 16);
            return 1;
        } else if (bArr[i + 3] == 61) {
            int i4 = ((decodabet[bArr[i]] & EQUALS_SIGN_ENC) << 18) | ((decodabet[bArr[i + 1]] & EQUALS_SIGN_ENC) << 12) | ((decodabet[bArr[i + 2]] & EQUALS_SIGN_ENC) << 6);
            bArr2[i2] = (byte) (i4 >>> 16);
            bArr2[i2 + 1] = (byte) (i4 >>> 8);
            return 2;
        } else {
            try {
                byte b = ((decodabet[bArr[i]] & EQUALS_SIGN_ENC) << 18) | ((decodabet[bArr[i + 1]] & EQUALS_SIGN_ENC) << 12) | ((decodabet[bArr[i + 2]] & EQUALS_SIGN_ENC) << 6) | (decodabet[bArr[i + 3]] & EQUALS_SIGN_ENC);
                bArr2[i2] = (byte) (b >> 16);
                bArr2[i2 + 1] = (byte) (b >> 8);
                bArr2[i2 + 2] = (byte) b;
                return 3;
            } catch (Exception e) {
                LOGGER.log(Level.SEVERE, e.getMessage(), (Throwable) e);
                LOGGER.severe("" + ((int) bArr[i]) + ": " + ((int) decodabet[bArr[i]]));
                LOGGER.severe("" + ((int) bArr[i + 1]) + ": " + ((int) decodabet[bArr[i + 1]]));
                LOGGER.severe("" + ((int) bArr[i + 2]) + ": " + ((int) decodabet[bArr[i + 2]]));
                LOGGER.severe("" + ((int) bArr[i + 3]) + ": " + ((int) decodabet[bArr[i + 3]]));
                return -1;
            }
        }
    }

    public static byte[] decode(byte[] bArr, int i, int i2, int i3) {
        int i4;
        int i5;
        int i6;
        byte[] decodabet = getDecodabet(i3);
        byte[] bArr2 = new byte[((i2 * 3) / 4)];
        byte[] bArr3 = new byte[4];
        int i7 = i;
        int i8 = 0;
        int i9 = 0;
        while (true) {
            if (i7 >= i + i2) {
                i4 = i9;
                break;
            }
            byte b = (byte) (bArr[i7] & Byte.MAX_VALUE);
            byte b2 = decodabet[b];
            if (b2 >= -5) {
                if (b2 >= -1) {
                    i5 = i8 + 1;
                    bArr3[i8] = b;
                    if (i5 > 3) {
                        i4 = decode4to3(bArr3, 0, bArr2, i9, i3) + i9;
                        if (b == 61) {
                            break;
                        }
                        i6 = i4;
                        i5 = 0;
                    } else {
                        i6 = i9;
                    }
                } else {
                    i5 = i8;
                    i6 = i9;
                }
                i7++;
                i9 = i6;
                i8 = i5;
            } else {
                LOGGER.warning("Bad Base64 input character at " + i7 + ": " + ((int) bArr[i7]) + "(decimal)");
                return null;
            }
        }
        byte[] bArr4 = new byte[i4];
        System.arraycopy(bArr2, 0, bArr4, 0, i4);
        return bArr4;
    }

    public static byte[] decode(String str) {
        return decode(str, 0);
    }

    public static byte[] decode(String str, int i) {
        byte[] bytes;
        ByteArrayOutputStream byteArrayOutputStream;
        ByteArrayInputStream byteArrayInputStream;
        ByteArrayInputStream byteArrayInputStream2;
        GZIPInputStream gZIPInputStream = null;
        try {
            bytes = str.getBytes(PREFERRED_ENCODING);
        } catch (UnsupportedEncodingException e) {
            bytes = str.getBytes();
        }
        byte[] decode = decode(bytes, 0, bytes.length, i);
        if (decode != null && decode.length >= 4 && 35615 == ((decode[0] & EQUALS_SIGN_ENC) | ((decode[1] << 8) & 65280))) {
            byte[] bArr = new byte[2048];
            try {
                byteArrayOutputStream = new ByteArrayOutputStream();
                try {
                    byteArrayInputStream2 = new ByteArrayInputStream(decode);
                    try {
                        GZIPInputStream gZIPInputStream2 = new GZIPInputStream(byteArrayInputStream2);
                        while (true) {
                            try {
                                int read = gZIPInputStream2.read(bArr);
                                if (read < 0) {
                                    break;
                                }
                                byteArrayOutputStream.write(bArr, 0, read);
                            } catch (IOException e2) {
                                gZIPInputStream = gZIPInputStream2;
                                byteArrayInputStream = byteArrayInputStream2;
                            } catch (Throwable th) {
                                th = th;
                                gZIPInputStream = gZIPInputStream2;
                                try {
                                    byteArrayOutputStream.close();
                                } catch (Exception e3) {
                                }
                                try {
                                    gZIPInputStream.close();
                                } catch (Exception e4) {
                                }
                                try {
                                    byteArrayInputStream2.close();
                                } catch (Exception e5) {
                                }
                                throw th;
                            }
                        }
                        decode = byteArrayOutputStream.toByteArray();
                        try {
                            byteArrayOutputStream.close();
                        } catch (Exception e6) {
                        }
                        try {
                            gZIPInputStream2.close();
                        } catch (Exception e7) {
                        }
                        try {
                            byteArrayInputStream2.close();
                        } catch (Exception e8) {
                        }
                    } catch (IOException e9) {
                        byteArrayInputStream = byteArrayInputStream2;
                        try {
                            byteArrayOutputStream.close();
                        } catch (Exception e10) {
                        }
                        try {
                            gZIPInputStream.close();
                        } catch (Exception e11) {
                        }
                        try {
                            byteArrayInputStream.close();
                        } catch (Exception e12) {
                        }
                        return decode;
                    } catch (Throwable th2) {
                        th = th2;
                        byteArrayOutputStream.close();
                        gZIPInputStream.close();
                        byteArrayInputStream2.close();
                        throw th;
                    }
                } catch (IOException e13) {
                    byteArrayInputStream = null;
                    byteArrayOutputStream.close();
                    gZIPInputStream.close();
                    byteArrayInputStream.close();
                    return decode;
                } catch (Throwable th3) {
                    th = th3;
                    byteArrayInputStream2 = null;
                    byteArrayOutputStream.close();
                    gZIPInputStream.close();
                    byteArrayInputStream2.close();
                    throw th;
                }
            } catch (IOException e14) {
                byteArrayOutputStream = null;
                byteArrayInputStream = null;
                byteArrayOutputStream.close();
                gZIPInputStream.close();
                byteArrayInputStream.close();
                return decode;
            } catch (Throwable th4) {
                th = th4;
                byteArrayOutputStream = null;
                byteArrayInputStream2 = null;
                byteArrayOutputStream.close();
                gZIPInputStream.close();
                byteArrayInputStream2.close();
                throw th;
            }
        }
        return decode;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
     arg types: [java.util.logging.Level, java.lang.String, java.io.IOException]
     candidates:
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
     arg types: [java.util.logging.Level, java.lang.String, java.lang.ClassNotFoundException]
     candidates:
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:23:0x0032=Splitter:B:23:0x0032, B:14:0x001d=Splitter:B:14:0x001d} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.Object decodeToObject(java.lang.String r7) {
        /*
            r0 = 0
            byte[] r1 = decode(r7)
            java.io.ByteArrayInputStream r3 = new java.io.ByteArrayInputStream     // Catch:{ IOException -> 0x001a, ClassNotFoundException -> 0x002f, all -> 0x0044 }
            r3.<init>(r1)     // Catch:{ IOException -> 0x001a, ClassNotFoundException -> 0x002f, all -> 0x0044 }
            java.io.ObjectInputStream r2 = new java.io.ObjectInputStream     // Catch:{ IOException -> 0x0066, ClassNotFoundException -> 0x0061, all -> 0x005b }
            r2.<init>(r3)     // Catch:{ IOException -> 0x0066, ClassNotFoundException -> 0x0061, all -> 0x005b }
            java.lang.Object r0 = r2.readObject()     // Catch:{ IOException -> 0x0069, ClassNotFoundException -> 0x0064 }
            r3.close()     // Catch:{ Exception -> 0x004f }
        L_0x0016:
            r2.close()     // Catch:{ Exception -> 0x0051 }
        L_0x0019:
            return r0
        L_0x001a:
            r1 = move-exception
            r2 = r0
            r3 = r0
        L_0x001d:
            java.util.logging.Logger r4 = org.jivesoftware.smack.util.Base64.LOGGER     // Catch:{ all -> 0x005f }
            java.util.logging.Level r5 = java.util.logging.Level.SEVERE     // Catch:{ all -> 0x005f }
            java.lang.String r6 = "Error reading object"
            r4.log(r5, r6, r1)     // Catch:{ all -> 0x005f }
            r3.close()     // Catch:{ Exception -> 0x0053 }
        L_0x0029:
            r2.close()     // Catch:{ Exception -> 0x002d }
            goto L_0x0019
        L_0x002d:
            r1 = move-exception
            goto L_0x0019
        L_0x002f:
            r1 = move-exception
            r2 = r0
            r3 = r0
        L_0x0032:
            java.util.logging.Logger r4 = org.jivesoftware.smack.util.Base64.LOGGER     // Catch:{ all -> 0x005f }
            java.util.logging.Level r5 = java.util.logging.Level.SEVERE     // Catch:{ all -> 0x005f }
            java.lang.String r6 = "Class not found for encoded object"
            r4.log(r5, r6, r1)     // Catch:{ all -> 0x005f }
            r3.close()     // Catch:{ Exception -> 0x0055 }
        L_0x003e:
            r2.close()     // Catch:{ Exception -> 0x0042 }
            goto L_0x0019
        L_0x0042:
            r1 = move-exception
            goto L_0x0019
        L_0x0044:
            r1 = move-exception
            r2 = r0
            r3 = r0
            r0 = r1
        L_0x0048:
            r3.close()     // Catch:{ Exception -> 0x0057 }
        L_0x004b:
            r2.close()     // Catch:{ Exception -> 0x0059 }
        L_0x004e:
            throw r0
        L_0x004f:
            r1 = move-exception
            goto L_0x0016
        L_0x0051:
            r1 = move-exception
            goto L_0x0019
        L_0x0053:
            r1 = move-exception
            goto L_0x0029
        L_0x0055:
            r1 = move-exception
            goto L_0x003e
        L_0x0057:
            r1 = move-exception
            goto L_0x004b
        L_0x0059:
            r1 = move-exception
            goto L_0x004e
        L_0x005b:
            r1 = move-exception
            r2 = r0
            r0 = r1
            goto L_0x0048
        L_0x005f:
            r0 = move-exception
            goto L_0x0048
        L_0x0061:
            r1 = move-exception
            r2 = r0
            goto L_0x0032
        L_0x0064:
            r1 = move-exception
            goto L_0x0032
        L_0x0066:
            r1 = move-exception
            r2 = r0
            goto L_0x001d
        L_0x0069:
            r1 = move-exception
            goto L_0x001d
        */
        throw new UnsupportedOperationException("Method not decompiled: org.jivesoftware.smack.util.Base64.decodeToObject(java.lang.String):java.lang.Object");
    }

    public static boolean encodeToFile(byte[] bArr, String str) {
        OutputStream outputStream;
        boolean z = true;
        OutputStream outputStream2 = null;
        try {
            outputStream = new OutputStream(new FileOutputStream(str), 1);
            try {
                outputStream.write(bArr);
                try {
                    outputStream.close();
                } catch (Exception e) {
                }
            } catch (IOException e2) {
                z = false;
                try {
                    outputStream.close();
                } catch (Exception e3) {
                }
                return z;
            } catch (Throwable th) {
                th = th;
                outputStream2 = outputStream;
                try {
                    outputStream2.close();
                } catch (Exception e4) {
                }
                throw th;
            }
        } catch (IOException e5) {
            outputStream = null;
            z = false;
            outputStream.close();
            return z;
        } catch (Throwable th2) {
            th = th2;
            outputStream2.close();
            throw th;
        }
        return z;
    }

    public static boolean decodeToFile(String str, String str2) {
        OutputStream outputStream;
        boolean z = false;
        OutputStream outputStream2 = null;
        try {
            outputStream = new OutputStream(new FileOutputStream(str2), 0);
            try {
                outputStream.write(str.getBytes(PREFERRED_ENCODING));
                z = true;
                try {
                    outputStream.close();
                } catch (Exception e) {
                }
            } catch (IOException e2) {
                try {
                    outputStream.close();
                } catch (Exception e3) {
                }
                return z;
            } catch (Throwable th) {
                th = th;
                outputStream2 = outputStream;
                try {
                    outputStream2.close();
                } catch (Exception e4) {
                }
                throw th;
            }
        } catch (IOException e5) {
            outputStream = null;
            outputStream.close();
            return z;
        } catch (Throwable th2) {
            th = th2;
            outputStream2.close();
            throw th;
        }
        return z;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
     arg types: [java.util.logging.Level, java.lang.String, java.io.IOException]
     candidates:
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
    public static byte[] decodeFromFile(String str) {
        InputStream inputStream;
        Throwable th;
        byte[] bArr = null;
        int i = 0;
        InputStream inputStream2 = null;
        try {
            File file = new File(str);
            if (file.length() > 2147483647L) {
                LOGGER.warning("File is too big for this convenience method (" + file.length() + " bytes).");
                try {
                    inputStream2.close();
                } catch (Exception e) {
                }
            } else {
                byte[] bArr2 = new byte[((int) file.length())];
                inputStream = new InputStream(new BufferedInputStream(new FileInputStream(file)), 0);
                while (true) {
                    try {
                        int read = inputStream.read(bArr2, i, 4096);
                        if (read < 0) {
                            break;
                        }
                        i += read;
                    } catch (IOException e2) {
                        e = e2;
                        try {
                            LOGGER.log(Level.SEVERE, "Error decoding from file " + str, (Throwable) e);
                            try {
                                inputStream.close();
                            } catch (Exception e3) {
                            }
                            return bArr;
                        } catch (Throwable th2) {
                            th = th2;
                            try {
                                inputStream.close();
                            } catch (Exception e4) {
                            }
                            throw th;
                        }
                    }
                }
                bArr = new byte[i];
                System.arraycopy(bArr2, 0, bArr, 0, i);
                try {
                    inputStream.close();
                } catch (Exception e5) {
                }
            }
        } catch (IOException e6) {
            e = e6;
            inputStream = null;
            LOGGER.log(Level.SEVERE, "Error decoding from file " + str, (Throwable) e);
            inputStream.close();
            return bArr;
        } catch (Throwable th3) {
            inputStream = null;
            th = th3;
            inputStream.close();
            throw th;
        }
        return bArr;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
     arg types: [java.util.logging.Level, java.lang.String, java.io.IOException]
     candidates:
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
    public static String encodeFromFile(String str) {
        InputStream inputStream;
        try {
            File file = new File(str);
            byte[] bArr = new byte[Math.max((int) (((double) file.length()) * 1.4d), 40)];
            inputStream = new InputStream(new BufferedInputStream(new FileInputStream(file)), 1);
            int i = 0;
            while (true) {
                try {
                    int read = inputStream.read(bArr, i, 4096);
                    if (read >= 0) {
                        i = read + i;
                    } else {
                        String str2 = new String(bArr, 0, i, PREFERRED_ENCODING);
                        try {
                            inputStream.close();
                            return str2;
                        } catch (Exception e) {
                            return str2;
                        }
                    }
                } catch (IOException e2) {
                    e = e2;
                    try {
                        LOGGER.log(Level.SEVERE, "Error encoding from file " + str, (Throwable) e);
                        try {
                            inputStream.close();
                            return null;
                        } catch (Exception e3) {
                            return null;
                        }
                    } catch (Throwable th) {
                        th = th;
                        try {
                            inputStream.close();
                        } catch (Exception e4) {
                        }
                        throw th;
                    }
                }
            }
        } catch (IOException e5) {
            e = e5;
            inputStream = null;
            LOGGER.log(Level.SEVERE, "Error encoding from file " + str, (Throwable) e);
            inputStream.close();
            return null;
        } catch (Throwable th2) {
            th = th2;
            inputStream = null;
            inputStream.close();
            throw th;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
     arg types: [java.util.logging.Level, java.lang.String, java.io.IOException]
     candidates:
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
    public static void encodeFileToFile(String str, String str2) {
        BufferedOutputStream bufferedOutputStream;
        String encodeFromFile = encodeFromFile(str);
        try {
            bufferedOutputStream = new BufferedOutputStream(new FileOutputStream(str2));
            try {
                bufferedOutputStream.write(encodeFromFile.getBytes("US-ASCII"));
                try {
                    bufferedOutputStream.close();
                } catch (Exception e) {
                }
            } catch (IOException e2) {
                e = e2;
                try {
                    LOGGER.log(Level.SEVERE, "Error encoding file " + str, (Throwable) e);
                    try {
                        bufferedOutputStream.close();
                    } catch (Exception e3) {
                    }
                } catch (Throwable th) {
                    th = th;
                    try {
                        bufferedOutputStream.close();
                    } catch (Exception e4) {
                    }
                    throw th;
                }
            }
        } catch (IOException e5) {
            e = e5;
            bufferedOutputStream = null;
            LOGGER.log(Level.SEVERE, "Error encoding file " + str, (Throwable) e);
            bufferedOutputStream.close();
        } catch (Throwable th2) {
            th = th2;
            bufferedOutputStream = null;
            bufferedOutputStream.close();
            throw th;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
     arg types: [java.util.logging.Level, java.lang.String, java.io.IOException]
     candidates:
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
    public static void decodeFileToFile(String str, String str2) {
        BufferedOutputStream bufferedOutputStream;
        byte[] decodeFromFile = decodeFromFile(str);
        try {
            bufferedOutputStream = new BufferedOutputStream(new FileOutputStream(str2));
            try {
                bufferedOutputStream.write(decodeFromFile);
                try {
                    bufferedOutputStream.close();
                } catch (Exception e) {
                }
            } catch (IOException e2) {
                e = e2;
                try {
                    LOGGER.log(Level.SEVERE, "Error decoding file " + str, (Throwable) e);
                    try {
                        bufferedOutputStream.close();
                    } catch (Exception e3) {
                    }
                } catch (Throwable th) {
                    th = th;
                    try {
                        bufferedOutputStream.close();
                    } catch (Exception e4) {
                    }
                    throw th;
                }
            }
        } catch (IOException e5) {
            e = e5;
            bufferedOutputStream = null;
            LOGGER.log(Level.SEVERE, "Error decoding file " + str, (Throwable) e);
            bufferedOutputStream.close();
        } catch (Throwable th2) {
            th = th2;
            bufferedOutputStream = null;
            bufferedOutputStream.close();
            throw th;
        }
    }

    public static class InputStream extends FilterInputStream {
        private boolean breakLines;
        private byte[] buffer;
        private int bufferLength;
        private byte[] decodabet;
        private boolean encode;
        private int lineLength;
        private int numSigBytes;
        private int options;
        private int position;

        public InputStream(java.io.InputStream inputStream) {
            this(inputStream, 0);
        }

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public InputStream(java.io.InputStream inputStream, int i) {
            super(inputStream);
            boolean z = true;
            this.breakLines = (i & 8) != 8;
            this.encode = (i & 1) != 1 ? false : z;
            this.bufferLength = this.encode ? 4 : 3;
            this.buffer = new byte[this.bufferLength];
            this.position = -1;
            this.lineLength = 0;
            this.options = i;
            this.decodabet = Base64.getDecodabet(i);
        }

        public int read() throws IOException {
            int read;
            if (this.position < 0) {
                if (this.encode) {
                    byte[] bArr = new byte[3];
                    int i = 0;
                    for (int i2 = 0; i2 < 3; i2++) {
                        try {
                            int read2 = this.in.read();
                            if (read2 >= 0) {
                                bArr[i2] = (byte) read2;
                                i++;
                            }
                        } catch (IOException e) {
                            if (i2 == 0) {
                                throw e;
                            }
                        }
                    }
                    if (i <= 0) {
                        return -1;
                    }
                    byte[] unused = Base64.encode3to4(bArr, 0, i, this.buffer, 0, this.options);
                    this.position = 0;
                    this.numSigBytes = 4;
                } else {
                    byte[] bArr2 = new byte[4];
                    int i3 = 0;
                    while (i3 < 4) {
                        do {
                            read = this.in.read();
                            if (read < 0) {
                                break;
                            }
                        } while (this.decodabet[read & TransportMediator.KEYCODE_MEDIA_PAUSE] <= -5);
                        if (read < 0) {
                            break;
                        }
                        bArr2[i3] = (byte) read;
                        i3++;
                    }
                    if (i3 == 4) {
                        this.numSigBytes = Base64.decode4to3(bArr2, 0, this.buffer, 0, this.options);
                        this.position = 0;
                    } else if (i3 == 0) {
                        return -1;
                    } else {
                        throw new IOException("Improperly padded Base64 input.");
                    }
                }
            }
            if (this.position < 0) {
                throw new IOException("Error in Base64 code reading stream.");
            } else if (this.position >= this.numSigBytes) {
                return -1;
            } else {
                if (!this.encode || !this.breakLines || this.lineLength < Base64.MAX_LINE_LENGTH) {
                    this.lineLength++;
                    byte[] bArr3 = this.buffer;
                    int i4 = this.position;
                    this.position = i4 + 1;
                    byte b = bArr3[i4];
                    if (this.position >= this.bufferLength) {
                        this.position = -1;
                    }
                    return b & Base64.EQUALS_SIGN_ENC;
                }
                this.lineLength = 0;
                return 10;
            }
        }

        public int read(byte[] bArr, int i, int i2) throws IOException {
            int i3 = 0;
            while (i3 < i2) {
                int read = read();
                if (read >= 0) {
                    bArr[i + i3] = (byte) read;
                    i3++;
                } else if (i3 == 0) {
                    return -1;
                } else {
                    return i3;
                }
            }
            return i3;
        }
    }

    public static class OutputStream extends FilterOutputStream {
        private byte[] b4;
        private boolean breakLines;
        private byte[] buffer;
        private int bufferLength;
        private byte[] decodabet;
        private boolean encode;
        private int lineLength;
        private int options;
        private int position;
        private boolean suspendEncoding;

        public OutputStream(java.io.OutputStream outputStream) {
            this(outputStream, 1);
        }

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public OutputStream(java.io.OutputStream outputStream, int i) {
            super(outputStream);
            int i2;
            boolean z = true;
            this.breakLines = (i & 8) != 8;
            this.encode = (i & 1) != 1 ? false : z;
            if (this.encode) {
                i2 = 3;
            } else {
                i2 = 4;
            }
            this.bufferLength = i2;
            this.buffer = new byte[this.bufferLength];
            this.position = 0;
            this.lineLength = 0;
            this.suspendEncoding = false;
            this.b4 = new byte[4];
            this.options = i;
            this.decodabet = Base64.getDecodabet(i);
        }

        public void write(int i) throws IOException {
            if (this.suspendEncoding) {
                this.out.write(i);
            } else if (this.encode) {
                byte[] bArr = this.buffer;
                int i2 = this.position;
                this.position = i2 + 1;
                bArr[i2] = (byte) i;
                if (this.position >= this.bufferLength) {
                    this.out.write(Base64.encode3to4(this.b4, this.buffer, this.bufferLength, this.options));
                    this.lineLength += 4;
                    if (this.breakLines && this.lineLength >= Base64.MAX_LINE_LENGTH) {
                        this.out.write(10);
                        this.lineLength = 0;
                    }
                    this.position = 0;
                }
            } else if (this.decodabet[i & TransportMediator.KEYCODE_MEDIA_PAUSE] > -5) {
                byte[] bArr2 = this.buffer;
                int i3 = this.position;
                this.position = i3 + 1;
                bArr2[i3] = (byte) i;
                if (this.position >= this.bufferLength) {
                    this.out.write(this.b4, 0, Base64.decode4to3(this.buffer, 0, this.b4, 0, this.options));
                    this.position = 0;
                }
            } else if (this.decodabet[i & TransportMediator.KEYCODE_MEDIA_PAUSE] != -5) {
                throw new IOException("Invalid character in Base64 data.");
            }
        }

        public void write(byte[] bArr, int i, int i2) throws IOException {
            if (this.suspendEncoding) {
                this.out.write(bArr, i, i2);
                return;
            }
            for (int i3 = 0; i3 < i2; i3++) {
                write(bArr[i + i3]);
            }
        }

        public void flushBase64() throws IOException {
            if (this.position <= 0) {
                return;
            }
            if (this.encode) {
                this.out.write(Base64.encode3to4(this.b4, this.buffer, this.position, this.options));
                this.position = 0;
                return;
            }
            throw new IOException("Base64 input not properly padded.");
        }

        public void close() throws IOException {
            flushBase64();
            super.close();
            this.buffer = null;
            this.out = null;
        }

        public void suspendEncoding() throws IOException {
            flushBase64();
            this.suspendEncoding = true;
        }

        public void resumeEncoding() {
            this.suspendEncoding = false;
        }
    }
}
