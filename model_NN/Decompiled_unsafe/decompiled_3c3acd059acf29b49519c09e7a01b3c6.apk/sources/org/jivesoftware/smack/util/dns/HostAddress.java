package org.jivesoftware.smack.util.dns;

public class HostAddress {
    private Exception exception;
    private final String fqdn;
    private final int port;

    public HostAddress(String str) {
        this(str, 5222);
    }

    public HostAddress(String str, int i) {
        if (str == null) {
            throw new IllegalArgumentException("FQDN is null");
        } else if (i < 0 || i > 65535) {
            throw new IllegalArgumentException("Port must be a 16-bit unsiged integer (i.e. between 0-65535. Port was: " + i);
        } else {
            if (str.charAt(str.length() - 1) == '.') {
                this.fqdn = str.substring(0, str.length() - 1);
            } else {
                this.fqdn = str;
            }
            this.port = i;
        }
    }

    public String getFQDN() {
        return this.fqdn;
    }

    public int getPort() {
        return this.port;
    }

    public void setException(Exception exc) {
        this.exception = exc;
    }

    public Exception getException() {
        return this.exception;
    }

    public String toString() {
        return this.fqdn + ":" + this.port;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof HostAddress)) {
            return false;
        }
        HostAddress hostAddress = (HostAddress) obj;
        if (!this.fqdn.equals(hostAddress.fqdn)) {
            return false;
        }
        if (this.port != hostAddress.port) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return ((this.fqdn.hashCode() + 37) * 37) + this.port;
    }

    public String getErrorMessage() {
        String message;
        if (this.exception == null) {
            message = "No error logged";
        } else {
            message = this.exception.getMessage();
        }
        return toString() + " Exception: " + message;
    }
}
