package org.jivesoftware.smack;

import java.util.Collection;
import org.jivesoftware.smack.packet.RosterPacket;

public interface RosterStore {
    boolean addEntry(RosterPacket.Item item, String str);

    Collection<RosterPacket.Item> getEntries();

    RosterPacket.Item getEntry(String str);

    String getRosterVersion();

    boolean removeEntry(String str, String str2);

    boolean resetEntries(Collection<RosterPacket.Item> collection, String str);
}
