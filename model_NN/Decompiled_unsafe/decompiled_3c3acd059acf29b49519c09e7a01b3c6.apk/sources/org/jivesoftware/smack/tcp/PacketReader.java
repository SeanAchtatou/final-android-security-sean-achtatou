package org.jivesoftware.smack.tcp;

import java.io.IOException;
import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.parsing.ParsingExceptionCallback;
import org.jivesoftware.smack.parsing.UnparsablePacket;
import org.jivesoftware.smack.sasl.SASLMechanism;
import org.jivesoftware.smack.util.PacketParserUtils;
import org.jivesoftware.smackx.caps.EntityCapsManager;
import org.jivesoftware.smackx.privacy.packet.PrivacyItem;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

class PacketReader {
    private XMPPTCPConnection connection;
    volatile boolean done;
    private volatile boolean lastFeaturesParsed;
    private XmlPullParser parser;
    private Thread readerThread;

    protected PacketReader(XMPPTCPConnection xMPPTCPConnection) throws SmackException {
        this.connection = xMPPTCPConnection;
        init();
    }

    /* access modifiers changed from: protected */
    public void init() throws SmackException {
        this.done = false;
        this.lastFeaturesParsed = false;
        this.readerThread = new Thread() {
            public void run() {
                PacketReader.this.parsePackets(this);
            }
        };
        this.readerThread.setName("Smack Packet Reader (" + this.connection.getConnectionCounter() + ")");
        this.readerThread.setDaemon(true);
        resetParser();
    }

    public synchronized void startup() throws SmackException.NoResponseException, IOException {
        this.readerThread.start();
        try {
            wait(this.connection.getPacketReplyTimeout());
        } catch (InterruptedException e) {
        }
        if (!this.lastFeaturesParsed) {
            this.connection.throwConnectionExceptionOrNoResponse();
        }
    }

    public void shutdown() {
        this.done = true;
    }

    private void resetParser() throws SmackException {
        try {
            this.parser = PacketParserUtils.newXmppParser();
            this.parser.setInput(this.connection.getReader());
        } catch (XmlPullParserException e) {
            throw new SmackException(e);
        }
    }

    /* access modifiers changed from: private */
    public void parsePackets(Thread thread) {
        try {
            int eventType = this.parser.getEventType();
            do {
                if (eventType == 2) {
                    int depth = this.parser.getDepth();
                    ParsingExceptionCallback parsingExceptionCallback = this.connection.getParsingExceptionCallback();
                    if (this.parser.getName().equals("message")) {
                        try {
                            this.connection.processPacket(PacketParserUtils.parseMessage(this.parser));
                        } catch (Exception e) {
                            UnparsablePacket unparsablePacket = new UnparsablePacket(PacketParserUtils.parseContentDepth(this.parser, depth), e);
                            if (parsingExceptionCallback != null) {
                                parsingExceptionCallback.handleUnparsablePacket(unparsablePacket);
                            }
                            eventType = this.parser.next();
                        }
                    } else if (this.parser.getName().equals("iq")) {
                        try {
                            this.connection.processPacket(PacketParserUtils.parseIQ(this.parser, this.connection));
                        } catch (Exception e2) {
                            UnparsablePacket unparsablePacket2 = new UnparsablePacket(PacketParserUtils.parseContentDepth(this.parser, depth), e2);
                            if (parsingExceptionCallback != null) {
                                parsingExceptionCallback.handleUnparsablePacket(unparsablePacket2);
                            }
                            eventType = this.parser.next();
                        }
                    } else if (this.parser.getName().equals("presence")) {
                        try {
                            this.connection.processPacket(PacketParserUtils.parsePresence(this.parser));
                        } catch (Exception e3) {
                            UnparsablePacket unparsablePacket3 = new UnparsablePacket(PacketParserUtils.parseContentDepth(this.parser, depth), e3);
                            if (parsingExceptionCallback != null) {
                                parsingExceptionCallback.handleUnparsablePacket(unparsablePacket3);
                            }
                            eventType = this.parser.next();
                        }
                    } else if (this.parser.getName().equals("stream")) {
                        if ("jabber:client".equals(this.parser.getNamespace(null))) {
                            for (int i = 0; i < this.parser.getAttributeCount(); i++) {
                                if (this.parser.getAttributeName(i).equals("id")) {
                                    this.connection.connectionID = this.parser.getAttributeValue(i);
                                } else if (this.parser.getAttributeName(i).equals(PrivacyItem.SUBSCRIPTION_FROM)) {
                                    this.connection.setServiceName(this.parser.getAttributeValue(i));
                                }
                            }
                        }
                    } else if (this.parser.getName().equals("error")) {
                        throw new XMPPException.StreamErrorException(PacketParserUtils.parseStreamError(this.parser));
                    } else if (this.parser.getName().equals("features")) {
                        parseFeatures(this.parser);
                    } else if (this.parser.getName().equals("proceed")) {
                        this.connection.proceedTLSReceived();
                        resetParser();
                    } else if (this.parser.getName().equals("failure")) {
                        String namespace = this.parser.getNamespace(null);
                        if ("urn:ietf:params:xml:ns:xmpp-tls".equals(namespace)) {
                            throw new Exception("TLS negotiation has failed");
                        } else if ("http://jabber.org/protocol/compress".equals(namespace)) {
                            this.connection.streamCompressionNegotiationDone();
                        } else {
                            SASLMechanism.SASLFailure parseSASLFailure = PacketParserUtils.parseSASLFailure(this.parser);
                            this.connection.processPacket(parseSASLFailure);
                            this.connection.getSASLAuthentication().authenticationFailed(parseSASLFailure);
                        }
                    } else if (this.parser.getName().equals("challenge")) {
                        String nextText = this.parser.nextText();
                        this.connection.processPacket(new SASLMechanism.Challenge(nextText));
                        this.connection.getSASLAuthentication().challengeReceived(nextText);
                    } else if (this.parser.getName().equals("success")) {
                        this.connection.processPacket(new SASLMechanism.Success(this.parser.nextText()));
                        this.connection.packetWriter.openStream();
                        resetParser();
                        this.connection.getSASLAuthentication().authenticated();
                    } else if (this.parser.getName().equals("compressed")) {
                        this.connection.startStreamCompression();
                        resetParser();
                    }
                } else if (eventType == 3 && this.parser.getName().equals("stream")) {
                    this.connection.disconnect();
                }
                eventType = this.parser.next();
                if (this.done || eventType == 1) {
                    return;
                }
            } while (thread == this.readerThread);
        } catch (Exception e4) {
            if (!this.done && !this.connection.isSocketClosed()) {
                synchronized (this) {
                    notify();
                    this.connection.notifyConnectionError(e4);
                }
            }
        }
    }

    private void parseFeatures(XmlPullParser xmlPullParser) throws Exception {
        boolean z = false;
        boolean z2 = false;
        boolean z3 = false;
        while (!z) {
            int next = xmlPullParser.next();
            if (next == 2) {
                if (xmlPullParser.getName().equals("starttls")) {
                    z3 = true;
                } else if (xmlPullParser.getName().equals("mechanisms")) {
                    this.connection.getSASLAuthentication().setAvailableSASLMethods(PacketParserUtils.parseMechanisms(xmlPullParser));
                } else if (xmlPullParser.getName().equals("bind")) {
                    this.connection.serverRequiresBinding();
                } else if (xmlPullParser.getName().equals(EntityCapsManager.ELEMENT)) {
                    String attributeValue = xmlPullParser.getAttributeValue(null, "node");
                    String attributeValue2 = xmlPullParser.getAttributeValue(null, "ver");
                    if (!(attributeValue2 == null || attributeValue == null)) {
                        this.connection.setServiceCapsNode(attributeValue + "#" + attributeValue2);
                    }
                } else if (xmlPullParser.getName().equals("session")) {
                    this.connection.serverSupportsSession();
                } else if (xmlPullParser.getName().equals("ver")) {
                    if (xmlPullParser.getNamespace().equals("urn:xmpp:features:rosterver")) {
                        this.connection.setRosterVersioningSupported();
                    }
                } else if (xmlPullParser.getName().equals("compression")) {
                    this.connection.setAvailableCompressionMethods(PacketParserUtils.parseCompressionMethods(xmlPullParser));
                } else if (xmlPullParser.getName().equals("register")) {
                    this.connection.serverSupportsAccountCreation();
                }
            } else if (next == 3) {
                if (xmlPullParser.getName().equals("starttls")) {
                    this.connection.startTLSReceived(z2);
                } else if (xmlPullParser.getName().equals("required") && z3) {
                    z2 = true;
                } else if (xmlPullParser.getName().equals("features")) {
                    z = true;
                }
            }
        }
        if (!this.connection.isSecureConnection() && !z3 && this.connection.getConfiguration().getSecurityMode() == ConnectionConfiguration.SecurityMode.required) {
            throw new SmackException.SecurityRequiredException();
        } else if (!z3 || this.connection.getConfiguration().getSecurityMode() == ConnectionConfiguration.SecurityMode.disabled) {
            this.lastFeaturesParsed = true;
            synchronized (this) {
                notify();
            }
        }
    }
}
