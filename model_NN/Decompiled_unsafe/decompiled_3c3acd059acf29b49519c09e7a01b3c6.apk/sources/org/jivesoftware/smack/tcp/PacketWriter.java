package org.jivesoftware.smack.tcp;

import java.io.IOException;
import java.io.Writer;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.util.ArrayBlockingQueueWithShutdown;

class PacketWriter {
    private static final Logger LOGGER = Logger.getLogger(PacketWriter.class.getName());
    public static final int QUEUE_SIZE = 500;
    private final XMPPTCPConnection connection;
    volatile boolean done;
    private final ArrayBlockingQueueWithShutdown<Packet> queue = new ArrayBlockingQueueWithShutdown<>(QUEUE_SIZE, true);
    AtomicBoolean shutdownDone = new AtomicBoolean(false);
    private Writer writer;
    private Thread writerThread;

    protected PacketWriter(XMPPTCPConnection xMPPTCPConnection) {
        this.connection = xMPPTCPConnection;
        init();
    }

    /* access modifiers changed from: protected */
    public void init() {
        this.writer = this.connection.getWriter();
        this.done = false;
        this.shutdownDone.set(false);
        this.queue.start();
        this.writerThread = new Thread() {
            public void run() {
                PacketWriter.this.writePackets(this);
            }
        };
        this.writerThread.setName("Smack Packet Writer (" + this.connection.getConnectionCounter() + ")");
        this.writerThread.setDaemon(true);
    }

    public void sendPacket(Packet packet) throws SmackException.NotConnectedException {
        if (this.done) {
            throw new SmackException.NotConnectedException();
        }
        try {
            this.queue.put(packet);
        } catch (InterruptedException e) {
            throw new SmackException.NotConnectedException();
        }
    }

    public void startup() {
        this.writerThread.start();
    }

    /* access modifiers changed from: package-private */
    public void setWriter(Writer writer2) {
        this.writer = writer2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
     arg types: [java.util.logging.Level, java.lang.String, java.lang.InterruptedException]
     candidates:
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
    public void shutdown() {
        this.done = true;
        this.queue.shutdown();
        synchronized (this.shutdownDone) {
            if (!this.shutdownDone.get()) {
                try {
                    this.shutdownDone.wait(this.connection.getPacketReplyTimeout());
                } catch (InterruptedException e) {
                    LOGGER.log(Level.WARNING, "shutdown", (Throwable) e);
                }
            }
        }
    }

    private Packet nextPacket() {
        Packet packet;
        if (this.done) {
            return null;
        }
        try {
            packet = this.queue.take();
        } catch (InterruptedException e) {
            packet = null;
        }
        return packet;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
     arg types: [java.util.logging.Level, java.lang.String, java.lang.Exception]
     candidates:
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
    /* access modifiers changed from: private */
    public void writePackets(Thread thread) {
        try {
            openStream();
            while (!this.done && this.writerThread == thread) {
                Packet nextPacket = nextPacket();
                if (nextPacket != null) {
                    this.writer.write(nextPacket.toXML().toString());
                    if (this.queue.isEmpty()) {
                        this.writer.flush();
                    }
                }
            }
            while (!this.queue.isEmpty()) {
                try {
                    this.writer.write(this.queue.remove().toXML().toString());
                } catch (Exception e) {
                    LOGGER.log(Level.WARNING, "Exception flushing queue during shutdown, ignore and continue", (Throwable) e);
                }
            }
            this.writer.flush();
            this.queue.clear();
            try {
                this.writer.write("</stream:stream>");
                this.writer.flush();
                try {
                    this.writer.close();
                } catch (Exception e2) {
                }
            } catch (Exception e3) {
                LOGGER.log(Level.WARNING, "Exception writing closing stream element", (Throwable) e3);
                try {
                    this.writer.close();
                } catch (Exception e4) {
                }
            } catch (Throwable th) {
                try {
                    this.writer.close();
                } catch (Exception e5) {
                }
                throw th;
            }
            this.shutdownDone.set(true);
            synchronized (this.shutdownDone) {
                this.shutdownDone.notify();
            }
        } catch (IOException e6) {
            if (!this.done && !this.connection.isSocketClosed()) {
                shutdown();
                this.connection.notifyConnectionError(e6);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void openStream() throws IOException {
        StringBuilder sb = new StringBuilder();
        sb.append("<stream:stream");
        sb.append(" to=\"").append(this.connection.getServiceName()).append("\"");
        sb.append(" xmlns=\"jabber:client\"");
        sb.append(" xmlns:stream=\"http://etherx.jabber.org/streams\"");
        sb.append(" version=\"1.0\">");
        this.writer.write(sb.toString());
        this.writer.flush();
    }
}
