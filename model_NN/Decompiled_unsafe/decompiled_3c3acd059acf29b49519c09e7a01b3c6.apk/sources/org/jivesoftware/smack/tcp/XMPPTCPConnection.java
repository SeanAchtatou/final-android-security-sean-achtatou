package org.jivesoftware.smack.tcp;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.net.Socket;
import java.security.KeyStore;
import java.security.Provider;
import java.security.SecureRandom;
import java.security.Security;
import java.security.cert.CertificateException;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.KeyManager;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocket;
import org.apache.harmony.javax.security.auth.callback.Callback;
import org.apache.harmony.javax.security.auth.callback.CallbackHandler;
import org.apache.harmony.javax.security.auth.callback.PasswordCallback;
import org.apache.harmony.javax.security.sasl.SaslException;
import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.ConnectionCreationListener;
import org.jivesoftware.smack.ConnectionListener;
import org.jivesoftware.smack.SASLAuthentication;
import org.jivesoftware.smack.SmackConfiguration;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.compression.XMPPInputOutputStream;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smack.parsing.ParsingExceptionCallback;
import org.jivesoftware.smack.util.StringUtils;
import org.jivesoftware.smack.util.dns.HostAddress;

public class XMPPTCPConnection extends XMPPConnection {
    private static final Logger LOGGER = Logger.getLogger(XMPPTCPConnection.class.getName());
    private boolean anonymous = false;
    private final Object compressionLock = new Object();
    private Collection<String> compressionMethods;
    private boolean connected = false;
    String connectionID = null;
    PacketReader packetReader;
    PacketWriter packetWriter;
    private ParsingExceptionCallback parsingExceptionCallback = SmackConfiguration.getDefaultParsingExceptionCallback();
    private boolean serverAckdCompression = false;
    Socket socket;
    private volatile boolean socketClosed = false;
    private String user = null;
    private boolean usingTLS = false;

    public XMPPTCPConnection(String str, CallbackHandler callbackHandler) {
        super(new ConnectionConfiguration(str));
        this.config.setCallbackHandler(callbackHandler);
    }

    public XMPPTCPConnection(String str) {
        super(new ConnectionConfiguration(str));
    }

    public XMPPTCPConnection(ConnectionConfiguration connectionConfiguration) {
        super(connectionConfiguration);
    }

    public XMPPTCPConnection(ConnectionConfiguration connectionConfiguration, CallbackHandler callbackHandler) {
        super(connectionConfiguration);
        connectionConfiguration.setCallbackHandler(callbackHandler);
    }

    public String getConnectionID() {
        if (!isConnected()) {
            return null;
        }
        return this.connectionID;
    }

    public String getUser() {
        if (!isAuthenticated()) {
            return null;
        }
        return this.user;
    }

    public void setParsingExceptionCallback(ParsingExceptionCallback parsingExceptionCallback2) {
        this.parsingExceptionCallback = parsingExceptionCallback2;
    }

    public ParsingExceptionCallback getParsingExceptionCallback() {
        return this.parsingExceptionCallback;
    }

    public synchronized void login(String str, String str2, String str3) throws XMPPException, SmackException, SaslException, IOException {
        if (!isConnected()) {
            throw new SmackException.NotConnectedException();
        } else if (this.authenticated) {
            throw new SmackException.AlreadyLoggedInException();
        } else {
            String trim = str.toLowerCase(Locale.US).trim();
            if (this.saslAuthentication.hasNonAnonymousAuthentication()) {
                if (str2 != null) {
                    this.saslAuthentication.authenticate(trim, str2, str3);
                } else {
                    this.saslAuthentication.authenticate(str3, this.config.getCallbackHandler());
                }
                if (this.config.isCompressionEnabled()) {
                    useCompression();
                }
                String bindResourceAndEstablishSession = bindResourceAndEstablishSession(str3);
                if (bindResourceAndEstablishSession != null) {
                    this.user = bindResourceAndEstablishSession;
                    setServiceName(StringUtils.parseServer(bindResourceAndEstablishSession));
                } else {
                    this.user = trim + "@" + getServiceName();
                    if (str3 != null) {
                        this.user += "/" + str3;
                    }
                }
                this.authenticated = true;
                this.anonymous = false;
                setLoginInfo(trim, str2, str3);
                if (this.config.isDebuggerEnabled() && this.debugger != null) {
                    this.debugger.userHasLogged(this.user);
                }
                callConnectionAuthenticatedListener();
                if (this.config.isSendPresence()) {
                    sendPacket(new Presence(Presence.Type.available));
                }
            } else {
                throw new SaslException("No non-anonymous SASL authentication mechanism available");
            }
        }
    }

    public synchronized void loginAnonymously() throws XMPPException, SmackException, SaslException, IOException {
        if (!isConnected()) {
            throw new SmackException.NotConnectedException();
        } else if (this.authenticated) {
            throw new SmackException.AlreadyLoggedInException();
        } else if (this.saslAuthentication.hasAnonymousAuthentication()) {
            this.saslAuthentication.authenticateAnonymously();
            String bindResourceAndEstablishSession = bindResourceAndEstablishSession(null);
            this.user = bindResourceAndEstablishSession;
            setServiceName(StringUtils.parseServer(bindResourceAndEstablishSession));
            if (this.config.isCompressionEnabled()) {
                useCompression();
            }
            sendPacket(new Presence(Presence.Type.available));
            this.authenticated = true;
            this.anonymous = true;
            if (this.config.isDebuggerEnabled() && this.debugger != null) {
                this.debugger.userHasLogged(this.user);
            }
            callConnectionAuthenticatedListener();
        } else {
            throw new SaslException("No anonymous SASL authentication mechanism available");
        }
    }

    public boolean isConnected() {
        return this.connected;
    }

    public boolean isSecureConnection() {
        return isUsingTLS();
    }

    public boolean isSocketClosed() {
        return this.socketClosed;
    }

    public boolean isAuthenticated() {
        return this.authenticated;
    }

    public boolean isAnonymous() {
        return this.anonymous;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
     arg types: [java.util.logging.Level, java.lang.String, java.lang.Exception]
     candidates:
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
    /* access modifiers changed from: protected */
    public void shutdown() {
        if (this.packetReader != null) {
            this.packetReader.shutdown();
        }
        if (this.packetWriter != null) {
            this.packetWriter.shutdown();
        }
        this.socketClosed = true;
        try {
            this.socket.close();
        } catch (Exception e) {
            LOGGER.log(Level.WARNING, "shutdown", (Throwable) e);
        }
        setWasAuthenticated(this.authenticated);
        this.authenticated = false;
        this.connected = false;
        this.usingTLS = false;
        this.reader = null;
        this.writer = null;
    }

    /* access modifiers changed from: protected */
    public void sendPacketInternal(Packet packet) throws SmackException.NotConnectedException {
        this.packetWriter.sendPacket(packet);
    }

    private void connectUsingConfiguration(ConnectionConfiguration connectionConfiguration) throws SmackException, IOException {
        try {
            maybeResolveDns();
            Iterator<HostAddress> it = connectionConfiguration.getHostAddresses().iterator();
            LinkedList linkedList = new LinkedList();
            do {
                if (it.hasNext()) {
                    Exception e = null;
                    HostAddress next = it.next();
                    String fqdn = next.getFQDN();
                    int port = next.getPort();
                    try {
                        if (connectionConfiguration.getSocketFactory() == null) {
                            this.socket = new Socket(fqdn, port);
                        } else {
                            this.socket = connectionConfiguration.getSocketFactory().createSocket(fqdn, port);
                        }
                    } catch (Exception e2) {
                        e = e2;
                    }
                    if (e == null) {
                        next.getFQDN();
                        next.getPort();
                    } else {
                        next.setException(e);
                        linkedList.add(next);
                    }
                }
                this.socketClosed = false;
                initConnection();
                return;
            } while (it.hasNext());
            throw new SmackException.ConnectionException(linkedList);
        } catch (Exception e3) {
            throw new SmackException(e3);
        }
    }

    private void initConnection() throws SmackException, IOException {
        boolean z = true;
        if (!(this.packetReader == null || this.packetWriter == null)) {
            z = false;
        }
        this.compressionHandler = null;
        this.serverAckdCompression = false;
        initReaderAndWriter();
        if (z) {
            try {
                this.packetWriter = new PacketWriter(this);
                this.packetReader = new PacketReader(this);
                if (this.config.isDebuggerEnabled()) {
                    addPacketListener(this.debugger.getReaderListener(), null);
                    if (this.debugger.getWriterListener() != null) {
                        addPacketSendingListener(this.debugger.getWriterListener(), null);
                    }
                }
            } catch (SmackException e) {
                shutdown();
                throw e;
            }
        } else {
            this.packetWriter.init();
            this.packetReader.init();
        }
        this.packetWriter.startup();
        this.packetReader.startup();
        this.connected = true;
        if (z) {
            for (ConnectionCreationListener connectionCreated : getConnectionCreationListeners()) {
                connectionCreated.connectionCreated(this);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
     arg types: [java.util.logging.Level, java.lang.String, java.lang.Exception]
     candidates:
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
    private void initReaderAndWriter() throws IOException {
        try {
            if (this.compressionHandler == null) {
                this.reader = new BufferedReader(new InputStreamReader(this.socket.getInputStream(), "UTF-8"));
                this.writer = new BufferedWriter(new OutputStreamWriter(this.socket.getOutputStream(), "UTF-8"));
            } else {
                try {
                    this.writer = new BufferedWriter(new OutputStreamWriter(this.compressionHandler.getOutputStream(this.socket.getOutputStream()), "UTF-8"));
                    this.reader = new BufferedReader(new InputStreamReader(this.compressionHandler.getInputStream(this.socket.getInputStream()), "UTF-8"));
                } catch (Exception e) {
                    LOGGER.log(Level.WARNING, "initReaderAndWriter()", (Throwable) e);
                    this.compressionHandler = null;
                    this.reader = new BufferedReader(new InputStreamReader(this.socket.getInputStream(), "UTF-8"));
                    this.writer = new BufferedWriter(new OutputStreamWriter(this.socket.getOutputStream(), "UTF-8"));
                }
            }
            initDebugger();
        } catch (UnsupportedEncodingException e2) {
            throw new IllegalStateException(e2);
        }
    }

    public boolean isUsingTLS() {
        return this.usingTLS;
    }

    /* access modifiers changed from: package-private */
    public void startTLSReceived(boolean z) throws IOException {
        if (z && this.config.getSecurityMode() == ConnectionConfiguration.SecurityMode.disabled) {
            notifyConnectionError(new IllegalStateException("TLS required by server but not allowed by connection configuration"));
        } else if (this.config.getSecurityMode() != ConnectionConfiguration.SecurityMode.disabled) {
            this.writer.write("<starttls xmlns=\"urn:ietf:params:xml:ns:xmpp-tls\"/>");
            this.writer.flush();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{javax.net.ssl.SSLSocketFactory.createSocket(java.net.Socket, java.lang.String, int, boolean):java.net.Socket throws java.io.IOException}
     arg types: [java.net.Socket, java.lang.String, int, int]
     candidates:
      ClspMth{javax.net.SocketFactory.createSocket(java.lang.String, int, java.net.InetAddress, int):java.net.Socket throws java.io.IOException, java.net.UnknownHostException}
      ClspMth{javax.net.SocketFactory.createSocket(java.net.InetAddress, int, java.net.InetAddress, int):java.net.Socket throws java.io.IOException}
      ClspMth{javax.net.ssl.SSLSocketFactory.createSocket(java.net.Socket, java.lang.String, int, boolean):java.net.Socket throws java.io.IOException} */
    /* access modifiers changed from: package-private */
    public void proceedTLSReceived() throws Exception {
        KeyManager[] keyManagerArr;
        KeyStore instance;
        PasswordCallback passwordCallback;
        SSLContext sSLContext;
        SSLContext customSSLContext = this.config.getCustomSSLContext();
        if (this.config.getCallbackHandler() == null) {
            keyManagerArr = null;
        } else if (customSSLContext == null) {
            if (this.config.getKeystoreType().equals("NONE")) {
                passwordCallback = null;
                instance = null;
            } else if (this.config.getKeystoreType().equals("PKCS11")) {
                try {
                    Provider provider = (Provider) Class.forName("sun.security.pkcs11.SunPKCS11").getConstructor(InputStream.class).newInstance(new ByteArrayInputStream(("name = SmartCard\nlibrary = " + this.config.getPKCS11Library()).getBytes()));
                    Security.addProvider(provider);
                    instance = KeyStore.getInstance("PKCS11", provider);
                    passwordCallback = new PasswordCallback("PKCS11 Password: ", false);
                    this.config.getCallbackHandler().handle(new Callback[]{passwordCallback});
                    instance.load(null, passwordCallback.getPassword());
                } catch (Exception e) {
                    passwordCallback = null;
                    instance = null;
                }
            } else if (this.config.getKeystoreType().equals("Apple")) {
                KeyStore instance2 = KeyStore.getInstance("KeychainStore", "Apple");
                instance2.load(null, null);
                instance = instance2;
                passwordCallback = null;
            } else {
                instance = KeyStore.getInstance(this.config.getKeystoreType());
                try {
                    passwordCallback = new PasswordCallback("Keystore Password: ", false);
                    this.config.getCallbackHandler().handle(new Callback[]{passwordCallback});
                    instance.load(new FileInputStream(this.config.getKeystorePath()), passwordCallback.getPassword());
                } catch (Exception e2) {
                    passwordCallback = null;
                    instance = null;
                }
            }
            KeyManagerFactory instance3 = KeyManagerFactory.getInstance("SunX509");
            if (passwordCallback == null) {
                try {
                    instance3.init(instance, null);
                } catch (NullPointerException e3) {
                    keyManagerArr = null;
                }
            } else {
                instance3.init(instance, passwordCallback.getPassword());
                passwordCallback.clearPassword();
            }
            keyManagerArr = instance3.getKeyManagers();
        } else {
            keyManagerArr = null;
        }
        if (customSSLContext == null) {
            sSLContext = SSLContext.getInstance("TLS");
            sSLContext.init(keyManagerArr, null, new SecureRandom());
        } else {
            sSLContext = customSSLContext;
        }
        Socket socket2 = this.socket;
        this.socket = sSLContext.getSocketFactory().createSocket(socket2, socket2.getInetAddress().getHostAddress(), socket2.getPort(), true);
        initReaderAndWriter();
        SSLSocket sSLSocket = (SSLSocket) this.socket;
        try {
            sSLSocket.startHandshake();
            HostnameVerifier hostnameVerifier = getConfiguration().getHostnameVerifier();
            if (hostnameVerifier == null || hostnameVerifier.verify(getServiceName(), sSLSocket.getSession())) {
                this.usingTLS = true;
                this.packetWriter.setWriter(this.writer);
                this.packetWriter.openStream();
                return;
            }
            throw new CertificateException("Hostname verification of certificate failed. Certificate does not authenticate " + getServiceName());
        } catch (IOException e4) {
            setConnectionException(e4);
            throw e4;
        }
    }

    /* access modifiers changed from: package-private */
    public void setAvailableCompressionMethods(Collection<String> collection) {
        this.compressionMethods = collection;
    }

    private XMPPInputOutputStream maybeGetCompressionHandler() {
        if (this.compressionMethods != null) {
            for (XMPPInputOutputStream next : SmackConfiguration.getCompresionHandlers()) {
                if (this.compressionMethods.contains(next.getCompressionMethod())) {
                    return next;
                }
            }
        }
        return null;
    }

    public boolean isUsingCompression() {
        return this.compressionHandler != null && this.serverAckdCompression;
    }

    private boolean useCompression() throws IOException {
        if (this.authenticated) {
            throw new IllegalStateException("Compression should be negotiated before authentication.");
        }
        XMPPInputOutputStream maybeGetCompressionHandler = maybeGetCompressionHandler();
        this.compressionHandler = maybeGetCompressionHandler;
        if (maybeGetCompressionHandler == null) {
            return false;
        }
        synchronized (this.compressionLock) {
            requestStreamCompression(this.compressionHandler.getCompressionMethod());
            try {
                this.compressionLock.wait(getPacketReplyTimeout());
            } catch (InterruptedException e) {
            }
        }
        return isUsingCompression();
    }

    private void requestStreamCompression(String str) throws IOException {
        this.writer.write("<compress xmlns='http://jabber.org/protocol/compress'>");
        this.writer.write("<method>" + str + "</method></compress>");
        this.writer.flush();
    }

    /* access modifiers changed from: package-private */
    public void startStreamCompression() throws IOException {
        this.serverAckdCompression = true;
        initReaderAndWriter();
        this.packetWriter.setWriter(this.writer);
        this.packetWriter.openStream();
        streamCompressionNegotiationDone();
    }

    /* access modifiers changed from: package-private */
    public void streamCompressionNegotiationDone() {
        synchronized (this.compressionLock) {
            this.compressionLock.notify();
        }
    }

    /* access modifiers changed from: protected */
    public void connectInternal() throws SmackException, IOException, XMPPException {
        connectUsingConfiguration(this.config);
        if (this.connected) {
            callConnectionConnectedListener();
        }
        if (this.connected && this.wasAuthenticated) {
            if (isAnonymous()) {
                loginAnonymously();
            } else {
                login(this.config.getUsername(), this.config.getPassword(), this.config.getResource());
            }
            notifyReconnection();
        }
    }

    /* access modifiers changed from: package-private */
    public synchronized void notifyConnectionError(Exception exc) {
        if ((this.packetReader != null && !this.packetReader.done) || (this.packetWriter != null && !this.packetWriter.done)) {
            shutdown();
            callConnectionClosedOnErrorListener(exc);
        }
    }

    /* access modifiers changed from: protected */
    public void processPacket(Packet packet) {
        super.processPacket(packet);
    }

    /* access modifiers changed from: protected */
    public Reader getReader() {
        return super.getReader();
    }

    /* access modifiers changed from: protected */
    public Writer getWriter() {
        return super.getWriter();
    }

    /* access modifiers changed from: protected */
    public void throwConnectionExceptionOrNoResponse() throws IOException, SmackException.NoResponseException {
        super.throwConnectionExceptionOrNoResponse();
    }

    /* access modifiers changed from: protected */
    public void setServiceName(String str) {
        super.setServiceName(str);
    }

    /* access modifiers changed from: protected */
    public void serverRequiresBinding() {
        super.serverRequiresBinding();
    }

    /* access modifiers changed from: protected */
    public void setServiceCapsNode(String str) {
        super.setServiceCapsNode(str);
    }

    /* access modifiers changed from: protected */
    public void serverSupportsSession() {
        super.serverSupportsSession();
    }

    /* access modifiers changed from: protected */
    public void setRosterVersioningSupported() {
        super.setRosterVersioningSupported();
    }

    /* access modifiers changed from: protected */
    public void serverSupportsAccountCreation() {
        super.serverSupportsAccountCreation();
    }

    /* access modifiers changed from: protected */
    public SASLAuthentication getSASLAuthentication() {
        return super.getSASLAuthentication();
    }

    /* access modifiers changed from: protected */
    public ConnectionConfiguration getConfiguration() {
        return super.getConfiguration();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
     arg types: [java.util.logging.Level, java.lang.String, java.lang.Exception]
     candidates:
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
    private void notifyReconnection() {
        for (ConnectionListener reconnectionSuccessful : getConnectionListeners()) {
            try {
                reconnectionSuccessful.reconnectionSuccessful();
            } catch (Exception e) {
                LOGGER.log(Level.WARNING, "notifyReconnection()", (Throwable) e);
            }
        }
    }
}
