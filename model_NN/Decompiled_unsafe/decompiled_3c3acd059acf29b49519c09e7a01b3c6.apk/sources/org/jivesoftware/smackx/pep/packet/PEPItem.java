package org.jivesoftware.smackx.pep.packet;

import org.jivesoftware.smack.packet.PacketExtension;
import org.jivesoftware.smackx.pubsub.packet.PubSub;
import org.jivesoftware.smackx.xdata.packet.DataForm;

public abstract class PEPItem implements PacketExtension {
    String id;

    /* access modifiers changed from: package-private */
    public abstract String getItemDetailsXML();

    /* access modifiers changed from: package-private */
    public abstract String getNode();

    public PEPItem(String str) {
        this.id = str;
    }

    public String getElementName() {
        return DataForm.Item.ELEMENT;
    }

    public String getNamespace() {
        return PubSub.NAMESPACE;
    }

    public String toXML() {
        StringBuilder sb = new StringBuilder();
        sb.append("<").append(getElementName()).append(" id=\"").append(this.id).append("\">");
        sb.append(getItemDetailsXML());
        sb.append("</").append(getElementName()).append(">");
        return sb.toString();
    }
}
