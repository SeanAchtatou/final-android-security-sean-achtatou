package org.jivesoftware.smackx.caps.packet;

import org.jivesoftware.smack.packet.PacketExtension;
import org.jivesoftware.smack.util.XmlStringBuilder;
import org.jivesoftware.smackx.caps.EntityCapsManager;

public class CapsExtension implements PacketExtension {
    private final String hash;
    private final String node;
    private final String ver;

    public CapsExtension(String str, String str2, String str3) {
        this.node = str;
        this.ver = str2;
        this.hash = str3;
    }

    public String getElementName() {
        return EntityCapsManager.ELEMENT;
    }

    public String getNamespace() {
        return EntityCapsManager.NAMESPACE;
    }

    public String getNode() {
        return this.node;
    }

    public String getVer() {
        return this.ver;
    }

    public String getHash() {
        return this.hash;
    }

    public CharSequence toXML() {
        XmlStringBuilder xmlStringBuilder = new XmlStringBuilder(this);
        xmlStringBuilder.attribute("hash", this.hash).attribute("node", this.node).attribute("ver", this.ver);
        xmlStringBuilder.closeEmptyElement();
        return xmlStringBuilder;
    }
}
