package org.jivesoftware.smackx.muc;

import org.jivesoftware.smack.PacketInterceptor;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.packet.PacketExtension;
import org.jivesoftware.smack.packet.Presence;

public class DeafOccupantInterceptor implements PacketInterceptor {
    public void interceptPacket(Packet packet) {
        Presence presence = (Presence) packet;
        if (Presence.Type.available == presence.getType() && presence.getExtension("x", "http://jabber.org/protocol/muc") != null) {
            packet.addExtension(new DeafExtension());
        }
    }

    private static class DeafExtension implements PacketExtension {
        private DeafExtension() {
        }

        public String getElementName() {
            return "x";
        }

        public String getNamespace() {
            return "http://jivesoftware.org/protocol/muc";
        }

        public String toXML() {
            StringBuilder sb = new StringBuilder();
            sb.append("<").append(getElementName()).append(" xmlns=\"").append(getNamespace()).append("\">");
            sb.append("<deaf-occupant/>");
            sb.append("</").append(getElementName()).append(">");
            return sb.toString();
        }
    }
}
