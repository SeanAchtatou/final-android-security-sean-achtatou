package org.jivesoftware.smackx.chatstates.packet;

import org.jivesoftware.smack.packet.PacketExtension;
import org.jivesoftware.smack.provider.PacketExtensionProvider;
import org.jivesoftware.smackx.chatstates.ChatState;
import org.jivesoftware.smackx.chatstates.ChatStateManager;
import org.xmlpull.v1.XmlPullParser;

public class ChatStateExtension implements PacketExtension {
    private ChatState state;

    public ChatStateExtension(ChatState chatState) {
        this.state = chatState;
    }

    public String getElementName() {
        return this.state.name();
    }

    public String getNamespace() {
        return ChatStateManager.NAMESPACE;
    }

    public String toXML() {
        return "<" + getElementName() + " xmlns=\"" + getNamespace() + "\" />";
    }

    public static class Provider implements PacketExtensionProvider {
        public PacketExtension parseExtension(XmlPullParser xmlPullParser) throws Exception {
            ChatState chatState;
            try {
                chatState = ChatState.valueOf(xmlPullParser.getName());
            } catch (Exception e) {
                chatState = ChatState.active;
            }
            return new ChatStateExtension(chatState);
        }
    }
}
