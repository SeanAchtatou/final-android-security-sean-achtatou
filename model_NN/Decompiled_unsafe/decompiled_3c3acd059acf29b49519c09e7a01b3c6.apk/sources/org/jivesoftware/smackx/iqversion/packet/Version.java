package org.jivesoftware.smackx.iqversion.packet;

import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.util.StringUtils;

public class Version extends IQ {
    public static final String NAMESPACE = "jabber:iq:version";
    private String name;
    private String os;
    private String version;

    public Version(String str, String str2, String str3) {
        setType(IQ.Type.RESULT);
        this.name = str;
        this.version = str2;
        this.os = str3;
    }

    public Version(Version version2) {
        this(version2.name, version2.version, version2.os);
    }

    public String getName() {
        return this.name;
    }

    public void setName(String str) {
        this.name = str;
    }

    public String getVersion() {
        return this.version;
    }

    public void setVersion(String str) {
        this.version = str;
    }

    public String getOs() {
        return this.os;
    }

    public void setOs(String str) {
        this.os = str;
    }

    public String getChildElementXML() {
        StringBuilder sb = new StringBuilder();
        sb.append("<query xmlns=\"");
        sb.append(NAMESPACE);
        sb.append("\">");
        if (this.name != null) {
            sb.append("<name>").append(StringUtils.escapeForXML(this.name)).append("</name>");
        }
        if (this.version != null) {
            sb.append("<version>").append(StringUtils.escapeForXML(this.version)).append("</version>");
        }
        if (this.os != null) {
            sb.append("<os>").append(StringUtils.escapeForXML(this.os)).append("</os>");
        }
        sb.append("</query>");
        return sb.toString();
    }
}
