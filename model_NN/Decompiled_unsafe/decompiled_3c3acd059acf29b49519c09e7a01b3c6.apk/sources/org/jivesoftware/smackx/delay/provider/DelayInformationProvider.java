package org.jivesoftware.smackx.delay.provider;

import java.text.ParseException;
import java.util.Date;
import org.jivesoftware.smack.packet.PacketExtension;
import org.jivesoftware.smack.provider.PacketExtensionProvider;
import org.jivesoftware.smack.util.XmppDateTime;
import org.jivesoftware.smackx.delay.packet.DelayInformation;
import org.jivesoftware.smackx.privacy.packet.PrivacyItem;
import org.xmlpull.v1.XmlPullParser;

public class DelayInformationProvider implements PacketExtensionProvider {
    public PacketExtension parseExtension(XmlPullParser xmlPullParser) throws Exception {
        Date date;
        String str = null;
        try {
            date = XmppDateTime.parseDate(xmlPullParser.getAttributeValue("", "stamp"));
        } catch (ParseException e) {
            date = 0 == 0 ? new Date(0) : null;
        }
        DelayInformation delayInformation = new DelayInformation(date);
        delayInformation.setFrom(xmlPullParser.getAttributeValue("", PrivacyItem.SUBSCRIPTION_FROM));
        String nextText = xmlPullParser.nextText();
        if (!"".equals(nextText)) {
            str = nextText;
        }
        delayInformation.setReason(str);
        return delayInformation;
    }
}
