package org.jivesoftware.smackx.iqlast;

import java.util.Map;
import java.util.WeakHashMap;
import org.jivesoftware.smack.ConnectionCreationListener;
import org.jivesoftware.smack.Manager;
import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.filter.AndFilter;
import org.jivesoftware.smack.filter.IQTypeFilter;
import org.jivesoftware.smack.filter.PacketFilter;
import org.jivesoftware.smack.filter.PacketTypeFilter;
import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smackx.disco.ServiceDiscoveryManager;
import org.jivesoftware.smackx.iqlast.packet.LastActivity;

public class LastActivityManager extends Manager {
    private static final PacketFilter IQ_GET_LAST_FILTER = new AndFilter(new IQTypeFilter(IQ.Type.GET), new PacketTypeFilter(LastActivity.class));
    private static boolean enabledPerDefault = true;
    private static final Map<XMPPConnection, LastActivityManager> instances = new WeakHashMap();
    /* access modifiers changed from: private */
    public boolean enabled = false;
    private volatile long lastMessageSent;

    static {
        XMPPConnection.addConnectionCreationListener(new ConnectionCreationListener() {
            public void connectionCreated(XMPPConnection xMPPConnection) {
                LastActivityManager.getInstanceFor(xMPPConnection);
            }
        });
    }

    public static void setEnabledPerDefault(boolean z) {
        enabledPerDefault = z;
    }

    public static synchronized LastActivityManager getInstanceFor(XMPPConnection xMPPConnection) {
        LastActivityManager lastActivityManager;
        synchronized (LastActivityManager.class) {
            lastActivityManager = instances.get(xMPPConnection);
            if (lastActivityManager == null) {
                lastActivityManager = new LastActivityManager(xMPPConnection);
            }
        }
        return lastActivityManager;
    }

    private LastActivityManager(XMPPConnection xMPPConnection) {
        super(xMPPConnection);
        xMPPConnection.addPacketSendingListener(new PacketListener() {
            public void processPacket(Packet packet) {
                Presence.Mode mode = ((Presence) packet).getMode();
                if (mode != null) {
                    switch (AnonymousClass5.$SwitchMap$org$jivesoftware$smack$packet$Presence$Mode[mode.ordinal()]) {
                        case 1:
                        case 2:
                            LastActivityManager.this.resetIdleTime();
                            return;
                        default:
                            return;
                    }
                }
            }
        }, PacketTypeFilter.PRESENCE);
        xMPPConnection.addPacketSendingListener(new PacketListener() {
            public void processPacket(Packet packet) {
                if (((Message) packet).getType() != Message.Type.error) {
                    LastActivityManager.this.resetIdleTime();
                }
            }
        }, PacketTypeFilter.MESSAGE);
        xMPPConnection.addPacketListener(new PacketListener() {
            public void processPacket(Packet packet) throws SmackException.NotConnectedException {
                if (LastActivityManager.this.enabled) {
                    LastActivity lastActivity = new LastActivity();
                    lastActivity.setType(IQ.Type.RESULT);
                    lastActivity.setTo(packet.getFrom());
                    lastActivity.setFrom(packet.getTo());
                    lastActivity.setPacketID(packet.getPacketID());
                    lastActivity.setLastActivity(LastActivityManager.this.getIdleTime());
                    LastActivityManager.this.connection().sendPacket(lastActivity);
                }
            }
        }, IQ_GET_LAST_FILTER);
        if (enabledPerDefault) {
            enable();
        }
        resetIdleTime();
        instances.put(xMPPConnection, this);
    }

    /* renamed from: org.jivesoftware.smackx.iqlast.LastActivityManager$5  reason: invalid class name */
    static /* synthetic */ class AnonymousClass5 {
        static final /* synthetic */ int[] $SwitchMap$org$jivesoftware$smack$packet$Presence$Mode = new int[Presence.Mode.values().length];

        static {
            try {
                $SwitchMap$org$jivesoftware$smack$packet$Presence$Mode[Presence.Mode.available.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                $SwitchMap$org$jivesoftware$smack$packet$Presence$Mode[Presence.Mode.chat.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
        }
    }

    public synchronized void enable() {
        ServiceDiscoveryManager.getInstanceFor(connection()).addFeature(LastActivity.NAMESPACE);
        this.enabled = true;
    }

    public synchronized void disable() {
        ServiceDiscoveryManager.getInstanceFor(connection()).removeFeature(LastActivity.NAMESPACE);
        this.enabled = false;
    }

    /* access modifiers changed from: private */
    public void resetIdleTime() {
        this.lastMessageSent = System.currentTimeMillis();
    }

    /* access modifiers changed from: private */
    public long getIdleTime() {
        return (System.currentTimeMillis() - this.lastMessageSent) / 1000;
    }

    public LastActivity getLastActivity(String str) throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        return (LastActivity) connection().createPacketCollectorAndSend(new LastActivity(str)).nextResultOrThrow();
    }

    public boolean isLastActivitySupported(String str) throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        return ServiceDiscoveryManager.getInstanceFor(connection()).supportsFeature(str, LastActivity.NAMESPACE);
    }
}
