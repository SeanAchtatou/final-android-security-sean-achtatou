package org.jivesoftware.smackx.privacy.provider;

import java.util.ArrayList;
import org.jivesoftware.smack.packet.DefaultPacketExtension;
import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.provider.IQProvider;
import org.jivesoftware.smackx.amp.packet.AMPExtension;
import org.jivesoftware.smackx.privacy.packet.Privacy;
import org.jivesoftware.smackx.privacy.packet.PrivacyItem;
import org.jivesoftware.smackx.xdata.packet.DataForm;
import org.xmlpull.v1.XmlPullParser;

public class PrivacyProvider implements IQProvider {
    public IQ parseIQ(XmlPullParser xmlPullParser) throws Exception {
        Privacy privacy = new Privacy();
        privacy.addExtension(new DefaultPacketExtension(xmlPullParser.getName(), xmlPullParser.getNamespace()));
        boolean z = false;
        while (!z) {
            int next = xmlPullParser.next();
            if (next == 2) {
                if (xmlPullParser.getName().equals("active")) {
                    String attributeValue = xmlPullParser.getAttributeValue("", "name");
                    if (attributeValue == null) {
                        privacy.setDeclineActiveList(true);
                    } else {
                        privacy.setActiveName(attributeValue);
                    }
                } else if (xmlPullParser.getName().equals("default")) {
                    String attributeValue2 = xmlPullParser.getAttributeValue("", "name");
                    if (attributeValue2 == null) {
                        privacy.setDeclineDefaultList(true);
                    } else {
                        privacy.setDefaultName(attributeValue2);
                    }
                } else if (xmlPullParser.getName().equals("list")) {
                    parseList(xmlPullParser, privacy);
                }
            } else if (next == 3 && xmlPullParser.getName().equals("query")) {
                z = true;
            }
        }
        return privacy;
    }

    public void parseList(XmlPullParser xmlPullParser, Privacy privacy) throws Exception {
        boolean z = false;
        String attributeValue = xmlPullParser.getAttributeValue("", "name");
        ArrayList arrayList = new ArrayList();
        while (!z) {
            int next = xmlPullParser.next();
            if (next == 2) {
                if (xmlPullParser.getName().equals(DataForm.Item.ELEMENT)) {
                    arrayList.add(parseItem(xmlPullParser));
                }
            } else if (next == 3 && xmlPullParser.getName().equals("list")) {
                z = true;
            }
        }
        privacy.setPrivacyList(attributeValue, arrayList);
    }

    public PrivacyItem parseItem(XmlPullParser xmlPullParser) throws Exception {
        boolean z;
        String attributeValue = xmlPullParser.getAttributeValue("", AMPExtension.Action.ATTRIBUTE_NAME);
        String attributeValue2 = xmlPullParser.getAttributeValue("", "order");
        String attributeValue3 = xmlPullParser.getAttributeValue("", "type");
        if ("allow".equalsIgnoreCase(attributeValue)) {
            z = true;
        } else if ("deny".equalsIgnoreCase(attributeValue)) {
            z = false;
        } else {
            z = true;
        }
        int parseInt = Integer.parseInt(attributeValue2);
        if (attributeValue3 == null) {
            return new PrivacyItem(z, parseInt);
        }
        PrivacyItem privacyItem = new PrivacyItem(PrivacyItem.Type.valueOf(attributeValue3), xmlPullParser.getAttributeValue("", "value"), z, parseInt);
        boolean z2 = false;
        while (!z2) {
            int next = xmlPullParser.next();
            if (next == 2) {
                if (xmlPullParser.getName().equals("iq")) {
                    privacyItem.setFilterIQ(true);
                }
                if (xmlPullParser.getName().equals("message")) {
                    privacyItem.setFilterMessage(true);
                }
                if (xmlPullParser.getName().equals("presence-in")) {
                    privacyItem.setFilterPresenceIn(true);
                }
                if (xmlPullParser.getName().equals("presence-out")) {
                    privacyItem.setFilterPresenceOut(true);
                }
            } else if (next == 3 && xmlPullParser.getName().equals(DataForm.Item.ELEMENT)) {
                z2 = true;
            }
        }
        return privacyItem;
    }
}
