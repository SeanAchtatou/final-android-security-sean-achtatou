package org.jivesoftware.smackx.search;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smackx.search.ReportedData;
import org.jivesoftware.smackx.xdata.Form;
import org.jivesoftware.smackx.xdata.FormField;
import org.jivesoftware.smackx.xdata.packet.DataForm;
import org.xmlpull.v1.XmlPullParser;

class SimpleUserSearch extends IQ {
    private ReportedData data;
    private Form form;

    SimpleUserSearch() {
    }

    public void setForm(Form form2) {
        this.form = form2;
    }

    public ReportedData getReportedData() {
        return this.data;
    }

    public String getChildElementXML() {
        return "<query xmlns=\"jabber:iq:search\">" + getItemsToSearch() + "</query>";
    }

    private String getItemsToSearch() {
        StringBuilder sb = new StringBuilder();
        if (this.form == null) {
            this.form = Form.getFormFrom(this);
        }
        if (this.form == null) {
            return "";
        }
        for (FormField next : this.form.getFields()) {
            String variable = next.getVariable();
            String singleValue = getSingleValue(next);
            if (singleValue.trim().length() > 0) {
                sb.append("<").append(variable).append(">").append(singleValue).append("</").append(variable).append(">");
            }
        }
        return sb.toString();
    }

    private static String getSingleValue(FormField formField) {
        List<String> values = formField.getValues();
        if (values.isEmpty()) {
            return "";
        }
        return values.get(0);
    }

    /* access modifiers changed from: protected */
    public void parseItems(XmlPullParser xmlPullParser) throws Exception {
        ArrayList arrayList;
        boolean z;
        boolean z2;
        ReportedData reportedData = new ReportedData();
        reportedData.addColumn(new ReportedData.Column("JID", "jid", FormField.TYPE_TEXT_SINGLE));
        ArrayList arrayList2 = new ArrayList();
        boolean z3 = false;
        while (!z3) {
            if (xmlPullParser.getAttributeCount() > 0) {
                String attributeValue = xmlPullParser.getAttributeValue("", "jid");
                ArrayList arrayList3 = new ArrayList();
                arrayList3.add(attributeValue);
                arrayList2.add(new ReportedData.Field("jid", arrayList3));
            }
            int next = xmlPullParser.next();
            if (next == 2 && xmlPullParser.getName().equals(DataForm.Item.ELEMENT)) {
                arrayList = new ArrayList();
                z = z3;
            } else if (next == 3 && xmlPullParser.getName().equals(DataForm.Item.ELEMENT)) {
                reportedData.addRow(new ReportedData.Row(arrayList2));
                arrayList = arrayList2;
                z = z3;
            } else if (next == 2) {
                String name = xmlPullParser.getName();
                String nextText = xmlPullParser.nextText();
                ArrayList arrayList4 = new ArrayList();
                arrayList4.add(nextText);
                arrayList2.add(new ReportedData.Field(name, arrayList4));
                Iterator<ReportedData.Column> it = reportedData.getColumns().iterator();
                while (true) {
                    if (it.hasNext()) {
                        if (it.next().getVariable().equals(name)) {
                            z2 = true;
                            break;
                        }
                    } else {
                        z2 = false;
                        break;
                    }
                }
                if (!z2) {
                    reportedData.addColumn(new ReportedData.Column(name, name, FormField.TYPE_TEXT_SINGLE));
                }
                arrayList = arrayList2;
                z = z3;
            } else if (next != 3 || !xmlPullParser.getName().equals("query")) {
                arrayList = arrayList2;
                z = z3;
            } else {
                arrayList = arrayList2;
                z = true;
            }
            z3 = z;
            arrayList2 = arrayList;
        }
        this.data = reportedData;
    }
}
