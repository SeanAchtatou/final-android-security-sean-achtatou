package org.jivesoftware.smackx.si.provider;

import java.text.ParseException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.provider.IQProvider;
import org.jivesoftware.smack.util.XmppDateTime;
import org.jivesoftware.smackx.si.packet.StreamInitiation;
import org.jivesoftware.smackx.xdata.packet.DataForm;
import org.jivesoftware.smackx.xdata.provider.DataFormProvider;
import org.xmlpull.v1.XmlPullParser;

public class StreamInitiationProvider implements IQProvider {
    private static final Logger LOGGER = Logger.getLogger(StreamInitiationProvider.class.getName());

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
     arg types: [java.util.logging.Level, java.lang.String, java.lang.NumberFormatException]
     candidates:
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
    public IQ parseIQ(XmlPullParser xmlPullParser) throws Exception {
        String attributeValue = xmlPullParser.getAttributeValue("", "id");
        String attributeValue2 = xmlPullParser.getAttributeValue("", "mime-type");
        StreamInitiation streamInitiation = new StreamInitiation();
        DataFormProvider dataFormProvider = new DataFormProvider();
        String str = null;
        String str2 = null;
        boolean z = false;
        String str3 = null;
        String str4 = null;
        String str5 = null;
        boolean z2 = false;
        DataForm dataForm = null;
        while (!z) {
            int next = xmlPullParser.next();
            String name = xmlPullParser.getName();
            String namespace = xmlPullParser.getNamespace();
            if (next == 2) {
                if (name.equals("file")) {
                    String attributeValue3 = xmlPullParser.getAttributeValue("", "name");
                    String attributeValue4 = xmlPullParser.getAttributeValue("", "size");
                    str4 = xmlPullParser.getAttributeValue("", "hash");
                    str = attributeValue4;
                    str2 = attributeValue3;
                    str3 = xmlPullParser.getAttributeValue("", "date");
                } else if (name.equals("desc")) {
                    str5 = xmlPullParser.nextText();
                } else if (name.equals("range")) {
                    z2 = true;
                } else if (name.equals("x") && namespace.equals(DataForm.NAMESPACE)) {
                    dataForm = (DataForm) dataFormProvider.parseExtension(xmlPullParser);
                }
            } else if (next == 3) {
                if (name.equals("si")) {
                    z = true;
                } else if (name.equals("file")) {
                    long j = 0;
                    if (!(str == null || str.trim().length() == 0)) {
                        try {
                            j = Long.parseLong(str);
                        } catch (NumberFormatException e) {
                            LOGGER.log(Level.SEVERE, "Failed to parse file size from " + 0L, (Throwable) e);
                        }
                    }
                    Date date = new Date();
                    if (str3 != null) {
                        try {
                            date = XmppDateTime.parseDate(str3);
                        } catch (ParseException e2) {
                        }
                    }
                    StreamInitiation.File file = new StreamInitiation.File(str2, j);
                    file.setHash(str4);
                    file.setDate(date);
                    file.setDesc(str5);
                    file.setRanged(z2);
                    streamInitiation.setFile(file);
                }
            }
        }
        streamInitiation.setSessionID(attributeValue);
        streamInitiation.setMimeType(attributeValue2);
        streamInitiation.setFeatureNegotiationForm(dataForm);
        return streamInitiation;
    }
}
