package org.jivesoftware.smackx.si.packet;

import java.util.Date;
import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.packet.PacketExtension;
import org.jivesoftware.smack.util.StringUtils;
import org.jivesoftware.smack.util.XmppDateTime;
import org.jivesoftware.smackx.xdata.packet.DataForm;

public class StreamInitiation extends IQ {
    private Feature featureNegotiation;
    private File file;
    private String id;
    private String mimeType;

    public void setSessionID(String str) {
        this.id = str;
    }

    public String getSessionID() {
        return this.id;
    }

    public void setMimeType(String str) {
        this.mimeType = str;
    }

    public String getMimeType() {
        return this.mimeType;
    }

    public void setFile(File file2) {
        this.file = file2;
    }

    public File getFile() {
        return this.file;
    }

    public void setFeatureNegotiationForm(DataForm dataForm) {
        this.featureNegotiation = new Feature(dataForm);
    }

    public DataForm getFeatureNegotiationForm() {
        return this.featureNegotiation.getData();
    }

    public String getChildElementXML() {
        StringBuilder sb = new StringBuilder();
        if (getType().equals(IQ.Type.SET)) {
            sb.append("<si xmlns=\"http://jabber.org/protocol/si\" ");
            if (getSessionID() != null) {
                sb.append("id=\"").append(getSessionID()).append("\" ");
            }
            if (getMimeType() != null) {
                sb.append("mime-type=\"").append(getMimeType()).append("\" ");
            }
            sb.append("profile=\"http://jabber.org/protocol/si/profile/file-transfer\">");
            String xml = this.file.toXML();
            if (xml != null) {
                sb.append(xml);
            }
        } else if (getType().equals(IQ.Type.RESULT)) {
            sb.append("<si xmlns=\"http://jabber.org/protocol/si\">");
        } else {
            throw new IllegalArgumentException("IQ Type not understood");
        }
        if (this.featureNegotiation != null) {
            sb.append(this.featureNegotiation.toXML());
        }
        sb.append("</si>");
        return sb.toString();
    }

    public static class File implements PacketExtension {
        private Date date;
        private String desc;
        private String hash;
        private boolean isRanged;
        private final String name;
        private final long size;

        public File(String str, long j) {
            if (str == null) {
                throw new NullPointerException("name cannot be null");
            }
            this.name = str;
            this.size = j;
        }

        public String getName() {
            return this.name;
        }

        public long getSize() {
            return this.size;
        }

        public void setHash(String str) {
            this.hash = str;
        }

        public String getHash() {
            return this.hash;
        }

        public void setDate(Date date2) {
            this.date = date2;
        }

        public Date getDate() {
            return this.date;
        }

        public void setDesc(String str) {
            this.desc = str;
        }

        public String getDesc() {
            return this.desc;
        }

        public void setRanged(boolean z) {
            this.isRanged = z;
        }

        public boolean isRanged() {
            return this.isRanged;
        }

        public String getElementName() {
            return "file";
        }

        public String getNamespace() {
            return "http://jabber.org/protocol/si/profile/file-transfer";
        }

        public String toXML() {
            StringBuilder sb = new StringBuilder();
            sb.append("<").append(getElementName()).append(" xmlns=\"").append(getNamespace()).append("\" ");
            if (getName() != null) {
                sb.append("name=\"").append(StringUtils.escapeForXML(getName())).append("\" ");
            }
            if (getSize() > 0) {
                sb.append("size=\"").append(getSize()).append("\" ");
            }
            if (getDate() != null) {
                sb.append("date=\"").append(XmppDateTime.formatXEP0082Date(this.date)).append("\" ");
            }
            if (getHash() != null) {
                sb.append("hash=\"").append(getHash()).append("\" ");
            }
            if ((this.desc == null || this.desc.length() <= 0) && !this.isRanged) {
                sb.append("/>");
            } else {
                sb.append(">");
                if (getDesc() != null && this.desc.length() > 0) {
                    sb.append("<desc>").append(StringUtils.escapeForXML(getDesc())).append("</desc>");
                }
                if (isRanged()) {
                    sb.append("<range/>");
                }
                sb.append("</").append(getElementName()).append(">");
            }
            return sb.toString();
        }
    }

    public class Feature implements PacketExtension {
        private final DataForm data;

        public Feature(DataForm dataForm) {
            this.data = dataForm;
        }

        public DataForm getData() {
            return this.data;
        }

        public String getNamespace() {
            return "http://jabber.org/protocol/feature-neg";
        }

        public String getElementName() {
            return "feature";
        }

        public String toXML() {
            return "<feature xmlns=\"http://jabber.org/protocol/feature-neg\">" + ((CharSequence) this.data.toXML()) + "</feature>";
        }
    }
}
