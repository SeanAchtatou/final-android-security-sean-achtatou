package org.jivesoftware.smackx.pubsub;

import org.jivesoftware.smack.packet.PacketExtension;

public class PayloadItem<E extends PacketExtension> extends Item {
    private E payload;

    public PayloadItem(E e) {
        if (e == null) {
            throw new IllegalArgumentException("payload cannot be 'null'");
        }
        this.payload = e;
    }

    public PayloadItem(String str, E e) {
        super(str);
        if (e == null) {
            throw new IllegalArgumentException("payload cannot be 'null'");
        }
        this.payload = e;
    }

    public PayloadItem(String str, String str2, E e) {
        super(str, str2);
        if (e == null) {
            throw new IllegalArgumentException("payload cannot be 'null'");
        }
        this.payload = e;
    }

    public E getPayload() {
        return this.payload;
    }

    public String toXML() {
        StringBuilder sb = new StringBuilder("<item");
        if (getId() != null) {
            sb.append(" id='");
            sb.append(getId());
            sb.append("'");
        }
        if (getNode() != null) {
            sb.append(" node='");
            sb.append(getNode());
            sb.append("'");
        }
        sb.append(">");
        sb.append(this.payload.toXML());
        sb.append("</item>");
        return sb.toString();
    }

    public String toString() {
        return getClass().getName() + " | Content [" + toXML() + "]";
    }
}
