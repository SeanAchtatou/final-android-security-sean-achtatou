package org.jivesoftware.smackx.pubsub.provider;

import org.jivesoftware.smack.provider.PacketExtensionProvider;

public class SubscriptionProvider implements PacketExtensionProvider {
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0047  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public org.jivesoftware.smack.packet.PacketExtension parseExtension(org.xmlpull.v1.XmlPullParser r11) throws java.lang.Exception {
        /*
            r10 = this;
            r9 = 3
            r8 = 2
            r4 = 0
            java.lang.String r0 = "jid"
            java.lang.String r1 = r11.getAttributeValue(r4, r0)
            java.lang.String r0 = "node"
            java.lang.String r2 = r11.getAttributeValue(r4, r0)
            java.lang.String r0 = "subid"
            java.lang.String r3 = r11.getAttributeValue(r4, r0)
            java.lang.String r0 = "subscription"
            java.lang.String r6 = r11.getAttributeValue(r4, r0)
            r0 = 0
            int r5 = r11.next()
            if (r5 != r8) goto L_0x004f
            java.lang.String r5 = r11.getName()
            java.lang.String r7 = "subscribe-options"
            boolean r5 = r5.equals(r7)
            if (r5 == 0) goto L_0x004f
            int r5 = r11.next()
            if (r5 != r8) goto L_0x0041
            java.lang.String r5 = r11.getName()
            java.lang.String r7 = "required"
            boolean r5 = r5.equals(r7)
            if (r5 == 0) goto L_0x0041
            r0 = 1
        L_0x0041:
            int r5 = r11.next()
            if (r5 == r9) goto L_0x004f
            java.lang.String r5 = r11.getName()
            java.lang.String r7 = "subscribe-options"
            if (r5 != r7) goto L_0x0041
        L_0x004f:
            r5 = r0
        L_0x0050:
            int r0 = r11.getEventType()
            if (r0 == r9) goto L_0x005a
            r11.next()
            goto L_0x0050
        L_0x005a:
            org.jivesoftware.smackx.pubsub.Subscription r0 = new org.jivesoftware.smackx.pubsub.Subscription
            if (r6 != 0) goto L_0x0062
        L_0x005e:
            r0.<init>(r1, r2, r3, r4, r5)
            return r0
        L_0x0062:
            org.jivesoftware.smackx.pubsub.Subscription$State r4 = org.jivesoftware.smackx.pubsub.Subscription.State.valueOf(r6)
            goto L_0x005e
        */
        throw new UnsupportedOperationException("Method not decompiled: org.jivesoftware.smackx.pubsub.provider.SubscriptionProvider.parseExtension(org.xmlpull.v1.XmlPullParser):org.jivesoftware.smack.packet.PacketExtension");
    }
}
