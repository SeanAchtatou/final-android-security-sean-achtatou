package org.jivesoftware.smackx.pubsub;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.filter.OrFilter;
import org.jivesoftware.smack.filter.PacketFilter;
import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.packet.PacketExtension;
import org.jivesoftware.smackx.delay.packet.DelayInformation;
import org.jivesoftware.smackx.disco.packet.DiscoverInfo;
import org.jivesoftware.smackx.pubsub.listener.ItemDeleteListener;
import org.jivesoftware.smackx.pubsub.listener.ItemEventListener;
import org.jivesoftware.smackx.pubsub.listener.NodeConfigListener;
import org.jivesoftware.smackx.pubsub.packet.PubSub;
import org.jivesoftware.smackx.pubsub.packet.PubSubNamespace;
import org.jivesoftware.smackx.pubsub.util.NodeUtils;
import org.jivesoftware.smackx.shim.packet.Header;
import org.jivesoftware.smackx.shim.packet.HeadersExtension;
import org.jivesoftware.smackx.xdata.Form;
import org.jivesoftware.smackx.xdata.packet.DataForm;

public abstract class Node {
    protected XMPPConnection con;
    protected ConcurrentHashMap<NodeConfigListener, PacketListener> configEventToListenerMap = new ConcurrentHashMap<>();
    protected String id;
    protected ConcurrentHashMap<ItemDeleteListener, PacketListener> itemDeleteToListenerMap = new ConcurrentHashMap<>();
    protected ConcurrentHashMap<ItemEventListener<Item>, PacketListener> itemEventToListenerMap = new ConcurrentHashMap<>();
    protected String to;

    Node(XMPPConnection xMPPConnection, String str) {
        this.con = xMPPConnection;
        this.id = str;
    }

    /* access modifiers changed from: package-private */
    public void setTo(String str) {
        this.to = str;
    }

    public String getId() {
        return this.id;
    }

    public ConfigureForm getNodeConfiguration() throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        return NodeUtils.getFormFromPacket(sendPubsubPacket(createPubsubPacket(IQ.Type.GET, new NodeExtension(PubSubElementType.CONFIGURE_OWNER, getId()), PubSubNamespace.OWNER)), PubSubElementType.CONFIGURE_OWNER);
    }

    public void sendConfigurationForm(Form form) throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        this.con.createPacketCollectorAndSend(createPubsubPacket(IQ.Type.SET, new FormNode(FormNodeType.CONFIGURE_OWNER, getId(), form), PubSubNamespace.OWNER)).nextResultOrThrow();
    }

    public DiscoverInfo discoverInfo() throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        DiscoverInfo discoverInfo = new DiscoverInfo();
        discoverInfo.setTo(this.to);
        discoverInfo.setNode(getId());
        return (DiscoverInfo) this.con.createPacketCollectorAndSend(discoverInfo).nextResultOrThrow();
    }

    public List<Subscription> getSubscriptions() throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        return getSubscriptions(null, null);
    }

    public List<Subscription> getSubscriptions(List<PacketExtension> list, Collection<PacketExtension> collection) throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        PubSub createPubsubPacket = createPubsubPacket(IQ.Type.GET, new NodeExtension(PubSubElementType.SUBSCRIPTIONS, getId()));
        if (list != null) {
            for (PacketExtension addExtension : list) {
                createPubsubPacket.addExtension(addExtension);
            }
        }
        PubSub pubSub = (PubSub) sendPubsubPacket(createPubsubPacket);
        if (collection != null) {
            collection.addAll(pubSub.getExtensions());
        }
        return ((SubscriptionsExtension) pubSub.getExtension(PubSubElementType.SUBSCRIPTIONS)).getSubscriptions();
    }

    public List<Affiliation> getAffiliations() throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        return getAffiliations(null, null);
    }

    public List<Affiliation> getAffiliations(List<PacketExtension> list, Collection<PacketExtension> collection) throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        PubSub createPubsubPacket = createPubsubPacket(IQ.Type.GET, new NodeExtension(PubSubElementType.AFFILIATIONS, getId()));
        if (list != null) {
            for (PacketExtension addExtension : list) {
                createPubsubPacket.addExtension(addExtension);
            }
        }
        PubSub pubSub = (PubSub) sendPubsubPacket(createPubsubPacket);
        if (collection != null) {
            collection.addAll(pubSub.getExtensions());
        }
        return ((AffiliationsExtension) pubSub.getExtension(PubSubElementType.AFFILIATIONS)).getAffiliations();
    }

    public Subscription subscribe(String str) throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        return (Subscription) ((PubSub) sendPubsubPacket(createPubsubPacket(IQ.Type.SET, new SubscribeExtension(str, getId())))).getExtension(PubSubElementType.SUBSCRIPTION);
    }

    public Subscription subscribe(String str, SubscribeForm subscribeForm) throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        PubSub createPubsubPacket = createPubsubPacket(IQ.Type.SET, new SubscribeExtension(str, getId()));
        createPubsubPacket.addExtension(new FormNode(FormNodeType.OPTIONS, subscribeForm));
        return (Subscription) ((PubSub) PubSubManager.sendPubsubPacket(this.con, createPubsubPacket)).getExtension(PubSubElementType.SUBSCRIPTION);
    }

    public void unsubscribe(String str) throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        unsubscribe(str, null);
    }

    public void unsubscribe(String str, String str2) throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        sendPubsubPacket(createPubsubPacket(IQ.Type.SET, new UnsubscribeExtension(str, getId(), str2)));
    }

    public SubscribeForm getSubscriptionOptions(String str) throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        return getSubscriptionOptions(str, null);
    }

    public SubscribeForm getSubscriptionOptions(String str, String str2) throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        return new SubscribeForm(((FormNode) ((PubSub) sendPubsubPacket(createPubsubPacket(IQ.Type.GET, new OptionsExtension(str, getId(), str2)))).getExtension(PubSubElementType.OPTIONS)).getForm());
    }

    public void addItemEventListener(ItemEventListener itemEventListener) {
        ItemEventTranslator itemEventTranslator = new ItemEventTranslator(itemEventListener);
        this.itemEventToListenerMap.put(itemEventListener, itemEventTranslator);
        this.con.addPacketListener(itemEventTranslator, new EventContentFilter(EventElementType.items.toString(), DataForm.Item.ELEMENT));
    }

    public void removeItemEventListener(ItemEventListener itemEventListener) {
        PacketListener remove = this.itemEventToListenerMap.remove(itemEventListener);
        if (remove != null) {
            this.con.removePacketListener(remove);
        }
    }

    public void addConfigurationListener(NodeConfigListener nodeConfigListener) {
        NodeConfigTranslator nodeConfigTranslator = new NodeConfigTranslator(nodeConfigListener);
        this.configEventToListenerMap.put(nodeConfigListener, nodeConfigTranslator);
        this.con.addPacketListener(nodeConfigTranslator, new EventContentFilter(EventElementType.configuration.toString()));
    }

    public void removeConfigurationListener(NodeConfigListener nodeConfigListener) {
        PacketListener remove = this.configEventToListenerMap.remove(nodeConfigListener);
        if (remove != null) {
            this.con.removePacketListener(remove);
        }
    }

    public void addItemDeleteListener(ItemDeleteListener itemDeleteListener) {
        ItemDeleteTranslator itemDeleteTranslator = new ItemDeleteTranslator(itemDeleteListener);
        this.itemDeleteToListenerMap.put(itemDeleteListener, itemDeleteTranslator);
        EventContentFilter eventContentFilter = new EventContentFilter(EventElementType.items.toString(), "retract");
        EventContentFilter eventContentFilter2 = new EventContentFilter(EventElementType.purge.toString());
        this.con.addPacketListener(itemDeleteTranslator, new OrFilter(eventContentFilter, eventContentFilter2));
    }

    public void removeItemDeleteListener(ItemDeleteListener itemDeleteListener) {
        PacketListener remove = this.itemDeleteToListenerMap.remove(itemDeleteListener);
        if (remove != null) {
            this.con.removePacketListener(remove);
        }
    }

    public String toString() {
        return super.toString() + " " + getClass().getName() + " id: " + this.id;
    }

    /* access modifiers changed from: protected */
    public PubSub createPubsubPacket(IQ.Type type, PacketExtension packetExtension) {
        return createPubsubPacket(type, packetExtension, null);
    }

    /* access modifiers changed from: protected */
    public PubSub createPubsubPacket(IQ.Type type, PacketExtension packetExtension, PubSubNamespace pubSubNamespace) {
        return PubSub.createPubsubPacket(this.to, type, packetExtension, pubSubNamespace);
    }

    /* access modifiers changed from: protected */
    public Packet sendPubsubPacket(PubSub pubSub) throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        return PubSubManager.sendPubsubPacket(this.con, pubSub);
    }

    /* access modifiers changed from: private */
    public static List<String> getSubscriptionIds(Packet packet) {
        HeadersExtension headersExtension = (HeadersExtension) packet.getExtension("headers", HeadersExtension.NAMESPACE);
        if (headersExtension == null) {
            return null;
        }
        ArrayList arrayList = new ArrayList(headersExtension.getHeaders().size());
        for (Header value : headersExtension.getHeaders()) {
            arrayList.add(value.getValue());
        }
        return arrayList;
    }

    public class ItemEventTranslator implements PacketListener {
        private ItemEventListener listener;

        public ItemEventTranslator(ItemEventListener itemEventListener) {
            this.listener = itemEventListener;
        }

        public void processPacket(Packet packet) {
            ItemsExtension itemsExtension = (ItemsExtension) ((EventElement) packet.getExtension("event", PubSubNamespace.EVENT.getXmlns())).getEvent();
            DelayInformation delayInformation = (DelayInformation) packet.getExtension("delay", "urn:xmpp:delay");
            if (delayInformation == null) {
                delayInformation = (DelayInformation) packet.getExtension("x", "jabber:x:delay");
            }
            this.listener.handlePublishedItems(new ItemPublishEvent(itemsExtension.getNode(), itemsExtension.getItems(), Node.getSubscriptionIds(packet), delayInformation == null ? null : delayInformation.getStamp()));
        }
    }

    public class ItemDeleteTranslator implements PacketListener {
        private ItemDeleteListener listener;

        public ItemDeleteTranslator(ItemDeleteListener itemDeleteListener) {
            this.listener = itemDeleteListener;
        }

        public void processPacket(Packet packet) {
            EventElement eventElement = (EventElement) packet.getExtension("event", PubSubNamespace.EVENT.getXmlns());
            if (eventElement.getExtensions().get(0).getElementName().equals(PubSubElementType.PURGE_EVENT.getElementName())) {
                this.listener.handlePurge();
                return;
            }
            ItemsExtension itemsExtension = (ItemsExtension) eventElement.getEvent();
            List<? extends PacketExtension> items = itemsExtension.getItems();
            ArrayList arrayList = new ArrayList(items.size());
            Iterator<? extends PacketExtension> it = items.iterator();
            while (it.hasNext()) {
                arrayList.add(((RetractItem) it.next()).getId());
            }
            this.listener.handleDeletedItems(new ItemDeleteEvent(itemsExtension.getNode(), arrayList, Node.getSubscriptionIds(packet)));
        }
    }

    public class NodeConfigTranslator implements PacketListener {
        private NodeConfigListener listener;

        public NodeConfigTranslator(NodeConfigListener nodeConfigListener) {
            this.listener = nodeConfigListener;
        }

        public void processPacket(Packet packet) {
            this.listener.handleNodeConfiguration((ConfigurationEvent) ((EventElement) packet.getExtension("event", PubSubNamespace.EVENT.getXmlns())).getEvent());
        }
    }

    class EventContentFilter implements PacketFilter {
        private String firstElement;
        private String secondElement;

        EventContentFilter(String str) {
            this.firstElement = str;
        }

        EventContentFilter(String str, String str2) {
            this.firstElement = str;
            this.secondElement = str2;
        }

        public boolean accept(Packet packet) {
            if (!(packet instanceof Message)) {
                return false;
            }
            EventElement eventElement = (EventElement) packet.getExtension("event", PubSubNamespace.EVENT.getXmlns());
            if (eventElement == null) {
                return false;
            }
            NodeExtension event = eventElement.getEvent();
            if (event == null) {
                return false;
            }
            if (event.getElementName().equals(this.firstElement)) {
                if (!event.getNode().equals(Node.this.getId())) {
                    return false;
                }
                if (this.secondElement == null) {
                    return true;
                }
                if (event instanceof EmbeddedPacketExtension) {
                    List<PacketExtension> extensions = ((EmbeddedPacketExtension) event).getExtensions();
                    if (extensions.size() > 0 && extensions.get(0).getElementName().equals(this.secondElement)) {
                        return true;
                    }
                }
            }
            return false;
        }
    }
}
