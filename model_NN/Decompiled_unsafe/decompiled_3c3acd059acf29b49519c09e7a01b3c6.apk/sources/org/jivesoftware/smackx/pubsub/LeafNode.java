package org.jivesoftware.smackx.pubsub;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smackx.disco.packet.DiscoverItems;
import org.jivesoftware.smackx.pubsub.ItemsExtension;
import org.jivesoftware.smackx.pubsub.packet.PubSub;

public class LeafNode extends Node {
    LeafNode(XMPPConnection xMPPConnection, String str) {
        super(xMPPConnection, str);
    }

    public DiscoverItems discoverItems() throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        DiscoverItems discoverItems = new DiscoverItems();
        discoverItems.setTo(this.to);
        discoverItems.setNode(getId());
        return (DiscoverItems) this.con.createPacketCollectorAndSend(discoverItems).nextResultOrThrow();
    }

    public <T extends Item> List<T> getItems() throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        return ((ItemsExtension) ((PubSub) this.con.createPacketCollectorAndSend(createPubsubPacket(IQ.Type.GET, new GetItemsRequest(getId()))).nextResultOrThrow()).getExtension(PubSubElementType.ITEMS)).getItems();
    }

    public <T extends Item> List<T> getItems(String str) throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        return ((ItemsExtension) ((PubSub) this.con.createPacketCollectorAndSend(createPubsubPacket(IQ.Type.GET, new GetItemsRequest(getId(), str))).nextResultOrThrow()).getExtension(PubSubElementType.ITEMS)).getItems();
    }

    public <T extends Item> List<T> getItems(Collection<String> collection) throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        ArrayList arrayList = new ArrayList(collection.size());
        for (String item : collection) {
            arrayList.add(new Item(item));
        }
        return ((ItemsExtension) ((PubSub) this.con.createPacketCollectorAndSend(createPubsubPacket(IQ.Type.GET, new ItemsExtension(ItemsExtension.ItemsElementType.items, getId(), arrayList))).nextResultOrThrow()).getExtension(PubSubElementType.ITEMS)).getItems();
    }

    public <T extends Item> List<T> getItems(int i) throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        return ((ItemsExtension) ((PubSub) this.con.createPacketCollectorAndSend(createPubsubPacket(IQ.Type.GET, new GetItemsRequest(getId(), i))).nextResultOrThrow()).getExtension(PubSubElementType.ITEMS)).getItems();
    }

    public <T extends Item> List<T> getItems(int i, String str) throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        return ((ItemsExtension) ((PubSub) this.con.createPacketCollectorAndSend(createPubsubPacket(IQ.Type.GET, new GetItemsRequest(getId(), str, i))).nextResultOrThrow()).getExtension(PubSubElementType.ITEMS)).getItems();
    }

    public void publish() throws SmackException.NotConnectedException {
        this.con.sendPacket(createPubsubPacket(IQ.Type.SET, new NodeExtension(PubSubElementType.PUBLISH, getId())));
    }

    public <T extends Item> void publish(T t) throws SmackException.NotConnectedException {
        ArrayList arrayList = new ArrayList(1);
        if (t == null) {
            t = new Item();
        }
        arrayList.add(t);
        publish(arrayList);
    }

    public <T extends Item> void publish(Collection<T> collection) throws SmackException.NotConnectedException {
        this.con.sendPacket(createPubsubPacket(IQ.Type.SET, new PublishItem(getId(), collection)));
    }

    public void send() throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        this.con.createPacketCollectorAndSend(createPubsubPacket(IQ.Type.SET, new NodeExtension(PubSubElementType.PUBLISH, getId()))).nextResultOrThrow();
    }

    public <T extends Item> void send(T t) throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        ArrayList arrayList = new ArrayList(1);
        if (t == null) {
            t = new Item();
        }
        arrayList.add(t);
        send(arrayList);
    }

    public <T extends Item> void send(Collection<T> collection) throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        this.con.createPacketCollectorAndSend(createPubsubPacket(IQ.Type.SET, new PublishItem(getId(), collection))).nextResultOrThrow();
    }

    public void deleteAllItems() throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        this.con.createPacketCollectorAndSend(createPubsubPacket(IQ.Type.SET, new NodeExtension(PubSubElementType.PURGE_OWNER, getId()), PubSubElementType.PURGE_OWNER.getNamespace())).nextResultOrThrow();
    }

    public void deleteItem(String str) throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        ArrayList arrayList = new ArrayList(1);
        arrayList.add(str);
        deleteItem(arrayList);
    }

    public void deleteItem(Collection<String> collection) throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        ArrayList arrayList = new ArrayList(collection.size());
        for (String item : collection) {
            arrayList.add(new Item(item));
        }
        this.con.createPacketCollectorAndSend(createPubsubPacket(IQ.Type.SET, new ItemsExtension(ItemsExtension.ItemsElementType.retract, getId(), arrayList))).nextResultOrThrow();
    }
}
