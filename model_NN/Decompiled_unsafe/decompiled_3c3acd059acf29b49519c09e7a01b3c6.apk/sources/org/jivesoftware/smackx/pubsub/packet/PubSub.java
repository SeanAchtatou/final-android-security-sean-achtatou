package org.jivesoftware.smackx.pubsub.packet;

import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.packet.PacketExtension;
import org.jivesoftware.smackx.pubsub.PubSubElementType;

public class PubSub extends IQ {
    public static final String ELEMENT = "pubsub";
    public static final String NAMESPACE = "http://jabber.org/protocol/pubsub";
    private PubSubNamespace ns;

    public PubSub() {
        this.ns = PubSubNamespace.BASIC;
    }

    public PubSub(String str, IQ.Type type) {
        this.ns = PubSubNamespace.BASIC;
        setTo(str);
        setType(type);
    }

    public PubSub(String str, IQ.Type type, PubSubNamespace pubSubNamespace) {
        this(str, type);
        if (pubSubNamespace != null) {
            setPubSubNamespace(pubSubNamespace);
        }
    }

    public String getElementName() {
        return ELEMENT;
    }

    public String getNamespace() {
        return this.ns.getXmlns();
    }

    public void setPubSubNamespace(PubSubNamespace pubSubNamespace) {
        this.ns = pubSubNamespace;
    }

    public PacketExtension getExtension(PubSubElementType pubSubElementType) {
        return getExtension(pubSubElementType.getElementName(), pubSubElementType.getNamespace().getXmlns());
    }

    public PubSubNamespace getPubSubNamespace() {
        return this.ns;
    }

    public String getChildElementXML() {
        StringBuilder sb = new StringBuilder();
        sb.append("<").append(getElementName()).append(" xmlns=\"").append(getNamespace()).append("\">");
        sb.append(getExtensionsXML());
        sb.append("</").append(getElementName()).append(">");
        return sb.toString();
    }

    public static PubSub createPubsubPacket(String str, IQ.Type type, PacketExtension packetExtension, PubSubNamespace pubSubNamespace) {
        PubSub pubSub = new PubSub(str, type, pubSubNamespace);
        pubSub.addExtension(packetExtension);
        return pubSub;
    }
}
