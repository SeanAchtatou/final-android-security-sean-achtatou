package org.jivesoftware.smackx.amp.packet;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.concurrent.CopyOnWriteArrayList;
import org.jivesoftware.smack.packet.PacketExtension;

public class AMPExtension implements PacketExtension {
    public static final String ELEMENT = "amp";
    public static final String NAMESPACE = "http://jabber.org/protocol/amp";
    private final String from;
    private boolean perHop;
    private CopyOnWriteArrayList<Rule> rules;
    private final Status status;
    private final String to;

    public enum Action {
        alert,
        drop,
        error,
        notify;
        
        public static final String ATTRIBUTE_NAME = "action";
    }

    public interface Condition {
        public static final String ATTRIBUTE_NAME = "condition";

        String getName();

        String getValue();
    }

    public enum Status {
        alert,
        error,
        notify
    }

    public AMPExtension(String str, String str2, Status status2) {
        this.rules = new CopyOnWriteArrayList<>();
        this.perHop = false;
        this.from = str;
        this.to = str2;
        this.status = status2;
    }

    public AMPExtension() {
        this.rules = new CopyOnWriteArrayList<>();
        this.perHop = false;
        this.from = null;
        this.to = null;
        this.status = null;
    }

    public String getFrom() {
        return this.from;
    }

    public String getTo() {
        return this.to;
    }

    public Status getStatus() {
        return this.status;
    }

    public Collection<Rule> getRules() {
        return Collections.unmodifiableList(new ArrayList(this.rules));
    }

    public void addRule(Rule rule) {
        this.rules.add(rule);
    }

    public int getRulesCount() {
        return this.rules.size();
    }

    public synchronized void setPerHop(boolean z) {
        this.perHop = z;
    }

    public synchronized boolean isPerHop() {
        return this.perHop;
    }

    public String getElementName() {
        return ELEMENT;
    }

    public String getNamespace() {
        return NAMESPACE;
    }

    public String toXML() {
        StringBuilder sb = new StringBuilder();
        sb.append("<").append(getElementName()).append(" xmlns=\"").append(getNamespace()).append("\"");
        if (this.status != null) {
            sb.append(" status=\"").append(this.status.toString()).append("\"");
        }
        if (this.to != null) {
            sb.append(" to=\"").append(this.to).append("\"");
        }
        if (this.from != null) {
            sb.append(" from=\"").append(this.from).append("\"");
        }
        if (this.perHop) {
            sb.append(" per-hop=\"true\"");
        }
        sb.append(">");
        for (Rule access$000 : getRules()) {
            sb.append(access$000.toXML());
        }
        sb.append("</").append(getElementName()).append(">");
        return sb.toString();
    }

    public static class Rule {
        public static final String ELEMENT = "rule";
        private final Action action;
        private final Condition condition;

        public Action getAction() {
            return this.action;
        }

        public Condition getCondition() {
            return this.condition;
        }

        public Rule(Action action2, Condition condition2) {
            if (action2 == null) {
                throw new NullPointerException("Can't create Rule with null action");
            } else if (condition2 == null) {
                throw new NullPointerException("Can't create Rule with null condition");
            } else {
                this.action = action2;
                this.condition = condition2;
            }
        }

        /* access modifiers changed from: private */
        public String toXML() {
            return "<rule action=\"" + this.action.toString() + "\" " + Condition.ATTRIBUTE_NAME + "=\"" + this.condition.getName() + "\" " + "value=\"" + this.condition.getValue() + "\"/>";
        }
    }
}
