package org.jivesoftware.smackx.xhtmlim;

import java.util.List;
import org.jivesoftware.smack.ConnectionCreationListener;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smackx.disco.ServiceDiscoveryManager;
import org.jivesoftware.smackx.xhtmlim.packet.XHTMLExtension;

public class XHTMLManager {
    private static final String namespace = "http://jabber.org/protocol/xhtml-im";

    static {
        XMPPConnection.addConnectionCreationListener(new ConnectionCreationListener() {
            public void connectionCreated(XMPPConnection xMPPConnection) {
                XHTMLManager.setServiceEnabled(xMPPConnection, true);
            }
        });
    }

    public static List<String> getBodies(Message message) {
        XHTMLExtension xHTMLExtension = (XHTMLExtension) message.getExtension("html", namespace);
        if (xHTMLExtension != null) {
            return xHTMLExtension.getBodies();
        }
        return null;
    }

    public static void addBody(Message message, String str) {
        XHTMLExtension xHTMLExtension = (XHTMLExtension) message.getExtension("html", namespace);
        if (xHTMLExtension == null) {
            xHTMLExtension = new XHTMLExtension();
            message.addExtension(xHTMLExtension);
        }
        xHTMLExtension.addBody(str);
    }

    public static boolean isXHTMLMessage(Message message) {
        return message.getExtension("html", namespace) != null;
    }

    public static synchronized void setServiceEnabled(XMPPConnection xMPPConnection, boolean z) {
        synchronized (XHTMLManager.class) {
            if (isServiceEnabled(xMPPConnection) != z) {
                if (z) {
                    ServiceDiscoveryManager.getInstanceFor(xMPPConnection).addFeature(namespace);
                } else {
                    ServiceDiscoveryManager.getInstanceFor(xMPPConnection).removeFeature(namespace);
                }
            }
        }
    }

    public static boolean isServiceEnabled(XMPPConnection xMPPConnection) {
        return ServiceDiscoveryManager.getInstanceFor(xMPPConnection).includesFeature(namespace);
    }

    public static boolean isServiceEnabled(XMPPConnection xMPPConnection, String str) throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        return ServiceDiscoveryManager.getInstanceFor(xMPPConnection).supportsFeature(str, namespace);
    }
}
