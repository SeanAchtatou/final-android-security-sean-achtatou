package org.jivesoftware.smackx.xdata.provider;

import java.util.ArrayList;
import org.jivesoftware.smack.packet.PacketExtension;
import org.jivesoftware.smack.provider.PacketExtensionProvider;
import org.jivesoftware.smackx.xdata.FormField;
import org.jivesoftware.smackx.xdata.packet.DataForm;
import org.xmlpull.v1.XmlPullParser;

public class DataFormProvider implements PacketExtensionProvider {
    public PacketExtension parseExtension(XmlPullParser xmlPullParser) throws Exception {
        boolean z = false;
        DataForm dataForm = new DataForm(xmlPullParser.getAttributeValue("", "type"));
        while (!z) {
            int next = xmlPullParser.next();
            if (next == 2) {
                if (xmlPullParser.getName().equals("instructions")) {
                    dataForm.addInstruction(xmlPullParser.nextText());
                } else if (xmlPullParser.getName().equals("title")) {
                    dataForm.setTitle(xmlPullParser.nextText());
                } else if (xmlPullParser.getName().equals(FormField.ELEMENT)) {
                    dataForm.addField(parseField(xmlPullParser));
                } else if (xmlPullParser.getName().equals(DataForm.Item.ELEMENT)) {
                    dataForm.addItem(parseItem(xmlPullParser));
                } else if (xmlPullParser.getName().equals(DataForm.ReportedData.ELEMENT)) {
                    dataForm.setReportedData(parseReported(xmlPullParser));
                }
            } else if (next == 3 && xmlPullParser.getName().equals(dataForm.getElementName())) {
                z = true;
            }
        }
        return dataForm;
    }

    private FormField parseField(XmlPullParser xmlPullParser) throws Exception {
        boolean z = false;
        FormField formField = new FormField(xmlPullParser.getAttributeValue("", "var"));
        formField.setLabel(xmlPullParser.getAttributeValue("", "label"));
        formField.setType(xmlPullParser.getAttributeValue("", "type"));
        while (!z) {
            int next = xmlPullParser.next();
            if (next == 2) {
                if (xmlPullParser.getName().equals("desc")) {
                    formField.setDescription(xmlPullParser.nextText());
                } else if (xmlPullParser.getName().equals("value")) {
                    formField.addValue(xmlPullParser.nextText());
                } else if (xmlPullParser.getName().equals("required")) {
                    formField.setRequired(true);
                } else if (xmlPullParser.getName().equals(FormField.Option.ELEMENT)) {
                    formField.addOption(parseOption(xmlPullParser));
                }
            } else if (next == 3 && xmlPullParser.getName().equals(FormField.ELEMENT)) {
                z = true;
            }
        }
        return formField;
    }

    private DataForm.Item parseItem(XmlPullParser xmlPullParser) throws Exception {
        boolean z = false;
        ArrayList arrayList = new ArrayList();
        while (!z) {
            int next = xmlPullParser.next();
            if (next == 2) {
                if (xmlPullParser.getName().equals(FormField.ELEMENT)) {
                    arrayList.add(parseField(xmlPullParser));
                }
            } else if (next == 3 && xmlPullParser.getName().equals(DataForm.Item.ELEMENT)) {
                z = true;
            }
        }
        return new DataForm.Item(arrayList);
    }

    private DataForm.ReportedData parseReported(XmlPullParser xmlPullParser) throws Exception {
        boolean z = false;
        ArrayList arrayList = new ArrayList();
        while (!z) {
            int next = xmlPullParser.next();
            if (next == 2) {
                if (xmlPullParser.getName().equals(FormField.ELEMENT)) {
                    arrayList.add(parseField(xmlPullParser));
                }
            } else if (next == 3 && xmlPullParser.getName().equals(DataForm.ReportedData.ELEMENT)) {
                z = true;
            }
        }
        return new DataForm.ReportedData(arrayList);
    }

    private FormField.Option parseOption(XmlPullParser xmlPullParser) throws Exception {
        boolean z = false;
        FormField.Option option = null;
        String attributeValue = xmlPullParser.getAttributeValue("", "label");
        while (!z) {
            int next = xmlPullParser.next();
            if (next == 2) {
                if (xmlPullParser.getName().equals("value")) {
                    option = new FormField.Option(attributeValue, xmlPullParser.nextText());
                }
            } else if (next == 3 && xmlPullParser.getName().equals(FormField.Option.ELEMENT)) {
                z = true;
            }
        }
        return option;
    }
}
