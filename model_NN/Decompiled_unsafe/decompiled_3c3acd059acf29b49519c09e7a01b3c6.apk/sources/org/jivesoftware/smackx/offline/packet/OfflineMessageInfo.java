package org.jivesoftware.smackx.offline.packet;

import org.jivesoftware.smack.packet.PacketExtension;
import org.jivesoftware.smack.provider.PacketExtensionProvider;
import org.jivesoftware.smackx.xdata.packet.DataForm;
import org.jivesoftware.smackx.xevent.packet.MessageEvent;
import org.xmlpull.v1.XmlPullParser;

public class OfflineMessageInfo implements PacketExtension {
    private String node = null;

    public String getElementName() {
        return MessageEvent.OFFLINE;
    }

    public String getNamespace() {
        return "http://jabber.org/protocol/offline";
    }

    public String getNode() {
        return this.node;
    }

    public void setNode(String str) {
        this.node = str;
    }

    public String toXML() {
        StringBuilder sb = new StringBuilder();
        sb.append("<").append(getElementName()).append(" xmlns=\"").append(getNamespace()).append("\">");
        if (getNode() != null) {
            sb.append("<item node=\"").append(getNode()).append("\"/>");
        }
        sb.append("</").append(getElementName()).append(">");
        return sb.toString();
    }

    public static class Provider implements PacketExtensionProvider {
        public PacketExtension parseExtension(XmlPullParser xmlPullParser) throws Exception {
            OfflineMessageInfo offlineMessageInfo = new OfflineMessageInfo();
            boolean z = false;
            while (!z) {
                int next = xmlPullParser.next();
                if (next == 2) {
                    if (xmlPullParser.getName().equals(DataForm.Item.ELEMENT)) {
                        offlineMessageInfo.setNode(xmlPullParser.getAttributeValue("", "node"));
                    }
                } else if (next == 3 && xmlPullParser.getName().equals(MessageEvent.OFFLINE)) {
                    z = true;
                }
            }
            return offlineMessageInfo;
        }
    }
}
