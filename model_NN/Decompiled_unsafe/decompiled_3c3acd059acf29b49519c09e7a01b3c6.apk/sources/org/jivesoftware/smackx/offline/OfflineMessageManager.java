package org.jivesoftware.smackx.offline;

import java.util.ArrayList;
import java.util.List;
import org.jivesoftware.smack.PacketCollector;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.filter.AndFilter;
import org.jivesoftware.smack.filter.PacketExtensionFilter;
import org.jivesoftware.smack.filter.PacketFilter;
import org.jivesoftware.smack.filter.PacketTypeFilter;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smackx.disco.ServiceDiscoveryManager;
import org.jivesoftware.smackx.disco.packet.DiscoverItems;
import org.jivesoftware.smackx.offline.packet.OfflineMessageInfo;
import org.jivesoftware.smackx.offline.packet.OfflineMessageRequest;
import org.jivesoftware.smackx.xdata.Form;
import org.jivesoftware.smackx.xevent.packet.MessageEvent;

public class OfflineMessageManager {
    private static final String namespace = "http://jabber.org/protocol/offline";
    private XMPPConnection connection;
    private PacketFilter packetFilter = new AndFilter(new PacketExtensionFilter(MessageEvent.OFFLINE, namespace), new PacketTypeFilter(Message.class));

    public OfflineMessageManager(XMPPConnection xMPPConnection) {
        this.connection = xMPPConnection;
    }

    public boolean supportsFlexibleRetrieval() throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        return ServiceDiscoveryManager.getInstanceFor(this.connection).supportsFeature(this.connection.getServiceName(), namespace);
    }

    public int getMessageCount() throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        Form formFrom = Form.getFormFrom(ServiceDiscoveryManager.getInstanceFor(this.connection).discoverInfo(null, namespace));
        if (formFrom != null) {
            return Integer.parseInt(formFrom.getField("number_of_messages").getValues().get(0));
        }
        return 0;
    }

    public List<OfflineMessageHeader> getHeaders() throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        ArrayList arrayList = new ArrayList();
        for (DiscoverItems.Item offlineMessageHeader : ServiceDiscoveryManager.getInstanceFor(this.connection).discoverItems(null, namespace).getItems()) {
            arrayList.add(new OfflineMessageHeader(offlineMessageHeader));
        }
        return arrayList;
    }

    public List<Message> getMessages(final List<String> list) throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        ArrayList arrayList = new ArrayList();
        OfflineMessageRequest offlineMessageRequest = new OfflineMessageRequest();
        for (String item : list) {
            OfflineMessageRequest.Item item2 = new OfflineMessageRequest.Item(item);
            item2.setAction("view");
            offlineMessageRequest.addItem(item2);
        }
        PacketCollector createPacketCollector = this.connection.createPacketCollector(new AndFilter(this.packetFilter, new PacketFilter() {
            public boolean accept(Packet packet) {
                return list.contains(((OfflineMessageInfo) packet.getExtension(MessageEvent.OFFLINE, OfflineMessageManager.namespace)).getNode());
            }
        }));
        try {
            this.connection.createPacketCollectorAndSend(offlineMessageRequest).nextResultOrThrow();
            for (Message message = (Message) createPacketCollector.nextResult(); message != null; message = (Message) createPacketCollector.nextResult()) {
                arrayList.add(message);
            }
            return arrayList;
        } finally {
            createPacketCollector.cancel();
        }
    }

    public List<Message> getMessages() throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        ArrayList arrayList = new ArrayList();
        OfflineMessageRequest offlineMessageRequest = new OfflineMessageRequest();
        offlineMessageRequest.setFetch(true);
        PacketCollector createPacketCollector = this.connection.createPacketCollector(this.packetFilter);
        try {
            this.connection.createPacketCollectorAndSend(offlineMessageRequest).nextResultOrThrow();
            for (Message message = (Message) createPacketCollector.nextResult(); message != null; message = (Message) createPacketCollector.nextResult()) {
                arrayList.add(message);
            }
            return arrayList;
        } finally {
            createPacketCollector.cancel();
        }
    }

    public void deleteMessages(List<String> list) throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        OfflineMessageRequest offlineMessageRequest = new OfflineMessageRequest();
        for (String item : list) {
            OfflineMessageRequest.Item item2 = new OfflineMessageRequest.Item(item);
            item2.setAction(DiscoverItems.Item.REMOVE_ACTION);
            offlineMessageRequest.addItem(item2);
        }
        this.connection.createPacketCollectorAndSend(offlineMessageRequest).nextResultOrThrow();
    }

    public void deleteMessages() throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        OfflineMessageRequest offlineMessageRequest = new OfflineMessageRequest();
        offlineMessageRequest.setPurge(true);
        this.connection.createPacketCollectorAndSend(offlineMessageRequest).nextResultOrThrow();
    }
}
