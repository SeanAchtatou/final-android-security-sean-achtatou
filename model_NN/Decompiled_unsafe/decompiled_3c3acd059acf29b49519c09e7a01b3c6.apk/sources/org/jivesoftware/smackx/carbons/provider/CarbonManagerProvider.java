package org.jivesoftware.smackx.carbons.provider;

import org.jivesoftware.smack.packet.PacketExtension;
import org.jivesoftware.smack.provider.PacketExtensionProvider;
import org.jivesoftware.smack.util.PacketParserUtils;
import org.jivesoftware.smackx.carbons.packet.CarbonExtension;
import org.jivesoftware.smackx.forward.Forwarded;
import org.xmlpull.v1.XmlPullParser;

public class CarbonManagerProvider implements PacketExtensionProvider {
    public PacketExtension parseExtension(XmlPullParser xmlPullParser) throws Exception {
        boolean z;
        Forwarded forwarded;
        CarbonExtension.Direction valueOf = CarbonExtension.Direction.valueOf(xmlPullParser.getName());
        Forwarded forwarded2 = null;
        boolean z2 = false;
        while (!z2) {
            int next = xmlPullParser.next();
            if (next == 2 && xmlPullParser.getName().equals(Forwarded.ELEMENT_NAME)) {
                boolean z3 = z2;
                forwarded = (Forwarded) PacketParserUtils.parsePacketExtension(Forwarded.ELEMENT_NAME, Forwarded.NAMESPACE, xmlPullParser);
                z = z3;
            } else if (next == 3 && valueOf == CarbonExtension.Direction.valueOf(xmlPullParser.getName())) {
                z = true;
                forwarded = forwarded2;
            } else {
                z = z2;
                forwarded = forwarded2;
            }
            forwarded2 = forwarded;
            z2 = z;
        }
        if (forwarded2 != null) {
            return new CarbonExtension(valueOf, forwarded2);
        }
        throw new Exception("sent/received must contain exactly one <forwarded> tag");
    }
}
