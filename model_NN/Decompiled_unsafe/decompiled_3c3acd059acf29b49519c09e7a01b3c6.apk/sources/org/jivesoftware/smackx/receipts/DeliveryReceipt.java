package org.jivesoftware.smackx.receipts;

import java.util.List;
import java.util.Map;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.packet.PacketExtension;
import org.jivesoftware.smack.provider.EmbeddedExtensionProvider;

public class DeliveryReceipt implements PacketExtension {
    public static final String ELEMENT = "received";
    public static final String NAMESPACE = "urn:xmpp:receipts";
    private String id;

    public DeliveryReceipt(String str) {
        this.id = str;
    }

    public String getId() {
        return this.id;
    }

    public String getElementName() {
        return ELEMENT;
    }

    public String getNamespace() {
        return NAMESPACE;
    }

    public String toXML() {
        return "<received xmlns='urn:xmpp:receipts' id='" + this.id + "'/>";
    }

    public static DeliveryReceipt getFrom(Packet packet) {
        return (DeliveryReceipt) packet.getExtension(ELEMENT, NAMESPACE);
    }

    public static class Provider extends EmbeddedExtensionProvider {
        /* access modifiers changed from: protected */
        public PacketExtension createReturnExtension(String str, String str2, Map<String, String> map, List<? extends PacketExtension> list) {
            return new DeliveryReceipt(map.get("id"));
        }
    }
}
