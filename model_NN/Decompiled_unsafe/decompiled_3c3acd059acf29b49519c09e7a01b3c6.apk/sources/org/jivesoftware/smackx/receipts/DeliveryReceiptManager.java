package org.jivesoftware.smackx.receipts;

import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;
import org.jivesoftware.smack.ConnectionCreationListener;
import org.jivesoftware.smack.Manager;
import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.filter.PacketExtensionFilter;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smackx.disco.ServiceDiscoveryManager;

public class DeliveryReceiptManager extends Manager implements PacketListener {
    private static Map<XMPPConnection, DeliveryReceiptManager> instances = new WeakHashMap();
    private boolean auto_receipts_enabled = false;
    private Set<ReceiptReceivedListener> receiptReceivedListeners = Collections.synchronizedSet(new HashSet());

    static {
        XMPPConnection.addConnectionCreationListener(new ConnectionCreationListener() {
            public void connectionCreated(XMPPConnection xMPPConnection) {
                DeliveryReceiptManager.getInstanceFor(xMPPConnection);
            }
        });
    }

    private DeliveryReceiptManager(XMPPConnection xMPPConnection) {
        super(xMPPConnection);
        ServiceDiscoveryManager.getInstanceFor(xMPPConnection).addFeature(DeliveryReceipt.NAMESPACE);
        xMPPConnection.addPacketListener(this, new PacketExtensionFilter(DeliveryReceipt.NAMESPACE));
    }

    public static synchronized DeliveryReceiptManager getInstanceFor(XMPPConnection xMPPConnection) {
        DeliveryReceiptManager deliveryReceiptManager;
        synchronized (DeliveryReceiptManager.class) {
            deliveryReceiptManager = instances.get(xMPPConnection);
            if (deliveryReceiptManager == null) {
                deliveryReceiptManager = new DeliveryReceiptManager(xMPPConnection);
                instances.put(xMPPConnection, deliveryReceiptManager);
            }
        }
        return deliveryReceiptManager;
    }

    public boolean isSupported(String str) throws SmackException, XMPPException {
        return ServiceDiscoveryManager.getInstanceFor(connection()).supportsFeature(str, DeliveryReceipt.NAMESPACE);
    }

    public void processPacket(Packet packet) throws SmackException.NotConnectedException {
        DeliveryReceipt from = DeliveryReceipt.getFrom(packet);
        if (from != null) {
            for (ReceiptReceivedListener onReceiptReceived : this.receiptReceivedListeners) {
                onReceiptReceived.onReceiptReceived(packet.getFrom(), packet.getTo(), from.getId());
            }
        }
        if (this.auto_receipts_enabled && DeliveryReceiptRequest.getFrom(packet) != null) {
            XMPPConnection connection = connection();
            Message message = new Message(packet.getFrom(), Message.Type.normal);
            message.addExtension(new DeliveryReceipt(packet.getPacketID()));
            connection.sendPacket(message);
        }
    }

    public void setAutoReceiptsEnabled(boolean z) {
        this.auto_receipts_enabled = z;
    }

    public void enableAutoReceipts() {
        setAutoReceiptsEnabled(true);
    }

    public void disableAutoReceipts() {
        setAutoReceiptsEnabled(false);
    }

    public boolean getAutoReceiptsEnabled() {
        return this.auto_receipts_enabled;
    }

    public void addReceiptReceivedListener(ReceiptReceivedListener receiptReceivedListener) {
        this.receiptReceivedListeners.add(receiptReceivedListener);
    }

    public void removeReceiptReceivedListener(ReceiptReceivedListener receiptReceivedListener) {
        this.receiptReceivedListeners.remove(receiptReceivedListener);
    }

    public static boolean hasDeliveryReceiptRequest(Packet packet) {
        return DeliveryReceiptRequest.getFrom(packet) != null;
    }

    public static String addDeliveryReceiptRequest(Message message) {
        message.addExtension(new DeliveryReceiptRequest());
        return message.getPacketID();
    }
}
