package org.jivesoftware.smackx.receipts;

import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.packet.PacketExtension;
import org.jivesoftware.smack.provider.PacketExtensionProvider;
import org.xmlpull.v1.XmlPullParser;

public class DeliveryReceiptRequest implements PacketExtension {
    public static final String ELEMENT = "request";

    public String getElementName() {
        return ELEMENT;
    }

    public String getNamespace() {
        return DeliveryReceipt.NAMESPACE;
    }

    public String toXML() {
        return "<request xmlns='urn:xmpp:receipts'/>";
    }

    public static DeliveryReceiptRequest getFrom(Packet packet) {
        return (DeliveryReceiptRequest) packet.getExtension(ELEMENT, DeliveryReceipt.NAMESPACE);
    }

    public static class Provider implements PacketExtensionProvider {
        public PacketExtension parseExtension(XmlPullParser xmlPullParser) {
            return new DeliveryReceiptRequest();
        }
    }
}
