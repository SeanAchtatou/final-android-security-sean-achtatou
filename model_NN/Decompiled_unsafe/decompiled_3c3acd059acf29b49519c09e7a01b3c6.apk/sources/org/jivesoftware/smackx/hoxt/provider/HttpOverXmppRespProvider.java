package org.jivesoftware.smackx.hoxt.provider;

import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smackx.hoxt.packet.HttpOverXmppResp;
import org.xmlpull.v1.XmlPullParser;

public class HttpOverXmppRespProvider extends AbstractHttpOverXmppProvider {
    private static final String ATTRIBUTE_STATUS_CODE = "statusCode";
    private static final String ATTRIBUTE_STATUS_MESSAGE = "statusMessage";
    private static final String ELEMENT_RESP = "resp";

    public IQ parseIQ(XmlPullParser xmlPullParser) throws Exception {
        String attributeValue = xmlPullParser.getAttributeValue("", "version");
        String attributeValue2 = xmlPullParser.getAttributeValue("", ATTRIBUTE_STATUS_MESSAGE);
        int parseInt = Integer.parseInt(xmlPullParser.getAttributeValue("", ATTRIBUTE_STATUS_CODE));
        HttpOverXmppResp.Resp resp = new HttpOverXmppResp.Resp();
        resp.setVersion(attributeValue);
        resp.setStatusMessage(attributeValue2);
        resp.setStatusCode(parseInt);
        parseHeadersAndData(xmlPullParser, ELEMENT_RESP, resp);
        HttpOverXmppResp httpOverXmppResp = new HttpOverXmppResp();
        httpOverXmppResp.setResp(resp);
        return httpOverXmppResp;
    }
}
