package org.jivesoftware.smackx.hoxt.packet;

import org.jivesoftware.smack.util.StringUtils;
import org.jivesoftware.smackx.hoxt.HOXTManager;
import org.jivesoftware.smackx.hoxt.packet.AbstractHttpOverXmpp;

public class HttpOverXmppReq extends AbstractHttpOverXmpp {
    private Req req;

    public String getChildElementXML() {
        return this.req.toXML();
    }

    public Req getReq() {
        return this.req;
    }

    public void setReq(Req req2) {
        this.req = req2;
    }

    public static class Req extends AbstractHttpOverXmpp.AbstractBody {
        private boolean ibb = true;
        private boolean jingle = true;
        private int maxChunkSize = 0;
        private HttpMethod method;
        private String resource;
        private boolean sipub = true;

        public Req(HttpMethod httpMethod, String str) {
            this.method = httpMethod;
            this.resource = str;
        }

        /* access modifiers changed from: protected */
        public String getStartTag() {
            StringBuilder sb = new StringBuilder();
            sb.append("<req");
            sb.append(" ");
            sb.append("xmlns='").append(HOXTManager.NAMESPACE).append("'");
            sb.append(" ");
            sb.append("method='").append(this.method.toString()).append("'");
            sb.append(" ");
            sb.append("resource='").append(StringUtils.escapeForXML(this.resource)).append("'");
            sb.append(" ");
            sb.append("version='").append(StringUtils.escapeForXML(this.version)).append("'");
            if (this.maxChunkSize != 0) {
                sb.append(" ");
                sb.append("maxChunkSize='").append(Integer.toString(this.maxChunkSize)).append("'");
            }
            sb.append(" ");
            sb.append("sipub='").append(Boolean.toString(this.sipub)).append("'");
            sb.append(" ");
            sb.append("ibb='").append(Boolean.toString(this.ibb)).append("'");
            sb.append(" ");
            sb.append("jingle='").append(Boolean.toString(this.jingle)).append("'");
            sb.append(">");
            return sb.toString();
        }

        /* access modifiers changed from: protected */
        public String getEndTag() {
            return "</req>";
        }

        public HttpMethod getMethod() {
            return this.method;
        }

        public String getResource() {
            return this.resource;
        }

        public int getMaxChunkSize() {
            return this.maxChunkSize;
        }

        public void setMaxChunkSize(int i) {
            this.maxChunkSize = i;
        }

        public boolean isSipub() {
            return this.sipub;
        }

        public void setSipub(boolean z) {
            this.sipub = z;
        }

        public boolean isIbb() {
            return this.ibb;
        }

        public void setIbb(boolean z) {
            this.ibb = z;
        }

        public boolean isJingle() {
            return this.jingle;
        }

        public void setJingle(boolean z) {
            this.jingle = z;
        }
    }
}
