package org.jivesoftware.smackx.hoxt.provider;

import java.util.HashSet;
import org.jivesoftware.smack.provider.IQProvider;
import org.jivesoftware.smack.util.StringUtils;
import org.jivesoftware.smackx.hoxt.packet.AbstractHttpOverXmpp;
import org.jivesoftware.smackx.shim.packet.Header;
import org.jivesoftware.smackx.shim.packet.HeadersExtension;
import org.jivesoftware.smackx.shim.provider.HeaderProvider;
import org.xmlpull.v1.XmlPullParser;

public abstract class AbstractHttpOverXmppProvider implements IQProvider {
    private static final String ATTRIBUTE_SID = "sid";
    private static final String ATTRIBUTE_STREAM_ID = "streamId";
    static final String ATTRIBUTE_VERSION = "version";
    private static final String ELEMENT_BASE_64 = "base64";
    private static final String ELEMENT_CHUNKED_BASE_64 = "chunkedBase64";
    private static final String ELEMENT_DATA = "data";
    private static final String ELEMENT_HEADER = "header";
    private static final String ELEMENT_HEADERS = "headers";
    static final String ELEMENT_IBB = "ibb";
    static final String ELEMENT_JINGLE = "jingle";
    static final String ELEMENT_SIPUB = "sipub";
    private static final String ELEMENT_TEXT = "text";
    private static final String ELEMENT_XML = "xml";

    /* access modifiers changed from: protected */
    public void parseHeadersAndData(XmlPullParser xmlPullParser, String str, AbstractHttpOverXmpp.AbstractBody abstractBody) throws Exception {
        boolean z = false;
        while (!z) {
            int next = xmlPullParser.next();
            if (next == 2) {
                if (xmlPullParser.getName().equals(ELEMENT_HEADERS)) {
                    abstractBody.setHeaders(parseHeaders(xmlPullParser));
                } else if (xmlPullParser.getName().endsWith("data")) {
                    abstractBody.setData(parseData(xmlPullParser));
                } else {
                    throw new IllegalArgumentException("unexpected tag:" + xmlPullParser.getName() + "'");
                }
            } else if (next == 3 && xmlPullParser.getName().equals(str)) {
                z = true;
            }
        }
    }

    private HeadersExtension parseHeaders(XmlPullParser xmlPullParser) throws Exception {
        boolean z;
        HeaderProvider headerProvider = new HeaderProvider();
        HashSet hashSet = new HashSet();
        boolean z2 = false;
        while (!z2) {
            int next = xmlPullParser.next();
            if (next == 2) {
                if (xmlPullParser.getName().equals(ELEMENT_HEADER)) {
                    hashSet.add((Header) headerProvider.parseExtension(xmlPullParser));
                    z = z2;
                }
                z = z2;
            } else {
                if (next == 3 && xmlPullParser.getName().equals(ELEMENT_HEADERS)) {
                    z = true;
                }
                z = z2;
            }
            z2 = z;
        }
        return new HeadersExtension(hashSet);
    }

    private AbstractHttpOverXmpp.Data parseData(XmlPullParser xmlPullParser) throws Exception {
        AbstractHttpOverXmpp.DataChild dataChild = null;
        boolean z = false;
        while (!z) {
            int next = xmlPullParser.next();
            if (next == 2) {
                if (xmlPullParser.getName().equals(ELEMENT_TEXT)) {
                    dataChild = parseText(xmlPullParser);
                } else if (xmlPullParser.getName().equals(ELEMENT_BASE_64)) {
                    dataChild = parseBase64(xmlPullParser);
                } else if (xmlPullParser.getName().equals(ELEMENT_CHUNKED_BASE_64)) {
                    dataChild = parseChunkedBase64(xmlPullParser);
                } else if (xmlPullParser.getName().equals(ELEMENT_XML)) {
                    dataChild = parseXml(xmlPullParser);
                } else if (xmlPullParser.getName().equals(ELEMENT_IBB)) {
                    dataChild = parseIbb(xmlPullParser);
                } else if (xmlPullParser.getName().equals(ELEMENT_SIPUB)) {
                    throw new UnsupportedOperationException("sipub is not supported yet");
                } else if (xmlPullParser.getName().equals(ELEMENT_JINGLE)) {
                    throw new UnsupportedOperationException("jingle is not supported yet");
                } else {
                    throw new IllegalArgumentException("unsupported child tag: " + xmlPullParser.getName());
                }
            } else if (next == 3 && xmlPullParser.getName().equals("data")) {
                z = true;
            }
        }
        return new AbstractHttpOverXmpp.Data(dataChild);
    }

    private AbstractHttpOverXmpp.Text parseText(XmlPullParser xmlPullParser) throws Exception {
        String str = null;
        boolean z = false;
        while (!z) {
            int next = xmlPullParser.next();
            if (next == 3) {
                if (xmlPullParser.getName().equals(ELEMENT_TEXT)) {
                    z = true;
                } else {
                    throw new IllegalArgumentException("unexpected end tag of: " + xmlPullParser.getName());
                }
            } else if (next == 4) {
                str = xmlPullParser.getText();
            } else {
                throw new IllegalArgumentException("unexpected eventType: " + next);
            }
        }
        return new AbstractHttpOverXmpp.Text(str);
    }

    private AbstractHttpOverXmpp.Xml parseXml(XmlPullParser xmlPullParser) throws Exception {
        StringBuilder sb = new StringBuilder();
        boolean z = true;
        boolean z2 = false;
        while (!z2) {
            int next = xmlPullParser.next();
            if (next == 3 && xmlPullParser.getName().equals(ELEMENT_XML)) {
                z2 = true;
            } else if (next == 2) {
                if (!z) {
                    sb.append('>');
                }
                sb.append('<');
                sb.append(xmlPullParser.getName());
                appendXmlAttributes(xmlPullParser, sb);
                z = false;
            } else if (next == 3) {
                if (z) {
                    sb.append("</");
                    sb.append(xmlPullParser.getName());
                    sb.append('>');
                } else {
                    sb.append("/>");
                    z = true;
                }
            } else if (next == 4) {
                if (!z) {
                    sb.append('>');
                    z = true;
                }
                sb.append(StringUtils.escapeForXML(xmlPullParser.getText()));
            } else {
                throw new IllegalArgumentException("unexpected eventType: " + next);
            }
        }
        return new AbstractHttpOverXmpp.Xml(sb.toString());
    }

    private void appendXmlAttributes(XmlPullParser xmlPullParser, StringBuilder sb) throws Exception {
        int attributeCount = xmlPullParser.getAttributeCount();
        if (attributeCount > 0) {
            for (int i = 0; i < attributeCount; i++) {
                sb.append(' ');
                sb.append(xmlPullParser.getAttributeName(i));
                sb.append("=\"");
                sb.append(StringUtils.escapeForXML(xmlPullParser.getAttributeValue(i)));
                sb.append('\"');
            }
        }
    }

    private AbstractHttpOverXmpp.Base64 parseBase64(XmlPullParser xmlPullParser) throws Exception {
        String str = null;
        boolean z = false;
        while (!z) {
            int next = xmlPullParser.next();
            if (next == 3) {
                if (xmlPullParser.getName().equals(ELEMENT_BASE_64)) {
                    z = true;
                } else {
                    throw new IllegalArgumentException("unexpected end tag of: " + xmlPullParser.getName());
                }
            } else if (next == 4) {
                str = xmlPullParser.getText();
            } else {
                throw new IllegalArgumentException("unexpected eventType: " + next);
            }
        }
        return new AbstractHttpOverXmpp.Base64(str);
    }

    private AbstractHttpOverXmpp.ChunkedBase64 parseChunkedBase64(XmlPullParser xmlPullParser) throws Exception {
        AbstractHttpOverXmpp.ChunkedBase64 chunkedBase64 = new AbstractHttpOverXmpp.ChunkedBase64(xmlPullParser.getAttributeValue("", "streamId"));
        boolean z = false;
        while (!z) {
            int next = xmlPullParser.next();
            if (next != 3) {
                throw new IllegalArgumentException("unexpected event type: " + next);
            } else if (xmlPullParser.getName().equals(ELEMENT_CHUNKED_BASE_64)) {
                z = true;
            } else {
                throw new IllegalArgumentException("unexpected end tag: " + xmlPullParser.getName());
            }
        }
        return chunkedBase64;
    }

    private AbstractHttpOverXmpp.Ibb parseIbb(XmlPullParser xmlPullParser) throws Exception {
        AbstractHttpOverXmpp.Ibb ibb = new AbstractHttpOverXmpp.Ibb(xmlPullParser.getAttributeValue("", ATTRIBUTE_SID));
        boolean z = false;
        while (!z) {
            int next = xmlPullParser.next();
            if (next != 3) {
                throw new IllegalArgumentException("unexpected event type: " + next);
            } else if (xmlPullParser.getName().equals(ELEMENT_IBB)) {
                z = true;
            } else {
                throw new IllegalArgumentException("unexpected end tag: " + xmlPullParser.getName());
            }
        }
        return ibb;
    }
}
