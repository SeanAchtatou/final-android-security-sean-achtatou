package org.jivesoftware.smackx.forward;

import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.packet.PacketExtension;
import org.jivesoftware.smackx.delay.packet.DelayInfo;

public class Forwarded implements PacketExtension {
    public static final String ELEMENT_NAME = "forwarded";
    public static final String NAMESPACE = "urn:xmpp:forward:0";
    private DelayInfo delay;
    private Packet forwardedPacket;

    public Forwarded(DelayInfo delayInfo, Packet packet) {
        this.delay = delayInfo;
        this.forwardedPacket = packet;
    }

    public Forwarded(Packet packet) {
        this.forwardedPacket = packet;
    }

    public String getElementName() {
        return ELEMENT_NAME;
    }

    public String getNamespace() {
        return NAMESPACE;
    }

    public String toXML() {
        StringBuilder sb = new StringBuilder();
        sb.append("<").append(getElementName()).append(" xmlns=\"").append(getNamespace()).append("\">");
        if (this.delay != null) {
            sb.append(this.delay.toXML());
        }
        sb.append(this.forwardedPacket.toXML());
        sb.append("</").append(getElementName()).append(">");
        return sb.toString();
    }

    public Packet getForwardedPacket() {
        return this.forwardedPacket;
    }

    public DelayInfo getDelayInfo() {
        return this.delay;
    }
}
