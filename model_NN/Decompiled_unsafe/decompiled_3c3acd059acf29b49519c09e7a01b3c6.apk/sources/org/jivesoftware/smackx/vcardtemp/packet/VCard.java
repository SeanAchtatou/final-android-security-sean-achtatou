package org.jivesoftware.smackx.vcardtemp.packet;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.util.StringUtils;
import org.jivesoftware.smackx.vcardtemp.VCardManager;

public class VCard extends IQ {
    private static final String DEFAULT_MIME_TYPE = "image/jpeg";
    private static final Logger LOGGER = Logger.getLogger(VCard.class.getName());
    /* access modifiers changed from: private */
    public String emailHome;
    /* access modifiers changed from: private */
    public String emailWork;
    /* access modifiers changed from: private */
    public String firstName;
    /* access modifiers changed from: private */
    public Map<String, String> homeAddr = new HashMap();
    /* access modifiers changed from: private */
    public Map<String, String> homePhones = new HashMap();
    /* access modifiers changed from: private */
    public String lastName;
    /* access modifiers changed from: private */
    public String middleName;
    /* access modifiers changed from: private */
    public String organization;
    /* access modifiers changed from: private */
    public String organizationUnit;
    /* access modifiers changed from: private */
    public Map<String, String> otherSimpleFields = new HashMap();
    /* access modifiers changed from: private */
    public Map<String, String> otherUnescapableFields = new HashMap();
    /* access modifiers changed from: private */
    public String photoBinval;
    /* access modifiers changed from: private */
    public String photoMimeType;
    /* access modifiers changed from: private */
    public Map<String, String> workAddr = new HashMap();
    /* access modifiers changed from: private */
    public Map<String, String> workPhones = new HashMap();

    private interface ContentBuilder {
        void addTagContent();
    }

    public String getField(String str) {
        return this.otherSimpleFields.get(str);
    }

    public void setField(String str, String str2) {
        setField(str, str2, false);
    }

    public void setField(String str, String str2, boolean z) {
        if (!z) {
            this.otherSimpleFields.put(str, str2);
        } else {
            this.otherUnescapableFields.put(str, str2);
        }
    }

    public String getFirstName() {
        return this.firstName;
    }

    public void setFirstName(String str) {
        this.firstName = str;
        updateFN();
    }

    public String getLastName() {
        return this.lastName;
    }

    public void setLastName(String str) {
        this.lastName = str;
        updateFN();
    }

    public String getMiddleName() {
        return this.middleName;
    }

    public void setMiddleName(String str) {
        this.middleName = str;
        updateFN();
    }

    public String getNickName() {
        return this.otherSimpleFields.get("NICKNAME");
    }

    public void setNickName(String str) {
        this.otherSimpleFields.put("NICKNAME", str);
    }

    public String getEmailHome() {
        return this.emailHome;
    }

    public void setEmailHome(String str) {
        this.emailHome = str;
    }

    public String getEmailWork() {
        return this.emailWork;
    }

    public void setEmailWork(String str) {
        this.emailWork = str;
    }

    public String getJabberId() {
        return this.otherSimpleFields.get("JABBERID");
    }

    public void setJabberId(String str) {
        this.otherSimpleFields.put("JABBERID", str);
    }

    public String getOrganization() {
        return this.organization;
    }

    public void setOrganization(String str) {
        this.organization = str;
    }

    public String getOrganizationUnit() {
        return this.organizationUnit;
    }

    public void setOrganizationUnit(String str) {
        this.organizationUnit = str;
    }

    public String getAddressFieldHome(String str) {
        return this.homeAddr.get(str);
    }

    public void setAddressFieldHome(String str, String str2) {
        this.homeAddr.put(str, str2);
    }

    public String getAddressFieldWork(String str) {
        return this.workAddr.get(str);
    }

    public void setAddressFieldWork(String str, String str2) {
        this.workAddr.put(str, str2);
    }

    public void setPhoneHome(String str, String str2) {
        this.homePhones.put(str, str2);
    }

    public String getPhoneHome(String str) {
        return this.homePhones.get(str);
    }

    public void setPhoneWork(String str, String str2) {
        this.workPhones.put(str, str2);
    }

    public String getPhoneWork(String str) {
        return this.workPhones.get(str);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
     arg types: [java.util.logging.Level, java.lang.String, java.io.IOException]
     candidates:
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
    public void setAvatar(URL url) {
        byte[] bArr = new byte[0];
        try {
            bArr = getBytes(url);
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, "Error getting bytes from URL: " + url, (Throwable) e);
        }
        setAvatar(bArr);
    }

    public void removeAvatar() {
        this.photoBinval = null;
        this.photoMimeType = null;
    }

    public void setAvatar(byte[] bArr) {
        setAvatar(bArr, DEFAULT_MIME_TYPE);
    }

    public void setAvatar(byte[] bArr, String str) {
        if (bArr == null) {
            removeAvatar();
        } else {
            setAvatar(StringUtils.encodeBase64(bArr), str);
        }
    }

    public void setAvatar(String str, String str2) {
        this.photoBinval = str;
        this.photoMimeType = str2;
    }

    public void setEncodedImage(String str) {
        setAvatar(str, DEFAULT_MIME_TYPE);
    }

    public byte[] getAvatar() {
        if (this.photoBinval == null) {
            return null;
        }
        return StringUtils.decodeBase64(this.photoBinval);
    }

    public String getAvatarMimeType() {
        return this.photoMimeType;
    }

    public static byte[] getBytes(URL url) throws IOException {
        File file = new File(url.getPath());
        if (file.exists()) {
            return getFileBytes(file);
        }
        return null;
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x0024  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static byte[] getFileBytes(java.io.File r4) throws java.io.IOException {
        /*
            r2 = 0
            java.io.BufferedInputStream r1 = new java.io.BufferedInputStream     // Catch:{ all -> 0x002e }
            java.io.FileInputStream r0 = new java.io.FileInputStream     // Catch:{ all -> 0x002e }
            r0.<init>(r4)     // Catch:{ all -> 0x002e }
            r1.<init>(r0)     // Catch:{ all -> 0x002e }
            long r2 = r4.length()     // Catch:{ all -> 0x0021 }
            int r0 = (int) r2     // Catch:{ all -> 0x0021 }
            byte[] r0 = new byte[r0]     // Catch:{ all -> 0x0021 }
            int r2 = r1.read(r0)     // Catch:{ all -> 0x0021 }
            int r3 = r0.length     // Catch:{ all -> 0x0021 }
            if (r2 == r3) goto L_0x0028
            java.io.IOException r0 = new java.io.IOException     // Catch:{ all -> 0x0021 }
            java.lang.String r2 = "Entire file not read"
            r0.<init>(r2)     // Catch:{ all -> 0x0021 }
            throw r0     // Catch:{ all -> 0x0021 }
        L_0x0021:
            r0 = move-exception
        L_0x0022:
            if (r1 == 0) goto L_0x0027
            r1.close()
        L_0x0027:
            throw r0
        L_0x0028:
            if (r1 == 0) goto L_0x002d
            r1.close()
        L_0x002d:
            return r0
        L_0x002e:
            r0 = move-exception
            r1 = r2
            goto L_0x0022
        */
        throw new UnsupportedOperationException("Method not decompiled: org.jivesoftware.smackx.vcardtemp.packet.VCard.getFileBytes(java.io.File):byte[]");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
     arg types: [java.util.logging.Level, java.lang.String, java.security.NoSuchAlgorithmException]
     candidates:
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
    public String getAvatarHash() {
        byte[] avatar = getAvatar();
        if (avatar == null) {
            return null;
        }
        try {
            MessageDigest instance = MessageDigest.getInstance("SHA-1");
            instance.update(avatar);
            return StringUtils.encodeHex(instance.digest());
        } catch (NoSuchAlgorithmException e) {
            LOGGER.log(Level.SEVERE, "Failed to get message digest", (Throwable) e);
            return null;
        }
    }

    private void updateFN() {
        StringBuilder sb = new StringBuilder();
        if (this.firstName != null) {
            sb.append(StringUtils.escapeForXML(this.firstName)).append(' ');
        }
        if (this.middleName != null) {
            sb.append(StringUtils.escapeForXML(this.middleName)).append(' ');
        }
        if (this.lastName != null) {
            sb.append(StringUtils.escapeForXML(this.lastName));
        }
        setField("FN", sb.toString());
    }

    public void save(XMPPConnection xMPPConnection) throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        checkAuthenticated(xMPPConnection, true);
        setType(IQ.Type.SET);
        setFrom(xMPPConnection.getUser());
        xMPPConnection.createPacketCollectorAndSend(this).nextResultOrThrow();
    }

    public void load(XMPPConnection xMPPConnection) throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        checkAuthenticated(xMPPConnection, true);
        setFrom(xMPPConnection.getUser());
        doLoad(xMPPConnection, xMPPConnection.getUser());
    }

    public void load(XMPPConnection xMPPConnection, String str) throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        checkAuthenticated(xMPPConnection, false);
        setTo(str);
        doLoad(xMPPConnection, str);
    }

    private void doLoad(XMPPConnection xMPPConnection, String str) throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        setType(IQ.Type.GET);
        copyFieldsFrom((VCard) xMPPConnection.createPacketCollectorAndSend(this).nextResultOrThrow());
    }

    public String getChildElementXML() {
        StringBuilder sb = new StringBuilder();
        new VCardWriter(sb).write();
        return sb.toString();
    }

    private void copyFieldsFrom(VCard vCard) {
        for (Field field : VCard.class.getDeclaredFields()) {
            if (field.getDeclaringClass() == VCard.class && !Modifier.isFinal(field.getModifiers())) {
                try {
                    field.setAccessible(true);
                    field.set(this, field.get(vCard));
                } catch (IllegalAccessException e) {
                    throw new RuntimeException("This cannot happen:" + field, e);
                }
            }
        }
    }

    private void checkAuthenticated(XMPPConnection xMPPConnection, boolean z) {
        if (xMPPConnection == null) {
            throw new IllegalArgumentException("No connection was provided");
        } else if (!xMPPConnection.isAuthenticated()) {
            throw new IllegalArgumentException("XMPPConnection is not authenticated");
        } else if (z && xMPPConnection.isAnonymous()) {
            throw new IllegalArgumentException("XMPPConnection cannot be anonymous");
        }
    }

    /* access modifiers changed from: private */
    public boolean hasContent() {
        return hasNameField() || hasOrganizationFields() || this.emailHome != null || this.emailWork != null || this.otherSimpleFields.size() > 0 || this.otherUnescapableFields.size() > 0 || this.homeAddr.size() > 0 || this.homePhones.size() > 0 || this.workAddr.size() > 0 || this.workPhones.size() > 0 || this.photoBinval != null;
    }

    /* access modifiers changed from: private */
    public boolean hasNameField() {
        return (this.firstName == null && this.lastName == null && this.middleName == null) ? false : true;
    }

    /* access modifiers changed from: private */
    public boolean hasOrganizationFields() {
        return (this.organization == null && this.organizationUnit == null) ? false : true;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        VCard vCard = (VCard) obj;
        if (this.emailHome != null) {
            if (!this.emailHome.equals(vCard.emailHome)) {
                return false;
            }
        } else if (vCard.emailHome != null) {
            return false;
        }
        if (this.emailWork != null) {
            if (!this.emailWork.equals(vCard.emailWork)) {
                return false;
            }
        } else if (vCard.emailWork != null) {
            return false;
        }
        if (this.firstName != null) {
            if (!this.firstName.equals(vCard.firstName)) {
                return false;
            }
        } else if (vCard.firstName != null) {
            return false;
        }
        if (!this.homeAddr.equals(vCard.homeAddr) || !this.homePhones.equals(vCard.homePhones)) {
            return false;
        }
        if (this.lastName != null) {
            if (!this.lastName.equals(vCard.lastName)) {
                return false;
            }
        } else if (vCard.lastName != null) {
            return false;
        }
        if (this.middleName != null) {
            if (!this.middleName.equals(vCard.middleName)) {
                return false;
            }
        } else if (vCard.middleName != null) {
            return false;
        }
        if (this.organization != null) {
            if (!this.organization.equals(vCard.organization)) {
                return false;
            }
        } else if (vCard.organization != null) {
            return false;
        }
        if (this.organizationUnit != null) {
            if (!this.organizationUnit.equals(vCard.organizationUnit)) {
                return false;
            }
        } else if (vCard.organizationUnit != null) {
            return false;
        }
        if (!this.otherSimpleFields.equals(vCard.otherSimpleFields) || !this.workAddr.equals(vCard.workAddr)) {
            return false;
        }
        if (this.photoBinval != null) {
            if (!this.photoBinval.equals(vCard.photoBinval)) {
                return false;
            }
        } else if (vCard.photoBinval != null) {
            return false;
        }
        return this.workPhones.equals(vCard.workPhones);
    }

    public int hashCode() {
        int i;
        int i2;
        int i3;
        int i4;
        int i5;
        int i6;
        int i7 = 0;
        int hashCode = ((this.firstName != null ? this.firstName.hashCode() : 0) + (((((((this.homePhones.hashCode() * 29) + this.workPhones.hashCode()) * 29) + this.homeAddr.hashCode()) * 29) + this.workAddr.hashCode()) * 29)) * 29;
        if (this.lastName != null) {
            i = this.lastName.hashCode();
        } else {
            i = 0;
        }
        int i8 = (i + hashCode) * 29;
        if (this.middleName != null) {
            i2 = this.middleName.hashCode();
        } else {
            i2 = 0;
        }
        int i9 = (i2 + i8) * 29;
        if (this.emailHome != null) {
            i3 = this.emailHome.hashCode();
        } else {
            i3 = 0;
        }
        int i10 = (i3 + i9) * 29;
        if (this.emailWork != null) {
            i4 = this.emailWork.hashCode();
        } else {
            i4 = 0;
        }
        int i11 = (i4 + i10) * 29;
        if (this.organization != null) {
            i5 = this.organization.hashCode();
        } else {
            i5 = 0;
        }
        int i12 = (i5 + i11) * 29;
        if (this.organizationUnit != null) {
            i6 = this.organizationUnit.hashCode();
        } else {
            i6 = 0;
        }
        int hashCode2 = (((i6 + i12) * 29) + this.otherSimpleFields.hashCode()) * 29;
        if (this.photoBinval != null) {
            i7 = this.photoBinval.hashCode();
        }
        return hashCode2 + i7;
    }

    public String toString() {
        return getChildElementXML();
    }

    private class VCardWriter {
        /* access modifiers changed from: private */
        public final StringBuilder sb;

        VCardWriter(StringBuilder sb2) {
            this.sb = sb2;
        }

        public void write() {
            appendTag(VCardManager.ELEMENT, "xmlns", VCardManager.NAMESPACE, VCard.this.hasContent(), new ContentBuilder() {
                public void addTagContent() {
                    VCardWriter.this.buildActualContent();
                }
            });
        }

        /* access modifiers changed from: private */
        public void buildActualContent() {
            if (VCard.this.hasNameField()) {
                appendN();
            }
            appendOrganization();
            appendGenericFields();
            appendPhoto();
            appendEmail(VCard.this.emailWork, "WORK");
            appendEmail(VCard.this.emailHome, "HOME");
            appendPhones(VCard.this.workPhones, "WORK");
            appendPhones(VCard.this.homePhones, "HOME");
            appendAddress(VCard.this.workAddr, "WORK");
            appendAddress(VCard.this.homeAddr, "HOME");
        }

        private void appendPhoto() {
            if (VCard.this.photoBinval != null) {
                appendTag("PHOTO", true, new ContentBuilder() {
                    public void addTagContent() {
                        VCardWriter.this.appendTag("BINVAL", VCard.this.photoBinval);
                        VCardWriter.this.appendTag("TYPE", StringUtils.escapeForXML(VCard.this.photoMimeType));
                    }
                });
            }
        }

        private void appendEmail(final String str, final String str2) {
            if (str != null) {
                appendTag("EMAIL", true, new ContentBuilder() {
                    public void addTagContent() {
                        VCardWriter.this.appendEmptyTag(str2);
                        VCardWriter.this.appendEmptyTag("INTERNET");
                        VCardWriter.this.appendEmptyTag("PREF");
                        VCardWriter.this.appendTag("USERID", StringUtils.escapeForXML(str));
                    }
                });
            }
        }

        private void appendPhones(Map<String, String> map, final String str) {
            for (final Map.Entry next : map.entrySet()) {
                appendTag("TEL", true, new ContentBuilder() {
                    public void addTagContent() {
                        VCardWriter.this.appendEmptyTag(next.getKey());
                        VCardWriter.this.appendEmptyTag(str);
                        VCardWriter.this.appendTag("NUMBER", StringUtils.escapeForXML((String) next.getValue()));
                    }
                });
            }
        }

        private void appendAddress(final Map<String, String> map, final String str) {
            if (map.size() > 0) {
                appendTag("ADR", true, new ContentBuilder() {
                    public void addTagContent() {
                        VCardWriter.this.appendEmptyTag(str);
                        for (Map.Entry entry : map.entrySet()) {
                            VCardWriter.this.appendTag((String) entry.getKey(), StringUtils.escapeForXML((String) entry.getValue()));
                        }
                    }
                });
            }
        }

        /* access modifiers changed from: private */
        public void appendEmptyTag(Object obj) {
            this.sb.append('<').append(obj).append("/>");
        }

        private void appendGenericFields() {
            for (Map.Entry entry : VCard.this.otherSimpleFields.entrySet()) {
                appendTag(((String) entry.getKey()).toString(), StringUtils.escapeForXML((String) entry.getValue()));
            }
            for (Map.Entry entry2 : VCard.this.otherUnescapableFields.entrySet()) {
                appendTag(((String) entry2.getKey()).toString(), (CharSequence) entry2.getValue());
            }
        }

        private void appendOrganization() {
            if (VCard.this.hasOrganizationFields()) {
                appendTag("ORG", true, new ContentBuilder() {
                    public void addTagContent() {
                        VCardWriter.this.appendTag("ORGNAME", StringUtils.escapeForXML(VCard.this.organization));
                        VCardWriter.this.appendTag("ORGUNIT", StringUtils.escapeForXML(VCard.this.organizationUnit));
                    }
                });
            }
        }

        private void appendN() {
            appendTag("N", true, new ContentBuilder() {
                public void addTagContent() {
                    VCardWriter.this.appendTag("FAMILY", StringUtils.escapeForXML(VCard.this.lastName));
                    VCardWriter.this.appendTag("GIVEN", StringUtils.escapeForXML(VCard.this.firstName));
                    VCardWriter.this.appendTag("MIDDLE", StringUtils.escapeForXML(VCard.this.middleName));
                }
            });
        }

        private void appendTag(String str, String str2, String str3, boolean z, ContentBuilder contentBuilder) {
            this.sb.append('<').append(str);
            if (str2 != null) {
                this.sb.append(' ').append(str2).append('=').append('\'').append(str3).append('\'');
            }
            if (z) {
                this.sb.append('>');
                contentBuilder.addTagContent();
                this.sb.append("</").append(str).append(">\n");
                return;
            }
            this.sb.append("/>\n");
        }

        private void appendTag(String str, boolean z, ContentBuilder contentBuilder) {
            appendTag(str, null, null, z, contentBuilder);
        }

        /* access modifiers changed from: private */
        public void appendTag(String str, final CharSequence charSequence) {
            if (charSequence != null) {
                appendTag(str, true, new ContentBuilder() {
                    public void addTagContent() {
                        VCardWriter.this.sb.append(charSequence.toString().trim());
                    }
                });
            }
        }
    }
}
