package org.jivesoftware.smackx.xevent;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.filter.PacketExtensionFilter;
import org.jivesoftware.smack.filter.PacketFilter;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smackx.xevent.packet.MessageEvent;

public class MessageEventManager {
    private static final Logger LOGGER = Logger.getLogger(MessageEventManager.class.getName());
    private XMPPConnection con;
    private List<MessageEventNotificationListener> messageEventNotificationListeners = new ArrayList();
    private List<MessageEventRequestListener> messageEventRequestListeners = new ArrayList();
    private PacketFilter packetFilter = new PacketExtensionFilter("x", "jabber:x:event");
    private PacketListener packetListener;

    public MessageEventManager(XMPPConnection xMPPConnection) {
        this.con = xMPPConnection;
        init();
    }

    public static void addNotificationsRequests(Message message, boolean z, boolean z2, boolean z3, boolean z4) {
        MessageEvent messageEvent = new MessageEvent();
        messageEvent.setOffline(z);
        messageEvent.setDelivered(z2);
        messageEvent.setDisplayed(z3);
        messageEvent.setComposing(z4);
        message.addExtension(messageEvent);
    }

    public void addMessageEventRequestListener(MessageEventRequestListener messageEventRequestListener) {
        synchronized (this.messageEventRequestListeners) {
            if (!this.messageEventRequestListeners.contains(messageEventRequestListener)) {
                this.messageEventRequestListeners.add(messageEventRequestListener);
            }
        }
    }

    public void removeMessageEventRequestListener(MessageEventRequestListener messageEventRequestListener) {
        synchronized (this.messageEventRequestListeners) {
            this.messageEventRequestListeners.remove(messageEventRequestListener);
        }
    }

    public void addMessageEventNotificationListener(MessageEventNotificationListener messageEventNotificationListener) {
        synchronized (this.messageEventNotificationListeners) {
            if (!this.messageEventNotificationListeners.contains(messageEventNotificationListener)) {
                this.messageEventNotificationListeners.add(messageEventNotificationListener);
            }
        }
    }

    public void removeMessageEventNotificationListener(MessageEventNotificationListener messageEventNotificationListener) {
        synchronized (this.messageEventNotificationListeners) {
            this.messageEventNotificationListeners.remove(messageEventNotificationListener);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
     arg types: [java.util.logging.Level, java.lang.String, java.lang.Exception]
     candidates:
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
    /* access modifiers changed from: private */
    public void fireMessageEventRequestListeners(String str, String str2, String str3) {
        MessageEventRequestListener[] messageEventRequestListenerArr;
        synchronized (this.messageEventRequestListeners) {
            messageEventRequestListenerArr = new MessageEventRequestListener[this.messageEventRequestListeners.size()];
            this.messageEventRequestListeners.toArray(messageEventRequestListenerArr);
        }
        try {
            Method declaredMethod = MessageEventRequestListener.class.getDeclaredMethod(str3, String.class, String.class, MessageEventManager.class);
            for (MessageEventRequestListener invoke : messageEventRequestListenerArr) {
                declaredMethod.invoke(invoke, str, str2, this);
            }
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, "Error while invoking MessageEventRequestListener", (Throwable) e);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
     arg types: [java.util.logging.Level, java.lang.String, java.lang.Exception]
     candidates:
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
    /* access modifiers changed from: private */
    public void fireMessageEventNotificationListeners(String str, String str2, String str3) {
        MessageEventNotificationListener[] messageEventNotificationListenerArr;
        synchronized (this.messageEventNotificationListeners) {
            messageEventNotificationListenerArr = new MessageEventNotificationListener[this.messageEventNotificationListeners.size()];
            this.messageEventNotificationListeners.toArray(messageEventNotificationListenerArr);
        }
        try {
            Method declaredMethod = MessageEventNotificationListener.class.getDeclaredMethod(str3, String.class, String.class);
            for (MessageEventNotificationListener invoke : messageEventNotificationListenerArr) {
                declaredMethod.invoke(invoke, str, str2);
            }
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, "Error while invoking MessageEventNotificationListener", (Throwable) e);
        }
    }

    private void init() {
        this.packetListener = new PacketListener() {
            public void processPacket(Packet packet) {
                Message message = (Message) packet;
                MessageEvent messageEvent = (MessageEvent) message.getExtension("x", "jabber:x:event");
                if (messageEvent.isMessageEventRequest()) {
                    for (String concat : messageEvent.getEventTypes()) {
                        MessageEventManager.this.fireMessageEventRequestListeners(message.getFrom(), message.getPacketID(), concat.concat("NotificationRequested"));
                    }
                    return;
                }
                for (String concat2 : messageEvent.getEventTypes()) {
                    MessageEventManager.this.fireMessageEventNotificationListeners(message.getFrom(), messageEvent.getPacketID(), concat2.concat("Notification"));
                }
            }
        };
        this.con.addPacketListener(this.packetListener, this.packetFilter);
    }

    public void sendDeliveredNotification(String str, String str2) throws SmackException.NotConnectedException {
        Message message = new Message(str);
        MessageEvent messageEvent = new MessageEvent();
        messageEvent.setDelivered(true);
        messageEvent.setPacketID(str2);
        message.addExtension(messageEvent);
        this.con.sendPacket(message);
    }

    public void sendDisplayedNotification(String str, String str2) throws SmackException.NotConnectedException {
        Message message = new Message(str);
        MessageEvent messageEvent = new MessageEvent();
        messageEvent.setDisplayed(true);
        messageEvent.setPacketID(str2);
        message.addExtension(messageEvent);
        this.con.sendPacket(message);
    }

    public void sendComposingNotification(String str, String str2) throws SmackException.NotConnectedException {
        Message message = new Message(str);
        MessageEvent messageEvent = new MessageEvent();
        messageEvent.setComposing(true);
        messageEvent.setPacketID(str2);
        message.addExtension(messageEvent);
        this.con.sendPacket(message);
    }

    public void sendCancelledNotification(String str, String str2) throws SmackException.NotConnectedException {
        Message message = new Message(str);
        MessageEvent messageEvent = new MessageEvent();
        messageEvent.setCancelled(true);
        messageEvent.setPacketID(str2);
        message.addExtension(messageEvent);
        this.con.sendPacket(message);
    }

    public void destroy() {
        if (this.con != null) {
            this.con.removePacketListener(this.packetListener);
        }
    }

    /* access modifiers changed from: protected */
    public void finalize() throws Throwable {
        destroy();
        super.finalize();
    }
}
