package org.jivesoftware.smackx.ping;

import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jivesoftware.smack.AbstractConnectionListener;
import org.jivesoftware.smack.ConnectionCreationListener;
import org.jivesoftware.smack.Manager;
import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.filter.AndFilter;
import org.jivesoftware.smack.filter.IQTypeFilter;
import org.jivesoftware.smack.filter.PacketFilter;
import org.jivesoftware.smack.filter.PacketTypeFilter;
import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smackx.disco.ServiceDiscoveryManager;
import org.jivesoftware.smackx.ping.packet.Ping;
import org.jivesoftware.smackx.ping.packet.Pong;

public class PingManager extends Manager {
    private static final Map<XMPPConnection, PingManager> INSTANCES = Collections.synchronizedMap(new WeakHashMap());
    /* access modifiers changed from: private */
    public static final Logger LOGGER = Logger.getLogger(PingManager.class.getName());
    public static final String NAMESPACE = "urn:xmpp:ping";
    private static final PacketFilter PING_PACKET_FILTER = new AndFilter(new PacketTypeFilter(Ping.class), new IQTypeFilter(IQ.Type.GET));
    private static final PacketFilter PONG_PACKET_FILTER = new AndFilter(new PacketTypeFilter(Pong.class), new IQTypeFilter(IQ.Type.RESULT));
    private static int defaultPingInterval = 1800;
    /* access modifiers changed from: private */
    public long lastPongReceived = -1;
    private ScheduledFuture<?> nextAutomaticPing;
    /* access modifiers changed from: private */
    public final Set<PingFailedListener> pingFailedListeners = Collections.synchronizedSet(new HashSet());
    /* access modifiers changed from: private */
    public int pingInterval = defaultPingInterval;
    private final Runnable pingServerRunnable = new Runnable() {
        private static final int DELTA = 1000;
        private static final int TRIES = 3;

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
         arg types: [java.util.logging.Level, java.lang.String, org.jivesoftware.smack.SmackException]
         candidates:
          ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
          ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
          ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
          ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
        public void run() {
            PingManager.LOGGER.fine("ServerPingTask run()");
            XMPPConnection access$500 = PingManager.this.connection();
            if (access$500 != null && PingManager.this.pingInterval > 0) {
                long lastReceivedPong = PingManager.this.getLastReceivedPong();
                if (lastReceivedPong > 0) {
                    int access$600 = (int) ((((long) (PingManager.this.pingInterval * DELTA)) - (System.currentTimeMillis() - lastReceivedPong)) / 1000);
                    if (access$600 > 0) {
                        PingManager.this.maybeSchedulePingServerTask(access$600);
                        return;
                    }
                }
                if (access$500.isAuthenticated()) {
                    boolean z = false;
                    for (int i = 0; i < 3; i++) {
                        if (i != 0) {
                            try {
                                Thread.sleep(1000);
                            } catch (InterruptedException e) {
                                return;
                            }
                        }
                        try {
                            z = PingManager.this.pingMyServer(false);
                        } catch (SmackException e2) {
                            PingManager.LOGGER.log(Level.WARNING, "SmackError while pinging server", (Throwable) e2);
                            z = false;
                        }
                        if (z) {
                            break;
                        }
                    }
                    PingManager.LOGGER.fine("ServerPingTask res=" + z);
                    if (!z) {
                        for (PingFailedListener pingFailed : PingManager.this.pingFailedListeners) {
                            pingFailed.pingFailed();
                        }
                        return;
                    }
                    PingManager.this.maybeSchedulePingServerTask();
                    return;
                }
                PingManager.LOGGER.warning("ServerPingTask: XMPPConnection was not authenticated");
            }
        }
    };

    static {
        XMPPConnection.addConnectionCreationListener(new ConnectionCreationListener() {
            public void connectionCreated(XMPPConnection xMPPConnection) {
                PingManager.getInstanceFor(xMPPConnection);
            }
        });
    }

    public static synchronized PingManager getInstanceFor(XMPPConnection xMPPConnection) {
        PingManager pingManager;
        synchronized (PingManager.class) {
            pingManager = INSTANCES.get(xMPPConnection);
            if (pingManager == null) {
                pingManager = new PingManager(xMPPConnection);
            }
        }
        return pingManager;
    }

    public static void setDefaultPingInterval(int i) {
        defaultPingInterval = i;
    }

    private PingManager(XMPPConnection xMPPConnection) {
        super(xMPPConnection);
        ServiceDiscoveryManager.getInstanceFor(xMPPConnection).addFeature(NAMESPACE);
        INSTANCES.put(xMPPConnection, this);
        xMPPConnection.addPacketListener(new PacketListener() {
            public void processPacket(Packet packet) throws SmackException.NotConnectedException {
                PingManager.this.connection().sendPacket(new Pong(packet));
            }
        }, PING_PACKET_FILTER);
        xMPPConnection.addPacketListener(new PacketListener() {
            public void processPacket(Packet packet) throws SmackException.NotConnectedException {
                long unused = PingManager.this.lastPongReceived = System.currentTimeMillis();
            }
        }, PONG_PACKET_FILTER);
        xMPPConnection.addConnectionListener(new AbstractConnectionListener() {
            public void authenticated(XMPPConnection xMPPConnection) {
                PingManager.this.maybeSchedulePingServerTask();
            }

            public void connectionClosed() {
                PingManager.this.maybeStopPingServerTask();
            }

            public void connectionClosedOnError(Exception exc) {
                PingManager.this.maybeStopPingServerTask();
            }
        });
        maybeSchedulePingServerTask();
    }

    public boolean ping(String str, long j) throws SmackException.NotConnectedException, SmackException.NoResponseException {
        try {
            connection().createPacketCollectorAndSend(new Ping(str)).nextResultOrThrow(j);
            return true;
        } catch (XMPPException e) {
            return str.equals(connection().getServiceName());
        }
    }

    public boolean ping(String str) throws SmackException.NotConnectedException, SmackException.NoResponseException {
        return ping(str, connection().getPacketReplyTimeout());
    }

    public boolean isPingSupported(String str) throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        return ServiceDiscoveryManager.getInstanceFor(connection()).supportsFeature(str, NAMESPACE);
    }

    public boolean pingMyServer() throws SmackException.NotConnectedException {
        return pingMyServer(true);
    }

    public boolean pingMyServer(boolean z) throws SmackException.NotConnectedException {
        boolean z2;
        try {
            z2 = ping(connection().getServiceName());
        } catch (SmackException.NoResponseException e) {
            z2 = false;
        }
        if (!z2 && z) {
            for (PingFailedListener pingFailed : this.pingFailedListeners) {
                pingFailed.pingFailed();
            }
        }
        return z2;
    }

    public void setPingInterval(int i) {
        this.pingInterval = i;
        maybeSchedulePingServerTask();
    }

    public int getPingInterval() {
        return this.pingInterval;
    }

    public void registerPingFailedListener(PingFailedListener pingFailedListener) {
        this.pingFailedListeners.add(pingFailedListener);
    }

    public void unregisterPingFailedListener(PingFailedListener pingFailedListener) {
        this.pingFailedListeners.remove(pingFailedListener);
    }

    public long getLastReceivedPong() {
        return this.lastPongReceived;
    }

    /* access modifiers changed from: private */
    public void maybeSchedulePingServerTask() {
        maybeSchedulePingServerTask(0);
    }

    /* access modifiers changed from: private */
    public synchronized void maybeSchedulePingServerTask(int i) {
        maybeStopPingServerTask();
        if (this.pingInterval > 0) {
            int i2 = this.pingInterval - i;
            LOGGER.fine("Scheduling ServerPingTask in " + i2 + " seconds (pingInterval=" + this.pingInterval + ", delta=" + i + ")");
            this.nextAutomaticPing = schedule(this.pingServerRunnable, (long) i2, TimeUnit.SECONDS);
        }
    }

    /* access modifiers changed from: private */
    public void maybeStopPingServerTask() {
        if (this.nextAutomaticPing != null) {
            this.nextAutomaticPing.cancel(true);
            this.nextAutomaticPing = null;
        }
    }
}
