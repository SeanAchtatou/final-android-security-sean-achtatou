package org.jivesoftware.smackx.ping.packet;

import org.jivesoftware.smack.packet.IQ;

public class Ping extends IQ {
    public static final String ELEMENT = "ping";

    public Ping() {
    }

    public Ping(String str) {
        setTo(str);
        setType(IQ.Type.GET);
    }

    public String getChildElementXML() {
        return "<ping xmlns='urn:xmpp:ping' />";
    }
}
