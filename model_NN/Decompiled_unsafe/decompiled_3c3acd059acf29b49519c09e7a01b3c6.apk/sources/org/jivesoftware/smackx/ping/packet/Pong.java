package org.jivesoftware.smackx.ping.packet;

import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.packet.Packet;

public class Pong extends IQ {
    public Pong(Packet packet) {
        setType(IQ.Type.RESULT);
        setFrom(packet.getTo());
        setTo(packet.getFrom());
        setPacketID(packet.getPacketID());
    }

    public String getChildElementXML() {
        return null;
    }
}
