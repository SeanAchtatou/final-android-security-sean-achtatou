package org.jivesoftware.smackx.disco.packet;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.util.XmlStringBuilder;
import org.jivesoftware.smackx.amp.packet.AMPExtension;
import org.jivesoftware.smackx.xdata.packet.DataForm;

public class DiscoverItems extends IQ {
    public static final String NAMESPACE = "http://jabber.org/protocol/disco#items";
    private final List<Item> items = new LinkedList();
    private String node;

    public void addItem(Item item) {
        this.items.add(item);
    }

    public void addItems(Collection<Item> collection) {
        if (collection != null) {
            for (Item addItem : collection) {
                addItem(addItem);
            }
        }
    }

    public List<Item> getItems() {
        return Collections.unmodifiableList(this.items);
    }

    public String getNode() {
        return this.node;
    }

    public void setNode(String str) {
        this.node = str;
    }

    public XmlStringBuilder getChildElementXML() {
        XmlStringBuilder xmlStringBuilder = new XmlStringBuilder();
        xmlStringBuilder.halfOpenElement("query");
        xmlStringBuilder.xmlnsAttribute(NAMESPACE);
        xmlStringBuilder.optAttribute("node", getNode());
        xmlStringBuilder.rightAngelBracket();
        for (Item xml : this.items) {
            xmlStringBuilder.append(xml.toXML());
        }
        xmlStringBuilder.closeElement("query");
        return xmlStringBuilder;
    }

    public static class Item {
        public static final String REMOVE_ACTION = "remove";
        public static final String UPDATE_ACTION = "update";
        private String action;
        private String entityID;
        private String name;
        private String node;

        public Item(String str) {
            this.entityID = str;
        }

        public String getEntityID() {
            return this.entityID;
        }

        public String getName() {
            return this.name;
        }

        public void setName(String str) {
            this.name = str;
        }

        public String getNode() {
            return this.node;
        }

        public void setNode(String str) {
            this.node = str;
        }

        public String getAction() {
            return this.action;
        }

        public void setAction(String str) {
            this.action = str;
        }

        public XmlStringBuilder toXML() {
            XmlStringBuilder xmlStringBuilder = new XmlStringBuilder();
            xmlStringBuilder.halfOpenElement(DataForm.Item.ELEMENT);
            xmlStringBuilder.attribute("jid", this.entityID);
            xmlStringBuilder.optAttribute("name", this.name);
            xmlStringBuilder.optAttribute("node", this.node);
            xmlStringBuilder.optAttribute(AMPExtension.Action.ATTRIBUTE_NAME, this.action);
            xmlStringBuilder.closeEmptyElement();
            return xmlStringBuilder;
        }
    }
}
