package org.jivesoftware.smackx.disco.packet;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.util.XmlStringBuilder;

public class DiscoverInfo extends IQ implements Cloneable {
    public static final String NAMESPACE = "http://jabber.org/protocol/disco#info";
    private final List<Feature> features = new LinkedList();
    private final List<Identity> identities = new LinkedList();
    private String node;

    public DiscoverInfo() {
    }

    public DiscoverInfo(DiscoverInfo discoverInfo) {
        super(discoverInfo);
        setNode(discoverInfo.getNode());
        for (Feature clone : discoverInfo.features) {
            addFeature(clone.clone());
        }
        for (Identity clone2 : discoverInfo.identities) {
            addIdentity(clone2.clone());
        }
    }

    public void addFeature(String str) {
        addFeature(new Feature(str));
    }

    public void addFeatures(Collection<String> collection) {
        if (collection != null) {
            for (String addFeature : collection) {
                addFeature(addFeature);
            }
        }
    }

    private void addFeature(Feature feature) {
        this.features.add(feature);
    }

    public List<Feature> getFeatures() {
        return Collections.unmodifiableList(this.features);
    }

    public void addIdentity(Identity identity) {
        this.identities.add(identity);
    }

    public void addIdentities(Collection<Identity> collection) {
        if (collection != null) {
            this.identities.addAll(collection);
        }
    }

    public List<Identity> getIdentities() {
        return Collections.unmodifiableList(this.identities);
    }

    public String getNode() {
        return this.node;
    }

    public void setNode(String str) {
        this.node = str;
    }

    public boolean containsFeature(String str) {
        for (Feature var : getFeatures()) {
            if (str.equals(var.getVar())) {
                return true;
            }
        }
        return false;
    }

    public CharSequence getChildElementXML() {
        XmlStringBuilder xmlStringBuilder = new XmlStringBuilder();
        xmlStringBuilder.halfOpenElement("query");
        xmlStringBuilder.xmlnsAttribute(NAMESPACE);
        xmlStringBuilder.optAttribute("node", getNode());
        xmlStringBuilder.rightAngelBracket();
        for (Identity xml : this.identities) {
            xmlStringBuilder.append(xml.toXML());
        }
        for (Feature xml2 : this.features) {
            xmlStringBuilder.append(xml2.toXML());
        }
        xmlStringBuilder.append(getExtensionsXML());
        xmlStringBuilder.closeElement("query");
        return xmlStringBuilder;
    }

    public boolean containsDuplicateIdentities() {
        LinkedList<Identity> linkedList = new LinkedList<>();
        for (Identity next : this.identities) {
            for (Identity equals : linkedList) {
                if (next.equals(equals)) {
                    return true;
                }
            }
            linkedList.add(next);
        }
        return false;
    }

    public boolean containsDuplicateFeatures() {
        LinkedList<Feature> linkedList = new LinkedList<>();
        for (Feature next : this.features) {
            for (Feature equals : linkedList) {
                if (next.equals(equals)) {
                    return true;
                }
            }
            linkedList.add(next);
        }
        return false;
    }

    public DiscoverInfo clone() {
        return new DiscoverInfo(this);
    }

    public static class Identity implements Comparable<Identity>, Cloneable {
        private final String category;
        private String lang;
        private String name;
        private final String type;

        public Identity(Identity identity) {
            this(identity.category, identity.name, identity.type);
            this.lang = identity.lang;
        }

        public Identity(String str, String str2, String str3) {
            if (str == null || str3 == null) {
                throw new IllegalArgumentException("category and type cannot be null");
            }
            this.category = str;
            this.name = str2;
            this.type = str3;
        }

        public String getCategory() {
            return this.category;
        }

        public String getName() {
            return this.name;
        }

        public void setName(String str) {
            this.name = str;
        }

        public String getType() {
            return this.type;
        }

        public void setLanguage(String str) {
            this.lang = str;
        }

        public String getLanguage() {
            return this.lang;
        }

        public XmlStringBuilder toXML() {
            XmlStringBuilder xmlStringBuilder = new XmlStringBuilder();
            xmlStringBuilder.halfOpenElement("identity");
            xmlStringBuilder.xmllangAttribute(this.lang);
            xmlStringBuilder.attribute("category", this.category);
            xmlStringBuilder.optAttribute("name", this.name);
            xmlStringBuilder.optAttribute("type", this.type);
            xmlStringBuilder.closeEmptyElement();
            return xmlStringBuilder;
        }

        public boolean equals(Object obj) {
            if (obj == null) {
                return false;
            }
            if (obj == this) {
                return true;
            }
            if (obj.getClass() != getClass()) {
                return false;
            }
            Identity identity = (Identity) obj;
            if (!this.category.equals(identity.category)) {
                return false;
            }
            if (!(identity.lang == null ? "" : identity.lang).equals(this.lang == null ? "" : this.lang)) {
                return false;
            }
            if (!(identity.type == null ? "" : identity.type).equals(this.type == null ? "" : this.type)) {
                return false;
            }
            if (!(this.name == null ? "" : identity.name).equals(identity.name == null ? "" : identity.name)) {
                return false;
            }
            return true;
        }

        public int hashCode() {
            int i = 0;
            int hashCode = ((this.type == null ? 0 : this.type.hashCode()) + (((this.lang == null ? 0 : this.lang.hashCode()) + ((this.category.hashCode() + 37) * 37)) * 37)) * 37;
            if (this.name != null) {
                i = this.name.hashCode();
            }
            return hashCode + i;
        }

        public int compareTo(Identity identity) {
            String str = identity.lang == null ? "" : identity.lang;
            String str2 = this.lang == null ? "" : this.lang;
            String str3 = identity.type == null ? "" : identity.type;
            String str4 = this.type == null ? "" : this.type;
            if (!this.category.equals(identity.category)) {
                return this.category.compareTo(identity.category);
            }
            if (!str4.equals(str3)) {
                return str4.compareTo(str3);
            }
            if (str2.equals(str)) {
                return 0;
            }
            return str2.compareTo(str);
        }

        public Identity clone() {
            return new Identity(this);
        }
    }

    public static class Feature implements Cloneable {
        private final String variable;

        public Feature(Feature feature) {
            this.variable = feature.variable;
        }

        public Feature(String str) {
            if (str == null) {
                throw new IllegalArgumentException("variable cannot be null");
            }
            this.variable = str;
        }

        public String getVar() {
            return this.variable;
        }

        public XmlStringBuilder toXML() {
            XmlStringBuilder xmlStringBuilder = new XmlStringBuilder();
            xmlStringBuilder.halfOpenElement("feature");
            xmlStringBuilder.attribute("var", this.variable);
            xmlStringBuilder.closeEmptyElement();
            return xmlStringBuilder;
        }

        public boolean equals(Object obj) {
            if (obj == null) {
                return false;
            }
            if (obj == this) {
                return true;
            }
            if (obj.getClass() == getClass()) {
                return this.variable.equals(((Feature) obj).variable);
            }
            return false;
        }

        public int hashCode() {
            return this.variable.hashCode() * 37;
        }

        public Feature clone() {
            return new Feature(this);
        }
    }
}
