package org.jivesoftware.smackx.iqprivate;

import java.util.Hashtable;
import java.util.Map;
import java.util.WeakHashMap;
import org.jivesoftware.smack.Manager;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.provider.IQProvider;
import org.jivesoftware.smackx.iqprivate.packet.PrivateData;
import org.jivesoftware.smackx.iqprivate.provider.PrivateDataProvider;

public class PrivateDataManager extends Manager {
    private static final Map<XMPPConnection, PrivateDataManager> instances = new WeakHashMap();
    private static Map<String, PrivateDataProvider> privateDataProviders = new Hashtable();

    public static synchronized PrivateDataManager getInstanceFor(XMPPConnection xMPPConnection) {
        PrivateDataManager privateDataManager;
        synchronized (PrivateDataManager.class) {
            privateDataManager = instances.get(xMPPConnection);
            if (privateDataManager == null) {
                privateDataManager = new PrivateDataManager(xMPPConnection);
            }
        }
        return privateDataManager;
    }

    public static PrivateDataProvider getPrivateDataProvider(String str, String str2) {
        return privateDataProviders.get(getProviderKey(str, str2));
    }

    public static void addPrivateDataProvider(String str, String str2, PrivateDataProvider privateDataProvider) {
        privateDataProviders.put(getProviderKey(str, str2), privateDataProvider);
    }

    public static void removePrivateDataProvider(String str, String str2) {
        privateDataProviders.remove(getProviderKey(str, str2));
    }

    private PrivateDataManager(XMPPConnection xMPPConnection) {
        super(xMPPConnection);
        instances.put(xMPPConnection, this);
    }

    public PrivateData getPrivateData(final String str, final String str2) throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        AnonymousClass1 r0 = new IQ() {
            public String getChildElementXML() {
                StringBuilder sb = new StringBuilder();
                sb.append("<query xmlns=\"jabber:iq:private\">");
                sb.append("<").append(str).append(" xmlns=\"").append(str2).append("\"/>");
                sb.append("</query>");
                return sb.toString();
            }
        };
        r0.setType(IQ.Type.GET);
        return ((PrivateDataResult) connection().createPacketCollectorAndSend(r0).nextResultOrThrow()).getPrivateData();
    }

    public void setPrivateData(final PrivateData privateData) throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        AnonymousClass2 r0 = new IQ() {
            public String getChildElementXML() {
                return "<query xmlns=\"jabber:iq:private\">" + privateData.toXML() + "</query>";
            }
        };
        r0.setType(IQ.Type.SET);
        connection().createPacketCollectorAndSend(r0).nextResultOrThrow();
    }

    private static String getProviderKey(String str, String str2) {
        StringBuilder sb = new StringBuilder();
        sb.append("<").append(str).append("/><").append(str2).append("/>");
        return sb.toString();
    }

    public static class PrivateDataIQProvider implements IQProvider {
        /* JADX WARN: Failed to insert an additional move for type inference into block B:35:0x0020 */
        /* JADX INFO: additional move instructions added (1) to help type inference */
        /* JADX WARN: Type inference failed for: r0v10, types: [org.jivesoftware.smackx.iqprivate.packet.PrivateData] */
        /* JADX WARNING: Multi-variable type inference failed */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public org.jivesoftware.smack.packet.IQ parseIQ(org.xmlpull.v1.XmlPullParser r13) throws java.lang.Exception {
            /*
                r12 = this;
                r10 = 3
                r9 = 2
                r2 = 1
                r4 = 0
                r0 = 0
                r1 = r4
            L_0x0006:
                if (r1 != 0) goto L_0x0073
                int r3 = r13.next()
                if (r3 != r9) goto L_0x0062
                java.lang.String r5 = r13.getName()
                java.lang.String r3 = r13.getNamespace()
                org.jivesoftware.smackx.iqprivate.provider.PrivateDataProvider r0 = org.jivesoftware.smackx.iqprivate.PrivateDataManager.getPrivateDataProvider(r5, r3)
                if (r0 == 0) goto L_0x0027
                org.jivesoftware.smackx.iqprivate.packet.PrivateData r0 = r0.parsePrivateData(r13)
            L_0x0020:
                r11 = r1
                r1 = r0
                r0 = r11
            L_0x0023:
                r11 = r0
                r0 = r1
                r1 = r11
                goto L_0x0006
            L_0x0027:
                org.jivesoftware.smackx.iqprivate.packet.DefaultPrivateData r0 = new org.jivesoftware.smackx.iqprivate.packet.DefaultPrivateData
                r0.<init>(r5, r3)
                r3 = r4
            L_0x002d:
                if (r3 != 0) goto L_0x0020
                int r6 = r13.next()
                if (r6 != r9) goto L_0x0054
                java.lang.String r6 = r13.getName()
                boolean r7 = r13.isEmptyElementTag()
                if (r7 == 0) goto L_0x0045
                java.lang.String r7 = ""
                r0.setValue(r6, r7)
                goto L_0x002d
            L_0x0045:
                int r7 = r13.next()
                r8 = 4
                if (r7 != r8) goto L_0x002d
                java.lang.String r7 = r13.getText()
                r0.setValue(r6, r7)
                goto L_0x002d
            L_0x0054:
                if (r6 != r10) goto L_0x002d
                java.lang.String r6 = r13.getName()
                boolean r6 = r6.equals(r5)
                if (r6 == 0) goto L_0x002d
                r3 = r2
                goto L_0x002d
            L_0x0062:
                if (r3 != r10) goto L_0x0079
                java.lang.String r3 = r13.getName()
                java.lang.String r5 = "query"
                boolean r3 = r3.equals(r5)
                if (r3 == 0) goto L_0x0079
                r1 = r0
                r0 = r2
                goto L_0x0023
            L_0x0073:
                org.jivesoftware.smackx.iqprivate.PrivateDataManager$PrivateDataResult r1 = new org.jivesoftware.smackx.iqprivate.PrivateDataManager$PrivateDataResult
                r1.<init>(r0)
                return r1
            L_0x0079:
                r11 = r1
                r1 = r0
                r0 = r11
                goto L_0x0023
            */
            throw new UnsupportedOperationException("Method not decompiled: org.jivesoftware.smackx.iqprivate.PrivateDataManager.PrivateDataIQProvider.parseIQ(org.xmlpull.v1.XmlPullParser):org.jivesoftware.smack.packet.IQ");
        }
    }

    private static class PrivateDataResult extends IQ {
        private PrivateData privateData;

        PrivateDataResult(PrivateData privateData2) {
            this.privateData = privateData2;
        }

        public PrivateData getPrivateData() {
            return this.privateData;
        }

        public String getChildElementXML() {
            StringBuilder sb = new StringBuilder();
            sb.append("<query xmlns=\"jabber:iq:private\">");
            if (this.privateData != null) {
                sb.append(this.privateData.toXML());
            }
            sb.append("</query>");
            return sb.toString();
        }
    }
}
