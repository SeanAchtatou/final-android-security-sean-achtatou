package org.jivesoftware.smackx.filetransfer;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smackx.filetransfer.FileTransfer;

public class IncomingFileTransfer extends FileTransfer {
    /* access modifiers changed from: private */
    public InputStream inputStream;
    /* access modifiers changed from: private */
    public FileTransferRequest recieveRequest;

    protected IncomingFileTransfer(FileTransferRequest fileTransferRequest, FileTransferNegotiator fileTransferNegotiator) {
        super(fileTransferRequest.getRequestor(), fileTransferRequest.getStreamID(), fileTransferNegotiator);
        this.recieveRequest = fileTransferRequest;
    }

    public InputStream recieveFile() throws SmackException, XMPPException.XMPPErrorException {
        if (this.inputStream != null) {
            throw new IllegalStateException("Transfer already negotiated!");
        }
        try {
            this.inputStream = negotiateStream();
            return this.inputStream;
        } catch (XMPPException.XMPPErrorException e) {
            setException(e);
            throw e;
        }
    }

    public void recieveFile(final File file) throws SmackException {
        if (file != null) {
            if (!file.exists()) {
                try {
                    file.createNewFile();
                } catch (IOException e) {
                    throw new SmackException("Could not create file to write too", e);
                }
            }
            if (!file.canWrite()) {
                throw new IllegalArgumentException("Cannot write to provided file");
            }
            new Thread(new Runnable() {
                /* JADX WARNING: Removed duplicated region for block: B:12:0x0042 A[SYNTHETIC, Splitter:B:12:0x0042] */
                /* JADX WARNING: Removed duplicated region for block: B:15:0x004d A[SYNTHETIC, Splitter:B:15:0x004d] */
                /* JADX WARNING: Removed duplicated region for block: B:30:? A[RETURN, SYNTHETIC] */
                /* JADX WARNING: Removed duplicated region for block: B:9:0x0033  */
                /* Code decompiled incorrectly, please refer to instructions dump. */
                public void run() {
                    /*
                        r5 = this;
                        org.jivesoftware.smackx.filetransfer.IncomingFileTransfer r0 = org.jivesoftware.smackx.filetransfer.IncomingFileTransfer.this     // Catch:{ Exception -> 0x0051 }
                        org.jivesoftware.smackx.filetransfer.IncomingFileTransfer r1 = org.jivesoftware.smackx.filetransfer.IncomingFileTransfer.this     // Catch:{ Exception -> 0x0051 }
                        java.io.InputStream r1 = r1.negotiateStream()     // Catch:{ Exception -> 0x0051 }
                        java.io.InputStream unused = r0.inputStream = r1     // Catch:{ Exception -> 0x0051 }
                        r1 = 0
                        java.io.FileOutputStream r0 = new java.io.FileOutputStream     // Catch:{ SmackException -> 0x005f, FileNotFoundException -> 0x0077 }
                        java.io.File r2 = r5     // Catch:{ SmackException -> 0x005f, FileNotFoundException -> 0x0077 }
                        r0.<init>(r2)     // Catch:{ SmackException -> 0x005f, FileNotFoundException -> 0x0077 }
                        org.jivesoftware.smackx.filetransfer.IncomingFileTransfer r1 = org.jivesoftware.smackx.filetransfer.IncomingFileTransfer.this     // Catch:{ SmackException -> 0x0095, FileNotFoundException -> 0x0093 }
                        org.jivesoftware.smackx.filetransfer.FileTransfer$Status r2 = org.jivesoftware.smackx.filetransfer.FileTransfer.Status.in_progress     // Catch:{ SmackException -> 0x0095, FileNotFoundException -> 0x0093 }
                        r1.setStatus(r2)     // Catch:{ SmackException -> 0x0095, FileNotFoundException -> 0x0093 }
                        org.jivesoftware.smackx.filetransfer.IncomingFileTransfer r1 = org.jivesoftware.smackx.filetransfer.IncomingFileTransfer.this     // Catch:{ SmackException -> 0x0095, FileNotFoundException -> 0x0093 }
                        org.jivesoftware.smackx.filetransfer.IncomingFileTransfer r2 = org.jivesoftware.smackx.filetransfer.IncomingFileTransfer.this     // Catch:{ SmackException -> 0x0095, FileNotFoundException -> 0x0093 }
                        java.io.InputStream r2 = r2.inputStream     // Catch:{ SmackException -> 0x0095, FileNotFoundException -> 0x0093 }
                        r1.writeToStream(r2, r0)     // Catch:{ SmackException -> 0x0095, FileNotFoundException -> 0x0093 }
                    L_0x0025:
                        org.jivesoftware.smackx.filetransfer.IncomingFileTransfer r1 = org.jivesoftware.smackx.filetransfer.IncomingFileTransfer.this
                        org.jivesoftware.smackx.filetransfer.FileTransfer$Status r1 = r1.getStatus()
                        org.jivesoftware.smackx.filetransfer.FileTransfer$Status r2 = org.jivesoftware.smackx.filetransfer.FileTransfer.Status.in_progress
                        boolean r1 = r1.equals(r2)
                        if (r1 == 0) goto L_0x003a
                        org.jivesoftware.smackx.filetransfer.IncomingFileTransfer r1 = org.jivesoftware.smackx.filetransfer.IncomingFileTransfer.this
                        org.jivesoftware.smackx.filetransfer.FileTransfer$Status r2 = org.jivesoftware.smackx.filetransfer.FileTransfer.Status.complete
                        r1.setStatus(r2)
                    L_0x003a:
                        org.jivesoftware.smackx.filetransfer.IncomingFileTransfer r1 = org.jivesoftware.smackx.filetransfer.IncomingFileTransfer.this
                        java.io.InputStream r1 = r1.inputStream
                        if (r1 == 0) goto L_0x004b
                        org.jivesoftware.smackx.filetransfer.IncomingFileTransfer r1 = org.jivesoftware.smackx.filetransfer.IncomingFileTransfer.this     // Catch:{ Throwable -> 0x0091 }
                        java.io.InputStream r1 = r1.inputStream     // Catch:{ Throwable -> 0x0091 }
                        r1.close()     // Catch:{ Throwable -> 0x0091 }
                    L_0x004b:
                        if (r0 == 0) goto L_0x0050
                        r0.close()     // Catch:{ Throwable -> 0x008f }
                    L_0x0050:
                        return
                    L_0x0051:
                        r0 = move-exception
                        org.jivesoftware.smackx.filetransfer.IncomingFileTransfer r1 = org.jivesoftware.smackx.filetransfer.IncomingFileTransfer.this
                        org.jivesoftware.smackx.filetransfer.FileTransfer$Status r2 = org.jivesoftware.smackx.filetransfer.FileTransfer.Status.error
                        r1.setStatus(r2)
                        org.jivesoftware.smackx.filetransfer.IncomingFileTransfer r1 = org.jivesoftware.smackx.filetransfer.IncomingFileTransfer.this
                        r1.setException(r0)
                        goto L_0x0050
                    L_0x005f:
                        r0 = move-exception
                        r4 = r0
                        r0 = r1
                        r1 = r4
                    L_0x0063:
                        org.jivesoftware.smackx.filetransfer.IncomingFileTransfer r2 = org.jivesoftware.smackx.filetransfer.IncomingFileTransfer.this
                        org.jivesoftware.smackx.filetransfer.FileTransfer$Status r3 = org.jivesoftware.smackx.filetransfer.FileTransfer.Status.error
                        r2.setStatus(r3)
                        org.jivesoftware.smackx.filetransfer.IncomingFileTransfer r2 = org.jivesoftware.smackx.filetransfer.IncomingFileTransfer.this
                        org.jivesoftware.smackx.filetransfer.FileTransfer$Error r3 = org.jivesoftware.smackx.filetransfer.FileTransfer.Error.stream
                        r2.setError(r3)
                        org.jivesoftware.smackx.filetransfer.IncomingFileTransfer r2 = org.jivesoftware.smackx.filetransfer.IncomingFileTransfer.this
                        r2.setException(r1)
                        goto L_0x0025
                    L_0x0077:
                        r0 = move-exception
                        r4 = r0
                        r0 = r1
                        r1 = r4
                    L_0x007b:
                        org.jivesoftware.smackx.filetransfer.IncomingFileTransfer r2 = org.jivesoftware.smackx.filetransfer.IncomingFileTransfer.this
                        org.jivesoftware.smackx.filetransfer.FileTransfer$Status r3 = org.jivesoftware.smackx.filetransfer.FileTransfer.Status.error
                        r2.setStatus(r3)
                        org.jivesoftware.smackx.filetransfer.IncomingFileTransfer r2 = org.jivesoftware.smackx.filetransfer.IncomingFileTransfer.this
                        org.jivesoftware.smackx.filetransfer.FileTransfer$Error r3 = org.jivesoftware.smackx.filetransfer.FileTransfer.Error.bad_file
                        r2.setError(r3)
                        org.jivesoftware.smackx.filetransfer.IncomingFileTransfer r2 = org.jivesoftware.smackx.filetransfer.IncomingFileTransfer.this
                        r2.setException(r1)
                        goto L_0x0025
                    L_0x008f:
                        r0 = move-exception
                        goto L_0x0050
                    L_0x0091:
                        r1 = move-exception
                        goto L_0x004b
                    L_0x0093:
                        r1 = move-exception
                        goto L_0x007b
                    L_0x0095:
                        r1 = move-exception
                        goto L_0x0063
                    */
                    throw new UnsupportedOperationException("Method not decompiled: org.jivesoftware.smackx.filetransfer.IncomingFileTransfer.AnonymousClass1.run():void");
                }
            }, "File Transfer " + this.streamID).start();
            return;
        }
        throw new IllegalArgumentException("File cannot be null");
    }

    /* access modifiers changed from: private */
    public InputStream negotiateStream() throws SmackException, XMPPException.XMPPErrorException {
        setStatus(FileTransfer.Status.negotiating_transfer);
        final StreamNegotiator selectStreamNegotiator = this.negotiator.selectStreamNegotiator(this.recieveRequest);
        setStatus(FileTransfer.Status.negotiating_stream);
        FutureTask futureTask = new FutureTask(new Callable<InputStream>() {
            public InputStream call() throws Exception {
                return selectStreamNegotiator.createIncomingStream(IncomingFileTransfer.this.recieveRequest.getStreamInitiation());
            }
        });
        futureTask.run();
        try {
            InputStream inputStream2 = (InputStream) futureTask.get(15, TimeUnit.SECONDS);
            futureTask.cancel(true);
            setStatus(FileTransfer.Status.negotiated);
            return inputStream2;
        } catch (InterruptedException e) {
            throw new SmackException("Interruption while executing", e);
        } catch (ExecutionException e2) {
            throw new SmackException("Error in execution", e2);
        } catch (TimeoutException e3) {
            throw new SmackException("Request timed out", e3);
        } catch (Throwable th) {
            futureTask.cancel(true);
            throw th;
        }
    }

    public void cancel() {
        setStatus(FileTransfer.Status.cancelled);
    }
}
