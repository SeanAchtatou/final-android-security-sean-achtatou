package org.jivesoftware.smackx.filetransfer;

import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;
import org.jivesoftware.smack.AbstractConnectionListener;
import org.jivesoftware.smack.PacketCollector;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.packet.XMPPError;
import org.jivesoftware.smackx.bytestreams.ibb.InBandBytestreamManager;
import org.jivesoftware.smackx.bytestreams.socks5.Socks5BytestreamManager;
import org.jivesoftware.smackx.disco.ServiceDiscoveryManager;
import org.jivesoftware.smackx.si.packet.StreamInitiation;
import org.jivesoftware.smackx.xdata.Form;
import org.jivesoftware.smackx.xdata.FormField;
import org.jivesoftware.smackx.xdata.packet.DataForm;

public class FileTransferNegotiator {
    public static boolean IBB_ONLY = false;
    private static final String[] NAMESPACE = {"http://jabber.org/protocol/si/profile/file-transfer", "http://jabber.org/protocol/si"};
    protected static final String STREAM_DATA_FIELD_NAME = "stream-method";
    private static final String STREAM_INIT_PREFIX = "jsi_";
    private static final Random randomGenerator = new Random();
    private static final Map<XMPPConnection, FileTransferNegotiator> transferObject = new ConcurrentHashMap();
    private final StreamNegotiator byteStreamTransferManager;
    private final XMPPConnection connection;
    private final StreamNegotiator inbandTransferManager;

    static {
        boolean z = true;
        if (System.getProperty("ibb") == null) {
            z = false;
        }
        IBB_ONLY = z;
    }

    public static FileTransferNegotiator getInstanceFor(XMPPConnection xMPPConnection) {
        if (xMPPConnection == null) {
            throw new IllegalArgumentException("XMPPConnection cannot be null");
        } else if (!xMPPConnection.isConnected()) {
            return null;
        } else {
            if (transferObject.containsKey(xMPPConnection)) {
                return transferObject.get(xMPPConnection);
            }
            FileTransferNegotiator fileTransferNegotiator = new FileTransferNegotiator(xMPPConnection);
            setServiceEnabled(xMPPConnection, true);
            transferObject.put(xMPPConnection, fileTransferNegotiator);
            return fileTransferNegotiator;
        }
    }

    public static void setServiceEnabled(XMPPConnection xMPPConnection, boolean z) {
        ServiceDiscoveryManager instanceFor = ServiceDiscoveryManager.getInstanceFor(xMPPConnection);
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.addAll(Arrays.asList(NAMESPACE));
        arrayList.add(InBandBytestreamManager.NAMESPACE);
        if (!IBB_ONLY) {
            arrayList.add(Socks5BytestreamManager.NAMESPACE);
        }
        for (String str : arrayList) {
            if (!z) {
                instanceFor.removeFeature(str);
            } else if (!instanceFor.includesFeature(str)) {
                instanceFor.addFeature(str);
            }
        }
    }

    public static boolean isServiceEnabled(XMPPConnection xMPPConnection) {
        ServiceDiscoveryManager instanceFor = ServiceDiscoveryManager.getInstanceFor(xMPPConnection);
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.addAll(Arrays.asList(NAMESPACE));
        arrayList.add(InBandBytestreamManager.NAMESPACE);
        if (!IBB_ONLY) {
            arrayList.add(Socks5BytestreamManager.NAMESPACE);
        }
        for (String includesFeature : arrayList) {
            if (!instanceFor.includesFeature(includesFeature)) {
                return false;
            }
        }
        return true;
    }

    public static IQ createIQ(String str, String str2, String str3, IQ.Type type) {
        AnonymousClass1 r0 = new IQ() {
            public String getChildElementXML() {
                return null;
            }
        };
        r0.setPacketID(str);
        r0.setTo(str2);
        r0.setFrom(str3);
        r0.setType(type);
        return r0;
    }

    public static Collection<String> getSupportedProtocols() {
        ArrayList arrayList = new ArrayList();
        arrayList.add(InBandBytestreamManager.NAMESPACE);
        if (!IBB_ONLY) {
            arrayList.add(Socks5BytestreamManager.NAMESPACE);
        }
        return Collections.unmodifiableList(arrayList);
    }

    private FileTransferNegotiator(XMPPConnection xMPPConnection) {
        configureConnection(xMPPConnection);
        this.connection = xMPPConnection;
        this.byteStreamTransferManager = new Socks5TransferNegotiator(xMPPConnection);
        this.inbandTransferManager = new IBBTransferNegotiator(xMPPConnection);
    }

    private void configureConnection(final XMPPConnection xMPPConnection) {
        xMPPConnection.addConnectionListener(new AbstractConnectionListener() {
            public void connectionClosed() {
                FileTransferNegotiator.this.cleanup(xMPPConnection);
            }

            public void connectionClosedOnError(Exception exc) {
                FileTransferNegotiator.this.cleanup(xMPPConnection);
            }
        });
    }

    /* access modifiers changed from: private */
    public void cleanup(XMPPConnection xMPPConnection) {
        if (transferObject.remove(xMPPConnection) != null) {
            this.inbandTransferManager.cleanup();
        }
    }

    public StreamNegotiator selectStreamNegotiator(FileTransferRequest fileTransferRequest) throws XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        StreamInitiation streamInitiation = fileTransferRequest.getStreamInitiation();
        FormField streamMethodField = getStreamMethodField(streamInitiation.getFeatureNegotiationForm());
        if (streamMethodField == null) {
            XMPPError xMPPError = new XMPPError(XMPPError.Condition.bad_request, "No stream methods contained in packet.");
            IQ createIQ = createIQ(streamInitiation.getPacketID(), streamInitiation.getFrom(), streamInitiation.getTo(), IQ.Type.ERROR);
            createIQ.setError(xMPPError);
            this.connection.sendPacket(createIQ);
            throw new XMPPException.XMPPErrorException("No stream methods contained in packet.", xMPPError);
        }
        try {
            return getNegotiator(streamMethodField);
        } catch (XMPPException.XMPPErrorException e) {
            IQ createIQ2 = createIQ(streamInitiation.getPacketID(), streamInitiation.getFrom(), streamInitiation.getTo(), IQ.Type.ERROR);
            createIQ2.setError(e.getXMPPError());
            this.connection.sendPacket(createIQ2);
            throw e;
        }
    }

    private FormField getStreamMethodField(DataForm dataForm) {
        for (FormField next : dataForm.getFields()) {
            if (next.getVariable().equals(STREAM_DATA_FIELD_NAME)) {
                return next;
            }
        }
        return null;
    }

    private StreamNegotiator getNegotiator(FormField formField) throws XMPPException.XMPPErrorException {
        boolean z;
        boolean z2;
        boolean z3 = false;
        boolean z4 = false;
        for (FormField.Option value : formField.getOptions()) {
            String value2 = value.getValue();
            if (value2.equals(Socks5BytestreamManager.NAMESPACE) && !IBB_ONLY) {
                z = z3;
                z2 = true;
            } else if (value2.equals(InBandBytestreamManager.NAMESPACE)) {
                z = true;
                z2 = z4;
            } else {
                z = z3;
                z2 = z4;
            }
            z4 = z2;
            z3 = z;
        }
        if (!z4 && !z3) {
            throw new XMPPException.XMPPErrorException(new XMPPError(XMPPError.Condition.bad_request, "No acceptable transfer mechanism"));
        } else if (z4 && z3) {
            return new FaultTolerantNegotiator(this.connection, this.byteStreamTransferManager, this.inbandTransferManager);
        } else {
            if (z4) {
                return this.byteStreamTransferManager;
            }
            return this.inbandTransferManager;
        }
    }

    public String getNextStreamID() {
        return STREAM_INIT_PREFIX + Math.abs(randomGenerator.nextLong());
    }

    public StreamNegotiator negotiateOutgoingTransfer(String str, String str2, String str3, long j, String str4, int i) throws XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        StreamInitiation streamInitiation = new StreamInitiation();
        streamInitiation.setSessionID(str2);
        streamInitiation.setMimeType(URLConnection.guessContentTypeFromName(str3));
        StreamInitiation.File file = new StreamInitiation.File(str3, j);
        file.setDesc(str4);
        streamInitiation.setFile(file);
        streamInitiation.setFeatureNegotiationForm(createDefaultInitiationForm());
        streamInitiation.setFrom(this.connection.getUser());
        streamInitiation.setTo(str);
        streamInitiation.setType(IQ.Type.SET);
        PacketCollector createPacketCollectorAndSend = this.connection.createPacketCollectorAndSend(streamInitiation);
        Packet nextResult = createPacketCollectorAndSend.nextResult((long) i);
        createPacketCollectorAndSend.cancel();
        if (!(nextResult instanceof IQ)) {
            return null;
        }
        IQ iq = (IQ) nextResult;
        if (iq.getType().equals(IQ.Type.RESULT)) {
            return getOutgoingNegotiator(getStreamMethodField(((StreamInitiation) nextResult).getFeatureNegotiationForm()));
        }
        throw new XMPPException.XMPPErrorException(iq.getError());
    }

    private StreamNegotiator getOutgoingNegotiator(FormField formField) throws XMPPException.XMPPErrorException {
        boolean z;
        boolean z2;
        boolean z3 = false;
        boolean z4 = false;
        for (String next : formField.getValues()) {
            if (next.equals(Socks5BytestreamManager.NAMESPACE) && !IBB_ONLY) {
                z = z3;
                z2 = true;
            } else if (next.equals(InBandBytestreamManager.NAMESPACE)) {
                z = true;
                z2 = z4;
            } else {
                z = z3;
                z2 = z4;
            }
            z4 = z2;
            z3 = z;
        }
        if (!z4 && !z3) {
            throw new XMPPException.XMPPErrorException(new XMPPError(XMPPError.Condition.bad_request, "No acceptable transfer mechanism"));
        } else if (z4 && z3) {
            return new FaultTolerantNegotiator(this.connection, this.byteStreamTransferManager, this.inbandTransferManager);
        } else {
            if (z4) {
                return this.byteStreamTransferManager;
            }
            return this.inbandTransferManager;
        }
    }

    private DataForm createDefaultInitiationForm() {
        DataForm dataForm = new DataForm(Form.TYPE_FORM);
        FormField formField = new FormField(STREAM_DATA_FIELD_NAME);
        formField.setType(FormField.TYPE_LIST_SINGLE);
        if (!IBB_ONLY) {
            formField.addOption(new FormField.Option(Socks5BytestreamManager.NAMESPACE));
        }
        formField.addOption(new FormField.Option(InBandBytestreamManager.NAMESPACE));
        dataForm.addField(formField);
        return dataForm;
    }
}
