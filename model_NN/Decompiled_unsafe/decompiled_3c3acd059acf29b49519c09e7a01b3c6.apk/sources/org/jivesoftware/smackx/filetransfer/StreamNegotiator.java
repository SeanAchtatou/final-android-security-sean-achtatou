package org.jivesoftware.smackx.filetransfer;

import java.io.InputStream;
import java.io.OutputStream;
import org.jivesoftware.smack.PacketCollector;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.filter.PacketFilter;
import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.packet.XMPPError;
import org.jivesoftware.smackx.si.packet.StreamInitiation;
import org.jivesoftware.smackx.xdata.Form;
import org.jivesoftware.smackx.xdata.FormField;
import org.jivesoftware.smackx.xdata.packet.DataForm;

public abstract class StreamNegotiator {
    public abstract void cleanup();

    public abstract InputStream createIncomingStream(StreamInitiation streamInitiation) throws XMPPException.XMPPErrorException, InterruptedException, SmackException.NoResponseException, SmackException;

    public abstract OutputStream createOutgoingStream(String str, String str2, String str3) throws XMPPException.XMPPErrorException, SmackException.NoResponseException, SmackException, XMPPException;

    public abstract PacketFilter getInitiationPacketFilter(String str, String str2);

    public abstract String[] getNamespaces();

    /* access modifiers changed from: package-private */
    public abstract InputStream negotiateIncomingStream(Packet packet) throws XMPPException.XMPPErrorException, InterruptedException, SmackException.NoResponseException, SmackException;

    public StreamInitiation createInitiationAccept(StreamInitiation streamInitiation, String[] strArr) {
        StreamInitiation streamInitiation2 = new StreamInitiation();
        streamInitiation2.setTo(streamInitiation.getFrom());
        streamInitiation2.setFrom(streamInitiation.getTo());
        streamInitiation2.setType(IQ.Type.RESULT);
        streamInitiation2.setPacketID(streamInitiation.getPacketID());
        DataForm dataForm = new DataForm(Form.TYPE_SUBMIT);
        FormField formField = new FormField("stream-method");
        for (String addValue : strArr) {
            formField.addValue(addValue);
        }
        dataForm.addField(formField);
        streamInitiation2.setFeatureNegotiationForm(dataForm);
        return streamInitiation2;
    }

    public IQ createError(String str, String str2, String str3, XMPPError xMPPError) {
        IQ createIQ = FileTransferNegotiator.createIQ(str3, str2, str, IQ.Type.ERROR);
        createIQ.setError(xMPPError);
        return createIQ;
    }

    /* access modifiers changed from: package-private */
    public Packet initiateIncomingStream(XMPPConnection xMPPConnection, StreamInitiation streamInitiation) throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        StreamInitiation createInitiationAccept = createInitiationAccept(streamInitiation, getNamespaces());
        PacketCollector createPacketCollector = xMPPConnection.createPacketCollector(getInitiationPacketFilter(streamInitiation.getFrom(), streamInitiation.getSessionID()));
        xMPPConnection.sendPacket(createInitiationAccept);
        return createPacketCollector.nextResultOrThrow();
    }
}
