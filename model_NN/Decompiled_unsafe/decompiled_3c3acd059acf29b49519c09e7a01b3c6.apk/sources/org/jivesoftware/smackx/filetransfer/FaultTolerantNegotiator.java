package org.jivesoftware.smackx.filetransfer;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import org.jivesoftware.smack.PacketCollector;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.filter.OrFilter;
import org.jivesoftware.smack.filter.PacketFilter;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smackx.si.packet.StreamInitiation;

public class FaultTolerantNegotiator extends StreamNegotiator {
    private XMPPConnection connection;
    private PacketFilter primaryFilter;
    private StreamNegotiator primaryNegotiator;
    private PacketFilter secondaryFilter;
    private StreamNegotiator secondaryNegotiator;

    public FaultTolerantNegotiator(XMPPConnection xMPPConnection, StreamNegotiator streamNegotiator, StreamNegotiator streamNegotiator2) {
        this.primaryNegotiator = streamNegotiator;
        this.secondaryNegotiator = streamNegotiator2;
        this.connection = xMPPConnection;
    }

    public PacketFilter getInitiationPacketFilter(String str, String str2) {
        if (this.primaryFilter == null || this.secondaryFilter == null) {
            this.primaryFilter = this.primaryNegotiator.getInitiationPacketFilter(str, str2);
            this.secondaryFilter = this.secondaryNegotiator.getInitiationPacketFilter(str, str2);
        }
        return new OrFilter(this.primaryFilter, this.secondaryFilter);
    }

    /* access modifiers changed from: package-private */
    public InputStream negotiateIncomingStream(Packet packet) {
        throw new UnsupportedOperationException("Negotiation only handled by create incoming stream method.");
    }

    /* access modifiers changed from: package-private */
    public final Packet initiateIncomingStream(XMPPConnection xMPPConnection, StreamInitiation streamInitiation) {
        throw new UnsupportedOperationException("Initiation handled by createIncomingStream method");
    }

    public InputStream createIncomingStream(StreamInitiation streamInitiation) throws SmackException {
        InputStream inputStream;
        SmackException smackException;
        InputStream inputStream2;
        SmackException smackException2 = null;
        PacketCollector createPacketCollector = this.connection.createPacketCollector(getInitiationPacketFilter(streamInitiation.getFrom(), streamInitiation.getSessionID()));
        this.connection.sendPacket(super.createInitiationAccept(streamInitiation, getNamespaces()));
        ExecutorService newFixedThreadPool = Executors.newFixedThreadPool(2);
        ExecutorCompletionService executorCompletionService = new ExecutorCompletionService(newFixedThreadPool);
        ArrayList<Future> arrayList = new ArrayList<>();
        try {
            arrayList.add(executorCompletionService.submit(new NegotiatorService(createPacketCollector)));
            arrayList.add(executorCompletionService.submit(new NegotiatorService(createPacketCollector)));
            int i = 0;
            inputStream = null;
            while (inputStream == null && i < arrayList.size()) {
                int i2 = i + 1;
                try {
                    Future poll = executorCompletionService.poll(10, TimeUnit.SECONDS);
                    if (poll == null) {
                        i = i2;
                    } else {
                        SmackException smackException3 = smackException2;
                        inputStream2 = (InputStream) poll.get();
                        smackException = smackException3;
                        inputStream = inputStream2;
                        smackException2 = smackException;
                        i = i2;
                    }
                } catch (InterruptedException e) {
                    i = i2;
                }
            }
            for (Future cancel : arrayList) {
                cancel.cancel(true);
            }
            createPacketCollector.cancel();
            newFixedThreadPool.shutdownNow();
            if (inputStream != null) {
                return inputStream;
            }
            if (smackException2 != null) {
                throw smackException2;
            }
            throw new SmackException("File transfer negotiation failed.");
        } catch (InterruptedException e2) {
            smackException = smackException2;
            inputStream2 = inputStream;
        } catch (ExecutionException e3) {
            smackException = new SmackException(e3.getCause());
            inputStream2 = inputStream;
        } catch (Throwable th) {
            Throwable th2 = th;
            for (Future cancel2 : arrayList) {
                cancel2.cancel(true);
            }
            createPacketCollector.cancel();
            newFixedThreadPool.shutdownNow();
            throw th2;
        }
    }

    /* access modifiers changed from: private */
    public StreamNegotiator determineNegotiator(Packet packet) {
        return this.primaryFilter.accept(packet) ? this.primaryNegotiator : this.secondaryNegotiator;
    }

    public OutputStream createOutgoingStream(String str, String str2, String str3) throws SmackException, XMPPException {
        try {
            return this.primaryNegotiator.createOutgoingStream(str, str2, str3);
        } catch (Exception e) {
            return this.secondaryNegotiator.createOutgoingStream(str, str2, str3);
        }
    }

    public String[] getNamespaces() {
        String[] namespaces = this.primaryNegotiator.getNamespaces();
        String[] namespaces2 = this.secondaryNegotiator.getNamespaces();
        String[] strArr = new String[(namespaces.length + namespaces2.length)];
        System.arraycopy(namespaces, 0, strArr, 0, namespaces.length);
        System.arraycopy(namespaces2, 0, strArr, namespaces.length, namespaces2.length);
        return strArr;
    }

    public void cleanup() {
    }

    private class NegotiatorService implements Callable<InputStream> {
        private PacketCollector collector;

        NegotiatorService(PacketCollector packetCollector) {
            this.collector = packetCollector;
        }

        public InputStream call() throws XMPPException.XMPPErrorException, InterruptedException, SmackException {
            Packet nextResult = this.collector.nextResult();
            if (nextResult != null) {
                return FaultTolerantNegotiator.this.determineNegotiator(nextResult).negotiateIncomingStream(nextResult);
            }
            throw new SmackException.NoResponseException();
        }
    }
}
