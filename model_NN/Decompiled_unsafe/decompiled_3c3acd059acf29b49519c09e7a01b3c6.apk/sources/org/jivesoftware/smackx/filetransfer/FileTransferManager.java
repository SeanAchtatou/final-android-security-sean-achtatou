package org.jivesoftware.smackx.filetransfer;

import java.util.ArrayList;
import java.util.List;
import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.filter.AndFilter;
import org.jivesoftware.smack.filter.IQTypeFilter;
import org.jivesoftware.smack.filter.PacketTypeFilter;
import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.packet.XMPPError;
import org.jivesoftware.smack.util.StringUtils;
import org.jivesoftware.smackx.si.packet.StreamInitiation;

public class FileTransferManager {
    private XMPPConnection connection;
    private final FileTransferNegotiator fileTransferNegotiator;
    private List<FileTransferListener> listeners;

    public FileTransferManager(XMPPConnection xMPPConnection) {
        this.connection = xMPPConnection;
        this.fileTransferNegotiator = FileTransferNegotiator.getInstanceFor(xMPPConnection);
    }

    public void addFileTransferListener(FileTransferListener fileTransferListener) {
        if (this.listeners == null) {
            initListeners();
        }
        synchronized (this.listeners) {
            this.listeners.add(fileTransferListener);
        }
    }

    private void initListeners() {
        this.listeners = new ArrayList();
        this.connection.addPacketListener(new PacketListener() {
            public void processPacket(Packet packet) {
                FileTransferManager.this.fireNewRequest((StreamInitiation) packet);
            }
        }, new AndFilter(new PacketTypeFilter(StreamInitiation.class), new IQTypeFilter(IQ.Type.SET)));
    }

    /* access modifiers changed from: protected */
    public void fireNewRequest(StreamInitiation streamInitiation) {
        FileTransferListener[] fileTransferListenerArr;
        synchronized (this.listeners) {
            fileTransferListenerArr = new FileTransferListener[this.listeners.size()];
            this.listeners.toArray(fileTransferListenerArr);
        }
        FileTransferRequest fileTransferRequest = new FileTransferRequest(this, streamInitiation);
        for (FileTransferListener fileTransferRequest2 : fileTransferListenerArr) {
            fileTransferRequest2.fileTransferRequest(fileTransferRequest);
        }
    }

    public void removeFileTransferListener(FileTransferListener fileTransferListener) {
        if (this.listeners != null) {
            synchronized (this.listeners) {
                this.listeners.remove(fileTransferListener);
            }
        }
    }

    public OutgoingFileTransfer createOutgoingFileTransfer(String str) {
        if (str == null) {
            throw new IllegalArgumentException("userID was null");
        } else if (StringUtils.isFullJID(str)) {
            return new OutgoingFileTransfer(this.connection.getUser(), str, this.fileTransferNegotiator.getNextStreamID(), this.fileTransferNegotiator);
        } else {
            throw new IllegalArgumentException("The provided user id was not a full JID (i.e. with resource part)");
        }
    }

    /* access modifiers changed from: protected */
    public IncomingFileTransfer createIncomingFileTransfer(FileTransferRequest fileTransferRequest) {
        if (fileTransferRequest == null) {
            throw new NullPointerException("RecieveRequest cannot be null");
        }
        IncomingFileTransfer incomingFileTransfer = new IncomingFileTransfer(fileTransferRequest, this.fileTransferNegotiator);
        incomingFileTransfer.setFileInfo(fileTransferRequest.getFileName(), fileTransferRequest.getFileSize());
        return incomingFileTransfer;
    }

    /* access modifiers changed from: protected */
    public void rejectIncomingFileTransfer(FileTransferRequest fileTransferRequest) throws SmackException.NotConnectedException {
        StreamInitiation streamInitiation = fileTransferRequest.getStreamInitiation();
        IQ createIQ = FileTransferNegotiator.createIQ(streamInitiation.getPacketID(), streamInitiation.getFrom(), streamInitiation.getTo(), IQ.Type.ERROR);
        createIQ.setError(new XMPPError(XMPPError.Condition.forbidden));
        this.connection.sendPacket(createIQ);
    }
}
