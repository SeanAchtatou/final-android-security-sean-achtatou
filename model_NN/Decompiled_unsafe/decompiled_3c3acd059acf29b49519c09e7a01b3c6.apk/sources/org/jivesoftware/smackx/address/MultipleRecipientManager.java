package org.jivesoftware.smackx.address;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.util.Cache;
import org.jivesoftware.smack.util.StringUtils;
import org.jivesoftware.smackx.address.packet.MultipleAddresses;
import org.jivesoftware.smackx.disco.ServiceDiscoveryManager;
import org.jivesoftware.smackx.disco.packet.DiscoverInfo;
import org.jivesoftware.smackx.disco.packet.DiscoverItems;

public class MultipleRecipientManager {
    private static final Logger LOGGER = Logger.getLogger(MultipleRecipientManager.class.getName());
    private static Cache<String, String> services = new Cache<>(100, 86400000);

    public static void send(XMPPConnection xMPPConnection, Packet packet, List<String> list, List<String> list2, List<String> list3) throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.FeatureNotSupportedException, SmackException.NotConnectedException {
        send(xMPPConnection, packet, list, list2, list3, null, null, false);
    }

    public static void send(XMPPConnection xMPPConnection, Packet packet, List<String> list, List<String> list2, List<String> list3, String str, String str2, boolean z) throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.FeatureNotSupportedException, SmackException.NotConnectedException {
        String multipleRecipienServiceAddress = getMultipleRecipienServiceAddress(xMPPConnection);
        if (multipleRecipienServiceAddress != null) {
            sendThroughService(xMPPConnection, packet, list, list2, list3, str, str2, z, multipleRecipienServiceAddress);
        } else if (z || ((str != null && str.trim().length() > 0) || (str2 != null && str2.trim().length() > 0))) {
            throw new SmackException.FeatureNotSupportedException("Extended Stanza Addressing");
        } else {
            sendToIndividualRecipients(xMPPConnection, packet, list, list2, list3);
        }
    }

    public static void reply(XMPPConnection xMPPConnection, Message message, Message message2) throws SmackException, XMPPException.XMPPErrorException {
        MultipleRecipientInfo multipleRecipientInfo = getMultipleRecipientInfo(message);
        if (multipleRecipientInfo == null) {
            throw new SmackException("Original message does not contain multiple recipient info");
        } else if (multipleRecipientInfo.shouldNotReply()) {
            throw new SmackException("Original message should not be replied");
        } else if (multipleRecipientInfo.getReplyRoom() != null) {
            throw new SmackException("Reply should be sent through a room");
        } else {
            if (message.getThread() != null) {
                message2.setThread(message.getThread());
            }
            MultipleAddresses.Address replyAddress = multipleRecipientInfo.getReplyAddress();
            if (replyAddress == null || replyAddress.getJid() == null) {
                ArrayList arrayList = new ArrayList();
                ArrayList arrayList2 = new ArrayList();
                for (MultipleAddresses.Address jid : multipleRecipientInfo.getTOAddresses()) {
                    arrayList.add(jid.getJid());
                }
                for (MultipleAddresses.Address jid2 : multipleRecipientInfo.getCCAddresses()) {
                    arrayList2.add(jid2.getJid());
                }
                if (!arrayList.contains(message.getFrom()) && !arrayList2.contains(message.getFrom())) {
                    arrayList.add(message.getFrom());
                }
                String user = xMPPConnection.getUser();
                if (!arrayList.remove(user) && !arrayList2.remove(user)) {
                    String parseBareAddress = StringUtils.parseBareAddress(user);
                    arrayList.remove(parseBareAddress);
                    arrayList2.remove(parseBareAddress);
                }
                String multipleRecipienServiceAddress = getMultipleRecipienServiceAddress(xMPPConnection);
                if (multipleRecipienServiceAddress != null) {
                    sendThroughService(xMPPConnection, message2, arrayList, arrayList2, null, null, null, false, multipleRecipienServiceAddress);
                } else {
                    sendToIndividualRecipients(xMPPConnection, message2, arrayList, arrayList2, null);
                }
            } else {
                message2.setTo(replyAddress.getJid());
                xMPPConnection.sendPacket(message2);
            }
        }
    }

    public static MultipleRecipientInfo getMultipleRecipientInfo(Packet packet) {
        MultipleAddresses multipleAddresses = (MultipleAddresses) packet.getExtension(MultipleAddresses.ELEMENT, MultipleAddresses.NAMESPACE);
        if (multipleAddresses == null) {
            return null;
        }
        return new MultipleRecipientInfo(multipleAddresses);
    }

    private static void sendToIndividualRecipients(XMPPConnection xMPPConnection, Packet packet, List<String> list, List<String> list2, List<String> list3) throws SmackException.NotConnectedException {
        if (list != null) {
            for (String to : list) {
                packet.setTo(to);
                xMPPConnection.sendPacket(new PacketCopy(packet.toXML()));
            }
        }
        if (list2 != null) {
            for (String to2 : list2) {
                packet.setTo(to2);
                xMPPConnection.sendPacket(new PacketCopy(packet.toXML()));
            }
        }
        if (list3 != null) {
            for (String to3 : list3) {
                packet.setTo(to3);
                xMPPConnection.sendPacket(new PacketCopy(packet.toXML()));
            }
        }
    }

    private static void sendThroughService(XMPPConnection xMPPConnection, Packet packet, List<String> list, List<String> list2, List<String> list3, String str, String str2, boolean z, String str3) throws SmackException.NotConnectedException {
        MultipleAddresses multipleAddresses = new MultipleAddresses();
        if (list != null) {
            for (String addAddress : list) {
                multipleAddresses.addAddress("to", addAddress, null, null, false, null);
            }
        }
        if (list2 != null) {
            for (String addAddress2 : list2) {
                multipleAddresses.addAddress(MultipleAddresses.CC, addAddress2, null, null, false, null);
            }
        }
        if (list3 != null) {
            for (String addAddress3 : list3) {
                multipleAddresses.addAddress(MultipleAddresses.BCC, addAddress3, null, null, false, null);
            }
        }
        if (z) {
            multipleAddresses.setNoReply();
        } else {
            if (str != null && str.trim().length() > 0) {
                multipleAddresses.addAddress(MultipleAddresses.REPLY_TO, str, null, null, false, null);
            }
            if (str2 != null && str2.trim().length() > 0) {
                multipleAddresses.addAddress(MultipleAddresses.REPLY_ROOM, str2, null, null, false, null);
            }
        }
        packet.setTo(str3);
        packet.addExtension(multipleAddresses);
        xMPPConnection.sendPacket(packet);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
     arg types: [java.util.logging.Level, java.lang.String, org.jivesoftware.smack.XMPPException$XMPPErrorException]
     candidates:
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
    private static String getMultipleRecipienServiceAddress(XMPPConnection xMPPConnection) throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        DiscoverInfo discoverInfo;
        DiscoverItems.Item item;
        String serviceName = xMPPConnection.getServiceName();
        String str = services.get(serviceName);
        if (str == null) {
            ServiceDiscoveryManager instanceFor = ServiceDiscoveryManager.getInstanceFor(xMPPConnection);
            try {
                discoverInfo = instanceFor.discoverInfo(serviceName);
            } catch (XMPPException.XMPPErrorException e) {
                LOGGER.log(Level.WARNING, "Exception while discovering info of service", (Throwable) e);
                discoverInfo = null;
            }
            if (discoverInfo == null || !discoverInfo.containsFeature(MultipleAddresses.NAMESPACE)) {
                try {
                    Iterator<DiscoverItems.Item> it = instanceFor.discoverItems(serviceName).getItems().iterator();
                    while (true) {
                        if (!it.hasNext()) {
                            break;
                        }
                        DiscoverItems.Item next = it.next();
                        try {
                            if (instanceFor.discoverInfo(next.getEntityID(), next.getNode()).containsFeature(MultipleAddresses.NAMESPACE)) {
                                str = serviceName;
                                break;
                            }
                        } catch (SmackException.NoResponseException | XMPPException.XMPPErrorException e2) {
                            LOGGER.log(Level.WARNING, "Exception while discovering info of " + item.getEntityID() + " node: " + item.getNode(), e2);
                        }
                    }
                } catch (XMPPException.XMPPErrorException e3) {
                    LOGGER.log(Level.WARNING, "Exception while disvering items of service", (Throwable) e3);
                }
            } else {
                str = serviceName;
            }
            if (str == null) {
                str = "";
            }
            services.put(serviceName, str);
        }
        if ("".equals(str)) {
            return null;
        }
        return str;
    }

    private static class PacketCopy extends Packet {
        private CharSequence text;

        public PacketCopy(CharSequence charSequence) {
            this.text = charSequence;
        }

        public CharSequence toXML() {
            return this.text;
        }
    }
}
