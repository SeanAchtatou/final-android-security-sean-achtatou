package org.jivesoftware.smackx.address.provider;

import org.jivesoftware.smack.packet.PacketExtension;
import org.jivesoftware.smack.provider.PacketExtensionProvider;
import org.jivesoftware.smackx.address.packet.MultipleAddresses;
import org.jivesoftware.smackx.xevent.packet.MessageEvent;
import org.xmlpull.v1.XmlPullParser;

public class MultipleAddressesProvider implements PacketExtensionProvider {
    public PacketExtension parseExtension(XmlPullParser xmlPullParser) throws Exception {
        boolean z;
        boolean z2 = false;
        MultipleAddresses multipleAddresses = new MultipleAddresses();
        while (!z2) {
            int next = xmlPullParser.next();
            if (next == 2) {
                if (xmlPullParser.getName().equals("address")) {
                    multipleAddresses.addAddress(xmlPullParser.getAttributeValue("", "type"), xmlPullParser.getAttributeValue("", "jid"), xmlPullParser.getAttributeValue("", "node"), xmlPullParser.getAttributeValue("", "desc"), "true".equals(xmlPullParser.getAttributeValue("", MessageEvent.DELIVERED)), xmlPullParser.getAttributeValue("", "uri"));
                    z = z2;
                }
                z = z2;
            } else {
                if (next == 3 && xmlPullParser.getName().equals(multipleAddresses.getElementName())) {
                    z = true;
                }
                z = z2;
            }
            z2 = z;
        }
        return multipleAddresses;
    }
}
