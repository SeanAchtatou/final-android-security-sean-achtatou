package org.jivesoftware.smackx.bytestreams.ibb;

import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smackx.bytestreams.BytestreamRequest;
import org.jivesoftware.smackx.bytestreams.ibb.packet.Open;

public class InBandBytestreamRequest implements BytestreamRequest {
    private final Open byteStreamRequest;
    private final InBandBytestreamManager manager;

    protected InBandBytestreamRequest(InBandBytestreamManager inBandBytestreamManager, Open open) {
        this.manager = inBandBytestreamManager;
        this.byteStreamRequest = open;
    }

    public String getFrom() {
        return this.byteStreamRequest.getFrom();
    }

    public String getSessionID() {
        return this.byteStreamRequest.getSessionID();
    }

    public InBandBytestreamSession accept() throws SmackException.NotConnectedException {
        XMPPConnection connection = this.manager.getConnection();
        InBandBytestreamSession inBandBytestreamSession = new InBandBytestreamSession(connection, this.byteStreamRequest, this.byteStreamRequest.getFrom());
        this.manager.getSessions().put(this.byteStreamRequest.getSessionID(), inBandBytestreamSession);
        connection.sendPacket(IQ.createResultIQ(this.byteStreamRequest));
        return inBandBytestreamSession;
    }

    public void reject() throws SmackException.NotConnectedException {
        this.manager.replyRejectPacket(this.byteStreamRequest);
    }
}
