package org.jivesoftware.smackx.bytestreams.socks5;

import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.packet.XMPPError;
import org.jivesoftware.smack.util.Cache;
import org.jivesoftware.smackx.bytestreams.BytestreamRequest;
import org.jivesoftware.smackx.bytestreams.socks5.packet.Bytestream;

public class Socks5BytestreamRequest implements BytestreamRequest {
    private static final Cache<String, Integer> ADDRESS_BLACKLIST = new Cache<>(BLACKLIST_MAX_SIZE, BLACKLIST_LIFETIME);
    private static final long BLACKLIST_LIFETIME = 7200000;
    private static final int BLACKLIST_MAX_SIZE = 100;
    private static int CONNECTION_FAILURE_THRESHOLD = 2;
    private Bytestream bytestreamRequest;
    private Socks5BytestreamManager manager;
    private int minimumConnectTimeout = 2000;
    private int totalConnectTimeout = 10000;

    public static int getConnectFailureThreshold() {
        return CONNECTION_FAILURE_THRESHOLD;
    }

    public static void setConnectFailureThreshold(int i) {
        CONNECTION_FAILURE_THRESHOLD = i;
    }

    protected Socks5BytestreamRequest(Socks5BytestreamManager socks5BytestreamManager, Bytestream bytestream) {
        this.manager = socks5BytestreamManager;
        this.bytestreamRequest = bytestream;
    }

    public int getTotalConnectTimeout() {
        if (this.totalConnectTimeout <= 0) {
            return 10000;
        }
        return this.totalConnectTimeout;
    }

    public void setTotalConnectTimeout(int i) {
        this.totalConnectTimeout = i;
    }

    public int getMinimumConnectTimeout() {
        if (this.minimumConnectTimeout <= 0) {
            return 2000;
        }
        return this.minimumConnectTimeout;
    }

    public void setMinimumConnectTimeout(int i) {
        this.minimumConnectTimeout = i;
    }

    public String getFrom() {
        return this.bytestreamRequest.getFrom();
    }

    public String getSessionID() {
        return this.bytestreamRequest.getSessionID();
    }

    /* JADX WARN: Type inference failed for: r0v3 */
    /* JADX WARN: Type inference failed for: r0v4, types: [java.net.Socket] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public org.jivesoftware.smackx.bytestreams.socks5.Socks5BytestreamSession accept() throws java.lang.InterruptedException, org.jivesoftware.smack.XMPPException.XMPPErrorException, org.jivesoftware.smack.SmackException {
        /*
            r9 = this;
            r1 = 0
            org.jivesoftware.smackx.bytestreams.socks5.packet.Bytestream r0 = r9.bytestreamRequest
            java.util.Collection r0 = r0.getStreamHosts()
            int r2 = r0.size()
            if (r2 != 0) goto L_0x0010
            r9.cancelRequest()
        L_0x0010:
            org.jivesoftware.smackx.bytestreams.socks5.packet.Bytestream r2 = r9.bytestreamRequest
            java.lang.String r2 = r2.getSessionID()
            org.jivesoftware.smackx.bytestreams.socks5.packet.Bytestream r3 = r9.bytestreamRequest
            java.lang.String r3 = r3.getFrom()
            org.jivesoftware.smackx.bytestreams.socks5.Socks5BytestreamManager r4 = r9.manager
            org.jivesoftware.smack.XMPPConnection r4 = r4.getConnection()
            java.lang.String r4 = r4.getUser()
            java.lang.String r2 = org.jivesoftware.smackx.bytestreams.socks5.Socks5Utils.createDigest(r2, r3, r4)
            int r3 = r9.getTotalConnectTimeout()
            int r4 = r0.size()
            int r3 = r3 / r4
            int r4 = r9.getMinimumConnectTimeout()
            int r3 = java.lang.Math.max(r3, r4)
            java.util.Iterator r4 = r0.iterator()
        L_0x003f:
            boolean r0 = r4.hasNext()
            if (r0 == 0) goto L_0x00b9
            java.lang.Object r0 = r4.next()
            org.jivesoftware.smackx.bytestreams.socks5.packet.Bytestream$StreamHost r0 = (org.jivesoftware.smackx.bytestreams.socks5.packet.Bytestream.StreamHost) r0
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r6 = r0.getAddress()
            java.lang.StringBuilder r5 = r5.append(r6)
            java.lang.String r6 = ":"
            java.lang.StringBuilder r5 = r5.append(r6)
            int r6 = r0.getPort()
            java.lang.StringBuilder r5 = r5.append(r6)
            java.lang.String r5 = r5.toString()
            int r6 = r9.getConnectionFailures(r5)
            int r7 = org.jivesoftware.smackx.bytestreams.socks5.Socks5BytestreamRequest.CONNECTION_FAILURE_THRESHOLD
            if (r7 <= 0) goto L_0x0076
            int r7 = org.jivesoftware.smackx.bytestreams.socks5.Socks5BytestreamRequest.CONNECTION_FAILURE_THRESHOLD
            if (r6 >= r7) goto L_0x003f
        L_0x0076:
            org.jivesoftware.smackx.bytestreams.socks5.Socks5Client r6 = new org.jivesoftware.smackx.bytestreams.socks5.Socks5Client     // Catch:{ TimeoutException -> 0x00aa, IOException -> 0x00af, XMPPException -> 0x00b4 }
            r6.<init>(r0, r2)     // Catch:{ TimeoutException -> 0x00aa, IOException -> 0x00af, XMPPException -> 0x00b4 }
            java.net.Socket r1 = r6.getSocket(r3)     // Catch:{ TimeoutException -> 0x00aa, IOException -> 0x00af, XMPPException -> 0x00b4 }
            r8 = r1
            r1 = r0
            r0 = r8
        L_0x0082:
            if (r1 == 0) goto L_0x0086
            if (r0 != 0) goto L_0x0089
        L_0x0086:
            r9.cancelRequest()
        L_0x0089:
            org.jivesoftware.smackx.bytestreams.socks5.packet.Bytestream r2 = r9.createUsedHostResponse(r1)
            org.jivesoftware.smackx.bytestreams.socks5.Socks5BytestreamManager r3 = r9.manager
            org.jivesoftware.smack.XMPPConnection r3 = r3.getConnection()
            r3.sendPacket(r2)
            org.jivesoftware.smackx.bytestreams.socks5.Socks5BytestreamSession r2 = new org.jivesoftware.smackx.bytestreams.socks5.Socks5BytestreamSession
            java.lang.String r1 = r1.getJID()
            org.jivesoftware.smackx.bytestreams.socks5.packet.Bytestream r3 = r9.bytestreamRequest
            java.lang.String r3 = r3.getFrom()
            boolean r1 = r1.equals(r3)
            r2.<init>(r0, r1)
            return r2
        L_0x00aa:
            r0 = move-exception
            r9.incrementConnectionFailures(r5)
            goto L_0x003f
        L_0x00af:
            r0 = move-exception
            r9.incrementConnectionFailures(r5)
            goto L_0x003f
        L_0x00b4:
            r0 = move-exception
            r9.incrementConnectionFailures(r5)
            goto L_0x003f
        L_0x00b9:
            r0 = r1
            goto L_0x0082
        */
        throw new UnsupportedOperationException("Method not decompiled: org.jivesoftware.smackx.bytestreams.socks5.Socks5BytestreamRequest.accept():org.jivesoftware.smackx.bytestreams.socks5.Socks5BytestreamSession");
    }

    public void reject() throws SmackException.NotConnectedException {
        this.manager.replyRejectPacket(this.bytestreamRequest);
    }

    private void cancelRequest() throws XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        XMPPError xMPPError = new XMPPError(XMPPError.Condition.item_not_found, "Could not establish socket with any provided host");
        this.manager.getConnection().sendPacket(IQ.createErrorResponse(this.bytestreamRequest, xMPPError));
        throw new XMPPException.XMPPErrorException("Could not establish socket with any provided host", xMPPError);
    }

    private Bytestream createUsedHostResponse(Bytestream.StreamHost streamHost) {
        Bytestream bytestream = new Bytestream(this.bytestreamRequest.getSessionID());
        bytestream.setTo(this.bytestreamRequest.getFrom());
        bytestream.setType(IQ.Type.RESULT);
        bytestream.setPacketID(this.bytestreamRequest.getPacketID());
        bytestream.setUsedHost(streamHost.getJID());
        return bytestream;
    }

    private void incrementConnectionFailures(String str) {
        Integer num = ADDRESS_BLACKLIST.get(str);
        ADDRESS_BLACKLIST.put(str, Integer.valueOf(num == null ? 1 : num.intValue() + 1));
    }

    private int getConnectionFailures(String str) {
        Integer num = ADDRESS_BLACKLIST.get(str);
        if (num != null) {
            return num.intValue();
        }
        return 0;
    }
}
