package org.jivesoftware.smackx.bytestreams.socks5;

import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeoutException;
import org.jivesoftware.smack.AbstractConnectionListener;
import org.jivesoftware.smack.ConnectionCreationListener;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.packet.XMPPError;
import org.jivesoftware.smackx.bytestreams.BytestreamListener;
import org.jivesoftware.smackx.bytestreams.BytestreamManager;
import org.jivesoftware.smackx.bytestreams.socks5.packet.Bytestream;
import org.jivesoftware.smackx.disco.ServiceDiscoveryManager;
import org.jivesoftware.smackx.disco.packet.DiscoverInfo;
import org.jivesoftware.smackx.disco.packet.DiscoverItems;

public final class Socks5BytestreamManager implements BytestreamManager {
    public static final String NAMESPACE = "http://jabber.org/protocol/bytestreams";
    private static final String SESSION_ID_PREFIX = "js5_";
    private static final Map<XMPPConnection, Socks5BytestreamManager> managers = new HashMap();
    private static final Random randomGenerator = new Random();
    private final List<BytestreamListener> allRequestListeners = Collections.synchronizedList(new LinkedList());
    private final XMPPConnection connection;
    private List<String> ignoredBytestreamRequests = Collections.synchronizedList(new LinkedList());
    private final InitiationListener initiationListener;
    private String lastWorkingProxy = null;
    private final List<String> proxyBlacklist = Collections.synchronizedList(new LinkedList());
    private int proxyConnectionTimeout = 10000;
    private boolean proxyPrioritizationEnabled = true;
    private int targetResponseTimeout = 10000;
    private final Map<String, BytestreamListener> userListeners = new ConcurrentHashMap();

    static {
        XMPPConnection.addConnectionCreationListener(new ConnectionCreationListener() {
            public void connectionCreated(final XMPPConnection xMPPConnection) {
                Socks5BytestreamManager.getBytestreamManager(xMPPConnection);
                xMPPConnection.addConnectionListener(new AbstractConnectionListener() {
                    public void connectionClosed() {
                        Socks5BytestreamManager.getBytestreamManager(xMPPConnection).disableService();
                    }

                    public void connectionClosedOnError(Exception exc) {
                        Socks5BytestreamManager.getBytestreamManager(xMPPConnection).disableService();
                    }

                    public void reconnectionSuccessful() {
                        Socks5BytestreamManager.getBytestreamManager(xMPPConnection);
                    }
                });
            }
        });
    }

    public static synchronized Socks5BytestreamManager getBytestreamManager(XMPPConnection xMPPConnection) {
        Socks5BytestreamManager socks5BytestreamManager;
        synchronized (Socks5BytestreamManager.class) {
            if (xMPPConnection == null) {
                socks5BytestreamManager = null;
            } else {
                socks5BytestreamManager = managers.get(xMPPConnection);
                if (socks5BytestreamManager == null) {
                    socks5BytestreamManager = new Socks5BytestreamManager(xMPPConnection);
                    managers.put(xMPPConnection, socks5BytestreamManager);
                    socks5BytestreamManager.activate();
                }
            }
        }
        return socks5BytestreamManager;
    }

    private Socks5BytestreamManager(XMPPConnection xMPPConnection) {
        this.connection = xMPPConnection;
        this.initiationListener = new InitiationListener(this);
    }

    public void addIncomingBytestreamListener(BytestreamListener bytestreamListener) {
        this.allRequestListeners.add(bytestreamListener);
    }

    public void removeIncomingBytestreamListener(BytestreamListener bytestreamListener) {
        this.allRequestListeners.remove(bytestreamListener);
    }

    public void addIncomingBytestreamListener(BytestreamListener bytestreamListener, String str) {
        this.userListeners.put(str, bytestreamListener);
    }

    public void removeIncomingBytestreamListener(String str) {
        this.userListeners.remove(str);
    }

    public void ignoreBytestreamRequestOnce(String str) {
        this.ignoredBytestreamRequests.add(str);
    }

    public synchronized void disableService() {
        this.connection.removePacketListener(this.initiationListener);
        this.initiationListener.shutdown();
        this.allRequestListeners.clear();
        this.userListeners.clear();
        this.lastWorkingProxy = null;
        this.proxyBlacklist.clear();
        this.ignoredBytestreamRequests.clear();
        managers.remove(this.connection);
        if (managers.size() == 0) {
            Socks5Proxy.getSocks5Proxy().stop();
        }
        ServiceDiscoveryManager instanceFor = ServiceDiscoveryManager.getInstanceFor(this.connection);
        if (instanceFor != null) {
            instanceFor.removeFeature(NAMESPACE);
        }
    }

    public int getTargetResponseTimeout() {
        if (this.targetResponseTimeout <= 0) {
            this.targetResponseTimeout = 10000;
        }
        return this.targetResponseTimeout;
    }

    public void setTargetResponseTimeout(int i) {
        this.targetResponseTimeout = i;
    }

    public int getProxyConnectionTimeout() {
        if (this.proxyConnectionTimeout <= 0) {
            this.proxyConnectionTimeout = 10000;
        }
        return this.proxyConnectionTimeout;
    }

    public void setProxyConnectionTimeout(int i) {
        this.proxyConnectionTimeout = i;
    }

    public boolean isProxyPrioritizationEnabled() {
        return this.proxyPrioritizationEnabled;
    }

    public void setProxyPrioritizationEnabled(boolean z) {
        this.proxyPrioritizationEnabled = z;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.jivesoftware.smackx.bytestreams.socks5.Socks5BytestreamManager.establishSession(java.lang.String, java.lang.String):org.jivesoftware.smackx.bytestreams.socks5.Socks5BytestreamSession
     arg types: [java.lang.String, java.lang.String]
     candidates:
      org.jivesoftware.smackx.bytestreams.socks5.Socks5BytestreamManager.establishSession(java.lang.String, java.lang.String):org.jivesoftware.smackx.bytestreams.BytestreamSession
      org.jivesoftware.smackx.bytestreams.BytestreamManager.establishSession(java.lang.String, java.lang.String):org.jivesoftware.smackx.bytestreams.BytestreamSession
      org.jivesoftware.smackx.bytestreams.socks5.Socks5BytestreamManager.establishSession(java.lang.String, java.lang.String):org.jivesoftware.smackx.bytestreams.socks5.Socks5BytestreamSession */
    public Socks5BytestreamSession establishSession(String str) throws XMPPException, IOException, InterruptedException, SmackException {
        return establishSession(str, getNextSessionID());
    }

    public Socks5BytestreamSession establishSession(String str, String str2) throws IOException, InterruptedException, SmackException.NoResponseException, SmackException, XMPPException {
        Bytestream.StreamHost streamHost;
        if (!supportsSocks5(str)) {
            throw new SmackException.FeatureNotSupportedException("SOCKS5 Bytestream", str);
        }
        ArrayList arrayList = new ArrayList();
        try {
            arrayList.addAll(determineProxies());
            e = null;
        } catch (XMPPException.XMPPErrorException e) {
            e = e;
        }
        List<Bytestream.StreamHost> determineStreamHostInfos = determineStreamHostInfos(arrayList);
        if (!determineStreamHostInfos.isEmpty()) {
            String createDigest = Socks5Utils.createDigest(str2, this.connection.getUser(), str);
            if (this.proxyPrioritizationEnabled && this.lastWorkingProxy != null) {
                Iterator<Bytestream.StreamHost> it = determineStreamHostInfos.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        streamHost = null;
                        break;
                    }
                    streamHost = it.next();
                    if (streamHost.getJID().equals(this.lastWorkingProxy)) {
                        break;
                    }
                }
                if (streamHost != null) {
                    determineStreamHostInfos.remove(streamHost);
                    determineStreamHostInfos.add(0, streamHost);
                }
            }
            Socks5Proxy socks5Proxy = Socks5Proxy.getSocks5Proxy();
            try {
                socks5Proxy.addTransfer(createDigest);
                Bytestream createBytestreamInitiation = createBytestreamInitiation(str2, str, determineStreamHostInfos);
                Bytestream.StreamHost streamHost2 = createBytestreamInitiation.getStreamHost(((Bytestream) this.connection.createPacketCollectorAndSend(createBytestreamInitiation).nextResultOrThrow((long) getTargetResponseTimeout())).getUsedHost().getJID());
                if (streamHost2 == null) {
                    throw new SmackException("Remote user responded with unknown host");
                }
                Socket socket = new Socks5ClientForInitiator(streamHost2, createDigest, this.connection, str2, str).getSocket(getProxyConnectionTimeout());
                this.lastWorkingProxy = streamHost2.getJID();
                Socks5BytestreamSession socks5BytestreamSession = new Socks5BytestreamSession(socket, streamHost2.getJID().equals(this.connection.getUser()));
                socks5Proxy.removeTransfer(createDigest);
                return socks5BytestreamSession;
            } catch (TimeoutException e2) {
                throw new IOException("Timeout while connecting to SOCKS5 proxy");
            } catch (Throwable th) {
                socks5Proxy.removeTransfer(createDigest);
                throw th;
            }
        } else if (e != null) {
            throw e;
        } else {
            throw new SmackException("no SOCKS5 proxies available");
        }
    }

    private boolean supportsSocks5(String str) throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        return ServiceDiscoveryManager.getInstanceFor(this.connection).supportsFeature(str, NAMESPACE);
    }

    private List<String> determineProxies() throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        ServiceDiscoveryManager instanceFor = ServiceDiscoveryManager.getInstanceFor(this.connection);
        ArrayList arrayList = new ArrayList();
        for (DiscoverItems.Item next : instanceFor.discoverItems(this.connection.getServiceName()).getItems()) {
            if (!this.proxyBlacklist.contains(next.getEntityID())) {
                try {
                    Iterator<DiscoverInfo.Identity> it = instanceFor.discoverInfo(next.getEntityID()).getIdentities().iterator();
                    while (true) {
                        if (!it.hasNext()) {
                            break;
                        }
                        DiscoverInfo.Identity next2 = it.next();
                        if ("proxy".equalsIgnoreCase(next2.getCategory()) && "bytestreams".equalsIgnoreCase(next2.getType())) {
                            arrayList.add(next.getEntityID());
                            break;
                        }
                        this.proxyBlacklist.add(next.getEntityID());
                    }
                } catch (SmackException.NoResponseException | XMPPException.XMPPErrorException e) {
                    this.proxyBlacklist.add(next.getEntityID());
                }
            }
        }
        return arrayList;
    }

    private List<Bytestream.StreamHost> determineStreamHostInfos(List<String> list) {
        ArrayList arrayList = new ArrayList();
        List<Bytestream.StreamHost> localStreamHost = getLocalStreamHost();
        if (localStreamHost != null) {
            arrayList.addAll(localStreamHost);
        }
        for (String next : list) {
            try {
                arrayList.addAll(((Bytestream) this.connection.createPacketCollectorAndSend(createStreamHostRequest(next)).nextResultOrThrow()).getStreamHosts());
            } catch (Exception e) {
                this.proxyBlacklist.add(next);
            }
        }
        return arrayList;
    }

    private Bytestream createStreamHostRequest(String str) {
        Bytestream bytestream = new Bytestream();
        bytestream.setType(IQ.Type.GET);
        bytestream.setTo(str);
        return bytestream;
    }

    private List<Bytestream.StreamHost> getLocalStreamHost() {
        Socks5Proxy socks5Proxy = Socks5Proxy.getSocks5Proxy();
        if (socks5Proxy.isRunning()) {
            List<String> localAddresses = socks5Proxy.getLocalAddresses();
            int port = socks5Proxy.getPort();
            if (localAddresses.size() >= 1) {
                ArrayList arrayList = new ArrayList();
                for (String streamHost : localAddresses) {
                    Bytestream.StreamHost streamHost2 = new Bytestream.StreamHost(this.connection.getUser(), streamHost);
                    streamHost2.setPort(port);
                    arrayList.add(streamHost2);
                }
                return arrayList;
            }
        }
        return null;
    }

    private Bytestream createBytestreamInitiation(String str, String str2, List<Bytestream.StreamHost> list) {
        Bytestream bytestream = new Bytestream(str);
        for (Bytestream.StreamHost addStreamHost : list) {
            bytestream.addStreamHost(addStreamHost);
        }
        bytestream.setType(IQ.Type.SET);
        bytestream.setTo(str2);
        return bytestream;
    }

    /* access modifiers changed from: protected */
    public void replyRejectPacket(IQ iq) throws SmackException.NotConnectedException {
        this.connection.sendPacket(IQ.createErrorResponse(iq, new XMPPError(XMPPError.Condition.not_acceptable)));
    }

    private void activate() {
        this.connection.addPacketListener(this.initiationListener, this.initiationListener.getFilter());
        enableService();
    }

    private void enableService() {
        ServiceDiscoveryManager instanceFor = ServiceDiscoveryManager.getInstanceFor(this.connection);
        if (!instanceFor.includesFeature(NAMESPACE)) {
            instanceFor.addFeature(NAMESPACE);
        }
    }

    private String getNextSessionID() {
        return SESSION_ID_PREFIX + Math.abs(randomGenerator.nextLong());
    }

    /* access modifiers changed from: protected */
    public XMPPConnection getConnection() {
        return this.connection;
    }

    /* access modifiers changed from: protected */
    public BytestreamListener getUserListener(String str) {
        return this.userListeners.get(str);
    }

    /* access modifiers changed from: protected */
    public List<BytestreamListener> getAllRequestListeners() {
        return this.allRequestListeners;
    }

    /* access modifiers changed from: protected */
    public List<String> getIgnoredBytestreamRequests() {
        return this.ignoredBytestreamRequests;
    }
}
