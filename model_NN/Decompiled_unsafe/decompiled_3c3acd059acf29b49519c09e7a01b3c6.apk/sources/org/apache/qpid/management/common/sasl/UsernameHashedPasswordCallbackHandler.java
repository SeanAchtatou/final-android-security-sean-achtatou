package org.apache.qpid.management.common.sasl;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import org.apache.harmony.javax.security.auth.callback.Callback;
import org.apache.harmony.javax.security.auth.callback.CallbackHandler;
import org.apache.harmony.javax.security.auth.callback.NameCallback;
import org.apache.harmony.javax.security.auth.callback.PasswordCallback;
import org.apache.harmony.javax.security.auth.callback.UnsupportedCallbackException;

public class UsernameHashedPasswordCallbackHandler implements CallbackHandler {
    private char[] pwchars;
    private String user;

    public UsernameHashedPasswordCallbackHandler(String str, String str2) throws Exception {
        this.user = str;
        this.pwchars = getHash(str2);
    }

    public void handle(Callback[] callbackArr) throws IOException, UnsupportedCallbackException {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < callbackArr.length) {
                if (callbackArr[i2] instanceof NameCallback) {
                    ((NameCallback) callbackArr[i2]).setName(this.user);
                } else if (callbackArr[i2] instanceof PasswordCallback) {
                    ((PasswordCallback) callbackArr[i2]).setPassword(this.pwchars);
                } else {
                    throw new UnsupportedCallbackException(callbackArr[i2]);
                }
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    private void clearPassword() {
        if (this.pwchars != null) {
            for (int i = 0; i < this.pwchars.length; i++) {
                this.pwchars[i] = 0;
            }
            this.pwchars = null;
        }
    }

    /* access modifiers changed from: protected */
    public void finalize() throws Throwable {
        clearPassword();
        super.finalize();
    }

    public static char[] getHash(String str) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        int i = 0;
        byte[] bytes = str.getBytes("utf-8");
        MessageDigest instance = MessageDigest.getInstance("MD5");
        for (byte update : bytes) {
            instance.update(update);
        }
        byte[] digest = instance.digest();
        char[] cArr = new char[digest.length];
        int length = digest.length;
        int i2 = 0;
        while (i < length) {
            cArr[i2] = (char) digest[i];
            i++;
            i2++;
        }
        return cArr;
    }
}
