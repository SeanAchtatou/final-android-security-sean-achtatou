package org.apache.qpid.management.common.sasl;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import org.apache.harmony.javax.security.auth.callback.Callback;
import org.apache.harmony.javax.security.auth.callback.CallbackHandler;
import org.apache.harmony.javax.security.auth.callback.NameCallback;
import org.apache.harmony.javax.security.auth.callback.PasswordCallback;
import org.apache.harmony.javax.security.auth.callback.UnsupportedCallbackException;
import org.apache.harmony.javax.security.sasl.SaslClient;
import org.apache.harmony.javax.security.sasl.SaslException;

public class PlainSaslClient implements SaslClient {
    private static byte SEPARATOR = 0;
    private String authenticationID;
    private String authorizationID;
    private CallbackHandler cbh;
    private boolean completed = false;
    private byte[] password;

    public PlainSaslClient(String str, CallbackHandler callbackHandler) throws SaslException {
        this.cbh = callbackHandler;
        Object[] userInfo = getUserInfo();
        this.authorizationID = str;
        this.authenticationID = (String) userInfo[0];
        this.password = (byte[]) userInfo[1];
        if (this.authenticationID == null || this.password == null) {
            throw new SaslException("PLAIN: authenticationID and password must be specified");
        }
    }

    public byte[] evaluateChallenge(byte[] bArr) throws SaslException {
        byte[] bytes;
        int i;
        int i2 = 0;
        if (this.completed) {
            throw new IllegalStateException("PLAIN: authentication already completed");
        }
        this.completed = true;
        try {
            if (this.authorizationID == null) {
                bytes = null;
            } else {
                bytes = this.authorizationID.getBytes("UTF8");
            }
            byte[] bytes2 = this.authenticationID.getBytes("UTF8");
            int length = this.password.length + bytes2.length + 2;
            if (bytes != null) {
                i = bytes.length;
            } else {
                i = 0;
            }
            byte[] bArr2 = new byte[(i + length)];
            if (bytes != null) {
                System.arraycopy(bytes, 0, bArr2, 0, bytes.length);
                i2 = bytes.length;
            }
            int i3 = i2 + 1;
            bArr2[i2] = SEPARATOR;
            System.arraycopy(bytes2, 0, bArr2, i3, bytes2.length);
            int length2 = bytes2.length + i3;
            bArr2[length2] = SEPARATOR;
            System.arraycopy(this.password, 0, bArr2, length2 + 1, this.password.length);
            clearPassword();
            return bArr2;
        } catch (UnsupportedEncodingException e) {
            throw new SaslException("PLAIN: Cannot get UTF-8 encoding of ids", e);
        }
    }

    public String getMechanismName() {
        return Constants.MECH_PLAIN;
    }

    public boolean hasInitialResponse() {
        return true;
    }

    public boolean isComplete() {
        return this.completed;
    }

    public byte[] unwrap(byte[] bArr, int i, int i2) throws SaslException {
        if (this.completed) {
            throw new IllegalStateException("PLAIN: this mechanism supports neither integrity nor privacy");
        }
        throw new IllegalStateException("PLAIN: authentication not completed");
    }

    public byte[] wrap(byte[] bArr, int i, int i2) throws SaslException {
        if (this.completed) {
            throw new IllegalStateException("PLAIN: this mechanism supports neither integrity nor privacy");
        }
        throw new IllegalStateException("PLAIN: authentication not completed");
    }

    public Object getNegotiatedProperty(String str) {
        if (!this.completed) {
            throw new IllegalStateException("PLAIN: authentication not completed");
        } else if (str.equals("javax.security.sasl.qop")) {
            return "auth";
        } else {
            return null;
        }
    }

    private void clearPassword() {
        if (this.password != null) {
            for (int i = 0; i < this.password.length; i++) {
                this.password[i] = 0;
            }
            this.password = null;
        }
    }

    public void dispose() throws SaslException {
        clearPassword();
    }

    /* access modifiers changed from: protected */
    public void finalize() throws Throwable {
        clearPassword();
        super.finalize();
    }

    private Object[] getUserInfo() throws SaslException {
        byte[] bArr;
        try {
            NameCallback nameCallback = new NameCallback("PLAIN authentication id: ");
            PasswordCallback passwordCallback = new PasswordCallback("PLAIN password: ", false);
            this.cbh.handle(new Callback[]{nameCallback, passwordCallback});
            String name = nameCallback.getName();
            char[] password2 = passwordCallback.getPassword();
            if (password2 != null) {
                bArr = new String(password2).getBytes("UTF8");
                passwordCallback.clearPassword();
            } else {
                bArr = null;
            }
            return new Object[]{name, bArr};
        } catch (IOException e) {
            throw new SaslException("Cannot get password", e);
        } catch (UnsupportedCallbackException e2) {
            throw new SaslException("Cannot get userid/password", e2);
        }
    }
}
