package org.apache.harmony.javax.security.auth;

import java.security.DomainCombiner;
import java.security.Principal;
import java.security.ProtectionDomain;
import java.util.Set;

public class SubjectDomainCombiner implements DomainCombiner {
    private static final AuthPermission _GET = new AuthPermission("getSubjectFromDomainCombiner");
    private Subject subject;

    public SubjectDomainCombiner(Subject subject2) {
        if (subject2 == null) {
            throw new NullPointerException();
        }
        this.subject = subject2;
    }

    public Subject getSubject() {
        SecurityManager securityManager = System.getSecurityManager();
        if (securityManager != null) {
            securityManager.checkPermission(_GET);
        }
        return this.subject;
    }

    public ProtectionDomain[] combine(ProtectionDomain[] protectionDomainArr, ProtectionDomain[] protectionDomainArr2) {
        int i;
        int i2;
        if (protectionDomainArr != null) {
            i = protectionDomainArr.length + 0;
        } else {
            i = 0;
        }
        if (protectionDomainArr2 != null) {
            i += protectionDomainArr2.length;
        }
        if (i == 0) {
            return null;
        }
        ProtectionDomain[] protectionDomainArr3 = new ProtectionDomain[i];
        if (protectionDomainArr != null) {
            Set<Principal> principals = this.subject.getPrincipals();
            Principal[] principalArr = (Principal[]) principals.toArray(new Principal[principals.size()]);
            i2 = 0;
            while (i2 < protectionDomainArr.length) {
                if (protectionDomainArr[i2] != null) {
                    protectionDomainArr3[i2] = new ProtectionDomain(protectionDomainArr[i2].getCodeSource(), protectionDomainArr[i2].getPermissions(), protectionDomainArr[i2].getClassLoader(), principalArr);
                }
                i2++;
            }
        } else {
            i2 = 0;
        }
        if (protectionDomainArr2 != null) {
            System.arraycopy(protectionDomainArr2, 0, protectionDomainArr3, i2, protectionDomainArr2.length);
        }
        return protectionDomainArr3;
    }
}
