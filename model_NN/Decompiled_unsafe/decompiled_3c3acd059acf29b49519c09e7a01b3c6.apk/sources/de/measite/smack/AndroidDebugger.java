package de.measite.smack;

import android.util.Log;
import java.io.Reader;
import java.io.Writer;
import org.jivesoftware.smack.AbstractConnectionListener;
import org.jivesoftware.smack.ConnectionListener;
import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.debugger.SmackDebugger;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.util.ObservableReader;
import org.jivesoftware.smack.util.ObservableWriter;
import org.jivesoftware.smack.util.ReaderListener;
import org.jivesoftware.smack.util.WriterListener;

public class AndroidDebugger implements SmackDebugger {
    public static boolean printInterpreted = false;
    private ConnectionListener connListener = null;
    /* access modifiers changed from: private */
    public XMPPConnection connection = null;
    private PacketListener listener = null;
    private Reader reader;
    private ReaderListener readerListener;
    private Writer writer;
    private WriterListener writerListener;

    public AndroidDebugger(XMPPConnection xMPPConnection, Writer writer2, Reader reader2) {
        this.connection = xMPPConnection;
        this.writer = writer2;
        this.reader = reader2;
        createDebug();
    }

    private void createDebug() {
        ObservableReader observableReader = new ObservableReader(this.reader);
        this.readerListener = new ReaderListener() {
            public void read(String str) {
                Log.d("SMACK", "RCV (" + AndroidDebugger.this.connection.getConnectionCounter() + "): " + str);
            }
        };
        observableReader.addReaderListener(this.readerListener);
        ObservableWriter observableWriter = new ObservableWriter(this.writer);
        this.writerListener = new WriterListener() {
            public void write(String str) {
                Log.d("SMACK", "SENT (" + AndroidDebugger.this.connection.getConnectionCounter() + "): " + str);
            }
        };
        observableWriter.addWriterListener(this.writerListener);
        this.reader = observableReader;
        this.writer = observableWriter;
        this.listener = new PacketListener() {
            public void processPacket(Packet packet) {
                if (AndroidDebugger.printInterpreted) {
                    Log.d("SMACK", "RCV PKT (" + AndroidDebugger.this.connection.getConnectionCounter() + "): " + ((Object) packet.toXML()));
                }
            }
        };
        this.connListener = new AbstractConnectionListener() {
            public void connectionClosed() {
                Log.d("SMACK", "Connection closed (" + AndroidDebugger.this.connection.getConnectionCounter() + ")");
            }

            public void connectionClosedOnError(Exception exc) {
                Log.d("SMACK", "Connection closed due to an exception (" + AndroidDebugger.this.connection.getConnectionCounter() + ")");
            }

            public void reconnectionFailed(Exception exc) {
                Log.d("SMACK", "Reconnection failed due to an exception (" + AndroidDebugger.this.connection.getConnectionCounter() + ")");
            }

            public void reconnectionSuccessful() {
                Log.d("SMACK", "Connection reconnected (" + AndroidDebugger.this.connection.getConnectionCounter() + ")");
            }

            public void reconnectingIn(int i) {
                Log.d("SMACK", "Connection (" + AndroidDebugger.this.connection.getConnectionCounter() + ") will reconnect in " + i);
            }
        };
    }

    public Reader newConnectionReader(Reader reader2) {
        ((ObservableReader) this.reader).removeReaderListener(this.readerListener);
        ObservableReader observableReader = new ObservableReader(reader2);
        observableReader.addReaderListener(this.readerListener);
        this.reader = observableReader;
        return this.reader;
    }

    public Writer newConnectionWriter(Writer writer2) {
        ((ObservableWriter) this.writer).removeWriterListener(this.writerListener);
        ObservableWriter observableWriter = new ObservableWriter(writer2);
        observableWriter.addWriterListener(this.writerListener);
        this.writer = observableWriter;
        return this.writer;
    }

    public void userHasLogged(String str) {
        Log.d("SMACK", "User logged (" + this.connection.getConnectionCounter() + "): " + str + "@" + this.connection.getServiceName() + ":" + this.connection.getPort());
        this.connection.addConnectionListener(this.connListener);
    }

    public Reader getReader() {
        return this.reader;
    }

    public Writer getWriter() {
        return this.writer;
    }

    public PacketListener getReaderListener() {
        return this.listener;
    }

    public PacketListener getWriterListener() {
        return null;
    }
}
