package com.admob.android.ads;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

public class AdManager {
    private static final long LOCATION_UPDATE_INTERVAL = 900000;
    static final String LOG = "AdMob SDK";
    static final String SDK_SITE_ID = "a1496ced2842262";
    public static final String SDK_VERSION = "20091123-ANDROID-3312276cc1406347";
    private static final String SDK_VERSION_CHECKSUM = "3312276cc1406347";
    static final String SDK_VERSION_DATE = "20091123";
    private static GregorianCalendar birthday;
    /* access modifiers changed from: private */
    public static Location coordinates;
    /* access modifiers changed from: private */
    public static long coordinatesTimestamp;
    private static Gender gender;
    private static String publisherId;
    private static boolean testMode;
    private static String userAgent;
    private static String userId;

    public enum Gender {
        MALE,
        FEMALE
    }

    static {
        Log.i(LOG, "AdMob SDK version is 20091123-ANDROID-3312276cc1406347");
    }

    protected static void clientError(String message) {
        Log.e(LOG, message);
        throw new IllegalArgumentException(message);
    }

    public static String getPublisherId(Context context) {
        if (publisherId == null) {
            try {
                ApplicationInfo info = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128);
                if (info != null) {
                    String id = info.metaData.getString("ADMOB_PUBLISHER_ID");
                    Log.d(LOG, "Publisher ID read from AndroidManifest.xml is " + id);
                    if (!id.equals(SDK_SITE_ID) || (!context.getPackageName().equals("com.admob.android.test") && !context.getPackageName().equals("com.example.admob.lunarlander"))) {
                        setPublisherId(id);
                    } else {
                        Log.i(LOG, "This is a sample application so allowing sample publisher ID.");
                        publisherId = id;
                    }
                }
            } catch (Exception e) {
                Log.e(LOG, "Could not read ADMOB_PUBLISHER_ID meta-data from AndroidManifest.xml.", e);
            }
        }
        return publisherId;
    }

    public static void setPublisherId(String id) {
        if (id == null || id.length() != 15) {
            clientError("SETUP ERROR:  Incorrect AdMob publisher ID.  Should 15 [a-f,0-9] characters:  " + publisherId);
        }
        if (id.equalsIgnoreCase(SDK_SITE_ID)) {
            clientError("SETUP ERROR:  Cannot use the sample publisher ID (a1496ced2842262).  Yours is available on www.admob.com.");
        }
        Log.i(LOG, "Publisher ID set to " + id);
        publisherId = id;
    }

    public static boolean isInTestMode() {
        return testMode;
    }

    public static void setInTestMode(boolean testing) {
        testMode = testing;
    }

    public static String getUserId(Context context) {
        if (userId == null) {
            userId = Settings.System.getString(context.getContentResolver(), "android_id");
            userId = md5(userId);
            Log.i(LOG, "The user ID is " + userId);
        }
        return userId;
    }

    private static String md5(String val) {
        if (val == null || val.length() <= 0) {
            return null;
        }
        try {
            MessageDigest md5 = MessageDigest.getInstance("MD5");
            md5.update(val.getBytes(), 0, val.length());
            return String.format("%032X", new BigInteger(1, md5.digest()));
        } catch (Exception e) {
            Log.d(LOG, "Could not generate hash of " + val, e);
            userId = userId.substring(0, 32);
            return null;
        }
    }

    public static Location getCoordinates(Context context) {
        if (context != null && (coordinates == null || System.currentTimeMillis() > coordinatesTimestamp + LOCATION_UPDATE_INTERVAL)) {
            synchronized (context) {
                if (coordinates == null || System.currentTimeMillis() > coordinatesTimestamp + LOCATION_UPDATE_INTERVAL) {
                    coordinatesTimestamp = System.currentTimeMillis();
                    boolean permissions = false;
                    LocationManager manager = null;
                    String provider = null;
                    if (context.checkCallingOrSelfPermission("android.permission.ACCESS_COARSE_LOCATION") == 0) {
                        if (Log.isLoggable(LOG, 3)) {
                            Log.d(LOG, "Trying to get locations from the network.");
                        }
                        permissions = true;
                        manager = (LocationManager) context.getSystemService("location");
                        if (manager != null) {
                            Criteria criteria = new Criteria();
                            criteria.setAccuracy(2);
                            criteria.setCostAllowed(false);
                            provider = manager.getBestProvider(criteria, true);
                        }
                    }
                    if (provider == null && context.checkCallingOrSelfPermission("android.permission.ACCESS_FINE_LOCATION") == 0) {
                        if (Log.isLoggable(LOG, 3)) {
                            Log.d(LOG, "Trying to get locations from GPS.");
                        }
                        permissions = true;
                        manager = (LocationManager) context.getSystemService("location");
                        if (manager != null) {
                            Criteria criteria2 = new Criteria();
                            criteria2.setAccuracy(1);
                            criteria2.setCostAllowed(false);
                            provider = manager.getBestProvider(criteria2, true);
                        }
                    }
                    if (!permissions) {
                        if (Log.isLoggable(LOG, 3)) {
                            Log.d(LOG, "Cannot access user's location.  Permissions are not set.");
                        }
                    } else if (provider != null) {
                        Log.i(LOG, "Location provider setup successfully.");
                        final LocationManager mgr = manager;
                        manager.requestLocationUpdates(provider, 0, 0.0f, new LocationListener() {
                            public void onLocationChanged(Location location) {
                                Location unused = AdManager.coordinates = location;
                                long unused2 = AdManager.coordinatesTimestamp = System.currentTimeMillis();
                                mgr.removeUpdates(this);
                                Log.i(AdManager.LOG, "Aquired location " + AdManager.coordinates.getLatitude() + "," + AdManager.coordinates.getLongitude() + " at " + new Date(AdManager.coordinatesTimestamp).toString() + ".");
                            }

                            public void onProviderDisabled(String provider) {
                            }

                            public void onProviderEnabled(String provider) {
                            }

                            public void onStatusChanged(String provider, int status, Bundle extras) {
                            }
                        }, context.getMainLooper());
                    } else if (Log.isLoggable(LOG, 3)) {
                        Log.d(LOG, "No location providers are available.  Ads will not be geotargeted.");
                    }
                }
            }
        }
        return coordinates;
    }

    static String getCoordinatesAsString(Context context) {
        String result = null;
        Location l = getCoordinates(context);
        if (l != null) {
            result = l.getLatitude() + "," + l.getLongitude();
        }
        if (Log.isLoggable(LOG, 3)) {
            Log.d(LOG, "User coordinates are " + result);
        }
        return result;
    }

    public static GregorianCalendar getBirthday() {
        return birthday;
    }

    static String getBirthdayAsString() {
        GregorianCalendar dob = getBirthday();
        if (dob == null) {
            return null;
        }
        return String.format("%04d%02d%02d", Integer.valueOf(dob.get(1)), Integer.valueOf(dob.get(2) + 1), Integer.valueOf(dob.get(5)));
    }

    public static void setBirthday(GregorianCalendar birthday2) {
        birthday = birthday2;
    }

    public static void setBirthday(int year, int month, int day) {
        GregorianCalendar cal = new GregorianCalendar();
        cal.set(year, month - 1, day);
        setBirthday(cal);
    }

    public static Gender getGender() {
        return gender;
    }

    static String getGenderAsString() {
        if (gender == Gender.MALE) {
            return "m";
        }
        if (gender == Gender.FEMALE) {
            return "f";
        }
        return null;
    }

    public static void setGender(Gender gender2) {
        gender = gender2;
    }

    static String getUserAgent() {
        if (userAgent == null) {
            StringBuffer arg = new StringBuffer();
            String version = Build.VERSION.RELEASE;
            if (version.length() > 0) {
                arg.append(version);
            } else {
                arg.append("1.0");
            }
            arg.append("; ");
            Locale l = Locale.getDefault();
            String language = l.getLanguage();
            if (language != null) {
                arg.append(language.toLowerCase());
                String country = l.getCountry();
                if (country != null) {
                    arg.append("-");
                    arg.append(country.toLowerCase());
                }
            } else {
                arg.append("en");
            }
            String model = Build.MODEL;
            if (model.length() > 0) {
                arg.append("; ");
                arg.append(model);
            }
            String id = Build.ID;
            if (id.length() > 0) {
                arg.append(" Build/");
                arg.append(id);
            }
            userAgent = String.format("Mozilla/5.0 (Linux; U; Android %s) AppleWebKit/525.10+ (KHTML, like Gecko) Version/3.0.4 Mobile Safari/523.12.2 (AdMob-ANDROID-%s)", arg, SDK_VERSION_DATE);
            if (Log.isLoggable(LOG, 3)) {
                Log.d(LOG, "Phone's user-agent is:  " + userAgent);
            }
        }
        return userAgent;
    }
}
