package com.admob.android.ads;

import android.content.Context;
import android.util.Log;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

class AdRequester {
    private static final String ADMOB_ENDPOINT = "http://r.admob.com/ad_source.php";
    private static int REQUEST_TIMEOUT = 3000;

    AdRequester() {
    }

    /* JADX INFO: Multiple debug info for r12v2 java.lang.String: [D('ex' java.lang.Exception), D('html' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r12v35 java.lang.String: [D('iconURL' java.lang.String), D('line' java.lang.String)] */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x00b4 A[SYNTHETIC, Splitter:B:21:0x00b4] */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x00b9 A[Catch:{ Exception -> 0x013d }] */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x00ce  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00db  */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x0157  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.admob.android.ads.Ad requestAd(android.content.Context r11, java.lang.String r12, java.lang.String r13) {
        /*
            java.lang.String r0 = "android.permission.INTERNET"
            int r0 = r11.checkCallingOrSelfPermission(r0)
            r1 = -1
            if (r0 != r1) goto L_0x000e
            java.lang.String r0 = "Cannot request an ad without Internet permissions!  Open manifest.xml and just before the final </manifest> tag add:  <uses-permission android:name=\"android.permission.INTERNET\" />"
            com.admob.android.ads.AdManager.clientError(r0)
        L_0x000e:
            com.admob.android.ads.AdMobLocalizer.init(r11)
            r0 = 0
            long r5 = java.lang.System.currentTimeMillis()
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            r2 = 0
            java.lang.String r3 = buildParamString(r11, r12, r13)     // Catch:{ Exception -> 0x013a }
            r7 = 0
            r4 = 0
            java.net.URL r12 = new java.net.URL     // Catch:{ all -> 0x0140 }
            java.lang.String r13 = "http://r.admob.com/ad_source.php"
            r12.<init>(r13)     // Catch:{ all -> 0x0140 }
            java.net.URLConnection r13 = r12.openConnection()     // Catch:{ all -> 0x0140 }
            java.net.HttpURLConnection r13 = (java.net.HttpURLConnection) r13     // Catch:{ all -> 0x0140 }
            java.lang.String r12 = "POST"
            r13.setRequestMethod(r12)     // Catch:{ all -> 0x0140 }
            r12 = 1
            r13.setDoOutput(r12)     // Catch:{ all -> 0x0140 }
            java.lang.String r12 = "User-Agent"
            java.lang.String r8 = com.admob.android.ads.AdManager.getUserAgent()     // Catch:{ all -> 0x0140 }
            r13.setRequestProperty(r12, r8)     // Catch:{ all -> 0x0140 }
            java.lang.String r12 = "Content-Type"
            java.lang.String r8 = "application/x-www-form-urlencoded"
            r13.setRequestProperty(r12, r8)     // Catch:{ all -> 0x0140 }
            java.lang.String r12 = "Content-Length"
            int r8 = r3.length()     // Catch:{ all -> 0x0140 }
            java.lang.String r8 = java.lang.Integer.toString(r8)     // Catch:{ all -> 0x0140 }
            r13.setRequestProperty(r12, r8)     // Catch:{ all -> 0x0140 }
            int r12 = com.admob.android.ads.AdRequester.REQUEST_TIMEOUT     // Catch:{ all -> 0x0140 }
            r13.setConnectTimeout(r12)     // Catch:{ all -> 0x0140 }
            int r12 = com.admob.android.ads.AdRequester.REQUEST_TIMEOUT     // Catch:{ all -> 0x0140 }
            r13.setReadTimeout(r12)     // Catch:{ all -> 0x0140 }
            r12 = r3
            java.lang.String r3 = "AdMob SDK"
            r8 = 3
            boolean r3 = android.util.Log.isLoggable(r3, r8)     // Catch:{ all -> 0x0140 }
            if (r3 == 0) goto L_0x0081
            java.lang.String r3 = "AdMob SDK"
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ all -> 0x0140 }
            r8.<init>()     // Catch:{ all -> 0x0140 }
            java.lang.String r9 = "Requesting an ad with POST parmams:  "
            java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ all -> 0x0140 }
            java.lang.StringBuilder r8 = r8.append(r12)     // Catch:{ all -> 0x0140 }
            java.lang.String r8 = r8.toString()     // Catch:{ all -> 0x0140 }
            android.util.Log.d(r3, r8)     // Catch:{ all -> 0x0140 }
        L_0x0081:
            java.io.OutputStream r3 = r13.getOutputStream()     // Catch:{ all -> 0x0140 }
            java.io.BufferedWriter r8 = new java.io.BufferedWriter     // Catch:{ all -> 0x0140 }
            java.io.OutputStreamWriter r9 = new java.io.OutputStreamWriter     // Catch:{ all -> 0x0140 }
            r9.<init>(r3)     // Catch:{ all -> 0x0140 }
            r8.<init>(r9)     // Catch:{ all -> 0x0140 }
            r8.write(r12)     // Catch:{ all -> 0x0147 }
            r8.close()     // Catch:{ all -> 0x0147 }
            java.io.BufferedReader r3 = new java.io.BufferedReader     // Catch:{ all -> 0x0147 }
            java.io.InputStreamReader r12 = new java.io.InputStreamReader     // Catch:{ all -> 0x0147 }
            java.io.InputStream r7 = r13.getInputStream()     // Catch:{ all -> 0x0147 }
            r12.<init>(r7)     // Catch:{ all -> 0x0147 }
            r3.<init>(r12)     // Catch:{ all -> 0x0147 }
        L_0x00a3:
            java.lang.String r12 = r3.readLine()     // Catch:{ all -> 0x00ad }
            if (r12 == 0) goto L_0x0101
            r1.append(r12)     // Catch:{ all -> 0x00ad }
            goto L_0x00a3
        L_0x00ad:
            r12 = move-exception
            r13 = r3
            r3 = r12
            r12 = r2
            r2 = r8
        L_0x00b2:
            if (r2 == 0) goto L_0x00b7
            r2.close()     // Catch:{ Exception -> 0x013d }
        L_0x00b7:
            if (r13 == 0) goto L_0x00bc
            r13.close()     // Catch:{ Exception -> 0x013d }
        L_0x00bc:
            throw r3     // Catch:{ Exception -> 0x00bd }
        L_0x00bd:
            r13 = move-exception
            r10 = r13
            r13 = r12
            r12 = r10
        L_0x00c1:
            java.lang.String r2 = "AdMob SDK"
            java.lang.String r3 = "Could not get ad from AdMob servers."
            android.util.Log.w(r2, r3, r12)
        L_0x00c8:
            java.lang.String r12 = r1.toString()
            if (r12 == 0) goto L_0x0157
            com.admob.android.ads.Ad r11 = com.admob.android.ads.Ad.createAd(r11, r12, r13)
        L_0x00d2:
            java.lang.String r12 = "AdMob SDK"
            r13 = 4
            boolean r12 = android.util.Log.isLoggable(r12, r13)
            if (r12 == 0) goto L_0x0100
            long r12 = java.lang.System.currentTimeMillis()
            long r12 = r12 - r5
            if (r11 != 0) goto L_0x0117
            java.lang.String r0 = "AdMob SDK"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Server replied that no ads are available ("
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r12 = r1.append(r12)
            java.lang.String r13 = "ms)"
            java.lang.StringBuilder r12 = r12.append(r13)
            java.lang.String r12 = r12.toString()
            android.util.Log.i(r0, r12)
        L_0x0100:
            return r11
        L_0x0101:
            java.lang.String r12 = "X-AdMob-Android-Category-Icon"
            java.lang.String r12 = r13.getHeaderField(r12)     // Catch:{ all -> 0x00ad }
            if (r12 != 0) goto L_0x010b
            java.lang.String r12 = "http://mm.admob.com/static/android/tiles/default.png"
        L_0x010b:
            if (r8 == 0) goto L_0x0110
            r8.close()     // Catch:{ Exception -> 0x0155 }
        L_0x0110:
            if (r3 == 0) goto L_0x0115
            r3.close()     // Catch:{ Exception -> 0x0155 }
        L_0x0115:
            r13 = r12
            goto L_0x00c8
        L_0x0117:
            java.lang.String r0 = "AdMob SDK"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Ad returned in "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r12 = r1.append(r12)
            java.lang.String r13 = "ms:  "
            java.lang.StringBuilder r12 = r12.append(r13)
            java.lang.StringBuilder r12 = r12.append(r11)
            java.lang.String r12 = r12.toString()
            android.util.Log.i(r0, r12)
            goto L_0x0100
        L_0x013a:
            r12 = move-exception
            r13 = r2
            goto L_0x00c1
        L_0x013d:
            r13 = move-exception
            goto L_0x00bc
        L_0x0140:
            r12 = move-exception
            r3 = r12
            r13 = r4
            r12 = r2
            r2 = r7
            goto L_0x00b2
        L_0x0147:
            r12 = move-exception
            r3 = r12
            r13 = r4
            r12 = r2
            r2 = r8
            goto L_0x00b2
        L_0x014e:
            r13 = move-exception
            r2 = r8
            r10 = r3
            r3 = r13
            r13 = r10
            goto L_0x00b2
        L_0x0155:
            r13 = move-exception
            goto L_0x0115
        L_0x0157:
            r11 = r0
            goto L_0x00d2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.admob.android.ads.AdRequester.requestAd(android.content.Context, java.lang.String, java.lang.String):com.admob.android.ads.Ad");
    }

    static String buildParamString(Context context, String keywords, String searchQuery) {
        StringBuilder post = new StringBuilder();
        long now = System.currentTimeMillis();
        post.append("z").append("=").append(now / 1000).append(".").append(now % 1000);
        appendParams(post, "rt", "0");
        String publisherID = AdManager.getPublisherId(context);
        if (publisherID == null) {
            throw new IllegalStateException("Publisher ID is not set!  To serve ads you must set your publisher ID assigned from www.admob.com.  Either add it to AndroidManifest.xml under the <application> tag or call AdManager.setPublisherId().");
        }
        appendParams(post, "s", publisherID);
        appendParams(post, "f", "html_no_js");
        appendParams(post, "y", "text");
        appendParams(post, "client_sdk", "1");
        appendParams(post, "ex", "1");
        appendParams(post, "v", AdManager.SDK_VERSION);
        appendParams(post, "t", AdManager.getUserId(context));
        appendParams(post, "d[coord]", AdManager.getCoordinatesAsString(context));
        appendParams(post, "d[dob]", AdManager.getBirthdayAsString());
        appendParams(post, "k", keywords);
        appendParams(post, "search", searchQuery);
        if (AdManager.isInTestMode()) {
            appendParams(post, "m", "test");
        }
        return post.toString();
    }

    private static void appendParams(StringBuilder param, String key, String val) {
        if (val != null && val.length() > 0) {
            try {
                param.append("&").append(URLEncoder.encode(key, "UTF-8")).append("=").append(URLEncoder.encode(val, "UTF-8"));
            } catch (UnsupportedEncodingException e) {
                Log.e("AdMob SDK", "UTF-8 encoding is not supported on this device.  Ad requests are impossible.", e);
            }
        }
    }
}
