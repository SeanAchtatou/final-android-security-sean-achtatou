package com.admob.android.ads;

import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.Log;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.widget.RelativeLayout;
import java.util.Timer;
import java.util.TimerTask;

public class AdView extends RelativeLayout {
    private static final String ADMOB_EMULATOR_NOTICE = "http://api.admob.com/v1/pubcode/android_sdk_emulator_notice";
    private static final int ANIMATION_DURATION = 700;
    private static final float ANIMATION_Z_DEPTH_PERCENTAGE = -0.4f;
    public static final int HEIGHT = 48;
    private static boolean checkedForMessages = false;
    /* access modifiers changed from: private */
    public static Handler uiThreadHandler;
    /* access modifiers changed from: private */
    public AdContainer ad;
    private int backgroundColor;
    private boolean hideWhenNoAd;
    private boolean isOnScreen;
    /* access modifiers changed from: private */
    public String keywords;
    /* access modifiers changed from: private */
    public AdListener listener;
    /* access modifiers changed from: private */
    public int requestInterval;
    private Timer requestIntervalTimer;
    /* access modifiers changed from: private */
    public boolean requestingFreshAd;
    /* access modifiers changed from: private */
    public String searchQuery;
    private int textColor;

    public interface AdListener {
        void onFailedToReceiveAd(AdView adView);

        @Deprecated
        void onNewAd();

        void onReceiveAd(AdView adView);
    }

    public AdView(Context context) {
        this(context, null, 0);
    }

    public AdView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public AdView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.isOnScreen = false;
        setFocusable(true);
        setDescendantFocusability(262144);
        setClickable(true);
        int tc = -1;
        int bc = AdContainer.DEFAULT_BACKGROUND_COLOR;
        if (attrs != null) {
            String namespace = "http://schemas.android.com/apk/res/" + context.getPackageName();
            boolean testing = attrs.getAttributeBooleanValue(namespace, "testing", false);
            if (testing) {
                AdManager.setInTestMode(testing);
            }
            tc = attrs.getAttributeUnsignedIntValue(namespace, "textColor", -1);
            bc = attrs.getAttributeUnsignedIntValue(namespace, "backgroundColor", AdContainer.DEFAULT_BACKGROUND_COLOR);
            this.keywords = attrs.getAttributeValue(namespace, "keywords");
            setRequestInterval(attrs.getAttributeIntValue(namespace, "refreshInterval", 0));
            setGoneWithoutAd(attrs.getAttributeBooleanValue(namespace, "isGoneWithoutAd", isGoneWithoutAd()));
        }
        setTextColor(tc);
        setBackgroundColor(bc);
        if (super.getVisibility() == 0) {
            requestFreshAd();
        }
    }

    public void requestFreshAd() {
        Context context = getContext();
        if (AdManager.getUserId(context) == null && !checkedForMessages) {
            checkedForMessages = true;
            retrieveDeveloperMessage(context);
        }
        if (super.getVisibility() != 0) {
            Log.w("AdMob SDK", "Cannot requestFreshAd() when the AdView is not visible.  Call AdView.setVisibility(View.VISIBLE) first.");
        } else if (!this.requestingFreshAd) {
            this.requestingFreshAd = true;
            if (uiThreadHandler == null) {
                uiThreadHandler = new Handler();
            }
            new Thread() {
                public void run() {
                    final boolean firstAd;
                    try {
                        Context context = AdView.this.getContext();
                        Ad newAd = AdRequester.requestAd(context, AdView.this.keywords, AdView.this.searchQuery);
                        if (newAd != null) {
                            synchronized (this) {
                                if (AdView.this.ad == null || !newAd.equals(AdView.this.ad.getAd()) || AdManager.isInTestMode()) {
                                    if (AdView.this.ad == null) {
                                        firstAd = true;
                                    } else {
                                        firstAd = false;
                                    }
                                    final int visibility = AdView.super.getVisibility();
                                    final AdContainer newAdContainer = new AdContainer(newAd, context);
                                    newAdContainer.setBackgroundColor(AdView.this.getBackgroundColor());
                                    newAdContainer.setTextColor(AdView.this.getTextColor());
                                    newAdContainer.setVisibility(visibility);
                                    newAdContainer.setLayoutParams(new RelativeLayout.LayoutParams(-1, 48));
                                    if (AdView.this.listener != null) {
                                        try {
                                            AdView.this.listener.onNewAd();
                                            AdView.this.listener.onReceiveAd(AdView.this);
                                        } catch (Exception e) {
                                            Log.w("AdMob SDK", "Unhandled exception raised in your AdListener.onReceiveAd.", e);
                                        }
                                    }
                                    AdView.uiThreadHandler.post(new Runnable() {
                                        public void run() {
                                            try {
                                                AdView.this.addView(newAdContainer);
                                                if (visibility != 0) {
                                                    AdContainer unused = AdView.this.ad = newAdContainer;
                                                } else if (firstAd) {
                                                    AdView.this.applyFadeIn(newAdContainer);
                                                } else {
                                                    AdView.this.applyRotation(newAdContainer);
                                                }
                                            } catch (Exception e) {
                                                Log.e("AdMob SDK", "Unhandled exception placing AdContainer into AdView.", e);
                                            } finally {
                                                boolean unused2 = AdView.this.requestingFreshAd = false;
                                            }
                                        }
                                    });
                                } else {
                                    if (Log.isLoggable("AdMob SDK", 3)) {
                                        Log.d("AdMob SDK", "Received the same ad we already had.  Discarding it.");
                                    }
                                    boolean unused = AdView.this.requestingFreshAd = false;
                                }
                            }
                            return;
                        }
                        if (AdView.this.listener != null) {
                            try {
                                AdView.this.listener.onFailedToReceiveAd(AdView.this);
                            } catch (Exception e2) {
                                Log.w("AdMob SDK", "Unhandled exception raised in your AdListener.onFailedToReceiveAd.", e2);
                            }
                        }
                        boolean unused2 = AdView.this.requestingFreshAd = false;
                        return;
                    } catch (Exception e3) {
                        Log.e("AdMob SDK", "Unhandled exception requesting a fresh ad.", e3);
                        boolean unused3 = AdView.this.requestingFreshAd = false;
                    }
                }
            }.start();
        } else if (Log.isLoggable("AdMob SDK", 3)) {
            Log.d("AdMob SDK", "Ignoring requestFreshAd() because we are already getting a fresh ad.");
        }
    }

    public int getRequestInterval() {
        return this.requestInterval / 1000;
    }

    public void setRequestInterval(int requestInterval2) {
        if (requestInterval2 <= 0) {
            requestInterval2 = 0;
        } else if (requestInterval2 < 15) {
            AdManager.clientError("AdView.setRequestInterval(" + requestInterval2 + ") seconds must be >= " + 15);
        } else if (requestInterval2 > 600) {
            AdManager.clientError("AdView.setRequestInterval(" + requestInterval2 + ") seconds must be <= " + 600);
        }
        this.requestInterval = requestInterval2 * 1000;
        if (requestInterval2 == 0) {
            manageRequestIntervalTimer(false);
            return;
        }
        Log.i("AdMob SDK", "Requesting fresh ads every " + requestInterval2 + " seconds.");
        manageRequestIntervalTimer(true);
    }

    private void manageRequestIntervalTimer(boolean start) {
        synchronized (this) {
            if (start) {
                if (this.requestInterval > 0) {
                    if (this.requestIntervalTimer == null) {
                        this.requestIntervalTimer = new Timer();
                        this.requestIntervalTimer.schedule(new TimerTask() {
                            public void run() {
                                if (Log.isLoggable("AdMob SDK", 3)) {
                                    int secs = AdView.this.requestInterval / 1000;
                                    if (Log.isLoggable("AdMob SDK", 3)) {
                                        Log.d("AdMob SDK", "Requesting a fresh ad because a request interval passed (" + secs + " seconds).");
                                    }
                                }
                                AdView.this.requestFreshAd();
                            }
                        }, (long) this.requestInterval, (long) this.requestInterval);
                    }
                }
            }
            if ((!start || this.requestInterval == 0) && this.requestIntervalTimer != null) {
                this.requestIntervalTimer.cancel();
                this.requestIntervalTimer = null;
            }
        }
    }

    public void onWindowFocusChanged(boolean hasWindowFocus) {
        manageRequestIntervalTimer(hasWindowFocus);
    }

    public void setTextColor(int color) {
        this.textColor = -16777216 | color;
        if (this.ad != null) {
            this.ad.setTextColor(color);
        }
        invalidate();
    }

    public int getTextColor() {
        return this.textColor;
    }

    public void setBackgroundColor(int color) {
        this.backgroundColor = -16777216 | color;
        if (this.ad != null) {
            this.ad.setBackgroundColor(color);
        }
        invalidate();
    }

    public int getBackgroundColor() {
        return this.backgroundColor;
    }

    public String getKeywords() {
        return this.keywords;
    }

    public void setKeywords(String keywords2) {
        this.keywords = keywords2;
    }

    public String getSearchQuery() {
        return this.searchQuery;
    }

    public void setSearchQuery(String searchQuery2) {
        this.searchQuery = searchQuery2;
    }

    public void setGoneWithoutAd(boolean hide) {
        this.hideWhenNoAd = hide;
    }

    public boolean isGoneWithoutAd() {
        return this.hideWhenNoAd;
    }

    public void setVisibility(int visibility) {
        if (super.getVisibility() != visibility) {
            synchronized (this) {
                int children = getChildCount();
                for (int i = 0; i < children; i++) {
                    getChildAt(i).setVisibility(visibility);
                }
                super.setVisibility(visibility);
                if (visibility == 0) {
                    requestFreshAd();
                } else {
                    removeView(this.ad);
                    this.ad = null;
                    invalidate();
                }
            }
        }
    }

    public int getVisibility() {
        if (!this.hideWhenNoAd || hasAd()) {
            return super.getVisibility();
        }
        return 8;
    }

    public void setListener(AdListener listener2) {
        synchronized (this) {
            this.listener = listener2;
        }
    }

    public boolean hasAd() {
        return this.ad != null;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        setMeasuredDimension(getMeasuredWidth(), 48);
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        this.isOnScreen = true;
        super.onAttachedToWindow();
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        this.isOnScreen = false;
        super.onDetachedFromWindow();
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x004a A[SYNTHETIC, Splitter:B:10:0x004a] */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0076 A[SYNTHETIC, Splitter:B:24:0x0076] */
    /* JADX WARNING: Removed duplicated region for block: B:33:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void retrieveDeveloperMessage(android.content.Context r13) {
        /*
            r12 = this;
            r7 = 0
            r10 = 0
            r11 = 0
            java.lang.String r6 = com.admob.android.ads.AdRequester.buildParamString(r13, r10, r11)     // Catch:{ Exception -> 0x0081, all -> 0x0073 }
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0081, all -> 0x0073 }
            r9.<init>()     // Catch:{ Exception -> 0x0081, all -> 0x0073 }
            java.lang.String r10 = "http://api.admob.com/v1/pubcode/android_sdk_emulator_notice"
            r9.append(r10)     // Catch:{ Exception -> 0x0081, all -> 0x0073 }
            java.lang.String r10 = "?"
            r9.append(r10)     // Catch:{ Exception -> 0x0081, all -> 0x0073 }
            r9.append(r6)     // Catch:{ Exception -> 0x0081, all -> 0x0073 }
            java.net.URL r2 = new java.net.URL     // Catch:{ Exception -> 0x0081, all -> 0x0073 }
            java.lang.String r10 = r9.toString()     // Catch:{ Exception -> 0x0081, all -> 0x0073 }
            r2.<init>(r10)     // Catch:{ Exception -> 0x0081, all -> 0x0073 }
            java.net.URLConnection r0 = r2.openConnection()     // Catch:{ Exception -> 0x0081, all -> 0x0073 }
            r0.connect()     // Catch:{ Exception -> 0x0081, all -> 0x0073 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0081, all -> 0x0073 }
            r1.<init>()     // Catch:{ Exception -> 0x0081, all -> 0x0073 }
            java.io.BufferedReader r8 = new java.io.BufferedReader     // Catch:{ Exception -> 0x0081, all -> 0x0073 }
            java.io.InputStreamReader r10 = new java.io.InputStreamReader     // Catch:{ Exception -> 0x0081, all -> 0x0073 }
            java.io.InputStream r11 = r0.getInputStream()     // Catch:{ Exception -> 0x0081, all -> 0x0073 }
            r10.<init>(r11)     // Catch:{ Exception -> 0x0081, all -> 0x0073 }
            r8.<init>(r10)     // Catch:{ Exception -> 0x0081, all -> 0x0073 }
        L_0x003c:
            java.lang.String r4 = r8.readLine()     // Catch:{ Exception -> 0x0046, all -> 0x007e }
            if (r4 == 0) goto L_0x004e
            r1.append(r4)     // Catch:{ Exception -> 0x0046, all -> 0x007e }
            goto L_0x003c
        L_0x0046:
            r10 = move-exception
            r7 = r8
        L_0x0048:
            if (r7 == 0) goto L_0x004d
            r7.close()     // Catch:{ Exception -> 0x007a }
        L_0x004d:
            return
        L_0x004e:
            org.json.JSONObject r3 = new org.json.JSONObject     // Catch:{ Exception -> 0x0046, all -> 0x007e }
            org.json.JSONTokener r10 = new org.json.JSONTokener     // Catch:{ Exception -> 0x0046, all -> 0x007e }
            java.lang.String r11 = r1.toString()     // Catch:{ Exception -> 0x0046, all -> 0x007e }
            r10.<init>(r11)     // Catch:{ Exception -> 0x0046, all -> 0x007e }
            r3.<init>(r10)     // Catch:{ Exception -> 0x0046, all -> 0x007e }
            java.lang.String r10 = "data"
            java.lang.String r5 = r3.getString(r10)     // Catch:{ Exception -> 0x0046, all -> 0x007e }
            if (r5 == 0) goto L_0x0069
            java.lang.String r10 = "AdMob SDK"
            android.util.Log.e(r10, r5)     // Catch:{ Exception -> 0x0046, all -> 0x007e }
        L_0x0069:
            if (r8 == 0) goto L_0x006e
            r8.close()     // Catch:{ Exception -> 0x0070 }
        L_0x006e:
            r7 = r8
            goto L_0x004d
        L_0x0070:
            r10 = move-exception
            r7 = r8
            goto L_0x004d
        L_0x0073:
            r10 = move-exception
        L_0x0074:
            if (r7 == 0) goto L_0x0079
            r7.close()     // Catch:{ Exception -> 0x007c }
        L_0x0079:
            throw r10
        L_0x007a:
            r10 = move-exception
            goto L_0x004d
        L_0x007c:
            r11 = move-exception
            goto L_0x0079
        L_0x007e:
            r10 = move-exception
            r7 = r8
            goto L_0x0074
        L_0x0081:
            r10 = move-exception
            goto L_0x0048
        */
        throw new UnsupportedOperationException("Method not decompiled: com.admob.android.ads.AdView.retrieveDeveloperMessage(android.content.Context):void");
    }

    /* access modifiers changed from: private */
    public void applyFadeIn(AdContainer newAd) {
        this.ad = newAd;
        if (this.isOnScreen) {
            AlphaAnimation animation = new AlphaAnimation(0.0f, 1.0f);
            animation.setDuration(233);
            animation.startNow();
            animation.setFillAfter(true);
            animation.setInterpolator(new AccelerateInterpolator());
            startAnimation(animation);
        }
    }

    /* access modifiers changed from: private */
    public void applyRotation(final AdContainer newAd) {
        newAd.setVisibility(8);
        Rotate3dAnimation rotation = new Rotate3dAnimation(0.0f, -90.0f, ((float) getWidth()) / 2.0f, ((float) getHeight()) / 2.0f, ANIMATION_Z_DEPTH_PERCENTAGE * ((float) getWidth()), true);
        rotation.setDuration(700);
        rotation.setFillAfter(true);
        rotation.setInterpolator(new AccelerateInterpolator());
        rotation.setAnimationListener(new Animation.AnimationListener() {
            public void onAnimationStart(Animation animation) {
            }

            public void onAnimationEnd(Animation animation) {
                AdView.this.post(new SwapViews(newAd));
            }

            public void onAnimationRepeat(Animation animation) {
            }
        });
        startAnimation(rotation);
    }

    private final class SwapViews implements Runnable {
        /* access modifiers changed from: private */
        public AdContainer newAd;
        /* access modifiers changed from: private */
        public AdContainer oldAd;

        public SwapViews(AdContainer newAd2) {
            this.newAd = newAd2;
        }

        public void run() {
            this.oldAd = AdView.this.ad;
            if (this.oldAd != null) {
                this.oldAd.setVisibility(8);
            }
            this.newAd.setVisibility(0);
            Rotate3dAnimation rotation = new Rotate3dAnimation(90.0f, 0.0f, ((float) AdView.this.getWidth()) / 2.0f, ((float) AdView.this.getHeight()) / 2.0f, AdView.ANIMATION_Z_DEPTH_PERCENTAGE * ((float) AdView.this.getWidth()), false);
            rotation.setDuration(700);
            rotation.setFillAfter(true);
            rotation.setInterpolator(new DecelerateInterpolator());
            rotation.setAnimationListener(new Animation.AnimationListener() {
                public void onAnimationStart(Animation animation) {
                }

                public void onAnimationEnd(Animation animation) {
                    if (SwapViews.this.oldAd != null) {
                        AdView.this.removeView(SwapViews.this.oldAd);
                        SwapViews.this.oldAd.recycleBitmaps();
                    }
                    AdContainer unused = AdView.this.ad = SwapViews.this.newAd;
                }

                public void onAnimationRepeat(Animation animation) {
                }
            });
            AdView.this.startAnimation(rotation);
        }
    }
}
