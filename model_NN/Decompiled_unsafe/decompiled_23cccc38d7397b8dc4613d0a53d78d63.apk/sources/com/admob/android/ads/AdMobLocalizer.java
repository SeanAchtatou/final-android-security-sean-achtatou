package com.admob.android.ads;

import android.content.Context;
import android.util.Log;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Locale;
import java.util.Properties;

class AdMobLocalizer {
    /* access modifiers changed from: private */
    public static String ADMOB_LOCALIZATION_CACHE_DIR = "admob_cache";
    /* access modifiers changed from: private */
    public static String ADMOB_LOCALIZATION_URL_STRING = "http://mm.admob.com/static/android/i18n/20091123";
    private static String DEFAULT_LANGUAGE = "en";
    /* access modifiers changed from: private */
    public static String PROPS_FILE_SUFFIX = ".properties";
    private static AdMobLocalizer singleton = null;
    private static Context staticContext = null;
    /* access modifiers changed from: private */
    public Context context;
    /* access modifiers changed from: private */
    public String language = null;
    private Properties strings;

    public static void init(Context c) {
        if (staticContext == null) {
            staticContext = c;
        }
        initSingleton();
    }

    private static void initSingleton() {
        if (singleton == null) {
            singleton = new AdMobLocalizer(staticContext);
        }
    }

    public static AdMobLocalizer singleton() {
        initSingleton();
        return singleton;
    }

    public static String localize(String key) {
        init(staticContext);
        return singleton.internalLocalize(key);
    }

    private AdMobLocalizer(Context c) {
        this.context = c;
        Locale locale = Locale.getDefault();
        setLanguage(locale.getLanguage());
    }

    public void setLanguage(String langCode) {
        if (langCode != this.language) {
            this.strings = null;
            this.language = langCode;
            if (this.language == null || this.language.equals("")) {
                this.language = DEFAULT_LANGUAGE;
            }
            if (!loadStrings()) {
                new InitLocalizerThread().start();
            }
        }
    }

    public String getLanguage() {
        return this.language;
    }

    private boolean loadStrings() {
        if (this.strings == null) {
            try {
                Properties temp = new Properties();
                File versionedDir = new File(this.context.getDir(ADMOB_LOCALIZATION_CACHE_DIR, 0), "20091123");
                if (!versionedDir.exists()) {
                    versionedDir.mkdir();
                }
                File propsFile = new File(versionedDir, this.language + PROPS_FILE_SUFFIX);
                if (propsFile.exists()) {
                    temp.load(new FileInputStream(propsFile));
                    this.strings = temp;
                }
            } catch (IOException e) {
                this.strings = null;
            }
        }
        if (this.strings != null) {
            return true;
        }
        return false;
    }

    private String internalLocalize(String key) {
        loadStrings();
        String localizedString = key;
        if (this.strings == null) {
            return localizedString;
        }
        String localizedString2 = this.strings.getProperty(key);
        if (localizedString2 == null || localizedString2.equals("")) {
            return key;
        }
        return localizedString2;
    }

    private class InitLocalizerThread extends Thread {
        private InitLocalizerThread() {
        }

        public void run() {
            BufferedOutputStream os;
            try {
                URLConnection urlConnection = new URL(AdMobLocalizer.ADMOB_LOCALIZATION_URL_STRING + "/" + AdMobLocalizer.this.language + AdMobLocalizer.PROPS_FILE_SUFFIX).openConnection();
                urlConnection.connect();
                BufferedInputStream is = new BufferedInputStream(urlConnection.getInputStream());
                File versionedDir = new File(AdMobLocalizer.this.context.getDir(AdMobLocalizer.ADMOB_LOCALIZATION_CACHE_DIR, 0), "20091123");
                if (!versionedDir.exists()) {
                    versionedDir.mkdir();
                }
                os = new BufferedOutputStream(new FileOutputStream(new File(versionedDir, AdMobLocalizer.this.language + AdMobLocalizer.PROPS_FILE_SUFFIX)));
                byte[] buffer = new byte[512];
                while (true) {
                    int bytes_read = is.read(buffer, 0, buffer.length);
                    if (bytes_read > 0) {
                        os.write(buffer, 0, bytes_read);
                    } else {
                        os.close();
                        return;
                    }
                }
            } catch (Exception e) {
                if (Log.isLoggable("AdMob SDK", 3)) {
                    Log.d("AdMob SDK", "Could not get localized strings from the AdMob servers.");
                }
            } catch (Throwable th) {
                os.close();
                throw th;
            }
        }
    }
}
