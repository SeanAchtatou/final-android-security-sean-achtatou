package com.iPhand.FirstAid;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;

public class DBUtil {
    private static final String DATABASE_FILENAME = "firstaid.sqlite";
    private static final String DATABASE_PATH = "/data/data/com.iPhand.FirstAid/databases";

    public static SQLiteDatabase openDatabase(Context context) {
        try {
            File dir = new File(DATABASE_PATH);
            File d = new File(DATABASE_PATH);
            if (!d.exists()) {
                d.mkdir();
            }
            if (!dir.exists()) {
                dir.mkdir();
            }
            if (!new File("/data/data/com.iPhand.FirstAid/databases/firstaid.sqlite").exists()) {
                InputStream is = context.getResources().openRawResource(R.raw.firstaid);
                FileOutputStream fos = new FileOutputStream("/data/data/com.iPhand.FirstAid/databases/firstaid.sqlite");
                byte[] buffer = new byte[8192];
                while (true) {
                    int count = is.read(buffer);
                    if (count <= 0) {
                        break;
                    }
                    fos.write(buffer, 0, count);
                }
                fos.close();
                is.close();
            }
            return SQLiteDatabase.openOrCreateDatabase("/data/data/com.iPhand.FirstAid/databases/firstaid.sqlite", (SQLiteDatabase.CursorFactory) null);
        } catch (Exception e) {
            return null;
        }
    }
}
